
/*---------------------------------------Custom proc starts here---------------------------*/

/* Run this just before populating fact_planorder statement  */
/* ( for another proc, create a similar custom proc and run that before the actual proc ) */


/* Populate tmp_getStdPrice - This will change for each proc where this function is required*/

DELETE FROM tmp_getStdPrice
WHERE fact_script_name = 'bi_populate_planorder_fact';


INSERT INTO tmp_getStdPrice
(
 pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
SELECT DISTINCT
        pl.CompanyCode,
        pl.PlantCode,
        p.PLAF_MATNR,
        dd.CalendarYear,
        dd.FinancialMonthNumber,
        CASE WHEN p.PLAF_UMREZ = 0 THEN 1 ELSE PLAF_UMREZ END,
        CASE WHEN p.PLAF_UMREN = 0 THEN 1 ELSE PLAF_UMREN END,
        NULL,
        0,
	'bi_populate_planorder_fact'
FROM plaf p
          INNER JOIN dim_plant pl
             ON pl.PlantCode = p.PLAF_PLWRK
          INNER JOIN dim_company dc
             ON dc.CompanyCode = pl.CompanyCode
          INNER JOIN Dim_Currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date dd
             ON dd.DateValue = p.PLAF_PSTTR
                AND pl.CompanyCode = dd.CompanyCode
          INNER JOIN dim_part dp
             ON     dp.PartNumber = p.PLAF_MATNR
                AND dp.Plant = p.PLAF_PLWRK
          INNER JOIN dim_unitofmeasure uom
             ON uom.UOM = p.plaf_meins
          INNER JOIN dim_purchaseorg pog
             ON pog.PurchaseOrgCode = pl.PurchOrg
WHERE pl.RowIsCurrent = 1;


UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE fact_script_name = 'bi_populate_planorder_fact';


