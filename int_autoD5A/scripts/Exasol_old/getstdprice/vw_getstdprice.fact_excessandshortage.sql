

/**************************************************************************************************************/
/*   Script         : vw_getstdprice.fact_excessandshortage.sql	 */
/*   Author         : Lokesh */
/*   Created On     : 5 Aug 2013 */
/*   Description    : Function to populate tmp_getStdPrice for getstdprice  */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   5 Aug 2013      Lokesh    1.0               New script                          */
/*   23 Sep 2013     Ashu      2.0               Query Optimization */
/******************************************************************************************************************/

/* Stdprice script for fact_excessandshortage starts here */

/* Populate tmp_getStdPrice  This will change for each proc where this function is required*/

DROP TABLE IF EXISTS tmp_getStdPrice_fact_ex_sh;
CREATE TABLE tmp_getStdPrice_fact_ex_sh 
as
SELECT * FROM tmp_getStdPrice
WHERE 1=2;

drop table if exists mdtb_pi00;

Create table mdtb_pi00 as Select m.*,ifnull(MDTB_UMDAT, MDTB_DAT01) dateval_upd from mdtb m
Where ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL ;

drop table if exists plaf_pi00;

Create table plaf_pi00 as
Select PLAF_UMREZ,
        PLAF_UMREN,
        PLAF_PLNUM,
        ifnull(PLAF_EMLIF, 'Not Set') PLAF_EMLIF,
        ifnull(PLAF_FLIEF, 'Not Set') PLAF_FLIEF,
        ifnull(PLAF_KZVBR, 'Not Set') PLAF_KZVBR
from plaf
Where ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL;


DELETE FROM tmp_getStdPrice
WHERE fact_script_name = 'fact_excessandshortage';


INSERT INTO tmp_getStdPrice_fact_ex_sh
(
 pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
SELECT distinct 
pl.CompanyCode,
pl.PlantCode,
MDKP_MATNR,
dd.CalendarYear,
dd.FinancialMonthNumber,
PLAF_UMREZ,
PLAF_UMREN,
null,
0,
'fact_excessandshortage'
FROM  plaf_pi00 po,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_vendor fv,
       dim_consumptiontype ct,
       dim_part p,
       dim_plant pl,
       Dim_Date dd,
	   fact_excessandshortage m
WHERE     m.Dim_ActionStateid = 2
AND k.MDKP_DTNUM = t.MDTB_DTNUM
AND dd.Dim_Dateid <> 1
AND m.dd_DocumentNo = po.PLAF_PLNUM
AND m.dd_DocumentNo = t.MDTB_DELNR
AND p.Dim_Partid = m.Dim_Partid
AND pl.PlantCode = p.Plant
AND v.VendorNumber = PLAF_EMLIF
AND v.RowIsCurrent = 1
AND fv.VendorNumber = PLAF_FLIEF
AND fv.RowIsCurrent = 1
AND ct.ConsumptionCode = po.PLAF_KZVBR
AND ct.RowIsCurrent = 1
AND dd.DateValue = dateval_upd
AND dd.CompanyCode = pl.CompanyCode;		 
	
INSERT INTO tmp_getStdPrice
SELECT DISTINCT * FROM tmp_getStdPrice_fact_ex_sh;

DROP TABLE IF EXISTS tmp_getStdPrice_fact_ex_sh;					  
drop table if exists mdtb_pi00;
drop table if exists plaf_pi00;


UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE fact_script_name = 'fact_excessandshortage';

