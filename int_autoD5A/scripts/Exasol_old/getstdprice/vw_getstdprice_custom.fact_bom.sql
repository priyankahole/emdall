
/* Custom proc starts here */

/* Run this just before bi_process_bom_fact  */
/* ( for another proc, create a similar custom proc and run that before the actual proc ) */


/* Populate tmp_getStdPrice  This will change for each proc where this function is required*/

DROP TABLE IF EXISTS tmp_getStdPrice_q2;
CREATE TABLE tmp_getStdPrice_q2 
like 
tmp_getStdPrice INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM tmp_getStdPrice
WHERE fact_script_name = 'bi_process_bom_fact';


INSERT INTO tmp_getStdPrice_q2
(
 pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
SELECT DISTINCT
pl.CompanyCode,
p.plant,
p.PartNumber,
dd.FinancialYear,
dd.FinancialMonthNumber,
1,
1,
NULL,
amt_PriceUnit,
'bi_process_bom_fact'
FROM 
fact_bom bom, dim_part p, dim_plant pl, dim_date dd
WHERE     bom.Dim_BOMComponentId = p.Dim_Partid
AND pl.PlantCode = p.Plant
AND pl.RowIsCurrent = 1
AND bom.Dim_ValidFromDateid = dd.Dim_Dateid
AND p.RowIsCurrent = 1
AND bom.Dim_BOMComponentId <> 1
AND pl.Dim_PlantId <> 1;

/* Having 2 separate queries here as UNION did not support NULL  */

INSERT INTO tmp_getStdPrice_q2
(
 pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
SELECT DISTINCT 
pl.CompanyCode,
pl.PlantCode,
p.PartNumber,
dd.FinancialYear,
dd.FinancialMonthNumber,
1,
1,
NULL,
amt_PriceUnit,
'bi_process_bom_fact'
FROM fact_bom bom, dim_part p, dim_plant pl, dim_date dd
WHERE     bom.Dim_PartId = p.Dim_Partid
AND bom.Dim_MaterialPlantId = pl.Dim_PlantId
AND pl.PlantCode = p.Plant
AND pl.RowIsCurrent = 1
AND bom.Dim_ValidFromDateid = dd.Dim_Dateid
AND p.RowIsCurrent = 1
AND bom.Dim_PartId <> 1
AND bom.Dim_MaterialPlantId <> 1;

INSERT INTO tmp_getStdPrice
SELECT DISTINCT * FROM tmp_getStdPrice_q2;

DROP TABLE IF EXISTS tmp_getStdPrice_q2;					  


UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE fact_script_name = 'bi_process_bom_fact';


