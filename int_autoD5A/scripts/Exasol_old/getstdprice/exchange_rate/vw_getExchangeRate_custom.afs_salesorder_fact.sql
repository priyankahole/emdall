/*********************************************Change History*******************************************************/
/* Date             By             Version           Desc                                                         */
/* 03 Mar 2015      Liviu Ionescu                    Add Combine for table tmp_getExchangeRate1 */
/******************************************************************************************************************/

/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */


/* LATEST VERSION AS OF 29th JAN - TESTED WITH VARIAN,KLA AND ALBEA	*/
/* This is specific to bi_populate_afs_salesorder_fact ( Change required at one place - while initially populating tmp_getExchangeRate1 ) */
/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

drop table if exists tmp_getExchangeRate1;

CREATE TABLE tmp_getExchangeRate1
(
pFromCurrency  varchar(3),
pToCurrency   varchar(3),
pFromExchangeRate decimal(9, 5),
pDate    date,
exchangeRate   decimal(9, 5),
exchangeRate1   decimal(9, 5),
vType  varchar(4),
vRefCur  varchar(3),
vFromCurr  varchar(3),
vToCurr  varchar(3),
vInvAllow  varchar(1),
vReverse  varchar(1),  
vFfact     int,
vTfact     int,
updflag  varchar(1),
updflag2 varchar(1),
vTCURFExists int,
vFTfact int
);


/* DISTINCT is necessary as this query may return dups for the 4 fields.
Note that we are populating only those currencies in tmp_getExchangeRate1 which will be required by the actual queries 
Those queries(in inventoryaging sp ) have the same joins 
 INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,a.MARD_ERSDA as pDate, 'USD' pToCurrency, 1.00000 exchangeRate
FROM MARD a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MARD_WERKS = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency		*/



/* This section populates tmp_getExchangeRate1. Only this will change for different procs, remaining code will remain the same */
/******************************** START OF POPULATE tmp_getExchangeRate1 *********************************************************************************/
/* VBRK_VBRP is the base table from which data is populated. This can change according to different procs. e.g ivaging has different driving table */


/* VBAK_VBAP_VBEP is the base table from which data is populated. This can change according to different procs. e.g ivaging has different driving table */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pDate,pToCurrency,pFromExchangeRate,exchangeRate)
SELECT DISTINCT co.Currency,VBAK_AUDAT,'USD' pToCurrency,vbap_stcur,1.00000 exchangeRate
FROM VBAK_VBAP_VBEP,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
UNION
SELECT DISTINCT co.Currency,VBAK_AUDAT,s.property_value pToCurrency,vbap_stcur,1.00000 exchangeRate
FROM VBAK_VBAP_VBEP,
     Dim_Plant pl,
     Dim_Company co,
	 systemproperty s
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
AND s.property = 'customer.global.currency';

/* LI Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_getExchangeRate1');
