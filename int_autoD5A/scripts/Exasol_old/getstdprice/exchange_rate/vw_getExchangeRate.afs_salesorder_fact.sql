/*********************************************Change History*******************************************************/
/*  Date             By              Version           Desc                                                       */
/*  03 Mar 2015      Liviu Ionescu                     Add Combines for table tmp_getExchangeRate1 */
/******************************************************************************************************************/

/* LATEST VERSION AS OF 29th JAN - TESTED WITH VARIAN,KLA AND ALBEA	*/
/* This is specific to bi_populate_afs_salesorder_fact ( Change required at one place - while initially populating tmp_getExchangeRate1 ) */
/* set session authorization albea	*/
/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

drop table if exists tmp_getExchangeRate1;

CREATE TABLE tmp_getExchangeRate1
(
pFromCurrency  varchar(3),
pToCurrency   varchar(3),
pFromExchangeRate decimal(9, 5),
pDate    date,
exchangeRate   decimal(9, 5),
exchangeRate1   decimal(9, 5),
vType  varchar(4),
vRefCur  varchar(3),
vFromCurr  varchar(3),
vToCurr  varchar(3),
vInvAllow  varchar(1),
vReverse  varchar(1),  
vFfact     int,
vTfact     int,
updflag  varchar(1),
updflag2 varchar(1),
vTCURFExists int,
vFTfact int
);


/* DISTINCT is necessary as this query may return dups for the 4 fields.
Note that we are populating only those currencies in tmp_getExchangeRate1 which will be required by the actual queries 
Those queries(in inventoryaging sp ) have the same joins 
 INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,a.MARD_ERSDA as pDate, 'USD' pToCurrency, 1.00000 exchangeRate
FROM MARD a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MARD_WERKS = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency		*/


/* VBAK_VBAP_VBEP is the base table from which data is populated. This can change according to different procs. e.g ivaging has different driving table */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pDate,pToCurrency,pFromExchangeRate,exchangeRate)
SELECT DISTINCT co.Currency,VBAK_AUDAT,'USD' pToCurrency,vbap_stcur,1.00000 exchangeRate 
FROM VBAK_VBAP_VBEP,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

UPDATE tmp_getExchangeRate1
set exchangeRate = NULL;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD');

UPDATE tmp_getExchangeRate1
set vReverse = 'N',updflag = 'N';      


UPDATE tmp_getExchangeRate1
SET exchangeRate = 1,updflag = 'Y'              
WHERE pFromCurrency = pToCurrency;

/* For now pFromExchangeRate is null(We did not populate this in tmp ). As bi_populate_inventoryaging_fact does not supply this parm. 
For other procs that supply this parm, we might need to modify the current sql file or create another file, that can be called according 
to how the function is currently called by such procs */

UPDATE tmp_getExchangeRate1
SET vReverse = 'Y',
 pFromExchangeRate = -1 * pFromExchangeRate,
 vRefCur = pFromCurrency,
 pFromCurrency = pToCurrency,
 pToCurrency = vRefCur
WHERE updflag = 'N'
AND pFromExchangeRate < 0;


UPDATE  tmp_getExchangeRate1
SET vType = ifnull((select TCURV_KURST from tcurv ),'M'),
vRefCur = (select TCURV_BWAER from tcurv ),
vInvAllow = ifnull((select 'Y' from tcurv where TCURV_XINVR is not null),'N'),
vFromCurr = pFromCurrency,
vToCurr = pToCurrency,
vTCURFExists = ifnull((select count(*) from TCURF ),0)		
WHERE updflag = 'N'     ;


/* Prepare and use TCURF ( Moved this section up, so that data is available for Hiten's changes */ 

drop table if exists tmp_TCURF;
CREATE table tmp_TCURF
AS
SELECT  TCURF_FCURR,TCURF_TCURR,TCURF_KURST,TCURF_GDATU,TCURF_FFACT,TCURF_TFACT,
cast(left((99999999 - TCURF_GDATU),4) + '-' + left(right((99999999 - TCURF_GDATU),4),2) + '-' + right((99999999 - TCURF_GDATU),2) AS date) "cast_TCURF_GDATU"
FROM TCURF;

/* Now get the max value of cast_TCURF_GDATU for each row of tmp_TCURF, where cast_TCURF_GDATU <= g.pDate */ 

drop table if exists tmp2_TCURF;
CREATE TABLE tmp2_TCURF
AS
SELECT   TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate,max(cast_TCURF_GDATU) as max_cast_TCURF_GDATU
FROM tmp_TCURF T,tmp_getExchangeRate1 g
where T.TCURF_FCURR = g.vFromCurr and T.TCURF_TCURR = g.vToCurr and T.TCURF_KURST = g.vType
                                  and g.pDate >= cast_TCURF_GDATU
AND g.updflag = 'N'
group by TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate;

/*Wherever row with g.pDate >= cast_TCURF_GDATU was not found, use TIMESTAMP(LOCAL_TIMESTAMP)*/

INSERT INTO tmp2_TCURF
SELECT   TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate,max(cast_TCURF_GDATU) as max_cast_TCURF_GDATU
FROM tmp_TCURF T,tmp_getExchangeRate1 g
where T.TCURF_FCURR = g.vFromCurr and T.TCURF_TCURR = g.vToCurr and T.TCURF_KURST = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_TCURF_GDATU
AND g.updflag = 'N'
AND NOT EXISTS ( SELECT 1 FROM tmp2_TCURF t2 where t2.TCURF_FCURR = T.TCURF_FCURR and t2.TCURF_TCURR = T.TCURF_TCURR and
                                        t2.TCURF_KURST = T.TCURF_KURST AND g.pDate = t2.pDate )
group by TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate;



/*Update vFTfact. Note that there should be 1-1 mapping for g.pDate >= t2.max_cast_TCURF_GDATU due to the above queries*/

UPDATE tmp_getExchangeRate1 g
SET updflag = 'Z'
WHERE g.updflag = 'N'
AND g.pFromExchangeRate > 0
AND g.vTCURFExists > 0;

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vFTfact = decimal(t.TCURF_FFACT * t.TCURF_TFACT,19,5)
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND g.updflag = 'Z';

-- This is to take care of rows which we had stored using TIMESTAMP(LOCAL_TIMESTAMP) >= cast_TCURF_GDATU

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vFTfact = decimal(t.TCURF_FFACT * t.TCURF_TFACT,19,5)
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate 
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND g.updflag = 'Z';


UPDATE tmp_getExchangeRate1 g
SET vFTfact = 1
where vFTfact IS NULL
AND g.updflag = 'Z';


UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate * vFTfact
WHERE vReverse = 'Y'
AND g.updflag = 'Z';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate / vFTfact
WHERE vReverse <> 'Y'
AND g.updflag = 'Z';

/* End of Hiten's changes1 18th Jan	*/

UPDATE  tmp_getExchangeRate1
set vToCurr = vRefCur
where vRefCur is not null AND vRefCur <> pToCurrency
AND updflag = 'N';


UPDATE  tmp_getExchangeRate1
set vReverse = 'Y', vToCurr = pFromCurrency, pFromCurrency = pToCurrency,pToCurrency = vToCurr,vFromCurr = pFromCurrency
where vRefCur is not null AND vRefCur <> pToCurrency
AND vReverse = 'N' AND vRefCur = pFromCurrency
AND updflag = 'N';

/* LI Start Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_getExchangeRate1');
/* End Changes 03 Mar 2015 */

/*
Section 2
Update vFfact. Note that there should be 1-1 mapping for g.pDate >= t2.max_cast_TCURF_GDATU
due to the above queries */

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vFfact = t.TCURF_FFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND updflag = 'N';

/* This is to take care of rows which we had stored using TIMESTAMP(LOCAL_TIMESTAMP) >= cast_TCURF_GDATU*/

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vFfact = t.TCURF_FFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND g.vFfact IS NULL
AND updflag = 'N';


UPDATE tmp_getExchangeRate1
SET vFfact = 1
where vFfact IS NULL
AND updflag = 'N';

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vTfact = t.TCURF_TFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND updflag = 'N';


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vTfact = t.TCURF_TFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND g.vTfact IS NULL
AND updflag = 'N';

UPDATE tmp_getExchangeRate1
SET vTfact = 1
where vTfact IS NULL
AND updflag = 'N';

/*Block 1 ( vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0 )*/


UPDATE tmp_getExchangeRate1
SET updflag = 'A'
WHERE updflag = 'N'
AND vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0;



UPDATE tmp_getExchangeRate1
SET updflag = 'B'
WHERE updflag = 'N';



UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate * t.TCURF_FFACT * t.TCURF_TFACT,updflag = 'A1'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vReverse = 'Y'
AND g.updflag = 'A';

/*Upd2 : Take care of rows which were not processed by above update*/

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate * t.TCURF_FFACT * t.TCURF_TFACT,updflag = 'A2'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'Y'
AND g.updflag = 'A';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate * 1,updflag='A3'
where exchangeRate IS NULL
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'Y'
AND g.updflag = 'A';


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate / t.TCURF_FFACT * t.TCURF_TFACT,updflag='A4'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'N'
AND g.updflag = 'A';

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate / t.TCURF_FFACT * t.TCURF_TFACT,updflag='A5'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'N'
AND g.updflag = 'A';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate / 1,updflag='A6'
where exchangeRate IS NULL
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'N'
AND g.updflag = 'A';


/* Block 2 ( pFromExchangeRate = 0 )
 and ! vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0 */


drop table if exists tmp_tcurr;
CREATE table tmp_tcurr
AS
SELECT  tcurr_fcurr,tcurr_tcurr,tcurr_kurst,tcurr_gdatu,tcurr_ffact,tcurr_tfact,tcurr_ukurs,
cast(left((99999999 - tcurr_gdatu),4) + '-' + left(right((99999999 - tcurr_gdatu),4),2) + '-' + right((99999999 - tcurr_gdatu),2) AS date) "cast_tcurr_gdatu"
FROM tcurr;


/*Now get the max value of cast_TCURF_GDATU for each row of tmp_TCURF, where cast_TCURF_GDATU <= g.pDate*/

drop table if exists tmp2_tcurr;
CREATE TABLE tmp2_tcurr
AS
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and g.pDate >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;


/*Wherever row with g.pDate >= cast_tcurr_gdatu was not found, use TIMESTAMP(LOCAL_TIMESTAMP)*/

INSERT INTO tmp2_tcurr
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
AND NOT EXISTS ( SELECT 1 FROM tmp2_tcurr t2 where t2.tcurr_fcurr = T.tcurr_fcurr and t2.tcurr_tcurr = T.tcurr_tcurr and
                                        t2.tcurr_kurst = T.tcurr_kurst AND g.pDate = t2.pDate )
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;


/*tmp3 : This just has the max dates ( no g.pDate >= cast_tcurr_gdatu condition ), as some queries require just this*/

drop table if exists tmp3_tcurr;
CREATE TABLE tmp3_tcurr
AS
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;


UPDATE tmp_getExchangeRate1
SET updflag='C1'
WHERE updflag = 'B'
and pFromExchangeRate = 0;

UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END )
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND g.updflag = 'C1'
AND ( g.pFromExchangeRate = 0 )
AND t.tcurr_ukurs > 0;




UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) )
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND g.updflag = 'C1'
AND ( g.pFromExchangeRate = 0 )
AND t.tcurr_ukurs < 0;


/*
Upd2a ( Not updated in the above block : pFromExchangeRate = 0 ). 
AND tcurr_ukurs = pFromExchangeRate instead of pFromExchangeRate = 0
where B ( set as C2 ) */

UPDATE tmp_getExchangeRate1
SET updflag='C2'
WHERE updflag = 'B';

UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END )
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND g.updflag = 'C2'
AND tcurr_ukurs = pFromExchangeRate
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) )
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND g.updflag = 'C2'
AND tcurr_ukurs = pFromExchangeRate
AND t.tcurr_ukurs < 0;

/* LI Start Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_getExchangeRate1');
/* End Changes 03 Mar 2015 */

/* Block 3 ( exchangeRate IS NULL ). Exactly similar to Block 1, other than these 3 conditions
(Block 1 ( vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0 ) ) */

UPDATE tmp_getExchangeRate1
SET updflag='D'
WHERE updflag = 'C2'	
AND exchangeRate IS NULL;

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate * t.TCURF_FFACT * t.TCURF_TFACT,updflag='E'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vReverse = 'Y'
AND g.updflag = 'D';

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate * t.TCURF_FFACT * t.TCURF_TFACT,updflag='E'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vReverse = 'Y'
AND g.updflag = 'D';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate * 1,updflag='E'
where g.vReverse = 'Y'
AND g.updflag = 'D';


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate / t.TCURF_FFACT * t.TCURF_TFACT,updflag='D5'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vReverse = 'N'
AND g.updflag = 'D';

/* pd2 : Take care of rows which were not processed by above update */
UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate / t.TCURF_FFACT * t.TCURF_TFACT,updflag='D5'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.updflag = 'D'
AND g.vReverse = 'N';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate / 1,updflag='D5'
where exchangeRate IS NULL
AND g.updflag = 'D'
AND g.vReverse = 'N';


/* Block 4 ( exchangeRate IS NULL , but this time no condition on updflag = 'N' )
: 21 Mar 2013 : THIS BLOCK NEEDS TO BE RELOOKED till C

Exactly similar to Block 2, but no condition on pFromExchangeRate = 0
Also, no condition on g.pDate >= t2.max_cast_tcurr_gdatu

LK:Main Block  (  IF (exchangeRate IS NULL)       --LK:where flag = B )
albea gets updated here
Note : This should use tmp3 and not tmp2 as it cares just about the max dates ( and does not need to check if its less than pDate )*/


UPDATE tmp_getExchangeRate1 g FROM tmp3_tcurr t2,tmp_tcurr t
SET exchangeRate = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END )
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
and t.cast_tcurr_gdatu = t2.max_cast_tcurr_gdatu
AND g.updflag = 'B'
AND exchangeRate IS NULL
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp3_tcurr t2,tmp_tcurr t
SET exchangeRate = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) )
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
and t.cast_tcurr_gdatu = t2.max_cast_tcurr_gdatu
AND g.updflag = 'B'
AND exchangeRate IS NULL
AND t.tcurr_ukurs < 0;




/* Block 5 (vRefCur is not null AND vRefCur <> pToCurrency)
Exactly similar to Block 2, but no condition on pFromExchangeRate = 0
Instead vRefCur is not null AND vRefCur <> pToCurrency
And exchangeRate1 is updated instead of exchangeRate*/


UPDATE tmp_getExchangeRate1
set updflag='F'
where updflag NOT IN ( 'A','Y','Z')
AND vRefCur is not null AND vRefCur <> pToCurrency;

UPDATE tmp_getExchangeRate1
SET  vFromCurr = pToCurrency
where updflag='F';

UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate1 = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),
        updflag='F1'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND updflag='F'
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate1 = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),
        updflag='F2'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND updflag='F'
AND t.tcurr_ukurs < 0;

/* Block 5 a - Remove the condition on g.pDate >= t2.max_cast_tcurr_gdatu where exchangeRate1 is null and
vRefCur is not null AND vRefCur <> pToCurrency
Refer this line in getExchangeRate.sql.orig.albea.comments
IF (exchangeRate1 IS NULL)          --LK: WHERE Flag = F and exchangeRate1 IS NULL in getExchangeRate.sql.orig.albea.comments */

UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate1 = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),
        updflag='F3'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND exchangeRate1 IS NULL
AND updflag IN ( 'F','F1','F2' )
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate1 = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),
        updflag='F4'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND updflag IN ( 'F','F1','F2' )
AND exchangeRate1 IS NULL
AND t.tcurr_ukurs < 0;

UPDATE tmp_getExchangeRate1
SET exchangeRate = exchangeRate / exchangeRate1
WHERE updflag IN ( 'F','F1','F2','F3','F4');

/* LI Start Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_getExchangeRate1');
/* End Changes 03 Mar 2015 */

UPDATE tmp_getExchangeRate1
SET exchangeRate = ifnull(pFromExchangeRate,0)
WHERE ifnull(exchangeRate, 0) = 0;


UPDATE tmp_getExchangeRate1
SET exchangeRate = -1 * exchangeRate
where vReverse = 'Y';


UPDATE tmp_getExchangeRate1
SET exchangeRate = 1
WHERE ifnull(exchangeRate, 0) = 0;

UPDATE tmp_getExchangeRate1
SET exchangeRate = -1/exchangeRate
WHERE exchangeRate < 0;

/* LI Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_getExchangeRate1');