


/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Lokesh */
/*   Created On     : 25 Jul 2013 */
/*   Description    : Current version  Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                               */
/*   8  Sep 2013      Lokesh	1.3               Currency and exchange rate chgs. tran->local,tran->global	*/
/*   7  Aug 2013      Lokesh    1.2               Removed the std script logic as recursive query is not required any more for billing */
/*   25 Jul 2013      Lokesh    1.1               Merged std price changes done on 25th Jul for reverse and rounding */
/*   25 Jul 2013      Lokesh    1.0               Named current version as 1.0                                       */
/******************************************************************************************************************/


/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_billing_fact';


/* pFromExchangeRate to be used only for local curr */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT VBRK_WAERK,'USD',NULL,ANSIDATE(LOCAL_TIMESTAMP),'bi_populate_billing_fact'
FROM VBRK_VBRP;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_billing_fact';

/* Where explicit currency other than global currency is populated */

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT VBRK_WAERK,dc.Currency,VBRK_KURRF,VBRK_FKDAT,'bi_populate_billing_fact'
FROM vbrk_vbrp INNER JOIN dim_company dc ON dc.CompanyCode = vbrk_bukrs AND dc.RowIsCurrent = 1 ;


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_billing;
create table tmp_getExchangeRate1_nodups_billing
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_billing_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_billing_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_billing;

drop table tmp_getExchangeRate1_nodups_billing;


CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');

 

