/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_invaging_fact_salesorderstock';



INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,a.MSKA_ERSDA as pDate, 'USD' pToCurrency, NULL exchangeRate,'bi_populate_invaging_fact_salesorderstock'
FROM MSKA a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MSKA_WERKS = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency;


UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_invaging_fact_salesorderstock';


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_inventoryaging1;
create table tmp_getExchangeRate1_nodups_inventoryaging1
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_invaging_fact_salesorderstock';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_invaging_fact_salesorderstock';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_inventoryaging1;

drop table tmp_getExchangeRate1_nodups_inventoryaging1;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');

