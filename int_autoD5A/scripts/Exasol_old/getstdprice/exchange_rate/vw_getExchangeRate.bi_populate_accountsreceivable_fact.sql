
/*   17 Sep 2013      Lokesh	1.1  		  Changed queries to use BSID and BSAD so that there is no need to split AR script */

/* IMPORTANT : THIS SHOULD BE RUN AFTER AR PART 1 AND BEFORE AR PART 2 */


/* Populate Tran->Global curr */
/* LK: As discussed with Hiten, from exchange rate should never be used for getting blobal rate */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_accountsreceivable_fact';

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSID_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSID_BLDAT pDate,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM   BSID arc;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAD_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSAD_BLDAT pDate,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM BSAD arc;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSID_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,ANSIDATE(LOCAL_TIMESTAMP) pDate,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM   BSID arc;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAD_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,ANSIDATE(LOCAL_TIMESTAMP) pDate,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM BSAD arc;


UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_accountsreceivable_fact';

/* Populate Tran->Local curr */
/* LK: 11 Sep: Note that these are not used any more as its retrieved from DMBTR and WRBTR in the script itself. Remove this once validation is done */

/*
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSID_WAERS,dcm.Currency,VBRK_KURRF,BSID_BLDAT,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM   dim_company dcm,
           dim_customer dc,
           VBRK_VBRP vkp,
           BSID arc,fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_billingno = vkp.VBRK_VBELN
AND far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND far.dd_billingno = arc.BSID_VBELN
AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.CustomerNumber = arc.BSID_KUNNR

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAD_WAERS,dcm.Currency,VBRK_KURRF,BSAD_BLDAT,NULL exchangeRate,'bi_populate_accountsreceivable_fact'
FROM dim_company dcm,
           dim_customer dc,
           VBRK_VBRP vkp,
           BSAD arc,fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_billingno = vkp.VBRK_VBELN
AND far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND far.dd_billingno = arc.BSAD_VBELN
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
*/


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_fact_accountsreceivable_nodups;
create table tmp_getExchangeRate1_fact_accountsreceivable_nodups
as
select distinct * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_accountsreceivable_fact';

delete from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_accountsreceivable_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_fact_accountsreceivable_nodups;

drop table tmp_getExchangeRate1_fact_accountsreceivable_nodups;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
