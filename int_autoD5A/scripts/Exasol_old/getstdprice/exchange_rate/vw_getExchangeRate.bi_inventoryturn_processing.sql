
/* Start of custom exchange rate proc for bi_inventoryturn_processing. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_inventoryturn_processing';

Drop table if exists tmp_globalcur;

Declare global temporary table tmp_globalcur(
	pGlobalCurrency)
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD'),varchar(3)) ON COMMIT PRESERVE ROWS;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT c.Currency pFromCurrency, t.pGlobalCurrency pToCurrency,null pFromExchangeRate,ANSIDATE(LOCAL_TIMESTAMP) pDate,'bi_inventoryturn_processing' fact_script_name
FROM fact_inventory inv,dim_company c, tmp_globalcur t
WHERE inv.Dim_Companyid = c.Dim_Companyid;

/* tmptable for exchrate is populated here, rather than creating another separate custom script */

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT HWAER pFromCurrency,t.pGlobalCurrency pToCurrency,null pFromExchangeRate,ANSIDATE(LOCAL_TIMESTAMP) pDate,'bi_inventoryturn_processing' fact_script_name
FROM S031 a, tmp_globalcur t;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_inventoryturn_processing' ;

UPDATE tmp_getExchangeRate1
set exchangeRate = NULL
WHERE fact_script_name = 'bi_inventoryturn_processing';

drop table if exists tmp_getExchangeRate1_nodups_sof;
create table tmp_getExchangeRate1_nodups_sof
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_sof;

drop table tmp_getExchangeRate1_nodups_sof;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
