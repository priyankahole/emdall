/*********************************************Change History*******************************************************/
/*  Date             By             Version       Desc                                                            */
/*  03 Mar 2015      Liviu Ionescu                Add additional combines for table tmp_getExchangeRate1 */
/******************************************************************************************************************/

drop table if exists tmp_getExchangeRate1;

CREATE TABLE tmp_getExchangeRate1
(
pFromCurrency  varchar(3),
pToCurrency   varchar(3),
pFromExchangeRate decimal(9, 5),
pDate    date,
exchangeRate   decimal(9, 5),
exchangeRate1   decimal(9, 5),
vType  varchar(4),
vRefCur  varchar(3),
vFromCurr  varchar(3),
vToCurr  varchar(3),
vInvAllow  varchar(1),
vReverse  varchar(1), 
vFfact     int,
vTfact     int,
updflag  varchar(1),
updflag2 varchar(1),
vTCURFExists int,
vFTfact int
);

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,a.MKPF_BUDAT as pDate, 'USD' pToCurrency, 1.00000 exchangeRate
FROM MKPF_MSEG a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MSEG_UMWRK = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency
UNION
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,a.MKPF_BUDAT as pDate, 'USD' pToCurrency, 1.00000 exchangeRate
FROM MKPF_MSEG1 a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MSEG1_UMWRK = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD');;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate )
SELECT DISTINCT a.MSEG_WAERS as pFromCurrency, a.MKPF_BUDAT as pDate, dc.CurrencyCode pToCurrency, 1.00000 exchangeRate
FROM MKPF_MSEG a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MSEG_UMWRK = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency
AND NOT EXISTS ( SELECT 1 FROM tmp_getExchangeRate1 x where x.pFromCurrency = a.MSEG_WAERS and x.pToCurrency = dc.CurrencyCode)
UNION
SELECT DISTINCT a.MSEG1_WAERS as pFromCurrency, a.MKPF_BUDAT as pDate, dc.CurrencyCode pToCurrency, 1.00000 exchangeRate
FROM MKPF_MSEG1 a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MSEG1_UMWRK = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency
AND NOT EXISTS ( SELECT 1 FROM tmp_getExchangeRate1 x where x.pFromCurrency = a.MSEG1_WAERS and x.pToCurrency = dc.CurrencyCode);


CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');

UPDATE tmp_getExchangeRate1
set exchangeRate = NULL;


UPDATE tmp_getExchangeRate1
set vReverse = 'N',updflag = 'N';      


UPDATE tmp_getExchangeRate1
SET exchangeRate = 1,updflag = 'Y'              
WHERE pFromCurrency = pToCurrency;


UPDATE tmp_getExchangeRate1
SET vReverse = 'Y',
 pFromExchangeRate = -1 * pFromExchangeRate,
 vRefCur = pFromCurrency,
 pFromCurrency = pToCurrency,
 pToCurrency = vRefCur
WHERE updflag = 'N'
AND pFromExchangeRate < 0;


UPDATE  tmp_getExchangeRate1
SET vType = ifnull((select TCURV_KURST from tcurv ),'M'),
vRefCur = (select TCURV_BWAER from tcurv ),
vInvAllow = ifnull((select 'Y' from tcurv where TCURV_XINVR is not null),'N'),
vFromCurr = pFromCurrency,
vToCurr = pToCurrency,
vTCURFExists = ifnull((select count(*) from TCURF ),0)		
WHERE updflag = 'N'     ;



drop table if exists tmp_TCURF;
CREATE table tmp_TCURF
AS
SELECT  TCURF_FCURR,TCURF_TCURR,TCURF_KURST,TCURF_GDATU,TCURF_FFACT,TCURF_TFACT,
cast(left((99999999 - TCURF_GDATU),4) + '-' + left(right((99999999 - TCURF_GDATU),4),2) + '-' + right((99999999 - TCURF_GDATU),2) AS date) "cast_TCURF_GDATU"
FROM TCURF;


drop table if exists tmp2_TCURF;
CREATE TABLE tmp2_TCURF
AS
SELECT   TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate,max(cast_TCURF_GDATU) as max_cast_TCURF_GDATU
FROM tmp_TCURF T,tmp_getExchangeRate1 g
where T.TCURF_FCURR = g.vFromCurr and T.TCURF_TCURR = g.vToCurr and T.TCURF_KURST = g.vType
                                  and g.pDate >= cast_TCURF_GDATU
AND g.updflag = 'N'
group by TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate;

CALL VECTORWISE( COMBINE 'tmp_TCURF');


INSERT INTO tmp2_TCURF
SELECT   TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate,max(cast_TCURF_GDATU) as max_cast_TCURF_GDATU
FROM tmp_TCURF T,tmp_getExchangeRate1 g
where T.TCURF_FCURR = g.vFromCurr and T.TCURF_TCURR = g.vToCurr and T.TCURF_KURST = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_TCURF_GDATU
AND g.updflag = 'N'
AND NOT EXISTS ( SELECT 1 FROM tmp2_TCURF t2 where t2.TCURF_FCURR = T.TCURF_FCURR and t2.TCURF_TCURR = T.TCURF_TCURR and
                                        t2.TCURF_KURST = T.TCURF_KURST AND g.pDate = t2.pDate )
group by TCURF_FCURR,TCURF_TCURR,TCURF_KURST,g.pDate;



/***** Hiten's changes1 18th Jan ******/

select 'Z0',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE tmp_getExchangeRate1 g
SET updflag = 'Z'
WHERE g.updflag = 'N'
AND g.pFromExchangeRate > 0
AND g.vTCURFExists > 0;

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vFTfact = decimal(t.TCURF_FFACT * t.TCURF_TFACT,19,5)
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND g.updflag = 'Z';


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vFTfact = decimal(t.TCURF_FFACT * t.TCURF_TFACT,19,5)
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate 
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND g.updflag = 'Z';


UPDATE tmp_getExchangeRate1 g
SET vFTfact = 1
where vFTfact IS NULL
AND g.updflag = 'Z';


UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate * vFTfact
WHERE vReverse = 'Y'
AND g.updflag = 'Z';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate / vFTfact
WHERE vReverse <> 'Y'
AND g.updflag = 'Z';

select 'Z1',TIMESTAMP(LOCAL_TIMESTAMP);
/* End of Hiten's changes1 18th Jan	*/

UPDATE  tmp_getExchangeRate1
set vToCurr = vRefCur
where vRefCur is not null AND vRefCur <> pToCurrency
AND updflag = 'N';


UPDATE  tmp_getExchangeRate1
set vReverse = 'Y', vToCurr = pFromCurrency, pFromCurrency = pToCurrency,pToCurrency = vToCurr,vFromCurr = pFromCurrency
where vRefCur is not null AND vRefCur <> pToCurrency
AND vReverse = 'N' AND vRefCur = pFromCurrency
AND updflag = 'N';

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vFfact = t.TCURF_FFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND updflag = 'N';


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vFfact = t.TCURF_FFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND g.vFfact IS NULL
AND updflag = 'N';


UPDATE tmp_getExchangeRate1
SET vFfact = 1
where vFfact IS NULL
AND updflag = 'N';


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vTfact = t.TCURF_TFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND updflag = 'N';


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET vTfact = t.TCURF_TFACT
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND t.cast_TCURF_GDATU = t2.max_cast_TCURF_GDATU
AND g.vTfact IS NULL
AND updflag = 'N';

UPDATE tmp_getExchangeRate1
SET vTfact = 1
where vTfact IS NULL
AND updflag = 'N';

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
CALL VECTORWISE( COMBINE 'tmp2_TCURF');


UPDATE tmp_getExchangeRate1
SET updflag = 'A'
WHERE updflag = 'N'
AND vRefCur is not null AND vRefCur <> pToCurrency AND pFromExchangeRate > 0;

UPDATE tmp_getExchangeRate1
SET updflag = 'B'
WHERE updflag = 'N';


UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate * t.TCURF_FFACT * t.TCURF_TFACT,updflag = 'A1'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vReverse = 'Y'
AND g.updflag = 'A';

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate * t.TCURF_FFACT * t.TCURF_TFACT,updflag = 'A2'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'Y'
AND g.updflag = 'A';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate * 1,updflag='A3'
where exchangeRate IS NULL
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'Y'
AND g.updflag = 'A';



UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate / t.TCURF_FFACT * t.TCURF_TFACT,updflag='A4'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'N'
AND g.updflag = 'A';

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate / t.TCURF_FFACT * t.TCURF_TFACT,updflag='A5'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'N'
AND g.updflag = 'A';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate / 1,updflag='A6'
where exchangeRate IS NULL
AND g.vRefCur is not null AND g.vRefCur <> g.pToCurrency AND g.pFromExchangeRate > 0
AND g.vReverse = 'N'
AND g.updflag = 'A';




drop table if exists tmp_tcurr;
CREATE table tmp_tcurr
AS
SELECT  tcurr_fcurr,tcurr_tcurr,tcurr_kurst,tcurr_gdatu,tcurr_ffact,tcurr_tfact,tcurr_ukurs,
cast(left((99999999 - tcurr_gdatu),4) + '-' + left(right((99999999 - tcurr_gdatu),4),2) + '-' + right((99999999 - tcurr_gdatu),2) AS date) "cast_tcurr_gdatu"
FROM tcurr;



drop table if exists tmp2_tcurr;
CREATE TABLE tmp2_tcurr
AS
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and g.pDate >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;



INSERT INTO tmp2_tcurr
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
AND NOT EXISTS ( SELECT 1 FROM tmp2_tcurr t2 where t2.tcurr_fcurr = T.tcurr_fcurr and t2.tcurr_tcurr = T.tcurr_tcurr and
                                        t2.tcurr_kurst = T.tcurr_kurst AND g.pDate = t2.pDate )
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;



drop table if exists tmp3_tcurr;
CREATE TABLE tmp3_tcurr
AS
SELECT   tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate,max(cast_tcurr_gdatu) as max_cast_tcurr_gdatu
FROM tmp_tcurr T,tmp_getExchangeRate1 g
where T.tcurr_fcurr = g.vFromCurr and T.tcurr_tcurr = g.vToCurr and T.tcurr_kurst = g.vType
                                  and TIMESTAMP(LOCAL_TIMESTAMP) >= cast_tcurr_gdatu
AND g.updflag NOT IN ( 'Y','Z')
group by tcurr_fcurr,tcurr_tcurr,tcurr_kurst,g.pDate;



UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),
        updflag='C1'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND g.updflag = 'B'
AND ( g.pFromExchangeRate = 0 )
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),
        updflag='C1'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND g.updflag = 'B'
AND ( g.pFromExchangeRate = 0 )
AND t.tcurr_ukurs < 0;




UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),
        updflag='C2'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND g.updflag = 'B'
AND tcurr_ukurs = pFromExchangeRate
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),
        updflag='C2'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND g.updflag = 'B'
AND tcurr_ukurs = pFromExchangeRate
AND t.tcurr_ukurs < 0;

/* LI Start Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_getExchangeRate1');
/* End Changes 03 Mar 2015 */

UPDATE tmp_getExchangeRate1
SET updflag='D'
WHERE updflag = 'C2'
AND exchangeRate IS NULL;

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate * t.TCURF_FFACT * t.TCURF_TFACT,updflag='E'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vReverse = 'Y'
AND g.updflag = 'D';

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate * t.TCURF_FFACT * t.TCURF_TFACT,updflag='E'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vReverse = 'Y'
AND g.updflag = 'D';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate * 1,updflag='E'
where g.vReverse = 'Y'
AND g.updflag = 'D';



UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate / t.TCURF_FFACT * t.TCURF_TFACT,updflag='D5'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_TCURF_GDATU
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.vReverse = 'N'
AND g.updflag = 'D';

UPDATE tmp_getExchangeRate1 g FROM tmp2_TCURF t2,tmp_TCURF t
SET exchangeRate = pFromExchangeRate / t.TCURF_FFACT * t.TCURF_TFACT,updflag='D5'
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
AND g.pDate=t2.pDate
AND t.TCURF_FCURR = t2.TCURF_FCURR AND t.TCURF_TCURR = t2.TCURF_TCURR AND t.TCURF_KURST = t2.TCURF_KURST
AND g.updflag = 'D'
AND g.vReverse = 'N';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate / 1,updflag='D5'
where exchangeRate IS NULL
AND g.updflag = 'D'
AND g.vReverse = 'N';





UPDATE tmp_getExchangeRate1 g FROM tmp3_tcurr t2,tmp_tcurr t
SET exchangeRate = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END )
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
and t.cast_tcurr_gdatu = t2.max_cast_tcurr_gdatu
AND g.updflag = 'B'
AND exchangeRate IS NULL
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp3_tcurr t2,tmp_tcurr t
SET exchangeRate = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) )
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
and t.cast_tcurr_gdatu = t2.max_cast_tcurr_gdatu
AND g.updflag = 'B'
AND exchangeRate IS NULL
AND t.tcurr_ukurs < 0;




UPDATE tmp_getExchangeRate1
set updflag='F'
where updflag NOT IN ( 'A','Y','Z')
AND vRefCur is not null AND vRefCur <> pToCurrency;

UPDATE tmp_getExchangeRate1
SET  vFromCurr = pToCurrency
where updflag='F';

UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate1 = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),
        updflag='F1'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND updflag='F'
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate1 = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),
        updflag='F2'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate AND  g.pDate >= t2.max_cast_tcurr_gdatu
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND updflag='F'
AND t.tcurr_ukurs < 0;


UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate1 = tcurr_ukurs / ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ),
        updflag='F3'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND exchangeRate1 IS NULL
AND updflag IN ( 'F','F1','F2' )
AND t.tcurr_ukurs > 0;


UPDATE tmp_getExchangeRate1 g FROM tmp2_tcurr t2,tmp_tcurr t
SET exchangeRate1 = -1/ ( tcurr_ukurs * ( CASE WHEN tcurr_ffact = 0 THEN vFfact ELSE tcurr_ffact END   *
                                                                CASE WHEN t.tcurr_tfact = 0 THEN vTfact ELSE tcurr_tfact END ) ),
        updflag='F4'
WHERE g.vFromCurr = t2.tcurr_fcurr AND g.vToCurr = t2.tcurr_tcurr AND g.vType = t2.tcurr_kurst
AND g.pDate=t2.pDate
AND t.tcurr_fcurr = t2.tcurr_fcurr AND t.tcurr_tcurr = t2.tcurr_tcurr AND t.tcurr_kurst = t2.tcurr_kurst
AND updflag IN ( 'F','F1','F2' )
AND exchangeRate1 IS NULL
AND t.tcurr_ukurs < 0;

UPDATE tmp_getExchangeRate1
SET exchangeRate = exchangeRate / exchangeRate1
WHERE updflag IN ( 'F','F1','F2','F3','F4');

/* LI Start Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_getExchangeRate1');
/* End Changes 03 Mar 2015 */

UPDATE tmp_getExchangeRate1
SET exchangeRate = ifnull(pFromExchangeRate,0)
WHERE ifnull(exchangeRate, 0) = 0;


UPDATE tmp_getExchangeRate1
SET exchangeRate = -1 * exchangeRate
where vReverse = 'Y';


UPDATE tmp_getExchangeRate1
SET exchangeRate = 1
WHERE ifnull(exchangeRate, 0) = 0;

UPDATE tmp_getExchangeRate1
SET exchangeRate = -1/exchangeRate
WHERE exchangeRate < 0;

/* LI Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_getExchangeRate1');


