/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_custom_kla_post_process_invaging';



INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP) as pDate, 'USD' pToCurrency, NULL exchangeRate,'bi_custom_kla_post_process_invaging'
FROM dim_Currency dc;




UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_custom_kla_post_process_invaging';


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_inventoryaging_klacustom;
create table tmp_getExchangeRate1_nodups_inventoryaging_klacustom
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_custom_kla_post_process_invaging';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_custom_kla_post_process_invaging';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_inventoryaging_klacustom;

drop table tmp_getExchangeRate1_nodups_inventoryaging_klacustom;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');

