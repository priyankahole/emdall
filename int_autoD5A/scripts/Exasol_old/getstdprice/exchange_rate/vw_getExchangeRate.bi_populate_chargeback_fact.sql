/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_chargeback_fact';

Drop table if exists tmp_globalcur;

Declare global temporary table tmp_globalcur(
	pGlobalCurrency)
AS
Select  CAST(ifnull((SELECT property_value
			FROM systemproperty
			WHERE property = 'customer.global.currency'),
		'USD'),varchar(3)) ON COMMIT PRESERVE ROWS;


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT IRM_IPCBHDR_WAERS as pFromCurrency,ifnull(IRM_IPCBITM_PRSDT,IRM_IPCBHDR_ERDAT) as pDate, co.currency pToCurrency, NULL exchangeRate,'bi_populate_chargeback_fact'
FROM IRM_IPCBHDR a, IRM_IPCBITM b,
     Dim_Company co
WHERE co.CompanyCode = IRM_IPCBHDR_BUKRS
	and IRM_IPCBHDR_IPNUM = IRM_IPCBITM_IPNUM;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT IRM_IPCBHDR_WAERS as pFromCurrency,ifnull(IRM_IPCBITM_PRSDT,IRM_IPCBHDR_ERDAT) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_chargeback_fact'
FROM IRM_IPCBHDR a, IRM_IPCBITM b,
     Dim_Company co,
     tmp_globalcur
WHERE co.CompanyCode = IRM_IPCBHDR_BUKRS
	and IRM_IPCBHDR_IPNUM = IRM_IPCBITM_IPNUM;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT IRM_IPCBHDR_WAERS as pFromCurrency,ansidate(LOCAL_TIMESTAMP) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_chargeback_fact'
FROM IRM_IPCBHDR a, IRM_IPCBITM b,
     Dim_Company co,
     tmp_globalcur
WHERE co.CompanyCode = IRM_IPCBHDR_BUKRS
	and IRM_IPCBHDR_IPNUM = IRM_IPCBITM_IPNUM;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT IRM_IPCBHDR_WAERS as pFromCurrency,ifnull(IRM_IPCBITM_PRSDT,IRM_IPCBHDR_ERDAT) as pDate, IRM_IPCBHDR_STWAE pToCurrency,IRM_IPCBHDR_STCUR pFromExchangeRate, NULL exchangeRate,'bi_populate_chargeback_fact'
FROM IRM_IPCBHDR a, IRM_IPCBITM b,
     Dim_Company co
WHERE co.CompanyCode = IRM_IPCBHDR_BUKRS
	and IRM_IPCBHDR_IPNUM = IRM_IPCBITM_IPNUM;


/* For all from-to curr combinations, add rows with fromexchangerate = 0 as well */
INSERT INTO tmp_getExchangeRate1( pFromCurrency, pDate, pToCurrency, exchangeRate, pFromExchangeRate,fact_script_name)
SELECT pFromCurrency, pDate, pToCurrency, exchangeRate,0,'bi_populate_chargeback_fact'
FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_chargeback_fact';

drop table if exists tmp_getExchangeRate1_nodups_sof;
create table tmp_getExchangeRate1_nodups_sof
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_chargeback_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_chargeback_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_sof;

drop table tmp_getExchangeRate1_nodups_sof;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
