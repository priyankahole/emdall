/***************************************************************************************************** 
Creation Date : Sep 03, 12013
Created By : Hiten Suthar
Change History
Date            By        Version           Desc
13 Sep 2013     Issam    1.1	         Added calculation for backordred Qty
18 Sep 2013     Issam    1.4	         Added fields dd_SOCreateTime dd_ReqDeliveryTime, 
										 dd_SOLineCreateTime, dd_DeliveryTime, dd_PlannedGITime    
										 dd_SDCreateTime dd_DeliveryTime, dd_PickingTime, 
										 dd_GITime, dd_SDLineCreateTime		
28 May 2014      Hiten    1.57		Logic change for dd_BackOrderFlag_Merck flag
9 July 2014	 Cornelia 1.58		Add changes for dim_customersla_merck	
10 Sept 2014 Cornelia Add changes for DeliveryBlock							 
*****************************************************************************************************/
	
/* changes 18 Sep 2013 */	
	
UPDATE customer_sla
SET customergrp = case when customergrp is null then null when length(customergrp)<2 then lpad(customergrp,2,'0') else customergrp end;



/*** insert dim_customersla_merck ***/

INSERT INTO dim_customersla_merck(dim_customersla_merckid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customersla_merck
               WHERE dim_customersla_merckid = 1);

delete from NUMBER_FOUNTAIN where table_name = 'dim_customersla_merck';

INSERT INTO NUMBER_FOUNTAIN
select 'dim_customersla_merck',ifnull(max(dim_customersla_merckid),0)
FROM dim_customersla_merck;               
               
INSERT INTO dim_customersla_merck(dim_customersla_merckid,
				salesorg,
				shiptocountry,
				customergrp,
				shiptolocation,
				ordertoshipbdays,
				daysearly,
				dayslate,
				RowStartDate,
				RowIsCurrent)
   SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_customersla_merck') + row_number() over (order by ''),
	  salesorg,
	  ifnull(shiptocountry, 'Not Set'),
	  ifnull(customergrp, 'Not Set'),
	  ifnull(shiptolocation, 'Not Set'),
	  ordertoshipbdays,
	  daysearly,
	  dayslate,
          current_date,
          1
     FROM (select distinct a.* from customer_sla a) s 
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customersla_merck d
               WHERE d.salesorg = s.salesorg
			AND d.shiptocountry = ifnull(s.shiptocountry, 'Not Set')
			AND d.customergrp = ifnull(s.customergrp, 'Not Set')
			AND d.shiptolocation = ifnull(s.shiptolocation, 'Not Set'));

UPDATE dim_customersla_merck a
SET a.dayslate = b.dayslate
FROM customer_sla b, dim_customersla_merck a
WHERE a.salesorg = b.salesorg
	AND a.shiptocountry = ifnull(b.shiptocountry, 'Not Set')
	AND a.customergrp = ifnull(b.customergrp, 'Not Set')
	AND a.shiptolocation = ifnull(b.shiptolocation, 'Not Set')
	AND a.dayslate <> b.dayslate;

UPDATE dim_customersla_merck a
SET a.daysearly = b.daysearly
FROM customer_sla b, dim_customersla_merck a
WHERE a.salesorg = b.salesorg
	AND a.shiptocountry = ifnull(b.shiptocountry, 'Not Set')
	AND a.customergrp = ifnull(b.customergrp, 'Not Set')
	AND a.shiptolocation = ifnull(b.shiptolocation, 'Not Set')
	AND a.daysearly <> b.daysearly;

UPDATE dim_customersla_merck a
SET a.ordertoshipbdays = b.ordertoshipbdays
FROM customer_sla b,dim_customersla_merck a
WHERE a.salesorg = b.salesorg
	AND a.shiptocountry = ifnull(b.shiptocountry, 'Not Set')
	AND a.customergrp = ifnull(b.customergrp, 'Not Set')
	AND a.shiptolocation = ifnull(b.shiptolocation, 'Not Set')
	AND a.ordertoshipbdays <> b.ordertoshipbdays;



/*** end insert dim_customersla_merck ***/

UPDATE shipptscutofftime
SET cutofftime = rpad(lpad(replace(cutofftime,':',''),4,'0'),6,'0');


 UPDATE fact_salesorderdelivery sod 
 SET dd_SDCreateTime = ifnull(LIKP_ERZET, 'Not Set'),
	dd_DeliveryTime = ifnull(LIKP_LFUHR, 'Not Set'),
	dd_PickingTime = ifnull(LIKP_KOUHR, 'Not Set'),
	dd_GITime = ifnull(LIKP_WAUHR, 'Not Set'),
	dd_SDLineCreateTime = ifnull(LIPS_ERZET, 'Not Set')	
  FROM LIKP_LIPS lp,fact_salesorderdelivery sod 
 WHERE sod.dd_SalesDlvrDocNo = lp.LIKP_VBELN
   AND sod.dd_SalesDlvrItemNo = lp.LIPS_POSNR;	

UPDATE fact_salesorderdelivery 
SET dd_SDCreateTime = '000000'
WHERE dd_SDCreateTime is null;	

UPDATE fact_salesorderdelivery 
SET dd_DeliveryTime = '000000'
WHERE dd_DeliveryTime is null;

UPDATE fact_salesorderdelivery 
SET dd_PickingTime = '000000'
WHERE dd_PickingTime is null;

UPDATE fact_salesorderdelivery 
SET dd_GITime = '000000'
WHERE dd_GITime is null;

UPDATE fact_salesorderdelivery 
SET dd_SDLineCreateTime = '000000'
WHERE dd_SDLineCreateTime is null;


UPDATE fact_salesorder so
SET dd_SOCreateTime = ifnull(VBAK_ERZET, 'Not Set'),
dd_ReqDeliveryTime = ifnull(VBAK_VZEIT, 'Not Set'),
dd_SOLineCreateTime = ifnull(VBAP_ERZET, 'Not Set'),
dd_DeliveryTime = ifnull(VBEP_EZEIT, 'Not Set'),
dd_PlannedGITime = ifnull(VBEP_WAUHR, 'Not Set')
FROM vbak_vbap_vbep vbk, fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo;

UPDATE fact_salesorder 
SET dd_SOCreateTime = '000000'
WHERE dd_SOCreateTime is null;

UPDATE fact_salesorder 
SET dd_ReqDeliveryTime = '000000'
WHERE dd_ReqDeliveryTime is null;

UPDATE fact_salesorder 
SET dd_SOLineCreateTime = '000000'
WHERE dd_SOLineCreateTime is null;

UPDATE fact_salesorder 
SET dd_DeliveryTime = '000000'
WHERE dd_DeliveryTime is null;

UPDATE fact_salesorder 
SET dd_PlannedGITime = '000000'
WHERE dd_PlannedGITime is null;

UPDATE fact_salesorder 
SET Dim_CustomerCategoryid = 1 
WHERE Dim_CustomerCategoryid is null;

update fact_salesorder 
set dd_SDLineCreateTime_Merck = '000000'
where dd_SDLineCreateTime_Merck is null;

UPDATE fact_salesorder f_so
SET f_so.Dim_CustomerCategoryid = 2 
FROM dim_salesdocumenttype sdt,
	dim_distributionchannel dch,
	dim_Customer c,  fact_salesorder f_so
WHERE f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
	and f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
	and f_so.dim_CustomerID = c.dim_Customerid
	and sdt.DocumentType = 'ZSO'
	and dch.DistributionChannelCode in ('30','40')
	and c.CustAccountGroup <> 'Z001';

UPDATE fact_salesorder
SET ct_otifqty_merck = 0;

UPDATE fact_salesorder
SET dd_soheaderotif_merck = 'N';

UPDATE fact_salesorder
SET dd_solineotif_merck = 'N';

UPDATE fact_salesorder
SET dd_OTIFCalcFlag_Merck = 'N';

UPDATE fact_salesorder
SET dd_InFullFlag_Merck = 'N';

UPDATE fact_salesorder
SET dd_OnTimeFlag_Merck = 'N';

UPDATE fact_salesorder 
SET dd_OTIFEnabledFlag = 'N';

UPDATE fact_salesorder 
SET dd_InFullFlagHdr_Merck = 'N';

UPDATE fact_salesorder 
SET dd_OnTimeFlagHdr_Merck = 'N';

UPDATE fact_salesorder
SET dim_DateidExpGI_Merck = Dim_DateidGoodsIssue
WHERE dim_DateidExpGI_Merck is null or dim_DateidExpGI_Merck = 1;

update fact_salesorder 
set dim_DateidAdjSOCreated_Merck = Dim_DateidSalesOrderCreated
where dim_DateidAdjSOCreated_Merck is null or dim_DateidAdjSOCreated_Merck = 1;

update fact_salesorder 
set Dim_DateidAdjDropToWH_Merck = Dim_DateidDlvrDocCreated
where Dim_DateidAdjDropToWH_Merck is null or Dim_DateidAdjDropToWH_Merck = 1;

merge into fact_salesorder so
using
(
select distinct max(dd_SDLineCreateTime) as dd_SDLineCreateTime, sd.dd_SalesDocNo, sd.dd_SalesItemNo, sd.dd_ScheduleNo
from fact_salesorderdelivery sd, fact_salesorder so
where dd_SDLineCreateTime_Merck <> dd_SDLineCreateTime
	and so.dd_SalesDocNo = sd.dd_SalesDocNo
	and so.dd_SalesItemNo = sd.dd_SalesItemNo
	and so.dd_ScheduleNo = sd.dd_ScheduleNo
Group by sd.dd_SalesDocNo, sd.dd_SalesItemNo, sd.dd_ScheduleNo) t on so.dd_SalesDocNo = t.dd_SalesDocNo
	and so.dd_SalesItemNo = t.dd_SalesItemNo
	and so.dd_ScheduleNo = t.dd_ScheduleNo
when matched then update set dd_SDLineCreateTime_Merck = t.dd_SDLineCreateTime;

UPDATE fact_salesorder so
SET so.ct_BackorderedQty_Merck = 0
WHERE so.ct_BackorderedQty_Merck is null;

UPDATE fact_salesorder so
SET so.ct_HeaderBackorderedQty_Merck = 0
WHERE so.ct_HeaderBackorderedQty_Merck is null;

UPDATE fact_salesorder
SET dd_BackOrderFlag_Merck = 'Not Set'
WHERE dd_BackOrderFlag_Merck is null;

UPDATE fact_salesorder f
SET f.dd_BackOrderFlag_Merck = 'X'
FROM dim_salesorderitemstatus s, dim_deliveryblock db, dim_salesorderitemstatus sois,  fact_salesorder f
WHERE db.dim_deliveryblockid = f.dim_deliveryblockid
	AND sois.dim_salesorderitemstatusid = f.dim_salesorderitemstatusid
	AND f.Dim_SalesOrderRejectReasonid = 1
	AND f.dim_salesorderitemstatusid = s.dim_salesorderitemstatusid
	AND f.dd_ItemRelForDelv = 'X' 
	AND ( db.DeliveryBlock = 'ZD' OR f.Dim_Deliveryblockid = 1) 
	AND sois.DeliveryStatus <> 'Completely processed' 
	AND ((f.ct_ScheduleQtySalesUnit - f.ct_ConfirmedQty) > 0 or s.ConfirmationStatus = 'Not yet processed')
	AND f.dd_BackOrderFlag_Merck <> 'X';


UPDATE fact_salesorder f
SET f.dd_OTIFCalcFlag_Merck = 'Y'
FROM Dim_SalesOrderRejectReason sorr,
	dim_salesorg sorg,
	dim_distributionchannel dch,
	dim_salesdocumenttype sdt,
	otifenabledmap_merck otf,
	dim_overallstatusforcreditcheck scc, fact_salesorder f
WHERE f.Dim_SalesOrderRejectReasonid = sorr.Dim_SalesOrderRejectReasonid
	and f.Dim_OverallStatusCreditCheckId = scc.dim_overallstatusforcreditcheckid
	and f.dim_salesorgid = sorg.dim_salesorgid
	and f.dim_distributionchannelid = dch.dim_distributionchannelid
	and f.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
	and f.dd_itemrelfordelv = 'X' 
	and scc.OverAllStatusForCreditCheck <> 'B'
	and (f.Dim_SalesOrderRejectReasonid = 1 or (sorr.rejectreasoncode in ('01', 'Z0', 'Z2', 'Z6', 'ZU')))
	and otf.SalesOrg = sorg.SalesOrgCode
		and otf.DistributionChannel = dch.DistributionChannelCode
		and otf.DocType = sdt.DocumentType;

UPDATE fact_salesorder 
SET dd_OTIFEnabledFlag = dd_OTIFCalcFlag_Merck
WHERE dd_OTIFEnabledFlag <> dd_OTIFCalcFlag_Merck;


DROP TABLE IF EXISTS tmp_SOLineDlvrCount_001;
CREATE TABLE tmp_SOLineDlvrCount_001 AS
select dd_salesdocno, dd_salesitemno, dd_scheduleno, count(*) TotDlvrLines
from (select distinct dd_salesdocno, dd_salesitemno, dd_scheduleno, dd_salesdlvrdocno, dim_dateidactualgoodsissue from fact_salesorderdelivery sod ) a
group by dd_salesdocno, dd_salesitemno, dd_scheduleno;

DROP TABLE IF EXISTS tmp_SOCustomerSLA_001;
CREATE TABLE tmp_SOCustomerSLA_001 AS
SELECT distinct dd_salesdocno, dd_salesitemno, sorg.SalesOrgCode, cg.CustomerGroup, TRIM(LEADING '0' FROM c.CustomerNumber) ShipToLoc, c.Country ShipToCountry,
	convert(bigint,0) as ordertoshipbdays, convert(bigint,0) as daysearly, convert(bigint,0) as  dayslate, convert(varchar(3),'N') as has_sla
FROM fact_salesorder so 
	inner join Dim_SalesOrg sorg on so.Dim_SalesOrgid = sorg.Dim_SalesOrgid
	inner join Dim_CustomerGroup cg on so.Dim_CustomerGroupId = cg.Dim_CustomerGroupId
	inner join dim_Customer c on c.dim_Customerid = so.Dim_CustomeridShipTo;

UPDATE tmp_SOCustomerSLA_001 a
SET a.ordertoshipbdays = b.ordertoshipbdays,
	a.daysearly = b.daysearly,
	a.dayslate = b.dayslate,
	a.has_sla = 'Y'
FROM customer_sla b, tmp_SOCustomerSLA_001 a
WHERE b.salesorg = a.SalesOrgCode
	and b.shiptocountry is null
	and b.customergrp is null
	and b.shiptolocation is null;

UPDATE tmp_SOCustomerSLA_001 a
SET a.ordertoshipbdays = b.ordertoshipbdays,
	a.daysearly = b.daysearly,
	a.dayslate = b.dayslate,
	a.has_sla = 'Y'
FROM customer_sla b,  tmp_SOCustomerSLA_001 a
WHERE b.salesorg = a.SalesOrgCode
	and b.shiptocountry = a.ShipToCountry
	and b.customergrp is null
	and b.shiptolocation is null;

UPDATE tmp_SOCustomerSLA_001 a
SET a.ordertoshipbdays = b.ordertoshipbdays,
	a.daysearly = b.daysearly,
	a.dayslate = b.dayslate,
	a.has_sla = 'Y'
FROM customer_sla b, tmp_SOCustomerSLA_001 a
WHERE b.salesorg = a.SalesOrgCode
	and b.shiptocountry = a.ShipToCountry
	and b.customergrp = a.CustomerGroup
	and b.shiptolocation is null;
	
merge into tmp_SOCustomerSLA_001 a
using (
select distinct b.ordertoshipbdays,b.daysearly,b.dayslate,b.salesorg,b.shiptocountry, b.shiptolocation
from  customer_sla b, tmp_SOCustomerSLA_001 a
WHERE b.salesorg = a.SalesOrgCode
	and b.shiptocountry = a.ShipToCountry
	and b.shiptolocation = a.ShipToLoc
	and b.customergrp is null) t on t.salesorg = a.SalesOrgCode
	and t.shiptocountry = a.ShipToCountry
	and t.shiptolocation = a.ShipToLoc
when matched then update set a.ordertoshipbdays = t.ordertoshipbdays,
	a.daysearly = t.daysearly,
	a.dayslate = t.dayslate,
	a.has_sla = 'Y';

	
merge into tmp_SOCustomerSLA_001 a
using (
select distinct b.ordertoshipbdays,b.daysearly,b.dayslate,b.salesorg,b.shiptocountry, b.shiptolocation,b.customergrp
FROM customer_sla b, tmp_SOCustomerSLA_001 a
WHERE b.salesorg = a.SalesOrgCode
	and b.shiptocountry = a.ShipToCountry
	and b.customergrp = a.CustomerGroup
	and b.shiptolocation = a.ShipToLoc) t on t.salesorg = a.SalesOrgCode
	and t.shiptocountry = a.ShipToCountry
	and t.customergrp = a.CustomerGroup
	and t.shiptolocation = a.ShipToLoc
when matched then update set a.ordertoshipbdays = t.ordertoshipbdays,
	a.daysearly = t.daysearly,
	a.dayslate = t.dayslate,
	a.has_sla = 'Y';


DROP TABLE IF EXISTS tmp_DateDropToWH_001;
CREATE TABLE tmp_DateDropToWH_001 AS
select so.dd_salesdocno, 
	so.dd_salesitemno, 
	so.dd_scheduleno,
	ctdt.Dim_Dateid AdjDropToWH_calc,
	row_number() over(partition by so.dd_salesdocno, so.dd_salesitemno, so.dd_scheduleno order by ctdt.DateValue) rowseqno
from fact_salesorder so
	inner join Dim_Date sddt on so.Dim_DateidDlvrDocCreated = sddt.Dim_Dateid -- Shipment created date 
	-- inner join tmp_SOCustomerSLA_001 sla on sla.dd_salesdocno = so.dd_salesdocno and sla.dd_salesitemno = so.dd_salesitemno
	inner join dim_shipreceivepoint srpt on srpt.dim_shipreceivepointid = so.dim_shipreceivepointid
	inner join shipptscutofftime cuttm on cuttm.shippingpoint = srpt.shipreceivepointcode
	inner join dim_date ctdt on (ctdt.CompanyCode = sddt.CompanyCode 
	 				and ctdt.BusinessDaysSeqNo = (sddt.BusinessDaysSeqNo
									+ case when so.dd_SDLineCreateTime_merck > cuttm.cutofftime
											or (cuttm.cutoffweekday = 'THURSDAY' and sddt.weekdaynumber > 5) 
											or sddt.weekdaynumber in (1,7)
										then case when cuttm.cutoffweekday = 'THURSDAY' and sddt.weekdaynumber = 5 
											  then 2 
											  else 1 
											end
										else case when sddt.isaweekendday = 1 or sddt.isapublicholiday = 1 then 1 else 0 end
									  end))
where so.dd_OTIFCalcFlag_Merck = 'Y'
	and so.Dim_DateidDlvrDocCreated <> 1;

insert into tmp_DateDropToWH_001
select so.dd_salesdocno, 
	so.dd_salesitemno, 
	so.dd_scheduleno,
	ctdt.Dim_Dateid AdjDropToWH_calc,
	row_number() over(partition by so.dd_salesdocno, so.dd_salesitemno, so.dd_scheduleno order by ctdt.DateValue) rowseqno
from fact_salesorder so
	inner join Dim_Date sddt on so.Dim_DateidDlvrDocCreated = sddt.Dim_Dateid -- Shipment created date 
	inner join dim_shipreceivepoint srpt on srpt.dim_shipreceivepointid = so.dim_shipreceivepointid
	inner join dim_date ctdt on (ctdt.CompanyCode = sddt.CompanyCode 
	 				and ctdt.BusinessDaysSeqNo = (sddt.BusinessDaysSeqNo
									+ case when sddt.isaweekendday = 1 or sddt.isapublicholiday = 1 then 1 else 0 end))
where so.dd_OTIFCalcFlag_Merck = 'Y'
	and so.Dim_DateidDlvrDocCreated <> 1
	and not exists (select 1 from tmp_DateDropToWH_001 x 
			where x.dd_salesdocno = so.dd_salesdocno and x.dd_salesitemno = so.dd_salesitemno and x.dd_scheduleno = so.dd_scheduleno);
	
Insert into tmp_DateDropToWH_001
select so.dd_salesdocno, 
	so.dd_salesitemno, 
	so.dd_scheduleno,
	1 AdjDropToWH_calc,
	1 rowseqno
from fact_salesorder so
where so.dd_OTIFCalcFlag_Merck = 'Y'
	and so.Dim_DateidDlvrDocCreated = 1;

	/*NN - Oct 16 2015- Rewrite the scripts for tmp_SOLineAdjDt_001 without sub selects in CREATE/INSERT*/
	
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper;
	CREATE TABLE tmp_SOLineAdjDt_001_helper AS
	SELECT so.dd_salesdocno, 
		so.dd_salesitemno, 
		so.dd_scheduleno, 
		so.dd_SOLineCreateTime,
		cuttm.cutofftime,
		cuttm.cutoffweekday,
		ocdt.datevalue,
		ocdt.CompanyCode,	
		ocdt.BusinessDaysSeqNo,
		ocdt.weekdaynumber,
		ocdt.isaweekendday,
		ocdt.isapublicholiday,
		ocdt.plantcode_factory,
		sla.ordertoshipbdays,
		sddt.AdjDropToWH_calc
	FROM fact_salesorder so
		INNER JOIN Dim_Date ocdt ON so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid
		INNER JOIN tmp_DateDropToWH_001 sddt ON (so.dd_salesdocno = sddt.dd_salesdocno 
								AND so.dd_salesitemno = sddt.dd_salesitemno
								AND so.dd_scheduleno = sddt.dd_scheduleno
								AND sddt.rowseqno = 1) 
		INNER JOIN tmp_SOCustomerSLA_001 sla ON sla.dd_salesdocno = so.dd_salesdocno AND sla.dd_salesitemno = so.dd_salesitemno
		INNER JOIN dim_shipreceivepoint srpt ON srpt.dim_shipreceivepointid = so.dim_shipreceivepointid
		INNER JOIN shipptscutofftime cuttm ON cuttm.shippingpoint = srpt.shipreceivepointcode;
		
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001;
	CREATE TABLE tmp_SOLineAdjDt_001 AS
	SELECT DISTINCT dd_salesdocno, 
	dd_salesitemno, 
	dd_scheduleno, 
	CompanyCode,
	plantcode_factory,
	to_date('0001-01-01') AdjSOCreated_calc,
	to_date('0001-01-01') ExpDlvrDt,		
    AdjDropToWH_calc 
	FROM tmp_SOLineAdjDt_001_helper;
	
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper2;
	CREATE TABLE tmp_SOLineAdjDt_001_helper2 AS
	SELECT  v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno, MIN(ctdt.DateValue) AdjSOCreated_calc
	FROM tmp_SOLineAdjDt_001_helper v, dim_date ctdt 
    WHERE ctdt.CompanyCode = v.CompanyCode 
    AND ctdt.BusinessDaysSeqNo = (v.BusinessDaysSeqNo
	                             + 
	                             CASE WHEN v.dd_SOLineCreateTime > v.cutofftime 
								 	  OR (v.cutoffweekday = 'THURSDAY' AND v.weekdaynumber > 5) 
								 	  OR v.weekdaynumber IN (1,7)
								 THEN CASE WHEN v.cutoffweekday = 'THURSDAY' AND v.weekdaynumber = 5 
	  									   THEN 2 
	  									   ELSE 1 
									   END
								 ELSE CASE WHEN v.isaweekendday = 1 or v.isapublicholiday = 1 
								 		   THEN 1 
								 		   ELSE 0 
								 	   END
								  END)
	GROUP BY v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno;							  
	
	UPDATE tmp_SOLineAdjDt_001 t
	SET t.AdjSOCreated_calc = v.AdjSOCreated_calc
	FROM tmp_SOLineAdjDt_001_helper2 v, tmp_SOLineAdjDt_001 t
	WHERE t.dd_salesdocno = v.dd_salesdocno AND t.dd_salesitemno = v.dd_salesitemno AND t.dd_scheduleno = v.dd_scheduleno;
	
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper2;
	CREATE TABLE tmp_SOLineAdjDt_001_helper2 AS
	SELECT  v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno, MIN(ctdt.DateValue) ExpDlvrDt
	FROM tmp_SOLineAdjDt_001_helper v, dim_date ctdt 
    WHERE ctdt.CompanyCode = v.CompanyCode 
    AND ctdt.BusinessDaysSeqNo = (v.BusinessDaysSeqNo + v.ordertoshipbdays
								  + CASE WHEN v.dd_SOLineCreateTime > v.cutofftime 
											 OR (v.cutoffweekday = 'THURSDAY' AND v.weekdaynumber > 5) 
											 OR v.weekdaynumber IN (1,7)
										THEN CASE WHEN v.cutoffweekday = 'THURSDAY' AND v.weekdaynumber = 5 
	  											  THEN 2 
	  											  ELSE 1 
											 END
									    ELSE CASE WHEN v.isaweekendday = 1 or v.isapublicholiday = 1 
									              THEN 1 
									              ELSE 0 
									         END
									 END)
	GROUP BY v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno;	
	
	UPDATE tmp_SOLineAdjDt_001 t
	SET t.ExpDlvrDt = v.ExpDlvrDt
	FROM tmp_SOLineAdjDt_001_helper2 v, tmp_SOLineAdjDt_001 t
	WHERE t.dd_salesdocno = v.dd_salesdocno AND t.dd_salesitemno = v.dd_salesitemno AND t.dd_scheduleno = v.dd_scheduleno;
	
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper;
	CREATE TABLE tmp_SOLineAdjDt_001_helper AS
	SELECT so.dd_salesdocno, 
		so.dd_salesitemno, 
		so.dd_scheduleno, 
		so.dd_SOLineCreateTime,
		ocdt.datevalue,
		ocdt.CompanyCode,	
		ocdt.plantcode_factory,
		ocdt.BusinessDaysSeqNo,
		ocdt.weekdaynumber,
		ocdt.isaweekendday,
		ocdt.isapublicholiday,
		sla.ordertoshipbdays,
		sddt.AdjDropToWH_calc
	FROM fact_salesorder so
		INNER JOIN Dim_Date ocdt ON so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid
		INNER JOIN tmp_SOCustomerSLA_001 sla ON sla.dd_salesdocno = so.dd_salesdocno AND sla.dd_salesitemno = so.dd_salesitemno
		INNER JOIN dim_shipreceivepoint srpt ON srpt.dim_shipreceivepointid = so.dim_shipreceivepointid
		INNER JOIN tmp_DateDropToWH_001 sddt ON (so.dd_salesdocno = sddt.dd_salesdocno 
								AND so.dd_salesitemno = sddt.dd_salesitemno
								AND so.dd_scheduleno = sddt.dd_scheduleno
								AND sddt.rowseqno = 1)
	WHERE so.dd_OTIFCalcFlag_Merck = 'Y'
		AND NOT EXISTS (SELECT 1 FROM tmp_SOLineAdjDt_001 x 
			  WHERE x.dd_salesdocno = so.dd_salesdocno AND x.dd_salesitemno = so.dd_salesitemno AND x.dd_scheduleno = so.dd_scheduleno);
	
	INSERT INTO tmp_SOLineAdjDt_001	
	SELECT DISTINCT dd_salesdocno, 
	dd_salesitemno, 
	dd_scheduleno, 
	CompanyCode,
	plantcode_factory,
	to_date('0001-01-01') AdjSOCreated_calc,
	to_date('0001-01-01') ExpDlvrDt,		
    AdjDropToWH_calc 
	FROM tmp_SOLineAdjDt_001_helper;	

	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper2;
	CREATE TABLE tmp_SOLineAdjDt_001_helper2 AS
	SELECT  v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno, MIN(ctdt.DateValue) AdjSOCreated_calc
	FROM tmp_SOLineAdjDt_001_helper v, dim_date ctdt 
    WHERE ctdt.CompanyCode = v.CompanyCode 
    AND ctdt.BusinessDaysSeqNo = (v.BusinessDaysSeqNo + CASE WHEN v.isaweekendday = 1 or v.isapublicholiday = 1 THEN 1 ELSE 0 END)
	GROUP BY v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno;							  
	
	UPDATE tmp_SOLineAdjDt_001 t
	SET t.AdjSOCreated_calc = v.AdjSOCreated_calc
	FROM tmp_SOLineAdjDt_001_helper2 v, tmp_SOLineAdjDt_001 t
	WHERE t.dd_salesdocno = v.dd_salesdocno AND t.dd_salesitemno = v.dd_salesitemno AND t.dd_scheduleno = v.dd_scheduleno;
	
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper2;
	CREATE TABLE tmp_SOLineAdjDt_001_helper2 AS
	SELECT  v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno, MIN(ctdt.DateValue) ExpDlvrDt
	FROM tmp_SOLineAdjDt_001_helper v, dim_date ctdt 
    WHERE ctdt.CompanyCode = v.CompanyCode 
    AND ctdt.BusinessDaysSeqNo = (v.BusinessDaysSeqNo + v.ordertoshipbdays + CASE WHEN v.isaweekendday = 1 OR v.isapublicholiday = 1 THEN 1 ELSE 0 END)
	GROUP BY v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno;	
	
	UPDATE tmp_SOLineAdjDt_001 t
	SET t.ExpDlvrDt = v.ExpDlvrDt
	FROM tmp_SOLineAdjDt_001_helper2 v,  tmp_SOLineAdjDt_001 t
	WHERE t.dd_salesdocno = v.dd_salesdocno AND t.dd_salesitemno = v.dd_salesitemno AND t.dd_scheduleno = v.dd_scheduleno;
	
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper;
	CREATE TABLE tmp_SOLineAdjDt_001_helper AS
	SELECT so.dd_salesdocno, 
		so.dd_salesitemno, 
		so.dd_scheduleno, 
		so.dd_SOLineCreateTime,
		cuttm.cutofftime,
		cuttm.cutoffweekday,
		ocdt.datevalue,
		ocdt.CompanyCode,	
		ocdt.plantcode_factory,
		ocdt.BusinessDaysSeqNo,
		ocdt.weekdaynumber,
		ocdt.isaweekendday,
		ocdt.isapublicholiday,
		sddt.AdjDropToWH_calc,
		pgid.BusinessDaysSeqNo BusinessDaysSeqNo_pgid, 
		pgid.isaweekendday isaweekendday_pgid,
		pgid.isapublicholiday isapublicholiday_pgid
	from fact_salesorder so
	inner join Dim_Date ocdt on so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid 
	inner join Dim_Date pgid on so.Dim_DateidGoodsIssue = pgid.Dim_Dateid 
	inner join tmp_DateDropToWH_001 sddt on (so.dd_salesdocno = sddt.dd_salesdocno 
							and so.dd_salesitemno = sddt.dd_salesitemno
							and so.dd_scheduleno = sddt.dd_scheduleno
							and sddt.rowseqno = 1) 
	inner join dim_shipreceivepoint srpt on srpt.dim_shipreceivepointid = so.dim_shipreceivepointid
	inner join shipptscutofftime cuttm on cuttm.shippingpoint = srpt.shipreceivepointcode
	where so.dd_OTIFCalcFlag_Merck = 'Y'
	and not exists (select 1 from tmp_SOLineAdjDt_001 x 
		  where x.dd_salesdocno = so.dd_salesdocno and x.dd_salesitemno = so.dd_salesitemno and x.dd_scheduleno = so.dd_scheduleno);

	INSERT INTO tmp_SOLineAdjDt_001	
	SELECT DISTINCT dd_salesdocno, 
	dd_salesitemno, 
	dd_scheduleno, 
	CompanyCode,
	plantcode_factory,
	to_date('0001-01-01') AdjSOCreated_calc,
	to_date('0001-01-01') ExpDlvrDt,		
    AdjDropToWH_calc 
	FROM tmp_SOLineAdjDt_001_helper;	

	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper2;
	CREATE TABLE tmp_SOLineAdjDt_001_helper2 AS
	SELECT  v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno, MIN(ctdt.DateValue) AdjSOCreated_calc
	FROM tmp_SOLineAdjDt_001_helper v, dim_date ctdt 
    WHERE ctdt.CompanyCode = v.CompanyCode 
    AND ctdt.BusinessDaysSeqNo = (v.BusinessDaysSeqNo
								  + case when v.dd_SOLineCreateTime > v.cutofftime 
										 or (v.cutoffweekday = 'THURSDAY' and v.weekdaynumber > 5) 
										 or v.weekdaynumber in (1,7)
									then case when v.cutoffweekday = 'THURSDAY' and v.weekdaynumber = 5 
											  then 2 
											  else 1 
										end
									else case when v.isaweekendday = 1 or v.isapublicholiday = 1 
											  then 1 
											  else 0 
										 end
									end)
	GROUP BY v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno;							  
	
	UPDATE tmp_SOLineAdjDt_001 t
	SET t.AdjSOCreated_calc = v.AdjSOCreated_calc
	FROM tmp_SOLineAdjDt_001_helper2 v, tmp_SOLineAdjDt_001 t
	WHERE t.dd_salesdocno = v.dd_salesdocno AND t.dd_salesitemno = v.dd_salesitemno AND t.dd_scheduleno = v.dd_scheduleno;
	
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper2;
	CREATE TABLE tmp_SOLineAdjDt_001_helper2 AS
	SELECT  v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno, MIN(ctdt.DateValue) ExpDlvrDt
	FROM tmp_SOLineAdjDt_001_helper v, dim_date ctdt 
    WHERE ctdt.CompanyCode = v.CompanyCode 
    AND ctdt.BusinessDaysSeqNo = (v.BusinessDaysSeqNo_pgid + case when v.isaweekendday_pgid = 1 or v.isapublicholiday_pgid = 1 then 1 else 0 end)
	GROUP BY v.dd_salesdocno, v.dd_salesitemno, v.dd_scheduleno;	
	
	UPDATE tmp_SOLineAdjDt_001 t
	SET t.ExpDlvrDt = v.ExpDlvrDt
	FROM tmp_SOLineAdjDt_001_helper2 v, tmp_SOLineAdjDt_001 t
	WHERE t.dd_salesdocno = v.dd_salesdocno AND t.dd_salesitemno = v.dd_salesitemno AND t.dd_scheduleno = v.dd_scheduleno;
	
	INSERT INTO tmp_SOLineAdjDt_001
	select so.dd_salesdocno, 
		so.dd_salesitemno, 
		so.dd_scheduleno, 
		ocdt.CompanyCode,
		ocdt.plantcode_factory,
		ocdt.DateValue AdjSOCreated_calc,
		pgid.DateValue ExpDlvrDt,
		sddt.AdjDropToWH_calc
	from fact_salesorder so
		inner join Dim_Date ocdt on so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid -- SO line created date
		inner join Dim_Date pgid on so.Dim_DateidGoodsIssue = pgid.Dim_Dateid -- planned GI date
		inner join tmp_DateDropToWH_001 sddt on (so.dd_salesdocno = sddt.dd_salesdocno 
								and so.dd_salesitemno = sddt.dd_salesitemno
								and so.dd_scheduleno = sddt.dd_scheduleno
								and sddt.rowseqno = 1) -- Shipment created date
	where so.dd_OTIFCalcFlag_Merck = 'Y'
		and not exists (select 1 from tmp_SOLineAdjDt_001 x 
				where x.dd_salesdocno = so.dd_salesdocno and x.dd_salesitemno = so.dd_salesitemno and x.dd_scheduleno = so.dd_scheduleno);
	
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper;
	DROP TABLE IF EXISTS tmp_SOLineAdjDt_001_helper2;

	/*NN - Oct 16 2015- Rewrite the scripts for tmp_SOLineAdjDt_001 without sub selects in CREATE/INSERT*/

UPDATE tmp_SOLineAdjDt_001 t
SET t.ExpDlvrDt = dd.datevalue
FROM fact_salesorder so, dim_salesorg ds,dim_distributionchannel dc, dim_date dd,  tmp_SOLineAdjDt_001 t
WHERE so.dim_salesorgid = ds.dim_salesorgid 
	  and so.dim_distributionchannelid = dc.dim_distributionchannelid
	  and salesorgcode ='NL10' 
	  and distributionchannelcode = '50'
	  and so.dim_dateidorginalrequestdate = dd.dim_dateid
	  and t.ExpDlvrDt <> dd.datevalue
	  and t.dd_salesdocno = so.dd_salesdocno 
	  and t.dd_salesitemno = so.dd_salesitemno 
	  and t.dd_scheduleno = so.dd_scheduleno;
	  
/* Script to update Expected GI Date for OTIF - BI-788 */	

UPDATE fact_salesorder so
SET so.dim_DateidExpGI_Merck = so.dim_dateidorginalrequestdate
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_salesorg ds,dim_distributionchannel dc,fact_salesorder so
WHERE so.dim_salesorgid = ds.dim_salesorgid 
	  and so.dim_distributionchannelid = dc.dim_distributionchannelid
	 -- and salesorgcode ='NL10' 
	  and distributionchannelcode = '50'
	  and so.dim_DateidExpGI_Merck <> so.dim_dateidorginalrequestdate;
	  
/* Script to update Expected GI Date for OTIF - BI-788 */		
	  
/*Andrian - update channel 50 with Transit Calendar Days based on BI-2308*/
UPDATE fact_salesorder so
SET so.dim_DateidExpGI_Merck = edd.dim_dateid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_distributionchannel dc, dim_date dd, dim_date edd, dim_route dr,  fact_salesorder so
WHERE  so.dim_distributionchannelid = dc.dim_distributionchannelid
	  and distributionchannelcode = '50'
	  and so.dim_DateidExpGI_Merck = dd.dim_dateid
	  and so.dim_routeid = dr.dim_routeid
	  and dd.datevalue-convert(integer, dr.transittimeincalendardays) = edd.datevalue
	  and dd.companycode = edd.companycode
	  and dd.plantcode_factory = edd.plantcode_factory
	  and so.dim_DateidExpGI_Merck <> 1
	  and so.dim_DateidExpGI_Merck <> edd.dim_dateid;

UPDATE tmp_SOLineAdjDt_001 t
SET t.ExpDlvrDt = dd.datevalue
FROM fact_salesorder so, dim_salesorg ds,dim_distributionchannel dc, dim_date dd, tmp_SOLineAdjDt_001 t
WHERE so.dim_salesorgid = ds.dim_salesorgid 
	  and so.dim_distributionchannelid = dc.dim_distributionchannelid
	  and distributionchannelcode = '50'
	  and so.dim_DateidExpGI_Merck = dd.dim_dateid
	  and t.ExpDlvrDt <> dd.datevalue
	  and t.dd_salesdocno = so.dd_salesdocno 
	  and t.dd_salesitemno = so.dd_salesitemno 
	  and t.dd_scheduleno = so.dd_scheduleno;
	  
UPDATE    fact_salesorder so
SET so.dim_dateidshiptopartypodate  =  dt.dim_dateid
	,so.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbkd vkd, dim_date dt, dim_company dc,  fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR 
AND so.dim_companyid  =  dc.dim_companyid
AND dt.datevalue  =  vkd.VBKD_BSTDK_E
AND dt.companycode  =  dc.CompanyCode
and dt.plantcode_factory = 'Not Set'
AND vkd.VBKD_BSTDK_E IS NOT NULL
AND so.dim_dateidshiptopartypodate <> dt.dim_dateid;		  
	  

DROP TABLE IF EXISTS tmp_SOLineShipDetail_001;
CREATE TABLE tmp_SOLineShipDetail_001 AS
select so.dd_salesdocno, 
	so.dd_salesitemno, 
	so.dd_scheduleno, 
	so.ct_scheduleqtysalesunit OrderQty, 
	so.ct_confirmedqty ConfirmedQty, 
	so.ct_DeliveredQty DeliveredQty, 
	ocdt.DateValue SOCreated, 
	ddc.datevalue SODlvrDocCreated, 
	pgid.DateValue PlannedGI, 
	agidt.datevalue ActualGI,
	sdrdp.DateValue ReqDD, 
	sdd.DateValue PromiseDD, 
	spdd.DateValue ActualDD,
	case when so.dim_dateidshiptopartypodate = 1 AND ((agidt.BusinessDaysSeqNo - case when dc.distributionchannelcode = '50' then expdt.BusinessDaysSeqNo else adjdt.BusinessDaysSeqNo end) between (sla.ordertoshipbdays - sla.daysearly) and (sla.ordertoshipbdays + sla.dayslate)) then so.ct_DeliveredQty else 0 end OTIFQty,
	case when so.dim_dateidshiptopartypodate = 1 AND (agidt.BusinessDaysSeqNo - case when dc.distributionchannelcode = '50' then expdt.BusinessDaysSeqNo else adjdt.BusinessDaysSeqNo end) between (sla.ordertoshipbdays - sla.daysearly) and (sla.ordertoshipbdays + sla.dayslate)
		  and (so.ct_scheduleqtysalesunit <= so.ct_DeliveredQty 
				or (so.ct_DeliveredQty = 0 and so.ct_scheduleqtysalesunit = so.ct_ConfirmedQty)) then 'Y' else 'N' end LineOTIFFlag,
	case when so.dim_dateidshiptopartypodate = 1 AND (so.dd_BackOrderFlag_Merck <> 'X' 
			or (so.dd_BackOrderFlag_Merck = 'X' and (agidt.BusinessDaysSeqNo - case when dc.distributionchannelcode = '50' then expdt.BusinessDaysSeqNo else adjdt.BusinessDaysSeqNo end) between (sla.ordertoshipbdays - sla.daysearly) and (sla.ordertoshipbdays + sla.dayslate)))
			and (so.ct_scheduleqtysalesunit <= so.ct_DeliveredQty 
				or (so.ct_DeliveredQty = 0 and so.ct_scheduleqtysalesunit = so.ct_ConfirmedQty)) then 'Y' else 'N' end InFullFlag,
	case when so.dim_dateidshiptopartypodate = 1 AND (agidt.BusinessDaysSeqNo - case when dc.distributionchannelcode = '50' then expdt.BusinessDaysSeqNo else adjdt.BusinessDaysSeqNo end) between (sla.ordertoshipbdays - sla.daysearly) and (sla.ordertoshipbdays + sla.dayslate) then 'Y' else 'N' end OnTimeFlag,
	sorr.rejectreasoncode,
	dd_OTIFCalcFlag_Merck,
	expdt.Dim_Dateid ExpectedGIDate,
	adjdt.Dim_Dateid AdjSOCreatedDate,
	sdt.documenttype,
	ic.salesorderitemcategory,
	so.dim_deliveryblockid,
	so.Dim_OverallStatusCreditCheckId
from fact_salesorder so
	inner join Dim_Date spdd on so.Dim_DateidShipmentDelivery = spdd.Dim_Dateid -- delivery date from shipment - actual
	inner join Dim_Date agidt on so.Dim_DateidActualGI = agidt.Dim_Dateid -- actual GI date
	inner join Dim_Date pgid on so.Dim_DateidGoodsIssue = pgid.Dim_Dateid -- planned GI date
	inner join Dim_Date fd on so.Dim_DateidFirstDate = fd.Dim_Dateid -- first date
	inner join Dim_Date ocdt on so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid -- SO line created date
	inner join Dim_Date sdd on so.Dim_DateidSchedDelivery = sdd.Dim_Dateid -- promise delivery date
	inner join Dim_Date sdrdp on so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid -- requested delivery date
	inner join Dim_Date td on so.Dim_DateidTransport = td.Dim_Dateid -- transport date
	inner join Dim_Date ddc on so.Dim_DateidDlvrDocCreated = ddc.Dim_Dateid -- delivery doc created date
	inner join dim_salesdocumenttype sdt on sdt.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid
	inner join dim_salesorderitemcategory ic on so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
	inner join dim_salesorderheaderstatus sohs on sohs.Dim_SalesOrderHeaderStatusid = so.Dim_SalesOrderHeaderStatusid
	inner join tmp_SOLineDlvrCount_001 cnt on cnt.dd_salesdocno = so.dd_salesdocno and cnt.dd_salesitemno = so.dd_salesitemno and cnt.dd_scheduleno = so.dd_scheduleno
	inner join tmp_SOCustomerSLA_001 sla on sla.dd_salesdocno = so.dd_salesdocno and sla.dd_salesitemno = so.dd_salesitemno
	inner join Dim_SalesOrderRejectReason sorr on so.Dim_SalesOrderRejectReasonid = sorr.Dim_SalesOrderRejectReasonid
	inner join tmp_SOLineAdjDt_001 adj on adj.dd_salesdocno = so.dd_salesdocno and adj.dd_salesitemno = so.dd_salesitemno and adj.dd_scheduleno = so.dd_scheduleno
	inner join Dim_Date adjdt on adjdt.DateValue = adj.AdjSOCreated_calc and adjdt.CompanyCode = adj.CompanyCode and adjdt.plantcode_factory = adj.plantcode_factory
	inner join Dim_Date expdt on expdt.DateValue = adj.ExpDlvrDt and expdt.CompanyCode = adj.CompanyCode and expdt.plantcode_factory = adj.plantcode_factory
	inner join Dim_SalesOrg ds on so.dim_salesorgid = ds.dim_salesorgid
	inner join dim_distributionchannel dc on so.dim_distributionchannelid = dc.dim_distributionchannelid
where so.dd_OTIFCalcFlag_Merck = 'Y' 
	and cnt.TotDlvrLines = 1
	and sla.has_sla = 'Y';
	
INSERT INTO tmp_SOLineShipDetail_001
select so.dd_salesdocno, 
	so.dd_salesitemno, 
	so.dd_scheduleno, 
	so.ct_scheduleqtysalesunit OrderQty, 
	so.ct_confirmedqty ConfirmedQty, 
	so.ct_DeliveredQty DeliveredQty, 
	ocdt.DateValue SOCreated, 
	ddc.datevalue SODlvrDocCreated, 
	pgid.DateValue PlannedGI, 
	agidt.datevalue ActualGI,
	sdrdp.DateValue ReqDD, 
	sdd.DateValue PromiseDD, 
	spdd.DateValue ActualDD,
	case when pgid.BusinessDaysSeqNo between (agidt.BusinessDaysSeqNo - 5) and agidt.BusinessDaysSeqNo+1 then so.ct_DeliveredQty else 0 end OTIFQty,
	case when pgid.BusinessDaysSeqNo between (agidt.BusinessDaysSeqNo - 5) and agidt.BusinessDaysSeqNo+1 
		  and (so.ct_scheduleqtysalesunit <= so.ct_DeliveredQty 
				or (so.ct_DeliveredQty = 0 and so.ct_scheduleqtysalesunit = so.ct_ConfirmedQty)) then 'Y' else 'N' end LineOTIFFlag,
	case when (so.dd_BackOrderFlag_Merck <> 'X' 
			or (so.dd_BackOrderFlag_Merck = 'X' and pgid.BusinessDaysSeqNo between (agidt.BusinessDaysSeqNo - 5) and agidt.BusinessDaysSeqNo+1))
			and (so.ct_scheduleqtysalesunit <= so.ct_DeliveredQty 
				or (so.ct_DeliveredQty = 0 and so.ct_scheduleqtysalesunit = so.ct_ConfirmedQty)) then 'Y' else 'N' end InFullFlag,
	case when pgid.BusinessDaysSeqNo between (agidt.BusinessDaysSeqNo - 5) and agidt.BusinessDaysSeqNo+1 then 'Y' else 'N' end OnTimeFlag,
	sorr.rejectreasoncode,
	dd_OTIFCalcFlag_Merck,
	expdt.Dim_Dateid ExpectedGIDate,
	adjdt.Dim_Dateid AdjSOCreatedDate,
	sdt.documenttype,
	ic.salesorderitemcategory,
	so.dim_deliveryblockid,
	so.Dim_OverallStatusCreditCheckId
from fact_salesorder so
	inner join Dim_Date spdd on so.Dim_DateidShipmentDelivery = spdd.Dim_Dateid -- delivery date from shipment - actual
	inner join Dim_Date agidt on so.Dim_DateidActualGI = agidt.Dim_Dateid -- actual GI date
	inner join Dim_Date pgid on so.Dim_DateidGoodsIssue = pgid.Dim_Dateid -- planned GI date
	inner join Dim_Date fd on so.Dim_DateidFirstDate = fd.Dim_Dateid -- first date
	inner join Dim_Date ocdt on so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid -- SO line created date
	inner join Dim_Date sdd on so.Dim_DateidSchedDelivery = sdd.Dim_Dateid -- promise delivery date
	inner join Dim_Date sdrdp on so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid -- requested delivery date
	inner join Dim_Date td on so.Dim_DateidTransport = td.Dim_Dateid -- transport date
	inner join Dim_Date ddc on so.Dim_DateidDlvrDocCreated = ddc.Dim_Dateid -- delivery doc created date
	inner join dim_salesdocumenttype sdt on sdt.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid
	inner join dim_salesorderitemcategory ic on so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
	inner join dim_salesorderheaderstatus sohs on sohs.Dim_SalesOrderHeaderStatusid = so.Dim_SalesOrderHeaderStatusid
	inner join tmp_SOLineDlvrCount_001 cnt on cnt.dd_salesdocno = so.dd_salesdocno and cnt.dd_salesitemno = so.dd_salesitemno and cnt.dd_scheduleno = so.dd_scheduleno
	inner join tmp_SOCustomerSLA_001 sla on sla.dd_salesdocno = so.dd_salesdocno and sla.dd_salesitemno = so.dd_salesitemno
	inner join Dim_SalesOrderRejectReason sorr on so.Dim_SalesOrderRejectReasonid = sorr.Dim_SalesOrderRejectReasonid
	inner join tmp_SOLineAdjDt_001 adj on adj.dd_salesdocno = so.dd_salesdocno and adj.dd_salesitemno = so.dd_salesitemno and adj.dd_scheduleno = so.dd_scheduleno
	inner join Dim_Date adjdt on adjdt.DateValue = adj.AdjSOCreated_calc and adjdt.CompanyCode = adj.CompanyCode and adjdt.plantcode_factory = adj.plantcode_factory
	inner join Dim_Date expdt on expdt.DateValue = adj.ExpDlvrDt and expdt.CompanyCode = adj.CompanyCode and expdt.plantcode_factory = adj.plantcode_factory
where so.dd_OTIFCalcFlag_Merck = 'Y' 
	and cnt.TotDlvrLines = 1
	and sla.has_sla = 'N';
	
INSERT INTO tmp_SOLineShipDetail_001
select so.dd_salesdocno, 
	so.dd_salesitemno, 
	so.dd_scheduleno, 
	so.ct_scheduleqtysalesunit OrderQty, 
	so.ct_confirmedqty ConfirmedQty, 
	so.ct_DeliveredQty DeliveredQty, 
	ocdt.DateValue SOCreated, 
	ddc.datevalue SODlvrDocCreated, 
	pgid.DateValue PlannedGI, 
	agidt.datevalue ActualGI,
	sdrdp.DateValue ReqDD, 
	sdd.DateValue PromiseDD, 
	spdd.DateValue ActualDD,
	0 OTIFQty,
	'N' LineOTIFFlag,
	'N' InFullFlag,
	case when so.dim_dateidshiptopartypodate = 1 AND (agidt.BusinessDaysSeqNo - case when dc.distributionchannelcode = '50' then expdt.BusinessDaysSeqNo else adjdt.BusinessDaysSeqNo end) between (sla.ordertoshipbdays - sla.daysearly) and (sla.ordertoshipbdays + sla.dayslate) then 'Y' else 'N' end OnTimeFlag,
	sorr.rejectreasoncode,
	dd_OTIFCalcFlag_Merck,
	expdt.Dim_Dateid ExpectedGIDate,
	adjdt.Dim_Dateid AdjSOCreatedDate,
	sdt.documenttype,
	ic.salesorderitemcategory,
	so.dim_deliveryblockid,
	so.Dim_OverallStatusCreditCheckId
from fact_salesorder so
	inner join Dim_Date spdd on so.Dim_DateidShipmentDelivery = spdd.Dim_Dateid -- delivery date from shipment - actual
	inner join Dim_Date agidt on so.Dim_DateidActualGI = agidt.Dim_Dateid -- actual GI date
	inner join Dim_Date pgid on so.Dim_DateidGoodsIssue = pgid.Dim_Dateid -- planned GI date
	inner join Dim_Date fd on so.Dim_DateidFirstDate = fd.Dim_Dateid -- first date
	inner join Dim_Date ocdt on so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid -- SO line created date
	inner join Dim_Date sdd on so.Dim_DateidSchedDelivery = sdd.Dim_Dateid -- promise delivery date
	inner join Dim_Date sdrdp on so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid -- requested delivery date
	inner join Dim_Date td on so.Dim_DateidTransport = td.Dim_Dateid -- transport date
	inner join Dim_Date ddc on so.Dim_DateidDlvrDocCreated = ddc.Dim_Dateid -- delivery doc created date
	inner join dim_salesdocumenttype sdt on sdt.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid
	inner join dim_salesorderitemcategory ic on so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
	inner join dim_salesorderheaderstatus sohs on sohs.Dim_SalesOrderHeaderStatusid = so.Dim_SalesOrderHeaderStatusid
	inner join tmp_SOLineDlvrCount_001 cnt on cnt.dd_salesdocno = so.dd_salesdocno and cnt.dd_salesitemno = so.dd_salesitemno and cnt.dd_scheduleno = so.dd_scheduleno
	inner join tmp_SOCustomerSLA_001 sla on sla.dd_salesdocno = so.dd_salesdocno and sla.dd_salesitemno = so.dd_salesitemno
	inner join Dim_SalesOrderRejectReason sorr on so.Dim_SalesOrderRejectReasonid = sorr.Dim_SalesOrderRejectReasonid
	inner join tmp_SOLineAdjDt_001 adj on adj.dd_salesdocno = so.dd_salesdocno and adj.dd_salesitemno = so.dd_salesitemno and adj.dd_scheduleno = so.dd_scheduleno
	inner join Dim_Date adjdt on adjdt.DateValue = adj.AdjSOCreated_calc and adjdt.CompanyCode = adj.CompanyCode and adjdt.plantcode_factory = adj.plantcode_factory
	inner join Dim_Date expdt on expdt.DateValue = adj.ExpDlvrDt and expdt.CompanyCode = adj.CompanyCode and expdt.plantcode_factory = adj.plantcode_factory
	inner join Dim_SalesOrg ds on so.dim_salesorgid = ds.dim_salesorgid
	inner join dim_distributionchannel dc on so.dim_distributionchannelid = dc.dim_distributionchannelid
where so.dd_OTIFCalcFlag_Merck = 'Y' 
	and cnt.TotDlvrLines > 1
	and sla.has_sla = 'Y';
	
INSERT INTO tmp_SOLineShipDetail_001
select so.dd_salesdocno, 
	so.dd_salesitemno, 
	so.dd_scheduleno, 
	so.ct_scheduleqtysalesunit OrderQty, 
	so.ct_confirmedqty ConfirmedQty, 
	so.ct_DeliveredQty DeliveredQty, 
	ocdt.DateValue SOCreated, 
	ddc.datevalue SODlvrDocCreated, 
	pgid.DateValue PlannedGI, 
	agidt.datevalue ActualGI,
	sdrdp.DateValue ReqDD, 
	sdd.DateValue PromiseDD, 
	spdd.DateValue ActualDD,
	0 OTIFQty,
	'N' LineOTIFFlag,
	'N' InFullFlag,
	case when pgid.BusinessDaysSeqNo between (agidt.BusinessDaysSeqNo - 5) and agidt.BusinessDaysSeqNo+1 then 'Y' else 'N' end OnTimeFlag,
	sorr.rejectreasoncode,
	dd_OTIFCalcFlag_Merck,
	expdt.Dim_Dateid ExpectedGIDate,
	adjdt.Dim_Dateid AdjSOCreatedDate,
	sdt.documenttype,
	ic.salesorderitemcategory,
	so.dim_deliveryblockid,
	so.Dim_OverallStatusCreditCheckId
from fact_salesorder so
	inner join Dim_Date spdd on so.Dim_DateidShipmentDelivery = spdd.Dim_Dateid -- delivery date from shipment - actual
	inner join Dim_Date agidt on so.Dim_DateidActualGI = agidt.Dim_Dateid -- actual GI date
	inner join Dim_Date pgid on so.Dim_DateidGoodsIssue = pgid.Dim_Dateid -- planned GI date
	inner join Dim_Date fd on so.Dim_DateidFirstDate = fd.Dim_Dateid -- first date
	inner join Dim_Date ocdt on so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid -- SO line created date
	inner join Dim_Date sdd on so.Dim_DateidSchedDelivery = sdd.Dim_Dateid -- promise delivery date
	inner join Dim_Date sdrdp on so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid -- requested delivery date
	inner join Dim_Date td on so.Dim_DateidTransport = td.Dim_Dateid -- transport date
	inner join Dim_Date ddc on so.Dim_DateidDlvrDocCreated = ddc.Dim_Dateid -- delivery doc created date
	inner join dim_salesdocumenttype sdt on sdt.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid
	inner join dim_salesorderitemcategory ic on so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
	inner join dim_salesorderheaderstatus sohs on sohs.Dim_SalesOrderHeaderStatusid = so.Dim_SalesOrderHeaderStatusid
	inner join tmp_SOLineDlvrCount_001 cnt on cnt.dd_salesdocno = so.dd_salesdocno and cnt.dd_salesitemno = so.dd_salesitemno and cnt.dd_scheduleno = so.dd_scheduleno
	inner join tmp_SOCustomerSLA_001 sla on sla.dd_salesdocno = so.dd_salesdocno and sla.dd_salesitemno = so.dd_salesitemno
	inner join Dim_SalesOrderRejectReason sorr on so.Dim_SalesOrderRejectReasonid = sorr.Dim_SalesOrderRejectReasonid
	inner join tmp_SOLineAdjDt_001 adj on adj.dd_salesdocno = so.dd_salesdocno and adj.dd_salesitemno = so.dd_salesitemno and adj.dd_scheduleno = so.dd_scheduleno
	inner join Dim_Date adjdt on adjdt.DateValue = adj.AdjSOCreated_calc and adjdt.CompanyCode = adj.CompanyCode  and adjdt.plantcode_factory = adj.plantcode_factory
	inner join Dim_Date expdt on expdt.DateValue = adj.ExpDlvrDt and expdt.CompanyCode = adj.CompanyCode and expdt.plantcode_factory = adj.plantcode_factory
where so.dd_OTIFCalcFlag_Merck = 'Y' 
	and cnt.TotDlvrLines > 1
	and sla.has_sla = 'N';
	
INSERT INTO tmp_SOLineShipDetail_001
select so.dd_salesdocno, 
	so.dd_salesitemno, 
	so.dd_scheduleno, 
	so.ct_scheduleqtysalesunit OrderQty, 
	so.ct_confirmedqty ConfirmedQty, 
	so.ct_DeliveredQty DeliveredQty, 
	ocdt.DateValue SOCreated, 
	ddc.datevalue SODlvrDocCreated, 
	pgid.DateValue PlannedGI, 
	agidt.datevalue ActualGI,
	sdrdp.DateValue ReqDD, 
	sdd.DateValue PromiseDD, 
	spdd.DateValue ActualDD,
	0 OTIFQty,
	'N' LineOTIFFlag,
	'N' InFullFlag,
	'N' OnTimeFlag,
	sorr.rejectreasoncode,
	dd_OTIFCalcFlag_Merck,
	expdt.Dim_Dateid ExpectedGIDate,
	adjdt.Dim_Dateid AdjSOCreatedDate,
	sdt.documenttype,
	ic.salesorderitemcategory,
	so.dim_deliveryblockid,
	so.Dim_OverallStatusCreditCheckId
from fact_salesorder so
	inner join Dim_Date spdd on so.Dim_DateidShipmentDelivery = spdd.Dim_Dateid -- delivery date from shipment - actual
	inner join Dim_Date agidt on so.Dim_DateidActualGI = agidt.Dim_Dateid -- actual GI date
	inner join Dim_Date pgid on so.Dim_DateidGoodsIssue = pgid.Dim_Dateid -- planned GI date
	inner join Dim_Date fd on so.Dim_DateidFirstDate = fd.Dim_Dateid -- first date
	inner join Dim_Date ocdt on so.Dim_DateidSalesOrderCreated = ocdt.Dim_Dateid -- SO line created date
	inner join Dim_Date sdd on so.Dim_DateidSchedDelivery = sdd.Dim_Dateid -- promise delivery date
	inner join Dim_Date sdrdp on so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid -- requested delivery date
	inner join Dim_Date td on so.Dim_DateidTransport = td.Dim_Dateid -- transport date
	inner join Dim_Date ddc on so.Dim_DateidDlvrDocCreated = ddc.Dim_Dateid -- delivery doc created date
	inner join dim_salesdocumenttype sdt on sdt.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid
	inner join dim_salesorderitemcategory ic on so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
	inner join dim_salesorderheaderstatus sohs on sohs.Dim_SalesOrderHeaderStatusid = so.Dim_SalesOrderHeaderStatusid
	inner join Dim_SalesOrderRejectReason sorr on so.Dim_SalesOrderRejectReasonid = sorr.Dim_SalesOrderRejectReasonid
	inner join tmp_SOLineAdjDt_001 adj on adj.dd_salesdocno = so.dd_salesdocno and adj.dd_salesitemno = so.dd_salesitemno and adj.dd_scheduleno = so.dd_scheduleno
	inner join Dim_Date adjdt on adjdt.DateValue = adj.AdjSOCreated_calc and adjdt.CompanyCode = adj.CompanyCode  and adjdt.plantcode_factory = adj.plantcode_factory
	inner join Dim_Date expdt on expdt.DateValue = adj.ExpDlvrDt and expdt.CompanyCode = adj.CompanyCode          and expdt.plantcode_factory = adj.plantcode_factory
	inner join dim_overallstatusforcreditcheck scc on so.Dim_OverallStatusCreditCheckId = scc.dim_overallstatusforcreditcheckid
	inner join dim_deliveryblock db on so.dim_deliveryblockid = db.dim_deliveryblockid
where so.dd_OTIFCalcFlag_Merck = 'Y' 
	and not exists (select 1 from tmp_SOLineShipDetail_001 x
			where x.dd_salesdocno = so.dd_salesdocno and x.dd_salesitemno = so.dd_salesitemno)
	and (sorr.rejectreasoncode in ('01',  'Z0', 'Z2', 'Z6', 'ZU')
		or db.deliveryblock = 'ZD'
		or scc.OverAllStatusForCreditCheck = 'B');

UPDATE tmp_SOLineShipDetail_001 so
SET so.OTIFQty = 0,
	so.LineOTIFFlag = 'N',
	so.InFullFlag = 'N',
	so.OnTimeFlag = 'N' 
FROM dim_overallstatusforcreditcheck scc, dim_deliveryblock db, tmp_SOLineShipDetail_001 so
WHERE so.Dim_OverallStatusCreditCheckId = scc.dim_overallstatusforcreditcheckid
	and so.dim_deliveryblockid = db.dim_deliveryblockid
	and (so.rejectreasoncode in ('01',  'Z0', 'Z2', 'Z6', 'ZU')
		or db.deliveryblock = 'ZD'
		or scc.OverAllStatusForCreditCheck = 'B'); 
		
/* 19 Oct 2018 Georgiana changes Unable to get a stable source of rows fix*/
/*UPDATE tmp_SOLineShipDetail_001 a
SET a.InFullFlag = 'N', a.LineOTIFFlag = 'N', a.OTIFQty = 0
FROM tmp_SOLineShipDetail_001 a, fact_salesorder so 
	inner join dim_salesorderitemstatus ist on so.dim_salesorderitemstatusid = ist.dim_salesorderitemstatusid 
WHERE so.dd_salesdocno = a.dd_salesdocno
	and so.dd_salesitemno = a.dd_salesitemno
	and ist.ConfirmationStatus = 'Not yet processed'
	and so.ct_scheduleqtysalesunit > so.ct_ConfirmedQty
	and so.ct_scheduleqtysalesunit > so.ct_DeliveredQty
	and (a.InFullFlag = 'Y' or a.LineOTIFFlag = 'Y')*/

	
merge into tmp_SOLineShipDetail_001 a
using (
select distinct a.dd_salesdocno,a.dd_salesitemno,a.dd_scheduleno
FROM tmp_SOLineShipDetail_001 a, fact_salesorder so
inner join dim_salesorderitemstatus ist on so.dim_salesorderitemstatusid = ist.dim_salesorderitemstatusid
WHERE so.dd_salesdocno = a.dd_salesdocno
and so.dd_salesitemno = a.dd_salesitemno
and ist.ConfirmationStatus = 'Not yet processed'
and so.ct_scheduleqtysalesunit > so.ct_ConfirmedQty
and so.ct_scheduleqtysalesunit > so.ct_DeliveredQty
and (a.InFullFlag = 'Y' or a.LineOTIFFlag = 'Y')) t
on t.dd_salesdocno=a.dd_salesdocno
and t.dd_salesitemno=a.dd_salesitemno
and t.dd_scheduleno=a.dd_scheduleno
when matched then update set a.InFullFlag = 'N', a.LineOTIFFlag = 'N', a.OTIFQty = 0;

DROP TABLE IF EXISTS tmp_SOLineDlvrCount_001;
CREATE TABLE tmp_SOLineDlvrCount_001 AS
select dd_salesdocno, count(*) TotOTIFLines
from tmp_SOLineShipDetail_001
where LineOTIFFlag = 'Y'
group by dd_salesdocno;

DROP TABLE IF EXISTS tmp_SOLineIFCount_001;
CREATE TABLE tmp_SOLineIFCount_001 AS
select dd_salesdocno, count(*) TotIFLines
from tmp_SOLineShipDetail_001
where InFullFlag = 'Y'
group by dd_salesdocno;

DROP TABLE IF EXISTS tmp_SOLineOTCount_001;
CREATE TABLE tmp_SOLineOTCount_001 AS
select dd_salesdocno, count(*) TotOTLines
from tmp_SOLineShipDetail_001
where OnTimeFlag = 'Y'
group by dd_salesdocno;

DROP TABLE IF EXISTS tmp_SOLineDlvrCount_002;
CREATE TABLE tmp_SOLineDlvrCount_002 AS
select dd_salesdocno, count(*) TotSOLines, sum(case when sdt.documenttype = 'ZSO' and ic.salesorderitemcategory = 'TANN' then 1 else 0 end) TotSOTANNLines
from fact_salesorder so
	inner join dim_salesdocumenttype sdt on sdt.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid
	inner join dim_salesorderitemcategory ic on so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
where exists (select 1 from tmp_SOLineShipDetail_001 so0 where so0.dd_salesdocno = so.dd_salesdocno)
	and dd_OTIFCalcFlag_Merck = 'Y'
group by dd_salesdocno;

DROP TABLE IF EXISTS tmp_SOLineShipDetail_002;
CREATE TABLE tmp_SOLineShipDetail_002 AS
select DISTINCT a.dd_salesdocno, 
	a.dd_salesitemno, 
	a.dd_scheduleno, 
	a.OrderQty, 
	a.ConfirmedQty, 
	a.DeliveredQty, 
	a.SOCreated, 
	a.SODlvrDocCreated, 
	a.PlannedGI, 
	a.ActualGI,
	a.ReqDD, 
	a.PromiseDD, 
	a.ActualDD,
	a.OTIFQty,
	a.LineOTIFFlag,
	case when b.TotSOLines = ifnull(c.TotOTIFLines,0) then 'Y' else 'N' end HeaderOTIFFlag,
	case when b.TotSOLines = ifnull(d.TotIFLines,0) then 'Y' else 'N' end HeaderIFFlag,
	case when b.TotSOLines = ifnull(e.TotOTLines,0) then 'Y' else 'N' end HeaderOTFlag,
	case when b.TotSOLines = TotSOTANNLines then 'N' else 'Y' end OTIFCalcFlag_Merck,
	a.InFullFlag,
	a.OnTimeFlag,
	a.ExpectedGIDate,
	a.AdjSOCreatedDate
from tmp_SOLineShipDetail_001 a
	inner join tmp_SOLineDlvrCount_002 b on a.dd_salesdocno = b.dd_salesdocno
	left join tmp_SOLineDlvrCount_001 c on a.dd_salesdocno = c.dd_salesdocno
	left join tmp_SOLineIFCount_001 d on a.dd_salesdocno = d.dd_salesdocno
	left join tmp_SOLineOTCount_001 e on a.dd_salesdocno = e.dd_salesdocno;

UPDATE fact_salesorder so
SET so.ct_otifqty_merck = otf.OTIFQty
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.ct_otifqty_merck <> otf.OTIFQty;

UPDATE fact_salesorder so
SET so.dd_solineotif_merck = otf.LineOTIFFlag
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dd_solineotif_merck <> otf.LineOTIFFlag;

/*Georgiana 19 Oct 2018 Unable to get a stabe source of rows fix*/
/*UPDATE fact_salesorder so
SET so.dd_soheaderotif_merck = otf.HeaderOTIFFlag
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dd_soheaderotif_merck <> otf.HeaderOTIFFlag*/

merge into fact_salesorder so
using (select distinct so.dd_salesdocno,so.dd_salesitemno,so.dd_scheduleno,otf.HeaderOTIFFlag
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dd_soheaderotif_merck <> otf.HeaderOTIFFlag) t
on t.dd_salesdocno=so.dd_salesdocno
and t.dd_salesitemno=so.dd_salesitemno
and t.dd_scheduleno=so.dd_scheduleno
when matched then update set so.dd_soheaderotif_merck = t.HeaderOTIFFlag;

	
/*  March 9 2017 - OSTOIAN - Unable to get a stable set of rows fix */
/*
UPDATE fact_salesorder so
SET so.dd_InFullFlagHdr_Merck = otf.HeaderIFFlag
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dd_InFullFlagHdr_Merck <> otf.HeaderIFFlag
*/

merge into fact_salesorder so
using (
select so.dd_salesdocno,so.dd_salesitemno,so.dd_scheduleno,max(otf.HeaderIFFlag) as dd_InFullFlagHdr_Merck
from tmp_SOLineShipDetail_002 otf, fact_salesorder so
 WHERE so.dd_salesdocno = otf.dd_salesdocno
 and so.dd_salesitemno = otf.dd_salesitemno
 and so.dd_scheduleno = otf.dd_scheduleno
group by so.dd_salesdocno,so.dd_salesitemno,so.dd_scheduleno )x
on (
so.dd_salesdocno = x.dd_salesdocno
and so.dd_salesitemno =x.dd_salesitemno
and so.dd_scheduleno = x.dd_scheduleno
)
when matched then 
update set so.dd_InFullFlagHdr_Merck = x.dd_InFullFlagHdr_Merck;

/* End of changes March 9 2017 - OSTOIAN */
/*Georgiana 19 Oct 2018 Unable to get a stabe source of rows fix*/
/*UPDATE fact_salesorder so
SET so.dd_OnTimeFlagHdr_Merck = otf.HeaderOTFlag
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dd_OnTimeFlagHdr_Merck <> otf.HeaderOTFlag*/

merge into fact_salesorder so
using (select distinct so.dd_salesdocno,so.dd_salesitemno,so.dd_scheduleno,otf.HeaderOTFlag
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dd_OnTimeFlagHdr_Merck <> otf.HeaderOTFlag) t
on t.dd_salesdocno=so.dd_salesdocno
and t.dd_salesitemno=so.dd_salesitemno
and t.dd_scheduleno=so.dd_scheduleno
when matched then update set so.dd_OnTimeFlagHdr_Merck= t.HeaderOTFlag;

/*  March 9 2017 - OSTOIAN - Unable to get a stable set of rows fix */
/*
UPDATE fact_salesorder so
SET so.dd_InFullFlag_Merck = otf.InFullFlag
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dd_InFullFlag_Merck <> otf.InFullFlag
*/


merge into fact_salesorder so
using (
select so.dd_salesdocno,so.dd_salesitemno,so.dd_scheduleno,max(otf.InFullFlag) as dd_InFullFlag_Merck
from tmp_SOLineShipDetail_002 otf, fact_salesorder so
 WHERE so.dd_salesdocno = otf.dd_salesdocno
 and so.dd_salesitemno = otf.dd_salesitemno
 and so.dd_scheduleno = otf.dd_scheduleno
group by so.dd_salesdocno,so.dd_salesitemno,so.dd_scheduleno )x
on (
so.dd_salesdocno = x.dd_salesdocno
and so.dd_salesitemno =x.dd_salesitemno
and so.dd_scheduleno = x.dd_scheduleno
)
when matched then 
update set so.dd_InFullFlag_Merck = x.dd_InFullFlag_Merck;

/* End of changes  March 9 2017 - OSTOIAN  */

/* UPDATE fact_salesorder so
SET so.dd_OnTimeFlag_Merck = otf.OnTimeFlag
FROM tmp_SOLineShipDetail_002 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dd_OnTimeFlag_Merck <> otf.OnTimeFlag
	*/
merge into fact_salesorder so
using (
select so.dd_salesdocno,so.dd_salesitemno,so.dd_scheduleno,max(otf.OnTimeFlag) as dd_OnTimeFlag_Merck
from tmp_SOLineShipDetail_002 otf, fact_salesorder so
 WHERE so.dd_salesdocno = otf.dd_salesdocno
 and so.dd_salesitemno = otf.dd_salesitemno
 and so.dd_scheduleno = otf.dd_scheduleno
group by so.dd_salesdocno,so.dd_salesitemno,so.dd_scheduleno )x
on (
so.dd_salesdocno = x.dd_salesdocno
and so.dd_salesitemno =x.dd_salesitemno
and so.dd_scheduleno = x.dd_scheduleno
)
when matched then 
update set so.dd_OnTimeFlag_Merck = x.dd_OnTimeFlag_Merck;


DROP TABLE IF EXISTS tmp_SOLineBackOrderedQty;
CREATE TABLE tmp_SOLineBackOrderedQty AS
select dd_salesdocno, dd_salesitemno, SUM(so.ct_ConfirmedQty) ct_ConfirmedQty, SUM(so.ct_ScheduleQtySalesUnit) ct_ScheduleQtySalesUnit
from fact_salesorder so
where so.Dim_SalesOrderRejectReasonid = 1 and so.dd_ItemRelForDelv = 'X' 
group by dd_salesdocno, dd_salesitemno;

UPDATE fact_salesorder so
SET so.ct_BackorderedQty_Merck = (sobk.ct_ScheduleQtySalesUnit - sobk.ct_ConfirmedQty)
FROM tmp_SOLineBackOrderedQty sobk, fact_salesorder so
WHERE so.dd_salesdocno = sobk.dd_salesdocno
	and so.dd_salesitemno = sobk.dd_salesitemno
	and so.ct_BackorderedQty_Merck <> (sobk.ct_ScheduleQtySalesUnit - sobk.ct_ConfirmedQty);

DROP TABLE IF EXISTS tmp_SOBackOrderedQty;
CREATE TABLE tmp_SOBackOrderedQty AS
select dd_salesdocno, SUM(so.ct_ConfirmedQty) ct_ConfirmedQty, SUM(so.ct_ScheduleQtySalesUnit) ct_ScheduleQtySalesUnit
from fact_salesorder so
where so.Dim_SalesOrderRejectReasonid = 1 and so.dd_ItemRelForDelv = 'X' 
group by dd_salesdocno;

UPDATE fact_salesorder so
SET so.ct_HeaderBackorderedQty_Merck = (sobk.ct_ScheduleQtySalesUnit - sobk.ct_ConfirmedQty)
FROM tmp_SOBackOrderedQty sobk, fact_salesorder so
WHERE so.dd_salesdocno = sobk.dd_salesdocno
	and so.ct_HeaderBackorderedQty_Merck <> (sobk.ct_ScheduleQtySalesUnit - sobk.ct_ConfirmedQty);

UPDATE fact_salesorder f
SET f.dd_OTIFCalcFlag_Merck = 'N', f.ct_otifqty_merck = 0, f.dd_otifenabledflag = 'N'
FROM Dim_SalesDocumentType sdt, dim_salesorderitemcategory ic, tmp_SOLineShipDetail_002 otf, fact_salesorder f
WHERE f.Dim_SalesDocumentTypeid = sdt.Dim_SalesDocumentTypeid
	and f.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
	and sdt.documenttype = 'ZSO' 
	and ic.salesorderitemcategory = 'TANN'
	and f.dd_salesdocno = otf.dd_salesdocno
	and f.dd_salesitemno = otf.dd_salesitemno
	and f.dd_scheduleno = otf.dd_scheduleno
	and otf.OTIFCalcFlag_Merck = 'N';

DROP TABLE IF EXISTS tmp_ADJDateDim_001;
CREATE TABLE tmp_ADJDateDim_001 AS
SELECT adj.dd_salesdocno, 
	adj.dd_salesitemno, 
	adj.dd_scheduleno, 
	expdt.Dim_Dateid ExpectedGIDate,
	adjdt.Dim_Dateid AdjSOCreatedDate,
	adj.AdjDropToWH_calc AdjDropToWH
FROM tmp_SOLineAdjDt_001 adj 
	inner join Dim_Date adjdt on adjdt.DateValue = adj.AdjSOCreated_calc and adjdt.CompanyCode = adj.CompanyCode
	and adjdt.plantcode_factory = adj.plantcode_factory
	inner join Dim_Date expdt on expdt.DateValue = adj.ExpDlvrDt and expdt.CompanyCode = adj.CompanyCode
	and expdt.plantcode_factory = adj.plantcode_factory;

UPDATE fact_salesorder so
SET so.dim_DateidExpGI_Merck = otf.ExpectedGIDate
FROM tmp_ADJDateDim_001 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dim_DateidExpGI_Merck <> otf.ExpectedGIDate;
	
/* Script to update Expected GI Date for OTIF - BI-788 */	

UPDATE fact_salesorder so
SET so.dim_DateidExpGI_Merck = so.dim_dateidorginalrequestdate
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_salesorg ds,dim_distributionchannel dc, fact_salesorder so
WHERE so.dim_salesorgid = ds.dim_salesorgid 
	  and so.dim_distributionchannelid = dc.dim_distributionchannelid
	  and salesorgcode ='NL10' 
	  and distributionchannelcode = '50'
	  and so.dim_DateidExpGI_Merck <> so.dim_dateidorginalrequestdate;
	  
/* Script to update Expected GI Date for OTIF - BI-788 */		

/*Andrian - update channel 50 with Transit Calendar Days based on BI-2308*/
UPDATE fact_salesorder so
SET so.dim_DateidExpGI_Merck = edd.dim_dateid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_distributionchannel dc, dim_date dd, dim_date edd, dim_route dr, fact_salesorder so
WHERE  so.dim_distributionchannelid = dc.dim_distributionchannelid
	  and distributionchannelcode = '50'
	  and so.dim_DateidExpGI_Merck = dd.dim_dateid
	  and so.dim_routeid = dr.dim_routeid
	  and dd.datevalue-cast(dr.transittimeincalendardays as integer) = edd.datevalue
	  and dd.companycode = edd.companycode
	  and dd.plantcode_factory=edd.plantcode_factory
	  and so.dim_DateidExpGI_Merck <> 1
	  and so.dim_DateidExpGI_Merck <> edd.dim_dateid;

	
UPDATE fact_salesorder so
SET so.dim_DateidAdjSOCreated_Merck = otf.AdjSOCreatedDate
FROM tmp_ADJDateDim_001 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.dim_DateidAdjSOCreated_Merck <> otf.AdjSOCreatedDate;
	
UPDATE fact_salesorder so
SET so.Dim_DateidAdjDropToWH_Merck = otf.AdjDropToWH
FROM tmp_ADJDateDim_001 otf, fact_salesorder so
WHERE so.dd_salesdocno = otf.dd_salesdocno
	and so.dd_salesitemno = otf.dd_salesitemno
	and so.dd_scheduleno = otf.dd_scheduleno
	and so.Dim_DateidAdjDropToWH_Merck <> otf.AdjDropToWH;

/* Inventory quantity status - Past due reason code */

DROP TABLE IF EXISTS tmp_PartInventory_001;
CREATE TABLE tmp_PartInventory_001 AS
SELECT inv.dim_partid, sum(inv.ct_stockqty) OnHandQty, 
	case when inv.dim_dateidexpirydate = 1 or dded.DateValue  > 365 + current_date then 'Y' else 'N' end OverYearExpiry
FROM fact_inventoryaging inv
	INNER JOIN dim_date dded on dded.dim_dateid = inv.dim_dateidexpirydate
WHERE inv.ct_stockqty > 0 -- and inv.dim_dateidexpirydate <> 1
GROUP BY inv.dim_partid, case when inv.dim_dateidexpirydate = 1 or dded.DateValue  > 365 + current_date then 'Y' else 'N' end;

DROP TABLE IF EXISTS tmp_SOBackOrdered_001;
CREATE TABLE tmp_SOBackOrdered_001 AS
SELECT fact_salesorderid, db.DeliveryBlock
FROM fact_salesorder f_so 
	inner join dim_deliveryblock db on db.dim_deliveryblockid = f_so.dim_deliveryblockid
	inner join dim_salesorderitemstatus sois on sois.dim_salesorderitemstatusid = f_so.dim_salesorderitemstatusid
	inner join dim_documentcategory dc on dc.dim_documentcategoryid = f_so.dim_documentcategoryid
WHERE (dc.DocumentCategory ='C' OR dc.DocumentCategory= 'I')  
	AND sois.DeliveryStatus <> 'Completely processed' 
	AND f_so.Dim_SalesOrderRejectReasonid = 1
	AND (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) > 0;
/*	AND ( db.DeliveryBlock = 'ZD' OR f_so.Dim_Deliveryblockid = 1)*/
UPDATE fact_salesorder f
SET dd_InvQtyStatus_Merck = 'Not Set';

UPDATE fact_salesorder f
SET dd_InvQtyStatus_Merck = 'Backordered'
FROM tmp_SOBackOrdered_001 bo,fact_salesorder f
WHERE bo.fact_salesorderid = f.fact_salesorderid
AND ( bo.DeliveryBlock = 'ZD' OR f.Dim_Deliveryblockid = 1);

UPDATE fact_salesorder f
SET dd_InvQtyStatus_Merck = 'Allocated'
FROM tmp_PartInventory_001 inv, tmp_SOBackOrdered_001 bo,  fact_salesorder f
WHERE f.dim_partid = inv.dim_partid and inv.OverYearExpiry = 'Y'
	and bo.fact_salesorderid = f.fact_salesorderid
	and f.ct_OnHandCoveredQty > 0
	AND ( bo.DeliveryBlock = 'ZD' OR f.Dim_Deliveryblockid = 1);

UPDATE fact_salesorder f
SET dd_InvQtyStatus_Merck = 'Short Dated'
FROM tmp_PartInventory_001 inv, tmp_SOBackOrdered_001 bo, fact_salesorder f
WHERE f.dim_partid = inv.dim_partid and OverYearExpiry = 'N'
	and bo.fact_salesorderid = f.fact_salesorderid
	and f.ct_OnHandCoveredQty > 0
	AND ( bo.DeliveryBlock = 'ZD' OR f.Dim_Deliveryblockid = 1);
	
/* if dim_deliveryblockid = '01' or 'Z2' then Past Due Reason = Credit Issue	*/
UPDATE fact_salesorder f
SET dd_InvQtyStatus_Merck = 'Credit Issue'
FROM tmp_SOBackOrdered_001 bo , fact_salesorder f
WHERE bo.fact_salesorderid = f.fact_salesorderid
	AND (bo.DeliveryBlock in ('01','Z2'));
		
		
/* If dim_deliveryblockid is Filled in and not equal to ZD, 01 or Z2 then Past Due Reason = Delivery Block*/
UPDATE fact_salesorder f
SET dd_InvQtyStatus_Merck = 'Delivery Block'
FROM tmp_SOBackOrdered_001 bo,fact_salesorder f
WHERE bo.fact_salesorderid = f.fact_salesorderid
	AND ( bo.DeliveryBlock <>'Not Set' AND bo.DeliveryBlock NOT IN ('ZD','01','Z2'));

/*****/

/*** update dim_customerslaid_merck ***/

update fact_salesorder
set dim_customersla_merckid = 1;

UPDATE fact_salesorder so
SET so.dim_customersla_merckid = cst.dim_customersla_merckid
FROM dim_customersla_merck cst, Dim_SalesOrg sorg, Dim_CustomerGroup cg, dim_Customer c, fact_salesorder so
WHERE so.Dim_SalesOrgid = sorg.Dim_SalesOrgid
	and so.Dim_CustomerGroupId = cg.Dim_CustomerGroupId
	and so.Dim_CustomeridShipTo = c.dim_Customerid
	and cst.salesorg = sorg.SalesOrgCode
	and cst.shiptocountry = 'Not Set'
	and cst.customergrp = 'Not Set'
	and cst.shiptolocation = 'Not Set'
	and so.dim_customersla_merckid <> cst.dim_customersla_merckid;

UPDATE fact_salesorder so
SET so.dim_customersla_merckid = cst.dim_customersla_merckid
FROM dim_customersla_merck cst, Dim_SalesOrg sorg, Dim_CustomerGroup cg, dim_Customer c, fact_salesorder so
WHERE so.Dim_SalesOrgid = sorg.Dim_SalesOrgid
	and so.Dim_CustomerGroupId = cg.Dim_CustomerGroupId
	and so.Dim_CustomeridShipTo = c.dim_Customerid
	and cst.salesorg = sorg.SalesOrgCode
	and cst.shiptocountry = c.Country
	and cst.customergrp = 'Not Set'
	and cst.shiptolocation = 'Not Set'
	and so.dim_customersla_merckid <> cst.dim_customersla_merckid;
	

UPDATE fact_salesorder so
SET so.dim_customersla_merckid = cst.dim_customersla_merckid
FROM dim_customersla_merck cst, Dim_SalesOrg sorg, Dim_CustomerGroup cg, dim_Customer c, fact_salesorder so
WHERE so.Dim_SalesOrgid = sorg.Dim_SalesOrgid
	and so.Dim_CustomerGroupId = cg.Dim_CustomerGroupId
	and so.Dim_CustomeridShipTo = c.dim_Customerid
	and cst.salesorg = sorg.SalesOrgCode
	and cst.shiptocountry = c.Country
	and cst.customergrp = cg.CustomerGroup
	and cst.shiptolocation = 'Not Set'
	and so.dim_customersla_merckid <> cst.dim_customersla_merckid;
	
/*Begin Andrian-based on BI-1575*/	
MERGE INTO fact_salesorder so
USING (SELECT DISTINCT so.fact_salesorderid, FIRST_VALUE(cst.dim_customersla_merckid) OVER (PARTITION BY so.fact_salesorderid ORDER BY '') AS dim_customersla_merckid
FROM dim_customersla_merck cst, Dim_SalesOrg sorg, Dim_CustomerGroup cg, dim_Customer c, fact_salesorder so
WHERE so.Dim_SalesOrgid = sorg.Dim_SalesOrgid
	and so.Dim_CustomerGroupId = cg.Dim_CustomerGroupId
	and so.Dim_CustomeridShipTo = c.dim_Customerid
	and cst.salesorg = sorg.SalesOrgCode
	and cst.shiptocountry = c.Country
	and cst.customergrp = 'Not Set'
	and cst.shiptolocation = TRIM(LEADING '0' FROM c.CustomerNumber)
	and so.dim_customersla_merckid <> cst.dim_customersla_merckid
) src
ON so.fact_salesorderid = src.fact_salesorderid
WHEN MATCHED THEN UPDATE
SET so.dim_customersla_merckid = src.dim_customersla_merckid;	
/*End Andrian-based on BI-1575*/		

MERGE INTO fact_salesorder so
USING (SELECT DISTINCT so.fact_salesorderid, FIRST_VALUE(cst.dim_customersla_merckid) OVER (PARTITION BY so.fact_salesorderid ORDER BY '') AS dim_customersla_merckid
FROM dim_customersla_merck cst, Dim_SalesOrg sorg, Dim_CustomerGroup cg, dim_Customer c, fact_salesorder so
WHERE so.Dim_SalesOrgid = sorg.Dim_SalesOrgid
	and so.Dim_CustomerGroupId = cg.Dim_CustomerGroupId
	and so.Dim_CustomeridShipTo = c.dim_Customerid
	and cst.salesorg = sorg.SalesOrgCode
	and cst.shiptocountry = c.Country
	and cst.customergrp = cg.CustomerGroup
	and cst.shiptolocation = TRIM(LEADING '0' FROM c.CustomerNumber)
	and so.dim_customersla_merckid <> cst.dim_customersla_merckid) src
ON so.fact_salesorderid = src.fact_salesorderid
WHEN MATCHED THEN UPDATE
SET so.dim_customersla_merckid = src.dim_customersla_merckid;

/*** end update dim_customerslaid_merck ***/

update fact_salesorder
set dim_dateidlatestactualgi_merck = 1
where dim_dateidlatestactualgi_merck is null;

update fact_salesorder
set dim_plantidordering_merck = 1
where dim_plantidordering_merck is null;

/* OTIF reason code and desc */
UPDATE fact_salesorder so
SET dd_reasoncode = ifnull(VBAP_ZZRSC, 'Not Set')
FROM vbak_vbap_vbep vbk, fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
and dd_reasoncode <> ifnull(VBAP_ZZRSC, 'Not Set');

update fact_salesorder f
set dd_reasondesc = ifnull(r.reason,'Not Set')
from 
dim_salesorg sorg,
dim_distributionchannel dch,
Z1SD_SR_REASON r, fact_salesorder f
where 
 f.dim_salesorgid = sorg.dim_salesorgid
 and f.dim_distributionchannelid = dch.dim_distributionchannelid
 and sorg.SalesOrgCode = r.VKORG
 and dch.DistributionChannelCode = r.VTWEG
 and f.dd_reasoncode = r.zzrsc
 and dd_reasondesc <> ifnull(r.reason,'Not Set');


DROP TABLE IF EXISTS tmp_SOBackOrderedQty;
DROP TABLE IF EXISTS tmp_SOLineBackOrderedQty;
DROP TABLE IF EXISTS tmp_SOLineDlvrCount_001;
DROP TABLE IF EXISTS tmp_SOLineDlvrCount_002;
DROP TABLE IF EXISTS tmp_SOLineShipDetail_001;
DROP TABLE IF EXISTS tmp_SOLineShipDetail_002;
DROP TABLE IF EXISTS tmp_SOCustomerSLA_001;
DROP TABLE IF EXISTS tmp_SOLineAdjDt_001;
DROP TABLE IF EXISTS tmp_ADJDateDim_001;
DROP TABLE IF EXISTS tmp_PartInventory_001;

