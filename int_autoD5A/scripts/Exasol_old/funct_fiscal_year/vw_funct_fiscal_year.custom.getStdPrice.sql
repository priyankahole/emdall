/*********************************************Change History*******************************************************/
/* Date             By              Version           Desc                                                        */
/* 03 Mar 2015      Liviu Ionescu                     Add Combine for tmp_funct_fiscal_year                       */
/******************************************************************************************************************/
/* Custom proc starts here */

/* Call this between vw_getstdprice_part1 and vw_getstdprice_part2 */
/* ( for another proc/function, create a similar custom proc and run that before or in the middle of the actual proc. */
/* If it has to be used in the middle, e.g if its not at all possible to pre-populate before the actual proc, */
/* then call this in-line in the other proc) */


      
DELETE FROM tmp_funct_fiscal_year
where fact_script_name = 'getstdprice';

INSERT INTO tmp_funct_fiscal_year
(pCompanyCode,FiscalYear,Period,fact_script_name)
SELECT DISTINCT t.pCompanyCode,  t.pprevFiYear, t.pprevPeriod ,'getstdprice'
FROM tmp_getStdPrice t
WHERE PONumber is null AND StandardPrice <= 0;

/* LI Changes 03 Mar 2015 */
call vectorwise(combine 'tmp_funct_fiscal_year');

			
