/*********************************************Change History*******************************************************/
/* Date             By              Version           Desc                                                        */
/* 03 Mar 2015      Liviu Ionescu                     Add Combine for tmp_funct_fiscal_year */
/******************************************************************************************************************/

/* Custom proc starts here */

/* Call this between vw_getstdprice_part1 and vw_getstdprice_part2 */
/* ( for another proc/function, create a similar custom proc and run that before or in the middle of the actual proc. */
/* If it has to be used in the middle, e.g if its not at all possible to pre-populate before the actual proc, */
/* then call this in-line in the other proc) */


      
DROP TABLE IF EXISTS tmp_funct_fiscal_year;
CREATE TABLE tmp_funct_fiscal_year
(
pCompanyCode varchar(18), 
FiscalYear 		Int, 
Period 			Int,
pFromDate date,
pToDate date,
pToDate1 date,
CalDate date,
pCalYear  Int,
pCalMth   Int,
pCalMthDay  Int,
pVariant1 varchar(4),
pVariant2 varchar(4),
pReturn varchar(45),
pYearShift varchar(4),
pYearShiftCnt Int,
pTempDate1 date,
pTempDate2 date,
pPrevFIYEAR Int,
pPrevPeriod Int,
pPeriv varchar(2),
upd_flag varchar(3)

);

/* Populate tmp_funct_fiscal_year */

/* mysql usage 
	funct_Fiscal_Year(pCompanyCode,
	pprevFiYear,
	pprevPeriod)
*/							


INSERT INTO tmp_funct_fiscal_year
(pCompanyCode,FiscalYear,Period)
SELECT  t.pCompanyCode,  t.pprevFiYear, t.pprevPeriod 
FROM tmp_getStdPrice t
WHERE PONumber is null AND StandardPrice <= 0;


			
