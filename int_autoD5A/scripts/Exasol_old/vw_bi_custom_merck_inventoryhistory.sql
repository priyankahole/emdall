/*****************************************************************************************************
Creation Date : Sept 18, 12013
Created By : Hiten Suthar
*****************************************************************************************************/

DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

UPDATE fact_inventoryhistory f
SET dim_Currencyid_GBL = ifnull(c.dim_Currencyid, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory f CROSS JOIN tmp_GlobalCurr_001 gbl inner join dim_currency c on gbl.pGlobalCurrency = c.CurrencyCode
WHERE dim_Currencyid_GBL is null or dim_Currencyid_GBL <> ifnull(c.dim_Currencyid, 1);

DROP TABLE IF EXISTS tmp_GlobalCurr_001;