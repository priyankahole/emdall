/* Populate Billing block Count */


DROP TABLE IF EXISTS tmp_SOMinItemSchedule_billingblockcnt;

CREATE TABLE tmp_SOMinItemSchedule_billingblockcnt AS
SELECT dd_SalesdocNo,dd_SalesItemNo,dd_scheduleno,RANK() OVER(PARTITION BY so.dd_SalesDocNo ORDER BY so.dd_SalesItemNo ASC) AS  RankItem,
  RANK() OVER (PARTITION BY so.dd_SalesDocNo,so.dd_SalesItemNo ORDER BY so.dd_ScheduleNo ASC) as RankSchedule 
FROM fact_salesorder so
WHERE so.dim_billingblockid <> 1;

DELETE FROM tmp_SOMinItemSchedule_billingblockcnt WHERE RankItem <> 1 OR RankSchedule <> 1;

UPDATE fact_salesorder so
SET so.ct_BillingBlockCnt_Merck  = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_SOMinItemSchedule_billingblockcnt t, fact_salesorder so
WHERE so.dd_SalesdocNo = t.dd_SalesdocNo
AND so.dd_SalesItemNo = t.dd_SalesItemNo
AND so.dd_ScheduleNo = t.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_SOMinItemSchedule_billingblockcnt;



/* Populate Credit Block Count */


DROP TABLE IF EXISTS tmp_SOMinItemSchedule_creditblockcnt;

CREATE TABLE tmp_SOMinItemSchedule_creditblockcnt AS
SELECT dd_SalesdocNo,dd_SalesItemNo,dd_scheduleno,RANK() OVER(PARTITION BY so.dd_SalesDocNo ORDER BY so.dd_SalesItemNo ASC) AS  RankItem,
  RANK() OVER (PARTITION BY so.dd_SalesDocNo,so.dd_SalesItemNo ORDER BY so.dd_ScheduleNo ASC) as RankSchedule 
FROM fact_salesorder so, dim_overallstatusforcreditcheck scc
WHERE so.Dim_OverallStatusCreditCheckId = scc.dim_overallstatusforcreditcheckid
AND scc.OverAllStatusForCreditCheck = 'B';

DELETE FROM tmp_SOMinItemSchedule_creditblockcnt WHERE RankItem <> 1 AND RankSchedule <> 1;

UPDATE fact_salesorder so
SET so.ct_CreditBlockCnt_Merck  = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_SOMinItemSchedule_creditblockcnt t, fact_salesorder so
WHERE so.dd_SalesdocNo = t.dd_SalesdocNo
AND so.dd_SalesItemNo = t.dd_SalesItemNo
AND so.dd_ScheduleNo = t.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_SOMinItemSchedule_creditblockcnt;


/* Populate Delivery block Count */


DROP TABLE IF EXISTS tmp_SOMinItemSchedule_deliveryblockcnt;

CREATE TABLE tmp_SOMinItemSchedule_deliveryblockcnt AS
SELECT dd_SalesdocNo,dd_SalesItemNo,dd_scheduleno,RANK() OVER(PARTITION BY so.dd_SalesDocNo ORDER BY so.dd_SalesItemNo ASC) AS  RankItem,
  RANK() OVER (PARTITION BY so.dd_SalesDocNo,so.dd_SalesItemNo ORDER BY so.dd_ScheduleNo ASC) as RankSchedule 
FROM fact_salesorder so
WHERE so.dim_deliveryblockid <> 1;

DELETE FROM tmp_SOMinItemSchedule_deliveryblockcnt WHERE RankItem <> 1 OR RankSchedule <> 1;

UPDATE fact_salesorder so
SET so.ct_DeliveryBlockCnt_Merck  = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM tmp_SOMinItemSchedule_deliveryblockcnt t, fact_salesorder so
WHERE so.dd_SalesdocNo = t.dd_SalesdocNo
AND so.dd_SalesItemNo = t.dd_SalesItemNo
AND so.dd_ScheduleNo = t.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_SOMinItemSchedule_deliveryblockcnt;

/* 23.03.2015 new Base Uom fields */
UPDATE fact_salesorder so
SET so.ct_ScheduleQtyBaseUoM  =
  ifnull(vbep_wmeng * (convert(decimal (18,2),VBEP_UMVKZ) / convert(decimal (18,2),VBEP_UMVKN)), 0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk, fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
and so.ct_ScheduleQtyBaseUoM  <> ifnull(vbep_wmeng * (convert(decimal (18,2),VBEP_UMVKZ) / convert(decimal (18,2),VBEP_UMVKN)), 0);

UPDATE fact_salesorder so
SET so.ct_ShippedAgnstOrderQtyBaseUoM
 =  ifnull(ct_ShippedAgnstOrderQty *  (convert(decimal(18,2),VBEP_UMVKZ) / convert(decimal(18,2),VBEP_UMVKN)), 0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk,fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
and so.ct_ShippedAgnstOrderQtyBaseUoM <>  ifnull(ct_ShippedAgnstOrderQty *  (convert(decimal(18,2),VBEP_UMVKZ) / convert(decimal(18,2),VBEP_UMVKN)), 0);

UPDATE fact_salesorder so
SET so.ct_CmlQtyReceivedBaseUoM  =  ifnull(ct_CmlQtyReceived *  (convert(decimal(18,2),VBEP_UMVKZ) / convert(decimal(18,2),VBEP_UMVKN)), 0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk,fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
and so.ct_CmlQtyReceivedBaseUoM  <>  ifnull(ct_CmlQtyReceived * (convert(decimal(18,2),VBEP_UMVKZ) / convert(decimal(18,2),VBEP_UMVKN)), 0);

UPDATE fact_salesorder so
SET so.ct_ConfirmedQtyBaseUoM  =  ifnull(vbep_bmeng * (convert(decimal(18,2),VBEP_UMVKZ) / convert(decimal(18,2),VBEP_UMVKN)), 0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk,fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
and so.ct_ConfirmedQtyBaseUoM  <> ifnull(vbep_bmeng * (convert(decimal(18,2),VBEP_UMVKZ) / convert(decimal(18,2),VBEP_UMVKN)), 0);
/* end 23.03.2015 new Base Uom fields */

update fact_salesorder f
set  ct_ScheduleQtyBaseUoMIRU = ifnull(f.ct_ScheduleQtyBaseUoM ,0)/marm_umrez 
,ct_ShippedAgnstOrderQtyBaseUoMIRU  = ifnull(f.ct_ShippedAgnstOrderQtyBaseUoM,0)/marm_umrez 
,ct_CmlQtyReceivedBaseUoMIRU = ifnull(f.ct_CmlQtyReceivedBaseUoM,0)/marm_umrez 
,ct_ConfirmedQtyBaseUoMIRU = ifnull(f.ct_ConfirmedQtyBaseUoM,0)/marm_umrez 
,ct_ShippedOrderQtyBaseUoMIRU  = ifnull(f.ct_ShippedOrderQtyBaseUoM,0)/marm_umrez 
from dim_part dp, MARM m, fact_salesorder f
WHERE f.dim_partid = dp.dim_partid
	and m.marm_matnr =dp.partnumber
	and marm_meinh = 'IRU';

/*Andrian-Based on ticket BI-1546*/	
drop table if exists tmp01_reference;
create table tmp01_reference as
select distinct vbak_vbeln,vbak_ihrez from VBAK_VBAP_VBEP
where VBAK_IHREZ is not null;

update fact_salesorder a 
set a.dd_ourreference = ifnull(b.vbak_ihrez,'Not Set')
from tmp01_reference b, fact_salesorder a 
where a.dd_salesdocno = b.vbak_vbeln
and a.dd_ourreference <> ifnull(b.vbak_ihrez,'Not Set');	
	

/*Georgiana 17 Feb 2015 Every Angle Addons*/

/*KONV columns*/

/*First updating the currencyid forn KONV*/

drop table if exists tmp_for_updcurrencysales;
create table tmp_for_updcurrencysales as
select distinct min(cur.dim_currencyid) as dim_currencyid,VBAK_VBELN,VBAP_POSNR,VBEP_ETENR
from fact_salesorder so,
        VBAK_VBAP_VBEP vbk,
        Dim_Plant pl,
        Dim_Company co,
        Dim_Currency cur,
        KONV_SALES_PURCHASE_tmp k
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo    
AND cur.CurrencyCode  =  KONV_WAERS
AND vbk.VBAK_KNUMV = ifnull(k.KONV_KNUMV, 'Not Set')
AND vbk.VBAP_POSNR = ifnull(k.KONV_KPOSN, '0')
group by VBAK_VBELN,VBAP_POSNR,VBEP_ETENR;


update fact_salesorder so
SET so.Dim_Currencyid_Konv = u.dim_currencyid
from tmp_for_updcurrencysales u, fact_salesorder so
where 
u.VBAK_VBELN = so.dd_SalesDocNo
AND u.VBAP_POSNR = so.dd_SalesItemNo
AND u.VBEP_ETENR = so.dd_ScheduleNo
AND so.Dim_Currencyid_Konv <> u.dim_currencyid;

drop table if exists tmp_for_updcurrencysales;

DROP TABLE IF EXISTS TMP_col_KONV;
CREATE TABLE TMP_col_KONV
AS
SELECT documentconditionnumber,conditionitemnumber,conditiontypecode,AVG(conditionrate) avg_baseprice, sum(ConditionValue) sum_conditionvalue
FROM dim_transactionpricecondition
WHERE conditiontypecode IN ('PN00','PR00','ZCMX','ZTRS', 'NETP', 'PB00','PBXX') 
and sourceflag ='SALES'
GROUP BY  documentconditionnumber,conditionitemnumber,conditiontypecode;

UPDATE fact_salesorder so
SET so.amt_grossprice = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONV k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontypecode = 'PB00'
AND so.amt_grossprice <> k.avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_grossprice = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONV k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontypecode = 'PBXX'
AND so.amt_grossprice <> k.avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_netprice = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONV k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontypecode = 'PN00'
AND so.amt_netprice <> avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_price = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONV k,  fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontypecode = 'PR00'
AND so.amt_price <> avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_freightcharged = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONV k,fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontypecode = 'ZTRS'
AND so.amt_freightcharged <> avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_conditionprice = k.sum_conditionvalue
FROM vbak_vbap_vbep vbk, TMP_col_KONV k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontypecode = 'PR00'
AND so.amt_conditionprice <> sum_conditionvalue;

UPDATE fact_salesorder so
SET so.amt_netpricepicking = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONV k,fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontypecode = 'NETP'
AND so.amt_netpricepicking <> avg_baseprice;

/*KONP columns*/

DROP TABLE IF EXISTS TMP_col_KONP;
CREATE TABLE TMP_col_KONP
AS
SELECT documentconditionnumber,conditionitemnumber,conditiontype_konp,conditionunit,AVG(conditionrate_konp) avg_baseprice,avg(conditionpriceunit) avg_conditionpriceunit
FROM dim_transactionpricecondition
WHERE conditiontype_konp IN ('PN00','PR00','ZCMX','ZTRS','PB00','PI01','ZFR1','ZFR2','ZFR3','ZFR4') 
and sourceflag ='SALES'
GROUP BY documentconditionnumber,conditionitemnumber,conditiontype_konp,conditionunit;

/*Georgiana 25 Aug 2016 Ambiguous Replace fix*/
merge into fact_salesorder so
using (select distinct so.fact_salesorderid, k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'PR00'
AND so.amt_grossprice <> k.avg_baseprice ) t
on t.fact_salesorderid=so.fact_salesorderid
when matched then update set so.amt_grossprice = t.avg_baseprice 
where so.amt_grossprice <> t.avg_baseprice ;


UPDATE fact_salesorder so
SET so.amt_IntercompanyPrice = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'PI01'
AND so.amt_IntercompanyPrice <> k.avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_stdFrghtandInsurnceprice = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'ZFR1'
AND so.amt_stdFrghtandInsurnceprice <> k.avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_stdQualityCntrlprice = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'ZFR2'
AND so.amt_stdQualityCntrlprice <> k.avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_stdCustomDutiesprice = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'ZFR3'
AND so.amt_stdCustomDutiesprice <> k.avg_baseprice;

UPDATE fact_salesorder so
SET so.amt_stdOthrLandedCost = k.avg_baseprice
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'ZFR4'
AND so.amt_stdOthrLandedCost <> k.avg_baseprice;


UPDATE fact_salesorder so
SET so.dd_conditionunitforgrossprice = ifnull(k.conditionunit,'Not Set')
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'PB00'
AND so.dd_conditionunitforgrossprice <> ifnull(k.conditionunit,'Not Set');

UPDATE fact_salesorder so
SET so.dd_conditionunitforintercompanyprice = ifnull(k.conditionunit,'Not Set')
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'PI01'
AND so.dd_conditionunitforintercompanyprice <> ifnull(k.conditionunit,'Not Set');

UPDATE fact_salesorder so
SET so.amt_conditiongrossprice = k.avg_conditionpriceunit
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'PB00'
AND so.amt_conditiongrossprice <> k.avg_conditionpriceunit;

UPDATE fact_salesorder so
SET so.amt_conditionIntercompanyPrice = k.avg_conditionpriceunit
FROM vbak_vbap_vbep vbk, TMP_col_KONP k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'PI01'
AND so.amt_conditionIntercompanyPrice <> k.avg_conditionpriceunit;

/*Folowing tables are used to check if there are two currencies for the same combination of KONV_KNUMV,KONV_KPOSN*/

drop table if exists tmp_tmp_konh1;
create table tmp_tmp_konh1 as
select KONV_KNUMV,KONV_KPOSN, GROUP_CONCAT (KONP_KONWA order by KONP_KONWA) as rateunit
from KONV_SALES_PURCHASE_tmp k, KONH_KONP p
where k.konv_knumh = p.konh_knumh
and k.konv_kopos = p.konp_kopos
GROUP BY  KONV_KNUMV,KONV_KPOSN
order by KONV_KNUMV,KONV_KPOSN;

drop table if exists tmp_for_upd_rateunit;
create table tmp_for_upd_rateunit as
select * from tmp_tmp_konh1
where left(rateunit,3) <> right(rateunit,3);

update fact_salesorder so
SET so.dd_rateunit = ifnull(k.rateunit,'Not Set')
FROM vbak_vbap_vbep vbk, dim_transactionpricecondition k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.documentconditionnumber in (select KONV_KNUMV  from tmp_for_upd_rateunit)
AND k.conditionitemnumber in (select KONV_KPOSN from tmp_for_upd_rateunit)
AND k.conditiontype_konp = 'PB00';

update fact_salesorder so
SET so.dd_rateunit = 'USD'
FROM vbak_vbap_vbep vbk, dim_transactionpricecondition k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.documentconditionnumber not in (select KONV_KNUMV  from tmp_for_upd_rateunit)
AND k.conditionitemnumber not in (select KONV_KPOSN from tmp_for_upd_rateunit)
AND k.conditiontype_konp = 'PB00';

update fact_salesorder so
SET so.dd_conditiontypecode_konp = ifnull(k.conditiontype_konp,'Not Set')
FROM vbak_vbap_vbep vbk, dim_transactionpricecondition k, fact_salesorder so
WHERE vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBAK_KNUMV = k.documentconditionnumber
AND vbk.VBAP_POSNR = k.conditionitemnumber
AND k.conditiontype_konp = 'PB00'
AND so.dd_conditiontypecode_konp <> ifnull(k.conditiontype_konp,'Not Set');

/* Andrei APP 7424 */
merge into fact_batchmaster b
using ( select distinct b.fact_batchmasterid,
                       max(f.dim_dateidactualgi) as dim_dateidactualgi
from fact_batchmaster b, fact_salesorder f
where f.dim_partid = b.dim_materialmasterid
and f.dim_plantid = b.dim_plantmasterid
group by b.fact_batchmasterid) x
on x.fact_batchmasterid = b.fact_batchmasterid
when matched then update set b.dim_dateidactualgi = x.dim_dateidactualgi;

merge into fact_batchmaster b
using ( select distinct b.fact_batchmasterid,
                       max(f.dim_dateidgoodsissue) as dim_dateidgoodsissue
from fact_batchmaster b, fact_salesorder f
where f.dim_partid = b.dim_materialmasterid
and f.dim_plantid = b.dim_plantmasterid
group by b.fact_batchmasterid) x
on x.fact_batchmasterid = b.fact_batchmasterid
when matched then update set b.dim_dateidgoodsissue = x.dim_dateidgoodsissue;

merge into fact_batchmaster b
using ( select distinct b.fact_batchmasterid,
                       max(f.dim_dateidexpirydate) as dim_dateidexpirydate
from fact_batchmaster b, fact_salesorder f
where f.dim_partid = b.dim_materialmasterid
and f.dim_plantid = b.dim_plantmasterid
and f.dim_batchid = b.dim_batchmasterid
group by b.fact_batchmasterid) x
on x.fact_batchmasterid = b.fact_batchmasterid
when matched then update set b.dim_dateidexpirydate = x.dim_dateidexpirydate;