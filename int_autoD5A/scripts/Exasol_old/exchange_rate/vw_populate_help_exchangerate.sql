
/**************************************************************************************************************/
/*   Script         : vw_populate_help_exchangerate.sql   */
/*   Author         : Lokesh */
/*   Created On     : 16 Dec 2015 */
/*   Description    : Script to populate standard help_exchangerate table */
/*			This will populate data in help_exchangerate table for all currency pairs in TCURR_ALLTYPES for all currency types
			for date range 1-Jan-2005 to 31-Dec of next year */
/*   Last Update    : 16 Dec 2015 */
/*********************************************Change History*******************************************************/
/*   Date             By            	Version           Desc                                                        */
/*   21 Dec 2015      Lokesh Kamani	2.0		  Replaced tcurr/tcurv/tcurf with _alltypes. */
/*   16 Dec 2015      Lokesh Kamani	1.0               Initial Version 						  */
/******************************************************************************************************************/

DROP TABLE IF EXISTS tmp_populate_help_exchangerate;
CREATE TABLE tmp_populate_help_exchangerate
LIKE tmp_getexchangerate1 INCLUDING DEFAULTS INCLUDING IDENTITY;

/* Store from and to currency. Also store reverse of them in case that pair does not exist. 
e.g If INR-USD exists, but USD-INR does not exist in TCURR_ALLTYPES, add USD-INR. 
Only populate the dates between 1 Jan 2005 and last day of next year */

DROP TABLE IF EXISTS tmp_dim_date_helpexchgrate;
CREATE TABLE tmp_dim_date_helpexchgrate
as
select distinct datevalue 
from dim_date d
where d.datevalue >= TO_DATE('01-01-2000','DD-MM-YYYY') AND year(d.datevalue) <= year(current_date) + 5;

/*Split this into multiple queries for performance */
INSERT INTO tmp_populate_help_exchangerate ( pFromCurrency, pDate, pToCurrency,vtype  )
SELECT DISTINCT tcurr_fcurr,d.datevalue pDate,tcurr_tcurr pToCurrency,tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2000','DD-MM-YYYY') AND year(d.datevalue) <= 2002
UNION
SELECT DISTINCT tcurr_tcurr,d.datevalue pDate,tcurr_fcurr pToCurrency, tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2000','DD-MM-YYYY') AND year(d.datevalue) <= 2002;

INSERT INTO tmp_populate_help_exchangerate ( pFromCurrency, pDate, pToCurrency,vtype  )
SELECT DISTINCT tcurr_fcurr,d.datevalue pDate,tcurr_tcurr pToCurrency,tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2003','DD-MM-YYYY') AND year(d.datevalue) <= 2004
UNION
SELECT DISTINCT tcurr_tcurr,d.datevalue pDate,tcurr_fcurr pToCurrency, tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2003','DD-MM-YYYY') AND year(d.datevalue) <= 2004;

INSERT INTO tmp_populate_help_exchangerate ( pFromCurrency, pDate, pToCurrency,vtype  )
SELECT DISTINCT tcurr_fcurr,d.datevalue pDate,tcurr_tcurr pToCurrency,tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2005','DD-MM-YYYY') AND year(d.datevalue) <= 2008
UNION
SELECT DISTINCT tcurr_tcurr,d.datevalue pDate,tcurr_fcurr pToCurrency, tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2005','DD-MM-YYYY') AND year(d.datevalue) <= 2008;

INSERT INTO tmp_populate_help_exchangerate ( pFromCurrency, pDate, pToCurrency,vtype  )
SELECT DISTINCT tcurr_fcurr,d.datevalue pDate,tcurr_tcurr pToCurrency,tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2009','DD-MM-YYYY') AND year(d.datevalue) <= 2012
UNION
SELECT DISTINCT tcurr_tcurr,d.datevalue pDate,tcurr_fcurr pToCurrency, tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2009','DD-MM-YYYY') AND year(d.datevalue) <= 2012;

INSERT INTO tmp_populate_help_exchangerate ( pFromCurrency, pDate, pToCurrency,vtype  )
SELECT DISTINCT tcurr_fcurr,d.datevalue pDate,tcurr_tcurr pToCurrency,tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2013','DD-MM-YYYY') AND year(d.datevalue) <= 2015
UNION
SELECT DISTINCT tcurr_tcurr,d.datevalue pDate,tcurr_fcurr pToCurrency, tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2013','DD-MM-YYYY') AND year(d.datevalue) <= 2015;

INSERT INTO tmp_populate_help_exchangerate ( pFromCurrency, pDate, pToCurrency,vtype  )
SELECT DISTINCT tcurr_fcurr,d.datevalue pDate,tcurr_tcurr pToCurrency,tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2016','DD-MM-YYYY') AND year(d.datevalue) <= 2018
UNION
SELECT DISTINCT tcurr_tcurr,d.datevalue pDate,tcurr_fcurr pToCurrency, tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2016','DD-MM-YYYY') AND year(d.datevalue) <= 2018;

INSERT INTO tmp_populate_help_exchangerate ( pFromCurrency, pDate, pToCurrency,vtype  )
SELECT DISTINCT tcurr_fcurr,d.datevalue pDate,tcurr_tcurr pToCurrency,tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2019','DD-MM-YYYY') AND year(d.datevalue) <= 2021
UNION
SELECT DISTINCT tcurr_tcurr,d.datevalue pDate,tcurr_fcurr pToCurrency, tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2019','DD-MM-YYYY') AND year(d.datevalue) <= 2021;

INSERT INTO tmp_populate_help_exchangerate ( pFromCurrency, pDate, pToCurrency,vtype  )
SELECT DISTINCT tcurr_fcurr,d.datevalue pDate,tcurr_tcurr pToCurrency,tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2022','DD-MM-YYYY') AND year(d.datevalue) <= 2024
UNION
SELECT DISTINCT tcurr_tcurr,d.datevalue pDate,tcurr_fcurr pToCurrency, tcurr_kurst vtype
FROM tmp_dim_date_helpexchgrate d, TCURR_ALLTYPES
WHERE d.datevalue >= TO_DATE('01-01-2022','DD-MM-YYYY') AND year(d.datevalue) <= 2024;

UPDATE tmp_populate_help_exchangerate
set vReverse = 'N',
	updflag = 'N',
	processed_flag = 'Y',
	vFromCurr = pFromCurrency,
	vToCurr = pToCurrency,
	vFTfact = 0,
	vffact = 0,
	vtfact = 0
WHERE processed_flag IS NULL;      

UPDATE tmp_populate_help_exchangerate
SET exchangeRate = 0
WHERE exchangeRate is null
	AND processed_flag = 'Y' 
	AND updflag = 'N';    

UPDATE tmp_populate_help_exchangerate
SET pFromExchangeRate = 0
WHERE pFromExchangeRate is null
	AND processed_flag = 'Y' 
	AND updflag = 'N';    

UPDATE tmp_populate_help_exchangerate
SET exchangeRate = 1,
	updflag = 'Y'
WHERE pFromCurrency = pToCurrency
	AND processed_flag = 'Y' 
	AND updflag = 'N';


UPDATE tmp_populate_help_exchangerate
SET vReverse = 'Y',
	pFromExchangeRate = -1 * pFromExchangeRate,
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND pFromExchangeRate < 0
	AND processed_flag = 'Y';


UPDATE tmp_populate_help_exchangerate x
SET vReverse = 'Y',
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND NOT EXISTS ( SELECT 1 FROM TCURR_ALLTYPES y where y.tcurr_fcurr = x.pFromCurrency AND y.tcurr_tcurr = x.pToCurrency )
	AND EXISTS ( SELECT 1 FROM TCURR_ALLTYPES z where z.tcurr_fcurr = x.pToCurrency AND z.tcurr_tcurr = x.pFromCurrency )
	AND processed_flag = 'Y'
	AND vRefCur is null
	AND ifnull(pFromExchangeRate,0) = 0; /* added 01/24/2014 to fix varian PO 6512990 */ 

UPDATE  tmp_populate_help_exchangerate t
SET vRefCur = v.TCURV_BWAER
from tmp_populate_help_exchangerate t left join TCURV_ALLTYPES v on v.tcurv_kurst = t.vtype
WHERE updflag = 'N'
	AND processed_flag = 'Y';


UPDATE  tmp_populate_help_exchangerate t
SET vInvAllow = ifnull(t1,'N')
from tmp_populate_help_exchangerate t left join (select 'Y' as t1,tcurv_kurst from TCURV_ALLTYPES  where TCURV_XINVR is not null) v1
 on v1.tcurv_kurst = t.vtype
WHERE updflag = 'N'
	AND processed_flag = 'Y';

UPDATE  tmp_populate_help_exchangerate t
SET vTCURFExists = ifnull(t2,0)
from tmp_populate_help_exchangerate t left join (select distinct 1 as t2,tcurf_kurst,tcurf_fcurr,tcurf_tcurr
 from TCURF_ALLTYPES) f
 on f.tcurf_kurst = t.vtype AND f.tcurf_fcurr = t.pfromcurrency AND f.tcurf_tcurr = t.ptocurrency
WHERE updflag = 'N'
	AND processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate
SET vRefCur = null
WHERE updflag = 'N' 
	AND vRefCur = pToCurrency
	AND processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate
SET vReverse = 'Y',
	pFromExchangeRate = -1 * pFromExchangeRate,
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency,
	vRefCur = null
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND vRefCur = pFromCurrency
	AND processed_flag = 'Y'	
	AND ifnull(pFromExchangeRate,0) = 0; /* added 01/24/2014 to fix varian PO 6512990 */ 
	
UPDATE tmp_populate_help_exchangerate
SET pFromCurrency = vFromCurr,
	pToCurrency = vToCurr
WHERE updflag = 'N' 
	AND processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate
SET vToCurr = vRefCur
WHERE updflag = 'N' 
	AND vRefCur is not null
	AND vRefCur <> vToCurr
	AND processed_flag = 'Y';


/***** Prepare and use TCURF_ALLTYPES *****/

drop table if exists tmp_helpstdexchgrt_tcurf;
CREATE table tmp_helpstdexchgrt_tcurf
AS
SELECT  TCURF_FCURR,TCURF_TCURR,TCURF_KURST,tcurf_gdatu,TCURF_FFACT,TCURF_TFACT,
cast(left((99999999 - tcurf_gdatu),4) || '-' ||left(right((99999999 - tcurf_gdatu),4),2)|| '-' ||right((99999999 - tcurf_gdatu),2) AS date) "cast_tcurf_gdatu"
FROM TCURF_ALLTYPES;

/* Denormalize to get date ranges */

drop table if exists tmp_helpstdexchgrt_tcurf_denorm;
CREATE table tmp_helpstdexchgrt_tcurf_denorm
AS
SELECT t.*, "cast_tcurf_gdatu" as cast_tcurf_gdatu_to
FROM tmp_helpstdexchgrt_tcurf t;

drop table if exists tmp_min_helpstdexchgrt_tcurf;

create table tmp_min_helpstdexchgrt_tcurf as
select distinct y.TCURF_FCURR,y.TCURF_TCURR,y.TCURF_KURST,y."cast_tcurf_gdatu",
ifnull(lead(y."cast_tcurf_gdatu",1) over (partition by y.TCURF_FCURR, y.TCURF_TCURR, y.TCURF_KURST ORDER BY y."cast_tcurf_gdatu"), '9999-12-31') AS MIN_cast_tcurf_gdatu
 from tmp_helpstdexchgrt_tcurf y,tmp_helpstdexchgrt_tcurf_denorm x
WHERE x.TCURF_FCURR = y.TCURF_FCURR
AND x.TCURF_TCURR = y.TCURF_TCURR
AND x.TCURF_KURST = y.TCURF_KURST
AND y."cast_tcurf_gdatu" > x."cast_tcurf_gdatu";

UPDATE  tmp_helpstdexchgrt_tcurf_denorm x
SET cast_tcurf_gdatu_to = MIN_cast_tcurf_gdatu
from tmp_min_helpstdexchgrt_tcurf y,tmp_helpstdexchgrt_tcurf_denorm x
WHERE x.TCURF_FCURR = y.TCURF_FCURR
AND x.TCURF_TCURR = y.TCURF_TCURR
AND x.TCURF_KURST = y.TCURF_KURST
and y."cast_tcurf_gdatu" = x."cast_tcurf_gdatu"
AND cast_tcurf_gdatu_to <> MIN_cast_tcurf_gdatu ;

/* Now update the latest row with 31 Dec 9999 */

UPDATE tmp_helpstdexchgrt_tcurf_denorm x
SET cast_tcurf_gdatu_to = '31 Dec 9999'
WHERE cast_tcurf_gdatu_to IS NULL;

/**********/

/***** Prepare and use TCURR_ALLTYPES *****/

drop table if exists tmp_stgexchgrt_tcurr;
CREATE table tmp_stgexchgrt_tcurr
AS
SELECT  tcurr_fcurr,tcurr_tcurr,tcurr_kurst,tcurr_gdatu,tcurr_ffact,tcurr_tfact,tcurr_ukurs,
case when length(regexp_REPLACE(tcurr_gdatu,'\D',''))<8 then null else 
cast(left((99999999 - tcurr_gdatu),4) || '-' || left(right((99999999 - tcurr_gdatu),4),2) || '-'|| right((99999999 - tcurr_gdatu),2) AS date) END AS "cast_tcurr_gdatu"
from TCURR_ALLTYPES;


/* Denormalize to get date ranges */

drop table if exists tmp_helpexchgrt_tcurr_denorm;
CREATE table tmp_helpexchgrt_tcurr_denorm
AS
SELECT t.*,"cast_tcurr_gdatu" as cast_tcurr_gdatu_to
FROM tmp_stgexchgrt_tcurr t;

drop table if exists tmp_min_stgexchgrt_tcurr;

create table tmp_min_stgexchgrt_tcurr as
select DISTINCT y.tcurr_fcurr,y.tcurr_tcurr,y.tcurr_kurst, y."cast_tcurr_gdatu",
ifnull(lead(y."cast_tcurr_gdatu",1) over (partition by y.TCURR_FCURR, y.TCURR_TCURR, y.TCURR_KURST ORDER BY y."cast_tcurr_gdatu"), '9999-12-31') AS MIN_cast_tcurr_gdatu
 from tmp_stgexchgrt_tcurr y,tmp_helpexchgrt_tcurr_denorm x
WHERE x.TCURR_FCURR = y.TCURR_FCURR
AND x.TCURR_TCURR = y.TCURR_TCURR
AND x.TCURR_KURST = y.TCURR_KURST;
/*AND y."cast_tcurr_gdatu" > x."cast_tcurr_gdatu" */

UPDATE  tmp_helpexchgrt_tcurr_denorm x
SET cast_tcurr_gdatu_to = MIN_cast_tcurr_gdatu
from tmp_min_stgexchgrt_tcurr y,tmp_helpexchgrt_tcurr_denorm x
WHERE x.TCURR_FCURR = y.TCURR_FCURR
AND x.TCURR_TCURR = y.TCURR_TCURR
AND x.TCURR_KURST = y.TCURR_KURST
and y."cast_tcurr_gdatu" = x."cast_tcurr_gdatu"
AND cast_tcurr_gdatu_to <> MIN_cast_tcurr_gdatu;

/* Now update the latest row with 31 Dec 9999 */

UPDATE tmp_helpexchgrt_tcurr_denorm x
SET cast_tcurr_gdatu_to = TO_DATE('31-12-9999','DD-MM-YYYY')
WHERE cast_tcurr_gdatu_to IS NULL;

/**********/

UPDATE tmp_populate_help_exchangerate g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_helpstdexchgrt_tcurf_denorm t2,tmp_populate_help_exchangerate g 
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.pDate >= t2."cast_tcurf_gdatu" AND  g.pDate < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_helpstdexchgrt_tcurf_denorm t2, tmp_populate_help_exchangerate g
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND CURRENT_DATE >= t2."cast_tcurf_gdatu" AND  CURRENT_DATE < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM (select TCURF_FCURR,TCURF_TCURR,TCURF_KURST,min(TCURF_FFACT) TCURF_FFACT,min(TCURF_TFACT) TCURF_TFACT from tmp_helpstdexchgrt_tcurf_denorm
group by TCURF_FCURR,TCURF_TCURR,TCURF_KURST ) t2,
tmp_populate_help_exchangerate g 
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_helpstdexchgrt_tcurf_denorm t2,tmp_populate_help_exchangerate g 
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.pDate >= t2."cast_tcurf_gdatu" AND  g.pDate < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_helpstdexchgrt_tcurf_denorm t2,tmp_populate_help_exchangerate g 
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND CURRENT_DATE >= t2."cast_tcurf_gdatu" AND  CURRENT_DATE < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_helpstdexchgrt_tcurf_denorm t2, tmp_populate_help_exchangerate g 
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET vFTfact = t2.TCURR_FFACT * t2.TCURR_TFACT,
	vffact = t2.TCURR_FFACT,
	vtfact = t2.TCURR_TFACT
FROM tmp_helpexchgrt_tcurr_denorm t2, tmp_populate_help_exchangerate g 
WHERE g.pFromCurrency = t2.TCURR_FCURR AND g.pToCurrency = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2."cast_tcurr_gdatu" AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET vFTfact = t2.TCURR_FFACT * t2.TCURR_TFACT,
	vffact = t2.TCURR_FFACT,
	vtfact = t2.TCURR_TFACT
FROM tmp_helpexchgrt_tcurr_denorm t2, tmp_populate_help_exchangerate g 
WHERE g.pFromCurrency = t2.TCURR_FCURR AND g.pToCurrency = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2."cast_tcurr_gdatu" AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';





UPDATE tmp_populate_help_exchangerate g
SET exchangeRate = case vReverse 
			when 'Y' then pFromExchangeRate * case vFTfact when 0 then 1.00000000 else vFTfact end
			else pFromExchangeRate / case vFTfact when 0 then 1.00000000 else vFTfact end
		   end,
	updflag = 'A'
WHERE pFromExchangeRate > 0
	AND updflag = 'N' 
	AND processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g
SET exchangeRate = pFromExchangeRate,
	updflag = 'A'
WHERE pFromExchangeRate > 0
	AND updflag = 'A'
	AND exchangeRate = 0
	AND processed_flag = 'Y';


UPDATE tmp_populate_help_exchangerate g 
SET exchangeRate = tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,
	updflag = 'B'
FROM tmp_helpexchgrt_tcurr_denorm t2,tmp_populate_help_exchangerate g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2."cast_tcurr_gdatu" AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND vReverse = 'Y'
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET exchangeRate = tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,
	updflag = 'B'
FROM tmp_helpexchgrt_tcurr_denorm t2, tmp_populate_help_exchangerate g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2."cast_tcurr_gdatu" AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND vReverse = 'Y'
	AND g.processed_flag = 'Y';


UPDATE tmp_populate_help_exchangerate g 
SET exchangeRate = CASE WHEN tcurr_ukurs = 0 THEN 1
			WHEN tcurr_ukurs > 0
                        THEN tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end
			ELSE -1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end)
		   END,
	updflag = 'B'
FROM tmp_helpexchgrt_tcurr_denorm t2,tmp_populate_help_exchangerate g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2."cast_tcurr_gdatu" AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND g.processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g 
SET exchangeRate = CASE WHEN tcurr_ukurs = 0 THEN 1
			WHEN tcurr_ukurs > 0
                        THEN tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end
			ELSE -1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end)
		   END,
	updflag = 'B'
FROM tmp_helpexchgrt_tcurr_denorm t2,tmp_populate_help_exchangerate g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2."cast_tcurr_gdatu" AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND g.processed_flag = 'Y';



UPDATE tmp_populate_help_exchangerate
SET vFromCurr = pToCurrency, updflag = 'C'
WHERE updflag = 'B' 
	AND vRefCur is not null
	AND vRefCur = vToCurr
	AND vRefCur <> pToCurrency
	AND processed_flag = 'Y';


UPDATE tmp_populate_help_exchangerate g 
SET exchangeRate = exchangeRate / CASE WHEN tcurr_ukurs = 0 THEN 1
					WHEN tcurr_ukurs > 0
					THEN tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end
					ELSE -1.00000000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end)
				   END,
	updflag = 'D'
FROM tmp_helpexchgrt_tcurr_denorm t2,tmp_populate_help_exchangerate g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2."cast_tcurr_gdatu" AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'C' 
	AND g.processed_flag = 'Y';


















UPDATE tmp_populate_help_exchangerate g 
SET exchangeRate = exchangeRate / CASE WHEN tcurr_ukurs = 0 THEN 1
					WHEN tcurr_ukurs > 0
					THEN tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end
					ELSE -1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end)
				   END,
	updflag = 'D'
FROM tmp_helpexchgrt_tcurr_denorm t2,tmp_populate_help_exchangerate g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2."cast_tcurr_gdatu" AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'C' 
	AND g.processed_flag = 'Y';


UPDATE tmp_populate_help_exchangerate g
SET exchangeRate = -1 * exchangeRate
WHERE updflag <> 'N'
	AND vReverse = 'Y'
	AND processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g
SET exchangeRate = 1, updflag = 'E'
WHERE exchangeRate = 0
	AND processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g
SET exchangeRate = round(-1/exchangeRate,6), updflag = 'E'
WHERE exchangeRate < 0
	AND processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g
SET vFromCurr = pFromCurrency,
	vToCurr = pToCurrency
WHERE processed_flag = 'Y';

UPDATE tmp_populate_help_exchangerate g
SET pFromCurrency = vToCurr,
	pToCurrency = vFromCurr,
	pfromexchangerate = -1 * pfromexchangerate
WHERE processed_flag = 'Y'
	AND vReverse = 'Y';

UPDATE tmp_populate_help_exchangerate
SET processed_flag = 'D'
WHERE processed_flag = 'Y';

/* Instead of delete, using intermediate-rename as we don't want data to be unavailable at any time in help_exchangerate */
DROP TABLE IF EXISTS tmp_intermediate_help_exchangerate;
CREATE TABLE tmp_intermediate_help_exchangerate
as
select * from help_exchangerate where 1=2;

INSERT INTO tmp_intermediate_help_exchangerate
( pfromcurrency,ptocurrency,pDate,exchangeratetype,exchangerate,datasource)
SELECT DISTINCT pfromcurrency,ptocurrency,pDate,vType,exchangerate,'SAP'
from tmp_populate_help_exchangerate;

DROP TABLE IF EXISTS bkp_help_exchangerate_previousday;
RENAME help_exchangerate to bkp_help_exchangerate_previousday;
RENAME tmp_intermediate_help_exchangerate to help_exchangerate;


DROP TABLE IF EXISTS tmp_populate_help_exchangerate;
DROP TABLE IF EXISTS tmp_dim_date_helpexchgrate;
DROP TABLE IF EXISTS TMP_TCURR_ALLTYPES_UNIQUE;
drop table if exists tmp_helpstdexchgrt_tcurf;
drop table if exists tmp_helpstdexchgrt_tcurf_denorm;
drop table if exists tmp_stgexchgrt_tcurr;
drop table if exists tmp_helpexchgrt_tcurr_denorm;
DROP TABLE IF EXISTS tmp_intermediate_help_exchangerate;

/*
THE STATEMENT BELOW INSERTS THE COMBINATIONS OF CURRENCIES THAT MISS FROM TCURR(TCURR_ALLTYPES) TABLE
EXAMPLE: IF WE MISS THE COMBINATION AUD-USD, BUT WE HAVE THE COMBINATION AUD-EUR AND EUR-USD, 
WE CAN GET THE MISSING COMBINATION BY COMBINING THE VALUES WE HAVE
EXAMPLE 2: IF WE MISS THE COMBINATION AUD-HNY, BUT WE HAVE THE COMBINATION AUD-EUR AND EUR-HNY, 
WE CAN GET THE MISSING COMBINATION BY COMBINING THE VALUES WE HAVE
*/
insert into help_exchangerate
select ptoeur.pfromcurrency,pfromeur.ptocurrency,pfromeur.pdate,pfromeur.exchangeratetype,
pfromeur.exchangerate * ptoeur.exchangerate as exchangerate,'Derived from EUR' as datasource,pfromeur.dw_update_date 
from 

(select * from help_exchangerate
where pfromcurrency = 'EUR'
) pfromeur
inner join
(select * from help_exchangerate
where ptocurrency = 'EUR'
) ptoeur

on pfromeur.pfromcurrency = ptoeur.ptocurrency
and pfromeur.pdate = ptoeur.pdate
and pfromeur.exchangeratetype = ptoeur.exchangeratetype
and pfromeur.datasource = ptoeur.datasource

where 

not exists (select 1 from help_exchangerate t
where t.pfromcurrency =  ptoeur.pfromcurrency
and t.ptocurrency = pfromeur.ptocurrency
and t.pdate = pfromeur.pdate
and t.exchangeratetype = pfromeur.exchangeratetype);

