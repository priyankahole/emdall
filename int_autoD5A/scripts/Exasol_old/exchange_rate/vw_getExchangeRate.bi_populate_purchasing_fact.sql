/*********************************************Change History*******************************************************/
/* Date             By             Version           Desc                                                         */
/* 03 Mar 2015      Liviu Ionescu                    Add Combine for ekko_ekpo_eket */
/******************************************************************************************************************/

/*Alin 5 mar 2018 APP-8849 - Purchasing document type code
this merge is here so EKKO_EKPO_EKET_EKBE can be used*/
drop table if exists EKKO_EKPO_EKET_EKBE_tmp_for_AP;
create table EKKO_EKPO_EKET_EKBE_tmp_for_AP
	as select * from EKKO_EKPO_EKET_EKBE;

merge into fact_accountspayable f
using(
select distinct fact_accountspayableid, 
ifnull(EKKO_BSART, 'Not Set') as BSART
from EKKO_EKPO_EKET_EKBE_tmp_for_AP e, 
fact_accountspayable f
WHERE  
f.DD_PURCH_ORDER_NUMBER = IFNULL(EKPO_EBELN, 'Not Set')
)t
on f.fact_accountspayableid = t.fact_accountspayableid
when matched then update
set f.dd_purch_doc_type_code = BSART
where f.dd_purch_doc_type_code <> BSART;

/* Yogini 13 March 2018 APP-8849 - PO Plant */
merge into fact_accountspayable f
using(
	select fact_accountspayableid, max(ifnull(pl.dim_plantid, 1)) as dim_plantid
	from EKKO_EKPO_EKET_EKBE_tmp_for_AP e,  fact_accountspayable f, dim_plant pl, dim_company c
	where f.DD_PURCH_ORDER_NUMBER = ifnull(EKPO_EBELN, 'Not Set')
	and pl.plantcode = ifnull(EKPO_WERKS, 'Not Set')
	and c.companycode = ifnull(EKPO_BUKRS, 'Not Set')
	--AND pl.companycode = c.companycode
	and pl.RowIsCurrent = 1
	group by fact_accountspayableid
)upd
on f.fact_accountspayableid = upd.fact_accountspayableid
when matched then update set f.Dim_PoPlantId = upd.dim_plantid
where f.Dim_PoPlantId <> upd.dim_plantid;

/* Yogini 4 May 2018 APP-8849 - MaterialGroupCode */
merge into fact_accountspayable f
using(
	select fact_accountspayableid, ifnull(max(MG.Dim_MaterialGroupid), 1) as Dim_MaterialGroupid
	from EKKO_EKPO_EKET_EKBE_tmp_for_AP e,  fact_accountspayable f, Dim_MaterialGroup mg
	where f.DD_PURCH_ORDER_NUMBER = ifnull(EKPO_EBELN, 'Not Set')
	and mg.MaterialGroupCode = EKPO_MATKL
	group by fact_accountspayableid
)upd
on f.fact_accountspayableid = upd.fact_accountspayableid
when matched then update set f.Dim_MaterialGroupid = upd.Dim_MaterialGroupid
where f.Dim_MaterialGroupid <> upd.Dim_MaterialGroupid;

/* Yogini 21 May 2018 APP-8849 - PO Part */
merge into fact_accountspayable f
using(
	select fact_accountspayableid, max(ifnull(pt.dim_partid, 1)) as dim_PoPartId
	from EKKO_EKPO_EKET_EKBE_tmp_for_AP e,  fact_accountspayable f, dim_part pt
	WHERE f.DD_PURCH_ORDER_NUMBER = IFNULL(EKPO_EBELN, 'Not Set')
	AND pt.partnumber = ifnull(EKPO_MATNR, 'Not Set')
	AND pt.RowIsCurrent = 1
	group by fact_accountspayableid
)upd
on f.fact_accountspayableid = upd.fact_accountspayableid
when matched then update set f.Dim_PoPartId = ifnull(upd.dim_PoPartId, 1)
where f.Dim_PoPartId <> ifnull(upd.dim_PoPartId, 1);

drop table if exists EKKO_EKPO_EKET_EKBE_tmp_for_AP;



/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
select 'processinglog',ifnull(max(processinglogid),0)
FROM processinglog;


/* ekko_ekpo_eket updates need to be handled here as this is used for exchange rate processing of purchasing_fact */

/* Start of ekko_ekpo_eket updates */

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact', current_date, 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

 
INSERT INTO ekko_ekpo_eket 
(eket_banfn,
eket_bedat,
eket_bnfpo,
eket_charg,
eket_dabmg,
eket_eindt,
eket_eldat,
eket_etenr,
eket_j_3aefda,
eket_j_3aelikz ,
eket_j_3aetenr ,
eket_j_3aetenv ,
eket_j_3anetp,
eket_j_3anetw,
eket_j_3asize,
eket_j_3auanr,
eket_j_3aupos,
eket_j_4kscat,
eket_lddat,
eket_licha,
eket_mbdat,
eket_menge,
eket_sernr,
eket_slfdt,
eket_wamng,
eket_wemng,
ekko_absgr,
ekko_aedat,
ekko_autlf,
ekko_bedat,
ekko_bsart,
ekko_bstyp,
ekko_bukrs,
ekko_ekgrp,
ekko_ekorg,
ekko_ernam,
ekko_exnum,
ekko_fixpo,
ekko_frggr,
ekko_frgke,
ekko_frgsx,
ekko_frgzu,
ekko_ihrez,
ekko_inco1,
ekko_inco2,
ekko_kalsm,
ekko_kdatb,
ekko_kdate,
ekko_knumv,
ekko_kufix,
ekko_lifnr,
ekko_loekz,
ekko_lponr,
ekko_reswk,
ekko_statu,
ekko_unsez,
ekko_waers,
ekko_wkurs,
ekko_zterm,
ekpo_abskz,
ekpo_adrn2,
ekpo_adrnr,
ekpo_aedat,
ekpo_afnam,
ekpo_afs_collection   ,
ekpo_afs_theme ,
ekpo_banfn,
ekpo_bednr,
ekpo_bnfpo,
ekpo_bprme,
ekpo_bpumn,
ekpo_bpumz,
ekpo_brtwr,
ekpo_bstae,
ekpo_bukrs,
ekpo_bwtar,
ekpo_drunr,
ekpo_ean11,
ekpo_ebeln,
ekpo_ebelp,
ekpo_effwr,
ekpo_eglkz,
ekpo_elikz,
ekpo_erekz,
ekpo_etfz1,
ekpo_idnlf,
ekpo_inco1,
ekpo_inco2,
ekpo_infnr,
ekpo_insmk,
ekpo_j_1bownpro,
ekpo_j_3adat,
ekpo_j_3aexfcm ,
ekpo_j_3asean,
ekpo_knttp,
ekpo_ko_prctr,
ekpo_konnr,
ekpo_ktmng,
ekpo_ktpnr,
ekpo_kzabs,
ekpo_kzvbr,
ekpo_kzwi1,
ekpo_kzwi2,
ekpo_kzwi3,
ekpo_kzwi4,
ekpo_kzwi5,
ekpo_kzwi6,
ekpo_lgbzo,
ekpo_lgbzo_b,
ekpo_lgort,
ekpo_lmein,
ekpo_loekz,
ekpo_ltsnr,
ekpo_matkl,
ekpo_matnr,
ekpo_meins,
ekpo_menge,
ekpo_mtart,
ekpo_mwskz,
ekpo_netpr,
ekpo_netwr,
ekpo_packno,
ekpo_peinh,
ekpo_plifz,
ekpo_prdat,
ekpo_pstyp,
ekpo_repos,
ekpo_reslo,
ekpo_retpo,
ekpo_revlv,
ekpo_route,
ekpo_stapo,
ekpo_statu,
ekpo_status,
ekpo_tsbed,
ekpo_txz01,
ekpo_uebtk,
ekpo_uebto,
ekpo_umren,
ekpo_umrez,
ekpo_untto,
ekpo_vrtkz,
ekpo_webaz,
ekpo_wepos,
ekpo_werks,
ekpo_weunb,
ekpo_zzppv_rc,
ekpo_zzppv_rct ,
ekpo_zzprior
)
SELECT DISTINCT 
eket_banfn,
eket_bedat,
eket_bnfpo,
eket_charg,
eket_dabmg,
eket_eindt,
eket_eldat,
eket_etenr,
eket_j_3aefda,
eket_j_3aelikz ,
eket_j_3aetenr ,
eket_j_3aetenv ,
eket_j_3anetp,
eket_j_3anetw,
eket_j_3asize,
eket_j_3auanr,
eket_j_3aupos,
eket_j_4kscat,
eket_lddat,
eket_licha,
eket_mbdat,
eket_menge,
eket_sernr,
eket_slfdt,
eket_wamng,
eket_wemng,
ekko_absgr,
ekko_aedat,
ekko_autlf,
ekko_bedat,
ekko_bsart,
ekko_bstyp,
ekko_bukrs,
ekko_ekgrp,
ekko_ekorg,
ekko_ernam,
ekko_exnum,
ekko_fixpo,
ekko_frggr,
ekko_frgke,
ekko_frgsx,
ekko_frgzu,
ekko_ihrez,
ekko_inco1,
ekko_inco2,
ekko_kalsm,
ekko_kdatb,
ekko_kdate,
ekko_knumv,
ekko_kufix,
ekko_lifnr,
ekko_loekz,
ekko_lponr,
ekko_reswk,
ekko_statu,
ekko_unsez,
ekko_waers,
ekko_wkurs,
ekko_zterm,
ekpo_abskz,
ekpo_adrn2,
ekpo_adrnr,
ekpo_aedat,
ekpo_afnam,
ekpo_afs_collection   ,
ekpo_afs_theme ,
ekpo_banfn,
ekpo_bednr,
ekpo_bnfpo,
ekpo_bprme,
ekpo_bpumn,
ekpo_bpumz,
ekpo_brtwr,
ekpo_bstae,
ekpo_bukrs,
ekpo_bwtar,
ekpo_drunr,
ekpo_ean11,
ekpo_ebeln,
ekpo_ebelp,
ekpo_effwr,
ekpo_eglkz,
ekpo_elikz,
ekpo_erekz,
ekpo_etfz1,
ekpo_idnlf,
ekpo_inco1,
ekpo_inco2,
ekpo_infnr,
ekpo_insmk,
ekpo_j_1bownpro,
ekpo_j_3adat,
ekpo_j_3aexfcm ,
ekpo_j_3asean,
ekpo_knttp,
ekpo_ko_prctr,
ekpo_konnr,
ekpo_ktmng,
ekpo_ktpnr,
ekpo_kzabs,
ekpo_kzvbr,
ekpo_kzwi1,
ekpo_kzwi2,
ekpo_kzwi3,
ekpo_kzwi4,
ekpo_kzwi5,
ekpo_kzwi6,
ekpo_lgbzo,
ekpo_lgbzo_b,
ekpo_lgort,
ekpo_lmein,
ekpo_loekz,
ekpo_ltsnr,
ekpo_matkl,
ekpo_matnr,
ekpo_meins,
ekpo_menge,
ekpo_mtart,
ekpo_mwskz,
ekpo_netpr,
ekpo_netwr,
ekpo_packno,
ekpo_peinh,
ekpo_plifz,
ekpo_prdat,
ekpo_pstyp,
ekpo_repos,
ekpo_reslo,
ekpo_retpo,
ekpo_revlv,
ekpo_route,
ekpo_stapo,
ekpo_statu,
ekpo_status,
ekpo_tsbed,
ekpo_txz01,
ekpo_uebtk,
ekpo_uebto,
ekpo_umren,
ekpo_umrez,
ekpo_untto,
ekpo_vrtkz,
ekpo_webaz,
ekpo_wepos,
ekpo_werks,
ekpo_weunb,
ekpo_zzppv_rc,
ekpo_zzppv_rct ,
ekpo_zzprior
FROM ekko_ekpo_eket_mseg a
WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);

	
	
DELETE FROM ekko_ekpo_eket_mseg;

INSERT INTO ekko_ekpo_eket
(eket_banfn,
eket_bedat,
eket_bnfpo,
eket_charg,
eket_dabmg,
eket_eindt,
eket_eldat,
eket_etenr,
eket_j_3aefda,
eket_j_3aelikz ,
eket_j_3aetenr ,
eket_j_3aetenv ,
eket_j_3anetp,
eket_j_3anetw,
eket_j_3asize,
eket_j_3auanr,
eket_j_3aupos,
eket_j_4kscat,
eket_lddat,
eket_licha,
eket_mbdat,
eket_menge,
eket_sernr,
eket_slfdt,
eket_wamng,
eket_wemng,
ekko_absgr,
ekko_aedat,
ekko_autlf,
ekko_bedat,
ekko_bsart,
ekko_bstyp,
ekko_bukrs,
ekko_ekgrp,
ekko_ekorg,
ekko_ernam,
ekko_exnum,
ekko_fixpo,
ekko_frggr,
ekko_frgke,
ekko_frgsx,
ekko_frgzu,
ekko_ihrez,
ekko_inco1,
ekko_inco2,
ekko_kalsm,
ekko_kdatb,
ekko_kdate,
ekko_knumv,
ekko_kufix,
ekko_lifnr,
ekko_loekz,
ekko_lponr,
ekko_reswk,
ekko_statu,
ekko_unsez,
ekko_waers,
ekko_wkurs,
ekko_zterm,
ekpo_abskz,
ekpo_adrn2,
ekpo_adrnr,
ekpo_aedat,
ekpo_afnam,
ekpo_afs_collection   ,
ekpo_afs_theme ,
ekpo_banfn,
ekpo_bednr,
ekpo_bnfpo,
ekpo_bprme,
ekpo_bpumn,
ekpo_bpumz,
ekpo_brtwr,
ekpo_bstae,
ekpo_bukrs,
ekpo_bwtar,
ekpo_drunr,
ekpo_ean11,
ekpo_ebeln,
ekpo_ebelp,
ekpo_effwr,
ekpo_eglkz,
ekpo_elikz,
ekpo_erekz,
ekpo_etfz1,
ekpo_idnlf,
ekpo_inco1,
ekpo_inco2,
ekpo_infnr,
ekpo_insmk,
ekpo_j_1bownpro,
ekpo_j_3adat,
ekpo_j_3aexfcm ,
ekpo_j_3asean,
ekpo_knttp,
ekpo_ko_prctr,
ekpo_konnr,
ekpo_ktmng,
ekpo_ktpnr,
ekpo_kzabs,
ekpo_kzvbr,
ekpo_kzwi1,
ekpo_kzwi2,
ekpo_kzwi3,
ekpo_kzwi4,
ekpo_kzwi5,
ekpo_kzwi6,
ekpo_lgbzo,
ekpo_lgbzo_b,
ekpo_lgort,
ekpo_lmein,
ekpo_loekz,
ekpo_ltsnr,
ekpo_matkl,
ekpo_matnr,
ekpo_meins,
ekpo_menge,
ekpo_mtart,
ekpo_mwskz,
ekpo_netpr,
ekpo_netwr,
ekpo_packno,
ekpo_peinh,
ekpo_plifz,
ekpo_prdat,
ekpo_pstyp,
ekpo_repos,
ekpo_reslo,
ekpo_retpo,
ekpo_revlv,
ekpo_route,
ekpo_stapo,
ekpo_statu,
ekpo_status,
ekpo_tsbed,
ekpo_txz01,
ekpo_uebtk,
ekpo_uebto,
ekpo_umren,
ekpo_umrez,
ekpo_untto,
ekpo_vrtkz,
ekpo_webaz,
ekpo_wepos,
ekpo_werks,
ekpo_weunb,
ekpo_zzppv_rc,
ekpo_zzppv_rct ,
ekpo_zzprior
)
SELECT distinct 
eket_banfn,
eket_bedat,
eket_bnfpo,
eket_charg,
eket_dabmg,
eket_eindt,
eket_eldat,
eket_etenr,
eket_j_3aefda,
eket_j_3aelikz ,
eket_j_3aetenr ,
eket_j_3aetenv ,
eket_j_3anetp,
eket_j_3anetw,
eket_j_3asize,
eket_j_3auanr,
eket_j_3aupos,
eket_j_4kscat,
eket_lddat,
eket_licha,
eket_mbdat,
eket_menge,
eket_sernr,
eket_slfdt,
eket_wamng,
eket_wemng,
ekko_absgr,
ekko_aedat,
ekko_autlf,
ekko_bedat,
ekko_bsart,
ekko_bstyp,
ekko_bukrs,
ekko_ekgrp,
ekko_ekorg,
ekko_ernam,
ekko_exnum,
ekko_fixpo,
ekko_frggr,
ekko_frgke,
ekko_frgsx,
ekko_frgzu,
ekko_ihrez,
ekko_inco1,
ekko_inco2,
ekko_kalsm,
ekko_kdatb,
ekko_kdate,
ekko_knumv,
ekko_kufix,
ekko_lifnr,
ekko_loekz,
ekko_lponr,
ekko_reswk,
ekko_statu,
ekko_unsez,
ekko_waers,
ekko_wkurs,
ekko_zterm,
ekpo_abskz,
ekpo_adrn2,
ekpo_adrnr,
ekpo_aedat,
ekpo_afnam,
ekpo_afs_collection   ,
ekpo_afs_theme ,
ekpo_banfn,
ekpo_bednr,
ekpo_bnfpo,
ekpo_bprme,
ekpo_bpumn,
ekpo_bpumz,
ekpo_brtwr,
ekpo_bstae,
ekpo_bukrs,
ekpo_bwtar,
ekpo_drunr,
ekpo_ean11,
ekpo_ebeln,
ekpo_ebelp,
ekpo_effwr,
ekpo_eglkz,
ekpo_elikz,
ekpo_erekz,
ekpo_etfz1,
ekpo_idnlf,
ekpo_inco1,
ekpo_inco2,
ekpo_infnr,
ekpo_insmk,
ekpo_j_1bownpro,
ekpo_j_3adat,
ekpo_j_3aexfcm ,
ekpo_j_3asean,
ekpo_knttp,
ekpo_ko_prctr,
ekpo_konnr,
ekpo_ktmng,
ekpo_ktpnr,
ekpo_kzabs,
ekpo_kzvbr,
ekpo_kzwi1,
ekpo_kzwi2,
ekpo_kzwi3,
ekpo_kzwi4,
ekpo_kzwi5,
ekpo_kzwi6,
ekpo_lgbzo,
ekpo_lgbzo_b,
ekpo_lgort,
ekpo_lmein,
ekpo_loekz,
ekpo_ltsnr,
ekpo_matkl,
ekpo_matnr,
ekpo_meins,
ekpo_menge,
ekpo_mtart,
ekpo_mwskz,
ekpo_netpr,
ekpo_netwr,
ekpo_packno,
ekpo_peinh,
ekpo_plifz,
ekpo_prdat,
ekpo_pstyp,
ekpo_repos,
ekpo_reslo,
ekpo_retpo,
ekpo_revlv,
ekpo_route,
ekpo_stapo,
ekpo_statu,
ekpo_status,
ekpo_tsbed,
ekpo_txz01,
ekpo_uebtk,
ekpo_uebto,
ekpo_umren,
ekpo_umrez,
ekpo_untto,
ekpo_vrtkz,
ekpo_webaz,
ekpo_wepos,
ekpo_werks,
ekpo_weunb,
ekpo_zzppv_rc,
ekpo_zzppv_rct ,
ekpo_zzprior
FROM ekko_ekpo_eket_ekbe a
WHERE not exists (select 1 from ekko_ekpo_eket b where a.EKPO_EBELN = b.EKPO_EBELN and a.EKPO_EBELP = b.EKPO_EBELP and a.EKET_ETENR = b.EKET_ETENR);

DELETE FROM ekko_ekpo_eket_ekbe;

UPDATE ekko_ekpo_eket
SET EKPO_BWTAR = IFNULL(EKPO_BWTAR,'Not Set'), EKPO_BUKRS = IFNULL(EKPO_BUKRS,EKKO_BUKRS);


INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_purchasing_fact', current_date, 'update ekko_ekpo_eket set EKPO_BWTAR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

/* End of ekko_ekpo_eket updates */

				 
								 
DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchasing_fact';

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT EKKO_WAERS as pFromCurrency, 'USD' as pToCurrency, 0 as pFromExchangeRate, EKKO_BEDAT as pDate, 
'bi_populate_purchasing_fact' as fact_script_name
FROM ekko_ekpo_eket dm_di_ds;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_purchasing_fact'; 

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency,pToCurrency,pFromExchangeRate,pDate, fact_script_name  )
SELECT DISTINCT EKKO_WAERS as pFromCurrency, dc.Currency as pToCurrency, 0 as pFromExchangeRate, EKKO_BEDAT as pDate,
'bi_populate_purchasing_fact' as fact_script_name
FROM ekko_ekpo_eket dm_di_ds, dim_company dc
WHERE dc.companycode = EKPO_BUKRS;


drop table if exists tmp_getExchangeRate1_nodups_pur_fact;
create table tmp_getExchangeRate1_nodups_pur_fact
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchasing_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchasing_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_pur_fact;

drop table tmp_getExchangeRate1_nodups_pur_fact;

