
/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_planorder_fact';

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT dp.Currency,'USD' pToCurrency, NULL pFromExchangeRate,PLAF_PEDTR pDate, NULL exchangeRate,'bi_populate_planorder_fact'
FROM plaf p
INNER JOIN dim_part dp
on dp.partnumber = ifnull(p.PLAF_MATNR, 'Not Set')
and dp.plant = ifnull(p.PLAF_PLWRK, 'Not Set');

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT dp.Currency,'USD' pToCurrency,NULL pFromExchangeRate, CURRENT_DATE pDate,NULL exchangeRate,'bi_populate_planorder_fact'
FROM plaf p
INNER JOIN dim_part dp
on dp.partnumber = ifnull(p.PLAF_MATNR, 'Not Set')
and dp.plant = ifnull(p.PLAF_PLWRK, 'Not Set');


UPDATE tmp_getExchangeRate1 ex
SET ex.pToCurrency = ifnull( s.property_value, 'USD')
FROM tmp_getExchangeRate1 ex
    CROSS JOIN (SELECT property_value
                            FROM systemproperty
                            WHERE property = 'customer.global.currency') s
WHERE ex.fact_script_name = 'bi_populate_planorder_fact';

/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_planorder_fact_nodups;
create table tmp_getExchangeRate1_planorder_fact_nodups
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_planorder_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_planorder_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_planorder_fact_nodups;

drop table tmp_getExchangeRate1_planorder_fact_nodups;
