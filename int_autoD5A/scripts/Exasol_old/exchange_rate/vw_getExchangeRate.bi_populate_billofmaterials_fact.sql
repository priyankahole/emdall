

/* Populate Tran->Global curr */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_billofmaterials_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT STPO_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,STPO_AEDAT pDate,NULL exchangeRate,'bi_populate_billofmaterials_fact'
FROM   STPO arc;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT STPO_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,STKO_AEDAT pDate,NULL exchangeRate,'bi_populate_billofmaterials_fact'
FROM STKO hdr , STPO item 
WHERE hdr.STKO_STLNR = item.STPO_STLNR
AND hdr.STKO_STLTY = item.STPO_STLTY;


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT STPO_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,current_date pDate,NULL exchangeRate,'bi_populate_billofmaterials_fact'
FROM STKO hdr , STPO item
WHERE hdr.STKO_STLNR = item.STPO_STLNR
AND hdr.STKO_STLTY = item.STPO_STLTY;



UPDATE tmp_getExchangeRate1 ter
SET ter.pToCurrency = ifNULL(s.property_value,'USD')
FROM tmp_getExchangeRate1 ter
CROSS JOIN (SELECT property_value
           FROM systemproperty 
   WHERE  property = 'customer.global.currency' ) s
WHERE ter.fact_script_name = 'bi_populate_billofmaterials_fact'
AND   ter.pToCurrency <> ifNULL(s.property_value,'USD') ;

/* Populate Tran->Local curr */

/* Item Data - stpo */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT STPO_WAERS pFromCurrency,c.currency,NULL pFromExchangeRate,STPO_AEDAT pDate,NULL exchangeRate,'bi_populate_billofmaterials_fact'
FROM STKO hdr,STPO item,dim_plant dp,dim_company c
WHERE hdr.STKO_STLNR = item.STPO_STLNR
AND hdr.STKO_STLTY = item.STPO_STLTY
AND dp.PlantCode = STPO_PSWRK AND dp.RowIsCurrent = 1
AND dp.companycode = c.companycode;


/* Header data - stko */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT STPO_WAERS pFromCurrency,c.currency,NULL pFromExchangeRate,STKO_AEDAT pDate,NULL exchangeRate,'bi_populate_billofmaterials_fact'
FROM STKO hdr,STPO item,dim_plant dp,dim_company c
WHERE hdr.STKO_STLNR = item.STPO_STLNR
AND hdr.STKO_STLTY = item.STPO_STLTY
AND dp.PlantCode = STKO_WRKAN AND dp.RowIsCurrent = 1
AND dp.companycode = c.companycode;



/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_fact_billofmaterials_nodups;
create table tmp_getExchangeRate1_fact_billofmaterials_nodups
as
select distinct * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_billofmaterials_fact';

delete from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_billofmaterials_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_fact_billofmaterials_nodups;

drop table tmp_getExchangeRate1_fact_billofmaterials_nodups;
