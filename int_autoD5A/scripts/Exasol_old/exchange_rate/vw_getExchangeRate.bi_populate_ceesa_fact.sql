/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_ceesa_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT CURRENCYLOC pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,
to_date(to_char(concat(FINYEAR, case when QUARTER='1' 
					then '-01-01' else case when QUARTER='2' 
					then '-04-01' else case when QUARTER='3' then '-07-01' else 
					case when QUARTER='4' then '-10-01' end end end end)),'YYYY-MM-DD') pDate,
NULL exchangeRate,'bi_populate_ceesa_fact'
FROM CEESA ces;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT CURRENCYLOC pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate, current_date pDate,NULL exchangeRate,'bi_populate_ceesa_fact'
FROM CEESA ces;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_ceesa_fact';

/* there is no data available to get the company currency */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT CURRENCYLOC,dc.Currency,NULL,
to_date(to_char(concat(FINYEAR, case when QUARTER='1' 
					then '-01-01' else case when QUARTER='2' 
					then '-04-01' else case when QUARTER='3' then '-07-01' else 
					case when QUARTER='4' then '-10-01' end end end end)),'YYYY-MM-DD'),
					NULL exchangeRate,'bi_populate_ceesa_fact'
FROM CEESA ces,dim_company dc
WHERE dc.CompanyCode = ces.CompanyCode;


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_ces;
create table tmp_getExchangeRate1_nodups_ces
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_ceesa_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_ceesa_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_ces;

drop table if exists tmp_getExchangeRate1_nodups_ces;