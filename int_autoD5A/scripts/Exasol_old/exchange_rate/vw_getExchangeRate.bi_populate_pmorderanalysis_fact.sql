
/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_pmorder_analysis_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT AUFK_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,AUFK_ERDAT pDate,NULL exchangeRate,'bi_populate_pmorder_analysis_fact'
FROM AUFK mhi;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT AUFK_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,current_date pDate,NULL exchangeRate,'bi_populate_pmorder_analysis_fact'
FROM AUFK mhi;



UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_pmorder_analysis_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT AUFK_WAERS,dc.Currency,NULL,AUFK_ERDAT,NULL exchangeRate,'bi_populate_pmorder_analysis_fact'
FROM AUFK mhi,dim_company dc
WHERE dc.CompanyCode = mhi.AUFK_BUKRS;


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_fact_pmorder_analysis_nodups;
create table tmp_getExchangeRate1_fact_pmorder_analysis_nodups
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_pmorder_analysis_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_pmorder_analysis_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_fact_pmorder_analysis_nodups;

drop table tmp_getExchangeRate1_fact_pmorder_analysis_nodups;
