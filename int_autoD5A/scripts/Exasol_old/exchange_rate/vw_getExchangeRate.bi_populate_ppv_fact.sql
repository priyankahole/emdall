/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_ppv_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT COEP_OWAER ,'USD' pToCurrency, NULL pFromExchangeRate,COBK_BUDAT pDate, NULL exchangeRate,'bi_populate_ppv_fact'
from COEP c
INNER join CSKU cu
on c.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on c.COEP_KOKRS = ck.COBK_KOKRS
and c.COEP_BELNR = ck.COBK_BELNR;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT COEP_OWAER,'USD' pToCurrency, NULL pFromExchangeRate,current_date pDate, NULL exchangeRate,'bi_populate_ppv_fact'
from COEP c
INNER join CSKU cu
on c.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on c.COEP_KOKRS = ck.COBK_KOKRS
and c.COEP_BELNR = ck.COBK_BELNR;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_ppv_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT COEP_OWAER,dc.Currency,NULL,COBK_BUDAT,NULL exchangeRate,'bi_populate_ppv_fact'
from COEP c
INNER join CSKU cu
on c.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on c.COEP_KOKRS = ck.COBK_KOKRS
and c.COEP_BELNR = ck.COBK_BELNR
inner join dim_company dc
on dc.CompanyCode = ifnull(c.COEP_BUKRS, 'Not Set');

/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_fact_ppv_nodups;
create table tmp_getExchangeRate1_fact_ppv_nodups
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_ppv_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_ppv_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_fact_ppv_nodups;

drop table if exists tmp_getExchangeRate1_fact_ppv_nodups;
