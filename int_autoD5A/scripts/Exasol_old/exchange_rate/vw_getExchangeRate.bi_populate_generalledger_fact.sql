
/*   17 Sep 2013      Lokesh	1.1  		  Changed queries to use BSIS and BSAS so that there is no need to split AR script */



/* Populate Tran->Global curr */
/* LK: As discussed with Hiten, from exchange rate should never be used for getting global rate */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_generalledger_fact';


/* As per Chris, posting date budat should be used. But there's also a document date. Commented that out for now */

/*
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSIS_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSIS_BLDAT pDate,NULL exchangeRate,'bi_populate_generalledger_fact'
FROM   BSIS arc

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAS_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSAS_BLDAT pDate,NULL exchangeRate,'bi_populate_generalledger_fact'
FROM BSAS arc*/

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSIS_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSIS_BUDAT pDate,NULL exchangeRate,'bi_populate_generalledger_fact'
FROM   BSIS arc;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAS_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSAS_BUDAT pDate,NULL exchangeRate,'bi_populate_generalledger_fact'
FROM BSAS arc;


UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull(t.property_value,'USD')
FROM tmp_getExchangeRate1, (SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD') t
WHERE fact_script_name = 'bi_populate_generalledger_fact';

/* Tran->Local exchg rates  : Done in the fact script itself using BSIS_WRBTR (amt in doc currency) and BSIS_DMBTR (amt in local currency) */


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1fact_gl_nodups;
create table tmp_getExchangeRate1fact_gl_nodups
as
select distinct * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_generalledger_fact';

delete from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_generalledger_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1fact_gl_nodups;

drop table tmp_getExchangeRate1fact_gl_nodups;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
