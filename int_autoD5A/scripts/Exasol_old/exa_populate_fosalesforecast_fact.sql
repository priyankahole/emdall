DELETE FROM stage_fosalesforecast;
insert into stage_fosalesforecast select * from stage_fosalesforecast_3holdout;
insert into stage_fosalesforecast select * from stage_fosalesforecast_6holdout;


/*15 Feb 2018 Georgiana Changes according to App - 5981*/
/*Round Forecast Quantity*/

UPDATE merck.stage_fosalesforecast
SET ct_forecastquantity=ceil(ct_forecastquantity);

/*Recalculate Mapes based on the new forecast quantities*/
DROP TABLE IF EXISTS tmp_recalculatemape;
CREATE TABLE tmp_recalculatemape
AS 
SELECT 
f.dd_reportingdate,dd_partnumber,dd_sales_cocd,f.dd_REPORTING_COMPANY_CODE,
f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,
dd_forecasttype,
100 * avg(abs((ct_forecastquantity-ct_salesquantity)/ct_salesquantity)) ct_mape_fofcst
FROM stage_fosalesforecast f
WHERE dd_forecastsample = 'Test'
GROUP BY 
f.dd_reportingdate,dd_partnumber,dd_sales_cocd,f.dd_REPORTING_COMPANY_CODE,
f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,
dd_forecasttype;

update stage_fosalesforecast f
set f.ct_mape = t.ct_mape_fofcst
from tmp_recalculatemape t ,stage_fosalesforecast f
where f.dd_reportingdate=t.dd_reportingdate
and f.dd_partnumber=t.dd_partnumber
and t.dd_sales_cocd=f.dd_sales_cocd
and f.dd_REPORTING_COMPANY_CODE =t.dd_REPORTING_COMPANY_CODE
and f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
and f.DD_HEI_CODE=t.DD_HEI_CODE
and f.DD_MARKET_GROUPING=t.DD_MARKET_GROUPING
and f.dd_forecasttype=t.dd_forecasttype;


/*Ranks Recalculation based on the new mapes*/
DROP TABLE IF EXISTS tmp_recalculaterank;
CREATE TABLE tmp_recalculaterank
AS
SELECT row_number() over(partition by f.dd_reportingdate,dd_partnumber,dd_sales_cocd,f.dd_REPORTING_COMPANY_CODE,
f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING
 order by ct_mape,dd_forecasttype asc) dd_rank,
f.dd_reportingdate,dd_partnumber,dd_sales_cocd,f.dd_REPORTING_COMPANY_CODE,
f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype
FROM (
SELECT DISTINCT f.dd_reportingdate,dd_partnumber,dd_sales_cocd,f.dd_REPORTING_COMPANY_CODE,
f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,f.dd_forecasttype, f.ct_mape
FROM stage_fosalesforecast f
WHERE f.dd_forecastsample = 'Test') f;

UPDATE stage_fosalesforecast f
SET dd_forecastrank=dd_rank
FROM stage_fosalesforecast  f,tmp_recalculaterank t
where f.dd_reportingdate=t.dd_reportingdate
and f.dd_partnumber=t.dd_partnumber
and t.dd_sales_cocd=f.dd_sales_cocd
and f.dd_REPORTING_COMPANY_CODE =t.dd_REPORTING_COMPANY_CODE
and f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
and f.DD_HEI_CODE=t.DD_HEI_CODE
and f.DD_MARKET_GROUPING=t.DD_MARKET_GROUPING
and f.dd_forecasttype=t.dd_forecasttype
and f.dd_forecastrank <> t.dd_rank;

 UPDATE stage_fosalesforecast f
SET f.ct_mape = 0 where f.ct_mape is null;

/* 15 Feb 2018 End of changes*/

DROP TABLE IF EXISTS tmp_forecastcov1;
CREATE TABLE tmp_forecastcov1
AS
select DD_PARTNUMBER,DD_SALES_COCD,DD_HEI_CODE,DD_REPORTING_COMPANY_CODE,DD_COUNTRY_DESTINATION_CODE,DD_MARKET_GROUPING,
dd_forecastdate,dd_forecastrank,dd_forecasttype,dd_reportingdate,
ct_forecastquantity
FROM stage_fosalesforecast s 
WHERE s.dd_forecastsample in ('Test','Horizon');

UPDATE tmp_forecastcov1
SET ct_forecastquantity = 1
WHERE ct_forecastquantity = 0;

DROP TABLE IF EXISTS tmp_forecastcov;
CREATE TABLE tmp_forecastcov
as
SELECT DD_PARTNUMBER,DD_SALES_COCD,DD_HEI_CODE,DD_REPORTING_COMPANY_CODE,DD_COUNTRY_DESTINATION_CODE,DD_MARKET_GROUPING,
dd_forecastrank,dd_forecasttype,dd_reportingdate,
(stddev(ct_forecastquantity) / avg(ct_forecastquantity)) ct_variationcoeff
FROM tmp_forecastcov1
GROUP BY DD_PARTNUMBER,DD_SALES_COCD,DD_HEI_CODE,DD_REPORTING_COMPANY_CODE,DD_COUNTRY_DESTINATION_CODE,DD_MARKET_GROUPING,
dd_forecastrank,dd_forecasttype,dd_reportingdate;


/* Get all parts-plant-mkt.. which have atleast 1 non-straightline forecast e.g CoV > 1% */

DROP TABLE IF EXISTS tmp_atleast1nonstlinefcst;
CREATE TABLE tmp_atleast1nonstlinefcst
AS
SELECT DISTINCT DD_PARTNUMBER,DD_SALES_COCD,DD_HEI_CODE,DD_REPORTING_COMPANY_CODE,DD_COUNTRY_DESTINATION_CODE,DD_MARKET_GROUPING,dd_reportingdate
FROM tmp_forecastcov
WHERE ct_variationcoeff > 0.01;

/* Delete all ranks where there's a straight line forecast */
DELETE FROM stage_fosalesforecast s
WHERE EXISTS ( SELECT 1 FROM tmp_atleast1nonstlinefcst l 
WHERE l.DD_PARTNUMBER = s.DD_PARTNUMBER AND l.DD_SALES_COCD = s.DD_SALES_COCD AND l.DD_REPORTING_COMPANY_CODE = s.DD_REPORTING_COMPANY_CODE
AND l.DD_HEI_CODE = s.DD_HEI_CODE AND l.DD_COUNTRY_DESTINATION_CODE = s.DD_COUNTRY_DESTINATION_CODE AND l.DD_MARKET_GROUPING = s.DD_MARKET_GROUPING
and l.dd_reportingdate=s.dd_reportingdate)
AND EXISTS ( SELECT 1 FROM tmp_forecastcov t 
WHERE t.DD_PARTNUMBER = s.DD_PARTNUMBER AND t.DD_SALES_COCD = s.DD_SALES_COCD AND t.DD_REPORTING_COMPANY_CODE = s.DD_REPORTING_COMPANY_CODE
AND t.DD_HEI_CODE = s.DD_HEI_CODE AND t.DD_COUNTRY_DESTINATION_CODE = s.DD_COUNTRY_DESTINATION_CODE AND t.DD_MARKET_GROUPING = s.DD_MARKET_GROUPING
AND t.dd_forecastrank = s.dd_forecastrank
AND t.dd_forecasttype = s.dd_forecasttype
and t.dd_reportingdate=s.dd_reportingdate
AND t.ct_variationcoeff <= 0.01);

/* Explicitly delete the 4 methods which always give st. line forecasts */
DELETE FROM stage_fosalesforecast s
WHERE (DD_FORECASTTYPE in ('Auto-Regressive','ARMA','Auto ETS')
OR DD_FORECASTTYPE like 'Croston%');

DROP TABLE IF EXISTS tmp_partvsmape;
CREATE TABLE tmp_partvsmape
AS
SELECT DISTINCT DD_PARTNUMBER,DD_SALES_COCD,DD_HEI_CODE,DD_REPORTING_COMPANY_CODE,DD_COUNTRY_DESTINATION_CODE,DD_MARKET_GROUPING,
s.dd_reportingdate, s.dd_forecasttype,s.dd_forecastrank,ct_mape
FROM stage_fosalesforecast s 
WHERE dd_forecastsample = 'Test';

DROP TABLE IF EXISTS tmp_partvsmape_rerank;
CREATE TABLE tmp_partvsmape_rerank
AS
SELECT t.*,rank() over(partition by DD_PARTNUMBER,DD_SALES_COCD,DD_HEI_CODE,DD_REPORTING_COMPANY_CODE,DD_COUNTRY_DESTINATION_CODE,DD_MARKET_GROUPING,
    t.dd_reportingdate order by ct_mape,dd_forecasttype ) dd_rank
FROM tmp_partvsmape t;

UPDATE stage_fosalesforecast s
SET s.dd_forecastrank = t.dd_rank
FROM stage_fosalesforecast s,tmp_partvsmape_rerank t
WHERE t.DD_PARTNUMBER = s.DD_PARTNUMBER AND t.DD_SALES_COCD = s.DD_SALES_COCD AND t.DD_REPORTING_COMPANY_CODE = s.DD_REPORTING_COMPANY_CODE
AND t.DD_HEI_CODE = s.DD_HEI_CODE AND t.DD_COUNTRY_DESTINATION_CODE = s.DD_COUNTRY_DESTINATION_CODE AND t.DD_MARKET_GROUPING = s.DD_MARKET_GROUPING
AND s.dd_reportingdate = t.dd_reportingdate
AND s.dd_forecasttype = t.dd_forecasttype;

/* Cap High PI and Bias errors */
UPDATE stage_fosalesforecast
SET ct_highpi = 1000000
where ct_highpi > 1000000;
UPDATE stage_fosalesforecast
SET ct_bias_error = 1000000
where ct_bias_error > 1000000;

/* Update future sales to NULL */
update stage_fosalesforecast 
set ct_salesquantity = NULL 
where ct_salesquantity = 0 
AND DD_FORECASTSAMPLE = 'Horizon';

/* Update highpi and lowpi to NULL for dates before holdout date */
UPDATE stage_fosalesforecast f
set ct_highpi = NULL
WHERE ct_highpi = 0
AND dd_forecastsample = 'Train';

UPDATE stage_fosalesforecast f
set ct_lowpi = NULL
WHERE ct_lowpi = 0
AND dd_forecastsample = 'Train';

DELETE FROM stage_fosalesforecast
WHERE ct_forecastquantity > 1000000;



/* Staging data cleaned up --> St. line forecasts have been removed and reranking is done. Each part should have only 1 forecast type for a given rank */
/* Exception: There might still be some st. line forecasts left. For example when all forecast types gave a st. line */

/***************************************************************/
/*** Populate fact from staging table                        ***/
/*  1. Delete all rows with same reporting date as in STG      */
/*  2. Update dimension ids                                    */
/***************************************************************/

DROP TABLE IF EXISTS tmp_maxrptdate;
CREATE TABLE tmp_maxrptdate
as
SELECT DISTINCT TO_DATE(dd_reportingdate,'DD MON YYYY') dd_reportingdate
from stage_fosalesforecast ;

/* Delete rows from fact_fosalesforecast where reporting date matches that in stage_fosalesforecast and re-populate from stage_fosalesforecast */

DELETE FROM fact_fosalesforecast f
WHERE EXISTS ( SELECT 1 FROM tmp_maxrptdate t WHERE TO_DATE(f.dd_reportingdate,'DD MON YYYY') = t.dd_reportingdate );

drop table if exists tmp_saleshistory_grain_reqmonths;
drop table if exists tmp_saleshistory_grain_reqmonths_2;

drop table if exists fact_fosalesforecast_temp;
create table fact_fosalesforecast_temp as
select * from fact_fosalesforecast WHERE 1=2;

alter table fact_fosalesforecast_temp add column dd_forecastdatevalue date default '1900-01-01';

delete from number_fountain m WHERE m.table_name = 'fact_fosalesforecast';

insert into number_fountain
select  'fact_fosalesforecast',
ifnull(max(d.fact_fosalesforecastid),
	ifnull((select min(s.dim_projectsourceid * s.multiplier)
			from dim_projectsource s),0))
from fact_fosalesforecast d
WHERE d.fact_fosalesforecastid <> 1;

insert into fact_fosalesforecast_temp
(
fact_fosalesforecastid,
DD_PARTNUMBER,      --Grain col1
dd_plantcode,       --Grain col2
dd_SALES_COCD,      --Grain col2
dd_REPORTING_COMPANY_CODE,  --Grain col3
dd_home_vs_export,          --Grain col4
dd_HEI_CODE,                --Grain col4
dd_COUNTRY_DESTINATION_CODE,    --Grain col5
DD_MARKET_COUNTRY,              --Grain col6
dd_MARKET_GROUPING,             --Grain col6
dim_partid,
dim_plantid,
dd_reportingdate,
dim_dateidreporting,
dd_forecasttype,
dd_forecastsample,
dd_forecastdate,
dim_dateidforecast,
ct_salesquantity,
ct_forecastquantity,
ct_lowpi,
ct_highpi,
ct_mape,
dd_forecastrank,
dd_holdoutdate,
dd_lastdate,
dd_forecastmode,
dd_forecastdatevalue,
ct_bias_error,
ct_bias_error_rank,
dd_forecastapproach
)
select  (select ifnull(m.max_id, 0) from number_fountain m WHERE m.table_name = 'fact_fosalesforecast') 
+ row_number() over(order by dd_partnumber,dd_SALES_COCD,dd_REPORTING_COMPANY_CODE,dd_HEI_CODE,dd_COUNTRY_DESTINATION_CODE,dd_MARKET_GROUPING,dd_reportingdate,dd_forecastdate,dd_forecasttype) as fact_fosalesforecastid,
ifnull(DD_PARTNUMBER,'Not Set'),
ifnull(dd_SALES_COCD,'Not Set') dd_plantcode,
ifnull(dd_SALES_COCD,'Not Set') dd_SALES_COCD,  /* Dup of dd_plantcode */
ifnull(dd_REPORTING_COMPANY_CODE,'Not Set'),
ifnull(dd_HEI_CODE,'Not Set') dd_home_vs_export,
ifnull(dd_HEI_CODE,'Not Set') dd_HEI_CODE,  /* Dup of dd_home_vs_export */
ifnull(dd_COUNTRY_DESTINATION_CODE,'Not Set'),
ifnull(dd_MARKET_GROUPING,'Not Set') DD_MARKET_COUNTRY,
ifnull(dd_MARKET_GROUPING,'Not Set') dd_MARKET_GROUPING,    /* Dup of DD_MARKET_COUNTRY */
ifnull((select min(dim_partid) from dim_part dp where dp.partnumber = sf.dd_partnumber and dp.plant = sf.dd_SALES_COCD),1) dim_partid,
ifnull((select min(dim_plantid) from dim_plant pl where pl.plantcode = sf.dd_SALES_COCD),1) dim_plantid,
--TO_DATE(sf.dd_reportingdate,'DD MON YYYY') dd_reportingdate, --ifnull(cast(sf.dd_reportingdate as date),'1 Jan 1900'),
sf.dd_reportingdate , 
ifnull((select min(dim_dateid) from dim_date d where d.companycode = 'Not Set' and d.datevalue = TO_DATE(sf.dd_reportingdate,'DD MON YYYY')),1) as dim_dateidreporting,
ifnull(sf.dd_forecasttype,'Not Set'),
ifnull(sf.dd_forecastsample,'Not Set'),
ifnull(sf.dd_forecastdate,1),
1 as dim_dateidforecast,
sf.ct_salesquantity,
sf.ct_forecastquantity,
sf.ct_lowpi,
sf.ct_highpi,
sf.ct_mape,
ifnull(sf.dd_forecastrank,0),
ifnull(sf.dd_holdoutdate,'1'),
ifnull(sf.dd_lastdate,'1'),
ifnull(sf.dd_forecastmode,'Not Set'),
case when sf.dd_forecastdate is null then cast('1900-01-01' as date)
else cast(concat(substring(sf.dd_forecastdate,1,4) , '-' ,
substring(sf.dd_forecastdate,5,2) , '-' ,
substring(sf.dd_forecastdate,7,2) ) as date)
end dd_forecastdatevalue,
ct_bias_error,
ct_bias_error_rank,
dd_forecastapproach
from stage_fosalesforecast sf;

UPDATE fact_fosalesforecast_temp f
SET f.dim_dateidforecast = d.dim_dateid
from (select datevalue,min(dim_dateid) dim_dateid
	        from dim_date d  where d.companycode = 'Not Set' group by datevalue)d,fact_fosalesforecast_temp f
WHERE f.dd_forecastdatevalue = d.datevalue AND f.dim_dateidforecast <> d.dim_dateid;


insert into fact_fosalesforecast
(
fact_fosalesforecastid,
DD_PARTNUMBER,
dd_plantcode,
dd_SALES_COCD,
dd_REPORTING_COMPANY_CODE,
dd_home_vs_export,
dd_HEI_CODE,
dd_COUNTRY_DESTINATION_CODE,
DD_MARKET_COUNTRY,
dd_MARKET_GROUPING,
dim_partid,
dim_plantid,
dd_companycode,
dd_reportingdate,
dim_dateidreporting,
dd_forecasttype,
dd_forecastsample,
dd_forecastdate,
dim_dateidforecast,
ct_salesquantity,
ct_forecastquantity,
ct_lowpi,
ct_highpi,
ct_mape,
dd_forecastrank,
dd_holdoutdate,
dd_lastdate,
dd_forecastmode,
ct_bias_error,
ct_bias_error_rank,
dd_forecastapproach
)
select
fact_fosalesforecastid,
DD_PARTNUMBER,
dd_plantcode,
dd_SALES_COCD,
dd_REPORTING_COMPANY_CODE,
dd_home_vs_export,
dd_HEI_CODE,
dd_COUNTRY_DESTINATION_CODE,
DD_MARKET_COUNTRY,
dd_MARKET_GROUPING,
dim_partid,
dim_plantid,
dd_companycode,
dd_reportingdate,
dim_dateidreporting,
dd_forecasttype,
dd_forecastsample,
dd_forecastdate,
dim_dateidforecast,
ct_salesquantity,
ct_forecastquantity,
ct_lowpi,
ct_highpi,
ct_mape,
dd_forecastrank,
dd_holdoutdate,
dd_lastdate,
dd_forecastmode,
ct_bias_error,
ct_bias_error_rank,
dd_forecastapproach
from fact_fosalesforecast_temp;


/* Format reporting date as DD MON YYYY */
UPDATE fact_fosalesforecast f
set f.dd_reportingdate = to_char(to_date(f.dd_reportingdate,'YYYY-MM-DD') , 'DD MON YYYY')
where f.dd_reportingdate like '%-%-%';

UPDATE fact_fosalesforecast f
SET dd_latestreporting = 'No'
WHERE dd_latestreporting <> 'No';

UPDATE fact_fosalesforecast f
SET dd_latestreporting = 'Yes'
FROM tmp_maxrptdate r,fact_fosalesforecast f
where DATE_TRUNC('month',  TO_DATE(f.dd_reportingdate,'DD MON YYYY')) =DATE_TRUNC('month',  r.dd_reportingdate)
and  dd_forecastapproach is null ;

/* BI-5598 - OZ,GN,MH - Fixes for several columns in Sales Forecast */
UPDATE fact_fosalesforecast f
SET f.dim_dateidforecast = dnew.dim_dateid
from dim_date dold,fact_fosalesforecast f, dim_plant pl, dim_date dnew,tmp_maxrptdate r
WHERE f.dim_dateidforecast = dold.dim_dateid
and dnew.datevalue = dold.datevalue
AND f.dim_plantid = pl.dim_plantid
AND dnew.plantcode_factory = pl.plantcode
AND dnew.companycode = pl.companycode
AND dold.plantcode_factory = 'Not Set'
AND dold.companycode = 'Not Set'
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
AND f.dim_dateidforecast <> dnew.dim_dateid;


update fact_fosalesforecast f
set f.dim_partid = dp.dim_partid
from fact_fosalesforecast f, dim_part dp,tmp_maxrptdate r
where f.dd_partnumber = dp.partnumber
AND CASE WHEN f.dd_plantcode = 'NL10' then 'XX20' ELSE f.dd_plantcode END = dp.plant
AND f.dim_partid = 1
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
AND f.dim_partid <> dp.dim_partid;

update fact_fosalesforecast f
set f.dim_plantid = pl.dim_plantid
from fact_fosalesforecast f, dim_part dp, dim_plant pl,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
AND f.dd_plantcode = 'NL10'
AND dp.plant = pl.plantcode
and dp.plant = 'XX20'
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and f.dim_plantid <> pl.dim_plantid;

update fact_fosalesforecast f
set f.dd_plantcode = pl.plantcode
from fact_fosalesforecast f, dim_plant pl, tmp_maxrptdate r
where f.dim_plantid = pl.dim_plantid
and pl.plantcode = 'XX20'
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and f.dd_plantcode <> pl.plantcode;

update fact_fosalesforecast f
set dd_country_destination_code = trim(dd_country_destination_code)
where dd_country_destination_code <> trim(dd_country_destination_code);

update fact_fosalesforecast f
set dd_market_grouping = trim(dd_market_grouping)
where dd_market_grouping <> trim(dd_market_grouping);

merge into fact_fosalesforecast a
using (
select distinct b.dim_partid, min(b.dd_market_grouping) over (partition by b.dim_partid) as market_grouping
from fact_fosalesforecast b
where b.dd_country_destination_code is not null
) t on a.dim_partid = t.dim_partid
when matched then update set a.dd_market_grouping = t.market_grouping
where a.dd_market_grouping is null and a.dd_country_destination_code is null;
/* BI-5598 - OZ,GN,MH - Fixes for several columns in Sales Forecast */                                   


/***************************************************************/
/*** Post-processing data in fact table                        */
/*  1. Update selling price, q-o-q measures                    */
/*  2. Update customer forecast and related measures           */
/*  3. Any other additional measures                           */
/***************************************************************/


/* Update amt_sellingpriceperunit_gbl for each part-plant pair as avg selling price over last 1 year (from reporting date) */
DROP TABLE IF EXISTS tmp_amt_sellingpriceperunit_gbl;
CREATE TABLE tmp_amt_sellingpriceperunit_gbl
AS
SELECT dp.partnumber,pl.plantcode,avg(fso.amt_UnitPrice * amt_exchangerate_gbl) amt_sellingpriceperunit_gbl_oldmethod,
avg(amt_exchangerate_gbl * amt_UnitPriceUoM/(CASE WHEN fso.ct_PriceUnit <> 0 THEN fso.ct_PriceUnit ELSE 1 END)) amt_sellingpriceperunit_gbl
FROM fact_salesorder fso inner join dim_part dp on dp.dim_partid = fso.dim_partid
INNER JOIN dim_date d on d.dim_dateid = fso.dim_dateidsocreated
left outer join dim_currency tra on tra.dim_currencyid = fso.dim_currencyid_tra
left outer join dim_currency lcl on lcl.dim_currencyid = fso.dim_currencyid
left outer join dim_currency gbl on tra.dim_currencyid = fso.dim_currencyid_gbl
inner join dim_plant pl on pl.dim_plantid = fso.dim_plantid,
tmp_maxrptdate r
WHERE d.datevalue >= r.dd_reportingdate - interval '1' year
AND ct_ScheduleQtySalesUnit > 0
group by dp.partnumber,pl.plantcode;

UPDATE fact_fosalesforecast f
SET f.amt_sellingpriceperunit = t.amt_sellingpriceperunit_gbl
FROM fact_fosalesforecast f,tmp_amt_sellingpriceperunit_gbl t,tmp_maxrptdate r
WHERE f.dd_partnumber = t.partnumber
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
AND f.dd_plantcode = t.plantcode;

DROP TABLE IF EXISTS tmp_qoqmeasures;
CREATE TABLE tmp_qoqmeasures
AS
SELECT f.dd_reportingdate,dd_forecasttype,dd_partnumber,dd_plantcode,dd_market_country,dd_home_vs_export,
CALENDARQUARTERID,sum(CT_SALESQUANTITY) CT_SALESQUANTITY,
sum(CT_FORECASTQUANTITY) CT_FORECASTQUANTITY
--row_number() over(partition by dd_reportingdate,dd_forecasttype,dd_partnumber,dd_plantcode,dd_market_country,dd_home_vs_export 
--order by d.datevalue) qtr_no
FROM fact_fosalesforecast f inner join dim_date d on d.dim_dateid = f.dim_dateidforecast
INNER JOIN tmp_maxrptdate r ON TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
GROUP BY f.dd_reportingdate,dd_forecasttype,dd_partnumber,dd_plantcode,dd_market_country,dd_home_vs_export,CALENDARQUARTERID;

DROP TABLE IF EXISTS tmp_qoqmeasures_2;
CREATE TABLE tmp_qoqmeasures_2
AS
SELECT f.*,row_number() over(partition by dd_reportingdate,dd_forecasttype,dd_partnumber,dd_plantcode,dd_market_country,dd_home_vs_export order by CALENDARQUARTERID) qtr_no
from tmp_qoqmeasures f;

DROP TABLE IF EXISTS tmp_qoqmeasures_3;
CREATE TABLE tmp_qoqmeasures_3
AS 
SELECT DISTINCT f.*,t.CT_SALESQUANTITY CT_SALESQUANTITY_prevqtr,t.CT_FORECASTQUANTITY CT_FORECASTQUANTITY_prevqtr
FROM tmp_qoqmeasures_2 f,tmp_qoqmeasures_2 t
WHERE f.dd_reportingdate = t.dd_reportingdate AND f.dd_forecasttype = t.dd_forecasttype
AND f.dd_partnumber = t.dd_partnumber AND f.dd_plantcode = t.dd_plantcode AND f.dd_market_country = t.dd_market_country AND f.dd_home_vs_export = t.dd_home_vs_export
AND f.qtr_no = t.qtr_no + 1;

UPDATE fact_fosalesforecast f
SET f.CT_SALES_PREVQTR = t.CT_SALESQUANTITY_prevqtr,
f.CT_FORECAST_PREVQTR = t.CT_FORECASTQUANTITY_prevqtr,
f.CT_SALES_CURRENTQTR = t.CT_SALESQUANTITY,
f.CT_FORECAST_CURRENTQTR = t.CT_FORECASTQUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_qoqmeasures_3 t,tmp_maxrptdate r
WHERE f.dd_reportingdate = t.dd_reportingdate
AND f.dd_forecasttype = t.dd_forecasttype
AND f.dd_partnumber = t.dd_partnumber AND f.dd_plantcode = t.dd_plantcode AND f.dd_market_country = t.dd_market_country AND f.dd_home_vs_export = t.dd_home_vs_export
AND f.dim_dateidforecast = d.dim_dateid
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
AND d.CALENDARQUARTERID = t.CALENDARQUARTERID;

/* Update customer forecast */

/*09 Feb 2017 Georgiana changes according to BI-5459*/
/* Horizon Forecast Sample*/
drop table if exists tmp_for_upd_customerquantity;
create table tmp_for_upd_customerquantity as
select distinct fact_fosalesforecastid,dim_dateidforecast,f.dd_reportingdate,d.calendarmonthid as focalendarmonthid,dd_country_destination_code,
year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) as repcalendarmonthid,
f.DD_MARKET_GROUPING,f.dd_partnumber,f.dd_plantcode,f.dd_REPORTING_COMPANY_CODE
FROM fact_fosalesforecast f
inner join tmp_maxrptdate r on TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
inner join dim_date d on f.dim_dateidforecast = d.dim_dateid
where dd_forecastsample='Horizon';

/*This will include only NL10 plant*/
/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
AND f.dd_plantcode='NL10'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='NL10';

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from 
fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;


/*XX20 separate update*/
/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = case when t.PLANT_CODE ='NL10' then 'XX20' end AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and f.dd_plantcode='XX20'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='XX20';

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from 
fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;


/*This update is for USA0 plants*/

/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = 'USA'
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code in ( 'USA0')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE /* AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE */
AND  f.DD_MARKET_GROUPING = 'USA' and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='USA0';

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from 
fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;


drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode not in ( 'NL10','USA0');

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;

drop table if exists tmp_for_upd_customerquantity2;

/*Train and Test Forecast sample*/


/*Train and Test Forecast sample*/
/*This will include NL10 plant*/

drop table if exists tmp_for_upd_customerquantity;
create table tmp_for_upd_customerquantity as
select distinct fact_fosalesforecastid,dim_dateidforecast,f.dd_reportingdate,d.calendarmonthid as focalendarmonthid,dd_country_destination_code,
year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) as repcalendarmonthid,
f.DD_MARKET_GROUPING,f.dd_partnumber,f.dd_plantcode,f.dd_REPORTING_COMPANY_CODE
FROM fact_fosalesforecast f
inner join tmp_maxrptdate r on TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
inner join dim_date d on f.dim_dateidforecast = d.dim_dateid
where dd_forecastsample in ('Train','Test');

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='NL10';

update fact_fosalesforecast f
set f.ct_forecastquantity_customer = t.PRA_QUANTITY 
from tmp_for_upd_customerquantity2 t,fact_fosalesforecast f
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;

/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and dd_forecastsample in ('Train','Test')
and f.dd_plantcode = 'NL10'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/


/*This update is for USA10 plants*/

/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = 'USA'
AND f.dim_dateidforecast = d.dim_dateid 
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code in ( 'USA0')
and dd_forecastsample in ('Train','Test')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/


drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE /* AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE */
AND  f.DD_MARKET_GROUPING = 'USA' and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='USA0';

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from 
fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;



/*This update is for the rest of plants*/

/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid 
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code not in ( 'NL10','USA0')
and dd_forecastsample in ('Train','Test')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/


/*merge into fact_fosalesforecast f
using (select distinct f.fact_fosalesforecastid,t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid 
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code not in ( 'NL10','USA0')
and dd_forecastsample in ('Train','Test')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate) t
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY*/

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode not in ( 'NL10','USA0');

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;

drop table if exists tmp_for_upd_customerquantity2;


/*09 Feb 2017 End of changes*/


DROP TABLE IF EXISTS merck.tmp_custmapes_fosp;
CREATE TABLE merck.tmp_custmapes_fosp
AS
SELECT f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype,
100 * avg(abs((ct_forecastquantity_customer-ct_salesquantity)/ct_salesquantity)) ct_mape_customerfcst
FROM merck.fact_fosalesforecast f,tmp_maxrptdate r
WHERE f.DD_FORECASTSAMPLE = 'Test'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
GROUP BY f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype;


UPDATE merck.fact_fosalesforecast f
SET f.ct_mape_customerfcst = t.ct_mape_customerfcst
FROM merck.fact_fosalesforecast f,merck.tmp_custmapes_fosp t
WHERE f.dd_reportingdate = t.dd_reportingdate AND f.dd_partnumber = t.dd_partnumber AND f.dd_plantcode = t.dd_plantcode
AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND f.DD_HEI_CODE = t.DD_HEI_CODE AND f.DD_MARKET_GROUPING = t.DD_MARKET_GROUPING
AND f.dd_forecasttype = t.dd_forecasttype;

/* Other measures */
update fact_fosalesforecast f set ct_ratio_mape = cT_MAPE_CUSTOMERFCST/ct_mape where ct_mape > 0 and cT_MAPE_CUSTOMERFCST > 0
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') in (select r.dd_reportingdate from tmp_maxrptdate r);
update fact_fosalesforecast f set ct_ratio_mape = 0.01 where ct_ratio_mape = 0
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') in (select r.dd_reportingdate from tmp_maxrptdate r);

DROP TABLE IF EXISTS tmp_ct_salescontribglobal;
CREATE TABLE tmp_ct_salescontribglobal
(
    dd_forecastdate date,
    dd_partnumber varchar(40),
    dd_plantcode varchar(30),
    ct_salescontribglobal decimal(18,4)
);



/* Update NASP measures from fact_atlaspharmlogiforecast_merck */
/*DROP TABLE IF EXISTS tmp_update_nasmpeasures_to_fopsfcst
CREATE TABLE tmp_update_nasmpeasures_to_fopsfcst
AS
SELECT year(d.datevalue) || lpad( month(d.datevalue),2,0)  fcstdate_yyyymm,dp.partnumber dd_partnumber,pl.plantcode dd_plantcode,reporting_company_code dd_REPORTING_COMPANY_CODE,
country_destination_code dd_COUNTRY_DESTINATION_CODE,
market_grouping DD_MARKET_GROUPING, hei_code DD_HEI_CODE,
sum(f.ct_nasp) ct_nasp,sum(f.ct_nasp_cy) ct_nasp_cy,sum(f.ct_nasp_global) ct_nasp_global,
sum(f.ct_nasp_py) ct_nasp_py,sum(f.ct_nasp_fc_cy) ct_nasp_fc_cy,sum(f.ct_nasp_fc_ny) ct_nasp_fc_ny,
sum(f.ct_salesdeliv2mthsnasp) as ct_salesdeliv2mthsnasp
FROM fact_atlaspharmlogiforecast_merck f inner join dim_part dp on dp.dim_partid = f.dim_partid
inner join dim_plant pl on pl.dim_plantid = f.dim_plantid
inner join dim_date d on d.dim_dateid = f.dim_dateidforecast
group by year(d.datevalue) || lpad( month(d.datevalue),2,0),dp.partnumber,pl.plantcode,reporting_company_code,country_destination_code,market_grouping,hei_code*/

/*UPDATE merck.fact_fosalesforecast f
SET f.ct_nasp = t.ct_nasp,
    f.ct_nasp_cy = t.ct_nasp_cy,
    f.ct_nasp_global = t.ct_nasp_global,
    f.ct_nasp_py = t.ct_nasp_py,
    f.ct_nasp_fc_cy = t.ct_nasp_fc_cy,
    f.ct_nasp_fc_ny = t.ct_nasp_fc_ny,
	f.ct_salesdeliv2mthsnasp=t.ct_salesdeliv2mthsnasp
FROM merck.fact_fosalesforecast f, tmp_update_nasmpeasures_to_fopsfcst t,tmp_maxrptdate r,dim_date d
WHERE f.dim_dateidforecast = d.dim_dateid AND d.calendarmonthid = t.fcstdate_yyyymm
AND f.dd_partnumber = t.dd_partnumber AND f.dd_plantcode = t.dd_plantcode
AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND f.DD_HEI_CODE = t.DD_HEI_CODE AND f.DD_MARKET_GROUPING = t.DD_MARKET_GROUPING
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/

/*Georgiana 01 Mar 2017 modified the default of all nasp columns in order to have default 0 and not null constraint, this update is not needed*/
/*UPDATE merck.fact_fosalesforecast f
SET f.ct_nasp = CASE WHEN f.ct_nasp is null then 0 else f.ct_nasp end ,
    f.ct_nasp_cy = CASE WHEN f.ct_nasp_cy is null then 0 else f.ct_nasp_cy end ,
    f.ct_nasp_global = CASE WHEN f.ct_nasp_global is null then 0 else f.ct_nasp_global end ,
    f.ct_nasp_py = CASE WHEN f.ct_nasp_py is null then 0 else f.ct_nasp_py end ,
    f.ct_nasp_fc_cy = CASE WHEN f.ct_nasp_fc_cy is null then 0 else f.ct_nasp_fc_cy end ,
    f.ct_nasp_fc_ny = CASE WHEN f.ct_nasp_fc_ny is null then 0 else f.ct_nasp_fc_ny end 
FROM merck.fact_fosalesforecast f,tmp_maxrptdate r
WHERE TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
AND (f.ct_nasp is null or ct_nasp_cy is null or ct_nasp_global is null or ct_nasp_py is null or ct_nasp_fc_cy is null or ct_nasp_fc_ny is null)*/
/* BI-5395 - OZ,GN,MH : Add PlantTitle and PlantCode for DFA and Report Available */

drop table if exists fact_atlaspharmlogiforecast_merck_for_salesforecast;
create table fact_atlaspharmlogiforecast_merck_for_salesforecast
as select fd.*,
(case when (fd.dd_version = 'SFA' and fd.dim_plantid in (91,124)) or fd.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
dp.partnumber as dd_partnumber, 
(CASE WHEN dp.plant = 'NL10' THEN 'XX20' ELSE dp.plant END) as dd_plant_upd,
d.MonthEndDate as reporting_monthenddate,(CASE WHEN d.plantcode_factory = 'NL10' THEN 'XX20' ELSE d.plantcode_factory END) as reporting_plantcode_factory_upd,
d.companycode as reporting_companycode
from fact_atlaspharmlogiforecast_merck fd, dim_date d, dim_part dp
where fd.dim_dateidreporting = d.dim_dateid
AND fd.dim_partid = dp.dim_partid
and dd_version = 'SFA';

/*merge into fact_fosalesforecast fso
using (
select distinct fso.fact_fosalesforecastid,fd.dd_planttitlemerck,fd.dd_plantcode,fd.dd_reportavailable
from
fact_fosalesforecast fso inner join dim_date d on fso.dim_dateidforecast = d.dim_dateid
inner join fact_atlaspharmlogiforecast_merck_for_salesforecast fd
on fd.dd_partnumber = fso.dd_partnumber
AND fd.dd_plant_upd = fso.dd_plantcode
AND ifnull(fso.DD_MARKET_GROUPING, 'Not Set') = fd.market_grouping 
AND ifnull(fso.dd_COUNTRY_DESTINATION_CODE, 'Not Set') = ifnull(country_destination_code_upd,'Not Set')
AND fd.reporting_MonthEndDate = d.MonthEndDate
AND fd.reporting_plantcode_factory_upd = d.plantcode_factory
AND fd.reporting_companycode = d.companycode
/* where -- fd.dd_partnumber is null and
 d.datevalue < current_date + interval '1' month and 
d.datevalue not between '2013-01-01' AND '2014-12-31' and
CT_FORECASTQUANTITY_CUSTOMER > 1 and
fd.dd_version = 'SFA' 
) t
ON fso.fact_fosalesforecastid = t.fact_fosalesforecastid
WHEN MATCHED THEN UPDATE
SET fso.dd_planttitlemerck = t.dd_planttitlemerck,
fso.dd_plantcodemerck = t.dd_plantcode,
fso.dd_reportavailable = t.dd_reportavailable*/

merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid, f.dd_reportavailable
from fact_atlaspharmlogiforecast_merck_for_salesforecast f, fact_fosalesforecast fos,dim_date dt,dim_part dp, tmp_maxrptdate r 
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(f.country_destination_code_upd,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
and TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dt.calendarmonthid like '2017%'
and dd_version='SFA'
and fos.dd_partnumber='000215'
and fos.DD_PLANTCODEMERCK='Not Set'*/
) t
ON fos.fact_fosalesforecastid = t.fact_fosalesforecastid
WHEN MATCHED THEN UPDATE
SET 
fos.dd_reportavailable = t.dd_reportavailable;

drop table if exists tmp_for_updplanttitle;
create table tmp_for_updplanttitle as
select distinct p.partnumber,pl.plantcode,COUNTRY_DESTINATION_CODE,REPORTING_COMPANY_CODE,dd_planttitlemerck,dd_plantcode from fact_atlaspharmlogiforecast_merck f, dim_part p,dim_plant pl
where f.dim_partid=p.dim_partid
and f.dim_plantid=pl.dim_plantid
and dd_planttitlemerck<>'Not Set';

merge into fact_fosalesforecast f
using (
select distinct f.fact_fosalesforecastid,t.dd_planttitlemerck,t.dd_plantcode
from fact_fosalesforecast f,tmp_for_updplanttitle  t,tmp_maxrptdate r
where f.dd_partnumber=t.partnumber
and f.dd_plantcode=t.plantcode
and f.dd_COUNTRY_DESTINATION_CODE=case when t.plantcode <>'NL10' then 'Not Set' else t.COUNTRY_DESTINATION_CODE end
and f.dd_REPORTING_COMPANY_CODE=t.REPORTING_COMPANY_CODE
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
--and dd_forecasttype='Manual'
/*having count(*)>1*/
 ) t 
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set f.dd_planttitlemerck=ifnull(t.dd_planttitlemerck,'Not Set'),
f.dd_plantcodemerck=t.dd_plantcode;

/* BI-5395 - OZ,GN,MH : Add PlantTitle and PlantCode for DFA and Report Available */

DROP TABLE IF EXISTS tmp_fact_salesorder_12monthsales_1;
create table tmp_fact_salesorder_12monthsales_1(
dd_partnumber varchar(22) not null ,
dd_plantcode varchar(20) not null ,
dd_REPORTING_COMPANY_CODE varchar(200),
dd_COUNTRY_DESTINATION_CODE varchar(200),
DD_HEI_CODE varchar(10),
DD_MARKET_GROUPING varchar(100),
ct_DeliveredQty_YTD decimal(36,6),
amt_shipped decimal(36,6) ,
dd_countpartplant int,
dd_sales_rank int not null 
);

INSERT INTO tmp_fact_salesorder_12monthsales_1
SELECT SALES_UIN dd_partnumber,
SALES_COCD dd_plantcode,
REPORTING_COMPANY_CODE dd_REPORTING_COMPANY_CODE,
COUNTRY_DESTINATION_CODE dd_COUNTRY_DESTINATION_CODE,
HEI_CODE DD_HEI_CODE,
MARKET_GROUPING DD_MARKET_GROUPING,
sum(BUOM_QUANTITY) ct_DeliveredQty_YTD,
cast(0 as decimal(12,4)) amt_shipped,
cast(0 as decimal(12,4)) dd_countpartplant,
0 dd_sales_rank
FROM merck.atlas_forecast_sales_merck_DC
WHERE SALES_REPORTING_PERIOD is not null and SALES_UIN is not null
AND year(to_date(to_char(SALES_REPORTING_PERIOD)||'01','YYYYMMDD')) >= year(current_date)
group by SALES_UIN,SALES_COCD,REPORTING_COMPANY_CODE,COUNTRY_DESTINATION_CODE,HEI_CODE,MARKET_GROUPING;

/* Avg price is calculated at part level*/
DROP TABLE IF EXISTS tmp_avg_sp_fromfso;
CREATE TABLE tmp_avg_sp_fromfso 
AS
SELECT
dp.partnumber dd_partnumber,
avg(f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl) amt_avgprice
FROM fact_salesorder f_so 
INNER JOIN dim_part dp on dp.dim_partid = f_so.dim_partid
INNER JOIN dim_date spdd on spdd.dim_dateid = f_so.DIM_DATEIDSALESORDERCREATED --f_so.dim_dateidshipmentdelivery
WHERE year(spdd.datevalue) >= year(current_date) - 1
GROUP BY dp.partnumber;

UPDATE tmp_fact_salesorder_12monthsales_1 f
SET f.amt_shipped = ct_DeliveredQty_YTD * t.amt_avgprice
FROM tmp_fact_salesorder_12monthsales_1 f, tmp_avg_sp_fromfso t
WHERE f.dd_partnumber = t.dd_partnumber;

DROP TABLE IF EXISTS tmp_upd_dd_sales_rank;
CREATE TABLE tmp_upd_dd_sales_rank
as
select t.*,rank() over(order by amt_shipped desc) dd_rank
from tmp_fact_salesorder_12monthsales_1 t;

UPDATE tmp_fact_salesorder_12monthsales_1 t1
SET t1.dd_sales_rank = t.dd_rank
FROM tmp_upd_dd_sales_rank t,tmp_fact_salesorder_12monthsales_1 t1
WHERE t.dd_partnumber = t1.dd_partnumber
AND t.dd_plantcode = t1.dd_plantcode
AND t.dd_REPORTING_COMPANY_CODE = t1.dd_REPORTING_COMPANY_CODE
AND t.dd_COUNTRY_DESTINATION_CODE = t1.dd_COUNTRY_DESTINATION_CODE
AND t.DD_HEI_CODE = t1.DD_HEI_CODE
AND t.DD_MARKET_GROUPING = t1.DD_MARKET_GROUPING;


UPDATE tmp_fact_salesorder_12monthsales_1
SET dd_countpartplant = (SELECT max(dd_sales_rank)
FROM tmp_fact_salesorder_12monthsales_1);

DROP TABLE IF EXISTS tmp_fact_salesorder_12monthsales;
CREATE TABLE tmp_fact_salesorder_12monthsales
AS
SELECT t1.*,
case WHEN (dd_sales_rank/dd_countpartplant) < 0.25 THEN 1
     WHEN (dd_sales_rank/dd_countpartplant) >= 0.25 and (dd_sales_rank/dd_countpartplant) < 0.5 THEN 2
     WHEN (dd_sales_rank/dd_countpartplant) >= 0.5 and (dd_sales_rank/dd_countpartplant) < 0.75 THEN 3
     ELSE 4 END
dd_salescontribution_quartile_rank
FROM tmp_fact_salesorder_12monthsales_1 t1;

UPDATE fact_fosalesforecast f
SET f.dd_salescontribution_quartile_rank = t.dd_salescontribution_quartile_rank,
f.amt_shippedqtyytd = t.amt_shipped,
f.ct_DeliveredQty_YTD = t.ct_DeliveredQty_YTD
FROM fact_fosalesforecast f,tmp_fact_salesorder_12monthsales t,tmp_maxrptdate r
WHERE f.dd_partnumber = t.dd_partnumber
AND f.dd_plantcode = t.dd_plantcode
AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND f.DD_HEI_CODE = t.DD_HEI_CODE
AND f.DD_MARKET_GROUPING = t.DD_MARKET_GROUPING
AND r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY');


/* Partnumber was not found in fact_salesorder for sales in last 1 year */
UPDATE fact_fosalesforecast f
SET f.dd_salescontribution_quartile_rank = -1
WHERE f.dd_salescontribution_quartile_rank IS NULL;

/* Clean up data */
/* Remove part numbers that were not found. e.g if input data was from prd and fcst is in stg, then there will be some parts that are not found */
/*
DELETE from fact_fosalesforecast
where dd_latestreporting = 'Yes'
and dim_partid = 1*/

UPDATE fact_fosalesforecast
SET dd_forecastquantity = round(ct_forecastquantity,0);

DROP TABLE IF EXISTS tmp_fact_salesorder_12monthsales_1;
create table tmp_fact_salesorder_12monthsales_1(
dd_partnumber varchar(22) not null ,
dd_plantcode varchar(20) not null ,
dd_REPORTING_COMPANY_CODE varchar(200),
dd_COUNTRY_DESTINATION_CODE varchar(200),
DD_HEI_CODE varchar(10),
DD_MARKET_GROUPING varchar(100),
ct_DeliveredQty_YTD decimal(36,6),
amt_shipped decimal(36,6) ,
dd_countpartplant int,
dd_sales_rank int not null 
);

INSERT INTO tmp_fact_salesorder_12monthsales_1
SELECT SALES_UIN dd_partnumber,
SALES_COCD dd_plantcode,
REPORTING_COMPANY_CODE dd_REPORTING_COMPANY_CODE,
case when SALES_COCD NOT IN ( 'NL10','USA0')  then 'Not Set' else COUNTRY_DESTINATION_CODE end dd_COUNTRY_DESTINATION_CODE,
case when SALES_COCD NOT IN ( 'NL10','USA0')  then 'Not Set' else HEI_CODE end  DD_HEI_CODE,
MARKET_GROUPING DD_MARKET_GROUPING,
sum(BUOM_QUANTITY) ct_DeliveredQty_YTD,
cast(0 as decimal(12,4)) amt_shipped,
cast(0 as decimal(12,4)) dd_countpartplant,
0 dd_sales_rank
FROM merck.atlas_forecast_sales_merck_DC
WHERE SALES_REPORTING_PERIOD is not null and SALES_UIN is not null
AND SALES_REPORTING_PERIOD <= to_char(current_date-interval '1' month,'YYYYMM')
AND SALES_REPORTING_PERIOD >= to_char(current_date-interval '1' year,'YYYYMM')
group by SALES_UIN,SALES_COCD,REPORTING_COMPANY_CODE,case when SALES_COCD NOT IN ( 'NL10','USA0')  then 'Not Set' else COUNTRY_DESTINATION_CODE end,case when SALES_COCD NOT IN ( 'NL10','USA0')  then 'Not Set' else HEI_CODE end,MARKET_GROUPING;


/* Avg price is calculated at part level*/
DROP TABLE IF EXISTS tmp_avg_sp_fromfso;
CREATE TABLE tmp_avg_sp_fromfso 
AS
SELECT
dp.partnumber dd_partnumber,
avg(f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END) * amt_exchangerate_gbl) amt_avgprice
FROM fact_salesorder f_so 
INNER JOIN dim_part dp on dp.dim_partid = f_so.dim_partid
INNER JOIN dim_date spdd on spdd.dim_dateid = f_so.DIM_DATEIDSALESORDERCREATED --f_so.dim_dateidshipmentdelivery
WHERE year(spdd.datevalue) >= year(current_date) - 1 /* To have more no. of parts, check current yr + last years data */
GROUP BY dp.partnumber;

UPDATE tmp_fact_salesorder_12monthsales_1 f
SET f.amt_shipped = ct_DeliveredQty_YTD * t.amt_avgprice
FROM tmp_fact_salesorder_12monthsales_1 f, tmp_avg_sp_fromfso t
WHERE f.dd_partnumber = t.dd_partnumber;

DROP TABLE IF EXISTS tmp_upd_dd_sales_rank;
CREATE TABLE tmp_upd_dd_sales_rank
as
select t.*,rank() over(order by amt_shipped desc) dd_rank
from tmp_fact_salesorder_12monthsales_1 t;

UPDATE tmp_fact_salesorder_12monthsales_1 t1
SET t1.dd_sales_rank = t.dd_rank
FROM tmp_upd_dd_sales_rank t,tmp_fact_salesorder_12monthsales_1 t1
WHERE t.dd_partnumber = t1.dd_partnumber
AND t.dd_plantcode = t1.dd_plantcode
AND t.dd_REPORTING_COMPANY_CODE = t1.dd_REPORTING_COMPANY_CODE
AND t.dd_COUNTRY_DESTINATION_CODE = t1.dd_COUNTRY_DESTINATION_CODE
AND t.DD_HEI_CODE = t1.DD_HEI_CODE
AND t.DD_MARKET_GROUPING = t1.DD_MARKET_GROUPING;


UPDATE tmp_fact_salesorder_12monthsales_1
SET dd_countpartplant = (SELECT max(dd_sales_rank)
FROM tmp_fact_salesorder_12monthsales_1);

DROP TABLE IF EXISTS tmp_fact_salesorder_12monthsales;
CREATE TABLE tmp_fact_salesorder_12monthsales
AS
SELECT t1.*,
case WHEN (dd_sales_rank/dd_countpartplant) < 0.25 THEN 1
     WHEN (dd_sales_rank/dd_countpartplant) >= 0.25 and (dd_sales_rank/dd_countpartplant) < 0.5 THEN 2
     WHEN (dd_sales_rank/dd_countpartplant) >= 0.5 and (dd_sales_rank/dd_countpartplant) < 0.75 THEN 3
     ELSE 4 END
dd_salescontribution_quartile_rank,
case    WHEN (dd_sales_rank/dd_countpartplant) < 0.333 THEN 1
    WHEN (dd_sales_rank/dd_countpartplant) >= 0.333 and (dd_sales_rank/dd_countpartplant) < 0.667 THEN 2
    ELSE 3 END
dd_salescontribution_3levels_rank
FROM tmp_fact_salesorder_12monthsales_1 t1;

UPDATE fact_fosalesforecast  f
SET f.dd_salescontribution_quartile_rank = NULL,f.dd_salescontribution_3levels_rank = NULL,
f.amt_shippedqtyytd = 0,f.ct_DeliveredQty_YTD = 0
FROM fact_fosalesforecast f,tmp_maxrptdate r
WHERE r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY');

UPDATE fact_fosalesforecast f
SET f.dd_salescontribution_quartile_rank = t.dd_salescontribution_quartile_rank,
f.amt_shippedqtyytd = t.amt_shipped,
f.ct_DeliveredQty_YTD = t.ct_DeliveredQty_YTD,
f.dd_salescontribution_3levels_rank = t.dd_salescontribution_3levels_rank
FROM fact_fosalesforecast f,tmp_fact_salesorder_12monthsales t,tmp_maxrptdate r
WHERE f.dd_partnumber = t.dd_partnumber
AND f.dd_plantcode = t.dd_plantcode
AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND f.DD_HEI_CODE = t.DD_HEI_CODE
AND f.DD_MARKET_GROUPING = t.DD_MARKET_GROUPING
AND r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY');


DROP TABLE IF EXISTS tmp_upd_ct_volatility_yoy_percent;
CREATE TABLE tmp_upd_ct_volatility_yoy_percent
AS
SELECT dd_partnumber,d.CALENDARYEAR,sum(CASE WHEN dd_forecastsample in ('Train','Test') THEN (ct_salesquantity) ELSE (ct_forecastquantity) END) ct_salesquantity
from fact_fosalesforecast f inner join dim_date d on d.dim_dateid = f.dim_dateidforecast
inner join tmp_maxrptdate r on r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
WHERE dd_forecastrank = 1
AND d.CALENDARYEAR <= year(current_date)
GROUP BY dd_partnumber,d.CALENDARYEAR;


DROP TABLE IF EXISTS tmp_upd_ct_volatility_yoy_percent_STDDEV;
CREATE TABLE tmp_upd_ct_volatility_yoy_percent_STDDEV
AS
SELECT dd_partnumber,stddev(ct_salesquantity) ct_salesquantity_yoy_sd,avg(ct_salesquantity) ct_salesquantity_yoy_avg,
100 * stddev(ct_salesquantity)/avg(ct_salesquantity) ct_volatility_yoy_percent
from tmp_upd_ct_volatility_yoy_percent
group by dd_partnumber;


UPDATE fact_fosalesforecast f
SET f.ct_volatility_yoy_percent = t.ct_volatility_yoy_percent, dd_volatility_yoy_gt50pc = 'Y' 
FROM fact_fosalesforecast f, tmp_upd_ct_volatility_yoy_percent_STDDEV t
WHERE f.dd_partnumber = t.dd_partnumber;

UPDATE fact_fosalesforecast f
SET dd_volatility_yoy_gt50pc =  CASE WHEN f.ct_volatility_yoy_percent >= 50 THEN 'Y' ELSE 'N' END ;

DROP TABLE IF EXISTS tmp_upd_ct_volatility_yoy_percent_overall;
CREATE TABLE tmp_upd_ct_volatility_yoy_percent_overall
AS
SELECT d.CALENDARYEAR,sum(ct_salesquantity) ct_salesquantity
from fact_fosalesforecast f inner join dim_date d on d.dim_dateid = f.dim_dateidforecast
inner join tmp_maxrptdate r on r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
GROUP BY d.CALENDARYEAR;

/* Update ct_coeffofvariation */
DROP TABLE IF EXISTS tmp_monthlysales_cov;
CREATE TABLE tmp_monthlysales_cov
AS
SELECT  DD_PARTNUMBER,dd_SALES_COCD,dd_REPORTING_COMPANY_CODE,dd_HEI_CODE,dd_COUNTRY_DESTINATION_CODE,dd_MARKET_GROUPING,
stddev(CT_SALESQUANTITY) CT_SALESQUANTITY_stddev,avg(CT_SALESQUANTITY) CT_SALESQUANTITY_avg,
stddev(CT_SALESQUANTITY)/avg(CT_SALESQUANTITY) ct_coeffofvariation
FROM
(
SELECT distinct DD_PARTNUMBER,dd_SALES_COCD,dd_REPORTING_COMPANY_CODE,dd_HEI_CODE,dd_COUNTRY_DESTINATION_CODE,dd_MARKET_GROUPING,
d.datevalue dd_forecastdate,CT_SALESQUANTITY,f.dd_reportingdate,dd_forecasttype
from fact_fosalesforecast f inner join dim_date d on d.dim_dateid = f.dim_dateidforecast
inner join tmp_maxrptdate r on r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
WHERE dd_latestreporting = 'Yes'
AND dd_forecastsample in ('Train','Test')
AND CT_SALESQUANTITY is not null
AND dd_forecastrank = 1) t
GROUP BY DD_PARTNUMBER,dd_SALES_COCD,dd_REPORTING_COMPANY_CODE,dd_HEI_CODE,dd_COUNTRY_DESTINATION_CODE,dd_MARKET_GROUPING
HAVING avg(CT_SALESQUANTITY) > 0;

UPDATE fact_fosalesforecast f
SET f.ct_coeffofvariation = ifnull(c.ct_coeffofvariation,0)
FROM tmp_monthlysales_cov c,fact_fosalesforecast f,tmp_maxrptdate r
WHERE f.DD_PARTNUMBER = c.DD_PARTNUMBER
AND ifnull(f.dd_SALES_COCD,'xxx') = ifnull(c.dd_SALES_COCD,'xxx')
AND ifnull(f.dd_REPORTING_COMPANY_CODE,'xxx') = ifnull(c.dd_REPORTING_COMPANY_CODE,'xxx')
AND ifnull(f.dd_HEI_CODE,'xxx') = ifnull(c.dd_HEI_CODE,'xxx')
AND ifnull(f.dd_COUNTRY_DESTINATION_CODE,'xxx') = ifnull(c.dd_COUNTRY_DESTINATION_CODE,'xxx')
AND ifnull(f.dd_MARKET_GROUPING,'xxx') = ifnull(c.dd_MARKET_GROUPING,'xxx')
AND dd_latestreporting = 'Yes'
AND r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
AND ifnull(f.ct_coeffofvariation,-1) <> ifnull(c.ct_coeffofvariation,0);

/* Update coeffovfariation_forecast */
UPDATE fact_fosalesforecast f
SET f.ct_coeffofvariation_forecast = c.ct_variationcoeff
FROM tmp_forecastcov c,fact_fosalesforecast f,tmp_maxrptdate r
WHERE f.DD_PARTNUMBER = c.DD_PARTNUMBER
AND ifnull(f.dd_SALES_COCD,'xxx') = ifnull(c.dd_SALES_COCD,'xxx')
AND ifnull(f.dd_REPORTING_COMPANY_CODE,'xxx') = ifnull(c.dd_REPORTING_COMPANY_CODE,'xxx')
AND ifnull(f.dd_HEI_CODE,'xxx') = ifnull(c.dd_HEI_CODE,'xxx')
AND ifnull(f.dd_COUNTRY_DESTINATION_CODE,'xxx') = ifnull(c.dd_COUNTRY_DESTINATION_CODE,'xxx')
AND ifnull(f.dd_MARKET_GROUPING,'xxx') = ifnull(c.dd_MARKET_GROUPING,'xxx')
AND f.dd_forecasttype = c.dd_forecasttype
AND r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
AND c.dd_reportingdate = f.dd_reportingdate
AND f.dd_latestreporting = 'Yes';

DROP TABLE IF EXISTS tmp_upd_forecastproductcategory;
CREATE TABLE tmp_upd_forecastproductcategory
AS
SELECT t.*,ceiling(MONTHS_BETWEEN(dd_lastdate , min_forecastdate)) ct_diff_holdout_minus_minfcst
from (
SELECT DD_PARTNUMBER,dd_plantcode,dd_SALES_COCD,dd_REPORTING_COMPANY_CODE,dd_home_vs_export,dd_HEI_CODE,dd_COUNTRY_DESTINATION_CODE,dd_MARKET_GROUPING,
f.dd_reportingdate,to_date(dd_lastdate,'DD MON YYYY') dd_lastdate,min(d.datevalue) min_forecastdate
FROM fact_fosalesforecast f inner join dim_date d on d.dim_dateid = f.dim_dateidforecast
inner join tmp_maxrptdate r on r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
WHERE f.dd_latestreporting = 'Yes'
AND f.ct_salesquantity > 0
GROUP BY DD_PARTNUMBER,dd_plantcode,dd_SALES_COCD,dd_REPORTING_COMPANY_CODE,dd_home_vs_export,dd_HEI_CODE,dd_COUNTRY_DESTINATION_CODE,dd_MARKET_GROUPING,
f.dd_reportingdate,to_date(dd_lastdate,'DD MON YYYY')) t;

UPDATE fact_fosalesforecast f
SET f.dd_forecastproductcategory =  CASE WHEN t.ct_diff_holdout_minus_minfcst < 24 THEN 'New Launch' ELSE 'Established Product' END
FROM fact_fosalesforecast f,tmp_upd_forecastproductcategory t,tmp_maxrptdate r
WHERE f.DD_PARTNUMBER = t.DD_PARTNUMBER AND f.dd_plantcode = t.dd_plantcode
AND f.dd_SALES_COCD = t.dd_SALES_COCD AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE
AND f.dd_home_vs_export = t.dd_home_vs_export AND f.dd_HEI_CODE = t.dd_HEI_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
AND f.dd_MARKET_GROUPING = t.dd_MARKET_GROUPING AND f.dd_reportingdate = t.dd_reportingdate;


DROP TABLE IF EXISTS tmp_upd_forecastproductcategory_2;
CREATE TABLE tmp_upd_forecastproductcategory_2
AS
SELECT DD_PARTNUMBER,dd_plantcode,dd_SALES_COCD,dd_REPORTING_COMPANY_CODE,dd_home_vs_export,dd_HEI_CODE,dd_COUNTRY_DESTINATION_CODE,dd_MARKET_GROUPING,
f.dd_reportingdate,to_date(dd_lastdate,'DD MON YYYY') dd_lastdate,max(CT_FORECASTQUANTITY_CUSTOMER) max_forecast_in_first3horizonmonths,
sum(CT_FORECASTQUANTITY_CUSTOMER) sum_forecast_in_first3horizonmonths,min(CT_FORECASTQUANTITY_CUSTOMER) min_forecast_in_first3horizonmonths
FROM fact_fosalesforecast f inner join dim_date d on d.dim_dateid = f.dim_dateidforecast
inner join tmp_maxrptdate r on r.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
WHERE f.dd_latestreporting = 'Yes'
AND f.dd_forecastsample = 'Horizon'
AND MONTHS_BETWEEN(d.datevalue,to_date(dd_lastdate,'DD MON YYYY')) <= 3
AND f.dd_forecastrank = 1
GROUP BY DD_PARTNUMBER,dd_plantcode,dd_SALES_COCD,dd_REPORTING_COMPANY_CODE,dd_home_vs_export,dd_HEI_CODE,dd_COUNTRY_DESTINATION_CODE,dd_MARKET_GROUPING,
f.dd_reportingdate,to_date(dd_lastdate,'DD MON YYYY')
HAVING max(CT_FORECASTQUANTITY_CUSTOMER) = 0;

UPDATE fact_fosalesforecast f
SET f.dd_forecastproductcategory =  'Potential Phase out'
FROM fact_fosalesforecast f,tmp_upd_forecastproductcategory_2 t
WHERE f.DD_PARTNUMBER = t.DD_PARTNUMBER AND f.dd_plantcode = t.dd_plantcode
AND f.dd_SALES_COCD = t.dd_SALES_COCD AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE
AND f.dd_home_vs_export = t.dd_home_vs_export AND f.dd_HEI_CODE = t.dd_HEI_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND f.dd_MARKET_GROUPING = t.dd_MARKET_GROUPING AND f.dd_reportingdate = t.dd_reportingdate;

/* New HEI Code changes - 20 Dec */
update fact_fosalesforecast set dd_heicode_new = DD_HEI_CODE where DD_SALES_COCD = 'NL10' AND dd_heicode_new <> DD_HEI_CODE;
update fact_fosalesforecast set dd_heicode_new = 'Not Set' where DD_SALES_COCD <> 'NL10' AND dd_heicode_new <> 'Not Set' AND dd_latestreporting = 'Yes';

/* BI-5395 - OZ : Adapt grain for Sales Quantity for all plants different than NL10 to ignore market group */
/* update fact_fosalesforecast
set dd_country_destination_code = 'Not Set'
where DD_SALES_COCD not in ('NL10','XX20')
and dd_country_destination_code <> 'Not Set'


drop table if exists fosalesforecast_updateSalesQuantity
create table fosalesforecast_updateSalesQuantity AS
select substring(dd_forecastdate,1,6) as dd_forecastdate,dd_forecastrank, dd_forecastsample,dd_reportingdate,f.DD_PARTNUMBER,f.DD_SALES_COCD,DD_MARKET_GROUPING,DD_REPORTING_COMPANY_CODE,
avg(DD_FORECASTQUANTITY) as old_DD_FORECASTQUANTITY,
sum(DD_FORECASTQUANTITY) as new_DD_FORECASTQUANTITY
from fact_fosalesforecast f
 where f.DD_SALES_COCD not in ('NL10','XX20')
and DD_PARTNUMBER = '141166'
 AND DD_SALES_COCD = 'CZ20'
and dd_forecastrank = '1'
and dd_forecastsample = 'Test'
 AND dd_reportingdate = '12 Dec 2016'
group by substring(dd_forecastdate,1,6),dd_forecastrank, dd_forecastsample,dd_reportingdate,f.DD_PARTNUMBER,f.DD_SALES_COCD,DD_MARKET_GROUPING,DD_REPORTING_COMPANY_CODE

update fact_fosalesforecast fo
set fo.dd_forecastquantity = CASE WHEN fo.dd_flag_salesquantity_editable = 'oldrows' THEN temp.old_DD_FORECASTQUANTITY ELSE temp.new_DD_FORECASTQUANTITY END
from fact_fosalesforecast fo 
inner join fosalesforecast_updateSalesQuantity temp on temp.dd_forecastdate = substring(fo.dd_forecastdate,1,6)
and fo.dd_forecastrank = temp.dd_forecastrank
and fo.dd_forecastsample = temp.dd_forecastsample
and fo.dd_reportingdate = temp.dd_reportingdate
and fo.DD_PARTNUMBER = temp.DD_PARTNUMBER
and fo.DD_SALES_COCD = temp.DD_SALES_COCD
and fo.dd_market_grouping = temp.dd_market_grouping
and fo.DD_REPORTING_COMPANY_CODE = temp.DD_REPORTING_COMPANY_CODE
and fo.dd_forecastquantity <> CASE WHEN fo.dd_flag_salesquantity_editable = 'oldrows' THEN temp.old_DD_FORECASTQUANTITY ELSE temp.new_DD_FORECASTQUANTITY END

update fact_fosalesforecast
set dd_flag_salesquantity_editable = 'oldrows'
where dd_flag_salesquantity_editable <> 'oldrows'

drop table if exists fosalesforecast_updateSalesQuantity */
/* BI-5395 - OZ : Adapt grain for Sales Quantity for all plants different than NL10 to ignore market group */


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,dd_market_grouping,
sum(f.ct_nasp) as ct_nasp,
sum(f.ct_nasp_cy) as ct_nasp_cy,
sum(f.ct_nasp_global) as ct_nasp_global,
sum(f.ct_nasp_py) as ct_nasp_py,
sum(f.ct_nasp_fc_cy) as ct_nasp_fc_cy,
sum(f.ct_nasp_fc_ny) as ct_nasp_fc_ny,
SUM(f.ct_salesdeliv2mthsnasp) as ct_salesdeliv2mthsnasp,
dt.calendarmonthid 
from  fact_atlaspharmlogiforecast_merck f, fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate m
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull((case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end),'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
AND m.dd_reportingdate = TO_DATE(fos.dd_reportingdate,'DD MON YYYY')
and f.market_grouping = fos.dd_market_grouping
and dd_version='SFA'
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,dd_market_grouping,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 
fos.ct_nasp = t.ct_nasp,
fos.ct_nasp_cy=t.ct_nasp_cy,
fos.ct_nasp_global=t.ct_nasp_global,
fos.ct_nasp_py=t.ct_nasp_py,
fos.ct_nasp_fc_cy=t.ct_nasp_fc_cy,
fos.ct_nasp_fc_ny=t.ct_nasp_fc_ny,
fos.ct_salesdeliv2mthsnasp=t.ct_salesdeliv2mthsnasp;

/*BI-4363 End*/
/*28 Feb 2017 End of Changes*/

/* 28 Feb 2017 Georgiana add Sales Delivered PY from DF SA according to BI-5589*/

/*new logic*/
drop table if exists tmp_for_salesdeliveredPY;
create table tmp_for_salesdeliveredPY as
select distinct f.dim_partid,f.dim_plantid,COUNTRY_DESTINATION_CODE,REPORTING_COMPANY_CODE,dim_dateidreporting,f.ct_salesdeliveredPY  from fact_atlaspharmlogiforecast_merck f, dim_part p,dim_plant pl
where f.dim_partid=p.dim_partid
and f.dim_plantid=pl.dim_plantid;


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
SUM(f.ct_salesdeliveredPY) as ct_salesdeliveredPY,
dt.calendarmonthid 
from tmp_for_salesdeliveredPY f,fact_fosalesforecast fos,dim_date dt,dim_part dp,dim_plant pl, tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
and f.dim_plantid=pl.dim_plantid
and f.dim_plantid=fos.dim_plantid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(case when pl.plantcode <>'NL10' then 'Not Set' else f.COUNTRY_DESTINATION_CODE end,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*and dp.partnumber ='119977' and dp.plant='IE20'and dt.calendarmonthid like '2016%'*/
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set fos.ct_salesdeliveredPY=t.ct_salesdeliveredPY;

/*BI-5589 End*/

/* BI-4363 corrected NASP Update and calculated Sales Amount from Demand forecast in order to have the same aggregation between the two SA's*/
drop table if exists tmp_for_upd_salesdeliveredmonth;
create table tmp_for_upd_salesdeliveredmonth as
select f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
CASE WHEN 
MAX( (f.dd_reportavailable) ) = 'Yes' 
THEN 
SUM((f.ct_salesmonth1) * (f.ct_nasp)) 
ELSE 
NULL 
END as col1,
sum(f.ct_salesmonth1) as col2,sum(f.ct_nasp) as col3 from fact_atlaspharmlogiforecast_merck f,dim_part dp,dim_Date dt
where f.dim_partid=dp.dim_partid 
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dt.datevalue='2016-02-01'*/
and dim_dateidreporting=dt.dim_dateid
and  dd_version='SFA'
group by f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end);


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
sum(f.col1) as col1,
dt.calendarmonthid 
from tmp_for_upd_salesdeliveredmonth f, fact_fosalesforecast fos,dim_date dt,dim_part dp, tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(f.country_destination_code_upd,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
and TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dd_version='SFA'
and dt.calendarmonthid like '2016%'
and dd_reportingdate='08 Feb 2017'
and dd_forecastrank='1'
and dd_forecastdate='20160229'*/
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 
fos.ct_salesdeliveredmonth = ifnull(t.col1,0);




/*06 Mar 2017 Georgiaan changes according to BI-5640 adding dd_forecastrank2 as an editable attribute*/

merge into fact_fosalesforecast fos 
using (
select distinct fact_fosalesforecastid,
dd_forecastrank as dd_forecastrank2
from fact_fosalesforecast f,tmp_maxrptdate r
where TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set fos.dd_forecastrank2=t.dd_forecastrank2;

/* 06 Mar 2017 End of changes*/

/*Georgiana BI 5717 Changes*/


/*Start inserting missing data from Demand Forecast SA with 'Manual' forecast type, we are using DTA version in order to have two years of forecast*/
drop table if exists fact_atlaspharmlogiforecast_merck_for_salesforecast;
create table fact_atlaspharmlogiforecast_merck_for_salesforecast
as select fd.*,
(case when fd.dim_plantid in (91,124) and fd.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
dp.partnumber as dd_partnumber, 
(CASE WHEN dp.plant = 'NL10' THEN 'XX20' ELSE dp.plant END) as dd_plant_upd,
d.MonthEndDate as reporting_monthenddate,(CASE WHEN d.plantcode_factory = 'NL10' THEN 'XX20' ELSE d.plantcode_factory END) as reporting_plantcode_factory_upd,
d.companycode as reporting_companycode
from fact_atlaspharmlogiforecast_merck fd, dim_date d, dim_part dp
where fd.dim_dateidreporting = d.dim_dateid
AND fd.dim_partid = dp.dim_partid
and dd_version = 'DTA';

/* 06 Jun 2018 Georgiana Changes: commented out the condition on forecast date as we don't want to insert Manual inserts for materials that have a PF run and added a rank condition in order to optimize the statement*/
drop table if exists tmp_for_insert_fopssales;
create table tmp_for_insert_fopssales as select distinct a.dim_partid,a.dim_plantid,a.dim_dateidreporting,a.dim_dateidforecast,a.country_destination_code_upd,a.reporting_company_code,b.dd_reportingdate
from fact_fosalesforecast b,dim_date dt,fact_atlaspharmlogiforecast_merck_for_salesforecast a,tmp_maxrptdate m
 where  a.dim_partid=b.dim_partid
and a.dim_plantid=b.dim_plantid
AND a.dim_dateidreporting = dt.dim_dateid  
--AND year(to_date(to_char(b.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(b.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(a.country_destination_code_upd,'Not Set') = ifnull(b.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(a.reporting_company_code,'Not Set')=ifnull(b.dd_reporting_company_code,'Not Set')
and m.dd_reportingdate = TO_DATE(b.dd_reportingdate,'DD MON YYYY') and dd_version='DTA'
and dd_forecastrank='1'
;



delete from number_fountain m WHERE m.table_name = 'fact_fosalesforecast';

insert into number_fountain
select  'fact_fosalesforecast',
ifnull(max(d.fact_fosalesforecastid),
	ifnull((select min(s.dim_projectsourceid * s.multiplier)
			from dim_projectsource s),0))
from fact_fosalesforecast d
WHERE d.fact_fosalesforecastid <> 1;


insert into fact_fosalesforecast (fact_fosalesforecastid,dim_partid,dim_plantid,dim_dateidforecast,dd_reportingdate,dd_country_destination_code,dd_reporting_company_code,dd_insertsource,dd_forecasttype,dd_market_grouping)
select 
(select ifnull(m.max_id, 0) from number_fountain m WHERE m.table_name = 'fact_fosalesforecast') 
+ row_number() over(order by '') as fact_fosalesforecastid,
t.* 
from (
select distinct a.dim_partid,a.dim_plantid,a.dim_dateidreporting,to_char(to_date(m.dd_reportingdate, 'YYYY-MM-DD'),'DD Mon YYYY'),a.country_destination_code_upd,a.reporting_company_code,'Manual','Manual',market_grouping--,
 from fact_atlaspharmlogiforecast_merck_for_salesforecast a,tmp_maxrptdate m
where not exists (select 1 from tmp_for_insert_fopssales b
where 
a.dim_partid=b.dim_partid and a.dim_plantid=b.dim_plantid and a.dim_dateidreporting=b.dim_dateidreporting and 
 a.country_destination_code_upd=b.country_destination_code_upd and a.reporting_company_code =b.reporting_company_code
and dd_version='DTA'
 and m.dd_reportingdate = TO_DATE(b.dd_reportingdate,'DD MON YYYY')
)) t; 


merge into fact_fosalesforecast f
using (select distinct fact_fosalesforecastid,min(dim_dateid) as dim_dateid
from dim_date d,fact_fosalesforecast sf,tmp_maxrptdate m
where d.companycode = 'Not Set' 
and d.datevalue = TO_DATE(sf.dd_reportingdate,'DD MON YYYY')
and dd_insertsource='Manual'
and m.dd_reportingdate = TO_DATE(sf.dd_reportingdate,'DD MON YYYY')
group by fact_fosalesforecastid ) t
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set f.dim_dateidreporting=ifnull(t.dim_dateid,1);

merge into fact_fosalesforecast f
using (select distinct fact_fosalesforecastid,min( cast(concat(year(d.datevalue),case when length(month(d.datevalue))=1 then concat('0',month(d.datevalue)) else month(d.datevalue) end,case when length(day(d.datevalue))=1 then concat('0',day(d.datevalue)) else day(d.datevalue) end)as integer)) as datevalue
from dim_date d,fact_fosalesforecast sf ,tmp_maxrptdate m
where d.dim_dateid=sf.dim_dateidforecast
and dd_insertsource='Manual'
and m.dd_reportingdate = TO_DATE(sf.dd_reportingdate,'DD MON YYYY')
group by fact_fosalesforecastid ) t
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set f.dd_forecastdate=ifnull(t.datevalue,'00010101');


update fact_fosalesforecast f
set dd_partnumber=dp.partnumber
from fact_fosalesforecast f,dim_part dp,tmp_maxrptdate m
where f.dim_partid=dp.dim_partid
and m.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
and dd_forecasttype='Manual';


update fact_fosalesforecast f
set dd_plantcode=dp.plant
from fact_fosalesforecast f,dim_part dp,tmp_maxrptdate m
where f.dim_partid=dp.dim_partid
and m.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
and dd_forecasttype='Manual';

update fact_fosalesforecast f
set dd_forecastrank='1'
from fact_fosalesforecast f,tmp_maxrptdate m
where m.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
and dd_forecasttype='Manual';

update fact_fosalesforecast f
set DD_MARKET_COUNTRY=f.dd_MARKET_GROUPING
from fact_fosalesforecast f,tmp_maxrptdate m
where
m.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
and dd_forecasttype='Manual';

merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,dd_market_grouping,
sum(f.ct_nasp) as ct_nasp,
sum(f.ct_nasp_cy) as ct_nasp_cy,
sum(f.ct_nasp_global) as ct_nasp_global,
sum(f.ct_nasp_py) as ct_nasp_py,
sum(f.ct_nasp_fc_cy) as ct_nasp_fc_cy,
sum(f.ct_nasp_fc_ny) as ct_nasp_fc_ny,
SUM(f.ct_salesdeliv2mthsnasp) as ct_salesdeliv2mthsnasp,
dt.calendarmonthid 
from  fact_atlaspharmlogiforecast_merck f, fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate m
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull((case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end),'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
AND m.dd_reportingdate = TO_DATE(fos.dd_reportingdate,'DD MON YYYY')
and f.market_grouping = fos.dd_market_grouping
and dd_forecasttype='Manual'
and dd_version='SFA'
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,dd_market_grouping,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 
fos.ct_nasp = t.ct_nasp,
fos.ct_nasp_cy=t.ct_nasp_cy,
fos.ct_nasp_global=t.ct_nasp_global,
fos.ct_nasp_py=t.ct_nasp_py,
fos.ct_nasp_fc_cy=t.ct_nasp_fc_cy,
fos.ct_nasp_fc_ny=t.ct_nasp_fc_ny,
fos.ct_salesdeliv2mthsnasp=t.ct_salesdeliv2mthsnasp;

drop table if exists tmp_for_updplanttitle;
create table tmp_for_updplanttitle as
select distinct p.partnumber,pl.plantcode,COUNTRY_DESTINATION_CODE,REPORTING_COMPANY_CODE,dd_planttitlemerck,dd_plantcode from fact_atlaspharmlogiforecast_merck f, dim_part p,dim_plant pl
where f.dim_partid=p.dim_partid
and f.dim_plantid=pl.dim_plantid
and dd_planttitlemerck<>'Not Set';

merge into fact_fosalesforecast f
using (
select distinct f.fact_fosalesforecastid,t.dd_planttitlemerck,t.dd_plantcode
from fact_fosalesforecast f,tmp_for_updplanttitle  t, tmp_maxrptdate r
where f.dd_partnumber=t.partnumber
and f.dd_plantcode=t.plantcode
and f.dd_COUNTRY_DESTINATION_CODE=case when t.plantcode <>'NL10' then 'Not Set' else t.COUNTRY_DESTINATION_CODE end
and f.dd_REPORTING_COMPANY_CODE=t.REPORTING_COMPANY_CODE
and dd_forecasttype='Manual'
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*having count(*)>1*/
 ) t 
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set f.dd_planttitlemerck=ifnull(t.dd_planttitlemerck,'Not Set'),
f.dd_plantcodemerck=t.dd_plantcode;


drop table if exists tmp_for_salesdeliveredPY;
create table tmp_for_salesdeliveredPY as
select distinct f.dim_partid,f.dim_plantid,COUNTRY_DESTINATION_CODE,REPORTING_COMPANY_CODE,dim_dateidreporting,f.ct_salesdeliveredPY  from fact_atlaspharmlogiforecast_merck f, dim_part p,dim_plant pl
where f.dim_partid=p.dim_partid
and f.dim_plantid=pl.dim_plantid;


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
SUM(f.ct_salesdeliveredPY) as ct_salesdeliveredPY,
dt.calendarmonthid 
from tmp_for_salesdeliveredPY f,fact_fosalesforecast fos,dim_date dt,dim_part dp,dim_plant pl, tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
and f.dim_plantid=pl.dim_plantid
and f.dim_plantid=fos.dim_plantid
AND f.dim_dateidreporting = dt.dim_dateid 
AND fos.dd_forecasttype='Manual' 
and TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(case when pl.plantcode <>'NL10' then 'Not Set' else f.COUNTRY_DESTINATION_CODE end,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
/*and dp.partnumber ='119977' and dp.plant='IE20'and dt.calendarmonthid like '2016%'*/
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set fos.ct_salesdeliveredPY=t.ct_salesdeliveredPY;

drop table if exists tmp_for_upd_salesdeliveredmonth;
create table tmp_for_upd_salesdeliveredmonth as
select f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
CASE WHEN 
MAX( (f.dd_reportavailable) ) = 'Yes' 
THEN 
SUM((f.ct_salesmonth1) * (f.ct_nasp)) 
ELSE 
NULL 
END as col1,
sum(f.ct_salesmonth1) as col2,sum(f.ct_nasp) as col3 from fact_atlaspharmlogiforecast_merck f,dim_part dp,dim_Date dt
where f.dim_partid=dp.dim_partid 
and dim_dateidreporting=dt.dim_dateid
and  dd_version='SFA'
group by f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end);


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
sum(f.col1) as col1,
dt.calendarmonthid 
from tmp_for_upd_salesdeliveredmonth f, fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate m
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(f.country_destination_code_upd,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
AND m.dd_reportingdate = TO_DATE(fos.dd_reportingdate,'DD MON YYYY')
and dd_forecasttype='Manual'
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 
fos.ct_salesdeliveredmonth = ifnull(t.col1,0);


merge into fact_fosalesforecast fos 
using (
select distinct fact_fosalesforecastid,
dd_forecastrank as dd_forecastrank2
from fact_fosalesforecast f,tmp_maxrptdate r
where TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and dd_forecasttype='Manual') t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set fos.dd_forecastrank2=t.dd_forecastrank2;
/*This will include only NL10 plant*/
UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE*/ AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
AND f.dd_plantcode='NL10'
and dd_forecasttype ='Manual'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate;

/*XX20 separate update*/
UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = case when t.PLANT_CODE ='NL10' then 'XX20' end AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE*/ AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and f.dd_plantcode='XX20'
and dd_forecasttype ='Manual'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate;


/*This update is for USA0 plants*/

UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE*/ AND f.DD_MARKET_GROUPING = 'USA'
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code in ( 'USA0')
and dd_forecasttype ='Manual'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate;

/*This update is for the rest of plants*/
merge into fact_fosalesforecast f
using (
select distinct fact_fosalesforecastid,t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE*/ AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code not in ( 'NL10','USA0')
and dd_forecasttype ='Manual'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate) t
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY;



/*Train and Test Forecast sample*/
/*This will include NL10 plant*/
UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE*/ AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
---and dd_forecastsample in ('Train','Test')
and d.calendarmonthid < year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0)
and dd_forecasttype ='Manual'
and f.dd_plantcode = 'NL10'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate;

/*This update is for USA10 plants*/

UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE*/ AND f.DD_MARKET_GROUPING = 'USA'
AND f.dim_dateidforecast = d.dim_dateid 
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code in ( 'USA0')
and dd_forecasttype ='Manual'
and d.calendarmonthid < year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0)
---and dd_forecastsample in ('Train','Test')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate;

/*This update is for the rest of plants*/

merge into fact_fosalesforecast f
using (select distinct f.fact_fosalesforecastid,
t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE*/ AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid 
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code not in ( 'NL10','USA0')
---and dd_forecastsample in ('Train','Test')
and d.calendarmonthid < year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0)
and dd_forecasttype ='Manual'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate) t
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY;


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
avg(ct_salesdelivered) as ct_salesdelivered ,
dt.calendarmonthid 
from fact_atlaspharmlogiforecast_merck f, fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull((case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end),'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
/*and dp.partnumber ='136190' and dp.plant='AE20'
and dt.calendarmonthid like '201701'*/
and dd_version='SFA'
and TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and dd_forecasttype='Manual'
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 	fos.ct_salesquantity=t.ct_salesdelivered;

update fact_fosalesforecast 
set ct_salesquantity=null
from fact_fosalesforecast f,tmp_maxrptdate m
where ct_salesquantity=0 and dd_forecasttype='Manual'
and m.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY');

update fact_fosalesforecast f
set ct_forecastquantity=f.CT_FORECASTQUANTITY_CUSTOMER
from fact_fosalesforecast f,tmp_maxrptdate m
where
m.dd_reportingdate = TO_DATE(f.dd_reportingdate,'DD MON YYYY')
and dd_forecasttype='Manual';

/*22 Mar 2017 Adding ct_mape_customerfcst for Manual Records*/ 
DROP TABLE IF EXISTS merck.tmp_custmapes_fosp;
CREATE TABLE merck.tmp_custmapes_fosp
AS
SELECT f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype,
100 * avg(abs((ct_forecastquantity_customer-ct_salesquantity)/case when ct_salesquantity=0 then 1 else ct_salesquantity end )) as ct_mape_customerfcst
FROM merck.fact_fosalesforecast f,tmp_maxrptdate r
WHERE
cast(concat(substring(dd_forecastdate,1,4) , '-' ,
    substring(dd_forecastdate,5,2) , '-' ,
    substring(dd_forecastdate,7,2) ) as date)
between   add_months(TO_DATE(f.dd_reportingdate,'DD MON YYYY'),-3)  and TO_DATE(f.dd_reportingdate,'DD MON YYYY')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
--and f.dd_reportingdate='08 Sep 2017'
AND dd_forecasttype='Manual'
---and dd_partnumber='004798'
GROUP BY f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype;

UPDATE merck.fact_fosalesforecast f
SET f.ct_mape_customerfcst = t.ct_mape_customerfcst
FROM merck.fact_fosalesforecast f,merck.tmp_custmapes_fosp t,tmp_maxrptdate r
WHERE f.dd_reportingdate = t.dd_reportingdate AND f.dd_partnumber = t.dd_partnumber AND f.dd_plantcode = t.dd_plantcode
AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND ifnull(f.DD_HEI_CODE,'Not Set')= ifnull(t.DD_HEI_CODE,'Not Set') AND f.DD_MARKET_GROUPING = t.DD_MARKET_GROUPING
and f.dd_forecasttype='Manual'
--and f.dd_reportingdate in ('08 Mar 2017', '09 Mar 2017')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
AND f.dd_forecasttype = t.dd_forecasttype;

update fact_fosalesforecast f
set ct_mape=f.ct_mape_customerfcst
from fact_fosalesforecast f,tmp_maxrptdate r
where TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and dd_forecasttype='Manual';

UPDATE merck.fact_fosalesforecast f
SET f.ct_mape = 0 where f.ct_mape is null
and  dd_forecasttype='Manual';

UPDATE merck.fact_fosalesforecast f
SET f.ct_mape_customerfcst = 0 where f.ct_mape_customerfcst is null
and  dd_forecasttype='Manual';

UPDATE fact_fosalesforecast f
SET dd_latestreporting = 'No'
WHERE dd_latestreporting <> 'No'
and dd_forecasttype='Manual';

UPDATE fact_fosalesforecast f
SET dd_latestreporting = 'Yes'
FROM tmp_maxrptdate r,fact_fosalesforecast f
where DATE_TRUNC('month',  TO_DATE(f.dd_reportingdate,'DD MON YYYY')) =DATE_TRUNC('month',  r.dd_reportingdate)
and  dd_forecastapproach is null 
and dd_forecasttype='Manual';


/*End of BI-5717*/



DELETE FROM stage_fosalesforecast;
DROP TABLE IF EXISTS backup_fact_fosalesforecast_largefcstqty;
drop table if exists fact_fosalesforecast_temp;
DROP TABLE IF EXISTS merck.tmp_custmapes_fosp;
DROP TABLE IF EXISTS tmp_amt_sellingpriceperunit_gbl;
DROP TABLE IF EXISTS tmp_atleast1nonstlinefcst;
DROP TABLE IF EXISTS tmp_avg_sp_fromfso;
DROP TABLE IF EXISTS tmp_ct_salescontribglobal;
DROP TABLE IF EXISTS tmp_custmapes_fosp;
DROP TABLE IF EXISTS tmp_distinctdmdunit_loc_mape;
DROP TABLE IF EXISTS tmp_distinctdmdunit_loc_mape_upd;
DROP TABLE IF EXISTS tmp_fact_salesorder_12monthsales;
DROP TABLE IF EXISTS tmp_fact_salesorder_12monthsales_1;
DROP TABLE IF EXISTS tmp_forecastcov;
DROP TABLE IF EXISTS tmp_forecastcov1;
DROP TABLE IF EXISTS tmp_gaincritlt0;
/*DROP TABLE IF EXISTS tmp_maxrptdate*/
DROP TABLE IF EXISTS tmp_monthlysales_cov;
DROP TABLE IF EXISTS tmp_partvsmape;
DROP TABLE IF EXISTS tmp_partvsmape_rerank;
DROP TABLE IF EXISTS tmp_qoqmeasures;
DROP TABLE IF EXISTS tmp_qoqmeasures_2;
DROP TABLE IF EXISTS tmp_qoqmeasures_3;
DROP TABLE IF EXISTS tmp_rerank_fcst;
drop table if exists tmp_saleshistory_grain_reqmonths;
drop table if exists tmp_saleshistory_grain_reqmonths_2;
DROP TABLE IF EXISTS tmp_sod_denorm_fcst;
DROP TABLE IF EXISTS tmp_update_nasmpeasures_to_fopsfcst;
DROP TABLE IF EXISTS tmp_upd_ct_volatility_yoy_percent;
DROP TABLE IF EXISTS tmp_upd_ct_volatility_yoy_percent_overall;
DROP TABLE IF EXISTS tmp_upd_ct_volatility_yoy_percent_STDDEV;
DROP TABLE IF EXISTS tmp_upd_dd_sales_rank;
DROP TABLE IF EXISTS tmp_upd_fcstrank_fosf;
DROP TABLE IF EXISTS tmp_upd_fcstrank_fosf1;
DROP TABLE IF EXISTS tmp_upd_fcstrank_fosf2;
DROP TABLE IF EXISTS tmp_upd_fcstrank_fosf3;
DROP TABLE IF EXISTS tmp_upd_forecastproductcategory;
DROP TABLE IF EXISTS tmp_upd_forecastproductcategory_2;


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
avg(f.ct_forecast4mth1) as ct_forecast4mth1,
avg(f.ct_forecast4mth2) as ct_forecast4mth2,
avg(f.ct_salesmonth1) as ct_salesmonth1,
avg(f.ct_salesmonth2) as ct_salesmonth2,
dt.calendarmonthid 
from fact_atlaspharmlogiforecast_merck f,fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull((case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end),'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
and  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*
and dp.partnumber ='136190' and dp.plant='NL10' 
and dt.calendarmonthid like '201702%'
AND dd_reportingdate = '09 Mar 2017'
*/
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,dt.calendarmonthid
) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then 
update set fos.ct_forecast4mth1=t.ct_forecast4mth1,
fos.ct_forecast4mth2=t.ct_forecast4mth2,
fos.ct_salesmonth1=t.ct_salesmonth1,
fos.ct_salesmonth2=t.ct_salesmonth2;



merge into fact_fosalesforecast fos
using (

select distinct fact_fosalesforecastid, 
dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate, dt.calendarmonthid,f.dd_version,
f.dd_forecast4mthis1null,
f.dd_forecast4mthis2null

--select f.*
from fact_atlaspharmlogiforecast_merck f,fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull((case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end),'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
--and fact_fosalesforecastid = '56041574'
and f.dd_version = 'SFA'
and  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate

) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then 
update set fos.dd_forecast4mthis1null=t.dd_forecast4mthis1null,
fos.dd_forecast4mthis2null=t.dd_forecast4mthis2null;


update fact_fosalesforecast fos
set ct_forecast4mth1 = 0 where ct_forecast4mth1 is null;

update fact_fosalesforecast fos
set ct_forecast4mth2 = 0 where ct_forecast4mth2 is null;

update fact_fosalesforecast fos
set ct_salesmonth1 = 0 where ct_salesmonth1 is null;

update fact_fosalesforecast fos
set ct_salesmonth2 = 0 where ct_salesmonth2 is null;




merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,fos.dd_market_grouping,
SUM(f.CT_SALESDELIVMOVING) as CT_SALESDELIVMOVING,
dt.calendarmonthid 
from fact_atlaspharmlogiforecast_merck f,fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull((case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end),'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
and  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and f.market_grouping = fos.dd_market_grouping
and f.dd_version = 'SFA'

/*
and dp.partnumber ='136190' and dp.plant='NL10' 
and dt.calendarmonthid like '201702%'
AND dd_reportingdate = '09 Mar 2017'
*/
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,fos.dd_market_grouping,dt.calendarmonthid
) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then 
update set fos.CT_SALESDELIVMOVING=t.CT_SALESDELIVMOVING;


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate, dd_market_grouping,
sum(f.ct_salesdelivmoving*f.ct_nasp) as CT_SALESDELIVMOVINGusd,
dt.calendarmonthid
from fact_atlaspharmlogiforecast_merck f,fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid 
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull((case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end),'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set') 
and  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and f.market_grouping = fos.dd_market_grouping
and f.dd_version = 'SFA'
--and dp.partnumber ='085030' and dp.plant='NL20' 
--and dt.calendarmonthid like '201702%'
--AND dd_reportingdate = '09 Mar 2017'

group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,dd_market_grouping,dt.calendarmonthid
) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then 
update set fos.CT_SALESDELIVMOVINGusd = t.CT_SALESDELIVMOVINGusd;


drop table if exists tmp_for_upd_salesdelivactual_ytd;
create table tmp_for_upd_salesdelivactual_ytd as
select f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,market_grouping,
(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
ct_Salesdelivactualytd as col01 
from fact_atlaspharmlogiforecast_merck f,dim_part dp,dim_Date dt
where f.dim_partid=dp.dim_partid 
and dim_dateidreporting=dt.dim_dateid
and  dd_version='SFA';

merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate, dd_market_grouping,
sum(f.col01) as col01,
dt.calendarmonthid 
from tmp_for_upd_salesdelivactual_ytd f, fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(f.country_destination_code_upd,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
and  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and f.market_grouping = fos.dd_market_grouping
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate, dd_market_grouping,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 
fos.CT_DELIVEREDQTY_YTD = ifnull(t.col01,0);


/*Georgiana Changes according to APP-6365: Calculate all quantities that are needed for FO DFA-4 Measure*/

/*Pick the correct holdout period, the 3 month holdout was requested, but not all runs had the holdout perios for 3 months so in those cases we are picking the lowest value*/
drop table if exists tmp_for_holdoutperiod;
create table tmp_for_holdoutperiod as
select distinct f.dd_reportingdate,dd_forecastsample,dd_forecastdate from fact_fosalesforecast f, tmp_maxrptdate r where 
TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and dd_forecastsample='Test';

drop table if exists tmp_for_holdoutperiod_2;
create table tmp_for_holdoutperiod_2 as
select distinct dd_reportingdate, count(*) as holdout from tmp_for_holdoutperiod
group by dd_reportingdate;

update fact_fosalesforecast
set dd_holdout=holdout
from  fact_fosalesforecast f,tmp_for_holdoutperiod_2 t
where f.dd_reportingdate=t.dd_reportingdate
and dd_holdout<>holdout;


drop table if exists tmp_for_selectminholdout;
create table tmp_for_selectminholdout as
select month(TO_DATE(f.dd_reportingdate,'DD MON YYYY')) as holdoutmonth,year(TO_DATE(f.dd_reportingdate,'DD MON YYYY')) as holdoutyear,min(dd_holdout) as min_dd_holdout
from fact_fosalesforecast f,tmp_maxrptdate m
where  month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))=month(m.dd_reportingdate)
and year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))=year(TO_DATE(m.dd_reportingdate,'DD MON YYYY'))
group by month(TO_DATE(f.dd_reportingdate,'DD MON YYYY')),year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'));


drop table if exists tmp_forlastmonthholdout;
create table  tmp_forlastmonthholdout as select distinct case when month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))-1=0 then 12 else month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))-1 end  as holdoutmonth,
case when month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))-1=0 then year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))-1 else year(TO_DATE(f.dd_reportingdate,'DD MON YYYY')) end as holdoutyear,dd_holdout
from fact_fosalesforecast f;


drop table if exists tmp_for_selectminholdoutlast4month;
create table tmp_for_selectminholdoutlast4month as
select distinct
month(TO_DATE(f.dd_reportingdate,'DD MON YYYY')) as holdoutmonth,
year(TO_DATE(f.dd_reportingdate,'DD MON YYYY')) as holdoutyear,min(dd_holdout) as min_dd_holdout
from fact_fosalesforecast f,tmp_maxrptdate m
where  month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))=case when month(m.dd_reportingdate)-4 <=0 then month(m.dd_reportingdate)+8 else month(m.dd_reportingdate)-4 end
and year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))=case when month(m.dd_reportingdate)-4 <=0 then year(m.dd_reportingdate)-1 else year(m.dd_reportingdate) end
group by month(TO_DATE(f.dd_reportingdate,'DD MON YYYY')),year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'));

/*Sales from previous Month */
drop table if exists tmp_for_prevmonthsales;
create table tmp_for_prevmonthsales as
select distinct f.dd_reportingdate,f.dd_forecastdate,f.dd_partnumber,f.dd_plantcode,f.dd_reporting_company_code,f.dd_country_destination_code,f.dd_forecasttype,f.dd_market_grouping,f.dd_forecastrank,f.dd_holdout,
lag(avg(f.ct_salesquantity)) over (partition by f.dd_reportingdate,f.dd_partnumber,f.dd_plantcode,f.dd_reporting_company_code,f.dd_country_destination_code,f.dd_forecasttype,f.dd_market_grouping,f.dd_forecastrank,f.dd_holdout order by f.dd_forecastdate asc) as ct_salesquantity
from fact_fosalesforecast f,tmp_for_selectminholdout t
where  month(TO_DATE(dd_reportingdate,'DD MON YYYY'))=t.holdoutmonth
and year(TO_DATE(dd_reportingdate,'DD MON YYYY'))=t.holdoutyear
and f.dd_holdout=t.min_dd_holdout
group by f.dd_reportingdate,f.dd_forecastdate,f.dd_partnumber,f.dd_plantcode,f.dd_reporting_company_code,f.dd_country_destination_code,f.dd_forecasttype,f.dd_market_grouping,f.dd_forecastrank,f.dd_holdout;

merge into fact_fosalesforecast f
using (
select distinct  f.fact_fosalesforecastid,t.ct_salesquantity
from fact_fosalesforecast f, tmp_for_prevmonthsales t 
where t.dd_reportingDate=f.dd_reportingdate
and t.dd_partnumber=f.dd_partnumber
and t.dd_plantcode=f.dd_plantcode
and t.dd_reporting_company_code=f.dd_reporting_company_code
and t.dd_country_destination_code=f.dd_country_destination_code
and t.dd_forecasttype=f.dd_forecasttype
and f.dd_forecastdate=t.dd_forecastdate
and t.dd_market_grouping=f.dd_market_grouping
---and t.dd_hei_code=f.dd_hei_code
and f.dd_holdout=t.dd_holdout
and f.dd_forecastrank=t.dd_forecastrank

) t
on f.fact_fosalesforecastid=t.fact_fosalesforecastid
when matched then update set  f.ct_salesquantityprevmonth=ifnull(t.ct_salesquantity,0);

/*Forecast 4 months from current run for current month run*/

drop table if exists tmp_for_prev4repmonth;
create table tmp_for_prev4repmonth as
select distinct dd_reportingdate,dd_partnumber,dd_plantcode,dd_reporting_company_code,dd_forecastdate,
case when dd_plantcode='NL10' then dd_country_destination_code else 'Not Set' end as dd_countrydestination_code,dd_forecasttype,dd_hei_code,dd_market_grouping,dd_forecastrank,f.dd_holdout,
case when month(TO_DATE(dd_reportingdate,'DD MON YYYY'))-4<=0  then (month(TO_DATE(dd_reportingdate,'DD MON YYYY'))-4 +12) else month(TO_DATE(dd_reportingdate,'DD MON YYYY'))-4 end foremonth,
case when month(TO_DATE(dd_reportingdate,'DD MON YYYY'))-4<=0 then year(TO_DATE(dd_reportingdate,'DD MON YYYY'))-1 else year(TO_DATE(dd_reportingdate,'DD MON YYYY')) end foreyear ,
month(TO_DATE(dd_reportingdate,'DD MON YYYY')) as snapmonth,
year(TO_DATE(dd_reportingdate,'DD MON YYYY')) as snapyear
from fact_fosalesforecast f, tmp_for_selectminholdout t
where  month(TO_DATE(dd_reportingdate,'DD MON YYYY'))=t.holdoutmonth
and year(TO_DATE(dd_reportingdate,'DD MON YYYY'))=t.holdoutyear
/*and dd_partnumber='013774'
and dD_reportingdate='05 May 2017'*/
and f.dd_holdout=t.min_dd_holdout;


drop table if exists tmp_for_prev4monthforecast;
create table tmp_for_prev4monthforecast as
select  t.dd_reportingdate,f.dd_forecastdate,f.dd_partnumber,f.dd_plantcode,f.dd_reporting_company_code,case when f.dd_plantcode='NL10' then f.dd_country_destination_code else 'Not Set' end as dd_country_destination_code,t.dd_forecasttype,t.dd_hei_code,f.dd_market_grouping,t.dd_forecastrank,t.dd_holdout,avg(ct_forecastquantity) as ct_forecastquantity4mth1
from fact_fosalesforecast f, tmp_for_prev4repmonth t,tmp_for_selectminholdoutlast4month t1
where
 t.foremonth=month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))
and t.foreyear=year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))
and  f.dd_forecastdate=t.dd_forecastdate
and t.dd_partnumber=f.dd_partnumber
and t.dd_plantcode=f.dd_plantcode
and t.dd_reporting_company_code=f.dd_reporting_company_code
and t.dd_countrydestination_code=case when f.dd_plantcode='NL10' then f.dd_country_destination_code else 'Not Set' end
and t.dd_forecasttype=f.dd_forecasttype
---and t.dd_hei_code=f.dd_hei_code
and month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))=t1.holdoutmonth
and year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))=t1.holdoutyear
and f.dd_holdout=t1.min_dd_holdout
---and t.dd_forecastrank=f.dd_forecastrank
and t.dd_market_grouping=f.dd_market_grouping
group by  t.dd_reportingdate,f.dd_forecastdate,f.dd_partnumber,f.dd_plantcode,f.dd_reporting_company_code,case when f.dd_plantcode='NL10' then f.dd_country_destination_code else 'Not Set' end,t.dd_forecasttype,t.dd_hei_code,f.dd_market_grouping,t.dd_forecastrank,t.dd_holdout;


merge into fact_fosalesforecast f
using (select distinct fact_fosalesforecastid,t.dd_forecasttype,t.ct_forecastquantity4mth1
from fact_fosalesforecast f, tmp_for_prev4monthforecast t
where t.dd_reportingDate=f.dd_reportingdate
and t.dd_partnumber=f.dd_partnumber
and t.dd_plantcode=f.dd_plantcode
and t.dd_reporting_company_code=f.dd_reporting_company_code
and case when f.dd_plantcode='NL10' then f.dd_country_destination_code else 'Not Set' end=t.dd_country_destination_code
and t.dd_forecasttype=f.dd_forecasttype
and t.dd_hei_code=f.dd_hei_code
and t.dd_market_grouping=f.dd_market_grouping
and f.dd_forecastdate=t.dd_forecastdate
and f.dd_forecastrank=t.dd_forecastrank
and f.dd_holdout=t.dd_holdout) t
on f.fact_fosalesforecastid=t.fact_fosalesforecastid
when matched then update set  f.ct_forecastquantity4mth1=ifnull(t.ct_forecastquantity4mth1,0);



/*Forecast 4 months from current run for previous month run*/


drop table if exists tmp_for_prev4repmonth;
create table tmp_for_prev4repmonth as
select distinct dd_reportingdate,dd_partnumber,dd_plantcode,dd_reporting_company_code,dd_forecastdate,
case when dd_plantcode='NL10' then dd_country_destination_code else 'Not Set' end as dd_countrydestination_code,dd_forecasttype,dd_hei_code,dd_market_grouping,dd_forecastrank,f.dd_holdout,
case when month(TO_DATE(dd_reportingdate,'DD MON YYYY'))-4<=0  then (month(TO_DATE(dd_reportingdate,'DD MON YYYY'))-4 +12) else month(TO_DATE(dd_reportingdate,'DD MON YYYY'))-4 end foremonth,
case when month(TO_DATE(dd_reportingdate,'DD MON YYYY'))-4<=0 then year(TO_DATE(dd_reportingdate,'DD MON YYYY'))-1 else year(TO_DATE(dd_reportingdate,'DD MON YYYY')) end foreyear ,
case when month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))-1=0 then month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))+11 else month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))-1 end as snapmonth,
case when month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))-1=0 then year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))-1 else year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) end as snapyear
from fact_fosalesforecast f, tmp_for_selectminholdout t
where  month(TO_DATE(dd_reportingdate,'DD MON YYYY'))=t.holdoutmonth
and year(TO_DATE(dd_reportingdate,'DD MON YYYY'))=t.holdoutyear
and f.dd_holdout=t.min_dd_holdout;
/*and dd_partnumber='013774'
and dD_reportingdate='05 May 2017'*/
/*and  dd_partnumber='131058' and dd_plantcode='CZ20' and dd_reportingdate='05 May 2017' */


drop table if exists tmp_for_prev4monthforecast;
create table tmp_for_prev4monthforecast as
select  t.dd_reportingdate,t.dd_forecastdate,f.dd_partnumber,f.dd_plantcode,f.dd_reporting_company_code,case when f.dd_plantcode='NL10' then f.dd_country_destination_code else 'Not Set' end as dd_country_destination_code,t.dd_forecasttype,t.dd_hei_code,f.dd_market_grouping,t.dd_forecastrank,t.dd_holdout,avg(ct_forecastquantity) as ct_forecastquantity4mth2
from fact_fosalesforecast f, tmp_for_prev4repmonth t,tmp_for_selectminholdoutlast4month t1
where
 t.foremonth=month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))
and t.foreyear=year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))
and t.dd_partnumber=f.dd_partnumber
and t.dd_plantcode=f.dd_plantcode
and t.dd_reporting_company_code=f.dd_reporting_company_code
and t.dd_countrydestination_code=case when f.dd_plantcode='NL10' then f.dd_country_destination_code else 'Not Set' end
and  month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))=t.snapmonth
and year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))=t.snapyear
---and t.dd_hei_code=f.dd_hei_code
and month(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))=t1.holdoutmonth
and year(TO_DATE(f.dd_reportingdate,'DD MON YYYY'))=t1.holdoutyear
and f.dd_holdout=t1.min_dd_holdout
and t.dd_forecasttype=f.dd_forecasttype
---and t.dd_forecastrank=f.dd_forecastrank
and t.dd_market_grouping=f.dd_market_grouping
group by  t.dd_reportingdate,t.dd_forecastdate,f.dd_partnumber,f.dd_plantcode,f.dd_reporting_company_code,case when f.dd_plantcode='NL10' then f.dd_country_destination_code else 'Not Set' end,t.dd_forecasttype,t.dd_hei_code,f.dd_market_grouping,t.dd_forecastrank,t.dd_holdout;



merge into fact_fosalesforecast f
using (select distinct fact_fosalesforecastid,t.ct_forecastquantity4mth2
from fact_fosalesforecast f, tmp_for_prev4monthforecast t
where t.dd_reportingDate=f.dd_reportingdate
and t.dd_partnumber=f.dd_partnumber
and t.dd_plantcode=f.dd_plantcode
and t.dd_reporting_company_code=f.dd_reporting_company_code
and case when f.dd_plantcode='NL10' then f.dd_country_destination_code else 'Not Set' end=t.dd_country_destination_code
and t.dd_forecasttype=f.dd_forecasttype
and t.dd_hei_code=f.dd_hei_code
and f.dd_forecastdate=t.dd_forecastdate
and t.dd_market_grouping=f.dd_market_grouping
and f.dd_forecastrank=t.dd_forecastrank
and f.dd_holdout=t.dd_holdout) t
on f.fact_fosalesforecastid=t.fact_fosalesforecastid
when matched then update set  f.ct_forecastquantity4mth2=ifnull(t.ct_forecastquantity4mth2,0);

/*APP-6365 End*/

/*Georgiana Adding changes according to APP-7309*/

DROP TABLE IF EXISTS merck.tmp_custmapes_fosp_2;
CREATE TABLE merck.tmp_custmapes_fosp_2
AS
SELECT f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype,
100 * avg(abs((ct_forecastquantity-ct_salesquantity)/ct_salesquantity)) ct_mape_fofcst
FROM merck.fact_fosalesforecast f,tmp_maxrptdate r
WHERE
cast(concat(substring(dd_forecastdate,1,4) , '-' ,
    substring(dd_forecastdate,5,2) , '-' ,
    substring(dd_forecastdate,7,2) ) as date)
between   add_months(TO_DATE(f.dd_reportingdate,'DD MON YYYY'),-12)  and TO_DATE(f.dd_reportingdate,'DD MON YYYY')
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
GROUP BY f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype;

UPDATE merck.fact_fosalesforecast f
SET f.ct_mape12months = ifnull(t.ct_mape_fofcst,0)
FROM merck.fact_fosalesforecast f,merck.tmp_custmapes_fosp_2 t
WHERE f.dd_reportingdate = t.dd_reportingdate AND f.dd_partnumber = t.dd_partnumber AND f.dd_plantcode = t.dd_plantcode
AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND f.DD_HEI_CODE = t.DD_HEI_CODE AND f.DD_MARKET_GROUPING = t.DD_MARKET_GROUPING
AND f.dd_forecasttype = t.dd_forecasttype;

DROP TABLE IF EXISTS merck.tmp_custmapes_fosp_2_customerforecast;
CREATE TABLE merck.tmp_custmapes_fosp_2_customerforecast
AS
SELECT f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype,
100 * avg(abs((ct_forecastquantity_customer-ct_salesquantity)/case when ct_salesquantity=0 then 1 else ct_salesquantity end )) as ct_mape_customerfcst
FROM merck.fact_fosalesforecast f,tmp_maxrptdate r
WHERE
cast(concat(substring(dd_forecastdate,1,4) , '-' ,
    substring(dd_forecastdate,5,2) , '-' ,
    substring(dd_forecastdate,7,2) ) as date)
between   add_months(TO_DATE(f.dd_reportingdate,'DD MON YYYY'),-12)  and TO_DATE(f.dd_reportingdate,'DD MON YYYY')
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
GROUP BY f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype;

UPDATE merck.fact_fosalesforecast f
SET f.ct_mapecustomerforecast12months = ifnull(t.ct_mape_customerfcst,0)
FROM merck.fact_fosalesforecast f,merck.tmp_custmapes_fosp_2_customerforecast t
WHERE f.dd_reportingdate = t.dd_reportingdate AND f.dd_partnumber = t.dd_partnumber AND f.dd_plantcode = t.dd_plantcode
AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND f.DD_HEI_CODE = t.DD_HEI_CODE AND f.DD_MARKET_GROUPING = t.DD_MARKET_GROUPING
AND f.dd_forecasttype = t.dd_forecasttype;

/*Georgiana Added changes for Forecast Selected Editable field form PF SA*/

/*update fact_fosalesforecast 
set dd_lowermape=CASE WHEN  CT_MAPE_CUSTOMERFCST  <  ct_mape THEN 'Current' WHEN  CT_MAPE_CUSTOMERFCST  >  ct_mape THEN 'Aera' ELSE ' - ' END
from fact_fosalesforecast fos, tmp_maxrptdate r
where  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/

/*Calculating lower mape at material, plant and rank level*/
 merge into fact_fosalesforecast f
using ( select distinct   f.dd_reportingdate,dd_partnumber,dd_plantcode,dd_forecastrank, CASE WHEN  avg(CT_MAPE_CUSTOMERFCST)  <  avg(ct_mape) THEN 'Current' WHEN  avg(CT_MAPE_CUSTOMERFCST) >  avg(ct_mape) THEN 'Aera' ELSE ' - ' END as lowermape
from fact_fosalesforecast f, tmp_maxrptdate r
where  TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
group by  f.dd_reportingdate,dd_partnumber,dd_plantcode,dd_forecastrank
) t 
on t.dd_reportingdate=f.dd_reportingdate
and t.dd_partnumber=f.dd_partnumber
and t.dd_plantcode = f.dd_plantcode
and t.dd_forecastrank=f.dd_forecastrank
when matched then update set f.dd_lowermape=t.lowermape;

/*25 Oct 2017 Georgiana changes: added new field category according to APP-7656*/
update fact_fosalesforecast
set dd_category =to_char(add_months(TO_DATE(dd_reportingdate,'DD MON YYYY'),1),'Mon_YYYY')
from fact_fosalesforecast fos, tmp_maxrptdate r
where  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate;

merge into fact_fosalesforecast a
using(
select distinct s.region,s.market_grouping,r.dd_reportingdate
from region_marketing_grouping s, tmp_maxrptdate r
) t
on t.market_grouping=a.dd_market_grouping
when matched then update set a.dd_region=ifnull(t.region,'Not Set')
where a.dd_region<>ifnull(t.region,'Not Set')
and  TO_DATE(a.dd_reportingdate,'DD MON YYYY') = t.dd_reportingdate;


/* 17 Jan 2018 Georgiana Changes according to APP-8530 new logic for NL10/XX20 cases, where we have one material with both NL10 and XX20 Plant Codes only the NL10 should be kept*/
delete from fact_fosalesforecast
where TO_DATE( dd_reportingdate,'DD MON YYYY') in ( select distinct dd_reportingdate from tmp_maxrptdate r)
and dim_plantid='91'
and dd_plantcode='XX20';

merge into fact_fosalesforecast f
using(
select distinct f.fact_fosalesforecastid ,f.dim_partid, f.DD_PLANTCODEMERCK, f.DD_FORECASTRANK, f.dd_reportingdate, dt.monthyear,
AVG(CT_MAPE) as FOMAPE, AVG(CT_MAPE_CUSTOMERFCST)  AS CURRENTMAPE FROM  fact_fosalesforecast f, dim_date dt,tmp_maxrptdate r
where  f.dim_dateidforecast = dt.dim_dateid
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
group by f.fact_fosalesforecastid,f.dim_partid, f.DD_PLANTCODEMERCK, f.DD_FORECASTRANK, f.dd_reportingdate, dt.monthyear) t 
on t.fact_fosalesforecastid = f.fact_fosalesforecastid
when matched then update set
f.DD_FOMAPE = ifnull(t.FOMAPE,0), f.DD_CURRENTMAPE = ifnull(t.CURRENTMAPE,0)
;

/*Georgiana 02 Jul 2018 - new logic for ct_forecastquantity_costomer according to APP-9892*/

/*Insert new records in PF with Manual Forecast type in order to have the next 2 years forecast date availavale, for example in July in PF we have the max forecast date 202006 with Manual forecasst type, and we need to insert another record for 202007*/
/*for this insert we are copying the row from the same month last year together will all details and insert it for 202007*/
/*The quantity will be updated bellow*/


drop table if exists tmp_for_insertnullforecastforManualdate;
create table tmp_for_insertnullforecastforManualdate as
select distinct max(calendarmonthid) as maxcalendarmonthid from fact_fosalesforecast f ,dim_date d1,tmp_maxrptdate r
where TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and dim_dateidforecast=d1.dim_dateid
and dd_forecasttype='Manual';


delete from number_fountain m WHERE m.table_name = 'fact_fosalesforecast';

insert into number_fountain
select  'fact_fosalesforecast',
ifnull(max(d.fact_fosalesforecastid),
	ifnull((select min(s.dim_projectsourceid * s.multiplier)
			from dim_projectsource s),0))
from fact_fosalesforecast d
WHERE d.fact_fosalesforecastid <> 1;

insert into fact_fosalesforecast 
select 
(select ifnull(m.max_id, 0) from number_fountain m WHERE m.table_name = 'fact_fosalesforecast') 
+ row_number() over(order by '') as fact_fosalesforecastid,
t.* 
from
(select distinct DIM_PARTID,DIM_PLANTID,DD_COMPANYCODE,DIM_DATEIDREPORTING,DD_FORECASTTYPE,DD_FORECASTSAMPLE,
min(d2.dim_dateid) as dim_dateidforecast, 
CT_FORECASTQUANTITY,CT_MAPE_OLD,DD_FORECASTRANK,DD_FORECASTMODE,DIM_PROJECTSOURCEID,DD_LATESTREPORTING,CT_FORECASTQUANTITY_CUSTOMER,
DD_FORECASTGRAINATTRONE,DD_FORECASTGRAINATTRTWO,DD_FORECASTGRAINATTRTHREE,DD_FORECASTGRAINATTRFOUR,DD_FORECASTGRAINATTRFIVE,
DD_FORECASTGRAIN,CT_SALESQUANTITY_NEW,CT_SALESQUANTITY,AMT_STDCOSTPERUNIT,AMT_SELLINGPRICEPERUNIT,DIM_SALESUOMID,DD_LASTDATE,DD_HOLDOUTDATE,
f.DD_REPORTINGDATE,
min( cast(concat(year(d2.datevalue),case when length(month(d2.datevalue))=1 then concat('0',month(d2.datevalue)) else month(d2.datevalue) end,case when length(day(d2.datevalue))=1 then concat('0',day(d2.datevalue)) else day(d2.datevalue) end)as integer)) as dd_forecastdate,
CT_LOWPI,CT_HIGHPI,CT_SALES_PREVMONTH,CT_SALES_PREVQTR,CT_SALES_PREVYR,CT_FORECAST_PREVMONTH,CT_FORECAST_PREVQTR,CT_FORECAST_PREVYR,
CT_SALES_CURRENTQTR,CT_FORECAST_CURRENTQTR,CT_SALES_PREVQTR_PREVYEAR,CT_FORECAST_PREVQTR_PREVYEAR,DIM_PART_PRESCRIPTIVEID,
DIM_PLANT_PRESCRIPTIVEID,STD_EXCHANGERATE_DATEID,DD_LASTREFRESHDATE,DD_PARTNUMBER,DD_PLANTCODE,CT_BIAS_ERROR,CT_BIAS_ERROR_RANK,CT_MAPE_CUSTOMERFCST,
CT_SALESCONTRIBGLOBAL,CT_RATIO_MAPE,DD_SALESCONTRIBUTION_QUARTILE_RANK,AMT_SHIPPEDQTYYTD,AMT_EXCHANGERATE_GBL,AMT_EXCHANGERATE,CT_DELIVEREDQTY_YTD,DD_SALES_COCD,DD_REPORTING_COMPANY_CODE,
DD_HEI_CODE,DD_COUNTRY_DESTINATION_CODE,DD_MARKET_GROUPING,CT_NASP,CT_NASP_CY,CT_NASP_GLOBAL,CT_NASP_PY,CT_NASP_FC_CY,CT_NASP_FC_NY,DD_MARKET_COUNTRY,CT_COEFFOFVARIATION,
DD_SALESCONTRIBUTION_3LEVELS_RANK,CT_VOLATILITY_YOY_PERCENT,DD_VOLATILITY_YOY_GT50PC,DD_FORECASTQUANTITY,CT_SALESDELIV2MTHSIRU,CT_SALESDELIV2MTHSNASP,DD_DYNAMICCONTRIBUTIONRANK,
DD_DYNAMICCONTRIBUTION,DD_HOME_VS_EXPORT,CT_COEFFOFVARIATION_FORECAST,DD_FORECASTPRODUCTCATEGORY,DD_HEICODE_NEW,DD_FLAG_SALESQUANTITY_EDITABLE,DD_PLANTTITLEMERCK,DD_PLANTCODEMERCK,
DD_REPORTAVAILABLE,CT_SALESDELIVEREDPY,CT_SALESDELIVEREDMONTH,DD_FORECASTRANK2,DD_INSERTSOURCE,CT_SALESDELIVACTUALYTD,CT_FORECAST4MTH1,CT_FORECAST4MTH2,CT_SALESMONTH1,CT_SALESMONTH2,
DD_FORECAST3MTHISNULL,DD_FORECAST4MTHIS1NULL,DD_FORECAST4MTHIS2NULL,DD_FORECAST4MTHISNULL,DD_FORECAST4MTHISNULL_SUBPRODLEV,DD_FORECAST6MTHISNULL,DD_FORECAST9MTHISNULL,CT_SALESDELIVMOVING,
DD_FOMAPE,DD_CURRENTMAPE,CT_SALESDELIVMOVINGUSD,CT_MAPE,CT_SALESDELIVACTUALYTD_TESTGH,CT_SALESDELIVMOVINGUSD_TESTGH,DIM_CURRENCYID,CT_SALESQUANTITYPREVMONTH,CT_FORECASTQUANTITY4MTH1,
DD_HOLDOUT,CT_FORECASTQUANTITY4MTH2,DD_FORECASTSELECTED,CT_FODFA_4,DD_UNIQUEKEY,CT_MAPE12MONTHS,CT_MAPECUSTOMERFORECAST12MONTHS,DD_LOWERMAPE,DD_CATEGORY,DD_COMMENTS,CT_FORECASTQUANTITYRANK1,
CT_FORECASTQUANTITY_CUSTOMER_RANK1,DD_REGION,DD_FORECASTAPPROACH,f.DW_UPDATE_DATE,FLAG_UPDATE,DD_FORECASTCHANGED_BYMERCK,CT_MONTHSSINCELASTCHANGE,CT_FORECASTSELECTEDLASTMONTH,CT_FORECASTQUANTITYSELECTED,
CT_SALESDELIVMOVING_NEW,
DD_FORECASTSELECTEDFINAL,DD_NEWMATERIALNUMBER,
DD_OLDATERIALNUMBER,dd_confirmed,DD_FORECASTSELECTED_ORIG,DD_CURRENCYCODE,CT_EXCHANGERATE_PL
from fact_fosalesforecast f
, dim_date d1, dim_date d2,
tmp_for_insertnullforecastforManualdate t,tmp_maxrptdate r
where TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and dd_forecasttype='Manual'
and dim_dateidforecast=d1.dim_dateid
and d1.calendarmonthid=maxcalendarmonthid -99
and d2.calendarmonthid=case when right(maxcalendarmonthid + 1,2) >12 then maxcalendarmonthid + 89 else maxcalendarmonthid+1 end 
and d2.plantcode_factory='Not Set' and d2.companycode='Not Set'
group by DIM_PARTID,DIM_PLANTID,DD_COMPANYCODE,DIM_DATEIDREPORTING,DD_FORECASTTYPE,DD_FORECASTSAMPLE,
CT_FORECASTQUANTITY,CT_MAPE_OLD,DD_FORECASTRANK,DD_FORECASTMODE,DIM_PROJECTSOURCEID,DD_LATESTREPORTING,CT_FORECASTQUANTITY_CUSTOMER,
DD_FORECASTGRAINATTRONE,DD_FORECASTGRAINATTRTWO,DD_FORECASTGRAINATTRTHREE,DD_FORECASTGRAINATTRFOUR,DD_FORECASTGRAINATTRFIVE,
DD_FORECASTGRAIN,CT_SALESQUANTITY_NEW,CT_SALESQUANTITY,AMT_STDCOSTPERUNIT,AMT_SELLINGPRICEPERUNIT,DIM_SALESUOMID,DD_LASTDATE,DD_HOLDOUTDATE,
f.DD_REPORTINGDATE,CT_LOWPI,CT_HIGHPI,CT_SALES_PREVMONTH,CT_SALES_PREVQTR,CT_SALES_PREVYR,CT_FORECAST_PREVMONTH,CT_FORECAST_PREVQTR,CT_FORECAST_PREVYR,
CT_SALES_CURRENTQTR,CT_FORECAST_CURRENTQTR,CT_SALES_PREVQTR_PREVYEAR,CT_FORECAST_PREVQTR_PREVYEAR,DIM_PART_PRESCRIPTIVEID,
DIM_PLANT_PRESCRIPTIVEID,STD_EXCHANGERATE_DATEID,DD_LASTREFRESHDATE,DD_PARTNUMBER,DD_PLANTCODE,CT_BIAS_ERROR,CT_BIAS_ERROR_RANK,CT_MAPE_CUSTOMERFCST,
CT_SALESCONTRIBGLOBAL,CT_RATIO_MAPE,DD_SALESCONTRIBUTION_QUARTILE_RANK,AMT_SHIPPEDQTYYTD,AMT_EXCHANGERATE_GBL,AMT_EXCHANGERATE,CT_DELIVEREDQTY_YTD,DD_SALES_COCD,DD_REPORTING_COMPANY_CODE,
DD_HEI_CODE,DD_COUNTRY_DESTINATION_CODE,DD_MARKET_GROUPING,CT_NASP,CT_NASP_CY,CT_NASP_GLOBAL,CT_NASP_PY,CT_NASP_FC_CY,CT_NASP_FC_NY,DD_MARKET_COUNTRY,CT_COEFFOFVARIATION,
DD_SALESCONTRIBUTION_3LEVELS_RANK,CT_VOLATILITY_YOY_PERCENT,DD_VOLATILITY_YOY_GT50PC,DD_FORECASTQUANTITY,CT_SALESDELIV2MTHSIRU,CT_SALESDELIV2MTHSNASP,DD_DYNAMICCONTRIBUTIONRANK,
DD_DYNAMICCONTRIBUTION,DD_HOME_VS_EXPORT,CT_COEFFOFVARIATION_FORECAST,DD_FORECASTPRODUCTCATEGORY,DD_HEICODE_NEW,DD_FLAG_SALESQUANTITY_EDITABLE,DD_PLANTTITLEMERCK,DD_PLANTCODEMERCK,
DD_REPORTAVAILABLE,CT_SALESDELIVEREDPY,CT_SALESDELIVEREDMONTH,DD_FORECASTRANK2,DD_INSERTSOURCE,CT_SALESDELIVACTUALYTD,CT_FORECAST4MTH1,CT_FORECAST4MTH2,CT_SALESMONTH1,CT_SALESMONTH2,
DD_FORECAST3MTHISNULL,DD_FORECAST4MTHIS1NULL,DD_FORECAST4MTHIS2NULL,DD_FORECAST4MTHISNULL,DD_FORECAST4MTHISNULL_SUBPRODLEV,DD_FORECAST6MTHISNULL,DD_FORECAST9MTHISNULL,CT_SALESDELIVMOVING,
DD_FOMAPE,DD_CURRENTMAPE,CT_SALESDELIVMOVINGUSD,CT_MAPE,CT_SALESDELIVACTUALYTD_TESTGH,CT_SALESDELIVMOVINGUSD_TESTGH,DIM_CURRENCYID,CT_SALESQUANTITYPREVMONTH,CT_FORECASTQUANTITY4MTH1,
DD_HOLDOUT,CT_FORECASTQUANTITY4MTH2,DD_FORECASTSELECTED,CT_FODFA_4,DD_UNIQUEKEY,CT_MAPE12MONTHS,CT_MAPECUSTOMERFORECAST12MONTHS,DD_LOWERMAPE,DD_CATEGORY,DD_COMMENTS,CT_FORECASTQUANTITYRANK1,
CT_FORECASTQUANTITY_CUSTOMER_RANK1,DD_REGION,DD_FORECASTAPPROACH,f.DW_UPDATE_DATE,FLAG_UPDATE,DD_FORECASTCHANGED_BYMERCK,CT_MONTHSSINCELASTCHANGE,CT_FORECASTSELECTEDLASTMONTH,CT_FORECASTQUANTITYSELECTED,
CT_SALESDELIVMOVING_NEW,
DD_FORECASTSELECTEDFINAL,DD_NEWMATERIALNUMBER,
DD_OLDATERIALNUMBER,dd_confirmed,DD_FORECASTSELECTED_ORIG,DD_CURRENCYCODE,CT_EXCHANGERATE_PL
) t;
/* This logic was implemented to bring the quantity for current reporting month but from next year and transpose it to current forecast month but for current year +2 for forecast quantity customer, as from PMRA we have it only until previous month*/
/* Example if reporting Month is 201806 then we are taking the vaule from 201906 and update it also for 202006 as for 2020 we have it populated until 202005 from PMRA*/

drop table if exists  tmp_for_24automation;
create table tmp_for_24automation as
select distinct ct_forecastquantity_customer,dd_forecastdate,dd_partnumber,dd_plantcode,dd_reporting_company_code,dd_country_destination_code, dd_forecastrank,f.dd_reportingdate,dd_market_grouping,
concat(year(to_date(f.dd_reportingdate,'DD MON YYYY'))+2,case when length(month(to_date(f.dd_reportingdate,'DD MON YYYY')))=1 then concat('0',month(to_date(f.dd_reportingdate,'DD MON YYYY'))) else month(to_date(f.dd_reportingdate,'DD MON YYYY')) end) as next2yearsforecast
 from fact_fosalesforecast f,tmp_maxrptdate r
where TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
and concat(year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')), case when length(month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')))=1 then concat('0',month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))) else month(to_date(to_char(dd_forecastdate),'YYYYMMDD')) end) =concat(year(to_date(f.dd_reportingdate,'DD MON YYYY'))+1,case when length(month(to_date(f.dd_reportingdate,'DD MON YYYY')))=1 then concat('0',month(to_date(f.dd_reportingdate,'DD MON YYYY'))) else month(to_date(f.dd_reportingdate,'DD MON YYYY')) end);


update  fact_fosalesforecast f
set f.ct_forecastquantity_customer=t.ct_forecastquantity_customer
from fact_fosalesforecast f,tmp_for_24automation t
where f.dd_partnumber=t.dd_partnumber
and f.dd_plantcode=t.dd_plantcode
and f.dd_reporting_company_code=t.dd_reporting_company_code
and f.dd_country_destination_code=t.dd_country_destination_code
and f.dd_reportingdate=t.dd_reportingdate
and f.dd_forecastrank=t.dd_forecastrank
and f.dd_market_grouping=t.dd_market_grouping
and concat(year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')), case when length(month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')))=1 then concat('0',month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD'))) else month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) end) =t.next2yearsforecast
and f.ct_forecastquantity_customer=0;

/*28 Aug 2018 Georgiana changes adding new MAT measure according to APP-9991*/

drop table if exists tmp_movinganualtotal;
create table tmp_movinganualtotal as
select distinct t1.sales_uin,t1.sales_cocd,t1.calendarmonthid,t1.reporting_company_code,t1.country_destination_code,
sum(t2.salesquantity) as salesmovingtotalMAT
from
(select sales_uin, sales_cocd,d.calendarmonthid,d.calendaryear,d.calendarmonthnumber,reporting_company_code,case when sales_cocd in ('NL10','XX20') then country_destination_code else 'Not Set' end as country_destination_code,sum(buom_quantity) as salesquantity
from atlas_forecast_sales_merck_DC,dim_date d
where d.datevalue=to_date(case when sales_reporting_period is null then '00010101' else sales_reporting_period||'01' end ,'YYYYMMDD')
and d.companycode='Not Set'
and d.plantcode_factory='Not Set'
group by sales_uin, sales_cocd,d.calendarmonthid,d.calendaryear,d.calendarmonthnumber,reporting_company_code,case when sales_cocd in ('NL10','XX20') then country_destination_code else 'Not Set' end) t1,
(select sales_uin, sales_cocd,d.calendarmonthid,d.calendaryear,d.calendarmonthnumber,reporting_company_code,case when sales_cocd in ('NL10','XX20') then country_destination_code else 'Not Set' end as country_destination_code,sum(buom_quantity) as salesquantity
from atlas_forecast_sales_merck_DC,dim_date d
where d.datevalue=to_date(case when sales_reporting_period is null then '00010101' else sales_reporting_period||'01' end ,'YYYYMMDD')
and d.companycode='Not Set'
and d.plantcode_factory='Not Set'
group by sales_uin, sales_cocd,d.calendarmonthid,d.calendaryear,d.calendarmonthnumber,reporting_company_code,case when sales_cocd in ('NL10','XX20') then country_destination_code else 'Not Set' end) t2
where
t1.sales_uin=t2.sales_uin and t1.sales_cocd=t2.sales_cocd
and t1.reporting_company_code=t2.reporting_company_code
and t1.country_destination_code=t2.country_destination_code
and ((t2.calendaryear=t1.calendaryear and t2.calendarmonthnumber<=t1.calendarmonthnumber) or
( t2.calendarmonthnumber > case when t1.calendarmonthnumber = 12 then 1 else t1.calendarmonthnumber end 
and t2.calendaryear = case when t1.calendarmonthnumber =12 then t1.calendaryear else t1.calendaryear-1 end))
group by t1.sales_uin,t1.sales_cocd,t1.calendarmonthid,t1.reporting_company_code,t1.country_destination_code;

merge into fact_fosalesforecast f
using (select distinct fact_fosalesforecastid,t.salesmovingtotalMAT
from fact_fosalesforecast f, tmp_movinganualtotal t, tmp_maxrptdate r
where dd_partnumber=sales_uin
and dd_plantcode=sales_cocd
and dd_reporting_company_code=reporting_company_code
and dd_country_destination_code=country_destination_code
and year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) =t.calendarmonthid
and  TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate) t
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set CT_SALESDELIVMOVING_NEW=t.salesmovingtotalMAT;
                                                               
/* 29 Mar 2017 Georgiana Changes adding Sales Amount YTD according to BI-4363*/
drop table if exists tmp_for_upd_salesdelivactualytd;
create table tmp_for_upd_salesdelivactualytd as
select f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
ct_salesdelivactualytd*ct_nasp as col01 
from fact_atlaspharmlogiforecast_merck f,dim_part dp,dim_Date dt
where f.dim_partid=dp.dim_partid 
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dt.datevalue='2016-02-01'*/
and dim_dateidreporting=dt.dim_dateid
and  dd_version='SFA';
/*group by f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end)*/


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
sum(f.col01) as col01,
dt.calendarmonthid 
from tmp_for_upd_salesdelivactualytd f, fact_fosalesforecast fos,dim_date dt,dim_part dp, tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(f.country_destination_code_upd,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
and TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dd_version='SFA'
and dt.calendarmonthid like '2017%'
and dd_reportingdate='08 Feb 2017'
and dd_forecastrank='1'
---and dd_forecastdate='20160229'*/
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 
fos.ct_salesdelivactualytd  = ifnull(t.col01,0);
/*28 Mar 2017 End of changes*/                                                               

/* 07 Sept 2018 IDP skill changes: default the Forecast Qunatity selected to  CF  */
update fact_fosalesforecast fos
set fos.CT_FORECASTQUANTITYSELECTED=fos.ct_forecastquantity_customer
from fact_fosalesforecast fos, tmp_maxrptdate r
where  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate;


/* 23 Nov 2018 Georgiana Changes calculating the months between last change date and current date whenever we have a new run according to Bug found on APP-10678*/

merge into fact_fosalesforecast fosf
using (
select distinct fosf.fact_fosalesforecastid,
round(min(months_between(current_date,changed_date)),0) as ct_monthssincelastchange
 from fact_fosalesforecast fosf,dim_autidtrailforpredictiveforecast atfp,tmp_maxrptdate r
where   atfp.dd_partnumber=fosf.dd_partnumber
and atfp.dd_plantcode=fosf.dd_plantcode 
/*and fosf.dd_reportingdate = atfp.dd_reportingdate*/
and dd_holdout=3
and TO_DATE(fosf.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*and atfp.last_entry='Y'*/
group by fosf.fact_fosalesforecastid) t
on t.fact_fosalesforecastid=fosf.fact_fosalesforecastid
when matched then update set fosf.ct_monthssincelastchange=t.ct_monthssincelastchange;


/* App- 10710 Georgiana Changes adding currency details based on csv file from Merck*/

merge into fact_fosalesforecast f
using(
select distinct f.fact_fosalesforecastid,currency_code,actual_exchange_rate_pl from fact_fosalesforecast f,tmp_maxrptdate r,
(select distinct plant_code, currency_code,first_value(actual_exchange_rate_pl) over(partition by plant_code, currency_code order by period desc) as actual_exchange_rate_pl from PMRA_LOCAL_CURRENCY_RATES
where actual_exchange_rate_pl<>0) t1
where  DD_PLANTCODEMERCK=plant_code
and TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate) t
on f.fact_fosalesforecastid = t.fact_fosalesforecastid
when matched then update set dd_currencycode =currency_code,
ct_exchangerate_pl = actual_exchange_rate_pl;


