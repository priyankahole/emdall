/*****************************************************************************************************
Creation Date : Sept 18, 12013
Create By : Hiten Suthar
*****************************************************************************************************/


DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT CONVERT(varchar(3), ifnull((SELECT property_value
                                   FROM systemproperty
                                   WHERE property = 'customer.global.currency'),
                                  'USD')) as pGlobalCurrency;

UPDATE fact_inventoryaging f
SET dim_Currencyid_GBL = c.dim_Currencyid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryaging f CROSS JOIN tmp_GlobalCurr_001 gbl inner join dim_currency c on gbl.pGlobalCurrency = c.CurrencyCode
where ifnull(dim_Currencyid_GBL,-1) <> ifnull(c.dim_Currencyid,-2);

update fact_inventoryaging
set ct_GlobalOnHandAmt_Merck = amt_OnHand * amt_ExchangeRate_GBL,
	ct_GlobalExtTotalCost_Merck = (ct_StockQty + ct_StockInQInsp + ct_BlockedStock + ct_StockInTransit + ct_StockInTransfer + ct_TotalRestrictedStock)
					* (amt_MtlDlvrCost + amt_OverheadCost + amt_OtherCost)
					* amt_ExchangeRate_GBL,
	/*ct_localonhandamt_merck = amt_OnHand - 03 Oct Changes Georgiana Changes According to BI-4230*/
	ct_localexttotalcost_merck = (ct_StockQty + ct_StockInQInsp + ct_BlockedStock + ct_StockInTransit + ct_StockInTransfer + ct_TotalRestrictedStock)
					* (amt_MtlDlvrCost + amt_OverheadCost + amt_OtherCost)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/;

update fact_inventoryaging
set ct_localonhandamt_merck = 
case when priceunit = 0 then (f.ct_StockQty + f.ct_StockInQInsp + f.ct_BlockedStock + f.ct_StockInTransit + f.ct_StockInTransfer + f.ct_TotalRestrictedStock)*standardprice_mbew
else  (f.ct_StockQty + f.ct_StockInQInsp + f.ct_BlockedStock + f.ct_StockInTransit + f.ct_StockInTransfer + f.ct_TotalRestrictedStock)*standardprice_mbew/priceunit  end
 from fact_inventoryaging f, dim_part dp 
 where f.dim_partid=dp.dim_partid;
 
 update fact_inventoryaging
set amt_OnHand = 
case when priceunit = 0 then (f.ct_StockQty + f.ct_StockInQInsp + f.ct_BlockedStock + f.ct_StockInTransit + f.ct_StockInTransfer + f.ct_TotalRestrictedStock)*standardprice_mbew
else  (f.ct_StockQty + f.ct_StockInQInsp + f.ct_BlockedStock + f.ct_StockInTransit + f.ct_StockInTransfer + f.ct_TotalRestrictedStock)*standardprice_mbew/priceunit  end
 from fact_inventoryaging f, dim_part dp 
 where f.dim_partid=dp.dim_partid;
 
 /*End of 03 Oct Changes*/


/* Nicoleta 14.05.2014 */
/*drop table if exists tmp_ccpdatags_upd_budgetstdprice
create table tmp_ccpdatags_upd_budgetstdprice as
 select distinct plantcode, uin,  ccpbudgetunitprice
from ccpdatagscost
where uin not like 'INV%' and ccpbudgetunitprice <> 0

update fact_inventoryaging f
from tmp_ccpdatags_upd_budgetstdprice u, dim_part dp, dim_plant pl
set amt_gblStdPrice_Merck = ccpbudgetunitprice
WHERE   dp.partnumber = u.uin
        and f.dim_partid = dp.dim_partid
		AND u.plantcode = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode =pl.plantcode

drop table if exists tmp_ccpdatags_upd_budgetstdprice
*/

merge into fact_inventoryaging f
using (select distinct f.fact_inventoryagingid,max(ccpbudgetunitprice) as ccpbudgetunitprice
from  
ccpstpricefile201602 u, dim_part dp, dim_plant pl, fact_inventoryaging f
WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcode = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode =pl.plantcode
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
	group by f.fact_inventoryagingid) t
on t.fact_inventoryagingid=f.fact_inventoryagingid
when matched then update set f.amt_gblStdPrice_Merck = t.ccpbudgetunitprice;

/*andrei APP 4243 updating with values from 2017 ( previos) when 2018 (latest is 0) */
merge into fact_inventoryaging f
using (select distinct f.fact_inventoryagingid,max(uu.ccpbudgetunitprice) as ccpbudgetunitprice
from  
ccpstpricefile201602 u, dim_part dp, dim_plant pl, fact_inventoryaging f, ccpstpricefile201602old uu
WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcode = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode =pl.plantcode
		and u.ccpbudgetunitprice = 0
		and u.plantcode = uu.plantcode
		and u.plantcountry = uu.plantcountry
		and u.partnumber = uu.partnumber
		and uu.ccpbudgetunitprice <> 0
		and amt_gblStdPrice_Merck <> uu.ccpbudgetunitprice
	group by f.fact_inventoryagingid) t
on t.fact_inventoryagingid=f.fact_inventoryagingid
when matched then update set f.amt_gblStdPrice_Merck = t.ccpbudgetunitprice;
/*andrei */

/*update fact_inventoryaging f
from
ccpdatagscost_new_2015 u, dim_part dp, dim_plant pl
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcountry = substr(pl.plantcode,1,2)
		and f.dim_plantid = pl.dim_plantid
		and  dp.plant =pl.plantcode
		and u.plantcode<>pl.plantcode
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
*/
drop table if exists tmp_upd_gblStdPrice_Merck2;
create table tmp_upd_gblStdPrice_Merck2 as
select   max(u.ccpbudgetunitprice) as ccpbudgetunitprice,  f.dim_plantid,
f.dim_partid
from fact_inventoryaging f,
ccpstpricefile201602 u, dim_part dp, dim_plant pl
WHERE lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
and f.dim_partid = dp.dim_partid
AND u.plantcountry = substr(pl.plantcode,1,2)
and f.dim_plantid = pl.dim_plantid
and u.plantcode<>pl.plantcode
and not exists ( select 1 from ccpstpricefile201602 u2
where u2.plantcode = dp.plant
	and lpad(dp.partnumber,8,'0') = lpad(u2.partnumber,8,'0'))
	group by f.dim_plantid,
f.dim_partid;

update fact_inventoryaging f
set f.amt_gblStdPrice_Merck = u.ccpbudgetunitprice
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_upd_gblStdPrice_Merck2 u, fact_inventoryaging f
WHERE f.dim_partid = u.dim_partid
and f.dim_plantid = u.dim_plantid
and ifnull(f.amt_gblStdPrice_Merck,-99) <> u.ccpbudgetunitprice;

/* Nicoleta 14.05.2014 */


/* 28 oct 2014 StdPricePMRA change */

/*
update
fact_inventoryaging f
from stdprice_pmra_merck_v3 u, dim_part dp, dim_plant pl
set f.amt_StdPricePMRA_Merck = u.stdprice_pmra
WHERE   lpad(dp.partnumber,8,'0') =  lpad(u.uin,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plant = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plant =pl.plantcode
		and ifnull(f.amt_StdPricePMRA_Merck,-99) <> u.stdprice_pmra
		*/

update fact_inventoryaging f
set f.amt_StdPricePMRA_Merck = u.stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from  
(select distinct u.partnumber,u.plantcode, max(u.stdprice_pmra) as stdprice_pmra from ccpstpricefile201602 u group by u.partnumber,u.plantcode) u , dim_part dp, dim_plant pl, fact_inventoryaging f
WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcode = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode =pl.plantcode
		and ifnull(f.amt_StdPricePMRA_Merck,-99) <> u.stdprice_pmra;


/*update fact_inventoryaging f
from
stdprice_pmra_merck_new_2015 u, dim_part dp, dim_plant pl
set f.amt_StdPricePMRA_Merck = u.stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcountry = substr(pl.plantcode,1,2)
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode<>pl.plantcode
		and ifnull(f.amt_StdPricePMRA_Merck,-99) <> u.stdprice_pmra
*/
drop table if exists tmp_upd_StdPricePMRA_Merck3;
create table tmp_upd_StdPricePMRA_Merck3 as
select   max(u.stdprice_pmra) as stdprice_pmra,  f.dim_plantid,
f.dim_partid
from fact_inventoryaging f,
ccpstpricefile201602 u, dim_part dp, dim_plant pl
WHERE lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
and f.dim_partid = dp.dim_partid
AND u.plantcountry = substr(pl.plantcode,1,2)
and f.dim_plantid = pl.dim_plantid
and u.plantcode<>pl.plantcode
and not exists ( select 1 from ccpstpricefile201602 u2
where u2.plantcode = dp.plant
	and lpad(dp.partnumber,8,'0') = lpad(u2.partnumber,8,'0'))
	group by f.dim_plantid,
f.dim_partid;

update fact_inventoryaging f
set f.amt_StdPricePMRA_Merck = u.stdprice_pmra
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from
tmp_upd_StdPricePMRA_Merck3 u, fact_inventoryaging f
WHERE f.dim_partid = u.dim_partid
and f.dim_plantid = u.dim_plantid
and ifnull(f.amt_StdPricePMRA_Merck,-99) <> u.stdprice_pmra;

drop table if exists tmp_upd_StdPricePMRA_Merck3;



/* 28 oct 2014 StdPricePMRA change */

/* 10 feb 2015 update to IRU */




update fact_inventoryaging f
set ct_StockQtyIRU = ct_StockQty/marm_umrez
from  dim_part pt,MARM m, fact_inventoryaging f
where pt.dim_partid = f.dim_partid and m.marm_matnr = pt.partnumber and marm_meinh = 'IRU'
and ifnull(ct_StockQtyIRU,-1) <> ct_StockQty/marm_umrez;

update fact_inventoryaging f
set ct_TotalRestrictedStockIRU = ct_TotalRestrictedStock/marm_umrez
from  dim_part pt,MARM m, fact_inventoryaging f
where pt.dim_partid = f.dim_partid and m.marm_matnr = pt.partnumber and marm_meinh = 'IRU'
and ifnull(ct_TotalRestrictedStockIRU,-1) <> ct_TotalRestrictedStock/marm_umrez;

update fact_inventoryaging f
set ct_StockInTransferIRU = ct_StockInTransfer/marm_umrez
from  dim_part pt,MARM m, fact_inventoryaging f
where pt.dim_partid = f.dim_partid and m.marm_matnr = pt.partnumber and marm_meinh = 'IRU'
and ifnull(ct_StockInTransferIRU,-1) <> ct_StockInTransfer/marm_umrez;

update fact_inventoryaging f
set ct_StockInQInspIRU = ct_StockInQInsp/marm_umrez
from  dim_part pt,MARM m, fact_inventoryaging f
where pt.dim_partid = f.dim_partid and m.marm_matnr = pt.partnumber and marm_meinh = 'IRU'
and ifnull(ct_StockInQInspIRU ,-1) <> ct_StockInQInsp/marm_umrez;

update fact_inventoryaging f
set ct_BlockedStockIRU = ct_BlockedStock/marm_umrez
from  dim_part pt,MARM m, fact_inventoryaging f
where pt.dim_partid = f.dim_partid and m.marm_matnr = pt.partnumber and marm_meinh = 'IRU'
and ifnull(ct_BlockedStockIRU ,-1) <> ct_BlockedStock/marm_umrez;

update fact_inventoryaging f
set ct_StockInTransitIRU = ct_StockInTransit/marm_umrez
from  dim_part pt,MARM m, fact_inventoryaging f
where pt.dim_partid = f.dim_partid and m.marm_matnr = pt.partnumber and marm_meinh = 'IRU'
and ifnull(ct_StockInTransitIRU  ,-1) <> ct_StockInTransit/marm_umrez;


update fact_inventoryaging f
set ct_onhandqtyIRU  = ct_StockQtyIRU + ct_StockInQInspIRU + ct_BlockedStockIRU
 + ct_StockInTransferIRU + ct_TotalRestrictedStockIRU
+ct_intransitstockqty
where  ifnull(ct_onhandqtyIRU ,-1) <> ct_StockQtyIRU + ct_StockInQInspIRU + ct_BlockedStockIRU
 + ct_StockInTransferIRU + ct_TotalRestrictedStockIRU
 +ct_intransitstockqty;

 /*update ct_onhandqtyIRU with values for STOCKQTY for plantcodes in newiteminstockbrinit - APP-3134 alin 16 Nov*/
merge into fact_inventoryaging f
using(
select distinct f.fact_inventoryagingid, dp.partnumber, pl.plantcode, STOCKQTY
from fact_inventoryaging f, dim_part dp, dim_plant pl, newiteminstockbrinit n
where f.dim_plantid = pl.dim_plantid
and f.dim_partid = dp.dim_partid
and pl.plantcode = dp.plant
and n.plantcode = pl.plantcode
and n.uin = dp.partnumber
and pl.plantcode in (select distinct plantcode from newiteminstockbrinit)
) t
on f.fact_inventoryagingid = t.fact_inventoryagingid
when matched then update set
f.ct_onhandqtyIRU = t.STOCKQTY
where f.ct_onhandqtyIRU <> t.STOCKQTY;



/* end 10 feb 2015 update to IRU */



/* 07-04-2015  Adding dd_sitecodefore2e,dd_sitecodefore2etitle from dim_plant */
update
fact_inventoryaging f
set dd_sitecodefore2e = ifnull(case when pl.tacticalring_merck <> 'ComOps' then pl.plantcode
else pt.primarysite end,'Not Set')
from dim_part pt, dim_plant pl, fact_inventoryaging f
where f.dim_partid = pt.dim_partid
and pl.dim_plantid = f.dim_plantid
and dd_sitecodefore2e <> ifnull(case when pl.tacticalring_merck <> 'ComOps' then pl.plantcode
else pt.primarysite end,'Not Set');

update fact_inventoryaging f
set dd_sitecodefore2etitle = ifnull(planttitle_merck,'Not Set')
from dim_plant pl, fact_inventoryaging f
where pl.plantcode = f.dd_sitecodefore2e
and dd_sitecodefore2etitle <> ifnull(planttitle_merck,'Not Set');
/* 07-04-2015 END OF CHANGES */

/* 15.04.2015 */
update
fact_inventoryaging f
set dim_plantidsitefore2e = ifnull(site.dim_plantid ,1)
from  dim_part pt, dim_plant pl,dim_plant site, fact_inventoryaging f
where f.dim_partid = pt.dim_partid
and f.dim_plantid = pl.dim_plantid
and site.plantcode = ifnull(case when pl.tacticalring_merck <> 'ComOps' then pl.plantcode
else pt.primarysite end,'Not Set')
and dim_plantidsitefore2e <> ifnull(site.dim_plantid ,1);

/* 08.07.2015 add Restricted measures from xls */
drop table if exists tmp_reservedqtyforstock_merck;
create table tmp_reservedqtyforstock_merck
as select plantcode,uin,batch,sum(reservedqty) as reservedqty
from reservedqtyforstock_merck
group by plantcode,uin,batch;

drop table if exists tmp_stock_reservedqty_upd;
create table tmp_stock_reservedqty_upd
as select fact_inventoryagingid, ifnull(reservedqty,0) reservedqty,
row_number() over (partition by f.dim_partid,f.dim_plantid,dd_batchno ORDER BY '') as rowseqno
from fact_inventoryaging f
, tmp_reservedqtyforstock_merck r,
dim_part pt, dim_plant pl
where f.dim_partid = pt.dim_partid
and f.dim_plantid = pl.dim_plantid
and r.plantcode = pl.plantcode
and  r.uin  = pt.partnumber
and r.batch = dd_batchno
and ct_reservedqty <> ifnull(reservedqty,0);

update fact_inventoryaging f
set ct_reservedqty = ifnull(reservedqty,0)
from tmp_stock_reservedqty_upd r, fact_inventoryaging f
where f.fact_inventoryagingid = r.fact_inventoryagingid
and r.rowseqno = 1
and ct_reservedqty <> ifnull(reservedqty,0);

drop table if exists tmp_reservedqtyforstock_merck;
drop table if exists tmp_stock_reservedqty_upd;

 update fact_inventoryaging f
set ct_reservedqtyiru = ct_reservedqty/marm_umrez
from  MARM m, dim_part pt, fact_inventoryaging f
where f.dim_partid = pt.dim_partid and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and ct_reservedqtyiru <> ct_reservedqty/marm_umrez;

/*Andrian - Based on ticket BI-1546*/
update fact_inventoryaging
set dim_plantidstockatsite = 1
where dim_plantidstockatsite <> 1;

drop table if exists tmp01_plantstockatsite;

create table tmp01_plantstockatsite as
select a.fact_inventoryagingid,
       ifnull(c.dim_plantidordering,d.dim_plantidordering) as dim_plantidstockatsite
       from fact_inventoryaging a
                         left join
              (select distinct dd_salesdocno,dd_salesitemno,dd_ourreference from fact_salesorder
                       where dd_salesdocno<>'Not Set' and dd_salesitemno<>0) b
                         on a.dd_DocumentNo = b.dd_salesdocno
                    and a.dd_documentitemno = b.dd_salesitemno
                          left join
			  (select distinct dd_DocumentNo,dd_documentitemno,dim_plantidordering from fact_purchase
                       where dd_DocumentNo<>'Not Set' and dd_documentitemno<>0 ) c
                         on a.dd_DocumentNo = c.dd_DocumentNo
                    and a.dd_documentitemno = c.dd_documentitemno
                          left join
			  (select distinct dd_ourreference,dim_plantidordering from fact_purchase
                       where dd_ourreference <> 'Not Set') d
                         on b.dd_ourreference = d.dd_ourreference
where ifnull(c.dim_plantidordering,d.dim_plantidordering) is not null;

/*update fact_inventoryaging a
set a.dim_plantidstockatsite = b.dim_plantidstockatsite
from tmp01_plantstockatsite b, fact_inventoryaging a
where a.fact_inventoryagingid = b.fact_inventoryagingid
 and a.dim_plantidstockatsite <> b.dim_plantidstockatsite*/
 
 MERGE INTO fact_inventoryaging a
 USING (SELECT a.fact_inventoryagingid fact_inventoryagingid, MAX(b.dim_plantidstockatsite) as dim_plantidstockatsite
		from tmp01_plantstockatsite b, fact_inventoryaging a
		 where a.fact_inventoryagingid = b.fact_inventoryagingid
		 GROUP BY a.fact_inventoryagingid) x
ON (a.fact_inventoryagingid = x.fact_inventoryagingid)
WHEN MATCHED THEN
UPDATE SET a.dim_plantidstockatsite = x.dim_plantidstockatsite
where a.dim_plantidstockatsite <> x.dim_plantidstockatsite;


DROP TABLE IF EXISTS tmp_GlobalCurr_001;

update fact_inventoryaging f
set f.amt_stdcostloc_Merck = u.stdcostloc
from (SELECT DISTINCT u.partnumber,u.plantcode,max(u.stdcostloc) AS stdcostloc from ccpstpricefile201602 u GROUP BY u.partnumber,u.plantcode) u, dim_part dp, dim_plant pl, fact_inventoryaging f
WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcode = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode =pl.plantcode
		and ifnull(f.amt_stdcostloc_Merck,-99) <> u.stdcostloc;


update fact_inventoryaging f
set f.amt_stdcostloc_Merck = u.stdcostloc
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from (SELECT DISTINCT u.partnumber,plantcountry,max(u.stdcostloc) AS stdcostloc from ccpstpricefile201602 u, dim_plant pl WHERE u.plantcode<>pl.plantcode AND u.plantcountry = substr(pl.plantcode,1,2) GROUP BY u.partnumber,plantcountry) u, dim_part dp, dim_plant pl, fact_inventoryaging f
WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcountry = substr(pl.plantcode,1,2)
		and f.dim_plantid = pl.dim_plantid
		and ifnull(f.amt_stdcostloc_Merck,-99) <> u.stdcostloc;

/* Primary Site Code and Part Number	*/

update fact_inventoryaging f
set amt_stdcostloc_Merck = stdcostloc
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from (SELECT partnumber,plantcode,max(stdcostloc) AS stdcostloc FROM ccpstpricefile201602 GROUP by partnumber,plantcode) u, dim_part dp, fact_inventoryaging f
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid /*and  dp.plant = 'XX20'*/
		AND u.plantcode = /*dp.plant*/ dp.primarysite
		and amt_stdcostloc_Merck <> stdcostloc
		and amt_stdcostloc_Merck = 0;


/* Primary Site Country Code and Part Number */


drop table if exists tmp_stdcostloc_upd2;
create table tmp_stdcostloc_upd2 as
select fact_inventoryagingid, avg(stdcostloc) as stdcostloc
from
fact_inventoryaging f,
ccpstpricefile201602 u, dim_part dp, dim_plant pl
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcountry = pl.country
		and  dp.primarysite = pl.plantcode
		and amt_stdcostloc_Merck <> stdcostloc
		and amt_stdcostloc_Merck = 0
group by fact_inventoryagingid;

 update fact_inventoryaging f
set amt_stdcostloc_Merck = stdcostloc
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from
tmp_stdcostloc_upd2 u, fact_inventoryaging f
WHERE  f.fact_inventoryagingid = u.fact_inventoryagingid
		and amt_stdcostloc_Merck <> stdcostloc
		and amt_stdcostloc_Merck = 0;

drop table if exists tmp_stdcostloc_upd2;


/* Primary Site Code and Part Number	*/

update fact_inventoryaging f
set amt_StdPricePMRA_Merck = stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from
(SELECT u.partnumber,u.plantcode,avg(u.stdprice_pmra) AS stdprice_pmra FROM ccpstpricefile201602 u GROUP BY u.partnumber,u.plantcode) u, dim_part dp, fact_inventoryaging f
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid /*and  dp.plant = 'XX20'*/
		AND u.plantcode = /*dp.plant*/ dp.primarysite
		and amt_StdPricePMRA_Merck <> stdprice_pmra
		and amt_StdPricePMRA_Merck = 0;


/* Primary Site Country Code and Part Number */


drop table if exists tmp_stdprice_pmra_upd2;
create table tmp_stdprice_pmra_upd2 as
select fact_inventoryagingid, avg(stdprice_pmra) as stdprice_pmra
from
fact_inventoryaging f,
ccpstpricefile201602 u, dim_part dp, dim_plant pl
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcountry = pl.country
		and  dp.primarysite = pl.plantcode
		and amt_StdPricePMRA_Merck <> stdprice_pmra
		and amt_StdPricePMRA_Merck = 0
group by fact_inventoryagingid;

 update fact_inventoryaging f
set amt_StdPricePMRA_Merck = stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from
tmp_stdprice_pmra_upd2 u, fact_inventoryaging f
WHERE  f.fact_inventoryagingid = u.fact_inventoryagingid
		and amt_StdPricePMRA_Merck <> stdprice_pmra
		and amt_StdPricePMRA_Merck = 0;

drop table if exists tmp_stdprice_pmra_upd2;

/* Primary Site Code and Part Number	*/

update fact_inventoryaging f
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from
(SELECT u.partnumber,u.plantcode,max(ccpbudgetunitprice) AS ccpbudgetunitprice FROM ccpstpricefile201602 u GROUP BY u.partnumber,u.plantcode) u, dim_part dp , fact_inventoryaging f
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid /*and  dp.plant = 'XX20'*/
		AND u.plantcode = /*dp.plant*/ dp.primarysite
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0;


/* Primary Site Country Code and Part Number */


drop table if exists tmp_ccpbudgetunitprice_upd2;
create table tmp_ccpbudgetunitprice_upd2 as
select fact_inventoryagingid, max(ccpbudgetunitprice) as ccpbudgetunitprice
from
fact_inventoryaging f,
ccpstpricefile201602 u, dim_part dp, dim_plant pl
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcountry = pl.country
		and  dp.primarysite = pl.plantcode
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0
group by fact_inventoryagingid;

 update fact_inventoryaging f
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from
tmp_ccpbudgetunitprice_upd2 u, fact_inventoryaging f
WHERE  f.fact_inventoryagingid = u.fact_inventoryagingid
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0;

drop table if exists tmp_ccpbudgetunitprice_upd2;

/*	BI-4693 */
/*Part Number*/

drop table if exists tmp_ccpdatagscost_upd3;
create table tmp_ccpdatagscost_upd3 as	
select fact_inventoryagingid, avg(ccpbudgetunitprice) as ccpbudgetunitprice
from
fact_inventoryaging f, 
ccpstpricefile201602 u, dim_part dp, dim_plant pl 
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid=dp.dim_partid
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0
group by fact_inventoryagingid;	

 update fact_inventoryaging f
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from  
tmp_ccpdatagscost_upd3 u ,fact_inventoryaging f
WHERE  f.fact_inventoryagingid = u.fact_inventoryagingid
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0;

drop table if exists tmp_ccpdatagscost_upd3;
/*BI-4693 End*/

update fact_inventoryaging
set dim_partsalesid = 1
where dim_partsalesid is null;

update fact_inventoryaging
set dim_currencyid_gbl = 1
where dim_currencyid_gbl is null;

update fact_inventoryaging
set dim_productionorderstatusid = 1
where dim_productionorderstatusid is null;

update fact_inventoryaging
set dim_productionordertypeid = 1
where dim_productionordertypeid is null;

update fact_inventoryaging
set dim_dateidactualrelease = 1
where dim_dateidactualrelease is null;

update fact_inventoryaging
set dim_dateidtransaction = 1
where dim_dateidtransaction is null;

update fact_inventoryaging
set dim_dateidactualstart = 1
where dim_dateidactualstart is null;

/*Alin Changes - BI-4669 9 Nov 2016*/
update fact_inventoryaging f
set ct_onhandqty  = ct_StockQty + ct_StockInQInsp + ct_BlockedStock + ct_StockInTransit + ct_StockInTransfer + ct_TotalRestrictedStock
+ct_intransitstockqty
where  ifnull(ct_onhandqty ,-1) <> ct_StockQty + ct_StockInQInsp + ct_BlockedStock + ct_StockInTransit + ct_StockInTransfer + ct_TotalRestrictedStock
 +ct_intransitstockqty;
/*end Alin changes*/

/* 06 Dec 2016 Georgiana Changes according to BI-4933*/
update fact_inventoryaging set ct_totaliqvamountCCP=0;

drop table if exists tmp_for_upd_ct_totaliqvamountCCP;
create table tmp_for_upd_ct_totaliqvamountCCP as
select distinct f.dim_partid, f.dim_storagelocationid,f.dim_plantid,f.dd_batchno,
AVG(CASE WHEN DD_IQVRISKCODE='1' THEN case when prt.uomiru = 0 then (CT_IQVPROVISION*amt_gblstdprice_merck) else ((CT_IQVPROVISION*amt_gblstdprice_merck) / prt.uomiru) end  
WHEN DD_IQVRISKCODE='2' THEN ct_onhandqtyIRU * amt_gblstdprice_merck
WHEN DD_IQVRISKCODE='3' THEN ct_onhandqtyIRU * amt_gblstdprice_merck
ELSE 0 END) as ct_totaliqvamountCCP
From fact_inventoryaging f, dim_part prt
where f.dim_partid=prt.dim_partid
group by f.dim_partid, f.dim_storagelocationid,f.dim_plantid,f.dd_batchno;

merge into fact_inventoryaging f
using (select distinct f.fact_inventoryagingid, t.ct_totaliqvamountCCP
from fact_inventoryaging f,tmp_for_upd_ct_totaliqvamountCCP t
where
f.dim_partid=t.dim_partid 
and  f.dim_storagelocationid=t.dim_storagelocationid 
and f.dim_plantid=t.dim_plantid 
and f.dd_batchno=t.dd_batchno
) s
on f.fact_inventoryagingid=s.fact_inventoryagingid
when matched then update set f.ct_totaliqvamountCCP=s.ct_totaliqvamountCCP;

/* start BI-5595 ALin Gh 27 mar 2017*/
drop table if exists tmp_for_upd_ct_iqvnoprovision;
create table tmp_for_upd_ct_iqvnoprovision as
select distinct 
prt.dim_partid, 
prt.partnumber,
 f.dim_storagelocationid,f.dim_plantid,f.dd_batchno,
(CASE WHEN ( (SUM(case when prt.uomiru = 0 then 
(CT_IQVPROVISION*amt_gblstdprice_merck) else ((CT_IQVPROVISION*amt_gblstdprice_merck) / prt.uomiru) end ) 
) > 0) THEN 0 
ELSE sum( (ct_onhandqtyIRU * amt_gblstdprice_merck) ) 
END ) / count(*)
 as ct_iqvnoprovision
From fact_inventoryaging f, dim_part prt
where f.dim_partid=prt.dim_partid
group by 
prt.dim_partid, 
prt.partnumber, f.dim_storagelocationid,f.dim_plantid,f.dd_batchno;

update fact_inventoryaging f
set f.ct_iqvnoprovision = t.ct_iqvnoprovision
from fact_inventoryaging f,  tmp_for_upd_ct_iqvnoprovision t
where f.dim_partid = t.dim_partid
and  f.dim_storagelocationid = t.dim_storagelocationid 
and f.dim_plantid=t.dim_plantid 
and f.dd_batchno=t.dd_batchno
and f.ct_iqvnoprovision <> t.ct_iqvnoprovision;

drop table if exists tmp_for_upd_ct_iqvnoprovision;
/* end BI-5595 ALin Gh 27 mar 2017*/

/* start APP-6090 ALin Gh 25 apr 2017*/
/*Total IQV Amount (CCP)_v2 - GH*/
drop table if exists tmp_for_upd_ct_iqvnoprovision_v2;
create table tmp_for_upd_ct_iqvnoprovision_v2 as

select distinct 
prt.dim_partid, 
prt.partnumber,
 f.dim_storagelocationid,f.dim_plantid,f.dd_batchno,dd_status,

CASE WHEN MAX( (f.DD_IQVRISKCODE) ) = '1' THEN (sum( 
case when prt.uomiru = 0 then ((CASE WHEN f.DD_IQVRISKCODE = '1' and CT_IQVFIXED > '0'THEN CT_IQVFIXED 
WHEN f.DD_IQVRISKCODE = '1' and CT_IQVFIXED <= 0 THEN (f.ct_StockQty + f.ct_StockInQInsp + f.ct_BlockedStock + f.ct_StockInTransit + f.ct_StockInTransfer + f.ct_TotalRestrictedStock) 
ELSE 0 END) *amt_gblstdprice_merck) else 
(((CASE WHEN f.DD_IQVRISKCODE = '1' and CT_IQVFIXED > '0'THEN CT_IQVFIXED 
WHEN f.DD_IQVRISKCODE = '1' and CT_IQVFIXED <= 0 THEN (f.ct_StockQty + f.ct_StockInQInsp + f.ct_BlockedStock + f.ct_StockInTransit + f.ct_StockInTransfer + f.ct_TotalRestrictedStock) 
ELSE 0 END) *amt_gblstdprice_merck) / prt.uomiru) end )) 
WHEN MAX( (f.DD_IQVRISKCODE) ) = '2' THEN SUM( (ct_onhandqtyIRU * amt_gblstdprice_merck) )
WHEN MAX( (f.DD_IQVRISKCODE) ) = '3' THEN SUM( (ct_onhandqtyIRU * amt_gblstdprice_merck) )
ELSE SUM(0) END/count(*) as ct_iqvnoprovision_v2

From fact_inventoryaging f, dim_part prt
where f.dim_partid=prt.dim_partid
group by 
prt.dim_partid, 
prt.partnumber, f.dim_storagelocationid,f.dim_plantid,f.dd_batchno,dd_status;


update fact_inventoryaging f
set f.ct_iqvnoprovision_v2 = t.ct_iqvnoprovision_v2
from fact_inventoryaging f,  tmp_for_upd_ct_iqvnoprovision_v2 t
where f.dim_partid = t.dim_partid
and  f.dim_storagelocationid = t.dim_storagelocationid 
and f.dim_plantid=t.dim_plantid 
and f.dd_batchno=t.dd_batchno
and f.dd_status=t.dd_status
and f.ct_iqvnoprovision_v2 <> t.ct_iqvnoprovision_v2;

drop table if exists tmp_for_upd_ct_iqvnoprovision_v2;
/*Net Inventory (CCP) _V2*/
drop table if exists tmp_netinv_v2;
create table  tmp_netinv_v2 as 
select distinct 
prt.dim_partid, prt.partnumber, f_invagng.dim_storagelocationid, f_invagng.dim_plantid, f_invagng.dd_batchno, dd_status,
(SUM( (ct_onhandqtyIRU * amt_gblstdprice_merck) )- (sum( 
case when prt.uomiru = 0 then ((CASE WHEN f_invagng.DD_IQVRISKCODE = '1' and CT_IQVFIXED > '0'THEN CT_IQVFIXED 
WHEN f_invagng.DD_IQVRISKCODE = '1' and CT_IQVFIXED <= 0 THEN (f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) 
ELSE 0 END) *amt_gblstdprice_merck) else 
(((CASE WHEN f_invagng.DD_IQVRISKCODE = '1' and CT_IQVFIXED > '0'THEN CT_IQVFIXED 
WHEN f_invagng.DD_IQVRISKCODE = '1' and CT_IQVFIXED <= 0 THEN (f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) 
ELSE 0 END) *amt_gblstdprice_merck) / prt.uomiru) end )))/count(*) as ct_netinv_v2
from fact_inventoryaging f_invagng, 	dim_part	prt
where f_invagng.dim_partid=prt.dim_partid
group by prt.dim_partid, prt.partnumber, f_invagng.dim_storagelocationid,f_invagng.dim_plantid,f_invagng.dd_batchno,dd_status;


update fact_inventoryaging f
set f.ct_netinv_v2  = t.ct_netinv_v2
from fact_inventoryaging f, tmp_netinv_v2 t
where f.dim_partid = t.dim_partid
and  f.dim_storagelocationid = t.dim_storagelocationid 
and f.dim_plantid=t.dim_plantid 
and f.dd_batchno=t.dd_batchno
and f.dd_status=t.dd_status
and f.ct_netinv_v2 <> t.ct_netinv_v2;

drop table if exists tmp_netinv_v2;



/*ALIN 2 june 2017 APP-6090*/
drop table if exists IQV_Provision_Amount_CCP_v2;
create table  IQV_Provision_Amount_CCP_v2 as 
select distinct 
prt.dim_partid, prt.partnumber, f_invagng.dim_storagelocationid, f_invagng.dim_plantid, f_invagng.dd_batchno, dd_status, 
sum( 
case when prt.uomiru = 0 then ((CASE WHEN f_invagng.DD_IQVRISKCODE = '1' and CT_IQVFIXED > '0'THEN CT_IQVFIXED 
WHEN f_invagng.DD_IQVRISKCODE = '1' and CT_IQVFIXED <= 0 THEN (f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) 
ELSE 0 END) *amt_gblstdprice_merck) else 
(((CASE WHEN f_invagng.DD_IQVRISKCODE = '1' and CT_IQVFIXED > '0'THEN CT_IQVFIXED 
WHEN f_invagng.DD_IQVRISKCODE = '1' and CT_IQVFIXED <= 0 THEN (f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock) 
ELSE 0 END) *amt_gblstdprice_merck) / prt.uomiru) end )/count(*) as ct_iqvprovision_v2
from fact_inventoryaging f_invagng, 	dim_part	prt
where f_invagng.dim_partid=prt.dim_partid
group by prt.dim_partid, prt.partnumber, f_invagng.dim_storagelocationid,f_invagng.dim_plantid,f_invagng.dd_batchno,dd_status;

update fact_inventoryaging f
set f.ct_iqvprovision_v2  = t.ct_iqvprovision_v2
from fact_inventoryaging f, IQV_Provision_Amount_CCP_v2 t
where f.dim_partid = t.dim_partid
and  f.dim_storagelocationid = t.dim_storagelocationid 
and f.dim_plantid=t.dim_plantid 
and f.dd_batchno=t.dd_batchno
and f.dd_status=t.dd_status
and f.ct_iqvprovision_v2 <> t.ct_iqvprovision_v2;

drop table if exists IQV_Provision_Amount_CCP_v2;