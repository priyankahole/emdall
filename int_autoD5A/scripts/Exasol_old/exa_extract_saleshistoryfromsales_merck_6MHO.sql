
/* v1.0 : Script to extract sales history from merck AH table "merck.tmp_atlas_sales_GS" */
/* It does some pre-processing(e.g imputing 0's to have continuous time-series) and has some checks like min. no of data points */

DROP TABLE IF EXISTS tmp_stage_saleshistory_input_prd;
CREATE TABLE tmp_stage_saleshistory_input_prd
(
    dd_partnumber varchar(30),
    dd_level2_plant_comp_hei_ctrydest_mktgrp varchar(200),
    dd_companycode_dummy_ALL varchar(30),
    dd_yearmonth varchar(10),
    ct_salesqty decimal(18,2)
);

/* Get all historic data from merck.tmp_atlas_sales_GS */
/* Only retrieve data till the last date of the previous month */

/* OZ BI-5395: Add Greg's preprocess part */
drop table if exists tmp_atlas_Sales_gs;
create table tmp_atlas_Sales_gs as 
select
sales_uin,
sales_reporting_period,
sales_cocd,
sum(BuoM_quantity) as BUOM_QUantity,
Reporting_company_code,
'Not Set' hei_code,
'Not Set' Country_destination_Code,
Market_grouping,
sum (BuoM_quantityFPU) as BUOM_QUantityFPU,
sum (BUOM_QUantitybulk) as BUOM_QUantityBULK,
Region
from MERCK.ATLAS_FORECAST_SALES_MERCK_DC 
where sales_Cocd NOT IN ( 'NL10','USA0') 
Group by sales_uin, sales_reporting_period, sales_cocd, Reporting_company_code, Market_grouping,Region;


Insert into tmp_atlas_sales_gs(
sales_uin,
sales_reporting_period,
sales_cocd,
BUOM_QUantity,
Reporting_company_code,
hei_code,
Country_destination_Code,
Market_grouping,
BUOM_QUantityFPU,
BUOM_QUantityBULK,
Region)
select
sales_uin,
sales_reporting_period,
sales_cocd,
sum(BuoM_quantity) as BUOM_QUantity,
Reporting_company_code,
'Not Set' hei_code,
'Not Set' Country_destination_Code,
'USA' Market_grouping,
sum (BuoM_quantityFPU) as BUOM_QUantityFPU,
sum (BUOM_QUantitybulk) as BUOM_QUantityBULK,
'North America' as Region
from MERCK.ATLAS_FORECAST_SALES_MERCK_DC 
where sales_Cocd = 'USA0'
Group by sales_uin,
sales_reporting_period,
sales_cocd,Reporting_company_code;


Insert into tmp_atlas_sales_gs(
sales_uin,
sales_reporting_period,
sales_cocd,
BUOM_QUantity,
Reporting_company_code,
hei_code,
Country_destination_Code,
Market_grouping,
BUOM_QUantityFPU,
BUOM_QUantityBULK,
Region)
select 
sales_uin,
sales_reporting_period,
sales_cocd,
BUOM_QUantity,
Reporting_company_code,
hei_code,
Country_destination_Code,
Market_grouping,
BUOM_QUantityFPU,
BUOM_QUantityBULK,
Region
 from MERCK.ATLAS_FORECAST_SALES_MERCK_DC 
where sales_Cocd = 'NL10';

/* OZ BI-5395: End Greg preprocess part */

INSERT INTO tmp_stage_saleshistory_input_prd
(
dd_partnumber,
dd_level2_plant_comp_hei_ctrydest_mktgrp,
dd_companycode_dummy_ALL,
dd_yearmonth,
ct_salesqty
)
SELECT SALES_UIN dd_partnumber,
SALES_COCD || '|' || REPORTING_COMPANY_CODE || '|' || HEI_CODE || '|' || COUNTRY_DESTINATION_CODE || '|' || MARKET_GROUPING dd_level2_plant_comp_hei_ctrydest_mktgrp,
'ALL' dd_companycode_dummy_ALL,
SALES_REPORTING_PERIOD dd_yearmonth,
(BUOM_QUANTITY) ct_salesqty
from merck.tmp_atlas_sales_GS
WHERE SALES_REPORTING_PERIOD is not null and SALES_UIN is not null
AND SALES_REPORTING_PERIOD <= to_char(current_date-interval '1' month,'YYYYMM');

DROP TABLE IF EXISTS tmp_bckp_merckah_saleshist;
CREATE TABLE tmp_bckp_merckah_saleshist
AS
SELECT dd_partnumber dd_partnumber,
dd_level2_plant_comp_hei_ctrydest_mktgrp comop,
'ALL' companycode,
cast(dd_yearmonth as int) YYYYMM,
sum(ct_salesqty) Sales
FROM tmp_stage_saleshistory_input_prd
group by dd_partnumber , dd_level2_plant_comp_hei_ctrydest_mktgrp,cast(dd_yearmonth as int);

----------------------------------------------------------------------
/* Check the numbers in Source table */
/*select  sales_reporting_period, sum(buom_quantity) sales , count(distinct sales_uin) cnt_of_parts,
from MERCK.ATLAS_FORECAST_SALES_MERCK_DC 
group by sales_reporting_period 
order by sales_reporting_period desc*/

/* Check the numbers in input data table */
/*select YYYYMM, sum(sales) sales, count(distinct dd_partnumber) cnt_of_parts,count(distinct dd_partnumber||comop)  cnt_of_grain
from tmp_bckp_merckah_saleshist
group by YYYYMM 
order by YYYYMM desc*/
-----------------------------------------------------------------------

/* Check the latest month with non-zero sale */
DROP TABLE IF EXISTS tmp_cnt_min_saleshist;
CREATE TABLE tmp_cnt_min_saleshist
AS
SELECT dd_partnumber,comop,count(*) cnt,max(to_number(yyyymm)) max_yyyymm
from tmp_bckp_merckah_saleshist
WHERE Sales > 0
GROUP BY dd_partnumber,comop;

/* There should be atleast one non-zero sale month in last 3 months */
/* This may be commented out if needed for the ad-hoc runs as they are all in the past and some of those parts may not have sales in recent 3 months(though they will have sales in respective holdout period)   */
DELETE FROM tmp_bckp_merckah_saleshist f
WHERE EXISTS ( select 1 from tmp_cnt_min_saleshist m where m.dd_partnumber = f.dd_partnumber and m.comop = f.comop and max_yyyymm not in (to_char(current_date-interval '1' month,'YYYYMM'),to_char(current_date-interval '2' month,'YYYYMM'),to_char(current_date-interval '3' month,'YYYYMM')));

/* Remove grain(e.g part+comop) that have all zero's */
DROP TABLE IF EXISTS tmp_cnt_min_saleshist;
CREATE TABLE tmp_cnt_min_saleshist
AS
SELECT dd_partnumber,comop,max(Sales) max_sales 
from tmp_bckp_merckah_saleshist
GROUP BY dd_partnumber,comop;

DELETE FROM tmp_bckp_merckah_saleshist f
WHERE EXISTS ( select 1 from tmp_cnt_min_saleshist m where m.dd_partnumber = f.dd_partnumber and m.comop = f.comop and max_sales = 0);

/* Impute 0 sales in missing months */

/* Get the min AND max Sched Dlvry Date */
drop table if exists tmp_saleshistory_daterange;
create table tmp_saleshistory_daterange as
select max(YYYYMM) maxdate, min(YYYYMM) mindate
from tmp_bckp_merckah_saleshist fs1;

drop table if exists tmp_saleshistory_yyyymm;
create table tmp_saleshistory_yyyymm as
select distinct calendarmonthid --, datevalue
from dim_date, tmp_saleshistory_daterange
WHERE calendarmonthid between mindate AND maxdate
order by calendarmonthid;


/* Insert zero for all intermediate months */
INSERT INTO tmp_bckp_merckah_saleshist
SELECT DISTINCT dd_partnumber,comop,companycode,t.calendarmonthid YYYYMM,0
FROM tmp_saleshistory_yyyymm t,
(SELECT DISTINCT dd_partnumber,comop,companycode from tmp_bckp_merckah_saleshist) s
WHERE NOT EXISTS ( SELECT 1 FROM tmp_bckp_merckah_saleshist s2 where s2.dd_partnumber = s.dd_partnumber AND s2.comop = s.comop AND s2.companycode = s.companycode AND s2.YYYYMM = t.calendarmonthid);

/* Delete all zero sales before the first positive sale */

DROP TABLE IF EXISTS tmp_bckp_merckah_saleshist1;
CREATE TABLE tmp_bckp_merckah_saleshist1
AS
SELECT dd_partnumber,comop,min(YYYYMM) yyyymm
FROM tmp_bckp_merckah_saleshist
WHERE Sales > 0
group by dd_partnumber,comop;

/* Delete leading 0's e.g all 0's before the first month with +ve sales qty */
DELETE FROM tmp_bckp_merckah_saleshist f
WHERE EXISTS ( SELECT 1 FROM tmp_bckp_merckah_saleshist1 f2 WHERE f.dd_partnumber = f2.dd_partnumber AND f.comop = f2.comop AND f.yyyymm < f2.yyyymm);

/* Delete parts that have all zeros */
DELETE FROM tmp_bckp_merckah_saleshist f
WHERE NOT EXISTS ( SELECT 1 FROM tmp_bckp_merckah_saleshist1 f2 WHERE f.dd_partnumber = f2.dd_partnumber AND f.comop = f2.comop );


/* Check for min data points */
DROP TABLE IF EXISTS tmp_cnt_min_saleshist;
CREATE TABLE tmp_cnt_min_saleshist
AS
SELECT dd_partnumber,comop,count(*) cnt,max(to_number(yyyymm)) max_yyyymm
from tmp_bckp_merckah_saleshist
GROUP BY dd_partnumber,comop;

/* Delete part if there are less than 12 non-zero data points in the time-series(This assumes 3-month holdout. 
Change this to 18 if you need 6-month holdout)*/
DELETE FROM tmp_bckp_merckah_saleshist f
WHERE EXISTS ( select 1 from tmp_cnt_min_saleshist m where m.dd_partnumber = f.dd_partnumber and m.comop = f.comop and cnt < 18);

DROP TABLE IF EXISTS saleshistory_fromprd_dfsubjarea_6MHO;
CREATE TABLE saleshistory_fromprd_dfsubjarea_6MHO
AS
SELECT *
FROM tmp_bckp_merckah_saleshist
ORDER BY dd_partnumber,YYYYMM;

/* Export data to a csv file. This csv file can be consumed by the forecasting scripts on forecast server */
/* This should have 5 comma-separated columns. part-plant-companycode-yyyymm-salesqty 
EXPORT (SELECT * FROM merck.saleshistory_fromprd_dfsubjarea_6MHO ORDER BY DD_PARTNUMBER,COMOP,COMPANYCODE,YYYYMM)  
INTO LOCAL CSV FILE '<path>' COLUMN SEPARATOR = ',' 
*/

DROP TABLE IF EXISTS tmp_stage_saleshistory_input_prd;
DROP TABLE IF EXISTS tmp_bckp_merckah_saleshist;
DROP TABLE IF EXISTS tmp_cnt_min_saleshist;
DROP TABLE IF EXISTS tmp_cnt_min_saleshist;
drop table if exists tmp_saleshistory_daterange;
drop table if exists tmp_saleshistory_yyyymm;
DROP TABLE IF EXISTS tmp_bckp_merckah_saleshist1;
DROP TABLE IF EXISTS tmp_cnt_min_saleshist;  

