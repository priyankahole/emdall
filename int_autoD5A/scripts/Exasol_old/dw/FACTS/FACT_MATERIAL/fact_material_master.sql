CREATE OR REPLACE VIEW FACT_DW_MATERIAL
AS
SELECT row_number()over(order by '') as FACT_DW_MATERIALID,
		  t.*
FROM dim_material__xbi t;
