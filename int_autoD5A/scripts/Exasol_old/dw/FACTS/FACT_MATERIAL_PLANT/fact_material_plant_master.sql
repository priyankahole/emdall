CREATE OR REPLACE VIEW FACT_DW_MATERIAL_PLANT
AS
SELECT row_number()over(order by '') as FACT_DW_MATERIAL_PLANTID,
		  t.*
FROM dim_material_plant__xbi t;
