/* Build SCN for Material */
		create or replace view fact_scn_material 	as
		select  row_number()over(order by '') 		as fact_scn_materialid,
				MARA_MATNR_HSH 						as dim_material__xbiid,
				MARA_MATNR_RSRC						as dd_material_recordsource,
				PARTNUMBER_RSRC 					as dd_scn_material_recordsource,
				CT_ONHAND_AMOUNT_STPR_IRU,
				CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE,
				CT_OPENDELIV_AMT_NEXT7DAYS,
				CT_OPENORDER_AMOUNT,
				CT_YTDORDER_AMOUNT,
				CT_PASTDUEORDER_AMOUNT_ORD
		from HUB_MATERIAL h1
				inner join sat_material_scn s1 on h1.MARA_MATNR_HSH = s1.PARTNUMBER_HSH
		where s1.PARTNUMBER_LEDTS is null;
		
		/* will use PRODUCT_HSH to join to dim_material__xbi dimension */



