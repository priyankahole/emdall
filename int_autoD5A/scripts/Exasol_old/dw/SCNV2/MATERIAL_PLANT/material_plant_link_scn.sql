/* Staging tables preparation */

	/* ********************* START: Material-Plant ********************* */	
		
			-- 1 source Inventory
			drop table if exists stg_scn_material_plant_inventory1;
			create table stg_scn_material_plant_inventory1 as
			select 	ROUND(SUM(ct_onhandqtyiru),2) 										as ct_Stock_Quantity_IRU,
			        ROUND(SUM(ct_onhandqtyIRU * amt_stdpricepmra_merck),2) 				as ct_OnHand_Amount_StPr_IRU,
			        ROUND(SUM(ct_totalrestrictedstockiru * amt_stdpricepmra_merck),2) 	as ct_RestrictedStock_Amount_StdPrice,
			        ROUND(SUM(ct_stockqtyiru * amt_stdpricepmra_merck),2) 				as ct_UnrestrictedStock_Amount_StdPrice,
					dp.partnumber,
					dp.plant
			from fact_inventoryaging AS f_invagng
					INNER JOIN dim_storagelocation AS stloc ON f_invagng.Dim_StorageLocationid = stloc.dim_storagelocationid
					inner join DIM_PART dp on f_invagng.DIM_PARTID = dp.DIM_PARTID
			where (lower(CASE WHEN right(stloc.LocationCode, 1) = 'T' THEN 'Yes'
			                      ELSE 'No' END) = lower('No'))
			group by dp.partnumber, dp.plant;


			-- 2 source Sales1
			drop table if exists stg_scn_material_plant_sales1;
			create table stg_scn_material_plant_sales1 as
			select  prt.partnumber as partnumber, prt.plant,
					ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
			                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
			                                  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
			                              END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                   END) * f_so.amt_ExchangeRate_GBL), 2) ct_OpenOrder_Amount
			from fact_salesorder AS f_so
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
			            							AND (   soegidt.DateValue BETWEEN (trunc(current_date) - INTERVAL '1' YEAR) AND trunc(current_date)
			                 							 OR soegidt.DateValue BETWEEN trunc(current_date,'YEAR') AND trunc(current_date)
														 OR soegidt.DateValue BETWEEN current_date AND (current_date + 364))
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
			            										   AND (lower(sorr.RejectReasonCode) = lower('Not Set'))
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
															 AND ((CASE WHEN sdt.DocumentType = 'AF' THEN 'IN'
			                      										WHEN sdt.DocumentType = 'AG' THEN 'QT'
													                    WHEN sdt.DocumentType = 'AU' THEN 'SI'
													                    WHEN sdt.DocumentType = 'G2' THEN 'CR'
													                    WHEN sdt.DocumentType = 'KL' THEN 'FD'
													                    WHEN sdt.DocumentType = 'KM' THEN 'CQ'
													                    WHEN sdt.DocumentType = 'KN' THEN 'SD'
													                    WHEN sdt.DocumentType = 'L2' THEN 'DR'
													                    WHEN sdt.DocumentType = 'LP' THEN 'DS'
													                    WHEN sdt.DocumentType = 'TA' THEN 'OR'
													                    ELSE sdt.DocumentType END) NOT IN (('KA'),('KB')))
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
			            								   AND (lower(dc.DocumentCategory) = lower('C'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
			            									   AND (lower(dch.DistributionChannelCode) != lower('20'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
			            										 AND (lower(sois.OverallProcessingStatus) != lower('Completely processed'))
																 AND (lower(sois.OverallDeliveryStatus) != lower('Completely processed'))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
			where ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
						 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
								THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                               ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			                        WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			             ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END > 0))
			group by prt.partnumber, prt.plant;

			-- 3 source Sales2
			drop table if exists stg_scn_material_plant_sales2;
			create table stg_scn_material_plant_sales2 as
			select	prt.partnumber, prt.plant,
					ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                        WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
			                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
											  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) 
										  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                   END) * f_so.amt_ExchangeRate_GBL),0) ct_PastDueOrder_Amount_ORD
			from fact_salesorder AS f_so
					INNER JOIN Dim_Date AS sdrdp ON f_so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid
												  AND ((sdrdp.DateValue) BETWEEN    (add_months(trunc(current_date - INTERVAL '9' MONTH, 'MM'), 1)-1 + INTERVAL '1' DAY) 
												  								AND (add_months(trunc(current_date, 'MM'), 1)-1))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ItemSubType_Merck) = lower('FPP'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
															   AND (lower(dch.DistributionChannelName) != lower('Intercompany sales'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
					INNER JOIN dim_overallstatusforcreditcheck AS oscc ON f_so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckid
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
					INNER JOIN dim_salesorderitemcategory AS ic ON f_so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
			WHERE (lower((CASE WHEN dc.DocumentCategory ='C'
			                          AND sdt.DocumentType NOT IN ('KA','KB')
			                          AND oscc.OverallStatusforCreditCheck <> 'B'
			                          AND sois.OverallProcessingStatus <> 'Completely processed'
			                          AND sois.OverallDeliveryStatus <>'Completely processed'
			                          AND sorr.RejectReasonCode = 'Not Set'
			                          AND soegidt.DateValue < CURRENT_DATE THEN 'X'
			                       ELSE 'Not Set' END)) = lower('X'))
			      AND ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                 WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
							 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') 
								 THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			                 ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                            ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END) END >= 0.1))
			group by prt.partnumber, prt.plant;

	/* ********************* END: Material-Plant ********************* */

-- >>

	/* ********************* START: Unifying measures for Material-Plant ********************* */

			drop table if exists stg_scn_material_plant_unique;
			create table stg_scn_material_plant_unique as
			select PARTNUMBER, PLANT from stg_scn_material_plant_inventory1
			union
			select PARTNUMBER, PLANT from stg_scn_material_plant_sales1
			union
			select PARTNUMBER, PLANT from stg_scn_material_plant_sales2;
			
			drop table if exists stg_scn_material_plant;
			create table stg_scn_material_plant as
			select  t0.PARTNUMBER, t0.PLANT,
					cast(ifnull(t1.CT_ONHAND_AMOUNT_STPR_IRU, 0.00) 			as decimal(18,5)) as CT_ONHAND_AMOUNT_STPR_IRU,
					cast(ifnull(t1.CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE, 0.00) 	as decimal(18,5)) as CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE,
					cast(ifnull(t1.CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE, 0.00) 	as decimal(18,5)) as CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE,
					cast(ifnull(t2.CT_OPENORDER_AMOUNT, 0.00) 					as decimal(18,5)) as CT_OPENORDER_AMOUNT,
					cast(ifnull(t3.CT_PASTDUEORDER_AMOUNT_ORD, 0.00) 			as decimal(18,5)) as CT_PASTDUEORDER_AMOUNT_ORD
			from stg_scn_material_plant_unique t0
					left join stg_scn_material_plant_inventory1 t1 on t0.PARTNUMBER = t1.PARTNUMBER and t0.PLANT = t1.PLANT
					left join stg_scn_material_plant_sales1     t2 on t0.PARTNUMBER = t2.PARTNUMBER and t0.PLANT = t2.PLANT
					left join stg_scn_material_plant_sales2     t3 on t0.PARTNUMBER = t3.PARTNUMBER and t0.PLANT = t3.PLANT;

			drop table if exists stg_scn_material_plant_hash;
			create table stg_scn_material_plant_hash as
			select 	cast(UPPER(HASH_MD5(PARTNUMBER || '|^|' || PLANT)) as varchar(32))  as PARTNUMBER_PLANT_HSH,
					cast(UPPER(HASH_MD5(PARTNUMBER)) as varchar(32)) 					as PARTNUMBER_HSH,
					cast(UPPER(HASH_MD5(PLANT))      as varchar(32)) 					as PLANT_HSH,
					current_timestamp 				 				 					as PARTNUMBER_PLANT_LDTS,
					cast('AERA BV: SCN - Material-Plant Multiple SA' as varchar(50))  	as PARTNUMBER_PLANT_RSRC,
					cast(UPPER(HASH_MD5(upper(PARTNUMBER 										|| '|^|' ||
											  PLANT												|| '|^|' ||
											  IFNULL(CT_ONHAND_AMOUNT_STPR_IRU, 0.0) 			|| '|^|' ||
											  IFNULL(CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE, 0.0) 	|| '|^|' ||
											  IFNULL(CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE, 0.0) || '|^|' ||
											  IFNULL(CT_OPENORDER_AMOUNT, 0.0) 					|| '|^|' ||
											  IFNULL(CT_PASTDUEORDER_AMOUNT_ORD, 0.0)
							)))as varchar(32)) 						 					as PARTNUMBER_PLANT_SCN_HSH_DIFF,
					PARTNUMBER,
					PLANT,
					CT_ONHAND_AMOUNT_STPR_IRU,
					CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE,
					CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE,
					CT_OPENORDER_AMOUNT,
					CT_PASTDUEORDER_AMOUNT_ORD
			from stg_scn_material_plant;

	/* ********************* END: Unifying measures for Material-Plant ********************* */

-- >>

/* ********************* START: Material-Plant SCN measures populating ********************* */


					/* START: As we have cases where a Material-Plant combination does not come from MARC (master) table, we need to insert missing relationships */
						insert into link_material_plant (MARC_MATNR_WERKS_HSH, MARC_MATNR_WERKS_LDTS, MARC_MATNR_WERKS_RSRC, MARC_MATNR_HSH, MARC_WERKS_HSH)
						select distinct st1.PARTNUMBER_PLANT_HSH,
						       st1.PARTNUMBER_PLANT_LDTS,
						       st1.PARTNUMBER_PLANT_RSRC,
						       st1.PARTNUMBER_HSH,
							   st1.PLANT_HSH
						from stg_scn_material_plant_hash as st1
						where not exists (
					                      select 1
					                      from link_material_plant l1
										  where     st1.PARTNUMBER_HSH = l1.MARC_MATNR_HSH
												and st1.PLANT_HSH = l1.MARC_WERKS_HSH
					                      );
					/* END: Material-Plant link enrich */

	/* SAT Material-Plant SCN table population */

	/* 	drop table if exists sat_material_plant_scn 
		create table sat_material_plant_scn (
												PARTNUMBER_PLANT_HSH 			varchar(32),
												PARTNUMBER_PLANT_LDTS 			timestamp,
												PARTNUMBER_PLANT_LEDTS 			timestamp,
												PARTNUMBER_PLANT_RSRC 			varchar(50),
												PARTNUMBER_PLANT_SCN_HSH_DIFF 	varchar(32),
												CT_ONHAND_AMOUNT_STPR_IRU 			 decimal(18,5),
												CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE 	 decimal(18,5),
												CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE decimal(18,5),
												CT_OPENORDER_AMOUNT 				 decimal(18,5),
												CT_PASTDUEORDER_AMOUNT_ORD 			 decimal(18,5)
											) */

	/* insert into sat dummy record in order to avoid left joins in future */
	insert into sat_material_plant_scn (PARTNUMBER_PLANT_HSH, PARTNUMBER_PLANT_LDTS, PARTNUMBER_PLANT_LEDTS, PARTNUMBER_PLANT_RSRC, PARTNUMBER_PLANT_SCN_HSH_DIFF, 
										CT_ONHAND_AMOUNT_STPR_IRU, CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE, CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE, CT_OPENORDER_AMOUNT, CT_PASTDUEORDER_AMOUNT_ORD)
	select  '00000000000000000000000000000000' as PARTNUMBER_PLANT_HSH,
			'0001-01-01' as PARTNUMBER_PLANT_LDTS,
			'9999-12-31' as PARTNUMBER_PLANT_LEDTS,
			'SYSTEM'	 as PARTNUMBER_PLANT_RSRC,
			'00000000000000000000000000000000' as PARTNUMBER_PLANT_SCN_HSH_DIFF,
			0.00 as CT_ONHAND_AMOUNT_STPR_IRU, 
			0.00 as CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE, 
			0.00 as CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE, 
			0.00 as CT_OPENORDER_AMOUNT, 
			0.00 as CT_PASTDUEORDER_AMOUNT_ORD
	from dual
	where not exists (select 1 from sat_material_plant_scn where PARTNUMBER_PLANT_HSH = '00000000000000000000000000000000');

	/* insert new records and changed ones */
	insert into sat_material_plant_scn (PARTNUMBER_PLANT_HSH, PARTNUMBER_PLANT_LDTS, PARTNUMBER_PLANT_RSRC, PARTNUMBER_PLANT_SCN_HSH_DIFF, 
										CT_ONHAND_AMOUNT_STPR_IRU, CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE, CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE, CT_OPENORDER_AMOUNT, CT_PASTDUEORDER_AMOUNT_ORD)
	select 	st1.PARTNUMBER_PLANT_HSH, st1.PARTNUMBER_PLANT_LDTS, st1.PARTNUMBER_PLANT_RSRC, st1.PARTNUMBER_PLANT_SCN_HSH_DIFF, 
			st1.CT_ONHAND_AMOUNT_STPR_IRU, st1.CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE, st1.CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE, st1.CT_OPENORDER_AMOUNT, st1.CT_PASTDUEORDER_AMOUNT_ORD
	from stg_scn_material_plant_hash as st1
	      left outer join sat_material_plant_scn as s1
	                   on     st1.PARTNUMBER_PLANT_HSH = s1.PARTNUMBER_PLANT_HSH
	                      and s1.PARTNUMBER_PLANT_LEDTS is NULL
	where (
	        s1.PARTNUMBER_PLANT_HSH is null OR
	        (
	              st1.PARTNUMBER_PLANT_SCN_HSH_DIFF != s1.PARTNUMBER_PLANT_SCN_HSH_DIFF
	          AND s1.PARTNUMBER_PLANT_LDTS = (
                                        select max(z.PARTNUMBER_PLANT_LDTS)
                                        from sat_material_plant_scn z
                                        where s1.PARTNUMBER_PLANT_HSH = z.PARTNUMBER_PLANT_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s104
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s104;
	insert into upd_sat_end_date_loop_s104 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.PARTNUMBER_PLANT_HSH,
	       s1.PARTNUMBER_PLANT_LDTS
	from sat_material_plant_scn as s1
	where (
	            s1.PARTNUMBER_PLANT_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.PARTNUMBER_PLANT_HSH)
	                     from sat_material_plant_scn as y
	                     where   y.PARTNUMBER_PLANT_HSH = s1.PARTNUMBER_PLANT_HSH
	                         AND y.PARTNUMBER_PLANT_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s104
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s104;
	insert into upd_sat_end_date_cross_s104 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s104 t1
					inner join upd_sat_end_date_loop_s104 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_material_plant_scn dst
	   set dst.PARTNUMBER_PLANT_LEDTS = src.BUSKEY_LEDTS
	  from sat_material_plant_scn dst
				inner join upd_sat_end_date_cross_s104 src on dst.PARTNUMBER_PLANT_HSH = src.BUSKEY_HSH
	      												and dst.PARTNUMBER_PLANT_LDTS = src.BUSKEY_LDTS;	