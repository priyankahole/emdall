/* Build SCN for Material-Plant */
		create or replace view fact_scn_material_plant as
		select 	MARC_MATNR_WERKS_HSH 	as fact_scn_material_plantid,
				MARC_MATNR_WERKS_RSRC 	as dd_material_plant_recordsource,
				MARC_MATNR_HSH 			as dim_material__xbiid,
				MARC_WERKS_HSH 			as dim_plant__xbiid,
				PARTNUMBER_PLANT_RSRC 	as dd_scn_material_plant_recordsource,
				CT_ONHAND_AMOUNT_STPR_IRU, 
				CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE,
				CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE,
				CT_OPENORDER_AMOUNT,
				CT_PASTDUEORDER_AMOUNT_ORD
		from link_material_plant t
				inner join sat_material_plant_scn x on t.MARC_MATNR_WERKS_HSH = x.PARTNUMBER_PLANT_HSH
		where x.PARTNUMBER_PLANT_LEDTS is null;

		/* will use PRODUCT_HSH to join to dim_material__xbi dimension 
			and  PLANT_HSH to join to dim_plant__xbi dimension */

