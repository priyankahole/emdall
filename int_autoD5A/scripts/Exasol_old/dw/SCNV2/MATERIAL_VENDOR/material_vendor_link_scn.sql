/* Staging tables preparation */

	/* ********************* START: Material-Vendor ********************* */

			-- 1 source Purchase
			drop table if exists stg_scn_material_vendor_purchase1;
			create table stg_scn_material_vendor_purchase1 as
			select 	ROUND(SUM((CASE WHEN atrb.itemdeliverycomplete = 'X' THEN 0.0000
			                       WHEN atrb.ItemGRIndicator = 'Not Set' THEN 0.0000
			                       WHEN (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 THEN 0.0000
			                       ELSE f_deliv.amt_UnitPrice*(f_deliv.ct_DeliveryQty-f_deliv.ct_ReceivedQty)
			                  END) * f_deliv.amt_ExchangeRate_GBL), 2) 											as ct_OpenDeliv_Amt_Next7Days,
			       	ROUND(SUM(CASE WHEN atrb.ItemGRIndicator = 'X' AND 
									   atrb.itemdeliverycomplete <> 'X' AND
									   (f_Deliv.ct_DeliveryQty - f_Deliv.ct_ReceivedQty) > 0 THEN 1.0000
			                      ELSE 0.0000
			                 END), 0) 																			as ct_OpenDelivSchedule_Count,
					prt.PartNumber,
					dv.VENDORNUMBER
			from fact_purchase AS f_deliv
					INNER JOIN Dim_Date AS dtdel ON     f_deliv.Dim_DateidDelivery = dtdel.Dim_Dateid
			      								   AND (dtdel.DateValue BETWEEN current_date AND current_date + 6)
					INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid
												 AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))
					INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid
					INNER JOIN dim_vendor as dv on f_deliv.dim_vendorid = dv.dim_vendorid
			where VENDORNUMBER <> 'Not Set'
			group by prt.PartNumber, dv.VENDORNUMBER;


			-- 2 source Purchase
			drop table if exists stg_scn_material_vendor_purchase2;
			create table stg_scn_material_vendor_purchase2 as
			select 	ROUND(SUM((CASE WHEN atrb.itemdeliverycomplete = 'X' THEN 0.0000
			                        WHEN atrb.ItemGRIndicator = 'Not Set' THEN 0.0000
			                        WHEN (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 THEN 0.0000
			                        ELSE f_deliv.amt_UnitPrice*(f_deliv.ct_DeliveryQty-f_deliv.ct_ReceivedQty) END) * f_deliv.amt_ExchangeRate_GBL),2) ct_OpenDeliv_Amt,
					
					prt.PartNumber,
					dv.VENDORNUMBER
			from fact_purchase AS f_deliv
					INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))
					INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid
					INNER JOIN Dim_Date AS dtdel ON f_deliv.Dim_DateidDelivery = dtdel.Dim_Dateid
					INNER JOIN dim_vendor as dv on f_deliv.dim_vendorid = dv.dim_vendorid
			where ((CASE WHEN atrb.itemdeliverycomplete = 'X' THEN 0.0000
						 WHEN atrb.ItemGRIndicator = 'Not Set' THEN 0.0000
						 WHEN (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 THEN 0.0000
						 ELSE (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) END >= 1 ))
				AND (((CASE WHEN f_deliv.Dim_DateidDelivery > 1 THEN (dtdel.DateValue - CURRENT_DATE) ELSE 0.0000 END) <= -1))
				AND VENDORNUMBER <> 'Not Set'
			group by prt.PartNumber, dv.VENDORNUMBER;


	/* ********************* END: Material-Vendor ********************* */

-- >>

	/* ********************* START: Unifying measures for Material-Vendor ********************* */
			
			drop table if exists stg_scn_material_vendor_unique;
			create table stg_scn_material_vendor_unique as
			select PARTNUMBER, VENDORNUMBER from stg_scn_material_vendor_purchase1
			union
			select PARTNUMBER, VENDORNUMBER from stg_scn_material_vendor_purchase2;

			drop table if exists stg_scn_material_vendor;
			create table stg_scn_material_vendor as
			select  t0.PARTNUMBER, t0.VENDORNUMBER,
					cast(ifnull(t1.CT_OPENDELIV_AMT_NEXT7DAYS, 0.00) as decimal(18,5)) as CT_OPENDELIV_AMT_NEXT7DAYS,
					cast(ifnull(t1.CT_OPENDELIVSCHEDULE_COUNT, 0.00) as decimal(18,5)) as CT_OPENDELIVSCHEDULE_COUNT,
					cast(ifnull(t2.CT_OPENDELIV_AMT, 0.00) 			 as decimal(18,5)) as CT_OPENDELIV_AMT
			from stg_scn_material_vendor_unique t0
					left join stg_scn_material_vendor_purchase1 t1 on t0.PARTNUMBER = t1.PARTNUMBER and t0.VENDORNUMBER = t1.VENDORNUMBER
					left join stg_scn_material_vendor_purchase2 t2 on t0.PARTNUMBER = t2.PARTNUMBER and t0.VENDORNUMBER = t2.VENDORNUMBER;

			drop table if exists stg_scn_material_vendor_hash;
			create table stg_scn_material_vendor_hash as
			select 	cast(UPPER(HASH_MD5(PARTNUMBER || '|^|' || VENDORNUMBER)) as varchar(32)) 	as PARTNUMBER_VENDOR_HSH,
					cast(UPPER(HASH_MD5(PARTNUMBER)) as varchar(32)) 							as PARTNUMBER_HSH,
					cast(UPPER(HASH_MD5(VENDORNUMBER)) as varchar(32)) 							as VENDOR_HSH,
					current_timestamp 				 				 							as PARTNUMBER_VENDOR_LDTS,
					cast('AERA BV: Material-Vendor Purchase' as varchar(50))  	 				as PARTNUMBER_VENDOR_RSRC,
					cast(UPPER(HASH_MD5(upper(PARTNUMBER 								|| '|^|' ||
											  VENDORNUMBER								|| '|^|' ||
											  IFNULL(CT_OPENDELIV_AMT_NEXT7DAYS, 0.0) 	|| '|^|' ||
											  IFNULL(CT_OPENDELIVSCHEDULE_COUNT, 0.0) 	|| '|^|' ||
											  IFNULL(CT_OPENDELIV_AMT, 0.0) 
							)))as varchar(32)) 						 							as PARTNUMBER_VENDOR_SCN_HSH_DIFF,
					PARTNUMBER,
					VENDORNUMBER,
					CT_OPENDELIV_AMT_NEXT7DAYS,
					CT_OPENDELIVSCHEDULE_COUNT,
					CT_OPENDELIV_AMT
			from stg_scn_material_vendor;

	/* ********************* END: Unifying measures for Material-Vendor ********************* */

-- >>

/* ********************* START: Material-Vendor SCN measures populating ********************* */

	/* LINK Material-Vendor table populating */

	/* drop table if exists link_material_vendor
	   create table link_material_vendor ( PARTNUMBER_VENDOR_HSH varchar(32),
								   			 PARTNUMBER_VENDOR_LDTS timestamp,
								   			 PARTNUMBER_VENDOR_RSRC varchar(50),
								   			 PARTNUMBER_HSH varchar(32),
											 VENDOR_HSH 	varchar(32)
								  ) */

	insert into link_material_vendor (PARTNUMBER_VENDOR_HSH, PARTNUMBER_VENDOR_LDTS, PARTNUMBER_VENDOR_RSRC, PARTNUMBER_HSH, VENDOR_HSH)
	select distinct st1.PARTNUMBER_VENDOR_HSH,
	       st1.PARTNUMBER_VENDOR_LDTS,
	       st1.PARTNUMBER_VENDOR_RSRC,
	       st1.PARTNUMBER_HSH,
		   st1.VENDOR_HSH
	from stg_scn_material_vendor_hash as st1
	where not exists (
                      select 1
                      from link_material_vendor l1
					  where     st1.PARTNUMBER_HSH = l1.PARTNUMBER_HSH
							and st1.VENDOR_HSH = l1.VENDOR_HSH
                      );


	/* SAT Material-Vendor SCN table population */

	/* 	drop table if exists sat_material_vendor_scn
		create table sat_material_vendor_scn (
												PARTNUMBER_VENDOR_HSH 		varchar(32),
												PARTNUMBER_VENDOR_LDTS 		timestamp,
												PARTNUMBER_VENDOR_LEDTS 	timestamp,
												PARTNUMBER_VENDOR_RSRC 		varchar(50),
												PARTNUMBER_VENDOR_SCN_HSH_DIFF varchar(32),
												CT_OPENDELIV_AMT_NEXT7DAYS decimal(18,5),
												CT_OPENDELIVSCHEDULE_COUNT decimal(18,5),
												CT_OPENDELIV_AMT decimal(18,5)
												) */

	/* insert into sat dummy record in order to avoid left joins in future */ 
	insert into sat_material_vendor_scn (PARTNUMBER_VENDOR_HSH, PARTNUMBER_VENDOR_LDTS, PARTNUMBER_VENDOR_LEDTS, PARTNUMBER_VENDOR_RSRC, PARTNUMBER_VENDOR_SCN_HSH_DIFF, 
										 CT_OPENDELIV_AMT_NEXT7DAYS, CT_OPENDELIVSCHEDULE_COUNT, CT_OPENDELIV_AMT)
	select  '00000000000000000000000000000000' as PARTNUMBER_VENDOR_HSH,
			'0001-01-01' as PARTNUMBER_VENDOR_LDTS,
			'9999-12-31' as PARTNUMBER_VENDOR_LEDTS,
			'SYSTEM'	 as PARTNUMBER_VENDOR_RSRC,
			'00000000000000000000000000000000' as PARTNUMBER_VENDOR_SCN_HSH_DIFF,
			0.00 as CT_OPENDELIV_AMT_NEXT7DAYS, 
			0.00 as CT_OPENDELIVSCHEDULE_COUNT, 
			0.00 as CT_OPENDELIV_AMT
	from dual
	where not exists (select 1 from sat_material_vendor_scn where PARTNUMBER_VENDOR_HSH = '00000000000000000000000000000000');

	/* insert new records and changed ones */
	insert into sat_material_vendor_scn (PARTNUMBER_VENDOR_HSH, PARTNUMBER_VENDOR_LDTS, PARTNUMBER_VENDOR_RSRC, PARTNUMBER_VENDOR_SCN_HSH_DIFF, 
										 CT_OPENDELIV_AMT_NEXT7DAYS, CT_OPENDELIVSCHEDULE_COUNT, CT_OPENDELIV_AMT)
	select 	st1.PARTNUMBER_VENDOR_HSH, st1.PARTNUMBER_VENDOR_LDTS, st1.PARTNUMBER_VENDOR_RSRC, st1.PARTNUMBER_VENDOR_SCN_HSH_DIFF, 
			st1.CT_OPENDELIV_AMT_NEXT7DAYS, st1.CT_OPENDELIVSCHEDULE_COUNT, st1.CT_OPENDELIV_AMT
	from stg_scn_material_vendor_hash as st1
	      left outer join sat_material_vendor_scn as s1
	                   on     st1.PARTNUMBER_VENDOR_HSH = s1.PARTNUMBER_VENDOR_HSH
	                      and s1.PARTNUMBER_VENDOR_LEDTS is NULL
	where (
	        s1.PARTNUMBER_VENDOR_HSH is null OR
	        (
	              st1.PARTNUMBER_VENDOR_SCN_HSH_DIFF != s1.PARTNUMBER_VENDOR_SCN_HSH_DIFF
	          AND s1.PARTNUMBER_VENDOR_LDTS = (
                                        select max(z.PARTNUMBER_VENDOR_LDTS)
                                        from sat_material_vendor_scn z
                                        where s1.PARTNUMBER_VENDOR_HSH = z.PARTNUMBER_VENDOR_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s103
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s103;
	insert into upd_sat_end_date_loop_s103 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.PARTNUMBER_VENDOR_HSH,
	       s1.PARTNUMBER_VENDOR_LDTS
	from sat_material_vendor_scn as s1
	where (
	            s1.PARTNUMBER_VENDOR_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.PARTNUMBER_VENDOR_HSH)
	                     from sat_material_vendor_scn as y
	                     where   y.PARTNUMBER_VENDOR_HSH = s1.PARTNUMBER_VENDOR_HSH
	                         AND y.PARTNUMBER_VENDOR_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s103
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s103;
	insert into upd_sat_end_date_cross_s103 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s103 t1
					inner join upd_sat_end_date_loop_s103 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_material_vendor_scn dst
	   set dst.PARTNUMBER_VENDOR_LEDTS = src.BUSKEY_LEDTS
	  from sat_material_vendor_scn dst
				inner join upd_sat_end_date_cross_s103 src on dst.PARTNUMBER_VENDOR_HSH = src.BUSKEY_HSH
	      												and dst.PARTNUMBER_VENDOR_LDTS = src.BUSKEY_LDTS;	