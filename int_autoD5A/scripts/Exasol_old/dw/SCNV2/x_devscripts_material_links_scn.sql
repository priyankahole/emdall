/* Staging tables preparation */

	/* ********************* START: Material-Plant ********************* */	
		
			-- 1 source Inventory
			drop table if exists stg_scn_material_plant_inventory1;
			create table stg_scn_material_plant_inventory1 as
			select 	ROUND(SUM(ct_onhandqtyiru),2) 										as ct_Stock_Quantity_IRU,
			        ROUND(SUM(ct_onhandqtyIRU * amt_stdpricepmra_merck),2) 				as ct_OnHand_Amount_StPr_IRU,
			        ROUND(SUM(ct_totalrestrictedstockiru * amt_stdpricepmra_merck),2) 	as ct_RestrictedStock_Amount_StdPrice,
			        ROUND(SUM(ct_stockqtyiru * amt_stdpricepmra_merck),2) 				as ct_UnrestrictedStock_Amount_StdPrice,
					dp.partnumber,
					dp.plant
			from fact_inventoryaging AS f_invagng
					INNER JOIN dim_storagelocation AS stloc ON f_invagng.Dim_StorageLocationid = stloc.dim_storagelocationid
					inner join DIM_PART dp on f_invagng.DIM_PARTID = dp.DIM_PARTID
			where (lower(CASE WHEN right(stloc.LocationCode, 1) = 'T' THEN 'Yes'
			                      ELSE 'No' END) = lower('No'))
			group by dp.partnumber, dp.plant;


			-- 2 source Sales1
			drop table if exists stg_scn_material_plant_sales1;
			create table stg_scn_material_plant_sales1 as
			select  prt.partnumber as partnumber, prt.plant,
					ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
			                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
			                                  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
			                              END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                   END) * f_so.amt_ExchangeRate_GBL), 2) ct_OpenOrder_Amount
			from fact_salesorder AS f_so
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
			            							AND (   soegidt.DateValue BETWEEN (trunc(current_date) - INTERVAL '1' YEAR) AND trunc(current_date)
			                 							 OR soegidt.DateValue BETWEEN trunc(current_date,'YEAR') AND trunc(current_date)
														 OR soegidt.DateValue BETWEEN current_date AND (current_date + 364))
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
			            										   AND (lower(sorr.RejectReasonCode) = lower('Not Set'))
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
															 AND ((CASE WHEN sdt.DocumentType = 'AF' THEN 'IN'
			                      										WHEN sdt.DocumentType = 'AG' THEN 'QT'
													                    WHEN sdt.DocumentType = 'AU' THEN 'SI'
													                    WHEN sdt.DocumentType = 'G2' THEN 'CR'
													                    WHEN sdt.DocumentType = 'KL' THEN 'FD'
													                    WHEN sdt.DocumentType = 'KM' THEN 'CQ'
													                    WHEN sdt.DocumentType = 'KN' THEN 'SD'
													                    WHEN sdt.DocumentType = 'L2' THEN 'DR'
													                    WHEN sdt.DocumentType = 'LP' THEN 'DS'
													                    WHEN sdt.DocumentType = 'TA' THEN 'OR'
													                    ELSE sdt.DocumentType END) NOT IN (('KA'),('KB')))
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
			            								   AND (lower(dc.DocumentCategory) = lower('C'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
			            									   AND (lower(dch.DistributionChannelCode) != lower('20'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
			            										 AND (lower(sois.OverallProcessingStatus) != lower('Completely processed'))
																 AND (lower(sois.OverallDeliveryStatus) != lower('Completely processed'))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
			where ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
						 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
								THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                               ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			                        WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			             ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END > 0))
			group by prt.partnumber, prt.plant;

			-- 3 source Sales2
			drop table if exists stg_scn_material_plant_sales2;
			create table stg_scn_material_plant_sales2 as
			select	prt.partnumber, prt.plant,
					ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                        WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
			                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
											  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) 
										  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                   END) * f_so.amt_ExchangeRate_GBL),0) ct_PastDueOrder_Amount_ORD
			from fact_salesorder AS f_so
					INNER JOIN Dim_Date AS sdrdp ON f_so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid
												  AND ((sdrdp.DateValue) BETWEEN    (add_months(trunc(current_date - INTERVAL '9' MONTH, 'MM'), 1)-1 + INTERVAL '1' DAY) 
												  								AND (add_months(trunc(current_date, 'MM'), 1)-1))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ItemSubType_Merck) = lower('FPP'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
															   AND (lower(dch.DistributionChannelName) != lower('Intercompany sales'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
					INNER JOIN dim_overallstatusforcreditcheck AS oscc ON f_so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckid
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
					INNER JOIN dim_salesorderitemcategory AS ic ON f_so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
			WHERE (lower((CASE WHEN dc.DocumentCategory ='C'
			                          AND sdt.DocumentType NOT IN ('KA','KB')
			                          AND oscc.OverallStatusforCreditCheck <> 'B'
			                          AND sois.OverallProcessingStatus <> 'Completely processed'
			                          AND sois.OverallDeliveryStatus <>'Completely processed'
			                          AND sorr.RejectReasonCode = 'Not Set'
			                          AND soegidt.DateValue < CURRENT_DATE THEN 'X'
			                       ELSE 'Not Set' END)) = lower('X'))
			      AND ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                 WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
							 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') 
								 THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			                 ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                            ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END) END >= 0.1))
			group by prt.partnumber, prt.plant;

	/* ********************* END: Material-Plant ********************* */

-- >>

	/* ********************* START: Material-Vendor ********************* */

			-- 1 source Purchase
			drop table if exists stg_scn_material_vendor_purchase1;
			create table stg_scn_material_vendor_purchase1 as
			select 	ROUND(SUM((CASE WHEN atrb.itemdeliverycomplete = 'X' THEN 0.0000
			                       WHEN atrb.ItemGRIndicator = 'Not Set' THEN 0.0000
			                       WHEN (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 THEN 0.0000
			                       ELSE f_deliv.amt_UnitPrice*(f_deliv.ct_DeliveryQty-f_deliv.ct_ReceivedQty)
			                  END) * f_deliv.amt_ExchangeRate_GBL), 2) 											as ct_OpenDeliv_Amt_Next7Days,
			       	ROUND(SUM(CASE WHEN atrb.ItemGRIndicator = 'X' AND 
									   atrb.itemdeliverycomplete <> 'X' AND
									   (f_Deliv.ct_DeliveryQty - f_Deliv.ct_ReceivedQty) > 0 THEN 1.0000
			                      ELSE 0.0000
			                 END), 0) 																			as ct_OpenDelivSchedule_Count,
					prt.PartNumber,
					dv.VENDORNUMBER
			from fact_purchase AS f_deliv
					INNER JOIN Dim_Date AS dtdel ON     f_deliv.Dim_DateidDelivery = dtdel.Dim_Dateid
			      								   AND (dtdel.DateValue BETWEEN current_date AND current_date + 6)
					INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid
												 AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))
					INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid
					INNER JOIN dim_vendor as dv on f_deliv.dim_vendorid = dv.dim_vendorid
			group by prt.PartNumber, dv.VENDORNUMBER;


			-- 2 source Purchase
			drop table if exists stg_scn_material_vendor_purchase2;
			create table stg_scn_material_vendor_purchase2 as
			select 	ROUND(SUM((CASE WHEN atrb.itemdeliverycomplete = 'X' THEN 0.0000
			                        WHEN atrb.ItemGRIndicator = 'Not Set' THEN 0.0000
			                        WHEN (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 THEN 0.0000
			                        ELSE f_deliv.amt_UnitPrice*(f_deliv.ct_DeliveryQty-f_deliv.ct_ReceivedQty) END) * f_deliv.amt_ExchangeRate_GBL),2) ct_OpenDeliv_Amt,
					
					prt.PartNumber,
					dv.VENDORNUMBER
			from fact_purchase AS f_deliv
					INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))
					INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid
					INNER JOIN Dim_Date AS dtdel ON f_deliv.Dim_DateidDelivery = dtdel.Dim_Dateid
					INNER JOIN dim_vendor as dv on f_deliv.dim_vendorid = dv.dim_vendorid
			where ((CASE WHEN atrb.itemdeliverycomplete = 'X' THEN 0.0000
						 WHEN atrb.ItemGRIndicator = 'Not Set' THEN 0.0000
						 WHEN (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 THEN 0.0000
						 ELSE (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) END >= 1 ))
				AND (((CASE WHEN f_deliv.Dim_DateidDelivery > 1 THEN (dtdel.DateValue - CURRENT_DATE) ELSE 0.0000 END) <= -1))
			group by prt.PartNumber, dv.VENDORNUMBER;


	/* ********************* END: Material-Vendor ********************* */

-- >>

	/* ********************* START: Material-Customer ********************* */

			-- 1 source Sales1
			drop table if exists stg_scn_material_customer_sales1;
			create table stg_scn_material_customer_sales1 as
			select  prt.partnumber as partnumber, dcus.CUSTOMERNUMBER,
					ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
			                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
			                                  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
			                              END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                   END) * f_so.amt_ExchangeRate_GBL), 2) ct_OpenOrder_Amount
			from fact_salesorder AS f_so
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
			            							AND (   soegidt.DateValue BETWEEN (trunc(current_date) - INTERVAL '1' YEAR) AND trunc(current_date)
			                 							 OR soegidt.DateValue BETWEEN trunc(current_date,'YEAR') AND trunc(current_date)
														 OR soegidt.DateValue BETWEEN current_date AND (current_date + 364))
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
			            										   AND (lower(sorr.RejectReasonCode) = lower('Not Set'))
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
															 AND ((CASE WHEN sdt.DocumentType = 'AF' THEN 'IN'
			                      										WHEN sdt.DocumentType = 'AG' THEN 'QT'
													                    WHEN sdt.DocumentType = 'AU' THEN 'SI'
													                    WHEN sdt.DocumentType = 'G2' THEN 'CR'
													                    WHEN sdt.DocumentType = 'KL' THEN 'FD'
													                    WHEN sdt.DocumentType = 'KM' THEN 'CQ'
													                    WHEN sdt.DocumentType = 'KN' THEN 'SD'
													                    WHEN sdt.DocumentType = 'L2' THEN 'DR'
													                    WHEN sdt.DocumentType = 'LP' THEN 'DS'
													                    WHEN sdt.DocumentType = 'TA' THEN 'OR'
													                    ELSE sdt.DocumentType END) NOT IN (('KA'),('KB')))
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
			            								   AND (lower(dc.DocumentCategory) = lower('C'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
			            									   AND (lower(dch.DistributionChannelCode) != lower('20'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
			            										 AND (lower(sois.OverallProcessingStatus) != lower('Completely processed'))
																 AND (lower(sois.OverallDeliveryStatus) != lower('Completely processed'))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
					INNER JOIN DIM_CUSTOMER as dcus on f_so.DIM_CUSTOMERID = dcus.DIM_CUSTOMERID
			where ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
						 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
								THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                               ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			                        WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			             ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END > 0))
			group by prt.partnumber, dcus.CUSTOMERNUMBER;

			-- 2 source Sales2
			drop table if exists stg_scn_material_customer_sales2;
			create table stg_scn_material_customer_sales2 as
			SELECT 	prt.partnumber, dcus.CUSTOMERNUMBER,
					ROUND(SUM(((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal)
			                         ELSE f_so.amt_ScheduleTotal END)) * f_so.amt_ExchangeRate_GBL),2) 	as ct_YTDOrder_Amount,
					ROUND(SUM(ct_scheduleqtybaseuomiru),2) 												as ct_Order_Quantity_IRUUnits,
					ROUND(SUM((f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)) * f_so.amt_ExchangeRate_GBL),2) as ct_Shipped_Amount
			FROM fact_salesorder AS f_so
					INNER JOIN Dim_Date AS ocdt ON f_so.Dim_DateIdSOCreated = ocdt.Dim_Dateid
												 AND (ocdt.DateValue BETWEEN trunc(current_date,'YEAR') AND trunc(current_date))
					INNER JOIN dim_salesmisc AS dsi ON f_so.Dim_SalesMiscId = dsi.dim_salesmiscid
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
					INNER JOIN DIM_CUSTOMER as dcus on f_so.DIM_CUSTOMERID = dcus.DIM_CUSTOMERID
			group by prt.partnumber, dcus.CUSTOMERNUMBER;

			-- 3 source Sales3
			drop table if exists stg_scn_material_customer_sales3;
			create table stg_scn_material_customer_sales3 as
			select	prt.partnumber, dcus.CUSTOMERNUMBER,
					ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                        WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
			                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
											  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) 
										  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                   END) * f_so.amt_ExchangeRate_GBL),0) ct_PastDueOrder_Amount_ORD
			from fact_salesorder AS f_so
					INNER JOIN Dim_Date AS sdrdp ON f_so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid
												  AND ((sdrdp.DateValue) BETWEEN    (add_months(trunc(current_date - INTERVAL '9' MONTH, 'MM'), 1)-1 + INTERVAL '1' DAY) 
												  								AND (add_months(trunc(current_date, 'MM'), 1)-1))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ItemSubType_Merck) = lower('FPP'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
															   AND (lower(dch.DistributionChannelName) != lower('Intercompany sales'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
					INNER JOIN dim_overallstatusforcreditcheck AS oscc ON f_so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckid
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
					INNER JOIN dim_salesorderitemcategory AS ic ON f_so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
					INNER JOIN DIM_CUSTOMER as dcus on f_so.DIM_CUSTOMERID = dcus.DIM_CUSTOMERID
			WHERE (lower((CASE WHEN dc.DocumentCategory ='C'
			                          AND sdt.DocumentType NOT IN ('KA','KB')
			                          AND oscc.OverallStatusforCreditCheck <> 'B'
			                          AND sois.OverallProcessingStatus <> 'Completely processed'
			                          AND sois.OverallDeliveryStatus <>'Completely processed'
			                          AND sorr.RejectReasonCode = 'Not Set'
			                          AND soegidt.DateValue < CURRENT_DATE THEN 'X'
			                       ELSE 'Not Set' END)) = lower('X'))
			      AND ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                 WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
							 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') 
								 THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			                 ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                            ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END) END >= 0.1))
			group by prt.partnumber, dcus.CUSTOMERNUMBER;

	/* ********************* END: Material-Customer ********************* */

-- >>

	/* ********************* START: Unifying measures for Material-Plant ********************* */

			drop table if exists stg_scn_material_plant_unique;
			create table stg_scn_material_plant_unique as
			select PARTNUMBER, PLANT from stg_scn_material_plant_inventory1
			union
			select PARTNUMBER, PLANT from stg_scn_material_plant_sales1
			union
			select PARTNUMBER, PLANT from stg_scn_material_plant_sales2;
			
			drop table if exists stg_scn_material_plant;
			create table stg_scn_material_plant as
			select  t0.PARTNUMBER, t0.PLANT,
					cast(ifnull(t1.CT_ONHAND_AMOUNT_STPR_IRU, 0.00) 			as decimal(18,5)) as CT_ONHAND_AMOUNT_STPR_IRU,
					cast(ifnull(t1.CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE, 0.00) 	as decimal(18,5)) as CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE,
					cast(ifnull(t1.CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE, 0.00) 	as decimal(18,5)) as CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE,
					cast(ifnull(t2.CT_OPENORDER_AMOUNT, 0.00) 					as decimal(18,5)) as CT_OPENORDER_AMOUNT,
					cast(ifnull(t3.CT_PASTDUEORDER_AMOUNT_ORD, 0.00) 			as decimal(18,5)) as CT_PASTDUEORDER_AMOUNT_ORD
			from stg_scn_material_plant_unique t0
					left join stg_scn_material_plant_inventory1 t1 on t0.PARTNUMBER = t1.PARTNUMBER and t0.PLANT = t1.PLANT
					left join stg_scn_material_plant_sales1     t2 on t0.PARTNUMBER = t2.PARTNUMBER and t0.PLANT = t2.PLANT
					left join stg_scn_material_plant_sales2     t3 on t0.PARTNUMBER = t3.PARTNUMBER and t0.PLANT = t3.PLANT;

			drop table if exists stg_scn_material_plant_hash;
			create table stg_scn_material_plant_hash as
			select 	cast(UPPER(HASH_MD5(PARTNUMBER || '|^|' || PLANT)) as varchar(32))  as PARTNUMBER_PLANT_HSH,
					cast(UPPER(HASH_MD5(PARTNUMBER)) as varchar(32)) 					as PARTNUMBER_HSH,
					cast(UPPER(HASH_MD5(PLANT))      as varchar(32)) 					as PLANT_HSH,
					current_timestamp 				 				 					as PARTNUMBER_PLANT_LDTS,
					cast('AERA BV: Material-Plant Multiple SA' as varchar(50))  	 	as PARTNUMBER_PLANT_RSRC,
					cast(UPPER(HASH_MD5(upper(PARTNUMBER 										|| '|^|' ||
											  PLANT												|| '|^|' ||
											  IFNULL(CT_ONHAND_AMOUNT_STPR_IRU, 0.0) 			|| '|^|' ||
											  IFNULL(CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE, 0.0) 	|| '|^|' ||
											  IFNULL(CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE, 0.0) || '|^|' ||
											  IFNULL(CT_OPENORDER_AMOUNT, 0.0) 					|| '|^|' ||
											  IFNULL(CT_PASTDUEORDER_AMOUNT_ORD, 0.0)
							)))as varchar(32)) 						 					as PARTNUMBER_PLANT_HSH_DIFF,
					PARTNUMBER,
					PLANT,
					CT_ONHAND_AMOUNT_STPR_IRU,
					CT_RESTRICTEDSTOCK_AMOUNT_STDPRICE,
					CT_UNRESTRICTEDSTOCK_AMOUNT_STDPRICE,
					CT_OPENORDER_AMOUNT,
					CT_PASTDUEORDER_AMOUNT_ORD
			from stg_scn_material_plant;

			select * from stg_scn_material_plant_hash;

	/* ********************* END: Unifying measures for Material-Plant ********************* */

-- >>

	/* ********************* START: Unifying measures for Material-Vendor ********************* */
			
			drop table if exists stg_scn_material_vendor_unique;
			create table stg_scn_material_vendor_unique as
			select PARTNUMBER, VENDORNUMBER from stg_scn_material_vendor_purchase1
			union
			select PARTNUMBER, VENDORNUMBER from stg_scn_material_vendor_purchase2;

			drop table if exists stg_scn_material_vendor;
			create table stg_scn_material_vendor as
			select  t0.PARTNUMBER, t0.VENDORNUMBER,
					cast(ifnull(t1.CT_OPENDELIV_AMT_NEXT7DAYS, 0.00) as decimal(18,5)) as CT_OPENDELIV_AMT_NEXT7DAYS,
					cast(ifnull(t1.CT_OPENDELIVSCHEDULE_COUNT, 0.00) as decimal(18,5)) as CT_OPENDELIVSCHEDULE_COUNT,
					cast(ifnull(t2.CT_OPENDELIV_AMT, 0.00) 			 as decimal(18,5)) as CT_OPENDELIV_AMT
			from stg_scn_material_vendor_unique t0
					left join stg_scn_material_vendor_purchase1 t1 on t0.PARTNUMBER = t1.PARTNUMBER and t0.VENDORNUMBER = t1.VENDORNUMBER
					left join stg_scn_material_vendor_purchase2 t2 on t0.PARTNUMBER = t2.PARTNUMBER and t0.VENDORNUMBER = t2.VENDORNUMBER;

			drop table if exists stg_scn_material_vendor_hash;
			create table stg_scn_material_vendor_hash as
			select 	cast(UPPER(HASH_MD5(PARTNUMBER || '|^|' || VENDORNUMBER)) as varchar(32)) 	as PARTNUMBER_VENDOR_HSH,
					cast(UPPER(HASH_MD5(PARTNUMBER)) as varchar(32)) 							as PARTNUMBER_HSH,
					cast(UPPER(HASH_MD5(VENDORNUMBER)) as varchar(32)) 							as VENDOR_HSH,
					current_timestamp 				 				 							as PARTNUMBER_VENDOR_LDTS,
					cast('AERA BV: Material-Vendor Purchase' as varchar(50))  	 				as PARTNUMBER_VENDOR_RSRC,
					cast(UPPER(HASH_MD5(upper(PARTNUMBER 								|| '|^|' ||
											  VENDORNUMBER								|| '|^|' ||
											  IFNULL(CT_OPENDELIV_AMT_NEXT7DAYS, 0.0) 	|| '|^|' ||
											  IFNULL(CT_OPENDELIVSCHEDULE_COUNT, 0.0) 	|| '|^|' ||
											  IFNULL(CT_OPENDELIV_AMT, 0.0) 
							)))as varchar(32)) 						 							as PARTNUMBER_VENDOR_HSH_DIFF,
					PARTNUMBER,
					VENDORNUMBER,
					CT_OPENDELIV_AMT_NEXT7DAYS,
					CT_OPENDELIVSCHEDULE_COUNT,
					CT_OPENDELIV_AMT
			from stg_scn_material_vendor;

			select * from stg_scn_material_vendor_hash;

	/* ********************* END: Unifying measures for Material-Vendor ********************* */

-- >>

	/* ********************* START: Unifying measures for Material-Customer ********************* */
			
			drop table if exists stg_scn_material_customer_unique;
			create table stg_scn_material_customer_unique as
			select PARTNUMBER, CUSTOMERNUMBER from stg_scn_material_customer_sales1
			union
			select PARTNUMBER, CUSTOMERNUMBER from stg_scn_material_customer_sales2
			union
			select PARTNUMBER, CUSTOMERNUMBER from desc stg_scn_material_customer_sales3;

			drop table if exists stg_scn_material_customer;
			create table stg_scn_material_customer as
			select  t0.PARTNUMBER, t0.CUSTOMERNUMBER,
					cast(ifnull(t1.CT_OPENORDER_AMOUNT, 0.00) as decimal(18,5)) 		as CT_OPENORDER_AMOUNT,
					cast(ifnull(t2.CT_YTDORDER_AMOUNT, 0.00) as decimal(18,5)) 			as CT_YTDORDER_AMOUNT,
					cast(ifnull(t3.CT_PASTDUEORDER_AMOUNT_ORD, 0.00) as decimal(18,5)) 	as CT_PASTDUEORDER_AMOUNT_ORD
			from stg_scn_material_customer_unique t0
					left join stg_scn_material_customer_sales1 t1 on t0.PARTNUMBER = t1.PARTNUMBER and t0.CUSTOMERNUMBER = t1.CUSTOMERNUMBER
					left join stg_scn_material_customer_sales2 t2 on t0.PARTNUMBER = t2.PARTNUMBER and t0.CUSTOMERNUMBER = t2.CUSTOMERNUMBER
					left join stg_scn_material_customer_sales3 t3 on t0.PARTNUMBER = t3.PARTNUMBER and t0.CUSTOMERNUMBER = t3.CUSTOMERNUMBER;

			drop table if exists stg_scn_material_customer_hash;
			create table stg_scn_material_customer_hash as
			select 	cast(UPPER(HASH_MD5(PARTNUMBER || '|^|' || CUSTOMERNUMBER)) as varchar(32)) as PARTNUMBER_CUSTOMER_HSH,
					cast(UPPER(HASH_MD5(PARTNUMBER)) as varchar(32)) 							as PARTNUMBER_HSH,
					cast(UPPER(HASH_MD5(CUSTOMERNUMBER)) as varchar(32)) 						as CUSTOMER_HSH,
					current_timestamp 				 				 							as PARTNUMBER_CUSTOMER_LDTS,
					cast('AERA BV: Material-Customer Sales' as varchar(50))  	 				as PARTNUMBER_CUSTOMER_RSRC,
					cast(UPPER(HASH_MD5(upper(PARTNUMBER 						|| '|^|' ||
											  CUSTOMERNUMBER					|| '|^|' ||
											  IFNULL(CT_OPENORDER_AMOUNT, 0.0) 	|| '|^|' ||
											  IFNULL(CT_YTDORDER_AMOUNT, 0.0) 	|| '|^|' ||
											  IFNULL(CT_PASTDUEORDER_AMOUNT_ORD, 0.0) 
							)))as varchar(32)) 						 							as PARTNUMBER_CUSTOMER_HSH_DIFF,
					PARTNUMBER,
					CUSTOMERNUMBER,
					CT_OPENORDER_AMOUNT,
					CT_YTDORDER_AMOUNT,
					CT_PASTDUEORDER_AMOUNT_ORD
			from stg_scn_material_customer;

			select * from stg_scn_material_customer_hash;

	/* ********************* END: Unifying measures for Material-Customer ********************* */