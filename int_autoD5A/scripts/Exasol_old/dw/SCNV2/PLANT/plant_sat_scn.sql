/* Staging tables preparation */

/* ********************* START: Inventory data for Plant ********************* */
drop table if exists stg_scn_plant_inventory;
create table stg_scn_plant_inventory as
select 	ROUND(SUM(ct_onhandqtyiru),2) 										as ct_Stock_Quantity_IRU_pl,
		ROUND(SUM(ct_onhandqtyIRU * amt_stdpricepmra_merck),2) 				as ct_OnHand_Amount_StPr_IRU_pl,
		ROUND(SUM(ct_totalrestrictedstockiru * amt_stdpricepmra_merck),2) 	as ct_RestrictedStock_Amount_StdPrice_pl,
		ROUND(SUM(ct_stockqtyiru * amt_stdpricepmra_merck),2) 				as ct_UnrestrictedStock_Amount_StdPrice_pl,
		dp.PlantCode
from fact_inventoryaging AS f_invagng
	 INNER JOIN dim_storagelocation AS stloc ON f_invagng.Dim_StorageLocationid = stloc.dim_storagelocationid
	 inner join dim_plant dp on f_invagng.dim_plantid = dp.dim_plantid
where (lower(CASE WHEN right(stloc.LocationCode, 1) = 'T' 
                  THEN 'Yes'
		          ELSE 'No' END) = lower('No'))
group by dp.PlantCode;
			
			
/* ********************* START: Sales data for Plant ********************* */

/* ********************* START: Open Order Qty ********************* */
						 
drop table if exists stg_scn_plant_sales1;
create table stg_scn_plant_sales1 as
select  dp.plantcode as plantcode,
		ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 
                             THEN 0.0000
                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
							 THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 
                                             THEN 0.0000
                                        ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
                                   END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 
                                                                            THEN f_so.ct_PriceUnit 
                                                                       ELSE 1 
                                                                  END))
                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 
                                        THEN 0.0000
                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
                              END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 
                                                                       THEN f_so.ct_PriceUnit 
                                                                  ELSE 1 
                                                             END))
                   END) * f_so.amt_ExchangeRate_GBL), 2) ct_OpenOrder_Amount_pl
from fact_salesorder AS f_so
		INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
            						   AND (soegidt.DateValue BETWEEN (trunc(current_date) - INTERVAL '1' YEAR) AND trunc(current_date)
                 							OR soegidt.DateValue BETWEEN trunc(current_date,'YEAR') AND trunc(current_date)
											OR soegidt.DateValue BETWEEN current_date AND (current_date + 364))
		INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
            										  AND (lower(sorr.RejectReasonCode) = lower('Not Set'))
		INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
												AND ((CASE WHEN sdt.DocumentType = 'AF' THEN 'IN'
                      									   WHEN sdt.DocumentType = 'AG' THEN 'QT'
										                   WHEN sdt.DocumentType = 'AU' THEN 'SI'
										                   WHEN sdt.DocumentType = 'G2' THEN 'CR'
										                   WHEN sdt.DocumentType = 'KL' THEN 'FD'
										                   WHEN sdt.DocumentType = 'KM' THEN 'CQ'
										                   WHEN sdt.DocumentType = 'KN' THEN 'SD'
										                   WHEN sdt.DocumentType = 'L2' THEN 'DR'
										                   WHEN sdt.DocumentType = 'LP' THEN 'DS'
										                   WHEN sdt.DocumentType = 'TA' THEN 'OR'
										                   ELSE sdt.DocumentType 
                                                      END) NOT IN (('KA'),('KB')))
		INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
            								  AND (lower(dc.DocumentCategory) = lower('C'))
		INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
            									  AND (lower(dch.DistributionChannelCode) != lower('20'))
		INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
            										AND (lower(sois.OverallProcessingStatus) != lower('Completely processed'))
													AND (lower(sois.OverallDeliveryStatus) != lower('Completely processed'))
		inner join dim_plant dp on f_so.dim_plantid = dp.dim_plantid
where ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 
				  THEN 0.0000
			 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
				  THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 
								  THEN 0.0000
                             ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) 
                        END)
             WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 
                  THEN 0.0000
             ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) 
         END > 0))
group by dp.plantcode;
			
/* ********************* START: Past Due Amt ********************* */
								 
drop table if exists stg_scn_plant_sales2;
create table stg_scn_plant_sales2 as
select	dp.plantcode,
		ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
                        WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') 
							THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
								  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) 
							  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
                   END) * f_so.amt_ExchangeRate_GBL),0) ct_PastDueOrder_Amount_ORD_pl
from fact_salesorder AS f_so
		INNER JOIN Dim_Date AS sdrdp ON f_so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid
									  AND ((sdrdp.DateValue) BETWEEN    (add_months(trunc(current_date - INTERVAL '9' MONTH, 'MM'), 1)-1 + INTERVAL '1' DAY) 
									  								AND (add_months(trunc(current_date, 'MM'), 1)-1))
		INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ItemSubType_Merck) = lower('FPP'))
		INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
												   AND (lower(dch.DistributionChannelName) != lower('Intercompany sales'))
		INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
		INNER JOIN dim_overallstatusforcreditcheck AS oscc ON f_so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckid
		INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
		INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
		INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
		INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
		INNER JOIN dim_salesorderitemcategory AS ic ON f_so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
		inner join dim_plant dp on f_so.dim_plantid = dp.dim_plantid
WHERE (lower((CASE WHEN dc.DocumentCategory ='C'
                          AND sdt.DocumentType NOT IN ('KA','KB')
                          AND oscc.OverallStatusforCreditCheck <> 'B'
                          AND sois.OverallProcessingStatus <> 'Completely processed'
                          AND sois.OverallDeliveryStatus <>'Completely processed'
                          AND sorr.RejectReasonCode = 'Not Set'
                          AND soegidt.DateValue < CURRENT_DATE THEN 'X'
                       ELSE 'Not Set' END)) = lower('X'))
      AND ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
                 WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
				 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') 
					 THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
                                ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
                 ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
                            ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END) END >= 0.1))
group by dp.plantcode;
			 
/* ********************* START: Custom - SO % OTIF ********************* */
									   
drop table if exists stg_scn_plant_sales3;
create table stg_scn_plant_sales3 as
SELECT pl.PlantCode AS plantcode,
       ROUND(COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN f_so.dd_SalesDocNo
                                 ELSE 'Not Set'
                            END) - 
							CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0
                                                        ELSE 1
                                                   END) > 0 THEN 1
                                 ELSE 0
                            END,0) AS ct_total_sales_orders,
       ROUND(COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y'
                                     AND f_so.dd_soheaderotif_merck = 'Y' THEN f_so.dd_SalesDocNo
                                 ELSE 'Not Set'
                            END) - 
                            CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y'
                                                             AND f_so.dd_soheaderotif_merck = 'Y' THEN 0
                                                        ELSE 1
                                                   END) > 0 THEN 1
                                 ELSE 0
                            END,0) AS ct_total_sales_orders_otif,
       ROUND(((COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y'
                                        AND f_so.dd_soheaderotif_merck = 'Y' THEN f_so.dd_SalesDocNo
                                   ELSE 'Not Set'
                              END) - 
                              CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y'
                                                               AND f_so.dd_soheaderotif_merck = 'Y' THEN 0.0000
                                                          ELSE 1.0000
                                                     END) > 0 
                                   THEN 1.0000
                                   ELSE 0.0000
                              END) / (CASE WHEN (COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN f_so.dd_SalesDocNo
                                                                     ELSE 'Not Set'
                                                                END) - 
                                                                CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
                                                                                            ELSE 1.0000
                                                                                       END) > 0 THEN 1.0000
                                                                     ELSE 0.0000
                                                                END) = 0.0000 THEN 1.0000
                                           ELSE (COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN f_so.dd_SalesDocNo
                                                                      ELSE 'Not Set'
                                                                END) - CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
                                                                                                   ELSE 1.0000
                                                                                               END) > 0 THEN 1.0000
                                                                            ELSE 0.0000
                                                                       END)
                                      END)) * 100,2) AS ct_sales_orders_otif_perc,
       ROUND(((COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y'
                                        AND f_so.dd_solineotif_merck = 'Y' THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo)
                                   ELSE 'Not Set'
                              END) - CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y'
                                                                      AND f_so.dd_solineotif_merck = 'Y' THEN 0.0000
                                                                 ELSE 1.0000
                                                            END) > 0.0000 THEN 1.0000
                                          ELSE 0.0000
                                     END) / (CASE WHEN (COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo)
                                                                            ELSE 'Not Set'
                                                                       END) - CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
                                                                                                          ELSE 1.0000
                                                                                                     END) > 0.0000 THEN 1.0000
                                                                                   ELSE 0.0000
                                                                              END) = 0.0000 THEN 1.0000
                                                  ELSE (COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo)
                                                                            ELSE 'Not Set'
                                                                       END) - CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
                                                                                                          ELSE 1.0000
                                                                                                     END) > 0.0000 THEN 1.0000
                                                                                   ELSE 0.0000
                                                                              END)
                                             END)) * 100,2) AS ct_sales_orders_lines_otif_perc
FROM fact_salesorder AS f_so
INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
AND (soegidt.DateValue BETWEEN CONCAT(year(CURRENT_DATE),'-01-01') AND CURRENT_DATE)
INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
AND (lower(prt.ProductHierarchy) = lower('Not Set'))
INNER JOIN Dim_Plant AS pl ON f_so.Dim_Plantid = pl.Dim_Plantid
INNER JOIN Dim_Date AS sdd ON f_so.Dim_DateidSchedDelivery = sdd.Dim_Dateid
WHERE (lower(f_so.dd_ItemRelForDelv) = lower('X'))
  AND (lower(f_so.dd_otifenabledflag) = lower('Y'))
GROUP BY pl.PlantCode;
				
				    			
/* ********************* START: Purchasing data for Plant ********************* */
/* ********************* START: START: Custom - % OTIF Document Level: ordering plant ********************* */
                    						   
drop table if exists stg_scn_plant_purchase1;
create table stg_scn_plant_purchase1 as
SELECT plord.plantcode,
       ROUND(count(DISTINCT f_deliv.dd_DocumentNo),0) AS ct_purchase_doc_count_ord_pl,
       ROUND(count(DISTINCT concat(f_deliv.dd_DocumentNo, f_deliv.dd_DocumentItemNo,f_deliv.dd_ScheduleNo)),0) AS ct_deliv_sched_count_ord_pl,
       ROUND(100 * (1 - CAST((COUNT(DISTINCT CASE
                                                    WHEN (f_deliv.dd_iotif_merck = 'N'
                                                          OR f_deliv.dd_iotif_merck = 'Not Set') THEN dd_documentno
                                                    ELSE NULL
                                                END)) as DECIMAL(18,4)) / (CASE
                                                                   WHEN CAST(COUNT(DISTINCT dd_documentno) as DECIMAL(18,4)) <> 0 THEN CAST(COUNT(DISTINCT dd_documentno)as DECIMAL(18,4))
                                                                   ELSE 1.000
                                                               END)),2) AS ct_iotif_doclvl_ord_pl,
       ROUND(100 * cast((COUNT(DISTINCT CASE
                                            WHEN (f_deliv.dd_ontime_stat = 'Y'
                                                  AND f_deliv.dd_qtyinfull = 'Y') THEN CONCAT(f_deliv.dd_documentno,f_deliv.dd_documentitemno,f_deliv.dd_scheduleno)
                                            ELSE NULL
                                        END)) AS decimal (18,4)) / (CASE
                                                                        WHEN cast(COUNT(DISTINCT CONCAT(f_deliv.dd_documentno,f_deliv.dd_documentitemno, f_deliv.dd_scheduleno)) AS decimal (18,4)) <> 0 THEN cast(COUNT(DISTINCT CONCAT(f_deliv.dd_documentno, f_deliv.dd_documentitemno,f_deliv.dd_scheduleno)) AS decimal (18,4))
                                                                        ELSE 1.0000
                                                                    END),2) AS ct_iotif_schedlvl_ord_pl,
       ROUND(count(DISTINCT f_deliv.dd_DocumentNo) - COUNT(DISTINCT CASE
                                                                        WHEN (f_deliv.dd_iotif_merck = 'N'
                                                                              OR f_deliv.dd_iotif_merck = 'Not Set') THEN dd_documentno
                                                                        ELSE NULL
                                                                    END),0) AS ct_iotifcount_doclvl_ord_pl,
       ROUND(SUM(CASE
                     WHEN (f_deliv.dd_ontime_stat = 'Y'
                           AND f_deliv.dd_qtyinfull = 'Y') THEN 1
                     ELSE 0
                 END),2) AS ct_iotifcount_schedlvl_ord_pl 
FROM fact_purchase AS f_deliv
INNER JOIN Dim_Date AS dtst ON f_deliv.Dim_DateidStatDelivery = dtst.Dim_Dateid
AND (dtst.DateValue BETWEEN CONCAT(year(CURRENT_DATE),'-01-01') AND CURRENT_DATE)
INNER JOIN Dim_Plant AS plord ON f_deliv.Dim_PlantidOrdering = plord.Dim_Plantid
AND ((plord.tacticalring_merck) IN (('Large Molecule'),('Small Molecule')))
INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid
AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))
AND ((prt.MRPController) NOT IN (('690'),('797'),('798')))
INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid
AND (lower(CASE
                   WHEN atrb.ItemReturn= 'X' THEN 'Yes'
                   ELSE 'No'
               END) != lower('Yes'))
AND (lower(CASE
                   WHEN atrb.ItemGRIndicator= 'X' THEN 'Yes'
                   ELSE 'No'
               END) != lower('No'))
INNER JOIN dim_documenttype AS doctyp ON f_deliv.Dim_DocumentTypeid = doctyp.dim_documenttypeid
AND ((doctyp.Type) IN (('NB'),('LPA'),('UB')))
WHERE (lower(f_deliv.dd_ourreference) = lower('Not Set'))
  AND (lower(f_deliv.dd_intraplantflag_merck) = lower('N'))
  AND (lower(f_deliv.dd_lpa_merck) = lower('Not Set'))
GROUP BY plord.plantcode;  	      
					
					
/* ********************* START: Custom - % OTIF Document Level: supplying plant ********************* */
			   
drop table if exists stg_scn_plant_purchase2;
create table stg_scn_plant_purchase2 as
SELECT plord.plantcode,
       ROUND(count(DISTINCT f_deliv.dd_DocumentNo),0) AS ct_purchase_doc_count_supply_pl,
       ROUND(count(DISTINCT concat(f_deliv.dd_DocumentNo, f_deliv.dd_DocumentItemNo,f_deliv.dd_ScheduleNo)),0) AS ct_deliv_sched_count_supply_pl,
       ROUND(100 * (1 - CAST((COUNT(DISTINCT CASE
                                                    WHEN (f_deliv.dd_iotif_merck = 'N'
                                                          OR f_deliv.dd_iotif_merck = 'Not Set') THEN dd_documentno
                                                    ELSE NULL
                                                END)) as DECIMAL(18,4)) / (CASE
                                                                   WHEN CAST(COUNT(DISTINCT dd_documentno) AS DECIMAL(18,4)) <> 0 THEN CAST(COUNT(DISTINCT dd_documentno) as DECIMAL(18,4))
                                                                   ELSE 1.000
                                                               END)),2) AS ct_iotif_doclvl_supply_pl,
       ROUND(100 * cast((COUNT(DISTINCT CASE
                                            WHEN (f_deliv.dd_ontime_stat = 'Y'
                                                  AND f_deliv.dd_qtyinfull = 'Y') THEN CONCAT(f_deliv.dd_documentno,f_deliv.dd_documentitemno,f_deliv.dd_scheduleno)
                                            ELSE NULL
                                        END)) AS decimal (18,4)) / (CASE
                                                                        WHEN cast(COUNT(DISTINCT CONCAT(f_deliv.dd_documentno,f_deliv.dd_documentitemno, f_deliv.dd_scheduleno)) AS decimal (18,4)) <> 0 THEN cast(COUNT(DISTINCT CONCAT(f_deliv.dd_documentno, f_deliv.dd_documentitemno,f_deliv.dd_scheduleno)) AS decimal (18,4))
                                                                        ELSE 1.0000
                                                                    END),2) AS ct_iotif_schedlvl_supply_pl,
       ROUND(count(DISTINCT f_deliv.dd_DocumentNo) - COUNT(DISTINCT CASE
                                                                        WHEN (f_deliv.dd_iotif_merck = 'N'
                                                                              OR f_deliv.dd_iotif_merck = 'Not Set') THEN dd_documentno
                                                                        ELSE NULL
                                                                    END),0) AS ct_iotifcount_doclvl_supply_pl,
       ROUND(SUM(CASE
                     WHEN (f_deliv.dd_ontime_stat = 'Y'
                           AND f_deliv.dd_qtyinfull = 'Y') THEN 1
                     ELSE 0
                 END),2) AS ct_iotifcount_schedlvl_supply_pl /* , dtst.CalendarMonthID Row_ord_0 */
FROM fact_purchase AS f_deliv
INNER JOIN Dim_Date AS dtst ON f_deliv.Dim_DateidStatDelivery = dtst.Dim_Dateid
AND (dtst.DateValue BETWEEN CONCAT(year(CURRENT_DATE),'-01-01') AND CURRENT_DATE)
INNER JOIN Dim_Plant AS plord ON f_deliv.dim_plantidsupplying = plord.Dim_Plantid
AND ((plord.tacticalring_merck) IN (('Large Molecule'),('Small Molecule')))
INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid
AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))
AND ((prt.MRPController) NOT IN (('690'),('797'),('798')))
INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid
AND (lower(CASE
                   WHEN atrb.ItemReturn= 'X' THEN 'Yes'
                   ELSE 'No'
               END) != lower('Yes'))
AND (lower(CASE
                   WHEN atrb.ItemGRIndicator= 'X' THEN 'Yes'
                   ELSE 'No'
               END) != lower('No'))
INNER JOIN dim_documenttype AS doctyp ON f_deliv.Dim_DocumentTypeid = doctyp.dim_documenttypeid
AND ((doctyp.Type) IN (('NB'),('LPA'),('UB')))
WHERE (lower(f_deliv.dd_ourreference) = lower('Not Set'))
  AND (lower(f_deliv.dd_intraplantflag_merck) = lower('N'))
  AND (lower(f_deliv.dd_lpa_merck) = lower('Not Set'))
GROUP BY plord.plantcode;
					
/* ********************* START: Production data for Plant ********************* */
                        
/* ********************* START: START: Custom  - %SA Last Month ********************* */
						
drop table if exists stg_scn_plant_productionsnapshot;
create table stg_scn_plant_productionsnapshot as
SELECT pl.plantcode,
       ROUND(100*sum(CASE WHEN (origactfd.DateValue) >= CASE WHEN Month(CURRENT_DATE) = 1 THEN Concat(Year(add_months(CURRENT_DATE,-12)),'-01-01')
                                                             ELSE Concat(Year(CURRENT_DATE),'-01-01')
                                                        END                                         
                               AND (origactfd.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 
                               THEN (CASE WHEN ((fprodforsn.ct_GRQty<>0
                                           AND ((fprodforsn.dd_unlimitedoverdelflagorder = 'Not Set' 
                                           AND f_prsnap.ct_OrderItemQty_orig_merck * (1 - fprodforsn.ct_orderunderdeltolerance/100) <= fprodforsn.ct_GRQty
                                           AND fprodforsn.ct_GRQty <= f_prsnap.ct_OrderItemQty_orig_merck * (1 + fprodforsn.ct_OrderOverdelivTolerance/100))
                                           OR (fprodforsn.dd_unlimitedoverdelflagorder = 'X'
                                           AND f_prsnap.ct_OrderItemQty_orig_merck * (1 - fprodforsn.ct_orderunderdeltolerance/100) <= f_prsnap.ct_GRQty)))
                                           AND (fprodforsn.dim_dateidactualheaderfinishdate_merck>1
                                           AND (ahfdfprod.CalendarYear) <= (origactfd.CalendarYear)
                                           AND week((ahfdfprod.DateValue)) - week((origactfd.DateValue)) <= 0)) THEN 1.0000
                                     ELSE 0.0000
                                     END)
                     ELSE 0
                     END)/(CASE Count(DISTINCT (CASE WHEN f_prsnap.dd_ordernumber=0.0000 THEN NULL
                                                     ELSE (CASE WHEN (origactfd.DateValue) >= CASE WHEN Month(CURRENT_DATE) = 1 THEN Concat(Year(add_months(CURRENT_DATE,-12)),'-01-01')
                                                                                                   ELSE Concat(Year(CURRENT_DATE),'-01-01')
                                                                                              END
                                                                    AND (origactfd.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 
                                                                THEN (f_prsnap.dd_ordernumber || to_char(f_prsnap.dim_dateidsnapshot))
                                                           END)
                                                END))
                                WHEN 0 THEN 1
                                ELSE Count(DISTINCT (CASE WHEN f_prsnap.dd_ordernumber=0.0000 THEN NULL
                                                          ELSE (CASE WHEN (origactfd.DateValue) >= CASE WHEN Month(CURRENT_DATE) = 1 THEN Concat(Year(add_months(CURRENT_DATE,-12)),'-01-01')
										                                                                ELSE Concat(Year(CURRENT_DATE),'-01-01')
										                                                           END
                                                                        AND (origactfd.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 THEN (f_prsnap.dd_ordernumber || to_char(f_prsnap.dim_dateidsnapshot))
                                                                END)
                                                     END))
                           END) ,3) AS ct_actytd_endmonth_prd_pl,
       ROUND(100*sum(CASE WHEN (origactfd.DateValue) >= Concat(Year(add_months(CURRENT_DATE,-1)), '-', LPAD(Month(Add_Months(CURRENT_DATE,-1)),2,'0'),'-01')
                              AND (origactfd.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 
                          THEN (CASE WHEN ((fprodforsn.ct_GRQty<>0
                                          AND ((fprodforsn.dd_unlimitedoverdelflagorder = 'Not Set'
                                          AND f_prsnap.ct_OrderItemQty_orig_merck * (1 - fprodforsn.ct_orderunderdeltolerance/100) <= fprodforsn.ct_GRQty
                                          AND fprodforsn.ct_GRQty <= f_prsnap.ct_OrderItemQty_orig_merck * (1 + fprodforsn.ct_OrderOverdelivTolerance/100))
                                          OR (fprodforsn.dd_unlimitedoverdelflagorder = 'X'
                                          AND f_prsnap.ct_OrderItemQty_orig_merck * (1 - fprodforsn.ct_orderunderdeltolerance/100) <= f_prsnap.ct_GRQty)))
                                          AND (fprodforsn.dim_dateidactualheaderfinishdate_merck>1
                                          AND (ahfdfprod.CalendarYear) <= (origactfd.CalendarYear)
                                          AND week((ahfdfprod.DateValue)) - week((origactfd.DateValue)) <= 0)) THEN 1.0000
                                     ELSE 0.0000
                                END)
                         ELSE 0
                     END) / (CASE Count(DISTINCT (CASE WHEN f_prsnap.dd_ordernumber=0.0000 THEN NULL
                                                       ELSE (CASE WHEN (origactfd.DateValue) >= Concat(Year(add_months(CURRENT_DATE,-1)), '-', LPAD(Month(Add_Months(CURRENT_DATE,-1)),2,'0'),'-01')
                                                                     AND (origactfd.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 THEN (f_prsnap.dd_ordernumber || to_char(f_prsnap.dim_dateidsnapshot))
                                                             END)
                                                  END))
                                 WHEN 0 THEN 1
                                 ELSE Count(DISTINCT (CASE WHEN f_prsnap.dd_ordernumber=0.0000 THEN NULL
                                                           ELSE (CASE WHEN (origactfd.DateValue) >= Concat(Year(add_months(CURRENT_DATE,-1)), '-', LPAD(Month(Add_Months(CURRENT_DATE,-1)),2,'0'),'-01')
                                                                         AND (origactfd.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 THEN (f_prsnap.dd_ordernumber || to_char(f_prsnap.dim_dateidsnapshot))
                                                                 END)
                                                      END))
                             END) ,3) ct_sa_last_month_prod_pl
FROM fact_productionorder_snapshot AS f_prsnap
INNER JOIN Dim_Date AS origactfd ON f_prsnap.dim_dateidscheduledfinish_orig_merck = origactfd.Dim_Dateid
INNER JOIN Dim_Part AS ip ON f_prsnap.Dim_PartidItem = ip.Dim_Partid
AND (lower(ip.mrpcontrollerplantcode) != lower('ES10/402'))
AND ((ip.batchclass_merck) IN (('ACTIVE'),('ACTIVE_PROP_UNIT'),('ANTIGEN'),('ANTIGEN_NO_FPE'),('BIO_BULK_SPHEREONS'), ('BULK_BIO'),('BULK_PHA'),('EXCIPIENT'),('FPL_PHA/BIO_OTHER'),('FPP_BIO'),('FPP_BIO_DW'),('FPP_PHA'),('FPU_BIO_LIVE'), ('FPU_BIO_LIVE_DW'),('FPU_BIO_OTHER'),('FPU_PHA'),('INTERMEDIATE'),('MEDIA'),('SEED'),('ANTIGEN_2T')))
INNER JOIN dim_productionordertype AS otype ON f_prsnap.Dim_ordertypeid = otype.dim_productionordertypeid
AND (lower(otype.TypeCode) != lower('ZRDV'))
INNER JOIN Dim_Plant AS pl ON f_prsnap.Dim_Plantid = pl.Dim_Plantid
INNER JOIN dim_date AS x_varDate ON x_varDate.DateValue = CURRENT_DATE
AND origactfd.CompanyCode = x_varDate.CompanyCode
AND origactfd.plantcode_factory = x_varDate.plantcode_factory
AND (origactfd.CalendarWeekID != x_varDate.CalendarWeekID)
INNER JOIN fact_productionorder AS fprodforsn ON fprodforsn.dd_ordernumber = f_prsnap.dd_ordernumber
AND fprodforsn.dd_orderitemno = f_prsnap.dd_orderitemno
INNER JOIN Dim_Date AS ahfdfprod ON f_prsnap.dim_dateidactualheaderfinishdate_prodorder = ahfdfprod.Dim_Dateid
WHERE (lower(f_prsnap.dd_cancelledorder_orig_merck) = lower('Not Set'))
GROUP BY pl.plantcode;
			
					
/* ********************* START: Quality/Inspection Lot data for Plant ********************* */
                        
/* ********************* START:  Custom  - % LROT Last Month ********************* */
						
drop table if exists stg_scn_plant_quality;
create table stg_scn_plant_quality as
SELECT pl.plantcode, 
       ROUND(100*sum(CASE WHEN (dtgr.DateValue) >= (CASE WHEN Month(CURRENT_DATE) = 1 THEN Concat(Year(add_months(CURRENT_DATE,-12)), '-01-01')
                                                         ELSE Concat(Year(CURRENT_DATE),'-01-01')
                                                    END)
                               AND (dtgr.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 
                          THEN (CASE WHEN f_il.dim_dateidusagedecisionmade=1 THEN 0.0000
                                     ELSE (CASE WHEN cast((CASE WHEN dlgrq.isapublicholiday = 0 THEN dmd.BusinessDaysSeqNo - dlgrq.BusinessDaysSeqNo - ct_diffpublicholiday
                                                                ELSE dmd.BusinessDaysSeqNo - nxtgrq.BusinessDaysSeqNo - ct_diffpublicholiday
                                                           END) AS decimal (18,0)) <= 
                                                     cast((CASE WHEN ilo.inspectionlotorigincode = '01' THEN ct_grprocessingtime_purchaseord
                                                                WHEN ilo.inspectionlotorigincode = '04' THEN ct_grprocessingtime_prodord
                                                                ELSE prt.GRProcessingTime
                                                           END) AS decimal) THEN 1.0000
                                                ELSE 0.0000
                                           END)
                                END)
                          ELSE 0
                      END)/ CASE WHEN count(DISTINCT (CASE WHEN f_il.dd_inspectionlotno= 0.0000 THEN NULL
                                                           ELSE (CASE WHEN (dtgr.DateValue) >= (CASE WHEN Month(CURRENT_DATE) = 1 THEN Concat(Year(add_months(CURRENT_DATE,-12)), '-01-01')
                                                                                                     ELSE Concat(Year(CURRENT_DATE), '-01-01')
                                                                                                END)
                                                                           AND (dtgr.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 THEN f_il.dd_inspectionlotno
                                                                      ELSE NULL
                                                                 END)
                                                           END)) =0 THEN 1
                                 ELSE count(DISTINCT (CASE WHEN f_il.dd_inspectionlotno= 0.0000 THEN NULL
                                                           ELSE (CASE WHEN (dtgr.DateValue) >= (CASE WHEN Month(CURRENT_DATE) = 1 THEN Concat(Year(add_months(CURRENT_DATE,-12)), '-01-01')
                                                                                                     ELSE Concat(Year(CURRENT_DATE), '-01-01')
                                                                                                END)
                                                                           AND (dtgr.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 THEN f_il.dd_inspectionlotno
                                                                      ELSE NULL
                                                                 END)
                                                      END))
                             END,2) AS ct_lrot_ytd_insp_pl,
       ROUND(100*sum(CASE WHEN (dtgr.DateValue) >= Concat(Year(add_months(CURRENT_DATE,-1)), '-', LPAD(Month(Add_Months(CURRENT_DATE,-1)),2,'0'),'-01')
                               AND (dtgr.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 THEN (CASE WHEN f_il.dim_dateidusagedecisionmade=1 THEN 0.0000
                                                                                                        ELSE (CASE WHEN cast((CASE WHEN dlgrq.isapublicholiday = 0 THEN dmd.BusinessDaysSeqNo - dlgrq.BusinessDaysSeqNo - ct_diffpublicholiday
                                                                                                                                   ELSE dmd.BusinessDaysSeqNo - nxtgrq.BusinessDaysSeqNo - ct_diffpublicholiday
                                                                                                                              END) AS decimal) <= cast((CASE WHEN ilo.inspectionlotorigincode = '01' THEN ct_grprocessingtime_purchaseord
                                                                                                                                                             WHEN ilo.inspectionlotorigincode = '04' THEN ct_grprocessingtime_prodord
                                                                                                                                                             ELSE prt.GRProcessingTime
                                                                                                                                                        END) AS decimal) THEN 1.0000
                                                                                                                   ELSE 0.0000
                                                                                                              END)
                                                                                                    END)
                          ELSE 0
                      END)/ CASE WHEN count(DISTINCT (CASE WHEN f_il.dd_inspectionlotno= 0.0000 THEN NULL
                                                           ELSE (CASE WHEN (dtgr.DateValue) >= Concat(Year(add_months(CURRENT_DATE,-1)), '-', LPAD(Month(Add_Months(CURRENT_DATE,-1)),2,'0'),'-01')
                                                                           AND (dtgr.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 THEN f_il.dd_inspectionlotno
                                                                      ELSE NULL
                                                                 END)
                                                      END)) = 0 THEN 1
                                 ELSE count(DISTINCT (CASE WHEN f_il.dd_inspectionlotno= 0.0000 THEN NULL
                                                           ELSE (CASE WHEN (dtgr.DateValue) >= Concat(Year(add_months(CURRENT_DATE,-1)), '-', LPAD(Month(Add_Months(CURRENT_DATE,-1)),2,'0'),'-01')
                                                                            AND (dtgr.DateValue) <= TRUNC(CURRENT_DATE,'MM')-1 THEN f_il.dd_inspectionlotno
                                                                      ELSE NULL
                                                                 END)
                                                       END))
                            END,2) ct_lrot_lastmonth_insp_pl                                                                              
FROM fact_inspectionlot AS f_il
INNER JOIN dim_inspectionlotorigin AS ilo ON f_il.dim_inspectionlotoriginid = ilo.dim_inspectionlotoriginid
AND ((ilo.InspectionLotOriginCode) IN (('04'),('01'),('08')))
INNER JOIN dim_inspectionlotstatus AS ils ON f_il.Dim_InspectionLotStatusId = ils.dim_inspectionlotstatusid
AND (lower(CASE
                   WHEN ils.LotCancelled = 'X' THEN 'Yes'
                   ELSE 'No'
               END) = lower('No'))
INNER JOIN dim_productionordertype AS dpot ON f_il.dim_productionordertypeid = dpot.dim_productionordertypeid
AND (lower(dpot.TypeCode) != lower('ZRDV'))
INNER JOIN Dim_Part AS prt ON f_il.Dim_Partid = prt.Dim_Partid
AND ((prt.batchclass_merck) IN (('ACTIVE'),('ACTIVE_PROP_UNIT'),('ANTIGEN'),('ANTIGEN_2T'),('ANTIGEN_NO_FPE'), ('BIO_BULK_SPHEREONS'),('BULK_BIO'),('BULK_PHA'),('EXCIPIENT'),('FPL_PHA/BIO_OTHER'),('FPP_BIO'),('FPP_BIO_DW'), ('FPP_PHA'),('FPU_BIO_LIVE'),('FPU_BIO_LIVE_DW'),('FPU_BIO_OTHER'),('FPU_PHA'),('INTERMEDIATE'), ('MEDIA'),('SEED'),('SEED_WITH_FPE')))
INNER JOIN Dim_Plant AS pl ON f_il.Dim_Plantid = pl.Dim_Plantid
INNER JOIN Dim_Date AS dlgrq ON f_il.dim_lastgrdate_q_id = dlgrq.Dim_Dateid
INNER JOIN Dim_Date AS nxtgrq ON f_il.dim_nextbusday_lastgrdate_q_id = nxtgrq.Dim_Dateid
INNER JOIN Dim_Date AS dtgr ON f_il.dim_targetreleasedateid = dtgr.Dim_Dateid
INNER JOIN Dim_Date AS dmd ON f_il.dim_dateidusagedecisionmade = dmd.Dim_Dateid
WHERE (lower(f_il.dd_posttoinspstock) = lower('X'))
  AND ((f_il.dd_movementtype) IN (('101'),('309'),('Not Set')))
  AND (lower(CASE
                     WHEN pl.PlantCode ='USC0'
                          AND prt.prod_supervisor ='110' THEN 'Yes'
                     ELSE 'No'
                 END) = lower('No'))
GROUP BY pl.plantcode;

/* ********************* END: Sales data for Plant ********************* */


/* ********************* START: Unifying measures for Plant ********************* */

drop table if exists stg_scn_plant_unique;
create table stg_scn_plant_unique as
select plantcode from stg_scn_plant_sales1 union
select plantcode from stg_scn_plant_sales2 union
select plantcode from stg_scn_plant_sales3 union
select plantcode from stg_scn_plant_purchase1 union
select plantcode from stg_scn_plant_purchase2 union
select plantcode from stg_scn_plant_productionsnapshot union
select plantcode from stg_scn_plant_quality;



drop table if exists stg_scn_plant;
create table stg_scn_plant as
select  t0.plantcode,
        cast(ifnull(t1.CT_OPENORDER_AMOUNT_PL, 0.0) as decimal(18,5)) CT_OPENORDER_AMOUNT_PL,
		cast(ifnull(t2.CT_PASTDUEORDER_AMOUNT_ORD_PL, 0.0) as decimal(18,5)) CT_PASTDUEORDER_AMOUNT_ORD_PL,
		cast(ifnull(t3.CT_TOTAL_SALES_ORDERS, 0.0) as decimal(18,5)) CT_TOTAL_SALES_ORDERS,
		cast(ifnull(t3.CT_TOTAL_SALES_ORDERS_OTIF, 0.0) as decimal(18,5)) CT_TOTAL_SALES_ORDERS_OTIF,		
		cast(ifnull(t3.CT_SALES_ORDERS_OTIF_PERC, 0.0) as decimal(18,5)) CT_SALES_ORDERS_OTIF_PERC, 		
		cast(ifnull(t3.CT_SALES_ORDERS_LINES_OTIF_PERC, 0.0) as decimal(18,5)) CT_SALES_ORDERS_LINES_OTIF_PERC,	
		cast(ifnull(t4.CT_PURCHASE_DOC_COUNT_ORD_PL, 0.0) as decimal(18,5)) CT_PURCHASE_DOC_COUNT_ORD_PL,		
		cast(ifnull(t4.CT_DELIV_SCHED_COUNT_ORD_PL, 0.0) as decimal(18,5)) CT_DELIV_SCHED_COUNT_ORD_PL, 		
		cast(ifnull(t4.CT_IOTIF_DOCLVL_ORD_PL, 0.0) as decimal(18,5)) CT_IOTIF_DOCLVL_ORD_PL,		
		cast(ifnull(t4.CT_IOTIF_SCHEDLVL_ORD_PL, 0.0) as decimal(18,5)) CT_IOTIF_SCHEDLVL_ORD_PL,		
		cast(ifnull(t4.CT_IOTIFCOUNT_DOCLVL_ORD_PL, 0.0) as decimal(18,5)) CT_IOTIFCOUNT_DOCLVL_ORD_PL,		
		cast(ifnull(t4.CT_IOTIFCOUNT_SCHEDLVL_ORD_PL, 0.0) as decimal(18,5)) CT_IOTIFCOUNT_SCHEDLVL_ORD_PL,	
		cast(ifnull(t5.CT_PURCHASE_DOC_COUNT_SUPPLY_PL, 0.0) as decimal(18,5)) CT_PURCHASE_DOC_COUNT_SUPPLY_PL, 		
		cast(ifnull(t5.CT_DELIV_SCHED_COUNT_SUPPLY_PL, 0.0) as decimal(18,5)) CT_DELIV_SCHED_COUNT_SUPPLY_PL, 		
		cast(ifnull(t5.CT_IOTIF_DOCLVL_SUPPLY_PL, 0.0) as decimal(18,5)) CT_IOTIF_DOCLVL_SUPPLY_PL,		
		cast(ifnull(t5.CT_IOTIF_SCHEDLVL_SUPPLY_PL, 0.0) as decimal(18,5)) CT_IOTIF_SCHEDLVL_SUPPLY_PL, 		
		cast(ifnull(t5.CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL, 0.0) as decimal(18,5)) CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL, 		
		cast(ifnull(t5.CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL, 0.0) as decimal(18,5)) CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL,	
		cast(ifnull(t6.CT_ACTYTD_ENDMONTH_PRD_PL, 0.0) as decimal(18,5)) CT_ACTYTD_ENDMONTH_PRD_PL,		
		cast(ifnull(t6.CT_SA_LAST_MONTH_PROD_PL, 0.0) as decimal(18,5)) CT_SA_LAST_MONTH_PROD_PL,	
		cast(ifnull(t7.CT_LROT_YTD_INSP_PL, 0.0) as decimal(18,5)) CT_LROT_YTD_INSP_PL,		
		cast(ifnull(t7.CT_LROT_LASTMONTH_INSP_PL, 0.0) as decimal(18,5)) CT_LROT_LASTMONTH_INSP_PL	
from stg_scn_plant_unique t0
		left join stg_scn_plant_sales1 t1 on t0.plantcode = t1.plantcode
		left join stg_scn_plant_sales2 t2 on t0.plantcode = t2.plantcode
		left join stg_scn_plant_sales3 t3 on t0.plantcode = t3.plantcode
		left join stg_scn_plant_purchase1 t4 on t0.plantcode = t4.plantcode
		left join stg_scn_plant_purchase2 t5 on t0.plantcode = t5.plantcode
		left join stg_scn_plant_productionsnapshot t6 on t0.plantcode = t6.plantcode
		left join stg_scn_plant_quality t7 on t0.plantcode = t7.plantcode;

drop table if exists stg_scn_plant_hash;
create table stg_scn_plant_hash as
select 	cast(UPPER(HASH_MD5(plantcode)) as varchar(32)) as plantcode_HSH,
		current_timestamp 				 				 as plantcode_LDTS,
		cast('AERA BV: SCN - Plant from Multiple SA' as varchar(50))  	 as plantcode_RSRC,
		cast(UPPER(HASH_MD5(upper(plantcode || '|^|' ||
								    ifnull(CT_OPENORDER_AMOUNT_PL, 0.0) || '|^|' ||
									ifnull(CT_PASTDUEORDER_AMOUNT_ORD_PL, 0.0) || '|^|' ||
									ifnull(CT_TOTAL_SALES_ORDERS, 0.0) || '|^|' ||
									ifnull(CT_TOTAL_SALES_ORDERS_OTIF, 0.0) || '|^|' ||
									ifnull(CT_SALES_ORDERS_OTIF_PERC, 0.0) || '|^|' ||
									ifnull(CT_SALES_ORDERS_LINES_OTIF_PERC, 0.0) || '|^|' ||
									ifnull(CT_PURCHASE_DOC_COUNT_ORD_PL, 0.0) || '|^|' ||
									ifnull(CT_DELIV_SCHED_COUNT_ORD_PL, 0.0) || '|^|' ||
									ifnull(CT_IOTIF_DOCLVL_ORD_PL, 0.0) || '|^|' ||
									ifnull(CT_IOTIF_SCHEDLVL_ORD_PL, 0.0) || '|^|' ||
									ifnull(CT_IOTIFCOUNT_DOCLVL_ORD_PL, 0.0) || '|^|' ||
									ifnull(CT_IOTIFCOUNT_SCHEDLVL_ORD_PL, 0.0) || '|^|' ||
									ifnull(CT_PURCHASE_DOC_COUNT_SUPPLY_PL, 0.0) || '|^|' ||
									ifnull(CT_DELIV_SCHED_COUNT_SUPPLY_PL, 0.0) || '|^|' ||
									ifnull(CT_IOTIF_DOCLVL_SUPPLY_PL, 0.0) || '|^|' ||
									ifnull(CT_IOTIF_SCHEDLVL_SUPPLY_PL, 0.0) || '|^|' ||
									ifnull(CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL, 0.0) || '|^|' ||
									ifnull(CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL, 0.0) || '|^|' ||
									ifnull(CT_ACTYTD_ENDMONTH_PRD_PL, 0.0) || '|^|' ||
									ifnull(CT_SA_LAST_MONTH_PROD_PL, 0.0) || '|^|' ||
									ifnull(CT_LROT_YTD_INSP_PL, 0.0) || '|^|' ||
									ifnull(CT_LROT_LASTMONTH_INSP_PL, 0.0)
				)))as varchar(32)) as plantcode_SCN_HSH_DIFF,
		PLANTCODE,
		CT_OPENORDER_AMOUNT_PL,
		CT_PASTDUEORDER_AMOUNT_ORD_PL,
		CT_TOTAL_SALES_ORDERS,
		CT_TOTAL_SALES_ORDERS_OTIF,
		CT_SALES_ORDERS_OTIF_PERC,
		CT_SALES_ORDERS_LINES_OTIF_PERC,
		CT_PURCHASE_DOC_COUNT_ORD_PL,
		CT_DELIV_SCHED_COUNT_ORD_PL,
		CT_IOTIF_DOCLVL_ORD_PL,
		CT_IOTIF_SCHEDLVL_ORD_PL,
		CT_IOTIFCOUNT_DOCLVL_ORD_PL,
		CT_IOTIFCOUNT_SCHEDLVL_ORD_PL,
		CT_PURCHASE_DOC_COUNT_SUPPLY_PL,
		CT_DELIV_SCHED_COUNT_SUPPLY_PL,
		CT_IOTIF_DOCLVL_SUPPLY_PL,
		CT_IOTIF_SCHEDLVL_SUPPLY_PL,
		CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL,
		CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL,
		CT_ACTYTD_ENDMONTH_PRD_PL,
		CT_SA_LAST_MONTH_PROD_PL,
		CT_LROT_YTD_INSP_PL,
		CT_LROT_LASTMONTH_INSP_PL
from stg_scn_plant;

/* ********************* END: Unifying measures for Plant ********************* */

/* ********************* START: Material SCN measures Plant ********************* */

/* START: As we have cases where a Plant does not come from T001W (master) table, we need to insert missing business key in HUB */
	insert into hub_plant (T001W_WERKS_HSH, T001W_WERKS_LDTS, T001W_WERKS_RSRC, T001W_WERKS)
	select DISTINCT st1.PLANTCODE_HSH,
	       st1.PLANTCODE_LDTS,
	       st1.PLANTCODE_RSRC,
	       st1.PLANTCODE
	from stg_scn_plant_hash as st1
	where st1.PLANTCODE_HSH not in (
                                   	 select T001W_WERKS_HSH
                                   	 from hub_plant
                                    );
/* END: Plant hub enrich */

	/* insert into sat dummy record in order to avoid left joins in future */
	insert into sat_plant_scn (PLANTCODE_HSH, PLANTCODE_LDTS, PLANTCODE_LEDTS, PLANTCODE_RSRC, PLANTCODE_SCN_HSH_DIFF, 
	CT_OPENORDER_AMOUNT_PL,	CT_PASTDUEORDER_AMOUNT_ORD_PL, CT_TOTAL_SALES_ORDERS, CT_TOTAL_SALES_ORDERS_OTIF, CT_SALES_ORDERS_OTIF_PERC, CT_SALES_ORDERS_LINES_OTIF_PERC,
	CT_PURCHASE_DOC_COUNT_ORD_PL, CT_DELIV_SCHED_COUNT_ORD_PL, CT_IOTIF_DOCLVL_ORD_PL, CT_IOTIF_SCHEDLVL_ORD_PL, CT_IOTIFCOUNT_DOCLVL_ORD_PL, CT_IOTIFCOUNT_SCHEDLVL_ORD_PL,
	CT_PURCHASE_DOC_COUNT_SUPPLY_PL, CT_DELIV_SCHED_COUNT_SUPPLY_PL, CT_IOTIF_DOCLVL_SUPPLY_PL, CT_IOTIF_SCHEDLVL_SUPPLY_PL, CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL,
	CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL, CT_ACTYTD_ENDMONTH_PRD_PL, CT_SA_LAST_MONTH_PROD_PL, CT_LROT_YTD_INSP_PL, CT_LROT_LASTMONTH_INSP_PL)
	select  '00000000000000000000000000000000' as PLANTCODE_HSH,
			'0001-01-01' as PLANTCODE_LDTS,
			'9999-12-31' as PLANTCODE_LEDTS,
			'SYSTEM'	 as PLANTCODE_RSRC,
			'00000000000000000000000000000000' as PLANTCODE_SCN_HSH_DIFF,
			0.00 as CT_OPENORDER_AMOUNT_PL,
			0.00 as CT_PASTDUEORDER_AMOUNT_ORD_PL,
			0.00 as CT_TOTAL_SALES_ORDERS,
			0.00 as CT_TOTAL_SALES_ORDERS_OTIF,
			0.00 as CT_SALES_ORDERS_OTIF_PERC,
			0.00 as CT_SALES_ORDERS_LINES_OTIF_PERC,
			0.00 as CT_PURCHASE_DOC_COUNT_ORD_PL,
			0.00 as CT_DELIV_SCHED_COUNT_ORD_PL,
			0.00 as CT_IOTIF_DOCLVL_ORD_PL,
			0.00 as CT_IOTIF_SCHEDLVL_ORD_PL,
			0.00 as CT_IOTIFCOUNT_DOCLVL_ORD_PL,
			0.00 as CT_IOTIFCOUNT_SCHEDLVL_ORD_PL,
			0.00 as CT_PURCHASE_DOC_COUNT_SUPPLY_PL,
			0.00 as CT_DELIV_SCHED_COUNT_SUPPLY_PL,
			0.00 as CT_IOTIF_DOCLVL_SUPPLY_PL,
			0.00 as CT_IOTIF_SCHEDLVL_SUPPLY_PL,
			0.00 as CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL,
			0.00 as CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL,
			0.00 as CT_ACTYTD_ENDMONTH_PRD_PL,
			0.00 as CT_SA_LAST_MONTH_PROD_PL,
			0.00 as CT_LROT_YTD_INSP_PL,
			0.00 as CT_LROT_LASTMONTH_INSP_PL
	from dual
	where not exists (select 1 from sat_plant_scn where plantcode_HSH = '00000000000000000000000000000000');

	/* insert new records and changed ones */
	insert into sat_plant_scn (PLANTCODE_HSH, PLANTCODE_LDTS, PLANTCODE_RSRC, PLANTCODE_SCN_HSH_DIFF, 
							   CT_OPENORDER_AMOUNT_PL,	CT_PASTDUEORDER_AMOUNT_ORD_PL, CT_TOTAL_SALES_ORDERS, CT_TOTAL_SALES_ORDERS_OTIF, CT_SALES_ORDERS_OTIF_PERC, CT_SALES_ORDERS_LINES_OTIF_PERC,
	CT_PURCHASE_DOC_COUNT_ORD_PL, CT_DELIV_SCHED_COUNT_ORD_PL, CT_IOTIF_DOCLVL_ORD_PL, CT_IOTIF_SCHEDLVL_ORD_PL, CT_IOTIFCOUNT_DOCLVL_ORD_PL, CT_IOTIFCOUNT_SCHEDLVL_ORD_PL,
	CT_PURCHASE_DOC_COUNT_SUPPLY_PL, CT_DELIV_SCHED_COUNT_SUPPLY_PL, CT_IOTIF_DOCLVL_SUPPLY_PL, CT_IOTIF_SCHEDLVL_SUPPLY_PL, CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL,
	CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL, CT_ACTYTD_ENDMONTH_PRD_PL, CT_SA_LAST_MONTH_PROD_PL, CT_LROT_YTD_INSP_PL, CT_LROT_LASTMONTH_INSP_PL)
	select 	st1.PLANTCODE_HSH, st1.PLANTCODE_LDTS, st1.PLANTCODE_RSRC, st1.PLANTCODE_SCN_HSH_DIFF, 
			st1.CT_OPENORDER_AMOUNT_PL, st1.CT_PASTDUEORDER_AMOUNT_ORD_PL, st1.CT_TOTAL_SALES_ORDERS, st1.CT_TOTAL_SALES_ORDERS_OTIF, st1.CT_SALES_ORDERS_OTIF_PERC, st1.CT_SALES_ORDERS_LINES_OTIF_PERC,
			st1.CT_PURCHASE_DOC_COUNT_ORD_PL, st1.CT_DELIV_SCHED_COUNT_ORD_PL, st1.CT_IOTIF_DOCLVL_ORD_PL, st1.CT_IOTIF_SCHEDLVL_ORD_PL, st1.CT_IOTIFCOUNT_DOCLVL_ORD_PL, st1.CT_IOTIFCOUNT_SCHEDLVL_ORD_PL,
			st1.CT_PURCHASE_DOC_COUNT_SUPPLY_PL, st1.CT_DELIV_SCHED_COUNT_SUPPLY_PL, st1.CT_IOTIF_DOCLVL_SUPPLY_PL, st1.CT_IOTIF_SCHEDLVL_SUPPLY_PL, st1.CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL, st1.CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL,
			st1.CT_ACTYTD_ENDMONTH_PRD_PL, st1.CT_SA_LAST_MONTH_PROD_PL, st1.CT_LROT_YTD_INSP_PL, st1.CT_LROT_LASTMONTH_INSP_PL
	from stg_scn_plant_hash as st1
	      left outer join sat_plant_scn as s1
	                   on     st1.PLANTCODE_HSH = s1.PLANTCODE_HSH
	                      and s1.PLANTCODE_LEDTS is NULL
	where (
	        s1.PLANTCODE_HSH is null OR
	        (
	              st1.PLANTCODE_SCN_HSH_DIFF != s1.PLANTCODE_SCN_HSH_DIFF
	          AND s1.PLANTCODE_LDTS = (
                                        select max(z.PLANTCODE_LDTS)
                                        from sat_plant_scn z
                                        where s1.PLANTCODE_HSH = z.PLANTCODE_HSH
                                       )
	        )
	      );
          
    /* update Load End Date */

	/* create table upd_sat_end_date_loop_s301
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s301;
	insert into upd_sat_end_date_loop_s301 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.PLANTCODE_HSH,
	       s1.PLANTCODE_LDTS
	from sat_plant_scn as s1
	where (
	            s1.PLANTCODE_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.PLANTCODE_HSH)
	                     from sat_plant_scn as y
	                     where   y.PLANTCODE_HSH = s1.PLANTCODE_HSH
	                         AND y.PLANTCODE_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s301
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s301;
	insert into upd_sat_end_date_cross_s301 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s301 t1
					inner join upd_sat_end_date_loop_s301 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_plant_scn dst
	   set dst.PLANTCODE_LEDTS = src.BUSKEY_LEDTS
	  from sat_plant_scn dst
				inner join upd_sat_end_date_cross_s301 src on dst.PLANTCODE_HSH = src.BUSKEY_HSH
	      												and dst.PLANTCODE_LDTS = src.BUSKEY_LDTS;	
          