/* Build SCN for Material-Customer */
		create or replace view fact_scn_material_customer as
		select 	t.PARTNUMBER_CUSTOMER_HSH 	as fact_scn_material_customerid,
				t.PARTNUMBER_CUSTOMER_RSRC 	as dd_scn_material_customer_recordsource,
				t.PARTNUMBER_HSH 			as dim_material__xbiid,
				t.CUSTOMER_HSH 				as dim_customer__xbiid,
				CT_OPENORDER_AMOUNT,
				CT_YTDORDER_AMOUNT,
				CT_PASTDUEORDER_AMOUNT_ORD
		from link_material_customer t
				inner join sat_material_customer_scn x on t.PARTNUMBER_CUSTOMER_HSH = x.PARTNUMBER_CUSTOMER_HSH
		where x.PARTNUMBER_CUSTOMER_LEDTS is null;

		/* will use PRODUCT_HSH to join to dim_material__xbi dimension 
			and  CUSTOMER_HSH to join to dim_customer__xbi dimension */
