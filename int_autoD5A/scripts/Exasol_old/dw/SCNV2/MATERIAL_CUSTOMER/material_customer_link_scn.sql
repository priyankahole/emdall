/* Staging tables preparation */

	/* ********************* START: Material-Customer ********************* */

			-- 1 source Sales1
			drop table if exists stg_scn_material_customer_sales1;
			create table stg_scn_material_customer_sales1 as
			select  prt.partnumber as partnumber, dcus.CUSTOMERNUMBER,
					ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
			                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
			                                  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
			                              END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                   END) * f_so.amt_ExchangeRate_GBL), 2) ct_OpenOrder_Amount
			from fact_salesorder AS f_so
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
			            							AND (   soegidt.DateValue BETWEEN (trunc(current_date) - INTERVAL '1' YEAR) AND trunc(current_date)
			                 							 OR soegidt.DateValue BETWEEN trunc(current_date,'YEAR') AND trunc(current_date)
														 OR soegidt.DateValue BETWEEN current_date AND (current_date + 364))
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
			            										   AND (lower(sorr.RejectReasonCode) = lower('Not Set'))
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
															 AND ((CASE WHEN sdt.DocumentType = 'AF' THEN 'IN'
			                      										WHEN sdt.DocumentType = 'AG' THEN 'QT'
													                    WHEN sdt.DocumentType = 'AU' THEN 'SI'
													                    WHEN sdt.DocumentType = 'G2' THEN 'CR'
													                    WHEN sdt.DocumentType = 'KL' THEN 'FD'
													                    WHEN sdt.DocumentType = 'KM' THEN 'CQ'
													                    WHEN sdt.DocumentType = 'KN' THEN 'SD'
													                    WHEN sdt.DocumentType = 'L2' THEN 'DR'
													                    WHEN sdt.DocumentType = 'LP' THEN 'DS'
													                    WHEN sdt.DocumentType = 'TA' THEN 'OR'
													                    ELSE sdt.DocumentType END) NOT IN (('KA'),('KB')))
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
			            								   AND (lower(dc.DocumentCategory) = lower('C'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
			            									   AND (lower(dch.DistributionChannelCode) != lower('20'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
			            										 AND (lower(sois.OverallProcessingStatus) != lower('Completely processed'))
																 AND (lower(sois.OverallDeliveryStatus) != lower('Completely processed'))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
					INNER JOIN DIM_CUSTOMER as dcus on f_so.DIM_CUSTOMERID = dcus.DIM_CUSTOMERID
			where ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
						 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
								THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                               ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			                        WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			             ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END > 0))
			group by prt.partnumber, dcus.CUSTOMERNUMBER;

			-- 2 source Sales2
			drop table if exists stg_scn_material_customer_sales2;
			create table stg_scn_material_customer_sales2 as
			SELECT 	prt.partnumber, dcus.CUSTOMERNUMBER,
					ROUND(SUM(((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal)
			                         ELSE f_so.amt_ScheduleTotal END)) * f_so.amt_ExchangeRate_GBL),2) 	as ct_YTDOrder_Amount,
					ROUND(SUM(ct_scheduleqtybaseuomiru),2) 												as ct_Order_Quantity_IRUUnits,
					ROUND(SUM((f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)) * f_so.amt_ExchangeRate_GBL),2) as ct_Shipped_Amount
			FROM fact_salesorder AS f_so
					INNER JOIN Dim_Date AS ocdt ON f_so.Dim_DateIdSOCreated = ocdt.Dim_Dateid
												 AND (ocdt.DateValue BETWEEN trunc(current_date,'YEAR') AND trunc(current_date))
					INNER JOIN dim_salesmisc AS dsi ON f_so.Dim_SalesMiscId = dsi.dim_salesmiscid
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
					INNER JOIN DIM_CUSTOMER as dcus on f_so.DIM_CUSTOMERID = dcus.DIM_CUSTOMERID
			group by prt.partnumber, dcus.CUSTOMERNUMBER;

			-- 3 source Sales3
			drop table if exists stg_scn_material_customer_sales3;
			create table stg_scn_material_customer_sales3 as
			select	prt.partnumber, dcus.CUSTOMERNUMBER,
					ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                        WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
			                        WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
											  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                        ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                   ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) 
										  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                   END) * f_so.amt_ExchangeRate_GBL),0) ct_PastDueOrder_Amount_ORD
			from fact_salesorder AS f_so
					INNER JOIN Dim_Date AS sdrdp ON f_so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid
												  AND ((sdrdp.DateValue) BETWEEN    (add_months(trunc(current_date - INTERVAL '9' MONTH, 'MM'), 1)-1 + INTERVAL '1' DAY) 
												  								AND (add_months(trunc(current_date, 'MM'), 1)-1))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ItemSubType_Merck) = lower('FPP'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
															   AND (lower(dch.DistributionChannelName) != lower('Intercompany sales'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
					INNER JOIN dim_overallstatusforcreditcheck AS oscc ON f_so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckid
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
					INNER JOIN dim_salesorderitemcategory AS ic ON f_so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
					INNER JOIN DIM_CUSTOMER as dcus on f_so.DIM_CUSTOMERID = dcus.DIM_CUSTOMERID
			WHERE (lower((CASE WHEN dc.DocumentCategory ='C'
			                          AND sdt.DocumentType NOT IN ('KA','KB')
			                          AND oscc.OverallStatusforCreditCheck <> 'B'
			                          AND sois.OverallProcessingStatus <> 'Completely processed'
			                          AND sois.OverallDeliveryStatus <>'Completely processed'
			                          AND sorr.RejectReasonCode = 'Not Set'
			                          AND soegidt.DateValue < CURRENT_DATE THEN 'X'
			                       ELSE 'Not Set' END)) = lower('X'))
			      AND ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                 WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
							 WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') 
								 THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			                 ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                            ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END) END >= 0.1))
			group by prt.partnumber, dcus.CUSTOMERNUMBER;
			
			-- 4 source Sales4 (coming from customer-material tab)
			DROP TABLE IF EXISTS stg_scn_material_customer_sales4;
			CREATE TABLE stg_scn_material_customer_sales4 AS
			SELECT cust.customernumber,
			       prt.partnumber,
			       ROUND(((COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' AND f_so.dd_solineotif_merck = 'Y' 
											   THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo) ELSE 'Not Set' END) 
										- CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' AND f_so.dd_solineotif_merck = 'Y' 
																	  THEN 0.0000 ELSE 1.0000 END) > 0.0000 THEN 1.0000
			                                   ELSE 0.0000 END) 
										/ (CASE WHEN (COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo)
			                                                              ELSE 'Not Set' END) 
																   - CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
			                                                                                     ELSE 1.0000 END) > 0.0000 THEN 1.0000
			                                                              ELSE 0.0000 END) = 0.0000 THEN 1.0000
			                                    ELSE (COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo)
			                                                              ELSE 'Not Set' END)
												 	- CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
			                                                                      ELSE 1.0000 END) > 0.0000 THEN 1.0000
			                                               ELSE 0.0000 END) END)) * 100, 2) ct_so_lines_perc_cust_part
			FROM fact_salesorder AS f_so
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
													AND (soegidt.DateValue BETWEEN to_date(CONCAT(YEAR(trunc(CURRENT_DATE)),'-01-01'),'YYYY-MM-DD') AND trunc(CURRENT_DATE))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ProductHierarchy) = lower('Not Set'))
					INNER JOIN Dim_Date AS sdd ON f_so.Dim_DateidSchedDelivery = sdd.Dim_Dateid
					INNER JOIN dim_customer AS cust ON f_so.dim_customerid = cust.dim_customerid
			WHERE 	(lower(f_so.dd_ItemRelForDelv) = lower('X'))
			  	AND (lower(f_so.dd_otifenabledflag) = lower('Y'))
			GROUP BY cust.customernumber, prt.partnumber;

	/* ********************* END: Material-Customer ********************* */

-- >>

	/* ********************* START: Unifying measures for Material-Customer ********************* */
			
			drop table if exists stg_scn_material_customer_unique;
			create table stg_scn_material_customer_unique as
			select PARTNUMBER, CUSTOMERNUMBER from stg_scn_material_customer_sales1
			union
			select PARTNUMBER, CUSTOMERNUMBER from stg_scn_material_customer_sales2
			union
			select PARTNUMBER, CUSTOMERNUMBER from stg_scn_material_customer_sales3
			union
			select PARTNUMBER, CUSTOMERNUMBER from stg_scn_material_customer_sales4;

			drop table if exists stg_scn_material_customer;
			create table stg_scn_material_customer as
			select  t0.PARTNUMBER, t0.CUSTOMERNUMBER,
					cast(ifnull(t1.CT_OPENORDER_AMOUNT, 0.00) as decimal(18,5)) 		as CT_OPENORDER_AMOUNT,
					cast(ifnull(t2.CT_YTDORDER_AMOUNT, 0.00) as decimal(18,5)) 			as CT_YTDORDER_AMOUNT,
					cast(ifnull(t3.CT_PASTDUEORDER_AMOUNT_ORD, 0.00) as decimal(18,5)) 	as CT_PASTDUEORDER_AMOUNT_ORD,
					cast(ifnull(t4.CT_SO_LINES_PERC_CUST_PART, 0.00) as decimal(18,5)) 	as CT_SO_LINES_PERC_CUST_PART
			from stg_scn_material_customer_unique t0
					left join stg_scn_material_customer_sales1 t1 on t0.PARTNUMBER = t1.PARTNUMBER and t0.CUSTOMERNUMBER = t1.CUSTOMERNUMBER
					left join stg_scn_material_customer_sales2 t2 on t0.PARTNUMBER = t2.PARTNUMBER and t0.CUSTOMERNUMBER = t2.CUSTOMERNUMBER
					left join stg_scn_material_customer_sales3 t3 on t0.PARTNUMBER = t3.PARTNUMBER and t0.CUSTOMERNUMBER = t3.CUSTOMERNUMBER
					left join stg_scn_material_customer_sales4 t4 on t0.PARTNUMBER = t4.PARTNUMBER and t0.CUSTOMERNUMBER = t4.CUSTOMERNUMBER;

			drop table if exists stg_scn_material_customer_hash;
			create table stg_scn_material_customer_hash as
			select 	cast(UPPER(HASH_MD5(PARTNUMBER || '|^|' || CUSTOMERNUMBER)) as varchar(32)) as PARTNUMBER_CUSTOMER_HSH,
					cast(UPPER(HASH_MD5(PARTNUMBER)) as varchar(32)) 							as PARTNUMBER_HSH,
					cast(UPPER(HASH_MD5(CUSTOMERNUMBER)) as varchar(32)) 						as CUSTOMER_HSH,
					current_timestamp 				 				 							as PARTNUMBER_CUSTOMER_LDTS,
					cast('AERA BV: Material-Customer Sales' as varchar(50))  	 				as PARTNUMBER_CUSTOMER_RSRC,
					cast(UPPER(HASH_MD5(upper(PARTNUMBER 								|| '|^|' ||
											  CUSTOMERNUMBER							|| '|^|' ||
											  IFNULL(CT_OPENORDER_AMOUNT, 0.0) 			|| '|^|' ||
											  IFNULL(CT_YTDORDER_AMOUNT, 0.0) 			|| '|^|' ||
											  IFNULL(CT_PASTDUEORDER_AMOUNT_ORD, 0.0) 	|| '|^|' ||
											  IFNULL(CT_SO_LINES_PERC_CUST_PART, 0.0)
							)))as varchar(32)) 						 							as PARTNUMBER_CUSTOMER_SCN_HSH_DIFF,
					PARTNUMBER,
					CUSTOMERNUMBER,
					CT_OPENORDER_AMOUNT,
					CT_YTDORDER_AMOUNT,
					CT_PASTDUEORDER_AMOUNT_ORD,
					CT_SO_LINES_PERC_CUST_PART
			from stg_scn_material_customer;

	/* ********************* END: Unifying measures for Material-Customer ********************* */

-- >>

/* ********************* START: Material-Customer SCN measures populating ********************* */

	/* LINK Material-Customer table populating */

	/* drop table if exists link_material_customer
	   create table link_material_customer ( PARTNUMBER_CUSTOMER_HSH varchar(32),
								   			 PARTNUMBER_CUSTOMER_LDTS timestamp,
								   			 PARTNUMBER_CUSTOMER_RSRC varchar(50),
								   			 PARTNUMBER_HSH varchar(32),
											 CUSTOMER_HSH 	varchar(32)
								  ) */

	insert into link_material_customer (PARTNUMBER_CUSTOMER_HSH, PARTNUMBER_CUSTOMER_LDTS, PARTNUMBER_CUSTOMER_RSRC, PARTNUMBER_HSH, CUSTOMER_HSH)
	select distinct st1.PARTNUMBER_CUSTOMER_HSH,
	       st1.PARTNUMBER_CUSTOMER_LDTS,
	       st1.PARTNUMBER_CUSTOMER_RSRC,
	       st1.PARTNUMBER_HSH,
		   st1.CUSTOMER_HSH
	from stg_scn_material_customer_hash as st1
	where not exists (
                      select 1
                      from link_material_customer l1
					  where     st1.PARTNUMBER_HSH = l1.PARTNUMBER_HSH
							and st1.CUSTOMER_HSH = l1.CUSTOMER_HSH
                      );


	/* SAT Material-Customer SCN table population */

	/* 	drop table if exists sat_material_customer_scn
		create table sat_material_customer_scn (
												PARTNUMBER_CUSTOMER_HSH 		varchar(32),
												PARTNUMBER_CUSTOMER_LDTS 		timestamp,
												PARTNUMBER_CUSTOMER_LEDTS 		timestamp,
												PARTNUMBER_CUSTOMER_RSRC 		varchar(50),
												PARTNUMBER_CUSTOMER_SCN_HSH_DIFF varchar(32),
												CT_OPENORDER_AMOUNT decimal(18,5),
												CT_YTDORDER_AMOUNT decimal(18,5),
												CT_PASTDUEORDER_AMOUNT_ORD decimal(18,5),
												CT_SO_LINES_PERC_CUST_PART decimal(18,5)
												) */

	/* insert into sat dummy record in order to avoid left joins in future */
	insert into sat_material_customer_scn (PARTNUMBER_CUSTOMER_HSH, PARTNUMBER_CUSTOMER_LDTS, PARTNUMBER_CUSTOMER_LEDTS, PARTNUMBER_CUSTOMER_RSRC, PARTNUMBER_CUSTOMER_SCN_HSH_DIFF,
										   CT_OPENORDER_AMOUNT, CT_YTDORDER_AMOUNT, CT_PASTDUEORDER_AMOUNT_ORD, CT_SO_LINES_PERC_CUST_PART)
	select  '00000000000000000000000000000000' as PARTNUMBER_CUSTOMER_HSH,
			'0001-01-01' as PARTNUMBER_CUSTOMER_LDTS,
			'9999-12-31' as PARTNUMBER_CUSTOMER_LEDTS,
			'SYSTEM'	 as PARTNUMBER_CUSTOMER_RSRC,
			'00000000000000000000000000000000' as PARTNUMBER_CUSTOMER_SCN_HSH_DIFF,
			0.00 as CT_OPENORDER_AMOUNT, 
			0.00 as CT_YTDORDER_AMOUNT, 
			0.00 as CT_PASTDUEORDER_AMOUNT_ORD,
			0.00 as CT_SO_LINES_PERC_CUST_PART
				from dual
	where not exists (select 1 from sat_material_customer_scn where PARTNUMBER_CUSTOMER_HSH = '00000000000000000000000000000000');

	/* insert new records and changed ones */
	insert into sat_material_customer_scn ( PARTNUMBER_CUSTOMER_HSH, PARTNUMBER_CUSTOMER_LDTS, PARTNUMBER_CUSTOMER_RSRC, PARTNUMBER_CUSTOMER_SCN_HSH_DIFF,
											CT_OPENORDER_AMOUNT, CT_YTDORDER_AMOUNT, CT_PASTDUEORDER_AMOUNT_ORD, CT_SO_LINES_PERC_CUST_PART)
	select 	st1.PARTNUMBER_CUSTOMER_HSH, st1.PARTNUMBER_CUSTOMER_LDTS, st1.PARTNUMBER_CUSTOMER_RSRC, st1.PARTNUMBER_CUSTOMER_SCN_HSH_DIFF,
			st1.CT_OPENORDER_AMOUNT, st1.CT_YTDORDER_AMOUNT, st1.CT_PASTDUEORDER_AMOUNT_ORD, st1.CT_SO_LINES_PERC_CUST_PART
	from stg_scn_material_customer_hash as st1
	      left outer join sat_material_customer_scn as s1
	                   on     st1.PARTNUMBER_CUSTOMER_HSH = s1.PARTNUMBER_CUSTOMER_HSH
	                      and s1.PARTNUMBER_CUSTOMER_LEDTS is NULL
	where (
	        s1.PARTNUMBER_CUSTOMER_HSH is null OR
	        (
	              st1.PARTNUMBER_CUSTOMER_SCN_HSH_DIFF != s1.PARTNUMBER_CUSTOMER_SCN_HSH_DIFF
	          AND s1.PARTNUMBER_CUSTOMER_LDTS = (
                                        select max(z.PARTNUMBER_CUSTOMER_LDTS)
                                        from sat_material_customer_scn z
                                        where s1.PARTNUMBER_CUSTOMER_HSH = z.PARTNUMBER_CUSTOMER_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s102
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s102;
	insert into upd_sat_end_date_loop_s102 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.PARTNUMBER_CUSTOMER_HSH,
	       s1.PARTNUMBER_CUSTOMER_LDTS
	from sat_material_customer_scn as s1
	where (
	            s1.PARTNUMBER_CUSTOMER_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.PARTNUMBER_CUSTOMER_HSH)
	                     from sat_material_customer_scn as y
	                     where   y.PARTNUMBER_CUSTOMER_HSH = s1.PARTNUMBER_CUSTOMER_HSH
	                         AND y.PARTNUMBER_CUSTOMER_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s102
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s102;
	insert into upd_sat_end_date_cross_s102 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s102 t1
					inner join upd_sat_end_date_loop_s102 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_material_customer_scn dst
	   set dst.PARTNUMBER_CUSTOMER_LEDTS = src.BUSKEY_LEDTS
	  from sat_material_customer_scn dst
				inner join upd_sat_end_date_cross_s102 src on dst.PARTNUMBER_CUSTOMER_HSH = src.BUSKEY_HSH
	      												and dst.PARTNUMBER_CUSTOMER_LDTS = src.BUSKEY_LDTS;	