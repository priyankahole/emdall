/* Build SCN for Customer-Plant */
		create or replace view fact_scn_customer_plant as
		select 	t.CUSTOMER_PLANT_HSH 	as fact_scn_customer_plantid,
				t.CUSTOMER_PLANT_RSRC 	as dd_scn_customer_plant_recordsource,
				t.CUSTOMER_HSH 			as dim_customer__xbiid,
				t.PLANT_HSH 			as dim_plant__xbiid,
				CT_PAST_DUE_AMOUNT_CUST_PLANT,
				CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT,
				CT_SO_LINES_PERC_CUST_PLANT
		from link_customer_plant t
				inner join sat_customer_plant_scn x on t.CUSTOMER_PLANT_HSH = x.CUSTOMER_PLANT_HSH
		where x.CUSTOMER_PLANT_LEDTS is null;

		/* will use CUSTOMER_HSH to join to dim_customer__xbi dimension 
			and  Plant_HSH to join to dim_plant__xbi dimension */