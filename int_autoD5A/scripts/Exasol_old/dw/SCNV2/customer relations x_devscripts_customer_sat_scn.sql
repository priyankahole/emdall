


-- SCN - iOTIF - vendor at supply performance summary

drop table if exists stg_scn_vendor_purchase1;
create table stg_scn_vendor_purchase1 as
SELECT ven.VendorNumber ,
ROUND(100 * (1 - (COUNT(DISTINCT CASE when (f_deliv.dd_iotif_merck = 'N' OR f_deliv.dd_iotif_merck = 'Not Set') then dd_documentno ELSE NULL END) ) /
(CASE WHEN COUNT(DISTINCT dd_documentno) <> 0 THEN COUNT(DISTINCT dd_documentno) ELSE 1.000 END)),2) ct_otif_doc_level_vendor
FROM fact_purchase AS f_deliv
INNER JOIN Dim_Date AS dtst ON f_deliv.Dim_DateidStatDelivery = dtst.Dim_Dateid
AND (dtst.DateValue  BETWEEN to_date(CONCAT(YEAR(trunc(current_date)),'-01-01'),'YYYY-MM-DD') AND trunc(current_date))
INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid  AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))  AND ((prt.MRPController) NOT IN (('690'),('797'),('798')) )
INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid  AND (lower(case when atrb.ItemReturn= 'X' then 'Yes' else 'No' end) != lower('Yes'))
AND (lower(case when atrb.ItemGRIndicator= 'X' then 'Yes' else 'No' end) != lower('No'))
INNER JOIN dim_documenttype AS doctyp ON f_deliv.Dim_DocumentTypeid = doctyp.dim_documenttypeid  AND ((doctyp.Type) IN (('NB'),('LPA'),('UB')) )
INNER JOIN Dim_Plant AS plord ON f_deliv.Dim_PlantidOrdering = plord.Dim_Plantid  AND ((plord.tacticalring_merck) IN (('Large Molecule'),('Small Molecule')) )
INNER JOIN Dim_Vendor AS ven ON f_deliv.Dim_Vendorid = ven.Dim_Vendorid  INNER JOIN Dim_Plant AS plsup ON f_deliv.Dim_PlantidSupplying = plsup.Dim_Plantid
WHERE (lower(f_deliv.dd_ourreference) = lower('Not Set'))  AND (lower(f_deliv.dd_intraplantflag_merck) = lower('N'))
AND (lower(f_deliv.dd_lpa_merck) = lower('Not Set'))
group by ven.VendorNumber
;



















