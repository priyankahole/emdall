/* ---------------------------------------------------------------------------- */

/* SAT Customer Text table population */

	/* drop table if exists sat_customer_all
	create table sat_customer_all (
			KNA1_KUNNR_HSH varchar(32),
			KNA1_KUNNR_LDTS timestamp,
			KNA1_KUNNR_LEDTS timestamp,
			KNA1_KUNNR_RSRC varchar(50),
			KNA1_KUNNR_TEXT_HSH_DIFF varchar(32),
			KNA1_NAME1 VARCHAR(100),
			KNA1_WERKS VARCHAR(4),
			KNA1_LAND1 VARCHAR(3),
			KNA1_XCPDK VARCHAR(1),
			KNA1_ORT01 VARCHAR(35),
			KNA1_PSTLZ VARCHAR(10),
			KNA1_REGIO VARCHAR(3),
			KNA1_BRSCH VARCHAR(3),
			KNA1_KUKLA VARCHAR(2),
			KNA1_DEAR2 VARCHAR(1),
			KNA1_ERDAT date ,
			KNA1_KTOKD VARCHAR(4),
			KNA1_NAME2 VARCHAR(100),
			KNA1_VBUND VARCHAR(6),
			KNA1_KATR1 VARCHAR(2),
			KNA1_KATR2 VARCHAR(2),
			KNA1_KATR3 VARCHAR(2),
			KNA1_KATR4 VARCHAR(2),
			KNA1_KATR5 VARCHAR(2),
			KNA1_KATR6 VARCHAR(3),
			KNA1_KATR7 VARCHAR(3),
			KNA1_KATR8 VARCHAR(3),
			KNA1_KDKG1 VARCHAR(2),
			KNA1_KDKG2 VARCHAR(2),
			KNA1_BRAN1 VARCHAR(10),
			KNA1_FAKSD VARCHAR(2),
			KNA1_DATLT VARCHAR(14),
			KNA1_LIFSD VARCHAR(2),
			KNA1_LOEVM VARCHAR(1),
			KNA1_TELF1 VARCHAR(16),
			KNA1_TELFX VARCHAR(31),
			KNA1_AUFSD VARCHAR(2),
			KNA1_SORTL VARCHAR(10),
			KNA1_LZONE VARCHAR(10),
			KNA1_STRAS VARCHAR(35),
			KNA1_ADRNR VARCHAR(10),
			KNA1_SPRAS VARCHAR(1),
			KNA1_TXJCD VARCHAR(15)
			) */

	/* insert new records and changed ones */
	insert into sat_customer_all (KNA1_KUNNR_HSH, KNA1_KUNNR_LDTS, KNA1_KUNNR_RSRC, KNA1_KUNNR_TEXT_HSH_DIFF,
								  KNA1_NAME1, KNA1_WERKS, KNA1_LAND1, KNA1_XCPDK, KNA1_ORT01, KNA1_PSTLZ, KNA1_REGIO, KNA1_BRSCH, KNA1_KUKLA, KNA1_DEAR2, KNA1_ERDAT, KNA1_KTOKD, KNA1_NAME2,
								  KNA1_VBUND, KNA1_KATR1, KNA1_KATR2, KNA1_KATR3, KNA1_KATR4, KNA1_KATR5, KNA1_KATR6, KNA1_KATR7, KNA1_KATR8, KNA1_KDKG1, KNA1_KDKG2, KNA1_BRAN1, KNA1_FAKSD,
								  KNA1_DATLT, KNA1_LIFSD,  KNA1_LOEVM, KNA1_TELF1, KNA1_TELFX, KNA1_AUFSD, KNA1_SORTL, KNA1_LZONE, KNA1_STRAS, KNA1_ADRNR, KNA1_SPRAS, KNA1_TXJCD)
	select 	st1.KNA1_KUNNR_HSH,
			st1.KNA1_KUNNR_LDTS,
			st1.KNA1_KUNNR_RSRC,
			st1.KNA1_KUNNR_TEXT_HSH_DIFF,
			st1.KNA1_NAME1,
			st1.KNA1_WERKS,
			st1.KNA1_LAND1,
			st1.KNA1_XCPDK,
			st1.KNA1_ORT01,
			st1.KNA1_PSTLZ,
			st1.KNA1_REGIO,
			st1.KNA1_BRSCH,
			st1.KNA1_KUKLA,
			st1.KNA1_DEAR2,
			st1.KNA1_ERDAT,
			st1.KNA1_KTOKD,
			st1.KNA1_NAME2,
			st1.KNA1_VBUND,
			st1.KNA1_KATR1,
			st1.KNA1_KATR2,
			st1.KNA1_KATR3,
			st1.KNA1_KATR4,
			st1.KNA1_KATR5,
			st1.KNA1_KATR6,
			st1.KNA1_KATR7,
			st1.KNA1_KATR8,
			st1.KNA1_KDKG1,
			st1.KNA1_KDKG2,
			st1.KNA1_BRAN1,
			st1.KNA1_FAKSD,
			st1.KNA1_DATLT,
			st1.KNA1_LIFSD,
			st1.KNA1_LOEVM,
			st1.KNA1_TELF1,
			st1.KNA1_TELFX,
			st1.KNA1_AUFSD,
			st1.KNA1_SORTL,
			st1.KNA1_LZONE,
			st1.KNA1_STRAS,
			st1.KNA1_ADRNR,
			st1.KNA1_SPRAS,
			st1.KNA1_TXJCD
	from kna1__xbi_hash_stg as st1
	      left outer join sat_customer_all as s1
	                   on     st1.KNA1_KUNNR_HSH = s1.KNA1_KUNNR_HSH
	                      and s1.KNA1_KUNNR_LEDTS is NULL
	where (
	        s1.KNA1_KUNNR_HSH is null OR
	        (
	              st1.KNA1_KUNNR_TEXT_HSH_DIFF != s1.KNA1_KUNNR_TEXT_HSH_DIFF
	          AND s1.KNA1_KUNNR_LDTS = (
                                        select max(z.KNA1_KUNNR_LDTS)
                                        from sat_customer_all z
                                        where s1.KNA1_KUNNR_HSH = z.KNA1_KUNNR_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s49
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s49;
	insert into upd_sat_end_date_loop_s49 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.KNA1_KUNNR_HSH,
	       s1.KNA1_KUNNR_LDTS
	from sat_customer_all as s1
	where (
	            s1.KNA1_KUNNR_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.KNA1_KUNNR_HSH)
	                     from sat_customer_all as y
	                     where   y.KNA1_KUNNR_HSH = s1.KNA1_KUNNR_HSH
	                         AND y.KNA1_KUNNR_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s49
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s49;
	insert into upd_sat_end_date_cross_s49 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s49 t1
					inner join upd_sat_end_date_loop_s49 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_customer_all dst
	   set dst.KNA1_KUNNR_LEDTS = src.BUSKEY_LEDTS
	  from sat_customer_all dst
				inner join upd_sat_end_date_cross_s49 src on dst.KNA1_KUNNR_HSH = src.BUSKEY_HSH
	      												and dst.KNA1_KUNNR_LDTS = src.BUSKEY_LDTS;
