/* Staging table preparation */

	/* drop table if exists KNA1__xbi_hash_stg
	create table KNA1__xbi_hash_stg as */

	truncate table kna1__xbi_hash_stg;
	insert into kna1__xbi_hash_stg
	 (
		KNA1_KUNNR_HSH,
		KNA1_KUNNR_LDTS,
		KNA1_KUNNR_RSRC,
		KNA1_KUNNR_TEXT_HSH_DIFF,
		KNA1_KUNNR,
		KNA1_NAME1,
		KNA1_WERKS,
		KNA1_LAND1,
		KNA1_XCPDK,
		KNA1_ORT01,
		KNA1_PSTLZ,
		KNA1_REGIO,
		KNA1_BRSCH,
		KNA1_KUKLA,
		KNA1_DEAR2,
		KNA1_ERDAT,
		KNA1_KTOKD,
		KNA1_NAME2,
		KNA1_KATR1,
		KNA1_KATR2,
		KNA1_VBUND,
		KNA1_KATR3,
		KNA1_KATR4,
		KNA1_KATR5,
		KNA1_KATR6,
		KNA1_KATR7,
		KNA1_KATR8,
		KNA1_KDKG1,
		KNA1_KDKG2,
		KNA1_BRAN1,
		KNA1_FAKSD,
		KNA1_DATLT,
		KNA1_LIFSD,
		KNA1_LOEVM,
		KNA1_TELF1,
		KNA1_TELFX,
		KNA1_AUFSD,
		KNA1_SORTL,
		KNA1_LZONE,
		KNA1_STRAS,
		KNA1_ADRNR,
		KNA1_SPRAS,
		KNA1_TXJCD)
		select 	cast(UPPER(HASH_MD5(KNA1_KUNNR)) as varchar(32)) as KNA1_KUNNR_HSH,
		       	current_timestamp as KNA1_KUNNR_LDTS,
		       	cast('ECC: KNA1' as varchar(50)) as KNA1_KUNNR_RSRC,
				cast(UPPER(HASH_MD5(upper(KNA1_KUNNR ||'^'||
																	IFNULL(KNA1_NAME1, '') ||'^'||
																	IFNULL(KNA1_WERKS, '') ||'^'||
																	IFNULL(KNA1_LAND1, '') ||'^'||
																	IFNULL(KNA1_XCPDK, '') ||'^'||
																	IFNULL(KNA1_ORT01, '') ||'^'||
																	IFNULL(KNA1_PSTLZ, '') ||'^'||
																	IFNULL(KNA1_REGIO, '') ||'^'||
																	IFNULL(KNA1_BRSCH, '') ||'^'||
																	IFNULL(KNA1_KUKLA, '') ||'^'||
																	IFNULL(KNA1_DEAR2, '') ||'^'||
																	IFNULL(KNA1_ERDAT, '0001-01-01') ||'^'||
																	IFNULL(KNA1_KTOKD, '') ||'^'||
																	IFNULL(KNA1_NAME2, '') ||'^'||
																	IFNULL(KNA1_KATR1, '') ||'^'||
																	IFNULL(KNA1_KATR2, '') ||'^'||
																	IFNULL(KNA1_VBUND, '') ||'^'||
																	IFNULL(KNA1_KATR3, '') ||'^'||
																	IFNULL(KNA1_KATR4, '') ||'^'||
																	IFNULL(KNA1_KATR5, '') ||'^'||
																	IFNULL(KNA1_KATR6, '') ||'^'||
																	IFNULL(KNA1_KATR7, '') ||'^'||
																	IFNULL(KNA1_KATR8, '') ||'^'||
																	IFNULL(KNA1_KDKG1, '') ||'^'||
																	IFNULL(KNA1_KDKG2, '') ||'^'||
																	IFNULL(KNA1_BRAN1, '') ||'^'||
																	IFNULL(KNA1_FAKSD, '') ||'^'||
																	IFNULL(KNA1_DATLT, '') ||'^'||
																	IFNULL(KNA1_LIFSD, '') ||'^'||
																	IFNULL(KNA1_LOEVM, '') ||'^'||
																	IFNULL(KNA1_TELF1, '') ||'^'||
																	IFNULL(KNA1_TELFX, '') ||'^'||
																	IFNULL(KNA1_AUFSD, '') ||'^'||
																	IFNULL(KNA1_SORTL, '') ||'^'||
																	IFNULL(KNA1_LZONE, '') ||'^'||
																	IFNULL(KNA1_STRAS, '') ||'^'||
																	IFNULL(KNA1_ADRNR, '') ||'^'||
																	IFNULL(KNA1_SPRAS, '') ||'^'||
																	IFNULL(KNA1_TXJCD, '')
					)))as varchar(32)) as KNA1_KUNNR_TEXT_HSH_DIFF,
					KNA1_KUNNR,
					KNA1_NAME1,
					KNA1_WERKS,
					KNA1_LAND1,
					KNA1_XCPDK,
					KNA1_ORT01,
					KNA1_PSTLZ,
					KNA1_REGIO,
					KNA1_BRSCH,
					KNA1_KUKLA,
					KNA1_DEAR2,
					KNA1_ERDAT,
					KNA1_KTOKD,
					KNA1_NAME2,
					KNA1_KATR1,
					KNA1_KATR2,
					KNA1_VBUND,
					KNA1_KATR3,
					KNA1_KATR4,
					KNA1_KATR5,
					KNA1_KATR6,
					KNA1_KATR7,
					KNA1_KATR8,
					KNA1_KDKG1,
					KNA1_KDKG2,
					KNA1_BRAN1,
					KNA1_FAKSD,
					KNA1_DATLT,
					KNA1_LIFSD,
					KNA1_LOEVM,
					KNA1_TELF1,
					KNA1_TELFX,
					KNA1_AUFSD,
					KNA1_SORTL,
					KNA1_LZONE,
					KNA1_STRAS,
					KNA1_ADRNR,
					KNA1_SPRAS,
					KNA1_TXJCD
		from KNA1__XBI;
