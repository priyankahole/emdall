/* HUB Plant table populating */

	/* create table hub_plant (T001W_WERKS_HSH varchar(32),
							   T001W_WERKS_LDTS timestamp,
							   T001W_WERKS_RSRC varchar(50),
							   T001W_WERKS varchar(4)
							  ) */

	insert into hub_plant (T001W_WERKS_HSH, T001W_WERKS_LDTS, T001W_WERKS_RSRC, T001W_WERKS)
	select distinct st1.T001W_WERKS_HSH,
	       st1.T001W_WERKS_LDTS,
	       st1.T001W_WERKS_RSRC,
	       st1.T001W_WERKS
	from t001w__xbi_hash_stg as st1
	where st1.T001W_WERKS_HSH not in (
                                   	 select T001W_WERKS_HSH
                                   	 from hub_plant
                                    );
