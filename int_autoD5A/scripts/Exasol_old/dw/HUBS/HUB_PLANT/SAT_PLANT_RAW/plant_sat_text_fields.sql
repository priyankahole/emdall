/* ---------------------------------------------------------------------------- */

/* SAT Plant Text table population */

	/*
	create table sat_plant_text (
			T001W_WERKS_HSH varchar(32),
			T001W_WERKS_LDTS timestamp,
			T001W_WERKS_LEDTS timestamp,
			T001W_WERKS_RSRC varchar(50),
			T001W_WERKS_TEXT_HSH_DIFF varchar(32),
			T001W_VKORG	VARCHAR(4),
			T001W_ADRNR	VARCHAR(10),
			T001W_CHAZV	VARCHAR(1),
			T001W_FABKL	VARCHAR(2),
			T001W_KUNNR	VARCHAR(10),
			T001W_LIFNR	VARCHAR(10),
			T001W_NAME2	VARCHAR(30),
			T001W_PFACH	VARCHAR(10),
			T001W_REGIO	VARCHAR(3),
			T001W_STRAS	VARCHAR(30),
			T001W_BWKEY	VARCHAR(4),
			T001W_EKORG	VARCHAR(4),
			T001W_LAND1	VARCHAR(3),
			T001W_NAME1	VARCHAR(30),
			T001W_ORT01	VARCHAR(25),
			T001W_PSTLZ	VARCHAR(10),
			T001W_SPRAS	VARCHAR(1)
			) */

	/* insert new records and changed ones */
	insert into sat_plant_text (T001W_WERKS_HSH, T001W_WERKS_LDTS, T001W_WERKS_RSRC, T001W_WERKS_TEXT_HSH_DIFF,
								T001W_VKORG, T001W_ADRNR, T001W_CHAZV, T001W_FABKL, T001W_KUNNR, T001W_LIFNR, T001W_NAME2, T001W_PFACH, T001W_REGIO, T001W_STRAS, T001W_BWKEY, T001W_EKORG,
								T001W_LAND1, T001W_NAME1, T001W_ORT01, T001W_PSTLZ, T001W_SPRAS)
	select 	st1.T001W_WERKS_HSH,
		   	st1.T001W_WERKS_LDTS,
			st1.T001W_WERKS_RSRC,
			st1.T001W_WERKS_TEXT_HSH_DIFF,
			st1.T001W_VKORG,
			st1.T001W_ADRNR,
			st1.T001W_CHAZV,
			st1.T001W_FABKL,
			st1.T001W_KUNNR,
			st1.T001W_LIFNR,
			st1.T001W_NAME2,
			st1.T001W_PFACH,
			st1.T001W_REGIO,
			st1.T001W_STRAS,
			st1.T001W_BWKEY,
			st1.T001W_EKORG,
			st1.T001W_LAND1,
			st1.T001W_NAME1,
			st1.T001W_ORT01,
			st1.T001W_PSTLZ,
			st1.T001W_SPRAS
	from t001w__xbi_hash_stg as st1
	      left outer join sat_plant_text as s1
	                   on     st1.T001W_WERKS_HSH = s1.T001W_WERKS_HSH
	                      and s1.T001W_WERKS_LEDTS is NULL
	where (
	        s1.T001W_WERKS_HSH is null OR
	        (
	              st1.T001W_WERKS_TEXT_HSH_DIFF != s1.T001W_WERKS_TEXT_HSH_DIFF
	          AND s1.T001W_WERKS_LDTS = (
                                        select max(z.T001W_WERKS_LDTS)
                                        from sat_plant_text z
                                        where s1.T001W_WERKS_HSH = z.T001W_WERKS_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s5
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s5;
	insert into upd_sat_end_date_loop_s5 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.T001W_WERKS_HSH,
	       s1.T001W_WERKS_LDTS
	from sat_plant_text as s1
	where (
	            s1.T001W_WERKS_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.T001W_WERKS_HSH)
	                     from sat_plant_text as y
	                     where   y.T001W_WERKS_HSH = s1.T001W_WERKS_HSH
	                         AND y.T001W_WERKS_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s5
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s5;
	insert into upd_sat_end_date_cross_s5 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s5 t1
					inner join upd_sat_end_date_loop_s5 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_plant_text dst
	   set dst.T001W_WERKS_LEDTS = src.BUSKEY_LEDTS
	  from sat_plant_text dst
				inner join upd_sat_end_date_cross_s5 src on dst.T001W_WERKS_HSH = src.BUSKEY_HSH
	      												and dst.T001W_WERKS_LDTS = src.BUSKEY_LDTS;
