/* 	Reference table for AUSP_ATWRT based on CAWNT table
	SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
    /*
    DROP TABLE IF EXISTS ref_itemsubtypedescription
    CREATE TABLE ref_itemsubtypedescription
    (
        AUSP_ATWRT VARCHAR(30) -- Item Type
        ,CAWNT_SPRAS VARCHAR(1)
        ,CAWNT_ATWTB VARCHAR(30) -- Description of Item Type
    ) */


    MERGE INTO ref_itemsubtypedescription dst
    USING (SELECT CAWN_ATWRT, CAWNT_SPRAS, CAWNT_ATWTB FROM CAWNT__XBI WHERE CAWN_ATINN = 13) src
    ON ( dst.AUSP_ATWRT = src.CAWN_ATWRT and dst.CAWNT_SPRAS = src.CAWNT_SPRAS)
    WHEN MATCHED THEN
        UPDATE SET dst.CAWNT_ATWTB = src.CAWNT_ATWTB
    WHEN NOT MATCHED THEN
        INSERT (AUSP_ATWRT, CAWNT_SPRAS, CAWNT_ATWTB) VALUES (src.CAWN_ATWRT, src.CAWNT_SPRAS, src.CAWNT_ATWTB);
