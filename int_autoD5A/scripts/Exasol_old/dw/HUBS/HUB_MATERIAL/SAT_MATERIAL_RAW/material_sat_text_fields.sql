/* ---------------------------------------------------------------------------- */

/* SAT Material Text table population */

	/*
	create table sat_material_text (
			MARA_MATNR_HSH varchar(32),
			MARA_MATNR_LDTS timestamp,
			MARA_MATNR_LEDTS timestamp,
			MARA_MATNR_RSRC varchar(50),
			MARA_MATNR_TEXT_HSH_DIFF varchar(32),
			MARA_EAN11 VARCHAR(18),
			MARA_ERNAM VARCHAR(12),
			MARA_EXTWG VARCHAR(18),
			MARA_GROES VARCHAR(32),
			MARA_KZREV VARCHAR(1),
			MARA_MATKL VARCHAR(9),
			MARA_MEINS VARCHAR(3),
			MARA_MFRPN VARCHAR(40),
			MARA_MSTAV VARCHAR(2),
			MARA_MTART VARCHAR(4),
			MARA_PSTAT VARCHAR(15),
			MARA_RAUBE VARCHAR(2),
			MARA_SLED_BBD VARCHAR(1),
			MARA_STOFF VARCHAR(18),
			MARA_TRAGR VARCHAR(4),
			MARA_VPSTA VARCHAR(15),
			MARA_WRKST VARCHAR(48),
			MARA_BISMT VARCHAR(18),
			MARA_BSTME VARCHAR(3),
			MARA_EKWSL VARCHAR(4),
			MARA_GEWEI VARCHAR(3),
			MARA_IPRKZ VARCHAR(1),
			MARA_LABOR VARCHAR(3),
			MARA_LVORM VARCHAR(1),
			MARA_MFRNR VARCHAR(10),
			MARA_MSTAE VARCHAR(2),
			MARA_PRDHA VARCHAR(18),
			MARA_QMPUR VARCHAR(1),
			MARA_RDMHD VARCHAR(1),
			MARA_SPART VARCHAR(2),
			MARA_TEMPB VARCHAR(2),
			MARA_MTPOS_MARA VARCHAR(4)
			) */

	/* insert new records and changed ones */
	insert into sat_material_text
					(MARA_MATNR_HSH, MARA_MATNR_LDTS, MARA_MATNR_RSRC, MARA_MATNR_TEXT_HSH_DIFF,
					MARA_EAN11, MARA_ERNAM, MARA_EXTWG, MARA_GROES, MARA_KZREV, MARA_MATKL, MARA_MEINS, MARA_MFRPN, MARA_MSTAV, MARA_MTART,
					MARA_PSTAT, MARA_RAUBE, MARA_SLED_BBD, MARA_STOFF, MARA_TRAGR, MARA_VPSTA, MARA_WRKST, MARA_BISMT, MARA_BSTME, MARA_EKWSL,
					MARA_GEWEI, MARA_IPRKZ, MARA_LABOR, MARA_LVORM, MARA_MFRNR, MARA_MSTAE, MARA_PRDHA, MARA_QMPUR, MARA_RDMHD, MARA_SPART, MARA_TEMPB, MARA_MTPOS_MARA)
	select 	st1.MARA_MATNR_HSH,
	       	st1.MARA_MATNR_LDTS,
	       	st1.MARA_MATNR_RSRC,
	       	st1.MARA_MATNR_TEXT_HSH_DIFF,
	       	st1.MARA_EAN11,
		   	st1.MARA_ERNAM,
			st1.MARA_EXTWG,
			st1.MARA_GROES,
			st1.MARA_KZREV,
			st1.MARA_MATKL,
			st1.MARA_MEINS,
			st1.MARA_MFRPN,
			st1.MARA_MSTAV,
			st1.MARA_MTART,
		   	st1.MARA_PSTAT,
			st1.MARA_RAUBE,
			st1.MARA_SLED_BBD,
			st1.MARA_STOFF,
			st1.MARA_TRAGR,
			st1.MARA_VPSTA,
			st1.MARA_WRKST,
			st1.MARA_BISMT,
			st1.MARA_BSTME,
			st1.MARA_EKWSL,
		   	st1.MARA_GEWEI,
			st1.MARA_IPRKZ,
			st1.MARA_LABOR,
			st1.MARA_LVORM,
			st1.MARA_MFRNR,
			st1.MARA_MSTAE,
			st1.MARA_PRDHA,
			st1.MARA_QMPUR,
			st1.MARA_RDMHD,
			st1.MARA_SPART,
			st1.MARA_TEMPB,
			st1.MARA_MTPOS_MARA
	from mara__xbi_hash_stg as st1
	      left outer join sat_material_text as s1
	                   on     st1.MARA_MATNR_HSH = s1.MARA_MATNR_HSH
	                      and s1.MARA_MATNR_LEDTS is NULL
	where (
	        s1.MARA_MATNR_HSH is null OR
	        (
	              st1.MARA_MATNR_TEXT_HSH_DIFF != s1.MARA_MATNR_TEXT_HSH_DIFF
	          AND s1.MARA_MATNR_LDTS = (
                                        select max(z.MARA_MATNR_LDTS)
                                        from sat_material_text z
                                        where s1.MARA_MATNR_HSH = z.MARA_MATNR_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s3
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s3;
	insert into upd_sat_end_date_loop_s3 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.MARA_MATNR_HSH,
	       s1.MARA_MATNR_LDTS
	from sat_material_text as s1
	where (
	            s1.MARA_MATNR_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.MARA_MATNR_HSH)
	                     from sat_material_text as y
	                     where   y.MARA_MATNR_HSH = s1.MARA_MATNR_HSH
	                         AND y.MARA_MATNR_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s3
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s3;
	insert into upd_sat_end_date_cross_s3 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s3 t1
					inner join upd_sat_end_date_loop_s3 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_material_text dst
	   set dst.MARA_MATNR_LEDTS = src.BUSKEY_LEDTS
	  from sat_material_text dst
				inner join upd_sat_end_date_cross_s3 src on dst.MARA_MATNR_HSH = src.BUSKEY_HSH
	      												and dst.MARA_MATNR_LDTS = src.BUSKEY_LDTS;
