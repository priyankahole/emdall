/* 	Reference table for MARA_SPART based on TSPAT table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_division
   CREATE TABLE ref_division
   (
       MARA_SPART VARCHAR(2) -- Division
       ,TSPAT_SPRAS VARCHAR(1)
       ,TSPAT_VTEXT VARCHAR(20) -- DivisionDescription
   ) */


   MERGE INTO ref_division dst
   USING (SELECT TSPAT_SPART, TSPAT_SPRAS, TSPAT_VTEXT FROM TSPAT__XBI) src
   ON ( dst.MARA_SPART = src.TSPAT_SPART
           AND dst.TSPAT_SPRAS = src.TSPAT_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.TSPAT_VTEXT = src.TSPAT_VTEXT
   WHEN NOT MATCHED THEN INSERT (MARA_SPART, TSPAT_SPRAS, TSPAT_VTEXT) VALUES (src.TSPAT_SPART, src.TSPAT_SPRAS, src.TSPAT_VTEXT);
