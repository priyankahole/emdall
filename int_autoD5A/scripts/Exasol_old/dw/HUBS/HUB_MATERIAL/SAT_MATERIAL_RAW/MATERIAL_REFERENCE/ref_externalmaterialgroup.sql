/* 	Reference table for MARA_EXTWG based on TWEWT table 
	SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
	/*
		drop table if exists ref_externalmaterialgroup
		create table ref_externalmaterialgroup (
			MARA_EXTWG varchar(18), -- external material group code
			TWEWT_SPRAS varchar(1),
			TWEWT_EWBEZ varchar(20) -- external material group descr
			)
	*/
	
	merge into ref_externalmaterialgroup dst
	using (select TWEWT_EXTWG, TWEWT_SPRAS, TWEWT_EWBEZ  from TWEWT__XBI) src
	on (    dst.MARA_EXTWG  = src.TWEWT_EXTWG 
		and dst.TWEWT_SPRAS = src.TWEWT_SPRAS)
	WHEN MATCHED THEN update set dst.TWEWT_EWBEZ = src.TWEWT_EWBEZ
	WHEN NOT MATCHED THEN insert (MARA_EXTWG, TWEWT_SPRAS, TWEWT_EWBEZ) values (src.TWEWT_EXTWG, src.TWEWT_SPRAS, src.TWEWT_EWBEZ);
