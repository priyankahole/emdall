/* 	Reference table for MARA_MTART based on T134T table
	SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
    /*
    DROP TABLE IF EXISTS ref_parttypedescription
    CREATE TABLE ref_parttypedescription
    (
        MARA_MTART VARCHAR(4) -- Material Type
        ,T134T_SPRAS VARCHAR(1)
        ,T134T_MTBEZ VARCHAR(25) -- Description of Material Type
    ) */

    MERGE INTO ref_parttypedescription dst
    USING (SELECT T134T_MTART, T134T_SPRAS, T134T_MTBEZ FROM T134T__XBI) src
    ON ( dst.MARA_MTART = src.T134T_MTART
            AND dst.T134T_SPRAS = src.T134T_SPRAS)
    WHEN MATCHED THEN UPDATE SET dst.T134T_MTBEZ = src.T134T_MTBEZ
    WHEN NOT MATCHED THEN INSERT (MARA_MTART, T134T_SPRAS, T134T_MTBEZ) VALUES (src.T134T_MTART, src.T134T_SPRAS, src.T134T_MTBEZ);
