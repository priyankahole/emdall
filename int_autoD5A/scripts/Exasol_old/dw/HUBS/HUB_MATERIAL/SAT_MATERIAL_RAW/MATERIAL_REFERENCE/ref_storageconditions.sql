 /* 	Reference table for MARA_RAUBE based on T142T table
	SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
    /*
    DROP TABLE IF EXISTS ref_storageconditions
    CREATE TABLE ref_storageconditions
    (
        MARA_RAUBE VARCHAR(2) -- Storage Conditions
        ,T142T_SPRAS VARCHAR(1)
        ,T142T_RBTXT VARCHAR(20) -- Description of storage condition(s)
    ) */

    MERGE INTO ref_storageconditions dst
    USING (SELECT T142T_RAUBE, T142T_SPRAS, T142T_RBTXT FROM T142T__XBI) src
    ON ( dst.MARA_RAUBE = src.T142T_RAUBE
            AND dst.T142T_SPRAS = src.T142T_SPRAS)
    WHEN MATCHED THEN UPDATE SET dst.T142T_RBTXT = src.T142T_RBTXT
    WHEN NOT MATCHED THEN INSERT (MARA_RAUBE, T142T_SPRAS, T142T_RBTXT) VALUES (src.T142T_RAUBE, src.T142T_SPRAS, src.T142T_RBTXT);
