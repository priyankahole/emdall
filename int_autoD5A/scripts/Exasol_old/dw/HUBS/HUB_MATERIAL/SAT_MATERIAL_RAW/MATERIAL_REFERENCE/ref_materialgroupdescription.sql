/* 	Reference table for MARA_MATKL based on T023T table
	SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
    /*
    DROP TABLE IF EXISTS ref_materialgroupdescription
    CREATE TABLE ref_materialgroupdescription
    (
        MARA_MATKL VARCHAR(9) -- Material Group
        ,T023T_SPRAS VARCHAR(1)
        ,T023T_WGBEZ VARCHAR(20) -- Material Group Description
    	,T023T_WGBEZ60 VARCHAR(60) -- Long text describing the material group
    ) */

    MERGE INTO ref_materialgroupdescription dst
    USING (SELECT T023T_MATKL, T023T_SPRAS, T023T_WGBEZ, T023T_WGBEZ60 FROM T023T__XBI) src
    ON ( dst.MARA_MATKL = src.T023T_MATKL
            AND dst.T023T_SPRAS = src.T023T_SPRAS)
    WHEN MATCHED THEN UPDATE SET dst.T023T_WGBEZ = src.T023T_WGBEZ
    WHEN NOT MATCHED THEN INSERT (MARA_MATKL,T023T_SPRAS,T023T_WGBEZ, T023T_WGBEZ60) VALUES (src.T023T_MATKL, src.T023T_SPRAS, src.T023T_WGBEZ, T023T_WGBEZ60);
