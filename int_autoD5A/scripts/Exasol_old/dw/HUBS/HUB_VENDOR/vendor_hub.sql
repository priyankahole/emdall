/* HUB Vendor table populating */

	/* create table hub_vendor (LFA1_LIFNR_HSH varchar(32),
							   LFA1_LIFNR_LDTS timestamp,
							   LFA1_LIFNR_RSRC varchar(50),
							   LFA1_LIFNR varchar(10)
							  ) */

	insert into hub_vendor (LFA1_LIFNR_HSH, LFA1_LIFNR_LDTS, LFA1_LIFNR_RSRC, LFA1_LIFNR)
	select DISTINCT st1.LFA1_LIFNR_HSH,
	       st1.LFA1_LIFNR_LDTS,
	       st1.LFA1_LIFNR_RSRC,
	       st1.LFA1_LIFNR
	from lfa1__xbi_hash_stg as st1
	where st1.LFA1_LIFNR_HSH not in (
                                   	 select LFA1_LIFNR_HSH
                                   	 from hub_vendor
                                    );