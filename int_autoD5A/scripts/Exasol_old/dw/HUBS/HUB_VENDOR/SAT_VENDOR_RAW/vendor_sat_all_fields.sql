/* ---------------------------------------------------------------------------- */

/* SAT Vendor ALL table population */

	/*
	create table sat_vendor_all (
			LFA1_LIFNR_HSH varchar(32),
			LFA1_LIFNR_LDTS timestamp,
			LFA1_LIFNR_LEDTS timestamp,
			LFA1_LIFNR_RSRC varchar(50),
			LFA1_LIFNR_ALL_HSH_DIFF  varchar(32),
			LFA1_ADRNR VARCHAR(256),
            LFA1_NAME1 VARCHAR(35),
            LFA1_STRAS VARCHAR(256),
            LFA1_ORT01 VARCHAR(256),
            LFA1_REGIO VARCHAR(256),
            LFA1_PSTLZ VARCHAR(256),
            LFA1_LAND1 VARCHAR(3),
            LFA1_TELF1 VARCHAR(256),
            LFA1_TELFX VARCHAR(256),
            LFA1_PFACH VARCHAR(256),
            LFA1_ORT02 VARCHAR(256),
            LFA1_PSTL2 VARCHAR(256),
            LFA1_TELF2 VARCHAR(256),
            LFA1_SPRAS VARCHAR(256),
            LFA1_SORTL VARCHAR(256),
            LFA1_SPERM VARCHAR(256),
            LFA1_SPERZ VARCHAR(50),
            LFA1_KTOKK VARCHAR(4),
            LFA1_BRSCH VARCHAR(4),
            LFA1_LOEVM VARCHAR(1),
            LFA1_SPERR VARCHAR(1),
            LFA1_XCPDK VARCHAR(1),
            LFA1_QSSYS VARCHAR(4),
            LFA1_VBUND VARCHAR(6),
            LFA1_KONZS VARCHAR(10),
            LFA1_EMNFR VARCHAR(10),
            LFA1_ERDAT DATE,
            LFA1_ERNAM VARCHAR(12),
            LFA1_STCD1 VARCHAR(16),
            LFA1_STCD2 VARCHAR(11),
            LFA1_KUNNR VARCHAR(10),
            LFA1_STCEG VARCHAR(50)
			) */

	/* insert new records and changed ones */
	insert into sat_vendor_all
					(LFA1_LIFNR_HSH,
                     LFA1_LIFNR_LDTS,
                     LFA1_LIFNR_RSRC,
                     LFA1_LIFNR_ALL_HSH_DIFF,
                     LFA1_ADRNR,
                     LFA1_NAME1,
                     LFA1_STRAS,
                     LFA1_ORT01,
                     LFA1_REGIO,
                     LFA1_PSTLZ,
                     LFA1_LAND1,
                     LFA1_TELF1,
                     LFA1_TELFX,
                     LFA1_PFACH,
                     LFA1_ORT02,
                     LFA1_PSTL2,
                     LFA1_TELF2,
                     LFA1_SPRAS,
                     LFA1_SORTL,
                     LFA1_SPERM,
                     LFA1_SPERZ,
                     LFA1_KTOKK,
                     LFA1_BRSCH,
                     LFA1_LOEVM,
                     LFA1_SPERR,
                     LFA1_XCPDK,
                     LFA1_QSSYS,
                     LFA1_VBUND,
                     LFA1_KONZS,
                     LFA1_EMNFR,
                     LFA1_ERDAT,
                     LFA1_ERNAM,
                     LFA1_STCD1,
                     LFA1_STCD2,
                     LFA1_KUNNR,
                     LFA1_STCEG)
	select 	  st1.LFA1_LIFNR_HSH,
              st1.LFA1_LIFNR_LDTS,
              st1.LFA1_LIFNR_RSRC,
              st1.LFA1_LIFNR_ALL_HSH_DIFF,
              st1.LFA1_ADRNR,
              st1.LFA1_NAME1,
              st1.LFA1_STRAS,
              st1.LFA1_ORT01,
              st1.LFA1_REGIO,
              st1.LFA1_PSTLZ,
              st1.LFA1_LAND1,
              st1.LFA1_TELF1,
              st1.LFA1_TELFX,
              st1.LFA1_PFACH,
              st1.LFA1_ORT02,
              st1.LFA1_PSTL2,
              st1.LFA1_TELF2,
              st1.LFA1_SPRAS,
              st1.LFA1_SORTL,
              st1.LFA1_SPERM,
              st1.LFA1_SPERZ,
              st1.LFA1_KTOKK,
              st1.LFA1_BRSCH,
              st1.LFA1_LOEVM,
              st1.LFA1_SPERR,
              st1.LFA1_XCPDK,
              st1.LFA1_QSSYS,
              st1.LFA1_VBUND,
              st1.LFA1_KONZS,
              st1.LFA1_EMNFR,
              st1.LFA1_ERDAT,
              st1.LFA1_ERNAM,
              st1.LFA1_STCD1,
              st1.LFA1_STCD2,
              st1.LFA1_KUNNR,
              st1.LFA1_STCEG
	from lfa1__xbi_hash_stg as st1
	      left outer join sat_vendor_all as s1
	                   on     st1.LFA1_LIFNR_HSH = s1.LFA1_LIFNR_HSH
	                      and s1.LFA1_LIFNR_LEDTS is NULL
	where (
	        s1.LFA1_LIFNR_HSH is null OR
	        (
	              st1.LFA1_LIFNR_ALL_HSH_DIFF != s1.LFA1_LIFNR_ALL_HSH_DIFF
	          AND s1.LFA1_LIFNR_LDTS = (
                                        select max(z.LFA1_LIFNR_LDTS)
                                        from sat_vendor_all z
                                        where s1.LFA1_LIFNR_HSH = z.LFA1_LIFNR_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s7
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s7;
	insert into upd_sat_end_date_loop_s7 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.LFA1_LIFNR_HSH,
	       s1.LFA1_LIFNR_LDTS
	from sat_vendor_all as s1
	where (
	            s1.LFA1_LIFNR_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.LFA1_LIFNR_HSH)
	                     from sat_vendor_all as y
	                     where   y.LFA1_LIFNR_HSH = s1.LFA1_LIFNR_HSH
	                         AND y.LFA1_LIFNR_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s7
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s7;
	insert into upd_sat_end_date_cross_s7 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s7 t1
					inner join upd_sat_end_date_loop_s7 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_vendor_all dst
	   set dst.LFA1_LIFNR_LEDTS = src.BUSKEY_LEDTS
	  from sat_vendor_all dst
				inner join upd_sat_end_date_cross_s7 src on dst.LFA1_LIFNR_HSH = src.BUSKEY_HSH
	      												and dst.LFA1_LIFNR_LDTS = src.BUSKEY_LDTS;
