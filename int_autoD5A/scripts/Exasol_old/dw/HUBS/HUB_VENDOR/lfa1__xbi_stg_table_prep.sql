/* Staging table preparation */

	/* drop table if exists lfa1__xbi_hash_stg
	create table lfa1__xbi_hash_stg */

	truncate table lfa1__xbi_hash_stg;
	insert into lfa1__xbi_hash_stg
	 (
		LFA1_LIFNR_HSH,
		LFA1_LIFNR_LDTS,
		LFA1_LIFNR_RSRC,
		LFA1_LIFNR_ALL_HSH_DIFF,
        LFA1_LIFNR,
        LFA1_ADRNR,
        LFA1_NAME1,
        LFA1_STRAS,
        LFA1_ORT01,
        LFA1_REGIO,
        LFA1_PSTLZ,
        LFA1_LAND1,
        LFA1_TELF1,
        LFA1_TELFX,
        LFA1_PFACH,
        LFA1_ORT02,
        LFA1_PSTL2,
        LFA1_TELF2,
        LFA1_SPRAS,
        LFA1_SORTL,
        LFA1_SPERM,
        LFA1_SPERZ,
        LFA1_KTOKK,
        LFA1_BRSCH,
        LFA1_LOEVM,
        LFA1_SPERR,
        LFA1_XCPDK,
        LFA1_QSSYS,
        LFA1_VBUND,
        LFA1_KONZS,
        LFA1_EMNFR,
        LFA1_ERDAT,
        LFA1_ERNAM,
        LFA1_STCD1,
        LFA1_STCD2,
        LFA1_KUNNR,
        LFA1_STCEG)
	select 	cast(UPPER(HASH_MD5(LFA1_LIFNR)) as varchar(32)) as LFA1_LIFNR_HSH,
	       	current_timestamp as LFA1_LIFNR_LDTS,
	       	cast('ECC: LFA1' as varchar(50)) as LFA1_LIFNR_RSRC,
			cast(UPPER(HASH_MD5(upper(LFA1_LIFNR || '|^|' ||
			                            IFNULL(LFA1_ADRNR,'') || '|^|' ||
			                            IFNULL(LFA1_NAME1,'') || '|^|' ||
			                            IFNULL(LFA1_STRAS,'') || '|^|' ||
			                            IFNULL(LFA1_ORT01,'') || '|^|' ||
			                            IFNULL(LFA1_REGIO,'') || '|^|' ||
			                            IFNULL(LFA1_PSTLZ,'') || '|^|' ||
			                            IFNULL(LFA1_LAND1,'') || '|^|' ||
			                            IFNULL(LFA1_TELF1,'') || '|^|' ||
			                            IFNULL(LFA1_TELFX,'') || '|^|' ||
			                            IFNULL(LFA1_PFACH,'') || '|^|' ||
			                            IFNULL(LFA1_ORT02,'') || '|^|' ||
			                            IFNULL(LFA1_PSTL2,'') || '|^|' ||
			                            IFNULL(LFA1_TELF2,'') || '|^|' ||
			                            IFNULL(LFA1_SPRAS,'') || '|^|' ||
			                            IFNULL(LFA1_SORTL,'') || '|^|' ||
			                            IFNULL(LFA1_SPERM,'') || '|^|' ||
			                            IFNULL(LFA1_SPERZ,'') || '|^|' ||
			                            IFNULL(LFA1_KTOKK,'') || '|^|' ||
			                            IFNULL(LFA1_BRSCH,'') || '|^|' ||
			                            IFNULL(LFA1_LOEVM,'') || '|^|' ||
			                            IFNULL(LFA1_SPERR,'') || '|^|' ||
			                            IFNULL(LFA1_XCPDK,'') || '|^|' ||
			                            IFNULL(LFA1_QSSYS,'') || '|^|' ||
			                            IFNULL(LFA1_VBUND,'') || '|^|' ||
			                            IFNULL(LFA1_KONZS,'') || '|^|' ||
			                            IFNULL(LFA1_EMNFR,'') || '|^|' ||
			                            IFNULL(LFA1_ERDAT,'0001-01-01') || '|^|' ||
			                            IFNULL(LFA1_ERNAM,'') || '|^|' ||
			                            IFNULL(LFA1_STCD1,'') || '|^|' ||
			                            IFNULL(LFA1_STCD2,'') || '|^|' ||
			                            IFNULL(LFA1_KUNNR,'') || '|^|' ||
			                            IFNULL(LFA1_STCEG,'')
				)))as varchar(32)) as LFA1_LIFNR_ALL_HSH_DIFF,
        LFA1_LIFNR,
	    LFA1_ADRNR,
        LFA1_NAME1,
        LFA1_STRAS,
        LFA1_ORT01,
        LFA1_REGIO,
        LFA1_PSTLZ,
        LFA1_LAND1,
        LFA1_TELF1,
        LFA1_TELFX,
        LFA1_PFACH,
        LFA1_ORT02,
        LFA1_PSTL2,
        LFA1_TELF2,
        LFA1_SPRAS,
        LFA1_SORTL,
        LFA1_SPERM,
        LFA1_SPERZ,
        LFA1_KTOKK,
        LFA1_BRSCH,
        LFA1_LOEVM,
        LFA1_SPERR,
        LFA1_XCPDK,
        LFA1_QSSYS,
        LFA1_VBUND,
        LFA1_KONZS,
        LFA1_EMNFR,
        LFA1_ERDAT,
        LFA1_ERNAM,
        LFA1_STCD1,
        LFA1_STCD2,
        LFA1_KUNNR,
        LFA1_STCEG
	from  LFA1__XBI;
