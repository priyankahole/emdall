/* 	Reference table for MARC_SOBSL based on T460T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_specialprocurementdescription
   CREATE TABLE ref_specialprocurementdescription
   (
       MARC_SOBSL VARCHAR(4), --  Code
       MARC_WERKS VARCHAR(4) UTF8, -- Code
       T460T_LTEXT VARCHAR(40) UTF8, -- Descr
       T460T_SPRAS VARCHAR(1) UTF8
   )
*/


   MERGE INTO ref_specialprocurementdescription dst
   USING (SELECT T460T_SOBSL, T460T_WERKS, T460T_LTEXT, T460T_SPRAS FROM T460T__XBI) src
   ON ( dst.MARC_SOBSL = src.T460T_SOBSL AND dst.MARC_WERKS = src.T460T_WERKS
           AND dst.T460T_SPRAS = src.T460T_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.T460T_LTEXT = src.T460T_LTEXT
   WHEN NOT MATCHED THEN INSERT (MARC_SOBSL, MARC_WERKS, T460T_LTEXT, T460T_SPRAS) VALUES (src.T460T_SOBSL, src.T460T_WERKS, src.T460T_LTEXT, src.T460T_SPRAS);
