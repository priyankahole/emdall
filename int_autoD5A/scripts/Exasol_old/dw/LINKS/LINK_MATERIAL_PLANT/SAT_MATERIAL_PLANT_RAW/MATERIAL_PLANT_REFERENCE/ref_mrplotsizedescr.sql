/* 	Reference table for MARC_DISLS based on T439T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_mrplotsizedescription
   CREATE TABLE ref_mrplotsizedescription
   (
       MARC_DISLS VARCHAR(4), --  Code
       T439T_LOSLT VARCHAR(40) UTF8, -- Descr
       T439T_SPRAS VARCHAR(1) UTF8
   )
*/

   MERGE INTO ref_mrplotsizedescription dst
   USING (SELECT T439T_DISLS, T439T_SPRAS, T439T_LOSLT FROM T439T__XBI) src
   ON ( dst.MARC_DISLS = src.T439T_DISLS
           AND dst.T439T_SPRAS = src.T439T_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.T439T_LOSLT = src.T439T_LOSLT
   WHEN NOT MATCHED THEN INSERT (MARC_DISLS, T439T_SPRAS, T439T_LOSLT) VALUES (src.T439T_DISLS, src.T439T_SPRAS, src.T439T_LOSLT);
