/* 	Reference table for MARC_EKGRP based on T024 table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_purchasegroupdescription
   CREATE TABLE ref_purchasegroupdescription
   (
       MARC_EKGRP VARCHAR(4), -- Code
       T024_EKNAM VARCHAR(18) UTF8 -- Desc
   )
*/


   MERGE INTO ref_purchasegroupdescription dst
   USING (SELECT T024_EKGRP, T024_EKNAM FROM T024__XBI WHERE T024_EKGRP IS NOT NULL) src
   ON ( dst.MARC_EKGRP= src.T024_EKGRP)
   WHEN MATCHED THEN UPDATE SET dst.T024_EKNAM = src.T024_EKNAM
   WHEN NOT MATCHED THEN INSERT (MARC_EKGRP, T024_EKNAM) VALUES (src.T024_EKGRP, src.T024_EKNAM);
