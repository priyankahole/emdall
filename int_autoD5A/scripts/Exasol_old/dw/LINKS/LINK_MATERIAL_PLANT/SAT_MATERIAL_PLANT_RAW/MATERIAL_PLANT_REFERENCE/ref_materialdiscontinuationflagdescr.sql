/* 	Reference table for MARC_KZAUS based on DD07T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_materialdiscontinuationflagdescription
   CREATE TABLE ref_materialdiscontinuationflagdescription
   (
       MARC_KZAUS VARCHAR(10), -- Code
       DD07T_DDTEXT     VARCHAR(60) UTF8,
       DD07T_DDLANGUAGE VARCHAR(1) UTF8 -- Desc
   ) */


   MERGE INTO ref_materialdiscontinuationflagdescription dst
   USING (SELECT DD07T_DOMVALUE_L, DD07T_DDLANGUAGE, DD07T_DDTEXT FROM DD07T__XBI WHERE DD07T_DOMNAME = 'KZAUS') src
   ON ( dst.MARC_KZAUS = src.DD07T_DOMVALUE_L
           AND dst.DD07T_DDLANGUAGE = src.DD07T_DDLANGUAGE)
   WHEN MATCHED THEN UPDATE SET dst.DD07T_DDTEXT = src.DD07T_DDTEXT
   WHEN NOT MATCHED THEN INSERT (MARC_KZAUS, DD07T_DDLANGUAGE, DD07T_DDTEXT) VALUES (src.DD07T_DOMVALUE_L, src.DD07T_DDLANGUAGE, src.DD07T_DDTEXT);
