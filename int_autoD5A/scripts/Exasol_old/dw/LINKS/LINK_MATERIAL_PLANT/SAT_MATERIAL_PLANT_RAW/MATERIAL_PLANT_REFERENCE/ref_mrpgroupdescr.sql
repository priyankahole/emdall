/* 	Reference table for MARC_DISGR based on T438X table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_mrpgroupdescription
   CREATE TABLE ref_mrpgroupdescription
   (
       MARC_DISGR VARCHAR(4), --
       MARC_WERKS  VARCHAR(4) UTF8, --
       T438X_TEXT40 VARCHAR(40) UTF8, -- Descr
       T438X_SPRAS  VARCHAR(1) UTF8
   )
*/

   MERGE INTO ref_mrpgroupdescription dst
   USING (SELECT T438X_DISGR, T438X_WERKS, T438X_TEXT40, T438X_SPRAS FROM T438X__XBI) src
   ON ( dst.MARC_DISGR = src.T438X_DISGR AND dst.MARC_WERKS = src.T438X_WERKS
           AND dst.T438X_SPRAS = src.T438X_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.T438X_TEXT40 = src.T438X_TEXT40
   WHEN NOT MATCHED THEN INSERT (MARC_DISGR, MARC_WERKS, T438X_TEXT40, T438X_SPRAS) VALUES (src.T438X_DISGR, src.T438X_WERKS, src.T438X_TEXT40, src.T438X_SPRAS);
