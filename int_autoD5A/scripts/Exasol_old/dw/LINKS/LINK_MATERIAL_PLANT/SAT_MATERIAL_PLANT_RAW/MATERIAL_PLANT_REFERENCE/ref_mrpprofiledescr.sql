/* 	Reference table for MARC_DISPR based on T401T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_mrpprofiledescr
   CREATE TABLE ref_mrpprofiledescr
   (
       MARC_DISPR VARCHAR(4) -- MRP Profile Code
       ,T401T_SPRAS VARCHAR(1)
       ,T401T_KTEXT VARCHAR(40) -- MRP Profile Description
   ) */

   MERGE INTO ref_mrpprofiledescr dst
   USING (SELECT T401T_DISPR, T401T_SPRAS, T401T_KTEXT FROM T401T__XBI) src
   ON ( dst.MARC_DISPR = src.T401T_DISPR
           AND dst.T401T_SPRAS = src.T401T_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.T401T_KTEXT = src.T401T_KTEXT
   WHEN NOT MATCHED THEN INSERT (MARC_DISPR, T401T_SPRAS, T401T_KTEXT) VALUES (src.T401T_DISPR, src.T401T_SPRAS, src.T401T_KTEXT);
