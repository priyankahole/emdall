/* Staging table preparation */

	/* drop table if exists marc__xbi_hash_stg
	create table marc__xbi_hash_stg */

truncate table marc__xbi_hash_stg;
insert into marc__xbi_hash_stg (
	MARC_MATNR_WERKS_HSH,
  MARC_MATNR_WERKS_LDTS,
  MARC_MATNR_WERKS_RSRC,
	MARC_MATNR_HSH,
	MARC_WERKS_HSH,
    MARC_MATNR_WERKS_NUMBER_HSH_DIFF,
	MARC_MATNR_WERKS_TEXT_HSH_DIFF,
    -- date
    MARC_PREND,
    -- number
    MARC_BSTMI, MARC_BWESB, MARC_GLGMG, MARC_UMLMC, MARC_TRAME, MARC_UEETO, MARC_UNETO, MARC_AUSSS, MARC_BEARZ, MARC_BSTFE, MARC_EISBE, MARC_LOSGR,
    MARC_MINBE, MARC_PLIFZ, MARC_BSTMA, MARC_BSTRF, MARC_DZEIT, MARC_EISLO, MARC_MABST, MARC_MAXLZ, MARC_PRFRQ, MARC_WEBAZ, MARC_WZEIT,
    -- text
	MARC_MATNR, MARC_WERKS, MARC_UEETK, MARC_LADGR, MARC_LVORM, MARC_ALTSL, MARC_DISGR, MARC_DISMM, MARC_DISPR, MARC_EKGRP, MARC_FHORI, MARC_FVIDK,
    MARC_INSMK, MARC_KZAUS, MARC_KZKRI, MARC_LGFSB, MARC_MAABC, MARC_MTVFP, MARC_PERIV, MARC_PRENC, MARC_PRENO, MARC_PSTAT, MARC_QMATV, MARC_RGEKZ,
    MARC_SBDKZ, MARC_SFCPF, MARC_SHFLG, MARC_SOBSK, MARC_SSQSS, MARC_STRGR, MARC_USEQU, MARC_VINT1, MARC_VRMOD, MARC_XCHAR, MARC_AUSME, MARC_AWSLS,
    MARC_BESKZ, MARC_BWSCL, MARC_DISLS, MARC_DISPO, MARC_FEVOR, MARC_FRTME, MARC_FXHOR, MARC_HERKL, MARC_KAUTB, MARC_KZDKZ, MARC_LGPRO, MARC_MMSTA,
    MARC_NCOST, MARC_PERKZ, MARC_PRCTR, MARC_QMATA, MARC_QZGTP, MARC_RWPRO, MARC_SCHGT, MARC_SFEPR, MARC_SHZET, MARC_SOBSL, MARC_STAWN, MARC_VERKZ,
    MARC_VINT2, MARC_XCHPF )
select 	cast(UPPER(HASH_MD5(ifnull(MARC_MATNR,'') ||'|^|'|| ifnull(MARC_WERKS,''))) as varchar(32)) as MARC_MATNR_WERKS_HSH,
        current_timestamp as MARC_MATNR_WERKS_LDTS,
        cast('ECC: MARC' as varchar(50)) as MARC_MATNR_WERKS_RSRC,
		cast(UPPER(HASH_MD5(ifnull(MARC_MATNR,''))) as varchar(32)) as MARC_MATNR_HSH,
		cast(UPPER(HASH_MD5(ifnull(MARC_WERKS,''))) as varchar(32)) as MARC_WERKS_HSH,
    cast(UPPER(HASH_MD5(ifnull(MARC_MATNR,'') ||'|^|'|| ifnull(MARC_WERKS,'') ||'|^|'||
							-- date
							ifnull(MARC_PREND, '0001-01-01') ||'|^|'||
    						-- number
    						ifnull(MARC_BSTMI, 0.00) ||'|^|'||
							ifnull(MARC_BWESB, 0.00) ||'|^|'||
							ifnull(MARC_GLGMG, 0.00) ||'|^|'||
							ifnull(MARC_UMLMC, 0.00) ||'|^|'||
							ifnull(MARC_TRAME, 0.00) ||'|^|'||
							ifnull(MARC_UEETO, 0.00) ||'|^|'||
							ifnull(MARC_UNETO, 0.00) ||'|^|'||
							ifnull(MARC_AUSSS, 0.00) ||'|^|'||
							ifnull(MARC_BEARZ, 0.00) ||'|^|'||
							ifnull(MARC_BSTFE, 0.00) ||'|^|'||
							ifnull(MARC_EISBE, 0.00) ||'|^|'||
							ifnull(MARC_LOSGR, 0.00) ||'|^|'||
							ifnull(MARC_MINBE, 0.00) ||'|^|'||
							ifnull(MARC_PLIFZ, 0.00) ||'|^|'||
							ifnull(MARC_BSTMA, 0.00) ||'|^|'||
							ifnull(MARC_BSTRF, 0.00) ||'|^|'||
							ifnull(MARC_DZEIT, 0.00) ||'|^|'||
							ifnull(MARC_EISLO, 0.00) ||'|^|'||
							ifnull(MARC_MABST, 0.00) ||'|^|'||
							ifnull(MARC_MAXLZ, 0.00) ||'|^|'||
							ifnull(MARC_PRFRQ, 0.00) ||'|^|'||
							ifnull(MARC_WEBAZ, 0.00) ||'|^|'||
							ifnull(MARC_WZEIT, 0.00))) as varchar(32)) as MARC_MATNR_WERKS_NUMBER_HSH_DIFF,
		cast(UPPER(HASH_MD5(ifnull(MARC_MATNR,'') ||'|^|'|| ifnull(MARC_WERKS,'') ||'|^|'||
							-- text
							ifnull(MARC_UEETK,'')  ||'|^|'||
							ifnull(MARC_LADGR, '')  ||'|^|'||
							ifnull(MARC_LVORM, '')  ||'|^|'||
							ifnull(MARC_ALTSL, '')  ||'|^|'||
							ifnull(MARC_DISGR, '')  ||'|^|'||
							ifnull(MARC_DISMM, '')  ||'|^|'||
							ifnull(MARC_DISPR, '')  ||'|^|'||
							ifnull(MARC_EKGRP, '')  ||'|^|'||
							ifnull(MARC_FHORI, '')  ||'|^|'||
							ifnull(MARC_FVIDK, '')  ||'|^|'||
							ifnull(MARC_INSMK, '')  ||'|^|'||
							ifnull(MARC_KZAUS, '')  ||'|^|'||
							ifnull(MARC_KZKRI, '')  ||'|^|'||
							ifnull(MARC_LGFSB, '')  ||'|^|'||
							ifnull(MARC_MAABC, '')  ||'|^|'||
							ifnull(MARC_MTVFP, '')  ||'|^|'||
							ifnull(MARC_PERIV, '')  ||'|^|'||
							ifnull(MARC_PRENC, '')  ||'|^|'||
							ifnull(MARC_PRENO, '')  ||'|^|'||
							ifnull(MARC_PSTAT, '')  ||'|^|'||
							ifnull(MARC_QMATV, '')  ||'|^|'||
							ifnull(MARC_RGEKZ, '')  ||'|^|'||
							ifnull(MARC_SBDKZ, '')  ||'|^|'||
							ifnull(MARC_SFCPF, '')  ||'|^|'||
							ifnull(MARC_SHFLG, '')  ||'|^|'||
							ifnull(MARC_SOBSK, '')  ||'|^|'||
							ifnull(MARC_SSQSS, '')  ||'|^|'||
							ifnull(MARC_STRGR, '')  ||'|^|'||
							ifnull(MARC_USEQU, '')  ||'|^|'||
							ifnull(MARC_VINT1, '')  ||'|^|'||
							ifnull(MARC_VRMOD, '')  ||'|^|'||
							ifnull(MARC_XCHAR, '')  ||'|^|'||
							ifnull(MARC_AUSME, '')  ||'|^|'||
							ifnull(MARC_AWSLS, '')  ||'|^|'||
							ifnull(MARC_BESKZ, '')  ||'|^|'||
							ifnull(MARC_BWSCL, '')  ||'|^|'||
							ifnull(MARC_DISLS, '')  ||'|^|'||
							ifnull(MARC_DISPO, '')  ||'|^|'||
							ifnull(MARC_FEVOR, '')  ||'|^|'||
							ifnull(MARC_FRTME, '')  ||'|^|'||
							ifnull(MARC_FXHOR, '')  ||'|^|'||
							ifnull(MARC_HERKL, '')  ||'|^|'||
							ifnull(MARC_KAUTB, '')  ||'|^|'||
							ifnull(MARC_KZDKZ, '')  ||'|^|'||
							ifnull(MARC_LGPRO, '')  ||'|^|'||
							ifnull(MARC_MMSTA, '')  ||'|^|'||
							ifnull(MARC_NCOST, '')  ||'|^|'||
							ifnull(MARC_PERKZ, '')  ||'|^|'||
							ifnull(MARC_PRCTR, '')  ||'|^|'||
							ifnull(MARC_QMATA, '')  ||'|^|'||
							ifnull(MARC_QZGTP, '')  ||'|^|'||
							ifnull(MARC_RWPRO, '')  ||'|^|'||
							ifnull(MARC_SCHGT, '')  ||'|^|'||
							ifnull(MARC_SFEPR, '')  ||'|^|'||
							ifnull(MARC_SHZET, '')  ||'|^|'||
							ifnull(MARC_SOBSL, '')  ||'|^|'||
							ifnull(MARC_STAWN, '')  ||'|^|'||
							ifnull(MARC_VERKZ, '')  ||'|^|'||
							ifnull(MARC_VINT2, '')  ||'|^|'||
							ifnull(MARC_XCHPF, ''))) as varchar(32)) as MARC_MATNR_WERKS_TEXT_HSH_DIFF,
    -- date
    MARC_PREND,
    -- number
    MARC_BSTMI, MARC_BWESB, MARC_GLGMG, MARC_UMLMC, MARC_TRAME, MARC_UEETO, MARC_UNETO, MARC_AUSSS, MARC_BEARZ, MARC_BSTFE, MARC_EISBE, MARC_LOSGR,
    MARC_MINBE, MARC_PLIFZ, MARC_BSTMA, MARC_BSTRF, MARC_DZEIT, MARC_EISLO, MARC_MABST, MARC_MAXLZ, MARC_PRFRQ, MARC_WEBAZ, MARC_WZEIT,
    -- text
	MARC_MATNR, MARC_WERKS, MARC_UEETK, MARC_LADGR, MARC_LVORM, MARC_ALTSL, MARC_DISGR, MARC_DISMM, MARC_DISPR, MARC_EKGRP, MARC_FHORI, MARC_FVIDK,
    MARC_INSMK, MARC_KZAUS, MARC_KZKRI, MARC_LGFSB, MARC_MAABC, MARC_MTVFP, MARC_PERIV, MARC_PRENC, MARC_PRENO, MARC_PSTAT, MARC_QMATV, MARC_RGEKZ,
    MARC_SBDKZ, MARC_SFCPF, MARC_SHFLG, MARC_SOBSK, MARC_SSQSS, MARC_STRGR, MARC_USEQU, MARC_VINT1, MARC_VRMOD, MARC_XCHAR, MARC_AUSME, MARC_AWSLS,
    MARC_BESKZ, MARC_BWSCL, MARC_DISLS, MARC_DISPO, MARC_FEVOR, MARC_FRTME, MARC_FXHOR, MARC_HERKL, MARC_KAUTB, MARC_KZDKZ, MARC_LGPRO, MARC_MMSTA,
    MARC_NCOST, MARC_PERKZ, MARC_PRCTR, MARC_QMATA, MARC_QZGTP, MARC_RWPRO, MARC_SCHGT, MARC_SFEPR, MARC_SHZET, MARC_SOBSL, MARC_STAWN, MARC_VERKZ,
    MARC_VINT2, MARC_XCHPF
from MARC__XBI;
