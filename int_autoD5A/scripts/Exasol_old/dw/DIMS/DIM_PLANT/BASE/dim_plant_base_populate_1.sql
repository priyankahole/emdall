-- alter table dim_plant__xbi add constraint PK_dim_plant__xbi primary key (dim_plant__xbiid)

truncate table dim_plant__xbi;
insert into dim_plant__xbi (dim_plant__xbiid, dd_plantcode, dd_SalesOrgIntercompanyBilling, dd_Address, dd_Indicator_BatchStatusManagementActive, dd_FactoryCalendarKey, dd_CustomerNumberPlant, 
							dd_VendorNumberPlant, dd_Name, dd_Name2, dd_POBox, dd_ValuationArea, dd_PurchasingOrganization, dd_CountryKey, dd_Region, dd_City, dd_HouseNumberStreet, dd_PostalCode, dd_LanguageKey)
select  hp.T001W_WERKS_HSH as dim_plant__xbiid,
		hp.T001W_WERKS as dd_plantcode,
		ifnull(spt.T001W_VKORG, 'Not Set') as dd_SalesOrgIntercompanyBilling,
		ifnull(spt.T001W_ADRNR, 'Not Set') as dd_Address,
		ifnull(spt.T001W_CHAZV, 'Not Set') as dd_Indicator_BatchStatusManagementActive,
		ifnull(spt.T001W_FABKL, 'Not Set') as dd_FactoryCalendarKey,
		ifnull(spt.T001W_KUNNR, 'Not Set') as dd_CustomerNumberPlant,
		ifnull(spt.T001W_LIFNR, 'Not Set') as dd_VendorNumberPlant,
		ifnull(spt.T001W_NAME1, 'Not Set') as dd_Name,
		ifnull(spt.T001W_NAME2, 'Not Set') as dd_Name2,
		ifnull(spt.T001W_PFACH, 'Not Set') as dd_POBox,
		ifnull(spt.T001W_BWKEY, 'Not Set') as dd_ValuationArea,
		ifnull(spt.T001W_EKORG, 'Not Set') as dd_PurchasingOrganization,
		ifnull(spt.T001W_LAND1, 'Not Set') as dd_CountryKey,
		ifnull(spt.T001W_REGIO, 'Not Set') as dd_Region,
		ifnull(spt.T001W_ORT01, 'Not Set') as dd_City,
		ifnull(spt.T001W_STRAS, 'Not Set') as dd_HouseNumberStreet,
		ifnull(spt.T001W_PSTLZ, 'Not Set') as dd_PostalCode,
		ifnull(spt.T001W_SPRAS, 'Not Set') as dd_LanguageKey		
from HUB_PLANT hp
		left join SAT_PLANT_TEXT spt on hp.T001W_WERKS_HSH = spt.T001W_WERKS_HSH
where spt.T001W_WERKS_LEDTS is null;


