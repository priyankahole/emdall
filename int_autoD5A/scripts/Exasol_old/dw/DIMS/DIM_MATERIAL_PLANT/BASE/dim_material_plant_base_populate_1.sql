-- alter table dim_material_plant__xbi add constraint PK_dim_material_plant__xbi primary key (dim_material_plant__xbiid)
-- alter table DIM_MATERIAL_PLANT__XBI add column dim_plant__xbiid varchar(32)
-- alter table DIM_MATERIAL_PLANT__XBI add column dim_material__xbiid varchar(32)

truncate table dim_material_plant__xbi;
insert into dim_material_plant__xbi (dim_material_plant__xbiid, dim_plant__xbiid, dim_material__xbiid, dd_IssueDateExemptionCertificate, ct_MinimumLotSize, ct_ValuatedGoodsReceiptBlockedStock, ct_TiedEmptiesStock, ct_StockTransfer_PlantToPlant,
									ct_StockTransit, ct_OverdeliveryToleranceLimit, ct_UnderdeliveryToleranceLimit, ct_AssemblyScrapPercent, ct_ProcessingTime, ct_FixedLotSize, ct_SafetyStock, 
									ct_LotSizeProductCosting, ct_ReorderPoint, ct_PlannedDeliveryTimeDays, ct_MaximumLotSize, ct_RoundingValuePurchaseOrderQuantity, ct_InHouseProductionTime,
									ct_MinimumSafetyStock, ct_MaximumStockLevel, ct_MaximumStoragePeriod, ct_IntervalUntilNextRecurringInspection, ct_GoodsReceiptProcessingTimeDays, 
									ct_TotalReplenishmentLeadTimeWorkdays, dd_Indicator_UnlimitedOverdeliveryAllowed, dd_LoadingGroup, dd_FlagMaterialDeletionPlantLevel, dd_MethodSelectingAlternativeBillsMaterial,
									dd_MRPGroup, dd_MRPType, dd_MRPProfile_Material, dd_PurchasingGroup, dd_SchedulingMarginKeyFloats, dd_ProductionVersionCosted, dd_PostInspectionStock, dd_DiscontinuationIndicator, 
									dd_Indicator_CriticalPart, dd_DefaultStorageLocationExternalProcurement, dd_ABC_Indicator, dd_CheckingGroupAvailabilityCheck, dd_FiscalYearVariant, 
									dd_ExemptionCertificateIndicatorLegalControl, dd_ExemptionCertificateNumberLegalControl, dd_MaintenanceStatus, dd_InspectionSetupExists, dd_Indicator_Backflush, 
									dd_DependentRequirementsIndividual, dd_ProductionSchedulingProfile, dd_SafetyTimeIndicator, dd_SpecialProcurementTypeCosting, dd_ControlKeyQualityManagementProcurement, 
									dd_PlanningStrategyGroup, dd_QuotaArrangementUsage, dd_ConsumptionPeriodBackward, dd_ConsumptionMode, dd_BatchManagementIndicator, dd_UnitOfIssue, dd_VarianceKey,
									dd_ProcurementType, dd_SourceSupply, dd_LotSize_MaterialsPlanning, dd_MRPController_MaterialsPlanner, dd_ProductionSupervisor, dd_ProductionUnit, dd_PlanningTimeFence, 
									dd_CountryOriginMaterial, dd_Indicator_AutomaticPurchaseOrderAllowed, dd_DocumentationRequiredIndicator, dd_IssueStorageLocation, dd_PlantSpecificMaterialStatus, dd_DoNotCost, 
									dd_PeriodIndicator, dd_ProfitCenter, dd_MaterialAuthorizationGroupActivities, dd_CertificateType, dd_RangeCoverageProfile, dd_Indicator_BulkMaterial, 
									dd_RepetitiveManufacturingProfile, dd_SafetyTimeWorkdays, dd_SpecialProcurementType, dd_CommodityCodeImportCode, dd_VersionIndicator, dd_ConsumptionPeriodForward, 
									dd_BatchManagementRequirementIndicator)
select 	lmp.MARC_MATNR_WERKS_HSH as dim_material_plant__xbiid,
        lmp.MARC_WERKS_HSH dim_plant__xbiid,
        lmp.MARC_MATNR_HSH dim_material__xbiid, 
		ifnull(lsmpn.MARC_PREND, '0001-01-01') as dd_IssueDateExemptionCertificate,
		ifnull(lsmpn.MARC_BSTMI, 0.00) as ct_MinimumLotSize,
		ifnull(lsmpn.MARC_BWESB, 0.00) as ct_ValuatedGoodsReceiptBlockedStock,
		ifnull(lsmpn.MARC_GLGMG, 0.00) as ct_TiedEmptiesStock,
		ifnull(lsmpn.MARC_UMLMC, 0.00) as ct_StockTransfer_PlantToPlant,
		ifnull(lsmpn.MARC_TRAME, 0.00) as ct_StockTransit,
		ifnull(lsmpn.MARC_UEETO, 0.00) as ct_OverdeliveryToleranceLimit,
		ifnull(lsmpn.MARC_UNETO, 0.00) as ct_UnderdeliveryToleranceLimit,
		ifnull(lsmpn.MARC_AUSSS, 0.00) as ct_AssemblyScrapPercent,
		ifnull(lsmpn.MARC_BEARZ, 0.00) as ct_ProcessingTime,
		ifnull(lsmpn.MARC_BSTFE, 0.00) as ct_FixedLotSize,
		ifnull(lsmpn.MARC_EISBE, 0.00) as ct_SafetyStock,
		ifnull(lsmpn.MARC_LOSGR, 0.00) as ct_LotSizeProductCosting,
		ifnull(lsmpn.MARC_MINBE, 0.00) as ct_ReorderPoint,
		ifnull(lsmpn.MARC_PLIFZ, 0.00) as ct_PlannedDeliveryTimeDays,
		ifnull(lsmpn.MARC_BSTMA, 0.00) as ct_MaximumLotSize,
		ifnull(lsmpn.MARC_BSTRF, 0.00) as ct_RoundingValuePurchaseOrderQuantity,
		ifnull(lsmpn.MARC_DZEIT, 0.00) as ct_InHouseProductionTime,
		ifnull(lsmpn.MARC_EISLO, 0.00) as ct_MinimumSafetyStock,
		ifnull(lsmpn.MARC_MABST, 0.00) as ct_MaximumStockLevel,
		ifnull(lsmpn.MARC_MAXLZ, 0.00) as ct_MaximumStoragePeriod,
		ifnull(lsmpn.MARC_PRFRQ, 0.00) as ct_IntervalUntilNextRecurringInspection,
		ifnull(lsmpn.MARC_WEBAZ, 0.00) as ct_GoodsReceiptProcessingTimeDays,
		ifnull(lsmpn.MARC_WZEIT, 0.00) as ct_TotalReplenishmentLeadTimeWorkdays,
		ifnull(lsmpt.MARC_UEETK, 'Not Set') as dd_Indicator_UnlimitedOverdeliveryAllowed,
		ifnull(lsmpt.MARC_LADGR, 'Not Set') as dd_LoadingGroup,
		ifnull(lsmpt.MARC_LVORM, 'Not Set') as dd_FlagMaterialDeletionPlantLevel,
		ifnull(lsmpt.MARC_ALTSL, 'Not Set') as dd_MethodSelectingAlternativeBillsMaterial,
		ifnull(lsmpt.MARC_DISGR, 'Not Set') as dd_MRPGroup,
		ifnull(lsmpt.MARC_DISMM, 'Not Set') as dd_MRPType,
		ifnull(lsmpt.MARC_DISPR, 'Not Set') as dd_MRPProfile_Material,
		ifnull(lsmpt.MARC_EKGRP, 'Not Set') as dd_PurchasingGroup,
		ifnull(lsmpt.MARC_FHORI, 'Not Set') as dd_SchedulingMarginKeyFloats,
		ifnull(lsmpt.MARC_FVIDK, 'Not Set') as dd_ProductionVersionCosted,
		ifnull(lsmpt.MARC_INSMK, 'Not Set') as dd_PostInspectionStock,
		ifnull(lsmpt.MARC_KZAUS, 'Not Set') as dd_DiscontinuationIndicator,
		ifnull(lsmpt.MARC_KZKRI, 'Not Set') as dd_Indicator_CriticalPart,
		ifnull(lsmpt.MARC_LGFSB, 'Not Set') as dd_DefaultStorageLocationExternalProcurement,
		ifnull(lsmpt.MARC_MAABC, 'Not Set') as dd_ABC_Indicator,
		ifnull(lsmpt.MARC_MTVFP, 'Not Set') as dd_CheckingGroupAvailabilityCheck,
		ifnull(lsmpt.MARC_PERIV, 'Not Set') as dd_FiscalYearVariant,
		ifnull(lsmpt.MARC_PRENC, 'Not Set') as dd_ExemptionCertificateIndicatorLegalControl,
		ifnull(lsmpt.MARC_PRENO, 'Not Set') as dd_ExemptionCertificateNumberLegalControl,
		ifnull(lsmpt.MARC_PSTAT, 'Not Set') as dd_MaintenanceStatus,
		ifnull(lsmpt.MARC_QMATV, 'Not Set') as dd_InspectionSetupExists,
		ifnull(lsmpt.MARC_RGEKZ, 'Not Set') as dd_Indicator_Backflush,
		ifnull(lsmpt.MARC_SBDKZ, 'Not Set') as dd_DependentRequirementsIndividual,
		ifnull(lsmpt.MARC_SFCPF, 'Not Set') as dd_ProductionSchedulingProfile,
		ifnull(lsmpt.MARC_SHFLG, 'Not Set') as dd_SafetyTimeIndicator,
		ifnull(lsmpt.MARC_SOBSK, 'Not Set') as dd_SpecialProcurementTypeCosting,
		ifnull(lsmpt.MARC_SSQSS, 'Not Set') as dd_ControlKeyQualityManagementProcurement,
		ifnull(lsmpt.MARC_STRGR, 'Not Set') as dd_PlanningStrategyGroup,
		ifnull(lsmpt.MARC_USEQU, 'Not Set') as dd_QuotaArrangementUsage,
		ifnull(lsmpt.MARC_VINT1, 'Not Set') as dd_ConsumptionPeriodBackward,
		ifnull(lsmpt.MARC_VRMOD, 'Not Set') as dd_ConsumptionMode,
		ifnull(lsmpt.MARC_XCHAR, 'Not Set') as dd_BatchManagementIndicator,
		ifnull(lsmpt.MARC_AUSME, 'Not Set') as dd_UnitOfIssue,
		ifnull(lsmpt.MARC_AWSLS, 'Not Set') as dd_VarianceKey,
		ifnull(lsmpt.MARC_BESKZ, 'Not Set') as dd_ProcurementType,
		ifnull(lsmpt.MARC_BWSCL, 'Not Set') as dd_SourceSupply,
		ifnull(lsmpt.MARC_DISLS, 'Not Set') as dd_LotSize_MaterialsPlanning,
		ifnull(lsmpt.MARC_DISPO, 'Not Set') as dd_MRPController_MaterialsPlanner,
		ifnull(lsmpt.MARC_FEVOR, 'Not Set') as dd_ProductionSupervisor,
		ifnull(lsmpt.MARC_FRTME, 'Not Set') as dd_ProductionUnit,
		ifnull(lsmpt.MARC_FXHOR, 'Not Set') as dd_PlanningTimeFence,
		ifnull(lsmpt.MARC_HERKL, 'Not Set') as dd_CountryOriginMaterial,
		ifnull(lsmpt.MARC_KAUTB, 'Not Set') as dd_Indicator_AutomaticPurchaseOrderAllowed,
		ifnull(lsmpt.MARC_KZDKZ, 'Not Set') as dd_DocumentationRequiredIndicator,
		ifnull(lsmpt.MARC_LGPRO, 'Not Set') as dd_IssueStorageLocation,
		ifnull(lsmpt.MARC_MMSTA, 'Not Set') as dd_PlantSpecificMaterialStatus,
		ifnull(lsmpt.MARC_NCOST, 'Not Set') as dd_DoNotCost,
		ifnull(lsmpt.MARC_PERKZ, 'Not Set') as dd_PeriodIndicator,
		ifnull(lsmpt.MARC_PRCTR, 'Not Set') as dd_ProfitCenter,
		ifnull(lsmpt.MARC_QMATA, 'Not Set') as dd_MaterialAuthorizationGroupActivities,
		ifnull(lsmpt.MARC_QZGTP, 'Not Set') as dd_CertificateType,
		ifnull(lsmpt.MARC_RWPRO, 'Not Set') as dd_RangeCoverageProfile,
		ifnull(lsmpt.MARC_SCHGT, 'Not Set') as dd_Indicator_BulkMaterial,
		ifnull(lsmpt.MARC_SFEPR, 'Not Set') as dd_RepetitiveManufacturingProfile,
		ifnull(lsmpt.MARC_SHZET, 'Not Set') as dd_SafetyTimeWorkdays,
		ifnull(lsmpt.MARC_SOBSL, 'Not Set') as dd_SpecialProcurementType,
		ifnull(lsmpt.MARC_STAWN, 'Not Set') as dd_CommodityCodeImportCode,
		ifnull(lsmpt.MARC_VERKZ, 'Not Set') as dd_VersionIndicator,
		ifnull(lsmpt.MARC_VINT2, 'Not Set') as dd_ConsumptionPeriodForward,
		ifnull(lsmpt.MARC_XCHPF, 'Not Set') as dd_BatchManagementRequirementIndicator
from LINK_MATERIAL_PLANT lmp
		left join SAT_MATERIAL_PLANT_NUMBER lsmpn on lmp.MARC_MATNR_WERKS_HSH = lsmpn.MARC_MATNR_WERKS_HSH
		left join SAT_MATERIAL_PLANT_TEXT lsmpt on lmp.MARC_MATNR_WERKS_HSH = lsmpt.MARC_MATNR_WERKS_HSH
where   lsmpn.MARC_MATNR_WERKS_LEDTS is null 
	and lsmpt.MARC_MATNR_WERKS_LEDTS is null;
