-- alter table dim_material__xbi add constraint PK_dim_material__xbi primary key (dim_material__xbiid)

/* keep only one records from ADRC based on NATION field */
drop table if exists tmp_SAT_MATERIAL_DESCRIPTION_l1;
create table tmp_SAT_MATERIAL_DESCRIPTION_l1 as
select xx.*
from (select t.*,
			 row_number()over(partition by t.MAKT_MATNR_HSH order by (case when t.MAKT_SPRAS = 'E' then 1 else 2 end) asc) rn
	  from SAT_MATERIAL_DESCRIPTION t
	  where MAKT_MATNR_LEDTS is null) xx
where xx.rn = 1;

truncate table dim_material__xbi;
insert into dim_material__xbi (dim_material__xbiid,
								dd_MaterialNumber, dd_DateLastChange, dd_DateCreatedOn, dd_DateFromWhichCrossPlantMaterialStatusValid,
								ct_GrossWeight, ct_MinimumRemainingShelfLife, ct_NetWeight, ct_TotalShelfLife, ct_Volume, ct_NrGRGISlipsToPrint,
								dd_InternationalArticleNumber, dd_PersonCreatedObject, dd_ExternalMaterialGroup, dd_Size, dd_RevisionLevelAssignedMaterial, dd_MaterialGroup, dd_BaseUnitOfMeasure,
								dd_ManufacturerPartNumber, dd_CrossDistributionChainMaterialStatus, dd_MaterialType, dd_MaintenanceStatus, dd_StorageConditions, dd_ExpirationDate, dd_HazardousMaterialNumber,
								dd_TransportationGroup, dd_MaintenanceStatusCompleteMaterial, dd_BasicMaterial, dd_OldMaterialNumber, dd_PurchaseOrderUnitMeasure, dd_PurchasingValueKey, dd_WeightUnit,
								dd_PeriodIndicatorShelfLifeExpirationDate, dd_LaboratoryDesignOffice, dd_FlagMaterialDeletionClientLevel, dd_NumberManufacturer, dd_CrossPlantMaterialStatus, dd_ProductHierarchy,
								dd_QMProcurementActive, dd_RoundingCalculationOfSLED, dd_Division, dd_TemperatureConditionsIndicator, dd_GeneralItemCategoryGroup, dd_MaterialDescrLanguageKey,
								dd_MaterialDescriptionShort, dd_MaterialDescriptionUpperCase)
/* drop table dim_material__xbi
   create table dim_material__xbi as */
select 	ifnull(hm.MARA_MATNR_HSH, '') as dim_material__xbiid,
	   	case when hm.MARA_MATNR is not null then hm.MARA_MATNR else '?' end as dd_MaterialNumber,
		ifnull(MARA_LAEDA, '0001-01-01') as dd_DateLastChange,
		ifnull(MARA_ERSDA, '0001-01-01') as dd_DateCreatedOn,
		ifnull(MARA_MSTDE, '0001-01-01') as dd_DateFromWhichCrossPlantMaterialStatusValid,
	   	ifnull(smn.MARA_BRGEW, 0) as ct_GrossWeight,
	   	ifnull(smn.MARA_MHDRZ, 0) as ct_MinimumRemainingShelfLife,
	   	ifnull(smn.MARA_NTGEW, 0) as ct_NetWeight,
	   	ifnull(smn.MARA_MHDHB, 0) as ct_TotalShelfLife,
	   	ifnull(smn.MARA_VOLUM, 0) as ct_Volume,
	   	ifnull(smn.MARA_WESCH, 0) as ct_NrGRGISlipsToPrint,
		ifnull(smt.MARA_EAN11, 'Not Set') as dd_InternationalArticleNumber,
		ifnull(smt.MARA_ERNAM, 'Not Set') as dd_PersonCreatedObject,
		ifnull(smt.MARA_EXTWG, 'Not Set') as dd_ExternalMaterialGroup,
		ifnull(smt.MARA_GROES, 'Not Set') as dd_Size,
		ifnull(smt.MARA_KZREV, 'Not Set') as dd_RevisionLevelAssignedMaterial,
		ifnull(smt.MARA_MATKL, 'Not Set') as dd_MaterialGroup,
		ifnull(smt.MARA_MEINS, 'Not Set') as dd_BaseUnitOfMeasure,
		ifnull(smt.MARA_MFRPN, 'Not Set') as dd_ManufacturerPartNumber,
		ifnull(smt.MARA_MSTAV, 'Not Set') as dd_CrossDistributionChainMaterialStatus,
		ifnull(smt.MARA_MTART, 'Not Set') as dd_MaterialType,
		ifnull(smt.MARA_PSTAT, 'Not Set') as dd_MaintenanceStatus,
		ifnull(smt.MARA_RAUBE, 'Not Set') as dd_StorageConditions,
		ifnull(smt.MARA_SLED_BBD, 'Not Set') as dd_ExpirationDate,
		ifnull(smt.MARA_STOFF, 'Not Set') as dd_HazardousMaterialNumber,
		ifnull(smt.MARA_TRAGR, 'Not Set') as dd_TransportationGroup,
		ifnull(smt.MARA_VPSTA, 'Not Set') as dd_MaintenanceStatusCompleteMaterial,
		ifnull(smt.MARA_WRKST, 'Not Set') as dd_BasicMaterial,
		ifnull(smt.MARA_BISMT, 'Not Set') as dd_OldMaterialNumber,
		ifnull(smt.MARA_BSTME, 'Not Set') as dd_PurchaseOrderUnitMeasure,
		ifnull(smt.MARA_EKWSL, 'Not Set') as dd_PurchasingValueKey,
		ifnull(smt.MARA_GEWEI, 'Not Set') as dd_WeightUnit,
		ifnull(smt.MARA_IPRKZ, 'Not Set') as dd_PeriodIndicatorShelfLifeExpirationDate,
		ifnull(smt.MARA_LABOR, 'Not Set') as dd_LaboratoryDesignOffice,
		ifnull(smt.MARA_LVORM, 'Not Set') as dd_FlagMaterialDeletionClientLevel,
		ifnull(smt.MARA_MFRNR, 'Not Set') as dd_NumberManufacturer,
		ifnull(smt.MARA_MSTAE, 'Not Set') as dd_CrossPlantMaterialStatus,
		ifnull(smt.MARA_PRDHA, 'Not Set') as dd_ProductHierarchy,
		ifnull(smt.MARA_QMPUR, 'Not Set') as dd_QMProcurementActive,
		ifnull(smt.MARA_RDMHD, 'Not Set') as dd_RoundingCalculationOfSLED,
		ifnull(smt.MARA_SPART, 'Not Set') as dd_Division,
		ifnull(smt.MARA_TEMPB, 'Not Set') as dd_TemperatureConditionsIndicator,
		ifnull(smt.MARA_MTPOS_MARA, 'Not Set') as dd_GeneralItemCategoryGroup,
		ifnull(smdl.MAKT_SPRAS, 'Not Set') as dd_MaterialDescrLanguageKey,
		ifnull(smdl.MAKT_MAKTX, 'Not Set') as dd_MaterialDescriptionShort,
		ifnull(smdl.MAKT_MAKTG, 'Not Set') as dd_MaterialDescriptionUpperCase
from HUB_MATERIAL hm
		left join SAT_MATERIAL_DATE smd on hm.MARA_MATNR_HSH = smd.MARA_MATNR_HSH
		left join SAT_MATERIAL_NUMBER smn on hm.MARA_MATNR_HSH = smn.MARA_MATNR_HSH
	 	left join SAT_MATERIAL_TEXT smt on hm.MARA_MATNR_HSH = smt.MARA_MATNR_HSH
		left join tmp_SAT_MATERIAL_DESCRIPTION_l1 smdl on hm.MARA_MATNR_HSH = smdl.MAKT_MATNR_HSH
where   smn.MARA_MATNR_LEDTS is null
	AND smt.MARA_MATNR_LEDTS is null
	AND smd.MARA_MATNR_LEDTS is null
	AND smdl.MAKT_MATNR_LEDTS is null;
