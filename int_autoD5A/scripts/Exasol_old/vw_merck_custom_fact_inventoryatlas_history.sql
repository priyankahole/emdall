/*   Script         :    */
/*   Author         : Radu */
/*   Created On     : 07 Mar 2014 */
/*   Description    : Populating script for Merck table fact_inventoryatlashistory   */
/*14 Mar 2014 Radu   1.1  Added the extraction of _valueold_ columns in order to be able to compute history values for aggregated measures */


delete from fact_inventoryatlashistory where snapshotdate = current_date;

insert into fact_inventoryatlashistory 
(
fact_inventoryatlashistoryid,
dim_partid,
dim_plantid,
dim_plantdeliveryid,
dim_mprdateid,
dim_unitofmeasureid,
dim_lastexecutiondateid,
dd_execusion_time,
dd_lastexecution_timestamp,
dim_currencyid,
dim_currencyid_tra,
dim_currencyid_gbl,
amt_exchangerate,
amt_exchangerate_gbl,
amt_partstdprice,
dd_inventory_kpi,
dd_vmi_indicator,
ct_available_qi_stock_coverage,
ct_available_qi_stockqty,
ct_available_stock_coverage,
ct_available_stockqty,
ct_safety_workdays,
ct_safety_workdays_grprocessing_time,
ct_safety_eoqeotfx_workdays_gr_time,
ct_future_demandqty,
ct_custorder_segm_stock,--app-8409
/* 14.05.2015 */
ct_intransitstockqty,
amt_gblStdPrice_Merck,
ct_indepreqqty,
ct_unrestrictedstockqty_merck,
ct_stockintransfer_merck,
ct_stockinqinsp_merck,
ct_totalrestrictedstock_merck,
ct_blockedstock_merck,	
ct_blockedstockreturns_merck,
ct_stockintransit_merck,
ct_minreleasedstockindays_merck,
ct_mintotalstockindays_merck,
ct_mthsofsupplytarget_merck,	
ct_mthsofsupplytargetindays_merck,	
ct_maxstocklevelindays_merck,
ct_lotsizeindays_merck,	
dd_itemkeptonstock_merck,
ct_interplantdemandqty,
dd_sitecodeformos,
ct_sitedemandqty,
dd_itemkeptonstocksite_merck,
ct_lotsizeindayssite_merck,
ct_minreleasedstockindayssite_merck,
ct_mintotalstockindayssite_merck,
ct_mthsofsupplytargetindayssite_merck,
ct_mthsofsupplytargetsite_merck,
ct_maxstocklevelindayssite_merck,
ct_interplantdemandqtypurchdoc,
dd_sitecodeformostitle,
dim_plantidsitefore2e,
ct_lotsize_merck,
ct_lotsizesite_merck,
ct_mosfixedtarget_merck,
/*  end 14.05.2015 */
amt_available_qi_stock,
amt_available_stock,
amt_future_demand,
CT_FUTURE_DEMANDQT,
CT_GRPROCESSTIMECALDAYS,
CT_INDEPREQRESTOFMONTH,
CT_INDEPREQMONTH1,
CT_INDEPREQMONTH2,
CT_INDEPREQMONTH3,
CT_INDEPREQMONTH4,
CT_INDEPREQMONTH5,
CT_INDEPREQMONTH6,
CT_INDEPREQMONTH7,
CT_INDEPREQMONTH8,
CT_INDEPREQMONTH9,
CT_INDEPREQMONTH10,
CT_INDEPREQMONTH11,
AMT_GROSSWEIGHT_MARM,
CT_UNR,
CT_INDE1,
CT_INDE2,
CT_INDE3,
CT_INDE4,
CT_INDE5,
CT_INDE6,
CT_INDE7,
CT_INDE8,
CT_INDE9,
CT_INDE10,
CT_INDE11,
CT_INDERESTOF,
CT_ONHANDQTYIRU,
CT_OPENORDERDEMANDNEXTCALYEAR,
CT_BASELINEVALUE,
CT_BASELINEMOS,
CT_BASELINEMATERIALMOS,
CT_ANTICIPATIONSTOCK,
CT_SITEBASELINEDEMANDCCP,
CT_ONHANDAMOUNTNOTAVAILABLEINMINMAX,
CT_TOTALSTOCKCCP,
CT_TOTALSTOCKIRU,
CT_OTHERNONSAPMRPSTOCKVALUECCP,
CT_MOS_STD_PRICE,
CT_MOS_STD,
CT_MOS_UIN,
CT_ONHANDQTY,
DD_TRAFFICLIGHT,
DD_SITECODEFORMOS_OLD,
DD_ISBATCHMANAGED_MERCK,
CATEGORYCODE,
STD_EXCHANGERATE_DATEID,
snapshotdate,
dim_snapshotdateid,
/*APP-6090 */
ct_iqvprovision_v2,
ct_iqvnoprovision_v2
)
select
ifnull((select max(fact_inventoryatlashistoryid) from fact_inventoryatlashistory),0) + row_number() over (ORDER BY ''),
dim_partid,
f.dim_plantid,
ifnull(dim_plantdeliveryid,1),
ifnull(dim_mprdateid,1),
ifnull(dim_unitofmeasureid,1),
ifnull(dim_lastexecutiondateid,1),
ifnull(dd_execusion_time,'000000'),
ifnull(dd_lastexecution_timestamp,'0001-01-01 00:00:00'),
ifnull(dim_currencyid,1),
ifnull(dim_currencyid_tra,1),
ifnull(dim_currencyid_gbl,1),
ifnull(amt_exchangerate,1),
ifnull(amt_exchangerate_gbl,1),
ifnull(amt_partstdprice,0),
ifnull(dd_inventory_kpi,'Not Set'),
ifnull(dd_vmi_indicator,'Not Set'),
/*computed like this in the UI in source table*/
case when ct_available_qi_stock_coverage >= 0 then ct_available_qi_stock_coverage else 0 end
as ct_available_qi_stock_coverage,
ifnull(ct_available_qi_stockqty,0),
ifnull(ct_available_stock_coverage,0),
ifnull(ct_available_stockqty,0),
ifnull(ct_safety_workdays,0),
ifnull(ct_safety_workdays_grprocessing_time,0),
ifnull(ct_safety_eoqeotfx_workdays_gr_time,0),
ifnull(ct_future_demandqty,0),
ifnull(ct_custorder_segm_stock,0),--app-8409
/* 14.05.2015 */
ct_intransitstockqty,
amt_gblStdPrice_Merck,
ct_indepreqqty,
ct_unrestrictedstockqty_merck,
ct_stockintransfer_merck,
ct_stockinqinsp_merck,
ct_totalrestrictedstock_merck,
ct_blockedstock_merck,	
ct_blockedstockreturns_merck,
ct_stockintransit_merck,
ct_minreleasedstockindays_merck,
ct_mintotalstockindays_merck,
ct_mthsofsupplytarget_merck,	
ct_mthsofsupplytargetindays_merck,	
ct_maxstocklevelindays_merck,
ct_lotsizeindays_merck,	
dd_itemkeptonstock_merck,
ct_interplantdemandqty,
dd_sitecodeformos,
ct_sitedemandqty,
dd_itemkeptonstocksite_merck,
ct_lotsizeindayssite_merck,
ct_minreleasedstockindayssite_merck,
ct_mintotalstockindayssite_merck,
ct_mthsofsupplytargetindayssite_merck,
ct_mthsofsupplytargetsite_merck,
ct_maxstocklevelindayssite_merck,
ct_interplantdemandqtypurchdoc,
dd_sitecodeformostitle,
dim_plantidsitefore2e,
ct_lotsize_merck,
ct_lotsizesite_merck,
ct_mosfixedtarget_merck,
/*  end 14.05.2015 */
ifnull((amt_PartStdPrice*ct_available_qi_stockqty),0) as amt_available_qi_stock,
ifnull((amt_PartStdPrice*ct_available_stockqty),0) as amt_available_stock,
ifnull((amt_PartStdPrice*ct_future_demandqty),0) as amt_future_demand,
ifnull(CT_FUTURE_DEMANDQT,0) as CT_FUTURE_DEMANDQT,
ifnull(CT_GRPROCESSTIMECALDAYS,0) as CT_GRPROCESSTIMECALDAYS,
ifnull(CT_INDEPREQRESTOFMONTH,0) as CT_INDEPREQRESTOFMONTH,
ifnull(CT_INDEPREQMONTH1,0) as CT_INDEPREQMONTH1,
ifnull(CT_INDEPREQMONTH2,0) as CT_INDEPREQMONTH2,
ifnull(CT_INDEPREQMONTH3,0) as CT_INDEPREQMONTH3,
ifnull(CT_INDEPREQMONTH4,0) as CT_INDEPREQMONTH4,
ifnull(CT_INDEPREQMONTH5,0) as CT_INDEPREQMONTH5,
ifnull(CT_INDEPREQMONTH6,0) as CT_INDEPREQMONTH6,
ifnull(CT_INDEPREQMONTH7,0) as CT_INDEPREQMONTH7,
ifnull(CT_INDEPREQMONTH8,0) as CT_INDEPREQMONTH8,
ifnull(CT_INDEPREQMONTH9,0) as CT_INDEPREQMONTH9,
ifnull(CT_INDEPREQMONTH10,0) as CT_INDEPREQMONTH10,
ifnull(CT_INDEPREQMONTH11,0) as CT_INDEPREQMONTH11,
ifnull(AMT_GROSSWEIGHT_MARM,0) as AMT_GROSSWEIGHT_MARM,
ifnull(CT_UNR,0) as CT_UNR,
ifnull(CT_INDE1,0) as CT_INDE1,
ifnull(CT_INDE2,0) as CT_INDE2,
ifnull(CT_INDE3,0) as CT_INDE3,
ifnull(CT_INDE4,0) as CT_INDE4,
ifnull(CT_INDE5,0) as CT_INDE5,
ifnull(CT_INDE6,0) as CT_INDE6,
ifnull(CT_INDE7,0) as CT_INDE7,
ifnull(CT_INDE8,0) as CT_INDE8,
ifnull(CT_INDE9,0) as CT_INDE9,
ifnull(CT_INDE10,0) as CT_INDE10,
ifnull(CT_INDE11,0) as CT_INDE11,
ifnull(CT_INDERESTOF,0) as CT_INDERESTOF,
ifnull(CT_ONHANDQTYIRU,0) as CT_ONHANDQTYIRU,
ifnull(CT_OPENORDERDEMANDNEXTCALYEAR,0) as CT_OPENORDERDEMANDNEXTCALYEAR,
ifnull(CT_BASELINEVALUE,0) as CT_BASELINEVALUE,
ifnull(CT_BASELINEMOS,0) as CT_BASELINEMOS,
ifnull(CT_BASELINEMATERIALMOS,0) as CT_BASELINEMATERIALMOS,
ifnull(CT_ANTICIPATIONSTOCK,0) as CT_ANTICIPATIONSTOCK,
ifnull(CT_SITEBASELINEDEMANDCCP,0) as CT_SITEBASELINEDEMANDCCP,
ifnull(CT_ONHANDAMOUNTNOTAVAILABLEINMINMAX,0) as CT_ONHANDAMOUNTNOTAVAILABLEINMINMAX,
ifnull(CT_TOTALSTOCKCCP,0) as CT_TOTALSTOCKCCP,
ifnull(CT_TOTALSTOCKIRU,0) as CT_TOTALSTOCKIRU,
ifnull(CT_OTHERNONSAPMRPSTOCKVALUECCP,0) as CT_OTHERNONSAPMRPSTOCKVALUECCP,
ifnull(CT_MOS_STD_PRICE,0) as CT_MOS_STD_PRICE,
ifnull(CT_MOS_STD,0) as CT_MOS_STD,
ifnull(CT_MOS_UIN,0) as CT_MOS_UIN,
ifnull(CT_ONHANDQTY,0) as CT_ONHANDQTY,
DD_TRAFFICLIGHT,
DD_SITECODEFORMOS_OLD,
DD_ISBATCHMANAGED_MERCK,
CATEGORYCODE,
STD_EXCHANGERATE_DATEID,
current_date as snapshotdate,
1 as dim_snapshotdateid,
/*APP-6090 */
ct_iqvprovision_v2,
ct_iqvnoprovision_v2
from 
  fact_inventoryatlas f
  inner join dim_plant dp
  	on f.dim_plantid=dp.dim_plantid;

update fact_inventoryatlashistory  a
set a.dim_snapshotdateid = dd.dim_dateid
from fact_inventoryatlashistory  a, dim_Date dd, dim_plant dp
where a.dim_plantid = dp.dim_plantid
  and dd.DateValue = a.snapshotdate
  AND dd.companycode = dp.companycode
  and dd.plantcode_factory=dp.plantcode
  and a.dim_snapshotdateid <> dd.dim_dateid;

/*create temp update tables around dim_part(+dim_plant)+snapshotdate columns as unique key,if this changes,then perform sum of the historic values grouping by history date*/

drop table if exists tmp_fact_inventoryatlashistory_updweekly;

create table tmp_fact_inventoryatlashistory_updweekly as
select DISTINCT
	fcur.dim_partid,
	fcur.dim_plantid,
	fcur.snapshotdate,
	ifnull((fcur.ct_available_qi_stock_coverage-fhist_week.ct_available_qi_stock_coverage),0) as ct_available_qi_stock_coverage_1WeekChange,
	ifnull((fcur.ct_available_qi_stockqty-fhist_week.ct_available_qi_stockqty),0) as ct_available_qi_stockqty_1WeekChange,
	ifnull((fcur.ct_available_stock_coverage-fhist_week.ct_available_stock_coverage),0) as ct_available_stock_coverage_1WeekChange,
	ifnull((fcur.ct_available_stockqty-fhist_week.ct_available_stockqty),0) as ct_available_stockqty_1WeekChange,
	ifnull((fcur.ct_safety_workdays-fhist_week.ct_safety_workdays),0) as ct_safety_workdays_1WeekChange,
	ifnull((fcur.ct_safety_workdays_grprocessing_time-fhist_week.ct_safety_workdays_grprocessing_time),0) as ct_safety_workdays_grprocessing_time_1WeekChange,
	ifnull((fcur.ct_safety_eoqeotfx_workdays_gr_time-fhist_week.ct_safety_eoqeotfx_workdays_gr_time),0) as ct_safety_eoqeotfx_workdays_gr_time_1WeekChange,
	ifnull((fcur.ct_future_demandqty-fhist_week.ct_future_demandqty),0) as ct_future_demandqty_1WeekChange,
	ifnull((fcur.amt_available_qi_stock-fhist_week.amt_available_qi_stock),0) as amt_available_qi_stock_1WeekChange,
	ifnull((fcur.amt_available_stock-fhist_week.amt_available_stock),0) as amt_available_stock_1WeekChange,
	ifnull((fcur.amt_future_demand-fhist_week.amt_future_demand),0) as amt_future_demand_1WeekChange,
    ifnull(fhist_week.ct_available_qi_stock_coverage,0) as ct_available_qi_stock_coverage_1weekold_value,
	ifnull(fhist_week.ct_safety_eoqeotfx_workdays_gr_time,0) as ct_safety_eoqeotfx_workdays_gr_time_1weekold_value,
	ifnull(fhist_week.ct_safety_workdays_grprocessing_time,0) as ct_safety_workdays_grprocessing_time_1weekold_value,
	ifnull(fhist_week.ct_available_stockqty,0) as ct_available_stockqty_1weekold_value,
	ifnull(fhist_week.ct_future_demandqty,0) as ct_future_demandqty_1weekold_value,
	/* 14.05.2015 */
	ifnull((fcur.ct_intransitstockqty-fhist_week.ct_intransitstockqty),0) as ct_intransitstockqty_1weekChange,
	ifnull((fcur.ct_indepreqqty-fhist_week.ct_indepreqqty),0) as ct_indepreqqty_1weekChange,
	ifnull((fcur.ct_unrestrictedstockqty_merck-fhist_week.ct_unrestrictedstockqty_merck),0) as ct_unrestrictedstockqty_merck_1weekChange,
	ifnull((fcur.ct_stockintransfer_merck-fhist_week.ct_stockintransfer_merck),0) as ct_stockintransfer_merck_1weekChange,
	ifnull((fcur.ct_stockinqinsp_merck-fhist_week.ct_stockinqinsp_merck),0) as ct_stockinqinsp_merck_1weekChange,
	ifnull((fcur.ct_totalrestrictedstock_merck-fhist_week.ct_totalrestrictedstock_merck),0) as ct_totalrestrictedstock_merck_1weekChange,
	ifnull((fcur.ct_blockedstock_merck-fhist_week.ct_blockedstock_merck),0) as ct_blockedstock_merck_1weekChange,
	ifnull((fcur.ct_blockedstockreturns_merck-fhist_week.ct_blockedstockreturns_merck),0) as ct_blockedstockreturns_merck_1weekChange,
	ifnull((fcur.ct_stockintransit_merck-fhist_week.ct_stockintransit_merck),0) as ct_stockintransit_merck_1weekChange,
	ifnull((fcur.ct_minreleasedstockindays_merck-fhist_week.ct_minreleasedstockindays_merck),0) as ct_minreleasedstockindays_merck_1weekChange,
	ifnull((fcur.ct_mintotalstockindays_merck-fhist_week.ct_mintotalstockindays_merck),0) as ct_mintotalstockindays_merck_1weekChange,
	ifnull((fcur.ct_mthsofsupplytarget_merck-fhist_week.ct_mthsofsupplytarget_merck),0) as ct_mthsofsupplytarget_merck_1weekChange,
	ifnull((fcur.ct_mthsofsupplytargetindays_merck-fhist_week.ct_mthsofsupplytargetindays_merck),0) as ct_mthsofsupplytargetindays_merck_1weekChange,
	ifnull((fcur.ct_maxstocklevelindays_merck-fhist_week.ct_maxstocklevelindays_merck),0) as ct_maxstocklevelindays_merck_1weekChange,
	ifnull((fcur.ct_lotsizeindays_merck-fhist_week.ct_lotsizeindays_merck),0) as ct_lotsizeindays_merck_1weekChange,
	ifnull((fcur.ct_interplantdemandqty-fhist_week.ct_interplantdemandqty),0) as ct_interplantdemandqty_1weekChange,
	ifnull((fcur.ct_sitedemandqty-fhist_week.ct_sitedemandqty),0) as ct_sitedemandqty_1weekChange,
	ifnull((fcur.ct_lotsizeindayssite_merck-fhist_week.ct_lotsizeindayssite_merck),0) as ct_lotsizeindayssite_merck_1weekChange,
	ifnull((fcur.ct_minreleasedstockindayssite_merck-fhist_week.ct_minreleasedstockindayssite_merck),0) as ct_minreleasedstockindayssite_merck_1weekChange,
	ifnull((fcur.ct_mintotalstockindayssite_merck-fhist_week.ct_mintotalstockindayssite_merck),0) as ct_mintotalstockindayssite_merck_1weekChange,
	ifnull((fcur.ct_mthsofsupplytargetindayssite_merck-fhist_week.ct_mthsofsupplytargetindayssite_merck),0) as ct_mthsofsupplytargetindayssite_merck_1weekChange,
	ifnull((fcur.ct_mthsofsupplytargetsite_merck-fhist_week.ct_mthsofsupplytargetsite_merck),0) as ct_mthsofsupplytargetsite_merck_1weekChange,
	ifnull((fcur.ct_maxstocklevelindayssite_merck-fhist_week.ct_maxstocklevelindayssite_merck),0) as ct_maxstocklevelindayssite_merck_1weekChange,
	ifnull((fcur.ct_interplantdemandqtypurchdoc-fhist_week.ct_interplantdemandqtypurchdoc),0) as ct_interplantdemandqtypurchdoc_1weekChange,
	ifnull((fcur.ct_lotsize_merck-fhist_week.ct_lotsize_merck),0) as ct_lotsize_merck_1weekChange,
	ifnull((fcur.ct_lotsizesite_merck-fhist_week.ct_lotsizesite_merck),0) as ct_lotsizesite_merck_1weekChange,
	ifnull((fcur.ct_mosfixedtarget_merck-fhist_week.ct_mosfixedtarget_merck),0) as ct_mosfixedtarget_merck_1weekChange
/*  end 14.05.2015 */
from 
	fact_inventoryatlashistory fcur
	left outer join fact_inventoryatlashistory  fhist_week
		on fcur.dim_partid=fhist_week.dim_partid
		and fcur.dim_plantid=fhist_week.dim_plantid
		and fcur.snapshotdate=(fhist_week.snapshotdate + INTERVAL '7' DAY)
/*comment where clause if full recomputation is needed*/
where
fcur.snapshotdate=current_date
and fcur.dim_partid not in (select dim_partid from dim_part where partnumber='XXXXXX');


 
 merge into fact_inventoryatlashistory ftrg
using 
(
select ftrg.dim_partid,ftrg.dim_plantid,ftrg.snapshotdate,
max(ftmp.ct_available_qi_stock_coverage_1WeekChange) as ct_available_qi_stock_coverage_1WeekChange,
max(ftmp.ct_available_qi_stockqty_1WeekChange) as ct_available_qi_stockqty_1WeekChange,
max(ftmp.ct_available_stock_coverage_1WeekChange)
as ct_available_stock_coverage_1WeekChange,max(ftmp.ct_available_stockqty_1WeekChange) as ct_available_stockqty_1WeekChange,
max(ftmp.ct_safety_workdays_1WeekChange) as ct_safety_workdays_1WeekChange,max(ftmp.ct_safety_workdays_grprocessing_time_1WeekChange) as ct_safety_workdays_grprocessing_time_1WeekChange,
max(ftmp.ct_safety_eoqeotfx_workdays_gr_time_1WeekChange) as ct_safety_eoqeotfx_workdays_gr_time_1WeekChange,max(ftmp.ct_future_demandqty_1WeekChange) as ct_future_demandqty_1WeekChange,
max(ftmp.amt_available_qi_stock_1WeekChange) as amt_available_qi_stock_1WeekChange,max(ftmp.amt_available_stock_1WeekChange) as amt_available_stock_1WeekChange,
max(ftmp.amt_future_demand_1WeekChange) as amt_future_demand_1WeekChange,max(ftmp.ct_available_qi_stock_coverage_1weekold_value) as ct_available_qi_stock_coverage_1weekold_value,
max(ftmp.ct_safety_eoqeotfx_workdays_gr_time_1weekold_value) as ct_safety_eoqeotfx_workdays_gr_time_1weekold_value,max(ftmp.ct_safety_workdays_grprocessing_time_1weekold_value) as ct_safety_workdays_grprocessing_time_1weekold_value,
max(ftmp.ct_available_stockqty_1weekold_value) as ct_available_stockqty_1weekold_value,max(ftmp.ct_future_demandqty_1weekold_value) as ct_future_demandqty_1weekold_value,
max(ftmp.ct_intransitstockqty_1weekChange) as ct_intransitstockqty_1weekChange, max(ftmp.ct_indepreqqty_1weekChange) as ct_indepreqqty_1weekChange,
max(ftmp.ct_unrestrictedstockqty_merck_1weekChange) as ct_unrestrictedstockqty_merck_1weekChange,max(ftmp.ct_stockintransfer_merck_1weekChange) as ct_stockintransfer_merck_1weekChange,
max(ftmp.ct_stockinqinsp_merck_1weekChange) as ct_stockinqinsp_merck_1weekChange,max(ftmp.ct_totalrestrictedstock_merck_1weekChange) as ct_totalrestrictedstock_merck_1weekChange,
max(ftmp.ct_blockedstock_merck_1weekChange) as ct_blockedstock_merck_1weekChange,max(ftmp.ct_blockedstockreturns_merck_1weekChange) as ct_blockedstockreturns_merck_1weekChange,
max(ftmp.ct_stockintransit_merck_1weekChange) as ct_stockintransit_merck_1weekChange,max(ftmp.ct_minreleasedstockindays_merck_1weekChange) as ct_minreleasedstockindays_merck_1weekChange,
max(ftmp.ct_mintotalstockindays_merck_1weekChange) as ct_mintotalstockindays_merck_1weekChange,max(ftmp.ct_mthsofsupplytarget_merck_1weekChange) as ct_mthsofsupplytarget_merck_1weekChange,
max(ftmp.ct_mthsofsupplytargetindays_merck_1weekChange) as ct_mthsofsupplytargetindays_merck_1weekChange,max(ftmp.ct_maxstocklevelindays_merck_1weekChange) as ct_maxstocklevelindays_merck_1weekChange,
max(ftmp.ct_lotsizeindays_merck_1weekChange) as ct_lotsizeindays_merck_1weekChange,max(ftmp.ct_interplantdemandqty_1weekChange) as ct_interplantdemandqty_1weekChange,
max(ftmp.ct_sitedemandqty_1weekChange) as ct_sitedemandqty_1weekChange,max(ftmp.ct_lotsizeindayssite_merck_1weekChange) as ct_lotsizeindayssite_merck_1weekChange,
max(ftmp.ct_minreleasedstockindayssite_merck_1weekChange) as ct_minreleasedstockindayssite_merck_1weekChange,max(ftmp.ct_mintotalstockindayssite_merck_1weekChange) as ct_mintotalstockindayssite_merck_1weekChange,
max(ftmp.ct_mthsofsupplytargetindayssite_merck_1weekChange) as ct_mthsofsupplytargetindayssite_merck_1weekChange,max(ftmp.ct_mthsofsupplytargetsite_merck_1weekChange) as ct_mthsofsupplytargetsite_merck_1weekChange,
max(ftmp.ct_maxstocklevelindayssite_merck_1weekChange) as ct_maxstocklevelindayssite_merck_1weekChange,max(ftmp.ct_interplantdemandqtypurchdoc_1weekChange) as ct_interplantdemandqtypurchdoc_1weekChange,
max(ftmp.ct_lotsize_merck_1weekChange) as ct_lotsize_merck_1weekChange,max(ftmp.ct_lotsizesite_merck_1weekChange) as ct_lotsizesite_merck_1weekChange,
max(ftmp.ct_mosfixedtarget_merck_1weekChange) as ct_mosfixedtarget_merck_1weekChange
from tmp_fact_inventoryatlashistory_updweekly ftmp, fact_inventoryatlashistory ftrg
 where ftrg.dim_partid=ftmp.dim_partid 
 and ftrg.dim_plantid=ftmp.dim_plantid 
 and ftrg.snapshotdate=ftmp.snapshotdate 
 /*comment where clause if full re-computation is needed,in normal runs it is redundant*/
 and ftrg.snapshotdate=current_date
group by ftrg.dim_partid,ftrg.dim_plantid,ftrg.snapshotdate)x
on (ftrg.dim_partid=x.dim_partid
and ftrg.dim_plantid=x.dim_plantid
and ftrg.snapshotdate=x.snapshotdate)
when matched then update 
set ftrg.ct_available_qi_stock_coverage_1WeekChange=x.ct_available_qi_stock_coverage_1WeekChange,
ftrg.ct_available_qi_stockqty_1WeekChange=x.ct_available_qi_stockqty_1WeekChange,
ftrg.ct_available_stock_coverage_1WeekChange=x.ct_available_stock_coverage_1WeekChange,
ftrg.ct_available_stockqty_1WeekChange=x.ct_available_stockqty_1WeekChange,
ftrg.ct_safety_workdays_1WeekChange=x.ct_safety_workdays_1WeekChange,
ftrg.ct_safety_workdays_grprocessing_time_1WeekChange=x.ct_safety_workdays_grprocessing_time_1WeekChange,
ftrg.ct_safety_eoqeotfx_workdays_gr_time_1WeekChange=x.ct_safety_eoqeotfx_workdays_gr_time_1WeekChange,
ftrg.ct_future_demandqty_1WeekChange=x.ct_future_demandqty_1WeekChange,
ftrg.amt_available_qi_stock_1WeekChange=x.amt_available_qi_stock_1WeekChange,
ftrg.amt_available_stock_1WeekChange=x.amt_available_stock_1WeekChange,
ftrg.amt_future_demand_1WeekChange=x.amt_future_demand_1WeekChange,
ftrg.ct_available_qi_stock_coverage_1weekold_value=x.ct_available_qi_stock_coverage_1weekold_value,
ftrg.ct_safety_eoqeotfx_workdays_gr_time_1weekold_value=x.ct_safety_eoqeotfx_workdays_gr_time_1weekold_value,
ftrg.ct_safety_workdays_grprocessing_time_1weekold_value=x.ct_safety_workdays_grprocessing_time_1weekold_value,
ftrg.ct_available_stockqty_1weekold_value=x.ct_available_stockqty_1weekold_value,
ftrg.ct_future_demandqty_1weekold_value=x.ct_future_demandqty_1weekold_value,
ftrg.ct_intransitstockqty_1weekChange=x.ct_intransitstockqty_1weekChange,
ftrg.ct_indepreqqty_1weekChange=x.ct_indepreqqty_1weekChange,
ftrg.ct_unrestrictedstockqty_merck_1weekChange=x.ct_unrestrictedstockqty_merck_1weekChange,
ftrg.ct_stockintransfer_merck_1weekChange=x.ct_stockintransfer_merck_1weekChange,
ftrg.ct_stockinqinsp_merck_1weekChange=x.ct_stockinqinsp_merck_1weekChange,
ftrg.ct_totalrestrictedstock_merck_1weekChange=x.ct_totalrestrictedstock_merck_1weekChange,
ftrg.ct_blockedstock_merck_1weekChange=x.ct_blockedstock_merck_1weekChange,
ftrg.ct_blockedstockreturns_merck_1weekChange=x.ct_blockedstockreturns_merck_1weekChange,
ftrg.ct_stockintransit_merck_1weekChange=x.ct_stockintransit_merck_1weekChange,
ftrg.ct_minreleasedstockindays_merck_1weekChange=x.ct_minreleasedstockindays_merck_1weekChange,
ftrg.ct_mintotalstockindays_merck_1weekChange=x.ct_mintotalstockindays_merck_1weekChange,
ftrg.ct_mthsofsupplytarget_merck_1weekChange=x.ct_mthsofsupplytarget_merck_1weekChange,
ftrg.ct_mthsofsupplytargetindays_merck_1weekChange=x.ct_mthsofsupplytargetindays_merck_1weekChange,
ftrg.ct_maxstocklevelindays_merck_1weekChange=x.ct_maxstocklevelindays_merck_1weekChange,
ftrg.ct_lotsizeindays_merck_1weekChange=x.ct_lotsizeindays_merck_1weekChange,
ftrg.ct_interplantdemandqty_1weekChange=x.ct_interplantdemandqty_1weekChange,
ftrg.ct_sitedemandqty_1weekChange=x.ct_sitedemandqty_1weekChange,
ftrg.ct_lotsizeindayssite_merck_1weekChange=x.ct_lotsizeindayssite_merck_1weekChange,
ftrg.ct_minreleasedstockindayssite_merck_1weekChange=x.ct_minreleasedstockindayssite_merck_1weekChange,
ftrg.ct_mintotalstockindayssite_merck_1weekChange=x.ct_mintotalstockindayssite_merck_1weekChange,
ftrg.ct_mthsofsupplytargetindayssite_merck_1weekChange=x.ct_mthsofsupplytargetindayssite_merck_1weekChange,
ftrg.ct_mthsofsupplytargetsite_merck_1weekChange=x.ct_mthsofsupplytargetsite_merck_1weekChange,
ftrg.ct_maxstocklevelindayssite_merck_1weekChange=x.ct_maxstocklevelindayssite_merck_1weekChange,
ftrg.ct_interplantdemandqtypurchdoc_1weekChange=x.ct_interplantdemandqtypurchdoc_1weekChange,
ftrg.ct_lotsize_merck_1weekChange=x.ct_lotsize_merck_1weekChange,
ftrg.ct_lotsizesite_merck_1weekChange=x.ct_lotsizesite_merck_1weekChange,
ftrg.ct_mosfixedtarget_merck_1weekChange=x.ct_mosfixedtarget_merck_1weekChange;


/*WEEKLY PART END*/


drop table if exists tmp_fact_inventoryatlashistory_updmonthly;

create table tmp_fact_inventoryatlashistory_updmonthly as
select
	fcur.dim_partid,
	fcur.dim_plantid,
	fcur.snapshotdate,
	ifnull((fcur.ct_available_qi_stock_coverage-fhist_month.ct_available_qi_stock_coverage),0) as ct_available_qi_stock_coverage_1MonthChange,
	ifnull((fcur.ct_available_qi_stockqty-fhist_month.ct_available_qi_stockqty),0) as ct_available_qi_stockqty_1MonthChange,
	ifnull((fcur.ct_available_stock_coverage-fhist_month.ct_available_stock_coverage),0) as ct_available_stock_coverage_1MonthChange,
	ifnull((fcur.ct_available_stockqty-fhist_month.ct_available_stockqty),0) as ct_available_stockqty_1MonthChange,
	ifnull((fcur.ct_safety_workdays-fhist_month.ct_safety_workdays),0) as ct_safety_workdays_1MonthChange,
	ifnull((fcur.ct_safety_workdays_grprocessing_time-fhist_month.ct_safety_workdays_grprocessing_time),0) as ct_safety_workdays_grprocessing_time_1MonthChange,
	ifnull((fcur.ct_safety_eoqeotfx_workdays_gr_time-fhist_month.ct_safety_eoqeotfx_workdays_gr_time),0) as ct_safety_eoqeotfx_workdays_gr_time_1MonthChange,
	ifnull((fcur.ct_future_demandqty-fhist_month.ct_future_demandqty),0) as ct_future_demandqty_1MonthChange,
	ifnull((fcur.amt_available_qi_stock-fhist_month.amt_available_qi_stock),0) as amt_available_qi_stock_1MonthChange,
	ifnull((fcur.amt_available_stock-fhist_month.amt_available_stock),0) as amt_available_stock_1MonthChange,
	ifnull((fcur.amt_future_demand-fhist_month.amt_future_demand),0) as amt_future_demand_1MonthChange,
	ifnull(fhist_month.ct_available_qi_stock_coverage,0) as ct_available_qi_stock_coverage_1monthold_value,
	ifnull(fhist_month.ct_safety_eoqeotfx_workdays_gr_time,0) as ct_safety_eoqeotfx_workdays_gr_time_1monthold_value,
	ifnull(fhist_month.ct_safety_workdays_grprocessing_time,0) as ct_safety_workdays_grprocessing_time_1monthold_value,
	ifnull(fhist_month.ct_available_stockqty,0) as ct_available_stockqty_1monthold_value,
	ifnull(fhist_month.ct_future_demandqty,0) as ct_future_demandqty_1monthold_value,
/* 14.05.2015 */
	ifnull((fcur.ct_intransitstockqty-fhist_month.ct_intransitstockqty),0) as ct_intransitstockqty_1monthChange,
	ifnull((fcur.ct_indepreqqty-fhist_month.ct_indepreqqty),0) as ct_indepreqqty_1monthChange,
	ifnull((fcur.ct_unrestrictedstockqty_merck-fhist_month.ct_unrestrictedstockqty_merck),0) as ct_unrestrictedstockqty_merck_1monthChange,
	ifnull((fcur.ct_stockintransfer_merck-fhist_month.ct_stockintransfer_merck),0) as ct_stockintransfer_merck_1monthChange,
	ifnull((fcur.ct_stockinqinsp_merck-fhist_month.ct_stockinqinsp_merck),0) as ct_stockinqinsp_merck_1monthChange,
	ifnull((fcur.ct_totalrestrictedstock_merck-fhist_month.ct_totalrestrictedstock_merck),0) as ct_totalrestrictedstock_merck_1monthChange,
	ifnull((fcur.ct_blockedstock_merck-fhist_month.ct_blockedstock_merck),0) as ct_blockedstock_merck_1monthChange,
	ifnull((fcur.ct_blockedstockreturns_merck-fhist_month.ct_blockedstockreturns_merck),0) as ct_blockedstockreturns_merck_1monthChange,
	ifnull((fcur.ct_stockintransit_merck-fhist_month.ct_stockintransit_merck),0) as ct_stockintransit_merck_1monthChange,
	ifnull((fcur.ct_minreleasedstockindays_merck-fhist_month.ct_minreleasedstockindays_merck),0) as ct_minreleasedstockindays_merck_1monthChange,
	ifnull((fcur.ct_mintotalstockindays_merck-fhist_month.ct_mintotalstockindays_merck),0) as ct_mintotalstockindays_merck_1monthChange,
	ifnull((fcur.ct_mthsofsupplytarget_merck-fhist_month.ct_mthsofsupplytarget_merck),0) as ct_mthsofsupplytarget_merck_1monthChange,
	ifnull((fcur.ct_mthsofsupplytargetindays_merck-fhist_month.ct_mthsofsupplytargetindays_merck),0) as ct_mthsofsupplytargetindays_merck_1monthChange,
	ifnull((fcur.ct_maxstocklevelindays_merck-fhist_month.ct_maxstocklevelindays_merck),0) as ct_maxstocklevelindays_merck_1monthChange,
	ifnull((fcur.ct_lotsizeindays_merck-fhist_month.ct_lotsizeindays_merck),0) as ct_lotsizeindays_merck_1monthChange,
	ifnull((fcur.ct_interplantdemandqty-fhist_month.ct_interplantdemandqty),0) as ct_interplantdemandqty_1monthChange,
	ifnull((fcur.ct_sitedemandqty-fhist_month.ct_sitedemandqty),0) as ct_sitedemandqty_1monthChange,
	ifnull((fcur.ct_lotsizeindayssite_merck-fhist_month.ct_lotsizeindayssite_merck),0) as ct_lotsizeindayssite_merck_1monthChange,
	ifnull((fcur.ct_minreleasedstockindayssite_merck-fhist_month.ct_minreleasedstockindayssite_merck),0) as ct_minreleasedstockindayssite_merck_1monthChange,
	ifnull((fcur.ct_mintotalstockindayssite_merck-fhist_month.ct_mintotalstockindayssite_merck),0) as ct_mintotalstockindayssite_merck_1monthChange,
	ifnull((fcur.ct_mthsofsupplytargetindayssite_merck-fhist_month.ct_mthsofsupplytargetindayssite_merck),0) as ct_mthsofsupplytargetindayssite_merck_1monthChange,
	ifnull((fcur.ct_mthsofsupplytargetsite_merck-fhist_month.ct_mthsofsupplytargetsite_merck),0) as ct_mthsofsupplytargetsite_merck_1monthChange,
	ifnull((fcur.ct_maxstocklevelindayssite_merck-fhist_month.ct_maxstocklevelindayssite_merck),0) as ct_maxstocklevelindayssite_merck_1monthChange,
	ifnull((fcur.ct_interplantdemandqtypurchdoc-fhist_month.ct_interplantdemandqtypurchdoc),0) as ct_interplantdemandqtypurchdoc_1monthChange,
	ifnull((fcur.ct_lotsize_merck-fhist_month.ct_lotsize_merck),0) as ct_lotsize_merck_1monthChange,
	ifnull((fcur.ct_lotsizesite_merck-fhist_month.ct_lotsizesite_merck),0) as ct_lotsizesite_merck_1monthChange,
	ifnull((fcur.ct_mosfixedtarget_merck-fhist_month.ct_mosfixedtarget_merck),0) as ct_mosfixedtarget_merck_1monthChange
/*  end 14.05.2015 */
from 
	fact_inventoryatlashistory fcur
	left outer join fact_inventoryatlashistory  fhist_month
		on fcur.dim_partid=fhist_month.dim_partid
		and fcur.dim_plantid=fhist_month.dim_plantid
		and fcur.snapshotdate=(fhist_month.snapshotdate + INTERVAL '1' MONTH)
/*comment where clause if full re-computation is needed*/
where
fcur.snapshotdate=current_date
and fcur.dim_partid not in (select dim_partid from dim_part where partnumber='XXXXXX');

merge into fact_inventoryatlashistory ftrg
using 
(
select ftrg.dim_partid,ftrg.dim_plantid,ftrg.snapshotdate,
max(ftmp.ct_available_qi_stock_coverage_1monthChange) as ct_available_qi_stock_coverage_1monthChange,
max(ftmp.ct_available_qi_stockqty_1monthChange) as ct_available_qi_stockqty_1monthChange,
max(ftmp.ct_available_stock_coverage_1monthChange)
as ct_available_stock_coverage_1monthChange,max(ftmp.ct_available_stockqty_1monthChange) as ct_available_stockqty_1monthChange,
max(ftmp.ct_safety_workdays_1monthChange) as ct_safety_workdays_1monthChange,max(ftmp.ct_safety_workdays_grprocessing_time_1monthChange) as ct_safety_workdays_grprocessing_time_1monthChange,
max(ftmp.ct_safety_eoqeotfx_workdays_gr_time_1monthChange) as ct_safety_eoqeotfx_workdays_gr_time_1monthChange,max(ftmp.ct_future_demandqty_1monthChange) as ct_future_demandqty_1monthChange,
max(ftmp.amt_available_qi_stock_1monthChange) as amt_available_qi_stock_1monthChange,max(ftmp.amt_available_stock_1monthChange) as amt_available_stock_1monthChange,
max(ftmp.amt_future_demand_1monthChange) as amt_future_demand_1monthChange,max(ftmp.ct_available_qi_stock_coverage_1monthold_value) as ct_available_qi_stock_coverage_1monthold_value,
max(ftmp.ct_safety_eoqeotfx_workdays_gr_time_1monthold_value) as ct_safety_eoqeotfx_workdays_gr_time_1monthold_value,max(ftmp.ct_safety_workdays_grprocessing_time_1monthold_value) as ct_safety_workdays_grprocessing_time_1monthold_value,
max(ftmp.ct_available_stockqty_1monthold_value) as ct_available_stockqty_1monthold_value,max(ftmp.ct_future_demandqty_1monthold_value) as ct_future_demandqty_1monthold_value,
max(ftmp.ct_intransitstockqty_1monthChange) as ct_intransitstockqty_1monthChange, max(ftmp.ct_indepreqqty_1monthChange) as ct_indepreqqty_1monthChange,
max(ftmp.ct_unrestrictedstockqty_merck_1monthChange) as ct_unrestrictedstockqty_merck_1monthChange,max(ftmp.ct_stockintransfer_merck_1monthChange) as ct_stockintransfer_merck_1monthChange,
max(ftmp.ct_stockinqinsp_merck_1monthChange) as ct_stockinqinsp_merck_1monthChange,max(ftmp.ct_totalrestrictedstock_merck_1monthChange) as ct_totalrestrictedstock_merck_1monthChange,
max(ftmp.ct_blockedstock_merck_1monthChange) as ct_blockedstock_merck_1monthChange,max(ftmp.ct_blockedstockreturns_merck_1monthChange) as ct_blockedstockreturns_merck_1monthChange,
max(ftmp.ct_stockintransit_merck_1monthChange) as ct_stockintransit_merck_1monthChange,max(ftmp.ct_minreleasedstockindays_merck_1monthChange) as ct_minreleasedstockindays_merck_1monthChange,
max(ftmp.ct_mintotalstockindays_merck_1monthChange) as ct_mintotalstockindays_merck_1monthChange,max(ftmp.ct_mthsofsupplytarget_merck_1monthChange) as ct_mthsofsupplytarget_merck_1monthChange,
max(ftmp.ct_mthsofsupplytargetindays_merck_1monthChange) as ct_mthsofsupplytargetindays_merck_1monthChange,max(ftmp.ct_maxstocklevelindays_merck_1monthChange) as ct_maxstocklevelindays_merck_1monthChange,
max(ftmp.ct_lotsizeindays_merck_1monthChange) as ct_lotsizeindays_merck_1monthChange,max(ftmp.ct_interplantdemandqty_1monthChange) as ct_interplantdemandqty_1monthChange,
max(ftmp.ct_sitedemandqty_1monthChange) as ct_sitedemandqty_1monthChange,max(ftmp.ct_lotsizeindayssite_merck_1monthChange) as ct_lotsizeindayssite_merck_1monthChange,
max(ftmp.ct_minreleasedstockindayssite_merck_1monthChange) as ct_minreleasedstockindayssite_merck_1monthChange,max(ftmp.ct_mintotalstockindayssite_merck_1monthChange) as ct_mintotalstockindayssite_merck_1monthChange,
max(ftmp.ct_mthsofsupplytargetindayssite_merck_1monthChange) as ct_mthsofsupplytargetindayssite_merck_1monthChange,max(ftmp.ct_mthsofsupplytargetsite_merck_1monthChange) as ct_mthsofsupplytargetsite_merck_1monthChange,
max(ftmp.ct_maxstocklevelindayssite_merck_1monthChange) as ct_maxstocklevelindayssite_merck_1monthChange,max(ftmp.ct_interplantdemandqtypurchdoc_1monthChange) as ct_interplantdemandqtypurchdoc_1monthChange,
max(ftmp.ct_lotsize_merck_1monthChange) as ct_lotsize_merck_1monthChange,max(ftmp.ct_lotsizesite_merck_1monthChange) as ct_lotsizesite_merck_1monthChange,
max(ftmp.ct_mosfixedtarget_merck_1monthChange) as ct_mosfixedtarget_merck_1monthChange
from tmp_fact_inventoryatlashistory_updmonthly ftmp, fact_inventoryatlashistory ftrg
 where ftrg.dim_partid=ftmp.dim_partid 
 and ftrg.dim_plantid=ftmp.dim_plantid 
 and ftrg.snapshotdate=ftmp.snapshotdate 
 /*comment where clause if full re-computation is needed,in normal runs it is redundant*/
 and ftrg.snapshotdate=current_date
group by ftrg.dim_partid,ftrg.dim_plantid,ftrg.snapshotdate)x
on (ftrg.dim_partid=x.dim_partid
and ftrg.dim_plantid=x.dim_plantid
and ftrg.snapshotdate=x.snapshotdate)
when matched then update 
set ftrg.ct_available_qi_stock_coverage_1monthChange=x.ct_available_qi_stock_coverage_1monthChange,
ftrg.ct_available_qi_stockqty_1monthChange=x.ct_available_qi_stockqty_1monthChange,
ftrg.ct_available_stock_coverage_1monthChange=x.ct_available_stock_coverage_1monthChange,
ftrg.ct_available_stockqty_1monthChange=x.ct_available_stockqty_1monthChange,
ftrg.ct_safety_workdays_1monthChange=x.ct_safety_workdays_1monthChange,
ftrg.ct_safety_workdays_grprocessing_time_1monthChange=x.ct_safety_workdays_grprocessing_time_1monthChange,
ftrg.ct_safety_eoqeotfx_workdays_gr_time_1monthChange=x.ct_safety_eoqeotfx_workdays_gr_time_1monthChange,
ftrg.ct_future_demandqty_1monthChange=x.ct_future_demandqty_1monthChange,
ftrg.amt_available_qi_stock_1monthChange=x.amt_available_qi_stock_1monthChange,
ftrg.amt_available_stock_1monthChange=x.amt_available_stock_1monthChange,
ftrg.amt_future_demand_1monthChange=x.amt_future_demand_1monthChange,
ftrg.ct_available_qi_stock_coverage_1monthold_value=x.ct_available_qi_stock_coverage_1monthold_value,
ftrg.ct_safety_eoqeotfx_workdays_gr_time_1monthold_value=x.ct_safety_eoqeotfx_workdays_gr_time_1monthold_value,
ftrg.ct_safety_workdays_grprocessing_time_1monthold_value=x.ct_safety_workdays_grprocessing_time_1monthold_value,
ftrg.ct_available_stockqty_1monthold_value=x.ct_available_stockqty_1monthold_value,
ftrg.ct_future_demandqty_1monthold_value=x.ct_future_demandqty_1monthold_value,
ftrg.ct_intransitstockqty_1monthChange=x.ct_intransitstockqty_1monthChange,
ftrg.ct_indepreqqty_1monthChange=x.ct_indepreqqty_1monthChange,
ftrg.ct_unrestrictedstockqty_merck_1monthChange=x.ct_unrestrictedstockqty_merck_1monthChange,
ftrg.ct_stockintransfer_merck_1monthChange=x.ct_stockintransfer_merck_1monthChange,
ftrg.ct_stockinqinsp_merck_1monthChange=x.ct_stockinqinsp_merck_1monthChange,
ftrg.ct_totalrestrictedstock_merck_1monthChange=x.ct_totalrestrictedstock_merck_1monthChange,
ftrg.ct_blockedstock_merck_1monthChange=x.ct_blockedstock_merck_1monthChange,
ftrg.ct_blockedstockreturns_merck_1monthChange=x.ct_blockedstockreturns_merck_1monthChange,
ftrg.ct_stockintransit_merck_1monthChange=x.ct_stockintransit_merck_1monthChange,
ftrg.ct_minreleasedstockindays_merck_1monthChange=x.ct_minreleasedstockindays_merck_1monthChange,
ftrg.ct_mintotalstockindays_merck_1monthChange=x.ct_mintotalstockindays_merck_1monthChange,
ftrg.ct_mthsofsupplytarget_merck_1monthChange=x.ct_mthsofsupplytarget_merck_1monthChange,
ftrg.ct_mthsofsupplytargetindays_merck_1monthChange=x.ct_mthsofsupplytargetindays_merck_1monthChange,
ftrg.ct_maxstocklevelindays_merck_1monthChange=x.ct_maxstocklevelindays_merck_1monthChange,
ftrg.ct_lotsizeindays_merck_1monthChange=x.ct_lotsizeindays_merck_1monthChange,
ftrg.ct_interplantdemandqty_1monthChange=x.ct_interplantdemandqty_1monthChange,
ftrg.ct_sitedemandqty_1monthChange=x.ct_sitedemandqty_1monthChange,
ftrg.ct_lotsizeindayssite_merck_1monthChange=x.ct_lotsizeindayssite_merck_1monthChange,
ftrg.ct_minreleasedstockindayssite_merck_1monthChange=x.ct_minreleasedstockindayssite_merck_1monthChange,
ftrg.ct_mintotalstockindayssite_merck_1monthChange=x.ct_mintotalstockindayssite_merck_1monthChange,
ftrg.ct_mthsofsupplytargetindayssite_merck_1monthChange=x.ct_mthsofsupplytargetindayssite_merck_1monthChange,
ftrg.ct_mthsofsupplytargetsite_merck_1monthChange=x.ct_mthsofsupplytargetsite_merck_1monthChange,
ftrg.ct_maxstocklevelindayssite_merck_1monthChange=x.ct_maxstocklevelindayssite_merck_1monthChange,
ftrg.ct_interplantdemandqtypurchdoc_1monthChange=x.ct_interplantdemandqtypurchdoc_1monthChange,
ftrg.ct_lotsize_merck_1monthChange=x.ct_lotsize_merck_1monthChange,
ftrg.ct_lotsizesite_merck_1monthChange=x.ct_lotsizesite_merck_1monthChange,
ftrg.ct_mosfixedtarget_merck_1monthChange=x.ct_mosfixedtarget_merck_1monthChange;



/*MONTHLY PART END*/

drop table if exists tmp_fact_inventoryatlashistory_updquarterly;

create table tmp_fact_inventoryatlashistory_updquarterly as
select distinct
	fcur.dim_partid,
	fcur.dim_plantid,
	fcur.snapshotdate,
	ifnull((fcur.ct_available_qi_stock_coverage-fhist_quarter.ct_available_qi_stock_coverage),0) as ct_available_qi_stock_coverage_1QuarterChange,
	ifnull((fcur.ct_available_qi_stockqty-fhist_quarter.ct_available_qi_stockqty),0) as ct_available_qi_stockqty_1QuarterChange,
	ifnull((fcur.ct_available_stock_coverage-fhist_quarter.ct_available_stock_coverage),0) as ct_available_stock_coverage_1QuarterChange,
	ifnull((fcur.ct_available_stockqty-fhist_quarter.ct_available_stockqty),0) as ct_available_stockqty_1QuarterChange,
	ifnull((fcur.ct_safety_workdays-fhist_quarter.ct_safety_workdays),0) as ct_safety_workdays_1QuarterChange,
	ifnull((fcur.ct_safety_workdays_grprocessing_time-fhist_quarter.ct_safety_workdays_grprocessing_time),0) as ct_safety_workdays_grprocessing_time_1QuarterChange,
	ifnull((fcur.ct_safety_eoqeotfx_workdays_gr_time-fhist_quarter.ct_safety_eoqeotfx_workdays_gr_time),0) as ct_safety_eoqeotfx_workdays_gr_time_1QuarterChange,
	ifnull((fcur.ct_future_demandqty-fhist_quarter.ct_future_demandqty),0) as ct_future_demandqty_1QuarterChange,
	ifnull((fcur.amt_available_qi_stock-fhist_quarter.amt_available_qi_stock),0) as amt_available_qi_stock_1QuarterChange,
	ifnull((fcur.amt_available_stock-fhist_quarter.amt_available_stock),0) as amt_available_stock_1QuarterChange,
	ifnull((fcur.amt_future_demand-fhist_quarter.amt_future_demand),0) as amt_future_demand_1QuarterChange,
	ifnull(fhist_quarter.ct_available_qi_stock_coverage,0) as ct_available_qi_stock_coverage_1quarterold_value,
	ifnull(fhist_quarter.ct_safety_eoqeotfx_workdays_gr_time,0) as ct_safety_eoqeotfx_workdays_gr_time_1quarterold_value,
	ifnull(fhist_quarter.ct_safety_workdays_grprocessing_time,0) as ct_safety_workdays_grprocessing_time_1quarterold_value,
	ifnull(fhist_quarter.ct_available_stockqty,0) as ct_available_stockqty_1quarterold_value,
	ifnull(fhist_quarter.ct_future_demandqty,0) as ct_future_demandqty_1quarterold_value,
	/* 14.05.2015 */
	ifnull((fcur.ct_intransitstockqty-fhist_quarter.ct_intransitstockqty),0) as ct_intransitstockqty_1quarterChange,
	ifnull((fcur.ct_indepreqqty-fhist_quarter.ct_indepreqqty),0) as ct_indepreqqty_1quarterChange,
	ifnull((fcur.ct_unrestrictedstockqty_merck-fhist_quarter.ct_unrestrictedstockqty_merck),0) as ct_unrestrictedstockqty_merck_1quarterChange,
	ifnull((fcur.ct_stockintransfer_merck-fhist_quarter.ct_stockintransfer_merck),0) as ct_stockintransfer_merck_1quarterChange,
	ifnull((fcur.ct_stockinqinsp_merck-fhist_quarter.ct_stockinqinsp_merck),0) as ct_stockinqinsp_merck_1quarterChange,
	ifnull((fcur.ct_totalrestrictedstock_merck-fhist_quarter.ct_totalrestrictedstock_merck),0) as ct_totalrestrictedstock_merck_1quarterChange,
	ifnull((fcur.ct_blockedstock_merck-fhist_quarter.ct_blockedstock_merck),0) as ct_blockedstock_merck_1quarterChange,
	ifnull((fcur.ct_blockedstockreturns_merck-fhist_quarter.ct_blockedstockreturns_merck),0) as ct_blockedstockreturns_merck_1quarterChange,
	ifnull((fcur.ct_stockintransit_merck-fhist_quarter.ct_stockintransit_merck),0) as ct_stockintransit_merck_1quarterChange,
	ifnull((fcur.ct_minreleasedstockindays_merck-fhist_quarter.ct_minreleasedstockindays_merck),0) as ct_minreleasedstockindays_merck_1quarterChange,
	ifnull((fcur.ct_mintotalstockindays_merck-fhist_quarter.ct_mintotalstockindays_merck),0) as ct_mintotalstockindays_merck_1quarterChange,
	ifnull((fcur.ct_mthsofsupplytarget_merck-fhist_quarter.ct_mthsofsupplytarget_merck),0) as ct_mthsofsupplytarget_merck_1quarterChange,
	ifnull((fcur.ct_mthsofsupplytargetindays_merck-fhist_quarter.ct_mthsofsupplytargetindays_merck),0) as ct_mthsofsupplytargetindays_merck_1quarterChange,
	ifnull((fcur.ct_maxstocklevelindays_merck-fhist_quarter.ct_maxstocklevelindays_merck),0) as ct_maxstocklevelindays_merck_1quarterChange,
	ifnull((fcur.ct_lotsizeindays_merck-fhist_quarter.ct_lotsizeindays_merck),0) as ct_lotsizeindays_merck_1quarterChange,
	ifnull((fcur.ct_interplantdemandqty-fhist_quarter.ct_interplantdemandqty),0) as ct_interplantdemandqty_1quarterChange,
	ifnull((fcur.ct_sitedemandqty-fhist_quarter.ct_sitedemandqty),0) as ct_sitedemandqty_1quarterChange,
	ifnull((fcur.ct_lotsizeindayssite_merck-fhist_quarter.ct_lotsizeindayssite_merck),0) as ct_lotsizeindayssite_merck_1quarterChange,
	ifnull((fcur.ct_minreleasedstockindayssite_merck-fhist_quarter.ct_minreleasedstockindayssite_merck),0) as ct_minreleasedstockindayssite_merck_1quarterChange,
	ifnull((fcur.ct_mintotalstockindayssite_merck-fhist_quarter.ct_mintotalstockindayssite_merck),0) as ct_mintotalstockindayssite_merck_1quarterChange,
	ifnull((fcur.ct_mthsofsupplytargetindayssite_merck-fhist_quarter.ct_mthsofsupplytargetindayssite_merck),0) as ct_mthsofsupplytargetindayssite_merck_1quarterChange,
	ifnull((fcur.ct_mthsofsupplytargetsite_merck-fhist_quarter.ct_mthsofsupplytargetsite_merck),0) as ct_mthsofsupplytargetsite_merck_1quarterChange,
	ifnull((fcur.ct_maxstocklevelindayssite_merck-fhist_quarter.ct_maxstocklevelindayssite_merck),0) as ct_maxstocklevelindayssite_merck_1quarterChange,
	ifnull((fcur.ct_interplantdemandqtypurchdoc-fhist_quarter.ct_interplantdemandqtypurchdoc),0) as ct_interplantdemandqtypurchdoc_1quarterChange,
	ifnull((fcur.ct_lotsize_merck-fhist_quarter.ct_lotsize_merck),0) as ct_lotsize_merck_1quarterChange,
	ifnull((fcur.ct_lotsizesite_merck-fhist_quarter.ct_lotsizesite_merck),0) as ct_lotsizesite_merck_1quarterChange,
	ifnull((fcur.ct_mosfixedtarget_merck-fhist_quarter.ct_mosfixedtarget_merck),0) as ct_mosfixedtarget_merck_1quarterChange
/*  end 14.05.2015 */
from 
	fact_inventoryatlashistory fcur
	left outer join fact_inventoryatlashistory  fhist_quarter
		on fcur.dim_partid=fhist_quarter.dim_partid
		and fcur.dim_plantid=fhist_quarter.dim_plantid
		and fcur.snapshotdate=(fhist_quarter.snapshotdate + INTERVAL '3' MONTH)
/*comment where clause if full re-computation is needed*/
where fcur.snapshotdate=current_date
and fcur.dim_partid not in (select dim_partid from dim_part where partnumber='XXXXXX');

update fact_inventoryatlashistory ftrg
set ftrg.ct_available_qi_stock_coverage_1QuarterChange=ftmp.ct_available_qi_stock_coverage_1QuarterChange,
ftrg.ct_available_qi_stockqty_1QuarterChange=ftmp.ct_available_qi_stockqty_1QuarterChange,
ftrg.ct_available_stock_coverage_1QuarterChange=ftmp.ct_available_stock_coverage_1QuarterChange,
ftrg.ct_available_stockqty_1QuarterChange=ftmp.ct_available_stockqty_1QuarterChange,
ftrg.ct_safety_workdays_1QuarterChange=ftmp.ct_safety_workdays_1QuarterChange,
ftrg.ct_safety_workdays_grprocessing_time_1QuarterChange=ftmp.ct_safety_workdays_grprocessing_time_1QuarterChange,
ftrg.ct_safety_eoqeotfx_workdays_gr_time_1QuarterChange=ftmp.ct_safety_eoqeotfx_workdays_gr_time_1QuarterChange,
ftrg.ct_future_demandqty_1QuarterChange=ftmp.ct_future_demandqty_1QuarterChange,
ftrg.amt_available_qi_stock_1QuarterChange=ftmp.amt_available_qi_stock_1QuarterChange,
ftrg.amt_available_stock_1QuarterChange=ftmp.amt_available_stock_1QuarterChange,
ftrg.amt_future_demand_1QuarterChange=ftmp.amt_future_demand_1QuarterChange,
ftrg.ct_available_qi_stock_coverage_1quarterold_value=ftmp.ct_available_qi_stock_coverage_1quarterold_value,
ftrg.ct_safety_eoqeotfx_workdays_gr_time_1quarterold_value=ftmp.ct_safety_eoqeotfx_workdays_gr_time_1quarterold_value,
ftrg.ct_safety_workdays_grprocessing_time_1quarterold_value=ftmp.ct_safety_workdays_grprocessing_time_1quarterold_value,
ftrg.ct_available_stockqty_1quarterold_value=ftmp.ct_available_stockqty_1quarterold_value,
ftrg.ct_future_demandqty_1quarterold_value=ftmp.ct_future_demandqty_1quarterold_value,
/*  end 14.05.2015 */
ftrg.ct_intransitstockqty_1quarterChange=ftmp.ct_intransitstockqty_1quarterChange,
ftrg.ct_indepreqqty_1quarterChange=ftmp.ct_indepreqqty_1quarterChange,
ftrg.ct_unrestrictedstockqty_merck_1quarterChange=ftmp.ct_unrestrictedstockqty_merck_1quarterChange,
ftrg.ct_stockintransfer_merck_1quarterChange=ftmp.ct_stockintransfer_merck_1quarterChange,
ftrg.ct_stockinqinsp_merck_1quarterChange=ftmp.ct_stockinqinsp_merck_1quarterChange,
ftrg.ct_totalrestrictedstock_merck_1quarterChange=ftmp.ct_totalrestrictedstock_merck_1quarterChange,
ftrg.ct_blockedstock_merck_1quarterChange=ftmp.ct_blockedstock_merck_1quarterChange,
ftrg.ct_blockedstockreturns_merck_1quarterChange=ftmp.ct_blockedstockreturns_merck_1quarterChange,
ftrg.ct_stockintransit_merck_1quarterChange=ftmp.ct_stockintransit_merck_1quarterChange,
ftrg.ct_minreleasedstockindays_merck_1quarterChange=ftmp.ct_minreleasedstockindays_merck_1quarterChange,
ftrg.ct_mintotalstockindays_merck_1quarterChange=ftmp.ct_mintotalstockindays_merck_1quarterChange,
ftrg.ct_mthsofsupplytarget_merck_1quarterChange=ftmp.ct_mthsofsupplytarget_merck_1quarterChange,
ftrg.ct_mthsofsupplytargetindays_merck_1quarterChange=ftmp.ct_mthsofsupplytargetindays_merck_1quarterChange,
ftrg.ct_maxstocklevelindays_merck_1quarterChange=ftmp.ct_maxstocklevelindays_merck_1quarterChange,
ftrg.ct_lotsizeindays_merck_1quarterChange=ftmp.ct_lotsizeindays_merck_1quarterChange,
ftrg.ct_interplantdemandqty_1quarterChange=ftmp.ct_interplantdemandqty_1quarterChange,
ftrg.ct_sitedemandqty_1quarterChange=ftmp.ct_sitedemandqty_1quarterChange,
ftrg.ct_lotsizeindayssite_merck_1quarterChange=ftmp.ct_lotsizeindayssite_merck_1quarterChange,
ftrg.ct_minreleasedstockindayssite_merck_1quarterChange=ftmp.ct_minreleasedstockindayssite_merck_1quarterChange,
ftrg.ct_mintotalstockindayssite_merck_1quarterChange=ftmp.ct_mintotalstockindayssite_merck_1quarterChange,
ftrg.ct_mthsofsupplytargetindayssite_merck_1quarterChange=ftmp.ct_mthsofsupplytargetindayssite_merck_1quarterChange,
ftrg.ct_mthsofsupplytargetsite_merck_1quarterChange=ftmp.ct_mthsofsupplytargetsite_merck_1quarterChange,
ftrg.ct_maxstocklevelindayssite_merck_1quarterChange=ftmp.ct_maxstocklevelindayssite_merck_1quarterChange,
ftrg.ct_interplantdemandqtypurchdoc_1quarterChange=ftmp.ct_interplantdemandqtypurchdoc_1quarterChange,
ftrg.ct_lotsize_merck_1quarterChange=ftmp.ct_lotsize_merck_1quarterChange,
ftrg.ct_lotsizesite_merck_1quarterChange=ftmp.ct_lotsizesite_merck_1quarterChange,
ftrg.ct_mosfixedtarget_merck_1quarterChange=ftmp.ct_mosfixedtarget_merck_1quarterChange
/*  end 14.05.2015 */
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_fact_inventoryatlashistory_updquarterly ftmp,fact_inventoryatlashistory ftrg
where
ftrg.dim_partid=ftmp.dim_partid 
and ftrg.dim_plantid=ftmp.dim_plantid 
and ftrg.snapshotdate=ftmp.snapshotdate 
/*comment where clause if full re-computation is needed,in normal runs it is redundant*/
and ftrg.snapshotdate=current_date;

/*QUARTERLY PART END*/


/* 28 oct 2014 StdPricePMRA change */
update fact_inventoryatlashistory f
set f.amt_StdPricePMRA_Merck = a.stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from (select fact_inventoryatlashistoryid,avg(stdprice_pmra) as stdprice_pmra from  
ccpstpricefile201602 u, dim_part dp, dim_plant pl,fact_inventoryatlashistory f
where  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcode = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode =pl.plantcode 
group by fact_inventoryatlashistoryid) a, fact_inventoryatlashistory f
WHERE   a.fact_inventoryatlashistoryid = f.fact_inventoryatlashistoryid
and  ifnull(f.amt_StdPricePMRA_Merck,-99) <> a.stdprice_pmra;


/* 28 oct 2014 StdPricePMRA change */


drop table if exists tmp_fact_inventoryatlashistory_updweekly;
drop table if exists tmp_fact_inventoryatlashistory_updmonthly;
drop table if exists tmp_fact_inventoryatlashistory_updquarterly;


/* OZ: Hit or miss on different levels */

merge into fact_inventoryatlashistory ia
using (
select dp.dim_partid,dp.productfamily_merck,dp.sub_product_family_code_pma,dp.ItemSubType_Merck
from dim_part dp) dp
on ia.dim_partid = dp.dim_partid
when matched then update
set ia.dd_gpfcode = dp.ProductFamily_Merck,
ia.dd_sub_product_family_code_pma = ifnull(dp.sub_product_family_code_pma,'Not Set'),
ia.dd_itemsubtype_merck = ifnull(dp.ItemSubType_Merck,'Not Set');

merge into fact_inventoryatlashistory ia
using (
select dim_dateid,MonthYear,datevalue
from dim_date dd) dd
on ia.dim_snapshotdateid = dd.dim_dateid
when matched then update
set ia.dd_snapshot_monthyear = dd.MonthYear,
ia.dd_snapshot_datevalue = dd.datevalue;

merge into fact_inventoryatlashistory ia
using (
select dim_plantid,tacticalring_merck
from dim_plant pl) pl
on ia.dim_plantid = pl.dim_plantid
when matched then update
set ia.dd_tacticalring_merck = pl.tacticalring_merck;

merge into fact_inventoryatlashistory fiah using (
select t.productfamily_merck,t.tacticalring_merck,t.ItemSubType_Merck,t.dim_plantid
, avg(hitormiss) as avg_hitormiss from 
(select pl.dim_plantid,pl.plantcode,pl.tacticalring_merck,ItemSubType_Merck,dp.productfamily_merck,dp.dim_partid,
dd.MonthYear,dd_sub_product_family_code_pma,
CASE WHEN sum(ct_safety_workdays_grprocessing_time - 0.25*ct_safety_workdays) 
< sum(f_invatlashist.ct_available_qi_stock_coverage) AND
sum(f_invatlashist.ct_available_qi_stock_coverage) < 
sum(ct_safety_eoqeotfx_workdays_gr_time + 0.25* ct_safety_workdays)
THEN 100
ELSE 0 END as hitormiss
from fact_inventoryatlashistory f_invatlashist,
dim_plant pl, dim_part dp, dim_date dd
where pl.dim_plantid = f_invatlashist.dim_plantid
and dp.dim_partid = f_invatlashist.dim_partid
and dd.dim_dateid = f_invatlashist.dim_snapshotdateid
group by pl.dim_plantid,pl.plantcode,pl.tacticalring_merck,ItemSubType_Merck,productfamily_merck,
dd.MonthYear,dd_sub_product_family_code_pma,dp.dim_partid) t
group by t.productfamily_merck,t.tacticalring_merck,t.ItemSubType_Merck,t.dim_plantid
) t
on t.productfamily_merck = fiah.dd_gpfcode
and fiah.dd_itemsubtype_merck = t.ItemSubType_Merck
and fiah.dd_tacticalring_merck = t.tacticalring_merck
and fiah.dim_plantid = t.dim_plantid
when matched then update 
set fiah.ct_hitormiss_plant = t.avg_hitormiss;



merge into fact_inventoryatlashistory fiah using (
select t.productfamily_merck,t.MonthYear,t.tacticalring_merck,t.ItemSubType_Merck
, avg(hitormiss) as avg_hitormiss from 
(select pl.dim_plantid,pl.plantcode,pl.tacticalring_merck,ItemSubType_Merck,dp.productfamily_merck,dp.dim_partid,
dd.MonthYear,dd_sub_product_family_code_pma,
CASE WHEN sum(ct_safety_workdays_grprocessing_time - 0.25*ct_safety_workdays) 
< sum(f_invatlashist.ct_available_qi_stock_coverage) AND
sum(f_invatlashist.ct_available_qi_stock_coverage) < 
sum(ct_safety_eoqeotfx_workdays_gr_time + 0.25* ct_safety_workdays)
THEN 100
ELSE 0 END as hitormiss
from fact_inventoryatlashistory f_invatlashist,
dim_plant pl, dim_part dp, dim_date dd
where pl.dim_plantid = f_invatlashist.dim_plantid
and dp.dim_partid = f_invatlashist.dim_partid
and dd.dim_dateid = f_invatlashist.dim_snapshotdateid
group by pl.dim_plantid,pl.plantcode,pl.tacticalring_merck,ItemSubType_Merck,productfamily_merck,
dd.MonthYear,dd_sub_product_family_code_pma,dp.dim_partid) t
group by t.productfamily_merck,t.MonthYear,t.tacticalring_merck,t.ItemSubType_Merck
) t
on t.productfamily_merck = fiah.dd_gpfcode
and t.MonthYear = fiah.dd_snapshot_monthyear
and fiah.dd_itemsubtype_merck = t.ItemSubType_Merck
and fiah.dd_tacticalring_merck = t.tacticalring_merck
when matched then update 
set fiah.ct_hitormiss_months = t.avg_hitormiss;




merge into fact_inventoryatlashistory fiah using (
select t.monthyear,t.productfamily_merck,t.tacticalring_merck,t.ItemSubType_Merck
,dim_plantid,plantcode
, avg(hitormiss) as avg_hitormiss from 
(select pl.dim_plantid,pl.plantcode,pl.tacticalring_merck,ItemSubType_Merck,dp.productfamily_merck,
dd.MonthYear,dd_sub_product_family_code_pma,dp.partnumber,
CASE WHEN sum(ct_safety_workdays_grprocessing_time - 0.25*ct_safety_workdays) 
< sum(f_invatlashist.ct_available_qi_stock_coverage) AND
sum(f_invatlashist.ct_available_qi_stock_coverage) < 
sum(ct_safety_eoqeotfx_workdays_gr_time + 0.25* ct_safety_workdays)
THEN 100
ELSE 0 END as hitormiss
from fact_inventoryatlashistory f_invatlashist,
dim_plant pl, dim_part dp, dim_date dd
where pl.dim_plantid = f_invatlashist.dim_plantid
and dp.dim_partid = f_invatlashist.dim_partid
and dd.dim_dateid = f_invatlashist.dim_snapshotdateid
group by pl.dim_plantid,pl.plantcode,pl.tacticalring_merck,ItemSubType_Merck,productfamily_merck,
dd.MonthYear,dd_sub_product_family_code_pma,dp.partnumber) t
group by t.monthyear,t.productfamily_merck,t.tacticalring_merck,t.ItemSubType_Merck
,dim_plantid,plantcode
) t
on t.productfamily_merck = fiah.dd_gpfcode
and t.MonthYear = fiah.dd_snapshot_monthyear
 and t.dim_plantid = fiah.dim_plantid
and fiah.dd_itemsubtype_merck = t.ItemSubType_Merck
and fiah.dd_tacticalring_merck = t.tacticalring_merck
when matched then update 
set fiah.ct_hitormiss_months_plant = t.avg_hitormiss;



merge into fact_inventoryatlashistory fiah using (
select t.monthyear,t.productfamily_merck,t.tacticalring_merck,t.ItemSubType_Merck
,dim_plantid,plantcode,t.datevalue
, avg(hitormiss) as avg_hitormiss from 
(select pl.dim_plantid,pl.plantcode,pl.tacticalring_merck,ItemSubType_Merck,dp.productfamily_merck,
dd.MonthYear,dd_sub_product_family_code_pma,dp.partnumber,dd.datevalue,
CASE WHEN sum(ct_safety_workdays_grprocessing_time - 0.25*ct_safety_workdays) 
< sum(f_invatlashist.ct_available_qi_stock_coverage) AND
sum(f_invatlashist.ct_available_qi_stock_coverage) < 
sum(ct_safety_eoqeotfx_workdays_gr_time + 0.25* ct_safety_workdays)
THEN 100
ELSE 0 END as hitormiss
from fact_inventoryatlashistory f_invatlashist,
dim_plant pl, dim_part dp, dim_date dd
where pl.dim_plantid = f_invatlashist.dim_plantid
and dp.dim_partid = f_invatlashist.dim_partid
and dd.dim_dateid = f_invatlashist.dim_snapshotdateid
group by pl.dim_plantid,pl.plantcode,pl.tacticalring_merck,ItemSubType_Merck,productfamily_merck,
dd.MonthYear,dd_sub_product_family_code_pma,dp.partnumber,dd.datevalue) t
group by t.monthyear,t.productfamily_merck,t.tacticalring_merck,t.ItemSubType_Merck,t.datevalue
,dim_plantid,plantcode
) t
on t.productfamily_merck = fiah.dd_gpfcode
and t.MonthYear = fiah.dd_snapshot_monthyear
 and t.dim_plantid = fiah.dim_plantid
and fiah.dd_itemsubtype_merck = t.ItemSubType_Merck
and fiah.dd_tacticalring_merck = t.tacticalring_merck
and t.datevalue = fiah.dd_snapshot_datevalue
when matched then update 
set fiah.ct_hitormiss = t.avg_hitormiss;

/*BI-5286 Alin Gheorghe 26.01.2017 */

delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryatlashistory';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryatlashistory',ifnull(max(fact_inventoryatlashistoryid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
FROM fact_inventoryatlashistory;


INSERT INTO fact_inventoryatlashistory(fact_inventoryatlashistoryid,
dim_plantid,
dim_partid,
DD_IQVRISKCODE ,
DD_IQVRISKDESCRIPTION,
DD_IQVREASONCODE,
DD_IQVREASONDESCRIPTION,
DD_IQVREASONCOMMENT,
dd_iqvriskcatcode,
dd_iqvriskcatdescription,
--AMT_IQVPROVISION, 
--CT_IQVPROVISION,
CT_IQVFIXED,
/*new cols*/
dd_update_mode,
dd_batch_per_plant,
dd_Base_Unit,
dd_Responsible_Area,
dd_IQV_Action_Code,
dd_IQV_Action_Description,
dd_Comment_IQV_Action,
dd_Last_Changed_by,
dim_First_IQV_Review_Dateid,
dim_Last_IQV_Review_Dateid,
dim_IQV_Due_Dateid,
dim_Risk_Creation_Dateid
)
SELECT 
(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryatlashistory') + row_number() over (order by ''),
pl.dim_plantid, dp.dim_partid, 
iqv_risk_code ,
iqv_risk_description ,
iqv_reason_code ,
iqv_reason_description ,
iqv_comment_reason,
IQV_RISKCATCODE,
IQV_RISKCATDESC,
--iqv_provision_amount ,
--iqv_provsion_qty ,
iqv_fixed_qty,
/*new cols*/
update_mode,
batch_per_plant,
Base_Unit,
Responsible_Area,
IQV_Action_Code,
IQV_Action_Description,
Comment_IQV_Action,
Last_Changed_by,
1,
1,
1,
1
/*new cols*/

FROM IQV i, dim_part dp, dim_plant pl
where i.plant = pl.plantcode and 
i.plant = dp.plant and
i.material = dp.partnumber and
dp.plant = pl.plantcode 
and not exists(
select 1 from fact_inventoryatlashistory f  where
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL);


 merge into fact_inventoryatlashistory f
using(
select fact_inventoryatlashistoryid,
min(iqv_risk_code) as iqv_risk_code,
min(iqv_risk_description) as iqv_risk_description,
min(iqv_reason_code) as iqv_reason_code,
min(iqv_reason_description) as iqv_reason_description,
min(iqv_comment_reason) as iqv_comment_reason,
min(IQV_RISKCATCODE) as IQV_RISKCATCODE,
min(IQV_RISKCATDESC) as IQV_RISKCATDESC,
/*new cols*/
min(update_mode) as update_mode,
min(batch_per_plant) as batch_per_plant,
min(Base_Unit) as Base_Unit,
min(Responsible_Area) as Responsible_Area,
min(IQV_Action_Code) as IQV_Action_Code,
min(IQV_Action_Description) as IQV_Action_Description,
min(Comment_IQV_Action) as Comment_IQV_Action,
min(Last_Changed_by) as Last_Changed_by

from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlashistory f
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
DP.PARTNUMBER = I.MATERIAL AND
PL.PLANTCODE = I.PLANT
GROUP BY fact_inventoryatlashistoryid
)t
on t.fact_inventoryatlashistoryid = f.fact_inventoryatlashistoryid
when matched then 
update set
DD_IQVRISKCODE = ifnull(iqv_risk_code,'Not Set') ,
DD_IQVRISKDESCRIPTION = ifnull(iqv_risk_description,'Not Set'),
DD_IQVREASONCODE =ifnull(iqv_reason_code,'Not Set'),
DD_IQVREASONDESCRIPTION =ifnull(iqv_reason_description,'Not Set'),
DD_IQVREASONCOMMENT = ifnull(iqv_comment_reason,'Not Set'),
DD_IQVRISKCATCODE = ifnull(IQV_RISKCATCODE,'Not Set'),
DD_IQVRISKCATDESCRIPTION = ifnull(IQV_RISKCATDESC,'Not Set'),
/*new cols*/
dd_update_mode = ifnull(update_mode,'Not Set'),
dd_batch_per_plant = ifnull(batch_per_plant,'Not Set'),
dd_Base_Unit = ifnull(Base_Unit,'Not Set'),
dd_Responsible_Area = ifnull(Responsible_Area,'Not Set'),
dd_IQV_Action_Code = ifnull(IQV_Action_Code,'Not Set'),
dd_IQV_Action_Description = ifnull(IQV_Action_Description,'Not Set'),
dd_Comment_IQV_Action = ifnull(Comment_IQV_Action,'Not Set'),
dd_Last_Changed_by = ifnull(Last_Changed_by,'Not Set');


DROP TABLE IF EXISTS temp_test_invatl;
create table temp_test_invatl as
select  f.fact_inventoryatlashistoryid, f.dim_partid,f.dim_plantid, 
iqv_provision_amount as iqv_provision_amount,
iqv_provsion_qty as iqv_provsion_qty,
iqv_fixed_qty as iqv_fixed_qty
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlashistory f
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL;


merge into fact_inventoryatlashistory f
using(
select  Z.fact_inventoryatlashistoryid, Z.DIM_PARTID, Z.DIM_PLANTID,
SUM(iqv_provision_amount) AS iqv_provision_amount ,
SUM(iqv_provsion_qty) AS iqv_provsion_qty,
SUM(iqv_fixed_qty) AS iqv_fixed_qty
from temp_test_invatl Z
GROUP BY Z.fact_inventoryatlashistoryid,  Z.DIM_PARTID, Z.DIM_PLANTID
) t
on  f.fact_inventoryatlashistoryid = t.fact_inventoryatlashistoryid
when matched then 
update set
--AMT_IQVPROVISION = iqv_provision_amount, 
--CT_IQVPROVISION = iqv_provsion_qty,
CT_IQVFIXED = iqv_fixed_qty;

DROP TABLE IF EXISTS temp_test_invatl;


/*NEW COLS DIM_DATES*/
MERGE INTO fact_inventoryatlashistory ST
USING
	(
SELECT DISTINCT F.fact_inventoryatlashistoryid, ifnull(dd.dim_dateid,1) dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlashistory f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL AND
f.DD_BATCH_PER_PLANT = I.IQV_BATCH
AND dd.datevalue =  cast(ifnull(i.First_IQV_Review_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryatlashistoryid = SRC.fact_inventoryatlashistoryid
WHEN MATCHED THEN UPDATE
	SET ST.dim_First_IQV_Review_Dateid = SRC.dim_dateid
WHERE ST.dim_First_IQV_Review_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryatlashistory ST
USING
	(
SELECT DISTINCT F.fact_inventoryatlashistoryid, ifnull(dd.dim_dateid,1) dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlashistory f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL AND
f.DD_BATCH_PER_PLANT = I.IQV_BATCH
AND dd.datevalue =  cast(ifnull(i.Last_IQV_Review_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryatlashistoryid = SRC.fact_inventoryatlashistoryid
WHEN MATCHED THEN UPDATE
	SET ST.dim_Last_IQV_Review_Dateid = SRC.dim_dateid
WHERE ST.dim_Last_IQV_Review_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryatlashistory ST
USING
	(
SELECT DISTINCT F.fact_inventoryatlashistoryid, ifnull(dd.dim_dateid,1) dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlashistory f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL AND
f.DD_BATCH_PER_PLANT = I.IQV_BATCH
AND dd.datevalue =  cast(ifnull(i.IQV_Due_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryatlashistoryid = SRC.fact_inventoryatlashistoryid
WHEN MATCHED THEN UPDATE
	SET ST.dim_IQV_Due_Dateid = SRC.dim_dateid
WHERE ST.dim_IQV_Due_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryatlashistory ST
USING
	(
SELECT DISTINCT F.fact_inventoryatlashistoryid, ifnull(dd.dim_dateid,1) dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlashistory f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL AND
f.DD_BATCH_PER_PLANT = I.IQV_BATCH
AND dd.datevalue =  cast(ifnull(i.Risk_Creation_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryatlashistoryid = SRC.fact_inventoryatlashistoryid
WHEN MATCHED THEN UPDATE
	SET ST.dim_Risk_Creation_Dateid = SRC.dim_dateid
WHERE ST.dim_Risk_Creation_Dateid <> SRC.dim_dateid;







update fact_inventoryatlashistory set ct_totaliqvamountCCP=0;

drop table if exists tmp_for_upd_ct_totaliqvamountCCP;
create table tmp_for_upd_ct_totaliqvamountCCP as
select distinct f.dim_partid, f.dim_plantid,
AVG(CASE WHEN DD_IQVRISKCODE='1' THEN case when prt.uomiru = 0 then (CT_IQVPROVISION*amt_gblstdprice_merck) else ((CT_IQVPROVISION*amt_gblstdprice_merck) / prt.uomiru) end  
WHEN DD_IQVRISKCODE='2' THEN ct_onhandqtyIRU * amt_gblstdprice_merck
WHEN DD_IQVRISKCODE='3' THEN ct_onhandqtyIRU * amt_gblstdprice_merck
ELSE 0 END) as ct_totaliqvamountCCP
From fact_inventoryatlashistory f, dim_part prt
where f.dim_partid=prt.dim_partid
group by f.dim_partid,f.dim_plantid;


merge into fact_inventoryatlashistory f
using (select distinct f.fact_inventoryatlashistoryid, t.ct_totaliqvamountCCP
from fact_inventoryatlashistory f,tmp_for_upd_ct_totaliqvamountCCP t
where
f.dim_partid=t.dim_partid 
and f.dim_plantid=t.dim_plantid 
) s
on f.fact_inventoryatlashistoryid=s.fact_inventoryatlashistoryid
when matched then update set f.ct_totaliqvamountCCP= ifnull(s.ct_totaliqvamountCCP,0);

/*Alin 26 sept APP-7603 START*/
update fact_inventoryatlashistory set ct_future_demand_qty_v2=0;

drop table if exists tmp_for_upd_ct_future_demand_qty_v2;
create table tmp_for_upd_ct_future_demand_qty_v2 as
select distinct f.dim_partid, f.dim_plantid,
future_demandqty as ct_future_demand_qty_v2
From fact_inventoryatlashistory f, dim_part prt
where f.dim_partid=prt.dim_partid;


merge into fact_inventoryatlashistory f
using (select distinct f.fact_inventoryatlashistoryid, t.ct_future_demand_qty_v2
from fact_inventoryatlashistory f, tmp_for_upd_ct_future_demand_qty_v2 t
where
f.dim_partid=t.dim_partid 
and f.dim_plantid=t.dim_plantid 
) s
on f.fact_inventoryatlashistoryid=s.fact_inventoryatlashistoryid
when matched then update set f.ct_future_demand_qty_v2= ifnull(s.ct_future_demand_qty_v2,0);

drop table if exists tmp_for_upd_ct_future_demand_qty_v2;
/*Alin 26 sept APP-7603 END*/

/*Andrei R According to App-6119*/

update fact_inventoryatlashistory
set dd_monthlyflag=case when datevalue like '%-%-01' then 'Yes' Else 'No' end ,
dd_quaterlyflag=case when datevalue like '%-01-01' or datevalue like '%-04-01' or datevalue like '%-07-01' or datevalue like '%-10-01'    then 'Yes' Else 'No' end,
dd_yearlyflag = case when datevalue like '%-01-01' then 'Yes' else 'No' end 
 from fact_inventoryatlashistory, dim_date where dim_snapshotdateid=dim_dateid;
 
/*Georgiana Changes according to APP-8762 calculate the %Supply level Trend as count(partnumber) for each Supply level/total count(part number) for the past 52 weeks*/
/*Octavian S - 28-FEB-2018 - Updated according to APP-8762 'Supply Level 2.0' measure for this report which has 4 instead of 3 entries (Normal, At Risk, Critical, Out of Stock)*/ 
/*Octavian S - 16-APR-2018 - Updated accordint to APP-8762 'Supply Level 2.0' (BC Ticket) measure for this report should be calculated based on nine filters*/
 
UPDATE fact_inventoryatlashistory       f
   SET ct_supplyLevelhistory            = 0
  FROM fact_inventoryatlashistory       f;
 
DROP TABLE IF EXISTS tmp_count_materials;
CREATE TABLE tmp_count_materials AS 
SELECT dd3.datevalue,
       dp.prodfamilydescription_merck,
       dp.DOMINANTSPECIES_MERCK,
	   dp.HEALTHPLATFORM_MERCK,
	   dp.SEGMENTATION,
       dpl.PLANTCODE                        AS devileringplant,
	   dpl.PLANTTITLE_MERCK                 AS comops,
	   dpl.TACTICALRING_MERCK               AS lineofbussiness,
       COUNT(dp.PartNumber_NoLeadZero)      AS totalcnt,
       ROW_NUMBER() OVER (PARTITION BY dp.prodfamilydescription_merck,  
                                       dp.prodfamilydescription_merck,
                                       dp.DOMINANTSPECIES_MERCK,
	                                   dp.HEALTHPLATFORM_MERCK,
	                                   dp.SEGMENTATION,
                                       dpl.PLANTCODE,
	                                   dpl.PLANTTITLE_MERCK,
	                                   dpl.TACTICALRING_MERCK 
                              ORDER BY datevalue) 
                                            AS row_num 
  FROM fact_inventoryatlashistory           AS f_invatlashist 
 INNER JOIN Dim_Date                        AS dd3 
    ON f_invatlashist.dim_snapshotdateid    = dd3.Dim_Dateid  
   AND (LOWER(dd3.WeekDayAbbreviation)      = LOWER('Mon'))
 INNER JOIN Dim_Part                        AS dp 
    ON f_invatlashist.dim_partid            = dp.Dim_Partid  
   AND (LOWER(dp.ItemSubType_Merck)         = LOWER('FPP'))  
   AND ((dp.PlantMaterialStatusDescription) NOT IN (('Blocked - New Item'),('Blocked for procment/whse'),('Blocked for procurement'),('Not active any portfolio'),
                                                   ('Blocked - Inactive'),('Blocked - Phased out'),('NEW New product'),('No Inventory Mgt')) )  
   AND ((dp.MRPControllerDescription)       NOT IN (('NO USE MATERIAL'),('Obsolete')) )
 INNER JOIN Dim_Plant                       AS dpl2 
    ON f_invatlashist.dim_plantdeliveryid   = dpl2.Dim_Plantid  
   AND (LOWER(dpl2.PlantCode)              != LOWER('Not Set'))  
 INNER JOIN Dim_Plant                       AS dpl 
    ON f_invatlashist.dim_plantid           = dpl.Dim_Plantid
   AND (LOWER(dpl.tacticalring_merck)       = LOWER('ComOps'))  
   AND (LOWER(dpl.PlantCode)               != LOWER('XX20'))
 WHERE dd3.datevalue BETWEEN ADD_WEEKS(dd3.datevalue, -51) AND dd3.datevalue
 GROUP BY dd3.datevalue,
          dp.prodfamilydescription_merck,
          dp.DOMINANTSPECIES_MERCK,
	      dp.HEALTHPLATFORM_MERCK,
	      dp.SEGMENTATION,
          dpl.PLANTCODE,
	      dpl.PLANTTITLE_MERCK,
	      dpl.TACTICALRING_MERCK 
 ORDER BY datevalue DESC;

DROP TABLE IF EXISTS tmp_count_materials_sum;
CREATE TABLE tmp_count_materials_sum AS
SELECT t1.datevalue, 
       t1.prodfamilydescription_merck,
       t1.DOMINANTSPECIES_MERCK,
	   t1.HEALTHPLATFORM_MERCK,
	   t1.SEGMENTATION,
       t1.devileringplant,
	   t1.comops,
	   t1.lineofbussiness,
       avg(t1.totalcnt) AS totalcnt,
       sum(t2.totalcnt) AS totalcntsum 
  FROM tmp_count_materials t1
 INNER JOIN tmp_count_materials t2
    ON t1.row_num BETWEEN t2.row_num AND t2.row_num+ 51
 WHERE t1.prodfamilydescription_merck=t2.prodfamilydescription_merck
   AND t1.DOMINANTSPECIES_MERCK=t2.DOMINANTSPECIES_MERCK
   AND t1.HEALTHPLATFORM_MERCK=t2.HEALTHPLATFORM_MERCK
   AND t1.SEGMENTATION=t2.SEGMENTATION
   AND t1.devileringplant=t2.devileringplant
   AND t1.comops=t2.comops
   AND t1.lineofbussiness=t2.lineofbussiness
 GROUP BY t1.datevalue, 
          t1.prodfamilydescription_merck,
          t1.DOMINANTSPECIES_MERCK,
	      t1.HEALTHPLATFORM_MERCK,
	      t1.SEGMENTATION,
          t1.devileringplant,
	      t1.comops,
	      t1.lineofbussiness   
 ORDER BY t1.datevalue DESC;

DROP TABLE IF EXISTS tmp_for_measure;
CREATE TABLE tmp_for_measure AS
SELECT 
      CASE WHEN f_invatlashist.ct_available_stock_coverage = 0 
                THEN ' Out of Stock'
           WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.75 
                THEN 'Normal'
           WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.25 
                THEN 'At Risk'
           WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.01 
                THEN '.Critical'
                ELSE ' Out of Stock'
           END                                                                   AS Col_ord_1,
      dd3.datevalue,  
      COUNT( dp.PartNumber_NoLeadZero )                                          AS cntgroup,
      avg(totalcntsum)                                                           AS avgcount,
      ROUND(100*(COUNT( dp.PartNumber_NoLeadZero )       / avg(totalcntsum) ),2) AS measure,
      t.prodfamilydescription_merck,
      t.DOMINANTSPECIES_MERCK,
	  t.HEALTHPLATFORM_MERCK,
	  t.SEGMENTATION,
      t.devileringplant,
	  t.comops,
      t.lineofbussiness
 FROM fact_inventoryatlashistory           AS f_invatlashist 
INNER JOIN Dim_Date                        AS dd3 
   ON f_invatlashist.dim_snapshotdateid    = dd3.Dim_Dateid  
  AND (LOWER(dd3.WeekDayAbbreviation)      = LOWER('Mon')) 
INNER JOIN Dim_Part                        AS dp 
   ON f_invatlashist.dim_partid            = dp.Dim_Partid  
  AND (LOWER(dp.ItemSubType_Merck)         = LOWER('FPP'))  
  AND ((dp.PlantMaterialStatusDescription) NOT IN (('Blocked - New Item'),('Blocked for procment/whse'),('Blocked for procurement'),('Not active any portfolio'),
                                                  ('Blocked - Inactive'),('Blocked - Phased out'),('NEW New product'),('No Inventory Mgt')) )  
  AND ((dp.MRPControllerDescription)       NOT IN (('NO USE MATERIAL'),('Obsolete')) )
INNER JOIN Dim_Plant                       AS dpl2 
   ON f_invatlashist.dim_plantdeliveryid   = dpl2.Dim_Plantid  
  AND (LOWER(dpl2.PlantCode)              != LOWER('Not Set'))  
INNER JOIN Dim_Plant                       AS dpl 
   ON f_invatlashist.dim_plantid           = dpl.Dim_Plantid
  AND (LOWER(dpl.tacticalring_merck)       = LOWER('ComOps'))  
  AND (LOWER(dpl.PlantCode)               != LOWER('XX20'))
INNER JOIN  tmp_count_materials_sum T 
   ON dd3.datevalue =t.datevalue   
  AND dp.prodfamilydescription_merck=t.prodfamilydescription_merck
  AND dp.DOMINANTSPECIES_MERCK=t.DOMINANTSPECIES_MERCK
  AND dp.HEALTHPLATFORM_MERCK=t.HEALTHPLATFORM_MERCK
  AND dp.SEGMENTATION=t.SEGMENTATION
  AND dpl.PLANTCODE=t.devileringplant
  AND dpl.PLANTTITLE_MERCK=t.comops
  AND dpl.TACTICALRING_MERCK=t.lineofbussiness
GROUP BY  CASE WHEN f_invatlashist.ct_available_stock_coverage = 0 
                   THEN ' Out of Stock'
               WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.75 
                   THEN 'Normal'
               WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.25 
                   THEN 'At Risk'
               WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.01 
                   THEN '.Critical'
               ELSE ' Out of Stock'
          END,
          dd3.datevalue, 
          t.prodfamilydescription_merck, 
          t.DOMINANTSPECIES_MERCK,
	      t.HEALTHPLATFORM_MERCK,
	      t.SEGMENTATION,
          t.devileringplant,
	      t.comops,
	      t.lineofbussiness
 ORDER BY 2,1;

DROP TABLE IF EXISTS tmp_for_check;
CREATE TABLE tmp_for_check AS
SELECT DISTINCT f_invatlashist.fact_inventoryatlashistoryid,
                m.Col_ord_1,
                m.measure,
                m.datevalue,
                m.prodfamilydescription_merck,
                m.DOMINANTSPECIES_MERCK,
	            m.HEALTHPLATFORM_MERCK,
	            m.SEGMENTATION,
                m.devileringplant,
	            m.comops,
	            m.lineofbussiness,
                ROW_NUMBER() OVER (PARTITION BY m.Col_ord_1,
                	                            m.measure, 
                	                            m.datevalue,
                	                            m.prodfamilydescription_merck,
                                                m.DOMINANTSPECIES_MERCK,
	                                            m.HEALTHPLATFORM_MERCK,
	                                            m.SEGMENTATION,
                                                m.devileringplant,
	                                            m.comops,
	                                            m.lineofbussiness 
	                              ORDER BY m.datevalue) AS row_num 
 FROM fact_inventoryatlashistory AS f_invatlashist 
INNER JOIN Dim_Part              AS dp 
   ON f_invatlashist.dim_partid            = dp.Dim_Partid  
  AND (LOWER(dp.ItemSubType_Merck)         = LOWER('FPP'))  
  AND ((dp.PlantMaterialStatusDescription) NOT IN (('Blocked - New Item'),('Blocked for procment/whse'),('Blocked for procurement'),('Not active any portfolio'),
                                                  ('Blocked - Inactive'),('Blocked - Phased out'),('NEW New product'),('No Inventory Mgt')) )  
  AND ((dp.MRPControllerDescription)       NOT IN (('NO USE MATERIAL'),('Obsolete')) )
INNER JOIN tmp_for_measure m 
   ON CASE WHEN f_invatlashist.ct_available_stock_coverage = 0 
                THEN ' Out of Stock'
           WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.75 
                THEN 'Normal'
           WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.25 
                THEN 'At Risk'
           WHEN f_invatlashist.ct_available_stock_coverage >= f_invatlashist.ct_safety_workdays * 0.01 
                THEN '.Critical'
                ELSE ' Out of Stock'
          END                              = m.Col_ord_1
INNER JOIN Dim_Date                        AS dd3 
   ON f_invatlashist.dim_snapshotdateid    = dd3.Dim_Dateid  
  AND (LOWER(dd3.WeekDayAbbreviation)      = LOWER('Mon')) 
  AND dd3.datevalue=m.datevalue
INNER JOIN Dim_Plant                       AS dpl2 
   ON f_invatlashist.dim_plantdeliveryid   = dpl2.Dim_Plantid  
  AND (LOWER(dpl2.PlantCode)              != LOWER('Not Set'))  
INNER JOIN Dim_Plant                       AS dpl 
   ON f_invatlashist.dim_plantid           = dpl.Dim_Plantid 
  AND (LOWER(dpl.PlantCode)               != LOWER('XX20'))
WHERE dp.prodfamilydescription_merck       = m.prodfamilydescription_merck    
  AND dp.DOMINANTSPECIES_MERCK             = m.DOMINANTSPECIES_MERCK
  AND dp.HEALTHPLATFORM_MERCK              = m.HEALTHPLATFORM_MERCK
  AND dp.SEGMENTATION                      = m.SEGMENTATION
  AND dpl.PLANTCODE                        = m.devileringplant
  AND dpl.PLANTTITLE_MERCK                 = m.comops
  AND dpl.TACTICALRING_MERCK               = m.lineofbussiness;

UPDATE fact_inventoryatlashistory       f
   SET ct_supplyLevelhistory            = y.measure
  FROM fact_inventoryatlashistory       f, 
       tmp_for_check                    y
 WHERE y.fact_inventoryatlashistoryid   = f.fact_inventoryatlashistoryid
   AND y.row_num=1;

DROP TABLE IF EXISTS tmp_count_materials;
DROP TABLE IF EXISTS tmp_count_materials_sum;
DROP TABLE IF EXISTS tmp_for_measure;
DROP TABLE IF EXISTS tmp_for_check;