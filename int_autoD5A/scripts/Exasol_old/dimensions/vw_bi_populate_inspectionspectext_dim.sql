
/* ################################################################################################################## */ 
/* */
/*   Script         : vw_bi_populate_inspectionspectext_dim.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 30 May 2016 */
/*  */
/*  */
/*   Description    : Populate Inspection Specifications for Certificates Dimension */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */

insert into dim_inspectionspectext ( dim_inspectionspectextid, inspectionspecification, descInspSpec)
select 1, 'Not Set', 'Not Set'
from ( select 1) a
where not exists ( select 'x' from dim_inspectionspectext where dim_inspectionspectextid = 1 );

delete from number_fountain m where m.table_name = 'dim_inspectionspectext';

insert into number_fountain
select 'dim_inspectionspectext',
		ifnull( max(d.dim_inspectionspectextid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
		from dim_inspectionspectext d
		where d.dim_inspectionspectextid <> 1;
		
insert into dim_inspectionspectext
	(
		dim_inspectionspectextid,
		inspectionspecification
	)
	select ( select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectionspectext') + row_number() over(order by '') AS dim_inspectionspectextid,
			ifnull( TQ63T_KZHERKVORG, 'Not Set') as inspectionspecification
	from TQ63T t 
	where not exists (select 1 from dim_inspectionspectext i
    				  where i.inspectionspecification = ifnull(t.TQ63T_KZHERKVORG, 'Not Set'));
	
update dim_inspectionspectext i
set i.descInspSpec = ifnull(t.TQ63T_KURZTEXT,'Not Set'),
	dw_update_date = current_timestamp 
from TQ63T t, dim_inspectionspectext i
where i.inspectionspecification = ifnull(t.TQ63T_KZHERKVORG, 'Not Set')
	and i.descInspSpec <> ifnull(t.TQ63T_KURZTEXT,'Not Set');

