UPDATE    dim_purchaseorg po
       FROM
          t024e t
   SET po.CompanyCode = ifnull(t.T024E_BUKRS, 'Not Set'),
       po.Name = ifnull(t.T024E_EKOTX, 'Not Set')
          FROM
          t024e t,   dim_purchaseorg po
 WHERE po.RowIsCurrent = 1
 AND po.PurchaseOrgCode = t.T024E_EKORG
;
