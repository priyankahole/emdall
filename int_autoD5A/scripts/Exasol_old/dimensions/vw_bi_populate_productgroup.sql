truncate table dim_productgroup;

INSERT INTO DIM_PRODUCTGROUP (DIM_PRODUCTGROUPID)
select 1
from (select 1) a
where not exists (select '1' from DIM_PRODUCTGROUP where DIM_PRODUCTGROUPID = 1);

DELETE FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'DIM_PRODUCTGROUP';
INSERT INTO NUMBER_FOUNTAIN 
SELECT 'DIM_PRODUCTGROUP', ifnull(max(DIM_PRODUCTGROUPID),1)
FROM DIM_PRODUCTGROUP;

INSERT INTO DIM_PRODUCTGROUP (DIM_PRODUCTGROUPID,
                           Productgrouptype,
                           Nameoftheproductgroup,
						   WERKSPlant,
						   NRMITNameoftheproductgroup,
						   WEMITPlant,
						   Validtodate,
						   Productionversion,
						   DW_UPDATE_DATE,
						   DW_INSERT_DATE,
						   RowStartDate,
						   RowEndDate,
						   rowchangereason,
						   RowIsCurrent)
SELECT( 
SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'DIM_PRODUCTGROUP') + row_number() over(order by '') as DIM_PRODUCTGROUPID,
		ifnull(PGMI_PGTYP,'Not Set'),
		ifnull(PGMI_PRGRP,'Not Set'),
		ifnull(PGMI_WERKS,'Not Set'),
		ifnull(PGMI_NRMIT,'Not Set'),
		ifnull(PGMI_WEMIT,'Not Set'),
		ifnull(PGMI_DATUM,'0001-01-01'),
		ifnull(PGMI_VSNDA,'Not Set'),
		current_timestamp,
		current_timestamp,
		null,
		null,
		'Not Set',
		1
from PGMI P
 WHERE NOT EXISTS
	(SELECT 1  FROM DIM_PRODUCTGROUP dp where ifnull(p.PGMI_PGTYP,'Not Set') = dp.Productgrouptype
	                                   and ifnull(p.PGMI_PRGRP,'Not Set') = dp.Nameoftheproductgroup
									   and ifnull(p.PGMI_WERKS,'Not Set') = dp.WERKSPlant
									   and ifnull(p.PGMI_NRMIT,'Not Set') = dp.NRMITNameoftheproductgroup
									   and ifnull(p.PGMI_WEMIT,'Not Set') = dp.WEMITPlant
									   and ifnull(p.PGMI_DATUM,'0001-01-01') = dp.Validtodate
		                               and ifnull(p.PGMI_VSNDA,'Not Set') = dp.Productionversion);
									   

 
 
 
 
 
 