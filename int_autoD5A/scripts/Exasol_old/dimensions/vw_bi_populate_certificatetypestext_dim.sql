/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_certificatetypestext_dim.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 30 May 2016 */
/*  */
/*  */
/*   Description    : Populate Certificate Types Text Dimension */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */


insert into dim_certificatetypestext (dim_certificatetypestextid, certificatetype, descCertifType)
select '1', 'Not Set', 'Not Set'
from ( select 1) a
where not exists ( select 'x' from dim_certificatetypestext where dim_certificatetypestextid = 1 ); 

delete from number_fountain m where m.table_name = 'dim_certificatetypestext';

insert into number_fountain 
select 'dim_certificatetypestext',
	ifnull(max(d.dim_certificatetypestextid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from dim_certificatetypestext d
	where dim_certificatetypestextid <> 1;
	
insert into dim_certificatetypestext
	(
	dim_certificatetypestextid,
	certificatetype
	)
	select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_certificatetypestext') + row_number() over(order by '') AS dim_certificatetypestextid,
    		ifnull(TQ05T_ZGTYP, 'Not Set') as certificatetype
    from TQ05T t
    where not exists (select 1 from dim_certificatetypestext c
    				  where c.certificatetype = ifnull(t.TQ05T_ZGTYP, 'Not Set'));

update dim_certificatetypestext c
set c.descCertifType = ifnull(t.TQ05T_KURZTEXT, 'Not Set'),
	dw_update_date = current_timestamp 
from TQ05T t, dim_certificatetypestext c
where c.certificatetype = ifnull(t.TQ05T_ZGTYP, 'Not Set') 
	and c.descCertifType <> ifnull(t.TQ05T_KURZTEXT, 'Not Set');
	