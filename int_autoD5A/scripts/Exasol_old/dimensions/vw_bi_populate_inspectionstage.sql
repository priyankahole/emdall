UPDATE    dim_inspectionstage s
   SET s.InspectionStageName = q.QDPST_KURZTEXT,
			s.dw_update_date = current_timestamp
   FROM
       dim_inspectionstage s JOIN QDPST q
	   ON s.InspectionStageCode = q.QDPST_PRSTUFE
	   AND s.InspectionStageRuleCode = q.QDPST_DYNREGEL
 WHERE s.RowIsCurrent = 1;
		  
INSERT INTO dim_inspectionstage(dim_inspectionstageid, RowIsCurrent,rowstartdate,inspectionstagerulecode,inspectionstagecode,inspectionstagename)
SELECT 1, 1,current_timestamp,'Not Set',1,'Not Set'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionstage
               WHERE dim_inspectionstageid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectionstage';
   
insert into number_fountain
select 	'dim_inspectionstage',
	ifnull(max(d.dim_InspectionStageid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectionstage d
where d.dim_InspectionStageid <> 1; 



INSERT INTO dim_inspectionstage(dim_InspectionStageid,
                                          InspectionStageRuleCode,
                                          InspectionStageCode,
                                          InspectionStageName,
                                          RowStartDate,
                                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectionstage')
          + row_number() over(order by ''),
                 q.QDPST_DYNREGEL,
          q.QDPST_PRSTUFE,
          q.QDPST_KURZTEXT,
          current_timestamp,
          1
     FROM QDPST q
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionstage s
               WHERE     s.InspectionStageCode = q.QDPST_PRSTUFE
                     AND s.InspectionStageRuleCode = q.QDPST_DYNREGEL
                     AND s.RowIsCurrent = 1);
