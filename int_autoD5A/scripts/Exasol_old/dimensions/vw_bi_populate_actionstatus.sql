
INSERT INTO dim_ActionStatus(dim_ActionStatusid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_ActionStatus
               WHERE dim_ActionStatusid = 1);

UPDATE 	dim_ActionStatus das SET 	das.ActionStatus = ifnull(as1.MNEMONIC,'Not Set'),
		das.Description = ifnull(as1.DESCRIPTION,'Not Set'),
		das.DetailDescription = ifnull(as1.DETAIL_DESCRIPTION,'Not Set'),
		das.AlertFlag = as1.ALERT_FLAG,
		das.ExceptionFlag = as1.EXCEPTION_FLAG,
		das.CompleteFlag = as1.COMPLETE_FLAG,
		das.WaitFlag = as1.WAIT_FLAG,
		das.dw_update_date = current_timestamp
FROM  actionstatus as1, dim_ActionStatus das 
WHERE das.ActionStatusId = as1.ID AND das.RowIsCurrent = 1;
			   
delete from number_fountain m where m.table_name = 'dim_ActionStatus';

insert into number_fountain				   
select 	'dim_ActionStatus',
	ifnull(max(d.dim_ActionStatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ActionStatus d
where d.dim_ActionStatusid <> 1;
			   
INSERT INTO dim_ActionStatus(Dim_ActionStatusId,
                                ActionStatusId,
                             ActionStatus,
                             Description,
                             AlertFlag,
                             CompleteFlag,
                             DetailDescription,
                             ExceptionFlag,
                             WaitFlag,
                             RowIsCurrent,
                             RowStartDate)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_ActionStatus')
          + row_number() over(order by ''),
                  as1.ID,
          ifnull(MNEMONIC,'Not Set'),
          ifnull(DESCRIPTION,'Not Set'),
          ALERT_FLAG,
          COMPLETE_FLAG,
          ifnull(DETAIL_DESCRIPTION,'Not Set'),
          EXCEPTION_FLAG,
          WAIT_FLAG,
          1,
          current_timestamp
     FROM actionstatus as1
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_actionstatus
               WHERE ifnull(ActionStatusId,0) = as1.ID AND RowIsCurrent = 1);
			   
delete from number_fountain m where m.table_name = 'dim_ActionStatus';
			   
