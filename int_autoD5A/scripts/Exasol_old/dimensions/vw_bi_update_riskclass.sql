UPDATE dim_riskclass
   SET RiskClassText = ifnull(t.RISK_CLASS_TXT, 'Not Set')'
   FROM dim_riskclass, ukm_risk_cl0t t
 WHERE RiskClass = t.RISK_CLASS AND RowIsCurrent = 1
;
