/* Octavian: Every Angle changes */
drop table if exists KONV_SALES_PURCHASE_tmp;
create table KONV_SALES_PURCHASE_tmp AS 
select 
    KONV_KNUMV, 
	KONV_KPOSN, 
	KONV_STUNR, 
	KONV_ZAEHK, 
	KONV_WAERS, 
	KONV_STUFE, 
	KONV_KBETR, 
	KONV_KPEIN, 
	KONV_KSCHL, 
	KONV_KWERT, 
	KONV_KDATU, 
	KONV_KRECH, 
	KONV_KRUEK, 
	KONV_KNTYP, 
	KONV_KINAK, 
	KONV_KSTAT, 
	KONV_KAPPL, 
	KONV_KNUMH, 
	KONV_KOPOS, 
   'SALES' sourceflag from KONV_SALES s
 union
 select 	KONV_KNUMV, 
	KONV_KPOSN, 
	KONV_STUNR, 
	KONV_ZAEHK, 
	KONV_WAERS, 
	KONV_STUFE, 
	KONV_KBETR, 
	KONV_KPEIN, 
	KONV_KSCHL, 
	KONV_KWERT, 
	KONV_KDATU, 
	KONV_KRECH, 
	KONV_KRUEK, 
	KONV_KNTYP, 
	KONV_KINAK, 
	KONV_KSTAT, 
	KONV_KAPPL, 
	KONV_KNUMH, 
	KONV_KOPOS,
    'PURCHASE' sourceflag from KONV_PURCHASE p;
/* Octavian: Every Angle changes */

INSERT INTO dim_transactionpricecondition
(dim_transactionpriceconditionid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_transactionpricecondition where dim_transactionpriceconditionid = 1);

/*
delete from dim_transactionpricecondition tpcd where dim_transactionpriceconditionid <> 1
AND ConditionPricingDate >= ifnull((SELECT MIN(KONV_KDATU) FROM KONV), '9999-12-31')
AND NOT EXISTS (SELECT 1
                    FROM KONV k
					WHERE tpcd.ConditionCounter = ifnull(k.KONV_ZAEHK, '0')
					AND tpcd.ConditionItemNumber = ifnull(k.KONV_KPOSN, '0')
					AND tpcd.DocumentConditionNumber = ifnull(k.KONV_KNUMV, 'Not Set')
					AND tpcd.StepNumber = ifnull(k.KONV_STUNR, '0')
				) */


delete from number_fountain m where m.table_name = 'dim_transactionpricecondition';

insert into number_fountain
select 	'dim_transactionpricecondition',
	ifnull(max(d.dim_transactionpriceconditionid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_transactionpricecondition d
where d.dim_transactionpriceconditionid <> 1;	
				
INSERT INTO dim_transactionpricecondition(dim_transactionpriceconditionid,
								ConditionTypeCode,
								CalculationTypeCode,
								RelevantForAccrualCondition,
								StatisticsCondition,
								ConditionCategory ,
								InactiveCondition,
								ConditionRate,
								ConditionValue,
								DocumentConditionNumber,
								ConditionItemNumber,
								StepNumber,
								ConditionCounter,
								ApplicationCode,
								ConditionPricingDate,
								Conditionseqnumber,
								Conditionrecordnumber,
								RowStartDate,
								RowIsCurrent,
								sourceflag
								)
 SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_transactionpricecondition') 
          + row_number() over(order by ''), tp.*
 FROM (SELECT DISTINCT ifnull(k.KONV_KSCHL, 'Not Set'),
		ifnull(k.KONV_KRECH, 'Not Set'),
		ifnull(k.KONV_KRUEK, 'Not Set'),
		ifnull(k.KONV_KSTAT, 'Not Set'),
		ifnull(k.KONV_KNTYP , 'Not Set'),
		ifnull(k.KONV_KINAK , 'Not Set'),
		ifnull(k.KONV_KBETR, 0),
		ifnull(k.KONV_KWERT, 0),
		ifnull(k.KONV_KNUMV, 'Not Set'),
		ifnull(k.KONV_KPOSN, 0),
		ifnull(k.KONV_STUNR, 0),
		ifnull(k.KONV_ZAEHK, 0),
		ifnull(k.KONV_KAPPL, 'Not Set'),
		ifnull(k.KONV_KDATU, '0001-01-01'),
		ifnull(k.KONV_KOPOS,0),
		ifnull(k.KONV_KNUMH, 'Not Set'),
        current_timestamp,
         1,
		 sourceflag
     FROM KONV_SALES_PURCHASE_tmp k
    WHERE /* k.KONV_KSCHL NOT IN ('ZPRS') AND */ 
	NOT EXISTS
                (SELECT 1
                    FROM dim_transactionpricecondition tpcd
					WHERE tpcd.ConditionCounter = ifnull(k.KONV_ZAEHK, 0)
					AND tpcd.ConditionItemNumber = ifnull(k.KONV_KPOSN, 0)
					AND tpcd.DocumentConditionNumber = ifnull(k.KONV_KNUMV, 'Not Set')
					AND tpcd.StepNumber = ifnull(k.KONV_STUNR, 0))) tp	;

UPDATE  dim_transactionpricecondition tpc
SET	tpc.ConditionTypeCode = ifnull(k.KONV_KSCHL, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.ConditionTypeCode <> ifnull(k.KONV_KSCHL, 'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET tpc.CalculationTypeCode = ifnull(k.KONV_KRECH, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.CalculationTypeCode <> ifnull(k.KONV_KRECH, 'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET tpc.RelevantForAccrualCondition = ifnull(k.KONV_KRUEK, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.RelevantForAccrualCondition <> ifnull(k.KONV_KRUEK, 'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET  tpc.StatisticsCondition = ifnull(k.KONV_KSTAT, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.StatisticsCondition <> ifnull(k.KONV_KSTAT, 'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET tpc.ConditionCategory  = ifnull(k.KONV_KNTYP , 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.ConditionCategory  <> ifnull(k.KONV_KNTYP , 'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET tpc.InactiveCondition = ifnull(k.KONV_KINAK , 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND  tpc.InactiveCondition <> ifnull(k.KONV_KINAK , 'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET tpc.ConditionRate = ifnull(k.KONV_KBETR, '0'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.ConditionRate <> ifnull(k.KONV_KBETR, '0');

UPDATE  dim_transactionpricecondition tpc
SET tpc.ConditionValue = ifnull(k.KONV_KWERT, '0'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.ConditionValue <> ifnull(k.KONV_KWERT, '0');

UPDATE  dim_transactionpricecondition tpc
SET tpc.DocumentConditionNumber = ifnull(k.KONV_KNUMV, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.DocumentConditionNumber <> ifnull(k.KONV_KNUMV, 'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET tpc.ConditionItemNumber = ifnull(k.KONV_KPOSN, '0'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.ConditionItemNumber <> ifnull(k.KONV_KPOSN, '0');

/* This does not update anything
UPDATE  dim_transactionpricecondition tpc
FROM    KONV k
SET tpc.StepNumber = ifnull(k.KONV_STUNR, '0'),
			tpc.dw_update_date = current_timestamp
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.StepNumber <> ifnull(k.KONV_STUNR, '0')*/

UPDATE  dim_transactionpricecondition tpc
SET tpc.ConditionCounter = ifnull(k.KONV_ZAEHK, '0'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.ConditionCounter <> ifnull(k.KONV_ZAEHK, '0');

UPDATE  dim_transactionpricecondition tpc
SET tpc.ApplicationCode = ifnull(k.KONV_KAPPL, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.ApplicationCode <> ifnull(k.KONV_KAPPL, 'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET tpc.ConditionPricingDate = ifnull(k.KONV_KDATU, '0001-01-01'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.ConditionPricingDate <> ifnull(k.KONV_KDATU, '0001-01-01');

/*Georgiana 16 Feb 2016 EA Transition Addons*/
UPDATE  dim_transactionpricecondition tpc 
SET tpc.Conditionseqnumber = ifnull(k.KONV_KOPOS, '0'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.Conditionseqnumber <> ifnull(k.KONV_KOPOS, '0');

UPDATE  dim_transactionpricecondition tpc
SET tpc.Conditionrecordnumber = ifnull(k.KONV_KNUMH, '0'),
			tpc.dw_update_date = current_timestamp
FROM	KONV_SALES_PURCHASE_tmp k, dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND ifnull(k.KONV_ZAEHK, '0') = tpc.ConditionCounter
AND ifnull(k.KONV_KPOSN, '0') = tpc.ConditionItemNumber
AND ifnull(k.KONV_KNUMV, 'Not Set') = tpc.DocumentConditionNumber
AND ifnull(k.KONV_STUNR, '0') = tpc.StepNumber
AND tpc.Conditionrecordnumber <> ifnull(k.KONV_KNUMH, '0');


UPDATE  dim_transactionpricecondition tpc
SET  conditionrate_konp = ifnull(KONP_KBETR,0),
			tpc.dw_update_date = current_timestamp
FROM    KONH_KONP k,dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND Conditionrecordnumber = ifnull(k.KONH_KNUMH,'Not Set')
AND tpc.Conditionseqnumber = ifnull(k.KONP_KOPOS, 'Not Set')
AND conditionrate_konp <> ifnull(KONP_KBETR,0);

UPDATE  dim_transactionpricecondition tpc
SET  conditionunit = ifnull(KONP_KMEIN,'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM    KONH_KONP k,dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND Conditionrecordnumber = ifnull(k.KONH_KNUMH,'Not Set')
AND tpc.Conditionseqnumber = ifnull(k.KONP_KOPOS, 'Not Set')
AND conditionunit <> ifnull(KONP_KMEIN,'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET  rateunit = ifnull(KONP_KONWA,'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM    KONH_KONP k,dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND Conditionrecordnumber = ifnull(k.KONH_KNUMH,'Not Set')
AND tpc.Conditionseqnumber = ifnull(k.KONP_KOPOS, 'Not Set')
AND rateunit <> ifnull(KONP_KONWA,'Not Set');

UPDATE  dim_transactionpricecondition tpc
SET  conditionpriceunit = ifnull(KONP_KPEIN,0),
			tpc.dw_update_date = current_timestamp
FROM    KONH_KONP k,dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND Conditionrecordnumber = ifnull(k.KONH_KNUMH,'Not Set')
AND tpc.Conditionseqnumber = ifnull(k.KONP_KOPOS, 'Not Set')
AND conditionpriceunit <> ifnull(KONP_KPEIN,0);

UPDATE  dim_transactionpricecondition tpc
SET  conditiontype_konp = ifnull(KONH_KSCHL,'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM    KONH_KONP k,dim_transactionpricecondition tpc
WHERE tpc.RowIsCurrent = 1
AND Conditionrecordnumber = ifnull(k.KONH_KNUMH,'Not Set')
AND tpc.Conditionseqnumber = ifnull(k.KONP_KOPOS, 'Not Set')
AND conditiontype_konp <> ifnull(KONH_KSCHL,'Not Set');

/*End of Changes 16 Feb 2016*/

/* Update ConditionType */
UPDATE dim_transactionpricecondition tpc

SET tpc.ConditionType = ifnull(t.T685T_VTEXT, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM T685T t,dim_transactionpricecondition tpc
WHERE t.T685T_KSCHL = tpc.ConditionTypeCode
AND t.T685T_KAPPL = tpc.ApplicationCode
AND tpc.ConditionType <> ifnull(t.T685T_VTEXT, 'Not Set');

/* Update CalculationType */
UPDATE dim_transactionpricecondition tpc
SET tpc.CalculationType = ifnull(d.DD07T_DDTEXT, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM DD07T d,dim_transactionpricecondition tpc
WHERE d.DD07T_DOMVALUE = tpc.CalculationTypeCode
AND d.DD07T_DOMNAME = 'KRECH'
AND tpc.CalculationType <> ifnull(d.DD07T_DDTEXT, 'Not Set');

/* Update Application */
UPDATE dim_transactionpricecondition tpc

SET tpc.Application = ifnull(t.T681B_VTEXT, 'Not Set'),
			tpc.dw_update_date = current_timestamp
FROM T681B t,dim_transactionpricecondition tpc
WHERE t.T681B_KAPPL = tpc.ApplicationCode
AND tpc.Application <> ifnull(t.T681B_VTEXT, 'Not Set');



