

UPDATE dim_plant 
   SET CompanyCode = k.BUKRS,
       Name = t.NAME1,
       PostalCode = ifnull(PSTLZ, 'Not Set'),
       City = ifnull(ORT01, 'Not Set'),
       "state" = ifnull(REGIO, 'Not Set'),
       Country = ifnull(LAND1, 'Not Set'),
       SalesOrg = case when SalesOrg = 'Not Set' then SalesOrg
 else ifnull(T001W_VKORG, 'Not Set') end,
       PurchOrg =
          CASE
             WHEN PurchOrg = 'Not Set' THEN PurchOrg
             ELSE ifnull(EKORG, WERKS)
          END,
       PlanningPlant = ifnull(T001W_IWERK, 'Not Set'),
       ValuationArea = t.BWKEY,
       FactoryCalendarKey = ifnull(t.FABKL, 'Not Set'),
       LanguageKey = ifnull(t.SPRAS, 'E'),
       CountryName = ifnull(t005t_landx, 'Not Set'),
       dw_update_date = current_timestamp
 FROM T001W t, t001k k, T005T,  dim_plant 
 WHERE t.bwkey = k.bwkey AND PlantCode = t.werks AND t.LAND1 = T005T_LAND1;