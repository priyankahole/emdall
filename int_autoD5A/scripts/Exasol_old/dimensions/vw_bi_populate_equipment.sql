/* ##################################################################################################################*/
/*	Script         : vw_bi_populate_equipment																		*/
/*     Created By     : Amar															*/
/*     Created On     : 12 March 2015													*/
/*     Change History																	*/
/*     Date            By        Version           Desc									*/
/*	 3/12/2015		 Amar	   1				 Multi-Statement changed to Script		*/
/*#################################################################################################################### */


INSERT INTO dim_equipment(Dim_equipmentid,
				Equipment,
			       TechnicalDescription,
                               Language,
			       EquipmentDescription,
                               RowStartDate,
                               RowIsCurrent)
SELECT 			1,
				'Not Set',
				'Not Set',
				'EN',
				'Not Set',
				current_timestamp,
				1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_equipment WHERE dim_equipmentId = 1);

							   
DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_equipment';

INSERT INTO NUMBER_FOUNTAIN
select 	'dim_equipment',
		ifnull(max(d.dim_equipmentId), 
		ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_equipment d
where d.dim_equipmentId <> 1;

INSERT INTO dim_equipment(Dim_equipmentid,
				Equipment,
			       TechnicalDescription,
                               Language,
			       EquipmentDescription,
                               RowStartDate,
                               RowIsCurrent)
   SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_equipment') 
          + row_number() over(order by '') ,
	    EQKT_EQUNR,
	    ifnull(EQKT_EQKTX, 'Not Set'),
            EQKT_SPRAS,
	    ifnull(EQKT_EQKTU, 'Not Set'),
            current_timestamp,
            1			 
       FROM EQKT
      WHERE EQKT_SPRAS = 'E'
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_equipment
                    WHERE equipment = EQKT_EQUNR AND language = EQKT_SPRAS);
					
					

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_equipment';

/*columns asked to be added when Equipment SA was requested*/
update dim_equipment a
set a.deletion_flag = ifnull(b.EQUI_LVORM,'Not Set')
from EQUI b,dim_equipment a
where a.EquipmentNo = ifnull(b.equi_equnr ,'Not Set')
and a.deletion_flag <> ifnull(b.EQUI_LVORM,'Not Set');		

update dim_equipment a
set a.aquisition_value = ifnull(b.EQUI_ANSWT,0)
from EQUI b,dim_equipment a
where a.EquipmentNo = ifnull(b.equi_equnr ,'Not Set')
and a.aquisition_value <> ifnull(b.EQUI_ANSWT,0);	

update dim_equipment a
set a.currency = ifnull(b.EQUI_WAERS, 'Not Set')
from EQUI b,dim_equipment a
where a.EquipmentNo = ifnull(b.equi_equnr ,'Not Set')
and a.currency <> ifnull(b.EQUI_WAERS, 'Not Set');

update dim_equipment a
set a.superior_equipment = ifnull(b.EQUZ_HEQUI,'Not Set')
from EQUZ b,dim_equipment a
where a.EquipmentNo = ifnull(b.equz_equnr ,'Not Set')
and a.EquipmentValidtoDate = ifnull(b.equz_datbi,'0001-01-01')
and  a.EquipUsagePeriods = ifnull(b.equz_eqlfn,0)
and a.superior_equipment <> ifnull(b.EQUZ_HEQUI,'Not Set');

update dim_equipment a
set a.equi_pos_at_install_loc = ifnull(b.EQUZ_HEQNR,'Not Set')
from EQUZ b,dim_equipment a
where a.EquipmentNo = ifnull(b.equz_equnr ,'Not Set')
and a.EquipmentValidtoDate = ifnull(b.equz_datbi,'0001-01-01')
and  a.EquipUsagePeriods = ifnull(b.equz_eqlfn,0)
and a.equi_pos_at_install_loc <> ifnull(b.EQUZ_HEQNR,'Not Set');