/*INSERT DEFAULT*/
INSERT INTO dim_chargebacktypes(dim_chargebacktypesid,

							AccrualIndicator,
							ApprovalRequired,
							ItemIncrementNumber,
							StartItem,
							Application,
							ConditionSearchProfile,
							CustomerSubscreen,
							DebitCreditIndicator,
							HeaderDisplayProfile,
							ItemDisplayProfile,
							"group",
							Type,
							LogRangeIntervalNumber,
							LockRecipientForSettlement,
							LogAccruals,
							LogSettlements,
							LogParking,
							DocumentPhysicalDeletionNumber ,
							RangeNumber,
							AggrItemsPartRole,
							ProcessCancelledInvoices,
							PartnerCommunicationRelevance,
							ParallelUoMCheck,
							PaymentScheduleSettle,
							ToleranceGroup,
							UpdateBillbackClearing,
							InformationStructureVersionNumber,
							ListDisplaysAuthorizationCheck,

                            RowStartDate,
                            RowEndDate,
                            RowIsCurrent,
                            RowChangeReason)
   SELECT 1,
   
		'Not Set',
		'Not Set',
		'0',
		'0',
		'Not Set',
		'Not Set',
		'0',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',

		current_timestamp,
        NULL,
        1,
        NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_chargebacktypes
               WHERE dim_chargebacktypesid = 1);
			   

/*UPDATE*/			   
UPDATE    dim_chargebacktypes cbt

   SET 		cbt.AccrualIndicator = ifnull(itt.IRM_TIPTYP_ACIND, 'Not Set'),
			cbt.ApprovalRequired = ifnull(itt.IRM_TIPTYP_APRCK, 'Not Set'),
			cbt.ItemIncrementNumber = ifnull(itt.IRM_TIPTYP_BMITI, '0'),
			cbt.StartItem = ifnull(itt.IRM_TIPTYP_BMSTI, '0'),
			cbt.Application = ifnull(itt.IRM_TIPTYP_CLASS, 'Not Set'),
			cbt.ConditionSearchProfile = ifnull(itt.IRM_TIPTYP_CNDSP, 'Not Set'),
			cbt.CustomerSubscreen = ifnull(itt.IRM_TIPTYP_CSSCR, '0'),
			cbt.DebitCreditIndicator = ifnull(itt.IRM_TIPTYP_DCIND, 'Not Set'),
			cbt.HeaderDisplayProfile = ifnull(itt.IRM_TIPTYP_DPHDR, 'Not Set'),
			cbt.ItemDisplayProfile = ifnull(itt.IRM_TIPTYP_DPITM, 'Not Set'),
			cbt."group" = ifnull(itt.IRM_TIPTYP_IPGRP, 'Not Set'),
			cbt.Type = ifnull(itt.IRM_TIPTYP_IPTYP, 'Not Set'),
			cbt.LogRangeIntervalNumber = ifnull(itt.IRM_TIPTYP_LGNRI, 'Not Set'),
			cbt.LockRecipientForSettlement = ifnull(itt.IRM_TIPTYP_LOCKR, 'Not Set'),
			cbt.LogAccruals = ifnull(itt.IRM_TIPTYP_LOGAC, 'Not Set'),
			cbt.LogSettlements = ifnull(itt.IRM_TIPTYP_LOGSE, 'Not Set'),
			cbt.LogParking = ifnull(itt.IRM_TIPTYP_LOGSP, 'Not Set'),
			cbt.DocumentPhysicalDeletionNumber  = ifnull(itt.IRM_TIPTYP_NOPDEL, 'Not Set'),
			cbt.RangeNumber = ifnull(itt.IRM_TIPTYP_NRRANGENR, 'Not Set'),
			cbt.AggrItemsPartRole = ifnull(itt.IRM_TIPTYP_PARVW, 'Not Set'),
			cbt.ProcessCancelledInvoices = ifnull(itt.IRM_TIPTYP_PCINV, 'Not Set'),
			cbt.PartnerCommunicationRelevance = ifnull(itt.IRM_TIPTYP_PCREL, 'Not Set'),
			cbt.ParallelUoMCheck = ifnull(itt.IRM_TIPTYP_PUOMCK, 'Not Set'),
			cbt.PaymentScheduleSettle = ifnull(itt.IRM_TIPTYP_SPSCH, 'Not Set'),
			cbt.ToleranceGroup = ifnull(itt.IRM_TIPTYP_TLGRP, 'Not Set'),
			cbt.UpdateBillbackClearing = ifnull(itt.IRM_TIPTYP_UPCLR, 'Not Set'),
			cbt.InformationStructureVersionNumber = ifnull(itt.IRM_TIPTYP_VRSIO, 'Not Set'),
			cbt.ListDisplaysAuthorizationCheck = ifnull(itt.IRM_TIPTYP_XAUTH, 'Not Set'),
			cbt.dw_update_date = current_timestamp
       FROM
          IRM_TIPTYP  itt, dim_chargebacktypes cbt
   WHERE cbt.Type = itt.IRM_TIPTYP_IPTYP;

delete from number_fountain m where m.table_name = 'dim_chargebacktypes';

insert into number_fountain
select 	'dim_chargebacktypes',
	ifnull(max(d.dim_chargebacktypesid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_chargebacktypes d
where d.dim_chargebacktypesid <> 1;
   
/*INSERT*/   
INSERT INTO dim_chargebacktypes(dim_chargebacktypesid,
                          				
							AccrualIndicator,
							ApprovalRequired,
							ItemIncrementNumber,
							StartItem,
							Application,
							ConditionSearchProfile,
							CustomerSubscreen,
							DebitCreditIndicator,
							HeaderDisplayProfile,
							ItemDisplayProfile,
							"group",
							Type,
							LogRangeIntervalNumber,
							LockRecipientForSettlement,
							LogAccruals,
							LogSettlements,
							LogParking,
							DocumentPhysicalDeletionNumber ,
							RangeNumber,
							AggrItemsPartRole,
							ProcessCancelledInvoices,
							PartnerCommunicationRelevance,
							ParallelUoMCheck,
							PaymentScheduleSettle,
							ToleranceGroup,
							UpdateBillbackClearing,
							InformationStructureVersionNumber,
							ListDisplaysAuthorizationCheck,
							
                            RowStartDate,
                            RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_chargebacktypes') 
          + row_number() over(order by ''),
		  
		ifnull(itt.IRM_TIPTYP_ACIND , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_APRCK , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_BMITI , '0'),
		ifnull(itt.IRM_TIPTYP_BMSTI , '0'),
		ifnull(itt.IRM_TIPTYP_CLASS , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_CNDSP , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_CSSCR , '0'),
		ifnull(itt.IRM_TIPTYP_DCIND , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_DPHDR , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_DPITM , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_IPGRP , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_IPTYP , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_LGNRI , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_LOCKR , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_LOGAC , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_LOGSE , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_LOGSP , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_NOPDEL , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_NRRANGENR , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_PARVW , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_PCINV , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_PCREL , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_PUOMCK , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_SPSCH , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_TLGRP , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_UPCLR , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_VRSIO , 'Not Set'),
		ifnull(itt.IRM_TIPTYP_XAUTH , 'Not Set'),
	  
		  current_timestamp,
          1
     FROM IRM_TIPTYP  itt
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_chargebacktypes cbtt
               WHERE cbtt.Type = itt.IRM_TIPTYP_IPTYP);

delete from number_fountain m where m.table_name = 'dim_chargebacktypes';
			   