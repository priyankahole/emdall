
UPDATE dim_ActionType dat 
   SET dat.ActionMnemonic = ifnull(aty.ACTION_MNEMONIC,'Not Set'),
       dat.Description = ifnull(aty.DESCRIPTION,'Not Set'),
       dat.DueByPeriod = aty.DUE_BY_PERIOD,
       dat.MatchingEnabled = aty.Matching_enabled
from actiontype aty, dim_ActionType dat 
 WHERE dat.ActionTypeId = aty.ACTION_TYPE AND dat.RowIsCurrent = 1;
