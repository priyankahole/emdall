insert into dim_controlkey (dim_controlkeyid,
                          applicationtasklist,
                          controlkey,
                          description
						  )
select 1, 'Not Set', 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_controlkey where dim_controlkeyid = 1); 


delete from number_fountain m where m.table_name = 'dim_controlkey';
insert into number_fountain
select 'dim_controlkey',
 ifnull(max(d.dim_controlkeyid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_controlkey d
where d.dim_controlkeyid <> 1;

insert into dim_controlkey (dim_controlkeyid,
                          applicationtasklist,
                          controlkey,
                          description)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_controlkey') + row_number() over(order by '') AS dim_controlkeyid,
       ifnull(t.T430T_PLNAW,'Not Set') AS applicationtasklist,
       ifnull(t.T430T_STEUS,'Not Set') AS controlkey,
       ifnull(t.T430T_TXT,'Not Set') AS description
from T430T t
 where not exists (select 'x' from dim_controlkey c where 
	      c.applicationtasklist = ifnull(t.T430T_PLNAW,'Not Set')
	  AND c.controlkey = ifnull(t.T430T_STEUS,'Not Set'))
	  AND T430T_SPRAS = 'E';
	  

update dim_controlkey c
set c.description = ifnull(t.T430T_TXT, 'Not Set'),
    c.dw_update_date = current_timestamp
from T430T t,dim_controlkey c
where c.applicationtasklist = ifnull(t.T430T_PLNAW,'Not Set')
AND c.controlkey = ifnull(t.T430T_STEUS,'Not Set')
AND T430T_SPRAS = 'E'
and c.description <> ifnull(t.T430T_TXT, 'Not Set');