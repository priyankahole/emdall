UPDATE    dim_billingdocumenttype bdt
		SET bdt.Description = ifnull(t.TVFKT_VTEXT,'Not Set'),
			bdt.dw_update_date = current_timestamp
	    FROM
          dim_billingdocumenttype bdt, tvfkt t
    WHERE t.TVFKT_FKART = bdt.Type
   AND bdt.RowIsCurrent = 1;