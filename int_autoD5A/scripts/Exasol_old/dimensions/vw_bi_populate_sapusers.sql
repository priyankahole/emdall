INSERT INTO dim_SapUsers(dim_SapUsersid,
							 logind,
                             name_first,
                             name_last,
							 name_text)
   SELECT 1,
          'Not Set',
          'Not Set',
		  'Not Set',
		  'Not Set' 
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_SapUsers
               WHERE dim_SapUsersid = 1);

UPDATE    dim_SapUsers a
   SET a.name_first = ifnull(t.name_first, 'Not Set'),
			a.dw_update_date = current_timestamp
       FROM
          v_usr_name t, dim_SapUsers a
WHERE a.name_first <> t.name_first  AND a.logind = t.bname;

UPDATE    dim_SapUsers a
   SET a.name_last = ifnull(t.name_last, 'Not Set'),
			a.dw_update_date = current_timestamp
 FROM
          v_usr_name t, dim_SapUsers a
WHERE a.name_last <> t.name_last  AND a.logind = t.bname;

UPDATE    dim_SapUsers a
   SET a.name_text = ifnull(t.name_text, 'Not Set'),
			a.dw_update_date = current_timestamp
          FROM
          v_usr_name t, dim_SapUsers a
WHERE a.name_text <> t.name_text  AND a.logind = t.bname;

delete from number_fountain m where m.table_name = 'dim_SapUsers';

insert into number_fountain
select 	'dim_SapUsers',
	ifnull(max(d.dim_SapUsersid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_SapUsers d
where d.dim_SapUsersid <> 1; 
insert into dim_SapUsers
	(dim_SapUsersid,
	 logind,
	 name_first,
	 name_last,
	 name_text)
 SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_SapUsers') 
          + row_number() over(order by ''),
			  bname,
			  ifnull(name_first,'Not Set'),
			  ifnull(name_last,'Not Set'),
			  ifnull(name_text,'Not Set')
from v_usr_name
where bname is not null
	and not exists (select 1 from dim_SapUsers a where a.logind = bname) ;
