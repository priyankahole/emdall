insert into dim_inspcatalogselectedsets
(dim_inspcatalogselectedsetsid,
Plant,
"catalog",
SelectedStes,
ShortDescription,
Status
)
select 	1, 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_inspcatalogselectedsets where dim_inspcatalogselectedsetsid = 1);

delete from number_fountain m where m.table_name = 'dim_inspcatalogselectedsets';
insert into number_fountain
select 'dim_inspcatalogselectedsets',
 ifnull(max(d.dim_inspcatalogselectedsetsid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspcatalogselectedsets d
where d.dim_inspcatalogselectedsetsid <> 1;

insert into dim_inspcatalogselectedsets
(dim_inspcatalogselectedsetsid,
Plant,
"catalog",
SelectedStes,
ShortDescription,
Status
)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_inspcatalogselectedsets') + row_number() over(order by '') AS dim_inspcatalogselectedsetsid,
 ifnull(q.QPAM_WERKS, 'Not Set') as Plant,
 ifnull(q.QPAM_KATALOGART, 'Not Set') as "catalog",
 ifnull(q.QPAM_AUSWAHLMGE, 'Not Set') as SelectedStes,
 ifnull(q.QPAM_KTX01, 'Not Set') as ShortDescription,
 ifnull(q.QPAM_STATUS, 'Not Set') as Status

 From QPAM q
 where not exists (select 'x' from dim_inspcatalogselectedsets di
                       where  di.Plant = ifnull(q.QPAM_WERKS, 'Not Set')
                               AND di."catalog" = ifnull(q.QPAM_KATALOGART, 'Not Set') 
                               AND di.SelectedStes = ifnull(q.QPAM_AUSWAHLMGE, 'Not Set') );
							  
UPDATE dim_inspcatalogselectedsets di
SET di.ShortDescription = ifnull(q.QPAM_KTX01, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPAM q,dim_inspcatalogselectedsets di
WHERE di.Plant = ifnull(q.QPAM_WERKS, 'Not Set')
      AND di."catalog" = ifnull(q.QPAM_KATALOGART, 'Not Set') 
      AND di.SelectedStes = ifnull(q.QPAM_AUSWAHLMGE, 'Not Set')  
      AND di.ShortDescription <> ifnull(q.QPAM_KTX01, 'Not Set');	
	  
UPDATE dim_inspcatalogselectedsets di
SET di.Status = ifnull(q.QPAM_STATUS, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPAM q,dim_inspcatalogselectedsets di
WHERE di.Plant = ifnull(q.QPAM_WERKS, 'Not Set')
      AND di."catalog" = ifnull(q.QPAM_KATALOGART, 'Not Set') 
      AND di.SelectedStes = ifnull(q.QPAM_AUSWAHLMGE, 'Not Set')  
      AND di.Status <> ifnull(q.QPAM_STATUS, 'Not Set');	

	  
 



