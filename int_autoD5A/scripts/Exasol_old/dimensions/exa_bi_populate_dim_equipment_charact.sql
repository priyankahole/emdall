/*##################################################################################################################
	Script         : exa_bi_populate_dim_equipment_charact.sql                                                                                                                                            
	Created By     : Alin                                                                                                        
	Created On     : 14 Dec 20167 
	Description    : Script to populated Equipment Characteristic dimension
			Grain :  Internal characteristic (AUSP-ATINN)
					 Key of object to be classified (AUSP-OBJEK)
					 Characteristic value counter (AUSP-ATZHL)
					 Indicator: Object/Class (AUSP-MAFID)
					 Class Type (AUSP-KLART)
					 Internal counter for archiving objects via engin. chg. mgmt (AUSP-ADZHL)

	Change History                                                                                                                                   
	Date            By        	Version		Desc                                                                 
	14Dec2017	Alin	1.1		First draft
####################################################################################################################*/ 

/*DEFAULT ROW*/

INSERT INTO dim_equi_charact(
	DIM_EQUI_CHARACTID,
	key_of_object,
char_value_counter,
class_type,
indicator_obj_class,
 	INTERNAL_CHARACTERISTIC,
	INTERNAL_COUNTER,
	CHARACTERISTIC_VALUE,
	INTERNAL_FLPOINT_FROM,
	INTERNAL_FLPOINT_TO,
	CHARACTERISTIC_NAME,
	CHARACTERISTIC_DESCRIPTION
)
  SELECT    
	1,
	'Not Set',
	0,
	'Not Set',
	'Not Set',
	0,
	0,
	'Not Set',
	0,
	0,
	'Not Set',
	'Not Set'
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_equi_charact WHERE dim_equi_charactid = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_equi_charact';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_equi_charact',
                ifnull(max(d.dim_equi_charactid),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_equi_charact d
where d.dim_equi_charactid <> 1;


drop table if exists tmp_dim_equi_charact;
create table tmp_dim_equi_charact as
SELECT 
*
FROM AUSP_CHARACT a, CABN c, CABNT t
where a.AUSP_ATINN = c.CABN_ATINN
and c.CABN_ATINN = t.CABNT_ATINN;
/*
and not exists (select 1 from dim_equi_charact d
WHERE ifnull(a.AUSP_OBJEK, 'Not Set') = key_of_object
AND ifnull(a.AUSP_ATZHL, 0) = char_value_counter
AND ifnull(a.AUSP_KLART, 'Not Set') = class_type
AND ifnull(a.AUSP_MAFID, 'Not Set') = indicator_obj_class
AND IFNULL(a.AUSP_ATINN, 0) = INTERNAL_CHARACTERISTIC
AND IFNULL(a.AUSP_ADZHL,0) = INTERNAL_COUNTER
)*/

/*insert new rows*/

INSERT INTO dim_equi_charact(
dim_equi_charactid,
key_of_object,
char_value_counter,
class_type,
indicator_obj_class,
INTERNAL_CHARACTERISTIC,
INTERNAL_COUNTER,
CHARACTERISTIC_VALUE,
INTERNAL_FLPOINT_FROM,
INTERNAL_FLPOINT_TO,
CHARACTERISTIC_NAME,
CHARACTERISTIC_DESCRIPTION
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_equi_charact') + row_number() over(order by '') as dim_equi_charactid,
ifnull(t.AUSP_OBJEK, 'Not Set') as key_of_object,
ifnull(t.AUSP_ATZHL, 0) as char_value_counter,
ifnull(t.AUSP_KLART, 'Not Set') as class_type,
ifnull(t.AUSP_MAFID, 'Not Set') as indicator_obj_class,
IFNULL(t.AUSP_ATINN, 0) AS INTERNAL_CHARACTERISTIC,
IFNULL(t.AUSP_ADZHL,0) AS INTERNAL_COUNTER,
IFNULL(t.AUSP_ATWRT, 'Not Set') AS CHARACTERISTIC_VALUE,
IFNULL(t.AUSP_ATFLV, 0) AS INTERNAL_FLPOINT_FROM,
IFNULL(t.AUSP_ATFLB, 0) AS INTERNAL_FLPOINT_TO,
IFNULL(t.CABN_ATNAM, 'Not Set') AS CHARACTERISTIC_NAME,
IFNULL(t.CABNT_ATBEZ, 'Not Set') AS CHARACTERISTIC_DESCRIPTION
FROM tmp_dim_equi_charact t
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_equi_charact d
        where ifnull(t.AUSP_OBJEK, 'Not Set') = d.key_of_object
		AND ifnull(t.AUSP_ATZHL, 0) = d.char_value_counter
		AND ifnull(t.AUSP_KLART, 'Not Set') = d.class_type
		AND ifnull(t.AUSP_MAFID, 'Not Set') = d.indicator_obj_class
		AND IFNULL(t.AUSP_ATINN, 0) = d.INTERNAL_CHARACTERISTIC
		AND IFNULL(t.AUSP_ADZHL,0) = d.INTERNAL_COUNTER);

/*update existing rows*/
/*
update dim_equi_charact d
set CHARACTERISTIC_VALUE = IFNULL(t.AUSP_ATWRT, 'Not Set')
FROM dim_equi_charact d,  tmp_dim_equi_charact t
where ifnull(t.AUSP_OBJEK, 'Not Set') = d.key_of_object
AND ifnull(t.AUSP_ATZHL, 0) = d.char_value_counter
AND ifnull(t.AUSP_KLART, 'Not Set') = d.class_type
AND ifnull(t.AUSP_MAFID, 'Not Set') = d.indicator_obj_class
AND IFNULL(t.AUSP_ATINN, 0) = d.INTERNAL_CHARACTERISTIC
AND IFNULL(t.AUSP_ADZHL,0) = d.INTERNAL_COUNTER
and  CHARACTERISTIC_VALUE <> IFNULL(t.AUSP_ATWRT, 'Not Set')
*/
MERGE INTO dim_equi_charact d
USING (
SELECT DISTINCT d.dim_equi_charactid,
IFNULL(t.AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
FROM dim_equi_charact d, tmp_dim_equi_charact t
WHERE
ifnull(t.AUSP_OBJEK, 'Not Set') = d.key_of_object
AND ifnull(t.AUSP_ATZHL, 0) = d.char_value_counter
AND ifnull(t.AUSP_KLART, 'Not Set') = d.class_type
AND ifnull(t.AUSP_MAFID, 'Not Set') = d.indicator_obj_class
AND IFNULL(t.AUSP_ATINN, 0) = d.INTERNAL_CHARACTERISTIC
AND IFNULL(t.AUSP_ADZHL,0) = d.INTERNAL_COUNTER
AND IFNULL(t.class,'Not Set')=d.Equipment_Class
AND IFNULL(t.KSCHL,'Not Set')=d.Equipment_Class_DESC
)x
ON d.dim_equi_charactid = x.dim_equi_charactid
WHEN MATCHED THEN UPDATE SET
d.CHARACTERISTIC_VALUE = IFNULL(x.AUSP_ATWRT, 'Not Set')
WHERE
d.CHARACTERISTIC_VALUE <> IFNULL(x.AUSP_ATWRT, 'Not Set');

update dim_equi_charact d
set INTERNAL_FLPOINT_FROM = IFNULL(t.AUSP_ATFLV, 0)
FROM dim_equi_charact d,  tmp_dim_equi_charact t
where ifnull(t.AUSP_OBJEK, 'Not Set') = d.key_of_object
AND ifnull(t.AUSP_ATZHL, 0) = d.char_value_counter
AND ifnull(t.AUSP_KLART, 'Not Set') = d.class_type
AND ifnull(t.AUSP_MAFID, 'Not Set') = d.indicator_obj_class
AND IFNULL(t.AUSP_ATINN, 0) = d.INTERNAL_CHARACTERISTIC
AND IFNULL(t.AUSP_ADZHL,0) = d.INTERNAL_COUNTER
and  INTERNAL_FLPOINT_FROM <> IFNULL(t.AUSP_ATFLV, 0);


update dim_equi_charact d
set INTERNAL_FLPOINT_TO = IFNULL(t.AUSP_ATFLB, 0)
FROM dim_equi_charact d,  tmp_dim_equi_charact t
where ifnull(t.AUSP_OBJEK, 'Not Set') = d.key_of_object
AND ifnull(t.AUSP_ATZHL, 0) = d.char_value_counter
AND ifnull(t.AUSP_KLART, 'Not Set') = d.class_type
AND ifnull(t.AUSP_MAFID, 'Not Set') = d.indicator_obj_class
AND IFNULL(t.AUSP_ATINN, 0) = d.INTERNAL_CHARACTERISTIC
AND IFNULL(t.AUSP_ADZHL,0) = d.INTERNAL_COUNTER
and  INTERNAL_FLPOINT_TO <> IFNULL(t.AUSP_ATFLB, 0);


update dim_equi_charact d
set CHARACTERISTIC_NAME = IFNULL(t.CABN_ATNAM, 'Not Set')
FROM dim_equi_charact d,  tmp_dim_equi_charact t
where ifnull(t.AUSP_OBJEK, 'Not Set') = d.key_of_object
AND ifnull(t.AUSP_ATZHL, 0) = d.char_value_counter
AND ifnull(t.AUSP_KLART, 'Not Set') = d.class_type
AND ifnull(t.AUSP_MAFID, 'Not Set') = d.indicator_obj_class
AND IFNULL(t.AUSP_ATINN, 0) = d.INTERNAL_CHARACTERISTIC
AND IFNULL(t.AUSP_ADZHL,0) = d.INTERNAL_COUNTER
and  CHARACTERISTIC_NAME <> IFNULL(t.CABN_ATNAM, 'Not Set');

update dim_equi_charact d
set CHARACTERISTIC_DESCRIPTION = IFNULL(t.CABNT_ATBEZ, 'Not Set')
FROM dim_equi_charact d,  tmp_dim_equi_charact t
where ifnull(t.AUSP_OBJEK, 'Not Set') = d.key_of_object
AND ifnull(t.AUSP_ATZHL, 0) = d.char_value_counter
AND ifnull(t.AUSP_KLART, 'Not Set') = d.class_type
AND ifnull(t.AUSP_MAFID, 'Not Set') = d.indicator_obj_class
AND IFNULL(t.AUSP_ATINN, 0) = d.INTERNAL_CHARACTERISTIC
AND IFNULL(t.AUSP_ADZHL,0) = d.INTERNAL_COUNTER
and  CHARACTERISTIC_DESCRIPTION <> IFNULL(t.CABNT_ATBEZ, 'Not Set');

--drop table if exists tmp_dim_equi_charact

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_equi_charact'; 