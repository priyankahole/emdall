UPDATE    dim_inspectionseverity isv
      SET isv.InspectionSeverityName = q.QDEPT_KURZTEXT,
			isv.dw_update_date = current_timestamp
	  FROM
          dim_inspectionseverity isv,QDEPT q
 WHERE isv.RowIsCurrent = 1
 AND isv.InspectionSeverityCode = q.QDEPT_PRSCHAERFE;
