/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_certifcharacttext_dim.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 30 May 2016 */
/*  */
/*  */
/*   Description    : Populate Certificates characteristic text Dimension */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */

insert into dim_certifcharacttext (dim_certifcharacttextid, characteristicshorttext, descCharactText)
select 1, 'Not Set', 'Not Set'
from ( select 1) a
where not exists ( select 'x' from dim_certifcharacttext where dim_certifcharacttextid = 1);

delete from number_fountain m where m.table_name = 'dim_certifcharacttext';

insert into number_fountain
select 'dim_certifcharacttext',
		ifnull( max(d.dim_certifcharacttextid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
		from dim_certifcharacttext d
		where dim_certifcharacttextid <> 1;

insert into dim_certifcharacttext
	(
		dim_certifcharacttextid,
		characteristicshorttext
	)
	select ( select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_certifcharacttext') + row_number() over(order by '') AS dim_certifcharacttextid,
			ifnull(TQ64T_KZHERKTEXT, 'Not Set') as characteristicshorttext
	from TQ64T t
	where not exists (select 1 from dim_certifcharacttext c
    				  where c.characteristicshorttext = ifnull(t.TQ64T_KZHERKTEXT, 'Not Set'));

update dim_certifcharacttext c
set c.descCharactText = ifnull(t.TQ64T_KURZTEXT, 'Not Set'),
	dw_update_date = current_timestamp 
from TQ64T t,dim_certifcharacttext c
where c.characteristicshorttext = ifnull(TQ64T_KZHERKTEXT, 'Not Set')
	and c.descCharactText <> ifnull(t.TQ64T_KURZTEXT, 'Not Set');
