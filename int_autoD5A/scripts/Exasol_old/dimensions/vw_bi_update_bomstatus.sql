

UPDATE    dim_bomstatus bs

   SET bs.Description = ifnull(T415T_STTXT, 'Not Set')
       FROM
          T415T t, dim_bomstatus bs

 WHERE bs.RowIsCurrent = 1
 AND bs.bomstatuscode = T415T_STLST;
