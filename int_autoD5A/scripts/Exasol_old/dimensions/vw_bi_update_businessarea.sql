
UPDATE    dim_businessarea ba
       SET ba.Description = t.TGSBT_GTEXT,
			ba.dw_update_date = current_timestamp
		FROM
          dim_businessarea ba, TGSBT t
 WHERE ba.RowIsCurrent = 1
 AND ba.BusinessArea = t.TGSBT_GSBER;
