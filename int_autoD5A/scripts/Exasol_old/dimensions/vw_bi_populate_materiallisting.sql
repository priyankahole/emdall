/* Octavian: Every Angle Transition Addons */
insert into dim_materiallisting (dim_materiallistingid,
                          application,
                          matlisting,
						  salesorg,
						  distributionchannel,
						  customer,
						  materialnumber,
						  validenddate
						  )
select 1, 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set' , '0001-01-01'
from (select 1) a
where not exists ( select 'x' from dim_materiallisting where dim_materiallistingid = 1); 


delete from number_fountain m where m.table_name = 'dim_materiallisting';
insert into number_fountain
select 'dim_materiallisting',
 ifnull(max(d.dim_materiallistingid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_materiallisting d
where d.dim_materiallistingid <> 1;

insert into dim_materiallisting (dim_materiallistingid,
                          application,
                          matlisting,
						  salesorg,
						  distributionchannel,
						  customer,
						  materialnumber,
						  validenddate,
						  dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_materiallisting') + row_number() over(order by '') AS dim_materiallistingid,
       ifnull(kg.KOTG004_KAPPL,'Not Set') AS application,
	   ifnull(kg.KOTG004_KSCHL,'Not Set') AS matlisting,
	   ifnull(kg.KOTG004_VKORG,'Not Set') AS salesorg,
	   ifnull(kg.KOTG004_VTWEG,'Not Set') AS distributionchannel,
	   ifnull(kg.KOTG004_KUNNR,'Not Set') AS customer,
	   ifnull(kg.KOTG004_MATNR,'Not Set') AS materialnumber,
	   ifnull(kg.KOTG004_DATBI,'0001-01-01') AS validenddate,
       current_timestamp
from KOTG004 kg
 where not exists (select 'x' from dim_materiallisting ml where 
	      ml.application = ifnull(kg.KOTG004_KAPPL,'Not Set')
	  and ml.matlisting = ifnull(kg.KOTG004_KSCHL,'Not Set')
	  and ml.salesorg = ifnull(kg.KOTG004_VKORG,'Not Set')
	  and ml.distributionchannel = ifnull(kg.KOTG004_VTWEG,'Not Set')
	  and ml.customer = ifnull(kg.KOTG004_KUNNR,'Not Set')
	  and ml.materialnumber = ifnull(kg.KOTG004_MATNR,'Not Set')
	  and ml.validenddate = ifnull(kg.KOTG004_DATBI,'0001-01-01'));
	  

update dim_materiallisting ml
set ml.validstartdate = ifnull(kg.KOTG004_DATAB, '0001-01-01'),
    ml.dw_update_date = current_timestamp
from KOTG004 kg,dim_materiallisting ml
where     ml.application = ifnull(kg.KOTG004_KAPPL,'Not Set')
	  and ml.matlisting = ifnull(kg.KOTG004_KSCHL,'Not Set')
	  and ml.salesorg = ifnull(kg.KOTG004_VKORG,'Not Set')
	  and ml.distributionchannel = ifnull(kg.KOTG004_VTWEG,'Not Set')
	  and ml.customer = ifnull(kg.KOTG004_KUNNR,'Not Set')
	  and ml.materialnumber = ifnull(kg.KOTG004_MATNR,'Not Set')
	  and ml.validenddate = ifnull(kg.KOTG004_DATBI,'0001-01-01')
      and ml.validstartdate <> ifnull(kg.KOTG004_DATAB, '0001-01-01');
	  
