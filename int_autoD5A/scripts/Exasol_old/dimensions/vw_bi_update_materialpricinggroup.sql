UPDATE dim_MaterialPricingGroup a
   SET a.MaterialPricingGroupName = ifnull(T178T_VTEXT, 'Not Set')
 FROM T178T , dim_MaterialPricingGroup a
 WHERE a.MaterialPricingGroupCode = T178T_KONDM AND RowIsCurrent = 1;
