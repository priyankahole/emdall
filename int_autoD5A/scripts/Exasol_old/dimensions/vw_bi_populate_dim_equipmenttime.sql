/* ##################################################################################################################
  
     Script         : vw_bi_populate_dim_equipmenttime 
     Author         : Simona
     Created On     : 1 April 2014

  
     Change History
     Date            By        Version           Desc
#################################################################################################################### */

SELECT 'START OF PROC vw_bi_populate_dim_equipmenttime',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS tmp_Dim_EquipmentTime;

CREATE TABLE tmp_Dim_EquipmentTime
LIKE 
 Dim_EquipmentTime INCLUDING DEFAULTS INCLUDING IDENTITY;


SELECT 'Insert 1 on tmp_Dim_EquipmentTime',TIMESTAMP(LOCAL_TIMESTAMP);

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'tmp_Dim_EquipmentTime';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_Dim_EquipmentTime',ifnull(max(Dim_EquipmentTimeId),1)
FROM tmp_Dim_EquipmentTime;
select 	'tmp_Dim_EquipmentTime',
		ifnull(max(d.Dim_EquipmentTimeId), 
		ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_Dim_EquipmentTime d
where d.Dim_EquipmentTimeId <> 1;


INSERT INTO tmp_Dim_EquipmentTime( Dim_EquipmentTimeId
  ,EquipmentNumber
  ,ValidT
  ,UsgePerConsecNo
  ,NxtUsagePeriodNo
  ,ValidFrom
  ,Customer
  ,EndCustomer
  ,Operator
  ,RowStartDate
  ,RowEndDate
  ,RowIsCurrent
  ,RowChangeReason )
SELECT 			1,
				'Not Set',
				null,
				null,
				null,
				null,
				'Not Set',
				'Not Set',
				'Not Set',
				current_timestamp,
				null,
				1,
				null
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM Dim_EquipmentTime WHERE Dim_EquipmentTimeId = 1);

INSERT INTO tmp_Dim_EquipmentTime( Dim_EquipmentTimeId
  ,EquipmentNumber
  ,ValidTo
  ,UsgePerConsecNo
  ,NxtUsagePeriodNo
  ,ValidFrom
  ,Customer
  ,EndCustomer
  ,Operator
  ,RowStartDate
  ,RowEndDate
  ,RowIsCurrent
  ,RowChangeReason )
SELECT (( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'tmp_Dim_EquipmentTime') 
+ row_number() over (order by '') ) Dim_EquipmentTimeId,
a.*	
FROM		 
(SELECT DISTINCT    ifnull(EQUZ.EQUZ_EQUNR,'Not Set'),
					EQUZ.EQUZ_DATBI,
					EQUZ.EQUZ_EQLFN,
					EQUZ.EQUZ_EQUZN,
					EQUZ.EQUZ_DATAB,
					ifnull(EQUZ.EQUZ_KUND1,'Not Set'),
					ifnull(EQUZ.EQUZ_KUND2,'Not Set'),
					ifnull(EQUZ.EQUZ_KUND3,'Not Set'),
					current_timestamp,
					null,
					1,
					null
FROM EQUZ
)a;
  

/*CALL vectorwise(combine 'tmp_Dim_EquipmentTime')
CALL vectorwise(combine 'Dim_EquipmentTime - Dim_EquipmentTime')
CALL vectorwise(combine 'Dim_EquipmentTime')
CALL vectorwise(combine 'Dim_EquipmentTime + tmp_Dim_EquipmentTime')*/

DROP TABLE IF EXISTS Dim_EquipmentTime;
RENAME tmp_Dim_EquipmentTime to Dim_EquipmentTime'


SELECT 'END OF PROC  vw_bi_populate_dim_equipmenttime',TIMESTAMP(LOCAL_TIMESTAMP);