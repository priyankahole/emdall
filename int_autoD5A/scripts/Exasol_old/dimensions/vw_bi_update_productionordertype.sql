UPDATE    dim_ProductionOrderType pot

   SET pot.Description = ifnull(T003P_TXT, 'Not Set')
       FROM
          T003P tp, dim_ProductionOrderType pot
 WHERE pot.RowIsCurrent = 1
 AND tp.T003P_AUART = pot.TypeCode;

UPDATE    dim_ProductionOrderType pot

   SET pot.Description = ifnull(T003P_TXT, 'Not Set')
       FROM
          T003P tp, dim_ProductionOrderType pot
 WHERE pot.RowIsCurrent = 1
 AND tp.T003P_AUART = pot.TypeCode
;
