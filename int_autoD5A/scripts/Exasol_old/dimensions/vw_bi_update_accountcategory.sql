UPDATE    dim_accountcategory ac

   SET ac.Description = ifnull(t163i_knttx, 'Not Set')
       from
          t163i t,  dim_accountcategory ac
   WHERE ac.Category = t.t163i_knttp AND ac.RowIsCurrent = 1;
