/* ***********************************************************************************************************************************
   Script         - exa_bi_populate_dim_merckpersondata.sql
   Author         - Octavian Stepan
   Created ON     - Jun 2018
   Description    - Populate dim_merckpersondata as requested by Merck Ah - APP-9861
   ************************************************************************************************************************************/

delete from number_fountain m where m.table_name = 'dim_merckpersondata';

insert into number_fountain				   
select 	'dim_merckpersondata',
	ifnull(max(d.dim_merckpersondataid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_merckpersondata d
where d.dim_merckpersondataid <> 1;

INSERT INTO dim_merckpersondata(
dim_merckpersondataid,
ISID,
PRMRY_FULL_NM,
ACTV_STATUS_IND,
PRMRY_SITE_DESC,
REGION_DESC,
CMPNY_NM,
ORG_UNIT_2_NM,
ORG_UNIT_3_NM,
ORG_UNIT_4_NM,
ORG_UNIT_5_NM,
ORG_UNIT_6_NM,
ORG_UNIT_7_NM,
ORG_UNIT_8_NM)
   SELECT (
select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_merckpersondata')   + row_number() over(order by ''),
t.ISID,
t.PRMRY_FULL_NM,
t.ACTV_STATUS_IND,
t.PRMRY_SITE_DESC,
t.REGION_DESC,
t.CMPNY_NM,
t.ORG_UNIT_2_NM,
t.ORG_UNIT_3_NM,
t.ORG_UNIT_4_NM,
t.ORG_UNIT_5_NM,
t.ORG_UNIT_6_NM,
t.ORG_UNIT_7_NM,
t.ORG_UNIT_8_NM
FROM stg_merckpersondata t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_merckpersondata s
                  WHERE s.PRMRY_FULL_NM = t.PRMRY_FULL_NM
                    AND ifnull(s.isid,'x')= ifnull (t.isid,'x')) ;
