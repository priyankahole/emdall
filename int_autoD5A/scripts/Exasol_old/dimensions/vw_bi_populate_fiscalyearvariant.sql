UPDATE    dim_fiscalyearvariant fp

   SET fp.FiscalYearVariantDescription = t.T009T_LTEXT,
			fp.dw_update_date = current_timestamp
          FROM
          T009T t, dim_fiscalyearvariant fp
   WHERE fp.FiscalYearVariantCode = t.T009T_PERIV AND fp.RowIsCurrent = 1;
   
INSERT INTO dim_fiscalyearvariant(dim_fiscalyearvariantid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_fiscalyearvariant
               WHERE dim_fiscalyearvariantid = 1);

delete from number_fountain m where m.table_name = 'dim_fiscalyearvariant';
   
insert into number_fountain
select 	'dim_fiscalyearvariant',
	ifnull(max(d.Dim_FiscalYearVariantid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_fiscalyearvariant d
where d.Dim_FiscalYearVariantid <> 1; 
			   
INSERT INTO dim_fiscalyearvariant(Dim_FiscalYearVariantid,
                                                                FiscalYearVariantCode,
                                 FiscalYearVariantDescription,
                                 RowStartDate,
                                 RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_fiscalyearvariant')
          + row_number() over(order by '') ,
                 t.T009T_PERIV,
          t.T009T_LTEXT,
          current_timestamp,
          1
     FROM T009T t
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_fiscalyearvariant fp
                       WHERE fp.FiscalYearVariantCode = t.T009T_PERIV AND fp.RowIsCurrent = 1);

