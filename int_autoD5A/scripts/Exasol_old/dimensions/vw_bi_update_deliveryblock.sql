			   
UPDATE dim_deliveryblock a
   SET a.Description = ifnull(t.TVLST_VTEXT, 'Not Set'),
			a.dw_update_date = current_timestamp
  FROM dim_deliveryblock a, TVLST t
 WHERE a.DeliveryBlock = t.TVLST_LIFSP 
 AND a.RowIsCurrent = 1;

