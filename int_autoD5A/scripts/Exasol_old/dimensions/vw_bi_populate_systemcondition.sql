INSERT INTO dim_systemcondition(dim_systemconditionid, RowIsCurrent,systemcondition,systemconditiondesc,
rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_systemcondition
               WHERE dim_systemconditionid = 1);

delete from number_fountain m where m.table_name = 'dim_systemcondition';
   
insert into number_fountain
select 	'dim_systemcondition',
	ifnull(max(d.dim_systemconditionid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_systemcondition d
where d.dim_systemconditionid <> 1; 

INSERT INTO dim_systemcondition(dim_systemconditionid,
                                     systemcondition,
									 systemconditiondesc,
                                     RowStartDate,
                                     RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_systemcondition') 
          + row_number() over(order by ''),
			 T357M_T_ANLZU,
          ifnull(T357M_T_ANLZUX,'Not Set'),
          current_timestamp,
          1
     FROM T357M_T t1
    WHERE  NOT EXISTS
                (SELECT 1
                   FROM dim_systemcondition s
                  WHERE     s.systemcondition = t1.T357M_T_ANLZU
                        AND s.RowIsCurrent = 1)
;


UPDATE  dim_systemcondition s
   SET s.systemconditiondesc = ifnull(T357M_T_ANLZUX,'Not Set'),
			s.dw_update_date = current_timestamp
FROM T357M_T t1, dim_systemcondition s
 WHERE s.RowIsCurrent = 1
 AND  s.systemcondition = t1.T357M_T_ANLZU
 AND s.systemconditiondesc <> ifnull(T357M_T_ANLZUX,'Not Set');