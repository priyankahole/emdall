UPDATE    dim_MRPProcedure mrpp

   SET mrpp.Description = ifnull(DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T t, dim_MRPProcedure mrpp
 WHERE mrpp.RowIsCurrent = 1
       AND t.DD07T_DOMNAME = 'DISVF'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND mrpp.MRPProcedure = t.DD07T_DOMVALUE 
;