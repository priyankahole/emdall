UPDATE    Dim_Currency dc
   SET dc.Currency = ifnull(ifnull(c.KTEXT, c.ltext), 'Not Set')
       FROM
          tcurt c,  Dim_Currency dc
   WHERE c.WAERS = dc.CurrencyCode   
;