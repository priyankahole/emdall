UPDATE    dim_conditionprocedure cp
   SET cp.ProcedureDescription = t.T683U_VTEXT,
       cp.ProcedureApplication = t.T683U_KAPPL,
       cp.UsageOfConditionTable = t.T683U_KVEWE
          FROM
          T683U t,  dim_conditionprocedure cp
 WHERE cp.RowIsCurrent = 1
 AND cp.ProcedureCode = t.T683U_KALSM
 ;