UPDATE    dim_billingblock bb

	   SET Description = ifnull(TVFST_VTEXT, 'Not Set')
       FROM
          TVFST tv,  dim_billingblock bb
       WHERE bb.BillingBlockCode = tv.TVFST_FAKSP AND tv.TVFST_FAKSP IS NOT NULL;