UPDATE    dim_tasklisttype y

   SET y.Description = ifnull(TCA02_TXT, 'Not Set')
       FROM
          TCA02 t, dim_tasklisttype y
 WHERE y.RowIsCurrent = 1
 AND y.TaskListTypeCode = t.TCA02_PLNTY
;