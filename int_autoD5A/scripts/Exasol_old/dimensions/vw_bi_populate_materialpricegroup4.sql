UPDATE 
dim_MaterialPriceGroup4 a
SET a.Description = ifnull(TVM4T_BEZEI, 'Not Set'),
    a.dw_update_date = current_timestamp
FROM 
dim_MaterialPriceGroup4 a,
TVM4T
WHERE a.MaterialPriceGroup4 = TVM4T_MVGR4 AND RowIsCurrent = 1;
 
INSERT INTO dim_MaterialPriceGroup4(dim_MaterialPriceGroup4Id, MaterialPriceGroup4,Description,RowIsCurrent, RowStartDate)
SELECT 1, 'Not Set', 'Not Set', 1, current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup4
               WHERE dim_MaterialPriceGroup4Id = 1);

delete from number_fountain m where m.table_name = 'dim_MaterialPriceGroup4';
   
insert into number_fountain
select 	'dim_MaterialPriceGroup4',
	ifnull(max(d.dim_materialpricegroup4Id), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MaterialPriceGroup4 d
where d.dim_materialpricegroup4Id <> 1; 

INSERT INTO dim_MaterialPriceGroup4(dim_materialpricegroup4Id,
                                    MaterialPriceGroup4,
                                    Description,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MaterialPriceGroup4')
          + row_number() over(order by '') ,
                         t.TVM4T_MVGR4,
          ifnull(TVM4T_BEZEI, 'Not Set'),
          current_timestamp,
          1
     FROM TVM4T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MaterialPriceGroup4 a
               WHERE a.MaterialPriceGroup4 = TVM4T_MVGR4);

