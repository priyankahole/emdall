UPDATE    dim_documentstatus ds
SET ds.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			ds.dw_update_date = current_timestamp
       FROM
          DD07T dt, dim_documentstatus ds   
 WHERE ds.RowIsCurrent = 1
 AND    dt.DD07T_DOMNAME = 'ESTAK'
          AND dt.DD07T_DOMVALUE IS NOT NULL
          AND ds.Status = dt.DD07T_DOMVALUE;

INSERT INTO dim_documentstatus(dim_documentstatusid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_documentstatus
               WHERE dim_documentstatusid = 1);

delete from number_fountain m where m.table_name = 'dim_documentstatus';
   
insert into number_fountain
select 	'dim_documentstatus',
	ifnull(max(d.Dim_DocumentStatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_documentstatus d
where d.Dim_DocumentStatusid <> 1; 

INSERT INTO dim_documentstatus(Dim_DocumentStatusid,
                                                                Description,
                               Status,
                               RowStartDate,
                               RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_documentstatus')
          + row_number() over(order by '') ,
                         ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'ESTAK' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_documentstatus
                    WHERE Status = DD07T_DOMVALUE AND DD07T_DOMNAME = 'ESTAK')
   ORDER BY 2 ;
