UPDATE dim_AvailabilityCheck a 
   SET a.AvailabilityCheckDescription = ifnull(TMVFT_BEZEI, 'Not Set')
   FROM TMVFT, dim_AvailabilityCheck a 
 WHERE a.AvailabilityCheck = TMVFT_MTVFP AND RowIsCurrent = 1;