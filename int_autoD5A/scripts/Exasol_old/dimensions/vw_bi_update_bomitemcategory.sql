UPDATE	dim_bomitemcategory bic
SET 	bic.ItemCatText = ifnull(t.T418T_PTEXT, 'Not Set'),
		bic.dw_update_date = current_timestamp
		FROM	dim_bomitemcategory bic, T418T t
WHERE bic.ItemCategory = t.T418T_POSTP 
AND bic.RowIsCurrent = 1;

