UPDATE    dim_OverallStatusforCreditCheck oscc

   SET oscc.Description = ifnull(DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T t, dim_OverallStatusforCreditCheck oscc
 WHERE oscc.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'CMGST'
          AND oscc.OverallStatusforCreditCheck = ifnull(t.DD07T_DOMVALUE,'Not Set')
;
