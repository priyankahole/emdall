

UPDATE    dim_OrderType o

   SET o.Description = ifnull(t.DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T t, dim_OrderType o
 WHERE o.RowIsCurrent = 1
  AND t.DD07T_DOMNAME = 'PAART'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND o.OrderTypeCode = t.DD07T_DOMVALUE;
