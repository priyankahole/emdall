
UPDATE dim_ActionStatus das 
   SET das.ActionStatus = ifnull(as1.MNEMONIC,'Not Set'),
       das.Description = ifnull(as1.DESCRIPTION,'Not Set'),
       das.DetailDescription = ifnull(as1.DETAIL_DESCRIPTION,'Not Set'),
       das.AlertFlag = as1.ALERT_FLAG,
       das.ExceptionFlag = as1.EXCEPTION_FLAG,
       das.CompleteFlag = as1.COMPLETE_FLAG,
       das.WaitFlag = as1.WAIT_FLAG
from actionstatus as1, dim_ActionStatus das 
 WHERE das.ActionStatusId = as1.ID AND das.RowIsCurrent = 1;
