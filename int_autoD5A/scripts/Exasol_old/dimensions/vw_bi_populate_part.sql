
DROP TABLE IF EXISTS tmp_distinct_mara_marc_mbew;
CREATE TABLE tmp_distinct_mara_marc_mbew
AS
SELECT DISTINCT *
FROM MARA_MARC_MBEW;

DROP TABLE IF EXISTS MARA_MARC_MBEW_orig;
rename MARA_MARC_MBEW to MARA_MARC_MBEW_orig;
RENAME tmp_distinct_mara_marc_mbew to MARA_MARC_MBEW;


insert into dim_part (dim_partid,rowstartdate,rowiscurrent,partnumber_noleadzero)
select 1,current_timestamp,1,'Not Set' from (select 1) as src
where
not exists (select 1 from dim_part where dim_partid=1);

truncate table tmp_mara_marc_makt_spras_ins;
truncate table tmp_mara_marc_makt_spras_del;

insert into tmp_mara_marc_makt_spras_ins
select  a.MARA_MATNR,a.MARC_WERKS
FROM    mara_marc_makt_spras a;


insert into tmp_mara_marc_makt_spras_del
select b.MARA_MATNR,b.MARC_WERKS
from mara_marc_makt b;

merge into tmp_mara_marc_makt_spras_ins dst
using (select distinct t1.rowid rid
	   from tmp_mara_marc_makt_spras_ins t1
				inner join tmp_mara_marc_makt_spras_del t2 on t1.MARA_MATNR = t2.MARA_MATNR and t1.MARC_WERKS = t2.MARC_WERKS) src
on dst.rowid = src.rid
when matched then delete;
		/* VW Original
			call vectorwise (combine 'tmp_mara_marc_makt_spras_ins-tmp_mara_marc_makt_spras_del') */

INSERT INTO mara_marc_makt(
  MARA_MATNR,
MARA_MTART,
MARA_MATKL,
MARA_PRDHA,
MARA_TRAGR,
MARA_SPART,
MARA_MTPOS,
MARA_KZREV,
MARA_MEINS,
MARA_MFRPN,
MARA_MSTAE,
MARA_LAEDA,
MARA_ERSDA,
MARC_WERKS,
MARC_MMSTA,
MARC_MAABC,
MARC_KZKRI,
MARC_EKGRP,
MARC_DISPO,
MARC_PLIFZ,
MARC_BESKZ,
MARC_MINBE,
MARC_EISBE,
MARC_MABST,
MARC_USEQU,
MARC_KAUTB,
MARC_STAWN,
MARC_LGPRO,
MARC_SOBSL,
MARC_INSMK,
MARC_XCHPF,
MARC_LVORM,
MARC_BSTRF,
MARC_DISMM,
MARC_DISLS,
MARC_BSTMI,
MARC_WEBAZ,
MARC_LGFSB,
MARC_HERKL,
MARC_BWSCL,
MARC_STRGR,
MARC_DISPR,
MARC_DISGR,
MARC_SCHGT,
MAKT_MAKTX,
MARC_DZEIT,
MAKT_SPRAS,
MARC_PRCTR,
MARA_EXTWG,
MARC_KZAUS,
MARC_WZEIT,
MARC_BEARZ,
MARA_BISMT,
MARA_MSTAV,
MARA_VOLUM,
MARA_LABOR,
MARC_NCOST,
MARC_SFEPR,
MARC_FXHOR,
MARC_MTVFP,
MARA_WRKST,
MARC_VRMOD,
MARC_VINT1,
MARC_VINT2,
MAKT_MAKTG,
MARA_EAN11,
MARA_GROES,
MARC_RWPRO,
MARA_MFRNR,
MARC_UNETO,
MARC_UEETO,
MARC_UEETK,
MARC_FHORI,
MARA_RAUBE,
MARA_STOFF,
MARC_SHFLG,
MARC_SHZET,
MARC_BSTMA,
MARC_BSTFE,
MARA_TEMPB,
MARA_EKWSL,
MARA_QMPUR,
MARA_BRGEW,
MARA_BSTME,
MARA_ERNAM,
MARA_GEWEI,
MARA_IPRKZ,
MARA_LVORM,
MARA_MHDHB,
MARA_MHDRZ,
MARA_NTGEW,
MARA_PSTAT,
MARA_RDMHD,
MARC_AWSLS,
MARC_FEVOR,
MARC_LADGR,
MARC_LOSGR,
MARC_PSTAT,
MARC_QMATA,
MARC_QMATV,
MARC_SFCPF,
MARC_SSQSS,
MARC_VERKZ,
MARC_XCHAR,
MARA_MSTDE,
MARC_FVIDK,
MARA_SLED_BBD,
MARA_VPSTA,
MARA_WESCH,
MARC_ALTSL,
MARC_AUSME,
MARC_AUSSS,
MARC_EISLO,
MARC_FRTME,
MARC_KZDKZ,
MARC_PERIV,
MARC_PERKZ,
MARC_PRENC,
MARC_PREND,
MARC_PRENO,
MARC_PRFRQ,
MARC_QZGTP,
MARC_RGEKZ,
MARC_SOBSK,
MARC_SBDKZ,
MARC_MAXLZ,
MARC_SERNP,
MARC_MFRGR
) SELECT a.MARA_MATNR,
MARA_MTART,
MARA_MATKL,
MARA_PRDHA,
MARA_TRAGR,
MARA_SPART,
MARA_MTPOS,
MARA_KZREV,
MARA_MEINS,
MARA_MFRPN,
MARA_MSTAE,
MARA_LAEDA,
MARA_ERSDA,
a.MARC_WERKS,
MARC_MMSTA,
MARC_MAABC,
MARC_KZKRI,
MARC_EKGRP,
MARC_DISPO,
MARC_PLIFZ,
MARC_BESKZ,
MARC_MINBE,
MARC_EISBE,
MARC_MABST,
MARC_USEQU,
MARC_KAUTB,
MARC_STAWN,
MARC_LGPRO,
MARC_SOBSL,
MARC_INSMK,
MARC_XCHPF,
MARC_LVORM,
MARC_BSTRF,
MARC_DISMM,
MARC_DISLS,
MARC_BSTMI,
MARC_WEBAZ,
MARC_LGFSB,
MARC_HERKL,
MARC_BWSCL,
MARC_STRGR,
MARC_DISPR,
MARC_DISGR,
MARC_SCHGT,
MAKT_MAKTX,
MARC_DZEIT,
MAKT_SPRAS,
MARC_PRCTR,
MARA_EXTWG,
MARC_KZAUS,
MARC_WZEIT,
MARC_BEARZ,
MARA_BISMT,
MARA_MSTAV,
MARA_VOLUM,
MARA_LABOR,
MARC_NCOST,
MARC_SFEPR,
MARC_FXHOR,
MARC_MTVFP,
MARA_WRKST,
MARC_VRMOD,
MARC_VINT1,
MARC_VINT2,
MAKT_MAKTG,
MARA_EAN11,
MARA_GROES,
MARC_RWPRO,
MARA_MFRNR,
MARC_UNETO,
MARC_UEETO,
MARC_UEETK,
MARC_FHORI,
MARA_RAUBE,
MARA_STOFF,
MARC_SHFLG,
MARC_SHZET,
MARC_BSTMA,
MARC_BSTFE,
MARA_TEMPB,
MARA_EKWSL,
MARA_QMPUR,
MARA_BRGEW,
MARA_BSTME,
MARA_ERNAM,
MARA_GEWEI,
MARA_IPRKZ,
MARA_LVORM,
MARA_MHDHB,
MARA_MHDRZ,
MARA_NTGEW,
MARA_PSTAT,
MARA_RDMHD,
MARC_AWSLS,
MARC_FEVOR,
MARC_LADGR,
MARC_LOSGR,
MARC_PSTAT,
MARC_QMATA,
MARC_QMATV,
MARC_SFCPF,
MARC_SSQSS,
MARC_VERKZ,
MARC_XCHAR,
MARA_MSTDE,
MARC_FVIDK,
MARA_SLED_BBD,
MARA_VPSTA,
MARA_WESCH,
MARC_ALTSL,
MARC_AUSME,
MARC_AUSSS,
MARC_EISLO,
MARC_FRTME,
MARC_KZDKZ,
MARC_PERIV,
MARC_PERKZ,
MARC_PRENC,
MARC_PREND,
MARC_PRENO,
MARC_PRFRQ,
MARC_QZGTP,
MARC_RGEKZ,
MARC_SOBSK,
MARC_SBDKZ,
MARC_MAXLZ,
MARC_SERNP,
MARC_MFRGR
FROM mara_marc_makt_spras a,  tmp_mara_marc_makt_spras_ins b
WHERE a.MARA_MATNR = b.MARA_MATNR and a.MARC_WERKS = b.MARC_WERKS;

truncate table tmp_mara_marc_makt_spras_ins;
truncate table tmp_mara_marc_makt_spras_del;

/*

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
       Revision = ifnull(MARA_KZREV, 'Not Set'),
       UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),
       CommodityCode = ifnull(MARC_STAWN, 'Not Set'),
       PartType = ifnull(MARA_MTART, 'Not Set'),
       LeadTime = ifnull(MARC_PLIFZ, 0),
       StockLocation = ifnull(marc_lgpro, 'Not Set'),
       PurchaseGroupCode = ifnull(marc_ekgrp, 'Not Set'),
       PurchaseGroupDescription =
          ifnull((SELECT t.T024_EKNAM
                    FROM t024 t
                   WHERE t.T024_EKGRP = MARC_EKGRP),
                 'Not Set'),
       MRPController = ifnull(marc_dispo, 'Not Set'),
       MaterialGroup = ifnull(mara_matkl, 'Not Set'),
     --  Plant = ifnull(mck.marc_werks, 'Not Set'),
       ABCIndicator = ifnull(marc_maabc, 'Not Set'),
       ProcurementType = ifnull(marc_beskz, 'Not Set'),
       StorageLocation = ifnull(MARC_LGFSB, 'Not Set'),
       CriticalPart = ifnull(MARC_KZKRI, 'Not Set'),
       MRPType = ifnull(MARC_DISMM, 'Not Set'),
       SupplySource = ifnull(MARC_BWSCL, 'Not Set'),
       StrategyGroup = ifnull(MARC_STRGR, 'Not Set'),
       TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),
       Division = ifnull(MARA_SPART, 'Not Set'),
       DivisionDescription =
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),
                 'Not Set'),
       GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
       DeletionFlag = ifnull(MARC_LVORM, 'Not Set'),
       MaterialStatus = ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
       ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
       MPN = ifnull(MARA_MFRPN, 'Not Set'),
       BulkMaterial = ifnull(MARC_SCHGT, 'Not Set'),
       ProductHierarchyDescription =
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set'),
       MRPProfile = ifnull(mck.MARC_DISPR, 'Not Set'),
       MRPProfileDescription =
          ifnull((SELECT p.T401T_KTEXT
                    FROM t401t p
                   WHERE p.T401T_DISPR = mck.MARC_DISPR),
                 'Not Set'),
       PartTypeDescription =
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set'),
       MaterialGroupDescription =
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set'),
       MRPTypeDescription =
          ifnull((SELECT mt.T438T_DIBEZ
                    FROM T438T mt
                   WHERE mt.T438T_DISMM = mck.MARC_DISMM),
                 'Not Set'),
       ProcurementTypeDescription =
          ifnull(
             (SELECT dd.DD07T_DDTEXT
                FROM DD07T dd
               WHERE dd.DD07T_DOMNAME = 'BESKZ'
                     AND dd.DD07T_DOMVALUE = MARC_BESKZ),
             'Not Set'),
       MRPControllerDescription =
          ifnull(
             (SELECT mc.T024D_DSNAM
                FROM T024D mc
               WHERE mc.T024D_DISPO = mck.MARC_DISPO
                     AND mc.T024D_WERKS = mck.MARC_WERKS),
             'Not Set'),
       MRPGroup = ifnull(MARC_DISGR, 'Not Set'),
       MRPGroupDescription =
          ifnull(
             (SELECT mpg.T438X_TEXT40
                FROM T438X mpg
               WHERE mpg.T438X_DISGR = mck.MARC_DISGR
                     AND mpg.T438X_WERKS = mck.MARC_WERKS),
             'Not Set'),
       MRPLotSize = ifnull(mck.MARC_DISLS, 'Not Set'),
       MRPLotSizeDescription =
          ifnull((SELECT ml.T439T_LOSLT
                    FROM T439T ml
                   WHERE ml.T439T_DISLS = mck.MARC_DISLS),
                 'Not Set'),
       materialstatusdescription =
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set'),
       dp.SpecialProcurement = ifnull(mck.MARC_SOBSL, 'Not Set'),
       dp.SpecialProcurementDescription =
          ifnull(
             (SELECT sp.T460T_LTEXT
                FROM t460t sp
               WHERE sp.T460T_SOBSL = mck.marc_sobsl
                     AND sp.T460T_WERKS = mck.MARC_WERKS),
             'Not Set'),
       dp.InhouseProductionTime = mck.MARC_DZEIT,
       dp.ProfitCenterCode = ifnull(mck.MARC_PRCTR, 'Not Set'),
       dp.ProfitCenterName =
          ifnull(
             (SELECT c.CEPCT_KTEXT
                FROM cepct c
               WHERE c.CEPCT_PRCTR = mck.MARC_PRCTR
                     AND c.CEPCT_DATBI > current_date),
             'Not Set'),
       dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
       dp.ExternalMaterialGroupDescription =
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set'),
       dp.MaterialDiscontinuationFlag = ifnull(MARC_KZAUS, 'Not Set'),
       dp.MaterialDiscontinuationFlagDescription =
          ifnull(
             (SELECT DD07T_DDTEXT
                FROM DD07T
               WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS),
             'Not Set'),
       dp.ProcessingTime = MARC_BEARZ,
       dp.TotalReplenishmentLeadTime = MARC_WZEIT,
       dp.GRProcessingTime = MARC_WEBAZ,
       dp.OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
       dp.AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
       dp.AFSColorDescription =
          ifnull((SELECT J_3ACOLRT_TEXT
                    FROM J_3ACOLRT
                   WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL),
                 'Not Set'),
       dp.SDMaterialStatusDescription =
          ifnull((SELECT TVMST_VMSTB
                    FROM tvmst
                   WHERE TVMST_VMSTA = MARA_MSTAV),
                 'Not Set'),
       dp.Volume = MARA_VOLUM,
       dp.AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),
       dp.AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
       dp.DoNotCost = ifnull(MARC_NCOST, 'Not Set'),
       dp.PlantMaterialStatus = ifnull(mck.MARC_MMSTA, 'Not Set'),
       dp.PlantMaterialStatusDescription =
          ifnull((SELECT t141t_MTSTB
                    FROM t141t mst
                   WHERE mst.T141T_MMSTA = mck.MARC_MMSTA),
                 'Not Set'),
       dp.REMProfile = ifnull(MARC_SFEPR, 'Not Set'),
       dp.PlanningTimeFence = ifnull(MARC_FXHOR, 0),
       dp.SafetyStock = ifnull(MARC_EISBE, 0.0000),
       dp.RoundingValue = ifnull(MARC_BSTRF, 0.0000),
       dp.ReorderPoint = ifnull(MARC_MINBE, 0.0000),
       dp.MinimumLotSize = ifnull(MARC_BSTMI,0.0000),
       dp.ValidFrom = (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and  j1.J_3AMAD_WERKS = mck.MARC_WERKS),
       dp.MRPStatus = ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and  j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
       dp.CheckingGroupCode = ifnull(mck.MARC_MTVFP, 'Not Set'),
       dp.CheckingGroup = ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
       dp.UPCNumber=ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set'),
       dp.MaximumStockLevel = ifnull(mck.MARC_MABST, 0.0000),
       dp.ConsumptionMode = ifnull(mck.MARC_VRMOD, 'Not Set'),
       dp.ConsumptionModeDescription = ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
       dp.ConsumptionPeriodBackward = ifnull(mck.MARC_VINT1, 0),
       dp.ConsumptionPeriodForward = ifnull(mck.MARC_VINT2, 0),
       dp.PartLongDesc = ifnull(mck.MAKT_MAKTG,'Not Set'),
       dp.ManfacturerNumber = ifnull(mck.MARA_MFRNR,'Not Set'),
       dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
       dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
	   dp.SchedMarginKey = ifnull(mck.MARC_FHORI,'Not Set'),
       dp.MRPControllerTelephone = ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set'),
			dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   
	   */

UPDATE dim_part dp
   SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
	   	dp.dw_update_date = current_timestamp
from mara_marc_makt_spras mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and PartDescription <> ifnull(MAKT_MAKTX, 'Not Set');

/* Madalina 14 Jul 2016 - Fields from MARA shouldn't be updated from MARA_MARC_MAKT- use MARA for updates, without WERKS as join condition. The updates are done after all the inserts in dim_part - BI-3461 */ 	  
/*UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET Revision = ifnull(MARA_KZREV, 'Not Set'),	
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and Revision <> ifnull(MARA_KZREV, 'Not Set') 
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),	
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and UnitOfMeasure <> ifnull(MARA_MEINS, 'Not Set') 

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p       
   SET PartType = ifnull(MARA_MTART, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and PartType <> ifnull(MARA_MTART, 'Not Set') 

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET   MaterialGroup = ifnull(mara_matkl, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	
	   and MaterialGroup <> ifnull(mara_matkl, 'Not Set')	   
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),	   
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and TransportationGroup <> ifnull(MARA_TRAGR, 'Not Set') 
	
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  Division = ifnull(MARA_SPART, 'Not Set'),
     dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and Division <> ifnull(MARA_SPART, 'Not Set')
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  DivisionDescription =
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),
                 'Not Set'),
				   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and DivisionDescription <>
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),
                 'Not Set')
   				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
   			   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  GeneralItemCategory <> ifnull(MARA_MTPOS, 'Not Set')	   

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ProductHierarchy <> ifnull(MARA_PRDHA, 'Not Set') 
	   
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET MPN = ifnull(MARA_MFRPN, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MPN <> ifnull(MARA_MFRPN, 'Not Set')

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET ProductHierarchyDescription =
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ProductHierarchyDescription <>
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set') 

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET PartTypeDescription =
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and PartTypeDescription <>
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set')
				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET  MaterialGroupDescription =
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set'),
dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MaterialGroupDescription <>
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set')

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET materialstatusdescription =
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARA_MSTAE, 'Not Set')),
             'Not Set'),
	  dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = ifnull(mck.MARA_MATNR, 'Not Set')
       AND dp.Plant = ifnull(mck.MARC_WERKS, 'Not Set')
       AND p.plantcode = ifnull(marc_werks, 'Not Set')
	   and materialstatusdescription <>
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARA_MSTAE, 'Not Set')),
             'Not Set') 	

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
   dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ExternalMaterialGroupCode <> ifnull(MARA_EXTWG, 'Not Set') 
	   				 
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ExternalMaterialGroupDescription =
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set'),
  dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ExternalMaterialGroupDescription <>
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set')	

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.OldPartNumber <> ifnull(MARA_BISMT, 'Not Set')

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.SDMaterialStatusDescription =
          ifnull((SELECT TVMST_VMSTB
                    FROM tvmst
                   WHERE TVMST_VMSTA = MARA_MSTAV),
                 'Not Set'),
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SDMaterialStatusDescription <>
          ifnull((SELECT TVMST_VMSTB
                    FROM tvmst
                   WHERE TVMST_VMSTA = MARA_MSTAV),
                 'Not Set')

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.Volume = MARA_VOLUM,	
	dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
and dp.Volume <> MARA_VOLUM

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.ManfacturerNumber = ifnull(mck.MARA_MFRNR,'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks  
	   and dp.ManfacturerNumber <> ifnull(mck.MARA_MFRNR,'Not Set') 
	      
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
      dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	   and dp.UPCCode <> ifnull(mck.MARA_EAN11,'Not Set')

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
    dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks 
	  and dp.NDCCode <> ifnull(mck.MARA_GROES,'Not Set')

UPDATE dim_part dp from mara_marc_makt mck, dim_plant p
   SET dp.storageconditioncode = ifnull(mck.MARA_RAUBE,'Not Set'),
	   dp.hazardousmaterialnumber = ifnull(mck.MARA_STOFF,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks	   
	   */
	   
UPDATE dim_part dp       
   SET CommodityCode = ifnull(MARC_STAWN, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
 from mara_marc_makt mck, dim_plant p, dim_part dp 
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and CommodityCode <> ifnull(MARC_STAWN, 'Not Set');
	   
UPDATE dim_part dp        
   SET LeadTime = ifnull(MARC_PLIFZ, 0),
	   	   	dp.dw_update_date = current_timestamp
 from mara_marc_makt mck, dim_plant p, dim_part dp  
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and LeadTime <> ifnull(MARC_PLIFZ, 0);

UPDATE dim_part dp
   SET  StockLocation = ifnull(marc_lgpro, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p    , dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and StockLocation <> ifnull(marc_lgpro, 'Not Set');	   

UPDATE dim_part dp
   SET  PurchaseGroupCode = ifnull(marc_ekgrp, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p      , dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and PurchaseGroupCode <> ifnull(marc_ekgrp, 'Not Set');

UPDATE dim_part dp
   SET  PurchaseGroupDescription = ifnull(t.T024_EKNAM,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join t024 t on t.T024_EKGRP = mck.MARC_EKGRP
WHERE PurchaseGroupDescription <> ifnull(t.T024_EKNAM,'Not Set');

UPDATE dim_part dp
   SET  MRPController = ifnull(marc_dispo, 'Not Set'),
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPController <> ifnull(marc_dispo, 'Not Set');

/*UPDATE dim_part dp
   SET   MaterialGroup = ifnull(mara_matkl, 'Not Set'),
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MaterialGroup <> ifnull(mara_matkl, 'Not Set')*/

UPDATE dim_part dp
   SET  ABCIndicator = ifnull(marc_maabc, 'Not Set'),
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ABCIndicator <> ifnull(marc_maabc, 'Not Set');

UPDATE dim_part dp
   SET  ProcurementType = ifnull(marc_beskz, 'Not Set'),
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and ProcurementType <> ifnull(marc_beskz, 'Not Set');

UPDATE dim_part dp
   SET  StorageLocation = ifnull(MARC_LGFSB, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and StorageLocation <> ifnull(MARC_LGFSB, 'Not Set');

UPDATE dim_part dp
   SET CriticalPart = ifnull(MARC_KZKRI, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and CriticalPart <> ifnull(MARC_KZKRI, 'Not Set');

UPDATE dim_part dp
   SET MRPType = ifnull(MARC_DISMM, 'Not Set'),
       dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPType <> ifnull(MARC_DISMM, 'Not Set');

UPDATE dim_part dp
   SET  SupplySource = ifnull(MARC_BWSCL, 'Not Set'),
dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  SupplySource <> ifnull(MARC_BWSCL, 'Not Set');

UPDATE dim_part dp
   SET   StrategyGroup = ifnull(MARC_STRGR, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  StrategyGroup <> ifnull(MARC_STRGR, 'Not Set');
	   
UPDATE dim_part dp   
   SET DeletionFlag = ifnull(MARC_LVORM, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp 
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and DeletionFlag <> ifnull(MARC_LVORM, 'Not Set');
	   
UPDATE dim_part dp
   SET  MaterialStatus = ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MaterialStatus <> ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set');
	   
UPDATE dim_part dp  
   SET BulkMaterial = ifnull(MARC_SCHGT, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp  
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and BulkMaterial <> ifnull(MARC_SCHGT, 'Not Set');
				
UPDATE dim_part dp   
   SET  MRPProfile = ifnull(mck.MARC_DISPR, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp  
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPProfile <> ifnull(mck.MARC_DISPR, 'Not Set');
	   
UPDATE dim_part dp
   SET  MRPProfileDescription = ifnull(t.T401T_KTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join t401t t on t.T401T_DISPR = mck.MARC_DISPR
WHERE MRPProfileDescription <> ifnull(t.T401T_KTEXT,'Not Set');

				 	 
UPDATE dim_part dp
   SET  MRPTypeDescription = ifnull(t.T438T_DIBEZ,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join T438T t on t.T438T_DISMM = mck.MARC_DISMM
WHERE MRPTypeDescription <> ifnull(t.T438T_DIBEZ,'Not Set');

UPDATE dim_part dp
 SET ProcurementTypeDescription = ifnull(dd.DD07T_DDTEXT,'Not Set'),
 dp.dw_update_date = current_timestamp
 from dim_plant p,dim_part dp, mara_marc_makt mck
 Left Join DD07T dd on dd.DD07T_DOMVALUE = mck.MARC_BESKZ
 WHERE dp.PartNumber = mck.MARA_MATNR
 AND dp.Plant = mck.MARC_WERKS
 AND p.plantcode = marc_werks
 AND dd.DD07T_DOMNAME = 'BESKZ'
 and ProcurementTypeDescription <> ifnull(dd.DD07T_DDTEXT,'Not Set');

UPDATE dim_part dp
   SET  MRPControllerDescription = ifnull(t.T024D_DSNAM,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join T024D t on t.T024D_DISPO = mck.MARC_DISPO AND t.T024D_WERKS = mck.MARC_WERKS
WHERE MRPControllerDescription <> ifnull(t.T024D_DSNAM,'Not Set');


UPDATE dim_part dp
   SET MRPGroup = ifnull(MARC_DISGR, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPGroup <> ifnull(MARC_DISGR, 'Not Set');

UPDATE dim_part dp
   SET  MRPGroupDescription = ifnull(t.T438X_TEXT40,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join T438X t on t.T438X_DISGR = mck.MARC_DISGR AND t.T438X_WERKS = mck.MARC_WERKS
WHERE MRPGroupDescription <> ifnull(t.T438X_TEXT40,'Not Set');

UPDATE dim_part dp
   SET MRPLotSize = ifnull(mck.MARC_DISLS, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and MRPLotSize <> ifnull(mck.MARC_DISLS, 'Not Set');
	   
UPDATE dim_part dp
   SET  MRPLotSizeDescription = ifnull(t.T439T_LOSLT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join T439T t on t.T439T_DISLS = mck.MARC_DISLS
WHERE MRPLotSizeDescription <> ifnull(t.T439T_LOSLT,'Not Set');
/* Madalina Herghelegiu 02 June 2016 - materialstatusdescription takes values from t141t_MTSTB, based on the link MARA-MSTAE - T141-MMSTA only. BI-2885	*/ 
/*
UPDATE dim_part dp from mara_marc_makt mck, dim_plant p  
   SET materialstatusdescription =
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set'),
	  dp.dw_update_date = current_timestamp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and materialstatusdescription <>
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set')
 /* END BI-2885	*/ 
 
UPDATE dim_part dp
   SET dp.SpecialProcurement = ifnull(mck.MARC_SOBSL, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SpecialProcurement <> ifnull(mck.MARC_SOBSL, 'Not Set');

UPDATE dim_part dp
   SET  SpecialProcurementDescription = ifnull(t.T460T_LTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join t460t t on t.T460T_SOBSL = mck.marc_sobsl AND t.T460T_WERKS = mck.MARC_WERKS
WHERE SpecialProcurementDescription <> ifnull(t.T460T_LTEXT,'Not Set');

UPDATE dim_part dp
   SET dp.InhouseProductionTime = mck.MARC_DZEIT,
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.InhouseProductionTime <> mck.MARC_DZEIT;

UPDATE dim_part dp
   SET dp.ProfitCenterCode = ifnull(mck.MARC_PRCTR, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ProfitCenterCode <> ifnull(mck.MARC_PRCTR, 'Not Set');

UPDATE dim_part dp
   SET  ProfitCenterName = ifnull(t.CEPCT_KTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join cepct t on t.CEPCT_PRCTR = mck.MARC_PRCTR AND t.CEPCT_DATBI > current_date
WHERE ProfitCenterName <> ifnull(t.CEPCT_KTEXT,'Not Set');

UPDATE dim_part dp
   SET dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ExternalMaterialGroupCode <> ifnull(MARA_EXTWG, 'Not Set');

UPDATE dim_part dp
   SET  ExternalMaterialGroupDescription = ifnull(t.TWEWT_EWBEZ,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join MARA mck on dp.PartNumber = mck.MARA_MATNR 
		left join twewt t on t.TWEWT_EXTWG = mck.MARA_EXTWG
WHERE ExternalMaterialGroupDescription <> ifnull(t.TWEWT_EWBEZ,'Not Set');

UPDATE dim_part dp
   SET dp.MaterialDiscontinuationFlag = ifnull(MARC_KZAUS, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MaterialDiscontinuationFlag <> ifnull(MARC_KZAUS, 'Not Set');

UPDATE dim_part dp
   SET  MaterialDiscontinuationFlagDescription = ifnull(t.DD07T_DDTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join DD07T t on t.DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = mck.MARC_KZAUS
WHERE MaterialDiscontinuationFlagDescription <> ifnull(t.DD07T_DDTEXT,'Not Set');

UPDATE dim_part dp
   SET dp.ProcessingTime = MARC_BEARZ,
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks;

UPDATE dim_part dp
   SET dp.TotalReplenishmentLeadTime = MARC_WZEIT,
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.TotalReplenishmentLeadTime <> MARC_WZEIT;

UPDATE dim_part dp
   SET dp.GRProcessingTime = MARC_WEBAZ,
       dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.GRProcessingTime <> MARC_WEBAZ;



UPDATE dim_part dp
   SET dp.AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.AFSColor <> ifnull(MARA_J_3ACOL, 'Not Set');

UPDATE dim_part dp
   SET  AFSColorDescription = ifnull(t.J_3ACOLRT_TEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join J_3ACOLRT t on t.J_3ACOLRT_J_3ACOL = mck.MARA_J_3ACOL
WHERE AFSColorDescription <> ifnull(t.J_3ACOLRT_TEXT,'Not Set');


UPDATE dim_part dp
   SET dp.AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),
	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.AFSMasterGrid <> ifnull(MARA_J_3APGNR, 'Not Set');

UPDATE dim_part dp
   SET dp.AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.AFSPattern <> ifnull(MARA_AFS_SCHNITT, 'Not Set');

UPDATE dim_part dp
   SET  dp.DoNotCost = ifnull(MARC_NCOST, 'Not Set'),
    	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.DoNotCost <> ifnull(MARC_NCOST, 'Not Set');

UPDATE dim_part dp
   SET dp.PlantMaterialStatus = ifnull(mck.MARC_MMSTA, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.PlantMaterialStatus <> ifnull(mck.MARC_MMSTA, 'Not Set');

UPDATE dim_part dp
   SET  PlantMaterialStatusDescription = ifnull(t.t141t_MTSTB,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join t141t t on t.T141T_MMSTA = mck.MARC_MMSTA
WHERE PlantMaterialStatusDescription <> ifnull(t.t141t_MTSTB,'Not Set');

UPDATE dim_part dp
   SET dp.REMProfile = ifnull(MARC_SFEPR, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.REMProfile <> ifnull(MARC_SFEPR, 'Not Set');

UPDATE dim_part dp
   SET dp.PlanningTimeFence = ifnull(MARC_FXHOR, 0),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.PlanningTimeFence <> ifnull(MARC_FXHOR, 0);

UPDATE dim_part dp
   SET dp.SafetyStock = ifnull(MARC_EISBE, 0.0000),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SafetyStock <> ifnull(MARC_EISBE, 0.0000);


UPDATE dim_part dp
   SET dp.RoundingValue = ifnull(MARC_BSTRF, 0.0000),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and  dp.RoundingValue <> ifnull(MARC_BSTRF, 0.0000);

UPDATE dim_part dp
   SET dp.ReorderPoint = ifnull(MARC_MINBE, 0.0000),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ReorderPoint <> ifnull(MARC_MINBE, 0.0000);

UPDATE dim_part dp
   SET dp.MinimumLotSize = ifnull(MARC_BSTMI,0.0000),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MinimumLotSize <> ifnull(MARC_BSTMI,0.0000);

UPDATE dim_part dp
   SET  ValidFrom = ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join J_3AMAD j1 on mck.mara_matnr = j1.j_3amad_matnr and j1.J_3AMAD_WERKS = mck.MARC_WERKS
WHERE ValidFrom <> ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01');

UPDATE dim_part dp
SET dp.MRPStatus = 'Not Set',
dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
AND dp.Plant = mck.MARC_WERKS
AND p.plantcode = marc_werks
and dp.MRPStatus <> 'Not Set';


UPDATE dim_part dp
SET dp.MRPStatus = IFNULL(j2.J_3AMAD_J_4ASTAT,'Not Set'),
dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p,J_3AMAD j2, dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
AND dp.Plant = mck.MARC_WERKS
AND p.plantcode = marc_werks
AND mck.mara_matnr = j2.j_3amad_matnr and j2.J_3AMAD_WERKS = mck.MARC_WERKS
and dp.MRPStatus <> IFNULL(j2.J_3AMAD_J_4ASTAT,'Not Set');

UPDATE dim_part dp
   SET dp.CheckingGroupCode = ifnull(mck.MARC_MTVFP, 'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
   and dp.CheckingGroupCode <> ifnull(mck.MARC_MTVFP, 'Not Set');

UPDATE dim_part dp
   SET  CheckingGroup = ifnull(t.TMVFT_BEZEI,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join TMVFT t on mck.marc_mtvfp = t.tmvft_mtvfp
WHERE CheckingGroup <> ifnull(t.TMVFT_BEZEI,'Not Set');

/* Ambiguos repalce solved */
merge into dim_part dim
using(select dp.dim_partid,
			 ifnull(max(t.MEAN_EAN11),'Not Set') as UPCNumber,
			 ifnull(max(t1.DD07T_DDTEXT),'Not Set') as ProcurementTypeDescription
	  from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join MEAN t on t.MEAN_MATNR = mck.mara_matnr
		left join DD07T t1 on t1.DD07T_DOMVALUE = mck.MARC_BESKZ
	  group by dp.dim_partid
	  ) src on dim.dim_Partid = src.dim_partid
when matched then update set dim.UPCNumber = src.UPCNumber,
							 dim.ProcurementTypeDescription = src.ProcurementTypeDescription
where (dim.UPCNumber <> src.UPCNumber or dim.ProcurementTypeDescription = src.ProcurementTypeDescription);

UPDATE dim_part dp
   SET dp.MaximumStockLevel = ifnull(mck.MARC_MABST, 0.0000),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.MaximumStockLevel <> ifnull(mck.MARC_MABST, 0.0000);

UPDATE dim_part dp
   SET dp.ConsumptionMode = ifnull(mck.MARC_VRMOD, 'Not Set'),
     dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ConsumptionMode <> ifnull(mck.MARC_VRMOD, 'Not Set');
	   
/* update transformed into a merge due to UPDATE-target-table error caused by 2 left joins
UPDATE dim_part dp
   SET  ConsumptionModeDescription = ifnull(t.DD07T_DDTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		left join dim_plant p on p.plantcode = mck.marc_werks AND p.languagekey = makt_spras
		left join DD07T t on t.DD07T_DOMNAME = 'VRMOD' AND t.DD07T_DOMVALUE = mck.MARC_VRMOD
WHERE ConsumptionModeDescription <> ifnull(t.DD07T_DDTEXT,'Not Set')
*/

merge into dim_part dp
using(
select ifnull(t.DD07T_DDTEXT,'Not Set') as DD07T_DDTEXT,dp.dim_partid
from dim_part dp
inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS		
left join dim_plant p on p.plantcode = mck.marc_werks AND p.languagekey = makt_spras	
left join DD07T t on t.DD07T_DOMNAME = 'VRMOD' AND t.DD07T_DOMVALUE = mck.MARC_VRMOD
) tst
on tst.dim_partid = dp.dim_partid
when matched then update 
set dp.ConsumptionModeDescription = tst.DD07T_DDTEXT,
dp.dw_update_date = current_timestamp
where dp.ConsumptionModeDescription <> tst.DD07T_DDTEXT;

UPDATE dim_part dp
   SET dp.ConsumptionPeriodBackward = ifnull(mck.MARC_VINT1, 0),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ConsumptionPeriodBackward <> ifnull(mck.MARC_VINT1, 0);

UPDATE dim_part dp
   SET dp.ConsumptionPeriodForward = ifnull(mck.MARC_VINT2, 0),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
and dp.ConsumptionPeriodForward <> ifnull(mck.MARC_VINT2, 0);

UPDATE dim_part dp
   SET dp.PartLongDesc = ifnull(mck.MAKT_MAKTG,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.PartLongDesc <> ifnull(mck.MAKT_MAKTG,'Not Set');

UPDATE dim_part dp
   SET dp.ManfacturerNumber = ifnull(mck.MARA_MFRNR,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.ManfacturerNumber <> ifnull(mck.MARA_MFRNR,'Not Set');


UPDATE dim_part dp
   SET dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
      dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.UPCCode <> ifnull(mck.MARA_EAN11,'Not Set');

UPDATE dim_part dp
   SET dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	  and dp.NDCCode <> ifnull(mck.MARA_GROES,'Not Set');

UPDATE dim_part dp
   SET dp.SchedMarginKey = ifnull(mck.MARC_FHORI,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.SchedMarginKey <> ifnull(mck.MARC_FHORI,'Not Set');

UPDATE dim_part dp
   SET  MRPControllerTelephone = ifnull(t.T024D_DSTEL,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara_marc_makt mck on dp.PartNumber = mck.MARA_MATNR AND dp.Plant = mck.MARC_WERKS
		inner join dim_plant p on p.plantcode = mck.marc_werks
		left join T024D t on t.T024D_DISPO = mck.MARC_DISPO AND t.T024D_WERKS = mck.MARC_WERKS
WHERE MRPControllerTelephone <> ifnull(t.T024D_DSTEL,'Not Set');

 UPDATE dim_part dp
   SET dp.storageconditioncode = ifnull(mck.MARA_RAUBE,'Not Set'),
	   dp.hazardousmaterialnumber = ifnull(mck.MARA_STOFF,'Not Set'),
	   	dp.dw_update_date = current_timestamp
 from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
       AND (dp.storageconditioncode <> ifnull(mck.MARA_RAUBE,'Not Set') OR
			dp.hazardousmaterialnumber <> ifnull(mck.MARA_STOFF,'Not Set')
			);


/*17 feb 2015 new MARC columns */

UPDATE dim_part dp
SET dp.IndustryStandardDescription = ifnull(MARA_NORMT, 'Not Set'),
   dp.dw_update_date = current_timestamp
FROM mara_marc_makt mck, dim_plant p  , dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
AND dp.Plant = mck.MARC_WERKS
AND p.plantcode = marc_werks
AND dp.IndustryStandardDescription <> ifnull(MARA_NORMT, 'Not Set');
/* END Yogini APP-8849 29 May 2018 */

/*17 feb 2015 new MARC columns */

UPDATE dim_part dp
   SET dp.rangeofcoverage=ifnull(mck.MARC_RWPRO,'Not Set')
from mara_marc_makt mck, dim_plant p , dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and dp.rangeofcoverage<>ifnull(mck.MARC_RWPRO,'Not Set');

 UPDATE dim_part dp
   SET dp.safetytimeind = ifnull(mck.MARC_SHFLG,'Not Set')
   , dp.dw_update_date = current_timestamp
 from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.safetytimeind <> ifnull(mck.MARC_SHFLG,'Not Set');

  UPDATE dim_part dp
   SET dp.safetytime = ifnull(mck.MARC_SHZET,0)
   , dp.dw_update_date = current_timestamp
  from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.safetytime <> ifnull(mck.MARC_SHZET,0);

  UPDATE dim_part dp
   SET dp.maxlotsize  = ifnull(mck.MARC_BSTMA,0)
   , dp.dw_update_date = current_timestamp
  from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.maxlotsize  <> ifnull(mck.MARC_BSTMA,0);

  UPDATE dim_part dp
   SET dp.fixedlotsize = ifnull(mck.MARC_BSTFE,0)
   , dp.dw_update_date = current_timestamp
  from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR    AND dp.Plant = mck.MARC_WERKS   AND p.plantcode = marc_werks
 and dp.fixedlotsize <> ifnull(mck.MARC_BSTFE,0);

 /* end 17 feb 2015 new MARC columns */

truncate table tmp_dim_MARA_MARC_MAKT_ins;
truncate table tmp_dim_MARA_MARC_MAKT_del;

insert into tmp_dim_MARA_MARC_MAKT_ins
select mck.MARA_MATNR,mck.MARC_WERKS
FROM    MARA_MARC_MAKT mck;

insert into tmp_dim_MARA_MARC_MAKT_del
select p.PartNumber,p.Plant
from dim_part p;

merge into tmp_dim_MARA_MARC_MAKT_ins dst
using (select distinct t1.rowid rid
	   from tmp_dim_MARA_MARC_MAKT_ins t1
				inner join tmp_dim_MARA_MARC_MAKT_del t2 on t1.MARA_MATNR = t2.MARA_MATNR and t1.MARC_WERKS = t2.MARC_WERKS ) src
on dst.rowid = src.rid
when matched then delete;
	/* VW Original: call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_ins-tmp_dim_MARA_MARC_MAKT_del') */
   
drop table if exists tmp_dim_part_f_ins1;
create table tmp_dim_part_f_ins1 as
select    distinct
		  mck.MARA_MATNR PartNumber,
          ifnull(MAKT_MAKTX, 'Not Set') PartDescription,
          ifnull(MARA_KZREV, 'Not Set') Revision,
          ifnull(MARA_MEINS, 'Not Set') UnitOfMeasure,
          current_timestamp RowStartDate,
          1 RowIsCurrent,
          ifnull(MARC_STAWN, 'Not Set') CommodityCode,
          ifnull(MARA_MTART, 'Not Set') PartType,
          ifnull(MARC_PLIFZ, 0) LeadTime,
          ifnull(MARC_LGPRO, 'Not Set') StockLocation,
          ifnull(MARC_EKGRP, 'Not Set') PurchaseGroupCode,
          convert(varchar(200), 'Not Set') as PurchaseGroupDescription, -- ifnull((SELECT t.T024_EKNAM FROM t024 t WHERE t.T024_EKGRP = MARC_EKGRP), 'Not Set') PurchaseGroupDescription,
          ifnull(MARC_DISPO, 'Not Set') MRPController,
          ifnull(MARA_MATKL, 'Not Set') MaterialGroup,
          mck.MARC_WERKS Plant,
          ifnull(MARC_MAABC, 'Not Set') ABCIndicator,
          ifnull(MARC_BESKZ, 'Not Set') ProcurementType,
          ifnull(MARC_LGFSB, 'Not Set') StorageLocation,
          ifnull(MARC_KZKRI, 'Not Set') CriticalPart,
          ifnull(MARC_DISMM, 'Not Set') MRPType,
          ifnull(MARC_BWSCL, 'Not Set') SupplySource,
          ifnull(MARC_STRGR, 'Not Set') StrategyGroup,
          ifnull(MARA_TRAGR, 'Not Set') TransportationGroup,
          ifnull(MARA_SPART, 'Not Set') Division,
		  convert(varchar(200), 'Not Set') as DivisionDescription, 			-- ifnull((SELECT TSPAT_VTEXT FROM TSPAT WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),'Not Set') DivisionDescription,
          ifnull(MARA_MTPOS, 'Not Set') GeneralItemCategory,
          ifnull(MARC_LVORM, 'Not Set') DeletionFlag,
          ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set') MaterialStatus,
          ifnull(MARA_PRDHA, 'Not Set') ProductHierarchy,
          ifnull(MARA_MFRPN, 'Not Set') MPN,
          convert(varchar(200), 'Not Set') as ProductHierarchyDescription,  -- ifnull((SELECT t.VTEXT FROM t179t t WHERE t.PRODH = mck.MARA_PRDHA), 'Not Set') ProductHierarchyDescription,
          ifnull(mck.MARC_DISPR, 'Not Set') MRPProfile,
          convert(varchar(200), 'Not Set') as MRPProfileDescription, 		-- ifnull((SELECT p.T401T_KTEXT FROM t401t p WHERE p.T401T_DISPR = mck.MARC_DISPR), 'Not Set') MRPProfileDescription,
          convert(varchar(200), 'Not Set') as PartTypeDescription, 			-- ifnull((SELECT pt.T134T_MTBEZ FROM T134T pt WHERE pt.T134T_MTART = mck.MARA_MTART), 'Not Set') PartTypeDescription,
          convert(varchar(200), 'Not Set') as MaterialGroupDescription, 	-- ifnull((SELECT mg.T023T_WGBEZ FROM T023T mg WHERE mg.T023T_MATKL = mck.MARA_MATKL), 'Not Set') MaterialGroupDescription,
          convert(varchar(200), 'Not Set') as MRPTypeDescription, 			-- ifnull((SELECT mt.T438T_DIBEZ FROM T438T mt WHERE mt.T438T_DISMM = mck.MARC_DISMM), 'Not Set') MRPTypeDescription,
          convert(varchar(200), 'Not Set') as ProcurementTypeDescription, 	-- ifnull((SELECT dd.DD07T_DDTEXT FROM DD07T dd WHERE dd.DD07T_DOMNAME = 'BESKZ' AND dd.DD07T_DOMVALUE = MARC_BESKZ), 'Not Set') ProcurementTypeDescription,
          convert(varchar(200), 'Not Set') as MRPControllerDescription, 	-- ifnull((SELECT mc.T024D_DSNAM FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set') MRPControllerDescription,
          ifnull(MARC_DISGR, 'Not Set') MRPGroup,
          convert(varchar(200), 'Not Set') as MRPGroupDescription, 			-- ifnull((SELECT mpg.T438X_TEXT40 FROM T438X mpg WHERE mpg.T438X_DISGR = mck.MARC_DISGR AND mpg.T438X_WERKS = mck.MARC_WERKS), 'Not Set') MRPGroupDescription,
          ifnull(mck.MARC_BSTMI,0.0000) MinimumLotSize,
          ifnull(mck.MARC_DISLS, 'Not Set') MRPLotSize,
          convert(varchar(200), 'Not Set') as MRPLotSizeDescription, 		-- ifnull((SELECT ml.T439T_LOSLT FROM T439T ml WHERE ml.T439T_DISLS = mck.MARC_DISLS), 'Not Set') MRPLotSizeDescription,
          convert(varchar(200), 'Not Set') as materialstatusdescription, 	-- ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)), 'Not Set') materialstatusdescription,
          ifnull(mck.MARC_SOBSL, 'Not Set') SpecialProcurement,
          convert(varchar(200), 'Not Set') as SpecialProcurementDescription, -- ifnull((SELECT sp.T460T_LTEXT FROM t460t sp WHERE sp.T460T_SOBSL = mck.MARC_SOBSL AND sp.T460T_WERKS = mck.MARC_WERKS), 'Not Set') SpecialProcurementDescription,
          ifnull(MARC_SCHGT, 'Not Set') BulkMaterial,
          mck.MARC_DZEIT InhouseProductionTime,
          ifnull(MARC_PRCTR, 'Not Set') ProfitCenterCode,
          convert(varchar(200), 'Not Set') as ProfitCenterName, 			-- ifnull((SELECT c.CEPCT_KTEXT FROM cepct c WHERE c.CEPCT_PRCTR = MARC_PRCTR AND c.CEPCT_DATBI > current_date), 'Not Set') ProfitCenterName,
          ifnull(MARA_EXTWG, 'Not Set') ExternalMaterialGroupCode,
          convert(varchar(200), 'Not Set') as ExternalMaterialGroupDescription, 	-- ifnull((SELECT t.TWEWT_EWBEZ FROM twewt t WHERE t.TWEWT_EXTWG = MARA_EXTWG), 'Not Set') ExternalMaterialGroupDescription,
          ifnull(MARC_KZAUS, 'Not Set') MaterialDiscontinuationFlag,
          convert(varchar(200), 'Not Set') as MaterialDiscontinuationFlagDescription, 	-- ifnull((SELECT DD07T_DDTEXT FROM DD07T WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS), 'Not Set') MaterialDiscontinuationFlagDescription,
          mck.MARC_BEARZ ProcessingTime,
          mck.MARC_WZEIT TotalReplenishmentLeadTime,
		  mck.MARC_WEBAZ GRProcessingTime,
		  ifnull(mck.MARA_BISMT, 'Not Set') OldPartNumber,
		  ifnull(mck.MARA_J_3ACOL, 'Not Set') AFSColor,
		  convert(varchar(200), 'Not Set') as AFSColorDescription, 			-- ifnull((SELECT J_3ACOLRT_TEXT FROM J_3ACOLRT WHERE J_3ACOLRT_J_3ACOL = mck.MARA_J_3ACOL), 'Not Set') AFSColorDescription,
		  convert(varchar(200), 'Not Set') as SDMaterialStatusDescription, 	-- ifnull((SELECT TVMST_VMSTB FROM tvmst WHERE TVMST_VMSTA = MARA_MSTAV), 'Not Set') SDMaterialStatusDescription,
		  MARA_VOLUM Volume,
		  ifnull(MARA_J_3APGNR, 'Not Set') AFSMasterGrid,
		  ifnull(MARA_AFS_SCHNITT, 'Not Set') AFSPattern,
		  ifnull(MARA_LABOR, 'Not Set') Laboratory,
		  ifnull(MARC_NCOST,'Not Set') DoNotCost,
		  ifnull(mck.MARC_MMSTA,'Not Set') PlantMaterialStatus,
		  convert(varchar(200), 'Not Set') as PlantMaterialStatusDescription, 	-- ifnull((SELECT t141t_MTSTB FROM t141t mst WHERE mst.T141T_MMSTA = mck.MARC_MMSTA), 'Not Set') PlantMaterialStatusDescription,
		  ifnull(MARC_SFEPR,'Not Set') REMProfile,
		  ifnull(MARC_FXHOR,0) PlanningTimeFence,
		  ifnull(MARC_EISBE,0.0000) SafetyStock,
		  ifnull(MARC_BSTRF,0.0000) RoundingValue,
		  ifnull(MARC_MINBE,0.0000) ReorderPoint,
		  to_date('0001-01-01','YYYY-MM-DD') as ValidFrom, 					-- (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and  j1.J_3AMAD_WERKS = mck.MARC_WERKS) ValidFrom,
		  convert(varchar(200), 'Not Set') as MRPStatus, 					-- ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and  j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set') MRPStatus,
		  ifnull(mck.MARC_MTVFP, 'Not Set') CheckingGroupCode,
		  convert(varchar(200), 'Not Set') as CheckingGroup, 				-- ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set') CheckingGroup,
		  convert(varchar(200), 'Not Set') as UPCNumber, 					-- ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set') UPCNumber,
		  ifnull(mck.MARC_MABST, 0.0000) MaximumStockLevel,
		  ifnull(mck.MARC_VRMOD, 'Not Set') ConsumptionMode,
		  convert(varchar(200), 'Not Set') as ConsumptionModeDescription, 	-- ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set') ConsumptionModeDescription,
		  ifnull(mck.MARC_VINT1, 0) ConsumptionPeriodBackward,
		  ifnull(mck.MARC_VINT2, 0) ConsumptionPeriodForward,
		  ifnull(mck.MAKT_MAKTG,'Not Set') PartLongDesc,
		  ifnull(mck.MARA_MFRNR,'Not Set') ManfacturerNumber,
		  ifnull(mck.MARA_EAN11,'Not Set') UPCCode,
		  ifnull(mck.MARA_GROES,'Not Set') NDCCode,
		  ifnull(mck.MARC_FHORI,'Not Set') SchedMarginKey,
		  ifnull(mck.MARA_RAUBE,'Not Set') storageconditioncode,
		  ifnull(mck.MARA_STOFF,'Not Set') hazardousmaterialnumber,
		  ifnull(mck.MARC_RWPRO,'Not Set') rangeofcoverage,
		  ifnull(mck.MARC_SHFLG,'Not Set') safetytimeind,
		  ifnull(mck.MARC_SHZET,0) safetytime,
		  ifnull(mck.MARC_BSTMA,0) maxlotsize,
		  ifnull(mck.MARC_BSTFE,0) fixedlotsize,
		  convert(varchar(200), 'Not Set') as MRPControllerTelephone		-- ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set') MRPControllerTelephone
		  ,MARC_EKGRP 				-- PurchaseGroupDescription
		  ,MARA_SPART				-- DivisionDescription
		  ,MARA_PRDHA				-- ProductHierarchyDescription
		  ,MARC_DISPR				-- MRPProfileDescription,
		  ,MARA_MTART				-- PartTypeDescription,
		  ,MARA_MATKL				-- MaterialGroupDescription,
		  ,MARC_DISMM				-- MRPTypeDescription,
		  ,MARC_BESKZ				-- ProcurementTypeDescription,
		  ,MARC_DISPO				-- MRPControllerDescription,   		-- use Plant
		  ,MARC_DISGR				-- MRPGroupDescription,		  		-- use Plant
		  ,MARC_DISLS				-- MRPLotSizeDescription,
		  ,MARC_MMSTA, MARA_MSTAE	-- materialstatusdescription,
		  ,MARC_SOBSL				-- SpecialProcurementDescription,	-- use Plant
		  ,MARC_PRCTR				-- ProfitCenterName,
		  ,MARA_EXTWG				-- ExternalMaterialGroupDescription,
		  ,MARC_KZAUS				-- MaterialDiscontinuationFlagDescription,
		  ,MARA_J_3ACOL				-- AFSColorDescription,
		  ,MARA_MSTAV				-- SDMaterialStatusDescription,
		  ,marc_mtvfp				-- CheckingGroup,
		  ,MARC_VRMOD				-- ConsumptionModeDescription,
			,ifnull(MARA_NORMT,'Not Set') as IndustryStandardDescription
FROM MARA_MARC_MAKT mck
        LEFT JOIN dim_plant dp ON dp.plantcode = mck.marc_werks AND dp.languagekey = makt_spras
		INNER JOIN tmp_dim_MARA_MARC_MAKT_ins p ON    p.MARA_MATNR = mck.MARA_MATNR
												  AND p.MARC_WERKS = mck.MARC_WERKS
WHERE ifnull(mck.MARC_WERKS,'NULL') <> 'NULL';

/* PurchaseGroupDescription */
update tmp_dim_part_f_ins1 t1
set t1.PurchaseGroupDescription = ifnull(t.T024_EKNAM, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t024 t on t.T024_EKGRP = MARC_EKGRP
where t1.PurchaseGroupDescription <> ifnull(t.T024_EKNAM, 'Not Set');

/* DivisionDescription */
update tmp_dim_part_f_ins1 t1
set t1.DivisionDescription = ifnull(t.TSPAT_VTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join TSPAT t on TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'
where t1.DivisionDescription <> ifnull(t.TSPAT_VTEXT, 'Not Set');

/* ProductHierarchyDescription */
update tmp_dim_part_f_ins1 t1
set t1.ProductHierarchyDescription = ifnull(t.VTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t179t t on t.PRODH = MARA_PRDHA
where t1.ProductHierarchyDescription <> ifnull(t.VTEXT, 'Not Set');

/* MRPProfileDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPProfileDescription = ifnull(t.T401T_KTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t401t t on t.T401T_DISPR = MARC_DISPR
where t1.MRPProfileDescription <> ifnull(t.T401T_KTEXT, 'Not Set');

/* PartTypeDescription */
update tmp_dim_part_f_ins1 t1
set t1.PartTypeDescription = ifnull(t.T134T_MTBEZ, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T134T t on t.T134T_MTART = MARA_MTART
where t1.PartTypeDescription <> ifnull(t.T134T_MTBEZ, 'Not Set');

/* MaterialGroupDescription */
update tmp_dim_part_f_ins1 t1
set t1.MaterialGroupDescription = ifnull(t.T023T_WGBEZ, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T023T t on t.T023T_MATKL = MARA_MATKL
where t1.MaterialGroupDescription <> ifnull(t.T023T_WGBEZ, 'Not Set');

/* MRPTypeDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPTypeDescription = ifnull(t.T438T_DIBEZ, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T438T t on t.T438T_DISMM = MARC_DISMM
where t1.MRPTypeDescription <> ifnull(t.T438T_DIBEZ, 'Not Set');

/* ProcurementTypeDescription */
update tmp_dim_part_f_ins1 t1
set t1.ProcurementTypeDescription = ifnull(t.DD07T_DDTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join DD07T t on t.DD07T_DOMNAME = 'BESKZ' AND t.DD07T_DOMVALUE = MARC_BESKZ
where t1.ProcurementTypeDescription <> ifnull(t.DD07T_DDTEXT, 'Not Set');

/* MRPControllerDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPControllerDescription = ifnull(t.T024D_DSNAM, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T024D t on t.T024D_DISPO = MARC_DISPO AND t.T024D_WERKS = Plant
where t1.MRPControllerDescription <> ifnull(t.T024D_DSNAM, 'Not Set');

/* MRPGroupDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPGroupDescription = ifnull(t.T438X_TEXT40, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T438X t on t.T438X_DISGR = MARC_DISGR AND t.T438X_WERKS = Plant
where t1.MRPGroupDescription <> ifnull(t.T438X_TEXT40, 'Not Set');

/* MRPLotSizeDescription */
update tmp_dim_part_f_ins1 t1
set t1.MRPLotSizeDescription = ifnull(t.T439T_LOSLT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T439T t on t.T439T_DISLS = MARC_DISLS
where t1.MRPLotSizeDescription <> ifnull(t.T439T_LOSLT, 'Not Set');

/* materialstatusdescription */
update tmp_dim_part_f_ins1 t1
set t1.materialstatusdescription = ifnull(t.t141t_MTSTB, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t141t t on t.T141T_MMSTA = ifnull(t1.MARA_MSTAE, 'Not Set')
where t1.materialstatusdescription <> ifnull(t.t141t_MTSTB, 'Not Set');

/* SpecialProcurementDescription */
update tmp_dim_part_f_ins1 t1
set t1.SpecialProcurementDescription = ifnull(t.T460T_LTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t460t t on t.T460T_SOBSL = MARC_SOBSL AND t.T460T_WERKS = Plant
where t1.SpecialProcurementDescription <> ifnull(t.T460T_LTEXT, 'Not Set');

/* ProfitCenterName */
update tmp_dim_part_f_ins1 t1
set t1.ProfitCenterName = ifnull(t.CEPCT_KTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join cepct t on t.CEPCT_PRCTR = MARC_PRCTR AND t.CEPCT_DATBI > current_date
where t1.ProfitCenterName <> ifnull(t.CEPCT_KTEXT, 'Not Set');

/* ExternalMaterialGroupDescription */
update tmp_dim_part_f_ins1 t1
set t1.ExternalMaterialGroupDescription = ifnull(t.TWEWT_EWBEZ, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join twewt t on t.TWEWT_EXTWG = MARA_EXTWG
where t1.ExternalMaterialGroupDescription <> ifnull(t.TWEWT_EWBEZ, 'Not Set');

/* MaterialDiscontinuationFlagDescription */
update tmp_dim_part_f_ins1 t1
set t1.MaterialDiscontinuationFlagDescription = ifnull(t.DD07T_DDTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join DD07T t on t.DD07T_DOMNAME = 'KZAUS' AND t.DD07T_DOMVALUE = MARC_KZAUS
where t1.MaterialDiscontinuationFlagDescription <> ifnull(t.DD07T_DDTEXT, 'Not Set');

/* AFSColorDescription */
update tmp_dim_part_f_ins1 t1
set t1.AFSColorDescription = ifnull(t.J_3ACOLRT_TEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join J_3ACOLRT t on t.J_3ACOLRT_J_3ACOL = MARA_J_3ACOL
where t1.AFSColorDescription <> ifnull(t.J_3ACOLRT_TEXT, 'Not Set');

/* SDMaterialStatusDescription */
update tmp_dim_part_f_ins1 t1
set t1.SDMaterialStatusDescription = ifnull(t.TVMST_VMSTB, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join tvmst t on t.TVMST_VMSTA = MARA_MSTAV
where t1.SDMaterialStatusDescription <> ifnull(t.TVMST_VMSTB, 'Not Set');

/* PlantMaterialStatusDescription */
update tmp_dim_part_f_ins1 t1
set t1.PlantMaterialStatusDescription = ifnull(t.t141t_MTSTB, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join t141t t on t.T141T_MMSTA = MARC_MMSTA
where t1.PlantMaterialStatusDescription <> ifnull(t.t141t_MTSTB, 'Not Set');

/* ValidFrom */
update tmp_dim_part_f_ins1 t1
set t1.ValidFrom = ifnull(t.J_3AMAD_J_4ADTFR, to_date('0001-01-01','YYYY-MM-DD'))
from tmp_dim_part_f_ins1 t1
		left join J_3AMAD t on t.j_3amad_matnr = PartNumber and t.J_3AMAD_WERKS = Plant
where t1.ValidFrom <> ifnull(t.J_3AMAD_J_4ADTFR, to_date('0001-01-01','YYYY-MM-DD'));

/* MRPStatus */
update tmp_dim_part_f_ins1 t1
set t1.MRPStatus = ifnull(t.J_3AMAD_J_4ASTAT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join J_3AMAD t on t.j_3amad_matnr = PartNumber and t.J_3AMAD_WERKS = Plant
where t1.MRPStatus <> ifnull(t.J_3AMAD_J_4ASTAT, 'Not Set');

/* CheckingGroup */
update tmp_dim_part_f_ins1 t1
set t1.CheckingGroup = ifnull(t.TMVFT_BEZEI, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join TMVFT t on t.tmvft_mtvfp = marc_mtvfp
where t1.CheckingGroup <> ifnull(t.TMVFT_BEZEI, 'Not Set');

/* UPCNumber ambiguos replace */
merge into tmp_dim_part_f_ins1 dim
using(select dp.rowid rid, ifnull(max(t.MEAN_EAN11),'Not Set') as UPCNumber
	  from tmp_dim_part_f_ins1 dp
				left join MEAN t on t.MEAN_MATNR = dp.PartNumber
	  group by dp.rowid
	  ) src on dim.rowid = src.rid
when matched then update set dim.UPCNumber = src.UPCNumber
where dim.UPCNumber <> src.UPCNumber;

/* ConsumptionModeDescription */
update tmp_dim_part_f_ins1 t1
set t1.ConsumptionModeDescription = ifnull(t.DD07T_DDTEXT, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join DD07T t on t.DD07T_DOMNAME = 'VRMOD' AND t.DD07T_DOMVALUE = MARC_VRMOD
where t1.ConsumptionModeDescription <> ifnull(t.DD07T_DDTEXT, 'Not Set');

/* MRPControllerTelephone */
update tmp_dim_part_f_ins1 t1
set t1.MRPControllerTelephone = ifnull(t.T024D_DSTEL, 'Not Set')
from tmp_dim_part_f_ins1 t1
		left join T024D t on t.T024D_DISPO = MARC_DISPO AND t.T024D_WERKS = Plant
where t1.MRPControllerTelephone <> ifnull(t.T024D_DSTEL, 'Not Set');

delete from number_fountain m where m.table_name = 'dim_part';

insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1;

INSERT INTO dim_part(dim_partid,
                     PartNumber,
                     PartDescription,
                     Revision,
                     UnitOfMeasure,
                     RowStartDate,
                     RowIsCurrent,
                     CommodityCode,
                     PartType,
                     LeadTime,
                     StockLocation,
                     PurchaseGroupCode,
                     PurchaseGroupDescription,
                     MRPController,
                     MaterialGroup,
                     Plant,
                     ABCIndicator,
                     ProcurementType,
                     StorageLocation,
                     CriticalPart,
                     MRPType,
                     SupplySource,
                     StrategyGroup,
                     TransportationGroup,
                     Division,
                     DivisionDescription,
                     GeneralItemCategory,
                     DeletionFlag,
                     MaterialStatus,
                     ProductHierarchy,
                     MPN,
                     ProductHierarchyDescription,
                     MRPProfile,
                     MRPProfileDescription,
                     PartTypeDescription,
                     MaterialGroupDescription,
                     MRPTypeDescription,
                     ProcurementTypeDescription,
                     MRPControllerDescription,
                     MRPGroup,
                     MRPGroupDescription,
                     MinimumLotSize,
                     MRPLotSize,
                     MRPLotSizeDescription,
                     MaterialStatusDescription,
                     SpecialProcurement,
                     SpecialProcurementDescription,
                     BulkMaterial,
                     InhouseProductionTime,
                     ProfitCenterCode,
                     ProfitCenterName,
                     ExternalMaterialGroupCode,
                     ExternalMaterialGroupDescription,
                     MaterialDiscontinuationFlag,
                     MaterialDiscontinuationFlagDescription,
                     ProcessingTime,
                     TotalReplenishmentLeadTime,
                     GRProcessingTime,
                     OldPartNumber,
                     AFSColor,
                     AFSColorDescription,
                     SDMaterialStatusDescription,
                     Volume,
                     AFSMasterGrid,
                     AFSPattern,
                     Laboratory,
                     DoNotCost,
                     PlantMaterialStatus,
                     PlantMaterialStatusDescription,
                     REMProfile,
                     PlanningTimeFence,
                     SafetyStock,
                     RoundingValue,
                     ReorderPoint,
                     ValidFrom,
                     MRPStatus,
                     CheckingGroupCode,
                     CheckingGroup,
                        UPCNumber,
                        MaximumStockLevel,
                        ConsumptionMode,
                        ConsumptionModeDescription,
                        ConsumptionPeriodBackward,
                        ConsumptionPeriodForward,
                        PartLongDesc,
			ManfacturerNumber,
                        UPCCode,
                        NDCCode,
						SchedMarginKey,
						storageconditioncode,
						hazardousmaterialnumber,
						rangeofcoverage,
						safetytimeind,
						safetytime,
						maxlotsize,
						fixedlotsize,
						MRPControllerTelephone,
					IndustryStandardDescription)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') + row_number() over(order by ''), t.*
   from (SELECT DISTINCT PartNumber,
                     PartDescription,
                     Revision,
                     UnitOfMeasure,
                     RowStartDate,
                     RowIsCurrent,
                     CommodityCode,
                     PartType,
                     LeadTime,
                     StockLocation,
                     PurchaseGroupCode,
                     PurchaseGroupDescription,
                     MRPController,
                     MaterialGroup,
                     Plant,
                     ABCIndicator,
                     ProcurementType,
                     StorageLocation,
                     CriticalPart,
                     MRPType,
                     SupplySource,
                     StrategyGroup,
                     TransportationGroup,
                     Division,
                     DivisionDescription,
                     GeneralItemCategory,
                     DeletionFlag,
                     MaterialStatus,
                     ProductHierarchy,
                     MPN,
                     ProductHierarchyDescription,
                     MRPProfile,
                     MRPProfileDescription,
                     PartTypeDescription,
                     MaterialGroupDescription,
                     MRPTypeDescription,
                     ProcurementTypeDescription,
                     MRPControllerDescription,
                     MRPGroup,
                     MRPGroupDescription,
                     MinimumLotSize,
                     MRPLotSize,
                     MRPLotSizeDescription,
                     MaterialStatusDescription,
                     SpecialProcurement,
                     SpecialProcurementDescription,
                     BulkMaterial,
                     InhouseProductionTime,
                     ProfitCenterCode,
                     ProfitCenterName,
                     ExternalMaterialGroupCode,
                     ExternalMaterialGroupDescription,
                     MaterialDiscontinuationFlag,
                     MaterialDiscontinuationFlagDescription,
                     ProcessingTime,
                     TotalReplenishmentLeadTime,
                     GRProcessingTime,
                     OldPartNumber,
                     AFSColor,
                     AFSColorDescription,
                     SDMaterialStatusDescription,
                     Volume,
                     AFSMasterGrid,
                     AFSPattern,
                     Laboratory,
                     DoNotCost,
                     PlantMaterialStatus,
                     PlantMaterialStatusDescription,
                     REMProfile,
                     PlanningTimeFence,
                     SafetyStock,
                     RoundingValue,
                     ReorderPoint,
                     ValidFrom,
                     MRPStatus,
                     CheckingGroupCode,
                     CheckingGroup,
                        UPCNumber,
                        MaximumStockLevel,
                        ConsumptionMode,
                        ConsumptionModeDescription,
                        ConsumptionPeriodBackward,
                        ConsumptionPeriodForward,
                        PartLongDesc,
						ManfacturerNumber,
                        UPCCode,
                        NDCCode,
						SchedMarginKey,
						storageconditioncode,
						hazardousmaterialnumber,
						rangeofcoverage,
						safetytimeind,
						safetytime,
						maxlotsize,
						fixedlotsize,
						MRPControllerTelephone,
						IndustryStandardDescription
		 from tmp_dim_part_f_ins1) t
		 where not exists (select 1 from dim_part tt where tt.PartNumber = t.PartNumber and tt.Plant = t.Plant);

drop table if exists tmp_dim_part_f_ins1;


/* Octavian: Add the partnumbers without a plant for MARA */
/* This values will just be used in the Material Master Subject Area. The other SAs will still be based on partnumber and plant combination. There are some other cases as well like Material Listing that doesn't have
a plant nor can be joined with a table that has a plant, but has a material, that will benefit from this change */
delete from dim_part where sourceflag = 'mara'; /* the rows from MARA_MARC_MAKT are defaulted with mara_marc value, and the rows from MARA are inserted using mara on the flag */

delete from number_fountain m where m.table_name = 'dim_part';

insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1;

insert into dim_part
(dim_partid,
PartNumber,PartType,MaterialGroup,ProductHierarchy,TransportationGroup,Division,GeneralItemCategory,Revision,UnitOfMeasure,MPN,
crossmatplantsts,changedate,creationdate,ExternalMaterialGroupCode,OldPartNumber,SDMaterialStatusDescription,Volume,Laboratory,basicmaterial,UPCCode,NDCCode,
ManfacturerNumber,storageconditioncode,hazardousmaterialnumber,purchasevaluekey,qminprocureactive,grossweight,orderunit,creator,weightunit,periodindforSLED,
delflagatclientlevel,totalshelflife,minremainshelflife,netweight,maintenancestatus,roundingruleSLED,Xdistrchainvalidfromdate,expirationdate,completeMaintenancestatus,numberofslips,
IndustryStandardDescription,rowiscurrent,dw_insert_date,dw_update_date,
plant,sourceflag)
select
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') + row_number() over(order by ''),
MARA_MATNR,ifnull(MARA_MTART,'Not Set'),ifnull(MARA_MATKL,'Not Set'),ifnull(MARA_PRDHA,'Not Set'),ifnull(MARA_TRAGR,'Not Set'),ifnull(MARA_SPART,'Not Set'),ifnull(MARA_MTPOS,'Not Set'),ifnull(MARA_KZREV,'Not Set'),ifnull(MARA_MEINS,'Not Set'),ifnull(MARA_MFRPN,'Not Set'),
ifnull(MARA_MSTAE,'Not Set'),ifnull(MARA_LAEDA,'0001-01-01'),ifnull(MARA_ERSDA,'0001-01-01'),ifnull(MARA_EXTWG,'Not Set'),ifnull(MARA_BISMT,'Not Set'),ifnull(MARA_MSTAV,'Not Set'),ifnull(MARA_VOLUM,0),ifnull(MARA_LABOR,'Not Set'),ifnull(MARA_WRKST,'Not Set'),ifnull(MARA_EAN11,'Not Set'),ifnull(MARA_GROES,'Not Set'),
ifnull(MARA_MFRNR,'Not Set'),ifnull(MARA_RAUBE,'Not Set'),ifnull(MARA_STOFF,'Not Set')/*,MARA_TEMPB*/,ifnull(MARA_EKWSL,'Not Set'),ifnull(MARA_QMPUR,'Not Set'),ifnull(MARA_BRGEW,0),ifnull(MARA_BSTME,'Not Set'),ifnull(MARA_ERNAM,'Not Set'),ifnull(MARA_GEWEI,'Not Set'),ifnull(MARA_IPRKZ,'Not Set'),
ifnull(MARA_LVORM,'Not Set'),ifnull(MARA_MHDHB,0),ifnull(MARA_MHDRZ,0),ifnull(MARA_NTGEW,0),ifnull(MARA_PSTAT,'Not Set'),ifnull(MARA_RDMHD,'Not Set'),ifnull(MARA_MSTDE,'0001-01-01'),ifnull(MARA_SLED_BBD,'Not Set'),ifnull(MARA_VPSTA,'Not Set'),ifnull(MARA_WESCH,0),
ifnull(MARA_NORMT,'Not Set'),1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,
'Not S' as plant,'mara' as sourceflag
from MARA m
where not exists (select 1 from dim_part dp
where m.MARA_MATNR = dp.partnumber);

/* Madalina 14 Jul 2016 - comment all the updates with sourceflag = 'mara'. All these attributes from dim_part will be updated from MARA - BI- 3461 */
/*UPDATE dim_part dp
SET dp.DivisionDescription = ifnull(TSPAT_VTEXT,'Not Set'),
      dp.dw_update_date = current_timestamp
from dim_part dp , MARA m left join TSPAT on TSPAT_SPART = ifnull(m.MARA_SPART,'Not Set')
WHERE dp.PartNumber = m.MARA_MATNR
AND TSPAT_SPRAS = 'E'
AND dp.DivisionDescription <> ifnull(TSPAT_VTEXT,'Not Set')
AND sourceflag = 'mara'

UPDATE dim_part dp
SET dp.ExternalMaterialGroupDescription = ifnull(t.TWEWT_EWBEZ,'Not Set'),
    dp.dw_update_date = current_timestamp
from dim_part dp , MARA m left join twewt t ON t.TWEWT_EXTWG = ifnull(m.MARA_EXTWG,'Not Set')
WHERE dp.PartNumber = m.MARA_MATNR
AND dp.ExternalMaterialGroupDescription <> ifnull(t.TWEWT_EWBEZ,'Not Set')
AND sourceflag = 'mara'

UPDATE dim_part dp
SET dp.SDMaterialStatusDescription = ifnull(TVMST_VMSTB,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp , MARA m left join tvmst ON TVMST_VMSTA = ifnull(MARA_MSTAV,'Not Set')
WHERE dp.PartNumber = m.MARA_MATNR
and dp.SDMaterialStatusDescription <> ifnull(TVMST_VMSTB,'Not Set')
AND sourceflag = 'mara'*/

merge into dim_part dp1
using(select dp.dim_partid , ifnull(max(t.MEAN_EAN11),'Not Set') as UPCNumber
	  from dim_part dp
				left join MEAN t on t.MEAN_MATNR = dp.PartNumber
           WHERE  sourceflag = 'mara'
	  group by dp.dim_partid
	  ) src on dp1.dim_partid = src.dim_partid
when matched then update set dp1.UPCNumber = src.UPCNumber
where dp1.UPCNumber <> src.UPCNumber;

UPDATE dim_part dp
SET dp.PartDescription = ifnull(m.MAKT_MAKTX,'Not Set'),
dp.dw_update_date = current_timestamp
from MAKT m, dim_part dp
WHERE     dp.PartNumber = m.MAKT_MATNR
and m.MAKT_SPRAS = 'E'
and dp.PartDescription <> ifnull(m.MAKT_MAKTX,'Not Set');

UPDATE dim_part dp
SET dp.PartDescription = ifnull(m.MAKT_MAKTX,'Not Set'),
dp.dw_update_date = current_timestamp
FROM MAKT m, dim_part dp
WHERE     dp.PartNumber = m.MAKT_MATNR
and dp.PartDescription = 'Not Set'
AND m.MAKT_SPRAS <> 'E'
and dp.PartDescription <> ifnull(m.MAKT_MAKTX,'Not Set');

UPDATE dim_part dp
SET dp.PartLongDesc = ifnull(m.MAKT_MAKTG,'Not Set'),
dp.dw_update_date = current_timestamp
 FROM MAKT m, dim_part dp
WHERE     dp.PartNumber = m.MAKT_MATNR
and m.MAKT_SPRAS = 'E'
and dp.PartLongDesc <> ifnull(m.MAKT_MAKTG,'Not Set');

UPDATE dim_part dp
SET dp.PartLongDesc = ifnull(m.MAKT_MAKTG,'Not Set'),
dp.dw_update_date = current_timestamp
FROM MAKT m, dim_part dp
WHERE     dp.PartNumber = m.MAKT_MATNR
and dp.PartLongdesc = 'Not Set'
AND m.MAKT_SPRAS <> 'E'
and dp.PartLongDesc <> ifnull(m.MAKT_MAKTG,'Not Set');
/* Octavian: Add the partnumbers without a plant for MARA */


/* Andrian: Add new part and plant from PRA Stock Qty*/
delete from number_fountain m where m.table_name = 'dim_part';

insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1;

insert into dim_part
(dim_partid,PartNumber,Plant,Flag_Demandforecast,rowiscurrent)
select
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') + row_number() over(order by ''),
uin,
plantcode,
'N' as Flag_Demandforecast,
1 from newiteminstockbrinit b
where
not exists (select 1 from dim_part a
where a.partnumber = b.uin and a.plant = b.plantcode);
/* Andrian: Add new part and plant from PRA Stock Qty*/

UPDATE dim_part dp
SET  dp.PartNumber_NoLeadZero = ifnull(case when length(dp.partnumber) = 18 and dp.partnumber REGEXP_LIKE '[0-9]*' then trim(leading '0' from dp.partnumber) else dp.partnumber end,'Not Set'),
	 dp.dw_update_date = current_timestamp
FROM dim_part dp;

update dim_part dp
set dp.loadinggroup = ifnull(ds.marc_ladgr,'Not Set'),
    dp.dw_update_date = current_timestamp
from mara_marc_makt ds, dim_part dp
where     dp.partnumber = ds.mara_matnr
	  and dp.plant = marc_werks
	  and dp.loadinggroup <> ifnull(ds.marc_ladgr,'Not Set');

truncate table tmp_dim_MARA_MARC_MAKT_ins;
truncate table tmp_dim_MARA_MARC_MAKT_del;
		/* VW Original
			call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_ins-tmp_dim_MARA_MARC_MAKT_ins')
			call vectorwise (combine 'tmp_dim_MARA_MARC_MAKT_del-tmp_dim_MARA_MARC_MAKT_del') */


UPDATE MBEW m
   SET VERPR = MBEW_VERPR
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_VERPR FROM MARA_MARC_MBEW) mmm, MBEW m
WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND VERPR <> MBEW_VERPR;


UPDATE MBEW m
   SET STPRS = MBEW_STPRS
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_STPRS FROM MARA_MARC_MBEW) mmm, MBEW m
WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND STPRS <> MBEW_STPRS;

UPDATE MBEW m
   SET SALK3 = MBEW_SALK3
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_SALK3 FROM MARA_MARC_MBEW) mmm, MBEW m
 WHERE     m.MATNR = mmm.MBEW_MATNR
        AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND SALK3 <> MBEW_SALK3;

UPDATE MBEW m
   SET PEINH = MBEW_PEINH
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_PEINH FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND PEINH <> MBEW_PEINH;

UPDATE MBEW m
   SET m.MBEW_LBKUM = mmm.MBEW_LBKUM
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_LBKUM FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_LBKUM <> mmm.MBEW_LBKUM;

UPDATE MBEW m
   SET LFMON = MBEW_LFMON
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND LFMON <> MBEW_LFMON;

UPDATE MBEW m
   SET m.MBEW_VMSTP = mmm.MBEW_VMSTP
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_VMSTP FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE m.MATNR = mmm.MBEW_MATNR
	AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.MBEW_VMSTP <> mmm.MBEW_VMSTP
       AND m.LFMON = mmm.MBEW_LFMON;

UPDATE MBEW m
   SET VPRSV = MBEW_VPRSV
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_VPRSV FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND VPRSV <> MBEW_VPRSV
       AND m.LFMON = mmm.MBEW_LFMON;

UPDATE MBEW m
   SET m.MBEW_VJSTP = mmm.MBEW_VJSTP
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_VJSTP FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_VJSTP <> mmm.MBEW_VJSTP;

UPDATE MBEW m
   SET m.MBEW_VMPEI = mmm.MBEW_VMPEI
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_VMPEI FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND  m.MBEW_VMPEI <> mmm.MBEW_VMPEI;

UPDATE MBEW m
   SET m.MBEW_VJPEI = mmm.MBEW_VJPEI
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_VJPEI FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND  m.MBEW_VJPEI <> mmm.MBEW_VJPEI;

UPDATE MBEW m
   SET LVORM = MBEW_LVORM
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_LVORM FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND LVORM <> MBEW_LVORM;

UPDATE MBEW m
   SET m.MBEW_ZPLD1 = mmm.MBEW_ZPLD1
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_ZPLD1 FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_ZPLD1 <> mmm.MBEW_ZPLD1;

UPDATE MBEW m
   SET        m.MBEW_KALN1 = mmm.MBEW_KALN1
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_KALN1 FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON;

UPDATE MBEW m
   SET        m.MBEW_BWPRH = mmm.MBEW_BWPRH
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_BWPRH FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_BWPRH <> mmm.MBEW_BWPRH;

UPDATE MBEW m
   SET        m.MBEW_LPLPR = mmm.MBEW_LPLPR
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_LPLPR FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON;

UPDATE MBEW m
   SET m.MBEW_VMVER = mmm.MBEW_VMVER
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_VMVER FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_VMVER <> mmm.MBEW_VMVER;

UPDATE MBEW m
   SET m.MBEW_ZPLP1 = mmm.MBEW_ZPLP1
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_ZPLP1 FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON;

UPDATE MBEW m
   SET m.MBEW_STPRV = mmm.MBEW_STPRV
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_STPRV FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_STPRV <> mmm.MBEW_STPRV;

UPDATE MBEW m
   SET m.MBEW_VPLPR = mmm.MBEW_VPLPR
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_VPLPR FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON;

UPDATE MBEW m
   SET m.MBEW_LAEPR = mmm.MBEW_LAEPR
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_LAEPR FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND  m.MBEW_LAEPR <> mmm.MBEW_LAEPR;

UPDATE MBEW m
   SET m.MBEW_ZPLD2 = mmm.MBEW_ZPLD2
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_ZPLD2 FROM MARA_MARC_MBEW) mmm, MBEW m
       WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_ZPLD2 <> mmm.MBEW_ZPLD2;

UPDATE MBEW m
   SET m.MBEW_BWVA2 = mmm.MBEW_BWVA2
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_BWVA2 FROM MARA_MARC_MBEW) mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_BWVA2 <> mmm.MBEW_BWVA2;

UPDATE MBEW m
   SET 	m.MBEW_KALNR = mmm.MBEW_KALNR
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_KALNR FROM MARA_MARC_MBEW) mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_KALNR <> mmm.MBEW_KALNR;

UPDATE MBEW m
   SET 	m.MARA_LAEDA = mmm.MARA_LAEDA
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MARA_LAEDA FROM MARA_MARC_MBEW) mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MARA_LAEDA <> mmm.MARA_LAEDA;

UPDATE MBEW m
   SET 	m.MBEW_ZPLPR = mmm.MBEW_ZPLPR
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_ZPLPR FROM MARA_MARC_MBEW) mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND 	m.MBEW_ZPLPR <> mmm.MBEW_ZPLPR;

UPDATE MBEW m
   SET 	m.MBEW_PDATL = mmm.MBEW_PDATL
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_PDATL FROM MARA_MARC_MBEW) mmm, MBEW m
	WHERE     m.MATNR = mmm.MBEW_MATNR
              AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_PDATL <> mmm.MBEW_PDATL;

UPDATE MBEW m
   SET 	m.MBEW_PPRDL = mmm.MBEW_PPRDL
FROM (SELECT DISTINCT MBEW_MATNR,MBEW_BWKEY,MBEW_LFGJA,MBEW_BWTAR,MBEW_LFMON,MBEW_PPRDL FROM MARA_MARC_MBEW) mmm, MBEW m
 WHERE     m.MATNR = mmm.MBEW_MATNR
       AND m.BWKEY = mmm.MBEW_BWKEY
       AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON
       AND m.MBEW_PPRDL <> mmm.MBEW_PPRDL;

DROP TABLE IF EXISTS MBEW_000;

CREATE TABLE MBEW_000
AS
   SELECT MATNR,
          BWKEY,
          LFGJA,
          ifnull(mb.BWTAR, 'Not Set') BWTAR,
          LFMON
     FROM MBEW mb;

INSERT INTO MBEW(VERPR,
                 STPRS,
                 SALK3,
                 PEINH,
                 MBEW_LBKUM,
                 LFMON,
                 LFGJA,
                 MBEW_VMSTP,
                 VPRSV,
                 MBEW_VJSTP,
                 MBEW_VMPEI,
                 MBEW_VJPEI,
                 LVORM,
                 MATNR,
                 BWTAR,
                 BWKEY,
                 MBEW_ZPLD1,
                 MBEW_KALN1,
                 MBEW_BWPRH,
                 MBEW_LPLPR,
                 MBEW_VMVER,
                 MBEW_ZPLP1,
                 MBEW_STPRV,
                 MBEW_VPLPR,
                 MBEW_LAEPR,
                 MBEW_ZPLD2,
                 MBEW_BWVA2,
                 MBEW_KALNR,
                 MARA_LAEDA,
                 MBEW_ZPLPR,
                 MBEW_PDATL,
                 MBEW_PPRDL)
   SELECT DISTINCT MBEW_VERPR,
                   MBEW_STPRS,
                   MBEW_SALK3,
                   MBEW_PEINH,
                   MBEW_LBKUM,
                   MBEW_LFMON,
                   MBEW_LFGJA,
                   MBEW_VMSTP,
                   MBEW_VPRSV,
                   MBEW_VJSTP,
                   MBEW_VMPEI,
                   MBEW_VJPEI,
                   MBEW_LVORM,
                   MBEW_MATNR,
                   MBEW_BWTAR,
                   MBEW_BWKEY,
                   MBEW_ZPLD1,
                   MBEW_KALN1,
                   MBEW_BWPRH,
                   MBEW_LPLPR,
                   MBEW_VMVER,
                   MBEW_ZPLP1,
                   MBEW_STPRV,
                   MBEW_VPLPR,
                   MBEW_LAEPR,
                   MBEW_ZPLD2,
                   MBEW_BWVA2,
                   MBEW_KALNR,
                   MARA_LAEDA,
                   MBEW_ZPLPR,
                   MBEW_PDATL,
                   MBEW_PPRDL
     FROM MARA_MARC_MBEW mmm
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM MBEW_000 mb
                   WHERE     mb.MATNR = mmm.MBEW_MATNR
                         AND mb.BWKEY = mmm.MBEW_BWKEY
                         AND mb.LFGJA = mmm.MBEW_LFGJA
                         AND BWTAR = ifnull(mmm.MBEW_BWTAR, 'Not Set')
                         AND mb.LFMON = mmm.MBEW_LFMON);

DROP TABLE IF EXISTS MBEW_000;

truncate table mbew_no_bwtar;
		/* VW Original: CALL vectorwise(combine 'mbew_no_bwtar-mbew_no_bwtar') */

DROP TABLE IF EXISTS SALK3_000;

CREATE TABLE SALK3_000
AS
   SELECT a.MATNR,
          a.BWKEY,
          ((a.LFGJA * 100) + a.LFMON) LFGJA_LFMON,
          max(a.SALK3) MAXSALKS,
          max(a.MBEW_KALNR) MAXMBEW_KALNR
     FROM mbew a
    WHERE a.BWTAR IS NULL
   GROUP BY a.MATNR, a.BWKEY, ((a.LFGJA * 100) + a.LFMON);

INSERT INTO mbew_no_bwtar(BWKEY,
                          MATNR,
                          LVORM,
                          VPRSV,
                          VERPR,
                          STPRS,
                          LFGJA,
                          LFMON,
                          PEINH,
                          SALK3,
                          BWTAR,
                          MBEW_LBKUM,
                          MBEW_VMPEI,
                          MBEW_VMSTP,
                          MBEW_VJSTP,
                          MBEW_VJPEI,
                          MBEW_ZPLD1,
                          MBEW_KALN1,
                          MBEW_VMVER,
                          MBEW_STPRV,
                          MBEW_LAEPR,
                          MBEW_BWPRH,
                          MBEW_ZPLP1,
                          MBEW_ZPLD2,
                          MBEW_VPLPR,
                          MBEW_LPLPR,
                          MBEW_BWVA2,
                          MBEW_KALNR,
                          MARA_LAEDA,
                          MBEW_ZPLPR,
                          MBEW_PDATL,
                          MBEW_PPRDL)
   SELECT DISTINCT BWKEY,
                   MATNR,
                   LVORM,
                   VPRSV,
                   VERPR,
                   STPRS,
                   LFGJA,
                   LFMON,
                   PEINH,
                   SALK3,
                   BWTAR,
                   MBEW_LBKUM,
                   MBEW_VMPEI,
                   MBEW_VMSTP,
                   MBEW_VJSTP,
                   MBEW_VJPEI,
                   NULL MBEW_ZPLD1,
                   MBEW_KALN1,
                   MBEW_VMVER,
                   MBEW_STPRV,
                   NULL MBEW_LAEPR,
                   MBEW_BWPRH,
                   MBEW_ZPLP1,
                   NULL MBEW_ZPLD2,
                   MBEW_VPLPR,
                   MBEW_LPLPR,
                   MBEW_BWVA2,
                   MBEW_KALNR,
                   NULL MARA_LAEDA,
                   MBEW_ZPLPR,
                   MBEW_PDATL,
                   MBEW_PPRDL
     FROM mbew m
    WHERE BWTAR IS NULL
          AND SALK3 IN
                 (SELECT MAXSALKS
                    FROM SALK3_000 a
                   WHERE     a.MATNR = m.MATNR
                         AND a.BWKEY = m.BWKEY
                         AND LFGJA_LFMON = ((m.LFGJA * 100) + m.LFMON))
          AND MBEW_KALNR IN
                 (SELECT MAXMBEW_KALNR
                    FROM SALK3_000 a
                   WHERE     a.MATNR = m.MATNR
                         AND a.BWKEY = m.BWKEY
                         AND LFGJA_LFMON = ((m.LFGJA * 100) + m.LFMON));

DROP TABLE IF EXISTS SALK3_000;

merge into dim_part dpt
using (select distinct dim_partid,MARA_J_3AGEND,atg.J_3AGENDRT_TEXT
from dim_part dpt
		inner join mara_marc_makt mck on    dpt.PartNumber = mck.MARA_MATNR
										AND dpt.Plant = mck.MARC_WERKS
		left join dim_plant p on    p.plantcode = marc_werks
								 AND p.languagekey = makt_spras
		left join j_3agendrt atg on atg.J_3AGENDRT_J_3AGEND = mck.MARA_J_3AGEND) t
on t.dim_partid=dpt.dim_partid
when matched then update set 
dpt.AFSTargetGroup = ifnull(t.MARA_J_3AGEND, 'Not Set'),
       dpt.AFSTargetGroupDescription = ifnull(t.J_3AGENDRT_TEXT, 'Not Set'),
			dpt.dw_update_date = current_timestamp;
			
/* Madalina 14 Jul 2016 - update ExtManfacturerCode and ExtManfacturerDesc after updating ManfacturerNumber - BI-3461 */
/*UPDATE dim_part dpt
 SET dpt.ExtManfacturerCode = dv.VendorMfgCode,
     dpt.ExtManfacturerDesc = dv.VendorName,
			dpt.dw_update_date = current_timestamp
from  dim_Vendor dv, dim_part dpt
WHERE dpt.ManfacturerNumber = dv.VendorNumber*/

update dim_part
set 	ValidFrom ='0001-01-01',
	dw_update_date = current_timestamp
where ValidFrom is null;

/* 17 feb 2015 */
/* Madalina 14 Jul 2016 - update storageconditiondescription after updating storageconditioncode - BI-3461 */
/*update  dim_part pt
set storageconditiondescription = ifnull(rbtxt,'Not Set'),
	dw_update_date = current_timestamp
from t142t t, dim_part pt
where storageconditioncode = raube
and storageconditioncode <> ifnull(rbtxt,'Not Set')*/

/* 19 feb 2015 */
merge into dim_part pt
using
(select distinct pt.dim_partid,T439A.T439A_LOSKZ
 from T439A, dim_part pt
 where T439A.T439A_DISLS = pt.MRPLotSize) t on t.dim_partid=pt.dim_partid
when matched then update  set lotsizeindicator = ifnull(t.T439A_LOSKZ,'Not Set'),
 dw_update_date = current_timestamp
where lotsizeindicator <> ifnull(t.T439A_LOSKZ,'Not Set');

merge into dim_part pt
using
(select distinct pt.dim_partid,T439A.T439A_PERAZ
 from T439A, dim_part pt
 where T439A.T439A_DISLS = pt.MRPLotSize) t on t.dim_partid=pt.dim_partid
when matched then update  set 
numberofperiods = ifnull(t.T439A_PERAZ,0),
 dw_update_date = current_timestamp
where numberofperiods <> ifnull(t.T439A_PERAZ,0);

merge into dim_part pt
using
(select distinct pt.dim_partid,T438R.RWPER
from T438R, dim_part pt
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage) t on t.dim_partid=pt.dim_partid
when matched then update  set periodindicator = ifnull(t.RWPER,'Not Set'),
	dw_update_date = current_timestamp
where periodindicator <> ifnull(t.RWPER,'Not Set');

merge into dim_part pt
using
(select distinct pt.dim_partid,T438R.RW1TG
from T438R, dim_part pt
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage) t on t.dim_partid=pt.dim_partid
when matched then update  set targetcoverage1 = ifnull(t.RW1TG,0),
	dw_update_date = current_timestamp
where targetcoverage1 <> ifnull(t.RW1TG,0);


merge into dim_part pt
using
(select distinct pt.dim_partid,T438R.RWART
from T438R, dim_part pt
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage) t on t.dim_partid=pt.dim_partid
when matched then update  set typeperiodlength = ifnull(t.RWART,'Not Set'),
	dw_update_date = current_timestamp
where typeperiodlength <> ifnull(t.RWART,'Not Set');


/* end 19 feb 2015 */

/* Madalina - 18 May 2018 - APP-9645- Adding Days Per Period field */
/*
merge into dim_part pt using
( select distinct pt.dim_partid, T438R.RWNTG
  from T438R, dim_part pt
where T438R.WERKS = pt.plant and T438R.RWPRO = pt.rangeofcoverage) t 
on t.dim_partid=pt.dim_partid
when matched then update  
set daysPerPeriod = ifnull(t.RWNTG,1), 
  dw_update_date = current_timestamp
where daysPerPeriod <> ifnull(t.RWNTG,1)
*/
merge into dim_part pt using
 (
 select  pt.dim_partid, max(T438R.RWNTG) as RWNTG
 from T438R, dim_part pt
 where T438R.WERKS = pt.plant 
and T438R.RWPRO = pt.rangeofcoverage
group by  pt.dim_partid
) t 
 on t.dim_partid=pt.dim_partid

 when matched then update 
 set daysPerPeriod = ifnull(t.RWNTG,1), 
 dw_update_date = current_timestamp
 where daysPerPeriod <> ifnull(t.RWNTG,1);

/* Madalina - 30 May 2018 - Since T438R.RWNTG coming 0 from SAP, replace with 1 */
update dim_part
set daysPerPeriod = 1
where daysPerPeriod = 0;

INSERT INTO tmp_MARA_MARC_MBEWH_ins
select mmh.MBEWH_MATNR,mmh.MBEWH_BWKEY,mmh.MBEWH_LFGJA,mmh.MBEWH_LFMON,MBEWH_BWTAR
from MARA_MARC_MBEWH mmh;

insert into tmp_MARA_MARC_MBEWH_del
select mh.MATNR,mh.BWKEY,mh.LFGJA,mh.LFMON,BWTAR
from MBEWH mh;

merge into tmp_MARA_MARC_MBEWH_ins dst
using (select distinct t1.rowid rid
	   from tmp_MARA_MARC_MBEWH_ins t1
				inner join tmp_MARA_MARC_MBEWH_del t2 on    ifnull(t1.MBEWH_MATNR,'Not Set') = ifnull(t2.MBEWH_MATNR,'Not Set')
														and ifnull(t1.MBEWH_BWKEY,'Not Set') = ifnull(t2.MBEWH_BWKEY,'Not Set')
														and ifnull(t1.MBEWH_LFGJA, 0.00) = ifnull(t2.MBEWH_LFGJA, 0.00)
														and ifnull(t1.MBEWH_LFMON, 0.00) = ifnull(t2.MBEWH_LFMON, 0.00)
														and ifnull(t1.MBEWH_BWTAR,'Not Set') = ifnull(t2.MBEWH_BWTAR,'Not Set')) src
on dst.rowid = src.rid
when matched then delete;
			/* VW Original:
				call vectorwise (combine 'tmp_MARA_MARC_MBEWH_ins-tmp_MARA_MARC_MBEWH_del')*/

INSERT INTO MBEWH(VERPR,
                  STPRS,
                  SALK3,
                  PEINH,
                  MBEWH_LBKUM,
                  LFMON,
                  LFGJA,
                  VPRSV,
                  MATNR,
                  BWTAR,
                  BWKEY)
   SELECT distinct mmh.MBEWH_VERPR,
          mmh.MBEWH_STPRS,
          mmh.MBEWH_SALK3,
          mmh.MBEWH_PEINH,
          mmh.MBEWH_LBKUM,
          mmh.MBEWH_LFMON,
          mmh.MBEWH_LFGJA,
          mmh.MBEWH_VPRSV,
          mmh.MBEWH_MATNR,
          mmh.MBEWH_BWTAR,
          mmh.MBEWH_BWKEY
     FROM MARA_MARC_MBEWH mmh, tmp_MARA_MARC_MBEWH_ins mh
        WHERE mmh.MBEWH_MATNR = mh.MBEWH_MATNR
        AND mmh.MBEWH_BWKEY = mh.MBEWH_BWKEY
        AND mmh.MBEWH_LFGJA = mh.MBEWH_LFGJA
        AND mmh.MBEWH_LFMON = mh.MBEWH_LFMON
        AND ifnull(mmh.MBEWH_BWTAR, 'Not Set') = ifnull(mh.MBEWH_BWTAR, 'Not Set');

delete from mbewh_no_bwtar;

INSERT INTO mbewh_no_bwtar(MATNR,
                           BWKEY,
                           LFGJA,
                           LFMON,
                           VPRSV,
                           VERPR,
                           STPRS,
                           PEINH,
                           SALK3,
                           BWTAR,
                           MBEWH_LBKUM)
   SELECT distinct MATNR,
          BWKEY,
          LFGJA,
          LFMON,
          VPRSV,
          VERPR,
          STPRS,
          PEINH,
          SALK3,
          BWTAR,
          MBEWH_LBKUM
     FROM mbewh
     WHERE ifnull(BWTAR,'null') <> 'null';



/* Madalina 14 Jul 2016 - Fields from MARA shouldn't be updated from MARA_MARC_MAKT- use MARA for updates, without WERKS as join condition. The updates are done after all the inserts in dim_part - BI-3461 */ 	  
UPDATE dim_part dp 
   SET Revision = ifnull(MARA_KZREV, 'Not Set'),	
	   	dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
	   and Revision <> ifnull(MARA_KZREV, 'Not Set'); 
	   
UPDATE dim_part dp 
   SET UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),	
	   	dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
	   and UnitOfMeasure <> ifnull(MARA_MEINS, 'Not Set'); 

UPDATE dim_part dp     
   SET PartType = ifnull(MARA_MTART, 'Not Set'),
	   	   	dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
	   and PartType <> ifnull(MARA_MTART, 'Not Set'); 

UPDATE dim_part dp 
   SET   MaterialGroup = ifnull(mara_matkl, 'Not Set'),  	 
	dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and MaterialGroup <> ifnull(mara_matkl, 'Not Set');	   
	   
UPDATE dim_part dp  
   SET  TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),	   
     dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and TransportationGroup <> ifnull(MARA_TRAGR, 'Not Set');
	
UPDATE dim_part dp  
   SET  Division = ifnull(MARA_SPART, 'Not Set'),
     dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and Division <> ifnull(MARA_SPART, 'Not Set');
	   
UPDATE dim_part dp
   SET  DivisionDescription = ifnull(t.TSPAT_VTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR
		left join TSPAT t on t.TSPAT_SPART = mck.MARA_SPART AND t.TSPAT_SPRAS = 'E'
WHERE DivisionDescription <> ifnull(t.TSPAT_VTEXT,'Not Set');
   				 
UPDATE dim_part dp 
   SET  GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
   			   dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and  GeneralItemCategory <> ifnull(MARA_MTPOS, 'Not Set');  

UPDATE dim_part dp 
   SET ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
   dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and ProductHierarchy <> ifnull(MARA_PRDHA, 'Not Set'); 
	   
UPDATE dim_part dp 
   SET MPN = ifnull(MARA_MFRPN, 'Not Set'),
    dp.dw_update_date = current_timestamp
    from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and MPN <> ifnull(MARA_MFRPN, 'Not Set');


UPDATE dim_part dp
   SET  ProductHierarchyDescription = ifnull(t.VTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR 
		left join t179t t on t.PRODH = mck.MARA_PRDHA
WHERE ProductHierarchyDescription <> ifnull(t.VTEXT,'Not Set');

UPDATE dim_part dp
   SET  ProductHierarchyDescription = ifnull(t.VTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR 
		left join t179t t on t.PRODH = mck.MARA_PRDHA
WHERE ProductHierarchyDescription <> ifnull(t.VTEXT,'Not Set');

UPDATE dim_part dp
   SET  ProductHierarchyDescription = ifnull(t.VTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR 
		left join t179t t on t.PRODH = mck.MARA_PRDHA
WHERE ProductHierarchyDescription <> ifnull(t.VTEXT,'Not Set');

UPDATE dim_part dp
   SET  ProductHierarchyDescription = ifnull(t.VTEXT,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR 
		left join t179t t on t.PRODH = mck.MARA_PRDHA
WHERE ProductHierarchyDescription <> ifnull(t.VTEXT,'Not Set');


UPDATE dim_part dp
   SET  PartTypeDescription = ifnull(t.T134T_MTBEZ,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR 
		left join T134T t on t.T134T_MTART = mck.MARA_MTART
WHERE PartTypeDescription <> ifnull(t.T134T_MTBEZ,'Not Set');
				 

UPDATE dim_part dp
   SET  MaterialGroupDescription = ifnull(t.T023T_WGBEZ,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR 
		left join T023T t on t.T023T_MATKL = mck.MARA_MATKL
WHERE MaterialGroupDescription <> ifnull(t.T023T_WGBEZ,'Not Set');

/* Liviu Ionescu - unstable set of rows */
/* MARC_MMSTA removed from selectionas it can't be in MARA */
drop table if exists tmp_upd_materialstatusdescription;
create table tmp_upd_materialstatusdescription as
select distinct t.t141t_MTSTB,mck.MARA_MSTAE,mck.MARA_MATNR, dp.dim_partid
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR 
		left join t141t t on t.T141T_MMSTA = mck.MARA_MSTAE;

/*
UPDATE dim_part dp
   SET  materialstatusdescription = ifnull(t.t141t_MTSTB,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR 
		left join t141t t on t.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)
WHERE materialstatusdescription <> ifnull(t.t141t_MTSTB,'Not Set')
*/

UPDATE dim_part dp
   SET  materialstatusdescription = ifnull(t.t141t_MTSTB,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp, tmp_upd_materialstatusdescription t
WHERE 	dp.dim_partid = t.dim_partid
		and materialstatusdescription <> ifnull(t.t141t_MTSTB,'Not Set');

drop table if exists tmp_upd_materialstatusdescription;


UPDATE dim_part dp 
   SET dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
   dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and dp.ExternalMaterialGroupCode <> ifnull(MARA_EXTWG, 'Not Set'); 
	   				 

UPDATE dim_part dp
   SET  ExternalMaterialGroupDescription = ifnull(t.TWEWT_EWBEZ,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join MARA mck on dp.PartNumber = mck.MARA_MATNR 
		left join twewt t on t.TWEWT_EXTWG = mck.MARA_EXTWG
WHERE ExternalMaterialGroupDescription <> ifnull(t.TWEWT_EWBEZ,'Not Set');	

UPDATE dim_part dp 
   SET dp.OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
    dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and dp.OldPartNumber <> ifnull(MARA_BISMT, 'Not Set');


UPDATE dim_part dp
   SET  SDMaterialStatusDescription = ifnull(t.TVMST_VMSTB,'Not Set'),
	dp.dw_update_date = current_timestamp
from dim_part dp
		inner join mara mck on dp.PartNumber = mck.MARA_MATNR
		left join tvmst t on t.TVMST_VMSTA = MARA_MSTAV
WHERE SDMaterialStatusDescription <> ifnull(t.TVMST_VMSTB,'Not Set');

UPDATE dim_part dp  
   SET dp.Volume = MARA_VOLUM,	
	dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
and dp.Volume <> MARA_VOLUM;

UPDATE dim_part dp 
   SET dp.ManfacturerNumber = ifnull(mck.MARA_MFRNR,'Not Set'),
    dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and dp.ManfacturerNumber <> ifnull(mck.MARA_MFRNR,'Not Set'); 
	   
UPDATE dim_part dpt 
 SET dpt.ExtManfacturerCode = dv.VendorMfgCode,
     dpt.ExtManfacturerDesc = dv.VendorName,
			dpt.dw_update_date = current_timestamp
 from 
 dim_Vendor dv,  dim_part dpt 
WHERE dpt.ManfacturerNumber = dv.VendorNumber;
	      
UPDATE dim_part dp 
   SET dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
      dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and dp.UPCCode <> ifnull(mck.MARA_EAN11,'Not Set');

UPDATE dim_part dp 
   SET dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
    dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	  and dp.NDCCode <> ifnull(mck.MARA_GROES,'Not Set');

UPDATE dim_part dp 
   SET dp.storageconditioncode = ifnull(mck.MARA_RAUBE,'Not Set'),
	   dp.hazardousmaterialnumber = ifnull(mck.MARA_STOFF,'Not Set'),
	   	dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR;
 
update  dim_part pt 
set storageconditiondescription = ifnull(rbtxt,'Not Set')
from t142t t, dim_part pt 
where storageconditioncode = raube
and storageconditioncode <> ifnull(rbtxt,'Not Set');

/* Yogini APP-8849 29 May 2018 */
UPDATE dim_part dp
   SET dp.IndustryStandardDescription = ifnull(mck.MARA_NORMT,'Not Set'),
      dp.dw_update_date = current_timestamp
from MARA mck,dim_part dp
	   WHERE     dp.PartNumber = mck.MARA_MATNR
	   and dp.IndustryStandardDescription <> ifnull(mck.MARA_NORMT,'Not Set');
/* END 14 Jun 2016 */
	   
	   
UPDATE dim_part dp
SET overdelivery_tolerance = ifnull(MARC_UEETO,0),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MAKT mck, dim_plant p,dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
AND dp.overdelivery_tolerance <> ifnull(MARC_UEETO,0);

UPDATE dim_part dp
SET underdelivery_tolerance = ifnull(MARC_UNETO,0),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MAKT mck, dim_plant p,dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
AND dp.underdelivery_tolerance <> ifnull(MARC_UNETO,0);


UPDATE dim_part dp
SET unlimited_overdelivery_flag = ifnull(MARC_UEETK,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MAKT mck, dim_plant p,dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
AND unlimited_overdelivery_flag <> ifnull(MARC_UEETK,'Not Set');

UPDATE dim_part dp
SET dependentreqind = ifnull(marc_sbdkz,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MAKT mck, dim_plant p, dim_part dp
WHERE dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
AND dependentreqind <> ifnull(marc_sbdkz,'Not Set');


/* Octavian: Every Angle Transition Addons */

update dim_part
set ProductionVersionToBeCosted = ifnull(MARC_FVIDK,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and ProductionVersionToBeCosted <> ifnull(MARC_FVIDK,'Not Set');


update dim_part
set prodvers_validform = ifnull(MKAL_ADATU,'0001-01-01'),
dw_update_date = current_timestamp
from MARA_MARC_MKAL, dim_part
where partnumber = ifnull(MKAL_MATNR,'Not Set')
and plant = ifnull(MKAL_WERKS,'Not Set') and ProductionVersionToBeCosted = ifnull(MKAL_VERID,'Not Set')
and prodvers_validform <> ifnull(MKAL_ADATU,'0001-01-01');

update dim_part
set prodvers_validto = ifnull(MKAL_BDATU,'0001-01-01'),
dw_update_date = current_timestamp
from MARA_MARC_MKAL, dim_part
where partnumber = ifnull(MKAL_MATNR,'Not Set')
and plant = ifnull(MKAL_WERKS,'Not Set') and ProductionVersionToBeCosted = ifnull(MKAL_VERID,'Not Set')
and prodvers_validto <> ifnull(MKAL_BDATU,'0001-01-01');

update dim_part
set prodvers_group = ifnull(MKAL_PLNNR,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MKAL, dim_part
where partnumber = ifnull(MKAL_MATNR,'Not Set')
and plant = ifnull(MKAL_WERKS,'Not Set') and ProductionVersionToBeCosted = ifnull(MKAL_VERID,'Not Set')
and prodvers_group <> ifnull(MKAL_PLNNR,'Not Set');

update dim_part
set prodvers_dateoflastcheck = ifnull(MKAL_PRDAT,'0001-01-01'),
dw_update_date = current_timestamp
from MARA_MARC_MKAL, dim_part
where partnumber = ifnull(MKAL_MATNR,'Not Set')
and plant = ifnull(MKAL_WERKS,'Not Set') and ProductionVersionToBeCosted = ifnull(MKAL_VERID,'Not Set')
and prodvers_dateoflastcheck <> ifnull(MKAL_PRDAT,'0001-01-01');

update dim_part
set prodvers_statusbom = ifnull(MKAL_PRFG_S,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MKAL, dim_part
where partnumber = ifnull(MKAL_MATNR,'Not Set')
and plant = ifnull(MKAL_WERKS,'Not Set') and ProductionVersionToBeCosted = ifnull(MKAL_VERID,'Not Set')
and prodvers_statusbom <> ifnull(MKAL_PRFG_S,'Not Set');

update dim_part
set prodvers_altbom = ifnull(MKAL_STLAL,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MKAL, dim_part
where partnumber = ifnull(MKAL_MATNR,'Not Set')
and plant = ifnull(MKAL_WERKS,'Not Set') and ProductionVersionToBeCosted = ifnull(MKAL_VERID,'Not Set')
and prodvers_altbom <> ifnull(MKAL_STLAL,'Not Set');

update dim_part
set prodvers_prodversion = ifnull(MKAL_VERID,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MKAL, dim_part
where partnumber = ifnull(MKAL_MATNR,'Not Set')
and plant = ifnull(MKAL_WERKS,'Not Set') and ProductionVersionToBeCosted = ifnull(MKAL_VERID,'Not Set')
and prodvers_prodversion <> ifnull(MKAL_VERID,'Not Set');

/* Madalina 14 Jul 2016 - Fields from MARA shouldn't be updated from MARA_MARC_MAKT- use MARA for updates, without WERKS as join condition. The updates are done after all the inserts in dim_part - BI-3461 */  


/*update dim_part
set totalshelflife = ifnull(MARA_MHDHB,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and totalshelflife <> ifnull(MARA_MHDHB,0)

update dim_part
set minremainshelflife = ifnull(MARA_MHDRZ,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and minremainshelflife <> ifnull(MARA_MHDRZ,0)

update dim_part
set netweight = ifnull(MARA_NTGEW,0.000),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and netweight <> ifnull(MARA_NTGEW,0.000)

update dim_part
set maintenancestatus = ifnull(MARA_PSTAT,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and maintenancestatus <> ifnull(MARA_PSTAT,'Not Set')

update dim_part
set qminprocureactive = ifnull(MARA_QMPUR,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and qminprocureactive <> ifnull(MARA_QMPUR,'Not Set')

update dim_part
set roundingruleSLED = ifnull(MARA_RDMHD,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and roundingruleSLED <> ifnull(MARA_RDMHD,'Not Set')

update dim_part
set basicmaterial = ifnull(MARA_WRKST,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and basicmaterial <> ifnull(MARA_WRKST,'Not Set')*/


update dim_part
set totalshelflife = ifnull(MARA_MHDHB,0),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and totalshelflife <> ifnull(MARA_MHDHB,0);

update dim_part
set minremainshelflife = ifnull(MARA_MHDRZ,0),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and minremainshelflife <> ifnull(MARA_MHDRZ,0);

update dim_part
set netweight = ifnull(MARA_NTGEW,0.000),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and netweight <> ifnull(MARA_NTGEW,0.000);

update dim_part
set maintenancestatus = ifnull(MARA_PSTAT,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and maintenancestatus <> ifnull(MARA_PSTAT,'Not Set');

update dim_part
set qminprocureactive = ifnull(MARA_QMPUR,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and qminprocureactive <> ifnull(MARA_QMPUR,'Not Set');

update dim_part
set roundingruleSLED = ifnull(MARA_RDMHD,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and roundingruleSLED <> ifnull(MARA_RDMHD,'Not Set');

update dim_part
set basicmaterial = ifnull(MARA_WRKST,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and basicmaterial <> ifnull(MARA_WRKST,'Not Set');

/* END 14 Jul 2016 */

update dim_part
set variancekey = ifnull(MARC_AWSLS,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and variancekey <> ifnull(MARC_AWSLS,'Not Set');

update dim_part
set variancekey = ifnull(MARC_AWSLS,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and variancekey <> ifnull(MARC_AWSLS,'Not Set');

update dim_part
set prod_supervisor = ifnull(MARC_FEVOR,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and prod_supervisor <> ifnull(MARC_FEVOR,'Not Set');

update dim_part
set country_of_origin = ifnull(MARC_HERKL,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and country_of_origin <> ifnull(MARC_HERKL,'Not Set');

update dim_part
set lotsizeprodcosting = ifnull(MARC_LOSGR,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and lotsizeprodcosting <> ifnull(MARC_LOSGR,0);

/*start alin 21.12.2016 - BI-4542*/
update dim_part
set prodvers_group = ifnull(MARC_PLNNR,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and prodvers_group <> ifnull(MARC_PLNNR,'Not Set');

update dim_part
set groupcounter = ifnull(MARC_APLAL,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and groupcounter <> ifnull(MARC_APLAL,'Not Set');
/*end alin 21.12.2016 - BI-4542*/

update dim_part
set lotsizeprodcosting = ifnull(MARC_LOSGR,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT_SPRAS, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and lotsizeprodcosting = 0
and lotsizeprodcosting <> ifnull(MARC_LOSGR,0);

/* Madalina 16 Aug 2016 - new field for maintenancestatus retrived from MARC */
update dim_part
/* set maintenancestatus = ifnull(MARC_PSTAT,'Not Set'), */
set maintenancestatus_marc = ifnull(MARC_PSTAT,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
/* and maintenancestatus <> ifnull(MARC_PSTAT,'Not Set') */
and maintenancestatus_marc <> ifnull(MARC_PSTAT,'Not Set');
/* End 16 Aug 2016 */

update dim_part
set qmmatauthorisation = ifnull(MARC_QMATA,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and qmmatauthorisation <> ifnull(MARC_QMATA,'Not Set');

update dim_part
set inspsetup = ifnull(MARC_QMATV,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and inspsetup <> ifnull(MARC_QMATV,'Not Set');

update dim_part
set prodschprof = ifnull(MARC_SFCPF,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and prodschprof <> ifnull(MARC_SFCPF,'Not Set');

update dim_part
set qmcontrolkey = ifnull(MARC_SSQSS,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and qmcontrolkey <> ifnull(MARC_SSQSS,'Not Set');

update dim_part
set versindicator = ifnull(MARC_VERKZ,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and versindicator <> ifnull(MARC_VERKZ,'Not Set');

update dim_part
set batchmgmtind = ifnull(MARC_XCHAR,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and batchmgmtind <> ifnull(MARC_XCHAR,'Not Set');

/* Madalina 14 Jul 2016 - Fields from MARA shouldn't be updated from MARA_MARC_MAKT- use MARA for updates, without WERKS as join condition. The updates are done after all the inserts in dim_part - BI-3461 */ 
/*update dim_part
set crossmatplantsts = ifnull(MARA_MSTAE,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and crossmatplantsts <> ifnull(MARA_MSTAE,'Not Set')

update dim_part
set delflagatclientlevel = ifnull(MARA_LVORM,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and delflagatclientlevel <> ifnull(MARA_LVORM,'Not Set')

update dim_part
set periodindforSLED = ifnull(MARA_IPRKZ,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and periodindforSLED <> ifnull(MARA_IPRKZ,'Not Set')

update dim_part
set purchasevaluekey = ifnull(MARA_EKWSL,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and purchasevaluekey <> ifnull(MARA_EKWSL,'Not Set')

update dim_part
set grossweight = ifnull(MARA_BRGEW,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and grossweight <> ifnull(MARA_BRGEW,0)

update dim_part
set orderunit = ifnull(MARA_BSTME,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and orderunit <> ifnull(MARA_BSTME,'Not Set')

update dim_part
set creationdate = ifnull(MARA_ERSDA,'0001-01-01'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and creationdate <> ifnull(MARA_ERSDA,'0001-01-01')

update dim_part
set weightunit = ifnull(MARA_GEWEI,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and weightunit <> ifnull(MARA_GEWEI,'Not Set')


update dim_part
set changedate = ifnull(MARA_LAEDA,'0001-01-01'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and changedate <> ifnull(MARA_LAEDA,'0001-01-01')*/
update dim_part 
set crossmatplantsts = ifnull(MARA_MSTAE,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and crossmatplantsts <> ifnull(MARA_MSTAE,'Not Set');

update dim_part
set delflagatclientlevel = ifnull(MARA_LVORM,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and delflagatclientlevel <> ifnull(MARA_LVORM,'Not Set');

update dim_part
set periodindforSLED = ifnull(MARA_IPRKZ,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and periodindforSLED <> ifnull(MARA_IPRKZ,'Not Set');

update dim_part
set purchasevaluekey = ifnull(MARA_EKWSL,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and purchasevaluekey <> ifnull(MARA_EKWSL,'Not Set');

update dim_part
set grossweight = ifnull(MARA_BRGEW,0),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and grossweight <> ifnull(MARA_BRGEW,0);

update dim_part
set orderunit = ifnull(MARA_BSTME,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and orderunit <> ifnull(MARA_BSTME,'Not Set');

update dim_part
set creationdate = ifnull(MARA_ERSDA,'0001-01-01'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and creationdate <> ifnull(MARA_ERSDA,'0001-01-01');

update dim_part
set weightunit = ifnull(MARA_GEWEI,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and weightunit <> ifnull(MARA_GEWEI,'Not Set');


update dim_part
set changedate = ifnull(MARA_LAEDA,'0001-01-01'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and changedate <> ifnull(MARA_LAEDA,'0001-01-01');
/* END 14 Jul 2016 */
/*Georgiana Every Angle Transition Addons */
/* Madalina 14 Jul 2016 - Fields from MARA shouldn't be updated from MARA_MARC_MAKT- use MARA for updates, without WERKS as join condition. The updates are done after all the inserts in dim_part - BI-3461 */ 
/*update dim_part
set Xdistrchainvalidfromdate = ifnull(MARA_MSTDE,'0001-01-01'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and Xdistrchainvalidfromdate <> ifnull(MARA_MSTDE,'0001-01-01')

update dim_part
set expirationdate = ifnull(MARA_SLED_BBD,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and expirationdate <> ifnull(MARA_SLED_BBD,'Not Set')

update dim_part
set completeMaintenancestatus = ifnull(MARA_VPSTA,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and completeMaintenancestatus <> ifnull(MARA_VPSTA,'Not Set')

update dim_part
set numberofslips = ifnull(MARA_WESCH,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and numberofslips <> ifnull(MARA_WESCH,0)*/

update dim_part 
set Xdistrchainvalidfromdate = ifnull(MARA_MSTDE,'0001-01-01'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and Xdistrchainvalidfromdate <> ifnull(MARA_MSTDE,'0001-01-01');

update dim_part
set expirationdate = ifnull(MARA_SLED_BBD,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and expirationdate <> ifnull(MARA_SLED_BBD,'Not Set');

update dim_part
set completeMaintenancestatus = ifnull(MARA_VPSTA,'Not Set'),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and completeMaintenancestatus <> ifnull(MARA_VPSTA,'Not Set');

update dim_part
set numberofslips = ifnull(MARA_WESCH,0),
dw_update_date = current_timestamp
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and numberofslips <> ifnull(MARA_WESCH,0);
/* END 14 Jul 2016 */

update dim_part
set selectingmethod = ifnull(MARC_ALTSL,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and selectingmethod <> ifnull(MARC_ALTSL,'Not Set');


update dim_part
set unitofissue = ifnull(MARC_AUSME,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and unitofissue <> ifnull(MARC_AUSME,'Not Set');


update dim_part
set assemblyscrap = ifnull(MARC_AUSSS,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and assemblyscrap <> ifnull(MARC_AUSSS,0);

update dim_part
set minimumsafetystock = ifnull(MARC_EISLO,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and minimumsafetystock <> ifnull(MARC_EISLO,0);

update dim_part
set productionunit = ifnull(MARC_FRTME,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and productionunit <> ifnull(MARC_FRTME,'Not Set');

update dim_part
set posttoinspstock = ifnull(MARC_INSMK,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and posttoinspstock <> ifnull(MARC_INSMK,'Not Set');

update dim_part
set Documentationrequiredindicator = ifnull(MARC_KZDKZ,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and Documentationrequiredindicator <> ifnull(MARC_KZDKZ,'Not Set');

update dim_part
set FiscalYearVariant = ifnull(MARC_PERIV,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and FiscalYearVariant <> ifnull(MARC_PERIV,'Not Set');

update dim_part
set periodindicator_marc = ifnull(MARC_PERKZ,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and periodindicator_marc <> ifnull(MARC_PERKZ,'Not Set');

update dim_part
set indlegalcontrol = ifnull(MARC_PRENC,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and indlegalcontrol <> ifnull(MARC_PRENC,'Not Set');

update dim_part
set exemptioncertificateissuedate = ifnull(MARC_PREND,'0001-01-01'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and exemptioncertificateissuedate <> ifnull(MARC_PREND,'0001-01-01');

update dim_part
set numberlegalcontrol = ifnull(MARC_PRENO,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and numberlegalcontrol <> ifnull(MARC_PRENO,'Not Set');

update dim_part
set intuntilnextrecinsp = ifnull(MARC_PRFRQ,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and intuntilnextrecinsp <> ifnull(MARC_PRFRQ,0);

update dim_part
set certificatetype = ifnull(MARC_QZGTP,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and certificatetype <> ifnull(MARC_QZGTP,'Not Set');

update dim_part
set backflushindicator = ifnull(MARC_RGEKZ,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and backflushindicator <> ifnull(MARC_RGEKZ,'Not Set');

update dim_part
set ProcurementTypeCosting = ifnull(MARC_SOBSK,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and ProcurementTypeCosting <> ifnull(MARC_SOBSK,'Not Set');


/* Liviu Ionescu - unstable set of rows */
drop table if exists tmp_upd_taxclassifmaterial;
create table tmp_upd_taxclassifmaterial as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXIM,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial = ifnull(MLAN_TAXIM,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial <> ifnull(MLAN_TAXIM,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial = ifnull(MLAN_TAXIM,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial <> ifnull(MLAN_TAXIM,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial;


drop table if exists tmp_upd_taxclassifmaterial1;
create table tmp_upd_taxclassifmaterial1 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM1,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial1 = ifnull(MLAN_TAXM1,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial1 <> ifnull(MLAN_TAXM1,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial1 = ifnull(MLAN_TAXM1,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial1, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial1 <> ifnull(MLAN_TAXM1,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial1;

/* Liviu Ionescu - unstable set of rows */
drop table if exists tmp_upd_taxclassifmaterial2;
create table tmp_upd_taxclassifmaterial2 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM2,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial2 = ifnull(MLAN_TAXM2,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial2 <> ifnull(MLAN_TAXM2,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial2 = ifnull(MLAN_TAXM2,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial2, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial2 <> ifnull(MLAN_TAXM2,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial2;

drop table if exists tmp_upd_taxclassifmaterial3;
create table tmp_upd_taxclassifmaterial3 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM3,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial3 = ifnull(MLAN_TAXM3,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial3 <> ifnull(MLAN_TAXM3,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial3 = ifnull(MLAN_TAXM3,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial3, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial3 <> ifnull(MLAN_TAXM3,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial3;

drop table if exists tmp_upd_taxclassifmaterial4;
create table tmp_upd_taxclassifmaterial4 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM4,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial4 = ifnull(MLAN_TAXM4,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial4 <> ifnull(MLAN_TAXM4,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial4 = ifnull(MLAN_TAXM4,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial4, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial4 <> ifnull(MLAN_TAXM4,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial4;

drop table if exists tmp_upd_taxclassifmaterial5;
create table tmp_upd_taxclassifmaterial5 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM5,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial5 = ifnull(MLAN_TAXM5,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial5 <> ifnull(MLAN_TAXM5,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial5 = ifnull(MLAN_TAXM5,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial5, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial5 <> ifnull(MLAN_TAXM5,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial5;


drop table if exists tmp_upd_taxclassifmaterial6;
create table tmp_upd_taxclassifmaterial6 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM6,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial6 = ifnull(MLAN_TAXM6,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial6 <> ifnull(MLAN_TAXM6,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial6 = ifnull(MLAN_TAXM6,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial6, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial6 <> ifnull(MLAN_TAXM6,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial6;

drop table if exists tmp_upd_taxclassifmaterial7;
create table tmp_upd_taxclassifmaterial7 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM7,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial7 = ifnull(MLAN_TAXM7,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial7 <> ifnull(MLAN_TAXM7,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial7 = ifnull(MLAN_TAXM7,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial7, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial7 <> ifnull(MLAN_TAXM7,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial7;

drop table if exists tmp_upd_taxclassifmaterial8;
create table tmp_upd_taxclassifmaterial8 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM8,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial8 = ifnull(MLAN_TAXM8,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial8 <> ifnull(MLAN_TAXM8,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial8 = ifnull(MLAN_TAXM8,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial8, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial8 <> ifnull(MLAN_TAXM8,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial8;

drop table if exists tmp_upd_taxclassifmaterial9;
create table tmp_upd_taxclassifmaterial9 as
select distinct MLAN_MATNR, MLAN_ALAND, MLAN_TAXM9,
	row_number() over (partition by MLAN_MATNR, MLAN_ALAND  order by '') as rownumber
from mara_marc_mlan;

/*
UPDATE dim_part
SET taxclassifmaterial9 = ifnull(MLAN_TAXM9,'Not Set'),
dw_update_date = current_timestamp
FROM mara_marc_mlan, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND  taxclassifmaterial9 <> ifnull(MLAN_TAXM9,'Not Set')
*/

UPDATE dim_part
SET taxclassifmaterial9 = ifnull(MLAN_TAXM9,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_upd_taxclassifmaterial9, dim_part
WHERE partnumber = ifnull(MLAN_MATNR,'Not Set')
AND country_of_origin = ifnull(MLAN_ALAND, 'Not Set')
AND rownumber = 1
AND  taxclassifmaterial9 <> ifnull(MLAN_TAXM9,'Not Set');

drop table if exists tmp_upd_taxclassifmaterial9;

update dim_part dp
set dp.Width = ifnull(MARM_BREIT,0),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.Width <> ifnull(MARM_BREIT,0);

update dim_part dp
set dp.intarticleno = ifnull(MARM_EAN11, 'Not Set'),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.intarticleno <> ifnull(MARM_EAN11, 'Not Set');

update dim_part dp
set dp.WeightUnit_marm = ifnull(MARM_GEWEI, 'Not Set'),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.WeightUnit_marm <> ifnull(MARM_GEWEI, 'Not Set');

update dim_part dp
set dp.Height = ifnull(MARM_HOEHE, 0),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.Height <> ifnull(MARM_HOEHE, 0);

update dim_part dp
set dp.unitofmeasureusage = ifnull(MARM_KZWSO, 'Not Set'),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.unitofmeasureusage <> ifnull(MARM_KZWSO, 'Not Set');

update dim_part dp
set dp.Length = ifnull(MARM_LAENG, 'Not Set'),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.Length <> ifnull(MARM_LAENG, 'Not Set');


update dim_part dp
set dp.unitofdimensionlwh = ifnull(MARM_MEABM, 'Not Set'),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.unitofdimensionlwh <> ifnull(MARM_MEABM, 'Not Set');


update dim_part dp
set dp.denominator_marm = ifnull(MARM_UMREN, 0),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.denominator_marm <> ifnull(MARM_UMREN, 0);

/* Liviu Ionescu - Add IRU UOM - 3637*/
update dim_part dp
set dp.UoMIRU = ifnull(m.MARM_UMREZ, 0),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where 'IRU' = ifnull(m.MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(m.MARM_MATNR, 'Not Set')
and  dp.UoMIRU <> ifnull(m.MARM_UMREZ, 0);
/* Liviu Ionescu */

/* alin gheorghe - Add PK UOM and  CTN UOM - 3637*/
update dim_part dp
set dp.UOMPK = ifnull(m.MARM_UMREZ, 0),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where 'PK' = ifnull(m.MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(m.MARM_MATNR, 'Not Set')
and  dp.UOMPK <> ifnull(m.MARM_UMREZ, 0);

update dim_part dp
set dp.UOMCTN = ifnull(m.MARM_UMREZ, 0),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where 'CTN' = ifnull(m.MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(m.MARM_MATNR, 'Not Set')
and  dp.UOMCTN <> ifnull(m.MARM_UMREZ, 0);
/* alin gheorghe */

update dim_part dp
set dp.numerator_marm = ifnull(MARM_UMREZ, 0),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.numerator_marm <> ifnull(MARM_UMREZ, 0);

update dim_part dp
set dp.volumeunit = ifnull(MARM_VOLEH, 'Not Set'),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.volumeunit <> ifnull(MARM_VOLEH, 'Not Set');

update dim_part dp
set dp.volume_marm = ifnull(MARM_VOLUM, 0),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and  dp.volume_marm <> ifnull(MARM_VOLUM, 0);

/*Georgiana End of Changes*/


drop table if exists tmp_mbew_update_part;
create table tmp_mbew_update_part AS
select DISTINCT * from MARA_MARC_MBEW
WHERE MBEW_BWTAR is null AND
MARC_WERKS = MBEW_BWKEY;

update dim_part
set valuationcategory = ifnull(MBEW_BWTTY,'Not Set'),
dw_update_date = current_timestamp
from tmp_mbew_update_part,dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and valuationcategory <> ifnull(MBEW_BWTTY,'Not Set');

update dim_part
set origingroup_mbew = ifnull(MBEW_HRKFT,'Not Set'),
dw_update_date = current_timestamp
from tmp_mbew_update_part,dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and origingroup_mbew <> ifnull(MBEW_HRKFT,'Not Set');

update dim_part
set overheadgroup = ifnull(MBEW_KOSGR,'Not Set'),
dw_update_date = current_timestamp
from tmp_mbew_update_part,dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and overheadgroup <> ifnull(MBEW_KOSGR,'Not Set');

update dim_part
set pricecontrol = ifnull(MBEW_VPRSV,'Not Set'),
dw_update_date = current_timestamp
from tmp_mbew_update_part,dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and pricecontrol <> ifnull(MBEW_VPRSV,'Not Set');

update dim_part
set validfrom = ifnull(MBEW_ZKDAT,'0001-01-01'),
dw_update_date = current_timestamp
from (select distinct mbew_matnr,mbew_bwkey,MBEW_ZKDAT from tmp_mbew_update_part),dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and validfrom <> ifnull(MBEW_ZKDAT,'0001-01-01');

update dim_part
set valuationclass = ifnull(MBEW_BKLAS,'Not Set'),
dw_update_date = current_timestamp
from tmp_mbew_update_part,dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and valuationclass <> ifnull(MBEW_BKLAS,'Not Set');

update dim_part
set matiscostedwqtstruct = ifnull(MBEW_EKALR,'Not Set'),
dw_update_date = current_timestamp
from tmp_mbew_update_part,dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and matiscostedwqtstruct <> ifnull(MBEW_EKALR,'Not Set');

update dim_part
set matrelorigin = ifnull(MBEW_HKMAT,'Not Set'),
dw_update_date = current_timestamp
from tmp_mbew_update_part,dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and matrelorigin <> ifnull(MBEW_HKMAT,'Not Set');

update dim_part
set fiscalyearofcurrentstandard = ifnull(MBEW_PDATL,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part,dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and fiscalyearofcurrentstandard <> ifnull(MBEW_PDATL,0);

/*Georgiana EA Changes -creating a temporary table for Prices*/
/*26 MAy 2016 Georgiana EA changes : changing the logic of this temp table in order to take the values only when mbew_bwtar is null according to BI-3018*/
drop table if exists tmp_mbew_update_part_prices;
create table tmp_mbew_update_part_prices AS
select DISTINCT mbew_matnr,mbew_bwkey,MBEW_STPRS,MBEW_VERPR,MBEW_LBKUM,MBEW_LPLPR,MBEW_BWPEI,MBEW_BWPH1,MBEW_BWPRH,MBEW_PEINH,MBEW_SALK3,MBEW_VJBWH,MBEW_VPLPR,MBEW_ZKPRS,MBEW_STPRV
from MARA_MARC_MBEW
WHERE
MARC_WERKS = MBEW_BWKEY
and mbew_bwtar is null;
--and upd_flag_tcurx is null

update dim_part
set standardprice_mbew = ifnull(MBEW_STPRS,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and standardprice_mbew <> ifnull(MBEW_STPRS,0);

update dim_part
set movingprice = ifnull(MBEW_VERPR,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and movingprice <> ifnull(MBEW_VERPR,0);

update dim_part
set totalvaluatedstock = ifnull(MBEW_LBKUM,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and totalvaluatedstock <> ifnull(MBEW_LBKUM,0);

update dim_part
set currentplanprice = ifnull(MBEW_LPLPR,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and currentplanprice <> ifnull(MBEW_LPLPR,0);

update dim_part
set priceunitvalpricestclaw = ifnull(MBEW_BWPEI,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and priceunitvalpricestclaw <> ifnull(MBEW_BWPEI,0);

update dim_part
set valpricebasedoncommlaw2 = ifnull(MBEW_BWPH1,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and valpricebasedoncommlaw2 <> ifnull(MBEW_BWPH1,0);

update dim_part
set valpricebasedoncommlaw1 = ifnull(MBEW_BWPRH,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and valpricebasedoncommlaw1 <> ifnull(MBEW_BWPRH,0);

update dim_part
set priceunit = ifnull(MBEW_PEINH,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and priceunit <> ifnull(MBEW_PEINH,0);

update dim_part
set valueofvaluatedstock = ifnull(MBEW_SALK3,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and valueofvaluatedstock <> ifnull(MBEW_SALK3,0);

update dim_part
set valpricebasedoncommlaw3 = ifnull(MBEW_VJBWH,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and valpricebasedoncommlaw3 <> ifnull(MBEW_VJBWH,0);

update dim_part
set prevplannedprice = ifnull(MBEW_VPLPR,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and prevplannedprice <> ifnull(MBEW_VPLPR,0);

update dim_part
set futureprice = ifnull(MBEW_ZKPRS,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and futureprice <> ifnull(MBEW_ZKPRS,0);

update dim_part
set previousprice = ifnull(MBEW_STPRV,0),
dw_update_date = current_timestamp
from tmp_mbew_update_part_prices, dim_part
where ifnull(mbew_matnr,'Not Set') = partnumber
and ifnull(mbew_bwkey,'Not Set') = plant
and previousprice <> ifnull(MBEW_STPRV,0);


drop table if exists tmp_mbew_update_part_prices;    /*Georgiana End of EA changes*/
drop table if exists tmp_mbew_update_part;

/* Madalina 26 Jul 2016 - replace link between plant and WERKS with WEMIT - BI-3557 */ 
/*Georgiana 23 Nov 2016 reset productgroup_pgmi to default value before updating it - and commented the merge statement as is not correct*/
update dim_part
set productgroup_pgmi='Not Set'
where productgroup_pgmi <> 'Not Set';

/*merge into dim_part d
using (
select distinct x.dim_partid, FIRST_VALUE(productgroup_pgmi) OVER (PARTITION BY PGMI_NRMIT,PGMI_WERKS ORDER BY PGMI_DATUM desc) AS productgroup_pgmi
FROM dim_part x INNER JOIN PGMI p
ON x.partnumber = p.PGMI_NRMIT and x.plant = ifnull(PGMI_WEMIT, 'Not set')
where productgroup_pgmi <> ifnull(PGMI_PRGRP,'Not Set')) src
ON d.dim_partid = src.dim_partid
WHEN MATCHED THEN UPDATE
SET d.productgroup_pgmi = src.productgroup_pgmi*/

/* Liviu Ionescu - unstable set of rows */
drop table if exists tmp_upd_productgroup_pgmi;
create table tmp_upd_productgroup_pgmi as
select distinct PGMI_PRGRP, PGMI_NRMIT, PGMI_WEMIT,
	row_number() over (partition by PGMI_NRMIT, PGMI_WEMIT order by '') as rownumber
from PGMI;

/*
update dim_part
set productgroup_pgmi = ifnull(PGMI_PRGRP,'Not Set'),
dw_update_date = current_timestamp
from PGMI, dim_part
where partnumber = PGMI_NRMIT
and plant = ifnull(PGMI_WEMIT, 'Not set')
and productgroup_pgmi <> ifnull(PGMI_PRGRP,'Not Set') */

update dim_part
set productgroup_pgmi = ifnull(PGMI_PRGRP,'Not Set'),
dw_update_date = current_timestamp
from tmp_upd_productgroup_pgmi, dim_part
where partnumber = PGMI_NRMIT
and plant = ifnull(PGMI_WEMIT, 'Not set')
and rownumber = 1
and productgroup_pgmi <> ifnull(PGMI_PRGRP,'Not Set');

drop table if exists tmp_upd_productgroup_pgmi;
/* END 26 Jul 2016 */

merge into dim_part dp
using (select distinct dp.dim_partid,w.WBEW_MEINH
from WBEW w, dim_part dp
where w.WBEW_MATNR = dp.partnumber
and w.WBEW_BWKEY = dp.plant
and w.WBEW_BWTAR is null) t on t.dim_partid=dp.dim_partid
when matched then update set 
alternativeuom_stockkeeping = ifnull(t.WBEW_MEINH,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
where alternativeuom_stockkeeping <> ifnull(t.WBEW_MEINH,'Not Set');

merge into dim_part dp
using (select distinct dp.dim_partid,w.WBEW_PEINH
from WBEW w, dim_part dp
where w.WBEW_MATNR = dp.partnumber
and w.WBEW_BWKEY = dp.plant
and w.WBEW_BWTAR is null) t on t.dim_partid=dp.dim_partid
when matched then update set 
priceunit_wbew = ifnull(t.WBEW_PEINH,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
where priceunit_wbew <> ifnull(t.WBEW_PEINH,'Not Set');


merge into dim_part dp
using (select distinct dp.dim_partid,w.WBEW_STPRS
from WBEW w, dim_part dp
where w.WBEW_MATNR = dp.partnumber
and w.WBEW_BWKEY = dp.plant
and w.WBEW_BWTAR is null) t on t.dim_partid=dp.dim_partid
when matched then update set 
stdprice_wbew = ifnull(t.WBEW_STPRS,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
where stdprice_wbew <> ifnull(t.WBEW_STPRS,'Not Set');

merge into dim_part dp
using (select distinct dp.dim_partid,w.WBEW_STPRV
from WBEW w, dim_part dp
where w.WBEW_MATNR = dp.partnumber
and w.WBEW_BWKEY = dp.plant
and w.WBEW_BWTAR is null) t on t.dim_partid=dp.dim_partid
when matched then update set 
prevprice_wbew = ifnull(t.WBEW_STPRV,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
where prevprice_wbew <> ifnull(t.WBEW_STPRV,'Not Set');

/* Octavian: Every Angle Transition Addons */

/* Octavian: Every Angle Transition MGEF */
/* Countries such as USA will be substringed to US only */
drop table if exists MGEF_transformed;
create table MGEF_transformed AS
select MGEF_STOFF,
CASE WHEN length(MGEF_REGKZ)>2 THEN substring(MGEF_REGKZ,1,2) ELSE MGEF_REGKZ END AS mgef_regkz,
MGEF_LAGKL
FROM MGEF;

/* some mapping based on the above stmt */
drop table if exists MGEF_transformed2;
create table MGEF_transformed2 as
select distinct MGEF_STOFF,
	CASE WHEN MGEF_REGKZ = 'UK' THEN 'GB'
            WHEN MGEF_REGKZ = 'D' THEN 'DE'
            ELSE MGEF_REGKZ END AS mgef_regkz,
        MGEF_LAGKL
from MGEF_transformed;

/*UPDATE dim_part dp
SET dp.storageclass = ifnull(m.MGEF_LAGKL,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MGEF_transformed2 m,dim_part dp
WHERE dp.partnumber = m.MGEF_STOFF
and substring(dp.plant,1,2) = m.MGEF_REGKZ
AND dp.storageclass <> ifnull(m.MGEF_LAGKL,'Not Set')*/
/*Unable to get a stable source of rows fix*/

merge into dim_part dp
using (select distinct dim_partid, first_value(m.MGEF_LAGKL) over (partition by dim_partid ) as MGEF_LAGKL
FROM MGEF_transformed2 m,dim_part dp
WHERE dp.partnumber = m.MGEF_STOFF
and substring(dp.plant,1,2) = m.MGEF_REGKZ
AND dp.storageclass <> ifnull(m.MGEF_LAGKL,'Not Set')) t
on t.dim_partid=dp.dim_partid
when matched then update set dp.storageclass = ifnull(t.MGEF_LAGKL,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP;

drop table if exists MGEF_transformed;
drop table if exists MGEF_transformed2;
/* Octavian: Every Angle Transition MGEF */

/*Georgiana EA Changes adding maxstorageperiod*/

UPDATE dim_part dp
   SET maxstorageperiod = ifnull(MARC_MAXLZ, 0),
	   	dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
	   and maxstorageperiod <> ifnull(MARC_MAXLZ, 0);
/*Georgiana End Of Changes*/

/* Madalina 14 Jul 2016 - Fields from MARA shouldn't be updated from MARA_MARC_MAKT- use MARA for updates, without WERKS as join condition. The updates are done after all the inserts in dim_part - BI-3461 */
/*update dim_part
set creator = ifnull(MARA_ERNAM,'Not Set'),
dw_update_date = current_timestamp
from MARA_MARC_MAKT,dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and plant = ifnull(MARC_WERKS,'Not Set')
and creator <> ifnull(MARA_ERNAM,'Not Set')*/

update dim_part
set creator = ifnull(MARA_ERNAM,'Not Set'),
dw_update_date = current_timestamp 
from MARA, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and creator <> ifnull(MARA_ERNAM,'Not Set');
/* END 14 Jul 2016 */

/*Georgiana 26 Apr 2016 Adding Valuation Class Description BI-2636*/

UPDATE dim_part dp
SET dp.valuationclassdescription = ifnull(t.T025T_BKBEZ,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM T025T t, dim_part dp
WHERE dp.valuationclass = ifnull(t.T025T_BKLAS, 'Not Set')
AND dp.valuationclassdescription <> ifnull(t.T025T_BKBEZ,'Not Set');

/*26 Apr 2016 End of Changes*/

/*Georgiana Changes 10 May 2016 Adding intarticlenoorderuom according to BI-2797*/
update dim_part dp
set dp.intarticlenoorderuom = ifnull(MARM_EAN11, 'Not Set')
from MARM m,dim_part dp
where dp.orderunit = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and dp.intarticlenoorderuom <> ifnull(MARM_EAN11, 'Not Set');

/*10 May 2016 End of Changes*/

/*23 May 2016 Georgiana EA Changes adding proportionunitqty according to BI-2930*/

merge into dim_part dp
USING
(select distinct dp.dim_partid, 
case when dp.UnitOfMeasure in ('GR','ML') then (m.MARM_UMREN/m.MARM_UMREZ) * (t.T006_NENNR/t.T006_ZAEHL)*1000 else (m.MARM_UMREN/m.MARM_UMREZ) * (t.T006_NENNR/t.T006_ZAEHL) end as proportionunitqty,
row_number() over (partition by dp.dim_partid order by dim_partid) as rowseqno
 from MARM m, T006 t,dim_part dp
 WHERE
 dp.partnumber = MARM_MATNR
 AND m.MARM_MSEHI = t.T006_MSEHI
 AND (m.MARM_ATINN <> '' or m.MARM_ATINN <> 0) ) t on dp.dim_partid=t.dim_partid
WHEN MATCHED THEN UPDATE SET dp.proportionunitqty=t.proportionunitqty
WHERE t.rowseqno=1
and dp.proportionunitqty<>t.proportionunitqty;
/*23 May 2016 End of Changes*/
/*25 May 2016 Georgiana EA Changes adding 4 new fields from MVER table according to BI-2890*/
update dim_part set Consumptionlastyear=0;
update dim_part set  avgconsumptionpermonth=0;
update dim_part set  previousmonthenddate='1900-01-01';
update dim_part set consumptiontrend= 0;

drop table if exists tmp_global;
create table tmp_global as
select * from
(
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV01 as TotalConsumption,'01' as MonthYear, '12' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION all
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV02 as TotalConsumption,'02' as MonthYear, '01' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV03 as TotalConsumption,'03' as MonthYear, '02' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV04 as TotalConsumption,'04' as MonthYear,'03' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV05 as TotalConsumption,'05' as MonthYear, '04' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV06 as TotalConsumption,'06' as MonthYear, '05' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV07 as TotalConsumption,'07' as MonthYear, '06' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV08 as TotalConsumption,'08' as MonthYear, '07' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV09 as TotalConsumption,'09' as MonthYear, '08' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV10 as TotalConsumption,'10' as MonthYear, '09' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV11 as TotalConsumption,'11' as MonthYear, '10' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)
UNION ALL
(select  MVER_MATNR,MVER_WERKS,MVER_GJAHR,MVER_PERKZ,MVER_ZAHLR,MVER_GSV12 as TotalConsumption,'12' as MonthYear, '11' as PreviousMonth, MVER_GJAHR -1 as PreviousYear
from MVER)) t;

/* For calculating consumption value we need to treat 2 situatons, where last year of consumption is equal with the current one or when is smaller*/
 /* 1 */
 /* Last year of cosumption is equal with the current one */

 /*drop table if exists tmp_for_consumption
create table tmp_for_consumption as
select MVER_MATNR,MVER_WERKS, sum(totalconsumption) as currentyearconsumption from tmp_global
 where
(((MonthYear between month(current_date)+1 and 12) and MVER_GJAHR=year(current_date)-1)
or (MVER_GJAHR=year(current_date) and MonthYear between 1 and  month(current_date)))
and totalconsumption>0
group by MVER_MATNR,MVER_WERKS*/

 drop table if exists tmp_for_consumption;
create table tmp_for_consumption as
select MVER_MATNR,MVER_WERKS, sum(case when MonthYear=month(current_date) then 0 else totalconsumption end) as currentyearconsumption from tmp_global
 where

(
((MonthYear between month(current_date- INTERVAL '1' YEAR) and month(current_date)) and MVER_GJAHR=year(current_date)-1)
or (MVER_GJAHR=year(current_date) and MonthYear between 1 and  month(current_date)))
and totalconsumption>0
group by MVER_MATNR,MVER_WERKS;

/*
drop table if exists tmp_for_daysinmonth
create table tmp_for_daysinmonth as
select
distinct convert(decimal (18,4),daysinmonth) daysinmonth,convert(decimal (18,4),dayofmonth)dayofmonth ,MVER_MATNR,MVER_WERKS,MVER_GJAHR-1 PreviousYear,MVER_GJAHR currentYear,MVER_PERKZ,MVER_ZAHLR,(daysinmonth-dayofmonth) remaineddays
from tmp_global b, dim_date dt
where  current_date = dt.datevalue
and MVER_GJAHR=dt.calendaryear*/

drop table if exists tmp_for_daysinmonth;
create table tmp_for_daysinmonth as
select
distinct 
convert(decimal (18,4),daysinmonth) daysinmonth,
(daysinmonth-dayofmonth) remaineddays
from 
dim_date dt
where  current_date = dt.datevalue
and dt.companycode = 'Not Set'
and dt.plantcode_factory = 'Not Set';

/* Liviu Ionescu - unstable set of rows */
/*drop table if exists tmp_for_totalconsumption
create table tmp_for_totalconsumption as
select case when td.remaineddays <> 0 then (td.remaineddays/td.daysinmonth)*TotalConsumption + tc.currentyearconsumption else TotalConsumption + tc.currentyearconsumption end OneYearConsumption,
tc.MVER_MATNR,tc.MVER_WERKS ,
row_number() over (partition by tc.MVER_MATNR,tc.MVER_WERKS  order by '') as rownumber /* unstable set of rows 
from
tmp_for_daysinmonth td,tmp_for_consumption tc,tmp_global tg
where
(MonthYear= month(current_date)) and tg.MVER_GJAHR=year(current_date)-1
and  tc.MVER_MATNR=td.MVER_MATNR
and tc.MVER_WERKS=td.MVER_WERKS
and tg.MVER_MATNR=td.MVER_MATNR
and tg.MVER_WERKS=td.MVER_WERKS
and tg.totalconsumption>0*/


drop table if exists tmp_for_totalconsumption;
create table tmp_for_totalconsumption as
select case when td.remaineddays <> 0 then (td.remaineddays/td.daysinmonth)*TotalConsumption + tc.currentyearconsumption else TotalConsumption + tc.currentyearconsumption end OneYearConsumption,
tc.MVER_MATNR,tc.MVER_WERKS ,
row_number() over (partition by tc.MVER_MATNR,tc.MVER_WERKS  order by '') as rownumber /* unstable set of rows */
from
tmp_for_daysinmonth td,tmp_for_consumption tc,tmp_global tg
where
(MonthYear= month(current_date)) and tg.MVER_GJAHR=year(current_date)-1
and tg.MVER_MATNR=tc.MVER_MATNR
and tg.MVER_WERKS=tc.MVER_WERKS
and tg.totalconsumption>0;

update dim_part dp
set dp.Consumptionlastyear=tu.OneYearConsumption,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_for_totalconsumption tu,dim_part dp
where
dp.partnumber=tu.MVER_MATNR
and dp.plant= tu.MVER_WERKS
and rownumber = 1 /* unstable set of rows */
and dp.Consumptionlastyear <> tu.OneYearConsumption;

update dim_part dp
set dp.avgconsumptionpermonth=tu.OneYearConsumption/12,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_for_totalconsumption tu, dim_part dp
where
dp.partnumber=tu.MVER_MATNR
and dp.plant= tu.MVER_WERKS
and rownumber = 1 /* unstable set of rows */
and dp.avgconsumptionpermonth <> tu.OneYearConsumption/12;

/* Georgiana 03 Aug 2016 Bellow statements are treating those situations where we only have consumption in current year*/
drop table if exists tmp_for_totalconsumption;

/* Liviu Ionescu - unstable set of rows */
create table tmp_for_totalconsumption as
select  tc.currentyearconsumption OneYearConsumption,
tc.MVER_MATNR,tc.MVER_WERKS,
row_number() over (partition by tc.MVER_MATNR,tc.MVER_WERKS  order by '') as rownumber /* unstable set of rows */
from
tmp_for_daysinmonth td,tmp_for_consumption tc,tmp_global tg
where
tg.MVER_GJAHR=year(current_date)
and tg.MVER_MATNR=tc.MVER_MATNR
and tg.MVER_WERKS=tc.MVER_WERKS
and tg.totalconsumption>0;

update dim_part dp
set dp.Consumptionlastyear=tu.OneYearConsumption,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_for_totalconsumption tu, dim_part dp
where
dp.partnumber=tu.MVER_MATNR
and dp.plant= tu.MVER_WERKS
and rownumber = 1 /* unstable set of rows */
and dp.Consumptionlastyear <> tu.OneYearConsumption;

update dim_part dp
set dp.avgconsumptionpermonth=tu.OneYearConsumption/12,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_for_totalconsumption tu, dim_part dp
where
dp.partnumber=tu.MVER_MATNR
and dp.plant= tu.MVER_WERKS
and rownumber = 1 /* unstable set of rows */
and dp.avgconsumptionpermonth <> tu.OneYearConsumption/12;
/*End of Changes 03 Aug 2016*/

/*2*/ /*Last year of cosumption is smaller than current one*/ 

 drop table if exists tmp_for_consumption_lasty;
create table tmp_for_consumption_lasty as
select MVER_MATNR,MVER_WERKS, sum(totalconsumption) as currentyearconsumption from tmp_global tg
 where
(((MonthYear between month(current_date)+1 and 12) and MVER_GJAHR=year(current_date)-1)
or (MVER_GJAHR=year(current_date) and MonthYear between 1 and  month(current_date)))
/*and totalconsumption>0*/
group by MVER_MATNR,MVER_WERKS;

drop table if exists tmp_for_previousmonthyear1;
create table tmp_for_previousmonthyear1 as
select distinct MVER_MATNR,MVER_WERKS,first_value(totalconsumption) over (partition by mver_matnr,mver_werks order by mver_gjahr desc,monthyear desc ) as "last_value"
from tmp_global tg where totalconsumption>0;

drop table if exists tmp_for_previousmonthyear2;
create table tmp_for_previousmonthyear2 as
select tg.MVER_MATNR,tg.MVER_WERKS, max(concat(tg.MVER_GJAHR,tg.monthyear)) yearmonth
from tmp_global tg, tmp_for_previousmonthyear1 tp
where tg.MVER_MATNR=tp.MVER_MATNR
and tg.MVER_WERKS=tp.MVER_WERKS
and tg.totalconsumption=tp."last_value"
group by tg.MVER_MATNR,tg.MVER_WERKS;

/* Results to big, create table crashes, remove unused columns - not working either */
drop table if exists tmp_for_daysinmonth;
create table tmp_for_daysinmonth as 
select 
-- distinct cast(daysinmonth as decimal(18,4)) daysinmonth,cast(dayofmonth as decimal(18,4))dayofmonth ,MVER_MATNR,MVER_WERKS,MVER_GJAHR-1 PreviousYear,MVER_GJAHR currentYear,MVER_PERKZ,MVER_ZAHLR,(daysinmonth-dayofmonth) remaineddays
distinct cast(daysinmonth as decimal(18,4)) daysinmonth,MVER_MATNR,MVER_WERKS,(daysinmonth-dayofmonth) remaineddays
from tmp_global b, dim_date dt, dim_plant pl
where  current_date = dt.datevalue
and year(current_date)=dt.calendaryear
and pl.companycode = dt.companycode /* Octavian : Add dim_plant join */
AND pl.plantcode = MVER_WERKS
AND dt.plantcode_factory = pl.plantcode;

/* Liviu Ionescu - unstable set of rows */
drop table if exists tmp_for_totalconsumption;
create table tmp_for_totalconsumption as
select case when td.remaineddays <> 0 then (td.remaineddays/td.daysinmonth)*TotalConsumption + tc.currentyearconsumption else TotalConsumption + tc.currentyearconsumption end OneYearConsumption,
tc.MVER_MATNR,tc.MVER_WERKS,(td.remaineddays/td.daysinmonth)*TotalConsumption AS remdaysconsumption /* add alias */,tc.currentyearconsumption,
row_number() over (partition by tc.MVER_MATNR,tc.MVER_WERKS  order by '') as rownumber /* unstable set of rows */
from
tmp_for_daysinmonth td,tmp_for_consumption_lasty tc,tmp_global tg
where
(MonthYear= month(current_date)) and tg.MVER_GJAHR = year(current_date)-1
and  tc.MVER_MATNR=td.MVER_MATNR
and tc.MVER_WERKS=td.MVER_WERKS
and tg.MVER_MATNR=td.MVER_MATNR
and tg.MVER_WERKS=td.MVER_WERKS;

update dim_part dp
set dp.Consumptionlastyear=tu.OneYearConsumption,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_for_totalconsumption tu, dim_part dp
where
dp.partnumber=tu.MVER_MATNR
and dp.plant= tu.MVER_WERKS
and rownumber = 1 /* unstable set of rows */
and dp.Consumptionlastyear <> tu.OneYearConsumption;

update dim_part dp
set dp.avgconsumptionpermonth=tu.OneYearConsumption/12,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_for_totalconsumption tu, dim_part dp
where
dp.partnumber=tu.MVER_MATNR
and dp.plant= tu.MVER_WERKS
and rownumber = 1 /* unstable set of rows */
and dp.avgconsumptionpermonth <> tu.OneYearConsumption/12;

drop table if exists tmp_for_previousmonthyear;
create table tmp_for_previousmonthyear
as
select tg.*, concat(MVER_GJAHR,tg.monthyear) previousmonthyear, row_number() over (partition by mver_matnr,mver_werks order by mver_gjahr desc,monthyear desc) as xranking
from tmp_global tg
where totalconsumption>0;


drop table if exists tmp_for_previousconsumptionmonth;
create table tmp_for_previousconsumptionmonth as
select
MVER_MATNR,MVER_WERKS,MVER_GJAHR, dt.monthenddate as previousconsumptionmonth
from tmp_for_previousmonthyear tg, dim_date dt
where dt.companycode='Not Set'
and dt.calendarmonthid = tg.previousmonthyear
and dt.dayofmonth = 1
and xranking = 1;

/* previousmonthenddate depends to the hour the processing of Part Dimension is taking place, so we are adding 12 hours in order to be sure that we are not taking the date/time of the server*/
/*update dim_part dp
set dp.previousmonthenddate= case when month(tp.previousconsumptionmonth)=month(current_date) then to_date(current_timestamp + INTERVAL '12' HOUR)-1  else tp.previousconsumptionmonth end,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl
from tmp_for_previousconsumptionmonth tp, dim_part dp
where
dp.partnumber=tp.MVER_MATNR
and dp.plant= tp.MVER_WERKS
and dp.previousmonthenddate <> tp.previousconsumptionmonth*/

/*when the Last Consumption Date is in the current month, we do not have to set it to the end of the month but on current date minus 1 day*/

/* update dim_part dp
set dp.previousmonthenddate= case when month(tp.previousconsumptionmonth)=month(current_date) and year(tp.previousconsumptionmonth) =year(current_date) then  to_date(current_timestamp + INTERVAL '12' HOUR)-1 else tp.previousconsumptionmonth end,
dw_update_date = current_timestamp
from tmp_for_previousconsumptionmonth tp, dim_part dp
where
dp.partnumber=tp.MVER_MATNR
and dp.plant= tp.MVER_WERKS
and dp.previousmonthenddate <> tp.previousconsumptionmonth */

/* 08mar2017 Andrei R.  Unable to get a stable set of rows in the source tables     */
merge into dim_part dp
using( 
select distinct dp.partnumber, dp.plant  ,  case 
when month(tp.previousconsumptionmonth)=month(current_date) 
and year(tp.previousconsumptionmonth) =year(current_date)
 then to_date(current_timestamp + INTERVAL '12' HOUR)-1 
else tp.previousconsumptionmonth end AS previousconsumptionmonth
from tmp_for_previousconsumptionmonth tp, dim_part dp
where
dp.partnumber=tp.MVER_MATNR
and dp.plant= tp.MVER_WERKS) tmp
on dp.partnumber = tmp.partnumber and dp.plant = tmp.plant
WHEN MATCHED THEN UPDATE
SET  dp.previousmonthenddate = tmp.previousconsumptionmonth
 where dp.previousmonthenddate <> tmp.previousconsumptionmonth;
 /* 08mar2017 end */

drop table if exists tmp_for_trend_previousyear;
create table tmp_for_trend_previousyear as
select tg.mver_matnr,tg.mver_werks, sum(tg.totalconsumption) as consumptionlast2mon
from tmp_global tg,tmp_for_previousmonthyear2 tp
where tg.MVER_MATNR=tp.MVER_MATNR
and tg.MVER_WERKS=tp.MVER_WERKS
and concat(tg.MVER_GJAHR,tg.monthyear) between tp.yearmonth -101 and tp.yearmonth -100
group by tg.MVER_MATNR,tg.MVER_WERKS;

drop table if exists tmp_for_trend_currentyear;
create table tmp_for_trend_currentyear as
select tg.MVER_MATNR,tg.MVER_WERKS,sum(tg.totalconsumption) consumptionlast2mon
from tmp_global tg,tmp_for_previousmonthyear2 tp
where tg.MVER_MATNR=tp.MVER_MATNR
and tg.MVER_WERKS=tp.MVER_WERKS
and concat(tg.MVER_GJAHR,tg.monthyear) between tp.yearmonth - 1 and tp.yearmonth
group by tg.MVER_MATNR,tg.MVER_WERKS;

update dim_part dp
set dp.consumptiontrend= case when tp.consumptionlast2mon = 0 then 0 else ifnull(tc.consumptionlast2mon/tp.consumptionlast2mon,0) end ,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_for_trend_currentyear tc, tmp_for_trend_previousyear tp , dim_part dp
where dp.partnumber=tp.MVER_MATNR
and dp.plant= tp.MVER_WERKS
and tc.MVER_MATNR=tp.MVER_MATNR
and tc.MVER_WERKS=tp.MVER_WERKS
AND dp.consumptiontrend <> case when tp.consumptionlast2mon = 0 then 0 else ifnull(tc.consumptionlast2mon/tp.consumptionlast2mon,0) end;

/*25 May 2016 End of Changes*/

/* Madalina 19 Jul 2016 - Move the updates from dim_part into fact_materialmovement script - Check logic - Material Master: Date of Last Goods Movement and Type */
/* Octavian: Movement Type and Date of last movement type per material and plant */
/*

DROP TABLE IF EXISTS dim_part_dateoflastgoodsmvmttype
CREATE TABLE dim_part_dateoflastgoodsmvmttype AS
SELECT  dim_partid, PostingDate, movementtype
FROM
(
    select
f.dim_partid,dt.datevalue AS PostingDate,concat(mt.movementtype,' - ',mt.description) AS movementtype,
row_number() over (partition by f.dim_partid order by dt.datevalue desc) as rn
from fact_materialmovement f, dim_movementtype mt, dim_date dt, dim_part dp
where f.dim_movementtypeid = mt.dim_movementtypeid
and dt.dim_dateid = f.dim_dateidpostingdate
and dp.dim_partid = f.dim_partid
AND  ((dp.lastmovementtype <> concat(mt.movementtype,' - ',mt.description)) OR (dp.dateoflastgoods <> dt.datevalue))
) t
WHERE t.rn = 1

UPDATE dim_part dp
SET dp.lastmovementtype = mm.movementtype,
dw_update_date = CURRENT_TIMESTAMP
FROM    dim_part_dateoflastgoodsmvmttype mm,dim_part dp
WHERE dp.dim_partid = mm.dim_partid
AND dp.lastmovementtype <> mm.movementtype

UPDATE dim_part dp
SET dp.dateoflastgoods = mm.PostingDate,
dw_update_date = CURRENT_TIMESTAMP
FROM    dim_part_dateoflastgoodsmvmttype mm,dim_part dp
WHERE dp.dim_partid = mm.dim_partid
AND dp.dateoflastgoods <> mm.PostingDate

DROP TABLE IF EXISTS dim_part_dateoflastgoodsmvmttype
*/
/* Octavian: Movement Type and Date of last movement type per material and plant */
/* END 19 Jul 2016 */

/* Madalina 23 Jul 2016 - add new attribute, therapeuticclassDescription - Therapeutical classes (PH5) Description - BI-3292 */
UPDATE dim_part dp
SET therapeuticclassDescription = ifnull(c.CAWNT_ATWTB, 'Not Set')
  ,dw_update_date = current_timestamp
FROM AUSP a, CAWNT c, dim_part dp
WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 130
  AND a.AUSP_KLART = 1
  AND a.AUSP_ATINN = c.CAWN_ATINN
  AND a.AUSP_ATWRT = c.CAWN_ATWRT
  AND therapeuticclassDescription <> ifnull(c.CAWNT_ATWTB, 'Not Set');

/* Yogini 24 October 2016 - add new attribute, International Article No. 'PK' according to BI-4469 */
/*Georgiana Changes 08 Nov 2016- removing the join on unit of measure*/
update dim_part dp
set dp.intarticlenopk = ifnull(MARM_EAN11, 'Not Set'),
dw_update_date = current_timestamp
from MARM m, dim_part dp
where /*dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and*/ dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
and m.MARM_MEINH = 'PK'
and dp.intarticlenopk <> ifnull(MARM_EAN11, 'Not Set');

/* End 24 October 2016 */


/* Yogini 8 Nov 2016 Added column capacity_score according to BI-4628 */
merge into dim_part dp 
using (select gpf_code, capacity_score
	from capscore c) c
on dp.productfamily_merck = c.gpf_code
when matched then update 
set dp.capacity_score = ifnull(c.capacity_score,0),
	dw_update_date = current_timestamp
where dp.capacity_score <> ifnull(c.capacity_score,0);

/* Andrei R 24 mar 2017 Added Serial No Profile and its description according to BI-5842*/
UPDATE dim_part dp
SET dp.SerialNumberProfile = ifnull(m.MARC_SERNP,'Not Set')
FROM MARA_MARC_MAKT m, dim_part dp
WHERE m.MARC_WERKS = dp.PLANT
AND m.MARA_MATNR = dp.PARTNUMBER
AND dp.SerialNumberProfile <> ifnull(m.MARC_SERNP,'Not Set');

UPDATE dim_part dp
SET dp.SerialNumberProfileText = ifnull(t.T377P_T_SERAILTXT,'Not Set')
FROM dim_part dp, T377P_T t
where t.T377P_T_SERAIL = dp.SerialNumberProfile
AND dp.SerialNumberProfileText <> ifnull(t.T377P_T_SERAILTXT,'Not Set');

/* Andrei R 12 apr 2017 Added Material freight group according to BI-6010*/
UPDATE dim_part dp
SET dp.MaterialFreightGroup = ifnull(m.MARC_MFRGR,'Not Set')
FROM MARA_MARC_MAKT m, dim_part dp
WHERE m.MARC_WERKS = dp.PLANT
AND m.MARA_MATNR = dp.PARTNUMBER
AND dp.MaterialFreightGroup <> ifnull(m.MARC_MFRGR,'Not Set');

/*Alin 2 june 2017 APP-6416*/
UPDATE dim_part dp
SET dp.SOURCELIST = ifnull(m.MARC_KORDB,'Not Set')
FROM MARA_MARC_MAKT m, dim_part dp
WHERE m.MARC_WERKS = dp.PLANT
AND m.MARA_MATNR = dp.PARTNUMBER
AND dp.SOURCELIST <> ifnull(m.MARC_KORDB,'Not Set');

/* Yogini 21 Aug 2017 APP-7239*/
update dim_part dp
set dp.servicelevel = ifnull(m.MARC_LGRAD,0),
	dw_update_date = current_timestamp
from MARA_MARC_MAKT m, dim_part dp
where dp.partnumber = ifnull(m.MARA_MATNR,'Not Set')
and dp.plant = ifnull(m.MARC_WERKS,'Not Set')
and dp.servicelevel <> ifnull(m.MARC_LGRAD,0);

update dim_part dp
set dp.SafetyTimePeriodProfile = ifnull(m.MARC_SHPRO,'Not Set'),
	dw_update_date = current_timestamp
from MARA_MARC_MAKT m, dim_part dp
where dp.partnumber = ifnull(m.MARA_MATNR,'Not Set')
and dp.plant = ifnull(m.MARC_WERKS,'Not Set')
and dp.SafetyTimePeriodProfile <> ifnull(m.MARC_SHPRO,'Not Set');

/*Alin 17 Oct 2017 APP-7778*/
UPDATE dim_part dp
   SET dp.batch_entry = ifnull(mck.MARC_KZECH,'Not Set'),
   dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p  , dim_part dp
     WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks
     and batch_entry <> ifnull(mck.MARC_KZECH,'Not Set');

/*Alin 7 Nov 2017 APP-7946*/
update dim_part
set lotsizeprodcosting_primarysite = ifnull(MARC_LOSGR,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and primarysite = ifnull(MARC_WERKS,'Not Set')
and lotsizeprodcosting_primarysite <> ifnull(MARC_LOSGR,0);

update dim_part
set lotsizeprodcosting_primarysite = ifnull(MARC_LOSGR,0),
dw_update_date = current_timestamp
from MARA_MARC_MAKT_SPRAS, dim_part
where partnumber = ifnull(MARA_MATNR,'Not Set')
and primarysite = ifnull(MARC_WERKS,'Not Set')
and lotsizeprodcosting_primarysite = 0
and lotsizeprodcosting_primarysite <> ifnull(MARC_LOSGR,0);

/*Ana 04 Jan 2018 APP-8534*/
update dim_part dp
set dp.JDNETProductName = ifnull(m.MARA_KJJDC_PRODUCT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from dim_part dp inner join mara m
on m.mara_matnr = dp.partnumber
and dp.JDNETProductName <> ifnull(m.MARA_KJJDC_PRODUCT,'Not Set');

update dim_part dp
set dp.JDNETUniversalProduct = ifnull(m.MARA_KJJDC_UNI_MATNR, 0),
dw_update_date = CURRENT_TIMESTAMP
from dim_part dp inner join mara m
on m.mara_matnr = dp.partnumber
and dp.JDNETUniversalProduct <> ifnull(m.MARA_KJJDC_UNI_MATNR, 0);

update dim_part dp
set dp.JDNETRefrigeratedProductDivision = ifnull(m.MARA_KJJDC_REFG_DIV,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from dim_part dp inner join mara m
on m.mara_matnr = dp.partnumber
and dp.JDNETRefrigeratedProductDivision <> ifnull(m.MARA_KJJDC_REFG_DIV,'Not Set');

update dim_part dp
set dp.JDNETDistinguishValidation = ifnull(m.MARA_KJJDC_VAL_DATE,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from dim_part dp inner join mara m
on m.mara_matnr = dp.partnumber
and dp.JDNETDistinguishValidation <> ifnull(m.MARA_KJJDC_VAL_DATE,'Not Set');

/*Alin APP-7276*/
update dim_plant d
set non_top = ifnull(z.Z1MM_FO_TARGET_NON_TOP, 0)
from dim_plant d,  Z1MM_FO_TARGET z
where d.plantcode = ifnull(z.Z1MM_FO_TARGET_ORDERING_PLANT, 'Not Set');

update dim_plant d
set top = ifnull(z.Z1MM_FO_TARGET_TOP, 0)
from dim_plant d,  Z1MM_FO_TARGET z
where d.plantcode = ifnull(z.Z1MM_FO_TARGET_ORDERING_PLANT, 'Not Set');

/*Octavian S 15-FEB-2018 APP-6837 Add new attribute*/
--Purchase order text
 MERGE INTO dim_part dp
 USING (
        SELECT DISTINCT dp.dim_partid,dp.partnumber,shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%MATERIAL%' 
                   AND stxh_tdid     = 'BEST'
                 GROUP BY stxh_tdname)      s
                 ,dim_part                  dp
         WHERE stxh_tdname  = dp.partnumber) t
   ON t.dim_partid  = dp.dim_partid
  AND t.partnumber  = dp.partnumber
 WHEN MATCHED 
 THEN UPDATE SET lONgtext_stxh_prch_ord_txt = IFNULL(t.shorttext,'Not Set');
