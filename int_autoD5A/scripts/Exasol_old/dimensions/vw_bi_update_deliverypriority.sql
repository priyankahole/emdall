UPDATE dim_DeliveryPriority dp
SET dp.Description = ifnull(t.BEZEI,'Not Set')
  FROM tprit t, dim_DeliveryPriority dp
WHERE dp.DeliveryPriority = t.LPRIO
 AND dp.RowIsCurrent = 1;
