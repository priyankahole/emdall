UPDATE    dim_notificationorigin nor
   SET nor.NotificationOriginName = t.DD07T_DDTEXT,
			nor.dw_update_date = current_timestamp
			       FROM
          dim_notificationorigin nor, DD07T t
 WHERE nor.RowIsCurrent = 1
       AND t.DD07T_DOMNAME = 'HERKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND nor.NotificationOriginCode = t.DD07T_DOMVALUE 
;