INSERT INTO dim_basecamp_todolists
(
dim_basecamp_todolistsid
)
SELECT 1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_basecamp_todolists WHERE dim_basecamp_todolistsid = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_basecamp_todolists';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_basecamp_todolists',
                ifnull(max(d.dim_basecamp_todolistsId),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_basecamp_todolists d
where d.dim_basecamp_todolistsid <> 1;


INSERT INTO dim_basecamp_todolists(
	dim_basecamp_todolistsid,
	app_url,
	completed,
	completed_count,
	created_at,
	creator,
	description,
	todolist_id,
	todolist_name,
	todolist_position,
	private,
	remaining_count,
	trashed,
	updated_at,
	url
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_basecamp_todolists') + row_number() over(order by '')  as dim_basecamp_todolistsid,
	app_url,
	completed,
	completed_count,
	created_at,
	creator,
	ifnull(description,'Not Set'),
	todolist_id,
	ifnull(todolist_name,'Not Set'),
	ifnull(todolist_position,0),
	private,
	remaining_count,
	trashed,
	updated_at,
	url
FROM stg_basecamp_todolists s
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_basecamp_todolists d
        WHERE s.todolist_id = d.todolist_id
	);

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_basecamp_todolists';


UPDATE dim_basecamp_todolists d
SET
d.app_url = s.app_url,
d.completed = s.completed,
d.completed_count = s.completed_count,
d.created_at = s.created_at,
d.creator = s.creator,
d.description = ifnull(s.description,'Not Set'),
d.todolist_name = ifnull(s.todolist_name,'Not Set'),
d.todolist_position = ifnull(s.todolist_position,0),
d.private = s.private,
d.remaining_count = s.remaining_count,
d.trashed = s.trashed,
d.updated_at = s.updated_at,
d.url = s.url

FROM dim_basecamp_todolists d 
	INNER JOIN stg_basecamp_todolists s ON d.todolist_id = s.todolist_id;

