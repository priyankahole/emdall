UPDATE dim_deliverytype a 
   SET Description = ifnull(TVLKT_VTEXT, 'Not Set')
   FROM TVLKT, dim_deliverytype a 
 WHERE a.deliverytype = TVLKT_LFART AND RowIsCurrent = 1;

