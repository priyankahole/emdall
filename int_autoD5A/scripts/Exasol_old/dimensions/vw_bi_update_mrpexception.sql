UPDATE dim_mrpexception mrpx 
   SET mrpx.Description = ifnull(T458B_AUSLT, 'Not Set')
FROM T458B t, dim_mrpexception mrpx
 WHERE mrpx.RowIsCurrent = 1
 AND mrpx.exceptionkey = t.T458B_AUSSL
;
UPDATE dim_mrpexception mrpx 
   SET 	mrpx.exceptionnumber = ifnull(a.T458A_AUSKT,'Not Set')
FROM T458B t, dim_mrpexception mrpx
 left join T458A on a.T458A_AUSSL = t.T458B_AUSSL
 WHERE mrpx.RowIsCurrent = 1
 AND mrpx.exceptionkey = t.T458B_AUSSL
 and mrpx.exceptionnumber <> ifnull(a.T458A_AUSKT,'Not Set')