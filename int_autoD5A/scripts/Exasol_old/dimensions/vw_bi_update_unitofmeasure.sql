UPDATE    dim_unitofmeasure uom

   SET uom.Description = a.T006A_MSEHT
       FROM
          t006a a , dim_unitofmeasure uom
 WHERE uom.RowIsCurrent = 1
 AND uom.UOM = a.T006A_MSEHI
;