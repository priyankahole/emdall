UPDATE    Dim_MaterialStagingZone ms
   SET ms.MSAreaTxt = ifnull(t.T30CT_LBZOT, 'Not Set'),
			ms.dw_update_date = current_timestamp
       FROM
          Dim_MaterialStagingZone ms, T30CT t
 WHERE ms.RowIsCurrent = 1
 AND ms.WarehouseCode = t.T30CT_LGNUM 
 AND ms.StagingArea = t.T30CT_LGBZO;

;