INSERT INTO dim_casetype(dim_casetypeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_casetype
               WHERE dim_casetypeId = 1);

UPDATE    
dim_casetype ds
SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set'),
	ds.dw_update_date = current_timestamp
FROM
dim_casetype ds,
SCMGCASETYPET dt
WHERE ds.RowIsCurrent = 1
  AND ds.CaseType = dt.CASE_TYPE
  AND dt.CASE_TYPE IS NOT NULL;
		  
delete from number_fountain m where m.table_name = 'dim_casetype';

insert into number_fountain
select 	'dim_casetype',
	ifnull(max(d.dim_casetypeId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_casetype d
where d.dim_casetypeId <> 1;

INSERT INTO dim_casetype(dim_CaseTypeid,
			       CaseType,
			       Description,
                               RowStartDate,
                               RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_casetype') 
          + row_number() over(order by '') ,
			 ifnull(CASE_TYPE, 'Not Set'),
			 ifnull(DESCRIPTION,'Not Set'),
            current_timestamp,
            1
       FROM SCMGCASETYPET
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_casetype
                    WHERE CaseType = CASE_TYPE)
   ORDER BY 2;

delete from number_fountain m where m.table_name = 'dim_casetype';
