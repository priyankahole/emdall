UPDATE    dim_effectiveformatplanning emp
   SET emp.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			emp.dw_update_date = current_timestamp
       FROM
          dim_effectiveformatplanning emp, DD07T t
 WHERE emp.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'NO_DISP_PLUS'
          AND emp.EffectiveForMatPlanning = ifnull(t.DD07T_DOMVALUE, 'Not Set');
		  

