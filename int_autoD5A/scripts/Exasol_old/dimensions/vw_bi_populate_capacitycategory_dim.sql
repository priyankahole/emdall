/* Added by Octavian - linked to Capacity SA as the time of the development - for Every Angle project */

insert into dim_capacitycategory (dim_capacitycategoryid,
                          capcategory,
                          description
						  )
select 1, 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_capacitycategory where dim_capacitycategoryid = 1); 


delete from number_fountain m where m.table_name = 'dim_capacitycategory';
insert into number_fountain
select 'dim_capacitycategory',
 ifnull(max(d.dim_capacitycategoryid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_capacitycategory d
where d.dim_capacitycategoryid <> 1;

insert into dim_capacitycategory (dim_capacitycategoryid,
                          capcategory,
                          description)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_capacitycategory') + row_number() over(order by '') AS dim_capacitycategoryid,
       ifnull(t.TC26T_KAPAR,'Not Set') AS capcategory,
       ifnull(t.TC26T_TXT,'Not Set') AS description
from TC26T t
 where not exists (select 'x' from dim_capacitycategory c where 
	      c.capcategory = ifnull(t.TC26T_KAPAR,'Not Set'));
	  

update dim_capacitycategory c
set c.description = ifnull(t.TC26T_TXT, 'Not Set'),
    c.dw_update_date = current_timestamp
from TC26T t, dim_capacitycategory c
where c.capcategory = ifnull(t.TC26T_KAPAR,'Not Set')
and c.description <> ifnull(t.TC26T_TXT, 'Not Set');