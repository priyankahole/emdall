insert into dim_inspectioncataloggroup
(dim_inspectioncataloggroupid,
"catalog",
CodeGroup,
ShortDescription)
select 
1, 'Not Set','Not Set','Not Set'
from (select 1) a
where not exists ( select 'x' from dim_inspectioncataloggroup where dim_inspectioncataloggroupid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectioncataloggroup';
insert into number_fountain
select 'dim_inspectioncataloggroup',
 ifnull(max(d.dim_inspectioncataloggroupid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectioncataloggroup d
where d.dim_inspectioncataloggroupid <> 1;

drop table if exists tmp_QPGR_QPGT;
create table tmp_QPGR_QPGT as 
select distinct QPGR_KATALOGART,QPGR_CODEGRUPPE,QPGT_KURZTEXT,QPGR_STATUS,first_value(QPGT_SPRACHE) over (partition by QPGR_KATALOGART,QPGR_CODEGRUPPE,QPGT_KURZTEXT order by QPGT_SPRACHE desc) as QPGT_SPRACHE
from QPGR_QPGT;

drop table if exists QPGR_QPGT;
rename table tmp_QPGR_QPGT to QPGR_QPGT;


insert into dim_inspectioncataloggroup
(dim_inspectioncataloggroupid,
"catalog",
CodeGroup,
ShortDescription)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_inspectioncataloggroup') + row_number() over(order by '') AS dim_inspectioncataloggroupid,
         ifnull(QPGR_KATALOGART, 'Not Set') as "catalog",
         ifnull(QPGR_CODEGRUPPE,'Not Set') as CodeGroup,
		 ifnull(QPGT_KURZTEXT, 'Not Set') as ShortDescription
FROM 
QPGR_QPGT q
where not exists ( select 'x' from  dim_inspectioncataloggroup
                    where 
					 "catalog" = ifnull(QPGR_KATALOGART, 'Not Set')
                     AND CodeGroup = ifnull(QPGR_CODEGRUPPE,'Not Set') );