UPDATE    dim_inventoryindicator ii
        SET ii.Description = ifnull(dt.DD07T_DDTEXT, 'Not Set'),
			ii.dw_update_date = current_timestamp
       FROM
          dim_inventoryindicator ii, DD07T dt
 WHERE ii.InventoryIndicator = dt.DD07T_DOMVALUE 
 AND ii.RowIsCurrent = 1
 AND dt.DD07T_DOMNAME = 'LVS_KZINV' 
 AND dt.DD07T_DOMNAME IS NOT NULL;
 
INSERT INTO dim_inventoryindicator(dim_inventoryindicatorid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inventoryindicator
               WHERE dim_inventoryindicatorid = 1);

delete from number_fountain m where m.table_name = 'dim_inventoryindicator';
   
insert into number_fountain
select 	'dim_inventoryindicator',
	ifnull(max(d.dim_inventoryindicatorid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inventoryindicator d
where d.dim_inventoryindicatorid <> 1; 

INSERT INTO dim_inventoryindicator(dim_inventoryindicatorid,
                                                                Description,
                               InventoryIndicator,
                               RowStartDate,
                               RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inventoryindicator')
          + row_number() over(order by '') ,
                        ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'LVS_KZINV'
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_inventoryindicator
                    WHERE InventoryIndicator = DD07T_DOMVALUE AND DD07T_DOMNAME = 'LVS_KZINV')
   ORDER BY 2;
