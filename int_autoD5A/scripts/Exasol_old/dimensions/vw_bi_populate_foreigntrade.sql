insert into dim_foreigntrade_decltoauth(dim_foreigntrade_decltoauthid,exclusioninclusionindicator,description)
select 1, 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_foreigntrade_decltoauth where dim_foreigntrade_decltoauthid = 1);

DELETE FROM number_fountain m where m.table_name = 'dim_foreigntrade_decltoauth';

INSERT INTO number_fountain
select 	'dim_foreigntrade_decltoauth',
	ifnull(max(d.dim_foreigntrade_decltoauthid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM dim_foreigntrade_decltoauth d
WHERE d.dim_foreigntrade_decltoauthid <> 1; 


INSERT INTO dim_foreigntrade_decltoauth
(dim_foreigntrade_decltoauthid,
exclusioninclusionindicator,
description
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_foreigntrade_decltoauth') + row_number() over(order by '') AS dim_foreigntrade_decltoauthid,
T609AT_SEGAL AS exclusioninclusionindicator,
ifnull(T609AT_TEXT1,'Not Set')AS description
from T609AT t
WHERE 
not exists (select 1 from dim_foreigntrade_decltoauth ft
WHERE ft.exclusioninclusionindicator = t.T609AT_SEGAL
);


UPDATE dim_foreigntrade_decltoauth ft
SET description = ifnull(t.T609AT_TEXT1,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM T609AT t, dim_foreigntrade_decltoauth ft
WHERE ft.exclusioninclusionindicator = t.T609AT_SEGAL
AND description <> ifnull(t.T609AT_TEXT1,'Not Set');