/* 
#################################################################################################################
Script         : vw_bi_populate_dim_rebatesagreement 
Author         : SVinayan 
Created On     : 24 Apr 2015 
Description    : 
*********************************************Change History**************************************************
Date               By              Version              Desc                      
24Apr2015          Suchithra        1.0                 New Script                                      
14Jun2015	   Suchithra	    1.1                 New field Owner
#################################################################################################################
*/


/* Create the default entry in the dimension table if its not already present*/
INSERT INTO dim_rebateagreements(	
	dim_rebateagreementsid,
    agreementnum,
	externaldesc,
	participationperiod,
	rebatebasis,
	owner,
	rowstartdate,
	rowenddate,
	rowiscurrent,
	rowchangereason
)
SELECT
        1,
        'Not Set',
        'Not Set',
        'Not Set',
        'Not Set',
        'Not Set',
        CURRENT_TIMESTAMP,
        NULL,
        1,
        NULL
FROM (SELECT 1) a
WHERE NOT EXISTS (SELECT 1 FROM dim_rebateagreements WHERE dim_rebateagreementsid = 1);


delete from number_fountain m where m.table_name = 'dim_rebateagreements';
   
insert into number_fountain
select 	'dim_rebateagreements',
	ifnull(max(d.dim_rebateagreementsid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_rebateagreements d
where d.dim_rebateagreementsid <> 1; 

INSERT INTO dim_rebateagreements(
	dim_rebateagreementsid,
	agreementnum,
	externaldesc,
	participationperiod,
	rebatebasis,
	owner,
	rowstartdate,
	rowiscurrent
)
SELECT
        (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_rebateagreements')  + row_number() over(order by ''),
        ifnull(irm_ipprasp_knuma_ag, 'Not Set'),
        ifnull(irm_ipprasp_BOART_AG, 'Not Set'),
        ifnull(irm_ipprasp_ZD_PARTPERIOD_PR, 'Not Set'),
        ifnull(irm_ipprasp_OBJEC, 'Not Set'),
        ifnull(irm_ipprasp_OWNER,'Not Set'),
	current_timestamp,
        1
FROM irm_ipprasp
WHERE NOT EXISTS (Select 1 from dim_rebateagreements
					where agreementnum = irm_ipprasp_knuma_ag
					and rowiscurrent = 1);
					
UPDATE dim_rebateagreements
SET externaldesc = ifnull(irm_ipprasp_BOART_AG, 'Not Set')
FROM irm_ipprasp, dim_rebateagreements
WHERE agreementnum = irm_ipprasp_knuma_ag
AND externaldesc <> ifnull(irm_ipprasp_BOART_AG, 'Not Set') ;

UPDATE dim_rebateagreements
SET participationperiod = ifnull(irm_ipprasp_ZD_PARTPERIOD_PR, 'Not Set')
FROM irm_ipprasp, dim_rebateagreements
WHERE agreementnum = irm_ipprasp_knuma_ag
AND participationperiod <> ifnull(irm_ipprasp_ZD_PARTPERIOD_PR, 'Not Set') ;

UPDATE dim_rebateagreements
SET rebatebasis = ifnull(irm_ipprasp_OBJEC, 'Not Set')
FROM irm_ipprasp, dim_rebateagreements
WHERE agreementnum = irm_ipprasp_knuma_ag
AND rebatebasis <> ifnull(irm_ipprasp_OBJEC, 'Not Set') ;

/* 14Jun2015 - New field Owner */
UPDATE dim_rebateagreements
SET owner = ifnull(irm_ipprasp_OWNER, 'Not Set')
FROM irm_ipprasp, dim_rebateagreements
WHERE agreementnum = irm_ipprasp_knuma_ag
AND owner <> ifnull(irm_ipprasp_OWNER, 'Not Set') ;




