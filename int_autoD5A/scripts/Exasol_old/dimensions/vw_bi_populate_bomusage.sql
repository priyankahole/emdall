INSERT INTO dim_bomusage(dim_bomusageid, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_bomusage
               WHERE dim_bomusageid = 1);

UPDATE    dim_bomusage bu
   SET bu.Description = ifnull(t.T416T_ANTXT, 'Not Set'),
			bu.dw_update_date = current_timestamp
       FROM dim_bomusage bu,
          T416T t
 WHERE bu.RowIsCurrent = 1
 AND bu.BOMUsageCode = t.T416T_STLAN;

delete from number_fountain m where m.table_name = 'dim_bomusage';

insert into number_fountain
select 	'dim_bomusage',
	ifnull(max(d.dim_bomusageid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_bomusage d
where d.dim_bomusageid <> 1;
			   
INSERT INTO dim_bomusage(Dim_bomusageid,
                                                                Description,
                                BOMUsageCode,
                                RowStartDate,
                                RowIsCurrent)
     SELECT  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_bomusage')
          + row_number() over(order by ''),
                        ifnull(T416T_ANTXT, 'Not Set'),
            T416T_STLAN,
            current_timestamp,
            1
       FROM T416T
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_bomusage
                    WHERE BOMUsageCode = T416T_STLAN)
   ORDER BY 2 ;

delete from number_fountain m where m.table_name = 'dim_bomusage';
   