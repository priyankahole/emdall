/* Octavian: Every Angle Transition Addons */

/* preprocess */ 
drop table if exists INOB_BB;
create table INOB_BB
as select b.*,RTRIM(SUBSTR(INOB_OBJEK,1,INSTR(INOB_OBJEK,'  '))) INOB_MATNR,
LTRIM(SUBSTR(INOB_OBJEK,INSTR(INOB_OBJEK,'   ',-1)+1,LENGTH(INOB_OBJEK)-INSTR(INOB_OBJEK,'   ',-1))) INOB_CHARG from INOB b;

drop table if exists tmp_distinct_INOB_BB;
create table tmp_distinct_INOB_BB as
select b.* from (
select a.*,row_Number() over (partition by INOB_MATNR,INOB_CHARG order by INOB_CUOBJ desc) as RN 
from INOB_BB a) b
where b.rn=1;

drop table if exists INOB_BB;
rename tmp_distinct_INOB_BB to INOB_BB;
alter table INOB_BB drop RN cascade;


drop table if exists tmp_distinct_mch1;
create table tmp_distinct_mch1 as
select b.* from (
select a.*,row_Number() over (partition by mch1_matnr,mch1_charg order by mch1_laeda,mch1_ersda desc) as RN 
from MCH1 a) b
where b.rn=1;	

drop table if exists tmp_distinct_mcha;
create table tmp_distinct_mcha as
select b.* from (
select a.*,row_Number() over (partition by mcha_matnr,mcha_werks,mcha_charg order by mcha_laeda,mcha_ersda desc) as RN 
from MCHA a) b
where b.rn=1;

drop table if exists MCH1;
drop table if exists MCHA;
rename tmp_distinct_mcha to MCHA;
rename tmp_distinct_mch1 to MCH1;
alter table MCHA drop RN cascade;
alter table MCH1 drop RN cascade;



drop table if exists tmp_dim_batch;
CREATE TABLE tmp_dim_batch LIKE dim_batch INCLUDING DEFAULTS INCLUDING IDENTITY;

insert into tmp_dim_batch
select * from dim_batch;

insert into tmp_dim_batch (dim_batchid,
                          PartNumber,
                          BatchNumber,
     InternalObjNo)
select 1, 'Not Set', 'Not Set', 0
from (select 1) a
where not exists ( select 'x' from tmp_dim_batch where dim_batchid = 1);
	  
delete from number_fountain m where m.table_name = 'dim_batch';
insert into number_fountain
select 'dim_batch',
 ifnull(max(d.dim_batchid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_dim_batch d
where d.dim_batchid <> 1;

insert into tmp_dim_batch (dim_batchid,
                          PartNumber,
                          BatchNumber,
     InternalObjNo,
                          dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_batch') + row_number() over(order by '') AS dim_batchid,
        ifnull(ds.MCH1_MATNR, 'Not Set') AS PartNumber,
        ifnull(ds.MCH1_CHARG, 'Not Set') AS BatchNumber,
 ifnull(ds.MCH1_CUOBJ_BM, 0) AS InternalObjNo,
       current_timestamp
from MCH1 ds
 where not exists (select 'x' from dim_batch dc where dc.PartNumber = ifnull(ds.MCH1_MATNR, 'Not Set') AND dc.BatchNumber = ifnull(ds.MCH1_CHARG, 'Not Set'));

delete from tmp_dim_batch d
where not exists (select 1 from MCH1 ds where d.PartNumber = ifnull(ds.MCH1_MATNR, 'Not Set') AND d.BatchNumber = ifnull(ds.MCH1_CHARG, 'Not Set'))
and d.plantcode = 'Not Set' and d.dim_batchid <> 1;

 /*Georgiana EA Addons -adding MCHA fields 03 Feb 2016*/
 
delete from number_fountain m where m.table_name = 'dim_batch';
insert into number_fountain
select 'dim_batch',
 ifnull(max(d.dim_batchid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_dim_batch d
where d.dim_batchid <> 1;

insert into tmp_dim_batch (dim_batchid,
                          PartNumber,
                          BatchNumber,
                          PlantCode,
                          dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_batch') + row_number() over(order by '') AS dim_batchid,
        ifnull(ds.MCHA_MATNR, 'Not Set') AS PartNumber,
        ifnull(ds.MCHA_CHARG, 'Not Set') AS BatchNumber,
        ifnull( ds.MCHA_WERKS, 'Not Set') as PlantCode,
       current_timestamp
from MCHA ds
 where not exists (select 'x' from dim_batch dc where dc.PartNumber = ifnull(ds.MCHA_MATNR, 'Not Set') AND dc.BatchNumber = ifnull(ds.MCHA_CHARG, 'Not Set') and dc.PlantCode = ifnull( ds.MCHA_WERKS, 'Not Set'));
 
delete from tmp_dim_batch d
 where not exists (select 1 from MCHA ds where d.PartNumber = ifnull(ds.MCHA_MATNR, 'Not Set') AND d.BatchNumber = ifnull(ds.MCHA_CHARG, 'Not Set') and d.PlantCode = ifnull( ds.MCHA_WERKS, 'Not Set'))
and d.plantcode <> 'Not Set' and d.dim_batchid <> 1;

/*Georgiana End Of Changes*/ 

update tmp_dim_batch bc
set bc.InternalObjNo = ifnull(m.MCH1_CUOBJ_BM, 0),
    bc.dw_update_date = current_timestamp
from MCH1 m, tmp_dim_batch bc
where     bc.PartNumber = ifnull(m.MCH1_MATNR,'Not Set')
	 and  bc.BatchNumber = ifnull(m.MCH1_CHARG,'Not Set')
      and InternalObjNo <> ifnull(m.MCH1_CUOBJ_BM, 0);

/* Roxana - Fix processig eror : different ATZHL column  */
	  
drop table if exists tmp_AUSP_BATCH;
create table tmp_AUSP_BATCH as select distinct AUSP_OBJEK,AUSP_KLART,AUSP_ATINN,AUSP_ATFLV,AUSP_ATWRT from AUSP_BATCH;

/* end fix */ 

/*Alin 1 Nov 2017 - logic change for financialprovision acc to APP-7705*/
update tmp_dim_batch
set financialprovision = 0;

update tmp_dim_batch bt
set bt.financialprovision = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, MCH1 b, tmp_dim_batch bt
where b.MCH1_CUOBJ_BM= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 21
and ifnull(b.MCH1_MATNR, 'Not Set') = bt.partnumber 
and ifnull(b.MCH1_CHARG, 'Not Set') = bt.batchnumber;

/*
update tmp_dim_batch bt
set bt.financialprovision = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 21
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.financialprovision <> ifnull(AUSP_ATFLV,0)*/

update tmp_dim_batch
set actionlogreview = 'Not Set';

update tmp_dim_batch bt
set bt.actionlogreview = concat(ifnull(a.AUSP_ATWRT,0),' (',ifnull(c.cawnt_atwtb,'Not Set'),')'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, CAWNT c, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 22
and a.AUSP_ATINN = c.CAWN_ATINN
and a.AUSP_ATWRT = c.CAWN_ATWRT
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.actionlogreview <> concat(ifnull(a.AUSP_ATWRT,0),' (',ifnull(c.cawnt_atwtb,'Not Set'),')');

/*Alin 15 Mar 2018 fix app-7505*/
update tmp_dim_batch
set reasonlogreview= 'Not Set';

update tmp_dim_batch bt
set bt.reasonlogreview = concat(ifnull(a.AUSP_ATWRT,0),' (',ifnull(c.cawnt_atwtb,'Not Set'),')'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, CAWNT c,tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 23
and a.AUSP_ATINN = c.CAWN_ATINN
and a.AUSP_ATWRT = c.CAWN_ATWRT
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber;


/*
update tmp_dim_batch bt
set bt.releasestatusint = concat(ifnull(a.AUSP_ATWRT,0),' (',ifnull(c.cawnt_atwtb,'Not Set'),')'),
dw_update_date = current_timestamp
FROM AUSP_BATCH a, INOB_BB b, CAWNT c,tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 47
and a.AUSP_ATINN = c.CAWN_ATINN
and a.AUSP_ATWRT = c.CAWN_ATWRT
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.releasestatusint <> concat(ifnull(a.AUSP_ATWRT,0),' (',ifnull(c.cawnt_atwtb,'Not Set'),')')
*/

MERGE INTO tmp_dim_batch bt
USING ( SELECT bt.partnumber, bt.batchnumber, MAX(concat(ifnull(a.AUSP_ATWRT,0),' (',ifnull(c.cawnt_atwtb,'Not Set'),')')) as releasestatusint
        FROM AUSP_BATCH a, INOB_BB b, CAWNT c,tmp_dim_batch bt
		where b.INOB_CUOBJ= a.AUSP_OBJEK
		and AUSP_KLART = 23
		and AUSP_ATINN = 47
		and a.AUSP_ATINN = c.CAWN_ATINN
		and a.AUSP_ATWRT = c.CAWN_ATWRT
		and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
        GROUP BY bt.partnumber, bt.batchnumber) x
ON (bt.partnumber = x.partnumber and bt.batchnumber = x.batchnumber)
WHEN MATCHED THEN
UPDATE SET bt.releasestatusint = x.releasestatusint
WHERE bt.releasestatusint <> x.releasestatusint; 

update tmp_dim_batch bt
set bt.primreleasespec = concat(ifnull(a.AUSP_ATWRT,'Not Set'),' (',ifnull(c.cawnt_atwtb,'Not Set'),')'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, CAWNT c, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 26
and a.AUSP_ATINN = c.CAWN_ATINN
and a.AUSP_ATWRT = c.CAWN_ATWRT
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.primreleasespec <> concat(ifnull(a.AUSP_ATWRT,'Not Set'),' (',ifnull(c.cawnt_atwtb,'Not Set'),')');

update tmp_dim_batch bt
set bt.totalexpirydate = ifnull(CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2)),'0001-01-01'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 30
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.totalexpirydate <> ifnull(CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2)),'0001-01-01');

/* Andrei - APP-7424 - Merck AH - Inventory: New cutoff date fields */
update tmp_dim_batch set donotshipafter = '0001-01-01';
update tmp_dim_batch set donotsellafter = '0001-01-01';

update tmp_dim_batch bt
set bt.donotshipafter = ifnull(CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2)),'0001-01-01'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 2792 
and AUSP_ATFLV <> 0
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.donotshipafter <> ifnull(CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2)),'0001-01-01');


update tmp_dim_batch bt
set bt.donotsellafter = ifnull(CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2)),'0001-01-01'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 2791
and AUSP_ATFLV <> 0
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.donotsellafter <> ifnull(CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2)),'0001-01-01');

/* end  Andrei - APP-7424 - Merck AH - Inventory: New cutoff date fields */

update tmp_dim_batch bt
set bt.qctiterin10log = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b,tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 35
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.qctiterin10log <> CAST(AUSP_ATFLV AS decimal (35,16));

update tmp_dim_batch bt
set bt.qctiterin2log = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 36
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.qctiterin2log <> CAST(AUSP_ATFLV AS decimal (35,16));

update tmp_dim_batch bt
set bt.qctiterforstdnumeric = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 37
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.qctiterforstdnumeric <> CAST(AUSP_ATFLV AS decimal (35,16));

update tmp_dim_batch bt
set bt.activityfactorl = ifnull(AUSP_ATFLV,0.0000),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b,tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 40
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.activityfactorl <> ifnull(AUSP_ATFLV,0.0000);

update tmp_dim_batch bt
set bt.activityfactorkg = ifnull(AUSP_ATFLV,0.0000),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b,tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 41
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.activityfactorkg <> ifnull(AUSP_ATFLV,0.0000);

/*
update tmp_dim_batch bt
set bt.batchcurrstagests = ifnull(a.ausp_atwrt,0),
dw_update_date = current_timestamp
FROM AUSP_BATCH a, INOB_BB b,tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 49
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.batchcurrstagests <> ifnull(a.ausp_atwrt,0)
*/


MERGE INTO tmp_dim_batch bt
USING ( SELECT bt.partnumber, bt.batchnumber, MAX(ifnull(a.ausp_atwrt,0)) as batchcurrstagests
		FROM AUSP_BATCH a, INOB_BB b,tmp_dim_batch bt
		where b.INOB_CUOBJ= a.AUSP_OBJEK
		and AUSP_KLART = 23
		and AUSP_ATINN = 49
		and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
		GROUP BY bt.partnumber, bt.batchnumber) x
ON (bt.partnumber = x.partnumber and bt.batchnumber = x.batchnumber)
WHEN MATCHED THEN
UPDATE SET bt.batchcurrstagests = x.batchcurrstagests
WHERE bt.batchcurrstagests <> x.batchcurrstagests;

update tmp_dim_batch bt
set bt.activityfactor = ifnull(AUSP_ATFLV,0.0000),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 244
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.activityfactor <> ifnull(AUSP_ATFLV,0.0000);

update tmp_dim_batch bt
set bt.dateofmanufacture = ifnull(m.MCH1_HSDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where m.MCH1_MATNR = bt.partnumber and m.MCH1_CHARG = bt.batchnumber
and bt.dateofmanufacture <> ifnull(m.MCH1_HSDAT,'0001-01-01');


 update tmp_dim_batch bt
SET vendorbatchnumber = ifnull(MCH1_LICHA,'Not Set'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and vendorbatchnumber <> ifnull(MCH1_LICHA,'Not Set');

/* Octavian: Every Angle Transition Addons - PPE changes */
 update tmp_dim_batch bt
SET vendoracctnumber = ifnull(MCH1_LIFNR,'Not Set'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and vendoracctnumber <> ifnull(MCH1_LIFNR,'Not Set');

 update tmp_dim_batch bt
SET batch_restriction = ifnull(MCH1_ZUSTD,'Not Set'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and batch_restriction <> ifnull(MCH1_ZUSTD,'Not Set');

 update tmp_dim_batch bt
SET lastgoodreceipt = ifnull(MCH1_LWEDT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and lastgoodreceipt <> ifnull(MCH1_LWEDT,'0001-01-01');

 update tmp_dim_batch bt
SET bestbeforedate = ifnull(MCH1_VFDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1 m, tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and bestbeforedate <> ifnull(MCH1_VFDAT,'0001-01-01');
/* Octavian: Every Angle Transition Addons - PPE changes */

/* Octavian: Every Angle Transition Addons */

/* Ovidiu: Every Angle Transition Addons */

 update tmp_dim_batch bt
set bt.externalshelflife = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 27
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.externalshelflife <> ifnull(AUSP_ATFLV,0);


 update tmp_dim_batch bt
set bt.internalexpirydate = ifnull(CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2)),'0001-01-01'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 29
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.internalexpirydate <> ifnull(CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2)),'0001-01-01');

  update tmp_dim_batch bt
set bt.shelflifeextendedorreduced = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 31
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.shelflifeextendedorreduced <> ifnull(AUSP_ATWRT,'Not Set');


 update tmp_dim_batch bt
set bt.formulationtiter10logperm = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 32
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.formulationtiter10logperm <> CAST(AUSP_ATFLV AS decimal (35,16));


 update tmp_dim_batch bt
set bt.formulationtiter2logperml = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 33
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.formulationtiter2logperml <> CAST(AUSP_ATFLV AS decimal (35,16));

 update tmp_dim_batch bt
set bt.formulationtiterstdval = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 34
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.formulationtiterstdval <> CAST(AUSP_ATFLV AS decimal (35,16));


 update tmp_dim_batch bt
set bt.concentrationfactor = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 38
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.concentrationfactor <> ifnull(AUSP_ATFLV,0);

 update tmp_dim_batch bt
set bt.maximumalloweddosage = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 45
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.maximumalloweddosage <> ifnull(AUSP_ATFLV,0);

 update tmp_dim_batch bt
set bt.minimumalloweddosage = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 46
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.minimumalloweddosage <> ifnull(AUSP_ATFLV,0);


 update tmp_dim_batch bt
set bt.ReleasestatusExternal = ifnull(AUSP_ATWRT,0),
dw_update_date = current_timestamp
FROM AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 46
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.ReleasestatusExternal <> ifnull(AUSP_ATWRT,0);

/*
 update tmp_dim_batch bt
set bt.registrationuse = ifnull(AUSP_ATWRT,0),
dw_update_date = current_timestamp
FROM AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 54
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.registrationuse <> ifnull(AUSP_ATWRT,0)
*/

MERGE INTO tmp_dim_batch bt
USING ( SELECT bt.partnumber, bt.batchnumber, MAX(ifnull(AUSP_ATWRT,0)) as registrationuse
		FROM AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
		where b.INOB_CUOBJ= a.AUSP_OBJEK
		and AUSP_KLART = 23
		and AUSP_ATINN = 54
		and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
		GROUP BY bt.partnumber, bt.batchnumber) x
ON (bt.partnumber = x.partnumber and bt.batchnumber = x.batchnumber)
WHEN MATCHED THEN
UPDATE SET bt.registrationuse = x.registrationuse
WHERE bt.registrationuse <> x.registrationuse;

 update tmp_dim_batch bt
set bt.suitableforstabilitystudies = ifnull(AUSP_ATWRT,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 55
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.suitableforstabilitystudies <> ifnull(AUSP_ATWRT,0);


/*
 update tmp_dim_batch bt
set bt.containsreworkedcomponents = ifnull(AUSP_ATWRT,0),
dw_update_date = current_timestamp
FROM AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 56
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.containsreworkedcomponents <> ifnull(AUSP_ATWRT,0)
*/

MERGE INTO tmp_dim_batch bt
USING ( SELECT bt.partnumber, bt.batchnumber, MAX(ifnull(AUSP_ATWRT,0)) as containsreworkedcomponents
		FROM AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
		where b.INOB_CUOBJ= a.AUSP_OBJEK
		and AUSP_KLART = 23
		and AUSP_ATINN = 56
		and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
		GROUP BY bt.partnumber, bt.batchnumber) x
ON (bt.partnumber = x.partnumber and bt.batchnumber = x.batchnumber)
WHEN MATCHED THEN
UPDATE SET bt.containsreworkedcomponents = x.containsreworkedcomponents
WHERE bt.containsreworkedcomponents <> x.containsreworkedcomponents;


 update tmp_dim_batch bt
set bt.batchhasbeenreturned = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 57
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.batchhasbeenreturned <> ifnull(AUSP_ATFLV,0);



 update tmp_dim_batch bt
set bt.bendorbulkbatchnumber = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 74
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.bendorbulkbatchnumber <> ifnull(AUSP_ATWRT,'Not Set');


 update tmp_dim_batch bt
set bt.productionpaperreview = ifnull(AUSP_ATWRT,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 79
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.productionpaperreview <> ifnull(AUSP_ATWRT,0);


 update tmp_dim_batch bt
set bt.activityfactorkgdka = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 243
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.activityfactorkgdka <> ifnull(AUSP_ATFLV,0);


 update tmp_dim_batch bt
set bt.exponentialvaluetiterrange = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 253
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.exponentialvaluetiterrange <> ifnull(AUSP_ATWRT,'Not Set');


 update tmp_dim_batch bt
set bt.formtiterstdval2ecompml = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 254
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.formtiterstdval2ecompml <> ifnull(AUSP_ATFLV,0);


 update tmp_dim_batch bt
set bt.qctiterforstdnum2ecomp = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 255
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.qctiterforstdnum2ecomp <> CAST(AUSP_ATFLV AS decimal (35,16));


 update tmp_dim_batch bt
set bt.activityfactlfpe2ecomp = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 256
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.activityfactlfpe2ecomp <> ifnull(AUSP_ATFLV,0);

 update tmp_dim_batch bt
set bt.activityfactkgfpe2ecomp = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 257
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.activityfactkgfpe2ecomp <> ifnull(AUSP_ATFLV,0);

 update tmp_dim_batch bt
set bt.formulationtiterexpvalml = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 264
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.formulationtiterexpvalml <> CAST(AUSP_ATFLV AS decimal (35,16));


 update tmp_dim_batch bt
set bt.qctiterforexpnumeric = CAST(AUSP_ATFLV AS decimal (35,16)),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 265
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.qctiterforexpnumeric <> CAST(AUSP_ATFLV AS decimal (35,16));



 update tmp_dim_batch bt
set bt.concentrationinmgmlwv = ifnull(AUSP_ATFLV,0),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 272
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.concentrationinmgmlwv <> ifnull(AUSP_ATFLV,0);


 update tmp_dim_batch bt
set bt.batchquality = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 496
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.batchquality <> ifnull(AUSP_ATWRT,'Not Set');

drop table if exists AUSP_BATCH_544;
create table AUSP_BATCH_544
as 
SELECT AUSP_OBJEK,MAX(AUSP_ATWRT) AUSP_ATWRT FROM AUSP_BATCH
WHERE AUSP_KLART = 23
and AUSP_ATINN = 544
group by AUSP_OBJEK;

 update tmp_dim_batch bt
set bt.changenumber = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM AUSP_BATCH_544 a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.changenumber <> ifnull(AUSP_ATWRT,'Not Set');

drop table if exists AUSP_BATCH_544;

 update tmp_dim_batch bt
set bt.changestatus = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 545
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.changestatus <> ifnull(AUSP_ATWRT,'Not Set');

update tmp_dim_batch bt
set bt.nextinspdateforbatch = 
CASE WHEN (AUSP_ATFLV > 00010101 AND AUSP_ATFLV < 99991231) THEN
CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2))
ELSE '0001-01-01' END
,
dw_update_date = current_timestamp
FROM tmp_AUSP_BATCH a, INOB_BB b, tmp_dim_batch bt
where b.INOB_CUOBJ= a.AUSP_OBJEK
and AUSP_KLART = 23
and AUSP_ATINN = 9999999409
and b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber
and bt.nextinspdateforbatch <>
CASE WHEN (AUSP_ATFLV > 00010101 AND AUSP_ATFLV < 99991231) THEN
CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2))
ELSE '0001-01-01' END;

/* Ovidiu: Every Angle changes */

/* Octavian: Every Angle changes */
 update tmp_dim_batch bt
SET creator = ifnull(MCH1_ERNAM,'Not Set'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and creator <> ifnull(MCH1_ERNAM,'Not Set'); 

 update tmp_dim_batch bt
SET creationdate = ifnull(MCH1_ERSDA,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and creationdate <> ifnull(MCH1_ERSDA,'0001-01-01');

/* APP - 7424 */ 
update tmp_dim_batch bt
SET CREATIONDATEMCH1 = ifnull(MCH1_ERSDA,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and CREATIONDATEMCH1 <> ifnull(MCH1_ERSDA,'0001-01-01');
 update tmp_dim_batch bt
SET expirydate = ifnull(MCH1_VFDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and expirydate <> ifnull(MCH1_VFDAT,'0001-01-01');
/* END APP - 7424 */ 

 update tmp_dim_batch bt
SET nextinspectiondate = ifnull(MCH1_QNDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and nextinspectiondate <> ifnull(MCH1_QNDAT,'0001-01-01');

 update tmp_dim_batch bt
SET laststatuschangedate = ifnull(MCH1_ZAEDT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and laststatuschangedate <> ifnull(MCH1_ZAEDT,'0001-01-01');
/* Octavian: Every Angle changes */

/*Georgiana EA Addons 03 Feb 2016*/

 update tmp_dim_batch bt
SET creationdate = ifnull(MCHA_ERSDA,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCHA,tmp_dim_batch bt
where partnumber = ifnull(MCHA_MATNR,'Not Set')
and batchnumber = ifnull(MCHA_CHARG,'Not Set')
and plantcode = ifnull(MCHA_WERKS, 'Not Set')
and creationdate <> ifnull(MCHA_ERSDA,'0001-01-01');

 update tmp_dim_batch bt
SET bestbeforedate_mcha = ifnull(MCHA_VFDAT,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCHA,tmp_dim_batch bt
where partnumber = ifnull(MCHA_MATNR,'Not Set')
and batchnumber = ifnull(MCHA_CHARG,'Not Set')
and plantcode = ifnull(MCHA_WERKS, 'Not Set')
and bestbeforedate_mcha <> ifnull(MCHA_VFDAT,'0001-01-01');

 update tmp_dim_batch bt
SET dateoflastchange = ifnull(MCHA_LAEDA,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCHA,tmp_dim_batch bt

where partnumber = ifnull(MCHA_MATNR,'Not Set')
and batchnumber = ifnull(MCHA_CHARG,'Not Set')
and plantcode = ifnull(MCHA_WERKS, 'Not Set')
and dateoflastchange <> ifnull(MCHA_LAEDA,'0001-01-01');

/* Octavian: Every Angle: Adding characteristic 51 which means multiplying lines as there could be multiple
for one single batch number */


/*Alin APP-7834 13 Nov*/
 update tmp_dim_batch bt
SET dateforunrestrickteduse_FVDT2 = ifnull(MCH1_FVDT2,'0001-01-01'),
dw_update_date = current_timestamp
FROM MCH1,tmp_dim_batch bt
where partnumber = ifnull(MCH1_MATNR,'Not Set')
and batchnumber = ifnull(MCH1_CHARG,'Not Set')
and dateforunrestrickteduse_FVDT2 <> ifnull(MCH1_FVDT2,'0001-01-01');

drop table if exists distinct_dim_batch;
create table distinct_dim_batch
as select distinct dim_batchid,partnumber,batchnumber,plantcode from tmp_dim_batch bt;


drop table if exists dim_batch_51;
create table dim_batch_51 as
select bt.*,
ifnull(concat(c.cawnt_atwtb,' - ',c.cawn_atwrt),'') regulatedgroupsbio,ifnull(a.ausp_atzhl,0) counter51
from INOB_BB b 
INNER JOIN AUSP_BATCH a ON b.INOB_CUOBJ= a.AUSP_OBJEK and a.AUSP_KLART = 23 and a.AUSP_ATINN = 51  
INNER JOIN CAWNT c ON a.AUSP_ATINN = c.CAWN_ATINN and a.AUSP_ATWRT = c.CAWN_ATWRT
RIGHT OUTER JOIN distinct_dim_batch bt ON b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber;

delete from dim_batchregulatedgroupsbio;

insert into dim_batchregulatedgroupsbio
(dim_batchregulatedgroupsbioid,
regulatedgroupsbio,
counter51)
select distinct dim_batchid,
regulatedgroupsbio,counter51 from dim_batch_51 s
where not exists 
(select 1 from dim_batchregulatedgroupsbio b
where ifnull(s.dim_batchid, 1) = ifnull(b.dim_batchregulatedgroupsbioid, 1)
and s.counter51 = b.counter51
and s.regulatedgroupsbio = b.regulatedgroupsbio);


drop table if exists dim_batch_notset;
create table dim_batch_notset as
select dim_batchid from dim_batch
group by dim_batchid 
having count(*) > 1;

delete from dim_batchregulatedgroupsbio b where b.dim_batchregulatedgroupsbioid in (select n.dim_batchid from dim_batch_notset n) and b.counter51 = 0; 
drop table if exists dim_batch_notset;
drop table if exists dim_batch_51;
drop table if exists distinct_dim_batch;

/* Octavian: Every Angle: Adding characteristic 51 which means multiplying lines as there could be multiple
for one single batch number */

/* Yogini BI-5639 New Dimension 'Batch Regulated Groups Pharma' */

drop table if exists distinct_dim_batch_new;
create table distinct_dim_batch_new
as select distinct dim_batchid,partnumber,batchnumber,plantcode from tmp_dim_batch bt;


drop table if exists dim_batch_52;
create table dim_batch_52 as
select bt.*,
ifnull(concat(c.cawnt_atwtb,' - ',c.cawn_atwrt),'') regulatedgroupspharma,ifnull(a.ausp_atzhl,0) counter52
from INOB_BB b 
INNER JOIN AUSP_BATCH a ON b.INOB_CUOBJ= a.AUSP_OBJEK and a.AUSP_KLART = 23 and a.AUSP_ATINN = 52  
INNER JOIN CAWNT c ON a.AUSP_ATINN = c.CAWN_ATINN and a.AUSP_ATWRT = c.CAWN_ATWRT
RIGHT OUTER JOIN distinct_dim_batch_new bt ON b.INOB_MATNR = bt.partnumber and b.INOB_CHARG = bt.batchnumber;

delete from dim_batchregulatedgroupspharma;

insert into dim_batchregulatedgroupspharma
(dim_batchregulatedgroupspharmaid,
regulatedgroupspharma,
counter52)
select distinct dim_batchid,
regulatedgroupspharma,counter52 from dim_batch_52 s
where not exists 
(select 1 from dim_batchregulatedgroupspharma b
where ifnull(s.dim_batchid, 1) = ifnull(b.dim_batchregulatedgroupspharmaid, 1)
and s.counter52 = b.counter52
and s.regulatedgroupspharma = b.regulatedgroupspharma);


drop table if exists dim_batch_notset_new;
create table dim_batch_notset_new as
select dim_batchid from dim_batch
group by dim_batchid 
having count(*) > 1;

delete from dim_batchregulatedgroupspharma b 
where b.dim_batchregulatedgroupspharmaid in (select n.dim_batchid from dim_batch_notset_new n) and b.counter52 = 0; 

drop table if exists dim_batch_notset_new;
drop table if exists dim_batch_52;
drop table if exists distinct_dim_batch_new;

/* END Yogini BI-5639 New Dimension 'Batch Regulated Groups Pharma' */

/* Andrei - APP-10671 */
drop table if exists distinct_dim_batch_2843;
create table distinct_dim_batch_2843
as select distinct dim_batchid,partnumber,batchnumber from tmp_dim_batch bt;

drop table if exists dim_batch_2843;
create table dim_batch_2843 as
select bt.*,
ifnull(a.AUSP_ATWRT,'Not Set') deviationstsno   /* ,ifnull(a.ausp_atzhl,0) counter52 */
from MCH1 m
INNER JOIN AUSP_BATCH a ON m.MCH1_CUOBJ_BM= a.AUSP_OBJEK and a.AUSP_KLART = 23 and a.AUSP_ATINN = 2843 
 /*INNER */  RIGHT OUTER JOIN distinct_dim_batch_2843 bt ON m.MCH1_MATNR = bt.partnumber and m.MCH1_CHARG = bt.batchnumber;

delete from dim_batchdeviationnumber;

insert into dim_batchdeviationnumber
(dim_batchdeviationnumberid,
deviationstsno /* ,
counter52 */)
select distinct dim_batchid,
deviationstsno /*,counter52 */ from dim_batch_2843 s
where not exists 
(select 1 from dim_batchdeviationnumber b
where ifnull(s.dim_batchid, 1) = ifnull(b.dim_batchdeviationnumberid, 1)
/* and s.counter52 = b.counter52 */
and s.deviationstsno = b.deviationstsno);

drop table if exists tmp_AUSP_BATCH_2843;
create table tmp_AUSP_BATCH_2843 as select distinct AUSP_OBJEK,AUSP_KLART,AUSP_ATINN,AUSP_ATFLV,AUSP_ATWRT ,
AUSP_ATZHL from AUSP_BATCH;

update tmp_dim_batch bt
set bt.deviationstsdesc = ifnull(c.CAWNT_ATWTB,'Not Set')
from tmp_dim_batch bt, tmp_AUSP_BATCH_2843 a, CAWNT c, MCH1 m
where  a.AUSP_KLART = 23
and a.AUSP_ATINN = 2809
and a.AUSP_ATINN = c.CAWN_ATINN 
and a.AUSP_ATZHL = c.CAWN_ATZHL
and m.MCH1_CUOBJ_BM= a.AUSP_OBJEK
and ifnull(m.MCH1_MATNR, 'Not Set') = bt.partnumber 
and ifnull(m.MCH1_CHARG, 'Not Set') = bt.batchnumber
and bt.deviationstsdesc <> ifnull(c.CAWNT_ATWTB,'Not Set');

/* end Andrei - APP-10671 */

drop table if exists dim_batch;
rename table tmp_dim_batch to dim_batch;

/* Update facts before the processing hit the fact scripts for current data since batches can be reassigned */

MERGE INTO fact_resultsrecording fact
USING (SELECT 
		f_rr.fact_resultsrecordingid, ifnull(b.dim_batchid,1) as dim_batchid
		FROM fact_resultsrecording f_rr 
			INNER JOIN dim_part dp ON ifnull(f_rr.dim_partid, 1) = ifnull(dp.dim_partid, 1)
         	LEFT JOIN dim_batch b ON ifnull(f_rr.dd_batchno, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
		 		ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set') 
		WHERE
		f_rr.dim_batchid <> ifnull(b.dim_batchid,1)) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dim_batchid = src.dim_batchid,
dw_update_date = CURRENT_TIMESTAMP;


/* Madalina 19 Aug 2016 - Reset dim_batchids in SAs where the link with dim_batch doesn't exist anymore- BI-3822 */
/* Inspection lot */
merge into fact_inspectionlot ia
using ( select
		ia.fact_inspectionlotid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_inspectionlot ia
			inner join dim_part dp ON ifnull(ia.dim_partid, 1) = ifnull(dp.dim_partid, 1)
			left join dim_batch b on ifnull(ia.dd_batchno, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
				ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set') 
		where
			ia.dim_batchid <> ifnull(b.dim_batchid,1)) src
on ia.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE
set ia.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;

/* Inventory */
merge into fact_inventoryaging fi
using ( select	
		fi.fact_inventoryagingid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_inventoryaging fi
			inner join dim_part dp ON ifnull(fi.dim_partid, 1) = ifnull(dp.dim_partid, 1)
			left join dim_batch b on ifnull(fi.dd_batchno, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
				ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set') 
		where
			fi.dim_batchid <> ifnull(b.dim_batchid,1)) src
on fi.fact_inventoryagingid = src.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
set fi.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;
	
/* Inventory History */
merge into fact_inventoryhistory f_ih
using (select	
		f_ih.fact_inventoryhistoryid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_inventoryhistory f_ih
			     inner join dim_part dp ON ifnull(f_ih.dim_partid, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(f_ih.dd_batchno, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
		where f_ih.dim_batchid <> ifnull(b.dim_batchid,1)
		and f_ih.SnapshotDate = CURRENT_DATE) src
on f_ih.fact_inventoryhistoryid = src.fact_inventoryhistoryid
WHEN MATCHED THEN UPDATE
set f_ih.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;
	
/* Material movement */
merge into fact_materialmovement mm
using (select
		mm.fact_materialmovementid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_materialmovement mm
			    inner join dim_part dp ON ifnull(mm.dim_partid, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(mm.dd_batchnumber, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
			where mm.dim_batchid <> ifnull(b.dim_batchid,1)) src
on mm.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
set mm.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;

/* Production order */
merge into fact_productionorder po 
using (select
		po.fact_productionorderid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_productionorder po
			     inner join dim_part dp ON ifnull(po.dim_partidheader, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(po.dd_batch, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
			where po.dim_batchid <> ifnull(b.dim_batchid,1)) src
on po.fact_productionorderid = src.fact_productionorderid
WHEN MATCHED THEN UPDATE			
set po.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;	
	
/* Production order snapshot */
merge into fact_productionorder_snapshot s
using (select
		s.fact_productionorder_snapshotid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_productionorder_snapshot s
			     inner join dim_part dp ON ifnull(s.dim_partidheader, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(s.dd_batch, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
			where s.dim_batchid <> ifnull(b.dim_batchid,1)) src
on s.fact_productionorder_snapshotid = src.fact_productionorder_snapshotid
WHEN MATCHED THEN UPDATE			
set s.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;	
	
/* Purchasing */
merge into fact_purchase fact	
using (select
		fact.fact_purchaseid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_purchase fact	
			     inner join dim_part dp ON ifnull(fact.dim_partid, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(fact.dd_batch, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
			where fact.dim_batchid <> ifnull(b.dim_batchid,1)) src
on fact.fact_purchaseid = src.fact_purchaseid
WHEN MATCHED THEN UPDATE			
set fact.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;

/* 	Quality notification */
merge into fact_qualitynotification qn
using (select
		qn.fact_qualitynotificationid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_qualitynotification qn
			     inner join dim_part dp ON ifnull(qn.dim_partid, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(qn.dd_batchno, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
			where qn.dim_batchid <> ifnull(b.dim_batchid,1)) src
on qn.fact_qualitynotificationid = src.fact_qualitynotificationid
WHEN MATCHED THEN UPDATE			
set qn.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;
	
/* Sales */
merge into fact_salesorder so	
using (select
		so.fact_salesorderid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_salesorder so
			     inner join dim_part dp ON ifnull(so.dim_partid, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(so.dd_batchno, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
			where so.dim_batchid <> ifnull(b.dim_batchid,1)) src
on so.fact_salesorderid = src.fact_salesorderid
WHEN MATCHED THEN UPDATE			
set so.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;

/* Shipping */
merge into fact_salesorderdelivery sod
using (select
		sod.fact_salesorderdeliveryid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_salesorderdelivery sod
			     inner join dim_part dp ON ifnull(sod.dim_partid, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(sod.dd_batch, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
				and dp.partnumber = b.partnumber and b.plantcode = dp.plant
			where sod.dim_batchid <> ifnull(b.dim_batchid,1)) src
on sod.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
WHEN MATCHED THEN UPDATE			
set sod.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;
	
/* Warehouse Quants */
merge into fact_wmquant fwmq
using (select
		fwmq.fact_wmquantid, ifnull(b.dim_batchid,1) as dim_batchid
		from fact_wmquant fwmq
			     inner join dim_part dp ON ifnull(fwmq.dim_partid, 1) = ifnull(dp.dim_partid, 1)
      left join dim_batch b on ifnull(fwmq.dd_batchnumber, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
        ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set') and ifnull(b.plantcode, 'Not Set') = ifnull(dp.plant, 'Not Set')
			where fwmq.dim_batchid <> ifnull(b.dim_batchid,1)) src
on fwmq.fact_wmquantid = src.fact_wmquantid
WHEN MATCHED THEN UPDATE			
set fwmq.dim_batchid = src.dim_batchid,
	dw_update_date = CURRENT_TIMESTAMP;
	
	/*Octavian S 15-FEB-2018 APP-6837 Add new attribute*/
 --Short text
 MERGE INTO dim_batch b
 USING ( 
        SELECT DISTINCT b.partnumber,b.batchnumber, s.shorttext
         FROM (SELECT DISTINCT stxh_tdname, 
                      group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                 FROM  stxh s 
                WHERE stxh_tdobject LIKE '%CHARGE%' 
                  AND stxh_tdid     = 'VERM'
                GROUP BY stxh_tdname) s
                ,dim_batch            b
        WHERE TRIM(substr(stxh_tdname,1,9))    = b.partnumber 
          AND TRIM( substr(stxh_tdname,10,30)) = b.batchnumber
       )t
   ON t.batchnumber = b.batchnumber 
  AND t.partnumber  = b.partnumber
 WHEN MATCHED 
 THEN UPDATE SET Shorttext_stxh = IFNULL(t.shorttext,'Not Set'); 
  