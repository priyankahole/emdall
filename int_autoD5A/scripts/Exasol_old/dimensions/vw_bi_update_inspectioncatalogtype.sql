UPDATE    dim_inspectioncatalogtype  
   SET inspectioncatalogtypename = TQ15T_katalogtxt,
			dw_update_date = current_timestamp
FROM TQ15T, dim_inspectioncatalogtype
   WHERE inspectioncatalogtypecode = tq15t_katalogart
        and inspectioncatalogtypekey = tq15t_schlagwort
        and RowIsCurrent = 1;

