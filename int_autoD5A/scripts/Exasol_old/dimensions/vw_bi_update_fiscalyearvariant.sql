UPDATE    dim_fiscalyearvariant fp

   SET fp.FiscalYearVariantDescription = t.T009T_LTEXT
       FROM
          T009T t, dim_fiscalyearvariant fp
   WHERE fp.FiscalYearVariantCode = t.T009T_PERIV AND fp.RowIsCurrent = 1;
