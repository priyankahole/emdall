
INSERT INTO dim_costcenter(dim_costcenterId, RowIsCurrent,ValidTo)
SELECT 1, 1, '0001-01-01'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_costcenter
               WHERE dim_costcenterId = 1);
			   
/*UPDATE dim_costcenter cc
   SET cc.Name = ifnull(CSKT_KTEXT, 'Not Set'),
			cc.dw_update_date = current_timestamp
FROM CSKT ck, dim_costcenter cc
 WHERE cc.RowIsCurrent = 1   
 AND  ck.CSKT_KOSTL = cc.Code
          AND cc.ControllingArea = ifnull(CSKT_KOKRS, 'Not Set')
          AND cc.ValidTo = CSKT_DATBI */


merge into dim_costcenter cc
using
(select distinct cc.RowIsCurrent, cc.ControllingArea, cc.ValidTo, cc.Name, ck.CSKT_KTEXT, cc.Code
FROM CSKT ck, dim_costcenter cc
WHERE cc.RowIsCurrent = 1 
 AND ck.CSKT_KOSTL = cc.Code
 AND cc.ControllingArea = ifnull(CSKT_KOKRS, 'Not Set')
 AND cc.ValidTo = CSKT_DATBI
group by cc.RowIsCurrent, cc.ControllingArea, cc.ValidTo, cc.Name, ck.CSKT_KTEXT, cc.Code
)ck
on (
ck.Code = cc.Code and
ck.ControllingArea = cc.ControllingArea and
ck.ValidTo = cc.ValidTo
)
when matched then update set cc.Name = ifnull(ck.CSKT_KTEXT, 'Not Set'),
cc.dw_update_date = current_timestamp
where cc.Name <> ifnull(ck.CSKT_KTEXT, 'Not Set');


		  
delete from number_fountain m where m.table_name = 'dim_costcenter';

insert into number_fountain
select 	'dim_costcenter',
	ifnull(max(d.dim_costcenterId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_costcenter d
where d.dim_costcenterId <> 1;

INSERT INTO dim_costcenter(dim_costcenterid,
			Code,
			  Name,
			  ControllingArea,
			  ValidTo,
                          RowStartDate,
                          RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_costcenter') 
          + row_number() over(order by ''),
			CSKT_KOSTL,
	    ifnull(CSKT_KTEXT, 'Not Set'),
	    ifnull(CSKT_KOKRS, 'Not Set'),
	    CSKT_DATBI,
            current_timestamp,
            1
       FROM CSKT
      WHERE CSKT_KOSTL IS NOT NULL
	    AND NOT EXISTS
                  (SELECT 1
                     FROM dim_costcenter
                    WHERE Code = CSKT_KOSTL
                    AND ControllingArea = CSKT_KOKRS
                    AND ValidTo = CSKT_DATBI);
					
delete from number_fountain m where m.table_name = 'dim_costcenter';

/*update dim_costcenter dc
SET dc.Name = ifnull(c.CSKT_KTEXT, 'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM CSKT c, dim_costcenter dc
WHERE dc.Code = c.CSKT_KOSTL
AND dc.ControllingArea = c.CSKT_KOKRS
AND dc.ValidTo = c.CSKT_DATBI
AND dc.Name <> ifnull(c.CSKT_KTEXT, 'Not Set') */



merge into dim_costcenter dc
using
(select distinct dc.Code, dc.ControllingArea, dc.ValidTo , dc.Name, c.CSKT_KTEXT
FROM CSKT c, dim_costcenter dc
WHERE dc.Code = c.CSKT_KOSTL
 AND dc.ControllingArea = c.CSKT_KOKRS
 AND dc.ValidTo = c.CSKT_DATBI
 AND dc.Name <> ifnull(c.CSKT_KTEXT, 'Not Set')
group by dc.Code, dc.ControllingArea, dc.ValidTo , dc.Name, c.CSKT_KTEXT
)c
on (
c.Code = dc.Code and
c.ControllingArea = dc.ControllingArea and
c.ValidTo = dc.ValidTo
)
when matched then update set dc.Name = ifnull(c.CSKT_KTEXT, 'Not Set'),
dc.dw_update_date = current_timestamp
where dc.Name <> ifnull(c.CSKT_KTEXT, 'Not Set');

/*
update dim_costcenter dc
SET dc.description = ifnull(c.CSKT_LTEXT, 'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM CSKT c, dim_costcenter dc
WHERE dc.Code = c.CSKT_KOSTL
AND dc.ControllingArea = c.CSKT_KOKRS
AND dc.ValidTo = c.CSKT_DATBI
AND dc.description <> ifnull(c.CSKT_LTEXT, 'Not Set') */


merge into dim_costcenter dc
using
(select distinct dc.Code, dc.ControllingArea, dc.ValidTo , dc.description, max(ifnull(c.CSKT_LTEXT,'Not Set')) as descriptionn
FROM CSKT c, dim_costcenter dc
WHERE dc.Code = c.CSKT_KOSTL
 AND dc.ControllingArea = c.CSKT_KOKRS
 AND dc.ValidTo = c.CSKT_DATBI
 AND dc.description <> ifnull(c.CSKT_LTEXT, 'Not Set')
group by dc.Code, dc.ControllingArea, dc.ValidTo , dc.description
)c
on (
c.Code = dc.Code and
c.ControllingArea = dc.ControllingArea and
c.ValidTo = dc.ValidTo
)
when matched then update set dc.description = c.descriptionn,
dc.dw_update_date = current_timestamp
where dc.description <> c.descriptionn;

/*
update dim_costcenter dc
SET dc.searchterm = ifnull(c.CSKT_MCTXT, 'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM CSKT c, dim_costcenter dc
WHERE dc.Code = c.CSKT_KOSTL
AND dc.ControllingArea = c.CSKT_KOKRS
AND dc.ValidTo = c.CSKT_DATBI
AND dc.searchterm <> ifnull(c.CSKT_MCTXT, 'Not Set') */




merge into dim_costcenter dc
using
(select distinct dc.Code, dc.ControllingArea, dc.ValidTo , dc.searchterm, c.CSKT_MCTXT
FROM CSKT c, dim_costcenter dc
WHERE dc.Code = c.CSKT_KOSTL
 AND dc.ControllingArea = c.CSKT_KOKRS
 AND dc.ValidTo = c.CSKT_DATBI
 AND dc.searchterm <> ifnull(c.CSKT_MCTXT, 'Not Set')
group by dc.Code, dc.ControllingArea, dc.ValidTo , dc.searchterm, c.CSKT_MCTXT
)c
on (
c.Code = dc.Code and
c.ControllingArea = dc.ControllingArea and
c.ValidTo = dc.ValidTo
)
when matched then update set dc.searchterm = ifnull(c.CSKT_MCTXT, 'Not Set'),
dc.dw_update_date = current_timestamp
where dc.searchterm <> ifnull(c.CSKT_MCTXT, 'Not Set');


update dim_costcenter dc
SET dc.companycode = ifnull(c.CSKS_BUKRS, 'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM CSKS c, dim_costcenter dc
WHERE dc.Code = c.CSKS_KOSTL
AND dc.ControllingArea = c.CSKS_KOKRS
AND dc.ValidTo = c.CSKS_DATBI
AND dc.companycode <> ifnull(c.CSKS_BUKRS, 'Not Set');

update dim_costcenter dc
SET dc.costcentercategory = ifnull(c.CSKS_KOSAR, 'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM CSKS c, dim_costcenter dc
WHERE dc.Code = c.CSKS_KOSTL
AND dc.ControllingArea = c.CSKS_KOKRS
AND dc.ValidTo = c.CSKS_DATBI
AND dc.costcentercategory <> ifnull(c.CSKS_KOSAR, 'Not Set');
