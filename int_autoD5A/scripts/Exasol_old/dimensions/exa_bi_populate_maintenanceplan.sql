DROP TABLE IF EXISTS tmp_dim_maintenanceplan;

CREATE TABLE tmp_dim_maintenanceplan
LIKE dim_maintenanceplan INCLUDING DEFAULTS INCLUDING IDENTITY;

insert into tmp_dim_maintenanceplan
(dim_maintenanceplanid,
maintenanceplan
)
select 
1, 'Not Set'
from (select 1) a
where not exists ( select 'x' from tmp_dim_maintenanceplan where dim_maintenanceplanid = 1);

delete from number_fountain m where m.table_name = 'tmp_dim_maintenanceplan';
insert into number_fountain
select 'tmp_dim_maintenanceplan',
 ifnull(max(d.dim_maintenanceplanid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_dim_maintenanceplan d
where d.dim_maintenanceplanid <> 1;


insert into tmp_dim_maintenanceplan
(dim_maintenanceplanid,
maintenanceplan,
maintenanceplantext,
maintenancestrategy,
schedulingperiod,
callhorizon,
callnumber)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'tmp_dim_maintenanceplan') + row_number() over(order by '') AS dim_maintenanceplanid,
 ifnull(MPLA_WARPL, 'Not Set') as maintenanceplan,
 ifnull(MPLA_WPTXT,'Not Set') as maintenanceplantext,
 ifnull(MPLA_STRAT,'Not Set') as maintenancestrategy,
 ifnull(MPLA_ABRHO, 'Not Set') as schedulingperiod,
 ifnull(MPLA_HORIZ_DAYS, 'Not Set') as callhorizon,
 ifnull(MPLA_ABNUM,'Not Set') as callnumber
FROM 
MPLA q
where not exists ( select 'x' from  tmp_dim_maintenanceplan
                    where  ifnull(MPLA_WARPL, 'Not Set') = maintenanceplan);


					
update tmp_dim_maintenanceplan d
set d.maintenanceplantext=ifnull(MPLA_WPTXT,'Not Set')
from MPLA m ,tmp_dim_maintenanceplan d
where ifnull(MPLA_WARPL, 'Not Set') = maintenanceplan
and d.maintenanceplantext <> ifnull(MPLA_WPTXT,'Not Set');

update tmp_dim_maintenanceplan d
set d.maintenancestrategy=ifnull(MPLA_STRAT,'Not Set')
from MPLA m ,tmp_dim_maintenanceplan d
where ifnull(MPLA_WARPL, 'Not Set') = maintenanceplan
and d.maintenancestrategy <> ifnull(MPLA_STRAT,'Not Set');

update tmp_dim_maintenanceplan d
set d.schedulingperiod=ifnull(MPLA_ABRHO,'Not Set')
from MPLA m ,tmp_dim_maintenanceplan d
where ifnull(MPLA_WARPL, 'Not Set') = maintenanceplan
and d.schedulingperiod <> ifnull(MPLA_ABRHO,'Not Set');

update tmp_dim_maintenanceplan d
set d.callhorizon=ifnull(MPLA_HORIZ_DAYS,'Not Set')
from MPLA m ,tmp_dim_maintenanceplan d
where ifnull(MPLA_WARPL, 'Not Set') = maintenanceplan
and d.callhorizon <> ifnull(MPLA_HORIZ_DAYS,'Not Set');

update tmp_dim_maintenanceplan d
set d.callnumber=ifnull(MPLA_ABNUM,'Not Set')
from MPLA m ,tmp_dim_maintenanceplan d
where ifnull(MPLA_WARPL, 'Not Set') = maintenanceplan
and d.callnumber <> ifnull(MPLA_ABNUM,'Not Set');

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'tmp_dim_maintenanceplan';        

DROP TABLE if EXISTS dim_maintenanceplan;
RENAME tmp_dim_maintenanceplan to dim_maintenanceplan;