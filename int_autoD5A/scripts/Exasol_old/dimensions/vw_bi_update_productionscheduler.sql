UPDATE    dim_productionscheduler ps

   SET ps.Description = ifnull(T024F_TXT, 'Not Set')
       FROM
          T024F t, dim_productionscheduler ps
 WHERE ps.RowIsCurrent = 1
      AND ps.ProductionScheduler = t.T024F_FEVOR AND ps.plant = t.T024F_WERKS 
;
