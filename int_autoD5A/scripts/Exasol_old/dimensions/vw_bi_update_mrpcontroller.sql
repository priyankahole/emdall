update dim_mrpcontroller dm
set dm.Description = ifnull(ds.T024D_DSNAM, 'Not Set'),
    dm.TelephoneNo = ifnull(ds.T024D_DSTEL,'Not Set'),
			dm.dw_update_date = current_timestamp
 from dim_mrpcontroller dm, T024D ds 
where     dm.MRPController = ds.T024D_DISPO
      AND dm.Plant         = ds.T024D_WERKS
      AND RowIsCurrent = 1
	  AND (   ifnull(dm.Description,'NS') <> ifnull(ds.T024D_DSNAM, 'Not Set')
           OR ifnull(dm.TelephoneNo,'NS') <> ifnull(ds.T024D_DSTEL, 'Not Set'));

