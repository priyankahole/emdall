UPDATE    dim_incoterm it

   SET it.Name = t.TINCT_BEZEI
       FROM
          tinct t,  dim_incoterm it
WHERE it.RowIsCurrent = 1
AND it.IncoTermCode = t.TINCT_INCO1;

