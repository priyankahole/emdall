/* ##################################################################################################################

   Script         : vw_bi_populate_dim_date_flags_sap.sql 
   Author         : Victor Criclivii
   Created On     : 22 12 2015


   Description    : Dim Date Flags

   Change History
   Date            By        	Version           Desc
   22 12 2015     Victor         1.0              Dim Date Flags
     
#################################################################################################################### */

/*currentperiodname*/
UPDATE dim_date SET currentperiodname = 'Not Set';

UPDATE dim_date d
SET d.currentperiodname = t.monthyear
from dim_date t,dim_date d
WHERE TO_DATE(d.datevalue,'YYYY-MM') = TO_DATE(current_Date,'YYYY-MM')
AND cast(t.datevalue as date) = cast(Current_Date as date) AND d.companycode = t.companycode
AND d.plantcode_factory = t.plantcode_factory;


/*fiscalyearstartflag*/
/*SELECT COUNT(*),companycode FROM dim_date WHERE fiscalyearstartflag ='Y' GROUP BY companycode*/
UPDATE dim_date SET fiscalyearstartflag ='N';

DROP TABLE IF EXISTS dim_date_fiscalyearstartflag_tmp;

CREATE TABLE dim_date_fiscalyearstartflag_tmp AS
SELECT MAX(d.datevalue) datevalue, d.financialweek, d.monthyear, d.financialyear, d.companycode, d.plantcode_factory
FROM dim_date d, (SELECT MIN(t.financialweek) financialweek, t.companycode FROM dim_date t GROUP BY t.companycode ) t
WHERE d.financialweek = t.financialweek
  AND t.companycode = d.companycode
GROUP BY d.financialweek, d.monthyear, d.financialyear, d.companycode, d.plantcode_factory;

/*
UPDATE dim_date d
SET d.fiscalyearstartflag = 'Y'
FROM dim_date_fiscalyearstartflag_tmp tmp ,dim_date d
WHERE d.datevalue = tmp.datevalue
  AND d.companycode = tmp.companycode
  AND d.plantcode_factory = tmp.plantcode_factory
  */
  
  merge into dim_date d
using(select distinct d.dim_dateid
from dim_date d,dim_date_fiscalyearstartflag_tmp tmp
WHERE d.datevalue = tmp.datevalue
  AND d.companycode = tmp.companycode
  AND d.plantcode_factory = tmp.plantcode_factory)t
on t.dim_dateid = d.dim_dateid
when matched then update set d.fiscalyearstartflag = 'Y'
where d.fiscalyearstartflag <> 'Y';

DROP TABLE IF EXISTS dim_date_fiscalyearstartflag_tmp;

/*fiscalyearendflag*/
/*SELECT COUNT(*),companycode FROM dim_date WHERE fiscalyearendflag ='Y'  GROUP BY companycode*/

UPDATE dim_date SET fiscalyearendflag ='N';

DROP TABLE IF EXISTS dim_date_fiscalyearendflag_tmp;

CREATE TABLE dim_date_fiscalyearendflag_tmp AS
SELECT MAX(d.datevalue) datevalue, d.financialweek, d.monthyear, d.financialyear, d.companycode
FROM dim_date d, (SELECT MAX(t.financialweek) financialweek, t.companycode FROM dim_date t GROUP BY t.companycode ) t
WHERE d.financialweek = t.financialweek
  AND t.companycode = d.companycode
GROUP BY d.financialweek, d.monthyear, d.financialyear, d.companycode;

		          
UPDATE dim_date d
SET d.fiscalyearendflag = 'Y'
FROM dim_date_fiscalyearendflag_tmp tmp ,dim_date d
WHERE d.datevalue = tmp.datevalue
 AND d.companycode = tmp.companycode;

DROP TABLE IF EXISTS dim_date_fiscalyearendflag_tmp;

/*dim_date_tmp*/
DROP TABLE IF EXISTS dim_date_tmp;

CREATE TABLE dim_date_tmp AS 
SELECT dim_dateid, datevalue, financialyear, financialquarter, monthyear, financialweek ,companycode,plantcode_factory,
LAST_VALUE(financialweek ) OVER(PARTITION BY financialyear, financialquarter,monthyear 
ORDER BY financialweek ASC ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) financialweek_m
FROM dim_date
ORDER BY datevalue,financialweek;

/*dim_date_rank_tmp*/
DROP TABLE IF EXISTS dim_date_rank_tmp;

CREATE TABLE dim_date_rank_tmp AS
SELECT 
dim_dateid, datevalue,
financialyear, financialquarter, monthyear, financialweek ,financialweek_m,companycode,plantcode_factory,
DENSE_RANK() over(ORDER BY financialyear ASC ) RANK_YEAR,
DENSE_RANK() over(ORDER BY financialyear ASC, financialquarter ASC) RANK_quarter,
DENSE_RANK() over(ORDER BY financialyear ASC, financialweek_m ASC) RANK_month,
DENSE_RANK() over(ORDER BY financialyear ASC, financialweek ASC) RANK_week
FROM dim_date_tmp
ORDER BY datevalue;

/*yearflag*/
/*SELECT DISTINCT yearflag  FROM dim_date WHERE yearflag in ('Current','Previous','Next')*/
UPDATE dim_date SET yearflag ='Not Set';

UPDATE dim_date d
SET d.yearflag = 'Current'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_YEAR, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory
AND t1.RANK_YEAR = t2.RANK_YEAR
AND t1.companycode = t2.companycode;

UPDATE dim_date d
SET d.yearflag = 'Previous'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_YEAR, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory
AND t1.RANK_YEAR + 1 = t2.RANK_YEAR
AND t1.companycode = t2.companycode;

UPDATE dim_date d
SET d.yearflag = 'Next'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_YEAR, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory
AND t1.RANK_YEAR - 1 = t2.RANK_YEAR
AND t1.companycode = t2.companycode;
 
/*quarterflag*/
/*SELECT DISTINCT quarterflag  FROM dim_date*/
UPDATE dim_date
SET quarterflag = 'Not Set';

UPDATE dim_date d
SET d.quarterflag = 'Current'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_quarter, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_quarter = t2.RANK_quarter
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory
AND t1.companycode = t2.companycode;

UPDATE dim_date d
SET d.quarterflag = 'Previous'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_quarter, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_quarter + 1 = t2.RANK_quarter
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory
AND t1.companycode = t2.companycode;

UPDATE dim_date d
SET d.quarterflag = 'Next'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_quarter, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_quarter - 1 = t2.RANK_quarter
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory
AND t1.companycode = t2.companycode;


/*monthflag*/
/*SELECT DISTINCT monthflag  FROM dim_date*/
UPDATE dim_date SET monthflag ='Not Set';

UPDATE dim_date d
SET d.monthflag = 'Current'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_month, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_month = t2.RANK_month
AND t1.companycode = t2.companycode
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory;

UPDATE dim_date d
SET d.monthflag = 'Previous'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_month, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_month + 1 = t2.RANK_month
AND t1.companycode = t2.companycode
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory;

UPDATE dim_date d
SET d.monthflag = 'Next'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_month, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
AND t1.RANK_month - 1 = t2.RANK_month
AND t1.companycode = t2.companycode
AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory;
 

/*weekflag*/
/*SELECT  DISTINCT weekflag FROM dim_date */
UPDATE dim_date SET weekflag ='Not Set';

UPDATE dim_date d
SET d.weekflag = 'Current'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_week, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2, 
dim_date d
WHERE d.datevalue = t1.datevalue 
  AND t1.RANK_week = t2.Rank_week
  AND t1.companycode = t2.companycode
  AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory;

UPDATE dim_date d
SET d.weekflag = 'Previous'
FROM  dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_week, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
  AND t1.RANK_week + 1 = t2.Rank_week
  AND t1.companycode = t2.companycode
  AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory;


UPDATE dim_date d
SET d.weekflag = 'Next'
FROM dim_date_rank_tmp t1, (SELECT DISTINCT t2.RANK_week, t2.companycode FROM dim_date_rank_tmp t2 WHERE current_date = t2.datevalue) t2,
dim_date d
WHERE d.datevalue = t1.datevalue 
  AND t1.RANK_week - 1 = t2.Rank_week
  AND t1.companycode = t2.companycode
  AND d.companycode = t1.companycode
AND d.plantcode_factory = t1.plantcode_factory;

DROP TABLE IF EXISTS dim_date_tmp;
DROP TABLE IF EXISTS dim_date_rank_tmp;

/*weekstartflag and weekendflag*/
/*SELECT DISTINCT weekstartflag  FROM dim_date*/

UPDATE dim_date SET weekstartflag = 'N';
UPDATE dim_date SET weekendflag = 'N';

UPDATE dim_date SET weekstartflag = 'Y' WHERE weekstartdate = datevalue;
UPDATE dim_date SET weekendflag = 'Y' WHERE weekenddate = datevalue;


/*dayflag*/
/*SELECT DISTINCT dayflag  FROM dim_date*/
UPDATE dim_date SET dayflag = 'Not Set';

UPDATE dim_date
SET dayflag = 'Current'
WHERE CURRENT_DATE = datevalue;

UPDATE dim_date
SET dayflag = 'Previous'
WHERE CURRENT_DATE - 1 = datevalue;

UPDATE dim_date
SET dayflag = 'Next'
WHERE CURRENT_DATE + 1 = datevalue;


/* dim_date quarter_startdate and quarter_enddate modifications Victor July 13 2016 - Start */

DROP TABLE IF EXISTS tmp_dim_dateqse;
CREATE TABLE tmp_dim_dateqse AS
SELECT 
      financialquartername
     ,MAX(datevalue) OVER (PARTITION BY financialquartername,companycode)quarter_enddate
     ,MIN(datevalue) OVER (PARTITION BY financialquartername,companycode)quarter_startdate
     ,dim_dateid
  FROM dim_date;

UPDATE dim_date d 
SET d.quarter_startdate = t.quarter_startdate
FROM tmp_dim_dateqse t, dim_date d
WHERE d.dim_dateid = t.dim_dateid
 AND d.quarter_startdate <> t.quarter_startdate;


UPDATE dim_date d
SET d.quarter_enddate = t.quarter_enddate 
FROM tmp_dim_dateqse t, dim_date d
WHERE d.dim_dateid = t.dim_dateid
  AND d.quarter_enddate <> t.quarter_enddate;
  /* dim_date quarter_startdate and quarter_enddate modifications Victor July 13 2016 - End */	

/*Georgiana 15 Jul 2016 Changes according to BI-3464*/

/*Updating the weeknum column with the default value*/
update dim_date d set d.weeknum=-9999;

/*Updating weeknum column for previous and next weeks*/

update dim_date d set d.weeknum=0 where d.weekflag='Current';
update dim_date d set d.weeknum=-1 where d.datevalue in (select d.datevalue-7 from dim_date d where d.weekflag='Current');
update dim_date d set d.weeknum=-2 where d.datevalue in (select d.datevalue-14 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-3 where d.datevalue in (select d.datevalue-21 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-4 where d.datevalue in (select d.datevalue-28 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-5 where d.datevalue in (select d.datevalue-35 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-6 where d.datevalue in (select d.datevalue-42 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-7 where d.datevalue in (select d.datevalue-49 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-8 where d.datevalue in (select d.datevalue-56 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-9 where d.datevalue in (select d.datevalue-63 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-10 where d.datevalue in (select d.datevalue-70 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-11 where d.datevalue in (select d.datevalue-77 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-12 where d.datevalue in (select d.datevalue-84 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-13 where d.datevalue in (select d.datevalue-91 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=-14 where d.datevalue in (select d.datevalue-98 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=1 where d.datevalue in (select d.datevalue+7 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=2 where d.datevalue in (select d.datevalue+14 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=3 where d.datevalue in (select d.datevalue+21 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=4 where d.datevalue in (select d.datevalue+28 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=5 where d.datevalue in (select d.datevalue+35 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=6 where d.datevalue in (select d.datevalue+42 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=7 where d.datevalue in (select d.datevalue+49 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=8 where d.datevalue in (select d.datevalue+56 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=9 where d.datevalue in (select d.datevalue+63 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=10 where d.datevalue in (select d.datevalue+70 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=11 where d.datevalue in (select d.datevalue+77 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=12 where d.datevalue in (select d.datevalue+84 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=13 where d.datevalue in (select d.datevalue+91 from dim_date d where weekflag='Current');
update dim_date d set d.weeknum=14 where d.datevalue in (select d.datevalue+98 from dim_date d where weekflag='Current');




