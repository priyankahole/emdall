drop table if exists tffy_i10;
create table tffy_i10 as 
select t.*, 
	   cast(pFromDate as timestamp) f_date,
	   cast(pToDate as timestamp) t_date 
from tmp_funct_fiscal_year t;
Update tffy_i10 
set pReturn = to_char(cast(f_date as date)) || '|' || to_char(cast(t_date as date));
Update companydetailed_d00 c
Set pPeriodDates = pReturn
from companydetailed_d00 c
		left join (select * 
				   from tffy_i10 
				   where Period = 1 
					 and fact_script_name = 'dim_date') s on    s.pCompanyCode = c.pCompanyCode 
														    and s.FiscalYear = c.FINYEAR
where  c.pflag1 = 1 and c.pCompanyCode <>  'Not Set' and pflag2 = 1;
Update companydetailed_d00
Set pflag3=1
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and pPeriodDates is null;
Update companydetailed_d00
Set pflag3=2
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and pPeriodDates is not null;
Update companydetailed_d00 c
set pPeriodDates = pReturn
from companydetailed_d00 c
		left join (select * 
				   from tffy_i10 
				   where Period = 12 
					 and fact_script_name = 'dim_date') s on    s.pCompanyCode = c.pCompanyCode 
														    and s.FiscalYear = c.FINYEAR
where    c.pflag1=1 and c.pCompanyCode <>  'Not Set' 
	 and c.pflag2=1 and c.pflag3=1;
drop table if exists tffy_i10;
Update companydetailed_d00
Set pPeriodFromDate = (cast(substr(pPeriodDates,1,19) as date))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;
Update companydetailed_d00
Set pPeriodToDate = (cast(substr(pPeriodDates,21,19) as date))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;
Update companydetailed_d00
Set FINMONTHSTART = MONTH(pPeriodFromDate + (( INTERVAL '1' DAY) * ceiling((cast(pPeriodToDate as date) - cast(pPeriodFromDate as date))/2) ))+1
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;
	
UPDATE companydetailed_d00
Set FINWEEKSTART = CASE 
					WHEN TO_CHAR(TRUNC(pPeriodFromDate,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(pPeriodFromDate,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(pPeriodFromDate,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(pPeriodFromDate,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(pPeriodFromDate,'UW') + 1 
					ELSE TO_CHAR(pPeriodFromDate,'UW') 
				END + 1 /* WEEK(pPeriodFromDate)+1 */ /* VW original: WEEK(pPeriodFromDate,4)+1 */
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=1;
	
Update companydetailed_d00
set pPeriodFromDate = (cast(substr(decode(trim(pPeriodDates),'|',null,pPeriodDates),1,10) as date))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=2;
Update companydetailed_d00
set pPeriodToDate = (cast(decode(trim(substr(pPeriodDates,12,19)),null,null,substr(pPeriodDates,12,19)) as date))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=2;
Update companydetailed_d00
set FINMONTHSTART = MONTH(pPeriodFromDate + (( INTERVAL '1' DAY) * ceiling((cast(pPeriodToDate as date) - cast(pPeriodFromDate as date))/2)))
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=2;
	
UPDATE companydetailed_d00
set FINWEEKSTART = CASE 
					WHEN TO_CHAR(TRUNC(pPeriodFromDate,'YYYY'),'UW') = '00' 
						AND TO_CHAR(TRUNC(pPeriodFromDate,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
						AND TO_CHAR(TRUNC(pPeriodFromDate,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
						AND TO_CHAR(TRUNC(pPeriodFromDate,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
					THEN  TO_CHAR(pPeriodFromDate,'UW') + 1 
					ELSE TO_CHAR(pPeriodFromDate,'UW') 
				END /*WEEK(pPeriodFromDate)*/   /* VW original: WEEK(pPeriodFromDate,4) */
where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 and  pflag3=2;
	
Update companydetailed_d00
SET FINMONTH = FINMONTHSTART
Where  pflag1=1 and pCompanyCode <>  'Not Set' 
	and pflag2=1 ;
Update companydetailed_d00
SET FINQUARTER=case when FINMONTH in (1,2,3) then 1
		    when FINMONTH in (4,5,6) then 2
		    when FINMONTH in (7,8,9) then 3
		    when FINMONTH in (10,11,12) then 4
		end
Where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1 ;            
Update companydetailed_d00
SET FINQTRID = (FINYEAR * 10) + FINQUARTER
Where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1 ;
Update companydetailed_d00
SET FINQTRNAME = concat(FINYEAR,' Q ',CAST(FINQUARTER AS VARCHAR (1)))
Where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1 ;

Update companydetailed_d00
SET FINMONTHID = (FINYEAR * 100) + FINMONTH
Where  pflag1=1 and pCompanyCode <>  'Not Set' and pflag2=1 ;
drop table if exists git_i10;
create table git_i10 as 
Select pCompanyCode,
min(dt) dt,
cast(0 as Integer) FINMONTHSTART,
cast(0 as Integer) FINWEEKSTART,
cast(NULL as timestamp) pPeriodToDate,
cast(NULL as timestamp) pPeriodFromDate,
convert(varchar(53),NULL) pPeriodDates,
cast(0 as Integer) FINYEAR,
cast(0 as Integer) FINMONTH,
cast(0 as Integer) FINQUARTER,
cast(0 as Integer) FINQTRID
from companydetailed_d00 
where pflag2=1 
group by pCompanyCode;
insert into git_i10 
select pCompanyCode,min(dt),1,1,null,null,null,0,0,0,0 
from companydetailed_d00 
where pCompanyCode='Not Set' 
group by pCompanyCode;

merge into companydetailed_d00 dest
using (select t1.rowid as rid
	   from companydetailed_d00 t1
				inner join git_i10 t2 on t2.pCompanyCode = t1.pCompanyCode
	   where t1.dt >= t2.dt) src on dest.rowid = src.rid
when matched then update set loop2 = 1;
			/* original VW script
			Update companydetailed_d00
			set loop2 = 1
			where DT >=(select dt from git_i10 where git_i10.pCompanyCode=companydetailed_d00.pCompanyCode ) */
Update git_i10 t1
set t1.FINMONTHSTART = ifnull(t2.FINMONTHSTART, 0)
from git_i10 t1
		left join companydetailed_d00 t2 on    t2.pCompanyCode = t1.pCompanyCode 
										   and t2.dt = t1.dt
Where t1.pCompanyCode <> 'Not Set';

Update git_i10 t1
set t1.FINWEEKSTART = ifnull(t2.FINWEEKSTART, 0)
from git_i10 t1
		left join companydetailed_d00 t2 on    t2.pCompanyCode = t1.pCompanyCode 
										   and t2.dt = t1.dt
Where t1.pCompanyCode <> 'Not Set';

Update git_i10 t1
set t1.pPeriodToDate = t2.pPeriodToDate
from git_i10 t1
		left join companydetailed_d00 t2 on    t2.pCompanyCode = t1.pCompanyCode 
										   and t2.dt = t1.dt;

Update git_i10 t1
set t1.pPeriodFromDate = t2.pPeriodFromDate
from git_i10 t1
		left join companydetailed_d00 t2 on    t2.pCompanyCode = t1.pCompanyCode 
										   and t2.dt = t1.dt
Where t1.pCompanyCode <> 'Not Set';

Update git_i10 t1
set t1.pPeriodDates = t2.pPeriodDates
from git_i10 t1
		left join companydetailed_d00 t2 on    t2.pCompanyCode = t1.pCompanyCode 
										   and t2.dt = t1.dt
Where t1.pCompanyCode <> 'Not Set';

Update companydetailed_d00 t1
set t1.FINMONTHSTART = ifnull(t2.FINMONTHSTART, 0)
from companydetailed_d00 t1
		left join git_i10 t2 on t1.pCompanyCode = t2.pCompanyCode
Where t1.loop2 = 1;

Update companydetailed_d00 t1
set t1.FINWEEKSTART = ifnull(t2.FINWEEKSTART, 0)
from companydetailed_d00 t1
		left join git_i10 t2 on t1.pCompanyCode = t2.pCompanyCode
Where t1.loop2 = 1;

Update companydetailed_d00 t1
set t1.pPeriodToDate = t2.pPeriodToDate
from companydetailed_d00 t1
		left join git_i10 t2 on t1.pCompanyCode = t2.pCompanyCode
Where t1.loop2 = 1;

Update companydetailed_d00 t1
set t1.pPeriodFromDate = t2.pPeriodFromDate
from companydetailed_d00 t1
		left join git_i10 t2 on t1.pCompanyCode = t2.pCompanyCode
Where t1.loop2 = 1;

Update companydetailed_d00 t1
set t1.pPeriodDates = t2.pPeriodDates
from companydetailed_d00 t1
		left join git_i10 t2 on t1.pCompanyCode = t2.pCompanyCode
Where t1.loop2 = 1;

drop table if exists git_i10;
Update companydetailed_d00
set p1=1
where loop2=1 and DT > pPeriodToDate;
Update companydetailed_d00
set p1=2
where loop2=1 and DT <= pPeriodToDate;
Update companydetailed_d00 
set pPeriodToDate = add_months(trunc(DT, 'MONTH'),1)-1				/* VW original: LAST_DAY(DT) */
where p1=1 and pCompanyCode = 'Not Set';

Update companydetailed_d00 
set FINYEAR = YEAR(DT) 
where p1=1 and pCompanyCode = 'Not Set';        
Update companydetailed_d00 
set FINMONTH = MONTH(DT) 
where p1=1 and pCompanyCode = 'Not Set';
Update companydetailed_d00
Set FINQUARTER=( case when FINMONTH in (1,2,3) then 1
		when FINMONTH in (4,5,6) then 2
		when FINMONTH in (7,8,9) then 3
		when FINMONTH in (10,11,12) then 4
end)
where p1=1 and pCompanyCode = 'Not Set';
Update companydetailed_d00 
SET FINQTRID = (FINYEAR * 10) + FINQUARTER 
where p1=1 and pCompanyCode = 'Not Set';
Update companydetailed_d00 
SET FINQTRNAME = concat(FINYEAR,' Q ',CAST(FINQUARTER AS VARCHAR (1))) 
where p1=1 and pCompanyCode = 'Not Set';
Update companydetailed_d00 
SET FINMONTHID = (FINYEAR * 100) + FINMONTH 
where p1=1 and pCompanyCode = 'Not Set';

Update companydetailed_d00 t1
set t1.pPeriodDates = t2.pReturn
from companydetailed_d00 t1
			left join fiscalprd_i00 t2 on    t2.pCompanyCode = t1.pCompanyCode 
										 and t2.CalDate = t1.DT
where t1.p1 = 1 and t1.pCompanyCode <> 'Not Set';
Update companydetailed_d00 
set p2=1 
where p1=1 and pCompanyCode <> 'Not Set' and pPeriodDates is not null;
Update companydetailed_d00 
set pPeriodFromDate = (cast(substr(decode(trim(substr(pPeriodDates,9,19)),'|',null,pPeriodDates),9,19) as date))
where p2=1;
Update companydetailed_d00 
set pPeriodToDate = (cast(trim(substr(pPeriodDates,29,39)) as date)) 
where p2=1;
Update companydetailed_d00 
set FINMONTH = (substr(pPeriodDates,6,2)) 
where p2=1;
Update companydetailed_d00 
SET FINYEAR = (substr(pPeriodDates,1,4)) 
where p2=1;
Update companydetailed_d00
Set FINQUARTER =(case when FINMONTH in (1,2,3) then 1
		 when FINMONTH in (4,5,6) then 2
		 when FINMONTH in (7,8,9) then 3
		 when FINMONTH in (10,11,12) then 4
	end )
where p2=1;
Update companydetailed_d00 
SET FINQTRID = (FINYEAR * 10) + FINQUARTER 
where p2=1;
Update companydetailed_d00 
SET FINQTRNAME = concat(FINYEAR,' Q ',CAST(FINQUARTER AS VARCHAR (1))) 
where p2=1;
Update companydetailed_d00 
SET FINMONTHID = (FINYEAR * 100) + FINMONTH 
where p2=1;
update companydetailed_d00 set p1=0,p2=0;
Update companydetailed_d00 
set p1=1 
where loop2=1 and (pPeriodDates is not null or pCompanyCode = 'Not Set');

Update companydetailed_d00 SET ISWEEKENDDAY =0 where p1=1;    

Update companydetailed_d00 SET CALYEAR = YEAR(DT)  where p1=1;    
Update companydetailed_d00 SET CALQUARTER = case when MONTH(DT) in (1,2,3) then 1		/* VW original: QUARTER(DT) */ 
												 when MONTH(DT) in (4,5,6) then 2
												 when MONTH(DT) in (7,8,9) then 3
												 when MONTH(DT) in (10,11,12) then 4 end
where p1 = 1;   
Update companydetailed_d00 SET CALMONTH = MONTH(DT) where p1=1;    
Update companydetailed_d00 SET vMONTHNAME = to_char(DT,'Month') where p1=1;  
  
UPDATE companydetailed_d00 
SET CALWEEK = CASE 
				WHEN TO_CHAR(TRUNC(DT,'YYYY'),'UW') = '00' 
					AND TO_CHAR(TRUNC(DT,'YYYY') + INTERVAL '1' DAY,'UW') = '00'
					AND TO_CHAR(TRUNC(DT,'YYYY') + INTERVAL '2' DAY,'UW') = '00' 
					AND TO_CHAR(TRUNC(DT,'YYYY') + INTERVAL '3' DAY,'UW') = '00'  
				THEN  TO_CHAR(DT,'UW') + 1 
				ELSE TO_CHAR(DT,'UW') 
			END /* WEEK(DT) */
where p1=1;   											/* VW original:  WEEK(DT,4) */ 

Update companydetailed_d00 SET DAYOFCALYEAR = cast(to_char(DT,'DDD') as integer) where p1=1;  				/* VW original:  DAYOFMONTH(DT) */
Update companydetailed_d00 SET vDAYOFMONTH = cast(to_char(DT,'DD') as integer) where p1=1;    				/* VW original:  DAYOFMONTH(DT) */
Update companydetailed_d00 SET vDAYOFWEEK = cast(to_char(DT,'D') as integer) where p1=1;					/* VW original:  DAYOFWEEK(DT) */    
Update companydetailed_d00 SET DAYOFWEEKNAME = to_char(DT,'Day') where p1=1;  
Update companydetailed_d00 SET JDATE = (CALYEAR * 1000) + DAYOFCALYEAR where p1=1;
Update companydetailed_d00 SET CALQTRID = (CALYEAR * 10) + CALQUARTER where p1=1;
Update companydetailed_d00 SET CALQTRNAME = concat(CALYEAR, ' Q ',CALQUARTER)  where p1=1;
Update companydetailed_d00 SET CALWEEKID = (CALYEAR * 100) + CALWEEK where p1=1;
Update companydetailed_d00 set ISWEEKENDDAY =1 where p1=1 and (vDAYOFWEEK = 1 OR  vDAYOFWEEK = 7);
Update companydetailed_d00 
set ISLEAPYEAR=(case when ((mod(CALYEAR,4) = 0)  AND (mod(CALYEAR,100) != 0 OR mod(CALYEAR,400) = 0)) then 1 else 0 end)
where p1=1;
Update companydetailed_d00 
set SEASON=(case when CALMONTH IN(9,10,11) then 'Spring'
				when CALMONTH IN(12,1,2) then 'Summer'
				when CALMONTH IN(3,4,5) then 'Autumn'
				when CALMONTH IN(6,7,8) then 'Winter'
			end)
where p1=1;
Update companydetailed_d00 
SET CALMONTHID = (CALYEAR * 100) + CALMONTH 
where p1=1;
Update companydetailed_d00 
SET MONTHABBR = LEFT(vMONTHNAME, 3) 
where p1=1;
Update companydetailed_d00 
set FINWEEK= ( case when CALWEEK > FINWEEKSTART then CALWEEK - (FINWEEKSTART - 1)
						else CALWEEK + (FINWEEKSTART - 1) end)
where p1=1;
Update companydetailed_d00 
SET FINWEEKID = (FINYEAR * 100) + FINWEEK 
where p1=1;

Update companydetailed_d00 
SET DAYOFFINYEAR = (case when (CALMONTH > (FINMONTHSTART - 1)) 
				then cast(to_char(DT,'DDD') as integer) - (cast(concat(ifnull(FINYEAR, '0001'), '-1-1') as date) - cast(concat(ifnull(FINYEAR, '0001'), '-', decode(FINMONTHSTART,'0','01',FINMONTHSTART), '-01') as date))  /* VW original: DAYOFYEAR(DT) */
				else cast(to_char(DT,'DDD') as integer) - (cast(concat(ifnull(FINYEAR, '0001'), '-1-1') as date) - cast(concat(ifnull(FINYEAR, '0001'), '-', decode(FINMONTHSTART,'0','01',FINMONTHSTART), '-01') as date))  /* VW original: DAYOFYEAR(DT) */
			 
	end
)
where p1=1;

Update companydetailed_d00 
SET DAYOFWEEKABBR = LEFT(DAYOFWEEKNAME, 3) 
where p1=1;
Update companydetailed_d00 
SET DAYSINCALYEAR = cast(to_char((CAST(concat(CALYEAR, '-12-31') AS timestamp)),'DDD') as integer)		 /* VW original:  DAYSINCALYEAR = DAYOFYEAR(CAST(concat(CALYEAR, '-12-31') AS timestamp(0)))  */
where p1=1;
Update companydetailed_d00 
SET DAYSINFINYEAR = (CAST(concat(ifnull(FINYEAR, '0001'), '-', decode(FINMONTHSTART,'0','01',FINMONTHSTART), '-1') AS date) -  CAST(concat(ifnull(FINYEAR, '0001'), '-', decode(FINMONTHSTART,'0','01',FINMONTHSTART), '-1') AS date)) 
where p1=1
AND DAYSINFINYEAR <> 
(CAST(concat(ifnull(FINYEAR, '0001'), '-', decode(FINMONTHSTART,'0','01',FINMONTHSTART), '-1') AS date) - 
CAST(concat(ifnull(FINYEAR, '0001'), '-', decode(FINMONTHSTART,'0','01',FINMONTHSTART), '-1') AS date)) ;

Update companydetailed_d00 
SET DAYSINMONTH = DAY(( CAST(concat(CALYEAR, '-', LEFT(MONTH((DT + ( INTERVAL '1' MONTH))),3), '-1') AS DATE) - ( INTERVAL '1' DAY))) 
where p1=1;
Update companydetailed_d00 
set WEEKDAYSCALYEARSOFAR=0
 where p1=1 and DAYOFCALYEAR = 1;
Update companydetailed_d00 
set WEEKDAYSFINYEARSOFAR=0 
where p1=1 and CALMONTH = FINMONTHSTART AND vDAYOFMONTH = 1;
Update companydetailed_d00 
set WEEKDAYSMONTHSOFAR = 0 
where p1=1 and vDAYOFMONTH = 1;
Update companydetailed_d00 
SET WEEKDAYSCALYEARSOFAR= WEEKDAYSCALYEARSOFAR + 1 
Where p1=1 and ISWEEKENDDAY = 0;
Update companydetailed_d00 
SET WEEKDAYSFINYEARSOFAR = WEEKDAYSFINYEARSOFAR + 1 
Where p1=1 and ISWEEKENDDAY = 0;
Update companydetailed_d00 
SET WEEKDAYSMONTHSOFAR = WEEKDAYSMONTHSOFAR + 1 
Where p1=1 and ISWEEKENDDAY = 0;    

Update companydetailed_d00 t1
set DateExists = ifnull(a.exst,0) 
from companydetailed_d00 t1
		left join (select 1 as exst, x.*
				   from dim_date x where x.plantcode_factory='Not Set') a on    a.DateValue = t1.DT 
							          and a.CompanyCode = t1.pCompanyCode
where t1.p1 = 1;
drop table if exists mh_i01;
Create table mh_i01
as
select 	ifnull(max(d.dim_dateid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from dim_date d
where d.dim_dateid <> 1;
INSERT INTO dim_date(dim_dateid,DateValue,
						  DateName,
                          JulianDate,
                          CalendarYear,
                          FinancialYear,
                          CalendarQuarter,
                          CalendarQuarterID,
                          CalendarQuarterName,
                          FinancialQuarter,
                          FinancialQuarterID,
                          FinancialQuarterName,
                          Season,
                          CalendarMonthID,
                          CalendarMonthNumber,
                          MonthName,
                          MonthAbbreviation,
                          FinancialMonthID,
                          FinancialMonthNumber,
                          CalendarWeekID,
                          CalendarWeek,
                          FinancialWeekID,
                          FinancialWeek,
                          DayofCalendarYear,
                          DayofFinancialYear,
                          DayofMonth,
                          WeekDayNumber,
                          WeekDayName,
                          WeekDayAbbreviation,
                          IsaWeekendday,
                          IsaLeapYear,
                          DaysInCalendarYear,
                          DaysInFinancialYear,
                          DaysInCalendarYearSofar,
                          DaysInFinancialYearSofar,
                          WeekdaysInCalendarYearSofar,
                          WeekdaysInFinancialYearSofar,
                          WorkdaysInCalendarYearSofar,
                          WorkdaysInFinancialYearSofar,
                          DaysInMonth,
                          DaysInMonthSofar,
                          WeekdaysInMonthSofar,
                          WorkdaysInMonthSofar,
                          CompanyCode,
                          CalendarWeekYr)
          Select mh_i01.maxid + row_number() over (order by '') ,DT,
	  /* Liviu Ionescu - change syntax */
                  to_char(DT, 'DD Mon YYYY') DateName,
		  /* date_format(DT, '%d %b %Y') DateName, */ 
                  JDATE,
                  CALYEAR,
                  FINYEAR,
                  CALQUARTER,
                  CALQTRID,
                  CALQTRNAME,
                  ifnull(FINQUARTER,0),
                  ifnull(FINQTRID,0),
                  FINQTRNAME,
                  SEASON,
                  CALMONTHID,
                  CALMONTH,
                  vMONTHNAME,
                  MONTHABBR,
                  ifnull(FINMONTHID, 0),
                  ifnull(FINMONTH, 0),
                  CALWEEKID,
                  CALWEEK,
                  FINWEEKID,
                  FINWEEK,
                  DAYOFCALYEAR,
                  ifnull(DAYOFFINYEAR, 0),
                  vDAYOFMONTH,
                  vDAYOFWEEK,
                  DAYOFWEEKNAME,
                  DAYOFWEEKABBR,
                  ISWEEKENDDAY,
                  ISLEAPYEAR,
                  DAYSINCALYEAR,
                  ifnull(DAYSINFINYEAR, 0),
                  DAYOFCALYEAR - 1,
                  ifnull(DAYOFFINYEAR - 1, 0),
                  WEEKDAYSCALYEARSOFAR,
                  ifnull(WEEKDAYSFINYEARSOFAR, 0),
                  WEEKDAYSCALYEARSOFAR,
                  ifnull(WEEKDAYSFINYEARSOFAR, 0),
                  DAYSINMONTH,
                  vDAYOFMONTH,
                  WEEKDAYSMONTHSOFAR,
                  WEEKDAYSMONTHSOFAR,
                  pCompanyCode,/* Liviu Ionescu - change syntax */
/*                  concat(CALYEAR,'-',lpad(cast(CALWEEK as varchar),2,'0')) */		concat(CALYEAR,'-',lpad(convert(varchar(2),CALWEEK),2,'0'))
		From companydetailed_d00 ,mh_i01 where DateExists = 0 and p1=1;
     
    /*  UPDATE  dim_date dd
	from companydetailed_d00 c
        SET   dd.FinancialYear = c.FINYEAR,
              dd.CalendarQuarter = c.CALQUARTER,
              dd.CalendarQuarterID = c.CALQTRID,
              dd.CalendarQuarterName = c.CALQTRNAME,
              dd.FinancialQuarter = c.FINQUARTER,
              dd.FinancialQuarterID = c.FINQTRID,
              dd.FinancialQuarterName = c.FINQTRNAME,
              dd.Season = c.SEASON,
              dd.CalendarMonthID = c.CALMONTHID,
              dd.CalendarMonthNumber = c.CALMONTH,
              dd.MonthName = c.vMONTHNAME,
              dd.MonthAbbreviation = c.MONTHABBR,
              dd.FinancialMonthID = ifnull(c.FINMONTHID, 0),
              dd.FinancialMonthNumber = ifnull(c.FINMONTH, 0),
              dd.CalendarWeekID = c.CALWEEKID,
              dd.CalendarWeek = c.CALWEEK,
              dd.FinancialWeekID = c.FINWEEKID,
              dd.FinancialWeek = c.FINWEEK,
              dd.DayofCalendarYear = c.DAYOFCALYEAR,
              dd.DayofFinancialYear = ifnull(c.DAYOFFINYEAR, 0),
              dd.DayofMonth = c.vDAYOFMONTH,
              dd.WeekDayNumber = c.vDAYOFWEEK,
              dd.WeekDayName = c.DAYOFWEEKNAME,
              dd.WeekDayAbbreviation = c.DAYOFWEEKABBR,
              dd.IsaWeekendday = c.ISWEEKENDDAY,
              dd.IsaLeapYear = c.ISLEAPYEAR,
              dd.DaysInCalendarYear = c.DAYSINCALYEAR,
              dd.DaysInFinancialYear = ifnull(c.DAYSINFINYEAR, 0),
              dd.DaysInCalendarYearSofar = c.DAYOFCALYEAR - 1,
              dd.DaysInFinancialYearSofar = ifnull(c.DAYOFFINYEAR - 1, 0),
              dd.WeekdaysInCalendarYearSofar = c.WEEKDAYSCALYEARSOFAR,
              dd.WeekdaysInFinancialYearSofar = ifnull(c.WEEKDAYSFINYEARSOFAR, 0),
              dd.WorkdaysInCalendarYearSofar = c.WEEKDAYSCALYEARSOFAR,
              dd.WorkdaysInFinancialYearSofar = ifnull(c.WEEKDAYSFINYEARSOFAR, 0),
              dd.DaysInMonth = c.DAYSINMONTH,
              dd.DaysInMonthSofar = c.vDAYOFMONTH,
              dd.WeekdaysInMonthSofar = c.WEEKDAYSMONTHSOFAR,
              dd.WorkdaysInMonthSofar = c.WEEKDAYSMONTHSOFAR,
              dd.CalendarWeekYr = concat(CALYEAR,'-',lpad(cast(c.CALWEEK as char(2)),2,'0'))
      WHERE DateValue = DT and CompanyCode = pCompanyCode
	    and DateExists <>  0 and p1=1 */

 UPDATE  dim_date dd
SET dd.FinancialYear = c.FINYEAR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND    (dd.FinancialYear <> c.FINYEAR or dd.FinancialYear is null);

 UPDATE  dim_date dd
SET dd.CalendarQuarter = c.CALQUARTER,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.CalendarQuarter <> c.CALQUARTER or dd.CalendarQuarter is null);

UPDATE  dim_date dd
SET dd.CalendarQuarterID = c.CALQTRID,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
and DateExists <>  0 and p1=1AND (dd.CalendarQuarterID <> c.CALQTRID or dd.CalendarQuarterID is null);
 UPDATE  dim_date dd
SET dd.CalendarQuarterName = c.CALQTRNAME,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.CalendarQuarterName <> c.CALQTRNAME or dd.CalendarQuarterName is null);

 UPDATE  dim_date dd
SET dd.FinancialQuarter = c.FINQUARTER,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.FinancialQuarter <> c.FINQUARTER or dd.FinancialQuarter is null);
UPDATE  dim_date dd
SET dd.FinancialQuarterID = c.FINQTRID,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.FinancialQuarterID <> c.FINQTRID or dd.FinancialQuarterID is null);
UPDATE  dim_date dd
SET dd.FinancialQuarterName = c.FINQTRNAME,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.FinancialQuarterName <> c.FINQTRNAME or dd.FinancialQuarterName is null);

 UPDATE  dim_date dd
SET dd.Season = c.SEASON,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.Season <> c.SEASON or dd.Season is null);
 UPDATE  dim_date dd
SET dd.CalendarMonthID = c.CALMONTHID,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.CalendarMonthID <> c.CALMONTHID or dd.CalendarMonthID is null);
UPDATE  dim_date dd
SET dd.CalendarMonthNumber = c.CALMONTH,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.CalendarMonthNumber <> c.CALMONTH or dd.CalendarMonthNumber is null);

 UPDATE  dim_date dd
SET dd.MonthName = c.vMONTHNAME,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.MonthName <> c.vMONTHNAME or dd.MonthName is null);
 UPDATE  dim_date dd
SET dd.MonthAbbreviation = c.MONTHABBR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.MonthAbbreviation <> c.MONTHABBR or dd.MonthAbbreviation is null);
 UPDATE  dim_date dd
SET dd.FinancialMonthID = ifnull(c.FINMONTHID, 0),
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.FinancialMonthID <> ifnull(c.FINMONTHID, 0) or dd.FinancialMonthID is null);
 UPDATE  dim_date dd
SET dd.FinancialMonthNumber = ifnull(c.FINMONTH, 0),
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.FinancialMonthNumber <> ifnull(c.FINMONTH, 0) or dd.FinancialMonthNumber is null);
 UPDATE  dim_date dd
SET dd.CalendarWeekID = c.CALWEEKID,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.CalendarWeekID <> c.CALWEEKID or dd.CalendarWeekID is null);
 UPDATE  dim_date dd
SET dd.CalendarWeek = c.CALWEEK,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.CalendarWeek <> c.CALWEEK or dd.CalendarWeek is null);

 UPDATE  dim_date dd
SET dd.FinancialWeekID = c.FINWEEKID,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.FinancialWeekID <> c.FINWEEKID or dd.FinancialWeekID is null);

 UPDATE  dim_date dd
SET dd.FinancialWeek = c.FINWEEK,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.FinancialWeek <> c.FINWEEK or dd.FinancialWeek is null);
 UPDATE  dim_date dd
SET dd.DayofCalendarYear = c.DAYOFCALYEAR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DayofCalendarYear <> c.DAYOFCALYEAR or dd.DayofCalendarYear is null);

 UPDATE  dim_date dd
SET dd.DayofFinancialYear = ifnull(c.DAYOFFINYEAR, 0),
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DayofFinancialYear <> ifnull(c.DAYOFFINYEAR, 0) or dd.DayofFinancialYear is null);

 UPDATE  dim_date dd
SET dd.DayofMonth = c.vDAYOFMONTH,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DayofMonth <> c.vDAYOFMONTH or dd.DayofMonth is null);

 UPDATE  dim_date dd
SET dd.WeekDayNumber = c.vDAYOFWEEK,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.WeekDayNumber <> c.vDAYOFWEEK or dd.WeekDayNumber is null);

 UPDATE  dim_date dd
SET dd.WeekDayName = c.DAYOFWEEKNAME,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.WeekDayName <> c.DAYOFWEEKNAME or dd.WeekDayName is null);

 UPDATE  dim_date dd
SET dd.WeekDayAbbreviation = c.DAYOFWEEKABBR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.WeekDayAbbreviation <> c.DAYOFWEEKABBR or dd.WeekDayAbbreviation is null);

 UPDATE  dim_date dd
SET dd.IsaWeekendday = c.ISWEEKENDDAY,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
and DateExists <>  0 and p1=1AND (dd.IsaWeekendday <> c.ISWEEKENDDAY or dd.IsaWeekendday is null);

 UPDATE  dim_date dd
SET dd.IsaLeapYear = c.ISLEAPYEAR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.IsaLeapYear <> c.ISLEAPYEAR or dd.IsaLeapYear is null);

 UPDATE  dim_date dd
SET dd.DaysInCalendarYear = c.DAYSINCALYEAR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DaysInCalendarYear <> c.DAYSINCALYEAR or dd.DaysInCalendarYear is null);

 UPDATE  dim_date dd
SET dd.DaysInFinancialYear = ifnull(c.DAYSINFINYEAR, 0),
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DaysInFinancialYear <> ifnull(c.DAYSINFINYEAR, 0) or dd.DaysInFinancialYear is null);
 UPDATE  dim_date dd
SET dd.DaysInCalendarYearSofar = c.DAYOFCALYEAR - 1,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DaysInCalendarYearSofar <> c.DAYOFCALYEAR - 1 or dd.DaysInCalendarYearSofar is null);
UPDATE  dim_date dd
SET dd.DaysInFinancialYearSofar = ifnull(c.DAYOFFINYEAR - 1, 0),
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DaysInFinancialYearSofar <> ifnull(c.DAYOFFINYEAR - 1, 0) or dd.DaysInFinancialYearSofar is null);

 UPDATE  dim_date dd
SET dd.WeekdaysInCalendarYearSofar = c.WEEKDAYSCALYEARSOFAR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.WeekdaysInCalendarYearSofar <> c.WEEKDAYSCALYEARSOFAR or dd.WeekdaysInCalendarYearSofar is null);

 UPDATE  dim_date dd
SET dd.WeekdaysInFinancialYearSofar = ifnull(c.WEEKDAYSFINYEARSOFAR, 0),
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.WeekdaysInFinancialYearSofar <> ifnull(c.WEEKDAYSFINYEARSOFAR, 0) or dd.WeekdaysInFinancialYearSofar is null);

 UPDATE  dim_date dd
SET dd.WorkdaysInCalendarYearSofar = c.WEEKDAYSCALYEARSOFAR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.WorkdaysInCalendarYearSofar <> c.WEEKDAYSCALYEARSOFAR or dd.WorkdaysInCalendarYearSofar is null);

 UPDATE  dim_date dd
SET dd.WorkdaysInFinancialYearSofar = ifnull(c.WEEKDAYSFINYEARSOFAR, 0),
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.WorkdaysInFinancialYearSofar <> ifnull(c.WEEKDAYSFINYEARSOFAR, 0) or dd.WorkdaysInFinancialYearSofar is null);
 UPDATE  dim_date dd
SET dd.DaysInMonth = c.DAYSINMONTH,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DaysInMonth <> c.DAYSINMONTH or dd.DaysInMonth is null);

 UPDATE  dim_date dd
SET dd.DaysInMonthSofar = c.vDAYOFMONTH,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.DaysInMonthSofar <> c.vDAYOFMONTH or dd.DaysInMonthSofar is null);

 UPDATE  dim_date dd
SET dd.WeekdaysInMonthSofar = c.WEEKDAYSMONTHSOFAR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.WeekdaysInMonthSofar <> c.WEEKDAYSMONTHSOFAR or dd.WeekdaysInMonthSofar is null);

 UPDATE  dim_date dd
SET dd.WorkdaysInMonthSofar = c.WEEKDAYSMONTHSOFAR,
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
and DateExists <>  0 and p1=1AND (dd.WorkdaysInMonthSofar <> c.WEEKDAYSMONTHSOFAR or dd.WorkdaysInMonthSofar is null);

UPDATE  dim_date dd
SET dd.CalendarWeekYr = concat(CALYEAR,'-',lpad(cast(c.CALWEEK as varchar(2)),2,'0')),
			dd.dw_update_date = current_timestamp
from companydetailed_d00 c, dim_date dd
WHERE DateValue = DT and CompanyCode = pCompanyCode
 and DateExists <>  0 and p1=1AND (dd.CalendarWeekYr <> concat(CALYEAR,'-',lpad(cast(c.CALWEEK as varchar(2)),2,'0')) or dd.CalendarWeekYr is null);

drop table if exists CWeekends_i01;
drop table if exists mh_i01;

create table CWeekends_i01 as 
SELECT CompanyCode,CalendarYear AS CYear,COUNT(IsaWeekendday) AS Num_Weekend_Days 
FROM dim_date 
WHERE IsaWeekendday = 1 
GROUP BY CompanyCode,CalendarYear;

drop table if exists CWeekdays_i01;

create table CWeekdays_i01 as
Select CompanyCode,CalendarYear AS CYear,MAX(DaysInCalendarYear) AS Days_In_Calendar_Year
From dim_date
GROUP BY CompanyCode,CalendarYear;

UPDATE dim_date dd
SET WeekdaysInCalendarYear = CWeekdays_i01.Days_In_Calendar_Year - CWeekends_i01.Num_Weekend_Days,
			dd.dw_update_date = current_timestamp
from CWeekends_i01, CWeekdays_i01, dim_date dd
WHERE dd.CalendarYear = CWeekends_i01.CYear and dd.CalendarYear = CWeekdays_i01.CYear
      and dd.CompanyCode = CWeekends_i01.CompanyCode
	and dd.CompanyCode = CWeekdays_i01.CompanyCode
AND (WeekdaysInCalendarYear <> CWeekdays_i01.Days_In_Calendar_Year - CWeekends_i01.Num_Weekend_Days or WeekdaysInCalendarYear is null);
drop table if exists CWeekends_i01;
drop table if exists CWeekdays_i01;

drop table if exists FWeekends_i01;
create table FWeekends_i01 as
Select CompanyCode,FinancialYear AS FYear,COUNT(IsaWeekendday) AS Num_Weekend_Days
FROM dim_date
WHERE IsaWeekendday = 1
GROUP BY CompanyCode,FinancialYear;
drop table if exists FWeekdays_i01;
create table FWeekdays_i01 as
Select CompanyCode,FinancialYear AS FYear,MAX(DaysInFinancialYear) AS Days_In_Financial_Year
From dim_date
GROUP BY CompanyCode,FinancialYear;

UPDATE dim_date dd
SET WeekdaysInFinancialYear = FWeekdays_i01.Days_In_Financial_Year - FWeekends_i01.Num_Weekend_Days,
			dd.dw_update_date = current_timestamp
From FWeekends_i01, FWeekdays_i01, dim_date dd
 WHERE dd.FinancialYear = FWeekends_i01.FYear and dd.FinancialYear = FWeekdays_i01.FYear
      and dd.CompanyCode = FWeekends_i01.CompanyCode
      and dd.CompanyCode = FWeekdays_i01.CompanyCode
AND (WeekdaysInFinancialYear <> FWeekdays_i01.Days_In_Financial_Year - FWeekends_i01.Num_Weekend_Days or WeekdaysInFinancialYear is null);
drop table if exists FWeekends_i01;
drop table if exists FWeekdays_i01;

UPDATE dim_date
   SET WorkdaysInCalendarYear = WeekdaysInCalendarYear,
			dw_update_date = current_timestamp
WHERE (WorkdaysInCalendarYear <> WeekdaysInCalendarYear or WorkdaysInCalendarYear is null);
UPDATE dim_date
SET WorkdaysInFinancialYear = WeekdaysInFinancialYear,
			dw_update_date = current_timestamp
WHERE (WorkdaysInFinancialYear <> WeekdaysInFinancialYear or WorkdaysInFinancialYear is null);
UPDATE dim_date
SET WorkdaysInMonth = WeekdaysInMonth,
			dw_update_date = current_timestamp
WHERE (WorkdaysInMonth <> WeekdaysInMonth or WorkdaysInMonth is null);
UPDATE dim_date
SET MonthYear = (case when CalendarYear = 0 then  'Not Set' else CONCAT(MonthAbbreviation, ' ', CalendarYear) end),
			dw_update_date = current_timestamp
WHERE (MonthYear <> (case when CalendarYear = 0 then  'Not Set' else CONCAT(MonthAbbreviation, ' ', CalendarYear) end) or MonthYear is null);

UPDATE dim_date
SET FinancialMonthYear = ( case when FinancialYear = 0 then 'Not Set' else  CONCAT(FinancialYear, '-', LPAD(FinancialMonthNumber,2,'0')) end),
			dw_update_date = current_timestamp
WHERE (FinancialMonthYear <> ( case when FinancialYear = 0 then 'Not Set' else  CONCAT(FinancialYear, '-', LPAD(FinancialMonthNumber,2,'0')) end) or FinancialMonthYear is null);

drop table if exists StartEnd_i01;
create table StartEnd_i01 as
Select  min(datevalue + ( INTERVAL '1' day) - cast(to_char(datevalue,'D') as integer) )  Week_Start,			/* VW original: DAYOFWEEK(datevalue) */
	    max(datevalue + ( INTERVAL '7' day) - cast(to_char(datevalue,'D') as integer)) Week_End,				/* VW original: DAYOFWEEK(datevalue) */
	CalendarWeekID,
	CompanyCode
from dim_date 
where   datevalue < to_date('9999-12-31', 'YYYY-MM-DD')
	and datevalue > to_date('0001-01-01', 'YYYY-MM-DD')
group by CompanyCode,CalendarWeekID;
				/* VW Original: 
				drop table if exists StartEnd_i01
				create table StartEnd_i01 as
				Select  min(datevalue + ( ( INTERVAL '1' day) - DAYOFWEEK(datevalue)))  Week_Start,
					max(datevalue +( ( INTERVAL '7' day) - DAYOFWEEK(datevalue))) Week_End,
					CalendarWeekID,
					CompanyCode
				from dim_date 
				group by CompanyCode,CalendarWeekID */

 UPDATE dim_date dd
    SET WeekStartDate = StartEnd.Week_Start,
			dd.dw_update_date = current_timestamp
from StartEnd_i01 StartEnd, dim_date dd
 WHERE dd.CompanyCode = StartEnd.CompanyCode
      and dd.CalendarWeekID = StartEnd.CalendarWeekID
AND (WeekStartDate <> StartEnd.Week_Start or WeekStartDate is null);
 UPDATE dim_date dd
SET WeekEndDate = StartEnd.Week_End,
			dd.dw_update_date = current_timestamp
from StartEnd_i01 StartEnd, dim_date dd
 WHERE dd.CompanyCode = StartEnd.CompanyCode
      and dd.CalendarWeekID = StartEnd.CalendarWeekID
AND (WeekEndDate <> StartEnd.Week_End or WeekEndDate is null);
drop table if exists StartEnd_i01;

create table StartEnd_i01 as 
Select min(datevalue) Month_Start, max(datevalue) Month_End, CalendarMonthID ,CompanyCode
From  dim_date 
group by CompanyCode,CalendarMonthID;
	
UPDATE dim_date dd
SET MonthStartDate = StartEnd_i01.Month_Start,
			dd.dw_update_date = current_timestamp
From StartEnd_i01, dim_date dd
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.CalendarMonthID = StartEnd_i01.CalendarMonthID
AND (MonthStartDate <> StartEnd_i01.Month_Start or MonthStartDate is null);

 UPDATE dim_date dd
SET MonthEndDate = StartEnd_i01.Month_End,
			dd.dw_update_date = current_timestamp
From StartEnd_i01, dim_date dd
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.CalendarMonthID = StartEnd_i01.CalendarMonthID
AND (MonthEndDate <> StartEnd_i01.Month_End or MonthEndDate is null);

drop table if exists StartEnd_i01;
 
drop table if exists StartEnd_i01;
create table StartEnd_i01 as 
Select min(datevalue) Month_Start, max(datevalue) Month_End, FinancialMonthID,CompanyCode
         from dim_date 
         group by CompanyCode,FinancialMonthID;

 UPDATE dim_date dd
SET FinancialMonthStartDate = StartEnd_i01.Month_Start,
			dd.dw_update_date = current_timestamp
From StartEnd_i01, dim_date dd
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.FinancialMonthID = StartEnd_i01.FinancialMonthID
AND (FinancialMonthStartDate <> StartEnd_i01.Month_Start or FinancialMonthStartDate is null);

 UPDATE dim_date dd
SET  FinancialMonthEndDate = StartEnd_i01.Month_End,
			dd.dw_update_date = current_timestamp
From StartEnd_i01, dim_date dd
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.FinancialMonthID = StartEnd_i01.FinancialMonthID
AND (FinancialMonthEndDate <> StartEnd_i01.Month_End or FinancialMonthEndDate is null);

drop table if exists StartEnd_i01;
      
drop table if exists StartEnd_i01;
Create table StartEnd_i01 as
select min(datevalue) FinYr_Start, max(datevalue) FinYr_End,FinancialYear,CompanyCode
        from dim_date 
         group by CompanyCode,FinancialYear;

 UPDATE dim_date dd
    SET FinancialYearStartDate = StartEnd_i01.FinYr_Start,
        FinancialYearEndDate = StartEnd_i01.FinYr_End,
			dd.dw_update_date = current_timestamp
FROM StartEnd_i01, dim_date dd
 WHERE dd.CompanyCode = StartEnd_i01.CompanyCode
      and dd.FinancialYear = StartEnd_i01.FinancialYear;      
