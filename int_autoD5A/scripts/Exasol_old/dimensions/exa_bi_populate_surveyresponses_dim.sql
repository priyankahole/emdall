/* ################################################################################################################## */
/* */
/*   Script         : exa_bi_populate_surveyresponses_dim */
/*   Author         : Madalina */
/*   Created On     : 21 Iun 2017 */
/*   Modifications  :  */
/*	 Description    : Contains the questions and answers from surveys. For the questions with multiple answers, each answer stays on a different row. 
					  The main fact (FACT_CUSTOMERSURVEY) contains the  questions and answers from surveys as well, but for questions with multiple answers, the answers are concatenated on a single row.
					  The dimension is linked to the fact using Join condition (on questionid, surveyid and customercode)
				/*grain: surveycode, dim_surveyquestionsid, customercode*/
/*  */
/* ################################################################################################################## */
 

update surveyresults_all s
set s.dim_surveyquestionsid = dq.dim_surveyquestionsid,
	s.surveycode = dq.surveycode
from surveyresults_all s, dim_surveyquestions dq
where s.questioncode = dq.questioncode
	and s.subquestioncode = dq.subquestioncode;

delete from dim_surveyresults_all s where exists 
	(SELECT 1 FROM surveyresults_all ds where s.surveycode = ds.surveycode);	
	
	
insert into dim_surveyresults_all (dim_surveyresults_allid)
select 1
from (select 1) a
where not exists (select '1' from dim_surveyresults_all where dim_surveyresults_allid = 1);

/*insert into the dimension the questions that do not exist*/
delete from number_fountain where table_name = 'dim_surveyresults_all';
insert into number_fountain 
select 'dim_surveyresults_all', ifnull(max(dim_surveyresults_allid),1)
from dim_surveyresults_all;

insert into dim_surveyresults_all
 (dim_surveyresults_allid,surveycode,dim_surveyquestionsid,surveyresponse,customercode,responseposition,EnglishResponse)
select(select ifnull(m.max_id,1) from number_fountain m where m.table_name = 'dim_surveyresults_all') + row_number() over(order by '') as dim_surveyresults_allid,
ifnull(surveycode,'Not Set'),
ifnull(dim_surveyquestionsid,1),
ifnull(surveyresponse, 'Not Set'),
ifnull(customercode, 0),
ifnull(responseposition,0),
ifnull(EnglishResponse,'Not Set')
from surveyresults_all s where not exists(select 1  from dim_surveyresults_all ds where s.surveycode = ds.surveycode
																					and ifnull(s.dim_surveyquestionsid, 1) = ds.dim_surveyquestionsid
																					and s.customercode = ds.customercode
										) ; 

update dim_surveyresults_all dsall
set dsall.dim_surveyid = ds.dim_surveyid
from dim_surveyresults_all dsall, dim_survey ds
where dsall.surveycode = ds.surveycode
and dsall.dim_surveyid <> ds.dim_surveyid;


/*Madalina - 29 May 18 - Adding new attribute, to mask some responses - APP-6054*/
update dim_surveyresults_all dsall
set commentsAndThoughts = surveyresponse 
from dim_surveyresults_all dsall
    inner join dim_surveyquestions q on q.dim_surveyquestionsid = dsall.dim_surveyquestionsid
where q.nrcrt = 10;

update dim_surveyresults_all dsall
set surveyresponse = 'Not Set'
from dim_surveyresults_all dsall
    inner join dim_surveyquestions q on q.dim_surveyquestionsid = dsall.dim_surveyquestionsid
where q.nrcrt = 10;