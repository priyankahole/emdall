UPDATE    dim_availabilityconfirmation ac

   SET ac.Description = ifnull(DD07T_DDTEXT, 'Not Set')
          FROM
          DD07T dt, dim_availabilityconfirmation ac
 WHERE ac.AvailabilityConfirmationCode = dt.DD07T_DOMVALUE
 AND dt.DD07T_DOMNAME = 'MDPBV' AND dt.DD07T_DOMVALUE IS NOT NULL;