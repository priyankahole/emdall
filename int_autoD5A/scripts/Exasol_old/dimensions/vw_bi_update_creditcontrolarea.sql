UPDATE    dim_creditcontrolarea cca

   SET cca.CreditControlAreaName = t.T014T_KKBTX
       FROM
          T014T t, dim_creditcontrolarea cca
 WHERE cca.RowIsCurrent = 1
 AND t.T014T_KKBER = cca.CreditControlAreaCode
;