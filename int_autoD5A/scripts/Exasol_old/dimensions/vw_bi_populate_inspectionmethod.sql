insert into dim_inspectionmethod
(dim_inspectionmethodid, 
PlantCode,
insprectionmethod,
VersionNumber,
Status,
ShortText)

select 	1, 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_inspectionmethod where dim_inspectionmethodid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectionmethod';
insert into number_fountain
select 'dim_inspectionmethod',
 ifnull(max(d.dim_inspectionmethodid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectionmethod d
where d.dim_inspectionmethodid <> 1;

insert into dim_inspectionmethod (dim_inspectionmethodid,
                          PlantCode,
                          insprectionmethod,
     					  VersionNumber,
     					  Status,
						  ShortText,
     					  dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_inspectionmethod') + row_number() over(order by '') AS dim_inspectionmethodid,
        ifnull(q.QMTB_WERKS, 'Not Set') AS PlantCode,
        ifnull(q.QMTB_PMTNR, 'Not Set') AS insprectionmethod,
        ifnull(q.QMTB_VERSION, 'Not Set') AS VersionNumber,
		ifnull(q.QMTB_LOEKZ, 'Not Set') as Status,
		ifnull(q.QMTT_KURZTEXT, 'Not Set') as ShortText,
       current_timestamp
from QMTB_QMTT q
 where not exists (select 'x' from dim_inspectionmethod di
                        where di.PlantCode = ifnull(q.QMTB_WERKS, 'Not Set')
                             AND di.insprectionmethod = ifnull(q.QMTB_PMTNR, 'Not Set') 
							 AND di.VersionNumber = ifnull(q.QMTB_VERSION, 'Not Set'));
							 
UPDATE dim_inspectionmethod di
SET di.Status = ifnull(q.QMTB_LOEKZ, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMTB_QMTT q,dim_inspectionmethod di
WHERE di.PlantCode = ifnull(q.QMTB_WERKS, 'Not Set')
AND di.insprectionmethod = ifnull(q.QMTB_PMTNR, 'Not Set') 
AND di.VersionNumber = ifnull(q.QMTB_VERSION, 'Not Set') 
AND di.Status <> ifnull(q.QMTB_LOEKZ, 'Not Set');

UPDATE dim_inspectionmethod di
SET di.ShortText = ifnull(q.QMTT_KURZTEXT, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMTB_QMTT q,dim_inspectionmethod di
WHERE di.PlantCode = ifnull(q.QMTB_WERKS, 'Not Set')
AND di.insprectionmethod = ifnull(q.QMTB_PMTNR, 'Not Set') 
AND di.VersionNumber = ifnull(q.QMTB_VERSION, 'Not Set') 
AND di.ShortText <> ifnull(q.QMTT_KURZTEXT, 'Not Set');