UPDATE    dim_transactiongroup tg
   SET tg.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			tg.dw_update_date = current_timestamp
       FROM dim_transactiongroup tg,
          DD07T t
   WHERE     t.DD07T_DOMNAME = 'TRVOG'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND tg.TransactionGroup = t.DD07T_DOMVALUE
;

INSERT INTO dim_transactiongroup(dim_transactiongroupId,transactiongroup,description )
SELECT 1,'Not Set','Not Set'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_transactiongroup
               WHERE dim_transactiongroupId = 1);

delete from number_fountain m where m.table_name = 'dim_transactiongroup';

insert into number_fountain
select 	'dim_transactiongroup',
	ifnull(max(d.Dim_TransactionGroupid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_transactiongroup d
where d.Dim_TransactionGroupid <> 1;	

INSERT INTO dim_transactiongroup(Dim_TransactionGroupid,
								TransactionGroup,
                                 Description)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_transactiongroup') 
          + row_number() over(order by ''),
			 DD07T_DOMVALUE,
	ifnull(DD07T_DDTEXT, 'Not Set')
       FROM DD07T
      WHERE DD07T_DOMNAME = 'TRVOG' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM Dim_transactiongroup
                    WHERE TransactionGroup = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'TRVOG')
;
