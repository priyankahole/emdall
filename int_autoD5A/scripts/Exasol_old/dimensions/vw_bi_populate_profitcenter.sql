DROP TABLE IF EXISTS tmp_dimpc_insert;
CREATE TABLE tmp_dimpc_insert
AS
SELECT DISTINCT CEPCT_PRCTR,max(CEPCT_DATBI) CEPCT_DATBI,max(CEPCT_KOKRS) CEPCT_KOKRS
FROM CEPCT
group by CEPCT_PRCTR;

DROP TABLE IF EXISTS tmp_dimpc_insert_full;
CREATE TABLE tmp_dimpc_insert_full
AS
SELECT DISTINCT c.*
FROM CEPCT c, tmp_dimpc_insert t
WHERE c.CEPCT_PRCTR = t.CEPCT_PRCTR AND c.CEPCT_DATBI = t.CEPCT_DATBI AND c.CEPCT_KOKRS = t.CEPCT_KOKRS;

UPDATE    dim_profitcenter pc
   SET pc.ProfitCenterName = ifnull(CEPCT_KTEXT, 'Not Set'),
       pc.ControllingArea = ifnull(CEPCT_KOKRS, 'Not Set'),
       pc.ValidTo = CEPCT_DATBI,
			pc.dw_update_date = current_timestamp
	FROM
          dim_profitcenter pc, tmp_dimpc_insert_full c
 WHERE pc.RowIsCurrent = 1
 AND c.CEPCT_PRCTR = pc.ProfitCenterCode
;

INSERT INTO dim_profitcenter(dim_profitcenterId, RowIsCurrent,profitcentercode,controllingarea,profitcentername,rowstartdate)
SELECT 1, 1,'Not Set','Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_profitcenter
               WHERE dim_profitcenterId = 1);

delete from number_fountain m where m.table_name = 'dim_profitcenter';
   
insert into number_fountain
select 	'dim_profitcenter',
	ifnull(max(d.Dim_ProfitCenterid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_profitcenter d
where d.Dim_ProfitCenterid <> 1; 


INSERT INTO dim_profitcenter(Dim_ProfitCenterid,
			  ProfitCenterCode,
			  ValidTo,
			  ControllingArea,
			  ProfitCenterName,
                          RowStartDate,
                          RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_profitcenter')
         + row_number() over(order by ''),
			  CEPCT_PRCTR,
	    CEPCT_DATBI,
	    ifnull(CEPCT_KOKRS, 'Not Set'),
	    ifnull(CEPCT_KTEXT, 'Not Set'),
            current_timestamp,
            1
       FROM tmp_dimpc_insert_full
      WHERE CEPCT_PRCTR IS NOT NULL
	    AND NOT EXISTS
                  (SELECT 1
                     FROM dim_profitcenter
                    WHERE ProfitCenterCode = CEPCT_PRCTR)
;

DROP TABLE IF EXISTS tmp_dimpc_insert;
DROP TABLE IF EXISTS tmp_dimpc_insert_full;
