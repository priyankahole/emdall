
UPDATE    dim_customergroup2 cg2

   SET cg2.Description = ifnull(TVV2T_BEZEI, 'Not Set')
       FROM
          TVV2T t, dim_customergroup2 cg2
   WHERE t.TVV2T_KVGR2 = cg2.CustomerGroup AND t.TVV2T_SPRAS = 'E';
