UPDATE    dim_term dt

   SET Name = ifnull(T052U_TEXT1, 'Not Set'),
          FROM
          t052u u, dim_term dt
 WHERE dt.RowIsCurrent = 1
 AND u.T052U_ZTERM = dt.Termcode
;