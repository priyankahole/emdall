

UPDATE dim_customerpurchaseordertype cpo
   SET cpo.Description = ifnull(t.T176T_VTEXT, 'Not Set'),
            cpo.dw_update_date = current_timestamp
			 FROM dim_customerpurchaseordertype cpo, T176T t
 WHERE cpo.CustomerPOType = t.T176T_BSARK 
 AND cpo.RowIsCurrent = 1;

