/*##################################################################################################################
	Script         : vw_bi_populate_dim_plantsection                                                                                                                                             
	Created By     : Suchithra                                                                                                        
	Created On     : 04 May 2016  
	Description    : Script to populated Plant Secion dimension to store description
			Grain : Plant(t357_werk),Plant Section(t357_beber)
			
	Change History                                                                                                                                   
	Date            By        Version           Desc                                                                 

####################################################################################################################*/ 


INSERT INTO dim_plantsection(
	dim_plantsectionid,
	Plant,
	PlantSection,
	CompanyAreaPerson,
	ComapanyAreaPersonPhone,
	RowStartDate,
	RowIsCurrent
)
  SELECT    
  	1,			 
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	current_timestamp,
	1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_plantsection WHERE dim_plantsectionId = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_plantsection';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_plantsection',
                ifnull(max(d.dim_plantsectionId),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_plantsection d
where d.dim_plantsectionId <> 1;


INSERT INTO dim_plantsection(
	dim_plantsectionid,
	Plant,
	PlantSection,
	CompanyAreaPerson,
	ComapanyAreaPersonPhone,
	RowStartDate,
	RowIsCurrent
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_plantsection') + row_number() over(order by '')  as dim_plantsectionid,
	ifnull(T357_WERKS,'Not Set') as Plant,
	ifnull(T357_BEBER,'Not Set') as PlantSection,
	ifnull(T357_FING,'Not Set') as CompanyAreaPerson,
	ifnull(T357_TELE,'Not Set') as ComapanyAreaPersonPhone,
    	current_timestamp,
    	1
FROM T357
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_plantsection d1
        WHERE d1.Plant = ifnull(T357_WERKS,'Not Set') 
		AND d1.PlantSection = ifnull(T357_BEBER,'Not Set') 
	);

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_plantsection';
