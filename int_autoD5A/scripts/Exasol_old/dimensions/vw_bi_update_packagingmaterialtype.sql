UPDATE    dim_packagingmaterialtype s

 SET s.Description = t.TVTYT_BEZEI
       FROM
          TVTYT t, dim_packagingmaterialtype s
 WHERE s.RowIsCurrent = 1
 AND s.packagingmaterialtype = t.TVTYT_TRATY
;
