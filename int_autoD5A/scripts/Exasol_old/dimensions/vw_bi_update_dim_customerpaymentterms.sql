UPDATE    dim_customerpaymentterms cpt
     
   SET cpt.PaymentTermName = ifnull(TVZBT_VTEXT, 'Not Set')
       FROM
          TVZBT t , dim_customerpaymentterms cpt
	WHERE t.TVZBT_ZTERM = cpt.PaymentTermCode   ;
