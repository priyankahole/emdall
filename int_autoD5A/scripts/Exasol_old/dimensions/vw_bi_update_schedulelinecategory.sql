UPDATE    dim_schedulelinecategory slc
   SET slc.Description = TVEPT_VTEXT,
			slc.dw_update_date = current_timestamp
   FROM
          dim_schedulelinecategory slc, TVEPT t
 WHERE slc.RowIsCurrent = 1
 AND t.TVEPT_SPRAS = 'E' AND t.TVEPT_ETTYP = slc.ScheduleLineCategory
;
