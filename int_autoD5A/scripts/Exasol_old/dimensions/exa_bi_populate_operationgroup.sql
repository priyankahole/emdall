
/* Measures correction - APP - 8120
  Some portions of Actual and Target LT time measures ( the arrow ones) are missing when a main state (FPU) has more than one substates (FPU 1, FPU 2) */

update scm_dynamics_input_hierarchy_for_dimension scm 
set toplant = pl.planttitle_merck
from  scm_dynamics_input_hierarchy_for_dimension scm 
  inner join  dim_plant pl on toplant = cast(pl.dim_plantid as varchar(30));


merge into scm_dynamics_input_hierarchy_for_dimension scm2 using
( select distinct segmenttypeseq, 
    brand ,operationgroup ,operationtype ,fromoperation ,leftoperationgroup ,rightoperationgroup ,operationgroupseq ,
    operationtypeseq ,dim_batchid_transittime ,fromOperation_real ,fromplant ,toplant
  from scm_dynamics_input_hierarchy_for_dimension scm1
  where scm1.segmenttypeseq <> 99999
) scm1
on scm1.brand =  scm2.brand
  and scm1.operationgroup = scm2.operationgroup
  and scm1.operationtype = scm2.operationtype
  and scm1.fromoperation = scm2.fromoperation
  and scm1.leftoperationgroup = scm2.leftoperationgroup
  and scm1.rightoperationgroup = scm2.rightoperationgroup
  and scm1.operationgroupseq = scm2.operationgroupseq
  and scm1.operationtypeseq = scm2.operationtypeseq
  and scm1.dim_batchid_transittime = scm2.dim_batchid_transittime
  and scm1.fromOperation_real = scm2.fromOperation_real
  and scm1.fromplant = scm2.fromplant
  and  scm1.toplant = scm2.toplant
  and scm2.segmenttypeseq = 99999
when matched then update
set scm2.segmenttypeseq = scm1.segmenttypeseq;



truncate table dim_operation_group;


INSERT INTO dim_operation_group(dim_operation_groupid)
SELECT 1 
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_operation_group
               WHERE dim_operation_groupid = 1);

DELETE FROM number_fountain m where m.table_name = 'dim_operation_group';

INSERT INTO number_fountain
SELECT 'dim_operation_group',
       ifnull(max(d.dim_operation_groupid),
       ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM dim_operation_group d
WHERE d.dim_operation_groupid <> 1;

insert into dim_operation_group  (
    DIM_OPERATION_GROUPID ,
    DW_INSERT_DATE          ,
    DW_UPDATE_DATE          ,
    DIM_PROJECTSOURCEID     ,
    BRAND                  ,
    OPERATIONGROUP          ,
    OPERATIONGROUPSEQUNCE   ,
    OPERATIONTYPE           ,
    OPERATIONTYPESEQUENCE   ,
    PLANTCODE               ,
    MATERIAL                ,
    MATERIALDESC           ,
    MATERIALTYPE          ,
    STARTINGPLANT           ,
    OPERATIONGROUPNAME      ,
    LEFTOPERATIONGROUPCODE  ,
    RIGHTOPERATIONGROUPCODE ,
    OPERATIONTYPENAME       ,
    LEFTOPERATIONTYPECODE   ,
    RIGHTOPERATIONTYPECODE  ,
    PLANTTO                 ,
    PLANTFROM               ,
    FROMOPERATIONGROUP      ,
    SEGMENTTYPE            ,
    SEGMENTTYPESEQ,
    dim_batchid_transittime,
    InspectionLotNo,
    ct_segmenttargettime_dim,
    CT_SEGMENT_QTY_DIM,
    batchnumber,
    dateidpostingdate,
    LEVEL2ACTUALLT,
    LEVEL2TARGETLT,
    level2HitOrMiss,
    LEVEL3ACTUALLT,
    LEVEL3TARGETLT,
    level3HitOrMiss )
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_operation_group')
          +  row_number() over(order by ''),
		    current_timestamp,
			current_timestamp,
			1,
			brand,
			operationgroup,
			operationgroupseq,
			operationtype,
			operationtypeseq,		
			ifnull(b.plantcode,'Not Set'),   /*Madalina 27 Jun 18 - APP-9897 - Plantcode correction*/
			ifnull(b.partnumber,'Not Set'),
			'Not Set',
			'Not Set',
			'Not Set',
			operationgroup,
			leftoperationgroup,
			rightoperationgroup,
			'Not Set',
			'Not Set',
			'Not Set',
			toplant,
			fromplant,
			fromoperation,
			/* ifnull(b.batchnumber,'Transit Time'), */
			/* case when b.batchnumber is not null then b.batchnumber || ' ' || b.partnumber  else 'Transit Time' end,*/
      s.stage_type,
			segmenttypeseq,
      dim_batchid_transittime,
      InspectionLotNo,
      ct_segmenttargettime,
      CT_SEGMENT_QTY,
      case when b.batchnumber is not null then b.batchnumber || ' ' || b.partnumber  else 'Transit Time' end,
      dateidpostingdate,
      LEVEL2ACTUALLT,
      LEVEL2TARGETLT,
      level2HitOrMiss,
      LEVEL3ACTUALLT,
      LEVEL3TARGETLT,
      level3HitOrMiss
			from scm_dynamics_input_hierarchy_for_dimension s LEFT JOIN dim_batch b on s.dim_batchid_transittime = cast(b.dim_batchid as varchar(100));

update dim_operation_group 
set ct_segmenttargettime_dim= ct_segmenttargettime_dim/3,
    CT_SEGMENT_QTY_dim = CT_SEGMENT_QTY_dim/3
where segmenttype <> 'Transit Time';

update dim_operation_group 
set operationtype = plantfrom
where operationtype = 'COMOPS' AND OPERATIONGROUP  = 'COMOPS'
AND FROMOPERATIONGROUP = 'COMOPS' and segmenttype <> 'Transit Time';

update dim_operation_group
set operationgroup = 'MARKET'
where operationgroup = 'COMOPS';

  /* updates for comops plng region initial version 
merge into dim_operation_group d
using ( select distinct d.dim_operation_groupid,
                       first_value(p.region_merck) over (partition by p.planttitle_merck  order by p.planttitle_merck) region_merck
from dim_operation_group d, dim_plant p
where d.plantto = p.planttitle_merck
AND d.operationgroup = 'COMOPS' ) t
on t.dim_operation_groupid = d.dim_operation_groupid
when matched then update set d.plantto = t.region_merck 

merge into dim_operation_group d
using ( select distinct d.dim_operation_groupid,
                       first_value(p.region_merck) over (partition by p.planttitle_merck  order by p.planttitle_merck) region_merck
from dim_operation_group d, dim_plant p
where d.plantfrom = p.planttitle_merck
AND d.operationgroup = 'COMOPS' ) t
on t.dim_operation_groupid = d.dim_operation_groupid
when matched then update set d.plantfrom = t.region_merck 
   end updates for comops plng region initial version */ 

/* update dim_operation_group d
set d.materialdesc = ifnull(p.partdescription,'Not Set')
from dim_operation_group d 
inner join dim_plant pl on d.plantto = pl.planttitle_merck
inner join  dim_part p on d.material = p.partnumber and pl.plantcode = p.plant
and d.materialdesc <> ifnull(p.partdescription,'Not Set') */
	