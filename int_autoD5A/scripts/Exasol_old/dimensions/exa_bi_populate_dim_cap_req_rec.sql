/*DEFAULT ROW*/
INSERT INTO dim_cap_req_rec(
	dim_cap_req_recid,
	cap_id_of_req,
	internal_counter,
	split,
	personnel_number,
	first_name,
	last_name,
	first_second_name,
	workk,
	work_uom,
	work_centerid,
	earliest_split_start,
	latest_split_finish,
	opac,
	counter,
	Main_rqts_record,
	dw_insert_date ,
	dw_update_date
)
  SELECT    
	1,
	0,
	0,
	0,
	0,
	'Not Set',
	'Not Set',
	'Not Set',
	0,
	0,
	0,
	'0001-01-01',
	'0001-01-01',
	'Not Set',
	0,
	0,
	current_timestamp,
	current_timestamp

FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_cap_req_rec WHERE dim_cap_req_recid = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_cap_req_rec';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_cap_req_rec',
                ifnull(max(d.dim_cap_req_recid),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_cap_req_rec d
where d.dim_cap_req_recid <> 1;

drop table if exists tmp_dim_cap_req_rec;
create table tmp_dim_cap_req_rec as
SELECT DISTINCT *
FROM KBED k
inner join AUFK_AFKO_AFVC a
ON a.AFVC_VORNR = k.KBED_VORNR
and a.AFVC_BEDID = k.KBED_BEDID
and a.AFVC_BEDZL = k.KBED_BEDZL
left join PA0002 p
on KBED_PERNR = PA0002_PERNR
where KBED_SPLIT <> 0; 


/*insert new rows*/

INSERT INTO dim_cap_req_rec(
	dim_cap_req_recid,
	cap_id_of_req,
	internal_counter,
	split,
	personnel_number,
	first_name,
	last_name,
	first_second_name,
	workk,
	work_uom,
	work_centerid,
	earliest_split_start,
	latest_split_finish,
	opac,
	counter,
	Main_rqts_record,
	dw_insert_date 
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_cap_req_rec') + row_number() over(order by '') as dim_cap_req_recid,
ifnull(KBED_BEDID, 0) as cap_id_of_req,
ifnull(KBED_BEDZL, 0) as internal_counter,
ifnull(KBED_SPLIT, 0) as split,
ifnull(kbed_pernr, 0) as personnel_number,
ifnull(PA0002_VORNA, 'Not Set') as first_name,
ifnull(PA0002_NACHN, 'Not Set') as last_name,
'Not Set' as first_second_name,
ifnull(KBED_KBEASOLL, 0) as workk,
ifnull(KBED_KEINH, 0) as work_uom,
ifnull(KBED_ARBID, 0) as work_centerid,
ifnull(KBED_FSTAD, '0001-01-01') as earliest_split_start,
ifnull(KBED_SENDD, '0001-01-01') as latest_split_finish,
ifnull(KBED_VORNR, 'Not Set') as opac,
ifnull(KBED_CANUM, 0) as counter,
ifnull(KBED_CANUMF, 0) as Main_rqts_record,
current_timestamp
FROM tmp_dim_cap_req_rec t
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_cap_req_rec d
        where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
		AND ifnull(KBED_BEDZL, 0) = d.internal_counter
		AND ifnull(KBED_CANUM, 0) = d.counter);


/*update existing rows*/
update dim_cap_req_rec d
set Main_rqts_record = ifnull(KBED_CANUMF, 0),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  Main_rqts_record <> ifnull(KBED_CANUMF, 0);

update dim_cap_req_rec d
set opac = ifnull(KBED_VORNR, 'Not Set'),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  opac <> ifnull(KBED_VORNR, 'Not Set');

update dim_cap_req_rec d
set earliest_split_start = ifnull(KBED_FSTAD, '0001-01-01'),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  earliest_split_start <> ifnull(KBED_FSTAD, '0001-01-01');

update dim_cap_req_rec d
set latest_split_finish = ifnull(KBED_SENDD, '0001-01-01'),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  latest_split_finish <> ifnull(KBED_SENDD, '0001-01-01');

update dim_cap_req_rec d
set work_centerid = ifnull(KBED_ARBID, 0),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  work_centerid <> ifnull(KBED_ARBID, 0);

update dim_cap_req_rec d
set work_uom = ifnull(KBED_KEINH, 0),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  work_uom <> ifnull(KBED_KEINH, 0);

update dim_cap_req_rec d
set workk = ifnull(KBED_KBEASOLL, 0),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  workk <> ifnull(KBED_KBEASOLL, 0);

/*APP-10528 Unable to get a stable source of rows fix*/
/*update dim_cap_req_rec d
set split = ifnull(KBED_SPLIT, 0),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  split <> ifnull(KBED_SPLIT, 0)*/

/*update dim_cap_req_rec d
set personnel_number = ifnull(kbed_pernr, 'Not Set'),
dw_update_date = current_timestamp
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and  personnel_number <> ifnull(kbed_pernr, 'Not Set')*/

merge into dim_cap_req_rec d 
using (select distinct d.cap_id_of_req,d.internal_counter,d.counter,ifnull(KBED_SPLIT, 0) as split
 FROM dim_cap_req_rec d, tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and split <> ifnull(KBED_SPLIT, 0)
) t
on  d.cap_id_of_req=t.cap_id_of_req	
and d.internal_counter=t.internal_counter
and d.counter=t.counter
when matched then update set d.split = t.split,
dw_update_date = current_timestamp;


MERGE INTO dim_cap_req_rec d
USING (
SELECT distinct d.cap_id_of_req ,d.internal_counter ,d.counter , ifnull(kbed_pernr, 'Not Set') AS personnel_number
FROM dim_cap_req_rec d, tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
and personnel_number <> ifnull(kbed_pernr, 'Not Set')
)b
ON
d.cap_id_of_req = b.cap_id_of_req AND
d.internal_counter = b.internal_counter AND
d.counter=b.counter
WHEN MATCHED THEN UPDATE SET
d.personnel_number = b.personnel_number ,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE
d.personnel_number <> b.personnel_number AND
dw_update_date <> current_timestamp ;

merge into dim_cap_req_rec d
using(
select distinct dim_cap_req_recid, 
first_value(t.PA0002_VORNA) over(partition by KBED_BEDID, KBED_BEDZL, KBED_CANUM) as PA0002_VORNA
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
)t
on t.dim_cap_req_recid = d.dim_cap_req_recid
when matched then update
set first_name = ifnull(t.PA0002_VORNA, 'Not Set')
where first_name <> ifnull(t.PA0002_VORNA, 'Not Set');

merge into dim_cap_req_rec d
using(
select distinct dim_cap_req_recid, 
first_value(t.PA0002_NACHN) over(partition by KBED_BEDID, KBED_BEDZL, KBED_CANUM) as PA0002_NACHN
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
)t
on t.dim_cap_req_recid = d.dim_cap_req_recid
when matched then update
set last_name = ifnull(t.PA0002_NACHN, 'Not Set')
where last_name <> ifnull(t.PA0002_NACHN, 'Not Set');

MERGE INTO DIM_CAP_REQ_REC d
using( 
select distinct d.dim_cap_req_recid, max(t.aufk_aufnr) as aufk_aufnr
FROM dim_cap_req_rec d,  tmp_dim_cap_req_rec t
where ifnull(KBED_BEDID, 0) = d.cap_id_of_req
AND ifnull(KBED_BEDZL, 0) = d.internal_counter
AND ifnull(KBED_CANUM, 0) = d.counter
group by 1
)t
on t.dim_cap_req_recid = d.dim_cap_req_recid
when matched then update
set d.orderr = ifnull(t.aufk_aufnr, 'Not Set')
where d.orderr <> ifnull(t.aufk_aufnr, 'Not Set');

drop table if exists tmp_dim_cap_req_rec;

/*concatenation of first and second name*/
update dim_cap_req_rec
set first_second_name = 'Not Set';

update dim_cap_req_rec d
set first_second_name = CONCAT(first_name, ' ', last_name)
from dim_cap_req_rec;

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_cap_req_rec';

/*
Alin 15 may 2018 - this was requested on APP-7223 by Michal on Feb 26th
statements below are needed to insert the missing object numbers in dim_cap_req_rec dimension
PM order analysis SA broughts the data from dim_cap_req_rec dimension by using the join condition with
dim_ordermaster on object_number
*/
DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_cap_req_rec';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_cap_req_rec',
                ifnull(max(d.dim_cap_req_recid),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_cap_req_rec d
where d.dim_cap_req_recid <> 1;


INSERT INTO dim_cap_req_rec(
DIM_CAP_REQ_RECID,
CAP_ID_OF_REQ,
INTERNAL_COUNTER,
SPLIT,
PERSONNEL_NUMBER,
FIRST_NAME,
LAST_NAME,
FIRST_SECOND_NAME,
"work",
WORK_UOM,
WORK_CENTERID,
EARLIEST_SPLIT_START,
LATEST_SPLIT_FINISH,
OPAC,
COUNTER,
MAIN_RQTS_RECORD,
WORKK,
orderr,
flag_source,
DW_INSERT_DATE
)

SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_cap_req_rec') + row_number() over(order by '') as dim_cap_req_recid,
om.dd_cap_req_rec_id as CAP_ID_OF_REQ,
0 as INTERNAL_COUNTER,
0 as SPLIT,
0 as PERSONNEL_NUMBER,
'Not Set'  as FIRST_NAME,
'Not Set' as LAST_NAME,
'Not Set' as FIRST_SECOND_NAME,
0 as "work",
'Not Set' as WORK_UOM,
0 as WORK_CENTERID,
'0001-01-01' as EARLIEST_SPLIT_START,
'0001-01-01' as LATEST_SPLIT_FINISH,
'Not Set' as OPAC,
0 as COUNTER,
0 as MAIN_RQTS_RECORD,
0 as WORKK,
'Not Set' as orderr,
'from PM Order fact' as flag_source,
current_timestamp

from fact_pmorder om
where not exists (select 1
from dim_cap_req_rec rp
where rp.cap_id_of_req = om.dd_cap_req_rec_id);

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_cap_req_rec';
