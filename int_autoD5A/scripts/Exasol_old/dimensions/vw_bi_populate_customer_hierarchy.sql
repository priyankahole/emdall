/* ##################################################################################################################

   Script         : bi_populate_customer_hierarchy 
   Author         : Ashu
   Created On     : 17 Jan 2013


   Description    : Stored Proc bi_populate_customer_hierarchy from MySQL to Vectorwise syntax

   Change History
   Date            By        Version           Desc
   17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
#################################################################################################################### */

Drop table if exists pCustHierType_701;

create table pCustHierType_701 ( pCustomerHierarchyType ) As
Select CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.hierarchy.type'),
              'A'),varchar(5));

Drop table if exists custhierarchy_tmp;

Create table custhierarchy_tmp
as Select HierarchyType,
                              CustomerNumber,
                              HighCustomerNumber,
                              HigherCustomerNumber,
                              TopLevelCustomerNumber,
                              HighCustomerDesc,
                              HigherCustomerDesc,
                              TopLevelCustomerDesc,
                              EndDate,
                              StartDate,
			      HigherLevelSalesOrg
from dim_customer where 1=2;

INSERT INTO custhierarchy_tmp(dd_HierarchyType,
                              CustomerNumber,
                              HighCustomerNumber,
                              HigherCustomerNumber,
                              TopLevelCustomerNumber,
                              HighCustomerDesc,
                              HigherCustomerDesc,
                              TopLevelCustomerDesc,
			      EndDate,
                              StartDate,
							  HigherLevelSalesOrg)
   SELECT ifnull(KNVH_HITYP,'Not Set'),
          ifnull(KNVH_KUNNR,'Not Set'),
          ifnull(KNVH_HKUNNR,'Not Set'),
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          'Not Set',
          KNVH_DATBI,
          KNVH_DATAB,
		  ifnull(KNVH_HVKORG, 'Not Set')
     FROM KNVH
 WHERE  current_date BETWEEN KNVH_DATAB AND KNVH_DATBI;

      
 UPDATE    custhierarchy_tmp ct 
   SET ct.HigherCustomerNumber = ctPar.HighCustomerNumber
   FROM
          custhierarchy_tmp ctPar, custhierarchy_tmp ct 
 WHERE     ctPar.CustomerNumber = ct.HighCustomerNumber
          AND ctPar.dd_HierarchyType = ct.dd_HierarchyType AND  ct.HigherCustomerNumber = 'Not Set';


UPDATE    custhierarchy_tmp ct  
   SET ct.TopLevelCustomerNumber = ctPar.HighCustomerNumber
      FROM
          custhierarchy_tmp ctPar, custhierarchy_tmp ct 
 WHERE ctPar.CustomerNumber = ct.HigherCustomerNumber
          AND ctPar.dd_HierarchyType = ct.dd_HierarchyType 
          AND ct.TopLevelCustomerNumber = 'Not Set';
          
UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.HigherCustomerNumber,
       ct.HigherCustomerNumber = ct.HighCustomerNumber,
       ct.HighCustomerNumber = 'Not Set'
          FROM custhierarchy_tmp ct 
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber <> 'Not Set'
    and ct.HighCustomerNumber <> 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.HighCustomerNumber,
       ct.HigherCustomerNumber = 'Not Set',
       ct.HighCustomerNumber = 'Not Set'
          FROM custhierarchy_tmp ct 
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber = 'Not Set'
    and ct.HighCustomerNumber <> 'Not Set';

UPDATE    custhierarchy_tmp ct
   SET ct.TopLevelCustomerNumber = ct.CustomerNumber,
       ct.HigherCustomerNumber = 'Not Set',
       ct.HighCustomerNumber = 'Not Set'
          FROM custhierarchy_tmp ct 
 WHERE ct.TopLevelCustomerNumber = 'Not Set'
    and ct.HigherCustomerNumber = 'Not Set'
    and ct.HighCustomerNumber = 'Not Set';

UPDATE    custhierarchy_tmp ct 
             
   SET ct.TopLevelCustomerDesc = c.Name
      FROM custhierarchy_tmp ct ,dim_customer c    
 WHERE ct.TopLevelCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

UPDATE    custhierarchy_tmp ct
   SET ct.HigherCustomerDesc = c.Name
         FROM custhierarchy_tmp ct ,dim_customer c  
 WHERE ct.HigherCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;
 
 UPDATE    custhierarchy_tmp ct    
   SET ct.HighCustomerDesc = c.Name
         FROM custhierarchy_tmp ct ,dim_customer c  
 WHERE ct.HighCustomerNumber = c.CustomerNumber
 and c.RowIsCurrent = 1;

UPDATE    custhierarchy_tmp ct 
   SET ct.TopLevelCustomerDesc = 'Not Set'
         FROM custhierarchy_tmp ct 
 WHERE ct.TopLevelCustomerDesc IS NULL;

UPDATE    custhierarchy_tmp ct 
   SET ct.HigherCustomerDesc = 'Not Set'
         FROM custhierarchy_tmp ct 
 WHERE ct.HigherCustomerDesc IS NULL;
 
 UPDATE    custhierarchy_tmp ct 
   SET ct.HighCustomerDesc = 'Not Set'
         FROM custhierarchy_tmp ct 
 WHERE ct.HighCustomerDesc IS NULL;
 
 UPDATE dim_customer c 
  SET c.HighCustomerNumber = ct.HighCustomerNumber,
      c.HierarchyType = ct.dd_HierarchyType,
      c.HigherCustomerNumber = ct.HigherCustomerNumber,
      c.TopLevelCustomerNumber = ct.TopLevelCustomerNumber,
      c.HighCustomerDesc = ct.HighCustomerDesc,
      c.HigherCustomerDesc = ct.HigherCustomerDesc,
      c.TopLevelCustomerDesc = ct.TopLevelCustomerDesc,
	  c.dw_update_date = current_timestamp
	  FROM
         custhierarchy_tmp ct, pCustHierType_701 p,dim_customer c
 WHERE ct.CustomerNumber = c.CustomerNumber
       AND ct.dd_HierarchyType = p.pCustomerHierarchyType;

truncate table custhierarchy_tmp;
