INSERT INTO dim_basecamp_todos
(
dim_basecamp_todosid
)
SELECT 1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_basecamp_todos WHERE dim_basecamp_todosid = 1);  



DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_basecamp_todos';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_basecamp_todos',
                ifnull(max(d.dim_basecamp_todosId),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_basecamp_todos d
where d.dim_basecamp_todosid <> 1;

INSERT INTO dim_basecamp_todos(
	dim_basecamp_todosid,
	app_url,
	assignee,
	comments_count,
	completed,
	completed_at,
	completer,
	content,
	created_at,
	creator,
	due_at,
	due_on,
	todo_id,
	todo_position,
	private,
	todolist,
	todolist_id,
	trashed,
	updated_at,
	url,
	todolist_name,
	assignee_id,
	assignee_name,
	created_id,
	creator_name,
	completer_id,
	completer_name
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_basecamp_todos') + row_number() over(order by '')  as dim_basecamp_todosoid,
app_url,
ifnull(assignee,'Not Set'),
comments_count,
completed,
completed_at,
ifnull(completer,'Not Set'),
content,
created_at,
creator,
due_at,
ifnull(due_on,'0001-01-01'),
todo_id,
todo_position,
private,
todolist,
todolist_id,
trashed,
updated_at,
url,
todolist_name,
assignee_id,
ifnull(assignee_name,'Not Set'),
created_id,
ifnull(creator_name,'Not Set'),
ifnull(completer_id,1),
ifnull(completer_name,'Not Set')
FROM stg_basecamp_todos s
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_basecamp_todos d
        WHERE s.todo_id = d.todo_id
	);

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_basecamp_todos';


UPDATE dim_basecamp_todos d
SET
d.app_url = s.app_url,
d.assignee = ifnull(s.assignee,'Not Set'),
d.comments_count = s.comments_count,
d.completed = s.completed,
d.completed_at = s.completed_at,
d.completer = ifnull(s.completer,'Not Set'),
d.content = s.content,
d.created_at = s.created_at,
d.creator = s.creator,
d.due_at = ifnull(s.due_at,'0001-01-01'),
d.due_on = ifnull(s.due_on,'0001-01-01'),
d.todo_position = s.todo_position,
d.private = s.private,
d.todolist = s.todolist,
d.todolist_id = s.todolist_id,
d.trashed = s.trashed,
d.updated_at = s.updated_at,
d.url = s.url,
d.todolist_name = s.todolist_name,
d.assignee_id = s.assignee_id,
d.assignee_name = ifnull(s.assignee_name,'Not Set'),
d.created_id = s.created_id,
d.creator_name = ifnull(s.creator_name,'Not Set'),
d.completer_id = ifnull(s.completer_id,1),
d.completer_name = ifnull(s.completer_name,'Not Set')

FROM dim_basecamp_todos d 
	INNER JOIN stg_basecamp_todos s ON d.todo_id = s.todo_id;


/*  ADD link from backend - DOESN'T WORK
UPDATE dim_basecamp_todos
SET APP_URL = concat('<a href="',APP_URL,'">',APP_URL,'</a>')
where APP_URL <> 'Not Set' 


<a href="url">link text</a> */


