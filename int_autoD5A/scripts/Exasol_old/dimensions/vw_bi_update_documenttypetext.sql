
UPDATE    dim_documenttypetext dt
   SET dt.Description = t.T003T_LTEXT,
			dt.dw_update_date = current_timestamp
	FROM
          dim_documenttypetext dt, T003T t
   WHERE dt.Type = t.T003T_BLART AND dt.RowIsCurrent = 1;
