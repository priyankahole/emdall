/* 
Developer: Octavian
Project: Every Angle Transition Addons
Dimension Name: Production Version 
Initial purpose: This data cannot be contained in dim_part due to VERID field in PK which multiplies the lines for a partnumber-plant combination. 
MARC-FVIDK = MKAL-VERID is not a valid join condition and even though in some cases it matches it should be avoided in order to have consistency data. 
This dimension will be linked to the Material Master SA, without affecting it s grain since it will be used as an outrigger dimension to dim_part
Script Name: /dimensions/vw_bi_populate_productionversion.sql 
Dimension Table Name: dim_productionversion
Source Table: MKAL data 
Process ID: BB31B7E5_A0EE_42D8_A472_B33ED8289E81
*/

/* Liviu Ionescu - BI-3588 - remove deleted rows */

DROP TABLE IF EXISTS tmp_delete_productionversion;
CREATE TABLE tmp_delete_productionversion AS
SELECT pv.dim_productionversionid
	FROM dim_productionversion pv
	LEFT JOIN MARA_MARC_MKAL m ON pv.partnumber = m.MKAL_MATNR
							AND pv.plant = m.MKAL_WERKS
							AND pv.productionversion = m.MKAL_VERID
	WHERE m.MKAL_MATNR IS NULL
		AND m.MKAL_WERKS IS NULL
		AND m.MKAL_VERID IS NULL
		AND pv.productionversion <> 'Not Set';

MERGE INTO dim_productionversion pv
USING tmp_delete_productionversion del
ON (pv.dim_productionversionid = del.dim_productionversionid)
WHEN MATCHED THEN DELETE;

DROP TABLE IF EXISTS tmp_delete_productionversion;

/* End Liviu Ionescu */

insert into dim_productionversion(dim_productionversionid,partnumber,plant,productionversion)
select 1, 'Not Set', 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_productionversion where dim_productionversionid = 1);

DELETE FROM number_fountain m where m.table_name = 'dim_productionversion';

INSERT INTO number_fountain
select 	'dim_productionversion',
	ifnull(max(d.dim_productionversionid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM dim_productionversion d
WHERE d.dim_productionversionid <> 1; 

INSERT INTO dim_productionversion
(dim_productionversionid,
dim_partid,
dim_plantid,
partnumber,
plant,
productionversion,
shorttext,
islocked,
validfrom,
validto,
fromlotsize,
tolotsize,
bomusage,
bomalternative,
tasklisttype,
tasklistgroup,
tasklistgroupcounter
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_productionversion') + row_number() over(order by '') AS dim_productionversionid,
dp.dim_partid AS dim_partid,

convert(bigint, 1) as dim_plantid,      ----ifnull((select ifnull(pl.dim_plantid,1) from dim_plant pl where pl.plantcode = m.MKAL_WERKS AND m.MKAL_WERKS is not null),1) AS dim_plantid,
dp.partnumber as partnumber,
dp.plant as plant,
ifnull(m.MKAL_VERID,'Not Set') as productionversion,
ifnull(m.MKAL_TEXT1,'Not Set') as shorttext,
ifnull(m.MKAL_MKSP,'Not Set') as islocked,
ifnull(m.MKAL_ADATU,'0001-01-01') as validfrom,
ifnull(m.MKAL_BDATU,'0001-01-01') as validto,
ifnull(m.MKAL_BSTMI,0) as fromlotsize,
ifnull(m.MKAL_BSTMA,0) as tolotsize,
ifnull(m.MKAL_STLAN,'Not Set') as bomusage,
ifnull(m.MKAL_STLAL,'Not Set') as bomalternative,
ifnull(m.MKAL_PLNTY,'Not Set') as tasklisttype,
ifnull(m.MKAL_PLNNR,'Not Set') as tasklistgroup,
ifnull(m.MKAL_ALNAL,'Not Set') as tasklistgroupcounter
from dim_part dp LEFT JOIN MARA_MARC_MKAL m ON
m.MKAL_MATNR = dp.partnumber AND
m.MKAL_WERKS = dp.plant 
WHERE 
dp.dim_partid <> 1 AND
not exists (select 1 from dim_productionversion pv
WHERE pv.partnumber = dp.partnumber
AND pv.plant = dp.plant
AND pv.productionversion = ifnull(m.MKAL_VERID,'Not Set')
);

/*dim_plantid*/

UPDATE dim_productionversion pv
SET pv.dim_plantid = ifnull(pl.dim_plantid,1),
dw_update_date = CURRENT_TIMESTAMP
FROM dim_productionversion pv,MARA_MARC_MKAL m
LEFT JOIN dim_plant pl on pl.plantcode = m.MKAL_WERKS AND m.MKAL_WERKS is not null
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.dim_plantid <> ifnull(pl.dim_plantid,1);


UPDATE dim_productionversion pv
SET pv.shorttext = ifnull(m.MKAL_TEXT1,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.shorttext <> ifnull(m.MKAL_TEXT1,'Not Set');

UPDATE dim_productionversion pv
SET pv.islocked = ifnull(m.MKAL_MKSP,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.islocked <> ifnull(m.MKAL_MKSP,'Not Set');

UPDATE dim_productionversion pv
SET pv.validfrom = ifnull(m.MKAL_ADATU,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.validfrom <> ifnull(m.MKAL_ADATU,'0001-01-01');

UPDATE dim_productionversion pv
SET pv.validto = ifnull(m.MKAL_BDATU,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.validto <> ifnull(m.MKAL_BDATU,'0001-01-01');

UPDATE dim_productionversion pv
SET pv.fromlotsize = ifnull(m.MKAL_BSTMI,0),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.fromlotsize <> ifnull(m.MKAL_BSTMI,0);

UPDATE dim_productionversion pv
SET pv.tolotsize = ifnull(m.MKAL_BSTMA,0),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.tolotsize <> ifnull(m.MKAL_BSTMA,0);

UPDATE dim_productionversion pv
SET pv.bomusage = ifnull(m.MKAL_STLAN,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.bomusage <> ifnull(m.MKAL_STLAN,'Not Set');

UPDATE dim_productionversion pv
SET pv.bomalternative = ifnull(m.MKAL_STLAL,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.bomalternative <> ifnull(m.MKAL_STLAL,'Not Set');

UPDATE dim_productionversion pv
SET pv.tasklisttype = ifnull(m.MKAL_PLNTY,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.tasklisttype <> ifnull(m.MKAL_PLNTY,'Not Set');

UPDATE dim_productionversion pv
SET pv.tasklistgroup = ifnull(m.MKAL_PLNNR,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.tasklistgroup <> ifnull(m.MKAL_PLNNR,'Not Set');

UPDATE dim_productionversion pv
SET pv.tasklistgroupcounter = ifnull(m.MKAL_ALNAL,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM MARA_MARC_MKAL m,dim_productionversion pv
WHERE pv.partnumber = m.MKAL_MATNR
AND pv.plant = m.MKAL_WERKS
AND pv.productionversion = m.MKAL_VERID
AND pv.tasklistgroupcounter <> ifnull(m.MKAL_ALNAL,'Not Set');

/* Madalina 15 Sep 2016 -Delete rows from dim_productionversion, where new valid and Not Set Productionversion values were inserted from MKAL - BI-4024 */
merge into dim_productionversion pv
using (select pv.dim_productionversionid from dim_productionversion pv
		where pv.partnumber <> 'Not Set' 
			and pv.plant <> 'Not Set'
			and pv.productionversion = 'Not Set'
			and exists ( select 'x' 
						 from dim_productionversion pv1 
						 where pv.partnumber = pv1.partnumber
							and pv.plant = pv1.plant
							and pv1.productionversion <> 'Not Set')) del
on pv.dim_productionversionid = del.dim_productionversionid
when matched then delete;




