DROP TABLE IF EXISTS tmp_dimpc_insert;
CREATE TABLE tmp_dimpc_insert
AS
SELECT DISTINCT CEPCT_PRCTR,max(CEPCT_DATBI) CEPCT_DATBI,max(CEPCT_KOKRS) CEPCT_KOKRS
FROM CEPCT
group by CEPCT_PRCTR;

DROP TABLE IF EXISTS tmp_dimpc_insert_full;
CREATE TABLE tmp_dimpc_insert_full
AS
SELECT DISTINCT c.*
FROM CEPCT c, tmp_dimpc_insert t
WHERE c.CEPCT_PRCTR = t.CEPCT_PRCTR AND c.CEPCT_DATBI = t.CEPCT_DATBI AND c.CEPCT_KOKRS = t.CEPCT_KOKRS;

UPDATE    dim_profitcenter pc
   SET pc.ProfitCenterName = ifnull(CEPCT_KTEXT, 'Not Set'),
       pc.ControllingArea = ifnull(CEPCT_KOKRS, 'Not Set'),
       pc.ValidTo = CEPCT_DATBI,
			pc.dw_update_date = current_timestamp
	FROM
          dim_profitcenter pc, tmp_dimpc_insert_full c
 WHERE pc.RowIsCurrent = 1
 AND c.CEPCT_PRCTR = pc.ProfitCenterCode
;
