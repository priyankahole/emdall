DELETE FROM dim_workcenter;

delete from number_fountain m where m.table_name = 'dim_workcenter';
insert into number_fountain
select 'dim_workcenter',
 ifnull(max(d.dim_workcenterid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_workcenter d
where d.dim_workcenterid <> 1;



INSERT INTO dim_workcenter
(dim_workcenterid)
SELECT 1;

drop table if exists tmp_dimworkcenter;
create table tmp_dimworkcenter
 AS
 (SELECT ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)
 + row_number() over (order by crhd_objid ) dim_workcenterid,
 ifnull(crhd_objid,'Not Set') as objectid,
 ifnull(crhd_objty,'Not Set') as objecttype,
 ifnull(crhd_begda,'1900-01-01') as headerstartdate,
 ifnull(crhd_endda,'1900-01-01') as headerenddate,
 ifnull(crhd_aedat_grnd,'1900-01-01') as  headerchgdon_grnd,
 ifnull(crhd_aenam_grnd,'Not Set') as headerusername_grnd,
 ifnull(crhd_aedat_vora,'1900-01-01') as headerchgdon_vora,
 ifnull(crhd_aenam_vora,'Not Set') as  headerusername_vora,
 ifnull(crhd_aedat_term,'1900-01-01') as  headerchgdon_term,
 ifnull(crhd_aenam_term,'Not Set') as headerusername_term,
 ifnull(crhd_aedat_tech,'1900-01-01') as  headerchgdon_tech,
 ifnull(crhd_aenam_tech,'Not Set') as headerusername_tech,
 ifnull(crhd_arbpl,'Not Set') as workcenter,
 ifnull(crhd_werks,'Not Set') as plant,
 ifnull(crhd_verwe,'Not Set') as workcentercategory,
 ifnull(crhd_ktsch,'Not Set') as headerstdtextkey,
 ifnull(crhd_steus,'Not Set') as headercontrolkey,
 ifnull(crtx_spras,'Not Set') as languagekey,
 ifnull(crtx_aedat_text,'1900-01-01') as wctextchangedon,
 ifnull(crtx_aenam_text,'Not Set') as wctextusername_vora,
 ifnull(crtx_ktext,'Not Set') as ktext_description,
 ifnull(crtx_ktext_up,'Not Set') as ktext_up_description,
 ifnull(t430t_plnaw,'Not Set') as application,
 ifnull(t430t_txt,'Not Set') as controlkeydescription,
 ifnull(crco_LASET,'Not Set') as activitytypeset,
 ifnull(crco_ENDDA,'1900-01-01') as costcenterenddate,
 ifnull(crco_BEGDA,'1900-01-01') as costcenterstartdate,
 ifnull(crco_LANUM,'Not Set') as costcenternumber,
 ifnull(crco_AEDAT_KOST,'1900-01-01') as costcenterchangedon,
 ifnull(crco_AENAM_KOST,'Not Set') as costcenterusername,
 ifnull(crco_KOKRS,'Not Set') as costcentercontrollingarea,
 ifnull(crco_KOSTL,'Not Set') as costcentername,
 ifnull(crco_LSTAR,'Not Set') as costcenteractivitytype,
 ifnull(crco_LSTAR_REF,'Not Set') as costcenterrefindicator,
 ifnull(crco_FORML,'Not Set') as ccformulakeycosting,
 ifnull(crco_PRZ,'Not Set') as ccbusinessprocess,
 ifnull(crco_ACTXY,'Not Set') as ccactivitydescriptiontype,
 ifnull(crhd_prvbe, 'Not Set') as productionsupplyarea,
 /* Octavian: Every Angle */
 ifnull(CRHD_KAPID,0) as capacityid, 
 ifnull(CRHD_LVORM,'Not Set') as deletionflag
 /* Octavian: Every Angle */
 
 FROM CRHD LEFT OUTER JOIN CRTX ON crhd_objid = crtx_objid 
 LEFT OUTER JOIN CRCO on crhd_objid = crco_objid 
 LEFT OUTER JOIN T430T ON crhd_steus = t430t_steus AND crtx_spras = t430t_spras
 ORDER BY crhd_objid);
 

 
INSERT INTO dim_workcenter
(
dim_workcenterid,
objectid,
objecttype,
headerstartdate,
headerenddate,
headerchgdon_grnd,
headerusername_grnd,
headerchgdon_vora,
headerusername_vora,
headerchgdon_term,
headerusername_term,
headerchgdon_tech,
headerusername_tech,
workcenter,
plant,
workcentercategory,
headerstdtextkey,
headercontrolkey,
languagekey,
wctextchangedon,
wctextusername_vora,
ktext_description,
ktext_up_description,
application,
controlkeydescription,
activitytypeset,
costcenterenddate,
costcenterstartdate,
costcenternumber,
costcenterchangedon,
costcenterusername,
costcentercontrollingarea,
costcentername,
costcenteractivitytype,
costcenterrefindicator,
ccformulakeycosting,
ccbusinessprocess,
ccactivitydescriptiontype,
productionsupplyarea,
capacityid,
deletionflag
)
SELECT 
dim_workcenterid,
objectid,
objecttype,
headerstartdate,
headerenddate,
headerchgdon_grnd,
headerusername_grnd,
headerchgdon_vora,
headerusername_vora,
headerchgdon_term,
headerusername_term,
headerchgdon_tech,
headerusername_tech,
workcenter,
plant,
workcentercategory,
headerstdtextkey,
headercontrolkey,
languagekey,
wctextchangedon,
wctextusername_vora,
ktext_description,
ktext_up_description,
application,
controlkeydescription,
activitytypeset,
costcenterenddate,
costcenterstartdate,
costcenternumber,
costcenterchangedon,
costcenterusername,
costcentercontrollingarea,
costcentername,
costcenteractivitytype,
costcenterrefindicator,
ccformulakeycosting,
ccbusinessprocess,
ccactivitydescriptiontype,
productionsupplyarea,
capacityid,
deletionflag
from tmp_dimworkcenter where dim_workcenterid <> 1;

drop table if exists tmp_dimworkcenter;

/* Octavian: EA */
UPDATE dim_workcenter wc
SET wc.categorydescription = ifnull(t.TC30T_KTEXT,'Not Set')
FROM TC30T t,dim_workcenter wc
WHERE wc.workcentercategory = t.TC30T_VERWE
AND wc.categorydescription <> ifnull(t.TC30T_KTEXT,'Not Set');

update dim_workcenter wc
SET wc.categoryanddescription = CONCAT(wc.workcentercategory,' (',wc.categorydescription,')')
FROM dim_workcenter wc
where wc.categoryanddescription <> CONCAT(wc.workcentercategory,' (',wc.categorydescription,')');
