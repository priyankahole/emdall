insert into dim_inspectioncharacteristic
(dim_inspectioncharacteristicid, 
InspectionPlant,
Masterinspcharacteristics,
VersionNumber,
FirstUpperLimit,
FirstLowerLimit,
ValidFromDate,
Status,
DataRecord,
PlacesNumber,
PlantCode,
ShortText)

select 	1, 'Not Set', 'Not Set', 'Not Set', 0,0, '1900-01-01', 'Not Set', 'Not Set', 0, 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_inspectioncharacteristic where dim_inspectioncharacteristicid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectioncharacteristic';
insert into number_fountain
select 'dim_inspectioncharacteristic',
 ifnull(max(d.dim_inspectioncharacteristicid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectioncharacteristic d
where d.dim_inspectioncharacteristicid <> 1;

insert into dim_inspectioncharacteristic (dim_inspectioncharacteristicid,
                          InspectionPlant,
                          Masterinspcharacteristics,
     					  VersionNumber,
     					  FirstUpperLimit,
						  FirstLowerLimit,
						  ValidFromDate,
						  Status,
						  DataRecord,
						  PlacesNumber,
						  PlantCode,
						  ShortText,
     					  dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_inspectioncharacteristic') + row_number() over(order by '') AS dim_inspectioncharacteristicid,
        ifnull(q.QPMK_ZAEHLER, 'Not Set') AS InspectionPlant,
        ifnull(q.QPMK_MKMNR, 'Not Set') AS Masterinspcharacteristics,
        ifnull(q.QPMK_VERSION, 'Not Set') AS VersionNumber,
		ifnull(q.QPMK_GRENZEOB1, 0) as FirstUpperLimit,
		ifnull(q.QPMK_GRENZEUN1, 0) as FirstLowerLimit,
		q.QPMK_GUELTIGAB as ValidFromDate,
		ifnull(q.QPMK_LOEKZ, 'Not Set') as Status,
		ifnull(q.QPMK_LSPER, 'Not Set') as DataRecord,
		ifnull(q.QPMK_STELLEN, 0) as PlacesNumber,
		ifnull(q.QPMK_WERKS, 'Not Set') as PlantCode,
		ifnull(q.QPMT_KURZTEXT, 'Not Set') as ShortText,
       current_timestamp
from QPMK_QPMT q
 where not exists (select 'x' from dim_inspectioncharacteristic dic
                        where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
							AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
							AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set'));
							 
UPDATE dim_inspectioncharacteristic dic
SET dic.FirstUpperLimit = ifnull(q.QPMK_GRENZEOB1, 0) 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPMK_QPMT q, dim_inspectioncharacteristic dic
where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
AND dic.FirstUpperLimit <> ifnull(q.QPMK_GRENZEOB1, 0) ;

UPDATE dim_inspectioncharacteristic dic
SET dic.FirstLowerLimit = ifnull(q.QPMK_GRENZEUN1, 0) 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPMK_QPMT q, dim_inspectioncharacteristic dic
where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
AND dic.FirstLowerLimit <> ifnull(q.QPMK_GRENZEUN1,0)  ;

UPDATE dim_inspectioncharacteristic dic
SET dic.ValidFromDate = q.QPMK_GUELTIGAB 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPMK_QPMT q, dim_inspectioncharacteristic dic
where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
AND dic.ValidFromDate <> q.QPMK_GUELTIGAB;

UPDATE dim_inspectioncharacteristic dic
SET dic.Status = ifnull(q.QPMK_LOEKZ, 'Not Set') 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPMK_QPMT q, dim_inspectioncharacteristic dic
where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
AND dic.Status <> ifnull(q.QPMK_LOEKZ, 'Not Set');

UPDATE dim_inspectioncharacteristic dic
SET dic.DataRecord = ifnull(q.QPMK_LSPER, 'Not Set') 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPMK_QPMT q, dim_inspectioncharacteristic dic
where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
AND dic.DataRecord <> ifnull(q.QPMK_LSPER, 'Not Set') ;

UPDATE dim_inspectioncharacteristic dic
SET dic.PlacesNumber = ifnull(q.QPMK_STELLEN, 0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPMK_QPMT q, dim_inspectioncharacteristic dic
where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
AND dic.PlacesNumber <> ifnull(q.QPMK_STELLEN, 0) ;

UPDATE dim_inspectioncharacteristic dic
SET dic.PlantCode = ifnull(q.QPMK_WERKS, 'Not Set') 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPMK_QPMT q, dim_inspectioncharacteristic dic
where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
AND dic.PlantCode <> ifnull(q.QPMK_WERKS, 'Not Set') ;

UPDATE dim_inspectioncharacteristic dic
SET dic.ShortText = ifnull(q.QPMT_KURZTEXT, 'Not Set') 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPMK_QPMT q, dim_inspectioncharacteristic dic
where dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
AND dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
AND dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
AND dic.ShortText <> ifnull(q.QPMT_KURZTEXT, 'Not Set') ;




