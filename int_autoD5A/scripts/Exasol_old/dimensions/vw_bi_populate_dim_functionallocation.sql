/*##################################################################################################################
	Script         : vw_bi_populate_dim_functionallocation                                                                                                                                              
	Created By     : Suchithra                                                                                                        
	Created On     : 09 Mar 2016  
	Description    : Script to populated functional location dimension for Plant Maintanence 
			Grain :  FunctonalLocation(IFLOT_TPLNR),AcctAssignment (ILOA_ILOAN)
	Change History                                                                                                                                   
	Date            By        	Version		Desc                                                                 
	28Apr2016	Suchithra	1.1		Added iflot_objnr
							Logic to populate the Object status
	06May2016	Suchithra	1.2		Logic to add FLOC desc in E and N Languages 
							Original ProdWorkCenter changed to Prod Work Center ID
							Added Prod Work Center and Desc from Work Center Dimension	
							FLCO Location Desc from T499S table 
####################################################################################################################*/ 


INSERT INTO dim_functionallocation(
	dim_functionallocationid,
	FLOCDescription ,
	AcctAssignment,
	FunctonalLocation ,
	SuperiorFuncLocation ,
	FLOCABCIndicator ,
	PlantSection ,
	FLOCSortField ,
	FLOCCostCenter ,
	FLOCLocationDesc ,
	Room ,
	ProductionWorkCenter ,
	ProdWorkCenterID,
	ProdWorkCenterDesc,
	FLOCLocation ,
	FLOCPlanningPlant,
	objectNumber,
	TechObjectAuthGroup,
	RowStartDate,
	RowIsCurrent
)
  SELECT    
	1,
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set', 
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	current_timestamp,
	1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_functionallocation WHERE dim_functionallocationId = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_functionallocation';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_functionallocation',
                ifnull(max(d.dim_functionallocationId),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_functionallocation d
where d.dim_functionallocationId <> 1;

drop table if exists tmp_dfall_ins_t1;
create table tmp_dfall_ins_t1 as
SELECT 
ifnull(ILOA_ILOAN,'Not Set') as AcctAssignment,
ifnull(ILOA_ABCKZ,'Not Set') as FLOCABCIndicator ,
ifnull(ILOA_KOSTL,'Not Set') as FLOCCostCenter ,
convert (varchar(200), 'Not Set') as FLOCDescription,  /*ifnull((select fx.IFLOTX_PLTXU from IFLOTX fx where ft.iflot_tplnr = fx.IFLOTX_tplnr and fx.IFLOTX_SPRAS= 'E'),'Not Set') as FLOCDescription , 06May2016 - First consider E lang texts */
ifnull(ILOA_STORT,'Not Set') as FLOCLocation ,
convert (varchar(200), 'Not Set') as FLOCLocationDesc,  /*ifnull((select t.T499S_KTEXT from T499S t where t.T499S_STAND = ILOA_STORT and t.T499S_WERKS = IFLOT_IWERK),'Not Set') as FLOCLocationDesc, 06May2016 */
ifnull(IFLOT_IWERK,'Not Set') as FLOCPlanningPlant,
ifnull(ILOA_EQFNR,'Not Set') as FLOCSortField ,
ifnull(IFLOT_TPLNR,'Not Set') as FunctonalLocation ,
ifnull(ILOA_BEBER,'Not Set') as PlantSection ,
ifnull(convert(varchar(8),ILOA_PPSID),'Not Set') as ProdWorkCenterID,
ifnull(ILOA_MSGRP,'Not Set') as Room ,
ifnull(IFLOT_TPLMA,'Not Set') as SuperiorFuncLocation ,
ifnull(IFLOT_OBJNR,'Not Set') as objectnumber,
ifnull(IFLOT_BEGRU,'Not Set') as TechObjectAuthGroup,
convert (varchar(200), 'Not Set') as descofdunctionalloc,
ifnull(IFLOT_FLTYP,'Not Set') as functionalloccategory,
ifnull(IFLOT_INGRP,'Not Set') as plannergroupforPM,
ifnull(IFLOT_LGWID,0) as workcenterobjectid,
current_timestamp as RowStartDate,
1 as RowIsCurrent
FROM IFLOT ft, ILOA il
where ft.iflot_tplnr = il.ILOA_TPLNR
and not exists (select 1 from dim_functionallocation d1
where d1.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and d1.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set'));



/* FLOCDescription  */
UPDATE tmp_dfall_ins_t1
SET FLOCDescription = ifnull(fx.IFLOTX_PLTXU,'Not Set')
FROM tmp_dfall_ins_t1 INNER JOIN IFLOT ft ON FunctonalLocation = iflot_tplnr INNER JOIN ILOA il ON ft.iflot_tplnr = il.ILOA_TPLNR AND AcctAssignment = ILOA_ILOAN
LEFT JOIN IFLOTX fx on ft.iflot_tplnr = fx.IFLOTX_tplnr
where fx.IFLOTX_SPRAS= 'E'
AND FLOCDescription <> ifnull(fx.IFLOTX_PLTXU,'Not Set');

/* FLOCLocationDesc  */
MERGE INTO tmp_dfall_ins_t1 t
USING (SELECT FunctonalLocation,AcctAssignment,FLOCLocationDesc
FROM tmp_dfall_ins_t1 INNER JOIN IFLOT ft ON FunctonalLocation = iflot_tplnr INNER JOIN ILOA il ON ft.iflot_tplnr = il.ILOA_TPLNR AND AcctAssignment = ILOA_ILOAN
LEFT JOIN T499S t ON t.T499S_STAND = ILOA_STORT AND t.T499S_WERKS = IFLOT_IWERK
where FLOCLocationDesc <> ifnull(t.T499S_KTEXT,'Not Set')
) src
ON t.FunctonalLocation = src.FunctonalLocation AND t.AcctAssignment = src.AcctAssignment
WHEN MATCHED THEN UPDATE 
SET t.FLOCLocationDesc = src.FLOCLocationDesc;

UPDATE tmp_dfall_ins_t1
SET descofdunctionalloc = ifnull(fx.IFLOTX_PLTXT,'Not Set')
FROM tmp_dfall_ins_t1 INNER JOIN IFLOT ft ON FunctonalLocation = iflot_tplnr INNER JOIN ILOA il ON ft.iflot_tplnr = il.ILOA_TPLNR AND AcctAssignment = ILOA_ILOAN
LEFT JOIN IFLOTX fx on ft.iflot_tplnr = fx.IFLOTX_tplnr
where fx.IFLOTX_SPRAS= 'E'
AND descofdunctionalloc <> ifnull(fx.IFLOTX_PLTXT,'Not Set');


 INSERT INTO dim_functionallocation(
dim_functionallocationid,
AcctAssignment,
FLOCABCIndicator ,
FLOCCostCenter ,
FLOCDescription ,
FLOCLocation ,
FLOCLocationDesc ,
FLOCPlanningPlant,
FLOCSortField ,
FunctonalLocation ,
PlantSection ,
ProdWorkCenterID,
Room ,
SuperiorFuncLocation ,
objectnumber,
TechObjectAuthGroup,
descofdunctionalloc,
functionalloccategory,
plannergroupforPM,
workcenterobjectid,
RowStartDate,
RowIsCurrent
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_functionallocation') + row_number() over(order by '') as dim_functionallocationid,
a.* FROM (SELECT distinct AcctAssignment,
FLOCABCIndicator ,
FLOCCostCenter ,
FLOCDescription ,
FLOCLocation ,
FLOCLocationDesc ,
FLOCPlanningPlant,
FLOCSortField ,
FunctonalLocation ,
PlantSection ,
ProdWorkCenterID,
Room ,
SuperiorFuncLocation ,
objectnumber,
TechObjectAuthGroup,
descofdunctionalloc,
functionalloccategory,
plannergroupforPM,
workcenterobjectid,
RowStartDate,
RowIsCurrent
FROM tmp_dfall_ins_t1 ) a;

DROP TABLE IF EXISTS tmp_dfall_ins_t1;
				
/* 28Apr2016 - Logic to Populate the stats for functionallocation */
drop table if exists tmp_objstatus;
create table tmp_objstatus as
select distinct objectnumber, t.jest_stat
from dim_functionallocation f,jest_iflot t
where f.objectnumber = t.jest_objnr
and jest_inact is null;

drop table if exists tmp_objlistagg;
create table tmp_objlistagg as
SELECT objectnumber,group_concat(jest_stat ORDER BY jest_stat SEPARATOR ',') as stats
FROM tmp_objstatus
group by objectnumber;

update dim_functionallocation d
set 	
	flgsafetysiteprotection = case when t.stats like '%E0001%' then 'X' else 'Not Set' end 
	,flgenvprotection = case when t.stats like '%E0002%' then 'X' else 'Not Set' end 
	,flgcgxprelated = case when t.stats like '%E0003%' then 'X' else 'Not Set' end 
	,flgrepairasrequired = case when t.stats like '%E0004%' then 'X' else 'Not Set' end 
	,flgruntofailure = case when t.stats like '%E0005%' then 'X' else 'Not Set' end 
	,flgcgxpcritical = case when t.stats like '%E0006%' then 'X' else 'Not Set' end 
	,dw_update_date = current_timestamp
from tmp_objlistagg t,dim_functionallocation d
where t.objectnumber =  d.objectnumber;

drop table if exists tmp_objstatus;
drop table if exists tmp_objlistagg;
/* End of changes 28Arp2016*/

/* APP-9913 29 June 2018 Ionel Ene */

UPDATE dim_functionallocation
SET FLOCDescription = ifnull(fx.IFLOTX_PLTXU,'Not Set')
FROM dim_functionallocation 
INNER JOIN IFLOT ft ON FunctonalLocation = iflot_tplnr 
INNER JOIN ILOA il ON ft.iflot_tplnr = il.ILOA_TPLNR AND AcctAssignment = ILOA_ILOAN
LEFT JOIN IFLOTX fx on ft.iflot_tplnr = fx.IFLOTX_tplnr
where fx.IFLOTX_SPRAS= 'E'
AND FLOCDescription <> ifnull(fx.IFLOTX_PLTXU,'Not Set');


/* 06May2016 - Update FLOC desc if description is not available in E */
update dim_functionallocation d
set d.FLOCDescription = ifnull(fx.IFLOTX_PLTXU,'Not Set' )
from IFLOTX fx,dim_functionallocation d 
where d.FunctonalLocation = fx.IFLOTX_tplnr 
and d.FLOCDescription = 'Not Set' /* could not find E descriptions */
and fx.IFLOTX_SPRAS<> 'E'
and d.FLOCDescription <> ifnull(fx.IFLOTX_PLTXU,'Not Set' );

/*Update WorkCenter Code and desc from Work center Dimension */
drop table if exists tmp_dimwworkcenter_dstnct;
create table tmp_dimwworkcenter_dstnct
as
select distinct objectid, workcenter, ktext_description
from dim_workcenter;

/* Work Center code */
update dim_functionallocation f
set f.ProductionWorkCenter = ifnull(w.workcenter,'Not Set'), dw_update_date = current_timestamp
from tmp_dimwworkcenter_dstnct w, dim_functionallocation f
where f.prodworkcenterid = w.objectid
and f.ProductionWorkCenter <> ifnull(w.workcenter,'Not Set');

/* Work Center Desc */
update dim_functionallocation f
set f.prodworkcenterdesc = ifnull(w.ktext_Description,'Not Set'), dw_update_date = current_timestamp
from tmp_dimwworkcenter_dstnct w,dim_functionallocation f
where f.prodworkcenterid = w.objectid
and f.prodworkcenterdesc <> ifnull(w.ktext_Description,'Not Set');

/* Andrian - 19May2016 - Update Superior Floc Description */
update dim_functionallocation d
set d.superiroflocdesc = ifnull(fx.IFLOTX_PLTXU,'Not Set' )
from IFLOTX fx ,dim_functionallocation d
where d.superiorfunclocation = fx.IFLOTX_tplnr 
and d.superiroflocdesc <> ifnull(fx.IFLOTX_PLTXU,'Not Set' );

drop table if exists tmp_dimwworkcenter_dstnct;

/* 06 Nov 2017 Georgiana Changes according to APP-7878- adeed new fields*/
/*Update descofdunctionalloc where SPRAS=E is not available*/
update dim_functionallocation d
set d.descofdunctionalloc = ifnull(fx.IFLOTX_PLTXT,'Not Set' )
from IFLOTX fx,dim_functionallocation d 
where d.FunctonalLocation = fx.IFLOTX_tplnr 
and d.descofdunctionalloc = 'Not Set' /* could not find E descriptions */
and fx.IFLOTX_SPRAS<> 'E'
and d.descofdunctionalloc <> ifnull(fx.IFLOTX_PLTXT,'Not Set' );

update dim_functionallocation d
set d.functionalloccategory = ifnull(ft.IFLOT_FLTYP,'Not Set' )
from IFLOT ft, ILOA il,dim_functionallocation d 
where d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and d.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and d.functionalloccategory <> ifnull(ft.IFLOT_FLTYP,'Not Set' );

update dim_functionallocation d
set d.plannergroupforPM = ifnull(ft.IFLOT_INGRP,'Not Set' )
from IFLOT ft, ILOA il,dim_functionallocation d 
where d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and d.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and d.plannergroupforPM <> ifnull(ft.IFLOT_INGRP,'Not Set' );

update dim_functionallocation d
set d.workcenterobjectid = ifnull(ft.IFLOT_LGWID,0)
from IFLOT ft, ILOA il,dim_functionallocation d 
where d.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and d.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and d.workcenterobjectid <> ifnull(ft.IFLOT_LGWID,0);

/*06 Nov 2017 End of changes*/

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_functionallocation';     

