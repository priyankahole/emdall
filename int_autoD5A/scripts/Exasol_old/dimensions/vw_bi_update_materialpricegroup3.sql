
UPDATE dim_MaterialPriceGroup3 a 
   SET a.Description = ifnull(TVM3T_BEZEI, 'Not Set')
FROM TVM3T, dim_MaterialPriceGroup3 a 
 WHERE a.MaterialPriceGroup3 = TVM3T_MVGR3 AND RowIsCurrent = 1;
