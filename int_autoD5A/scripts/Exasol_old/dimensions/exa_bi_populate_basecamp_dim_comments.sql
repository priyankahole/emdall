
INSERT INTO dim_basecamp_comments
(
dim_basecamp_commentsid
)
SELECT 1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_basecamp_comments WHERE dim_basecamp_commentsid = 1);  

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_basecamp_comments';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_basecamp_comments',
                ifnull(max(d.dim_basecamp_commentsid),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_basecamp_comments d
where d.dim_basecamp_commentsid <> 1;



INSERT INTO dim_basecamp_comments(
	dim_basecamp_commentsid,
	todo_id,
	todo_name,
	todo_url,
	comment_id,
	comment_created_on,
	comment_creator_name,
	comment_creator_image,
	comment_content
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_basecamp_comments') + row_number() over(order by '')  as dim_basecamp_commentsid,
	todo_id,
	todo_name,
	todo_url,
	comment_id,
	comment_created_on,
	comment_creator_name,
	comment_creator_image,
	comment_content
FROM stg_basecamp_comments s
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_basecamp_comments d
        WHERE s.comment_id = d.comment_id
	);

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_basecamp_comments';


UPDATE dim_basecamp_comments d
SET
d.todo_id = s.todo_id,
d.todo_name = s.todo_name,
d.todo_url = s.todo_url,
d.comment_created_on = s.comment_created_on,
d.comment_creator_name = s.comment_creator_name,
d.comment_creator_image = s.comment_creator_image,
d.comment_content = s.comment_content

FROM dim_basecamp_comments d 
	INNER JOIN stg_basecamp_comments s ON d.comment_id = s.comment_id;


/* Update is_client */
update dim_basecamp_comments
set is_client = 'False'
where comment_creator_name in ('Razvan Deac','Chris Martin','Iulian Ilea','Greg Shorr','Suchithra Vinayan','Andra Haraga',
'Lokesh Mandava','Liviu Ionescu','Andrian Russu','Georgiana Nartea','Alexandru Ungureanu','Javier Rincon','Octavian Stepan',
'Hiten Suthar','Andrei Robescu','Alin Gheorghe','Mykala Waldron','Allen Jacques','Shanthi Chillara','George Ilie','Steven Shi',
'Nicoleta Constantin','Ana Maria Rusu','Octavian Zarzu','Nicu Naum','Cornelia Mahulea','Gary Meyers','Alexandra Muresan','Madalina Herghelegiu',
'Victor Criclivii','Radu Cocosila','Yogini Jagtap','Gwenda Waldron','Alex Diaconu','Lokesh Kamani');


update dim_basecamp_comments
set is_client = 'True'
where comment_creator_name not in ('Razvan Deac','Chris Martin','Iulian Ilea','Greg Shorr','Suchithra Vinayan','Andra Haraga',
'Lokesh Mandava','Liviu Ionescu','Andrian Russu','Georgiana Nartea','Alexandru Ungureanu','Javier Rincon','Octavian Stepan',
'Hiten Suthar','Andrei Robescu','Alin Gheorghe','Mykala Waldron','Allen Jacques','Shanthi Chillara','George Ilie','Steven Shi',
'Nicoleta Constantin','Ana Maria Rusu','Octavian Zarzu','Nicu Naum','Cornelia Mahulea','Gary Meyers','Alexandra Muresan','Madalina Herghelegiu',
'Victor Criclivii','Radu Cocosila','Yogini Jagtap','Gwenda Waldron','Alex Diaconu','Lokesh Kamani',
'Not Set');

update dim_basecamp_comments
set is_client = 'Not Set'
where comment_creator_name = 'Not Set';

/* Update is_client */

/* Create sequence for comments within a todo */

merge into dim_basecamp_comments b
using (
select c.dim_basecamp_commentsid, row_number() over (partition by todo_id order by comment_created_on asc) as comment_order
from dim_basecamp_comments c) t
on b.dim_basecamp_commentsid = t.dim_basecamp_commentsid
when matched then update
set b.comment_order = t.comment_order;

/* Create sequence for comments within a todo */

/* Create delay between comments within a todo */

merge into dim_basecamp_comments b
using (
select c.dim_basecamp_commentsid, lead(to_timestamp(concat(substring(c.comment_created_on,1,10),' ', substring(c.comment_created_on,12,6))))
 over (partition by todo_id order by comment_created_on asc) - to_timestamp(concat(substring(c.comment_created_on,1,10),' ', substring(c.comment_created_on,12,6))) as comment_delay
from dim_basecamp_comments c
 where c.dim_basecamp_commentsid <> 1) t
on b.dim_basecamp_commentsid = t.dim_basecamp_commentsid
when matched then update
set b.comment_delay = t.comment_delay;

update dim_basecamp_comments b
set comment_delay = INTERVAL '0 00:00:00' DAY(9) TO SECOND(9)
where comment_delay is null AND todo_id in (
select todo_id from dim_basecamp_todos where completed = 'True') ; /* default last entry in a todo sequence for todos completed */


update dim_basecamp_comments
set comment_delay_seconds = EXTRACT(DAY FROM comment_delay) * 24 * 3600 + EXTRACT(HOUR FROM comment_delay) * 3600 + EXTRACT(MINUTE FROM comment_delay);
/* Create delay between comments within a todo */

