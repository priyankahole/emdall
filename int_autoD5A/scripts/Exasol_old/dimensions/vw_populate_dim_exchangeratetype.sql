/**********************************************************************************/
/*  Script : vw_populate_dim_exchangeratetype.sql                                 */
/*  Description :Populate dim_exchangeratetype dimension					      */
/*  Created On : January 15, 2016                                                 */
/*  Create By : Popescu Florian                                                   */
/*                                                                                */
/*  Change History                                                                */
/*  Date           CVSversion  By          Description                            */
/*  15 January 2015  1.0       FPOPESCU    New Script                             */
/**********************************************************************************/

/*insert default row*/

INSERT INTO dim_exchangeratetype
(
dim_exchangeratetypeid,
exchange_rate_type,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,key_id,source_id
)
SELECT T.* FROM 
(SELECT
1 dim_exchangeratetypeid,
'Not Set' exchange_rate_type,
timestamp '0001-01-01 00:00:00.000000' DW_INSERT_DATE,
timestamp '0001-01-01 00:00:00.000000' DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
0 KEY_ID,
'SAP' SOURCE_ID
FROM (SELECT 1) A) T
LEFT JOIN dim_exchangeratetype D
ON T.dim_exchangeratetypeid = D.dim_exchangeratetypeid
WHERE D.dim_exchangeratetypeid is null;

/*initialize NUMBER_FOUNTAIN*/
delete from NUMBER_FOUNTAIN where table_name = 'dim_exchangeratetype';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_exchangeratetype',IFNULL(MAX(dim_exchangeratetypeid),0)
FROM dim_exchangeratetype;

/*update dimension rows*/

UPDATE dim_exchangeratetype T
SET
T.exchange_rate_type=ifnull(S1.tcurv_KURST, 'Not Set'),
T.rowiscurrent = 1,
T.DW_UPDATE_DATE = current_timestamp,
T.SOURCE_ID ='SAP'
FROM TCURV_ALLTYPES S1 , dim_exchangeratetype T
WHERE CONCAT(IFNULL(convert(VARCHAR(200),S1.tcurv_BWAER),'Not Set'),'~',IFNULL(convert (VARCHAR(200),S1.tcurv_kurst),'Not Set')) = T.KEY_ID;


/*insert new rows*/

INSERT INTO dim_exchangeratetype
(
dim_exchangeratetypeid,
exchange_rate_type,
dw_insert_date,dw_update_date,rowstartdate,rowenddate,rowiscurrent,rowchangereason,key_id,source_id
)
SELECT
IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_exchangeratetype' ),0) + ROW_NUMBER() OVER(order by '') dim_exchangeratetypeid,
ifnull(S1.tcurv_KURST, 'Not Set') exchange_rate_type,
current_timestamp DW_INSERT_DATE,
current_timestamp DW_UPDATE_DATE,
current_timestamp AS rowstartdate,
timestamp '9999-12-31 00:00:00.000000' AS rowenddate,
1 AS rowiscurrent,
'Insert' AS rowchangereason,
CONCAT(IFNULL(convert(VARCHAR(200),S1.tcurv_BWAER),'Not Set'),'~',IFNULL(convert (VARCHAR(200),S1.tcurv_kurst),'Not Set')) KEY_ID,
'SAP' SOURCE_ID
FROM TCURV_ALLTYPES S1
		LEFT OUTER JOIN dim_exchangeratetype D ON
D.KEY_ID = CONCAT(IFNULL(convert(VARCHAR(200),S1.tcurv_BWAER),'Not Set'),'~',IFNULL(convert (VARCHAR(200),S1.tcurv_kurst),'Not Set'))
WHERE D.KEY_ID IS NULL;

