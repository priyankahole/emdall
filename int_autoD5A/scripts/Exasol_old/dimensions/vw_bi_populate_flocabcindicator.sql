INSERT INTO dim_flocabcindicator(dim_flocabcindicatorid, RowIsCurrent,flocabcindicator,flocabcindicatordesc,
rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_flocabcindicator
               WHERE dim_flocabcindicatorid = 1);

delete from number_fountain m where m.table_name = 'dim_flocabcindicator';
   
insert into number_fountain
select 	'dim_flocabcindicator',
	ifnull(max(d.dim_flocabcindicatorid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_flocabcindicator d
where d.dim_flocabcindicatorid <> 1; 

INSERT INTO dim_flocabcindicator(dim_flocabcindicatorid,
                                     flocabcindicator,
									 flocabcindicatordesc,
                                     RowStartDate,
                                     RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_flocabcindicator') 
          + row_number() over(order by ''),
			 T370C_T_ABCKZ,
          ifnull(T370C_T_ABCTX,'Not Set'),
          current_timestamp,
          1
     FROM T370C_T t1
    WHERE  NOT EXISTS
                (SELECT 1
                   FROM dim_flocabcindicator s
                  WHERE     s.flocabcindicator = t1.T370C_T_ABCKZ
                        AND s.RowIsCurrent = 1)
;


UPDATE       dim_flocabcindicator s
   SET s.flocabcindicatordesc = ifnull(T370C_T_ABCTX,'Not Set'),
			s.dw_update_date = current_timestamp
FROM T370C_T t1, dim_flocabcindicator s
 WHERE s.RowIsCurrent = 1
 AND  s.flocabcindicator = t1.T370C_T_ABCKZ
 AND s.flocabcindicatordesc <> ifnull(T370C_T_ABCTX,'Not Set');