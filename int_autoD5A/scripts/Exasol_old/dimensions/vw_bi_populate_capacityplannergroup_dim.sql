/* Added by Octavian - linked to Capacity SA as the time of the development - for Every Angle project */

insert into dim_capacityplannergroup (dim_capacityplannergroupid,
                          capplannergroup,
                          text
						  )
select 1, 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_capacityplannergroup where dim_capacityplannergroupid = 1); 


delete from number_fountain m where m.table_name = 'dim_capacityplannergroup';
insert into number_fountain
select 'dim_capacityplannergroup',
 ifnull(max(d.dim_capacityplannergroupid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_capacityplannergroup d
where d.dim_capacityplannergroupid <> 1;

insert into dim_capacityplannergroup (dim_capacityplannergroupid,
                          capplannergroup,
                          text)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_capacityplannergroup') + row_number() over(order by '') AS dim_capacityplannergroupid,
       ifnull(t.TC27T_PLANR,'Not Set') AS capplannergroup,
       ifnull(t.TC27T_TXT,'Not Set') AS text
from TC27T t
 where not exists (select 'x' from dim_capacityplannergroup c where 
	      c.capplannergroup = ifnull(t.TC27T_PLANR,'Not Set'));
	  

update dim_capacityplannergroup c
set c.text = ifnull(t.TC27T_TXT, 'Not Set'),
    c.dw_update_date = current_timestamp
from TC27T t,dim_capacityplannergroup c
where c.capplannergroup = ifnull(t.TC27T_PLANR,'Not Set')
and c.text <> ifnull(t.TC27T_TXT, 'Not Set');
