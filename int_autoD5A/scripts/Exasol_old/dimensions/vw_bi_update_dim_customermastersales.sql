UPDATE dim_customermastersales 
   SET ABCClassification = ifnull(KNVV_KLABC, 'Not Set'),
   dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;
       
UPDATE dim_customermastersales 
   SET AccountAssignmentGroup = ifnull(KNVV_KTGRD, 'Not Set'),
			dw_update_date = current_timestamp
   FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

UPDATE dim_customermastersales 
   SET AccountAssignmentGroupDesciption = ifnull(TVKTT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVKTT, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND tvktt.TVKTT_KTGRD = KNVV_KTGRD;
       
UPDATE dim_customermastersales 
   SET SalesOffice = ifnull(knvv_VKBUR, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

UPDATE dim_customermastersales 
   SET SalesOfficeDescription = ifnull(TVKBT_BEZEI, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVKBT, dim_customermastersales 			
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_VKBUR = TVKBT_VKBUR;

UPDATE dim_customermastersales 
   SET SalesDistrict = ifnull(KNVV_BZIRK, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;    

UPDATE dim_customermastersales 
   SET SalesDistrictDescription = ifnull(T171T_BZTXT, 'Not Set'),
			dw_update_date = current_timestamp
   FROM KNVV, T171T,  dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_BZIRK = T171T_BZIRK;    

UPDATE dim_customermastersales 
   SET CustomerGroup = ifnull(KNVV_KDGRP, 'Not Set'),
			dw_update_date = current_timestamp
   FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode; 

UPDATE dim_customermastersales
   SET CustomerGroupDescription = ifnull(T151T_KTEXT, 'Not Set'),
			dw_update_date = current_timestamp
 FROM KNVV, T151T, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND knvv.knvv_KDGRP = T151T_KDGRP;       
       
UPDATE dim_customermastersales 
   SET PaymentTerms = ifnull(KNVV_ZTERM, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;       
       
UPDATE dim_customermastersales 
   SET CustomerName = ifnull(k.KNA1_NAME1, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, KNA1 k, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNA1_KUNNR = knvv.knvv_KUNNR;

UPDATE dim_customermastersales 
   SET DistributionChannelDescription = ifnull(TVTWT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
   FROM KNVV, TVTWT, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVTWT_VTWEG = knvv.KNVV_VTWEG;

UPDATE dim_customermastersales 
   SET Division = ifnull(TSPAT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
   FROM KNVV, TSPAT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TSPAT_SPART = knvv.KNVV_SPART;

UPDATE dim_customermastersales
   SET SalesOrgDescription = ifnull(TVKOT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
    FROM KNVV, TVKOT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVKOT_VKORG = knvv.KNVV_VKORG;


UPDATE dim_customermastersales 
   SET Customergroup3 = ifnull(KNVV_KVGR3, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND ifnull(Customergroup3,'X') <> ifnull(KNVV_KVGR3, 'Not Set');
       

UPDATE dim_customermastersales 
   SET CustomerGroup3Description = ifnull(TVV3T_BEZEI, 'Not Set')
FROM KNVV, TVV3T, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_KVGR3 = TVV3T_KVGR3
       AND ifnull(CustomerGroup3Description,'X') <> ifnull(TVV3T_BEZEI, 'Not Set');       
       
UPDATE dim_customermastersales 
   SET SalesGroup =  ifnull(KNVV_VKGRP,'Not Set')
FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode;

/*Update added by Victor on 12.17.14*/	   
UPDATE dim_customermastersales 
   SET commissiongroup = ifnull(KNVV_KONDA, 'Not Set')
 FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND commissiongroup <> ifnull(KNVV_KONDA, 'Not Set');
       
 /* Start New fields added as a part of Columbia Report 856 */ 
UPDATE dim_customermastersales 
   SET pricingProcedure = ifnull(KNVV_KALKS, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND pricingProcedure <> ifnull(KNVV_KALKS, 'Not Set'); 
	   
UPDATE dim_customermastersales 
   SET deliveryPriority = ifnull(KNVV_LPRIO,1.0000),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND deliveryPriority <> ifnull(KNVV_LPRIO,1.0000);   

UPDATE dim_customermastersales 
   SET incoTerm2 = ifnull(KNVV_INCO2, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV,dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND incoTerm2 <> ifnull(KNVV_INCO2, 'Not Set');   
	   
UPDATE dim_customermastersales 
   SET maxPartialDelivery = ifnull(KNVV_ANTLF,1.0000),
			dw_update_date = current_timestamp
   FROM KNVV, dim_customermastersales 
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode	
       AND maxPartialDelivery <> ifnull(KNVV_ANTLF,1.0000);
	   
UPDATE dim_customermastersales
   SET storeLocator = ifnull(KNVV_AGREL, 'Not Set'),
			dw_update_date = current_timestamp
 FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND storeLocator <> ifnull(KNVV_AGREL, 'Not Set');

UPDATE dim_customermastersales
   SET customergroup5 = ifnull(KNVV_KVGR5, 'Not Set'),
			dw_update_date = current_timestamp
   FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup5 <> ifnull(KNVV_KVGR5, 'Not Set');	
	   
UPDATE dim_customermastersales 
   SET customergroup2 = ifnull(KNVV_KVGR2, 'Not Set'),
			dw_update_date = current_timestamp
   FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup2 <> ifnull(KNVV_KVGR2, 'Not Set');	
	   
UPDATE dim_customermastersales 
   SET customergroup1 = ifnull(KNVV_KVGR1, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup1 <> ifnull(KNVV_KVGR1, 'Not Set');	   