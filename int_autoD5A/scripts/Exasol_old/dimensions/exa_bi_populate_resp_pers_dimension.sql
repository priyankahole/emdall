/*DEFAULT ROW*/


INSERT INTO dim_responsibleperson(
	dim_responsiblepersonid,
	personnel_number,
	first_name,
	second_name,
	object_number,
	counter_ihpa,
	dw_insert_date,
	dw_update_date
)
  SELECT    
	1,
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	0,
	current_timestamp,
	current_timestamp

FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_responsibleperson WHERE dim_responsiblepersonid = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_responsibleperson';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_responsibleperson',
                ifnull(max(d.dim_responsiblepersonid),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_responsibleperson d
where d.dim_responsiblepersonid <> 1;

DROP TABLE IF EXISTS IHPA_V2;
create table IHPA_V2 AS 
SELECT DISTINCT IHPA_OBJNR, IHPA_PARVW,IHPA_COUNTER,IHPA_OBTYP,IHPA_INHER,IHPA_ERDAT,IHPA_ERZEIT,IHPA_ERNAM,IHPA_AEDAT,IHPA_AEZEIT,IHPA_AENAM,IHPA_KZLOESCH,IHPA_ADRNR,IHPA_TZONSP,
case when IHPA_PARVW = 'LF' then substr(ihpa_parnr, 4)  
else ihpa_parnr
end ihpa_parnr
FROM IHPA;

drop table if exists tmp_dim_resp_pers;
create table tmp_dim_resp_pers as
SELECT DISTINCT *
FROM IHPA_V2 a, PA0002 p
where ihpa_parnr  = PA0002_PERNR; 


/*insert new rows*/

INSERT INTO dim_responsibleperson(
dim_responsiblepersonid,
object_number,
counter_ihpa,
personnel_number,
first_name,
second_name,
dw_insert_date
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_responsibleperson') + row_number() over(order by '') as dim_responsiblepersonid,
ifnull(IHPA_OBJNR, 'Not Set') as object_number,
ifnull(IHPA_COUNTER, 0) as counter_ihpa,
ifnull(ihpa_parnr, 'Not Set') as personnel_number,
ifnull(PA0002_VORNA, 'Not Set') as first_name,
ifnull(PA0002_NACHN, 'Not Set') as second_name,
current_timestamp
FROM tmp_dim_resp_pers t
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_responsibleperson d
        where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
		AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa);

/*Octavian S - 14-MAY-2018 - APP-9599 - Weekend processsing errors - Commented Update statements and used Merge statements*/
/*update existing rows*/

/*update dim_responsibleperson d
set personnel_number = ifnull(ihpa_parnr, 'Not Set')
FROM dim_responsibleperson d,  tmp_dim_resp_pers t
where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa
and  personnel_number <> ifnull(ihpa_parnr, 'Not Set')

update dim_responsibleperson d
set first_name = ifnull(PA0002_VORNA, 'Not Set')
FROM dim_responsibleperson d,  tmp_dim_resp_pers t
where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa
and  first_name <> ifnull(PA0002_VORNA, 'Not Set')

update dim_responsibleperson d
set second_name = ifnull(PA0002_NACHN, 'Not Set')
FROM dim_responsibleperson d,  tmp_dim_resp_pers t
where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa
and  second_name <> ifnull(PA0002_NACHN, 'Not Set')*/

merge into dim_responsibleperson drp
using
(
select distinct dim_responsiblepersonid, MAX(ifnull(ihpa_parnr, 'Not Set')) as IHPA_PARNR
FROM dim_responsibleperson d,  tmp_dim_resp_pers t
where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa
and  personnel_number <> ifnull(ihpa_parnr, 'Not Set')
group by dim_responsiblepersonid
)upd
on drp.dim_responsiblepersonid = upd.dim_responsiblepersonid
when matched then update set personnel_number = ifnull(upd.IHPA_PARNR, 'Not Set');

merge into dim_responsibleperson drp
using
(
select distinct dim_responsiblepersonid, MAX(ifnull(PA0002_VORNA, 'Not Set')) as PA0002_VORNA
FROM dim_responsibleperson d,  tmp_dim_resp_pers t
where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa
and  first_name <> ifnull(PA0002_VORNA, 'Not Set')
group by dim_responsiblepersonid
)upd
on drp.dim_responsiblepersonid = upd.dim_responsiblepersonid
when matched then update set first_name = ifnull(upd.PA0002_VORNA, 'Not Set');

/*
merge into dim_responsibleperson drp
using
(
select distinct dim_responsiblepersonid,max(ifnull(PA0002_NACHN, 'Not Set')) as PA0002_NACHN
FROM dim_responsibleperson d,  tmp_dim_resp_pers t
where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa
and  second_name <> ifnull(PA0002_NACHN, 'Not Set')
)upd
on drp.dim_responsiblepersonid = upd.dim_responsiblepersonid
when matched then update set second_name = ifnull(upd.PA0002_NACHN, 'Not Set') */


merge into dim_responsibleperson drp
using
(
select distinct dim_responsiblepersonid,max(ifnull(PA0002_NACHN, 'Not Set')) as PA0002_NACHN
FROM dim_responsibleperson d, tmp_dim_resp_pers t
where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa
and second_name <> ifnull(PA0002_NACHN, 'Not Set')
group by dim_responsiblepersonid
)upd
on drp.dim_responsiblepersonid = upd.dim_responsiblepersonid
when matched then update set second_name = ifnull(upd.PA0002_NACHN, 'Not Set');


merge into dim_responsibleperson drp
using
(
select distinct dim_responsiblepersonid,max(ifnull(PA0002_VORSW, '')) as PA0002_VORSW
FROM dim_responsibleperson d, tmp_dim_resp_pers t
where ifnull(t.IHPA_OBJNR, 'Not Set') = d.object_number
AND ifnull(t.IHPA_COUNTER, 0) = d.counter_ihpa
group by dim_responsiblepersonid
)upd
on drp.dim_responsiblepersonid = upd.dim_responsiblepersonid
when matched then update set middle_name = ifnull(upd.PA0002_VORSW, '');


drop table if exists tmp_dim_resp_pers;
DROP TABLE IF EXISTS IHPA_V2;

/*concatenation of first and second name*/
update dim_responsibleperson
set first_second_name = 'Not Set';

merge into dim_responsibleperson d
using(select distinct dim_responsiblepersonid,
case when middle_name is null then CONCAT(first_name, ' ', second_name)
else CONCAT(first_name, ' ', middle_name, ' ', second_name)
end as first_sec_name
from dim_responsibleperson
)t
on d.dim_responsiblepersonid = t.dim_responsiblepersonid
when matched then update set
d.first_second_name = t.first_sec_name;

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_responsibleperson'; 

/*
Alin 13 mar 2018 - this was requested on APP-7223 by Michal on Feb 26th
statements below are needed to insert the missing object numbers in responsible person dimension
PM order analysis SA broughts the data from dim_Responsibleperson dimension by using the join condition with
dim_ordermaster on object_number
*/
DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_responsibleperson';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_responsibleperson',
                ifnull(max(d.dim_responsiblepersonid),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_responsibleperson d
where d.dim_responsiblepersonid <> 1;


INSERT INTO dim_responsibleperson(
dim_responsiblepersonid,
object_number,
counter_ihpa,
personnel_number,
first_name,
second_name,
flag_source,
dw_insert_date
)

SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_responsibleperson') + row_number() over(order by '') as dim_responsiblepersonid,
om.objectnumber as object_number,
0 as counter_ihpa,
'Not Set' as personnel_number,
'Not Set' as first_name,
'Not Set' as second_name,
'from Order Master dim' as flag_source,
current_timestamp
from dim_ordermaster om
where not exists (select 1
from dim_responsibleperson rp
where rp.object_number = om.objectnumber);

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_responsibleperson'; 


 
