
INSERT INTO dim_codegroup(dim_codegroupId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_codegroup
               WHERE dim_codegroupId = 1);

UPDATE	dim_codegroup d

SET 	d.SHORTDESCRIPTION = s.QPGT_KURZTEXT,
		d.dw_update_date = current_timestamp
FROM 	QPGT s,dim_codegroup d
WHERE 	d."catalog" = s.QPGT_KATALOGART
		and d.CODEGROUP = s.QPGT_CODEGRUPPE
		and d.RowIsCurrent = 1;			   
			   
delete from number_fountain m where m.table_name = 'dim_codegroup';

insert into number_fountain
select 	'dim_codegroup',
	ifnull(max(d.dim_codegroupId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_codegroup d
where d.dim_codegroupId <> 1;

INSERT INTO dim_codegroup(Dim_codegroupid,
                                          "catalog",
                                          codegroup,               
					  shortdescription,
                                          RowStartDate,
                                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_codegroup') 
          + row_number() over(order by '') ,
	qpgt_KATALOGART,
          qpgt_codegruppe,
	  qpgt_kurztext,
          current_timestamp,
          1
     FROM qpgt 
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_codegroup 
               WHERE "catalog" = qpgt_KATALOGART
                	AND CodeGroup = qpgt_codegruppe
                         AND shortdescription = qpgt_kurztext
                     AND RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_codegroup';
					 