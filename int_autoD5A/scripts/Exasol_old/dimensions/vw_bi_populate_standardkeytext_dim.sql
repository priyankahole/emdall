insert into dim_standardkeytext (dim_standardkeytextid,
                          standardtextkey,
                          description
						  )
select 1, 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_standardkeytext where dim_standardkeytextid = 1); 


delete from number_fountain m where m.table_name = 'dim_standardkeytext';
insert into number_fountain
select 'dim_standardkeytext',
 ifnull(max(d.dim_standardkeytextid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_standardkeytext d
where d.dim_standardkeytextid <> 1;

insert into dim_standardkeytext (dim_standardkeytextid,
                          standardtextkey,
                          description)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_standardkeytext') + row_number() over(order by '') AS dim_standardkeytextid,
       ifnull(t.T435T_VLSCH,'Not Set') AS standardtextkey,
       ifnull(t.T435T_TXT,'Not Set') AS description
from T435T t
 where not exists (select 'x' from dim_standardkeytext c where 
	      c.standardtextkey = ifnull(t.T435T_VLSCH,'Not Set')
	  )
	  AND T435T_SPRAS = 'E';
	  

update dim_standardkeytext c
set c.description = ifnull(t.T435T_TXT, 'Not Set'),
    c.dw_update_date = current_timestamp
from T435T t,dim_standardkeytext c
where c.standardtextkey = ifnull(t.T435T_VLSCH,'Not Set')
AND T435T_SPRAS = 'E'
and c.description <> ifnull(t.T435T_TXT, 'Not Set');