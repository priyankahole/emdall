insert into dim_AllocationReleaseRule (dim_AllocationReleaseRuleid,ReleaseRule,Description,rowstartdate,rowenddate,rowiscurrent,rowchangereason)
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from dim_AllocationReleaseRule where dim_AllocationReleaseRuleid = 1);

UPDATE   dim_allocationreleaserule d1
SET d1.Description = ifnull(t.J_3ARESG_T_BEZEI,'Not Set'),
	d1.dw_update_date = current_timestamp
 FROM j_3aresg_t t, dim_allocationreleaserule d1
WHERE d1.RowIsCurrent = 1 AND d1.ReleaseRule  = t.J_3ARESG_T_J_3ARESGY;

delete from number_fountain m where m.table_name = 'dim_AllocationReleaseRule';

insert into number_fountain
select 	'dim_AllocationReleaseRule',
	ifnull(max(d.dim_AllocationReleaseRuleid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_AllocationReleaseRule d
where d.dim_AllocationReleaseRuleid <> 1;
               
INSERT INTO dim_allocationreleaserule(dim_AllocationReleaseRuleid,
                                      ReleaseRule,
                                      Description,
                                      rowstartdate,
                                      rowiscurrent)
   SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_AllocationReleaseRule') 
			+ row_number() over(order by ''))
             dim_allocationreleaseruleid,
          t.J_3ARESG_T_J_3ARESGY,
          t.J_3ARESG_T_BEZEI,
          current_date,
          1
     FROM j_3aresg_t t
    WHERE     t.J_3ARESG_T_SPRAS = 'E'
          AND NOT EXISTS
                 (SELECT 1
                    FROM dim_allocationreleaserule d1
                   WHERE d1.ReleaseRule = t.J_3ARESG_T_J_3ARESGY);

delete from number_fountain m where m.table_name = 'dim_AllocationReleaseRule';				   