/***********************************************************************************************************/
/*  Script : vw_populate_hierarchy.sql                                 						   */
/*  Description :Populate SAP Hierarchy			  		              						   */
/*  Created On : January 6, 2016                                               						   */
/*  Create By :  Victor                                                   						   */
/*                                                                                						   */
/*  Change History                                                                						   */
/*  Date               CVSversion  By          Description                        						   */
/*  9 February 2015  1.0         Victor    Added the script */
/***********************************************************************************************************/

/*

Someone deleted this scrip from base prod "/home/fusionops/ispring_clustered_storage/configurations/FusionOps-S/scripts/dimensions" 

Please put it back.

*/

DROP TABLE IF EXISTS tmp_dummy_non_existing_table;