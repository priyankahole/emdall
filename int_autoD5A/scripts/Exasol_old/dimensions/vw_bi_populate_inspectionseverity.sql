UPDATE    dim_inspectionseverity isv
      SET isv.InspectionSeverityName = q.QDEPT_KURZTEXT,
			isv.dw_update_date = current_timestamp
	  FROM
          dim_inspectionseverity isv,QDEPT q
 WHERE isv.RowIsCurrent = 1
 AND isv.InspectionSeverityCode = q.QDEPT_PRSCHAERFE;
 
INSERT INTO dim_inspectionseverity(dim_inspectionseverityid, RowIsCurrent,inspectionseveritycode,inspectionseverityname,rowstartdate)
SELECT 1, 1,1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionseverity
               WHERE dim_inspectionseverityid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectionseverity';
   
insert into number_fountain
select 	'dim_inspectionseverity',
	ifnull(max(d.dim_InspectionSeverityid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectionseverity d
where d.dim_InspectionSeverityid <> 1; 
			   
INSERT INTO dim_inspectionseverity(dim_InspectionSeverityid,
                                   InspectionSeverityCode,
                                   InspectionSeverityName,
                                   RowStartDate,
                                   RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectionseverity')
          + row_number() over(order by ''),
                         q.QDEPT_PRSCHAERFE,
          q.QDEPT_KURZTEXT,
          current_timestamp,
          1
     FROM QDEPT q
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionseverity isv
               WHERE isv.InspectionSeverityCode = q.QDEPT_PRSCHAERFE
                     AND isv.RowIsCurrent = 1);
