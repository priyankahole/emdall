UPDATE    dim_route r

   SET r.RouteName = t.TVROT_BEZEI,
       r.TransitTime = t.TVRO_TRAZT,
       r.TransitTimeInCalendarDays = t.TVRO_TRAZTD/240000,
       r.TransportLeadTimeInCalendarDays = t.TVRO_TDVZTD,
	   r.factorycalendar = ifnull(t.tvro_spfbk,'Not Set'),
	   r.modeoftransport = ifnull(t.tvro_expvz,'Not Set'),
	   r.shippingtype = ifnull(t.tvro_vsart,'Not Set')
          FROM
          TVROT t,  dim_route r
 WHERE r.RowIsCurrent = 1
 AND r.RouteCode = t.TVROT_ROUTE
;

 update dim_route r

 set r.shippingtypedesc = t.shippingtypedesc
 from dim_tranportbyshippingtype t, dim_route r
where t.ShippingType = r.ShippingType 
 and r.shippingtypedesc <> t.shippingtypedesc;
					
