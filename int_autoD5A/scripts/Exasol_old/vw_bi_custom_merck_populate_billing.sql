/* Madalina 17 Oct 2016 - Copy of vw_bi_populate_shipmentattributesforbilling.sql, called from Billing Post Process */

DROP TABLE IF EXISTS tmp_sod_billing_intermediate;
CREATE TABLE tmp_sod_billing_intermediate
AS
SELECT f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo,sum(f_sod.amt_Cost) amt_sd_Cost,
SUM(f_sod.amt_UnitPrice * f_sod.ct_QtyDelivered) amt_sd_Shipped,
SUM(f_sod.ct_QtyDelivered) ct_sd_QtyDelivered
FROM fact_salesorderdelivery f_sod
GROUP BY f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo;



UPDATE fact_billing fb 
    SET fb.amt_sd_Cost = sod.amt_sd_Cost
	,fb.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp, fact_billing fb 
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
	AND ifnull(fb.amt_sd_Cost,-1) <> ifnull(sod.amt_sd_Cost,0);


UPDATE fact_billing fb
    SET fb.amt_sd_Shipped = sod.amt_sd_Shipped
	,fb.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp, fact_billing fb
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
        AND ifnull(fb.amt_sd_Shipped,-1) <> ifnull(sod.amt_sd_Shipped,0);

UPDATE fact_billing fb
    SET fb.ct_sd_QtyDelivered = sod.ct_sd_QtyDelivered
	,fb.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/	
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp,fact_billing fb
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
        AND ifnull(fb.ct_sd_QtyDelivered,-1) <> ifnull(sod.ct_sd_QtyDelivered,0);


/*Begin Andrian 20 Jan 2016 based on BI-1589*/
/*Georgiana 13 Jan 2016 Unable to get a stable source of rows*/
drop table if exists tmp_fact_salesorderdelivery001;
create table tmp_fact_salesorderdelivery001 as
select distinct dd_SalesDlvrDocNo, max(Dim_DateidActualGoodsIssue) as Dim_DateidActualGoodsIssue
from fact_salesorderdelivery
group by dd_SalesDlvrDocNo;


UPDATE fact_billing fb
SET fb.Dim_sd_DateidActualGoodsIssue = sod.Dim_DateidActualGoodsIssue
FROM tmp_fact_salesorderdelivery001 sod, fact_billing fb
WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
AND ifnull(fb.Dim_sd_DateidActualGoodsIssue,-1) <> ifnull(sod.Dim_DateidActualGoodsIssue,1);

drop table if exists tmp_fact_salesorderdelivery001;
/*End Andrian 20 Jan 2016 based on BI-1589*/

DROP TABLE IF EXISTS tmp_sod_billing_intermediate;

/* Madalina 27 Sep 2016 - updates moved later in the script, in order to catch all the updates of the dependent columns */
update fact_billing
set dd_BusinessDaysSeqNo_cd = d.BusinessDaysSeqNo,dw_update_date = current_timestamp
from fact_billing AS f_bill INNER JOIN Dim_Date AS cd ON f_bill.Dim_DateidCreated = cd.Dim_Dateid 
INNER JOIN Dim_Date AS sdagi ON f_bill.Dim_sd_DateidActualGoodsIssue = sdagi.Dim_Dateid
inner join  dim_date d  on d.datevalue =   (sdagi.DateValue) and d.companycode = cd.companycode and d.plantcode_factory = cd.plantcode_factory
where dd_BusinessDaysSeqNo_cd <> d.BusinessDaysSeqNo;

update fact_billing
set dd_BusinessDaysSeqNo_bd = d.BusinessDaysSeqNo,dw_update_date = current_timestamp
FROM fact_billing AS f_bill INNER JOIN Dim_Date AS bd ON f_bill.Dim_DateidBilling = bd.Dim_Dateid  
INNER JOIN Dim_Date AS sdagi ON f_bill.Dim_sd_DateidActualGoodsIssue = sdagi.Dim_Dateid
inner join dim_date d on d.datevalue =   (sdagi.DateValue) and d.companycode = bd.companycode and d.plantcode_factory = bd.plantcode_factory
where dd_BusinessDaysSeqNo_bd <> d.BusinessDaysSeqNo;

/* Madalina 6 Dec 2016 - Add Days B/W Creation Date and AGI (Date) (filter use) - BI-4792 */
update fact_billing f_bill
set dd_daysBWCreationDateAndAGI = 
 	ifnull(( case when dim_dateidcreated=1 or dim_sd_dateidactualgoodsissue=1 then 0.0000 
	else (cd.businessdaysseqno) - ifnull((f_bill.dd_BusinessDaysSeqNo_cd),0) end ),0)
from fact_billing f_bill, dim_date sdagi, dim_date cd
where cd.dim_dateid = f_bill.dim_dateidcreated
	and sdagi.dim_dateid = f_bill.dim_sd_dateidactualgoodsissue;
