/*****************************************************************************************************
Last Update Date : Aug 20, 12013
Last Updated By : Hiten Suthar
Last Change Description : Late-Early days calculations changed to consider business days only
*****************************************************************************************************/


INSERT INTO processinglog
(processinglogid,
referencename,
startdate,
description)
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),
'bi_purchase_vendorperformance_processing',
current_timestamp,
'bi_purchase_vendorperformance_processing START');

Drop table if exists fact_purchase_tmp_709;
Drop table if exists doc_tmp_709;
Drop table if exists mainholder_709;
Drop table if exists holder_709;
Drop table if exists fact_purch_tmp_709_fl_01;

drop table if exists fact_purch_invqty_001;
create table fact_purch_invqty_001 as
select sum(e.EKBE_MENGE) InvQty, e.EKBE_EBELN, e.EKBE_EBELP
from EKBE_PURCH e
where e.EKBE_VGABE = '2'
group by e.EKBE_EBELN, e.EKBE_EBELP;

drop table if exists fact_purch_lineqty_001;
create table fact_purch_lineqty_001 as
select sum(f1.ct_ScheduledQty_Merck) LineQty, f1.dd_DocumentNo f1_DocumentNo, f1.dd_DocumentItemNo f1_DocumentItemNo
from (select distinct(f1.*) from fact_purchase f1) f1 inner join dim_purchasemisc m on m.Dim_PurchaseMiscid = f1.Dim_PurchaseMiscid
where ((m.ItemGRIndicator = 'X'
            AND exists (select 1 from fact_purchase b
                        where f1.dd_DocumentNo = b.dd_DocumentNo and f1.dd_DocumentItemNo = b.dd_DocumentItemNo
                              and ((b.ct_ScheduledQty_Merck - b.ct_ReceivedQty) > 0 OR b.Dim_DateidLastGR = 1)))
          OR f1.dd_InvStatus <> 'Full')
group by f1.dd_DocumentNo, f1.dd_DocumentItemNo;

/*** Getting only work/business days - start ***/

drop table if exists tmp_dim_date_001;
create table tmp_dim_date_001 as
select Dim_Dateid, DateValue, CompanyCode, plantcode_factory,
	dense_rank() over(partition by companyCode order by DateValue) DateSeqNo,
	convert (BIGINT,0) MaxDateSeqNo,  convert (BIGINT,0) MinDateSeqNo
from dim_date
where isapublicholiday = 0 and isaweekendday = 0;

drop table if exists tmp_dim_date_002;
create table tmp_dim_date_002 as
select a.*,b.DateSeqNo as DateSeqNo
 from dim_date a left join tmp_dim_date_001 b
 on a.datevalue = b.datevalue
 and a.plantcode_factory = b.plantcode_factory
 and a.CompanyCode = b.CompanyCode;

drop table if exists tmp_dimdt_holiday_002;
create table tmp_dimdt_holiday_002 as
select b.* from
(select a.isapublicholiday,a.isaweekendday,a.Dim_Dateid, a.DateValue, a.CompanyCode, a.DateSeqNo,a.plantcode_factory,MAX(a.DateSeqNo)
over (PARTITION BY a.CompanyCode,a.plantcode_factory
             ORDER BY a.DateValue ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as MaxDateSeqNo
from tmp_dim_date_002  a) b
where isapublicholiday = 1 or isaweekendday = 1;

drop table if exists tmp_dimdt_holiday_001_prev;
create table tmp_dimdt_holiday_001_prev as
select b.* from
(select a.Dim_Dateid,
        a.isapublicholiday,
        a.isaweekendday,
        a.DateValue,
        a.CompanyCode,
        a.DateSeqNo,
        a.plantcode_factory,
        MIN(DateSeqNo) over (PARTITION BY a.CompanyCode,a.plantcode_factory
        ORDER BY a.DateValue DESC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as MinDateSeqNo
from tmp_dim_date_002  a) b
where isapublicholiday = 1 or isaweekendday = 1;



drop table if exists tmp_dimdt_holiday_001;
create table tmp_dimdt_holiday_001 as
select a.Dim_Dateid,
       a.DateValue,
       a.CompanyCode,
       ifnull(a.DateSeqNo,0) as DateSeqNo,
       a.MaxDateSeqNo,
       b.MinDateSeqNo
        from tmp_dimdt_holiday_002 a,
             tmp_dimdt_holiday_001_prev b
  where  a.Dim_Dateid = b.Dim_Dateid;

  insert into tmp_dim_date_001 (Dim_Dateid, DateValue, CompanyCode, DateSeqNo, MaxDateSeqNo, MinDateSeqNo)
select Dim_Dateid, DateValue, CompanyCode, DateSeqNo, MaxDateSeqNo, MinDateSeqNo
from tmp_dimdt_holiday_001;

/*** Getting only work/business days - end ***/

Create table fact_purch_tmp_709_fl_01  as
SELECT   row_number() over(order by dd_DocumentNo, dd_DocumentItemNo, sd.DateValue, dd_ScheduleNo) iid,
	 fact_purchaseid v_fact_vendperfid,
         dd_DocumentNo v_dd_DocumentNo,
         dd_DocumentItemNo v_dd_DocumentItemNo,
         dd_ScheduleNo v_dd_ScheduleNo,
         case when m.ItemGRIndicator = 'X' and (m.ItemDeliveryComplete = 'X' or m.DeliveryComplete = 'X') then ct_ReceivedQty else ct_ScheduledQty_Merck end v_ct_DeliveryQty,
         ct_ReceivedQty v_ct_ReceivedQty,
         dd.DateValue v_DeliveryDate,
	  a.Dim_DateidDelivery,
     convert (BIGINT,0) DlvrDateSeqNo,
         sd.DateValue v_StatDeliveryDate,
	 a.Dim_DateidStatDelivery,
	 convert (BIGINT,0) StatDateSeqNo,
          case when m.ItemDeliveryComplete = 'X' or m.DeliveryComplete = 'X' then 'X' else 'A' end v_DlvrComplete,
          m.ItemGRIndicator v_GRIndicator,
          pl.CompanyCode v_CompanyCode,
    convert (NUMERIC(18,4),0.0000) vQtyEarly ,
	convert (NUMERIC(18,4),0.0000) vQtyOnTime ,
	convert (NUMERIC(18,4),0.0000) vQtyLate ,
	convert (BIGINT,0) vLateDlvrDays,
	convert (BIGINT,0) vEarlyDlvrDays ,
	convert (NUMERIC(18,4),0.0000) vQtyEarly_ddt ,
	convert (NUMERIC(18,4),0.0000) vQtyOnTime_ddt ,
	convert (NUMERIC(18,4),0.0000) vQtyLate_ddt ,
	convert (BIGINT,0) vLateDlvrDays_ddt,
	convert (BIGINT,0) vEarlyDlvrDays_ddt,
	convert (NUMERIC(18,4),0.0000) vQtyReturn,
	to_date(current_date)  v_LastGRDate,
	to_date(current_date) v_FirstGRDate,
	convert (BIGINT,0) v_tmp_RcvdDlvCount,
	ifnull(e.InvQty,0) vInvoiceQty ,
	ifnull(f1.LineQty,0) vPOLineQty,
	ifnull((SELECT property_value
		    FROM systemproperty
		    WHERE property = 'purchasing.deliverytolerance.late'), 1) pLateToleranceDays,
	ifnull((SELECT property_value
		     FROM systemproperty
		     WHERE property = 'purchasing.deliverytolerance.early'), 1) pEarlyToleranceDays,
	180 vPastDueCutOffDays,
	convert (BIGINT,0) pflag,
	row_number() over(partition by dd_DocumentNo, dd_DocumentItemNo order by sd.DateValue, dd_ScheduleNo) LineDlvrSeq,
	pl.CompanyCode,
	pl.plantcode,
	dd.dim_dateid as dd_dim_dateid, -- used for DlvrDateSeqNo
	sd.dim_dateid as sd_dim_dateid	-- used for StatDateSeqNo
    FROM (select distinct a.ct_ScheduledQty_Merck,a.Dim_DateidDelivery,a.Dim_DateidStatDelivery,a.Dim_PlantidOrdering,a.Dim_PurchaseMiscid,a.dd_DocumentNo,a.dd_DocumentItemNo,a.dd_ScheduleNo, a.fact_purchaseid,ct_ReceivedQty from fact_purchase a) a
	INNER JOIN dim_purchasemisc m on m.Dim_PurchaseMiscid = a.Dim_PurchaseMiscid
	INNER JOIN Dim_Plant pl on ifnull(pl.Dim_Plantid, 1) = ifnull(a.Dim_PlantidOrdering, 1)
	INNER JOIN Dim_Date sd on sd.Dim_Dateid = ifnull(a.Dim_DateidStatDelivery, 1)
	INNER JOIN Dim_Date dd on dd.Dim_Dateid = ifnull(a.Dim_DateidDelivery, 1)
	INNER JOIN fact_purch_lineqty_001 f1 on f1.f1_DocumentNo = a.dd_DocumentNo and f1.f1_DocumentItemNo = a.dd_DocumentItemNo
	LEFT JOIN fact_purch_invqty_001 e on e.EKBE_EBELN = dd_DocumentNo and e.EKBE_EBELP = dd_DocumentItemNo;


UPDATE fact_purch_tmp_709_fl_01 f
SET f.DlvrDateSeqNo = CASE WHEN dd0.DateSeqNo = 0
                      THEN dd0.MaxDateSeqNo
					  ELSE dd0.DateSeqNo END
FROM fact_purch_tmp_709_fl_01 f
LEFT JOIN tmp_dim_date_001 dd0 ON dd0.Dim_Dateid = f.dd_dim_dateid;


UPDATE fact_purch_tmp_709_fl_01 f
SET f.StatDateSeqNo = CASE WHEN sd0.DateSeqNo = 0
                      THEN sd0.MaxDateSeqNo
					  ELSE sd0.DateSeqNo END
FROM fact_purch_tmp_709_fl_01 f
LEFT JOIN  tmp_dim_date_001 sd0 ON sd0.Dim_Dateid = f.sd_dim_dateid;



DROP TABLE IF EXISTS fact_materialmovement_vendperform;
create table fact_materialmovement_vendperform as
  select a.dd_DocumentNo Document_No,
         a.dd_DocumentItemNo DocumentLine_No,
         a.dd_DocumentScheduleNo dd_ScheduleNo ,
         mt.MovementType MovementType,
         dt.DateValue PostingDate,
	     convert (BIGINT,0) PostDateSeqNo,
         a.ct_QtyEntryUOM Quantity,
	     a.dim_DateIDPostingDate,
	     dt.CompanyCode,
	     dt.Dim_Dateid as dt_dim_dateid -- used in PostDateSeqNo
  from fact_materialmovement a
        inner join dim_movementtype mt on a.Dim_MovementTypeid = mt.Dim_MovementTypeid
        inner join dim_date dt on ifnull(a.dim_DateIDPostingDate, 1) = ifnull(dt.Dim_Dateid, 1)
  where mt.MovementType in ('122','161','123','162','101','102');


UPDATE fact_materialmovement_vendperform vpf
SET vpf.PostDateSeqNo= CASE WHEN dd0.DateSeqNo = 0
                       THEN dd0.MinDateSeqNo
					   ELSE dd0.DateSeqNo END
FROM fact_materialmovement_vendperform vpf
LEFT JOIN tmp_dim_date_001 dd0 ON dd0.Dim_Dateid = vpf.dt_dim_dateid;


DROP TABLE IF EXISTS fact_purchase_tmp_709_QtyEarly;
CREATE TABLE fact_purchase_tmp_709_QtyEarly AS
SELECT Sum(ifnull(a.Quantity,0)) vQtyEarly,
       a.Document_No,
	   a.DocumentLine_No,
	   a.dd_scheduleno
FROM fact_materialmovement_vendperform a
INNER JOIN fact_purch_tmp_709_fl_01 b ON a.Document_No = b.v_dd_DocumentNo
                                      AND a.DocumentLine_No = b.v_dd_DocumentItemNo
									  AND a.dd_scheduleno = b.v_dd_ScheduleNo
WHERE a.PostDateSeqNo < (b.StatDateSeqNo - b.pEarlyToleranceDays)
	 AND b.v_GRIndicator = 'X'
GROUP BY a.Document_No,a.DocumentLine_No,a.dd_scheduleno;


DROP TABLE IF EXISTS fact_purchase_tmp_709_QtyOnTime;
CREATE TABLE fact_purchase_tmp_709_QtyOnTime as
SELECT Sum(ifnull(a.Quantity,0)) vQtyOnTime, a.Document_No,a.DocumentLine_No,a.dd_scheduleno
FROM fact_materialmovement_vendperform a
INNER JOIN fact_purch_tmp_709_fl_01 b ON a.Document_No = b.v_dd_DocumentNo
                                      AND a.DocumentLine_No = b.v_dd_DocumentItemNo
									  AND a.dd_scheduleno = b.v_dd_ScheduleNo
WHERE    a.PostDateSeqNo <= b.StatDateSeqNo + b.pLateToleranceDays
	 AND b.v_GRIndicator = 'X'
GROUP BY a.Document_No,a.DocumentLine_No,a.dd_scheduleno;


DROP TABLE IF EXISTS fact_purchase_tmp_709_QtyEarly_ddt;
CREATE TABLE fact_purchase_tmp_709_QtyEarly_ddt AS
SELECT Sum(ifnull(a.Quantity,0)) vQtyEarly_ddt,
       a.Document_No,
	   a.DocumentLine_No,
	   a.dd_scheduleno
FROM fact_materialmovement_vendperform a
	INNER JOIN fact_purch_tmp_709_fl_01 b ON a.Document_No = b.v_dd_DocumentNo
	AND a.DocumentLine_No = b.v_dd_DocumentItemNo
	AND a.dd_scheduleno = b.v_dd_ScheduleNo
WHERE a.PostDateSeqNo < b.DlvrDateSeqNo - b.pEarlyToleranceDays
	AND b.v_GRIndicator = 'X'
GROUP BY a.Document_No,a.DocumentLine_No,a.dd_scheduleno;


DROP TABLE IF EXISTS fact_purchase_tmp_709_QtyOnTime_ddt;
CREATE TABLE fact_purchase_tmp_709_QtyOnTime_ddt as
SELECT Sum(ifnull(a.Quantity,0)) vQtyOnTime_ddt,
       a.Document_No,a.DocumentLine_No,
	   a.dd_scheduleno
FROM fact_materialmovement_vendperform a
INNER JOIN fact_purch_tmp_709_fl_01 b ON a.Document_No = b.v_dd_DocumentNo
                                      AND a.DocumentLine_No = b.v_dd_DocumentItemNo
									  AND a.dd_scheduleno = b.v_dd_ScheduleNo
WHERE    a.PostDateSeqNo <= b.DlvrDateSeqNo + b.pLateToleranceDays
     AND b.v_GRIndicator = 'X'
GROUP BY a.Document_No,a.DocumentLine_No,a.dd_scheduleno;


DROP TABLE IF EXISTS fact_purchase_tmp_709_01;
CREATE TABLE fact_purchase_tmp_709_01 as
SELECT iid,
	   v_fact_vendperfid,
       v_dd_DocumentNo,
       v_dd_DocumentItemNo,
       v_dd_ScheduleNo,
       v_ct_DeliveryQty,
       v_ct_ReceivedQty,
       v_DeliveryDate,
       v_StatDeliveryDate,
       v_DlvrComplete,
       v_GRIndicator,
       v_CompanyCode,
		plantcode,
	   convert (BIGINT,0) vQtyEarly,
	   convert (BIGINT,0) vQtyOnTime,
	   vQtyLate,
	   vLateDlvrDays,
	   vEarlyDlvrDays,
	   convert (BIGINT,0) vQtyEarly_ddt,
	   convert (BIGINT,0) vQtyOnTime_ddt,
	   vQtyLate_ddt,
	   vLateDlvrDays_ddt,
	   vEarlyDlvrDays_ddt,
	   IFNULL((SELECT SUM(IFNULL(a.Quantity,0))
	          FROM fact_materialmovement_vendperform a
	          WHERE a.Document_No = b.v_dd_DocumentNo
			  AND a.DocumentLine_No = b.v_dd_DocumentItemNo
			  AND a.dd_scheduleno = b.v_dd_ScheduleNo
	          AND a.MovementType in (122,123,161,162)),0) vQtyReturn,
	   (SELECT max(a.PostingDate)
	    FROM fact_materialmovement_vendperform a
	    WHERE a.Document_No = b.v_dd_DocumentNo
	         AND a.DocumentLine_No = b.v_dd_DocumentItemNo
		     AND a.MovementType = '101') v_LastGRDate,
	   (SELECT min(a.PostingDate)
	    FROM fact_materialmovement_vendperform a
	    WHERE a.Document_No = b.v_dd_DocumentNo
	         AND a.DocumentLine_No = b.v_dd_DocumentItemNo
		     AND a.MovementType = '101') v_FirstGRDate,
	   IFNULL((SELECT count(*) from fact_purchase a
	           WHERE a.dd_DocumentNo = b.v_dd_DocumentNo
		            AND a.dd_DocumentItemNo = b.v_dd_DocumentItemNo
		            AND ifnull(a.ct_ReceivedQty,0) > 0),1) v_tmp_RcvdDlvCount,
	   vInvoiceQty,
	   vPOLineQty,
	   pLateToleranceDays,
	   pEarlyToleranceDays,
	   vPastDueCutOffDays,
	   pflag,
	   DlvrDateSeqNo,
	   StatDateSeqNo
FROM fact_purch_tmp_709_fl_01 b
WHERE b.v_GRIndicator = 'X';

UPDATE fact_purchase_tmp_709_01 pt
SET pt.vQtyEarly = m.vQtyEarly
FROM fact_purchase_tmp_709_01 pt,fact_purchase_tmp_709_QtyEarly m
WHERE m.Document_No = pt.v_dd_DocumentNo
    AND m.DocumentLine_No = pt.v_dd_DocumentItemNo
    AND m.dd_scheduleno = pt.v_dd_ScheduleNo
and pt.v_GRIndicator = 'X';

UPDATE  fact_purchase_tmp_709_01 pt
SET pt.vQtyOnTime=m.vQtyOnTime
 from fact_purchase_tmp_709_QtyOnTime m ,fact_purchase_tmp_709_01 pt
where m.Document_No = pt.v_dd_DocumentNo
and m.DocumentLine_No = pt.v_dd_DocumentItemNo
and m.dd_scheduleno = pt.v_dd_ScheduleNo
and pt.v_GRIndicator = 'X';

UPDATE  fact_purchase_tmp_709_01 pt
SET pt.vQtyEarly_ddt=pt.vQtyEarly_ddt
 from fact_purchase_tmp_709_QtyOnTime m ,fact_purchase_tmp_709_01 pt
	 		where m.Document_No = pt.v_dd_DocumentNo and m.DocumentLine_No = pt.v_dd_DocumentItemNo
	 			and m.dd_scheduleno = pt.v_dd_ScheduleNo
and pt.v_GRIndicator = 'X';


UPDATE fact_purchase_tmp_709_01 pt
SET pt.vQtyOnTime_ddt=pt.vQtyOnTime_ddt
 from fact_purchase_tmp_709_QtyOnTime m ,fact_purchase_tmp_709_01 pt
	 		where m.Document_No = pt.v_dd_DocumentNo and m.DocumentLine_No = pt.v_dd_DocumentItemNo
	 			and m.dd_scheduleno = pt.v_dd_ScheduleNo
and pt.v_GRIndicator = 'X';

drop table if exists fact_purchase_tmp_709_QtyEarly;
drop table if exists fact_purchase_tmp_709_QtyOnTime;
drop table if exists fact_purchase_tmp_709_QtyEarly_ddt;
drop table if exists fact_purchase_tmp_709_QtyOnTime_ddt;


delete from fact_purch_tmp_709_fl_01
where v_GRIndicator = 'X';


drop table if exists fact_purchase_tmp_709_LDays;
create table fact_purchase_tmp_709_LDays as
select min(a.PostingDate) GRDate,
	min(b.v_StatDeliveryDate) DlvrDt,
	a.Document_No,
	a.DocumentLine_No,
	a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo,
	min(a.PostDateSeqNo) PostDateSeqNo
from fact_materialmovement_vendperform a
	inner join fact_purchase_tmp_709_01 b
		on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
where a.MovementType = '101'
	AND a.PostDateSeqNo > (b.StatDateSeqNo + b.pLateToleranceDays)
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo;

drop table if exists fact_purchase_tmp_709_EDays;
create table fact_purchase_tmp_709_EDays as
select max(a.PostingDate) GRDate,
	min(b.v_StatDeliveryDate) DlvrDt,
	a.Document_No,
	a.DocumentLine_No,
	a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo,
	max(a.PostDateSeqNo) PostDateSeqNo
from fact_materialmovement_vendperform a
	inner join fact_purchase_tmp_709_01 b
		on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
where a.MovementType = '101'
	AND a.PostDateSeqNo < (b.StatDateSeqNo - b.pEarlyToleranceDays)
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo;

drop table if exists fact_purchase_tmp_709_LDays_ddt;
create table fact_purchase_tmp_709_LDays_ddt as
select min(a.PostingDate) GRDate,
	min(b.v_DeliveryDate) DlvrDt,
	a.Document_No,
	a.DocumentLine_No,
	a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo,
	min(a.PostDateSeqNo) PostDateSeqNo
from fact_materialmovement_vendperform a
	inner join fact_purchase_tmp_709_01 b
		on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
where a.MovementType = '101'
	AND a.PostDateSeqNo > (b.DlvrDateSeqNo + b.pLateToleranceDays)
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo;

drop table if exists fact_purchase_tmp_709_EDays_ddt;
create table fact_purchase_tmp_709_EDays_ddt as
select max(a.PostingDate) GRDate,
	min(b.v_DeliveryDate) DlvrDt,
	a.Document_No,
	a.DocumentLine_No,
	a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo,
	max(a.PostDateSeqNo) PostDateSeqNo
from fact_materialmovement_vendperform a
	inner join fact_purchase_tmp_709_01 b
		on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
where a.MovementType = '101'
	AND a.PostDateSeqNo < (b.DlvrDateSeqNo - b.pEarlyToleranceDays)
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo;


drop table if exists fact_purchase_tmp_709_calc_X;
create table fact_purchase_tmp_709_calc_X as
select  iid,
	    v_fact_vendperfid,
        v_dd_DocumentNo,
        v_dd_DocumentItemNo,
        v_dd_ScheduleNo,
        v_ct_DeliveryQty,
        v_ct_ReceivedQty,
        v_DeliveryDate,
        v_StatDeliveryDate,
	case when vQtyEarly < 0 then 0 when vQtyEarly > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyEarly end vQtyEarly,
	(case when vQtyOnTime < 0 then 0 when vQtyOnTime > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyOnTime end)
		- (case when vQtyEarly < 0 then 0 when vQtyEarly > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyEarly end) vQtyOnTime,
	vQtyLate,
	vLateDlvrDays,
	convert(BIGINT, 0) vEarlyDlvrDays,
	case when vQtyEarly_ddt < 0 then 0 when vQtyEarly_ddt > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyEarly_ddt end vQtyEarly_ddt,
	(case when vQtyOnTime_ddt < 0 then 0 when vQtyOnTime_ddt > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyOnTime_ddt end)
		- (case when vQtyEarly_ddt < 0 then 0 when vQtyEarly_ddt > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyEarly_ddt end) vQtyOnTime_ddt,
	vQtyLate_ddt,
	vLateDlvrDays_ddt,
	convert(BIGINT, 0) vEarlyDlvrDays_ddt,
	pLateToleranceDays,
	pEarlyToleranceDays,
	vPastDueCutOffDays,
        v_CompanyCode,
		plantcode,
	DlvrDateSeqNo,
	StatDateSeqNo,
	convert(BIGINT, 0) CurrDateSeqNo
from fact_purchase_tmp_709_01 b;

UPDATE fact_purchase_tmp_709_calc_X cx
SET cx.vEarlyDlvrDays=(a.StatDateSeqNo - a.PostDateSeqNo)
		From fact_purchase_tmp_709_EDays a,fact_purchase_tmp_709_calc_X cx,fact_purchase_tmp_709_01 b
		Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
           and a.Document_No = cx.v_dd_DocumentNo and a.DocumentLine_No = cx.v_dd_DocumentItemNo and a.dd_scheduleno = cx.v_dd_ScheduleNo
			and b.vQtyEarly > 0;

UPDATE fact_purchase_tmp_709_calc_X cx
SET cx.vEarlyDlvrDays_ddt=(a.DlvrDateSeqNo - a.PostDateSeqNo)
		From fact_purchase_tmp_709_EDays_ddt a,fact_purchase_tmp_709_calc_X cx,fact_purchase_tmp_709_01 b
		Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
and a.Document_No = cx.v_dd_DocumentNo and a.DocumentLine_No = cx.v_dd_DocumentItemNo and a.dd_scheduleno = cx.v_dd_ScheduleNo
			and b.vQtyEarly_ddt > 0;

UPDATE fact_purchase_tmp_709_calc_X cx
SET cx.CurrDateSeqNo=case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end
	 from tmp_dim_date_001 dd0 ,fact_purchase_tmp_709_calc_X cx
where dd0.DateValue = to_date(current_timestamp)
and dd0.CompanyCode = cx.v_CompanyCode
and  dd0.plantcode_factory = cx.plantcode;


drop table if exists fact_purchase_tmp_709_calc_Y;
create table fact_purchase_tmp_709_calc_Y as
select iid,
	v_fact_vendperfid,
        v_dd_DocumentNo,
        v_dd_DocumentItemNo,
        v_dd_ScheduleNo,
        v_ct_DeliveryQty,
        v_ct_ReceivedQty,
        v_DeliveryDate,
        v_StatDeliveryDate,
	vQtyEarly,
	vQtyOnTime,
	case when StatDateSeqNo < (CurrDateSeqNo - pLateToleranceDays) then v_ct_DeliveryQty - (vQtyEarly+vQtyOnTime)
	     else 0
	end vQtyLate,
	convert(BIGINT, 0) vLateDlvrDays,
	vEarlyDlvrDays,
	vQtyEarly_ddt,
	vQtyOnTime_ddt,
	case when DlvrDateSeqNo < (CurrDateSeqNo - pLateToleranceDays) then v_ct_DeliveryQty - (vQtyEarly+vQtyOnTime)
	     else 0
	end vQtyLate_ddt,
	convert(BIGINT, 0) vLateDlvrDays_ddt,
	vEarlyDlvrDays_ddt,
	pLateToleranceDays,
	pEarlyToleranceDays,
	vPastDueCutOffDays,
	DlvrDateSeqNo,
	StatDateSeqNo,
	ifnull(CurrDateSeqNo, 0) CurrDateSeqNo
from fact_purchase_tmp_709_calc_X b;

UPDATE fact_purchase_tmp_709_calc_Y cy
SET cy.vLateDlvrDays=(a.PostDateSeqNo - a.StatDateSeqNo)
		From fact_purchase_tmp_709_LDays a,fact_purchase_tmp_709_calc_Y cy,fact_purchase_tmp_709_calc_X b
		Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
        and a.Document_No = cy.v_dd_DocumentNo and a.DocumentLine_No = cy.v_dd_DocumentItemNo and a.dd_scheduleno = cy.v_dd_ScheduleNo
		and b.StatDateSeqNo < (b.CurrDateSeqNo - b.pLateToleranceDays) and (b.v_ct_DeliveryQty - (b.vQtyEarly+b.vQtyOnTime)) > 0;


UPDATE fact_purchase_tmp_709_calc_Y cy
SET cy.vLateDlvrDays_ddt=(a.PostDateSeqNo - a.DlvrDateSeqNo)
		From fact_purchase_tmp_709_LDays_ddt a,fact_purchase_tmp_709_calc_Y cy,fact_purchase_tmp_709_calc_X b
		Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
       and a.Document_No = cy.v_dd_DocumentNo and a.DocumentLine_No = cy.v_dd_DocumentItemNo and a.dd_scheduleno = cy.v_dd_ScheduleNo
		and b.DlvrDateSeqNo < (b.CurrDateSeqNo - b.pLateToleranceDays) and (b.v_ct_DeliveryQty - (b.vQtyEarly+b.vQtyOnTime)) > 0;


drop table if exists fact_purchase_tmp_709;
create table fact_purchase_tmp_709 as
select a.iid,
	a.v_fact_vendperfid,
        a.v_dd_DocumentNo,
        a.v_dd_DocumentItemNo,
        a.v_dd_ScheduleNo,
        a.v_ct_DeliveryQty,
        a.v_ct_ReceivedQty,
        a.v_DeliveryDate,
        a.v_StatDeliveryDate,
        a.v_DlvrComplete,
        a.v_GRIndicator,
        a.v_CompanyCode,
	a.plantcode,
	b.vQtyEarly,
	b.vQtyOnTime,
	b.vQtyLate,
	b.vLateDlvrDays,
	b.vEarlyDlvrDays,
	b.vQtyEarly_ddt,
	b.vQtyOnTime_ddt,
	b.vQtyLate_ddt,
	b.vLateDlvrDays_ddt,
	b.vEarlyDlvrDays_ddt,
	a.vQtyReturn,
	a.v_LastGRDate,
	a.v_FirstGRDate,
	a.v_tmp_RcvdDlvCount,
	a.vInvoiceQty,
	a.vPOLineQty,
	a.pLateToleranceDays,
	a.pEarlyToleranceDays,
	a.vPastDueCutOffDays,
	a.pflag,
	a.DlvrDateSeqNo,
	a.StatDateSeqNo,
	convert(BIGINT, 0) CurrDateSeqNo
from fact_purchase_tmp_709_01 a
	inner join fact_purchase_tmp_709_calc_Y b on a.iid = b.iid;

UPDATE fact_purchase_tmp_709 ptm
SET ptm.CurrDateSeqNo=case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end
	 from tmp_dim_date_001 dd0,fact_purchase_tmp_709 ptm
 where dd0.DateValue = CURRENT_DATE and dd0.CompanyCode = ptm.v_CompanyCode and dd0.plantcode_factory = ptm.plantcode;

insert into fact_purchase_tmp_709
select a.iid,
	a.v_fact_vendperfid,
        a.v_dd_DocumentNo,
        a.v_dd_DocumentItemNo,
        a.v_dd_ScheduleNo,
        a.v_ct_DeliveryQty,
        a.v_ct_ReceivedQty,
        a.v_DeliveryDate,
        a.v_StatDeliveryDate,
        a.v_DlvrComplete,
        a.v_GRIndicator,
        a.v_CompanyCode,
	a.plantcode,
	a.vQtyEarly,
	a.vQtyOnTime,
	a.vQtyLate,
	a.vLateDlvrDays,
	a.vEarlyDlvrDays,
	a.vQtyEarly_ddt,
	a.vQtyOnTime_ddt,
	a.vQtyLate_ddt,
	a.vLateDlvrDays_ddt,
	a.vEarlyDlvrDays_ddt,
	a.vQtyReturn,
	a.v_LastGRDate,
	a.v_FirstGRDate,
	a.v_tmp_RcvdDlvCount,
	a.vInvoiceQty,
	a.vPOLineQty,
	a.pLateToleranceDays,
	a.pEarlyToleranceDays,
	a.vPastDueCutOffDays,
	a.pflag,
	a.DlvrDateSeqNo,
	a.StatDateSeqNo,
	convert(BIGINT, 0) CurrDateSeqNo
from fact_purch_tmp_709_fl_01 a;

UPDATE fact_purchase_tmp_709 prtm
SET prtm.CurrDateSeqNo=case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end
from tmp_dim_date_001 dd0 ,fact_purchase_tmp_709 prtm
where dd0.DateValue = CURRENT_DATE and dd0.CompanyCode = prtm.v_CompanyCode and dd0.plantcode_factory = prtm.plantcode
and  dd0.plantcode_factory = prtm.plantcode;




/*** final fact table update ***/

UPDATE fact_purchase
SET fact_purchase.ct_EarlyQty = ifnull(fact_purchase_tmp_709.vQtyEarly, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct v_fact_vendperfid,vQtyEarly from fact_purchase_tmp_709) fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET fact_purchase.ct_LateQty = ifnull(fact_purchase_tmp_709.vQtyLate, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;


UPDATE fact_purchase
SET fact_purchase.ct_AcceptQty = ifnull( (fact_purchase_tmp_709.vQtyOnTime + fact_purchase_tmp_709.vQtyEarly), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_OnTimeQty = ifnull(fact_purchase_tmp_709.vQtyOnTime, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_ReturnQty = ifnull(fact_purchase_tmp_709.vQtyReturn, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;


UPDATE fact_purchase
SET    fact_purchase.ct_OTD = ifnull( ((fact_purchase_tmp_709.v_ct_DeliveryQty
               - (fact_purchase_tmp_709.vQtyEarly
                  + fact_purchase_tmp_709.vQtyLate))
              / (case when fact_purchase_tmp_709.v_ct_DeliveryQty = 0 then 1 else fact_purchase_tmp_709.v_ct_DeliveryQty end)), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_LeadTimeQty = ifnull( ((fact_purchase_tmp_709.vQtyOnTime
               + fact_purchase_tmp_709.vQtyEarly)
              / (case when fact_purchase_tmp_709.v_ct_ReceivedQty=0 then 1 else fact_purchase_tmp_709.v_ct_ReceivedQty end )), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_latedeliverydays = ifnull(fact_purchase_tmp_709.vLateDlvrDays, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_earlydeliverydays = ifnull(fact_purchase_tmp_709.vEarlyDlvrDays, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_EarlyQty_ddt = ifnull(fact_purchase_tmp_709.vQtyEarly_ddt, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_LateQty_ddt = ifnull(fact_purchase_tmp_709.vQtyLate_ddt, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_AcceptQty_ddt = ifnull( (fact_purchase_tmp_709.vQtyOnTime_ddt + fact_purchase_tmp_709.vQtyEarly_ddt), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_OnTimeQty_ddt = ifnull(fact_purchase_tmp_709.vQtyOnTime_ddt, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_OTD_ddt = ifnull(
             ((fact_purchase_tmp_709.v_ct_DeliveryQty
               - (fact_purchase_tmp_709.vQtyEarly_ddt
                  + fact_purchase_tmp_709.vQtyLate_ddt))
              / ( case when fact_purchase_tmp_709.v_ct_DeliveryQty = 0 then 1 else fact_purchase_tmp_709.v_ct_DeliveryQty  end)), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709, fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_LeadTimeQty_ddt = ifnull(
             ((fact_purchase_tmp_709.vQtyOnTime_ddt
               + fact_purchase_tmp_709.vQtyEarly_ddt)
              / (case when fact_purchase_tmp_709.v_ct_ReceivedQty = 0 then 1 else  fact_purchase_tmp_709.v_ct_ReceivedQty end) ), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_latedeliverydays_ddt = ifnull(fact_purchase_tmp_709.vLateDlvrDays_ddt, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.ct_earlydeliverydays_ddt = ifnull(fact_purchase_tmp_709.vEarlyDlvrDays_ddt, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;


UPDATE fact_purchase
SET    fact_purchase.Dim_DateidLastGR = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

MERGE INTO fact_purchase fact
USING ( SELECT fds.v_fact_vendperfid ,ifnull(dt.dim_dateid,1) Dim_DateidLastGR
	FROM fact_purchase_tmp_709 fds
	LEFT JOIN dim_date dt
		ON dt.DateValue = fds.v_LastGRDate AND dt.CompanyCode = fds.v_CompanyCode and dt.plantcode_factory = fds.plantcode
	WHERE fds.v_ct_ReceivedQty > 0 AND fds.v_LastGRDate is not null) src
ON fact.fact_purchaseid = src.v_fact_vendperfid
WHEN MATCHED THEN UPDATE SET fact.Dim_DateidLastGR = src.Dim_DateidLastGR;


UPDATE fact_purchase
SET    fact_purchase.Dim_DateIdFirstGR = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

MERGE INTO fact_purchase fact
USING ( SELECT fds.v_fact_vendperfid ,ifnull(dt.dim_dateid,1) Dim_DateIdFirstGR
	FROM fact_purchase_tmp_709 fds
	LEFT JOIN dim_date dt
		ON dt.DateValue = fds.v_FirstGRDate AND dt.CompanyCode = fds.v_CompanyCode and dt.plantcode_factory = fds.plantcode
	WHERE fds.v_ct_ReceivedQty > 0 AND fds.v_FirstGRDate is not null) src
ON fact.fact_purchaseid = src.v_fact_vendperfid
WHEN MATCHED THEN UPDATE SET fact.Dim_DateIdFirstGR = src.Dim_DateIdFirstGR;


UPDATE fact_purchase
SET    fact_purchase.Dim_GRStatusid =
          CASE
             WHEN fact_purchase_tmp_709.v_ct_ReceivedQty > 0 AND fact_purchase_tmp_709.v_LastGRDate is not null
             THEN
                CASE
                   WHEN vLateDlvrDays > 0
                   THEN 3
                   WHEN vEarlyDlvrDays > 0
		   THEN 4
                   ELSE 5
                END
             ELSE
                CASE
                   WHEN StatDateSeqNo < (CurrDateSeqNo - fact_purchase_tmp_709.pLateToleranceDays )
                   THEN 3
                   ELSE 2
                END
          END
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
SET    fact_purchase.dd_InvStatus =
          CASE
             WHEN (fact_purchase_tmp_709.vInvoiceQty > 0
                   AND fact_purchase_tmp_709.vInvoiceQty <>
                          fact_purchase_tmp_709.vPOLineQty)
             THEN 'Partial'
             WHEN (fact_purchase_tmp_709.vInvoiceQty > 0
                   AND fact_purchase_tmp_709.vInvoiceQty =
                          fact_purchase_tmp_709.vPOLineQty)
             THEN 'Full'
             ELSE fact_purchase.dd_InvStatus
          END
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purchase_tmp_709,fact_purchase
 WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

/*Andrian 2 January 2016 - general method that covered all cases BI-1346*/
drop table if exists tmp_purchfirstlast_001;
create table tmp_purchfirstlast_001 as
select a1.*,
lag(Cumulativreceived,1,0) over (partition by dd_documentno,dd_documentitemno order by stat_deliv,rowseqno)+0.01
 previous_sum_cum from
(select a.*,
SUM(ifnull(a.ct_receivedqty,0)) over(partition by a.dd_DocumentNo,a.dd_DocumentItemNo
ORDER BY a.RowSeqNo ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
as Cumulativreceived
 from
(select dd_documentno,
dd_documentitemno,
dd_scheduleno,
ct_receivedqty,
dt.datevalue as stat_deliv,
dt.companycode,
dt.plantcode_factory,
row_number() over (partition by dd_documentno,
dd_documentitemno order by dt.datevalue,dd_ScheduleNo) as ROWSEQNO
from fact_purchase f, dim_date dt
where  ifnull(f.dim_dateidstatdelivery, 1) = ifnull(dt.dim_dateid,1)
and ct_receivedqty<>0) a ) a1;

drop table if exists tmp_purchfirstlast_002;
create table tmp_purchfirstlast_002 as
select c.*,lag(quantity_cum,1,0) over (partition by dd_documentno,dd_documentitemno order by posting_date,rowseqno)+0.01
 previous_sum_cum from
(select b.*,
SUM(ifnull(b.ct_QtyGROrdUnit,0)) over(partition by b.dd_DocumentNo,b.dd_DocumentItemNo
ORDER BY b.RowSeqNo ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as QUANTITY_CUM  from
(select f.dd_materialdocno,
f.dd_materialdocitemno,
f.dd_materialdocyear,
f.dd_documentno,
f.dd_documentitemno,
f.ct_QtyGROrdUnit,
dt.datevalue as posting_date,
dd_DocumentScheduleNo,
row_number() over (partition by dd_documentno,
dd_documentitemno order by dt.datevalue,dd_DocumentScheduleNo,case when ct_QtyGROrdUnit <> 0 then abs(ct_QtyGROrdUnit)/ct_QtyGROrdUnit else 0 end) as ROWSEQNO
from fact_materialmovement f,dim_movementtype mt,dim_date dt
where  f.dim_movementtypeid = ifnull(mt.dim_movementtypeid,1)
and mt.MovementType in ('101','102','122','123','161','162')
and ifnull(f.dim_DateIdPostingdate, 1) = ifnull(dt.dim_DateId, 1)) b
) c;


drop table if exists tmp_purchfirstlast_003;
create table tmp_purchfirstlast_003 as
select a.dd_documentno,
       a.dd_documentitemno,
       a.dd_scheduleno,
       a.companycode,
	   a.plantcode_factory,
       min(c.posting_date) as first_gr,
       max(b.posting_date) as last_gr
        from tmp_purchfirstlast_001 a
inner join tmp_purchfirstlast_002 b
on a.dd_documentno = b.dd_documentno
and a.dd_documentitemno = b.dd_documentitemno
and round(a.cumulativreceived,2) between round(b.previous_sum_cum,2) and round(b.quantity_cum,2)
inner join tmp_purchfirstlast_002 c
on a.dd_documentno = c.dd_documentno
and a.dd_documentitemno = c.dd_documentitemno
and round(a.previous_sum_cum,2) between round(c.previous_sum_cum,2) and round(c.quantity_cum,2)
group by a.dd_documentno,
       a.dd_documentitemno,
       a.dd_scheduleno,
        a.companycode,
		a.plantcode_factory;

UPDATE fact_purchase p
SET p.dim_dateIdSchedFirstGR_Merck = 1
WHERE p.dim_dateIdSchedFirstGR_Merck <> 1;

UPDATE fact_purchase p
SET p.dim_dateIdSchedLastGR_Merck = 1
WHERE p.dim_dateIdSchedLastGR_Merck <> 1;

UPDATE fact_purchase p
SET p.dim_dateIdSchedFirstGR_Merck = dt.dim_dateId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 FROM tmp_purchfirstlast_003 t, dim_date dt, fact_purchase p
WHERE p.dd_DocumentNo = t.dd_documentno
and p.dd_DocumentItemNo = t.dd_DocumentItemNo
AND p.dd_scheduleno = t.dd_scheduleno
AND dt.companycode = t.companycode
and dt.plantcode_factory = t.plantcode_factory
AND dt.datevalue = First_GR;

UPDATE fact_purchase p
SET p.dim_dateIdSchedLastGR_Merck = dt.dim_dateId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 FROM tmp_purchfirstlast_003 t, dim_date dt,fact_purchase p
WHERE p.dd_DocumentNo = t.dd_documentno
and p.dd_DocumentItemNo = t.dd_DocumentItemNo
AND p.dd_scheduleno = t.dd_scheduleno
AND dt.companycode = t.companycode
and dt.plantcode_factory = t.plantcode_factory
AND dt.datevalue = last_gr;

drop table if exists tmp_purchfirstlast_001;
drop table if exists tmp_purchfirstlast_002;
drop table if exists tmp_purchfirstlast_003;

update fact_purchase
set  Dim_DateidLastGR = dim_dateIdSchedLastGR_Merck
where Dim_DateidLastGR <> dim_dateIdSchedLastGR_Merck;

update fact_purchase
set  Dim_DateidFirstGR = dim_dateIdSchedFirstGR_Merck
where Dim_DateidFirstGR <> dim_dateIdSchedFirstGR_Merck;

/*Georgiana Changes according to APP-7597recalculatin First GR date and Last GR date as in order to have min of Posting Date for First GR Date and max of Posting Date from Last GR date*/
/* Yogini APP-9087 29 Nov 2018 - adding movement type condition */
drop table if exists tmp_forGRDates;
create table tmp_forGRDates as
select distinct  fp.fact_purchaseid,fp.dd_DocumentNo,fp.dd_DocumentItemNo,min(dt.datevalue) as firstgrdate,max(dt.datevalue) as lastgrdate
		from fact_materialmovement mm, fact_purchase fp ,dim_date dt, dim_movementtype mt 
		WHERE mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
		and fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
and ifnull(mm.dim_DateIDPostingDate, 1) = ifnull(dt.dim_dateid, 1)
and mt.MovementType in ('122','161','123','162','101','102') 
group by fp.fact_purchaseid,fp.dd_DocumentNo,fp.dd_DocumentItemNo;

merge into fact_purchase fp
using (select distinct fact_purchaseid,min(dt.dim_dateid) as dim_dateidmin
 from fact_materialmovement mm,  tmp_forGRDates fp ,dim_date dt
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
and ifnull(mm.dim_DateIDPostingDate, 1) = ifnull(dt.dim_dateid, 1)
and dt.datevalue=fp.firstgrdate
group by fact_purchaseid) t
on t.fact_purchaseid=fp.fact_purchaseid
when matched then update set fp.dim_dateidfirstgr=t.dim_dateidmin;

merge into fact_purchase fp
using (select distinct fact_purchaseid,max(dt.dim_dateid) as dim_dateidmax
 from fact_materialmovement mm,  tmp_forGRDates fp ,dim_date dt
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
and ifnull(mm.dim_DateIDPostingDate, 1) = ifnull(dt.dim_dateid, 1)
and dt.datevalue=fp.lastgrdate
group by fact_purchaseid) t
on t.fact_purchaseid=fp.fact_purchaseid
when matched then update set fp.dim_dateidlastgr=t.dim_dateidmax;



 /** OTIF Calc **/

UPDATE fact_purchase f
SET    f.dd_QtyInFull = 'Not Set'
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE (f.dd_QtyInFull <> 'Not Set' OR f.dd_QtyInFull IS NULL);

UPDATE fact_purchase f
SET    f.dd_OnTime_Delv = 'Not Set'
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE (f.dd_OnTime_Delv <> 'Not Set'  OR f.dd_OnTime_Delv IS NULL);

UPDATE fact_purchase f
SET    f.dd_OnTime_Stat = 'Not Set'
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE (f.dd_OnTime_Stat <> 'Not Set' OR f.dd_OnTime_Stat IS NULL);


drop table if exists fact_purch_lineqty_001;
create table fact_purch_lineqty_001 as
select f1.fact_purchaseid,
	f1.ct_ScheduledQty_Merck,
	f1.ct_ReceivedQty,
	f1.dd_DocumentNo,
	f1.dd_DocumentItemNo,
	f1.dd_ScheduleNo,
	m.UnlimitedOverDelivery,
	m.ItemDeliveryComplete,
	f1.ct_OverDeliveryTolerance,
	f1.ct_UnderDeliveryTolerance,
	f1.ct_OnTimeQty_ddt,
	f1.ct_OnTimeQty ct_OnTimeQty_stat,
    f1.dim_DateIdFirstGR,
	f1.dim_DateIdLastGR,
	convert (BIGINT,0) DlvrDateSeqNo,
	convert (BIGINT,0) StatDateSeqNo,
    convert (BIGINT,0) PostingDateSeqNo,
	convert (BIGINT,0) CurrDateSeqNo,
	ifnull((SELECT property_value
		 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.late'), 1) pLateToleranceDays,
	ifnull((SELECT property_value
		 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.early'), 1) pEarlyToleranceDays,
	row_number() over(partition by f1.dd_DocumentNo, f1.dd_DocumentItemNo order by dd.DateValue, f1.dd_ScheduleNo) LineDlvrSeq,
	f1.Dim_DateidStatDelivery,
	f1.Dim_DateidDelivery
FROM (select distinct(f1.*) from fact_purchase f1) f1
	INNER JOIN dim_purchasemisc m on m.Dim_PurchaseMiscid = f1.Dim_PurchaseMiscid
	INNER JOIN Dim_Date sd on sd.Dim_Dateid = ifnull(f1.Dim_DateidStatDelivery, 1)
	INNER JOIN Dim_Date dd on dd.Dim_Dateid = ifnull(f1.Dim_DateidDelivery, 1)
    inner join Dim_Date lgr on lgr.dim_Dateid = ifnull(f1.dim_dateidlastGR, 1)
WHERE m.ItemGRIndicator = 'X'
      AND (f1.dd_QtyInFull = 'Not Set'
      OR f1.dd_OnTime_Delv = 'Not Set'
      OR f1.dd_OnTime_Stat = 'Not Set');

UPDATE fact_purch_lineqty_001 lq
SET lq.DlvrDateSeqNo = case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end
FROM tmp_dim_date_001 dd0,fact_purch_lineqty_001 lq
WHERE dd0.Dim_Dateid = ifnull(lq.Dim_DateidDelivery, 1);

UPDATE fact_purch_lineqty_001 lq
SET lq.StatDateSeqNo=case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end
from tmp_dim_date_001 dd0 ,fact_purch_lineqty_001 lq
WHERE dd0.Dim_Dateid = ifnull(lq.Dim_DateidStatDelivery, 1);

UPDATE fact_purch_lineqty_001 lq
SET lq.PostingDateSeqNo=case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end
from tmp_dim_date_001 dd0 ,fact_purch_lineqty_001 lq
WHERE dd0.Dim_Dateid = ifnull(lq.dim_dateidlastGR, 1);

UPDATE fact_purch_lineqty_001 lq
SET lq.CurrDateSeqNo=case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end
		from tmp_dim_date_001 dd0,fact_purch_lineqty_001 lq,Dim_Date dd
 where dd.Dim_Dateid = ifnull(lq.Dim_DateidDelivery, 1)
AND dd0.DateValue = CURRENT_DATE
and dd0.CompanyCode = dd.CompanyCode
and dd0.plantcode_factory = dd.plantcode_factory;

drop table if exists fact_purch_lineqty_002;
create table fact_purch_lineqty_002 as
select sum(f1.ct_ScheduledQty_Merck) LineDelvQty, sum(f1.ct_ReceivedQty) LineRcvdQty,
	max(f.LineDlvrSeq) MaxLineDlvrSeq,
	f1.dd_DocumentNo f1_DocumentNo, f1.dd_DocumentItemNo f1_DocumentItemNo,f1.dd_scheduleno f1_scheduleno
from fact_purchase f1
	inner join fact_purch_lineqty_001 f on f1.dd_DocumentNo = f.dd_DocumentNo
						and f1.dd_DocumentItemNo = f.dd_DocumentItemNo
						and f1.dd_ScheduleNo = f.dd_ScheduleNo
group by f1.dd_DocumentNo, f1.dd_DocumentItemNo,f1.dd_scheduleno;

drop table if exists fact_purch_otif_001;
create table fact_purch_otif_001 as
select f1.fact_purchaseid,
	/*case when (lq.LineRcvdQty between (lq.LineDelvQty - (lq.LineDelvQty * f1.ct_UnderDeliveryTolerance / 100)) and (lq.LineDelvQty + (lq.LineDelvQty * f1.ct_OverDeliveryTolerance / 100))
				or f1.UnlimitedOverDelivery = 'X' and lq.LineRcvdQty >= (lq.LineDelvQty - (lq.LineDelvQty * f1.ct_UnderDeliveryTolerance / 100)))
			and f1.ct_ReceivedQty > 0
		then 'Y'
		when f1.ct_ReceivedQty >= f1.ct_DeliveryQty
		then 'Y'
		else 'N'
	end */
	case when (lq.LineRcvdQty between (lq.LineDelvQty - (lq.LineDelvQty * f1.ct_UnderDeliveryTolerance / 100)) and (lq.LineDelvQty + (lq.LineDelvQty * f1.ct_OverDeliveryTolerance / 100))
				or f1.UnlimitedOverDelivery = 'X' and lq.LineRcvdQty >= (lq.LineDelvQty - (lq.LineDelvQty * f1.ct_UnderDeliveryTolerance / 100)))
			and f1.ct_ReceivedQty > 0
		then 'Y'
	/*	when (f1.ct_ReceivedQty >= f1.ct_ScheduledQty_Merck)
		then 'Y' */
		else 'N'
	end dd_QtyInFull,
	/* case when f1.DlvrDateSeqNo <= (f1.CurrDateSeqNo + f1.pEarlyToleranceDays)
		then case when f1.ct_ReceivedQty > 0 and f1.ct_OnTimeQty_ddt = f1.ct_ReceivedQty
			then 'Y'
			else 'N'
		     end
		when f1.ct_ReceivedQty > 0
		then 'N'
		else 'Not Set'
	end */
	case when ( (f1.PostingDateSeqNo  between ( f1.DlvrDateSeqNo - f1.pEarlyToleranceDays) and ( f1.DlvrDateSeqNo + f1.pLateToleranceDays)) and  f1.ct_ReceivedQty > 0 and (f1.dim_DateIdFirstGR > 1 or f1.Dim_DateIdLastGR > 1))
			then 'Y'
		when f1.ct_ReceivedQty > 0
		then 'N'
		else 'Not Set'
	end dd_OnTime_Delv,
	/*case when f1.StatDateSeqNo <= (f1.CurrDateSeqNo + f1.pEarlyToleranceDays)
		then case when f1.ct_ReceivedQty > 0 and f1.ct_OnTimeQty_stat = f1.ct_ReceivedQty
			then 'Y'
			else 'N'
		     end
		when f1.ct_ReceivedQty > 0
		then 'N'
		else 'Not Set'
	end */
	case when ((f1.PostingDateSeqNo between ( f1.StatDateSeqNo - f1.pEarlyToleranceDays) and (f1.StatDateSeqNo + f1.pLateToleranceDays)) and f1.ct_ReceivedQty > 0 and (f1.dim_DateIdFirstGR > 1 or f1.Dim_DateIdLastGR > 1))
			then 'Y'
		when f1.ct_ReceivedQty > 0
		then 'N'
		else 'Not Set'
	end dd_OnTime_Stat
from fact_purch_lineqty_001 f1
	inner join fact_purch_lineqty_002 lq on f1.dd_DocumentNo = lq.f1_DocumentNo
						and f1.dd_DocumentItemNo = lq.f1_DocumentItemNo
						and f1.dd_ScheduleNo = lq.f1_ScheduleNo;

/* 25 Apr 2018 Georgiana Changes: Unable to get a stable source of rows fix*/
/*UPDATE fact_purchase f
SET    f.dd_QtyInFull = otf.dd_QtyInFull
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl
From fact_purch_otif_001 otf,fact_purchase f
WHERE f.fact_purchaseid = otf.fact_purchaseid
	and f.dd_QtyInFull <> otf.dd_QtyInFull*/

merge into fact_purchase f
using (select distinct f.fact_purchaseid,otf.dd_QtyInFull
From fact_purch_otif_001 otf,fact_purchase f
WHERE f.fact_purchaseid = otf.fact_purchaseid
and f.dd_QtyInFull <> otf.dd_QtyInFull) t
on t.fact_purchaseid=f.fact_purchaseid
when matched then update set f.dd_QtyInFull = t.dd_QtyInFull
,f.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

UPDATE fact_purchase f
SET    f.dd_OnTime_Delv = otf.dd_OnTime_Delv
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purch_otif_001 otf,fact_purchase f
WHERE f.fact_purchaseid = otf.fact_purchaseid
	and f.dd_OnTime_Delv <> otf.dd_OnTime_Delv;

UPDATE fact_purchase f
SET    f.dd_OnTime_Stat = otf.dd_OnTime_Stat
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_purch_otif_001 otf,fact_purchase f
WHERE f.fact_purchaseid = otf.fact_purchaseid
	and f.dd_OnTime_Stat <> otf.dd_OnTime_Stat;


UPDATE fact_purchase fp
SET  fp.dd_OnTime_Stat_Document = 'Not Set'
WHERE fp.dd_OnTime_Stat_Document = 'Y';

UPDATE fact_purchase fp
SET  fp.dd_InFull_Stat_Document = 'Not Set'
WHERE fp.dd_InFull_Stat_Document = 'Y';

update fact_purchase
set dd_iotif_merck = 'Not Set'
WHERE dd_iotif_merck = 'Y';

DROP TABLE if exists tmp_SclCount_Fact_purchase;
CREATE TABLE tmp_SclCount_Fact_purchase AS
SELECT dd_DocumentNo,std.monthyear as monthyear1, sum(case when dd_ontime_stat = 'Y' THEN 1 ELSE 0 END) as OnTimeCnt,sum(case when dd_qtyinfull = 'Y' THEN 1 ELSE 0 END) as InFullCnt ,count(*) scheduleCnt
from fact_purchase p, dim_Date std, dim_purchasemisc pm
where p.dim_dateidstatdelivery = std.dim_dateid
AND p.dim_purchasemiscid = pm.dim_purchasemiscid
AND std.datevalue <= current_date
and (p.dim_partid <> 1 AND pm.ItemReturn <> 'X')
group by dd_DocumentNo,std.monthyear;

UPDATE fact_purchase fp
SET  fp.dd_OnTime_Stat_Document = 'Y'
 FROM tmp_SclCount_Fact_purchase fp2 ,dim_Date st,fact_purchase fp
WHERE fp.dd_DocumentNo = fp2.dd_DocumentNo
AND fp.dim_dateidstatdelivery = st.dim_DateId
AND st.monthyear = fp2.monthyear1
AND fp2.scheduleCnt = fp2.OnTimeCnt
AND fp.dd_OnTime_Stat_Document <> 'Y';

UPDATE fact_purchase fp
SET  fp.dd_InFull_Stat_Document = 'Y'
 FROM tmp_SclCount_Fact_purchase fp2 ,dim_Date st,fact_purchase fp
WHERE fp.dd_DocumentNo = fp2.dd_DocumentNo
AND fp.dim_dateidstatdelivery = st.dim_DateId
AND st.monthyear = fp2.monthyear1
AND fp2.scheduleCnt = fp2.InFullCnt
AND fp.dd_InFull_Stat_Document <> 'Y';

update fact_purchase
set dd_iotif_merck = 'Y'
where dd_OnTime_Stat_Document = 'Y' and dd_InFull_Stat_Document = 'Y'
and dd_iotif_merck <> 'Y';

DROP TABLE if exists tmp_SclCount_Fact_purchase;


DROP TABLE IF EXISTS fact_materialmovement_upd;
CREATE TABLE fact_materialmovement_upd AS
select dd_DocumentNo,dd_DocumentItemNo,dd_DocumentScheduleNo,sum(ct_QtyEntryUOM) ct_QtyEntryUOM
from fact_materialmovement f, dim_movementtype mt
where ifnull(mt.dim_movementtypeid,1) = ifnull(f.dim_movementtypeid,1)
and mt.MovementType in ('101','102','122','123','161','162')
group by dd_DocumentNo,dd_DocumentItemNo,dd_DocumentScheduleNo;

/* DROP TABLE IF EXISTS fact_materialmovement_upd_add_schedule
CREATE TABLE fact_materialmovement_upd_add_schedule AS
select upd.dd_DocumentNo,upd.dd_DocumentItemNo,f.dd_DocumentScheduleNo,upd.ct_QtyEntryUOM from fact_materialmovement_upd upd, fact_materialmovement f, dim_movementtype mt
where mt.dim_movementtypeid = f.dim_movementtypeid
and upd.dd_DocumentNo = f.dd_DocumentNo
and upd.dd_DocumentItemNo = f.dd_DocumentItemNo
and upd.dd_MaterialDocNo = f.dd_MaterialDocNo
and mt.MovementType in (101,102,122,123,161,162)
order by f.dd_DocumentScheduleNo */

UPDATE fact_purchase pr
SET pr.ct_OnTimeQty = mm.ct_QtyEntryUOM
FROM fact_materialmovement_upd mm,fact_purchase pr
WHERE pr.dd_DocumentNo = mm.dd_DocumentNo
AND pr.dd_DocumentItemNo = mm.dd_DocumentItemNo
AND pr.dd_scheduleno = mm.dd_DocumentScheduleNo
AND ct_OnTimeQty <> mm.ct_QtyEntryUOM;

DROP TABLE IF EXISTS tmp_fact_mm_001;
CREATE TABLE tmp_fact_mm_001 AS
select f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.dd_ScheduleNo,
	ct_OnTimeQty,
	f.Dim_AfsSizeId,
	row_number() over (partition by f.dd_DocumentNo,f.dd_DocumentItemNo,f.Dim_AfsSizeId
				order by dd.DateValue, f.dd_ScheduleNo) as RowSeqNum
from fact_purchase f inner join dim_date dd on dd.Dim_Dateid = f.Dim_DateidDelivery
where exists (select 1 from fact_materialmovement f1
			inner join dim_movementtype mt1 on ifnull(f1.Dim_MovementTypeid,1) = ifnull(mt1.Dim_MovementTypeid,1)
	    where f.dd_DocumentNo = f1.dd_DocumentNo and f.dd_DocumentItemNo = f1.dd_DocumentItemNo
		and ifnull(f.Dim_AfsSizeId, 1) = ifnull(f1.Dim_AfsSizeId, 1)
		and f1.Dim_DateIdDelivery = 1 and mt1.MovementType in ('101','102','122','123','161','162'));
DROP TABLE IF EXISTS tmp_fact_mm_002;
CREATE TABLE tmp_fact_mm_002 AS
select f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.dd_ScheduleNo,
	sum(b.ct_OnTimeQty) OnTimeQty
from tmp_fact_mm_001 f
	inner join tmp_fact_mm_001 b
	on f.dd_DocumentNo = b.dd_DocumentNo and f.dd_DocumentItemNo = b.dd_DocumentItemNo and ifnull(f.Dim_AfsSizeId, 1) = ifnull(b.Dim_AfsSizeId, 1)
where f.RowSeqNum >= b.RowSeqNum
group by f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.dd_ScheduleNo,
	f.RowSeqNum;
drop table if exists tmp_fact_mm_purch;
create table tmp_fact_mm_purch as
SELECT f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_ScheduleNo,
       (po.cumudlvrreceivedqty - ontimeqty) diff
FROM tmp_fact_purch_002 po
	inner join tmp_fact_mm_002 f
		on po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and po.dd_ScheduleNo = f.dd_ScheduleNo;

update fact_purchase pr
set pr.ct_lateqty = tmp.diff
from tmp_fact_mm_purch tmp,	fact_purchase pr
where pr.dd_DocumentNo = tmp.dd_DocumentNo and pr.dd_DocumentItemNo = tmp.dd_DocumentItemNo and pr.dd_ScheduleNo = tmp.dd_ScheduleNo
and pr.ct_lateqty <> tmp.diff and pr.ct_OnTimeQty <> 0 and tmp.diff >= 0;

/* Negative late qtys set to 0 */
update fact_purchase pr
set pr.ct_lateqty = 0
from tmp_fact_mm_purch tmp, fact_purchase pr
where pr.dd_DocumentNo = tmp.dd_DocumentNo and pr.dd_DocumentItemNo = tmp.dd_DocumentItemNo and pr.dd_ScheduleNo = tmp.dd_ScheduleNo
and tmp.diff < 0;
/* Negative late qtys set to 0 */

update fact_purchase pr
set pr.ct_OnTimeQty = pr.ct_ItemQty - pr.ct_LateQty
from tmp_fact_mm_purch tmp,fact_purchase pr
where pr.dd_DocumentNo = tmp.dd_DocumentNo and pr.dd_DocumentItemNo = tmp.dd_DocumentItemNo and pr.dd_ScheduleNo = tmp.dd_ScheduleNo
/* and tmp.diff <> 0 */
and pr.ct_OnTimeQty <> pr.ct_ItemQty - pr.ct_LateQty and pr.ct_OnTimeQty <> 0;


drop table if exists tmp_purch_openqty_itemqty;
create table tmp_purch_openqty_itemqty as
select f_deliv.dd_DocumentNo,f_deliv.dd_DocumentItemNo,f_deliv.dd_ScheduleNo,case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 then 0.0000 else (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) end openqty, f_deliv.ct_itemqty
from fact_purchase f_deliv, dim_purchasemisc atrb
where f_deliv.dim_purchasemiscid = atrb.dim_purchasemiscid;

drop table if exists tmp_purch_openqty_itemqty_equal;
create table tmp_purch_openqty_itemqty_equal as
select DISTINCT dd_documentno,dd_documentitemno,dd_scheduleno from tmp_purch_openqty_itemqty
where openqty = ct_itemqty;

/* 25 Apr 2018 Georgiana Changes: Unable to get a stable source of rows fix*/
/*update fact_purchase pr
set ct_lateqty = 0, ct_OnTimeQty = 0
from tmp_purch_openqty_itemqty_equal tmp, fact_purchase pr
where pr.dd_documentno = tmp.dd_documentno
and pr.dd_documentitemno = tmp.dd_documentitemno
and pr.dd_scheduleno = tmp.dd_scheduleno*/

merge into fact_purchase pr
using (select distinct pr.fact_purchaseid
from tmp_purch_openqty_itemqty_equal tmp, fact_purchase pr
where pr.dd_documentno = tmp.dd_documentno
and pr.dd_documentitemno = tmp.dd_documentitemno
and pr.dd_scheduleno = tmp.dd_scheduleno) t 
on pr.fact_purchaseid=t.fact_purchaseid
when matched then update set ct_lateqty = 0, ct_OnTimeQty = 0;

/* Madalina 4 Oct 2016 - add missing updates for Purchasing */
/*Georgiana EA Transition Addons*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MKPF_BKTXT, 'Not Set')) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MKPF_BKTXT
		from fact_materialmovement mm, MKPF_MSEG m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG_ZEILE) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_documentheadertext = ifnull(MKPF_BKTXT, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MKPF_BKTXT, 'Not Set')) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MKPF_BKTXT
		from fact_materialmovement mm, MKPF_MSEG1 m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG1_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG1_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_documentheadertext = ifnull(MKPF_BKTXT, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(dt.dim_dateid) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as dim_dateid
		from fact_materialmovement mm, MKPF_MSEG m, dim_date dt, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND dt.CompanyCode = m.MSEG_BUKRS
		AND m.MKPF_BLDAT = dt.DateValue
		and m.MSEG_WERKS = dt.plantcode_factory
		AND mm.dd_MaterialDocNo = m.MSEG_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dim_DateIDMaterialDocDate = upd.dim_dateid
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(dt.dim_dateid) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as dim_dateid
		from fact_materialmovement mm, MKPF_MSEG1 m, dim_date dt, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND dt.CompanyCode = m.MSEG1_BUKRS
		and m.MSEG1_WERKS = dt.plantcode_factory
		AND m.MKPF_BLDAT = dt.DateValue
		AND mm.dd_MaterialDocNo = m.MSEG1_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG1_ZEILE) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dim_DateIDMaterialDocDate=upd.dim_dateid
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(dt.dim_dateid) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as dim_dateid
		from fact_materialmovement mm, MKPF_MSEG m, dim_date dt, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND dt.CompanyCode = m.MSEG_BUKRS
		AND m.MKPF_CPUDT = dt.DateValue
		and m.MSEG_WERKS = dt.plantcode_factory
		AND mm.dd_MaterialDocNo = m.MSEG_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dim_dateidaccountiddoccreated = ifnull(upd.dim_dateid, 1)
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(dt.dim_dateid) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as dim_dateid
		from fact_materialmovement mm, MKPF_MSEG1 m, dim_date dt, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND dt.CompanyCode = m.MSEG1_BUKRS
		AND m.MKPF_CPUDT = dt.DateValue
		and m.MSEG1_WERKS = dt.plantcode_factory
		AND mm.dd_MaterialDocNo = m.MSEG1_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG1_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dim_dateidaccountiddoccreated = ifnull(upd.dim_dateid, 1)
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MKPF_USNAM, 'Not Set')) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MKPF_USNAM
		from fact_materialmovement mm, MKPF_MSEG m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_username = ifnull(MKPF_USNAM, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value( ifnull(MKPF_USNAM, 'Not Set')) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MKPF_USNAM
		from fact_materialmovement mm, MKPF_MSEG1 m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG1_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG1_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_username = ifnull(MKPF_USNAM, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MSEG_ABLAD, 'Not Set')) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MSEG_ABLAD
		from fact_materialmovement mm, MKPF_MSEG m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_unloadingpoint_mseg = ifnull(MSEG_ABLAD, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MSEG1_ABLAD, 'Not Set'))  over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MSEG1_ABLAD
		from fact_materialmovement mm, MKPF_MSEG1 m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG1_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG1_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_unloadingpoint_mseg = ifnull(MSEG1_ABLAD, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MSEG_SMBLN, 'Not Set')) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MSEG_SMBLN
		from fact_materialmovement mm, MKPF_MSEG m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_numberofmaterialdoc = ifnull(MSEG_SMBLN, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MSEG1_SMBLN, 'Not Set'))  over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MSEG1_SMBLN
		from fact_materialmovement mm, MKPF_MSEG1 m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG1_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG1_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_numberofmaterialdoc = ifnull(MSEG1_SMBLN, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MSEG_WEMPF, 'Not Set')) over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MSEG_WEMPF
		from fact_materialmovement mm, MKPF_MSEG m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_goodsrecipent = ifnull(MSEG_WEMPF, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

merge into fact_purchase fp
using ( select distinct fact_purchaseid,
			first_value(ifnull(MSEG1_WEMPF, 'Not Set'))  over (partition by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleno order by '') as MSEG1_WEMPF
		from fact_materialmovement mm, MKPF_MSEG1 m, fact_purchase fp
		WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
		AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
		AND fp.dd_scheduleno = mm.dd_DocumentScheduleNo
		AND mm.dd_MaterialDocNo = m.MSEG1_MBLNR
		AND mm.dd_MaterialDocItemNo = m.MSEG1_ZEILE ) upd
on fp.fact_purchaseid = upd.fact_purchaseid
when matched then update
Set fp.dd_goodsrecipent = ifnull(MSEG1_WEMPF, 'Not Set')
 ,fp.dw_update_date = current_timestamp; /* Added automatically by update_dw_update_date.pl*/

/*End of EA Changes*/
/* 4 Oct 2016 */

drop table if exists fact_materialmovement_upd;
drop table if exists fact_materialmovement_upd_add_schedule;
drop table if exists tmp_fact_mm_001;
drop table if exists tmp_fact_mm_002;

drop table if exists tmp_purch_openqty_itemqty;
drop table if exists tmp_purch_openqty_itemqty_equal;


INSERT INTO processinglog (processinglogid,referencename, startdate, description)
VALUES ((select ifnull(max(processinglogid),0) + 1 from processinglog),
	'bi_purchase_vendorperformance_processing',
	current_timestamp,
	'bi_purchase_vendorperformance_processing END');

drop table if exists tmp_fact_materialmovement_001;
DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink;
DROP TABLE IF EXISTS tmp_fact_purch_001;
DROP TABLE IF EXISTS tmp_fact_purch_002;


Drop table if exists fact_purchase_tmp_709;
Drop table if exists doc_tmp_709;
Drop table if exists mainholder_709;
Drop table if exists holder_709;
Drop table if exists fact_purch_tmp_709_fl_01;
drop table if exists fact_purch_invqty_001;
drop table if exists fact_purch_lineqty_001;
drop table if exists fact_purch_lineqty_002;
drop table if exists tmp_dim_date_001;
drop table if exists tmp_dimdt_holiday_001;
drop table if exists tmp_dimdt_holiday_002;
drop table if exists fact_purchase_tmp_709_LDays;
drop table if exists fact_purchase_tmp_709_EDays;
drop table if exists fact_purchase_tmp_709_LDays_ddt;
drop table if exists fact_purchase_tmp_709_EDays_ddt;
drop table if exists fact_purch_otif_001;

