drop table if exists AFVC_AFVV_remove_dupl;
CREATE TABLE AFVC_AFVV_remove_dupl AS 
SELECT distinct x.* FROM AFVC_AFVV x;

drop table if exists AFVC_AFVV_test;
RENAME TABLE AFVC_AFVV TO AFVC_AFVV_test; 
RENAME TABLE AFVC_AFVV_remove_dupl TO AFVC_AFVV;

Drop table if exists pGlobalCurrency_po_77;

Create table pGlobalCurrency_po_77(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_77(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_77
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

Drop table if exists fact_productionoperation_tmp;
Drop table if exists max_holder_prop;

Create table fact_productionoperation_tmp as
SELECT dd_RoutingOperationNo,dd_GeneralOrderCounter
FROM fact_productionoperation ;

Create table max_holder_prop
as
Select ifnull(max(fact_productionoperationid),0) maxid
from fact_productionoperation;

INSERT INTO fact_productionoperation(fact_productionoperationid,
									amt_Meteriel_PrimaryCost,
									amt_ActivityCostTtl,
									amt_Price,
									amt_PriceUnit,
									ct_ActivityDuration,
									ct_ActualWork,
									ct_ActivityWorkInvolved,
									dim_businessareaid,
									dim_companyid,
									dim_controllingareaid,
									dim_materialgroupid,
									dim_currencyid,
									dim_plantid,
									dim_profitcenterid,
									dim_purchasegroupid,
									dim_purchaseorgid,
									dim_tasklisttypeid,
									dim_vendorid,
									dim_costcenterid,
									dd_PODocumentNo,
									dim_functionalareaid,
									dim_objecttypeid,
									dim_unitofmeasureid,
									dd_RoutingOperationNo,
									dd_GeneralOrderCounter,
									dd_POItemNo,
									Dim_DateIdActualStartExec,
									Dim_DateIdActualFinishExec,
									dd_hourActualStartExec,
									dd_hourActualFinishExe,
									Dim_DateIdSchedStartExec,
									Dim_DateIdSchedFinishExec,
									dd_hourSchedStartExec,
									dd_hourSchedFinishExec,
									dd_RoutingRefSequence,
									dd_Operationshorttext,
									dd_descriptionline2,
									dd_OperationNumber,
									dd_WorkCenter,
									dd_bomexplosionno,
									dim_productionorderstatusid,
									dim_currencyid_TRA,
									dim_currencyid_GBL)
		SELECT maxid + row_number() over(order by ''),tp.*
		From (SELECT DISTINCT
				ifnull(AA.AFVC_MAT_PRKST, 0) amt_Meteriel_PrimaryCost,
				ifnull(AA.AFVC_PRKST, 0) amt_ActivityCostTtl,
				ifnull(AA.AFVC_PREIS, 0) amt_Price,
				ifnull(AA.AFVC_PEINH, 0) amt_PriceUnit,
				ifnull(AA.AFVV_DAUNO, 0) ct_ActivityDuration,
				ifnull(AA.AFVV_ISMNW, 0) ct_ActualWork,
				ifnull(AA.AFVV_ARBEI, 0) ct_ActivityWorkInvolved,
				1	dim_businessareaid,
				1	dim_companyid,
				1	dim_controllingareaid,
				1	dim_materialgroupid,
				1	dim_currencyid,
				1	dim_plantid,
				1	dim_profitcenterid,
				1	dim_purchasegroupid,
				1	dim_purchaseorgid,
				1	dim_tasklisttypeid,
				1	dim_vendorid,
				1	dim_costcenterid,
				ifnull(AA.AFVC_EBELN, 'Not Set') dd_PODocumentNo,
				1	dim_functionalareaid,
				1	dim_objecttypeid,
				1	dim_unitofmeasureid,
				ifnull(AA.AFVC_AUFPL, 0) dd_RoutingOperationNo,
				ifnull(AA.AFVC_APLZL, 0) dd_GeneralOrderCounter,
				ifnull(AA.AFVC_EBELP, 0) dd_POItemNo,
				1	Dim_DateIdActualStartExec,
				1	Dim_DateIdActualFinishExec,
				ifnull(AA.AFVV_ISDZ, 'Not Set') dd_hourActualStartExec,
				ifnull(AA.AFVV_IEDZ, 'Not Set') dd_hourActualFinishExe,
				1	Dim_DateIdSchedStartExec,
				1	Dim_DateIdSchedFinishExec,
				ifnull(AA.AFVV_FSAVZ, 'Not Set') dd_hourSchedStartExec,
				ifnull(AA.AFVV_FSEDZ, 'Not Set') dd_hourSchedFinishExec,
				ifnull(AA.AFVC_VPLFL, 'Not Set') dd_RoutingRefSequence,
				ifnull(AA.AFVC_LTXA1, 'Not Set') dd_Operationshorttext,
				ifnull(AA.AFVC_LTXA2, 'Not Set') dd_descriptionline2,
				'Not Set'	dd_OperationNumber,
				'Not Set'	dd_WorkCenter,
				'Not Set'	dd_bomexplosionno,
				1	dim_productionorderstatusid,
				/*ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = AFVC_WAERS),1) */ CONVERT(BIGINT,1) AS Dim_Currencyid_TRA,
				/* ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pGlobalCurrency),1) */ CONVERT(BIGINT,1) AS dim_Currencyid_GBL
		FROM  AFVC_AFVV AA,pGlobalCurrency_po_77
			WHERE NOT EXISTS
						 (SELECT 1
							FROM fact_productionoperation_tmp pop
						   WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
								 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL)) tp, max_holder_prop m;


UPDATE fact_productionoperation pop
SET pop.dim_currencyid_TRA = cur.dim_currencyid,
dw_update_date = CURRENT_TIMESTAMP
FROM Dim_Currency cur, AFVC_AFVV AA, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND cur.CurrencyCode = AFVC_WAERS
AND pop.dim_currencyid_TRA <> cur.dim_currencyid;

UPDATE fact_productionoperation pop
SET pop.dim_Currencyid_GBL = cur.dim_currencyid,
dw_update_date = CURRENT_TIMESTAMP
FROM Dim_Currency cur, AFVC_AFVV AA, pGlobalCurrency_po_77, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND cur.CurrencyCode = pGlobalCurrency
AND pop.dim_Currencyid_GBL <> cur.dim_currencyid;

Drop table if exists fact_productionoperation_tmp;
Drop table if exists max_holder_prop;

DELETE FROM fact_productionoperation
WHERE EXISTS
          (SELECT 1
             FROM AFVC_AFVV
            WHERE     AFVC_AUFPL = dd_RoutingOperationNo
                  AND AFVC_APLZL = dd_GeneralOrderCounter
				  AND AUFK_LOEKZ = 'X');

UPDATE fact_productionoperation pop
SET amt_Meteriel_PrimaryCost = ifnull(AFVC_MAT_PRKST ,0)
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_Meteriel_PrimaryCost <> ifnull(AA.AFVC_MAT_PRKST ,0);

UPDATE fact_productionoperation pop
SET amt_ActivityCostTtl = ifnull (AFVC_PRKST ,0)
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_ActivityCostTtl <> ifnull (AFVC_PRKST ,0);

UPDATE fact_productionoperation pop
SET amt_Price = ifnull(AFVC_PREIS ,0)
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_Price <> ifnull(AA.AFVC_PREIS ,0);

UPDATE fact_productionoperation pop
SET amt_PriceUnit = ifnull(AFVC_PEINH ,0)
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_PriceUnit <> ifnull(AA.AFVC_PEINH ,0);

UPDATE fact_productionoperation pop
SET ct_ActivityDuration = ifnull(AFVV_DAUNO ,0)
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActivityDuration <> ifnull(AA.AFVV_DAUNO ,0);

UPDATE fact_productionoperation pop
SET ct_ActualWork = ifnull(AFVV_ISMNW ,0)
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActualWork <> ifnull(AA.AFVV_ISMNW ,0);


UPDATE fact_productionoperation pop
SET ct_ActivityWorkInvolved = ifnull(AFVV_ARBEI ,0)
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActivityWorkInvolved <> ifnull(AA.AFVV_ARBEI ,0);

UPDATE fact_productionoperation pop
SET dim_businessareaid = bsar.dim_businessareaid
	from AFVC_AFVV AA,dim_businessarea bsar, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_GSBER = bsar.Businessarea
		AND pop.dim_businessareaid <> bsar.dim_businessareaid;

UPDATE fact_productionoperation pop
SET dim_companyid = comp.dim_companyid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_company comp, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_BUKRS = comp.companycode
		AND pop.dim_companyid <> comp.dim_companyid;

UPDATE fact_productionoperation pop
SET dim_controllingareaid = ctrar.dim_controllingareaid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_controllingarea ctrar, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_ANFKOKRS = ctrar.controllingareacode
		AND pop.dim_controllingareaid <> ctrar.dim_controllingareaid;

UPDATE fact_productionoperation pop
SET dim_materialgroupid = matgp.dim_materialgroupid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_materialgroup matgp, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_MATKL = matgp.materialgroupcode
		AND pop.dim_materialgroupid <> matgp.dim_materialgroupid;

UPDATE fact_productionoperation pop
SET dim_currencyid = cur.dim_currencyid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_currency cur, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_WAERS = cur.currencycode
		AND pop.dim_currencyid <> cur.dim_currencyid;

UPDATE fact_productionoperation pop
SET dim_plantid = pla.dim_plantid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_plant pla, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_WERKS = pla.plantcode
		AND pop.dim_plantid <> pla.dim_plantid;

UPDATE fact_productionoperation pop
SET dim_profitcenterid = proctr.dim_profitcenterid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_profitcenter proctr, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_PRCTR = proctr.profitcentercode
		AND pop.dim_profitcenterid <> proctr.dim_profitcenterid;

UPDATE fact_productionoperation pop
SET dim_purchasegroupid = porgp.dim_purchasegroupid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_purchasegroup porgp, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_EKGRP = porgp.purchasegroup
		AND pop.dim_purchasegroupid <> porgp.dim_purchasegroupid;

UPDATE fact_productionoperation pop
SET dim_purchaseorgid = pororg.dim_purchaseorgid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_purchaseorg pororg, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_EKORG = pororg.purchaseorgcode
		AND pop.dim_purchaseorgid <> pororg.dim_purchaseorgid;

UPDATE fact_productionoperation pop
SET dim_tasklisttypeid = tsklt.dim_tasklisttypeid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_tasklisttype tsklt, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_PLNTY = tsklt.tasklisttypecode
		AND pop.dim_tasklisttypeid <> tsklt.dim_tasklisttypeid;

UPDATE fact_productionoperation pop
SET dim_vendorid = ven.dim_vendorid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_vendor ven, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_LIFNR = ven.vendornumber
		AND pop.dim_vendorid <> ven.dim_vendorid;

UPDATE fact_productionoperation pop
SET dim_costcenterid = coctr.dim_costcenterid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_costcenter coctr, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_ANFKO = coctr.code
		AND pop.dim_costcenterid <> coctr.dim_costcenterid;

UPDATE fact_productionoperation pop
SET dd_PODocumentNo = ifnull(AFVC_EBELN, 'Not Set')
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_PODocumentNo <> ifnull(AA.AFVC_EBELN, 'Not Set');

UPDATE fact_productionoperation pop
SET dim_functionalareaid = fctar.dim_functionalareaid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_functionalarea fctar, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_FUNC_AREA = fctar.functionalarea
		AND pop.dim_functionalareaid <> fctar.dim_functionalareaid;

UPDATE fact_productionoperation pop
SET dim_objecttypeid = objt.dim_objecttypeid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_objecttype objt, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_OTYPE = objt.objecttype
		AND pop.dim_objecttypeid <> objt.dim_objecttypeid;

UPDATE fact_productionoperation pop
SET dim_unitofmeasureid = uniom.dim_unitofmeasureid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_unitofmeasure uniom, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVV_MEINH = uniom.uom
		AND pop.dim_unitofmeasureid <> uniom.dim_unitofmeasureid;

UPDATE fact_productionoperation pop
SET dd_POItemNo = ifnull(AFVC_EBELP, 0)
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_POItemNo <> ifnull(AA.AFVC_EBELP, 0);

UPDATE fact_productionoperation pop
SET Dim_DateIdActualStartExec = asedt.Dim_DateId
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_date asedt, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVV_ISDD = asedt.datevalue
		AND AA.AFVC_BUKRS = asedt.companycode
		AND pop.Dim_DateIdActualStartExec <> asedt.Dim_DateId;

UPDATE fact_productionoperation pop
SET Dim_DateIdActualFinishExec = afedt.Dim_DateId
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_date afedt, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVV_IEDD = afedt.datevalue
		AND AA.AFVC_BUKRS = afedt.companycode
		AND pop.Dim_DateIdActualFinishExec <> afedt.Dim_DateId;

UPDATE fact_productionoperation pop
SET dd_hourActualStartExec = ifnull(AFVV_ISDZ, 'Not Set')
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_hourActualStartExec <> ifnull(AA.AFVV_ISDZ, 'Not Set');

UPDATE fact_productionoperation pop
SET dd_hourActualFinishExe = ifnull(AFVV_IEDZ, 'Not Set')
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_hourActualFinishExe <> ifnull(AA.AFVV_IEDZ, 'Not Set');

UPDATE fact_productionoperation pop
SET Dim_DateIdSchedStartExec = ssedt.Dim_DateId
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_date ssedt, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVV_FSAVD = ssedt.datevalue
		AND AA.AFVC_BUKRS = ssedt.companycode
		AND pop.Dim_DateIdSchedStartExec <> ssedt.Dim_DateId;

UPDATE fact_productionoperation pop
SET Dim_DateIdSchedFinishExec = sfedt.Dim_DateId
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA,dim_date sfedt, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVV_FSEDD = sfedt.datevalue
		AND AA.AFVC_BUKRS = sfedt.companycode
		AND pop.Dim_DateIdSchedFinishExec <> sfedt.Dim_DateId;

UPDATE fact_productionoperation pop
SET dd_hourSchedStartExec = ifnull(AFVV_FSAVZ, 'Not Set')
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_hourSchedStartExec <> ifnull(AA.AFVV_FSAVZ, 'Not Set');

UPDATE fact_productionoperation pop
SET dd_hourSchedFinishExec = ifnull(AFVV_FSEDZ, 'Not Set')
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_hourSchedFinishExec <> ifnull(AA.AFVV_FSEDZ, 'Not Set');

UPDATE fact_productionoperation pop
SET dd_RoutingRefSequence = ifnull(AFVC_VPLFL, 'Not Set')
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_RoutingRefSequence <> ifnull(AA.AFVC_VPLFL, 'Not Set');

UPDATE fact_productionoperation pop
SET dd_Operationshorttext = ifnull(AFVC_LTXA1, 'Not Set')
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_Operationshorttext <> ifnull(AA.AFVC_LTXA1, 'Not Set');

UPDATE fact_productionoperation pop
SET dd_descriptionline2 = ifnull(AFVC_LTXA2, 'Not Set')
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from AFVC_AFVV AA, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.dd_descriptionline2 <> ifnull(AA.AFVC_LTXA2, 'Not Set');

UPDATE fact_productionoperation pop
SET pop.dd_OrderNumber = po.dd_OrderNumber
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from fact_productionorder po , fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = po.dd_RoutingOperationNo;

UPDATE fact_productionoperation pop
SET dd_OperationNumber = po.dd_OperationNumber,
dd_WorkCenter = po.dd_WorkCenter,
dd_bomexplosionno = ifnull(po.dd_bomexplosionno,'Not Set'),
dim_productionorderstatusid = po.dim_productionorderstatusid
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	from fact_productionorder po , fact_productionoperation pop
WHERE	pop.dd_OrderNumber = po.dd_ordernumber;

UPDATE fact_productionoperation pop
SET dd_OperationNumber = 'Not Set'
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	WHERE dd_OperationNumber is NULL;

UPDATE fact_productionoperation pop
SET dd_WorkCenter = 'Not Set'
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	WHERE dd_WorkCenter is NULL;

UPDATE fact_productionoperation pop
SET dd_bomexplosionno = 'Not Set'
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	WHERE dd_bomexplosionno is NULL;

UPDATE fact_productionoperation pop
SET dim_productionorderstatusid = 1
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	WHERE dim_productionorderstatusid is NULL;

UPDATE fact_productionoperation pop
SET amt_ExchangeRate = 1
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM AFVC_AFVV AA,
     dim_company dc, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND dc.CompanyCode = AA.AFVC_BUKRS
AND ifnull(amt_ExchangeRate,-1) <> 1;

UPDATE fact_productionoperation pop
SET amt_ExchangeRate = ex.exchangeRate
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM AFVC_AFVV AA,
     dim_company dc,
	 tmp_getExchangeRate1 ex, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND dc.CompanyCode = AA.AFVC_BUKRS
AND  pFromCurrency = AA.AFVC_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = CURRENT_DATE
and fact_script_name = 'bi_populate_productionorder_fact'
AND amt_ExchangeRate <> ex.exchangeRate ;


UPDATE fact_productionoperation pop
SET amt_ExchangeRate_GBL = 1
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM AFVC_AFVV AA,
     dim_company dc, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND dc.CompanyCode = AA.AFVC_BUKRS
AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;

UPDATE fact_productionoperation pop
SET amt_ExchangeRate_GBL = ex.exchangeRate
	,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM AFVC_AFVV AA,
     dim_company dc,
	 tmp_getExchangeRate1 ex,
	 pGlobalCurrency_po_77, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND dc.CompanyCode = AA.AFVC_BUKRS
AND  pFromCurrency = AA.AFVC_WAERS
and pToCurrency = pGlobalCurrency
and pFromExchangeRate = 0
and pDate =  CURRENT_DATE   /*current date to be used for global rate*/
and fact_script_name = 'bi_populate_productionorder_fact'
AND amt_ExchangeRate <> ex.exchangeRate ;
/* NN  Oct 14 2015  Updates dateid's to 1 for all 00010101 dates Start*/

update fact_productionoperation f
set f.Dim_DateIdActualStartExec = 1
from dim_date dd
, fact_productionoperation f
where f.Dim_DateIdActualStartExec = dd.dim_dateid
and dd.datevalue in ('0001-01-01','1900-01-01') and f.Dim_DateIdActualStartExec<>1;

update fact_productionoperation f
set f.Dim_DateIdActualFinishExec = 1
from dim_date dd
, fact_productionoperation f
where f.Dim_DateIdActualFinishExec = dd.dim_dateid
and dd.datevalue in ('0001-01-01','1900-01-01') and f.Dim_DateIdActualFinishExec<>1;

update fact_productionoperation f
set f.Dim_DateIdSchedStartExec = 1
from dim_date dd
, fact_productionoperation f
where f.Dim_DateIdSchedStartExec = dd.dim_dateid
and dd.datevalue in ('0001-01-01','1900-01-01') and f.Dim_DateIdSchedStartExec<>1;

update fact_productionoperation f
set f.Dim_DateIdSchedFinishExec = 1
from dim_date dd
, fact_productionoperation f
where f.Dim_DateIdSchedFinishExec = dd.dim_dateid
and dd.datevalue in ('0001-01-01','1900-01-01') and f.Dim_DateIdSchedFinishExec<>1;


/* Update 03 February 2016 OSTOIAN */

UPDATE fact_productionoperation pop
SET pop.dim_unitofmeasurestdvalue1id = uom.dim_unitofmeasureid
,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from AFVC_AFVV AA,dim_unitofmeasure uom, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND uom.uom = ifnull(AA.AFVV_VGE01,'Not Set')
AND pop.dim_unitofmeasurestdvalue1id <> uom.dim_unitofmeasureid;

UPDATE fact_productionoperation pop
SET pop.dim_unitofmeasurestdvalue2id = uom.dim_unitofmeasureid
,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from AFVC_AFVV AA,dim_unitofmeasure uom, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND uom.uom = ifnull(AA.AFVV_VGE02,'Not Set')
AND pop.dim_unitofmeasurestdvalue2id <> uom.dim_unitofmeasureid;

UPDATE fact_productionoperation pop
SET pop.dim_unitofmeasurestdvalue3id = uom.dim_unitofmeasureid
,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from AFVC_AFVV AA,dim_unitofmeasure uom, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND uom.uom = ifnull(AA.AFVV_VGE03,'Not Set')
AND pop.dim_unitofmeasurestdvalue3id <> uom.dim_unitofmeasureid;

UPDATE fact_productionoperation pop
SET pop.dim_unitofmeasurestdvalue4id = uom.dim_unitofmeasureid
,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from AFVC_AFVV AA,dim_unitofmeasure uom, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND uom.uom = ifnull(AA.AFVV_VGE04,'Not Set')
AND pop.dim_unitofmeasurestdvalue4id <> uom.dim_unitofmeasureid;

UPDATE fact_productionoperation pop
SET pop.dim_unitofmeasurestdvalue5id = uom.dim_unitofmeasureid
,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from AFVC_AFVV AA,dim_unitofmeasure uom, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND uom.uom = ifnull(AA.AFVV_VGE05,'Not Set')
AND pop.dim_unitofmeasurestdvalue5id <> uom.dim_unitofmeasureid;

UPDATE fact_productionoperation pop
SET pop.dim_unitofmeasurestdvalue6id = uom.dim_unitofmeasureid
,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from AFVC_AFVV AA,dim_unitofmeasure uom, fact_productionoperation pop
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND uom.uom = ifnull(AA.AFVV_VGE06,'Not Set')
AND pop.dim_unitofmeasurestdvalue6id <> uom.dim_unitofmeasureid;

 UPDATE fact_productionoperation pop
 SET ct_stdvalue1 = ifnull(AA.AFVV_VGW01,'Not Set')
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_stdvalue1 <> ifnull(AA.AFVV_VGW01,'Not Set');

   UPDATE fact_productionoperation pop
 SET ct_stdvalue2 = ifnull(AA.AFVV_VGW02,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_stdvalue2 <> ifnull(AA.AFVV_VGW02,0);

  UPDATE fact_productionoperation pop
 SET ct_stdvalue3 = ifnull(AA.AFVV_VGW03,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_stdvalue3 <> ifnull(AA.AFVV_VGW03,0);

   UPDATE fact_productionoperation pop
 SET ct_stdvalue4 = ifnull(AA.AFVV_VGW04,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_stdvalue4 <> ifnull(AA.AFVV_VGW04,0);

   UPDATE fact_productionoperation pop
 SET ct_stdvalue5 = ifnull(AA.AFVV_VGW05,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_stdvalue5 <> ifnull(AA.AFVV_VGW05,0);

   UPDATE fact_productionoperation pop
 SET ct_stdvalue6 = ifnull(AA.AFVV_VGW06,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_stdvalue6 <> ifnull(AA.AFVV_VGW06,0);


   UPDATE fact_productionoperation pop
 SET ct_totalscrapqty = ifnull(AA.AFVV_XMNGA,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_totalscrapqty <> ifnull(AA.AFVV_XMNGA,0);

  UPDATE fact_productionoperation pop
 SET ct_totalyieldconfirmed = ifnull(AA.AFVV_LMNGA,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_totalyieldconfirmed <> ifnull(AA.AFVV_LMNGA,0);

   UPDATE fact_productionoperation pop
 SET ct_previoslyconfirmedactivity1 = ifnull(AA.AFVV_ISM01,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_previoslyconfirmedactivity1 <> ifnull(AA.AFVV_ISM01,0);

   UPDATE fact_productionoperation pop
 SET ct_previoslyconfirmedactivity2 = ifnull(AA.AFVV_ISM02,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_previoslyconfirmedactivity2 <> ifnull(AA.AFVV_ISM02,0);

   UPDATE fact_productionoperation pop
 SET ct_previoslyconfirmedactivity3 = ifnull(AA.AFVV_ISM03,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_previoslyconfirmedactivity3 <> ifnull(AA.AFVV_ISM03,0);

   UPDATE fact_productionoperation pop
 SET pop.dim_uomtobeconfirmed1id = uom.dim_unitofmeasureid
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA,dim_unitofmeasure uom
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND uom.uom = ifnull(AA.AFVV_ILE01,'Not Set')
 AND pop.dim_uomtobeconfirmed1id <> uom.dim_unitofmeasureid;

   UPDATE fact_productionoperation pop
 SET pop.dim_uomtobeconfirmed2id = uom.dim_unitofmeasureid
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA,dim_unitofmeasure uom
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND uom.uom = ifnull(AA.AFVV_ILE02,'Not Set')
 AND pop.dim_uomtobeconfirmed2id <> uom.dim_unitofmeasureid;

   UPDATE fact_productionoperation pop
 SET pop.dim_uomtobeconfirmed3id = uom.dim_unitofmeasureid
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA,dim_unitofmeasure uom
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND uom.uom = ifnull(AA.AFVV_ILE03,'Not Set')
 AND pop.dim_uomtobeconfirmed3id <> uom.dim_unitofmeasureid;

   UPDATE fact_productionoperation pop
 SET dd_keyfortasklistgroup = ifnull(AA.AFVC_PLNNR,'Not Set')
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND dd_keyfortasklistgroup <> ifnull(AA.AFVC_PLNNR,'Not Set');

   UPDATE fact_productionoperation pop
 SET dd_activitytype1 = ifnull(AA.AFVC_LAR01,'Not Set')
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND dd_activitytype1 <> ifnull(AA.AFVC_LAR01,'Not Set');

   UPDATE fact_productionoperation pop
 SET dd_activitytype2 = ifnull(AA.AFVC_LAR02,'Not Set')
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND dd_activitytype2 <> ifnull(AA.AFVC_LAR02,'Not Set');

   UPDATE fact_productionoperation pop
 SET dd_activitytype3 = ifnull(AA.AFVC_LAR03,'Not Set')
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND dd_activitytype3 <> ifnull(AA.AFVC_LAR03,'Not Set');

  UPDATE fact_productionoperation pop
SET dim_DateIdLatestScheduleStartDate = sfedt.Dim_DateId
from AFVC_AFVV AA,dim_date sfedt, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND AA.AFVV_SSAVD = sfedt.datevalue
AND AA.AFVC_BUKRS = sfedt.companycode
AND pop.dim_DateIdLatestScheduleStartDate <> sfedt.Dim_DateId;

UPDATE fact_productionoperation pop
SET dim_DateIdLatestScheduleFinishDate = sfedt.Dim_DateId
from AFVC_AFVV AA,dim_date sfedt, fact_productionoperation pop
WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND AA.AFVV_SSEDD = sfedt.datevalue
AND AA.AFVC_BUKRS = sfedt.companycode
AND pop.dim_DateIdLatestScheduleFinishDate <> sfedt.Dim_DateId;

  UPDATE fact_productionoperation pop
 SET ct_operationquantity = ifnull(AA.AFVV_MGVRG,0)
 ,pop.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 from AFVC_AFVV AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
 AND ct_operationquantity <> ifnull(AA.AFVV_MGVRG,0);

/* End of changes 03 February 2016 OSTOIAN */


 /*
   UPDATE fact_productionoperation pop
 SET dd_postingdate = ifnull(AA.AFRU_BUDAT,'0001-01-01')
 ,pop.dw_update_date = current_timestamp Added automatically by update_dw_update_date.pl
 from AFRU AA
 , fact_productionoperation pop
 WHERE pop.dd_RoutingOperationNo = AA.AFRU_AUFPL
 AND pop.dd_GeneralOrderCounter = AA.AFRU_APLZL
 AND dd_postingdate <> ifnull(AA.AFRU_BUDAT,'0001-01-01')
*/

UPDATE fact_productionoperation f
SET dd_objectid = ifnull(a.afvc_arbid,'Not Set')
FROM afvc_afvv a, fact_productionoperation f
WHERE f.dd_RoutingOperationNo = a.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = a.AFVC_APLZL
AND dd_objectid <> ifnull(a.afvc_arbid,'Not Set');


MERGE INTO fact_productionoperation fact
USING (SELECT DISTINCT f.fact_productionoperationid, FIRST_VALUE(wc.dim_workcenterid) OVER (PARTITION BY wc.objectid,wc.objecttype ORDER BY wc.objectid DESC) AS dim_workcenterid
		FROM afvc_afvv a, dim_workcenter wc,fact_productionoperation f
		WHERE  f.dd_RoutingOperationNo = A.AFVC_AUFPL
	    AND f.dd_GeneralOrderCounter = A.AFVC_APLZL
		AND wc.objectid = ifnull(CONVERT(VARCHAR(8),f.dd_objectid),'Not Set')
		AND f.dim_workcenterid <> wc.dim_workcenterid) src
ON fact.fact_productionoperationid = src.fact_productionoperationid
WHEN MATCHED THEN UPDATE
SET fact.dim_workcenterid = src.dim_workcenterid,
dw_update_date = CURRENT_TIMESTAMP;

/*UPDATE fact_productionoperation f
SET f.dim_costcenterid = dc.dim_costcenterid
FROM dim_costcenter dc, CRCO c
, fact_productionoperation f
WHERE f.dd_objectid = c.crco_objid
AND c.crco_KOSTL = dc.code
AND f.dim_costcenterid <> dc.dim_costcenterid*/

DROP TABLE IF EXISTS tmp_dim_costcenter;
CREATE TABLE tmp_dim_costcenter
AS
SELECT DISTINCT dc.dim_costcenterid, c.crco_objid, row_number() over (partition by  c.crco_objid order by '') as rn
FROM  dim_costcenter dc, CRCO c, fact_productionoperation f, afvc_afvv a
WHERE f.dd_RoutingOperationNo = A.AFVC_AUFPL
	AND f.dd_GeneralOrderCounter = A.AFVC_APLZL
 AND f.dd_objectid = c.crco_objid
AND c.crco_KOSTL = dc.code
AND c.crco_objid = ifnull(CONVERT(VARCHAR(8),a.afvc_arbid),'Not Set')
AND f.dim_costcenterid <> dc.dim_costcenterid;

UPDATE fact_productionoperation f
SET f.dim_costcenterid = dc.dim_costcenterid
FROM tmp_dim_costcenter dc, afvc_afvv a
, fact_productionoperation f
WHERE f.dd_RoutingOperationNo = A.AFVC_AUFPL
	AND f.dd_GeneralOrderCounter = A.AFVC_APLZL
 AND f.dd_objectid = dc.crco_objid
AND f.dim_costcenterid <> dc.dim_costcenterid
and rn=1;
DROP TABLE IF EXISTS tmp_dim_costcenter;

/* Octavian : Every Angle */
UPDATE fact_productionoperation s
SET s.dim_vendorpurchasingid = vp.dim_vendorpurchasingid,
s.dw_update_date = CURRENT_TIMESTAMP
FROM AFVC_AFVV AA,dim_vendorpurchasing vp
, fact_productionoperation s
WHERE vp.purchasingorg = ifnull(AA.AFVC_EKORG,'Not Set')
AND vp.vendornumber = ifnull(AA.AFVC_LIFNR,'Not Set')
AND s.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND s.dd_GeneralOrderCounter = AA.AFVC_APLZL
AND s.dim_vendorpurchasingid <> vp.dim_vendorpurchasingid;
/* Octavian : Every Angle */

update fact_productionoperation
set dd_operationshorttext = 'Not Set'
where dd_operationshorttext is null;


update fact_productionoperation
set dd_descriptionline2 = 'Not Set'
where dd_descriptionline2 is null;

update fact_productionoperation
set dd_objectid = 'Not Set'
where dd_objectid is null;

/* Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */

UPDATE fact_productionoperation f
	SET f.std_exchangerate_dateid = dt.dim_dateid
FROM fact_productionoperation f
		 INNER JOIN dim_plant p ON f.dim_plantid = p.dim_plantid
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode
	WHERE dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

 /* END Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */

/*START - moved to vw_bi_populate_prodorder_fact.sql*/ 
/* Madalina 11 Jul 2016 - add new measures in fact_productionorder, based on AFVC_AFVV - BI-3412 */
/*
update fact_productionorder po

set po.ct_laborFixed = ifnull(AA.AFVV_ISM01,0),
	po.dw_update_date = current_timestamp
from AFVC_AFVV AA, fact_productionorder po
where po.dd_routingoperationno = ifnull(AFVC_AUFPL,'Not Set')
	and po.dd_firstoperationcounter = ifnull(AFVC_APLZL,0)
	and po.ct_laborFixed <> ifnull(AA.AFVV_ISM01,0)

update fact_productionorder po
set po.ct_laborVariable = ifnull(AA.AFVV_ISM03,0),
	po.dw_update_date = current_timestamp
from AFVC_AFVV AA, fact_productionorder po
where po.dd_routingoperationno = ifnull(AFVC_AUFPL,'Not Set')
	and po.dd_firstoperationcounter = ifnull(AFVC_APLZL,0)
	and po.ct_laborVariable <> ifnull(AA.AFVV_ISM03,0)

update fact_productionorder po

set po.ct_machineFixed = ifnull(AA.AFVV_ISM01,0),
	po.dw_update_date = current_timestamp
from AFVC_AFVV AA, fact_productionorder po
where po.dd_lastroutingoperationno = ifnull(AFVC_AUFPL,'Not Set')
	and po.dd_lasttoperationcounter = ifnull(AFVC_APLZL,0)
	and po.ct_machineFixed <> ifnull(AA.AFVV_ISM01,0)
	
update fact_productionorder po

set po.ct_machineVariable = ifnull(AA.AFVV_ISM02,0),
	po.dw_update_date = current_timestamp
from AFVC_AFVV AA, fact_productionorder po
where po.dd_lastroutingoperationno = ifnull(AFVC_AUFPL,'Not Set')
	and po.dd_lasttoperationcounter = ifnull(AFVC_APLZL,0)
	and po.ct_machineVariable <> ifnull(AA.AFVV_ISM02,0)
*/
/*END - moved to vw_bi_populate_prodorder_fact.sql*/ 