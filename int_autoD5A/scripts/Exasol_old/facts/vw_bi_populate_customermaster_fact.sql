
/* ***********************************************************************************************************************************
   Script         - vi_bw_populate_customermaster_fact.sql
   Author         - Cristina
   Created ON     - Jun 2015
   Description    - Populate fact_customermaster for base consolidation project

 ********************************************Change History**************************************************************************
  Date			    By			 Version		Desc

  28 Jun 2015		Cristina		1.0		Modify the script from Columbia project, keep only View1 which is using STD SAP tables
  02 Feb 2016       Cristina        1.1    New fields added as request of Wacom
  04 Mar 2016    Liviu I           1.2    Copy script to Merck
    29 Jun 2016	 Madalina			1.3		Replace Inner join with left join KNVP, to avoid loosing customer numbers  basecamp.com/2105228/projects/3367794/todos/259593833, BI-3175
 *************************************************************************************************************************************/

 /* Temp table used for INSERTS */
 DROP TABLE if exists tmp_fact_customermaster;
 CREATE TABLE tmp_fact_customermaster
 LIKE fact_customermaster INCLUDING DEFAULTS INCLUDING IDENTITY;


 /*  ------- VIEW 1 -------- */
/* KNA1, ADRC, KNVV, KNVP */
DROP TABLE IF EXISTS tmp_masterView_001_merck;
CREATE TABLE tmp_masterView_001_merck
AS
SELECT
	 kna.KNA1_KUNNR  /* KNA1 key for dim_customerid (Master Table)*/
	,kna.KNA1_ADRNR
	,adr.ADRC_ADDRNUMBER /* ADRC key for dim_address */
	,knv.KNVV_KUNNR
	,knv.KNVV_SPART /* KNVV keys for dim_customermastersalesid*/
	,knv.KNVV_VKORG
	,knv.KNVV_VTWEG
	/* ,knv.KNVV_J_3AGRSGY - ONLY AFS */
	,knv.KNVV_INCO1
	,knv.KNVV_VSBED
	,knv.KNVV_WAERS
	,kvp.KNVP_KUNNR	/* KNVP keys for dim_customerpartnerfunctionsid*/
	,kvp.KNVP_VKORG
	,kvp.KNVP_SPART
	,kvp.KNVP_VTWEG
	,kvp.KNVP_PARVW
	,kvp.KNVP_PARZA
FROM KNA1 kna
LEFT JOIN ADRC adr
	ON kna.KNA1_ADRNR = adr.ADRC_ADDRNUMBER
/*29 Jun 2016	 Madalina - Replace Inner join with left join KNVV, to avoid loosing customer numbers - basecamp.com/2105228/projects/3367794/todos/259593833, BI-3175 */
/*INNER JOIN KNVV knv */
LEFT JOIN KNVV knv
	ON kna.KNA1_KUNNR = knv.KNVV_KUNNR
/*29 Jun 2016	 Madalina - Replace Inner join with left join KNVP, to avoid loosing customer numbers - basecamp.com/2105228/projects/3367794/todos/259593833, BI-3175 */
/*INNER JOIN KNVP kvp */
LEFT JOIN KNVP kvp
	ON kna.KNA1_KUNNR = kvp.KNVP_KUNNR;


/* To generate surrogate keys*/
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_customermaster';

INSERT INTO NUMBER_FOUNTAIN
select 	'tmp_fact_customermaster',
	ifnull(max(f.fact_customermasterid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from tmp_fact_customermaster f;


/* Insert data FROM  KNA1, ADRC, KNVV, KNVP  table into fact_customermaster  -- View 1*/
INSERT INTO tmp_fact_customermaster(
	fact_customermasterid
	,dim_customerid
	,dim_addressid
	,dim_customermastersalesid
	,dim_customerpartnerfunctionsid
	,dim_incotermid
	,dim_shippingconditionid
	,dim_currencyid
	)
SELECT
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_customermaster') + row_number() over(ORDER BY '') as fact_customermasterid
	,ifnull(dim_customerid,1)
	,ifnull(dadr.dim_addressid, 1)
	,ifnull(dsal.dim_customermastersalesid ,1)
	,ifnull(dpn.dim_customerpartnerfunctionsid ,1)
	,ifnull(inco.dim_incotermid, 1) as dim_incotermid
	,ifnull(dim_ShippingConditionid, 1) as dim_ShippingConditionid
	,ifnull(cur.dim_currencyid ,1) AS dim_currencyid
FROM tmp_masterView_001_merck tmp
INNER JOIN dim_customer dcust
	ON ifnull(tmp.KNA1_KUNNR,'Not Set') = dcust.customernumber
LEFT OUTER JOIN dim_address dadr
	ON	ifnull(tmp.KNA1_ADRNR,'Not Set') = dadr.addressnumber
INNER JOIN dim_customermastersales dsal
	ON  ifnull(tmp.KNVV_KUNNR,'Not Set') = dsal.customernumber
	AND ifnull(tmp.KNVV_SPART,'Not Set') = dsal.divisioncode
	AND ifnull(tmp.KNVV_VKORG,'Not Set') = dsal.salesorg
	AND ifnull(tmp.KNVV_VTWEG,'Not Set') = dsal.distributionchannel
LEFT JOIN  dim_customerpartnerfunctions dpn
	ON ifnull(tmp.KNVP_KUNNR,'Not Set') = dpn.customernumber1
	AND ifnull(tmp.KNVP_VKORG,'Not Set')	= dpn.salesorgcode
	AND ifnull(tmp.KNVP_SPART,'Not Set') = dpn.divisioncode
	AND ifnull(tmp.KNVP_VTWEG,'Not Set') = dpn.distributionchannelcode
	AND ifnull(tmp.KNVP_PARVW,'Not Set') = dpn.partnerfunction
	AND convert(varchar(7),tmp.KNVP_PARZA) = convert(varchar(7),dpn.partnercounter)
LEFT JOIN dim_incoterm inco
	ON tmp.KNVV_INCO1 = inco.incotermcode
LEFT JOIN TVSBT t
	ON tmp.KNVV_VSBED = t.TVSBT_VSBED
LEFT JOIN dim_ShippingCondition dsh
	ON  t.TVSBT_VSBED = dsh.shippingconditioncode
LEFT JOIN dim_currency cur
	ON tmp.KNVV_WAERS = cur.currencycode
WHERE NOT EXISTS (SELECT 1
		FROM tmp_fact_customermaster f
		WHERE f.dim_customerid = dcust.dim_customerid
		AND f.dim_addressid = dadr.dim_addressid
		AND f.dim_customermastersalesid = dsal.dim_customermastersalesid
		AND f.dim_customerpartnerfunctionsid = dpn.dim_customerpartnerfunctionsid
	);

/* Freshly Insert Data from Temp Table to Fact Table*/
TRUNCATE TABLE fact_customermaster;

INSERT INTO fact_customermaster
select * from tmp_fact_customermaster;


DROP TABLE IF EXISTS tmp_masterView_001_merck;
DROP TABLE IF EXISTS tmp_fact_customermaster;

/*start ALIN BI-5447   07 feb 2017*/
drop table if exists tmp_salesrepres;
create table tmp_salesrepres  as
select distinct fact_customermasterID, max(dcm.dim_customerpartnerfunctionsid) as dim_customerpartnerfunctionsid
from fact_customermaster so, KNVP k, 
dim_customerpartnerfunctions dcm, dim_customer c
where 
ifnull(k.KNVP_KUNNR, 'Not Set') = DCM.customernumber1
AND ifnull(k.KNVP_VKORG, 'Not Set') = DCM.salesorgCODE
AND ifnull(k.KNVP_SPART, 'Not Set') = DCM.divisioncode
AND ifnull(k.KNVP_VTWEG, 'Not Set') = DCM.distributionchannelCODE
and ifnull(k.knvp_kunn2, 'Not Set') = dcm.CustomerNumberBusinessPartner
and ifnull(k.knVp_parvw, 'Not Set') = dcm.partnerfunction
AND k.knVp_parvw = 'ZR'
AND so.dim_customerid = c.dim_customerid
AND c.customernumber = ifnull(k.knvp_kunnr, 'Not Set')
group by fact_customermasterID;


update fact_customermaster so 
set so.dim_salesrepresentativeid = ifnull(t.dim_customerpartnerfunctionsid,1)
from tmp_salesrepres t, fact_customermaster so 
where so.fact_customermasterid = t.fact_customermasterid
and so.dim_salesrepresentativeid <> ifnull(t.dim_customerpartnerfunctionsid,1);

drop table if exists tmp_salesrepres;


drop table if exists tmp_salesrepca;
create table tmp_salesrepca  as
select distinct fact_customermasterID, max(dcm.dim_customerpartnerfunctionsid) as dim_customerpartnerfunctionsid
from fact_customermaster so, KNVP k, 
dim_customerpartnerfunctions dcm, dim_customer c
where 
ifnull(k.KNVP_KUNNR, 'Not Set') = DCM.customernumber1
AND ifnull(k.KNVP_VKORG, 'Not Set') = DCM.salesorgCODE
AND ifnull(k.KNVP_SPART, 'Not Set') = DCM.divisioncode
AND ifnull(k.KNVP_VTWEG, 'Not Set') = DCM.distributionchannelCODE
and ifnull(k.knvp_kunn2, 'Not Set') = dcm.CustomerNumberBusinessPartner
and ifnull(k.knVp_parvw, 'Not Set') = dcm.partnerfunction
AND k.knVp_parvw = 'ZZ'
AND so.dim_customerid = c.dim_customerid
AND c.customernumber = ifnull(k.knvp_kunnr, 'Not Set')
group by fact_customermasterID;


update fact_customermaster so 
set so.dim_Salesrepresentativeca= ifnull(t.dim_customerpartnerfunctionsid,1)
from tmp_salesrepca t, fact_customermaster so 
where so.fact_customermasterid = t.fact_customermasterid
and so.dim_Salesrepresentativeca <> ifnull(t.dim_customerpartnerfunctionsid,1);

drop table if exists salesrepca;

drop table if exists tmp_salesrepzy;
create table tmp_salesrepzy  as
select distinct fact_customermasterID, max(dcm.dim_customerpartnerfunctionsid) as dim_customerpartnerfunctionsid
from fact_customermaster so, KNVP k, 
dim_customerpartnerfunctions dcm, dim_customer c
where 
ifnull(k.KNVP_KUNNR, 'Not Set') = DCM.customernumber1
AND ifnull(k.KNVP_VKORG, 'Not Set') = DCM.salesorgCODE
AND ifnull(k.KNVP_SPART, 'Not Set') = DCM.divisioncode
AND ifnull(k.KNVP_VTWEG, 'Not Set') = DCM.distributionchannelCODE
and ifnull(k.knvp_kunn2, 'Not Set') = dcm.CustomerNumberBusinessPartner
and ifnull(k.knVp_parvw, 'Not Set') = dcm.partnerfunction
AND k.knVp_parvw = 'ZY'
AND so.dim_customerid = c.dim_customerid
AND c.customernumber = ifnull(k.knvp_kunnr, 'Not Set')
group by fact_customermasterID;


update fact_customermaster so 
set so.dim_Salesrepresentativezy= ifnull(t.dim_customerpartnerfunctionsid,1)
from tmp_salesrepzy t, fact_customermaster so 
where so.fact_customermasterid = t.fact_customermasterid
  and so.dim_salesrepresentativezy <> ifnull(t.dim_customerpartnerfunctionsid,1);

drop table if exists salesrepzy;

drop table if exists tmp_salesrepzx;
create table tmp_salesrepzx  as
select distinct fact_customermasterID, max(dcm.dim_customerpartnerfunctionsid) as dim_customerpartnerfunctionsid
from fact_customermaster so, KNVP k, 
dim_customerpartnerfunctions dcm, dim_customer c
where 
ifnull(k.KNVP_KUNNR, 'Not Set') = DCM.customernumber1
AND ifnull(k.KNVP_VKORG, 'Not Set') = DCM.salesorgCODE
AND ifnull(k.KNVP_SPART, 'Not Set') = DCM.divisioncode
AND ifnull(k.KNVP_VTWEG, 'Not Set') = DCM.distributionchannelCODE
and ifnull(k.knvp_kunn2, 'Not Set') = dcm.CustomerNumberBusinessPartner
and ifnull(k.knVp_parvw, 'Not Set') = dcm.partnerfunction
AND k.knVp_parvw = 'ZX'
AND so.dim_customerid = c.dim_customerid
AND c.customernumber = ifnull(k.knvp_kunnr, 'Not Set')
group by fact_customermasterID;


update fact_customermaster so 
set so.dim_SalesrepresentativeZX = ifnull(t.dim_customerpartnerfunctionsid,1)
from tmp_salesrepzx t, fact_customermaster so 
where so.fact_customermasterid = t.fact_customermasterid
  and so.dim_SalesrepresentativeZX <> ifnull(t.dim_customerpartnerfunctionsid,1);

drop table if exists salesrepzx;

drop table if exists tmp_salesrepzc;
create table tmp_salesrepzc  as
select distinct fact_customermasterID, max(dcm.dim_customerpartnerfunctionsid) as dim_customerpartnerfunctionsid
from fact_customermaster so, KNVP k, 
dim_customerpartnerfunctions dcm, dim_customer c
where 
ifnull(k.KNVP_KUNNR, 'Not Set') = DCM.customernumber1
AND ifnull(k.KNVP_VKORG, 'Not Set') = DCM.salesorgCODE
AND ifnull(k.KNVP_SPART, 'Not Set') = DCM.divisioncode
AND ifnull(k.KNVP_VTWEG, 'Not Set') = DCM.distributionchannelCODE
and ifnull(k.knvp_kunn2, 'Not Set') = dcm.CustomerNumberBusinessPartner
and ifnull(k.knVp_parvw, 'Not Set') = dcm.partnerfunction
AND k.knVp_parvw = 'ZC'
AND so.dim_customerid = c.dim_customerid
AND c.customernumber = ifnull(k.knvp_kunnr, 'Not Set')
group by fact_customermasterID;

update fact_customermaster so 
set so.dim_Salesrepresentativezc = ifnull(t.dim_customerpartnerfunctionsid,1)
from tmp_salesrepzc t, fact_customermaster so 
where so.fact_customermasterid = t.fact_customermasterid
  and so.dim_Salesrepresentativezc <> ifnull(t.dim_customerpartnerfunctionsid,1);

drop table if exists salesrepzc;


/*APP-8126 ALIN 28 Nov 2017*/
--DIM_SALESREPRESENTATIVECR
drop table if exists tmp_salesrepcr;
create table tmp_salesrepcr  as
select distinct fact_customermasterID, max(dcm.dim_customerpartnerfunctionsid) as dim_customerpartnerfunctionsid
from fact_customermaster so, KNVP k, 
dim_customerpartnerfunctions dcm, dim_customer c
where 
ifnull(k.KNVP_KUNNR, 'Not Set') = DCM.customernumber1
AND ifnull(k.KNVP_VKORG, 'Not Set') = DCM.salesorgCODE
AND ifnull(k.KNVP_SPART, 'Not Set') = DCM.divisioncode
AND ifnull(k.KNVP_VTWEG, 'Not Set') = DCM.distributionchannelCODE
and ifnull(k.knvp_kunn2, 'Not Set') = dcm.CustomerNumberBusinessPartner
and ifnull(k.knVp_parvw, 'Not Set') = dcm.partnerfunction
AND k.knVp_parvw = 'SP'
AND so.dim_customerid = c.dim_customerid
AND c.customernumber = ifnull(k.knvp_kunnr, 'Not Set')
group by fact_customermasterID;


update fact_customermaster so 
set so.DIM_SALESREPRESENTATIVECR = ifnull(t.dim_customerpartnerfunctionsid,1)
from tmp_salesrepcr t, fact_customermaster so 
where so.fact_customermasterid = t.fact_customermasterid
  and so.DIM_SALESREPRESENTATIVECR <> ifnull(t.dim_customerpartnerfunctionsid,1);

drop table if exists tmp_salesrepcr;

--DIM_SALESREPRESENTATIVEZ1
drop table if exists tmp_salesrepZ1;
create table tmp_salesrepZ1  as
select distinct fact_customermasterID, max(dcm.dim_customerpartnerfunctionsid) as dim_customerpartnerfunctionsid
from fact_customermaster so, KNVP k, 
dim_customerpartnerfunctions dcm, dim_customer c
where 
ifnull(k.KNVP_KUNNR, 'Not Set') = DCM.customernumber1
AND ifnull(k.KNVP_VKORG, 'Not Set') = DCM.salesorgCODE
AND ifnull(k.KNVP_SPART, 'Not Set') = DCM.divisioncode
AND ifnull(k.KNVP_VTWEG, 'Not Set') = DCM.distributionchannelCODE
and ifnull(k.knvp_kunn2, 'Not Set') = dcm.CustomerNumberBusinessPartner
and ifnull(k.knVp_parvw, 'Not Set') = dcm.partnerfunction
AND k.knVp_parvw = 'Z1'
AND so.dim_customerid = c.dim_customerid
AND c.customernumber = ifnull(k.knvp_kunnr, 'Not Set')
group by fact_customermasterID;


update fact_customermaster so 
set so.DIM_SALESREPRESENTATIVEZ1 = ifnull(t.dim_customerpartnerfunctionsid,1)
from tmp_salesrepZ1 t, fact_customermaster so 
where so.fact_customermasterid = t.fact_customermasterid
  and so.DIM_SALESREPRESENTATIVEZ1 <> ifnull(t.dim_customerpartnerfunctionsid,1);

drop table if exists tmp_salesrepZ1;

--DIM_SALESREPRESENTATIVEZ2
drop table if exists tmp_salesrepz2;
create table tmp_salesrepz2  as
select distinct fact_customermasterID, max(dcm.dim_customerpartnerfunctionsid) as dim_customerpartnerfunctionsid
from fact_customermaster so, KNVP k, 
dim_customerpartnerfunctions dcm, dim_customer c
where 
ifnull(k.KNVP_KUNNR, 'Not Set') = DCM.customernumber1
AND ifnull(k.KNVP_VKORG, 'Not Set') = DCM.salesorgCODE
AND ifnull(k.KNVP_SPART, 'Not Set') = DCM.divisioncode
AND ifnull(k.KNVP_VTWEG, 'Not Set') = DCM.distributionchannelCODE
and ifnull(k.knvp_kunn2, 'Not Set') = dcm.CustomerNumberBusinessPartner
and ifnull(k.knVp_parvw, 'Not Set') = dcm.partnerfunction
AND k.knVp_parvw = 'Z2'
AND so.dim_customerid = c.dim_customerid
AND c.customernumber = ifnull(k.knvp_kunnr, 'Not Set')
group by fact_customermasterID;


update fact_customermaster so 
set so.DIM_SALESREPRESENTATIVEZ2 = ifnull(t.dim_customerpartnerfunctionsid,1)
from tmp_salesrepz2 t, fact_customermaster so 
where so.fact_customermasterid = t.fact_customermasterid
  and so.DIM_SALESREPRESENTATIVEZ2 <> ifnull(t.dim_customerpartnerfunctionsid,1);

drop table if exists tmp_salesrepz2;