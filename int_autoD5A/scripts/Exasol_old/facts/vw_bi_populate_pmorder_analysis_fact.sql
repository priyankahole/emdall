delete from number_fountain m where m.table_name = 'fact_pmorder';

insert into number_fountain
select  'fact_pmorder',
        ifnull(max(d.fact_pmorderid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_pmorder d
where d.fact_pmorderid <> 1;


drop table if exists tmp_insert_into_fact_pmo;

create table tmp_insert_into_fact_pmo as
select 
	ifnull((select max(od.dim_ordermasterid) from dim_ordermaster od where od."order" = aufk_aufnr),1) as dim_ordermasterid   
from aufk
where AUFK_AUART IN('PM01', 'PM02', 'PM03', 'PM04', 'PM05', 'PM06', 'PM07', 'PM08', 'PM09');  


insert into fact_pmorder 
(
	fact_pmorderid, 
	dim_ordermasterid
)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_pmorder') + row_number() over(order by '') as fact_pmorderid,
		a.dim_ordermasterid	
from tmp_insert_into_fact_pmo a
where not exists (select 1 from fact_pmorder f where f.dim_ordermasterid = a.dim_ordermasterid);


/* Maint Order Plant */
update fact_pmorder f
set f.dim_PlantidOrder = ifnull(p.dim_plantid,1)
from dim_ordermaster o, dim_plant p, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and o.plant = p.plantcode
and f.dim_PlantidOrder <> ifnull(p.dim_plantid,1);

/* Maintainance Plan */
drop table if exists tmp_mhio_pmorder;

create table  tmp_mhio_pmorder as 
select distinct mhio_warpl, mhio_aufnr
from mhio m, dim_ordermaster o
where m.mhio_aufnr = o."order";

update fact_pmorder f
set f.dd_maintenancePlan = ifnull(t.mhio_warpl,'Not Set'),
	dw_update_date = current_timestamp
from dim_ordermaster o, tmp_mhio_pmorder t, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and o."order" = t.mhio_aufnr
and f.dd_maintenancePlan <> ifnull(t.mhio_warpl,'Not Set');


/* order revision desc*/
update fact_pmorder f
set f.dd_orderrevisiondesc = ifnull(T352R_REVTX, 'Not Set')
	,dw_update_date = current_timestamp
from  AUFK p, AFIH q, T352r r, dim_ordermaster d, fact_pmorder f	
where p.aufk_aufnr = q.afih_aufnr
and p.aufk_werks = r.t352r_iwerk
and q.afih_revnr = r.t352r_revnr
and f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = afih_aufnr
and AUFK_AUART IN('PM01', 'PM02', 'PM03', 'PM04', 'PM05', 'PM06', 'PM07', 'PM08', 'PM09')
and f.dd_orderrevisiondesc <> ifnull(T352R_REVTX, 'Not Set');


/* Maint Order Created Date */
update fact_pmorder f
set f.dim_dateIDOrderCreatedDt = ifnull(dt.dim_dateid,1)
        ,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt,  fact_pmorder f		
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d.Created_on = '1970-01-01' then '0001-01-01' else d.Created_on end
and dt.companycode = d.company_code
and dt.plantcode_factory = d.plant 
and f.dim_dateIDOrderCreatedDt <> dt.dim_dateid;

/* Equipment Master */
/*
update fact_pmorder f
set f.dim_equipmentid = ifnull(d.dim_equipmentid, 1)
	,dw_update_date = current_timestamp
from dim_equipment d, AFIH, dim_date b, dim_ordermaster o, fact_pmorder f	
where d.EquipmentNo = ifnull(AFIH_EQUNR,'Not Set')  
and o.dim_ordermasterid = f.dim_ordermasterid 
and o."order" = ifnull(AFIH_AUFNR, 'Not Set')
and f.dim_dateidordercreateddt = b.dim_dateid  
and b.datevalue between equipmentvalidfromdate and EquipmentValidtoDate 
and f.dim_equipmentid <> ifnull(d.dim_equipmentid,1)
*/

/*unable error temporary fix*/

merge into fact_pmorder f
using
(select distinct f.fact_pmorderid, max(d.dim_equipmentid) as dim_equipmentid
from dim_equipment d, AFIH, dim_date b, dim_ordermaster o, fact_pmorder f	
where d.EquipmentNo = ifnull(AFIH_EQUNR,'Not Set')  
and o.dim_ordermasterid = f.dim_ordermasterid 
and o."order" = ifnull(AFIH_AUFNR, 'Not Set')
and f.dim_dateidordercreateddt = b.dim_dateid  
and b.datevalue between equipmentvalidfromdate and EquipmentValidtoDate 
group by f.fact_pmorderid
)t
on t.fact_pmorderid = f.fact_pmorderid
when matched then update set
f.dim_equipmentid = t.dim_equipmentid
where f.dim_equipmentid <> t.dim_equipmentid;

/* Equipment Category */
update fact_pmorder f
set f.dim_equipmentcategoryid = ifnull(t.dim_equipmentcategoryid, 1),
	dw_update_date = current_timestamp
from dim_equipment d, dim_equipmentcategory t, fact_pmorder f	
where f.dim_equipmentid = d.dim_equipmentid
and d.EquipmentCategory = t.EquipmentCategory
and f.dim_equipmentcategoryid <> ifnull(t.dim_equipmentcategoryid, 1);

/* Equipment Start Date */
update fact_pmorder f
set f.dim_dateideqpstartdt = ifnull(ddt.dim_dateid, 1), 
	dw_update_date = current_timestamp
from dim_equipment d, dim_plant pl, dim_date ddt,  fact_pmorder f	
where f.dim_equipmentid = d.dim_equipmentid
and f.dim_plantidorder = pl.dim_plantid
and ddt.datevalue = ifnull(d.EquipmentStartUpDate, '0001-01-01')
and ddt.companycode = pl.companycode
and ddt.plantcode_factory = pl.plantcode 
and f.dim_dateideqpstartdt <>  ifnull(ddt.dim_dateid,1);

/* Equipment Valid-To Date */
update fact_pmorder f
set f.dim_dateideqpvalidtodt = ifnull(ddt.dim_dateid,1), 
	dw_update_date  = current_timestamp
from dim_equipment d, dim_plant pl, dim_date ddt, fact_pmorder f	
where  f.dim_equipmentid = d.dim_equipmentid
and f.dim_plantidorder = pl.dim_plantid
and ddt.datevalue = ifnull(d.EquipmentValidtoDate,'0001-01-01')
and ddt.companycode = pl.companycode
and ddt.plantcode_factory = pl.plantcode 
and f.dim_dateideqpvalidtodt <>  ifnull(ddt.dim_dateid,1);

/* Equipment Vendor  EQUI_ELIEF */
/*logic change acc to APP7223 Alin 21 May 2018*/
/*update fact_pmorder f
set f.dim_vendoridequipment = ifnull(v.dim_vendorid,1), 
	dw_update_date  = current_timestamp
from  dim_equipment d, dim_vendor v,fact_pmorder f	
where f.dim_equipmentid = d.dim_equipmentid
and d.EquipmentVendor = v.vendornumber
and f.dim_vendoridequipment <> ifnull(v.dim_vendorid,1)*/

update fact_pmorder f
set f.dim_vendoridequipment = ifnull(v.dim_vendorid,1), 
  dw_update_date  = current_timestamp
from IHPA, EQUI, LFA1, dim_equipment d, dim_vendor v, fact_pmorder f
where EQUI_OBJNR = IHPA_OBJNR
and IHPA_PARNR = LIFNR
and IHPA_PARVW = 'LF'
and d.EquipmentNo = ifnull(equi_equnr ,'Not Set')
and v.vendornumber = ifnull(LIFNR, 'Not Set')
and f.dim_equipmentid = d.dim_equipmentid
and f.dim_vendoridequipment <> ifnull(v.dim_vendorid,1);

/* Functional Location */
update fact_pmorder f 
set f.dim_functionallocationid = ifnull(d.dim_functionallocationid,1),
	dw_update_date = current_timestamp
from afih a, iloa i, dim_functionallocation d , dim_ordermaster o, fact_pmorder f 	
where f.dim_ordermasterid = o.dim_ordermasterid
and o."order" = ifnull(a.afih_aufnr, 'Not Set')
and ifnull(a.afih_iloan, 'Not Set') = ifnull(i.iloa_iloan, 'Not Set')     
and ifnull(i.iloa_iloan, 'Not Set') = ifnull(d.AcctAssignment, 'Not Set')
and ifnull(i.iloa_tplnr, 'Not Set') = ifnull(d.FunctonalLocation, 'Not Set')
and f.dim_functionallocationid <> ifnull(d.dim_functionallocationid,1);


/* FLOC ABC Indicator */
update fact_pmorder f
set f.dim_flocabcindicatorid = ifnull(t.dim_flocabcindicatorid,1),
	dw_update_date = current_timestamp
from dim_functionallocation d, dim_flocabcindicator t,  fact_pmorder f	
where f.dim_functionallocationid = d.dim_functionallocationid
and d.FLOCABCIndicator = t.FLOCABCIndicator
and f.dim_flocabcindicatorid <> ifnull(t.dim_flocabcindicatorid,1);

/* FLOC Auth group */
update fact_pmorder f
set f.dim_techobjectauthgroupidfloc = ifnull(t.dim_techobjectauthgroupid,1),
	dw_update_date = current_timestamp
from dim_functionallocation d, dim_techobjectauthgroup t,  fact_pmorder f	
where f.dim_functionallocationid = d.dim_functionallocationid
and d.TechObjectAuthGroup = t.TechObjectAuthGroup
and f.dim_techobjectauthgroupidfloc <> ifnull(t.dim_techobjectauthgroupid,1);

/* FLOC Cost Center ILOA_KOSTL  */
merge into fact_pmorder f using ( 
select fact_pmorderid,max(c.dim_costcenterid) dim_costcenterid
	from  dim_costcenter c, dim_functionallocation floc ,  fact_pmorder f	
	where f.dim_functionallocationid = floc.dim_functionallocationid
	and floc.FLOCCostCenter = c.code
	and f.dim_costcenteridfloc <> ifnull(c.dim_costcenterid,1)  
group by fact_pmorderid)d on d.fact_pmorderid = f.fact_pmorderid 
when matched then update
set f.dim_costcenteridfloc = ifnull(d.dim_costcenterid,1), 
	dw_update_date  = current_timestamp ; 

/* FLOC Plant Section */
update fact_pmorder f
set f.dim_plantsectionid = ifnull(p.dim_plantsectionid,1), 
	dw_update_date = current_timestamp
from dim_functionallocation floc, dim_plantsection p, fact_pmorder f	
where f.dim_functionallocationid = floc.dim_functionallocationid
and floc.plantsection = p.plantsection
and floc.FLOCPlanningPlant = p.Plant
and f.dim_plantsectionid <> ifnull(p.dim_plantsectionid,1);

/* Maint Order Change Date */
update fact_pmorder f
set f.dim_dateIDOrderchangeDt = ifnull(dt.dim_dateid,1)
	,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt,  fact_pmorder f	
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d.change_date = '1970-01-01' then '0001-01-01' else d.change_date end
and dt.companycode = d.company_code
and dt.plantcode_factory = d.plant 
and f.dim_dateIDOrderchangeDt <> dt.dim_dateid;

/* Maint Order Close Date */
update fact_pmorder f
set f.dim_dateIDOrdercloseDt = ifnull(dt.dim_dateid,1),
	dw_update_date = current_timestamp
from dim_ordermaster d, dim_Date dt,  fact_pmorder f	
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d."close" = '1970-01-01' then '0001-01-01' else d."close" end
and dt.companycode = d.company_code
and dt.plantcode_factory = d.plant 
and f.dim_dateIDOrdercloseDt <> dt.dim_dateid;

/* Maint Order Profit Center AUFK - PRCTR  */
update fact_pmorder f
set f.dim_profitcenterid = ifnull(p.dim_profitcenterid,1), 
	dw_update_date = current_timestamp
from dim_ordermaster o, dim_profitcenter p, fact_pmorder f	
where f.dim_ordermasterid = o.dim_ordermasterid
and o.profitcenter = p.profitcentercode
and f.dim_profitcenterid <> ifnull(p.dim_profitcenterid,1);

/* Maint Order Rel Date */
update fact_pmorder f
set f.dim_dateIDOrderReleasedDt = ifnull(dt.dim_dateid,1)
        ,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt,  fact_pmorder f		
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d.release_date = '1970-01-01' then '0001-01-01' else d.release_date end
and dt.companycode = d.company_code
and dt.plantcode_factory = d.plant 
and f.dim_dateIDOrderReleasedDt <> dt.dim_dateid;

/* Maint Order TECO Date */
update fact_pmorder f
set f.dim_dateidtechcompdt = ifnull(dt.dim_dateid,1)
	,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt, fact_pmorder f	
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d.ReferenceDate= '1970-01-01' then '0001-01-01' else d.ReferenceDate end
and dt.companycode = d.company_code
and dt.plantcode_factory = d.plant 
and f.dim_dateidtechcompdt <> dt.dim_dateid;


update fact_pmorder f
set f.dim_dateidorderteco = ifnull(dt.dim_dateid,1),dw_update_date = current_timestamp
from dim_ordermaster o, dim_Date dt,fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and dt.datevalue = case when Technical_completion = '1970-01-01' then '0001-01-01' else Technical_Completion end
and dt.companycode = company_code
and dt.plantcode_factory = o.plant /* BI-5389 */
and f.dim_dateidorderteco <> ifnull(dt.dim_dateid,1);

/*
update fact_pmorder f
set f.dim_dateIDCallObjCompletion = ifnull(dt.dim_dateid, 1), dw_update_date = current_timestamp
from dim_ordermaster o, mhio t, mhis, dim_date dt,  fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(t.mhio_aufnr,'Not Set') = o."order"
and t.mhio_warpl = f.dd_maintenancePlan
and t.mhio_warpl = mhis_warpl
and t.mhio_abnum = mhis_abnum
and dt.companycode = o.company_code
and dt.datevalue = ifnull(t.mhio_addat,'0001-01-01')
and dt.plantcode_factory = o.plant  BI-5389 */
/* and f.dim_dateIDCallObjCompletion <> ifnull(dt.dim_dateid, 1) */


merge into fact_pmorder f
using 
(
select f.dim_ordermasterid,f.dd_maintenancePlan,max(ifnull(dt.dim_dateid, 1)) as dim_dateIDCallObjCompletion
from dim_ordermaster o, mhio t, mhis, dim_date dt, fact_pmorder f 
where f.dim_ordermasterid = o.dim_ordermasterid and 
ifnull(t.mhio_aufnr,'Not Set') = o."order" and 
t.mhio_warpl = f.dd_maintenancePlan and 
t.mhio_warpl = mhis_warpl and 
t.mhio_abnum = mhis_abnum and 
dt.companycode = o.company_code and 
dt.datevalue = ifnull(t.mhio_addat,'0001-01-01') 
and dt.plantcode_factory = o.plant /* BI-5389 */ 
and f.dim_dateIDCallObjCompletion <> ifnull(dt.dim_dateid, 1)
group by f.dim_ordermasterid,f.dd_maintenancePlan
)x
on 
(
f.dim_ordermasterid = x.dim_ordermasterid and 
f.dd_maintenancePlan = x.dd_maintenancePlan
)
when matched then update set
f.dim_dateIDCallObjCompletion = x.dim_dateIDCallObjCompletion,
dw_update_date = current_timestamp;

update fact_pmorder f
set f.dim_dateIDTechCompDt = ifnull((case when f.dd_maintenancePlan = 'Not Set' 
										then dim_dateidorderteco 
										else dim_dateIDCallObjCompletion end),1)
,dw_update_date = current_timestamp
where f.dim_dateIDTechCompDt <> ifnull((case when f.dd_maintenancePlan = 'Not Set' 
											then dim_dateidorderteco 
											else dim_dateIDCallObjCompletion end),1);


/* Maint Order Type */
update fact_pmorder f
set f.dim_productionordertypeid = ifnull(dtp.dim_productionordertypeid,1)
	,dw_update_date = current_timestamp
from dim_ordermaster d, dim_productionordertype dtp,  fact_pmorder f
where f.dim_ordermasterid = d.dim_ordermasterid
and d.Order_Type = dtp.TypeCode /* T003P_AUART*/
and f.dim_productionordertypeid <> ifnull(dtp.dim_productionordertypeid,1);

/* Maint Plan Auth Group */
update fact_pmorder e
set dim_techobjectauthgrouporderid = ifnull(d.dim_techobjectauthgroupid,1) ,
    dw_update_date = current_timestamp
from afih a, 
     MPLA b, 
     dim_ordermaster o, 
     dim_techobjectauthgroup d,fact_pmorder e
where trim(leading '0' from o."order") = trim(leading '0' from afih_aufnr)
   and e.dim_ordermasterid = o.dim_ordermasterid
   and d.techobjectauthgroup = b.MPLA_BEGRU
   and a.AFIH_WARPL = MPLA_WARPL
   and dim_techobjectauthgrouporderid <>  ifnull(d.dim_techobjectauthgroupid,1);

/* Maintenance Activity Type  */
update fact_pmorder f
set f.dim_mainactivitytypeid = ifnull(m.dim_mainactivitytypeid, 1)
from  dim_ordermaster d, afih ,dim_mainactivitytype m, fact_pmorder f
where f.dim_ordermasterid = d.dim_ordermasterid
and trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and ifnull(afih_ilart,'Not Set') = m.mainactivitytype
and f.dim_mainactivitytypeid <> ifnull(m.dim_mainactivitytypeid,1);

/* Maintenance Plan Due Date MHIS_NPLDA  */
/* update fact_pmorder f
set f.dim_DateidMaintPlanDueDt = ifnull(dt.dim_Dateid,1)
	,dw_update_date = current_timestamp
from  dim_ordermaster d, afih , mhis, dim_date dt, fact_pmorder f
where f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = afih_aufnr
and AFIH_WARPL = MHIS_WARPL
and AFIH_ABNUM = MHIS_ABNUM 
and ifnull(MHIS_NPLDA,'0001-01-01') = dt.datevalue
and dt.companycode = d.company_code
and dt.plantcode_factory = d.plant 
and f.dim_DateidMaintPlanDueDt <> ifnull(dt.dim_Dateid,1) */

merge into fact_pmorder f
using 
(
select f.dim_ordermasterid,max(ifnull(dt.dim_dateid, 1)) as dim_DateidMaintPlanDueDt
from dim_ordermaster d, afih , mhis, dim_date dt, fact_pmorder f 
where f.dim_ordermasterid = d.dim_ordermasterid 
and d."order" = afih_aufnr 
and AFIH_WARPL = MHIS_WARPL 
and AFIH_ABNUM = MHIS_ABNUM 
and ifnull(MHIS_NPLDA,'0001-01-01') = dt.datevalue 
and dt.companycode = d.company_code 
and dt.plantcode_factory = d.plant
and f.dim_DateidMaintPlanDueDt <> ifnull(dt.dim_Dateid,1)
group by f.dim_ordermasterid
)x
on 
(
f.dim_ordermasterid = x.dim_ordermasterid
)
when matched then update set
f.dim_DateidMaintPlanDueDt = x.dim_DateidMaintPlanDueDt,
dw_update_date = current_timestamp;

/* Order Priority */
update fact_pmorder f
set f.dim_ordernotificationpriorityid = nt.dim_notificationpriorityid
from dim_notificationpriority nt, AFIH q, dim_ordermaster o, fact_pmorder f
where nt.notificationprioritytypecode = ifnull(q.AFIH_ARTPR,'Not Set')
and nt.notificationprioritycode = ifnull(q.AFIH_PRIOK,'Not Set')
and o.dim_ordermasterid = f.dim_ordermasterid
and q.afih_aufnr = o."order"
and f.dim_ordernotificationpriorityid <> nt.dim_notificationpriorityid;

/* planner group */
update fact_pmorder f
set f.dim_plannergroupid = ifnull(d.dim_plannergroupid,1)
	, dw_update_date = current_timestamp
from dim_plannergroup d, afih a ,dim_ordermaster o  ,  fact_pmorder f	
where f.dim_ordermasterid = o.dim_ordermasterid
and o."order" = ifnull(a.afih_aufnr, 'Not Set') 
and ifnull(a.afih_IWERK,'Not Set') = ifnull(d.MaintPlanningPlant,'Not Set')
and ifnull(a.afih_ingpr,'Not Set') = ifnull(d.CustomerServiceGroup,'Not Set')      
and f.dim_plannergroupid <> ifnull(d.dim_plannergroupid,1);

/* Responsible Cost Center */
merge into fact_pmorder f using (
	select fact_pmorderid, max(ifnull(csc.dim_costcenterid,1)) dim_costcenterid
	from  dim_ordermaster d, dim_costcenter csc,  fact_pmorder f		
	where f.dim_ordermasterid = d.dim_ordermasterid
		and d.Responsible_CCtr  = csc.code
		and f.dim_Costcenteridresponsible <> ifnull(csc.dim_costcenterid,1)
	group by fact_pmorderid)csc on csc.fact_pmorderid = f.fact_pmorderid
when matched then update
set f.dim_Costcenteridresponsible = ifnull(csc.dim_costcenterid,1)
        ,dw_update_date = current_timestamp ;

/*  System condition */
update fact_pmorder f
set f.dim_systemconditionid = ifnull(s.dim_systemconditionid,1)
from dim_ordermaster o, dim_systemcondition s, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and o.systemcondition = s.systemcondition
and f.dim_systemconditionid <> ifnull(s.dim_systemconditionid,1);

/* Tech Object Auth Group  */
update fact_pmorder f
set f.dim_techobjectauthgroupid = ifnull(t.dim_techobjectauthgroupid,1),
	dw_update_date = current_timestamp
from dim_equipment d, dim_techobjectauthgroup t,  fact_pmorder f	
where f.dim_equipmentid = d.dim_equipmentid
and d.TechObjectAuthGroup = t.TechObjectAuthGroup
and f.dim_techobjectauthgroupid <> ifnull(t.dim_techobjectauthgroupid,1);

/* Tech object type  */
update fact_pmorder f
set f.dim_techobjecttypeid = ifnull(t.dim_techobjecttypeid,1),
	dw_update_date = current_timestamp
from dim_equipment d, dim_techobjecttype t, fact_pmorder f	
where f.dim_equipmentid = d.dim_equipmentid
and d.TechnicalObjType = t.TechObjectType
and f.dim_techobjecttypeid <> ifnull(t.dim_techobjecttypeid,1);

/* Order User Status and Order System Status  */

DROP TABLE IF EXISTS tmp_dim_notificationuserstatus3;

CREATE TABLE  tmp_dim_notificationuserstatus3 AS 
SELECT distinct j1.JEST_OBJNR AS dd_ObjectNumber,
	/*t.TJ30T_TXT04 as dd_userstatus, 
	j1.JSTO_STSMA as dd_statusprofile,*/
	'Not Set' as WAITINGFORMATERIALS,
	'Not Set' as SHUTDOWNREQUIRED,
	'Not Set' as EHSRELEVANT,
	'Not Set' as NOTICEOFEVENTASSOCIATED,
	'Not Set' as CAPAASSOCIATED,
	'Not Set' as CALLIN,
	'Not Set' as CHANGECONTROL,
	'Not Set' as REFRIGERANT,
	'Not Set' as EMERGENCYWORK,
	'Not Set' as SAFETYWORK,
	'Not Set' as COMPLIANCEWORKREQUEST,
	'Not Set' as DELIVERYWORKREQUEST,
	'Not Set' as WORKREQUESTGENERATEDFROMPM,
	'Not Set' as EXECUTIONWOCOMPLETED,
	'Not Set' as Discontinued
FROM JEST_AUFK j1; 
	

UPDATE   tmp_dim_notificationuserstatus3 t
 SET WaitingForMaterials = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t 
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'WMTL' 
AND j1.JEST_STAT = 'E0010'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 

UPDATE   tmp_dim_notificationuserstatus3 t
SET ShutdownRequired = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'SDRQ' 
AND j1.JEST_STAT = 'E0011'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET EHSRelevant = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'EHS' 
AND j1.JEST_STAT = 'E0012'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET NoticeofEventAssociated = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'NOE' 
AND j1.JEST_STAT = 'E0013'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET CAPAassociated = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'CAPA' 
AND j1.JEST_STAT = 'E0014'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 

UPDATE   tmp_dim_notificationuserstatus3 t
SET CallIn = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'CALL' 
AND j1.JEST_STAT = 'E0015'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET ChangeControl = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'CHGC' 
AND j1.JEST_STAT = 'E0016'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET Refrigerant = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'REFG' 
AND j1.JEST_STAT = 'E0017'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 

UPDATE   tmp_dim_notificationuserstatus3 t
SET EmergencyWork = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'EMER' 
AND j1.JEST_STAT = 'E0018'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 

UPDATE   tmp_dim_notificationuserstatus3 t
SET SafetyWork = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'SFTY' 
AND j1.JEST_STAT = 'E0019'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET ComplianceWorkRequest = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'CMPL' 
AND j1.JEST_STAT = 'E0020'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET DeliveryWorkRequest = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'DLVY' 
AND j1.JEST_STAT = 'E0021'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET WorkRequestGeneratedfromPM = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'WRPM' 
AND j1.JEST_STAT = 'E0022'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET EXECUTIONWOCOMPLETED = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'EXCO' 
AND j1.JEST_STAT = 'E0034'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 


UPDATE   tmp_dim_notificationuserstatus3 t
SET Discontinued = 'X'
FROM JEST_AUFK j1, JSTO_AUFK j, TJ30T_2 t3,  tmp_dim_notificationuserstatus3 t
WHERE j.JSTO_OBJNR = dd_ObjectNumber
AND j1.JEST_OBJNR = j.JSTO_OBJNR
AND t3.TJ30T_TXT04 = 'DISC' 
AND j1.JEST_STAT = 'E0023'
AND j.JSTO_STSMA = t3.TJ30T_STSMA
AND j.JSTO_STSMA like 'ZPM_ORD%'; 





insert into dim_OrderuserstatusPMNotif
	(
	dim_OrderuserstatusPMNotifid,
    WAITINGFORMATERIALS,
	SHUTDOWNREQUIRED,
	EHSRELEVANT,
	NOTICEOFEVENTASSOCIATED,
	CAPAASSOCIATED,
	CALLIN,
	CHANGECONTROL,
	REFRIGERANT,
	EMERGENCYWORK,
	SAFETYWORK,
	COMPLIANCEWORKREQUEST,
	DELIVERYWORKREQUEST,
	WORKREQUESTGENERATEDFROMPM
	)
select 1, 'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set', 'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set'
from ( SELECT 1 )  AS t
where not exists ( select 1 from dim_OrderuserstatusPMNotif where dim_OrderuserstatusPMNotifid = 1);

delete from number_fountain m where m.table_name = 'dim_OrderuserstatusPMNotif';

insert into number_fountain
select  'dim_OrderuserstatusPMNotif',
        ifnull(max(d.dim_OrderuserstatusPMNotifid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_OrderuserstatusPMNotif d
where d.dim_OrderuserstatusPMNotifid <> 1;

insert into dim_OrderuserstatusPMNotif
(	
	dim_OrderuserstatusPMNotifid,
	WAITINGFORMATERIALS,
	SHUTDOWNREQUIRED,
	EHSRELEVANT,
	NOTICEOFEVENTASSOCIATED,
	CAPAASSOCIATED,
	CALLIN,
	CHANGECONTROL,
	REFRIGERANT,
	EMERGENCYWORK,
	SAFETYWORK,
	COMPLIANCEWORKREQUEST,
	DELIVERYWORKREQUEST,
	WORKREQUESTGENERATEDFROMPM,
	EXECUTIONWOCOMPLETED,
	Discontinued
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_OrderuserstatusPMNotif') + row_number() over(order by '') as dim_OrderuserstatusPMNotifid, 
	t.* from (select distinct 
		WAITINGFORMATERIALS,
			SHUTDOWNREQUIRED,
			EHSRELEVANT,
			NOTICEOFEVENTASSOCIATED,
			CAPAASSOCIATED,
			CALLIN,
			CHANGECONTROL,
			REFRIGERANT,
			EMERGENCYWORK,
			SAFETYWORK,
			COMPLIANCEWORKREQUEST,
			DELIVERYWORKREQUEST,
			WORKREQUESTGENERATEDFROMPM,
			EXECUTIONWOCOMPLETED,
			Discontinued
		from tmp_dim_notificationuserstatus3 t3
		where not exists (select 1 from dim_OrderuserstatusPMNotif pm
		where
		pm.WAITINGFORMATERIALS 	= t3.WAITINGFORMATERIALS and
		pm.SHUTDOWNREQUIRED = t3.SHUTDOWNREQUIRED and
		pm.EHSRELEVANT = t3.EHSRELEVANT and
		pm.NOTICEOFEVENTASSOCIATED = t3.NOTICEOFEVENTASSOCIATED and
		pm.CAPAASSOCIATED = t3.CAPAASSOCIATED and
		pm.CALLIN = t3.CALLIN and
		pm.CHANGECONTROL = t3.CHANGECONTROL and
		pm.REFRIGERANT = t3.REFRIGERANT and
		pm.EMERGENCYWORK = t3.EMERGENCYWORK and
		pm.SAFETYWORK = t3.SAFETYWORK and
		pm.COMPLIANCEWORKREQUEST = t3.COMPLIANCEWORKREQUEST and
		pm.DELIVERYWORKREQUEST = t3.DELIVERYWORKREQUEST and
		pm.WORKREQUESTGENERATEDFROMPM = t3.WORKREQUESTGENERATEDFROMPM and
		pm.EXECUTIONWOCOMPLETED = t3.EXECUTIONWOCOMPLETED and
		pm.Discontinued = t3.Discontinued
		/*and pm.userstatus=dd_userstatus  */
		) )
		 t;
 
	   
UPDATE fact_pmorder fpm
SET fpm.dim_notifuserstatusid_2 = pm.dim_OrderuserstatusPMNotifid
	,dw_update_date = current_timestamp 	
FROM
	dim_OrderuserstatusPMNotif pm,
	AUFK p, AFIH q, dim_ordermaster d,
	tmp_dim_notificationuserstatus3 t3 , fact_pmorder fpm	
WHERE  p.AUFK_AUFNR = q.AFIH_AUFNR
	and fpm.dim_ordermasterid = d.dim_ordermasterid
	and d."order" = AFIH_AUFNR
	and t3.dd_ObjectNumber=p.AUFK_OBJNR and
	pm.WAITINGFORMATERIALS 	= t3.WAITINGFORMATERIALS and
	pm.SHUTDOWNREQUIRED = t3.SHUTDOWNREQUIRED and
	pm.EHSRELEVANT = t3.EHSRELEVANT and
	pm.NOTICEOFEVENTASSOCIATED = t3.NOTICEOFEVENTASSOCIATED and
	pm.CAPAASSOCIATED = t3.CAPAASSOCIATED and
	pm.CALLIN = t3.CALLIN and
	pm.CHANGECONTROL = t3.CHANGECONTROL and
	pm.REFRIGERANT = t3.REFRIGERANT and
	pm.EMERGENCYWORK = t3.EMERGENCYWORK and
	pm.SAFETYWORK = t3.SAFETYWORK and
	pm.COMPLIANCEWORKREQUEST = t3.COMPLIANCEWORKREQUEST and
	pm.DELIVERYWORKREQUEST = t3.DELIVERYWORKREQUEST and
	pm.WORKREQUESTGENERATEDFROMPM = t3.WORKREQUESTGENERATEDFROMPM and 
	pm.EXECUTIONWOCOMPLETED = t3.EXECUTIONWOCOMPLETED and
	pm.Discontinued = t3.Discontinued;
	/*and fpm.dim_notifuserstatusid_2 <> pm.dim_OrderuserstatusPMNotifid*/

/* Order System Status dimension - similar to Order Status from Production Order, but starting with JEST_AUFK instead of fact_productionorder po,AFKO_AFPO_AUFK mhi*/
/* Correction: Instead of using dim_productionorderstatus from Production Order, create a new dimension for Order System Status, containing only the requested attributes */
Drop table if exists subqry_po_002;

Create table subqry_po_002 as 
Select distinct j1.JEST_OBJNR AS dd_ObjectNumber,
	convert(varchar(20),'Not Set') Closed,
	convert(varchar(20),'Not Set') GoodsMovementPosted,
	convert(varchar(20),'Not Set') TechnicallyCompleted,
	convert(varchar(20),'Not Set') Created,
	convert(varchar(20),'Not Set') Released,
	convert(varchar(20),'Not Set') PartiallyConfirmed,
	convert(varchar(20),'Not Set') confirmed,

	convert(varchar(20),'Not Set') MaterialCommitted,
	convert(varchar(20),'Not Set') PreCosted, 
	convert(varchar(20),'Not Set') MaterialShortage, 
	convert(varchar(20),'Not Set') MaterialAvailabilityNotChecked, 
	convert(varchar(20),'Not Set') SettlementRuleCreated, 

	convert(varchar(20),'Not Set') nomaterialcomponents, 
	convert(varchar(20),'Not Set') datesarenotupdated, 
	convert(varchar(20),'Not Set') notcompleted 


	/* varchar('Not Set',20) deletionflag,
	convert(varchar(20),'Not Set') PartPrinted,
	varchar('Not Set',20) deletionindicator,	 
	varchar('Not Set', 20) Delivered, 
	convert(varchar(20),'Not Set') Printed
	varchar('Not Set',20) InspectionLotAssigned,
	
	varchar('Not Set',20) controlrecipefinished,
	varchar('Not Set',20) controlrecipediscarded, 
	
	varchar('Not Set',20) ResultsAnalysisCarriedOut,
	varchar('Not Set', 20) VariancesCalculated,*/
FROM JEST_AUFK j1;

Update subqry_po_002 k
Set k.Closed = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0046' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.GoodsMovementPosted = 'X'
From JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0321' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.TechnicallyCompleted = 'X'
From JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0045' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.Created = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0001' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.Released = 'X'
From JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0002' AND j.JEST_INACT is NULL;


Update subqry_po_002 k
Set k.PartiallyConfirmed = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0010' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.confirmed = 'X'
From JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0009' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.MaterialCommitted = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0340' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.PreCosted = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0016' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.MaterialShortage = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0004' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.MaterialAvailabilityNotChecked = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0420' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.SettlementRuleCreated = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0028' AND j.JEST_INACT is NULL;


Update subqry_po_002 k
Set k.nomaterialcomponents = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0485' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.datesarenotupdated = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0118' AND j.JEST_INACT is NULL;

Update subqry_po_002 k
Set k.notcompleted = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0015' AND j.JEST_INACT is NULL;

/*
Update subqry_po_002 k
Set k.Printed = 'X'
From JEST_AUFK j,  subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0007' AND j.JEST_INACT is NULL


 Update subqry_po_002 k
From JEST_AUFK j
Set k.Delivered = 'X'
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0012' AND j.JEST_INACT is NULL

Update subqry_po_002 k
From JEST_AUFK j
Set k.deletionflag = 'X'
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0076' AND j.JEST_INACT is NULL

Update subqry_po_002 k
From JEST_AUFK j
Set k.deletionindicator = 'X'
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0013' AND j.JEST_INACT is NULL

Update subqry_po_002 k
From JEST_AUFK j
Set k.controlrecipefinished = 'X'
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0267' AND j.JEST_INACT is NULL

Update subqry_po_002 k
From JEST_AUFK j
Set k.controlrecipediscarded = 'X'
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0181' AND j.JEST_INACT is NULL

Update subqry_po_002 k
From JEST_AUFK j
Set k.ResultsAnalysisCarriedOut = 'X'
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0082' AND j.JEST_INACT is NULL

Update subqry_po_002 k
From JEST_AUFK j
Set k.InspectionLotAssigned = 'X'
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0281' AND j.JEST_INACT is NULL

Update subqry_po_002 k
Set k.PartPrinted = 'X'
From JEST_AUFK j, subqry_po_002 k
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0008' AND j.JEST_INACT is NULL


Update subqry_po_002 k
From JEST_AUFK j
Set k.VariancesCalculated = 'X'
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0056' AND j.JEST_INACT is NULL

*/


/*Add to dimension the missing combinations only, not all of the possible combinations of X and Not Set */
delete from number_fountain m where m.table_name = 'dim_OrderSystemStatus';

insert into number_fountain
select  'dim_OrderSystemStatus',
        ifnull(max(d.dim_OrderSystemStatusid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_OrderSystemStatus d
where d.dim_OrderSystemStatusid <> 1;

insert into dim_OrderSystemStatus
(
	dim_OrderSystemStatusid,
	Closed,
	GoodsMovementPosted,
	TechnicallyCompleted,
	Created,
	Released,
	PartiallyConfirmed,
	confirmed,
	MaterialCommitted,
	PreCosted, 
	MaterialShortage, 
	MaterialAvailabilityNotChecked, 
	SettlementRuleCreated, 
	nomaterialcomponents, 
	datesarenotupdated,
	notcompleted 
)
select
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_OrderSystemStatus') + row_number() over(order by '') as dim_OrderSystemStatusid, 
	t.* 
	from 
		( select distinct
					Closed,
					GoodsMovementPosted,
					TechnicallyCompleted,
					Created,
					Released,
					PartiallyConfirmed,
					confirmed,
					MaterialCommitted,
					PreCosted, 
					MaterialShortage, 
					MaterialAvailabilityNotChecked, 
					SettlementRuleCreated, 
					nomaterialcomponents, 
					datesarenotupdated,
					notcompleted 
			from subqry_po_002 sq
			where not exists	
				( select 1 from dim_OrderSystemStatus post
				  where  

				  	post.Closed	= SQ.Closed and
					post.GoodsMovementPosted = SQ.GoodsMovementPosted and
					post.TechnicallyCompleted = SQ.TechnicallyCompleted and
					post.Created= SQ.Created and
					post.Released = SQ.Released and
					post.PartiallyConfirmed = SQ.PartiallyConfirmed and
					post.confirmed = SQ.confirmed and
					post.MaterialCommitted = SQ.MaterialCommitted and
					post.PreCosted = SQ.PreCosted and
					post.MaterialShortage = SQ.MaterialShortage and
					post.MaterialAvailabilityNotChecked = SQ.MaterialAvailabilityNotChecked and
					post.SettlementRuleCreated = SQ.SettlementRuleCreated and
					post.nomaterialcomponents = SQ.nomaterialcomponents and
					post.datesarenotupdated = SQ.datesarenotupdated and
					post.notcompleted = SQ.notcompleted) 
		) t;
	
UPDATE fact_pmorder pm
SET pm.dim_OrderSystemStatusid = post.dim_OrderSystemStatusid
FROM dim_OrderSystemStatus post,
     AUFK p, AFIH q, dim_ordermaster d,
     subqry_po_002 sq, fact_pmorder pm
WHERE p.AUFK_AUFNR=q.AFIH_AUFNR
and pm.dim_ordermasterid = d.dim_ordermasterid
and d."order" = AFIH_AUFNR
and sq.dd_ObjectNumber = ifnull(AUFK_OBJNR, 'Not Set') and
post.Closed	= SQ.Closed and
post.GoodsMovementPosted = SQ.GoodsMovementPosted and
post.TechnicallyCompleted = SQ.TechnicallyCompleted and
post.Created= SQ.Created and
post.Released = SQ.Released and
post.PartiallyConfirmed = SQ.PartiallyConfirmed and
post.confirmed = SQ.confirmed and
post.MaterialCommitted = SQ.MaterialCommitted and
post.PreCosted = SQ.PreCosted and
post.MaterialShortage = SQ.MaterialShortage and
post.MaterialAvailabilityNotChecked = SQ.MaterialAvailabilityNotChecked and
post.SettlementRuleCreated = SQ.SettlementRuleCreated and
post.nomaterialcomponents = SQ.nomaterialcomponents and
post.datesarenotupdated = SQ.datesarenotupdated and
post.notcompleted = SQ.notcompleted;
	   /*AND pm.dim_OrderSystemStatusid <> post.dim_OrderSystemStatusid*/
	   
   
/* Planned Hours */
merge into fact_pmorder f
using
	(SELECT distinct o.dim_ordermasterid, sum(a.AFVV_ARBEI) as plannedhrs
	FROM AUFK_AFKO_AFVV a, AFKO_AUFK aa, dim_ordermaster o
	where ifnull(a.AUFK_AUFNR, 'Not Set') = o."order"
	and cast(ifnull(a.AFVV_AUFPL, 0) as integer) = ifnull(aa.AFKO_AUFPL, 0)
	and ifnull(a.AUFK_AUFNR, 'Not Set') = ifnull(aa.AFKO_AUFNR, 'Not Set')
	and aa.AUFK_AUART IN('PM01', 'PM02', 'PM03', 'PM04', 'PM05', 'PM06', 'PM07')
	group by o.dim_ordermasterid) sc
on f.dim_ordermasterid = sc.dim_ordermasterid
when matched then update 
set f.ct_plannedhours = sc.plannedhrs;



/* Actual Hours */
merge into fact_pmorder f
using 
	(
select t.dim_ordermasterid, ifnull(sum(t.actualhrs), 0) as actualhrs
from
(SELECT distinct o.dim_ordermasterid, 
case when stg.AFRU_ISMNE = 'STD' then sum(stg.AFRU_ISMNW) 
when  stg.AFRU_ISMNE = 'MIN' then sum(stg.AFRU_ISMNW/60) END
as actualhrs
	FROM dim_ordermaster o, stg_afru stg
	where ifnull(stg.AFRU_AUFNR, 'Not Set') = o."order"
	group by o.dim_ordermasterid, AFRU_ISMNE)t
group by t.dim_ordermasterid
) sc
on f.dim_ordermasterid = sc.dim_ordermasterid
when matched then update 
set f.ct_actualhours = sc.actualhrs;

/* Routing number of operations in the order AFKO_AUFPL */
update fact_pmorder f
set f.dd_routingnooforderoperations = ifnull(aa.AFKO_AUFPL, 0)
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and f.dd_routingnooforderoperations <> ifnull(aa.AFKO_AUFPL, 0);


merge into dim_ordermaster d
using (select distinct AUFK_AUFNR,first_value(AFVC_APLZL) over (partition by AUFK_AUFNR order by AUFK_AUFNR desc) as AFVC_APLZL FROM AUFK_AFKO_AFVC)
 a
on d."order" = AUFK_AUFNR
when matched then update set
d.GENCOUNTERFORORDER = AFVC_APLZL;

/* Completion Confirmation Number AFVC_RUECK*/

update fact_pmorder f
set dd_completionconfirmationno = ifnull(aaa.AFVC_RUECK, 0) 
from AUFK_AFKO_AFVC aaa, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aaa.AUFK_AUFNR, 'Not Set') = o."order"
and ifnull(AFVC_APLZL, 'Not Set') =  o.GENCOUNTERFORORDER
and dd_completionconfirmationno <> ifnull(aaa.AFVC_RUECK, 0);

merge into fact_pmorder f
using(
select distinct fact_pmorderid, 
first_value(ifnull(stg.AFRU_VORNR, 'Not Set')) over (partition by fact_pmorderid) as vornr
FROM dim_ordermaster o, stg_afru stg, --dim_orderconfirmation ocnf, 
fact_pmorder f
	where ifnull(stg.AFRU_AUFNR, 'Not Set') = o."order"
and dd_completionconfirmationno = ifnull(AFRU_RUECK, 'Not Set')
and f.dim_ordermasterid = o.dim_ordermasterid) t
on t.fact_pmorderid = f.fact_pmorderid
when matched then update set
f.dd_opac_number = t.vornr;


/* PMCO fields */

/* Planned Labor Costs */
drop table if exists tmp_pmco_010_1;

create table tmp_pmco_010_1
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where pmco_acpos = 010       
	and pmco_wrttp = 01  
	and pmco_versn = '000'
	and pmco_cocur = 'EUR'    
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_010_1;

create table tmp_010_1
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_010_1
order by pmco_objnr;

update fact_pmorder
set amt_plannedlabor = 0;

update fact_pmorder f
set amt_plannedlabor = t.costs
from tmp_010_1 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";

/* Planned Storeroom Material Costs */
drop table if exists tmp_pmco_020_1;

create table tmp_pmco_020_1
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where   pmco_acpos = 020       
	and pmco_wrttp = 01  
	and pmco_versn = '000'   
	and pmco_cocur = 'EUR'    
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_020_1;

create table tmp_020_1
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_020_1
order by pmco_objnr;

update fact_pmorder
set amt_plannedstoreroommaterial = 0;

update fact_pmorder f
set amt_plannedstoreroommaterial = t.costs
from tmp_020_1 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";

/* Planned Purchased Material Costs */
drop table if exists tmp_pmco_030_1;

create table tmp_pmco_030_1
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where   pmco_acpos = 030       
	and pmco_wrttp = 01 
	and pmco_versn = '000'      
	and pmco_cocur = 'EUR' 
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_030_1;

create table tmp_030_1
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_030_1
order by pmco_objnr;

update fact_pmorder
set amt_plannedpurchasedmaterial = 0;

update fact_pmorder f
set amt_plannedpurchasedmaterial = t.costs
from tmp_030_1 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";


/* Planned Outside Services Costs */
drop table if exists tmp_pmco_040_1;

create table tmp_pmco_040_1
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where pmco_acpos = 040       
and pmco_wrttp = 01   
and pmco_versn = '000'    
and pmco_cocur = 'EUR' 
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_040_1;

create table tmp_040_1
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_040_1
order by pmco_objnr;

update fact_pmorder f
set amt_plannedoutsideservices = 0;

update fact_pmorder f
set amt_plannedoutsideservices = t.costs
from tmp_040_1 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";

/* Planned Miscellaneous Costs */
drop table if exists tmp_pmco_050_1;

create table tmp_pmco_050_1
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where pmco_cocur = 'USD'
	and pmco_acpos = 050       
	and pmco_wrttp = 01      
	and pmco_cocur = 'EUR'  
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_050_1;

create table tmp_050_1
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_050_1
order by pmco_objnr;

update fact_pmorder f
set amt_plannedmiscellaneous = 0;

update fact_pmorder f
set amt_plannedmiscellaneous = t.costs
from tmp_050_1 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";

/* Actual Labor Costs */
drop table if exists tmp_pmco_010_4;

create table tmp_pmco_010_4
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where   pmco_acpos = 010       
	and pmco_wrttp = 04       
	and pmco_cocur = 'EUR' 
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_010_4;

create table tmp_010_4
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_010_4
order by pmco_objnr;

update fact_pmorder f
set amt_actuallabor = 0;

update fact_pmorder f
set amt_actuallabor = t.costs
from tmp_010_4 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";


/* Actual Storeroom Material Costs */
drop table if exists tmp_pmco_020_4;

create table tmp_pmco_020_4
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where pmco_cocur = 'EUR'
	and pmco_acpos = 020       
	and pmco_wrttp = 04       
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_020_4;

create table tmp_020_4
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_020_4
order by pmco_objnr;

update fact_pmorder f
set amt_actualstoreroommaterial = 0;

update fact_pmorder f
set amt_actualstoreroommaterial = t.costs
from tmp_020_4 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";


/* Actual Purchased Material Costs */
drop table if exists tmp_pmco_030_4;

create table tmp_pmco_030_4
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where pmco_cocur = 'EUR'
	and pmco_acpos = 030       
	and pmco_wrttp = 04       
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_030_4;

create table tmp_030_4
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_030_4
order by pmco_objnr;

update fact_pmorder f
set amt_actualpurchasedmaterial = 0;

update fact_pmorder f
set amt_actualpurchasedmaterial = t.costs
from tmp_030_4 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";


/* Actual Outside Services Costs */
drop table if exists tmp_pmco_040_4;

create table tmp_pmco_040_4
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where   pmco_cocur = 'EUR'
	and pmco_acpos = 040       
	and pmco_wrttp = 04       
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_040_4;

create table tmp_040_4
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_040_4
order by pmco_objnr;

update fact_pmorder f
set amt_actualoutsideservices = 0;

update fact_pmorder f
set amt_actualoutsideservices = t.costs
from tmp_040_4 t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";


/* Actual Miscellaneous Costs */
drop table if exists tmp_pmco_050_4;

create table tmp_pmco_050_4
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where pmco_cocur = 'EUR'
	and pmco_acpos = 050       
	and pmco_wrttp = 04       
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_050_4;

create table tmp_050_4
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_050_4
order by pmco_objnr;

update fact_pmorder f
set amt_actualmiscellaneous = 0;

update fact_pmorder f
set amt_actualmiscellaneous = t.costs
from tmp_050_4 t, dim_ordermaster o,  fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";

/*Alin 3 apr 2018 APP-7223 - 4 new measures*/
/* Estimated Labor Costs */
drop table if exists tmp_pmco_010_1_PM;

create table tmp_pmco_010_1_PM
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where   pmco_acpos = 010       
	and pmco_wrttp = 01   
	and pmco_versn = 'PM' 
	and pmco_cocur = 'EUR'   
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_010_1_PM;

create table tmp_010_1_PM
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_010_1_PM
order by pmco_objnr;

update fact_pmorder f
set amt_estimated_labor_costs = 0;

update fact_pmorder f
set amt_estimated_labor_costs = t.costs
from tmp_010_1_PM t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";

/* Estimated Material Costs */
drop table if exists tmp_pmco_020_1_PM;

create table tmp_pmco_020_1_PM
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where   pmco_acpos = 020       
	and pmco_wrttp = 01   
	and pmco_versn = 'PM'    
	and pmco_cocur = 'EUR'
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_020_1_PM;

create table tmp_020_1_PM
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_020_1_PM
order by pmco_objnr;

update fact_pmorder f
set amt_estimated_material_costs = 0;

update fact_pmorder f
set amt_estimated_material_costs = t.costs
from tmp_020_1_PM t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";

/* Estimated Outside Services Costs */
drop table if exists tmp_pmco_030_1_PM;

create table tmp_pmco_030_1_PM
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where   pmco_acpos = 030       
	and pmco_wrttp = 01   
	and pmco_versn = 'PM'    
	and pmco_cocur = 'EUR'
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_030_1_PM;

create table tmp_030_1_PM
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_030_1_PM
order by pmco_objnr;

update fact_pmorder f
set amt_est_outside_serv_costs = 0;

update fact_pmorder f
set amt_est_outside_serv_costs = t.costs
from tmp_030_1_PM t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";

/* Estimated Miscellaneous Costs */
drop table if exists tmp_pmco_040_1_PM;

create table tmp_pmco_040_1_PM
as
select pmco_objnr,  sum(pmco_wrt00) as w00, sum(pmco_wrt01) as w01, 
	sum(pmco_wrt02) as w02, sum(pmco_wrt03) as w03, sum(pmco_wrt04) as w04, sum(pmco_wrt05) as w05, 
	sum(pmco_wrt06) as w06, sum(pmco_wrt07) as w07, sum(pmco_wrt08) as w08, sum(pmco_wrt09) as w09,
	sum(pmco_wrt10) as w10, sum(pmco_wrt11) as w11, sum(pmco_wrt12) as w12, sum(pmco_wrt13) as w13, 
	sum(pmco_wrt14) as w14, sum(pmco_wrt15) as w15, sum(pmco_wrt16) as w16
from pmco
where   pmco_acpos = 040       
	and pmco_wrttp = 01   
	and pmco_versn = 'PM'  
	and pmco_cocur = 'EUR'  
group by pmco_objnr
order by pmco_objnr;

drop table if exists tmp_040_1_PM;

create table tmp_040_1_PM
as
select pmco_objnr,
	w00+w01+w02+w03+w04+w05+w06+w07+w08+w09+w10+w11+w12+w13+w14+w15+w16 as costs 
from tmp_pmco_040_1_PM
order by pmco_objnr;

update fact_pmorder f
set amt_est_misc_costs = 0;

update fact_pmorder f
set amt_est_misc_costs = t.costs
from tmp_040_1_PM t, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and substr(t.pmco_objnr, 3) = o."order";


/* Update std_exchangerate_dateid by AROBESCU on 18 May 2017  */

UPDATE fact_pmorder pm
SET pm.std_exchangerate_dateid = pm.dim_dateIDOrderCreatedDt
WHERE  pm.std_exchangerate_dateid <> pm.dim_dateIDOrderCreatedDt;

/*Alin 27 Apr 2018 APP-7223 use PMCO instead of AUFK*/
/*UPDATE fact_pmorder pm
SET pm.dim_currencyid = ifnull(dc.dim_currencyid,1)
FROM dim_currency dc , AUFK , dim_ordermaster o, dim_company c , fact_pmorder pm
where pm.dim_ordermasterid = o.dim_ordermasterid 
AND o."order" = AUFK_AUFNR 
AND c.currency = dc.currencycode
and c.companycode = ifnull(AUFK_BUKRS,'Not Set')
and pm.dim_currencyid <> ifnull(dc.dim_currencyid,1)*/

merge into fact_pmorder f
using(
select distinct fact_pmorderid, ifnull(dc.dim_currencyid,1) as dim_currencyid
FROM dim_currency dc , --PMCO p, 
dim_ordermaster o, fact_pmorder pm
where pm.dim_ordermasterid = o.dim_ordermasterid 
--AND o."order" = substr(p.pmco_objnr, 3) 
--AND dc.currencycode = ifnull(p.PMCO_COCUR, 'Not Set')
AND dc.currencycode = 'EUR'
--and PMCO_COCUR = 'EUR'
) t
on t.fact_pmorderid = f.fact_pmorderid
when matched then update
set f.dim_currencyid = t.dim_currencyid
where f.dim_currencyid <> t.dim_currencyid;

 
/*UPDATE fact_pmorder pm
SET dim_currencyid_tra = ifnull(dc.dim_currencyid,1)
FROM dim_currency dc , AUFK , dim_ordermaster o, fact_pmorder pm
where pm.dim_ordermasterid = o.dim_ordermasterid 
AND o."order" = AUFK_AUFNR 
AND ifnull(AUFK_WAERS, 'Not Set') = dc.currencycode
and dim_currencyid_tra <> ifnull(dc.dim_currencyid,1)*/

merge into fact_pmorder f
using(
select distinct fact_pmorderid, ifnull(dc.dim_currencyid,1) as dim_currencyid
FROM dim_currency dc , --PMCO p, 
dim_ordermaster o, fact_pmorder pm
where pm.dim_ordermasterid = o.dim_ordermasterid 
--AND o."order" = substr(p.pmco_objnr, 3) 
--AND dc.currencycode = ifnull(p.PMCO_COCUR, 'Not Set')
AND dc.currencycode = 'EUR'
--and PMCO_COCUR = 'EUR'
) t
on t.fact_pmorderid = f.fact_pmorderid
when matched then update
set f.dim_currencyid_tra = t.dim_currencyid
where f.dim_currencyid_tra <> t.dim_currencyid;



UPDATE fact_pmorder pm
SET dim_currencyid_gbl = ifnull(dc.dim_currencyid,1)
FROM dim_currency dc , pGlobalCurrency_po_43 , fact_pmorder pm
  WHERE dc.CurrencyCode = pGlobalCurrency 
  AND dim_currencyid_gbl <> ifnull(dc.dim_currencyid,1);


UPDATE fact_pmorder  f
        SET f.amt_exchangerate = z.exchangerate
    FROM tmp_getexchangerate1 z, dim_currency tra, dim_currency lcl, dim_date d , fact_pmorder  f
    WHERE f.dim_currencyid_tra = tra.dim_currencyid
      AND f.dim_currencyid = lcl.dim_currencyid
      AND f.dim_dateIDOrderCreatedDt = d.dim_dateid
      AND z.pFromCurrency = tra.currencycode
      AND z.pToCurrency = lcl.currencycode
      AND z.pFromExchangeRate = 0
      AND z.pDate = d.datevalue
      AND z.fact_script_name = 'bi_populate_pmorder_analysis_fact'
      AND f.amt_exchangerate <> z.exchangerate;
	  
	  
	  
UPDATE fact_pmorder  f
        SET f.amt_exchangerate_gbl = z.exchangerate
    FROM tmp_getexchangerate1 z, dim_currency tra, dim_currency gbl, dim_date d, fact_pmorder  f
    WHERE f.dim_currencyid_tra = tra.dim_currencyid
      AND f.dim_currencyid_gbl = gbl.dim_currencyid
      AND f.dim_dateIDOrderCreatedDt = d.dim_dateid
      AND z.pFromCurrency = tra.currencycode
      AND z.pToCurrency = gbl.currencycode
      AND z.pFromExchangeRate = 0
      AND z.pDate = d.datevalue
      AND z.fact_script_name = 'bi_populate_pmorder_analysis_fact'
      AND f.amt_exchangerate_gbl <> z.exchangerate;

/*Alin 19 Oct 2017 APP-7223*/
update fact_pmorder f
set f.DIM_DATEIDBASICFINISHDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_GLTRP, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEIDBASICFINISHDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEIDBASICSTARTDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_GSTRP, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEIDBASICSTARTDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEIDRELEASEDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_FTRMS, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEIDRELEASEDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEISCHEDULEDFINISHDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_GLTRS, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEISCHEDULEDFINISHDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEISCHEDULEDSTARTDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_GSTRS, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEISCHEDULEDSTARTDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEIACTUALSTARTDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_GSTRI, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEIACTUALSTARTDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEIDCONFIRMEDACTUALFINISHDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_GETRI, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEIDCONFIRMEDACTUALFINISHDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEIDACTUALFINISHDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_GLTRI, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEIDACTUALFINISHDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEIDACTUALRELEASEDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_FTRMI, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEIDACTUALRELEASEDATE <> d.dim_dateid;

update fact_pmorder f
set f.DIM_DATEIDPLANNEDRELEASEDATE = d.dim_dateid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.AFKO_ZZNPLDA, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and f.DIM_DATEIDPLANNEDRELEASEDATE <> d.dim_dateid;

--system statuses

MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDWAITFORMATERIALS
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0010'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDWAITFORMATERIALS = T.DIM_DATEIDWAITFORMATERIALS
where F.DIM_DATEIDWAITFORMATERIALS <> T.DIM_DATEIDWAITFORMATERIALS;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDSHUTDOWNREQ
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0011'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDSHUTDOWNREQ = T.DIM_DATEIDSHUTDOWNREQ
where F.DIM_DATEIDSHUTDOWNREQ <> T.DIM_DATEIDSHUTDOWNREQ;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDEHSRELEVANT
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0012'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDEHSRELEVANT = T.DIM_DATEIDEHSRELEVANT
where F.DIM_DATEIDEHSRELEVANT <> T.DIM_DATEIDEHSRELEVANT;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDNOTOFEVENTASSOC
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0013'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDNOTOFEVENTASSOC = T.DIM_DATEIDNOTOFEVENTASSOC
where F.DIM_DATEIDNOTOFEVENTASSOC <> T.DIM_DATEIDNOTOFEVENTASSOC;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDCAPAASSOC
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0014'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDCAPAASSOC = T.DIM_DATEIDCAPAASSOC
where F.DIM_DATEIDCAPAASSOC <> T.DIM_DATEIDCAPAASSOC;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDBCALLIN
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0014'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDBCALLIN = T.DIM_DATEIDBCALLIN
where F.DIM_DATEIDBCALLIN <> T.DIM_DATEIDBCALLIN;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDCHANGECONTROL
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0016'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDCHANGECONTROL = T.DIM_DATEIDCHANGECONTROL
where F.DIM_DATEIDCHANGECONTROL <> T.DIM_DATEIDCHANGECONTROL;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDREFRIGERANT
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0017'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDREFRIGERANT = T.DIM_DATEIDREFRIGERANT
where F.DIM_DATEIDREFRIGERANT <> T.DIM_DATEIDREFRIGERANT;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDEMERGWORK
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0018'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDEMERGWORK = T.DIM_DATEIDEMERGWORK
where F.DIM_DATEIDEMERGWORK <> T.DIM_DATEIDEMERGWORK;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDSAFETYWORK
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0019'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDSAFETYWORK = T.DIM_DATEIDSAFETYWORK
where F.DIM_DATEIDSAFETYWORK <> T.DIM_DATEIDSAFETYWORK;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEICOMPLWORKREQ
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0020'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEICOMPLWORKREQ = T.DIM_DATEICOMPLWORKREQ
where F.DIM_DATEICOMPLWORKREQ <> T.DIM_DATEICOMPLWORKREQ;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDDELIVWORKREQ
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0021'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDDELIVWORKREQ = T.DIM_DATEIDDELIVWORKREQ
where F.DIM_DATEIDDELIVWORKREQ <> T.DIM_DATEIDDELIVWORKREQ;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDWORKREQGENFRPM
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0022'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDWORKREQGENFRPM = T.DIM_DATEIDWORKREQGENFRPM
where F.DIM_DATEIDWORKREQGENFRPM <> T.DIM_DATEIDWORKREQGENFRPM;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDEXECUTIONWOCOMPLETED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0034'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDEXECUTIONWOCOMPLETED = T.DIM_DATEIDEXECUTIONWOCOMPLETED
where F.DIM_DATEIDEXECUTIONWOCOMPLETED  <> T.DIM_DATEIDEXECUTIONWOCOMPLETED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_dateidDiscontinued
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0023'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_dateidDiscontinued = T.DIM_dateidDiscontinued
where F.DIM_dateidDiscontinued  <> T.DIM_dateidDiscontinued;

--user statuses

MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDCREATED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0001'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDCREATED = T.DIM_DATEIDCREATED
where F.DIM_DATEIDCREATED <> T.DIM_DATEIDCREATED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDRELEASED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0002'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDRELEASED = T.DIM_DATEIDRELEASED
where F.DIM_DATEIDRELEASED <> T.DIM_DATEIDRELEASED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDPARTIALLYCONF
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0010'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDPARTIALLYCONF = T.DIM_DATEIDPARTIALLYCONF
where F.DIM_DATEIDPARTIALLYCONF <> T.DIM_DATEIDPARTIALLYCONF;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDCONFIRMED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0009'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDCONFIRMED = T.DIM_DATEIDCONFIRMED
where F.DIM_DATEIDCONFIRMED <> T.DIM_DATEIDCONFIRMED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDNOMATCOMP
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0485'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDNOMATCOMP = T.DIM_DATEIDNOMATCOMP
where F.DIM_DATEIDNOMATCOMP <> T.DIM_DATEIDNOMATCOMP;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDMATCOMMITTED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0340'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDMATCOMMITTED = T.DIM_DATEIDMATCOMMITTED
where F.DIM_DATEIDMATCOMMITTED <> T.DIM_DATEIDMATCOMMITTED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDGOODSMOVCHECKED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0321'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDGOODSMOVCHECKED = T.DIM_DATEIDGOODSMOVCHECKED
where F.DIM_DATEIDGOODSMOVCHECKED <> T.DIM_DATEIDGOODSMOVCHECKED;



MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDMATAVNOTCHECKED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0420'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDMATAVNOTCHECKED = T.DIM_DATEIDMATAVNOTCHECKED
where F.DIM_DATEIDMATAVNOTCHECKED <> T.DIM_DATEIDMATAVNOTCHECKED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDMATSHORTAGE
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0004'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDMATSHORTAGE = T.DIM_DATEIDMATSHORTAGE
where F.DIM_DATEIDMATSHORTAGE <> T.DIM_DATEIDMATSHORTAGE;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDATESNOTUPDATED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0118'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDATESNOTUPDATED = T.DIM_DATEIDATESNOTUPDATED
where F.DIM_DATEIDATESNOTUPDATED <> T.DIM_DATEIDATESNOTUPDATED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDTECHCOMPLETED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0045'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDTECHCOMPLETED = T.DIM_DATEIDTECHCOMPLETED
where F.DIM_DATEIDTECHCOMPLETED <> T.DIM_DATEIDTECHCOMPLETED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIPRECOSTED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0016'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIPRECOSTED = T.DIM_DATEIPRECOSTED
where F.DIM_DATEIPRECOSTED <> T.DIM_DATEIPRECOSTED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDSETTLRULECREATED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0028'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDSETTLRULECREATED = T.DIM_DATEIDSETTLRULECREATED
where F.DIM_DATEIDSETTLRULECREATED <> T.DIM_DATEIDSETTLRULECREATED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEIDCLOSED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0046'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDCLOSED = T.DIM_DATEIDCLOSED
where F.DIM_DATEIDCLOSED <> T.DIM_DATEIDCLOSED;

MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
MIN(CASE WHEN JCDS_INACT = 'X' THEN 1 ELSE D.DIM_DATEID END) AS DIM_DATEID_NOTCOMPLETED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'I0015'
GROUP BY FACT_PMORDERID)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEID_NOTCOMPLETED = T.DIM_DATEID_NOTCOMPLETED
where F.DIM_DATEID_NOTCOMPLETED <> T.DIM_DATEID_NOTCOMPLETED;

/*Fields acc tpo APP-7878*/
/* update fact_pmorder f
set f.dd_maintenanceplan_afih_mhis = ifnull(AFIH_WARPL, 'Not Set')
	,dw_update_date = current_timestamp
from  dim_ordermaster d, afih , mhis, fact_pmorder f
where f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = ifnull(afih_aufnr, 'Not Set')
and AFIH_WARPL = MHIS_WARPL
and AFIH_ABNUM = MHIS_ABNUM 
and f.dd_maintenanceplan_afih_mhis <> ifnull(AFIH_WARPL, 'Not Set') */


merge into fact_pmorder f
using 
(
select f.dim_ordermasterid,
max(ifnull(AFIH_WARPL, 'Not Set')) as dd_maintenanceplan_afih_mhis
from dim_ordermaster d, afih , mhis, fact_pmorder f 
where f.dim_ordermasterid = d.dim_ordermasterid 
and d."order" = ifnull(afih_aufnr, 'Not Set') 
and AFIH_WARPL = MHIS_WARPL and AFIH_ABNUM = MHIS_ABNUM 
and f.dd_maintenanceplan_afih_mhis <> ifnull(AFIH_WARPL, 'Not Set')
group by f.dim_ordermasterid
)x
on
(
f.dim_ordermasterid = x.dim_ordermasterid
)
when matched then update
set f.dd_maintenanceplan_afih_mhis = x.dd_maintenanceplan_afih_mhis,
dw_update_date = current_timestamp;

/*
update fact_pmorder f
set f.DD_MAINT_PLAN_CALL_NUMBER = ifnull(MHIS_ABNUM, 'Not Set')
	,dw_update_date = current_timestamp
from  dim_ordermaster d, afih , mhis, fact_pmorder f
where f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = ifnull(afih_aufnr, 'Not Set')
and AFIH_WARPL = MHIS_WARPL
and AFIH_ABNUM = MHIS_ABNUM 
and f.DD_MAINT_PLAN_CALL_NUMBER <> ifnull(MHIS_ABNUM, 'Not Set')*/

MERGE INTO fact_pmorder f
USING (SELECT d.dim_ordermasterid as dim_ordermasterid, 
        MAX(ifnull(MHIS_ABNUM, 'Not Set')) as MHIS_ABNUM
        from dim_ordermaster d, afih , mhis, fact_pmorder f
         where f.dim_ordermasterid = d.dim_ordermasterid
         and d."order" = ifnull(afih_aufnr, 'Not Set')
         and AFIH_WARPL = MHIS_WARPL
         and AFIH_ABNUM = MHIS_ABNUM 
         group by d.dim_ordermasterid) x
ON (f.dim_ordermasterid = x.dim_ordermasterid)
WHEN MATCHED THEN
UPDATE SET f.DD_MAINT_PLAN_CALL_NUMBER = x.MHIS_ABNUM,
f.dw_update_date = current_timestamp
WHERE f.DD_MAINT_PLAN_CALL_NUMBER <> x.MHIS_ABNUM;

update fact_pmorder f
set f.dd_maint_item = ifnull(MPOS_WAPOS, 'Not Set')
	,dw_update_date = current_timestamp
from  dim_ordermaster d, afih , MPOS, fact_pmorder f
where f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = ifnull(afih_aufnr, 'Not Set')
and AFIH_WAPOS = MPOS_WAPOS
and f.dd_maint_item <> ifnull(MPOS_WAPOS, 'Not Set');

/*Alin APP-7878 vimhio-laufn field 26 feb 2018*/
update fact_pmorder f
set f.dd_last_order = ifnull(VIMHIO_LAUFN, 'Not Set')
,dw_update_date = current_timestamp
from  dim_ordermaster d, VIMHIO v , fact_pmorder f
where f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = ifnull(vimhio_aufnr, 'Not Set')
and f.dd_maintenancePlan = ifnull(v.vimhio_warpl,'Not Set')
and f.dd_last_order <> ifnull(VIMHIO_LAUFN, 'Not Set');

/*KBED dim changes APP-7223 ALIN 23 APR 2018*/
update fact_pmorder f
set f.dd_cap_req_rec_id = ifnull(aa.AFKO_BEDID, 0)
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and f.dd_cap_req_rec_id <> ifnull(aa.AFKO_BEDID, 0);

merge into fact_pmorder f
using(
select distinct fact_pmorderid, max(crc.dim_cap_req_recid) as dim_cap_req_recid
FROM AFKO_AUFK aa, dim_ordermaster o, fact_pmorder f, dim_cap_req_rec crc
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AFKO_AUFNR, 'Not Set') = o."order"
and crc.cap_id_of_req = ifnull(aa.AFKO_BEDID, 0)
group by fact_pmorderid
)t
on t.fact_pmorderid = f.fact_pmorderid
when matched then update 
set f.dim_cap_req_recid = t.dim_cap_req_recid
where f.dim_cap_req_recid <> t.dim_cap_req_recid;

drop table if exists earliest_latest_splits_dates;
create table earliest_latest_splits_dates as
select distinct
cap_id_of_req,  orderr, 
first_value(earliest_split_start) over (partition by cap_id_of_req,  orderr
order by internal_counter asc) as earliest_split_start,
first_value(latest_split_finish) over (partition by cap_id_of_req,  orderr
order by internal_counter desc)as latest_split_finish
from dim_cap_req_rec
where orderr <> 'Not Set';

update fact_pmorder f
set f.dim_dateid_EARLIEST_SPLIT_ST = ifnull(dt.dim_dateid,1)
from  dim_ordermaster d, dim_Date dt,  fact_pmorder f, earliest_latest_splits_dates cap	
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = ifnull(cap.EARLIEST_SPLIT_START, '0001-01-01')
and dt.companycode = d.company_code
and dt.plantcode_factory = d.plant 
and f.dd_cap_req_rec_id = cap.cap_id_of_req
and cap.orderr = d."order";

update fact_pmorder f
set f.dim_dateid_latest_split_finish = ifnull(dt.dim_dateid,1)
from  dim_ordermaster d, dim_Date dt,  fact_pmorder f, earliest_latest_splits_dates cap	
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = ifnull(cap.LATEST_SPLIT_FINISH, '0001-01-01')
and dt.companycode = d.company_code
and dt.plantcode_factory = d.plant 
and f.dd_cap_req_rec_id = cap.cap_id_of_req
and cap.orderr = d."order";

drop table if exists earliest_latest_splits_dates;


/*Alin 15 may work attribute remodelled as measure*/
merge into fact_pmorder f
using(
select distinct fact_pmorderid, avg(d.workk) as workk
from fact_pmorder f, dim_cap_req_rec d
where d.cap_id_of_req = f.dd_cap_req_rec_id
group by fact_pmorderid
)t
on t.fact_pmorderid = f.fact_pmorderid
when matched then update
set f.ct_work = t.workk;

/*Alin 15 May 2018 coloring indicator measure transformed to Attribute*/
drop table if exists tmp_ind_coloring;
create table tmp_ind_coloring as
SELECT distinct 
 "order" as orderr,
f_pmo.DD_MAINT_PLAN_CALL_NUMBER,
									ROUND(
										case
											when
												(
													AVG(
														CASE
															WHEN
																(
																	CASE
																		WHEN
																			tecodt.DateValue = '0001-01-01'
																		THEN
																			'No TECO Date'
																		ELSE
																			'TECOed'
																	END
																) = 'No TECO Date'
															THEN
																CURRENT_DATE -(mntpldt.DateValue)
															ELSE
																0
														END
													)
												) between 1 and
												34
											then
												'1'
											when
												(
													AVG(
														CASE
															WHEN
																(
																	CASE
																		WHEN
																			tecodt.DateValue = '0001-01-01'
																		THEN
																			'No TECO Date'
																		ELSE
																			'TECOed'
																	END
																) = 'No TECO Date'
															THEN
																CURRENT_DATE -(mntpldt.DateValue)
															ELSE
																0
														END
													)
												) > 34
											then
												'2'
											when
												(
													AVG(
														CASE
															WHEN
																(
																	CASE
																		WHEN
																			tecodt.DateValue = '0001-01-01'
																		THEN
																			'No TECO Date'
																		ELSE
																			'TECOed'
																	END
																) = 'No TECO Date'
															THEN
																CURRENT_DATE -(mntpldt.DateValue)
															ELSE
																0
														END
													)
												) < 1
											then
												'3'
											else
												null
										end,
										0
									) coloring_indicator
								FROM
										fact_pmorder AS f_pmo
									INNER JOIN
										dim_ordermaster AS ord
									ON
										f_pmo.dim_ordermasterid = ord.dim_ordermasterid 
									INNER JOIN
										Dim_Date AS mntpldt
									ON
										f_pmo.dim_dateidmaintplanduedt = mntpldt.Dim_Dateid
									INNER JOIN
										Dim_Date AS tecodt
									ON
										f_pmo.dim_dateidtechcompdt = tecodt.Dim_Dateid
								GROUP BY
									 "order",
									f_pmo.DD_MAINT_PLAN_CALL_NUMBER;


drop table if exists tmp_ind_coloring2;
create table tmp_ind_coloring2
as select 
t.*,
case when coloring_indicator = 1 then '1-34 Days'
when coloring_indicator = 2 then '>34 Days'
when coloring_indicator = 3 then 'No Aging'
end as coloring_indicator2
 from tmp_ind_coloring t;

update fact_pmorder f
set dd_due_date_indicator_coloring = t.coloring_indicator2
from fact_pmorder f, tmp_ind_coloring2 t, dim_ordermaster d
where f.dim_ordermasterid = d.dim_ordermasterid
--and f.DD_MAINT_PLAN_CALL_NUMBER = t.DD_MAINT_PLAN_CALL_NUMBER
and d."order" = t.orderr
and dd_due_date_indicator_coloring <> t.coloring_indicator2;

drop table if exists tmp_ind_coloring;
drop table if exists tmp_ind_coloring2;

/*Alin 09 Sept 2018 coloring indicator measure transformed to Attribute*/
drop table if exists Due_Date_Aging_Executed_Orders;
create table Due_Date_Aging_Executed_Orders as
SELECT distinct 
 "order" as orderr,
f_pmo.DD_MAINT_PLAN_CALL_NUMBER,
ROUND(AVG(CASE WHEN  case when dnu2.EXECUTIONWOCOMPLETED like '%X%' then 'Yes' else 'No' end  = 'No' 
THEN CURRENT_DATE -  mntpldt.DateValue  ELSE 0 END),0) Col_ord_1,ROUND(case when  AVG(CASE WHEN  case when dnu2.EXECUTIONWOCOMPLETED like '%X%' then 'Yes' else 'No' end  = 'No' THEN CURRENT_DATE -  mntpldt.DateValue  ELSE 0 END)  between 1 and 34 then '1'
when  AVG(CASE WHEN  case when dnu2.EXECUTIONWOCOMPLETED like '%X%' then 'Yes' else 'No' end  = 'No' THEN CURRENT_DATE -  mntpldt.DateValue  ELSE 0 END)  > 34 then '2'
when  AVG(CASE WHEN  case when dnu2.EXECUTIONWOCOMPLETED like '%X%' then 'Yes' else 'No' end  = 'No' THEN CURRENT_DATE -  mntpldt.DateValue  ELSE 0 END)  < 1 then '3'
else null
end,0) coloring_indicator

FROM fact_pmorder AS f_pmo 
INNER JOIN dim_ordermaster AS ord 
ON f_pmo.dim_ordermasterid = ord.dim_ordermasterid  
INNER JOIN dim_OrderuserstatusPMNotif AS dnu2 
ON f_pmo.DIM_NOTIFUSERSTATUSID_2 = dnu2.dim_OrderuserstatusPMNotifid  
INNER JOIN Dim_Date AS mntpldt 
ON f_pmo.dim_dateidmaintplanduedt = mntpldt.Dim_Dateid  
GROUP BY
 "order",
f_pmo.DD_MAINT_PLAN_CALL_NUMBER;

drop table if exists Due_Date_Aging_Executed_Orders2;
create table Due_Date_Aging_Executed_Orders2
as select 
t.*,
case when coloring_indicator = 1 then '1-34 Days'
when coloring_indicator = 2 then '>34 Days'
when coloring_indicator = 3 then 'No Aging'
end as coloring_indicator2
 from Due_Date_Aging_Executed_Orders t;

update fact_pmorder f
set dd_due_date_indicator_coloring2 = t.coloring_indicator2
from fact_pmorder f, Due_Date_Aging_Executed_Orders2 t, dim_ordermaster d
where f.dim_ordermasterid = d.dim_ordermasterid
--and f.DD_MAINT_PLAN_CALL_NUMBER = t.DD_MAINT_PLAN_CALL_NUMBER
and d."order" = t.orderr
and dd_due_date_indicator_coloring2 <> t.coloring_indicator2;

drop table if exists Due_Date_Aging_Executed_Orders;
drop table if exists Due_Date_Aging_Executed_Orders2;


/*Alin 26 July 2018 APP-7223*/
update fact_pmorder f
set f.dd_objnr = ifnull(AUFK_OBJNR, 'Not Set')
  ,dw_update_date = current_timestamp
from  AUFK p,  dim_ordermaster d, fact_pmorder f  
where f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = aufk_aufnr
and AUFK_AUART IN('PM01', 'PM02', 'PM03', 'PM04', 'PM05', 'PM06', 'PM07', 'PM08', 'PM09')
and f.dd_objnr <> ifnull(AUFK_OBJNR, 'Not Set');

merge into fact_pmorder f
using(
select distinct fact_pmorderid, avg(r.settlement_percentage_rate) as settlement_percentage_rate
from  AUFK p,  dim_ordermaster d, fact_pmorder f, dim_receiving_costcenter r  
where f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = aufk_aufnr
and AUFK_AUART IN('PM01', 'PM02', 'PM03', 'PM04', 'PM05', 'PM06', 'PM07', 'PM08', 'PM09')
and f.dd_objnr = r.objectnumber
group by fact_pmorderid
)t 
on t.fact_pmorderid = f.fact_pmorderid
when matched then update 
set f.ct_sett_perc_rate = t.settlement_percentage_rate,
dw_update_date = current_timestamp
where f.ct_sett_perc_rate <> t.settlement_percentage_rate;

/*Alin App-7223 22 Oct*/
merge into fact_pmorder f
using(
select distinct f.fact_pmorderid, c.dim_costcenterid
from fact_pmorder f, dim_ordermaster d, AUFK a, dim_costcenter c
where f.dim_ordermasterid = d.dim_ordermasterid
and ifnull(a.AUFK_AUFNR, 'Not Set') = d."order"
and c.code = ifnull(a.AUFK_KOSTL, 'Not Set')
and c.ValidTo = '9999-12-31'
) t
on t.fact_pmorderid = f.fact_pmorderid
when matched then update
set f.dim_costcenterid = t.dim_costcenterid
where f.dim_costcenterid <> t.dim_costcenterid;

merge into fact_pmorder f
using(
select distinct fact_pmorderid, ifnull(c.CSKS_VERAK, 'Not Set') as pers_resp
from fact_pmorder f, dim_ordermaster d, AUFK a, CSKS c
where f.dim_ordermasterid = d.dim_ordermasterid
and ifnull(a.AUFK_AUFNR, 'Not Set') = d."order"
and a.AUFK_KOSTL = c.CSKS_KOSTL
and c.CSKS_DATBI = '9999-12-31'
) t
on t.fact_pmorderid = f.fact_pmorderid
when matched then update
set f.dd_person_responsible = t.pers_resp
where f.dd_person_responsible <> t.pers_resp;

MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
first_value(d.dim_dateid) over (partition by JCDS_OBJNR order by jcds_udate asc)  AS DIM_DATEIDEXECUTIONWOCOMPLETED
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0034'
)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_DATEIDEXECUTIONWOCOMPLETED = T.DIM_DATEIDEXECUTIONWOCOMPLETED
where F.DIM_DATEIDEXECUTIONWOCOMPLETED  <> T.DIM_DATEIDEXECUTIONWOCOMPLETED;


MERGE INTO FACT_PMORDER F
USING(
SELECT DISTINCT FACT_PMORDERID,
first_value(d.dim_dateid) over (partition by JCDS_OBJNR order by jcds_udate asc) AS DIM_dateidDiscontinued
FROM JCDS_AUFK aa,  dim_ordermaster o, fact_pmorder f, dim_date d
where f.dim_ordermasterid = o.dim_ordermasterid
and ifnull(aa.AUFK_AUFNR, 'Not Set') = o."order"
and d.datevalue = ifnull(aa.JCDS_UDATE, '0001-01-01')
and d.companycode = o.company_code
and d.plantcode_factory = o.plant 
and aa.JCDS_STAT = 'E0023'
)T
ON T.FACT_PMORDERID = F.FACT_PMORDERID
WHEN MATCHED THEN UPDATE SET
F.DIM_dateidDiscontinued = T.DIM_dateidDiscontinued
where F.DIM_dateidDiscontinued  <> T.DIM_dateidDiscontinued;

/*APP-10655 Alin 13 Nov 2018 */
merge into fact_pmorder f
using(select distinct f_pmo.fact_pmorderid,
case when case when
ordtyp.TypeCode = 'PM01' then 'Corrective'
when
ordtyp.TypeCode = 'PM02' and mtacttype.mainactivitytype <> '141' then 'Preventive'
when
ordtyp.TypeCode = 'PM02' and mtacttype.mainactivitytype = '141' then 'Legal'
when
ordtyp.TypeCode = 'PM05' and trim(leading '0' from f_pmo.DD_MAINTENANCEPLAN_AFIH_MHIS) <> 'Not Set' then 'Preventive'
when
ordtyp.TypeCode = 'PM05' and trim(leading '0' from f_pmo.DD_MAINTENANCEPLAN_AFIH_MHIS) = 'Not Set' then 'Corrective'
when
ordtyp.TypeCode = 'PM06' then 'Corrective'
when
ordtyp.TypeCode = 'PM07' then 'Corrective'
else 'Not Set' end = 'Preventive' then f_pmo.dim_dateischeduledstartdate

when case when
ordtyp.TypeCode = 'PM01' then 'Corrective'
when
ordtyp.TypeCode = 'PM02' and mtacttype.mainactivitytype <> '141' then 'Preventive'
when
ordtyp.TypeCode = 'PM02' and mtacttype.mainactivitytype = '141' then 'Legal'
when
ordtyp.TypeCode = 'PM05' and trim(leading '0' from f_pmo.DD_MAINTENANCEPLAN_AFIH_MHIS) <> 'Not Set' then 'Preventive'
when
ordtyp.TypeCode = 'PM05' and trim(leading '0' from f_pmo.DD_MAINTENANCEPLAN_AFIH_MHIS) = 'Not Set' then 'Corrective'
when
ordtyp.TypeCode = 'PM06' then 'Corrective'
when
ordtyp.TypeCode = 'PM07' then 'Corrective'
else 'Not Set' end = 'Corrective' then f_pmo.dim_dateischeduledstartdate

else f_pmo.dim_dateidplannedreleasedate
end as dim_dateidschedadh

from fact_pmorder f_pmo, dim_productionordertype ordtyp, dim_mainactivitytype mtacttype, dim_ordermaster d
where f_pmo.dim_productionordertypeid = ordtyp.dim_productionordertypeid
and f_pmo.dim_mainactivitytypeid = mtacttype.dim_mainactivitytypeid
and f_pmo.dim_ordermasterid = d.dim_ordermasterid
)t
on f.fact_pmorderid = t.fact_pmorderid
when matched then update
set f.dim_dateidschedadherance = t.dim_dateidschedadh;