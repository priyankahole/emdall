/* ***********************************************************************************************************************************
   Script         - vw_bi_populate_effacts_fact.sql
   Author         - Octavian Stepan
   Created ON     - Jun 2018
   Description    - Populate fact_effacts as requested by Merck Ah - APP-9861
   ************************************************************************************************************************************/


drop table if exists tmp_fact_effacts;
create table tmp_fact_effacts
LIKE fact_effacts INCLUDING DEFAULTS INCLUDING IDENTITY;

/* To generate surrogate keys*/
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_effacts';

INSERT INTO NUMBER_FOUNTAIN
select 	'tmp_fact_effacts',
	ifnull(max(f.fact_effactsid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from tmp_fact_effacts f;

/* Insert new rows */

INSERT INTO tmp_fact_effacts(
	fact_effactsid,
	dim_merckpersondataid,
	dim_systemreportcontractsid
	)
SELECT
(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_effacts') + row_number() over(order by '') as fact_effactsid,
	ifnull(m.dim_merckpersondataid,1) AS dim_merckpersondataid,
	ifnull(s.dim_systemreportcontractsid,1) AS dim_systemreportcontractsid
FROM dim_merckpersondata m
INNER JOIN dim_systemreportcontracts s ON m.isid = s.login;

INSERT INTO fact_effacts f
SELECT * FROM tmp_fact_effacts t WHERE NOT EXISTS (SELECT 1 
                                                     FROM fact_effacts f 
                                                    WHERE f.dim_merckpersondataid = t.dim_merckpersondataid
                                                      AND f.dim_systemreportcontractsid = t.dim_systemreportcontractsid) ;
UPDATE fact_effacts
   SET dim_dateidexpirationdate = d.dim_dateid,
       dw_update_date = current_timestamp
  FROM fact_effacts f  
 INNER JOIN dim_systemreportcontracts s ON f.dim_systemreportcontractsid = s.dim_systemreportcontractsid
 INNER JOIN dim_date d ON ifnull(CASE WHEN s.expirationdate < '2050-01-01' OR s.expirationdate is null
                                      THEN s.expirationdate 
                                      ELSE '9999-12-31'
                                 END,'0001-01-01') = d.datevalue
                       AND d.companycode = 'Not Set' 
                       AND d.plantcode_factory = 'Not Set'
WHERE dim_dateidexpirationdate <> d.dim_dateid;

DROP TABLE IF EXISTS tmp_fact_effacts;

