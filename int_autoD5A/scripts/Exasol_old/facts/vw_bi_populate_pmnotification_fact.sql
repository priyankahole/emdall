/*##################################################################################################################

  Script         : vw_bi_populate_pmnotification_fact.sql
  Author         : Suchithra
  Created On     : 13 Mar 2016
  Description    : Script for populating  PM Notification Subject area
                   Key: QMEL-QMNUM,QMFE-FENUM,QMAA-MANUM, QMUR-URNUM
		   (dd_notificationno,dd_notificationItemno,dd_consecutiveactivityNo,dd_causeNo)

  Change History
  Date                          Version         Description
  25Mar2016	Suchithra	1.0		Included FunctionalLocation and Equipment Dimensions
  07Apr2016	Suchihtra	1.1		Changed the source of Plant to QMEL-ARBPLWERK from MAWERK
  13Apr2016	Suchithra	1.2		Added Order Change date as a dimension
						Added Order Master Dimension
  18Apr2016	Suchithra	1.3		Changed the logic for Functional Location
  19Apr2016	Suchithra	1.4		Added AUFK-IDAT2 as a date dimension
						Added Planner Group dimension
						Added Order Category dimension
  03May2016	Suchithra	1.5		Added EQUI_MATNR as a dimension
						Added EQUI_ELIEF Vendor Dimension
						EQUI_INBDT, EQUZ_DATBI as date dimensions
						ILOA_KOSTL Cost Center Dimension
						Added AUFK_IDAT3, AUFK_ERDAT as date dimensions
						AUFK_PRCTR Profit Center Dimension
						MHIS_NPLDA - Maint Plan Due Date
						ILOA-BEBER Plant Section Dimension
						AFIH-ANLZU - Systemcondition Dimension
####################################################################################################################   */

delete from number_fountain m where m.table_name = 'fact_pmnotification';

insert into number_fountain
select  'fact_pmnotification',
        ifnull(max(d.fact_pmnotificationid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_pmnotification d
where d.fact_pmnotificationid <> 1;


drop table if exists tmp_insert_into_fact;
create table tmp_insert_into_fact as
select
	ifnull(QMEL_QMNUM,'Not Set') as dd_notificationno ,
	ifnull(QMFE_FENUM,0) as dd_notificationItemno ,
	ifnull(QMMA_MANUM,0) as dd_consecutiveactivityNo ,
	ifnull(QMUR_URNUM,0) as dd_causeNo ,
	ifnull(QMUR_URGRP,'Not Set') as dd_causecodegroup,
	ifnull(QMUR_URTXT,'Not Set') as dd_causeText,
	ifnull(QMFE_POSNR,0) as dd_notifitemSortNo ,
	ifnull(QMFE_FETXT,'Not Set') as dd_notifitemtext ,
	/* ifnull((select nt.dim_notificationtypeid from dim_notificationtype nt where nt.notificationtypecode = ifnull(QMEL_QMART,'Not Set')),1)*/ CONVERT(BIGINT,1) as dim_notificationtypeid,
	/* ifnull((select pl.dim_plantid from dim_plant pl where pl.plantcode = ifnull(QMEL_ARBPLWERK,'Not Set')),1)*/ CONVERT(BIGINT,1) as dim_plantID,
	/* ifnull((select dft.dim_Dateid from dim_Date dft where dft.datevalue = QMIH_AUSBS and dft.companycode = 'Not Set'),1)*/ CONVERT(BIGINT,1) as dim_dateidMalfunctionEndDate ,
	/* ifnull((select dmt.dim_Dateid from dim_Date dmt where dmt.datevalue = QMIH_AUSVN and dmt.companycode = 'Not Set'),1)*/ CONVERT(BIGINT,1) as dim_dateidMalfunctionStartDate ,
	ifnull(QMEL_REFNUM,'Not Set') as dd_externalRefNo ,
	ifnull(QMEL_AENAM,'Not Set') as dd_notifChangedBy ,
	ifnull(QMEL_ERNAM,'Not Set') as dd_notifcreatedBy ,
	ifnull(QMEL_QMTXT,'Not Set') as dd_notifDescription ,
	ifnull(QMEL_PRIOK,'Not Set') as dd_notifPriority ,
	ifnull(QMEL_QMNAM,'Not Set') as dd_notifreportedBy ,
	ifnull(QMEL_QWRNUM,'Not Set') as dd_referencenotification ,
	ifnull(QMFE_OTGRP,'Not Set') as dd_objectPart ,
	ifnull(QMFE_OTEIL,'Not Set') as dd_objectPartCode ,
	ifnull(QMFE_FEGRP,'Not Set') as dd_probemcodegrp ,
	ifnull(QMMA_MATXT,'Not Set') as dd_acitivityText ,
	ifnull(QMIH_MSAUS,'Not Set') as dd_breakdownIndicator ,
	ifnull(QMIH_AUSZT,0) as dd_breakdownduration ,
	ifnull(QMIH_MAUEH,'Not Set') as dd_breakdowndurationunit ,
	ifnull(QMFE_FEKAT,'Not Set') as dd_catalogtype,
	/* ifnull((select da.dim_codetextid from dim_codetext da
			where da.Catalog = 'A'
			and da.CodeGroup in ('0000','0001')
			and da.code =qmma_mncod
			and da.version = '000001' ),1)*/ CONVERT(BIGINT,1) as dim_activitycodetextid,
	/* ifnull((select dc.dim_codetextid from dim_codetext dc
			where dc.Catalog = '5'
			and dc.CodeGroup in ('0000', '0001','ZPM-N002')
			and dc.code =QMUR_URCOD
			and dc.version = '000001' ),1)*/ CONVERT(BIGINT,1) as dim_causecodetextid,
	/* ifnull((select dm.dim_codetextid from dim_codetext dm
			where dm.Catalog = 'C'
			and dm.CodeGroup = QMFE_FEGRP
			and dm.code =QMFE_FECOD
			and dm.version = '000001'),1)*/ CONVERT(BIGINT,1) as dim_Damagecodetextid,
	/* ifnull((select dm.dim_codetextid from dim_codetext dm
                        where dm.Catalog = 'D'
                        and dm.CodeGroup  = 'ZPM-N001'
                        and dm.code =QMEL_QMCOD
                        and dm.version = '000001'),1) */ CONVERT(BIGINT,1) as dim_codingcodetextid,
        /* ifnull((select dm.dim_codetextid from dim_codetext dm
                        where dm.Catalog = 'B'
                        and dm.CodeGroup  = QMFE_OTGRP
                        and dm.code = QMFE_OTEIL
                        and dm.version = '000001'),1) */ CONVERT(BIGINT,1) as dim_objectPartcodetextid,
        /* ifnull((select dm.dim_codetextid from dim_codetext dm
                        where dm.Catalog = 'A'
                        and dm.CodeGroup  = QMFE_FEGRP
                        and dm.code = QMFE_FECOD
                        and dm.version = '000001'),1)*/ CONVERT(BIGINT,1) as dim_Problemcodetextid,
	/* ifnull((select od.dim_ordermasterid from dim_ordermaster od
			where od.order = qmel_aufnr),1)*/ CONVERT(BIGINT,1) as dim_ordermasterid
from qmel
left join qmfe on qmfe_qmnum = qmel_qmnum
left join qmih on qmih_qmnum =qmel_qmnum
left join qmur on qmur_qmnum = qmel_qmnum and ifnull(qmur_fenum,0) = ifnull(qmfe_fenum,0)
left join qmma on qmma_qmnum = qmel_qmnum
where qmel_qmart in ('M1', 'M2', 'M3');

UPDATE tmp_insert_into_fact pn
SET pn.dim_notificationtypeid = nt.dim_notificationtypeid
from dim_notificationtype nt, tmp_insert_into_fact pn, QMEL
where nt.notificationtypecode = ifnull(QMEL_QMART,'Not Set')
AND ifnull(QMEL_QMNUM,'Not Set') = dd_notificationno
AND pn.dim_notificationtypeid <> nt.dim_notificationtypeid;

UPDATE tmp_insert_into_fact pn
SET pn.dim_plantID = pl.dim_plantID
from dim_plant pl, tmp_insert_into_fact pn, QMEL
where pl.plantcode = ifnull(QMEL_QMART,'Not Set')
AND ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
AND pn.dim_plantID <> pl.dim_plantID;

UPDATE tmp_insert_into_fact pn
SET pn.dim_dateidMalfunctionEndDate = dft.dim_Dateid
from dim_Date dft, tmp_insert_into_fact pn, QMEL, QMIH
where dft.datevalue = QMIH_AUSBS and dft.companycode = 'Not Set'
AND dft.plantcode_factory = ifnull(QMEL_MAWERK,'Not Set')
AND ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
AND qmih_qmnum =qmel_qmnum
AND pn.dim_dateidMalfunctionEndDate <> dft.dim_Dateid;

UPDATE tmp_insert_into_fact pn
SET pn.dim_dateidMalfunctionStartDate = dmt.dim_Dateid
from dim_Date dmt, tmp_insert_into_fact pn, QMEL, QMIH
where dmt.datevalue = QMIH_AUSVN and dmt.companycode = 'Not Set'
AND dmt.plantcode_factory = ifnull(QMEL_MAWERK,'Not Set')
AND ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
AND qmih_qmnum =qmel_qmnum
AND pn.dim_dateidMalfunctionStartDate <> dmt.dim_Dateid;

UPDATE tmp_insert_into_fact pn
SET pn.dim_notificationtypeid = nt.dim_notificationtypeid
from tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
INNER JOIN dim_notificationtype nt ON nt.notificationtypecode = ifnull(QMEL_QMART,'Not Set')
where nt.notificationtypecode = ifnull(QMEL_QMART,'Not Set')
AND ifnull(QMEL_QMNUM,'Not Set') = dd_notificationno
AND pn.dim_notificationtypeid <> nt.dim_notificationtypeid;

UPDATE tmp_insert_into_fact pn
SET pn.dim_plantID = pl.dim_plantID
from tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
INNER JOIN dim_plant pl ON pl.plantcode = ifnull(QMEL_ARBPLWERK,'Not Set')
where pn.dim_plantID <> pl.dim_plantID;

UPDATE tmp_insert_into_fact pn
SET pn.dim_dateidMalfunctionEndDate = dft.dim_Dateid
FROM tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
LEFT JOIN QMIH ON qmih_qmnum =qmel_qmnum INNER JOIN dim_Date dft ON dft.datevalue = ifnull(QMIH_AUSBS,'0001-01-01') and dft.companycode = 'Not Set'
AND dft.plantcode_factory = ifnull(QMEL_MAWERK,'Not Set')
AND pn.dim_dateidMalfunctionEndDate <> dft.dim_Dateid;

UPDATE tmp_insert_into_fact pn
SET pn.dim_dateidMalfunctionStartDate = dft.dim_Dateid
FROM tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
LEFT JOIN QMIH ON qmih_qmnum =qmel_qmnum INNER JOIN dim_Date dft ON dft.datevalue = ifnull(QMIH_AUSVN,'0001-01-01') and dft.companycode = 'Not Set'
AND dft.plantcode_factory = ifnull(QMEL_MAWERK,'Not Set')
AND pn.dim_dateidMalfunctionStartDate <> dft.dim_Dateid;

UPDATE tmp_insert_into_fact pn
SET pn.dim_activitycodetextid = da.dim_codetextid
FROM tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
LEFT JOIN QMMA ON qmma_qmnum = qmel_qmnum
INNER JOIN dim_codetext da ON da."catalog" = 'A' and da.CodeGroup in ('0000','0001') and da.code =qmma_mncod and da.version = '000001'
WHERE pn.dim_activitycodetextid <> da.dim_codetextid;

UPDATE tmp_insert_into_fact pn
SET pn.dim_causecodetextid = da.dim_codetextid
FROM tmp_insert_into_fact pn INNER JOIN (SELECT l.QMEL_QMNUM,r.QMUR_URCOD FROM QMEL l LEFT JOIN QMFE f on f.qmfe_qmnum = l.qmel_qmnum
LEFT JOIN QMUR r on ifnull(r.qmur_fenum,0) = ifnull(f.qmfe_fenum,0) and r.qmur_qmnum = l.qmel_qmnum
) l ON ifnull(l.QMEL_QMNUM,'Not Set') = pn.dd_notificationno
INNER JOIN dim_codetext da ON da."catalog" = '5' and da.CodeGroup in ('0000', '0001','ZPM-N002') and da.code =l.QMUR_URCOD and da.version = '000001'
WHERE pn.dim_causecodetextid <> da.dim_codetextid;

MERGE INTO tmp_insert_into_fact pn
USING (SELECT DISTINCT pn.dd_notificationno,pn.dd_notificationItemno,dm.dim_codetextid
FROM tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
LEFT JOIN qmfe ON qmfe_qmnum = qmel_qmnum AND qmfe_fenum = dd_notificationItemno
INNER JOIN dim_codetext dm ON dm."catalog" = 'C' and dm.CodeGroup = QMFE_FEGRP and dm.code =QMFE_FECOD and dm.version = '000001'
WHERE pn.dim_Damagecodetextid <> dm.dim_codetextid
) src
ON pn.dd_notificationno = src.dd_notificationno AND pn.dd_notificationItemno = src.dd_notificationItemno
WHEN MATCHED THEN UPDATE
SET pn.dim_Damagecodetextid = src.dim_codetextid; 

UPDATE tmp_insert_into_fact pn
SET pn.dim_codingcodetextid = dm.dim_codetextid
FROM tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
INNER JOIN dim_codetext dm ON dm."catalog" = 'D' and dm.CodeGroup  = 'ZPM-N001' and dm.code =QMEL_QMCOD and dm.version = '000001'
WHERE pn.dim_codingcodetextid <> dm.dim_codetextid;


MERGE INTO tmp_insert_into_fact pn
USING (SELECT DISTINCT pn.dd_notificationno,pn.dd_notificationItemno,dm.dim_codetextid
FROM tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
LEFT JOIN qmfe ON qmfe_qmnum = qmel_qmnum AND qmfe_fenum = dd_notificationItemno
INNER JOIN dim_codetext dm ON dm."catalog" = 'B' and dm.CodeGroup  = QMFE_OTGRP and dm.code = QMFE_OTEIL and dm.version = '000001'
WHERE pn.dim_objectPartcodetextid <> dm.dim_codetextid
) src
ON pn.dd_notificationno = src.dd_notificationno AND pn.dd_notificationItemno = src.dd_notificationItemno
WHEN MATCHED THEN UPDATE
SET pn.dim_objectPartcodetextid = src.dim_codetextid; 


MERGE INTO tmp_insert_into_fact pn
USING (SELECT DISTINCT pn.dd_notificationno,pn.dd_notificationItemno,dm.dim_codetextid
FROM tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
LEFT JOIN qmfe ON qmfe_qmnum = qmel_qmnum AND qmfe_fenum = dd_notificationItemno
INNER JOIN dim_codetext dm ON dm."catalog" = 'A' and dm.CodeGroup  = QMFE_FEGRP and dm.code = QMFE_FECOD and dm.version = '000001'
WHERE pn.dim_Problemcodetextid <> dm.dim_codetextid
) src
ON pn.dd_notificationno = src.dd_notificationno AND pn.dd_notificationItemno = src.dd_notificationItemno
WHEN MATCHED THEN UPDATE
SET pn.dim_Problemcodetextid = src.dim_codetextid; 

UPDATE tmp_insert_into_fact pn
SET pn.dim_ordermasterid = od.dim_ordermasterid
FROM tmp_insert_into_fact pn INNER JOIN QMEL ON ifnull(QMEL_QMNUM,'Not Set') = pn.dd_notificationno
INNER JOIN dim_ordermaster od ON od."order" = qmel_aufnr
WHERE pn.dim_ordermasterid <> od.dim_ordermasterid;

insert into fact_pmnotification(
    fact_pmnotificationid,
	dd_notificationno,
	dd_notificationItemno,
	dd_consecutiveactivityNo,
	dd_causeNo,
	dd_causecodegroup,
	dd_causeText,
	dd_notifitemSortNo,
	dd_notifitemtext,
	dim_notificationtypeid,
	dim_plantid,
	dim_dateidMalfunctionEndDate,
	dim_dateidMalfunctionStartDate,
	dd_externalRefNo,
	dd_notifChangedBy,
	dd_notifcreatedBy,
	dd_notifDescription,
	dd_notifPriority,
	dd_notifreportedBy,
	dd_referencenotification,
	dd_objectPart,
	dd_objectPartCode,
	dd_probemcodegrp,
	dd_acitivityText,
	dd_breakdownIndicator,
	dd_breakdownduration,
	dd_breakdowndurationunit,
	dd_catalogtype,
	dim_activitycodetextid,
	dim_causecodetextid,
	dim_Damagecodetextid,
	dim_codingcodetextid,
	dim_objectPartcodetextid,
	dim_Problemcodetextid,
	dim_ordermasterid)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_pmnotification') + row_number() over(order by '') as fact_pmnotificationid,
    a.dd_notificationno,
	a.dd_notificationItemno,
	a.dd_consecutiveactivityNo,
	a.dd_causeNo,
	a.dd_causecodegroup,
	a.dd_causeText,
	a.dd_notifitemSortNo,
	a.dd_notifitemtext,
	a.dim_notificationtypeid,
	a.dim_plantid,
	a.dim_dateidMalfunctionEndDate,
	a.dim_dateidMalfunctionStartDate,
	a.dd_externalRefNo,
	a.dd_notifChangedBy,
	a.dd_notifcreatedBy,
	a.dd_notifDescription,
	a.dd_notifPriority,
	a.dd_notifreportedBy,
	a.dd_referencenotification,
	a.dd_objectPart,
	a.dd_objectPartCode,
	a.dd_probemcodegrp,
	a.dd_acitivityText,
	a.dd_breakdownIndicator,
	a.dd_breakdownduration,
	a.dd_breakdowndurationunit,
	a.dd_catalogtype,
	a.dim_activitycodetextid,
	a.dim_causecodetextid,
	a.dim_Damagecodetextid,
	a.dim_codingcodetextid,
	a.dim_objectPartcodetextid,
	a.dim_Problemcodetextid,
	a.dim_ordermasterid
from tmp_insert_into_fact a
where not exists (select 1 from fact_pmnotification f1
		where f1.dd_notificationno = a.dd_notificationno
		and f1.dd_notificationItemno = a.dd_notificationItemno
		and f1.dd_consecutiveactivityNo = a.dd_consecutiveactivityNo
		and f1.dd_causeNo = a.dd_causeNo
		);

update 	fact_pmnotification fpm
set fpm.dim_ordermasterid = ifnull(dord.dim_ordermasterid,1)
from qmel q, dim_ordermaster dord, fact_pmnotification fpm
where fpm.dd_notificationno = q.qmel_qmnum
and dord."order" = q.qmel_aufnr
and fpm.dim_ordermasterid <> ifnull(dord.dim_ordermasterid,1);


/* Date fields - Use companycode from Plant dimension */
Update fact_pmnotification f
set f.dim_dateIDNotifChangedOn = ifnull(dt.dim_dateid,1)
	,dw_update_date = current_timestamp
from qmel, dim_date dt, dim_plant pl, fact_pmnotification f
where f.dim_plantid = pl.dim_plantid
and f.dd_notificationno = qmel_qmnum
and dt.datevalue = qmel_AEDAT
and dt.companycode = pl.companycode
AND dt.plantcode_factory = pl.plantcode
and f.dim_dateIDNotifChangedOn <> ifnull(dt.dim_dateid,1);

Update fact_pmnotification f
set f.dim_DateIDNotifCompletedOn = ifnull(dt.dim_dateid,1)
	,dw_update_date = current_timestamp
from qmel, dim_date dt, dim_plant pl, fact_pmnotification f
where f.dim_plantid = pl.dim_plantid
and f.dd_notificationno = qmel_qmnum
and dt.datevalue = qmel_QMDAB
and dt.companycode = pl.companycode
AND dt.plantcode_factory = pl.plantcode
and f.dim_DateIDNotifCompletedOn <> ifnull(dt.dim_dateid,1);

Update fact_pmnotification f
set f.dim_dateIDNotifCreatedOn = ifnull(dt.dim_dateid,1)
	,dw_update_date = current_timestamp
from qmel, dim_date dt, dim_plant pl, fact_pmnotification f
where f.dim_plantid = pl.dim_plantid
and f.dd_notificationno = qmel_qmnum
and dt.datevalue = qmel_ERDAT
AND dt.plantcode_factory = pl.plantcode
and dt.companycode = pl.companycode
and f.dim_dateIDNotifCreatedOn <> ifnull(dt.dim_dateid,1);

update fact_pmnotification f
set f.dim_dateidMalfunctionEndDate = ifnull(dft.dim_Dateid,1)
	,dw_update_date = current_timestamp
from dim_Date dft , dim_plant pl, qmih, fact_pmnotification f
where f.dim_plantid = pl.dim_plantid
and f.dd_notificationno = qmih_qmnum
and dft.datevalue = QMIH_AUSBS
and dft.companycode = pl.companycode
AND dft.plantcode_factory = pl.plantcode
and f.dim_dateidMalfunctionEndDate <> ifnull(dft.dim_Dateid,1);

update fact_pmnotification f
set f.dim_dateidMalfunctionStartDate = ifnull(dmt.dim_Dateid,1)
	,dw_update_date = current_timestamp
from dim_Date dmt , dim_plant pl, qmih, fact_pmnotification f
where f.dim_plantid = pl.dim_plantid
and f.dd_notificationno = qmih_qmnum
and dmt.datevalue = QMIH_AUSVN
and dmt.companycode = pl.companycode
AND dmt.plantcode_factory = pl.plantcode
and f.dim_dateidMalfunctionStartDate <> ifnull(dmt.dim_Dateid,1);



/* 24 March 2016 - Functional Location Dimension
update fact_pmnotification f
from dim_functionallocation d, QMIH
set f.dim_functionallocationid = ifnull(d.dim_functionallocationid,1)
where f.dd_notificationno = ifnull(QMIH_QMNUM,'Not Set')
and d.FunctonalLocation= ifnull(QMIH_BTPLN,'Not Set')
and d.AcctAssignment = ifnull(QMIH_ILOAN,'Not Set')
and f.dim_functionallocationid <> ifnull(d.dim_functionallocationid,1)
*/

/* 25March2016 Equipment Dimension */
/* 24 Aug 2016 Ambiguous replace fix*/
merge into fact_pmnotification f
using (select distinct fact_pmnotificationid,first_value(d.dim_equipmentid) over (partition by fact_pmnotificationid order by d.equipmentvalidfromdate desc) as dim_equipmentid
from dim_equipment d, QMIH, dim_date b, fact_pmnotification f
where f.dd_notificationno = ifnull(QMIH_QMNUM,'Not Set')
and d.EquipmentNo = ifnull(QMIH_EQUNR,'Not Set')
and f.dim_dateIDNotifCreatedOn = b.dim_dateid
and b.datevalue between equipmentvalidfromdate and decode(EquipmentValidtoDate,cast('0001-01-01' as date),cast('2099-12-31' as date),EquipmentValidtoDate)-1
and f.dim_equipmentid <> ifnull(d.dim_equipmentid,1)
) t on t.fact_pmnotificationid=f.fact_pmnotificationid
when matched then update set f.dim_equipmentid = ifnull(t.dim_equipmentid,1)
,dw_update_date = current_timestamp;


/* 13Apr2016 - Order Change Date as a date dimension*/
update fact_pmnotification f
set f.dim_dateIDOrderchangeDt = ifnull(dt.dim_dateid,1)
	,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt, fact_pmnotification f, dim_plant pl
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d.change_date = '1970-01-01' then '0001-01-01' else d.change_date end
and dt.companycode = d.company_code
AND f.dim_plantid = pl.dim_plantid
AND pl.plantcode = dt.plantcode_factory
and f.dim_dateIDOrderchangeDt <> dt.dim_dateid;
/*End of Changes 13Apr2016*/

/* 18April2016 - Updated logic for Functional Location Dimension */
update fact_pmnotification f
set f.dim_functionallocationid = ifnull(d.dim_functionallocationid,1)
        ,dw_update_date = current_timestamp
from qmih q, iloa i, dim_functionallocation d, fact_pmnotification f
where f.dd_notificationno = q.qmih_qmnum
and q.qmih_iloan = i.iloa_iloan
and i.iloa_iloan  = d.AcctAssignment
and i.iloa_tplnr = d.FunctonalLocation
and f.dim_functionallocationid <> ifnull(d.dim_functionallocationid,1);
/* End of changes 18Apr2016 */

/* 19Apr2016 - Additional fields */
/*Tech Completion Date as Date dimension */
update fact_pmnotification f
set f.dim_dateIDTechCompDt= ifnull(dt.dim_dateid,1)
	,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt, fact_pmnotification f, dim_plant pl
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d.Technical_completion= '1970-01-01' then '0001-01-01' else d.Technical_completion end
and dt.companycode = d.company_code
AND f.dim_plantid = pl.dim_plantid
AND pl.plantcode = dt.plantcode_factory
and f.dim_dateIDTechCompDt <> dt.dim_dateid;

/* Planner group Dimension*/
update fact_pmnotification f
set f.dim_plannergroupid = ifnull(d.dim_plannergroupid,1)
	, dw_update_date = current_timestamp
from dim_plannergroup d, qmih, fact_pmnotification f
where f.dd_notificationno = qmih_qmnum
and qmih_IWERK = d.MaintPlanningPlant
and qmih_ingrp = d.CustomerServiceGroup
and f.dim_plannergroupid <> ifnull(d.dim_plannergroupid,1);

/* Order Category Description*/
update fact_pmnotification f
set f.dim_productionordertypeid = ifnull(dtp.dim_productionordertypeid,1)
	,dw_update_date = current_timestamp
from dim_ordermaster d, dim_productionordertype dtp, fact_pmnotification f
where f.dim_ordermasterid = d.dim_ordermasterid
and d.Order_Type = dtp.TypeCode /* T003P_AUART*/
and f.dim_productionordertypeid <> ifnull(dtp.dim_productionordertypeid,1);

/* End of Changes 19Apr2016 */

/* 03May2016 - Modifications post UAT */


/* Breakdown Duration Converted */
update fact_pmnotification f
set f.dd_breakdowndurationconv = ifnull((dd_breakdownduration/convert(decimal (18,4),t.T006_ZAEHL))*(t.T006_NENNR*1.0000),0.0000)
from T006 t, fact_pmnotification f
where f.dd_breakdowndurationunit = t.T006_MSEHI
and f.dd_breakdownduration <> 0
and f.dd_breakdowndurationconv <> ifnull((dd_breakdownduration/convert(decimal (18,4),t.T006_ZAEHL))*(t.T006_NENNR*1.0000),0.0000);

/* EQUI_MATNR dimension */
update fact_pmnotification f
set f.dim_partidEquipment = ifnull(pr.dim_partid,1)
	,dw_update_date = current_timestamp
from dim_equipment d, dim_plant pl, dim_part pr, fact_pmnotification f
where f.dim_equipmentid = d.dim_equipmentid
and f.dim_plantid = pl.dim_plantid
and pr.partnumber = d.materialno and pr.plant = pl.plantcode
and f.dim_partidEquipment <> ifnull(pr.dim_partid,1);

/* EQUI_INBDT, EQUZ_DATBI as date dimensions */

update fact_pmnotification f
set f.dim_dateideqpvalidtodt = ifnull(vdt.dim_dateid,1), dw_update_date  = current_timestamp
from dim_equipment d, dim_plant pl, dim_date vdt, fact_pmnotification f
where  f.dim_equipmentid = d.dim_equipmentid
and f.dim_plantid = pl.dim_plantid
and vdt.datevalue = ifnull(d.EquipmentValidtoDate,'0001-01-01')
and vdt.companycode = pl.companycode
AND pl.plantcode = vdt.plantcode_factory
and f.dim_dateideqpvalidtodt <>  ifnull(vdt.dim_dateid,1);

update fact_pmnotification f
set f.dim_dateideqpstartdt = ifnull(stdt.dim_dateid,1), dw_update_date = current_timestamp
from dim_equipment d, dim_plant pl, dim_date stdt, fact_pmnotification f
where f.dim_equipmentid = d.dim_equipmentid
and f.dim_plantid = pl.dim_plantid
and stdt.datevalue = ifnull(d.EquipmentStartUpDate,'0001-01-01')
and stdt.companycode = pl.companycode
AND pl.plantcode = stdt.plantcode_factory
and f.dim_dateideqpstartdt <>  ifnull(stdt.dim_dateid,1);


/* EQUI_ELIEF Vendor Dimension */
update fact_pmnotification f
set f.dim_vendoridequipment = ifnull(v.dim_vendorid,1), dw_update_date  = current_timestamp
from  dim_equipment d, dim_vendor v, fact_pmnotification f
where f.dim_equipmentid = d.dim_equipmentid
and d.EquipmentVendor = v.vendornumber
and f.dim_vendoridequipment <> ifnull(v.dim_vendorid,1);


/* ILOA_KOSTL - costcenter dimension */
merge into  fact_pmnotification f
using (select distinct f.fact_pmnotificationid, first_value(c.dim_costcenterid) over (partition by f.fact_pmnotificationid order by f.fact_pmnotificationid) as  dim_costcenterid
from dim_costcenter c, dim_functionallocation floc, fact_pmnotification f
where f.dim_functionallocationid = floc.dim_functionallocationid
and floc.FLOCCostCenter = c.code) t
on f.fact_pmnotificationid=t.fact_pmnotificationid
when matched then update set f.dim_costcenteridfloc = ifnull(t.dim_costcenterid,1)
where  f.dim_costcenteridfloc <> ifnull(t.dim_costcenterid,1);

/* AUFK_IDAT3, AUFK_ERDAT,AIFK_IDAT1 */
update fact_pmnotification f
set f.dim_dateIDOrdercloseDt = ifnull(dt.dim_dateid,1)
        ,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt, fact_pmnotification f, dim_plant pl
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d."close" = '1970-01-01' then '0001-01-01' else d."close" end
and dt.companycode = d.company_code
AND pl.dim_plantid = f.dim_plantid
AND pl.plantcode = dt.plantcode_factory
and f.dim_dateIDOrdercloseDt <> dt.dim_dateid;

update fact_pmnotification f
set f.dim_dateIDOrderCreatedDt = ifnull(dt.dim_dateid,1)
        ,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt, fact_pmnotification f, dim_plant pl
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d.Created_on = '1970-01-01' then '0001-01-01' else d.Created_on end
and dt.companycode = d.company_code
AND pl.dim_plantid = f.dim_plantid
AND pl.plantcode = dt.plantcode_factory
and f.dim_dateIDOrderCreatedDt <> dt.dim_dateid;

update fact_pmnotification f
set f.dim_dateIDOrderReleasedDt = ifnull(dt.dim_dateid,1)
        ,dw_update_date = current_timestamp
from  dim_ordermaster d, dim_Date dt, fact_pmnotification f, dim_plant pl
where f.dim_ordermasterid = d.dim_ordermasterid
and dt.datevalue = case when d.release_date = '1970-01-01' then '0001-01-01' else d.release_date end
and dt.companycode = d.company_code
AND pl.dim_plantid = f.dim_plantid
AND pl.plantcode = dt.plantcode_factory
and f.dim_dateIDOrderReleasedDt <> dt.dim_dateid;

/* AUFK - PRCTR - Profit Center Dimension */
update fact_pmnotification f
set f.dim_profitcenterid = ifnull(p.dim_profitcenterid,1), dw_update_date = current_timestamp
from dim_ordermaster o, dim_profitcenter p, fact_pmnotification f
where f.dim_ordermasterid = o.dim_ordermasterid
and o.profitcenter = p.profitcentercode
and f.dim_profitcenterid <> ifnull(p.dim_profitcenterid,1);

/* MHIS_NPLDA */
update fact_pmnotification f
set f.dim_DateidMaintPlanDueDt = ifnull(dt.dim_Dateid,1)
,dw_update_date = current_timestamp
from dim_ordermaster d, afih , (select max (MHIS_ZAEHL), MHIS_WARPL,MHIS_ABNUM,MHIS_NPLDA 
         from mhis group by MHIS_WARPL,MHIS_ABNUM,MHIS_NPLDA ) mhis, dim_date dt, fact_pmnotification f, dim_plant pl
where f.dim_ordermasterid = d.dim_ordermasterid
and d."order" = afih_aufnr
and AFIH_WARPL = mhis.MHIS_WARPL
and AFIH_ABNUM = mhis.MHIS_ABNUM
and ifnull(mhis.MHIS_NPLDA,'0001-01-01') = dt.datevalue
and dt.companycode = d.company_code
AND pl.dim_plantid = f.dim_plantid
AND pl.plantcode = dt.plantcode_factory
and f.dim_DateidMaintPlanDueDt <> ifnull(dt.dim_Dateid,1);

/* AFIH_BAUTL - Assembly Material */
update fact_pmnotification f
set f.dim_partidassembly =  ifnull(p.dim_partid,1), dw_update_date  = current_timestamp
from dim_part p, dim_ordermaster o, afih, fact_pmnotification f
where f.dim_ordermasterid  = o.dim_ordermasterid
and afih_aufnr = o."order"
and o.plant = p.plant
and p.partnumber  = AFIH_BAUTL
and f.dim_partidassembly <> ifnull(p.dim_partid,1);

/* Text/Description dimesions */

/* Eqp Category*/
update fact_pmnotification f
set f.dim_equipmentcategoryid = ifnull(t.dim_equipmentcategoryid,1),dw_update_date = current_timestamp
from dim_equipment d, dim_equipmentcategory t, fact_pmnotification f
where f.dim_equipmentid = d.dim_equipmentid
and d.EquipmentCategory = t.EquipmentCategory
and f.dim_equipmentcategoryid <> ifnull(t.dim_equipmentcategoryid,1);

/* TechObjectAuthGroup */
update fact_pmnotification f
set f.dim_techobjectauthgroupid = ifnull(t.dim_techobjectauthgroupid,1),dw_update_date = current_timestamp
from dim_equipment d, dim_techobjectauthgroup t, fact_pmnotification f
where f.dim_equipmentid = d.dim_equipmentid
and d.TechObjectAuthGroup = t.TechObjectAuthGroup
and f.dim_techobjectauthgroupid <> ifnull(t.dim_techobjectauthgroupid,1);

/* Tech object type */
update fact_pmnotification f
set f.dim_techobjecttypeid = ifnull(t.dim_techobjecttypeid,1),dw_update_date = current_timestamp
from dim_equipment d, dim_techobjecttype t, fact_pmnotification f
where f.dim_equipmentid = d.dim_equipmentid
and d.TechnicalObjType = t.TechObjectType
and f.dim_techobjecttypeid <> ifnull(t.dim_techobjecttypeid,1);

/* FLOC ABC Indicator */
update fact_pmnotification f
set f.dim_flocabcindicatorid = ifnull(t.dim_flocabcindicatorid,1),dw_update_date = current_timestamp
from dim_functionallocation d, dim_flocabcindicator t, fact_pmnotification f
where f.dim_functionallocationid = d.dim_functionallocationid
and d.FLOCABCIndicator = t.FLOCABCIndicator
and f.dim_flocabcindicatorid <> ifnull(t.dim_flocabcindicatorid,1);

/* Maintenance Activity Type */
update fact_pmnotification f
set f.dim_mainactivitytypeid = ifnull(m.dim_mainactivitytypeid,1)
from  dim_ordermaster d, afih ,dim_mainactivitytype m, fact_pmnotification f
where f.dim_ordermasterid = d.dim_ordermasterid
and trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and ifnull(afih_ilart,'Not Set') = m.mainactivitytype
and f.dim_mainactivitytypeid <> ifnull(m.dim_mainactivitytypeid,1);

/* FLOC TEch objec Auth Desc */
update fact_pmnotification f
set f.dim_techobjectauthgroupidfloc = ifnull(t.dim_techobjectauthgroupid,1),dw_update_date = current_timestamp
from dim_functionallocation d, dim_techobjectauthgroup t, fact_pmnotification f
where f.dim_functionallocationid = d.dim_functionallocationid
and d.TechObjectAuthGroup = t.TechObjectAuthGroup
and f.dim_techobjectauthgroupidfloc <> ifnull(t.dim_techobjectauthgroupid,1);

/* Plant Section */
update fact_pmnotification f
set f.dim_plantsectionid = ifnull(p.dim_plantsectionid,1), dw_update_date = current_timestamp
from dim_functionallocation floc, dim_plantsection p, fact_pmnotification f
where f.dim_functionallocationid = floc.dim_functionallocationid
and floc.plantsection = p.plantsection
and floc.FLOCPlanningPlant = p.Plant
and f.dim_plantsectionid <> ifnull(p.dim_plantsectionid,1);

/* System condition */
update fact_pmnotification f
set f.dim_systemconditionid = ifnull(s.dim_systemconditionid,1)
from dim_ordermaster o, dim_systemcondition s, fact_pmnotification f
where f.dim_ordermasterid = o.dim_ordermasterid
and o.systemcondition = s.systemcondition
and f.dim_systemconditionid <> ifnull(s.dim_systemconditionid,1);

/* End of Text dimensions*/

/* End of Changes 03May2016 */

/*04May2016 - Cost Center Dimension for AUFK-KOSTV */
MERGE INTO fact_pmnotification fact
USING (
SELECT DISTINCT f.fact_pmnotificationid, FIRST_VALUE(cx.dim_costcenterid) OVER (PARTITION BY CX.CODE ORDER BY CX.VALIDTO DESC) AS dim_costcenterid
from  dim_ordermaster d, dim_costcenter cx, fact_pmnotification f
where f.dim_ordermasterid = d.dim_ordermasterid
and d.Responsible_CCtr  = cx.code
and f.dim_Costcenteridresponsible <> ifnull(cx.dim_costcenterid,1)
) src
ON fact.fact_pmnotificationid = src.fact_pmnotificationid
WHEN MATCHED THEN UPDATE
SET fact.dim_Costcenteridresponsible = src.dim_costcenterid;

/* Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */


UPDATE fact_pmnotification f
SET f.std_exchangerate_dateid = dt.dim_dateid
	,dw_update_date = current_timestamp
FROM fact_pmnotification f INNER JOIN dim_plant p ON f.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode AND dt.plantcode_factory = p.plantcode
WHERE dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

/* END Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

/* To capture the last refreshed date time infomration. Should be the last statement in the processing script */
Update fact_pmnotification
set dd_lastrefreshdate = current_timestamp
where dd_lastrefreshdate <> current_timestamp;

/* Notitication Priority */
MERGE INTO fact_pmnotification fact
USING (
SELECT DISTINCT f.fact_pmnotificationid, FIRST_VALUE(d.dim_notificationpriorityid) OVER (PARTITION BY d.notificationprioritycode ORDER BY d.dim_notificationpriorityid ASC) AS dim_notificationpriorityid
from dim_notificationpriority d, fact_pmnotification f
where f.dd_notifPriority = d.notificationprioritycode
and f.dim_notificationpriorityid <> ifnull(d.dim_notificationpriorityid,1)
) src
ON fact.fact_pmnotificationid = src.fact_pmnotificationid
WHEN MATCHED THEN UPDATE
SET fact.dim_notificationpriorityid = src.dim_notificationpriorityid;

/* Order Plant */
update fact_pmnotification f
set f.dim_PlantidOrder = ifnull(p.dim_plantid,1)
from dim_ordermaster o, dim_plant p, fact_pmnotification f
where f.dim_ordermasterid = o.dim_ordermasterid
and o.plant = p.plantcode
and f.dim_PlantidOrder <> ifnull(p.dim_plantid,1);

update fact_pmnotification fpm
set fpm.dim_notificationtypeid = nt.dim_notificationtypeid
from dim_notificationtype nt, QMEL q, fact_pmnotification fpm
where  nt.notificationtypecode = ifnull(q.QMEL_QMART,'Not Set')
and q.qmel_qmnum = fpm.dd_notificationno
and fpm.dim_notificationtypeid <> nt.dim_notificationtypeid;

update fact_pmnotification fpm
set fpm.dim_notificationpriorityid = nt.dim_notificationpriorityid
from dim_notificationpriority nt, QMEL q, fact_pmnotification fpm
where  nt.notificationprioritytypecode = ifnull(q.QMEL_ARTPR,'Not Set')
and nt.notificationprioritycode = ifnull(q.QMEL_PRIOK,'Not Set')
and q.qmel_qmnum = fpm.dd_notificationno
and fpm.dim_notificationpriorityid <> nt.dim_notificationpriorityid;

update fact_pmnotification fpm
set fpm.dim_ordernotificationpriorityid = nt.dim_notificationpriorityid
from dim_notificationpriority nt, AFIH q, dim_ordermaster dor, fact_pmnotification fpm
where  nt.notificationprioritytypecode = ifnull(q.AFIH_ARTPR,'Not Set')
and nt.notificationprioritycode = ifnull(q.AFIH_PRIOK,'Not Set')
and dor.dim_ordermasterid = fpm.dim_ordermasterid
and q.afih_aufnr = dor."order"
and fpm.dim_ordernotificationpriorityid <> nt.dim_notificationpriorityid;

update fact_pmnotification e
set dim_techobjectauthgrouporderid = ifnull(d.dim_techobjectauthgroupid,1) ,
    dw_update_date = current_timestamp
from afih a,
     MPLA b,
     dim_ordermaster c,
     dim_techobjectauthgroup d, fact_pmnotification e
where trim(leading '0' from c."order") = trim(leading '0' from afih_aufnr)
   and e.dim_ordermasterid = c.dim_ordermasterid
   and d.techobjectauthgroup = b.MPLA_BEGRU
   and a.AFIH_WARPL = MPLA_WARPL
   and dim_techobjectauthgrouporderid <>  ifnull(d.dim_techobjectauthgroupid,1);

/*Alin APP-9909 12 jULY 2018*/
merge into fact_pmnotification f
using(
select distinct fact_pmnotificationid, max(ifnull(d.dim_workcenterid, 1)) as dim_workcenterid
from fact_pmnotification f, QMEL q, CRHD c, dim_workcenter d
where ifnull(QMEL_QMNUM,'Not Set') = f.dd_notificationno
and q.QMEL_ARBPL = c.CRHD_OBJID
and d.objectid = cast(c.CRHD_OBJID as varchar(8))
group by fact_pmnotificationid
) t
on t.fact_pmnotificationid = f.fact_pmnotificationid
when matched then update
set f.dim_workcenterid = t.dim_workcenterid;

/*Alin 24 July 2018 APP-9909*/
/*dim_PMNotifStatuses embeded in pmnotif script, following the example for pmstatuses in pmorder sa*/
DROP TABLE IF EXISTS tmp_dim_notificationuserstatus3;

CREATE TABLE  tmp_dim_notificationuserstatus3 AS 
SELECT distinct 
	QMEL_QMNUM as dd_notificationno,
	'Not Set' as Notification_Approved,
	'Not Set' as Notified,
	'Not Set' as Notification_Not_Approved,
	'Not Set' as Discontinued
FROM JEST_JSTO_QMEL
where JEST_INACT is null; 
	
UPDATE   tmp_dim_notificationuserstatus3 t
SET Notification_Approved = 'X'
FROM JEST_JSTO_QMEL q, TJ30T t3,  tmp_dim_notificationuserstatus3 t
WHERE q.QMEL_QMNUM = dd_notificationno
AND t3.TJ30T_TXT04 = 'APPR'
AND q.JEST_STAT = 'E0030'
AND q.JSTO_STSMA = t3.TJ30T_STSMA
AND q.JSTO_STSMA like 'ZPM%'
AND JEST_INACT is null; 

UPDATE   tmp_dim_notificationuserstatus3 t
SET Notified = 'X'
FROM JEST_JSTO_QMEL q, TJ30T t3,  tmp_dim_notificationuserstatus3 t
WHERE q.QMEL_QMNUM = dd_notificationno
AND t3.TJ30T_TXT04 = 'NOTF'
AND q.JEST_STAT = 'E0028'
AND q.JSTO_STSMA = t3.TJ30T_STSMA
AND q.JSTO_STSMA like 'ZPM%'
AND JEST_INACT is null; 

UPDATE   tmp_dim_notificationuserstatus3 t
SET Notification_Not_Approved = 'X'
FROM JEST_JSTO_QMEL q, TJ30T t3,  tmp_dim_notificationuserstatus3 t
WHERE q.QMEL_QMNUM = dd_notificationno
AND t3.TJ30T_TXT04 = 'NOAP'
AND q.JEST_STAT = 'E0029'
AND q.JSTO_STSMA = t3.TJ30T_STSMA
AND q.JSTO_STSMA like 'ZPM%'
AND JEST_INACT is null; 

UPDATE   tmp_dim_notificationuserstatus3 t
SET Discontinued = 'X'
FROM JEST_JSTO_QMEL q, TJ30T t3,  tmp_dim_notificationuserstatus3 t
WHERE q.QMEL_QMNUM = dd_notificationno
AND t3.TJ30T_TXT04 = 'DISC'
AND q.JEST_STAT = 'E0023'
AND q.JSTO_STSMA = t3.TJ30T_STSMA
AND q.JSTO_STSMA like 'ZPM%'
AND JEST_INACT is null; 


insert into dim_PMNotifStatuses
	(
	dim_PMNotifStatusesID,
	Notification_Approved,
	Notified,
	Notification_Not_Approved,
	Discontinued
	)
select 1, 'Not Set','Not Set','Not Set','Not Set'

from ( SELECT 1 )  AS t
where not exists ( select 1 from dim_PMNotifStatuses where dim_PMNotifStatusesID = 1);

delete from number_fountain m where m.table_name = 'dim_PMNotifStatuses';

insert into number_fountain
select  'dim_PMNotifStatuses',
        ifnull(max(d.dim_PMNotifStatusesID),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_PMNotifStatuses d
where d.dim_PMNotifStatusesid <> 1;

insert into dim_PMNotifStatuses
(	
	dim_PMNotifStatusesid,
	Notification_Approved,
	Notified,
	Notification_Not_Approved,
	Discontinued
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_PMNotifStatuses') + row_number() over(order by '') as dim_PMNotifStatusesID, 
	t.* from (select distinct 
		Notification_Approved,
	 	Notified,
		Notification_Not_Approved,
		Discontinued
		from tmp_dim_notificationuserstatus3 t3
		where not exists (select 1 from dim_PMNotifStatuses pm
		where
		pm.Notification_Approved = t3.Notification_Approved and
		pm.Notified = t3.Notified and
		pm.Notification_Not_Approved = t3.Notification_Not_Approved and
		pm.Discontinued = t3.Discontinued
		) )
		 t;
 
	   
UPDATE fact_pmnotification fpm
SET fpm.dim_PMNotifStatusesID = pm.dim_PMNotifStatusesID
	,dw_update_date = current_timestamp 	
FROM
	dim_PMNotifStatuses pm,
	tmp_dim_notificationuserstatus3 t3 , fact_pmnotification fpm	
WHERE  
	t3.dd_notificationno = fpm.dd_notificationno and

	pm.Notification_Approved = t3.Notification_Approved and
	pm.Notified = t3.Notified and
	pm.Notification_Not_Approved = t3.Notification_Not_Approved and
	pm.Discontinued = t3.Discontinued;
/*Alin App-7223 22 Oct*/
merge into fact_pmnotification f
using(
select distinct f.fact_pmnotificationid, c.dim_costcenterid
from fact_pmnotification f, dim_ordermaster d, AUFK a, dim_costcenter c
where f.dim_ordermasterid = d.dim_ordermasterid
and ifnull(a.AUFK_AUFNR, 'Not Set') = d."order"
and c.code = ifnull(a.AUFK_KOSTL, 'Not Set')
and c.ValidTo = '9999-12-31'
) t
on t.fact_pmnotificationid = f.fact_pmnotificationid
when matched then update
set f.dim_costcenterid = t.dim_costcenterid
where f.dim_costcenterid <> t.dim_costcenterid;

merge into fact_pmnotification f
using(
select distinct fact_pmnotificationid, ifnull(c.CSKS_VERAK, 'Not Set') as pers_resp
from fact_pmnotification f, dim_ordermaster d, AUFK a, CSKS c
where f.dim_ordermasterid = d.dim_ordermasterid
and ifnull(a.AUFK_AUFNR, 'Not Set') = d."order"
and a.AUFK_KOSTL = c.CSKS_KOSTL
and c.CSKS_DATBI = '9999-12-31'
) t
on t.fact_pmnotificationid = f.fact_pmnotificationid
when matched then update
set f.dd_person_responsible = t.pers_resp
where f.dd_person_responsible <> t.pers_resp;