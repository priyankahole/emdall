


/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   11 Jan 2014      Lokesh    1.15              Removed the incorrect last 2 queries. In sync with prod/app now */
/*   11 Jan 2014      Lokesh    1.14              Add <> in updates for performance      */
/******************************************************************************************************************/


/*30 Mar 2016 Georgiana adding creation statement for mdtb_tmp_q01*/

Drop table if exists mdtb_tmp_q01;
Create table mdtb_tmp_q01 as Select m.*,(ifnull(MDTB_UMDAT,MDTB_DAT01) -  MDTB_DAT02) leadcolupd from  mdtb m Where  ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL ;

/* 30 Mar 2016 End of Changes*/

UPDATE fact_mrp m
    SET m.Dim_ItemCategoryid = ic.Dim_ItemCategoryid
        ,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t, dim_mrpelement me,
        dim_vendor v, dim_itemcategory ic, dim_documenttype dt, dim_consumptiontype ct, dim_part dp, dim_vendor fv, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND m.dd_DocumentNo = pr.EBAN_BANFN
        AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;


UPDATE fact_mrp m
    SET         m.Dim_Vendorid = v.Dim_Vendorid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr, tmp_mdkp_mdtb_distinct t,dim_mrpelement me,
        dim_vendor v, dim_itemcategory ic, dim_documenttype dt, dim_consumptiontype ct, dim_part dp, dim_vendor fv, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND m.dd_DocumentNo = pr.EBAN_BANFN
        AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND m.Dim_Vendorid <> v.Dim_Vendorid;

UPDATE fact_mrp m
    SET m.Dim_FixedVendorid = fv.Dim_Vendorid
        ,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr, tmp_mdkp_mdtb_distinct t,dim_mrpelement me,
        dim_vendor v, dim_itemcategory ic, dim_documenttype dt, dim_consumptiontype ct, dim_part dp, dim_vendor fv, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND m.dd_DocumentNo = pr.EBAN_BANFN
        AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND m.Dim_FixedVendorid <> fv.Dim_Vendorid;

UPDATE fact_mrp m
    SET m.Dim_DocumentTypeid = dt.Dim_DocumentTypeid
        ,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr, tmp_mdkp_mdtb_distinct t,dim_mrpelement me,
        dim_vendor v, dim_itemcategory ic, dim_documenttype dt, dim_consumptiontype ct, dim_part dp, dim_vendor fv, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND m.dd_DocumentNo = pr.EBAN_BANFN
        AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND m.Dim_DocumentTypeid <> dt.Dim_DocumentTypeid;


UPDATE fact_mrp m
    SET m.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
        ,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr, tmp_mdkp_mdtb_distinct t,dim_mrpelement me,
        dim_vendor v, dim_itemcategory ic, dim_documenttype dt, dim_consumptiontype ct, dim_part dp, dim_vendor fv, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
        AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
        AND me.Dim_MRPElementID = m.Dim_MRPElementid
        AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
        AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND m.dd_DocumentNo = pr.EBAN_BANFN
        AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND m.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;


drop table if exists eban_p00;
Create table eban_p00 as Select e.*, ifnull(EBAN_FLIEF, EBAN_LIFNR) eban_colupd from eban e where ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL;
Drop table if exists fact_mrp_del_p00;
Create table fact_mrp_del_p00 as Select fact_mrpid,ct_leadTimeVariance,(ifnull(MDTB_UMDAT,MDTB_DAT01) -  MDTB_DAT02) leadcolupd,t.MDKP_MATNR,t.MDKP_PLWRK,eban_colupd,EBAN_BANFN,EBAN_BNFPO,dp.LeadTime
From fact_mrp m, eban_p00 pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me,
        dim_vendor v, dim_itemcategory ic, dim_documenttype dt, dim_consumptiontype ct, dim_part dp, dim_vendor fv, dim_plant pl, tmpvariable_00e
 WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
       AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
       AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
       AND me.Dim_MRPElementID = m.Dim_MRPElementid
       AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
       AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND m.dd_DocumentNo = pr.EBAN_BANFN
        AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
	AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL;


	Update fact_mrp_del_p00 pdo
Set pdo.ct_leadTimeVariance =   ifnull((pdo.leadcolupd) - glt.v_leadTime, -1 * pdo.LeadTime)
FROM fact_mrp_del_p00 pdo
              LEFT JOIN ( SELECT t0.* from tmp_getLeadTime t0
	                      where    t0.fact_script_name = 'bi_populate_mrp_fact'
                              AND t0.DocumentType=  'PR') glt 
	      ON glt.pPart =pdo.MDKP_MATNR 
              AND glt.pPlant =  pdo.MDKP_PLWRK
              AND glt.pVendor =   pdo.eban_colupd
              AND glt.DocumentNumber =  pdo.eban_BANFN
              AND glt.DocumentLineNumber =  pdo.eban_BNFPO

AND pdo.ct_leadTimeVariance <>   ifnull((pdo.leadcolupd) - glt.v_leadTime, -1 * pdo.LeadTime);



Update fact_mrp m
Set m.ct_leadTimeVariance = pdo.ct_leadTimeVariance
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_mrp_del_p00 pdo,fact_mrp m
Where m.fact_mrpid = pdo.fact_mrpid
AND m.ct_leadTimeVariance <> pdo.ct_leadTimeVariance;



		
/* Update curr/exchg rate from eban */
UPDATE fact_mrp m
   SET dim_CurrencyID = 1
FROM
       eban pr,tmp_mdkp_mdtb_distinct t, tmpvariable_00e, dim_mrpelement me, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid
AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND m.dim_CurrencyID <> 1;

UPDATE fact_mrp m
    SET dim_CurrencyID = cur.dim_CurrencyID 
FROM dim_plant pl,
	dim_currency cur,dim_company dc , fact_mrp m
	WHERE     m.Dim_ActionStateid = 2
        AND pl.dim_plantid = m.dim_plantid
	AND dc.companycode = pl.companycode
	And dc.currency = cur.currencycode
	and dc.RowIsCurrent = 1
	AND m.dim_CurrencyID <> cur.dim_CurrencyID;		
		

UPDATE fact_mrp m
   SET dim_CurrencyID_GBL =ifnull(cur.dim_CurrencyID,1)
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_mrp m, tmpvariable_00e
LEFT JOIN dim_currency cur on pGlobalCurrency = cur.currencycode
 WHERE     m.Dim_ActionStateid = 2
       AND ifnull(m.dim_CurrencyID_GBL, -1) <> ifnull(cur.dim_CurrencyID,1);


					   
UPDATE fact_mrp m
   SET dim_CurrencyID_TRA = ifnull(cur.dim_CurrencyID,1)
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmpvariable_00e, fact_mrp m
INNER JOIN eban pr on  m.dd_DocumentNo = pr.EBAN_BANFN AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
LEFT JOIN dim_currency cur on pr.EBAN_WAERS = cur.currencycode
 WHERE     m.Dim_ActionStateid = 2
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
       AND ifnull(m.dim_CurrencyID_TRA, -1) <> ifnull(cur.dim_CurrencyID,1);	


merge into fact_mrp m 
using (select distinct m.fact_mrpid,exchangeRate
From  eban pr, tmpvariable_00e, tmp_getExchangeRate1 ex, dim_company dc, fact_mrp m
WHERE m.Dim_ActionStateid = 2
 AND m.dd_DocumentNo = pr.EBAN_BANFN
 AND m.dd_DocumentItemNo = pr.EBAN_BNFPO 
AND m.dim_companyid = dc.dim_companyid 
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL 
AND tmpTotalCount > 0 
AND pFromCurrency = pr.EBAN_WAERS 
AND pToCurrency = dc.currency 
AND pFromExchangeRate = 0 
AND pDate = pr.EBAN_BADAT 
AND fact_script_name = 'bi_populate_mrp_fact') t
on t.fact_mrpid=m.fact_mrpid
when matched then update set amt_ExchangeRate = t.exchangeRate
where ifnull(m.amt_ExchangeRate,-1) <> t.exchangeRate;

		
merge into fact_mrp m 
using (select distinct m.fact_mrpid,ex.exchangeRate
From  eban pr, tmpvariable_00e, tmp_getExchangeRate1 ex,fact_mrp m
WHERE     m.Dim_ActionStateid = 2
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND  tmpTotalCount > 0 and pFromCurrency = pr.EBAN_WAERS
AND pToCurrency = pGlobalCurrency
AND pFromExchangeRate = 0
AND pDate = current_date
AND fact_script_name = 'bi_populate_mrp_fact') t
on t.fact_mrpid=m.fact_mrpid
when matched then update set amt_ExchangeRate_GBL = t.exchangeRate
where ifnull(m.amt_ExchangeRate_GBL,-1) <> t.exchangeRate;

		

UPDATE fact_mrp m
SET amt_ExtendedPrice = ifnull(pr.EBAN_MENGE * EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 then null else EBAN_PEINH end), 1), 0)  
	/* LK: EBAN_PREIS is in tran currency ( EBAN_WAERS ), no need to use exchg rates here */	
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmpvariable_00e,fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND ifnull(m.amt_ExtendedPrice,-1) <> ifnull(pr.EBAN_MENGE * EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 then null else EBAN_PEINH end), 1), 0);		
		
		
UPDATE fact_mrp m
SET amt_ExtendedPrice_GBL = ifnull(pr.EBAN_MENGE * EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 then null else EBAN_PEINH end), 1), 0) * amt_ExchangeRate_GBL
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmpvariable_00e, fact_mrp m
WHERE     m.Dim_ActionStateid = 2
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND ifnull(m.amt_ExtendedPrice_GBL,-1) <> ifnull(pr.EBAN_MENGE * EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 then null else EBAN_PEINH end), 1), 0) * amt_ExchangeRate_GBL;	
	
UPDATE fact_mrp m
    SET Dim_DateidReschedule = ifnull(od.dim_dateid, 1)
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  fact_mrp m 
INNER JOIN EBAN pr on m.dd_DocumentNo = pr.EBAN_BANFN AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
Inner JOIN tmp_mdkp_mdtb_distinct t on t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
INNER JOIN dim_plant pl on m.dim_plantid = pl.dim_plantid
INNER JOIN dim_date od on od.DateValue = t.MDTB_UMDAT AND od.CompanyCode = pl.CompanyCode and od.plantcode_factory=pl.plantcode
CROSS JOIN tmpvariable_00e
WHERE  tmpTotalCount > 0 and m.Dim_ActionStateid = 2
 AND t.MDTB_MNG01 = m.ct_QtyMRP
 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
 AND Dim_DateidReschedule <> ifnull(od.dim_dateid, 1);
 
UPDATE fact_mrp m
    SET
        m.Dim_DateidActionRequired = dt.Dim_Dateid
       ,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_date dt, tmpvariable_00e, dim_company c, fact_mrp m,dim_plant pl
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP and m.dim_plantid=pl.dim_plantid
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
        AND m.dim_companyid = c.dim_companyid AND c.companycode = dt.companycode and dt.plantcode_factory=pl.plantcode
        AND dt.datevalue = current_date 
        AND m.Dim_DateidActionRequired <> dt.Dim_Dateid;
		
		
/* Octavian: Every Angle updates from MDKP done similar to the above ones only for actionstateid = 2 */
UPDATE fact_mrp m
    SET m.dd_exceptiongroup1 = ifnull(t.MDKP_AUSZ1,'Not Set')
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_plant pl, tmpvariable_00e,fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dim_plantid = pl.dim_plantid
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND m.dd_exceptiongroup1 <> ifnull(t.MDKP_AUSZ1,'Not Set');	

UPDATE fact_mrp m
    SET m.dd_exceptiongroup2 = ifnull(t.MDKP_AUSZ2,'Not Set')
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_plant pl, tmpvariable_00e,fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dim_plantid = pl.dim_plantid
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND m.dd_exceptiongroup2 <> ifnull(t.MDKP_AUSZ2,'Not Set');	

UPDATE fact_mrp m
    SET m.dd_exceptiongroup3 = ifnull(t.MDKP_AUSZ3,'Not Set')
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dim_plantid = pl.dim_plantid
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND m.dd_exceptiongroup3 <> ifnull(t.MDKP_AUSZ3,'Not Set');	

UPDATE fact_mrp m
    SET m.dd_exceptiongroup4 = ifnull(t.MDKP_AUSZ4,'Not Set')
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dim_plantid = pl.dim_plantid
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND m.dd_exceptiongroup4 <> ifnull(t.MDKP_AUSZ4,'Not Set');	

UPDATE fact_mrp m
    SET m.dd_exceptiongroup5 = ifnull(t.MDKP_AUSZ5,'Not Set')
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dim_plantid = pl.dim_plantid
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND m.dd_exceptiongroup5 <> ifnull(t.MDKP_AUSZ5,'Not Set');	


UPDATE fact_mrp m
    SET m.dd_exceptiongroup6 = ifnull(t.MDKP_AUSZ6,'Not Set')
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_plant pl, tmpvariable_00e,fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dim_plantid = pl.dim_plantid
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND m.dd_exceptiongroup6 <> ifnull(t.MDKP_AUSZ6,'Not Set');	


UPDATE fact_mrp m
    SET m.dd_exceptiongroup7 = ifnull(t.MDKP_AUSZ7,'Not Set')
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_plant pl, tmpvariable_00e, fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dim_plantid = pl.dim_plantid
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND m.dd_exceptiongroup7 <> ifnull(t.MDKP_AUSZ7,'Not Set');	

UPDATE fact_mrp m
    SET m.dd_exceptiongroup8 = ifnull(t.MDKP_AUSZ8,'Not Set')
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  eban pr,tmp_mdkp_mdtb_distinct t,dim_mrpelement me, dim_plant pl, tmpvariable_00e,fact_mrp m
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
AND t.MDTB_DELNR = m.dd_DocumentNo AND t.MDTB_DELPS = m.dd_DocumentItemNo AND t.MDTB_DELET = m.dd_ScheduleNo
AND t.dim_plantid = m.dim_plantid AND t.dim_partid = m.dim_partid AND t.MDKP_PLSCN = m.dd_PlannScenarioLTP
AND me.Dim_MRPElementID = m.Dim_MRPElementid AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set') AND t.MDTB_MNG01 = m.ct_QtyMRP
AND m.dim_plantid = pl.dim_plantid
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND m.dd_exceptiongroup8 <> ifnull(t.MDKP_AUSZ8,'Not Set');	
/* Octavian: Every Angle updates from MDKP done similar to the above ones only for actionstateid = 2 */

/* Fields added for BI-5935 */
UPDATE fact_mrp m
SET      m.Dim_CustomerGroup1id = p.Dim_CustomerGroup1id
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_CustomerGroup1id <> p.Dim_CustomerGroup1id;

    UPDATE fact_mrp m
SET    m.Dim_CustomerID = p.Dim_CustomerID
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p,  fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_CustomerID  <> p.Dim_CustomerID;

    UPDATE fact_mrp m
SET    m.Dim_SalesOrderHeaderStatusid = p.Dim_SalesOrderHeaderStatusid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_SalesOrderHeaderStatusid <> p.Dim_SalesOrderHeaderStatusid;

    UPDATE fact_mrp m
SET    m.Dim_SalesOrderItemStatusid = p.Dim_SalesOrderItemStatusid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_SalesOrderItemStatusid <> p.Dim_SalesOrderItemStatusid;

    UPDATE fact_mrp m
SET    m.Dim_SalesOrderRejectReasonid = p.Dim_SalesOrderRejectReasonid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_SalesOrderRejectReasonid  <>  p.Dim_SalesOrderRejectReasonid;

    UPDATE fact_mrp m
SET    ct_QtyOpenOrder =
          CASE
             WHEN (ct_ScheduleQtySalesUnit - ct_DeliveredQty) < 0 THEN 0
             ELSE (ct_ScheduleQtySalesUnit - ct_DeliveredQty)
          END
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND ct_QtyOpenOrder <> CASE
             WHEN (ct_ScheduleQtySalesUnit - ct_DeliveredQty) < 0 THEN 0
             ELSE (ct_ScheduleQtySalesUnit - ct_DeliveredQty)
          END ;

DROP TABLE IF EXISTS mdtb_pi00;

CREATE TABLE mdtb_pi00 AS 
SELECT MDTB_DELNR, MDTB_DTNUM, IFNULL(MDTB_UMDAT, MDTB_DAT01) DATEvalue_upd
FROM mdtb 
WHERE IFNULL(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL;

DROP TABLE IF EXISTS plaf_pi00;

Create table plaf_pi00 as 
Select PLAF_UMREZ,
	PLAF_UMREN,
	PLAF_PLNUM,
	ifnull(PLAF_EMLIF, 'Not Set') PLAF_EMLIF, 
	ifnull(PLAF_FLIEF, 'Not Set') PLAF_FLIEF,
	ifnull(PLAF_KZVBR, 'Not Set') PLAF_KZVBR
from plaf
where ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL;

MERGE INTO fact_mrp fact
USING(SELECT m.fact_mrpid, IFNULL(z.StandardPrice,0) StandardPrice
	  FROM fact_mrp m 
	   INNER JOIN plaf_pi00 po ON m.dd_DocumentNo = po.PLAF_PLNUM
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR 
	   INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
	   INNER JOIN dim_part p ON p.Dim_Partid = m.Dim_Partid
	   INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant
	   INNER JOIN Dim_Date dd ON to_date(dd.DateValue) = to_date(t.datevalue_upd) AND dd.Dim_Dateid <> 1
			 AND dd.CompanyCode = pl.CompanyCode 
			 AND pl.plantcode = dd.plantcode_factory
       LEFT JOIN tmp_getStdPrice z
			            ON  z.pCompanyCode = pl.CompanyCode
						AND z.pPlant = pl.PlantCode
						AND z.pMaterialNo =  k.MDKP_MATNR
						AND z.pFiYear = dd.CalendarYear
						AND z.pPeriod =  dd.FinancialMonthNumber
						AND z.vUMREZ = PLAF_UMREZ
						AND z.vUMREN =   PLAF_UMREN
						AND z.fact_script_name = 'fact_mrp'
						AND z.PONumber IS NULL
                        AND m.Dim_ActionStateid = 2
			           AND z.pUnitPrice =  0) src
ON fact.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.StandardPrice;

drop table if exists mdtb_pi00;
drop table if exists plaf_pi00;

MERGE INTO fact_mrp fact
USING (SELECT m.fact_mrpid,
			  ifnull((p.amt_DeliveryTotal / (case when p.ct_BaseUOMQty = 0 
			                                      then null 
												  else p.ct_BaseUOMQty 
										     end ))
				     ,0) as amt_UnitPrice,
			  ifnull((p.amt_DeliveryTotal / (case when p.ct_BaseUOMQty = 0 
			                                      then null 
												  else p.ct_BaseUOMQty 
										     end))
					 ,0) * p.amt_ExchangeRate_GBL as amt_UnitPrice_GBL
	   FROM  fact_mrp m
	         INNER JOIN fact_purchase p ON m.dd_DocumentNo = p.dd_DocumentNo
										AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
										AND m.dd_ScheduleNo = p.dd_ScheduleNo
			 INNER JOIN dim_vendor v ON v.Dim_Vendorid = p.Dim_Vendorid
             INNER JOIN dim_date dt ON p.Dim_DateidDelivery = dt.Dim_Dateid
		WHERE  m.Dim_ActionStateid = 2
               AND dt.Dim_Dateid <> 1) src
ON fact.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN UPDATE SET 
	fact.amt_UnitPrice = src.amt_UnitPrice,
	fact.amt_UnitPrice_GBL = src.amt_UnitPrice_GBL;

    UPDATE fact_mrp m
SET    m.amt_UnitPrice = ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end)
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.amt_UnitPrice  <> ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end);

    UPDATE fact_mrp m
SET    m.amt_UnitPrice_GBL = ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end) * p.amt_ExchangeRate_GBL
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.amt_UnitPrice_GBL <> ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end) * p.amt_ExchangeRate_GBL;

UPDATE fact_mrp m
SET m.amt_UnitPrice = ifnull((p.amt_estimatedTotalCost/(case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0)
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  fact_productionorder p, fact_mrp m
WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_ordernumber
       AND m.dd_DocumentItemNo = p.dd_orderitemno
       AND m.dd_ScheduleNo = p.dd_SalesOrderNo
       AND p.dd_SalesOrderNo <> 'Not Set'
	AND m.amt_UnitPrice  <> ifnull((p.amt_estimatedTotalCost/(case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0);

UPDATE fact_mrp m
SET m.amt_UnitPrice_GBL = ifnull((p.amt_estimatedTotalCost/(Case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0) * p.amt_ExchangeRate_GBL
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  fact_productionorder p, fact_mrp m
WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_ordernumber
       AND m.dd_DocumentItemNo = p.dd_orderitemno
       AND m.dd_ScheduleNo = p.dd_SalesOrderNo
       AND p.dd_SalesOrderNo <> 'Not Set'
	AND m.amt_UnitPrice_GBL <>  ifnull((p.amt_estimatedTotalCost/(Case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0) * p.amt_ExchangeRate_GBL;

drop table if exists mdtb_pi00;

create table mdtb_pi00 as 
Select MDTB_DELNR,MDTB_DELPS,MDTB_DTNUM from mdtb 
where ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL and MDTB_DAT02 IS NOT NULL;

MERGE INTO fact_mrp fact
USING (select distinct m.fact_mrpid,ifnull(EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 then null else EBAN_PEINH end), 1), 0) as amt_UnitPrice
 From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.EBAN_BANFN
       AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
       AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.amt_UnitPrice
WHERE fact.amt_UnitPrice <> src.amt_UnitPrice;

drop table if exists mdtb_pi00;

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY)
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR AS
SELECT DISTINCT m.MATNR,
       m.BWKEY,
       m.LFGJA,
       m.LFMON,
       m.VPRSV,
       m.VERPR,
       m.STPRS,
       m.PEINH,
       m.SALK3,
       m.BWTAR,
       m.MBEW_LBKUM,
       ((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON,
       m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

UPDATE fact_mrp m
   SET m.amt_UnitPrice = (b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end))
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  tmp2_MBEW_NO_BWTAR b,
       dim_plant p,
       dim_part dp, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND dp.Dim_Partid = m.Dim_Partid
       AND b.MATNR = dp.PartNumber
       AND p.Dim_Plantid = m.Dim_Plantid
       AND b.BWKEY = p.ValuationArea
       AND b.BWTAR IS NULL
       AND b.MBEW_LBKUM > 0
       AND ifnull(m.amt_UnitPrice, 0) = 0
	AND m.amt_UnitPrice <> (b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end));

UPDATE fact_mrp m
   SET m.amt_UnitPrice = (b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end))
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  tmp2_MBEW_NO_BWTAR b,
       dim_plant p,
       dim_part dp, fact_mrp m
 WHERE     m.Dim_ActionStateid = 2
       AND dp.Dim_Partid = m.Dim_Partid
       AND b.MATNR = dp.PartNumber
       AND p.Dim_Plantid = m.Dim_Plantid
       AND b.BWKEY = p.ValuationArea
       AND b.BWTAR IS NULL
       AND b.MBEW_LBKUM > 0
       AND ifnull(m.amt_UnitPrice, 0) = 0
	AND m.amt_UnitPrice <> (b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end));

update fact_mrp m
set m.dd_MRPExcessShortageFlag = case when m.ct_QtyShortage = 0 and m.ct_QtyExcess = 0 then 'MRP'
								when m.ct_QtyShortage > 0 or m.ct_QtyExcess > 0 then 'DR'
								else 'Not Set' end;

/* end BI-5935 */

/* Update Dim_DateidPurchaseRequisition, by FPOPESCU on 05 January 2016  */
		
UPDATE fact_mrp m
 SET m.Dim_DateidPurchaseRequisition = ifnull(g.dim_dateid, 1)
From fact_mrp m
Inner join 
(SELECT distinct od.dim_dateid, m.fact_mrpid from 
fact_mrp m
INNER JOIN 	eban pr ON m.dd_DocumentNo = pr.EBAN_BANFN AND m.dd_DocumentItemNo = pr.EBAN_BNFPO	
INNER JOIN mdkp k on m.dd_PlannScenarioLTP = k.MDKP_PLSCN
INNER JOIN mdtb_tmp_q01 t ON m.dd_DocumentNo = t.MDTB_DELNR AND m.dd_DocumentItemNo = t.MDTB_DELPS AND k.MDKP_DTNUM = t.MDTB_DTNUM
INNER JOIN dim_plant pl ON m.dim_plantid = pl.dim_plantid
LEFT JOIN dim_date od ON od.DateValue = pr.EBAN_BADAT AND od.CompanyCode = pl.CompanyCode and od.plantcode_factory=pl.plantcode
WHERE 
ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) g on m.fact_mrpid=g.fact_mrpid
WHERE m.Dim_ActionStateid = 2 
AND m.Dim_DateidPurchaseRequisition <> ifnull (g.dim_dateid, 1);

/* END Update Dim_DateidPurchaseRequisition, by FPOPESCU on 05 January 2016  */

drop table if exists fact_mrp_del_p00;
drop table if exists eban_p00;
Drop table if exists mdtb_tmp_q01;

truncate table mdtb;

