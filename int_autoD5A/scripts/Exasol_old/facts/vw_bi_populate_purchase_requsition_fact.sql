/* Octavian: script redone for Every Angle project */

/* drop table if exists fact_purchase_requisition
create table fact_purchase_requisition
LIKE fact_purchase_requisition INCLUDING DEFAULTS INCLUDING IDENTITY

ALTER TABLE fact_purchase_requisition ADD PRIMARY KEY (fact_purchase_requisitionid) */

delete from NUMBER_FOUNTAIN where table_name = 'fact_purchase_requisition';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_purchase_requisition', ifnull(max(fact_purchase_requisitionid), 0)
				 FROM fact_purchase_requisition;


insert into fact_purchase_requisition(fact_purchase_requisitionid,dd_purchasereqno,dd_purchasereqitemno,dd_serialnumber,dim_documenttypeid,dd_purchasereqdoccat,dd_deletionindicator,dd_processingstatus,dd_creationindicator,dim_purchasegroupid,dd_releaseindicator,dd_createdobjectbyperson,dim_datechangeonid,dim_materialid,dim_plantid,dim_storagelocationid,dim_materialgroupid,dd_requesttrackno,dim_supplplantstocktranspordid,ct_purchasereq,dim_unitofmeasureid,dim_daterequsiotionid,dim_datepurchasereqreleaseid,dim_dateitemdeliveryid,amt_purchasereqprice,ct_priceunit,dim_itemcategoryid,dim_accountcategoryid,dim_consumptiontypeid,dim_vendorid,dim_purchaseorgid,dim_fixedvendorid,dd_purchaseinforrecord,dd_purchaseorderno,dd_purchaseorderitemno,dim_datepurchaseorderid,ct_orderedagainstpurchase,dd_purchasereqclosed,dim_currencyid,dd_manufacturepartno,dd_planneddeliverydays,dd_purchasereqblocked,dd_requisitionername,dd_requisitionprocessingstate,dd_fixedpurchasereq,dd_releasenotyeteffected,dd_matnrusedbyvendor,dd_shorttext,dd_goodsreceiptindicator,dd_unloadingpoint,dd_ordernumber,dd_glaccountnumber,dd_goodsrec_shipttoparty,dim_costcenterid,dd_releasestatus,ct_goodsreceipttimeindays,amt_exchangerate,amt_exchangerate_gbl,dim_projectsourceid,dw_insert_date,dw_update_date)
select (SELECT max_id
				FROM NUMBER_FOUNTAIN
				WHERE table_name = 'fact_purchase_requisition'
				) + row_number() over(ORDER BY '') 							AS fact_purchase_requisitionid,
		EBAN_BANFN dd_purchasereqno,
		EBAN_BNFPO dd_purchasereqitemno,
		ifnull(EBKN_ZEBKN,0) dd_serialnumber,
		/* ifnull((SELECT Dim_DocumentTypeid
						FROM Dim_DocumentType dtp
						WHERE     dtp.Type     = EBAN_BSART
							  AND dtp.Category = EBAN_BSTYP),1) */ 1  AS Dim_DocumentTypeid,       /* 1. */
				ifnull(convert(varchar(7),EBAN_BSTYP), 'Not Set')  AS dd_PurchaseReqDocCat,     /* 2. */
				ifnull(convert(varchar(7),EBAN_LOEKZ), 'Not Set')  AS dd_deletionindicator,     /* 3. */
				ifnull(convert(varchar(7),EBAN_STATU), 'Not Set')  AS dd_ProcessingStatus,      /* 4. */
				ifnull(convert(varchar(7),EBAN_ESTKZ), 'Not Set')  AS dd_CreationIndicator,     /* 5. */
				/* ifnull((SELECT Dim_PurchaseGroupid
						FROM Dim_PurchaseGroup pg
						WHERE pg.PurchaseGroup = EBAN_EKGRP),1) */ 1  AS Dim_PurchaseGroupid,      /* 6. */
				ifnull(convert(varchar(7),EBAN_FRGKZ),'Not Set')  AS dd_ReleaseIndicator,      /* 7. */
				ifnull(convert(varchar(12),EBAN_ERNAM),'Not Set') AS dd_CreatedObjectByPerson, /* 8. */
				/* ifnull((SELECT pd.dim_dateid
						FROM dim_date pd
						WHERE     pd.datevalue   = EBAN_ERDAT
							  AND pd.companycode = 'Not Set'),1) */ CONVERT(BIGINT,1) AS dim_datechangeonid, 	   /* 9. */ /* check this line, solved */
				/* ifnull((SELECT dim_partid
						FROM dim_Part pt
						WHERE     pt.PartNumber = EBAN_MATNR
							  AND pt.Plant      = EBAN_WERKS),1) */ CONVERT(BIGINT,1)  AS DIM_MATERIALID, 			/* 10. */ /* check this line, solved */
				/* ifnull((SELECT pl.dim_plantid
						FROM dim_plant pl
						WHERE pl.PlantCode = EBAN_WERKS),1) */ 1		 AS Dim_Plantid,            /* 11. */
				/* ifnull((SELECT Dim_StorageLocationid
						FROM dim_StorageLocation sl
						WHERE     sl.LocationCode = EBAN_LGORT
							  AND sl.Plant        = EBAN_WERKS ),1) */ CONVERT(BIGINT,1) AS dim_storagelocationid,   /* 12. */
				/* ifnull((SELECT Dim_MaterialGroupid
						FROM Dim_MaterialGroup mg
						WHERE mg.MaterialGroupCode = EBAN_MATKL), 1) */ CONVERT(BIGINT,1) AS Dim_MaterialGroupid,    /* 13. */
				ifnull(convert(varchar(10),EBAN_BEDNR),'Not Set')    AS dd_RequestTrackNo,      /* 14. */
				/* ifnull((SELECT Dim_Plantid
						FROM Dim_Plant dps
						WHERE dps.PlantCode = EBAN_RESWK),1) */ CONVERT(BIGINT,1)        AS DIM_SupplPlantStockTranspOrdId, /* 15. */
				ifnull(EBAN_MENGE,0) 								 AS ct_PurchaseReq,         /* 16. */
				/* ifnull((SELECT Dim_UnitOfMeasureid
						FROM Dim_UnitOfMeasure uom
						WHERE uom.UOM = EBAN_MEINS), 1) */ CONVERT(BIGINT,1) AS Dim_UnitOfMeasureid,                 /* 17. */
				/* ifnull((SELECT pd.dim_dateid
						FROM dim_date pd
						WHERE     pd.datevalue   = EBAN_BADAT
							  AND pd.companycode = 'Not Set'),1) */ CONVERT(BIGINT,1) AS dim_dateRequsiotionid,      /* 18. */ /* check this line, solved */
				/* ifnull((SELECT pd.dim_dateid
						FROM dim_date pd
						WHERE     pd.datevalue   = EBAN_FRGDT
							  AND pd.companycode = 'Not Set'),1) */ CONVERT(BIGINT,1) AS dim_datePurchaseReqReleaseid, /* 19. */ /* check this line, solved */
				/* ifnull((SELECT pd.dim_dateid
						FROM dim_date pd
						WHERE     pd.datevalue   = EBAN_LFDAT
							  AND pd.companycode = 'Not Set'),1) */ CONVERT(BIGINT,1) AS dim_dateItemDeliveryid,      /* 20. */ /* check this line, solved */
				ifnull(EBAN_PREIS,0) 								 AS amt_PurchaseReqPrice,    /* 21. */
				ifnull(EBAN_PEINH,0) 								 AS ct_PriceUnit,            /* 22. */
				/* ifnull((SELECT Dim_ItemCategoryid
						FROM Dim_ItemCategory ic
						WHERE ic.CategoryCode = EBAN_PSTYP),1) */ 1	 AS Dim_ItemCategoryid,          /* 23. */
				/* ifnull((SELECT Dim_AccountCategoryid
						FROM Dim_AccountCategory ac
						WHERE ac.Category = EBAN_KNTTP), 1) */ 1  AS dim_accountcategoryid,   /* 24. */
				/* ifnull((SELECT Dim_ConsumptionTypeid
						FROM Dim_ConsumptionType ct
						WHERE ct.ConsumptionCode = EBAN_KZVBR),1) */ CONVERT(BIGINT,1) AS dim_consumptiontypeid,     /* 25. */
				/* ifnull((SELECT v.Dim_Vendorid
						FROM dim_vendor v
						WHERE v.VendorNumber = EBAN_LIFNR),1) */ 1	 AS Dim_Vendorid,                /* 26. */
				/* ifnull((SELECT Dim_PurchaseOrgid
						FROM Dim_PurchaseOrg po
						WHERE po.PurchaseOrgCode = EBAN_EKORG),1) */ 1  AS Dim_PurchaseOrgid,         /* 27. */
				/* ifnull((SELECT v.Dim_Vendorid
						FROM dim_vendor v
						WHERE v.VendorNumber = EBAN_FLIEF),1) */ 1 	 AS Dim_FixedVendorid,           /* 28. */
				ifnull(EBAN_INFNR,'Not Set') 								 AS dd_PurchaseInforRecord,  /* 29. */
				ifnull(EBAN_EBELN,'Not Set')								 AS dd_PurchaseOrderNo,      /* 30. */
				ifnull(EBAN_EBELP,0)								 AS dd_PurchaseOrderItemNo,  /* 31. */
				/* ifnull((SELECT pd.dim_dateid
						FROM dim_date pd
						WHERE     pd.datevalue   = EBAN_BEDAT
							  AND pd.companycode = 'Not Set'),1) */ CONVERT(BIGINT,1) AS dim_datePurchaseOrderId,     /* 32. */ /* check this line, solved */
				ifnull(EBAN_BSMNG,0)                                 AS ct_OrderedAgainstPurchase, /* 33. */
				ifnull(convert(varchar(7),EBAN_EBAKZ),'Not Set')     AS dd_PurchaseReqClosed,      /* 34. */
				/* ifnull((SELECT Dim_Currencyid
						FROM Dim_Currency dcr
						WHERE dcr.CurrencyCode = EBAN_WAERS),1) */ 1	 AS Dim_Currencyid,            /* 35. */
				ifnull(convert(varchar(40),EBAN_MFRPN),'Not Set')    AS dd_ManufacturePartNo,      /* 36. */
				ifnull(EBAN_PLIFZ,0) 								 AS dd_PlannedDeliveryDays,    /* 37. */
				ifnull(convert(varchar(7),EBAN_BLCKD),'Not Set')     AS dd_PurchaseReqBlocked,     /* 38. */
				ifnull(EBAN_AFNAM,'Not Set')						 AS dd_RequisitionerName,	   /* Every Angle */
				ifnull(EBAN_BANPR,'Not Set')						 AS dd_RequisitionProcessingState, /* Every Angle */
				ifnull(EBAN_FIXKZ,'Not Set')						 AS dd_FixedPurchaseReq, /* Every Angle */
				ifnull(EBAN_FRGRL,'Not Set')						 AS dd_ReleaseNotYetEffected, /* Every Angle */
				ifnull(EBAN_IDNLF,'Not Set')						 AS dd_matnrusedbyvendor, /* Every Angle */
				ifnull(EBAN_TXZ01,'Not Set')						 AS dd_shorttext, /* Every Angle */
				ifnull(EBAN_WEPOS,'Not Set')						 AS dd_goodsreceiptindicator, /* Every Angle */
				ifnull(EBKN_ABLAD,'Not Set')						 AS dd_unloadingpoint, /* Every Angle */
				ifnull(EBKN_AUFNR,'Not Set')						 AS dd_ordernumber, /* Every Angle */
				ifnull(EBKN_SAKTO,'Not Set')						 AS dd_glaccountnumber, /* Every Angle */
				ifnull(EBKN_WEMPF,'Not Set')						 AS dd_goodsrec_shipttoparty, /* Every Angle */
				/* ifnull((SELECT v.Dim_CostCenterID
				FROM dim_costcenter v
				WHERE v.code = ifnull(eb.EBKN_KOSTL,'Not Set')),1) */ 1	 AS dim_costcenterid, /* Every Angle */
				ifnull(EBAN_FRGZU,'Not Set')						 AS dd_releasestatus, /* Every Angle */
				ifnull(EBAN_WEBAZ,0)						         AS ct_goodsreceipttimeindays, /* Every Angle */
				1 													 AS amt_ExchangeRate,
				1 													 AS amt_ExchangeRate_GBL,
				1													 AS dim_projectsourceid,
				CURRENT_TIMESTAMP									 AS dw_insert_date,
				CURRENT_TIMESTAMP									 AS dw_update_date
from EBAN e LEFT JOIN EBAN_EBKN eb
ON e.EBAN_BANFN = eb.EBKN_BANFN AND e.EBAN_BNFPO = eb.EBKN_BNFPO
WHERE NOT EXISTS 
(SELECT 1 from fact_purchase_requisition fpr
WHERE ifnull(e.EBAN_BANFN,'Not Set') = fpr.dd_purchasereqno AND 
      ifnull(e.EBAN_BNFPO,0) = fpr.dd_purchasereqitemno AND
      ifnull(eb.EBKN_ZEBKN,0) = fpr.dd_serialnumber);

/* Octavian : Every Angle */
/* Missing updates */
UPDATE fact_purchase_requisition fpr
SET fpr.dd_PurchaseOrderNo = ifnull(EBAN_EBELN,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND fpr.dd_PurchaseOrderNo <> ifnull(EBAN_EBELN,'Not Set');

UPDATE fact_purchase_requisition fpr
SET fpr.dd_PurchaseOrderItemNo = ifnull(EBAN_EBELP,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND fpr.dd_PurchaseOrderItemNo <> ifnull(EBAN_EBELP,0);


UPDATE fact_purchase_requisition fpr
SET fpr.ct_OrderedAgainstPurchase = ifnull(EBAN_BSMNG,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND fpr.ct_OrderedAgainstPurchase <> ifnull(EBAN_BSMNG,0);



/* Missing updates */

/* Madalina 14 Sep 2016 - missing updates */
update fact_purchase_requisition 
set dd_PurchaseReqDocCat = ifnull(convert(varchar(7),EBAN_BSTYP), 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_PurchaseReqDocCat <> ifnull(convert(varchar(7),EBAN_BSTYP), 'Not Set');	

update fact_purchase_requisition 
set dd_deletionindicator = ifnull(convert(varchar(7),EBAN_LOEKZ), 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_deletionindicator <> ifnull(convert(varchar(7),EBAN_LOEKZ), 'Not Set');	
	
update fact_purchase_requisition 
set dd_ProcessingStatus = ifnull(convert(varchar(7),EBAN_STATU), 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_ProcessingStatus <> ifnull(convert(varchar(7),EBAN_STATU), 'Not Set');		
	
update fact_purchase_requisition 
set dd_CreationIndicator = ifnull(convert(varchar(7),EBAN_ESTKZ), 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_CreationIndicator <> ifnull(convert(varchar(7),EBAN_ESTKZ), 'Not Set');	
		
update fact_purchase_requisition 
set dd_ReleaseIndicator = ifnull(convert(varchar(7),EBAN_FRGKZ),'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_ReleaseIndicator <> ifnull(convert(varchar(7),EBAN_FRGKZ),'Not Set');				
				
update fact_purchase_requisition 
set dd_CreatedObjectByPerson = ifnull(convert(varchar(12),EBAN_ERNAM),'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_CreatedObjectByPerson <> ifnull(convert(varchar(12),EBAN_ERNAM),'Not Set');
				
update fact_purchase_requisition 
set dd_RequestTrackNo = ifnull(convert(varchar(10),EBAN_BEDNR),'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_RequestTrackNo <> ifnull(convert(varchar(10),EBAN_BEDNR),'Not Set');			
				
update fact_purchase_requisition 
set amt_PurchaseReqPrice = ifnull(EBAN_PREIS,0),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and amt_PurchaseReqPrice <> ifnull(EBAN_PREIS,0);						
			
update fact_purchase_requisition 
set ct_PriceUnit = ifnull(EBAN_PEINH,0),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and ct_PriceUnit <> ifnull(EBAN_PEINH,0);					
				
update fact_purchase_requisition 
set dd_PurchaseInforRecord = ifnull(EBAN_INFNR,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_PurchaseInforRecord <> ifnull(EBAN_INFNR,'Not Set');		
	
update fact_purchase_requisition 
set dd_PurchaseReqClosed = ifnull(convert(varchar(7),EBAN_EBAKZ),'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_PurchaseReqClosed <> ifnull(convert(varchar(7),EBAN_EBAKZ),'Not Set');		
				
update fact_purchase_requisition 
set dd_ManufacturePartNo = ifnull(convert(varchar(40),EBAN_MFRPN),'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_ManufacturePartNo <> ifnull(convert(varchar(40),EBAN_MFRPN),'Not Set');					

update fact_purchase_requisition 
set dd_PlannedDeliveryDays = ifnull(EBAN_PLIFZ,0),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_PlannedDeliveryDays <> ifnull(EBAN_PLIFZ,0);					
				
update fact_purchase_requisition 
set dd_PurchaseReqBlocked = ifnull(convert(varchar(7),EBAN_BLCKD),'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_PurchaseReqBlocked <> ifnull(convert(varchar(7),EBAN_BLCKD),'Not Set');		

update fact_purchase_requisition 
set dd_RequisitionerName = ifnull(EBAN_AFNAM,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_RequisitionerName <> ifnull(EBAN_AFNAM,'Not Set');		
				
update fact_purchase_requisition 
set dd_RequisitionProcessingState = ifnull(EBAN_BANPR,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_RequisitionProcessingState <> ifnull(EBAN_BANPR,'Not Set');	
	
update fact_purchase_requisition 
set dd_FixedPurchaseReq = ifnull(EBAN_FIXKZ,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_FixedPurchaseReq <> ifnull(EBAN_FIXKZ,'Not Set');	
	
update fact_purchase_requisition 
set dd_ReleaseNotYetEffected = ifnull(EBAN_FRGRL,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_ReleaseNotYetEffected <> ifnull(EBAN_FRGRL,'Not Set');					
				
update fact_purchase_requisition 
set dd_matnrusedbyvendor = ifnull(EBAN_IDNLF,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_matnrusedbyvendor <> ifnull(EBAN_IDNLF,'Not Set');	

update fact_purchase_requisition 
set dd_shorttext = ifnull(EBAN_TXZ01,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_shorttext <> ifnull(EBAN_TXZ01,'Not Set');
	
update fact_purchase_requisition 
set dd_goodsreceiptindicator = ifnull(EBAN_WEPOS,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_goodsreceiptindicator <> ifnull(EBAN_WEPOS,'Not Set');	

/* Unstable set of rows */	
				
/* update fact_purchase_requisition fpr
set dd_unloadingpoint = ifnull(EBKN_ABLAD,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition fpr, EBAN_EBKN e 
where dd_purchasereqno = ifnull(e.EBKN_BANFN,'Not Set') 
	and dd_purchasereqitemno = ifnull(e.EBKN_BNFPO, 0)
	and ifnull(e.EBKN_ZEBKN,0) = fpr.dd_serialnumber
	and dd_unloadingpoint <> ifnull(EBKN_ABLAD,'Not Set')

update fact_purchase_requisition fpr
set dd_ordernumber = ifnull(EBKN_AUFNR,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition fpr, EBAN_EBKN e 
where dd_purchasereqno = e.EBKN_BANFN 
	and dd_purchasereqitemno = e.EBKN_BNFPO
	and ifnull(e.EBKN_ZEBKN,0) = fpr.dd_serialnumber
	and dd_ordernumber <> ifnull(EBKN_AUFNR,'Not Set')
				
update fact_purchase_requisition fpr
set dd_glaccountnumber = ifnull(EBKN_SAKTO,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition fpr, EBAN_EBKN e 
where dd_purchasereqno = e.EBKN_BANFN 
	and dd_purchasereqitemno = e.EBKN_BNFPO
	and ifnull(e.EBKN_ZEBKN,0) = fpr.dd_serialnumber
	and dd_glaccountnumber <> ifnull(EBKN_SAKTO,'Not Set')

update fact_purchase_requisition fpr
set dd_goodsrec_shipttoparty = ifnull(EBKN_WEMPF,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition fpr, EBAN_EBKN e 
where dd_purchasereqno = e.EBKN_BANFN 
	and dd_purchasereqitemno = e.EBKN_BNFPO
	and ifnull(e.EBKN_ZEBKN,0) = fpr.dd_serialnumber
	and dd_goodsrec_shipttoparty <> ifnull(EBKN_WEMPF,'Not Set') */
	
MERGE INTO fact_purchase_requisition fpr
using ( select distinct fpr.fact_purchase_requisitionid, first_value(ifnull(EBKN_ABLAD,'Not Set')) OVER (PARTITION BY fpr.fact_purchase_requisitionid ORDER BY '' DESC) AS EBKN_ABLAD
from fact_purchase_requisition fpr, EBAN_EBKN e 
where dd_purchasereqno = ifnull(e.EBKN_BANFN,'Not Set') 
	and dd_purchasereqitemno = ifnull(e.EBKN_BNFPO, 0)
	and ifnull(e.EBKN_ZEBKN,0) = fpr.dd_serialnumber
	and dd_unloadingpoint <> ifnull(EBKN_ABLAD,'Not Set')) src
ON fpr.fact_purchase_requisitionid = src.fact_purchase_requisitionid
WHEN MATCHED THEN UPDATE
set dd_unloadingpoint = ifnull(EBKN_ABLAD,'Not Set');

MERGE INTO fact_purchase_requisition fpr
using ( select distinct fpr.fact_purchase_requisitionid, first_value(ifnull(EBKN_AUFNR,'Not Set'))  OVER (PARTITION BY fpr.fact_purchase_requisitionid ORDER BY '' DESC) AS EBKN_AUFNR
from fact_purchase_requisition fpr, EBAN_EBKN e 
where dd_purchasereqno = ifnull(e.EBKN_BANFN,'Not Set') 
	and dd_purchasereqitemno = ifnull(e.EBKN_BNFPO, 0)
	and ifnull(e.EBKN_ZEBKN,0) = fpr.dd_serialnumber
	and dd_ordernumber <> ifnull(EBKN_AUFNR,'Not Set')) src
ON fpr.fact_purchase_requisitionid = src.fact_purchase_requisitionid
WHEN MATCHED THEN UPDATE
set dd_ordernumber = ifnull(EBKN_AUFNR,'Not Set');

MERGE INTO fact_purchase_requisition fpr
using ( select distinct fpr.fact_purchase_requisitionid, first_value(ifnull(EBKN_SAKTO,'Not Set')) OVER (PARTITION BY fpr.fact_purchase_requisitionid ORDER BY '' DESC) AS EBKN_SAKTO
from fact_purchase_requisition fpr, EBAN_EBKN e 
where dd_purchasereqno = ifnull(e.EBKN_BANFN,'Not Set') 
	and dd_purchasereqitemno = ifnull(e.EBKN_BNFPO, 0)
	and ifnull(e.EBKN_ZEBKN,0) = fpr.dd_serialnumber
	and dd_glaccountnumber <> ifnull(EBKN_SAKTO,'Not Set')) src
ON fpr.fact_purchase_requisitionid = src.fact_purchase_requisitionid
WHEN MATCHED THEN UPDATE
set dd_glaccountnumber = ifnull(EBKN_SAKTO,'Not Set');

MERGE INTO fact_purchase_requisition fpr
using ( select distinct fpr.fact_purchase_requisitionid, first_value(ifnull(EBKN_WEMPF,'Not Set'))  OVER (PARTITION BY fpr.fact_purchase_requisitionid ORDER BY '' DESC) AS EBKN_WEMPF
from fact_purchase_requisition fpr, EBAN_EBKN e 
where dd_purchasereqno = ifnull(e.EBKN_BANFN,'Not Set') 
	and dd_purchasereqitemno = ifnull(e.EBKN_BNFPO, 0)
	and ifnull(e.EBKN_ZEBKN,0) = fpr.dd_serialnumber
	and dd_goodsrec_shipttoparty <> ifnull(EBKN_WEMPF,'Not Set')) src
ON fpr.fact_purchase_requisitionid = src.fact_purchase_requisitionid
WHEN MATCHED THEN UPDATE
set dd_goodsrec_shipttoparty = ifnull(EBKN_WEMPF,'Not Set');
	
update fact_purchase_requisition 
set dd_releasestatus = ifnull(EBAN_FRGZU,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and dd_releasestatus <> ifnull(EBAN_FRGZU,'Not Set');				
						
update fact_purchase_requisition 
set ct_goodsreceipttimeindays = ifnull(EBAN_WEBAZ,0),
	dw_update_date = CURRENT_TIMESTAMP
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and ct_goodsreceipttimeindays <> ifnull(EBAN_WEBAZ,0);		
/* End missing updates */

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_DocumentTypeid = dtp.Dim_DocumentTypeid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_DocumentType dtp
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND dtp.Type     = EBAN_BSART
AND dtp.Category = EBAN_BSTYP
AND fpr.Dim_DocumentTypeid <> dtp.Dim_DocumentTypeid;

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_PurchaseGroupid = pg.Dim_PurchaseGroupid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_PurchaseGroup pg
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND pg.PurchaseGroup = EBAN_EKGRP
AND fpr.Dim_PurchaseGroupid <> pg.Dim_PurchaseGroupid;

UPDATE fact_purchase_requisition fpr
SET fpr.dim_plantid = pl.Dim_Plantid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, dim_plant pl
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND pl.PlantCode = EBAN_WERKS
AND fpr.dim_plantid <> pl.Dim_Plantid;


UPDATE fact_purchase_requisition fpr
SET fpr.dim_datechangeonid = pd.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, dim_date pd, dim_plant pl
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND pd.datevalue   = EBAN_ERDAT
AND fpr.dim_plantid=pl.dim_plantid
AND pl.companycode=pd.companycode
and pd.plantcode_factory=pl.plantcode
AND fpr.dim_datechangeonid <> pd.dim_dateid;

UPDATE fact_purchase_requisition fpr
SET fpr.DIM_MATERIALID = pt.dim_partid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, dim_Part pt
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND pt.PartNumber = EBAN_MATNR
AND pt.Plant      = EBAN_WERKS
AND fpr.DIM_MATERIALID <> pt.dim_partid;


UPDATE fact_purchase_requisition fpr
SET fpr.dim_storagelocationid = sl.Dim_StorageLocationid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, dim_StorageLocation sl
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND sl.LocationCode = EBAN_LGORT
AND sl.Plant = EBAN_WERKS
AND fpr.dim_storagelocationid <> sl.Dim_StorageLocationid;

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_MaterialGroupid = mg.Dim_MaterialGroupid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_MaterialGroup mg
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND mg.MaterialGroupCode = EBAN_MATKL
AND fpr.Dim_MaterialGroupid <> mg.Dim_MaterialGroupid;

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_MaterialGroupid = mg.Dim_MaterialGroupid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_MaterialGroup mg
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND mg.MaterialGroupCode = EBAN_MATKL
AND fpr.Dim_MaterialGroupid <> mg.Dim_MaterialGroupid;

UPDATE fact_purchase_requisition fpr
SET fpr.DIM_SupplPlantStockTranspOrdId = dps.Dim_Plantid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_plant dps
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND dps.PlantCode = EBAN_RESWK
AND fpr.DIM_SupplPlantStockTranspOrdId <> dps.Dim_Plantid;

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_UnitOfMeasure uom
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND uom.UOM = EBAN_MEINS
AND fpr.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;

UPDATE fact_purchase_requisition fpr
SET fpr.dim_dateRequsiotionid = pd.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, dim_date pd,dim_plant pl
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND pd.datevalue   = EBAN_BADAT
AND fpr.dim_plantid=pl.dim_plantid
AND pd.companycode = pl.companycode
and pd.plantcode_factory=pl.plantcode
AND fpr.dim_dateRequsiotionid <> pd.dim_dateid;

UPDATE fact_purchase_requisition fpr
SET fpr.dim_datePurchaseReqReleaseid = pd.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, dim_date pd,dim_plant pl
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND pd.datevalue   = EBAN_FRGDT
AND fpr.dim_plantid=pl.dim_plantid
AND pd.companycode = pl.companycode
and pd.plantcode_factory=pl.plantcode
AND fpr.dim_datePurchaseReqReleaseid <> pd.dim_dateid;

UPDATE fact_purchase_requisition fpr
SET fpr.dim_dateItemDeliveryid = pd.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, dim_date pd,dim_plant pl
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND pd.datevalue   = EBAN_LFDAT
AND fpr.dim_plantid=pl.dim_plantid
AND pd.companycode = pl.companycode
and pd.plantcode_factory=pl.plantcode
AND fpr.dim_dateItemDeliveryid <> pd.dim_dateid;

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_ItemCategoryid = ic.Dim_ItemCategoryid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_ItemCategory ic
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND ic.CategoryCode = EBAN_PSTYP
AND fpr.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;

UPDATE fact_purchase_requisition fpr
SET fpr.dim_accountcategoryid = ac.Dim_AccountCategoryid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_AccountCategory ac
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND ac.Category = EBAN_KNTTP
AND fpr.dim_accountcategoryid <> ac.Dim_AccountCategoryID;

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_Vendorid = dv.Dim_Vendorid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_Vendor dv
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND dv.VendorNumber = EBAN_LIFNR
AND fpr.Dim_Vendorid <> dv.Dim_Vendorid;

UPDATE fact_purchase_requisition fpr
SET fpr.dim_consumptiontypeid = ct.Dim_ConsumptionTypeid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_ConsumptionType ct
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND ct.ConsumptionCode = EBAN_KZVBR
AND fpr.dim_consumptiontypeid <> ct.Dim_ConsumptionTypeid;

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_PurchaseOrgid = po.Dim_PurchaseOrgid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_PurchaseOrg po
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND po.PurchaseOrgCode = EBAN_EKORG
AND fpr.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_FixedVendorid = dv.Dim_Vendorid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_Vendor dv
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND dv.VendorNumber = EBAN_FLIEF
AND fpr.Dim_FixedVendorid <> dv.Dim_Vendorid;

UPDATE fact_purchase_requisition fpr
SET fpr.dim_datePurchaseOrderId = pd.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, dim_date pd, dim_plant pl
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND pd.datevalue   = EBAN_BEDAT
and fpr.dim_plantid=pl.dim_plantid
AND pd.companycode = pl.companycode
and pd.plantcode_factory=pl.plantcode
AND fpr.dim_datePurchaseOrderId <> pd.dim_dateid;


UPDATE fact_purchase_requisition fpr
SET fpr.Dim_CurrencyID = dcr.Dim_CurrencyID,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_Currency dcr
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND dcr.CurrencyCode = EBAN_WAERS
AND fpr.Dim_CurrencyID <> dcr.Dim_CurrencyID;

MERGE INTO fact_purchase_requisition fpr
USING (SELECT DISTINCT fpr.fact_purchase_requisitionid, FIRST_VALUE(dv.Dim_CostCenterid) OVER (PARTITION BY fpr.fact_purchase_requisitionid ORDER BY dv.code DESC) AS dim_costcenterid
FROM fact_purchase_requisition fpr, dim_costcenter dv, EBAN_EBKN eb
WHERE dd_purchasereqno =   eb.EBKN_BANFN
AND dd_purchasereqitemno = eb.EBKN_BNFPO
AND dv.code = ifnull(eb.EBKN_KOSTL,'Not Set')
AND fpr.Dim_CostCenterid <> dv.Dim_CostCenterid) src
ON fpr.fact_purchase_requisitionid = src.fact_purchase_requisitionid
WHEN MATCHED THEN UPDATE
SET fpr.Dim_CostCenterid = src.Dim_CostCenterid;


UPDATE fact_purchase_requisition fpr
SET fpr.dim_vendorpurchasingid = vp.dim_vendorpurchasingid,
fpr.dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e,dim_vendorpurchasing vp,fact_purchase_requisition fpr
WHERE fpr.dd_purchasereqno = e.EBAN_BANFN
AND fpr.dd_purchasereqitemno = e.EBAN_BNFPO
AND vp.purchasingorg = ifnull(e.EBAN_EKORG,'Not Set')
AND vp.vendornumber = ifnull(e.EBAN_LIFNR,'Not Set')
AND fpr.dim_vendorpurchasingid <> vp.dim_vendorpurchasingid;
/* Octavian : Every Angle */

UPDATE fact_purchase_requisition fpr
SET fpr.Dim_DocumentTypeid = dtp.Dim_DocumentTypeid,
dw_update_date = CURRENT_TIMESTAMP
FROM EBAN e, fact_purchase_requisition fpr, Dim_DocumentType dtp
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND dtp.Type     = EBAN_BSART
AND dtp.Category = EBAN_BSTYP
AND fpr.Dim_DocumentTypeid <> dtp.Dim_DocumentTypeid;



/* Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

UPDATE fact_purchase_requisition f
SET f.std_exchangerate_dateid = dt.dim_dateid
FROM dim_plant p
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode and p.plantcode=dt.plantcode_factory
		 INNER JOIN fact_purchase_requisition f ON f.dim_plantid = p.dim_plantid
WHERE dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

/* END Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

/* Madalina 13 Sep 2016 - Correct Purchase Requisition Quantity BI-4095 */
update fact_purchase_requisition 
set ct_PurchaseReq = ifnull(EBAN_MENGE,0)
from fact_purchase_requisition, EBAN e 
where dd_purchasereqno = e.EBAN_BANFN 
	and dd_purchasereqitemno = e.EBAN_BNFPO
	and ct_PurchaseReq <> ifnull(EBAN_MENGE,0);
/* END BI-4095 */

/* 26 May 2016 EA Changes adding dd_executionastatus according to BI-3021*/

drop table if exists tmp_for_executionstatus;
create table tmp_for_executionstatus as select distinct dd_purchasereqno, dd_purchasereqitemno, CASE WHEN dd_deletionindicator = 'X' AND dd_purchasereqclosed = 'X' AND 	ct_orderedagainstpurchase = 0 THEN 'Cancelled'
WHEN dd_deletionindicator = 'X' AND dd_purchasereqclosed = 'X' AND 	ct_orderedagainstpurchase <> 0 THEN 'Closed'
WHEN dd_deletionindicator = 'X' AND dd_purchasereqclosed <> 'X' AND 	ct_orderedagainstpurchase = 0 AND ct_purchasereq = 0 THEN 'Closed'
WHEN dd_deletionindicator = 'X' AND dd_purchasereqclosed <> 'X' AND 	ct_orderedagainstpurchase = 0 AND ct_purchasereq <> 0 THEN 'Cancelled'
WHEN dd_deletionindicator = 'X' AND dd_purchasereqclosed <> 'X' AND 	ct_orderedagainstpurchase <> 0 THEN 'Closed'
WHEN dd_deletionindicator <> 'X' AND dd_purchasereqclosed = 'X' AND 	ct_orderedagainstpurchase = 0 THEN 'Cancelled'
WHEN dd_deletionindicator <> 'X' AND dd_purchasereqclosed = 'X' AND 	ct_orderedagainstpurchase <> 0 THEN 'Closed'
WHEN dd_deletionindicator <> 'X' AND dd_purchasereqclosed <> 'X' AND 	ct_orderedagainstpurchase = 0 THEN 'Open'
WHEN dd_deletionindicator <> 'X' AND dd_purchasereqclosed <> 'X' AND ct_orderedagainstpurchase <> 0 AND ct_orderedagainstpurchase < ct_purchasereq THEN 'Partially open'
WHEN dd_deletionindicator <> 'X' AND dd_purchasereqclosed <> 'X' AND 	ct_orderedagainstpurchase <> 0 AND not(ct_orderedagainstpurchase < ct_purchasereq) THEN 'Closed' end as executionstatus
from fact_purchase_requisition;


update fact_purchase_requisition fpr
set fpr.dd_executionastatus = ifnull(t.executionstatus,'Not Set')
,dw_update_date = current_timestamp

from tmp_for_executionstatus t,fact_purchase_requisition fpr
where fpr.dd_purchasereqno = t.dd_purchasereqno
and fpr.dd_purchasereqitemno = t.dd_purchasereqitemno
AND fpr.dd_executionastatus <> ifnull(t.executionstatus,'Not Set');

drop table if exists tmp_for_executionstatus;

DELETE FROM fact_purchase_requisition fpr
WHERE NOT EXISTS (SELECT 1 
from EBAN e LEFT JOIN EBAN_EBKN eb
ON e.EBAN_BANFN = eb.EBKN_BANFN AND e.EBAN_BNFPO = eb.EBKN_BNFPO
WHERE ifnull(e.EBAN_BANFN,'Not Set') = fpr.dd_purchasereqno AND 
      ifnull(e.EBAN_BNFPO,0) = fpr.dd_purchasereqitemno AND
      ifnull(eb.EBKN_ZEBKN,0) = fpr.dd_serialnumber);

/*Alin APP-10520 3 Dec 2018 - currency enable*/

/*update std_exchangerate_dateid*/
UPDATE fact_purchase_requisition f
SET f.std_exchangerate_dateid = f.dim_dateRequsiotionid
WHERE  f.std_exchangerate_dateid <> f.dim_dateRequsiotionid;


/*amt_exchangerates */
update fact_purchase_requisition f
set amt_exchangerate = z.exchangeRate
from fact_purchase_requisition f,  EBAN e, tmp_getExchangeRate1 z, dim_date d
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
and z.pFromCurrency  = ifnull(e.EBAN_WAERS, 'Not Set') 
and z.fact_script_name = 'bi_populate_purchase_requisition_fact'
and d.dim_dateid = f.dim_dateRequsiotionid
and z.pdate = d.datevalue
and z.pFromCurrency = z.pToCurrency
and amt_exchangerate <> z.exchangeRate;


drop table if exists tmp_gblcurr_purchase_requisition;
Create table tmp_gblcurr_purchase_requisition(
pGlobalCurrency varchar(3) null);

Insert into tmp_gblcurr_purchase_requisition(pGlobalCurrency) values(null);

Update tmp_gblcurr_purchase_requisition
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');


update fact_purchase_requisition f
set amt_exchangerate_gbl = z.exchangeRate
from fact_purchase_requisition f,  EBAN e, tmp_getExchangeRate1 z, dim_date d, tmp_gblcurr_purchase_requisition req
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
and z.pFromCurrency  = ifnull(e.EBAN_WAERS, 'Not Set') 
and z.fact_script_name = 'bi_populate_purchase_requisition_fact'
and d.dim_dateid = f.dim_dateRequsiotionid
and z.pdate = d.datevalue
and z.pFromCurrency <> z.pToCurrency
and z.pToCurrency = req.pGlobalCurrency
and amt_exchangerate_gbl <> z.exchangeRate;


/* Update currency IDs */

update fact_purchase_requisition f
set f.dim_currencyid = cur.dim_currencyid
from fact_purchase_requisition f,  EBAN e, dim_currency cur
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
and cur.currencycode  = ifnull(e.EBAN_WAERS, 'Not Set') 
and f.dim_currencyid <> cur.dim_currencyid;


update fact_purchase_requisition f
set f.dim_CurrencyId_TRA = cur.dim_currencyid
from fact_purchase_requisition f,  EBAN e,  dim_currency cur, dim_date d
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
and cur.currencycode = ifnull(e.EBAN_WAERS, 'Not Set') 
and d.dim_dateid = f.dim_dateRequsiotionid
and f.dim_CurrencyId_TRA <> cur.dim_currencyid;


update fact_purchase_requisition f
set f.dim_CurrencyId_GBL = ifnull(cur.dim_currencyid,1)  
from fact_purchase_requisition f,  EBAN e, tmp_gblcurr_ppv, dim_currency cur
WHERE dd_purchasereqno =   e.EBAN_BANFN
AND dd_purchasereqitemno = e.EBAN_BNFPO
AND cur.currencycode = pGlobalCurrency
and f.dim_CurrencyId_GBL <> ifnull(cur.dim_currencyid,1);

