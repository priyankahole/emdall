/*   Script         : bi_populate_salesorder_fillrate */
/*   Author         : Ashu */
/*   Created On     : 13 Feb 2013 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_salesorder_fillrate migration from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        	Version           Desc */
/*   13 Feb 2013     Ashu      	1.0               Existing code migrated to Vectorwise */
/*   28 Apr 2015     Suchithra 	1.1               Modified Update for Dim_DateidShipDlvrFill */
/*   6 Jul 2015      Lokesh	1.1               All queries changed to remove subqueries from update - to resolve memory errors on Merck QA */

drop table if exists fact_salesorder_720;

Create table fact_salesorder_720(
  v_Fact_SalesOrderid   bigint null,
  v_dd_SalesDocNo       varchar(50) null,
  v_dd_SalesItemNo      integer null,
  v_dd_ScheduleNo       integer null,
  v_dim_doccategoryid   bigint null,
  v_dim_part_id         bigint null,
  v_goods_issue_date    date null, 
  v_ct_FillQty          decimal(18,4) null,
  v_ct_OnHandQty        decimal(18,4) null,
  v_TotalQty            decimal(18,4) null,
  iflag 		integer default 0,
  sr_no		int null);

Insert into fact_salesorder_720(
  v_Fact_SalesOrderid,
  v_dd_SalesDocNo ,
  v_dd_SalesItemNo,
  v_dd_ScheduleNo,
  v_dim_part_id,
  v_goods_issue_date,
  v_ct_FillQty,
  v_ct_OnHandQty,
  v_TotalQty
  )
  SELECT Fact_SalesOrderid v_Fact_SalesOrderid,
         dd_SalesDocNo v_dd_SalesDocNo ,
         dd_SalesItemNo v_dd_SalesItemNo,
         dd_ScheduleNo v_dd_ScheduleNo,
         dim_partid v_dim_part_id,
         dt.DateValue v_goods_issue_date,
         ct_FillQty v_ct_FillQty,
         ct_OnHandQty v_ct_OnHandQty,
         ct_ConfirmedQty v_TotalQty
  FROM fact_salesorder f
      INNER JOIN dim_date dt ON dt.dim_dateid = f.Dim_DateidGoodsIssue
      INNER JOIN dim_date adt ON adt.dim_dateid = f.Dim_DateidActualGI
      inner join dim_documentcategory dg on dg.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
      inner join Dim_ScheduleLineCategory slc on f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
  WHERE (   dt.DateValue = current_date
         OR adt.DateValue = current_date
         OR ((dt.DateValue < current_date OR adt.DateValue < current_date) AND ct_FillQty = 0 AND ct_OnHandQty = 0)
		)
        AND f.Dim_SalesOrderRejectReasonid = 1 
		AND dg.DocumentCategory <> 'H' 
        AND f.dd_ItemRelForDelv = 'X'
  ORDER BY dt.DateValue DESC, dd_SalesDocNo, dd_SalesItemNo,dd_ScheduleNo;


drop table if exists tmp1_exists_f_sod;
create table tmp1_exists_f_sod as
select f_sod.dd_SalesDocNo,f_sod.dd_SalesItemNo,f_sod.dd_ScheduleNo,d_gi.dim_dateid,d_gi.datevalue,d_pgi.datevalue pgi_datevalue,f_sod.ct_QtyDelivered
FROM fact_salesorderdelivery f_sod
                      INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                      INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                      inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.GoodsMovementStatus <> 'Not yet processed';

 
drop table if exists fact_salesorder_720_a;
create table fact_salesorder_720_a as
select v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_goods_issue_date,
sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END) ct_QtyDelivered_sum
from tmp1_exists_f_sod f_sod,fact_salesorder_720 
WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue > v_goods_issue_date) or (f_sod.dim_dateid = 1 and f_sod.pgi_datevalue > v_goods_issue_date))
group by v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_goods_issue_date;


/***
UPDATE fact_salesorder f
From dim_salesorderitemstatus s,fact_salesorder_720 x
        SET   ct_FillQty = f.ct_ConfirmedQty - ifnull((SELECT f_sod.ct_QtyDelivered_sum
                                            FROM    fact_salesorder_720_a f_sod 
                                            WHERE     f_sod.v_dd_SalesDocNo = x.v_dd_SalesDocNo
                                                  AND f_sod.v_dd_SalesItemNo = x.v_dd_SalesItemNo
                                                  AND f_sod.v_dd_ScheduleNo = x.v_dd_ScheduleNo
						AND f_sod.v_goods_issue_date = x.v_goods_issue_date),0)
where s.OverallDeliveryStatus = 'Completely processed'
and exists (SELECT 1 FROM tmp1_exists_f_sod f_sod
		 WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                      AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                      AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue <= v_goods_issue_date) or (f_sod.dim_dateid = 1 and f_sod.pgi_datevalue <= v_goods_issue_date)))
AND  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty = 0
***/

/* Split the above update into 2 queries */

DROP TABLE IF EXISTS tmp_fact_salesorder_ct_FillQty;
CREATE TABLE tmp_fact_salesorder_ct_FillQty AS
SELECT f.dd_SalesDocNo,f.dd_SalesItemNo,f.dd_ScheduleNo,f.ct_ConfirmedQty,
	   ifnull(f_sod.ct_QtyDelivered_sum, 0) ct_QtyDelivered_sum
FROM fact_salesorder f
		inner join dim_salesorderitemstatus s on f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
		inner join fact_salesorder_720 x on f.Fact_SalesOrderid = x.v_Fact_SalesOrderid
		left join fact_salesorder_720_a f_sod on  f_sod.v_dd_SalesDocNo = x.v_dd_SalesDocNo
											  AND f_sod.v_dd_SalesItemNo = x.v_dd_SalesItemNo
											  AND f_sod.v_dd_ScheduleNo = x.v_dd_ScheduleNo
											  AND f_sod.v_goods_issue_date = x.v_goods_issue_date
where   s.OverallDeliveryStatus = 'Completely processed'
	AND x.v_ct_FillQty = 0
	AND exists (SELECT 1 FROM tmp1_exists_f_sod f_sod
			    WHERE    f_sod.dd_SalesDocNo = x.v_dd_SalesDocNo
				     AND f_sod.dd_SalesItemNo = x.v_dd_SalesItemNo
				     AND f_sod.dd_ScheduleNo = x.v_dd_ScheduleNo
					 AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue <= x.v_goods_issue_date) or (f_sod.dim_dateid = 1 and f_sod.pgi_datevalue <= x.v_goods_issue_date)));


UPDATE fact_salesorder f
SET ct_FillQty = c.ct_ConfirmedQty - c.ct_QtyDelivered_sum
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_fact_salesorder_ct_FillQty c, fact_salesorder f
WHERE f.dd_SalesDocNo = c.dd_SalesDocNo
AND f.dd_SalesItemNo = c.dd_SalesItemNo
AND f.dd_ScheduleNo = c.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_fact_salesorder_ct_FillQty;
/* Lokesh 26 Jun 2015 : Above update has been split to prevent it from failing with memory err */
DROP TABLE IF EXISTS tmp_upd_fillqty_fromsod;
CREATE TABLE tmp_upd_fillqty_fromsod
AS
SELECT v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_goods_issue_date,sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END) upd_ct_FillQty
FROM fact_salesorder_720 f, fact_salesorderdelivery f_sod, dim_date d_gi, dim_date d_pgi,dim_salesorderitemstatus sois
WHERE d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue AND d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
AND f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
AND f_sod.dd_SalesDocNo = v_dd_SalesDocNo
AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND sois.GoodsMovementStatus <> 'Not yet processed'
AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))
GROUP BY v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_goods_issue_date;


DROP TABLE IF EXISTS tmp_upd_fillqty_2;
CREATE TABLE tmp_upd_fillqty_2
AS
SELECT v_dd_SalesDocNo v_dd_SalesDocNo_U,v_dd_SalesItemNo v_dd_SalesItemNo_U,v_dd_ScheduleNo v_dd_ScheduleNo_U
FROM fact_salesorder_720
WHERE NOT EXISTS (SELECT 1 FROM fact_salesorderdelivery f_sod
                      INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue
                      INNER JOIN dim_date d_pgi ON d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue
                      inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                      AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                      AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
                      and sois.GoodsMovementStatus <> 'Not yet processed'
			AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date)));


UPDATE fact_salesorder f
SET f.ct_FillQty = 0
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 t,fact_salesorder f
WHERE s.OverallDeliveryStatus <> 'Completely processed'
AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = t.v_Fact_SalesOrderid
AND v_ct_FillQty=0
AND ifnull(f.ct_FillQty,-1) <> 0;

UPDATE fact_salesorder f
SET f.ct_FillQty = 0
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 t, tmp_upd_fillqty_2 t2,fact_salesorder f
WHERE s.OverallDeliveryStatus = 'Completely processed'
AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = t.v_Fact_SalesOrderid
AND v_ct_FillQty=0
AND t.v_dd_SalesDocNo = t2.v_dd_SalesDocNo_U AND t.v_dd_SalesItemNo = t2.v_dd_SalesItemNo_U AND t.v_dd_ScheduleNo = t2.v_dd_ScheduleNo_U
AND ifnull(f.ct_FillQty,-1) <> 0;

UPDATE fact_salesorder f
SET f.ct_FillQty = u.upd_ct_FillQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 t, tmp_upd_fillqty_fromsod u,fact_salesorder f
WHERE s.OverallDeliveryStatus <> 'Completely processed'
AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = t.v_Fact_SalesOrderid
AND t.v_dd_SalesDocNo = u.v_dd_SalesDocNo AND t.v_dd_SalesItemNo = u.v_dd_SalesItemNo AND t.v_dd_ScheduleNo = u.v_dd_ScheduleNo AND t.v_goods_issue_date = u.v_goods_issue_date
AND v_ct_FillQty=0
AND f.ct_FillQty <> u.upd_ct_FillQty;


UPDATE fact_salesorder f
SET   ct_FillQty = u.upd_ct_FillQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 t , tmp_upd_fillqty_2 t2, tmp_upd_fillqty_fromsod u,fact_salesorder f
where s.OverallDeliveryStatus = 'Completely processed'
AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND t.v_dd_SalesDocNo = u.v_dd_SalesDocNo AND t.v_dd_SalesItemNo = u.v_dd_SalesItemNo AND t.v_dd_ScheduleNo = u.v_dd_ScheduleNo AND t.v_goods_issue_date = u.v_goods_issue_date
AND t.v_dd_SalesDocNo = t2.v_dd_SalesDocNo_U AND t.v_dd_SalesItemNo = t2.v_dd_SalesItemNo_U AND t.v_dd_ScheduleNo = t2.v_dd_ScheduleNo_U
AND v_ct_FillQty=0
AND f.ct_FillQty <> u.upd_ct_FillQty;

DROP TABLE IF EXISTS tmp_upd_fillqty_2;
DROP TABLE IF EXISTS tmp_upd_fillqty_fromsod;

DROP TABLE IF EXISTS tmp_upd_Dim_DateidShipDlvrFill;
CREATE TABLE tmp_upd_Dim_DateidShipDlvrFill
AS
SELECT f_sod.dd_SalesDocNo,f_sod.dd_SalesItemNo,f_sod.dd_ScheduleNo,v_goods_issue_date,max(f_sod.Dim_DateidDeliveryDate) max_Dim_DateidDeliveryDate,
max(f_sod.Dim_DateidActualGoodsIssue) max_Dim_DateidActualGoodsIssue,
max(f_sod.Dim_DateidLoadingDate) max_Dim_DateidLoadingDate
FROM fact_salesorderdelivery f_sod, fact_salesorder_720 , dim_date d_gi, dim_date d_pgi, dim_salesorderitemstatus sois
WHERE f_sod.dd_SalesDocNo = v_dd_SalesDocNo AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND d_gi.Dim_Dateid = f_sod.Dim_DateidActualGoodsIssue AND d_pgi.Dim_Dateid = f_sod.Dim_DateidPlannedGoodsIssue AND f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
and sois.GoodsMovementStatus <> 'Not yet processed'
AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_goods_issue_date) or (d_gi.dim_dateid = 1 and d_pgi.datevalue <= v_goods_issue_date))
AND v_ct_FillQty=0
GROUP BY f_sod.dd_SalesDocNo,f_sod.dd_SalesItemNo,f_sod.dd_ScheduleNo,v_goods_issue_date;

UPDATE fact_salesorder f
SET f.Dim_DateidShipDlvrFill = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 fs2,fact_salesorder f
Where  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty=0
AND ifnull(f.Dim_DateidShipDlvrFill,-1) <> 1;

UPDATE fact_salesorder f
SET f.Dim_DateidActualGIFill = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 fs2,fact_salesorder f
Where  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty=0
AND ifnull(f.Dim_DateidActualGIFill,-1) <> 1;

UPDATE fact_salesorder f
SET f.Dim_DateidLoadingFill = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 fs2,fact_salesorder f
Where  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty=0
AND ifnull(f.Dim_DateidLoadingFill,-1) <> 1;



UPDATE fact_salesorder f
SET f.Dim_DateidShipDlvrFill = f_sod.max_Dim_DateidDeliveryDate
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 fs2, tmp_upd_Dim_DateidShipDlvrFill f_sod,fact_salesorder f
Where  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty=0
AND f_sod.dd_SalesDocNo = v_dd_SalesDocNo AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo AND fs2.v_goods_issue_date = f_sod.v_goods_issue_date
AND f.Dim_DateidShipDlvrFill <> f_sod.max_Dim_DateidDeliveryDate;

UPDATE fact_salesorder f
SET f.Dim_DateidActualGIFill = f_sod.max_Dim_DateidActualGoodsIssue
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 fs2, tmp_upd_Dim_DateidShipDlvrFill f_sod,fact_salesorder f
Where  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty=0
AND f_sod.dd_SalesDocNo = v_dd_SalesDocNo AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo AND fs2.v_goods_issue_date = f_sod.v_goods_issue_date
AND f.Dim_DateidActualGIFill <> f_sod.max_Dim_DateidActualGoodsIssue;


UPDATE fact_salesorder f
SET f.Dim_DateidLoadingFill = f_sod.max_Dim_DateidLoadingDate
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 fs2, tmp_upd_Dim_DateidShipDlvrFill f_sod,fact_salesorder f
Where  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty=0
AND f_sod.dd_SalesDocNo = v_dd_SalesDocNo AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo 
AND fs2.v_goods_issue_date = f_sod.v_goods_issue_date
AND f.Dim_DateidLoadingFill <> f_sod.max_Dim_DateidLoadingDate;

UPDATE fact_salesorder
SET   ct_OnHandQty = ct_FillQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_salesorder_720, fact_salesorder
WHERE Fact_SalesOrderid = v_Fact_SalesOrderid
        AND v_goods_issue_date < current_date
        AND v_ct_OnHandQty =0
AND ct_OnHandQty <>  ct_FillQty;

DROP TABLE IF EXISTS tmp_fillrate1;
create table tmp_fillrate1
as
select distinct f_so_sq_001.Dim_Partid,d_GoodsIssue_sq_001.DateValue,f_so_sq_001.dd_SalesDocNo , f_so_sq_001.dd_SalesItemNo,
f_so_sq_001.dd_ScheduleNo,( f_so_sq_001.ct_ConfirmedQty  - f_so_sq_001.ct_DeliveredQty ) diff_cnf_dlvr,
 f_so_sq_001.ct_ConfirmedQty sum_prev_diff_cnf_dlvr,
 row_number() over(ORDER BY f_so_sq_001.Dim_Partid,d_GoodsIssue_sq_001.DateValue,f_so_sq_001.dd_SalesDocNo , f_so_sq_001.dd_SalesItemNo,
f_so_sq_001.dd_ScheduleNo) sr_no
FROM  fact_salesorder f_so_sq_001 inner join dim_date d_GoodsIssue_sq_001
on  f_so_sq_001.Dim_DateidGoodsIssue = d_GoodsIssue_sq_001.Dim_Dateid
where f_so_sq_001.Dim_SalesOrderRejectReasonid = 1
and f_so_sq_001.dd_ItemRelForDelv = 'X'
and not exists ( select 1 from fact_salesorder_720 v where v.v_dim_doccategoryid  = f_so_sq_001.Dim_DocumentCategoryid )
ORDER BY f_so_sq_001.Dim_Partid,d_GoodsIssue_sq_001.DateValue,f_so_sq_001.dd_SalesDocNo , f_so_sq_001.dd_SalesItemNo,
f_so_sq_001.dd_ScheduleNo;


UPDATE tmp_fillrate1
set sum_prev_diff_cnf_dlvr = 0;

drop table if exists tmp_fillrate1b;
create table tmp_fillrate1b
as
select Dim_Partid,sr_no,diff_cnf_dlvr
from tmp_fillrate1
where diff_cnf_dlvr > 0;

drop table if exists tmp_fillrate1a;
create table tmp_fillrate1a
AS
SELECT b.Dim_Partid,a.sr_no,sum(b.diff_cnf_dlvr) as sum_diff_cnf_dlvr
from tmp_fillrate1 a, tmp_fillrate1b b
where a.Dim_Partid = b.Dim_Partid
and b.sr_no < a.sr_no
GROUP BY b.dim_partid,a.sr_no;

update tmp_fillrate1 a
set sum_prev_diff_cnf_dlvr = b.sum_diff_cnf_dlvr
from tmp_fillrate1a b,tmp_fillrate1 a
where a.Dim_Partid = b.Dim_Partid
and b.sr_no = a.sr_no
AND  sum_prev_diff_cnf_dlvr<>  b.sum_diff_cnf_dlvr;

drop table if exists tmp_fillrate1a;
drop table if exists tmp_fillrate1b;


DROP TABLE IF EXISTS tmp_fillrate2;
create table tmp_fillrate2
as
select f_inv_sq_001.dim_Partid,sum(f_inv_sq_001.ct_StockQty + f_inv_sq_001.ct_BlockedStock
                  + f_inv_sq_001.ct_StockInQInsp + f_inv_sq_001.ct_StockInTransfer
                  + f_inv_sq_001.ct_TotalRestrictedStock) sum_ivaging
FROM fact_inventoryaging f_inv_sq_001
GROUP BY f_inv_sq_001.dim_Partid;

merge into  fact_salesorder_720 f
using (select fs2.v_fact_salesorderid,  ifnull(f1.sum_ivaging,0) - ifnull(f2.sum_prev_diff_cnf_dlvr,0) as  v_ct_OnHandQty
FROM fact_salesorder_720 fs2
 LEFT JOIN tmp_fillrate2 f1 on f1.dim_Partid = fs2.v_dim_part_id
 LEFT JOIN tmp_fillrate1 f2  on f2.Dim_Partid = fs2.v_dim_part_id AND f2.DateValue = fs2.v_goods_issue_date
                                AND f2.dd_SalesDocNo = fs2.v_dd_SalesDocNo
                                AND f2.dd_SalesItemNo = fs2.v_dd_SalesItemNo
                                AND f2.dd_ScheduleNo = fs2.v_dd_ScheduleNo
WHERE v_ct_OnHandQty=0 
AND v_goods_issue_date >= current_date) upd on f.v_fact_salesorderid=upd.v_fact_salesorderid
when matched then update set  f.v_ct_OnHandQty = upd.v_ct_OnHandQty
where f.v_ct_OnHandQty <> upd.v_ct_OnHandQty;


Update fact_salesorder_720
Set v_ct_OnHandQty = (case when v_ct_OnHandQty < 0 then 0 when v_ct_OnHandQty > v_TotalQty then v_TotalQty else v_ct_OnHandQty end)
Where iflag=1;
UPDATE fact_salesorder
SET   ct_OnHandQty = v_ct_OnHandQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FRom fact_salesorder_720,fact_salesorder
WHERE Fact_SalesOrderid = v_Fact_SalesOrderid
AND iflag=1
AND ct_OnHandQty <> v_ct_OnHandQty;



drop table if exists fact_salesorder_720;

Create table fact_salesorder_720(
  v_Fact_SalesOrderid   bigint null,
  v_dd_SalesDocNo       varchar(50) null,
  v_dd_SalesItemNo      integer null,
  v_dd_ScheduleNo       integer null,
  v_dim_doccategoryid   bigint null,
  v_dim_part_id         bigint null,
  v_cust_req_date    date null,
  v_ct_FillQty_CRD      decimal(18,4) null,
  v_TotalQty            decimal(18,4) null,
  iflag                 integer default 0,
  sr_no         int null);

Insert into fact_salesorder_720(
  v_Fact_SalesOrderid,
  v_dd_SalesDocNo ,
  v_dd_SalesItemNo,
  v_dd_ScheduleNo,
  v_dim_part_id,
  v_cust_req_date,
  v_ct_FillQty_CRD,
  v_TotalQty
  )
  SELECT Fact_SalesOrderid v_Fact_SalesOrderid,
         dd_SalesDocNo v_dd_SalesDocNo ,
         dd_SalesItemNo v_dd_SalesItemNo,
         dd_ScheduleNo v_dd_ScheduleNo,
         dim_partid v_dim_part_id,
         dt.DateValue v_cust_req_date,
         ct_FillQty_CRD v_ct_FillQty_CRD,
         ct_ConfirmedQty v_TotalQty
  FROM fact_salesorder f
      INNER JOIN dim_date dt ON dt.dim_dateid = f.Dim_DateidSchedDeliveryReq
      INNER JOIN dim_date adt ON adt.dim_dateid = f.Dim_DateidActualGI
      inner join dim_documentcategory dg on dg.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
      inner join Dim_ScheduleLineCategory slc on f.Dim_ScheduleLineCategoryid = slc.Dim_ScheduleLineCategoryid
  WHERE (dt.DateValue = current_date OR adt.DateValue = current_date
            OR ((dt.DateValue < current_date OR adt.DateValue < current_date) AND ct_FillQty_CRD = 0))
        AND f.Dim_SalesOrderRejectReasonid = 1 and dg.DocumentCategory <> 'H'
        AND f.dd_ItemRelForDelv = 'X'
  ORDER BY dt.DateValue DESC, dd_SalesDocNo, dd_SalesItemNo,dd_ScheduleNo;


Update fact_salesorder_720
SET v_dim_doccategoryid = dg.Dim_DocumentCategoryid
from dim_documentcategory dg,fact_salesorder_720
where dg.DocumentCategory = 'H'
AND v_dim_doccategoryid <> dg.Dim_DocumentCategoryid;

drop table if exists tmp1_exists_f_sod;
create table tmp1_exists_f_sod
as
select f_sod.dd_SalesDocNo,f_sod.dd_SalesItemNo,f_sod.dd_ScheduleNo,d_gi.dim_dateid,d_gi.datevalue,d_crd.datevalue crd_datevalue,f_sod.ct_QtyDelivered
FROM fact_salesorderdelivery f_sod
                      INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGI_Original
                      INNER JOIN dim_date d_crd ON d_crd.Dim_Dateid = f_sod.Dim_DateidSchedDeliveryReq
                      inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.GoodsMovementStatus <> 'Not yet processed';


drop table if exists fact_salesorder_720_a;
create table fact_salesorder_720_a
as
select v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_cust_req_date,sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END) ct_QtyDelivered_sum
from tmp1_exists_f_sod f_sod,fact_salesorder_720
WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue > v_cust_req_date) or (f_sod.dim_dateid = 1 and f_sod.crd_datevalue > v_cust_req_date))
group by v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_cust_req_date;


/*UPDATE fact_salesorder f
From dim_salesorderitemstatus s,fact_salesorder_720 x
        SET   ct_FillQty_CRD = f.ct_ConfirmedQty - ifnull((SELECT f_sod.ct_QtyDelivered_sum
                                            FROM    fact_salesorder_720_a f_sod
                                            WHERE     f_sod.v_dd_SalesDocNo = x.v_dd_SalesDocNo
                                                  AND f_sod.v_dd_SalesItemNo = x.v_dd_SalesItemNo
                                                  AND f_sod.v_dd_ScheduleNo = x.v_dd_ScheduleNo
                                                AND f_sod.v_cust_req_date = x.v_cust_req_date),0)
where s.OverallDeliveryStatus = 'Completely processed'
and exists (SELECT 1 FROM tmp1_exists_f_sod f_sod
                 WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                      AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                      AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue <= v_cust_req_date) or (f_sod.dim_dateid = 1 and f_sod.crd_datevalue <= v_cust_req_date)))
AND  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty_CRD =0*/


merge into fact_salesorder f
using (select distinct x.v_fact_salesorderid,  f.ct_ConfirmedQty - ifnull(f_sod.ct_QtyDelivered_sum,0) as v_ct_FillQty_CRD
from fact_salesorder f,dim_salesorderitemstatus s,fact_salesorder_720 x
 left join fact_salesorder_720_a f_sod ON   f_sod.v_dd_SalesDocNo = x.v_dd_SalesDocNo
                                                  AND f_sod.v_dd_SalesItemNo = x.v_dd_SalesItemNo
                                                  AND f_sod.v_dd_ScheduleNo = x.v_dd_ScheduleNo
                                                AND f_sod.v_cust_req_date = x.v_cust_req_date
where s.OverallDeliveryStatus = 'Completely processed'
and exists (SELECT 1 FROM tmp1_exists_f_sod f_sod
                 WHERE     f_sod.dd_SalesDocNo = x.v_dd_SalesDocNo
                      AND f_sod.dd_SalesItemNo = x.v_dd_SalesItemNo
                      AND f_sod.dd_ScheduleNo = x.v_dd_ScheduleNo
AND ((f_sod.dim_dateid <> 1 and f_sod.datevalue <= x.v_cust_req_date) or (f_sod.dim_dateid = 1 and f_sod.crd_datevalue <= x.v_cust_req_date)))
AND  f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND v_ct_FillQty_CRD =0) upd on f.fact_salesorderid=upd.v_fact_salesorderid
when matched then update SET  ct_FillQty_CRD=upd.v_ct_FillQty_CRD
where ct_FillQty_CRD<>upd.v_ct_FillQty_CRD;



DROP TABLE IF EXISTS tmp_upd_fillqty_fromsod;
CREATE TABLE tmp_upd_fillqty_fromsod
AS
SELECT v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_cust_req_date,sum(CASE WHEN f_sod.ct_QtyDelivered < 0 THEN 0 ELSE f_sod.ct_QtyDelivered END) upd_ct_FillQty_CRD
FROM fact_salesorder_720 f, fact_salesorderdelivery f_sod, dim_date d_gi, dim_date d_crd,dim_salesorderitemstatus sois
WHERE d_gi.Dim_Dateid = f_sod.Dim_DateidActualGI_Original AND d_crd.Dim_Dateid = f_sod.Dim_DateidSchedDeliveryReq
AND f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
AND f_sod.dd_SalesDocNo = v_dd_SalesDocNo
AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
AND sois.GoodsMovementStatus <> 'Not yet processed'
AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_cust_req_date) or (d_gi.dim_dateid = 1 and d_crd.datevalue <= v_cust_req_date))
GROUP BY v_dd_SalesDocNo,v_dd_SalesItemNo,v_dd_ScheduleNo,v_cust_req_date;

DROP TABLE IF EXISTS tmp_upd_fillqty_2;
CREATE TABLE tmp_upd_fillqty_2
AS
SELECT v_dd_SalesDocNo v_dd_SalesDocNo_U,v_dd_SalesItemNo v_dd_SalesItemNo_U,v_dd_ScheduleNo v_dd_ScheduleNo_U
FROM fact_salesorder_720
WHERE NOT EXISTS (SELECT 1 FROM fact_salesorderdelivery f_sod
                      INNER JOIN dim_date d_gi ON d_gi.Dim_Dateid = f_sod.Dim_DateidActualGI_Original
                      INNER JOIN dim_date d_crd ON d_crd.Dim_Dateid = f_sod.Dim_DateidSchedDeliveryReq
                      inner join dim_salesorderitemstatus sois on f_sod.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
                WHERE     f_sod.dd_SalesDocNo = v_dd_SalesDocNo
                      AND f_sod.dd_SalesItemNo = v_dd_SalesItemNo
                      AND f_sod.dd_ScheduleNo = v_dd_ScheduleNo
                      and sois.GoodsMovementStatus <> 'Not yet processed'
                        AND ((d_gi.dim_dateid <> 1 and d_gi.datevalue <= v_cust_req_date) or (d_gi.dim_dateid = 1 and d_crd.datevalue <= v_cust_req_date)));



UPDATE fact_salesorder f
SET f.ct_FillQty_CRD = 0
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 t,fact_salesorder f
WHERE s.OverallDeliveryStatus <> 'Completely processed'
AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = t.v_Fact_SalesOrderid
AND v_ct_FillQty_CRD=0
AND ifnull(f.ct_FillQty_CRD,-1) <> 0;

UPDATE fact_salesorder f
SET f.ct_FillQty_CRD = 0
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 t, tmp_upd_fillqty_2 t2,fact_salesorder f
WHERE s.OverallDeliveryStatus = 'Completely processed'
AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = t.v_Fact_SalesOrderid
AND v_ct_FillQty_CRD=0
AND t.v_dd_SalesDocNo = t2.v_dd_SalesDocNo_U AND t.v_dd_SalesItemNo = t2.v_dd_SalesItemNo_U AND t.v_dd_ScheduleNo = t2.v_dd_ScheduleNo_U
AND ifnull(f.ct_FillQty_CRD,-1) <> 0;

UPDATE fact_salesorder f
SET f.ct_FillQty_CRD = u.upd_ct_FillQty_CRD
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 t, tmp_upd_fillqty_fromsod u,fact_salesorder f
WHERE s.OverallDeliveryStatus <> 'Completely processed'
AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = t.v_Fact_SalesOrderid
AND t.v_dd_SalesDocNo = u.v_dd_SalesDocNo AND t.v_dd_SalesItemNo = u.v_dd_SalesItemNo AND t.v_dd_ScheduleNo = u.v_dd_ScheduleNo AND t.v_cust_req_date = u.v_cust_req_date
AND v_ct_FillQty_CRD=0
AND f.ct_FillQty_CRD <> u.upd_ct_FillQty_CRD;


UPDATE fact_salesorder f
SET   ct_FillQty_CRD = u.upd_ct_FillQty_CRD
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From dim_salesorderitemstatus s,fact_salesorder_720 t , tmp_upd_fillqty_2 t2, tmp_upd_fillqty_fromsod u,fact_salesorder f
where s.OverallDeliveryStatus = 'Completely processed'
AND f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
AND Fact_SalesOrderid = v_Fact_SalesOrderid
AND t.v_dd_SalesDocNo = u.v_dd_SalesDocNo AND t.v_dd_SalesItemNo = u.v_dd_SalesItemNo AND t.v_dd_ScheduleNo = u.v_dd_ScheduleNo AND t.v_cust_req_date = u.v_cust_req_date
AND t.v_dd_SalesDocNo = t2.v_dd_SalesDocNo_U AND t.v_dd_SalesItemNo = t2.v_dd_SalesItemNo_U AND t.v_dd_ScheduleNo = t2.v_dd_ScheduleNo_U
AND v_ct_FillQty_CRD=0
AND f.ct_FillQty_CRD <> u.upd_ct_FillQty_CRD;


DROP TABLE IF EXISTS tmp_upd_fillqty_2;
DROP TABLE IF EXISTS tmp_upd_fillqty_fromsod;

drop table if exists fact_salesorder_720;
drop table if exists tmp1_exists_f_sod;
drop table if exists fact_salesorder_720_a;

