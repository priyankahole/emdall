
/* ################################################################################################################## */
/* */
/*   Author         : Georgiana */
/*   Created On     : 26 Aug 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   26 Aug 2016      Gergiana	1.0  		  First draft */

/******************************************************************************************************************/

delete from number_fountain m where m.table_name = 'fact_masterinspectioncharacteristic';
insert into number_fountain
select 'fact_masterinspectioncharacteristic',
ifnull(max(f.fact_masterinspectioncharacteristicid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_masterinspectioncharacteristic f;

insert into fact_masterinspectioncharacteristic (
fact_masterinspectioncharacteristicid,
dd_inspectionplant,
dd_masterinspection,
dd_inspectionversion,
dim_dateidvalidfromdate,
dd_status,
dim_inspectioncharacteristicid,
dim_plantid,
ct_firstupperlimit,
ct_firstlowerlimit,
dw_insert_date)
SELECT
(select max_id   from number_fountain   where table_name = 'fact_masterinspectioncharacteristic') + row_number() over(order by '') AS fact_masterinspectioncharacteristicid, 
ifnull(q.QPMK_ZAEHLER, 'Not Set'),
ifnull(q.QPMK_MKMNR,'Not Set'),
ifnull(q.QPMK_VERSION,'Not Set'),
1 as dim_dateidvalidfromdate,
'Not set' as dd_status,
1 as dim_inspectioncharacteristicid,
1 as dim_plantid,
0 as ct_firstupperlimit,
0 as ct_firstlowerlimit,
current_date
from QPMK_QPMT q
where not exists (select 1 from fact_masterinspectioncharacteristic where 
                                dd_inspectionplant = ifnull(q.QPMK_ZAEHLER, 'Not Set')
                                 and dd_masterinspection = ifnull(q.QPMK_MKMNR,'Not Set')
                                 and dd_inspectionversion = ifnull(q.QPMK_VERSION,'Not Set'));

update fact_masterinspectioncharacteristic f
set f.dim_dateidvalidfromdate = dt.dim_dateid
from fact_masterinspectioncharacteristic f, QPMK_QPMT q, dim_date dt, dim_plant dp
where 
dd_inspectionplant = ifnull(q.QPMK_ZAEHLER, 'Not Set')
and dd_masterinspection = ifnull(q.QPMK_MKMNR,'Not Set')
and dd_inspectionversion = ifnull(q.QPMK_VERSION,'Not Set')
and dt.CompanyCode = dp.CompanyCode
AND dt.plantcode_factory = dp.plantcode
and dp.plantcode = ifnull(QPMK_WERKS,'Not Set')
and dt.datevalue =ifnull(QPMK_GUELTIGAB,'0001-01-01')
and f.dim_dateidvalidfromdate <> dt.dim_dateid;

update fact_masterinspectioncharacteristic f
set f.dim_inspectioncharacteristicid = dic.dim_inspectioncharacteristicid
from fact_masterinspectioncharacteristic f, QPMK_QPMT q, dim_inspectioncharacteristic dic
where 
dd_inspectionplant = ifnull(q.QPMK_ZAEHLER, 'Not Set')
and dd_masterinspection = ifnull(q.QPMK_MKMNR,'Not Set')
and dd_inspectionversion = ifnull(q.QPMK_VERSION,'Not Set')
and dic.InspectionPlant = ifnull(q.QPMK_ZAEHLER, 'Not Set') 
and dic.Masterinspcharacteristics = ifnull(q.QPMK_MKMNR, 'Not Set') 
and dic.VersionNumber = ifnull(q.QPMK_VERSION, 'Not Set') 
and f.dim_inspectioncharacteristicid <> dic.dim_inspectioncharacteristicid;

update fact_masterinspectioncharacteristic f
set f.dim_plantid= pl.dim_plantid
from fact_masterinspectioncharacteristic f, QPMK_QPMT q, dim_plant pl
where 
dd_inspectionplant = ifnull(q.QPMK_ZAEHLER, 'Not Set')
and dd_masterinspection = ifnull(q.QPMK_MKMNR,'Not Set')
and dd_inspectionversion = ifnull(q.QPMK_VERSION,'Not Set')
and pl.plantcode=ifnull(q.QPMK_ZAEHLER, 'Not Set')
and f.dim_plantid <> pl.dim_plantid;

merge into fact_masterinspectioncharacteristic f
using (select distinct f.fact_masterinspectioncharacteristicid,QPMK_GRENZEOB1,QPMK_GRENZEUN1
from fact_masterinspectioncharacteristic f, QPMK_QPMT q
where 
dd_inspectionplant = ifnull(q.QPMK_ZAEHLER, 'Not Set')
and dd_masterinspection = ifnull(q.QPMK_MKMNR,'Not Set')
and dd_inspectionversion = ifnull(q.QPMK_VERSION,'Not Set')) t
on t.fact_masterinspectioncharacteristicid=f.fact_masterinspectioncharacteristicid
when matched then update set ct_firstupperlimit = ifnull(QPMK_GRENZEOB1,0),
ct_firstlowerlimit= ifnull(QPMK_GRENZEUN1,0);

merge into fact_masterinspectioncharacteristic f
using (select distinct f.fact_masterinspectioncharacteristicid, ifnull(case when q.QPMK_LOEKZ
='1' then '1 (Being created)' 
                               when q.QPMK_LOEKZ='2' then '2 (Released)'
							   when q.QPMK_LOEKZ='3' then '3 (Can no longer be used)'
							   when q.QPMK_LOEKZ='4' then '4 (Deletion flag)' end,'Not Set') as dd_status

from fact_masterinspectioncharacteristic f, QPMK_QPMT q
where 
dd_inspectionplant = ifnull(q.QPMK_ZAEHLER, 'Not Set')
and dd_masterinspection = ifnull(q.QPMK_MKMNR,'Not Set')
and dd_inspectionversion = ifnull(q.QPMK_VERSION,'Not Set')
and f.dd_status <>  ifnull(case when q.QPMK_LOEKZ='1' then '1 (Being created)' 
                               when q.QPMK_LOEKZ='2' then '2 (Released)'
							   when q.QPMK_LOEKZ='3' then '3 (Can no longer be used)'
							   when q.QPMK_LOEKZ='4' then '4 (Deletion flag)' end,'Not set')) t
on t.fact_masterinspectioncharacteristicid=f.fact_masterinspectioncharacteristicid
when matched then update set f.dd_status = t.dd_status;



