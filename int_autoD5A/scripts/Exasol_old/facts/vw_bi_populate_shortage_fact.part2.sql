
/* Run in this order : vw_bi_populate_shortage_fact.part1.sql, vw_getstdprice_custom.fact_shortage.sql,
  vw_getstdprice_std.part1.sql, vw_funct_fiscal_year.custom.getStdPrice.sql,vw_funct_fiscal_year.standard.sql, vw_getstdprice_std.part2.sql,
    vw_bi_populate_shortage_fact.part2.sql */

DROP TABLE IF EXISTS tmp_resb_for_shortagefact;
create table tmp_resb_for_shortagefact
as
select RESB_RSNUM,RESB_RSPOS,RESB_RSART,min(rb.RESB_BDTER) as min_RESB_BDTER
FROM RESB rb
GROUP BY RESB_RSNUM,RESB_RSPOS,RESB_RSART;

DROP TABLE IF EXISTS tmp_resb_for_shortagefact_2;
CREATE TABLE tmp_resb_for_shortagefact_2
AS
SELECT distinct rb.*,rb2.min_RESB_BDTER
FROM RESB rb,tmp_resb_for_shortagefact rb2
WHERE rb.RESB_RSNUM = rb2.RESB_RSNUM
AND rb.RESB_RSPOS = rb2.RESB_RSPOS
AND rb.RESB_RSART = rb2.RESB_RSART;


DROP TABLE IF EXISTS fact_shortage_temp_getStdPrice;
CREATE TABLE  fact_shortage_temp_getStdPrice
AS SELECT distinct r.RESB_RSNUM,r.RESB_RSPOS,r.RESB_RSART,
StandardPrice As amt_StdPrice
from tmp_getStdPrice z, dim_plant pl, tmp_resb_for_shortagefact_2 r, dim_date dt
where z.pCompanyCode = pl.CompanyCode
AND z.pPlant = pl.PlantCode
AND z.pMaterialNo = r.RESB_MATNR
AND z.pFiYear = dt.FinancialYear
AND z.pPeriod = dt.FinancialMonthNumber
AND z.fact_script_name = 'bi_populate_shortage_fact'
AND z.vUMREZ = RESB_UMREZ
AND z.vUMREN = RESB_UMREN
AND z.PONumber IS NULL
AND dt.DateValue = r.min_RESB_BDTER
AND dt.CompanyCode = pl.CompanyCode
and dt.plantcode_factory=pl.plantcode
AND pl.PlantCode = r.RESB_WERKS
AND z.pUnitPrice = ifnull((CASE WHEN ifnull(RESB_PEINH, 0) = 0 THEN 1 ELSE RESB_PEINH END),0);

UPDATE fact_shortage_temp f
SET f.amt_StdPrice = r.amt_StdPrice
FROM fact_shortage_temp f, fact_shortage_temp_getStdPrice r
WHERE f.dd_ReservationNo <> 0
AND f.dd_ReservationItemNo <> 0
AND f.dd_ReservationNo = r.RESB_RSNUM
AND f.dd_ReservationItemNo = r.RESB_RSPOS
AND f.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
AND f.amt_StdPrice <> r.amt_StdPrice;

/*
UPDATE fact_shortage_temp fs
   SET fs.amt_StdPrice = ifnull((SELECT StandardPrice from tmp_getStdPrice z
            where z.pCompanyCode = pl.CompanyCode
            AND z.pPlant = pl.PlantCode
            AND z.pMaterialNo = r.RESB_MATNR
            AND z.pFiYear = dt.FinancialYear
            AND z.pPeriod = dt.FinancialMonthNumber
            AND z.fact_script_name = 'bi_populate_shortage_fact'
            AND z.vUMREZ = RESB_UMREZ
            AND z.vUMREN = RESB_UMREN
            AND z.PONumber IS NULL
            AND z.pUnitPrice =   ifnull((CASE WHEN ifnull(RESB_PEINH, 0) = 0 THEN 1 ELSE RESB_PEINH END),0)),0)
FROM
       dim_plant pl,
       tmp_resb_for_shortagefact_2 r,
       dim_date dt, fact_shortage_temp fs
 WHERE     fs.dd_ReservationNo <> 0
       AND fs.dd_ReservationItemNo <> 0
       AND fs.dd_ReservationNo = r.RESB_RSNUM
       AND fs.dd_ReservationItemNo = r.RESB_RSPOS
       AND dt.DateValue = r.min_RESB_BDTER
       AND dt.CompanyCode = pl.CompanyCode
       AND pl.PlantCode = r.RESB_WERKS
       AND pl.RowIsCurrent = 1 */

DROP TABLE IF EXISTS tmp_resb_for_shortagefact;
DROP TABLE IF EXISTS tmp_resb_for_shortagefact_2;

insert into fact_shortage
select * from fact_shortage_temp;

/* Madalina 24 Jun 2016 - Delete rows from Reservations SA that were deleted from RASB - BI-3301 */
/*
delete from fact_shortage s
where not exists (select 1 from resb r
where s.dd_reservationno = r.resb_rsnum
and s.dd_reservationitemno = r.resb_rspos)
and Dim_DateIdReservRequirement in (select dim_dateid from dim_date where datevalue > '2011-01-01')*/
/* End 24 Jun 2016 */

UPDATE fact_shortage f
   SET f.amt_StdPrice = fp.amt_StdUnitPrice
FROM fact_shortage f, fact_purchase fp
WHERE     f.dd_DocumentNo = fp.dd_DocumentNo
       AND f.dd_DocumentItemNo = fp.dd_DocumentItemNo
       AND f.dd_ScheduleNo = fp.dd_ScheduleNo
       AND f.dim_ComponentId = fp.Dim_Partid
AND ifnull(f.amt_StdPrice,-1) <> ifnull(fp.amt_StdUnitPrice,-2) ;


DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1_pre;
CREATE TABLE TMP_FS_fact_inventoryaging1_pre
AS
SELECT   inv.dim_PartId,inv.dim_Plantid,inv.dim_stockcategoryid,
inv.ct_StockQty+ inv.ct_StockInQInsp+ inv.ct_BlockedStock+ inv.ct_StockInTransfer as upd_dd_OnHandQty,
inv.ct_StockQty as upd_dd_UnrestrictedStockQty
FROM fact_inventoryaging inv
WHERE EXISTS ( SELECT 1 from fact_shortage_temp s
WHERE s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2);

DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
CREATE TABLE TMP_FS_fact_inventoryaging1
AS
SELECT inv.dim_PartId,inv.dim_Plantid,inv.dim_stockcategoryid,
SUM(upd_dd_OnHandQty) as sum_upd_dd_OnHandQty,
SUM(inv.upd_dd_UnrestrictedStockQty) sum_upd_dd_UnrestrictedStockQty
FROM TMP_FS_fact_inventoryaging1_pre inv
GROUP BY  inv.dim_PartId,inv.dim_Plantid,inv.dim_stockcategoryid;

UPDATE fact_shortage s
   SET s.dd_OnHandQty = 0
WHERE IFNULL(dd_OnHandQty,-1) <> IFNULL(dd_OnHandQty,-2)   ;

UPDATE fact_shortage s
   SET s.dd_UnrestrictedStockQty = 0
WHERE IFNULL(dd_UnrestrictedStockQty,-1) <> IFNULL(dd_UnrestrictedStockQty,-2)   ;

UPDATE fact_shortage s
SET s.dd_OnHandQty = sum_upd_dd_OnHandQty
FROM TMP_FS_fact_inventoryaging1 inv, fact_shortage s
WHERE     s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2
AND s.dd_OnHandQty  <> sum_upd_dd_OnHandQty;


UPDATE fact_shortage s
SET s.dd_UnrestrictedStockQty = sum_upd_dd_UnrestrictedStockQty
FROM TMP_FS_fact_inventoryaging1 inv, fact_shortage s
WHERE     s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2
AND s.dd_UnrestrictedStockQty  <> sum_upd_dd_UnrestrictedStockQty;


DROP TABLE IF EXISTS TMP_FS_MARD;
CREATE TABLE TMP_FS_MARD
AS
SELECT m.MARD_MATNR,m.MARD_WERKS,sum(ifnull(m.MARD_LABST, 0)) as upd_dd_StockQty
FROM  MARD m
WHERE EXISTS ( select 1 from  fact_Shortage s, dim_part pt, dim_plant pl
WHERE   m.MARD_MATNR = pt.PartNumber
AND m.MARD_WERKS = pt.Plant
AND s.Dim_ComponentId = pt.Dim_Partid
AND pt.RowIsCurrent = 1
AND s.Dim_ComponentPlantid = pl.Dim_Plantid
AND pl.RowIsCurrent = 1
AND pt.Plant = pl.PlantCode )
GROUP BY m.MARD_MATNR,m.MARD_WERKS;

UPDATE fact_shortage s
   SET s.dd_StockQty = 0
WHERE IFNULL(dd_StockQty,-1) <> IFNULL(dd_StockQty,-2)   ;

UPDATE fact_shortage s
SET s.dd_StockQty =           upd_dd_StockQty
from dim_part pt, dim_plant pl,TMP_FS_MARD m, fact_shortage s
WHERE     s.Dim_ComponentId = pt.Dim_Partid
AND pt.RowIsCurrent = 1
AND s.Dim_ComponentPlantid = pl.Dim_Plantid
AND pl.RowIsCurrent = 1
AND pt.Plant = pl.PlantCode
AND m.MARD_MATNR = pt.PartNumber AND m.MARD_WERKS = pt.Plant;

update fact_shortage s
SET s.dim_costcenterid = cc.dim_costcenterid,
dw_update_date = CURRENT_TIMESTAMP
FROM RKPF r, (SELECT cc.code,cc.dim_costcenterid,row_number() over (partition by cc.code order by dim_costcenterid desc) AS rn FROM  dim_costcenter cc) cc, fact_shortage s
WHERE s.dd_ReservationNo <> 0
AND s.dd_ReservationItemNo <> 0
AND r.RKPF_RSNUM = s.dd_ReservationNo
AND cc.code = ifnull(r.rkpf_kostl,'Not Set')
AND cc.rn = 1
AND s.dim_costcenterid <> cc.dim_costcenterid;

update fact_shortage s
SET ct_goodreceiptsprocessingdays = ifnull(r.resb_webaz,0),
dw_update_date = CURRENT_TIMESTAMP
FROM RESB r, fact_shortage s
WHERE s.dd_ReservationNo <> 0
AND s.dd_ReservationItemNo <> 0
AND r.RESB_RSNUM = s.dd_ReservationNo
AND r.RESB_RSPOS = s.dd_ReservationItemNo
AND ifnull(r.RESB_RSART, 'Not Set') = s.dd_RecordType 
AND ct_goodreceiptsprocessingdays <> ifnull(r.resb_webaz,0);

/*
DELETE FROM fact_shortage_temp
 WHERE dd_ReservationNo <> 0
 AND dd_ReservationItemNo <> 0
 AND EXISTS
          (SELECT 1
             FROM RESB r
            WHERE     r.RESB_RSNUM = dd_ReservationNo
                  AND r.RESB_RSPOS = dd_ReservationItemNo
				  AND ifnull(r.RESB_RSART, 'Not Set') = s.dd_RecordType
                  AND r.RESB_XLOEK = 'X') */

/* drop table if exists fact_shortage
rename table fact_shortage_temp to fact_shortage */


UPDATE fact_shortage s
SET s.dd_ProductionSequenceNo = po.dd_SequenceNo,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, fact_shortage s
WHERE s.dd_OrderNo = po.dd_OrderNumber
AND s.dd_ProductionSequenceNo <> po.dd_SequenceNo;

UPDATE fact_shortage s
SET s.dd_OrderItemNo = po.dd_OrderItemNo,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, fact_shortage s
WHERE s.dd_OrderNo = po.dd_OrderNumber
AND s.dd_OrderItemNo <> po.dd_OrderItemNo;

UPDATE fact_shortage s
SET s.Dim_Partid = po.Dim_PartIdItem,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, fact_shortage s
WHERE s.dd_OrderNo = po.dd_OrderNumber
AND s.Dim_Partid <> po.Dim_PartIdItem;

UPDATE fact_shortage s
SET s.Dim_Plantid = po.Dim_Plantid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, fact_shortage s
WHERE s.dd_OrderNo = po.dd_OrderNumber
AND s.Dim_Plantid <> po.Dim_Plantid;

UPDATE fact_shortage s
SET s.Dim_StorageLocationid = po.Dim_StorageLocationid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, fact_shortage s
WHERE s.dd_OrderNo = po.dd_OrderNumber
AND s.Dim_StorageLocationid <> po.Dim_StorageLocationid;

UPDATE fact_shortage s
SET s.dim_productionorderstatusid = po.dim_productionorderstatusid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, fact_shortage s
WHERE s.dd_OrderNo = po.dd_OrderNumber
AND s.dim_productionorderstatusid <> po.dim_productionorderstatusid;

UPDATE fact_shortage s
SET s.Dim_ProfitCenterId = po.Dim_ProfitCenterId,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, fact_shortage s
WHERE s.dd_OrderNo = po.dd_OrderNumber
AND s.Dim_ProfitCenterId <> po.Dim_ProfitCenterId;

UPDATE fact_shortage s
SET s.Dim_DateIdRequirement = po.Dim_DateIdBasicStart,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, fact_shortage s
WHERE s.dd_OrderNo = po.dd_OrderNumber
AND s.Dim_DateIdRequirement <> po.Dim_DateIdBasicStart;



merge into fact_shortage s 
using (select distinct s.fact_shortageid,first_value(sm.dim_shortagemiscid) over( partition by s.fact_shortageid order by s.fact_shortageid) as dim_shortagemiscid
FROM resb r, dim_shortagemisc sm, fact_shortage s
 WHERE  s.dd_ReservationNo <> 0
       AND s.dd_ReservationItemNo <> 0
       AND r.RESB_RSNUM = s.dd_ReservationNo
       AND r.RESB_RSPOS = s.dd_ReservationItemNo
	   AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
       AND sm.BulkMaterial = ifnull(r.RESB_SCHGT, 'Not Set')
       AND sm.CoProduct = ifnull(r.RESB_KZKUP, 'Not Set')
       AND sm.FinalIssue = ifnull(r.RESB_KZEAR, 'Not Set')
       AND sm.MissingPart = ifnull(r.RESB_XFEHL, 'Not Set')
       AND sm.PhantomItem = ifnull(r.RESB_DUMPS, 'Not Set') ) t
on t.fact_shortageid = s.fact_shortageid
when matched then update set s.dim_shortagemiscid = t.dim_shortagemiscid
where  s.dim_shortagemiscid <> t.dim_shortagemiscid;


/*UPDATE fact_shortage s
SET s.dd_plannedorderno = ifnull(r.RESB_PLNUM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM RESB r, fact_shortage s
WHERE r.RESB_RSNUM = s.dd_ReservationNo
AND r.RESB_RSPOS = s.dd_ReservationItemNo
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
AND s.dd_plannedorderno <> ifnull(r.RESB_PLNUM,'Not Set')*/


merge into fact_shortage s
using (select distinct s.fact_shortageid, r.RESB_PLNUM
FROM RESB r, fact_shortage s
WHERE r.RESB_RSNUM = s.dd_ReservationNo
AND r.RESB_RSPOS = s.dd_ReservationItemNo
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
AND r.RESB_PLNUM is not null
AND s.dd_plannedorderno <> ifnull(r.RESB_PLNUM,'Not Set')) t
on t.fact_shortageid=s.fact_shortageid
when matched then update set 
s.dd_plannedorderno = ifnull(t.RESB_PLNUM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP;

/*UPDATE fact_shortage s
SET s.dim_partmrpdataid = dp.dim_partid,
dw_update_date = CURRENT_TIMESTAMP
FROM RESB r, dim_part dp, fact_shortage s
WHERE r.RESB_RSNUM = s.dd_ReservationNo
AND r.RESB_RSPOS = s.dd_ReservationItemNo
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
AND ifnull(r.RESB_BAUGR,'Not Set') = dp.partnumber
AND ifnull(r.RESB_WERKS,'Not S') = dp.plant
AND s.dim_partmrpdataid <> dp.dim_partid*/

merge into fact_shortage s
using (select distinct s.fact_shortageid,dp.dim_partid
FROM RESB r, dim_part dp, fact_shortage s
WHERE r.RESB_RSNUM = s.dd_ReservationNo
AND r.RESB_RSPOS = s.dd_ReservationItemNo
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
AND ifnull(r.RESB_BAUGR,'Not Set') = dp.partnumber
AND ifnull(r.RESB_WERKS,'Not S') = dp.plant
---AND r.RESB_PLNUM is not null--removed(BI-5442)
AND s.dim_partmrpdataid <> dp.dim_partid) t
on t.fact_shortageid=s.fact_shortageid
when matched then update set s.dim_partmrpdataid = t.dim_partid,
dw_update_date = CURRENT_TIMESTAMP;


/* Madalina 27 Jun 2016 - Add field Item Deleted, based on RESB_XLOEK - BI-3237 */
update fact_shortage s
set dd_ItemDeleted = ifnull(RESB_XLOEK, 'Not Set'),
  dw_update_date = CURRENT_TIMESTAMP
from RESB r,fact_shortage s
where r.RESB_RSNUM = s.dd_ReservationNo
  AND r.RESB_RSPOS = s.dd_ReservationItemNo
  AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
  AND dd_ItemDeleted <> ifnull(RESB_XLOEK, 'Not Set');
/* End 27 Jun 2016  */

/* Madalina 28 Jun 2016 - Add field Execution Status Reservations - BI-3237 */
update fact_shortage fsg
set dd_ExecStatusReserv =
  ifnull(case when (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'Yes' and ct_QtyWithdrawn = 0 THEN 'Cancelled'
    when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'Yes' and ct_QtyWithdrawn <> 0 THEN 'Closed'
    when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'Yes' AND ct_QtyWithdrawn = 0 THEN 'Cancelled'
    when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'Yes' AND ct_QtyWithdrawn <> 0 THEN 'Closed'
    when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'No' AND ct_QtyRequired = 0 THEN 'Cancelled'
    when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'No' AND ct_QtyRequired <> 0 AND ct_QtyWithdrawn = 0 THEN 'Open'
  end, 'Not Set'),
  dw_update_date = current_timestamp
from dim_shortagemisc sh, fact_shortage fsg
where fsg.dim_shortagemiscid = sh.dim_shortagemiscid
  and dd_ExecStatusReserv <>
      ifnull(case when (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'Yes' and ct_QtyWithdrawn = 0 THEN 'Cancelled'
        when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'Yes' and ct_QtyWithdrawn <> 0 THEN 'Closed'
        when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'Yes' AND ct_QtyWithdrawn = 0 THEN 'Cancelled'
        when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'Yes' AND ct_QtyWithdrawn <> 0 THEN 'Closed'
        when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'No' AND ct_QtyRequired = 0 THEN 'Cancelled'
        when  (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'No' AND ct_QtyRequired <> 0 AND ct_QtyWithdrawn = 0 THEN 'Open'
      end, 'Not Set');

update fact_shortage fsg
set dd_ExecStatusReserv =
  ifnull(case when (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'No'
            and ((ct_QtyRequired - ct_QtyWithdrawn)/ ct_QtyRequired ) < 0.0001 THEN 'Closed'
        when (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'No'
            and not(((ct_QtyRequired - ct_QtyWithdrawn)/ct_QtyRequired ) < 0.0001)  THEN 'Partially Open'
  end, 'Not Set'),
  dw_update_date = current_timestamp
from dim_shortagemisc sh,fact_shortage fsg
where fsg.dim_shortagemiscid = sh.dim_shortagemiscid
  and ct_QtyRequired <> 0 AND ct_QtyWithdrawn <> 0
  and dd_ExecStatusReserv = 'Not Set'
  and dd_ExecStatusReserv <>
        ifnull(case when (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'No'
            and ((ct_QtyRequired - ct_QtyWithdrawn)/ ct_QtyRequired ) < 0.0001 THEN 'Closed'
        when (case when dd_itemdeleted = 'X' then 'Yes' else 'No' end) = 'No' and (case when sh.FinalIssue = 'X' then 'Yes' else 'No' end) = 'No'
            and not(((ct_QtyRequired - ct_QtyWithdrawn)/ct_QtyRequired ) < 0.0001) THEN 'Partially Open'
        end, 'Not Set');

/* END  28 Jun 2016 */

UPDATE fact_shortage s
   SET s.amt_StdPrice = 0.0
 WHERE s.dd_ControlCycleNo <> 'Not Set' AND s.dd_ControlCycleNo IS NOT NULL;

UPDATE fact_shortage
SET dd_plannedproductionorder = CASE WHEN dd_plannedorderno = 'Not Set' THEN 'Production Order' ELSE 'Planned Order' END
WHERE dd_plannedproductionorder <> CASE WHEN dd_plannedorderno = 'Not Set' THEN 'Production Order' ELSE 'Planned Order' END;

update fact_shortage
SET dd_ordernumber = CASE WHEN dd_plannedorderno = 'Not Set' THEN dd_orderno ELSE dd_plannedorderno END
WHERE dd_ordernumber <> CASE WHEN dd_plannedorderno = 'Not Set' THEN dd_orderno ELSE dd_plannedorderno END;




 /* Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */

UPDATE fact_shortage f
SET f.std_exchangerate_dateid = dt.dim_dateid
FROM fact_shortage f INNER JOIN dim_plant p ON f.dim_plantid = p.dim_plantid
     INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
WHERE f.dim_plantid = p.dim_plantid
AND   dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;


 /* END Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */
 
 /* Madalina 17 Aug 2016 - Update Batch Number based on RESB_CHARG. Also set default 'Not Set' instead of null */
update fact_shortage s
SET dd_BatchNumber = ifnull(r.RESB_CHARG,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
FROM RESB r, fact_shortage s
WHERE s.dd_ReservationNo <> 0
	AND s.dd_ReservationItemNo <> 0
	AND r.RESB_RSNUM = s.dd_ReservationNo
	AND r.RESB_RSPOS = s.dd_ReservationItemNo
	AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
	AND dd_BatchNumber <> ifnull(r.RESB_CHARG,'Not Set');
/* End 17 Aug 2016 */
 
/*UPDATE fact_shortage s
SET s.Dim_MovementTypeId = mt.dim_movementtypeid
FROM RESB r, fact_shortage s, dim_movementtype mt
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
AND mt.MovementType = r.RESB_BWART
AND mt.ConsumptionIndicator = ifnull(r.RESB_KZVBR, 'Not Set')
AND mt.MovementIndicator = 'Not Set'
AND mt.ReceiptIndicator = 'Not Set'
AND mt.SpecialStockIndicator = ifnull(r.RESB_SOBKZ, 'Not Set')
AND RowIsCurrent = 1
AND s.Dim_MovementTypeId <> mt.dim_movementtypeid*/

merge into fact_shortage s
using (select distinct s.dd_ReservationNo,s.dd_ReservationItemNo,s.dd_RecordType,mt.dim_movementtypeid
 FROM resb r, fact_shortage s, dim_movementtype mt
 WHERE s.dd_ReservationNo = r.RESB_RSNUM
 AND s.dd_ReservationItemNo = r.RESB_RSPOS
 AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
 AND mt.MovementType = r.RESB_BWART
 AND mt.ConsumptionIndicator = ifnull(r.RESB_KZVBR, 'Not Set')
 AND mt.MovementIndicator = 'Not Set'
 AND mt.ReceiptIndicator = 'Not Set'
 AND mt.SpecialStockIndicator = ifnull(r.RESB_SOBKZ, 'Not Set')
 AND RowIsCurrent = 1
 AND s.Dim_MovementTypeId <> mt.dim_movementtypeid) t
on t.dd_ReservationNo=s.dd_ReservationNo
and t.dd_ReservationItemNo=s.dd_ReservationItemNo
and t.dd_RecordType=s.dd_RecordType
when matched then update set s.dim_movementtypeid = t.dim_movementtypeid;

DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1_pre;
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
DROP TABLE IF EXISTS TMP_FS_MARD;
drop table if exists fact_shortage_temp;
DROP TABLE IF EXISTS fact_shortage_temp_1;
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
DROP TABLE IF EXISTS TMP_FS_MARD;
DROP TABLE IF EXISTS tmp_fact_shortage_getStdPrice;

  /*Alin 26 sept APP-7409 START*/
merge into fact_shortage f
using(
select distinct s.fact_shortageid, dd_reservationno, dd_reservationitemno, dp.partnumber, dp.plant, 
s.dd_orderno, s.dd_plannedorderno, 
IFNULL(r.RESB_AUSCH, 0) as amt_component_scrap
from fact_shortage s, RESB r, dim_part dp
where
dp.dim_partid = s.dim_componentid and
s.dd_orderno = ifnull(r.RESB_AUfnr, 'Not Set')
and dd_reservationitemno = ifnull(r.RESB_RSPOS,0)
and r.RESB_BDART = 'AR'
) t
on f.fact_shortageid = t.fact_shortageid
when matched then update set
f.amt_component_scrap = t.amt_component_scrap,
dw_update_date = CURRENT_TIMESTAMP
where f.amt_component_scrap <> t.amt_component_scrap;

merge into fact_shortage f
using(
select distinct s.fact_shortageid, dd_reservationno, dd_reservationitemno, dp.partnumber, dp.plant, 
s.dd_orderno, s.dd_plannedorderno, 
IFNULL(r.RESB_AUSCH, 0) as amt_operation_scrap
from fact_shortage s, RESB r, dim_part dp
where
dp.dim_partid = s.dim_componentid 
and s.dd_plannedorderno = ifnull(r.RESB_PLNUM, 'Not Set') 
and dd_reservationitemno = ifnull(r.RESB_RSPOS,0)
and r.RESB_BDART = 'SB'
) t
on f.fact_shortageid = t.fact_shortageid
when matched then update set
f.amt_operation_scrap = t.amt_operation_scrap,
dw_update_date = CURRENT_TIMESTAMP
where f.amt_operation_scrap <> t.amt_operation_scrap;
