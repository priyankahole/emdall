
DROP TABLE IF EXISTS tmp_fpo_updbom;
CREATE TABLE tmp_fpo_updbom
AS
SELECT f_deliv.dim_partid,f_deliv.dim_plantidordering dim_plantid,SUM(f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) fp_openqty
FROM fact_purchase f_deliv
GROUP BY f_deliv.dim_partid,f_deliv.dim_plantidordering;

UPDATE fact_bom f
SET f.ct_onorderqty = t.fp_openqty
FROM tmp_fpo_updbom t, fact_bom f
WHERE f.Dim_BOMComponentId = t.dim_partid AND f.Dim_MaterialPlantId = t.dim_plantid
AND fp_openqty > 0
AND f.ct_onorderqty <> t.fp_openqty;

DROP TABLE IF EXISTS tmp_latest_po1;
CREATE TABLE tmp_latest_po1
AS
SELECT f_deliv.dim_partid,f_deliv.dim_plantidordering ,max(dd.datevalue) latestorderdate
FROM fact_purchase f_deliv, dim_date dd
WHERE f_deliv.dim_dateidorder = dd.dim_dateid
GROUP BY f_deliv.dim_partid,f_deliv.dim_plantidordering;

DROP TABLE IF EXISTS tmp_latest_po;
CREATE TABLE tmp_latest_po
AS
SELECT fp.dim_partid, fp.dim_plantidordering,max(fp.amt_UnitPrice) amt_UnitPrice,  max(fp.dim_vendorid) dim_vendorid_latest
FROM fact_purchase fp,dim_date d, tmp_latest_po1 t
WHERE fp.dim_dateidorder = d.dim_dateid
AND fp.dim_partid = t.dim_partid AND fp.dim_plantidordering = t.dim_plantidordering
AND d.datevalue = t.latestorderdate
GROUP BY fp.dim_partid, fp.dim_plantidordering;

UPDATE fact_bom f
SET amt_LastActualPO = t.amt_UnitPrice
FROM tmp_latest_po t, fact_bom f
WHERE f.Dim_BOMComponentId = t.dim_partid AND f.Dim_MaterialPlantId = t.dim_plantidordering
AND f.amt_LastActualPO <> t.amt_UnitPrice;

UPDATE fact_bom f
SET dim_vendoridsupplierinfo = t.dim_vendorid_latest
FROM tmp_latest_po t,fact_bom f
WHERE f.Dim_BOMComponentId = t.dim_partid AND f.Dim_MaterialPlantId = t.dim_plantidordering
AND f.dim_vendoridsupplierinfo <> t.dim_vendorid_latest;

DROP TABLE IF EXISTS tmp_upd_bom_stock;
CREATE TABLE tmp_upd_bom_stock
AS
SELECT dim_partid,dim_plantid,
SUM(f_invagng.ct_StockQty + f_invagng.ct_StockInQInsp + f_invagng.ct_BlockedStock + f_invagng.ct_StockInTransit + f_invagng.ct_StockInTransfer + f_invagng.ct_TotalRestrictedStock-f_invagng.ct_StockInTransit) qtyonhand, 
sum(amt_OnHand) amt_OnHand
FROM fact_inventoryaging f_invagng
GROUP BY dim_partid,dim_plantid;


UPDATE fact_bom f
SET ct_invohqty = t.qtyonhand
FROM tmp_upd_bom_stock t,fact_bom f
WHERE f.Dim_BOMComponentId = t.dim_partid AND f.Dim_MaterialPlantId = t.dim_plantid
AND f.ct_invohqty <> t.qtyonhand;

UPDATE fact_bom f
SET amt_onhandstock = t.amt_OnHand
FROM tmp_upd_bom_stock t, fact_bom f
WHERE f.Dim_BOMComponentId = t.dim_partid AND f.Dim_MaterialPlantId = t.dim_plantid
AND f.amt_onhandstock <> t.amt_OnHand;

DROP TABLE IF EXISTS tmp_upd_ct_totaldemandqty_planord;
CREATE TABLE tmp_upd_ct_totaldemandqty_planord
AS
SELECT dim_partid,dim_plantid,sum( ct_QtyTotal) ct_totaldemandqty
FROM fact_planorder group by dim_partid,dim_plantid;

UPDATE fact_bom f
SET ct_totaldemandqty = t.ct_totaldemandqty
FROM tmp_upd_ct_totaldemandqty_planord t, fact_bom f
WHERE f.Dim_BOMComponentId = t.dim_partid AND f.Dim_MaterialPlantId = t.dim_plantid
AND f.ct_totaldemandqty <> t.ct_totaldemandqty;

/* 11.06.2015 add new measures from Part */
update fact_bom b
set ct_comp_grproctime  = ifnull(grproctimecaldays_merck,0)
from dim_part pt, fact_bom b
where dim_bomcomponentid = pt.dim_partid
and ct_comp_grproctime <> ifnull(grproctimecaldays_merck,0);

update fact_bom b
set ct_comp_planneddelivtime  = ifnull(leadtime,0)
from dim_part pt,  fact_bom b
where dim_bomcomponentid = pt.dim_partid
and  ct_comp_planneddelivtime <> ifnull(leadtime,0);


update fact_bom b
set ct_parent_grproctime  = ifnull(grproctimecaldays_merck,0)
from dim_part pt,  fact_bom b
where b.dim_rootpartid = pt.dim_partid
and ct_parent_grproctime <> ifnull(grproctimecaldays_merck,0);


update fact_bom b
set ct_parent_planneddelivtime  = ifnull(leadtime,0)
from dim_part pt,  fact_bom b
where b.dim_rootpartid = pt.dim_partid
and ct_parent_planneddelivtime  <> ifnull(leadtime,0);

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

UPDATE fact_bom bom
SET bom.ct_totvalstkQty = ifnull(b.MBEW_LBKUM,0)
FROM dim_part p,dim_plant pl,tmp2_MBEW_NO_BWTAR b, fact_bom bom
WHERE p.dim_partid = bom.dim_bomcomponentid AND pl.dim_plantid = bom.dim_materialplantid
AND b.MATNR = p.partnumber AND b.BWKEY = pl.ValuationArea
AND bom.ct_totvalstkQty <> ifnull(b.MBEW_LBKUM,0);


UPDATE fact_bom f
SET f.dim_partidimmediateparent = dp.dim_partid
FROM dim_part dp, dim_plant pl, fact_bom f
WHERE f.dd_immediateparentpart = dp.partnumber AND f.Dim_MaterialPlantId = pl.dim_plantid
AND pl.plantcode = dp.plant
AND f.dim_partidimmediateparent <> dp.dim_partid;

UPDATE fact_bom f
SET f.dim_partidimmediateparent = dp.dim_partid
FROM dim_part dp, dim_plant pl, fact_bom f
WHERE f.dd_immediateparentpart = dp.partnumber AND f.Dim_IssuingPlantId = pl.dim_plantid
AND pl.plantcode = dp.plant
AND f.dim_partidimmediateparent = 1
AND f.dim_partidimmediateparent <> dp.dim_partid;



UPDATE fact_bom b 
   SET dim_parentpartprodhierarchyid = dim_producthierarchyid
from dim_producthierarchy dph, dim_part dpr, fact_bom b 
 WHERE dph.ProductHierarchy = dpr.ProductHierarchy
       AND dpr.Dim_Partid = b.dim_partidimmediateparent
AND b.dim_parentpartprodhierarchyid <> dph.dim_producthierarchyid;

/*Ambiguous Replace fix*/
merge into fact_bom b
using (
select distinct b.fact_bomid
, v.dim_vendorid
 FROM eord e,dim_part dp, dim_vendor v, fact_bom b
 WHERE dp.PArtnumber = e.EORD_MATNR
 AND dp.Plant = e.EORD_WERKS
 AND dp.RowIsCurrent = 1
 AND v.VendorNumber = e.EORD_LIFNR
 AND e.EORD_FLIFN = 'X'
 AND v.RowIsCurrent = 1
 AND e.EORD_BDATU > current_date
 AND dp.Dim_PartId = b.dim_partidimmediateparent
 AND b.dim_vendoridimmediateparent <> v.dim_VendorId) t
on t.fact_bomid=b.fact_bomid
when matched then update set b.dim_vendoridimmediateparent = t.dim_VendorId;

/* Days of Supply */

DROP TABLE IF EXISTS tmp_upd_daysofsupply;
CREATE TABLE tmp_upd_daysofsupply
AS
SELECT f_invatlas.dim_partid,f_invatlas.dim_plantid,
(CASE WHEN SUM(f_invatlas.ct_available_stockqty) = 0 THEN 0 WHEN SUM(f_invatlas.ct_future_demandqty) = 0 and SUM(f_invatlas.ct_available_stockqty) > 0 THEN 47 WHEN (12*SUM(f_invatlas.ct_available_stockqty)/(CASE WHEN SUM(f_invatlas.ct_future_demandqty) <> 0 THEN SUM(f_invatlas.ct_future_demandqty) ELSE NULL END)) > 999 THEN 999 ELSE (12*SUM(f_invatlas.ct_available_stockqty)/(CASE WHEN SUM(f_invatlas.ct_future_demandqty) <> 0 THEN SUM(f_invatlas.ct_future_demandqty) ELSE NULL END)) END) months_of_supply
FROM fact_inventoryatlas f_invatlas
GROUP BY f_invatlas.dim_partid,f_invatlas.dim_plantid;

UPDATE fact_bom f
SET ct_daysofsupply = t.months_of_supply * 30
FROM tmp_upd_daysofsupply t,fact_bom f
WHERE f.Dim_BOMComponentId = t.dim_partid AND f.Dim_MaterialPlantId = t.dim_plantid
AND ct_daysofsupply <> t.months_of_supply * 30;


update fact_bom fb
SET dd_bomcomponentdescription = CASE WHEN bic.ItemCategory in ('N','T') THEN fb.dd_bomitemtext1 ELSE dp.PartDescription END
from dim_bomitemcategory bic, dim_part dp, fact_bom fb
where bic.Dim_BomItemCategoryId = fb.Dim_BomItemCategoryId
AND fb.Dim_BOMComponentId = dp.dim_partid
AND dd_bomcomponentdescription <> CASE WHEN bic.ItemCategory in ('N','T') THEN fb.dd_bomitemtext1 ELSE dp.PartDescription END;

