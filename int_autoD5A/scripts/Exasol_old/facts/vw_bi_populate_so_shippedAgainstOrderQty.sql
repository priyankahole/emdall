
drop table if exists tmp_so_002;
create table tmp_so_002 as
select distinct 
	f.dd_SalesDocNo,
	f.dd_SalesItemNo,
	f.dd_ScheduleNo,
	PGI.DateValue GI_Date,
	f.ct_ScheduleQtySalesUnit SchedQty,
	p.PlantCode
from fact_Salesorder f
inner join fact_SalesOrderDelivery sd on f.dd_SalesDocNo = sd.dd_SalesDocNo and f.dd_SalesItemNo = sd.dd_SalesItemNo
/* inner join likp_lips l on l.likp_vbeln = sd.dd_SalesDlvrDocNo and l.lips_posnr = sd.dd_Salesdlvritemno - TEMPORARY COMMENTED TO FIX VALUES */
inner join dim_salesorderitemstatus sois on sd.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
inner join dim_Date PGI on PGI.Dim_DateId = f.Dim_DateidGoodsIssue
inner join Dim_Plant p on p.Dim_Plantid = f.Dim_Plantid
where f.ct_ScheduleQtySalesUnit > 0 
	and f.dd_ItemRelForDelv = 'X' 
	and sois.GoodsMovementStatus <> 'Not yet processed';

drop table if exists tmp_so_001;
create table tmp_so_001 as
select f.dd_SalesDocNo,
	f.dd_SalesItemNo,
	f.dd_ScheduleNo,
	f.GI_Date,
	f.SchedQty,
	f.PlantCode,
	ROW_NUMBER() OVER(PARTITION BY f.dd_SalesDocNo, f.dd_SalesItemNo 
			  ORDER BY f.GI_Date, f.dd_ScheduleNo) RowSeqNo
from tmp_so_002 f;


drop table if exists tmp_sodlvr_001;
create table tmp_sodlvr_001 as
select fd.dd_SalesDocNo, 
	fd.dd_SalesItemNo,
	sum(fd.ct_QtyDelivered) TotQtyDelivered
from fact_salesorderdelivery fd
where exists (select 1 from tmp_so_001 a
		where a.dd_SalesDocNo = fd.dd_SalesDocNo
			and a.dd_SalesItemNo = fd.dd_SalesItemNo)
group by fd.dd_SalesDocNo, fd.dd_SalesItemNo;


drop table if exists tmp_socummqty_001;
create table tmp_socummqty_001 as
select VBLB_VBELN c_SalesDocNo, 
	VBLB_POSNR c_SalesItemNo,
	max(VBLB_ABEFZ) c_TotalCmlRcvdQty
from VBLB
group by VBLB_VBELN, VBLB_POSNR;


drop table if exists tmp_so_maxseqno_001;
create table tmp_so_maxseqno_001 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	max(a.RowSeqNo) MaxRowSeqNo
from tmp_so_001 a
group by a.dd_SalesDocNo,
	a.dd_SalesItemNo;


drop table if exists tmp_so_002;
create table tmp_so_002 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo,
	sum(b.SchedQty) RunnSchedQty
from tmp_so_001 a 
	inner join tmp_so_001 b 
	on (a.dd_SalesDocNo = b.dd_SalesDocNo
		and a.dd_SalesItemNo = b.dd_SalesItemNo)
where a.RowSeqNo >= b.RowSeqNo
group by a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo;


drop table if exists tmp_so_001;
create table tmp_so_001 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo,
	a.RunnSchedQty,
	b.TotQtyDelivered,
	ifnull(c.c_TotalCmlRcvdQty,0) TotalCmlRcvdQty,
	m.MaxRowSeqNo
from tmp_so_002 a 
	inner join tmp_so_maxseqno_001 m on a.dd_SalesDocNo = m.dd_SalesDocNo and a.dd_SalesItemNo = m.dd_SalesItemNo
	inner join tmp_sodlvr_001 b on a.dd_SalesDocNo = b.dd_SalesDocNo and a.dd_SalesItemNo = b.dd_SalesItemNo
	left join tmp_socummqty_001 c on a.dd_SalesDocNo = c.c_SalesDocNo and a.dd_SalesItemNo = c.c_SalesItemNo;


drop table if exists tmp_so_002;
create table tmp_so_002 as 
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	case when a.TotQtyDelivered > (a.RunnSchedQty - a.SchedQty)
		then case when a.TotQtyDelivered >= a.RunnSchedQty and a.RowSeqNo < a.MaxRowSeqNo
			then a.SchedQty
			else a.TotQtyDelivered - (a.RunnSchedQty - a.SchedQty)
		     end
	     else 0
	end ShippedAgnstOrderQty,
	case when a.TotalCmlRcvdQty > (a.RunnSchedQty - a.SchedQty)
		then case when a.TotalCmlRcvdQty >= a.RunnSchedQty and a.RowSeqNo < a.MaxRowSeqNo
			then a.SchedQty
			else a.TotalCmlRcvdQty - (a.RunnSchedQty - a.SchedQty)
		     end
	     else 0
	end CmlQtyReceived
from tmp_so_001 a;


UPDATE fact_Salesorder f
      SET f.ct_ShippedAgnstOrderQty = a.ShippedAgnstOrderQty,
          f.ct_CmlQtyReceived = a.CmlQtyReceived
FROM tmp_so_002 a, fact_Salesorder f
    WHERE     f.dd_SalesDocNo = a.dd_SalesDocNo
          AND f.dd_SalesItemNo = a.dd_SalesItemNo
          AND f.dd_ScheduleNo = a.dd_ScheduleNo;

drop table if exists tmp_so_001;
drop table if exists tmp_sodlvr_001;
drop table if exists tmp_socummqty_001;
drop table if exists tmp_so_maxseqno_001;
drop table if exists tmp_so_002;
