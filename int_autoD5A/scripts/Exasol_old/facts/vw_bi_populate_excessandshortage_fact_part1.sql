Drop table if exists pGlobalCurrency_po_41;

Create table pGlobalCurrency_po_41(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_41(pGlobalCurrency) values(null);

UPDATE pGlobalCurrency_po_41 p
SET p.pGlobalCurrency = ifNULL(s.property_value,'USD')
FROM pGlobalCurrency_po_41 p
CROSS JOIN (SELECT property_value
           FROM systemproperty 
   WHERE  property = 'customer.global.currency' ) s;


UPDATE fact_excessandshortage mrp
SET ct_Completed = 1
	,mrp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_excessandshortage mrp
	INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
	INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = mrp.Dim_MRPElementid
	INNER JOIN dim_date dn ON mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
WHERE     Dim_DateidActionClosed = 1
       AND mrp.dd_documentno <> 'Not Set'
	AND ct_Completed <> 1
       AND NOT EXISTS
                  (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE     mrp.dd_documentno = t.MDTB_DELNR
                          AND mrp.dd_documentitemno = t.MDTB_DELPS
                          AND mrp.dd_scheduleno = t.MDTB_DELET
                          AND t.MDTB_DELNR IS NOT NULL
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), to_date('0001-01-01','YYYY-MM-DD'))
                          AND mrp.Dim_ActionStateid = 2);



UPDATE fact_excessandshortage mrp
   SET Dim_ActionStateid = 3
	,mrp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  fact_excessandshortage mrp
	INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
	INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = mrp.Dim_MRPElementid
	INNER JOIN dim_date dn ON mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
 WHERE     Dim_DateidActionClosed = 1
       AND mrp.dd_documentno <> 'Not Set'
	AND Dim_ActionStateid <> 3
       AND NOT EXISTS
                  (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE     mrp.dd_documentno = t.MDTB_DELNR
                          AND mrp.dd_documentitemno = t.MDTB_DELPS
                          AND mrp.dd_scheduleno = t.MDTB_DELET
                          AND t.MDTB_DELNR IS NOT NULL
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), to_date('0001-01-01','YYYY-MM-DD'))
                          AND mrp.Dim_ActionStateid = 2);



UPDATE fact_excessandshortage mrp
   SET Dim_DateidActionClosed = dt.Dim_Dateid
	,mrp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_excessandshortage mrp
	INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
	INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = mrp.Dim_MRPElementid
	INNER JOIN dim_DATE dn ON mrp.Dim_DATEidDATENeeded = dn.Dim_DATEid
    INNER JOIN dim_company cc ON cc.Dim_Companyid = mrp.Dim_Companyid
    INNER JOIN dim_plant pl ON pl.dim_plantid = mrp.dim_plantid
	INNER JOIN dim_DATE dt ON cc.CompanyCode = dt.CompanyCode AND dt.DATEValue = current_DATE
	AND pl.plantcode = dt.plantcode_factory

 WHERE     Dim_DateidActionClosed = 1
       AND mrp.dd_documentno <> 'Not Set'
       AND NOT EXISTS
                  (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE     mrp.dd_documentno = t.MDTB_DELNR
                          AND mrp.dd_documentitemno = t.MDTB_DELPS
                          AND mrp.dd_scheduleno = t.MDTB_DELET
                          AND t.MDTB_DELNR IS NOT NULL
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03 
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG 
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), to_date('0001-01-01','YYYY-MM-DD'))
                          AND mrp.Dim_ActionStateid = 2)
AND Dim_DateidActionClosed <> dt.Dim_Dateid;

                         

UPDATE fact_excessandshortage mrp
   SET ct_Completed = 1
	,mrp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_excessandshortage mrp 
	INNER JOIN dim_part dp ON mrp.Dim_Partid = dp.Dim_Partid
	INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
	INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = mrp.Dim_MRPElementid
	INNER JOIN dim_DATE dn ON mrp.Dim_DATEidDATENeeded = dn.Dim_DATEid
 WHERE     Dim_DateidActionClosed = 1
       AND mrp.dd_documentno ='Not Set'
	AND ct_Completed <> 1
       AND NOT EXISTS
                  (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE dp.PartNumber = m.MDKP_MATNR
                          AND dp.Plant = m.MDKP_PLWRK
                          and t.MDTB_DELNR is null
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), to_date('0001-01-01','YYYY-MM-DD'))
                          AND mrp.Dim_ActionStateid = 2);




UPDATE fact_excessandshortage mrp
   SET Dim_ActionStateid = 3
	,mrp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_excessandshortage mrp
	INNER JOIN dim_part dp ON mrp.Dim_Partid = dp.Dim_Partid
	INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
	INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = mrp.Dim_MRPElementid
	INNER JOIN dim_DATE dn ON mrp.Dim_DATEidDATENeeded = dn.Dim_DATEid
 WHERE     Dim_DateidActionClosed = 1
       AND mrp.dd_documentno ='Not Set'
	AND Dim_ActionStateid <> 3
       AND NOT EXISTS
                  (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE dp.PartNumber = m.MDKP_MATNR
                          AND dp.Plant = m.MDKP_PLWRK
                          and t.MDTB_DELNR is null
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), To_DATE('0001-01-01','YYYY-MM-DD'))
                          AND mrp.Dim_ActionStateid = 2);


UPDATE fact_excessandshortage mrp
   SET Dim_DateidActionClosed = dt.dim_dateid
	,mrp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_excessandshortage mrp
	INNER JOIN dim_part dp ON mrp.Dim_Partid = dp.Dim_Partid
	INNER JOIN dim_mrpexception mex ON mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
	INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = mrp.Dim_MRPElementid
	INNER JOIN dim_DATE dn ON mrp.Dim_DATEidDATENeeded = dn.Dim_DATEid
	INNER JOIN dim_company cc ON cc.Dim_Companyid = mrp.Dim_Companyid
	INNER JOIN dim_plant pl ON pl.dim_plantid = mrp.dim_plantid
	INNER JOIN dim_DATE dt ON cc.CompanyCode = dt.CompanyCode AND dt.DATEValue = current_DATE
	AND pl.plantcode = dt.plantcode_factory
 WHERE     Dim_DateidActionClosed = 1
       AND mrp.dd_documentno = 'Not Set'
       AND NOT EXISTS
                  (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE dp.PartNumber = m.MDKP_MATNR
                          AND dp.Plant = m.MDKP_PLWRK
                          and t.MDTB_DELNR is null
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03 
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG 
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), to_date('0001-01-01','YYYY-MM-DD'))
                          AND mrp.Dim_ActionStateid = 2)
AND Dim_DateidActionClosed <> dt.dim_dateid ;



DELETE FROM fact_excessandshortage
 WHERE EXISTS
          (SELECT 1
             FROM dim_date d
            WHERE d.Dim_Dateid = Dim_DateidMRP
                  AND d.DateValue < (current_date -  ( INTERVAL '3' MONTH)))
       AND ct_Completed = 1;

Drop table if exists max_holder_41;

Create table max_holder_41
as
select 	ifnull(max(f.fact_excessandshortageid), 
	       ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_excessandshortage f;

drop table if exists fact_excessandshortage_sub41;

Create table fact_excessandshortage_sub41 as
Select mrp.Dim_Partid,
     mrp.dim_mrpexceptionID1,
     mrp.Dim_MRPElementid,
     mrp.ct_QtyMRP,
     mrp.ct_QtyShortage,
     mrp.ct_QtyExcess ,
     mrp.dd_documentno,
     mrp.dd_documentitemno, 
     mrp.dd_scheduleno,
     mrp.Dim_DateidDateNeeded 
From fact_excessandshortage mrp
Where  mrp.dd_documentno <> 'Not Set'
AND mrp.Dim_ActionStateid = 2;

Drop table if exists fact_excessandshortage_tmp_41;
CREATE TABLE fact_excessandshortage_tmp_41 
LIKE  fact_excessandshortage  INCLUDING DEFAULTS INCLUDING IDENTITY; 

ALTER TABLE fact_excessandshortage_tmp_41 ADD PRIMARY KEY (fact_excessandshortageid);

ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_OLDSL_upd VARCHAR(5) ;
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_DAT01_upd DATE;
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN CompanyCode_upd VARCHAR(20);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_LGORT_upd VARCHAR(5);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN plantcode_upd VARCHAR(20);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_EKGRP_upd VARCHAR(3);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN mdkp_kzaus_upd VARCHAR(1);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_SOBES_upd VARCHAR(1);	
	

ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_DELKZ VARCHAR(7);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_AUSSL VARCHAR(7);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN currency VARCHAR(25);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN DATEvalue_upd DATE;
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_dsdat DATE;
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_PLWRK VARCHAR(7);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_MATNR VARCHAR(18);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_MEINS VARCHAR(7);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN PurchOrg VARCHAR(7);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_DISVF VARCHAR(7);		

Drop table if exists mdtb_pi00;

CREATE TABLE mdtb_pi00 AS 
SELECT MDTB_DAT00,	MDTB_DAT01,	MDTB_DAT02,	MDTB_DAT03,	MDTB_UMDAT,	MDTB_PLWRK,	MDTB_DTNUM,	MDTB_MNG01,	MDTB_MNG02,	MDTB_MNG03,	MDTB_AUSSL,	
MDTB_OLDSL,	MDTB_WRK02,	MDTB_DELNR,	MDTB_DELPS,	MDTB_DELET,	MDTB_LGORT,	MDTB_BESKZ,	MDTB_DTPOS,	MDTB_DELKZ,	MDTB_PLUMI,	MDTB_WRK01,	MDTB_MANDT,
MDTB_PLAAB,	MDTB_PRPER,	MDTB_BAART,	MDTB_WEBAZ,	MDTB_BAUGR,	MDTB_SERNR,	MDTB_PERKZ,	MDTB_SOBES,	MDTB_RDMNG,	MDTB_EINVR,	MDTB_POSVR,	MDTB_AUFVR,	
MDTB_VSTAT,	MDTB_DELVR,	MDTB_DEL12,	MDTB_PLART,IFNULL(MDTB_UMDAT, MDTB_DAT00) DATEvalue_upd
FROM mdtb 
WHERE (MDTB_MNG03 > 0 OR MDTB_RDMNG > 0) and MDTB_DELNR IS NOT NULL
AND IFNULL(MDTB_UMDAT, MDTB_DAT00) IS NOT NULL
AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI');					  

INSERT INTO fact_excessandshortage_tmp_41( 
			 fact_excessandshortageid,
		     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
			MDTB_OLDSL_upd,
			MDTB_DAT01_upd,
			CompanyCode_upd,
			MDTB_LGORT_upd,
			plantcode_upd,
			MDKP_EKGRP_upd,
			mdkp_kzaus_upd,
			MDTB_SOBES_upd )
	SELECT mx41.maxid + ROW_NUMBER() over(ORDER BY '') fact_excessandshortageid,
		  CONVERT(bigint,1) AS Dim_MRPElementid,
          CONVERT(bigint,1) AS dim_mrpexceptionID1,
	      CONVERT(bigint,1) AS dim_mrpexceptionID2,
          dc.Dim_Companyid,
          CONVERT(bigint,1) AS Dim_Currencyid,
          CONVERT(bigint,1) AS Dim_DATEidActionRequired,
          CONVERT(bigint,1) AS Dim_DATEidActionClosed,
          CONVERT(bigint,1) AS Dim_DATEidDATENeeded,
          CONVERT(bigint,1) AS Dim_DATEidOriginalDock,
          CONVERT(bigint,1) AS Dim_DATEidMRP,
          CONVERT(bigint,1) AS Dim_Partid,
          CONVERT(bigint,1) AS Dim_StorageLocationid,
          pl.Dim_Plantid,
          CONVERT(bigint,1) AS Dim_UnitOfMeasureid,
          CONVERT(bigint,1) AS Dim_PurchaseGroupid,
          CONVERT(bigint,1) AS Dim_PurchaseOrgid,
          CONVERT(bigint,1) AS Dim_Vendorid,
          CONVERT(bigint,2) AS Dim_ActionStateid,
          CONVERT(bigint,1) AS Dim_ItemCategoryid,
          CONVERT(bigint,1) AS Dim_DocumentTypeid,
          CONVERT(bigint,1) AS Dim_MRPProcedureid,
          CONVERT(bigint,1) AS Dim_MRPDiscontinuationIndicatorid,
          CONVERT(bigint,1) AS Dim_specialprocurementid,
          CONVERT(bigint,1) AS Dim_FixedVendorid,
          CONVERT(bigint,1) AS Dim_ConsumptionTypeid,
          t.MDTB_MNG01 ct_QtyMRP,
          t.MDTB_MNG03 ct_QtyShortage,
          t.MDTB_RDMNG ct_QtyExcess,
          m.MDKP_BERW2 ct_DaysReceiptCoverage,
          m.MDKP_BERW1 ct_DaysStockCoverage,
          IFNULL(t.mdtb_delnr,'Not Set') dd_DocumentNo,
          t.mdtb_delps dd_DocumentItemNo,
          t.mdtb_delet dd_ScheduleNo,
          t.mdtb_dtnum dd_mrptablenumber,
          t.mdtb_baugr dd_peggedrequirement,
          t.mdtb_sernr dd_bomexplosionno,
		  t.MDTB_OLDSL,
		  t.MDTB_DAT01,
		  dc.CompanyCode,
		  t.MDTB_LGORT,
		  pl.plantcode,
		  m.MDKP_EKGRP,
		  m.mdkp_kzaus,
	 t.MDTB_SOBES
     FROM max_holder_41 mx41 CROSS JOIN mdtb_pi00 t        
          INNER JOIN mdkp m
             ON t.MDTB_DTNUM = m.MDKP_DTNUM
          INNER JOIN dim_plant pl
             ON m.MDKP_PLWRK = pl.PlantCode AND pl.RowIsCurrent = 1
          INNER JOIN dim_company dc
             ON pl.CompanyCode = dc.CompanyCode AND dc.RowIsCurrent = 1;

DROP TABLE IF EXISTS mdtb_pi00;			 

UPDATE fact_excessandshortage_tmp_41 f
SET f.Dim_MRPElementid = IFNULL(me.Dim_MRPElementid,1) 
FROM fact_excessandshortage_tmp_41 f
	 LEFT JOIN dim_mrpelement me
             ON f.MDTB_DELKZ = me.MRPElement AND me.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 f
SET f.dim_mrpexceptionID1 = mex.dim_mrpexceptionid 
FROM fact_excessandshortage_tmp_41 f
	 LEFT JOIN dim_mrpexception mex
             ON IFNULL(f.MDTB_AUSSL, 'Not Set') = mex.exceptionkey AND mex.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 f
SET f.Dim_CurrencyId = IFNULL(c.Dim_Currencyid,1)
FROM fact_excessandshortage_tmp_41 f
LEFT JOIN  dim_currency c
			 ON c.currencycode = f.currency;
			 
/* Updates multiple columns: Dim_DATEidDATENeeded, Dim_DATEidActionRequired, Dim_DateidMRP, Dim_DateidOriginalDock */
MERGE INTO fact_excessandshortage_tmp_41 fact 
USING (SELECT fact_excessandshortageid, 
	          IFNULL(dn.dim_DATEid,1) Dim_DATEidDATENeeded,
			  IFNULL(ar.dim_DATEid,1) Dim_DATEidActionRequired,
			  IFNULL(mrpd.Dim_DATEid,1) Dim_DATEidMRP,
			  IFNULL(od.dim_dateid,1) Dim_DateidOriginalDock
	   FROM (SELECT t1.fact_excessandshortageid, dc.companycode, pl.plantcode, t1.datevalue_upd,
					t1.MDKP_dsdat, t1.MDTB_DAT01_upd
			 FROM fact_excessandshortage_tmp_41 t1
			 INNER JOIN dim_company dc 
				ON t1.Dim_Companyid = dc.Dim_Companyid
			 INNER JOIN dim_plant pl ON t1.dim_plantid = pl.dim_plantid) f
	   LEFT JOIN  dim_DATE ar
			 ON ar.DATEValue = f.DATEvalue_upd AND ar.CompanyCode = f.CompanyCode
			 AND ar.plantcode_factory = f.plantcode
	   LEFT JOIN  dim_DATE dn
			 ON dn.DATEValue = current_DATE AND dn.CompanyCode = f.CompanyCode
			 AND dn.plantcode_factory = f.plantcode
	   LEFT JOIN  dim_DATE mrpd
			 ON mrpd.DATEValue = f.MDKP_dsdat AND mrpd.CompanyCode = f.CompanyCode
			 AND mrpd.plantcode_factory = f.plantcode
	   LEFT JOIN  dim_DATE od
			 ON od.DATEValue = f.MDTB_DAT01_upd AND od.CompanyCode = f.CompanyCode
			 AND od.plantcode_factory = f.plantcode
	   )  src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_DATEidDATENeeded = src.Dim_DATEidDATENeeded,
							 fact.Dim_DATEidActionRequired = src.Dim_DATEidActionRequired,
							 fact.Dim_DATEidMRP = src.Dim_DATEidMRP,
							 fact.Dim_DateidOriginalDock = src.Dim_DateidOriginalDock;	
							 
UPDATE fact_excessandshortage_tmp_41 f
SET f.dim_Partid = IFNULL(dp.Dim_Partid,1)
FROM fact_excessandshortage_tmp_41 f
LEFT JOIN dim_part dp
	  ON f.MDKP_PLWRK = dp.Plant AND f.MDKP_MATNR = dp.PartNumber AND dp.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 f
SET f.Dim_UnitOfMeasureid = IFNULL (uom.Dim_UnitOfMeasureid,1)
FROM fact_excessandshortage_tmp_41 f
LEFT JOIN dim_unitofmeasure uom
	  ON uom.UOM = f.MDKP_MEINS AND uom.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 f
SET f.Dim_PurchaseOrgid = IFNULL(po.Dim_PurchaseOrgid,1)
FROM fact_excessandshortage_tmp_41 f
LEFT JOIN Dim_PurchaseOrg po
			 ON f.PurchOrg = po.PurchaseOrgCode AND po.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 f
SET f.Dim_MRPProcedureid = IFNULL(pr.Dim_MRPProcedureid,1)
FROM fact_excessandshortage_tmp_41 f
LEFT JOIN dim_mrpprocedure pr
			 ON f.MDKP_DISVF = pr.MRPProcedure AND pr.RowIsCurrent = 1;
			 
UPDATE fact_excessandshortage_tmp_41 sb
SET dim_mrpexceptionID2 = IFNULL(ex.dim_mrpexceptionid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_mrpexception ex 
	  ON sb.MDTB_OLDSL_upd = ex.exceptionkey AND ex.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 sb
SET sb.Dim_StorageLocationid = IFNULL(sl.Dim_StorageLocationid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_storagelocation sl
      ON sl.LocationCode = sb.MDTB_LGORT_upd AND sl.Plant = sb.plantcode_upd
WHERE sb.Dim_StorageLocationid <> IFNULL(sl.Dim_StorageLocationid,1);

UPDATE fact_excessandshortage_tmp_41 sb
SET sb.Dim_PurchaseGroupid =  IFNULL(pg.Dim_PurchaseGroupid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_purchasegroup pg
      ON pg.PurchaseGroup = sb.MDKP_EKGRP_upd AND pg.RowIsCurrent = 1
WHERE sb.Dim_PurchaseGroupid <>  IFNULL(pg.Dim_PurchaseGroupid,1) ;

UPDATE fact_excessandshortage_tmp_41 sb
SET sb.Dim_MRPDiscontinuationIndicatorid =  IFNULL(di.dim_mrpdiscontinuationindicatorid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_mrpdiscontinuationindicator di
      ON sb.mdkp_kzaus_upd = di."indicator" AND di.RowIsCurrent = 1
WHERE sb.Dim_MRPDiscontinuationIndicatorid <>  IFNULL(di.dim_mrpdiscontinuationindicatorid,1);

UPDATE fact_excessandshortage_tmp_41 sb
SET sb.Dim_specialprocurementid = IFNULL(sp.Dim_specialprocurementid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_specialprocurement sp
      ON sb.MDTB_SOBES_upd = sp.specialprocurement AND sp.RowIsCurrent = 1
WHERE sb.Dim_specialprocurementid <> IFNULL(sp.Dim_specialprocurementid,1);	
		

DROP TABLE IF EXISTS eands_pe01;
CREATE TABLE eands_pe01 AS 
SELECT FACT_EXCESSANDSHORTAGEID,	DIM_MRPELEMENTID,	DIM_MRPEXCEPTIONID1,	DIM_MRPEXCEPTIONID2,	DIM_COMPANYID,	DIM_CURRENCYID,	DIM_DATEIDACTIONREQUIRED,	DIM_DATEIDACTIONCLOSED,	DIM_DATEIDDATENEEDED,	
DIM_DATEIDORIGINALDOCK,	DIM_DATEIDMRP,	DIM_PARTID,	DIM_STORAGELOCATIONID,	DIM_PLANTID,	DIM_UNITOFMEASUREID,	DIM_PURCHASEGROUPID,	DIM_PURCHASEORGID,	DIM_VENDORID,	DIM_ACTIONSTATEID,	
DIM_ITEMCATEGORYID,	DIM_DOCUMENTTYPEID,	DIM_MRPPROCEDUREID,	DIM_MRPDISCONTINUATIONINDICATORID,	DIM_SALESDOCUMENTTYPEID,	DIM_SPECIALPROCUREMENTID,	DIM_CONSUMPTIONTYPEID,	CT_QTYMRP,	CT_QTYOPENORDER,	
CT_QTYSHORTAGE,	CT_QTYEXCESS,	CT_DAYSRECEIPTCOVERAGE,	CT_DAYSSTOCKCOVERAGE,	CT_COMPLETED,	AMT_UNITPRICE,	DD_DOCUMENTNO,	DD_DOCUMENTITEMNO,	DD_SCHEDULENO,	DD_MRPTABLENUMBER,	DD_PEGGEDREQUIREMENT,	
DD_BOMEXPLOSIONNO,	DIRTYROW,	DIM_FIXEDVENDORID,	DIM_CUSTOMERGROUP1ID,	DIM_CUSTOMERID,	DIM_DOCUMENTCATEGORYID,	DIM_SALESORDERREJECTREASONID,	DIM_SALESORDERITEMSTATUSID,	DIM_SALESORDERHEADERSTATUSID,	
AMT_UNITPRICE_GBL,	DIM_TOPASSEMBLYPARTID,	DD_POORDERTYPE,	AMT_EXCHANGERATE,	AMT_EXCHANGERATE_GBL,	DW_INSERT_DATE,	DW_UPDATE_DATE,	DIM_PROJECTSOURCEID,	MDTB_OLDSL_UPD,	MDTB_DAT01_UPD,	COMPANYCODE_UPD,	
MDTB_LGORT_UPD,	PLANTCODE_UPD,	MDKP_EKGRP_UPD,	MDKP_KZAUS_UPD,	MDTB_SOBES_UPD
FROM fact_excessandshortage_tmp_41 WHERE 1=2;

INSERT into eands_pe01(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
                        MDTB_OLDSL_upd,
                        MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
			MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd)
Select ne.fact_excessandshortageid,
                     ne.Dim_MRPElementid,
                     ne.dim_mrpexceptionID1,
                     ne.dim_mrpexceptionID2,
                     ne.Dim_Companyid,
                     ne.Dim_Currencyid,
                     ne.Dim_DateidActionRequired,
                     ne.Dim_DateidActionClosed,
                     ne.Dim_DateidDateNeeded,
                     ne.Dim_DateidOriginalDock,
                     ne.Dim_DateidMRP,
                     ne.Dim_Partid,
                     ne.Dim_StorageLocationid,
                     ne.Dim_Plantid,
                     ne.Dim_UnitOfMeasureid,
                     ne.Dim_PurchaseGroupid,
                     ne.Dim_PurchaseOrgid,
                     ne.Dim_Vendorid,
                     ne.Dim_ActionStateid,
                     ne.Dim_ItemCategoryid,
                     ne.Dim_DocumentTypeid,
                     ne.Dim_MRPProcedureid,
                     ne.Dim_MRPDiscontinuationIndicatorid,
                     ne.Dim_specialprocurementid,
                     ne.Dim_FixedVendorid,
                     ne.Dim_ConsumptionTypeid,
                     ne.ct_QtyMRP,
                     ne.ct_QtyShortage,
                     ne.ct_QtyExcess,
                     ne.ct_DaysReceiptCoverage,
                     ne.ct_DaysStockCoverage,
                     ne.dd_DocumentNo,
                     ne.dd_DocumentItemNo,
                     ne.dd_ScheduleNo,
                     ne.dd_mrptablenumber,
                     ne.dd_peggedrequirement,
                     ne.dd_bomexplosionno,
                        ne.MDTB_OLDSL_upd,
                        ne.MDTB_DAT01_upd,
                        ne.CompanyCode_upd,
                        ne.MDTB_LGORT_upd,
                        ne.plantcode_upd,
			ne.MDKP_EKGRP_upd,
                        ne.mdkp_kzaus_upd,
                        ne.MDTB_SOBES_upd
From fact_excessandshortage_tmp_41 ne
INNER JOIN fact_excessandshortage_sub41 mrp
ON mrp.Dim_Partid = ne.Dim_Partid
AND mrp.dim_mrpexceptionID1 = ne.dim_mrpexceptionID1
AND mrp.Dim_MRPElementid = ne.Dim_MRPElementid
AND mrp.ct_QtyMRP = ne.ct_QtyMRP
AND mrp.ct_QtyShortage = ne.ct_QtyShortage
AND mrp.ct_QtyExcess = ne.ct_QtyExcess
AND mrp.dd_documentno = ne.dd_documentno
AND mrp.dd_documentitemno = ne.dd_documentitemno
AND mrp.dd_scheduleno = ne.dd_scheduleno 
AND mrp.Dim_DateidDateNeeded = ne.Dim_DateidDateNeeded;

MERGE INTO fact_excessandshortage_tmp_41 t1
USING (select distinct fact_excessandshortageid from eands_pe01) t2
ON t1.fact_excessandshortageid = t2.fact_excessandshortageid	
WHEN MATCHED THEN DELETE; 					  
						  
INSERT INTO fact_excessandshortage( fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno)
SELECT max_holder_41.maxid + ROW_NUMBER() over(ORDER BY ''),
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
FROM fact_excessandshortage_tmp_41
CROSS JOIN max_holder_41;
						  
DROP TABLE IF EXISTS eands_pe01;
DROP TABLE IF EXISTS fact_excessandshortage_sub41;
DROP TABLE IF EXISTS fact_excessandshortage_tmp_41;
DROP TABLE IF EXISTS max_holder_41;						  
						  
						  
CREATE TABLE max_holder_41
AS
SELECT IFNULL(max(fact_excessandshortageid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) AS maxid
FROM fact_excessandshortage;							  
			  
						  
CREATE TABLE fact_excessandshortage_sub41 AS
SELECT   mrp.Dim_Partid ,
	     mrp.dim_mrpexceptionID1,
	     mrp.Dim_MRPElementid,
	     mrp.ct_QtyMRP ,
	     mrp.ct_QtyShortage,
	     mrp.ct_QtyExcess,
	     mrp.Dim_DateidDateNeeded 
FROM fact_excessandshortage mrp
Where mrp.dd_documentno = 'Not Set'
AND  mrp.Dim_ActionStateid = 2;

CREATE TABLE fact_excessandshortage_tmp_41 LIKE  fact_excessandshortage INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE fact_excessandshortage_tmp_41 ADD PRIMARY KEY (fact_excessandshortageid);

ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_OLDSL_upd VARCHAR(5);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_DAT01_upd DATE;
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN CompanyCode_upd VARCHAR(20);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_LGORT_upd VARCHAR(5);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN plantcode_upd VARCHAR(20);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_EKGRP_upd VARCHAR(3);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN mdkp_kzaus_upd VARCHAR(1);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_SOBES_upd VARCHAR(1);

DROP TABLE IF EXISTS mdtb_pi00;
CREATE TABLE mdtb_pi00 AS 
SELECT MDTB_DAT00,	MDTB_DAT01,	MDTB_DAT02,	MDTB_DAT03,	MDTB_UMDAT,	MDTB_PLWRK,	MDTB_DTNUM,	MDTB_MNG01,	MDTB_MNG02,	MDTB_MNG03,	MDTB_AUSSL,	
MDTB_OLDSL,	MDTB_WRK02,	MDTB_DELNR,	MDTB_DELPS,	MDTB_DELET,	MDTB_LGORT,	MDTB_BESKZ,	MDTB_DTPOS,	MDTB_DELKZ,	MDTB_PLUMI,	MDTB_WRK01,	MDTB_MANDT,
MDTB_PLAAB,	MDTB_PRPER,	MDTB_BAART,	MDTB_WEBAZ,	MDTB_BAUGR,	MDTB_SERNR,	MDTB_PERKZ,	MDTB_SOBES,	MDTB_RDMNG,	MDTB_EINVR,	MDTB_POSVR,	MDTB_AUFVR,	
MDTB_VSTAT,	MDTB_DELVR,	MDTB_DEL12,	MDTB_PLART, IFNULL(MDTB_UMDAT, MDTB_DAT00) DATEvalue_upd 
FROM mdtb 
WHERE (MDTB_MNG03 > 0 OR MDTB_RDMNG > 0) 
		AND MDTB_DELNR IS NULL
		AND IFNULL(MDTB_UMDAT, MDTB_DAT00) IS NOT NULL
		AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI');
								  
INSERT INTO fact_excessandshortage_tmp_41(fact_excessandshortageid,
		     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
			  MDTB_OLDSL_upd,
                        MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
                        MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd )
   SELECT max_holder_41.maxid + ROW_NUMBER() OVER(ORDER BY ''),
	  Dim_MRPElementid,
          dim_mrpexceptionID dim_mrpexceptionID1,
             1 dim_mrpexceptionID2,
          Dim_Companyid,
          c.Dim_Currencyid,
          ar.dim_dateid Dim_DateidActionRequired,
          1 Dim_DateidActionClosed,
          dn.dim_dateid Dim_DateidDateNeeded,
             1 Dim_DateidOriginalDock,
          mrpd.Dim_Dateid Dim_DateidMRP,
          Dim_Partid,
             1 Dim_StorageLocationid,
          Dim_Plantid,
          Dim_UnitOfMeasureid,
             1 Dim_PurchaseGroupid,
          Dim_PurchaseOrgid,
          1 Dim_Vendorid,
          2 Dim_ActionStateid,
          1 Dim_ItemCategoryid,
          1 Dim_DocumentTypeid,
          Dim_MRPProcedureid,
             1 Dim_MRPDiscontinuationIndicatorid,
             1 Dim_specialprocurementid,
          1 Dim_FixedVendorid,
          1 Dim_ConsumptionTypeid,
          t.MDTB_MNG01 ct_QtyMRP,
          t.MDTB_MNG03 ct_QtyShortage,
          t.MDTB_RDMNG ct_QtyExcess,
          m.MDKP_BERW2 ct_DaysReceiptCoverage,
          m.MDKP_BERW1 ct_DaysStockCoverage,
          IFNULL(t.mdtb_delnr,'Not Set') dd_DocumentNo,
          t.mdtb_delps dd_DocumentItemNo,
          t.mdtb_delet dd_ScheduleNo,
          t.mdtb_dtnum dd_mrptablenumber,
          t.mdtb_baugr dd_peggedrequirement,
          t.mdtb_sernr dd_bomexplosionno,
	      t.MDTB_OLDSL,
         t.MDTB_DAT01,
         dc.CompanyCode,
         t.MDTB_LGORT,
         pl.plantcode,
         m.MDKP_EKGRP,
         m.mdkp_kzaus,
         t.MDTB_SOBES
     FROM max_holder_41 CROSS JOIN mdtb_pi00 t
          INNER JOIN dim_mrpelement me
             ON     t.MDTB_DELKZ = me.MRPElement
                AND me.RowIsCurrent = 1
          INNER JOIN dim_mrpexception mex
             ON IFNULL(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
                AND mex.RowIsCurrent = 1
          INNER JOIN mdkp m
             ON t.MDTB_DTNUM = m.MDKP_DTNUM
          INNER JOIN dim_part dp
             ON     m.MDKP_PLWRK = dp.Plant
                AND m.MDKP_MATNR = dp.PartNumber
                AND dp.RowIsCurrent = 1
          INNER JOIN dim_plant pl
             ON m.MDKP_PLWRK = pl.PlantCode AND pl.RowIsCurrent = 1
          INNER JOIN dim_unitofmeasure uom
             ON uom.UOM = m.MDKP_MEINS AND uom.RowIsCurrent = 1
          INNER JOIN Dim_PurchaseOrg po
             ON pl.PurchOrg = po.PurchaseOrgCode AND po.RowIsCurrent = 1
          INNER JOIN dim_company dc
             ON pl.CompanyCode = dc.CompanyCode AND dc.RowIsCurrent = 1
          INNER JOIN dim_currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date mrpd
             ON mrpd.DateValue = m.MDKP_dsdat
                AND mrpd.CompanyCode = dc.CompanyCode AND mrpd.plantcode_factory = pl.plantcode
          INNER JOIN dim_date dn
             ON     dn.DateValue = DATEvalue_upd
                AND dn.CompanyCode = dc.CompanyCode AND dn.plantcode_factory = pl.plantcode
          INNER JOIN dim_date ar
             ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode AND ar.plantcode_factory = pl.plantcode
          INNER JOIN dim_mrpprocedure pr
             ON m.MDKP_DISVF = pr.MRPProcedure AND pr.RowIsCurrent = 1;

DROP TABLE IF EXISTS mdtb_pi00;	

UPDATE fact_excessandshortage_tmp_41 sb
SET dim_mrpexceptionID2 = IFNULL(ex.dim_mrpexceptionid,1)
FROM  fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_mrpexception ex
      ON sb.MDTB_OLDSL_upd = ex.exceptionkey AND ex.RowIsCurrent = 1
WHERE dim_mrpexceptionID2 <> IFNULL(ex.dim_mrpexceptionid,1);

MERGE INTO fact_excessandshortage_tmp_41 fact 
USING (SELECT fact_excessandshortageid, 
			  IFNULL(od.dim_dateid,1) Dim_DateidOriginalDock
	   FROM (SELECT t1.fact_excessandshortageid, dc.companycode, p.plantcode, t1.MDTB_DAT01_upd
			 FROM fact_excessandshortage_tmp_41 t1
			 INNER JOIN dim_company dc 
				ON t1.Dim_Companyid = dc.Dim_Companyid
			 INNER JOIN dim_plant p 
			 	ON p.dim_plantid = t1.dim_plantid) f
	   LEFT JOIN  dim_DATE od
			 ON od.DATEValue = f.MDTB_DAT01_upd AND od.CompanyCode = f.CompanyCode
			 AND od.plantcode_factory = f.plantcode
	   )  src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_DateidOriginalDock = src.Dim_DateidOriginalDock;	


UPDATE fact_excessandshortage_tmp_41 sb
SET sb.Dim_StorageLocationid = IFNULL(sl.Dim_StorageLocationid,1)
FROM  fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_storagelocation sl
      ON sl.LocationCode = sb.MDTB_LGORT_upd
                     AND sl.Plant = sb.plantcode_upd
WHERE sb.Dim_StorageLocationid <> IFNULL(sl.Dim_StorageLocationid,1);


UPDATE fact_excessandshortage_tmp_41 sb
SET sb.Dim_PurchaseGroupid =  IFNULL(pg.Dim_PurchaseGroupid,1)
FROM  fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_purchasegroup pg
	  ON pg.PurchaseGroup = sb.MDKP_EKGRP_upd AND pg.RowIsCurrent = 1
WHERE sb.Dim_PurchaseGroupid <>  IFNULL(pg.Dim_PurchaseGroupid,1);

UPDATE fact_excessandshortage_tmp_41 sb
SET sb.Dim_MRPDiscontinuationIndicatorid =  IFNULL(di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_mrpdiscontinuationindicator di
      ON sb.mdkp_kzaus_upd = di."indicator" AND di.RowIsCurrent = 1
WHERE 	 sb.Dim_MRPDiscontinuationIndicatorid <>  IFNULL(di.Dim_MRPDiscontinuationIndicatorid,1) ;
    

UPDATE fact_excessandshortage_tmp_41 sb
SET sb.Dim_specialprocurementid = IFNULL(sp.Dim_specialprocurementid,1)
FROM  fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_specialprocurement sp
      ON sb.MDTB_SOBES_upd = sp.specialprocurement AND sp.RowIsCurrent = 1
WHERE sb.Dim_specialprocurementid <> IFNULL(sp.Dim_specialprocurementid,1);
	   
CREATE TABLE eands_pe01 like fact_excessandshortage_tmp_41  INCLUDING DEFAULTS INCLUDING IDENTITY;

Insert into eands_pe01(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
                          MDTB_OLDSL_upd,
			 MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
                        MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd)
Select ne.fact_excessandshortageid,
                     ne.Dim_MRPElementid,
                     ne.dim_mrpexceptionID1,
                     ne.dim_mrpexceptionID2,
                     ne.Dim_Companyid,
                     ne.Dim_Currencyid,
                     ne.Dim_DateidActionRequired,
                     ne.Dim_DateidActionClosed,
                     ne.Dim_DateidDateNeeded,
                     ne.Dim_DateidOriginalDock,
                     ne.Dim_DateidMRP,
                     ne.Dim_Partid,
                     ne.Dim_StorageLocationid,
                     ne.Dim_Plantid,
                     ne.Dim_UnitOfMeasureid,
                     ne.Dim_PurchaseGroupid,
                     ne.Dim_PurchaseOrgid,
                     ne.Dim_Vendorid,
                     ne.Dim_ActionStateid,
                     ne.Dim_ItemCategoryid,
                     ne.Dim_DocumentTypeid,
                     ne.Dim_MRPProcedureid,
                     ne.Dim_MRPDiscontinuationIndicatorid,
                     ne.Dim_specialprocurementid,
                     ne.Dim_FixedVendorid,
                     ne.Dim_ConsumptionTypeid,
                     ne.ct_QtyMRP,
                     ne.ct_QtyShortage,
                     ne.ct_QtyExcess,
                     ne.ct_DaysReceiptCoverage,
                     ne.ct_DaysStockCoverage,
                     ne.dd_DocumentNo,
                     ne.dd_DocumentItemNo,
                     ne.dd_ScheduleNo,
                     ne.dd_mrptablenumber,
                     ne.dd_peggedrequirement,
                     ne.dd_bomexplosionno,
                          ne.MDTB_OLDSL_upd,
			 ne.MDTB_DAT01_upd,
                        ne.CompanyCode_upd,
                        ne.MDTB_LGORT_upd,
                        ne.plantcode_upd,
                        ne.MDKP_EKGRP_upd,
                        ne.mdkp_kzaus_upd,
                        ne.MDTB_SOBES_upd
FROM fact_excessandshortage_tmp_41 ne INNER JOIN fact_excessandshortage_sub41 mrp
	ON mrp.Dim_Partid = ne.Dim_Partid 
     AND mrp.dim_mrpexceptionID1 = ne.dim_mrpexceptionID1
     AND mrp.Dim_MRPElementid = ne.Dim_MRPElementid
     AND mrp.ct_QtyMRP = ne.ct_QtyMRP
     AND mrp.ct_QtyShortage = ne.ct_QtyShortage
     AND mrp.ct_QtyExcess = ne.ct_QtyExcess
     AND mrp.Dim_DateidDateNeeded = ne.Dim_DateidDateNeeded ;


/*call vectorwise(combine 'fact_excessandshortage_tmp_41 - eands_pe01')*/
MERGE INTO fact_excessandshortage_tmp_41 t1
USING (select distinct fact_excessandshortageid from eands_pe01) t2
ON t1.fact_excessandshortageid = t2.fact_excessandshortageid	
WHEN MATCHED THEN DELETE; 
				


INSERT INTO fact_excessandshortage(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
)
Select max_holder_41.maxid + row_number() over(order by ''),
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
From fact_excessandshortage_tmp_41,max_holder_41;

Drop table if exists eands_pe01;
Drop table if exists fact_excessandshortage_tmp_41;
Drop table if exists fact_excessandshortage_sub41;
Drop table if exists max_holder_41;

Create table max_holder_41
as
select 	ifnull(max(f.fact_excessandshortageid), 
               ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_excessandshortage f;

Create table fact_excessandshortage_sub41 as
Select  mrp.Dim_Partid ,
	     mrp.dim_mrpexceptionID1,
	     mrp.Dim_MRPElementid,
	     mrp.ct_QtyMRP,
	     mrp.ct_QtyShortage ,
	     mrp.ct_QtyExcess,
	     mrp.dd_documentno, 
	     mrp.dd_documentitemno, 
	     mrp.dd_scheduleno
From fact_excessandshortage mrp
Where  mrp.dd_documentno <> 'Not Set'
AND  mrp.Dim_DateidDateNeeded = 1
AND  mrp.Dim_ActionStateid = 2;

CREATE TABLE fact_excessandshortage_tmp_41 like  fact_excessandshortage INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE fact_excessandshortage_tmp_41 ADD PRIMARY KEY (fact_excessandshortageid);

ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_OLDSL_upd VARCHAR(5);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_DAT01_upd DATE;
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN CompanyCode_upd VARCHAR(20);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_LGORT_upd VARCHAR(5);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN plantcode_upd VARCHAR(20);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_EKGRP_upd VARCHAR(3);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN mdkp_kzaus_upd VARCHAR(1);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_SOBES_upd VARCHAR(1);

DROP TABLE IF EXISTS mdtb_pi00;
CREATE TABLE mdtb_pi00 AS 
SELECT * FROM mdtb   
WHERE MDTB_UMDAT IS NULL 
AND MDTB_DAT00 IS NULL 
AND (MDTB_MNG03 > 0 OR MDTB_RDMNG > 0) 
AND MDTB_DELNR is not null AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI');

INSERT INTO fact_excessandshortage_tmp_41(fact_excessandshortageid,
		     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
			MDTB_OLDSL_upd,
			MDTB_DAT01_upd,
			CompanyCode_upd,
			MDTB_LGORT_upd,
			plantcode_upd,
			MDKP_EKGRP_upd,
			mdkp_kzaus_upd,
			MDTB_SOBES_upd )
   SELECT  max_holder_41.maxid + ROW_NUMBER() over(order by ''),
	       Dim_MRPElementid,
           dim_mrpexceptionID dim_mrpexceptionID1,
           1 dim_mrpexceptionID2,
           Dim_Companyid,
           c.Dim_Currencyid,
           ar.dim_dateid Dim_DateidActionRequired,
           1 Dim_DateidActionClosed,
           1 Dim_DateidDateNeeded,
           1 Dim_DateidOriginalDock,
           mrpd.Dim_Dateid Dim_DateidMRP,
           Dim_Partid,
           1 Dim_StorageLocationid,
           Dim_Plantid,
           Dim_UnitOfMeasureid,
           1 Dim_PurchaseGroupid,
           Dim_PurchaseOrgid,
           1 Dim_Vendorid,
           2 Dim_ActionStateid,
           1 Dim_ItemCategoryid,
           1 Dim_DocumentTypeid,
           Dim_MRPProcedureid,
           1 Dim_MRPDiscontinuationIndicatorid,
           1 Dim_specialprocurementid,
           1 Dim_FixedVendorid,
           1 Dim_ConsumptionTypeid,
           t.MDTB_MNG01 ct_QtyMRP,
           t.MDTB_MNG03 ct_QtyShortage,
           t.MDTB_RDMNG ct_QtyExcess,
           m.MDKP_BERW2 ct_DaysReceiptCoverage,
           m.MDKP_BERW1 ct_DaysStockCoverage,
           IFNULL(t.mdtb_delnr,'Not Set') dd_DocumentNo,
           t.mdtb_delps dd_DocumentItemNo,
           t.mdtb_delet dd_ScheduleNo,
           t.mdtb_dtnum dd_mrptablenumber,
           t.mdtb_baugr dd_peggedrequirement,
           t.mdtb_sernr dd_bomexplosionno,
	       t.MDTB_OLDSL,
           t.MDTB_DAT01,
           dc.CompanyCode,
           t.MDTB_LGORT,
           pl.plantcode,
           m.MDKP_EKGRP,
           m.mdkp_kzaus,
           t.MDTB_SOBES
     FROM max_holder_41 CROSS JOIN mdtb_pi00 t
          INNER JOIN dim_mrpelement me
             ON     t.MDTB_DELKZ = me.MRPElement
                AND me.RowIsCurrent = 1
          INNER JOIN dim_mrpexception mex
             ON IFNULL(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
                AND mex.RowIsCurrent = 1
          INNER JOIN mdkp m
             ON t.MDTB_DTNUM = m.MDKP_DTNUM
          INNER JOIN dim_part dp
             ON     m.MDKP_PLWRK = dp.Plant
                AND m.MDKP_MATNR = dp.PartNumber
                AND dp.RowIsCurrent = 1
          INNER JOIN dim_unitofmeasure uom
             ON uom.UOM = m.MDKP_MEINS AND uom.RowIsCurrent = 1
          INNER JOIN dim_plant pl
             ON m.MDKP_PLWRK = pl.PlantCode AND pl.RowIsCurrent = 1
          INNER JOIN Dim_PurchaseOrg po
             ON pl.PurchOrg = po.PurchaseOrgCode AND po.RowIsCurrent = 1
          INNER JOIN dim_company dc
             ON pl.CompanyCode = dc.CompanyCode AND dc.RowIsCurrent = 1
          INNER JOIN Dim_Currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date mrpd
             ON mrpd.DateValue = m.MDKP_dsdat
                AND mrpd.CompanyCode = dc.CompanyCode AND pl.plantcode = mrpd.plantcode_factory
          INNER JOIN dim_date ar
             ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode AND pl.plantcode = ar.plantcode_factory
          INNER JOIN dim_mrpprocedure pr
             ON m.MDKP_DISVF = pr.MRPProcedure AND pr.RowIsCurrent = 1;

DROP TABLE IF EXISTS mdtb_pi00;						  
						  
UPDATE fact_excessandshortage_tmp_41 sb
SET dim_mrpexceptionID2 =  IFNULL(ex.dim_mrpexceptionid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_mrpexception ex
      ON IFNULL(sb.MDTB_OLDSL_upd, 'Not Set') = ex.exceptionkey AND ex.RowIsCurrent = 1;

MERGE INTO fact_excessandshortage_tmp_41 fact 
USING (SELECT fact_excessandshortageid, 
			  IFNULL(od.dim_dateid,1) Dim_DateidOriginalDock
	   FROM (SELECT t1.fact_excessandshortageid, dc.companycode, pl.plantcode, t1.MDTB_DAT01_upd
			 FROM fact_excessandshortage_tmp_41 t1
			 INNER JOIN dim_company dc 
				ON t1.Dim_Companyid = dc.Dim_Companyid
			 INNER JOIN dim_plant pl ON pl.dim_plantid = t1.dim_plantid) f
	   LEFT JOIN  dim_DATE od
			 ON od.DATEValue = f.MDTB_DAT01_upd AND od.CompanyCode = f.CompanyCode
			 AND od.plantcode_factory = f.plantcode
	   )  src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_DateidOriginalDock = src.Dim_DateidOriginalDock;	

UPDATE fact_excessandshortage_tmp_41 sb
SET Dim_StorageLocationid = IFNULL(sl.Dim_StorageLocationid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_storagelocation sl
      ON sl.LocationCode = sb.MDTB_LGORT_upd AND sl.Plant = sb.plantcode_upd;

UPDATE fact_excessandshortage_tmp_41 sb
SET Dim_PurchaseGroupid =  IFNULL(pg.Dim_PurchaseGroupid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_purchasegroup pg
      ON pg.PurchaseGroup = sb.MDKP_EKGRP_upd AND pg.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 sb
SET Dim_MRPDiscontinuationIndicatorid =  IFNULL(di.Dim_MRPDiscontinuationIndicatorid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_mrpdiscontinuationindicator di
      ON sb.mdkp_kzaus_upd = di."indicator" AND di.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 sb
SET Dim_specialprocurementid = IFNULL(sp.Dim_specialprocurementid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_specialprocurement sp
      ON sb.MDTB_SOBES_upd = sp.specialprocurement AND sp.RowIsCurrent = 1;						  
						  
CREATE TABLE eands_pe01 like fact_excessandshortage_tmp_41  INCLUDING DEFAULTS INCLUDING IDENTITY;

Insert into eands_pe01(fact_excessandshortageid,
			Dim_MRPElementid,
			dim_mrpexceptionID1,
			dim_mrpexceptionID2,
			Dim_Companyid,
			Dim_Currencyid,
			Dim_DateidActionRequired,
			Dim_DateidActionClosed,
			Dim_DateidDateNeeded,
			Dim_DateidOriginalDock,
			Dim_DateidMRP,
			Dim_Partid,
			Dim_StorageLocationid,
			Dim_Plantid,
			Dim_UnitOfMeasureid,
			Dim_PurchaseGroupid,
			Dim_PurchaseOrgid,
			Dim_Vendorid,
			Dim_ActionStateid,
			Dim_ItemCategoryid,
			Dim_DocumentTypeid,
			Dim_MRPProcedureid,
			Dim_MRPDiscontinuationIndicatorid,
			Dim_specialprocurementid,
			Dim_FixedVendorid,
			Dim_ConsumptionTypeid,
			ct_QtyMRP,
			ct_QtyShortage,
			ct_QtyExcess,
			ct_DaysReceiptCoverage,
			ct_DaysStockCoverage,
			dd_DocumentNo,
			dd_DocumentItemNo,
			dd_ScheduleNo,
			dd_mrptablenumber,
			dd_peggedrequirement,
			dd_bomexplosionno,
			MDTB_OLDSL_upd,
			MDTB_DAT01_upd,
			CompanyCode_upd,
			MDTB_LGORT_upd,
			plantcode_upd,
			MDKP_EKGRP_upd,
			mdkp_kzaus_upd,
			MDTB_SOBES_upd )
SELECT  ne.fact_excessandshortageid,
		ne.Dim_MRPElementid,
		ne.dim_mrpexceptionID1,
		ne.dim_mrpexceptionID2,
		ne.Dim_Companyid,
		ne.Dim_Currencyid,
		ne.Dim_DateidActionRequired,
		ne.Dim_DateidActionClosed,
		ne.Dim_DateidDateNeeded,
		ne.Dim_DateidOriginalDock,
		ne.Dim_DateidMRP,
		ne.Dim_Partid,
		ne.Dim_StorageLocationid,
		ne.Dim_Plantid,
		ne.Dim_UnitOfMeasureid,
		ne.Dim_PurchaseGroupid,
		ne.Dim_PurchaseOrgid,
		ne.Dim_Vendorid,
		ne.Dim_ActionStateid,
		ne.Dim_ItemCategoryid,
		ne.Dim_DocumentTypeid,
		ne.Dim_MRPProcedureid,
		ne.Dim_MRPDiscontinuationIndicatorid,
		ne.Dim_specialprocurementid,
		ne.Dim_FixedVendorid,
		ne.Dim_ConsumptionTypeid,
		ne.ct_QtyMRP,
		ne.ct_QtyShortage,
		ne.ct_QtyExcess,
		ne.ct_DaysReceiptCoverage,
		ne.ct_DaysStockCoverage,
		ne.dd_DocumentNo,
		ne.dd_DocumentItemNo,
		ne.dd_ScheduleNo,
		ne.dd_mrptablenumber,
		ne.dd_peggedrequirement,
		ne.dd_bomexplosionno,
		ne.MDTB_OLDSL_upd,
		ne.MDTB_DAT01_upd,
		ne.CompanyCode_upd,
		ne.MDTB_LGORT_upd,
		ne.plantcode_upd,
		ne.MDKP_EKGRP_upd,
		ne.mdkp_kzaus_upd,
		ne.MDTB_SOBES_upd 
FROM fact_excessandshortage_tmp_41 ne INNER JOIN fact_excessandshortage_sub41 mrp
	ON mrp.Dim_Partid = ne.Dim_Partid 
     AND mrp.dim_mrpexceptionID1 = ne.dim_mrpexceptionID1
     AND mrp.Dim_MRPElementid = ne.Dim_MRPElementid
     AND mrp.ct_QtyMRP = ne.ct_QtyMRP
     AND mrp.ct_QtyShortage = ne.ct_QtyShortage 
     AND mrp.ct_QtyExcess = ne.ct_QtyExcess
     AND mrp.dd_documentno = ne.dd_documentno
     AND mrp.dd_documentitemno = ne.dd_documentitemno 
     AND mrp.dd_scheduleno = ne.dd_scheduleno;						  
						  
DELETE *
FROM fact_excessandshortage_tmp_41 
WHERE fact_excessandshortageid IN  
     (SELECT a.fact_excessandshortageid 
      FROM fact_excessandshortage_tmp_41 a INNER JOIN eands_pe01 b 
      ON a.fact_excessandshortageid = b.fact_excessandshortageid);							  
						  
INSERT INTO fact_excessandshortage(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno)
SELECT max_holder_41.maxid + ROW_NUMBER() over(order by ''),
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
FROM fact_excessandshortage_tmp_41 CROSS JOIN max_holder_41; 


DROP TABLE IF EXISTS eands_pe01;
DROP TABLE IF EXISTS max_holder_41;
DROP TABLE IF EXISTS fact_excessandshortage_sub41;
DROP TABLE IF EXISTS fact_excessandshortage_tmp_41;						  

CREATE TABLE max_holder_41
AS
SELECT IFNULL(max(fact_excessandshortageid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) AS maxid
FROM fact_excessandshortage;	

CREATE TABLE fact_excessandshortage_sub41 AS
SELECT   mrp.Dim_Partid,
	     mrp.dim_mrpexceptionID1,
	     mrp.Dim_MRPElementid,
	     mrp.ct_QtyMRP ,
	     mrp.ct_QtyShortage, 
	     mrp.ct_QtyExcess
FROM fact_excessandshortage mrp
WHERE mrp.dd_documentno = 'Not Set'
AND mrp.Dim_DateidDateNeeded = 1
AND mrp.Dim_ActionStateid = 2;

CREATE TABLE fact_excessandshortage_tmp_41 like fact_excessandshortage INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE fact_excessandshortage_tmp_41 ADD PRIMARY KEY (fact_excessandshortageid);

ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_OLDSL_upd VARCHAR(5);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_DAT01_upd DATE;
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN CompanyCode_upd VARCHAR(20);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_LGORT_upd VARCHAR(5);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN plantcode_upd VARCHAR(20);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDKP_EKGRP_upd VARCHAR(3);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN mdkp_kzaus_upd VARCHAR(1);
ALTER TABLE fact_excessandshortage_tmp_41 ADD COLUMN MDTB_SOBES_upd VARCHAR(1);

DROP TABLE IF EXISTS mdtb_pi00;
CREATE TABLE mdtb_pi00 AS 
SELECT * FROM mdtb 
WHERE     MDTB_UMDAT IS NULL
AND MDTB_DAT00 IS NULL
AND (MDTB_MNG03 > 0 OR MDTB_RDMNG > 0)
AND MDTB_DELNR IS NULL AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI');						  
						  
INSERT INTO fact_excessandshortage_tmp_41(
		fact_excessandshortageid,
		Dim_MRPElementid,
		dim_mrpexceptionID1,
		dim_mrpexceptionID2,
		Dim_Companyid,
		Dim_Currencyid,
		Dim_DateidActionRequired,
		Dim_DateidActionClosed,
		Dim_DateidDateNeeded,
		Dim_DateidOriginalDock,
		Dim_DateidMRP,
		Dim_Partid,
		Dim_StorageLocationid,
		Dim_Plantid,
		Dim_UnitOfMeasureid,
		Dim_PurchaseGroupid,
		Dim_PurchaseOrgid,
		Dim_Vendorid,
		Dim_ActionStateid,
		Dim_ItemCategoryid,
		Dim_DocumentTypeid,
		Dim_MRPProcedureid,
		Dim_MRPDiscontinuationIndicatorid,
		Dim_specialprocurementid,
		Dim_FixedVendorid,
		Dim_ConsumptionTypeid,
		ct_QtyMRP,
		ct_QtyShortage,
		ct_QtyExcess,
		ct_DaysReceiptCoverage,
		ct_DaysStockCoverage,
		dd_DocumentNo,
		dd_DocumentItemNo,
		dd_ScheduleNo,
		dd_mrptablenumber,
		dd_peggedrequirement,
		dd_bomexplosionno,
		MDTB_OLDSL_upd,
		MDTB_DAT01_upd,
		CompanyCode_upd,
		MDTB_LGORT_upd,
		plantcode_upd,
		MDKP_EKGRP_upd,
		mdkp_kzaus_upd,
		MDTB_SOBES_upd)
SELECT   max_holder_41.maxid + ROW_NUMBER() over(order by ''),
		Dim_MRPElementid,
		dim_mrpexceptionID dim_mrpexceptionID1,
		1 dim_mrpexceptionID2,
		Dim_Companyid,
		c.Dim_Currencyid,
		ar.dim_dateid Dim_DateidActionRequired,
		1 Dim_DateidActionClosed,
		1 Dim_DateidDateNeeded,
		1 Dim_DateidOriginalDock,
		mrpd.Dim_Dateid Dim_DateidMRP,
		Dim_Partid,
		1 Dim_StorageLocationid,
		Dim_Plantid,
		Dim_UnitOfMeasureid,
		1 Dim_PurchaseGroupid,
		Dim_PurchaseOrgid,
		1 Dim_Vendorid,
		2 Dim_ActionStateid,
		1 Dim_ItemCategoryid,
		1 Dim_DocumentTypeid,
		Dim_MRPProcedureid,
		1 Dim_MRPDiscontinuationIndicatorid,
		1 Dim_specialprocurementid,
		1 Dim_FixedVendorid,
		1 Dim_ConsumptionTypeid,
		t.MDTB_MNG01 ct_QtyMRP,
		t.MDTB_MNG03 ct_QtyShortage,
		t.MDTB_RDMNG ct_QtyExcess,
		m.MDKP_BERW2 ct_DaysReceiptCoverage,
		m.MDKP_BERW1 ct_DaysStockCoverage,
		IFNULL(t.mdtb_delnr,'Not Set') dd_DocumentNo,
		t.mdtb_delps dd_DocumentItemNo,
		t.mdtb_delet dd_ScheduleNo,
		t.mdtb_dtnum dd_mrptablenumber,
		t.mdtb_baugr dd_peggedrequirement,
		t.mdtb_sernr dd_bomexplosionno,
		t.MDTB_OLDSL,
		t.MDTB_DAT01,
		dc.CompanyCode,
		t.MDTB_LGORT,
		pl.plantcode,
		m.MDKP_EKGRP,
		m.mdkp_kzaus,
		t.MDTB_SOBES
FROM max_holder_41 CROSS JOIN mdtb_pi00 t
	  INNER JOIN dim_mrpelement me
		 ON     t.MDTB_DELKZ = me.MRPElement
			AND me.RowIsCurrent = 1
	  INNER JOIN dim_mrpexception mex
		 ON IFNULL(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
			AND mex.RowIsCurrent = 1
	  INNER JOIN mdkp m
		 ON t.MDTB_DTNUM = m.MDKP_DTNUM
	  INNER JOIN dim_part dp
		 ON     m.MDKP_PLWRK = dp.Plant
			AND m.MDKP_MATNR = dp.PartNumber
			AND dp.RowIsCurrent = 1
	  INNER JOIN dim_unitofmeasure uom
		 ON uom.UOM = m.MDKP_MEINS AND uom.RowIsCurrent = 1
	  INNER JOIN dim_plant pl
		 ON m.MDKP_PLWRK = pl.PlantCode AND pl.RowIsCurrent = 1
	  INNER JOIN Dim_PurchaseOrg po
		 ON pl.PurchOrg = po.PurchaseOrgCode AND po.RowIsCurrent = 1
	  INNER JOIN dim_company dc
		 ON pl.CompanyCode = dc.CompanyCode AND dc.RowIsCurrent = 1
	  INNER JOIN Dim_Currency c
		 ON c.currencycode = dc.currency
	  INNER JOIN dim_date mrpd
		 ON mrpd.DateValue = m.MDKP_dsdat
			AND mrpd.CompanyCode = dc.CompanyCode
			AND pl.plantcode = mrpd.plantcode_factory
	  INNER JOIN dim_date ar
		 ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode
		 AND pl.plantcode = ar.plantcode_factory
	  INNER JOIN dim_mrpprocedure pr
		 ON m.MDKP_DISVF = pr.MRPProcedure AND pr.RowIsCurrent = 1;						  
						  
DROP TABLE IF EXISTS mdtb_pi00;						  
						  
UPDATE fact_excessandshortage_tmp_41 sb
SET dim_mrpexceptionID2 = IFNULL(ex.dim_mrpexceptionid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_mrpexception ex
      ON IFNULL(sb.MDTB_OLDSL_upd, 'Not Set') = ex.exceptionkey AND ex.RowIsCurrent = 1;

MERGE INTO fact_excessandshortage_tmp_41 fact 
USING (SELECT fact_excessandshortageid, 
			  IFNULL(od.dim_dateid,1) Dim_DateidOriginalDock
	   FROM (SELECT t1.fact_excessandshortageid, dc.companycode, pl.plantcode, t1.MDTB_DAT01_upd
			 FROM fact_excessandshortage_tmp_41 t1
			 INNER JOIN dim_company dc 
				ON t1.Dim_Companyid = dc.Dim_Companyid
			 INNER JOIN dim_plant pl
			 	ON pl.dim_plantid = t1.dim_plantid) f
	   LEFT JOIN  dim_DATE od
			 ON od.DATEValue = f.MDTB_DAT01_upd AND od.CompanyCode = f.CompanyCode
			 AND od.plantcode_factory = f.plantcode
	   )  src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_DateidOriginalDock = src.Dim_DateidOriginalDock;		

UPDATE fact_excessandshortage_tmp_41 sb
SET Dim_StorageLocationid = IFNULL(sl.Dim_StorageLocationid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_storagelocation sl
      ON sl.LocationCode = sb.MDTB_LGORT_upd AND sl.Plant = sb.plantcode_upd;

UPDATE fact_excessandshortage_tmp_41 sb
SET Dim_PurchaseGroupid =  IFNULL(pg.Dim_PurchaseGroupid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_purchasegroup pg
      ON pg.PurchaseGroup = sb.MDKP_EKGRP_upd AND pg.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 sb
SET Dim_MRPDiscontinuationIndicatorid =  IFNULL(di.Dim_MRPDiscontinuationIndicatorid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_mrpdiscontinuationindicator di
      ON sb.mdkp_kzaus_upd = di."indicator" AND di.RowIsCurrent = 1;

UPDATE fact_excessandshortage_tmp_41 sb
SET Dim_specialprocurementid = IFNULL(sp.Dim_specialprocurementid,1)
FROM fact_excessandshortage_tmp_41 sb
LEFT JOIN dim_specialprocurement sp
      ON sb.MDTB_SOBES_upd = sp.specialprocurement AND sp.RowIsCurrent = 1;						  
						  
CREATE TABLE eands_pe01 like  fact_excessandshortage_tmp_41 INCLUDING DEFAULTS INCLUDING IDENTITY;
						  
Insert into eands_pe01( fact_excessandshortageid,
						Dim_MRPElementid,
						dim_mrpexceptionID1,
						dim_mrpexceptionID2,
						Dim_Companyid,
						Dim_Currencyid,
						Dim_DateidActionRequired,
						Dim_DateidActionClosed,
						Dim_DateidDateNeeded,
						Dim_DateidOriginalDock,
						Dim_DateidMRP,
						Dim_Partid,
						Dim_StorageLocationid,
						Dim_Plantid,
						Dim_UnitOfMeasureid,
						Dim_PurchaseGroupid,
						Dim_PurchaseOrgid,
						Dim_Vendorid,
						Dim_ActionStateid,
						Dim_ItemCategoryid,
						Dim_DocumentTypeid,
						Dim_MRPProcedureid,
						Dim_MRPDiscontinuationIndicatorid,
						Dim_specialprocurementid,
						Dim_FixedVendorid,
						Dim_ConsumptionTypeid,
						ct_QtyMRP,
						ct_QtyShortage,
						ct_QtyExcess,
						ct_DaysReceiptCoverage,
						ct_DaysStockCoverage,
						dd_DocumentNo,
						dd_DocumentItemNo,
						dd_ScheduleNo,
						dd_mrptablenumber,
						dd_peggedrequirement,
						dd_bomexplosionno,
						MDTB_OLDSL_upd,
						MDTB_DAT01_upd,
						CompanyCode_upd,
						MDTB_LGORT_upd,
						plantcode_upd,
						MDKP_EKGRP_upd,
						mdkp_kzaus_upd,
						MDTB_SOBES_upd)
				SELECT  ne.fact_excessandshortageid,
						ne.Dim_MRPElementid,
						ne.dim_mrpexceptionID1,
						ne.dim_mrpexceptionID2,
						ne.Dim_Companyid,
						ne.Dim_Currencyid,
						ne.Dim_DateidActionRequired,
						ne.Dim_DateidActionClosed,
						ne.Dim_DateidDateNeeded,
						ne.Dim_DateidOriginalDock,
						ne.Dim_DateidMRP,
						ne.Dim_Partid,
						ne.Dim_StorageLocationid,
						ne.Dim_Plantid,
						ne.Dim_UnitOfMeasureid,
						ne.Dim_PurchaseGroupid,
						ne.Dim_PurchaseOrgid,
						ne.Dim_Vendorid,
						ne.Dim_ActionStateid,
						ne.Dim_ItemCategoryid,
						ne.Dim_DocumentTypeid,
						ne.Dim_MRPProcedureid,
						ne.Dim_MRPDiscontinuationIndicatorid,
						ne.Dim_specialprocurementid,
						ne.Dim_FixedVendorid,
						ne.Dim_ConsumptionTypeid,
						ne.ct_QtyMRP,
						ne.ct_QtyShortage,
						ne.ct_QtyExcess,
						ne.ct_DaysReceiptCoverage,
						ne.ct_DaysStockCoverage,
						ne.dd_DocumentNo,
						ne.dd_DocumentItemNo,
						ne.dd_ScheduleNo,
						ne.dd_mrptablenumber,
						ne.dd_peggedrequirement,
						ne.dd_bomexplosionno,
						ne.MDTB_OLDSL_upd,
						ne.MDTB_DAT01_upd,
						ne.CompanyCode_upd,
						ne.MDTB_LGORT_upd,
						ne.plantcode_upd,
						ne.MDKP_EKGRP_upd,
						ne.mdkp_kzaus_upd,
						ne.MDTB_SOBES_upd 
				FROM fact_excessandshortage_tmp_41 ne INNER JOIN 
					 fact_excessandshortage_sub41 mrp
				ON mrp.Dim_Partid = ne.Dim_Partid
				AND mrp.dim_mrpexceptionID1 =  ne.dim_mrpexceptionID1 
				AND mrp.Dim_MRPElementid = ne.Dim_MRPElementid
				AND mrp.ct_QtyMRP = ne.ct_QtyMRP
				AND mrp.ct_QtyShortage = ne.ct_QtyShortage 
				AND mrp.ct_QtyExcess = ne.ct_QtyExcess;						  
						  
DELETE *
FROM fact_excessandshortage_tmp_41 
WHERE fact_excessandshortageid IN  
     (SELECT a.fact_excessandshortageid 
      FROM fact_excessandshortage_tmp_41 a INNER JOIN eands_pe01 b 
      ON a.fact_excessandshortageid = b.fact_excessandshortageid);							  
						  
INSERT INTO fact_excessandshortage(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno)
SELECT max_holder_41.maxid + ROW_NUMBER() OVER(ORDER BY ''),
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
FROM fact_excessandshortage_tmp_41 CROSS JOIN max_holder_41;

DROP TABLE IF EXISTS eands_pe01;
DROP TABLE IF EXISTS fact_excessandshortage_tmp_41;
DROP TABLE IF EXISTS fact_excessandshortage_sub41;
DROP TABLE IF EXISTS max_holder_41;						  
						  
UPDATE fact_excessandshortage m
   SET m.Dim_Vendorid = v.Dim_Vendorid
FROM  fact_excessandshortage m
INNER JOIN plaf po ON m.dd_DocumentNo = po.PLAF_PLNUM 
INNER JOIN mdtb t ON m.dd_DocumentNo = t.MDTB_DELNR
INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
INNER JOIN dim_vendor v ON v.VendorNumber = IFNULL(po.PLAF_EMLIF, 'Not Set') AND v.RowIsCurrent = 1
INNER JOIN dim_vendor fv ON fv.VendorNumber = IFNULL(po.PLAF_FLIEF, 'Not Set') AND fv.RowIsCurrent = 1
INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = IFNULL(po.PLAF_KZVBR, 'Not Set') AND ct.RowIsCurrent = 1
INNER JOIN dim_part p ON p.Dim_Partid = m.Dim_Partid
INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant
INNER JOIN Dim_Date dd ON dd.DateValue = IFNULL(t.MDTB_UMDAT, t.MDTB_DAT01) 
					AND dd.CompanyCode = pl.CompanyCode
					AND pl.plantcode = dd.plantcode_factory
WHERE m.Dim_ActionStateid = 2
   AND dd.Dim_Dateid <> 1
   AND IFNULL(t.MDTB_UMDAT, t.MDTB_DAT01) IS NOT NULL
   AND t.MDTB_DAT02 IS NOT NULL
   AND IFNULL(po.PLAF_FLIEF, po.PLAF_EMLIF) IS NOT NULL
   AND  m.Dim_Vendorid <> v.Dim_Vendorid;			

UPDATE fact_excessandshortage m
SET m.Dim_FixedVendorid = fv.Dim_Vendorid
FROM fact_excessandshortage m 
   INNER JOIN plaf po ON m.dd_DocumentNo = po.PLAF_PLNUM
   INNER JOIN mdtb t ON m.dd_DocumentNo = t.MDTB_DELNR
   INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
   INNER JOIN dim_vendor v ON v.VendorNumber = IFNULL(po.PLAF_EMLIF, 'Not Set')
		 AND v.RowIsCurrent = 1
   INNER JOIN dim_vendor fv ON fv.VendorNumber = IFNULL(po.PLAF_FLIEF, 'Not Set')
		 AND fv.RowIsCurrent = 1
   INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = IFNULL(po.PLAF_KZVBR, 'Not Set')
		 AND ct.RowIsCurrent = 1
   INNER JOIN dim_part p ON p.Dim_Partid = m.Dim_Partid
   INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant
   INNER JOIN Dim_Date dd ON dd.DateValue = IFNULL(t.MDTB_UMDAT, t.MDTB_DAT01)
		 AND dd.CompanyCode = pl.CompanyCode 
		 AND pl.plantcode = dd.plantcode_factory	
WHERE   m.Dim_ActionStateid = 2
		AND dd.Dim_Dateid <> 1
		AND IFNULL(t.MDTB_UMDAT, t.MDTB_DAT01) IS NOT NULL
		AND t.MDTB_DAT02 IS NOT NULL
		AND IFNULL(po.PLAF_FLIEF, po.PLAF_EMLIF) IS NOT NULL
		AND m.Dim_FixedVendorid <> fv.Dim_Vendorid;

UPDATE fact_excessandshortage m
SET m.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
FROM fact_excessandshortage m 
   INNER JOIN plaf po ON m.dd_DocumentNo = po.PLAF_PLNUM
   INNER JOIN mdtb t ON m.dd_DocumentNo = t.MDTB_DELNR
   INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
   INNER JOIN dim_vendor v ON v.VendorNumber = IFNULL(po.PLAF_EMLIF, 'Not Set')
		 AND v.RowIsCurrent = 1
   INNER JOIN dim_vendor fv ON fv.VendorNumber = IFNULL(po.PLAF_FLIEF, 'Not Set')
		 AND fv.RowIsCurrent = 1
   INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = IFNULL(po.PLAF_KZVBR, 'Not Set')
		 AND ct.RowIsCurrent = 1
   INNER JOIN dim_part p ON p.Dim_Partid = m.Dim_Partid
   INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant
   INNER JOIN Dim_Date dd ON dd.DateValue = IFNULL(t.MDTB_UMDAT, t.MDTB_DAT01)
		 AND dd.CompanyCode = pl.CompanyCode 
		 AND pl.plantcode = dd.plantcode_factory	
WHERE   m.Dim_ActionStateid = 2
		AND dd.Dim_Dateid <> 1
		AND IFNULL(t.MDTB_UMDAT, t.MDTB_DAT01) IS NOT NULL
		AND t.MDTB_DAT02 IS NOT NULL
		AND IFNULL(po.PLAF_FLIEF, po.PLAF_EMLIF) IS NOT NULL
		AND m.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;