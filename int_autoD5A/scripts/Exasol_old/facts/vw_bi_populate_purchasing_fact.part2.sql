
/**************************************************************************************************************/
/*   Script         : vw_bi_populate_purchasing_fact.part2.sql   */
/*   Author         : Lokesh */
/*   Created On     : 20 Jul 2013 */
/*   Description    : Part 2 of migrated Stored Proc bi_populate_purchasing_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   29 Aug 2014      Lokesh    1.28    Add dd_intraplantflag_merck */
/*   08 Aug 2014      Lokesh    1.27    4 Additional columns updated for iOTIF */
/*   14 Jun 2014      Lokesh    1.26        Added LIFR % calculation */
/*   14 Apr 2014      Issam   1.25        Fixed Dim_PurchaseMisc INSERT                       */
/*   01 Apr 2014      Lokesh    1.22            Fixed substring length/position for CDPOS_TABKEY */
/*   01 Feb 2014      Mohamed   1.25        Added UnlimitedOverDelivery                       */
/*   12 Sep 2013      Lokesh  1.12        Currency changes Tran/Local/GBL                 */
/*   29 Jul 2013      Lokesh    1.3               Integer Divide by Zero errs on aei fixed              */
/*   24 Jul 2013      Lokesh    1.2               Merge Mircea's changes for QtyConversion_EqualTo,QtyConversion_Denom  */
/*               PriceConversion_EqualTo and PriceConversion_Denom      */
/*   24 Jul 2013      Lokesh    1.1               Merge Shanthi's changes for ct_qtyreduced                       */
/*   20 Jul 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/


/* Start of Part 2 of vw_bi_populate_purchasing_fact. This needs to be run after the stdprice scripts, which in turn should be run after part 1 above */
/* Reason for running stdprice in the middle - ct_ExchangeRate which is generated in part1 and needed by the function call */


/* Update 3 */
   /* Update amt_StdUnitPrice and Dim_DateidCosting */

/* 3a   amt_StdUnitPrice */

/* Update amt_StdUnitPrice using getStdPrice */
  DROP TABLE IF EXISTS tmp_distinct_upd_tmp_getStdPrice;
  CREATE TABLE tmp_distinct_upd_tmp_getStdPrice
  AS
  SELECT DISTINCT z.pCompanyCode,z.pPlant,z.pMaterialNo,z.pFiYear,z.pPeriod,z.vUMREZ,z.vUMREN,z.PONumber,z.pUnitPrice,z.StandardPrice
  FROM tmp_getStdPrice z
  WHERE z.fact_script_name = 'bi_populate_purchasing_fact';

  UPDATE fact_purchase fp
  SET fp.amt_StdUnitPrice = (z.StandardPrice / fp.amt_ExchangeRate)     /* LK 14 Sep: As its in local curr, convert it to tra curr. Note that amt_ExchangeRate is same as ct_ExchangeRate   */
  from ekko_ekpo_eket ds, fact_purchase fp,
     dim_date dt,
     tmp_distinct_upd_tmp_getStdPrice z
  WHERE     fp.dd_DocumentNo     = ds.EKPO_EBELN
      AND fp.dd_DocumentItemNo = ds.EKPO_EBELP
      AND fp.dd_ScheduleNo     = ds.EKET_ETENR
      AND dt.DateValue = (select min(x.EKET_BEDAT)
                from ekko_ekpo_eket x
                where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
      AND dt.CompanyCode      = ds.EKPO_BUKRS
    AND dt.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
      AND fp.amt_StdUnitPrice = 0
      AND z.pCompanyCode = ds.EKPO_BUKRS
      AND z.pPlant       = ds.EKPO_WERKS
      AND z.pMaterialNo  = ds.EKPO_MATNR
      AND z.pFiYear      = dt.FinancialYear
      AND z.pPeriod      = dt.FinancialMonthNumber
      /*AND z.fact_script_name = 'bi_populate_purchasing_fact'*/
      AND z.vUMREZ       = ds.EKPO_UMREZ
      AND z.vUMREN       = ds.EKPO_UMREN
      AND z.PONumber     = fp.dd_DocumentNo
      AND z.pUnitPrice - ifnull(( (ds.EKPO_NETPR / (CASE WHEN ifnull(ds.EKPO_PEINH, 0) = 0 THEN 1 ELSE ds.EKPO_PEINH END))
                   * fp.ct_ExchangeRate
                   * (CONVERT(DECIMAL(18,4),ds.EKPO_BPUMZ)/CASE WHEN ifnull(ds.EKPO_BPUMN,0) = 0 THEN 1 ELSE ds.EKPO_BPUMN END)),0) = 0
      AND z.StandardPrice IS NOT NULL;

/* Override amt_StdUnitPrice where condition matches case */
  UPDATE fact_purchase fp
  SET fp.amt_StdUnitPrice = (CASE WHEN ds.EKKO_LOEKZ = 'L' THEN 0
                  WHEN ds.EKPO_LOEKZ = 'L' THEN 0
                  WHEN ds.EKPO_RETPO = 'X' THEN 0
                  WHEN ds.EKPO_NETPR = 0 THEN 0
                  ELSE fp.amt_StdUnitPrice END)

  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   from    ekko_ekpo_eket ds,
         dim_date dt
 , fact_purchase fp
 WHERE     fp.dd_DocumentNo = EKPO_EBELN
         AND fp.dd_DocumentItemNo = EKPO_EBELP
         AND fp.dd_ScheduleNo = EKET_ETENR
         AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
                             where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
         AND dt.CompanyCode = EKPO_BUKRS
     AND dt.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
         AND fp.amt_StdUnitPrice = 0
     AND fp.amt_StdUnitPrice <> (CASE
                              WHEN EKKO_LOEKZ = 'L' THEN 0
                              WHEN EKPO_LOEKZ = 'L' THEN 0
                              WHEN EKPO_RETPO = 'X' THEN 0
                              WHEN EKPO_NETPR = 0 THEN 0
                ELSE fp.amt_StdUnitPrice
                END );



/* 3b Dim_DateidCosting */
  merge into fact_purchase fact
  using (select ds2.fact_purchaseid, ifnull(ds3.costingDate_Id, 1) Dim_DateidCosting
       from (select fp.fact_purchaseid, dt.FinancialYear, dt.FinancialMonthNumber,
              ds1.EKPO_BUKRS, ds1.EKPO_WERKS, ds1.EKPO_MATNR
           from fact_purchase fp
            inner join (select min(t.EKET_BEDAT)over(partition by t.EKPO_EBELN, t.EKPO_EBELP) min_EKET_BEDAT, t.*
                  from ekko_ekpo_eket t) ds1 on    fp.dd_DocumentNo = ds1.EKPO_EBELN
                                 AND fp.dd_DocumentItemNo = ds1.EKPO_EBELP
                                 AND fp.dd_ScheduleNo = ds1.EKET_ETENR
            inner join dim_date dt on    dt.DateValue = ds1.min_EKET_BEDAT
                         and dt.CompanyCode = ds1.EKPO_BUKRS
             and dt.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
         where fp.amt_StdUnitPrice = 0) ds2
      left join (select z.* from tmp_getCostingDate z where z.fact_script_name = 'bi_populate_purchasing_fact') ds3
          on    ds3.pCompanyCode = ds2.EKPO_BUKRS
            AND ds3.pPlant = ds2.EKPO_WERKS
            AND ds3.pMaterialNo = ds2.EKPO_MATNR
            AND ds3.pFiYear = ds2.FinancialYear
            AND ds3.pPeriod = ds2.FinancialMonthNumber
      ) src
  on fact.fact_purchaseid = src.fact_purchaseid
  when matched then update set fact.Dim_DateidCosting = src.Dim_DateidCosting
  where fact.Dim_DateidCosting <> src.Dim_DateidCosting
  AND ifnull(fact.Dim_DateidCosting ,-1) <> ifnull(src.Dim_DateidCosting ,-2);

      /* original
      UPDATE fact_purchase fp
      FROM  ekko_ekpo_eket,
          dim_date dt
      SET  fp.Dim_DateidCosting = ifnull((SELECT costingDate_Id
                        FROM tmp_getCostingDate z
                        WHERE z.pCompanyCode = EKPO_BUKRS
                          AND z.pPlant = EKPO_WERKS
                          AND z.pMaterialNo = EKPO_MATNR
                          AND z.pFiYear = dt.FinancialYear
                          AND z.pPeriod = dt.FinancialMonthNumber
                          AND z.fact_script_name = 'bi_populate_purchasing_fact'),1)
      WHERE     fp.dd_DocumentNo = EKPO_EBELN
          AND fp.dd_DocumentItemNo = EKPO_EBELP
          AND fp.dd_ScheduleNo = EKET_ETENR
          AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
                    where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
          AND dt.CompanyCode = EKPO_BUKRS
          AND fp.amt_StdUnitPrice = 0 */

/* End of Update 3 */

/* Update 4 */

UPDATE fact_purchase fp
SET amt_UnitPrice = ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                                      /* * ct_ExchangeRate */       /* LK: 12 Sep 2013: Removed as a part of currency changes */
                            * (convert(decimal (18,4), EKPO_BPUMZ) / CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END)
              ),0)
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
WHERE     fp.dd_DocumentNo     = EKPO_EBELN
    AND fp.dd_DocumentItemNo = EKPO_EBELP
    AND fp.dd_ScheduleNo     = EKET_ETENR;

UPDATE fact_purchase fp
SET  amt_DeliveryTotal = fp.ct_DeliveryQty * ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                            /* * ct_ExchangeRate */   /* Removed as a part of currency changes */
                            * (convert(decimal (18,4),EKPO_BPUMZ)/CASE WHEN ifnull(EKPO_BPUMN,0) = 0 THEN 1 ELSE EKPO_BPUMN END)),0)
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
WHERE     fp.dd_DocumentNo     = EKPO_EBELN
    AND fp.dd_DocumentItemNo = EKPO_EBELP
    AND fp.dd_ScheduleNo     = EKET_ETENR;

UPDATE fact_purchase fp
SET  amt_StdPriceAmt = fp.ct_DeliveryQty * fp.amt_StdUnitPrice
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
WHERE   fp.dd_DocumentNo = EKPO_EBELN
  AND fp.dd_DocumentItemNo = EKPO_EBELP
  AND fp.dd_ScheduleNo = EKET_ETENR;

/*UPDATE fact_purchase fp
SET  amt_DeliveryPPV = ifnull((CASE WHEN amt_StdUnitPrice = 0 THEN 0
                                       ELSE ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                                                    * (convert(decimal (18,4),EKPO_BPUMZ)/case when ifnull(EKPO_BPUMN,0)=0 then 1 else EKPO_BPUMN end)),0) - amt_StdUnitPrice
                                  END), 0) * fp.ct_DeliveryQty
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
WHERE fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR*/

MERGE INTO fact_purchase fp
 USING (SELECT fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_ScheduleNo, MAX(ifnull((CASE WHEN amt_StdUnitPrice = 0 THEN 0
                                       ELSE ifnull(((EKPO_NETPR / (CASE WHEN ifnull(EKPO_PEINH, 0) = 0 THEN 1 ELSE EKPO_PEINH END))
                                                    /** ct_ExchangeRate*/
                                                    * (convert(decimal (18,4),EKPO_BPUMZ)/case when ifnull(EKPO_BPUMN,0)=0 then 1 else EKPO_BPUMN end)),0) - amt_StdUnitPrice
                                  END), 0) * fp.ct_DeliveryQty) as ct_DeliveryQty
    FROM ekko_ekpo_eket dm_di_ds, fact_purchase fp
    where fp.dd_DocumentNo = EKPO_EBELN
    AND fp.dd_DocumentItemNo = EKPO_EBELP
    AND fp.dd_ScheduleNo = EKET_ETENR
    GROUP BY fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_ScheduleNo) x
ON (fp.dd_DocumentNo = x.dd_DocumentNo and
fp.dd_DocumentItemNo = x.dd_DocumentItemNo and
fp.dd_ScheduleNo = x.dd_ScheduleNo)
WHEN MATCHED THEN
UPDATE SET fp.amt_DeliveryPPV = x.ct_DeliveryQty;

/* End of Update 4 */

/* Insert 2 */
delete from NUMBER_FOUNTAIN where table_name = 'dim_purchasemisc';

INSERT INTO NUMBER_FOUNTAIN
select 'dim_purchasemisc',ifnull(max(dim_purchasemiscid),0)
FROM dim_purchasemisc;

INSERT INTO dim_purchasemisc(ItemGRIndicator,
                               ItemDeliveryComplete,
                               ItemReject,
                               ItemGRNonValue,
                               ItemFinalInvoice,
                               ItemProduceInhouse,
                               ItemReturn,
                               HeaderRelease,
                               ExchangeRateFix,
                               DeliveryFixed,
                               DeliveryComplete,
                               FirmedDelivery,
                               ItemStatistical,
                 UnlimitedOverDelivery,
                 dim_purchasemiscid)
SELECT tp.*,
    (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_purchasemisc')+ row_number() over(order by '') as dim_purchasemiscid
FROM (SELECT DISTINCT
         ifnull(EKPO_WEPOS, 'Not Set') ItemGRIndicator,
         ifnull(EKPO_ELIKZ, 'Not Set') ItemDeliveryComplete,
         ifnull(EKPO_ABSKZ, 'Not Set') ItemReject,
         ifnull(EKPO_WEUNB, 'Not Set') ItemGRNonValue,
         ifnull(EKPO_EREKZ, 'Not Set') ItemFinalInvoice,
         ifnull(EKPO_J_1BOWNPRO, 'Not Set') ItemProduceInhouse,
         ifnull(EKPO_RETPO, 'Not Set') ItemReturn,
         (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END) HeaderRelease,
         ifnull(EKKO_KUFIX, 'Not Set') ExchangeRateFix,
         ifnull(EKKO_FIXPO, 'Not Set') DeliveryFixed,
         ifnull(EKKO_AUTLF, 'Not Set') DeliveryComplete,
         CASE WHEN ( (fpr.ct_FirmZone > 0) AND (dtdel.DateValue - fpr.ct_FirmZone <= CURRENT_TIMESTAMP ) ) THEN 'X' ELSE 'Not Set' END FirmedDelivery,
         ifnull(EKPO_STAPO, 'Not Set') ItemStatistical,
     ifnull(EKPO_UEBTK, 'Not Set') UnlimitedOverDelivery
    FROM fact_purchase fpr,
           ekko_ekpo_eket dm_di,
           Dim_Date dtdel
      WHERE     fpr.dd_DocumentNo = dm_di.EKPO_EBELN
            AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
          AND fpr.dd_ScheduleNo = EKET_ETENR
            AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
            AND not exists (select 1
                      from Dim_PurchaseMisc pmisc
                            where pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
                              AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
                              AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
                              AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
                              AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
                              AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
                              AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
                              AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
                              AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
                              AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
                              AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
                              AND pmisc.FirmedDelivery = CASE WHEN ((fpr.ct_FirmZone > 0) AND (dtdel.DateValue -  fpr.ct_FirmZone <= CURRENT_TIMESTAMP)) THEN 'X' ELSE 'Not Set' END
                              AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')
                AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set'))) tp;

/* End of Insert 2 */

/* Update 5 */

DROP TABLE IF EXISTS tmp_upd_fp_Dim_PurchaseMisc;
CREATE TABLE tmp_upd_fp_Dim_PurchaseMisc
AS
SELECT fpr.dd_DocumentNo,fpr.dd_DocumentItemNo,fpr.dd_ScheduleNo,MAX(pmisc.Dim_PurchaseMiscid) Dim_PurchaseMiscid
FROM fact_purchase fpr,Dim_PurchaseMisc pmisc, ekko_ekpo_eket dm_di, Dim_Date dtdel
   WHERE     fpr.dd_DocumentNo = dm_di.EKPO_EBELN
         AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
         AND fpr.dd_ScheduleNo = EKET_ETENR
         AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
         AND pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
         AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
         AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
         AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
         AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
         AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
         AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
         AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
         AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
         AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
         AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
                 AND fpr.ct_FirmZone > 0 AND (dtdel.DateValue - fpr.ct_FirmZone <= CURRENT_TIMESTAMP)
                 AND pmisc.FirmedDelivery = 'X'
         AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')
                 AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set')
GROUP BY fpr.dd_DocumentNo,fpr.dd_DocumentItemNo,fpr.dd_ScheduleNo;

MERGE INTO fact_purchase fact
 USING (SELECT distinct fpr.fact_purchaseid, pmisc.Dim_PurchaseMiscid
 AS Dim_PurchaseMiscid
       FROM   tmp_upd_fp_Dim_PurchaseMisc pmisc
   , fact_purchase fpr
       WHERE     fpr.dd_DocumentNo = pmisc.dd_DocumentNo
       AND fpr.dd_DocumentItemNo = pmisc.dd_DocumentItemNo
       AND fpr.dd_ScheduleNo = pmisc.dd_ScheduleNo
AND fpr.Dim_PurchaseMiscid <> pmisc.Dim_PurchaseMiscid ) src
 ON fact.fact_purchaseid = src.fact_purchaseid
WHEN MATCHED THEN UPDATE
 SET fact.Dim_PurchaseMiscid  = src.Dim_PurchaseMiscid
WHERE ifnull(fact.Dim_PurchaseMiscid ,-1) <> ifnull(src.Dim_PurchaseMiscid ,-2);


DROP TABLE IF EXISTS tmp_upd_fp_Dim_PurchaseMisc;
CREATE TABLE tmp_upd_fp_Dim_PurchaseMisc
AS
SELECT fpr.dd_DocumentNo,fpr.dd_DocumentItemNo,fpr.dd_ScheduleNo,MAX(pmisc.Dim_PurchaseMiscid) Dim_PurchaseMiscid
FROM fact_purchase fpr,Dim_PurchaseMisc pmisc, ekko_ekpo_eket dm_di, Dim_Date dtdel
   WHERE     fpr.dd_DocumentNo = dm_di.EKPO_EBELN
         AND fpr.dd_DocumentItemNo = dm_di.EKPO_EBELP
         AND fpr.dd_ScheduleNo = EKET_ETENR
         AND dtdel.Dim_Dateid = fpr.Dim_DateidDelivery
         AND pmisc.DeliveryComplete = ifnull(EKKO_AUTLF, 'Not Set')
         AND pmisc.DeliveryFixed = ifnull(EKKO_FIXPO, 'Not Set')
         AND pmisc.ExchangeRateFix = ifnull(EKKO_KUFIX, 'Not Set')
         AND pmisc.HeaderRelease = (CASE WHEN EKKO_FRGZU IS NULL THEN 'Not Set' ELSE 'X' END)
         AND pmisc.ItemDeliveryComplete = ifnull(EKPO_ELIKZ, 'Not Set')
         AND pmisc.ItemFinalInvoice = ifnull(EKPO_EREKZ, 'Not Set')
         AND pmisc.ItemGRIndicator = ifnull(EKPO_WEPOS, 'Not Set')
         AND pmisc.ItemGRNonValue = ifnull(EKPO_WEUNB, 'Not Set')
         AND pmisc.ItemProduceInhouse = ifnull(EKPO_J_1BOWNPRO, 'Not Set')
         AND pmisc.ItemReject = ifnull(EKPO_ABSKZ, 'Not Set')
         AND pmisc.ItemReturn = ifnull(EKPO_RETPO, 'Not Set')
                 AND ( fpr.ct_FirmZone <= 0 OR (dtdel.DateValue - fpr.ct_FirmZone > CURRENT_TIMESTAMP))
                 AND pmisc.FirmedDelivery = 'Not Set'
         AND pmisc.ItemStatistical = ifnull(EKPO_STAPO, 'Not Set')
                 AND pmisc.UnlimitedOverDelivery = ifnull(EKPO_UEBTK, 'Not Set')
GROUP BY fpr.dd_DocumentNo,fpr.dd_DocumentItemNo,fpr.dd_ScheduleNo;

  MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, pmisc.Dim_PurchaseMiscid
         AS Dim_PurchaseMiscid
         FROM   tmp_upd_fp_Dim_PurchaseMisc pmisc
     , fact_purchase fpr
         WHERE     fpr.dd_DocumentNo = pmisc.dd_DocumentNo
         AND fpr.dd_DocumentItemNo = pmisc.dd_DocumentItemNo
         AND fpr.dd_ScheduleNo = pmisc.dd_ScheduleNo
        AND fpr.Dim_PurchaseMiscid <> pmisc.Dim_PurchaseMiscid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_PurchaseMiscid  = src.Dim_PurchaseMiscid
 WHERE ifnull(fact.Dim_PurchaseMiscid ,-1) <> ifnull(src.Dim_PurchaseMiscid ,-2);

/* End of Update 5 */
  /* Dim_DateIdPRRelease */

/*DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_eket_eban*/
/*
CREATE TABLE tmp_latest_ekko_ekpo_eket_eban
AS
SELECT DISTINCT dm_di.*
FROM ekko_ekpo_eket_eban dm_di,
(SELECT dm_di.EBAN_BANFN,dm_di.EBAN_BNFPO,dm_di.EKPO_BUKRS,max(EBAN_FRGDT) EBAN_FRGDT
FROM ekko_ekpo_eket_eban dm_di GROUP BY dm_di.EBAN_BANFN,dm_di.EBAN_BNFPO,dm_di.EKPO_BUKRS ) t
WHERE dm_di.EBAN_BANFN = t.EBAN_BANFN AND dm_di.EBAN_BNFPO = t.EBAN_BNFPO AND dm_di.EKPO_BUKRS = t.EKPO_BUKRS
AND dm_di.EBAN_FRGDT = t.EBAN_FRGDT*/

DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_eket_eban;
CREATE TABLE tmp_latest_ekko_ekpo_eket_eban
AS
SELECT dm_di.EBAN_BANFN,dm_di.EBAN_BNFPO,dm_di.EKPO_BUKRS,max(dm_di.EBAN_FRGDT) EBAN_FRGDT, max(dm_di.EBAN_BADAT) EBAN_BADAT
FROM ekko_ekpo_eket_eban dm_di
GROUP BY dm_di.EBAN_BANFN,dm_di.EBAN_BNFPO,dm_di.EKPO_BUKRS;

  MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, first_value(dt.Dim_DateId) over (partition by  fpr.fact_purchaseid order by fpr.fact_purchaseid)
   AS Dim_DateIdPRRelease
         FROM tmp_latest_ekko_ekpo_eket_eban dm_di,
         Dim_Date dt
     , fact_purchase fpr
         WHERE     fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
         AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
         AND dt.CompanyCode = dm_di.EKPO_BUKRS
         AND dt.DateValue = dm_di.EBAN_FRGDT
     and dt.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
         AND fpr.Dim_DateIdPRRelease <> dt.Dim_DateId
         AND fpr.dd_PurchaseReqNo <> 'Not Set' ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_DateIdPRRelease  = src.Dim_DateIdPRRelease
 WHERE ifnull(fact.Dim_DateIdPRRelease ,-1) <> ifnull(src.Dim_DateIdPRRelease ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, first_value(rdt.Dim_DateId) over (partition by  fpr.fact_purchaseid order by fpr.fact_purchaseid)
   AS Dim_DateIdRequistionDate
         FROM
         tmp_latest_ekko_ekpo_eket_eban dm_di,
         Dim_Date rdt
     , fact_purchase fpr
         WHERE     fpr.dd_PurchaseReqNo = dm_di.EBAN_BANFN
         AND fpr.dd_PurchaseReqItemNo = dm_di.EBAN_BNFPO
         AND rdt.CompanyCode = dm_di.EKPO_BUKRS
         AND rdt.DateValue = dm_di.EBAN_BADAT
     and rdt.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
         AND fpr.Dim_DateIdRequistionDate <> rdt.Dim_DateId
         AND fpr.dd_PurchaseReqNo <> 'Not Set' ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_DateIdRequistionDate  = src.Dim_DateIdRequistionDate
 WHERE ifnull(fact.Dim_DateIdRequistionDate ,-1) <> ifnull(src.Dim_DateIdRequistionDate ,-2);


/* Octavian: EA changes as to how the EBELN,EBELP comb is taken */
DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_ekkn_pre;
CREATE TABLE tmp_latest_ekko_ekpo_ekkn_pre AS
SELECT e.*,
row_number() over (partition by EKKN_EBELN,EKKN_EBELP
order by EKPO_AEDAT desc, EKKN_ZEKKN asc)  AS counter
FROM ekko_ekpo_ekkn e;

DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_ekkn;
CREATE TABLE tmp_latest_ekko_ekpo_ekkn AS
SELECT *
FROM tmp_latest_ekko_ekpo_ekkn_pre
WHERE counter = 1;

/* DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_ekkn
CREATE TABLE tmp_latest_ekko_ekpo_ekkn
AS
SELECT DISTINCT dm_di.*
FROM ekko_ekpo_ekkn dm_di,
(SELECT dm_di.EKKN_EBELN,dm_di.EKKN_EBELP,max(ekpo_aedat) ekpo_aedat
FROM ekko_ekpo_ekkn dm_di GROUP BY dm_di.EKKN_EBELN,dm_di.EKKN_EBELP ) t
WHERE dm_di.EKKN_EBELN = t.EKKN_EBELN AND dm_di.EKKN_EBELP = t.EKKN_EBELP AND dm_di.ekpo_aedat = t.ekpo_aedat */

/*Georgiana Changes 14 Jan 2015 Every Angle Addons*/
MERGE INTO fact_purchase fact
   USING (SELECT fpr.fact_purchaseid, ifnull(ek.EKKN_ABLAD,'Not Set') AS dd_unloadingpoint
         from tmp_latest_ekko_ekpo_ekkn ek
, fact_purchase fpr
         WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
  AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
  AND fpr.dd_unloadingpoint <> ifnull(ek.EKKN_ABLAD, 'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_unloadingpoint  = src.dd_unloadingpoint
 WHERE ifnull(fact.dd_unloadingpoint ,-1) <> ifnull(src.dd_unloadingpoint ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT DISTINCT fpr.fact_purchaseid, FIRST_VALUE(dc.dim_costcenterid) OVER (PARTITION BY ek.EKKN_EBELN,ek.EKKN_EBELP ORDER BY dc.dim_costcenterid DESC) AS dim_costcenterid
         from tmp_latest_ekko_ekpo_ekkn ek, dim_costcenter dc
, fact_purchase fpr
         WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
  AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
  AND ek.EKKN_KOSTL = dc.code
  AND fpr.dim_costcenterid <> dc.dim_costcenterid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_costcenterid  = src.dim_costcenterid
 WHERE ifnull(fact.dim_costcenterid ,-1) <> ifnull(src.dim_costcenterid ,-2);

  MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, ifnull(ek.EKKN_SAKTO,'Not Set') AS dd_glaccountno
         from tmp_latest_ekko_ekpo_ekkn ek
, fact_purchase fpr
         WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
  AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
  AND fpr.dd_glaccountno <> ifnull(ek.EKKN_SAKTO, 'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_glaccountno  = src.dd_glaccountno
 WHERE ifnull(fact.dd_glaccountno ,-1) <> ifnull(src.dd_glaccountno ,-2);

    MERGE INTO fact_purchase fact
   USING (SELECT distinct  fpr.fact_purchaseid, ifnull(ek.EKKN_WEMPF,'Not Set') AS dd_googsrecipient
         from tmp_latest_ekko_ekpo_ekkn ek
, fact_purchase fpr
         WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
  AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
  AND fpr.dd_googsrecipient <> ifnull(ek.EKKN_WEMPF, 'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_googsrecipient  = src.dd_googsrecipient
 WHERE ifnull(fact.dd_googsrecipient ,-1) <> ifnull(src.dd_googsrecipient ,-2);

  /*Georgiana End of Changes*/


DROP TABLE IF EXISTS tmp_upd_ddintord;
CREATE TABLE tmp_upd_ddintord
AS
SELECT ek.EKKN_EBELN,ek.EKKN_EBELP,MAX(ek.EKKN_AUFNR) EKKN_AUFNR
FROM tmp_latest_ekko_ekpo_ekkn ek
GROUP BY ek.EKKN_EBELN,ek.EKKN_EBELP;

  MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, ifnull(ek.EKKN_AUFNR, 'Not Set') AS dd_IntOrder
         FROM tmp_upd_ddintord ek
     , fact_purchase fpr
         WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
         AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
     AND ifnull(fpr.dd_IntOrder,'yy') <> ifnull(ek.EKKN_AUFNR, 'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_IntOrder  = src.dd_IntOrder;

DROP TABLE IF EXISTS tmp_upd_wbselement;
CREATE TABLE tmp_upd_wbselement
AS
SELECT ek.EKKN_EBELN,ek.EKKN_EBELP,MAX(p.PRPS_POSID) PRPS_POSID
FROM tmp_latest_ekko_ekpo_ekkn ek, PRPS p
WHERE p.PRPS_PSPNR = ek.EKKN_PS_PSP_PNR
GROUP BY ek.EKKN_EBELN,ek.EKKN_EBELP;


  MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, ifnull(p.PRPS_POSID,'Not Set') AS dd_WBSElement
         FROM tmp_latest_ekko_ekpo_ekkn ek, PRPS p
     , fact_purchase fpr
         WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
         AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
         AND p.PRPS_PSPNR = ek.EKKN_PS_PSP_PNR
     AND ifnull(fpr.dd_WBSElement,'xx') <> ifnull(p.PRPS_POSID, 'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_WBSElement  = src.dd_WBSElement
 WHERE ifnull(fact.dd_WBSElement ,-1) <> ifnull(src.dd_WBSElement ,-2);

DROP TABLE IF EXISTS tmp_upd_fp_eee;
CREATE TABLE tmp_upd_fp_eee
AS
SELECT DISTINCT ek.EKKN_EBELN,ek.EKKN_EBELP,MAX(ek.EKKN_VBELN) EKKN_VBELN,MAX(ek.EKKN_VBELP) EKKN_VBELP,MAX(ek.EKKN_VETEN) EKKN_VETEN
from tmp_latest_ekko_ekpo_ekkn ek
GROUP BY ek.EKKN_EBELN,ek.EKKN_EBELP;

  MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, ifnull(ek.EKKN_VBELN,'Not Set') AS dd_SalesDocNo
         FROM tmp_upd_fp_eee ek
     , fact_purchase fpr
         WHERE fpr.dd_DocumentNo = ek.EKKN_EBELN
         AND fpr.dd_DocumentItemNo = ek.EKKN_EBELP
         AND fpr.dd_SalesDocNo <> ifnull(ek.EKKN_VBELN, 'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_SalesDocNo  = src.dd_SalesDocNo
 WHERE ifnull(fact.dd_SalesDocNo ,-1) <> ifnull(src.dd_SalesDocNo ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT DISTINCT fp.fact_purchaseid, max(ifnull(e.EINE_MINBM,0)) AS ct_MinimumPOQty
         FROM EINE e
   , fact_purchase fp
         WHERE e.EBELN = fp.dd_DocumentNo AND e.EBELP = fp.dd_DocumentItemNo
 AND ifnull(fp.ct_MinimumPOQty,-1) <> ifnull(e.EINE_MINBM, -2)
 group by fp.fact_purchaseid
 ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_MinimumPOQty  = src.ct_MinimumPOQty
 WHERE ifnull(fact.ct_MinimumPOQty ,-1) <> ifnull(src.ct_MinimumPOQty ,-2);

DELETE FROM fact_purchase
   WHERE EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE     EKPO_EBELN = dd_DocumentNo
                    AND EKPO_EBELP = dd_DocumentItemNo
                    AND EKPO_LOEKZ = 'L');


/*

  DELETE FROM fact_purchase
   WHERE NOT EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo
                    AND EKPO_EBELP = dd_DocumentItemNo)
         AND EXISTS
                (SELECT 1
                   FROM EKKO_EKPO
                  WHERE EKPO_EBELN = dd_DocumentNo
                        AND EKPO_EBELP = dd_DocumentItemNo)

  DELETE FROM fact_purchase
   WHERE EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo)
         AND NOT EXISTS
                    (SELECT 1
                       FROM EKKO_EKPO_EKET
                      WHERE EKPO_EBELN = dd_DocumentNo
                            AND EKPO_EBELP = dd_DocumentItemNo)

  DELETE FROM fact_purchase
   WHERE EXISTS
            (SELECT 1
               FROM EKKO_EKPO_EKET
              WHERE EKPO_EBELN = dd_DocumentNo
                    AND EKPO_EBELP = dd_DocumentItemNo)
         AND NOT EXISTS
                    (SELECT 1
                       FROM EKKO_EKPO_EKET
                      WHERE     EKPO_EBELN = dd_DocumentNo
                            AND EKPO_EBELP = dd_DocumentItemNo
                            AND EKET_ETENR = dd_ScheduleNo)*/


/* Backup rows for tracking before they are deleted */
INSERT INTO tmp_fact_purchase_deleted
SELECT dd_documentno,dd_documentitemno,dd_scheduleno,CURRENT_TIMESTAMP,fact_purchaseid,'D'
FROM fact_purchase
WHERE EXISTS ( select 1 from CDPOS_DEL_EKKOEKPOEKET a where a.CDPOS_OBJECTID = dd_DocumentNo AND a.CDPOS_TABNAME = 'EKKO' AND a.CDPOS_CHNGIND = 'D');

INSERT INTO tmp_fact_purchase_deleted
SELECT dd_documentno,dd_documentitemno,dd_scheduleno,CURRENT_TIMESTAMP,fact_purchaseid,'I'
FROM fact_purchase
WHERE exists (select 1 from CDPOS_DEL_EKKOEKPOEKET a where SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_DocumentNo AND SUBSTRING(a.CDPOS_TABKEY,14,5) = dd_DocumentItemNo AND a.CDPOS_TABNAME = 'EKPO' AND  a.CDPOS_CHNGIND = 'D' )
AND NOT EXISTS ( SELECT 1 FROM EKKO_EKPO_EKET e WHERE e.EKPO_EBELN = dd_DocumentNo AND e.EKPO_EBELP = dd_DocumentItemNo);


INSERT INTO tmp_fact_purchase_deleted
SELECT dd_documentno,dd_documentitemno,dd_scheduleno,CURRENT_TIMESTAMP,fact_purchaseid,'S'
FROM fact_purchase
WHERE exists (select 1 from CDPOS_DEL_EKKOEKPOEKET a where SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_DocumentNo AND SUBSTRING(a.CDPOS_TABKEY,14,5) = dd_DocumentItemNo AND SUBSTRING(a.CDPOS_TABKEY,19,4) = dd_ScheduleNo  AND a.CDPOS_TABNAME = 'EKET' AND  a.CDPOS_CHNGIND = 'D' )
AND NOT EXISTS ( SELECT 1 FROM EKKO_EKPO_EKET e WHERE e.EKPO_EBELN = dd_DocumentNo AND e.EKPO_EBELP = dd_DocumentItemNo AND e.EKET_ETENR = dd_ScheduleNo);

/* Delete based on docno */
DELETE FROM fact_purchase
WHERE EXISTS ( select 1 from CDPOS_DEL_EKKOEKPOEKET a where a.CDPOS_OBJECTID = dd_DocumentNo AND a.CDPOS_TABNAME = 'EKKO' AND a.CDPOS_CHNGIND = 'D');

/* Delete based on itemno */
DELETE FROM fact_purchase
WHERE exists (select 1 from CDPOS_DEL_EKKOEKPOEKET a where SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_DocumentNo AND SUBSTRING(a.CDPOS_TABKEY,14,5) = dd_DocumentItemNo AND a.CDPOS_TABNAME = 'EKPO' AND  a.CDPOS_CHNGIND = 'D' )
AND NOT EXISTS ( SELECT 1 FROM EKKO_EKPO_EKET e WHERE e.EKPO_EBELN = dd_DocumentNo AND e.EKPO_EBELP = dd_DocumentItemNo);


/* Delete based on schedule */
DELETE FROM fact_purchase
WHERE exists (select 1 from CDPOS_DEL_EKKOEKPOEKET a where SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_DocumentNo AND SUBSTRING(a.CDPOS_TABKEY,14,5) = dd_DocumentItemNo AND SUBSTRING(a.CDPOS_TABKEY,19,4) = dd_ScheduleNo  AND a.CDPOS_TABNAME = 'EKET' AND  a.CDPOS_CHNGIND = 'D' )
AND NOT EXISTS ( SELECT 1 FROM EKKO_EKPO_EKET e WHERE e.EKPO_EBELN = dd_DocumentNo AND e.EKPO_EBELP = dd_DocumentItemNo AND e.EKET_ETENR = dd_ScheduleNo);

DROP TABLE IF EXISTS tmp_pf_mnb1;
CREATE TABLE  tmp_pf_mnb1
AS
SELECT    x.MATNR,x.BWKEY, max((x.LFGJA * 100) + x.LFMON) as max_lfgja_lfmon from mbew_no_bwtar x
GROUP BY x.MATNR,x.BWKEY;

DROP TABLE IF EXISTS tmp_pf_mnb;
CREATE TABLE  tmp_pf_mnb
AS
SELECT m.*
FROM mbew_no_bwtar m,tmp_pf_mnb1 x
WHERE  x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = x.max_lfgja_lfmon;


  MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, MBEW_ZPLP1/PEINH AS amt_PlannedPrice1
         FROM   tmp_pf_mnb m,
         dim_plant pl,
         dim_part prt
  , fact_purchase f
         WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_PlannedPrice1  = src.amt_PlannedPrice1
 WHERE fact.amt_PlannedPrice1 <> src.amt_PlannedPrice1;


  UPDATE fact_purchase f
  SET amt_PlannedPrice1 = 0
  FROM   tmp_pf_mnb m, fact_purchase f,
         dim_plant pl,
         dim_part prt
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
  AND amt_PlannedPrice1 IS NULL;


  MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, MBEW_ZPLPR/PEINH AS amt_PlannedPrice
         FROM   tmp_pf_mnb m,
         dim_plant pl,
         dim_part prt
  , fact_purchase f
         WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_PlannedPrice  = src.amt_PlannedPrice
 WHERE ifnull(fact.amt_PlannedPrice ,-1) <> ifnull(src.amt_PlannedPrice ,-2);

  UPDATE fact_purchase f
  SET amt_PlannedPrice = 0
  FROM   tmp_pf_mnb m, fact_purchase f,
         dim_plant pl,
         dim_part prt
  WHERE f.dim_partid = prt.Dim_partid
    and f.Dim_PlantidOrdering = pl.dim_plantid
    and prt.PartNumber = m.MATNR
    and pl.ValuationArea = m.BWKEY
  AND amt_PlannedPrice is null;



drop table if exists tmp_ekko_ekpo_ekes_purchasing_fact;
create table tmp_ekko_ekpo_ekes_purchasing_fact
as
select distinct e.*,dt.CompanyCode, dt.Dim_Dateid
from ekko_ekpo_ekes e, dim_date dt
WHERE e.EKES_EINDT = dt.DateValue
AND e.EKES_LOEKZ IS NULL
order by EKES_ETENS desc;
drop table if exists tmp_dim_pc_purchasing_fact;
create table tmp_dim_pc_purchasing_fact
as select * from dim_profitcenter order by ValidTo desc;

  UPDATE fact_purchase fpr
  SET dim_profitcenterid = ifnull(pc.Dim_ProfitCenterid, 1)
  FROM fact_purchase fpr
      inner join ekko_ekpo_eket dm_di_ds on fpr.dd_DocumentNo = dm_di_ds.EKPO_EBELN AND fpr.dd_DocumentItemNo = dm_di_ds.EKPO_EBELP AND fpr.dd_ScheduleNo = dm_di_ds.EKET_ETENR
      left join tmp_dim_pc_purchasing_fact pc on pc.ProfitCenterCode = dm_di_ds.EKPO_KO_PRCTR;

/* Dim_DateIdVendorConfirmation */
merge into fact_purchase fact
using (select distinct t2.fact_purchaseid, ifnull(dt.Dim_Dateid, 1) as Dim_DateIdVendorConfirmation
     from (select fact_purchaseid, EKES_EINDT, dp.CompanyCode, dp.plantcode
       from fact_purchase f
          inner join (select t1.*,
                     row_number() over (partition by t1.EKPO_EBELN, t1.EKPO_EBELP order by t1.EKES_ETENS desc) rno
                from ekko_ekpo_ekes t1
                ) e on    f.dd_DocumentNo = e.EKPO_EBELN
                    AND f.dd_DocumentItemNo = e.EKPO_EBELP
          inner join dim_plant dp on f.Dim_PlantidOrdering = dp.Dim_PlantId
       where e.rno = 1 ) t2
      left join dim_date dt on    dt.DateValue   = t2.EKES_EINDT
                      and dt.CompanyCode = t2.CompanyCode
                  and dt.plantcode_factory = t2.plantcode  /* plantcode_factory additional condition- BI-5085 */
                  ) src
on fact.fact_purchaseid = src.fact_purchaseid
when matched then update set fact.Dim_DateIdVendorConfirmation = src.Dim_DateIdVendorConfirmation
where fact.Dim_DateIdVendorConfirmation <> src.Dim_DateIdVendorConfirmation;

/*Georgiana Every Angle Addons 13 Jan 2016*/
 drop table if exists tmp_ekko_ekpo_ekes;
 create table tmp_ekko_ekpo_ekes as
  select es.*,et.EKET_ETENR, et.EKPO_BUKRS from
  ekko_ekpo_ekes es, ekko_ekpo_eket et
  where es.ekpo_ebeln = et.ekpo_ebeln
  and es.ekpo_ebelp = et.ekpo_ebelp
  and es.ekes_etens = et.ekpo_drunr;

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKES_EBTYP,'Not Set') AS dd_confirmationcategory
         from tmp_ekko_ekpo_ekes et
, fact_purchase fp
         where
 fp.dd_DocumentNo = et.EKPO_EBELN
 AND fp.dd_DocumentItemNo = et.EKPO_EBELP
 AND fp.dd_ScheduleNo = et.EKET_ETENR
 AND dd_confirmationcategory <> ifnull(EKES_EBTYP,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_confirmationcategory  = src.dd_confirmationcategory
 WHERE ifnull(fact.dd_confirmationcategory ,'Not Set') <> ifnull(src.dd_confirmationcategory, 'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKES_KZDIS,'Not Set') AS dd_confirmationisrelevantind
         from tmp_ekko_ekpo_ekes et
, fact_purchase fp
         where
 fp.dd_DocumentNo = et.EKPO_EBELN
 AND fp.dd_DocumentItemNo = et.EKPO_EBELP
 AND fp.dd_ScheduleNo = et.EKET_ETENR
 AND dd_confirmationisrelevantind <> ifnull(EKES_KZDIS ,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_confirmationisrelevantind = src.dd_confirmationisrelevantind
 WHERE ifnull(fact.dd_confirmationisrelevantind,'Not Set') <> ifnull(src.dd_confirmationisrelevantind,'Not Set');


MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, dt.dim_dateid
   AS dim_dateidconfcreationdate
         from tmp_ekko_ekpo_ekes et, dim_date dt
, fact_purchase fp
         where
 fp.dd_DocumentNo = et.EKPO_EBELN
 AND fp.dd_DocumentItemNo = et.EKPO_EBELP
 AND fp.dd_ScheduleNo = et.EKET_ETENR
 AND dt.datevalue = ifnull(EKES_ERDAT,'0001-01-01') and dt.companycode = ifnull(et.EKPO_BUKRS,'Not Set')
 and dt.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
 AND dim_dateidconfcreationdate <> dt.dim_dateid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_dateidconfcreationdate  = src.dim_dateidconfcreationdate;

  drop table if exists tmp_ekko_ekpo_ekes;
/*End of changes 13 Jan 2016*/

DROP TABLE IF EXISTS tmp_rbkp_rseg_latest_doc;
CREATE TABLE tmp_rbkp_rseg_latest_doc
AS
SELECT rbs.RSEG_EBELN,rbs.RSEG_EBELP,FIRST_VALUE(RBKP_BELNR) OVER(PARTITION BY rbs.RSEG_EBELN,rbs.RSEG_EBELP ORDER BY rbkp_bldat DESC ) RBKP_BELNR
FROM rbkp_rseg rbs;

DROP TABLE IF EXISTS tmp_rbkp_rseg_latest_doc_item;
CREATE TABLE tmp_rbkp_rseg_latest_doc_item
AS
SELECT rbs.RSEG_EBELN,rbs.RSEG_EBELP,rbs.RBKP_BELNR,max(item.rseg_buzei) rseg_buzei
FROM tmp_rbkp_rseg_latest_doc rbs,rbkp_rseg item
WHERE rbs.RSEG_EBELN = item.RSEG_EBELN AND rbs.RSEG_EBELP = item.RSEG_EBELP AND rbs.RBKP_BELNR = item.RBKP_BELNR
group by rbs.RSEG_EBELN,rbs.RSEG_EBELP,rbs.RBKP_BELNR;

DROP TABLE IF EXISTS tmp_upd_rbkp_rseg;
CREATE TABLE tmp_upd_rbkp_rseg
AS
SELECT DISTINCT rbs.*
FROM rbkp_rseg rbs, tmp_rbkp_rseg_latest_doc_item t
WHERE rbs.RSEG_EBELN = t.RSEG_EBELN AND rbs.RSEG_EBELP = t.RSEG_EBELP AND rbs.RBKP_BELNR = t.RBKP_BELNR AND rbs.rseg_buzei = t.rseg_buzei;

/* LK : NOTE : This query ( the next 5 querries which were 1 single query in mysql ) is updating more rows than there are in fp. Which means join returns dups */
/* Update  f.dd_InvoiceNumber */
 MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, ifnull(RBKP_BELNR,'yy') AS dd_InvoiceNumber
         FROM  tmp_upd_rbkp_rseg rbs
, fact_purchase f
         WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND  ifnull(f.dd_InvoiceNumber,'xx') <> ifnull(RBKP_BELNR,'yy') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_InvoiceNumber  = src.dd_InvoiceNumber
 WHERE ifnull(fact.dd_InvoiceNumber,'xx') <> ifnull(src.dd_InvoiceNumber,'yy');

/* Update f.dd_InvoiceItemNo */
 MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, ifnull(RSEG_BUZEI,0) AS dd_InvoiceItemNo
         FROM  tmp_upd_rbkp_rseg rbs
, fact_purchase f
         WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.dd_InvoiceItemNo,-1) <> ifnull(RSEG_BUZEI,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_InvoiceItemNo  = src.dd_InvoiceItemNo
 WHERE ifnull(fact.dd_InvoiceItemNo ,-1) <> ifnull(src.dd_InvoiceItemNo ,-2);

/* Update f.amt_GrossInvAmt */
DROP TABLE IF EXISTS tmp_ekko_ekpo_rbkp_rseg_purchasing_fact;
CREATE TABLE tmp_ekko_ekpo_rbkp_rseg_purchasing_fact AS
  SELECT rbs.RSEG_EBELN DocNo,
    rbs.RSEG_EBELP DocItemNo,
    SUM(ifnull(RBKP_RMWWR, 0)*(CASE WHEN rbs.RSEG_SHKZG = 'H' THEN -1 ELSE 1 END)) InvAmount
  FROM rbkp_rseg rbs
  GROUP BY rbs.RSEG_EBELN,
    rbs.RSEG_EBELP;

MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, InvAmount
   AS amt_GrossInvAmt
         FROM tmp_ekko_ekpo_rbkp_rseg_purchasing_fact rbs
, fact_purchase f
         WHERE rbs.DocNo = f.dd_DocumentNo
  AND rbs.DocItemNo = f.dd_DocumentItemNo
  AND ifnull(f.amt_GrossInvAmt, - 1) <> InvAmount ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_GrossInvAmt  = src.amt_GrossInvAmt
 WHERE ifnull(fact.amt_GrossInvAmt ,-1) <> ifnull(src.amt_GrossInvAmt ,-2);

/* Update f.amt_NetInvAmt */
DROP TABLE IF EXISTS tmp_ekko_ekpo_rbkp_rseg_purchasing_fact;
CREATE TABLE tmp_ekko_ekpo_rbkp_rseg_purchasing_fact AS
  SELECT rbs.RSEG_EBELN DocNo,
    rbs.RSEG_EBELP DocItemNo,
    SUM(ifnull(RSEG_WRBTR, 0)*(CASE WHEN rbs.RSEG_SHKZG = 'H' THEN -1 ELSE 1 END)) NetInvAmount
  FROM rbkp_rseg rbs
  GROUP BY rbs.RSEG_EBELN,
    rbs.RSEG_EBELP;

MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, rbs.NetInvAmount
   AS amt_NetInvAmt
         FROM tmp_ekko_ekpo_rbkp_rseg_purchasing_fact rbs
, fact_purchase f
         WHERE rbs.DocNo = f.dd_DocumentNo
  AND rbs.DocItemNo = f.dd_DocumentItemNo
  AND ifnull(f.amt_NetInvAmt, - 1) <> rbs.NetInvAmount ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_NetInvAmt  = src.amt_NetInvAmt
 WHERE ifnull(fact.amt_NetInvAmt ,-1) <> ifnull(src.amt_NetInvAmt ,-2);

/* Update f.ct_InvQtyinPOUnit */
 MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, ifnull(RSEG_BPMNG,0) AS ct_InvQtyinPOUnit
         FROM  tmp_upd_rbkp_rseg rbs
, fact_purchase f
         WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.ct_InvQtyinPOUnit,-1) <> ifnull(RSEG_BPMNG,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_InvQtyinPOUnit  = src.ct_InvQtyinPOUnit
 WHERE ifnull(fact.ct_InvQtyinPOUnit ,-1) <> ifnull(src.ct_InvQtyinPOUnit ,-2);

/* Update f.ct_VenInvQtyinPOUnit */
 MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, ifnull(RSEG_BPRBM,0) AS ct_VenInvQtyinPOUnit
         FROM  tmp_upd_rbkp_rseg rbs
, fact_purchase f
         WHERE rbs.RSEG_EBELN = f.dd_DocumentNo AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
AND ifnull(f.ct_VenInvQtyinPOUnit,-1) <> ifnull(RSEG_BPRBM,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_VenInvQtyinPOUnit  = src.ct_VenInvQtyinPOUnit
 WHERE ifnull(fact.ct_VenInvQtyinPOUnit ,-1) <> ifnull(src.ct_VenInvQtyinPOUnit ,-2);


/* LK : Looks like the joins are returning dups in this query as well ( for the next 2 queries ) */
 MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, uom.Dim_UnitOfMeasureId
   AS Dim_InvoicePOUOMId
         FROM tmp_upd_rbkp_rseg rbs,dim_unitofmeasure uom
, fact_purchase f
         WHERE rbs.RSEG_EBELN = f.dd_DocumentNo
 AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
 AND uom.UOM = RSEG_BPRME AND uom.RowIsCurrent = 1
 AND f.Dim_InvoicePOUOMId = ifnull(uom.Dim_UnitOfMeasureId,-1) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_InvoicePOUOMId  = src.Dim_InvoicePOUOMId
 WHERE ifnull(fact.Dim_InvoicePOUOMId ,-1) <> ifnull(src.Dim_InvoicePOUOMId ,-2);


 MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, first_value(dt.dim_dateid) over ( partition by f.fact_purchaseid order by dt.dim_dateid desc)
   AS Dim_DateIdInvoiceRcvdDate
         FROM tmp_upd_rbkp_rseg rbs,dim_date dt
  , fact_purchase f
         WHERE rbs.RSEG_EBELN = f.dd_DocumentNo
 AND rbs.RSEG_EBELP = f.dd_DocumentItemNo
 AND dt.datevalue = ifnull(rbs.RBKP_BLDAT,'0001-01-01') AND dt.CompanyCode = ifnull(rbs.RSEG_BUKRS,'Not Set')
 AND dt.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
 AND ifnull(f.Dim_DateIdInvoiceRcvdDate,-1) <> ifnull(dt.dim_dateid,-2) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_DateIdInvoiceRcvdDate  = src.Dim_DateIdInvoiceRcvdDate
 WHERE ifnull(fact.Dim_DateIdInvoiceRcvdDate ,-1) <> ifnull(src.Dim_DateIdInvoiceRcvdDate ,-2);




DROP TABLE IF EXISTS tmp_pur_fact_konv  ;
CREATE TABLE   tmp_pur_fact_konv
AS
SELECT  KONV_KSCHL,KONV_KNUMV,sum(konv_kwert) as sum_konv_kwert
FROM KONV_PURCHASE k
GROUP BY KONV_KSCHL,KONV_KNUMV;


MERGE INTO facT_purchase fact
   USING (SELECT distinct p.facT_purchaseid,
          sum_konv_kwert AS amt_ConditionValueTax
         FROM EKKO_EKPO_EKET e,
     facT_purchase p, tmp_pur_fact_konv k
WHERE     p.dd_DocumentNo = e.EKPO_EBELN
AND p.dd_DocumentItemNo = e.EKPO_EBELP
AND p.dd_scheduleNo = e.EKET_ETENR
AND k.KONV_KSCHL = 'NAVS' AND k.KONV_KNUMV = e.EKKO_KNUMV ) src
   ON fact.facT_purchaseid = src.facT_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_ConditionValueTax  = src.amt_ConditionValueTax
 WHERE ifnull(fact.amt_ConditionValueTax ,-1) <> ifnull(src.amt_ConditionValueTax ,-2);



  MERGE INTO fact_purchase fact
   USING (SELECT distinct p.fact_purchaseid, v.dim_vendorid AS Dim_CustomPartnerFunctionId1

         FROM EKPA e1,EKKO_EKPO_EKET e,tmp_var_purchasing_fact, dim_vendor v, fact_purchase p

  WHERE  p.dd_DocumentNo = e.EKPO_EBELN
        AND p.dd_DocumentItemNo = e.EKPO_EBELP
        AND p.dd_scheduleNo = e.EKET_ETENR
        AND e.EKPO_EBELN = e1.EKPA_EBELN
        AND e.EKKO_EKORG = e1.EKPA_EKORG
        AND e1.EKPA_PARVW = pCustomPartnerFunction1
        AND v.VendorNumber = ifnull(e1.EKPA_LIFN2,'Not Set')) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_CustomPartnerFunctionId1  = src.Dim_CustomPartnerFunctionId1
 WHERE ifnull(fact.Dim_CustomPartnerFunctionId1 ,-1) <> ifnull(src.Dim_CustomPartnerFunctionId1 ,-2);



MERGE INTO fact_purchase fact
   USING (SELECT distinct p.fact_purchaseid,v.dim_vendorid AS Dim_CustomPartnerFunctionId2
         FROM EKPA e1,EKKO_EKPO_EKET e,tmp_var_purchasing_fact, dim_vendor v
    , fact_purchase p

  WHERE    p.dd_DocumentNo = e.EKPO_EBELN
        AND p.dd_DocumentItemNo = e.EKPO_EBELP
        AND p.dd_scheduleNo = e.EKET_ETENR
        AND e.EKPO_EBELN = e1.EKPA_EBELN
        AND e.EKKO_EKORG = e1.EKPA_EKORG
        AND e1.EKPA_PARVW = pCustomPartnerFunction2
        AND v.VendorNumber = ifnull(e1.EKPA_LIFN2,'Not Set')) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_CustomPartnerFunctionId2  = src.Dim_CustomPartnerFunctionId2
 WHERE ifnull(fact.Dim_CustomPartnerFunctionId2 ,-1) <> ifnull(src.Dim_CustomPartnerFunctionId2 ,-2);



MERGE INTO fact_purchase fact
   USING (SELECT distinct p.fact_purchaseid,v.dim_vendorid AS Dim_CustomPartnerFunctionId3
         FROM EKPA e1,EKKO_EKPO_EKET e,tmp_var_purchasing_fact, fact_purchase p, dim_vendor v
         WHERE v.VendorNumber = ifnull(e1.EKPA_LIFN2,'Not Set')
        AND p.dd_DocumentNo = e.EKPO_EBELN
        AND p.dd_DocumentItemNo = e.EKPO_EBELP
        AND p.dd_scheduleNo = e.EKET_ETENR
        AND e.EKPO_EBELN = e1.EKPA_EBELN
        AND e.EKKO_EKORG = e1.EKPA_EKORG
        AND ifnull(e1.EKPA_PARVW,'Not Set') = pCustomPartnerFunction3 ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_CustomPartnerFunctionId3  = src.Dim_CustomPartnerFunctionId3
 WHERE ifnull(fact.Dim_CustomPartnerFunctionId3 ,-1) <> ifnull(src.Dim_CustomPartnerFunctionId3 ,-2);



  MERGE INTO fact_purchase fact
   USING (SELECT distinct p.fact_purchaseid,v.dim_vendorid AS Dim_CustomPartnerFunctionId4
         FROM EKPA e1,EKKO_EKPO_EKET e,tmp_var_purchasing_fact, fact_purchase p, dim_vendor v
  WHERE    p.dd_DocumentNo = e.EKPO_EBELN
        AND p.dd_DocumentItemNo = e.EKPO_EBELP
        AND p.dd_scheduleNo = e.EKET_ETENR
        AND e.EKPO_EBELN = e1.EKPA_EBELN
        AND e.EKKO_EKORG = e1.EKPA_EKORG
        AND e1.EKPA_PARVW = pCustomPartnerFunction4
        AND v.VendorNumber = ifnull(e1.EKPA_LIFN2,'Not Set')) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_CustomPartnerFunctionId4  = src.Dim_CustomPartnerFunctionId4
 WHERE ifnull(fact.Dim_CustomPartnerFunctionId4 ,-1) <> ifnull(src.Dim_CustomPartnerFunctionId4 ,-2);

/* If received qty = required/delivered qty (+ or - 5%) then calculate LIFR % */
DROP TABLE IF EXISTS tmp_fp_nonzeroqty;
CREATE TABLE tmp_fp_nonzeroqty
AS
SELECT p.dd_DocumentNo,p.dd_DocumentItemNo,p.dd_scheduleNo, cast(0 as bigint) ct_lifr_percent_merck, priceconversion_equalto, priceconversion_denom, ct_ReceivedQty, ct_BaseUOMQty,
dim_dateidstatdelivery
FROM fact_purchase  p
WHERE ct_BaseUOMQty <> 0
AND priceconversion_denom <> 0
AND priceconversion_equalto <> 0;

delete from tmp_fp_nonzeroqty
WHERE ( priceconversion_equalto / priceconversion_denom ) = 0;

delete from tmp_fp_nonzeroqty
WHERE (ct_ReceivedQty / ct_BaseUOMQty ) = 0;

UPDATE tmp_fp_nonzeroqty
SET ct_lifr_percent_merck = (ct_ReceivedQty / ct_BaseUOMQty ) * ( priceconversion_equalto / priceconversion_denom )
WHERE ct_ReceivedQty >= 0.95 * ct_BaseUOMQty / ( priceconversion_equalto / priceconversion_denom )
AND ct_ReceivedQty <= 1.05 * ct_BaseUOMQty / ( priceconversion_equalto / priceconversion_denom )
AND ct_lifr_percent_merck <> (ct_ReceivedQty / ct_BaseUOMQty ) * ( priceconversion_equalto / priceconversion_denom ) ;

/* if Requested delivery date < Today - 1 working day then calculate LIFR % */
UPDATE tmp_fp_nonzeroqty
SET ct_lifr_percent_merck = (ct_ReceivedQty / ct_BaseUOMQty ) * ( priceconversion_equalto / priceconversion_denom )
FROM dim_date req
, tmp_fp_nonzeroqty
WHERE req.dim_dateid = dim_dateidstatdelivery
AND req.datevalue < current_date - 1
AND ct_lifr_percent_merck <> (ct_ReceivedQty / ct_BaseUOMQty ) * ( priceconversion_equalto / priceconversion_denom ) ;

MERGE INTO fact_purchase fact
   USING (SELECT distinct  p.fact_purchaseid, n.ct_lifr_percent_merck
    AS ct_lifr_percent_merck
         FROM tmp_fp_nonzeroqty n
, fact_purchase p
         WHERE p.dd_DocumentNo = n.dd_DocumentNo AND p.dd_DocumentItemNo = n.dd_DocumentItemNo AND p.dd_scheduleNo = n.dd_scheduleNo
AND p.ct_lifr_percent_merck <> n.ct_lifr_percent_merck ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_lifr_percent_merck  = src.ct_lifr_percent_merck
 WHERE ifnull(fact.ct_lifr_percent_merck ,-1) <> ifnull(src.ct_lifr_percent_merck ,-2);

DROP TABLE IF EXISTS tmp_fp_nonzeroqty;


/* LIFR = 100% if received date is >= Requested Delvy Date - 21 (working days)
and <= Requested Delvy Date + 1 (working days)  and Received Qty = Requested Sch Qty ( + or - 5%) */

UPDATE fact_purchase
SET ct_lifr_percent_merck = 100
    ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_date req, dim_date recvd,fact_purchase
WHERE req.dim_dateid = dim_dateidstatdelivery
AND recvd.dim_dateid = dim_dateiddelivery
AND recvd.datevalue >= case when req.datevalue='0001-01-01' then req.datevalue else req.datevalue - 21 end
AND recvd.datevalue <= req.datevalue + 1
AND ct_ReceivedQty >= 0.95 * ct_BaseUOMQty
AND ct_ReceivedQty <= 1.05 * ct_BaseUOMQty;

/* In all other cases, ct_lifr_percent_merck is NULL */

/* iOTIF changes - Additional 4 columns updated */

MERGE INTO fact_purchase fact
   USING (SELECT distinct a.fact_purchaseid, extsuppbaseline_merck AS ct_extsuppbaseline_merck
         FROM dim_plant b
, fact_purchase a
         WHERE dim_plantidordering = b.dim_plantid
AND ct_extsuppbaseline_merck <> extsuppbaseline_merck ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_extsuppbaseline_merck  = src.ct_extsuppbaseline_merck
 WHERE ifnull(fact.ct_extsuppbaseline_merck ,-1) <> ifnull(src.ct_extsuppbaseline_merck ,-2);


MERGE INTO fact_purchase fact
   USING (SELECT distinct a.fact_purchaseid, extsupptarget_merck AS extsupptarget_merck
         FROM dim_plant b
, fact_purchase a
         WHERE dim_plantidordering = b.dim_plantid
AND ct_extsupptarget_merck <> extsupptarget_merck ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_extsupptarget_merck  = src.extsupptarget_merck
 WHERE ifnull(fact.ct_extsupptarget_merck ,-1) <> ifnull(src.extsupptarget_merck ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct a.fact_purchaseid, intsuppbaseline_merck AS intsuppbaseline_merck
         FROM dim_plant b
, fact_purchase a
         WHERE dim_plantidordering = b.dim_plantid
AND ct_intsuppbaseline_merck <> intsuppbaseline_merck ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_intsuppbaseline_merck  = src.intsuppbaseline_merck
 WHERE ifnull(fact.ct_intsuppbaseline_merck ,-1) <> ifnull(src.intsuppbaseline_merck ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct a.fact_purchaseid, intsupptarget_merck AS intsupptarget_merck
         FROM dim_plant b
, fact_purchase a
         WHERE dim_plantidordering = b.dim_plantid
AND ct_intsupptarget_merck <> intsupptarget_merck ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_intsupptarget_merck  = src.intsupptarget_merck
 WHERE ifnull(fact.ct_intsupptarget_merck ,-1) <> ifnull(src.intsupptarget_merck ,-2);

UPDATE fact_purchase
SET dd_intraplantflag_merck = 'N'
    ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_intraplantflag_merck <> 'N';

UPDATE fact_purchase
SET dd_intraplantflag_merck = 'Y'
    ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dim_plantidordering = dim_plantidsupplying
And dd_intraplantflag_merck <> 'Y';

/* Octavian: Every Angle Transition Addons */
MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, ifnull(e.EINA_UMREZ,0) AS dd_numerator
         FROM EINA e
, fact_purchase fpr
         where fpr.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fpr.dd_numerator <> ifnull(e.EINA_UMREZ,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_numerator  = src.dd_numerator
 WHERE ifnull(fact.dd_numerator ,-1) <> ifnull(src.dd_numerator ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct ia.fact_purchaseid, b.dim_batchid AS dim_batchid
         from dim_batch b, dim_part dp
, fact_purchase ia
         where ia.dim_partid = dp.dim_partid and
ia.dd_batch = b.batchnumber and
dp.partnumber = b.partnumber
and dp.plant = b.plantcode
and ia.dim_batchid <> b.dim_batchid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_batchid  = src.dim_batchid
 WHERE ifnull(fact.dim_batchid ,-1) <> ifnull(src.dim_batchid ,-2);
/* Octavian: Every Angle Transition Addons */

/*Georgiana: Every Angle Transition Addons*/
MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid,uom.dim_unitofmeasureid AS dim_unitofpricemeasureid
         from dim_unitofmeasure uom , ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND uom.uom = ifnull(EKPO_BPRME, 'Not Set')
AND dim_unitofpricemeasureid<>uom.dim_unitofmeasureid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_unitofpricemeasureid = src.dim_unitofpricemeasureid
 WHERE ifnull(fact.dim_unitofpricemeasureid,-1) <> ifnull(src.dim_unitofpricemeasureid,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_UMREN,0) AS dd_denominator
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.dd_denominator <> ifnull(e.EINA_UMREN,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_denominator  = src.dd_denominator
 WHERE ifnull(fact.dd_denominator ,-1) <> ifnull(src.dd_denominator ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_NETPR,0) AS amt_netorderprice
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.amt_netorderprice <> ifnull(EKPO_NETPR,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_netorderprice = src.amt_netorderprice
 WHERE ifnull(fact.amt_netorderprice,-1) <> ifnull(src.amt_netorderprice,-2);

/*Georgiana End of changes*/

/* Octavian: Every Angle Transition Addons */
MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, first_value(ifnull(ESUH_ACTVALUE,0)) over (partition by fpr.fact_purchaseid order by fpr.fact_purchaseid) AS amt_unplanentserv
         FROM ESUH e
, fact_purchase fpr
         WHERE fpr.dd_packagingno = ifnull(ESUH_PACKNO,0)
AND fpr.amt_unplanentserv <> ifnull(ESUH_ACTVALUE,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_unplanentserv  = src.amt_unplanentserv
 WHERE ifnull(fact.amt_unplanentserv ,-1) <> ifnull(src.amt_unplanentserv ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, first_value(ifnull(ESUH_COMMITMENT,0)) over (partition by fpr.fact_purchaseid order by fpr.fact_purchaseid) AS amt_expvalueoveralllimit
         FROM ESUH e
, fact_purchase fpr
         WHERE fpr.dd_packagingno = ifnull(ESUH_PACKNO,0)
AND fpr.amt_expvalueoveralllimit <> ifnull(ESUH_COMMITMENT,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_expvalueoveralllimit  = src.amt_expvalueoveralllimit
 WHERE ifnull(fact.amt_expvalueoveralllimit ,-1) <> ifnull(src.amt_expvalueoveralllimit ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct  fpr.fact_purchaseid, first_value(ifnull(ESUH_SUMLIMIT,0)) over (partition by fpr.fact_purchaseid order by fpr.fact_purchaseid) AS amt_overalllimit
         FROM ESUH e
, fact_purchase fpr
         WHERE fpr.dd_packagingno = ifnull(ESUH_PACKNO,0)
AND fpr.amt_overalllimit <> ifnull(ESUH_SUMLIMIT,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.amt_overalllimit  = src.amt_overalllimit
 WHERE ifnull(fact.amt_overalllimit ,-1) <> ifnull(src.amt_overalllimit ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct  fpr.fact_purchaseid, first_value(ifnull(ESUH_SUMNOLIM,'Not Set')) over (partition by fpr.fact_purchaseid order by fpr.fact_purchaseid)  AS dd_nolimit
         FROM ESUH e
, fact_purchase fpr
         WHERE fpr.dd_packagingno = ifnull(ESUH_PACKNO,0)
AND fpr.dd_nolimit <> ifnull(ESUH_SUMNOLIM,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_nolimit  = src.dd_nolimit
 WHERE ifnull(fact.dd_nolimit ,-1) <> ifnull(src.dd_nolimit ,-2);


MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, dc.dim_currencyid AS dim_servicelimitscurrencyid
         FROM ESUH e, dim_currency dc
, fact_purchase fpr
         WHERE dc.currencycode = ifnull(ESUH_WAERS,'Not Set')
AND fpr.dd_packagingno = ifnull(ESUH_PACKNO,0)
AND fpr.dim_servicelimitscurrencyid <> dc.dim_currencyid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_servicelimitscurrencyid  = src.dim_servicelimitscurrencyid
 WHERE ifnull(fact.dim_servicelimitscurrencyid ,-1) <> ifnull(src.dim_servicelimitscurrencyid ,-2);


MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, first_value(dd.dim_dateid) over (partition by fpr.fact_purchaseid order by dd.dim_dateid desc)  AS dim_dateiddeliverycreation
         FROM EKPV e, dim_date dd, ekko_ekpo_eket ek
, fact_purchase fpr
         WHERE dd.companycode = ifnull(ek.EKPO_BUKRS,'Not Set')
 AND fpr.dd_DocumentNo = ek.EKPO_EBELN
 AND fpr.dd_DocumentItemNo = ek.EKPO_EBELP
 AND fpr.dd_ScheduleNo = ek.EKET_ETENR
 AND dd.datevalue = ifnull(e.EKPV_LEDAT,'0001-01-01')
 AND dd.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
 AND fpr.dd_DocumentNo = EKPV_EBELN
 AND fpr.dd_DocumentItemNo = EKPV_EBELP
 AND fpr.dim_dateiddeliverycreation <> dd.dim_dateid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_dateiddeliverycreation  = src.dim_dateiddeliverycreation
 WHERE ifnull(fact.dim_dateiddeliverycreation ,-1) <> ifnull(src.dim_dateiddeliverycreation ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, ifnull(e.EKPV_PLIFZ,0) AS ct_planneddelivdays
         FROM EKPV e
, fact_purchase fpr
         WHERE fpr.dd_DocumentNo = e.EKPV_EBELN
AND fpr.dd_DocumentItemNo = e.EKPV_EBELP
AND ct_planneddelivdays <> ifnull(e.EKPV_PLIFZ,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_planneddelivdays  = src.ct_planneddelivdays
 WHERE ifnull(fact.ct_planneddelivdays,-1) <> ifnull(src.ct_planneddelivdays,-2);


MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, first_value(dr.dim_routeid) over (partition by fpr.fact_purchaseid order by fpr.fact_purchaseid) AS dim_routeid
         FROM EKPV e, dim_route dr
, fact_purchase fpr
         WHERE fpr.dd_DocumentNo = e.EKPV_EBELN
AND fpr.dd_DocumentItemNo = e.EKPV_EBELP
AND dr.routecode = ifnull(e.EKPV_ROUTE,'Not Set')
AND fpr.dim_routeid <> dr.dim_routeid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_routeid  = src.dim_routeid
 WHERE ifnull(fact.dim_routeid ,-1) <> ifnull(src.dim_routeid ,-2);


MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, dsrp.dim_shipreceivepointid AS dim_shipreceivepointid
         FROM EKPV e, dim_shipreceivepoint dsrp
, fact_purchase fpr
         WHERE fpr.dd_DocumentNo = e.EKPV_EBELN
AND fpr.dd_DocumentItemNo = e.EKPV_EBELP
AND dsrp.shipreceivepointcode = ifnull(e.EKPV_VSTEL,'Not Set')
AND fpr.dim_shipreceivepointid <> dsrp.dim_shipreceivepointid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_shipreceivepointid  = src.dim_shipreceivepointid
 WHERE ifnull(fact.dim_shipreceivepointid ,-1) <> ifnull(src.dim_shipreceivepointid ,-2);
/* Octavian: Every Angle Transition Addons */

/*Georgiana: Every Angle Transition Addons*/

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_EAN11,'Not Set') AS dd_intarticleno
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_intarticleno <> ifnull(EKPO_EAN11,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_intarticleno = src.dd_intarticleno
 WHERE ifnull(fact.dd_intarticleno,-1) <> ifnull(src.dd_intarticleno,-2);


MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKKO_FRGGR,'Not Set') AS dd_releasegroup
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_releasegroup <> ifnull(EKKO_FRGGR,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_releasegroup = src.dd_releasegroup
 WHERE ifnull(fact.dd_releasegroup,-1) <> ifnull(src.dd_releasegroup,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKKO_FRGSX,'Not Set') AS dd_releasestrategy
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_releasestrategy <> ifnull(EKKO_FRGSX,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_releasestrategy = src.dd_releasestrategy
 WHERE ifnull(fact.dd_releasestrategy,-1) <> ifnull(src.dd_releasestrategy,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_AFNAM,'Not Set') AS dd_requestername
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_requestername <> ifnull(EKPO_AFNAM,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_requestername = src.dd_requestername
 WHERE ifnull(fact.dd_requestername,-1) <> ifnull(src.dd_requestername,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_BEDNR,'Not Set') AS dd_requirementno
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_requirementno <> ifnull(EKPO_BEDNR,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_requirementno = src.dd_requirementno
 WHERE ifnull(fact.dd_requirementno,-1) <> ifnull(src.dd_requirementno,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_BSTAE,'Not Set') AS dd_controkeyconfirmation
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_controkeyconfirmation <> ifnull(EKPO_BSTAE,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_controkeyconfirmation = src.dd_controkeyconfirmation
 WHERE ifnull(fact.dd_controkeyconfirmation,-1) <> ifnull(src.dd_controkeyconfirmation,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_EGLKZ,'Not Set') AS dd_outwardindicator
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_outwardindicator <> ifnull(EKPO_EGLKZ,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_outwardindicator = src.dd_outwardindicator
 WHERE ifnull(fact.dd_outwardindicator,-1) <> ifnull(src.dd_outwardindicator,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_KONNR,'Not Set') AS dd_purchaseagreementno
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_purchaseagreementno <> ifnull(EKPO_KONNR,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_purchaseagreementno = src.dd_purchaseagreementno
 WHERE ifnull(fact.dd_purchaseagreementno,'Not Set') <> ifnull(src.dd_purchaseagreementno,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_KTMNG,0) AS ct_targetquantity
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.ct_targetquantity <> ifnull(EKPO_KTMNG,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_targetquantity = src.ct_targetquantity
 WHERE ifnull(fact.ct_targetquantity,-1) <> ifnull(src.ct_targetquantity,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_KTPNR,0) AS dd_purchaseagreementitemno
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_purchaseagreementitemno <> ifnull(EKPO_KTPNR,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_purchaseagreementitemno = src.dd_purchaseagreementitemno
 WHERE ifnull(fact.dd_purchaseagreementitemno,-1) <> ifnull(src.dd_purchaseagreementitemno,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_KZABS,'Not Set') AS dd_orderrequirement
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_orderrequirement <> ifnull(EKPO_KZABS,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_orderrequirement = src.dd_orderrequirement
 WHERE ifnull(fact.dd_orderrequirement,'Not Set') <> ifnull(src.dd_orderrequirement,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_REPOS,'Not Set') AS dd_invoicereceiptindicator
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_invoicereceiptindicator <> ifnull(EKPO_REPOS,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_invoicereceiptindicator = src.dd_invoicereceiptindicator
 WHERE ifnull(fact.dd_invoicereceiptindicator,'Not Set') <> ifnull(src.dd_invoicereceiptindicator,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKPO_STATU,'Not Set') AS dd_rfqstatus
         from ekko_ekpo_eket
, fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_rfqstatus <> ifnull(EKPO_STATU,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_rfqstatus = src.dd_rfqstatus
 WHERE ifnull(fact.dd_rfqstatus,'Not Set') <> ifnull(src.dd_rfqstatus,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, dv.dim_vendorid AS dim_vendorid_eina
         FROM EINA e, dim_vendor dv
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and dv.vendornumber = ifnull(e.LIFNR,'Not Set')
and fp.dim_vendorid_eina <> dv.dim_vendorid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_vendorid_eina  = src.dim_vendorid_eina
 WHERE ifnull(fact.dim_vendorid_eina ,-1) <> ifnull(src.dim_vendorid_eina ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.LOEKZ,'Not Set') AS dd_DeletionIndicator_eina
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.dd_DeletionIndicator_eina <> ifnull(e.LOEKZ,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_DeletionIndicator_eina  = src.dd_DeletionIndicator_eina
 WHERE ifnull(fact.dd_DeletionIndicator_eina,'Not Set') <> ifnull(src.dd_DeletionIndicator_eina,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_IDNLF,'Not Set') AS dd_vendormaterialno_eina
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.dd_vendormaterialno_eina <> ifnull(e.EINA_IDNLF,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_vendormaterialno_eina  = src.dd_vendormaterialno_eina
 WHERE ifnull(fact.dd_vendormaterialno_eina ,'Not Set') <> ifnull(src.dd_vendormaterialno_eina ,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, uom.dim_unitofmeasureid AS dim_unitofmeasureid_eina
         FROM EINA e, dim_unitofmeasure uom
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
 AND uom.UOM = EINA_LMEIN AND uom.RowIsCurrent = 1
and fp.dim_unitofmeasureid_eina <> uom.dim_unitofmeasureid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_unitofmeasureid_eina  = src.dim_unitofmeasureid_eina
 WHERE ifnull(fact.dim_unitofmeasureid_eina ,-1) <> ifnull(src.dim_unitofmeasureid_eina ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_LTSNR,'Not Set') AS dd_vebdorsubrange
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.dd_vebdorsubrange <> ifnull(e.EINA_LTSNR,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_vebdorsubrange  = src.dd_vebdorsubrange
 WHERE ifnull(fact.dd_vebdorsubrange ,'Not Set') <> ifnull(src.dd_vebdorsubrange ,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_MAHN1,0) AS ct_daysforfirstreminder
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.ct_daysforfirstreminder <> ifnull(e.EINA_MAHN1,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_daysforfirstreminder  = src.ct_daysforfirstreminder
 WHERE ifnull(fact.ct_daysforfirstreminder ,-1) <> ifnull(src.ct_daysforfirstreminder ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_MAHN2,0) AS ct_daysforsecondreminder
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.ct_daysforsecondreminder <> ifnull(e.EINA_MAHN2,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_daysforsecondreminder  = src.ct_daysforsecondreminder
 WHERE ifnull(fact.ct_daysforsecondreminder ,-1) <> ifnull(src.ct_daysforsecondreminder ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_MAHN3,0) AS ct_daysforthirdreminder
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.ct_daysforthirdreminder <> ifnull(e.EINA_MAHN3,0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_daysforthirdreminder  = src.ct_daysforthirdreminder
 WHERE ifnull(fact.ct_daysforthirdreminder ,-1) <> ifnull(src.ct_daysforthirdreminder ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_RELIF,'Not Set') AS dd_regularvendor
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.dd_regularvendor <> ifnull(e.EINA_RELIF,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_regularvendor  = src.dd_regularvendor;

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_TELF1,'Not Set') AS dd_vendortelphoneno
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.dd_vendortelphoneno <> ifnull(e.EINA_TELF1,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_vendortelphoneno  = src.dd_vendortelphoneno
 WHERE ifnull(fact.dd_vendortelphoneno ,-1) <> ifnull(src.dd_vendortelphoneno ,-2);

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_URZLA,'Not Set') AS dd_issuecountryofcertificate
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.dd_issuecountryofcertificate <> ifnull(e.EINA_URZLA,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_issuecountryofcertificate  = src.dd_issuecountryofcertificate
 WHERE ifnull(fact.dd_issuecountryofcertificate ,'Not Set') <> ifnull(src.dd_issuecountryofcertificate ,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.EINA_VERKF,'Not Set') AS dd_eventsalesresp
         FROM EINA e
, fact_purchase fp
         where fp.dd_inforecordno = ifnull(e.INFNR,'Not Set')
and fp.dd_eventsalesresp <> ifnull(e.EINA_VERKF,'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_eventsalesresp  = src.dd_eventsalesresp
 WHERE ifnull(fact.dd_eventsalesresp ,'Not Set') <> ifnull(src.dd_eventsalesresp ,'Not Set');


/*Georgiana End Changes*/

/* Octavian: Part of Every Angle: Add a dimension based on EORD table */
MERGE INTO fact_purchase fact
   USING (SELECT distinct f.fact_purchaseid, d.dim_purchasingsourcelistid AS dim_purchasingsourcelistid
         from dim_part dp, dim_purchasingsourcelist d
, fact_purchase f
         where f.dim_partid = dp.dim_partid
AND dp.partnumber = d.materialnumber
AND dp.plant = d.plant
AND f.dim_purchasingsourcelistid <> d.dim_purchasingsourcelistid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_purchasingsourcelistid  = src.dim_purchasingsourcelistid
 WHERE ifnull(fact.dim_purchasingsourcelistid ,-1) <> ifnull(src.dim_purchasingsourcelistid ,-2);
/* Octavian: End changes */

/* Octavian : Every Angle */
MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, vp.dim_vendorpurchasingid
   AS dim_vendorpurchasingid
         FROM dim_vendorpurchasing vp, ekko_ekpo_eket ek
, fact_purchase fpr
         WHERE
fpr.dd_DocumentNo = ek.EKPO_EBELN
AND fpr.dd_DocumentItemNo = ek.EKPO_EBELP
AND fpr.dd_ScheduleNo = ek.EKET_ETENR
AND vp.purchasingorg = ifnull(ek.EKKO_EKORG,'Not Set')
AND vp.vendornumber = ifnull(ek.EKKO_LIFNR,'Not Set')
AND fpr.dim_vendorpurchasingid <> vp.dim_vendorpurchasingid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_vendorpurchasingid  = src.dim_vendorpurchasingid
 WHERE ifnull(fact.dim_vendorpurchasingid ,-1) <> ifnull(src.dim_vendorpurchasingid ,-2);
/* Octavian : Every Angle */

/*Georgiana Every Angle Changes 05 Apt 2016*/

MERGE INTO fact_purchase fact
   USING (SELECT distinct fpr.fact_purchaseid, ms.dim_warehousenumberid
 AS dim_warehousenumberid
         from
ekko_ekpo_eket dm_di_ds,
T320 t,
dim_plant pl,
dim_company dc,
dim_warehousenumber ms
, fact_purchase fpr
         WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  t.T320_LGORT = EKPO_LGORT
 AND t.T320_WERKS = EKPO_WERKS
 AND t.T320_LGNUM = ms.warehousecode
 AND pl.companycode = dc.companycode
 AND fpr.dim_warehousenumberid <> ms.dim_warehousenumberid ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_warehousenumberid  = src.dim_warehousenumberid
 WHERE ifnull(fact.dim_warehousenumberid ,-1) <> ifnull(src.dim_warehousenumberid ,-2);

 DROP TABLE IF EXISTS tmp_for_upd_purchase;
CREATE TABLE tmp_for_upd_purchase AS
SELECT DISTINCT EKPO_EBELN,EKPO_EBELP,EKET_ETENR,t.MLGN_LHMG1
FROM fact_purchase fpr ,
ekko_ekpo_eket dm_di_ds,
MLGN t,
dim_part dp,
dim_company dc,
dim_warehousenumber ms
WHERE     dp.Plant = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND fpr.dim_warehousenumberid = ms.dim_warehousenumberid
 AND t.MLGN_LGNUM = ms.warehousecode
 AND t.MLGN_MATNR = dp.partnumber
 AND dp.dim_partid =fpr.dim_partid;

MERGE INTO fact_purchase fact
 USING (SELECT distinct fpr.fact_purchaseid, max(ifnull(t.MLGN_LHMG1,0)) AS ct_expecteddeliveryqty
 FROM tmp_for_upd_purchase t
 , fact_purchase fpr
 WHERE
 fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND fpr.ct_expecteddeliveryqty <> ifnull(t.MLGN_LHMG1,0)
group by fact_purchaseid) src
 ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
 SET fact.ct_expecteddeliveryqty = src.ct_expecteddeliveryqty
 WHERE ifnull(fact.ct_expecteddeliveryqty ,-1) <> ifnull(src.ct_expecteddeliveryqty ,-2);


 /*End Of Changes 05 Apr 2016*/
 /* Madalina Herghelegiu 26 May 2016 -add new column, Price Unit, calculated based on EKPO-PEINH. BI - 2965 */
MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(e.ekpo_peinh,0) AS ct_priceunit
         from ekko_ekpo_eket e
  , fact_purchase fp
         where fp.dd_DocumentNo = EKPO_EBELN
    and fp.dd_DocumentItemNo = EKPO_EBELP
    and fp.dd_ScheduleNo = EKET_ETENR
    and fp.ct_priceunit <> ifnull(e.ekpo_peinh, 0) ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_priceunit  = src.ct_priceunit
 WHERE ifnull(fact.ct_priceunit ,-1) <> ifnull(src.ct_priceunit ,-2);
/* END BI - 2965 */

/* 03 Jun 2016 Georgiana EA Changes adding  dd_numberofforeigntrade,dim_foreigntrade_decltoauthid columns according to BI-2720*/
MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(EKKO_EXNUM,'Not Set') AS dd_numberofforeigntrade
         FROM ekko_ekpo_eket
, fact_purchase fp
         WHERE
fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND dd_numberofforeigntrade <> ifnull(EKKO_EXNUM, 'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_numberofforeigntrade  = src.dd_numberofforeigntrade
 WHERE ifnull(fact.dd_numberofforeigntrade,'Not Set') <> ifnull(src.dd_numberofforeigntrade,'Not Set');

MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, fg.dim_foreigntrade_decltoauthid
 AS dim_foreigntrade_decltoauthid
         FROM EIPO e,dim_foreigntrade_decltoauth fg
, fact_purchase fp
         WHERE  dd_numberofforeigntrade = ifnull(EIPO_EXNUM, 'Not Set')
and fp.dd_DocumentItemNo = ifnull(EIPO_EXPOS, 0)
and exclusioninclusionindicator = EIPO_SEGAL ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_foreigntrade_decltoauthid  = src.dim_foreigntrade_decltoauthid
 WHERE ifnull(fact.dim_foreigntrade_decltoauthid ,-1) <> ifnull(src.dim_foreigntrade_decltoauthid ,-2);
/* 03 Jun 2016 End of Changes */

/* 06 Jun 2016 Georgiana EA Changes adding dd_executionstatusitem column according to BI-3117 */

drop table if exists tmp_for_schedule;
create table tmp_for_schedule as
select fp.dd_DocumentNo , fp.dd_DocumentItemNo, group_concat(distinct dd_executionstatusschedule ORDER BY dd_executionstatusschedule asc SEPARATOR ',') dd_schedulestatus
from fact_purchase fp
group by  fp.dd_DocumentNo , fp.dd_DocumentItemNo;

drop table if exists tmp_for_upd_statusitem;
create table tmp_for_upd_statusitem as
select distinct fp.dd_DocumentNo , fp.dd_DocumentItemNo,
case when dd_schedulestatus like '%Partially%open%' then 'Partially Open'
else (case when dd_schedulestatus like '%Closed%' then (case when dd_schedulestatus like '%Open%' then 'Partially Open' else 'Closed' end)
         else case when dd_schedulestatus like '%Open%' then 'Open' else 'Cancelled' end end)
         end as dd_schedulestatus1
         from tmp_for_schedule fp;

 MERGE INTO fact_purchase fact
   USING (SELECT distinct fp.fact_purchaseid, ifnull(t.dd_schedulestatus1, 'Not Set') AS dd_executionstatusitem
         from tmp_for_upd_statusitem t
 , fact_purchase fp
         where fp.dd_DocumentNo = t.dd_DocumentNo
 and fp.dd_DocumentItemNo = t.dd_DocumentItemNo
 and fp.dd_executionstatusitem <> ifnull(t.dd_schedulestatus1, 'Not Set') ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_executionstatusitem  = src.dd_executionstatusitem
 WHERE ifnull(fact.dd_executionstatusitem ,-1) <> ifnull(src.dd_executionstatusitem ,-2);

 /* 07 Jun 2016 Georgiana EA Changes adding dd_executionstatusheader column according to BI-3130*/
drop table if exists tmp_for_header;
create table tmp_for_header as
select fp.dd_DocumentNo , group_concat(distinct dd_executionstatusitem ORDER BY dd_executionstatusitem asc SEPARATOR ',') AS dd_headerstatus
from fact_purchase fp
group by  fp.dd_DocumentNo;

drop table if exists tmp_for_upd_statusheader;
create table tmp_for_upd_statusheader as
select distinct fp.dd_DocumentNo ,
case when dd_headerstatus like '%Partially%Open%' then 'Partially Open'
else (case when dd_headerstatus like '%Closed%' then (case when dd_headerstatus like '%Open%' then 'Partially Open' else 'Closed' end)
         else case when dd_headerstatus like '%Open%' then 'Open' else 'Cancelled' end end)
         end as dd_headerstatus1
         from tmp_for_header fp;

 update fact_purchase fp
 set fp.dd_executionstatusheader = ifnull(t.dd_headerstatus1, 'Not Set')
 from tmp_for_upd_statusheader t, fact_purchase fp
 where fp.dd_DocumentNo = t.dd_DocumentNo
 and fp.dd_executionstatusheader <> ifnull(t.dd_headerstatus1, 'Not Set');
 /*07 Jun End of EA Changes*/

/* Madalina 25 Jul 2016 - Add three new columns - Date of goods receipt, Date of invoice receipt and Date of goods issue BI-2939 */
merge into fact_purchase fp
using (select distinct fp.fact_purchaseid,first_value(d.dim_dateid) over (partition by fp.fact_purchaseid order by fp.fact_purchaseid) as dim_dateid
from dim_date d, EKBE_FPURCHASE ef, fact_materialmovement fm, dim_company dc,fact_purchase fp
where
    fm.dd_MaterialDocYear = ifnull(ef.EKBE_GJAHR, 0)    /* MKPF_MJAHR = EKBE_GJAHR */
    AND fp.dd_DocumentNo = fm.dd_DocumentNo
  AND fp.dd_DocumentItemNo = fm.dd_DocumentItemNo
    AND fp.dd_DocumentNo = ifnull(ef.EKBE_EBELN, 'Not Set')
    AND fp.dd_DocumentItemNo = ifnull(ef.EKBE_EBELP, 'Not Set')
    AND EKBE_VGABE = 1
    AND d.datevalue = ef.EKBE_BUDAT
    AND d.companycode = dc.companycode
  AND d.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
    AND fp.dim_companyid = dc.dim_companyid) t
on fp.fact_purchaseid=t.fact_purchaseid
when matched then update set  fp.dim_dategoodsreceipt = t.dim_dateid
WHERE   fp.dim_dategoodsreceipt <> t.dim_dateid;



merge into fact_purchase fp
using (select distinct fp.fact_purchaseid,first_value(d.dim_dateid) over (partition by fp.fact_purchaseid order by fp.fact_purchaseid) as dim_dateid
from dim_date d, EKBE_FPURCHASE ef, fact_materialmovement fm, dim_company dc,fact_purchase fp
where
    fm.dd_MaterialDocYear = ifnull(ef.EKBE_GJAHR, 0)    /* MKPF_MJAHR = EKBE_GJAHR */
    AND fp.dd_DocumentNo = fm.dd_DocumentNo
  AND fp.dd_DocumentItemNo = fm.dd_DocumentItemNo
    AND fp.dd_DocumentNo = ifnull(ef.EKBE_EBELN, 'Not Set')
    AND fp.dd_DocumentItemNo = ifnull(ef.EKBE_EBELP, 'Not Set')
    AND EKBE_VGABE = 2
    AND d.datevalue = ef.EKBE_BUDAT
    AND d.companycode = dc.companycode
  AND d.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
    AND fp.dim_companyid = dc.dim_companyid) t
on fp.fact_purchaseid=t.fact_purchaseid
when matched then update set  fp.dim_dateinvoicereceipt = t.dim_dateid
WHERE   fp.dim_dateinvoicereceipt <> t.dim_dateid;


merge into fact_purchase fp
using (select distinct fp.fact_purchaseid,first_value(d.dim_dateid) over (partition by fp.fact_purchaseid order by fp.fact_purchaseid) as dim_dateid
from dim_date d, EKBE_FPURCHASE ef, fact_materialmovement fm, dim_company dc,fact_purchase fp
where
    fm.dd_MaterialDocYear = ifnull(ef.EKBE_GJAHR, 0)    /* MKPF_MJAHR = EKBE_GJAHR */
    AND fp.dd_DocumentNo = fm.dd_DocumentNo
  AND fp.dd_DocumentItemNo = fm.dd_DocumentItemNo
    AND fp.dd_DocumentNo = ifnull(ef.EKBE_EBELN, 'Not Set')
    AND fp.dd_DocumentItemNo = ifnull(ef.EKBE_EBELP, 'Not Set')
    AND EKBE_VGABE = 6
    AND d.datevalue = ef.EKBE_BUDAT
    AND d.companycode = dc.companycode
  AND d.plantcode_factory = 'Not Set'  /* plantcode_factory additional condition- BI-5085 */
    AND fp.dim_companyid = dc.dim_companyid) t
on fp.fact_purchaseid=t.fact_purchaseid
when matched then update set  fp.dim_dategoodsissue = t.dim_dateid
WHERE   fp.dim_dategoodsissue <> t.dim_dateid;

/* END 25 Jul 2016 */

/* Madalina 09 Sep 2016 - Add Order Due Date field - BI-3860 */
drop table if exists tmp_upd_OrderDueDate;
create table tmp_upd_OrderDueDate as
select distinct fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_ScheduleNo,
  (case when fp.dd_confirmationisrelevantind = 'X' then fp.dim_dateidvendorconfirmation
    else fp.dim_dateiddelivery
  end) as dim_OrderDueDate
from fact_purchase fp, dim_date d1, dim_date d2
where d1.dim_dateid = fp.dim_dateiddelivery
  and d2.dim_dateid = fp.dim_dateidvendorconfirmation;

update fact_purchase fp
set fp.dim_OrderDueDate = ifnull(tmp.dim_OrderDueDate, 1)
from fact_purchase fp, tmp_upd_OrderDueDate tmp
where
  fp.dd_DocumentNo = tmp.dd_DocumentNo
  and fp.dd_DocumentItemNo = tmp.dd_DocumentItemNo
  and fp.dd_ScheduleNo = tmp.dd_ScheduleNo
  and fp.dim_OrderDueDate <> ifnull(tmp.dim_OrderDueDate, 1);
/* END BI-3860 */

/* Madalina 15 Sep 2016- Add Scheduled Quantity, based on EKET-MENGE- BI-4027 */
UPDATE fact_purchase fpr
SET ct_ScheduledQuantity = ifnull(EKET_MENGE, 0)
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
AND ct_ScheduledQuantity <> ifnull(EKET_MENGE, 0);
/* END BI-4027 */
DROP TABLE IF EXISTS tmp_var_purchasing_fact;
drop table if exists tmp_dim_pc_purchasing_fact;
drop table if exists tmp_ekko_ekpo_ekes_purchasing_fact;
DROP TABLE IF EXISTS tmp_pur_fact_konv  ;
DROP TABLE IF EXISTS tmp_pf_mnb;
DROP TABLE IF EXISTS tmp_pf_mnb1;
DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_ekkn_pre;
DROP TABLE IF EXISTS tmp_latest_ekko_ekpo_ekkn;


/* Alin 23 Sep 2016- Add denomninator(EKPO_UMREN) and numerator(EKPO_UMREZ) - BI-4094*/
UPDATE fact_purchase fpr
SET dd_numeratorekpo = ifnull(EKPO_UMREZ, 0)
FROM ekko_ekpo_eket dm_di_ds, fact_purchase fpr
WHERE fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_numeratorekpo <> ifnull(EKPO_UMREZ, 0);

UPDATE fact_purchase fpr
SET dd_denominatorekpo = ifnull(EKPO_UMREN, 0)
FROM ekko_ekpo_eket dm_di_ds,fact_purchase fpr
WHERE fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND dd_denominatorekpo <> ifnull(EKPO_UMREN, 0);

/* Madalina 4 Oct 2016 - Add Invoiced Quantity (EKBE) and Invoiced Value (EKBE) - BI-4020 */
drop table if exists tmp_upd_invoiceQtyValue;
create table tmp_upd_invoiceQtyValue as
select fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleNo,
    sum(ifnull(EKBE_MENGE * (case when EKBE_SHKZG = 'H' then -1 else 1 end), 0)) as EKBE_MENGE,
    sum(ifnull(EKBE_DMBTR * (case when EKBE_SHKZG = 'H' then -1 else 1 end), 0)) as EKBE_DMBTR,
    sum(ifnull(EKBE_DMBTR, 0)) as EKBE_DMBTR2
from EKBE e, fact_purchase fp
where fp.dd_DocumentNo = ifnull(e.EKBE_EBELN, 'Not Set')
    and fp.dd_DocumentItemNo = ifnull(e.EKBE_EBELP, 'Not Set')
  and ifnull(EKBE_BEWTP, 'Not Set') = 'Q'
group by fp.dd_DocumentNo, fp.dd_DocumentItemNo, fp.dd_scheduleNo;

update fact_purchase fp
set ct_invoicedQuantityEkbe = ifnull(EKBE_MENGE, 0)
from tmp_upd_invoiceQtyValue tmp, fact_purchase fp
where fp.dd_DocumentNo = tmp.dd_DocumentNo
    and fp.dd_DocumentItemNo = tmp.dd_DocumentItemNo
  and fp.dd_scheduleNo = tmp.dd_scheduleNo
  and ct_invoicedQuantityEkbe <> ifnull(EKBE_MENGE, 0);

update fact_purchase fp
set ct_invoicedValueEkbe = ifnull(EKBE_DMBTR, 0)
from tmp_upd_invoiceQtyValue tmp, fact_purchase fp
where fp.dd_DocumentNo = tmp.dd_DocumentNo
    and fp.dd_DocumentItemNo = tmp.dd_DocumentItemNo
  and fp.dd_scheduleNo = tmp.dd_scheduleNo
  and ct_invoicedValueEkbe <> ifnull(EKBE_DMBTR, 0);


/*Alin APP-7456 start*/
  merge into fact_purchase fp
using
(
select distinct fact_purchaseid,fp.dd_DocumentNo,fp.dd_DocumentItemNo,fp.dd_scheduleNo,
--avg(ifnull(EKBE_DMBTR2, 0)) over (partition by fp.dd_DocumentNo order by fp.dd_DocumentNo) as amt_invoicenet
avg(ifnull(EKBE_DMBTR2, 0))  as amt_invoicenet
from tmp_upd_invoiceQtyValue tmp, fact_purchase fp
where fp.dd_DocumentNo = tmp.dd_DocumentNo
    and fp.dd_DocumentItemNo = tmp.dd_DocumentItemNo
  and fp.dd_scheduleNo = tmp.dd_scheduleNo
group by fact_purchaseid,fp.dd_DocumentNo,fp.dd_DocumentItemNo,fp.dd_scheduleNo
)t
on t.fact_purchaseid = fp.fact_purchaseid
when matched then update
set fp.amt_invoicenet = t.amt_invoicenet
where fp.amt_invoicenet <> t.amt_invoicenet;

/*Alin APP-7456 end*/

/* Madalina - 11 Jan 2017 - Add GL Account description - BI-5214 */
update fact_purchase fp
set fp.Dim_ChartOfAccountsId = dca.Dim_ChartOfAccountsId
from fact_purchase fp, Dim_ChartOfAccounts dca
where fp.dd_glaccountno = dca.GLAccountNumber
  and dca.CharOfAccounts = 'ICOA'
  and fp.Dim_ChartOfAccountsId <> dca.Dim_ChartOfAccountsId;

/*16 Jun 2017 Georgiana changes according to APP-6505*/

drop table if exists tmp_forfirstpurchaseorderqty;
create table tmp_forfirstpurchaseorderqty as
select distinct po.dd_documentno,po.dd_documentitemno,first_value(c.CDPOS_VALUE_OLD) over (partition by po.dd_documentno,po.dd_documentitemno order by c.CDPOS_CHANGENR asc) as firstpurchaseorderqty
from fact_purchase po,CDPOS_PO c
where
po.dd_documentno=c.CDPOS_OBJECTID
and po.dd_documentitemno=right(c.CDPOS_TABKEY,2)
and c.CDPOS_FNAME='MENGE'
and c.CDPOS_TABNAME='EKPO';

update fact_purchase po
set ct_firstpurchaseorderqty=ifnull(firstpurchaseorderqty,0)
from fact_purchase po,  tmp_forfirstpurchaseorderqty t
where po.dd_documentno=t.dd_documentno
and po.dd_documentitemno=t.dd_documentitemno
and ct_firstpurchaseorderqty<>ifnull(firstpurchaseorderqty,0);


update fact_purchase po
set ct_firstpurchaseorderqty=ifnull(EKPO_MENGE,0)
from fact_purchase po,  EKKO_EKPO_EKET
where  po.dd_DocumentNo = EKPO_EBELN
 AND po.dd_DocumentItemNo = EKPO_EBELP
AND po.dd_ScheduleNo = EKET_ETENR
and ct_firstpurchaseorderqty=0;

drop table if exists tmp_forfirstpurchaseorderdelivdate;
create table tmp_forfirstpurchaseorderdelivdate as
select distinct po.dd_documentno,po.dd_documentitemno, po.dd_ScheduleNo,to_date(case when first_value(c.CDPOS_VALUE_OLD) over (partition by po.dd_documentno,po.dd_documentitemno, po.dd_ScheduleNo order by c.CDPOS_CHANGENR asc) ='00000000' then '00010101' else first_value(c.CDPOS_VALUE_OLD) over (partition by po.dd_documentno,po.dd_documentitemno, po.dd_ScheduleNo order by c.CDPOS_CHANGENR asc) end ,'YYYYMMDD') as firstpurchaseorderdelivdate
from fact_purchase po,CDPOS_PO c
where
po.dd_documentno=c.CDPOS_OBJECTID
and po.dd_documentitemno=left(right(c.CDPOS_TABKEY,6),2)
and po.dd_ScheduleNo =right(c.CDPOS_TABKEY,1)
and c.CDPOS_FNAME='EINDT'
and c.CDPOS_TABNAME='EKET';


update fact_purchase po
set dim_firstpurchaseorderdelivdateid=ifnull(dt.dim_dateid,1)
from fact_purchase po,tmp_forfirstpurchaseorderdelivdate t, dim_date dt,
dim_plant pl
where po.dd_documentno=t.dd_documentno
and po.dd_documentitemno=t.dd_documentitemno
and po.dd_ScheduleNo =t.dd_ScheduleNo
and po.Dim_PlantidOrdering=pl.dim_plantid
and pl.companycode=dt.companycode
and pl.plantcode=dt.plantcode_factory
and dt.datevalue=ifnull(firstpurchaseorderdelivdate,'0001-01-01')
and dim_firstpurchaseorderdelivdateid<>ifnull(dt.dim_dateid,1);

UPDATE fact_purchase po
SET dim_firstpurchaseorderdelivdateid = dc0.Dim_Dateid
FROM ekko_ekpo_eket dm_di_ds, dim_plant pl, dim_company dc,Dim_Date dc0, fact_purchase fpr
WHERE     pl.PlantCode = EKPO_WERKS
 AND dc.companycode = EKPO_BUKRS
 AND fpr.dd_DocumentNo = EKPO_EBELN
 AND fpr.dd_DocumentItemNo = EKPO_EBELP
 AND fpr.dd_ScheduleNo = EKET_ETENR
 AND  dc0.DateValue = EKET_EINDT AND dc0.CompanyCode = EKPO_BUKRS
 AND dc0.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
AND fpr.dim_firstpurchaseorderdelivdateid =1;

/*keep unique values of Calendar Week, per each Plant*/
drop table if exists tmp_dimDateCalendarMonth_Purch;
create table tmp_dimDateCalendarMonth_Purch as
select * from (
    select dim_dateid,MonthYear, plantcode_factory, datevalue, row_number() over (partition by MonthYear, plantcode_factory order by dayofmonth asc) rn from dim_date) t
    where rn = 1
    order by 1;


/* insert into Purchasing the plant - date combinations for JAN 2004 - today */
drop table if exists brinit_date_plants_purch;
create table brinit_date_plants_purch as
select distinct dd.dim_dateid as dim_dateidstatdelivery,
    dp.dim_plantid as dim_plantid
/*from dim_date dd*/
from tmp_dimDateCalendarMonth_Purch dd
inner join dim_plant dp on dd.plantcode_factory = dp.plantcode
where plantcode_factory in ( select plantcode from newiteminstockbrinit)
and monthyear between 'Jan 2004' and (select distinct monthyear from dim_date where datevalue = current_date)
and not exists ( select 1
        from fact_purchase pos
        inner join dim_date od
        on pos.dim_dateidstatdelivery = od.dim_dateid
        where dd.datevalue = od.datevalue
        and dd.plantcode_factory = od.plantcode_factory);

Drop table if exists max_holder_purch;
Create table max_holder_purch
as Select ifnull(max(fact_purchaseid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_purchase;

insert into fact_purchase
(fact_purchaseid, dd_DocumentNo, dd_DocumentItemNo, dd_ScheduleNo, dim_dateidstatdelivery, dim_plantidordering)
select
  max_holder_purch.maxid + row_number() over(order by '') as fact_purchaseid,
  'Not Available' dd_DocumentNo,
  0 as dd_DocumentItemNo,
  0 as dd_ScheduleNo,
  dim_dateidstatdelivery,
  dim_plantid
from brinit_date_plants_purch, max_holder_purch;


/* delete duplicates of Calendar MONTH from Purch*/
merge into fact_purchase fil using
(select distinct fact_purchaseid
   from fact_purchase fil
    inner join dim_date trd on fil.dim_dateidstatdelivery = trd.dim_dateid
  where not exists
    ( select 1 from tmp_dimDateCalendarMonth_Purch tmp
      where trd.dim_dateid = tmp.dim_dateid)
        and dd_DocumentNo = 'Not Available') del
on fil.fact_purchaseid = del.fact_purchaseid
when matched then delete;

/*19 Apr 2018 Georgiana Changes according to APP-8617 creating a default logic based on Ordering Plant.Plant Title and Statistics-Rel Dlvr Date.Date
additional filters for this will be: equals with Year to Date, before Yesterday and not equal with Current Month*/

/*05 June 2018 Georgiana - revert default logic changes*/
/*drop table if exists  tmp_for_default_external
create table tmp_for_default_external as
select distinct calendarmonthid,planttitle_merck,DD_EXTERNALIOTIF,dim_plantidordering,dim_dateidstatdelivery 
from fact_purchase f, dim_date d, dim_plant pl
where dim_dateidstatdelivery=dim_dateid
and dim_plantidordering=dim_plantid
and year(datevalue)=year(current_date)
and calendarmonthid < concat( year(current_date), case when length(month(current_date)-1)=1 then concat('0', month(current_date)-1) else month(current_Date)-1 end)
and DD_EXTERNALIOTIF <>'No comments'
order by 2,1*/


/*merge into fact_purchase f
using (select distinct f.fact_purchaseid,t.DD_EXTERNALIOTIF
from fact_purchase f,tmp_for_default_external t,dim_date d1, dim_date d2
where f.dim_plantidordering = t.dim_plantidordering
and f.dim_dateidstatdelivery = d1.dim_dateid
and t.dim_dateidstatdelivery = d2.dim_Dateid
and d1.calendarmonthid=d2.calendarmonthid
and d1.plantcode_factory=d2.plantcode_factory
and d1.companycode=d2.companycode
and f.DD_EXTERNALIOTIF='No comments') t
on f.fact_purchaseid=t.fact_purchaseid
when matched then update set f.DD_EXTERNALIOTIF=t.DD_EXTERNALIOTIF*/


/*drop table if exists  tmp_for_default_internal
create table tmp_for_default_internal as
select distinct calendarmonthid,planttitle_merck,DD_internalIOTIF,dim_plantidordering,dim_dateidstatdelivery from fact_purchase f, dim_date d, dim_plant pl
where dim_dateidstatdelivery=dim_dateid
and dim_plantidordering=dim_plantid
and year(datevalue)=year(current_date)
and calendarmonthid = concat( year(current_date), case when length(month(current_date)-1)=1 then concat('0', month(current_date)-1) else month(current_Date)-1 end)
and DD_INTERNALIOTIF <>'No comments'
order by 2,1*/

/*merge into fact_purchase f
using (select distinct f.fact_purchaseid,t.DD_INTERNALIOTIF
from fact_purchase f,tmp_for_default_internal t,dim_date d1, dim_date d2
where f.dim_plantidordering = t.dim_plantidordering
and f.dim_dateidstatdelivery = d1.dim_dateid
and t.dim_dateidstatdelivery = d2.dim_Dateid
and d1.calendarmonthid=d2.calendarmonthid
and d1.plantcode_factory=d2.plantcode_factory
and d1.companycode=d2.companycode
and f.DD_INTERNALIOTIF='No comments') t
on f.fact_purchaseid=t.fact_purchaseid
when matched then update set f.DD_INTERNALIOTIF=t.DD_INTERNALIOTIF*/

/*19 Apr 2018 End of changes*/

drop table if exists  tmp_for_default_external;
drop table if exists  tmp_for_default_internal;
