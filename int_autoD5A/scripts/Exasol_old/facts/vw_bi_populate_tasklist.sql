/* Georgiana / Octavian : SA added Part of the EAR transition */

delete from NUMBER_FOUNTAIN where table_name = 'fact_tasklist';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_tasklist',ifnull(max(f.fact_tasklistid ), 
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_tasklist f;

DROP TABLE IF EXISTS tmp_var_tasklist_fact;
CREATE table tmp_var_tasklist_fact AS
Select  CONVERT(varchar(3),ifnull((SELECT property_value
			FROM systemproperty
			WHERE property = 'customer.global.currency'),
		'USD')) pGlobalCurrency;




INSERT INTO fact_tasklist(fact_tasklistid,
									dd_tltype,
									dd_keytlgroup,
									dd_notlnode,
									dd_internalcounter_plpo,
									dd_groupcounter,
									dd_internalcounter_plko,
									dd_CharacteristicType,
									dd_inspCharacteristicNo,
									dd_internalcounter_plmk,
									dd_sequence,
									dd_internalcounter_plas,
									dd_materialno,
									dd_plantcode,
									dd_counterforaddcriteria,
									dd_internalcounter_mapl,
									dd_workcenterobjecttype,
									dd_workcenterobjectid
									)							
SELECT (SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_tasklist') + row_number() over(order by '') fact_tasklist,
		ifnull(po.PLPO_PLNTY,'Not Set') as dd_tltype,
		ifnull(po.PLPO_PLNNR,'Not Set') as dd_keytlgroup,
		ifnull(po.PLPO_PLNKN,0) as dd_notlnode,
		ifnull(po.PLPO_ZAEHL,0) as dd_internalcounter_plpo,
		ifnull(ko.PLKO_PLNAL,'Not Set') as dd_groupcounter,
		ifnull(ko.PLKO_ZAEHL,0) as dd_internalcounter_plko,
		ifnull(mk.PLMK_KZEINSTELL, 'Not Set') dd_CharacteristicType,
		ifnull(mk.PLMK_MERKNR,0) dd_inspCharacteristicNo,
		ifnull(mk.PLMK_ZAEHL,0) as dd_internalcounter_plmk,
		ifnull(ps.PLAS_PLNFL,'Not Set') as dd_sequence,
		ifnull(ps.PLAS_ZAEHL,0) as dd_internalcounter_plas,
		ifnull(mp.MAPL_MATNR, 'Not Set') as dd_materialno,
		ifnull(mp.MAPL_WERKS, 'Not Set') as dd_PlantCode,
		ifnull(mp.MAPL_ZKRIZ, 0) as dd_counterforaddcriteria,
		ifnull(mp.MAPL_ZAEHL, 0) as dd_internalcounter_mapl,
		ifnull(c.CRHD_OBJTY,'Not Set') as dd_workcenterobjecttype, 
		ifnull(c.CRHD_OBJID,0) as dd_workcenterobjectid
FROM 
MAPL mp
INNER JOIN PLKO ko ON mp.MAPL_PLNTY = ko.PLKO_PLNTY and mp.MAPL_PLNNR = ko.PLKO_PLNNR and mp.MAPL_PLNAL = ko.PLKO_PLNAL
INNER JOIN PLAS ps ON ps.PLAS_PLNTY = ko.PLKO_PLNTY and ps.PLAS_PLNNR = ko.PLKO_PLNNR and ps.PLAS_PLNAL = ko.PLKO_PLNAL
RIGHT JOIN PLPO po ON po.PLPO_PLNTY = ps.PLAS_PLNTY and po.PLPO_PLNNR = ps.PLAS_PLNNR and po.PLPO_PLNKN = ps.PLAS_PLNKN
LEFT JOIN PLMK mk ON mk.PLMK_PLNTY = po.PLPO_PLNTY and mk.PLMK_PLNNR = po.PLPO_PLNNR and mk.PLMK_PLNKN = po.PLPO_PLNKN
LEFT JOIN CRHD c ON ifnull(c.CRHD_OBJID,0) = ifnull(po.PLPO_ARBID,0)
WHERE NOT EXISTS (SELECT 1
FROM fact_tasklist tl
WHERE ifnull(po.PLPO_PLNTY,'Not Set') = tl.dd_tltype AND 
ifnull(po.PLPO_PLNNR,'Not Set') = tl.dd_keytlgroup AND 
ifnull(po.PLPO_PLNKN,0) = tl.dd_notlnode AND 
ifnull(po.PLPO_ZAEHL,0) = tl.dd_internalcounter_plpo AND 
ifnull(ko.PLKO_PLNAL,'Not Set') = tl.dd_groupcounter AND 
ifnull(ko.PLKO_ZAEHL,0) = tl.dd_internalcounter_plko AND 
ifnull(mk.PLMK_KZEINSTELL, 'Not Set') = tl.dd_CharacteristicType AND 
ifnull(mk.PLMK_MERKNR,0) = tl.dd_inspCharacteristicNo AND 
ifnull(mk.PLMK_ZAEHL,0) = tl.dd_internalcounter_plmk AND 
ifnull(ps.PLAS_PLNFL,'Not Set') = tl.dd_sequence AND 
ifnull(ps.PLAS_ZAEHL,0) = tl.dd_internalcounter_plas AND 
ifnull(mp.MAPL_MATNR, 'Not Set') = tl.dd_materialno AND 
ifnull(mp.MAPL_WERKS, 'Not Set') = tl.dd_PlantCode AND 
ifnull(mp.MAPL_ZKRIZ, 0) = tl.dd_counterforaddcriteria AND 
ifnull(mp.MAPL_ZAEHL, 0) = tl.dd_internalcounter_mapl AND 
ifnull(c.CRHD_OBJTY,'Not Set') = tl.dd_workcenterobjecttype AND 
ifnull(c.CRHD_OBJID,0) = tl.dd_workcenterobjectid);


UPDATE fact_tasklist tl
SET ct_basequantity = ifnull(po.PLPO_BMSCH,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND ct_basequantity <> ifnull(po.PLPO_BMSCH,0);

UPDATE fact_tasklist tl
SET dd_operationshorttext = ifnull(po.PLPO_LTXA1,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND dd_operationshorttext <> ifnull(po.PLPO_LTXA1,'Not Set');
	
UPDATE fact_tasklist tl
SET amt_standardvalue1 = ifnull(po.PLPO_VGW01,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND amt_standardvalue1 <> ifnull(po.PLPO_VGW01,0);
	
UPDATE fact_tasklist tl
SET amt_standardvalue2 = ifnull(po.PLPO_VGW02,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND amt_standardvalue2 <> ifnull(po.PLPO_VGW02,0);
	
UPDATE fact_tasklist tl
SET amt_standardvalue3 = ifnull(po.PLPO_VGW03,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND amt_standardvalue3 <> ifnull(po.PLPO_VGW03,0);
	
	
UPDATE fact_tasklist tl
SET tl.amt_standardvalue5 = ifnull(po.PLPO_VGW05,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.amt_standardvalue5 <> ifnull(po.PLPO_VGW05,0);
	
UPDATE fact_tasklist tl
SET tl.dd_operation = ifnull(po.PLPO_VORNR,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_operation <> ifnull(po.PLPO_VORNR,'Not Set');
	
MERGE INTO fact_tasklist fact
	USING ( SELECT tl.fact_tasklistid, dt.dim_dateid
			FROM fact_tasklist tl 
			INNER JOIN PLPO po
			ON tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
			AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
			AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
			AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
			INNER JOIN dim_date dt
			ON dt.datevalue=po.PLPO_ANDAT
			AND dt.companycode = ifnull(po.PLPO_BUKRS, 'Not Set')
			and dt.plantcode_factory='Not Set'
			AND tl.dim_dateidrecordcreated <> dt.dim_dateid) src
ON fact.fact_tasklistid = src.fact_tasklistid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidrecordcreated = src.dim_dateid;

MERGE INTO fact_tasklist fact
	USING ( SELECT tl.fact_tasklistid, dt.dim_dateid
			FROM fact_tasklist tl 
			INNER JOIN PLPO po
			ON tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
			AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
			AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
			AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
			INNER JOIN dim_date dt
			ON dt.datevalue=po.PLPO_AEDAT
			AND dt.companycode = ifnull(po.PLPO_BUKRS, 'Not Set')
			AND dt.plantcode_factory= 'Not Set'
			AND tl.dim_dateidchangedon <> dt.dim_dateid) src
ON fact.fact_tasklistid = src.fact_tasklistid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidchangedon = src.dim_dateid;
	
UPDATE fact_tasklist tl
SET tl.dd_deletionflag = ifnull(po.PLKO_DELKZ,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po, fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.dd_deletionflag <> ifnull(po.PLKO_DELKZ,'Not Set');
	
	
UPDATE fact_tasklist tl
SET tl.dd_status = ifnull(po.PLKO_STATU,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.dd_status <> ifnull(po.PLKO_STATU,'Not Set');
	

MERGE INTO fact_tasklist fact
	USING ( SELECT tl.fact_tasklistid, dt.dim_dateid
			FROM fact_tasklist tl 
			INNER JOIN PLKO po
			ON  tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
			AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
  			AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
			AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
			INNER JOIN dim_plant dp
		    ON dp.plantcode = ifnull(po.PLKO_WERKS, 'Not Set')
			INNER JOIN dim_date dt
			ON dt.datevalue=po.PLKO_DATUV
			AND dt.companyCode= dp.companycode and dp.plantcode=dt.plantcode_factory
			AND tl.dim_dateidvalidfrom <> dt.dim_dateid) src
ON fact.fact_tasklistid = src.fact_tasklistid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidvalidfrom = src.dim_dateid;


UPDATE fact_tasklist tl
SET tl.dd_shorttext = ifnull(po.PLKO_KTEXT,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po, fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.dd_shorttext <> ifnull(po.PLKO_KTEXT,'Not Set');


UPDATE fact_tasklist tl
SET tl.dd_deletionindicator = ifnull(po.PLKO_LOEKZ,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.dd_deletionindicator <> ifnull(po.PLKO_LOEKZ,'Not Set');	
	

UPDATE fact_tasklist tl
SET tl.dd_plannergroup = ifnull(po.PLKO_VAGRP,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.dd_plannergroup <> ifnull(po.PLKO_VAGRP,'Not Set');	
	

UPDATE fact_tasklist tl
SET tl.dim_tasklistusageid = u.dim_tasklistusageid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_tasklist tl,PLKO po, dim_tasklistusage u
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND u.usagecode = Ifnull(PLKO_VERWE, 'Not Set')
	AND tl.dim_tasklistusageid <> u.dim_tasklistusageid;	
	
		
UPDATE fact_tasklist tl
SET tl.dim_plantid = dp.dim_plantid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_tasklist tl,PLKO po, dim_plant dp
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND dp.plantcode = Ifnull(PLKO_WERKS, 'Not Set')
	AND tl.dim_plantid <> dp.dim_plantid;
	
UPDATE fact_tasklist tl
SET tl.dd_plantofassignedset = ifnull(pm.PLMK_AUSWMGWRK1, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_tasklist tl,PLMK pm
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_plantofassignedset <> ifnull(pm.PLMK_AUSWMGWRK1, 'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_textlineforadditionalinfo_10 = ifnull(pm.PLMK_DUMMY10, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_textlineforadditionalinfo_10 <> ifnull(pm.PLMK_DUMMY10, 'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_textlineforadditionalinfo_20 = ifnull(pm.PLMK_DUMMY20, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_textlineforadditionalinfo_20 <> ifnull(pm.PLMK_DUMMY20, 'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_textlineforadditionalinfo_40 = ifnull(pm.PLMK_DUMMY40, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_textlineforadditionalinfo_40 <> ifnull(pm.PLMK_DUMMY40, 'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_creationuser = ifnull(pm.PLMK_ERSTELLER, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_creationuser <> ifnull(pm.PLMK_ERSTELLER, 'Not Set');
	
UPDATE fact_tasklist tl
SET tl.ct_firstupperlimit = ifnull(pm.PLMK_GRENZEOB1, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.ct_firstupperlimit <> ifnull(pm.PLMK_GRENZEOB1, 'Not Set');
	
UPDATE fact_tasklist tl
SET tl.ct_firstlowerlimit = ifnull(pm.PLMK_GRENZEUN1, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.ct_firstlowerlimit <> ifnull(pm.PLMK_GRENZEUN1, 'Not Set');	


MERGE INTO fact_tasklist fact
	USING ( SELECT tl.fact_tasklistid, dt.dim_dateid
			FROM fact_tasklist tl 
			INNER JOIN PLMK pm
			ON tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
			AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
			AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
			AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
			AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    		AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
			INNER JOIN PLPO po
		    ON tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
			AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
			AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
			AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
			INNER JOIN dim_plant dp
			ON dp.plantcode= ifnull(po.PLPO_WERKS, 'Not Set')
			INNER JOIN dim_date dt
			ON dt.datevalue = ifnull(pm.PLMK_GUELTIGAB,'0001-01-01')
			AND dt.companyCode= dp.companycode
			AND dt.plantcode_factory=dp.plantcode
			AND tl.dim_dateidvalidfrom_plmk <> dt.dim_dateid) src
ON fact.fact_tasklistid = src.fact_tasklistid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidvalidfrom_plmk = src.dim_dateid;


UPDATE fact_tasklist tl
SET tl.dd_catalogentry = ifnull(pm.PLMK_KATAB1, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_catalogentry <> ifnull(pm.PLMK_KATAB1, 'Not Set');	
	
UPDATE fact_tasklist tl
SET tl.dd_shorttext_plmk = ifnull(pm.PLMK_KURZTEXT, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_shorttext_plmk <> ifnull(pm.PLMK_KURZTEXT, 'Not Set');	

UPDATE fact_tasklist tl
SET tl.dd_deletionindicator_plmk = ifnull(pm.PLMK_LOEKZ, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_deletionindicator_plmk <> ifnull(pm.PLMK_LOEKZ, 'Not Set');	

UPDATE fact_tasklist tl
SET tl.dim_unitofmeasureid = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND uom.uom = ifnull(pm.PLMK_MASSEINHSW, 'Not Set')
	AND tl.dim_unitofmeasureid <> uom.dim_unitofmeasureid;
	
UPDATE fact_tasklist tl
SET tl.dim_inspectionmethodid = di.dim_inspectionmethodid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm, dim_inspectionmethod di,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND di.PlantCode = ifnull(PLMK_QMTB_WERKS, 'Not Set')
	AND di.insprectionmethod = ifnull(pm.PLMK_PMETHODE,'Not Set')
	AND di.VersionNumber = ifnull(pm.PLMK_PMTVERSION, 'Not Set')
	AND tl.dim_inspectionmethodid <> di.dim_inspectionmethodid;
	
UPDATE fact_tasklist tl
SET tl.dd_sampleno = ifnull(pm.PLMK_PROBENR,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_sampleno <> ifnull(pm.PLMK_PROBENR,'Not Set');
	
UPDATE fact_tasklist tl
SET tl.ct_sampleQty = ifnull(pm.PLMK_PRUEFEINH,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.ct_sampleQty <> ifnull(pm.PLMK_PRUEFEINH,'Not Set');
	


UPDATE fact_tasklist tl
SET tl.dd_plannedresults = ifnull(pm.PLMK_QERGDATH,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_plannedresults <> ifnull(pm.PLMK_QERGDATH,'Not Set');

UPDATE fact_tasklist tl
SET tl.dd_accuracy = ifnull(pm.PLMK_STELLEN,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_accuracy <> ifnull(pm.PLMK_STELLEN,0);

UPDATE fact_tasklist tl
SET tl.dd_cntrlindicator = ifnull(pm.PLMK_STEUERKZ,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_cntrlindicator <> ifnull(pm.PLMK_STEUERKZ,'Not Set');

UPDATE fact_tasklist tl
SET tl.dd_samplingprocedure = ifnull(pm.PLMK_STICHPRVER,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.dd_samplingprocedure <> ifnull(pm.PLMK_STICHPRVER,'Not Set');
	

UPDATE fact_tasklist tl
SET tl.ct_upperlimit = ifnull(pm.PLMK_TOLERANZOB,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.ct_upperlimit <> ifnull(pm.PLMK_TOLERANZOB,0);

UPDATE fact_tasklist tl
SET tl.ct_lowerlimit = ifnull(pm.PLMK_TOLERANZUN,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLMK pm,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND tl.ct_lowerlimit <> ifnull(pm.PLMK_TOLERANZUN,0);
	
/*UPDATE fact_tasklist tl
SET tl.dim_inspectioncharacteristicid = dic.dim_inspectioncharacteristicid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl
FROM PLMK pm, dim_inspectioncharacteristic dic,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND dic.InspectionPlant = ifnull(pm.PLMK_QPMK_ZAEHL, 'Not Set')
	AND dic.Masterinspcharacteristics = ifnull(pm.PLMK_VERWMERKM, 'Not Set')
	AND dic.VersionNumber = ifnull(pm.PLMK_MKVERSION,'Not Set')
	AND tl.dim_inspectioncharacteristicid <>  dic.dim_inspectioncharacteristicid
	*/
	
merge into fact_tasklist tl
using (select distinct tl.fact_tasklistid,  first_value(dic.dim_inspectioncharacteristicid) over (partition by tl.fact_tasklistid order by tl.fact_tasklistid) as dim_inspectioncharacteristicid
FROM PLMK pm, dim_inspectioncharacteristic dic,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
	AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
	AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
	AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
    AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
	AND dic.InspectionPlant = ifnull(pm.PLMK_QPMK_ZAEHL, 'Not Set')
	AND dic.Masterinspcharacteristics = ifnull(pm.PLMK_VERWMERKM, 'Not Set')
	AND dic.VersionNumber = ifnull(pm.PLMK_MKVERSION,'Not Set')) t
on t.fact_tasklistid=tl.fact_tasklistid
when matched then update set  tl.dim_inspectioncharacteristicid = t.dim_inspectioncharacteristicid
where  tl.dim_inspectioncharacteristicid <> t.dim_inspectioncharacteristicid;

	
UPDATE fact_tasklist tl
SET tl.dim_dateidcreatedon_mapl = dt.dim_dateid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM MAPL m, dim_date dt, dim_plant dp,fact_tasklist tl
WHERE  tl.dd_materialno = ifnull(m.MAPL_MATNR, 'Not Set')
	AND tl.dd_PlantCode = ifnull(m.MAPL_WERKS, 'Not Set')
	AND tl.dd_counterforaddcriteria = ifnull(m.MAPL_ZKRIZ, 0)
	AND dt.datevalue = ifnull(m.MAPL_ANDAT,'Not Set')
	AND dt.companyCode= dp.companycode
	AND dp.plantcode= ifnull(m.MAPL_WERKS, 'Not Set')
	and dt.plantcode_factory=dp.plantcode
	AND tl.dd_tltype = ifnull(m.MAPL_PLNTY,'Not Set')
    AND tl.dd_keytlgroup = ifnull(m.MAPL_PLNNR,'Not Set')
	AND ifnull(m.MAPL_PLNAL,'Not Set') = tl.dd_groupcounter
	AND ifnull(m.MAPL_ZAEHL, 0) = dd_internalcounter_mapl
	AND tl.dim_dateidcreatedon_mapl <> dt.dim_dateid;

UPDATE fact_tasklist tl
SET tl.dim_dateidvalidfrom_mapl = dt.dim_dateid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM MAPL m, dim_date dt, dim_plant dp,fact_tasklist tl
WHERE  tl.dd_materialno = ifnull(m.MAPL_MATNR, 'Not Set')
	AND tl.dd_PlantCode = ifnull(m.MAPL_WERKS, 'Not Set')
	AND tl.dd_counterforaddcriteria = ifnull(m.MAPL_ZKRIZ, 0)
	AND dt.datevalue = ifnull(m.MAPL_DATUV,'0001-01-01')
	AND dt.companyCode= dp.companycode
	AND dp.plantcode= ifnull(m.MAPL_WERKS, 'Not Set')
	and dt.plantcode_factory=dp.plantcode
	AND tl.dd_tltype = ifnull(m.MAPL_PLNTY,'Not Set')
    AND tl.dd_keytlgroup = ifnull(m.MAPL_PLNNR,'Not Set')
	AND ifnull(m.MAPL_PLNAL,'Not Set') = tl.dd_groupcounter
	AND ifnull(m.MAPL_ZAEHL, 0) = dd_internalcounter_mapl
	AND tl.dim_dateidvalidfrom_mapl <> dt.dim_dateid;		
	
	
UPDATE fact_tasklist tl
SET tl.dd_creationuser_mapl = ifnull(m.MAPL_ANNAM, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM MAPL m,fact_tasklist tl
WHERE  tl.dd_materialno = ifnull(m.MAPL_MATNR, 'Not Set')
	AND tl.dd_PlantCode = ifnull(m.MAPL_WERKS, 'Not Set')
	AND tl.dd_counterforaddcriteria = ifnull(m.MAPL_ZKRIZ, 0)
	AND tl.dd_tltype = ifnull(m.MAPL_PLNTY,'Not Set')
    AND tl.dd_keytlgroup = ifnull(m.MAPL_PLNNR,'Not Set')
	AND ifnull(m.MAPL_PLNAL,'Not Set') = tl.dd_groupcounter
	AND ifnull(m.MAPL_ZAEHL, 0) = dd_internalcounter_mapl
	AND tl.dd_creationuser_mapl <> ifnull(m.MAPL_ANNAM, 'Not Set');
	
	
UPDATE fact_tasklist tl
SET tl.dd_deletionindicator_mapl = ifnull(m.MAPL_LOEKZ, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM MAPL m,fact_tasklist tl
WHERE  tl.dd_materialno = ifnull(m.MAPL_MATNR, 'Not Set')
	AND tl.dd_PlantCode = ifnull(m.MAPL_WERKS, 'Not Set')
	AND tl.dd_counterforaddcriteria = ifnull(m.MAPL_ZKRIZ, 0)
	AND tl.dd_tltype = ifnull(m.MAPL_PLNTY,'Not Set')
    AND tl.dd_keytlgroup = ifnull(m.MAPL_PLNNR,'Not Set')
	AND ifnull(m.MAPL_PLNAL,'Not Set') = dd_groupcounter
	AND ifnull(m.MAPL_ZAEHL, 0) = dd_internalcounter_mapl
	AND tl.dd_deletionindicator_mapl <> ifnull(m.MAPL_LOEKZ, 'Not Set');
	
	
UPDATE fact_tasklist tl
SET tl.dd_creationuser_plpo = ifnull(po.PLPO_ANNAM,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_creationuser_plpo <> ifnull(po.PLPO_ANNAM,'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dim_companyid = dc.dim_companyid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_company dc,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND ifnull(po.PLPO_BUKRS, 'Not Set') = dc.companycode
	AND tl.dim_companyid <> dc.dim_companyid;	

UPDATE fact_tasklist tl
SET tl.dim_dateidvalidfrom_plpo = dt.dim_dateid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_date dt, dim_plant dp,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND ifnull(po.PLPO_DATUV,'0001-01-01') = dt.datevalue
	AND dt.companyCode= dp.companycode
	AND dp.plantcode= ifnull(po.PLPO_WERKS, 'Not Set')
	and dt.plantcode_factory=dp.plantcode
	AND tl.dim_dateidvalidfrom_plpo <> dt.dim_dateid;	
	
UPDATE fact_tasklist tl
SET tl.dim_standardkeytextid = ds.dim_standardkeytextid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_standardkeytext ds,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND ifnull(PLPO_KTSCH, 'Not Set') = ds.standardtextkey
	/* Madalina 17 Aug 2016 - correct Standard Text Key dimension. Also set 1 as default value instead of 0 */
	/* AND tl.dim_standardkeytextid = ds.dim_standardkeytextid */
	AND tl.dim_standardkeytextid <> ds.dim_standardkeytextid;

UPDATE fact_tasklist tl
SET tl.dd_activitytype01 = ifnull(po.PLPO_LAR01,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_activitytype01 <> ifnull(po.PLPO_LAR01,'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_activitytype02 = ifnull(po.PLPO_LAR02,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_activitytype02 <> ifnull(po.PLPO_LAR02,'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_activitytype03 = ifnull(po.PLPO_LAR03,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_activitytype03 <> ifnull(po.PLPO_LAR03,'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_activitytype04 = ifnull(po.PLPO_LAR04,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_activitytype04 <> ifnull(po.PLPO_LAR04,'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_activitytype05 = ifnull(po.PLPO_LAR05,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_activitytype05 <> ifnull(po.PLPO_LAR05,'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_activitytype06 = ifnull(po.PLPO_LAR06,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_activitytype06 <> ifnull(po.PLPO_LAR06,'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dd_deletionindicator_plpo = ifnull(po.PLPO_LOEKZ,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_deletionindicator_plpo <> ifnull(po.PLPO_LOEKZ,'Not Set');


UPDATE fact_tasklist tl
SET tl.dim_unitofmeasureid_plpo = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND uom.uom = ifnull(po.PLPO_MEINH, 'Not Set')
	AND tl.dim_unitofmeasureid_plpo <> uom.dim_unitofmeasureid;	
	
UPDATE fact_tasklist tl
SET tl.dd_indicator = ifnull(po.PLPO_PHFLG, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_indicator <> ifnull(po.PLPO_PHFLG, 'Not Set');
	
UPDATE fact_tasklist tl
SET tl.dim_controlkeyid = dc.dim_controlkeyid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_controlkey dc,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND ifnull(po.PLPO_STEUS, 'Not Set') = dc.controlkey
	AND tl.dim_controlkeyid <> dc.dim_controlkeyid;
	
UPDATE fact_tasklist tl
SET tl.dd_denominator = ifnull(po.PLPO_UMREN, 1)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_denominator <> ifnull(po.PLPO_UMREN, 1);
	
UPDATE fact_tasklist tl
SET tl.dd_numerator = ifnull(po.PLPO_UMREZ,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_numerator <> ifnull(po.PLPO_UMREZ,0);
	
UPDATE fact_tasklist tl
SET tl.Dim_unitofmeasureid_forSTDvalue1 = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND uom.uom = ifnull(po.PLPO_VGE01, 'Not Set')
	AND tl.Dim_unitofmeasureid_forSTDvalue1 <> uom.dim_unitofmeasureid;	
	
UPDATE fact_tasklist tl
SET tl.Dim_unitofmeasureid_forSTDvalue2 = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND uom.uom = ifnull(po.PLPO_VGE02, 'Not Set')
	AND tl.Dim_unitofmeasureid_forSTDvalue2 <> uom.dim_unitofmeasureid;	
	
UPDATE fact_tasklist tl
SET tl.Dim_unitofmeasureid_forSTDvalue3 = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND uom.uom = ifnull(po.PLPO_VGE03, 'Not Set')
	AND tl.Dim_unitofmeasureid_forSTDvalue3 <> uom.dim_unitofmeasureid;	
	
UPDATE fact_tasklist tl
SET tl.Dim_unitofmeasureid_forSTDvalue4 = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND uom.uom = ifnull(po.PLPO_VGE04, 'Not Set')
	AND tl.Dim_unitofmeasureid_forSTDvalue4 <> uom.dim_unitofmeasureid;	

	
UPDATE fact_tasklist tl
SET tl.Dim_unitofmeasureid_forSTDvalue5 = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND uom.uom = ifnull(po.PLPO_VGE05, 'Not Set')
	AND tl.Dim_unitofmeasureid_forSTDvalue5 <> uom.dim_unitofmeasureid;	
	
UPDATE fact_tasklist tl
SET tl.Dim_unitofmeasureid_forSTDvalue6 = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND uom.uom = ifnull(po.PLPO_VGE06, 'Not Set')
	AND tl.Dim_unitofmeasureid_forSTDvalue6 <> uom.dim_unitofmeasureid;	
	

UPDATE fact_tasklist tl
SET amt_standardvalue4 = ifnull(po.PLPO_VGW04,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND amt_standardvalue4 <> ifnull(po.PLPO_VGW04,0);
	
UPDATE fact_tasklist tl
SET amt_standardvalue6 = ifnull(po.PLPO_VGW06,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND amt_standardvalue6 <> ifnull(po.PLPO_VGW06,0);

UPDATE fact_tasklist tl
SET Dim_plantid_plpo = dp.dim_plantid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, dim_plant dp,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND ifnull(po.PLPO_WERKS, 'Not Set')= ifnull(dp.plantcode, 'Not Set')
	AND Dim_plantid_plpo <> dp.dim_plantid;	
	
UPDATE fact_tasklist tl
SET tl.Dim_unitofmeasureid_plko = uom.dim_unitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po, dim_unitofmeasure uom,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND uom.uom = ifnull(po.PLKO_PLNME, 'Not Set')
	AND tl.Dim_unitofmeasureid_plko <> uom.dim_unitofmeasureid;	

UPDATE fact_tasklist tl
SET tl.dim_sampledrawingprocedureid = ds.dim_sampledrawingprocedureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po, dim_sampledrawingprocedure ds,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND ds.VersionNo = ifnull(po.PLKO_QVERSNPRZV, 'Not Set')
	AND ds.SampleProcedure = ifnull(po.PLKO_QPRZIEHVER, 'Not Set')
	AND tl.dim_sampledrawingprocedureid <> ds.dim_sampledrawingprocedureid;		
	
UPDATE fact_tasklist tl
SET tl.dim_partid = dp.dim_partid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM MAPL m, dim_part dp,fact_tasklist tl
WHERE  tl.dd_materialno = ifnull(m.MAPL_MATNR, 'Not Set')
	AND tl.dd_PlantCode = ifnull(MAPL_WERKS, 'Not Set')
	AND tl.dd_counterforaddcriteria = ifnull(MAPL_ZKRIZ, 0)
	AND ifnull(m.MAPL_MATNR, 'Not Set') = dp.partnumber
	AND ifnull(m.MAPL_WERKS, 'Not Set') = dp.plant
	AND tl.dd_tltype = ifnull(m.MAPL_PLNTY,'Not Set')
    AND tl.dd_keytlgroup = ifnull(m.MAPL_PLNNR,'Not Set')
	AND ifnull(m.MAPL_PLNAL,'Not Set') = dd_groupcounter
	AND ifnull(m.MAPL_ZAEHL, 0) = dd_internalcounter_mapl
    AND	tl.dim_partid <> dp.dim_partid;

UPDATE fact_tasklist tl
SET tl.ct_tolotsize = ifnull(po.PLKO_LOSBS,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.ct_tolotsize <> ifnull(po.PLKO_LOSBS,0);		
	
UPDATE fact_tasklist tl
SET tl.ct_fromlotsize = ifnull(po.PLKO_LOSVN,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.ct_fromlotsize <> ifnull(po.PLKO_LOSVN,0);	

UPDATE fact_tasklist tl
SET tl.dd_creationuser_plko = ifnull(po.PLKO_ANNAM,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.dd_creationuser_plko <> ifnull(po.PLKO_ANNAM,'Not Set');	

UPDATE fact_tasklist tl
SET tl.dd_inspectionpoint = ifnull(po.PLKO_QKZRASTER,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLKO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLKO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLKO_PLNNR,'Not Set')
    AND tl.dd_internalcounter_plko = ifnull(po.PLKO_ZAEHL,0)
	AND tl.dd_groupcounter = ifnull(po.PLKO_PLNAL,'Not Set')
	AND tl.dd_inspectionpoint <> ifnull(po.PLKO_QKZRASTER,'Not Set');

UPDATE fact_tasklist tl
SET tl.dd_nodenumber = ifnull(po.PLPO_SUMNR,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po,fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_nodenumber <> ifnull(po.PLPO_SUMNR,0);
	
/*VW comment: For dim_inspectioncatalogcodesid we are creating a temporary table in order to eliminate duplicates*/
/*EXA comment OZ: replaced temp with a merge instead */

MERGE INTO fact_tasklist fact
 USING ( SELECT distinct tl.fact_tasklistid,di.dim_inspectioncatalogcodesid 
		FROM fact_tasklist tl
		INNER JOIN PLMK pm
		ON tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
		AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
		AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
		AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
		AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
		AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
		INNER JOIN QPMK_QPMT q
		ON ifnull(q.QPMK_ZAEHLER,'Not Set') = ifnull(pm.PLMK_QPMK_ZAEHL,'Not Set')
		AND ifnull(q.QPMK_MKMNR,'Not Set') = ifnull(pm.PLMK_VERWMERKM,'Not Set')
		AND ifnull(q.QPMK_VERSION,'Not Set') = ifnull(pm.PLMK_MKVERSION,'Not Set')
		INNER JOIN dim_inspectioncatalogcodes di
		ON di.CodeGroup = ifnull(q.QPMK_CODEGRQUAL,'Not Set')
		AND di.Code= ifnull(q.QPMK_CODEQUAL,'Not Set')
		AND di.VersionNumber = ifnull(q.QPMK_VERSION,'Not Set')
		AND tl.dim_inspectioncatalogcodesid <> di.dim_inspectioncatalogcodesid) src
ON fact.fact_tasklistid = src.fact_tasklistid 
WHEN MATCHED THEN UPDATE
SET fact.dim_inspectioncatalogcodesid = src.dim_inspectioncatalogcodesid;

/*VW comment: For dim_inspectioncataloggroupid we are creating a temporary table in order to eliminate duplicates*/
/*EXA comment OZ: replaced temp with a merge instead */

MERGE INTO fact_tasklist fact
 USING ( SELECT distinct tl.fact_tasklistid,dig.dim_inspectioncataloggroupid
		FROM fact_tasklist tl
		INNER JOIN PLMK pm
		ON tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
		AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
		AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
		AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
		AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
		AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
		INNER JOIN QPMK_QPMT q
		ON ifnull(q.QPMK_ZAEHLER,'Not Set') = ifnull(pm.PLMK_QPMK_ZAEHL,'Not Set')
		AND ifnull(q.QPMK_MKMNR,'Not Set') = ifnull(pm.PLMK_VERWMERKM,'Not Set')
		AND ifnull(q.QPMK_VERSION,'Not Set') = ifnull(pm.PLMK_MKVERSION,'Not Set')
		INNER JOIN dim_inspectioncatalogcodes di
		ON di.CodeGroup = ifnull(q.QPMK_CODEGRQUAL,'Not Set')
		AND di.Code= ifnull(q.QPMK_CODEQUAL,'Not Set')
		AND di.VersionNumber = ifnull(q.QPMK_VERSION,'Not Set')
		INNER JOIN dim_inspectioncataloggroup dig
	    ON dig.CodeGroup = di.CodeGroup
		AND tl.dim_inspectioncataloggroupid <> dig.dim_inspectioncataloggroupid) src
ON fact.fact_tasklistid = src.fact_tasklistid 
WHEN MATCHED THEN UPDATE
SET fact.dim_inspectioncataloggroupid = src.dim_inspectioncataloggroupid;

/*VW comment: For dim_inspcatalogselectedsetsid we are creating a temporary table in order to eliminate duplicates*/
/*EXA comment OZ: replaced temp with a merge instead */

MERGE INTO fact_tasklist fact
 USING ( SELECT distinct tl.fact_tasklistid,di.dim_inspcatalogselectedsetsid
		FROM fact_tasklist tl
		INNER JOIN PLMK pm
		ON tl.dd_tltype = ifnull(pm.PLMK_PLNTY, 'Not Set')
		AND tl.dd_keytlgroup = ifnull(pm.PLMK_PLNNR,'Not Set')
		AND tl.dd_notlnode = ifnull(pm.PLMK_PLNKN,0)
		AND tl.dd_internalcounter_plmk = ifnull(pm.PLMK_ZAEHL,0)
		AND tl.dd_CharacteristicType = ifnull(pm.PLMK_KZEINSTELL, 'Not Set')
		AND tl.dd_inspCharacteristicNo = ifnull(pm.PLMK_MERKNR,0)
		INNER JOIN dim_inspcatalogselectedsets di
		ON di."catalog" = ifnull(pm.plmk_KATALGART1, 'Not Set')
		AND di.SelectedStes = ifnull(pm.PLMK_AUSWMENGE1, 'Not Set')
		AND di.Plant = ifnull(pm.PLMK_AUSWMGWRK1, 'Not Set')
		AND tl.dim_inspcatalogselectedsetsid <> di.dim_inspcatalogselectedsetsid) src
ON fact.fact_tasklistid = src.fact_tasklistid 
WHEN MATCHED THEN UPDATE
SET fact.dim_inspcatalogselectedsetsid = src.dim_inspcatalogselectedsetsid;


UPDATE fact_tasklist tl
SET tl.dd_deletionindicator_plas = ifnull(PLAS_LOEKZ, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLAS po,fact_tasklist tl
WHERE
tl.dd_tltype = ifnull(po.PLAS_PLNTY,'Not Set')
AND tl.dd_keytlgroup = ifnull(po.PLAS_PLNNR,'Not Set')
AND tl.dd_groupcounter = ifnull(po.PLAS_PLNAL,'Not Set')
AND tl.dd_notlnode = ifnull(po.PLAS_PLNKN,0)
AND tl.dd_internalcounter_plas = ifnull(po.PLAS_ZAEHL,0)
AND tl.dd_sequence = ifnull(po.PLAS_PLNFL,'Not Set')
AND tl.dd_deletionindicator_plas <> ifnull(po.PLAS_LOEKZ, 'Not Set');

/* OZ : There are duplicates but that's because work center contains cost center as well and can't be mapped into Task List. As of now, 05/2016, MAH is only interested in the Work Center for this SA */
MERGE INTO fact_tasklist fact
USING ( SELECT distinct tl.fact_tasklistid, FIRST_VALUE(wc.dim_workcenterid) OVER (PARTITION BY wc.objectid,wc.objecttype ORDER BY wc.dim_workcenterid DESC) AS dim_workcenterid
		FROM fact_tasklist tl, dim_workcenter wc
		WHERE ifnull(CONVERT(VARCHAR(8),tl.dd_workcenterobjectid),'Not Set') = wc.objectid AND 
		ifnull(TRIM(tl.dd_workcenterobjecttype),'Not Set') = wc.objecttype
		AND tl.dim_workcenterid <> wc.dim_workcenterid
		) src
ON fact.fact_tasklistid = src.fact_tasklistid
WHEN MATCHED THEN UPDATE
SET fact.dim_workcenterid = src.dim_workcenterid,
dw_update_date = CURRENT_TIMESTAMP;

/* Liviu Ionescu - BI-3560 Task List: add two fields */

UPDATE fact_tasklist tl
SET tl.dd_RelevancyCostingInd = ifnull(po.PLPO_CKSELKZ,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_RelevancyCostingInd <> ifnull(po.PLPO_CKSELKZ,'Not Set');

UPDATE fact_tasklist tl
SET tl.dd_SecondaryResourceInd = ifnull(po.PLPO_SUMNR,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM PLPO po, fact_tasklist tl
WHERE tl.dd_tltype = ifnull(po.PLPO_PLNTY,'Not Set')
	AND tl.dd_keytlgroup = ifnull(po.PLPO_PLNNR,'Not Set')
	AND tl.dd_notlnode = ifnull(po.PLPO_PLNKN,0)
	AND tl.dd_internalcounter_plpo = ifnull(po.PLPO_ZAEHL,0)
	AND tl.dd_SecondaryResourceInd <> ifnull(po.PLPO_SUMNR,0);

/* End Liviu Ionescu */

 
UPDATE fact_tasklist f
SET f.std_exchangerate_dateid = dt.dim_dateid
FROM dim_plant p INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN fact_tasklist f
ON f.dim_plantid = p.dim_plantid
AND   dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

DELETE FROM fact_tasklist tl
WHERE NOT EXISTS (SELECT 1
FROM 
MAPL mp
INNER JOIN PLKO ko ON mp.MAPL_PLNTY = ko.PLKO_PLNTY and mp.MAPL_PLNNR = ko.PLKO_PLNNR and mp.MAPL_PLNAL = ko.PLKO_PLNAL
INNER JOIN PLAS ps ON ps.PLAS_PLNTY = ko.PLKO_PLNTY and ps.PLAS_PLNNR = ko.PLKO_PLNNR and ps.PLAS_PLNAL = ko.PLKO_PLNAL
RIGHT JOIN PLPO po ON po.PLPO_PLNTY = ps.PLAS_PLNTY and po.PLPO_PLNNR = ps.PLAS_PLNNR and po.PLPO_PLNKN = ps.PLAS_PLNKN
LEFT JOIN PLMK mk ON mk.PLMK_PLNTY = po.PLPO_PLNTY and mk.PLMK_PLNNR = po.PLPO_PLNNR and mk.PLMK_PLNKN = po.PLPO_PLNKN
LEFT JOIN CRHD c ON ifnull(c.CRHD_OBJID,0) = ifnull(po.PLPO_ARBID,0)
WHERE
ifnull(po.PLPO_PLNTY,'Not Set') = tl.dd_tltype AND 
ifnull(po.PLPO_PLNNR,'Not Set') = tl.dd_keytlgroup AND 
ifnull(po.PLPO_PLNKN,0) = tl.dd_notlnode AND 
ifnull(po.PLPO_ZAEHL,0) = tl.dd_internalcounter_plpo AND 
ifnull(ko.PLKO_PLNAL,'Not Set') = tl.dd_groupcounter AND 
ifnull(ko.PLKO_ZAEHL,0) = tl.dd_internalcounter_plko AND 
ifnull(mk.PLMK_KZEINSTELL, 'Not Set') = tl.dd_CharacteristicType AND 
ifnull(mk.PLMK_MERKNR,0) = tl.dd_inspCharacteristicNo AND 
ifnull(mk.PLMK_ZAEHL,0) = tl.dd_internalcounter_plmk AND 
ifnull(ps.PLAS_PLNFL,'Not Set') = tl.dd_sequence AND 
ifnull(ps.PLAS_ZAEHL,0) = tl.dd_internalcounter_plas AND 
ifnull(mp.MAPL_MATNR, 'Not Set') = tl.dd_materialno AND 
ifnull(mp.MAPL_WERKS, 'Not Set') = tl.dd_PlantCode AND 
ifnull(mp.MAPL_ZKRIZ, 0) = tl.dd_counterforaddcriteria AND 
ifnull(mp.MAPL_ZAEHL, 0) = tl.dd_internalcounter_mapl AND 
ifnull(c.CRHD_OBJTY,'Not Set') = tl.dd_workcenterobjecttype AND 
ifnull(c.CRHD_OBJID,0) = tl.dd_workcenterobjectid);
