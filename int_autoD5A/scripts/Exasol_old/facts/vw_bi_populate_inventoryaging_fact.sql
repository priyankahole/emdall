/**********************************************************************************/
/*  Script : vw_bi_populate_inventoryaging_fact.sql                               */
/*  Description : This script is recreated to allocate total stock quantity into  */
/*                each material movement quantity to reflect actual stock aging.  */
/*  Created On : July 2, 2013                                                     */
/*  Create By : Hiten Suthar                                                      */
/*                                                                                */
/*  Change History                                                                */
/*  Date         CVSversion  By          Description                              */
/*  02 July 2013  1.0         Hiten      New Script                               */
/*  31 July 2013  1.42        Ashu       Optimized an Insert statement            */
/*  14 Aug 2013   1.47        Hiten      Only latest ex rate                      */
/*                                       and added MARC, STO and WIP stock        */
/*  27 Aug 2013   1.54        Hiten      Default for null column values           */
/*  27 Aug 2013   1.55        Shanthi    Checking in Profit Center dimension      */
/*  28 Aug 2013   1.56	      Shanthi    New Field WIP Qty from PRod Order	  */
/*  10 Sep 2013   1.57        Lokesh	 Currency changes			  */
/*  15 Oct 2013   1.81        Issam 	 Added material movement measures	  */
/*  01 Nov 2013   1.86        Hiten 	 On Hand Amount Adjustment       	  */
/*  20 Feb 2014	  1.87	      Nicoleta	 Added dim_DateidExpiryDate         */
/*  20 Feb 2014	  1.1	      Hiten	 Preference MCHB over MARD for batch number */
/*  09 Sep 2014	  2.0	     A.Ungureanu Insert Price Unit MBEW_PEINH, MBEW_LBKUM */
/*  04 Jun 2015   2.1         Suchithra  Modification on Update from fact_purchase to */
/*					 fix ambiguous replace error */
/*  05 Jun 2015   2.2         Georgiana Modification on Update from fact_purchase to*/
                              /*fix ambiguous replace error on inv.dim_profitcenterid*/
/*  09 Jun 2015   2.3        Georgiana  Fix error in operator tree */
/*                          for tmp_fact_inventoryaging_upd_expdate */
/*	15 Sept 2016  2.4*  	Alin Gheorghe 	add  Inventory Questionable Value (IQV) data from csv*/
/**********************************************************************************/

DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

DROP TABLE IF EXISTS fact_inventoryaging_tmp_populate;
CREATE TABLE fact_inventoryaging_tmp_populate
like  fact_inventoryaging  INCLUDING DEFAULTS INCLUDING IDENTITY;

ALTER TABLE fact_inventoryaging_tmp_populate ADD PRIMARY KEY (fact_inventoryagingid);


delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryaging_tmp_populate',ifnull(max(fact_inventoryagingid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
FROM fact_inventoryaging_tmp_populate;

DELETE FROM fact_inventoryaging_tmp_populate;

DELETE FROM MCHB WHERE MCHB_CLABS = 0 AND MCHB_CUMLM = 0 AND MCHB_CINSM = 0 AND MCHB_CEINM = 0 AND MCHB_CSPEM = 0;
DELETE FROM MARD WHERE MARD_LABST = 0 AND MARD_UMLME = 0 AND MARD_INSME = 0 AND MARD_EINME = 0 AND MARD_SPEME = 0
			AND MARD_KSPEM = 0 AND MARD_KINSM = 0 AND MARD_KEINM = 0 AND MARD_KLABS = 0;
DELETE FROM MSLB WHERE MSLB_LBLAB = 0 AND MSLB_LBINS = 0 AND MSLB_LBEIN = 0;
DELETE FROM MSKU WHERE MSKU_KULAB = 0 AND MSKU_KUINS = 0 AND MSKU_KUEIN = 0;
DELETE FROM MARC WHERE MARC_UMLMC = 0 AND MARC_TRAME = 0 AND MARC_GLGMG = 0 AND MARC_BWESB = 0;
DELETE FROM MARC WHERE MARC_LVORM = 'X';

delete from mard
where exists (select 1 from mard b where mard.mard_matnr = b.mard_matnr and mard.mard_werks = b.mard_werks
			and mard.mard_lgort <> b.mard_lgort and mard.mard_labst = -1 * b.mard_labst);

UPDATE MCHB SET MCHB_CHARG = 'Not Set' WHERE MCHB_CHARG IS NULL;
UPDATE MSLB SET MSLB_CHARG = 'Not Set' WHERE MSLB_CHARG IS NULL;
UPDATE MSKU SET MSKU_CHARG = 'Not Set' WHERE MSKU_CHARG IS NULL;

UPDATE fact_materialmovement SET dd_BatchNumber = 'Not Set' WHERE dd_BatchNumber IS NULL;

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'dim_storagelocation';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'dim_storagelocation', IFNULL(MAX(dim_storagelocationid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM dim_storagelocation WHERE dim_storagelocationid <> 1;

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
				Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_storagelocation') + row_number() over (order by ''),tty.*
FROM
   (SELECT DISTINCT MCHB_LGORT,
                   MCHB_LGORT,
                   'Not Set' Division,
                   'Not Set' FreezingBookInventory,
                   'Not Set' MRPExclude,
                   'Not Set' StorageResource,
                   'Not Set' PartnerStorageLocation,
                   'Not Set' StorageSalesOrg,
                   'Not Set' DistributionChannel,
                   'Not Set' ShipReceivePoint ,
                   MCHB_WERKS Plant ,
                   'Not Set' InTransit,
                   current_timestamp RowStartDate,
                   1 RowIsCurrent
     FROM MCHB m
    WHERE MCHB_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE ifnull(LocationCode,'xxx') = ifnull(MCHB_LGORT,'yyy') AND ifnull(Plant,'xxx') = ifnull(MCHB_WERKS,'yyy'))) tty;

 UPDATE NUMBER_FOUNTAIN
 SET max_id = ifnull(( select max(dim_storagelocationid) from dim_storagelocation), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
 WHERE table_name = 'dim_storagelocation';

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
				Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent)
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_storagelocation') + row_number() over (order by ''),tty.*
FROM
   (SELECT DISTINCT MARD_LGORT,
                   MARD_LGORT,
                   'Not Set' Division,
                   'Not Set' FreezingBookInventory ,
                   'Not Set' MRPExclude,
                   'Not Set' StorageResource,
                   'Not Set' PartnerStorageLocation,
                   'Not Set' StorageSalesOrg,
                   'Not Set' DistributionChannel,
                   'Not Set' ShipReceivePoint,
                   MARD_WERKS Plant ,
                   'Not Set' InTransit,
                   current_timestamp RowStartDate,
                   1 RowIsCurrent
     FROM MARD m
    WHERE MARD_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE ifnull(LocationCode,'xxx') = ifnull(MARD_LGORT,'yyy') AND ifnull(Plant,'xxx') = ifnull(MARD_WERKS,'yyy'))) tty;

DELETE FROM fact_materialmovement_tmp_invagin;

INSERT INTO fact_materialmovement_tmp_invagin
  (Dim_Partid, Dim_Plantid, Dim_MovementTypeid, dd_BatchNumber, dd_MaterialDocYearItem)
  SELECT x.Dim_Partid, x.Dim_Plantid, x.Dim_MovementTypeid, ifnull(x.dd_BatchNumber,'Not Set') dd_BatchNumber,
      max(concat(x.dd_MaterialDocYear,LPAD(x.dd_MaterialDocNo,10,'0'),LPAD(x.dd_MaterialDocItemNo,5,'0')))
  FROM fact_materialmovement x
        INNER JOIN dim_movementtype dmt ON x.Dim_MovementTypeid = dmt.Dim_MovementTypeid
  WHERE dmt.MovementType in ('101','131','105','501','511','521')
  GROUP BY x.Dim_Partid, x.Dim_Plantid, x.Dim_MovementTypeid, ifnull(x.dd_BatchNumber,'Not Set');


DROP TABLE IF EXISTS tmp_MARD_TotStk_001;
CREATE TABLE tmp_MARD_TotStk_001 AS
SELECT MARD_MATNR PartNumber,
	MARD_WERKS PlantCode,
	MARD_LGORT StorLocCode,
	convert(varchar(10),'Not Set') BatchNumber,
	MARD_UMLME MARD_UMLME_1,
	MARD_LABST MARD_LABST_2,
	MARD_SPEME MARD_SPEME_3,
	MARD_INSME MARD_INSME_4,
	MARD_EINME MARD_EINME_5,
       (MARD_LABST + MARD_EINME + MARD_INSME + MARD_SPEME + MARD_UMLME) TotalStock,
	MARD_KSPEM,
	MARD_KINSM,
	MARD_KEINM,
	MARD_KLABS,
	MARD_RETME,
	MARD_DLINL,
	MARD_ERSDA
FROM MARD
WHERE NOT EXISTS (SELECT 1 FROM MCHB
		   WHERE ifnull(MCHB_MATNR,'xxx') = ifnull(MARD_MATNR,'yyy')
			 AND ifnull(MCHB_WERKS,'xxx') = ifnull(MARD_WERKS,'yyy')
			 AND ifnull(MCHB_LGORT,'xxx') = ifnull(MARD_LGORT,'yyy'));

INSERT INTO tmp_MARD_TotStk_001
SELECT MCHB_MATNR PartNumber,
	MCHB_WERKS PlantCode,
	MCHB_LGORT StorLocCode,
	MCHB_CHARG BatchNumber,
	MCHB_CUMLM MARD_UMLME_1,
	MCHB_CLABS MARD_LABST_2,
	MCHB_CSPEM MARD_SPEME_3,
	MCHB_CINSM MARD_INSME_4,
	MCHB_CEINM MARD_EINME_5,
       (MCHB_CLABS + MCHB_CEINM + MCHB_CINSM + MCHB_CSPEM + MCHB_CUMLM) TotalStock,
	0 MARD_KSPEM,
	0 MARD_KINSM,
	0 MARD_KEINM,
	0 MARD_KLABS,
	MCHB_CRETM MARD_RETME,
	MCHB_ERSDA MARD_DLINL,
	MCHB_ERSDA MARD_ERSDA
FROM MCHB;



DROP TABLE IF EXISTS tmp_MARD_TotStk_001_U;
CREATE TABLE tmp_MARD_TotStk_001_U AS
SELECT PartNumber,
	PlantCode,
StorLocCode,
BatchNumber,
	SUM(TotalStock) TotalStockQty
FROM tmp_MARD_TotStk_001
GROUP BY PartNumber,
	PlantCode,StorLocCode,
BatchNumber;


DROP TABLE IF EXISTS tmp_MARD_TotStk_t01;
CREATE TABLE tmp_MARD_TotStk_t01 AS
SELECT PartNumber,
	PlantCode,
	StorLocCode,
	BatchNumber,
	TotalStock,
    ROW_NUMBER() OVER(PARTITION BY PartNumber, PlantCode,StorLocCode, BatchNumber
    			ORDER BY MARD_UMLME_1 desc,
				MARD_LABST_2 desc,
				MARD_SPEME_3 desc,
				MARD_INSME_4 desc,
				MARD_EINME_5 desc,
				StorLocCode, BatchNumber) M_RowSeqNo
FROM tmp_MARD_TotStk_001;

DROP TABLE IF EXISTS tmp_MARD_TotStk_t02;
CREATE TABLE tmp_MARD_TotStk_t02 AS
SELECT a.PartNumber,
	a.PlantCode,
	a.StorLocCode,
	a.BatchNumber,
	a.TotalStock,
        a.M_RowSeqNo,
	SUM(b.TotalStock) - a.TotalStock TotalStockCUM_A,
	SUM(b.TotalStock) TotalStockCUM_B
FROM tmp_MARD_TotStk_t01 a
	inner join tmp_MARD_TotStk_t01 b on a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode
WHERE a.M_RowSeqNo >= b.M_RowSeqNo
GROUP BY a.PartNumber,
	a.PlantCode,
	a.StorLocCode,
	a.BatchNumber,
	a.TotalStock,
        a.M_RowSeqNo;


DROP TABLE IF EXISTS materialmovement_tmp_001_pre;
CREATE TABLE materialmovement_tmp_001_pre AS
      SELECT cx.fact_materialmovementid,
          cx.ct_Quantity,
	      dp1.PartNumber,
		  dpl.PlantCode,
		  dt1.DateValue,
		  cx.dd_MaterialDocNo, cx.dd_MaterialDocItemNo, cx.dd_DocumentScheduleNo,cx.dim_storagelocationid,dd_BatchNumber
      FROM fact_materialmovement cx
	    INNER JOIN dim_movementtype dmt ON cx.Dim_MovementTypeid = dmt.Dim_MovementTypeid AND cx.dd_debitcreditid = 'Debit'
				AND dmt.MovementType in ('101','105','131','222','301','305','309','351','453','501','503','505','511','521','523','525','541','561','602','621','631','643','645','647','644','653','655','657','673','701')
        INNER JOIN dim_Plant dpl ON cx.dim_Plantid = dpl.dim_Plantid
        INNER JOIN dim_Part dp1 ON cx.dim_Partid = dp1.dim_Partid
        INNER JOIN dim_date dt1 ON dt1.Dim_Dateid = cx.dim_DateIDPostingDate
        inner join dim_storagelocation ds on cx.dim_storagelocationid=ds.dim_storagelocationid
	    INNER JOIN tmp_MARD_TotStk_001_U mrd ON dp1.PartNumber = mrd.PartNumber AND dpl.PlantCode = mrd.PlantCode and mrd.BatchNumber=cx.dd_BatchNumber and mrd.StorLocCode=ds.LocationCode and mrd.PlantCode=ds.Plant
     WHERE cx.ct_Quantity > 0 AND cx.dim_specialstockid = 1
	   AND (dmt.MovementType not in ('351','641','643','645','647')
			OR (dmt.MovementType in ('351','641','643','645','647')
				AND NOT EXISTS (SELECT 1
						FROM fact_materialmovement cx1
							INNER JOIN dim_movementtype dmt1 ON cx1.Dim_MovementTypeid = dmt1.Dim_MovementTypeid
						WHERE cx1.dd_debitcreditid = 'Debit' AND ifnull(dmt1.MovementType,'xxx') = '101'
						     AND cx1.dim_Partid = cx.dim_Partid AND cx1.dim_specialstockid = 1
		     AND ifnull(cx1.dd_DocumentNo,'xxx') = ifnull(cx.dd_DocumentNo,'yyy') AND ifnull(cx1.dd_DocumentItemNo,-1) = ifnull(cx.dd_DocumentItemNo,-2)
						     AND ifnull(cx1.ct_Quantity,-1) = ifnull(cx.ct_Quantity,-2))));


DROP TABLE IF EXISTS materialmovement_tmp_001;
CREATE TABLE materialmovement_tmp_001 AS
      SELECT cx.fact_materialmovementid,
          cx.ct_Quantity ct_Quantity,
	      cx.PartNumber PartNumber,
		  cx.PlantCode PlantCode,
		  cx.DateValue PostingDate,cx.dim_storagelocationid,cx.dd_BatchNumber,
		  ROW_NUMBER() OVER(PARTITION BY cx.PartNumber, cx.PlantCode,cx.dim_storagelocationid,cx.dd_BatchNumber
					ORDER BY cx.DateValue DESC, cx.dd_MaterialDocNo DESC, cx.dd_MaterialDocItemNo DESC, cx.dd_DocumentScheduleNo DESC) RowSeqNo
      FROM materialmovement_tmp_001_pre cx;

DROP TABLE IF EXISTS materialmovement_tmp_001_pre;

DROP TABLE IF EXISTS tmp_mm_lastrcptinfo_001;
CREATE TABLE tmp_mm_lastrcptinfo_001 AS
SELECT cx.PartNumber,
	cx.PlantCode,
	cx.ct_Quantity,
	cx.PostingDate,
cx.dim_storagelocationid,cx.dd_BatchNumber
FROM  materialmovement_tmp_001 cx
WHERE cx.RowSeqNo = 1;

DROP TABLE IF EXISTS mm_tmp_maxseqno_001;
CREATE TABLE mm_tmp_maxseqno_001 AS
SELECT a.PartNumber,
	  a.PlantCode,
a.dim_storagelocationid,a.dd_BatchNumber,
	  MAX(a.RowSeqNo) MaxRowSeqNo
FROM materialmovement_tmp_001 a
GROUP BY a.PartNumber,
	  a.PlantCode,a.dim_storagelocationid,a.dd_BatchNumber;

DROP TABLE IF EXISTS materialmovement_tmp_002;
CREATE TABLE materialmovement_tmp_002 AS
SELECT a.fact_materialmovementid,
	  a.ct_Quantity,
	  a.PartNumber,
	  a.PlantCode,a.dim_storagelocationid,a.dd_BatchNumber,
	  a.PostingDate,
	  a.RowSeqNo,
	  SUM(b.ct_Quantity) - a.ct_Quantity QuantityCUM_A,
	  SUM(b.ct_Quantity) QuantityCUM_B,
	  c.MaxRowSeqNo
FROM materialmovement_tmp_001 a
	inner join materialmovement_tmp_001 b on a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode and a.dim_storagelocationid =b.dim_storagelocationid and a.dd_BatchNumber=b.dd_BatchNumber
	inner join mm_tmp_maxseqno_001 c on a.PartNumber = c.PartNumber and a.PlantCode = c.PlantCode and a.dim_storagelocationid =c.dim_storagelocationid and a.dd_BatchNumber=c.dd_BatchNumber
WHERE a.RowSeqNo >= b.RowSeqNo
GROUP BY a.fact_materialmovementid,
	  a.ct_Quantity,
	  a.PartNumber,
	  a.PlantCode,a.dim_storagelocationid,a.dd_BatchNumber,
	  a.PostingDate,
	  a.RowSeqNo,
	  c.MaxRowSeqNo;
/*Georgiana 28 Oct Optimizing this too*/
/*DROP TABLE IF EXISTS materialmovement_tmp_new
CREATE TABLE materialmovement_tmp_new
AS
SELECT cx.fact_materialmovementid,
	cx.dd_MaterialDocNo dd_MaterialDocNo,
	cx.dd_MaterialDocItemNo dd_MaterialDocItemNo,
	cx.dd_MaterialDocYear dd_MaterialDocYear,
	ifnull(cx.dd_DocumentNo,'Not Set') dd_DocumentNo,
	cx.dd_DocumentItemNo dd_DocumentItemNo,
	cx.dd_DocumentScheduleNo,
	ifnull(cx.dd_SalesOrderNo,'Not Set') dd_SalesOrderNo,
	cx.dd_SalesOrderItemNo dd_SalesOrderItemNo,
	ifnull(cx.dd_SalesOrderDlvrNo,0) dd_SalesOrderDlvrNo,
	cx.dd_BatchNumber dd_BatchNumber,
	cx.dd_ValuationType dd_ValuationType,
	cx.dd_debitcreditid dd_debitcreditid,
	cx.amt_LocalCurrAmt amt_LocalCurrAmt,
	cx.amt_DeliveryCost amt_DeliveryCost,
	cx.amt_ExchangeRate_GBL amt_ExchangeRate_GBL,
	cx.amt_AltPriceControl amt_AltPriceControl,
	cx.amt_OnHand_ValStock amt_OnHand_ValStock,
	cx.ct_Quantity ct_Quantity,
	cx.ct_Quantity ct_QtyEntryUOM,
	cx.Dim_MovementTypeid Dim_MovementTypeid,
	cx.dim_Companyid dim_Companyid,
	cx.Dim_Partid Dim_Partid,
	cx.Dim_Plantid Dim_Plantid,
	cx.Dim_StorageLocationid Dim_StorageLocationid,
	cx.Dim_Vendorid Dim_Vendorid,
	cx.dim_DateIDPostingDate dim_DateIDPostingDate,
	cx.dim_MovementIndicatorid dim_MovementIndicatorid,
	cx.dim_CostCenterid dim_CostCenterid,
	cx.dim_specialstockid dim_specialstockid,
	cx.Dim_StockTypeid Dim_StockTypeid,
	cx.Dim_Currencyid Dim_Currencyid,
	cx.amt_ExchangeRate amt_ExchangeRate,
	a.PartNumber,
	a.PlantCode,
	a.PostingDate,
	a.QuantityCUM_A,
	a.QuantityCUM_B,
	b.ct_Quantity LatestReceiptQty,
	b.PostingDate LatestPostingDate,
	a.RowSeqNo,
	a.MaxRowSeqNo,
	cx.Dim_Currencyid_TRA Dim_Currencyid_TRA,
	cx.Dim_Currencyid_GBL Dim_Currencyid_GBL
FROM fact_materialmovement cx
	inner join materialmovement_tmp_002 a on a.fact_materialmovementid = cx.fact_materialmovementid
	inner join dim_storagelocation ds on cx.dim_storagelocationid=ds.dim_storagelocationid
	inner join tmp_mm_lastrcptinfo_001 b on a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode and a.dim_storagelocationid =b.dim_storagelocationid and a.dd_BatchNumber=b.dd_BatchNumber
	inner join tmp_MARD_TotStk_001_U c on a.PartNumber = c.PartNumber AND a.PlantCode = c.PlantCode and c.BatchNumber=a.dd_BatchNumber and c.StorLocCode=ds.LocationCode and c.PlantCode=ds.Plant and  a.QuantityCUM_A < c.TotalStockQty*/

	DROP TABLE IF EXISTS materialmovement_tmp_new;
CREATE TABLE materialmovement_tmp_new
AS
SELECT cx.fact_materialmovementid,
	cx.dd_MaterialDocNo dd_MaterialDocNo,
	cx.dd_MaterialDocItemNo dd_MaterialDocItemNo,
	cx.dd_MaterialDocYear dd_MaterialDocYear,
	ifnull(cx.dd_DocumentNo,'Not Set') dd_DocumentNo,
	cx.dd_DocumentItemNo dd_DocumentItemNo,
	cx.dd_DocumentScheduleNo,
	ifnull(cx.dd_SalesOrderNo,'Not Set') dd_SalesOrderNo,
	cx.dd_SalesOrderItemNo dd_SalesOrderItemNo,
	ifnull(cx.dd_SalesOrderDlvrNo,0) dd_SalesOrderDlvrNo,
	cx.dd_BatchNumber dd_BatchNumber,
	cx.dd_ValuationType dd_ValuationType,
	cx.dd_debitcreditid dd_debitcreditid,
	cx.amt_LocalCurrAmt amt_LocalCurrAmt,
	cx.amt_DeliveryCost amt_DeliveryCost,
	cx.amt_ExchangeRate_GBL amt_ExchangeRate_GBL,
	cx.amt_AltPriceControl amt_AltPriceControl,
	cx.amt_OnHand_ValStock amt_OnHand_ValStock,
	cx.ct_Quantity ct_Quantity,
	cx.ct_Quantity ct_QtyEntryUOM,
	cx.Dim_MovementTypeid Dim_MovementTypeid,
	cx.dim_Companyid dim_Companyid,
	cx.Dim_Partid Dim_Partid,
	cx.Dim_Plantid Dim_Plantid,
	cx.Dim_StorageLocationid Dim_StorageLocationid,
	cx.Dim_Vendorid Dim_Vendorid,
	cx.dim_DateIDPostingDate dim_DateIDPostingDate,
	cx.dim_MovementIndicatorid dim_MovementIndicatorid,
	cx.dim_CostCenterid dim_CostCenterid,
	cx.dim_specialstockid dim_specialstockid,
	cx.Dim_StockTypeid Dim_StockTypeid,
	cx.Dim_Currencyid Dim_Currencyid,
	cx.amt_ExchangeRate amt_ExchangeRate,
	a.PartNumber,
	a.PlantCode,
	a.PostingDate,
	a.QuantityCUM_A,
	a.QuantityCUM_B,
	b.ct_Quantity LatestReceiptQty,
	b.PostingDate LatestPostingDate,
	a.RowSeqNo,
	a.MaxRowSeqNo,
	cx.Dim_Currencyid_TRA Dim_Currencyid_TRA,
	cx.Dim_Currencyid_GBL Dim_Currencyid_GBL,
   ds.LocationCode
FROM fact_materialmovement cx
	inner join dim_storagelocation ds on cx.dim_storagelocationid=ds.dim_storagelocationid
	inner join materialmovement_tmp_002 a on a.fact_materialmovementid = cx.fact_materialmovementid
	inner join tmp_mm_lastrcptinfo_001 b on a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode and a.dim_storagelocationid =b.dim_storagelocationid and a.dd_BatchNumber=b.dd_BatchNumber;

drop table if exists  materialmovement_tmp_new_02;
create table materialmovement_tmp_new_02 as
select a.* from materialmovement_tmp_new a
	inner join tmp_MARD_TotStk_001_U c on a.PartNumber = c.PartNumber AND a.PlantCode = c.PlantCode and c.BatchNumber=a.dd_BatchNumber and c.StorLocCode=a.LocationCode and  a.QuantityCUM_A < c.TotalStockQty;

DROP TABLE IF EXISTS mm_tmp_maxseqno_001;
CREATE TABLE mm_tmp_maxseqno_001 AS
SELECT a.PartNumber,
	  a.PlantCode,dd_BatchNumber,Dim_StorageLocationid,
	  MAX(a.RowSeqNo) MaxRowSeqNo
FROM materialmovement_tmp_new_02 a
GROUP BY a.PartNumber,
	  a.PlantCode,dd_BatchNumber,Dim_StorageLocationid;

update materialmovement_tmp_new_02 a
set a.MaxRowSeqNo = b.MaxRowSeqNo
from materialmovement_tmp_new_02 a,mm_tmp_maxseqno_001 b
where a.PartNumber = b.PartNumber and a.PlantCode = b.PlantCode and a.dd_BatchNumber=b.dd_BatchNumber and a.Dim_StorageLocationid=b.Dim_StorageLocationid;


DROP TABLE IF EXISTS tmp_c2;
CREATE TABLE tmp_c2 AS
SELECT PartNumber,PlantCode,dd_BatchNumber,Dim_StorageLocationid,max(QuantityCUM_B) as max_stk
FROM materialmovement_tmp_new_02 c1
GROUP BY PartNumber,PlantCode,dd_BatchNumber,Dim_StorageLocationid;

DROP TABLE IF EXISTS tmp_c3;
CREATE TABLE tmp_c3 AS
SELECT c1.*
FROM materialmovement_tmp_new_02 c1, tmp_c2 c2
WHERE c2.PartNumber = c1.PartNumber AND c2.PlantCode = c1.PlantCode and c2.dd_BatchNumber=c1.dd_BatchNumber and c2.Dim_StorageLocationid=c1.Dim_StorageLocationid
	AND c1.QuantityCUM_B = ifnull(c2.max_stk,0) ;



/*** MBEW NO BWTAR values ***/

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY)
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR AS
SELECT DISTINCT m.MATNR,
       m.BWKEY,
       m.LFGJA,
       m.LFMON,
       m.VPRSV,
       m.VERPR,
       m.STPRS,
       m.PEINH,
       m.SALK3,
       m.BWTAR,
       m.MBEW_LBKUM,
       ((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON,
       m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR ADD column multiplier decimal(18,5) default 0;

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = convert(decimal (18,5), (b.STPRS / b.PEINH))
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  convert(decimal (18,5),(b.VERPR / b.PEINH))
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

/* divide inventory stock into material movement qty */

DROP TABLE IF EXISTS tmp_fact_invaging_001;
CREATE TABLE tmp_fact_invaging_001 AS
SELECT b.fact_materialmovementid,
	a.PartNumber,
	a.PlantCode,
	a.StorLocCode,
	a.BatchNumber,
	case when a.MARD_UMLME_1 <= 0 then 0
		when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_UMLME_1
				when a.MARD_UMLME_1 <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo
				then a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A)
				else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
			end
		else 0
	end MARD_UMLME_1,
	case when a.MARD_LABST_2 <= 0 then 0
		when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_LABST_2
				when (a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo
				then case when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - (a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when a.MARD_UMLME_1 > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when a.MARD_UMLME_1 <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - (a.MARD_UMLME_1 - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_LABST_2,
	case when a.MARD_SPEME_3 <= 0 then 0
		when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_SPEME_3
				when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo
				then case when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_SPEME_3,
	case when a.MARD_INSME_4 <= 0 then 0
		when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_INSME_4
				when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo
				then case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										- ((a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_INSME_4,
	case when a.MARD_EINME_5 <= 0 then 0
		when (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
		then case when a.TotalStock <= (b.QuantityCUM_B - c.TotalStockCUM_A) and (b.QuantityCUM_A - c.TotalStockCUM_A) <= 0 then a.MARD_EINME_5
				when (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A) or b.RowSeqNo = b.MaxRowSeqNo
				then case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
						else (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					 end
				else case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.QuantityCUM_A - c.TotalStockCUM_A)
						then case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.QuantityCUM_B - c.TotalStockCUM_A)
									then (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
										 - ((a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.QuantityCUM_A - c.TotalStockCUM_A))
									else 0
							end
						else (b.QuantityCUM_B - c.TotalStockCUM_A) - (b.QuantityCUM_A - c.TotalStockCUM_A)
					end
			end
		else 0
	end MARD_EINME_5,
	b.PostingDate,
	b.dim_DateIDPostingDate,
	b.LatestReceiptQty,
	b.LatestPostingDate,
	a.BatchNumber dd_BatchNumber,
	b.dd_ValuationType,
	b.Dim_StorageLocationid,
	b.dim_Companyid,
	b.dim_stocktypeid,
	b.dim_specialstockid,
	b.Dim_MovementIndicatorid,
	b.dim_CostCenterid,
	b.Dim_MovementTypeid,
	b.RowSeqNo,
	b.MaxRowSeqNo,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KSPEM ELSE 0 END MARD_KSPEM,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KINSM ELSE 0 END MARD_KINSM,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KEINM ELSE 0 END MARD_KEINM,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KLABS ELSE 0 END MARD_KLABS,
	CASE b.RowSeqNo WHEN 1 THEN MARD_RETME ELSE 0 END MARD_RETME,
	a.MARD_DLINL,
	a.MARD_ERSDA,
	ROW_NUMBER() OVER(PARTITION BY a.PartNumber, a.PlantCode,a.BatchNumber,a.StorLocCode
			  ORDER BY dt1.DateValue, b.dd_MaterialDocNo, b.dd_MaterialDocItemNo, b.dd_DocumentScheduleNo) MinRowSeqNo
FROM tmp_MARD_TotStk_001 a
	inner join materialmovement_tmp_new b on a.PartNumber = b.PartNumber
						and a.PlantCode = b.PlantCode
                         and a.BatchNumber=b.dd_BatchNumber
   inner join dim_storagelocation sl on sl.dim_storagelocationid=b.dim_storagelocationid and a.StorLocCode=sl.LocationCode
	inner join dim_date dt1 on b.dim_DateIDPostingDate = dt1.dim_dateid
	inner join tmp_MARD_TotStk_t02 c on c.PartNumber = a.PartNumber
						and c.PlantCode = a.PlantCode
						and c.StorLocCode = a.StorLocCode
						and c.BatchNumber = a.BatchNumber
WHERE ((b.QuantityCUM_A = 0 and c.TotalStockCUM_A = 0)
	or (b.QuantityCUM_A < c.TotalStockCUM_B and b.QuantityCUM_B > c.TotalStockCUM_A));


/*Insert 1 */

delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryaging_tmp_populate',ifnull(max(fact_inventoryagingid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
FROM fact_inventoryaging_tmp_populate;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid
	FROM dim_stockcategory stkc
	WHERE stkc.categorycode = 'MARD';

DROP TABLE IF EXISTS TMP_INSERT_1_STOCK;
CREATE TABLE TMP_INSERT_1_STOCK
AS
SELECT MARD_LABST_2 ct_StockQty,
	MARD_EINME_5 ct_TotalRestrictedStock,
	MARD_INSME_4 ct_StockInQInsp,
	MARD_SPEME_3 ct_BlockedStock,
	MARD_UMLME_1 ct_StockInTransfer,
	MARD_KSPEM ct_BlockedConsgnStock,
	MARD_KINSM ct_ConsgnStockInQInsp,
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock,
	MARD_RETME ct_BlockedStockReturns,
	IFNULL((CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END),0) amt_StockValueAmt,
	IFNULL((CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END),0) amt_StockValueAmt_GBL,
	IFNULL((CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END),0) amt_BlockedStockAmt,
	IFNULL((CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END),0)amt_BlockedStockAmt_GBL,
	IFNULL((CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END),0) amt_StockInQInspAmt,
	IFNULL((CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END),0) amt_StockInQInspAmt_GBL,
	IFNULL((CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END),0) amt_StockInTransferAmt,
	IFNULL((CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END),0) amt_StockInTransferAmt_GBL,
	IFNULL((CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_KLABS) END),0) amt_UnrestrictedConsgnStockAmt,
	IFNULL((CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_KLABS) END),0) amt_UnrestrictedConsgnStockAmt_GBL,
	IFNULL(b.multiplier,0) amt_StdUnitPrice,
	IFNULL(b.multiplier,0) amt_StdUnitPrice_GBL,
	IFNULL((CASE WHEN MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END
			  + CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END
			  + CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END
			  + CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END
			  + CASE WHEN MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_EINME_5) END)
	END),0) amt_OnHand,
	IFNULL((CASE WHEN MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_UMLME_1) END
			  + CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END
			  + CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END
			  + CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END
			  + CASE WHEN MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_EINME_5) END)
	END),0) amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) ct_LastReceivedQty,
	c1.dd_BatchNumber dd_BatchNo,
	ifnull(c1.dd_ValuationType,'Not Set') dd_ValuationType,
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo,
	dmt.MovementType dd_MovementType,
	c1.dim_DateIDPostingDate dim_StorageLocEntryDateid,
	convert(bigint,1) dim_LastReceivedDateid,
	dp.Dim_PartID,
	p.dim_plantid,
	sl.Dim_StorageLocationid,
	c.dim_Companyid,
	convert(bigint,1) dim_vendorid,
	dc.dim_currencyid,
	c1.dim_stocktypeid,
	convert(bigint,1) dim_specialstockid,
	convert(bigint,1) Dim_PurchaseOrgid,
	pg.Dim_PurchaseGroupid,
	dph.dim_producthierarchyid,
	uom.Dim_UnitOfMeasureid,
	c1.Dim_MovementIndicatorid,
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	convert(decimal (18,6),1) amt_ExchangeRate_GBL,
	convert(decimal (18,6),1) amt_ExchangeRate,
	dc.Dim_Currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001)),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,c1.LatestPostingDate /* LastReceivedDate */
	,p.CompanyCode /* LastReceivedDate */
	,b.PEINH as amt_priceunit
	,ifnull(b.MBEW_LBKUM,0) as ct_totvalstkQty
FROM tmp_fact_invaging_001 c1
	INNER JOIN dim_plant p ON c1.PlantCode = p.PlantCode
	INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = c1.PartNumber AND b.BWKEY = p.ValuationArea
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_part dp ON dp.PartNumber = c1.PartNumber AND dp.Plant = c1.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = c1.StorLocCode AND sl.Plant = c1.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
    INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
    INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
WHERE MARD_UMLME_1 + MARD_LABST_2 + MARD_SPEME_3 + MARD_INSME_4 + MARD_EINME_5 > 0;

MERGE INTO TMP_INSERT_1_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_1_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_UnrestrictedConsgnStockAmt_GBL = ST.amt_UnrestrictedConsgnStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockInTransferAmt_GBL = ST.amt_StockInTransferAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;

MERGE INTO TMP_INSERT_1_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_1_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_1_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(lrd.dim_dateid,1) dim_dateid
	 FROM TMP_INSERT_1_STOCK t
	    INNER JOIN dim_plant pl on pl.dim_plantid=t.dim_plantid
		LEFT JOIN dim_date lrd ON t.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = t.CompanyCode and pl.plantcode=lrd.plantcode_factory
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_LastReceivedDateid = SRC.dim_dateid;

INSERT INTO fact_inventoryaging_tmp_populate
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,amt_priceunit,ct_totvalstkQty)
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid,
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,amt_priceunit,ct_totvalstkQty
FROM TMP_INSERT_1_STOCK;

update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
where table_name = 'fact_inventoryaging_tmp_populate';

/* Changes in above query -
	a) Found out all columns where were not insert column list. Populate the default values ( according to earlier defn in MySQL )
	b) ifnull for c1.LatestReceiptQty,lrd.dim_dateid and c1.dd_ValuationType ( which were found after debugging errors )
	c) Fact row_number logic */
/* Above query works now */


/* INSERT 2 */

/* For now replaced getExchangeRate return value with 1 ( checked that tcurr does not have anything in fossil2 or the qa db on mysql
   For dbs which have data in this table, the function would need to be converted to SQLs.
   Already started that - refer getExchangeRate_func.sql */

drop table if exists tmp_tbl_StorageLocEntryDate;
CREATE table  tmp_tbl_StorageLocEntryDate AS
SELECT  c2.PartNumber, c2.PlantCode,c2.dd_BatchNumber,c2.LocationCode, MAX(c2.LatestPostingDate) as LatestPostingDate
FROM	 materialmovement_tmp_new_02 c2
GROUP BY c2.PartNumber, c2.PlantCode,c2.dd_BatchNumber,c2.LocationCode;

DROP TABLE IF EXISTS TMP_INSERT_2_STOCK;
CREATE TABLE TMP_INSERT_2_STOCK
AS
SELECT DISTINCT a.MARD_LABST_2 ct_StockQty,
	a.MARD_EINME_5 ct_TotalRestrictedStock,
	a.MARD_INSME_4 ct_StockInQInsp,
	a.MARD_SPEME_3 ct_BlockedStock,
	a.MARD_UMLME_1 ct_StockInTransfer,
	a.MARD_KSPEM ct_BlockedConsgnStock,
	a.MARD_KINSM ct_ConsgnStockInQInsp,
	a.MARD_KEINM ct_RestrictedConsgnStock,
	a.MARD_KLABS ct_UnrestrictedConsgnStock,
	a.MARD_RETME ct_BlockedStockReturns,
	ifnull((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END),0) amt_StockValueAmt,
	ifnull((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END),0) amt_StockValueAmt_GBL,
	ifnull((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END),0) amt_BlockedStockAmt,
	ifnull((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END),0) amt_BlockedStockAmt_GBL,
	ifnull((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END),0) amt_StockInQInspAmt,
	ifnull((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END),0) amt_StockInQInspAmt_GBL,
	ifnull((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END),0) amt_StockInTransferAmt,
	ifnull((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END),0) amt_StockInTransferAmt_GBL,
	ifnull((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END),0) amt_UnrestrictedConsgnStockAmt,
	ifnull((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END ),0) amt_UnrestrictedConsgnStockAmt_GBL,
	ifnull(b.multiplier,0) amt_StdUnitPrice,
	ifnull(b.multiplier,0) amt_StdUnitPrice_GBL,
	IFNULL((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END)
	END),0) amt_OnHand,
	IFNULL((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END)
		  END ),0) amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) ct_LastReceivedQty,
	a.BatchNumber dd_BatchNo,
	ifnull(c1.dd_ValuationType,'Not Set') dd_ValuationType,
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo,
	dmt.MovementType dd_MovementType,
	c1.dim_DateIDPostingDate dim_StorageLocEntryDateid,
	convert(bigint, 1) dim_LastReceivedDateid,
	Dim_Partid, p.dim_plantid, sl.Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	convert(bigint,1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid,
	c1.Dim_MovementIndicatorid,
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001)),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,c1.LatestPostingDate /* LastReceivedDate */
	,p.CompanyCode /* LastReceivedDate */
	,b.PEINH as amt_priceunit
	,ifnull(b.MBEW_LBKUM,0) as ct_totvalstkQty
FROM tmp_MARD_TotStk_001 a
	INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
	INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.PartNumber AND b.BWKEY = p.ValuationArea
	INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN tmp_fact_invaging_001 c1 ON a.PlantCode = c1.PlantCode AND a.PartNumber = c1.PartNumber and a.BatchNumber=c1.BatchNumber and a.storloccode=c1.StorLocCode  AND c1.MinRowSeqNo = 1
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode and p.PlantCode = sled.plantcode_factory
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and ifnull(c2.dd_BatchNo,'xxx') = ifnull(a.BatchNumber,'yyy'));

MERGE INTO TMP_INSERT_2_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_2_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_UnrestrictedConsgnStockAmt_GBL = ST.amt_UnrestrictedConsgnStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockInTransferAmt_GBL = ST.amt_StockInTransferAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;

MERGE INTO TMP_INSERT_2_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_2_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_2_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(lrd.dim_dateid,1) dim_dateid
	 FROM TMP_INSERT_2_STOCK t
	 inner join dim_plant p on t.dim_plantid=p.dim_plantid
		LEFT JOIN dim_date lrd ON t.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = t.CompanyCode and p.plantcode=lrd.plantcode_factory
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_LastReceivedDateid = SRC.dim_dateid;

INSERT INTO fact_inventoryaging_tmp_populate
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,amt_priceunit,ct_totvalstkQty)
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid,
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,amt_priceunit,ct_totvalstkQty
FROM TMP_INSERT_2_STOCK;

/* INSERT 3 - No MBEW */

update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
where table_name = 'fact_inventoryaging_tmp_populate';

DROP TABLE IF EXISTS TMP_INSERT_3_STOCK;
CREATE TABLE TMP_INSERT_3_STOCK
AS
SELECT DISTINCT a.MARD_LABST_2 ct_StockQty,
	a.MARD_EINME_5 ct_TotalRestrictedStock,
	a.MARD_INSME_4 ct_StockInQInsp,
	a.MARD_SPEME_3 ct_BlockedStock,
	a.MARD_UMLME_1 ct_StockInTransfer,
	a.MARD_KSPEM ct_BlockedConsgnStock,
	a.MARD_KINSM ct_ConsgnStockInQInsp,
	a.MARD_KEINM ct_RestrictedConsgnStock,
	a.MARD_KLABS ct_UnrestrictedConsgnStock,
	a.MARD_RETME ct_BlockedStockReturns,
	ifnull((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END), 0) amt_StockValueAmt,
	ifnull((CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END), 0) amt_StockValueAmt_GBL,
	ifnull((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END), 0) amt_BlockedStockAmt,
	ifnull((CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END), 0) amt_BlockedStockAmt_GBL,
	ifnull((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END), 0) amt_StockInQInspAmt,
	ifnull((CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END), 0) amt_StockInQInspAmt_GBL,
	ifnull((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END), 0) amt_StockInTransferAmt,
	ifnull((CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END), 0) amt_StockInTransferAmt_GBL,
	ifnull((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END), 0) amt_UnrestrictedConsgnStockAmt,
	ifnull((CASE WHEN a.MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END), 0) amt_UnrestrictedConsgnStockAmt_GBL,
	ifnull((b.multiplier), 0) amt_StdUnitPrice,
	ifnull((b.multiplier), 0) amt_StdUnitPrice_GBL,
	ifnull((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END)
	END),0) amt_OnHand,
	ifnull((CASE WHEN a.MARD_UMLME_1 + a.MARD_LABST_2 + a.MARD_SPEME_3 + a.MARD_INSME_4 + a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3
		ELSE (CASE WHEN a.MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME_1) END
			  + CASE WHEN a.MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST_2) END
			  + CASE WHEN a.MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME_3) END
			  + CASE WHEN a.MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME_4) END
			  + CASE WHEN a.MARD_EINME_5 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_EINME_5) END)
		  END ), 0) amt_OnHand_GBL,
	0 ct_LastReceivedQty,
	a.BatchNumber dd_BatchNo,
	a.StorLocCode StorLocCode,
	'Not Set' dd_ValuationType,
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo,
	'Not Set' dd_MovementType,
	convert(bigint,1) dim_StorageLocEntryDateid,
	1 dim_LastReceivedDateid,
	Dim_Partid, p.dim_plantid, sl.Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	convert(bigint, 1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid,
	1 Dim_MovementIndicatorid,
	1 dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL,amt_OnHand_GBL,amt_StdUnitPrice_GBL,amt_UnrestrictedConsgnStockAmt_GBL,amt_StockInTransferAmt_GBL,amt_BlockedStockAmt_GBL,amt_StockValueAmt_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,p.CompanyCode /*StorageLocEntryDate*/
	,a.PartNumber /*StorageLocEntryDate*/
	,sled.Dim_Dateid /*StorageLocEntryDate*/
	,a.PlantCode /*StorageLocEntryDate*/
	,b.PEINH as amt_priceunit
	,ifnull(b.MBEW_LBKUM,0) as ct_totvalstkQty
FROM tmp_MARD_TotStk_001 a
	INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
	INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.PartNumber AND b.BWKEY = p.ValuationArea
	INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode and p.plantcode=sled.plantcode_factory
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
    WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and c2.dd_BatchNo = a.BatchNumber);

MERGE INTO TMP_INSERT_3_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_3_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_UnrestrictedConsgnStockAmt_GBL = ST.amt_UnrestrictedConsgnStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockInTransferAmt_GBL = ST.amt_StockInTransferAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;

MERGE INTO TMP_INSERT_3_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_3_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;



MERGE INTO TMP_INSERT_3_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, lrd.Dim_Dateid
	FROM TMP_INSERT_3_STOCK t
		LEFT JOIN tmp_tbl_StorageLocEntryDate c2 ON c2.PartNumber = t.PartNumber AND c2.PlantCode = t.PlantCode and c2.dd_batchnumber=t.dd_BatchNo and c2.LocationCode=t.StorLocCode
		LEFT JOIN dim_date lrd on lrd.DateValue = c2.LatestPostingDate and lrd.CompanyCode = t.CompanyCode and c2.PlantCode=lrd.plantcode_factory
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_StorageLocEntryDateid = ifnull(SRC.Dim_Dateid,ST.Dim_Dateid);

INSERT INTO fact_inventoryaging_tmp_populate
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,amt_priceunit,ct_totvalstkQty)
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid,
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,amt_priceunit,ct_totvalstkQty
FROM TMP_INSERT_3_STOCK;

/* INSERT 4 - No MBEW */

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
 where table_name = 'fact_inventoryaging_tmp_populate';


DROP TABLE IF EXISTS TMP_INSERT_4_STOCK;
CREATE TABLE TMP_INSERT_4_STOCK
AS
SELECT MARD_LABST_2 ct_StockQty,
	MARD_EINME_5 ct_TotalRestrictedStock,
	MARD_INSME_4 ct_StockInQInsp,
	MARD_SPEME_3 ct_BlockedStock,
	MARD_UMLME_1 ct_StockInTransfer,
	MARD_KSPEM ct_BlockedConsgnStock,
	MARD_KINSM ct_ConsgnStockInQInsp,
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock,
	MARD_RETME ct_BlockedStockReturns,
	0 amt_StockValueAmt,
	0 amt_StockValueAmt_GBL,
	0 amt_BlockedStockAmt,
	0 amt_BlockedStockAmt_GBL,
	0 amt_StockInQInspAmt,
	0 amt_StockInQInspAmt_GBL,
	0 amt_StockInTransferAmt,
	0 amt_StockInTransferAmt_GBL,
	0 amt_UnrestrictedConsgnStockAmt,
	0 amt_UnrestrictedConsgnStockAmt_GBL,
	0 amt_StdUnitPrice,
	0 amt_StdUnitPrice_GBL,
	0 amt_OnHand,
	0 amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) ct_LastReceivedQty,
	c1.dd_BatchNumber dd_BatchNo,
	c1.StorLocCode StorLocCode,
	ifnull(c1.dd_ValuationType,'Not Set') dd_ValuationType,
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo,
	dmt.MovementType dd_MovementType,
	c1.dim_DateIDPostingDate dim_StorageLocEntryDateid,
	convert(bigint, 1) dim_LastReceivedDateid,
	dp.Dim_PartID,
	p.dim_plantid,
	sl.Dim_StorageLocationid,
	c.dim_Companyid,
	1 dim_vendorid,
	dc.dim_currencyid,
	c1.dim_stocktypeid,
	1 dim_specialstockid,
	convert(bigint, 1) Dim_PurchaseOrgid,
	pg.Dim_PurchaseGroupid,
	dph.dim_producthierarchyid,
	uom.Dim_UnitOfMeasureid,
	c1.Dim_MovementIndicatorid,
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
	1 amt_ExchangeRate,
	dc.Dim_Currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001)),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,c1.LatestPostingDate /* LastReceivedDate */
	,p.CompanyCode /* LastReceivedDate */
FROM tmp_fact_invaging_001 c1
	INNER JOIN dim_plant p ON c1.PlantCode = p.PlantCode
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_part dp ON dp.PartNumber = c1.PartNumber AND dp.Plant = c1.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = c1.StorLocCode AND sl.Plant = c1.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2
		WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid
				and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
				and ifnull(c2.dd_BatchNo,'xxx') = ifnull(c1.dd_BatchNumber,'yyy'));

MERGE INTO TMP_INSERT_4_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_4_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;

MERGE INTO TMP_INSERT_4_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_4_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_4_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(lrd.dim_dateid,1) dim_dateid
	 FROM TMP_INSERT_4_STOCK t
	 INNER JOIN dim_plant p on t.dim_plantid=p.dim_plantid
		LEFT JOIN dim_date lrd ON t.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = t.CompanyCode and p.plantcode=lrd.plantcode_factory
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_LastReceivedDateid = SRC.dim_dateid;

INSERT INTO fact_inventoryaging_tmp_populate
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid,
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_4_STOCK;

/* INSERT 5 - No MBEW - No Movement TYpe*/

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
 where table_name = 'fact_inventoryaging_tmp_populate';

DROP TABLE IF EXISTS TMP_INSERT_5_STOCK;
CREATE TABLE TMP_INSERT_5_STOCK
AS
SELECT DISTINCT MARD_LABST_2 ct_StockQty,
	MARD_EINME_5 ct_TotalRestrictedStock,
	MARD_INSME_4 ct_StockInQInsp,
	MARD_SPEME_3 ct_BlockedStock,
	MARD_UMLME_1 ct_StockInTransfer,
	MARD_KSPEM ct_BlockedConsgnStock,
	MARD_KINSM ct_ConsgnStockInQInsp,
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock,
	MARD_RETME ct_BlockedStockReturns,
	0 amt_StockValueAmt,
	0 amt_StockValueAmt_GBL,
	0 amt_BlockedStockAmt,
	0 amt_BlockedStockAmt_GBL,
	0 amt_StockInQInspAmt,
	0 amt_StockInQInspAmt_GBL,
	0 amt_StockInTransferAmt,
	0 amt_StockInTransferAmt_GBL,
	0 amt_UnrestrictedConsgnStockAmt,
	0 amt_UnrestrictedConsgnStockAmt_GBL,
	0 amt_StdUnitPrice,
	0 amt_StdUnitPrice_GBL,
	0 amt_OnHand,
	0 amt_OnHand_GBL,
	0 ct_LastReceivedQty,
	a.BatchNumber dd_BatchNo,
	a.StorLocCode StorLocCode,
	'Not Set' dd_ValuationType,
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo,
	'Not Set' dd_MovementType,
	convert(bigint,1) dim_StorageLocEntryDateid,
	1 dim_LastReceivedDateid,
	Dim_Partid, p.dim_plantid, Dim_StorageLocationid, c.dim_companyid, 1 dim_vendorid,
	dc.dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	convert(bigint, 1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid, 1 Dim_MovementIndicatorid, 1 dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,	/* This is local to global ( Equivalent to Tran to Global ) */
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
	,p.PurchOrg /* Dim_PurchaseOrgid */
	,p.CompanyCode /*StorageLocEntryDate*/
	,a.PartNumber /*StorageLocEntryDate*/
	,sled.Dim_Dateid /*StorageLocEntryDate*/
	,a.PlantCode /*StorageLocEntryDate*/
FROM tmp_MARD_TotStk_001 a
	INNER JOIN dim_plant p ON a.PlantCode = p.PlantCode
	INNER JOIN dim_part dp ON dp.PartNumber = a.PartNumber AND dp.Plant = a.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.StorLocCode AND sl.Plant = a.PlantCode AND sl.RowIsCurrent = 1
	INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode and p.plantcode=sled.plantcode_factory
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON convert(varchar(10),dc.CurrencyCode) = convert(varchar(10),c.Currency)
	CROSS JOIN tmp_GlobalCurr_001 gbl
	CROSS JOIN tmp_stkcategory_001 stkc
WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid
					and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid
					and ifnull(c2.dd_BatchNo,'xxx') = ifnull(a.BatchNumber,'yyy'));

MERGE INTO TMP_INSERT_5_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_5_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;

MERGE INTO TMP_INSERT_5_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_5_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_5_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, lrd.Dim_Dateid
	FROM TMP_INSERT_5_STOCK t
		LEFT JOIN tmp_tbl_StorageLocEntryDate c2 ON c2.PartNumber = t.PartNumber AND c2.PlantCode = t.PlantCode and c2.dd_batchnumber=dd_batchno and c2.LocationCode=t.StorLocCode
		LEFT JOIN dim_date lrd on lrd.DateValue = c2.LatestPostingDate and lrd.CompanyCode = t.CompanyCode and t.plantcode=lrd.plantcode_factory
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_StorageLocEntryDateid = ifnull(SRC.Dim_Dateid,ST.Dim_Dateid);

INSERT INTO fact_inventoryaging_tmp_populate
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid,
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid,
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid,
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_5_STOCK;

 UPDATE NUMBER_FOUNTAIN
 SET max_id = ifnull((select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 WHERE table_name = 'fact_inventoryaging_tmp_populate';

/***********************LK: 26 Apr change : Insert queries 7/8/9/10/11 Starts ***********************/

/*Insert 7 */

/* Temporary tables to be used in inserts 7 to 11 */

/* LastReceivedQty */

/* Store mm and datevalue in a tmp table */

DROP TABLE IF EXISTS TMP_fact_mm_0;
CREATE TABLE TMP_fact_mm_0
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'O';

/* Only retrive the min dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_1;
CREATE TABLE TMP_fact_mm_1
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,min(mm.DateValue) as min_DateValue
from TMP_fact_mm_0 mm
GROUP by mm.Dim_Partid,mm.Dim_Vendorid;

/* Now get the ct_QtyEntryUOM corresponding to the min datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_2;
CREATE TABLE TMP_fact_mm_2
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,mm.ct_Quantity ct_QtyEntryUOM
FROM TMP_fact_mm_0 mm, TMP_fact_mm_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.min_DateValue;


/* StorageLocEntryDate/LastReceivedDate */

/* Simply use the posting date id for the min/max date */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where  mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'O';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm
GROUP by mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT distinct mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT distinct mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.min_DateValue;

DROP TABLE IF EXISTS fitp_tmp_p01;
CREATE TABLE fitp_tmp_p01 LIKE fact_inventoryaging_tmp_populate INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE fitp_tmp_p01 ADD PRIMARY KEY (fact_inventoryagingid);
ALTER TABLE fitp_tmp_p01 ADD COLUMN Dim_Partid_upd BIGINT DEFAULT 0;
ALTER TABLE fitp_tmp_p01 ADD COLUMN Dim_Vendorid_upd BIGINT DEFAULT 0;
ALTER TABLE fitp_tmp_p01 ADD COLUMN PurchOrg_upd varchar(20);
ALTER TABLE fitp_tmp_p01 ADD COLUMN CurrencyCode varchar(20) default 'Not Set';
ALTER TABLE fitp_tmp_p01 ADD COLUMN MSLB_SOBKZ varchar(20) default 'Not Set';
ALTER TABLE fitp_tmp_p01 ADD COLUMN pGlobalCurrency varchar(20) default 'Not Set';

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid
	FROM dim_stockcategory stkc
	WHERE stkc.categorycode = 'MSLB';

INSERT INTO fitp_tmp_p01(ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
				fact_inventoryagingid,
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL,
				Dim_Partid_upd,
				Dim_Vendorid_upd,
				PurchOrg_upd,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,
				CurrencyCode,pGlobalCurrency,MSLB_SOBKZ, amt_priceunit, ct_totvalstkQty)

 SELECT  a.MSLB_LBLAB ct_StockQty,
	a.MSLB_LBEIN ct_TotalRestrictedStock,
	a.MSLB_LBINS ct_StockInQInsp,
	ifnull((CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END), 0)  StockValueAmt,
	ifnull((CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END), 0)  StockValueAmt_GBL,
    ifnull((b.multiplier * a.MSLB_LBINS), 0) amt_StockInQInspAmt,
	ifnull((b.multiplier * a.MSLB_LBINS), 0)  amt_StockInQInspAmt_GBL,
    ifnull((b.multiplier), 0) amt_StdUnitPrice,
	ifnull((b.multiplier), 0) amt_StdUnitPrice_GBL,
	ifnull((CASE WHEN MSLB_LBLAB+MSLB_LBINS+MSLB_LBEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END
			+ CASE WHEN MSLB_LBINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBINS) END
			+ CASE WHEN MSLB_LBEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBEIN) END
		  ) END),0) amt_OnHand,
	ifnull((CASE WHEN MSLB_LBLAB+MSLB_LBINS+MSLB_LBEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END
			+ CASE WHEN MSLB_LBINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBINS) END
			+ CASE WHEN MSLB_LBEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBEIN) END
		  ) END), 0) amt_OnHand_GBL,
	1 LastReceivedQty,
	a.MSLB_CHARG BatchNo,
	'Not Set' dd_DocumentNo,
	0 dd_DocumentItemNo,
	1 StorageLocEntryDate,
	1 LastReceivedDate,
	Dim_Partid,
	p.dim_plantid,
	1 Dim_StorageLocationid,
	dim_companyid,
	v.dim_vendorid,
	dim_currencyid,
	1 dim_stocktypeid,
    convert(bigint,1) dim_specialstockid,
	convert(bigint,1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	1 Dim_MovementIndicatorid,
	1 dim_CostCenterid,
	stkc.dim_StockCategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (order by '') fact_inventoryagingid, /* table name is correct here do not change it to fitp_tmp_p01 */
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
	dp.Dim_Partid Dim_Partid_upd,
	v.Dim_Vendorid Dim_Vendorid_upd,
	p.PurchOrg PurchOrg_upd,
	1 amt_ExchangeRate,	/* As Tran and Local are same */
	dc.dim_currencyid dim_Currencyid_TRA,
	ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode =  (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
	,dc.CurrencyCode /* amt_ExchangeRate_GBL */
	,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
	,a.MSLB_SOBKZ /* dim_specialstockid */
	,b.PEINH as amt_priceunit
	,ifnull(b.MBEW_LBKUM,0) as ct_totvalstkQty
     FROM MSLB a
          INNER JOIN dim_plant p ON a.MSLB_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSLB_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSLB_MATNR AND dp.Plant = a.MSLB_WERKS
          INNER JOIN dim_vendor v ON v.VendorNumber = MSLB_LIFNR
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
          CROSS JOIN tmp_stkcategory_001  stkc
	    WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid
					and c2.dd_BatchNo = ifnull(a.MSLB_CHARG,'Not Set'));

MERGE INTO fitp_tmp_p01 ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM fitp_tmp_p01 t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_UnrestrictedConsgnStockAmt_GBL = ST.amt_UnrestrictedConsgnStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockInTransferAmt_GBL = ST.amt_StockInTransferAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;

MERGE INTO fitp_tmp_p01 FA
USING ( SELECT fact_inventoryagingid, IFNULL(mm.dim_specialstockid,1) dim_specialstockid
		FROM fitp_tmp_p01 T1
				LEFT JOIN dim_specialstock mm on mm.specialstockindicator = T1.MSLB_SOBKZ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_specialstockid = SRC.dim_specialstockid;

Update fitp_tmp_p01 fitp
Set dim_StorageLocEntryDateid = mm.dim_DateIDPostingDate
from
fitp_tmp_p01 fitp,
TMP_fact_mm_postingdate_2_min mm /* This has min date */
WHERE
mm.Dim_Partid = fitp.Dim_Partid_upd
and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd ;

update fitp_tmp_p01 fitp
set dim_StorageLocEntryDateId = 1
where dim_StorageLocEntryDateid is null;


Update fitp_tmp_p01 fitp
Set
dim_LastReceivedDateid = mm.dim_DateIDPostingDate
from fitp_tmp_p01 fitp,
(SELECT DISTINCT x.* FROM TMP_fact_mm_postingdate_2 x) mm     /* This has max date */
WHERE
mm.Dim_Partid = fitp.Dim_Partid_upd
and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd;

Update fitp_tmp_p01 fitp
SET dim_LastReceivedDateid = 1
WHERE dim_LastReceivedDateid IS NULL;

DROP TABLE IF EXISTS TMP_UPDT_TMP_fact_mm_2;
CREATE TABLE TMP_UPDT_TMP_fact_mm_2
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,sum(mm.ct_QtyEntryUOM) ct_QtyEntryUOM
FROM TMP_fact_mm_2 mm
GROUP BY mm.Dim_Partid,mm.Dim_Vendorid;

Update fitp_tmp_p01 fitp
Set ct_LastReceivedQty =  mm.ct_QtyEntryUOM
from fitp_tmp_p01 fitp,TMP_UPDT_TMP_fact_mm_2 mm
where
mm.Dim_Partid = fitp.Dim_Partid_upd
and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd;

Update fitp_tmp_p01 fitp
Set Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
from fitp_tmp_p01 fitp,dim_purchaseorg po
where po.PurchaseOrgCode = fitp.PurchOrg_upd;

Update fitp_tmp_p01 fitp
Set Dim_PurchaseOrgid = 1
WHERE  Dim_PurchaseOrgid is null;

Insert into fact_inventoryaging_tmp_populate(ct_StockQty,
		ct_TotalRestrictedStock,
		ct_StockInQInsp,
		amt_StockValueAmt,
		amt_StockValueAmt_GBL,
		amt_StockInQInspAmt,
		amt_StockInQInspAmt_GBL,
		amt_StdUnitPrice,
		amt_StdUnitPrice_GBL,
		amt_OnHand,
		amt_OnHand_GBL,
		ct_LastReceivedQty,
		dd_BatchNo,
		dd_DocumentNo,
		dd_DocumentItemNo,
		dim_StorageLocEntryDateid,
		dim_LastReceivedDateid,
		dim_Partid,
		dim_Plantid,
		dim_StorageLocationid,
		dim_Companyid,
		dim_Vendorid,
		dim_Currencyid,
		dim_StockTypeid,
		dim_specialstockid,
		Dim_PurchaseOrgid,
		Dim_PurchaseGroupid,
		dim_producthierarchyid,
		Dim_UnitOfMeasureid,
		Dim_MovementIndicatorid,
		dim_CostCenterid,
		dim_stockcategoryid,
		fact_inventoryagingid,
		Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
		Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
		amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
		amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
		amt_ExchangeRate_GBL,
		amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL, amt_priceunit, ct_totvalstkQty)
Select ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL, amt_priceunit, ct_totvalstkQty
from fitp_tmp_p01;

drop table if exists fitp_tmp_p01;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';


/*Insert 8 */
/* -- Vendor Stocks with no valuation	*/
DROP TABLE IF EXISTS TMP_INSERT_8_STOCK;
CREATE TABLE TMP_INSERT_8_STOCK
AS
  SELECT  a.MSLB_LBLAB ct_StockQty,
          a.MSLB_LBEIN ct_TotalRestrictedStock,
          a.MSLB_LBINS ct_StockInQInsp,
          0 amt_StockValueAmt,
          0 amt_StockValueAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
		  0 amt_OnHand,
		  0 amt_OnHand_GBL,
         convert(decimal (18,4), 0) ct_LastReceivedQty,
          a.MSLB_CHARG dd_BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          convert(bigint, 1) dim_StorageLocEntryDateid,
          convert(bigint, 1) dim_LastReceivedDateid,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          v.dim_vendorid,
          dim_currencyid,
          1 dim_stocktypeid,
          convert(bigint, 1) dim_specialstockid,
          convert(bigint, 1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_StockCategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (order by '') fact_inventoryagingid,
	  convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
		,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		,p.PurchOrg /* Dim_PurchaseOrgid */
		,a.MSLB_SOBKZ /* dim_specialstockid */
     FROM MSLB a
          INNER JOIN dim_plant p
             ON a.MSLB_WERKS = p.PlantCode
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSLB_MATNR AND dp.Plant = a.MSLB_WERKS
          INNER JOIN dim_vendor v
             ON v.VendorNumber = MSLB_LIFNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
          CROSS JOIN tmp_stkcategory_001  stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_inventoryaging_tmp_populate c2
           WHERE c2.Dim_Partid = dp.Dim_Partid
                AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_Vendorid = v.Dim_Vendorid and c2.dd_BatchNo = ifnull(a.MSLB_CHARG,'Not Set'));

MERGE INTO TMP_INSERT_8_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_8_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;

MERGE INTO TMP_INSERT_8_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_8_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_8_STOCK FA
USING ( SELECT fact_inventoryagingid, IFNULL(mm.dim_specialstockid,1) dim_specialstockid
		FROM TMP_INSERT_8_STOCK T1
				LEFT JOIN dim_specialstock mm on mm.specialstockindicator = T1.MSLB_SOBKZ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_specialstockid = SRC.dim_specialstockid;

UPDATE TMP_INSERT_8_STOCK fitp
SET dim_StorageLocEntryDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_8_STOCK fitp, TMP_fact_mm_postingdate_2_min mm /* This has min date */
WHERE mm.Dim_Partid = fitp.Dim_Partid AND mm.Dim_Vendorid = fitp.Dim_Vendorid ;

/* September 16 2016 - OSTOIAN - Unable to get a stable set of rows fix */
/*
Update TMP_INSERT_8_STOCK fitp
SET dim_LastReceivedDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_8_STOCK fitp, TMP_fact_mm_postingdate_2 mm     *//* This has max date *//*
WHERE mm.Dim_Partid = fitp.Dim_Partid AND mm.Dim_Vendorid = fitp.Dim_Vendorid
*/

Update TMP_INSERT_8_STOCK fitp
SET dim_LastReceivedDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_8_STOCK fitp, ( select distinct dim_partid,dim_vendorid,dim_dateidpostingdate,dim_specialstockid from TMP_fact_mm_postingdate_2) mm /* This has max date */
WHERE mm.Dim_Partid = fitp.Dim_Partid AND mm.Dim_Vendorid = fitp.Dim_Vendorid
AND dim_LastReceivedDateid <> mm.dim_DateIDPostingDate;

/* End of changes September 16 2016 - OSTOIAN*/


UPDATE TMP_INSERT_8_STOCK fitp
SET ct_LastReceivedQty =  mm.ct_QtyEntryUOM
FROM TMP_INSERT_8_STOCK fitp,TMP_UPDT_TMP_fact_mm_2 mm
WHERE mm.Dim_Partid = fitp.Dim_Partid AND mm.Dim_Vendorid = fitp.Dim_Vendorid;

INSERT INTO fact_inventoryaging_tmp_populate
	(ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_8_STOCK;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

/*Insert 9 */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'W';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm
GROUP by mm.Dim_Partid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate ,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.min_DateValue;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid
	FROM dim_stockcategory stkc
	WHERE stkc.categorycode = 'MSKU';

DROP TABLE IF EXISTS TMP_INSERT_9_STOCK;
CREATE TABLE TMP_INSERT_9_STOCK
AS
  SELECT  a.MSKU_KULAB ct_StockQty,
          a.MSKU_KUEIN ct_TotalRestrictedStock,
          a.MSKU_KUINS ct_StockInQInsp,
          ifnull((CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3
               ELSE (b.multiplier * a.MSKU_KULAB)
          END), 0) amt_StockValueAmt,
          ifnull((CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3
               ELSE (b.multiplier * a.MSKU_KULAB)
          END), 0) amt_StockValueAmt_GBL,
          ifnull((b.multiplier * a.MSKU_KUINS), 0) amt_StockInQInspAmt,
          ifnull((b.multiplier * a.MSKU_KUINS), 0) amt_StockInQInspAmt_GBL,
          ifnull(b.multiplier, 0) amt_StdUnitPrice,
          ifnull(b.multiplier, 0) amt_StdUnitPrice_GBL,
	ifnull((CASE WHEN MSKU_KULAB+MSKU_KUINS+MSKU_KUEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KULAB) END
			+ CASE WHEN MSKU_KUINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUINS) END
			+ CASE WHEN MSKU_KUEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUEIN) END
		  ) END ), 0) amt_OnHand,
	ifnull((CASE WHEN MSKU_KULAB+MSKU_KUINS+MSKU_KUEIN = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KULAB) END
			+ CASE WHEN MSKU_KUINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUINS) END
			+ CASE WHEN MSKU_KUEIN = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUEIN) END
		  ) END ), 0) amt_OnHand_GBL,
          0.00 ct_LastReceivedQty,
          a.MSKU_CHARG dd_BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          convert(bigint, 1) dim_StorageLocEntryDateid,
          convert(bigint, 1) dim_LastReceivedDateid,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          cm.dim_customerid,
          dim_currencyid,
          1 dim_stocktypeid,
          convert(bigint, 1) dim_specialstockid,
          convert(bigint, 1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
		(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (order by '') fact_inventoryagingid,
		convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
		,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		,p.PurchOrg /* Dim_PurchaseOrgid */
		,a.MSKU_SOBKZ /* dim_specialstockid */
		,b.PEINH as amt_priceunit
		,ifnull(b.MBEW_LBKUM,0) as ct_totvalstkQty
     FROM MSKU a
          INNER JOIN dim_plant p
             ON a.MSKU_WERKS = p.PlantCode
         INNER JOIN tmp2_MBEW_NO_BWTAR b ON     b.MATNR = a.MSKU_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKU_MATNR AND dp.Plant = a.MSKU_WERKS
          INNER JOIN dim_customer cm
             ON cm.CustomerNumber = MSKU_KUNNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
	      CROSS JOIN tmp_stkcategory_001 stkc
	    WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2
			WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid
					/*and c2.Dim_StorageLocationid = sl.Dim_StorageLocationid*/
					and c2.dd_BatchNo = ifnull(a.MSKU_CHARG,'Not Set'));

MERGE INTO TMP_INSERT_9_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_9_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_OnHand_GBL = ST.amt_OnHand_GBL * SRC.exchangeRate,
		ST.amt_StdUnitPrice_GBL = ST.amt_StdUnitPrice_GBL * SRC.exchangeRate,
		ST.amt_StockInQInspAmt_GBL = ST.amt_StockInQInspAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;

MERGE INTO TMP_INSERT_9_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_9_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_9_STOCK FA
USING ( SELECT fact_inventoryagingid, IFNULL(mm.dim_specialstockid,1) dim_specialstockid
		FROM TMP_INSERT_9_STOCK T1
				LEFT JOIN dim_specialstock mm on mm.specialstockindicator = T1.MSKU_SOBKZ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_specialstockid = SRC.dim_specialstockid;

UPDATE TMP_INSERT_9_STOCK fitp
SET dim_StorageLocEntryDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_9_STOCK fitp, (SELECT DISTINCT x.* FROM TMP_fact_mm_postingdate_2_min x) mm /* This has min date */
WHERE mm.Dim_Partid = fitp.Dim_Partid;

Update TMP_INSERT_9_STOCK fitp
SET dim_LastReceivedDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_9_STOCK fitp, (SELECT DISTINCT x.* FROM TMP_fact_mm_postingdate_2 x) mm     /* This has max date */
WHERE mm.Dim_Partid = fitp.Dim_Partid;

/* -- Customer stocks with no valuation	*/
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Customerid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,amt_priceunit,ct_totvalstkQty)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Customerid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,amt_priceunit,ct_totvalstkQty
FROM TMP_INSERT_9_STOCK;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

/*Insert 10 */
/* -- Customer stocks with no valuation	*/
DROP TABLE IF EXISTS TMP_INSERT_10_STOCK;
CREATE TABLE TMP_INSERT_10_STOCK
AS
  SELECT  a.MSKU_KULAB ct_StockQty,
          a.MSKU_KUEIN ct_TotalRestrictedStock,
          a.MSKU_KUINS ct_StockInQInsp,
          0 amt_StockValueAmt,
          0 amt_StockValueAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          0 amt_OnHand,
          0 amt_OnHand_GBL,
          0.00 ct_LastReceivedQty,
          a.MSKU_CHARG dd_BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          convert(bigint, 1) dim_StorageLocEntryDateid,
          convert(bigint, 1) dim_LastReceivedDateid,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          cm.dim_customerid,
          dim_currencyid,
          1 dim_stocktypeid,
          convert(bigint, 1)  dim_specialstockid,
          convert(bigint, 1)  Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
		(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (order by '') fact_inventoryagingid,
		convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,	/* As Tran and Local are same */
		dc.dim_currencyid dim_Currencyid_TRA,
		ifnull((select c.dim_currencyid from dim_Currency c where c.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ),1) dim_Currencyid_GBL
		,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		,p.PurchOrg /* Dim_PurchaseOrgid */
		,a.MSKU_SOBKZ /* dim_specialstockid */
     FROM MSKU a
          INNER JOIN dim_plant p
             ON a.MSKU_WERKS = p.PlantCode
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKU_MATNR AND dp.Plant = a.MSKU_WERKS
          INNER JOIN dim_customer cm
             ON cm.CustomerNumber = MSKU_KUNNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
		  CROSS JOIN tmp_stkcategory_001 stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_inventoryaging_tmp_populate c2
           WHERE c2.Dim_Partid = dp.Dim_Partid
                AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_CustomerId = cm.Dim_CustomerId
				and c2.dd_batchno=ifnull(a.MSKU_CHARG,'Not Set'));

MERGE INTO TMP_INSERT_10_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_10_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;

MERGE INTO TMP_INSERT_10_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_10_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_10_STOCK FA
USING ( SELECT fact_inventoryagingid, IFNULL(mm.dim_specialstockid,1) dim_specialstockid
		FROM TMP_INSERT_10_STOCK T1
				LEFT JOIN dim_specialstock mm on mm.specialstockindicator = T1.MSKU_SOBKZ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_specialstockid = SRC.dim_specialstockid;

UPDATE TMP_INSERT_10_STOCK fitp
SET dim_StorageLocEntryDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_10_STOCK fitp, (SELECT DISTINCT x.* FROM TMP_fact_mm_postingdate_2_min x) mm /* This has min date */
WHERE mm.Dim_Partid = fitp.Dim_Partid;

Update TMP_INSERT_10_STOCK fitp
SET dim_LastReceivedDateid = mm.dim_DateIDPostingDate
FROM TMP_INSERT_10_STOCK fitp, (SELECT DISTINCT x.* FROM TMP_fact_mm_postingdate_2_min x) mm     /* This has max date */
WHERE mm.Dim_Partid = fitp.Dim_Partid;

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Customerid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Customerid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL
FROM TMP_INSERT_10_STOCK;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
						where table_name = 'fact_inventoryaging_tmp_populate';

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.Dim_Partid, mm.Dim_StorageLocationid, mmdt.Dim_Dateid,
	row_number() over(partition by mm.Dim_Partid, mm.Dim_StorageLocationid order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate;

UPDATE
fact_inventoryaging_tmp_populate f
SET f.dim_dateidtransaction = IFNULL(mmdt.Dim_Dateid,1)
FROM
fact_inventoryaging_tmp_populate f
		LEFT JOIN tmp_custom_postproc_dim_date mmdt
		ON mmdt.Dim_Partid = f.dim_Partid
		and mmdt.Dim_StorageLocationid = f.dim_StorageLocationid
		and mmdt.RowSeqNo = 1
WHERE f.dim_stockcategoryid in (2,3);

/*Insert 11 */

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = '641';

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid
	FROM dim_stockcategory stkc
	WHERE stkc.categorycode = 'MARC';



/* LK: 12 Sep - Done till here. Do fact_purchase before coming back here */

/* # INTRA stock analytics */

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (fp.ct_QtyIssued - (CASE WHEN fp.ct_ReceivedQty < 0 THEN -1*fp.ct_ReceivedQty ELSE fp.ct_ReceivedQty END))
		* (fp.PriceConversion_EqualTo / fp.PriceConversion_Denom) ct_StockInTransit,
          fp.dd_DocumentNo,
          fp.dd_DocumentItemNo,
          fp.Dim_Partid,
          fp.dim_currencyid,	/* Local currency */
		  fp.Dim_currencyid Dim_CurrencyId_TRA,		/* inventoryaging should have tran curr same as local curr */
		  fp.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,
		  1 amt_ExchangeRate,	/* Tran->Local exch rate is 1 */
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          fp.Dim_PlantidSupplying,
	  (fp.amt_ExchangeRate_GBL/case when fp.amt_ExchangeRate = 0 THEN 1 ELSE fp.amt_ExchangeRate END) amt_ExchangeRate_GBL,
	  /*LK: amt_ExchangeRate_GBL : (Tran to Global) * (Local to Tran) = (Tran to Global)/(Tran to Local)  = Local to Global */
	  (fp.amt_UnitPrice * case when fp.amt_ExchangeRate = 0 THEN 1 ELSE fp.amt_ExchangeRate END) amt_UnitPrice	/* LK: fp in tran, ivntryaging in local */
     FROM fact_purchase fp
          INNER JOIN dim_purchasemisc pm ON fp.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
          INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = fp.Dim_Currencyid
     WHERE CASE WHEN fp.ct_ReceivedQty < 0 THEN -1*fp.ct_ReceivedQty ELSE fp.ct_ReceivedQty END < fp.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and fp.Dim_PlantidSupplying <> 1 and fp.Dim_Vendorid = 1;

DROP TABLE IF EXISTS TMP_INSERT_11_STOCK;
CREATE TABLE TMP_INSERT_11_STOCK
AS
   SELECT fp.ct_StockInTransit,
          (b.multiplier * fp.ct_StockInTransit) amt_StockInTransitAmt,
          (b.multiplier * fp.ct_StockInTransit) * fp.amt_ExchangeRate_GBL amt_StockInTransitAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          b.multiplier * fp.amt_ExchangeRate_GBL amt_StdUnitPrice_GBL,
          (b.multiplier * fp.ct_StockInTransit) amt_OnHand,
          0 amt_OnHand_GBL,
          0 ct_LastReceivedQty,
          'Not Set' dd_BatchNo,
          fp.dd_DocumentNo dd_DocumentNo,
          fp.dd_DocumentItemNo dd_DocumentItemNo,
          convert(bigint, 1) dim_StorageLocEntryDateid,
          1 dim_LastReceivedDateid,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          fp.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          fp.Dim_PlantidSupplying Dim_SupplyingPlantId,
		(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
		convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		1 amt_ExchangeRate,
		fp.dim_currencyid_TRA,
		fp.dim_currencyid_GBL
	  	,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		,b.PEINH as amt_priceunit
		,ifnull(b.MBEW_LBKUM,0) as ct_totvalstkQty
     FROM dim_plant p
          INNER JOIN MARC a ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN tmp_fact_fp_001 fp ON fp.Dim_Partid = dp.Dim_Partid
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
		  CROSS JOIN tmp_stkcategory_001 stkc;

MERGE INTO TMP_INSERT_11_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_11_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;

MERGE INTO TMP_INSERT_11_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(mmdt.Dim_Dateid,1) Dim_Dateid
	 FROM TMP_INSERT_11_STOCK t
		LEFT JOIN tmp_custom_postproc_dim_date mmdt ON mmdt.dd_DocumentNo = t.dd_DocumentNo and mmdt.dd_DocumentItemNo = t.dd_DocumentItemNo and mmdt.RowSeqNo = 1
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_StorageLocEntryDateid = SRC.Dim_Dateid;

/*INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
	amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	Dim_SupplyingPlantId,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL, amt_priceunit, ct_totvalstkQty)
SELECT ct_StockInTransit,
	amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	Dim_SupplyingPlantId,
	fact_inventoryagingid,
	1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL, amt_priceunit, ct_totvalstkQty
FROM TMP_INSERT_11_STOCK s
where not exists (select 1 from  fact_inventoryaging_tmp_populate t where
t.dim_partid=s.dim_partid and t.dim_plantid=s.dim_plantid and t.dim_StorageLocationid=s.dim_StorageLocationid and t.dd_BatchNo=s.dd_BatchNo)*/

/********* Replaced with above ********
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                ct_StockInTransfer,
                                amt_StockInTransferAmt,
                                amt_StockInTransferAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
   SELECT ifnull(MARC_TRAME,0) ct_StockInTransit,
          ifnull((b.multiplier * MARC_TRAME),0) amt_StockInTransitAmt,
          ifnull(b.multiplier * MARC_TRAME
	   * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0)
            amt_StockInTransitAmt_GBL,
          ifnull(MARC_UMLMC, 0) ct_StockInTransfer,
          ifnull((b.multiplier * MARC_UMLMC),0) amt_StockInTransferAmt,
          ifnull(b.multiplier * MARC_UMLMC
           * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0)
	    amt_StockInTransferAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          1  StorageLocEntryDate,
          1 LastReceivedDate,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          1,
          ifnull(pg.Dim_PurchaseGroupid,1),
          ifnull(dph.dim_producthierarchyid,1),
          1,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          1,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (),
	  1,1,1,1,1,1, 1,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)
     FROM dim_plant p
          INNER JOIN MARC a ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency,
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
********************************************/

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';


  /*# INTER stock analytics */

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = '643';

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid
	FROM dim_stockcategory stkc
	WHERE stkc.categorycode = 'STO';

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (a.ct_QtyIssued - (CASE WHEN a.ct_ReceivedQty < 0 THEN -1*a.ct_ReceivedQty ELSE a.ct_ReceivedQty END))
		* (a.PriceConversion_EqualTo / a.PriceConversion_Denom) ct_StockInTransit,
          a.amt_StdUnitPrice,
	  a.amt_UnitPrice,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
          a.Dim_Partid,
          a.dim_plantidordering,
          a.dim_currencyid,
		  a.Dim_currencyid Dim_CurrencyId_TRA,
		  a.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,
		  1 amt_ExchangeRate,
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          a.Dim_PlantIdSupplying,
	     (a.amt_ExchangeRate_GBL/case when a.amt_ExchangeRate = 0 THEN 1 ELSE a.amt_ExchangeRate END) amt_ExchangeRate_GBL
     FROM fact_purchase a
          INNER JOIN dim_purchasemisc pm ON a.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
     WHERE CASE WHEN a.ct_ReceivedQty < 0 THEN -1*a.ct_ReceivedQty ELSE a.ct_ReceivedQty END < a.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and a.Dim_PlantidSupplying <> 1
           and not exists (select 1 from fact_inventoryaging_tmp_populate fi inner join dim_stockcategory sc on fi.dim_stockcategoryid = sc.Dim_StockCategoryid
                          where fi.dim_Partid = a.dim_partid and ifnull(fi.dd_DocumentNo,'xxx') = ifnull(a.dd_DocumentNo,'yyy') and ifnull(sc.CategoryCode,'xxx') = 'MARC');

DROP TABLE IF EXISTS fitp_ui0;
CREATE TABLE fitp_ui0 LIKE fact_inventoryaging_tmp_populate INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE fitp_ui0  ADD PRIMARY KEY (fact_inventoryagingid);
ALTER TABLE fitp_ui0 ADD COLUMN CurrencyCode varchar(20) default 'Not Set';
ALTER TABLE fitp_ui0 ADD COLUMN pGlobalCurrency varchar(20) default 'Not Set';

INSERT INTO fitp_ui0(ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL,
				amt_ExchangeRate, dim_Currencyid_TRA,dim_Currencyid_GBL,CurrencyCode,pGlobalCurrency, amt_priceunit, ct_totvalstkQty)
   SELECT a.ct_StockInTransit,
          (b.multiplier * a.ct_StockInTransit) amt_StockInTransitAmt,
          (b.multiplier * a.ct_StockInTransit * a.amt_ExchangeRate_GBL) amt_StockInTransitAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          (b.multiplier * a.amt_ExchangeRate_GBL)  amt_StdUnitPrice_GBL,
          (b.multiplier * a.ct_StockInTransit) amt_OnHand,
          0 amt_OnHand_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
	  1 StorageLocEntryDate,
          1 LastReceivedDate,
          a.Dim_Partid,
          a.dim_plantidordering,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          a.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          a.Dim_PlantIdSupplying,
	     (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
		  convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
		  a.amt_ExchangeRate,
		  a.dim_Currencyid_TRA,
		  a.dim_Currencyid_GBL
		 ,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		 ,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		 ,b.PEINH as amt_priceunit
		 ,ifnull(b.MBEW_LBKUM,0) as ct_totvalstkQty
     FROM tmp_fact_fp_001 a
          INNER JOIN dim_plant p ON a.dim_plantidordering = p.dim_plantid
          INNER JOIN dim_part dp ON dp.dim_partid = a.dim_partid
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = dp.PartNumber AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency
		  CROSS JOIN tmp_GlobalCurr_001 gbl
		  CROSS JOIN tmp_stkcategory_001 stkc;

MERGE INTO fitp_ui0 ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM fitp_ui0 t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;

MERGE INTO fitp_ui0 FA
USING ( SELECT fact_inventoryagingid, IFNULL(mmdt.Dim_Dateid,1) dim_StorageLocEntryDateid
		FROM fitp_ui0 T1
				LEFT JOIN tmp_custom_postproc_dim_date mmdt ON (mmdt.dd_DocumentNo = T1.dd_DocumentNo
											and mmdt.dd_DocumentItemNo = T1.dd_DocumentItemNo
											and mmdt.RowSeqNo = 1) ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.dim_StorageLocEntryDateid = SRC.dim_StorageLocEntryDateid;


/*Georgiana 31 Oct 2016*/
/*Insert into fact_inventoryaging_tmp_populate(ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
                                fact_inventoryagingid,
                                amt_ExchangeRate_GBL,
                                amt_ExchangeRate,
				dim_Currencyid_TRA,
				dim_Currencyid_GBL,
				amt_priceunit,
				ct_totvalstkQty)
Select ct_StockInTransit,
	amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
        amt_OnHand,
        amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	Dim_SupplyingPlantId,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	amt_priceunit,
	ct_totvalstkQty
from fitp_ui0 s
where not exists (select 1 from  fact_inventoryaging_tmp_populate t where
t.dim_partid=s.dim_partid and t.dim_plantid=s.dim_plantid and t.dim_StorageLocationid=s.dim_StorageLocationid and t.dd_BatchNo=s.dd_BatchNo)*/
/*Georgiana END*/

drop table if exists fitp_ui0;


update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';


/*********************** LK: 26 Apr change : Insert queries 7/8/9/10/11 Ends ***********************/

/* Update 1 */

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
and ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x
			       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
				     AND ((x.VPRSV = 'S' AND x.STPRS > 0) OR (x.VPRSV = 'V' AND x.VERPR > 0)))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,
((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON,
m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  convert(decimal (18,5),(b.STPRS / b.PEINH))
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  convert(decimal (18,5),(b.VERPR / b.PEINH))
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

drop table if exists pl_prt_MBEW_NO_BWTAR;
create table pl_prt_MBEW_NO_BWTAR
as
select distinct ia.dim_partid,ia.dim_plantid,prt.PartNumber,pl.ValuationArea,pl.CompanyCode,pl.plantcode
from fact_inventoryaging_tmp_populate ia,dim_plant pl,dim_part prt
WHERE ia.dim_partid = prt.Dim_partid
 and ia.dim_plantid = pl.dim_plantid;

DROP TABLE IF EXISTS TMP_UPDT_INV;
CREATE TABLE TMP_UPDT_INV
AS
SELECT
	ia.FACT_INVENTORYAGINGID
	,MBEW_BWPRH
	,MBEW_LPLPR
	,MBEW_VMVER
	,MBEW_ZPLP1
	,MBEW_STPRV
	,MBEW_VPLPR
	,dt1.Dim_Dateid dt1_Dim_Dateid
	,dt2.Dim_Dateid dt2_Dim_Dateid
	,m.VPRSV
	,m.STPRS
	,m.PEINH
	,m.VERPR
FROM
fact_inventoryaging_tmp_populate ia
		INNER JOIN pl_prt_MBEW_NO_BWTAR pl ON ia.dim_partid = pl.Dim_partid
    		                                   and ia.dim_plantid = pl.dim_plantid
		INNER JOIN tmp2_MBEW_NO_BWTAR m ON pl.PartNumber = m.MATNR
    						   AND pl.ValuationArea = m.BWKEY
		LEFT JOIN dim_Date dt1 ON dt1.DateValue = m.MBEW_LAEPR
				       AND dt1.CompanyCode = pl.CompanyCode and dt1.plantcode_factory=pl.plantcode
		LEFT JOIN dim_Date dt2 ON dt2.DateValue = m.MBEW_ZPLD2
		                       AND dt2.CompanyCode = pl.CompanyCode and dt2.plantcode_factory=pl.plantcode;

UPDATE fact_inventoryaging_tmp_populate ia
  set amt_CommericalPrice1 = ifnull(t.MBEW_BWPRH,0),
      amt_CurPlannedPrice = ifnull(t.MBEW_LPLPR,0),
      amt_MovingAvgPrice = ifnull(t.MBEW_VMVER,0),
      amt_PlannedPrice1 = ifnull(t.MBEW_ZPLP1,0),
      amt_PreviousPrice = ifnull(t.MBEW_STPRV,0),
      amt_PrevPlannedPrice = ifnull(t.MBEW_VPLPR,0),
      Dim_DateIdLastChangedPrice = ifnull(t.dt1_Dim_Dateid,1),
      Dim_DateIdPlannedPrice2 = ifnull(t.dt2_Dim_Dateid,1),
      amt_MtlDlvrCost = ifnull((CASE t.VPRSV WHEN 'S' THEN t.STPRS / t.PEINH
                                WHEN 'V' THEN t.VERPR / t.PEINH END), 0)
FROM  fact_inventoryaging_tmp_populate ia, TMP_UPDT_INV t
WHERE ia.FACT_INVENTORYAGINGID = t.FACT_INVENTORYAGINGID;

DROP TABLE IF EXISTS TMP_UPDT_INV;


/* 2nd and 3rd updates	*/

DROP TABLE IF EXISTS TMP_UPDT_INV_PURCH;
CREATE TABLE TMP_UPDT_INV_PURCH
AS
SELECT fp.dd_DocumentNo,fp.dd_DocumentItemNo, fp.Dim_ConsumptionTypeid,fp.Dim_DocumentStatusid,fp.Dim_DocumentTypeid,fp.Dim_IncoTerm1id,fp.Dim_ItemCategoryid,fp.Dim_ItemStatusid, fp.Dim_Termid,fp.Dim_PurchaseMiscid,fp.Dim_PlantidSupplying,
	row_number() over(partition by fp.dd_DocumentNo,fp.dd_DocumentItemNo order by '') rono
FROM fact_purchase fp;

UPDATE fact_inventoryaging_tmp_populate ia
   SET ia.Dim_ConsumptionTypeid = fp.Dim_ConsumptionTypeid,
       ia.Dim_DocumentStatusid = fp.Dim_DocumentStatusid,
       ia.Dim_DocumentTypeid = fp.Dim_DocumentTypeid,
       ia.Dim_IncoTermid = fp.Dim_IncoTerm1id,
       ia.Dim_ItemCategoryid = fp.Dim_ItemCategoryid,
       ia.Dim_ItemStatusid = fp.Dim_ItemStatusid,
       ia.Dim_Termid = fp.Dim_Termid,
       ia.Dim_PurchaseMiscid = fp.Dim_PurchaseMiscid,
       ia.Dim_SupplyingPlantId = fp.Dim_PlantidSupplying
FROM fact_inventoryaging_tmp_populate ia,TMP_UPDT_INV_PURCH fp
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo
	   AND fp.rono = 1;

/* COMMENT - THIS UPDATE NEEDS TO BE CHECKED AGAIN.  */

UPDATE fact_purchase fp
SET fp.ct_UnrestrictedOnHandQty = ia.ct_StockQty
	,fp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_purchase fp,fact_inventoryaging_tmp_populate ia
WHERE ia.dim_Partid = fp.Dim_Partid
AND ia.dim_Plantid = fp.Dim_PlantidSupplying
AND ia.dd_documentno = fp.dd_documentno
AND ia.dd_documentitemno = fp.dd_documentitemno
AND fp.ct_UnrestrictedOnHandQty <> ia.ct_StockQty;

/* Update 4	*/
/* This update is using up a lot of memory on VW and does not complete */


drop table if exists tmp_keph;
CREATE TABLE tmp_keph
AS
SELECT KEPH_KALNR,KEPH_BWVAR,KEPH_KADKY,KEPH_KST001,k.KEPH_KST002,k.KEPH_KST003 + k.KEPH_KST004 + k.KEPH_KST005 + k.KEPH_KST006 + k.KEPH_KST007 + k.KEPH_KST008
                        + k.KEPH_KST009 + k.KEPH_KST010 + k.KEPH_KST011 + k.KEPH_KST012 + k.KEPH_KST013 + k.KEPH_KST014
                        + k.KEPH_KST015 + k.KEPH_KST016 + k.KEPH_KST017 + k.KEPH_KST018 + k.KEPH_KST019 + k.KEPH_KST020
                        + k.KEPH_KST021 + k.KEPH_KST022 + k.KEPH_KST023 + k.KEPH_KST024 + k.KEPH_KST025 + k.KEPH_KST026
                        + k.KEPH_KST027 as keph_othercosts, k.KEPH_KST001 + k.KEPH_KST002 as keph_allcosts
FROM keph k;

update tmp_keph
set keph_allcosts = keph_allcosts + keph_othercosts;

drop table if exists tmp_dim_date;
create table tmp_dim_date as
select DateValue,FinancialYear,FinancialMonthNumber,(FinancialYear * 100) + FinancialMonthNumber as yymm,CompanyCode,plantcode_factory from dim_date;

/*Use intermediate table ia1 as this was failing in fossil2 */

drop table if exists ia1;
create table ia1 as
select distinct ia.dim_Partid,ia.dim_plantid,pl.CompanyCode,pl.ValuationArea,p.PartNumber,pl.plantcode
from fact_inventoryaging_tmp_populate ia,dim_part p, dim_plant pl
where  ia.dim_Partid = p.Dim_Partid and ia.dim_plantid = pl.dim_plantid;

drop table if exists tmp3_MBEW_NO_BWTAR;
create table tmp3_MBEW_NO_BWTAR as
select distinct ia.dim_Partid,ia.dim_plantid,ia.CompanyCode,ia.plantcode, m.MATNR,m.BWKEY,m.MBEW_KALN1,m.MBEW_BWVA2,((m.MBEW_PDATL * 100) + m.MBEW_PPRDL) as m_yymm,m.multiplier
from ia1 ia, tmp2_MBEW_NO_BWTAR m
where ia.PartNumber = m.MATNR and ia.ValuationArea = m.BWKEY;

/*Create an intermediate table, which would decrease the number of rows read from keph */

drop table if exists tmp4_MBEW_NO_BWTAR;
create table tmp4_MBEW_NO_BWTAR as
select distinct m.*
from tmp3_MBEW_NO_BWTAR m,tmp_dim_date dt
where cast(dt.yymm as integer) = cast(m.m_yymm as integer);

drop table if exists tmp4_dim_date;
create table tmp4_dim_date as
select distinct dt.*
from tmp4_MBEW_NO_BWTAR m,tmp_dim_date dt
where dt.yymm = m.m_yymm;

drop table if exists tmp2_keph;
create table tmp2_keph as
select DISTINCT m.dim_Partid,m.dim_plantid,m.CompanyCode,m.plantcode,m.m_yymm,k.KEPH_KST001,k.KEPH_KST002,k.keph_othercosts,k.keph_allcosts,k.KEPH_KADKY,k.KEPH_KALNR,k.KEPH_BWVAR
FROM   tmp4_MBEW_NO_BWTAR m, tmp_keph k
where k.KEPH_KALNR = m.MBEW_KALN1 and k.KEPH_BWVAR = m.MBEW_BWVA2 and k.keph_allcosts = m.multiplier;


MERGE INTO fact_inventoryaging_tmp_populate ia
USING
(
SELECT DISTINCT
ia.fact_inventoryagingid,
max(k.KEPH_KST001) KEPH_KST001,
max(k.KEPH_KST002) KEPH_KST002,
max(k.keph_othercosts) keph_othercosts
FROM
fact_inventoryaging_tmp_populate ia
		INNER JOIN tmp2_keph k ON ( ia.dim_Partid = k.Dim_Partid
							   and  ia.dim_plantid = k.dim_plantid )
		INNER JOIN tmp4_dim_date dt ON ( k.KEPH_KADKY = dt.DateValue
									and  k.CompanyCode = dt.CompanyCode
									and  k.plantcode=dt.plantcode_factory
									and  dt.yymm = k.m_yymm )
GROUP BY ia.fact_inventoryagingid
) fb
ON fb.fact_inventoryagingid = ia.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
SET ia.amt_MtlDlvrCost = fb.KEPH_KST001,
       ia.amt_OverheadCost = fb.KEPH_KST002,
       ia.amt_OtherCost = fb.keph_othercosts,
	   ia.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
;

/* Issam change 24th Jun */
/* Update Purchase Order and Sales Order Open Qtys */

MERGE INTO fact_inventoryaging_tmp_populate ia
USING
(
SELECT fpr.Dim_partid,
SUM(case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (fpr.ct_DeliveryQty - fpr.ct_ReceivedQty) < 0 then 0.0000 else (fpr.ct_DeliveryQty - fpr.ct_ReceivedQty) end)  as ct_POOpenQty_sum
FROM
fact_purchase fpr,
Dim_PurchaseMisc atrb
WHERE
fpr.Dim_PurchaseMiscid = atrb.Dim_PurchaseMiscid
group by fpr.Dim_partid) fb
ON fb.dim_partid = ia.dim_partid
WHEN MATCHED THEN UPDATE
SET ia.ct_POOpenQty = ifnull(fb.ct_POOpenQty_sum,0);

MERGE INTO fact_inventoryaging_tmp_populate ia
USING
(
SELECT fso.Dim_partid,
SUM(fso.ct_AfsOpenQty) as ct_AfsOpenQty_sum
FROM
fact_salesorder fso
group by fso.Dim_partid) fb
ON fb.dim_partid = ia.dim_partid
WHEN MATCHED THEN UPDATE
SET ia.ct_SOOpenQty = ifnull(fb.ct_AfsOpenQty_sum,0);

/* Update 5	*/

MERGE INTO dim_part
USING
(
SELECT f_inv.dim_Partid,
sum(f_inv.ct_StockQty+f_inv.ct_StockInQInsp+f_inv.ct_BlockedStock+f_inv.ct_StockInTransfer) as TotalPlantStock_sum
FROM
fact_inventoryaging_tmp_populate f_inv
group by f_inv.dim_Partid) fb
ON fb.dim_partid = dim_part.dim_partid
WHEN MATCHED THEN UPDATE
SET dim_part.TotalPlantStock = ifnull(fb.TotalPlantStock_sum,0);

MERGE INTO dim_part
USING
(
SELECT f_inv.dim_Partid,
sum(f_inv.amt_StockValueAmt+f_inv.amt_StockInQInspAmt+f_inv.amt_BlockedStockAmt+f_inv.amt_StockInTransferAmt) as TotalPlantStockAmt_sum
FROM
fact_inventoryaging_tmp_populate f_inv
group by f_inv.dim_Partid) fb
ON fb.dim_partid = dim_part.dim_partid
WHEN MATCHED THEN UPDATE
SET dim_part.TotalPlantStockAmt = ifnull(fb.TotalPlantStockAmt_sum,0);

/* # WIP stock analytics */

/*Georgiana changes: excluding these wip changes in order to eliminate duplicates in inventory*/
/*DROP TABLE IF EXISTS tmp_stkcategory_001
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid
	FROM dim_stockcategory stkc
	WHERE stkc.categorycode = 'WIP'

INSERT INTO fact_inventoryaging_tmp_populate(ct_StockQty,
                               amt_StockValueAmt,
                               ct_LastReceivedQty,
                               dim_StorageLocEntryDateid,
                               dim_LastReceivedDateid,
                               dim_Partid,
                               dim_Plantid,
                               dim_StorageLocationid,
                               dim_Companyid,
                               dim_Vendorid,
                               dim_Currencyid,
				   dim_Currencyid_TRA,
				   dim_Currencyid_GBL,
                               dim_StockTypeid,
                               dim_specialstockid,
                               Dim_PurchaseOrgid,
                               Dim_PurchaseGroupid,
                               dim_producthierarchyid,
                               Dim_UnitOfMeasureid,
                               Dim_MovementIndicatorid,
                               Dim_ConsumptionTypeid,
                               dim_costcenterid,
                               Dim_DocumentStatusid,
                               Dim_DocumentTypeid,
                               Dim_IncoTermid,
                               Dim_ItemCategoryid,
                               Dim_ItemStatusid,
                               Dim_Termid,
                               Dim_PurchaseMiscid,
                               amt_StockValueAmt_GBL,
                               ct_TotalRestrictedStock,
                               ct_StockInQInsp,
                               ct_BlockedStock,
                               ct_StockInTransfer,
                               ct_UnrestrictedConsgnStock,
                               ct_RestrictedConsgnStock,
                               ct_BlockedConsgnStock,
                               ct_ConsgnStockInQInsp,
                               ct_BlockedStockReturns,
                               amt_BlockedStockAmt,
                               amt_BlockedStockAmt_GBL,
                               amt_StockInQInspAmt,
                               amt_StockInQInspAmt_GBL,
                               amt_StockInTransferAmt,
                               amt_StockInTransferAmt_GBL,
                               amt_UnrestrictedConsgnStockAmt,
                               amt_UnrestrictedConsgnStockAmt_GBL,
                               amt_StdUnitPrice,
                               amt_StdUnitPrice_GBL,
                               dim_stockcategoryid,
                               ct_StockInTransit,
                               amt_StockInTransitAmt,
                               amt_StockInTransitAmt_GBL,
                               Dim_SupplyingPlantId,
                               amt_MtlDlvrCost,
                               amt_OverheadCost,
                               amt_OtherCost,
                               amt_WIPBalance,
                               ct_WIPAging,
			       ct_WIPQty,
                               amt_MovingAvgPrice,
                               amt_PreviousPrice,
                               amt_CommericalPrice1,
                               amt_PlannedPrice1,
                               amt_PrevPlannedPrice,
                               amt_CurPlannedPrice,
                               Dim_DateIdPlannedPrice2,
                               Dim_DateIdLastChangedPrice,
                               Dim_DateidActualRelease,
                               Dim_DateidActualStart,
                               dd_ProdOrdernumber,
                               dd_ProdOrderitemno,
                               dd_Plannedorderno,
                               dim_productionorderstatusid,
                               Dim_productionordertypeid,
			       fact_inventoryagingid,
			       amt_ExchangeRate_GBL,amt_ExchangeRate)
  SELECT 0 ct_StockQty,
        0 amt_StockValueAmt,
        0 ct_LastReceivedQty,
        1 dim_StorageLocEntryDateid,
        1 dim_LastReceivedDateid,
        po.Dim_PartidItem dim_Partid,
        po.Dim_Plantid dim_Plantid,
        po.Dim_StorageLocationid dim_StorageLocationid,
        po.Dim_Companyid dim_Companyid,
        1 dim_Vendorid,
        po.Dim_Currencyid dim_Currencyid,
		po.Dim_Currencyid dim_Currencyid_TRA,
		po.Dim_Currencyid_GBL dim_Currencyid_GBL,
        po.Dim_StockTypeid dim_StockTypeid,
        po.dim_specialstockid dim_specialstockid,
        po.Dim_PurchaseOrgid Dim_PurchaseOrgid,
        1 Dim_PurchaseGroupid,
        1 dim_producthierarchyid,
        po.Dim_UnitOfMeasureid Dim_UnitOfMeasureid,
        1 Dim_MovementIndicatorid,
        po.Dim_ConsumptionTypeid Dim_ConsumptionTypeid,
        1 dim_costcenterid,
        1 Dim_DocumentStatusid,
        1 Dim_DocumentTypeid,
        1 Dim_IncoTermid,
        1 Dim_ItemCategoryid,
        1 Dim_ItemStatusid,
        1 Dim_Termid,
        1 Dim_PurchaseMiscid,
        0 amt_StockValueAmt_GBL,
        0 ct_TotalRestrictedStock,
        0 ct_StockInQInsp,
        0 ct_BlockedStock,
        0 ct_StockInTransfer,
        0 ct_UnrestrictedConsgnStock,
        0 ct_RestrictedConsgnStock,
        0 ct_BlockedConsgnStock,
        0 ct_ConsgnStockInQInsp,
        0 ct_BlockedStockReturns,
        0 amt_BlockedStockAmt,
        0 amt_BlockedStockAmt_GBL,
        0 amt_StockInQInspAmt,
        0 amt_StockInQInspAmt_GBL,
        0 amt_StockInTransferAmt,
        0 amt_StockInTransferAmt_GBL,
        0 amt_UnrestrictedConsgnStockAmt,
        0 amt_UnrestrictedConsgnStockAmt_GBL,
        0 amt_StdUnitPrice,
        0 amt_StdUnitPrice_GBL,
        stkc.dim_stockcategoryid,
        0 ct_StockInTransit,
        0 amt_StockInTransitAmt,
        0 amt_StockInTransitAmt_GBL,
        1 Dim_SupplyingPlantId,
        0 amt_MtlDlvrCost,
        0 amt_OverheadCost,
        0 amt_OtherCost,
        po.amt_WIPBalance * po.amt_ExchangeRate amt_WIPBalance,
		0 ct_WIPAging,
		ifnull(po.ct_WIPQty,0),
        0 amt_MovingAvgPrice,
        0 amt_PreviousPrice,
        0 amt_CommericalPrice1,
        0 amt_PlannedPrice1,
        0 amt_PrevPlannedPrice,
        0 amt_CurPlannedPrice,
        1 Dim_DateIdPlannedPrice2,
        1 Dim_DateIdLastChangedPrice,
        po.Dim_DateidActualRelease Dim_DateidActualRelease,
        po.Dim_DateidActualStart Dim_DateidActualStart,
        po.dd_ordernumber dd_ProdOrdernumber,
        po.dd_orderitemno dd_ProdOrderitemno,
        po.dd_plannedorderno dd_Plannedorderno,
        po.dim_productionorderstatusid dim_productionorderstatusid,
        po.Dim_ordertypeid Dim_productionordertypeid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	(po.amt_ExchangeRate_GBL/case when po.amt_ExchangeRate = 0 THEN 1 ELSE po.amt_ExchangeRate END) amt_ExchangeRate_GBL,	/* LK: GBL exchange rate for inventoryaging is local-->global
	1 amt_ExchangeRate
  FROM fact_productionorder po
	INNER JOIN dim_productionorderstatus ost ON po.dim_productionorderstatusid = ost.dim_productionorderstatusid
	INNER JOIN dim_part p ON po.Dim_PartidItem = p.dim_partid
	INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = po.Dim_Currencyid
	CROSS JOIN tmp_stkcategory_001 stkc
  WHERE ost.Closed = 'Not Set'
      and ost.Delivered = 'Not Set'
      and ost.Created = 'Not Set'
      and ost.TechnicallyCompleted = 'Not Set'
      and po.ct_OrderItemQty - po.ct_GRQty > 0*/

/*MERGE INTO fact_inventoryaging_tmp_populate FA
USING ( SELECT fact_inventoryagingid, ifnull(days_between(current_date,pard.DateValue),0) ct_WIPAging
		FROM fact_inventoryaging_tmp_populate T1
				LEFT JOIN dim_date pard ON ( pard.Dim_Dateid = t1.Dim_DateidActualRelease
										and t1.Dim_DateidActualRelease > 1 ) ) SRC
ON FA.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE SET FA.ct_WIPAging = SRC.ct_WIPAging
WHERE FA.ct_WIPAging <> SRC.ct_WIPAging*/

/*Georgiana End of Changes*/

DELETE FROM fact_materialmovement_tmp_invagin;



/* Begin 15 Oct 2013 changes */

DROP TABLE IF EXISTS fact_materialmovement_tmp0;
CREATE TABLE fact_materialmovement_tmp0 as
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity
FROM fact_materialmovement fmm
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE
mt.movementtype IN ('101', '501')
AND pd.DateValue >= current_date - INTERVAL '30' DAY
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GRQty_Late30 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp0 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp0;


DROP TABLE IF EXISTS fact_materialmovement_tmp1;
CREATE TABLE fact_materialmovement_tmp1 as
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity
FROM fact_materialmovement fmm
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE
mt.movementtype IN ('101', '501')
AND pd.DateValue >= (current_date - INTERVAL '60' DAY)
AND pd.DateValue <= (current_date - INTERVAL '31' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GRQty_31_60 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp1 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp1;


DROP TABLE IF EXISTS fact_materialmovement_tmp2;
CREATE TABLE fact_materialmovement_tmp2 as
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity
FROM fact_materialmovement fmm
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE
mt.movementtype IN ('101', '501')
AND pd.DateValue >= (current_date - INTERVAL '90' DAY)
AND pd.DateValue <= (current_date - INTERVAL '61' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GRQty_61_90 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp2 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp2;


DROP TABLE IF EXISTS fact_materialmovement_tmp3;
CREATE TABLE fact_materialmovement_tmp3 as
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity
FROM fact_materialmovement fmm
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE
mt.movementtype IN ('101', '501')
AND pd.DateValue between (current_date - INTERVAL '180' DAY(3)) and (current_date - INTERVAL '91' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GRQty_91_180 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp3 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp3;


DROP TABLE IF EXISTS fact_materialmovement_tmp4;
CREATE TABLE fact_materialmovement_tmp4 as
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity
FROM fact_materialmovement fmm
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE
mt.movementtype IN ('201', '261', '301', '601', '641')
AND pd.DateValue >= (current_date - INTERVAL '30' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GIQty_Late30 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp4 fm
ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp4;


DROP TABLE IF EXISTS fact_materialmovement_tmp5;
CREATE TABLE fact_materialmovement_tmp5 as
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity
FROM fact_materialmovement fmm
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE
mt.movementtype IN ('201', '261', '301', '601', '641')
AND pd.DateValue >= (current_date - INTERVAL '60' DAY) AND pd.DateValue <= (current_date - INTERVAL '31' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GIQty_31_60 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp5 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp5;


DROP TABLE IF EXISTS fact_materialmovement_tmp6;
CREATE TABLE fact_materialmovement_tmp6 as
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity
FROM fact_materialmovement fmm
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE
mt.movementtype IN ('201', '261', '301', '601', '641')
AND pd.DateValue >= (current_date - INTERVAL '90' DAY) AND pd.DateValue <= (current_date - INTERVAL '61' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GIQty_61_90 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp6 fm ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp6;


DROP TABLE IF EXISTS fact_materialmovement_tmp7;
CREATE TABLE fact_materialmovement_tmp7 as
SELECT fmm.dim_partid,SUM(fmm.ct_Quantity) as ct_quantity
FROM fact_materialmovement fmm
JOIN dim_movementtype mt ON fmm.dim_movementtypeid = mt.dim_movementtypeid
JOIN dim_date pd ON fmm.dim_DateIDPostingDate = pd.dim_dateid
WHERE
mt.movementtype IN ('201', '261', '301', '601', '641')
AND pd.DateValue >= (current_date - INTERVAL '180' DAY(3)) AND pd.DateValue <= (current_date - INTERVAL '91' DAY)
GROUP BY fmm.dim_partid;

Update fact_inventoryaging_tmp_populate fi
SET fi.ct_GIQty_91_180 = IFNULL(fm.ct_Quantity,0)
FROM
fact_inventoryaging_tmp_populate fi
		LEFT JOIN fact_materialmovement_tmp7 fm
ON fm.dim_partid = fi.dim_partid;

DROP TABLE IF EXISTS fact_materialmovement_tmp7;
/* End 15 Oct 2013 changes */

DROP TABLE IF EXISTS tmp_fact_purchase;
CREATE TABLE tmp_fact_purchase AS
select  max(fp.dim_profitcenterid)dim_profitcenterid,fp.dd_documentno ,fp.dd_documentitemno
from fact_purchase fp
where   fp.dim_profitcenterid <> 1
group by fp.dd_documentno ,fp.dd_documentitemno;

update fact_inventoryaging_tmp_populate inv
set inv.dim_profitcenterid = fp.dim_profitcenterid
from
fact_inventoryaging_tmp_populate inv,
tmp_fact_purchase fp
where
fp.dd_documentno = inv.dd_documentno
and fp.dd_documentitemno = inv.dd_documentitemno
and inv.dim_profitcenterid = 1;



/* Sales Order stock analytics */


DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid
	FROM dim_stockcategory stkc
	WHERE stkc.categorycode = 'MSKA';

update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';

DELETE FROM MSKA WHERE MSKA_KAEIN = 0 and MSKA_KAINS = 0 and MSKA_KALAB = 0 and MSKA_KASPE = 0;

   /* Intermediate tables -- replace MBEW_NO_BWTAR  */
drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = cast((b.STPRS / b.PEINH) as decimal (18,5))
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  cast((b.VERPR / b.PEINH) as decimal (18,5))
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

delete from NUMBER_FOUNTAIN where TABLE_NAME = 'dim_storagelocation';
insert into NUMBER_FOUNTAIN (MAX_ID,TABLE_NAME)
select ifnull(( select max(dim_storagelocationid) from dim_storagelocation), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) a,'dim_storagelocation';

  INSERT INTO dim_storagelocation(Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent,dim_storagelocationid)
   SELECT DISTINCT MSKA_LGORT,
                   MSKA_LGORT,
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   MSKA_WERKS,
                   'Not Set',
                   current_timestamp,
                   1,
				   row_number() over (order by '')+(select MAX_ID from NUMBER_FOUNTAIN where TABLE_NAME = 'dim_storagelocation')	/*This should be changed later ( to max + 1 ). This should be standard - common for all such scenarios*/
     FROM MSKA m
    WHERE MSKA_LGORT IS NOT NULL
      AND not exists (SELECT 1
                            FROM dim_storagelocation d
                           WHERE ifnull(d.LocationCode,'xxx') = ifnull(MSKA_LGORT,'yyy') AND ifnull(d.Plant,'xxx') = ifnull(MSKA_WERKS,'yyy'));


/* StorageLocEntryDate/LastReceivedDate */

/* Simply use the posting date id for the min/max date */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0
AS
SELECT mm.*,mmdt.DateValue,mt.MovementType
FROM fact_materialmovement mm
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
	 INNER JOIN dim_specialstock sp ON sp.dim_specialstockid = mm.dim_specialstockid
where mm.dd_debitcreditid = 'Debit'
	AND sp.specialstockindicator = 'E';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue,
	mm.dim_specialstockid
from TMP_fact_mm_postingdate_0 mm
GROUP by mm.Dim_Partid,mm.dim_specialstockid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate,mm.Dim_MovementTypeid,mm.MovementType,
	mm.dim_specialstockid, mm.Dim_MovementIndicatorid,
	ROW_NUMBER() OVER(PARTITION BY mm.Dim_Partid ORDER BY mm.DateValue DESC) RowSeqNo
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT mm.Dim_Partid,mm.dim_DateIDPostingDate,mm.Dim_MovementTypeid,
	mm.dim_specialstockid
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.DateValue = t.min_DateValue;

DROP TABLE IF EXISTS TMP_INSERT_12_STOCK;
CREATE TABLE TMP_INSERT_12_STOCK
AS
  SELECT a.MSKA_KALAB ct_StockQty,
          a.MSKA_KAEIN ct_TotalRestrictedStock,
          a.MSKA_KAINS ct_StockInQInsp,
          a.MSKA_KASPE ct_BlockedStock,
          0 ct_StockInTransfer,
          0 ct_BlockedConsgnStock,
          0 ct_ConsgnStockInQInsp,
          0 ct_RestrictedConsgnStock,
          0 ct_UnrestrictedConsgnStock,
          0 ct_BlockedStockReturns,
          ifnull((b.multiplier * a.MSKA_KALAB),0) amt_StockValueAmt,
          ifnull((b.multiplier * a.MSKA_KALAB),0) amt_StockValueAmt_GBL,
          ifnull((b.multiplier * a.MSKA_KASPE), 0) amt_BlockedStockAmt,
          ifnull((b.multiplier * a.MSKA_KASPE),0) amt_BlockedStockAmt_GBL,
          ifnull((b.multiplier * a.MSKA_KAINS), 0) amt_StockInQInspAmt,
          ifnull((b.multiplier * a.MSKA_KAINS),0) amt_StockInQInspAmt_GBL,
          0 amt_StockInTransferAmt,
          0 amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt,
          0 amt_UnrestrictedConsgnStockAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          (b.multiplier * (a.MSKA_KALAB+a.MSKA_KAEIN+a.MSKA_KAINS+a.MSKA_KASPE)) amt_OnHand,
          0 amt_OnHand_GBL,
          0 ct_LastReceivedQty,
          ifnull(a.MSKA_CHARG,'Not Set') dd_BatchNo,
		  ifnull(a.MSKA_LGORT, 'Not Set') StorageLocation,
          'Not Set' dd_ValuationType,
          a.MSKA_VBELN dd_DocumentNo,
          a.MSKA_POSNR dd_DocumentItemNo,
          'Not Set' dd_MovementType,
          convert(bigint,1) dim_StorageLocEntryDateid,
          convert(bigint,1) dim_LastReceivedDateid,
          dp.Dim_PartID,
          p.dim_plantid,
          sloc.Dim_StorageLocationid,
          cmp.dim_Companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
		  dc.dim_currencyid dim_currencyid_TRA,
		  ifnull((SELECT Dim_Currencyid
                 FROM Dim_Currency dcr
                WHERE dcr.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ), 1) Dim_Currencyid_GBL,
          1 dim_stocktypeid,
          spstk.dim_specialstockid,
          convert(bigint,1) Dim_PurchaseOrgid,
          pg.Dim_PurchaseGroupid,
          dph.dim_producthierarchyid,
          uom.Dim_UnitOfMeasureid,
          convert(bigint ,1) Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid,
		  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
		  convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
	      1 amt_ExchangeRate
		  ,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		  ,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		  ,p.PurchOrg /* Dim_PurchaseOrgid */
		  ,dt.dim_dateid /* dim_LastReceivedDateid, dim_StorageLocEntryDateid */
     FROM MSKA a
          INNER JOIN dim_plant p ON a.MSKA_WERKS = p.PlantCode
          INNER JOIN dim_Company cmp ON cmp.CompanyCode = p.CompanyCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSKA_MATNR AND dp.Plant = a.MSKA_WERKS
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_date dt ON a.MSKA_ERSDA = dt.DateValue and dt.CompanyCode = p.CompanyCode and p.plantcode=dt.plantcode_factory
          INNER JOIN dim_storagelocation sloc ON sloc.LocationCode = MSKA_LGORT AND sloc.Plant = MSKA_WERKS
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = cmp.Currency
          INNER JOIN dim_specialstock spstk ON spstk.specialstockindicator = a.MSKA_SOBKZ
		  CROSS JOIN tmp_GlobalCurr_001 gbl
	      CROSS JOIN tmp_stkcategory_001 stkc;

MERGE INTO TMP_INSERT_12_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_12_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate,
		ST.amt_StockInQInspAmt_GBL = ST.amt_StockInQInspAmt_GBL * SRC.exchangeRate,
		ST.amt_BlockedStockAmt_GBL = ST.amt_BlockedStockAmt_GBL * SRC.exchangeRate,
		ST.amt_StockValueAmt_GBL = ST.amt_StockValueAmt_GBL * SRC.exchangeRate;

MERGE INTO TMP_INSERT_12_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_12_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_12_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(mm.Dim_MovementIndicatorid, 1) Dim_MovementIndicatorid, ifnull(mm.dim_DateIDPostingDate, t.dim_dateid) dim_DateIDPostingDate, ifnull(mm.MovementType, 'Not Set') MovementType
	 FROM TMP_INSERT_12_STOCK t
		LEFT JOIN TMP_fact_mm_postingdate_2 mm ON mm.dim_partid = t.Dim_PartID and mm.RowSeqNo = 1
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_MovementIndicatorid = SRC.Dim_MovementIndicatorid,
		ST.dim_LastReceivedDateid = SRC.dim_DateIDPostingDate,
		ST.dim_StorageLocEntryDateid = SRC.dim_DateIDPostingDate,
		ST.dd_MovementType = SRC.MovementType;

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	ct_BlockedStock,
	ct_StockInTransfer,
	ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock,
	ct_UnrestrictedConsgnStock,
	ct_BlockedStockReturns,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StockInTransferAmt,
	amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_ValuationType,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dd_MovementType,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_StockCategoryid,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	ct_BlockedStock,
	ct_StockInTransfer,
	ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock,
	ct_UnrestrictedConsgnStock,
	ct_BlockedStockReturns,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StockInTransferAmt,
	amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_ValuationType,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dd_MovementType,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_StockCategoryid,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate
FROM TMP_INSERT_12_STOCK s
where not exists (select 1 from  fact_inventoryaging_tmp_populate t where
t.dim_partid=s.dim_partid and t.dim_plantid=s.dim_plantid and t.dim_StorageLocationid=s.dim_StorageLocationid and t.dd_BatchNo=s.dd_BatchNo);


update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
where table_name = 'fact_inventoryaging_tmp_populate';

DROP TABLE IF EXISTS TMP_INSERT_13_STOCK;
CREATE TABLE TMP_INSERT_13_STOCK
AS
  SELECT a.MSKA_KALAB ct_StockQty,
          a.MSKA_KAEIN ct_TotalRestrictedStock,
          a.MSKA_KAINS ct_StockInQInsp,
          a.MSKA_KASPE ct_BlockedStock,
          0 ct_StockInTransfer,
          0 ct_BlockedConsgnStock,
          0 ct_ConsgnStockInQInsp,
          0 ct_RestrictedConsgnStock,
          0 ct_UnrestrictedConsgnStock,
          0 ct_BlockedStockReturns,
          0 amt_StockValueAmt,
          0 amt_StockValueAmt_GBL,
          0 amt_BlockedStockAmt,
          0 amt_BlockedStockAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StockInTransferAmt,
          0 amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt,
          0 amt_UnrestrictedConsgnStockAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          0 amt_OnHand,
          0 amt_OnHand_GBL,
          0 ct_LastReceivedQty,
          ifnull(a.MSKA_CHARG,'Not Set') dd_BatchNo,
          'Not Set' dd_ValuationType,
          a.MSKA_VBELN dd_DocumentNo,
          a.MSKA_POSNR dd_DocumentItemNo,
          'Not Set' dd_MovementType,
          convert(bigint,1) dim_StorageLocEntryDateid,
          convert(bigint,1) dim_LastReceivedDateid,
          dp.Dim_PartID,
          p.dim_plantid,
          sloc.Dim_StorageLocationid,
          cmp.dim_Companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
	  dc.dim_currencyid dim_currencyid_TRA,
	  ifnull((SELECT Dim_Currencyid
                 FROM Dim_Currency dcr
                WHERE dcr.CurrencyCode = (select pGlobalCurrency from tmp_GlobalCurr_001) ), 1) Dim_Currencyid_GBL,
          1 dim_stocktypeid,
          spstk.dim_specialstockid,
          convert(bigint,1) Dim_PurchaseOrgid,
          pg.Dim_PurchaseGroupid,
          dph.dim_producthierarchyid,
          uom.Dim_UnitOfMeasureid,
          convert(bigint ,1) Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid,
	      (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') fact_inventoryagingid,
	      convert(decimal (18,6), 1) amt_ExchangeRate_GBL,
	      1 amt_ExchangeRate
	  	  ,dc.CurrencyCode /* amt_ExchangeRate_GBL */
		  ,gbl.pGlobalCurrency /* amt_ExchangeRate_GBL */
		  ,p.PurchOrg /* Dim_PurchaseOrgid */
		  ,dt.dim_dateid /* dim_LastReceivedDateid, dim_StorageLocEntryDateid */
		  ,b.PEINH as amt_priceunit
	  	  ,ifnull(b.MBEW_LBKUM,0) as ct_totvalstkQty
     FROM MSKA a
          INNER JOIN dim_plant p ON a.MSKA_WERKS = p.PlantCode
          INNER JOIN dim_Company cmp ON cmp.CompanyCode = p.CompanyCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MSKA_MATNR AND dp.Plant = a.MSKA_WERKS
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_date dt ON a.MSKA_ERSDA = dt.DateValue and dt.CompanyCode = p.CompanyCode and p.plantcode=dt.plantcode_factory
          INNER JOIN dim_storagelocation sloc ON sloc.LocationCode = MSKA_LGORT AND sloc.Plant = MSKA_WERKS
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = cmp.Currency
          INNER JOIN dim_specialstock spstk ON spstk.specialstockindicator = a.MSKA_SOBKZ
	      CROSS JOIN tmp_GlobalCurr_001 gbl
	      CROSS JOIN tmp_stkcategory_001 stkc
 WHERE not exists (select 1 from tmp2_MBEW_NO_BWTAR b where b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea);

MERGE INTO TMP_INSERT_13_STOCK ST
USING
	(SELECT t.fact_inventoryagingid,ifnull(z.exchangeRate,1) exchangeRate
	 FROM TMP_INSERT_13_STOCK t
		LEFT JOIN tmp_getExchangeRate1 z ON z.pFromCurrency = t.CurrencyCode AND z.pToCurrency = t.pGlobalCurrency AND z.pDate = current_date AND z.fact_script_name = 'bi_populate_inventoryaging_fact'
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.amt_ExchangeRate_GBL = SRC.exchangeRate;

MERGE INTO TMP_INSERT_13_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(po.Dim_PurchaseOrgid,1) Dim_PurchaseOrgid
	 FROM TMP_INSERT_13_STOCK t
		LEFT JOIN dim_purchaseorg po ON po.PurchaseOrgCode = t.PurchOrg
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_PurchaseOrgid = SRC.Dim_PurchaseOrgid;

MERGE INTO TMP_INSERT_13_STOCK ST
USING
	(SElECT t.fact_inventoryagingid, ifnull(mm.Dim_MovementIndicatorid, 1) Dim_MovementIndicatorid, ifnull(mm.dim_DateIDPostingDate, t.dim_dateid) dim_DateIDPostingDate, ifnull(mm.MovementType, 'Not Set') MovementType
	 FROM TMP_INSERT_13_STOCK t
		LEFT JOIN TMP_fact_mm_postingdate_2 mm ON mm.dim_partid = t.Dim_PartID and mm.RowSeqNo = 1
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.Dim_MovementIndicatorid = SRC.Dim_MovementIndicatorid,
		ST.dim_LastReceivedDateid = SRC.dim_DateIDPostingDate,
		ST.dim_StorageLocEntryDateid = SRC.dim_DateIDPostingDate,
		ST.dd_MovementType = SRC.MovementType;

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	ct_BlockedStock,
	ct_StockInTransfer,
	ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock,
	ct_UnrestrictedConsgnStock,
	ct_BlockedStockReturns,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StockInTransferAmt,
	amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_ValuationType,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dd_MovementType,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_StockCategoryid,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate)
SELECT ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	ct_BlockedStock,
	ct_StockInTransfer,
	ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp,
	ct_RestrictedConsgnStock,
	ct_UnrestrictedConsgnStock,
	ct_BlockedStockReturns,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_BlockedStockAmt,
	amt_BlockedStockAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StockInTransferAmt,
	amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt,
	amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_ValuationType,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dd_MovementType,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_Currencyid_TRA,
	dim_Currencyid_GBL,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_StockCategoryid,
	fact_inventoryagingid,
	amt_ExchangeRate_GBL,
	amt_ExchangeRate
FROM TMP_INSERT_13_STOCK s
where not exists (select 1 from  fact_inventoryaging_tmp_populate t where
t.dim_partid=s.dim_partid and t.dim_plantid=s.dim_plantid and t.dim_StorageLocationid=s.dim_StorageLocationid and t.dd_BatchNo=s.dd_BatchNo);



UPDATE fact_inventoryaging_tmp_populate ig
SET ig.dim_profitcenterid = pc.dim_profitcenterid
FROM
fact_inventoryaging_tmp_populate ig,
dim_part pt,
Dim_ProfitCenter pc
WHERE ig.dim_PartId = pt.Dim_PartId
 AND pc.ProfitCenterCode = pt.ProfitCenterCode
 AND pc.validto >= current_date
 AND pc.RowIsCurrent = 1;

UPDATE fact_inventoryaging_tmp_populate ig
SET ig.dim_profitcenterid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ig.dim_profitcenterid IS NULL;


UPDATE fact_inventoryaging_tmp_populate
SET Dim_DateidActualRelease = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE Dim_DateidActualRelease is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_DateidActualStart = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE Dim_DateidActualStart is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_ProdOrdernumber = 'Not Set'
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ProdOrdernumber is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_ProdOrderitemno = 0
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_ProdOrderitemno is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_Plannedorderno = 'Not Set'
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_Plannedorderno is null;

UPDATE fact_inventoryaging_tmp_populate
SET dim_productionorderstatusid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dim_productionorderstatusid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_productionordertypeid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE Dim_productionordertypeid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_profitcenterid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE Dim_profitcenterid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_partsalesid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE Dim_partsalesid is null;

/* Octavian: Every Angle Transition Addons */
update fact_inventoryaging_tmp_populate ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp, fact_inventoryaging_tmp_populate ia
where ia.dim_partid = dp.dim_partid and
ia.dd_batchno = b.batchnumber and
dp.partnumber = b.partnumber
and dp.plant = b.plantcode
and ia.dim_batchid <> b.dim_batchid;

update fact_inventoryaging_tmp_populate ia
set dd_deletionflag = ifnull(m.LVORM,'Not Set'),
ia.dw_update_date = current_timestamp
from MKOL m, dim_part dp, dim_storagelocation sl, dim_specialstock ss, dim_vendor vd, fact_inventoryaging_tmp_populate ia
where ia.dim_partid = dp.dim_partid
and dp.partnumber = ifnull(m.matnr,'Not Set')
and dp.plant = ifnull(m.werks,'Not Set')
and ia.dim_storagelocationid = sl.dim_storagelocationid
and sl.locationcode = ifnull(m.LGORT,'Not Set')
and ia.dd_batchno = ifnull(m.CHARG,'Not Set')
and ia.dim_specialstockid = ss.dim_specialstockid
and ss.specialstockindicator = ifnull(m.SOBKZ,'Not Set')
and ia.dim_vendorid = vd.dim_vendorid
and vd.vendornumber = ifnull(m.LIFNR,'Not Set')
and dd_deletionflag <> ifnull(m.LVORM,'Not Set');

update fact_inventoryaging_tmp_populate ia
set dd_deletionflag = ifnull(m.MARD_LVORM,'Not Set'),
ia.dw_update_date = current_timestamp
from MARD m, dim_part dp, dim_storagelocation sl, fact_inventoryaging_tmp_populate ia
where ia.dim_partid = dp.dim_partid
and dp.partnumber = ifnull(m.MARD_MATNR,'Not Set')
and dp.plant = ifnull(m.MARD_WERKS,'Not Set')
and ia.dim_storagelocationid = sl.dim_storagelocationid
and sl.locationcode = ifnull(m.MARD_LGORT,'Not Set')
and dd_deletionflag <> ifnull(m.MARD_LVORM,'Not Set');

update fact_inventoryaging_tmp_populate ia
set dd_deletionflag = ifnull(m.MCHB_LVORM,'Not Set'),
ia.dw_update_date = current_timestamp
from MCHB m, dim_part dp, dim_storagelocation sl, fact_inventoryaging_tmp_populate ia
where ia.dim_partid = dp.dim_partid
and dp.partnumber = ifnull(m.MCHB_MATNR,'Not Set')
and dp.plant = ifnull(m.MCHB_WERKS,'Not Set')
and ia.dim_storagelocationid = sl.dim_storagelocationid
and sl.locationcode = ifnull(m.MCHB_LGORT,'Not Set')
and dd_batchno = ifnull(m.MCHB_CHARG,'Not Set')
and dd_deletionflag <> ifnull(m.MCHB_LVORM,'Not Set');

update fact_inventoryaging_tmp_populate ia
set dd_stockdeletionflag = ifnull(m.MARC_LVORM,'Not Set'),
ia.dw_update_date = current_timestamp
from MARC m, dim_part dp, fact_inventoryaging_tmp_populate ia
where ia.dim_partid = dp.dim_partid
and dp.partnumber = ifnull(m.MARC_MATNR,'Not Set')
and dp.plant = ifnull(m.MARC_WERKS,'Not Set')
and dd_stockdeletionflag <> ifnull(m.MARC_LVORM,'Not Set');

update fact_inventoryaging_tmp_populate ia
set dd_storagebin = ifnull(m.MARD_LGPBE,'Not Set'),
ia.dw_update_date = current_timestamp
from MARD m, dim_part dp, dim_storagelocation sl, fact_inventoryaging_tmp_populate ia
where ia.dim_partid = dp.dim_partid
and dp.partnumber = ifnull(m.MARD_MATNR,'Not Set')
and dp.plant = ifnull(m.MARD_WERKS,'Not Set')
and ia.dim_storagelocationid = sl.dim_storagelocationid
and sl.locationcode = ifnull(m.MARD_LGORT,'Not Set')
and dd_storagebin <> ifnull(m.MARD_LGPBE,'Not Set');

/*update dim_storagelocation sl
set sl.mrpexclude = ifnull(MARD_DISKZ,'Not Set'),
dw_update_date = current_timestamp
from (SELECT DISTINCT MARD_LGORT,MARD_WERKS,MARD_DISKZ FROM MARD WHERE MARD_DISKZ is not null) m, dim_storagelocation sl
where ifnull(m.MARD_WERKS,'Not Set') = sl.plant and ifnull(m.MARD_LGORT,'Not Set') = sl.locationcode
and sl.mrpexclude <> ifnull(MARD_DISKZ,'Not Set')*/

MERGE INTO dim_storagelocation sl
USING ( SELECT sl.plant, sl.locationcode, MAX( IFNULL( MARD_DISKZ, 'Not Set' )) AS mrpexclude
          FROM (SELECT DISTINCT MARD_LGORT, MARD_WERKS, MARD_DISKZ
                  FROM MARD
                  WHERE MARD_DISKZ IS NOT NULL ) m, dim_storagelocation sl
         WHERE IFNULL( m.MARD_WERKS, 'Not Set' ) = sl.plant
           AND IFNULL( m.MARD_LGORT, 'Not Set' ) = sl.locationcode
           AND sl.mrpexclude <> IFNULL( MARD_DISKZ, 'Not Set' )
         GROUP BY sl.plant, sl.locationcode ) x 
   ON sl.plant = x.plant
  AND sl.locationcode = x.locationcode
 WHEN MATCHED THEN UPDATE
  SET sl.mrpexclude = x.mrpexclude, 
      sl.dw_update_date = CURRENT_TIMESTAMP
WHERE sl.mrpexclude <> x.mrpexclude;

update fact_inventoryaging_tmp_populate ia
set dd_inventcorrfactor = ifnull(m.MARD_BSKRF,0),
ia.dw_update_date = current_timestamp
from MARD m, dim_part dp, dim_storagelocation sl, fact_inventoryaging_tmp_populate ia
where ia.dim_partid = dp.dim_partid
and dp.partnumber = ifnull(m.MARD_MATNR,'Not Set')
and dp.plant = ifnull(m.MARD_WERKS,'Not Set')
and ia.dim_storagelocationid = sl.dim_storagelocationid
and sl.locationcode = ifnull(m.MARD_LGORT,'Not Set')
and dd_inventcorrfactor <> ifnull(m.MARD_BSKRF,0);


/* Stock Category dim */

update dim_stockcategory
set description = CASE WHEN categorycode = 'MARD' THEN 'Normal Stock'
					   WHEN categorycode = 'MCHB' THEN 'Batch Stock'
					   WHEN categorycode = 'MSKA' THEN 'Sales Order Stock'
					   WHEN categorycode = 'MSKU' THEN 'Returnal Packaging'
					   WHEN categorycode = 'MSLB' THEN 'Material Provided to Vendor'
					   WHEN categorycode = 'MARC' THEN 'Stock In Transit'
					   END
where categorycode in ('MARD','MCHB','MSKA','MSKU','MSLB','MARC');

update fact_inventoryaging_tmp_populate ia
set ia.dim_stockcategoryid = (select v.dim_stockcategoryid from dim_stockcategory v where v.categorycode = 'MCHB'),
ia.dw_update_date = current_timestamp
from dim_stockcategory sc, fact_inventoryaging_tmp_populate ia
where ia.dim_stockcategoryid = sc.dim_stockcategoryid
and sc.categorycode = 'MARD' and ia.dd_batchno <> 'Not Set';

/*Georgiana 01 Nov 2016 moving these inserts here in otder to update the status too-BI-4460*/

/* insert Stock in Transit IRU from Atlas Inventori KPI - 04.03.2015 */
 insert into dim_stockcategory
select distinct (select max(dim_stockcategoryid) + 1 from dim_stockcategory),
'MinMax' description,'MM' categorycode,null,null,1,current_timestamp,current_timestamp
from dim_stockcategory a
where not exists (select 1 from dim_stockcategory x where x.description = a.description);

 delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
			   SELECT 'fact_inventoryaging_tmp_populate', ifnull(max(fact_inventoryagingid), 0)
				 FROM fact_inventoryaging_tmp_populate;


  insert into fact_inventoryaging_tmp_populate
 (fact_inventoryagingid, dim_partid,dim_plantid,dd_batchno,ct_intransitstockqty, dim_stockcategoryid)
select (SELECT max_id
				FROM NUMBER_FOUNTAIN
				WHERE table_name = 'fact_inventoryaging_tmp_populate'
				) + row_number() over(ORDER BY '') AS fact_inventoryagingid, x.* from (
 select distinct a.dim_partid,a.dim_plantid, 'Not Set' dd_batchno,
 a.ct_intransitstockqty, ds.dim_stockcategoryid
  from
 fact_inventoryatlas a
  inner join dim_stockcategory ds on  ds.categorycode=a.categorycode
 where a.ct_intransitstockqty <>0
and not exists (SELECT 1 FROM fact_inventoryaging_tmp_populate c2
			WHERE c2.Dim_Partid = a.Dim_Partid AND c2.dim_plantid = a.dim_plantid
			and dd_batchno='Not Set')) x;

merge into fact_inventoryaging_tmp_populate f
using (select distinct f.fact_inventoryagingid,a.ct_intransitstockqty
from
 fact_inventoryatlas a, fact_inventoryaging_tmp_populate f
 where f.Dim_Partid = a.Dim_Partid AND f.dim_plantid = a.dim_plantid and f.dd_batchno='Not Set') t
 on t.fact_inventoryagingid=f.fact_inventoryagingid
 when matched then update set f.ct_intransitstockqty=t.ct_intransitstockqty
 where f.ct_intransitstockqty<>t.ct_intransitstockqty;

/* end insert Stock in Transit IRU from Atlas Inventori KPI - 04.03.2015 */
/* 01 Nov 2016 End of Changes*/

/* START insert the missing combiantions from MinMax for plants BR10, IN10 and IT10*/ 
delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
			   SELECT 'fact_inventoryaging_tmp_populate', ifnull(max(fact_inventoryagingid), 0)
				 FROM fact_inventoryaging_tmp_populate;

  insert into fact_inventoryaging_tmp_populate
 (fact_inventoryagingid, dim_partid,dim_plantid,dd_batchno, dim_stockcategoryid)
select (SELECT max_id
				FROM NUMBER_FOUNTAIN
				WHERE table_name = 'fact_inventoryaging_tmp_populate'
				) + row_number() over(ORDER BY '') AS fact_inventoryagingid, x.* from (
 select distinct a.dim_partid,a.dim_plantid, 'Not Set' dd_batchno,
ds.dim_stockcategoryid
  from
 fact_inventoryatlas a
  inner join dim_stockcategory ds on  ds.categorycode=a.categorycode
 where a.ct_totalstockiru <> 0 and 
not exists (SELECT 1 FROM fact_inventoryaging_tmp_populate c2
			WHERE c2.Dim_Partid = a.Dim_Partid AND c2.dim_plantid = a.dim_plantid
			and dd_batchno='Not Set')) x;

/* END insert the missing combiantions from MinMax for plants BR10, IN10 and IT10*/ 

insert into dim_stockcategory
select distinct (select max(dim_stockcategoryid) + 1 from dim_stockcategory),
'Stock in Transfer' description,'MARC-UMLMC' categorycode,null,null,1,current_timestamp,current_timestamp from dim_stockcategory a
where not exists (select 1 from dim_stockcategory x where x.description = a.description);

merge into  fact_inventoryaging_tmp_populate f
using (
select distinct f.fact_inventoryagingid, d.dim_stockcategoryid
from (select dim_stockcategoryid from dim_stockcategory c where c.categorycode = 'MARC-UMLMC') d,fact_inventoryaging_tmp_populate f
where ct_StockInTransfer <> 0
) t
on t.fact_inventoryagingid=f.fact_inventoryagingid
when matched then update
set f.dim_stockcategoryid = t.dim_stockcategoryid
where f.dim_stockcategoryid = t.dim_stockcategoryid;

/* Status */

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock = 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Blocked';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'In Transfer',
dw_update_date = current_timestamp
where ct_blockedstock = 0 and ct_stockqty = 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'In Transfer';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Quality Inspection',
dw_update_date = current_timestamp
where ct_blockedstock = 0 and ct_stockqty = 0 and ct_stockintransfer = 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Quality Inspection';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Restricted Use',
dw_update_date = current_timestamp
where ct_blockedstock = 0 and ct_stockqty = 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'In Transit',
dw_update_date = current_timestamp
where ct_blockedstock = 0 and ct_stockqty = 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'In Transit';




UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Blocked';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & In Transfer',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock = 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & In Transfer';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Quality Inspection',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock = 0 and ct_stockintransfer = 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Quality Inspection';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock = 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Restricted Use';


UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & In Transit',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock = 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Unrestricted & In Transit';


UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & In Transfer',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Blocked & In Transfer';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & Quality Inspection',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer = 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Blocked & Quality Inspection';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Blocked & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Blocked & In Transit';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'In Transfer & Quality Inspection',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'In Transfer & Quality Inspection';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'In Transfer & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'In Transfer & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'In Transfer & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'In Transfer & In Transit';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Quality Inspection & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer = 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Quality Inspection & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Quality Inspection & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer = 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Quality Inspection & In Transit';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Restricted Use & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock <> 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Restricted Use & In Transit';

/* */

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & In Transfer',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Blocked & In Transfer';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & Quality Inspection',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer = 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Blocked & Quality Inspection';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer = 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Blocked & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & In Transit',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer = 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Unrestricted & Blocked & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & In Transfer & Quality Inspection',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Blocked & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & In Transfer & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Blocked & In Transfer & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & In Transfer & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Blocked & In Transfer & In Transit';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'In Transfer & Quality Inspection & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'In Transfer & Quality Inspection & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'In Transfer & Quality Inspection & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'In Transfer & Quality Inspection & In Transit';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Quality Inspection & Restricted Use & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer = 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock <> 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Quality Inspection & Restricted Use & In Transit';

/* combination of 4 */
UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & In Transfer & Quality Inspection',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  = 0
and dd_status <> 'Unrestricted & Blocked & In Transfer & Quality Inspection';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & In Transfer & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Blocked & In Transfer & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & In Transfer & In Transit',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp = 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Unrestricted & Blocked & In Transfer & In Transit';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & In Transfer & Quality Inspection & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Blocked & In Transfer & Quality Inspection & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & In Transfer & Quality Inspection & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Blocked & In Transfer & Quality Inspection & In Transit';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'In Transfer & Quality Inspection & Restricted Use & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock = 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock <> 0 and ct_intransitstockqty  <> 0
and dd_status <> 'In Transfer & Quality Inspection & Restricted Use & In Transit';

/* combination of 5 */

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & In Transfer & Quality Inspection & Restricted Use',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock <> 0 and ct_StockInTransit = 0
and dd_status <> 'Unrestricted & Blocked & In Transfer & Quality Inspection & Restricted Use';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & In Transfer & Quality Inspection & In Transit',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock = 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Unrestricted & Blocked & In Transfer & Quality Inspection & In Transit';

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Blocked & In Transfer & Quality Inspection & Restricted Use & In Transit',
dw_update_date = current_timestamp
where ct_stockqty = 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock <> 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Blocked & In Transfer & Quality Inspection & Restricted Use & In Transit';

/* combination of 6 */

UPDATE fact_inventoryaging_tmp_populate
set dd_status = 'Unrestricted & Blocked & In Transfer & Quality Inspection & Restricted Use & In Transit',
dw_update_date = current_timestamp
where ct_stockqty <> 0 and ct_blockedstock <> 0 and ct_stockintransfer <> 0 and ct_stockInQInsp <> 0 and ct_TotalRestrictedStock <> 0 and ct_intransitstockqty  <> 0
and dd_status <> 'Unrestricted & Blocked & In Transfer & Quality Inspection & Restricted Use & In Transit';

/* Octavian, Madalina 29 Jul 2016 - BI-3525 */
/* composed statuses need to be splited in multiple lines */
/*delete from number_fountain where table_name = 'fact_inventoryaging'

insert into number_fountain
select 'fact_inventoryaging', ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging */

update NUMBER_FOUNTAIN
set max_id =
(
select 	ifnull(max(f.fact_inventoryagingid),
               ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inventoryaging_tmp_populate f
)
where table_name = 'fact_inventoryaging_tmp_populate';

/* the lines from fact containing combined statuses are inserted as splited lines into the temporary table */
drop table if exists tmp_fact_inventoryaging_for_status;
create table tmp_fact_inventoryaging_for_status as
SELECT (SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (order by '') ID,
newstatus as dd_status1,
CASE WHEN newstatus = 'Unrestricted' THEN ct_stockqty ELSE 0 END ct_stockqty1,
CASE WHEN newstatus = 'Blocked' THEN ct_blockedstock ELSE 0 END ct_blockedstock1,
CASE WHEN newstatus = 'In Transfer' THEN ct_stockintransfer ELSE 0 END ct_stockintransfer1,
CASE WHEN newstatus = 'Quality Inspection' THEN ct_stockInQInsp ELSE 0 END ct_stockInQInsp1,
CASE WHEN newstatus = 'Restricted Use' THEN ct_TotalRestrictedStock ELSE 0 END ct_TotalRestrictedStock1,
CASE WHEN newstatus = 'In Transit' THEN ct_StockInTransit ELSE 0 END ct_StockInTransit1
,t.*
from
(SELECT
trim(SUBSTRING(CONCAT('& ', dd_status, ' &'),1+INSTR(CONCAT('& ', dd_status, ' &'),'&',1,n.digit+1),INSTR(CONCAT('& ', dd_status, ' &'),'&',1,n.digit+2)-INSTR(CONCAT('& ', dd_status, ' &'),'&',1,n.digit+1)-1)) newstatus,
n.digit,ia.*
FROM
fact_inventoryaging_tmp_populate ia INNER JOIN
(SELECT 0 digit
UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6) n
ON LENGTH(REPLACE(dd_status,'&','')) <= LENGTH(dd_status)-n.digit
WHERE dd_status like '%&%'
) t;


/* update the old, composed statuses, with the new ones */
update tmp_fact_inventoryaging_for_status
set ct_stockqty = ct_stockqty1,
	ct_blockedstock = ct_blockedstock1,
	ct_stockintransfer = ct_stockintransfer1,
	ct_stockInQInsp = ct_stockInQInsp1,
	ct_TotalRestrictedStock = ct_TotalRestrictedStock1,
	ct_StockInTransit = ct_StockInTransit1,
	dd_status = dd_status1,
	fact_inventoryagingid = id;

/* drop the fields from temp table, in order to reach the fact structure */

alter table tmp_fact_inventoryaging_for_status drop column id cascade;

alter table tmp_fact_inventoryaging_for_status drop column dd_status1 cascade;

alter table tmp_fact_inventoryaging_for_status drop column ct_stockqty1 cascade;

alter table tmp_fact_inventoryaging_for_status drop column ct_blockedstock1 cascade;

alter table tmp_fact_inventoryaging_for_status drop column ct_stockintransfer1 cascade;

alter table tmp_fact_inventoryaging_for_status drop column ct_stockInQInsp1 cascade;

alter table tmp_fact_inventoryaging_for_status drop column ct_TotalRestrictedStock1 cascade;

alter table tmp_fact_inventoryaging_for_status drop column ct_StockInTransit1 cascade;

alter table tmp_fact_inventoryaging_for_status drop column newstatus cascade;

alter table tmp_fact_inventoryaging_for_status drop column digit cascade;


/* insert into the fact the new splited lines and delete the ones with composed statuses */
insert into fact_inventoryaging_tmp_populate
select * from tmp_fact_inventoryaging_for_status;

delete from fact_inventoryaging_tmp_populate where dd_status like '%&%';

/* END 29 Jul 2016 */

update fact_inventoryaging_tmp_populate f
SET amt_grossweight_marm = ifnull(MARM_BRGEW,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_part dp,MARM m,dim_unitofmeasure uom, fact_inventoryaging_tmp_populate f
where m.MARM_MATNR = dp.partnumber
and dp.dim_partid = f.dim_partid
and m.MARM_MEINH = uom.uom
and f.dim_unitofmeasureid = uom.dim_unitofmeasureid
and f.amt_grossweight_marm <> ifnull(MARM_BRGEW,0);
/* Octavian: Every Angle Transition Addons */



/*Georgiana EA Changes adding dim_vendormasterid*/

UPDATE fact_inventoryaging_tmp_populate t
SET t.dim_vendormasterid = vm.dim_vendormasterid,
dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_company dc, dim_vendormaster vm, dim_vendor v, fact_inventoryaging_tmp_populate t
where t.dim_vendorid=v.dim_vendorid
AND t.dim_companyid = dc.dim_Companyid
AND v.vendornumber = vm.vendornumber
AND dc.companycode= vm.companycode
and t.dim_vendormasterid <> vm.dim_vendormasterid;

/*Georgiana End of Changes*/



/* Drop original table and rename staging table to orig table name */
drop table if exists fact_inventoryaging;
rename table fact_inventoryaging_tmp_populate to fact_inventoryaging;

/* start changes 20 Feb for dim_DateidExpiryDate */

/*update fact_inventoryaging i
from	mch1 m,
	dim_Part dp1,
	dim_company dc,
	dim_date dt1
set dim_DateidExpiryDate = dt1.dim_dateid
	,i.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*where i.dim_Partid = dp1.dim_Partid
	and dp1.partnumber =  m.mch1_matnr
	and i.dd_BatchNo = m.mch1_charg
	and dt1.datevalue = m.mch1_vfdat
	and dt1.CompanyCode = dc.CompanyCode
	and i.dim_companyid = dc.dim_companyid
	*/
/*drop table if exists tmp_fact_inventoryaging_upd_expdate
create table tmp_fact_inventoryaging_upd_expdate as
	select  dt1.dim_dateid,
	(case when m.mch1_vfdat='9999-12-31' then '0001-01-01' else m.mch1_vfdat end) mch1_vfdat ,*/ /*Georgiana Changes 09 Jun 2015*/
	/*i.dim_Partid,
	i.dd_BatchNo,
	i.dim_companyid,
	ROW_NUMBER() OVER(PARTITION BY i.dim_Partid,i.dd_BatchNo,i.dim_companyid
				order by m.mch1_vfdat desc)  RowSeqNo
	from
	fact_inventoryaging i, mch1 m,
	dim_Part dp1,
	dim_company dc,
	dim_date dt1
where i.dim_Partid = dp1.dim_Partid
	and dp1.partnumber =  m.mch1_matnr
	and i.dd_BatchNo = m.mch1_charg
	and dt1.datevalue = m.mch1_vfdat
	and dt1.CompanyCode = dc.CompanyCode and dt1.plantcode_factory=dp1.plant
	and i.dim_companyid = dc.dim_companyid*/

/*	delete
	from tmp_fact_inventoryaging_upd_expdate
	where rowseqno > 1*/

/*update fact_inventoryaging i
set dim_DateidExpiryDate = u.dim_dateid
	,i.dw_update_date = current_timestamp*/ /* Added automatically by update_dw_update_date.pl*/
/*from	tmp_fact_inventoryaging_upd_expdate u, fact_inventoryaging i
where i.dim_Partid = u.dim_Partid
	and i.dd_BatchNo = u.dd_BatchNo
	and i.dim_companyid = u.dim_companyid*/

drop table if exists tmp_fact_inventoryaging_upd_expdate;
create table tmp_fact_inventoryaging_upd_expdate as
	select  dt1.dim_dateid,
	(case when m.mch1_vfdat='9999-12-31' then '0001-01-01' else m.mch1_vfdat end) mch1_vfdat , /*Georgiana Changes 09 Jun 2015*/
	i.dim_Partid,
	i.dd_BatchNo,
	i.dim_plantid,
	ROW_NUMBER() OVER(PARTITION BY i.dim_Partid,i.dd_BatchNo,i.dim_companyid
				order by m.mch1_vfdat desc)  RowSeqNo
	from
	fact_inventoryaging i, mch1 m,
	dim_Part dp1,
	dim_plant pl,
	dim_date dt1
where i.dim_Partid = dp1.dim_Partid
	and dp1.partnumber =  m.mch1_matnr
	and i.dd_BatchNo = m.mch1_charg
	and dt1.datevalue = m.mch1_vfdat
	and i.dim_plantid = pl.dim_plantid
	and dt1.CompanyCode = pl.CompanyCode and dt1.plantcode_factory=pl.plantcode;

	delete
	from tmp_fact_inventoryaging_upd_expdate
	where rowseqno > 1;

update fact_inventoryaging i
set dim_DateidExpiryDate = u.dim_dateid
	,i.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from	tmp_fact_inventoryaging_upd_expdate u, fact_inventoryaging i
where i.dim_Partid = u.dim_Partid
	and i.dd_BatchNo = u.dd_BatchNo
	and i.dim_plantid = u.dim_plantid;

UPDATE fact_inventoryaging i
set i.Dim_vendorid = v.dim_vendorid
from
fact_inventoryaging i
		INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid
		INNER JOIN dim_vendor v ON dp1.ManfacturerNumber =  v.vendornumber
where v.rowiscurrent = 1
AND   i.Dim_VendorId = 1;

update fact_inventoryaging i
set dim_DateidExpiryDate = 1 where dim_DateidExpiryDate is null;

/*Andrian 22 Mar 2016*/
update fact_inventoryaging a
set ct_stockdayssupply = d.mdkp_berw1
from dim_part b,dim_plant c, (SELECT mdkp_matnr, mdkp_plwrk, max(mdkp_berw1) AS mdkp_berw1 FROM MDKP group by mdkp_matnr,mdkp_plwrk) d, fact_inventoryaging a
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
and b.partnumber = d.mdkp_matnr
and c.plantcode = d.mdkp_plwrk
and ct_stockdayssupply <> d.mdkp_berw1;

/* Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */

UPDATE fact_inventoryaging am
SET am.std_exchangerate_dateid = dt.dim_dateid
FROM fact_inventoryaging am INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode and p.plantcode=dt.plantcode_factory
WHERE dt.datevalue = current_date
AND am.std_exchangerate_dateid <> dt.dim_dateid;
/* Madalina 12 Jul 2016 - add the measure Value of Total Valuated Stock - BI-3413 */
/*
using (select distinct fact_inventoryagingid, MBEW_SALK3
from dim_batch bt, mara_marc_mbew, fact_inventoryaging fi
where bt.partnumber = ifnull(mbew_matnr,'Not Set')
and bt.plantcode = ifnull(mbew_bwkey,'Not Set')
and bt.batchnumber = ifnull(mbew_bwtar,'Not Set')
and fi.dim_batchid = bt.dim_batchid) t
on t.fact_inventoryagingid=fi.fact_inventoryagingid
when matched then update set  fi.amt_valueofvaluatedstock = ifnull(MBEW_SALK3,0)
where fi.amt_valueofvaluatedstock <> ifnull(MBEW_SALK3,0)
*/

drop table if exists tmp_rankstockvalue_mara_marc_mbew;
create table tmp_rankstockvalue_mara_marc_mbew as
select distinct m.mbew_bwkey, m.mbew_matnr, m.mbew_bwtar, m.MBEW_SALK3,
       row_number() over (partition by m.mbew_bwkey, m.mbew_matnr, m.mbew_bwtar
                    order by m.mara_laeda desc) as rownumber
from mara_marc_mbew m;

drop table if exists tmp_laststockvalue_mara_marc_mbew;
create table tmp_laststockvalue_mara_marc_mbew as
select * from tmp_rankstockvalue_mara_marc_mbew where rownumber = 1;

merge into fact_inventoryaging fi
 using (select distinct fact_inventoryagingid, MBEW_SALK3
 from dim_batch bt, tmp_laststockvalue_mara_marc_mbew m, fact_inventoryaging f
 where bt.partnumber = ifnull(m.mbew_matnr,'Not Set')
 and bt.plantcode = ifnull(m.mbew_bwkey,'Not Set')
 and bt.batchnumber = ifnull(m.mbew_bwtar,'Not Set')
 and f.dim_batchid = bt.dim_batchid) t
 on t.fact_inventoryagingid=fi.fact_inventoryagingid
 when matched then update set fi.amt_valueofvaluatedstock = ifnull(t.MBEW_SALK3,0)
 where fi.amt_valueofvaluatedstock <> ifnull(MBEW_SALK3,0);

drop table if exists tmp_laststockvalue_mara_marc_mbew;
drop table if exists tmp_rankstockvalue_mara_marc_mbew;

/* END 12 Jul 2016 */
/* Drop all the intermediate tables created */

drop table if exists tmp_getExchangeRate1_with_globalvar;
drop table if exists materialmovement_tmp_new;
drop table if exists a1;
drop table if exists a99;
drop table if exists b2;
drop table if exists b2;
drop table if exists b3;
drop table if exists b4;
drop table if exists b;
drop table if exists ia1;
drop table if exists pl_prt_mbew_no_bwtar;
drop table if exists tmp2_keph;
drop table if exists tmp_MBEW_NO_BWTAR;
drop table if exists tmp_MBEWH_NO_BWTAR;
drop table if exists tmp2_mbew_no_bwtar;
drop table if exists tmp2_tcurf;
drop table if exists tmp2_tcurr;
drop table if exists tmp3_mbew_no_bwtar;
drop table if exists tmp3_tcurr;
drop table if exists tmp4_dim_date;
drop table if exists tmp4_mbew_no_bwtar;
drop table if exists tmp_all_x;
drop table if exists tmp_all_x_2;
drop table if exists tmp_c1;
drop table if exists tmp_c2;
drop table if exists tmp_c3;
drop table if exists tmp_dim_date;
drop table if exists tmp_fact_mm_0;
drop table if exists tmp_fact_mm_1;
drop table if exists tmp_fact_mm_2;
drop table if exists tmp_fact_mm_postingdate_0;
drop table if exists tmp_fact_mm_postingdate_1;
drop table if exists tmp_fact_mm_postingdate_2;
drop table if exists tmp_fact_mm_postingdate_2_min;
drop table if exists tmp_keph;
drop table if exists tmp_lrd;
drop table if exists tmp_mard;
drop table if exists tmp_mbew_no_bwtar;
drop table if exists tmp_tbl_storagelocentrydate;
drop table if exists tmp_tcurf;
drop table if exists tmp_tcurr;
DROP TABLE IF EXISTS tmp_GlobalCurr_001;
DROP TABLE IF EXISTS tmp_stkcategory_001;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins_c1;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_del;
DROP TABLE IF EXISTS TMP_INSERT_1_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_2_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_3_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_4_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_5_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_6_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_7_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_8_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_9_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_10_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_11_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_12_STOCK;
DROP TABLE IF EXISTS TMP_INSERT_13_STOCK;

/*BI-4114 Gheorghe Alexandru-Alin*/

/*replace the comma_replace string back to commas*/
update IQV
set 
IQV_RISKCATDESC = replace(IQV_RISKCATDESC, 'comma_replace', ','),
IQV_COMMENT_REASON = replace(IQV_COMMENT_REASON, 'comma_replace', ','),
COMMENT_IQV_ACTION = replace(COMMENT_IQV_ACTION, 'comma_replace', ',');

 insert into dim_stockcategory
select distinct (select max(dim_stockcategoryid) + 1 from dim_stockcategory),
'Inventory Questionable Value' description,'IQV' categorycode,null,null,1,current_timestamp,current_timestamp 
from dim_stockcategory a
where not exists (select 1 from dim_stockcategory x where x.description = a.description);

delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryaging',ifnull(max(fact_inventoryagingid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
FROM fact_inventoryaging;

INSERT INTO fact_inventoryaging(fact_inventoryagingid,
dim_plantid,
dim_partid,
dd_BatchNo,
DD_IQVRISKCODE ,
DD_IQVRISKDESCRIPTION,
DD_IQVREASONCODE,
DD_IQVREASONDESCRIPTION,
DD_IQVREASONCOMMENT,
dd_iqvriskcatcode,
dd_iqvriskcatdescription,
--AMT_IQVPROVISION, 
--CT_IQVPROVISION,
CT_IQVFIXED,
/*new cols*/
dd_update_mode,
dd_batch_per_plant,
dd_Base_Unit,
dd_Responsible_Area,
dd_IQV_Action_Code,
dd_IQV_Action_Description,
dd_Comment_IQV_Action,
dd_Last_Changed_by,
dim_First_IQV_Review_Dateid,
dim_Last_IQV_Review_Dateid,
dim_IQV_Due_Dateid,
dim_Risk_Creation_Dateid,
/*new cols*/
dim_stockCATEGORYid)
SELECT 
(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging') + row_number() over (order by ''),
pl.dim_plantid, dp.dim_partid, 
ifnull(iqv_batch , 'Not Set'),
ifnull(iqv_risk_code , 'Not Set'),
ifnull(iqv_risk_description ,'Not Set'),
ifnull(iqv_reason_code ,'Not Set'),
ifnull(iqv_reason_description ,'Not Set'),
ifnull(iqv_comment_reason,'Not Set'),
ifnull(IQV_RISKCATCODE,'Not Set'),
ifnull(IQV_RISKCATDESC,'Not Set'),
--iqv_provision_amount ,
--iqv_provsion_qty ,
ifnull(iqv_fixed_qty,0),
/*new cols*/
ifnull(update_mode,'Not Set'),
ifnull(batch_per_plant,'Not Set'),
ifnull(Base_Unit,'Not Set'),
ifnull(Responsible_Area,'Not Set'),
ifnull(IQV_Action_Code,'Not Set'),
ifnull(IQV_Action_Description,'Not Set'),
ifnull(Comment_IQV_Action,'Not Set'),
ifnull(Last_Changed_by,'Not Set'),
1,
1,
1,
1,
/*new cols*/
dim_stockCATEGORYid
FROM IQV i, dim_part dp, dim_plant pl, dim_stockCATEGORY st
where ifnull(i.plant, 'Not Set') = pl.plantcode and 
ifnull(i.plant, 'Not Set') = dp.plant and
ifnull(i.material, 'Not Set') = dp.partnumber and
dp.plant = pl.plantcode and
st.categorycode = 'IQV'
and not exists(
select 1 from fact_inventoryaging f  where
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = ifnull(i.plant, 'Not Set') and
PL.PLANTCODE = ifnull(i.plant, 'Not Set') AND
DP.PARTNUMBER = ifnull(i.material, 'Not Set') AND
f.dd_batchno = ifnull(I.IQV_BATCH, 'Not Set'));


merge into fact_inventoryaging f
using(
select distinct
first_value(fact_inventoryagingid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryagingid,
first_value(iqv_batch) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as iqv_batch,
first_value(iqv_risk_code) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as iqv_risk_code ,
first_value(iqv_risk_description ) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as iqv_risk_description,
first_value(iqv_reason_code) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as iqv_reason_code,
first_value(iqv_reason_description)over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as iqv_reason_description ,
first_value(iqv_comment_reason) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as iqv_comment_reason,
first_value(IQV_RISKCATCODE) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as IQV_RISKCATCODE,
first_value(IQV_RISKCATDESC) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as IQV_RISKCATDESC,
--new cols
first_value(update_mode) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as update_mode,
first_value(batch_per_plant) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as batch_per_plant,
first_value(Base_Unit) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as Base_Unit,
first_value(Responsible_Area) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as Responsible_Area,
first_value(IQV_Action_Code) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as IQV_Action_Code,
first_value(IQV_Action_Description) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as IQV_Action_Description,
first_value(Comment_IQV_Action) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as Comment_IQV_Action,
first_value(Last_Changed_by) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as Last_Changed_by

from IQV i, dim_part dp, dim_plant pl, fact_inventoryaging f
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = ifnull(i.plant, 'Not Set') and
PL.PLANTCODE = ifnull(I.PLANT, 'Not Set') AND
DP.PARTNUMBER = ifnull(I.MATERIAL, 'Not Set') AND
f.dd_batchno = ifnull(I.IQV_BATCH, 'Not Set')) t
on t.fact_inventoryagingid = f.fact_inventoryagingid
when matched then 
update set
dd_BatchNo = ifnull(iqv_batch, 'Not Set'),
DD_IQVRISKCODE = ifnull(iqv_risk_code , 'Not Set'),
DD_IQVRISKDESCRIPTION = ifnull(iqv_risk_description, 'Not Set'),
DD_IQVREASONCODE = ifnull(iqv_reason_code, 'Not Set'),
DD_IQVREASONDESCRIPTION = ifnull(iqv_reason_description, 'Not Set'),
DD_IQVREASONCOMMENT = ifnull(iqv_comment_reason, 'Not Set'),
DD_IQVRISKCATCODE = ifnull(IQV_RISKCATCODE, 'Not Set'),
DD_IQVRISKCATDESCRIPTION = ifnull(IQV_RISKCATDESC, 'Not Set'),
/*new cols*/
dd_update_mode = ifnull(update_mode, 'Not Set'),
dd_batch_per_plant = ifnull(batch_per_plant, 'Not Set'),
dd_Base_Unit = ifnull(Base_Unit, 'Not Set'),
dd_Responsible_Area = ifnull(Responsible_Area, 'Not Set'),
dd_IQV_Action_Code = ifnull(IQV_Action_Code, 'Not Set'),
dd_IQV_Action_Description = ifnull(IQV_Action_Description, 'Not Set'),
dd_Comment_IQV_Action = ifnull(Comment_IQV_Action, 'Not Set'),
dd_Last_Changed_by = ifnull(Last_Changed_by, 'Not Set');


merge into fact_inventoryaging f
using(
select  f.dim_partid,f.dim_plantid,f.dd_batchno,
--avg(iqv_provision_amount)/count(*) as iqv_provision_amount,
--avg(iqv_provsion_qty)/count(*) as iqv_provsion_qty,
avg(iqv_fixed_qty)/count(*) as iqv_fixed_qty

from IQV i, dim_part dp, dim_plant pl, fact_inventoryaging f
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = ifnull(i.plant, 'Not Set') and
PL.PLANTCODE = ifnull(I.PLANT, 'Not Set') AND
DP.PARTNUMBER = ifnull(I.MATERIAL, 'Not Set') AND
f.dd_batchno = ifnull(I.IQV_BATCH, 'Not Set')
group by f.dim_partid,f.dim_plantid,f.dd_batchno) t
on  f.dim_partid=t.dim_partid and f.dim_plantid=t.dim_plantid and f.dd_batchno=t.dd_batchno
when matched then 
update set
--AMT_IQVPROVISION = iqv_provision_amount, 
--CT_IQVPROVISION = iqv_provsion_qty,
CT_IQVFIXED = iqv_fixed_qty
;


/*NEW COLS DIM_DATES*/
MERGE INTO fact_inventoryaging ST
USING
	(
SELECT DISTINCT first_value(f.fact_inventoryagingid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryagingid, 
first_value(dd.dim_dateid)  over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryaging f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = ifnull(i.plant, 'Not Set') and
PL.PLANTCODE = ifnull(I.PLANT, 'Not Set') AND
DP.PARTNUMBER = ifnull(I.MATERIAL, 'Not Set') AND
f.dd_batchno = ifnull(I.IQV_BATCH, 'Not Set')
AND dd.datevalue =  cast(ifnull(i.First_IQV_Review_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_First_IQV_Review_Dateid = SRC.dim_dateid
WHERE ST.dim_First_IQV_Review_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryaging ST
USING
	(
SELECT DISTINCT first_value(f.fact_inventoryagingid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryagingid, 
first_value(dd.dim_dateid)  over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryaging f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = ifnull(i.plant, 'Not Set') and
PL.PLANTCODE = ifnull(I.PLANT, 'Not Set') AND
DP.PARTNUMBER = ifnull(I.MATERIAL, 'Not Set') AND
f.dd_batchno = ifnull(I.IQV_BATCH, 'Not Set')
AND dd.datevalue =  cast(ifnull(i.Last_IQV_Review_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_Last_IQV_Review_Dateid = SRC.dim_dateid
WHERE ST.dim_Last_IQV_Review_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryaging ST
USING
	(
SELECT DISTINCT first_value(f.fact_inventoryagingid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryagingid, 
first_value(dd.dim_dateid)  over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryaging f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = ifnull(i.plant, 'Not Set') and
PL.PLANTCODE = ifnull(I.PLANT, 'Not Set') AND
DP.PARTNUMBER = ifnull(I.MATERIAL, 'Not Set') AND
f.dd_batchno = ifnull(I.IQV_BATCH, 'Not Set')
AND dd.datevalue =  cast(ifnull(i.IQV_Due_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_IQV_Due_Dateid = SRC.dim_dateid
WHERE ST.dim_IQV_Due_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryaging ST
USING
	(
SELECT DISTINCT first_value(f.fact_inventoryagingid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryagingid, 
first_value(dd.dim_dateid)  over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryaging f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = ifnull(i.plant, 'Not Set') and
PL.PLANTCODE = ifnull(I.PLANT, 'Not Set') AND
DP.PARTNUMBER = ifnull(I.MATERIAL, 'Not Set') AND
f.dd_batchno = ifnull(I.IQV_BATCH, 'Not Set')
AND dd.datevalue =  cast(ifnull(i.Risk_Creation_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryagingid = SRC.fact_inventoryagingid
WHEN MATCHED THEN UPDATE
	SET ST.dim_Risk_Creation_Dateid = SRC.dim_dateid
WHERE ST.dim_Risk_Creation_Dateid <> SRC.dim_dateid;









/*Georgiana Changes according to BI-4460*/

update dim_part
set TotalPlantStock=0
where TotalPlantStock <> 0;

MERGE INTO dim_part
USING
(
SELECT f_inv.dim_Partid,
sum(f_inv.ct_StockQty+f_inv.ct_StockInQInsp+f_inv.ct_BlockedStock+f_inv.ct_StockInTransfer) as TotalPlantStock_sum
FROM
fact_inventoryaging f_inv
group by f_inv.dim_Partid) fb
ON fb.dim_partid = dim_part.dim_partid
WHEN MATCHED THEN UPDATE
SET dim_part.TotalPlantStock = ifnull(fb.TotalPlantStock_sum,0);

update dim_Part
set TotalPlantStockAmt=0
where TotalPlantStockAmt<>0;

MERGE INTO dim_part
USING
(
SELECT f_inv.dim_Partid,
sum(f_inv.amt_StockValueAmt+f_inv.amt_StockInQInspAmt+f_inv.amt_BlockedStockAmt+f_inv.amt_StockInTransferAmt) as TotalPlantStockAmt_sum
FROM
fact_inventoryaging f_inv
group by f_inv.dim_Partid) fb
ON fb.dim_partid = dim_part.dim_partid
WHEN MATCHED THEN UPDATE
SET dim_part.TotalPlantStockAmt = ifnull(fb.TotalPlantStockAmt_sum,0);

/*START BI-5163 Alin*/
merge into fact_inventoryaging f using
(
select  fact_inventoryagingid, dd.dim_dateid
from dim_date dd, fact_inventoryaging f_invagng, dim_plant p
where
p.dim_plantid = f_invagng.dim_plantid
and dd.datevalue = cast(current_date+cast(round( f_invagng.ct_stockdayssupply ,0) as integer) as date)
and dd.companycode = p.companycode
and dd.plantcode_factory = p.plantcode
) t
on f.fact_inventoryagingid = t.fact_inventoryagingid
when matched then
update set f.dim_dateidstockcoverragedt = t.dim_dateid
where f.dim_dateidstockcoverragedt <> t.dim_dateid;
/*END BI-5163 Alin*/

/*Madalina 28 Sept 2017 - BI-7622 */
merge into fact_inventoryaging AS f_invagng using
(SELECT distinct f_invagng.Dim_Partid, f_invagng.Dim_plantid, count(1),
	ROUND( (AVG ( (prt.future_demandqty) ) -  (AVG( (prt.SafetyStock) )) ) *AVG( (prt.uomiru) ),0)/ count(1) ct_futuredemandqtyBUoM
	FROM fact_inventoryaging AS f_invagng 
	INNER JOIN Dim_Part AS prt ON f_invagng.Dim_Partid = prt.Dim_Partid 
	group by  f_invagng.Dim_Partid, f_invagng.Dim_plantid
) upd
on f_invagng.dim_partid = upd.dim_partid and f_invagng.dim_plantid = upd.dim_plantid
when matched then update
set f_invagng.ct_futuredemandqtyBUoM = upd.ct_futuredemandqtyBUoM;

/* APP-7835 Andrei */
merge into fact_inventoryaging f using
( select distinct f.fact_inventoryagingid, 
         first_value(d.dim_dateid) over (partition by ifnull(m.MCHB_MATNR,'Not Set'),ifnull(m.MCHB_WERKS,'Not Set'), ifnull(m.MCHB_CHARG,'Not Set'),
		 ifnull(m.MCHB_LGORT,'Not Set') order by m.MCHB_LAEDA desc) as dim_dateid
         from fact_inventoryaging f, MCHB m, dim_date d, dim_plant p, dim_part dp, dim_storagelocation sl
where m.MCHB_LAEDA = d.datevalue
and d.companycode = p.companycode
and d.plantcode_factory = p.plantcode
and p.dim_plantid = f.dim_plantid
and dp.dim_partid = f.dim_partid
and sl.dim_storagelocationid = f.dim_storagelocationid
and dp.partnumber = ifnull(m.MCHB_MATNR,'Not Set')
and dp.plant = ifnull(m.MCHB_WERKS,'Not Set')
and sl.locationcode = ifnull(m.MCHB_LGORT,'Not Set')
and f.dd_batchno = ifnull(m.MCHB_CHARG,'Not Set')
and p.plantcode = ifnull(m.MCHB_WERKS,'Not Set')
) upd
on f.fact_inventoryagingid = upd.fact_inventoryagingid
when matched then update set f.dim_dateidlastchange = upd.dim_dateid;

/*Logic was changed by Octavian Stepan, accordingly with APP-8819*/
 MERGE 
  INTO fact_inventoryaging f
 USING
(
SELECT DISTINCT  fact_inventoryagingid
                ,FIRST_VALUE (dp.dim_partsalesid) OVER (PARTITION BY fact_inventoryagingid ORDER BY '') AS dim_partsalesid
  FROM fact_inventoryaging fi 
       ,mvke_mara             mm
       --,mard                  ma,
       ,dim_partsales         dp
       ,dim_part              dpr
       ,dim_plant             dpl
       --,dim_storagelocation   s
 WHERE dp.partnumber              = IFNULL(mm.mvke_matnr,'Not Set')
   AND dp.deliveryplant           = IFNULL(mm.mvke_dwerk,'Not Set') 
   AND fi.dim_partid              = dpr.dim_partid
   AND fi.dim_plantid             = dpl.dim_plantid
   AND dpl.plantcode              = dpr.plant
   AND dpr.plant                  = IFNULL(mm.mvke_dwerk,'Not Set') 
   AND dpr.partnumber             = IFNULL(mm.mvke_matnr,'Not Set')
   AND dp.salesorgcode            = IFNULL(mm.MVKE_VKORG,'Not Set')
   --AND dp.partnumber              = IFNULL(ma.mard_matnr,'Not Set')  
   --AND dp.deliveryplant           = IFNULL(ma.mard_werks,'Not Set')
   --AND dp.distributionchannelcode = IFNULL(mm.mvke_vtweg,'Not Set')
   --AND fi.dim_storagelocationid   = s.dim_storagelocationid
   --AND s.locationcode             = ifnull(mard_lgort,'Not Set')
   --AND s.plant                    = dpl.plantcode
) t 
 ON   f.fact_inventoryagingid = t.fact_inventoryagingid
 WHEN MATCHED 
 THEN UPDATE SET f.dim_partsalesid = t.dim_partsalesid
      WHERE f.dim_partsalesid <> t.dim_partsalesid;
