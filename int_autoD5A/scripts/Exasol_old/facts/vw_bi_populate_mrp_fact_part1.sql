

 
 /**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Ashu  */
/*   Created On     : Jul 2013 */
/*   Description    : Stored Proc bi_populate_mrp_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*    05 May 2017     Liviu I      1.6                Add excluded shortages and excess back in (MDTB_RDMNG and MDTB_MNG03 <> 0) */
/*   24 May 2014     Lokesh	1.5		 Reverted MDTB_RDMNG and MDTB_MNG03 changes ( excluded shortages and excess again ) */
/*   20 May 2014	Lokesh    1.4		 Only handle MDTB_MNG03 = 0 here in part 1 and move <>0 to part b */
/*   8 May 2014	     Lokesh    1.3		 Changes for missing plants and demand elements	*/
/*   6 Sep 2013      Lokesh    1.2               Curr, Exchange rate changes 	*/
/*   6 Aug 2013      Lokesh    1.1               Moved leadtime logic to standard format				  */
/*                                               Order of scripts: mrp_fact_part1, vw_getLeadTime.bi_populate_mrp_fact.part1.sql */
/*                                              leadtime std, mrp_fact_part2,leadtime std, vw_getLeadTime.bi_populate_mrp_fact.part2,  mrp_fact_part3    */
/*   Jul   2013      Ashu      1.0               Existing code migrated to Vectorwise.				  */
/******************************************************************************************************************/

/* Note that this is no more present in mysql. So, for validation, refresh this table from vw prod */

drop table if exists tmp_distinct_MDTB;
create table tmp_distinct_MDTB as
select distinct x.* from MDTB x;

drop table if exists MDTB;
rename table tmp_distinct_MDTB to MDTB;


Drop table if exists tmpvariable_00e ;

Create table tmpvariable_00e(
pGlobalCurrency varchar(3) null,
tmpTotalCount integer null);

Insert into tmpvariable_00e values(null,null);

Update tmpvariable_00e
SET tmpTotalCount = ifnull((select count(*) from mdkp k inner join mdtb t on k.MDKP_DTNUM = t.MDTB_DTNUM),0);

Update tmpvariable_00e
SET pGlobalCurrency = ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');

DROP TABLE IF EXISTS TMP_MRP_Q1_UPD;
CREATE TABLE TMP_MRP_Q1_UPD
as
SELECT DISTINCT mrp.fact_mrpid
FROM fact_mrp mrp,tmpvariable_00e, dim_mrpexception mex, dim_mrpelement me, dim_date dn, dim_plant pl
  WHERE     tmpTotalCount > 0
        AND Dim_DateidActionClosed = 1
    AND mrp.dim_plantid = pl.dim_plantid
        AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
        AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
        AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
        AND mrp.dd_documentno <> 'Not Set'
        AND (ct_Completed <> 1 OR Dim_ActionStateid <> 3)
        AND NOT EXISTS
                    (SELECT 1
                      FROM mdtb t, mdkp m
                      WHERE     mrp.dd_documentno = t.MDTB_DELNR
                            AND mrp.dd_documentitemno = t.MDTB_DELPS
                            AND mrp.dd_scheduleno = t.MDTB_DELET
                            AND t.MDTB_DELNR IS NOT NULL
                            AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                            AND m.MDKP_DTNUM = t.MDTB_DTNUM
                            AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                            AND mrp.ct_QtyMRP = t.MDTB_MNG01
			    /* added in BI-5935*/
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
			  /* Commented out in BI-5935*/ 
                           /* AND t.MDTB_MNG03 = 0
                            AND t.MDTB_RDMNG = 0 */ 
                            AND to_date(dn.DateValue, 'MM-DD-YYYY') =
                                  to_date(ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00),
                                          ('0001-01-01')), 'MM-DD-YYYY')
              AND dn.CompanyCode = pl.CompanyCode
              AND dn.plantcode_Factory = pl.plantcode
                            AND mrp.Dim_ActionStateid = 2
                            AND mrp.dd_PlannScenarioLTP = m.MDKP_PLSCN);




/* Q1 - Upd ct_Completed */
UPDATE fact_mrp mrp
SET     ct_Completed = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From    TMP_MRP_Q1_UPD t, fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND mrp.ct_Completed <> 1;

/* Q1 - For the same rows where ct_Completed was updated above, update Dim_ActionStateid to 3 */
UPDATE fact_mrp mrp
SET     Dim_ActionStateid = 3
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From    TMP_MRP_Q1_UPD t, fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND Dim_ActionStateid <> 3;


UPDATE fact_mrp mrp
SET Dim_DateidActionClosed = dt.Dim_Dateid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM TMP_MRP_Q1_UPD t,dim_date dt, dim_company cc,fact_mrp mrp,dim_plant pl
WHERE mrp.fact_mrpid = t.fact_mrpid
AND dt.DateValue = current_date
AND cc.Dim_Companyid = mrp.Dim_Companyid
AND cc.CompanyCode = dt.CompanyCode
and pl.dim_plantid=mrp.dim_plantid
and pl.companycode=dt.companycode
and dt.plantcode_factory=pl.plantcode
AND Dim_DateidActionClosed <> dt.Dim_Dateid;





DROP TABLE IF EXISTS TMP_MRP_Q2_UPD;
CREATE TABLE TMP_MRP_Q2_UPD
as
SELECT DISTINCT mrp.fact_mrpid
FROM fact_mrp mrp,tmpvariable_00e, dim_mrpexception mex, dim_mrpelement me, dim_date dn,dim_part dp, dim_plant pl
WHERE  tmpTotalCount > 0
AND Dim_DateidActionClosed = 1
AND mrp.dim_plantid = pl.dim_plantid
AND mrp.Dim_Partid = dp.Dim_Partid
AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
AND mrp.dd_documentno = 'Not Set'
AND  ct_Completed <> 1
AND NOT EXISTS
                    (SELECT 1
                      FROM mdtb t, mdkp m
                      WHERE dp.PartNumber = m.MDKP_MATNR
                            AND dp.Plant = m.MDKP_PLWRK
                            and t.MDTB_DELNR is null
                            AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                            AND m.MDKP_DTNUM = t.MDTB_DTNUM
                            AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                            AND mrp.ct_QtyMRP = t.MDTB_MNG01
			    /* added in BI-5935*/
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
			  /* Commented out in BI-5935*/ 
                           /* AND t.MDTB_MNG03 = 0
                            AND t.MDTB_RDMNG = 0 */ 
                            AND to_date(dn.DateValue, 'MM-DD-YYYY')=
                                  to_date(ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00),
                                          ('0001-01-01')),'MM-DD-YYYY' )
              AND dn.CompanyCode = pl.CompanyCode
              AND dn.plantcode_Factory = pl.plantcode
                            AND mrp.Dim_ActionStateid = 2
                            AND mrp.dd_PlannScenarioLTP = m.MDKP_PLSCN);

/* Q2 - Upd ct_Completed */
UPDATE fact_mrp mrp
SET     ct_Completed = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From    TMP_MRP_Q2_UPD t,fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND mrp.ct_Completed <> 1;

/* Q1 - For the same rows where ct_Completed was updated above, update Dim_ActionStateid to 3 */
UPDATE fact_mrp mrp
SET     Dim_ActionStateid = 3
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/	
From    TMP_MRP_Q2_UPD t,fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND Dim_ActionStateid <> 3;


UPDATE fact_mrp mrp
SET Dim_DateidActionClosed = dt.Dim_Dateid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM TMP_MRP_Q2_UPD t,dim_date dt, dim_company cc,fact_mrp mrp,dim_plant pl
WHERE mrp.fact_mrpid = t.fact_mrpid
AND dt.DateValue = current_date
AND cc.Dim_Companyid = mrp.Dim_Companyid
AND cc.CompanyCode = dt.CompanyCode
and pl.dim_plantid=mrp.dim_plantid
and pl.companycode=dt.companycode
and pl.plantcode=dt.plantcode_factory
AND Dim_DateidActionClosed <> dt.Dim_Dateid;




DELETE FROM fact_mrp
WHERE EXISTS
            (SELECT 1
              FROM dim_date d
              WHERE d.Dim_Dateid = Dim_DateidMRP
                    AND d.DateValue < (current_date - ( INTERVAL '6' MONTH)))
        AND ct_Completed = 1;






/* Insert 1 */

Drop table if exists max_holder_00e;
Create table max_holder_00e
as
Select ifnull(max(fact_mrpid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_mrp;

drop table if exists fact_mrp_00e;
Create table fact_mrp_00e as 
Select distinct Dim_Partid,
	dim_mrpexceptionID1,
	Dim_MRPElementid,
	ct_QtyMRP,
	/* Add cts for BI-5935 */
     ct_QtyShortage,
     ct_QtyExcess ,
	dd_documentno,
	dd_documentitemno,
	dd_scheduleno,
	Dim_DateidDateNeeded,
	dd_PlannScenarioLTP,
	dw_update_date
From fact_mrp
Where dd_documentno <> 'Not Set'
AND Dim_ActionStateid = 2;




DROP TABLE IF EXISTS TMP_fact_mrp_ne_01;
CREATE TABLE TMP_fact_mrp_ne_01
like  fact_mrp_00e INCLUDING DEFAULTS INCLUDING IDENTITY;

/*  t.MDTB_RDMNG = 0 condition is removed */
/* Not removing MDTB_MNG03 = 0 as it will increase the volume. Handled that separately in part1a */
Drop table if exists mdtb_r001;
Create table mdtb_r001 as 
Select DISTINCT t.*,ifnull(MDTB_UMDAT, MDTB_DAT00) calvalue01  
from mdtb t 
where ifnull(MDTB_UMDAT, MDTB_DAT00) IS NOT NULL and MDTB_DELNR is not null;
 /* AND MDTB_MNG03 = 0 AND t.MDTB_RDMNG = 0 */
/* Commented out in BI-5935*/ 

INSERT INTO TMP_fact_mrp_ne_01
SELECT DISTINCT Dim_Partid,dim_mrpexceptionID dim_mrpexceptionID1,Dim_MRPElementid,t.MDTB_MNG01 ct_QtyMRP,
/* Add cts for BI-5935 */
   t.MDTB_MNG03 ct_QtyShortage,
   t.MDTB_RDMNG ct_QtyExcess ,
ifnull(t.mdtb_delnr,'Not Set') dd_documentno,
t.mdtb_delps  dd_documentitemno,cast(t.mdtb_delet as integer) dd_ScheduleNo,dn.dim_dateid Dim_DateidDateNeeded,ifnull(MDKP_PLSCN,'Not Set') dd_PlannScenarioLTP, dp.dw_update_date
FROM mdtb_r001 t,mdkp m,dim_mrpelement me,dim_mrpexception mex,dim_part dp,dim_date dn,dim_plant pl
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND     t.MDTB_DELKZ = me.MRPElement
AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD') 
AND ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
AND     m.MDKP_PLWRK = dp.Plant AND m.MDKP_MATNR = dp.PartNumber
AND  m.MDKP_PLWRK = pl.PlantCode
AND     dn.DateValue = ifnull(t.MDTB_UMDAT, t.MDTB_DAT00) AND dn.CompanyCode = pl.CompanyCode and dn.plantcode_factory=pl.plantcode
AND t.MDTB_DELNR is not null; /* LK : 20 May - Added this as we don't want to insert Not Set here */

rename TMP_fact_mrp_ne_01 to TMP_fact_mrp_ne_01_1;
create table TMP_fact_mrp_ne_01 as
(select * from TMP_fact_mrp_ne_01_1) minus (Select * from fact_mrp_00e);
drop table TMP_fact_mrp_ne_01_1;
/*CALL VECTORWISE(COMBINE 'TMP_fact_mrp_ne_01-fact_mrp_00e')*/


Drop table if exists fact_mrp_ne_01;
Drop table if exists fact_mrp_ne_02;


Drop table if exists mdtb_r00;
Create table mdtb_r00 
as 
Select t.*
from mdtb_r001 t 
where EXISTS ( SELECT 1 FROM TMP_fact_mrp_ne_01 M,dim_mrpelement e WHERE M.ct_QtyMRP = t.MDTB_MNG01 
/* Add cts for BI-5935 */
 AND M.ct_QtyShortage = t.MDTB_MNG03
 AND M.ct_QtyExcess = t.MDTB_RDMNG
AND ifnull(t.mdtb_delnr,'Not Set') = M.dd_documentno
AND t.mdtb_delps = M.dd_documentitemno AND cast(t.mdtb_delet as integer) = M.dd_ScheduleNo AND M.dim_mrpelementid = e.dim_mrpelementid AND e.mrpelement = MDTB_DELKZ)
AND t.MDTB_DELNR is not null; /* LK : 20 May - Added this as we don't want to insert Not Set here */

DROP TABLE IF EXISTS mdkp_tmp_r00;
CREATE TABLE mdkp_tmp_r00
AS
SELECT DISTINCT m.*
FROM mdkp m,mdtb_r00 t
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD');


DROP TABLE IF EXISTS TMP_PERF_MRPINS1;
CREATE TABLE TMP_PERF_MRPINS1
AS
SELECT DISTINCT Dim_MRPElementid,dim_mrpexceptionID,MDTB_MNG01,
/* Add cts for BI-5935 */
MDTB_MNG03, MDTB_RDMNG,
MDTB_MNG02,MDTB_WEBAZ,MDKP_BERW2,MDKP_BERW1,mdtb_delnr,
t.mdtb_delet,t.mdtb_dtnum,t.mdtb_baugr,t.mdtb_sernr,mdtb_aufvr,mdtb_del12,MDKP_PLSCN,MDTB_BAART, MDTB_BESKZ, MDTB_PLART,
MDTB_OLDSL, MDTB_DAT01, MDTB_LGORT, m.MDKP_EKGRP, m.mdkp_kzaus, t.MDTB_PERKZ, t.MDTB_SOBES, m.MDKP_DTART, MDTB_DAT00,
MDTB_UMDAT,Dim_MRPProcedureid,MDKP_PLWRK,MDKP_MATNR,MDKP_MEINS , dp.dim_partid, uom.Dim_UnitOfMeasureid, MDKP_DSDAT, calvalue01, mdtb_delps
FROM mdtb_r00 t,mdkp_tmp_r00 m,dim_mrpexception mex, dim_mrpelement me, dim_mrpprocedure pr , dim_part dp, dim_unitofmeasure uom
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
AND t.MDTB_DELKZ = me.MRPElement
AND m.MDKP_DISVF = pr.MRPProcedure
AND m.MDKP_PLWRK = dp.Plant
AND m.MDKP_MATNR = dp.PartNumber
AND uom.UOM = m.MDKP_MEINS ;


Create table fact_mrp_ne_01
as SELECT  max_holder_00e.maxid + row_number() over(order by '') as fact_mrpid,
	    Dim_MRPElementid,
            dim_mrpexceptionID dim_mrpexceptionID1,
            CONVERT (BIGINT, 1) as dim_mrpexceptionID2,
            Dim_Companyid,
            c.Dim_Currencyid,
			c.Dim_Currencyid Dim_Currencyid_TRA, /*No separate tran currency */
			CONVERT(bigint,1) as Dim_Currencyid_GBL,
			CONVERT(decimal (18,5), 1 ) as amt_ExchangeRate,			
			 CONVERT(decimal (18,5), 1 ) as amt_ExchangeRate_GBL, /*cast (ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where varchar(z.pFromCurrency,3)  = varchar(c.CurrencyCode,3) and z.fact_script_name = 'bi_populate_mrp_fact' and z.pDate = current_date AND varchar(z.pToCurrency,3) = varchar(pGlobalCurrency,3) and z.pFromExchangeRate = 0 ),1)  as decimal(18,5) )	amt_ExchangeRate_GBL,*/
            ar.dim_dateid Dim_DateidActionRequired,
            CONVERT(bigint,1) as  Dim_DateidActionClosed,
            dn.dim_dateid Dim_DateidDateNeeded,
			CONVERT(bigint,1) as Dim_DateidOriginalDock,
            mrpd.Dim_Dateid Dim_DateidMRP,
            Dim_Partid,
            CONVERT(bigint,1) as  Dim_StorageLocationid,
            Dim_Plantid,
            Dim_UnitOfMeasureid,
            CONVERT(bigint,1) as Dim_PurchaseGroupid,
            convert(bigint,1) Dim_PurchaseOrgid,
            CONVERT(bigint,1) as  Dim_Vendorid,
            CONVERT(bigint,1) as Dim_ActionStateid,
            CONVERT(bigint,1) as  Dim_ItemCategoryid,
            CONVERT(bigint,1) as  Dim_DocumentTypeid,
            Dim_MRPProcedureid,
             CONVERT(bigint,1) as Dim_MRPDiscontinuationIndicatorid,
             CONVERT(bigint,1) as  Dim_periodindicatorid,
             CONVERT(bigint,1) as  Dim_specialprocurementid,
            CONVERT(bigint,1) as  Dim_FixedVendorid,
            CONVERT(bigint,1) as  Dim_ConsumptionTypeid,
            m.MDTB_MNG01 ct_QtyMRP,
	    /* Add cts for BI-5935 */
		m.MDTB_MNG03 ct_QtyShortage,
		m.MDTB_RDMNG ct_QtyExcess,
            m.MDTB_MNG02 ct_QtyScrap,
            ifnull(m.MDTB_WEBAZ, 0) ct_DaysGRPProcessing,
            m.MDKP_BERW2 ct_DaysReceiptCoverage,
            m.MDKP_BERW1 ct_DaysStockCoverage,
            ifnull(m.mdtb_delnr,'Not Set') dd_DocumentNo,
            m.mdtb_delps dd_DocumentItemNo,
            m.mdtb_delet dd_ScheduleNo,
            m.mdtb_dtnum dd_mrptablenumber,
            m.mdtb_baugr dd_peggedrequirement,
            m.mdtb_sernr dd_bomexplosionno,
            ifnull(m.mdtb_aufvr, 'Not Set') dd_SourceOrderNo,
            ifnull(m.mdtb_del12, 'Not Set') dd_MRPElementNo,
            CONVERT(bigint,1) as  dim_mrplisttypeid,
            ifnull(MDKP_PLSCN,'Not Set') dd_PlannScenarioLTP,
            ifnull(MDTB_BAART,'Not Set') dd_POOrderType,
            ifnull(MDTB_BESKZ,'Not Set') dd_ProcurementType,
            ifnull(MDTB_PLART,'Not Set') dd_PlanningType,
	CONVERT(bigint,1) as dim_DateidMRPDock,
	CONVERT(bigint,1) as Dim_DateidReschedule,
    m.MDTB_OLDSL MDTB_OLDSL_sub,
	m.MDTB_DAT01 MDTB_DAT01_sub,
	dc.CompanyCode CompanyCode_sub,
	m.MDTB_LGORT MDTB_LGORT_sub,
	pl.plantcode plantcode_sub,
	m.MDKP_EKGRP MDKP_EKGRP_sub,
	m.mdkp_kzaus mdkp_kzaus_sub,
	m.MDTB_PERKZ MDTB_PERKZ_sub,
	m.MDTB_SOBES MDTB_SOBES_sub,
	m.MDKP_DTART MDKP_DTART_sub,
	MDTB_DAT00 MDTB_DAT00_sub,
	MDTB_WEBAZ MDTB_WEBAZ_sub,
	m.MDTB_UMDAT MDTB_UMDAT_sub,
	pGlobalCurrency pGlobalCurrency_sub,
	current_timestamp dw_update_date
      FROM max_holder_00e,tmpvariable_00e,TMP_PERF_MRPINS1 m,
                dim_plant pl,
                dim_company dc,
                dim_currency c,
                dim_date mrpd,
                dim_date dn,
                dim_date ar
      WHERE   tmpTotalCount > 0
              AND m.MDKP_PLWRK = pl.PlantCode
              AND pl.CompanyCode = dc.CompanyCode
              AND c.currencycode = dc.currency
              AND mrpd.DateValue = m.MDKP_dsdat
                  AND mrpd.CompanyCode = dc.CompanyCode
				  AND mrpd.PlantCode_factory = pl.plantcode
              AND     dn.DateValue = m.calvalue01
                  AND dn.CompanyCode = dc.CompanyCode
               AND dn.plantcode_factory=pl.plantcode
              AND ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode  AND ar.plantcode_factory=pl.plantcode;

update fact_mrp_ne_01 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_mrp_ne_01 f
      INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmpvariable_00e tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_mrp_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);   

/* This is just for verifying - ideally the delete stmt below should delete 0 rows */
drop table if exists fact_mrp_00e1_test;
Create table fact_mrp_00e1_test as
Select distinct Dim_Partid,
        dim_mrpexceptionID1,
        Dim_MRPElementid,
        ct_QtyMRP,
  	/* Add cts for BI-5935 */
     ct_QtyShortage,
     ct_QtyExcess ,
        dd_documentno,
        dd_documentitemno,
        dd_scheduleno,
        Dim_DateidDateNeeded,
        dd_PlannScenarioLTP
From fact_mrp_ne_01;

DELETE FROM fact_mrp_00e1_test t
WHERE EXISTS ( SELECT 1 FROM fact_mrp_00e e 
where e.Dim_Partid = t.Dim_Partid
AND e.dim_mrpexceptionID1 = t.dim_mrpexceptionID1
AND e.Dim_MRPElementid = t.Dim_MRPElementid
AND e.ct_QtyExcess = t.ct_QtyExcess
/* Add cts for BI-5935 */
AND e.ct_QtyShortage = t.ct_QtyShortage
AND e.ct_QtyMRP = t.ct_QtyMRP
AND e.dd_documentno = t.dd_documentno
AND e.dd_documentitemno = t.dd_documentitemno
AND e.dd_scheduleno = t.dd_scheduleno
AND e.Dim_DateidDateNeeded = t.Dim_DateidDateNeeded
AND e.dd_PlannScenarioLTP = t.dd_PlannScenarioLTP );


UPDATE fact_mrp_ne_01 f
SET f.Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
FROM dim_plant pl,Dim_PurchaseOrg po,fact_mrp_ne_01 f
WHERE f.dim_plantid = pl.dim_plantid
AND pl.PurchOrg = po.PurchaseOrgCode
AND f.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;

Drop table if exists mdtb_r00;


Update fact_mrp_ne_01 f
Set Dim_Currencyid_GBL = ifnull(dcr.Dim_Currencyid,1)
FROM fact_mrp_ne_01 f
LEFT JOIN Dim_Currency dcr on dcr.CurrencyCode = pGlobalCurrency_sub 
WHERE Dim_Currencyid_GBL <> ifnull(dcr.Dim_Currencyid,1);

Update fact_mrp_ne_01 f
Set dim_mrpexceptionID2 = ifnull(dim_mrpexceptionid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_mrpexception ex ON f.MDTB_OLDSL_sub = ex.exceptionkey
WHERE dim_mrpexceptionID2 <> ifnull(dim_mrpexceptionid,1);

merge into  fact_mrp_ne_01 f
using ( select distinct fact_mrpid,od.dim_dateid
FROM fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_DAT01_sub 
		                      AND od.CompanyCode = f.CompanyCode_sub and pl.plantcode=od.plantcode_factory
WHERE Dim_DateidOriginalDock <> ifnull(od.dim_dateid,1)) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set f.Dim_DateidOriginalDock = ifnull(t.dim_dateid,1);

Update fact_mrp_ne_01 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = f.MDTB_LGORT_sub 
						 AND sl.Plant=f.plantcode_sub
WHERE f.Dim_StorageLocationid <> ifnull( sl.Dim_StorageLocationid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_PurchaseGroupid = ifnull( pg.Dim_PurchaseGroupid, 1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_purchasegroup pg ON pg.PurchaseGroup = f.MDKP_EKGRP_sub
WHERE f.Dim_PurchaseGroupid <> ifnull( pg.Dim_PurchaseGroupid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_MRPDiscontinuationIndicatorid = ifnull( di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_mrpdiscontinuationindicator di ON f.mdkp_kzaus_sub = di."indicator"
WHERE f.Dim_MRPDiscontinuationIndicatorid <> ifnull( di.Dim_MRPDiscontinuationIndicatorid,1);

Update fact_mrp_ne_01 f
Set f.Dim_periodindicatorid = ifnull( pid.Dim_periodindicatorid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_periodindicator pid ON f.MDTB_PERKZ_sub = pid.periodindicator
WHERE f.Dim_periodindicatorid <> ifnull( pid.Dim_periodindicatorid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_specialprocurementid =  ifnull( sp.Dim_specialprocurementid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_specialprocurement sp ON f.MDTB_SOBES_sub = sp.specialprocurement
WHERE f.Dim_specialprocurementid <> ifnull( sp.Dim_specialprocurementid,1);

Update fact_mrp_ne_01 f
Set f.dim_mrplisttypeid = ifnull( mlt.Dim_MRPListTypeid, 1)
from  fact_mrp_ne_01 f
		LEFT JOIN Dim_MRPListType mlt ON mlt.Type = f.MDKP_DTART_sub 
WHERE  f.dim_mrplisttypeid <> ifnull( mlt.Dim_MRPListTypeid, 1);

merge into fact_mrp_ne_01 f
using (select distinct fact_mrpid,mdt.Dim_Dateid
from  fact_mrp_ne_01 f
inner join dim_plant pl on f.dim_plantid=pl.dim_plantid
		LEFT JOIN  dim_date mdt ON mdt.CompanyCode = f.CompanyCode_sub and mdt.plantcode_factory=pl.plantcode
and mdt.DateValue = (CASE when f.MDTB_UMDAT_sub is null then f.MDTB_DAT00_sub 
		   	                ELSE (f.MDTB_UMDAT_sub - (interval '1' day) * f.MDTB_WEBAZ_sub ) 
					    END) 
WHERE dim_DateidMRPDock <> ifnull( mdt.Dim_Dateid,1)) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set dim_DateidMRPDock =  ifnull(t.Dim_Dateid,1);
						 
merge into fact_mrp_ne_01 f
using (select distinct f.fact_mrpid, od.dim_dateid
FROM fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_UMDAT_sub
		                      AND od.CompanyCode = f.CompanyCode_sub and od.plantcode_factory=pl.plantcode
WHERE f.Dim_DateidReschedule <> ifnull(od.dim_dateid,1)) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set  f.Dim_DateidReschedule = ifnull(t.dim_dateid,1);


Insert into fact_mrp(fact_mrpid,
                      Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
  /* Add cts for BI-5935 */
		ct_QtyShortage,
		ct_QtyExcess,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      dd_MRPElementNo,
                      dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dd_POOrderType,
		      dd_ProcurementType,
                      dd_PlanningType,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule)
 SELECT  max_holder_00e.maxid + row_number() over(order by ''),
		      f.Dim_MRPElementid,
                      f.dim_mrpexceptionID1,
                      f.dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,							  
                      Dim_DateidActionRequired,
                      f.Dim_DateidActionClosed,
                      f.Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      f.Dim_DateidMRP,
                      f.Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      f.ct_QtyMRP,
  /* Add cts for BI-5935 */
		f.ct_QtyShortage,
		f.ct_QtyExcess,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      f.dd_DocumentNo,
                      f.dd_DocumentItemNo,
                      f.dd_ScheduleNo,
                      dd_mrptablenumber,
		      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      f.dd_MRPElementNo,
                      dim_mrplisttypeid,
                      f.dd_PlannScenarioLTP,
                      dd_POOrderType,
                      dd_ProcurementType,
                      dd_PlanningType,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule
From fact_mrp_ne_01 f,max_holder_00e, TMP_fact_mrp_ne_01 t
WHERE f.Dim_Partid = t.Dim_Partid
AND f.dim_mrpexceptionID1 = t.dim_mrpexceptionID1
AND f.Dim_MRPElementid = t.Dim_MRPElementid
AND f.ct_QtyMRP = t.ct_QtyMRP
AND f.ct_QtyShortage = t.ct_QtyShortage
AND f.ct_QtyExcess = t.ct_QtyExcess
AND f.dd_documentno = t.dd_documentno
AND f.dd_documentitemno = t.dd_documentitemno
AND f.dd_scheduleno = t.dd_scheduleno
AND f.Dim_DateidDateNeeded = t.Dim_DateidDateNeeded
AND f.dd_PlannScenarioLTP = t.dd_PlannScenarioLTP;

drop table if exists fact_mrp_00e;
drop table if exists fact_mrp_ne_01;
drop table if exists fact_mrp_ne_02;


INSERT INTO tmp_checkpoint
SELECT 'C',count(*),current_date
FROM fact_mrp;

/* Insert 2 */

Drop table if exists max_holder_00e;
Create table max_holder_00e
as
Select ifnull(max(fact_mrpid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) maxid
from fact_mrp;

drop table if exists fact_mrp_00e;
Create table fact_mrp_00e as
Select distinct Dim_Partid,
	dim_mrpexceptionID1,
	Dim_MRPElementid,
	ct_QtyMRP,
  	/* Add cts for BI-5935 */
     ct_QtyShortage,
     ct_QtyExcess ,
	Dim_DateidDateNeeded,
	dd_PlannScenarioLTP
From fact_mrp
Where dd_documentno ='Not Set'
AND Dim_ActionStateid = 2;

DROP TABLE IF EXISTS TMP_fact_mrp_ne_01;
CREATE TABLE TMP_fact_mrp_ne_01 LIKE fact_mrp_00e INCLUDING DEFAULTS INCLUDING IDENTITY;

/* mdtb_r001 is already present, but with MDTB_DELNR not null. That should be null here */

Drop table if exists mdtb_r001;
Create table mdtb_r001 as Select t.*,ifnull(MDTB_UMDAT, MDTB_DAT00) calvalue01
from mdtb t
where ifnull(MDTB_UMDAT, MDTB_DAT00) IS NOT NULL and MDTB_DELNR is null;
/*  AND MDTB_MNG03 = 0 AND t.MDTB_RDMNG = 0 */
/* Commented out in BI-5935*/ 

INSERT INTO TMP_fact_mrp_ne_01
SELECT DISTINCT Dim_Partid,dim_mrpexceptionID dim_mrpexceptionID1,Dim_MRPElementid,t.MDTB_MNG01 ct_QtyMRP,
  /* Add cts for BI-5935 */
		t.MDTB_MNG03 ct_QtyShortage,
		t.MDTB_RDMNG ct_QtyExcess,
dn.dim_dateid Dim_DateidDateNeeded,ifnull(MDKP_PLSCN,'Not Set') dd_PlannScenarioLTP
FROM mdtb_r001 t,mdkp m,dim_mrpelement me,dim_mrpexception mex,dim_part dp,dim_date dn,dim_plant pl
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND     t.MDTB_DELKZ = me.MRPElement
AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
AND ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
AND     m.MDKP_PLWRK = dp.Plant AND m.MDKP_MATNR = dp.PartNumber
AND  m.MDKP_PLWRK = pl.PlantCode
AND  dn.DateValue = ifnull(t.MDTB_UMDAT, t.MDTB_DAT00) AND dn.CompanyCode = pl.CompanyCode and dn.plantcode_factory=pl.plantcode;

/*CALL VECTORWISE(COMBINE 'TMP_fact_mrp_ne_01-fact_mrp_00e')*/

rename TMP_fact_mrp_ne_01 to TMP_fact_mrp_ne_01_1;
create table TMP_fact_mrp_ne_01 as
(select * from TMP_fact_mrp_ne_01_1) minus (Select * from fact_mrp_00e);
drop table TMP_fact_mrp_ne_01_1;


Drop table if exists fact_mrp_ne_01;
Drop table if exists fact_mrp_ne_02;


Drop table if exists mdtb_r00;
Create table mdtb_r00 as
Select DISTINCT t.*
from mdtb_r001 t 
WHERE EXISTS ( SELECT 1 FROM TMP_fact_mrp_ne_01 M,dim_mrpelement e WHERE M.ct_QtyMRP = t.MDTB_MNG01 
  /* Add cts for BI-5935 */
		AND t.MDTB_MNG03 = m.ct_QtyShortage
		AND t.MDTB_RDMNG = m.ct_QtyExcess
AND M.dim_mrpelementid = e.dim_mrpelementid AND e.mrpelement = MDTB_DELKZ);

DROP TABLE IF EXISTS mdkp_tmp_r00;
CREATE TABLE mdkp_tmp_r00
AS
SELECT DISTINCT m.*
FROM mdkp m,mdtb_r00 t
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD');

Create table fact_mrp_ne_01
AS  SELECT max_holder_00e.maxid + row_number() over( order by '') as fact_mrpid,
		Dim_MRPElementid,
            dim_mrpexceptionID dim_mrpexceptionID1,
             CONVERT(bigint,1) as dim_mrpexceptionID2,
            Dim_Companyid,
            c.Dim_Currencyid,
			c.Dim_Currencyid Dim_Currencyid_TRA, /*No separate tran currency */
			CONVERT(bigint,1) as Dim_Currencyid_GBL,
			CONVERT(decimal (18,5), 1) amt_ExchangeRate,			
			CONVERT(decimal (18,5), 1)  amt_ExchangeRate_GBL, /*cast( ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where varchar(z.pFromCurrency,3)  = varchar(c.CurrencyCode,3) and z.fact_script_name = 'bi_populate_mrp_fact' and z.pDate = ANSIDATE(LOCAL_TIMESTAMP) AND varchar(z.pToCurrency,3) = varchar(pGlobalCurrency,3) and z.pFromExchangeRate = 0 ),1)  as decimal(18,5)) amt_ExchangeRate_GBL,	*/		
            ar.dim_dateid Dim_DateidActionRequired,
            CONVERT(bigint,1) as Dim_DateidActionClosed,
            dn.dim_dateid Dim_DateidDateNeeded,
             CONVERT(bigint,1) as Dim_DateidOriginalDock,
            mrpd.Dim_Dateid Dim_DateidMRP,
            Dim_Partid,
            CONVERT(bigint,1) as Dim_StorageLocationid,
            Dim_Plantid,
            Dim_UnitOfMeasureid,
            CONVERT(bigint,1) as Dim_PurchaseGroupid,
            CONVERT(bigint,1) as Dim_PurchaseOrgid,
            CONVERT(bigint,1) as Dim_Vendorid,
            CONVERT(bigint,1) as Dim_ActionStateid,
            CONVERT(bigint,1) as Dim_ItemCategoryid,
            CONVERT(bigint,1) as Dim_DocumentTypeid,
            Dim_MRPProcedureid,
            CONVERT(bigint,1) as Dim_MRPDiscontinuationIndicatorid,
             CONVERT(bigint,1) as Dim_periodindicatorid,
            CONVERT(bigint,1) as Dim_specialprocurementid,
            CONVERT(bigint,1) as Dim_FixedVendorid,
            CONVERT(bigint,1) as Dim_ConsumptionTypeid,
            t.MDTB_MNG01 ct_QtyMRP,
  /* Add cts for BI-5935 */
		t.MDTB_MNG03 ct_QtyShortage,
		t.MDTB_RDMNG ct_QtyExcess,
            t.MDTB_MNG02 ct_QtyScrap,
            ifnull(t.MDTB_WEBAZ, 0) ct_DaysGRPProcessing,
            m.MDKP_BERW2 ct_DaysReceiptCoverage,
            m.MDKP_BERW1 ct_DaysStockCoverage,
            ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
            t.mdtb_delps dd_DocumentItemNo,
            t.mdtb_delet dd_ScheduleNo,
            t.mdtb_dtnum dd_mrptablenumber,
            t.mdtb_baugr dd_peggedrequirement,
            t.mdtb_sernr dd_bomexplosionno,
            ifnull(t.mdtb_aufvr, 'Not Set') dd_SourceOrderNo,
            ifnull(t.mdtb_del12, 'Not Set') dd_MRPElementNo,
            CONVERT(bigint,1) as dim_mrplisttypeid,
            MDKP_PLSCN dd_PlannScenarioLTP,
            CONVERT(bigint,1) as dim_DateidMRPDock,
            CONVERT(bigint,1) as Dim_DateidReschedule,
	  t.MDTB_OLDSL MDTB_OLDSL_sub,
        t.MDTB_DAT01 MDTB_DAT01_sub,
        dc.CompanyCode CompanyCode_sub,
        t.MDTB_LGORT MDTB_LGORT_sub,
        pl.plantcode plantcode_sub,
        m.MDKP_EKGRP MDKP_EKGRP_sub,
        m.mdkp_kzaus mdkp_kzaus_sub,
        t.MDTB_PERKZ MDTB_PERKZ_sub,
        t.MDTB_SOBES MDTB_SOBES_sub,
        m.MDKP_DTART MDKP_DTART_sub,
        MDTB_DAT00 MDTB_DAT00_sub,
        MDTB_WEBAZ MDTB_WEBAZ_sub,
        t.MDTB_UMDAT MDTB_UMDAT_sub,
	pGlobalCurrency pGlobalCurrency_sub,
	current_timestamp dw_update_date
      FROM tmpvariable_00e,max_holder_00e,mdtb_r00 t
            INNER JOIN mdkp_tmp_r00 m
              ON t.MDTB_DTNUM = m.MDKP_DTNUM
            INNER JOIN dim_mrpelement me
              ON     t.MDTB_DELKZ = me.MRPElement
                  AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
            INNER JOIN dim_mrpexception mex
              ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
            INNER JOIN dim_part dp
              ON     m.MDKP_PLWRK = dp.Plant
                  AND m.MDKP_MATNR = dp.PartNumber
            INNER JOIN dim_plant pl
              ON m.MDKP_PLWRK = pl.PlantCode
            INNER JOIN dim_unitofmeasure uom
              ON uom.UOM = m.MDKP_MEINS
            /*INNER JOIN Dim_PurchaseOrg po
              ON pl.PurchOrg = po.PurchaseOrgCode*/
            INNER JOIN dim_company dc
              ON pl.CompanyCode = dc.CompanyCode
            INNER JOIN dim_currency c
              ON c.currencycode = dc.currency
            INNER JOIN dim_date mrpd
              ON mrpd.DateValue = m.MDKP_dsdat
                  AND mrpd.CompanyCode = dc.CompanyCode and pl.plantcode=mrpd.plantcode_factory
            INNER JOIN dim_date dn
              ON   dn.DateValue =ifnull(t.MDTB_UMDAT, t.MDTB_DAT00)
                  AND dn.CompanyCode = dc.CompanyCode and pl.plantcode=dn.plantcode_factory
            INNER JOIN dim_date ar
              ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode and pl.plantcode=ar.plantcode_factory
            INNER JOIN dim_mrpprocedure pr
              ON m.MDKP_DISVF = pr.MRPProcedure 
      WHERE tmpTotalCount > 0;


update fact_mrp_ne_01 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_mrp_ne_01 f
      INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmpvariable_00e tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_mrp_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);   
	  
UPDATE fact_mrp_ne_01 f
SET f.Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
FROM dim_plant pl,Dim_PurchaseOrg po,fact_mrp_ne_01 f
WHERE f.dim_plantid = pl.dim_plantid
AND pl.PurchOrg = po.PurchaseOrgCode
AND f.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;


drop table if exists mdtb_r01;

update fact_mrp_ne_01 f
set Dim_Currencyid_GBL = ifnull(dcr.Dim_Currencyid,1)
from fact_mrp_ne_01 f 
Left JOIN dim_currency dcr ON dcr.CurrencyCode = pGlobalCurrency_sub 
where Dim_Currencyid_GBL <> dcr.Dim_Currencyid;

update fact_mrp_ne_01 f
Set dim_mrpexceptionID2 = ifnull(dim_mrpexceptionid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_mrpexception ex ON f.MDTB_OLDSL_sub = ex.exceptionkey
WHERE dim_mrpexceptionID2 <> ifnull(dim_mrpexceptionid,1);

merge into fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,od.dim_dateid
FROM fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_DAT01_sub 
		                      AND od.CompanyCode = f.CompanyCode_sub and od.plantcode_factory=pl.plantcode
WHERE Dim_DateidOriginalDock <> ifnull(od.dim_dateid,1)) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set f.Dim_DateidOriginalDock = ifnull(t.dim_dateid,1);

Update fact_mrp_ne_01 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = f.MDTB_LGORT_sub 
						 AND sl.Plant=f.plantcode_sub
WHERE f.Dim_StorageLocationid <> ifnull( sl.Dim_StorageLocationid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_PurchaseGroupid = ifnull( pg.Dim_PurchaseGroupid, 1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_purchasegroup pg ON pg.PurchaseGroup = f.MDKP_EKGRP_sub
WHERE f.Dim_PurchaseGroupid <> ifnull( pg.Dim_PurchaseGroupid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_MRPDiscontinuationIndicatorid = ifnull( di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_mrpdiscontinuationindicator di ON f.mdkp_kzaus_sub = di."indicator"
WHERE f.Dim_MRPDiscontinuationIndicatorid <> ifnull( di.Dim_MRPDiscontinuationIndicatorid,1);

Update fact_mrp_ne_01 f
Set f.Dim_periodindicatorid = ifnull( pid.Dim_periodindicatorid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_periodindicator pid ON f.MDTB_PERKZ_sub = pid.periodindicator
WHERE f.Dim_periodindicatorid <> ifnull( pid.Dim_periodindicatorid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_specialprocurementid =  ifnull( sp.Dim_specialprocurementid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_specialprocurement sp ON f.MDTB_SOBES_sub = sp.specialprocurement
WHERE f.Dim_specialprocurementid <> ifnull( sp.Dim_specialprocurementid,1);

Update fact_mrp_ne_01 f
Set f.dim_mrplisttypeid = ifnull( mlt.Dim_MRPListTypeid, 1)
from  fact_mrp_ne_01 f
		LEFT JOIN Dim_MRPListType mlt ON mlt.Type = f.MDKP_DTART_sub 
WHERE  f.dim_mrplisttypeid <> ifnull( mlt.Dim_MRPListTypeid, 1);
	
merge into fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,mdt.Dim_Dateid
from  fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN  dim_date mdt ON mdt.CompanyCode = f.CompanyCode_sub and mdt.plantcode_factory=pl.plantcode
AND mdt.DateValue = (CASE when f.MDTB_UMDAT_sub is null then f.MDTB_DAT00_sub 
		   	                ELSE (f.MDTB_UMDAT_sub - (interval '1' day) * f.MDTB_WEBAZ_sub ) 
					    END) 
WHERE dim_DateidMRPDock <> ifnull( mdt.Dim_Dateid,1)) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set dim_DateidMRPDock =  ifnull(t.Dim_Dateid,1);


merge into fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,od.Dim_Dateid
FROM fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_UMDAT_sub
		                      AND od.CompanyCode = f.CompanyCode_sub and od.plantcode_factory=pl.plantcode
WHERE f.Dim_DateidReschedule <> ifnull(od.dim_dateid,1)) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set f.Dim_DateidReschedule = ifnull(t.dim_dateid,1);


INSERT INTO fact_mrp(fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
  	/* Add cts for BI-5935 */
     ct_QtyShortage,
     ct_QtyExcess ,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
			dim_mrplisttypeid,
                        dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule)
Select  max_holder_00e.maxid + row_number() over(order by ''),
	                        f.Dim_MRPElementid,
                      f.dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,						  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      f.Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      f.Dim_DateidMRP,
                      f.Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      f.Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      f.ct_QtyMRP,
  	/* Add cts for BI-5935 */
     f.ct_QtyShortage,
     f.ct_QtyExcess ,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
			dim_mrplisttypeid,
                        f.dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      f.Dim_DateidReschedule
From fact_mrp_ne_01 f,max_holder_00e, TMP_fact_mrp_ne_01 t
WHERE f.Dim_Partid = t.Dim_Partid
AND f.dim_mrpexceptionID1 = t.dim_mrpexceptionID1
AND f.Dim_MRPElementid = t.Dim_MRPElementid
AND f.ct_QtyMRP = t.ct_QtyMRP
  /* Add cts for BI-5935 */
AND f.ct_QtyShortage = t.ct_QtyShortage
AND f.ct_QtyExcess = t.ct_QtyExcess
AND f.Dim_DateidDateNeeded = t.Dim_DateidDateNeeded
AND f.dd_PlannScenarioLTP = t.dd_PlannScenarioLTP;


drop table if exists fact_mrp_00e;
drop table if exists fact_mrp_ne_01;
drop table if exists fact_mrp_ne_02;


INSERT INTO tmp_checkpoint
SELECT 'D',count(*),current_date
FROM fact_mrp;
/* Insert 3 */

Drop table if exists max_holder_00e;
Create table max_holder_00e
as
Select ifnull(max(fact_mrpid),0) as maxid
from fact_mrp;

drop table if exists fact_mrp_00e;
Create table fact_mrp_00e as
Select distinct  Dim_Partid,
dim_mrpexceptionID1,
Dim_MRPElementid,
ct_QtyMRP,
  	/* Add cts for BI-5935 */
     ct_QtyShortage,
     ct_QtyExcess ,
dd_documentno,
dd_documentitemno,
dd_scheduleno,dd_PlannScenarioLTP
From fact_mrp
Where dd_documentno <> 'Not Set'
AND Dim_DateidDateNeeded = 1
AND Dim_ActionStateid = 2;

/* mdtb_r001 is created again as insert query has changed -  MDTB_UMDAT IS NULL AND MDTB_DAT00 IS NULL */
/* Note that this already has MDTB_DELNR is not null condition - SO we don't want Not Set document in INsert 3 */
Drop table if exists mdtb_r001;
Create table mdtb_r001 as Select t.*,ifnull(MDTB_UMDAT, MDTB_DAT00) calvalue01 
from mdtb t where MDTB_UMDAT IS NULL AND MDTB_DAT00 IS NULL AND MDTB_DELNR is not null;
/* AND MDTB_MNG03 = 0 AND t.MDTB_RDMNG = 0 */
/* Commented out in BI-5935*/

DROP TABLE IF EXISTS TMP_fact_mrp_ne_01;
CREATE TABLE TMP_fact_mrp_ne_01
LIKE fact_mrp_00e INCLUDING DEFAULTS INCLUDING IDENTITY;


INSERT INTO TMP_fact_mrp_ne_01
SELECT DISTINCT Dim_Partid,dim_mrpexceptionID dim_mrpexceptionID1,Dim_MRPElementid,t.MDTB_MNG01 ct_QtyMRP,
  /* Add cts for BI-5935 */
		t.MDTB_MNG03 ct_QtyShortage,
		t.MDTB_RDMNG ct_QtyExcess,
ifnull(t.mdtb_delnr,'Not Set') dd_documentno,
t.mdtb_delps  dd_documentitemno,cast(t.mdtb_delet as integer) dd_ScheduleNo,ifnull(MDKP_PLSCN,'Not Set') dd_PlannScenarioLTP
FROM mdtb_r001 t,mdkp m,dim_mrpelement me,dim_mrpexception mex,dim_part dp
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND     t.MDTB_DELKZ = me.MRPElement
AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
AND ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
AND     m.MDKP_PLWRK = dp.Plant AND m.MDKP_MATNR = dp.PartNumber;

/*CALL VECTORWISE(COMBINE 'TMP_fact_mrp_ne_01-fact_mrp_00e')*/
rename TMP_fact_mrp_ne_01 to TMP_fact_mrp_ne_01_1;
create table TMP_fact_mrp_ne_01 as
(select * from TMP_fact_mrp_ne_01_1) minus (Select * from fact_mrp_00e);
drop table TMP_fact_mrp_ne_01_1;


Drop table if exists fact_mrp_ne_01;
Drop table if exists fact_mrp_ne_02;


Drop table if exists mdtb_r00;
Create table mdtb_r00 as
Select DISTINCT t.*
from mdtb_r001 t
WHERE EXISTS ( SELECT 1 FROM TMP_fact_mrp_ne_01 M,dim_mrpelement e WHERE M.ct_QtyMRP = t.MDTB_MNG01 
  /* Add cts for BI-5935 */
		AND t.MDTB_MNG03 = M.ct_QtyShortage
		AND t.MDTB_RDMNG = M.ct_QtyExcess
AND ifnull(t.mdtb_delnr,'Not Set') = M.dd_documentno
AND t.mdtb_delps = M.dd_documentitemno AND cast(t.mdtb_delet as integer) = M.dd_ScheduleNo AND M.dim_mrpelementid = e.dim_mrpelementid AND e.mrpelement = MDTB_DELKZ);

DROP TABLE IF EXISTS mdkp_tmp_r00;
CREATE TABLE mdkp_tmp_r00
AS
SELECT DISTINCT m.*
FROM mdkp m,mdtb_r00 t
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD');

Create table fact_mrp_ne_01
AS    SELECT max_holder_00e.maxid + row_number() over(order by '') as fact_mrpid,
		Dim_MRPElementid,
            dim_mrpexceptionID dim_mrpexceptionID1,
            CONVERT(BIGINT,1) as dim_mrpexceptionID2,
            Dim_Companyid,
            c.Dim_Currencyid,
			c.Dim_Currencyid Dim_Currencyid_TRA, /*No separate tran currency */
			CONVERT(bigint,1) as Dim_Currencyid_GBL,
			convert (decimal (18,5), 1) as amt_ExchangeRate,
			convert (decimal (18,5), 1) as amt_ExchangeRate_GBL,/*cast(ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = c.CurrencyCode and z.fact_script_name = 'bi_populate_mrp_fact' and z.pDate = ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency = pGlobalCurrency and z.pFromExchangeRate = 0 ),1) as decimal(18,5)) amt_ExchangeRate_GBL,*/		
            ar.dim_dateid Dim_DateidActionRequired,
            CONVERT(BIGINT,1) as  Dim_DateidActionClosed,
            CONVERT(BIGINT,1) as  Dim_DateidDateNeeded,
             CONVERT(BIGINT,1) as  Dim_DateidOriginalDock,
            mrpd.Dim_Dateid Dim_DateidMRP,
            Dim_Partid,
            CONVERT(BIGINT,1) as  Dim_StorageLocationid,
            Dim_Plantid,
            Dim_UnitOfMeasureid,
            CONVERT(BIGINT,1) as  Dim_PurchaseGroupid,
            CONVERT(bigint,1) as Dim_PurchaseOrgid,
            CONVERT(BIGINT,1) as  Dim_Vendorid,
            CONVERT(BIGINT,1) as  Dim_ActionStateid,
            CONVERT(BIGINT,1) as  Dim_ItemCategoryid,
            CONVERT(BIGINT,1) as  Dim_DocumentTypeid,
            Dim_MRPProcedureid,
            CONVERT(BIGINT,1) as  Dim_MRPDiscontinuationIndicatorid,
           CONVERT(BIGINT,1) as  Dim_periodindicatorid,
            CONVERT(BIGINT,1) as  Dim_specialprocurementid,
            CONVERT(BIGINT,1) as  Dim_FixedVendorid,
            CONVERT(BIGINT,1) as  Dim_ConsumptionTypeid,
            t.MDTB_MNG01 ct_QtyMRP,
  	/* Add cts for BI-5935 */
		t.MDTB_MNG03 ct_QtyShortage,
		t.MDTB_RDMNG ct_QtyExcess,
            t.MDTB_MNG02 ct_QtyScrap,
            ifnull(t.MDTB_WEBAZ, 0) ct_DaysGRPProcessing,
            m.MDKP_BERW2 ct_DaysReceiptCoverage,
            m.MDKP_BERW1 ct_DaysStockCoverage,
            ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
            t.mdtb_delps dd_DocumentItemNo,
            t.mdtb_delet dd_ScheduleNo,
            t.mdtb_dtnum dd_mrptablenumber,
            t.mdtb_baugr dd_peggedrequirement,
            t.mdtb_sernr dd_bomexplosionno,
            ifnull(t.mdtb_aufvr, 'Not Set') dd_SourceOrderNo,
            ifnull(t.mdtb_del12, 'Not Set') dd_MRPElementNo,
            CONVERT(BIGINT,1) as  dim_mrplisttypeid,
            MDKP_PLSCN dd_PlannScenarioLTP,
            CONVERT(BIGINT,1) as  dim_DateidMRPDock,
            CONVERT(BIGINT,1) as  Dim_DateidReschedule,
	    t.MDTB_OLDSL MDTB_OLDSL_sub,
        t.MDTB_DAT01 MDTB_DAT01_sub,
        dc.CompanyCode CompanyCode_sub,
        t.MDTB_LGORT MDTB_LGORT_sub,
        pl.plantcode plantcode_sub,
        m.MDKP_EKGRP MDKP_EKGRP_sub,
        m.mdkp_kzaus mdkp_kzaus_sub,
        t.MDTB_PERKZ MDTB_PERKZ_sub,
        t.MDTB_SOBES MDTB_SOBES_sub,
        m.MDKP_DTART MDKP_DTART_sub,
        MDTB_DAT00 MDTB_DAT00_sub,
        MDTB_WEBAZ MDTB_WEBAZ_sub,
        t.MDTB_UMDAT MDTB_UMDAT_sub,
	pGlobalCurrency pGlobalCurrency_sub,
	current_timestamp dw_update_date
      FROM tmpvariable_00e,max_holder_00e,mdtb_r00 t
            INNER JOIN mdkp_tmp_r00 m
              ON t.MDTB_DTNUM = m.MDKP_DTNUM
            INNER JOIN dim_mrpelement me
              ON     t.MDTB_DELKZ = me.MRPElement
                  AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
            INNER JOIN dim_mrpexception mex
              ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
            INNER JOIN dim_part dp
              ON     m.MDKP_PLWRK = dp.Plant
                  AND m.MDKP_MATNR = dp.PartNumber
            INNER JOIN dim_unitofmeasure uom
              ON uom.UOM = m.MDKP_MEINS
            INNER JOIN dim_plant pl
              ON m.MDKP_PLWRK = pl.PlantCode
            /*INNER JOIN Dim_PurchaseOrg po
              ON pl.PurchOrg = po.PurchaseOrgCode*/
            INNER JOIN dim_company dc
              ON pl.CompanyCode = dc.CompanyCode
            INNER JOIN Dim_Currency c
              ON c.currencycode = dc.currency
            INNER JOIN dim_date mrpd
              ON mrpd.DateValue = m.MDKP_dsdat
                  AND mrpd.CompanyCode = dc.CompanyCode and mrpd.plantcode_factory=pl.plantcode
            INNER JOIN dim_date ar
              ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode and ar.plantcode_factory=pl.plantcode
            INNER JOIN dim_mrpprocedure pr
              ON m.MDKP_DISVF = pr.MRPProcedure
      WHERE   tmpTotalCount > 0;
	  	 
update fact_mrp_ne_01 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_mrp_ne_01 f
      INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmpvariable_00e tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_mrp_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);     

UPDATE fact_mrp_ne_01 f
SET f.Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_plant pl,Dim_PurchaseOrg po, fact_mrp_ne_01 f
WHERE f.dim_plantid = pl.dim_plantid
AND pl.PurchOrg = po.PurchaseOrgCode
AND f.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;


Drop table if exists mdtb_r02 ;

Update fact_mrp_ne_01 f
Set Dim_Currencyid_GBL = ifnull(dcr.Dim_Currencyid,1)
FROM fact_mrp_ne_01 f
LEFT JOIN Dim_Currency dcr on dcr.CurrencyCode = pGlobalCurrency_sub 
WHERE Dim_Currencyid_GBL <> ifnull(dcr.Dim_Currencyid,1);

Update fact_mrp_ne_01 f
Set dim_mrpexceptionID2 = ifnull(dim_mrpexceptionid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_mrpexception ex ON f.MDTB_OLDSL_sub = ex.exceptionkey
WHERE dim_mrpexceptionID2 <> ifnull(dim_mrpexceptionid,1);

merge into  fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,od.dim_dateid
FROM fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_DAT01_sub 
		                      AND od.CompanyCode = f.CompanyCode_sub and od.plantcode_factory=pl.plantcode
WHERE Dim_DateidOriginalDock <> ifnull(od.dim_dateid,1)) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set f.Dim_DateidOriginalDock = ifnull(t.dim_dateid,1);

Update fact_mrp_ne_01 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = f.MDTB_LGORT_sub 
						 AND sl.Plant=f.plantcode_sub
WHERE f.Dim_StorageLocationid <> ifnull( sl.Dim_StorageLocationid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_PurchaseGroupid = ifnull( pg.Dim_PurchaseGroupid, 1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_purchasegroup pg ON pg.PurchaseGroup = f.MDKP_EKGRP_sub
WHERE f.Dim_PurchaseGroupid <> ifnull( pg.Dim_PurchaseGroupid, 1);


Update fact_mrp_ne_01 f
Set f.Dim_MRPDiscontinuationIndicatorid = ifnull( di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_mrpdiscontinuationindicator di ON f.mdkp_kzaus_sub = di."indicator"
WHERE f.Dim_MRPDiscontinuationIndicatorid <> ifnull( di.Dim_MRPDiscontinuationIndicatorid,1);


Update fact_mrp_ne_01 f
Set f.Dim_periodindicatorid = ifnull( pid.Dim_periodindicatorid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_periodindicator pid ON f.MDTB_PERKZ_sub = pid.periodindicator
WHERE f.Dim_periodindicatorid <> ifnull( pid.Dim_periodindicatorid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_specialprocurementid =  ifnull( sp.Dim_specialprocurementid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_specialprocurement sp ON f.MDTB_SOBES_sub = sp.specialprocurement
WHERE f.Dim_specialprocurementid <> ifnull( sp.Dim_specialprocurementid,1);

Update fact_mrp_ne_01 f
Set f.dim_mrplisttypeid = ifnull( mlt.Dim_MRPListTypeid, 1)
from  fact_mrp_ne_01 f
		LEFT JOIN Dim_MRPListType mlt ON mlt.Type = f.MDKP_DTART_sub 
WHERE  f.dim_mrplisttypeid <> ifnull( mlt.Dim_MRPListTypeid, 1);

merge into  fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,mdt.dim_dateid
from  fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN  dim_date mdt ON mdt.CompanyCode = f.CompanyCode_sub and mdt.plantcode_factory=pl.plantcode
AND mdt.DateValue = (CASE when f.MDTB_UMDAT_sub is null then f.MDTB_DAT00_sub 
		   	                ELSE (f.MDTB_UMDAT_sub - (interval '1' day) * f.MDTB_WEBAZ_sub ) 
					    END) ) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set dim_DateidMRPDock =  ifnull( t.Dim_Dateid,1);
						 

merge into  fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,od.dim_dateid
FROM fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_UMDAT_sub
		                      AND od.CompanyCode = f.CompanyCode_sub and od.plantcode_factory=pl.plantcode
WHERE f.Dim_DateidReschedule <> ifnull(od.dim_dateid,1) ) t
on t.fact_mrpid=f.fact_mrpid
when matched then update set f.Dim_DateidReschedule = ifnull(t.dim_dateid,1);



Insert into fact_mrp(fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
  	/* Add cts for BI-5935 */
     ct_QtyShortage,
     ct_QtyExcess ,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      dd_MRPElementNo,
			 dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule)
Select max_holder_00e.maxid + row_number() over(order by ''),
	            f.Dim_MRPElementid,
                      f.dim_mrpexceptionID1,
                      f.dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      f.Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      f.Dim_DateidMRP,
                      f.Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      f.ct_QtyMRP,
  	/* Add cts for BI-5935 */
     f.ct_QtyShortage,
     f.ct_QtyExcess ,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      f.dd_DocumentNo,
                      f.dd_DocumentItemNo,
                      f.dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      f.dd_MRPElementNo,
			 dim_mrplisttypeid,
                      f.dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule
From fact_mrp_ne_01 f,max_holder_00e, TMP_fact_mrp_ne_01 t
WHERE f.Dim_Partid = t.Dim_Partid
AND f.dim_mrpexceptionID1 = t.dim_mrpexceptionID1
AND f.Dim_MRPElementid = t.Dim_MRPElementid
AND f.ct_QtyMRP = t.ct_QtyMRP
  	/* Add cts for BI-5935 */
		AND f.ct_QtyShortage = t.ct_QtyShortage
		AND f.ct_QtyExcess = t.ct_QtyExcess
AND f.dd_documentno = t.dd_documentno
AND f.dd_documentitemno = t.dd_documentitemno
AND f.dd_scheduleno = t.dd_scheduleno
AND f.dd_PlannScenarioLTP = t.dd_PlannScenarioLTP;	/* These joins with TMP_fact.. may not be required, but adding them just in case there are dups */

drop table if exists fact_mrp_00e;
drop table if exists fact_mrp_ne_01;
drop table if exists fact_mrp_ne_02;

Drop table if exists max_holder_00e;
Create table max_holder_00e
as
Select ifnull(max(fact_mrpid),0) as maxid
from fact_mrp;


INSERT INTO tmp_checkpoint
SELECT 'E',count(*),current_date
FROM fact_mrp;
/* Insert 4 */
drop table if exists fact_mrp_00e;
Create table fact_mrp_00e as
Select distinct Dim_Partid,
dim_mrpexceptionID1,
Dim_MRPElementid,
ct_QtyMRP,
  	/* Add cts for BI-5935 */
     ct_QtyShortage,
     ct_QtyExcess ,
dd_PlannScenarioLTP
From fact_mrp
Where dd_documentno = 'Not Set'
      AND Dim_DateidDateNeeded = 1
      AND Dim_ActionStateid = 2;

/* mdtb_r001 is created again as insert query has changed -   MDTB_DELNR is null. Everything else is the same as Insert 3 */
/* Note that this already has MDTB_DELNR is not null condition - SO we only want Not Set document in Insert 4 */
Drop table if exists mdtb_r001;
Create table mdtb_r001 as Select t.*,ifnull(MDTB_UMDAT, MDTB_DAT00) calvalue01
from mdtb t where MDTB_UMDAT IS NULL AND MDTB_DAT00 IS NULL AND MDTB_DELNR is null;
/*  AND MDTB_MNG03 = 0 AND t.MDTB_RDMNG = 0 */
/* Commented out in BI-5935*/ 

DROP TABLE IF EXISTS TMP_fact_mrp_ne_01;
CREATE TABLE TMP_fact_mrp_ne_01 LIKE fact_mrp_00e INCLUDING DEFAULTS INCLUDING IDENTITY;


INSERT INTO TMP_fact_mrp_ne_01
SELECT DISTINCT Dim_Partid,dim_mrpexceptionID dim_mrpexceptionID1,Dim_MRPElementid,t.MDTB_MNG01 ct_QtyMRP,
  /* Add cts for BI-5935 */
		t.MDTB_MNG03 ct_QtyShortage,
		t.MDTB_RDMNG ct_QtyExcess,
ifnull(MDKP_PLSCN,'Not Set') dd_PlannScenarioLTP
FROM mdtb_r001 t,mdkp m,dim_mrpelement me,dim_mrpexception mex,dim_part dp
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND     t.MDTB_DELKZ = me.MRPElement
AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
AND ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
AND     m.MDKP_PLWRK = dp.Plant AND m.MDKP_MATNR = dp.PartNumber;

rename TMP_fact_mrp_ne_01 to TMP_fact_mrp_ne_01_1;
create table TMP_fact_mrp_ne_01 as
(select * from TMP_fact_mrp_ne_01_1) minus (Select * from fact_mrp_00e);
drop table TMP_fact_mrp_ne_01_1;
/*CALL VECTORWISE(COMBINE 'TMP_fact_mrp_ne_01-fact_mrp_00e')*/


Drop table if exists mdtb_r00;
Create table mdtb_r00 as
Select distinct t.*
from mdtb_r001 t
where EXISTS ( SELECT 1 FROM TMP_fact_mrp_ne_01 M,dim_mrpelement e WHERE M.ct_QtyMRP = t.MDTB_MNG01
     /* Add cts for BI-5935 */
		AND t.MDTB_MNG03 = M.ct_QtyShortage
		AND t.MDTB_RDMNG = M.ct_QtyExcess
AND M.dim_mrpelementid = e.dim_mrpelementid AND e.mrpelement = MDTB_DELKZ);

DROP TABLE IF EXISTS mdkp_tmp_r00;
CREATE TABLE mdkp_tmp_r00
AS
SELECT DISTINCT m.*
FROM mdkp m,mdtb_r00 t
WHERE t.MDTB_DTNUM = m.MDKP_DTNUM
AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM',  'SA', 'SI')) OR m.MDKP_DTART <> 'MD');


Create table fact_mrp_ne_01 
AS SELECT max_holder_00e.maxid + row_number() over(order by '') fact_mrpid,
		Dim_MRPElementid,
            dim_mrpexceptionID dim_mrpexceptionID1,
            CONVERT(BIGINT,1) as dim_mrpexceptionID2,
            Dim_Companyid,
            c.Dim_Currencyid,
			c.Dim_Currencyid Dim_Currencyid_TRA, /*No separate tran currency */
			CONVERT(bigint,1) as Dim_Currencyid_GBL,
			CONVERT(DECIMAL (18,5),1) as amt_ExchangeRate,
			CONVERT(DECIMAL (18,5),1) as amt_ExchangeRate_GBL, /*cast (ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = c.CurrencyCode and z.fact_script_name = 'bi_populate_mrp_fact' and z.pDate = ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency = pGlobalCurrency and z.pFromExchangeRate = 0 ),1) as decimal(18,5))	amt_ExchangeRate_GBL,*/			
            ar.dim_dateid Dim_DateidActionRequired,
            CONVERT(BIGINT,1) as Dim_DateidActionClosed,
            CONVERT(BIGINT,1) as Dim_DateidDateNeeded,
            CONVERT(BIGINT,1) as Dim_DateidOriginalDock,
            mrpd.Dim_Dateid Dim_DateidMRP,
            Dim_Partid,
            CONVERT(BIGINT,1) as Dim_StorageLocationid,
            Dim_Plantid,
            Dim_UnitOfMeasureid,
            CONVERT(BIGINT,1) as Dim_PurchaseGroupid,
            CONVERT(bigint,1) as Dim_PurchaseOrgid,
            CONVERT(BIGINT,1) as Dim_Vendorid,
            CONVERT(BIGINT,1) as Dim_ActionStateid,
            CONVERT(BIGINT,1) as Dim_ItemCategoryid,
            CONVERT(BIGINT,1) as Dim_DocumentTypeid,
            Dim_MRPProcedureid,
            CONVERT(BIGINT,1) as Dim_MRPDiscontinuationIndicatorid,
             CONVERT(BIGINT,1) as Dim_periodindicatorid,
            CONVERT(BIGINT,1) as Dim_specialprocurementid,
            CONVERT(BIGINT,1) as Dim_FixedVendorid,
            CONVERT(BIGINT,1) as Dim_ConsumptionTypeid,
            t.MDTB_MNG01 ct_QtyMRP,
	      	/* Add cts for BI-5935 */
		t.MDTB_MNG03 ct_QtyShortage,
		t.MDTB_RDMNG ct_QtyExcess,
            t.MDTB_MNG02 ct_QtyScrap,
            ifnull(t.MDTB_WEBAZ, 0) ct_DaysGRPProcessing,
            m.MDKP_BERW2 ct_DaysReceiptCoverage,
            m.MDKP_BERW1 ct_DaysStockCoverage,
            ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
            t.mdtb_delps dd_DocumentItemNo,
            t.mdtb_delet dd_ScheduleNo,
            t.mdtb_dtnum dd_mrptablenumber,
            t.mdtb_baugr dd_peggedrequirement,
            t.mdtb_sernr dd_bomexplosionno,
            ifnull(t.mdtb_aufvr, 'Not Set') dd_SourceOrderNo,
            ifnull(t.mdtb_del12, 'Not Set') dd_MRPElementNo,
            CONVERT(BIGINT,1) as  dim_mrplisttypeid,
            MDKP_PLSCN as dd_PlannScenarioLTP,
             CONVERT(BIGINT,1) as dim_DateidMRPDock,
             CONVERT(BIGINT,1) as Dim_DateidReschedule,
	 t.MDTB_OLDSL MDTB_OLDSL_sub,
        t.MDTB_DAT01 MDTB_DAT01_sub,
        dc.CompanyCode CompanyCode_sub,
        t.MDTB_LGORT MDTB_LGORT_sub,
        pl.plantcode plantcode_sub,
        m.MDKP_EKGRP MDKP_EKGRP_sub,
        m.mdkp_kzaus mdkp_kzaus_sub,
        t.MDTB_PERKZ MDTB_PERKZ_sub,
        t.MDTB_SOBES MDTB_SOBES_sub,
        m.MDKP_DTART MDKP_DTART_sub,
        MDTB_DAT00 MDTB_DAT00_sub,
        MDTB_WEBAZ MDTB_WEBAZ_sub,
        t.MDTB_UMDAT MDTB_UMDAT_sub,
	pGlobalCurrency pGlobalCurrency_sub,
	current_timestamp dw_update_date

      FROM tmpvariable_00e,max_holder_00e,mdtb_r00 t
            INNER JOIN mdkp_tmp_r00 m
              ON t.MDTB_DTNUM = m.MDKP_DTNUM
            INNER JOIN dim_mrpelement me
              ON     t.MDTB_DELKZ = me.MRPElement
            INNER JOIN dim_mrpexception mex
              ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
            INNER JOIN dim_part dp
              ON     m.MDKP_PLWRK = dp.Plant
                  AND m.MDKP_MATNR = dp.PartNumber
            INNER JOIN dim_unitofmeasure uom
              ON uom.UOM = m.MDKP_MEINS
            INNER JOIN dim_plant pl
              ON m.MDKP_PLWRK = pl.PlantCode
            /*INNER JOIN Dim_PurchaseOrg po
              ON pl.PurchOrg = po.PurchaseOrgCode*/
            INNER JOIN dim_company dc
              ON pl.CompanyCode = dc.CompanyCode
            INNER JOIN Dim_Currency c
              ON c.currencycode = dc.currency
            INNER JOIN dim_date mrpd
              ON mrpd.DateValue = m.MDKP_dsdat
                  AND mrpd.CompanyCode = dc.CompanyCode and mrpd.plantcode_factory=pl.plantcode
            INNER JOIN dim_date ar
              ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode and ar.plantcode_factory=pl.plantcode
            INNER JOIN dim_mrpprocedure pr
              ON m.MDKP_DISVF = pr.MRPProcedure
      WHERE     tmpTotalCount > 0 ;

		  
update fact_mrp_ne_01 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_mrp_ne_01 f
      INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmpvariable_00e tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_mrp_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);  

INSERT INTO tmp_checkpoint
SELECT 'F',count(*),current_date
FROM fact_mrp;

UPDATE fact_mrp_ne_01 f
SET f.Dim_PurchaseOrgid = po.Dim_PurchaseOrgid
FROM dim_plant pl,Dim_PurchaseOrg po, fact_mrp_ne_01 f
WHERE f.dim_plantid = pl.dim_plantid
AND pl.PurchOrg = po.PurchaseOrgCode
AND f.Dim_PurchaseOrgid <> po.Dim_PurchaseOrgid;

Drop table if exists mdtb_r03;

Update fact_mrp_ne_01 f
Set Dim_Currencyid_GBL = ifnull(dcr.Dim_Currencyid,1)
FROM fact_mrp_ne_01 f
LEFT JOIN Dim_Currency dcr on dcr.CurrencyCode = pGlobalCurrency_sub 
WHERE Dim_Currencyid_GBL <> ifnull(dcr.Dim_Currencyid,1);

Update fact_mrp_ne_01 f
Set dim_mrpexceptionID2 = ifnull(dim_mrpexceptionid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_mrpexception ex ON f.MDTB_OLDSL_sub = ex.exceptionkey
WHERE dim_mrpexceptionID2 <> ifnull(dim_mrpexceptionid,1);

merge into  fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,od.dim_dateid
FROM fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_DAT01_sub 
		                      AND od.CompanyCode = f.CompanyCode_sub and od.plantcode_factory=pl.plantcode
WHERE Dim_DateidOriginalDock <> ifnull(od.dim_dateid,1))t
on t.fact_mrpid=f.fact_mrpid
when matched then update set f.Dim_DateidOriginalDock = ifnull(t.dim_dateid,1);

Update fact_mrp_ne_01 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = f.MDTB_LGORT_sub 
						 AND sl.Plant=f.plantcode_sub
WHERE f.Dim_StorageLocationid <> ifnull( sl.Dim_StorageLocationid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_PurchaseGroupid = ifnull( pg.Dim_PurchaseGroupid, 1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_purchasegroup pg ON pg.PurchaseGroup = f.MDKP_EKGRP_sub
WHERE f.Dim_PurchaseGroupid <> ifnull( pg.Dim_PurchaseGroupid, 1);


Update fact_mrp_ne_01 f
Set f.Dim_MRPDiscontinuationIndicatorid = ifnull( di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_mrpdiscontinuationindicator di ON f.mdkp_kzaus_sub = di."indicator"
WHERE f.Dim_MRPDiscontinuationIndicatorid <> ifnull( di.Dim_MRPDiscontinuationIndicatorid,1);

Update fact_mrp_ne_01 f
Set f.Dim_periodindicatorid = ifnull( pid.Dim_periodindicatorid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_periodindicator pid ON f.MDTB_PERKZ_sub = pid.periodindicator
WHERE f.Dim_periodindicatorid <> ifnull( pid.Dim_periodindicatorid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_specialprocurementid =  ifnull( sp.Dim_specialprocurementid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_specialprocurement sp ON f.MDTB_SOBES_sub = sp.specialprocurement
WHERE f.Dim_specialprocurementid <> ifnull( sp.Dim_specialprocurementid,1);

Update fact_mrp_ne_01 f
Set f.dim_mrplisttypeid = ifnull( mlt.Dim_MRPListTypeid, 1)
from  fact_mrp_ne_01 f
		LEFT JOIN Dim_MRPListType mlt ON mlt.Type = f.MDKP_DTART_sub 
WHERE  f.dim_mrplisttypeid <> ifnull( mlt.Dim_MRPListTypeid, 1);
	 

merge into  fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,mdt.dim_dateid
from  fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN  dim_date mdt ON mdt.CompanyCode = f.CompanyCode_sub and mdt.plantcode_factory=pl.plantcode
AND mdt.DateValue = (CASE when f.MDTB_UMDAT_sub is null then f.MDTB_DAT00_sub 
		   	                ELSE (f.MDTB_UMDAT_sub - (interval '1' day) * f.MDTB_WEBAZ_sub ) 
					    END) 
WHERE dim_DateidMRPDock <> ifnull( mdt.Dim_Dateid,1))t
on t.fact_mrpid=f.fact_mrpid
when matched then update set dim_DateidMRPDock =  ifnull( t.Dim_Dateid,1);

					 
merge into  fact_mrp_ne_01 f
using (select distinct f.fact_mrpid,od.dim_dateid
FROM fact_mrp_ne_01 f
inner join dim_plant pl on pl.dim_plantid=f.dim_plantid
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_UMDAT_sub
		                      AND od.CompanyCode = f.CompanyCode_sub and od.plantcode_factory=pl.plantcode
WHERE f.Dim_DateidReschedule <> ifnull(od.dim_dateid,1))t
on t.fact_mrpid=f.fact_mrpid
when matched then update set f.Dim_DateidReschedule = ifnull(t.dim_dateid,1);



Insert into fact_mrp(fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
		        	/* Add cts for BI-5935 */
     ct_QtyShortage,
     ct_QtyExcess ,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
                        dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
			Dim_DateidReschedule)
 SELECT  max_holder_00e.maxid + row_number() over(order by ''),
                        f.Dim_MRPElementid,
                      f.dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,		  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      f.Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      f.Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      f.ct_QtyMRP,
		        	/* Add cts for BI-5935 */
     f.ct_QtyShortage,
     f.ct_QtyExcess ,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
                        dim_mrplisttypeid,
                      f.dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
			Dim_DateidReschedule
From fact_mrp_ne_01 f,max_holder_00e,TMP_fact_mrp_ne_01 t
WHERE f.Dim_Partid = t.Dim_Partid
AND f.dim_mrpexceptionID1 = t.dim_mrpexceptionID1
AND f.Dim_MRPElementid = t.Dim_MRPElementid
AND f.ct_QtyMRP = t.ct_QtyMRP
     /* Add cts for BI-5935 */
AND f.ct_QtyShortage = t.ct_QtyShortage
AND f.ct_QtyExcess = t.ct_QtyExcess
AND f.dd_PlannScenarioLTP = t.dd_PlannScenarioLTP;

drop table if exists fact_mrp_00e;
drop table if exists fact_mrp_ne_01;
drop table if exists fact_mrp_ne_02;

/* LK: 20 Sep 2013 - amt_UnitStdPrice is in local currency ( as STPRS in mbew_no_bwtar is in plant/company currency ) */
/* At this point, before updates from other facts, local and tran curr are the same. */
/* When the columns are updated from other fact tables, ensure that this is updated to tran curr */

DROP TABLE IF EXISTS tmp_unitstdprice_update;
CREATE TABLE tmp_unitstdprice_update
AS
SELECT p.PartNumber, pl.ValuationArea, fact_mrpid 
FROM  fact_mrp m
      CROSS JOIN  tmpvariable_00e tmp
      INNER JOIN dim_plant pl ON pl.dim_plantid = m.dim_plantid
	  INNER JOIN dim_part p ON m.Dim_Partid = p.Dim_Partid
WHERE   tmp.tmpTotalCount > 0
	AND m.Dim_ActionStateid = 2
	AND (m.amt_UnitStdPrice is null or m.amt_UnitStdPrice = 0);

UPDATE fact_mrp m
SET m.amt_UnitStdPrice =  ifnull((CASE sp.VPRSV WHEN 'S' 
										        THEN (sp.STPRS / (case when sp.PEINH = 0 then null else  sp.PEINH end))
												WHEN 'V' 
										        THEN (sp.VERPR / (case when sp.PEINH = 0 then null else  sp.PEINH end))
                                  END),0)
FROM fact_mrp m
      INNER JOIN tmp_unitstdprice_update tmp ON tmp.fact_mrpid = m.fact_mrpid
      LEFT JOIN mbew_no_bwtar sp ON sp.MATNR = tmp.PartNumber and sp.BWKEY = tmp.ValuationArea
WHERE ((sp.LFGJA * 100) + sp.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from mbew_no_bwtar x 
                                       where     x.MATNR = sp.MATNR 
									         AND x.BWKEY = sp.BWKEY
                                             AND ((x.VPRSV = 'S' AND x.STPRS > 0) OR 
											      (x.VPRSV = 'V' AND x.VERPR > 0)   )
									   )
     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0) OR (sp.VPRSV = 'V' AND sp.VERPR > 0));


UPDATE fact_mrp m
    SET m.Dim_Vendorid = po.Dim_Vendorid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	AND tmpTotalCount > 0
AND m.Dim_Vendorid <> po.Dim_Vendorid;


UPDATE fact_mrp m
    SET 
        m.Dim_ConsumptionTypeid = po.Dim_ConsumptionTypeid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	AND tmpTotalCount > 0
AND m.Dim_ConsumptionTypeid <> po.Dim_ConsumptionTypeid;


UPDATE fact_mrp m
set m.Dim_FixedVendorid = po.Dim_FixedVendorid
From  fact_planorder po, fact_mrp m
WHERE m.Dim_ActionStateid = 2
AND m.dd_DocumentNo = po.dd_PlanOrderNo
AND m.Dim_FixedVendorid <> po.Dim_FixedVendorid;


UPDATE fact_mrp m
SET m.dd_firmed = pm.FirmingIndicator
FROM fact_planorder po,dim_planordermisc pm, fact_mrp m
WHERE m.Dim_ActionStateid = 2
AND m.dd_DocumentNo = po.dd_PlanOrderNo
AND pm.Dim_PlanOrderMiscid = po.Dim_PlanOrderMiscid
AND m.dd_firmed <> pm.FirmingIndicator;


/*
    SET m.Dim_DateidReschedule = od.dim_dateid
	,dw_update_date = current_timestamp Added automatically by update_dw_update_date.pl
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, dim_date od, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	AND tmpTotalCount > 0
AND od.DateValue = t.MDTB_UMDAT AND od.CompanyCode = dc.CompanyCode
AND m.Dim_DateidReschedule <> od.dim_dateid
*/

merge into fact_mrp m
using (select distinct fact_mrpid,od.dim_dateid
from fact_planorder po,Dim_Company dc,mdkp k,mdtb t,dim_part p,tmpvariable_00e,dim_date od,fact_mrp m
WHERE m.Dim_ActionStateid = 2
 AND k.MDKP_DTNUM = t.MDTB_DTNUM
 AND m.dd_DocumentNo = po.dd_PlanOrderNo
 AND m.dd_DocumentNo = t.MDTB_DELNR
 AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
 AND po.Dim_Companyid = dc.Dim_Companyid
 AND m.Dim_Partid = p.Dim_Partid
 AND tmpTotalCount > 0
AND od.DateValue = t.MDTB_UMDAT 
AND od.CompanyCode = dc.CompanyCode and od.plantcode_factory=p.plant)t
on t.fact_mrpid= m.fact_mrpid
when matched then update SET m.Dim_DateidReschedule=t.dim_dateid,dw_update_date = current_timestamp
where m.Dim_DateidReschedule<>t.dim_dateid;



	
/* LK: 21 Sep 2013: Before updating amt, convert stdprice to tran currency in po */	
MERGE INTO fact_mrp m
USING (SELECT DISTINCT m.fact_mrpid,m.amt_UnitStdPrice / po.amt_ExchangeRate AS amt_UnitStdPrice
FROM fact_planorder po,
Dim_Company dc,
mdkp k,
mdtb t,
dim_part p,
tmpvariable_00e, fact_mrp m
WHERE     m.Dim_ActionStateid = 2
AND k.MDKP_DTNUM = t.MDTB_DTNUM
AND m.dd_DocumentNo = po.dd_PlanOrderNo
AND m.dd_DocumentNo = t.MDTB_DELNR
AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
AND po.Dim_Companyid = dc.Dim_Companyid
AND m.Dim_Partid = p.Dim_Partid
AND tmpTotalCount > 0) src
ON src.fact_mrpid = m.fact_mrpid
WHEN MATCHED THEN UPDATE 
SET m.amt_UnitStdPrice = src.amt_UnitStdPrice
WHERE m.amt_UnitStdPrice <> src.amt_UnitStdPrice;
	
UPDATE fact_mrp m
SET m.amt_ExtendedPrice = ifnull(m.amt_UnitStdPrice * m.ct_QtyMRP,0)
WHERE m.Dim_ActionStateid = 2
AND m.amt_ExtendedPrice <> ifnull(m.amt_UnitStdPrice * m.ct_QtyMRP,0);


DROP TABLE IF EXISTS tmp_upd_leadtimevar_mrp;
CREATE TABLE tmp_upd_leadtimevar_mrp
AS
SELECT DISTINCT m.dd_DocumentNo,m.dd_PlannScenarioLTP,m.Dim_Partid,MDTB_UMDAT,MDTB_DAT01,MDTB_DAT02,p.LeadTime, k.MDKP_DTNUM, t.MDTB_DELKZ, t.mdtb_dtpos
FROM fact_mrp m,mdkp k,mdtb t,dim_part p
WHERE m.Dim_ActionStateid = 2
AND m.dd_DocumentNo = t.MDTB_DELNR
AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
AND k.MDKP_DTNUM = t.MDTB_DTNUM
AND m.Dim_Partid = p.Dim_Partid
AND p.partnumber = k.mdkp_matnr
AND p.plant = k.mdkp_plwrk;

DELETE FROM tmp_upd_leadtimevar_mrp a
WHERE EXISTS ( SELECT 1 FROM tmp_upd_leadtimevar_mrp b
        WHERE a.dd_DocumentNo = b.dd_DocumentNo AND a.dd_PlannScenarioLTP = b.dd_PlannScenarioLTP AND a.MDKP_DTNUM = b.MDKP_DTNUM AND a.MDTB_DELKZ = b.MDTB_DELKZ
        AND a.Dim_Partid = b.Dim_Partid
        AND b.mdtb_dtpos > a.mdtb_dtpos);

UPDATE fact_mrp m
SET m.ct_leadTimeVariance = ifnull(((ifnull(MDTB_UMDAT, MDTB_DAT01) -  MDTB_DAT02) - p.LeadTime), -1 * p.LeadTime)
FROM tmp_upd_leadtimevar_mrp p,dim_mrpelement me, tmpvariable_00e, fact_mrp m
WHERE me.Dim_MRPElementID = m.Dim_MRPElementid AND me.mrpelement = p.MDTB_DELKZ
AND m.Dim_ActionStateid = 2 AND m.dd_DocumentNo = p.dd_DocumentNo AND m.dd_PlannScenarioLTP = p.dd_PlannScenarioLTP AND m.Dim_Partid = p.Dim_Partid
AND tmpTotalCount > 0
AND m.ct_leadTimeVariance <> ifnull(((ifnull(MDTB_UMDAT, MDTB_DAT01) -  MDTB_DAT02) - p.LeadTime), -1 * p.LeadTime);

DROP TABLE IF EXISTS tmp_upd_leadtimevar_mrp;

/* Get the currency and exchange rate columns from planorder */	
UPDATE fact_mrp m
    SET m.Dim_CurrencyID = po.Dim_CurrencyID
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  fact_planorder po,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
	AND tmpTotalCount > 0
AND m.Dim_CurrencyID <> po.Dim_CurrencyID ;
	
UPDATE fact_mrp m
    SET m.Dim_CurrencyID_TRA = po.Dim_CurrencyID_TRA
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  fact_planorder po,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
	AND tmpTotalCount > 0
AND m.Dim_CurrencyID_TRA <> po.Dim_CurrencyID_TRA;

UPDATE fact_mrp m
    SET m.Dim_CurrencyID_GBL = po.Dim_CurrencyID_GBL
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  fact_planorder po,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
	AND tmpTotalCount > 0
AND m.Dim_CurrencyID_GBL <> po.Dim_CurrencyID_GBL;

	UPDATE fact_mrp m
    SET m.amt_ExchangeRate = po.amt_ExchangeRate
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  fact_planorder po,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
	AND tmpTotalCount > 0
AND m.amt_ExchangeRate <> po.amt_ExchangeRate;
	
UPDATE fact_mrp m
    SET m.amt_ExchangeRate_GBL = po.amt_ExchangeRate_GBL
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  fact_planorder po,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
	AND tmpTotalCount > 0
AND m.amt_ExchangeRate_GBL <> po.amt_ExchangeRate_GBL;	
	


        
DROP TABLE IF EXISTS TMP_fact_mrp_ne_01;
DROP TABLE IF EXISTS TMP_MRP_Q1_UPD;
Drop table if exists mdtb_r001;
Drop table if exists mdtb_r00;
Drop table if exists mdkp_tmp_r00;
DROP TABLE IF EXISTS TMP_PERF_MRPINS1;

/* Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */

UPDATE fact_mrp am
SET am.std_exchangerate_dateid = am.Dim_DateidMRP
WHERE   am.std_exchangerate_dateid <> am.Dim_DateidMRP;

drop table if exists fact_mrp_00e1_test;

