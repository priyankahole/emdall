/* Start of vw_bi_populate_shortage_fact.part1 */
/* LK: This has been completely validated against kla on 17 Jun */


/* OCTAVIAN : CHANGE VERSION to only use RESB and update the other attributes coming from fact_productionorder,
MARD and remove the filter on movement types */

drop table if exists fact_shortage_temp;
create table fact_shortage_temp like fact_shortage INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_shortage_temp';

INSERT INTO NUMBER_FOUNTAIN
select  'fact_shortage_temp',
    ifnull(max(f.fact_shortageid),
           ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_shortage f;

/* Madalina 24 Jun 2016 - Refresh all data since 2011 - BI-3301 */
delete from fact_shortage s
where Dim_DateIdReservRequirement in (select dim_dateid from dim_date where datevalue > '2011-01-01');
/* End 24 Jun 2016 */

INSERT INTO fact_shortage_temp(fact_shortageid,
              ct_QtyActual,
                          ct_QtyRequired,
                          ct_QtyReservation,
                          ct_QtyShortage,
                          ct_QtyWithdrawn,
                          ct_QtyWithdrawnUOM,
                          ct_OpenQty,
                          ct_ConfirmedQty,
                          ct_OnHandQty,
                          ct_StockQty,
                          dd_ProductionSequenceNo,
                          dd_DocumentItemNo,
                          dd_DocumentNo,
                          dd_OrderNo,
                          dd_OrderItemNo,
                          dd_ScheduleNo,
                          dd_PeggedRequirement,
                          dd_BomItemNo,
                          Dim_DateIdLatestReqDate,
                          Dim_Unitofmeasureid,
                          Dim_EffectiveForMatPlanningId,
                          Dim_DocumentTypeid,
                          Dim_ItemCategoryid,
                          Dim_MrpControllerid,
                          Dim_MRPElementid,
                          Dim_ComponentId,
                          Dim_ComponentPlantId,
                          Dim_Partid,
                          Dim_Plantid,
                          Dim_StorageLocationid,
                          Dim_CompSLocId,
                          Dim_SupplyAreaId,
                          Dim_Vendorid,
                          dd_ControlCycleItemNo,
                          dd_ControlCycleNo,
                          Dim_MovementTypeId,
                          Dim_DateidRequirement,
                          Dim_DateIdReservRequirement,
                          dd_ReservationNo,
                          dd_ReservationItemNo,
						  dd_RecordType,
                          Dim_ProductionOrderStatusId,
                          Dim_ProfitCenterId)
   SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_shortage_temp') + row_number() over (order by ''),
   cast(0.00 as decimal (19,4)) ct_QtyActual,
       ifnull(RESB_BDMNG,0) ct_QtyRequired,
       cast(0.00 as decimal (19,4)) ct_QtyReservation,
       (CASE
           WHEN (r.RESB_BDMNG - r.RESB_ENMNG) > 0
           THEN
              r.RESB_BDMNG - r.RESB_ENMNG
           ELSE
              0
        END)
          ct_QtyShortage,
       ifnull(RESB_ENMNG,0) ct_QtyWithdrawn,
       ifnull((CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END),0) ct_QtyWithDrawnUOM,
      ifnull((CASE WHEN (( r.RESB_ERFMG > (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) AND RESB_KZEAR IS NULL AND (r.RESB_KZKUP IS NULL))
           THEN (r.RESB_ERFMG - (CASE
              WHEN r.RESB_KZKUP IS NULL
              THEN
                   r.RESB_ENMNG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))
              ELSE
                 r.RESB_ENMNG
           END)) ELSE 0 END),0)  AS ct_OpenQty,
       ifnull((CASE WHEN (r.RESB_VMENG = r.RESB_BDMNG) THEN r.RESB_ERFMG ELSE
          (r.RESB_VMENG * (r.RESB_UMREN/(case when r.RESB_UMREZ = 0 then null else r.RESB_UMREZ end))) END),0) ct_ConfirmedQty ,
       cast(0 as decimal (18,4)) AS ct_OnHandQty,
     cast(0 as decimal (18,4)) AS ct_StockQty,
       CONVERT(BIGINT,0) /* po.dd_SequenceNo */ AS dd_ProductionSequenceNo,
       CONVERT(BIGINT,0) AS dd_DocumentItemNo,
       'Not Set' dd_DocumentNo,
       ifnull(r.RESB_AUFNR,'Not Set') /* po.dd_OrderNumber */ AS dd_OrderNo,
       CONVERT(BIGINT,0) AS /* po.dd_OrderItemNo */ dd_OrderItemNo,
       CONVERT(BIGINT,0) AS dd_ScheduleNo,
       ifnull(r.RESB_BAUGR, 'Not Set') dd_PeggedRequirement ,
       ifnull(r.RESB_POSNR, 'Not Set') dd_BomItemNo,
       CONVERT(BIGINT,1) AS Dim_DateIdLatestReqDate,
       CONVERT(BIGINT,1) AS Dim_UnitOfMeasureId,
     CONVERT(BIGINT,1) AS Dim_EffectiveForMatPlanningId,
       CONVERT(BIGINT,1) AS Dim_DocumentTypeid,
       CONVERT(BIGINT,1) AS Dim_ItemCategoryid,
       CONVERT(BIGINT,1) AS Dim_MrpControllerid,
       CONVERT(BIGINT,1) AS Dim_MRPElementid,
       CONVERT(BIGINT,1) AS Dim_Componentid,
       CONVERT(BIGINT,1) AS Dim_ComponentPlantId,
       CONVERT(BIGINT,1) AS /* po.Dim_PartIdItem */ Dim_Partid,
       CONVERT(BIGINT,1) AS /* po.Dim_Plantid */ Dim_Plantid,
       CONVERT(BIGINT,1) AS /* po.Dim_StorageLocationId */ Dim_StorageLocationid,
       CONVERT(BIGINT,1) AS Dim_CompSLocId,
       CONVERT(BIGINT,1) AS Dim_SupplyAreaid,
       CONVERT(BIGINT,1) AS Dim_Vendorid,
       0 AS dd_ControlCycleItemNo,
       'Not Set' dd_ControlCycleNo,
       CONVERT(BIGINT,1) AS Dim_MovementTypeId,
       CONVERT(BIGINT,1) AS /* po.Dim_DateIdBasicStart */ Dim_DateIdRequirement,
       CONVERT(BIGINT,1) AS Dim_DateIdReservRequirement,
       r.RESB_RSNUM,
       r.RESB_RSPOS,
	   ifnull(r.RESB_RSART, 'Not Set') dd_RecordType,
       CONVERT(BIGINT,1) AS /* po.Dim_ProductionOrderStatusId */ dim_productionorderstatusid,
       CONVERT(BIGINT,1) AS /* po.Dim_ProfitCenterId */ Dim_ProfitCenterId
  FROM /* fact_productionorder po,
       dim_productionordermisc pm, */
       resb r
 /* WHERE r.RESB_AUFNR = po.dd_OrderNumber
       AND po.Dim_ProductionOrderMiscId = pm.Dim_ProductionOrderMiscId
       AND r.RESB_POSTP = 'L'
       AND r.RESB_BWART = '261'
       AND r.RESB_XLOEK IS NULL */
       WHERE NOT EXISTS
                  (SELECT 1
                     FROM fact_shortage s
                    WHERE s.dd_ReservationNo = r.RESB_RSNUM
                          AND s.dd_ReservationItemNo = r.RESB_RSPOS
						  AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') );

/*  Madalina 26 Sep 2016 - Unstable set of rows */
/* UPDATE fact_shortage_temp s
SET ct_OnHandQty = ifnull(m.MARD_LABST,0) + ifnull(m.MARD_SPEME,0),
ct_StockQty = ifnull(m.MARD_LABST,0)
FROM MARD m, RESB r, fact_shortage_temp s
WHERE m.MARD_WERKS = r.RESB_WERKS
AND m.MARD_MATNR = r.RESB_MATNR
AND m.MARD_LGORT = r.RESB_LGORT
AND s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') */

merge into fact_shortage_temp s
using (select distinct s.dd_ReservationNo, s.dd_ReservationItemNo, s.dd_RecordType,
	first_value (ifnull(m.MARD_LABST,0) + ifnull(m.MARD_SPEME,0)) over (partition by s.dd_ReservationNo, s.dd_ReservationItemNo, s.dd_RecordType order by '') as ct_OnHandQty,
	first_value (ifnull(m.MARD_LABST,0))  over (partition by s.dd_ReservationNo, s.dd_ReservationItemNo, s.dd_RecordType order by '') as ct_StockQty
FROM MARD m, RESB r, fact_shortage_temp s
WHERE m.MARD_WERKS = r.RESB_WERKS
AND m.MARD_MATNR = r.RESB_MATNR
AND m.MARD_LGORT = r.RESB_LGORT
AND s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') ) upd
on upd.dd_ReservationNo = s.dd_ReservationNo 
	and upd.dd_ReservationItemNo = s.dd_ReservationItemNo
	and upd.dd_RecordType = s.dd_RecordType
WHEN MATCHED THEN UPDATE
set s.ct_OnHandQty = upd.ct_OnHandQty,
	s.ct_StockQty = upd.ct_StockQty;

/* UPDATE fact_shortage_temp s
SET Dim_DateIdLatestReqDate = dt.dim_dateid
FROM RESB r, fact_shortage_temp s, dim_date dt, dim_plant pl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
AND dt.DateValue = RESB_SBTER AND dt.CompanyCode = pl.CompanyCode
AND pl.PlantCode = RESB_WERKS AND pl.RowIsCurrent = 1
AND Dim_DateIdLatestReqDate <> dt.dim_dateid */

/*Georgiana 05 Jan 2017 added dt.plantcode_factory=pl.plantcode condition and removed firstvalue function*/
merge into fact_shortage_temp s
using ( select distinct s.dd_ReservationNo, s.dd_ReservationItemNo, s.dd_RecordType,max(dt.dim_dateid) as Dim_DateIdLatestReqDate
	/*first_value (dt.dim_dateid) over  (partition by s.dd_ReservationNo, s.dd_ReservationItemNo, s.dd_RecordType order by '') as dim_dateid*/
	from RESB r, fact_shortage_temp s, dim_date dt, dim_plant pl
	WHERE s.dd_ReservationNo = r.RESB_RSNUM
	AND s.dd_ReservationItemNo = r.RESB_RSPOS
	AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
	AND dt.DateValue = RESB_SBTER AND dt.CompanyCode = pl.CompanyCode
    AND dt.plantcode_factory=pl.plantcode
	AND pl.PlantCode = RESB_WERKS AND pl.RowIsCurrent = 1
	AND Dim_DateIdLatestReqDate <> dt.dim_dateid
     group by s.dd_ReservationNo, s.dd_ReservationItemNo, s.dd_RecordType,Dim_DateIdLatestReqDate ) upd
on upd.dd_ReservationNo = s.dd_ReservationNo 
	and upd.dd_ReservationItemNo = s.dd_ReservationItemNo
	and upd.dd_RecordType = s.dd_RecordType 
WHEN MATCHED THEN UPDATE
set s.Dim_DateIdLatestReqDate = upd.Dim_DateIdLatestReqDate;

/*UPDATE fact_shortage_temp s
SET s.Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureid
FROM RESB r, fact_shortage_temp s, Dim_UnitOfMeasure uom
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND uom.UOM = r.RESB_MEINS
AND s.Dim_UnitOfMeasureId <> uom.Dim_UnitOfMeasureid*/

merge into fact_shortage_temp s
using (select distinct s.fact_shortageid,max(uom.Dim_UnitOfMeasureid) as Dim_UnitOfMeasureId
FROM RESB r, fact_shortage_temp s, Dim_UnitOfMeasure uom
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND uom.UOM = r.RESB_MEINS
AND s.Dim_UnitOfMeasureId <> uom.Dim_UnitOfMeasureid
group by s.fact_shortageid) t
on t.fact_shortageid=s.fact_shortageid
when matched then update set s.Dim_UnitOfMeasureId = t.Dim_UnitOfMeasureid;



 UPDATE fact_shortage_temp s
SET s.Dim_EffectiveForMatPlanningId = emp.Dim_EffectiveForMatPlanningId
FROM RESB r, fact_shortage_temp s, Dim_EffectiveForMatPlanning emp
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND emp.EffectiveForMatPlanning = r.RESB_NO_DISP
AND s.Dim_EffectiveForMatPlanningId <> emp.Dim_EffectiveForMatPlanningId;

/*UPDATE fact_shortage_temp s
SET Dim_ItemCategoryid = emp.dim_bomitemcategoryid
FROM RESB r, fact_shortage_temp s, dim_bomitemcategory emp
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND ItemCategory = RESB_POSTP AND RowIsCurrent = 1
AND Dim_ItemCategoryid <> emp.dim_bomitemcategoryid*/

merge into fact_shortage_temp s
using (select distinct s.fact_shortageid, max(emp.dim_bomitemcategoryid) as dim_bomitemcategoryid
FROM RESB r, fact_shortage_temp s, dim_bomitemcategory emp
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND ItemCategory = RESB_POSTP AND RowIsCurrent = 1
AND Dim_ItemCategoryid <> emp.dim_bomitemcategoryid
group by s.fact_shortageid) t
on t.fact_shortageid=s.fact_shortageid
when matched then update set Dim_ItemCategoryid = t.dim_bomitemcategoryid;

UPDATE fact_shortage_temp s
SET Dim_ItemCategoryid = emp.dim_bomitemcategoryid
FROM RESB r, fact_shortage_temp s, dim_bomitemcategory emp
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND ItemCategory = RESB_POSTP AND RowIsCurrent = 1
AND Dim_ItemCategoryid <> emp.dim_bomitemcategoryid;

/*UPDATE fact_shortage_temp s
SET Dim_Componentid = dp.dim_partid
FROM RESB r, fact_shortage_temp s, dim_part dp
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND PartNumber = ifnull(RESB_MATNR,'Not Set')
AND Plant = ifnull(RESB_WERKS,'Not S')
AND RowIsCurrent = 1
AND Dim_Componentid <> dp.dim_partid*/

merge into fact_shortage_temp s
using (select distinct s.fact_shortageid,max(dp.dim_partid) as dim_partid
FROM RESB r, fact_shortage_temp s, dim_part dp
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND PartNumber = ifnull(RESB_MATNR,'Not Set')
AND Plant = ifnull(RESB_WERKS,'Not S')
AND RowIsCurrent = 1
AND Dim_Componentid <> dp.dim_partid
group by fact_shortageid) t
on t.fact_shortageid=s.fact_shortageid
when matched then update set Dim_Componentid = t.dim_partid;


/*UPDATE fact_shortage_temp s
SET Dim_ComponentPlantId = pl.dim_plantid
FROM RESB r, fact_shortage_temp s, dim_plant pl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND pl.PlantCode = ifnull(RESB_WERKS,'Not Set')
AND pl.RowIsCurrent = 1
AND Dim_ComponentPlantId <> pl.dim_plantid*/

merge into fact_shortage_temp s
using (select distinct s.fact_shortageid,pl.dim_plantid
FROM RESB r, fact_shortage_temp s, dim_plant pl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND pl.PlantCode = ifnull(RESB_WERKS,'Not Set')
AND pl.RowIsCurrent = 1
AND Dim_ComponentPlantId <> pl.dim_plantid) t
on t.fact_shortageid=s.fact_shortageid
when matched then update set Dim_ComponentPlantId = t.dim_plantid;

/*UPDATE fact_shortage_temp s
SET Dim_ComponentPlantId = pl.dim_plantid
FROM RESB r, fact_shortage_temp s, dim_plant pl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND pl.PlantCode = ifnull(RESB_WERKS,'Not Set')
AND pl.RowIsCurrent = 1
AND Dim_ComponentPlantId <> pl.dim_plantid*/


/*UPDATE fact_shortage_temp s
SET Dim_CompSLocId = sl.dim_StorageLocationid
FROM RESB r, fact_shortage_temp s, dim_StorageLocation sl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND sl.Plant = RESB_WERKS
AND sl.LocationCode = RESB_LGORT
AND RowIsCurrent = 1
AND Dim_CompSLocId <> sl.dim_StorageLocationid*/

merge into fact_shortage_temp s
using (select distinct s.fact_shortageid,sl.dim_StorageLocationid
FROM RESB r, fact_shortage_temp s, dim_StorageLocation sl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND sl.Plant = RESB_WERKS
AND sl.LocationCode = RESB_LGORT
AND RowIsCurrent = 1
AND Dim_CompSLocId <> sl.dim_StorageLocationid) t
on t.fact_shortageid=s.fact_shortageid
when matched then update set Dim_CompSLocId = t.dim_StorageLocationid;

/*UPDATE fact_shortage_temp s
SET s.Dim_SupplyAreaid = sl.Dim_SupplyAreaid
FROM RESB r, fact_shortage_temp s, Dim_SupplyArea sl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND Plant = RESB_WERKS
AND SupplyArea = RESB_PRVBE
AND RowIsCurrent = 1
AND s.Dim_SupplyAreaid <> sl.Dim_SupplyAreaid*/

merge into fact_shortage_temp s
using (select distinct s.dd_ReservationNo,s.dd_ReservationItemNo,s.dd_RecordType,sl.Dim_SupplyAreaid
 FROM RESB r, fact_shortage_temp s, Dim_SupplyArea sl
 WHERE s.dd_ReservationNo = r.RESB_RSNUM
 AND s.dd_ReservationItemNo = r.RESB_RSPOS
 AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
 AND Plant = RESB_WERKS
 AND SupplyArea = RESB_PRVBE
 AND RowIsCurrent = 1
 AND s.Dim_SupplyAreaid <> sl.Dim_SupplyAreaid) t
on t.dd_ReservationNo=s.dd_ReservationNo
and t.dd_ReservationItemNo=s.dd_ReservationItemNo
and t.dd_RecordType=s.dd_RecordType
when matched then update set s.Dim_SupplyAreaid = t.Dim_SupplyAreaid;


/*UPDATE fact_shortage_temp s
SET s.Dim_Vendorid = dv.dim_Vendorid
FROM RESB r, fact_shortage_temp s, dim_Vendor dv,
(SELECT DISTINCT EORD_MATNR,EORD_WERKS,EORD_LIFNR,row_number() over (partition by EORD_MATNR,EORD_WERKS order by EORD_ERDAT desc) AS rn FROM EORD e WHERE EORD_FLIFN = 'X' AND EORD_BDATU > current_date) EORD
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND dv.vendornumber = EORD_LIFNR
AND dv.RowIsCurrent = 1
AND EORD_MATNR = r.RESB_MATNR
AND EORD_WERKS = r.RESB_WERKS
AND rn = 1
/* AND EORD_FLIFN = 'X' AND EORD_BDATU > current_date 
AND s.Dim_Vendorid <> dv.dim_Vendorid*/

merge into fact_shortage_temp s
using (select distinct s.fact_shortageid,dv.dim_Vendorid
FROM RESB r, fact_shortage_temp s, dim_Vendor dv,
(SELECT DISTINCT EORD_MATNR,EORD_WERKS,EORD_LIFNR,row_number() over (partition by EORD_MATNR,EORD_WERKS order by EORD_ERDAT desc) AS rn FROM EORD e WHERE EORD_FLIFN = 'X' AND EORD_BDATU > current_date) EORD
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND dv.vendornumber = EORD_LIFNR
AND dv.RowIsCurrent = 1
AND EORD_MATNR = r.RESB_MATNR
AND EORD_WERKS = r.RESB_WERKS
AND rn = 1
/* AND EORD_FLIFN = 'X' AND EORD_BDATU > current_date */
AND s.Dim_Vendorid <> dv.dim_Vendorid) t
on t.fact_shortageid=s.fact_shortageid
when matched then update set s.Dim_Vendorid =t.dim_Vendorid;


/*UPDATE fact_shortage_temp s
SET s.Dim_MovementTypeId = mt.dim_movementtypeid
FROM RESB r, fact_shortage_temp s, dim_movementtype mt
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND mt.MovementType = r.RESB_BWART
AND mt.ConsumptionIndicator = ifnull(r.RESB_KZVBR, 'Not Set')
AND mt.MovementIndicator = 'Not Set'
AND mt.ReceiptIndicator = 'Not Set'
AND mt.SpecialStockIndicator = ifnull(r.RESB_SOBKZ, 'Not Set')
AND RowIsCurrent = 1
AND s.Dim_MovementTypeId <> mt.dim_movementtypeid*/

merge into fact_shortage_temp s
using ( select distinct s.fact_shortageid, s.Dim_MovementTypeId
FROM RESB r, fact_shortage_temp s, dim_movementtype mt
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND mt.MovementType = r.RESB_BWART
AND mt.ConsumptionIndicator = ifnull(r.RESB_KZVBR, 'Not Set')
AND mt.MovementIndicator = 'Not Set'
AND mt.ReceiptIndicator = 'Not Set'
AND mt.SpecialStockIndicator = ifnull(r.RESB_SOBKZ, 'Not Set')
AND RowIsCurrent = 1
AND s.Dim_MovementTypeId <> mt.dim_movementtypeid ) upd
on s.fact_shortageid = upd.fact_shortageid
WHEN MATCHED THEN UPDATE
set s.Dim_MovementTypeId = upd.dim_movementtypeid;

/*UPDATE fact_shortage_temp s
SET Dim_DateIdReservRequirement = dt.Dim_DateId
FROM RESB r, fact_shortage_temp s, dim_date dt, dim_plant pl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND dt.DateValue = RESB_BDTER
AND dt.CompanyCode = pl.CompanyCode
AND pl.PlantCode = RESB_WERKS
AND pl.RowIsCurrent = 1
AND Dim_DateIdReservRequirement <> dt.Dim_DateId*/

/*Georgiana 05 Jan 2017 added dt.plantcode_factory=pl.plantcode condition and removed firstvalue function*/
merge into fact_shortage_temp s
using ( select distinct s.fact_shortageid, dt.Dim_DateId 
/*first_value (dt.Dim_DateId) over (partition by s.fact_shortageid order by '') as Dim_DateId*/
from RESB r, fact_shortage_temp s, dim_date dt, dim_plant pl
WHERE s.dd_ReservationNo = r.RESB_RSNUM
AND s.dd_ReservationItemNo = r.RESB_RSPOS
AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') 
AND dt.DateValue = RESB_BDTER
AND dt.CompanyCode = pl.CompanyCode
and dt.plantcode_factory=pl.plantcode
AND pl.PlantCode = RESB_WERKS
AND pl.RowIsCurrent = 1
AND Dim_DateIdReservRequirement <> dt.Dim_DateId ) upd
on s.fact_shortageid = upd.fact_shortageid
WHEN MATCHED THEN UPDATE
set Dim_DateIdReservRequirement = upd.Dim_DateId;


/* Yogini 11 Nov 2016 - Add 8 fields according to BI-4635 */
merge into fact_shortage_temp s
using (select distinct s.fact_shortageid, 
		r.RESB_BANFN, r.RESB_BNFPO,
		r.RESB_KDAUF, r.RESB_KDPOS, r.RESB_KDEIN,
		r.RESB_EBELN, r.RESB_EBELP, r.RESB_EBELE
	FROM RESB r, fact_shortage_temp s
	WHERE s.dd_ReservationNo = r.RESB_RSNUM 
	AND s.dd_ReservationItemNo = r.RESB_RSPOS 
	AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set') ) t
on t.fact_shortageid = s.fact_shortageid
when matched then update 
set s.dd_PurchaseReqNo = ifnull(t.RESB_BANFN,'Not Set'),
	s.dd_PurchaseReqItemNo = ifnull(t.RESB_BNFPO, 0),
	s.dd_SalesDocNo = ifnull(t.RESB_KDAUF,'Not Set'),
	s.dd_SalesItemNo = ifnull(t.RESB_KDPOS, 0),
	s.dd_SalesScheduleNo = ifnull(t.RESB_KDEIN, 0),
	s.dd_PurchaseDocumentNo = ifnull(t.RESB_EBELN,'Not Set'),
	s.dd_PurchaseDocumentItemNo = ifnull(t.RESB_EBELP, 0),
	s.dd_PurchaseScheduleNo = ifnull(t.RESB_EBELE, 0),
	dw_update_date = CURRENT_TIMESTAMP;


