truncate table fact_supply_chain_maps;

DELETE FROM number_fountain m where m.table_name = 'fact_supply_chain_maps';

INSERT INTO number_fountain
SELECT 'fact_supply_chain_maps',
       ifnull(max(d.fact_supply_chain_mapsid),
       ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_supply_chain_maps d
WHERE d.fact_supply_chain_mapsid <> 1;

INSERT INTO fact_supply_chain_maps (fact_supply_chain_mapsid,dw_insert_date,dw_update_date,dim_projectsourceid,dim_partid,dim_plantid, dim_operation_groupid_end, DD_SEGMENT_TYPE, DD_SEGMENT_SEQUENCE,
DD_FROM_PLANT_CODE, DD_TO_PLANT_CODE , ct_segmenttargettime, CT_SEGMENT_QTY,
dd_batchnumber, dim_dateidpostingdate, ct_LEVEL2ACTUALLT, ct_LEVEL2TARGETLT, ct_level2HitOrMiss,ct_LEVEL3ACTUALLT, ct_LEVEL3TARGETLT, ct_level3HitOrMiss )
SELECT (select ifnull(m.max_id, 0) from number_fountain m where m.table_name = 'fact_supply_chain_maps') + row_number() over(order by '') AS fact_supply_chain_mapsid,
       CURRENT_TIMESTAMP AS dw_insert_date,
       CURRENT_TIMESTAMP AS dw_update_date,
       1 AS dim_projectsourceid,
	   1 as dim_partid,
	   1 as dim_plantid,
	   dg.dim_operation_groupid,
	   dg.segmenttype,
	   dg.segmenttypeseq,
	   dg.plantfrom,
	   dg.plantto,
     dg.ct_segmenttargettime_dim,
     dg.CT_SEGMENT_QTY_dim,
     dg.batchnumber,
    dg.dateidpostingdate,
    dg.LEVEL2ACTUALLT,
    dg.LEVEL2TARGETLT,
    dg.level2HitOrMiss,
    dg.LEVEL3ACTUALLT,
    dg.LEVEL3TARGETLT,
    dg.level3HitOrMiss
from dim_operation_group dg 
where not exists (select 1 from fact_supply_chain_maps fscm
where fscm.dim_operation_groupid_end = dg.dim_operation_groupid);

/* updates for comops plng region */ 

merge into fact_supply_chain_maps f
using ( select distinct f.FACT_SUPPLY_CHAIN_MAPSID,
                       first_value(p.region_merck) over (partition by p.planttitle_merck  order by p.planttitle_merck) region_merck
from dim_operation_group d, dim_plant p, fact_supply_chain_maps f
where f.dim_operation_groupid_end = d.dim_operation_groupid
and f.dd_to_plant_code = p.planttitle_merck
AND d.operationgroup = 'COMOPS') t
on t.FACT_SUPPLY_CHAIN_MAPSID = f.FACT_SUPPLY_CHAIN_MAPSID
when matched then update set f.dd_to_plant_code = t.region_merck;

merge into fact_supply_chain_maps f
using ( select distinct f.FACT_SUPPLY_CHAIN_MAPSID,
                       first_value(p.region_merck) over (partition by p.planttitle_merck  order by p.planttitle_merck) region_merck
from dim_operation_group d, dim_plant p, fact_supply_chain_maps f
where f.dim_operation_groupid_end = d.dim_operation_groupid
and f.dd_from_plant_code = p.planttitle_merck
AND d.operationgroup = 'COMOPS'
and f.dd_segment_type <> 'Transit Time'  ) t
on t.FACT_SUPPLY_CHAIN_MAPSID = f.FACT_SUPPLY_CHAIN_MAPSID
when matched then update set f.dd_from_plant_code = t.region_merck;

/* end updates for comops plng region */ 

merge into  fact_supply_chain_maps f
using ( select distinct fact_supply_chain_mapsid,
               first_value(p.countryname) over ( partition by p.planttitle_merck order by p.planttitle_merck desc) as countryname
from fact_supply_chain_maps f, dim_plant p
where f.dd_from_plant_code = p.planttitle_merck
) t
on t.fact_supply_chain_mapsid = f.fact_supply_chain_mapsid
when matched then update set f.dd_from_plant_country = t.countryname;

merge into  fact_supply_chain_maps f
using ( select distinct fact_supply_chain_mapsid,
               first_value(p.region_merck) over ( partition by p.planttitle_merck order by p.planttitle_merck desc) as region
from fact_supply_chain_maps f, dim_plant p
where f.dd_from_plant_code = p.planttitle_merck
) t
on t.fact_supply_chain_mapsid = f.fact_supply_chain_mapsid
when matched then update set f.dd_from_plant_region = t.region;

merge into  fact_supply_chain_maps f
using ( select distinct fact_supply_chain_mapsid,
               first_value(p.countryname) over ( partition by p.planttitle_merck order by p.planttitle_merck desc) as countryname
from fact_supply_chain_maps f, dim_plant p
where f.dd_to_plant_code = p.planttitle_merck
) t
on t.fact_supply_chain_mapsid = f.fact_supply_chain_mapsid
when matched then update set f.dd_to_plant_country = t.countryname;

merge into  fact_supply_chain_maps f
using ( select distinct fact_supply_chain_mapsid,
               first_value(p.region_merck) over ( partition by p.planttitle_merck order by p.planttitle_merck desc) as region
from fact_supply_chain_maps f, dim_plant p
where f.dd_to_plant_code = p.planttitle_merck
) t
on t.fact_supply_chain_mapsid = f.fact_supply_chain_mapsid
when matched then update set f.dd_to_plant_region = t.region;

update fact_supply_chain_maps f
set f.dim_dateidsegment_start = f.dim_dateidpostingdate;

update fact_supply_chain_maps 
set dd_partnumber = substring(dd_batchnumber,instr(dd_batchnumber,' ')+1,length(dd_batchnumber));

update fact_supply_chain_maps 
set dd_plantcode = dd_from_plant_code;

update fact_supply_chain_maps
set dim_dateidsegment_end = dim_dateidsegment_start;

/*Madalina 27 Jun 18 - APP-9897 - adding Part dimension*/
update FACT_SUPPLY_CHAIN_MAPS f
set f.dim_partid = p.dim_partid
from FACT_SUPPLY_CHAIN_MAPS f 
  inner join dim_operation_group dg on f.dim_operation_groupid_end = dg.dim_operation_groupid
  inner join dim_part p on f.dd_partnumber =p.partnumber
              and dg.plantcode = p.plant;

