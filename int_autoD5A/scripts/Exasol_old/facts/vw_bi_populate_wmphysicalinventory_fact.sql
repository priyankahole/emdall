

drop table if exists fact_wmphysicalinventory_tmp;

CREATE TABLE fact_wmphysicalinventory_tmp 
AS 
select * from fact_wmphysicalinventory where 1 = 2;

/*initialize NUMBER_FOUNTAIN*/

delete from NUMBER_FOUNTAIN where table_name = 'fact_wmphysicalinventory';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_wmphysicalinventory',ifnull(max(fact_wmphysicalinventoryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_wmphysicalinventory;

DROP TABLE IF EXISTS tmp_fact_wmphysicalinventory_t1;
CREATE TABLE tmp_fact_wmphysicalinventory_t1
AS
SELECT row_number() over (order by '') rid,
				ifnull(st.ISEG_IBLNR, 'Not Set') as dd_physicalinventorydocno,
				ifnull(st.ISEG_ZEILI, 0) as dd_physicalinventorydocitemno,
				st.ISEG_GJAHR as dd_physicalinventorydocyear,
				ifnull(pl.Dim_Plantid, 1) Dim_Plantid,
				CONVERT(BIGINT, 1) dim_storagelocationid,
				ifnull(st.ISEG_USNAZ, 'Not Set') as dd_countedby,
				ifnull(st.ISEG_USNAD, 'Not Set') as dd_adjustpostby,
				ifnull(st.ISEG_MBLNR, 'Not Set') as dd_materialdocno,
				ifnull(st.ISEG_MJAHR, 0) as dd_materialdocyear,
				ifnull(st.ISEG_MENGE, 0) as ct_countqty,
				ifnull(st.ISEG_BUCHM, 0) as ct_bookqty,
				ifnull(st.ISEG_WSTI_POSM, 0) as ct_adjustmentqty,
			    CONVERT(BIGINT, 1) dim_dateidcount,
			    CONVERT(BIGINT, 1) dim_dateidadjustment,
				CONVERT(BIGINT, 1) dim_dateiddocument,
			    CONVERT(BIGINT, 1) dim_partid,
				ifnull(st.ISEG_CHARG,'Not Set') as dd_batchnumber,
			    CONVERT(BIGINT, 1) dim_specialstockid,
				CONVERT(BIGINT, 1) dim_wmstocktypeid,
				ifnull(st.ISEG_KDAUF,'Not Set') as dd_sddocno,
				ifnull(st.ISEG_KDPOS,0) as dd_sddocitemno,
				ifnull(st.ISEG_KDEIN,0) as dd_sdscheduleno,
				CONVERT(BIGINT, 1) dim_vendorid,
				CONVERT(BIGINT, 1) dim_customerid,
				ifnull(st.ISEG_PLPLA,'Not Set') as dd_productionstoragebin,
				ifnull(st.ISEG_XBLNI,'Not Set') as dd_physinventref,
				CONVERT(BIGINT, 1) dim_baseunitofmeasureid,
				ifnull(st.ISEG_ERFMG, 0) as ct_entryqty,
				CONVERT(BIGINT, 1) dim_entryunitofmeasureid,
				ifnull(st.ISEG_ZEILE, 0) as dd_materialdocitemno,
				ifnull(st.ISEG_NBLNR,'Not Set') as dd_recountdocno,
				ifnull(st.ISEG_DMBTR, 0) as amt_difference,
				CONVERT(BIGINT, 1) dim_diffcurrencyid,
				ifnull(st.ISEG_ABCIN, 'Not Set') as dd_physinvindicator,
				ifnull(st.ISEG_PS_PSP_PNR,'Not Set') as dd_wmbno,
				ifnull(ISEG_VKWRT, 0) as amt_salesvalueincvat,
				ifnull(ISEG_EXVKW, 0) as amt_salesvalue,
				ifnull(ISEG_BUCHW, 0) as amt_bookvalue,
				ifnull(ISEG_VKWRA, 0) as amt_salesvaluewovat,
				ifnull(ISEG_VKMZL, 0) as amt_salesvaluediffincvat,
				ifnull(ISEG_VKNZL, 0) as amt_salesvaluediffwovat,
				ifnull(ISEG_WRTZL, 0) as amt_physinvvalue,
				ifnull(ISEG_WRTBM, 0) as amt_bookqtyvalue,
				ifnull(ISEG_DIWZL, 0) as amt_differencevalue,
				ifnull(st.ISEG_GRUND, 'Not Set') as dd_invdiffreason,
				CONVERT(BIGINT, 1) dim_crossplantpartid,

				CONVERT(BIGINT, 1) dim_dateidcountopeningtime,
				TO_TIMESTAMP(st.ISEG_WSTI_COUNTTIME,'HH24MISS') as dd_counttime,
			    CONVERT(BIGINT, 1) dim_dateidfreeze,
				TO_TIMESTAMP(st.ISEG_WSTI_FREEZETIME,'HH24MISS') as dd_freezetime,
				ifnull(st.ISEG_WSTI_POSW, 0) as amt_retailchangevalue,
				CONVERT(BIGINT, 1) Dim_TransactionEventTypeid,
				CONVERT(BIGINT, 1) dim_dateidplannedcount,
				CONVERT(BIGINT, 1) dim_dateidlastcount,
				CONVERT(BIGINT, 1) dim_dateidposting,
				ifnull(st.IKPF_ZSTAT,'Not Set') as dd_countstatus,
				ifnull(st.IKPF_DSTAT,'Not Set') as dd_adjustmentstatus,
				ifnull(st.IKPF_LSTAT,'Not Set') as dd_deletestatus,
				CONVERT(BIGINT, 1) dim_wmgroupingtypeid,
				ifnull(st.IKPF_ORDNG,'Not Set') as dd_groupingcriterion,
				ifnull(st.IKPF_INVNU,'Not Set') as dd_physinventoryno,
				ifnull(st.IKPF_WSTI_BSTAT,'Not Set') as dd_calcstatus,
			    CONVERT(BIGINT, 1) Dim_WMPhysicalInvMiscId,  
				st.ISEG_WERKS ISEG_WERKS,
				st.ISEG_LGORT ISEG_LGORT,
				st.ISEG_ZLDAT ISEG_ZLDAT,
				pl.CompanyCode CompanyCode,
				st.ISEG_BUDAT ISEG_BUDAT,
				st.IKPF_BLDAT IKPF_BLDAT,
				st.ISEG_MATNR ISEG_MATNR,
				st.ISEG_SOBKZ ISEG_SOBKZ,
				st.ISEG_BSTAR ISEG_BSTAR,
				st.ISEG_LIFNR ISEG_LIFNR,
				st.ISEG_KUNNR ISEG_KUNNR,
				st.ISEG_MEINS ISEG_MEINS,
				st.ISEG_ERFME ISEG_ERFME,
				st.ISEG_WAERS ISEG_WAERS,
				st.ISEG_SAMAT ISEG_SAMAT,
				st.ISEG_WSTI_COUNTDATE ISEG_WSTI_COUNTDATE,
				st.ISEG_WSTI_FREEZEDATE ISEG_WSTI_FREEZEDATE,
				st.IKPF_VGART IKPF_VGART,
				st.IKPF_GIDAT IKPF_GIDAT,
				st.IKPF_ZLDAT IKPF_ZLDAT,
				st.IKPF_BUDAT IKPF_BUDAT,
				st.IKPF_KEORD IKPF_KEORD,
				st.ISEG_XZAEL ISEG_XZAEL,
				st.ISEG_XDIFF ISEG_XDIFF,
				st.ISEG_XNZAE ISEG_XNZAE,
				st.ISEG_XLOEK ISEG_XLOEK,
				st.ISEG_XAMEI ISEG_XAMEI,
				st.ISEG_XNULL ISEG_XNULL,
				st.ISEG_KWART ISEG_KWART,
				st.ISEG_XDISPATCH ISEG_XDISPATCH,
				st.ISEG_WSTI_XCALC ISEG_WSTI_XCALC,
				st.IKPF_SPERR IKPF_SPERR,
				st.IKPF_XBUFI IKPF_XBUFI
 FROM IKPF_ISEG st,
			  dim_plant pl
		WHERE ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
			  AND NOT EXISTS
					(SELECT 1
					   FROM fact_wmphysicalinventory wmphi
					  WHERE wmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
							AND wmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
							AND wmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR);		
							
UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_storagelocationid = ifnull( sl.dim_storagelocationid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_storagelocation sl
		     ON sl.plant = ifnull(t.ISEG_WERKS, 'Not Set')
	         AND sl.locationcode = t.ISEG_LGORT;
			 
UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_dateidcount = ifnull( cdt.dim_dateid  , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN  dim_date cdt
		     ON cdt.DateValue = t.ISEG_ZLDAT
			 AND cdt.CompanyCode = t.CompanyCode
			 AND cdt.plantcode_factory=t.ISEG_WERKS;
			 
			 
UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_dateidadjustment = ifnull( cdt.dim_dateid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_date cdt
		     ON cdt.DateValue = t.ISEG_BUDAT
			 AND cdt.CompanyCode = t.CompanyCode
			 AND cdt.plantcode_factory=t.ISEG_WERKS;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_dateiddocument = ifnull( cdt.dim_dateid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_date cdt
		     ON cdt.DateValue = t.IKPF_BLDAT
			 AND cdt.CompanyCode = t.CompanyCode
			 AND cdt.plantcode_factory=t.ISEG_WERKS;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_partid = ifnull(  dp.dim_partid, 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_part dp
		     ON dp.PartNumber = t.ISEG_MATNR
			 AND dp.plant = ifnull(t.ISEG_WERKS, 'Not Set');

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_specialstockid = ifnull(  ss.dim_specialstockid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_specialstock ss
		     ON ss.specialstockindicator = t.ISEG_SOBKZ;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_wmstocktypeid = ifnull( sty.dim_wmstocktypeid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_wmstocktype sty
		     ON sty.stocktype = t.ISEG_BSTAR;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_vendorid = ifnull(  v.Dim_Vendorid  , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_vendor v
		     ON  v.VendorNumber = t.ISEG_LIFNR;	

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_customerid = ifnull( c.Dim_Customerid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN  dim_customer c
		     ON c.CustomerNumber = t.ISEG_KUNNR;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_baseunitofmeasureid = ifnull( bum.dim_unitofmeasureid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_unitofmeasure bum
		     ON bum.uom = t.ISEG_MEINS;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET dim_entryunitofmeasureid = ifnull( eum.dim_unitofmeasureid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_unitofmeasure eum
		     ON eum.uom = t.ISEG_ERFME;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_diffcurrencyid = ifnull( curr.dim_currencyid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_currency curr
		     ON curr.currencycode = t.ISEG_WAERS;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_crossplantpartid = ifnull( dpcm.dim_partid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_part dpcm
		     ON dpcm.PartNumber = t.ISEG_SAMAT 
			 AND dpcm.plant = ifnull(t.ISEG_WERKS, 'Not Set');

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET   dim_dateidcountopeningtime = ifnull(cotdt.dim_dateid  , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_date cotdt
		     ON cotdt.DateValue = t.ISEG_WSTI_COUNTDATE
			 AND cotdt.CompanyCode = t.CompanyCode
			 AND cotdt.plantcode_factory=t.ISEG_WERKS;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_dateidfreeze = ifnull(fdt.dim_dateid  , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_date fdt
		     ON fdt.DateValue = t.ISEG_WSTI_FREEZEDATE
			 AND fdt.CompanyCode = t.CompanyCode
			 AND fdt.plantcode_factory=t.ISEG_WERKS;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  Dim_TransactionEventTypeid = ifnull( tet.Dim_TransactionEventTypeid, 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_transactioneventtype tet
		     ON tet.TransactionEventTypeCode = t.IKPF_VGART;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET dim_dateidplannedcount = ifnull(pcdt.dim_dateid  , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN  dim_date pcdt
		     ON pcdt.DateValue = t.IKPF_GIDAT
			 AND pcdt.CompanyCode = t.CompanyCode
			 AND pcdt.plantcode_factory=t.ISEG_WERKS;

UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_dateidlastcount = ifnull( lcdt.dim_dateid  , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_date lcdt
		     ON lcdt.DateValue = t.IKPF_ZLDAT
			 AND lcdt.CompanyCode = t.CompanyCode
			 AND lcdt.plantcode_factory=t.ISEG_WERKS;
			 
UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_dateidposting = ifnull( pdt.dim_dateid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_date pdt
		     ON pdt.DateValue = t.IKPF_BUDAT
			 AND pdt.CompanyCode = t.CompanyCode
			 AND pdt.plantcode_factory=t.ISEG_WERKS;
			 
UPDATE tmp_fact_wmphysicalinventory_t1 t
SET  dim_wmgroupingtypeid = ifnull( wmgt.dim_wmgroupingtypeid , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_wmgroupingtype wmgt
		     ON wmgt.WMGroupingType= t.IKPF_KEORD;
			 
UPDATE tmp_fact_wmphysicalinventory_t1 t
SET Dim_WMPhysicalInvMiscId  = ifnull( wmpimisc.Dim_WMPhysicalInvMiscId  , 1)
FROM tmp_fact_wmphysicalinventory_t1 t
        LEFT JOIN dim_wmphysicalinvmiscellaneous wmpimisc 
		     ON  wmpimisc.ItemCounted = ifnull(t.ISEG_XZAEL, 'Not Set') 
						      AND wmpimisc.DifferencePosted = ifnull(t.ISEG_XDIFF, 'Not Set')
					    	  AND wmpimisc.Recount = ifnull(t.ISEG_XNZAE, 'Not Set')
							  AND wmpimisc.ItemDeleted = ifnull(t.ISEG_XLOEK, 'Not Set')
							  AND wmpimisc.AlternativeUnit = ifnull(t.ISEG_XAMEI, 'Not Set')
							  AND wmpimisc.ZeroCount = ifnull(t.ISEG_XNULL, 'Not Set')
							  AND wmpimisc.InventoryValueOnlyMaterial = ifnull(t.ISEG_KWART, 'Not Set')
							  AND wmpimisc.PhysInvDiffDistribution = ifnull(t.ISEG_XDISPATCH, 'Not Set')
							  AND wmpimisc.BookInventoryCalculated = ifnull(t.ISEG_WSTI_XCALC, 'Not Set')
							  AND wmpimisc.PostingBlock = ifnull(t.IKPF_SPERR, 'Not Set')
							  AND wmpimisc.FreezeBookInvntory = ifnull(t.IKPF_XBUFI, 'Not Set');
			

			 

			 
			 
INSERT INTO fact_wmphysicalinventory_tmp(fact_wmphysicalinventoryid,
						dd_physicalinventorydocno, 
						dd_physicalinventorydocitemno,
						dd_physicalinventorydocyear,
						dim_plantid,
						dim_storagelocationid,
						dd_countedby,
						dd_adjustpostby,
						dd_materialdocno,
						dd_materialdocyear,
						ct_countqty,
						ct_bookqty,
						ct_adjustmentqty,
						dim_dateidcount,
						dim_dateidadjustment,
						dim_dateiddocument,
						  dim_partid,
						  dd_batchnumber,
						  dim_specialstockid,
						  dim_wmstocktypeid,
						  dd_sddocno,
						  dd_sddocitemno,
						  dd_sdscheduleno,
						  dim_vendorid,
						  dim_customerid,
						  dd_productionstoragebin,
						  dd_physinventref,
						  dim_baseunitofmeasureid,
						  ct_entryqty,
						  dim_entryunitofmeasureid,
						  dd_materialdocitemno,
						  dd_recountdocno,
						  amt_difference,
						  dim_diffcurrencyid,
						  dd_physinvindicator,
						  dd_wmbno,
						  amt_salesvalueincvat,
						  amt_salesvalue,
						  amt_bookvalue,
						  amt_salesvaluewovat,
						  amt_salesvaluediffincvat,
						  amt_salesvaluediffwovat,
						  amt_physinvvalue,
						  amt_bookqtyvalue,
						  amt_differencevalue,
						  dd_invdiffreason,
						  dim_crossplantpartid,
						  dim_dateidcountopeningtime,
						  dd_counttime,
						  dim_dateidfreeze,
						  dd_freezetime,
						  amt_retailchangevalue,
						  Dim_TransactionEventTypeid,
						  dim_dateidplannedcount,
						  dim_dateidlastcount,
						  dim_dateidposting,
						  dd_countstatus,
						  dd_adjustmentstatus,
						  dd_deletestatus,
						  dim_wmgroupingtypeid,
						  dd_groupingcriterion,
						  dd_physinventoryno,
						  dd_calcstatus,
						  Dim_WMPhysicalInvMiscId)

	   SELECT 	((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN 
                  WHERE table_name = 'fact_wmphysicalinventory') 
                  + row_number() over (order by '') ) fact_wmphysicalinventoryid ,
				a.*
				FROM (SELECT dd_physicalinventorydocno, 
						dd_physicalinventorydocitemno,
						dd_physicalinventorydocyear,
						dim_plantid,
						dim_storagelocationid,
						dd_countedby,
						dd_adjustpostby,
						dd_materialdocno,
						dd_materialdocyear,
						ct_countqty,
						ct_bookqty,
						ct_adjustmentqty,
						dim_dateidcount,
						dim_dateidadjustment,
						dim_dateiddocument,
						  dim_partid,
						  dd_batchnumber,
						  dim_specialstockid,
						  dim_wmstocktypeid,
						  dd_sddocno,
						  dd_sddocitemno,
						  dd_sdscheduleno,
						  dim_vendorid,
						  dim_customerid,
						  dd_productionstoragebin,
						  dd_physinventref,
						  dim_baseunitofmeasureid,
						  ct_entryqty,
						  dim_entryunitofmeasureid,
						  dd_materialdocitemno,
						  dd_recountdocno,
						  amt_difference,
						  dim_diffcurrencyid,
						  dd_physinvindicator,
						  dd_wmbno,
						  amt_salesvalueincvat,
						  amt_salesvalue,
						  amt_bookvalue,
						  amt_salesvaluewovat,
						  amt_salesvaluediffincvat,
						  amt_salesvaluediffwovat,
						  amt_physinvvalue,
						  amt_bookqtyvalue,
						  amt_differencevalue,
						  dd_invdiffreason,
						  dim_crossplantpartid,
						  dim_dateidcountopeningtime,
						  dd_counttime,
						  dim_dateidfreeze,
						  dd_freezetime,
						  amt_retailchangevalue,
						  Dim_TransactionEventTypeid,
						  dim_dateidplannedcount,
						  dim_dateidlastcount,
						  dim_dateidposting,
						  dd_countstatus,
						  dd_adjustmentstatus,
						  dd_deletestatus,
						  dim_wmgroupingtypeid,
						  dd_groupingcriterion,
						  dd_physinventoryno,
						  dd_calcstatus,
						  Dim_WMPhysicalInvMiscId 
			FROM tmp_fact_wmphysicalinventory_t1) a;
							

							

							
UPDATE fact_wmphysicalinventory_tmp fwmphi
SET fwmphi.dim_plantid = ifnull(pl.Dim_Plantid, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.dim_plantid <> pl.Dim_Plantid;


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_storagelocationid = ifnull(sl.dim_storagelocationid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_storagelocation sl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND sl.plant = st.ISEG_WERKS
AND sl.locationcode = st.ISEG_LGORT
AND fwmphi.dim_storagelocationid <> ifnull(sl.dim_storagelocationid,1);
	
UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_countedby = ifnull(st.ISEG_USNAZ, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_countedby <> ifnull(st.ISEG_USNAZ, 'Not Set');
						
	
UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_adjustpostby = ifnull(st.ISEG_USNAD, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_adjustpostby <> ifnull(st.ISEG_USNAD, 'Not Set');


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_materialdocno = ifnull(st.ISEG_MBLNR, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_materialdocno <> ifnull(st.ISEG_MBLNR, 'Not Set');

	
UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_materialdocyear = ifnull(st.ISEG_MJAHR, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_materialdocyear <> ifnull(st.ISEG_MJAHR, 0);
	
	
UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.ct_countqty = ifnull(st.ISEG_MENGE, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl ,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND fwmphi.ct_countqty <> st.ISEG_MENGE;				

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.ct_bookqty = ifnull(st.ISEG_BUCHM, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.ct_bookqty <> st.ISEG_BUCHM;		

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.ct_adjustmentqty = ifnull(st.ISEG_WSTI_POSM, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.ct_adjustmentqty <> st.ISEG_WSTI_POSM;

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_dateidcount = ifnull(cdt.dim_dateid, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date cdt,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND cdt.DateValue = st.ISEG_ZLDAT
AND cdt.CompanyCode = pl.CompanyCode
AND cdt.plantcode_factory=st.ISEG_WERKS
AND fwmphi.dim_dateidcount <> ifnull(cdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_dateidadjustment = ifnull(cdt.dim_dateid, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date cdt,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND cdt.DateValue = st.ISEG_BUDAT
AND cdt.CompanyCode = pl.CompanyCode
AND cdt.plantcode_factory=st.ISEG_WERKS
AND fwmphi.dim_dateidadjustment <> ifnull(cdt.dim_dateid, 1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_dateiddocument = ifnull(cdt.dim_dateid, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date cdt,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND cdt.DateValue = st.IKPF_BLDAT
AND cdt.CompanyCode = pl.CompanyCode
AND cdt.plantcode_factory=st.ISEG_WERKS
AND fwmphi.dim_dateiddocument <> ifnull(cdt.dim_dateid, 1);


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_partid = ifnull(dp.dim_partid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_part dp,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND dp.PartNumber = st.ISEG_MATNR
AND dp.plant = st.ISEG_WERKS
AND fwmphi.dim_partid <> ifnull(dp.dim_partid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_batchnumber = ifnull(st.ISEG_CHARG,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode		
AND fwmphi.dd_batchnumber <> ifnull(st.ISEG_CHARG,'Not Set');


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_specialstockid = ifnull(ss.dim_specialstockid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_specialstock ss,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND ss.specialstockindicator = st.ISEG_SOBKZ
AND fwmphi.dim_specialstockid <> ifnull(ss.dim_specialstockid,1);


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_wmstocktypeid = ifnull(sty.dim_wmstocktypeid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_wmstocktype sty,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND sty.stocktype = st.ISEG_BSTAR
AND fwmphi.dim_wmstocktypeid <> ifnull(sty.dim_wmstocktypeid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_sddocno = ifnull(st.ISEG_KDAUF,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_sddocno <> ifnull(st.ISEG_KDAUF,'Not Set');

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_sddocitemno = ifnull(st.ISEG_KDPOS,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_sddocitemno <> ifnull(st.ISEG_KDPOS,0);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_sdscheduleno = ifnull(st.ISEG_KDEIN,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_sdscheduleno <> ifnull(st.ISEG_KDEIN,0);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_vendorid = ifnull(v.Dim_Vendorid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_vendor v,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND v.VendorNumber = st.ISEG_LIFNR
AND fwmphi.dim_vendorid <> ifnull(v.Dim_Vendorid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_customerid = ifnull(c.Dim_Customerid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_customer c,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND c.CustomerNumber = st.ISEG_KUNNR
AND fwmphi.dim_customerid <> ifnull(c.Dim_Customerid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_productionstoragebin = ifnull(ISEG_PLPLA,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_productionstoragebin <> ifnull(ISEG_PLPLA,'Not Set');			

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_physinventref = ifnull(ISEG_XBLNI,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl ,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_physinventref <> ifnull(ISEG_XBLNI,'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_baseunitofmeasureid = ifnull(bum.dim_unitofmeasureid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_unitofmeasure bum,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND bum.uom = st.ISEG_MEINS
AND fwmphi.dim_baseunitofmeasureid <> ifnull(bum.dim_unitofmeasureid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.ct_entryqty = st.ISEG_ERFMG
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.ct_entryqty <> st.ISEG_ERFMG;						

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_entryunitofmeasureid = ifnull(eum.dim_unitofmeasureid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_unitofmeasure eum,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND eum.uom = st.ISEG_ERFME
AND fwmphi.dim_entryunitofmeasureid <> ifnull(eum.dim_unitofmeasureid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_materialdocitemno = ifnull(st.ISEG_ZEILE,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl ,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_materialdocitemno <> ifnull(st.ISEG_ZEILE,0);		

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_materialdocitemno = ifnull(st.ISEG_ZEILE,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl ,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_materialdocitemno <> ifnull(st.ISEG_ZEILE,0);		

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_recountdocno = ifnull(st.ISEG_NBLNR,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_recountdocno <> ifnull(st.ISEG_NBLNR,'Not Set');			

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_difference = ifnull(st.ISEG_DMBTR, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl	 ,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_difference <> ifnull(st.ISEG_DMBTR, 0);							
	
UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_diffcurrencyid = ifnull(curr.dim_currencyid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_currency curr,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND curr.currencycode = st.ISEG_WAERS
AND fwmphi.dim_diffcurrencyid <> ifnull(curr.dim_currencyid,1);


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_physinvindicator = ifnull(st.ISEG_ABCIN, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_physinvindicator <> ifnull(st.ISEG_ABCIN, 'Not Set');			

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_wmbno = ifnull(st.ISEG_PS_PSP_PNR,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_wmbno <> ifnull(st.ISEG_PS_PSP_PNR,'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_salesvalueincvat = ifnull(st.ISEG_VKWRT, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvalueincvat <> ifnull(st.ISEG_VKWRT, 0);		


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_salesvalue = ifnull(st.ISEG_EXVKW, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvalue <> ifnull(st.ISEG_EXVKW, 0);		
							
UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_bookvalue = ifnull(st.ISEG_BUCHW, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_bookvalue <> ifnull(st.ISEG_BUCHW, 0);	

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_salesvaluewovat = ifnull(st.ISEG_VKWRA, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvaluewovat <> ifnull(st.ISEG_VKWRA, 0);	

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_salesvaluediffincvat = ifnull(st.ISEG_VKMZL, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvaluediffincvat <> ifnull(st.ISEG_VKMZL, 0);			

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_salesvaluediffwovat = ifnull(st.ISEG_VKNZL, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl	,
       fact_wmphysicalinventory_tmp fwmphi	   
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_salesvaluediffwovat <> ifnull(st.ISEG_VKNZL, 0);		
		
UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_physinvvalue = ifnull(st.ISEG_WRTZL, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.amt_physinvvalue <> ifnull(st.ISEG_WRTZL, 0);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_bookqtyvalue = ifnull(st.ISEG_WRTBM, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.amt_bookqtyvalue <> ifnull(st.ISEG_WRTBM, 0);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_differencevalue = ifnull(st.ISEG_DIWZL, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
       fact_wmphysicalinventory_tmp fwmphi	   
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.amt_differencevalue <> ifnull(st.ISEG_DIWZL, 0);						

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_crossplantpartid = ifnull(dpcm.dim_partid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_part dpcm,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND dpcm.PartNumber = st.ISEG_SAMAT
AND dpcm.plant = st.ISEG_WERKS
AND fwmphi.dim_crossplantpartid <> ifnull(dpcm.dim_partid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_invdiffreason = ifnull(st.ISEG_GRUND, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = st.ISEG_ZEILI
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.dd_invdiffreason <> ifnull(st.ISEG_GRUND, 'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_dateidcountopeningtime = ifnull(cotdt.dim_dateid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date cotdt,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode		
AND cotdt.DateValue = st.ISEG_WSTI_COUNTDATE
AND cotdt.CompanyCode = pl.CompanyCode
AND cotdt.plantcode_factory=st.ISEG_WERKS
AND fwmphi.dim_dateidcountopeningtime <> ifnull(cotdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_counttime = TO_TIMESTAMP(st.ISEG_WSTI_COUNTTIME,'HH24MISS')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_counttime <> TO_TIMESTAMP(st.ISEG_WSTI_COUNTTIME,'HH24MISS');


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_dateidfreeze = ifnull(fdt.dim_dateid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date fdt,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = st.ISEG_ZEILI
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode			
AND fdt.DateValue = st.ISEG_WSTI_FREEZEDATE
AND fdt.CompanyCode = pl.CompanyCode
AND fdt.plantcode_factory=st.ISEG_WERKS
AND fwmphi.dim_dateidfreeze <> ifnull(fdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_freezetime = TO_TIMESTAMP(st.ISEG_WSTI_FREEZETIME,'HH24MISS')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
       fact_wmphysicalinventory_tmp fwmphi	   
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_freezetime <> TO_TIMESTAMP(st.ISEG_WSTI_FREEZETIME,'HH24MISS');	
					

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.amt_retailchangevalue = ifnull(ISEG_WSTI_POSW, 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode				
AND fwmphi.amt_retailchangevalue <> ifnull(ISEG_WSTI_POSW, 0);	

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.Dim_TransactionEventTypeid = ifnull(tet.Dim_TransactionEventTypeid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_transactioneventtype tet,
fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode					
AND tet.TransactionEventTypeCode = st.IKPF_VGART
AND fwmphi.Dim_TransactionEventTypeid <> ifnull(tet.Dim_TransactionEventTypeid,1);


UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_dateidplannedcount = ifnull(pcdt.dim_dateid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date pcdt,
fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND pcdt.DateValue = st.IKPF_GIDAT
AND pcdt.CompanyCode = pl.CompanyCode
AND pcdt.plantcode_factory=st.ISEG_WERKS
AND fwmphi.dim_dateidplannedcount <> ifnull(pcdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_dateidlastcount = ifnull(lcdt.dim_dateid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date lcdt,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode			
AND lcdt.DateValue = st.IKPF_ZLDAT
AND lcdt.CompanyCode = pl.CompanyCode
AND lcdt.plantcode_factory=st.ISEG_WERKS
AND fwmphi.dim_dateidlastcount <> ifnull(lcdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_dateidposting = ifnull(pdt.dim_dateid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_date pdt,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND pdt.DateValue = st.IKPF_BUDAT
AND pdt.CompanyCode = pl.CompanyCode
AND pdt.plantcode_factory=st.ISEG_WERKS
AND fwmphi.dim_dateidposting <> ifnull(pdt.dim_dateid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_countstatus = ifnull(st.IKPF_ZSTAT, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode			
AND fwmphi.dd_countstatus <> ifnull(st.IKPF_ZSTAT, 'Not Set');		

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_adjustmentstatus = ifnull(st.IKPF_DSTAT, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_adjustmentstatus <> ifnull(st.IKPF_DSTAT, 'Not Set');

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_deletestatus = ifnull(st.IKPF_LSTAT, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_deletestatus <> ifnull(st.IKPF_LSTAT, 'Not Set');		

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dim_wmgroupingtypeid = ifnull(wmgt.dim_wmgroupingtypeid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_wmgroupingtype wmgt,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND wmgt.WMGroupingType= st.IKPF_KEORD
AND fwmphi.dim_wmgroupingtypeid <> ifnull(wmgt.dim_wmgroupingtypeid,1);

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_groupingcriterion = ifnull(st.IKPF_ORDNG,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode	
AND fwmphi.dd_groupingcriterion <> ifnull(st.IKPF_ORDNG,'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_physinventoryno = ifnull(st.IKPF_INVNU,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_physinventoryno <> ifnull(st.IKPF_INVNU,'Not Set');	

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.dd_calcstatus = ifnull(st.IKPF_WSTI_BSTAT, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   fact_wmphysicalinventory_tmp fwmphi
WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND fwmphi.dd_calcstatus <> ifnull(st.IKPF_WSTI_BSTAT, 'Not Set');						

UPDATE fact_wmphysicalinventory_tmp fwmphi
SET    fwmphi.Dim_WMPhysicalInvMiscId = ifnull(wmpimisc.Dim_WMPhysicalInvMiscId, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       IKPF_ISEG st,
       dim_plant pl,
	   dim_wmphysicalinvmiscellaneous wmpimisc,
	   fact_wmphysicalinventory_tmp fwmphi

WHERE fwmphi.dd_physicalinventorydocno = ifnull(st.ISEG_IBLNR, 'Not Set')
AND fwmphi.dd_physicalinventorydocitemno = ifnull(st.ISEG_ZEILI, 0)
AND fwmphi.dd_physicalinventorydocyear = st.ISEG_GJAHR
AND ifnull(st.ISEG_WERKS, 'Not Set') = pl.PlantCode
AND wmpimisc.ItemCounted = ifnull(st.ISEG_XZAEL, 'Not Set')
AND wmpimisc.DifferencePosted = ifnull(st.ISEG_XDIFF, 'Not Set')
AND wmpimisc.Recount = ifnull(st.ISEG_XNZAE, 'Not Set')
AND wmpimisc.ItemDeleted = ifnull(st.ISEG_XLOEK, 'Not Set')
AND wmpimisc.AlternativeUnit = ifnull(st.ISEG_XAMEI, 'Not Set')
AND wmpimisc.ZeroCount = ifnull(st.ISEG_XNULL, 'Not Set')
AND wmpimisc.InventoryValueOnlyMaterial = ifnull(st.ISEG_KWART, 'Not Set')
AND wmpimisc.PhysInvDiffDistribution = ifnull(st.ISEG_XDISPATCH, 'Not Set')
AND wmpimisc.BookInventoryCalculated = ifnull(st.ISEG_WSTI_XCALC, 'Not Set')
AND wmpimisc.PostingBlock = ifnull(st.IKPF_SPERR, 'Not Set')
AND wmpimisc.FreezeBookInvntory = ifnull(st.IKPF_XBUFI, 'Not Set')
AND fwmphi.Dim_WMPhysicalInvMiscId <> ifnull(wmpimisc.Dim_WMPhysicalInvMiscId, 1);	

INSERT INTO fact_wmphysicalinventory
(fact_wmphysicalinventoryid,
						dd_physicalinventorydocno, 
						dd_physicalinventorydocitemno,
						dd_physicalinventorydocyear,
						dim_plantid,
						dim_storagelocationid,
						dd_countedby,
						dd_adjustpostby,
						dd_materialdocno,
						dd_materialdocyear,
						ct_countqty,
						ct_bookqty,
						ct_adjustmentqty,
						dim_dateidcount,
						dim_dateidadjustment,
						dim_dateiddocument,
						  dim_partid,
						  dd_batchnumber,
						  dim_specialstockid,
						  dim_wmstocktypeid,
						  dd_sddocno,
						  dd_sddocitemno,
						  dd_sdscheduleno,
						  dim_vendorid,
						  dim_customerid,
						  dd_productionstoragebin,
						  dd_physinventref,
						  dim_baseunitofmeasureid,
						  ct_entryqty,
						  dim_entryunitofmeasureid,
						  dd_materialdocitemno,
						  dd_recountdocno,
						  amt_difference,
						  dim_diffcurrencyid,
						  dd_physinvindicator,
						  dd_wmbno,
						  amt_salesvalueincvat,
						  amt_salesvalue,
						  amt_bookvalue,
						  amt_salesvaluewovat,
						  amt_salesvaluediffincvat,
						  amt_salesvaluediffwovat,
						  amt_physinvvalue,
						  amt_bookqtyvalue,
						  amt_differencevalue,
						  dd_invdiffreason,
						  dim_crossplantpartid,
						  dim_dateidcountopeningtime,
						  dd_counttime,
						  dim_dateidfreeze,
						  dd_freezetime,
						  amt_retailchangevalue,
						  Dim_TransactionEventTypeid,
						  dim_dateidplannedcount,
						  dim_dateidlastcount,
						  dim_dateidposting,
						  dd_countstatus,
						  dd_adjustmentstatus,
						  dd_deletestatus,
						  dim_wmgroupingtypeid,
						  dd_groupingcriterion,
						  dd_physinventoryno,
						  dd_calcstatus,
						  Dim_WMPhysicalInvMiscId
)
SELECT fact_wmphysicalinventoryid,
						dd_physicalinventorydocno, 
						dd_physicalinventorydocitemno,
						dd_physicalinventorydocyear,
						dim_plantid,
						dim_storagelocationid,
						dd_countedby,
						dd_adjustpostby,
						dd_materialdocno,
						dd_materialdocyear,
						ct_countqty,
						ct_bookqty,
						ct_adjustmentqty,
						dim_dateidcount,
						dim_dateidadjustment,
						dim_dateiddocument,
						  dim_partid,
						  dd_batchnumber,
						  dim_specialstockid,
						  dim_wmstocktypeid,
						  dd_sddocno,
						  dd_sddocitemno,
						  dd_sdscheduleno,
						  dim_vendorid,
						  dim_customerid,
						  dd_productionstoragebin,
						  dd_physinventref,
						  dim_baseunitofmeasureid,
						  ct_entryqty,
						  dim_entryunitofmeasureid,
						  dd_materialdocitemno,
						  dd_recountdocno,
						  amt_difference,
						  dim_diffcurrencyid,
						  dd_physinvindicator,
						  dd_wmbno,
						  amt_salesvalueincvat,
						  amt_salesvalue,
						  amt_bookvalue,
						  amt_salesvaluewovat,
						  amt_salesvaluediffincvat,
						  amt_salesvaluediffwovat,
						  amt_physinvvalue,
						  amt_bookqtyvalue,
						  amt_differencevalue,
						  dd_invdiffreason,
						  dim_crossplantpartid,
						  dim_dateidcountopeningtime,
						  dd_counttime,
						  dim_dateidfreeze,
						  dd_freezetime,
						  amt_retailchangevalue,
						  Dim_TransactionEventTypeid,
						  dim_dateidplannedcount,
						  dim_dateidlastcount,
						  dim_dateidposting,
						  dd_countstatus,
						  dd_adjustmentstatus,
						  dd_deletestatus,
						  dim_wmgroupingtypeid,
						  dd_groupingcriterion,
						  dd_physinventoryno,
						  dd_calcstatus,
						  Dim_WMPhysicalInvMiscId
FROM fact_wmphysicalinventory_tmp;

drop table if exists fact_wmphysicalinventory_tmp;

 /* Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */

UPDATE fact_wmphysicalinventory f

SET f.std_exchangerate_dateid = dt.dim_dateid
	FROM dim_plant p
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory = p.plantcode
		INNER JOIN fact_wmphysicalinventory f ON f.dim_plantid = p.dim_plantid
WHERE 
	dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

 /* END Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */