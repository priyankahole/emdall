DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot;	 
CREATE TABLE tmp_qave_fact_inspectionlot AS SELECT QAVE_PRUEFLOS,QAVE_VKATART,QAVE_VCODEGRP,QAVE_VERSIONCD,QAVE_VCODE,QAVE_VAEDATUM,QAVE_VNAME,
								MAX(QAVE_ZAEHLER) MAX_QAVE_ZAEHLER
                                 FROM qave
								GROUP BY QAVE_PRUEFLOS,QAVE_VKATART,QAVE_VCODEGRP,QAVE_VERSIONCD,QAVE_VCODE,QAVE_VAEDATUM,QAVE_VNAME;							
UPDATE fact_inspectionlot il
   SET Dim_InspUsageDecisionId = Dim_CodeTextId,
       dd_UsageDecisionMadeBy = ifnull(QAVE_VNAME,'Not Set'),
       Dim_DateIdActualInspectionEnd = dt.dim_dateid 
   FROM
       dim_inspectionlotstatus ils,
       tmp_qave_fact_inspectionlot q,
       dim_CodeText t,
       dim_plant pl,
       fact_inspectionlot il, dim_date dt
 WHERE q.QAVE_PRUEFLOS = il.dd_inspectionlotno
       AND ils.dim_inspectionlotstatusid = il.dim_inspectionlotstatusid
       AND pl.dim_plantid = il.dim_plantid
       AND q.QAVE_VKATART = t."catalog"
       AND q.QAVE_VCODEGRP = t.CodeGroup
       AND q.QAVE_VERSIONCD = t.Version
       AND q.QAVE_VCODE = t.Code
       AND ils.UsageDecisionMade = 'X'
	   AND dt.CompanyCode = pl.CompanyCode AND dt.DateValue = ifnull(q.QAVE_VAEDATUM,'0001-01-01')
	   AND dt.plantcode_factory = pl.plantcode;
DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot;

DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot2;	 
CREATE TABLE tmp_qave_fact_inspectionlot2 AS 
SELECT QAVE_PRUEFLOS,QAVE_VDATUM,QAVE_VAENAME,ROW_NUMBER() over (PARTITION BY QAVE_PRUEFLOS ORDER BY QAVE_ZAEHLER DESC) AS RNK
FROM QAVE;

merge into fact_inspectionlot il
using (select fact_inspectionlotid,ifnull(dd.dim_dateid,1) dim_dateid,ifnull(q2.QAVE_VAENAME,'Not Set') as QAVE_VAENAME
   from fact_inspectionlot il
inner join (select * from tmp_qave_fact_inspectionlot2 where RNK=1) q2
on il.dd_inspectionlotno=q2.QAVE_PRUEFLOS
inner join dim_plant pl
on  il.dim_plantid=pl.dim_plantid
left join dim_date dd
on dd.datevalue=q2.QAVE_VDATUM AND dd.CompanyCode=pl.CompanyCode AND dd.plantcode_factory = pl.plantcode
	  ) src on il.fact_inspectionlotid = src.fact_inspectionlotid
when matched then update set il.dim_dateidUsageDecisionMade = src.dim_dateid, 
                             il.dd_UsageDecisionChangeby=src.QAVE_VAENAME;

DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot2;	 