/* ################################################################################################################## */
/* */
/*   Author         : Octavian */
/*   Created On     : 31 Mar 2016 */
/*  */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   26 May 2016      Octavian  2.1           Add flags to delimit source tables 								  */
/*   21 May 2016 	  Octavian  2.0           Change subselects to update stmts									  */
/*											  Add KOTG992 data 													  */
/*   31 Mar 2016      Octavian	1.0  		  First draft 														  */
/******************************************************************************************************************/

/* insert rows from KOTG992 into KOTG004_w */

drop table if exists KOTG_matlisting;
create table KOTG_matlisting AS
SELECT
KOTG004_DATAB,KOTG004_DATBI,KOTG004_KAPPL,KOTG004_KSCHL,KOTG004_KUNNR,KOTG004_MATNR,KOTG004_VKORG,KOTG004_VTWEG,'Not Set' AS KOTG004_KDGRP,
'X' AS KOTG004_FLAG ,' ' AS KOTG992_FLAG
FROM KOTG004_w
UNION
SELECT
KOTG992_DATAB,KOTG992_DATBI,KOTG992_KAPPL,KOTG992_KSCHL,'Not Set' AS KOTG992_KUNNR,KOTG992_MATNR,KOTG992_VKORG,KOTG992_VTWEG,KOTG992_KDGRP,' ','X'
FROM KOTG992 k2;
/* where not exists(select 1 from KOTG004_w k4
where
k2.kotg992_datbi = k4.kotg004_datbi and
k2.kotg992_kappl = k4.kotg004_kappl and
k2.kotg992_kschl = k4.kotg004_kschl and
-- k2.kotg992_kunnr = k4.kotg004_kunnr and
k2.kotg992_matnr = k4.kotg004_matnr and
k2.kotg992_vkorg = k4.kotg004_vkorg and
k2.kotg992_vtweg = k4.kotg004_vtweg) */

/*
update kotg_matlisting k4
FROM KOTG992 k2
SET kotg992_flag = 'X'
where kotg992_datbi = k4.kotg004_datbi and
k2.kotg992_kappl = k4.kotg004_kappl and
k2.kotg992_kschl = k4.kotg004_kschl and
-- k2.kotg992_kunnr = k4.kotg004_kunnr and
k2.kotg992_matnr = k4.kotg004_matnr and
k2.kotg992_vkorg = k4.kotg004_vkorg and
k2.kotg992_vtweg = k4.kotg004_vtweg and
k2.KOTG992_KDGRP = k4.KOTG004_KDGRP
AND kotg992_flag <> 'X'
*/

drop table if exists tmp_fact_materiallisting;
create table tmp_fact_materiallisting
LIKE fact_materiallisting INCLUDING DEFAULTS INCLUDING IDENTITY;


delete from number_fountain m where m.table_name = 'tmp_fact_materiallisting';
insert into number_fountain
select 'tmp_fact_materiallisting',
ifnull(max(f.fact_materiallistingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_materiallisting f;

/* OZ: CHANGE the main insert as it was using only subqueries that exasol doesn t allow */


insert into tmp_fact_materiallisting(
fact_materiallistingid,
dd_application,
dd_matlistingtype,
dd_salesorg,
dd_distributionchannel,
dd_customer,
dd_part,
dd_dateidvalidenddate,
dd_dateidvalidstartdate,
dd_customergroup,
dd_mloncustomer,
dd_mloncustomergroup)

SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_materiallisting') + row_number() over(ORDER BY '') AS fact_materiallistingid,
ifnull(k.kotg004_kappl,'Not Set') AS dd_application,
ifnull(k.kotg004_kschl,'Not Set') AS dd_matlistingtype,
ifnull(k.kotg004_vkorg,'Not Set') AS dd_salesorg,
ifnull(k.kotg004_vtweg,'Not Set') AS dd_distributionchannel,
ifnull(k.kotg004_kunnr,'Not Set') AS dd_customer,
ifnull(k.kotg004_matnr,'Not Set') AS dd_part,
ifnull(k.kotg004_datbi,'0001-01-01') AS dd_dateidvalidenddate,
ifnull(k.kotg004_datab,'0001-01-01') AS dd_dateidvalidstartdate,
ifnull(k.kotg004_kdgrp,'Not Set') AS dd_customergroup,
k.kotg004_flag AS dd_mloncustomer,
k.kotg992_flag AS dd_mloncustomergroup

FROM KOTG_matlisting k;


/* 27 Sept 2016 Georgiana Changes Adding Customer Plant Code according to BI-4194*/

merge into tmp_fact_materiallisting f
using ( select distinct fact_materiallistingid, first_value(pl.dim_plantid) over (partition by fact_materiallistingid order by fact_materiallistingid) as dim_plantid
from  tmp_fact_materiallisting f,
(select distinct Z1SD_SR_SHIPTO_KUNAG, Z1SD_SR_SHIPTO_WERKS from  Z1SD_SR_SHIPTO
where Z1SD_SR_SHIPTO_MATNR is null and Z1SD_SR_SHIPTO_KUNAG is not null) t
 ,dim_plant pl
where dd_customer=ifnull(t.Z1SD_SR_SHIPTO_KUNAG, 'Not Set')
and ifnull(Z1SD_SR_SHIPTO_WERKS, 'Not set')= plantcode
and  f.dim_plantid <> pl.dim_plantid
) tmp
on tmp.fact_materiallistingid=f.fact_materiallistingid
when matched then update set f.dim_plantid = tmp.dim_plantid;

/* 27 Sept 2016 End of Changes*/

UPDATE tmp_fact_materiallisting f
SET f.dim_salesorgid = so.dim_salesorgid,
dw_update_date = CURRENT_TIMESTAMP
FROM dim_salesorg so, tmp_fact_materiallisting f
WHERE f.dd_salesorg = so.salesorgcode
AND f.dim_salesorgid <> so.dim_salesorgid;

UPDATE tmp_fact_materiallisting f
SET f.dim_distributionchannelid = dc.dim_distributionchannelid,
dw_update_date = CURRENT_TIMESTAMP
FROM dim_distributionchannel dc, tmp_fact_materiallisting f
WHERE f.dd_distributionchannel = dc.distributionchannelcode
AND f.dim_distributionchannelid <> dc.dim_distributionchannelid;

UPDATE tmp_fact_materiallisting f
SET f.dim_customerid = c.dim_customerid,
dw_update_date = CURRENT_TIMESTAMP
FROM dim_customer c, tmp_fact_materiallisting f
WHERE f.dd_customer = c.customernumber
AND f.dim_customerid <> c.dim_customerid;

drop table if exists fact_materiallisting_update_partid;
create table fact_materiallisting_update_partid
as SELECT DISTINCT f.fact_materiallistingid, FIRST_VALUE(dp.dim_partid) OVER (PARTITION BY dp.partnumber ORDER BY dp.dim_partid desc) AS dim_partid
FROM tmp_fact_materiallisting f, dim_part dp, /* , dim_salesorg so, */dim_plant p 
WHERE f.dd_part = dp.partnumber
/* AND f.dd_salesorg = so.salesorgcode
AND so.companycode = p.companycode*/
AND p.plantcode = dp.plant /*12 Oct Georgiana Changes added join with dim_plant based on BI-4429*/
AND f.dim_partid <> dp.dim_partid;

update tmp_fact_materiallisting f
SET f.dim_partid = upd.dim_partid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_materiallisting_update_partid upd, tmp_fact_materiallisting f
WHERE f.fact_materiallistingid = upd.fact_materiallistingid
AND f.dim_partid <> upd.dim_partid;

drop table if exists fact_materiallisting_update_partid;
/* TO CHECK
MERGE INTO tmp_fact_materiallisting fact
USING (SELECT DISTINCT f.fact_materiallistingid, FIRST_VALUE(dp.dim_partid) OVER (PARTITION BY dp.partnumber ORDER BY dp.dim_partid desc) AS dim_partid
FROM tmp_fact_materiallisting f, dim_part dp, dim_salesorg so, dim_plant p
WHERE f.dd_part = dp.partnumber
AND f.dd_salesorg = so.salesorgcode
AND so.companycode = p.companycode
AND p.plantcode = dp.plant
AND f.dim_partid <> dp.dim_partid) src
ON fact.fact_materiallistingid = src.fact_materiallistingid
WHEN MATCHED THEN UPDATE
SET fact.dim_partid = src.dim_partid,
dw_update_date = CURRENT_TIMESTAMP */

/* Madalina 23 Jan 2017 - Couldn't find a correspondent for all plants - BI-5287 */
UPDATE tmp_fact_materiallisting f
SET f.dim_dateidvalidenddate = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM dim_date dt, dim_salesorg so, tmp_fact_materiallisting f /* , dim_plant pl */
WHERE f.dd_dateidvalidenddate = dt.datevalue
/* AND f.dim_plantid = pl.dim_plantid */
AND f.dd_salesorg = so.salesorgcode
AND dt.companycode = so.companycode
/*AND dt.plantcode_factory = pl.plantcode */
AND dt.plantcode_factory = 'Not Set'
AND f.dim_dateidvalidenddate <> dt.dim_dateid;

UPDATE tmp_fact_materiallisting f
SET f.dim_dateidvalidstartdate = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM dim_date dt, dim_salesorg so, tmp_fact_materiallisting f /*, dim_plant pl */
WHERE f.dd_dateidvalidstartdate = dt.datevalue
/* AND f.dim_plantid = pl.dim_plantid */
AND f.dd_salesorg = so.salesorgcode
AND dt.companycode = so.companycode
/* AND dt.plantcode_factory = pl.plantcode */
AND dt.plantcode_factory = 'Not Set'
AND f.dim_dateidvalidstartdate <> dt.dim_dateid;

merge into tmp_fact_materiallisting f
using (select distinct f.fact_materiallistingid, first_value(cms.dim_customermastersalesid) over (partition by f.fact_materiallistingid order by f.fact_materiallistingid) as dim_customermastersalesid
FROM dim_customermastersales cms, tmp_fact_materiallisting f
WHERE f.dd_salesorg = cms.salesorg
AND f.dd_customer = cms.customernumber
AND f.dd_distributionchannel = cms.distributionchannel ) t
on t.fact_materiallistingid=f.fact_materiallistingid
when matched then update set f.dim_customermastersalesid = t.dim_customermastersalesid
where f.dim_customermastersalesid <> t.dim_customermastersalesid;

UPDATE tmp_fact_materiallisting f
SET f.dim_partsalesid = ps.dim_partsalesid,
dw_update_date = CURRENT_TIMESTAMP
FROM dim_partsales ps, tmp_fact_materiallisting f
WHERE f.dd_salesorg = ps.salesorgcode
AND f.dd_part = ps.partnumber
AND f.dd_distributionchannel = ps.distributionchannelcode
AND f.dim_partsalesid <> ps.dim_partsalesid;

UPDATE tmp_fact_materiallisting f
SET f.dim_customegroupid = cg.Dim_CustomerGroupId,
dw_update_date = CURRENT_TIMESTAMP
FROM dim_CustomerGroup cg, tmp_fact_materiallisting f
WHERE
f.dd_customergroup = cg.CustomerGroup
AND f.dim_customegroupid <> cg.Dim_CustomerGroupId;

update tmp_fact_materiallisting f
SET f.dim_customegroupid = cg.dim_customergroupid,
dw_update_date = CURRENT_TIMESTAMP
FROM dim_customermastersales cms, dim_customergroup cg, tmp_fact_materiallisting f
WHERE
f.dim_customermastersalesid = cms.dim_customermastersalesid
AND cms.customergroup = cg.customergroup
AND dd_mloncustomer = 'X'
AND f.dim_customegroupid <> cg.dim_customergroupid;



/*
insert into tmp_fact_materiallisting(
fact_materiallistingid,
dd_application,
dd_matlistingtype,
dim_salesorgid,
dim_distributionchannelid,
dim_customerid,
dim_partid,
dim_dateidvalidenddate,
dim_dateidvalidstartdate,
dim_customermastersalesid,
dim_partsalesid)

SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_materiallisting') + row_number() over() AS fact_materiallistingid,
ifnull(k.kotg004_kappl,'Not Set') AS dd_application,
ifnull(k.kotg004_kschl,'Not Set') AS dd_matlistingtype,
ifnull((SELECT so.dim_salesorgid FROM dim_salesorg so
WHERE ifnull(k.kotg004_vkorg,'Not Set') = so.salesorgcode),1) AS dim_salesorgid,
ifnull((SELECT dc.dim_distributionchannelid FROM dim_distributionchannel dc
WHERE ifnull(k.kotg004_vtweg,'Not Set') = dc.distributionchannelcode),1) AS dim_distributionchannelid,
ifnull((SELECT c.dim_customerid FROM dim_customer c
WHERE ifnull(k.kotg004_kunnr,'Not Set') = c.customernumber),1) AS dim_customerid,
ifnull((SELECT dp.dim_partid FROM dim_part dp
WHERE ifnull(k.kotg004_matnr,'Not Set') = dp.partnumber),1) AS dim_partid,
ifnull((SELECT dt.dim_dateid FROM dim_date dt, dim_salesorg so
WHERE ifnull(k.kotg004_datbi,'0001-01-01') = dt.datevalue
AND ifnull(k.kotg004_vkorg,'Not Set') = so.salesorgcode
AND dt.companycode = so.companycode
),1) AS dim_dateidvalidenddate,
ifnull((SELECT dt.dim_dateid FROM dim_date dt, dim_salesorg so
WHERE ifnull(k.kotg004_datab,'0001-01-01') = dt.datevalue
AND ifnull(k.kotg004_vkorg,'Not Set') = so.salesorgcode
AND dt.companycode = so.companycode
),1) AS dim_dateidvalidstartdate,
ifnull((SELECT cms.dim_customermastersalesid FROM dim_customermastersales cms
WHERE ifnull(k.kotg004_vkorg,'Not Set') = cms.salesorg
AND ifnull(k.kotg004_kunnr,'Not Set') = cms.customernumber),1) AS dim_customermastersalesid,
ifnull((SELECT ps.dim_partsalesid FROM dim_partsales ps
WHERE ifnull(k.kotg004_vkorg,'Not Set') = ps.salesorgcode
AND ifnull(k.kotg004_matnr,'Not Set') = ps.partnumber
AND ifnull(k.kotg004_vtweg,'Not Set') = ps.distributionchannelcode),1) AS dim_partsalesid

FROM KOTG_matlisting k */


TRUNCATE TABLE fact_materiallisting;
INSERT INTO fact_materiallisting
SELECT * FROM  tmp_fact_materiallisting;

DROP TABLE IF EXISTS tmp_fact_materiallisting;