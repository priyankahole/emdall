UPDATE fact_planorder fp
SET fp.Dim_CustomerGroup1id = fs1.Dim_CustomerGroup1id
FROM fact_salesorder fs1,fact_planorder fp
WHERE fp.dd_SalesOrderNo = fs1.dd_SalesDocNo AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo AND fp.dd_SalesOrderScheduleNo = fs1.dd_ScheduleNo
AND ifnull(fp.Dim_CustomerGroup1id,-1) <> ifnull(fs1.Dim_CustomerGroup1id,-2);

UPDATE fact_planorder fp
SET fp.Dim_CustomerID = fs1.Dim_CustomerID
FROM fact_salesorder fs1,fact_planorder fp
WHERE fp.dd_SalesOrderNo = fs1.dd_SalesDocNo AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo AND fp.dd_SalesOrderScheduleNo = fs1.dd_ScheduleNo
AND ifnull(fp.Dim_CustomerID,-1) <> ifnull(fs1.Dim_CustomerID,-2);

UPDATE fact_planorder fp
SET fp.Dim_DocumentCategoryid = fs1.Dim_DocumentCategoryid
FROM fact_salesorder fs1,fact_planorder fp
WHERE fp.dd_SalesOrderNo = fs1.dd_SalesDocNo AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo AND fp.dd_SalesOrderScheduleNo = fs1.dd_ScheduleNo
AND ifnull(fp.Dim_DocumentCategoryid,-1) <> ifnull(fs1.Dim_DocumentCategoryid,-2);

UPDATE fact_planorder fp
SET fp.Dim_SalesDocumentTypeid = fs1.Dim_SalesDocumentTypeid
FROM fact_salesorder fs1,fact_planorder fp
WHERE fp.dd_SalesOrderNo = fs1.dd_SalesDocNo AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo AND fp.dd_SalesOrderScheduleNo = fs1.dd_ScheduleNo
AND ifnull(fp.Dim_SalesDocumentTypeid,-1) <> ifnull(fs1.Dim_SalesDocumentTypeid,-2);


