/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_scm_fact.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 17 Mar 2017 */
/*  */
/*  */
/*   Description    : Populate SCM Fact */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */


/* instead of drop the temporary fact and recreating -> compare the describes and add the missing columns - to do if necessary !!!
/* Another possible optimization - the IL deletions */
DROP TABLE IF EXISTS fact_scm_test;
create table fact_scm_test 
like fact_mmprodhierarchy INCLUDING DEFAULTS INCLUDING IDENTITY;

/* ******************************************************************************************************************************************************************************** */
/* ****************************************first generate the temp tables containing the basic material states, along with the basic states transitions *************************** */
/* Source tables for this first part: Material movement, Purchasing, Inspection Lot, Production Order */
/* ******************************************************************************************************************************************************************************** */

drop table if exists tmp_basic_mat_states;
create table tmp_basic_mat_states as
select 'X' as raw, 'X' as bulk, 'X' as fpu, 'X' as fpp, 1 as ct_1;

delete from number_fountain m where m.table_name = 'tmp_basic_mat_states_combinations';
insert into number_fountain
select 'tmp_basic_mat_states_combinations', 1;  /*always recreate from the first rowid*/

drop table if exists tmp_basic_mat_states_combinations;
create table tmp_basic_mat_states_combinations as
select (select max_id  from number_fountain   where table_name = 'tmp_basic_mat_states_combinations') + row_number() over(ORDER BY '') AS tmp_basic_mat_states_combinationsId,
	 	 raw, bulk, fpu, fpp,  sum(ct_1) as ct_1
	from tmp_basic_mat_states
	group by cube (raw, bulk, fpu, fpp);

/*this table will be repopulated on each iteration*/
/* drop table if exists tmp_basic_active_materials_list
create table tmp_basic_active_materials_list (active_material varchar(10)) */

/*script creation for create_basic_temp_tables can be found in vw_bi_populate_scm_fact_create_exa_scripts.sql*/

execute script create_basic_temp_tables();



/* ******************************************************************************************************************************************************************************** */
/* ****************************************second step: generate the combinations of the material states ************************************************************************** */
/* Source tables for this first part: Material movement, Purchasing, Inspection Lot, Production Order */
/* ******************************************************************************************************************************************************************************** */


/*this table will hold all material states available in the area*/
drop table if exists tmp_mat_states;
create table tmp_mat_states as
select 'X' as dd_raw_1, 'X' dd_bulk_1, 'X' dd_bulk_2, 'X' dd_bulk_3, 'X' dd_fpu_1, 'X' dd_fpp_1, 1 as ct_1;  /* dummy values to generate combinations of materials */

/*create a temp table, from which I delete the unavailable material combinations, and insert into tmp_mat_states_combinations-> to keep a mapping between pkey and the row number */
drop table if exists tmp_mat_states_combinations_temp;
create table tmp_mat_states_combinations_temp as
select dd_raw_1, dd_bulk_1, dd_bulk_2, dd_bulk_3, dd_fpu_1, dd_fpp_1, sum(ct_1) as ct_1
from tmp_mat_states
group by cube (dd_raw_1, dd_bulk_1, dd_bulk_2, dd_bulk_3, dd_fpu_1, dd_fpp_1);

/*delete the combinations that are not valid*/
delete from tmp_mat_states_combinations_temp where (dd_bulk_2 = 'X' and dd_bulk_1 is null) or (dd_bulk_3 = 'X' and dd_bulk_1 is null) or ( dd_bulk_3 = 'X' and dd_bulk_2 is null);

/*determine the number of active materials per combination - in order to first process the combinations with the highest number of active materials */
update tmp_mat_states_combinations_temp 
set ct_1 = (
	case when dd_raw_1 = 'X' then 1 else 0 end +
	case when dd_bulk_1 = 'X' then 1 else 0 end +
	case when dd_bulk_2 = 'X' then 1 else 0 end +
	case when dd_bulk_3 = 'X' then 1 else 0 end +
	case when dd_fpu_1 = 'X' then 1 else 0 end +
	case when dd_fpp_1 = 'X' then 1 else 0 end );

/*no need to process the combinations of 1 active material*/
delete from tmp_mat_states_combinations_temp where ct_1 in ( 0, 1);

/*keep the correct order of combinations*/
drop table if exists tmp_mat_states_combinations_temp2;
create table tmp_mat_states_combinations_temp2 as
select * from tmp_mat_states_combinations_temp
order by ct_1 desc;

/*generate the final form of combinations of materials*/
/*it is VERY IMPORTANT to always start with 1 as pkey -> because this will be used when iterating through combinations*/
delete from number_fountain m where m.table_name = 'tmp_mat_states_combinations';
insert into number_fountain
select 'tmp_mat_states_combinations', 1;

drop table if exists tmp_mat_states_combinations;
create table tmp_mat_states_combinations as
select (select max_id  from number_fountain   where table_name = 'tmp_mat_states_combinations') + row_number() over(ORDER BY '') AS tmp_mat_states_combinationsId,
	 dd_raw_1, dd_bulk_1, dd_bulk_2, dd_bulk_3, dd_fpu_1, dd_fpp_1, ct_1
from tmp_mat_states_combinations_temp2;

/*this table will be repopulated on each iteration*/
/*drop table if exists tmp_active_materials_list
create table tmp_active_materials_list (active_material varchar(10)) */

/*script creation for fact_inserts can be found in vw_bi_populate_scm_fact_create_exa_scripts.sql*/
execute script fact_inserts();  
/*use this option - WITH OUTPUT -only for testing, because it won't stop the execution of the script*/

/*execution time for 1 = 22sec
					 2 = 35min
					 3( deletion) = 5min			*/
/* ******************************************************************************************************************************************************************************** */
/* ****************************************third step: fact deletions  ************************************************************************************************************ */
/* ******************************************************************************************************************************************************************************** */

/* Only include inspections starting with 1, 3 or 4 - BI-4358 */
/*tmp_transpose will always keep a list of material states, no matter if they were active or not in a specific combination*/

/*script creation for fact_deletions can be found in vw_bi_populate_scm_fact_create_exa_scripts.sql*/
execute script fact_deletions();
/* END BI-4358 */

/* BI-4287 - delete returns */
merge into fact_scm_test mmp
using ( select distinct mmp.fact_mmprodhierarchyid from fact_scm_test mmp, fact_salesorder so
	where dd_ReturnsItem = 'X'
	and mmp.dd_salesdocno_customer = so.dd_salesdocno
	and mmp.dd_salesitemno_customer = so.dd_salesitemno ) del
on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
when matched then delete;

/* ******************************************************************************************************************************************************************************** */
/* ****************************************fourth step: fact updates ************************************************************************************************************** */
/* ******************************************************************************************************************************************************************************** */

update fact_scm_test mmp
set DIM_CURRENCYID_TRA = c.dim_currencyid
from fact_scm_test mmp, dim_currency c
where currencycode = 'TRA'
and DIM_CURRENCYID_TRA <> c.dim_currencyid;

/* Adding Sales Order Amount */
merge into  fact_scm_test mmp
using ( select distinct fact_mmprodhierarchyid, 
		first_value((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) ELSE f_so.amt_ScheduleTotal END)) over (partition by fact_mmprodhierarchyid order by '') as amt_orderAmountSales
		from fact_salesorder f_so, fact_scm_test mmp, dim_salesmisc dsi
		where mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
			and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo
			and f_so.dim_salesmiscid = dsi.dim_salesmiscid ) upd
on mmp.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update
set mmp.amt_orderAmountSales = upd.amt_orderAmountSales;

/* Add amt_ExchangeRate, based on the related on from Sales */
merge into fact_scm_test mmp
using ( select distinct dd_SalesDocNo, dd_SalesItemNo, f_so.amt_ExchangeRate
	from  fact_salesorder f_so ) f_so
on  mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
	and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo 
when matched then update
set  mmp.amt_ExchangeRate = f_so.amt_ExchangeRate;

/* Add amt_ExchangeRate_GBL, based on the related on from Sales */
merge into fact_scm_test mmp
using ( select distinct dd_SalesDocNo, dd_SalesItemNo, /* f_so.amt_ExchangeRate_GBL - Correct unstable set of rows*/
	first_value(f_so.amt_ExchangeRate_GBL) over (partition by dd_SalesDocNo, dd_SalesItemNo order by '') as amt_ExchangeRate_GBL
	from  fact_salesorder f_so ) f_so
on  mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
	and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo 
when matched then update
set  mmp.amt_ExchangeRate_GBL = f_so.amt_ExchangeRate_GBL;

/* Set Order Sales Amount as Quantity, always expressed in global conversion */
update fact_scm_test
set amt_orderAmountSales = amt_orderAmountSales * amt_ExchangeRate_GBL;

/* BI-4227 Adding Material FPP COMOPS field */
update fact_scm_test mmp
set mmp.dim_partid_comops = fpp_comops.dim_partid
from fact_scm_test mmp, dim_part fpp, dim_part fpp_comops, dim_plant p_comops
where mmp.dim_partid_fpp_1 = fpp.dim_partid 
	and mmp.dim_plantid_comops = p_comops.dim_plantid
	and fpp_comops.partnumber = fpp.partnumber
	and fpp_comops.plant = p_comops.plantcode
	and mmp.dim_partid_comops <> fpp_comops.dim_partid;

/* Add Customer fields */
merge into fact_scm_test mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridShipTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_comops = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_comops = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_comops = upd.dd_salesdocno
	and mmp.dd_salesitemno_comops = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerShipComops = upd.Dim_CustomeridShipTo;

merge into fact_scm_test mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridSoldTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_comops = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_comops = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_comops = upd.dd_salesdocno
	and mmp.dd_salesitemno_comops = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerSoldComops = upd.Dim_CustomeridSoldTo;

merge into fact_scm_test mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridShipTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_customer = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_customer = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_customer = upd.dd_salesdocno
	and mmp.dd_salesitemno_customer = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerShipCust = upd.Dim_CustomeridShipTo;

merge into fact_scm_test mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridSoldTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_customer = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_customer = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_customer = upd.dd_salesdocno
	and mmp.dd_salesitemno_customer = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerSoldCust = upd.Dim_CustomeridSoldTo;

/* create temp table based on Planned Order */
drop table if exists tmp_PlanOrder_TargetPerPart;
create table tmp_PlanOrder_TargetPerPart as
select partnumber, 
	round(case when avg(df.businessdaysseqno - ds.businessdaysseqno) < '0.5' then 1
		else avg(df.businessdaysseqno - ds.businessdaysseqno) end) as targerMasterRecipe
from fact_planorder fp, dim_date ds, dim_date df, dim_part p
where fp.dd_PlannScenario = '0'
	and fp.dim_dateidstart = ds.dim_dateid
	and ds.datevalue > current_date
	and fp.dim_dateidfinish = df.dim_dateid
	and fp.dim_partid = p.dim_partid
group by partnumber;

/* Update the Target Master Recipe Days BI-4755*/
/*script creation for fact_update_target_master_recipe_days can be found in vw_bi_populate_scm_fact_create_exa_scripts.sql*/
execute script fact_update_target_master_recipe_days();

/* Use dim instead of dd for 'Date User Status SAMR was last set' and 'Date User Status QCCO was last set' - BI-4772  */
/*script creation for fact_update_date_user_status can be found in vw_bi_populate_scm_fact_create_exa_scripts.sql*/
execute script fact_update_date_user_status();

/* Add fields from Min Max: Min released stock in days and Lot size in days - BI-4868 */
/* add fields Float before production (in days) - BI-4886	*/
/*script creation for fact_update_min_max_measures can be found in vw_bi_populate_scm_fact_create_exa_scripts.sql*/
execute script fact_update_min_max_measures();

/* update the dim dates earlier in the script */
/* Update Actual receipt date to ComOps, according to the relation with Inspection Lot and EKBE - BI-4288 */
update fact_scm_test mm
set mm.dim_dateidactualreceipt_comops = dd.dim_dateid
from fact_inspectionlot f
	inner join dim_batch b ON f.dim_batchid = b.dim_batchid
	inner join EKBE_deliv d ON f.dd_materialdocno = d.EKBE_BELNR 
							AND f.dd_MaterialDocItemNo = d.EKBE_BUZEI
							AND d.EKBE_CHARG = b.batchnumber 
							AND d.EKBE_MATNR = b.partnumber 
							AND d.EKBE_WERKS = b.plantcode
	inner join fact_scm_test mm ON f.dd_inspectionlotno = mm.DD_INSPECTIONLOTNO_COMOPS
	inner join dim_plant p ON mm.dim_plantid_comops = p.dim_plantid
	inner join dim_date dd ON dd.datevalue = ifnull(d.EKBE_BUDAT,'Not Set')
							AND dd.plantcode_factory = p.plantcode
							AND dd.companycode = p.companycode
where mm.dim_dateidactualreceipt_comops <> dd.dim_dateid;

/* BI-4288 - Point Usage Decision Made Date FPP to Plant */
update fact_scm_test fmm
set fmm.dim_dateidusagedecisionmade_fpp_1 = danew.dim_dateid
from fact_scm_test fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateidusagedecisionmade_fpp_1 = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateidusagedecisionmade_fpp_1 <> danew.dim_dateid;

/* Point Shipment Create Date to ComOps to Plant */  
update fact_scm_test fmm
set fmm.dim_dateiddlvrdoccreated_comops = danew.dim_dateid
from fact_scm_test fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateiddlvrdoccreated_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateiddlvrdoccreated_comops <> danew.dim_dateid;

/* Point Actual Goods Issue Date to ComOps to Plant */  
update fact_scm_test fmm
set fmm.dim_dateidactualgoodsissue_comops = danew.dim_dateid
from fact_scm_test fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateidactualgoodsissue_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateidactualgoodsissue_comops <> danew.dim_dateid;

/* BI-4288 - Point Actual Receipt Date to ComOps and Usage Decision Made Date ComOps to the same Plant */
update fact_scm_test fmm
set fmm.dim_dateidactualreceipt_comops = danew.dim_dateid
from fact_scm_test fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidactualreceipt_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateidactualreceipt_comops <> danew.dim_dateid;

update fact_scm_test fmm
set fmm.dim_dateidusagedecisionmade_comops = danew.dim_dateid
from fact_scm_test fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidusagedecisionmade_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateidusagedecisionmade_comops <> danew.dim_dateid;

/* Point Shipment Create Date to Customer to PlantComOps */
update fact_scm_test fmm
set fmm.dim_dateiddlvrdoccreated_customer = danew.dim_dateid
from fact_scm_test fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateiddlvrdoccreated_customer = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateiddlvrdoccreated_customer <> danew.dim_dateid;

/* Point Actual Goods Issue Date to Customer to PlantComops */
update fact_scm_test fmm
set fmm.dim_dateidactualgoodsissue_customer = danew.dim_dateid
from fact_scm_test fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidactualgoodsissue_customer = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateidactualgoodsissue_customer <> danew.dim_dateid;
	
/* Add Hit or miss (E2E LT) and Sales order hit- BI-4857 */
/* BI-5265 - update formulas - include ACTIVE_PROP_UNIT in addition to ACTIVE */
/* Correction: dont't use ma.batchclass_merck in grouping */
drop table if exists tmp_upd_hitormissE2E_LT;
create table tmp_upd_hitormissE2E_LT as
select dd_SalesDocNo_customer, dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode as PlantCodeSite,  	mfp.PartNumber, agidcc.MonthYear,
	CASE WHEN (CASE WHEN (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 ELSE 0 END) +

						MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_BULK_1) ) +
						MAX( (mab.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_BULK_1) *5/7 /2 ) +

						MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_FPU_1) ) + 
						MAX( (mf.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +

						MAX( (CT_FLOATBEFOREPRODUCTION_FPP_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_FPP_1) ) + 
						MAX( (mfp.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_FPP_1) *5/7 /2) +

						MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
						MAX( (mfpc.GRProcessingTime) )+
						MAX( (CT_MINRELEASEDSTOCKDAYS_comops) *5/7) +
						MAX( (CT_LOTSIZEDAYS_comops) *5/7 /2) 


						) =0
					THEN 999
					ELSE ( (( (MAX( (agidcc.DateValue) - (grdr.DateValue) )*5/7) - (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 ELSE 0 END) +

						MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_BULK_1) ) +
						MAX( (mab.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_BULK_1) *5/7 /2 ) +

						MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_FPU_1) ) + 
						MAX( (mf.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +

						MAX( (CT_FLOATBEFOREPRODUCTION_FPP_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_FPP_1) ) + 
						MAX( (mfp.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_FPP_1) *5/7 /2) +

						MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
						MAX( (mfpc.GRProcessingTime) )+
						MAX( (CT_MINRELEASEDSTOCKDAYS_comops) *5/7) +
						MAX( (CT_LOTSIZEDAYS_comops) *5/7 /2) 


						) )) / (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 ELSE 0 END) +
						
						MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_BULK_1) ) +
						MAX( (mab.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_BULK_1) *5/7 /2 ) +

						MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_FPU_1) ) + 
						MAX( (mf.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +

						MAX( (CT_FLOATBEFOREPRODUCTION_FPP_1) ) +
						MAX( (CT_TARGETMASTERRECIPE_FPP_1) ) + 
						MAX( (mfp.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7) +
						MAX( (CT_LOTSIZEDAYS_FPP_1) *5/7 /2) +

						MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
						MAX( (mfpc.GRProcessingTime) )+
						MAX( (CT_MINRELEASEDSTOCKDAYS_comops) *5/7) +
						MAX( (CT_LOTSIZEDAYS_comops) *5/7 /2) 


						) )*100
					END) > 25
			THEN '0'
			WHEN (CASE WHEN (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 ELSE 0 END) +

					MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_BULK_1) ) +
					MAX( (mab.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_BULK_1) *5/7 /2 ) +

					MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_FPU_1) ) + 
					MAX( (mf.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +

					MAX( (CT_FLOATBEFOREPRODUCTION_FPP_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_FPP_1) ) + 
					MAX( (mfp.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_FPP_1) *5/7 /2) +

					MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
					MAX( (mfpc.GRProcessingTime) )+
					MAX( (CT_MINRELEASEDSTOCKDAYS_comops) *5/7) +
					MAX( (CT_LOTSIZEDAYS_comops) *5/7 /2) 


					) =0
				THEN 999
				ELSE ( (( (MAX( (agidcc.DateValue) - (grdr.DateValue) )*5/7) - (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 ELSE 0 END) +

					MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_BULK_1) ) +
					MAX( (mab.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_BULK_1) *5/7 /2 ) +

					MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_FPU_1) ) + 
					MAX( (mf.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +

					MAX( (CT_FLOATBEFOREPRODUCTION_FPP_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_FPP_1) ) + 
					MAX( (mfp.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_FPP_1) *5/7 /2) +

					MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
					MAX( (mfpc.GRProcessingTime) )+
					MAX( (CT_MINRELEASEDSTOCKDAYS_comops) *5/7) +
					MAX( (CT_LOTSIZEDAYS_comops) *5/7 /2) 


					) )) / (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 ELSE 0 END) +

					MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_BULK_1) ) +
					MAX( (mab.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_BULK_1) *5/7 /2 ) +

					MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_FPU_1) ) + 
					MAX( (mf.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +

					MAX( (CT_FLOATBEFOREPRODUCTION_FPP_1) ) +
					MAX( (CT_TARGETMASTERRECIPE_FPP_1) ) + 
					MAX( (mfp.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7) +
					MAX( (CT_LOTSIZEDAYS_FPP_1) *5/7 /2) +

					MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
					MAX( (mfpc.GRProcessingTime) )+
					MAX( (CT_MINRELEASEDSTOCKDAYS_comops) *5/7) +
					MAX( (CT_LOTSIZEDAYS_comops) *5/7 /2) 


					) )*100
			END) < -25
	THEN '0'
	ELSE '100'
	END as ct_hitormissE2E_LT,
/* Add Actual E2E LT and Target E2E LT - for Weighted av. Actual LT and Weighted av. Target LT - BI-4857 */
/* Actual E2E LT */	
	MAX( (agidcc.DateValue) - (grdr.DateValue) ) *5/7 as ct_actualE2E_LT,
/* Target E2E LT */
	MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 ELSE 0 END) +
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 ELSE 0 END) +

	MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) ) +
	MAX( (CT_TARGETMASTERRECIPE_BULK_1) ) +
	MAX( (mab.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7) +
	MAX( (CT_LOTSIZEDAYS_BULK_1) *5/7 /2 ) +

	MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) ) +
	MAX( (CT_TARGETMASTERRECIPE_FPU_1) ) + 
	MAX( (mf.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7) +
	MAX( (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +

	MAX( (CT_FLOATBEFOREPRODUCTION_FPP_1) ) +
	MAX( (CT_TARGETMASTERRECIPE_FPP_1) ) + 
	MAX( (mfp.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7) +
	MAX( (CT_LOTSIZEDAYS_FPP_1) *5/7 /2) +

	MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
	MAX( (mfpc.GRProcessingTime) )+
	MAX( (CT_MINRELEASEDSTOCKDAYS_comops) *5/7) +
	MAX( (CT_LOTSIZEDAYS_comops) *5/7 /2)  as ct_targetE2E_LT,
/* Madalina 13 Feb 2017 - BI-4857 */
/* Create new measures in order to obtain different values for Total Weighted av. Actual LT and Total Weighted av. Target LT*/ 
/* Actual LT * Sales - backend and Target LT * Sales - backend */
	avg(AMT_ORDERAMOUNTSALES) * MAX( (agidcc.DateValue) - (grdr.DateValue) ) *5/7 as ct_actualLTSales,
	avg(AMT_ORDERAMOUNTSALES) * (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 ELSE 0 END) +
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 ELSE 0 END) +

	MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) ) +
	MAX( (CT_TARGETMASTERRECIPE_BULK_1) ) +
	MAX( (mab.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7) +
	MAX( (CT_LOTSIZEDAYS_BULK_1) *5/7 /2 ) +

	MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) ) +
	MAX( (CT_TARGETMASTERRECIPE_FPU_1) ) + 
	MAX( (mf.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7) +
	MAX( (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +

	MAX( (CT_FLOATBEFOREPRODUCTION_FPP_1) ) +
	MAX( (CT_TARGETMASTERRECIPE_FPP_1) ) + 
	MAX( (mfp.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7) +
	MAX( (CT_LOTSIZEDAYS_FPP_1) *5/7 /2) +

	MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
	MAX( (mfpc.GRProcessingTime) )+
	MAX( (CT_MINRELEASEDSTOCKDAYS_comops) *5/7) +
	MAX( (CT_LOTSIZEDAYS_comops) *5/7 /2) ) as ct_TargetLTSales

from fact_scm_test fmm, dim_part mab, dim_part mfpc, dim_part mfp, dim_part mf, dim_part ma, dim_date grdr, dim_date agidcc, dim_plant pl, dim_plant plsite
where fmm.dim_partid_bulk_1 = mab.dim_partid
	and fmm.dim_partid_comops = mfpc.dim_partid
	and fmm.dim_partid_fpp_1 =	mfp.dim_partid
	and fmm.dim_partid_fpu_1 = mf.dim_partid
	and fmm.dim_partid_raw_1 = ma.dim_partid
	and fmm.dim_dateidpostingdate = grdr.dim_dateid
	and fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
	and fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_plantid = plsite.dim_plantid
	and agidcc.datevalue >= add_years(current_date, -2)
	and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')
group by dd_SalesDocNo_customer, dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode, mfp.PartNumber, agidcc.MonthYear;

update fact_scm_test fmm
set fmm.ct_hitormissE2E_LT = tmp.ct_hitormissE2E_LT,
	fmm.ct_actualE2E_LT = tmp.ct_actualE2E_LT,
	fmm.ct_targetE2E_LT = tmp.ct_targetE2E_LT,
	fmm.ct_actualLTSales = tmp.ct_actualLTSales,
	fmm.ct_TargetLTSales = tmp.ct_TargetLTSales
from fact_scm_test fmm, tmp_upd_hitormissE2E_LT tmp, dim_part ma, dim_date agidcc, dim_plant pl, dim_plant plsite, dim_part mfp
where fmm.dd_SalesDocNo_customer = tmp.dd_SalesDocNo_customer
	and fmm.dd_SalesItemNo_customer = tmp.dd_SalesItemNo_customer
	and fmm.dim_partid_raw_1 = ma.dim_partid
	and fmm.dim_partid_fpp_1 =	mfp.dim_partid
	and fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
	and fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_plantid = plsite.dim_plantid
	and agidcc.MonthYear = tmp.MonthYear
	and mfp.PartNumber = tmp.PartNumber
	and plsite.PlantCode = tmp.PlantCodeSite
	and pl.PlantCode = tmp.PlantCode
	and mfp.ProductFamily_Merck = tmp.ProductFamily_Merck;
	
update fact_scm_test fmm
set fmm.ct_hitvalue = (case when (CT_HITORMISSE2E_LT) = 0 then 0 else (AMT_ORDERAMOUNTSALES) end);	

/* End BI-4857 */	
	
/* Reset all the values from the Sales aggregation dimension - BI-3287 */
/* BI-5265 - include ACTIVE_PROP_UNIT in addition to ACTIVE */
delete from dim_aggregationMMProdOrd;

delete from number_fountain m where m.table_name = 'dim_aggregationMMProdOrd';
insert into number_fountain
select 'dim_aggregationMMProdOrd',
ifnull(max(da.dim_aggregationMMProdOrdId),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_aggregationMMProdOrd da;

insert into dim_aggregationMMProdOrd
	(	
		dim_aggregationMMProdOrdId,
		ProductFamily_Merck,
		PlantCode,
		PlantCodeSite,
		PartNumber,
		MonthYear,
		sum_fpp,
		salesOrderHit_fpp,
		totalSalesOrderValueHitUSD_fpp,
		TotalActualLTSales_fpp,
		TotalTargetLTSales_fpp
	)
select (select max_id  from number_fountain   where table_name = 'dim_aggregationMMProdOrd') + row_number() over(ORDER BY '') AS dim_aggregationMMProdOrdId,
	x.ProductFamily_Merck, 
	x.PlantCode, 
	x.PlantCodeSite,
	x.PartNumber,	
	x.MonthYear,
    sum(amt_orderAmountSales) sum_fpp,
	avg(ct_hitormissE2E_LT) as salesOrderHit_fpp,
	sum(ct_hitvalue) as totalSalesOrderValueHitUSD_fpp,
	sum(ct_actualLTSales) as TotalActualLTSales_fpp,
	sum(ct_TargetLTSales) as TotalTargetLTSales_fpp
	/* avg(amt_orderAmountSales)/ ( case when sum(amt_orderAmountSales) <> 0 then  sum(amt_orderAmountSales) else 1 end) * max(ct_actualE2E_LT) as weightedActualLT_fpp,
	avg(amt_orderAmountSales)/ ( case when sum(amt_orderAmountSales) <> 0 then  sum(amt_orderAmountSales) else 1 end) * max(ct_targetE2E_LT) as weightedTargetLT_fpp */
from 
	(select mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, p.ProductFamily_Merck, pl.PlantCode, 
		p.PartNumber, plsite.PlantCode as PlantCodeSite, dd.MonthYear,
		/* amt_orderAmountSales * amt_ExchangeRate_GBL as amt_orderAmountSales, */
		amt_orderAmountSales as amt_orderAmountSales,
		ct_hitormissE2E_LT as ct_hitormissE2E_LT,
		ct_hitvalue as ct_hitvalue,
		ct_actualLTSales as ct_actualLTSales,
		ct_TargetLTSales as ct_TargetLTSales,
		row_number() over ( partition by mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, p.ProductFamily_Merck, pl.PlantCode,
							p.PartNumber, plsite.PlantCode, dd.MonthYear order by '') rn
		from fact_scm_test mmp, dim_part p, dim_date dd, dim_plant pl, dim_plant plsite, dim_part ma
		where mmp.dim_partid_fpp_1 = p.dim_partid
		and mmp.dim_plantid_comops = pl.dim_plantid
		and mmp.dim_plantid = plsite.dim_plantid
		and mmp.dim_dateidactualgoodsissue_customer = dd.dim_dateid
		and dd.datevalue >= add_years(current_date, -2)
		and mmp.dim_partid_raw_1 = ma.dim_partid
		and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')
		) x
	where x.rn = 1
	group by CUBE (ProductFamily_Merck, PlantCode, PlantCodeSite, PartNumber, MonthYear);  
	
/* Madalina 13 Feb 2017 - Change the calculation for Total Weighted av. Actual LT -backend and Total Weighted av. Target LT -backend BI-4857 */	
update dim_aggregationMMProdOrd dimagg
set dimagg.totalWeightedActualLT = TotalActualLTSales_fpp/ case when sum_fpp = 0 then 1 else sum_fpp end,
	dimagg.totalWeightedTargetLT = TotalTargetLTSales_fpp/ case when sum_fpp = 0 then 1 else sum_fpp end;

/* 26 jan 2017 Georgiana Changes - according to BI 5295: Insert all GPF codes in dim_aggregationMMProdOrd*/
delete from number_fountain m where m.table_name = 'dim_aggregationMMProdOrd';
insert into number_fountain
select 'dim_aggregationMMProdOrd',
ifnull(max(da.dim_aggregationMMProdOrdId),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_aggregationMMProdOrd da;

insert into dim_aggregationMMProdOrd (dim_aggregationMMProdOrdid,ProductFamily_Merck)
select (select max_id  from number_fountain   where table_name = 'dim_aggregationMMProdOrd') + row_number() over(ORDER BY '') AS dim_aggregationMMProdOrdId,
t.ProductFamily_Merck
from (
select distinct ProductFamily_Merck from dim_part dp
where not exists (select 1 from dim_aggregationMMProdOrd agg where dp.ProductFamily_Merck=agg.ProductFamily_Merck)) t;
/*End of changes 26 Jan 2017*/

update dim_aggregationMMProdOrd
set plantcode = 'ZZZZ' where Plantcode is null;

update dim_aggregationMMProdOrd
set ProductFamily_Merck = 'ZZZZ' where ProductFamily_Merck is null;

update dim_aggregationMMProdOrd
set PlantCodeSite = 'ZZZZ' where PlantCodeSite is null;

update dim_aggregationMMProdOrd
set PartNumber = 'ZZZZ' where PartNumber is null;

update dim_aggregationMMProdOrd
set MonthYear = 'ZZZZ' where MonthYear is null;

/*Georgiana Changes 23 Jan 2017 adding new field ct_overallsupplyhealthscore, according to BI-5295*/

drop table if exists tmp_for_upd_SupplyHealthScore;
create table tmp_for_upd_SupplyHealthScore as 
SELECT x.*  from       ( 
	SELECT 
	DFA.ProductFamilyDescription_Merck , 
	DFA.ct_avg_hit_miss AS  HitOrMissFPPComop, 
	MinMax.CT_HITORMISS AS GrobalHitLast2YDFA , 
	salesorder.ct_avg_hit_miss_lifr AS GrobalHitLast2YLIFR , 
	materialmaster.CAPACITY_SCORE AS CapacityScore,
   ifnull(mmprod.SalesORderValueHit,0) as SalesORderValueHit	
FROM (SELECT aplfprt.ProductFamilyDescription_Merck,ROUND(AVG(ct_avg_hit_miss),2) as ct_avg_hit_miss 
					FROM fact_atlaspharmlogiforecast_merck AS f_iaplf 
					INNER JOIN Dim_Part AS aplfprt ON f_iaplf.dim_partid = aplfprt.Dim_Partid  
					INNER JOIN Dim_Date AS repdt ON f_iaplf.dim_dateidreporting = repdt.Dim_Dateid  
			WHERE (repdt.DateValue  BETWEEN '2015-01-01' AND '2016-12-31')  
			AND f_iaplf.dd_version = 'SFA'  
			AND f_iaplf.dd_regiondestionation NOT IN ('Head Quarters','NOT APPLICABLE')  
	        GROUP BY  aplfprt.ProductFamilyDescription_Merck
	) DFA  
LEFT  JOIN (SELECT dp.ProductFamilyDescription_Merck,ROUND(AVG(CT_HITORMISS),2) as CT_HITORMISS 
                   FROM fact_inventoryatlashistory AS f_invatlashist 
                   INNER JOIN Dim_Date AS dd3 ON f_invatlashist.dim_snapshotdateid = dd3.Dim_Dateid  
                   INNER JOIN Dim_Part AS dp ON f_invatlashist.dim_partid = dp.Dim_Partid 
                   INNER JOIN Dim_Plant AS dpl ON f_invatlashist.dim_plantid = dpl.Dim_Plantid  
              WHERE  dd3.WeekDayAbbreviation  = 'Mon'
              AND dp.ItemSubType_Merck = 'FPP'
              AND dpl.tacticalring_merck = 'ComOps'
              GROUP BY  dp.ProductFamilyDescription_Merck
	        ) MinMax ON (  DFA.ProductFamilyDescription_Merck = MinMax.ProductFamilyDescription_Merck ) 
LEFT  JOIN  (SELECT prt.ProductFamilyDescription_Merck,ROUND(AVG(ct_avg_hit_miss_lifr),0) as ct_avg_hit_miss_lifr 
                   FROM fact_salesorder AS f_so 
                   INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid  
                   INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid  
            WHERE (soegidt.DateValue  != current_date) AND (soegidt.DateValue  BETWEEN '2015-01-01' AND '2016-12-31')  AND prt.ProductHierarchy = 'Not Set' AND f_so.dd_ItemRelForDelv = 'X' 
 GROUP BY  prt.ProductFamilyDescription_Merck
             ) salesorder ON (  DFA.ProductFamilyDescription_Merck = salesorder.ProductFamilyDescription_Merck ) 
 INNER JOIN (SELECT m.ProductFamilyDescription_Merck,ROUND(AVG( (m.CAPACITY_SCORE) ),0) as CAPACITY_SCORE 
                   FROM fact_materialmaster AS f_mmi 
                   INNER JOIN Dim_Part AS m ON f_mmi.dim_materialmasterid = m.Dim_Partid  
                   GROUP BY  m.ProductFamilyDescription_Merck
			)  materialmaster  ON (  DFA.ProductFamilyDescription_Merck = materialmaster.ProductFamilyDescription_Merck )
LEFT  JOIN (SELECT mfp.ProductFamilyDescription_Merck,AVG(100 * agg3.totalSalesOrderValueHitUSD_fpp / case when agg3.SUM_FPP=0 then 1 else agg3.SUM_FPP end) as  SalesORderValueHit 
 FROM fact_scm_test AS f_mmprodhier 
 INNER JOIN Dim_Part AS mfp ON f_mmprodhier.DIM_PARTID_FPP_1 = mfp.Dim_Partid 
 INNER JOIN Dim_Date AS agidcc ON f_mmprodhier.DIM_DATEIDACTUALGOODSISSUE_CUSTOMER = agidcc.Dim_Dateid  
 INNER JOIN Dim_Plant AS pc ON f_mmprodhier.DIM_PLANTID_COMOPS = pc.Dim_Plantid  
 INNER JOIN Dim_Plant AS pl ON f_mmprodhier.DIM_PLANTID = pl.Dim_Plantid  
 INNER JOIN Dim_Part AS mab ON f_mmprodhier.DIM_PARTID_BULK_1 = mab.Dim_Partid  
 INNER JOIN Dim_Part AS mfpc ON f_mmprodhier.DIM_PARTID_COMOPS = mfpc.Dim_Partid  
 INNER JOIN Dim_Part AS mf ON f_mmprodhier.DIM_PARTID_FPU_1 = mf.Dim_Partid  
 INNER JOIN Dim_Part AS ma ON f_mmprodhier.DIM_PARTID_RAW_1 = ma.Dim_Partid  
 INNER JOIN Dim_Date AS posl ON f_mmprodhier.DIM_DATEIDSCHEDORDER = posl.Dim_Dateid  
 INNER JOIN dim_aggregationMMProdOrd AS agg3 ON agg3.PlantCode = (case when 'ALL' = 'ALL' then 'ZZZZ' else pc.plantcode end) 
 and agg3.ProductFamily_Merck = (case when 'ZZZZ' = 'ALL' then 'ZZZZ' else mfp.ProductFamily_Merck end) 
 and agg3.PlantCodeSite = (case when 'ALL' = 'ALL' then 'ZZZZ' else pl.PlantCode end) 
 and agg3.PartNumber = (case when 'ALL' = 'ALL' then 'ZZZZ' else mfp.PartNumber end) and 
 agg3.MonthYear = (case when 'ALL' = 'ALL' then 'ZZZZ' else agidcc.MonthYear end)   
 GROUP BY  mfp.ProductFamilyDescription_Merck) mmprod
 ON (  DFA.ProductFamilyDescription_Merck = mmprod.ProductFamilyDescription_Merck )
) x order by ProductFamilyDescription_Merck ;

merge into fact_scm_test f_mmp
using (select distinct fact_mmprodhierarchyid,
0.15 *( case when SalesORderValueHit > 80 then 3 when SalesORderValueHit > 60 then 2 else 1 end)
+ 0.15 * (case when tmp.GrobalHitLast2YDFA > 80 then 3 when tmp.GrobalHitLast2YDFA > 60 then 2 else 1 end)
+ 0.15* ( case when tmp.HitOrMissFPPComop > 80 then 3 when tmp.HitOrMissFPPComop > 60 then 2 else 1 end)
+ 0.3 * tmp.CapacityScore
+ 0.25* ( case when tmp.GrobalHitLast2YLIFR > 80 then 3 else 1 end) as overallsupplyhealthscore
 from fact_scm_test f_mmp,tmp_for_upd_SupplyHealthScore tmp,dim_part dp 
where tmp.ProductFamilyDescription_Merck=dp.ProductFamilyDescription_Merck
and  f_mmp.dim_partid_fpp_1=dp.dim_partid
/*and dp.ProductFamilyDescription_Merck='6075 Rotavec Corona'*/) t
on t.fact_mmprodhierarchyid=f_mmp.fact_mmprodhierarchyid
when matched then update set f_mmp.ct_overallsupplyhealthscore=t.overallsupplyhealthscore;

/* Avoid table recreation */
/* drop table if exists fact_mmprodhierarchy
rename table fact_scm_test to fact_mmprodhierarchy */	

truncate table fact_mmprodhierarchy;
insert into fact_mmprodhierarchy
select * from fact_scm_test;
