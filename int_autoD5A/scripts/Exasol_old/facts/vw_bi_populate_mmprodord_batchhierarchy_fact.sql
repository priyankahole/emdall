/* Lokesh - Initial Version - 29 Oct 2015 */
/* Lokesh - v2 - 17 Nov 2015 */
/* Lokesh - v3 - 25 Nov 2015 */

/* Get prod order start date and finish date and store it in a tmp table */
/* Note that each prod order has only 1 row in prod order. So no need to do check for min/max date */
DROP TABLE IF EXISTS tmp_prodorderstartdate;
CREATE TABLE tmp_prodorderstartdate
as
select DISTINCT f.dd_ordernumber,
Dim_DateidBasicStart Dim_DateidBasicStart_prodorder,
Dim_DateidActualHeaderFinishDate_merck Dim_DateidActualHeaderFinishDate_merck_prodorder
FROM fact_productionorder f;

/* First get the parent batch,productionord,plant,part,matdoc - Movement type = 101 */
DROP TABLE IF EXISTS tmp_factmmbatchhier_parent;
CREATE TABLE tmp_factmmbatchhier_parent
AS
SELECT DISTINCT  f.dd_batchnumber,f.dd_productionordernumber, t.Dim_DateidBasicStart_prodorder, t.Dim_DateidActualHeaderFinishDate_merck_prodorder,
f.dim_plantid,f.dim_partid,f.dd_materialdocno,f.ct_quantity
FROM fact_materialmovement f,dim_movementtype mt, tmp_prodorderstartdate t
WHERE f.dim_movementtypeid = mt.dim_movementtypeid AND f.dd_productionordernumber = t.dd_ordernumber
AND mt.movementtype = '101' AND mt.movementindicator = 'F'
AND f.dd_batchnumber <> 'Not Set' AND f.dd_materialdocno <> 'Not Set' AND f.dd_productionordernumber <> 'Not Set'
AND f.dim_partid <> 1 AND f.dim_plantid <> 1;
/*AND mt.movementtype in ( 101,102, 261, 262, 601, 641 )*/

/* Child batches - movement type = 261 */
DROP TABLE IF EXISTS tmp_factmmbatchhier_child;
CREATE TABLE tmp_factmmbatchhier_child
AS
SELECT DISTINCT  f.dd_batchnumber,f.dd_productionordernumber,t.Dim_DateidBasicStart_prodorder,t.Dim_DateidActualHeaderFinishDate_merck_prodorder,
f.dim_plantid,f.dim_partid,f.dd_materialdocno,f.ct_quantity
FROM fact_materialmovement f,dim_movementtype mt,tmp_prodorderstartdate t
WHERE f.dim_movementtypeid = mt.dim_movementtypeid AND f.dd_productionordernumber = t.dd_ordernumber
AND mt.movementtype = '261'
AND f.dd_batchnumber <> 'Not Set' AND f.dd_materialdocno <> 'Not Set' AND f.dd_productionordernumber <> 'Not Set'
AND f.dim_partid <> 1 AND f.dim_plantid <> 1;

/* Initially populate everything from root parent batch */
DROP TABLE IF EXISTS tmp_fact_mmprodord_batchhierarchy;
CREATE TABLE tmp_fact_mmprodord_batchhierarchy
AS
SELECT DISTINCT f.dd_batchnumber,f.dd_productionordernumber,f.Dim_DateidBasicStart_prodorder,f.Dim_DateidActualHeaderFinishDate_merck_prodorder,
f.dim_plantid,f.dim_partid,f.dd_materialdocno,f.ct_quantity,
f.dd_batchnumber dd_batchnumber_component,	/* For Level 0, make the component batch point to itself */
convert(varchar(12),'Not Set') dd_productionordernumber_component,
cast(1 as int) Dim_DateidBasicStart_prodorder_component,
cast(1 as int) Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
cast(0 as int) dd_hierarchylevel_component,f.dim_plantid dim_plantid_component,
cast(1 as int) dim_partid_component,convert(varchar(10),'Not Set') dd_materialdocno_component,convert(decimal (18,4),0) ct_quantity_component
FROM tmp_factmmbatchhier_parent f;

/* Insert Level 1 component */
/* Take the parent row(batchno,prodordno,plantid,partid,matdocno,qty). These are the 101 movements.
Now get all 261 movements and join with production order and plant of parent */

INSERT INTO tmp_fact_mmprodord_batchhierarchy
(dd_batchnumber,dd_productionordernumber,Dim_DateidBasicStart_prodorder,Dim_DateidActualHeaderFinishDate_merck_prodorder,dim_plantid,dim_partid,dd_materialdocno,ct_quantity,
dd_batchnumber_component,dd_productionordernumber_component,
Dim_DateidBasicStart_prodorder_component,Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
dim_plantid_component,dim_partid_component,dd_materialdocno_component,ct_quantity_component,
dd_hierarchylevel_component)
SELECT f.dd_batchnumber,f.dd_productionordernumber,f.Dim_DateidBasicStart_prodorder,f.Dim_DateidActualHeaderFinishDate_merck_prodorder,
f.dim_plantid,f.dim_partid,f.dd_materialdocno,f.ct_quantity,
f1.dd_batchnumber dd_batchnumber_component,
f1.dd_productionordernumber dd_productionordernumber_component,
f1.Dim_DateidBasicStart_prodorder Dim_DateidBasicStart_prodorder_component,
f1.Dim_DateidActualHeaderFinishDate_merck_prodorder Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
f1.dim_plantid dim_plantid_component ,
f1.dim_partid dim_partid_component,
f1.dd_materialdocno dd_materialdocno_component,
f1.ct_quantity ct_quantity_component,
1 dd_hierarchylevel_component
FROM tmp_fact_mmprodord_batchhierarchy f, tmp_factmmbatchhier_child f1
WHERE f.dd_productionordernumber = f1.dd_productionordernumber
AND f.dim_plantid = f1.dim_plantid;

/* Insert Level 2 component */

DROP TABLE IF EXISTS tmp_fact_mmprodord_batchhierarchy_l2;
CREATE TABLE tmp_fact_mmprodord_batchhierarchy_l2
AS
SELECT DISTINCT f.dd_batchnumber_component dd_batchnumber,
f.dd_productionordernumber_component dd_productionordernumber,
f.Dim_DateidBasicStart_prodorder_component Dim_DateidBasicStart_prodorder,
f.Dim_DateidActualHeaderFinishDate_merck_prodorder_component Dim_DateidActualHeaderFinishDate_merck_prodorder,
f.dim_plantid_component dim_plantid,
f.dim_partid_component dim_partid,
f.dd_materialdocno_component dd_materialdocno,
f.ct_quantity_component ct_quantity,
f.dd_batchnumber_component dd_batchnumber_component,      /* For Level 0, make the component batch point to itself */
convert(varchar(12),'Not Set') dd_productionordernumber_component,
cast(0 as int) dd_hierarchylevel_component,f.dim_plantid dim_plantid_component,
cast(1 as int) dim_partid_component,convert(varchar(10),'Not Set') dd_materialdocno_component,convert(decimal (18,4),0) ct_quantity_component
FROM tmp_fact_mmprodord_batchhierarchy f
WHERE dd_hierarchylevel_component = 1
AND EXISTS ( SELECT 1 FROM tmp_factmmbatchhier_parent p WHERE f.dd_batchnumber_component = p.dd_batchnumber AND f.dd_productionordernumber_component = p.dd_productionordernumber
AND f.dim_plantid_component = p.dim_plantid AND f.dim_partid_component = p.dim_partid) ;


/* All 101 movements already have the level 1 child. Now, we just need to check the component's 101 movement and use its child to populate level 2 */
INSERT INTO tmp_fact_mmprodord_batchhierarchy
(dd_batchnumber,dd_productionordernumber,Dim_DateidBasicStart_prodorder,Dim_DateidActualHeaderFinishDate_merck_prodorder,dim_plantid,dim_partid,dd_materialdocno,ct_quantity,
dd_batchnumber_component,dd_productionordernumber_component,
Dim_DateidBasicStart_prodorder_component,
Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
dim_plantid_component,dim_partid_component,dd_materialdocno_component,ct_quantity_component,
dd_hierarchylevel_component)
SELECT DISTINCT f.dd_batchnumber,f.dd_productionordernumber, f.Dim_DateidBasicStart_prodorder,f.Dim_DateidActualHeaderFinishDate_merck_prodorder,
f.dim_plantid,f.dim_partid,f.dd_materialdocno,f.ct_quantity,
f1.dd_batchnumber_component,f1.dd_productionordernumber_component,
f1.Dim_DateidBasicStart_prodorder_component,
f1.Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
f1.dim_plantid_component,f1.dim_partid_component,f1.dd_materialdocno_component,f1.ct_quantity_component,
2
FROM tmp_fact_mmprodord_batchhierarchy f, tmp_fact_mmprodord_batchhierarchy f1
WHERE f.dd_hierarchylevel_component = 1 AND f1.dd_hierarchylevel_component = 1
AND f.dd_batchnumber_component = f1.dd_batchnumber AND f.dim_partid_component = f1.dim_partid AND f.dim_plantid = f1.dim_plantid;
/*AND f.dd_batchnumber IN ( '31017D0','31017')*/


/* Insert Level 3 component */
INSERT INTO tmp_fact_mmprodord_batchhierarchy
(dd_batchnumber,dd_productionordernumber,Dim_DateidBasicStart_prodorder,Dim_DateidActualHeaderFinishDate_merck_prodorder,dim_plantid,dim_partid,dd_materialdocno,ct_quantity,
dd_batchnumber_component,dd_productionordernumber_component,
Dim_DateidBasicStart_prodorder_component,
Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
dim_plantid_component,dim_partid_component,dd_materialdocno_component,ct_quantity_component,
dd_hierarchylevel_component)
SELECT DISTINCT f.dd_batchnumber,f.dd_productionordernumber, f.Dim_DateidBasicStart_prodorder,f.Dim_DateidActualHeaderFinishDate_merck_prodorder,
f.dim_plantid,f.dim_partid,f.dd_materialdocno,f.ct_quantity,
f1.dd_batchnumber_component,f1.dd_productionordernumber_component,
f1.Dim_DateidBasicStart_prodorder_component,
f1.Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
f1.dim_plantid_component,f1.dim_partid_component,f1.dd_materialdocno_component,f1.ct_quantity_component,
3
FROM tmp_fact_mmprodord_batchhierarchy f, tmp_fact_mmprodord_batchhierarchy f1
WHERE f.dd_hierarchylevel_component = 2 AND f1.dd_hierarchylevel_component = 1
AND f.dd_batchnumber_component = f1.dd_batchnumber AND f.dim_plantid = f1.dim_plantid AND f.dim_partid_component = f1.dim_partid;

/* Insert Level 4 component */
INSERT INTO tmp_fact_mmprodord_batchhierarchy
(dd_batchnumber,dd_productionordernumber,Dim_DateidBasicStart_prodorder,Dim_DateidActualHeaderFinishDate_merck_prodorder,dim_plantid,dim_partid,dd_materialdocno,ct_quantity,
dd_batchnumber_component,dd_productionordernumber_component,
Dim_DateidBasicStart_prodorder_component,
Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
dim_plantid_component,dim_partid_component,dd_materialdocno_component,ct_quantity_component,
dd_hierarchylevel_component)
SELECT DISTINCT f.dd_batchnumber,f.dd_productionordernumber, f.Dim_DateidBasicStart_prodorder,f.Dim_DateidActualHeaderFinishDate_merck_prodorder,
f.dim_plantid,f.dim_partid,f.dd_materialdocno,f.ct_quantity,
f1.dd_batchnumber_component,f1.dd_productionordernumber_component,
f1.Dim_DateidBasicStart_prodorder_component,
f1.Dim_DateidActualHeaderFinishDate_merck_prodorder_component,
f1.dim_plantid_component,f1.dim_partid_component,f1.dd_materialdocno_component,f1.ct_quantity_component,
4
FROM tmp_fact_mmprodord_batchhierarchy f, tmp_fact_mmprodord_batchhierarchy f1
WHERE f.dd_hierarchylevel_component = 3 AND f1.dd_hierarchylevel_component = 1
AND f.dd_batchnumber_component = f1.dd_batchnumber AND f.dim_plantid = f1.dim_plantid AND f.dim_partid_component = f1.dim_partid;

/* LK : 0 rows are populated beyond these as there are no more 101 movements for components below this level */

DROP TABLE IF EXISTS fact_mmprodord_batchhierarchy;
CREATE TABLE fact_mmprodord_batchhierarchy
AS
SELECT row_number() over(order by '') fact_mmprodord_batchhierarchyid,t.*
FROM tmp_fact_mmprodord_batchhierarchy t;