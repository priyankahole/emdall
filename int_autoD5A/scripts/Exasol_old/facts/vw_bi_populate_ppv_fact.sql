/*##################################################################################################################

  Script         : vw_bi_populate_ppv_fact.sql
  Author         : Alin
  Created On     : 03 Oct 2017
  Description    : Script for populating  PPV Subject Area
  Grain          : Ref doc no, Cost element, plant code, posting_row
         
##################################################################################################################*/


delete from number_fountain m where m.table_name = 'fact_ppv';

insert into number_fountain
select  'fact_ppv',
        ifnull(max(d.fact_ppvid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_ppv d
where d.fact_ppvid <> 1;


--INSERT NEW ROWS

insert into fact_ppv (
FACT_PPVID,   
DD_REFDOCNUMBER,
DIM_PARTID,
DD_TRADINGPARTNER,
DD_PERIOD,
dd_plantcode,
dd_posting_row,
DIM_PLANTID,
dd_documentno,
DD_COSTELEMENT,
DD_COSTELEMENTNAME,
DD_COSTELEMENTDESCR,
DD_PURCHASEORDERTEXT,
DD_PURCHASINGDOC,
DD_OFFSETTINGACCOUNTTYPE,
DD_OFFSETTINGACCOUNTNO,
DD_NAMEOF_OFFSETTINGACC,
DIM_COSTCENTERID,
DD_USERNAME,
DD_TOPERIOD,
DD_OBJCURRENCY,
CT_VALUEINOBJCURR,
DIM_DATEIDCREATEDON,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCUMENTDATE,
CT_VALOCOAREA_CURRENCY,
CT_COMONTHAVERAGE,
CT_VALINUSDCOEXCH,
DD_EXCH_RATE_TYPE,
CT_TOTAL_QTY,
DD_BPCGEOORG,
DD_CO_AREA_CURRENCY,
dd_spend_cat,--(Calculated Field)
dw_insert_date
)
SELECT (select max_id   from number_fountain   where table_name = 'fact_ppv') + row_number() over(order by '') AS FACT_PPVID,    
ifnull(cc.COBK_REFBN, 'Not Set') as DD_REFDOCNUMBER,
1 AS DIM_PARTID, 
ifnull(cc.COEP_VBUND, 'Not Set') as DD_TRADINGPARTNER,
ifnull(cc.COEP_PERIO, 0) as DD_PERIOD,
ifnull(cc.COEP_WERKS, 'Not Set') as dd_plantcode,
ifnull(cc.COEP_BUZEI, 0) as dd_posting_row,
1 AS DIM_PLANTID, --needs update

ifnull(cc.COEP_BELNR, 'Not Set') AS dd_documentno,

ifnull(cc.COEP_KSTAR, 'Not Set') as DD_COSTELEMENT,
ifnull(cu.CSKU_KTEXT, 'Not Set') as DD_COSTELEMENTNAME,
ifnull(cu.CSKU_LTEXT, 'Not Set') as DD_COSTELEMENTDESCR,
'Not Set' as DD_PURCHASEORDERTEXT, --needs update
ifnull(cc.COEP_EBELN, 'Not Set') as DD_PURCHASINGDOC,
ifnull(cc.COEP_GKOAR, 'Not Set') as DD_OFFSETTINGACCOUNTTYPE,
ifnull(cc.COEP_GKONT, 'Not Set') as DD_OFFSETTINGACCOUNTNO,
'Not Set' as DD_NAMEOF_OFFSETTINGACC,--needs update
1 as DIM_COSTCENTERID,--needs update --de mapat din nou
ifnull(ck.COBK_USNAM, 'Not Set') as DD_USERNAME,
ifnull(ck.COBK_PERBI, 0) AS DD_TOPERIOD,
ifnull(cc.COEP_OWAER, 'Not Set') as DD_OBJCURRENCY,
ifnull(cc.COEP_WOGBTR, 0) AS CT_VALUEINOBJCURR,
1 AS DIM_DATEIDCREATEDON,--needs update
1 AS DIM_DATEIDPOSTINGDATE,--needs update
1 AS DIM_DATEIDDOCUMENTDATE,--needs update
ifnull(cc.COEP_WKGBTR,0) as CT_VALOCOAREA_CURRENCY,
0 as CT_COMONTHAVERAGE,--needs update
0 as CT_VALINUSDCOEXCH,--needs update
ifnull(ck.COBK_KURST, 'Not Set') as DD_EXCH_RATE_TYPE,
ifnull(cc.COEP_MEGBTR,0) as CT_TOTAL_QTY,
'Not Set' as DD_BPCGEOORG,--needs update
ifnull(ck.COBK_KWAER, 'Not Set') as DD_CO_AREA_CURRENCY,
'Not Set' as dd_spend_cat,--(Calculated Field)
current_timestamp

from COEP cc
INNER join CSKU cu
on cc.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on cc.COEP_KOKRS = ck.COBK_KOKRS 
and cc.COEP_BELNR = ck.COBK_BELNR

where not exists (select 1 from fact_ppv f
                     where DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
                       AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
                       AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
             AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
             AND dd_period = ifnull(cc.COEP_PERIO, 0)
            and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
            and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
/*
DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
*/


); 

--dim_partid
merge into fact_ppv f
using(
select distinct fact_ppvid, dp.dim_partid
from fact_ppv f, dim_part dp, COEP c
WHERE c.COEP_MATNR = dp.partnumber
and c.COEP_WERKS = dp.plant
and DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
)t
on t.fact_ppvid = f.fact_ppvid
when matched then update set
f.dim_partid = t.dim_partid
where f.dim_partid <> t.dim_partid;

--dim_plantid
merge into fact_ppv f
using(
select distinct fact_ppvid, d.dim_plantid
from fact_ppv f, dim_part dp, COEP c, dim_plant d
WHERE c.COEP_MATNR = dp.partnumber
and DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and d.plantcode = dd_plantcode
)t
on t.fact_ppvid = f.fact_ppvid
when matched then update set
f.dim_plantid = t.dim_plantid
where f.dim_plantid <> t.dim_plantid;

--dim_dates


merge into fact_ppv f
using(
select distinct fact_ppvid, 
d.dim_dateid as dim_DATEIDCREATEDON
from fact_ppv f, dim_part dp, COEP c, COBK ck, DIM_DATE D
WHERE ifnull(c.COEP_MATNR, 'Not Set') = dp.partnumber
and DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and  c.COEP_KOKRS = ck.COBK_KOKRS 
and c.COEP_BELNR = ck.COBK_BELNR
AND d.datevalue = ifnull(ck.COBK_CPUDT, '0001-01-01')
and d.plantcode_FACTORY = dd_plantcode
--and d.plantcode_factory = dp.plant
and d.companycode = ifnull(c.COEP_BUKRS, 'Not Set')
)t
on t.fact_ppvid = f.fact_ppvid
when matched then update set
f.dim_DATEIDCREATEDON = t.dim_DATEIDCREATEDON
where f.dim_DATEIDCREATEDON <> t.dim_DATEIDCREATEDON;

merge into fact_ppv f
using(
select distinct fact_ppvid, 
d.dim_dateid as DIM_DATEIDPOSTINGDATE
from fact_ppv f, dim_part dp, COEP c, COBK ck, DIM_DATE D
WHERE ifnull(c.COEP_MATNR, 'Not Set') = dp.partnumber
and DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and  c.COEP_KOKRS = ck.COBK_KOKRS 
and c.COEP_BELNR = ck.COBK_BELNR
AND d.datevalue = ifnull(ck.COBK_BUDAT, '0001-01-01')
and d.plantcode_FACTORY = dd_plantcode
--and d.plantcode_factory = dp.plant
and d.companycode = ifnull(c.COEP_BUKRS, 'Not Set')
)t
on t.fact_ppvid = f.fact_ppvid
when matched then update set
f.DIM_DATEIDPOSTINGDATE = t.DIM_DATEIDPOSTINGDATE
where f.DIM_DATEIDPOSTINGDATE <> t.DIM_DATEIDPOSTINGDATE;

merge into fact_ppv f
using(
select distinct fact_ppvid, 
d.dim_dateid as DIM_DATEIDDOCUMENTDATE
from fact_ppv f, dim_part dp, COEP c, COBK ck, DIM_DATE D
WHERE ifnull(c.COEP_MATNR, 'Not Set') = dp.partnumber
and DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and  c.COEP_KOKRS = ck.COBK_KOKRS 
and c.COEP_BELNR = ck.COBK_BELNR
AND d.datevalue = ifnull(ck.COBK_BLDAT, '0001-01-01')
and d.plantcode_FACTORY = dd_plantcode
--and d.plantcode_factory = dp.plant
and d.companycode = ifnull(c.COEP_BUKRS, 'Not Set')
)t
on t.fact_ppvid = f.fact_ppvid
when matched then update set
f.DIM_DATEIDDOCUMENTDATE = t.DIM_DATEIDDOCUMENTDATE
where f.DIM_DATEIDDOCUMENTDATE <> t.DIM_DATEIDDOCUMENTDATE;


--dim_costcenter

merge into fact_ppv f
using(
select distinct fact_ppvid, max(cce.dim_costcenterid) as dim_costcenterid
FROM dim_costcenter cce, fact_ppv f, CSKS c, COEP cc, COBK ck
where DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
AND  cce.code = ifnull(C.CSKS_KOSTL, 'Not Set')
AND ifnull(c.CSKS_OBJNR, 'Not Set') = ifnull(CC.COEP_OBJNR, 'Not Set')
AND c.CSKS_DATBI = CCE.VALIDTO
AND c.CSKS_DATBI = '9999-12-31'
AND cc.COEP_KOKRS = ck.COBK_KOKRS 
AND cc.COEP_BELNR = ck.COBK_BELNR
group by fact_ppvid
)t
on t.fact_ppvid = f.fact_ppvid
when matched then update set
f.dim_costcenterid = t.dim_costcenterid
where f.dim_costcenterid <> t.dim_costcenterid;


--CO Month Average
drop table if exists tmp_curr_comavg;
create table tmp_curr_comavg as 
select distinct 
t.tcurr_fcurr, t.tcurr_tcurr, --tcurr_date, --t.tcurr_ukurs,
case when t.month_period like '0%' then substr(month_period,2,1) else month_period end as month_period,
FIRST_VALUE(t.tcurr_ukurs) over (partition by t.tcurr_fcurr, t.tcurr_tcurr, T.MONTH_PERIOD ORDER BY  t.TCURR_date DESC) --order by t.TCURR_date desc) 
as tcurr_ukurs
from
(
select distinct TCURR_KURST, TCURR_FCURR, TCURR_TCURR,substr((99999999 - tcurr_gdatu),5,2) AS MONTH_PERIOD,
concat(substr((99999999 - tcurr_gdatu),0,4),'-',substr((99999999 - tcurr_gdatu),5,2),'-',substr((99999999 - tcurr_gdatu),7,2)) as tcurr_date,
TCURR_FFACT, TCURR_TFACT, CASE WHEN TCURR_UKURS > 0 THEN TCURR_UKURS ELSE -1 * TCURR_UKURS END as TCURR_UKURS
FROM TCURR_ALLTYPES
where TCURR_KURST = 'M'
)t;


merge into fact_ppv f
using (
SELECT distinct fact_ppvid,tcurr_fcurr, tcurr_tcurr, tcurr_ukurs
FROM tmp_curr_comavg t, COBK ck, COEP cp, fact_ppv f
where t.TCURR_FCURR = 'USD' 
and 
ck.COBK_KWAER = T.tcurr_tcurr
and cp.COEP_KOKRS = ck.COBK_KOKRS 
and cp.COEP_BELNR = ck.COBK_BELNR
and DD_REFDOCNUMBER = ifnull(cp.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cp.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cp.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cp.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cp.COEP_PERIO, 0)
and dd_documentno = ifnull(cp.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cp.COEP_EBELN, 'Not Set')

and dd_period = month_period
) t
on t.fact_ppvid = f.fact_ppvid
when matched then update set
f.CT_COMONTHAVERAGE = t.tcurr_ukurs,
dw_update_date = current_timestamp;

drop table if exists tmp_curr_comavg;

--DD_PURCHASEORDERTEXT
merge into fact_ppv f
using
(select distinct fact_ppvid,  IFNULL(MAKT_MAKTX, 'Not Set') as DD_PURCHASEORDERTEXT
from fact_ppv f, dim_part dp, COEP c, MAKT m
WHERE c.COEP_MATNR = dp.partnumber
and dp.PartNumber = m.MAKT_MATNR
and c.COEP_WERKS = dp.plant
and DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and m.MAKT_SPRAS = 'E')t
on t.fact_ppvid = f.fact_ppvid
when matched then update
set f.DD_PURCHASEORDERTEXT = t.DD_PURCHASEORDERTEXT
where f.DD_PURCHASEORDERTEXT <> t.DD_PURCHASEORDERTEXT;


--DD_NAMEOF_OFFSETTINGACC
/* 22 Oct 2018 Georgiana changes, adding join with CSKU on KTOPL in order to avoid unable to get a stable source of rows*/
drop table if exists tmp_coep_lfa1_skat_kna1;
create table tmp_coep_lfa1_skat_kna1 as
select distinct c.COBK_REFBN, c.COEP_KSTAR, c.COEP_WERKS, c.COEP_BUZEI, c.COEP_PERIO,
c.COEP_BELNR, c.COEP_EBELN,

case when COEP_GKOAR = 'K' THEN NAME1
WHEN COEP_GKOAR IN ('S', 'M') THEN SKAT_TXT20
WHEN COEP_GKOAR = 'D' THEN KNA1_NAME1 

END AS NAME_OF_OFFSETTING_ACCOUNT
from COEP C
INNER JOIN CSKU cu on c.COEP_KSTAR = cu.CSKU_KSTAR
LEFT JOIN LFA1 L
on  L.LIFNR = C.COEP_GKONT
LEFT JOIN SKAT S
on S.SKAT_SAKNR = C.COEP_GKONT  and s.SKAT_KTOPL = cu.CSKU_KTOPL
LEFT JOIN KNA1 K
ON K.KNA1_KUNNR = C.COEP_GKONT
WHERE SKAT_SPRAS = 'E';


update fact_ppv f
set f.DD_NAMEOF_OFFSETTINGACC = t.NAME_OF_OFFSETTING_ACCOUNT
from fact_ppv f, tmp_coep_lfa1_skat_kna1 t
where  DD_REFDOCNUMBER = ifnull(t.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(t.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(t.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(t.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(t.COEP_PERIO, 0)
and dd_documentno = ifnull(t.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(t.COEP_EBELN, 'Not Set')
and f.DD_NAMEOF_OFFSETTINGACC <> t.NAME_OF_OFFSETTING_ACCOUNT;


update fact_ppv f
  set DD_USERNAME = ifnull(ck.COBK_USNAM, 'Not Set')
from COEP cc
INNER join CSKU cu
on cc.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on cc.COEP_KOKRS = ck.COBK_KOKRS 
and cc.COEP_BELNR = ck.COBK_BELNR
inner join fact_ppv f
on DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
where DD_USERNAME <> ifnull(ck.COBK_USNAM, 'Not Set');

update fact_ppv f
  set DD_TOPERIOD = ifnull(ck.COBK_PERBI, 0)
from COEP cc
INNER join CSKU cu
on cc.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on cc.COEP_KOKRS = ck.COBK_KOKRS 
and cc.COEP_BELNR = ck.COBK_BELNR
inner join fact_ppv f
on DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
where DD_TOPERIOD <> ifnull(ck.COBK_PERBI, 0);

update fact_ppv f
  set DD_OBJCURRENCY = ifnull(cc.COEP_OWAER, 'Not Set')
from COEP cc
INNER join CSKU cu
on cc.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on cc.COEP_KOKRS = ck.COBK_KOKRS 
and cc.COEP_BELNR = ck.COBK_BELNR
inner join fact_ppv f
on DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
where DD_OBJCURRENCY <> ifnull(cc.COEP_OWAER, 'Not Set');

update fact_ppv f
  set CT_VALUEINOBJCURR = ifnull(cc.COEP_WOGBTR, 0)
from COEP cc
INNER join CSKU cu
on cc.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on cc.COEP_KOKRS = ck.COBK_KOKRS 
and cc.COEP_BELNR = ck.COBK_BELNR
inner join fact_ppv f
on DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
where CT_VALUEINOBJCURR <> ifnull(cc.COEP_WOGBTR, 0);

update fact_ppv f
  set CT_VALOCOAREA_CURRENCY = ifnull(cc.COEP_WKGBTR,0)
from COEP cc
INNER join CSKU cu
on cc.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on cc.COEP_KOKRS = ck.COBK_KOKRS 
and cc.COEP_BELNR = ck.COBK_BELNR
inner join fact_ppv f
on DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
where CT_VALOCOAREA_CURRENCY <> ifnull(cc.COEP_WKGBTR,0);

update fact_ppv f
  set DD_EXCH_RATE_TYPE = ifnull(ck.COBK_KURST, 'Not Set')
from COEP cc
INNER join CSKU cu
on cc.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on cc.COEP_KOKRS = ck.COBK_KOKRS 
and cc.COEP_BELNR = ck.COBK_BELNR
inner join fact_ppv f
on DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
where DD_EXCH_RATE_TYPE <> ifnull(ck.COBK_KURST, 'Not Set');

update fact_ppv f
  set DD_CO_AREA_CURRENCY = ifnull(ck.COBK_KWAER, 'Not Set')
from COEP cc
INNER join CSKU cu
on cc.COEP_KSTAR = cu.CSKU_KSTAR
INNER join COBK ck
on cc.COEP_KOKRS = ck.COBK_KOKRS 
and cc.COEP_BELNR = ck.COBK_BELNR
inner join fact_ppv f
on DD_REFDOCNUMBER = ifnull(cc.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(cc.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(cc.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(cc.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(cc.COEP_PERIO, 0)
and dd_documentno = ifnull(cc.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(cc.COEP_EBELN, 'Not Set')
where DD_CO_AREA_CURRENCY <> ifnull(ck.COBK_KWAER, 'Not Set');


/*APP-9339 alin 11 apr 2018 */
update fact_ppv f
set dd_gpc = (
CASE WHEN TRIM(LEADING '0' FROM csc.Code) = '100290' THEN 'P1593.0 - Millsboro, DE' 
WHEN TRIM(LEADING '0' FROM csc.Code) = '100291' THEN 'P1591.0 - De Soto, KS' 
WHEN TRIM(LEADING '0' FROM csc.Code) = '100292' THEN 'P1594.0 - Worthington, MN'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100293' THEN 'P1592.0 - Elkhorn, NE'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100294' THEN 'P_US03.0 - Baton Rouge, LA'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100296' THEN 'P_US_OTHER - Other US Locations'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100298' THEN 'P_USIA_AM - Ames, IA'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100380' THEN 'P1593.0 - Millsboro, DE'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100381' THEN 'P1591.0 - De Soto, KS'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100382' THEN 'P1594.0 - Worthington, MN'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100383' THEN 'P1592.0 - Elkhorn, NE'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100384' THEN 'P_US03.0 - Baton Rouge, LA'
WHEN TRIM(LEADING '0' FROM csc.Code) = '100386' THEN 'P_US_OTHER - Other US Locations'
WHEN TRIM(LEADING '0' FROM csc.Code) = '106980' THEN 'P_US_OTHER - Other US Locations'
WHEN TRIM(LEADING '0' FROM csc.Code) = '147810' THEN 'P_AT_OTH - Austria Other'
WHEN TRIM(LEADING '0' FROM csc.Code) = '407101' THEN 'P_AU01.0 - Bendigo, Australia'
WHEN TRIM(LEADING '0' FROM csc.Code) = '402954' THEN 'P_AU01.0 - Bendigo, Australia'
WHEN TRIM(LEADING '0' FROM csc.Code) = '251157' THEN 'P_NL01.0 - Boxmeer, Netherlands'
WHEN TRIM(LEADING '0' FROM csc.Code) = '251222' THEN 'P_NL01.0 - Boxmeer, Netherlands'
WHEN TRIM(LEADING '0' FROM csc.Code) = '337001' THEN 'P_DE01.0 - Burgwedel, Germany'
WHEN TRIM(LEADING '0' FROM csc.Code) = '347001' THEN 'P_DE02.0 - Friesoythe, Germany'
WHEN TRIM(LEADING '0' FROM csc.Code) = '176203' THEN 'P1598.0 - Salamanca, Spain'
WHEN TRIM(LEADING '0' FROM csc.Code) = '172604' THEN 'P1598.0 - Salamanca, Spain'
WHEN TRIM(LEADING '0' FROM csc.Code) = '422900' THEN 'P1599.0 - Santiago, Mexico'
WHEN TRIM(LEADING '0' FROM csc.Code) = '427001' THEN 'P1599.0 - Santiago, Mexico'
WHEN TRIM(LEADING '0' FROM csc.Code) = '493401' THEN 'P_FR09.0 - Segre, France'
WHEN TRIM(LEADING '0' FROM csc.Code) = '209111' THEN 'P_GB_OTH - United Kingdom'
WHEN TRIM(LEADING '0' FROM csc.Code) = '209013' THEN 'P_GB_OTH - United Kingdom'
WHEN TRIM(LEADING '0' FROM csc.Code) = '443490' THEN 'P_NZ01.0 - Upper Hutt, New Zealand'
WHEN TRIM(LEADING '0' FROM csc.Code) = '443491' THEN 'P_NZ01.0 - Upper Hutt, New Zealand'
WHEN TRIM(LEADING '0' FROM csc.Code) = '117111' THEN 'P1590.0 - Unterschleissheim, Germany'
WHEN TRIM(LEADING '0' FROM csc.Code) = '427005' THEN 'P1600.0 - Santa Clara, Mexico'
WHEN TRIM(LEADING '0' FROM csc.Code) = '157110' THEN 'P_FR08.0 - Igoville, France'
WHEN TRIM(LEADING '0' FROM csc.Code) = '117201' THEN 'P_DE03.0 - Cologne, Germany'
WHEN TRIM(LEADING '0' FROM csc.Code) = '411150' THEN 'P_CAQC_PC - MMD Point Claire Locations'
WHEN TRIM(LEADING '0' FROM csc.Code) = '427055' THEN 'P1600.0 - Santa Clara, Mexico'
ELSE NULL END)
from fact_ppv f, dim_costcenter csc
where csc.dim_costcenterid = f.dim_costcenterid;



drop table if exists tmp_for_amt_val_co_exch_v2;
create table tmp_for_amt_val_co_exch_v2
as  
SELECT  distinct  
first_value(fact_ppvid) over (partition by DD_REFDOCNUMBER, F.dim_dateidpostingdate , dp.partnumber, DD_COSTELEMENT, dd_plantcode, dd_posting_row, dd_period, DD_PURCHASINGDOC, dd_documentno, 
dd_gpc,
d.datevalue) as fact_ppvid, 
DD_REFDOCNUMBER, F.dim_dateidpostingdate , dp.partnumber, DD_COSTELEMENT, dd_plantcode, dd_posting_row, dd_period, DD_PURCHASINGDOC, dd_documentno, 
dd_gpc,
d.datevalue,
avg(ct_comonthaverage*ct_valocoarea_currency) as media
FROM FACT_PPV F, DIM_DATE d, dim_part dp
WHERE F.dim_dateidpostingdate = d.dim_dateid
and dp.dim_partid = f.dim_partid
/*and d.datevalue >= '2018-01-01'
and dd_gpc = 'P_NL01.0 - Boxmeer, Netherlands'*/
group by   fact_ppvid, DD_REFDOCNUMBER, F.dim_dateidpostingdate , dp.partnumber, DD_COSTELEMENT, dd_plantcode, dd_posting_row, dd_period, DD_PURCHASINGDOC, dd_documentno,
dd_gpc, 
d.datevalue;

update fact_ppv
set amt_val_co_exch_v2 = 0;

update fact_ppv f
set amt_val_co_exch_v2 = ifnull(t.media, 0)
from fact_ppv f, tmp_for_amt_val_co_exch_v2 t
where t.fact_ppvid = f.fact_ppvid
and amt_val_co_exch_v2 <> ifnull(t.media, 0);

drop table if exists tmp_for_amt_val_co_exch_v2;

/*Alin APP-9478 2 May 2018 - currency enable*/
/*amt_exchangerates */
update fact_ppv f
set amt_exchangerate = z.exchangeRate
from fact_ppv f,  COEP c, tmp_getExchangeRate1 z, dim_date d
WHERE 
DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and z.pFromCurrency  = ifnull(COEP_OWAER, 'Not Set') 
and z.fact_script_name = 'bi_populate_ppv_fact'
and d.dim_dateid = f.DIM_DATEIDPOSTINGDATE
and z.pdate = d.datevalue
and z.pFromCurrency = z.pToCurrency
and amt_exchangerate <> z.exchangeRate;


drop table if exists tmp_gblcurr_ppv;
Create table tmp_gblcurr_ppv(
pGlobalCurrency varchar(3) null);

Insert into tmp_gblcurr_ppv(pGlobalCurrency) values(null);

Update tmp_gblcurr_ppv
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');


update fact_ppv f
set amt_exchangerate_gbl = z.exchangeRate
from fact_ppv f,  COEP c, tmp_getExchangeRate1 z, dim_date d, tmp_gblcurr_ppv
WHERE 
DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and z.pFromCurrency  = ifnull(COEP_OWAER, 'Not Set') 
and z.fact_script_name = 'bi_populate_ppv_fact'
and d.dim_dateid = f.DIM_DATEIDPOSTINGDATE
and z.pdate = d.datevalue
and z.pFromCurrency <> z.pToCurrency
and z.pToCurrency = tmp_gblcurr_ppv.pGlobalCurrency
and amt_exchangerate_gbl <> z.exchangeRate;


/* Update currency IDs */

update fact_ppv f
set f.dim_currencyid = cur.dim_currencyid
from fact_ppv f,  COEP c, dim_currency cur
WHERE 
DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and cur.currencycode = ifnull(COEP_OWAER, 'Not Set')
and f.dim_currencyid <> cur.dim_currencyid;


update fact_ppv f
set f.dim_CurrencyId_TRA = cur.dim_currencyid
from fact_ppv f,  COEP c,  dim_currency cur, dim_date d
WHERE 
DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
and cur.currencycode = ifnull(c.COEP_OWAER, 'Not Set') 
and d.dim_dateid = f.DIM_DATEIDPOSTINGDATE
and f.dim_CurrencyId_TRA <> cur.dim_currencyid;


update fact_ppv f
set f.dim_CurrencyId_GBL = ifnull(cur.dim_currencyid,1)  
from fact_ppv f,  COEP c, tmp_gblcurr_ppv, dim_currency cur
WHERE 
DD_REFDOCNUMBER = ifnull(c.COBK_REFBN, 'Not Set')
AND DD_COSTELEMENT = ifnull(c.COEP_KSTAR, 'Not Set')
AND dd_plantcode = ifnull(c.COEP_WERKS, 'Not Set')
AND dd_posting_row = ifnull(c.COEP_BUZEI, 'Not Set')
AND dd_period = ifnull(c.COEP_PERIO, 0)
and dd_documentno = ifnull(c.COEP_BELNR, 'Not Set')
and DD_PURCHASINGDOC = ifnull(c.COEP_EBELN, 'Not Set')
AND cur.currencycode = pGlobalCurrency
and f.dim_CurrencyId_GBL <> ifnull(cur.dim_currencyid,1);

/*update std_exchangerate_dateid*/
UPDATE fact_ppv f
SET f.std_exchangerate_dateid = f.dim_dateidpostingdate
WHERE  f.std_exchangerate_dateid <> f.dim_dateidpostingdate;


/*Alin 19 june 2018 APP-9478*/
drop table if exists tmp_for_true_value_in_usd;
create table tmp_for_true_value_in_usd
as  
SELECT  distinct  
first_value(fact_ppvid) over (partition by DD_REFDOCNUMBER, F.dim_dateidpostingdate , dp.partnumber, DD_COSTELEMENT, dd_plantcode, dd_posting_row, dd_period, DD_PURCHASINGDOC, dd_documentno, 
dd_gpc,
d.datevalue) as fact_ppvid, 
DD_REFDOCNUMBER, F.dim_dateidpostingdate , dp.partnumber, DD_COSTELEMENT, dd_plantcode, dd_posting_row, dd_period, DD_PURCHASINGDOC, dd_documentno, 
dd_gpc,
d.datevalue,
avg(ct_valueinobjcurr*amt_exchangerate_gbl) as media
FROM FACT_PPV F, DIM_DATE d, dim_part dp
WHERE F.dim_dateidpostingdate = d.dim_dateid
and dp.dim_partid = f.dim_partid
/*and d.datevalue >= '2018-01-01'
and dd_gpc = 'P_NL01.0 - Boxmeer, Netherlands'*/
group by   fact_ppvid, DD_REFDOCNUMBER, F.dim_dateidpostingdate , dp.partnumber, DD_COSTELEMENT, dd_plantcode, dd_posting_row, dd_period, DD_PURCHASINGDOC, dd_documentno,
dd_gpc, 
d.datevalue;

update fact_ppv
set ct_true_value_in_usd = 0;

update fact_ppv f
set ct_true_value_in_usd = ifnull(t.media, 0)
from fact_ppv f, tmp_for_true_value_in_usd t
where t.fact_ppvid = f.fact_ppvid
and ct_true_value_in_usd <> ifnull(t.media, 0);

drop table if exists tmp_for_true_value_in_usd;
