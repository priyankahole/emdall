/* Marius 14 jul 2015 moved this from App Manager to script */

drop table if exists tmp_billing_sales_distinct;
create table tmp_billing_sales_distinct
as
select dd_SalesDocNo
	,dd_SalesItemNo
	,max(Dim_DateidSchedDelivery) Dim_DateidSchedDelivery
	,max(Dim_DocumentCategoryid) Dim_DocumentCategoryid
	,max(amt_UnitPrice) amt_UnitPrice
	,max(Dim_CustomerID) Dim_CustomerID
	,max(dim_salesorderitemstatusid) dim_salesorderitemstatusid
	,max(Dim_CostCenterid) Dim_CostCenterid
	,max(Dim_SalesDocumentTypeid) Dim_SalesDocumentTypeid
	,max(Dim_DateidSalesOrderCreated) Dim_DateidSalesOrderCreated
	,max(Dim_SalesOrderRejectReasonid) Dim_SalesOrderRejectReasonid
	from fact_salesorder
group by dd_SalesDocNo,dd_SalesItemNo;

UPDATE fact_billing fb
   SET fb.Dim_CustomerId = fso.Dim_CustomerID
  from fact_billing fb,
       tmp_billing_sales_distinct fso
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
 AND fb.Dim_CustomerId <> fso.Dim_CustomerID;

UPDATE fact_billing fb
SET  fb.Dim_SalesDocumentItemStatusId = fso.dim_salesorderitemstatusid
	from fact_billing fb,
       tmp_billing_sales_distinct fso
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
 AND  fb.Dim_SalesDocumentItemStatusId <> fso.dim_salesorderitemstatusid;

UPDATE fact_billing fb
SET   fb.Dim_so_CostCenterid = fso.Dim_CostCenterid
from  fact_billing fb,
       tmp_billing_sales_distinct fso
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
 AND fb.Dim_so_CostCenterid <> fso.Dim_CostCenterid;

UPDATE fact_billing fb
SET   fb.Dim_so_SalesDocumentTypeid = fso.Dim_SalesDocumentTypeid
from fact_billing fb,
       tmp_billing_sales_distinct fso
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND  fb.Dim_so_SalesDocumentTypeid <> fso.Dim_SalesDocumentTypeid;



/*UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET   fb.Dim_so_DateidSchedDelivery = fso.Dim_DateidSchedDelivery
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
 AND fb.Dim_so_DateidSchedDelivery <> fso.Dim_DateidSchedDelivery */
 
UPDATE fact_billing fb
SET   fb.Dim_so_DateidSchedDelivery = fso.Dim_DateidSchedDelivery
from   fact_billing fb,
       tmp_billing_sales_distinct fso
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
 AND fb.Dim_so_DateidSchedDelivery <> fso.Dim_DateidSchedDelivery;

UPDATE fact_billing fb
SET    fb.Dim_so_SalesOrderCreatedId = fso.Dim_DateidSalesOrderCreated
from   fact_billing fb,
       tmp_billing_sales_distinct fso
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
 AND fb.Dim_so_SalesOrderCreatedId <> fso.Dim_DateidSalesOrderCreated;

drop table if exists tmp_billing_sales_updt;
create table tmp_billing_sales_updt
as
select f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,f_so.Dim_DocumentCategoryid,sum (f_so.ct_ConfirmedQty) ct_ConfirmedQty, SUM(f_so.ct_DeliveredQty) ct_DeliveredQty, SUM(f_so.amt_ScheduleTotal) amt_ScheduleTotal, SUM(f_so.ct_ScheduleQtySalesUnit) ct_ScheduleQtySalesUnit,
	SUM(f_so.ct_ConfirmedQty * f_so.amt_UnitPrice / (case when f_so.ct_PriceUnit=0 then null else f_so.ct_PriceUnit end)) amt_so_confirmed_fso
from fact_salesorder f_so group by f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,f_so.Dim_DocumentCategoryid;
 
/* UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET   fb.ct_so_ConfirmedQty = ( SELECT SUM(f_so.ct_ConfirmedQty) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1 */
	   
UPDATE fact_billing fb
SET   fb.ct_so_ConfirmedQty = ifnull(fso.ct_ConfirmedQty,0)
from   fact_billing fb,
       tmp_billing_sales_updt fso
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND ifnull(fb.ct_so_ConfirmedQty,-1) <> ifnull(fso.ct_ConfirmedQty,0);

UPDATE fact_billing fb
SET  fb.amt_so_UnitPrice = fso.amt_UnitPrice
from   fact_billing fb,
       tmp_billing_sales_distinct fso
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
 AND fb.amt_so_UnitPrice <> fso.amt_UnitPrice;


/*UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET  fb.ct_so_DeliveredQty = ( SELECT SUM(f_so.ct_DeliveredQty) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1 */

UPDATE fact_billing fb
SET   fb.ct_so_DeliveredQty = ifnull(fso.ct_DeliveredQty,0)
from   fact_billing fb,
       tmp_billing_sales_updt fso
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND ifnull(fb.ct_so_DeliveredQty,-1) <> ifnull(fso.ct_DeliveredQty,0);

UPDATE fact_billing fb
SET   fb.amt_so_Returned = 0
from   fact_billing fb,
       fact_salesorder fso
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
 and fb.amt_so_Returned <> 0;

/*UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET   fb.amt_so_Returned = ( SELECT SUM(f_so.amt_ScheduleTotal) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
 AND dc.DocumentCategory IN ('H', 'K') AND fso.Dim_SalesOrderRejectReasonid <> 1 */
 
UPDATE fact_billing fb
SET   fb.amt_so_Returned = ifnull(tmp.amt_ScheduleTotal,0)
from   fact_billing fb,
       fact_salesorder fso,
       Dim_documentcategory dc,
	   tmp_billing_sales_updt tmp
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND tmp.dd_salesdocno = fso.dd_SalesDocNo
	   AND tmp.dd_SalesItemNo = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
	AND dc.DocumentCategory IN ('H', 'K') AND fso.Dim_SalesOrderRejectReasonid <> 1
	AND ifnull(fb.amt_so_Returned,-1) <> ifnull(tmp.amt_ScheduleTotal,0);
 
/*DROP TABLE IF EXISTS tmp_SalesBillingQty
CREATE TABLE tmp_SalesBillingQty AS
SELECT fso.dd_SalesDocNo, fso.dd_SalesItemNo, sum(fso.ct_ScheduleQtySalesUnit) as TotalOrderQty,
Sum(fso.ct_DeliveredQty) as TotalDlvrQty from
       fact_salesorder fso
group by fso.dd_SalesDocNo, fso.dd_SalesItemNo

UPDATE fact_billing fb
from
       tmp_SalesBillingQty t
SET    fb.ct_so_OpenOrderQty = ifnull(t.TotalOrderQty - t.TotalDlvrQty,0)
 WHERE     fb.dd_salesdocno = t.dd_SalesDocNo
       AND fb.dd_salesitemno = t.dd_SalesItemNo 

DROP TABLE IF EXISTS tmp_SalesBillingQty */

UPDATE fact_billing fb
SET    fb.ct_so_OpenOrderQty = ifnull(t.ct_ScheduleQtySalesUnit - t.ct_DeliveredQty,0)
from   fact_billing fb,
       tmp_billing_sales_updt t
WHERE     fb.dd_salesdocno = t.dd_SalesDocNo
       AND fb.dd_salesitemno = t.dd_SalesItemNo AND ifnull(fb.ct_so_OpenOrderQty,0) <> ifnull(t.ct_ScheduleQtySalesUnit - t.ct_DeliveredQty,0);

UPDATE fact_billing fb
SET   fb.ct_so_ReturnedQty =0
from   fact_billing fb,
       tmp_billing_sales_distinct fso
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND ifnull(fb.ct_so_ReturnedQty,-1) <> 0;

/* UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET   fb.ct_so_ReturnedQty = (SELECT SUM(f_so.ct_ScheduleQtySalesUnit) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno )
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1 AND dc.DocumentCategory IN ('H', 'K')
	   AND fso.Dim_SalesOrderRejectReasonid <> 1 */
	   
UPDATE fact_billing fb
SET   fb.ct_so_ReturnedQty = ifnull(tmp.ct_ScheduleQtySalesUnit,0)
from   fact_billing fb,
       tmp_billing_sales_distinct fso,
       Dim_documentcategory dc,
	   tmp_billing_sales_updt tmp
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND tmp.dd_salesdocno = fso.dd_SalesDocNo
	   AND tmp.dd_SalesItemNo = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
	AND dc.DocumentCategory IN ('H', 'K') AND fso.Dim_SalesOrderRejectReasonid <> 1
	AND ifnull(fb.ct_so_ReturnedQty,-1) <> ifnull(tmp.ct_ScheduleQtySalesUnit,0);

/*UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET   fb.amt_so_ScheduleTotal = ( SELECT SUM(f_so.amt_ScheduleTotal) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1 */

UPDATE fact_billing fb
SET   fb.amt_so_ScheduleTotal = ifnull(fso.amt_ScheduleTotal,0)
from   fact_billing fb,
       tmp_billing_sales_updt fso
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND ifnull(fb.amt_so_ScheduleTotal,-1) <> ifnull(fso.amt_ScheduleTotal,0);

/*UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET fb.ct_so_ScheduleQtySalesUnit = ( SELECT SUM(f_so.ct_ScheduleQtySalesUnit) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1 */

UPDATE fact_billing fb
SET   fb.ct_so_ScheduleQtySalesUnit = ifnull(fso.ct_ScheduleQtySalesUnit,0)
from   fact_billing fb,
       tmp_billing_sales_updt fso
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND ifnull(fb.ct_so_ScheduleQtySalesUnit,-1) <> ifnull(fso.ct_ScheduleQtySalesUnit,0);

update fact_billing fb
set fb.amt_revenue = 0 
where fb.amt_revenue is null;

/*
UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET fb.amt_revenue =
          (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
              WHEN '5' THEN 0
              WHEN '6' THEN 0
              WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
              WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
              ELSE fb.amt_NetValueItem
           END)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1 */
	   
UPDATE fact_billing fb
SET fb.amt_revenue =
          (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
              WHEN '5' THEN 0
              WHEN '6' THEN 0
              WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
              WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
              ELSE fb.amt_NetValueItem
           END)
from   fact_billing fb,
       tmp_billing_sales_distinct fso,
       Dim_documentcategory dc
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
	   AND fb.amt_revenue <>
          (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
              WHEN '5' THEN 0
              WHEN '6' THEN 0
              WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
              WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
              ELSE fb.amt_NetValueItem
           END);

/* UPDATE fact_billing fb
from
       fact_salesorder fso,
       Dim_documentcategory dc
SET fb.amt_so_confirmed =
          (SELECT SUM(
                       f_so.ct_ConfirmedQty
                     * f_so.amt_UnitPrice
                     / (case when f_so.ct_PriceUnit=0 then null else f_so.ct_PriceUnit end))
             FROM fact_salesorder f_so
            WHERE f_so.dd_SalesDocNo = fb.dd_SalesDocNo
                  AND f_so.dd_salesitemno = fb.dd_salesitemno)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1 */
	   
UPDATE fact_billing fb
SET   fb.amt_so_confirmed = ifnull(fso.amt_so_confirmed_fso,0)
from   fact_billing fb,
       tmp_billing_sales_updt fso
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND ifnull(fb.amt_so_confirmed,-1) <> ifnull(fso.amt_so_confirmed_fso,0);

DROP TABLE IF EXISTS fact_salesorder_updexp;


CREATE TABLE fact_salesorder_updexp
AS
   SELECT dd_SalesDocNo,
          dd_salesitemno,
          (SUM(ct_ScheduleQtySalesUnit) - SUM(ct_DeliveredQty)) exp1
     FROM fact_salesorder
   GROUP BY dd_SalesDocNo, dd_salesitemno;

DROP TABLE IF EXISTS TMP_UPDT_AMT_SO_OPENORDER;
CREATE TABLE TMP_UPDT_AMT_SO_OPENORDER
AS
SELECT 
	f_so.dd_SalesDocNo,
	f_so.dd_salesitemno,
	max(e1.exp1 * f_so.amt_UnitPrice / (case when f_so.ct_PriceUnit=0 then null else f_so.ct_PriceUnit end)) updt_amt_so_openorder
FROM fact_salesorder f_so,fact_salesorder_updexp e1
WHERE e1.dd_SalesDocNo = f_so.dd_SalesDocNo
	AND e1.dd_salesitemno = f_so.dd_salesitemno
GROUP BY f_so.dd_SalesDocNo,
	f_so.dd_salesitemno;
   
UPDATE fact_billing fb
SET  fb.amt_so_openorder = fso.updt_amt_so_openorder
from   fact_billing fb,
       TMP_UPDT_AMT_SO_OPENORDER fso
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND ifnull(fb.amt_so_openorder,-1) <> fso.updt_amt_so_openorder;

DROP TABLE IF EXISTS TMP_UPDT_AMT_SO_OPENORDER;
DROP TABLE IF EXISTS fact_salesorder_updexp;
drop table if exists tmp_billing_sales_updt;
drop table if exists tmp_billing_sales_distinct;