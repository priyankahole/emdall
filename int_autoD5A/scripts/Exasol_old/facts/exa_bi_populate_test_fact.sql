/* Ignore it*/
insert into fact_test (dd_companydescription)
select ifnull(T001_BUTXT,'Not Set') as dd_companydescription from T001_test t
where not exists (select 1 from fact_test ft
where ifnull(t.T001_BUTXT,'Not Set') = ft.dd_companydescription);

update fact_test ft 
set ct_allocationpercurrency = CASE WHEN T001_WAERS = 'USD' THEN ifnull(T001_KOKFI,0) * 100 ELSE ifnull(T001_KOKFI,0) END
from fact_test ft 
	inner join T001_test t on ft.dd_companydescription = ifnull(t.T001_BUTXT,'Not Set')
where ct_allocationpercurrency <> CASE WHEN T001_WAERS = 'USD' THEN ifnull(T001_KOKFI,0) * 100 ELSE ifnull(T001_KOKFI,0) END;

update fact_test  ft
set ft.dim_companyid = dc.dim_companyid
from fact_test ft
		inner join T001_test t on ft.dd_companydescription = ifnull(t.T001_BUTXT,'Not Set')
		inner join dim_company dc on dc.companycode = t.T001_BUKRS
where ft.dim_companyid <> dc.dim_companyid;
