/**********************************************************************************/
/* 24 Sep 2015  2.4     LiviuT          BI-1122 Convert dd_OrderUnderdelTolerance and dd_OrderOverdelivTolerance to measures */
/* 26 Aug 2015  2.3     LiviuT          BI-1122 populate dd_OrderUnderdelTolerance, dd_OrderOverdelivTolerance, dd_unlimitedoverdelflagorder */
/* 10 Jul 2015  2.2	Lokesh		BI-802 changes - Do not hard-delete Prod Orders	*/
/* 02 Mar 2015  2.1     L.Ionescu       Add combine for table subqry_po_001, tmp_pGlobalCurrency_planorder, fact_productionorder */
/* 05 Feb 2015	2.0	Suchithra	Added fields for AFPO_CHARG (dd_Batch).Resolution for Base camp item */
/* 15 Oct 2014    		  Amar 	 New fields for AFPO_CHARG (Batch) and Dimension Product Hierarchy */
/*                        	    for both Header and Item Part                 */
/* 5 Sept 2014    	Cornelia 	 add Dim_DateidActualHeaderFinishDate_merck 	  */
/* 3 Sept 2014    	Cornelia correct Error in operator tree for Dim_ProfitCenterId update */
/* 3 Sept 2014    	Cornelia correct dd_RoutingOperationNo default/comparision value */
/* 19 Aug 2014 		Alexandra	 add dd_CancelledOrder 		*/
/* 2 Sept 2014    		  Cornelia 	 add Dim_DateidActualHeaderFinishDate_merck	  */
/* 24 Jul 2014    		  Lokesh 	 Update ct_underdeliverytol_merck,ct_overdeliverytol_merck,ct_unimitedoverdelivery_merck	  */
/* 6 March 2014    	Cornelia 	 add ct_qtyuoe 	  */
/* 30 Dec 2013    	Cornelia 	 update dim_customer 	  */
/* 24 Oct 2013    1.11	Issam	 	 New fields AFKO.VORUE and AUFK.VAPLZ 	  */
/* 7 Sep 2013     1.5	Lokesh	 	Exchange rate, currency and amt changes */
/*  28 Aug 2013   1.4	Shanthi    	New Field WIP Qty from AUFM		  */
/**********************************************************************************/
Drop table if exists pGlobalCurrency_po_43;

Create table pGlobalCurrency_po_43(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_43(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_43
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');


Drop table if exists fact_productionorder_tmp_43;
Drop table if exists max_holder_43;

Create table fact_productionorder_tmp_43 as
SELECT dd_ordernumber,dd_orderitemno
FROM fact_productionorder ;

Create table max_holder_43
as Select ifnull(max(fact_productionorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_productionorder;

DROP TABLE IF EXISTS TMP_001_productionorder;
CREATE TABLE TMP_001_productionorder like  fact_productionorder INCLUDING DEFAULTS INCLUDING IDENTITY;

alter table TMP_001_productionorder add AUFK_WAERS varchar(7) UTF8 default 'Not Set'  NOT NULL ENABLE;

INSERT INTO TMP_001_productionorder(fact_productionorderid,
				 Dim_DateidRelease,
                                 Dim_DateidBOMExplosion,
                                 Dim_DateidLastScheduling,
                                 Dim_DateidTechnicalCompletion,
                                 Dim_DateidBasicStart,
                                 Dim_DateidScheduledRelease,
                                 Dim_DateidScheduledFinish,
                                 Dim_DateidScheduledStart,
                                 Dim_DateidActualStart,
                                 Dim_DateidConfirmedOrderFinish,
                                 Dim_DateidActualHeaderFinish,
                                 Dim_DateidActualHeaderFinishDate_merck,
                                 Dim_DateidActualRelease,
                                 Dim_DateidActualItemFinish,
                                 Dim_DateidPlannedOrderDelivery,
                                 Dim_DateidRoutingTransfer,
                                 Dim_DateidBasicFinish,
                                 Dim_ControllingAreaid,
                                 Dim_ProfitCenterId,
                                 Dim_tasklisttypeid,
                                 Dim_PartidHeader,
                                 Dim_bomstatusid,
                                 Dim_bomusageid,
                                 dim_productionschedulerid,
                                 Dim_ProcurementID,
                                 Dim_SpecialProcurementID,
                                 Dim_UnitOfMeasureid,
                                 Dim_PartidItem,
                                 Dim_AccountCategoryid,
                                 Dim_StockTypeid,
                                 Dim_StorageLocationid,
                                 Dim_Plantid,
                                 dim_specialstockid,
                                 Dim_ConsumptionTypeid,
                                 Dim_SalesOrgid,
                                 Dim_PurchaseOrgid,
                                 Dim_Currencyid,
                                 Dim_Companyid,
                                 Dim_ordertypeid,
                                 dim_productionorderstatusid,
                                 dim_productionordermiscid,
                                 Dim_BuildTypeid,
                                 dd_ordernumber,
                                 dd_orderitemno,
                                 dd_bomexplosionno,
                                 dd_plannedorderno,
                                 dd_SalesOrderNo,
                                 dd_SalesOrderItemNo,
                                 dd_SalesOrderDeliveryScheduleNo,
                                 ct_ConfirmedReworkQty,
                                 ct_ScrapQty,
                                 ct_OrderItemQty,
                                 ct_TotalOrderQty,
                                 ct_GRQty,
                                 ct_GRProcessingTime,
                                 amt_estimatedTotalCost,
                                 amt_ValueGR,
                                 dd_BOMLevel,
                                 dd_sequenceno,
                                 dd_ObjectNumber,
				 ct_ConfirmedScrapQty,
				 dd_OrderDescription,
                                 Dim_MrpControllerId,
				dim_currencyid_TRA,
				dim_currencyid_GBL,
				dd_OperationNumber,
				dd_WorkCenter,
				dim_customerid,
				AUFK_WAERS,
				dd_RoutingOperationNo,
        dd_batch,
        Dim_PartHeaderProductHierarchyid,
        Dim_PartItemProductHierarchyid,
		ct_OrderUnderdelTolerance,
		ct_OrderOverdelivTolerance,
		dd_unlimitedoverdelflagorder)
   SELECT max_holder_43.maxid + row_number() over(order by '') fact_productionorderid,
	  1   Dim_DateidRelease,
          1   Dim_DateidBOMExplosion,
          1   Dim_DateidLastScheduling,
          1   Dim_DateidTechnicalCompletion,
          1 Dim_DateidBasicStart,
          1   Dim_DateidScheduledRelease,
          1   Dim_DateidScheduledFinish,
          1   Dim_DateidScheduledStart,
          1   Dim_DateidActualStart,
          1   Dim_DateidConfirmedOrderFinish,
          1   Dim_DateidActualHeaderFinish,
          1   Dim_DateidActualHeaderFinishDate_merck,
          1   Dim_DateidActualRelease,
          1   Dim_DateidActualItemFinish,
          1   Dim_DateidPlannedOrderDelivery,
          1   Dim_DateidRoutingTransfer,
          1   Dim_DateidBasicFinish,
          1   Dim_ControllingAreaid,
          1 Dim_ProfitCenterId,
          1   Dim_tasklisttypeid,
          1   Dim_PartidHeader,
          1 Dim_bomstatusid,
          1   Dim_bomusageid,
          1   dim_productionschedulerid,
          1   Dim_ProcurementID,
          1   Dim_SpecialProcurementID,
          1   Dim_UnitOfMeasureid,
          1  Dim_PartidItem,
          1   Dim_AccountCategoryid,
          1   Dim_StockTypeid,
          1   Dim_StorageLocationid,
          1 Dim_Plantid,
          1   dim_specialstockid,
          1   Dim_ConsumptionTypeid,
          1  Dim_SalesOrgid,
          1  Dim_PurchaseOrgid,
          1  Dim_Currencyid,
          1  Dim_Companyid,
          1  Dim_productionordertypeid,
          1   dim_productionorderstatusid,
          1   dim_productionordermiscid,
          1   Dim_BuildTypeid,
          ifnull(AFKO_AUFNR,'Not Set') dd_ordernumber,
          AFPO_POSNR dd_orderitemno,
          ifnull(AFPO_SERNR, 'Not Set') dd_bomexplosionno,
          ifnull(AFPO_PLNUM,'Not Set') dd_plannedorderno,
          ifnull(AFPO_KDAUF,'Not Set') dd_SalesOrderNo,
          ifnull(AFPO_KDPOS,0) dd_SalesOrderItemNo,
          ifnull(AFPO_KDEIN,0) dd_SalesOrderDeliveryScheduleNo,
          AFKO_RMNGA ct_ConfirmedReworkQty,
          AFPO_PSAMG ct_ScrapQty,
          AFPO_PSMNG ct_OrderItemQty,
          AFKO_GAMNG ct_TotalOrderQty,
          AFPO_WEMNG ct_GRQty,
          AFPO_WEBAZ ct_GRProcessingTime,
          AUFK_USER4 amt_estimatedTotalCost,
          mhi.AFPO_WEWRT amt_ValueGR,
          AFKO_STUFE dd_BOMLevel,
          ifnull(AFKO_CY_SEQNR,'Not Set') dd_sequenceno,
          ifnull(AUFK_OBJNR,'Not Set') dd_ObjectNumber,
	  ifnull(AFKO_IASMG,0.000) ct_ConfirmedScrapQty,
	  ifnull(AUFK_KTEXT,'Not Set') dd_OrderDescription,
          1 Dim_MrpControllerId,
          1 Dim_Currencyid_TRA,
          1 dim_Currencyid_GBL,
	ifnull(AFKO_VORUE,'Not Set') dd_OperationNumber,
	ifnull(AUFK_VAPLZ,'Not Set') dd_WorkCenter,
	1 dim_customerid,
	mhi.AUFK_WAERS, -- used in Dim_Currencyid_TRA
	ifnull(AFKO_AUFPL, 0) dd_RoutingOperationNo,
  ifnull(AFPO_CHARG,'Not Set') dd_batch,
        1 Dim_PartHeaderProductHierarchyid,
        1 Dim_PartItemProductHierarchyid,
		ifnull(mhi.AFPO_UNTTO, 0) AS ct_OrderUnderdelTolerance,
		ifnull(mhi.AFPO_UEBTO, 0) AS ct_OrderOverdelivTolerance,
		ifnull(mhi.AFPO_UEBTK, 'Not Set') AS dd_unlimitedoverdelflagorder
     FROM  max_holder_43,AFKO_AFPO_AUFK mhi,pGlobalCurrency_po_43
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_productionorder_tmp_43 po
                   WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
                         AND po.dd_orderitemno = mhi.AFPO_POSNR);

UPDATE TMP_001_productionorder t1
SET t1.Dim_Currencyid_TRA = IFNULL(cur.Dim_Currencyid,1)
FROM TMP_001_productionorder t1 
		LEFT JOIN Dim_Currency cur ON		
			cur.CurrencyCode = t1.AUFK_WAERS;

UPDATE TMP_001_productionorder t1
SET t1.Dim_Currencyid_GBL = IFNULL(t2.Dim_Currencyid,1)
FROM TMP_001_productionorder t1 
		CROSS JOIN (SELECT cr.Dim_Currencyid 
		            FROM Dim_Currency cr 
							INNER JOIN pGlobalCurrency_po_43 p
                  on cr.CurrencyCode = p.pGlobalCurrency) t2;

INSERT INTO fact_productionorder(fact_productionorderid,
				 Dim_DateidRelease,
                                 Dim_DateidBOMExplosion,
                                 Dim_DateidLastScheduling,
                                 Dim_DateidTechnicalCompletion,
                                 Dim_DateidBasicStart,
                                 Dim_DateidScheduledRelease,
                                 Dim_DateidScheduledFinish,
                                 Dim_DateidScheduledStart,
                                 Dim_DateidActualStart,
                                 Dim_DateidConfirmedOrderFinish,
                                 Dim_DateidActualHeaderFinish,
                                 Dim_DateidActualHeaderFinishDate_merck,
                                 Dim_DateidActualRelease,
                                 Dim_DateidActualItemFinish,
                                 Dim_DateidPlannedOrderDelivery,
                                 Dim_DateidRoutingTransfer,
                                 Dim_DateidBasicFinish,
                                 Dim_ControllingAreaid,
                                 Dim_ProfitCenterId,
                                 Dim_tasklisttypeid,
                                 Dim_PartidHeader,
                                 Dim_bomstatusid,
                                 Dim_bomusageid,
                                 dim_productionschedulerid,
                                 Dim_ProcurementID,
                                 Dim_SpecialProcurementID,
                                 Dim_UnitOfMeasureid,
                                 Dim_PartidItem,
                                 Dim_AccountCategoryid,
                                 Dim_StockTypeid,
                                 Dim_StorageLocationid,
                                 Dim_Plantid,
                                 dim_specialstockid,
                                 Dim_ConsumptionTypeid,
                                 Dim_SalesOrgid,
                                 Dim_PurchaseOrgid,
                                 Dim_Currencyid,
                                 Dim_Companyid,
                                 Dim_ordertypeid,
                                 dim_productionorderstatusid,
                                 dim_productionordermiscid,
                                 Dim_BuildTypeid,
                                 dd_ordernumber,
                                 dd_orderitemno,
                                 dd_bomexplosionno,
                                 dd_plannedorderno,
                                 dd_SalesOrderNo,
                                 dd_SalesOrderItemNo,
                                 dd_SalesOrderDeliveryScheduleNo,
                                 ct_ConfirmedReworkQty,
                                 ct_ScrapQty,
                                 ct_OrderItemQty,
                                 ct_TotalOrderQty,
                                 ct_GRQty,
                                 ct_GRProcessingTime,
                                 amt_estimatedTotalCost,
                                 amt_ValueGR,
                                 dd_BOMLevel,
                                 dd_sequenceno,
                                 dd_ObjectNumber,
				 ct_ConfirmedScrapQty,
				 dd_OrderDescription,
                                 Dim_MrpControllerId,
				dim_currencyid_TRA,
				dim_currencyid_GBL,
				dd_OperationNumber,
				dd_WorkCenter,
				dim_customerid,
				dd_RoutingOperationNo,
        dd_batch,
        Dim_PartHeaderProductHierarchyid,
        Dim_PartItemProductHierarchyid,
		ct_OrderUnderdelTolerance,
		ct_OrderOverdelivTolerance,
		dd_unlimitedoverdelflagorder)
		
	select fact_productionorderid,
				 Dim_DateidRelease,
                                 Dim_DateidBOMExplosion,
                                 Dim_DateidLastScheduling,
                                 Dim_DateidTechnicalCompletion,
                                 Dim_DateidBasicStart,
                                 Dim_DateidScheduledRelease,
                                 Dim_DateidScheduledFinish,
                                 Dim_DateidScheduledStart,
                                 Dim_DateidActualStart,
                                 Dim_DateidConfirmedOrderFinish,
                                 Dim_DateidActualHeaderFinish,
                                 Dim_DateidActualHeaderFinishDate_merck,
                                 Dim_DateidActualRelease,
                                 Dim_DateidActualItemFinish,
                                 Dim_DateidPlannedOrderDelivery,
                                 Dim_DateidRoutingTransfer,
                                 Dim_DateidBasicFinish,
                                 Dim_ControllingAreaid,
                                 Dim_ProfitCenterId,
                                 Dim_tasklisttypeid,
                                 Dim_PartidHeader,
                                 Dim_bomstatusid,
                                 Dim_bomusageid,
                                 dim_productionschedulerid,
                                 Dim_ProcurementID,
                                 Dim_SpecialProcurementID,
                                 Dim_UnitOfMeasureid,
                                 Dim_PartidItem,
                                 Dim_AccountCategoryid,
                                 Dim_StockTypeid,
                                 Dim_StorageLocationid,
                                 Dim_Plantid,
                                 dim_specialstockid,
                                 Dim_ConsumptionTypeid,
                                 Dim_SalesOrgid,
                                 Dim_PurchaseOrgid,
                                 Dim_Currencyid,
                                 Dim_Companyid,
                                 Dim_ordertypeid,
                                 dim_productionorderstatusid,
                                 dim_productionordermiscid,
                                 Dim_BuildTypeid,
                                 dd_ordernumber,
                                 dd_orderitemno,
                                 dd_bomexplosionno,
                                 dd_plannedorderno,
                                 dd_SalesOrderNo,
                                 dd_SalesOrderItemNo,
                                 dd_SalesOrderDeliveryScheduleNo,
                                 ct_ConfirmedReworkQty,
                                 ct_ScrapQty,
                                 ct_OrderItemQty,
                                 ct_TotalOrderQty,
                                 ct_GRQty,
                                 ct_GRProcessingTime,
                                 amt_estimatedTotalCost,
                                 amt_ValueGR,
                                 dd_BOMLevel,
                                 dd_sequenceno,
                                 dd_ObjectNumber,
				 ct_ConfirmedScrapQty,
				 dd_OrderDescription,
                                 Dim_MrpControllerId,
				dim_currencyid_TRA,
				dim_currencyid_GBL,
				dd_OperationNumber,
				dd_WorkCenter,
				dim_customerid,
				dd_RoutingOperationNo,
        dd_batch,
        Dim_PartHeaderProductHierarchyid,
        Dim_PartItemProductHierarchyid,
		ct_OrderUnderdelTolerance,
		ct_OrderOverdelivTolerance,
		dd_unlimitedoverdelflagorder
		FROM TMP_001_productionorder;
Drop table if exists fact_productionorder_tmp_43;
Drop table if exists max_holder_43;

/* BI-802 : Update deletion flag instead of doing a hard-delete */
UPDATE fact_productionorder
SET dd_deletionflag = 'Y'
WHERE EXISTS ( SELECT 1 FROM AFKO_AFPO_AUFK WHERE AFKO_AUFNR = dd_ordernumber AND AFPO_POSNR = dd_orderitemno AND AFPO_XLOEK = 'X')
AND dd_deletionflag <> 'Y';

UPDATE fact_productionorder
SET dd_deletionflag = 'Y'
WHERE EXISTS ( SELECT 1 FROM AFKO_AFPO_AUFK WHERE AFKO_AUFNR = dd_ordernumber AND AUFK_LOEKZ = 'X' )
AND dd_deletionflag <> 'Y';

UPDATE fact_productionorder po
SET po.Dim_DateidRelease = dr.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date dr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dr.DateValue = AUFK_IDAT1 AND mhi.AUFK_BUKRS = dr.CompanyCode and dr.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidRelease <> dr.dim_dateid;

UPDATE fact_productionorder po
SET po.Dim_DateidBOMExplosion = dr.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date dr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dr.DateValue = AFKO_AUFLD AND mhi.AUFK_BUKRS = dr.CompanyCode and dr.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidBOMExplosion <> dr.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidLastScheduling = ls.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ls
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ls.DateValue = AFKO_TRMDT AND mhi.AUFK_BUKRS = ls.CompanyCode and ls.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidLastScheduling <> ls.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidTechnicalCompletion = tc.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date tc
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND tc.DateValue = AUFK_IDAT2 AND mhi.AUFK_BUKRS = tc.CompanyCode and tc.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidTechnicalCompletion <> tc.dim_dateid;

  UPDATE fact_productionorder po
   SET po.Dim_DateidBasicStart= bst.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date bst
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bst.DateValue = AFKO_GSTRP AND mhi.AUFK_BUKRS = bst.CompanyCode and bst.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidBasicStart <> bst.dim_dateid;

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledRelease = sr.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date sr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sr.DateValue = AFKO_FTRMS AND mhi.AUFK_BUKRS = sr.CompanyCode and sr.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidScheduledRelease <> sr.dim_dateid;

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledFinish = sf.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date sf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sf.DateValue = AFPO_DGLTS AND mhi.AUFK_BUKRS = sf.CompanyCode and sf.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidScheduledFinish <> sf.dim_dateid;

  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledStart = ss.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ss
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ss.DateValue = AFKO_GSTRS AND mhi.AUFK_BUKRS = ss.CompanyCode and ss.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidScheduledStart <> ss.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualStart = ast.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ast
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ast.DateValue = AFKO_GSTRI AND mhi.AUFK_BUKRS = ast.CompanyCode and ast.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidActualStart <> ast.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidConfirmedOrderFinish = cof.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date cof
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND cof.DateValue = AFKO_GETRI AND mhi.AUFK_BUKRS = cof.CompanyCode and cof.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidConfirmedOrderFinish <> cof.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualHeaderFinish = ahf.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ahf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ahf.DateValue = AFKO_GLTRI AND mhi.AUFK_BUKRS = ahf.CompanyCode and ahf.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidActualHeaderFinish <> ahf.dim_dateid;
  
  UPDATE fact_productionorder po
   SET po.Dim_DateidActualHeaderFinishDate_merck = ahf.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/   
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ahf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ahf.DateValue = mhi.AFPO_LTRMI AND mhi.AUFK_BUKRS = ahf.CompanyCode and ahf.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidActualHeaderFinishDate_merck <> ahf.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualRelease = ar.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date ar
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ar.DateValue = AFKO_FTRMI AND mhi.AUFK_BUKRS = ar.CompanyCode and ar.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidActualRelease <> ar.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualItemFinish = aif.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date aif
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND aif.DateValue = AFPO_LTRMI AND mhi.AUFK_BUKRS = aif.CompanyCode and aif.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidActualItemFinish <> aif.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidActualItemFinish = aif.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date aif
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND aif.DateValue = AFPO_LTRMI AND mhi.AUFK_BUKRS = aif.CompanyCode and aif.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidActualItemFinish <> aif.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidPlannedOrderDelivery = pod.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date pod
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pod.DateValue = AFPO_LTRMP AND mhi.AUFK_BUKRS = pod.CompanyCode and pod.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidPlannedOrderDelivery <> pod.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidRoutingTransfer = rt.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date rt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND rt.DateValue = AFKO_PLAUF AND mhi.AUFK_BUKRS = rt.CompanyCode and rt.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidRoutingTransfer <> rt.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_DateidBasicFinish = bf.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_date bf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bf.DateValue = AFPO_DGLTP AND mhi.AUFK_BUKRS = bf.CompanyCode and bf.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidBasicFinish <> bf.dim_dateid;

UPDATE fact_productionorder po
   SET po.Dim_ControllingAreaid = ca.dim_controllingareaid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_ControllingArea ca
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ca.ControllingAreaCode = AUFK_KOKRS
  AND po.Dim_ControllingAreaid <> ca.dim_controllingareaid;

Drop table if exists dim_profitcenter_tmp_43;


Create table dim_profitcenter_tmp_43 as
Select  * from dim_profitcenter  ORDER BY ValidTo ASC;



UPDATE  fact_productionorder po
   SET po.Dim_ProfitCenterId = IFNULL(pc.Dim_ProfitCenterid,1)
FROM fact_productionorder po 
		INNER JOIN AFKO_AFPO_AUFK mhi ON po.dd_ordernumber = mhi.AFKO_AUFNR
									 AND po.dd_orderitemno = mhi.AFPO_POSNR
		LEFT JOIN dim_profitcenter_tmp_43 pc ON pc.ProfitCenterCode = AUFK_PRCTR
											AND pc.ControllingArea = AUFK_KOKRS
											AND pc.ValidTo >= AFKO_GSTRP;

drop table if exists dim_profitcenter_tmp_43;


UPDATE fact_productionorder po
   SET po.Dim_tasklisttypeid = tlt.Dim_tasklisttypeid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_tasklisttype tlt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND tlt.TaskListTypeCode = AFKO_PLNTY 
  AND po.Dim_tasklisttypeid <> tlt.Dim_tasklisttypeid;

 UPDATE fact_productionorder po
   SET po.dd_RoutingOperationNo = ifnull(AFKO_AUFPL,0)
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ifnull(po.dd_RoutingOperationNo,0) <> ifnull(AFKO_AUFPL,0);

UPDATE fact_productionorder po

   SET po.Dim_PartidHeader = phdr.Dim_Partid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part phdr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND phdr.PartNumber = AFKO_STLBEZ AND phdr.Plant = mhi.AUFK_WERKS
  AND po.Dim_PartidHeader <> phdr.Dim_Partid;


UPDATE fact_productionorder po

   SET po.Dim_PartHeaderProductHierarchyid = phhdr.Dim_ProductHierarchyid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po, AFKO_AFPO_AUFK mhi,
       dim_part phdr,
       dim_producthierarchy phhdr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND phdr.PartNumber = AFKO_STLBEZ AND phdr.Plant = mhi.AUFK_WERKS
  AND phdr.producthierarchy = phhdr.producthierarchy
  AND po.Dim_PartHeaderProductHierarchyid <> phhdr.Dim_ProductHierarchyid;
  
  UPDATE fact_productionorder po
   SET po.Dim_bomstatusid = bs.Dim_bomstatusid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_bomstatus bs
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bs.BOMStatusCode = AFKO_STLST 
  AND po.Dim_bomstatusid <> bs.Dim_bomstatusid;

UPDATE fact_productionorder po
   SET po.Dim_bomusageid = bu.Dim_bomusageid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_bomusage bu
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND bu.BOMUsageCode = AFKO_STLAN 
  aND po.Dim_bomusageid <> bu.Dim_bomusageid;

UPDATE fact_productionorder po
   SET po.dim_productionschedulerid = ps.dim_productionschedulerid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_productionscheduler ps
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ps.ProductionScheduler = AFKO_FEVOR
  AND ps.Plant = mhi.AUFK_WERKS
  AND po.dim_productionschedulerid <> ps.dim_productionschedulerid ;

UPDATE fact_productionorder po
   SET po.Dim_ProcurementID = dpr.Dim_ProcurementID
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_procurement dpr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dpr.procurement = AFPO_BESKZ 
  AND po.Dim_ProcurementID <> dpr.Dim_ProcurementID;


UPDATE fact_productionorder po
   SET po.Dim_SpecialProcurementID = spr.Dim_SpecialProcurementID
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_specialprocurement spr
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND spr.specialprocurement = AFPO_PSOBS 
  AND po.Dim_SpecialProcurementID <> spr.Dim_SpecialProcurementID;

UPDATE fact_productionorder po
   SET po.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       Dim_UnitOfMeasure uom
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND uom.UOM = afpo_meins 
  AND po.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;

UPDATE fact_productionorder po
   SET po.Dim_PartidItem = pitem.Dim_Partid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part pitem
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pitem.PartNumber = mhi.AFPO_MATNR
       AND pitem.Plant = mhi.AFPO_DWERK 
       AND po.Dim_PartidItem <> pitem.Dim_Partid;
       
       UPDATE fact_productionorder po
   SET po.Dim_PartItemProductHierarchyid = phitem.Dim_ProductHierarchyid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_part pitem,
       dim_producthierarchy phitem
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND pitem.PartNumber = AFKO_STLBEZ AND pitem.Plant = mhi.AUFK_WERKS
  AND pitem.producthierarchy = phitem.producthierarchy
  AND po.Dim_PartItemProductHierarchyid <> phitem.Dim_ProductHierarchyid;
  
  UPDATE fact_productionorder po
   SET po.Dim_AccountCategoryid = ac.Dim_AccountCategoryid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_accountcategory ac
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ac.Category = AFPO_KNTTP 
  AND po.Dim_AccountCategoryid <> ac.Dim_AccountCategoryid;

UPDATE fact_productionorder po
   SET po.Dim_StockTypeid = st.Dim_StockTypeid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_stocktype st
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND st.TypeCode = AFPO_INSMK 
  AND po.Dim_StockTypeid <> st.Dim_StockTypeid;

UPDATE fact_productionorder po
   SET po.Dim_StorageLocationid = sl.Dim_StorageLocationid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi, dim_storagelocation sl
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND sl.LocationCode = AFPO_LGORT
       AND sl.Plant = AFPO_DWERK
       AND po.Dim_StorageLocationid <> sl.Dim_StorageLocationid;

UPDATE fact_productionorder po
   SET po.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_consumptiontype ct
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND ct.ConsumptionCode = AFPO_KZVBR 
  AND po.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;

UPDATE fact_productionorder po
   SET po.dim_specialstockid = spt.dim_specialstockid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_specialstock spt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND spt.specialstockindicator = AFPO_SOBKZ
  AND po.dim_specialstockid <> spt.dim_specialstockid;

UPDATE fact_productionorder po
   SET po.Dim_Plantid = dp.Dim_Plantid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_plant dp
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dp.PlantCode = mhi.afpo_dwerk
  ANd  po.Dim_Plantid <> dp.Dim_Plantid;

UPDATE fact_productionorder po
   SET po.dim_salesorgid = so.dim_salesorgid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_salesorg so,
       dim_plant dp
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dp.PlantCode = mhi.afpo_dwerk
       AND so.SalesOrgCode = dp.SalesOrg
       AND  po.dim_salesorgid <> so.dim_salesorgid;

UPDATE fact_productionorder po
   SET po.Dim_PurchaseOrgid = porg.Dim_PurchaseOrgid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_purchaseorg porg,
       dim_plant dp
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND porg.PurchaseOrgCode = dp.PurchOrg
       AND dp.PlantCode = mhi.AUFK_WERKS
       AND po.Dim_PurchaseOrgid <> porg.Dim_PurchaseOrgid;

	   /*
UPDATE fact_productionorder po
FROM AFKO_AFPO_AUFK mhi,
       dim_Currency cur
   SET po.Dim_Currencyid = cur.Dim_Currencyid
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND cur.CurrencyCode = mhi.AUFK_WAERS */
       
UPDATE fact_productionorder po
   SET po.Dim_Companyid = dc.Dim_Companyid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_Company dc
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dc.CompanyCode = mhi.AUFK_BUKRS
       AND po.Dim_Companyid <> dc.Dim_Companyid;
	   
/* Dim_Currencyid is now the local curr */	   
UPDATE fact_productionorder po
   SET po.Dim_Currencyid = cur.Dim_Currencyid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,dim_Company dc,
       dim_Currency cur
 WHERE     po.Dim_Companyid = dc.Dim_Companyid
 AND cur.CurrencyCode = dc.currency;	   


UPDATE fact_productionorder po
SET  ct_ConfirmedScrapQty = ifnull(AFKO_IASMG ,0.000)
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND ct_ConfirmedScrapQty <> ifnull(AFKO_IASMG ,0.000);


UPDATE fact_productionorder po
   SET dd_OrderDescription = ifnull(AUFK_KTEXT,'Not Set')
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dd_OrderDescription <>  ifnull(AUFK_KTEXT,'Not Set');
       
UPDATE fact_productionorder po
   SET po.dim_MrpControllerid = mrp.dim_MrpControllerid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_MrpController mrp, dim_plant pl
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dim_plantid = pl.dim_plantid
       AND mhi.AFKO_DISPO IS  NOT NULL
       AND mrp.MRPController = mhi.AFKO_DISPO AND mrp.plant = pl.plantcode
       AND mrp.RowIsCurrent = 1
	AND po.dim_MrpControllerid <>  mrp.dim_MrpControllerid;       
       
UPDATE fact_productionorder po
   SET po.dim_MrpControllerid = 1
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND mhi.AFKO_DISPO IS  NULL
	AND  po.dim_MrpControllerid <> 1; 

UPDATE fact_productionorder po
   SET po.Dim_ordertypeid = pot.Dim_productionordertypeid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       Dim_productionordertype pot
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND pot.typecode = AFPO_DAUAT AND pot.RowIsCurrent = 1
       AND po.Dim_ordertypeid <> pot.Dim_productionordertypeid;

UPDATE fact_productionorder po
   SET po.dim_productionordermiscid  = misc.dim_productionordermiscid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_productionordermisc misc
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND     CollectiveOrder = ifnull(AFKO_PRODNET, 'Not Set')
                  AND DeliveryComplete = ifnull(AFPO_ELIKZ, 'Not Set')
                  AND GRChangeIndicator = ifnull(AFPO_WEAED, 'Not Set')
                  AND GRIndicator = ifnull(AFPO_WEPOS, 'Not Set')
                  AND GRNonValue = ifnull(AFPO_WEUNB, 'Not Set')
                  AND NoAutoCost = ifnull(AFKO_NAUCOST, 'Not Set')
                  AND NoAutoSchedule = ifnull(AFKO_NAUTERM, 'Not Set')
                  AND NoCapacityRequirement = ifnull(AFKO_KBED, 'Not Set')
                  AND NonMRPMaterial = ifnull(AFPO_NDISR, 'Not Set')
                  AND NonMRPOrderItem = ifnull(AFPO_DNREL, 'Not Set')
                  AND NoPlannedCostCalculation = ifnull(AFKO_NOPCOST, 'Not Set')
                  AND OrderReleased = ifnull(AUFK_PHAS1, 'Not Set')
                  AND ReleasedToMRP = ifnull(AFPO_DFREI, 'Not Set')
                  AND SchedulingBreak = ifnull(AFKO_BREAKS, 'Not Set')
                  AND UnlimitedOverdelivery = ifnull(AFPO_UEBTK, 'Not Set')
                  AND ValueWorkRelevant = ifnull(AFKO_FLG_ARBEI, 'Not Set')
                  AND po.dim_productionordermiscid  <> misc.dim_productionordermiscid;

UPDATE fact_productionorder po
SET dd_bomexplosionno = AFPO_SERNR
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_bomexplosionno <>  AFPO_SERNR;

UPDATE fact_productionorder po

SET po.dd_batch = ifnull(mhi.AFPO_CHARG,'Not Set')
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND po.dd_batch <> ifnull(mhi.AFPO_CHARG,'Not Set');     


 UPDATE fact_productionorder po
   SET  dd_plannedorderno = ifnull(AFPO_PLNUM,'Not Set')
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_plannedorderno <> ifnull(AFPO_PLNUM,'Not Set');



       UPDATE fact_productionorder po
   SET dd_SalesOrderNo = ifnull(AFPO_KDAUF,'Not Set')
 	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_SalesOrderNo <> ifnull(AFPO_KDAUF,'Not Set');



       UPDATE fact_productionorder po
   SET dd_SalesOrderItemNo = AFPO_KDPOS
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_SalesOrderItemNo <> AFPO_KDPOS;



       UPDATE fact_productionorder po
   SET dd_SalesOrderDeliveryScheduleNo = AFPO_KDEIN
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_SalesOrderDeliveryScheduleNo <> AFPO_KDEIN;



       UPDATE fact_productionorder po
   SET ct_ConfirmedReworkQty = AFKO_RMNGA
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_ConfirmedReworkQty <> AFKO_RMNGA;



       UPDATE fact_productionorder po
   SET ct_ScrapQty = AFPO_PSAMG
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_ScrapQty  <> AFPO_PSAMG;



       UPDATE fact_productionorder po
   SET ct_OrderItemQty = AFPO_PSMNG
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_OrderItemQty <> AFPO_PSMNG;



       UPDATE fact_productionorder po
   SET ct_TotalOrderQty = AFKO_GAMNG
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_TotalOrderQty  <> AFKO_GAMNG;



       UPDATE fact_productionorder po
   SET ct_GRQty = AFPO_WEMNG
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_GRQty  <> AFPO_WEMNG;



       UPDATE fact_productionorder po
   SET ct_GRProcessingTime = AFPO_WEBAZ
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND ct_GRProcessingTime <> AFPO_WEBAZ;



       UPDATE fact_productionorder po
   SET amt_estimatedTotalCost = AUFK_USER4
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND amt_estimatedTotalCost <>  AUFK_USER4;


       UPDATE fact_productionorder po
   SET amt_ValueGR = mhi.AFPO_WEWRT
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND  amt_ValueGR <> mhi.AFPO_WEWRT;



       UPDATE fact_productionorder po
   SET Dim_BuildTypeid = (CASE WHEN AFPO_KDAUF IS NOT NULL THEN 1 ELSE 2 END)
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND  Dim_BuildTypeid <> (CASE WHEN AFPO_KDAUF IS NOT NULL THEN 1 ELSE 2 END);


       UPDATE fact_productionorder po
   SET dd_BOMLevel = AFKO_STUFE
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_BOMLevel <> AFKO_STUFE;


       UPDATE fact_productionorder po
   SET dd_sequenceno = ifnull(AFKO_CY_SEQNR,'Not Set')
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_sequenceno <> ifnull(AFKO_CY_SEQNR,'Not Set');



UPDATE fact_productionorder po
   SET dd_ObjectNumber = ifnull(AUFK_OBJNR, 'Not Set')
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dd_ObjectNumber <> ifnull(AUFK_OBJNR, 'Not Set');

Drop table if exists subqry_po_001;

Create table subqry_po_001 as 
Select distinct dd_ObjectNumber,
CONVERT(varchar(20),'Not Set') Closed,
CONVERT(varchar(20),'Not Set') Delivered,
CONVERT(varchar(20),'Not Set') GoodsMovementPosted,
CONVERT(varchar(20),'Not Set') MaterialCommitted,
CONVERT(varchar(20),'Not Set') PreCosted,
CONVERT(varchar(20),'Not Set') SettlementRuleCreated, 
CONVERT(varchar(20),'Not Set') VariancesCalculated,
CONVERT(varchar(20),'Not Set') TechnicallyCompleted,
CONVERT(varchar(20),'Not Set') Created_col,
CONVERT(varchar(20),'Not Set') Released,
CONVERT(varchar(20),'Not Set') MaterialShortage,
CONVERT(varchar(20),'Not Set') PartPrinted,
CONVERT(varchar(20),'Not Set') PartiallyConfirmed,
CONVERT(varchar(20),'Not Set') InspectionLotAssigned,
CONVERT(varchar(20),'Not Set') MaterialAvailabilityNotChecked,
CONVERT(varchar(20),'Not Set') ResultsAnalysisCarriedOut
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
Where  po.dd_ordernumber = mhi.AFKO_AUFNR AND po.dd_orderitemno = mhi.AFPO_POSNR;


Update subqry_po_001 k
Set k.Closed = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0046' AND j.JEST_INACT is NULL;



Update subqry_po_001 k
Set k.Delivered = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0012' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.GoodsMovementPosted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0321' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialCommitted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0340' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PreCosted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0016' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.SettlementRuleCreated = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0028' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.VariancesCalculated = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0056' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.TechnicallyCompleted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0045' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.Created_col = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0001' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.Released = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0002' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialShortage = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0004' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PartPrinted = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0008' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.PartiallyConfirmed = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0010' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.InspectionLotAssigned = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0281' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.MaterialAvailabilityNotChecked = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0420' AND j.JEST_INACT is NULL;


Update subqry_po_001 k
Set k.ResultsAnalysisCarriedOut = 'X'
From subqry_po_001 k,JEST_AUFK j
Where j.JEST_OBJNR = k.dd_ObjectNumber
AND j.JEST_STAT = 'I0082' AND j.JEST_INACT is NULL;

UPDATE fact_productionorder po
SET po.dim_productionorderstatusid = post.dim_productionorderstatusid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,dim_productionorderstatus post,
     AFKO_AFPO_AUFK mhi,
     subqry_po_001 sq
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND sq.dd_ObjectNumber = po.dd_ObjectNumber
       AND post.Closed = sq.Closed
       AND post.Delivered = sq.Delivered
       AND post.GoodsMovementPosted = sq.GoodsMovementPosted
       AND post.MaterialCommitted =sq.MaterialCommitted
       AND post.PreCosted = sq.PreCosted
       AND post.SettlementRuleCreated =sq.SettlementRuleCreated 
       AND post.VariancesCalculated =sq.VariancesCalculated
       AND post.TechnicallyCompleted = sq.TechnicallyCompleted
       AND post."CREATED" = sq.Created_col
       AND post.Released = sq.Released
       AND post.MaterialShortage = sq.MaterialShortage
       AND post.PartPrinted = sq.PartPrinted
       AND post.PartiallyConfirmed = sq.PartiallyConfirmed
       AND post.InspectionLotAssigned = sq.InspectionLotAssigned
       AND post.MaterialAvailabilityNotChecked = sq.MaterialAvailabilityNotChecked
       AND post.ResultsAnalysisCarriedOut = sq.ResultsAnalysisCarriedOut;

/*START - 19.08.2014*/
UPDATE fact_productionorder 
SET dd_cancelledorder = 'Not Set'
   ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_cancelledorder is null;

UPDATE fact_productionorder po
SET po.dd_cancelledorder = 'Yes'
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,dim_productionorderstatus post
WHERE post.dim_productionorderstatusid = po.dim_productionorderstatusid
	AND po.ct_grqty = 0 
	AND post.technicallycompleted = 'X'
	AND po.dd_cancelledorder <> 'Yes';
/*END - 19.08.2014*/

UPDATE fact_productionorder po
SET po.dd_cancelledorder = 'Yes'
FROM fact_productionorder po,dim_productionorderstatus post
WHERE post.dim_productionorderstatusid = po.dim_productionorderstatusid
        AND po.ct_grqty = 0
        AND post.Closed = 'X'
AND po.dd_cancelledorder <> 'Yes';

UPDATE fact_productionorder po
SET amt_ExchangeRate = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po, AFKO_AFPO_AUFK mhi,
     dim_company dc
WHERE       po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS
AND ifnull(amt_ExchangeRate,-1) <> 1; 

UPDATE fact_productionorder po
SET amt_ExchangeRate = ex.exchangeRate
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,dim_company dc,
     AFKO_AFPO_AUFK mhi,
	 tmp_getExchangeRate1 ex 
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS 
AND  pFromCurrency = AUFK_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = AUFK_AEDAT
and fact_script_name = 'bi_populate_prodorder_fact' 
AND amt_ExchangeRate <> ex.exchangeRate ;


UPDATE fact_productionorder po
SET amt_ExchangeRate_GBL = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
     dim_company dc
WHERE       po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS
AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;  

UPDATE fact_productionorder po
SET amt_ExchangeRate_GBL = ex.exchangeRate 
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,dim_company dc,
     AFKO_AFPO_AUFK mhi,
	 tmp_getExchangeRate1 ex,
	 pGlobalCurrency_po_43
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dc.CompanyCode = mhi.AUFK_BUKRS 
AND  pFromCurrency = AUFK_WAERS
and pToCurrency = pGlobalCurrency
and pFromExchangeRate = 0
and pDate =  current_date  /*AUFK_AEDAT - current date to be used for global rate*/
and fact_script_name = 'bi_populate_prodorder_fact'
AND amt_ExchangeRate <> ex.exchangeRate ;

/* 1 Aug 2018 Georgiana removing COSS from PO according to FP-5558*/

/*DROP TABLE IF EXISTS TMP_AUFM_UPDT*/
/*CREATE TABLE TMP_AUFM_UPDT
AS
select AUFM_AUFNR, sum((case a.AUFM_SHKZG when 'S' then -1 else 1 end) * a.AUFM_DMBTR) sum_aufm
from AUFM a group by a.AUFM_AUFNR*/

/*DROP TABLE IF EXISTS TMP_COSS_UPDT
CREATE TABLE TMP_COSS_UPDT
AS
select COSS_OBJNR,sum(c.COSS_WOG001 + c.COSS_WOG002 + c.COSS_WOG003 + c.COSS_WOG004
                                    + c.COSS_WOG005 + c.COSS_WOG006 + c.COSS_WOG007 + c.COSS_WOG008
                                    + c.COSS_WOG009 + c.COSS_WOG010 + c.COSS_WOG011 + c.COSS_WOG012
                                    + c.COSS_WOG013 + c.COSS_WOG014 + c.COSS_WOG015 + c.COSS_WOG016) sum_cos
from coss c
where c.COSS_WRTTP = '04'
group by COSS_OBJNR*/

/*UPDATE fact_productionorder po
SET po.amt_WIPBalance = ifnull(a.sum_aufm,0)
FROM fact_productionorder po
	LEFT JOIN TMP_AUFM_UPDT a ON po.dd_ordernumber = a.AUFM_AUFNR 
WHERE ifnull(amt_WIPBalance,-1) <> ifnull(a.sum_aufm,0)*/

/*UPDATE fact_productionorder po
SET po.amt_WIPBalance = ifnull(po.amt_WIPBalance,0) + ifnull(c.sum_cos,0)
FROM fact_productionorder po
	LEFT JOIN TMP_COSS_UPDT c ON 'OR' || po.dd_ordernumber = c.COSS_OBJNR
WHERE ifnull(amt_WIPBalance,-1) <> ifnull(po.amt_WIPBalance,0) + ifnull(c.sum_cos,0)*/

/*DROP TABLE IF EXISTS TMP_AUFM_UPDT
DROP TABLE IF EXISTS TMP_COSS_UPDT*/
/*1 Aug 2018 End of Changes*/
						   
/* LK: 15 Sep 2013: aufm_dmbtr and coss amts are in local currency. So convert amts to transaction currency */
						   
UPDATE fact_productionorder po
SET amt_WIPBalance = ( amt_WIPBalance / amt_ExchangeRate )
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE amt_ExchangeRate <> 1;						   

DROP TABLE IF EXISTS FPO_TMP;
CREATE TABLE FPO_TMP
AS SELECT a.AUFM_AUFNR,sum(a.AUFM_MENGE) sm_aufmenge
from AUFM a
GROUP BY a.AUFM_AUFNR;

UPDATE fact_productionorder po
SET ct_WIPQty = ifnull(sm_aufmenge,0)
from fact_productionorder po, FPO_TMP a 
where a.AUFM_AUFNR = po.dd_ordernumber;
						 
UPDATE fact_productionorder po
SET dd_OperationNumber = ifnull(AFKO_VORUE, 'Not Set')
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_OperationNumber <>  ifnull(AFKO_VORUE, 'Not Set');

UPDATE fact_productionorder po
SET dd_WorkCenter = ifnull(AUFK_VAPLZ, 'Not Set')
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND dd_WorkCenter <>  ifnull(AUFK_VAPLZ, 'Not Set');

/*
UPDATE fact_productionorder po
FROM  fact_salesorder fso
 SET po.Dim_customerid = fso.Dim_customerid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*WHERE     po.dd_salesorderno = fso.dd_salesdocno
     AND po.dd_salesorderitemno = fso.dd_salesitemno
     AND ifnull(po.Dim_customerid, -1) <> fso.Dim_customerid
*/
drop table if exists tmp_customerid_upd;
create table tmp_customerid_upd as
select distinct po.dd_salesorderno,po.dd_salesorderitemno,fso.Dim_customerid
from fact_productionorder po
, fact_salesorder fso
WHERE     po.dd_salesorderno = fso.dd_salesdocno
     AND po.dd_salesorderitemno = fso.dd_salesitemno
     AND ifnull(po.Dim_customerid, -1) <> fso.Dim_customerid;

UPDATE fact_productionorder po
 SET po.Dim_customerid = tmp.Dim_customerid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  fact_productionorder po,tmp_customerid_upd tmp
WHERE     po.dd_salesorderno = tmp.dd_salesorderno
     AND po.dd_salesorderitemno = tmp.dd_salesorderitemno
     AND ifnull(po.Dim_customerid, -1) <> tmp.Dim_customerid;
drop table if exists tmp_customerid_upd;
     
UPDATE fact_productionorder po
SET dd_OperationNumber = 'Not Set' 
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_OperationNumber IS NULL;

UPDATE fact_productionorder po
SET dd_WorkCenter = 'Not Set' 
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE dd_WorkCenter IS NULL;

/*
UPDATE fact_productionorder po
SET ct_qtyuoe = ifnull((SELECT SUM(r.RESB_ERFMG)
                         FROM RESB r WHERE r.RESB_AUFNR = po.dd_ordernumber),0)
*/

UPDATE fact_productionorder f
SET ct_underdeliverytol_merck = ifnull(m.marc_uneto,0)
    , dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder f, MARC_PRODORDER m,AFKO_AFPO_AUFK mhi
WHERE f.dd_ordernumber = mhi.AFKO_AUFNR
AND f.dd_orderitemno = mhi.AFPO_POSNR
AND m.MARC_MATNR = mhi.AFPO_MATNR
AND m.marc_werks = mhi.afpo_dwerk
AND f.ct_underdeliverytol_merck <> ifnull(m.marc_uneto,0);

UPDATE fact_productionorder f
SET ct_overdeliverytol_merck = ifnull(m.marc_ueeto,0)
    , dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder f, MARC_PRODORDER m,AFKO_AFPO_AUFK mhi
WHERE f.dd_ordernumber = mhi.AFKO_AUFNR
AND f.dd_orderitemno = mhi.AFPO_POSNR
AND m.MARC_MATNR = mhi.AFPO_MATNR
AND m.marc_werks = mhi.afpo_dwerk
AND f.ct_overdeliverytol_merck <> ifnull(m.marc_ueeto,0);

UPDATE fact_productionorder f
SET dd_unimitedoverdelivery_merck = ifnull(m.marc_ueetk,'Not Set')
, dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder f, MARC_PRODORDER m,AFKO_AFPO_AUFK mhi
WHERE f.dd_ordernumber = mhi.AFKO_AUFNR
AND f.dd_orderitemno = mhi.AFPO_POSNR
AND m.MARC_MATNR = mhi.AFPO_MATNR
AND m.marc_werks = mhi.afpo_dwerk
AND f.dd_unimitedoverdelivery_merck <> ifnull(m.marc_ueetk,'Not Set');

/* Update tolerance fields from dim_part */
UPDATE fact_productionorder f
SET ct_underdeliverytol_merck = ifnull(m.marc_uneto,0)
    , dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder f,MARC_PRODORDER m,dim_part p
WHERE f.dim_partiditem = p.dim_partid
AND p.partnumber = MARC_matnr
AND p.plant = marc_werks
AND f.ct_underdeliverytol_merck <> ifnull(m.marc_uneto,0);

UPDATE fact_productionorder f
SET ct_overdeliverytol_merck = ifnull(m.marc_ueeto,0)
    , dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder f,MARC_PRODORDER m,dim_part p
WHERE f.dim_partiditem = p.dim_partid
AND p.partnumber = MARC_matnr
AND p.plant = marc_werks
AND f.ct_overdeliverytol_merck <> ifnull(m.marc_ueeto,0);

UPDATE fact_productionorder f
SET dd_unimitedoverdelivery_merck = ifnull(m.marc_ueetk,'Not Set')
    , dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder f,MARC_PRODORDER m,dim_part p
WHERE f.dim_partiditem = p.dim_partid
AND p.partnumber = MARC_matnr
AND p.plant = marc_werks
AND f.dd_unimitedoverdelivery_merck <> ifnull(m.marc_ueetk,'Not Set');


  UPDATE fact_productionorder po
   SET po.Dim_DateidScheduledFinishHeader = sf.dim_dateid
From fact_productionorder po, AFKO_AFPO_AUFK mhi,
       dim_date sf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND sf.DateValue = afko_gltrs AND mhi.AUFK_BUKRS = sf.CompanyCode and sf.plantcode_factory = mhi.AUFK_WERKS
  AND po.Dim_DateidScheduledFinishHeader <> sf.dim_dateid;


UPDATE fact_productionorder po
SET po.Dim_DateidScheduledFinishHeader = po.Dim_DateidScheduledFinish
WHERE po.Dim_DateidScheduledFinishHeader = 1
AND po.Dim_DateidScheduledFinishHeader <> po.Dim_DateidScheduledFinish;

/* 10.06.2015 new dim */
update fact_productionorder set
dim_dateidactualstart_merck = 
ifnull(case when dim_dateidactualstart =1 then dim_dateidbasicstart else dim_dateidactualstart end,1)
where dim_dateidactualstart_merck <>
ifnull(case when dim_dateidactualstart =1 then dim_dateidbasicstart else dim_dateidactualstart end,1); 

/* 26 August 2015 Begin changes LiviuT */
UPDATE fact_productionorder po
SET po.ct_OrderUnderdelTolerance = ifnull(mhi.AFPO_UNTTO, 0)
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
      AND po.dd_orderitemno = mhi.AFPO_POSNR
      AND po.ct_OrderUnderdelTolerance <> ifnull(mhi.AFPO_UNTTO, 0);
  
UPDATE fact_productionorder po
SET po.ct_OrderOverdelivTolerance = ifnull(mhi.AFPO_UEBTO, 0)
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
      AND po.dd_orderitemno = mhi.AFPO_POSNR
      AND po.ct_OrderOverdelivTolerance <> ifnull(mhi.AFPO_UEBTO, 0);

UPDATE fact_productionorder po
SET po.dd_unlimitedoverdelflagorder = ifnull(mhi.AFPO_UEBTK, 'Not Set')
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
      AND po.dd_orderitemno = mhi.AFPO_POSNR
      AND po.dd_unlimitedoverdelflagorder <> ifnull(mhi.AFPO_UEBTK, 'Not Set');

/* Octavian: Every Angle Transition Addons */	  
update fact_productionorder ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from fact_productionorder ia, dim_batch b, dim_part dp
where 
ia.dd_batch = b.batchnumber and
 ia.Dim_PartidHeader = dp.dim_partid and dp.partnumber = b.partnumber and
 dp.plant = b.plantcode and
ia.dim_batchid <> b.dim_batchid;

update fact_productionorder po
set po.dim_tasklistheaderid = tlh.dim_tasklistheaderid,
dw_update_date = current_timestamp
from fact_productionorder po, afko_afpo_aufk afko, dim_tasklistheader tlh 
WHERE po.dd_ordernumber = afko.AFKO_AUFNR
AND po.dd_orderitemno = afko.AFPO_POSNR
AND ifnull(afko.AFKO_PLNTY,'Not Set') = tlh.tasklisttype
AND ifnull(afko.AFKO_PLNNR,'Not Set') = tlh.keytasklistgr
AND ifnull(afko.AFKO_PLNAL,'Not Set') = tlh.groupcounter
AND ifnull(afko.AFKO_ZAEHL,0) = tlh.internalcounter
AND po.dim_tasklistheaderid <> tlh.dim_tasklistheaderid;
/* Octavian: Every Angle Transition Addons */

/* Georgiana: Every Angle Transition Addons */	 

Update fact_productionorder po
set po.ct_expectedsurplusordeficit = ifnull(mhi.AFPO_IAMNG,0)
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	   AND po.ct_expectedsurplusordeficit <> ifnull(mhi.AFPO_IAMNG,0);
	   
Update fact_productionorder po
set po.dd_alternativebom = ifnull(mhi.AFKO_STLAL,'Not Set')
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	   AND po.dd_alternativebom <> ifnull(mhi.AFKO_STLAL,'Not Set');
	   
	   	   
Update fact_productionorder po
set po.dd_billofmaterial = ifnull(mhi.AFKO_STLNR,'Not Set')
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	   AND po.dd_billofmaterial <> ifnull(mhi.AFKO_STLNR,'Not Set');
	   
UPDATE fact_productionorder po
   SET po.Dim_baseUnitOfMeasureid = uom.Dim_UnitOfMeasureid
	,po.dw_update_date = current_timestamp 
FROM fact_productionorder po, AFKO_AFPO_AUFK mhi,
       Dim_UnitOfMeasure uom
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND uom.UOM = afko_gmein
  AND po.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;	   

Update fact_productionorder po
set po.dd_ordercategory = ifnull(mhi.AUFK_AUTYP,0)
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	   AND po.dd_ordercategory <> ifnull(mhi.AUFK_AUTYP,0);
	   
Update fact_productionorder po
set po.dd_enteredby = ifnull(mhi.AUFK_ERNAM,'Not Set')
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	   AND po.dd_enteredby <> ifnull(mhi.AUFK_ERNAM,'Not Set');
	   
UPDATE fact_productionorder po
SET dim_tehnicalcompletiondateid = dt.dim_dateid
,dw_update_date=current_timestamp
from fact_productionorder po,dim_date dt, AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	   AND dt.datevalue = AUFK_IDAT2 AND mhi.AUFK_BUKRS = dt.CompanyCode and dt.plantcode_factory = mhi.AUFK_WERKS
	   AND dim_tehnicalcompletiondateid <> dt.dim_dateid;
	   
UPDATE fact_productionorder po
SET Dim_dateidorderduedate = dt.dim_dateid
,dw_update_date=current_timestamp
from fact_productionorder po,dim_date dt, AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	   AND dt.datevalue = AFKO_GLTRP AND mhi.AUFK_BUKRS = dt.CompanyCode and dt.plantcode_factory = mhi.AUFK_WERKS
	   AND Dim_dateidorderduedate <> dt.dim_dateid;
	   
UPDATE fact_productionorder po
set dd_groupcounter = ifnull(mhi.AFKO_PLNAL, 'Not Set')
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND dd_groupcounter <> ifnull(mhi.AFKO_PLNAL, 'Not Set');
	   
UPDATE fact_productionorder po
set po.dd_group = ifnull(mhi.AFKO_PLNNR, 'Not Set')
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       and po.dd_group <> ifnull(mhi.AFKO_PLNNR, 'Not Set');
	   

UPDATE fact_productionorder po
set po.dd_productionversion = ifnull(mhi.AFPO_VERID, 'Not Set')
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       and po.dd_productionversion <> ifnull(mhi.AFPO_VERID, 'Not Set');
	   
UPDATE fact_productionorder po
set  po.dim_productionordertypeid=dp.dim_productionordertypeid
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi,dim_ProductionOrderType dp
where po.dd_ordernumber = mhi.AFKO_AUFNR
     AND po.dd_orderitemno = mhi.AFPO_POSNR
     AND dp.TypeCode=mhi.aufk_auart
/*Madalina 28 iul 2017 - rows aren't getting affected for null values - APP-5685*/
     AND ifnull(po.dim_productionordertypeid,-1)<>dp.dim_productionordertypeid;
	 
UPDATE fact_productionorder po
SET po.dim_dateidcreationdate = dt.dim_dateid
,dw_update_date=current_timestamp
from fact_productionorder po,dim_date dt, AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	   AND dt.datevalue = AUFK_ERDAT AND mhi.AUFK_BUKRS = dt.CompanyCode and dt.plantcode_factory = mhi.AUFK_WERKS
	   AND po.dim_dateidcreationdate <> dt.dim_dateid;
	   
UPDATE fact_productionorder po
set po.dd_overheadkey = ifnull(mhi.AUFK_ZSCHL, 'Not Set')
,dw_update_date=current_timestamp
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE  po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       and po.dd_overheadkey <> ifnull(mhi.AUFK_ZSCHL, 'Not Set');
/*Ambiguous Replace fix*/	   
/*UPDATE fact_productionorder po
set po.dd_productversion = ifnull(t.MAPL_ANNAM, 'Not Set')
from fact_productionorder po,AFKO_AFPO_AUFK mhi, MAPL t
where po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND mhi.AFPO_MATNR = t.MAPL_MATNR
       AND mhi.AUFK_WERKS = t.MAPL_WERKS
       AND mhi.AFKO_PLNTY = t.MAPL_PLNTY
       AND mhi.AFKO_PLNNR = t.MAPL_PLNNR
       AND mhi.AFKO_PLNAL = t.MAPL_PLNAL
       AND po.dd_productversion <> ifnull(t.MAPL_ANNAM, 'Not Set')*/
       
drop table if exists tmp_for_upd_prodvers;
create table tmp_for_upd_prodvers as 
select distinct MAPL_ANNAM, mhi.AFKO_AUFNR,mhi.AFPO_POSNR, row_number() over (partition by mhi.AFKO_AUFNR,mhi.AFPO_POSNR order by mhi.AFKO_AUFNR,mhi.AFPO_POSNR ) row_num
from fact_productionorder po,AFKO_AFPO_AUFK mhi, MAPL t
where po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
       AND mhi.AFPO_MATNR = t.MAPL_MATNR
       AND mhi.AUFK_WERKS = t.MAPL_WERKS
       AND mhi.AFKO_PLNTY = t.MAPL_PLNTY
       AND mhi.AFKO_PLNNR = t.MAPL_PLNNR
       AND mhi.AFKO_PLNAL = t.MAPL_PLNAL
       AND po.dd_productversion <> ifnull(t.MAPL_ANNAM, 'Not Set');


UPDATE fact_productionorder po
set po.dd_productversion = ifnull(t.MAPL_ANNAM, 'Not Set')
from fact_productionorder po, tmp_for_upd_prodvers t
where po.dd_ordernumber = t.AFKO_AUFNR
       AND po.dd_orderitemno = t.AFPO_POSNR
       AND row_num=1
       AND po.dd_productversion <> ifnull(t.MAPL_ANNAM, 'Not Set');


/* Georgiana: Every Angle Transition Addons */

/* Octavian: Every Angle Transition */
UPDATE fact_productionorder po
SET po.dd_released = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'I0002' and j.JEST_INACT is NULL
AND po.dd_released <> 'X'; 

UPDATE fact_productionorder po
SET po.dd_materialshortage = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'I0004' and j.JEST_INACT is NULL
AND po.dd_materialshortage <> 'X';

UPDATE fact_productionorder po
SET po.dd_confirmed = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'I0009' and j.JEST_INACT is NULL
AND po.dd_confirmed <> 'X';

UPDATE fact_productionorder po
SET po.dd_delivered = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'I0012' and j.JEST_INACT is NULL
AND po.dd_delivered <> 'X';

UPDATE fact_productionorder po
SET po.dd_technicallycompleted = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'I0045' and j.JEST_INACT is NULL
AND po.dd_technicallycompleted <> 'X';

UPDATE fact_productionorder po
SET po.dd_closed = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'I0046' and j.JEST_INACT is NULL
AND po.dd_closed <> 'X';

UPDATE fact_productionorder po
SET po.dd_paproductquality = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0004' and j.JEST_INACT is NULL
AND po.dd_paproductquality <> 'X';

UPDATE fact_productionorder po
SET po.dd_capacityreasons = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0005' and j.JEST_INACT is NULL
AND po.dd_capacityreasons <> 'X';

UPDATE fact_productionorder po
SET po.dd_waitforsupplypurchased = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0006' and j.JEST_INACT is NULL
AND po.dd_waitforsupplypurchased <> 'X';

UPDATE fact_productionorder po
SET po.dd_expirydateqpend = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0007' and j.JEST_INACT is NULL
AND po.dd_expirydateqpend <> 'X';

UPDATE fact_productionorder po
SET po.dd_changeversion = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0008' and j.JEST_INACT is NULL
AND po.dd_changeversion <> 'X';

UPDATE fact_productionorder po
SET po.dd_requestcust = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0010' and j.JEST_INACT is NULL
AND po.dd_requestcust <> 'X';

UPDATE fact_productionorder po
SET po.dd_delaypackpro = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0011' and j.JEST_INACT is NULL
AND po.dd_delaypackpro <> 'X';

UPDATE fact_productionorder po
SET po.dd_othercomment = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0012' and j.JEST_INACT is NULL
AND po.dd_othercomment <> 'X';

UPDATE fact_productionorder po
SET po.dd_paforecastdemand = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0014' and j.JEST_INACT is NULL
AND po.dd_paforecastdemand <> 'X';

UPDATE fact_productionorder po
SET po.dd_datefictoutind = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JEST_AUFK j
WHERE j.JEST_OBJNR = po.dd_ObjectNumber
AND j.JEST_STAT = 'E0015' and j.JEST_INACT is NULL
AND po.dd_datefictoutind <> 'X';

/* Octavian: Every Angle Transition END */

/*Georgiana Every Angle Transition Addons*/
UPDATE fact_productionorder po
SET po.dd_changeddocreleased = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JCDS_AUFK j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'I0002' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_changeddocreleased <> j.JCDS_UDATE; 

UPDATE fact_productionorder po
SET po.dd_changedmaterialshortage = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JCDS_AUFK j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'I0004' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_changedmaterialshortage <> j.JCDS_UDATE; 

UPDATE fact_productionorder po
SET po.dd_changeconfirmed = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JCDS_AUFK j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'I0009' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_changeconfirmed <> j.JCDS_UDATE; 

UPDATE fact_productionorder po
SET po.dd_changedelivered = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JCDS_AUFK j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'I0012' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_changedelivered <> j.JCDS_UDATE; 

/*Georgiana 01 Aug 2016 taking the max value of JCDS_UDATE as requested in BI-3439*/
/*UPDATE fact_productionorder po
FROM JCDS_AUFK j
SET po.dd_changetechnicallycompleted = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'I0045' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_changetechnicallycompleted <> j.JCDS_UDATE*/

drop table if exists tmp_for_changetechnicallycompleted;
create table tmp_for_changetechnicallycompleted as
select max(j.JCDS_UDATE) as JCDS_UDATE, j.JCDS_OBJNR from jcds_aufk j where j.JCDS_STAT = 'I0045' and j.JCDS_INACT is NULL
group by j.JCDS_OBJNR;

UPDATE fact_productionorder po

SET po.dd_changetechnicallycompleted = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_for_changetechnicallycompleted j, fact_productionorder po
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND po.dd_changetechnicallycompleted <> j.JCDS_UDATE; 

drop table if exists tmp_for_changetechnicallycompleted;
/*01 Aug 2016 End of changes*/
UPDATE fact_productionorder po
SET po.dd_changeclosed = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JCDS_AUFK j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'I0046' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_changeclosed <> j.JCDS_UDATE; 


UPDATE fact_productionorder po
SET po.dd_changedeletionflag = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JCDS_AUFK j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'I0076' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_changedeletionflag <> j.JCDS_UDATE; 

UPDATE fact_productionorder po
SET po.dd_changematerialcommitted = j.JCDS_UDATE,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po,JCDS_AUFK j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'I0340' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_changematerialcommitted <> j.JCDS_UDATE; 

/*Georgiana EA Changes End*/

/* Octavian EA missing JCDS value */
UPDATE fact_productionorder po
SET po.dd_qmtestsscheduled = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, JCDS_AUFK j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND j.JCDS_STAT = 'E0001' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dd_qmtestsscheduled <> j.JCDS_UDATE;
/* Octavian EA missing JCDS value */

/* Octavian adding AUFK-LTEXT */
UPDATE fact_productionorder po
   SET po.dd_longtextexists = ifnull(AUFK_LTEXT,'Not Set')
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_productionorder po, AFKO_AFPO_AUFK mhi
 WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND po.dd_longtextexists <>  ifnull(AUFK_LTEXT,'Not Set');
/* Octavian adding AUFK-LTEXT */

/* Updated std_exchangerate_dateid,dim_change_dateid by Florian on 5 January 2016 */

UPDATE fact_productionorder po
   SET po.dim_change_dateid = dt.dim_dateid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
       dim_plant dp,
	   dim_date dt
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dp.PlantCode = mhi.afpo_dwerk
  AND dt.datevalue = ifnull(mhi.AUFK_AEDAT,'0001-01-01')
  AND dt.companycode = dp.companycode
  and dt.plantcode_factory = mhi.AUFK_WERKS
  ANd  po.dim_change_dateid <> dt.dim_dateid;

UPDATE fact_productionorder po
SET po.std_exchangerate_dateid = po.dim_change_dateid
WHERE   po.std_exchangerate_dateid <> po.dim_change_dateid;

/* END Updated std_exchangerate_dateid,dim_change_dateid by Florian on 5 January 2016 */

 /*20 Apr 2016 Georgiana EA Changes according to BI-2623*/
       
UPDATE fact_productionorder po
set po.ct_proportionunit= f_mm.ct_proportionunit,
dw_update_date = CURRENT_TIMESTAMP

FROM fact_productionorder po,AFKO_AFPO_AUFK mhi,
fact_materialmaster f_mm,
dim_part dp
where 
po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND dp.PartNumber = mhi.AFPO_MATNR
AND dp.Plant = mhi.AFPO_DWERK 
AND po.Dim_PartidItem = dp.dim_partid
AND f_mm.dim_materialmasterid = dp.dim_partid
AND po.ct_proportionunit= f_mm.ct_proportionunit;

UPDATE fact_productionorder po
SET po.dim_unitofmeasureidstockkeeping = uom.dim_unitofmeasureid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder po, AFKO_AFPO_AUFK mhi,
dim_part dp,
dim_unitofmeasure uom
where 
po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dp.PartNumber = mhi.AFPO_MATNR
  AND dp.Plant = mhi.AFPO_DWERK 
  AND po.Dim_PartidItem = dp.dim_partid
  and uom.uom=dp.alternativeuom_stockkeeping
  AND po.dim_unitofmeasureidstockkeeping <> uom.dim_unitofmeasureid;

 /*20Apr 2016 End of Changes*/
 
/* Octavian: EA changes 25-04 */
drop table if exists tmp_firstoperationcounter;
create table tmp_firstoperationcounter as
select a.AFVC_AUFPL,a.AFVC_APLZL,row_number() over(partition by a.AFVC_AUFPL
order by a.AFVC_VORNR asc, a.AFVC_APLZL desc) counter 
from AFVC a;

drop table if exists tmp_firstoperationcounter_1;
create table tmp_firstoperationcounter_1 as
select * from tmp_firstoperationcounter
where counter = 1;

update fact_productionorder po
SET dd_firstoperationcounter = ifnull(t.AFVC_APLZL,0)
,po.dw_update_date = current_timestamp
from fact_productionorder po, tmp_firstoperationcounter_1 t
WHERE t.AFVC_AUFPL = po.dd_RoutingOperationNo
AND dd_firstoperationcounter <> ifnull(t.AFVC_APLZL,0); 

UPDATE fact_productionorder po
SET po.dd_tasklisttype = ifnull(AFKO_PLNTY,'Not Set')
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi 
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND po.dd_tasklisttype <> ifnull(AFKO_PLNTY,'Not Set');

UPDATE fact_productionorder po
SET po.dd_tasklistusage = ifnull(AFKO_PVERW,'Not Set')
	,po.dw_update_date = current_timestamp
	FROM fact_productionorder po,AFKO_AFPO_AUFK mhi 
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND po.dd_tasklistusage <> ifnull(AFKO_PVERW,'Not Set');

UPDATE fact_productionorder po
SET po.dd_changenumber = ifnull(AFKO_PAENR,'Not Set')
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi 
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND po.dd_changenumber <> ifnull(AFKO_PAENR,'Not Set');

UPDATE fact_productionorder po
SET po.dd_plannergroup = ifnull(AFKO_PLGRP,'Not Set')
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi 
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND po.dd_plannergroup <> ifnull(AFKO_PLGRP,'Not Set');


UPDATE fact_productionorder po
SET po.dim_dateidvalidfrom = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi , dim_date dd
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND dd.DateValue = ifnull(mhi.AFKO_PDATV,'0001-01-01')
  AND dd.CompanyCode = mhi.AUFK_BUKRS
  AND dd.plantcode_factory = mhi.AUFK_WERKS
  AND po.dim_dateidvalidfrom <> dd.dim_dateid; 
/* Octavian: EA changes */

/* Octavian: EA changes */
MERGE INTO fact_productionorder fact
USING (SELECT DISTINCT po.fact_productionorderid, FIRST_VALUE(wc.dim_workcenterid) OVER (PARTITION BY wc.objectid,wc.objecttype ORDER BY wc.objectid DESC) AS dim_workcenterid
from fact_productionorder po, AFVC a, dim_workcenter wc
WHERE po.dd_RoutingOperationNo = ifnull(a.AFVC_AUFPL,0)
AND dd_firstoperationcounter = ifnull(a.AFVC_APLZL,0)
AND wc.objectid = ifnull(convert(varchar(8),a.AFVC_ARBID), 'Not Set')) src
ON fact.fact_productionorderid = src.fact_productionorderid
WHEN MATCHED THEN UPDATE
SET fact.dim_workcenterid = src.dim_workcenterid,
dw_update_date = CURRENT_TIMESTAMP;
/* Octavian: EA changes */

/*12 May 2016  Georgiana Changes: Adding Actual Consumption field accroding to BI-2838*/

drop table if exists tmp_for_LocalCurrAmt;
create table tmp_for_LocalCurrAmt as 
select sum(mm.amt_LocalCurrAmt) sum_amt_LocalCurrAmt ,mm.dd_productionordernumber
from 
fact_materialmovement mm, 
dim_movementtype mt
where
mm.Dim_MovementTypeid=mt.Dim_MovementTypeid
and mt.MovementType in ('261','262') 
Group by mm.dd_productionordernumber;

update fact_productionorder fp
set fp.amt_actualconsumtion = t.sum_amt_LocalCurrAmt
,fp.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_productionorder fp,tmp_for_LocalCurrAmt t
where fp.dd_ordernumber = t.dd_productionordernumber
and fp.amt_actualconsumtion <> t.sum_amt_LocalCurrAmt;

drop table if exists tmp_for_LocalCurrAmt;

/*12 May 2016 End of Changes*/

/*08 Jun 2016 Georgiana EA changes: adding dd_LASTRoutingOperationNo,dd_lasttoperationcounter according to BI-2931*/

/*  Last Executed Operation Number*/
drop table if exists tmp_for_upd_LASTRoutingOperationNo;
create table tmp_for_upd_LASTRoutingOperationNo as 
select distinct mhi.AFKO_AUFNR,mhi.AFPO_POSNR,afv.AFVC_AUFPL 
FROM fact_productionorder po, AFKO_AFPO_AUFK mhi, AFVC_AFVV afv
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
and mhi.AFKO_AUFPL=afv.AFVC_AUFPL
and (afv.afvv_IEAVD is not null or afv.afvv_IEDD is not null);
  
  
UPDATE fact_productionorder po
SET po.dd_LASTRoutingOperationNo = ifnull(t.AFVC_AUFPL,0)
,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/ 
from fact_productionorder po,tmp_for_upd_LASTRoutingOperationNo t
WHERE po.dd_ordernumber = t.AFKO_AUFNR
AND po.dd_orderitemno = t.AFPO_POSNR
AND ifnull(po.dd_LASTRoutingOperationNo,0) <> ifnull(t.AFVC_AUFPL,0);

drop table if exists tmp_for_upd_LASTRoutingOperationNo;
  /*Last Executed Operation Counter*/ 

drop table if exists tmp_lastoperationcounter;
create table tmp_lastoperationcounter as
select distinct b.AFVC_AUFPL,b.AFVC_APLZL,b.AFVC_VORNR,b.afvv_IEAVD,b.afvv_IEDD,row_number() over(partition by b.AFVC_AUFPL
order by b.AFVC_VORNR desc, b.AFVC_APLZL desc) counter 
from AFVC_AFVV b
where
 b.afvv_IEAVD is not null and b.afvv_IEDD is not null;


drop table if exists tmp_lastoperationcounter_1;
create table tmp_lastoperationcounter_1 as
select * from tmp_lastoperationcounter
where counter = 1;

update fact_productionorder po
SET dd_lasttoperationcounter = ifnull(t.AFVC_APLZL,0)
,po.dw_update_date = current_timestamp
from fact_productionorder po,tmp_lastoperationcounter_1 t
WHERE t.AFVC_AUFPL = po.dd_RoutingOperationNo
AND dd_lasttoperationcounter <> ifnull(t.AFVC_APLZL,0); 

/*End of EA Changes 08 Jun 2016*/
drop table if exists tmp_lastoperationcounter;
drop table if exists tmp_lastoperationcounter_1;

/* Madalina 17 Aug 2016 - Add new field, Quantity Open */
drop table if exists tmp_upd_QuantityOpen;
create table tmp_upd_QuantityOpen as
select distinct dd_ordernumber, dd_orderitemno,
	(CASE WHEN MAX( (CASE WHEN f_prord.dd_deletionflag = 'Y' THEN 
						(CASE WHEN dd_delivered = 'X' THEN 'Closed' 
						 	ELSE (CASE WHEN dd_technicallycompleted = 'X' THEN 'Closed'
								   ELSE (CASE WHEN dd_closed = 'X' THEN 'Closed' ELSE 'Cancelled' END) END) END)
	                ELSE (CASE WHEN dd_delivered = 'X' THEN 'Closed' 
							ELSE (CASE WHEN dd_technicallycompleted= 'X' THEN 'Closed' 
										ELSE (CASE WHEN ct_GRQty= 0 THEN 'Open' ELSE (CASE WHEN (ct_OrderItemQty - ct_GRQty) <= 0 THEN 'Closed' ELSE 'Partially Open' END) END) END) END) END) ) = 'Cancelled' 
			OR MAX( (CASE WHEN f_prord.dd_deletionflag = 'Y' THEN 
						(CASE WHEN dd_delivered = 'X' THEN 'Closed' 
							ELSE (CASE WHEN dd_technicallycompleted = 'X' THEN 'Closed' 
									ELSE (CASE WHEN dd_closed = 'X' THEN 'Closed' ELSE 'Cancelled' END) END) END) 
					ELSE (CASE WHEN dd_delivered = 'X' THEN 'Closed'
							 ELSE (CASE WHEN dd_technicallycompleted= 'X' THEN 'Closed'
									 ELSE (CASE WHEN ct_GRQty= 0 THEN 'Open' ELSE (CASE WHEN (ct_OrderItemQty - ct_GRQty) <= 0 THEN 'Closed' ELSE 'Partially Open' END) END) END) END) END) ) = 'Closed'
		THEN 0
		ELSE SUM( (f_prord.ct_OrderItemQty) ) - SUM( (f_prord.ct_GRQty) )
	END) as ct_QuantityOpen
from fact_productionorder f_prord
group by dd_ordernumber, dd_orderitemno;


update fact_productionorder f_prord
set f_prord.ct_QuantityOpen = t.ct_QuantityOpen
from tmp_upd_QuantityOpen t, fact_productionorder f_prord
where 
	f_prord.dd_ordernumber = t.dd_ordernumber
	and f_prord.dd_orderitemno = t.dd_orderitemno
	and f_prord.ct_QuantityOpen <> t.ct_QuantityOpen;

drop table if exists tmp_upd_QuantityOpen;

/*Georgiana 23 Sept 2016 Added ct_basequantity accroding to BI-4215*/

UPDATE fact_productionorder po
SET  ct_basequantity = ifnull(AFKO_BMENGE ,0.000)
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE     po.dd_ordernumber = mhi.AFKO_AUFNR
       AND po.dd_orderitemno = mhi.AFPO_POSNR
	AND ct_basequantity <> ifnull(AFKO_BMENGE ,0.000);


/*Alin 14 Oct 2016 Added dd_userstatusAUFK to BI-4262*/
MERGE INTO  fact_productionorder po
USING 
(
SELECT DISTINCT po.fact_productionorderid,first_value(ifnull(tj.TJ30T_TXT04, 'Not Set')) over (partition by JEST_OBJNR, AUFK_AUFNR order by JEST_STAT asc) as dd_userstatusAUFK
FROM fact_productionorder po, JEST_JSTO_AUFK j1, tj30t tj
WHERE  po.dd_ObjectNumber = J1.JEST_OBJNR
AND po.dd_ordernumber = AUFK_AUFNR
AND J1.JSTO_STSMA = TJ.TJ30T_STSMA
AND TJ.TJ30T_ESTAT = j1.JEST_STAT
AND JEST_STAT LIKE 'E%'
AND po.dd_userstatusAUFK <> ifnull(tj.TJ30T_TXT04, 'Not Set')
) t
ON t.fact_productionorderid = po.fact_productionorderid
WHEN MATCHED THEN UPDATE
SET po.dd_userstatusAUFK = t.dd_userstatusAUFK;

/*Alin 1 Nov 2016 Added dd_systemstatusQALS to BI-4262*/
MERGE INTO  fact_inspectionlot il
USING 
(
SELECT 
--DISTINCT
 il.FACT_INSPECTIONLOTID,
 group_concat(tj.TJ02T_TXT04 SEPARATOR ' ') as dd_systemstatusQALS 
FROM fact_inspectionlot il, JEST_JSTO_QALS j1, tj02t tj
WHERE  il.dd_ObjectNumber = J1.JEST_OBJNR
AND il.dd_orderno = IFNULL(J1.QALS_AUFNR,'Not Set')
AND TJ.TJ02T_ISTAT = j1.JEST_STAT
AND JEST_STAT LIKE 'I%'
AND il.dd_systemstatusQALS <> ifnull(tj.TJ02T_TXT04, 'Not Set')
GROUP BY  il.FACT_INSPECTIONLOTID
) t
ON t.FACT_INSPECTIONLOTID = il.FACT_INSPECTIONLOTID
WHEN MATCHED THEN UPDATE
SET il.dd_systemstatusQALS = t.dd_systemstatusQALS;

/* Madalina 11 Jul 2016 - add new measures in fact_productionorder, based on AFVC_AFVV - BI-3412 */
update fact_productionorder po

set po.ct_laborFixed = ifnull(AA.AFVV_ISM01,0),
	po.dw_update_date = current_timestamp
from AFVC_AFVV AA, fact_productionorder po
where po.dd_routingoperationno = ifnull(AFVC_AUFPL,'Not Set')
	and po.dd_firstoperationcounter = ifnull(AFVC_APLZL,0)
	and po.ct_laborFixed <> ifnull(AA.AFVV_ISM01,0);

update fact_productionorder po
set po.ct_laborVariable = ifnull(AA.AFVV_ISM03,0),
	po.dw_update_date = current_timestamp
from AFVC_AFVV AA, fact_productionorder po
where po.dd_routingoperationno = ifnull(AFVC_AUFPL,'Not Set')
	and po.dd_firstoperationcounter = ifnull(AFVC_APLZL,0)
	and po.ct_laborVariable <> ifnull(AA.AFVV_ISM03,0);

update fact_productionorder po

set po.ct_machineFixed = ifnull(AA.AFVV_ISM01,0),
	po.dw_update_date = current_timestamp
from AFVC_AFVV AA, fact_productionorder po
where po.dd_lastroutingoperationno = ifnull(AFVC_AUFPL,'Not Set')
	and po.dd_lasttoperationcounter = ifnull(AFVC_APLZL,0)
	and po.ct_machineFixed <> ifnull(AA.AFVV_ISM01,0);
	
update fact_productionorder po

set po.ct_machineVariable = ifnull(AA.AFVV_ISM02,0),
	po.dw_update_date = current_timestamp
from AFVC_AFVV AA, fact_productionorder po
where po.dd_lastroutingoperationno = ifnull(AFVC_AUFPL,'Not Set')
	and po.dd_lasttoperationcounter = ifnull(AFVC_APLZL,0)
	and po.ct_machineVariable <> ifnull(AA.AFVV_ISM02,0);
	
/*START BI-5163 Alin   30.01.2017*/
UPDATE fact_productionorder po
SET po.dim_dateidtechcompldt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber and
j.JCDS_STAT = 'I0045' and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND po.dim_dateidtechcompldt <> dd.dim_dateid;

UPDATE fact_productionorder po
SET po.dim_dateidcloseddt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND j.JCDS_STAT = 'I0046' 
and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dim_dateidcloseddt <> dd.dim_dateid;

UPDATE fact_productionorder po
SET po.dim_dateidconfirmeddt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND j.JCDS_STAT = 'I0009'
and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dim_dateidconfirmeddt <> dd.dim_dateid;

UPDATE fact_productionorder po
SET po.dim_dateidchangedeletionflagdt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND j.JCDS_STAT = 'I0076' 
and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dim_dateidchangedeletionflagdt <> dd.dim_dateid;

UPDATE fact_productionorder po
SET po.dim_dateidchangedelivereddt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND j.JCDS_STAT = 'I0076' 
and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dim_dateidchangedelivereddt <> dd.dim_dateid;

UPDATE fact_productionorder po
SET po.dim_dateidchangematerialcommitteddt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND j.JCDS_STAT = 'I0340' 
and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dim_dateidchangematerialcommitteddt <> dd.dim_dateid;

UPDATE fact_productionorder po
SET po.dim_dateidchangedmaterialshortageddt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND j.JCDS_STAT = 'I0004' 
and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dim_dateidchangedmaterialshortageddt <> dd.dim_dateid;

UPDATE fact_productionorder po
SET po.dim_dateidqmtestsscheduleddt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND j.JCDS_STAT = 'E0001' 
and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dim_dateidqmtestsscheduleddt <> dd.dim_dateid;

UPDATE fact_productionorder po
SET po.dim_dateidchangeddocreleaseddt = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, jcds_aufk j
WHERE j.JCDS_OBJNR = po.dd_ObjectNumber
AND dd.DateValue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND dd.CompanyCode = j.AUFK_BUKRS
AND dd.plantcode_factory = j.AUFK_WERKS
AND j.JCDS_STAT = 'I0002' 
and j.JCDS_INACT is NULL
and j.jcds_chgnr = '1'
AND po.dim_dateidchangeddocreleaseddt <> dd.dim_dateid;
/*END BI-5163 Alin*/

/*START 24 feb 2017 - Alin Gh BI-5573*/
UPDATE fact_productionorder po
SET dd_overheadkeydescription = ifnull(tk.TKZSLT_ZTEXT, 'Not Set')
,dw_update_date=current_timestamp
FROM fact_productionorder po, TKZSLT tk
WHERE po.dd_overheadkey = tk.TKZSLT_ZSCHL
and dd_overheadkeydescription <> ifnull(tk.TKZSLT_ZTEXT, 'Not Set');
/*END 24 feb 2017 - Alin Gh BI-5573*/

/* Liviu Ionescu - APP-6092 add creation date */
UPDATE fact_productionorder po
SET po.dim_dateidcreation = dd.dim_dateid
	,po.dw_update_date = current_timestamp
FROM fact_productionorder po, dim_date dd, CAUFV c
WHERE c.CAUFV_AUFNR = po.dd_ordernumber
AND dd.DateValue = ifnull(c.CAUFV_ERDAT,'0001-01-01')
AND dd.CompanyCode = c.CAUFV_BUKRS
AND dd.plantcode_factory = c.CAUFV_WERKS
AND po.dim_dateidcreation <> dd.dim_dateid;

/*16 Jun 2017 Georgiana changes according to APP-6505*/

UPDATE fact_productionorder po
   SET po.ct_totalplannedorderqty = ifnull(mhi.AFPO_PGMNG,0)
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
  AND po.dd_orderitemno = mhi.AFPO_POSNR
  AND po.ct_totalplannedorderqty <> ifnull(mhi.AFPO_PGMNG,0);
  

drop table if exists tmp_forfirstprocessorderdate;
create table tmp_forfirstprocessorderdate as
select distinct po.dd_ordernumber,po.dd_orderitemno,to_date(first_value(c.CDPOS_VALUE_OLD) over (partition by po.dd_ordernumber order by c.CDPOS_CHANGENR asc),'YYYYMMDD') as firstprocessorderdate 
from fact_productionorder po,CDPOS_AFKO c
where po.dd_ordernumber=ifnull(right(c.CDPOS_OBJECTID,12),'Not Set')
and c.CDPOS_FNAME='GLTRP';

update fact_productionorder po
set dim_dateidfirstprocessordrdateid=ifnull(dt.dim_dateid,1)
from fact_productionorder po,tmp_forfirstprocessorderdate t, dim_date dt,
dim_plant pl
where po.dd_ordernumber=t.dd_ordernumber
and po.dd_orderitemno=t.dd_orderitemno
and po.dim_plantid=pl.dim_plantid
and pl.companycode=dt.companycode
and pl.plantcode=dt.plantcode_factory
and dt.datevalue=ifnull(firstprocessorderdate,'0001-01-01') 
and dim_dateidfirstprocessordrdateid<>ifnull(dt.dim_dateid,1);

UPDATE fact_productionorder po
SET po.dim_dateidfirstprocessordrdateid = sf.dim_dateid
   	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From fact_productionorder po,AFKO_AFPO_AUFK mhi,dim_date sf
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND sf.DateValue = AFKO_GLTRP 
AND mhi.AUFK_BUKRS = sf.CompanyCode 
and sf.plantcode_factory = mhi.AUFK_WERKS
AND po.dim_dateidfirstprocessordrdateid=1;

drop table if exists tmp_forfirstprocessorderqty;
create table tmp_forfirstprocessorderqty as
select distinct po.dd_ordernumber,po.dd_orderitemno,first_value(c.CDPOS_VALUE_OLD) over (partition by po.dd_ordernumber order by c.CDPOS_CHANGENR asc) as firstprocessorderqty
from fact_productionorder po,CDPOS_AFKO c
where po.dd_ordernumber=ifnull(right(c.CDPOS_OBJECTID,12),'Not Set')
and c.CDPOS_FNAME='GAMNG';

update fact_productionorder po
set ct_firstprocessorderqty=ifnull(firstprocessorderqty,0)
from fact_productionorder po,tmp_forfirstprocessorderqty t
where po.dd_ordernumber=t.dd_ordernumber
and po.dd_orderitemno=t.dd_orderitemno
and ct_firstprocessorderqty <> ifnull(firstprocessorderqty,0);

update fact_productionorder po
set ct_firstprocessorderqty=ifnull(AFKO_GAMNG,0)
from fact_productionorder po,AFKO_AFPO_AUFK mhi
WHERE po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
and ct_firstprocessorderqty =0;

/*start APP-7409 Alin 11 sept 17*/
merge into fact_productionorder f
using(
select distinct fact_productionorderid, sum(IFNULL(r.RESB_AUSCH, 0)) as resb_ausch
from fact_productionorder f, RESB r
where f.dd_ordernumber = ifnull(RESB_AUfnr, 'Not Set')
and r.RESB_BDART = 'AR'
group by fact_productionorderid) t
on t.fact_productionorderid = f.fact_productionorderid  
when matched then update set
amt_component_scrap = t.RESB_AUSCH
where amt_component_scrap <> t.RESB_AUSCH;

merge into fact_productionorder f
using(
select distinct fact_productionorderid, sum(IFNULL(r.RESB_AVOAU, 0)) as resb_avoau
from fact_productionorder f, RESB r
where f.dd_ordernumber = ifnull(RESB_AUfnr, 'Not Set')
and r.RESB_BDART = 'AR'
group by fact_productionorderid) t
on t.fact_productionorderid = f.fact_productionorderid  
when matched then update set
amt_operation_scrap = t.resb_avoau
where amt_operation_scrap <> t.resb_avoau;


/*Alin APP-6528 22 Jan*/
--COSTING LOT SIZE
drop table if exists tmp_for_calc_cost_lot_size;
create table tmp_for_calc_cost_lot_size as
select distinct d.partnumber, d.plant, IFNULL(CKIS_POSNR, 0) AS ORDERITEMNO, 
AVG(KEKO_LOSGR)  OVER (PARTITION BY D.PARTNUMBER, D.PLANT, CKIS_KADKY ORDER BY D.PARTNUMBER, D.PLANT, CKIS_KADKY) as ct_cost_loc_size
FROM CKIS C, KEKO K, dim_part d
WHERE
c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and c.CKIS_KADKY in  ('2018-01-01')
and d.plant = ifnull(KEKO_WERKS, 'Not Set')
and d.partnumber = ifnull(KEKO_MATNR, 'Not Set');

DROP TABLE IF EXISTS tmp_for_calc_cost_lot_size2;
CREATE TABLE tmp_for_calc_cost_lot_size2 AS
select distinct f.fact_productionorderid, dd_ordernumber, 
d.partnumber, d.plant, 
first_value(t.ct_cost_loc_size) over(partition by  f.fact_productionorderid order by orderitemno desc) as ct_cost_loc_size
from fact_productionorder f, tmp_for_calc_cost_lot_size t, dim_part d
where f.dim_partidheader = d.dim_partid
and t.plant = d.plant
and t.partnumber = d.partnumber;

update fact_productionorder f
set ct_costing_lot_size = ifnull(t.ct_cost_loc_size, 0)
from fact_productionorder f, tmp_for_calc_cost_lot_size2 t
where f.fact_productionorderid = t.fact_productionorderid;

DROP TABLE IF EXISTS tmp_for_calc_cost_lot_size;
DROP TABLE IF EXISTS tmp_for_calc_cost_lot_size2;


--NEW TTI FACTOR
DROP TABLE if exists tmp_max_date;
create table tmp_max_date as
select distinct 
ifnull(KEKO_WERKS, 'Not Set') as plant, 
ifnull(KEKO_MATNR, 'Not Set') as part,
max(c.CKIS_KADKY)  as KADKY
FROM CKIS C, KEKO K
where c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and year(c.CKIS_KADKY) = year(current_date)
GROUP BY 1,2;

drop table if exists tmp_for_tti_calc;
create table tmp_for_tti_calc as 

select  partnumber, plant, costing_lot_size,
sum(sum_wrtfw_kpf) over (partition by partnumber, plant) as sum_wrtfw_kpf
from
(
select  d.partnumber, d.plant, --IFNULL(CKIS_POSNR, 0) AS ORDERITEMNO,
ifnull(CKIS_WRTFW_KPF, 0) AS sum_wrtfw_kpf,
ifnull(KEKO_LOSGR, 0) as costing_lot_size
FROM CKIS C, KEKO K, dim_part d, tmp_max_date t
WHERE
c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and c.CKIS_KADKY in  ('2018-01-01')
and d.plant = ifnull(KEKO_WERKS, 'Not Set')
and d.partnumber = ifnull(KEKO_MATNR, 'Not Set')

and d.plant = t.plant
and d.partnumber = t.part
and c.CKIS_KADKY = t.KADKY
)t;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION;
CREATE TABLE TMP_2_TTI_CALCULATION AS
select distinct fact_productionorderid, dd_ordernumber, 
d.partnumber, d.plant, 
sum_wrtfw_kpf/costing_lot_size as tt_calc
from fact_productionorder f, tmp_for_tti_calc t, dim_part d
where f.dim_partidheader = d.dim_partid
and t.plant = d.plant
and t.partnumber = d.partnumber;

update fact_productionorder f
set ct_tti_new_factor = ifnull(t.tt_calc, 0)
from fact_productionorder f, TMP_2_TTI_CALCULATION t
where f.fact_productionorderid = t.fact_productionorderid;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION;
drop table if exists tmp_for_tti_calc;
DROP TABLE if exists tmp_max_date;

--------PRODUCTION ORDER FIXED AND VARIABLE TTIs
--FIXED TTI FACTOR
drop table if exists tmp_for_tti_calc_fixed;
create table tmp_for_tti_calc_fixed as 
select distinct partnumber, plant, /*costing_lot_size*/
sum(sum_wrtfw_kpf) over (partition by partnumber, plant) as sum_wrtfw_kpf
from
(
select  d.partnumber, d.plant, /*IFNULL(CKIS_POSNR, 0) AS ORDERITEMNO*/
ifnull(CKIS_WRTFW_KPF, 0) AS sum_wrtfw_kpf
/*ifnull(KEKO_LOSGR, 0) as costing_lot_size*/
FROM CKIS C, KEKO K, dim_part d
WHERE
c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and c.CKIS_KADKY in  ('2018-01-01')
AND c.ckis_psknz = 'F'
and d.plant = ifnull(KEKO_WERKS, 'Not Set')
and d.partnumber = ifnull(KEKO_MATNR, 'Not Set')
)t;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_fixed;
CREATE TABLE TMP_2_TTI_CALCULATION_fixed AS
select distinct fact_productionorderid, dd_ordernumber, 
d.partnumber, d.plant, 
sum_wrtfw_kpf as tt_calc
from fact_productionorder f, tmp_for_tti_calc_fixed t, dim_part d
where f.dim_partidheader = d.dim_partid
and t.plant = d.plant
and t.partnumber = d.partnumber;

update fact_productionorder f
set ct_fixed_tti = ifnull(t.tt_calc, 0)
from fact_productionorder f, TMP_2_TTI_CALCULATION_fixed t
where f.fact_productionorderid = t.fact_productionorderid;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_fixed;
drop table if exists tmp_for_tti_calc_fixed;



--VARIABLE TTI FACTOR
drop table if exists tmp_for_tti_calc_variable;
create table tmp_for_tti_calc_variable as 
select distinct partnumber, plant, costing_lot_size,
sum(sum_wrtfw_kpf) over (partition by partnumber, plant) as sum_wrtfw_kpf
from
(
select  d.partnumber, d.plant, /*IFNULL(CKIS_POSNR, 0) AS ORDERITEMNO*/
ifnull(CKIS_WRTFW_KPF, 0) AS sum_wrtfw_kpf,
ifnull(KEKO_LOSGR, 0) as costing_lot_size
FROM CKIS C, KEKO K, dim_part d
WHERE
c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and c.CKIS_KADKY in  ('2018-01-01')
AND c.ckis_psknz IS NULL
and d.plant = ifnull(KEKO_WERKS, 'Not Set')
and d.partnumber = ifnull(KEKO_MATNR, 'Not Set')
)t;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_variable;
CREATE TABLE TMP_2_TTI_CALCULATION_variable AS
select distinct fact_productionorderid, dd_ordernumber, 
d.partnumber, d.plant, 
sum_wrtfw_kpf/costing_lot_size as tt_calc
from fact_productionorder f, tmp_for_tti_calc_variable t, dim_part d
where f.dim_partidheader = d.dim_partid
and t.plant = d.plant
and t.partnumber = d.partnumber;

update fact_productionorder f
set ct_variable_tti = ifnull(t.tt_calc, 0)
from fact_productionorder f, TMP_2_TTI_CALCULATION_variable t
where f.fact_productionorderid = t.fact_productionorderid;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_variable;
drop table if exists tmp_for_tti_calc_variable;
