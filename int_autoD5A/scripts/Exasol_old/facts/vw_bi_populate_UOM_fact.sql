/* Add entry in number_fountain table for fact_unitofmeasure */
delete from number_fountain m where m.table_name = 'fact_unitofmeasure';
   
insert into number_fountain
select 	'fact_unitofmeasure',
	ifnull(max(d.fact_unitofmeasureid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_unitofmeasure d
where d.fact_unitofmeasureid <> 1;


drop table if exists tmp_mara_marm;
create table tmp_mara_marm
as
select mr.MARA_MATNR,
	mr.MARA_MEINS,
	 mr.MARA_NTGEW,
	mm.MARM_MATNR, 
	mm.MARM_MEINH,
	mm.MARM_UMREZ,
	mm.MARM_UMREN,
	mm.MARM_LAENG,
	mm.MARM_BREIT,
	mm.MARM_HOEHE,
	mm.MARM_VOLUM,
	mm.MARM_VOLEH,
	mm.MARM_BRGEW,
	mm.MARM_GEWEI,
	mm.MARM_MESUB,
	mm.MARM_MEABM
from MARA mr, MARM mm
where mr.MARA_MATNR = mm.MARM_MATNR;


insert into fact_unitofmeasure
(
fact_unitofmeasureid, 
dd_partnumber,
dd_alternativeunitofmeasure,
dd_numerator,
dd_denominator,
ct_length,
ct_width,
ct_height,
ct_volume,
amt_grossweight,
ct_netweight,
ct_altnetweight
)
select (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_unitofmeasure') + row_number() over(ORDER BY '') as fact_unitofmeasureid,
ifnull(t.MARM_MATNR, 'Not Set') as dd_partnumber,
ifnull(t.MARM_MEINH, 'Not Set') as dd_alternativeunitofmeasure,
ifnull(t.MARM_UMREZ, 0) as dd_numerator,
ifnull(t.MARM_UMREN, 1) as dd_denominator,
ifnull(t.MARM_LAENG, 0) as ct_length,
ifnull(t.MARM_BREIT, 0) as ct_width,
ifnull(t.MARM_HOEHE, 0) as ct_height,
ifnull(t.MARM_VOLUM, 0) as ct_volume,
ifnull(t.MARM_BRGEW, 0) as amt_grossweight,
ifnull(t.MARA_NTGEW, 0) as ct_netweight,
case
  when t.MARM_BRGEW is null or t.MARM_BRGEW = 0  then 0
  else ifnull (((t.marm_umrez / t.marm_umren) * t.MARA_NTGEW), 0)
end                    as ct_altnetweight
from tmp_mara_marm t
where not exists (select 1 
					from fact_unitofmeasure f
					where f.dd_partnumber = ifnull(t.MARM_MATNR, 'Not Set')
					and f.dd_alternativeunitofmeasure = ifnull(t.MARM_MEINH, 'Not Set'));
					
					
					
/* dim_alternativeunitofmeasureid */
update fact_unitofmeasure f
set f.dim_alternativeunitofmeasureid = uom.dim_unitofmeasureid,
	dw_update_date = current_timestamp
from fact_unitofmeasure f, dim_unitofmeasure uom
where f.dd_alternativeunitofmeasure = uom.uom
and f.dim_alternativeunitofmeasureid <> uom.dim_unitofmeasureid;

/* dim_volumeunitid MARM_VOLEH */ 
update fact_unitofmeasure f
set f.dim_volumeunitid = uom.dim_unitofmeasureid,
	dw_update_date = current_timestamp
from fact_unitofmeasure f, dim_unitofmeasure uom, tmp_mara_marm t
where ifnull(t.MARM_MATNR, 'Not Set') = f.dd_partnumber
and ifnull(t.MARM_MEINH, 'Not Set') = f.dd_alternativeunitofmeasure
and ifnull(t.MARM_VOLEH, 'Not Set') = uom.uom
and f.dim_volumeunitid <> uom.dim_unitofmeasureid;

/* dim_weightunitid MARM_GEWEI */
update fact_unitofmeasure f
set f.dim_weightunitid = uom.dim_unitofmeasureid,
	dw_update_date = current_timestamp
from fact_unitofmeasure f, dim_unitofmeasure uom, tmp_mara_marm t
where ifnull(t.MARM_MATNR, 'Not Set') = f.dd_partnumber
and ifnull(t.MARM_MEINH, 'Not Set') = f.dd_alternativeunitofmeasure
and ifnull(t.MARM_GEWEI, 'Not Set') = uom.uom
and f.dim_weightunitid <> uom.dim_unitofmeasureid;

/* dim_lowerlevelunitid MARM_MESUB */
update fact_unitofmeasure f
set f.dim_lowerlevelunitid = uom.dim_unitofmeasureid,
	dw_update_date = current_timestamp
from fact_unitofmeasure f, dim_unitofmeasure uom, tmp_mara_marm t
where ifnull(t.MARM_MATNR, 'Not Set') = f.dd_partnumber
and ifnull(t.MARM_MEINH, 'Not Set') = f.dd_alternativeunitofmeasure
and ifnull(t.MARM_MESUB, 'Not Set') = uom.uom
and f.dim_lowerlevelunitid <> uom.dim_unitofmeasureid;

/* dim_unitofdimensionid MARM_MEABM */
update fact_unitofmeasure f
set f.dim_unitofdimensionid = uom.dim_unitofmeasureid,
	dw_update_date = current_timestamp
from fact_unitofmeasure f, dim_unitofmeasure uom, tmp_mara_marm t
where ifnull(t.MARM_MATNR, 'Not Set') = f.dd_partnumber
and ifnull(t.MARM_MEINH, 'Not Set') = f.dd_alternativeunitofmeasure
and ifnull(t.MARM_MEABM, 'Not Set') = uom.uom
and f.dim_unitofdimensionid <> uom.dim_unitofmeasureid;

/* dim_baseunitofmeasureid MARA_MEINS */
update fact_unitofmeasure f
set f.dim_baseunitofmeasureid = uom.dim_unitofmeasureid,
	dw_update_date = current_timestamp
from fact_unitofmeasure f, dim_unitofmeasure uom, tmp_mara_marm t
where ifnull(t.MARM_MATNR, 'Not Set') = f.dd_partnumber
and ifnull(t.MARM_MEINH, 'Not Set') = f.dd_alternativeunitofmeasure
and ifnull(t.MARA_MEINS, 'Not Set') = uom.uom
and f.dim_baseunitofmeasureid <> uom.dim_unitofmeasureid;

/* Ana Rusu - Start APP-8736 - field Net Weight AltUoM change request */

update fact_unitofmeasure f
set f.ct_altnetweight = case
                         when t.MARM_BRGEW is null or t.MARM_BRGEW = 0  then 0
                         else ifnull (((t.marm_umrez / t.marm_umren) * t.MARA_NTGEW), 0)
                        end , 
	dw_update_date = current_timestamp
from tmp_mara_marm t, fact_unitofmeasure f
where f.dd_partnumber=ifnull(t.MARM_MATNR,'Not Set')
and f.dd_alternativeunitofmeasure=ifnull(t.MARM_MEINH,'Not Set');

/* End - APP-8736 - field Net Weight AltUoM change request */

/*Alin May 31st missing updates APP9712*/
update fact_unitofmeasure f
set f.dd_numerator = ifnull(t.MARM_UMREZ, 0)
from fact_unitofmeasure f, tmp_mara_marm t
where f.dd_partnumber = ifnull(t.MARM_MATNR, 'Not Set')
and f.dd_alternativeunitofmeasure = ifnull(t.MARM_MEINH, 'Not Set')
and f.dd_numerator <> ifnull(t.MARM_UMREZ, 0);

update fact_unitofmeasure f
set f.dd_denominator = ifnull(t.MARM_UMREN, 1)
from fact_unitofmeasure f, tmp_mara_marm t
where f.dd_partnumber = ifnull(t.MARM_MATNR, 'Not Set')
and f.dd_alternativeunitofmeasure = ifnull(t.MARM_MEINH, 'Not Set')
and f.dd_denominator <> ifnull(t.MARM_UMREN, 1);

update fact_unitofmeasure f
set f.ct_length = ifnull(t.MARM_LAENG, 0)
from fact_unitofmeasure f, tmp_mara_marm t
where f.dd_partnumber = ifnull(t.MARM_MATNR, 'Not Set')
and f.dd_alternativeunitofmeasure = ifnull(t.MARM_MEINH, 'Not Set')
and f.ct_length <> ifnull(t.MARM_LAENG, 0);

update fact_unitofmeasure f
set f.ct_width = ifnull(t.MARM_BREIT, 0)
from fact_unitofmeasure f, tmp_mara_marm t
where f.dd_partnumber = ifnull(t.MARM_MATNR, 'Not Set')
and f.dd_alternativeunitofmeasure = ifnull(t.MARM_MEINH, 'Not Set')
and f.ct_width <> ifnull(t.MARM_BREIT, 0);

update fact_unitofmeasure f
set f.ct_height = ifnull(t.MARM_HOEHE, 0)
from fact_unitofmeasure f, tmp_mara_marm t
where f.dd_partnumber = ifnull(t.MARM_MATNR, 'Not Set')
and f.dd_alternativeunitofmeasure = ifnull(t.MARM_MEINH, 'Not Set')
and f.ct_height <> ifnull(t.MARM_HOEHE, 0);

update fact_unitofmeasure f
set f.ct_volume = ifnull(t.MARM_VOLUM, 0)
from fact_unitofmeasure f, tmp_mara_marm t
where f.dd_partnumber = ifnull(t.MARM_MATNR, 'Not Set')
and f.dd_alternativeunitofmeasure = ifnull(t.MARM_MEINH, 'Not Set')
and f.ct_volume <> ifnull(t.MARM_VOLUM, 0);

/* Octavian Stepan - 23-MAR-2018 - Start APP-9218 - Gross weight not aligned with SAP */

UPDATE fact_unitofmeasure f
   SET f.amt_grossweight = IFNULL(t.MARM_BRGEW, 0) 
  FROM tmp_mara_marm      t,
       fact_unitofmeasure f
 WHERE f.dd_partnumber                = IFNULL(t.MARM_MATNR, 'Not Set')
   AND f.dd_alternativeunitofmeasure  = IFNULL(t.MARM_MEINH, 'Not Set')
   AND f.amt_grossweight             <> IFNULL(t.MARM_BRGEW, 0);

/* End - APP-9218 - Gross weight not aligned with SAP */

delete from fact_unitofmeasure f
where not exists (select 1
                                from tmp_mara_marm t
                                where f.dd_partnumber = ifnull(t.MARM_MATNR, 'Not Set')
                                and f.dd_alternativeunitofmeasure = ifnull(t.MARM_MEINH, 'Not Set'));

