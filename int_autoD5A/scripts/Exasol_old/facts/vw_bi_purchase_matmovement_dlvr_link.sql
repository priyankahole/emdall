
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   02 Sep 2014      Hiten	1.23              Fix for return/negative records 	  */
/*   18 May 2014      Hiten	1.17              Fix for duplicate records 	  */
/*   29 Sep 2013      Lokesh	1.1               Curr and Exchange rate changes	  */
/*   27 Sep 2013      Lokesh	1.0  		  Existing version taken from CVS */
/******************************************************************************************************************/

DROP TABLE IF EXISTS var_pToleranceDays;
CREATE TABLE var_pToleranceDays
(
pType  VARCHAR(10),
pToleranceDays INTEGER
);

INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('EARLY',0);
INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('LATE',0);

UPDATE var_pToleranceDays
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.early'), 0)
WHERE pType ='EARLY';
			  
UPDATE var_pToleranceDays			  
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.late'), 0)
WHERE pType ='LATE';

DROP TABLE IF EXISTS tmp_Purchase_uomvalues;
CREATE TABLE tmp_Purchase_uomvalues AS 
SELECT distinct p.dd_documentno,p.dd_documentitemno,duom.uom
from fact_purchase p 
inner join dim_unitofmeasure duom
on ifnull(p.dim_unitofmeasureid, 1) = ifnull(duom.dim_unitofmeasureid, 1);

/*drop table if exists tmp1_dim_batch
create table tmp1_dim_batch as
select distinct dim_batchid,batchnumber,partnumber,plantcode,activityfactorkg,activityfactorl from dim_batch
where plantcode <>'Not Set'*/

DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink;
CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
    SELECT f.Fact_materialmovementid,
		f.dd_MaterialDocNo,
		f.dd_MaterialDocItemNo,
		f.dd_MaterialDocYear,
		f.dd_DocumentNo,
		f.dd_DocumentItemNo,
		f.dd_DocumentScheduleNo,
		round(cast(f.ct_Quantity as decimal(18,5)) *(case when buom.uom = 'KG' and f.dim_batchid <> 1 and activityfactorkg <> 0 then activityfactorkg
		                                   when buom.uom = 'L' and f.dim_batchid <> 1 and activityfactorl <> 0 then activityfactorl     
										   else ifnull(buc.marm_umrez,1)/ifnull(buc.marm_umren,1)*ifnull(tpuc.marm_umren,1)/ifnull(tpuc.marm_umrez,1) end),2) ct_Quantity,
		round(cast(f.ct_QtyEntryUOM as decimal(18,5)) * (case when uuom.uom = 'KG' and f.dim_batchid <> 1 and activityfactorkg <> 0 then activityfactorkg
		                                   when uuom.uom = 'L' and f.dim_batchid <> 1  and activityfactorl <> 0 then activityfactorl     
										   else ifnull(uuc.marm_umrez,1)/ifnull(uuc.marm_umren,1)*ifnull(tpuc.marm_umren,1)/ifnull(tpuc.marm_umrez,1) end),2)  ct_QtyEntryUOM,
		f.Dim_MovementTypeid,
		f.dim_DateIDPostingDate,
		f.dd_debitcreditid,
		f.Dim_AfsSizeId,
		f.Fact_materialmovementid Fact_mmid_ref, 
		dp.DateValue PostingDate,
		f.dim_uomunitofentryid,
		f.dim_Partid,
		case when f.amt_LocalCurrAmt <> 0.00000000 and f.ct_QtyEntryUOM <> 0.00000000 
			then (f.amt_LocalCurrAmt / f.ct_QtyEntryUOM) 
			else 0.00000000 
		end mm_UnitPrice,
		ROW_NUMBER() OVER(PARTITION BY f.dd_DocumentNo, f.dd_DocumentItemNo, f.Dim_AfsSizeId
				  ORDER BY dp.DateValue, f.dd_MaterialDocNo, f.dd_MaterialDocItemNo, dd.DateValue, f.dd_DocumentScheduleNo) RowSeqNo
    FROM fact_materialmovement f 
	 INNER JOIN dim_unitofmeasure uom ON f.dim_unitofmeasureorderunitid = uom.dim_unitofmeasureid
         INNER JOIN dim_Part dp1 ON ifnull(f.dim_Partid, 1) = ifnull(dp1.dim_Partid, 1)
        inner join dim_movementtype mt on ifnull(f.Dim_MovementTypeid,1) = ifnull(mt.Dim_MovementTypeid,1)
        inner join dim_date dp on ifnull(dp.Dim_Dateid, 1) = ifnull(f.dim_DateIDPostingDate, 1)
        inner join dim_date dd on ifnull(dd.Dim_Dateid, 1) = ifnull(f.Dim_DateIdDelivery, 1)
		inner join dim_unitofmeasure buom on f.dim_unitofmeasureid = buom.dim_unitofmeasureid
		inner join dim_batch dbat on ifnull(f.dim_batchid, 1) = ifnull(dbat.dim_batchid, 1)
		inner join dim_unitofmeasure uuom on  f.dim_uomunitofentryid = uuom.dim_unitofmeasureid
	LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	LEFT JOIN MARM buc on buc.marm_matnr = dp1.partnumber and buc.marm_meinh = buom.uom
	LEFT JOIN MARM uuc on uuc.marm_matnr = dp1.partnumber and uuc.marm_meinh = uuom.uom
	LEFT JOIN tmp_Purchase_uomvalues tpu on f.dd_DocumentNo = tpu.dd_documentno and f.dd_documentitemno = tpu.dd_documentitemno
	LEFT JOIN MARM tpuc on tpuc.marm_matnr = dp1.partnumber and tpuc.marm_meinh = tpu.uom
    WHERE mt.MovementType in ('101','102','122','123','161','162') 
        and f.Dim_DateIdDelivery = 1 
        and exists (select 1 from fact_purchase fp 
                    where f.dd_DocumentNo = fp.dd_DocumentNo and f.dd_DocumentItemNo = fp.dd_DocumentItemNo
				and ifnull(f.Dim_AfsSizeId, 1) = ifnull(fp.Dim_AfsSizeId, 1));

DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink1;
CREATE TABLE fact_materialmovement_tmp_dlvrlink1 AS
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_DocumentScheduleNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       ifnull(f.Dim_MovementTypeid, 1) as Dim_MovementTypeid,
       ifnull(f.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
	f.dim_uomunitofentryid,
	ifnull(f.dim_Partid,1) as dim_Partid,
	f.mm_UnitPrice,
       sum(ifnull(f1.ct_QtyEntryUOM,0)) - f.ct_QtyEntryUOM  CumuMMGRQty,
       f.PostingDate,
       f.RowSeqNo
  FROM fact_materialmovement_tmp_dlvrlink f
	inner join fact_materialmovement_tmp_dlvrlink f1
  		on  (f1.dd_DocumentNo = f.dd_DocumentNo 
			and f1.dd_DocumentItemNo = f.dd_DocumentItemNo 
			and ifnull(f1.Dim_AfsSizeId, 1) = ifnull(f.Dim_AfsSizeId, 1))
  WHERE f.RowSeqNo >= f1.RowSeqNo
  GROUP BY f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_DocumentScheduleNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
	f.dim_uomunitofentryid,
	f.dim_Partid,
	f.mm_UnitPrice,
       f.PostingDate,
       f.RowSeqNo;

DROP TABLE IF EXISTS tmp_fact_purch_001;
CREATE TABLE tmp_fact_purch_001 AS
select f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.ct_scheduledqty_merck OrderQty,
	f.ct_ReceivedQty DlvrReceivedQty,
	(f.ct_scheduledqty_merck - f.ct_ReceivedQty) OpenQty,
	f.Dim_DateidDelivery dim_DeliveryDate,
	dd.DateValue DeliveryDate,
	f.Dim_DateidStatDelivery dim_StatDeliveryDate,
	f.Dim_UnitOfMeasureid,
	row_number() over (partition by f.dd_DocumentNo,f.dd_DocumentItemNo,f.Dim_AfsSizeId  
				order by dd.DateValue, f.dd_ScheduleNo) as RowSeqNum
from fact_purchase f inner join dim_date dd on ifnull(dd.Dim_Dateid, 1) = ifnull(f.Dim_DateidDelivery, 1)
where exists (select 1 from fact_materialmovement f1 
			inner join dim_movementtype mt1 on ifnull(f1.Dim_MovementTypeid,1) = ifnull(mt1.Dim_MovementTypeid,1)
	    where f.dd_DocumentNo = f1.dd_DocumentNo and f.dd_DocumentItemNo = f1.dd_DocumentItemNo
		and ifnull(f.Dim_AfsSizeId, 1) = ifnull(f1.Dim_AfsSizeId, 1)
		and f1.Dim_DateIdDelivery = 1 and mt1.MovementType in ('101','102','122','123','161','162'));

DROP TABLE IF EXISTS tmp_fact_purch_002;
CREATE TABLE tmp_fact_purch_002 AS
select f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.OrderQty,
	f.DlvrReceivedQty,
	f.OpenQty,
	f.dim_DeliveryDate,
	f.DeliveryDate,
	f.dim_StatDeliveryDate,
	f.Dim_UnitOfMeasureid,
	sum(ifnull(b.DlvrReceivedQty,0)) CumuDlvrReceivedQty,
	f.RowSeqNum
from tmp_fact_purch_001 f 
	inner join tmp_fact_purch_001 b 
	on f.dd_DocumentNo = b.dd_DocumentNo and f.dd_DocumentItemNo = b.dd_DocumentItemNo and ifnull(f.Dim_AfsSizeId, 1) = ifnull(b.Dim_AfsSizeId, 1)
where f.RowSeqNum >= b.RowSeqNum
group by f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.OrderQty,
	f.DlvrReceivedQty,
	f.OpenQty,
	f.dim_DeliveryDate,
	f.DeliveryDate,
	f.dim_StatDeliveryDate,
	f.Dim_UnitOfMeasureid,
	f.RowSeqNum;


DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink;
CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       ifnull(f.Dim_MovementTypeid,1) as Dim_MovementTypeid,
       ifnull(f.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref, 
	ifnull(f.dim_Partid,1) as dim_Partid,
	f.dim_uomunitofentryid,
	ifnull(po.Dim_UnitOfMeasureid, 1) as Dim_UnitOfMeasureid,
	po.dd_ScheduleNo ScheduleLineNo, 
	po.OrderQty,
	po.DlvrReceivedQty,
	po.OpenQty,
	po.dim_DeliveryDate,
	po.DeliveryDate,
	po.dim_StatDeliveryDate,
	po.CumuDlvrReceivedQty,
	po.CumuDlvrReceivedQty - po.DlvrReceivedQty CumuDlvrRcvdQtyPrev,
	row_number() over (partition by f.Fact_materialmovementid order by po.DeliveryDate, po.dd_ScheduleNo) as "rownum",
	f.CumuMMGRQty,
       f.PostingDate,
	f.mm_UnitPrice,
       f.dd_DocumentScheduleNo
FROM tmp_fact_purch_002 po 
	inner join fact_materialmovement_tmp_dlvrlink1 f 
		on po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and ifnull(po.Dim_AfsSizeId, 1) = ifnull(f.Dim_AfsSizeId,1)
WHERE (((po.CumuDlvrReceivedQty > f.CumuMMGRQty) or (po.CumuDlvrReceivedQty <= f.CumuMMGRQty and f.ct_QtyEntryUOM < 0)) 
	/*and (f.CumuMMGRQty + f.ct_QtyEntryUOM) > (po.CumuDlvrReceivedQty - po.DlvrReceivedQty) *** removed for return calc correction ***/

	and (((f.CumuMMGRQty + f.ct_QtyEntryUOM) >= (po.CumuDlvrReceivedQty - po.DlvrReceivedQty) and f.ct_QtyEntryUOM > 0)
			or ((f.CumuMMGRQty + f.ct_QtyEntryUOM) < po.CumuDlvrReceivedQty and f.ct_QtyEntryUOM < 0)) 
	)
      or (select count(*) from fact_purchase po1 
	    where po1.dd_DocumentNo = po.dd_DocumentNo and po1.dd_DocumentItemNo = po.dd_DocumentItemNo and ifnull(po1.Dim_AfsSizeId, 1) = ifnull(po.Dim_AfsSizeId, 1)) = 1;
  
  
 DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink1;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink1 as
  SELECT distinct cast(0 as bigint) Fact_materialmovementid,
        m.dd_MaterialDocNo,
        m.dd_MaterialDocItemNo,
        m.dd_MaterialDocYear,
        m.dd_DocumentNo,
        m.dd_DocumentItemNo,
        m.ct_Quantity ct_Quantity,
        m.ct_QtyEntryUOM  ct_QtyEntryUOM,
        ifnull(m.Dim_MovementTypeid,1) as Dim_MovementTypeid,
        ifnull(m.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
        m.dd_debitcreditid,
        m.Dim_AfsSizeId,
        m.Fact_mmid_ref,
        m.ScheduleLineNo,
        m.OrderQty OrderQty,
        m.DlvrReceivedQty  DlvrReceivedQty,
        m.OpenQty OpenQty,
        m.dim_DeliveryDate,
        m.dim_StatDeliveryDate,
	m.DeliveryDate,
        m.CumuDlvrReceivedQty CumuDlvrReceivedQty,
	m.CumuDlvrRcvdQtyPrev  CumuDlvrRcvdQtyPrev,
        m.CumuMMGRQty  CumuMMGRQty,
        m.PostingDate,
	ifnull(m.dim_Partid,1) as dim_Partid,
	m.mm_UnitPrice,
	m.dim_uomunitofentryid,
	m."rownum"
  FROM fact_materialmovement_tmp_dlvrlink m 
        INNER JOIN dim_Part dp1 ON ifnull(m.dim_Partid, 1)  = ifnull(dp1.dim_Partid, 1)
	INNER JOIN dim_unitofmeasure uom ON m.dim_uomunitofentryid = uom.dim_unitofmeasureid
	LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
	LEFT JOIN tmp_Purchase_uomvalues t1 on t1.dd_DocumentNo = m.dd_DocumentNo and t1.dd_DocumentItemNo = m.dd_DocumentItemNo
  where m."rownum" > 1;

INSERT INTO fact_materialmovement_tmp_dlvrlink1
SELECT m.Fact_materialmovementid,
        m.dd_MaterialDocNo,
        m.dd_MaterialDocItemNo,
        m.dd_MaterialDocYear,
        m.dd_DocumentNo,
        m.dd_DocumentItemNo,
        m.ct_Quantity ct_Quantity,
        m.ct_QtyEntryUOM ct_QtyEntryUOM,
        ifnull(m.Dim_MovementTypeid,1) as Dim_MovementTypeid,
        ifnull(m.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
        m.dd_debitcreditid,
        m.Dim_AfsSizeId,
        m.Fact_mmid_ref,
        m.ScheduleLineNo,
        m.OrderQty OrderQty,
        m.DlvrReceivedQty DlvrReceivedQty,
        m.OpenQty  OpenQty,
        m.dim_DeliveryDate,
        m.dim_StatDeliveryDate,
	m.DeliveryDate,
        m.CumuDlvrReceivedQty  CumuDlvrReceivedQty,
	m.CumuDlvrRcvdQtyPrev  CumuDlvrRcvdQtyPrev,
        m.CumuMMGRQty  CumuMMGRQty,
        m.PostingDate,
	ifnull(m.dim_Partid, 1) as dim_Partid,
	m.mm_UnitPrice,
	m.dim_uomunitofentryid,
	m."rownum"
FROM fact_materialmovement_tmp_dlvrlink m
            INNER JOIN dim_Part dp1 ON ifnull(m.dim_Partid, 1) = ifnull(dp1.dim_Partid, 1)
	    INNER JOIN dim_unitofmeasure uom ON m.dim_uomunitofentryid = uom.dim_unitofmeasureid
	    LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
	LEFT JOIN tmp_Purchase_uomvalues t1 on t1.dd_DocumentNo = m.dd_DocumentNo and t1.dd_DocumentItemNo = m.dd_DocumentItemNo
WHERE m."rownum" = 1;


drop table if exists fact_materialmovement_tmp_dlvrlink1_del;
create table fact_materialmovement_tmp_dlvrlink1_del as
select m.dd_MaterialDocNo dd_MaterialDocNo_del,
        m.dd_MaterialDocItemNo dd_MaterialDocItemNo_del,
        m.dd_MaterialDocYear dd_MaterialDocYear_del,
        m.dd_DocumentNo dd_DocumentNo_del,
        m.dd_DocumentItemNo dd_DocumentItemNo_del,
        m.ScheduleLineNo ScheduleLineNo_del
from fact_materialmovement_tmp_dlvrlink1 m
group by m.dd_MaterialDocNo,
        m.dd_MaterialDocItemNo,
        m.dd_MaterialDocYear,
        m.dd_DocumentNo,
        m.dd_DocumentItemNo,
        m.ScheduleLineNo
having count(*) > 1;

delete from fact_materialmovement_tmp_dlvrlink1
where exists (select 1 from fact_materialmovement_tmp_dlvrlink1_del m
		where m.dd_MaterialDocNo_del = dd_MaterialDocNo and
			m.dd_MaterialDocItemNo_del = dd_MaterialDocItemNo and
			m.dd_MaterialDocYear_del = dd_MaterialDocYear);


DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink;
CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
SELECT * FROM fact_materialmovement_tmp_dlvrlink1;

 DROP TABLE IF EXISTS fact_mm_tmp_dlvrlink_grqty;
 CREATE TABLE fact_mm_tmp_dlvrlink_grqty  AS
 SELECT f.Fact_materialmovementid, 
 		f.Fact_mmid_ref, 
 		f.ScheduleLineNo, 
		f.ct_QtyEntryUOM,
		"rownum" "rownum"
  FROM fact_materialmovement_tmp_dlvrlink1 f
  WHERE Fact_materialmovementid = 0 
        and exists (select 1 from fact_purchase po inner join dim_date dd1 on dd1.Dim_Dateid = po.Dim_DateidDelivery
                      where po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and po.Dim_AfsSizeId = f.Dim_AfsSizeId
                          and po.ct_ReceivedQty > 0
                          and (dd1.DateValue > f.DeliveryDate or (dd1.DateValue = f.DeliveryDate and po.dd_ScheduleNo > f.ScheduleLineNo)));


delete from fact_materialmovement_tmp_dlvrlink
where exists (select 1 from fact_mm_tmp_dlvrlink_grqty x 
		where x.Fact_mmid_ref = fact_materialmovement_tmp_dlvrlink.Fact_mmid_ref 
			and x.ScheduleLineNo = fact_materialmovement_tmp_dlvrlink.ScheduleLineNo 
			and x."rownum" = fact_materialmovement_tmp_dlvrlink."rownum");

 INSERT INTO fact_materialmovement_tmp_dlvrlink
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       gr.ct_QtyEntryUOM,
       ifnull(f.Dim_MovementTypeid, 1) as Dim_MovementTypeid,
       ifnull(f.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
	f.DeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate,
	ifnull(f.dim_Partid,1) as dim_Partid,
	f.mm_UnitPrice,
	f.dim_uomunitofentryid,
       f."rownum"
  FROM fact_materialmovement_tmp_dlvrlink1 f
  		inner join fact_mm_tmp_dlvrlink_grqty gr 
  		on f.Fact_mmid_ref = gr.Fact_mmid_ref and f.ScheduleLineNo = gr.ScheduleLineNo and f."rownum" = gr."rownum";


    
 DROP TABLE IF EXISTS fact_mm_tmp_dlvrlink_grqty;
 CREATE TABLE fact_mm_tmp_dlvrlink_grqty  AS
 SELECT f.Fact_materialmovementid, 
	f.Fact_mmid_ref, 
	f.ScheduleLineNo, 
	f.ct_QtyEntryUOM,
	"rownum"
  FROM fact_materialmovement_tmp_dlvrlink1 f
  WHERE Fact_materialmovementid = 0 
        and not exists (select 1 from fact_purchase po inner join dim_date dd1 on dd1.Dim_Dateid = po.Dim_DateidDelivery
                      where po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and ifnull(po.Dim_AfsSizeId, 1) = ifnull(f.Dim_AfsSizeId, 1)
                          and po.ct_ReceivedQty > 0
                          and (dd1.DateValue > f.DeliveryDate or (dd1.DateValue = f.DeliveryDate and po.dd_ScheduleNo > f.ScheduleLineNo)));

delete from fact_materialmovement_tmp_dlvrlink
where exists (select 1 from fact_mm_tmp_dlvrlink_grqty x 
		where x.Fact_mmid_ref = fact_materialmovement_tmp_dlvrlink.Fact_mmid_ref 
			and x.ScheduleLineNo = fact_materialmovement_tmp_dlvrlink.ScheduleLineNo 
			and x."rownum" = fact_materialmovement_tmp_dlvrlink."rownum");

  INSERT INTO fact_materialmovement_tmp_dlvrlink
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       gr.ct_QtyEntryUOM,
       ifnull(f.Dim_MovementTypeid,1) as Dim_MovementTypeid,
       ifnull(f.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
	f.DeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate,
	ifnull(f.dim_Partid,
	f.mm_UnitPrice,
	f.dim_uomunitofentryid,
       f."rownum"
  FROM fact_materialmovement_tmp_dlvrlink1 f
  	inner join fact_mm_tmp_dlvrlink_grqty gr 
  		on f.Fact_mmid_ref = gr.Fact_mmid_ref and f.ScheduleLineNo = gr.ScheduleLineNo and f."rownum" = gr."rownum";
  
/*
  UPDATE fact_materialmovement_tmp_dlvrlink f 
  SET ct_QtyEntryUOM = case when ct_QtyEntryUOM > 0 then CumuDlvrReceivedQty - CumuMMGRQty
			    else case when (CumuMMGRQty + ct_QtyEntryUOM) >= CumuDlvrRcvdQtyPrev then (CumuMMGRQty + ct_QtyEntryUOM) - CumuDlvrReceivedQty
					else (CumuDlvrRcvdQtyPrev - CumuMMGRQty) end
			end        
  WHERE exists (select 1 from fact_materialmovement_tmp_dlvrlink1 m
                where m.Fact_materialmovementid = 0 and m.Fact_mmid_ref = f.Fact_materialmovementid)
        and f.Fact_materialmovementid <> 0
*/


drop table if exists tmp_fact_materialmovement_001;
create table tmp_fact_materialmovement_001 as
select * 
from fact_materialmovement f
where exists (select 1 from fact_materialmovement_tmp_dlvrlink a where f.dd_DocumentNo = a.dd_DocumentNo and f.dd_DocumentItemNo = a.dd_DocumentItemNo);

UPDATE fact_materialmovement f 
    SET  f.Dim_DateIdDelivery = ifnull(m.dim_DeliveryDate, 1)
        f.Dim_DateIdStatDelivery = ifnull(m.dim_StatDeliveryDate, 1)
        f.dd_DocumentScheduleNo = m.ScheduleLineNo,
        f.ct_OrderQuantity = m.OrderQty,
        f.ct_QtyEntryUOM = m.ct_QtyEntryUOM,
        f.ct_Quantity = m.ct_Quantity,
        f.amt_LocalCurrAmt = ifnull(m.mm_UnitPrice * m.ct_QtyEntryUOM, 0)	
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_materialmovement_tmp_dlvrlink m ,fact_materialmovement f 
WHERE f.Fact_materialmovementid = m.Fact_materialmovementid
	AND not exists (select 1 from tmp_fact_materialmovement_001 a
			where a.Fact_materialmovementid <> f.Fact_materialmovementid
				and f.dd_MaterialDocNo = a.dd_MaterialDocNo and f.dd_MaterialDocItemNo = a.dd_MaterialDocItemNo and f.dd_MaterialDocYear = a.dd_MaterialDocYear
				and f.dd_DocumentNo = a.dd_DocumentNo and f.dd_DocumentItemNo = a.dd_DocumentItemNo and a.dd_DocumentScheduleNo = m.ScheduleLineNo);     
  
DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink1;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink1 AS
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       /*case when ct_QtyEntryUOM > 0 then case when CumuDlvrReceivedQty >= CumuMMGRQty then CumuMMGRQty - CumuDlvrRcvdQtyPrev else ct_QtyEntryUOM end
	    else case when (CumuMMGRQty + ct_QtyEntryUOM) >= CumuDlvrRcvdQtyPrev then (CumuMMGRQty + ct_QtyEntryUOM) - CumuDlvrReceivedQty
			else (CumuDlvrRcvdQtyPrev - CumuMMGRQty) end
	end ct_QtyEntryUOM*/
	f.ct_QtyEntryUOM,
       ifnull(f.Dim_MovementTypeid,1) as Dim_MovementTypeid,
       ifnull(f.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
       ifnull(f.dd_debitcreditid, 'Not Set') as dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.mm_UnitPrice,
       f.PostingDate 
  FROM  fact_materialmovement_tmp_dlvrlink f
  WHERE f.Fact_materialmovementid = 0;

 DROP TABLE IF EXISTS TMP_INS_fact_materialmovement;
CREATE TABLE TMP_INS_fact_materialmovement
AS
SELECT DISTINCT
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
        m.ScheduleLineNo
 FROM fact_materialmovement f
       inner join fact_materialmovement_tmp_dlvrlink1 m on f.Fact_materialmovementid = m.Fact_mmid_ref;



DROP TABLE IF EXISTS TMP_DEL_fact_materialmovement;
CREATE TABLE TMP_DEL_fact_materialmovement
as
select * from TMP_INS_fact_materialmovement where 1=2;

INSERT INTO TMP_DEL_fact_materialmovement
SELECT DISTINCT        f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
        f.dd_DocumentScheduleNo
 FROM fact_materialmovement f;

rename TMP_INS_fact_materialmovement to TMP_INS_fact_materialmovement1
;
create table TMP_INS_fact_materialmovement as 
(select * from TMP_INS_fact_materialmovement1) minus (Select * from TMP_DEL_fact_materialmovement)
;
drop table TMP_INS_fact_materialmovement1
;
 

INSERT INTO fact_materialmovement  
       (Fact_materialmovementid,
       dd_MaterialDocNo,
       dd_MaterialDocItemNo,
       dd_MaterialDocYear,
       dd_DocumentNo,
       dd_DocumentItemNo,
       dd_SalesOrderNo,
       dd_SalesOrderItemNo,
       dd_SalesOrderDlvrNo,
       dd_GLAccountNo,
       dd_ProductionOrderNumber,
       dd_ProductionOrderItemNo,
       dd_BatchNumber,
       dd_ValuationType,
       dd_debitcreditid,
       dd_GoodsMoveReason,
       amt_LocalCurrAmt,
       amt_DeliveryCost,
       amt_AltPriceControl,
       amt_OnHand_ValStock,
       ct_Quantity,
       ct_QtyEntryUOM,
       ct_QtyGROrdUnit,
       ct_QtyOnHand_ValStock,
       ct_QtyOrdPriceUnit,
       Dim_MovementTypeid,
       dim_Companyid,
       Dim_Currencyid,
	   Dim_Currencyid_TRA,
	   Dim_Currencyid_GBL,	   
       Dim_Partid,
       Dim_Plantid,
       Dim_StorageLocationid,
       Dim_Vendorid,
       dim_DateIDMaterialDocDate,
       dim_DateIDPostingDate,
       Dim_DateIDDocCreation,
       Dim_DateIDOrdered,
       dim_producthierarchyid,
       dim_MovementIndicatorid,
       dim_Customerid,
       dim_CostCenterid,
       Dim_AccountCategoryid,
       Dim_ConsumptionTypeid,
       Dim_ControllingAreaid,
       Dim_CustomerGroup1id,
       Dim_DocumentCategoryid,
       Dim_DocumentTypeid,
       Dim_ItemCategoryid,
       Dim_ItemStatusid,
       dim_productionorderstatusid,
       Dim_productionordertypeid,
       Dim_PurchaseGroupid,
       Dim_PurchaseOrgid,
       Dim_SalesDocumentTypeid,
       Dim_SalesGroupid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       dim_salesorderrejectreasonid,
       Dim_SalesOrgid,
       dim_specialstockid,
       Dim_StockTypeid,
       Dim_UnitOfMeasureid,
       dirtyrow,
       Dim_MaterialGroupid,
       amt_ExchangeRate_GBL,
       amt_ExchangeRate,
       Dim_Termid,
       dd_ConsignmentFlag,
       Dim_ProfitCenterId,
       Dim_ReceivingPlantId,
       Dim_ReceivingPartId,
       amt_POUnitPrice,
       amt_StdUnitPrice,
       Dim_DateidCosting,
       Dim_IncoTermid,
       Dim_DateIdDelivery,
       Dim_DateIdStatDelivery,
       Dim_GRStatusid,
       dd_incoterms2,
       dd_IntOrder,
       amt_PlannedPrice,
       amt_PlannedPrice1,
       dd_DocumentScheduleNo,
       Dim_RecvIssuStorLocid,
       ct_OrderQuantity,
       Dim_POPlantidOrdering,
       Dim_POPlantidSupplying,
       Dim_POStorageLocid,
       Dim_POIssuStorageLocid,
       Dim_InspUsageDecisionId,
       ct_LotNotThruQM,
       dd_ReferenceDocNo,
       dd_ReferenceDocItem,
       LotsAcceptedFlag,
       LotsRejectedFlag,
       PercentLotsInspectedFlag,
       PercentLotsRejectedFlag,
       PercentLotsAcceptedFlag,
       LotsSkippedFlag,
       LotsAwaitingInspectionFlag,
       LotsInspectedFlag,
       QtyRejectedExternalAmt,
       QtyRejectedInternalAmt,
       Dim_AfsSizeId,
       amt_vendorspend,
       dd_vendorspendflag,
       dim_uomunitofentryid,
       dim_unitofmeasureorderunitid,
       lotsreceivedflag,
       dd_inspectionstatusingrdoc)
  SELECT m.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_SalesOrderNo,
       f.dd_SalesOrderItemNo,
       f.dd_SalesOrderDlvrNo,
       f.dd_GLAccountNo,
       f.dd_ProductionOrderNumber,
       f.dd_ProductionOrderItemNo,
       f.dd_BatchNumber,
       f.dd_ValuationType,
       ifnull(f.dd_debitcreditid, 'Not Set') as dd_debitcreditid,
       ifnull(f.dd_GoodsMoveReason, 0) as dd_GoodsMoveReason,
       ifnull(m.mm_UnitPrice * m.ct_QtyEntryUOM, 0) amt_LocalCurrAmt,
       0 amt_DeliveryCost,
       0 amt_AltPriceControl,
       0 amt_OnHand_ValStock,
       m.ct_QtyEntryUOM ct_Quantity,
       m.ct_QtyEntryUOM ct_QtyEntryUOM,
       0 ct_QtyGROrdUnit,
       0 ct_QtyOnHand_ValStock,
       0 ct_QtyOrdPriceUnit,
       ifnull(f.Dim_MovementTypeid, 1) as Dim_MovementTypeid,
       ifnull(f.dim_Companyid, 1) as dim_Companyid,
       ifnull(f.Dim_Currencyid, 1) as Dim_Currencyid,
	   f.Dim_Currencyid_TRA,
	   f.Dim_Currencyid_GBL,
       ifnull(f.Dim_Partid,1) as Dim_Partid,
       ifnull(f.Dim_Plantid, 1) as Dim_Plantid,
       ifnull(f.Dim_StorageLocationid, 1) as Dim_StorageLocationid,
       ifnull(f.Dim_Vendorid, 1) as Dim_Vendorid,
       ifnull(f.dim_DateIDMaterialDocDate, 1) as dim_DateIDMaterialDocDate,
       ifnull(f.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
       ifnull(f.Dim_DateIDDocCreation, 1) as Dim_DateIDDocCreation,
       ifnull(f.Dim_DateIDOrdered, 1) as Dim_DateIDOrdered,
       ifnull(f.dim_producthierarchyid, 1) as dim_producthierarchyid,
       ifnull(f.dim_MovementIndicatorid, 1) as dim_MovementIndicatorid,
       ifnull(f.dim_Customerid, 1) as dim_Customerid,
       ifnull(f.dim_CostCenterid, 1) as dim_CostCenterid,
       ifnull(f.Dim_AccountCategoryid, 1) as Dim_AccountCategoryid,
       ifnull(f.Dim_ConsumptionTypeid, 1) as Dim_ConsumptionTypeid,
       ifnull(f.Dim_ControllingAreaid, 1) as Dim_ControllingAreaid,
       ifnull(f.Dim_CustomerGroup1id, 1) as Dim_CustomerGroup1id,
       ifnull(f.Dim_DocumentCategoryid, 1) as Dim_DocumentCategoryid,
       ifnull(f.Dim_DocumentTypeid, 1) as Dim_DocumentTypeid,
       ifnull(f.Dim_ItemCategoryid, 1) as Dim_ItemCategoryid,
       ifnull(f.Dim_ItemStatusid, 1) as Dim_ItemStatusid,
       ifnull(f.dim_productionorderstatusid, 1) as dim_productionorderstatusid,
       ifnull(f.Dim_productionordertypeid, 1) as Dim_productionordertypeid,
       ifnull(f.Dim_PurchaseGroupid, 1) as Dim_PurchaseGroupid,
       ifnull(f.Dim_PurchaseOrgid, 1) as Dim_PurchaseOrgid,
       ifnull(f.Dim_SalesDocumentTypeid, 1) as Dim_SalesDocumentTypeid,
       ifnull(f.Dim_SalesGroupid, 1) as Dim_SalesGroupid,
       ifnull(f.Dim_SalesOrderHeaderStatusid, 1) as Dim_SalesOrderHeaderStatusid,
       ifnull(f.Dim_SalesOrderItemStatusid, 1) as Dim_SalesOrderItemStatusid,
       ifnull(f.dim_salesorderrejectreasonid, 1) as dim_salesorderrejectreasonid,
       ifnull(f.Dim_SalesOrgid, 1) as Dim_SalesOrgid,
       ifnull(f.dim_specialstockid, 1) as dim_specialstockid,
       ifnull(f.Dim_StockTypeid, 1) as Dim_StockTypeid,
       ifnull(f.Dim_UnitOfMeasureid, 1) as Dim_UnitOfMeasureid,
       f.dirtyrow,
       ifnull(f.Dim_MaterialGroupid, ) as Dim_MaterialGroupid,
       f.amt_ExchangeRate_GBL,
       f.amt_ExchangeRate,
       ifnull(f.Dim_Termid, 1) as Dim_Termid,
       ifnull(f.dd_ConsignmentFlag, 0) as dd_ConsignmentFlag,
       ifnull(f.Dim_ProfitCenterId, 1) as Dim_ProfitCenterId,
       ifnull(f.Dim_ReceivingPlantId, 1) as Dim_ReceivingPlantId,
       ifnull(f.Dim_ReceivingPartId, 1) as Dim_ReceivingPartId,
       f.amt_POUnitPrice,
       f.amt_StdUnitPrice,
       ifnull(f.Dim_DateidCosting, 1) as Dim_DateidCosting,
       ifnull(f.Dim_IncoTermid, 1) as Dim_IncoTermid,
       ifnull(m.dim_DeliveryDate, 1) as  Dim_DateIdDelivery,
       ifnull(m.dim_StatDeliveryDate, 1) as Dim_DateIdStatDelivery,
       f.Dim_GRStatusid,
       f.dd_incoterms2,
       f.dd_IntOrder,
       f.amt_PlannedPrice,
       f.amt_PlannedPrice1,
       m.ScheduleLineNo dd_DocumentScheduleNo,
       ifnull(f.Dim_RecvIssuStorLocid, 1) as Dim_RecvIssuStorLocid,
       m.OrderQty ct_OrderQuantity,
       ifnull(f.Dim_POPlantidOrdering, 1) as Dim_POPlantidOrdering,
       ifnull(f.Dim_POPlantidSupplying, 1) as Dim_POPlantidSupplying,
       ifnull(f.Dim_POStorageLocid, 1) as Dim_POStorageLocid,
       ifnull(f.Dim_POIssuStorageLocid, 1) as Dim_POIssuStorageLocid
       ifnull(f.Dim_InspUsageDecisionId, 1) as Dim_InspUsageDecisionId,
       0 ct_LotNotThruQM,
       ifnull(f.dd_ReferenceDocNo, 'Not Set') as dd_ReferenceDocNo,
       ifnull(f.dd_ReferenceDocItem, 0) as dd_ReferenceDocItem,
       f.LotsAcceptedFlag,
       f.LotsRejectedFlag,
       f.PercentLotsInspectedFlag,
       f.PercentLotsRejectedFlag,
       f.PercentLotsAcceptedFlag,
       f.LotsSkippedFlag,
       f.LotsAwaitingInspectionFlag,
       f.LotsInspectedFlag,
       f.QtyRejectedExternalAmt,
       f.QtyRejectedInternalAmt,
       ifnull(f.Dim_AfsSizeId, 1) as Dim_AfsSizeId,
       0,
       ifnull(f.dd_vendorspendflag, 0) as dd_vendorspendflag,
       f.dim_uomunitofentryid,
       f.dim_unitofmeasureorderunitid,
       f.lotsreceivedflag,
       f.dd_inspectionstatusingrdoc
  FROM fact_materialmovement f 
       inner join fact_materialmovement_tmp_dlvrlink1 m on f.Fact_materialmovementid = m.Fact_mmid_ref
	INNER JOIN TMP_INS_fact_materialmovement i ON f.dd_materialdocno = i.dd_materialdocno AND f.dd_materialdocitemno = i.dd_materialdocitemno 
		AND f.dd_MaterialDocYear = i.dd_MaterialDocYear AND m.ScheduleLineNo = i.ScheduleLineNo
		where NOT EXISTS (SELECT 1 FROM FACT_MATERIALMOVEMENT fmov
		where f.dd_MaterialDocYear = fmov.dd_MaterialDocYear 
		and f.dd_materialdocitemno = fmov.dd_materialdocitemno
		and f.dd_materialdocno = fmov.dd_materialdocno);

DROP TABLE IF EXISTS TMP_INS_fact_materialmovement;
DROP TABLE IF EXISTS TMP_DEL_fact_materialmovement;

UPDATE fact_materialmovement f 
     set f.Dim_GRStatusid = case when dp.DateValue > (to_date(ds.DateValue) + (INTERVAL '1' DAY)*L.pToleranceDays ) then 3 
                                 when dp.DateValue < (to_date(ds.DateValue) - (INTERVAL '1' DAY)*E.pToleranceDays ) then 4 
                                 else 5 end
FROM fact_materialmovement f ,dim_date dp, dim_date ds, var_pToleranceDays E,var_pToleranceDays L
WHERE f.Dim_DateIdStatDelivery <> 1
     and ifnull(dp.Dim_Dateid, 1) = ifnull(f.dim_DateIDPostingDate, 1)
     and ds.Dim_Dateid = f.Dim_DateIdStatDelivery
     and E.pType = 'EARLY'
     and L.pType = 'LATE';
	 	 



DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink1;
DROP TABLE IF EXISTS fact_mm_tmp_dlvrlink_grqty;
DROP TABLE IF EXISTS fact_mm_dlvr_link_scheduleno_temp;
DROP TABLE IF EXISTS  fact_materialmovement_tmp_dlvrlink1_del;

DROP TABLE IF EXISTS tmp_Purchase_uomvalues;
