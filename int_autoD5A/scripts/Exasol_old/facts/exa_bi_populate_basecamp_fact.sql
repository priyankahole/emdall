
/* this is due to the relatively low number of rows and changes in todolists and todos relation.
Relations are maintained in each dim (todos contain todolistsid for example) */

/* Madalina 5 Jun 2018 - maintaining the values for the editable fields, and mapping them back after the fact re-creation - APP-9709 */
drop table if exists fact_basecamp_comments;
create table fact_basecamp_comments as
select distinct
	dim_basecamp_todolistsid,
	dim_basecamp_todosid,
	dim_basecamp_commentsid,
    dd_UIVersion,
	dd_status,
	dd_statusOverviewComment
from fact_basecamp;

truncate table fact_basecamp; 


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'fact_basecamp';

INSERT INTO NUMBER_FOUNTAIN
select  'fact_basecamp',
                ifnull(max(f.fact_basecampid),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_basecamp f;

INSERT INTO fact_basecamp(
	fact_basecampid,
	dim_basecamp_todolistsid,
	dim_basecamp_todosid,
	dim_basecamp_commentsid
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'fact_basecamp') + row_number() over(order by '')  as fact_basecampid,
ifnull(tl.dim_basecamp_todolistsid,1) as dim_basecamp_todolistsid,
ifnull(t.dim_basecamp_todosid,1) as dim_basecamp_todosid,
ifnull(c.dim_basecamp_commentsid,1) as dim_basecamp_commentsid -- null if todo created and closed without a comment
from
dim_basecamp_todolists tl 
	left join dim_basecamp_todos t on tl.todolist_id = t.todolist_id
	left join dim_basecamp_comments c on t.todo_id = c.todo_id
where tl.dim_basecamp_todolistsid <> 1; -- 17575


/* delay between comments */
update fact_basecamp fb
set fb.ct_comment_delay = bc.comment_delay,
	fb.ct_comment_delay_seconds = bc.comment_delay_seconds
from fact_basecamp fb 
	inner join dim_basecamp_comments bc on fb.dim_basecamp_commentsid = bc.dim_basecamp_commentsid;



/* ct_todolistscompletedcount */
merge into fact_basecamp f
using
(select distinct fact_basecampid, completed_count
from fact_basecamp f 
inner join dim_basecamp_todolists tdl
on tdl.dim_basecamp_todolistsid = f.dim_basecamp_todolistsid
)t
on t.fact_basecampid = f.fact_basecampid
when matched then update
set f.ct_todolistscompletedcount = t.completed_count;


/* ct_todolistsremainingcount */
merge into fact_basecamp f
using
(select distinct fact_basecampid, remaining_count
from fact_basecamp f 
inner join dim_basecamp_todolists tdl
on tdl.dim_basecamp_todolistsid = f.dim_basecamp_todolistsid
)t
on t.fact_basecampid = f.fact_basecampid
when matched then update
set f.ct_todolistsremainingcount = t.remaining_count;


/* ct_todocommentscount */
merge into fact_basecamp f
using
(select distinct fact_basecampid, comments_count
from fact_basecamp f 
inner join dim_basecamp_todos tdl
on tdl.dim_basecamp_todosid = f.dim_basecamp_todosid
)t
on t.fact_basecampid = f.fact_basecampid
when matched then update
set f.ct_todocommentscount = t.comments_count;



/* dim dates and times updates */

/*to dos dim dates and times*/
--created at date and time
update fact_basecamp f
set dim_dateid_todo_createdat = ifnull(d.dim_dateid, 1)
from fact_basecamp f, dim_date d, dim_basecamp_todos c, stg_basecamp_todos s
where f.dim_basecamp_todosid = c.dim_basecamp_todosid
and c.todo_id= s.todo_id
and d.datevalue = cast(substr(s.created_at,1,10) as date)
and d.companycode = 'Not Set'
and d.plantcode_factory = 'Not Set'
and dim_dateid_todo_createdat <> ifnull(d.dim_dateid, 1);

update fact_basecamp f
set dd_todo_createdat_time = substr(s.created_at, 11)
from fact_basecamp f,  dim_basecamp_todos c, stg_basecamp_todos s
where f.dim_basecamp_todosid = c.dim_basecamp_todosid
and c.todo_id= s.todo_id
and dd_todo_createdat_time <> substr(s.created_at, 11);


--updated at date and time
update fact_basecamp f
set dim_dateid_todo_updatedat = ifnull(d.dim_dateid, 1)
from fact_basecamp f, dim_date d, dim_basecamp_todos c, stg_basecamp_todos s
where f.dim_basecamp_todosid = c.dim_basecamp_todosid
and c.todo_id= s.todo_id
and d.datevalue = cast(substr(s.updated_at,1,10) as date)
and d.companycode = 'Not Set'
and d.plantcode_factory = 'Not Set'
and dim_dateid_todo_updatedat <> ifnull(d.dim_dateid, 1);

update fact_basecamp f
set dd_todo_updatedat_time = substr(s.updated_at, 11)
from fact_basecamp f,  dim_basecamp_todos c, stg_basecamp_todos s
where f.dim_basecamp_todosid = c.dim_basecamp_todosid
and c.todo_id= s.todo_id
and dd_todo_updatedat_time <> substr(s.updated_at, 11);


--completed at date and time
update fact_basecamp f
set dim_dateid_todo_completedat = ifnull(d.dim_dateid, 1)
from fact_basecamp f, dim_date d, dim_basecamp_todos c, stg_basecamp_todos s
where f.dim_basecamp_todosid = c.dim_basecamp_todosid
and c.todo_id= s.todo_id
and d.datevalue = cast(substr(s.completed_at,1,10) as date)
and d.companycode = 'Not Set'
and d.plantcode_factory = 'Not Set'
and dim_dateid_todo_completedat <> ifnull(d.dim_dateid, 1);

update fact_basecamp f
set dd_todo_completedat_time = substr(s.completed_at, 11)
from fact_basecamp f,  dim_basecamp_todos c, stg_basecamp_todos s
where f.dim_basecamp_todosid = c.dim_basecamp_todosid
and c.todo_id= s.todo_id
and dd_todo_completedat_time <> substr(s.completed_at, 11);


--due at and due on dates
update fact_basecamp f
set dim_dateid_todo_dueat = ifnull(d.dim_dateid, 1)
from fact_basecamp f, dim_date d, dim_basecamp_todos c, stg_basecamp_todos s
where f.dim_basecamp_todosid = c.dim_basecamp_todosid
and c.todo_id= s.todo_id
and d.datevalue = cast(substr(s.due_at,1,10) as date)
and d.companycode = 'Not Set'
and d.plantcode_factory = 'Not Set'
and dim_dateid_todo_dueat <> ifnull(d.dim_dateid, 1);

update fact_basecamp f
set dim_dateid_todo_dueon = ifnull(d.dim_dateid, 1)
from fact_basecamp f, dim_date d, dim_basecamp_todos c, stg_basecamp_todos s
where f.dim_basecamp_todosid = c.dim_basecamp_todosid
and c.todo_id= s.todo_id
and d.datevalue = cast(substr(s.due_on,1,10) as date)
and d.companycode = 'Not Set'
and d.plantcode_factory = 'Not Set'
and dim_dateid_todo_dueon <> ifnull(d.dim_dateid, 1);



/*to do lists dim dates and times*/
--created on date and time
update fact_basecamp f
set dim_dateid_todolist_createdon = ifnull(d.dim_dateid, 1)
from fact_basecamp f, dim_date d, dim_basecamp_todolists c, stg_basecamp_todolists s
where f.dim_basecamp_todolistsid = c.dim_basecamp_todolistsid
and c.todolist_id= s.todolist_id
and d.datevalue = cast(substr(s.created_at,1,10) as date)
and d.companycode = 'Not Set'
and d.plantcode_factory = 'Not Set'
and dim_dateid_todolist_createdon <> ifnull(d.dim_dateid, 1);

update fact_basecamp f
set dd_todolist_createdon_time = substr(s.created_at, 11)
from fact_basecamp f,  dim_basecamp_todolists c, stg_basecamp_todolists s
where f.dim_basecamp_todolistsid = c.dim_basecamp_todolistsid
and c.todolist_id= s.todolist_id
and dd_todolist_createdon_time <> substr(s.created_at, 11);

--updated at date and time
update fact_basecamp f
set dim_dateid_todolist_updatedat = ifnull(d.dim_dateid, 1)
from fact_basecamp f, dim_date d, dim_basecamp_todolists c, stg_basecamp_todolists s
where f.dim_basecamp_todolistsid = c.dim_basecamp_todolistsid
and c.todolist_id= s.todolist_id
and d.datevalue = cast(substr(s.updated_at,1,10) as date)
and d.companycode = 'Not Set'
and d.plantcode_factory = 'Not Set'
and dim_dateid_todolist_updatedat <> ifnull(d.dim_dateid, 1);

update fact_basecamp f
set dd_todolist_updatedat_time = substr(s.updated_at, 11)
from fact_basecamp f,  dim_basecamp_todolists c, stg_basecamp_todolists s
where f.dim_basecamp_todolistsid = c.dim_basecamp_todolistsid
and c.todolist_id= s.todolist_id
and dd_todolist_updatedat_time <> substr(s.updated_at, 11);

/*comment dim dates and times*/
--created on date and time
update fact_basecamp f
set dim_dateid_comment_createdon = ifnull(d.dim_dateid, 1)
from fact_basecamp f, dim_date d, dim_basecamp_comments c, stg_basecamp_comments s
where f.dim_basecamp_commentsid = c.dim_basecamp_commentsid
and c.comment_id = s.comment_id
and d.datevalue = cast(substr(s.comment_created_on,1,10) as date)
and d.companycode = 'Not Set'
and d.plantcode_factory = 'Not Set'
and dim_dateid_comment_createdon <> ifnull(d.dim_dateid, 1);

update fact_basecamp f
set dd_comment_createdon_time = substr(s.comment_created_on, 11)
from fact_basecamp f, dim_basecamp_comments c, stg_basecamp_comments s
where f.dim_basecamp_commentsid = c.dim_basecamp_commentsid
and c.comment_id = s.comment_id
and dd_comment_createdon_time <>  substr(s.comment_created_on, 11);


/* dim dates and times updates */


/* updates images used in dashboard - embed html in table cells */
update fact_basecamp fb
set fb.dd_comment_creator_image = concat('<img src="', bc.comment_creator_image,'"') 
from fact_basecamp fb 
	inner join dim_basecamp_comments bc on fb.dim_basecamp_commentsid = bc.dim_basecamp_commentsid;


merge into fact_basecamp fb
using (
select fb.fact_basecampid, max(ifnull(concat('<img src="', bc.comment_creator_image,'"'),'Not Set')) as dd_todo_completer_image
	from fact_basecamp fb
	inner join dim_basecamp_todos td on fb.dim_basecamp_todosid = td.dim_basecamp_todosid
	left join (select distinct comment_creator_image, comment_creator_name from dim_basecamp_comments) bc on bc.comment_creator_name = td.completer_name
group by fb.fact_basecampid -- Greg has two accounts with the same name
) t
on fb.fact_basecampid = t.fact_basecampid
when matched then update
set fb.dd_todo_completer_image = t.dd_todo_completer_image;

/* Madalina 5 Jun 2018 - maintaining the values for the editable fields, and mapping them back after the fact re-creation - APP-9709 */
update fact_basecamp b 
set  b.dd_UIVersion = bc.dd_UIVersion,
	b.dd_status = bc.dd_status,
	b.dd_statusOverviewComment = bc.dd_statusOverviewComment
from fact_basecamp b 
	inner join fact_basecamp_comments bc on b.dim_basecamp_todolistsid = bc.dim_basecamp_todolistsid
						 and b.dim_basecamp_todosid = bc.dim_basecamp_todosid
						 and b.dim_basecamp_commentsid = bc.dim_basecamp_commentsid;

