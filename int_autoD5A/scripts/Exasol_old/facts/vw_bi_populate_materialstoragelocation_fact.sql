
/* ################################################################################################################## */
/* */
/*   Author         : Georgiana */
/*   Created On     : 30 Aug 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   30 Aug 2016      Gergiana	1.0  		  First draft */

/******************************************************************************************************************/


delete from number_fountain m where m.table_name = 'fact_materialstoragelocation';
insert into number_fountain
select 'fact_materialstoragelocation',
ifnull(max(f.fact_materialstoragelocationid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_materialstoragelocation f;

Insert into fact_materialstoragelocation (
fact_materialstoragelocationid,
dd_materialno,
dd_plant,
dd_storagelocation,
dim_partid,
dim_plantid,
dim_storagelocationid,
dd_storagelocationMRPInd,
dd_storagebin,
dim_dateidcreatedon,
ct_inventorycorrection,
ct_unrestrictedstock,
ct_stockintransfer,
ct_stockinqualityinsp,
ct_totalstockofrestrictedbatches,
ct_blockedstock,
ct_blockedstockreturns,
dw_insert_date
)
SELECT
(select max_id   from number_fountain   where table_name = 'fact_materialstoragelocation') + row_number() over(order by '') AS fact_materialstoragelocationid,
ifnull(MARD_MATNR, 'Not Set'),
ifnull(MARD_WERKS,'Not Set'),
ifnull(MARD_LGORT, 'Not Set'),
1 as dim_partid,
1 as dim_plantid,
1 as dim_storagelocationid,
ifnull(MARD_DISKZ,'Not Set'),
ifnull(MARD_LGPBE,'Not Set'),
1 as dim_dateidcreatedon,
ifnull(MARD_BSKRF,0),
ifnull(MARD_LABST,0),
ifnull(MARD_UMLME,0),
ifnull(MARD_INSME,0),
ifnull(MARD_EINME,0),
ifnull(MARD_SPEME,0),
ifnull(MARD_RETME,0),
current_date
FROM MARD_N
where not exists (select 1 from fact_materialstoragelocation
                    where dd_materialno = ifnull(MARD_MATNR, 'Not Set')
					      and dd_plant = ifnull(MARD_WERKS,'Not Set')
						  and dd_storagelocation = ifnull(MARD_LGORT, 'Not Set'));

merge into fact_materialstoragelocation f
using ( select distinct f.fact_materialstoragelocationid,dp.dim_partid
from fact_materialstoragelocation f, MARD_N m, dim_part dp
where dd_materialno = ifnull(MARD_MATNR, 'Not Set')
and dd_plant = ifnull(MARD_WERKS,'Not Set')
and dd_storagelocation = ifnull(MARD_LGORT, 'Not Set')
and dp.partnumber = ifnull(MARD_MATNR, 'Not Set')
and dp.plant = ifnull(MARD_WERKS,'Not Set')
and f.dim_partid <> dp.dim_partid) t 
on t.fact_materialstoragelocationid=f.fact_materialstoragelocationid
when matched then update set f.dim_partid=t.dim_partid
,dw_update_date = current_timestamp;

merge into fact_materialstoragelocation f
using ( select distinct f.fact_materialstoragelocationid,dp.dim_plantid
from fact_materialstoragelocation f, MARD_N m, dim_plant dp
where dd_materialno = ifnull(MARD_MATNR, 'Not Set')
and dd_plant = ifnull(MARD_WERKS,'Not Set')
and dd_storagelocation = ifnull(MARD_LGORT, 'Not Set')
and dp.plantCode = ifnull(MARD_WERKS,'Not Set')
and f.dim_plantid <> dp.dim_plantid) t 
on t.fact_materialstoragelocationid=f.fact_materialstoragelocationid
when matched then update set f.dim_plantid=t.dim_plantid
,dw_update_date = current_timestamp;


merge into fact_materialstoragelocation f
using ( select distinct f.fact_materialstoragelocationid,ds.dim_storagelocationid
from fact_materialstoragelocation f, MARD_N m, dim_storagelocation ds
where dd_materialno = ifnull(MARD_MATNR, 'Not Set')
and dd_plant = ifnull(MARD_WERKS,'Not Set')
and dd_storagelocation = ifnull(MARD_LGORT, 'Not Set')
and ds.LocationCode = ifnull(MARD_LGORT, 'Not Set')
and ds.Plant = ifnull(MARD_WERKS,'Not Set')
and f.dim_storagelocationid <> ds.dim_storagelocationid) t 
on t.fact_materialstoragelocationid=f.fact_materialstoragelocationid
when matched then update set f.dim_storagelocationid=t.dim_storagelocationid
,dw_update_date = current_timestamp;

 
merge into fact_materialstoragelocation f
using ( select distinct f.fact_materialstoragelocationid,ifnull(MARD_DISKZ,'Not Set') as MARD_DISKZ
from fact_materialstoragelocation f, MARD_N m
where dd_materialno = ifnull(MARD_MATNR, 'Not Set')
and dd_plant = ifnull(MARD_WERKS,'Not Set')
and dd_storagelocation = ifnull(MARD_LGORT, 'Not Set')
and f.dd_storagelocationMRPInd <> ifnull(MARD_DISKZ,'Not Set')) t 
on t.fact_materialstoragelocationid=f.fact_materialstoragelocationid
when matched then update set f.dd_storagelocationMRPInd=t.MARD_DISKZ
,dw_update_date = current_timestamp;


merge into fact_materialstoragelocation f
using ( select distinct f.fact_materialstoragelocationid,ifnull(MARD_LGPBE,'Not Set') as MARD_LGPBE
from fact_materialstoragelocation f, MARD_N m
where dd_materialno = ifnull(MARD_MATNR, 'Not Set')
and dd_plant = ifnull(MARD_WERKS,'Not Set')
and dd_storagelocation = ifnull(MARD_LGORT, 'Not Set')
and f.dd_storagebin <> ifnull(MARD_LGPBE,'Not Set')) t 
on t.fact_materialstoragelocationid=f.fact_materialstoragelocationid
when matched then update set f.dd_storagebin=t.MARD_LGPBE
,dw_update_date = current_timestamp;

merge into fact_materialstoragelocation f
using ( select distinct f.fact_materialstoragelocationid,dt.dim_dateid 
from fact_materialstoragelocation f, MARD_N m, dim_date dt, dim_plant pl
where dd_materialno = ifnull(MARD_MATNR, 'Not Set')
and dd_plant = ifnull(MARD_WERKS,'Not Set')
and dd_storagelocation = ifnull(MARD_LGORT, 'Not Set')
and dt.datevalue = ifnull(MARD_ERSDA, '0001-01-01')
and dt.companycode=pl.companycode
and dt.plantcode_factory = pl.plantcode
and f.dim_plantid=pl.dim_plantid
and f.dim_dateidcreatedon <> dt.dim_dateid) t 
on t.fact_materialstoragelocationid=f.fact_materialstoragelocationid
when matched then update set f.dim_dateidcreatedon=t.dim_dateid
,dw_update_date = current_timestamp;


merge into fact_materialstoragelocation f
using ( select distinct f.fact_materialstoragelocationid,
MARD_BSKRF,
MARD_LABST,
MARD_UMLME,
MARD_INSME,
MARD_EINME,
MARD_SPEME,
MARD_RETME
from fact_materialstoragelocation f, MARD_N m
where dd_materialno = ifnull(MARD_MATNR, 'Not Set')
and dd_plant = ifnull(MARD_WERKS,'Not Set')
and dd_storagelocation = ifnull(MARD_LGORT, 'Not Set')) t
on t.fact_materialstoragelocationid=f.fact_materialstoragelocationid
when matched then update set
 ct_inventorycorrection=ifnull(MARD_BSKRF,0),
ct_unrestrictedstock=ifnull(MARD_LABST,0),
ct_stockintransfer=ifnull(MARD_UMLME,0),
ct_stockinqualityinsp=ifnull(MARD_INSME,0),
ct_totalstockofrestrictedbatches=ifnull(MARD_EINME,0),
ct_blockedstock=ifnull(MARD_SPEME,0),
ct_blockedstockreturns=ifnull(MARD_RETME,0)
,dw_update_date = current_timestamp;






