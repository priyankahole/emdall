delete from fact_scm_test; 
delete from fact_scm; 
alter table fact_scm drop column fact_id_identity cascade; 
alter table fact_scm add fact_id_identity int identity; 
alter table fact_scm_test drop column fact_id_identity cascade; 
alter table fact_scm_test add fact_id_identity int identity; 
drop table if exists temp_interplants; 
 create table temp_interplants as 
      select distinct a.dim_partid, a.dim_batchid, 
               a.dim_plantid as plant,  
              a.dim_poplantidsupplying  as plantsupplying, 
              b.dd_productionordernumber, 
              b.ItemSubType_Merck   
              from 
                  ( select distinct f_mm.dim_plantid,  
                                     f_mm.dim_partid,  
                                     f_mm.dim_batchid, 
                                     dp.partnumber, 
                                     b.batchnumber,  
                                     f_mm.dd_productionordernumber, 
                                    mt.movementtype, 
                                    f_mm.dim_poplantidsupplying , 
                                    dp.ItemSubType_Merck 
                    from fact_materialmovement f_mm,       
                          dim_movementtype mt, 
                          dim_part dp, 
                          dim_batch b   
                    where f_mm.dim_movementtypeid = mt.dim_movementtypeid   
                       and  mt.movementtype in ('101')   
                       and dp.dim_partid = f_mm.dim_partid      
                       and b.dim_batchid = f_mm.dim_batchid   
                        and f_mm.dim_poplantidsupplying  <> 1  
                       and dd_productionordernumber = 'Not Set') as a 
              inner join 
                  ( select distinct f_mm.dim_plantid,  
                            f_mm.dim_partid, 
                             f_mm.dim_batchid, 
                             dp.partnumber, 
                            b.batchnumber,    
                            f_mm.dd_productionordernumber, 
                             mt.movementtype, 
                            f_mm.dim_poplantidsupplying , 
                            dp.ItemSubType_Merck 
                  from fact_materialmovement f_mm,  
                      dim_movementtype mt, 
                      dim_part dp, 
                      dim_batch b         
                 where f_mm.dim_movementtypeid = mt.dim_movementtypeid   
                      and  mt.movementtype in ('321')   
                      and dp.dim_partid = f_mm.dim_partid    
                       and b.dim_batchid = f_mm.dim_batchid     
                       and f_mm.dim_poplantidsupplying  = 1 
                       and dd_productionordernumber <> 'Not Set') b  
             on a.dim_poplantidsupplying = b.dim_plantid   
                 and a.ItemSubType_Merck = b.ItemSubType_Merck  
                and lower(a.ItemSubType_Merck) in ('raw', 'bulk', 'fpu', 'fpp', 'interm')  
                 and  a.partnumber = b.partnumber   
                 and a.batchnumber = b.batchnumber; 
 /*create basic tables - temp_raw, temp_bulk, etc, temp_raw_bulk, etc*/ 
drop table if exists temp_antigen; 
create table temp_antigen as 
                     select distinct f_prodorder.dim_plantid, 
                             f_prodorder.dim_partidheader as dim_partid, 
                             f_prodorder.dim_batchid, 
                             ifnull(f_insp.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno, 
                             f_prodorder.dd_ordernumber, 
                             convert(varchar(50), 'Not Set') dd_DocumentNo, 
                             convert(decimal (18,0), 0) dd_DocumentItemNo, 
                             convert(decimal (18,0), 0) dd_scheduleno, 
                             convert(bigint, 1) dim_dateidschedorder, 
                             convert(bigint, 1) dim_dateidcreate, 
                             convert(bigint, 1) dim_dateidpostingdate, 
                             ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr, 
                             ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco, 
                             ifnull(f_insp.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart, 
                             ifnull(f_insp.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade, 
                             tmp.dim_partid as dim_partid_interpl, 
                             tmp.dim_batchid as dim_batchid_interpl, 
                             tmp.plant as dim_plantid_interpl, 
                             f_prodorder.dim_dateidactualrelease as dim_dateidactualrelease 
                     from fact_productionorder f_prodorder  
                     left join fact_inspectionlot f_insp on f_insp.dim_batchid = f_prodorder.dim_batchid 
                     and f_insp.dim_partid = f_prodorder.dim_partidheader 
                     and f_insp.dim_plantid = f_prodorder.dim_plantid 
                     and f_prodorder.dd_ordernumber = f_insp.dd_OrderNo 
                     inner join dim_part dp on f_prodorder.dim_partidheader = dp.dim_partid 
                     left join dim_ProductionOrderType dpot on f_prodorder.dim_productionordertypeid = dpot.dim_productionordertypeid 
                         and TypeCode <> 'ZRWK' 
                     left join temp_interplants tmp  
                         on f_prodorder.dim_plantid = tmp.plantsupplying  
                          and f_prodorder.dd_ordernumber =  tmp.dd_productionordernumber  
                          and dp.ItemSubType_Merck = tmp.ItemSubType_Merck  
                     where lower(dp.ItemSubType_Merck) =  'interm' 
                          and f_prodorder.dim_partidheader is not null 
                           and f_prodorder.dim_batchid is not null 
                           and f_prodorder.dim_plantid is not null 
                              ; 
drop table if exists temp_antigen_antigen; 
create table temp_antigen_antigen as 
                         select distinct f_mm.dim_plantid, 
                             f_mm.dim_partid, 
                             f_mm.dim_batchid, 
                             f_prodord.dd_ordernumber, 
                             f_prodord.dim_dateidactualheaderfinish, 
                             f_prodord.dim_dateidactualrelease, 
                             f_prodord.dim_dateidactualstart, 
                             f_prodord.dim_partidheader, 
                             f_prodord.dim_batchid as dim_batchidprod 
                         from fact_materialmovement f_mm, 
                             fact_productionorder f_prodord, 
                             dim_movementtype mt, 
                             dim_part dp, 
                             dim_part dph 
                         where f_mm.dim_movementtypeid = mt.dim_movementtypeid 
                             and  mt.movementtype in ('261','262') 
                             and f_mm.dim_plantid = f_prodord.dim_plantid 
                             and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber 
                             and dp.dim_partid = f_mm.dim_partid 
                             and dph.dim_partid = f_prodord.dim_partidheader 
                             and lower(dp.ItemSubType_Merck) = 'interm' 
                             and lower(dph.ItemSubType_Merck) = 'interm'; 
drop table if exists temp_bulk_fpp; 
create table temp_bulk_fpp as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'bulk'   
                 and lower(dph.ItemSubType_Merck) = 'fpp'; 
drop table if exists temp_raw_fpu; 
create table temp_raw_fpu as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'raw'   
                 and lower(dph.ItemSubType_Merck) = 'fpu'; 
drop table if exists temp_antigen_bulk; 
create table temp_antigen_bulk as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'interm'   
                 and lower(dph.ItemSubType_Merck) = 'bulk'; 
drop table if exists temp_antigen_fpp; 
create table temp_antigen_fpp as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'interm'   
                 and lower(dph.ItemSubType_Merck) = 'fpp'; 
drop table if exists temp_bulk; 
create table temp_bulk as 
                     select distinct f_prodorder.dim_plantid, 
                             f_prodorder.dim_partidheader as dim_partid, 
                             f_prodorder.dim_batchid, 
                             ifnull(f_insp.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno, 
                             f_prodorder.dd_ordernumber, 
                             convert(varchar(50), 'Not Set') dd_DocumentNo, 
                             convert(decimal (18,0), 0) dd_DocumentItemNo, 
                             convert(decimal (18,0), 0) dd_scheduleno, 
                             convert(bigint, 1) dim_dateidschedorder, 
                             convert(bigint, 1) dim_dateidcreate, 
                             convert(bigint, 1) dim_dateidpostingdate, 
                             ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr, 
                             ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco, 
                             ifnull(f_insp.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart, 
                             ifnull(f_insp.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade, 
                             tmp.dim_partid as dim_partid_interpl, 
                             tmp.dim_batchid as dim_batchid_interpl, 
                             tmp.plant as dim_plantid_interpl, 
                             f_prodorder.dim_dateidactualrelease as dim_dateidactualrelease 
                     from fact_productionorder f_prodorder  
                     left join fact_inspectionlot f_insp on f_insp.dim_batchid = f_prodorder.dim_batchid 
                     and f_insp.dim_partid = f_prodorder.dim_partidheader 
                     and f_insp.dim_plantid = f_prodorder.dim_plantid 
                     and f_prodorder.dd_ordernumber = f_insp.dd_OrderNo 
                     inner join dim_part dp on f_prodorder.dim_partidheader = dp.dim_partid 
                     left join dim_ProductionOrderType dpot on f_prodorder.dim_productionordertypeid = dpot.dim_productionordertypeid 
                         and TypeCode <> 'ZRWK' 
                     left join temp_interplants tmp  
                         on f_prodorder.dim_plantid = tmp.plantsupplying  
                          and f_prodorder.dd_ordernumber =  tmp.dd_productionordernumber  
                          and dp.ItemSubType_Merck = tmp.ItemSubType_Merck  
                     where lower(dp.ItemSubType_Merck) =  'bulk' 
                          and f_prodorder.dim_partidheader is not null 
                           and f_prodorder.dim_batchid is not null 
                           and f_prodorder.dim_plantid is not null 
                              ; 
drop table if exists temp_bulk_bulk; 
create table temp_bulk_bulk as 
                         select distinct f_mm.dim_plantid, 
                             f_mm.dim_partid, 
                             f_mm.dim_batchid, 
                             f_prodord.dd_ordernumber, 
                             f_prodord.dim_dateidactualheaderfinish, 
                             f_prodord.dim_dateidactualrelease, 
                             f_prodord.dim_dateidactualstart, 
                             f_prodord.dim_partidheader, 
                             f_prodord.dim_batchid as dim_batchidprod 
                         from fact_materialmovement f_mm, 
                             fact_productionorder f_prodord, 
                             dim_movementtype mt, 
                             dim_part dp, 
                             dim_part dph 
                         where f_mm.dim_movementtypeid = mt.dim_movementtypeid 
                             and  mt.movementtype in ('261','262') 
                             and f_mm.dim_plantid = f_prodord.dim_plantid 
                             and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber 
                             and dp.dim_partid = f_mm.dim_partid 
                             and dph.dim_partid = f_prodord.dim_partidheader 
                             and lower(dp.ItemSubType_Merck) = 'bulk' 
                             and lower(dph.ItemSubType_Merck) = 'bulk'; 
drop table if exists temp_fpp; 
create table temp_fpp as 
                     select distinct f_prodorder.dim_plantid, 
                             f_prodorder.dim_partidheader as dim_partid, 
                             f_prodorder.dim_batchid, 
                             ifnull(f_insp.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno, 
                             f_prodorder.dd_ordernumber, 
                             convert(varchar(50), 'Not Set') dd_DocumentNo, 
                             convert(decimal (18,0), 0) dd_DocumentItemNo, 
                             convert(decimal (18,0), 0) dd_scheduleno, 
                             convert(bigint, 1) dim_dateidschedorder, 
                             convert(bigint, 1) dim_dateidcreate, 
                             convert(bigint, 1) dim_dateidpostingdate, 
                             ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr, 
                             ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco, 
                             ifnull(f_insp.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart, 
                             ifnull(f_insp.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade, 
                             tmp.dim_partid as dim_partid_interpl, 
                             tmp.dim_batchid as dim_batchid_interpl, 
                             tmp.plant as dim_plantid_interpl, 
                             f_prodorder.dim_dateidactualrelease as dim_dateidactualrelease 
                     from fact_productionorder f_prodorder  
                     left join fact_inspectionlot f_insp on f_insp.dim_batchid = f_prodorder.dim_batchid 
                     and f_insp.dim_partid = f_prodorder.dim_partidheader 
                     and f_insp.dim_plantid = f_prodorder.dim_plantid 
                     and f_prodorder.dd_ordernumber = f_insp.dd_OrderNo 
                     inner join dim_part dp on f_prodorder.dim_partidheader = dp.dim_partid 
                     left join dim_ProductionOrderType dpot on f_prodorder.dim_productionordertypeid = dpot.dim_productionordertypeid 
                         and TypeCode <> 'ZRWK' 
                     left join temp_interplants tmp  
                         on f_prodorder.dim_plantid = tmp.plantsupplying  
                          and f_prodorder.dd_ordernumber =  tmp.dd_productionordernumber  
                          and dp.ItemSubType_Merck = tmp.ItemSubType_Merck  
                     where lower(dp.ItemSubType_Merck) =  'fpp' 
                          and f_prodorder.dim_partidheader is not null 
                           and f_prodorder.dim_batchid is not null 
                           and f_prodorder.dim_plantid is not null 
                              ; 
drop table if exists temp_fpp_fpp; 
create table temp_fpp_fpp as 
                         select distinct f_mm.dim_plantid, 
                             f_mm.dim_partid, 
                             f_mm.dim_batchid, 
                             f_prodord.dd_ordernumber, 
                             f_prodord.dim_dateidactualheaderfinish, 
                             f_prodord.dim_dateidactualrelease, 
                             f_prodord.dim_dateidactualstart, 
                             f_prodord.dim_partidheader, 
                             f_prodord.dim_batchid as dim_batchidprod 
                         from fact_materialmovement f_mm, 
                             fact_productionorder f_prodord, 
                             dim_movementtype mt, 
                             dim_part dp, 
                             dim_part dph 
                         where f_mm.dim_movementtypeid = mt.dim_movementtypeid 
                             and  mt.movementtype in ('261','262') 
                             and f_mm.dim_plantid = f_prodord.dim_plantid 
                             and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber 
                             and dp.dim_partid = f_mm.dim_partid 
                             and dph.dim_partid = f_prodord.dim_partidheader 
                             and lower(dp.ItemSubType_Merck) = 'fpp' 
                             and lower(dph.ItemSubType_Merck) = 'fpp'; 
drop table if exists temp_raw_antigen; 
create table temp_raw_antigen as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'raw'   
                 and lower(dph.ItemSubType_Merck) = 'interm'; 
drop table if exists temp_raw; 
create table temp_raw as 
                     select 
                         distinct f_mm.dim_plantid, 
                         f_mm.dim_partid, 
                         f_mm.dim_batchid, 
                         ifnull(f_ins.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno, 
                         convert(varchar(12), 'Not Set') as dd_ordernumber,  
                         f_mm.dd_DocumentNo, 
                         f_mm.dd_DocumentItemNo, 
                         f_pur.dd_scheduleno, 
                         f_pur.dim_dateidschedorder, 
                         f_pur.dim_dateidcreate, 
                         f_mm.dim_dateidpostingdate, 
                         ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr, 
                         ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco, 
                         ifnull(f_ins.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart, 
                         ifnull(f_ins.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade, 
                         1 as dim_dateidactualrelease 
                     from fact_materialmovement f_mm 
                         inner join fact_purchase f_pur on  f_mm.dim_plantid = f_pur.dim_plantidordering 
                         and f_mm.dim_partid = f_pur.dim_partid 
                         and f_pur.dd_documentno = f_mm.dd_documentno 
                         and f_pur.dd_documentitemno = f_mm.dd_documentitemno 
                     left join fact_inspectionlot f_ins on  f_mm.dim_plantid = f_ins.dim_plantid 
                         and f_mm.dim_partid = f_ins.dim_partid 
                         and f_mm.dim_batchid = f_ins.dim_batchid 
                     inner join dim_movementtype mt on f_mm.dim_movementtypeid = mt.dim_movementtypeid 
                     inner join dim_part dp on f_mm.dim_partid = dp.dim_partid 
                     where  
                         mt.movementtype in ('101','102') 
                         and f_mm.dim_dateidpostingdate <> 1  
                         and f_mm.dim_plantid <> 1 
                         and f_mm.dim_partid <> 1 
                         and f_mm.dim_batchid <> 1 
                         and lower(dp.ItemSubType_Merck) = 'raw'; 
drop table if exists temp_raw_raw; 
create table temp_raw_raw as 
                         select distinct f_mm.dim_plantid, 
                             f_mm.dim_partid, 
                             f_mm.dim_batchid, 
                             f_prodord.dd_ordernumber, 
                             f_prodord.dim_dateidactualheaderfinish, 
                             f_prodord.dim_dateidactualrelease, 
                             f_prodord.dim_dateidactualstart, 
                             f_prodord.dim_partidheader, 
                             f_prodord.dim_batchid as dim_batchidprod 
                         from fact_materialmovement f_mm, 
                             fact_productionorder f_prodord, 
                             dim_movementtype mt, 
                             dim_part dp, 
                             dim_part dph 
                         where f_mm.dim_movementtypeid = mt.dim_movementtypeid 
                             and  mt.movementtype in ('261','262') 
                             and f_mm.dim_plantid = f_prodord.dim_plantid 
                             and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber 
                             and dp.dim_partid = f_mm.dim_partid 
                             and dph.dim_partid = f_prodord.dim_partidheader 
                             and lower(dp.ItemSubType_Merck) = 'raw' 
                             and lower(dph.ItemSubType_Merck) = 'raw'; 
drop table if exists temp_fpu_fpp; 
create table temp_fpu_fpp as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'fpu'   
                 and lower(dph.ItemSubType_Merck) = 'fpp'; 
drop table if exists temp_bulk_fpu; 
create table temp_bulk_fpu as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'bulk'   
                 and lower(dph.ItemSubType_Merck) = 'fpu'; 
drop table if exists temp_raw_bulk; 
create table temp_raw_bulk as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'raw'   
                 and lower(dph.ItemSubType_Merck) = 'bulk'; 
drop table if exists temp_raw_fpp; 
create table temp_raw_fpp as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'raw'   
                 and lower(dph.ItemSubType_Merck) = 'fpp'; 
drop table if exists temp_antigen_fpu; 
create table temp_antigen_fpu as 
             select distinct f_mm.dim_plantid,  
                 f_mm.dim_partid,  
                 f_mm.dim_batchid,  
                 f_prodord.dd_ordernumber,  
                 f_prodord.dim_dateidactualheaderfinish,  
                 f_prodord.dim_dateidactualrelease,  
                 f_prodord.dim_dateidactualstart,  
                 f_prodord.dim_partidheader,  
                 f_prodord.dim_batchid as dim_batchidprod  
             from fact_materialmovement f_mm,  
                 fact_productionorder f_prodord,  
                 dim_movementtype mt,  
                 dim_part dp,  
                 dim_part dph  
             where f_mm.dim_movementtypeid = mt.dim_movementtypeid  
                 and  mt.movementtype in ('261','262')  
                 and f_mm.dim_plantid = f_prodord.dim_plantid  
                 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber  
                 and dp.dim_partid = f_mm.dim_partid  
                 and dph.dim_partid = f_prodord.dim_partidheader  
                 and lower(dp.ItemSubType_Merck) = 'interm'   
                 and lower(dph.ItemSubType_Merck) = 'fpu'; 
drop table if exists temp_fpu; 
create table temp_fpu as 
                     select distinct f_prodorder.dim_plantid, 
                             f_prodorder.dim_partidheader as dim_partid, 
                             f_prodorder.dim_batchid, 
                             ifnull(f_insp.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno, 
                             f_prodorder.dd_ordernumber, 
                             convert(varchar(50), 'Not Set') dd_DocumentNo, 
                             convert(decimal (18,0), 0) dd_DocumentItemNo, 
                             convert(decimal (18,0), 0) dd_scheduleno, 
                             convert(bigint, 1) dim_dateidschedorder, 
                             convert(bigint, 1) dim_dateidcreate, 
                             convert(bigint, 1) dim_dateidpostingdate, 
                             ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr, 
                             ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco, 
                             ifnull(f_insp.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart, 
                             ifnull(f_insp.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade, 
                             tmp.dim_partid as dim_partid_interpl, 
                             tmp.dim_batchid as dim_batchid_interpl, 
                             tmp.plant as dim_plantid_interpl, 
                             f_prodorder.dim_dateidactualrelease as dim_dateidactualrelease 
                     from fact_productionorder f_prodorder  
                     left join fact_inspectionlot f_insp on f_insp.dim_batchid = f_prodorder.dim_batchid 
                     and f_insp.dim_partid = f_prodorder.dim_partidheader 
                     and f_insp.dim_plantid = f_prodorder.dim_plantid 
                     and f_prodorder.dd_ordernumber = f_insp.dd_OrderNo 
                     inner join dim_part dp on f_prodorder.dim_partidheader = dp.dim_partid 
                     left join dim_ProductionOrderType dpot on f_prodorder.dim_productionordertypeid = dpot.dim_productionordertypeid 
                         and TypeCode <> 'ZRWK' 
                     left join temp_interplants tmp  
                         on f_prodorder.dim_plantid = tmp.plantsupplying  
                          and f_prodorder.dd_ordernumber =  tmp.dd_productionordernumber  
                          and dp.ItemSubType_Merck = tmp.ItemSubType_Merck  
                     where lower(dp.ItemSubType_Merck) =  'fpu' 
                          and f_prodorder.dim_partidheader is not null 
                           and f_prodorder.dim_batchid is not null 
                           and f_prodorder.dim_plantid is not null 
                              ; 
drop table if exists temp_fpu_fpu; 
create table temp_fpu_fpu as 
                         select distinct f_mm.dim_plantid, 
                             f_mm.dim_partid, 
                             f_mm.dim_batchid, 
                             f_prodord.dd_ordernumber, 
                             f_prodord.dim_dateidactualheaderfinish, 
                             f_prodord.dim_dateidactualrelease, 
                             f_prodord.dim_dateidactualstart, 
                             f_prodord.dim_partidheader, 
                             f_prodord.dim_batchid as dim_batchidprod 
                         from fact_materialmovement f_mm, 
                             fact_productionorder f_prodord, 
                             dim_movementtype mt, 
                             dim_part dp, 
                             dim_part dph 
                         where f_mm.dim_movementtypeid = mt.dim_movementtypeid 
                             and  mt.movementtype in ('261','262') 
                             and f_mm.dim_plantid = f_prodord.dim_plantid 
                             and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber 
                             and dp.dim_partid = f_mm.dim_partid 
                             and dph.dim_partid = f_prodord.dim_partidheader 
                             and lower(dp.ItemSubType_Merck) = 'fpu' 
                             and lower(dph.ItemSubType_Merck) = 'fpu'; 
