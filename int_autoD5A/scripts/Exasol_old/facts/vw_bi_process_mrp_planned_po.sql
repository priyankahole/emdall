/*********************************************Change History*******************************************************/
/*Date             By        Version           Desc                                                            */
/*   03 Mar 2015      Liviu Ionescu              Add Combine for fact_planorder */
/******************************************************************************************************************/

UPDATE fact_planorder po
SET po.Dim_DateidMRPSched = (case when fm.dim_dateidreschedule = 1 then fm.dim_dateidoriginaldock 
                                     else fm.dim_dateidreschedule end)
From    dim_plant pl ,
        dim_part prt ,
        fact_mrp fm,
        PLSC p ,
        dim_mrpelement me ,
        dim_date dt ,
        dim_date dt1,
        fact_planorder po
WHERE pl.dim_plantid = po.dim_plantid
AND prt.dim_partid = po.dim_partid
AND  fm.dd_Documentno = po.dd_PlanOrderNo
AND fm.dim_partid = prt.dim_partid
AND fm.dd_PlannScenarioLTP = po.dd_PlannScenario
AND p.PLSC_PLSCN = po.dd_PlannScenario
AND me.Dim_MRPElementID = fm.Dim_MRPElementID
AND dt.dim_dateid = fm.dim_dateidreschedule
AND dt1.dim_dateid = fm.dim_dateidoriginaldock
AND me.MRPElement <> 'U3'
AND (case when fm.dim_dateidreschedule = 1 then dt1.DateValue else dt.DateValue end) BETWEEN p.PLSC_PDAT1 AND p.PLSC_PDAT2
AND  po.Dim_DateidMRPSched <> case when fm.dim_dateidreschedule = 1 then fm.dim_dateidoriginaldock else fm.dim_dateidreschedule end;


MERGE INTO fact_planorder po 
USING (select po.fact_planorderid, max(case when fm.dim_dateidreschedule = 1 then fm.dim_dateidoriginaldock else 1 end) Dim_DateidMRPDock
	From    dim_plant pl ,
        dim_part prt ,
        fact_mrp fm,
        PLSC p ,
        dim_mrpelement me ,
        dim_date dt ,
        dim_date dt1,
		fact_planorder po
	WHERE pl.dim_plantid = po.dim_plantid
	AND prt.dim_partid = po.dim_partid
	AND  fm.dd_Documentno = po.dd_PlanOrderNo
	AND fm.dim_partid = prt.dim_partid
	AND fm.dd_PlannScenarioLTP = po.dd_PlannScenario
	AND p.PLSC_PLSCN = po.dd_PlannScenario
	AND me.Dim_MRPElementID = fm.Dim_MRPElementID
	AND dt.dim_dateid = fm.dim_dateidreschedule
	AND dt1.dim_dateid = fm.dim_dateidoriginaldock
	AND me.MRPElement <> 'U3'
	AND (case when fm.dim_dateidreschedule = 1 then dt1.DateValue else dt.DateValue end) BETWEEN p.PLSC_PDAT1 AND p.PLSC_PDAT2
	AND po.Dim_DateidMRPDock <> case when fm.dim_dateidreschedule = 1 then fm.dim_dateidoriginaldock else 1 end
	group by po.fact_planorderid) src
ON po.fact_planorderid = src.fact_planorderid
WHEN MATCHED THEN UPDATE SET po.Dim_DateidMRPDock = src.Dim_DateidMRPDock;

        
TRUNCATE TABLE fact_plord_tmp;

/* fn busines date function code */
Drop table if exists ti_p09;
Create table ti_p09 as
Select datevalue,companycode,plantcode_factory,row_number() over(partition by companycode,plantcode_factory order by datevalue asc) seq
From dim_date
where IsaPublicHoliday =0;

Drop table if exists businessdate_p09;
Create table businessdate_p09 as
Select dt.DateValue,dt.CompanyCode,dt.plantcode_factory,-1*fm.ct_DaysGRPProcessing rp, 
TO_DATE('0001-01-01', 'YYYY-MM-DD') mrpdockdate, CONVERT (BIGINT, null) seqno
From   fact_planorder po
        INNER JOIN dim_plant pl ON pl.dim_plantid = po.dim_plantid
        INNER JOIN dim_part prt ON prt.dim_partid = po.dim_partid
        INNER JOIN fact_mrp fm
              ON     fm.dd_Documentno = po.dd_PlanOrderNo
                  AND fm.dim_partid = prt.dim_partid
                  AND fm.dd_PlannScenarioLTP = po.dd_PlannScenario
        INNER JOIN PLSC p ON p.PLSC_PLSCN = po.dd_PlannScenario
        INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = fm.Dim_MRPElementID
        INNER JOIN dim_date dt ON dt.dim_dateid = fm.dim_dateidreschedule
  WHERE me.MRPElement <> 'U3' AND po.Dim_DateidMRPDock = 1
        AND dt.DateValue BETWEEN p.PLSC_PDAT1 AND p.PLSC_PDAT2;

Update businessdate_p09 bp
Set bp.seqno= ifnull( tmp.seq ,0)
FROM  businessdate_p09 bp
      LEFT JOIN ti_p09 tmp ON tmp.datevalue = bp.datevalue 
                           AND tmp.companycode = bp.companycode
			   AND tmp.plantcode_factory = bp.plantcode_factory;

Update businessdate_p09 bp
Set mrpdockdate= ifnull( datevalue , TO_DATE('0001-01-01', 'YYYY-MM-DD'))
FROM businessdate_p09 bp
	LEFT JOIN ti_p09 tmp ON tmp.seq =(bp.seqno + (bp.rp)) 
	                     and tmp.companycode = bp.companycode;
/* END busines date function code */

INSERT INTO fact_plord_tmp
				(fact_planorderid,
				 companycode,
				 mrpdockdate)
select po.Fact_planorderid,
 	   dt.CompanyCode,
       ifnull( mrpdockdate, TO_DATE('0001-01-01', 'YYYY-MM-DD'))	
from fact_planorder po
        INNER JOIN dim_plant pl ON pl.dim_plantid = po.dim_plantid
        INNER JOIN dim_part prt ON prt.dim_partid = po.dim_partid
        INNER JOIN fact_mrp fm
              ON     fm.dd_Documentno = po.dd_PlanOrderNo
                  AND fm.dim_partid = prt.dim_partid
                  AND fm.dd_PlannScenarioLTP = po.dd_PlannScenario
        INNER JOIN PLSC p ON p.PLSC_PLSCN = po.dd_PlannScenario
        INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = fm.Dim_MRPElementID
        INNER JOIN dim_date dt ON dt.dim_dateid = fm.dim_dateidreschedule
        LEFT JOIN businessdate_p09 bp ON bp.CompanyCode = dt.CompanyCode and rp = (-1*fm.ct_DaysGRPProcessing) and bp.datevalue = dt.DateValue AND dt.plantcode_factory = bp.plantcode_factory
  WHERE me.MRPElement <> 'U3' AND po.Dim_DateidMRPDock = 1
        AND dt.DateValue BETWEEN p.PLSC_PDAT1 AND p.PLSC_PDAT2;

DROP TABLE IF EXISTS TMP_UPDT_PORD;
CREATE TABLE TMP_UPDT_PORD
AS
SELECT fact_planorderid, companycode, mrpdockdate, row_number() over(partition by fact_planorderid order by mrpdockdate desc) rono FROM fact_plord_tmp;

UPDATE fact_planorder po
set po.Dim_DateidMRPDock = dt.Dim_DateId
From TMP_UPDT_PORD  t ,
   dim_date dt, fact_planorder po, dim_plant pl 
Where po.fact_planorderid = t.fact_planorderid
AND dt.datevalue = t.MRPDockDate and dt.companycode = t.companycode and t.rono = 1
AND pl.dim_plantid = po.dim_plantid AND pl.plantcode = dt.plantcode_factory;

DROP TABLE IF EXISTS TMP_UPDT_PORD;		

  
UPDATE fact_planorder po 
SET po.Dim_DateIdWeekEnding = dt1.dim_dateid 
FROM fact_planorder po      
    INNER JOIN dim_Date dt ON dt.Dim_DateId = po.Dim_DateidMRPDock 
    INNER JOIN dim_plant pl ON pl.dim_plantid = po.dim_plantid
	LEFT JOIN dim_date dt1 ON dt1.CompanyCode = pl.Companycode AND dt1.plantcode_factory = pl.plantcode
               
WHERE po.Dim_DateidMRPDock <> 1
AND  dt1.datevalue = 
                (CASE TO_CHAR(dt.datevalue, 'D')
		              WHEN 1 THEN (dt.datevalue + ( INTERVAL '6' DAY))
		              WHEN 2 THEN (dt.datevalue + ( INTERVAL '5' DAY))
		              WHEN 3 THEN (dt.datevalue + ( INTERVAL '4' DAY))
		              WHEN 4 THEN (dt.datevalue + ( INTERVAL '3' DAY))
		              WHEN 5 THEN (dt.datevalue + ( INTERVAL '2' DAY))
		              WHEN 6 THEN (dt.datevalue + ( INTERVAL '1' DAY))
		              ELSE dt.datevalue
		           END)
AND pl.rowiscurrent = 1;


Drop table if exists interval_tmp_p01;
Create table interval_tmp_p01 As
Select   po.fact_planorderid,
	 (dt.DateValue - (( INTERVAL '1' day) * pt.LeadTime )) calcolumn,
	 pl.CompanyCode
From     fact_planorder po,
	 dim_part pt ,
	 dim_plant pl ,
	 dim_Date dt
Where    po.Dim_DateidMRPDock <> 1
	 AND  po.Dim_PartId = pt.Dim_PartId AND pt.RowIsCurrent = 1
	 AND  pl.dim_plantid = po.dim_plantid AND pl.rowiscurrent = 1
	 AND dt.Dim_DateId = po.Dim_DateidMRPDock;

UPDATE fact_planorder po
SET po.Dim_DateIdMRPStartDate = ifnull(dt1.dim_dateid, 1)
FROM fact_planorder po
      INNER JOIN dim_plant pl ON pl.dim_plantid = po.dim_plantid
      INNER JOIN interval_tmp_p01 itp  ON  po.fact_planorderid = itp.fact_planorderid
      LEFT JOIN dim_date dt1 ON dt1.datevalue = itp.calcolumn
                    		  AND dt1.companycode = itp.CompanyCode
WHERE dt1.plantcode_factory = pl.plantcode
AND po.Dim_DateIdMRPStartDate <> ifnull(dt1.dim_dateid, 1);


Drop table if exists interval_tmp_p01;


