DROP TABLE IF EXISTS tmp_fact_maintenanceplan;

CREATE TABLE tmp_fact_maintenanceplan
LIKE fact_maintenanceplan INCLUDING DEFAULTS INCLUDING IDENTITY;

delete from number_fountain m where m.table_name = 'tmp_fact_maintenanceplan';
insert into number_fountain
select 'tmp_fact_maintenanceplan',
ifnull(max(f.fact_maintenanceplanid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_maintenanceplan f;

insert into tmp_fact_maintenanceplan (
fact_maintenanceplanid,
dd_maintenanceplan,
dd_maintenanceitem,
dd_maintplancallnumber,
dd_maintacknumber,
dim_dateidnextplandate,
dim_dateidstartdate,
dim_dateidlastcompconfdate,
dim_dateidcalldate,
dim_maintenanceitemid,
dim_maintenanceplanid,
dim_plantid,
DW_UPDATE_DATE,
DW_INSERT_DATE,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_CURRENCYID_GBL,
DIM_CURRENCYID,
DIM_CURRENCYID_TRA)
SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_maintenanceplan') + row_number() over(order by '') AS fact_maintenanceplanid,
ifnull(MPLA_WARPL,'Not Set') as dd_maintenanceplan,
ifnull(MPOS_WAPOS,'Not Set') as dd_maintenanceitem,
ifnull(MHIS_ABNUM,0) as dd_maintplancallnumber,
ifnull(MHIS_ZAEHL,0) as dd_maintacknumber,
1 as dim_dateidnextplandate,
1 as dim_dateidstartdate,
1 as dim_dateidlastcompconfdate,
1 as dim_dateidcalldate,
1 as dim_maintenanceitemid,
1 as dim_maintenanceplanid,
1 as dim_plantid,
current_date as DW_UPDATE_DATE,
current_date as DW_INSERT_DATE,
1 as AMT_EXCHANGERATE_GBL,
1 as AMT_EXCHANGERATE,
1 as DIM_CURRENCYID_GBL,
1 as DIM_CURRENCYID,
1 as DIM_CURRENCYID_TRA
from MPLA
inner join MPOS on ifnull(MPLA_WARPL, 'Not Set') = ifnull(MPOS_WARPL, 'Not Set')
LEFT join MHIS on ifnull(MPOS_WARPL, 'Not Set')= ifnull(MHIS_WARPL,'Not Set')
where 
 not exists (select 1 from tmp_fact_maintenanceplan
                 where dd_maintenanceplan =ifnull(MPLA_WARPL,'Not Set')
				      and dd_maintenanceitem = ifnull(MPOS_WAPOS,'Not Set')
                      and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
					  and dd_maintacknumber = ifnull(MHIS_ZAEHL,0))
and  MPLA_MPTYP <> 'ST';




merge into tmp_fact_maintenanceplan f
using
(select distinct f.fact_maintenanceplanid, ifnull(pl.dim_plantid,1) as dim_plantid
from tmp_fact_maintenanceplan f 
INNER JOIN MPOS 
ON dd_maintenanceplan =ifnull(MPOS_WARPL, 'Not Set')
AND dd_maintenanceitem = ifnull(MPOS_WAPOS,'Not Set')
INNER JOIN dim_plant pl
on pl.plantcode=ifnull(MPOS_IWERK,'Not Set')
LEFT JOIN MHIS
ON dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
and ifnull(MPOS_WARPL, 'Not Set')= ifnull(MHIS_WARPL,'Not Set'))t
on t.fact_maintenanceplanid = f.fact_maintenanceplanid
when matched then update
set f.dim_plantid=t.dim_plantid
where f.dim_plantid<>t.dim_plantid;


merge into tmp_fact_maintenanceplan f
using
(select distinct f.fact_maintenanceplanid, dt.dim_dateid as dim_dateid
from tmp_fact_maintenanceplan f
INNER JOIN dim_plant pl
ON f.dim_plantid=pl.dim_plantid
LEFT JOIN MHIS
on dd_maintenanceplan =ifnull(MHIS_WARPL,'Not Set')
and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
INNER JOIN dim_date dt
on pl.companycode=dt.companycode
and dt.plantcode_factory=pl.plantcode
and dt.datevalue=ifnull(MHIS_NPLDA,'0001-01-01')
)t
on t.fact_maintenanceplanid = f.fact_maintenanceplanid
when matched then update
set f.dim_dateidnextplandate = t.dim_dateid
where f.dim_dateidnextplandate <> t.dim_dateid;



merge into tmp_fact_maintenanceplan f
using
(select distinct f.fact_maintenanceplanid, dt.dim_dateid as dim_dateid
from tmp_fact_maintenanceplan f
INNER JOIN dim_plant pl
ON f.dim_plantid=pl.dim_plantid
LEFT JOIN MHIS
on dd_maintenanceplan =ifnull(MHIS_WARPL,'Not Set')
and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
INNER JOIN dim_date dt
on pl.companycode=dt.companycode
and dt.plantcode_factory=pl.plantcode
and dt.datevalue=ifnull(MHIS_STADT,'0001-01-01')
)t
on t.fact_maintenanceplanid = f.fact_maintenanceplanid
when matched then update 
set f.dim_dateidstartdate = t.dim_dateid
where f.dim_dateidstartdate <> t.dim_dateid;



update tmp_fact_maintenanceplan f
set f.dim_dateidlastcompconfdate=dt.dim_dateid
from tmp_fact_maintenanceplan f
INNER JOIN dim_plant pl
ON f.dim_plantid=pl.dim_plantid
LEFT JOIN MHIS
on dd_maintenanceplan =ifnull(MHIS_WARPL,'Not Set')
and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
INNER JOIN dim_date dt
on pl.companycode=dt.companycode
and dt.plantcode_factory=pl.plantcode
and dt.datevalue=ifnull(MHIS_LRMDT,'0001-01-01')
where f.dim_dateidlastcompconfdate <> dt.dim_dateid;

update tmp_fact_maintenanceplan f
set f.dim_dateidcalldate=dt.dim_dateid
from tmp_fact_maintenanceplan f
INNER JOIN dim_plant pl
ON f.dim_plantid=pl.dim_plantid
LEFT JOIN MHIS
on dd_maintenanceplan =ifnull(MHIS_WARPL,'Not Set')
and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
INNER JOIN dim_date dt
on pl.companycode=dt.companycode
and dt.plantcode_factory=pl.plantcode
and dt.datevalue=ifnull(MHIS_HORDA,'0001-01-01')
where f.dim_dateidcalldate <> dt.dim_dateid;

merge into tmp_fact_maintenanceplan f
using (
select distinct f.fact_maintenanceplanid, m.dim_maintenanceitemid
from tmp_fact_maintenanceplan f
inner join MPOS
on dd_maintenanceplan =ifnull(MPOS_WARPL, 'Not Set')
and dd_maintenanceitem = ifnull(MPOS_WAPOS,'Not Set')
inner join dim_maintenanceitem m
on ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
left join MHIS
on ifnull(MPOS_WARPL, 'Not Set')= ifnull(MHIS_WARPL,'Not Set')
and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
where f.dim_maintenanceitemid<>ifnull(m.dim_maintenanceitemid,1)
) t
on t.fact_maintenanceplanid=f.fact_maintenanceplanid
when matched then update set f.dim_maintenanceitemid=ifnull(t.dim_maintenanceitemid,1) ;

merge into tmp_fact_maintenanceplan f
using (
select distinct f.fact_maintenanceplanid, m.dim_maintenanceplanid
from tmp_fact_maintenanceplan f
inner join MPLA 
on dd_maintenanceplan =ifnull(MPLA_WARPL, 'Not Set')
inner join MPOS
on dd_maintenanceitem = ifnull(MPOS_WAPOS,'Not Set')
inner join dim_maintenanceplan m
on ifnull(MPLA_WARPL, 'Not Set') = maintenanceplan
left join MHIS
on ifnull(MPOS_WARPL, 'Not Set')= ifnull(MHIS_WARPL,'Not Set')
and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
where f.dim_maintenanceplanid<>ifnull(m.dim_maintenanceplanid,1)
) t
on t.fact_maintenanceplanid=f.fact_maintenanceplanid
when matched then update set f.dim_maintenanceplanid=ifnull(t.dim_maintenanceplanid,1);

merge into tmp_fact_maintenanceplan f
using (
select distinct fact_maintenanceplanid,  ifnull(T351X_KZYK1, 'Not Set') as dd_small_step_2_maint_packages
from tmp_fact_maintenanceplan f 
inner join MPLA m on dd_maintenanceplan =ifnull(MPLA_WARPL,'Not Set')
inner join T351X t on MPLA_STRAT = T351X_STRAT
inner join MPOS mp on ifnull(MPLA_WARPL, 'Not Set') = ifnull(MPOS_WARPL, 'Not Set')
and dd_maintenanceitem = ifnull(MPOS_WAPOS,'Not Set')
inner join MHIS mh on ifnull(MPOS_WARPL, 'Not Set')= ifnull(MHIS_WARPL,'Not Set')
and MHIS_ZAEHL = T351X_PAKET
and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
where MPLA_MPTYP <> 'ST'
) t
on t.fact_maintenanceplanid=f.fact_maintenanceplanid
when matched then update 
set f.dd_small_step_2_maint_packages=ifnull(t.dd_small_step_2_maint_packages, 'Not Set');

/*Alin APP-7878 vimhio-laufn field 26 feb 2018*/
merge into tmp_fact_maintenanceplan f
using
(select distinct fact_maintenanceplanid, ifnull(VIMHIO_LAUFN, 'Not Set') as laufn
from tmp_fact_maintenanceplan f
inner join MPLA 
on dd_maintenanceplan =ifnull(MPLA_WARPL, 'Not Set')
inner join MPOS
on dd_maintenanceitem = ifnull(MPOS_WAPOS,'Not Set')
inner join dim_maintenanceplan m
on ifnull(MPLA_WARPL, 'Not Set') = maintenanceplan
left join MHIS
on ifnull(MPOS_WARPL, 'Not Set')= ifnull(MHIS_WARPL,'Not Set')
and dd_maintplancallnumber = ifnull(MHIS_ABNUM,0)
and dd_maintacknumber = ifnull(MHIS_ZAEHL,0)
INNER JOIN VIMHIO v
on f.dd_maintenanceplan = ifnull(v.VIMHIO_WARPL, 'Not Set')
and f.dd_maintenanceitem = ifnull(v.VIMHIO_WAPOS, 'Not Set')
)t
on t.fact_maintenanceplanid = f.fact_maintenanceplanid
when matched then update
set f.dd_last_order = t.laufn
where f.dd_last_order <> t.laufn;

/*Alin 13 July APP-7223 - MAX added on 16th of July due to duplicates in JEST_MPLA - Ionel Ene*/
merge into tmp_fact_maintenanceplan f
using(
	select distinct fact_maintenanceplanid,
					max(case when (JEST_STAT = 'I0076' and JEST_INACT is null) then 'X'
							else 'Not Set'
						end) as dd_deletion_flag_mp
	from tmp_fact_maintenanceplan f, JEST_MPLA j             
	where f.dd_maintenanceplan =ifnull(j.MPLA_WARPL,'Not Set')
	group by fact_maintenanceplanid
 	) t
on t.fact_maintenanceplanid = f.fact_maintenanceplanid
when matched then update
set f.dd_deletion_flag_mp = t.dd_deletion_flag_mp
where f.dd_deletion_flag_mp <> t.dd_deletion_flag_mp;

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'tmp_fact_maintenanceplan';   

DROP TABLE if EXISTS fact_maintenanceplan;
RENAME tmp_fact_maintenanceplan to fact_maintenanceplan;

