
/* ################################################################################################################## */
/* */
/*   Author         : Georgiana */
/*   Created On     : 05 Sept 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   05 Sept 2016      Gergiana	1.0  		  First draft */

/******************************************************************************************************************/


delete from number_fountain m where m.table_name = 'fact_workcenter';
insert into number_fountain
select 'fact_workcenter',
ifnull(max(f.fact_workcenterid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_workcenter f;

insert into fact_workcenter (
fact_workcenterid,
dd_objecttype,
dd_objectid,
dim_dateidstartdate,
dd_workcentercategory,
dd_controlkey,
dd_deletionflag,
dd_productionSupplyArea,
dim_dateidenddate,
dd_workcenter,
dim_workcenterplantid,
dd_capacityid,
dd_shorttext,
dd_individualcapacities,
dd_starttimeinsecods,
dd_finishtimeinseconds,
dim_capacitycategoryid,
dim_unitofmeasureid,
ct_cputilizationrate,
dim_capacityplannergroupid,
dim_capacityplantid,
dd_cost,
dw_insert_date
)
SELECT
(select max_id   from number_fountain   where table_name = 'fact_workcenter') + row_number() over(order by '') AS fact_workcenterid, 
ifnull(c.CRHD_OBJTY,'Not Set'),
ifnull(c.CRHD_OBJID,0),
1 as dim_dateidstartdate,
ifnull(c.CRHD_VERWE,'Not Set'),
ifnull(c.CRHD_STEUS,'Not Set'),
ifnull(c.CRHD_LVORM,'Not Set'),
ifnull(c.CRHD_PRVBE,'Not Set'),
1 as dim_dateidenddate,
ifnull(c.CRHD_ARBPL,'Not Set'),
1 as dim_workcenterplantid,
ifnull(c.CRHD_KAPID,0),
ifnull(x.CRTX_KTEXT,'Not Set'),
ifnull(kk.KAKO_AZNOR,0),
ifnull(kk.KAKO_BEGZT,0),
ifnull(kk.KAKO_ENDZT,0),
1 as dim_capacitycategoryid,
1 as dim_unitofmeasureid,
ifnull(kk.KAKO_NGRAD,0),
1 as dim_capacityplannergroupid,
1 as dim_capacityplantid,
ifnull(t.T430_KALKZ, 'Not Set'),
current_date
from crhd c
inner join  crtx_n x
on  x.crtx_OBJTY = c.CRHD_OBJTY and x.CrtX_OBJID=c.CRHD_OBJID
left join kako kk on kk.KAKO_KAPID=c.CRHD_KAPID
left join t430 t on t.T430_STEUS=c.CRHD_STEUS
where not exists (select 1 from fact_workcenter f
                     where dd_objecttype=ifnull(c.CRHD_OBJTY,'Not Set')
						and dd_objectid=ifnull(c.CRHD_OBJID,0)
                        and dd_capacityid = ifnull(kk.KAKO_KAPID,0)
                        and dd_controlkey = ifnull(t.T430_STEUS,'Not Set'));
						
merge into fact_workcenter f
using (select distinct f.fact_workcenterid,CRHD_VERWE,c.CRHD_STEUS,c.CRHD_LVORM,c.CRHD_PRVBE,c.CRHD_ARBPL,c.CRHD_KAPID
from CRHD c, fact_workcenter f
where dd_objecttype=ifnull(c.CRHD_OBJTY,'Not Set')
	and dd_objectid=ifnull(c.CRHD_OBJID,0)) t
on t.fact_workcenterid=f.fact_workcenterid
when matched then update set
dd_workcentercategory=ifnull(t.CRHD_VERWE,'Not Set'),
dd_controlkey=ifnull(t.CRHD_STEUS,'Not Set'),
dd_deletionflag=ifnull(t.CRHD_LVORM,'Not Set'),
dd_productionSupplyArea=ifnull(t.CRHD_PRVBE,'Not Set'),
dd_workcenter=ifnull(t.CRHD_ARBPL,'Not Set'),
dd_capacityid =ifnull(t.CRHD_KAPID,0)
,dw_update_date=current_date;

merge into fact_workcenter f 
using
(select distinct fact_workcenterid, pl.dim_plantid
from CRHD c, fact_workcenter f, dim_plant pl
where dd_objecttype=ifnull(c.CRHD_OBJTY,'Not Set')
	and dd_objectid=ifnull(c.CRHD_OBJID,0)
	and pl.plantcode=ifnull(c.CRHD_WERKS,'Not Set')
	and f.dim_workcenterplantid<>pl.dim_plantid ) t 
on t.fact_workcenterid=f.fact_workcenterid
when matched then update set 
f.dim_workcenterplantid=t.dim_plantid
,dw_update_date=current_date;

merge into fact_workcenter f 
using
(select distinct fact_workcenterid, dt.dim_dateid
from CRHD c, fact_workcenter f, dim_date dt, dim_plant pl
where dd_objecttype=ifnull(c.CRHD_OBJTY,'Not Set')
	and dd_objectid=ifnull(c.CRHD_OBJID,0)
	and dt.datevalue=ifnull(c.CRHD_BEGDA,'0001-01-01')
	AND pl.dim_plantid = f.dim_workcenterplantid
	AND pl.plantcode = dt.plantcode_factory
	AND pl.companycode = dt.companycode
	and f.dim_dateidstartdate<>dt.dim_dateid ) t 
on t.fact_workcenterid=f.fact_workcenterid
when matched then update set 
f.dim_dateidstartdate=t.dim_dateid
,dw_update_date=current_date;

merge into fact_workcenter f 
using
(select distinct fact_workcenterid, dt.dim_dateid
from CRHD c, fact_workcenter f, dim_date dt, dim_plant pl
where dd_objecttype=ifnull(c.CRHD_OBJTY,'Not Set')
	and dd_objectid=ifnull(c.CRHD_OBJID,0)
	and dt.datevalue=ifnull(c.CRHD_ENDDA,'0001-01-01')
	AND pl.dim_plantid = f.dim_workcenterplantid
	AND pl.plantcode = dt.plantcode_factory
	AND pl.companycode = dt.companycode
	and f.dim_dateidenddate<>dt.dim_dateid ) t 
on t.fact_workcenterid=f.fact_workcenterid
when matched then update set 
f.dim_dateidenddate=t.dim_dateid
,dw_update_date=current_date;


merge into fact_workcenter f
using (select distinct f.fact_workcenterid, c.dim_capacitycategoryid
FROM KAKO kk, dim_capacitycategory c, fact_workcenter f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND c.capcategory = ifnull(kk.KAKO_KAPAR,'Not Set')
AND f.dim_capacitycategoryid <> c.dim_capacitycategoryid) t
on t.fact_workcenterid = f.fact_workcenterid
when matched then update set 
f.dim_capacitycategoryid = t.dim_capacitycategoryid,
dw_update_date=current_date;

merge into fact_workcenter f
using (select distinct f.fact_workcenterid, c.dim_capacityplannergroupid
FROM KAKO kk, dim_capacityplannergroup c, fact_workcenter f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND c.capplannergroup = ifnull(kk.KAKO_PLANR,'Not Set')
AND f.dim_capacityplannergroupid <> c.dim_capacityplannergroupid) t
on t.fact_workcenterid = f.fact_workcenterid
when matched then update set 
f.dim_capacityplannergroupid = t.dim_capacityplannergroupid,
dw_update_date=current_date;

merge into fact_workcenter f
using (select distinct f.fact_workcenterid, uom.dim_unitofmeasureid
FROM KAKO kk, dim_unitofmeasure uom, fact_workcenter f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND uom.uom = ifnull(kk.KAKO_MEINS,'Not Set')
AND f.dim_unitofmeasureid <> uom.dim_unitofmeasureid) t
on t.fact_workcenterid = f.fact_workcenterid
when matched then update set 
f.dim_unitofmeasureid = t.dim_unitofmeasureid
,dw_update_date=current_date;

merge into fact_workcenter f
using (select distinct f.fact_workcenterid,kk.KAKO_AZNOR,kk.KAKO_BEGZT,kk.KAKO_ENDZT,kk.KAKO_NGRAD
FROM KAKO kk, fact_workcenter f
WHERE f.dd_capacityid = kk.KAKO_KAPID) t
on t.fact_workcenterid = f.fact_workcenterid
when matched then update set 
dd_individualcapacities=ifnull(t.KAKO_AZNOR,0),
dd_starttimeinsecods = ifnull(t.KAKO_BEGZT,0),
dd_finishtimeinseconds = ifnull(t.KAKO_ENDZT,0),
ct_cputilizationrate =ifnull(t.KAKO_NGRAD,0)
,dw_update_date=current_date;

merge into fact_workcenter f
using (select distinct f.fact_workcenterid, x.CRTX_KTEXT
FROM CRTX_N x, fact_workcenter f
where dd_objecttype=ifnull(x.CRTX_OBJTY,'Not Set')
and dd_objectid=ifnull(x.CRTX_OBJID,0)
and dd_shorttext <> ifnull(x.CRTX_KTEXT,'Not Set')) t 
on t.fact_workcenterid = f.fact_workcenterid
when matched then update set
dd_shorttext = ifnull(t.CRTX_KTEXT,'Not Set')
,dw_update_date=current_date;

merge into fact_workcenter f
using (select distinct f.fact_workcenterid, t.T430_KALKZ
FROM t430 t, fact_workcenter f
where dd_controlkey = ifnull(t.T430_STEUS,'Not Set')
and dd_cost <> ifnull(t.T430_KALKZ,'Not Set')) t 
on t.fact_workcenterid = f.fact_workcenterid
when matched then update set
dd_cost = ifnull(t.T430_KALKZ,'Not Set')
,dw_update_date=current_date;
