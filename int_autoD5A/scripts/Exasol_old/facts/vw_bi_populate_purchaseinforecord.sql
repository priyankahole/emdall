/* ################################################################################################################## */
/* */
/*   Author         : Octavian */
/*   Created On     : 12 Jan 2016 */
/*  */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   16 Dec 2015      Octavian	1.0  		  First draft */
/******************************************************************************************************************/
truncate table tmpdaily_fact_purchaseinforecord;

DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

delete from number_fountain m where m.table_name = 'tmpdaily_fact_purchaseinforecord';
insert into number_fountain
select 'tmpdaily_fact_purchaseinforecord',
ifnull(max(f.fact_purchaseinforecordid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmpdaily_fact_purchaseinforecord f;

DROP TABLE IF EXISTS A017_SPLIT;
CREATE TABLE A017_SPLIT AS SELECT DISTINCT
A017_MATNR,
A017_LIFNR,
A017_EKORG,
A017_WERKS,
A017_DATBI,
A017_DATAB,
A017_KNUMH FROM A017;

insert into tmpdaily_fact_purchaseinforecord(
fact_purchaseinforecordid,
dd_inforecord,
dd_purchaseorg,
dd_inforecordcategory,
dd_plant,
dd_validenddate,
dd_validstartdate,
dd_condrecno,
dd_ratecondition,
dd_rateunit)

SELECT
(select max_id   from number_fountain   where table_name = 'tmpdaily_fact_purchaseinforecord') + row_number() over(order by '') AS fact_purchaseinforecordid,
ifnull(a.INFNR,'Not Set') AS dd_inforecord,
ifnull(e.EINE_EKORG,'Not Set') AS dd_purchaseorg,
ifnull(e.EINE_ESOKZ,'Not Set') AS dd_inforecordcategory,
ifnull(e.WERKS,'Not Set') AS dd_plant,
ifnull(aa.A017_DATBI,'0001-01-01') AS dd_validenddate,
ifnull(aa.A017_DATAB,'0001-01-01') AS dd_validstartdate,
ifnull(aa.A017_KNUMH,'Not Set') AS dd_condrecno,
ifnull(k.konp_kbetr,0) as dd_ratecondition,
ifnull(k.konp_konwa,'Not Set') as dd_rateunit
FROM
EINA a inner join EINE e on a.INFNR = e.INFNR
left join A017_SPLIT aa on  a.MATNR = aa.A017_MATNR and a.LIFNR = aa.A017_LIFNR and  e.EINE_EKORG = aa.A017_EKORG and e.WERKS = aa.A017_WERKS
left join KONP_SHORT k on aa.A017_KNUMH = k.konp_KNUMH
where not exists (
SELECT 1 from tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
and pir.dd_validenddate = ifnull(aa.A017_DATBI,'0001-01-01')
and pir.dd_validstartdate = ifnull(aa.A017_DATAB,'0001-01-01')
and pir.dd_condrecno = ifnull(aa.A017_KNUMH,'Not Set')
and pir.dd_ratecondition = ifnull(k.konp_kbetr,0)
and pir.dd_rateunit = ifnull(k.konp_konwa,'Not Set')
);


UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_purchaseorgid = porg.dim_purchaseorgid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_purchaseorg porg, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND porg.purchaseorgcode = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dim_purchaseorgid <> porg.dim_purchaseorgid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_plantid = pt.dim_plantid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_plant pt, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pt.plantcode = ifnull(e.WERKS,'Not Set')
AND pir.dim_plantid <> pt.dim_plantid;

/* EINA updates */
UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_matnrusedbyvendor = ifnull(a.EINA_IDNLF,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_matnrusedbyvendor <> ifnull(a.EINA_IDNLF,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_vendorid = dv.dim_vendorid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, dim_vendor dv, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND dv.vendornumber = ifnull(a.LIFNR,'Not Set')
AND pir.dim_vendorid <> dv.dim_vendorid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_baseuomid = uom.dim_unitofmeasureid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, dim_unitofmeasure uom, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND uom.uom = ifnull(a.EINA_LMEIN,'Not Set')
AND pir.dim_baseuomid <> uom.dim_unitofmeasureid;

/* Octavian: Split when plant is not available do updates only based on MATNR */
UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_partid = dp.dim_partid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, EINE e, (SELECT dim_partid, partnumber, row_number() over (partition by partnumber order by dim_partid desc) as rn FROM dim_part) dp, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND dp.partnumber = ifnull(a.MATNR,'Not Set')
AND e.WERKS is null
AND dp.rn = 1
AND pir.dim_partid <> dp.dim_partid;


UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_partid = dp.dim_partid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, EINE e, dim_part dp, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND dp.partnumber = ifnull(a.MATNR,'Not Set')
AND dp.plant = ifnull(e.WERKS,'Not Set')
and e.WERKS is not null
AND pir.dim_partid <> dp.dim_partid;
/* Octavian: Split when plant is not available do updates only based on MATNR */

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_purchaseuomid = uom.dim_unitofmeasureid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, dim_unitofmeasure uom, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND uom.uom = ifnull(a.EINA_MEINS,'Not Set')
AND pir.dim_purchaseuomid <> uom.dim_unitofmeasureid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_generaldeletionflag = ifnull(a.LOEKZ,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_generaldeletionflag <> ifnull(a.LOEKZ,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_vendorsubrange = ifnull(a.EINA_LTSNR,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_vendorsubrange <> ifnull(a.EINA_LTSNR,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_days1remainder = ifnull(a.EINA_MAHN1,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.ct_days1remainder <> ifnull(a.EINA_MAHN1,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_days2remainder = ifnull(a.EINA_MAHN2,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.ct_days2remainder <> ifnull(a.EINA_MAHN2,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_days3remainder = ifnull(a.EINA_MAHN3,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.ct_days3remainder <> ifnull(a.EINA_MAHN3,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_regularvendor = ifnull(a.EINA_RELIF,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_regularvendor <> ifnull(a.EINA_RELIF,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_personresponsible = ifnull(a.EINA_VERKF,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_personresponsible <> ifnull(a.EINA_VERKF,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_vendor_teleph_no = ifnull(a.EINA_TELF1,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_vendor_teleph_no <> ifnull(a.EINA_TELF1,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_country_cetificate = ifnull(a.EINA_URZLA,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.dd_country_cetificate <> ifnull(a.EINA_URZLA,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_denominator = CASE WHEN a.EINA_UMREN is null or a.EINA_UMREN = 0 THEN 1 ELSE a.EINA_UMREN END,
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.ct_denominator <> CASE WHEN a.EINA_UMREN is null or a.EINA_UMREN = 0 THEN 1 ELSE a.EINA_UMREN END;


UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_numerator = ifnull(a.EINA_UMREZ,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINA a, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND pir.ct_numerator <> ifnull(a.EINA_UMREZ,0);


/* EINE */
UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_datequotationvalidid = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_date dt, dim_plant pl
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND dt.datevalue = ifnull(e.EINE_ANGDT,'0001-01-01')
AND dt.companycode = pl.companycode
AND pl.plantcode = ifnull(e.WERKS,'Not Set')
AND pl.plantcode = dt.plantcode_factory
AND pir.dim_datequotationvalidid <> dt.dim_dateid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_quotationnumber = ifnull(e.EINE_ANGNR,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_quotationnumber <> ifnull(e.EINE_ANGNR,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_deliverydays = ifnull(e.APLFZ,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.ct_deliverydays <> ifnull(e.APLFZ,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_conf_controlkey = ifnull(e.EINE_BSTAE,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_conf_controlkey <> ifnull(e.EINE_BSTAE,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_dateidlastpo = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_date dt, dim_plant pl
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND dt.datevalue = ifnull(e.EINE_DATLB,'0001-01-01')
AND dt.companycode = pl.companycode
AND pl.plantcode = ifnull(e.WERKS,'Not Set')
AND pl.plantcode = dt.plantcode_factory
AND pir.dim_dateidlastpo <> dt.dim_dateid;

/*Octavian S 14-FEB-2018 APP-8889 Temporary commented until TCURX calcualtion will be ready*/
/*UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.amt_effectiveprice = ifnull(e.EINE_EFFPR,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.amt_effectiveprice <> ifnull(e.EINE_EFFPR,0)*/

/*Octavian S 14-FEB-2018 APP-8999 Temporary update accordingly*/
/*UPDATE tmpdaily_fact_purchaseinforecord pir
   SET pir.amt_effectiveprice = IFNULL(e.eine_effpr,0) * ( power(10, ( 2 - t.CURRDEC) ))
       , dw_update_date       = CURRENT_TIMESTAMP
FROM  eine e
     ,tmpdaily_fact_purchaseinforecord pir
     ,tcurx t 
WHERE pir.dd_inforecord         = IFNULL(e.INFNR,'Not Set')
  AND pir.dd_purchaseorg        = IFNULL(e.EINE_EKORG,'Not Set')
  AND pir.dd_inforecordcategory = IFNULL(e.EINE_ESOKZ,'Not Set')
  AND pir.dd_plant              = IFNULL(e.WERKS,'Not Set')
  AND e.eine_waers              = t.currkey
  AND pir.amt_effectiveprice    <> IFNULL(e.eine_effpr,0) * ( power(10, ( 2 - t.CURRDEC) ))*/
  
/* 22 May 2018 Georgiana changes according to APP-9661- adding a different update for TCURX calculation for EINE in order to avoid data mismatch*/
/*31 Jul 2018 Revert these changes as  the new procedure is availale in STG*/
/*UPDATE EINE 
SET EINE.EINE_EFFPR = EINE.EINE_EFFPR * ( power(10, ( 2 - t.CURRDEC) )) 
FROM TCURX t,EINE 
WHERE EINE.EINE_WAERS = t.CURRKEY*/


UPDATE tmpdaily_fact_purchaseinforecord pir
   SET pir.amt_effectiveprice = IFNULL(e.eine_effpr,0) 
       , dw_update_date       = CURRENT_TIMESTAMP
FROM  eine e
     ,tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord         = IFNULL(e.INFNR,'Not Set')
  AND pir.dd_purchaseorg        = IFNULL(e.EINE_EKORG,'Not Set')
  AND pir.dd_inforecordcategory = IFNULL(e.EINE_ESOKZ,'Not Set')
  AND pir.dd_plant              = IFNULL(e.WERKS,'Not Set')
  AND pir.amt_effectiveprice    <> IFNULL(e.eine_effpr,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_purchasinggroupid = pg.dim_purchasegroupid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e,dim_purchasegroup pg
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pg.purchasegroup = ifnull(e.EINE_EKGRP,'Not Set')
AND pir.dim_purchasinggroupid <> pg.dim_purchasegroupid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_dateidrecordcreated = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_date dt, dim_plant pl
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND dt.datevalue = ifnull(e.EINE_ERDAT,'0001-01-01')
AND dt.companycode = pl.companycode
AND pl.plantcode = ifnull(e.WERKS,'Not Set')
AND pl.plantcode = dt.plantcode_factory
AND pir.dim_dateidrecordcreated <> dt.dim_dateid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_creator = ifnull(e.EINE_ERNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_creator <> ifnull(e.EINE_ERNAM,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_shippinginstr = ifnull(e.EINE_EVERS,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_shippinginstr <> ifnull(e.EINE_EVERS,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_ei_proc_foreigntrade = ifnull(e.EINE_EXPRF,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_ei_proc_foreigntrade <> ifnull(e.EINE_EXPRF,'Not Set');


UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_ind_shelflifeexpdata = ifnull(e.EINE_IPRKZ,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_ind_shelflifeexpdata <> ifnull(e.EINE_IPRKZ,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_orderacknowledgerequirement = ifnull(e.EINE_KZABS,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_orderacknowledgerequirement <> ifnull(e.EINE_KZABS,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_orderacknowledgerequirement = ifnull(e.EINE_KZABS,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_orderacknowledgerequirement <> ifnull(e.EINE_KZABS,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_deletionflag_eine = ifnull(e.EINE_LOEKZ,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_deletionflag_eine <> ifnull(e.EINE_LOEKZ,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_minremainingshelflife = ifnull(e.EINE_MHDRZ,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.ct_minremainingshelflife <> ifnull(e.EINE_MHDRZ,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_minpo = ifnull(e.EINE_MINBM,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.ct_minpo <> ifnull(e.EINE_MINBM,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_salestaxcode = ifnull(e.EINE_MWSKZ,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_salestaxcode <> ifnull(e.EINE_MWSKZ,'Not Set');

/* Andrei - APP 7189 */

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_nameforvalueaddedtax = ifnull(t.T007S_TEXT1,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, tmpdaily_fact_purchaseinforecord pir, T007S__XBI t, T005 w, T001W s
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
and t.T007S_SPRAS = 'E'
and ifnull(t.T007S_MWSKZ,'Not Set') = ifnull(e.EINE_MWSKZ,'Not Set')
and ifnull(t.T007S_KALSM, 'Not Set') = ifnull(w.T005_KALSM,'Not Set')
and ifnull(s.WERKS,'Not Set')= ifnull(e.WERKS, 'Not Set')
and ifnull(t.T007S_SPRAS,'Not Set') = ifnull(s.SPRAS,'Not Set')
/*and ifnull(s.SPRAS,'Not Set') = ifnull(w.T005_SPRAS,'Not Set')*/
and ifnull(s.LAND1,'Not Set') = ifnull(w.T005_LAND1,'Not Set')
and pir.dd_nameforvalueaddedtax <> ifnull(t.T007S_TEXT1,'Not Set');

/* END - APP 7189 */

/*Octavian S 14-FEB-2018 APP-8889 Temporary commented until TCURX calcualtion will be ready*/
/*UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.amt_netprice = ifnull(e.NETPR,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.amt_netprice <> ifnull(e.NETPR,0)*/

/*Octavian S 14-FEB-2018 APP-8999 Temporary update accordingly*/
/*UPDATE tmpdaily_fact_purchaseinforecord pir
   SET pir.amt_netprice       = IFNULL(e.netpr,0) * ( power(10, ( 2 - t.CURRDEC) ))
       , dw_update_date       = CURRENT_TIMESTAMP
FROM  eine e
     ,tmpdaily_fact_purchaseinforecord pir
     ,tcurx t 
WHERE pir.dd_inforecord         = IFNULL(e.INFNR,'Not Set')
  AND pir.dd_purchaseorg        = IFNULL(e.EINE_EKORG,'Not Set')
  AND pir.dd_inforecordcategory = IFNULL(e.EINE_ESOKZ,'Not Set')
  AND pir.dd_plant              = IFNULL(e.WERKS,'Not Set')
  AND e.eine_waers              = t.currkey
  AND pir.amt_netprice         <> IFNULL(e.netpr,0) * ( power(10, ( 2 - t.CURRDEC) ))*/
 
 /* 22 May 2018 Georgiana changes according to APP-9661- adding a different update for TCURX calculation for EINE in order to avoid data mismatch*/
/*31 Jul 2018 Revert these changes as  the new procedure is availale in STG*/
/*UPDATE EINE 
SET EINE.NETPR = EINE.NETPR * ( power(10, ( 2 - t.CURRDEC) )) 
FROM TCURX t,EINE 
WHERE EINE.EINE_WAERS = t.CURRKEY*/


UPDATE tmpdaily_fact_purchaseinforecord pir
   SET pir.amt_netprice       = IFNULL(e.netpr,0) 
       , dw_update_date       = CURRENT_TIMESTAMP
FROM  eine e
     ,tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord         = IFNULL(e.INFNR,'Not Set')
  AND pir.dd_purchaseorg        = IFNULL(e.EINE_EKORG,'Not Set')
  AND pir.dd_inforecordcategory = IFNULL(e.EINE_ESOKZ,'Not Set')
  AND pir.dd_plant              = IFNULL(e.WERKS,'Not Set')
  AND pir.amt_netprice         <> IFNULL(e.netpr,0);


UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_stdpo = ifnull(e.EINE_NORBM,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.ct_stdpo <> ifnull(e.EINE_NORBM,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.amt_priceunit = ifnull(e.EINE_PEINH,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.amt_priceunit <> ifnull(e.EINE_PEINH,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_dateidpricevalid = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_date dt, dim_plant pl
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND dt.datevalue = ifnull(e.EINE_PRDAT,'0001-01-01')
AND dt.companycode = pl.companycode
AND pl.plantcode = ifnull(e.WERKS,'Not Set')
AND pl.plantcode = dt.plantcode_factory
AND pir.dim_dateidpricevalid <> dt.dim_dateid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_overdeliv_tolerance = ifnull(e.EINE_UEBTO,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.ct_overdeliv_tolerance <> ifnull(e.EINE_UEBTO,0);

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.ct_underdeliv_tolerance = ifnull(e.EINE_UNTTO,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.ct_underdeliv_tolerance <> ifnull(e.EINE_UNTTO,0);

/*Georgiana 12 May 2017 Adding changes according to BI-6233*/
UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_currencyid = dc.dim_currencyid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_currency dc,dim_company dcm,dim_plant pl
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
and pir.dim_plantid=pl.dim_plantid
and pl.companycode=dcm.companycode
AND dc.currencycode = dcm.currency
AND pir.dim_currencyid <> dc.dim_currencyid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_currencyid_TRA = dc.dim_currencyid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_currency dc
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND dc.currencycode = ifnull(e.EINE_WAERS,'Not Set')
AND pir.dim_currencyid_TRA <> dc.dim_currencyid;
/*12 May End*/

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_noeval_receipt_settlemetn = ifnull(e.EINE_XERSN,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e
, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.dd_noeval_receipt_settlemetn <> ifnull(e.EINE_XERSN,'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord fpr
SET fpr.dim_vendorpurchasingid = vp.dim_vendorpurchasingid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_vendorpurchasing vp, EINE e, EINA a
, tmpdaily_fact_purchaseinforecord fpr
WHERE
fpr.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND fpr.dd_inforecord = ifnull(a.INFNR,'Not Set')
AND fpr.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND fpr.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND fpr.dd_plant = ifnull(e.WERKS,'Not Set')
AND vp.vendornumber = ifnull(a.LIFNR,'Not Set')
AND vp.purchasingorg = ifnull(e.EINE_EKORG,'Not Set')
AND fpr.dim_vendorpurchasingid <> vp.dim_vendorpurchasingid;

drop table if exists a017_datefiltered;
create table a017_datefiltered
as select *
from a017
where a017_datbi >= current_date AND a017_datab <=current_date;

update tmpdaily_fact_purchaseinforecord fpr
set fpr.dim_dateidvalidstartdateid_a017 = dd.dim_dateid
,dw_update_date = CURRENT_TIMESTAMP
from a017_datefiltered a, dim_part dp, dim_vendor v,dim_date dd,dim_plant pl
, tmpdaily_fact_purchaseinforecord fpr
where
fpr.dim_vendorid = v.dim_vendorid AND
v.vendornumber = a.a017_lifnr AND
fpr.dim_partid = dp.dim_partid AND
dp.partnumber = a.a017_matnr AND
fpr.dd_purchaseorg = a.a017_ekorg AND
fpr.dd_plant = a.a017_werks AND
fpr.dd_inforecordcategory = a.a017_esokz AND
dd.datevalue = ifnull(a.a017_DATAB,'0001-01-01')
AND dd.companycode = pl.companycode AND
 pl.plantcode = ifnull(a.a017_werks,'Not Set')
 AND pl.plantcode = dd.plantcode_factory
 AND fpr.dim_dateidvalidstartdateid_a017 <> dd.dim_dateid;

 drop table if exists a017_datefiltered;

 /* Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

 UPDATE tmpdaily_fact_purchaseinforecord f
	SET f.std_exchangerate_dateid = dt.dim_dateid
FROM dim_plant p
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode
		 INNER JOIN tmpdaily_fact_purchaseinforecord f ON f.dim_plantid = p.dim_plantid AND p.plantcode = dt.plantcode_factory
	WHERE dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

 /* END Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */


 /*Georgiana Adding dim_vendormasterid*/

 update tmpdaily_fact_purchaseinforecord pir
set pir.dim_vendormasterid = mv.dim_vendormasterid,
dw_update_date = CURRENT_TIMESTAMP
from dim_vendormaster mv, dim_vendor v , dim_plant dp,dim_company dc
, tmpdaily_fact_purchaseinforecord pir
where pir.dim_vendorid=v.dim_vendorid
AND mv.vendornumber = v.vendornumber
AND pir.dim_plantid=dp.dim_plantid
AND dp.companycode = dc.companycode
AND mv.companycode = dc.companycode
AND pir.dim_vendormasterid <> mv.dim_vendormasterid;
/*END changes on dim_vendormasterid*/

/* Octavian: Add KONV-KBETR PB00 */
update tmpdaily_fact_purchaseinforecord pir
SET pir.amt_grossprice = fpr.amt_grossprice,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, tmpdaily_fact_purchaseinforecord pir,
(SELECT fpr.dd_DocumentNo, fpr.dd_DocumentItemNo,fpr.dd_inforecordno, MAX(fpr.amt_grossprice) AS amt_grossprice
 FROM fact_purchase fpr
GROUP BY fpr.dd_DocumentNo, fpr.dd_DocumentItemNo,fpr.dd_inforecordno) fpr
WHERE ifnull(e.EBELN,'Not Set') = fpr.dd_DocumentNo
and ifnull(e.EBELP,0) = fpr.dd_DocumentItemNo
AND fpr.dd_inforecordno = pir.dd_inforecord
AND pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND pir.amt_grossprice <> fpr.amt_grossprice;

/*START BI-5163 Alin*/
UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_dateidind_shelflifeexpdt = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM EINE e, dim_date dt, dim_plant pl, tmpdaily_fact_purchaseinforecord pir
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
AND dt.datevalue = ifnull(e.EINE_DATLB,'0001-01-01')
AND dt.companycode = pl.companycode
AND pl.plantcode = ifnull(e.WERKS,'Not Set')
AND dd_ind_shelflifeexpdata = ifnull(e.EINE_IPRKZ,'Not Set')
AND pl.plantcode = dt.plantcode_factory
AND pir.dim_dateidind_shelflifeexpdt <> dt.dim_dateid;
/*END BI-5163 Alin*/

/*24 feb 2017 - Alin Gh - bi5457*/
UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dim_dateidrealeseuntil = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmpdaily_fact_purchaseinforecord pir, dim_date dt, dim_part dp, dim_plant pl, dim_vendor v, QINF q
WHERE pir.dim_partid = dp.dim_partid
and pir.dim_plantid = pl.dim_plantid
and pir.dim_vendorid = v.dim_vendorid
and dt.datevalue = ifnull(q.QINF_FREI_DAT, '0001-01-01')
and pl.plantcode = dp.plant
AND dt.companycode = pl.companycode
AND pl.plantcode = dt.plantcode_factory	
AND pl.plantcode = ifnull(q.QINF_WERK,'Not Set')
AND v.vendornumber = IFNULL(Q.QINF_LIEFERANT, 'Not Set')
and dp.partnumber = ifnull(q.QINF_MATNR,0)
and pir.dim_dateidrealeseuntil <> dt.dim_dateid;

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_realeasequantityactive = ifnull(q.QINF_FREI_MGKZ, 'Not Set')
FROM tmpdaily_fact_purchaseinforecord pir, dim_part dp, dim_plant pl, dim_vendor v, QINF q
WHERE pir.dim_partid = dp.dim_partid
and pir.dim_plantid = pl.dim_plantid
and pir.dim_vendorid = v.dim_vendorid
and pl.plantcode = dp.plant	
AND pl.plantcode = ifnull(q.QINF_WERK,'Not Set')
AND v.vendornumber = IFNULL(Q.QINF_LIEFERANT, 'Not Set')
and dp.partnumber = ifnull(q.QINF_MATNR,0)
and pir.dd_realeasequantityactive <> ifnull(q.QINF_FREI_MGKZ, 'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_blockfnction = ifnull(q.QINF_SPERRFKT, 'Not Set')
FROM tmpdaily_fact_purchaseinforecord pir, dim_part dp, dim_plant pl, dim_vendor v, QINF q
WHERE pir.dim_partid = dp.dim_partid
and pir.dim_plantid = pl.dim_plantid
and pir.dim_vendorid = v.dim_vendorid
and pl.plantcode = dp.plant	
AND pl.plantcode = ifnull(q.QINF_WERK,'Not Set')
AND v.vendornumber = IFNULL(Q.QINF_LIEFERANT, 'Not Set')
and dp.partnumber = ifnull(q.QINF_MATNR,0)
and pir.dd_blockfnction <> ifnull(q.QINF_SPERRFKT, 'Not Set');

UPDATE tmpdaily_fact_purchaseinforecord pir
SET pir.dd_blockreason = ifnull(q.QINF_SPERRGRUND, 'Not Set')
FROM tmpdaily_fact_purchaseinforecord pir, dim_part dp, dim_plant pl, dim_vendor v, QINF q
WHERE pir.dim_partid = dp.dim_partid
and pir.dim_plantid = pl.dim_plantid
and pir.dim_vendorid = v.dim_vendorid
and pl.plantcode = dp.plant	
AND pl.plantcode = ifnull(q.QINF_WERK,'Not Set')
AND v.vendornumber = IFNULL(Q.QINF_LIEFERANT, 'Not Set')
and dp.partnumber = ifnull(q.QINF_MATNR,0)
and pir.dd_blockreason <> ifnull(q.QINF_SPERRGRUND, 'Not Set');

/*24 feb 2017 - Alin Gh - bi5457*/


/*Georgiana 12 May 2017 Adding changes according to BI-6233*/
 merge into tmpdaily_fact_purchaseinforecord pir
using (select distinct fact_purchaseinforecordid,max(z.exchangeRate) as exchangeRate
from EINE e, dim_Company dcm,dim_plant dpl, tmp_getexchangerate1 z,tmp_GlobalCurr_001,tmpdaily_fact_purchaseinforecord pir
 WHERE    pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
 AND pir.dim_plantid=dpl.dim_plantid
AND dpl.companycode = dcm.companycode
AND z.pFromCurrency  = EINE_WAERS
AND z.pToCurrency = pGlobalCurrency AND z.pDate = EINE_ERDAT
group by fact_purchaseinforecordid) t
on t.fact_purchaseinforecordid=pir.fact_purchaseinforecordid
when matched then update set amt_ExchangeRate_gbl =  t.exchangeRate;

/*Alin 18 Oct 2017 APP-7791*/
/*updated due to unable to get a stable set of rows error*/
merge into tmpdaily_fact_purchaseinforecord pir
using ( select distinct fact_purchaseinforecordid, min(c.dim_companyid) as dim_companyid
FROM EINE e, tmpdaily_fact_purchaseinforecord pir, T001 w, T001K s, dim_company c
WHERE pir.dd_inforecord = ifnull(e.INFNR,'Not Set')
AND pir.dd_purchaseorg = ifnull(e.EINE_EKORG,'Not Set')
AND pir.dd_inforecordcategory = ifnull(e.EINE_ESOKZ,'Not Set')
AND pir.dd_plant = ifnull(e.WERKS,'Not Set')
and ifnull(s.BWKEY,'Not Set')= ifnull(e.WERKS, 'Not Set')
and c.companycode = IFNULL(w.BUKRS, 'Not Set')
and IFNULL(w.BUKRS, 'Not Set') = IFNULL(s.BUKRS, 'Not Set')
and pir.dim_companyid <> c.dim_companyid
group by fact_purchaseinforecordid) t
on t.fact_purchaseinforecordid = pir.fact_purchaseinforecordid
when matched then update set pir.dim_companyid = t.dim_companyid,
dw_update_date = CURRENT_TIMESTAMP;

truncate table fact_purchaseinforecord;
insert into fact_purchaseinforecord select * from tmpdaily_fact_purchaseinforecord;