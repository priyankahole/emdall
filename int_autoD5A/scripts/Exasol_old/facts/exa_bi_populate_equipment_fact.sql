DROP TABLE IF EXISTS tmp_fact_equipment;

CREATE TABLE tmp_fact_equipment
LIKE fact_equipment INCLUDING DEFAULTS INCLUDING IDENTITY;

delete from number_fountain m where m.table_name = 'tmp_fact_equipment';
insert into number_fountain
select 'tmp_fact_equipment',
ifnull(max(f.fact_equipmentid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_equipment f;

insert into tmp_fact_equipment (
fact_equipmentid,
DD_EQUIPMENT_NUMBER,
DIM_VALIDTODATEID,
DIM_PLANTID,
DIM_FUNCTIONALLOCATIONID,
DD_LOC_AND_ACC_ASSIGNMENT_KEY,
DIM_WORKCENTERID,
dim_equipmentid,

dd_objectid,
dd_equipvalidtodate,
dd_usg_consec_days,


DW_UPDATE_DATE,
DW_INSERT_DATE,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_CURRENCYID_GBL,
DIM_CURRENCYID,
DIM_CURRENCYID_TRA,
DIM_PROJECTSOURCEID
)
SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_equipment') + row_number() over(order by '') AS fact_equipmentid,
ifnull(EQUI_EQUNR,'Not Set') as DD_EQUIPMENT_NUMBER,
1 as DIM_VALIDTODATEID,
1 as DIM_PLANTID,
1 as DIM_FUNCTIONALLOCATIONID,
'Not Set' as DD_LOC_AND_ACC_ASSIGNMENT_KEY,
1 as DIM_WORKCENTERID,
1 as dim_equipmentid,

ifnull((cast(EQUZ_GEWRK as varchar(8))), 'Not Set') as dd_objectid,
ifnull(EQUZ_DATBI, '0001-01-01') as dd_equipvalidtodate,
ifnull(EQUZ_EQLFN, '0001-01-01') as dd_usg_consec_days,


current_date as DW_UPDATE_DATE,
current_date as DW_INSERT_DATE,
1 as AMT_EXCHANGERATE_GBL,
1 as AMT_EXCHANGERATE,
1 as DIM_CURRENCYID_GBL,
1 as DIM_CURRENCYID,
1 as DIM_CURRENCYID_TRA,
1 as DIM_PROJECTSOURCEID
FROM EQUI i, EQUZ z, EQKT k
where EQUI_EQUNR = EQUZ_EQUNR 
and EQUZ_EQUNR =	EQKT_EQUNR
and not exists (select 1 from tmp_fact_equipment d1
where d1.DD_EQUIPMENT_NUMBER = ifnull(EQUI_EQUNR,'Not Set'));


update  tmp_fact_equipment f
set f.dim_equipmentid = dd.dim_equipmentid
from  tmp_fact_equipment f, EQUI i, EQUZ z, EQKT k, dim_equipment dd
where EQUI_EQUNR = EQUZ_EQUNR 
and EQUZ_EQUNR = EQKT_EQUNR 
and EQUIPMENTNO = EQKT_EQUNR
and EQUI_EQTYP <> 'S'
and f.DD_EQUIPMENT_NUMBER = EQUIPMENTNO
and equipusageperiods = equz_eqlfn
and dd_usg_consec_days = equipusageperiods
and equipmentvalidtodate = ifnull(z.EQUZ_DATBI, '0001-01-01')
and dd_equipvalidtodate = equipmentvalidtodate
and f.dim_equipmentid <> dd.dim_equipmentid;


update tmp_fact_equipment a
set a.DIM_VALIDTODATEID = dd.dim_dateid
from EQUZ b,tmp_fact_equipment a, dim_date dd
where a.DD_EQUIPMENT_NUMBER = ifnull(b.equz_equnr ,'Not Set')
and ifnull(b.EQUZ_DATBI,'0001-01-01') = dd_equipvalidtodate
and ifnull(b.equz_eqlfn,'Not Set') = dd_usg_consec_days
and dd.datevalue = ifnull(b.EQUZ_DATBI,'0001-01-01')
and dd.companycode = 'Not Set'
and dd.plantcode_factory = 'Not Set'
and a.DIM_VALIDTODATEID <> dd.dim_dateid;


update  tmp_fact_equipment f
set f.DIM_PLANTID = dd.DIM_PLANTID
from  tmp_fact_equipment f, EQUZ z, dim_plant dd
where f.DD_EQUIPMENT_NUMBER = ifnull(z.equz_equnr ,'Not Set')
and ifnull(z.EQUZ_DATBI,'0001-01-01') = dd_equipvalidtodate
and ifnull(z.equz_eqlfn,'Not Set') = dd_usg_consec_days
and dd.plantcode=ifnull(EQUZ_IWERK,'Not Set')
and f.DIM_PLANTID <> dd.DIM_PLANTID;

update  tmp_fact_equipment f
set f.DIM_FUNCTIONALLOCATIONID = dd.DIM_FUNCTIONALLOCATIONID
from  tmp_fact_equipment f, EQUZ z, ILOA, dim_functionallocation dd, IFLOT
where f.DD_EQUIPMENT_NUMBER = ifnull(z.equz_equnr ,'Not Set')
and ifnull(z.EQUZ_DATBI,'0001-01-01') = dd_equipvalidtodate
and ifnull(z.equz_eqlfn,'Not Set') = dd_usg_consec_days
AND iflot_tplnr = ILOA_TPLNR
AND Dd.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and dd.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
AND z.EQUZ_ILOAN = ILOA_ILOAN
and f.DIM_FUNCTIONALLOCATIONID <> dd.DIM_FUNCTIONALLOCATIONID;

update  tmp_fact_equipment f
set f.DD_LOC_AND_ACC_ASSIGNMENT_KEY = ifnull(ILOA_ILOAN, 'Not Set')
from  tmp_fact_equipment f, EQUZ z, ILOA i
where f.DD_EQUIPMENT_NUMBER = ifnull(z.equz_equnr ,'Not Set')
and ifnull(z.EQUZ_DATBI,'0001-01-01') = dd_equipvalidtodate
and ifnull(z.equz_eqlfn,'Not Set') = dd_usg_consec_days
AND z.EQUZ_ILOAN = ILOA_ILOAN
and f.DD_LOC_AND_ACC_ASSIGNMENT_KEY <> ifnull(ILOA_ILOAN, 'Not Set');


drop table if exists tmp_workcenter;
create table tmp_workcenter as
select distinct first_value(t.dim_workcenterid) over (partition by equnr, datbi, eqlfn order by equnr, datbi, eqlfn) as dim_workcenterid, equnr, datbi, eqlfn
from
(select distinct d.dim_workcenterid, ifnull(EQUZ_DATBI,'0001-01-01') as datbi, ifnull(equz_eqlfn,'Not Set') as eqlfn, ifnull(equz_equnr,'Not Set') as equnr
from dim_workcenter d, EQUZ, CRHD
where EQUZ_GEWRK = CRHD_OBJID
AND ifnull(crhd_objty,'Not Set') = objecttype
AND IFNULL((cast(CRHD_OBJID as varchar(8))), 'Not Set') = d.objectid) t;

update  tmp_fact_equipment f
set f.DIM_WORKCENTERID = d.DIM_WORKCENTERID
from tmp_fact_equipment f, tmp_workcenter d
WHERE DD_EQUIPMENT_NUMBER = d.equnr
and dd_equipvalidtodate = datbi
and dd_usg_consec_days = eqlfn
and f.DIM_WORKCENTERID <> d.DIM_WORKCENTERID;

drop table if exists tmp_workcenter;

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'tmp_fact_equipment';   

DROP TABLE if EXISTS fact_equipment;
RENAME tmp_fact_equipment to fact_equipment;
