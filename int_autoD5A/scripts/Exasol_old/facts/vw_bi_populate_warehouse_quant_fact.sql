/* #######################################CHANGE HISTORY#############################################################
/*	 Date             By                 Version              Desc                                                     */
/*   23 Mar 2016	  Andrei Postolache  1.1                  Add Joins for Dim_Date in some Update statements - BI-2385  */
/*   20 Jul 2016     Liviu Ionescu   1.2                        Post EAR - Warehouse Quants: Add 5 fields BI-3437 */
/***********************************************************************************************************************/

DROP TABLE IF EXISTS tmp_fact_wmquant ;

CREATE TABLE tmp_fact_wmquant LIKE fact_wmquant INCLUDING DEFAULTS INCLUDING IDENTITY;

/*initialize NUMBER_FOUNTAIN*/

DELETE from NUMBER_FOUNTAIN f WHERE f.table_name = 'tmp_fact_wmquant';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'tmp_fact_wmquant',IFNULL(max(f.fact_wmquantid) , IFNULL((SELECT s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_wmquant f;



INSERT INTO tmp_fact_wmquant (fact_wmquantid,
								dim_warehousenumberid,
								dd_quantno,
								dim_partid,
								dim_plantid,
								dim_storagetypeid,
								dim_storagebinid,
								dim_storagelocationid,
								dim_unitofmeasureid,
								ct_availablestock,
								dd_batchnumber,
								dim_wmstockcategoryid,
								dim_specialstockid,
								dd_specialstockno,
								dim_wmblockingreasonid,
								dim_dateidlastmovement,
								dd_lastchangetodocno,
								dd_lastchangetodocitemno,
								dim_dateidlaststockplacement,
								dim_dateidlaststockremoval,
								dim_dateidlaststockaddition,
								dim_dateidgoodsreceipt,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dim_wmstorageunittypeid,
								ct_totalqty,
								ct_putawayqty,
								ct_removeqty,
								ct_materialweight,
								dim_weightunitid,
								dd_transferreqno,
								dd_inventorydocitemno,
								dim_wmrequirementtypeid,
								dd_requirementno,
								dd_storageunitno,
								dd_inspectionlotno,
								dim_dateidbestbefore,
								ct_capacityusage,
								dim_wmpickingareaid,
								ct_opentransferqty,
								dd_deliveryno,
								dd_deliveryitemno,
								dim_dateidlastinventory,
								dim_wmquantmiscellaneousid,
								ct_totalcapacity,
								ct_remainingcapacity,
								ct_numberofquantsinuse,
								ct_maxnumberofquants,
								ct_loadingwquipmentquantity1,
								ct_loadingwquipmentquantity2,
								ct_loadingwquipmentquantity3)
   SELECT 	((SELECT IFNULL(max_id, 1) from NUMBER_FOUNTAIN  WHERE table_name = 'tmp_fact_wmquant')
            + row_number() over (order by '') )  fact_wmquantid,
			wn.dim_warehousenumberid as dim_warehousenumberid,
			IFNULL(st.LQUA_LQNUM, 'Not Set') as dd_quantno,
			/* IFNULL((SELECT dim_partid
                       FROM dim_part dp
                      WHERE dp.PartNumber = st.LQUA_MATNR AND dp.plant = st.LQUA_WERKS),
                    1) */ CONVERT(BIGINT,1) as dim_partid,
			pl.Dim_Plantid,
			/* IFNULL((SELECT sty.dim_wmstoragetypeid
                      FROM dim_wmstoragetype sty
                     WHERE sty.storagetype = st.LQUA_LGTYP
					 AND sty.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   1) */ CONVERT(BIGINT,1) as dim_storagetypeid,
			/* IFNULL((SELECT sb.dim_storagebinid
                      FROM dim_storagebin sb
                     WHERE sb.storagebin = st.LQUA_LGPLA
                           AND sb.storagetype = st.LQUA_LGTYP
						   AND sb.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   1) */ CONVERT(BIGINT,1) as dim_storagebinid,
			/* IFNULL((SELECT sl.dim_storagelocationid
                      FROM dim_storagelocation sl
                     WHERE sl.plant = st.LQUA_WERKS
                           AND sl.locationcode = st.LQUA_LGORT),
                   1) */ CONVERT(BIGINT,1) as dim_storagelocationid,
			/* IFNULL((SELECT um.dim_unitofmeasureid
                      FROM dim_unitofmeasure um
                     WHERE um.uom = st.LQUA_MEINS),
                   1) */ CONVERT(BIGINT,1) as dim_unitofmeasureid,
			IFNULL(st.LQUA_VERME, 0) as ct_availablestock,
			IFNULL(st.LQUA_CHARG,'Not Set') as dd_batchnumber,
			/* IFNULL((SELECT wmsc.dim_wmstockcategoryid
					  FROM dim_wmstockcategory wmsc
					 WHERE wmsc.WMStockCategory = st.LQUA_BESTQ),
					  1) */ CONVERT(BIGINT,1) as dim_wmstockcategoryid,
			/* IFNULL((SELECT ss.dim_specialstockid
					  FROM dim_specialstock ss
					 WHERE ss.specialstockindicator = st.LQUA_SOBKZ),
					  1) */ CONVERT(BIGINT,1) as dim_specialstockid,
			IFNULL(st.LQUA_SONUM,'Not Set') as dd_specialstockno,
			/* IFNULL((SELECT wmbr.dim_wmblockingreasonid
					  FROM dim_wmblockingreason wmbr
					 WHERE wmbr.WMBlockingReason = st.LQUA_SPGRU
			               AND wmbr.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
					  1) */ CONVERT(BIGINT,1) as dim_wmblockingreasonid,
			/* IFNULL((SELECT lmd.dim_dateid
								  FROM dim_date lmd
								 WHERE lmd.DateValue = st.LQUA_BDATU
									   AND lmd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlastmovement,
			IFNULL(st.LQUA_BTANR,'Not Set') as dd_lastchangetodocno,
			IFNULL(st.LQUA_BTAPS,0) as dd_lastchangetodocitemno,
			/* IFNULL((SELECT lspd.dim_dateid
								  FROM dim_date lspd
								 WHERE lspd.DateValue = st.LQUA_EDATU
									   AND lspd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlaststockplacement,
			/* IFNULL((SELECT lsrd.dim_dateid
								  FROM dim_date lsrd
								 WHERE lsrd.DateValue = st.LQUA_ADATU
									   AND lsrd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlaststockremoval,
			/* IFNULL((SELECT lsad.dim_dateid
								  FROM dim_date lsad
								 WHERE lsad.DateValue = st.LQUA_ZDATU
									   AND lsad.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlaststockaddition,
			/* IFNULL((SELECT grd.dim_dateid
								  FROM dim_date grd
								 WHERE grd.DateValue = st.LQUA_WDATU
									   AND grd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidgoodsreceipt,
			IFNULL(st.LQUA_WENUM,'Not Set') as dd_goodsreceiptno,
			IFNULL(st.LQUA_WEPOS,0) as dd_goodsreceiptitemno,
			/* IFNULL((SELECT  wmsut.dim_wmstorageunittypeid
					  FROM dim_wmstorageunittype wmsut
					 WHERE  wmsut.StorageUnitType = st.LQUA_LETYP
			AND  wmsut.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
					  1) */ CONVERT(BIGINT,1) as dim_wmstorageunittypeid,
			IFNULL(st.LQUA_GESME,0) as ct_totalqty,
			IFNULL(st.LQUA_EINME,0) as ct_putawayqty,
			IFNULL(st.LQUA_AUSME,0) as ct_removeqty,
			IFNULL(st.LQUA_MGEWI,0) as ct_materialweight,
			/* IFNULL((SELECT wum.dim_unitofmeasureid
					  FROM dim_unitofmeasure wum
					 WHERE wum.uom = st.LQUA_GEWEI),
					  1) */ CONVERT(BIGINT,1) as dim_weightunitid,
			IFNULL(st.LQUA_TBNUM,'Not Set') as dd_transferreqno,
			IFNULL(st.LQUA_IVPOS,0) as dd_inventorydocitemno,
			/* IFNULL((SELECT wmrt.dim_wmrequirementtypeid
					  FROM dim_wmrequirementtype wmrt
					 WHERE wmrt.RequirementType = st.LQUA_BETYP
			AND wmrt.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
					  1) */ CONVERT(BIGINT,1) as dim_wmrequirementtypeid,
			IFNULL(st.LQUA_BENUM,'Not Set') as dd_requirementno,
			IFNULL(st.LQUA_LENUM,'Not Set') as dd_storageunitno,
			IFNULL(st.LQUA_QPLOS,'Not Set') as dd_inspectionlotno,
			/* IFNULL((SELECT bbd.dim_dateid
								  FROM dim_date bbd
								 WHERE bbd.DateValue = st.LQUA_VFDAT
									   AND bbd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidbestbefore,
			IFNULL(st.LQUA_QKAPV,0) as ct_capacityusage,
			/* IFNULL((SELECT wmpa.dim_wmpickingareaid
					  FROM dim_wmpickingarea wmpa
					  WHERE wmpa.StorageType = st.LQUA_LGTYP
			               AND wmpa.PickingArea = st.LQUA_KOBER
			               AND wmpa.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
					  1) */ CONVERT(BIGINT,1) as dim_wmpickingareaid,
			IFNULL(st.LQUA_TRAME, 0) as ct_opentransferqty,
			IFNULL(st.LQUA_VBELN,'Not Set') as dd_deliveryno,
			IFNULL(st.LQUA_POSNR,0) as dd_deliveryitemno,
			/* IFNULL((SELECT lid.dim_dateid
								  FROM dim_date lid
								 WHERE lid.DateValue = st.LQUA_IDATU
									   AND lid.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlastinventory,
			/* IFNULL((SELECT wmqmisc.dim_wmquantmiscellaneousid
					    from dim_wmquantmiscellaneous wmqmisc
					    WHERE     wmqmisc.userputawayblocked  = IFNULL(st.LQUA_SKZUE,'Not Set')
							  AND wmqmisc.userremovalblocked  = IFNULL(st.LQUA_SKZUA,'Not Set')
							  AND wmqmisc.systcurrentplacementblocked = IFNULL(st.LQUA_SKZSE,'Not Set')
							  AND wmqmisc.systremovalblocked  = IFNULL(st.LQUA_SKZSA,'Not Set')
							  AND wmqmisc.systInventoryblocked  = IFNULL(st.LQUA_SKZSI,'Not Set')
							  AND wmqmisc.nogrdata = IFNULL(st.LQUA_VIRGO,'Not Set')
							  AND wmqmisc.OnHandlingUnit = IFNULL(st.LQUA_KZHUQ,'Not Set')),1) */ CONVERT(BIGINT,1) as dim_wmquantmiscellaneousid,
			/* IFNULL((SELECT l.LAGP_LKAPV
                      FROM LAGP l
                     WHERE l.LAGP_LGPLA = st.LQUA_LGPLA
                           AND l.LAGP_LGTYP = st.LQUA_LGTYP
						   AND l.LAGP_LGNUM = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   0) */ 0 AS ct_totalcapacity,
			/* IFNULL((SELECT l.LAGP_RKAPV
                      FROM LAGP l
                     WHERE l.LAGP_LGPLA = st.LQUA_LGPLA
                           AND l.LAGP_LGTYP = st.LQUA_LGTYP
						   AND l.LAGP_LGNUM = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   0) */ 0 AS ct_remainingcapacity,
			/* IFNULL((SELECT l.LAGP_ANZQU
                      FROM LAGP l
                     WHERE l.LAGP_LGPLA = st.LQUA_LGPLA
                           AND l.LAGP_LGTYP = st.LQUA_LGTYP
						   AND l.LAGP_LGNUM = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   0) */ 0 AS ct_numberofquantsinuse,
			/* IFNULL((SELECT l.LAGP_MAXQU
                      FROM LAGP l
                     WHERE l.LAGP_LGPLA = st.LQUA_LGPLA
                           AND l.LAGP_LGTYP = st.LQUA_LGTYP
						   AND l.LAGP_LGNUM = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   0) */ 0 AS ct_maxnumberofquants,
			/* IFNULL((SELECT m.MLGN_LHMG1
					FROM MLGN m
					WHERE ST.LQUA_MATNR = M.MLGN_MATNR
					AND ST.LQUA_LGNUM = M.MLGN_LGNUM),
				   0) */ 0 AS ct_loadingwquipmentquantity1,
			/* IFNULL((SELECT m.MLGN_LHMG2
					FROM MLGN m
					WHERE ST.LQUA_MATNR = M.MLGN_MATNR
					AND ST.LQUA_LGNUM = M.MLGN_LGNUM),
				   0) */ 0 AS ct_loadingwquipmentquantity2,
			/* IFNULL((SELECT m.MLGN_LHMG3
					FROM MLGN m
					WHERE ST.LQUA_MATNR = M.MLGN_MATNR
					AND ST.LQUA_LGNUM = M.MLGN_LGNUM),
				   0) */ 0 AS ct_loadingwquipmentquantity3

     FROM LQUA st,
          dim_plant pl,
		  dim_warehousenumber wn
    WHERE st.LQUA_WERKS = pl.PlantCode
		 AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
          AND NOT EXISTS
                (SELECT 1
                   FROM tmp_fact_wmquant fq
                  WHERE     wn.dim_warehousenumberid = fq.dim_warehousenumberid
                        AND  st.LQUA_LQNUM = fq.dd_quantno)	;

INSERT INTO tmp_fact_wmquant (fact_wmquantid,
								dim_warehousenumberid,
								dd_quantno,
								dim_partid,
								dim_plantid,
								dim_storagetypeid,
								dim_storagebinid,
								dim_storagelocationid,
								dim_unitofmeasureid,
								ct_availablestock,
								dd_batchnumber,
								dim_wmstockcategoryid,
								dim_specialstockid,
								dd_specialstockno,
								dim_wmblockingreasonid,
								dim_dateidlastmovement,
								dd_lastchangetodocno,
								dd_lastchangetodocitemno,
								dim_dateidlaststockplacement,
								dim_dateidlaststockremoval,
								dim_dateidlaststockaddition,
								dim_dateidgoodsreceipt,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dim_wmstorageunittypeid,
								ct_totalqty,
								ct_putawayqty,
								ct_removeqty,
								ct_materialweight,
								dim_weightunitid,
								dd_transferreqno,
								dd_inventorydocitemno,
								dim_wmrequirementtypeid,
								dd_requirementno,
								dd_storageunitno,
								dd_inspectionlotno,
								dim_dateidbestbefore,
								ct_capacityusage,
								dim_wmpickingareaid,
								ct_opentransferqty,
								dd_deliveryno,
								dd_deliveryitemno,
								dim_dateidlastinventory,
								dim_wmquantmiscellaneousid,
								ct_totalcapacity,
								ct_remainingcapacity,
								ct_numberofquantsinuse,
								ct_maxnumberofquants,
								ct_loadingwquipmentquantity1,
								ct_loadingwquipmentquantity2,
								ct_loadingwquipmentquantity3)
   SELECT 	(IFNULL((SELECT MAX(fact_wmquantid)FROM tmp_fact_wmquant), (SELECT IFNULL(max_id, 1) from NUMBER_FOUNTAIN  WHERE table_name = 'fact_wmquant'))
            + row_number() over (order by '') )  fact_wmquantid,
			IFNULL(wn.dim_warehousenumberid,1) as dim_warehousenumberid,
			IFNULL(st.LQUA_LQNUM, 'Not Set') as dd_quantno,
			/* IFNULL((SELECT dim_partid
                       FROM dim_part dp
                      WHERE dp.PartNumber = st.LQUA_MATNR AND dp.plant = st.LQUA_WERKS),
                    1) */ 1as dim_partid,
			IFNULL(pl.Dim_Plantid,1) as dim_plantid,
			/* IFNULL((SELECT sty.dim_wmstoragetypeid
                      FROM dim_wmstoragetype sty
                     WHERE sty.storagetype = st.LQUA_LGTYP
					 AND sty.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   1) */ CONVERT(BIGINT,1) AS dim_storagetypeid,
			IFNULL(sb.dim_storagebinid, 1) as dim_storagebinid,
			/* IFNULL((SELECT sl.dim_storagelocationid
                      FROM dim_storagelocation sl
                     WHERE sl.plant = st.LQUA_WERKS
                           AND sl.locationcode = st.LQUA_LGORT),
                   1) */ CONVERT(BIGINT,1) as dim_storagelocationid,
			/* IFNULL((SELECT um.dim_unitofmeasureid
                      FROM dim_unitofmeasure um
                     WHERE um.uom = st.LQUA_MEINS),
                   1) */ CONVERT(BIGINT,1) as dim_unitofmeasureid,
			IFNULL(st.LQUA_VERME, 0) as ct_availablestock,
			IFNULL(st.LQUA_CHARG,'Not Set') as dd_batchnumber,
			/* IFNULL((SELECT wmsc.dim_wmstockcategoryid
					  FROM dim_wmstockcategory wmsc
					 WHERE wmsc.WMStockCategory = st.LQUA_BESTQ),
					  1) */ CONVERT(BIGINT,1) as dim_wmstockcategoryid,
			/* IFNULL((SELECT ss.dim_specialstockid
					  FROM dim_specialstock ss
					 WHERE ss.specialstockindicator = st.LQUA_SOBKZ),
					  1) */ CONVERT(BIGINT,1) as dim_specialstockid,
			IFNULL(st.LQUA_SONUM,'Not Set') as dd_specialstockno,
			/* IFNULL((SELECT wmbr.dim_wmblockingreasonid
					  FROM dim_wmblockingreason wmbr
					 WHERE wmbr.WMBlockingReason = st.LQUA_SPGRU
			               AND wmbr.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
					  1) */ CONVERT(BIGINT,1) as dim_wmblockingreasonid,
			/* IFNULL((SELECT lmd.dim_dateid
								  FROM dim_date lmd
								 WHERE lmd.DateValue = st.LQUA_BDATU
									   AND lmd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlastmovement,
			IFNULL(st.LQUA_BTANR,'Not Set') as dd_lastchangetodocno,
			IFNULL(st.LQUA_BTAPS,0) as dd_lastchangetodocitemno,
			/* IFNULL((SELECT lspd.dim_dateid
								  FROM dim_date lspd
								 WHERE lspd.DateValue = st.LQUA_EDATU
									   AND lspd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlaststockplacement,
			/* IFNULL((SELECT lsrd.dim_dateid
								  FROM dim_date lsrd
								 WHERE lsrd.DateValue = st.LQUA_ADATU
									   AND lsrd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlaststockremoval,
			/* IFNULL((SELECT lsad.dim_dateid
								  FROM dim_date lsad
								 WHERE lsad.DateValue = st.LQUA_ZDATU
									   AND lsad.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlaststockaddition,
			/* IFNULL((SELECT grd.dim_dateid
								  FROM dim_date grd
								 WHERE grd.DateValue = st.LQUA_WDATU
									   AND grd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidgoodsreceipt,
			IFNULL(st.LQUA_WENUM,'Not Set') as dd_goodsreceiptno,
			IFNULL(st.LQUA_WEPOS,0) as dd_goodsreceiptitemno,
			/* IFNULL((SELECT  wmsut.dim_wmstorageunittypeid
					  FROM dim_wmstorageunittype wmsut
					 WHERE  wmsut.StorageUnitType = st.LQUA_LETYP
			AND  wmsut.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
					  1) */ CONVERT(BIGINT,1) as dim_wmstorageunittypeid,
			IFNULL(st.LQUA_GESME,0) as ct_totalqty,
			IFNULL(st.LQUA_EINME,0) as ct_putawayqty,
			IFNULL(st.LQUA_AUSME,0) as ct_removeqty,
			IFNULL(st.LQUA_MGEWI,0) as ct_materialweight,
			/* IFNULL((SELECT wum.dim_unitofmeasureid
					  FROM dim_unitofmeasure wum
					 WHERE wum.uom = st.LQUA_GEWEI),
					  1) */ CONVERT(BIGINT,1) as dim_weightunitid,
			IFNULL(st.LQUA_TBNUM,'Not Set') as dd_transferreqno,
			IFNULL(st.LQUA_IVPOS,0) as dd_inventorydocitemno,
			/* IFNULL((SELECT wmrt.dim_wmrequirementtypeid
					  FROM dim_wmrequirementtype wmrt
					 WHERE wmrt.RequirementType = st.LQUA_BETYP
			AND wmrt.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
					  1) */ CONVERT(BIGINT,1) as dim_wmrequirementtypeid,
			IFNULL(st.LQUA_BENUM,'Not Set') as dd_requirementno,
			IFNULL(st.LQUA_LENUM,'Not Set') as dd_storageunitno,
			IFNULL(st.LQUA_QPLOS,'Not Set') as dd_inspectionlotno,
			/* IFNULL((SELECT bbd.dim_dateid
								  FROM dim_date bbd
								 WHERE bbd.DateValue = st.LQUA_VFDAT
									   AND bbd.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidbestbefore,
			IFNULL(st.LQUA_QKAPV,0) as ct_capacityusage,
			/* IFNULL((SELECT wmpa.dim_wmpickingareaid
					  FROM dim_wmpickingarea wmpa
					  WHERE wmpa.StorageType = st.LQUA_LGTYP
			               AND wmpa.PickingArea = st.LQUA_KOBER
			               AND wmpa.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')),
					  1) */ CONVERT(BIGINT,1) as dim_wmpickingareaid,
			IFNULL(st.LQUA_TRAME, 0) as ct_opentransferqty,
			IFNULL(st.LQUA_VBELN,'Not Set') as dd_deliveryno,
			IFNULL(st.LQUA_POSNR,0) as dd_deliveryitemno,
			/* IFNULL((SELECT lid.dim_dateid
								  FROM dim_date lid
								 WHERE lid.DateValue = st.LQUA_IDATU
									   AND lid.CompanyCode = pl.CompanyCode), 1) */ CONVERT(BIGINT,1) as dim_dateidlastinventory,
			/* IFNULL((SELECT wmqmisc.dim_wmquantmiscellaneousid
					    from dim_wmquantmiscellaneous wmqmisc
					    WHERE     wmqmisc.userputawayblocked  = IFNULL(st.LQUA_SKZUE,'Not Set')
							  AND wmqmisc.userremovalblocked  = IFNULL(st.LQUA_SKZUA,'Not Set')
							  AND wmqmisc.systcurrentplacementblocked = IFNULL(st.LQUA_SKZSE,'Not Set')
							  AND wmqmisc.systremovalblocked  = IFNULL(st.LQUA_SKZSA,'Not Set')
							  AND wmqmisc.systInventoryblocked  = IFNULL(st.LQUA_SKZSI,'Not Set')
							  AND wmqmisc.nogrdata = IFNULL(st.LQUA_VIRGO,'Not Set')
							  AND wmqmisc.OnHandlingUnit = IFNULL(st.LQUA_KZHUQ,'Not Set')),1) */ CONVERT(BIGINT,1) as dim_wmquantmiscellaneousid,
			/* IFNULL((SELECT l.LAGP_LKAPV
                      FROM LAGP l
                     WHERE l.LAGP_LGPLA = st.LQUA_LGPLA
                           AND l.LAGP_LGTYP = st.LQUA_LGTYP
						   AND l.LAGP_LGNUM = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   0) */ 0 as ct_totalcapacity,
			/* IFNULL((SELECT l.LAGP_RKAPV
                      FROM LAGP l
                     WHERE l.LAGP_LGPLA = st.LQUA_LGPLA
                           AND l.LAGP_LGTYP = st.LQUA_LGTYP
						   AND l.LAGP_LGNUM = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   0) */ 0 as ct_remainingcapacity,
			/* IFNULL((SELECT l.LAGP_ANZQU
                      FROM LAGP l
                     WHERE l.LAGP_LGPLA = st.LQUA_LGPLA
                           AND l.LAGP_LGTYP = st.LQUA_LGTYP
						   AND l.LAGP_LGNUM = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   0) */ 0 as ct_numberofquantsinuse,
			/* IFNULL((SELECT l.LAGP_MAXQU
                      FROM LAGP l
                     WHERE l.LAGP_LGPLA = st.LQUA_LGPLA
                           AND l.LAGP_LGTYP = st.LQUA_LGTYP
						   AND l.LAGP_LGNUM = IFNULL(st.LQUA_LGNUM, 'Not Set')),
                   0) */ 0 as ct_maxnumberofquants,
			/* IFNULL((SELECT m.MLGN_LHMG1
					FROM MLGN m
					WHERE ST.LQUA_MATNR = M.MLGN_MATNR
					AND ST.LQUA_LGNUM = M.MLGN_LGNUM),
				   0) */ 0 AS ct_loadingwquipmentquantity1,
			/* IFNULL((SELECT m.MLGN_LHMG2
					FROM MLGN m
					WHERE ST.LQUA_MATNR = M.MLGN_MATNR
					AND ST.LQUA_LGNUM = M.MLGN_LGNUM),
				   0) */ 0 AS ct_loadingwquipmentquantity2,
			/* IFNULL((SELECT m.MLGN_LHMG3
					FROM MLGN m
					WHERE ST.LQUA_MATNR = M.MLGN_MATNR
					AND ST.LQUA_LGNUM = M.MLGN_LGNUM),
				   0) */ 0 AS ct_loadingwquipmentquantity3
     FROM LQUA st

    JOIN dim_plant pl
    ON st.LQUA_WERKS = pl.PlantCode

	JOIN dim_warehousenumber wn
	ON wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')

	RIGHT JOIN dim_storagebin SB
	ON sb.storagebin = st.LQUA_LGPLA
	AND sb.storagetype = st.LQUA_LGTYP
	AND sb.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')
    WHERE st.LQUA_LGPLA IS NULL
		 AND st.LQUA_LGTYP IS NULL
		 AND st.LQUA_LGNUM IS NULL
     /*    AND NOT EXISTS
                (SELECT 1
                   FROM fact_wmquant fq
                  WHERE     SB.dim_storagebinid = fq.dim_storagebinid) */
         AND NOT EXISTS
                (SELECT 1
                   FROM tmp_fact_wmquant fq
                  WHERE     SB.dim_storagebinid = fq.dim_storagebinid);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_partid = IFNULL(dp.dim_partid ,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
       dim_part dp,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE dp.PartNumber = st.LQUA_MATNR
	AND dp.plant = st.LQUA_WERKS
	AND  st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND  st.LQUA_LQNUM = fwmq.dd_quantno
    AND pl.dim_plantid = fwmq.dim_plantid
	AND  fwmq.dim_partid <> IFNULL(dp.dim_partid ,1);


UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_plantid = IFNULL(pl.Dim_Plantid ,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE  st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND  st.LQUA_LQNUM = fwmq.dd_quantno
	AND  fwmq.dim_plantid <> IFNULL(pl.Dim_Plantid ,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_storagetypeid = IFNULL(sty.dim_wmstoragetypeid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmstoragetype sty
, tmp_fact_wmquant fwmq
WHERE  sty.storagetype = st.LQUA_LGTYP
	AND sty.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_storagetypeid <> IFNULL(sty.dim_wmstoragetypeid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_storagebinid = IFNULL(sb.dim_storagebinid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_storagebin sb
, tmp_fact_wmquant fwmq
WHERE  sb.storagebin = st.LQUA_LGPLA
	AND sb.storagetype = st.LQUA_LGTYP
	AND sb.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_storagebinid <> IFNULL(sb.dim_storagebinid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_totalcapacity = IFNULL(l.LAGP_LKAPV,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       dim_storagebin sb,
	   LAGP l
, tmp_fact_wmquant fwmq
WHERE  l.LAGP_LGPLA = sb.StorageBin
	AND l.LAGP_LGTYP = sb.StorageType
	AND l.LAGP_LGNUM = sb.WarehouseNumber
	AND fwmq.dim_storagebinid = sb.dim_storagebinid
	AND (CASE WHEN l.LAGP_LGPLA = 'D04054002' AND l.LAGP_KZLER = 'X' THEN 0 ELSE 1 END) =1/*Octavian S - 7-SEP-2018 - Temporary fix until SAP  LAGP  will be fixed for grain LGNUM = 'NL1', LGTYP='RTO', LGPLA='D04054002'*/
	AND fwmq.ct_totalcapacity <> IFNULL(l.LAGP_LKAPV,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_remainingcapacity = IFNULL(l.LAGP_RKAPV,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       dim_storagebin sb,
	   LAGP l
, tmp_fact_wmquant fwmq
WHERE  l.LAGP_LGPLA = sb.StorageBin
	AND l.LAGP_LGTYP = sb.StorageType
	AND l.LAGP_LGNUM = sb.WarehouseNumber
	AND fwmq.dim_storagebinid = sb.dim_storagebinid
	AND fwmq.ct_remainingcapacity <> IFNULL(l.LAGP_RKAPV,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_numberofquantsinuse = IFNULL(l.LAGP_ANZQU,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       dim_storagebin sb,
	   LAGP l
, tmp_fact_wmquant fwmq
WHERE  l.LAGP_LGPLA = sb.StorageBin
	AND l.LAGP_LGTYP = sb.StorageType
	AND l.LAGP_LGNUM = sb.WarehouseNumber
	AND fwmq.dim_storagebinid = sb.dim_storagebinid
	AND fwmq.ct_numberofquantsinuse <> IFNULL(l.LAGP_ANZQU,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_maxnumberofquants = IFNULL(l.LAGP_MAXQU,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       dim_storagebin sb,
	   LAGP l
, tmp_fact_wmquant fwmq
WHERE  l.LAGP_LGPLA = sb.StorageBin
	AND l.LAGP_LGTYP = sb.StorageType
	AND l.LAGP_LGNUM = sb.WarehouseNumber
	AND fwmq.dim_storagebinid = sb.dim_storagebinid
	AND fwmq.ct_maxnumberofquants <> IFNULL(l.LAGP_MAXQU,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_storagelocationid = IFNULL(sl.dim_storagelocationid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_storagelocation sl
, tmp_fact_wmquant fwmq
WHERE  pl.plantcode = st.LQUA_WERKS
	AND sl.locationcode = st.LQUA_LGORT
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND sl.plant = st.LQUA_WERKS
	AND fwmq.dim_storagelocationid <> IFNULL(sl.dim_storagelocationid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_unitofmeasureid = IFNULL( um.dim_unitofmeasureid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	  dim_unitofmeasure um
, tmp_fact_wmquant fwmq
WHERE  um.uom = st.LQUA_MEINS
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_unitofmeasureid <> IFNULL( um.dim_unitofmeasureid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_availablestock = IFNULL(st.LQUA_VERME, 0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND  fwmq.ct_availablestock <> IFNULL(st.LQUA_VERME, 0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_batchnumber  = IFNULL(st.LQUA_CHARG,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_batchnumber  <> IFNULL(st.LQUA_CHARG,'Not Set');


UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_wmstockcategoryid  = IFNULL(wmsc.dim_wmstockcategoryid,1),
    dw_update_date = CURRENT_TIMESTAMP
	FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   (select WMStockCategory,max(dim_wmstockcategoryid) dim_wmstockcategoryid from dim_wmstockcategory
group by WMStockCategory) wmsc
, tmp_fact_wmquant fwmq
WHERE wmsc.WMStockCategory = st.LQUA_BESTQ
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_wmstockcategoryid  <> IFNULL(wmsc.dim_wmstockcategoryid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_specialstockid  = IFNULL(ss.dim_specialstockid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_specialstock ss
, tmp_fact_wmquant fwmq
WHERE ss.specialstockindicator = st.LQUA_SOBKZ
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_specialstockid  <> IFNULL(ss.dim_specialstockid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_specialstockno  = IFNULL(st.LQUA_SONUM,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_specialstockno  <>  IFNULL(st.LQUA_SONUM,'Not Set');

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_wmblockingreasonid  = IFNULL(wmbr.dim_wmblockingreasonid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmblockingreason wmbr
, tmp_fact_wmquant fwmq
WHERE wmbr.WMBlockingReason = st.LQUA_SPGRU
	AND wmbr.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_wmblockingreasonid  <> IFNULL(wmbr.dim_wmblockingreasonid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_dateidlastmovement = IFNULL(lmd.dim_dateid, 1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lmd
, tmp_fact_wmquant fwmq
WHERE lmd.DateValue = st.LQUA_BDATU
	AND lmd.CompanyCode = pl.CompanyCode
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND pl.plantcode = lmd.plantcode_factory
	AND fwmq.dim_dateidlastmovement <> IFNULL(lmd.dim_dateid, 1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_lastchangetodocno  = IFNULL(st.LQUA_BTANR,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
	   dim_warehousenumber wn,
	   dim_plant pl
, tmp_fact_wmquant fwmq
WHERE wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND st.LQUA_WERKS = pl.PlantCode
	AND fwmq.dd_lastchangetodocno <> IFNULL(st.LQUA_BTANR,'Not Set');


UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_lastchangetodocitemno  = IFNULL(st.LQUA_BTAPS,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_lastchangetodocitemno  <> IFNULL(st.LQUA_BTAPS,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_dateidlaststockplacement = IFNULL( lspd.dim_dateid, 1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lspd
, tmp_fact_wmquant fwmq
WHERE lspd.DateValue = st.LQUA_EDATU
	AND lspd.CompanyCode = pl.CompanyCode
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND pl.plantcode = lspd.plantcode_factory
	AND fwmq.dim_dateidlaststockplacement <> IFNULL( lspd.dim_dateid, 1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_dateidlaststockremoval = IFNULL( lsrd.dim_dateid, 1),
   dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lsrd
, tmp_fact_wmquant fwmq
WHERE lsrd.DateValue = st.LQUA_ADATU
	AND lsrd.CompanyCode = pl.CompanyCode
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND pl.plantcode = lsrd.plantcode_factory
	AND fwmq.dim_dateidlaststockremoval <> IFNULL( lsrd.dim_dateid, 1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_dateidlaststockaddition = IFNULL( lsad.dim_dateid, 1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lsad
, tmp_fact_wmquant fwmq
WHERE lsad.DateValue = st.LQUA_ZDATU
	AND lsad.CompanyCode = pl.CompanyCode
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND pl.plantcode = lsad.plantcode_factory
	AND fwmq.dim_dateidlaststockaddition <> IFNULL( lsad.dim_dateid, 1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_dateidgoodsreceipt = IFNULL( grd.dim_dateid, 1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date grd
, tmp_fact_wmquant fwmq
WHERE grd.DateValue = st.LQUA_WDATU
	AND grd.CompanyCode = pl.CompanyCode
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND pl.plantcode = grd.plantcode_factory
	AND fwmq.dim_dateidgoodsreceipt <> IFNULL( grd.dim_dateid, 1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_dateidgoodsreceipt = IFNULL( grd.dim_dateid, 1),
    dw_update_date = CURRENT_TIMESTAMP
FROM   LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date grd
, tmp_fact_wmquant fwmq
WHERE grd.DateValue = st.LQUA_WDATU
	AND grd.CompanyCode = pl.CompanyCode
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND pl.plantcode = grd.plantcode_factory
	AND fwmq.dim_dateidgoodsreceipt <> IFNULL( grd.dim_dateid, 1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_goodsreceiptno  = IFNULL(st.LQUA_WENUM,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
	   dim_warehousenumber wn,
	   dim_plant pl
, tmp_fact_wmquant fwmq
WHERE wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND st.LQUA_WERKS = pl.PlantCode
	AND fwmq.dd_goodsreceiptno <> IFNULL(st.LQUA_WENUM,'Not Set');


UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_goodsreceiptitemno  = IFNULL(st.LQUA_WEPOS,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
	   dim_warehousenumber wn,
	   dim_plant pl
, tmp_fact_wmquant fwmq
WHERE wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND st.LQUA_WERKS = pl.PlantCode
	AND fwmq.dd_goodsreceiptitemno  <> IFNULL(st.LQUA_WEPOS,0);


merge into tmp_fact_wmquant fwmq
using (select distinct fwmq.fact_wmquantid, wmsut.dim_wmstorageunittypeid
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmstorageunittype wmsut
, tmp_fact_wmquant fwmq
WHERE wmsut.StorageUnitType = st.LQUA_LETYP
	AND wmsut.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid) t
on t.fact_wmquantid = fwmq.fact_wmquantid
when matched then update set fwmq.dim_wmstorageunittypeid  =  IFNULL(t.dim_wmstorageunittypeid,1)
where fwmq.dim_wmstorageunittypeid <> IFNULL(t.dim_wmstorageunittypeid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_totalqty  = IFNULL(st.LQUA_GESME,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_totalqty <> IFNULL(st.LQUA_GESME,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_putawayqty  = IFNULL(st.LQUA_EINME,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_putawayqty <> IFNULL(st.LQUA_EINME,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_removeqty  = IFNULL(st.LQUA_AUSME,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_removeqty <> IFNULL(st.LQUA_AUSME,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_materialweight  = IFNULL( st.LQUA_MGEWI,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_materialweight <> IFNULL( st.LQUA_MGEWI,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_weightunitid  = IFNULL(wum.dim_unitofmeasureid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_unitofmeasure wum
, tmp_fact_wmquant fwmq
WHERE wum.uom = st.LQUA_GEWEI
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_weightunitid  <> IFNULL(wum.dim_unitofmeasureid,1);


UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_transferreqno  = IFNULL(st.LQUA_TBNUM,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_transferreqno <> IFNULL(st.LQUA_TBNUM,'Not Set');

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_inventorydocitemno  = IFNULL(st.LQUA_IVPOS,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_inventorydocitemno <> IFNULL(st.LQUA_IVPOS,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_wmrequirementtypeid  =  IFNULL(wmrt.dim_wmrequirementtypeid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmrequirementtype wmrt
, tmp_fact_wmquant fwmq
WHERE wmrt.RequirementType = st.LQUA_BETYP
	AND wmrt.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_wmrequirementtypeid <> IFNULL(wmrt.dim_wmrequirementtypeid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_requirementno  = IFNULL(st.LQUA_BENUM,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_requirementno <> IFNULL(st.LQUA_BENUM,'Not Set');

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_storageunitno  = IFNULL(st.LQUA_LENUM,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_storageunitno <> IFNULL(st.LQUA_LENUM,'Not Set');


UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_inspectionlotno  = IFNULL(st.LQUA_QPLOS,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_inspectionlotno <> IFNULL(st.LQUA_QPLOS,'Not Set');

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_dateidbestbefore = IFNULL( bbd.dim_dateid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date bbd
, tmp_fact_wmquant fwmq
WHERE bbd.DateValue = st.LQUA_VFDAT
	AND bbd.CompanyCode = pl.CompanyCode
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND pl.plantcode = bbd.plantcode_factory
	AND fwmq.dim_dateidbestbefore <> IFNULL( bbd.dim_dateid,1);


UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_capacityusage  = IFNULL(st.LQUA_QKAPV,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_capacityusage <> IFNULL(st.LQUA_QKAPV,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_wmpickingareaid  = IFNULL(wmpa.dim_wmpickingareaid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmpickingarea wmpa
, tmp_fact_wmquant fwmq
WHERE wmpa.StorageType = st.LQUA_LGTYP
	AND wmpa.PickingArea = st.LQUA_KOBER
	AND wmpa.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dim_wmpickingareaid <> IFNULL(wmpa.dim_wmpickingareaid,1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_opentransferqty  = IFNULL(st.LQUA_TRAME, 0) ,
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_opentransferqty  <> IFNULL(st.LQUA_TRAME, 0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_deliveryno  = IFNULL(st.LQUA_VBELN,'Not Set'),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_deliveryno <> IFNULL(st.LQUA_VBELN,'Not Set');

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dd_deliveryitemno  = IFNULL(st.LQUA_POSNR,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.dd_deliveryitemno  <> IFNULL(st.LQUA_POSNR,0);



UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_loadingwquipmentquantity1  = IFNULL(m.MLGN_LHMG1,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   MLGN m
, tmp_fact_wmquant fwmq
where  st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND ST.LQUA_MATNR = M.MLGN_MATNR
	AND ST.LQUA_LGNUM = M.MLGN_LGNUM
	and st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_loadingwquipmentquantity1  <> IFNULL(m.MLGN_LHMG1,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_loadingwquipmentquantity2  = IFNULL(m.MLGN_LHMG2,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   MLGN m
, tmp_fact_wmquant fwmq
where  st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND ST.LQUA_MATNR = M.MLGN_MATNR
	AND ST.LQUA_LGNUM = M.MLGN_LGNUM
	and st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_loadingwquipmentquantity2  <> IFNULL(m.MLGN_LHMG2,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.ct_loadingwquipmentquantity3  = IFNULL(m.MLGN_LHMG3,0),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   MLGN m
, tmp_fact_wmquant fwmq
where  st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND ST.LQUA_MATNR = M.MLGN_MATNR
	AND ST.LQUA_LGNUM = M.MLGN_LGNUM
	and st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND fwmq.ct_loadingwquipmentquantity3  <> IFNULL(m.MLGN_LHMG3,0);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_dateidlastinventory = IFNULL( lid.dim_dateid, 1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lid
, tmp_fact_wmquant fwmq
WHERE lid.DateValue = ifnull(st.LQUA_IDATU,'0001-01-01')
	AND lid.CompanyCode = pl.CompanyCode
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND pl.plantcode = lid.plantcode_factory
	AND fwmq.dim_dateidlastinventory <> IFNULL( lid.dim_dateid, 1);

UPDATE tmp_fact_wmquant fwmq
SET fwmq.dim_wmquantmiscellaneousid = IFNULL ( wmqmisc.dim_wmquantmiscellaneousid,1),
    dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmquantmiscellaneous wmqmisc
, tmp_fact_wmquant fwmq
WHERE st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND wmqmisc.userputawayblocked  = IFNULL(st.LQUA_SKZUE,'Not Set')
	AND wmqmisc.userremovalblocked  = IFNULL(st.LQUA_SKZUA,'Not Set')
	AND wmqmisc.systcurrentplacementblocked = IFNULL(st.LQUA_SKZSE,'Not Set')
	AND wmqmisc.systremovalblocked  = IFNULL(st.LQUA_SKZSA,'Not Set')
	AND wmqmisc.systInventoryblocked  = IFNULL(st.LQUA_SKZSI,'Not Set')
	AND wmqmisc.OnHandlingUnit = IFNULL(st.LQUA_KZHUQ,'Not Set')
	AND fwmq.dim_wmquantmiscellaneousid <> IFNULL ( wmqmisc.dim_wmquantmiscellaneousid,1);

update tmp_fact_wmquant ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp
, tmp_fact_wmquant ia
where ia.dim_partid = dp.dim_partid and
ia.dd_batchnumber = b.batchnumber and
dp.partnumber = b.partnumber and
dp.plant = b.plantcode
and ia.dim_batchid <> b.dim_batchid;

/* Liviu Ionescu BI-3437 */

merge into tmp_fact_wmquant fwmq
using (select distinct fwmq.fact_wmquantid, first_value(wmsut.dim_wmstorageunittypeid) over (partition by  fwmq.fact_wmquantid order by  fwmq.fact_wmquantid) as dim_wmstorageunittypeid 
FROM
	dim_part dp
	,dim_wmstorageunittype wmsut
	,MLGN ml, tmp_fact_wmquant fwmq
WHERE  fwmq.dim_partid = dp.dim_partid
	AND IFNULL(ml.MLGN_MATNR,'Not Set') = dp.partnumber
	AND IFNULL(ml.MLGN_LGNUM,'Not Set') = wmsut.WarehouseNumber
	AND wmsut.StorageUnitType = IFNULL(ml.MLGN_LETY1, 'Not Set')) t
on t.fact_wmquantid = fwmq.fact_wmquantid
when matched then update set fwmq.dim_wmstorageunittypeid1  =  IFNULL(t.dim_wmstorageunittypeid,1)
where fwmq.dim_wmstorageunittypeid1 <> IFNULL(t.dim_wmstorageunittypeid,1);


merge into tmp_fact_wmquant fwmq
using (select distinct fwmq.fact_wmquantid, first_value(wmsut.dim_wmstorageunittypeid) over (partition by  fwmq.fact_wmquantid order by  fwmq.fact_wmquantid) as dim_wmstorageunittypeid 
FROM
	dim_part dp
	,dim_wmstorageunittype wmsut
	,MLGN ml, tmp_fact_wmquant fwmq
WHERE  fwmq.dim_partid = dp.dim_partid
	AND IFNULL(ml.MLGN_MATNR,'Not Set') = dp.partnumber
	AND IFNULL(ml.MLGN_LGNUM,'Not Set') = wmsut.WarehouseNumber
	AND wmsut.StorageUnitType = IFNULL(ml.MLGN_LETY2, 'Not Set')) t
on t.fact_wmquantid = fwmq.fact_wmquantid
when matched then update set fwmq.dim_wmstorageunittypeid2  =  IFNULL(t.dim_wmstorageunittypeid,1)
where fwmq.dim_wmstorageunittypeid2 <> IFNULL(t.dim_wmstorageunittypeid,1);


merge into tmp_fact_wmquant fwmq
using (select distinct fwmq.fact_wmquantid, first_value(wmsut.dim_wmstorageunittypeid) over (partition by  fwmq.fact_wmquantid order by  fwmq.fact_wmquantid) as dim_wmstorageunittypeid 
FROM
	dim_part dp
	,dim_wmstorageunittype wmsut
	,MLGN ml, tmp_fact_wmquant fwmq
WHERE  fwmq.dim_partid = dp.dim_partid
	AND IFNULL(ml.MLGN_MATNR,'Not Set') = dp.partnumber
	AND IFNULL(ml.MLGN_LGNUM,'Not Set') = wmsut.WarehouseNumber
	AND wmsut.StorageUnitType = IFNULL(ml.MLGN_LETY3, 'Not Set')) t
on t.fact_wmquantid = fwmq.fact_wmquantid
when matched then update set fwmq.dim_wmstorageunittypeid3  =  IFNULL(t.dim_wmstorageunittypeid,1)
where fwmq.dim_wmstorageunittypeid3 <> IFNULL(t.dim_wmstorageunittypeid,1);

UPDATE tmp_fact_wmquant fwmq

set fwmq.ct_maxibinqty = IFNULL(ml.MLGT_LPMAX,0)
FROM dim_part dp
	,dim_warehousenumber wn
	,dim_wmstoragetype wmst
	,MLGT ml, tmp_fact_wmquant fwmq
where fwmq.dim_partid = dp.dim_partid
	AND fwmq.dim_warehousenumberid = wn.dim_warehousenumberid
	and fwmq.dim_storagetypeid = wmst.dim_wmstoragetypeid
	and IFNULL(ml.MLGT_MATNR,'Not Set') = dp.partnumber
	and IFNULL(ml.MLGT_LGNUM,'Not Set') = wn.warehousecode
	and IFNULL(ml.MLGT_LGTYP,'Not Set') = wmst.storagetype
	and fwmq.ct_maxibinqty <> IFNULL(ml.MLGT_LPMAX,0);

UPDATE tmp_fact_wmquant fwmq

set fwmq.ct_minbinqty = ifnull(ml.MLGT_LPMIN,0)
FROM dim_part dp
	,dim_warehousenumber wn
	,dim_wmstoragetype wmst
	,MLGT ml, tmp_fact_wmquant fwmq
where fwmq.dim_partid = dp.dim_partid
	AND fwmq.dim_warehousenumberid = wn.dim_warehousenumberid
	and fwmq.dim_storagetypeid = wmst.dim_wmstoragetypeid
	and IFNULL(ml.MLGT_MATNR,'Not Set') = dp.partnumber
	and IFNULL(ml.MLGT_LGNUM,'Not Set') = wn.warehousecode
	and IFNULL(ml.MLGT_LGTYP,'Not Set') = wmst.storagetype
	and fwmq.ct_minbinqty <> ifnull(ml.MLGT_LPMIN,0);

/* End Liviu Ionescu BI-3437 */
/*Alin Gheorghe - 9 Nov 2016 -  BI-4667*/
UPDATE tmp_fact_wmquant fwmq
set fwmq.ct_replenishmentqty = ifnull(ml.MLGT_NSMNG,0)
FROM dim_part dp
	,dim_warehousenumber wn
	,dim_wmstoragetype wmst
	,MLGT ml, tmp_fact_wmquant fwmq
where fwmq.dim_partid = dp.dim_partid
	AND fwmq.dim_warehousenumberid = wn.dim_warehousenumberid
	and fwmq.dim_storagetypeid = wmst.dim_wmstoragetypeid
	and IFNULL(ml.MLGT_MATNR,'Not Set') = dp.partnumber
	and IFNULL(ml.MLGT_LGNUM,'Not Set') = wn.warehousecode
	and IFNULL(ml.MLGT_LGTYP,'Not Set') = wmst.storagetype
	and fwmq.ct_replenishmentqty <> ifnull(ml.MLGT_NSMNG,0);
/* end BI-4667*/
 /* Update std_exchangerate_dateid by FPOPESCU on 05 January 2016 . */

UPDATE tmp_fact_wmquant f
	SET f.std_exchangerate_dateid = dt.dim_dateid
FROM dim_plant p
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode AND dt.plantcode_factory = p.plantcode
, tmp_fact_wmquant f
	WHERE f.dim_plantid = p.dim_plantid
AND   dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

 /* END Update std_exchangerate_dateid by FPOPESCU on 05 January 2016 . */

 /* Madalina 15 Sep 2016 - Add Bin Position field, based on LQUA_PLPOS - BI-4048 */
 UPDATE tmp_fact_wmquant fwmq
SET dd_binPosition = ifnull(LQUA_PLPOS, 'Not Set'), 
	dw_update_date = CURRENT_TIMESTAMP
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmstoragetype sty
, tmp_fact_wmquant fwmq
WHERE  sty.storagetype = st.LQUA_LGTYP
	AND sty.WarehouseNumber = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND st.LQUA_WERKS = pl.PlantCode
	AND wn.warehousecode = IFNULL(st.LQUA_LGNUM, 'Not Set')
	AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
	AND st.LQUA_LQNUM = fwmq.dd_quantno
	AND pl.dim_plantid = fwmq.dim_plantid
	AND dd_binPosition <> ifnull(LQUA_PLPOS, 'Not Set'); 
/* END BI-4048 */

TRUNCATE TABLE fact_wmquant;
INSERT INTO fact_wmquant
SELECT * FROM tmp_fact_wmquant;

DROP TABLE IF EXISTS tmp_fact_wmquant;

