/* ******************************************************************************************************************************************************************************** */
/* ****************************************second step: generate the combinations of the material states ************************************************************************** */
/* Source tables for this first part: Material movement, Purchasing, Inspection Lot, Production Order */
/* ******************************************************************************************************************************************************************************** */

drop table if exists tmp_mat_states;
create table tmp_mat_states as
select 'X' as  raw_1, 
'X' as  antigen_1, 'X' as  antigen_2,
'X' as  antigen_3, 'X' as  antigen_4, 'X' as  antigen_5,
'X' as  bulk_1, 'X' as  bulk_2, 'X' as  bulk_3, 
'X' as  fpu_1, 'X' as  fpu_2, 'X' as  fpu_3, 'X' as  fpu_4,'X' as  fpu_5,
'X' as  fpp_1, 'X' as  fpp_2, 'X' as  fpp_3,
1 as ct_1; /* dummy values to generate combinations of materials */

/*create a temp table, from which I delete the unavailable material combinations, and insert into tmp_mat_states_combinations-> to keep a mapping between pkey and the row number */
drop table if exists tmp_mat_states_combinations_temp_rawantigenbulk;
create table tmp_mat_states_combinations_temp_rawantigenbulk as
select  raw_1,  antigen_1,  antigen_2,  antigen_3,  antigen_4,  antigen_5,
 bulk_1,  bulk_2,  bulk_3
, sum(ct_1) as ct_1
from tmp_mat_states
group by cube ( raw_1,  antigen_1,  antigen_2,  antigen_3,  antigen_4,  antigen_5,
 bulk_1,  bulk_2,  bulk_3
);

drop table if exists tmp_mat_states_combinations_temp_fpufpp;
create table tmp_mat_states_combinations_temp_fpufpp as
select  fpu_1,  fpu_2,  fpu_3,  fpu_4,  fpu_5,  fpp_1, fpp_2, fpp_3
, sum(ct_1) as ct_2
from tmp_mat_states
group by cube ( fpu_1,  fpu_2,  fpu_3,  fpu_4,  fpu_5,  fpp_1, fpp_2, fpp_3);

drop table if exists tmp_mat_states_combinations_temp;
create table tmp_mat_states_combinations_temp as
select * from tmp_mat_states_combinations_temp_rawantigenbulk, tmp_mat_states_combinations_temp_fpufpp;

/*delete the combinations that are not valid*/
delete from tmp_mat_states_combinations_temp 
	where ( antigen_5 = 'X' and  antigen_4 is null) or ( antigen_5 = 'X' and  antigen_3 is null) or ( antigen_5 = 'X' and  antigen_2 is null)
	or ( antigen_5 = 'X' and  antigen_1 is null) or ( antigen_4 = 'X' and  antigen_3 is null) or ( antigen_4 = 'X' and  antigen_2 is null)
	or ( antigen_4 = 'X' and  antigen_1 is null) or ( antigen_3 = 'X' and  antigen_2 is null) or ( antigen_3 = 'X' and  antigen_1 is null) 
	or ( antigen_2 = 'X' and  antigen_1 is null)
	or ( bulk_2 = 'X' and  bulk_1 is null) or ( bulk_3 = 'X' and  bulk_1 is null) or (  bulk_3 = 'X' and  bulk_2 is null)
	or ( fpu_5 = 'X' and  fpu_4 is null) or ( fpu_5 = 'X' and  fpu_3 is null) or ( fpu_5 = 'X' and  fpu_2 is null) or ( fpu_5 = 'X' and  fpu_1 is null)
	or ( fpu_4 = 'X' and  fpu_3 is null) or ( fpu_4 = 'X' and  fpu_2 is null) or ( fpu_4 = 'X' and  fpu_1 is null) or ( fpu_3 = 'X' and  fpu_1 is null)
	or ( fpu_3 = 'X' and  fpu_2 is null) or ( fpu_2 = 'X' and  fpu_1 is null) 
	or ( fpp_3 = 'X' and  fpp_2 is null) or ( fpp_3 = 'X' and  fpp_1 is null) or ( fpp_2 = 'X' and  fpp_1 is null);
	

/*determine the number of active materials per combination - in order to first process the combinations with the highest number of active materials */
update tmp_mat_states_combinations_temp 
set ct_1 = (
	case when  raw_1 = 'X' then 1 else 0 end +
	case when  antigen_1 = 'X' then 1 else 0 end +
    case when  antigen_2 = 'X' then 1 else 0 end +
	case when  antigen_3 = 'X' then 1 else 0 end +
	case when  antigen_4 = 'X' then 1 else 0 end +
    case when  antigen_5 = 'X' then 1 else 0 end +
 	case when  bulk_1 = 'X' then 1 else 0 end +
	case when  bulk_2 = 'X' then 1 else 0 end +
	case when  bulk_3 = 'X' then 1 else 0 end +
	case when  fpu_1 = 'X' then 1 else 0 end +
	case when  fpu_2 = 'X' then 1 else 0 end +
	case when  fpu_3 = 'X' then 1 else 0 end +
	case when  fpu_4 = 'X' then 1 else 0 end +
	case when  fpu_5 = 'X' then 1 else 0 end +
	case when  fpp_1 = 'X' then 1 else 0 end +
	case when  fpp_2 = 'X' then 1 else 0 end +
	case when  fpp_3 = 'X' then 1 else 0 end);

/*no need to process the combinations of 1 active material*/
delete from tmp_mat_states_combinations_temp where ct_1 in ( 0, 1);

/*keep the correct order of combinations*/
drop table if exists tmp_mat_states_combinations;
create table tmp_mat_states_combinations as
select * from tmp_mat_states_combinations_temp
order by ct_1 desc;

/************** missing antigen records - APP-8329  ***********/

drop table if exists fact_prodorder_missingAntigen;
create table fact_prodorder_missingAntigen as 
select b.batchnumber, a.*
	from temp_antigen a inner join dim_batch b on a.dim_batchid = b.dim_batchid;

drop table if exists temp_antigen_missing;
create table temp_antigen_missing as
select distinct 
	f.dim_plantid, 
	f.dim_partid, 
	f.dim_batchid, 
	a.dd_inspectionlotno, 
	a.dd_ordernumber, 
	a.dd_DocumentNo, 
	a.dd_DocumentItemNo, 
	 a.dd_scheduleno, 
	 a.dim_dateidschedorder, 
	 a.dim_dateidcreate, 
	 a.dim_dateidpostingdate, 
	 a.dd_dateuserstatussamr, 
	 a.dd_dateuserstatusqcco, 
	 a.dim_dateidinspectionstart, 
	 a.dim_dateidusagedecisionmade, 
	 a.dim_partid_interpl, 
	 a.dim_batchid_interpl, 
	 a.dim_plantid_interpl, 
	 a.dim_dateidactualrelease 
from fact_materialmovement f 
	inner join dim_batch b on b.dim_batchid = f.dim_batchid 
	inner join dim_movementtype mt on f.dim_movementtypeid = mt.dim_movementtypeid 
	inner join dim_part dp on dp.dim_partid = f.dim_partid  
	inner join fact_prodorder_missingAntigen a on a.batchnumber = substring(b.batchnumber,1, len(b.batchnumber) -2)
and mt.movementtype in ('261','262') 
and lower(dp.ItemSubType_Merck) = 'interm' ;  

insert into temp_antigen a
select * from temp_antigen_missing am
	where not exists ( select 1 from temp_antigen a
			where a.dim_plantid = am.dim_plantid and
			a.dim_partid = am.dim_partid and
			a.dim_batchid = am.dim_batchid and
			a.dd_inspectionlotno = am.dd_inspectionlotno and 
			a.dd_ordernumber = am.dd_ordernumber);


/* ********************************************************** with less material states **************************** */

/* drop table if exists tmp_mat_states
create table tmp_mat_states as
select 'X' as  raw_1, 
'X' as  bulk_1, 'X' as  bulk_2, 'X' as  bulk_3, 
'X' as  fpu_1, 'X' as  fpu_2, 'X' as  fpu_3, 
'X' as  fpp_1, 'X' as  fpp_2, 
1 as ct_1 */  /* dummy values to generate combinations of materials */

/*create a temp table, from which I delete the unavailable material combinations, and insert into tmp_mat_states_combinations-> to keep a mapping between pkey and the row number */
/* drop table if exists tmp_mat_states_combinations_temp_rawantigenbulk
create table tmp_mat_states_combinations_temp_rawantigenbulk as
select  raw_1,  
 bulk_1,  bulk_2,  bulk_3
, sum(ct_1) as ct_1
from tmp_mat_states
group by cube ( raw_1,  
 bulk_1,  bulk_2,  bulk_3
) */

/* drop table if exists tmp_mat_states_combinations_temp_fpufpp
create table tmp_mat_states_combinations_temp_fpufpp as
select  fpu_1,  fpu_2,  fpu_3,   fpp_1, fpp_2
, sum(ct_1) as ct_2
from tmp_mat_states
group by cube ( fpu_1,  fpu_2,  fpu_3,   fpp_1, fpp_2)

drop table if exists tmp_mat_states_combinations_temp 
create table tmp_mat_states_combinations_temp as
select * from tmp_mat_states_combinations_temp_rawantigenbulk, tmp_mat_states_combinations_temp_fpufpp */

/*delete the combinations that are not valid*/
/* delete from tmp_mat_states_combinations_temp 
	where 
	( bulk_2 = 'X' and  bulk_1 is null) or ( bulk_3 = 'X' and  bulk_1 is null) or (  bulk_3 = 'X' and  bulk_2 is null)
    or ( fpu_3 = 'X' and  fpu_1 is null)
	or ( fpu_3 = 'X' and  fpu_2 is null) or ( fpu_2 = 'X' and  fpu_1 is null) 
    or ( fpp_2 = 'X' and  fpp_1 is null) */
	

/*determine the number of active materials per combination - in order to first process the combinations with the highest number of active materials */
/* update tmp_mat_states_combinations_temp 
set ct_1 = (
	case when  raw_1 = 'X' then 1 else 0 end +
 	case when  bulk_1 = 'X' then 1 else 0 end +
	case when  bulk_2 = 'X' then 1 else 0 end +
	case when  bulk_3 = 'X' then 1 else 0 end +
	case when  fpu_1 = 'X' then 1 else 0 end +
	case when  fpu_2 = 'X' then 1 else 0 end +
	case when  fpu_3 = 'X' then 1 else 0 end +
	case when  fpp_1 = 'X' then 1 else 0 end +
	case when  fpp_2 = 'X' then 1 else 0 end)  */


/*no need to process the combinations of 1 active material*/
/* delete from tmp_mat_states_combinations_temp where ct_1 in ( 0, 1) */

/*keep the correct order of combinations*/
/* drop table if exists tmp_mat_states_combinations
create table tmp_mat_states_combinations as
select * from tmp_mat_states_combinations_temp
order by ct_1 desc */

drop table if exists tmp_scm_fact_salesorderdelivery;
create table tmp_scm_fact_salesorderdelivery as 
select distinct f_sdeliv.dim_plantid,
                f_sdeliv.dim_plantidordering,
                f_sdeliv.dd_SalesDlvrDocNo,
                f_sdeliv.dd_SalesDlvrItemNo,
                f_sdeliv.dd_SalesDocNo,
                f_sdeliv.dd_SalesItemNo,
                f_sdeliv.dim_partid,
                f_sdeliv.dim_batchid,
                f_sdeliv.ct_QtyDelivered,
                f_sdeliv.dim_dateiddlvrdoccreated,
                f_sdeliv.dim_dateidactualgoodsissue,
                f_sdeliv.dim_dateidactualreceipt, 
                 f_sdeliv.Dim_CustomeridShipTo,
                 f_sdeliv.Dim_CustomeridSoldTo
 from fact_salesorderdelivery f_sdeliv 
  inner join dim_date dd on f_sdeliv.dim_dateidactualgoodsissue = dd.dim_dateid
where dd.datevalue >= add_years(current_date, -2.5);


drop table if exists tmp004_insplot_salesorderdelivery001;
create table tmp004_insplot_salesorderdelivery001 as
/* select distinct 
	dim_plantid,
    dim_plantidordering,
    dim_customeridshipto,
    dd_SalesDlvrDocNo,
    dd_SalesDlvrItemNo,
    dd_SalesDocNo,
    dd_SalesItemNo,
    dim_partid,
    dim_batchid,
    ct_QtyDelivered,
    dim_dateiddlvrdoccreated,
    dim_dateidactualgoodsissue,
    dim_dateidactualreceipt,
    dd_inspectionlotno,
    dd_dateuserstatussamr,
    dim_dateidinspectionstart,
    dd_dateuserstatusqcco,
    dim_dateidusagedecisionmade,
    ct_actuallotqty,
    partnumber,
    batchnumber,
    plantcode,
    plantcodeOrder

from 
( */
/* shipcomop */
select distinct f_sdeliv.dim_plantid,
                f_sdeliv.dim_plantidordering,
                /* f_sdeliv.dim_customeridshipto, */
                f_sdeliv.dd_SalesDlvrDocNo,
                f_sdeliv.dd_SalesDlvrItemNo,
                f_sdeliv.dd_SalesDocNo,
                f_sdeliv.dd_SalesItemNo,
                f_sdeliv.dim_partid,
                f_sdeliv.dim_batchid,
                f_sdeliv.ct_QtyDelivered,
                f_sdeliv.dim_dateiddlvrdoccreated,
                f_sdeliv.dim_dateidactualgoodsissue,
                f_sdeliv.dim_dateidactualreceipt,
                f_ins.dd_inspectionlotno,
                dd_dateuserstatussamr,
                f_ins.dim_dateidinspectionstart,
               dd_dateuserstatusqcco,
                f_ins.dim_dateidusagedecisionmade,
                f_ins.ct_actuallotqty,
                 padeliv.partnumber,
                 badeliv.batchnumber,
                 pldeliv.plantcode,
                 plorddeliv.plantcode as plantcodeOrder,
                 f_sdeliv.Dim_CustomeridShipTo,
                 f_sdeliv.Dim_CustomeridSoldTo
 from fact_inspectionlot f_ins,
      tmp_scm_fact_salesorderdelivery f_sdeliv,
      dim_part padeliv,
	  dim_part painsp,
      dim_batch badeliv,
	  dim_batch bainsp,
      dim_plant pldeliv,
      dim_plant plorddeliv
where bainsp.batchnumber = badeliv.batchnumber 
  and painsp.partnumber = padeliv.partnumber 
  and f_ins.dim_plantid = f_sdeliv.dim_plantidordering
  and padeliv.dim_partid = f_sdeliv.dim_partid
  and painsp.dim_partid = f_ins.dim_partid
  and badeliv.dim_batchid = f_sdeliv.dim_batchid
  and bainsp.dim_batchid = f_ins.dim_batchid
  and pldeliv.dim_plantid = f_sdeliv.dim_plantid
  and plorddeliv.dim_plantid = f_sdeliv.dim_plantidordering;
  
/*BI-5540 - Shippings with a set Delivery plant (f_sdeliv.dim_plantidordering <> 1) might also be un-inspected. 
Couldn't resolve with left join to Inspection lot because of  bainsp.batchnumber = badeliv.batchnumber and painsp.partnumber = padeliv.partnumber conditions */
  -- union all
  
  /*shipcust + un-inspected shipcomops*/
 insert into tmp004_insplot_salesorderdelivery001
(	
	dim_plantid,
    dim_plantidordering,
    /* dim_customeridshipto, */
    dd_SalesDlvrDocNo,
    dd_SalesDlvrItemNo,
    dd_SalesDocNo,
    dd_SalesItemNo,
    dim_partid,
    dim_batchid,
    ct_QtyDelivered,
    dim_dateiddlvrdoccreated,
    dim_dateidactualgoodsissue,
    dim_dateidactualreceipt,
    dd_inspectionlotno,
    dd_dateuserstatussamr,
    dim_dateidinspectionstart,
    dd_dateuserstatusqcco,
    dim_dateidusagedecisionmade,
    ct_actuallotqty,
    partnumber,
    batchnumber,
    plantcode,
    plantcodeOrder,
    Dim_CustomeridShipTo,
    Dim_CustomeridSoldTo
)
select distinct f_sdeliv.dim_plantid,
			f_sdeliv.dim_plantidordering,
			/*f_sdeliv.dim_customeridshipto, */
			f_sdeliv.dd_SalesDlvrDocNo,
			f_sdeliv.dd_SalesDlvrItemNo,
			f_sdeliv.dd_SalesDocNo,
			f_sdeliv.dd_SalesItemNo,
			f_sdeliv.dim_partid,
			f_sdeliv.dim_batchid,
			f_sdeliv.ct_QtyDelivered,
			f_sdeliv.dim_dateiddlvrdoccreated,
			f_sdeliv.dim_dateidactualgoodsissue,
			f_sdeliv.dim_dateidactualreceipt,
			convert(varchar(12), 'Not Set') as dd_inspectionlotno,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatussamr,
			convert(integer, 1) as dim_dateidinspectionstart,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatusqcco,
			convert(integer, 1) as dim_dateidusagedecisionmade,
			convert(decimal(18,4), 0) as ct_actuallotqty,
			 padeliv.partnumber,
			 badeliv.batchnumber,
			 pldeliv.plantcode,
			 plorddeliv.plantcode as plantcodeOrder,
       f_sdeliv.Dim_CustomeridShipTo,
       f_sdeliv.Dim_CustomeridSoldTo
from tmp_scm_fact_salesorderdelivery f_sdeliv,
  dim_part padeliv,
  dim_batch badeliv,
  dim_plant pldeliv,
  dim_plant plorddeliv
where 
  padeliv.dim_partid = f_sdeliv.dim_partid
  and badeliv.dim_batchid = f_sdeliv.dim_batchid
  and pldeliv.dim_plantid = f_sdeliv.dim_plantid
  and plorddeliv.dim_plantid = f_sdeliv.dim_plantidordering
 /* and f_sdeliv.dim_plantidordering = 1 */
  and not exists
	(select 1 from tmp004_insplot_salesorderdelivery001 tmp
		where tmp.dim_plantid = f_sdeliv.dim_plantid
				and tmp.dim_plantidordering = f_sdeliv.dim_plantidordering
				and tmp.dim_partid = f_sdeliv.dim_partid
				and tmp.dim_batchid = f_sdeliv.dim_batchid
	);
  
  
  
  /* Lite shipcomops - not working properly, some records are lost in the following left join for 002, but the fact triples its size */
 /* union all
 select distinct f_sdeliv.dim_plantid,
			f_sdeliv.dim_plantidordering,
			f_sdeliv.dim_customeridshipto,
			f_sdeliv.dd_SalesDlvrDocNo,
			f_sdeliv.dd_SalesDlvrItemNo,
			f_sdeliv.dd_SalesDocNo,
			f_sdeliv.dd_SalesItemNo,
			f_sdeliv.dim_partid,
			f_sdeliv.dim_batchid,
			f_sdeliv.ct_QtyDelivered,
			f_sdeliv.dim_dateiddlvrdoccreated,
			f_sdeliv.dim_dateidactualgoodsissue,
			f_sdeliv.dim_dateidactualreceipt,
			convert(varchar(12), 'Not Set') as dd_inspectionlotno,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatussamr,
			convert(integer, 1) as dim_dateidinspectionstart,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatusqcco,
			convert(integer, 1) as dim_dateidusagedecisionmade,
			convert(decimal(18,4), 0) as ct_actuallotqty,
			 padeliv.partnumber,
			 badeliv.batchnumber,
			 pldeliv.plantcode,
			 plorddeliv.plantcode as plantcodeOrder
from fact_salesorderdelivery f_sdeliv,
  dim_part padeliv,
  dim_batch badeliv,
  dim_plant pldeliv,
  dim_plant plorddeliv,
  dim_deliverytype dt
where 
  padeliv.dim_partid = f_sdeliv.dim_partid
  and badeliv.dim_batchid = f_sdeliv.dim_batchid
  and pldeliv.dim_plantid = f_sdeliv.dim_plantid
  and plorddeliv.dim_plantid = f_sdeliv.dim_plantidordering
  and f_sdeliv.dim_plantidordering <> 1
  and f_sdeliv.dim_deliverytypeid = dt.dim_deliverytypeid
  and dt.DeliveryType = 'LF' 
  
  ) a */

/*Madalina 2 Oct 2017 - fact optimizations - apply the returns deletion on the temp Shippping (001) table - part_2 script + bring in other Sales fields*/

drop table if exists tmp004_insplot_salesorderdelivery002;
create table tmp004_insplot_salesorderdelivery002 as
select  distinct convert( decimal(36,0), case when shipcomop.dim_plantidordering is null then shipcust.dim_plantid
        else shipcomop.dim_plantid end) as dim_plantid,
        ifnull(shipcomop.dim_plantidordering,1) as dim_plantid_comops,
        convert( decimal(36,0), shipcust.dim_partid) as dim_partid,
        convert( decimal(36,0), shipcust.dim_batchid) as dim_batchid,
        /* shipcust.dim_customeridshipto, */
        ifnull(shipcomop.dd_salesdlvrdocno,'Not Set') as dd_salesdlvrdocno_comops,
        ifnull(shipcomop.dd_salesdlvritemno,0) as dd_salesdlvritemno_comops,
        ifnull(shipcomop.dim_dateiddlvrdoccreated,1) as dim_dateiddlvrdoccreated_comops,
        ifnull(shipcomop.dim_dateidactualgoodsissue,1) as dim_dateidactualgoodsissue_comops,
        ifnull(shipcomop.dim_dateidactualreceipt,1) as dim_dateidactualreceipt_comops,
        case when shipcomop.dim_plantidordering is null then 'Not Set' else shipcomop.dd_inspectionlotno end as dd_inspectionlotno_comops,
        case when shipcomop.dim_plantidordering is null then '0001-01-01' else shipcomop.dd_dateuserstatussamr end as dd_dateuserstatussamr_comops,
        case when shipcomop.dim_plantidordering is null then 1 else shipcomop.dim_dateidinspectionstart end as dim_dateidinspectionstart_comops,
        case when shipcomop.dim_plantidordering is null then '0001-01-01' else shipcomop.dd_dateuserstatusqcco end as dd_dateuserstatusqcco_comops,
        case when shipcomop.dim_plantidordering is null then 1 else shipcomop.dim_dateidusagedecisionmade end as dim_dateidusagedecisionmade_comops,
        shipcust.dd_salesdlvrdocno as dd_salesdlvrdocno_customer,
        shipcust.dd_salesdlvritemno as dd_salesdlvritemno_customer,
        shipcust.dim_dateiddlvrdoccreated as dim_dateiddlvrdoccreated_customer,
        shipcust.dim_dateidactualgoodsissue as dim_dateidactualgoodsissue_customer,
        --ifnull(shipcomop.ct_qtydelivered,0) as  ct_qtydelivered, 
		ifnull(shipcust.ct_qtydelivered,0) as  ct_qtydelivered,
		/* Madalina 23 Sept 2016 - Adding Sales Document Number and Item, and Order Amount from the Sales Area*/ 
		shipcust.dd_SalesDocNo as dd_SalesDocNo_customer,
		shipcust.dd_SalesItemNo as dd_SalesItemNo_customer,
		shipcomop.dd_SalesDocNo as dd_SalesDocNo_comops,
		shipcomop.dd_SalesItemNo as dd_SalesItemNo_comops, 
		convert(varchar(10), 'Not Set') as basdbatchnumber,
		convert(varchar(22), 'Not Set') as pasdpartnumber,
    first_value((CASE WHEN ifnull(dsi.ReturnsItem, 'Not Set') = 'X' THEN (-1*so.amt_ScheduleTotal) 
                      ELSE so.amt_ScheduleTotal END)) over (partition by so.dd_SalesDocNo, so.dd_SalesItemNo order by '') as amt_orderAmountSales,
    first_value(so.amt_ExchangeRate) over (partition by so.dd_SalesDocNo, so.dd_SalesItemNo order by '') as amt_ExchangeRate,
    first_value(so.amt_ExchangeRate_GBL) over (partition by so.dd_SalesDocNo, so.dd_SalesItemNo order by '') as amt_ExchangeRate_GBL,
    convert(bigint, 1) as dim_partid_comops, /*update later*/
    shipcust.Dim_CustomeridShipTo as dim_customerShipCust,
    shipcust.Dim_CustomeridSoldTo as dim_customerSoldCust,
    shipcomop.Dim_CustomeridShipTo as dim_customerShipComops,
    shipcomop.Dim_CustomeridSoldTo as dim_customerSoldComops


        from tmp004_insplot_salesorderdelivery001 shipcust
        left join fact_salesorder so
           on shipcust.dd_SalesDocNo = so.dd_salesdocno
           and shipcust.dd_SalesItemNo = so.dd_salesitemno 
        left join dim_salesmisc dsi on so.dim_salesmiscid = dsi.dim_salesmiscid
       left join
        (select dim_plantidordering,dim_plantid,dd_salesdlvrdocno,dd_salesdlvritemno,dim_dateiddlvrdoccreated,
         dim_dateidactualgoodsissue,dim_dateidactualreceipt,ct_qtydelivered,partnumber,batchnumber,plantcodeOrder, dd_SalesDocNo, dd_SalesItemNo,
		dd_inspectionlotno, dd_dateuserstatussamr, dim_dateidinspectionstart, dd_dateuserstatusqcco, dim_dateidusagedecisionmade, Dim_CustomeridShipTo, Dim_CustomeridSoldTo
         from tmp004_insplot_salesorderdelivery001
where dim_plantidordering <> 1) shipcomop
  on shipcust.partnumber = shipcomop.partnumber
  and shipcust.batchnumber = shipcomop.batchnumber
  and shipcust.plantcode = shipcomop.plantcodeOrder
where shipcust.dim_plantidordering = 1
and ifnull(so.dd_ReturnsItem, 'Not Set') <> 'X';

update tmp004_insplot_salesorderdelivery002 mmp
set mmp.dim_partid_comops = fpp_comops.dim_partid
from tmp004_insplot_salesorderdelivery002 mmp, dim_part fpp, dim_part fpp_comops, dim_plant p_comops
where mmp.dim_partid = fpp.dim_partid 
  and mmp.dim_plantid_comops = p_comops.dim_plantid
  and fpp_comops.partnumber = fpp.partnumber
  and fpp_comops.plant = p_comops.plantcode
  and mmp.dim_partid_comops <> fpp_comops.dim_partid;


update tmp004_insplot_salesorderdelivery002
set amt_orderAmountSales = ifnull(amt_orderAmountSales,0) * ifnull(amt_ExchangeRate_GBL,0);

update tmp004_insplot_salesorderdelivery002 b
set basdbatchnumber = basd.batchnumber
from tmp004_insplot_salesorderdelivery002 b 
	inner join dim_batch basd on b.dim_batchid = basd.dim_batchid;  

update tmp004_insplot_salesorderdelivery002 b
set pasdpartnumber = pasd.partnumber
from tmp004_insplot_salesorderdelivery002 b 
	inner join dim_part pasd on  b.dim_partid = pasd.dim_partid;

alter table tmp004_insplot_salesorderdelivery002 add column tmp004_identity_id bigint identity;  

/*
merge into tmp004_insplot_salesorderdelivery002 fmm using
(select distinct tmp004_identity_id, 
         danew_usage.dim_dateid as dim_dateidusagedecisionmade_fpp_1, 
        danew_ship.dim_dateid as dim_dateiddlvrdoccreated_comops,
        danew_agi.dim_dateid as dim_dateidactualgoodsissue_comops,
        danew_ard.dim_dateid as dim_dateidactualreceipt_comops,
        danew_usagecom.dim_dateid as dim_dateidusagedecisionmade_comops,
        danew_shipcust.dim_dateid as dim_dateiddlvrdoccreated_customer,
        danew_agicust.dim_dateid as dim_dateidactualgoodsissue_customer
    from tmp004_insplot_salesorderdelivery002 fmm 
        inner join dim_plant pl_usage on fmm.dim_plantid = pl_usage.dim_plantid
        inner join dim_date da_usage on  fmm.dim_dateidusagedecisionmade_fpp_1 = da_usage.dim_dateid
        inner join dim_date danew_usage on da_usage.datevalue = danew_usage.datevalue
              and danew_usage.companycode = pl_usage.companycode
              and danew_usage.plantcode_factory = pl_usage.plantcode  

        inner join dim_plant pl_ship on fmm.dim_plantid = pl_ship.dim_plantid
        inner join  dim_date da_ship on fmm.dim_dateiddlvrdoccreated_comops = da_ship.dim_dateid
        inner join dim_date danew_ship on  da_ship.datevalue = danew_ship.datevalue
              and danew_ship.companycode = pl_ship.companycode
              and danew_ship.plantcode_factory = pl_ship.plantcode

        inner join dim_plant pl_agi on  fmm.dim_plantid = pl_agi.dim_plantid  
        inner join dim_date da_agi on fmm.dim_dateidactualgoodsissue_comops = da_agi.dim_dateid
        inner join dim_date danew_agi on  da_agi.datevalue = danew_agi.datevalue
              and danew_agi.companycode = pl_agi.companycode
              and danew_agi.plantcode_factory = pl_agi.plantcode

        inner join dim_plant pl_ard on  fmm.dim_plantid_comops = pl_ard.dim_plantid 
        inner join dim_date da_ard on fmm.dim_dateidactualreceipt_comops = da_ard.dim_dateid
        inner join dim_date danew_ard on  da_ard.datevalue = danew_ard.datevalue
              and danew_ard.companycode = pl_ard.companycode
              and danew_ard.plantcode_factory = pl_ard.plantcode

        inner join dim_plant pl_usagecom on fmm.dim_plantid_comops = pl_usagecom.dim_plantid  
        inner join dim_date da_usagecom on fmm.dim_dateidusagedecisionmade_comops = da_usagecom.dim_dateid
        inner join dim_date danew_usagecom on  da_usagecom.datevalue = danew_usagecom.datevalue
              and danew_usagecom.companycode = pl_usagecom.companycode
              and danew_usagecom.plantcode_factory = pl_usagecom.plantcode

        inner join dim_plant pl_shipcust on fmm.dim_plantid_comops = pl_shipcust.dim_plantid  
        inner join dim_date da_shipcust on fmm.dim_dateiddlvrdoccreated_customer = da_shipcust.dim_dateid
        inner join dim_date danew_shipcust on  da_shipcust.datevalue = danew_shipcust.datevalue
              and danew_shipcust.companycode = pl_shipcust.companycode
              and danew_shipcust.plantcode_factory = pl_shipcust.plantcode

        inner join dim_plant pl_agicust on fmm.dim_plantid_comops = pl_agicust.dim_plantid  
        inner join dim_date da_agicust on fmm.dim_dateidactualgoodsissue_customer = da_agicust.dim_dateid
        inner join dim_date danew_agicust on  da_agicust.datevalue = danew_agicust.datevalue
              and danew_agicust.companycode = pl_agicust.companycode
              and danew_agicust.plantcode_factory = pl_agicust.plantcode
    ) upd
on fmm.tmp004_identity_id = upd.tmp004_identity_id
when matched then update 
set
fmm.dim_dateidusagedecisionmade_fpp_1 = upd.dim_dateidusagedecisionmade_fpp_1,
fmm.dim_dateiddlvrdoccreated_comops = upd.dim_dateiddlvrdoccreated_comops,
fmm.dim_dateidactualgoodsissue_comops = upd.dim_dateidactualgoodsissue_comops,
fmm.dim_dateidactualreceipt_comops = upd.dim_dateidactualreceipt_comops,
fmm.dim_dateidusagedecisionmade_comops = upd.dim_dateidusagedecisionmade_comops,
fmm.dim_dateiddlvrdoccreated_customer = upd.dim_dateiddlvrdoccreated_customer,
fmm.dim_dateidactualgoodsissue_customer = upd.dim_dateidactualgoodsissue_customer */

merge into tmp004_insplot_salesorderdelivery002 fmm using
(select distinct tmp004_identity_id, 
                danew_ship.dim_dateid as dim_dateiddlvrdoccreated_comops
				  from tmp004_insplot_salesorderdelivery002 fmm 
             inner join dim_plant pl_ship on fmm.dim_plantid = pl_ship.dim_plantid
             inner join  dim_date da_ship on fmm.dim_dateiddlvrdoccreated_comops = da_ship.dim_dateid
             inner join dim_date danew_ship on  da_ship.datevalue = danew_ship.datevalue
              and danew_ship.companycode = pl_ship.companycode
              and danew_ship.plantcode_factory = pl_ship.plantcode) upd
on fmm.tmp004_identity_id = upd.tmp004_identity_id
when matched then update set fmm.dim_dateiddlvrdoccreated_comops = upd.dim_dateiddlvrdoccreated_comops;

merge into tmp004_insplot_salesorderdelivery002 fmm using
(select distinct tmp004_identity_id, 
                 danew_agi.dim_dateid as dim_dateidactualgoodsissue_comops
				  from tmp004_insplot_salesorderdelivery002 fmm 
             inner join dim_plant pl_agi on  fmm.dim_plantid = pl_agi.dim_plantid  
             inner join dim_date da_agi on fmm.dim_dateidactualgoodsissue_comops = da_agi.dim_dateid
             inner join dim_date danew_agi on  da_agi.datevalue = danew_agi.datevalue
              and danew_agi.companycode = pl_agi.companycode
              and danew_agi.plantcode_factory = pl_agi.plantcode) upd
on fmm.tmp004_identity_id = upd.tmp004_identity_id
when matched then update set fmm.dim_dateidactualgoodsissue_comops = upd.dim_dateidactualgoodsissue_comops;

merge into tmp004_insplot_salesorderdelivery002 fmm using
(select distinct tmp004_identity_id, 
                 danew_ard.dim_dateid as dim_dateidactualreceipt_comops
				  from tmp004_insplot_salesorderdelivery002 fmm 
                     inner join dim_plant pl_ard on  fmm.dim_plantid_comops = pl_ard.dim_plantid 
                     inner join dim_date da_ard on fmm.dim_dateidactualreceipt_comops = da_ard.dim_dateid
                     inner join dim_date danew_ard on  da_ard.datevalue = danew_ard.datevalue
                      and danew_ard.companycode = pl_ard.companycode
                      and danew_ard.plantcode_factory = pl_ard.plantcode) upd
on fmm.tmp004_identity_id = upd.tmp004_identity_id
when matched then update set fmm.dim_dateidactualreceipt_comops = upd.dim_dateidactualreceipt_comops;

merge into tmp004_insplot_salesorderdelivery002 fmm using
(select distinct tmp004_identity_id, 
                 danew_usagecom.dim_dateid as dim_dateidusagedecisionmade_comops
				  from tmp004_insplot_salesorderdelivery002 fmm 
                     inner join dim_plant pl_usagecom on fmm.dim_plantid_comops = pl_usagecom.dim_plantid  
                     inner join dim_date da_usagecom on fmm.dim_dateidusagedecisionmade_comops = da_usagecom.dim_dateid
                     inner join dim_date danew_usagecom on  da_usagecom.datevalue = danew_usagecom.datevalue
                      and danew_usagecom.companycode = pl_usagecom.companycode
                      and danew_usagecom.plantcode_factory = pl_usagecom.plantcode) upd
on fmm.tmp004_identity_id = upd.tmp004_identity_id
when matched then update set fmm.dim_dateidusagedecisionmade_comops = upd.dim_dateidusagedecisionmade_comops;

merge into tmp004_insplot_salesorderdelivery002 fmm using
(select distinct tmp004_identity_id, 
                  danew_shipcust.dim_dateid as dim_dateiddlvrdoccreated_customer
				  from tmp004_insplot_salesorderdelivery002 fmm 
                     inner join dim_plant pl_shipcust on fmm.dim_plantid_comops = pl_shipcust.dim_plantid  
                     inner join dim_date da_shipcust on fmm.dim_dateiddlvrdoccreated_customer = da_shipcust.dim_dateid
                     inner join dim_date danew_shipcust on  da_shipcust.datevalue = danew_shipcust.datevalue
                       and danew_shipcust.companycode = pl_shipcust.companycode
                       and danew_shipcust.plantcode_factory = pl_shipcust.plantcode) upd
on fmm.tmp004_identity_id = upd.tmp004_identity_id
when matched then update set fmm.dim_dateiddlvrdoccreated_customer = upd.dim_dateiddlvrdoccreated_customer;

merge into tmp004_insplot_salesorderdelivery002 fmm using
(select distinct tmp004_identity_id, 
                  danew_agicust.dim_dateid as dim_dateidactualgoodsissue_customer
				  from tmp004_insplot_salesorderdelivery002 fmm 
                      inner join dim_plant pl_agicust on fmm.dim_plantid_comops = pl_agicust.dim_plantid  
                      inner join dim_date da_agicust on fmm.dim_dateidactualgoodsissue_customer = da_agicust.dim_dateid
                      inner join dim_date danew_agicust on  da_agicust.datevalue = danew_agicust.datevalue
                        and danew_agicust.companycode = pl_agicust.companycode
                        and danew_agicust.plantcode_factory = pl_agicust.plantcode) upd
on fmm.tmp004_identity_id = upd.tmp004_identity_id
when matched then update set fmm.dim_dateidactualgoodsissue_customer = upd.dim_dateidactualgoodsissue_customer;

/*next script: fact_inserts.sql*/