/* ################################################################################################################## */
/* */
/*   Author         : Octavian */
/*   Created On     : 04 Mar 2016 */
/*  */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   18 May 2016	  Octavian  2.0           Exasol version													  */
/* 	 17 Mar 2016 	  Octavian  1.3           Create history tables for staging tables since 2005 - MerckAH 	  */
/* 	 17 Mar 2016 	  Octavian  1.2           Add rows that don't pass the inner join criteria					  */
/* 	 10 Mar 2016	  Octavian  1.1           Add QALS data in 													  */
/*   04 Mar 2016      Octavian	1.0  		  First draft 			 											  */
/******************************************************************************************************************/

/* Alin: Removed _stghist table and set static filters to 20050101 */
/*
delete from qasr_stghist s
where exists (select 1 from qasr q
where s.qasr_prueflos = q.qasr_prueflos
and s.qasr_vorglfnr = q.qasr_vorglfnr
and s.qasr_merknr = q.qasr_merknr
and s.qasr_probenr = q.qasr_probenr)

insert into qasr_stghist
select * from qasr q
where not exists (select 1 from qasr_stghist s
where s.qasr_prueflos = q.qasr_prueflos
and s.qasr_vorglfnr = q.qasr_vorglfnr
and s.qasr_merknr = q.qasr_merknr
and s.qasr_probenr = q.qasr_probenr)

delete from qapp_stghist s
where exists (select 1 from qapp q
where s.qapp_prueflos = q.qapp_prueflos
and s.qapp_vorglfnr = q.qapp_vorglfnr
and s.qapp_probenr = q.qapp_probenr)

insert into qapp_stghist
select * from qapp q
where not exists (select 1 from qapp_stghist s
where s.qapp_prueflos = q.qapp_prueflos
and s.qapp_vorglfnr = q.qapp_vorglfnr
and s.qapp_probenr = q.qapp_probenr)

delete from qamv_stghist s
where exists (select 1 from qamv q
where s.qamv_prueflos = q.qamv_prueflos
and s.qamv_vorglfnr = q.qamv_vorglfnr
and s.qamv_merknr = q.qamv_merknr)

insert into qamv_stghist
select * from qamv q
where not exists (select 1 from qamv_stghist s
where s.qamv_prueflos = q.qamv_prueflos
and s.qamv_vorglfnr = q.qamv_vorglfnr
and s.qamv_merknr = q.qamv_merknr)

delete from qamr_stghist s
where exists (select 1 from qamr q
where s.qamr_prueflos = q.qamr_prueflos
and s.qamr_vorglfnr = q.qamr_vorglfnr
and s.qamr_merknr = q.qamr_merknr)

insert into qamr_stghist
select * from qamr q
where not exists (select 1 from qamr_stghist s
where s.qamr_prueflos = q.qamr_prueflos
and s.qamr_vorglfnr = q.qamr_vorglfnr
and s.qamr_merknr = q.qamr_merknr)

delete from qals_stghist s
where exists (select 1 from qals q
where s.qals_prueflos = q.qals_prueflos)

insert into qals_stghist
select * from qals q
where not exists (select 1 from qals_stghist s
where s.qals_prueflos = q.qals_prueflos)
    
delete from QASE_stghist s
where exists (select 1 from qase q
where s.qase_prueflos = q.qase_prueflos
and s.qase_vorglfnr = q.qase_vorglfnr
and s.qase_merknr = q.qase_merknr
and s.qase_detailerg = q.qase_detailerg)

insert into QASE_stghist
select * from QASE q
where not exists (select 1 from qase_stghist s
where s.qase_prueflos = q.qase_prueflos
and s.qase_vorglfnr = q.qase_vorglfnr
and s.qase_merknr = q.qase_merknr
and s.qase_detailerg = q.qase_detailerg)

delete from QASV_stghist s
where exists (select 1 from qasv q
where s.qasv_prueflos = q.qasv_prueflos
and s.qasv_vorglfnr = q.qasv_vorglfnr
and s.qasv_merknr = q.qasv_merknr
and s.qasv_probenr = q.qasv_probenr)

insert into QASV_stghist
select * from QASV q
where not exists (select 1 from qasv_stghist s
where s.qasv_prueflos = q.qasv_prueflos
and s.qasv_vorglfnr = q.qasv_vorglfnr
and s.qasv_merknr = q.qasv_merknr
and s.qasv_probenr = q.qasv_probenr)
*/

delete from number_fountain m where m.table_name = 'fact_resultsrecording';
insert into number_fountain
select 'fact_resultsrecording',
ifnull(max(f.fact_resultsrecordingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_resultsrecording f;

insert into fact_resultsrecording(
fact_resultsrecordingid,
dd_inspectionlotno,
dd_currentnodeno,
dd_inspectioncharno,
dd_samplenumber,
dd_indivresultsno,
ct_nconfirming_sampleunits,
ct_defectsno,
ct_inspected_sampleunits,
dd_attribute_resultsrecord,
dim_inspectioncatalogcodesid_qamr,
dim_inspectioncataloggroupid_qamr,
dd_generalinfo_qamr,
ct_maxvalue_validmeasured_qamr,
dd_inspresultvaluation_qamr,
ct_minvalue_validmeasured_qamr,
ct_mean_validmeasured_qamr,
dd_originvalue_qamr,
dd_shorttext_qamr,
dim_dateendinpsectionid_qamr,
dim_datestartinspectionid_qamr,
dd_originresultsset_qamr,
dd_resultsrecordsets_qamr,
dim_inspcatalogselectedsetsid_qamv,
dd_additionalinfo10,
dd_additionalinfo20,
dd_additionalinfo40,
dim_dateidrecordcreted,
dd_shorttextinspch,
dim_unitofmeasurequantitativedata,
dim_inspectionmethodid,
dd_unplannedsampleno,
dd_plannedresultsorigin,
dim_plantmicid,
dd_specifrecordstatus,
dd_samplesizetobeinspected,
ct_upperspeclimit,
ct_lowertollimit,
dim_inspectioncharacteristicid,
dim_inspectioncatalogcodesid_qamv,
dim_inspectioncataloggroupid_qamv,
dd_codevaluation,
dd_attributresultsrecord,
dim_inspectioncatalogcodesid_qase,
dim_inspectioncataloggroupid_qase,
dd_generalinfo,
dd_inspectionresultvaluation,
ct_msvalueforsampleunit,
dd_originvalue_qase,
dd_shorttext_qase,
dim_dateendinpsectionid_qase,
dim_datestartinspectionid_qase,
dd_generalinfo_qasr,
dd_inspresultvaluation_qasr,
ct_minvalue_validmeasured_qasr,
ct_mean_validmeasured_qasr,
dd_originvalue_qasr,
dd_shorttext_qasr,
dim_dateendinpsectionid_qasr,
dim_datestartinspectionid_qasr,
dd_originresultsset_qasr,
dd_resultsrecordsets_qasr,
dd_physicalsamplenumber,
dim_sampledrawingitemsid,
dim_sampledrawingprocedureid,
dim_partid,
dim_plantid,
dim_batchid,
dd_orderno,
dim_inspectiontypeid,
dim_dateidrecordchanged,
dim_dateidrecordcreated,
dim_documenttypetextid,
dim_itemcategoryid,
dim_purchaseorgid,
dd_MaterialDocNo,
dd_MaterialDocYear,
dd_documentitemno,
dd_documentno,
Dim_AccountCategoryid,
dim_costcenterid,
dim_dateidinspectionend,
dim_dateidinspectionstart,
dim_inspectionlotmiscid,
dim_inspectionlotoriginid,
dim_storagelocationid,
dim_vendorid,
dim_warehousenumberid,
dim_inspectionseverityid,
ct_actualinspectedqty,
ct_actuallotqty,
ct_blockedqty,
ct_inspectionlotqty,
ct_postedqty,
ct_qtydefective,
ct_qtyreturned,
ct_reserveqty,
ct_sampleqty,
ct_samplesizeqty,
ct_scrapqty,
ct_unrestrictedqty)

select
(select max_id   from number_fountain   where table_name = 'fact_resultsrecording') + row_number() over(order by '') AS fact_resultsrecordingid,
ifnull(QAMV_PRUEFLOS,0) AS dd_inspectionlotno,
ifnull(QAMV_VORGLFNR,0) AS dd_currentnodeno,
ifnull(QAMV_MERKNR,0) AS dd_inspectioncharno,
ifnull(QASV_PROBENR,0) AS dd_samplenumber,
ifnull(QASE_DETAILERG,0) AS dd_indivresultsno,
ifnull(QAMR_ANZFEHLEH,0) AS ct_nconfirming_sampleunits,
ifnull(QAMR_ANZFEHLER,0) AS ct_defectsno,
ifnull(QAMR_ANZWERTG,0) AS ct_inspected_sampleunits,
ifnull(QAMR_ATTRIBUT,'Not Set') AS dd_attribute_resultsrecord, /* V1 */
/* ifnull((select icc.dim_inspectioncatalogcodesid from dim_inspectioncatalogcodes icc  where
icc.catalog = ifnull(QAMR_KATALGART1,'Not Set') AND
icc.codegroup = ifnull(QAMR_GRUPPE1,'Not Set') AND
icc.code = ifnull(QAMR_CODE1,'Not Set') AND
icc.versionnumber = ifnull(QAMR_VERSION1,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectioncatalogcodesid_qamr,
/* ifnull((select icg.dim_inspectioncataloggroupid from dim_inspectioncataloggroup icg where
icg.catalog = ifnull(QAMR_KATALGART1,'Not Set') AND
icg.codegroup = ifnull(QAMR_GRUPPE1,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectioncataloggroupid_qamr,
ifnull(QAMR_MASCHINE,'Not Set') AS dd_generalinfo_qamr, /* V18 */
ifnull(QAMR_MAXWERT,0) AS ct_maxvalue_validmeasured_qamr,
ifnull(QAMR_MBEWERTG,'Not Set') AS dd_inspresultvaluation_qamr, /* V1 */
ifnull(QAMR_MINWERT,0) AS ct_minvalue_validmeasured_qamr,
ifnull(QAMR_MITTELWERT,0) AS ct_mean_validmeasured_qamr,
ifnull(QAMR_ORIGINAL_INPUT,'Not Set') AS dd_originvalue_qamr, /* V25 */
ifnull(QAMR_PRUEFBEMKT,'Not Set') AS dd_shorttext_qamr, /* V40 */
/* ifnull((select dt.dim_dateid from dim_date dt, dim_plant p where
dt.datevalue = ifnull(QAMR_PRUEFDATUB,'0001-01-01') AND
dt.companycode = p.companycode and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_dateendinpsectionid_qamr,
/* ifnull((select dt.dim_dateid from dim_date dt, dim_plant p where
dt.datevalue = ifnull(QAMR_PRUEFDATUV,'0001-01-01') AND
dt.companycode = p.companycode and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_datestartinspectionid_qamr,
ifnull(QAMR_QERGDATH,'Not Set') AS dd_originresultsset_qamr, /* V2 */
ifnull(QAMR_SATZSTATUS,'Not Set') AS dd_resultsrecordsets_qamr, /* V1 */
/* ifnull((select iss.dim_inspcatalogselectedsetsid from dim_inspcatalogselectedsets iss where
iss.plant = ifnull(QAMV_AUSWMGWRK1,'Not Set') AND
iss.catalog = ifnull(QAMV_KATALGART1,'Not Set') AND
iss.selectedstes = ifnull(QAMV_AUSWMENGE1,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspcatalogselectedsetsid_qamv,
ifnull(QAMV_DUMMY10,'Not Set') AS dd_additionalinfo10, /* V10 */
ifnull(QAMV_DUMMY20,'Not Set') AS dd_additionalinfo20, /* V10 */
ifnull(QAMV_DUMMY40,'Not Set') AS dd_additionalinfo40, /* V10 */
/* ifnull((select dt.dim_dateid from dim_date dt, dim_plant p where
dt.datevalue = ifnull(QAMV_ERSTELLDAT,'0001-01-01') AND
dt.companycode = p.companycode and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_dateidrecordcreted,
ifnull(QAMV_KURZTEXT,'Not Set') AS dd_shorttextinspch, /* V40 */
/* ifnull((select uom.dim_unitofmeasureid from dim_unitofmeasure uom
where uom.uom = ifnull(QAMV_MASSEINHSW,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_unitofmeasurequantitativedata,
/* ifnull((select im.dim_inspectionmethodid from dim_inspectionmethod im where
im.PlantCode = ifnull(QAMV_QMTB_WERKS,'Not Set') AND
im.insprectionmethod = ifnull(QAMV_PMETHODE,'Not Set') AND
im.VersionNumber = ifnull(QAMV_PMTVERSION,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectionmethodid,
ifnull(QAMV_PPKTUNGEMK,0) AS dd_unplannedsampleno,
ifnull(QAMV_QERGDATH,'Not Set') AS dd_plannedresultsorigin, /* V2 */
/* ifnull((select dim_plantid from dim_plant where
plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_plantmicid ,
ifnull(QAMV_SATZSTATUS,'Not Set') AS dd_specifrecordstatus, /* V1 */
ifnull(QAMV_SOLLSTPUMF,0) AS dd_samplesizetobeinspected,
ifnull(QAMV_TOLERANZOB,0) AS ct_upperspeclimit,
ifnull(QAMV_TOLERANZUN,0) AS ct_lowertollimit,
/* ifnull((select ic.dim_inspectioncharacteristicid from dim_inspectioncharacteristic ic where
ic.InspectionPlant = ifnull(QAMV_QPMK_WERKS,'Not Set') AND
ic.Masterinspcharacteristics = ifnull(QAMV_VERWMERKM,'Not Set') AND
ic.VersionNumber = ifnull(QAMV_MKVERSION,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectioncharacteristicid /* QPMK */,
/* ifnull((select icc.dim_inspectioncatalogcodesid from dim_inspectioncatalogcodes icc where
icc.catalog = ifnull(QAMV_KATALGART1,'Not Set') AND
icc.codegroup = ifnull(QAMV_CODEGRQUAL,'Not Set') AND
icc.code = ifnull(QAMV_CODEQUAL,'Not Set') AND
icc.versionnumber = ifnull(QAMV_VERSION,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectioncatalogcodesid_qamv,
/* ifnull((select icg.dim_inspectioncataloggroupid from dim_inspectioncataloggroup icg where
icg.catalog = ifnull(QAMV_KATALGART1,'Not Set') AND
icg.codegroup = ifnull(QAMV_CODEGRQUAL,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectioncataloggroupid_qamv,
ifnull(QAPP_VBEWERTUNG,'Not Set') AS dd_codevaluation, /* V1 */
ifnull(QASE_ATTRIBUT,'Not Set') AS dd_attributresultsrecord, /* V1 */
/* ifnull((select icc.dim_inspectioncatalogcodesid from dim_inspectioncatalogcodes icc where
icc.catalog = ifnull(QASE_KATALGART1,'Not Set') AND
icc.codegroup = ifnull(QASE_GRUPPE1,'Not Set') AND
icc.code = ifnull(QASE_CODE1,'Not Set') AND
icc.versionnumber = ifnull(QASE_VERSION1,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectioncatalogcodesid_qase,
/* ifnull((select icg.dim_inspectioncataloggroupid from dim_inspectioncataloggroup icg where
icg.catalog = ifnull(QASE_KATALGART1,'Not Set') AND
icg.codegroup = ifnull(QASE_GRUPPE1,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectioncataloggroupid_qase,
ifnull(QASE_MASCHINE,'Not Set') AS dd_generalinfo, /* V18 */
ifnull(QASE_MBEWERTG,'Not Set') AS dd_inspectionresultvaluation, /* V1 */
ifnull(QASE_MESSWERT,0) AS ct_msvalueforsampleunit,
ifnull(QASE_ORIGINAL_INPUT,'Not Set') AS dd_originvalue_qase,
ifnull(QASE_PRUEFBEMKT,'Not Set') AS dd_shorttext_qase,
/* ifnull((select dt.dim_dateid from dim_date dt, dim_plant p where
dt.datevalue = ifnull(QASE_PRUEFDATUB,'0001-01-01') AND
dt.companycode = p.companycode and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_dateendinpsectionid_qase,
/* ifnull((select dt.dim_dateid from dim_date dt, dim_plant p where
dt.datevalue = ifnull(QASE_PRUEFDATUV,'0001-01-01') AND
dt.companycode = p.companycode and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_datestartinspectionid_qase,
ifnull(QASR_MASCHINE,'Not Set') AS dd_generalinfo_qasr, /* V18 */
ifnull(QASR_MBEWERTG,'Not Set') AS dd_inspresultvaluation_qasr, /* V1 */
ifnull(QASR_MINWERT,0) AS ct_minvalue_validmeasured_qasr,
ifnull(QASR_MITTELWERT,0) AS ct_mean_validmeasured_qasr,
ifnull(QASR_ORIGINAL_INPUT,'Not Set') AS dd_originvalue_qasr, /* V25 */
ifnull(QASR_PRUEFBEMKT,'Not Set') AS dd_shorttext_qasr, /* V40 */
/* ifnull((select dt.dim_dateid from dim_date dt, dim_plant p where
dt.datevalue = ifnull(QASR_PRUEFDATUB,'0001-01-01') AND
dt.companycode = p.companycode and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_dateendinpsectionid_qasr,
/* ifnull((select dt.dim_dateid from dim_date dt, dim_plant p where
dt.datevalue = ifnull(QASR_PRUEFDATUV,'0001-01-01') AND
dt.companycode = p.companycode and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_datestartinspectionid_qasr,
ifnull(QASR_QERGDATH,'Not Set') AS dd_originresultsset_qasr, /* V2 */
ifnull(QASR_SATZSTATUS,'Not Set') AS dd_resultsrecordsets_qasr, /* V1 */
ifnull(QAPP_PHYNR,'Not Set') AS dd_physicalsamplenumber, /* QPRS */
/* ifnull((select dim_sampledrawingitemsid from dim_sampledrawingitems
where sampleprocedure = ifnull(QAMV_PRZIEHVERF,'Not Set') AND
versionno = ifnull(QAMV_VERSION,'Not Set') AND
itemno = ifnull(QAMV_POSNRPRZV,0)),1) */ CONVERT(BIGINT,1) AS dim_sampledrawingitemsid,
/* ifnull((select dim_sampledrawingprocedureid from dim_sampledrawingprocedure
where sampleprocedure = ifnull(QAMV_PRZIEHVERF,'Not Set') AND
versionno = ifnull(QAMV_VERSION,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_sampledrawingprocedureid,
/* ifnull((select dim_partid from dim_part dp
where dp.partnumber = ifnull(QALS_MATNR, 'Not Set') AND
dp.plant = ifnull(QALS_WERK,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_partid,
/* ifnull((select dim_plantid from dim_plant dp
where dp.plantcode = ifnull(QALS_WERK,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_plantid,
/* ifnull((select dim_batchid from dim_batch b
where b.partnumber = ifnull(QALS_MATNR, 'Not Set') AND
b.plantcode = ifnull(QALS_WERK,'Not Set')
and b.batchnumber = ifnull(QALS_CHARG, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_batchid,
ifnull(q.QALS_AUFNR, 'Not Set') as dd_orderno,
/* ifnull((SELECT dim_inspectiontypeid
FROM dim_inspectiontype ityp
WHERE ityp.InspectionTypeCode = ifnull(QALS_ART, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectiontypeid,
/* ifnull((select dim_dateid
FROM dim_date rc, dim_plant dp
WHERE rc.DateValue = ifnull(QALS_AENDERDAT,'0001-01-01')
AND rc.companycode = dp.CompanyCode
AND dp.plantcode = q.QALS_WERK),1) */ 1 dim_dateidrecordchanged,
/* ifnull((select dim_dateid
FROM dim_date rcr, dim_plant dp
WHERE rcr.DateValue = ifnull(QALS_ERSTELDAT,'0001-01-01')
AND rcr.companycode = dp.CompanyCode
AND dp.plantcode = q.QALS_WERK),1) */ CONVERT(BIGINT,1) AS dim_dateidrecordcreated,
/* ifnull((SELECT dim_documenttypetextid
FROM dim_documenttypetext dtt
WHERE dtt.type = ifnull(QALS_BLART, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_documenttypetextid,
/* ifnull((SELECT dim_itemcategoryid
FROM dim_itemcategory icc
WHERE icc.CategoryCode = ifnull(QALS_PSTYP, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_itemcategoryid,
/* ifnull((SELECT dim_purchaseorgid
FROM dim_purchaseorg po WHERE po.PurchaseOrgCode = ifnull(QALS_EKORG, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_purchaseorgid,
ifnull(QALS_MBLNR, 'Not Set') AS dd_MaterialDocNo,
ifnull(QALS_MJAHR,0) AS dd_MaterialDocYear,
ifnull(QALS_EBELP,0) AS dd_documentitemno,
ifnull(QALS_EBELN, 'Not Set') AS dd_documentno,
/* ifnull((SELECT dim_accountcategoryid FROM dim_accountcategory ac
WHERE ac.Category = ifnull(QALS_KNTTP, 'Not Set')),1) */ CONVERT(BIGINT,1) AS Dim_AccountCategoryid,
/* ifnull((SELECT dim_costcenterid FROM dim_costcenter cc
WHERE cc.Code = ifnull(QALS_KOSTL, 'Not Set')
AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_costcenterid,
/* ifnull((SELECT dim_dateid FROM dim_date ie, dim_plant dp
WHERE ie.DateValue = ifnull(QALS_PAENDTERM,'0001-01-01')
AND ie.CompanyCode = dp.CompanyCode
AND dp.plantcode = q.QALS_WERK), 1) */ CONVERT(BIGINT,1) AS dim_dateidinspectionend,
/* ifnull((SELECT dim_dateid FROM dim_date dis, dim_plant dp
WHERE dis.DateValue = ifnull(QALS_PASTRTERM,'0001-01-01')
AND dis.CompanyCode = dp.CompanyCode
AND dp.plantcode = q.QALS_WERK), 1) */ CONVERT(BIGINT,1) AS dim_dateidinspectionstart,
/* ifnull((SELECT dim_inspectionlotmiscid
FROM dim_inspectionlotmisc
WHERE AutomaticInspectionLot = ifnull(QALS_STAT01, 'Not Set')
AND BatchManagementRequired = ifnull(QALS_XCHPF, 'Not Set')
AND CompleteInspection = ifnull(QALS_HPZ, 'Not Set')
AND DocumentationRequired = ifnull(QALS_STAT19, 'Not Set')
AND InspectionPlanRequired = ifnull(QALS_STAT20, 'Not Set')
AND InspectionStockQuantity = ifnull(QALS_INSMK, 'Not Set')
AND ShotTermInspectionComplete = ifnull(QALS_STAT14, 'Not Set')
AND StockPostingsComplete = ifnull(QALS_STAT34, 'Not Set')
AND UsageDecisionMade = ifnull(QALS_STAT35, 'Not Set')
AND LotSkipped = ifnull(QALS_KZSKIPLOT, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectionlotmiscid,
/* ifnull((SELECT dim_inspectionlotoriginid FROM dim_inspectionlotorigin ilo
WHERE ilo.InspectionLotOriginCode = ifnull(QALS_HERKUNFT, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_inspectionlotoriginid,
/* ifnull((SELECT dim_storagelocationid FROM dim_storagelocation isl, dim_plant dp
WHERE isl.LocationCode = ifnull(QALS_LAGORTVORG,'Not Set') AND isl.Plant = dp.PlantCode), 1) */ CONVERT(BIGINT,1) AS dim_storagelocationid,
/* ifnull((SELECT dim_vendorid FROM dim_vendor dv1
WHERE dv1.VendorNumber = ifnull(QALS_LIFNR, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_vendorid,
/* ifnull((SELECT wn.dim_warehousenumberid FROM dim_warehousenumber wn
WHERE wn.WarehouseCode = ifnull(QALS_LGNUM, 'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_warehousenumberid,
/* ifnull((SELECT dim_inspectionseverityid FROM dim_inspectionseverity isvr
WHERE isvr.InspectionSeverityCode = ifnull(QALS_PRSCHAERFE,0)),1) */ CONVERT(BIGINT,1) AS dim_inspectionseverityid,
ifnull(QALS_LMENGEPR,0) ct_actualinspectedqty,
ifnull(QALS_LMENGEIST,0) ct_actuallotqty,
ifnull(QALS_LMENGE04,0) ct_blockedqty,
ifnull(QALS_LOSMENGE,0) ct_inspectionlotqty,
ifnull(QALS_LMENGEZUB,0) ct_postedqty,
ifnull(QALS_LMENGESCH,0) ct_qtydefective,
ifnull(QALS_LMENGE07,0) ct_qtyreturned,
ifnull(QALS_LMENGE05,0) ct_reserveqty,
ifnull(QALS_LMENGE03,0) ct_sampleqty,
ifnull(QALS_GESSTICHPR,0) ct_samplesizeqty,
ifnull(QALS_LMENGE02,0) ct_scrapqty,
ifnull(QALS_LMENGE01,0) ct_unrestrictedqty


from QAMR mr
inner join QAMV mv
ON mr.QAMR_PRUEFLOS = mv.QAMV_PRUEFLOS
and mr.QAMR_VORGLFNR = mv.QAMV_VORGLFNR
and mr.QAMR_MERKNR = mv.QAMV_MERKNR
inner join QASE se
on mr.QAMR_PRUEFLOS = se.QASE_PRUEFLOS
and mr.QAMR_VORGLFNR = se.QASE_VORGLFNR
and mr.QAMR_MERKNR = se.QASE_MERKNR
inner join QASR sr
on mr.QAMR_PRUEFLOS = sr.QASR_PRUEFLOS
and mr.QAMR_VORGLFNR = sr.QASR_VORGLFNR
and mr.QAMR_MERKNR = sr.QASR_MERKNR
inner join QAPP pp
on mr.QAMR_PRUEFLOS = pp.QAPP_PRUEFLOS
and mr.QAMR_VORGLFNR = pp.QAPP_VORGLFNR
and sr.QASR_PROBENR = pp.QAPP_PROBENR
inner join QASV sv
on sr.QASR_PRUEFLOS = sv.QASV_PRUEFLOS
and sr.QASR_VORGLFNR = sv.QASV_VORGLFNR
and sr.QASR_PROBENR = sv.QASV_PROBENR
and sr.QASR_MERKNR = sv.QASV_MERKNR
inner join QALS q
ON mr.QAMR_PRUEFLOS = q.QALS_PRUEFLOS

where not exists(
select 1 from fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(QAMV_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(QASV_PROBENR,0)
and f_rr.dd_indivresultsno = ifnull(QASE_DETAILERG,0));


/* Octavian: Insert rows that are not available in all tables - above */

delete from number_fountain m where m.table_name = 'fact_resultsrecording';
insert into number_fountain
select 'fact_resultsrecording',
ifnull(max(f.fact_resultsrecordingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_resultsrecording f;

insert into fact_resultsrecording
(fact_resultsrecordingid,dd_inspectionlotno,dd_currentnodeno,dd_inspectioncharno,dd_indivresultsno, sourcetable)
select
(select max_id   from number_fountain   where table_name = 'fact_resultsrecording') + row_number() over(order by '') AS fact_resultsrecordingid,
QASE_PRUEFLOS,QASE_VORGLFNR,QASE_MERKNR,QASE_DETAILERG,'QASE' from QASE q
where not exists (select 1 from fact_resultsrecording f_rr where
ifnull(QASE_PRUEFLOS,'0') = f_rr.dd_inspectionlotno AND
ifnull(QASE_VORGLFNR,0) = f_rr.dd_currentnodeno AND
ifnull(QASE_MERKNR,0) = f_rr.dd_inspectioncharno AND
ifnull(QASE_DETAILERG,0) = f_rr.dd_indivresultsno);

delete from number_fountain m where m.table_name = 'fact_resultsrecording';
insert into number_fountain
select 'fact_resultsrecording',
ifnull(max(f.fact_resultsrecordingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_resultsrecording f;

insert into fact_resultsrecording
(fact_resultsrecordingid,dd_inspectionlotno,dd_currentnodeno,dd_inspectioncharno,dd_samplenumber, sourcetable)
select
(select max_id from number_fountain   where table_name = 'fact_resultsrecording') + row_number() over(order by '') AS fact_resultsrecordingid,
QASV_PRUEFLOS,QASV_VORGLFNR,QASV_MERKNR,QASV_PROBENR,'QASV' from QASV q
where not exists (select 1 from fact_resultsrecording f_rr where
ifnull(QASV_PRUEFLOS,'0') = f_rr.dd_inspectionlotno AND
ifnull(QASV_VORGLFNR,0) = f_rr.dd_currentnodeno AND
ifnull(QASV_MERKNR,0) = f_rr.dd_inspectioncharno AND
ifnull(QASV_PROBENR,0) = f_rr.dd_samplenumber);

delete from number_fountain m where m.table_name = 'fact_resultsrecording';
insert into number_fountain
select 'fact_resultsrecording',
ifnull(max(f.fact_resultsrecordingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_resultsrecording f;

insert into fact_resultsrecording
(fact_resultsrecordingid,dd_inspectionlotno,sourcetable)
select
(select max_id from number_fountain   where table_name = 'fact_resultsrecording') + row_number() over(order by '') AS fact_resultsrecordingid,
QALS_PRUEFLOS,'QALS' from QALS q
where not exists (select 1 from fact_resultsrecording f_rr where
ifnull(QALS_PRUEFLOS,'0') = f_rr.dd_inspectionlotno);

delete from number_fountain m where m.table_name = 'fact_resultsrecording';
insert into number_fountain
select 'fact_resultsrecording',
ifnull(max(f.fact_resultsrecordingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_resultsrecording f;

insert into fact_resultsrecording
(fact_resultsrecordingid,dd_inspectionlotno,dd_currentnodeno,dd_inspectioncharno, sourcetable)
select
(select max_id from number_fountain   where table_name = 'fact_resultsrecording') + row_number() over(order by '') AS fact_resultsrecordingid,
QAMR_PRUEFLOS,QAMR_VORGLFNR,QAMR_MERKNR,'QAMR' from QAMR q
where not exists (select 1 from fact_resultsrecording f_rr where
ifnull(QAMR_PRUEFLOS,'0') = f_rr.dd_inspectionlotno AND
ifnull(QAMR_VORGLFNR,0) = f_rr.dd_currentnodeno AND
ifnull(QAMR_MERKNR,0) = f_rr.dd_inspectioncharno);

delete from number_fountain m where m.table_name = 'fact_resultsrecording';
insert into number_fountain
select 'fact_resultsrecording',
ifnull(max(f.fact_resultsrecordingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_resultsrecording f;

insert into fact_resultsrecording
(fact_resultsrecordingid,dd_inspectionlotno,dd_currentnodeno,dd_inspectioncharno, sourcetable)
select
(select max_id from number_fountain   where table_name = 'fact_resultsrecording') + row_number() over(order by '') AS fact_resultsrecordingid,
QAMV_PRUEFLOS,QAMV_VORGLFNR,QAMV_MERKNR,'QAMV' from QAMV q
where not exists (select 1 from fact_resultsrecording f_rr where
ifnull(QAMV_PRUEFLOS,'0') = f_rr.dd_inspectionlotno AND
ifnull(QAMV_VORGLFNR,0) = f_rr.dd_currentnodeno AND
ifnull(QAMV_MERKNR,0) = f_rr.dd_inspectioncharno);

delete from number_fountain m where m.table_name = 'fact_resultsrecording';
insert into number_fountain
select 'fact_resultsrecording',
ifnull(max(f.fact_resultsrecordingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_resultsrecording f;

insert into fact_resultsrecording
(fact_resultsrecordingid,dd_inspectionlotno,dd_currentnodeno,dd_inspectioncharno,dd_samplenumber, sourcetable)
select
(select max_id from number_fountain   where table_name = 'fact_resultsrecording') + row_number() over(order by '') AS fact_resultsrecordingid,
QASR_PRUEFLOS,QASR_VORGLFNR,QASR_MERKNR,QASR_PROBENR,'QASR' from QASR q
where not exists (select 1 from fact_resultsrecording f_rr where
ifnull(QASR_PRUEFLOS,'0') = f_rr.dd_inspectionlotno AND
ifnull(QASR_VORGLFNR,0) = f_rr.dd_currentnodeno AND
ifnull(QASR_MERKNR,0) = f_rr.dd_inspectioncharno AND
ifnull(QASR_PROBENR,0) = f_rr.dd_samplenumber);

delete from number_fountain m where m.table_name = 'fact_resultsrecording';
insert into number_fountain
select 'fact_resultsrecording',
ifnull(max(f.fact_resultsrecordingid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_resultsrecording f;

insert into fact_resultsrecording
(fact_resultsrecordingid,dd_inspectionlotno,dd_currentnodeno,dd_samplenumber, sourcetable)
select
(select max_id from number_fountain   where table_name = 'fact_resultsrecording') + row_number() over(order by '') AS fact_resultsrecordingid,
QAPP_PRUEFLOS,QAPP_VORGLFNR,QAPP_PROBENR,'QAPP' from QAPP q
where not exists (select 1 from fact_resultsrecording f_rr where
ifnull(QAPP_PRUEFLOS,'0') = f_rr.dd_inspectionlotno AND
ifnull(QAPP_VORGLFNR,0) = f_rr.dd_currentnodeno AND
ifnull(QAPP_PROBENR,0) = f_rr.dd_samplenumber);

/* Octavian: Insert rows that are not available in all tables - above */



/* Start QPRS updates dependent on QAPP-PHYNR */
update fact_resultsrecording f_rr
set f_rr.dd_physicalsamplenumber = ifnull(q.QAPP_PHYNR,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAPP q,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(q.QAPP_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(q.QAPP_VORGLFNR,0)
and f_rr.dd_samplenumber = ifnull(q.QAPP_PROBENR,0)
and f_rr.dd_physicalsamplenumber <> ifnull(q.QAPP_PHYNR,'Not Set');

update fact_resultsrecording f_rr
set f_rr.dd_objectnumber = ifnull(q.QPRS_OBJNR,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QPRS_QAPP q,fact_resultsrecording f_rr
where f_rr.dd_physicalsamplenumber = ifnull(q.QPRS_PHYNR,'Not Set')
and f_rr.dd_objectnumber <> ifnull(q.QPRS_OBJNR,'Not Set');

update fact_resultsrecording f_rr
set f_rr.dd_samplecategory = ifnull(q.QPRS_PRTYP,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QPRS_QAPP q,fact_resultsrecording f_rr
where f_rr.dd_physicalsamplenumber = ifnull(q.QPRS_PHYNR,'Not Set')
and f_rr.dd_samplecategory <> ifnull(q.QPRS_PRTYP,'Not Set');
/* End QPRS updates dependent on QAPP-PHYNR */




/* Start QAMR updates */
merge into fact_resultsrecording f_rr using
	(select ifnull(mr.QAMR_PRUEFLOS,0) QAMR_PRUEFLOS,
			ifnull(mr.QAMR_VORGLFNR,0) QAMR_VORGLFNR,
			ifnull(mr.QAMR_MERKNR,0) QAMR_MERKNR,
			mr.QAMR_ANZFEHLEH,
			mr.QAMR_ANZFEHLER,
			mr.QAMR_ANZWERTG,
			mr.QAMR_ATTRIBUT,
			mr.QAMR_MASCHINE,
			mr.QAMR_MAXWERT,
			mr.QAMR_MBEWERTG,
			mr.QAMR_MINWERT,
			mr.QAMR_MITTELWERT,
			mr.QAMR_ORIGINAL_INPUT,
			mr.QAMR_PRUEFBEMKT,
			mr.QAMR_QERGDATH,
			mr.QAMR_SATZSTATUS,
			mr.QAMR_PRUEFER
	from QAMR mr) as mr
on f_rr.dd_inspectionlotno = QAMR_PRUEFLOS
	and f_rr.dd_currentnodeno = QAMR_VORGLFNR
	and f_rr.dd_inspectioncharno = QAMR_MERKNR
when matched then update 
set
	f_rr.ct_nconfirming_sampleunits = ifnull(mr.QAMR_ANZFEHLEH,0),
	f_rr.ct_defectsno = ifnull(mr.QAMR_ANZFEHLER,0),
	f_rr.ct_inspected_sampleunits = ifnull(mr.QAMR_ANZWERTG,0),
	f_rr.dd_attribute_resultsrecord = ifnull(mr.QAMR_ATTRIBUT,'Not Set'),
	f_rr.dd_generalinfo_qamr = ifnull(mr.QAMR_MASCHINE,'Not Set'),
	f_rr.ct_maxvalue_validmeasured_qamr = ifnull(mr.QAMR_MAXWERT,0),
	f_rr.dd_inspresultvaluation_qamr = ifnull(mr.QAMR_MBEWERTG,'Not Set'),
	f_rr.ct_minvalue_validmeasured_qamr = ifnull(mr.QAMR_MINWERT,0),
	f_rr.ct_mean_validmeasured_qamr = ifnull(mr.QAMR_MITTELWERT,0),
	f_rr.dd_originvalue_qamr = ifnull(mr.QAMR_ORIGINAL_INPUT,'Not Set'),
	f_rr.dd_shorttext_qamr = ifnull(mr.QAMR_PRUEFBEMKT,'Not Set'),
	f_rr.dd_originresultsset_qamr = ifnull(mr.QAMR_QERGDATH,'Not Set'),
	f_rr.dd_resultsrecordsets_qamr = ifnull(mr.QAMR_SATZSTATUS,'Not Set'),
	f_rr.dd_inspectorname = ifnull(mr.QAMR_PRUEFER,'Not Set'),
	f_rr.dw_update_date = CURRENT_TIMESTAMP;

/*
update fact_resultsrecording f_rr
set ct_nconfirming_sampleunits = ifnull(QAMR_ANZFEHLEH,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and ct_nconfirming_sampleunits <> ifnull(QAMR_ANZFEHLEH,0)

update fact_resultsrecording f_rr
set ct_defectsno = ifnull(QAMR_ANZFEHLER,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and ct_defectsno <> ifnull(QAMR_ANZFEHLER,0)

update fact_resultsrecording f_rr
set ct_inspected_sampleunits = ifnull(QAMR_ANZWERTG,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and ct_inspected_sampleunits <> ifnull(QAMR_ANZWERTG,0)

update fact_resultsrecording f_rr
set dd_attribute_resultsrecord = ifnull(QAMR_ATTRIBUT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and dd_attribute_resultsrecord <> ifnull(QAMR_ATTRIBUT,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_generalinfo_qamr = ifnull(mr.QAMR_MASCHINE,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.dd_generalinfo_qamr <> ifnull(mr.QAMR_MASCHINE,'Not Set')

update fact_resultsrecording f_rr
set f_rr.ct_maxvalue_validmeasured_qamr = ifnull(QAMR_MAXWERT,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.ct_maxvalue_validmeasured_qamr <> ifnull(QAMR_MAXWERT,0)

update fact_resultsrecording f_rr
set f_rr.dd_inspresultvaluation_qamr = ifnull(QAMR_MBEWERTG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.dd_inspresultvaluation_qamr <> ifnull(QAMR_MBEWERTG,'Not Set')

update fact_resultsrecording f_rr
set f_rr.ct_minvalue_validmeasured_qamr = ifnull(QAMR_MINWERT,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.ct_minvalue_validmeasured_qamr <> ifnull(QAMR_MINWERT,0)

update fact_resultsrecording f_rr
set f_rr.ct_mean_validmeasured_qamr = ifnull(QAMR_MITTELWERT,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.ct_mean_validmeasured_qamr <> ifnull(QAMR_MITTELWERT,0)

update fact_resultsrecording f_rr
set f_rr.dd_originvalue_qamr = ifnull(QAMR_ORIGINAL_INPUT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.dd_originvalue_qamr <> ifnull(QAMR_ORIGINAL_INPUT,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_shorttext_qamr = ifnull(QAMR_PRUEFBEMKT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.dd_shorttext_qamr <> ifnull(QAMR_PRUEFBEMKT,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_originresultsset_qamr = ifnull(mr.QAMR_QERGDATH,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.dd_originresultsset_qamr <> ifnull(mr.QAMR_QERGDATH,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_resultsrecordsets_qamr = ifnull(mr.QAMR_SATZSTATUS,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and f_rr.dd_resultsrecordsets_qamr <> ifnull(mr.QAMR_SATZSTATUS,'Not Set')

update fact_resultsrecording f_rr
set dd_inspectorname = ifnull(QAMR_PRUEFER,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
and dd_inspectorname <> ifnull(QAMR_PRUEFER,'Not Set')
*/


update fact_resultsrecording f_rr
set f_rr.dim_inspectioncatalogcodesid_qamr = icc.dim_inspectioncatalogcodesid,
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr, dim_inspectioncatalogcodes icc,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
/* and icc.catalog = ifnull(QAMR_KATALGART1,'Not Set') */
and icc.codegroup = ifnull(QAMR_GRUPPE1,'Not Set')
and icc.code = ifnull(QAMR_CODE1,'Not Set')
and icc.versionnumber = ifnull(QAMR_VERSION1,'Not Set')
and f_rr.dim_inspectioncatalogcodesid_qamr <> icc.dim_inspectioncatalogcodesid;

update fact_resultsrecording f_rr
set f_rr.dim_inspectioncataloggroupid_qamr = icg.dim_inspectioncataloggroupid,
dw_update_date = CURRENT_TIMESTAMP
from QAMR mr, dim_inspectioncataloggroup icg,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
/* and icg.catalog = ifnull(QAMR_KATALGART1,'Not Set') */
and icg.codegroup = ifnull(QAMR_GRUPPE1,'Not Set')
and f_rr.dim_inspectioncataloggroupid_qamr <> icg.dim_inspectioncataloggroupid;


/*Georgiana changes according to BI - 3828*/
MERGE INTO fact_resultsrecording fact
USING (SELECT distinct f_rr.fact_resultsrecordingid, dt.dim_dateid
	   FROM QAMR mr, QALS mv, dim_date dt, dim_plant p, fact_resultsrecording f_rr
	   where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
		and f_rr.dd_inspectionlotno = ifnull(mv.QALS_PRUEFLOS,0)
		and dt.datevalue = ifnull(mr.QAMR_PRUEFDATUB,'0001-01-01')
		and dt.companycode = p.companycode
		and dt.plantcode_factory = p.plantcode /* plantcode_factory additional condition- BI-5085 */
		and p.plantcode = ifnull(mv.QALS_WERK,'Not Set')
        and f_rr.dim_dateendinpsectionid_qamr <> dt.dim_dateid) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateendinpsectionid_qamr = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

MERGE INTO fact_resultsrecording fact
USING (SELECT distinct f_rr.fact_resultsrecordingid, dt.dim_dateid
	   FROM QAMR mr, QALS mv, dim_date dt, dim_plant p, fact_resultsrecording f_rr
	   where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
		and f_rr.dd_inspectionlotno = ifnull(mv.QALS_PRUEFLOS,0)
		and dt.datevalue = ifnull(mr.QAMR_PRUEFDATUV,'0001-01-01')
		and dt.companycode = p.companycode
		and dt.plantcode_factory = p.plantcode /* plantcode_factory additional condition- BI-5085 */
		and p.plantcode = ifnull(mv.QALS_WERK,'Not Set')
		and f_rr.dim_datestartinspectionid_qamr <> dt.dim_dateid) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dim_datestartinspectionid_qamr = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;


MERGE INTO fact_resultsrecording fact
USING (SELECT distinct f_rr.fact_resultsrecordingid,
 case when length(qamr_pruefzeitv)=5 then  
concat(substr(concat ('0',qamr_pruefzeitv),-6,2),':',substr(qamr_pruefzeitv,-4,2),':',substr(qamr_pruefzeitv,-2)) 
else concat(substr(qamr_pruefzeitv,-6,2),':',substr(qamr_pruefzeitv,-4,2),':',substr(qamr_pruefzeitv,-2)) end as starttime
	   FROM QAMR mr, fact_resultsrecording f_rr
	   where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
        and f_rr.dd_starttime <> case when length(qamr_pruefzeitv)=5 then  
concat(substr(concat ('0',qamr_pruefzeitv),-6,2),':',substr(qamr_pruefzeitv,-4,2),':',substr(qamr_pruefzeitv,-2)) 
else concat(substr(qamr_pruefzeitv,-6,2),':',substr(qamr_pruefzeitv,-4,2),':',substr(qamr_pruefzeitv,-2)) end
) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dd_starttime = ifnull(src.starttime,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP;


MERGE INTO fact_resultsrecording fact
USING (SELECT distinct f_rr.fact_resultsrecordingid,
 case when length(qamr_pruefzeitb)=5 then 
concat(substr(concat('0',qamr_pruefzeitb),-6,2),':',substr(qamr_pruefzeitb,-4,2),':',substr(qamr_pruefzeitb,-2)) else 
concat(substr(qamr_pruefzeitb,-6,2),':',substr(qamr_pruefzeitb,-4,2),':',substr(qamr_pruefzeitb,-2)) end as endtime
	   FROM QAMR mr, fact_resultsrecording f_rr
	   where f_rr.dd_inspectionlotno = ifnull(mr.QAMR_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(mr.QAMR_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(mr.QAMR_MERKNR,0)
        and f_rr.dd_endtime <>  case when length(qamr_pruefzeitb)=5 then 
concat(substr(concat('0',qamr_pruefzeitb),-6,2),':',substr(qamr_pruefzeitb,-4,2),':',substr(qamr_pruefzeitb,-2)) else 
concat(substr(qamr_pruefzeitb,-6,2),':',substr(qamr_pruefzeitb,-4,2),':',substr(qamr_pruefzeitb,-2)) end) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dd_endtime = ifnull(src.endtime,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP;

/*Georgiana End of BI 3828*/

/* End QAMR updates */



/* Start QAMV updates */
merge into fact_resultsrecording f_rr using
	(select ifnull(mv.QAMV_PRUEFLOS,0) QAMV_PRUEFLOS,
			ifnull(mv.QAMV_VORGLFNR,0) QAMV_VORGLFNR,
			ifnull(mv.QAMV_MERKNR,0) QAMV_MERKNR,
			mv.QAMV_DUMMY10,
			mv.QAMV_DUMMY20,
			mv.QAMV_DUMMY40,
			mv.QAMV_KURZTEXT,
			mv.QAMV_PPKTUNGEMK,
			mv.QAMV_QERGDATH,
			mv.QAMV_SATZSTATUS,
			mv.QAMV_SOLLSTPUMF,
			mv.QAMV_TOLERANZOB,
			mv.QAMV_TOLERANZUN
	from QAMV mv) mv
on f_rr.dd_inspectionlotno = mv.QAMV_PRUEFLOS
	and f_rr.dd_currentnodeno = mv.QAMV_VORGLFNR
	and f_rr.dd_inspectioncharno = mv.QAMV_MERKNR
when matched then update
set
	f_rr.dd_additionalinfo10 = ifnull(mv.QAMV_DUMMY10,'Not Set'),
	f_rr.dd_additionalinfo20 = ifnull(mv.QAMV_DUMMY20,'Not Set'),
	f_rr.dd_additionalinfo40 = ifnull(mv.QAMV_DUMMY40,'Not Set'),
	f_rr.dd_shorttextinspch = ifnull(mv.QAMV_KURZTEXT,'Not Set'),
	f_rr.dd_unplannedsampleno = ifnull(mv.QAMV_PPKTUNGEMK,0),
	f_rr.dd_plannedresultsorigin = ifnull(mv.QAMV_QERGDATH,'Not Set'),
	f_rr.dd_specifrecordstatus = ifnull(mv.QAMV_SATZSTATUS,'Not Set'),
	f_rr.dd_samplesizetobeinspected = ifnull(mv.QAMV_SOLLSTPUMF,0),
	f_rr.ct_upperspeclimit = ifnull(mv.QAMV_TOLERANZOB,0),
	f_rr.ct_lowertollimit = ifnull(mv.QAMV_TOLERANZUN,0),
	f_rr.dw_update_date = CURRENT_TIMESTAMP;


/*
update fact_resultsrecording f_rr
set dd_additionalinfo10 = ifnull(QAMV_DUMMY10,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and dd_additionalinfo10 <> ifnull(QAMV_DUMMY10,'Not Set')

update fact_resultsrecording f_rr
set dd_additionalinfo20 = ifnull(QAMV_DUMMY20,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and dd_additionalinfo20 <> ifnull(QAMV_DUMMY20,'Not Set')

update fact_resultsrecording f_rr
set dd_additionalinfo40 = ifnull(QAMV_DUMMY40,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and dd_additionalinfo40 <> ifnull(QAMV_DUMMY40,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_shorttextinspch = ifnull(QAMV_KURZTEXT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and f_rr.dd_shorttextinspch <> ifnull(QAMV_KURZTEXT,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_unplannedsampleno = ifnull(QAMV_PPKTUNGEMK,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and f_rr.dd_unplannedsampleno <> ifnull(QAMV_PPKTUNGEMK,0)

update fact_resultsrecording f_rr
set f_rr.dd_plannedresultsorigin = ifnull(mv.QAMV_QERGDATH,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and f_rr.dd_plannedresultsorigin <> ifnull(mv.QAMV_QERGDATH,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_specifrecordstatus = ifnull(mv.QAMV_SATZSTATUS,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and f_rr.dd_specifrecordstatus <> ifnull(mv.QAMV_SATZSTATUS,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_samplesizetobeinspected = ifnull(QAMV_SOLLSTPUMF,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and f_rr.dd_samplesizetobeinspected <> ifnull(QAMV_SOLLSTPUMF,0)

update fact_resultsrecording f_rr
set f_rr.ct_upperspeclimit = ifnull(QAMV_TOLERANZOB,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and f_rr.ct_upperspeclimit <> ifnull(QAMV_TOLERANZOB,0)

update fact_resultsrecording f_rr
set f_rr.ct_lowertollimit = ifnull(mv.QAMV_TOLERANZUN,0),
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and f_rr.ct_lowertollimit <> ifnull(mv.QAMV_TOLERANZUN,0)
*/

update fact_resultsrecording f_rr
set f_rr.dim_inspcatalogselectedsetsid_qamv = iss.dim_inspcatalogselectedsetsid,
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv, dim_inspcatalogselectedsets iss,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and iss.plant = ifnull(QAMV_AUSWMGWRK1,'Not Set')
/* and iss.catalog = ifnull(QAMV_KATALGART1,'Not Set') */
and iss.selectedstes = ifnull(QAMV_AUSWMENGE1,'Not Set')
and f_rr.dim_inspcatalogselectedsetsid_qamv <> iss.dim_inspcatalogselectedsetsid;

update fact_resultsrecording f_rr
set dim_dateidrecordcreted = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv, dim_date dt, dim_plant p, fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and dt.datevalue = ifnull(QAMV_ERSTELLDAT,'0001-01-01')
and dt.companycode = p.companycode and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')
and dt.plantcode_factory = p.plantcode /* plantcode_factory additional condition- BI-5085 */
and dim_dateidrecordcreted <> dt.dim_dateid;

update fact_resultsrecording f_rr
set f_rr.dim_unitofmeasurequantitativedata = uom.dim_unitofmeasureid,
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv, dim_unitofmeasure uom,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and uom.uom = ifnull(QAMV_MASSEINHSW,'Not Set')
and f_rr.dim_unitofmeasurequantitativedata <> uom.dim_unitofmeasureid;

update fact_resultsrecording f_rr
set f_rr.dim_inspectionmethodid = im.dim_inspectionmethodid,
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv, dim_inspectionmethod im,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and im.PlantCode = ifnull(QAMV_QMTB_WERKS,'Not Set')
and im.insprectionmethod = ifnull(QAMV_PMETHODE,'Not Set')
and im.VersionNumber = ifnull(QAMV_PMTVERSION,'Not Set')
and f_rr.dim_inspectionmethodid <> im.dim_inspectionmethodid;

update fact_resultsrecording f_rr
set dim_plantmicid = dp.dim_plantid,
dw_update_date = CURRENT_TIMESTAMP
from qamv mv, dim_plant dp,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and dp.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')
and dim_plantmicid <> dp.dim_plantid;

merge into fact_resultsrecording f_rr
using (select distinct f_rr.fact_resultsrecordingid, first_value(ic.dim_inspectioncharacteristicid) over (partition by  f_rr.fact_resultsrecordingid order by  f_rr.fact_resultsrecordingid) as dim_inspectioncharacteristicid 
from QAMV mv, dim_inspectioncharacteristic ic,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and ic.InspectionPlant = ifnull(QAMV_QPMK_WERKS,'Not Set')
and ic.Masterinspcharacteristics = ifnull(QAMV_VERWMERKM,'Not Set')
and ic.VersionNumber = ifnull(QAMV_MKVERSION,'Not Set')) t
on t.fact_resultsrecordingid=f_rr.fact_resultsrecordingid
when matched then update set f_rr.dim_inspectioncharacteristicid = t.dim_inspectioncharacteristicid
where  f_rr.dim_inspectioncharacteristicid <> t.dim_inspectioncharacteristicid;

update fact_resultsrecording f_rr
set f_rr.dim_inspectioncatalogcodesid_qamv = icc.dim_inspectioncatalogcodesid,
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv, dim_inspectioncatalogcodes icc,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
/* and icc.catalog = ifnull(QAMV_KATALGART1,'Not Set') doesn't return anything with this on */
and icc.codegroup = ifnull(QAMV_CODEGRQUAL,'Not Set')
and icc.code = ifnull(QAMV_CODEQUAL,'Not Set')
and icc.versionnumber = ifnull(QAMV_VERSION,'Not Set')
and f_rr.dim_inspectioncatalogcodesid_qamv <> icc.dim_inspectioncatalogcodesid;

update fact_resultsrecording f_rr
set f_rr.dim_inspectioncataloggroupid_qamv = icg.dim_inspectioncataloggroupid,
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv, dim_inspectioncataloggroup icg,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
/* and icg.catalog = ifnull(QAMV_KATALGART1,'Not Set') doesn't return anything with this on */
and icg.codegroup = ifnull(QAMV_CODEGRQUAL,'Not Set')
and f_rr.dim_inspectioncataloggroupid_qamv <> icg.dim_inspectioncataloggroupid;

update fact_resultsrecording f_rr
set f_rr.dim_sampledrawingitemsid = sdi.dim_sampledrawingitemsid,
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv, dim_sampledrawingitems sdi,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and sdi.sampleprocedure = ifnull(QAMV_PRZIEHVERF,'Not Set')
and sdi.versionno = ifnull(QAMV_VERSION,'Not Set')
and sdi.itemno = ifnull(QAMV_POSNRPRZV,0)
and f_rr.dim_sampledrawingitemsid <> sdi.dim_sampledrawingitemsid;

update fact_resultsrecording f_rr
set f_rr.dim_sampledrawingprocedureid = sdp.dim_sampledrawingprocedureid,
dw_update_date = CURRENT_TIMESTAMP
from QAMV mv, dim_sampledrawingprocedure sdp,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
and sdp.sampleprocedure = ifnull(QAMV_PRZIEHVERF,'Not Set')
and sdp.versionno = ifnull(QAMV_VERSION,'Not Set')
and f_rr.dim_sampledrawingprocedureid <> sdp.dim_sampledrawingprocedureid;
/* End QAMV updates */



/* Start QAPP updates */
update fact_resultsrecording f_rr
set f_rr.dd_codevaluation = ifnull(QAPP_VBEWERTUNG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QAPP pp,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(pp.QAPP_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(pp.QAPP_VORGLFNR,0)
and f_rr.dd_samplenumber = ifnull(QAPP_PROBENR,0)
and f_rr.dd_codevaluation <> ifnull(QAPP_VBEWERTUNG,'Not Set');
/* End QAPP updates */




/* Start QASE updates */
merge into fact_resultsrecording f_rr using
	(select ifnull(se.QASE_PRUEFLOS,0) QASE_PRUEFLOS,
			ifnull(se.QASE_VORGLFNR,0) QASE_VORGLFNR,
			ifnull(se.QASE_MERKNR,0) QASE_MERKNR,
			ifnull(se.QASE_DETAILERG,0) QASE_DETAILERG,
			se.QASE_ATTRIBUT,
			se.QASE_MASCHINE,
			se.QASE_MBEWERTG,
			se.QASE_MESSWERT,
			se.QASE_ORIGINAL_INPUT,
			se.QASE_PRUEFBEMKT
	from QASE se) se
on f_rr.dd_inspectionlotno = se.QASE_PRUEFLOS
	and f_rr.dd_currentnodeno = se.QASE_VORGLFNR
	and f_rr.dd_inspectioncharno = se.QASE_MERKNR
	and f_rr.dd_indivresultsno = se.QASE_DETAILERG
when matched then update
set
	f_rr.dd_attributresultsrecord = ifnull(se.QASE_ATTRIBUT,'Not Set'),
	f_rr.dd_generalinfo = ifnull(se.QASE_MASCHINE,'Not Set'),
	f_rr.dd_inspectionresultvaluation = ifnull(se.QASE_MBEWERTG,'Not Set'),
	f_rr.ct_msvalueforsampleunit = ifnull(se.QASE_MESSWERT,0),
	f_rr.dd_originvalue_qase = ifnull(se.QASE_ORIGINAL_INPUT,'Not Set'),
	f_rr.dd_shorttext_qase = ifnull(se.QASE_PRUEFBEMKT,'Not Set'),
	f_rr.dw_update_date = CURRENT_TIMESTAMP;





/*
update fact_resultsrecording f_rr
set f_rr.dd_attributresultsrecord = ifnull(QASE_ATTRIBUT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASE se,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
and f_rr.dd_attributresultsrecord <> ifnull(QASE_ATTRIBUT,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_generalinfo = ifnull(QASE_MASCHINE,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASE se,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
and f_rr.dd_generalinfo <> ifnull(QASE_MASCHINE,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_inspectionresultvaluation = ifnull(QASE_MBEWERTG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASE se,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
and f_rr.dd_inspectionresultvaluation <> ifnull(QASE_MBEWERTG,'Not Set')

update fact_resultsrecording f_rr
set f_rr.ct_msvalueforsampleunit = ifnull(QASE_MESSWERT,0),
dw_update_date = CURRENT_TIMESTAMP
from QASE se,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
and f_rr.ct_msvalueforsampleunit <> ifnull(QASE_MESSWERT,0)

update fact_resultsrecording f_rr
set f_rr.dd_originvalue_qase = ifnull(QASE_ORIGINAL_INPUT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASE se,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
and f_rr.dd_originvalue_qase <> ifnull(QASE_ORIGINAL_INPUT,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_shorttext_qase = ifnull(QASE_PRUEFBEMKT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASE se,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
and f_rr.dd_shorttext_qase <> ifnull(QASE_PRUEFBEMKT,'Not Set')
*/



update fact_resultsrecording f_rr
set f_rr.dim_inspectioncatalogcodesid_qase = icc.dim_inspectioncatalogcodesid,
dw_update_date = CURRENT_TIMESTAMP
from QASE se, dim_inspectioncatalogcodes icc,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
/* and icc.catalog = ifnull(QASE_KATALGART1,'Not Set') */
and icc.codegroup = ifnull(QASE_GRUPPE1,'Not Set')
and icc.code = ifnull(QASE_CODE1,'Not Set')
and icc.versionnumber = ifnull(QASE_VERSION1,'Not Set')
and f_rr.dim_inspectioncatalogcodesid_qase <> icc.dim_inspectioncatalogcodesid;

update fact_resultsrecording f_rr
set f_rr.dim_inspectioncataloggroupid_qase = icg.dim_inspectioncataloggroupid,
dw_update_date = CURRENT_TIMESTAMP
from QASE se, dim_inspectioncataloggroup icg,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
/* and icg.catalog = ifnull(QASE_KATALGART1,'Not Set') */
and icg.codegroup = ifnull(QASE_GRUPPE1,'Not Set')
and f_rr.dd_attributresultsrecord <> ifnull(QASE_ATTRIBUT,'Not Set');

MERGE INTO fact_resultsrecording fact
USING (SELECT f_rr.fact_resultsrecordingid, dt.dim_dateid
	   FROM QASE se, QAMV mv, dim_date dt, dim_plant p, fact_resultsrecording f_rr
	   where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
		and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
		and f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
		and dt.datevalue = ifnull(se.QASE_PRUEFDATUB,'0001-01-01')
		and dt.companycode = p.companycode
		and dt.plantcode_factory = p.plantcode /* plantcode_factory additional condition- BI-5085 */
		and p.plantcode = ifnull(mv.QAMV_QPMK_WERKS,'Not Set')
		and f_rr.dim_dateendinpsectionid_qase <> dt.dim_dateid) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateendinpsectionid_qase = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

MERGE INTO fact_resultsrecording fact
USING (SELECT f_rr.fact_resultsrecordingid, dt.dim_dateid
	   FROM QASE se, QAMV mv, dim_date dt, dim_plant p, fact_resultsrecording f_rr
	   where f_rr.dd_inspectionlotno = ifnull(se.QASE_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(se.QASE_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(se.QASE_MERKNR,0)
		and f_rr.dd_indivresultsno = ifnull(se.QASE_DETAILERG,0)
		and f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
		and dt.datevalue = ifnull(se.QASE_PRUEFDATUV,'0001-01-01')
		and dt.companycode = p.companycode
		and dt.plantcode_factory = p.plantcode /* plantcode_factory additional condition- BI-5085 */
		and p.plantcode = ifnull(mv.QAMV_QPMK_WERKS,'Not Set')
		and f_rr.dim_datestartinspectionid_qase <> dt.dim_dateid) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dim_datestartinspectionid_qase = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

/* End QASE updates */



/* Start QASR updates */
merge into fact_resultsrecording f_rr using
	(select ifnull(sr.QASR_PRUEFLOS,0) QASR_PRUEFLOS,
			ifnull(sr.QASR_VORGLFNR,0) QASR_VORGLFNR,
			ifnull(sr.QASR_MERKNR,0) QASR_MERKNR,
			ifnull(sr.QASR_PROBENR,0) QASR_PROBENR,
			sr.QASR_MASCHINE,
			sr.QASR_MBEWERTG,
			sr.QASR_MINWERT,
			sr.QASR_MITTELWERT,
			sr.QASR_ORIGINAL_INPUT,
			sr.QASR_PRUEFBEMKT,
			sr.QASR_QERGDATH,
			sr.QASR_SATZSTATUS
	from QASR sr) sr
on f_rr.dd_inspectionlotno = sr.QASR_PRUEFLOS
	and f_rr.dd_currentnodeno = sr.QASR_VORGLFNR
	and f_rr.dd_inspectioncharno = sr.QASR_MERKNR
	and f_rr.dd_samplenumber = sr.QASR_PROBENR
when matched then update
set
	f_rr.dd_generalinfo_qasr = ifnull(sr.QASR_MASCHINE,'Not Set'),
	f_rr.dd_inspresultvaluation_qasr = ifnull(sr.QASR_MBEWERTG,'Not Set'),
	f_rr.ct_minvalue_validmeasured_qasr = ifnull(sr.QASR_MINWERT,0),
	f_rr.ct_mean_validmeasured_qasr = ifnull(sr.QASR_MITTELWERT,0),
	f_rr.dd_originvalue_qasr = ifnull(sr.QASR_ORIGINAL_INPUT,'Not Set'),
	f_rr.dd_shorttext_qasr = ifnull(sr.QASR_PRUEFBEMKT,'Not Set'),
	f_rr.dd_originresultsset_qasr = ifnull(sr.QASR_QERGDATH,'Not Set'),
	f_rr.dd_resultsrecordsets_qasr = ifnull(sr.QASR_SATZSTATUS,'Not Set'),
	f_rr.dw_update_date = CURRENT_TIMESTAMP;




/*
update fact_resultsrecording f_rr
set f_rr.dd_generalinfo_qasr = ifnull(QASR_MASCHINE,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASR sr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
and f_rr.dd_generalinfo_qasr <> ifnull(QASR_MASCHINE,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_inspresultvaluation_qasr = ifnull(QASR_MBEWERTG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASR sr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
and f_rr.dd_inspresultvaluation_qasr <> ifnull(QASR_MBEWERTG,'Not Set')

update fact_resultsrecording f_rr
set f_rr.ct_minvalue_validmeasured_qasr = ifnull(QASR_MINWERT,0),
dw_update_date = CURRENT_TIMESTAMP
from QASR sr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
and f_rr.ct_minvalue_validmeasured_qasr <> ifnull(QASR_MINWERT,0)

update fact_resultsrecording f_rr
set f_rr.ct_mean_validmeasured_qasr = ifnull(QASR_MITTELWERT,0),
dw_update_date = CURRENT_TIMESTAMP
from QASR sr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
and f_rr.ct_mean_validmeasured_qasr <> ifnull(QASR_MITTELWERT,0)

update fact_resultsrecording f_rr
set f_rr.dd_originvalue_qasr = ifnull(QASR_ORIGINAL_INPUT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASR sr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
and f_rr.dd_originvalue_qasr <> ifnull(QASR_ORIGINAL_INPUT,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_shorttext_qasr = ifnull(QASR_PRUEFBEMKT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASR sr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
and f_rr.dd_shorttext_qasr <> ifnull(QASR_PRUEFBEMKT,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_originresultsset_qasr = ifnull(QASR_QERGDATH,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASR sr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
and f_rr.dd_originresultsset_qasr <> ifnull(QASR_QERGDATH,'Not Set')

update fact_resultsrecording f_rr
set f_rr.dd_resultsrecordsets_qasr = ifnull(QASR_SATZSTATUS,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QASR sr,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
and f_rr.dd_resultsrecordsets_qasr <> ifnull(QASR_SATZSTATUS,'Not Set')
*/



MERGE INTO fact_resultsrecording fact
USING (SELECT f_rr.fact_resultsrecordingid, dt.dim_dateid
	   FROM QASR sr, QAMV mv, dim_date dt, dim_plant p, fact_resultsrecording f_rr
	   where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
		and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
		and f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
		and dt.datevalue = ifnull(QASR_PRUEFDATUB,'0001-01-01')
		and dt.companycode = p.companycode
		and dt.plantcode_factory = p.plantcode /* plantcode_factory additional condition- BI-5085 */
		and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')
		and f_rr.dim_dateendinpsectionid_qasr <> dt.dim_dateid) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateendinpsectionid_qasr = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

MERGE INTO fact_resultsrecording fact
USING (SELECT f_rr.fact_resultsrecordingid, dt.dim_dateid
	   FROM QASR sr, QAMV mv, dim_date dt, dim_plant p, fact_resultsrecording f_rr
	   where f_rr.dd_inspectionlotno = ifnull(sr.QASR_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(sr.QASR_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(sr.QASR_MERKNR,0)
		and f_rr.dd_samplenumber = ifnull(sr.QASR_PROBENR,0)
		and f_rr.dd_inspectionlotno = ifnull(mv.QAMV_PRUEFLOS,0)
		and f_rr.dd_currentnodeno = ifnull(mv.QAMV_VORGLFNR,0)
		and f_rr.dd_inspectioncharno = ifnull(mv.QAMV_MERKNR,0)
		and dt.datevalue = ifnull(QASR_PRUEFDATUV,'0001-01-01')
		and dt.companycode = p.companycode
		and dt.plantcode_factory = p.plantcode /* plantcode_factory additional condition- BI-5085 */
		and p.plantcode = ifnull(QAMV_QPMK_WERKS,'Not Set')
		and f_rr.dim_datestartinspectionid_qasr <> dt.dim_dateid) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dim_datestartinspectionid_qasr = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

/* End QASR updates */



/* Start JEST and JCDS updates */
UPDATE fact_resultsrecording f_rr
SET f_rr.dd_deletionflag_qprs = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QPRS j,fact_resultsrecording f_rr
WHERE j.JEST_OBJNR = f_rr.dd_objectnumber
and j.JEST_STAT = 'I0076' and j.JEST_INACT is NULL
AND f_rr.dd_deletionflag_qprs <> 'X';
/* End JEST and JCDS updates */


/* Start QALS updates */
merge into fact_resultsrecording f_rr using
	(select ifnull(q.QALS_PRUEFLOS,'0') QALS_PRUEFLOS,
			q.QALS_CHARG,
			q.QALS_AUFNR,
			q.QALS_MBLNR,
			q.QALS_MJAHR,
			q.QALS_EBELP,
			q.QALS_EBELN,
			q.QALS_LMENGEPR,
			q.QALS_LMENGEIST,
			q.QALS_LMENGE04,
			q.QALS_LOSMENGE,
			q.QALS_LMENGEZUB,
			q.QALS_LMENGESCH,
			q.QALS_LMENGE07,
			q.QALS_LMENGE05,
			q.QALS_LMENGE03,
			q.QALS_GESSTICHPR,
			q.QALS_LMENGE02,
			q.QALS_LMENGE01
	from QALS q) q
on f_rr.dd_inspectionlotno = q.QALS_PRUEFLOS
when matched then update
set
	f_rr.dd_batchno = ifnull(q.QALS_CHARG,'Not Set'),
	f_rr.dd_orderno= ifnull(q.QALS_AUFNR, 'Not Set'),
	f_rr.dd_MaterialDocNo= ifnull(q.QALS_MBLNR, 'Not Set'),
	f_rr.dd_MaterialDocYear= ifnull(q.QALS_MJAHR,0),
	f_rr.dd_documentitemno= ifnull(q.QALS_EBELP,0),
	f_rr.dd_documentno= ifnull(q.QALS_EBELN,'Not Set'),
	f_rr.ct_actualinspectedqty= ifnull(q.QALS_LMENGEPR,0),
	f_rr.ct_actuallotqty= ifnull(q.QALS_LMENGEIST,0),
	f_rr.ct_blockedqty= ifnull(q.QALS_LMENGE04,0),
	f_rr.ct_inspectionlotqty= ifnull(q.QALS_LOSMENGE,0),
	f_rr.ct_postedqty= ifnull(q.QALS_LMENGEZUB,0),
	f_rr.ct_qtydefective= ifnull(q.QALS_LMENGESCH,0),
	f_rr.ct_qtyreturned= ifnull(q.QALS_LMENGE07,0),
	f_rr.ct_reserveqty= ifnull(q.QALS_LMENGE05,0),
	f_rr.ct_sampleqty= ifnull(q.QALS_LMENGE03,0),
	f_rr.ct_samplesizeqty= ifnull(q.QALS_GESSTICHPR,0),
	f_rr.ct_scrapqty= ifnull(q.QALS_LMENGE02,0),
	f_rr.ct_unrestrictedqty= ifnull(q.QALS_LMENGE01,0),
	f_rr.dw_update_date = CURRENT_TIMESTAMP;





/*
UPDATE fact_resultsrecording f_rr
SET f_rr.dd_batchno = ifnull(q.QALS_CHARG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
and f_rr.dd_batchno <> ifnull(q.QALS_CHARG,'Not Set')

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_orderno= ifnull(q.QALS_AUFNR, 'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
and f_rr.dd_orderno <> ifnull(q.QALS_AUFNR, 'Not Set')

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_MaterialDocNo= ifnull(q.QALS_MBLNR, 'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.dd_MaterialDocNo <> ifnull(q.QALS_MBLNR, 'Not Set')

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_MaterialDocYear= ifnull(q.QALS_MJAHR,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.dd_MaterialDocYear <> ifnull(q.QALS_MJAHR,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_documentitemno= ifnull(q.QALS_EBELP,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.dd_documentitemno <> ifnull(q.QALS_EBELP,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_documentno= ifnull(q.QALS_EBELN,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.dd_documentno <> ifnull(q.QALS_EBELN,'Not Set')

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_actualinspectedqty= ifnull(QALS_LMENGEPR,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_actualinspectedqty <> ifnull(QALS_LMENGEPR,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_actuallotqty= ifnull(QALS_LMENGEIST,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_actuallotqty <> ifnull(QALS_LMENGEIST,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_blockedqty= ifnull(QALS_LMENGE04,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_blockedqty <> ifnull(QALS_LMENGE04,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_inspectionlotqty= ifnull(QALS_LOSMENGE,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_inspectionlotqty <> ifnull(QALS_LOSMENGE,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_postedqty= ifnull(QALS_LMENGEZUB,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_postedqty <> ifnull(QALS_LMENGEZUB,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_qtydefective= ifnull(QALS_LMENGESCH,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_qtydefective <> ifnull(QALS_LMENGESCH,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_qtyreturned= ifnull(QALS_LMENGE07,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_qtyreturned <> ifnull(QALS_LMENGE07,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_reserveqty= ifnull(QALS_LMENGE05,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_reserveqty <> ifnull(QALS_LMENGE05,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_sampleqty= ifnull(QALS_LMENGE03,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_sampleqty <> ifnull(QALS_LMENGE03,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_samplesizeqty= ifnull(QALS_GESSTICHPR,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_samplesizeqty <> ifnull(QALS_GESSTICHPR,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_scrapqty= ifnull(QALS_LMENGE02,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_scrapqty <> ifnull(QALS_LMENGE02,0)

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_unrestrictedqty= ifnull(QALS_LMENGE01,0),
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND f_rr.ct_unrestrictedqty <> ifnull(QALS_LMENGE01,0)
*/



UPDATE fact_resultsrecording f_rr
SET f_rr.dim_partid= dp.dim_partid,
dw_update_date = CURRENT_TIMESTAMP
FROM qals q, dim_part dp,fact_resultsrecording f_rr
WHERE f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
and dp.partnumber = ifnull(QALS_MATNR, 'Not Set')
and dp.plant = ifnull(QALS_WERK,'Not Set')
and f_rr.dim_partid <> dp.dim_partid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_plantid= dp.dim_plantid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_plant dp,fact_resultsrecording f_rr
WHERE f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND dp.plantcode = ifnull(QALS_WERK,'Not Set')
and  f_rr.dim_plantid <> dp.dim_plantid;

MERGE INTO fact_resultsrecording fact
USING (SELECT f_rr.fact_resultsrecordingid, b.dim_batchid
		FROM dim_batch b, dim_part dp,fact_resultsrecording f_rr
		where f_rr.dim_partid = dp.dim_partid and
		f_rr.dd_batchno = b.batchnumber and
		dp.partnumber = b.partnumber and
		b.plantcode = dp.plant and
		f_rr.dim_batchid <> b.dim_batchid) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dim_batchid = src.dim_batchid,
dw_update_date = CURRENT_TIMESTAMP;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_inspectiontypeid= ityp.dim_inspectiontypeid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_inspectiontype ityp,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ityp.InspectionTypeCode = ifnull(QALS_ART, 'Not Set')
and f_rr.dim_inspectiontypeid <> ityp.dim_inspectiontypeid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidrecordchanged= rc.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date rc, dim_plant dp,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND rc.DateValue = ifnull(QALS_AENDERDAT,'0001-01-01')
AND rc.companycode = dp.CompanyCode
and rc.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND dp.plantcode = ifnull(q.QALS_WERK,'Not Set')
AND f_rr.dim_dateidrecordchanged <> rc.dim_dateid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidrecordcreated= rc.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date rc, dim_plant dp,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND rc.DateValue = ifnull(QALS_ERSTELDAT,'0001-01-01')
AND rc.companycode = dp.CompanyCode
and rc.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND dp.plantcode = ifnull(q.QALS_WERK, 'Not Set')
AND f_rr.dim_dateidrecordcreated <> rc.dim_dateid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidrecordcreated= rc.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date rc, dim_plant dp,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND rc.DateValue = ifnull(QALS_ERSTELDAT,'0001-01-01')
AND rc.companycode = dp.CompanyCode
and rc.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND dp.plantcode = ifnull(q.QALS_WERK, 'Not Set')
AND f_rr.dim_dateidrecordcreated <> rc.dim_dateid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_documenttypetextid= dtt.dim_documenttypetextid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_documenttypetext dtt,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND dtt.type = ifnull(QALS_BLART, 'Not Set')
AND f_rr.dim_documenttypetextid <> dtt.dim_documenttypetextid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_itemcategoryid= icc.dim_itemcategoryid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_itemcategory icc,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND icc.CategoryCode = ifnull(QALS_PSTYP, 'Not Set')
AND f_rr.dim_itemcategoryid <> icc.dim_itemcategoryid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_purchaseorgid= po.dim_purchaseorgid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_purchaseorg po,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND po.PurchaseOrgCode = ifnull(QALS_EKORG, 'Not Set')
AND f_rr.dim_purchaseorgid <> po.dim_purchaseorgid;


UPDATE fact_resultsrecording f_rr
SET f_rr.Dim_AccountCategoryid= ac.Dim_AccountCategoryid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_accountcategory ac,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ac.Category = ifnull(QALS_KNTTP, 'Not Set')
AND f_rr.Dim_AccountCategoryid <> ac.Dim_AccountCategoryid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_costcenterid= cc.dim_costcenterid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_costcenter cc,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND cc.Code = ifnull(QALS_KOSTL, 'Not Set')
AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set')
AND f_rr.dim_costcenterid <> cc.dim_costcenterid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidinspectionend= ie.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date ie, dim_plant dp,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ie.DateValue = ifnull(QALS_PAENDTERM,'0001-01-01')
AND ie.CompanyCode = dp.CompanyCode
and ie.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND dp.plantcode = ifnull(q.QALS_WERK, 'Not Set')
AND f_rr.dim_dateidinspectionend <> ie.dim_dateid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidinspectionstart= ie.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date ie, dim_plant dp,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ie.DateValue = ifnull(QALS_PASTRTERM,'0001-01-01')
AND ie.CompanyCode = dp.CompanyCode
and ie.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND dp.plantcode = ifnull(q.QALS_WERK, 'Not Set')
AND f_rr.dim_dateidinspectionstart <> ie.dim_dateid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_inspectionlotmiscid= ilm.dim_inspectionlotmiscid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_inspectionlotmisc ilm,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND AutomaticInspectionLot = ifnull(QALS_STAT01, 'Not Set')
AND BatchManagementRequired = ifnull(QALS_XCHPF, 'Not Set')
AND CompleteInspection = ifnull(QALS_HPZ, 'Not Set')
AND DocumentationRequired = ifnull(QALS_STAT19, 'Not Set')
AND InspectionPlanRequired = ifnull(QALS_STAT20, 'Not Set')
AND InspectionStockQuantity = ifnull(QALS_INSMK, 'Not Set')
AND ShotTermInspectionComplete = ifnull(QALS_STAT14, 'Not Set')
AND StockPostingsComplete = ifnull(QALS_STAT34, 'Not Set')
AND UsageDecisionMade = ifnull(QALS_STAT35, 'Not Set')
AND LotSkipped = ifnull(QALS_KZSKIPLOT, 'Not Set')
AND f_rr.dim_inspectionlotmiscid <> ilm.dim_inspectionlotmiscid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_inspectionlotoriginid= ilo.dim_inspectionlotoriginid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_inspectionlotorigin ilo,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ilo.InspectionLotOriginCode = ifnull(q.QALS_HERKUNFT, 'Not Set')
AND f_rr.dim_inspectionlotoriginid <> ilo.dim_inspectionlotoriginid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_storagelocationid= isl.dim_storagelocationid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_storagelocation isl, dim_plant dp,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND isl.LocationCode = ifnull(QALS_LAGORTVORG,'Not Set')
AND isl.Plant = dp.PlantCode
AND dp.PlantCode = ifnull(QALS_WERK,'Not Set')
AND f_rr.dim_storagelocationid <> isl.dim_storagelocationid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_vendorid= dv1.dim_vendorid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_vendor dv1,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND dv1.VendorNumber = ifnull(QALS_LIFNR, 'Not Set')
AND f_rr.dim_vendorid <> dv1.dim_vendorid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_warehousenumberid= wn.dim_warehousenumberid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_warehousenumber wn,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND wn.WarehouseCode = ifnull(QALS_LGNUM, 'Not Set')
AND f_rr.dim_warehousenumberid <> wn.dim_warehousenumberid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_inspectionseverityid= isvr.dim_inspectionseverityid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_inspectionseverity isvr,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND isvr.InspectionSeverityCode = ifnull(QALS_PRSCHAERFE,0)
AND f_rr.dim_inspectionseverityid <> isvr.dim_inspectionseverityid;

/* End QALS updates */


UPDATE fact_resultsrecording f_rr
SET f_rr.dim_inspectionlotstatusid = f_il.dim_inspectionlotstatusid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_inspectionlotstatusid <> f_il.dim_inspectionlotstatusid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_inspusagedecisionid = f_il.dim_inspusagedecisionid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_inspusagedecisionid <> f_il.dim_inspusagedecisionid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_lastgrdate_q_id = f_il.dim_lastgrdate_q_id,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_lastgrdate_q_id <> f_il.dim_lastgrdate_q_id;


UPDATE fact_resultsrecording f_rr
SET f_rr.dim_nextbusday_lastgrdate_q_id = f_il.dim_nextbusday_lastgrdate_q_id,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_nextbusday_lastgrdate_q_id <> f_il.dim_nextbusday_lastgrdate_q_id;


UPDATE fact_resultsrecording f_rr
SET f_rr.dim_targetreleasedateid = f_il.dim_targetreleasedateid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_targetreleasedateid <> f_il.dim_targetreleasedateid;

UPDATE fact_resultsrecording f_rr
SET f_rr.ct_AverageInspectionDuration = f_il.ct_AverageInspectionDuration,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.ct_AverageInspectionDuration <> f_il.ct_AverageInspectionDuration;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidactualinspectionend = f_il.dim_dateidactualinspectionend,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_dateidactualinspectionend <> f_il.dim_dateidactualinspectionend;

update fact_resultsrecording f_rr
SET f_rr.dd_shorttext = f_il.dd_shorttext,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dd_shorttext <> f_il.dd_shorttext;


/* OZ: duplicates because QALS APLZL is not populated accordingly for MERCK to match the entire PK of AFVC(AUFPL,APLZL) - took first value ordered by AFVC_APLZL */
MERGE INTO fact_resultsrecording fact
USING (select distinct f_rr.fact_resultsrecordingid, ifnull(a.AFVC_LTXA1,'Not Set') as AFVC_LTXA1
	  	FROM qals q, AFVC a,fact_resultsrecording f_rr
		where ifnull(q.QALS_AUFPL,0) = ifnull(a.AFVC_AUFPL,0)
		and f_rr.dd_currentnodeno = ifnull(a.AFVC_APLZL,0)
		and q.QALS_PRUEFLOS = f_rr.dd_inspectionlotno
		and dd_operation <> ifnull(a.AFVC_LTXA1,'Not Set')) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dd_operation = src.AFVC_LTXA1,
dw_update_date = CURRENT_TIMESTAMP;

/* Madalina 22 Aug 2016 - add new field Operation/Activity Number, based on AFVC-VORNR */
MERGE INTO fact_resultsrecording fact
USING (select distinct f_rr.fact_resultsrecordingid, ifnull(a.AFVC_VORNR,'Not Set') as AFVC_VORNR
		from qals q, AFVC a,fact_resultsrecording f_rr
		where  ifnull(q.QALS_AUFPL,0) = ifnull(a.AFVC_AUFPL,0)
		and f_rr.dd_currentnodeno = ifnull(a.AFVC_APLZL,0)
		and q.QALS_PRUEFLOS = f_rr.dd_inspectionlotno
		and dd_OperationActivityNo <> ifnull(a.AFVC_VORNR,'Not Set')) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dd_OperationActivityNo = src.AFVC_VORNR,
	dw_update_date = CURRENT_TIMESTAMP;
/* End 22 Aug 2016 */

/* Octavian: new columns added*/
update fact_resultsrecording f_rr
SET dd_group = ifnull(q.QALS_PLNNR,'Not Set'),
dw_update_date = current_timestamp
from qals q,fact_resultsrecording f_rr
WHERE
f_rr.dd_inspectionlotno = q.QALS_PRUEFLOS
and dd_group <> ifnull(q.QALS_PLNNR,'Not Set');

update fact_resultsrecording f_rr
SET dd_groupcounter = ifnull(q.QALS_PLNAL,'Not Set'),
dw_update_date = current_timestamp
from qals q,fact_resultsrecording f_rr
WHERE
f_rr.dd_inspectionlotno = q.QALS_PRUEFLOS
and dd_groupcounter <> ifnull(q.QALS_PLNAL,'Not Set');

/* 26 Jan 2017 Georgiana changes according to BI-5316: adding dd_objectnumber_qals in order to be sure that all rows from jcds_qals are correctly updated on j.JCDS_OBJNR = q.QALS_OBJNR condition*/

update fact_resultsrecording f_rr
set f_rr.dd_objectnumber_qals= ifnull(q.QALS_OBJNR,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from QALS q,fact_resultsrecording f_rr
 where  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')
AND  f_rr.dd_objectnumber_qals <> ifnull(q.QALS_OBJNR,'Not Set'); 

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_inspectionactivedate = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j,/* QALS q,*/fact_resultsrecording f_rr
WHERE /*j.JCDS_OBJNR = q.QALS_OBJNR BI-5316 */
f_rr.dd_objectnumber_qals = ifnull(j.JCDS_OBJNR,'Not Set')
/*AND f_rr.dd_inspectionlotno = q.QALS_PRUEFLOS*/
AND j.JCDS_STAT = 'I0212'
and j.jcds_chgnr = '1'
AND f_rr.dd_inspectionactivedate <> ifnull(j.JCDS_UDATE,'0001-01-01');


update fact_resultsrecording f_rr
SET dd_additionalinfo = CASE WHEN dd_generalinfo_qamr <> 'Not Set' THEN dd_generalinfo_qamr
 ELSE (CASE WHEN dd_generalinfo <> 'Not Set' THEN dd_generalinfo
 ELSE (CASE WHEN dd_generalinfo_qasr <> 'Not Set' THEN dd_generalinfo_qasr ELSE 'Not Set' END) END) END
where dd_additionalinfo <> CASE WHEN dd_generalinfo_qamr <> 'Not Set' THEN dd_generalinfo_qamr
 ELSE (CASE WHEN dd_generalinfo <> 'Not Set' THEN dd_generalinfo
 ELSE (CASE WHEN dd_generalinfo_qasr <> 'Not Set' THEN dd_generalinfo_qasr ELSE 'Not Set' END) END) END;
/* Octavian: new columns added*/

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_dateuserstatusqcco = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j, /*QALS q,*/fact_resultsrecording f_rr
WHERE /*j.JCDS_OBJNR = q.QALS_OBJNR BI-5316*/
f_rr.dd_objectnumber_qals = ifnull(j.JCDS_OBJNR,'Not Set')
/*AND f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')*/
AND j.JCDS_STAT = 'E0004'
and j.jcds_chgnr = '1'
AND f_rr.dd_dateuserstatusqcco <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_userstatusqcco = ifnull(j.JCDS_USNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j, /*QALS q,*/fact_resultsrecording f_rr
WHERE /*j.JCDS_OBJNR = q.QALS_OBJNR BI-5316*/
f_rr.dd_objectnumber_qals = ifnull(j.JCDS_OBJNR,'Not Set')
/*AND f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')*/
AND j.JCDS_STAT = 'E0004'
and j.jcds_chgnr = '1'
AND f_rr.dd_userstatusqcco <> ifnull(j.JCDS_USNAM,'Not Set');

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_dateuserstatussamr = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j,/* QALS q,*/fact_resultsrecording f_rr
WHERE /*j.JCDS_OBJNR = q.QALS_OBJNR BI-5316*/
f_rr.dd_objectnumber_qals = ifnull(j.JCDS_OBJNR,'Not Set')
/*AND f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')*/
AND j.JCDS_STAT = 'E0002'
and j.jcds_chgnr = '1'
AND f_rr.dd_dateuserstatussamr <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_userstatussamr = ifnull(j.JCDS_USNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j, /*QALS q,*/fact_resultsrecording f_rr
WHERE /*j.JCDS_OBJNR = q.QALS_OBJNR BI-5316*/
f_rr.dd_objectnumber_qals = ifnull(j.JCDS_OBJNR,'Not Set')
/*AND f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')*/
AND j.JCDS_STAT = 'E0002'
and j.jcds_chgnr = '1'
AND f_rr.dd_userstatussamr <> ifnull(j.JCDS_USNAM,'Not Set');

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_dateuserstatuscrtd = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j,/* QALS q,*/ fact_resultsrecording f_rr
WHERE /*j.JCDS_OBJNR = q.QALS_OBJNR BI-5316*/
f_rr.dd_objectnumber_qals = ifnull(j.JCDS_OBJNR,'Not Set')
/*AND f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')*/
AND j.JCDS_STAT = 'E0001'
and j.jcds_chgnr = '1'
AND f_rr.dd_dateuserstatuscrtd <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_resultsrecording f_rr
SET f_rr.dd_userstatuscrtd = ifnull(j.JCDS_USNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j, /*QALS q,*/fact_resultsrecording f_rr
WHERE /*j.JCDS_OBJNR = q.QALS_OBJNR BI-5316*/
f_rr.dd_objectnumber_qals = ifnull(j.JCDS_OBJNR,'Not Set')
/*AND f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')*/
AND j.JCDS_STAT = 'E0001'
and j.jcds_chgnr = '1'
AND f_rr.dd_userstatuscrtd <> ifnull(j.JCDS_USNAM,'Not Set');

update fact_resultsrecording f_rr
SET f_rr.dd_unitofmeasureqtdata = ifnull(q.QAMV_MASSEINHSW,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM QAMV q,fact_resultsrecording f_rr
WHERE
ifnull(QAMV_PRUEFLOS,'0') = f_rr.dd_inspectionlotno AND
ifnull(QAMV_VORGLFNR,0) = f_rr.dd_currentnodeno AND
ifnull(QAMV_MERKNR,0) = f_rr.dd_inspectioncharno AND
f_rr.dd_unitofmeasureqtdata <> ifnull(q.QAMV_MASSEINHSW,'Not Set');

/* OZ: duplicates as of now due to PLPO OBJTY being null, used FIRST VALUE */
--drop table if exists tmp_insplot_update_workcenter

/* Madalina 14 Oct 2016 - Change logic for the Workcenter calculation - BI-4434 */
/* create table tmp_insplot_update_workcenter as
select distinct q.QALS_PRUEFLOS,FIRST_VALUE(wc.dim_workcenterid) OVER (PARTITION BY q.QALS_PRUEFLOS ORDER BY wc.dim_workcenterid DESC) AS dim_workcenterid
from QALS q, PLPO po, CRHD c, dim_workcenter wc
where ifnull(po.PLPO_PLNNR,'Not Set')  = ifnull(q.QALS_PLNNR,'Not Set')
and ifnull(po.PLPO_PLNTY,'Not Set') = ifnull(q.QALS_PLNTY,'Not Set') */
-- and ifnull(po.PLPO_PLNAL,'Not Set') = ifnull(q.qals_PLNAL,'Not Set')
/* and ifnull(po.PLPO_OBJTY,'Not Set') = ifnull(c.CRHD_OBJTY,'Not Set') merck only has nulls on PLPO_OBJTY */
/* and ifnull(lpad(convert(varchar(4),po.PLPO_ZAEHL),2,'0'),'Not Set')  = ifnull(q.QALS_PLNAL,'Not Set')
and ifnull(po.PLPO_ARBID,0) = ifnull(c.CRHD_OBJID,0)
 and wc.objectid = cast(c.CRHD_OBJID as varchar(8))
and wc.objecttype = ifnull(TRIM(c.CRHD_OBJTY),'Not Set') */
/*
create table tmp_insplot_update_workcenter as
 select distinct q.QALS_PRUEFLOS,FIRST_VALUE(wc.dim_workcenterid) OVER (PARTITION BY q.QALS_PRUEFLOS ORDER BY wc.dim_workcenterid DESC) AS dim_workcenterid
 from QALS q, PLKO pk, PLPO po, CRHD c, dim_workcenter wc
 where ifnull(pk.PLKO_PLNNR,'Not Set') = ifnull(q.QALS_PLNNR,'Not Set')
 and ifnull(pk.PLKO_PLNAL,'Not Set') = ifnull(q.qals_PLNAL,'Not Set')
 and ifnull(po.PLPO_PLNNR,'Not Set') = ifnull(q.QALS_PLNNR,'Not Set')
 and ifnull(po.PLPO_ZAEHL,'Not Set') = ifnull(pk.PLKO_ZAEHL,'Not Set')
 and ifnull(po.PLPO_ARBID,0) = ifnull(c.CRHD_OBJID,0)
 and wc.objectid = cast(c.CRHD_OBJID as varchar(8))
 and wc.objecttype = ifnull(TRIM(c.CRHD_OBJTY),'Not Set')
 */
 /* END BI-4434 */
/*
update fact_resultsrecording f_rr
SET f_rr.dim_workcenterid = ifnull(q.dim_workcenterid,1),
dw_update_date = current_timestamp
from tmp_insplot_update_workcenter q ,fact_resultsrecording f_rr
WHERE f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')
and f_rr.dim_workcenterid <> ifnull(q.dim_workcenterid,1)
drop table if exists tmp_insplot_update_workcenter
*/

/*START Alin 19 may 2017 - logic changed acc to BI-4434 */
drop table if exists tmp_qals_dwc;
create table tmp_qals_dwc as 
select distinct q.QALS_PRUEFLOS, x.AFVC_VORNR, max(dim_workcenterid) as dim_workcenterid
from
(select distinct QALS_PRUEFLOS, QALS_AUFPL from QALS)q,
(select distinct a.AFVC_AUFPL, c.CRHD_ARBPL, a.AFVC_VORNR, c.CRHD_OBJID, c.CRHD_OBJTY from
(
SELECT DISTINCT AFVC_AUFPL, AFVC_VORNR, AFVC_ARBID, max(AFVC_APLZL) FROM AFVC
GROUP BY AFVC_AUFPL, AFVC_VORNR, AFVC_ARBID
) a,
(SELECT DISTINCT CRHD_OBJID, CRHD_ARBPL,CRHD_OBJTY from CRHD) c
WHERE a.AFVC_ARBID = c.CRHD_OBJID)x, 
dim_workcenter wc
where x.AFVC_AUFPL = q.QALS_AUFPL
and wc.objectid = cast(x.CRHD_OBJID as varchar(8))
and wc.objecttype = ifnull(TRIM(x.CRHD_OBJTY),'Not Set')
group by q.QALS_PRUEFLOS, x.AFVC_VORNR;

update fact_resultsrecording f_rr
SET f_rr.dim_workcenterid = ifnull(q.dim_workcenterid,1),
dw_update_date = current_timestamp
from tmp_qals_dwc q ,fact_resultsrecording f_rr
WHERE f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'Not Set')
and f_rr.DD_OPERATIONACTIVITYNO = ifnull(q.AFVC_VORNR,'Not Set')
and f_rr.dim_workcenterid <> ifnull(q.dim_workcenterid,1);

drop table if exists tmp_qals_dwc;
/*END Alin 19 may 2017 - logic changed acc to BI-4434 */

/* Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

UPDATE fact_resultsrecording f
SET f.std_exchangerate_dateid = dt.dim_dateid
FROM dim_plant p
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode
								and dt.plantcode_factory = p.plantcode /* plantcode_factory additional condition- BI-5085 */
		 INNER JOIN fact_resultsrecording f ON f.dim_plantid = p.dim_plantid

WHERE   dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

/* END Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

/*20 Apr Georgiana Changes according to BI-2621*/

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidorderduedate = po.dim_dateidorderduedate,
dw_update_date = current_timestamp
FROM
fact_productionorder po,
AFKO_AFPO_AUFK mhi,fact_resultsrecording f_rr
WHERE
po.dd_ordernumber = mhi.AFKO_AUFNR
AND po.dd_orderitemno = mhi.AFPO_POSNR
AND f_rr.dd_orderno = mhi.AFKO_AUFNR
and f_rr.dim_dateidorderduedate <> po.dim_dateidorderduedate;

/* 20 Apr End of changes*/

/* OZ: Merck wants the join on sample number removed so all rows from one group gets updated */
/*05 Dec 2016 Georgiana changes added back the link on SampleNumber also according to BI-4917*/
MERGE INTO fact_resultsrecording fact
USING (select distinct f_rr.fact_resultsrecordingid, ifnull(q.QAPP_USERC1,'Not Set') AS QAPP_USERC1
from qapp q,fact_resultsrecording f_rr
where f_rr.dd_inspectionlotno = ifnull(q.QAPP_PRUEFLOS,0)
and f_rr.dd_currentnodeno = ifnull(q.QAPP_VORGLFNR,0)
and f_rr.dd_samplenumber = ifnull(q.QAPP_PROBENR,0)
and f_rr.dd_userfieldinsppoint <> ifnull(q.QAPP_USERC1,'Not Set')) src
ON fact.fact_resultsrecordingid = src.fact_resultsrecordingid
WHEN MATCHED THEN UPDATE
SET fact.dd_userfieldinsppoint = QAPP_USERC1,
dw_update_date = CURRENT_TIMESTAMP;

/* Liviu Ionescu - add Origin of Results Data (Inspection Sample) Description -  BI-3552 */
update fact_resultsrecording f_rr
set f_rr.dd_OrgResultDataShortText = ifnull(tq.TQ73T_ERDATHETXT,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from TQ73T tq, fact_resultsrecording f_rr
where f_rr.dd_originresultsset_qasr = ifnull(tq.TQ73T_QERGDATH,'Not Set')
and f_rr.dd_OrgResultDataShortText <> ifnull(tq.TQ73T_ERDATHETXT,'Not Set');


/*START BI-5163 Alin*/

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateiduserstcrtdlastsetresrec = f_il.dim_dateiduserstcrtdlastset,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_dateiduserstcrtdlastsetresrec <> f_il.dim_dateiduserstcrtdlastset;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateiduserstQCCOlastsetresrec = f_il.dim_dateiduserstqccolastset,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_dateiduserstQCCOlastsetresrec <> f_il.dim_dateiduserstqccolastset;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateiduserstSAMRlastsetresrec = f_il.dim_dateiduserstsamrlastset,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_dateiduserstSAMRlastsetresrec <> f_il.dim_dateiduserstsamrlastset;

/* logic for dim_dateiduserstcrtdlastsetresrec, dim_dateiduserstQCCOlastsetresrec, dim_dateiduserstSAMRlastsetresrec changed as req in APP-4434

dim_dateiduserstSAMRlastsetresrec
UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateiduserstcrtdlastsetresrec= ie.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date ie, dim_plant dp, JCDS_QALS j, fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ie.DateValue = ifnull(QALS_PASTRTERM,'0001-01-01')
AND ie.CompanyCode = dp.CompanyCode
AND dp.plantcode = ifnull(q.QALS_WERK, 'Not Set')
and ie.plantcode_factory = dp.plantcode
and j.JCDS_OBJNR = q.QALS_OBJNR
AND j.JCDS_STAT = 'E0001'
and j.jcds_chgnr = '1'
AND f_rr.dim_dateiduserstcrtdlastsetresrec <> ie.dim_dateid

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateiduserstQCCOlastsetresrec= ie.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date ie, dim_plant dp, JCDS_QALS j, fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ie.DateValue = ifnull(QALS_PASTRTERM,'0001-01-01')
AND ie.CompanyCode = dp.CompanyCode
AND dp.plantcode = ifnull(q.QALS_WERK, 'Mot Set')
and ie.plantcode_factory = dp.plantcode
and j.JCDS_OBJNR = q.QALS_OBJNR
AND j.JCDS_STAT = 'E0004'
and j.jcds_chgnr = '1'
AND f_rr.dim_dateiduserstQCCOlastsetresrec <> ie.dim_dateid

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateiduserstSAMRlastsetresrec= ie.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date ie, dim_plant dp, JCDS_QALS j, fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ie.DateValue = ifnull(QALS_PASTRTERM,'0001-01-01')
AND ie.CompanyCode = dp.CompanyCode
AND dp.plantcode = ifnull(q.QALS_WERK, 'Mot Set')
and ie.plantcode_factory = dp.plantcode
and j.JCDS_OBJNR = q.QALS_OBJNR
AND j.JCDS_STAT = 'E0002'
and j.jcds_chgnr = '1'
AND f_rr.dim_dateiduserstSAMRlastsetresrec <> ie.dim_dateid
*/

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidinspectionactivedate= ie.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date ie, dim_plant dp, JCDS_QALS j, fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ie.DateValue = ifnull(QALS_PASTRTERM,'0001-01-01')
AND ie.CompanyCode = dp.CompanyCode
AND dp.plantcode = ifnull(q.QALS_WERK, 'Mot Set')
and ie.plantcode_factory = dp.plantcode
and j.JCDS_OBJNR = q.QALS_OBJNR
AND j.JCDS_STAT = 'I0212'
and j.jcds_chgnr = '1'
AND f_rr.dim_dateidinspectionactivedate <> ie.dim_dateid;

merge into fact_resultsrecording f using
(
select  fact_resultsrecordingid, upd.dim_dateid
from dim_date dinspsd, fact_resultsrecording f_rr, dim_plant p, dim_date upd
where p.dim_plantid = f_rr.dim_plantid 
AND dinspsd.dim_dateid = f_rr.dim_dateidinspectionstart
AND upd.datevalue = cast(CASE WHEN dinspsd.DateValue = '9999-12-31' THEN dinspsd.DateValue ELSE dinspsd.DateValue + ct_averageinspectionduration + 11 END as date)
and upd.plantcode_factory = p.plantcode
and upd.companycode = p.companycode
) t
on f.fact_resultsrecordingid = t.fact_resultsrecordingid
when matched then 
update set f.dim_dateidjtorigqcco = t.dim_dateid
where f.dim_dateidjtorigqcco <> t.dim_dateid;

merge into fact_resultsrecording f using
(
select  fact_resultsrecordingid, upd.dim_dateid
from dim_date dinsped, fact_resultsrecording f_rr, dim_plant p, dim_date upd
where p.dim_plantid = f_rr.dim_plantid 
AND dinsped.dim_dateid = f_rr.dim_dateidinspectionend
AND upd.datevalue = cast(CASE WHEN dinsped.DateValue = '0001-01-01' THEN dinsped.DateValue ELSE dinsped.DateValue - 14 END as date)
and upd.plantcode_factory = p.plantcode
and upd.companycode = p.companycode
) t
on f.fact_resultsrecordingid = t.fact_resultsrecordingid
when matched then 
update set f.dim_dateidjtplanqcco = t.dim_dateid
where f.dim_dateidjtplanqcco <> t.dim_dateid;

/*END BI-5163 Alin*/

/*start APP-6319 Alin Gh 22 may 2017*/
UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidusagedecisionmade = f_il.dim_dateidusagedecisionmade,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_dateidusagedecisionmade <> f_il.dim_dateidusagedecisionmade;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidinspectionactivedt = f_il.dim_dateidinspectionactivedt,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_dateidinspectionactivedt <> f_il.dim_dateidinspectionactivedt;
/*end APP-6319 Alin Gh 22 may 2017*/

/* 4 Dec 2017 Georgiana Changes, adding Date of Manufacture form Batch according to APP-8238*/
merge into fact_resultsrecording f
using(
select distinct f.fact_resultsrecordingid,dt.dim_dateid from fact_resultsrecording f
inner join  dim_batch b on f.dim_batchid=b.dim_batchid
inner join dim_plant pl on  f.dim_plantid=pl.dim_plantid
inner join  dim_date dt on pl.companycode=dt.companycode
and ifnull(dt.datevalue,'0001-01-01')=ifnull(b.dateofmanufacture,'0001-01-01')
and ifnull(pl.plantcode,'Not Set')=ifnull(dt.plantcode_factory,'Not Set')
where dim_dateiddateofmanufacture <> ifnull(dt.dim_dateid,1)) t
on t.fact_resultsrecordingid=f.fact_resultsrecordingid
when matched then update set f.dim_dateiddateofmanufacture = ifnull(t.dim_dateid,1) ;

/* Andrei APP-9336 */
/*
UPDATE fact_resultsrecording f_rr
SET f_rr.dim_dateidgrdutedate= ie.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM QALS q, dim_date ie, dim_plant dp,fact_resultsrecording f_rr
WHERE  f_rr.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
AND ie.DateValue = add_days(ifnull(QALS_PASTRTERM,'0001-01-01'),7)
AND ie.CompanyCode = dp.CompanyCode
and ie.plantcode_factory = dp.plantcode 
AND dp.plantcode = ifnull(q.QALS_WERK, 'Not Set')
AND f_rr.dim_dateidgrdutedate <> ie.dim_dateid */

drop table if exists tmp_new_seq_gr_date;
create table tmp_new_seq_gr_date as 
select fact_resultsrecordingid, f.dim_dateidinspectionstart,  dt.datevalue,
/*dd_inspectionlotno, d.batchnumber, dt.plantcode, dt.companycode, 
d.lastgoodreceipt, grprocessingtime, dt.businessdaysseqno old_seq*/
  dt.plantcode_factory, dt.companycode,  (dt.businessdaysseqno + 5) as new_seq
from
fact_resultsrecording f 
inner join dim_batch d
on f.dim_batchid = d.dim_batchid
inner join dim_part dp
on f.dim_partid = dp.dim_partid
inner join dim_plant dpl
on f.dim_plantid = dpl.dim_plantid
inner join dim_date dt
on f.dim_dateidinspectionstart = dt.dim_dateid
WHERE dpl.plantcode not in (select distinct plantcode from newiteminstockbrinit);

merge into fact_resultsrecording f
using(
select distinct sq.fact_resultsrecordingid, sq.datevalue,
case when sq.datevalue = '0001-01-01' then 1
else
min(dt.dim_dateid) end as dim_dateidgrdutedate
/*there are multiple dates that match the same businessdaysseqno because public holidays have the same businessdaysseqno
as the previous working day*/
from tmp_new_seq_gr_date sq 
inner join dim_date dt
on dt.plantcode_factory = sq.plantcode_factory
and dt.companycode = sq.companycode
and dt.businessdaysseqno = sq.new_seq
group by sq.fact_resultsrecordingid, sq.datevalue
) t
on t.fact_resultsrecordingid = f.fact_resultsrecordingid
when matched then update set
f.dim_dateidgrdutedate = t.dim_dateidgrdutedate;

drop table if exists tmp_new_seq_gr_date;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_targetuddateid = f_il.dim_targetuddateid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_targetuddateid <> f_il.dim_targetuddateid;

UPDATE fact_resultsrecording f_rr
SET f_rr.dim_targetqccodateid = f_il.dim_targetqccodateid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_inspectionlot f_il,fact_resultsrecording f_rr
where f_il.dd_inspectionlotno = cast(f_rr.dd_inspectionlotno as varchar(20))
and f_rr.dim_targetqccodateid <> f_il.dim_targetqccodateid;

/*Ionel 08th of June 2018, APP9765 Work Center Plant added*/
merge into fact_resultsrecording rr using
( select fact_resultsrecordingid, p.dim_plantid 
  from fact_resultsrecording rr
	inner join dim_workcenter w on rr.dim_workcenterid = w.dim_workcenterid
	inner join dim_plant p on p.plantcode = w.plant
) upd
on upd.fact_resultsrecordingid = rr.fact_resultsrecordingid
when matched then update
set dim_plantidworkcenter = upd.dim_plantid
where dim_plantidworkcenter <> upd.dim_plantid;

/* Andrei 11.09.2018 APP-9336 - Add Posting Date */
merge into fact_resultsrecording f
using ( select distinct f.fact_resultsrecordingid,
                        d.dim_dateid
from fact_resultsrecording f, dim_date d, QALS q, MKPF m, dim_plant dp
where d.datevalue = ifnull(MKPF_BUDAT,'0001-01-01') 
and f.dd_inspectionlotno = ifnull(q.QALS_PRUEFLOS,'0')
and ifnull(q.QALS_MJAHR, 0001) = m.MKPF_MJAHR
and ifnull(q.QALS_MBLNR, 'Not Set')= m.MKPF_MBLNR
AND d.CompanyCode = dp.CompanyCode
AND dp.plantcode = ifnull(q.QALS_WERK, 'Not Set')
and d.plantcode_factory = dp.plantcode ) t
on f.fact_resultsrecordingid = t.fact_resultsrecordingid
when matched then update set f.dim_dateidpostingdate = ifnull(t.dim_dateid,1)
where f.dim_dateidpostingdate <> ifnull(t.dim_dateid,1);
