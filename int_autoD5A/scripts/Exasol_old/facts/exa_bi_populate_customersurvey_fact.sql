/* ################################################################################################################## */
/* */
/*   Script         : exa_bi_populate_fact_customersurvey */
/*   Author         : Andrei R */
/*   Created On     : 10 Mar 2017 */
/*   Modifications  : Madalina 5 May 2017 - adjusting script */
/*  				Grain: dim_surveyid, dd_customercode (token?), DIM_SURVEYQUESTIONSID, DD_SURVEYRESPONSE (answer) */
/*  */
/* ################################################################################################################## */

/* DELETE FROM SURVEYRESULTS- the delete stmt is performed within the python script */
Alter session set NLS_DATE_FORMAT='YYYYMMDD';

/* IMPORT INTO SURVEYRESULTS FROM CSV
AT DC1STGC1INT01CONNECTION
FILE '/home/fusionops/ispring_clustered_storage/configurations/MerckDE6/files/CustomerSurvey/RESULTS4.csv'
COLUMN SEPARATOR = ','
ROW SEPARATOR = 'CRLF'
ENCODING = 'ISO-8859-1'
SKIP = 1 */

/*SURVEYRESULTS contains at this point the questionid + subquestionid. Update the dim_questionid based on them, to insert the id into the fact. dd_questioncode won't be further used*/
/*update the survey code from SURVEYRESULTS based on the survey code from dim_questionid*/
update SURVEYRESULTS s
set s.dim_surveyquestionsid = ifnull(dq.dim_surveyquestionsid,1),
	s.SURVEYCODE = dq.SURVEYCODE
from SURVEYRESULTS s, dim_surveyquestions dq
where s.questioncode = dq.questioncode
	and s.subquestioncode = dq.subquestioncode;

/* Madalina 24 iul 2017 - recreate the fact for the surveys that exist in the staging tables */
drop table if exists tmp_FACT_CUSTOMERSURVEY;
create table tmp_FACT_CUSTOMERSURVEY as
	select * from FACT_CUSTOMERSURVEY;

delete from tmp_FACT_CUSTOMERSURVEY tmp
	where exists (select 1 from SURVEY s
								where tmp.dd_surveycode = s.surveycode);

/*insert into fact*/
delete from number_fountain m where m.table_name = 'FACT_CUSTOMERSURVEY';
insert into number_fountain
select 'FACT_CUSTOMERSURVEY',
ifnull(max(f.FACT_CUSTOMERSURVEYID),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from FACT_CUSTOMERSURVEY f;

INSERT INTO tmp_FACT_CUSTOMERSURVEY
(
	fact_customersurveyid,
	dd_surveycode,
	dd_customercode,
	DD_SURVEYRESULTCODEID,
	DD_SURVEYRESPONSE,
	dim_surveyid,
	dim_surveyquestionsid,
	dim_surveycustomerid,
	DIM_DATEIDSURVEYCOMPLETION,
	dd_status,
	dd_platform,
	dd_browser,
	dd_responseposition,
	dd_englishresponse,
	DW_INSERT_DATE,
	DW_UPDATE_DATE
)
SELECT
	(SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'FACT_CUSTOMERSURVEY') + row_number() over(order by '') as FACT_CUSTOMERSURVEYID,
	ifnull(surveycode,'Not Set') AS dd_surveycode,		/*for dim_surveyid*/
	IFNULL(customercode,'Not Set') as dd_customercode,  /*token (grain)*/
	IFNULL(SURVEYRESULTCODEID,'Not Set') AS DD_SURVEYRESULTCODEID, /*answer unique id - not used*/
	IFNULL(SURVEYRESPONSE,'Not Set') AS DD_SURVEYRESPONSE,  /*answer (grain)*/
	1 AS dim_surveyid, 	/*(grain)*/
	ifnull(dim_surveyquestionsid,1) AS DIM_SURVEYQUESTIONSID,  /*question (grain)*/
	1 AS DIM_SURVEYCUSTOMERID, 		/*not used?*/
	1 AS DIM_DATEIDSURVEYCOMPLETION,
	ifnull(status, 'Not Set') dd_status,
	ifnull(platform, 'Not Set') dd_platform,
	ifnull(browser, 'Not Set') dd_browser,
	ifnull(responseposition, 0) as dd_responseposition,
	ifnull(englishresponse, 'Not Set') as dd_englishresponse,
	current_timestamp AS DW_INSERT_DATE,
	current_timestamp AS DW_UPDATE_DATE
from SURVEYRESULTS S
 WHERE NOT EXISTS(SELECT 1  FROM tmp_FACT_CUSTOMERSURVEY F where
ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set') and
IFNULL(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set') and
/* IFNULL(SURVEYRESULTCODEID,'Not Set') = ifnull(f.DD_SURVEYRESULTCODEID,'Not Set') and */
ifnull(s.dim_surveyquestionsid,1) = ifnull(f.dim_surveyquestionsid,1) and
IFNULL(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set') );

/*fact updates */
/* update the survey id, based on dd_surveycode*/
update tmp_fact_customersurvey fcs
set fcs.dim_surveyid = ds.dim_surveyid
from tmp_fact_customersurvey fcs, dim_survey ds
where fcs.dd_surveycode = ds.surveycode
and fcs.dim_surveyid <> ds.dim_surveyid;

/*update survey completion date*/
merge into  tmp_fact_customersurvey f using
( select distinct fact_customersurveyid,  dd.dim_dateid
from tmp_fact_customersurvey f, SURVEYRESULTS s, dim_date dd
where ifnull(dd.datevalue,'0001-01-01') = ifnull(s.surveycompletiondate,'0001-01-01')
	and dd.plantcode_factory = 'Not Set'
	and dd.companycode = 'Not Set'
	and ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set')
	and ifnull(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set')
	and ifnull(s.dim_surveyquestionsid,1) = ifnull(f.dim_surveyquestionsid,1)
	and ifnull(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set')
) upd
on f.fact_customersurveyid = upd.fact_customersurveyid
when matched then update 
set DIM_DATEIDSURVEYCOMPLETION = upd.dim_dateid;

/*
update tmp_fact_customersurvey f
set DIM_DATEIDSURVEYCOMPLETION = dd.dim_dateid
from tmp_fact_customersurvey f, SURVEYRESULTS s, dim_date dd
where ifnull(dd.datevalue,'0001-01-01') = ifnull(s.surveycompletiondate,'0001-01-01')
	and dd.plantcode_factory = 'Not Set'
	and dd.companycode = 'Not Set'
	and ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set')
	and ifnull(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set')
	and ifnull(s.dim_surveyquestionsid,1) = ifnull(f.dim_surveyquestionsid,1)
	and ifnull(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set')*/

/*other survey info (status/browser/platform) cannot be changed*/
update tmp_fact_customersurvey f
set dd_status = ifnull(status, 'Not Set'),
	dd_platform = ifnull(platform, 'Not Set'),
	dd_browser = ifnull(browser, 'Not Set')
from tmp_fact_customersurvey f, SURVEYRESULTS s
where ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set')
	and ifnull(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set')
	and ifnull(s.dim_surveyquestionsid,1) = ifnull(f.dim_surveyquestionsid,1)
	and ifnull(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set');

/*update statistics*/
merge into tmp_FACT_CUSTOMERSURVEY fcs using
(
	select dim_surveyid, dim_surveyquestionsid, dd_surveyresponse, count(1) as ddstatistics
	from tmp_FACT_CUSTOMERSURVEY
	/* where dd_statistics = 'Not Set' */ /*only populate the statistics that are not brought directly from API*/
	group by dim_surveyid, dim_surveyquestionsid, dd_surveyresponse ) upd
on fcs.dim_surveyid = upd.dim_surveyid
	and fcs.dim_surveyquestionsid = upd.dim_surveyquestionsid
	and fcs.dd_surveyresponse = upd.dd_surveyresponse
when matched then update
set dd_statistics = upd.dd_surveyresponse || '-' || upd.ddstatistics;

/*populate the statistics that are brought directly from API*/
merge into tmp_FACT_CUSTOMERSURVEY fcs using
(
	select fact_customersurveyid, sq.dd_statistics as dd_statistics
	from tmp_FACT_CUSTOMERSURVEY fcs,  dim_surveyquestions dsq, SURVEYQUESTIONS sq
	where fcs.dim_surveyquestionsid = dsq.dim_surveyquestionsid
		and sq.questioncode = dsq.questioncode
		and sq.subquestioncode = dsq.subquestioncode
		and sq.dd_statistics is not null
) upd
on fcs.fact_customersurveyid = upd.fact_customersurveyid
when matched then update
set fcs.dd_statistics = ifnull(upd.dd_statistics, 'Not Set');

truncate table FACT_CUSTOMERSURVEY;
insert into FACT_CUSTOMERSURVEY
(FACT_CUSTOMERSURVEYID,
DIM_SURVEYID,
DIM_SURVEYQUESTIONSID,
DIM_SURVEYCUSTOMERID,
DD_SURVEYRESULTCODEID,
DD_SURVEYRESPONSE,
DIM_DATEIDSURVEYCOMPLETION,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_SURVEYCODE,
DD_CUSTOMERCODE,
DD_STATUS,
DD_PLATFORM,
DD_BROWSER,
DD_STATISTICS,
dd_responseposition,
dd_englishresponse)
	select
FACT_CUSTOMERSURVEYID,
DIM_SURVEYID,
DIM_SURVEYQUESTIONSID,
DIM_SURVEYCUSTOMERID,
DD_SURVEYRESULTCODEID,
DD_SURVEYRESPONSE,
DIM_DATEIDSURVEYCOMPLETION,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_SURVEYCODE,
DD_CUSTOMERCODE,
DD_STATUS,
DD_PLATFORM,
DD_BROWSER,
DD_STATISTICS,
dd_responseposition,
dd_englishresponse
 from tmp_FACT_CUSTOMERSURVEY;


/* Madalina 19 Mar 2018 - APP-6054 - the following attributes will only be updated for specific hardcoded questions, for a single survey */
/* merge into  fact_customersurvey f using
(  select distinct
	  case when DD_SURVEYRESPONSE like 'Directly from Merck/ MSD Animal Health%' then 'Direct'
			when DD_SURVEYRESPONSE like 'A third party distributor%' then '3rd Party Distributor'
			when DD_SURVEYRESPONSE like 'A combination of both distributors and direct from Merck/MSD Animal Health%' then 'Both'
			when DD_SURVEYRESPONSE like 'Unsure%' then 'Unsure'
			else 'Unsure'
	   end dd_supplymodel, DD_CUSTOMERCODE, f.DIM_SURVEYID as DIM_SURVEYID
	from  fact_customersurvey f 
	inner join dim_surveyquestions q on f.dim_surveyquestionsid = q.dim_surveyquestionsid
	inner join DIM_SURVEY d on f.DIM_SURVEYID = d.DIM_SURVEYID
	where d.surveycode in ('555281','561931','561453','561350','561932','563478','563482') 
	and q.QUESTIONDESCRIPTION like '%Through which source(s) do you currently receive Merck/ MSD Animal Health products?%'
) upd
on f.DD_CUSTOMERCODE = upd.DD_CUSTOMERCODE
and f.DIM_SURVEYID = upd.DIM_SURVEYID
when matched then update 
set f.dd_supplymodel = upd.dd_supplymodel */

/* Only update for the surveys from SurveyRegionsCustomerSurvey */
merge into  fact_customersurvey f using
(  select distinct
	  case when dd_responseposition = 0 then 'Direct'
			when dd_responseposition = 1 then '3rd Party Distributor'
			when dd_responseposition = 2 then 'Both'
			else 'Unsure'
	   end dd_supplymodel, DD_CUSTOMERCODE, f.DIM_SURVEYID as DIM_SURVEYID
	from  fact_customersurvey f 
	inner join dim_surveyquestions q on f.dim_surveyquestionsid = q.dim_surveyquestionsid
	inner join dim_survey d on f.dim_surveyid = d.dim_surveyid
	inner join SurveyRegionsCustomerSurvey s on s.surveycode = d.surveycode
	/* where d.surveycode in ('555281','561931','561453','561350','561932','563478','563482') */
	and q.nrcrt = 2
) upd
on f.DD_CUSTOMERCODE = upd.DD_CUSTOMERCODE
and f.DIM_SURVEYID = upd.DIM_SURVEYID
when matched then update 
set f.dd_supplymodel = upd.dd_supplymodel;

/*Madalina 6 Jun 18 - New mapping for Market(ComOps) and Region - APP-6054 
Instead of maching the Market with RegionsCustomerSurvey info, use the mapping from SurveyRegionsCustomerSurvey*/

/* merge into  fact_customersurvey f using
(  select distinct countryname, DD_CUSTOMERCODE, f.DIM_SURVEYID as DIM_SURVEYID
	from  fact_customersurvey f 
	inner join dim_surveyquestions q on f.dim_surveyquestionsid = q.dim_surveyquestionsid
	inner join DIM_SURVEY d on f.DIM_SURVEYID = d.DIM_SURVEYID
	inner join RegionsCustomerSurvey r on r.REGIONCODE = f.DD_SURVEYRESPONSE
	where d.surveycode in ('555281','561931','561453','561932','563478','563482','561350') 
	and q.nrcrt = 4
) upd
on f.DD_CUSTOMERCODE = upd.DD_CUSTOMERCODE
and f.DIM_SURVEYID = upd.DIM_SURVEYID
when matched then update 
set f.dd_market = upd.countryname */


merge into  fact_customersurvey f using
(  select distinct ComOps, DD_CUSTOMERCODE, f.DIM_SURVEYID as DIM_SURVEYID
	from  fact_customersurvey f 
	inner join DIM_SURVEY d on f.DIM_SURVEYID = d.DIM_SURVEYID
	inner join SurveyRegionsCustomerSurvey r on r.surveycode = d.surveycode
) upd
on f.DD_CUSTOMERCODE = upd.DD_CUSTOMERCODE
and f.DIM_SURVEYID = upd.DIM_SURVEYID
when matched then update 
set f.dd_market = upd.ComOps;

merge into  fact_customersurvey f using
(  select distinct region, DD_CUSTOMERCODE, f.DIM_SURVEYID as DIM_SURVEYID
	from  fact_customersurvey f 
	inner join DIM_SURVEY d on f.DIM_SURVEYID = d.DIM_SURVEYID
	inner join SurveyRegionsCustomerSurvey r on r.surveycode = d.surveycode
) upd
on f.DD_CUSTOMERCODE = upd.DD_CUSTOMERCODE
and f.DIM_SURVEYID = upd.DIM_SURVEYID
when matched then update 
set f.dd_region = upd.region;


/* Only update for the surveys from SurveyRegionsCustomerSurvey */
merge into  fact_customersurvey f using
(  select distinct DD_SURVEYRESPONSE, DD_CUSTOMERCODE, f.DIM_SURVEYID as DIM_SURVEYID, dd_englishresponse
	from  fact_customersurvey f 
	inner join dim_surveyquestions q on f.dim_surveyquestionsid = q.dim_surveyquestionsid
	inner join dim_survey d on f.dim_surveyid = d.dim_surveyid
	inner join SurveyRegionsCustomerSurvey s on s.surveycode = d.surveycode
	/* where d.surveycode in ('555281','561931','561453','561350','561932','563478','563482') */
	/* and q.QUESTIONDESCRIPTION like '%With which of the following species do you or your business primarily work?%' */
	and q.nrcrt = 3
) upd
on f.DD_CUSTOMERCODE = upd.DD_CUSTOMERCODE
and f.DIM_SURVEYID = upd.DIM_SURVEYID
when matched then update 
set f.dd_species = upd.dd_englishresponse;

/* merge into  fact_customersurvey f using
(  select distinct REGIONNAME, DD_CUSTOMERCODE, f.DIM_SURVEYID as DIM_SURVEYID
	from  fact_customersurvey f 
	inner join dim_surveyquestions q on f.dim_surveyquestionsid = q.dim_surveyquestionsid
	inner join DIM_SURVEY d on f.DIM_SURVEYID = d.DIM_SURVEYID
	inner join RegionsCustomerSurvey r on r.REGIONCODE = f.DD_SURVEYRESPONSE
	where d.surveycode in ('555281','561931','561453','561932','563478','563482','561350') 
	and q.nrcrt = 4
) upd
on f.DD_CUSTOMERCODE = upd.DD_CUSTOMERCODE
and f.DIM_SURVEYID = upd.DIM_SURVEYID
when matched then update 
set f.dd_region = upd.REGIONNAME */


/* Madalina 4 Apr 2018 - adding a new dimension, that will contain the Species answer, split into distinct rows */
delete from dim_Splitspecies;

delete from number_fountain m where m.table_name = 'dim_Splitspecies';
insert into number_fountain
select 'dim_Splitspecies',
ifnull(max(f.dim_SplitspeciesID),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_Splitspecies f;


/*APP-6054 - Octavian S - 23 AUG 2018 - Adde Italy survey */
insert into dim_Splitspecies (dim_SplitSpeciesId, DIM_SURVEYID, dd_customercode, SplitSpeciesResponse, SplitSpeciesSpeciesResponseNo)
select (SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'dim_Splitspecies') + row_number() over(order by '') as dim_speciesid,
t.* from
	(select distinct  f.DIM_SURVEYID, f.dd_customercode, dsr.EnglishResponse, row_number() over (partition by f.DIM_SURVEYID, f.dd_customercode, f.dim_surveyquestionsid order by dsr.EnglishResponse) as rn
		from  fact_customersurvey f 
		inner join dim_surveyquestions q on f.dim_surveyquestionsid = q.dim_surveyquestionsid
		inner join DIM_SURVEY d on f.DIM_SURVEYID = d.DIM_SURVEYID
		inner join SurveyRegionsCustomerSurvey s on s.surveycode = d.surveycode
		inner join dim_surveyresults_all dsr on f.dim_surveyquestionsid = dsr.dim_surveyquestionsid and f.dim_surveyid = dsr.dim_surveyid and f.dd_customercode = dsr.CUSTOMERCODE
		where --d.surveycode in ('555281','561931','561453','561350','561932','563478','563482','582213','581761','584396','584395','582332','581777','581767','581760','585356','587287','586824') 
		/* and q.QUESTIONDESCRIPTION like '%With which of the following species do you or your business primarily work?%' */
		--and 
		q.nrcrt = 3
	) t;



/*
merge into fact_customersurvey f using
( select  fact_customersurveyid, 
		( COUNT( CASE WHEN (CASE WHEN LEFT( dsq.NPSINDICATOR ,3) = 'Yes'
			THEN
			CASE WHEN f_cs.DD_SURVEYRESPONSE = '10'
			OR f_cs.DD_SURVEYRESPONSE = '9'
			THEN 'Promoter'
			WHEN f_cs.DD_SURVEYRESPONSE = '8' 
			OR f_cs.DD_SURVEYRESPONSE = '7'
			THEN 'Passive'
			ELSE 'Detractor'
			END
			ELSE ' '
		END) = 'Promoter' THEN 1 END) -
		COUNT( CASE WHEN (CASE WHEN LEFT( dsq.NPSINDICATOR ,3) = 'Yes'
			THEN
			CASE WHEN f_cs.DD_SURVEYRESPONSE = '10'
			OR f_cs.DD_SURVEYRESPONSE = '9'
			THEN 'Promoter'
			WHEN f_cs.DD_SURVEYRESPONSE = '8' 
			OR f_cs.DD_SURVEYRESPONSE = '7'
			THEN 'Passive'
			ELSE 'Detractor'
			END
			ELSE ' '
			END) = 'Detractor' THEN 1 END) ) * 100 /
		CASE WHEN COUNT( CASE WHEN LEFT( (dsq.NPSINDICATOR) ,3)='Yes' THEN 1 END) = 0
		THEN 1
		ELSE COUNT( CASE WHEN LEFT( (dsq.NPSINDICATOR) ,3)='Yes' THEN 1 END)
		END as ct_npsScore
	from fact_customersurvey f_cs
	inner join dim_surveyquestions dsq on dsq.dim_surveyquestionsid = f_cs.dim_surveyquestionsid
	group by fact_customersurveyid
) upd 
on f.fact_customersurveyid = upd.fact_customersurveyid
when matched then update
set f.ct_npsScore = upd.ct_npsScore

update fact_customersurvey
set dd_npsdefinition = 
	case when ct_npsScore between 1 and 49 then 'Good'
		when ct_npsScore between 50 and 69 then 'Excellent'
		when ct_npsScore between 70 and 100 then 'World Class'
		else 'Poor'
	end
*/

/* Adding a plant - to use the related Plant Country in Maps report */
/* Madalina - 5 Jun 18 - APP-6054 - Don't match the response for q4 - use SurveyRegionsCustomerSurvey and RegionsCustomerSurvey instead*/

/* merge into fact_customersurvey f using
( select distinct dd_customercode,
	first_value(p.dim_plantid) over (partition by country order by p.dim_plantid) as dim_plantid
    from  fact_customersurvey f 
	inner join dim_surveyquestions q on f.dim_surveyquestionsid = q.dim_surveyquestionsid
	inner join dim_plant p on p.country = f.dd_surveyresponse
	inner join dim_survey d on f.dim_surveyid = d.dim_surveyid
	where d.surveycode in ('555281','561931','561453','561932','563478','563482','561350') 
	and q.nrcrt = 4
) upd
on f.dd_customercode = upd.dd_customercode
when matched then update 
set f.dim_plantid = upd.dim_plantid */

merge into fact_customersurvey f using
( select distinct dd_customercode,
	first_value(p.dim_plantid) over (partition by country order by p.dim_plantid) as dim_plantid
    from  fact_customersurvey f 
    inner join RegionsCustomerSurvey r on r.countryname = dd_market
	inner join dim_plant p on p.country = r.regioncode
	inner join dim_survey d on f.dim_surveyid = d.dim_surveyid
	inner join SurveyRegionsCustomerSurvey s on s.surveycode = d.surveycode
) upd
on f.dd_customercode = upd.dd_customercode
when matched then update 
set f.dim_plantid = upd.dim_plantid;

/*Madalina - 29 May 18 - Mask some responses - APP-6054*/
update fact_customersurvey f
set dd_surveyresponse = 'Not Set' 
from fact_customersurvey f
    inner join dim_surveyquestions q on q.dim_surveyquestionsid = f.dim_surveyquestionsid
where q.nrcrt = 10;

update fact_customersurvey f
set dd_statistics = 'Not Set' 
from fact_customersurvey f
    inner join dim_surveyquestions q on q.dim_surveyquestionsid = f.dim_surveyquestionsid
where q.nrcrt = 10;

/* END APP-6054 */

/*INSERT INTO FACT_CUSTOMERSURVEY (fact_customersurveyid,dd_surveycode,
dd_customercode, DD_SURVEYRESULTCODEID,dd_questioncode,DD_SURVEYRESPONSE,dim_surveyid,dim_surveyquestionsid,dim_surveycustomerid, DIM_DATEIDSURVEYCOMPLETION,DW_INSERT_DATE)
SELECT  (SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'FACT_CUSTOMERSURVEY') + row_number() over(order by '')
as FACT_CUSTOMERSURVEYID,
ifnull(surveycode,'Not Set') AS DD_SURVEYCODE,
IFNULL(customercode,'Not Set') as dd_customercode,
 IFNULL(SURVEYRESULTCODEID,'Not Set') AS DD_SURVEYRESULTCODEID,
ifnull(QUESTIONCODE,'Not Set') as dd_questioncode,
IFNULL(SURVEYRESPONSE,'Not Set') AS DD_SURVEYRESPONSE,
1 AS DIM_SURVEYID,
1 AS DIM_SURVEYQUESTIONSID,
1 AS DIM_SURVEYCUSTOMERID,
1 AS DIM_DATEIDSURVEYCOMPLETION ,
current_timestamp AS DW_INSERT_DATE
 from SURVEYRESULTS S
 WHERE NOT EXISTS(SELECT 1  FROM FACT_CUSTOMERSURVEY F where
ifnull(surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set') and
IFNULL(customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set') and
 IFNULL(SURVEYRESULTCODEID,'Not Set') = ifnull(f.DD_SURVEYRESULTCODEID,'Not Set') and
ifnull(QUESTIONCODE,'Not Set') = dd_questioncode and
IFNULL(SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set') )


merge into FACT_CUSTOMERSURVEY f
using(select distinct F.FACT_CUSTOMERSURVEYID,D.DIM_SURVEYID
FROM FACT_CUSTOMERSURVEY F, DIM_SURVEY D, SURVEYRESULTS S
WHERE IFNULL(D.SURVEYCODE,'Not Set') = ifnull(S.SURVEYCODE, 'Not Set')
 AND ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set') and
IFNULL(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set') and
 IFNULL(s.SURVEYRESULTCODEID,'Not Set') = ifnull(f.DD_SURVEYRESULTCODEID,'Not Set') and
ifnull(s.QUESTIONCODE,'Not Set') = dd_questioncode and
IFNULL(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set')
and F.DIM_SURVEYID <> IFNULL(D.DIM_SURVEYID,1)) t
on t.FACT_CUSTOMERSURVEYid=f.FACT_CUSTOMERSURVEYid
when matched then update set F.DIM_SURVEYID = IFNULL(t.DIM_SURVEYID,1),
DW_UPDATE_DATE = CURRENT_TIMESTAMP


merge into FACT_CUSTOMERSURVEY f
using(select distinct F.FACT_CUSTOMERSURVEYID,D.DIM_SURVEYQUESTIONSID
FROM FACT_CUSTOMERSURVEY F, DIM_SURVEYQUESTIONS D, SURVEYRESULTS S
WHERE IFNULL(D.QUESTIONCODE,'Not Set') = ifnull(S.QUESTIONCODE, 'Not Set')
   AND ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set') and
IFNULL(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set') and
 IFNULL(s.SURVEYRESULTCODEID,'Not Set') = ifnull(f.DD_SURVEYRESULTCODEID,'Not Set') and
IFNULL(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set') and
ifnull(s.QUESTIONCODE,'Not Set') = dd_questioncode
 and F.DIM_SURVEYQUESTIONSID <> IFNULL(D.DIM_SURVEYQUESTIONSID,1)) t
on t.FACT_CUSTOMERSURVEYid=f.FACT_CUSTOMERSURVEYid
when matched then update set  F.DIM_SURVEYQUESTIONSID = IFNULL(t.DIM_SURVEYQUESTIONSID,1),
DW_UPDATE_DATE = CURRENT_TIMESTAMP


UPDATE FACT_CUSTOMERSURVEY F
SET F.DIM_SURVEYCUSTOMERID = IFNULL(D.DIM_SURVEYCUSTOMERID,1),
DW_UPDATE_DATE = CURRENT_TIMESTAMP
FROM FACT_CUSTOMERSURVEY F, DIM_SURVEYCUSTOMER D, SURVEYRESULTS S
WHERE IFNULL(D.CUSTOMERCODE,'Not Set') = ifnull(S.CUSTOMERCODE, 'Not Set')
   AND ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set') and
IFNULL(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set') and
 IFNULL(s.SURVEYRESULTCODEID,'Not Set') = ifnull(f.DD_SURVEYRESULTCODEID,'Not Set') and
IFNULL(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set') and
ifnull(s.QUESTIONCODE,'Not Set') = dd_questioncode
 and F.DIM_SURVEYCUSTOMERID <> IFNULL(D.DIM_SURVEYCUSTOMERID,1)


merge into FACT_CUSTOMERSURVEY F
using (select distinct F.FACT_CUSTOMERSURVEYID,max(D.DIM_DATEID) as DIM_DATEID
FROM FACT_CUSTOMERSURVEY F, DIM_DATE D, SURVEYRESULTS S
WHERE IFNULL(D.DATEVALUE,'0001-01-01') = ifnull(S.SURVEYCOMPLETIONDATE, '0001-01-01')
   AND ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set') and
IFNULL(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set') and
 IFNULL(s.SURVEYRESULTCODEID,'Not Set') = ifnull(f.DD_SURVEYRESULTCODEID,'Not Set') and
IFNULL(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set') and
ifnull(s.QUESTIONCODE,'Not Set') = dd_questioncode
 and F.DIM_DATEIDSURVEYCOMPLETION <> IFNULL(D.DIM_DATEID,1)
group by f.FACT_CUSTOMERSURVEYID ) t
on t.FACT_CUSTOMERSURVEYID=f.FACT_CUSTOMERSURVEYID
when matched then update set F.DIM_DATEIDSURVEYCOMPLETION = IFNULL(t.DIM_DATEID,1),
DW_UPDATE_DATE = CURRENT_TIMESTAMP


UPDATE FACT_CUSTOMERSURVEY F
SET F.DD_SURVEYRESPONSE = IFNULL(SURVEYRESPONSE,'Not Set'),
DW_UPDATE_DATE = CURRENT_TIMESTAMP
FROM FACT_CUSTOMERSURVEY F,  SURVEYRESULTS S
WHERE     ifnull(s.surveycode,'Not Set') = ifnull(f.DD_SURVEYCODE,'Not Set') and
IFNULL(s.customercode,'Not Set') = ifnull(f.dd_customercode,'Not Set') and
 IFNULL(s.SURVEYRESULTCODEID,'Not Set') = ifnull(f.DD_SURVEYRESULTCODEID,'Not Set') and
IFNULL(s.SURVEYRESPONSE,'Not Set') = ifnull(f.DD_SURVEYRESPONSE,'Not Set') and
ifnull(s.QUESTIONCODE,'Not Set') = dd_questioncode
 and F.DD_SURVEYRESPONSE <> IFNULL(SURVEYRESPONSE,'Not Set')

 */
