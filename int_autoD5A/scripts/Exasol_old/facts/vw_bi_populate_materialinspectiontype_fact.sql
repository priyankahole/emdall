
/* ################################################################################################################## */
/* */
/*   Author         : Georgiana */
/*   Created On     : 4 Apr 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   4 Apr 2016      Georgiana	1.0  		  First draft */
/******************************************************************************************************************/


drop table if exists tmp_fact_materialinspectiontype;
create table tmp_fact_materialinspectiontype
LIKE fact_materialinspectiontype INCLUDING IDENTITY INCLUDING DEFAULTS;

ALTER TABLE tmp_fact_materialinspectiontype ADD PRIMARY KEY (fact_materialinspectiontypeid);

delete from number_fountain m where m.table_name = 'tmp_fact_materialinspectiontype';
insert into number_fountain
select 'tmp_fact_materialinspectiontype',
ifnull(max(f.fact_materialinspectiontypeid),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_materialinspectiontype f;

INSERT INTO tmp_fact_materialinspectiontype (
 fact_materialinspectiontypeid,
 dd_inspectiontype,
 dd_materialno,
 dd_plant,
 ct_averageinspduration,
 dd_posttoinspstock,
 dd_insptypematerialisactive,
 dd_controlofinsplotcreation,
 dd_prefferedinsptype,
 dd_inspectionwithtasklist
)
 SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_materialinspectiontype') + row_number() over(order by '') AS fact_materialinspectiontypeid,
ifnull(QMAT_ART, 'Not Set') as dd_inspectiontype,
ifnull(QMAT_MATNR, 'Not Set') as dd_materialno,
ifnull(QMAT_WERKS, 'Not Set') as dd_plant,
ifnull(QMAT_MPDAU, 0) as ct_averageinspduration,
ifnull(QMAT_INSMK, 'Not Set') as dd_posttoinspstock,
ifnull(QMAT_AKTIV, 'Not Set') as dd_insptypematerialisactive,
ifnull(QMAT_CHG, 'Not Set') as dd_controlofinsplotcreation,
ifnull(QMAT_APA, 'Not Set') as dd_prefferedinsptype,
ifnull(QMAT_PPL, 'Not Set') as dd_inspectionwithtasklist
FROM
QMAT q;

update tmp_fact_materialinspectiontype m
set m.dim_partid = dp.dim_partid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from QMAT q, dim_part dp, tmp_fact_materialinspectiontype m
where
dd_inspectiontype = ifnull(QMAT_ART, 'Not Set')
AND dd_materialno = ifnull(QMAT_MATNR, 'Not Set')
AND dd_plant = ifnull(QMAT_WERKS, 'Not Set')
AND ifnull(QMAT_MATNR, 'Not Set') = dp.partnumber
AND ifnull(QMAT_WERKS, 'Not Set') = dp.plant
AND m.dim_partid <> dp.dim_partid;


update tmp_fact_materialinspectiontype m
set m.dim_plantid = dp.dim_plantid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from QMAT q, dim_plant dp, tmp_fact_materialinspectiontype m
where
dd_inspectiontype = ifnull(QMAT_ART, 'Not Set')
AND dd_materialno = ifnull(QMAT_MATNR, 'Not Set')
AND dd_plant = ifnull(QMAT_WERKS, 'Not Set')
AND ifnull(QMAT_WERKS, 'Not Set') = dp.plantcode
AND m.dim_plantid <> dp.dim_plantid;

update tmp_fact_materialinspectiontype m
set m.dim_inspectiontypeid = ityp.dim_inspectiontypeid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from QMAT q, dim_inspectiontype ityp, tmp_fact_materialinspectiontype m
where
dd_inspectiontype = ifnull(QMAT_ART, 'Not Set')
AND dd_materialno = ifnull(QMAT_MATNR, 'Not Set')
AND dd_plant = ifnull(QMAT_WERKS, 'Not Set')
AND ityp.InspectionTypeCode = ifnull(QMAT_ART, 'Not Set')
AND m.dim_inspectiontypeid <> ityp.dim_inspectiontypeid;

/*13 May 2016 Georgiana Changes  Adding dd_defaultstatusreleaseint according to BI-2708*/
update tmp_fact_materialinspectiontype ft
SET ft.dd_defaultstatusreleaseint = ifnull(concat(z.atwrt,' (',c.cawnt_atwtb,')'),'')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from CAWNT c,
Z1QM_MAT_REL_ST z, tmp_fact_materialinspectiontype ft
where c.cawn_atwrt=z.ATWRT
and ft.dd_materialno = ifnull(z.MATNR, 'Not Set')
AND ft.dd_plant = ifnull(z.WERKS, 'Not Set')
and cawn_atinn=47
and ft.dd_defaultstatusreleaseint <> ifnull(concat(z.atwrt,' (',c.cawnt_atwtb,')'),'');

/*13 May 2016 End of Changes*/

TRUNCATE TABLE fact_materialinspectiontype;
INSERT into fact_materialinspectiontype
select * from tmp_fact_materialinspectiontype;

DROP TABLE IF EXISTS tmp_fact_materialinspectiontype;
