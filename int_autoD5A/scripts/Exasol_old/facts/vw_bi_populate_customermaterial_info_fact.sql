
/* ################################################################################################################## */
/* */
/*   Author         : Georgiana */
/*   Created On     : 24 Aug 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   24 Aug 2016      Gergiana	1.0  		  First draft */

/******************************************************************************************************************/


delete from number_fountain m where m.table_name = 'fact_customermaterial_info';
insert into number_fountain
select 'fact_customermaterial_info',
ifnull(max(f.fact_customermaterial_infoid),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_customermaterial_info f;


insert into fact_customermaterial_info(
fact_customermaterial_infoid,
dd_salesorganization,
dd_distributionchannel,
dd_customernumber,
dd_materialnumber,
dd_custdescofmaterial,
dim_plantid,
dd_materialnumberusedbysuct,
dim_unitofmeasureid,
dim_salesorgid,
dim_distributionchannelid,
dim_customerid,
dim_partid,
dw_insert_date)
SELECT
(select max_id   from number_fountain   where table_name = 'fact_customermaterial_info') + row_number() over(order by '') AS fact_customermaterial_infoid,
ifnull(k.KNMT_VKORG,'Not Set') AS dd_salesorganization,
ifnull(k.KNMT_VTWEG,'Not Set') AS dd_distributionchannel,
ifnull(k.KNMT_KUNNR,'Not Set') AS dd_customernumber,
ifnull(k.KNMT_MATNR,'Not Set') AS dd_materialnumber,
ifnull(k.KNMT_POSTX,'Not Set') AS dd_custdescofmaterial,
1 as dim_plantid,
ifnull(k.KNMT_KDMAT,'Not Set') AS dd_materialnumberusedbysuct,
1 as dim_unitofmeasureid,
1 as dim_salesorgid,
1 as dim_distributionchannelid,
1 as dim_customerid,
1 as dim_partid,
current_date as dw_insert_date
from KNMT k
where not exists (select 1 from fact_customermaterial_info f
                    where f.dd_salesorganization = ifnull(k.KNMT_VKORG,'Not Set')
					      and f.dd_distributionchannel = ifnull(k.KNMT_VTWEG,'Not Set')
						  and f.dd_customernumber = ifnull(k.KNMT_KUNNR,'Not Set')
						  and f.dd_materialnumber = ifnull(k.KNMT_MATNR,'Not Set'));

update fact_customermaterial_info f
set f.dim_plantid=pl.dim_plantid
,dw_update_date = current_timestamp
from fact_customermaterial_info f, dim_plant pl, KNMT k, MVKE_N m
where f.dd_salesorganization = ifnull(k.KNMT_VKORG,'Not Set')
	and f.dd_distributionchannel = ifnull(k.KNMT_VTWEG,'Not Set')
	and f.dd_customernumber = ifnull(k.KNMT_KUNNR,'Not Set')
	and f.dd_materialnumber = ifnull(k.KNMT_MATNR,'Not Set')
	and MVKE_VKORG=KNMT_VKORG and MVKE_VTWEG = KNMT_VTWEG and MVKE_MATNR = KNMT_MATNR
	and pl.plantcode=ifnull(m.MVKE_DWERK,'Not Set')
	and f.dim_plantid <> pl.dim_plantid;


update fact_customermaterial_info f
set f.dim_unitofmeasureid=uom.dim_unitofmeasureid
,dw_update_date = current_timestamp
from fact_customermaterial_info f, dim_unitofmeasure uom, KNMT k
where f.dd_salesorganization = ifnull(k.KNMT_VKORG,'Not Set')
	and f.dd_distributionchannel = ifnull(k.KNMT_VTWEG,'Not Set')
	and f.dd_customernumber = ifnull(k.KNMT_KUNNR,'Not Set')
	and f.dd_materialnumber = ifnull(k.KNMT_MATNR,'Not Set')
	and uom.uom=ifnull(k.KNMT_MEINS,'Not Set')
	and f.dim_unitofmeasureid <> uom.dim_unitofmeasureid;

update fact_customermaterial_info f
set f.dim_salesorgid=s.dim_salesorgid
,dw_update_date = current_timestamp
from fact_customermaterial_info f, dim_salesorg s, KNMT k
where f.dd_salesorganization = ifnull(k.KNMT_VKORG,'Not Set')
	and f.dd_distributionchannel = ifnull(k.KNMT_VTWEG,'Not Set')
	and f.dd_customernumber = ifnull(k.KNMT_KUNNR,'Not Set')
	and f.dd_materialnumber = ifnull(k.KNMT_MATNR,'Not Set')
	and s.salesorgcode=ifnull(k.KNMT_VKORG,'Not Set')
	and f.dim_salesorgid <> s.dim_salesorgid;

update fact_customermaterial_info f
set f.dim_distributionchannelid=dc.dim_distributionchannelid
,dw_update_date = current_timestamp
from fact_customermaterial_info f, dim_distributionchannel dc, KNMT k
where f.dd_salesorganization = ifnull(k.KNMT_VKORG,'Not Set')
	and f.dd_distributionchannel = ifnull(k.KNMT_VTWEG,'Not Set')
	and f.dd_customernumber = ifnull(k.KNMT_KUNNR,'Not Set')
	and f.dd_materialnumber = ifnull(k.KNMT_MATNR,'Not Set')
	and dc.distributionchannelcode=ifnull(k.KNMT_VTWEG,'Not Set')
	and f.dim_distributionchannelid <> dc.dim_distributionchannelid;

update fact_customermaterial_info f
set f.dim_customerid=dc.dim_customerid
,dw_update_date = current_timestamp
from fact_customermaterial_info f, dim_customer dc, KNMT k
where f.dd_salesorganization = ifnull(k.KNMT_VKORG,'Not Set')
	and f.dd_distributionchannel = ifnull(k.KNMT_VTWEG,'Not Set')
	and f.dd_customernumber = ifnull(k.KNMT_KUNNR,'Not Set')
	and f.dd_materialnumber = ifnull(k.KNMT_MATNR,'Not Set')
	and dc.customernumber=ifnull(k.KNMT_KUNNR,'Not Set')
	and  f.dim_customerid <> dc.dim_customerid;

merge into fact_customermaterial_info f
using (select distinct f.fact_customermaterial_infoid, first_value(dp.dim_partid) over (partition by f.fact_customermaterial_infoid order by f.fact_customermaterial_infoid) as dim_partid
from fact_customermaterial_info f, dim_part dp, KNMT k
where f.dd_salesorganization = ifnull(k.KNMT_VKORG,'Not Set')
	and f.dd_distributionchannel = ifnull(k.KNMT_VTWEG,'Not Set')
	and f.dd_customernumber = ifnull(k.KNMT_KUNNR,'Not Set')
	and f.dd_materialnumber = ifnull(k.KNMT_MATNR,'Not Set')
	and dp.partnumber=ifnull(k.KNMT_MATNR,'Not Set')
	and  f.dim_partid <> dp.dim_partid) t
on t.fact_customermaterial_infoid = f.fact_customermaterial_infoid
when matched then update set  f.dim_partid=t.dim_partid
,dw_update_date = current_timestamp;
	
/*Octavian S 15-FEB-2018 APP-6837 Add new attributes*/
-------------------
--Info for packaging
 MERGE INTO fact_customermaterial_info fc
 USING (
        SELECT DISTINCT fc.dd_salesorganization, fc.dd_distributionchannel, fc.dd_materialnumber, fc.dd_customernumber, shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%KNMT%' 
                   AND stxh_tdid     = 'Z002'
                 GROUP BY stxh_tdname)         s
                 ,fact_customermaterial_info   fc
          WHERE TRIM(substr(stxh_tdname,1,4))  = fc.dd_salesorganization 
            AND TRIM(substr(stxh_tdname,5,2))  = fc.dd_distributionchannel
            AND TRIM(substr(stxh_tdname,17,6)) = fc.dd_materialnumber  
            AND TRIM(substr(stxh_tdname,7,10)) = fc.dd_customernumber    
       )t
   ON fc.dd_salesorganization    = t.dd_salesorganization 
  AND fc.dd_distributionchannel  = t.dd_distributionchannel
  AND fc.dd_materialnumber       = t.dd_materialnumber  
  AND fc.dd_customernumber       = t.dd_customernumber  
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_stxh_info_pack = IFNULL(t.shorttext,'Not Set');

--Product License Number	
 MERGE INTO fact_customermaterial_info fc
 USING (
        SELECT DISTINCT fc.dd_salesorganization, fc.dd_distributionchannel, fc.dd_materialnumber, fc.dd_customernumber, shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%KNMT%' 
                   AND stxh_tdid     = 'Z005'
                 GROUP BY stxh_tdname)         s
                 ,fact_customermaterial_info   fc
          WHERE TRIM(substr(stxh_tdname,1,4))  = fc.dd_salesorganization 
            AND TRIM(substr(stxh_tdname,5,2))  = fc.dd_distributionchannel
            AND TRIM(substr(stxh_tdname,17,6)) = fc.dd_materialnumber  
            AND TRIM(substr(stxh_tdname,7,10)) = fc.dd_customernumber  
       )t
   ON fc.dd_salesorganization    = t.dd_salesorganization 
  AND fc.dd_distributionchannel  = t.dd_distributionchannel
  AND fc.dd_materialnumber       = t.dd_materialnumber  
  AND fc.dd_customernumber       = t.dd_customernumber  
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_prod_licence_no = IFNULL(t.shorttext,'Not Set');

--COA Footnote	
 MERGE INTO fact_customermaterial_info fc
 USING (
        SELECT DISTINCT fc.dd_salesorganization, fc.dd_distributionchannel, fc.dd_materialnumber, fc.dd_customernumber, shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%KNMT%' 
                   AND stxh_tdid     = 'Z003'
                 GROUP BY stxh_tdname)         s
                 ,fact_customermaterial_info   fc
          WHERE TRIM(substr(stxh_tdname,1,4))  = fc.dd_salesorganization 
            AND TRIM(substr(stxh_tdname,5,2))  = fc.dd_distributionchannel
            AND TRIM(substr(stxh_tdname,17,6)) = fc.dd_materialnumber  
            AND TRIM(substr(stxh_tdname,7,10)) = fc.dd_customernumber  
       )t
   ON fc.dd_salesorganization    = t.dd_salesorganization 
  AND fc.dd_distributionchannel  = t.dd_distributionchannel
  AND fc.dd_materialnumber       = t.dd_materialnumber  
  AND fc.dd_customernumber       = t.dd_customernumber  
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_stxh_COA_footnote = IFNULL(t.shorttext,'Not Set');

--Customer Material Info	
 MERGE INTO fact_customermaterial_info fc
 USING (
        SELECT DISTINCT fc.dd_salesorganization, fc.dd_distributionchannel, fc.dd_materialnumber, fc.dd_customernumber, shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%KNMT%' 
                   AND stxh_tdid     = '0001'
                 GROUP BY stxh_tdname)         s
                 ,fact_customermaterial_info   fc
          WHERE TRIM(substr(stxh_tdname,1,4))  = fc.dd_salesorganization 
            AND TRIM(substr(stxh_tdname,5,2))  = fc.dd_distributionchannel
            AND TRIM(substr(stxh_tdname,17,6)) = fc.dd_materialnumber  
            AND TRIM(substr(stxh_tdname,7,10)) = fc.dd_customernumber   
      )t
   ON fc.dd_salesorganization    = t.dd_salesorganization 
  AND fc.dd_distributionchannel  = t.dd_distributionchannel
  AND fc.dd_materialnumber       = t.dd_materialnumber  
  AND fc.dd_customernumber       = t.dd_customernumber  
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_stxh_cust_mat_info  = IFNULL(t.shorttext,'Not Set');

--Info for COP (NP)	
 MERGE INTO fact_customermaterial_info fc
 USING (
        SELECT DISTINCT fc.dd_salesorganization, fc.dd_distributionchannel, fc.dd_materialnumber, fc.dd_customernumber, shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%KNMT%' 
                   AND stxh_tdid     = 'Z001'
                 GROUP BY stxh_tdname)         s
                 ,fact_customermaterial_info   fc
          WHERE TRIM(substr(stxh_tdname,1,4))  = fc.dd_salesorganization 
            AND TRIM(substr(stxh_tdname,5,2))  = fc.dd_distributionchannel
            AND TRIM(substr(stxh_tdname,17,6)) = fc.dd_materialnumber  
            AND TRIM(substr(stxh_tdname,7,10)) = fc.dd_customernumber   
      )t
   ON fc.dd_salesorganization    = t.dd_salesorganization 
  AND fc.dd_distributionchannel  = t.dd_distributionchannel
  AND fc.dd_materialnumber       = t.dd_materialnumber  
  AND fc.dd_customernumber       = t.dd_customernumber  
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_stxh_info_COP_mat  = IFNULL(t.shorttext,'Not Set');

--Info for Customer Serv. (NP)
 MERGE INTO fact_customermaterial_info fc
 USING (
        SELECT DISTINCT fc.dd_salesorganization, fc.dd_distributionchannel, fc.dd_materialnumber, fc.dd_customernumber, shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%KNMT%' 
                   AND stxh_tdid     = 'Z004'
                 GROUP BY stxh_tdname)         s
                 ,fact_customermaterial_info   fc
          WHERE TRIM(substr(stxh_tdname,1,4))  = fc.dd_salesorganization 
            AND TRIM(substr(stxh_tdname,5,2))  = fc.dd_distributionchannel
            AND TRIM(substr(stxh_tdname,17,6)) = fc.dd_materialnumber  
            AND TRIM(substr(stxh_tdname,7,10)) = fc.dd_customernumber    
      )t
   ON fc.dd_salesorganization    = t.dd_salesorganization 
  AND fc.dd_distributionchannel  = t.dd_distributionchannel
  AND fc.dd_materialnumber       = t.dd_materialnumber  
  AND fc.dd_customernumber       = t.dd_customernumber  
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_stxh_info_cust_serv  = IFNULL(t.shorttext,'Not Set');
 