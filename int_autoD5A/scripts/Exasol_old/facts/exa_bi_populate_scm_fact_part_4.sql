
/*delete the rows older than 2.5 years*/
merge into fact_scm f using
( 
  select fact_mmprodhierarchyid
  from fact_scm fmm 
    inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
  where agidcc.datevalue < add_years(current_date, -2)
) upd
on f.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then delete;


/*Madalina 7 Oct 2017 - Move the inserts final part to part_4, until SW-509 is resolved*/
/*Madalina 31 Oct 2017 - deleting the Inserts final part from part 4 script */

update fact_scm f
set gpf_fpp_final = ifnull(productfamilydescription_merck,'Not Set')
from fact_scm f inner join dim_part dp
on f.dim_partid_fpp_final = dp.dim_partid
where gpf_fpp_final <> ifnull(productfamilydescription_merck,'Not Set');	












/* update the dim dates earlier in the script */
/* Update Actual receipt date to ComOps, according to the relation with Inspection Lot and EKBE - BI-4288 */
merge into fact_scm mm using
( select distinct fact_mmprodhierarchyid, dd.dim_dateid
from fact_inspectionlot f
	inner join dim_batch b ON f.dim_batchid = b.dim_batchid
	inner join EKBE_deliv d ON f.dd_materialdocno = d.EKBE_BELNR 
							AND f.dd_MaterialDocItemNo = d.EKBE_BUZEI
							AND d.EKBE_CHARG = b.batchnumber 
							AND d.EKBE_MATNR = b.partnumber 
							AND d.EKBE_WERKS = b.plantcode
	inner join fact_scm mm ON f.dd_inspectionlotno = mm.DD_INSPECTIONLOTNO_COMOPS
	inner join dim_plant p ON mm.dim_plantid_comops = p.dim_plantid
	inner join dim_date dd ON dd.datevalue = ifnull(d.EKBE_BUDAT,'Not Set')
							AND dd.plantcode_factory = p.plantcode
							AND dd.companycode = p.companycode) upd
on mm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set mm.dim_dateidactualreceipt_comops = upd.dim_dateid;

/*Query optimization for below updates*/
/* Madalina 3 Oct 2017 - perform below updates earlier in the script, directly on the temporary tables instead of the final fact form*/
/*merge into fact_scm fmm using
(select distinct fact_mmprodhierarchyid, 
				danew_usage.dim_dateid as dim_dateidusagedecisionmade_fpp_1,
				danew_ship.dim_dateid as dim_dateiddlvrdoccreated_comops,
				danew_agi.dim_dateid as dim_dateidactualgoodsissue_comops,
				danew_ard.dim_dateid as dim_dateidactualreceipt_comops,
				danew_usagecom.dim_dateid as dim_dateidusagedecisionmade_comops,
				danew_shipcust.dim_dateid as dim_dateiddlvrdoccreated_customer
				danew_agicust.dim_dateid as dim_dateidactualgoodsissue_customer
		from fact_scm fmm 
				inner join dim_plant pl_usage on fmm.dim_plantid = pl_usage.dim_plantid
				inner join dim_date da_usage on  fmm.dim_dateidusagedecisionmade_fpp_1 = da_usage.dim_dateid
				inner join dim_date danew_usage on da_usage.datevalue = danew_usage.datevalue
							and danew_usage.companycode = pl_usage.companycode
							and danew_usage.plantcode_factory = pl_usage.plantcode 

				inner join dim_plant pl_ship on fmm.dim_plantid = pl_ship.dim_plantid
				inner join  dim_date da_ship on fmm.dim_dateiddlvrdoccreated_comops = da_ship.dim_dateid
				inner join dim_date danew_ship on  da_ship.datevalue = danew_ship.datevalue
							and danew_ship.companycode = pl_ship.companycode
							and danew_ship.plantcode_factory = pl_ship.plantcode

				inner join dim_plant pl_agi on 	fmm.dim_plantid = pl_agi.dim_plantid	
				inner join dim_date da_agi on fmm.dim_dateidactualgoodsissue_comops = da_agi.dim_dateid
				inner join dim_date danew_agi on  da_agi.datevalue = danew_agi.datevalue
							and danew_agi.companycode = pl_agi.companycode
							and danew_agi.plantcode_factory = pl_agi.plantcode

				inner join dim_plant pl_ard on 	fmm.dim_plantid_comops = pl_ard.dim_plantid	
				inner join dim_date da_ard on fmm.dim_dateidactualreceipt_comops = da_ard.dim_dateid
				inner join dim_date danew_ard on  da_ard.datevalue = danew_ard.datevalue
							and danew_ard.companycode = pl_ard.companycode
							and danew_ard.plantcode_factory = pl_ard.plantcode

				inner join dim_plant pl_usagecom on fmm.dim_plantid_comops = pl_usagecom.dim_plantid	
				inner join dim_date da_usagecom on fmm.dim_dateidusagedecisionmade_comops = da_usagecom.dim_dateid
				inner join dim_date danew_usagecom on  da_usagecom.datevalue = danew_usagecom.datevalue
							and danew_usagecom.companycode = pl_usagecom.companycode
							and danew_usagecom.plantcode_factory = pl_usagecom.plantcode

				inner join dim_plant pl_shipcust on fmm.dim_plantid_comops = pl_shipcust.dim_plantid	
				inner join dim_date da_shipcust on fmm.dim_dateidusagedecisionmade_comops = da_shipcust.dim_dateid
				inner join dim_date danew_shipcust on  da_shipcust.datevalue = danew_shipcust.datevalue
							and danew_shipcust.companycode = pl_shipcust.companycode
							and danew_shipcust.plantcode_factory = pl_shipcust.plantcode

				inner join dim_plant pl_agicust on fmm.dim_plantid_comops = pl_agicust.dim_plantid	
				inner join dim_date da_agicust on fmm.dim_dateidusagedecisionmade_comops = da_agicust.dim_dateid
				inner join dim_date danew_agicust on  da_agicust.datevalue = danew_agicust.datevalue
							and danew_agicust.companycode = pl_agicust.companycode
							and danew_agicust.plantcode_factory = pl_agicust.plantcode
		) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set
fmm.dim_dateidusagedecisionmade_fpp_1 = upd.dim_dateidusagedecisionmade_fpp_1,
fmm.dim_dateiddlvrdoccreated_comops = upd.dim_dateiddlvrdoccreated_comops,
fmm.dim_dateidactualgoodsissue_comops = upd.dim_dateidactualgoodsissue_comops,
fmm.dim_dateidactualreceipt_comops = upd.dim_dateidactualreceipt_comops,
fmm.dim_dateidusagedecisionmade_comops = upd.dim_dateidusagedecisionmade_comops,
fmm.dim_dateiddlvrdoccreated_customer = upd.dim_dateiddlvrdoccreated_customer,
fmm.dim_dateidactualgoodsissue_customer = upd.dim_dateidactualgoodsissue_customer */

/* BI-4288 - Point Usage Decision Made Date FPP to Plant */
/*merge into fact_scm fmm using
(select distinct fact_mmprodhierarchyid, danew.dim_dateid
from fact_scm fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateidusagedecisionmade_fpp_1 = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode ) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set fmm.dim_dateidusagedecisionmade_fpp_1 = upd.dim_dateid*/

/* Point Shipment Create Date to ComOps to Plant */  
/*merge into fact_scm fmm using
(select distinct fact_mmprodhierarchyid, danew.dim_dateid
from fact_scm fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateiddlvrdoccreated_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode ) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 	
set fmm.dim_dateiddlvrdoccreated_comops = upd.dim_dateid */

/* Point Actual Goods Issue Date to ComOps to Plant */ 
/* merge into fact_scm fmm using
(select distinct fact_mmprodhierarchyid, danew.dim_dateid
from fact_scm fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateidactualgoodsissue_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode ) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set fmm.dim_dateidactualgoodsissue_comops = upd.dim_dateid */

/* BI-4288 - Point Actual Receipt Date to ComOps and Usage Decision Made Date ComOps to the same Plant */
/*  merge into fact_scm fmm using
(select distinct fact_mmprodhierarchyid, danew.dim_dateid
from fact_scm fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidactualreceipt_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set fmm.dim_dateidactualreceipt_comops = upd.dim_dateid */

/*merge into fact_scm fmm using
(select distinct fact_mmprodhierarchyid, danew.dim_dateid
from fact_scm fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidusagedecisionmade_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set fmm.dim_dateidusagedecisionmade_comops = upd.dim_dateid */

/* Point Shipment Create Date to Customer to PlantComOps */
/*merge into fact_scm fmm using
(select distinct fact_mmprodhierarchyid, danew.dim_dateid
from fact_scm fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateiddlvrdoccreated_customer = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode ) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set fmm.dim_dateiddlvrdoccreated_customer = upd.dim_dateid */

/* Point Actual Goods Issue Date to Customer to PlantComops */
/* merge into fact_scm fmm using
(select distinct fact_mmprodhierarchyid, danew.dim_dateid
from fact_scm fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidactualgoodsissue_customer = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode ) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set fmm.dim_dateidactualgoodsissue_customer = upd.dim_dateid */
	
/* Add Hit or miss (E2E LT) and Sales order hit- BI-4857 */
/* BI-5265 - update formulas - include ACTIVE_PROP_UNIT in addition to ACTIVE */
/* Correction: dont't use ma.batchclass_merck in grouping */
drop table if exists tmp_upd_hitormissE2E_LT;
create table tmp_upd_hitormissE2E_LT as
select dd_SalesDocNo_customer, dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode as PlantCodeSite,  	mfp.PartNumber, agidcc.MonthYear,
	
/* Add Actual E2E LT and Target E2E LT - for Weighted av. Actual LT and Weighted av. Target LT - BI-4857 */
/* Actual E2E LT */	
	MAX(
     CASE WHEN (pprtfppf.ProductGroup_Merck) = 'B'
     THEN (agidcc.DateValue) - (darant1.DateValue) 
     WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' 
     THEN (agidcc.DateValue) - (grdr.DateValue) 
     ELSE 0 
     END)*5/7 as ct_actualE2E_LT,
/* Target E2E LT */
	MAX( CASE WHEN (pprtfppf.ProductGroup_Merck) <> 'B'
	THEN
	CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' 
	THEN (ma.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 
	+ (CT_LOTSIZEDAYS_RAW_1) *5/7 /2 
	ELSE 0 END
	ELSE 0
	END
	
	+
	( (CT_FLOATBEFOREPRODUCTION_ANTIGEN_1) + (CT_TARGETMASTERRECIPE_ANTIGEN_1) + (pprtant1.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_1) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_1) *5/7/2  +
	 (CT_FLOATBEFOREPRODUCTION_ANTIGEN_2) + (CT_TARGETMASTERRECIPE_ANTIGEN_2) + (pprtant2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_2) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_2) *5/7/2 +
	 (CT_FLOATBEFOREPRODUCTION_ANTIGEN_3) + (CT_TARGETMASTERRECIPE_ANTIGEN_3) + (pprtant3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_3) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_3) *5/7/2  +
	(CT_FLOATBEFOREPRODUCTION_ANTIGEN_4) + (CT_TARGETMASTERRECIPE_ANTIGEN_4) + (pprtant4.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_4) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_4) *5/7/2  +
	 (CT_FLOATBEFOREPRODUCTION_ANTIGEN_5) + (CT_TARGETMASTERRECIPE_ANTIGEN_5) + (pprtant5.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_5) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_5) *5/7/2 ) +

	( (CT_FLOATBEFOREPRODUCTION_BULK_1) + (CT_TARGETMASTERRECIPE_BULK_1) + (mab.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7 + (CT_LOTSIZEDAYS_BULK_1) *5/7/2 +
	 (CT_FLOATBEFOREPRODUCTION_BULK_2) + (CT_TARGETMASTERRECIPE_BULK_2) + (mab2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_2) *5/7 + (CT_LOTSIZEDAYS_BULK_2) *5/7/2 +
	 (CT_FLOATBEFOREPRODUCTION_BULK_3) + (CT_TARGETMASTERRECIPE_BULK_3) + (mab3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_3) *5/7 + (CT_LOTSIZEDAYS_BULK_3) *5/7/2 ) +

	( (CT_FLOATBEFOREPRODUCTION_FPU_1) + (CT_TARGETMASTERRECIPE_FPU_1) + (mf.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7 + (CT_LOTSIZEDAYS_FPU_1) *5/7 /2 +
	 (CT_FLOATBEFOREPRODUCTION_FPU_2) + (CT_TARGETMASTERRECIPE_FPU_2) + (pprtfpu2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_2) *5/7 + (CT_LOTSIZEDAYS_FPU_2) *5/7/2 +
	 (CT_FLOATBEFOREPRODUCTION_FPU_3) + (CT_TARGETMASTERRECIPE_FPU_3) + (pprtfpu3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_3) *5/7 + (CT_LOTSIZEDAYS_FPU_3) *5/7/2  +
	(CT_FLOATBEFOREPRODUCTION_FPU_4) + (CT_TARGETMASTERRECIPE_FPU_4) + (pprtfpu4.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_4) *5/7 + (CT_LOTSIZEDAYS_FPU_4) *5/7/2  +
	(CT_FLOATBEFOREPRODUCTION_FPU_5) + (CT_TARGETMASTERRECIPE_FPU_5) + (pprtfpu5.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_5) *5/7 + (CT_LOTSIZEDAYS_FPU_5) *5/7/2 ) +

	 (CT_FLOATBEFOREPRODUCTION_FPP_1) + (CT_TARGETMASTERRECIPE_FPP_1) + (mfp.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7 + (CT_LOTSIZEDAYS_FPP_1) *5/7 /2 +
	 (CT_FLOATBEFOREPRODUCTION_FPP_2) + (CT_TARGETMASTERRECIPE_FPP_2) + (pprtfpp2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_2) *5/7 + (CT_LOTSIZEDAYS_FPP_2) *5/7/2 +
	 (CT_FLOATBEFOREPRODUCTION_FPP_3) + (CT_TARGETMASTERRECIPE_FPP_3) + (pprtfpp3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_3) *5/7 + (CT_LOTSIZEDAYS_FPP_3) *5/7/2  +

	 (ifnull(mfpc.LeadTime,0)) *5/7 + 
	 (mfpc.GRProcessingTime) +
	 (CT_MINRELEASEDSTOCKDAYS_COMOPS) *5/7 +
	 (CT_LOTSIZEDAYS_COMOPS) *5/7 /2 ) as ct_targetE2E_LT,
/* Madalina 13 Feb 2017 - BI-4857 */
/* Create new measures in order to obtain different values for Total Weighted av. Actual LT and Total Weighted av. Target LT*/ 
/* Actual LT * Sales - backend and Target LT * Sales - backend */
/* Madalina 5 Oct 2017 - add measures in backend*/	
/* Target E2E LT - ANTIGEN */
	MAX( (CT_FLOATBEFOREPRODUCTION_ANTIGEN_1) + (CT_TARGETMASTERRECIPE_ANTIGEN_1) + (pprtant1.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_1) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_1) *5/7/2 ) +
	MAX( (CT_FLOATBEFOREPRODUCTION_ANTIGEN_2) + (CT_TARGETMASTERRECIPE_ANTIGEN_2) + (pprtant2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_2) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_2) *5/7/2) +
	MAX( (CT_FLOATBEFOREPRODUCTION_ANTIGEN_3) + (CT_TARGETMASTERRECIPE_ANTIGEN_3) + (pprtant3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_3) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_3) *5/7/2 ) +
	MAX( (CT_FLOATBEFOREPRODUCTION_ANTIGEN_4) + (CT_TARGETMASTERRECIPE_ANTIGEN_4) + (pprtant4.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_4) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_4) *5/7/2 ) +
	MAX( (CT_FLOATBEFOREPRODUCTION_ANTIGEN_5) + (CT_TARGETMASTERRECIPE_ANTIGEN_5) + (pprtant5.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_5) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_5) *5/7/2 )
			as ct_targetE2E_LT_antigen,
/* Target E2E LT - BULK */
	MAX( (CT_FLOATBEFOREPRODUCTION_BULK_1) + (CT_TARGETMASTERRECIPE_BULK_1) + (mab.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7 + (CT_LOTSIZEDAYS_BULK_1) *5/7/2) +
	MAX( (CT_FLOATBEFOREPRODUCTION_BULK_2) + (CT_TARGETMASTERRECIPE_BULK_2) + (mab2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_2) *5/7 + (CT_LOTSIZEDAYS_BULK_2) *5/7/2) +
	MAX( (CT_FLOATBEFOREPRODUCTION_BULK_3) + (CT_TARGETMASTERRECIPE_BULK_3) + (mab3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_3) *5/7 + (CT_LOTSIZEDAYS_BULK_3) *5/7/2 )
			as ct_targetE2E_LT_bulk,
/* Target E2E LT - FPU */
	MAX( (CT_FLOATBEFOREPRODUCTION_FPU_1) + (CT_TARGETMASTERRECIPE_FPU_1) + (mf.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7 + (CT_LOTSIZEDAYS_FPU_1) *5/7 /2) +
	MAX( (CT_FLOATBEFOREPRODUCTION_FPU_2) + (CT_TARGETMASTERRECIPE_FPU_2) + (pprtfpu2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_2) *5/7 + (CT_LOTSIZEDAYS_FPU_2) *5/7/2) +
	MAX( (CT_FLOATBEFOREPRODUCTION_FPU_3) + (CT_TARGETMASTERRECIPE_FPU_3) + (pprtfpu3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_3) *5/7 + (CT_LOTSIZEDAYS_FPU_3) *5/7/2 ) +
	MAX( (CT_FLOATBEFOREPRODUCTION_FPU_4) + (CT_TARGETMASTERRECIPE_FPU_4) + (pprtfpu4.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_4) *5/7 + (CT_LOTSIZEDAYS_FPU_4) *5/7/2 ) +
	MAX( (CT_FLOATBEFOREPRODUCTION_FPU_5) + (CT_TARGETMASTERRECIPE_FPU_5) + (pprtfpu5.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_5) *5/7 + (CT_LOTSIZEDAYS_FPU_5) *5/7/2 )
			as ct_targetE2E_LT_fpu


from fact_scm fmm
	inner join dim_part mab on fmm.dim_partid_bulk_1 = mab.dim_partid
	inner join dim_part mfpc on fmm.dim_partid_comops = mfpc.dim_partid
	inner join dim_part mfp on fmm.dim_partid_fpp_1 =	mfp.dim_partid
	inner join dim_part mf on fmm.dim_partid_fpu_1 = mf.dim_partid
	inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
	inner join dim_date grdr on fmm.dim_dateidpostingdate = grdr.dim_dateid
	inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
	inner join dim_plant pl on fmm.dim_plantid_comops = pl.dim_plantid
	inner join dim_plant plsite on fmm.dim_plantid = plsite.dim_plantid
	inner join dim_part pprtant1 on fmm.dim_partid_antigen_1 = pprtant1.dim_partid
	inner join dim_part pprtfppf on fmm.dim_partid_fpp_final = pprtfppf.dim_partid
	inner join dim_date darant1 on fmm.dim_dateidactualrelease_antigen_1 = darant1.dim_dateid
	inner join dim_part pprtant2 on fmm.dim_partid_antigen_2 = pprtant2.dim_partid
	inner join dim_part pprtant3 on fmm.dim_partid_antigen_3 = pprtant3.dim_partid
	inner join dim_part pprtant4 on fmm.dim_partid_antigen_4 = pprtant4.dim_partid
	inner join dim_part pprtant5 on fmm.dim_partid_antigen_5 = pprtant5.dim_partid
	inner join dim_part mab2 on fmm.dim_partid_bulk_2 = mab2.dim_partid
	inner join dim_part mab3 on fmm.dim_partid_bulk_3 = mab3.dim_partid
	inner join dim_part pprtfpu2 on fmm.dim_partid_fpu_2 = pprtfpu2.dim_partid
	inner join dim_part pprtfpu3 on fmm.dim_partid_fpu_3 = pprtfpu3.dim_partid
	inner join dim_part pprtfpu4 on fmm.dim_partid_fpu_4 = pprtfpu4.dim_partid
	inner join dim_part pprtfpu5 on fmm.dim_partid_fpu_5 = pprtfpu5.dim_partid
	inner join dim_part pprtfpp2 on fmm.dim_partid_fpp_2 = pprtfpp2.dim_partid
	inner join dim_part pprtfpp3 on fmm.dim_partid_fpp_3 = pprtfpp3.dim_partid
	inner join dim_part p on fmm.dim_partid_fpp_final = p.dim_partid
	inner join dim_part pp on fmm.dim_partid_antigen_1 = pp.dim_partid
where /* agidcc.datevalue >= add_years(current_date, -2) and */
	 ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
		( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
group by dd_SalesDocNo_customer, dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode, mfp.PartNumber, agidcc.MonthYear;

merge into fact_scm fmm using
(select fmm.fact_mmprodhierarchyid, tmp.ct_actualE2E_LT, tmp.ct_targetE2E_LT,
	tmp.ct_targetE2E_LT_antigen, tmp.ct_targetE2E_LT_bulk, tmp.ct_targetE2E_LT_fpu
from fact_scm fmm, tmp_upd_hitormissE2E_LT tmp, dim_part ma, dim_date agidcc, dim_plant pl, dim_plant plsite, dim_part mfp
where fmm.dd_SalesDocNo_customer = tmp.dd_SalesDocNo_customer
	and fmm.dd_SalesItemNo_customer = tmp.dd_SalesItemNo_customer
	and fmm.dim_partid_raw_1 = ma.dim_partid
	and fmm.dim_partid_fpp_1 =	mfp.dim_partid
	and fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
	and fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_plantid = plsite.dim_plantid
	and agidcc.MonthYear = tmp.MonthYear
	and mfp.PartNumber = tmp.PartNumber
	and plsite.PlantCode = tmp.PlantCodeSite
	and pl.PlantCode = tmp.PlantCode
	and mfp.ProductFamily_Merck = tmp.ProductFamily_Merck) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update	
set fmm.ct_actualE2E_LT = upd.ct_actualE2E_LT,
	fmm.ct_targetE2E_LT = upd.ct_targetE2E_LT,
	fmm.ct_targetE2E_LT_antigen = upd.ct_targetE2E_LT_antigen,
	fmm.ct_targetE2E_LT_bulk = upd.ct_targetE2E_LT_bulk,
	fmm.ct_targetE2E_LT_fpu = upd.ct_targetE2E_LT_fpu
	;





drop table if exists fact_scm_for_measure_updates;
create table fact_scm_for_measure_updates as 
select
DD_INSPECTIONLOTNOT_RAW, DD_INSPECTIONLOTNO_ANTIGEN_1, DD_INSPECTIONLOTNO_ANTIGEN_2, DD_INSPECTIONLOTNO_ANTIGEN_3, DD_INSPECTIONLOTNO_ANTIGEN_4, 
DD_INSPECTIONLOTNO_ANTIGEN_5, DD_INSPECTIONLOTNO_BULK, DD_INSPECTIONLOTNO_BULK_1, DD_INSPECTIONLOTNO_BULK_2, DD_INSPECTIONLOTNO_BULK_3, 
DD_INSPECTIONLOTNO_COMOPS, DD_INSPECTIONLOTNO_FPP, DD_INSPECTIONLOTNO_FPP_1, DD_INSPECTIONLOTNO_FPP_2, DD_INSPECTIONLOTNO_FPP_3, DD_INSPECTIONLOTNO_FPU, 
DD_INSPECTIONLOTNO_FPU_1, DD_INSPECTIONLOTNO_FPU_2, DD_INSPECTIONLOTNO_FPU_3, DD_INSPECTIONLOTNO_FPU_4, DD_INSPECTIONLOTNO_FPU_5, DD_INSPECTIONLOTNO_RAW_1, 
DIM_BATCHID_ANTIGEN_1, DIM_BATCHID_ANTIGEN_2, DIM_BATCHID_ANTIGEN_3, DIM_BATCHID_ANTIGEN_4, DIM_BATCHID_ANTIGEN_5, DIM_BATCHID_BULK_1, DIM_BATCHID_BULK_2, 
DIM_BATCHID_BULK_3, DIM_BATCHID_FPP_1, DIM_BATCHID_FPP_2, DIM_BATCHID_FPP_3, DIM_BATCHID_FPP_FINAL, DIM_BATCHID_FPU_1, DIM_BATCHID_FPU_2, DIM_BATCHID_FPU_3, 
DIM_BATCHID_FPU_4, DIM_BATCHID_FPU_5, DIM_BATCHID_RAW_1, DIM_DATEIDACTUALGOODSISSUE_COMOPS, DIM_DATEIDACTUALGOODSISSUE_CUSTOMER, DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_1,
 DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_2, DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_3, DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_4, DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_5,
 DIM_DATEIDACTUALHEADERFINISH_BULK_1, DIM_DATEIDACTUALHEADERFINISH_BULK_2, DIM_DATEIDACTUALHEADERFINISH_BULK_3, DIM_DATEIDACTUALHEADERFINISH_FPP_1, 
DIM_DATEIDACTUALHEADERFINISH_FPP_2, DIM_DATEIDACTUALHEADERFINISH_FPP_3, DIM_DATEIDACTUALHEADERFINISH_FPU_1, DIM_DATEIDACTUALHEADERFINISH_FPU_2, 
DIM_DATEIDACTUALHEADERFINISH_FPU_3, DIM_DATEIDACTUALHEADERFINISH_FPU_4, DIM_DATEIDACTUALHEADERFINISH_FPU_5, DIM_DATEIDACTUALHEADERFINISH_RAW_1, 
DIM_DATEIDACTUALRECEIPT_COMOPS, DIM_DATEIDACTUALRELEASE_ANTIGEN_1, DIM_DATEIDACTUALRELEASE_ANTIGEN_2, DIM_DATEIDACTUALRELEASE_ANTIGEN_3, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_4, DIM_DATEIDACTUALRELEASE_ANTIGEN_5, DIM_DATEIDACTUALRELEASE_BULK_1, DIM_DATEIDACTUALRELEASE_BULK_2, 
DIM_DATEIDACTUALRELEASE_BULK_3, DIM_DATEIDACTUALRELEASE_FPP_1, DIM_DATEIDACTUALRELEASE_FPP_2, DIM_DATEIDACTUALRELEASE_FPP_3, DIM_DATEIDACTUALRELEASE_FPU_1, 
DIM_DATEIDACTUALRELEASE_FPU_2, DIM_DATEIDACTUALRELEASE_FPU_3, DIM_DATEIDACTUALRELEASE_FPU_4, DIM_DATEIDACTUALRELEASE_FPU_5, DIM_DATEIDACTUALRELEASE_RAW_1, 
DIM_PARTID_ANTIGEN_1, DIM_PARTID_ANTIGEN_2, DIM_PARTID_ANTIGEN_3, DIM_PARTID_ANTIGEN_4, DIM_PARTID_ANTIGEN_5, DIM_PARTID_BULK_1, DIM_PARTID_BULK_2, 
DIM_PARTID_BULK_3, DIM_PARTID_COMOPS, DIM_PARTID_FPP_1, DIM_PARTID_FPP_2, DIM_PARTID_FPP_3, DIM_PARTID_FPP_FINAL, DIM_PARTID_FPU_1, DIM_PARTID_FPU_2, 
DIM_PARTID_FPU_3, DIM_PARTID_FPU_4, DIM_PARTID_FPU_5, DIM_PARTID_RAW_1, DIM_PLANTID, DIM_PLANTID_ANTIGEN_1, DIM_PLANTID_ANTIGEN_2, DIM_PLANTID_ANTIGEN_3,
 DIM_PLANTID_ANTIGEN_4, DIM_PLANTID_ANTIGEN_5, DIM_PLANTID_BULK_1, DIM_PLANTID_BULK_2, DIM_PLANTID_BULK_3, DIM_PLANTID_COMOPS, DIM_PLANTID_FPP_1, DIM_PLANTID_FPP_2,
 DIM_PLANTID_FPP_3, DIM_PLANTID_FPP_FINAL, DIM_PLANTID_FPU_1, DIM_PLANTID_FPU_2, DIM_PLANTID_FPU_3, DIM_PLANTID_FPU_4, DIM_PLANTID_FPU_5, DIM_PLANTID_RAW_1, FACT_MMPRODHIERARCHYID, GPF_FPP_FINAL,
 CT_ACTUALE2E_LT, CT_ACTUALLTANTIGEN, CT_ACTUALLTANTIGEN1, CT_ACTUALLTANTIGEN2, CT_ACTUALLTANTIGEN3, CT_ACTUALLTANTIGEN4, CT_ACTUALLTANTIGEN5, CT_ACTUALLTBULK, 
CT_ACTUALLTBULK1, CT_ACTUALLTBULK2, CT_ACTUALLTBULK3, CT_ACTUALLTFPP, CT_ACTUALLTFPP1, CT_ACTUALLTFPP2, CT_ACTUALLTFPP3, 
CT_ACTUALLTFPU, CT_ACTUALLTFPU1, CT_ACTUALLTFPU2, CT_ACTUALLTFPU3, CT_ACTUALLTFPU4, CT_ACTUALLTFPU5, CT_ACTUALLTRAW, CT_ACTUALLTRAW1, CT_ACTUALLTSALES, 
CT_FLOATBEFOREPRODUCTIONBULK, CT_FLOATBEFOREPRODUCTIONFPP, CT_FLOATBEFOREPRODUCTIONFPPCOMOP, CT_FLOATBEFOREPRODUCTIONFPU, CT_FLOATBEFOREPRODUCTIONRAW, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_1, CT_FLOATBEFOREPRODUCTION_ANTIGEN_2, CT_FLOATBEFOREPRODUCTION_ANTIGEN_3, CT_FLOATBEFOREPRODUCTION_ANTIGEN_4, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_5, CT_FLOATBEFOREPRODUCTION_BULK_1, CT_FLOATBEFOREPRODUCTION_BULK_2, CT_FLOATBEFOREPRODUCTION_BULK_3, CT_FLOATBEFOREPRODUCTION_COMOPS,
 CT_FLOATBEFOREPRODUCTION_FPP_1, CT_FLOATBEFOREPRODUCTION_FPP_2, CT_FLOATBEFOREPRODUCTION_FPP_3, CT_FLOATBEFOREPRODUCTION_FPU_1, CT_FLOATBEFOREPRODUCTION_FPU_2,
 CT_FLOATBEFOREPRODUCTION_FPU_3, CT_FLOATBEFOREPRODUCTION_FPU_4, CT_FLOATBEFOREPRODUCTION_FPU_5, CT_FLOATBEFOREPRODUCTION_RAW_1, CT_HITORMISSE2E_LT, 
CT_HITORMISSE2E_LT_ANTIGEN, CT_HITORMISSE2E_LT_BULK, CT_HITORMISSE2E_LT_COMOPS, CT_HITORMISSE2E_LT_FPP, CT_HITORMISSE2E_LT_FPU, CT_HITORMISSE2E_LT_RAW, CT_HITVALUE, 
CT_LOTSIZEDAYSBULK, CT_LOTSIZEDAYSFPPCOMOP, CT_LOTSIZEDAYSFPPSITE, CT_LOTSIZEDAYSFPU, CT_LOTSIZEDAYSRAW, CT_LOTSIZEDAYS_ANTIGEN_1, CT_LOTSIZEDAYS_ANTIGEN_2, 
CT_LOTSIZEDAYS_ANTIGEN_3, CT_LOTSIZEDAYS_ANTIGEN_4, CT_LOTSIZEDAYS_ANTIGEN_5, CT_LOTSIZEDAYS_BULK_1, CT_LOTSIZEDAYS_BULK_2, CT_LOTSIZEDAYS_BULK_3, 
CT_LOTSIZEDAYS_COMOPS, CT_LOTSIZEDAYS_FPP_1, CT_LOTSIZEDAYS_FPP_2, CT_LOTSIZEDAYS_FPP_3, CT_LOTSIZEDAYS_FPU_1, CT_LOTSIZEDAYS_FPU_2, CT_LOTSIZEDAYS_FPU_3, 
CT_LOTSIZEDAYS_FPU_4, CT_LOTSIZEDAYS_FPU_5, CT_LOTSIZEDAYS_RAW_1, CT_MINRELEASEDSTOCKDAYSBULK, CT_MINRELEASEDSTOCKDAYSFPPCOMOP, CT_MINRELEASEDSTOCKDAYSFPPSITE, 
CT_MINRELEASEDSTOCKDAYSFPU, CT_MINRELEASEDSTOCKDAYSRAW, CT_MINRELEASEDSTOCKDAYS_ANTIGEN_1, CT_MINRELEASEDSTOCKDAYS_ANTIGEN_2, CT_MINRELEASEDSTOCKDAYS_ANTIGEN_3, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_4, CT_MINRELEASEDSTOCKDAYS_ANTIGEN_5, CT_MINRELEASEDSTOCKDAYS_BULK_1, CT_MINRELEASEDSTOCKDAYS_BULK_2, CT_MINRELEASEDSTOCKDAYS_BULK_3, 
CT_MINRELEASEDSTOCKDAYS_COMOPS, CT_MINRELEASEDSTOCKDAYS_FPP_1, CT_MINRELEASEDSTOCKDAYS_FPP_2, CT_MINRELEASEDSTOCKDAYS_FPP_3, CT_MINRELEASEDSTOCKDAYS_FPU_1,
 CT_MINRELEASEDSTOCKDAYS_FPU_2, CT_MINRELEASEDSTOCKDAYS_FPU_3, CT_MINRELEASEDSTOCKDAYS_FPU_4, CT_MINRELEASEDSTOCKDAYS_FPU_5, CT_MINRELEASEDSTOCKDAYS_RAW_1,
 CT_ORDERAMOUNTSALESFPP, CT_ORDERAMOUNTSALESFPP2, CT_ORDERAMOUNTSALESFPP_2, CT_OVERALLSUPPLYHEALTHSCORE, CT_QTYDELIVERED, CT_TARGETE2E_LT, CT_TARGETE2E_LT_ANTIGEN,
 CT_TARGETE2E_LT_BULK, CT_TARGETE2E_LT_FPU, CT_TARGETLTANTIGEN, CT_TARGETLTANTIGEN1, CT_TARGETLTANTIGEN2, CT_TARGETLTANTIGEN3, CT_TARGETLTANTIGEN4, CT_TARGETLTANTIGEN5,
 CT_TARGETLTBULK, CT_TARGETLTBULK1, CT_TARGETLTBULK2, CT_TARGETLTBULK3, CT_TARGETLTFPP, CT_TARGETLTFPP1, CT_TARGETLTFPP2,
 CT_TARGETLTFPP3, CT_TARGETLTFPU, CT_TARGETLTFPU1, CT_TARGETLTFPU2, CT_TARGETLTFPU3, CT_TARGETLTFPU4, CT_TARGETLTFPU5, CT_TARGETLTRAW, CT_TARGETLTRAW1, CT_TARGETLTSALES,
 CT_TARGETMASTERRECIPEBULK, CT_TARGETMASTERRECIPEFPP, CT_TARGETMASTERRECIPEFPP_COMOP, CT_TARGETMASTERRECIPEFPU, CT_TARGETMASTERRECIPERAW, CT_TARGETMASTERRECIPE_ANTIGEN_1, 
CT_TARGETMASTERRECIPE_ANTIGEN_2, CT_TARGETMASTERRECIPE_ANTIGEN_3, CT_TARGETMASTERRECIPE_ANTIGEN_4, CT_TARGETMASTERRECIPE_ANTIGEN_5, CT_TARGETMASTERRECIPE_BULK_1,
 CT_TARGETMASTERRECIPE_BULK_2, CT_TARGETMASTERRECIPE_BULK_3, CT_TARGETMASTERRECIPE_COMOPS, CT_TARGETMASTERRECIPE_FPP_1, CT_TARGETMASTERRECIPE_FPP_2, 
CT_TARGETMASTERRECIPE_FPP_3, CT_TARGETMASTERRECIPE_FPU_1, CT_TARGETMASTERRECIPE_FPU_2, CT_TARGETMASTERRECIPE_FPU_3, CT_TARGETMASTERRECIPE_FPU_4, 
CT_TARGETMASTERRECIPE_FPU_5, CT_TARGETMASTERRECIPE_RAW_1, CT_WEIGHTEDACTUALLT, CT_WEIGHTEDTARGETLT, dim_dateidpostingdate,
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_1,
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_2,
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_3,
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_4,
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_5,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK_1,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_FPP_1,
DIM_DATEIDUSAGEDECISIONMADE_FPP_2,
DIM_DATEIDUSAGEDECISIONMADE_FPP_3,
DIM_DATEIDUSAGEDECISIONMADE_FPU_1,
DIM_DATEIDUSAGEDECISIONMADE_FPU_2,
DIM_DATEIDUSAGEDECISIONMADE_FPU_3,
DIM_DATEIDUSAGEDECISIONMADE_FPU_4,
DIM_DATEIDUSAGEDECISIONMADE_FPU_5,
DIM_DATEIDUSAGEDECISIONMADE_RAW_1 
from fact_scm fmm
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
  /* inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid */
  inner join dim_part p on fmm.dim_partid_fpp_final = p.dim_partid
  inner join dim_part pp on fmm.dim_partid_antigen_1 = pp.dim_partid
where 
  /* agidcc.datevalue >= add_years(current_date, -2) and */
   ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')));

/************************************** RAW *************************************/
drop table if exists tmp_scm_for_actualraw;
create table tmp_scm_for_actualraw as
select  distinct  dim_partid_raw_1, dim_batchid_raw_1, DD_INSPECTIONLOTNO_RAW_1,		gpf_fpp_final,
/*Actual LT RAW */
MAX( CASE WHEN (dahfant1.DateValue) <> '0001-01-01'
	THEN (dahfant1.DateValue) 
	WHEN (afdb.DateValue) <> '0001-01-01'
	THEN (afdb.DateValue) 
	WHEN (afdf.DateValue) <> '0001-01-01'
	THEN (afdf.DateValue) 
	ELSE (afdfp.DateValue) 
	END
	- (grdr.DateValue) 
	) *5/7 as ct_actualLTRaw,
/*Actual LT RAW1 */
MAX( 
  CASE WHEN (grdr.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
  CASE WHEN (darant1.DateValue) <> '0001-01-01' THEN (darant1.DateValue) 
	       WHEN (ardb.DateValue) <> '0001-01-01' THEN (ardb.DateValue) 
	        WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
       ELSE (ardfp.DateValue) 
	   END )
	- (grdr.DateValue) 
  END
	)  *5/7 as ct_actualLTRaw1,

/*Actual LT RAW1 QC - LVL 3*/
MAX( 
  CASE WHEN (grdr.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (udmdr.datevalue)
  - (grdr.DateValue) 
  END
  )  *5/7 as ct_actual_Raw1_qc,

/*Actual LT RAW1 RL - LVL 3*/
MAX( 
  CASE WHEN (udmdr.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
  CASE WHEN (darant1.DateValue) <> '0001-01-01' THEN (darant1.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01' THEN (ardb.DateValue) 
          WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
       ELSE (ardfp.DateValue) 
     END )
  - (udmdr.DateValue) 
  END
  )  *5/7 as ct_actual_Raw1_rl,

/*Target LT RAW */
MAX( 
	(ma.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 + (CT_LOTSIZEDAYS_RAW_1) *5/7/2  +

	+( CASE WHEN (dahfant1.DateValue) <> '0001-01-01' /*check antigen*/
		THEN (CT_FLOATBEFOREPRODUCTION_ANTIGEN_1) + (CT_TARGETMASTERRECIPE_ANTIGEN_1) 	 WHEN (afdb.DateValue) <> '0001-01-01' /*check bulk*/
		THEN (CT_FLOATBEFOREPRODUCTION_BULK_1) + (CT_TARGETMASTERRECIPE_BULK_1)   WHEN (afdf.DateValue) <> '0001-01-01' /*check fpu*/
		THEN (CT_FLOATBEFOREPRODUCTION_FPU_1) + (CT_TARGETMASTERRECIPE_FPU_1) 		  WHEN (afdfp.DateValue) <> '0001-01-01' /*check fpp*/
		 THEN(CT_FLOATBEFOREPRODUCTION_FPP_1) + (CT_TARGETMASTERRECIPE_FPP_1) 
		ELSE 0 END)
		
		) as ct_targetLTRaw,
/*Target LT RAW1*/
MAX( 
	(ma.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 + (CT_LOTSIZEDAYS_RAW_1) *5/7/2  
		) as ct_targetLTRaw1,

/*Target LT RAW1 QC - LVL 3*/
MAX( 
  (ma.GRProcessingTime) 
    ) as ct_target_Raw1_qc,

/*Target LT RAW1  RL - LVL 3*/
MAX( 
   (CT_MINRELEASEDSTOCKDAYS_RAW_1) *5/7 + (CT_LOTSIZEDAYS_RAW_1) *5/7/2  
    ) as ct_target_Raw1_rl

	from fact_scm_for_measure_updates fmm 
          inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
					inner join dim_date grdr on fmm.dim_dateidpostingdate = grdr.dim_dateid
					inner join dim_date afdb on fmm.dim_dateidactualheaderfinish_bulk_1 = afdb.dim_dateid
					inner join dim_date afdf on fmm.dim_dateidactualheaderfinish_fpu_1 = afdf.dim_dateid
					inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid	
					inner join dim_date dahfant1 on fmm.dim_dateidactualheaderfinish_antigen_1 = dahfant1.dim_dateid
          inner join dim_date darant1 on fmm.dim_dateidactualrelease_antigen_1 = darant1.dim_dateid
          inner join dim_date ardb on fmm.dim_dateidactualrelease_bulk_1 = ardb.dim_dateid
          inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
          inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
          inner join dim_date udmdr on fmm.dim_dateidusagedecisionmade_raw_1 = udmdr.dim_dateid
group by dim_partid_raw_1, dim_batchid_raw_1, DD_INSPECTIONLOTNO_RAW_1,  gpf_fpp_final;

update fact_scm fmm
set 	
fmm.ct_actualLTRaw = upd.ct_actualLTRaw,
fmm.ct_actualLTRaw1 = upd.ct_actualLTRaw1,
fmm.ct_actual_Raw1_qc = upd.ct_actual_Raw1_qc,
fmm.ct_actual_Raw1_rl = upd.ct_actual_Raw1_rl,
fmm.ct_targetLTRaw = upd.ct_targetLTRaw,
fmm.ct_targetLTRaw1 = upd.ct_targetLTRaw1,
fmm.ct_target_Raw1_qc = upd.ct_target_Raw1_qc,
fmm.ct_target_Raw1_rl = upd.ct_target_Raw1_rl
from fact_scm fmm inner join tmp_scm_for_actualraw  upd
on  fmm.dim_partid_raw_1 = upd.dim_partid_raw_1
	and fmm.dim_batchid_raw_1 = upd.dim_batchid_raw_1
	and fmm.DD_INSPECTIONLOTNO_RAW_1 = upd.DD_INSPECTIONLOTNO_RAW_1
		and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where 	 ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
		( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
	/* and agidcc.datevalue >= add_years(current_date, -2) */;

/*********************************************** ANTIGEN related measures ********************************/
/*************** ANTIGEN  & ANTIGEN 1 ************************/
drop table if exists tmp_scm_for_actualantigen;
create table tmp_scm_for_actualantigen as
select  distinct  dim_partid_antigen_1, dim_batchid_antigen_1, DD_INSPECTIONLOTNO_ANTIGEN_1,  gpf_fpp_final,
    
/*Actual LT Antigen */
MAX( 
  CASE WHEN (darant1.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (afdb.DateValue) <> '0001-01-01'
    THEN (afdb.DateValue) 
    WHEN (afdf.DateValue) <> '0001-01-01'
    THEN (afdf.DateValue) 
    ELSE (afdfp.DateValue) 
    END )
  - (darant1.DateValue) 
  END
  ) *5/7 as ct_actualLTAntigen,
/*Actual LT Antigen1 */
MAX( 
  CASE WHEN (darant1.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (darant2.DateValue) <> '0001-01-01' THEN (darant2.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (darant1.DateValue) 
  END
  ) *5/7 as ct_actualLTAntigen1,

/*Actual LT Antigen1 PRD - LVL 3*/
MAX( 
  CASE WHEN (darant1.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (dahfant1.DateValue) 
  - (darant1.DateValue) 
  END
  ) *5/7 as ct_actual_antigen1_prd,

/*Actual LT Antigen1 QC - LVL 3*/
MAX( 
  CASE WHEN (dahfant1.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
   (dudmant1.DateValue)
  - (dahfant1.DateValue) 
  END
  ) *5/7 as ct_actual_antigen1_qc,

/*Actual LT Antigen1 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmant1.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (darant2.DateValue) <> '0001-01-01' THEN (darant2.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (dudmant1.DateValue) 
  END
  ) *5/7 as ct_actual_antigen1_rl,
  
/*Target LT Antigen*/
MAX( 
CASE WHEN (darant1.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_1) + (CT_TARGETMASTERRECIPE_ANTIGEN_1) + (pprtant1.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_1) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_1) *5/7/2  +
   (CT_FLOATBEFOREPRODUCTION_ANTIGEN_2) + (CT_TARGETMASTERRECIPE_ANTIGEN_2) + (pprtant2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_2) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_2) *5/7/2 +
   (CT_FLOATBEFOREPRODUCTION_ANTIGEN_3) + (CT_TARGETMASTERRECIPE_ANTIGEN_3) + (pprtant3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_3) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_3) *5/7/2 +
   (CT_FLOATBEFOREPRODUCTION_ANTIGEN_4) + (CT_TARGETMASTERRECIPE_ANTIGEN_4) + (pprtant4.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_4) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_4) *5/7/2 +
   (CT_FLOATBEFOREPRODUCTION_ANTIGEN_5) + (CT_TARGETMASTERRECIPE_ANTIGEN_5) + (pprtant5.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_5) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_5) *5/7/2
  +( CASE WHEN (afdb.DateValue) <> '0001-01-01' /*check bulk*/
    THEN (CT_FLOATBEFOREPRODUCTION_BULK_1) + (CT_TARGETMASTERRECIPE_BULK_1) /* +
    (CT_FLOATBEFOREPRODUCTION_BULK_2) + (CT_TARGETMASTERRECIPE_BULK_2) +
     (CT_FLOATBEFOREPRODUCTION_BULK_3) + (CT_TARGETMASTERRECIPE_BULK_3) */
     WHEN (afdf.DateValue) <> '0001-01-01' /*check fpu*/
    THEN (CT_FLOATBEFOREPRODUCTION_FPU_1) + (CT_TARGETMASTERRECIPE_FPU_1) /* +
     (CT_FLOATBEFOREPRODUCTION_FPU_2) + (CT_TARGETMASTERRECIPE_FPU_2) +
     (CT_FLOATBEFOREPRODUCTION_FPU_3) + (CT_TARGETMASTERRECIPE_FPU_3) +
     (CT_FLOATBEFOREPRODUCTION_FPU_4) + (CT_TARGETMASTERRECIPE_FPU_4) +
     (CT_FLOATBEFOREPRODUCTION_FPU_5) + (CT_TARGETMASTERRECIPE_FPU_5) */
      WHEN (afdfp.DateValue) <> '0001-01-01' /*check fpp*/
     THEN (CT_FLOATBEFOREPRODUCTION_FPP_1) + (CT_TARGETMASTERRECIPE_FPP_1) /*+
     (CT_FLOATBEFOREPRODUCTION_FPP_2) + (CT_TARGETMASTERRECIPE_FPP_2) +
     (CT_FLOATBEFOREPRODUCTION_FPP_3) + (CT_TARGETMASTERRECIPE_FPP_3) */
    ELSE 0 END)
    END
    ) as ct_targetLTAntigen,
/*Target LT Antigen1*/
MAX( 
CASE WHEN (darant1.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_1) + (CT_TARGETMASTERRECIPE_ANTIGEN_1) + (pprtant1.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_1) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_1) *5/7/2  
  END
    ) as ct_targetLTAntigen1,

/*Target LT Antigen1 PRD - LVL 3*/
MAX( 
CASE WHEN (darant1.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_1) + (CT_TARGETMASTERRECIPE_ANTIGEN_1) 
  END
    ) as ct_target_antigen1_prd,

/*Target LT Antigen1 QC - LVL 3*/
MAX( 
CASE WHEN (darant1.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (pprtant1.GRProcessingTime)   
  END
    ) as ct_target_antigen1_qc,

/*Target LT Antigen1 RL - LVL 3*/
MAX( 
CASE WHEN (darant1.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_1) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_1) *5/7/2  
  END
    ) as ct_target_antigen1_rl

from fact_scm_for_measure_updates fmm 
  inner join dim_part pprtant1 on fmm.dim_partid_antigen_1 = pprtant1.dim_partid
  inner join dim_part pprtant2 on fmm.dim_partid_antigen_2 = pprtant2.dim_partid
  inner join dim_part pprtant3 on fmm.dim_partid_antigen_3 = pprtant3.dim_partid
  inner join dim_part pprtant4 on fmm.dim_partid_antigen_4 = pprtant4.dim_partid
  inner join dim_part pprtant5 on fmm.dim_partid_antigen_5 = pprtant5.dim_partid
  inner join dim_part p on fmm.dim_partid_fpp_final = p.dim_partid
  inner join dim_part pp on fmm.dim_partid_antigen_1 = pp.dim_partid
  inner join dim_date darant1 on fmm.dim_dateidactualrelease_antigen_1 = darant1.dim_dateid
  inner join dim_date darant2 on fmm.dim_dateidactualrelease_antigen_2 = darant2.dim_dateid
    inner join dim_date ardb on fmm.dim_dateidactualrelease_bulk_1 = ardb.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
  inner join dim_date afdb on fmm.dim_dateidactualheaderfinish_bulk_1 = afdb.dim_dateid
  inner join dim_date afdf on fmm.dim_dateidactualheaderfinish_fpu_1 = afdf.dim_dateid
  inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid
  inner join dim_date dahfant1 on fmm.dim_dateidactualheaderfinish_antigen_1 = dahfant1.dim_dateid
  inner join dim_date dudmant1 on fmm.dim_dateidusagedecisionmade_antigen_1 = dudmant1.dim_dateid
group by dim_partid_antigen_1, dim_batchid_antigen_1, DD_INSPECTIONLOTNO_ANTIGEN_1, gpf_fpp_final
         ;


update fact_scm fmm
set   
fmm.ct_actualLTAntigen = upd.ct_actualLTAntigen,
fmm.ct_targetLTAntigen = upd.ct_targetLTAntigen,
fmm.ct_actualLTAntigen1 = upd.ct_actualLTAntigen1,
fmm.ct_actual_antigen1_prd = upd.ct_actual_antigen1_prd,
fmm.ct_actual_antigen1_qc = upd.ct_actual_antigen1_qc,
fmm.ct_actual_antigen1_rl = upd.ct_actual_antigen1_rl,
fmm.ct_targetLTAntigen1 = upd.ct_targetLTAntigen1,
fmm.ct_target_antigen1_prd = upd.ct_target_antigen1_prd,
fmm.ct_target_antigen1_qc = upd.ct_target_antigen1_qc,
fmm.ct_target_antigen1_rl = upd.ct_target_antigen1_rl
from fact_scm fmm inner join tmp_scm_for_actualantigen  upd
on  fmm.dim_partid_antigen_1 = upd.dim_partid_antigen_1
  and fmm.dim_batchid_antigen_1 = upd.dim_batchid_antigen_1
  and fmm.DD_INSPECTIONLOTNO_ANTIGEN_1 = upd.DD_INSPECTIONLOTNO_ANTIGEN_1
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;


/****** Antigen 2 ******************/
drop table if exists tmp_scm_for_actualantigen2;
create table tmp_scm_for_actualantigen2 as
select  distinct  dim_partid_antigen_2, dim_batchid_antigen_2, DD_INSPECTIONLOTNO_ANTIGEN_2,  gpf_fpp_final,
    

/*Actual LT Antigen2 */
MAX( 
  CASE WHEN (darant2.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (darant3.DateValue) <> '0001-01-01' THEN (darant3.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (darant2.DateValue) 
  END
  ) *5/7 as ct_actualLTAntigen2,

/*Actual LT Antigen2 PRD - LVL 3*/
MAX( 
  CASE WHEN (darant2.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (dahfant2.DateValue) 
  - (darant2.DateValue) 
  END
  ) *5/7 as ct_actual_antigen2_prd,

/*Actual LT Antigen2 QC - LVL 3*/
MAX( 
  CASE WHEN (dahfant2.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
   (dudmant2.DateValue)
  - (dahfant2.DateValue) 
  END
  ) *5/7 as ct_actual_antigen2_qc,

/*Actual LT Antigen2 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmant2.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (darant3.DateValue) <> '0001-01-01' THEN (darant3.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (dudmant2.DateValue) 
  END
  ) *5/7 as ct_actual_antigen2_rl,  

/*Target LT Antigen2*/
MAX( 
CASE WHEN (darant2.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_2) + (CT_TARGETMASTERRECIPE_ANTIGEN_2) + (pprtant2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_2) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_2) *5/7/2  
  END
    ) as ct_targetLTAntigen2,
  
/*Target LT Antigen2 PRD - LVL 3*/
MAX( 
CASE WHEN (darant2.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_2) + (CT_TARGETMASTERRECIPE_ANTIGEN_2) 
  END
    ) as ct_target_antigen2_prd,

/*Target LT Antigen2 QC - LVL 3*/
MAX( 
CASE WHEN (darant2.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (pprtant2.GRProcessingTime)   
  END
    ) as ct_target_antigen2_qc,

/*Target LT Antigen2 RL - LVL 3*/
MAX( 
CASE WHEN (darant2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_2) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_2) *5/7/2  
  END
    ) as ct_target_antigen2_rl

from fact_scm_for_measure_updates fmm 
  inner join dim_part pprtant2 on fmm.dim_partid_antigen_2 = pprtant2.dim_partid
  inner join dim_date darant2 on fmm.dim_dateidactualrelease_antigen_2 = darant2.dim_dateid
  inner join dim_date darant3 on fmm.dim_dateidactualrelease_antigen_3 = darant3.dim_dateid
    inner join dim_date ardb on fmm.dim_dateidactualrelease_bulk_1 = ardb.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
  inner join dim_date dahfant2 on fmm.dim_dateidactualheaderfinish_antigen_2 = dahfant2.dim_dateid
  inner join dim_date dudmant2 on fmm.dim_dateidusagedecisionmade_antigen_2 = dudmant2.dim_dateid
group by dim_partid_antigen_2, dim_batchid_antigen_2, DD_INSPECTIONLOTNO_ANTIGEN_2, gpf_fpp_final
         ;

update fact_scm fmm
set   
fmm.ct_actualLTAntigen2 = upd.ct_actualLTAntigen2,
fmm.ct_targetLTAntigen2 = upd.ct_targetLTAntigen2,
fmm.ct_actual_antigen2_prd = upd.ct_actual_antigen2_prd,
fmm.ct_actual_antigen2_qc = upd.ct_actual_antigen2_qc,
fmm.ct_actual_antigen2_rl = upd.ct_actual_antigen2_rl,
fmm.ct_target_antigen2_prd = upd.ct_target_antigen2_prd,
fmm.ct_target_antigen2_qc = upd.ct_target_antigen2_qc,
fmm.ct_target_antigen2_rl = upd.ct_target_antigen2_rl
from fact_scm fmm inner join tmp_scm_for_actualantigen2  upd
on  fmm.dim_partid_antigen_2 = upd.dim_partid_antigen_2
  and fmm.dim_batchid_antigen_2 = upd.dim_batchid_antigen_2
  and fmm.DD_INSPECTIONLOTNO_ANTIGEN_2 = upd.DD_INSPECTIONLOTNO_ANTIGEN_2
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;

/****** Antigen 3 ******************/
  drop table if exists tmp_scm_for_actualantigen3;
create table tmp_scm_for_actualantigen3 as
select  distinct  dim_partid_antigen_3, dim_batchid_antigen_3, DD_INSPECTIONLOTNO_ANTIGEN_3,  gpf_fpp_final,
    

/*Actual LT Antigen3 */
MAX( 
  CASE WHEN (darant3.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (darant4.DateValue) <> '0001-01-01' THEN (darant4.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (darant3.DateValue) 
  END
  ) *5/7 as ct_actualLTAntigen3,
  
/*Actual LT Antigen3 PRD - LVL 3*/
MAX( 
  CASE WHEN (darant3.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (dahfant3.DateValue) 
  - (darant3.DateValue) 
  END
  ) *5/7 as ct_actual_antigen3_prd,

/*Actual LT Antigen3 QC - LVL 3*/
MAX( 
  CASE WHEN (dahfant3.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
   (dudmant3.DateValue)
  - (dahfant3.DateValue) 
  END
  ) *5/7 as ct_actual_antigen3_qc,

/*Actual LT Antigen3 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmant3.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (darant4.DateValue) <> '0001-01-01' THEN (darant4.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (dudmant3.DateValue) 
  END
  ) *5/7 as ct_actual_antigen3_rl,  
  

/*Target LT Antigen3*/
MAX( 
CASE WHEN (darant3.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_3) + (CT_TARGETMASTERRECIPE_ANTIGEN_3) + (pprtant3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_3) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_3) *5/7/2  
  END
    ) as ct_targetLTAntigen3,
  
/*Target LT Antigen3 PRD - LVL 3*/
MAX( 
CASE WHEN (darant3.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_3) + (CT_TARGETMASTERRECIPE_ANTIGEN_3) 
  END
    ) as ct_target_antigen3_prd,

/*Target LT Antigen3 QC - LVL 3*/
MAX( 
CASE WHEN (darant3.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (pprtant3.GRProcessingTime)   
  END
    ) as ct_target_antigen3_qc,

/*Target LT Antigen3 RL - LVL 3*/
MAX( 
CASE WHEN (darant3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_3) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_3) *5/7/2  
  END
    ) as ct_target_antigen3_rl

from fact_scm_for_measure_updates fmm 
  inner join dim_part pprtant3 on fmm.dim_partid_antigen_3 = pprtant3.dim_partid
  inner join dim_date darant3 on fmm.dim_dateidactualrelease_antigen_3 = darant3.dim_dateid
  inner join dim_date darant4 on fmm.dim_dateidactualrelease_antigen_4 = darant4.dim_dateid
    inner join dim_date ardb on fmm.dim_dateidactualrelease_bulk_1 = ardb.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
  inner join dim_date dahfant3 on fmm.dim_dateidactualheaderfinish_antigen_3 = dahfant3.dim_dateid
  inner join dim_date dudmant3 on fmm.dim_dateidusagedecisionmade_antigen_3 = dudmant3.dim_dateid
group by dim_partid_antigen_3, dim_batchid_antigen_3, DD_INSPECTIONLOTNO_ANTIGEN_3, gpf_fpp_final
         ;

update fact_scm fmm
set   
fmm.ct_actualLTAntigen3 = upd.ct_actualLTAntigen3,
fmm.ct_targetLTAntigen3 = upd.ct_targetLTAntigen3,
fmm.ct_actual_antigen3_prd = upd.ct_actual_antigen3_prd,
fmm.ct_actual_antigen3_qc = upd.ct_actual_antigen3_qc,
fmm.ct_actual_antigen3_rl = upd.ct_actual_antigen3_rl,
fmm.ct_target_antigen3_prd = upd.ct_target_antigen3_prd,
fmm.ct_target_antigen3_qc = upd.ct_target_antigen3_qc,
fmm.ct_target_antigen3_rl = upd.ct_target_antigen3_rl
from fact_scm fmm inner join tmp_scm_for_actualantigen3  upd
on  fmm.dim_partid_antigen_3 = upd.dim_partid_antigen_3
  and fmm.dim_batchid_antigen_3 = upd.dim_batchid_antigen_3
  and fmm.DD_INSPECTIONLOTNO_ANTIGEN_3 = upd.DD_INSPECTIONLOTNO_ANTIGEN_3
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;

/****** Antigen 4 ******************/
drop table if exists tmp_scm_for_actualantigen4;
create table tmp_scm_for_actualantigen4 as
select  distinct  dim_partid_antigen_4, dim_batchid_antigen_4, DD_INSPECTIONLOTNO_ANTIGEN_4,  gpf_fpp_final,
    

/*Actual LT Antigen4 */
MAX( 
  CASE WHEN (darant4.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (darant5.DateValue) <> '0001-01-01' THEN (darant5.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (darant4.DateValue) 
  END
  ) *5/7 as ct_actualLTAntigen4,
  
/*Actual LT Antigen4 PRD - LVL 3*/
MAX( 
  CASE WHEN (darant4.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (dahfant4.DateValue) 
  - (darant4.DateValue) 
  END
  ) *5/7 as ct_actual_antigen4_prd,

/*Actual LT Antigen4 QC - LVL 3*/
MAX( 
  CASE WHEN (dahfant4.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
   (dudmant4.DateValue)
  - (dahfant4.DateValue) 
  END
  ) *5/7 as ct_actual_antigen4_qc,

/*Actual LT Antigen4 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmant4.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE WHEN (darant5.DateValue) <> '0001-01-01' THEN (darant5.DateValue) 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (dudmant4.DateValue) 
  END
  ) *5/7 as ct_actual_antigen4_rl, 

/*Target LT Antigen4*/
MAX( 
CASE WHEN (darant4.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_4) + (CT_TARGETMASTERRECIPE_ANTIGEN_4) + (pprtant4.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_4) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_4) *5/7/2  
  END
    ) as ct_targetLTAntigen4,
  
/*Target LT Antigen4 PRD - LVL 3*/
MAX( 
CASE WHEN (darant4.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_4) + (CT_TARGETMASTERRECIPE_ANTIGEN_4) 
  END
    ) as ct_target_antigen4_prd,

/*Target LT Antigen4 QC - LVL 3*/
MAX( 
CASE WHEN (darant4.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (pprtant4.GRProcessingTime)   
  END
    ) as ct_target_antigen4_qc,

/*Target LT Antigen4 RL - LVL 3*/
MAX( 
CASE WHEN (darant4.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_4) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_4) *5/7/2  
  END
    ) as ct_target_antigen4_rl

from fact_scm_for_measure_updates fmm 
  inner join dim_part pprtant4 on fmm.dim_partid_antigen_4 = pprtant4.dim_partid
  inner join dim_date darant4 on fmm.dim_dateidactualrelease_antigen_4 = darant4.dim_dateid
  inner join dim_date darant5 on fmm.dim_dateidactualrelease_antigen_5 = darant5.dim_dateid
    inner join dim_date ardb on fmm.dim_dateidactualrelease_bulk_1 = ardb.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
  inner join dim_date dahfant4 on fmm.dim_dateidactualheaderfinish_antigen_4 = dahfant4.dim_dateid
  inner join dim_date dudmant4 on fmm.dim_dateidusagedecisionmade_antigen_4 = dudmant4.dim_dateid
group by dim_partid_antigen_4, dim_batchid_antigen_4, DD_INSPECTIONLOTNO_ANTIGEN_4, gpf_fpp_final
         ;

update fact_scm fmm
set   
fmm.ct_actualLTAntigen4 = upd.ct_actualLTAntigen4,
fmm.ct_targetLTAntigen4 = upd.ct_targetLTAntigen4,
fmm.ct_actual_antigen4_prd = upd.ct_actual_antigen4_prd,
fmm.ct_actual_antigen4_qc = upd.ct_actual_antigen4_qc,
fmm.ct_actual_antigen4_rl = upd.ct_actual_antigen4_rl,
fmm.ct_target_antigen4_prd = upd.ct_target_antigen4_prd,
fmm.ct_target_antigen4_qc = upd.ct_target_antigen4_qc,
fmm.ct_target_antigen4_rl = upd.ct_target_antigen4_rl
from fact_scm fmm inner join tmp_scm_for_actualantigen4  upd
on  fmm.dim_partid_antigen_4 = upd.dim_partid_antigen_4
  and fmm.dim_batchid_antigen_4 = upd.dim_batchid_antigen_4
  and fmm.DD_INSPECTIONLOTNO_ANTIGEN_4 = upd.DD_INSPECTIONLOTNO_ANTIGEN_4
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;

/****** Antigen 5 ******************/
drop table if exists tmp_scm_for_actualantigen5;
create table tmp_scm_for_actualantigen5 as
select  distinct  dim_partid_antigen_5, dim_batchid_antigen_5, DD_INSPECTIONLOTNO_ANTIGEN_5,  gpf_fpp_final,
    

/*Actual LT Antigen5 */
MAX( 
  CASE WHEN (darant5.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE 
         WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (darant5.DateValue) 
  END
  ) *5/7 as ct_actualLTAntigen5,

/*Actual LT Antigen5 PRD - LVL 3*/
MAX( 
  CASE WHEN (darant5.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (dahfant5.DateValue) 
  - (darant5.DateValue) 
  END
  ) *5/7 as ct_actual_antigen5_prd,

/*Actual LT Antigen5 QC - LVL 3*/
MAX( 
  CASE WHEN (dahfant5.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
   (dudmant5.DateValue)
  - (dahfant5.DateValue) 
  END
  ) *5/7 as ct_actual_antigen5_qc,

/*Actual LT Antigen5 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmant5.DateValue) = '0001-01-01'
  THEN 0
  ELSE (
    CASE    WHEN (ardb.DateValue) <> '0001-01-01'  THEN (ardb.DateValue) 
             WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (dudmant5.DateValue) 
  END
  ) *5/7 as ct_actual_antigen5_rl,   

/*Target LT Antigen5*/
MAX( 
CASE WHEN (darant5.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_5) + (CT_TARGETMASTERRECIPE_ANTIGEN_5) + (pprtant5.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_5) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_5) *5/7/2  
  END
    ) as ct_targetLTAntigen5,

/*Target LT Antigen5 PRD - LVL 3*/
MAX( 
CASE WHEN (darant5.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (CT_FLOATBEFOREPRODUCTION_ANTIGEN_5) + (CT_TARGETMASTERRECIPE_ANTIGEN_5) 
  END
    ) as ct_target_antigen5_prd,

/*Target LT Antigen5 QC - LVL 3*/
MAX( 
CASE WHEN (darant5.DateValue) = '0001-01-01'
THEN 0
ELSE 
  (pprtant5.GRProcessingTime)   
  END
    ) as ct_target_antigen5_qc,

/*Target LT Antigen5 RL - LVL 3*/
MAX( 
CASE WHEN (darant5.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_MINRELEASEDSTOCKDAYS_ANTIGEN_5) *5/7 + (CT_LOTSIZEDAYS_ANTIGEN_5) *5/7/2  
  END
    ) as ct_target_antigen5_rl

from fact_scm_for_measure_updates fmm 
   inner join dim_part pprtant5 on fmm.dim_partid_antigen_5 = pprtant5.dim_partid
  inner join dim_date darant5 on fmm.dim_dateidactualrelease_antigen_5 = darant5.dim_dateid
    inner join dim_date ardb on fmm.dim_dateidactualrelease_bulk_1 = ardb.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
  inner join dim_date dahfant5 on fmm.dim_dateidactualheaderfinish_antigen_5 = dahfant5.dim_dateid
  inner join dim_date dudmant5 on fmm.dim_dateidusagedecisionmade_antigen_5 = dudmant5.dim_dateid
group by dim_partid_antigen_5, dim_batchid_antigen_5, DD_INSPECTIONLOTNO_ANTIGEN_5, gpf_fpp_final
         ;

update fact_scm fmm
set   
fmm.ct_actualLTAntigen5 = upd.ct_actualLTAntigen5,
fmm.ct_targetLTAntigen5 = upd.ct_targetLTAntigen5,
fmm.ct_actual_antigen5_prd = upd.ct_actual_antigen5_prd,
fmm.ct_actual_antigen5_qc = upd.ct_actual_antigen5_qc,
fmm.ct_actual_antigen5_rl = upd.ct_actual_antigen5_rl,
fmm.ct_target_antigen5_prd = upd.ct_target_antigen5_prd,
fmm.ct_target_antigen5_qc = upd.ct_target_antigen5_qc,
fmm.ct_target_antigen5_rl = upd.ct_target_antigen5_rl
from fact_scm fmm inner join tmp_scm_for_actualantigen5  upd
on  fmm.dim_partid_antigen_5 = upd.dim_partid_antigen_5
  and fmm.dim_batchid_antigen_5 = upd.dim_batchid_antigen_5
  and fmm.DD_INSPECTIONLOTNO_ANTIGEN_5 = upd.DD_INSPECTIONLOTNO_ANTIGEN_5
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
 /*  and agidcc.datevalue >= add_years(current_date, -2) */;

/*********************************************** BULK related measures ********************************/

/****************** BULK & BULK 1 ********************************/
drop table if exists tmp_scm_for_actualBulk_1;
create table tmp_scm_for_actualBulk_1 as
select  distinct  dim_partid_bulk_1, dim_batchid_bulk_1, dd_inspectionlotno_bulk_1,   gpf_fpp_final,

/*Actual LT Bulk */
MAX( 
  CASE WHEN (ardb.DateValue) = '0001-01-01'
  THEN 0
  ELSE (

  CASE WHEN (afdf.DateValue) <> '0001-01-01'
  THEN (afdf.DateValue) 
  ELSE (afdfp.DateValue) 
  END )
  - (ardb.DateValue) 
  END
  ) *5/7 as ct_actualLTBulk,

/*Actual LT Bulk1 */
MAX( 
  CASE WHEN (ardb.DateValue) = '0001-01-01'
  THEN 0
  ELSE (

  CASE WHEN (ardb2.DateValue) <> '0001-01-01'  THEN (ardb2.DateValue) 
       WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (ardb.DateValue) 
  END
  ) *5/7 as ct_actualLTBulk1,

/*Actual LT Bulk1 PRD - LVL 3*/
MAX( 
  CASE WHEN (ardb.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (afdb.DateValue)
  - (ardb.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk1_prd,

/*Actual LT Bulk1 QC - LVL 3*/
MAX( 
  CASE WHEN (afdb.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (udmdb.datevalue)
  - (afdb.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk1_qc,

/*Actual LT Bulk1 RL - LVL 3*/
MAX( 
  CASE WHEN (udmdb.DateValue) = '0001-01-01'
  THEN 0
  ELSE (

  CASE WHEN (ardb2.DateValue) <> '0001-01-01'  THEN (ardb2.DateValue) 
       WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (udmdb.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk1_rl,

/*Target LT BULK*/
MAX( 
CASE WHEN (ardb.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_BULK_1) + (CT_TARGETMASTERRECIPE_BULK_1) + (mab.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7 + (CT_LOTSIZEDAYS_BULK_1) *5/7/2 +
   (CT_FLOATBEFOREPRODUCTION_BULK_2) + (CT_TARGETMASTERRECIPE_BULK_2) + (mab2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_2) *5/7 + (CT_LOTSIZEDAYS_BULK_2) *5/7/2 +
   (CT_FLOATBEFOREPRODUCTION_BULK_3) + (CT_TARGETMASTERRECIPE_BULK_3) + (mab3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_3) *5/7 + (CT_LOTSIZEDAYS_BULK_3) *5/7/2 
  +( CASE WHEN (afdf.DateValue) <> '0001-01-01' /*check fpu*/
    THEN (CT_FLOATBEFOREPRODUCTION_FPU_1) + (CT_TARGETMASTERRECIPE_FPU_1) 
      WHEN (afdfp.DateValue) <> '0001-01-01' /*check fpp*/
     THEN (CT_FLOATBEFOREPRODUCTION_FPP_1) + (CT_TARGETMASTERRECIPE_FPP_1) 
    ELSE 0 END)
    END
    ) as ct_targetLTBULK,

/*Target LT BULK1*/
MAX( 
CASE WHEN (ardb.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_BULK_1) + (CT_TARGETMASTERRECIPE_BULK_1) + (mab.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7 + (CT_LOTSIZEDAYS_BULK_1) *5/7/2 

    END
    ) as ct_targetLTBULK1,

/*Target LT BULK1 PRD - LVL 3*/
MAX( 
CASE WHEN (ardb.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_BULK_1) + (CT_TARGETMASTERRECIPE_BULK_1) 

    END
    ) as ct_target_Bulk1_prd,

/*Target LT BULK1 QC - LVL 3*/
MAX( 
CASE WHEN (ardb.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (mab.GRProcessingTime) 

    END
    ) as ct_target_Bulk1_qc,

/*Target LT BULK1 RL - LVL 3*/
MAX( 
CASE WHEN (ardb.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (CT_MINRELEASEDSTOCKDAYS_BULK_1) *5/7 + (CT_LOTSIZEDAYS_BULK_1) *5/7/2 

    END
    ) as ct_target_Bulk1_rl

  from fact_scm_for_measure_updates fmm
    inner join dim_part mab on fmm.dim_partid_bulk_1 = mab.dim_partid
    inner join dim_part mab2 on fmm.dim_partid_bulk_2 = mab2.dim_partid
    inner join dim_part mab3 on fmm.dim_partid_bulk_3 = mab3.dim_partid
    inner join dim_date afdf on fmm.dim_dateidactualheaderfinish_fpu_1 = afdf.dim_dateid
    inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid
    inner join dim_date ardb on fmm.dim_dateidactualrelease_bulk_1 = ardb.dim_dateid
    inner join dim_date ardb2 on fmm.dim_dateidactualrelease_bulk_2 = ardb2.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
    inner join dim_date afdb on fmm.dim_dateidactualheaderfinish_bulk_1 = afdb.dim_dateid
    inner join dim_date udmdb on fmm.dim_dateidusagedecisionmade_bulk_1 = udmdb.dim_dateid
group by dim_partid_bulk_1, dim_batchid_bulk_1, dd_inspectionlotno_bulk_1,  gpf_fpp_final;

update fact_scm fmm
set 	
  fmm.ct_actualLTBulk = upd.ct_actualLTBulk,
  fmm.ct_actualLTBulk1 = upd.ct_actualLTBulk1,
  fmm.ct_actual_Bulk1_prd = upd.ct_actual_Bulk1_prd,
  fmm.ct_actual_Bulk1_qc = upd.ct_actual_Bulk1_qc,
  fmm.ct_actual_Bulk1_rl = upd.ct_actual_Bulk1_rl,
  fmm.ct_targetLTBULK = upd.ct_targetLTBULK,
  fmm.ct_targetLTBULK1 = upd.ct_targetLTBULK1,
  fmm.ct_target_Bulk1_prd = upd.ct_target_Bulk1_prd,
  fmm.ct_target_Bulk1_qc = upd.ct_target_Bulk1_qc,
  fmm.ct_target_Bulk1_rl = upd.ct_target_Bulk1_rl
from fact_scm fmm inner join tmp_scm_for_actualBulk_1  upd
on  fmm.dim_partid_bulk_1 = upd.dim_partid_bulk_1
	and fmm.dim_batchid_bulk_1 = upd.dim_batchid_bulk_1
	and fmm.dd_inspectionlotno_bulk_1 = upd.dd_inspectionlotno_bulk_1
	and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where 	 ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
		( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
	/* and agidcc.datevalue >= add_years(current_date, -2) */;

/****************** BULK 2 ********************************/
drop table if exists tmp_scm_for_actualBulk_2;
create table tmp_scm_for_actualBulk_2 as
select  distinct  dim_partid_bulk_2, dim_batchid_bulk_2, dd_inspectionlotno_bulk_2,   gpf_fpp_final,

/*Actual LT Bulk2 */
MAX( 
  CASE WHEN (ardb2.DateValue) = '0001-01-01'
  THEN 0
  ELSE (

  CASE WHEN (ardb3.DateValue) <> '0001-01-01'  THEN (ardb3.DateValue) 
       WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (ardb2.DateValue) 
  END
  ) *5/7 as ct_actualLTBulk2,

/*Actual LT Bulk2 PRD - LVL 3*/
MAX( 
  CASE WHEN (ardb2.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (afdb2.datevalue)
  - (ardb2.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk2_prd,

/*Actual LT Bulk2 QC - LVL 3*/
MAX( 
  CASE WHEN (afdb2.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (udmdb2.DateValue) 
  - (afdb2.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk2_qc,

/*Actual LT Bulk2 RL - LVL 3*/
MAX( 
  CASE WHEN (udmdb2.DateValue) = '0001-01-01'
  THEN 0
  ELSE (

  CASE WHEN (ardb3.DateValue) <> '0001-01-01'  THEN (ardb3.DateValue) 
       WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (udmdb2.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk2_rl,

/*Target LT BULK2*/
MAX( 
CASE WHEN (ardb2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_BULK_2) + (CT_TARGETMASTERRECIPE_BULK_2) + (mab2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_2) *5/7 + (CT_LOTSIZEDAYS_BULK_2) *5/7/2 

    END
    ) as ct_targetLTBULK2,

/*Target LT BULK2 PRD - LVL 3*/
MAX( 
CASE WHEN (ardb2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_BULK_2) + (CT_TARGETMASTERRECIPE_BULK_2) 

    END
    ) as ct_target_BULK2_prd,

/*Target LT BULK2 QC - LVL 3*/
MAX( 
CASE WHEN (ardb2.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (mab2.GRProcessingTime) 

    END
    ) as ct_target_BULK2_qc,

/*Target LT BULK2 RL - LVL 3*/
MAX( 
CASE WHEN (ardb2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_MINRELEASEDSTOCKDAYS_BULK_2) *5/7 + (CT_LOTSIZEDAYS_BULK_2) *5/7/2 

    END
    ) as ct_target_BULK2_rl

  from fact_scm_for_measure_updates fmm
    inner join dim_part mab2 on fmm.dim_partid_bulk_2 = mab2.dim_partid
    inner join dim_date ardb on fmm.dim_dateidactualrelease_bulk_1 = ardb.dim_dateid
    inner join dim_date ardb2 on fmm.dim_dateidactualrelease_bulk_2 = ardb2.dim_dateid
    inner join dim_date ardb3 on fmm.dim_dateidactualrelease_bulk_3 = ardb3.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
    inner join dim_date afdb2 on fmm.dim_dateidactualheaderfinish_bulk_2 = afdb2.dim_dateid
    inner join dim_date udmdb2 on fmm.dim_dateidusagedecisionmade_bulk_2 = udmdb2.dim_dateid

group by dim_partid_bulk_2, dim_batchid_bulk_2, dd_inspectionlotno_bulk_2,  gpf_fpp_final;

update fact_scm fmm
set   
  fmm.ct_actualLTBulk2 = upd.ct_actualLTBulk2,
  fmm.ct_actual_Bulk2_prd = upd.ct_actual_Bulk2_prd,
  fmm.ct_actual_Bulk2_qc = upd.ct_actual_Bulk2_qc,
  fmm.ct_actual_Bulk2_rl = upd.ct_actual_Bulk2_rl,
  fmm.ct_targetLTBULK2 = upd.ct_targetLTBULK2,
  fmm.ct_target_Bulk2_prd = upd.ct_target_Bulk2_prd,
  fmm.ct_target_Bulk2_qc = upd.ct_target_Bulk2_qc,
  fmm.ct_target_Bulk2_rl = upd.ct_target_Bulk2_rl
from fact_scm fmm inner join tmp_scm_for_actualBulk_2  upd
on  fmm.dim_partid_bulk_2 = upd.dim_partid_bulk_2
  and fmm.dim_batchid_bulk_2 = upd.dim_batchid_bulk_2
  and fmm.dd_inspectionlotno_bulk_2 = upd.dd_inspectionlotno_bulk_2
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;

/****************** BULK 3 ********************************/
drop table if exists tmp_scm_for_actualBulk_3;
create table tmp_scm_for_actualBulk_3 as
select  distinct  dim_partid_bulk_3, dim_batchid_bulk_3, dd_inspectionlotno_bulk_3,   gpf_fpp_final,

/*Actual LT Bulk3 */
MAX( 
  CASE WHEN (ardb3.DateValue) = '0001-01-01'
  THEN 0
  ELSE (

  CASE WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (ardb3.DateValue) 
  END
  ) *5/7 as ct_actualLTBulk3,

/*Actual LT Bulk3  PRD - LVL 3 */
MAX( 
  CASE WHEN (ardb3.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (afdb3.DateValue)
  - (ardb3.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk3_prd,

/*Actual LT Bulk3  QC - LVL 3*/
MAX( 
  CASE WHEN (afdb3.DateValue) = '0001-01-01'
  THEN 0
  ELSE 
    (udmdb3.Datevalue)
  - (afdb3.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk3_qc,
/*Actual LT Bulk3  RL - LVL 3*/
MAX( 
  CASE WHEN (udmdb3.DateValue) = '0001-01-01'
  THEN 0
  ELSE (

  CASE WHEN (ardf.DateValue) <> '0001-01-01'  THEN (ardf.DateValue) 
         ELSE (ardfp.DateValue) 
    END )
  - (udmdb3.DateValue) 
  END
  ) *5/7 as ct_actual_Bulk3_rl,

/*Target LT BULK3*/
MAX( 
CASE WHEN (ardb3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_BULK_3) + (CT_TARGETMASTERRECIPE_BULK_3) + (mab3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_BULK_3) *5/7 + (CT_LOTSIZEDAYS_BULK_3) *5/7/2 

    END
    ) as ct_targetLTBULK3,

/*Target LT BULK3  PRD - LVL 3*/
MAX( 
CASE WHEN (ardb3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_BULK_3) + (CT_TARGETMASTERRECIPE_BULK_3) 

    END
    ) as ct_target_BULK3_prd,

/*Target LT BULK3  QC - LVL 3*/
MAX( 
CASE WHEN (ardb3.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (mab3.GRProcessingTime) 

    END
    ) as ct_target_BULK3_qc,

/*Target LT BULK3  RL - LVL 3*/
MAX( 
CASE WHEN (ardb3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_MINRELEASEDSTOCKDAYS_BULK_3) *5/7 + (CT_LOTSIZEDAYS_BULK_3) *5/7/2 

    END
    ) as ct_target_BULK3_rl

  from fact_scm_for_measure_updates fmm
    inner join dim_part mab3 on fmm.dim_partid_bulk_3 = mab3.dim_partid
    inner join dim_date ardb3 on fmm.dim_dateidactualrelease_bulk_3 = ardb3.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
    inner join dim_date afdb3 on fmm.dim_dateidactualheaderfinish_bulk_3 = afdb3.dim_dateid
    inner join dim_date udmdb3 on fmm.dim_dateidusagedecisionmade_bulk_3 = udmdb3.dim_dateid
group by dim_partid_bulk_3, dim_batchid_bulk_3, dd_inspectionlotno_bulk_3,  gpf_fpp_final;

update fact_scm fmm
set   
  fmm.ct_actualLTBulk3 = upd.ct_actualLTBulk3,
  fmm.ct_actual_Bulk3_prd = upd.ct_actual_Bulk3_prd,
  fmm.ct_actual_Bulk3_qc = upd.ct_actual_Bulk3_qc,
  fmm.ct_actual_Bulk3_rl = upd.ct_actual_Bulk3_rl,
  fmm.ct_targetLTBULK3 = upd.ct_targetLTBULK3,
  fmm.ct_target_BULK3_prd = upd.ct_target_BULK3_prd,
  fmm.ct_target_BULK3_qc = upd.ct_target_BULK3_qc,
  fmm.ct_target_BULK3_rl = upd.ct_target_BULK3_rl
from fact_scm fmm inner join tmp_scm_for_actualBulk_3  upd
on  fmm.dim_partid_bulk_3 = upd.dim_partid_bulk_3
  and fmm.dim_batchid_bulk_3 = upd.dim_batchid_bulk_3
  and fmm.dd_inspectionlotno_bulk_3 = upd.dd_inspectionlotno_bulk_3
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2)*/;


/*********************************************** FPU related measures ********************************/

/****************** FPU & FPU 1 ********************************/
drop table if exists tmp_scm_for_actualfpu;
create table tmp_scm_for_actualfpu as
select  distinct  dim_partid_fpu_1, dim_batchid_fpu_1, DD_INSPECTIONLOTNO_FPU_1, gpf_fpp_final,
/*Actual LT FPU */
MAX( 
  CASE WHEN (ardf.DateValue) = '0001-01-01'
  THEN 0
  ELSE (afdfp.DateValue) - (ardf.DateValue) 
  END
  ) *5/7 as ct_actualLTFpu,
  
/*Actual LT FPU1 */
MAX( 
  CASE WHEN (ardf.DateValue) = '0001-01-01'
  THEN 0
    ELSE (
          CASE WHEN  (darfpu2.DateValue) <> '0001-01-01'  THEN (darfpu2.DateValue) 
              ELSE (ardfp.DateValue) 
    END )
  - (ardf.DateValue) 
  END
  ) *5/7 as ct_actualLTFpu1,
  
/*Actual LT FPU1 PRD - LVL 3*/
MAX( 
  CASE WHEN (ardf.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
   (afdf.DateValue)
  - (ardf.DateValue) 
  END
  ) *5/7 as ct_actual_fpu1_prd,
  
/*Actual LT FPU1 QC - LVL 3*/
MAX( 
  CASE WHEN (afdf.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
  (udmdf.DateValue)
  - (afdf.DateValue) 
  END
  ) *5/7 as ct_actual_fpu1_qc,
  
/*Actual LT FPU1 RL - LVL 3*/
MAX( 
  CASE WHEN (udmdf.DateValue) = '0001-01-01'
  THEN 0
    ELSE (
          CASE WHEN  (darfpu2.DateValue) <> '0001-01-01'  THEN (darfpu2.DateValue) 
              ELSE (ardfp.DateValue) 
    END )
  - (udmdf.DateValue) 
  END
  ) *5/7 as ct_actual_fpu1_rl,
  
/*Target LT FPU*/
MAX( 
CASE WHEN (ardf.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_1) + (CT_TARGETMASTERRECIPE_FPU_1) + (mf.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7 + (CT_LOTSIZEDAYS_FPU_1) *5/7 /2 +
   (CT_FLOATBEFOREPRODUCTION_FPU_2) + (CT_TARGETMASTERRECIPE_FPU_2) + (pprtfpu2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_2) *5/7 + (CT_LOTSIZEDAYS_FPU_2) *5/7/2 +
   (CT_FLOATBEFOREPRODUCTION_FPU_3) + (CT_TARGETMASTERRECIPE_FPU_3) + (pprtfpu3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_3) *5/7 + (CT_LOTSIZEDAYS_FPU_3) *5/7/2  +
   (CT_FLOATBEFOREPRODUCTION_FPU_4) + (CT_TARGETMASTERRECIPE_FPU_4) + (pprtfpu4.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_4) *5/7 + (CT_LOTSIZEDAYS_FPU_4) *5/7/2  +
   (CT_FLOATBEFOREPRODUCTION_FPU_5) + (CT_TARGETMASTERRECIPE_FPU_5) + (pprtfpu5.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_5) *5/7 + (CT_LOTSIZEDAYS_FPU_5) *5/7/2    
  +( CASE WHEN  (afdfp.DateValue) <> '0001-01-01' /*check fpp*/
    THEN (CT_FLOATBEFOREPRODUCTION_FPP_1) + (CT_TARGETMASTERRECIPE_FPP_1) 
    ELSE 0 END)
    END
    ) as ct_targetLTFPU,
  
/*Target LT FPU1*/
MAX( 
CASE WHEN (ardf.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_1) + (CT_TARGETMASTERRECIPE_FPU_1) + (mf.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7 + (CT_LOTSIZEDAYS_FPU_1) *5/7 /2 
  end
    ) as ct_targetLTFPU1,
  
/*Target LT FPU1 PRD - LVL 3*/
MAX( 
CASE WHEN (ardf.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_1) + (CT_TARGETMASTERRECIPE_FPU_1) 
  end
    ) as ct_target_fpu1_prd,
  
/*Target LT FPU1 QC - LVL 3*/
MAX( 
CASE WHEN (ardf.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (mf.GRProcessingTime)  
  end
    ) as ct_target_fpu1_qc,

/*Target LT FPU1 RL - LVL 3*/
MAX( 
CASE WHEN (ardf.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (CT_MINRELEASEDSTOCKDAYS_FPU_1) *5/7 + (CT_LOTSIZEDAYS_FPU_1) *5/7 /2 
  end
    ) as ct_target_fpu1_rl

  from fact_scm_for_measure_updates fmm 
    inner join dim_part mf on fmm.dim_partid_fpu_1 = mf.dim_partid
    inner join dim_part pprtfpu2 on fmm.dim_partid_fpu_2 = pprtfpu2.dim_partid
    inner join dim_part pprtfpu3 on fmm.dim_partid_fpu_3 = pprtfpu3.dim_partid
    inner join dim_part pprtfpu4 on fmm.dim_partid_fpu_4 = pprtfpu4.dim_partid
    inner join dim_part pprtfpu5 on fmm.dim_partid_fpu_5 = pprtfpu5.dim_partid
    inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid
    inner join dim_date ardf on fmm.dim_dateidactualrelease_fpu_1 = ardf.dim_dateid
    inner join dim_date darfpu2 on fmm.dim_dateidactualrelease_fpu_2 = darfpu2.dim_dateid
  inner join dim_date afdf on fmm.dim_dateidactualheaderfinish_fpu_1 = afdf.dim_dateid
  inner join dim_date udmdf on fmm.dim_dateidusagedecisionmade_fpu_1 = udmdf.dim_dateid
        inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid

group by dim_partid_fpu_1, dim_batchid_fpu_1, DD_INSPECTIONLOTNO_FPU_1,  gpf_fpp_final;


update fact_scm fmm
set   
fmm.ct_actualLTFpu = upd.ct_actualLTFpu,
fmm.ct_actualLTFpu1 = upd.ct_actualLTFpu1,
fmm.ct_actual_fpu1_prd = upd.ct_actual_fpu1_prd,
fmm.ct_actual_fpu1_qc = upd.ct_actual_fpu1_qc,
fmm.ct_actual_fpu1_rl = upd.ct_actual_fpu1_rl,
fmm.ct_targetLTFPU = upd.ct_targetLTFPU,
fmm.ct_targetLTFPU1 = upd.ct_targetLTFPU1,
fmm.ct_target_fpu1_prd = upd.ct_target_fpu1_prd,
fmm.ct_target_fpu1_qc = upd.ct_target_fpu1_qc,
fmm.ct_target_fpu1_rl = upd.ct_target_fpu1_rl
from fact_scm fmm inner join tmp_scm_for_actualfpu  upd
on  fmm.dim_partid_fpu_1 = upd.dim_partid_fpu_1
  and fmm.dim_batchid_fpu_1 = upd.dim_batchid_fpu_1
  and fmm.DD_INSPECTIONLOTNO_FPU_1 = upd.DD_INSPECTIONLOTNO_FPU_1
    and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;


/****************** FPU 2 ********************************/
drop table if exists tmp_scm_for_actualfpu2;
create table tmp_scm_for_actualfpu2 as
select  distinct  dim_partid_fpu_2, dim_batchid_fpu_2, DD_INSPECTIONLOTNO_FPU_2, gpf_fpp_final,

/*Actual LT FPU2 */
MAX( 
  CASE WHEN (darfpu2.DateValue) = '0001-01-01'
  THEN 0
    ELSE (
          CASE WHEN  (darfpu3.DateValue) <> '0001-01-01'  THEN (darfpu3.DateValue) 
              ELSE (ardfp.DateValue) 
    END )
  - (darfpu2.DateValue) 
  END
  ) *5/7 as ct_actualLTFpu2,
  
/*Actual LT FPU2 PRD - LVL 3*/
MAX( 
  CASE WHEN (darfpu2.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
   (dahffpu2.DateValue)
  - (darfpu2.DateValue) 
  END
  ) *5/7 as ct_actual_fpu2_prd,
  
/*Actual LT FPU2 QC - LVL 3*/
MAX( 
  CASE WHEN (dahffpu2.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
  (dudmfpu2.DateValue)
  - (dahffpu2.DateValue) 
  END
  ) *5/7 as ct_actual_fpu2_qc,
  
/*Actual LT FPU2 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmfpu2.DateValue) = '0001-01-01'
  THEN 0
    ELSE (
          CASE WHEN  (darfpu3.DateValue) <> '0001-01-01'  THEN (darfpu3.DateValue) 
              ELSE (ardfp.DateValue) 
    END )
  - (dudmfpu2.DateValue) 
  END
  ) *5/7 as ct_actual_fpu2_rl,

/*Target LT FPU2*/
MAX( 
CASE WHEN (darfpu2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_2) + (CT_TARGETMASTERRECIPE_FPU_2) + (pprtfpu2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_2) *5/7 + (CT_LOTSIZEDAYS_FPU_2) *5/7 /2 
  end
    ) as ct_targetLTFPU2,
  
/*Target LT FPU2 PRD - LVL 3*/
MAX( 
CASE WHEN (darfpu2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_2) + (CT_TARGETMASTERRECIPE_FPU_2) 
  end
    ) as ct_target_fpu2_prd,
  
/*Target LT FPU2 QC - LVL 3*/
MAX( 
CASE WHEN (darfpu2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (pprtfpu2.GRProcessingTime)  
  end
    ) as ct_target_fpu2_qc,

/*Target LT FPU2 RL - LVL 3*/
MAX( 
CASE WHEN (darfpu2.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (CT_MINRELEASEDSTOCKDAYS_FPU_2) *5/7 + (CT_LOTSIZEDAYS_FPU_2) *5/7 /2 
  end
    ) as ct_target_fpu2_rl
  
  from fact_scm_for_measure_updates fmm 
    inner join dim_part pprtfpu2 on fmm.dim_partid_fpu_2 = pprtfpu2.dim_partid
    inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid
    inner join dim_date darfpu2 on fmm.dim_dateidactualrelease_fpu_2 = darfpu2.dim_dateid
        inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
    inner join dim_date darfpu3 on fmm.dim_dateidactualrelease_fpu_3 = darfpu3.dim_dateid
  inner join dim_date dahffpu2 on fmm.dim_dateidactualheaderfinish_fpu_2 = dahffpu2.dim_dateid
  inner join dim_date dudmfpu2 on fmm.dim_dateidusagedecisionmade_fpu_2 = dudmfpu2.dim_dateid
group by dim_partid_fpu_2, dim_batchid_fpu_2, DD_INSPECTIONLOTNO_FPU_2,  gpf_fpp_final;


update fact_scm fmm
set   
fmm.ct_actualLTFpu2 = upd.ct_actualLTFpu2,
fmm.ct_targetLTFPU2 = upd.ct_targetLTFPU2,
fmm.ct_actual_fpu2_prd = upd.ct_actual_fpu2_prd,
fmm.ct_actual_fpu2_qc = upd.ct_actual_fpu2_qc,
fmm.ct_actual_fpu2_rl = upd.ct_actual_fpu2_rl,
fmm.ct_target_fpu2_prd = upd.ct_target_fpu2_prd,
fmm.ct_target_fpu2_qc = upd.ct_target_fpu2_qc,
fmm.ct_target_fpu2_rl = upd.ct_target_fpu2_rl
from fact_scm fmm inner join tmp_scm_for_actualfpu2  upd
on  fmm.dim_partid_fpu_2 = upd.dim_partid_fpu_2
  and fmm.dim_batchid_fpu_2 = upd.dim_batchid_fpu_2
  and fmm.DD_INSPECTIONLOTNO_FPU_2 = upd.DD_INSPECTIONLOTNO_FPU_2
    and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
 /* and agidcc.datevalue >= add_years(current_date, -2) */;

/****************** FPU 3 ********************************/
drop table if exists tmp_scm_for_actualfpu3;
create table tmp_scm_for_actualfpu3 as
select  distinct  dim_partid_fpu_3, dim_batchid_fpu_3, DD_INSPECTIONLOTNO_FPU_3, gpf_fpp_final,

/*Actual LT FPU3 */
MAX( 
  CASE WHEN (darfpu3.DateValue) = '0001-01-01'
  THEN 0
    ELSE (
          CASE WHEN  (darfpu4.DateValue) <> '0001-01-01'  THEN (darfpu4.DateValue) 
              ELSE (ardfp.DateValue) 
    END )
  - (darfpu3.DateValue) 
  END
  ) *5/7 as ct_actualLTFpu3,
  
/*Actual LT FPU3 PRD - LVL 3*/
MAX( 
  CASE WHEN (darfpu3.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
   (dahffpu3.DateValue)
  - (darfpu3.DateValue) 
  END
  ) *5/7 as ct_actual_fpu3_prd,
  
/*Actual LT FPU3 QC - LVL 3*/
MAX( 
  CASE WHEN (dahffpu3.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
  (dudmfpu3.DateValue)
  - (dahffpu3.DateValue) 
  END
  ) *5/7 as ct_actual_fpu3_qc,
  
/*Actual LT FPU3 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmfpu3.DateValue) = '0001-01-01'
  THEN 0
    ELSE (
          CASE WHEN  (darfpu4.DateValue) <> '0001-01-01'  THEN (darfpu4.DateValue) 
              ELSE (ardfp.DateValue) 
    END )
  - (dudmfpu3.DateValue) 
  END
  ) *5/7 as ct_actual_fpu3_rl,

/*Target LT FPU3*/
MAX( 
CASE WHEN (darfpu3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_3) + (CT_TARGETMASTERRECIPE_FPU_3) + (pprtfpu3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_3) *5/7 + (CT_LOTSIZEDAYS_FPU_3) *5/7 /2 
  end
    ) as ct_targetLTFPU3,
  
/*Target LT FPU3 PRD - LVL 3*/
MAX( 
CASE WHEN (darfpu3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_3) + (CT_TARGETMASTERRECIPE_FPU_3) 
  end
    ) as ct_target_fpu3_prd,
  
/*Target LT FPU3 QC - LVL 3*/
MAX( 
CASE WHEN (darfpu3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (pprtfpu3.GRProcessingTime)  
  end
    ) as ct_target_fpu3_qc,

/*Target LT FPU3 RL - LVL 3*/
MAX( 
CASE WHEN (darfpu3.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (CT_MINRELEASEDSTOCKDAYS_FPU_3) *5/7 + (CT_LOTSIZEDAYS_FPU_3) *5/7 /2 
  end
    ) as ct_target_fpu3_rl
  
  from fact_scm_for_measure_updates fmm 
    inner join dim_part pprtfpu3 on fmm.dim_partid_fpu_3 = pprtfpu3.dim_partid
    inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid
        inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
    inner join dim_date darfpu3 on fmm.dim_dateidactualrelease_fpu_3 = darfpu3.dim_dateid
    inner join dim_date darfpu4 on fmm.dim_dateidactualrelease_fpu_4 = darfpu4.dim_dateid
  inner join dim_date dahffpu3 on fmm.dim_dateidactualheaderfinish_fpu_3 = dahffpu3.dim_dateid
  inner join dim_date dudmfpu3 on fmm.dim_dateidusagedecisionmade_fpu_3 = dudmfpu3.dim_dateid
group by dim_partid_fpu_3, dim_batchid_fpu_3, DD_INSPECTIONLOTNO_FPU_3,  gpf_fpp_final;


update fact_scm fmm
set   
fmm.ct_actualLTFpu3 = upd.ct_actualLTFpu3,
fmm.ct_targetLTFPU3 = upd.ct_targetLTFPU3,
fmm.ct_actual_fpu3_prd = upd.ct_actual_fpu3_prd,
fmm.ct_actual_fpu3_qc = upd.ct_actual_fpu3_qc,
fmm.ct_actual_fpu3_rl = upd.ct_actual_fpu3_rl,
fmm.ct_target_fpu3_prd = upd.ct_target_fpu3_prd,
fmm.ct_target_fpu3_qc = upd.ct_target_fpu3_qc,
fmm.ct_target_fpu3_rl = upd.ct_target_fpu3_rl
from fact_scm fmm inner join tmp_scm_for_actualfpu3  upd
on  fmm.dim_partid_fpu_3 = upd.dim_partid_fpu_3
  and fmm.dim_batchid_fpu_3 = upd.dim_batchid_fpu_3
  and fmm.DD_INSPECTIONLOTNO_FPU_3 = upd.DD_INSPECTIONLOTNO_FPU_3
    and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;


/****************** FPU 4 ********************************/
drop table if exists tmp_scm_for_actualfpu4;
create table tmp_scm_for_actualfpu4 as
select  distinct  dim_partid_fpu_4, dim_batchid_fpu_4, DD_INSPECTIONLOTNO_FPU_4, gpf_fpp_final,

/*Actual LT FPU4 */
MAX( 
  CASE WHEN (darfpu4.DateValue) = '0001-01-01'
  THEN 0
    ELSE (
          CASE WHEN  (darfpu5.DateValue) <> '0001-01-01'  THEN (darfpu5.DateValue) 
              ELSE (ardfp.DateValue) 
    END )
  - (darfpu4.DateValue) 
  END
  ) *5/7 as ct_actualLTFpu4,
  
/*Actual LT FPU4 PRD - LVL 3*/
MAX( 
  CASE WHEN (darfpu4.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
   (dahffpu4.DateValue)
  - (darfpu4.DateValue) 
  END
  ) *5/7 as ct_actual_fpu4_prd,
  
/*Actual LT FPU4 QC - LVL 3*/
MAX( 
  CASE WHEN (dahffpu4.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
  (dudmfpu4.DateValue)
  - (dahffpu4.DateValue) 
  END
  ) *5/7 as ct_actual_fpu4_qc,
  
/*Actual LT FPU4 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmfpu4.DateValue) = '0001-01-01'
  THEN 0
    ELSE (
          CASE WHEN  (darfpu5.DateValue) <> '0001-01-01'  THEN (darfpu5.DateValue) 
              ELSE (ardfp.DateValue) 
    END )
  - (dudmfpu4.DateValue) 
  END
  ) *5/7 as ct_actual_fpu4_rl,


/*Target LT FPU4*/
MAX( 
CASE WHEN (darfpu4.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_4) + (CT_TARGETMASTERRECIPE_FPU_4) + (pprtfpu4.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_4) *5/7 + (CT_LOTSIZEDAYS_FPU_4) *5/7 /2 
  end
    ) as ct_targetLTFPU4,
  
/*Target LT FPU4 PRD - LVL 3*/
MAX( 
CASE WHEN (darfpu4.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_4) + (CT_TARGETMASTERRECIPE_FPU_4) 
  end
    ) as ct_target_fpu4_prd,
  
/*Target LT FPU4 QC - LVL 3*/
MAX( 
CASE WHEN (darfpu4.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (pprtfpu4.GRProcessingTime)  
  end
    ) as ct_target_fpu4_qc,

/*Target LT FPU4 RL - LVL 3*/
MAX( 
CASE WHEN (darfpu4.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (CT_MINRELEASEDSTOCKDAYS_FPU_4) *5/7 + (CT_LOTSIZEDAYS_FPU_4) *5/7 /2 
  end
    ) as ct_target_fpu4_rl
  
  from fact_scm_for_measure_updates fmm 
    inner join dim_part pprtfpu4 on fmm.dim_partid_fpu_4 = pprtfpu4.dim_partid
    inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid
        inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
    inner join dim_date darfpu4 on fmm.dim_dateidactualrelease_fpu_4 = darfpu4.dim_dateid
    inner join dim_date darfpu5 on fmm.dim_dateidactualrelease_fpu_5 = darfpu5.dim_dateid
  inner join dim_date dahffpu4 on fmm.dim_dateidactualheaderfinish_fpu_4 = dahffpu4.dim_dateid
  inner join dim_date dudmfpu4 on fmm.dim_dateidusagedecisionmade_fpu_4 = dudmfpu4.dim_dateid
group by dim_partid_fpu_4, dim_batchid_fpu_4, DD_INSPECTIONLOTNO_FPU_4,  gpf_fpp_final;


update fact_scm fmm
set   
fmm.ct_actualLTFpu4 = upd.ct_actualLTFpu4,
fmm.ct_targetLTFPU4 = upd.ct_targetLTFPU4,
fmm.ct_actual_fpu4_prd = upd.ct_actual_fpu4_prd,
fmm.ct_actual_fpu4_qc = upd.ct_actual_fpu4_qc,
fmm.ct_actual_fpu4_rl = upd.ct_actual_fpu4_rl,
fmm.ct_target_fpu4_prd = upd.ct_target_fpu4_prd,
fmm.ct_target_fpu4_qc = upd.ct_target_fpu4_qc,
fmm.ct_target_fpu4_rl = upd.ct_target_fpu4_rl
from fact_scm fmm inner join tmp_scm_for_actualfpu4  upd
on  fmm.dim_partid_fpu_4 = upd.dim_partid_fpu_4
  and fmm.dim_batchid_fpu_4 = upd.dim_batchid_fpu_4
  and fmm.DD_INSPECTIONLOTNO_FPU_4 = upd.DD_INSPECTIONLOTNO_FPU_4
    and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
/*  and agidcc.datevalue >= add_years(current_date, -2) */;

/****************** FPU 5 ********************************/
drop table if exists tmp_scm_for_actualfpu5;
create table tmp_scm_for_actualfpu5 as
select  distinct  dim_partid_fpu_5, dim_batchid_fpu_5, DD_INSPECTIONLOTNO_FPU_5, gpf_fpp_final,

/*Actual LT FPU5 */
MAX( 
  CASE WHEN (darfpu5.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
          (ardfp.DateValue) - (darfpu5.DateValue) 
  END
  ) *5/7 as ct_actualLTFpu5,
  
/*Actual LT FPU5 PRD - LVL 3*/
MAX( 
  CASE WHEN (darfpu5.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
   (dahffpu5.DateValue)
  - (darfpu5.DateValue) 
  END
  ) *5/7 as ct_actual_fpu5_prd,
  
/*Actual LT FPU5 QC - LVL 3*/
MAX( 
  CASE WHEN (dahffpu5.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
  (dudmfpu5.DateValue)
  - (dahffpu5.DateValue) 
  END
  ) *5/7 as ct_actual_fpu5_qc,
  
/*Actual LT FPU5 RL - LVL 3*/
MAX( 
  CASE WHEN (dudmfpu5.DateValue) = '0001-01-01'
  THEN 0
    ELSE 
                (ardfp.DateValue) 
     - (dudmfpu5.DateValue) 
  END
  ) *5/7 as ct_actual_fpu5_rl,


/*Target LT FPU5*/
MAX( 
CASE WHEN (darfpu5.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_5) + (CT_TARGETMASTERRECIPE_FPU_5) + (pprtfpu5.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPU_5) *5/7 + (CT_LOTSIZEDAYS_FPU_5) *5/7 /2 
  end
    ) as ct_targetLTFPU5,
  
/*Target LT FPU5 PRD - LVL 3*/
MAX( 
CASE WHEN (darfpu5.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPU_5) + (CT_TARGETMASTERRECIPE_FPU_5) 
  end
    ) as ct_target_fpu5_prd,
  
/*Target LT FPU5 QC - LVL 3*/
MAX( 
CASE WHEN (darfpu5.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (pprtfpu5.GRProcessingTime)  
  end
    ) as ct_target_fpu5_qc,

/*Target LT FPU3 RL - LVL 3*/
MAX( 
CASE WHEN (darfpu5.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (CT_MINRELEASEDSTOCKDAYS_FPU_5) *5/7 + (CT_LOTSIZEDAYS_FPU_5) *5/7 /2 
  end
    ) as ct_target_fpu5_rl

  from fact_scm_for_measure_updates fmm 
    inner join dim_part pprtfpu5 on fmm.dim_partid_fpu_5 = pprtfpu5.dim_partid
    inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid
        inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateid
    inner join dim_date darfpu5 on fmm.dim_dateidactualrelease_fpu_5 = darfpu5.dim_dateid
  inner join dim_date dahffpu5 on fmm.dim_dateidactualheaderfinish_fpu_5 = dahffpu5.dim_dateid
  inner join dim_date dudmfpu5 on fmm.dim_dateidusagedecisionmade_fpu_5 = dudmfpu5.dim_dateid
group by dim_partid_fpu_5, dim_batchid_fpu_5, DD_INSPECTIONLOTNO_FPU_5,  gpf_fpp_final;


update fact_scm fmm
set   
fmm.ct_actualLTFpu5 = upd.ct_actualLTFpu5,
fmm.ct_targetLTFPU5 = upd.ct_targetLTFPU5,
fmm.ct_actual_fpu5_prd = upd.ct_actual_fpu5_prd,
fmm.ct_actual_fpu5_qc = upd.ct_actual_fpu5_qc,
fmm.ct_actual_fpu5_rl = upd.ct_actual_fpu5_rl,
fmm.ct_target_fpu5_prd = upd.ct_target_fpu5_prd,
fmm.ct_target_fpu5_qc = upd.ct_target_fpu5_qc,
fmm.ct_target_fpu5_rl = upd.ct_target_fpu5_rl
from fact_scm fmm inner join tmp_scm_for_actualfpu5  upd
on  fmm.dim_partid_fpu_5 = upd.dim_partid_fpu_5
  and fmm.dim_batchid_fpu_5 = upd.dim_batchid_fpu_5
  and fmm.DD_INSPECTIONLOTNO_FPU_5 = upd.DD_INSPECTIONLOTNO_FPU_5
    and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;

/*********************************************** FPP related measures ********************************/

/******************* FPP  & FPP1 ********************************/
drop table if exists tmp_scm_for_actualfpp_1;
create table tmp_scm_for_actualfpp_1 as
select  distinct  dim_partid_fpp_1, dim_batchid_fpp_1, DD_INSPECTIONLOTNO_FPP_1,    gpf_fpp_final,
/*Actual LT FPP */
MAX( 
  CASE WHEN (ardfp.DateValue) = '0001-01-01' 
  THEN 0
  WHEN (agidtc.DateValue) = '0001-01-01'
  THEN (agidcc.DateValue) - (ardfp.DateValue) 
  ELSE (agidtc.DateValue) - (ardfp.DateValue) 
  END
  ) *5/7 as ct_actualLTFpp,
/*Actual LT FPP1 */
MAX( 
  CASE WHEN (ardfp.DateValue) = '0001-01-01' THEN 0
  ELSE CASE  WHEN (darfpp2.DateValue) <> '0001-01-01' THEN (darfpp2.DateValue) - (ardfp.DateValue)   /*Release FPP2 - Release FPP1*/
             WHEN (agidtc.DateValue) <> '0001-01-01' THEN (agidtc.DateValue) - (ardfp.DateValue)   /*AGI ComOps - Release FPP1*/
        ELSE (agidcc.DateValue) - (ardfp.DateValue)  /*AGI Cust - Release FPP1*/
        END
  END
  ) *5/7 as ct_actualLTFpp1,

/*Actual LT FPP1  PRD - LVL 3 */
MAX( 
  CASE WHEN (ardfp.DateValue) = '0001-01-01' THEN 0
        ELSE (afdfp.DateValue) - (ardfp.DateValue)  
  END
  ) *5/7 as ct_actual_Fpp1_prd,


/*Actual LT FPP1  QC - LVL 3*/
MAX( 
  CASE WHEN (afdfp.DateValue) = '0001-01-01' THEN 0
  ELSE   (udmdfp.DateValue) - (afdfp.DateValue)    
  END
  ) *5/7 as ct_actual_Fpp1_qc,


/*Target LT FPP*/
MAX( 
CASE WHEN (ardfp.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPP_1) + (CT_TARGETMASTERRECIPE_FPP_1) + (mfp.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7 + (CT_LOTSIZEDAYS_FPP_1) *5/7 /2 +
   (CT_FLOATBEFOREPRODUCTION_FPP_2) + (CT_TARGETMASTERRECIPE_FPP_2) + (pprtfpp2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_2) *5/7 + (CT_LOTSIZEDAYS_FPP_2) *5/7/2 +
   (CT_FLOATBEFOREPRODUCTION_FPP_3) + (CT_TARGETMASTERRECIPE_FPP_3) + (pprtfpp3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_3) *5/7 + (CT_LOTSIZEDAYS_FPP_3) *5/7/2  
  END
  )  as ct_targetLTFPP,
/*Target LT FPP1*/
MAX( 
CASE WHEN (ardfp.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPP_1) + (CT_TARGETMASTERRECIPE_FPP_1) + (mfp.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_1) *5/7 + (CT_LOTSIZEDAYS_FPP_1) *5/7 /2 
  END
  )  as ct_targetLTFPP1,

/*Target LT FPP1 PRD - LVL 3*/
MAX( 
CASE WHEN (ardfp.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPP_1) + (CT_TARGETMASTERRECIPE_FPP_1) 
  END
  )  as ct_target_FPP1_prd,

/*Target LT FPP1 QC - LVL 3*/
MAX( 
CASE WHEN (ardfp.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (mfp.GRProcessingTime) 
  END
  )  as ct_target_FPP1_qc

  from fact_scm_for_measure_updates fmm
    inner join dim_part mfp on fmm.dim_partid_fpp_1 = mfp.dim_partid
    inner join dim_part pprtfpp2 on fmm.dim_partid_fpp_2 = pprtfpp2.dim_partid
    inner join dim_part pprtfpp3 on fmm.dim_partid_fpp_3 = pprtfpp3.dim_partid
    inner join dim_date ardfp on fmm.dim_dateidactualrelease_fpp_1 = ardfp.dim_dateID
    inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
    inner join dim_date agidtc on fmm.dim_dateidactualgoodsissue_comops = agidtc.dim_dateid
    inner join dim_date darfpp2 on fmm.dim_dateidactualrelease_fpp_2 = darfpp2.dim_dateID 
    inner join dim_date afdfp on fmm.dim_dateidactualheaderfinish_fpp_1 = afdfp.dim_dateid
    inner join dim_date udmdfp on fmm.dim_dateidusagedecisionmade_fpp_1 = udmdfp.dim_dateid
group by dim_partid_fpp_1, dim_batchid_fpp_1, DD_INSPECTIONLOTNO_FPP_1,  gpf_fpp_final;

update fact_scm fmm
set 	
fmm.ct_actualLTFpp = upd.ct_actualLTFpp,
fmm.ct_actualLTFpp1 = upd.ct_actualLTFpp1,
fmm.ct_actual_Fpp1_prd = upd.ct_actual_Fpp1_prd,
fmm.ct_actual_Fpp1_qc = upd.ct_actual_Fpp1_qc,
fmm.ct_targetLTfpp = upd.ct_targetLTfpp,
fmm.ct_targetLTfpp1 = upd.ct_targetLTfpp1,
fmm.ct_target_Fpp1_prd = upd.ct_target_Fpp1_prd,
fmm.ct_target_Fpp1_qc = upd.ct_target_Fpp1_qc
from fact_scm fmm inner join tmp_scm_for_actualfpp_1  upd
on  fmm.dim_partid_fpp_1 = upd.dim_partid_fpp_1
	and fmm.dim_batchid_fpp_1 = upd.dim_batchid_fpp_1
	and fmm.DD_INSPECTIONLOTNO_FPP_1 = upd.DD_INSPECTIONLOTNO_FPP_1
	and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where 	 ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
		( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
	/* and agidcc.datevalue >= add_years(current_date, -2) */; 


/******************* FPP2 ********************************/
drop table if exists tmp_scm_for_actualfpp_2;
create table tmp_scm_for_actualfpp_2 as
select  distinct  dim_partid_fpp_2, dim_batchid_fpp_2, DD_INSPECTIONLOTNO_FPP_2,    gpf_fpp_final,
/*Actual LT FPP2 */
MAX( 
  CASE WHEN (darfpp2.DateValue) = '0001-01-01' THEN 0
  ELSE CASE  WHEN (darfpp3.DateValue) <> '0001-01-01' THEN (darfpp3.DateValue) - (darfpp2.DateValue)   /*Release FPP3 - Release FPP2*/
             WHEN (agidtc.DateValue) <> '0001-01-01' THEN (agidtc.DateValue) - (darfpp2.DateValue)   /*AGI ComOps - Release FPP1*/
        ELSE (agidcc.DateValue) - (darfpp2.DateValue)  /*AGI Cust - Release FPP1*/
        END
  END
  ) *5/7 as ct_actualLTFpp2,

/*Actual LT FPP2 PRD - LVL 3 */
MAX( 
  CASE WHEN (darfpp2.DateValue) = '0001-01-01' THEN 0
        ELSE (dahffpp2.DateValue) - (darfpp2.DateValue)  
  END
  ) *5/7 as ct_actual_Fpp2_prd,


/*Actual LT FPP2 QC - LVL 3*/
MAX( 
  CASE WHEN (dahffpp2.DateValue) = '0001-01-01' THEN 0
        ELSE (dudmfpp2.DateValue) - (dahffpp2.DateValue)  
  END
  ) *5/7 as ct_actual_Fpp2_qc,


/*Target LT FPP2 */
MAX( 
CASE WHEN (darfpp2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPP_2) + (CT_TARGETMASTERRECIPE_FPP_2) + (pprtfpp2.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_2) *5/7 + (CT_LOTSIZEDAYS_FPP_2) *5/7/2
  END
  )  as ct_targetLTFPP2,

/*Target LT FPP2 PRD - LVL 3*/
MAX( 
CASE WHEN (darfpp2.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPP_2) + (CT_TARGETMASTERRECIPE_FPP_2) 
  END
  )  as ct_target_FPP2_prd,


/*Target LT FPP2 QC - LVL 3*/
MAX( 
CASE WHEN (darfpp2.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (pprtfpp2.GRProcessingTime) 
  END
  )  as ct_target_FPP2_qc


  from fact_scm_for_measure_updates fmm
    inner join dim_part pprtfpp2 on fmm.dim_partid_fpp_2 = pprtfpp2.dim_partid
    inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
    inner join dim_date agidtc on fmm.dim_dateidactualgoodsissue_comops = agidtc.dim_dateid
    inner join dim_date darfpp2 on fmm.dim_dateidactualrelease_fpp_2 = darfpp2.dim_dateID 
    inner join dim_date darfpp3 on fmm.dim_dateidactualrelease_fpp_3 = darfpp3.dim_dateID
    inner join dim_date dahffpp2 on fmm.dim_dateidactualheaderfinish_fpp_2 = dahffpp2.dim_dateID
    inner join dim_date dudmfpp2 on fmm.dim_dateidusagedecisionmade_fpp_2 = dudmfpp2.dim_dateID
group by dim_partid_fpp_2, dim_batchid_fpp_2, DD_INSPECTIONLOTNO_FPP_2,  gpf_fpp_final;

update fact_scm fmm
set   
fmm.ct_actualLTFpp2 = upd.ct_actualLTFpp2,
fmm.ct_actual_Fpp2_prd = upd.ct_actual_Fpp2_prd,
fmm.ct_actual_Fpp2_qc = upd.ct_actual_Fpp2_qc,
fmm.ct_targetLTfpp2 = upd.ct_targetLTfpp2,
fmm.ct_target_Fpp2_prd = upd.ct_target_Fpp2_prd,
fmm.ct_target_Fpp2_qc = upd.ct_target_Fpp2_qc
from fact_scm fmm inner join tmp_scm_for_actualfpp_2  upd
on  fmm.dim_partid_fpp_2 = upd.dim_partid_fpp_2
  and fmm.dim_batchid_fpp_2 = upd.dim_batchid_fpp_2
  and fmm.DD_INSPECTIONLOTNO_FPP_2 = upd.DD_INSPECTIONLOTNO_FPP_2
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
 /* and agidcc.datevalue >= add_years(current_date, -2) */;

  /******************* FPP3 ********************************/
drop table if exists tmp_scm_for_actualfpp_3;
create table tmp_scm_for_actualfpp_3 as
select  distinct  dim_partid_fpp_3, dim_batchid_fpp_3, DD_INSPECTIONLOTNO_FPP_3,    gpf_fpp_final,
/*Actual LT FPP3 */
MAX( 
  CASE WHEN (darfpp3.DateValue) = '0001-01-01' THEN 0
  ELSE CASE  WHEN (agidtc.DateValue) <> '0001-01-01' THEN (agidtc.DateValue) - (darfpp3.DateValue)   /*AGI ComOps - Release FPP1*/
        ELSE (agidcc.DateValue) - (darfpp3.DateValue)  /*AGI Cust - Release FPP1*/
        END
  END
  ) *5/7 as ct_actualLTFpp3,

/*Actual LT FPP3 PRD - LVL 3*/
MAX( 
  CASE WHEN (darfpp3.DateValue) = '0001-01-01' THEN 0
        ELSE (dahffpp3.DateValue) - (darfpp3.DateValue)  
  END
  ) *5/7 as ct_actual_Fpp3_prd,

/*Actual LT FPP3 QC - LVL 3*/
MAX( 
  CASE WHEN (dahffpp3.DateValue) = '0001-01-01' THEN 0
        ELSE (dudmfpp3.DateValue) - (dahffpp3.DateValue)  
  END
  ) *5/7 as ct_actual_Fpp3_qc,


/*Target LT FPP3 */
MAX( 
CASE WHEN (darfpp3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPP_3) + (CT_TARGETMASTERRECIPE_FPP_3) + (pprtfpp3.GRProcessingTime) + (CT_MINRELEASEDSTOCKDAYS_FPP_3) *5/7 + (CT_LOTSIZEDAYS_FPP_3) *5/7/2  
  END
  )  as ct_targetLTFPP3,

/*Target LT FPP3 PRD - LVL 3*/
MAX( 
CASE WHEN (darfpp3.DateValue) = '0001-01-01'
THEN 0
ELSE 
   (CT_FLOATBEFOREPRODUCTION_FPP_3) + (CT_TARGETMASTERRECIPE_FPP_3) 
  END
  )  as ct_target_FPP3_prd,

/*Target LT FPP3 QC - LVL 3*/
MAX( 
CASE WHEN (darfpp3.DateValue) = '0001-01-01'
THEN 0
ELSE 
    (pprtfpp3.GRProcessingTime) 
  END
  )  as ct_target_FPP3_qc

  from fact_scm_for_measure_updates fmm
    inner join dim_part pprtfpp3 on fmm.dim_partid_fpp_3 = pprtfpp3.dim_partid
    inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
    inner join dim_date agidtc on fmm.dim_dateidactualgoodsissue_comops = agidtc.dim_dateid
    inner join dim_date darfpp3 on fmm.dim_dateidactualrelease_fpp_3 = darfpp3.dim_dateID
    inner join dim_date dahffpp3 on fmm.dim_dateidactualheaderfinish_fpp_3 = dahffpp3.dim_dateid
    inner join dim_date dudmfpp3 on fmm.dim_dateidusagedecisionmade_fpp_3 = dudmfpp3.dim_dateid
group by dim_partid_fpp_3, dim_batchid_fpp_3, DD_INSPECTIONLOTNO_FPP_3,  gpf_fpp_final;

update fact_scm fmm
set   
fmm.ct_actualLTFpp3 = upd.ct_actualLTFpp3,
fmm.ct_actual_Fpp3_prd = upd.ct_actual_Fpp3_prd,
fmm.ct_actual_Fpp3_qc = upd.ct_actual_Fpp3_qc,
fmm.ct_targetLTfpp3 = upd.ct_targetLTfpp3,
fmm.ct_target_fpp3_prd = upd.ct_target_fpp3_prd,
fmm.ct_target_fpp3_qc = upd.ct_target_fpp3_qc
from fact_scm fmm inner join tmp_scm_for_actualfpp_3  upd
on  fmm.dim_partid_fpp_3 = upd.dim_partid_fpp_3
  and fmm.dim_batchid_fpp_3 = upd.dim_batchid_fpp_3
  and fmm.DD_INSPECTIONLOTNO_FPP_3 = upd.DD_INSPECTIONLOTNO_FPP_3
  and fmm.gpf_fpp_final = upd.gpf_fpp_final
inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
inner join dim_part p on  fmm.dim_partid_fpp_final = p.dim_partid
inner join dim_part pp  on  fmm.dim_partid_antigen_1 = pp.dim_partid
inner join dim_part ma on fmm.dim_partid_raw_1 = ma.dim_partid
where    ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and fmm.dim_dateidactualrelease_antigen_1 <> 1) ) or 
    ( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
  /* and agidcc.datevalue >= add_years(current_date, -2) */;

/**************  END of  Actual & Target measures *********************/

update fact_scm fmm
set fmm.ct_hitormissE2E_LT =     CASE WHEN  (CASE WHEN CT_TARGETE2E_LT = 0 
THEN 999 
ELSE ( (( CT_ACTUALE2E_LT - CT_TARGETE2E_LT )) / CT_TARGETE2E_LT ) * 100 
END) < 25 AND  (CASE WHEN CT_TARGETE2E_LT = 0 
THEN 999 
ELSE ( (( CT_ACTUALE2E_LT - CT_TARGETE2E_LT )) / CT_TARGETE2E_LT ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

/* Madalina 15Jan2018 - Adding Hit Or Miss Raw, Antigen, Bulk, Fpu and Fpp - APP-8120 */
update fact_scm fmm
set fmm.ct_hitormissE2E_LT_RAW =     CASE WHEN  (CASE WHEN CT_TARGETLTRAW = 0 
THEN 999 
ELSE ( (( CT_ACTUALLTRAW - CT_TARGETLTRAW )) / CT_TARGETLTRAW ) * 100 
END) < 25 AND  (CASE WHEN CT_TARGETLTRAW = 0 
THEN 999 
ELSE ( ((CT_ACTUALLTRAW - CT_TARGETLTRAW )) / CT_TARGETLTRAW ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormissE2E_LT_ANTIGEN =     CASE WHEN  (CASE WHEN CT_TARGETLTANTIGEN = 0 
THEN 999 
ELSE ( (( CT_ACTUALLTANTIGEN - CT_TARGETLTANTIGEN )) / CT_TARGETLTANTIGEN ) * 100 
END) < 25 AND  (CASE WHEN CT_TARGETLTANTIGEN = 0 
THEN 999 
ELSE ( ((CT_ACTUALLTANTIGEN - CT_TARGETLTANTIGEN )) / CT_TARGETLTANTIGEN ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;


update fact_scm fmm
set fmm.ct_hitormissE2E_LT_BULK =     CASE WHEN  (CASE WHEN CT_TARGETLTBULK = 0 
THEN 999 
ELSE ( (( CT_ACTUALLTBULK - CT_TARGETLTBULK )) / CT_TARGETLTBULK ) * 100 
END) < 25 AND  (CASE WHEN CT_TARGETLTBULK = 0 
THEN 999 
ELSE ( ((CT_ACTUALLTBULK - CT_TARGETLTBULK )) / CT_TARGETLTBULK ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;


update fact_scm fmm
set fmm.ct_hitormissE2E_LT_FPU =     CASE WHEN  (CASE WHEN CT_TARGETLTFPU = 0 
THEN 999 
ELSE ( (( CT_ACTUALLTFPU - CT_TARGETLTFPU )) / CT_TARGETLTFPU ) * 100 
END) < 25 AND  (CASE WHEN CT_TARGETLTFPU = 0 
THEN 999 
ELSE ( ((CT_ACTUALLTFPU - CT_TARGETLTFPU )) / CT_TARGETLTFPU ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormissE2E_LT_FPP =     CASE WHEN  (CASE WHEN CT_TARGETLTFPP = 0 
THEN 999 
ELSE ( (( CT_ACTUALLTFPP - CT_TARGETLTFPP )) / CT_TARGETLTFPP ) * 100 
END) < 25 AND  (CASE WHEN CT_TARGETLTFPP = 0 
THEN 999 
ELSE ( ((CT_ACTUALLTFPP - CT_TARGETLTFPP )) / CT_TARGETLTFPP ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;
/* END APP-8120 */
	

/* Updating level 2 Hit Or Miss */
/*RAW*/
update fact_scm fmm
set fmm.ct_hitormisse2e_lt_raw1 =     CASE WHEN  (CASE WHEN ct_targetLTraw1 = 0 
THEN 999 
ELSE ( (( ct_actualLTRaw1 - ct_targetLTRaw1 )) / ct_targetLTRaw1 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTRaw1 = 0 
THEN 999 
ELSE ( ((ct_actualLTRaw1 - ct_targetLTRaw1 )) / ct_targetLTRaw1 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

/*Antigen*/
update fact_scm fmm
set fmm.ct_hitormisse2e_lt_antigen1 =     CASE WHEN  (CASE WHEN ct_targetLTantigen1 = 0 
THEN 999 
ELSE ( (( ct_actualLTantigen1 - ct_targetLTantigen1 )) / ct_targetLTantigen1 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTantigen1 = 0 
THEN 999 
ELSE ( ((ct_actualLTantigen1 - ct_targetLTantigen1 )) / ct_targetLTantigen1 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_antigen2 =     CASE WHEN  (CASE WHEN ct_targetLTantigen2 = 0 
THEN 999 
ELSE ( (( ct_actualLTantigen2 - ct_targetLTantigen2 )) / ct_targetLTantigen2 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTantigen2 = 0 
THEN 999 
ELSE ( ((ct_actualLTantigen2 - ct_targetLTantigen2 )) / ct_targetLTantigen2 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_antigen3 =     CASE WHEN  (CASE WHEN ct_targetLTantigen3 = 0 
THEN 999 
ELSE ( (( ct_actualLTantigen3 - ct_targetLTantigen3 )) / ct_targetLTantigen3 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTantigen3 = 0 
THEN 999 
ELSE ( ((ct_actualLTantigen3 - ct_targetLTantigen3 )) / ct_targetLTantigen3 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_antigen4 =     CASE WHEN  (CASE WHEN ct_targetLTantigen4 = 0 
THEN 999 
ELSE ( (( ct_actualLTantigen4 - ct_targetLTantigen4 )) / ct_targetLTantigen4 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTantigen4 = 0 
THEN 999 
ELSE ( ((ct_actualLTantigen4 - ct_targetLTantigen4 )) / ct_targetLTantigen4 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_antigen5 =     CASE WHEN  (CASE WHEN ct_targetLTantigen5 = 0 
THEN 999 
ELSE ( (( ct_actualLTantigen5 - ct_targetLTantigen5 )) / ct_targetLTantigen5 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTantigen5 = 0 
THEN 999 
ELSE ( ((ct_actualLTantigen5 - ct_targetLTantigen5 )) / ct_targetLTantigen5 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

/*Bulk*/
update fact_scm fmm
set fmm.ct_hitormisse2e_lt_bulk1 =     CASE WHEN  (CASE WHEN ct_targetLTbulk1 = 0 
THEN 999 
ELSE ( (( ct_actualLTbulk1 - ct_targetLTbulk1 )) / ct_targetLTbulk1 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTbulk1 = 0 
THEN 999 
ELSE ( ((ct_actualLTbulk1 - ct_targetLTbulk1 )) / ct_targetLTbulk1 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_bulk2 =     CASE WHEN  (CASE WHEN ct_targetLTbulk2 = 0 
THEN 999 
ELSE ( (( ct_actualLTbulk2 - ct_targetLTbulk2 )) / ct_targetLTbulk2 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTbulk2 = 0 
THEN 999 
ELSE ( ((ct_actualLTbulk2 - ct_targetLTbulk2 )) / ct_targetLTbulk2 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_bulk3 =     CASE WHEN  (CASE WHEN ct_targetLTbulk3 = 0 
THEN 999 
ELSE ( (( ct_actualLTbulk3 - ct_targetLTbulk3 )) / ct_targetLTbulk3 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTbulk3 = 0 
THEN 999 
ELSE ( ((ct_actualLTbulk3 - ct_targetLTbulk3 )) / ct_targetLTbulk3 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

/*Fpu*/

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_fpu1 =     CASE WHEN  (CASE WHEN ct_targetLTfpu1 = 0 
THEN 999 
ELSE ( (( ct_actualLTFpu1 - ct_targetLTfpu1 )) / ct_targetLTfpu1 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTfpu1 = 0 
THEN 999 
ELSE ( ((ct_actualLTFpu1 - ct_targetLTfpu1 )) / ct_targetLTfpu1 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_fpu2 =     CASE WHEN  (CASE WHEN ct_targetLTfpu2 = 0 
THEN 999 
ELSE ( (( ct_actualLTFpu2 - ct_targetLTfpu2 )) / ct_targetLTfpu2 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTfpu2 = 0 
THEN 999 
ELSE ( ((ct_actualLTFpu2 - ct_targetLTfpu2 )) / ct_targetLTfpu2 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_fpu3 =     CASE WHEN  (CASE WHEN ct_targetLTfpu3 = 0 
THEN 999 
ELSE ( (( ct_actualLTFpu3 - ct_targetLTfpu3 )) / ct_targetLTfpu3 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTfpu3 = 0 
THEN 999 
ELSE ( ((ct_actualLTFpu3 - ct_targetLTfpu3 )) / ct_targetLTfpu3 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_fpu4 =     CASE WHEN  (CASE WHEN ct_targetLTfpu4 = 0 
THEN 999 
ELSE ( (( ct_actualLTFpu4 - ct_targetLTfpu4 )) / ct_targetLTfpu4 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTfpu4 = 0 
THEN 999 
ELSE ( ((ct_actualLTFpu4 - ct_targetLTfpu4 )) / ct_targetLTfpu4 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_fpu5 =     CASE WHEN  (CASE WHEN ct_targetLTfpu5 = 0 
THEN 999 
ELSE ( (( ct_actualLTFpu5 - ct_targetLTfpu5 )) / ct_targetLTfpu5 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTfpu5 = 0 
THEN 999 
ELSE ( ((ct_actualLTFpu5 - ct_targetLTfpu5 )) / ct_targetLTfpu5 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

/*Fpp*/

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_fpp1 =     CASE WHEN  (CASE WHEN ct_targetLTfpp1 = 0 
THEN 999 
ELSE ( (( ct_actualLTFpp1 - ct_targetLTfpp1 )) / ct_targetLTfpp1 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTfpp1 = 0 
THEN 999 
ELSE ( ((ct_actualLTFpp1 - ct_targetLTfpp1 )) / ct_targetLTfpp1 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_fpp2 =     CASE WHEN  (CASE WHEN ct_targetLTfpp2 = 0 
THEN 999 
ELSE ( (( ct_actualLTFpp2 - ct_targetLTfpp2 )) / ct_targetLTfpp2 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTfpp2 = 0 
THEN 999 
ELSE ( ((ct_actualLTFpp2 - ct_targetLTfpp2 )) / ct_targetLTfpp2 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

update fact_scm fmm
set fmm.ct_hitormisse2e_lt_fpp3 =     CASE WHEN  (CASE WHEN ct_targetLTfpp3 = 0 
THEN 999 
ELSE ( (( ct_actualLTFpp3 - ct_targetLTfpp3 )) / ct_targetLTfpp3 ) * 100 
END) < 25 AND  (CASE WHEN ct_targetLTfpp3 = 0 
THEN 999 
ELSE ( ((ct_actualLTFpp3 - ct_targetLTfpp3 )) / ct_targetLTfpp3 ) * 100 
END) > -25 
THEN 100 
ELSE 0 
END;

/* END of Hot or Miss - level 2 measures */

/* Adding Hit Or Miss measures - level 3*/

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_RAW1_QC = 
     CASE WHEN  (CASE WHEN ct_target_RAW1_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_RAW1_QC - ct_target_RAW1_QC )) / ct_target_RAW1_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_RAW1_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_RAW1_QC - ct_target_RAW1_QC )) / ct_target_RAW1_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_RAW1_RL = 
     CASE WHEN  (CASE WHEN ct_target_RAW1_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_RAW1_RL - ct_target_RAW1_RL )) / ct_target_RAW1_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_RAW1_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_RAW1_RL - ct_target_RAW1_RL )) / ct_target_RAW1_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN1_PRD = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN1_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN1_PRD - ct_target_ANTIGEN1_PRD )) / ct_target_ANTIGEN1_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN1_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN1_PRD - ct_target_ANTIGEN1_PRD )) / ct_target_ANTIGEN1_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN1_QC = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN1_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN1_QC - ct_target_ANTIGEN1_QC )) / ct_target_ANTIGEN1_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN1_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN1_QC - ct_target_ANTIGEN1_QC )) / ct_target_ANTIGEN1_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN1_RL = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN1_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN1_RL - ct_target_ANTIGEN1_RL )) / ct_target_ANTIGEN1_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN1_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN1_RL - ct_target_ANTIGEN1_RL )) / ct_target_ANTIGEN1_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN2_PRD = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN2_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN2_PRD - ct_target_ANTIGEN2_PRD )) / ct_target_ANTIGEN2_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN2_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN2_PRD - ct_target_ANTIGEN2_PRD )) / ct_target_ANTIGEN2_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN2_QC = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN2_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN2_QC - ct_target_ANTIGEN2_QC )) / ct_target_ANTIGEN2_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN2_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN2_QC - ct_target_ANTIGEN2_QC )) / ct_target_ANTIGEN2_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN2_RL = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN2_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN2_RL - ct_target_ANTIGEN2_RL )) / ct_target_ANTIGEN2_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN2_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN2_RL - ct_target_ANTIGEN2_RL )) / ct_target_ANTIGEN2_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN3_PRD = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN3_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN3_PRD - ct_target_ANTIGEN3_PRD )) / ct_target_ANTIGEN3_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN3_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN3_PRD - ct_target_ANTIGEN3_PRD )) / ct_target_ANTIGEN3_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN3_QC = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN3_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN3_QC - ct_target_ANTIGEN3_QC )) / ct_target_ANTIGEN3_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN3_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN3_QC - ct_target_ANTIGEN3_QC )) / ct_target_ANTIGEN3_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN3_RL = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN3_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN3_RL - ct_target_ANTIGEN3_RL )) / ct_target_ANTIGEN3_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN3_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN3_RL - ct_target_ANTIGEN3_RL )) / ct_target_ANTIGEN3_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN4_PRD = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN4_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN4_PRD - ct_target_ANTIGEN4_PRD )) / ct_target_ANTIGEN4_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN4_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN4_PRD - ct_target_ANTIGEN4_PRD )) / ct_target_ANTIGEN4_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN4_QC = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN4_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN4_QC - ct_target_ANTIGEN4_QC )) / ct_target_ANTIGEN4_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN4_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN4_QC - ct_target_ANTIGEN4_QC )) / ct_target_ANTIGEN4_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN4_RL = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN4_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN4_RL - ct_target_ANTIGEN4_RL )) / ct_target_ANTIGEN4_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN4_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN4_RL - ct_target_ANTIGEN4_RL )) / ct_target_ANTIGEN4_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN5_PRD = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN5_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN5_PRD - ct_target_ANTIGEN5_PRD )) / ct_target_ANTIGEN5_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN5_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN5_PRD - ct_target_ANTIGEN5_PRD )) / ct_target_ANTIGEN5_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN5_QC = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN5_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN5_QC - ct_target_ANTIGEN5_QC )) / ct_target_ANTIGEN5_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN5_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN5_QC - ct_target_ANTIGEN5_QC )) / ct_target_ANTIGEN5_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_ANTIGEN5_RL = 
     CASE WHEN  (CASE WHEN ct_target_ANTIGEN5_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_ANTIGEN5_RL - ct_target_ANTIGEN5_RL )) / ct_target_ANTIGEN5_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_ANTIGEN5_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_ANTIGEN5_RL - ct_target_ANTIGEN5_RL )) / ct_target_ANTIGEN5_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK1_PRD = 
     CASE WHEN  (CASE WHEN ct_target_BULK1_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK1_PRD - ct_target_BULK1_PRD )) / ct_target_BULK1_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK1_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK1_PRD - ct_target_BULK1_PRD )) / ct_target_BULK1_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK1_QC = 
     CASE WHEN  (CASE WHEN ct_target_BULK1_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK1_QC - ct_target_BULK1_QC )) / ct_target_BULK1_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK1_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK1_QC - ct_target_BULK1_QC )) / ct_target_BULK1_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK1_RL = 
     CASE WHEN  (CASE WHEN ct_target_BULK1_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK1_RL - ct_target_BULK1_RL )) / ct_target_BULK1_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK1_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK1_RL - ct_target_BULK1_RL )) / ct_target_BULK1_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK2_PRD = 
     CASE WHEN  (CASE WHEN ct_target_BULK2_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK2_PRD - ct_target_BULK2_PRD )) / ct_target_BULK2_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK2_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK2_PRD - ct_target_BULK2_PRD )) / ct_target_BULK2_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK2_QC = 
     CASE WHEN  (CASE WHEN ct_target_BULK2_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK2_QC - ct_target_BULK2_QC )) / ct_target_BULK2_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK2_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK2_QC - ct_target_BULK2_QC )) / ct_target_BULK2_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK2_RL = 
     CASE WHEN  (CASE WHEN ct_target_BULK2_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK2_RL - ct_target_BULK2_RL )) / ct_target_BULK2_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK2_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK2_RL - ct_target_BULK2_RL )) / ct_target_BULK2_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK3_PRD = 
     CASE WHEN  (CASE WHEN ct_target_BULK3_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK3_PRD - ct_target_BULK3_PRD )) / ct_target_BULK3_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK3_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK3_PRD - ct_target_BULK3_PRD )) / ct_target_BULK3_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK3_QC = 
     CASE WHEN  (CASE WHEN ct_target_BULK3_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK3_QC - ct_target_BULK3_QC )) / ct_target_BULK3_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK3_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK3_QC - ct_target_BULK3_QC )) / ct_target_BULK3_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_BULK3_RL = 
     CASE WHEN  (CASE WHEN ct_target_BULK3_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_BULK3_RL - ct_target_BULK3_RL )) / ct_target_BULK3_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_BULK3_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_BULK3_RL - ct_target_BULK3_RL )) / ct_target_BULK3_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU1_PRD = 
     CASE WHEN  (CASE WHEN ct_target_FPU1_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU1_PRD - ct_target_FPU1_PRD )) / ct_target_FPU1_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU1_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU1_PRD - ct_target_FPU1_PRD )) / ct_target_FPU1_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU1_QC = 
     CASE WHEN  (CASE WHEN ct_target_FPU1_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU1_QC - ct_target_FPU1_QC )) / ct_target_FPU1_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU1_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU1_QC - ct_target_FPU1_QC )) / ct_target_FPU1_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU1_RL = 
     CASE WHEN  (CASE WHEN ct_target_FPU1_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU1_RL - ct_target_FPU1_RL )) / ct_target_FPU1_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU1_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU1_RL - ct_target_FPU1_RL )) / ct_target_FPU1_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU2_PRD = 
     CASE WHEN  (CASE WHEN ct_target_FPU2_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU2_PRD - ct_target_FPU2_PRD )) / ct_target_FPU2_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU2_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU2_PRD - ct_target_FPU2_PRD )) / ct_target_FPU2_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU2_QC = 
     CASE WHEN  (CASE WHEN ct_target_FPU2_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU2_QC - ct_target_FPU2_QC )) / ct_target_FPU2_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU2_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU2_QC - ct_target_FPU2_QC )) / ct_target_FPU2_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU2_RL = 
     CASE WHEN  (CASE WHEN ct_target_FPU2_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU2_RL - ct_target_FPU2_RL )) / ct_target_FPU2_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU2_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU2_RL - ct_target_FPU2_RL )) / ct_target_FPU2_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU3_PRD = 
     CASE WHEN  (CASE WHEN ct_target_FPU3_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU3_PRD - ct_target_FPU3_PRD )) / ct_target_FPU3_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU3_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU3_PRD - ct_target_FPU3_PRD )) / ct_target_FPU3_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU3_QC = 
     CASE WHEN  (CASE WHEN ct_target_FPU3_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU3_QC - ct_target_FPU3_QC )) / ct_target_FPU3_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU3_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU3_QC - ct_target_FPU3_QC )) / ct_target_FPU3_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU3_RL = 
     CASE WHEN  (CASE WHEN ct_target_FPU3_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU3_RL - ct_target_FPU3_RL )) / ct_target_FPU3_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU3_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU3_RL - ct_target_FPU3_RL )) / ct_target_FPU3_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU4_PRD = 
     CASE WHEN  (CASE WHEN ct_target_FPU4_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU4_PRD - ct_target_FPU4_PRD )) / ct_target_FPU4_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU4_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU4_PRD - ct_target_FPU4_PRD )) / ct_target_FPU4_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU4_QC = 
     CASE WHEN  (CASE WHEN ct_target_FPU4_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU4_QC - ct_target_FPU4_QC )) / ct_target_FPU4_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU4_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU4_QC - ct_target_FPU4_QC )) / ct_target_FPU4_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU4_RL = 
     CASE WHEN  (CASE WHEN ct_target_FPU4_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU4_RL - ct_target_FPU4_RL )) / ct_target_FPU4_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU4_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU4_RL - ct_target_FPU4_RL )) / ct_target_FPU4_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU5_PRD = 
     CASE WHEN  (CASE WHEN ct_target_FPU5_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU5_PRD - ct_target_FPU5_PRD )) / ct_target_FPU5_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU5_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU5_PRD - ct_target_FPU5_PRD )) / ct_target_FPU5_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU5_QC = 
     CASE WHEN  (CASE WHEN ct_target_FPU5_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU5_QC - ct_target_FPU5_QC )) / ct_target_FPU5_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU5_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU5_QC - ct_target_FPU5_QC )) / ct_target_FPU5_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPU5_RL = 
     CASE WHEN  (CASE WHEN ct_target_FPU5_RL = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPU5_RL - ct_target_FPU5_RL )) / ct_target_FPU5_RL ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPU5_RL = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPU5_RL - ct_target_FPU5_RL )) / ct_target_FPU5_RL ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPP1_PRD = 
     CASE WHEN  (CASE WHEN ct_target_FPP1_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPP1_PRD - ct_target_FPP1_PRD )) / ct_target_FPP1_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPP1_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPP1_PRD - ct_target_FPP1_PRD )) / ct_target_FPP1_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPP1_QC = 
     CASE WHEN  (CASE WHEN ct_target_FPP1_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPP1_QC - ct_target_FPP1_QC )) / ct_target_FPP1_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPP1_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPP1_QC - ct_target_FPP1_QC )) / ct_target_FPP1_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 


update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPP2_PRD = 
     CASE WHEN  (CASE WHEN ct_target_FPP2_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPP2_PRD - ct_target_FPP2_PRD )) / ct_target_FPP2_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPP2_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPP2_PRD - ct_target_FPP2_PRD )) / ct_target_FPP2_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPP2_QC = 
     CASE WHEN  (CASE WHEN ct_target_FPP2_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPP2_QC - ct_target_FPP2_QC )) / ct_target_FPP2_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPP2_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPP2_QC - ct_target_FPP2_QC )) / ct_target_FPP2_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 


update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPP3_PRD = 
     CASE WHEN  (CASE WHEN ct_target_FPP3_PRD = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPP3_PRD - ct_target_FPP3_PRD )) / ct_target_FPP3_PRD ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPP3_PRD = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPP3_PRD - ct_target_FPP3_PRD )) / ct_target_FPP3_PRD ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

update fact_scm fmm 
 set fmm.ct_hitormisse2e_lt_FPP3_QC = 
     CASE WHEN  (CASE WHEN ct_target_FPP3_QC = 0  
     THEN 999  
     ELSE ( (( ct_actual_FPP3_QC - ct_target_FPP3_QC )) / ct_target_FPP3_QC ) * 100  
     END) < 25 AND  (CASE WHEN ct_target_FPP3_QC = 0  
     THEN 999  
     ELSE ( ((ct_actual_FPP3_QC - ct_target_FPP3_QC )) / ct_target_FPP3_QC ) * 100  
     END) > -25 
     THEN 100 
     ELSE 0 
     END; 

/*END of Hit Or Miss - Level 3*/                    



update fact_scm fmm
set fmm.ct_actualLTSales = AMT_ORDERAMOUNTSALES * ct_actualE2E_LT;

update fact_scm fmm
set fmm.CT_TARGETLTSALES = AMT_ORDERAMOUNTSALES * CT_TARGETE2E_LT;

	
update fact_scm fmm
set fmm.ct_hitvalue = (case when (CT_HITORMISSE2E_LT) = 0 then 0 else (AMT_ORDERAMOUNTSALES) end);	

/* End BI-4857 */	
	
/* Reset all the values from the Sales aggregation dimension - BI-3287 */
/* BI-5265 - include ACTIVE_PROP_UNIT in addition to ACTIVE */
delete from dim_aggregationMMProdOrd;

delete from number_fountain m where m.table_name = 'dim_aggregationMMProdOrd';
insert into number_fountain
select 'dim_aggregationMMProdOrd',
ifnull(max(da.dim_aggregationMMProdOrdId),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_aggregationMMProdOrd da;

insert into dim_aggregationMMProdOrd
	(	
		dim_aggregationMMProdOrdId,
		ProductFamily_Merck,
		PlantCode,
		PlantCodeSite,
		PartNumber,
		MonthYear,
		sum_fpp,
		salesOrderHit_fpp,
		totalSalesOrderValueHitUSD_fpp,
		TotalActualLTSales_fpp,
		TotalTargetLTSales_fpp
	)
select (select max_id  from number_fountain   where table_name = 'dim_aggregationMMProdOrd') + row_number() over(ORDER BY '') AS dim_aggregationMMProdOrdId,
	x.ProductFamily_Merck, 
	x.PlantCode, 
	x.PlantCodeSite,
	x.PartNumber,	
	x.MonthYear,
    sum(amt_orderAmountSales) sum_fpp,
	avg(ct_hitormissE2E_LT) as salesOrderHit_fpp,
	sum(ct_hitvalue) as totalSalesOrderValueHitUSD_fpp,
	sum(ct_actualLTSales) as TotalActualLTSales_fpp,
	sum(ct_TargetLTSales) as TotalTargetLTSales_fpp
	/* avg(amt_orderAmountSales)/ ( case when sum(amt_orderAmountSales) <> 0 then  sum(amt_orderAmountSales) else 1 end) * max(ct_actualE2E_LT) as weightedActualLT_fpp,
	avg(amt_orderAmountSales)/ ( case when sum(amt_orderAmountSales) <> 0 then  sum(amt_orderAmountSales) else 1 end) * max(ct_targetE2E_LT) as weightedTargetLT_fpp */
from 
	(select mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, p.ProductFamily_Merck, pl.PlantCode, 
		p.PartNumber, plsite.PlantCode as PlantCodeSite, dd.MonthYear,
		/* amt_orderAmountSales * amt_ExchangeRate_GBL as amt_orderAmountSales, */
		amt_orderAmountSales as amt_orderAmountSales,
		ct_hitormissE2E_LT as ct_hitormissE2E_LT,
		ct_hitvalue as ct_hitvalue,
		ct_actualLTSales as ct_actualLTSales,
		ct_TargetLTSales as ct_TargetLTSales,
		row_number() over ( partition by mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, p.ProductFamily_Merck, pl.PlantCode,
							p.PartNumber, plsite.PlantCode, dd.MonthYear order by '') rn
		from fact_scm mmp, dim_part p, dim_date dd, dim_plant pl, dim_plant plsite, dim_part ma,  dim_part pp,dim_date darant1
		where mmp.dim_partid_fpp_final = p.dim_partid
		and mmp.dim_plantid_comops = pl.dim_plantid
		and mmp.dim_plantid = plsite.dim_plantid
		and mmp.dim_partid_antigen_1 = pp.dim_partid
		and mmp.dim_dateidactualgoodsissue_customer = dd.dim_dateid
		and dd.datevalue >= add_years(current_date, -2)
		and mmp.dim_dateidactualrelease_antigen_1 = darant1.dim_dateid
		and mmp.dim_partid_raw_1 = ma.dim_partid
		and ( (p.ProductGroup_Merck = 'B' and (pp.PartNumber_NoLeadZero <> 'Not Set' and mmp.dim_dateidactualrelease_antigen_1 <> 1) ) or 
		( p.ProductGroup_Merck <>'B' and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')))
		) x
	where x.rn = 1
	group by CUBE (ProductFamily_Merck, PlantCode, PlantCodeSite, PartNumber, MonthYear);  
	
/* Madalina 13 Feb 2017 - Change the calculation for Total Weighted av. Actual LT -backend and Total Weighted av. Target LT -backend BI-4857 */	
update dim_aggregationMMProdOrd dimagg
set dimagg.totalWeightedActualLT = TotalActualLTSales_fpp/ case when sum_fpp = 0 then 1 else sum_fpp end,
	dimagg.totalWeightedTargetLT = TotalTargetLTSales_fpp/ case when sum_fpp = 0 then 1 else sum_fpp end;

/* 26 jan 2017 Georgiana Changes - according to BI 5295: Insert all GPF codes in dim_aggregationMMProdOrd*/
delete from number_fountain m where m.table_name = 'dim_aggregationMMProdOrd';
insert into number_fountain
select 'dim_aggregationMMProdOrd',
ifnull(max(da.dim_aggregationMMProdOrdId),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_aggregationMMProdOrd da;

insert into dim_aggregationMMProdOrd (dim_aggregationMMProdOrdid,ProductFamily_Merck)
select (select max_id  from number_fountain   where table_name = 'dim_aggregationMMProdOrd') + row_number() over(ORDER BY '') AS dim_aggregationMMProdOrdId,
t.ProductFamily_Merck
from (
select distinct ProductFamily_Merck from dim_part dp
where not exists (select 1 from dim_aggregationMMProdOrd agg where dp.ProductFamily_Merck=agg.ProductFamily_Merck)) t;
/*End of changes 26 Jan 2017*/

update dim_aggregationMMProdOrd
set plantcode = 'ZZZZ' where Plantcode is null;

update dim_aggregationMMProdOrd
set ProductFamily_Merck = 'ZZZZ' where ProductFamily_Merck is null;

update dim_aggregationMMProdOrd
set PlantCodeSite = 'ZZZZ' where PlantCodeSite is null;

update dim_aggregationMMProdOrd
set PartNumber = 'ZZZZ' where PartNumber is null;

update dim_aggregationMMProdOrd
set MonthYear = 'ZZZZ' where MonthYear is null;

/* APP-9820 - Madalina 25 Jun 2018 - calculate fields earlier in the script - to be used for following measure  */
/* Madalina 15Jan2018 - APP-7695 */
update fact_scm f_mmprodhier
set dd_plantcode_agg = pl.PlantCode
from fact_scm f_mmprodhier
	INNER JOIN Dim_Plant AS pl ON f_mmprodhier.dim_plantid = pl.Dim_Plantid;

update fact_scm f_mmprodhier
set dd_plantcode_comops_agg = pc.PlantCode
from fact_scm f_mmprodhier
	INNER JOIN Dim_Plant AS pc ON f_mmprodhier.dim_plantid_comops = pc.Dim_Plantid;

update fact_scm f_mmprodhier
set dd_partnumber_agg = pprtfppf.PartNumber,
	dd_ProductFamily_Merck_agg = pprtfppf.ProductFamily_Merck
from fact_scm f_mmprodhier
	INNER JOIN Dim_Part AS pprtfppf ON f_mmprodhier.DIM_PARTID_FPP_FINAL = pprtfppf.Dim_Partid;

update fact_scm f_mmprodhier
set dd_monthyear_agg = agidcc.MonthYear
from fact_scm f_mmprodhier
	INNER JOIN Dim_Date AS agidcc ON f_mmprodhier.dim_dateidactualgoodsissue_customer = agidcc.Dim_Dateid;
/* END APP-7695 */

/*Georgiana Changes 23 Jan 2017 adding new field ct_overallsupplyhealthscore, according to BI-5295*/

drop table if exists tmp_for_upd_SupplyHealthScore;
create table tmp_for_upd_SupplyHealthScore as 
SELECT x.*  from       ( 
	SELECT 
	DFA.ProductFamilyDescription_Merck , 
	DFA.ct_avg_hit_miss AS  HitOrMissFPPComop, 
	MinMax.CT_HITORMISS AS GrobalHitLast2YDFA , 
	salesorder.ct_avg_hit_miss_lifr AS GrobalHitLast2YLIFR , 
	materialmaster.CAPACITY_SCORE AS CapacityScore,
   ifnull(mmprod.SalesORderValueHit,0) as SalesORderValueHit	
FROM (SELECT aplfprt.ProductFamilyDescription_Merck,ROUND(AVG(ct_avg_hit_miss),2) as ct_avg_hit_miss 
					FROM fact_atlaspharmlogiforecast_merck AS f_iaplf 
					INNER JOIN Dim_Part AS aplfprt ON f_iaplf.dim_partid = aplfprt.Dim_Partid  
					INNER JOIN Dim_Date AS repdt ON f_iaplf.dim_dateidreporting = repdt.Dim_Dateid  
			WHERE (repdt.DateValue  BETWEEN '2015-01-01' AND '2016-12-31')  
			AND f_iaplf.dd_version = 'SFA'  
			AND f_iaplf.dd_regiondestionation NOT IN ('Head Quarters','NOT APPLICABLE')  
	        GROUP BY  aplfprt.ProductFamilyDescription_Merck
	) DFA  
LEFT  JOIN (SELECT dp.ProductFamilyDescription_Merck,ROUND(AVG(CT_HITORMISS),2) as CT_HITORMISS 
                   FROM fact_inventoryatlashistory AS f_invatlashist 
                   INNER JOIN Dim_Date AS dd3 ON f_invatlashist.dim_snapshotdateid = dd3.Dim_Dateid  
                   INNER JOIN Dim_Part AS dp ON f_invatlashist.dim_partid = dp.Dim_Partid 
                   INNER JOIN Dim_Plant AS dpl ON f_invatlashist.dim_plantid = dpl.Dim_Plantid  
              WHERE  dd3.WeekDayAbbreviation  = 'Mon'
              AND dp.ItemSubType_Merck = 'FPP'
              AND dpl.tacticalring_merck = 'ComOps'
              GROUP BY  dp.ProductFamilyDescription_Merck
	        ) MinMax ON (  DFA.ProductFamilyDescription_Merck = MinMax.ProductFamilyDescription_Merck ) 
LEFT  JOIN  (SELECT prt.ProductFamilyDescription_Merck,ROUND(AVG(ct_avg_hit_miss_lifr),0) as ct_avg_hit_miss_lifr 
                   FROM fact_salesorder AS f_so 
                   INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid  
                   INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid  
            WHERE (soegidt.DateValue  != current_date) AND (soegidt.DateValue  BETWEEN '2015-01-01' AND '2016-12-31')  AND prt.ProductHierarchy = 'Not Set' AND f_so.dd_ItemRelForDelv = 'X' 
 GROUP BY  prt.ProductFamilyDescription_Merck
             ) salesorder ON (  DFA.ProductFamilyDescription_Merck = salesorder.ProductFamilyDescription_Merck ) 
 INNER JOIN (SELECT m.ProductFamilyDescription_Merck,ROUND(AVG( (m.CAPACITY_SCORE) ),0) as CAPACITY_SCORE 
                   FROM fact_materialmaster AS f_mmi 
                   INNER JOIN Dim_Part AS m ON f_mmi.dim_materialmasterid = m.Dim_Partid  
                   GROUP BY  m.ProductFamilyDescription_Merck
			)  materialmaster  ON (  DFA.ProductFamilyDescription_Merck = materialmaster.ProductFamilyDescription_Merck )
LEFT  JOIN (SELECT mfp.ProductFamilyDescription_Merck,AVG(100 * agg3.totalSalesOrderValueHitUSD_fpp / case when agg3.SUM_FPP=0 then 1 else agg3.SUM_FPP end) as  SalesORderValueHit 
 FROM fact_scm AS f_mmprodhier 
 INNER JOIN Dim_Part AS mfp ON f_mmprodhier.dim_partid_fpp_final = mfp.Dim_Partid 
 INNER JOIN dim_aggregationMMProdOrd AS agg3 ON agg3.PlantCode = f_mmprodhier.dd_defaultaggvalue 
 and agg3.ProductFamily_Merck = mfp.ProductFamily_Merck 
 and agg3.PlantCodeSite = f_mmprodhier.dd_defaultaggvalue   
 and agg3.PartNumber =  f_mmprodhier.dd_defaultaggvalue  and 
 agg3.MonthYear =  f_mmprodhier.dd_defaultaggvalue  
/* INNER JOIN Dim_Date AS agidcc ON f_mmprodhier.DIM_DATEIDACTUALGOODSISSUE_CUSTOMER = agidcc.Dim_Dateid  
 INNER JOIN Dim_Plant AS pc ON f_mmprodhier.DIM_PLANTID_COMOPS = pc.Dim_Plantid  
 INNER JOIN Dim_Plant AS pl ON f_mmprodhier.DIM_PLANTID = pl.Dim_Plantid  
 INNER JOIN Dim_Part AS mab ON f_mmprodhier.DIM_PARTID_BULK_1 = mab.Dim_Partid  
 INNER JOIN Dim_Part AS mfpc ON f_mmprodhier.DIM_PARTID_COMOPS = mfpc.Dim_Partid  
 INNER JOIN Dim_Part AS mf ON f_mmprodhier.DIM_PARTID_FPU_1 = mf.Dim_Partid  
 INNER JOIN Dim_Part AS ma ON f_mmprodhier.DIM_PARTID_RAW_1 = ma.Dim_Partid  
 INNER JOIN Dim_Date AS posl ON f_mmprodhier.DIM_DATEIDSCHEDORDER = posl.Dim_Dateid   
 INNER JOIN dim_aggregationMMProdOrd AS agg3 ON agg3.PlantCode = (case when 'ALL' = 'ALL' then f_mmprodhier.dd_defaultaggvalue  else pc.plantcode end) 
 and agg3.ProductFamily_Merck = (case when 'ZZZZ' = 'ALL' then f_mmprodhier.dd_defaultaggvalue  else mfp.ProductFamily_Merck end) 
 and agg3.PlantCodeSite = (case when 'ALL' = 'ALL' then f_mmprodhier.dd_defaultaggvalue  else pl.PlantCode end) 
 and agg3.PartNumber = (case when 'ALL' = 'ALL' then f_mmprodhier.dd_defaultaggvalue  else mfp.PartNumber end) and 
 agg3.MonthYear = (case when 'ALL' = 'ALL' then f_mmprodhier.dd_defaultaggvalue  else agidcc.MonthYear end )      APP-9820 - solve performance issue */
 GROUP BY  mfp.ProductFamilyDescription_Merck) mmprod
 ON (  DFA.ProductFamilyDescription_Merck = mmprod.ProductFamilyDescription_Merck )
) x order by ProductFamilyDescription_Merck; 

merge into fact_scm f_mmp
using (select distinct fact_mmprodhierarchyid,
0.15 *( case when SalesORderValueHit > 80 then 3 when SalesORderValueHit > 60 then 2 else 1 end)
+ 0.15 * (case when tmp.GrobalHitLast2YDFA > 80 then 3 when tmp.GrobalHitLast2YDFA > 60 then 2 else 1 end)
+ 0.15* ( case when tmp.HitOrMissFPPComop > 80 then 3 when tmp.HitOrMissFPPComop > 60 then 2 else 1 end)
+ 0.3 * tmp.CapacityScore
+ 0.25* ( case when tmp.GrobalHitLast2YLIFR > 80 then 3 else 1 end) as overallsupplyhealthscore
 from fact_scm f_mmp,tmp_for_upd_SupplyHealthScore tmp,dim_part dp 
where tmp.ProductFamilyDescription_Merck=dp.ProductFamilyDescription_Merck
and  f_mmp.dim_partid_fpp_final=dp.dim_partid 
/*and dp.ProductFamilyDescription_Merck='6075 Rotavec Corona'*/
) t
on t.fact_mmprodhierarchyid=f_mmp.fact_mmprodhierarchyid
when matched then update set f_mmp.ct_overallsupplyhealthscore=t.overallsupplyhealthscore;

/* Add include e2e lt Andrei R */
update fact_scm s
set dd_includee2elt = CASE WHEN pprtfppf.ProductGroup_Merck = 'B'
THEN CASE WHEN pprtant1.PartNumber_NoLeadZero = 'Not Set' OR darant1.DateValue = '0001-01-01'
THEN 'N'
ELSE 'Y'
END
ELSE CASE WHEN ma.batchclass_merck = 'ACTIVE' or ma.batchclass_merck = 'ACTIVE_PROP_UNIT' 
THEN 'Y'
ELSE 'N'
END
END
from fact_scm s inner join dim_part pprtfppf on s.dim_partid_fpp_final = pprtfppf.dim_partid
                inner join dim_part pprtant1 on s.dim_partid_antigen_1 = pprtant1.dim_partid
				inner join dim_date darant1 on s.dim_dateidactualrelease_antigen_1 = darant1.dim_dateid
				inner join dim_part ma on s.dim_partid_raw_1 = ma.dim_partid
where dd_includee2elt <> CASE WHEN pprtfppf.ProductGroup_Merck = 'B'
THEN CASE WHEN pprtant1.PartNumber_NoLeadZero = 'Not Set' OR darant1.DateValue = '0001-01-01'
THEN 'N'
ELSE 'Y'
END
ELSE CASE WHEN ma.batchclass_merck = 'ACTIVE' or ma.batchclass_merck = 'ACTIVE_PROP_UNIT' 
THEN 'Y'
ELSE 'N'
END
END;
/* end Add include e2e lt Andrei R */



/* Avoid table recreation */
/* drop table if exists fact_mmprodhierarchy
rename table fact_scm to fact_mmprodhierarchy */


truncate table fact_mmprodhierarchy;
insert into fact_mmprodhierarchy
(FACT_MMPRODHIERARCHYID, 
DIM_PLANTID, 
DD_INSPECTIONLOTNOT_RAW, 
DIM_RAWPARTID, 
DIM_RAWBATCHID, 
DD_PODOCUMENTNO, 
DD_PODOCUMENTITEMNO, 
DD_SCHEDULENO, 
DIM_DATEIDSCHEDORDER, 
DIM_DATEIDCREATE, 
DIM_DATEIDRAWPOSTINGDATE, 
DD_RAWDATEUSERSTATUSQCCO, 
DIM_DATEIDRAWINSPECTIONSTART, 
DD_RAWDATEUSERSTATUSSAMR, 
DIM_DATEIDRAWUSAGEDECISIONMADE, 
DD_ORDERNUMBERRAWBULK, 
DIM_BULKPARTID, 
DIM_BULKBATCHIDPROD, 
DIM_DATEIDBULKACTUALRELEASE, 
DIM_DATEIDBULKACTUALSTART, 
DIM_DATEIDBULKACTUALHEADERFINISH, 
DD_INSPECTIONLOTNO_BULK, 
DD_DATEUSERSTATUSQCCO_BULK, 
DIM_DATEIDINSPECTIONSTART_BULK, 
DD_DATEUSERSTATUSSAMR_BULK, 
DIM_DATEIDUSAGEDECISIONMADE_BULK, 
DD_ORDERNUMBER_BULK_FPU, 
DIM_FPUPARTID, 
DIM_FPUBATCHIDPROD, 
DIM_DATEIDFPUACTUALRELEASE, 
DIM_DATEIDFPUACTUALSTART, 
DIM_DATEIDFPUACTUALHEADERFINISH, 
DD_INSPECTIONLOTNO_FPU, 
DD_DATEUSERSTATUSSAMR_FPU, 
DD_DATEUSERSTATUSQCCO_FPU, 
DIM_DATEIDFPUINSPECTIONSTART, 
DIM_DATEIDFPUUSAGEDECISIONMADE, 
DD_ORDERNUMBER_FPU_FPP, 
DIM_FPPPARTID, 
DIM_FPPBATCHIDPROD, 
DIM_DATEIDFPPACTUALRELEASE, 
DIM_DATEIDFPPACTUALSTART, 
DIM_DATEIDFPPACTUALHEADERFINISH, 
DD_INSPECTIONLOTNO_FPP, 
DD_DATEUSERSTATUSSAMR_FPP, 
DD_DATEUSERSTATUSQCCO_FPP, 
DIM_DATEIDFPPINSPECTIONSTART, 
DIM_DATEIDFPPUSAGEDECISIONMADE, 
DIM_PLANTID_COMOPS, 
DD_SALESDLVRDOCNO_COMOPS, 
DD_SALESDLVRITEMNO_COMOPS, 
DIM_DATEIDDLVRDOCCREATED_COMOPS, 
DIM_DATEIDACTUALGOODSISSUE_COMOPS, 
DIM_DATEIDACTUALRECEIPT_COMOPS, 
DD_INSPECTIONLOTNO_COMOPS, 
DD_DATEUSERSTATUSSAMR_COMOPS, 
DIM_DATEIDINSPECTIONSTART_COMOPS, 
DD_DATEUSERSTATUSQCCO_COMOPS, 
DIM_DATEIDUSAGEDECISIONMADE_COMOPS, 
DD_SALESDLVRDOCNO_CUSTOMER, 
DD_SALESDLVRITEMNO_CUSTOMER, 
DIM_DATEIDDLVRDOCCREATED_CUSTOMER, 
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER, 
CT_QTYDELIVERED, 
DD_SALESDOCNO_CUSTOMER, 
DD_SALESITEMNO_CUSTOMER, 
AMT_ORDERAMOUNTSALES, 
AMT_EXCHANGERATE, 
AMT_EXCHANGERATE_GBL, 
DIM_FPPPARTID_COMOPS, 
DIM_CURRENCYID, 
DIM_CUSTOMERSHIPCOMOPS, 
DIM_CUSTOMERSOLDCOMOPS, 
DIM_CUSTOMERSHIPCUST, 
DIM_CUSTOMERSOLDCUST, 
DD_SALESDOCNO_COMOPS, 
DD_SALESITEMNO_COMOPS, 
AMT_ORDERAMOUNTSALESFPP, 
DIM_CURRENCYID_TRA, 
CT_ORDERAMOUNTSALESFPP, 
CT_ORDERAMOUNTSALESFPP2, 
CT_ORDERAMOUNTSALESFPP_2, 
CT_TARGETMASTERRECIPEFPP, 
CT_TARGETMASTERRECIPERAW, 
CT_TARGETMASTERRECIPEFPU, 
CT_TARGETMASTERRECIPEBULK, 
CT_TARGETMASTERRECIPEFPP_COMOP, 
DIM_RAWDATEUSERSTATUSSAMR, 
DIM_RAWDATEUSERSTATUSQCCO, 
DIM_DATEUSERSTATUSSAMR_BULK, 
DIM_DATEUSERSTATUSQCCO_BULK, 
DIM_DATEUSERSTATUSSAMR_FPU, 
DIM_DATEUSERSTATUSQCCO_FPU, 
DIM_DATEUSERSTATUSSAMR_FPP, 
DIM_DATEUSERSTATUSQCCO_FPP, 
CT_MINRELEASEDSTOCKDAYSRAW, 
CT_MINRELEASEDSTOCKDAYSBULK, 
CT_MINRELEASEDSTOCKDAYSFPU, 
CT_MINRELEASEDSTOCKDAYSFPPSITE, 
CT_MINRELEASEDSTOCKDAYSFPPCOMOP, 
CT_LOTSIZEDAYSRAW, 
CT_LOTSIZEDAYSBULK, 
CT_LOTSIZEDAYSFPU, 
CT_LOTSIZEDAYSFPPSITE, 
CT_LOTSIZEDAYSFPPCOMOP, 
CT_FLOATBEFOREPRODUCTIONRAW, 
CT_FLOATBEFOREPRODUCTIONFPU, 
CT_FLOATBEFOREPRODUCTIONFPP, 
CT_FLOATBEFOREPRODUCTIONFPPCOMOP, 
CT_FLOATBEFOREPRODUCTIONBULK, 
CT_HITORMISSE2E_LT, 
CT_HITVALUE, 
CT_ACTUALE2E_LT, 
CT_TARGETE2E_LT, 
CT_OVERALLSUPPLYHEALTHSCORE, 
CT_WEIGHTEDACTUALLT, 
CT_WEIGHTEDTARGETLT, 
DIM_BULKPARTID_2, 
DIM_BULKBATCHIDPROD_2, 
DD_ORDERNUMBERBULK_BULK, 
DIM_DATEIDBULK_2ACTUALRELEASE, 
DIM_DATEIDBULK_2ACTUALSTART, 
DIM_DATEIDBULK_2ACTUALHEADERFINISH, 
DD_ORDERNUMBER_BULK_2_FPU, 
DD_ORDERNUMBER_BULK_2_FPP, 
CT_ACTUALLTSALES, 
CT_TARGETLTSALES, 
DIM_BULKPARTID_3, 
DIM_BULKBATCHIDPROD_3, 
DD_ORDERNUMBERBULK_2_BULK_3, 
DIM_DATEIDBULK_3ACTUALRELEASE, 
DIM_DATEIDBULK_3ACTUALSTART, 
DIM_DATEIDBULK_3ACTUALHEADERFINISH, 
DD_ORDERNUMBER_BULK_3_FPU, 
DD_ORDERNUMBER_BULK_3_FPP, 
DD_ORDERNUMBER_BULK_3_FPU_1, 
DD_ORDERNUMBER_BULK_3_FPP_1, 
DD_ORDERNUMBER_BULK_2_FPP_1, 
DD_ORDERNUMBER_BULK_2_FPU_1, 
DD_ORDERNUMBER_BULK_1_FPP_1, 
DD_ORDERNUMBER_BULK_1_FPU_1, 
DD_DOCUMENTNO, 
DD_DOCUMENTITEMNO, 
DIM_DATEIDPOSTINGDATE, 
DIM_PARTID_COMOPS, 
CT_TARGETMASTERRECIPE_COMOPS, 
CT_MINRELEASEDSTOCKDAYS_COMOPS, 
CT_LOTSIZEDAYS_COMOPS, 
CT_FLOATBEFOREPRODUCTION_COMOPS, 
DD_INSPECTIONLOTNO_BULK_2, 
DIM_PARTID_BULK_2, 
DIM_BATCHID_BULK_2, 
DD_DATEUSERSTATUSQCCO_BULK_2, 
DD_DATEUSERSTATUSSAMR_BULK_2, 
DIM_DATEIDINSPECTIONSTART_BULK_2, 
DIM_DATEIDUSAGEDECISIONMADE_BULK_2, 
DIM_DATEIDACTUALRELEASE_BULK_2, 
DIM_DATEIDACTUALSTART_BULK_2, 
DIM_DATEIDACTUALHEADERFINISH_BULK_2, 
CT_TARGETMASTERRECIPE_BULK_2, 
DIM_DATEUSERSTATUSSAMR_BULK_2, 
DIM_DATEUSERSTATUSQCCO_BULK_2, 
CT_MINRELEASEDSTOCKDAYS_BULK_2, 
CT_LOTSIZEDAYS_BULK_2, 
CT_FLOATBEFOREPRODUCTION_BULK_2, 
DD_INSPECTIONLOTNO_ANTIGEN_2, 
DIM_PARTID_ANTIGEN_2, 
DIM_BATCHID_ANTIGEN_2, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_2, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_2, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_2, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_2, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_2, 
DIM_DATEIDACTUALSTART_ANTIGEN_2, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_2, 
CT_TARGETMASTERRECIPE_ANTIGEN_2, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_2, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_2, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_2, 
CT_LOTSIZEDAYS_ANTIGEN_2, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_2, 
DD_ORDERNUMBER_BULK_2_BULK_3, 
DD_ORDERNUMBER_BULK_1_BULK_3, 
DD_INSPECTIONLOTNO_ANTIGEN_5, 
DIM_PARTID_ANTIGEN_5, 
DIM_BATCHID_ANTIGEN_5, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_5, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_5, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_5, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_5, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_5, 
DIM_DATEIDACTUALSTART_ANTIGEN_5, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_5, 
CT_TARGETMASTERRECIPE_ANTIGEN_5, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_5, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_5, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_5, 
CT_LOTSIZEDAYS_ANTIGEN_5, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_5, 
DD_ORDERNUMBER_RAW_1_BULK_2, 
DD_INSPECTIONLOTNO_FPP_3, 
DIM_PARTID_FPP_3, 
DIM_BATCHID_FPP_3, 
DD_DATEUSERSTATUSQCCO_FPP_3, 
DD_DATEUSERSTATUSSAMR_FPP_3, 
DIM_DATEIDINSPECTIONSTART_FPP_3, 
DIM_DATEIDUSAGEDECISIONMADE_FPP_3, 
DIM_DATEIDACTUALRELEASE_FPP_3, 
DIM_DATEIDACTUALSTART_FPP_3, 
DIM_DATEIDACTUALHEADERFINISH_FPP_3, 
CT_TARGETMASTERRECIPE_FPP_3, 
DIM_DATEUSERSTATUSSAMR_FPP_3, 
DIM_DATEUSERSTATUSQCCO_FPP_3, 
CT_MINRELEASEDSTOCKDAYS_FPP_3, 
CT_LOTSIZEDAYS_FPP_3, 
CT_FLOATBEFOREPRODUCTION_FPP_3, 
DD_INSPECTIONLOTNO_FPP_2, 
DIM_PARTID_FPP_2, 
DIM_BATCHID_FPP_2, 
DD_DATEUSERSTATUSQCCO_FPP_2, 
DD_DATEUSERSTATUSSAMR_FPP_2, 
DIM_DATEIDINSPECTIONSTART_FPP_2, 
DIM_DATEIDUSAGEDECISIONMADE_FPP_2, 
DIM_DATEIDACTUALRELEASE_FPP_2, 
DIM_DATEIDACTUALSTART_FPP_2, 
DIM_DATEIDACTUALHEADERFINISH_FPP_2, 
CT_TARGETMASTERRECIPE_FPP_2, 
DIM_DATEUSERSTATUSSAMR_FPP_2, 
DIM_DATEUSERSTATUSQCCO_FPP_2, 
CT_MINRELEASEDSTOCKDAYS_FPP_2, 
CT_LOTSIZEDAYS_FPP_2, 
CT_FLOATBEFOREPRODUCTION_FPP_2, 
DD_ORDERNUMBER_RAW_1_FPP_1, 
DD_INSPECTIONLOTNO_FPP_1, 
DIM_PARTID_FPP_1, 
DIM_BATCHID_FPP_1, 
DD_DATEUSERSTATUSQCCO_FPP_1, 
DD_DATEUSERSTATUSSAMR_FPP_1, 
DIM_DATEIDINSPECTIONSTART_FPP_1, 
DIM_DATEIDUSAGEDECISIONMADE_FPP_1, 
DIM_DATEIDACTUALRELEASE_FPP_1, 
DIM_DATEIDACTUALSTART_FPP_1, 
DIM_DATEIDACTUALHEADERFINISH_FPP_1, 
CT_TARGETMASTERRECIPE_FPP_1, 
DIM_DATEUSERSTATUSSAMR_FPP_1, 
DIM_DATEUSERSTATUSQCCO_FPP_1, 
CT_MINRELEASEDSTOCKDAYS_FPP_1, 
CT_LOTSIZEDAYS_FPP_1, 
CT_FLOATBEFOREPRODUCTION_FPP_1, 
DD_INSPECTIONLOTNO_FPU_2, 
DIM_PARTID_FPU_2, 
DIM_BATCHID_FPU_2, 
DD_DATEUSERSTATUSQCCO_FPU_2, 
DD_DATEUSERSTATUSSAMR_FPU_2, 
DIM_DATEIDINSPECTIONSTART_FPU_2, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_2, 
DIM_DATEIDACTUALRELEASE_FPU_2, 
DIM_DATEIDACTUALSTART_FPU_2, 
DIM_DATEIDACTUALHEADERFINISH_FPU_2, 
CT_TARGETMASTERRECIPE_FPU_2, 
DIM_DATEUSERSTATUSSAMR_FPU_2, 
DIM_DATEUSERSTATUSQCCO_FPU_2, 
CT_MINRELEASEDSTOCKDAYS_FPU_2, 
CT_LOTSIZEDAYS_FPU_2, 
CT_FLOATBEFOREPRODUCTION_FPU_2, 
DD_INSPECTIONLOTNO_FPU_3, 
DIM_PARTID_FPU_3, 
DIM_BATCHID_FPU_3, 
DD_DATEUSERSTATUSQCCO_FPU_3, 
DD_DATEUSERSTATUSSAMR_FPU_3, 
DIM_DATEIDINSPECTIONSTART_FPU_3, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_3, 
DIM_DATEIDACTUALRELEASE_FPU_3, 
DIM_DATEIDACTUALSTART_FPU_3, 
DIM_DATEIDACTUALHEADERFINISH_FPU_3, 
CT_TARGETMASTERRECIPE_FPU_3, 
DIM_DATEUSERSTATUSSAMR_FPU_3, 
DIM_DATEUSERSTATUSQCCO_FPU_3, 
CT_MINRELEASEDSTOCKDAYS_FPU_3, 
CT_LOTSIZEDAYS_FPU_3, 
CT_FLOATBEFOREPRODUCTION_FPU_3, 
DD_INSPECTIONLOTNO_FPU_5, 
DIM_PARTID_FPU_5, 
DIM_BATCHID_FPU_5, 
DD_DATEUSERSTATUSQCCO_FPU_5, 
DD_DATEUSERSTATUSSAMR_FPU_5, 
DIM_DATEIDINSPECTIONSTART_FPU_5, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_5, 
DIM_DATEIDACTUALRELEASE_FPU_5, 
DIM_DATEIDACTUALSTART_FPU_5, 
DIM_DATEIDACTUALHEADERFINISH_FPU_5, 
CT_TARGETMASTERRECIPE_FPU_5, 
DIM_DATEUSERSTATUSSAMR_FPU_5, 
DIM_DATEUSERSTATUSQCCO_FPU_5, 
CT_MINRELEASEDSTOCKDAYS_FPU_5, 
CT_LOTSIZEDAYS_FPU_5, 
CT_FLOATBEFOREPRODUCTION_FPU_5, 
DD_ORDERNUMBER_RAW_1_FPU_1, 
DD_INSPECTIONLOTNO_FPU_1, 
DIM_PARTID_FPU_1, 
DIM_BATCHID_FPU_1, 
DD_DATEUSERSTATUSQCCO_FPU_1, 
DD_DATEUSERSTATUSSAMR_FPU_1, 
DIM_DATEIDINSPECTIONSTART_FPU_1, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_1, 
DIM_DATEIDACTUALRELEASE_FPU_1, 
DIM_DATEIDACTUALSTART_FPU_1, 
DIM_DATEIDACTUALHEADERFINISH_FPU_1, 
CT_TARGETMASTERRECIPE_FPU_1, 
DIM_DATEUSERSTATUSSAMR_FPU_1, 
DIM_DATEUSERSTATUSQCCO_FPU_1, 
CT_MINRELEASEDSTOCKDAYS_FPU_1, 
CT_LOTSIZEDAYS_FPU_1, 
CT_FLOATBEFOREPRODUCTION_FPU_1, 
DD_INSPECTIONLOTNO_BULK_3, 
DIM_PARTID_BULK_3, 
DIM_BATCHID_BULK_3, 
DD_DATEUSERSTATUSQCCO_BULK_3, 
DD_DATEUSERSTATUSSAMR_BULK_3, 
DIM_DATEIDINSPECTIONSTART_BULK_3, 
DIM_DATEIDUSAGEDECISIONMADE_BULK_3, 
DIM_DATEIDACTUALRELEASE_BULK_3, 
DIM_DATEIDACTUALSTART_BULK_3, 
DIM_DATEIDACTUALHEADERFINISH_BULK_3, 
CT_TARGETMASTERRECIPE_BULK_3, 
DIM_DATEUSERSTATUSSAMR_BULK_3, 
DIM_DATEUSERSTATUSQCCO_BULK_3, 
CT_MINRELEASEDSTOCKDAYS_BULK_3, 
CT_LOTSIZEDAYS_BULK_3, 
CT_FLOATBEFOREPRODUCTION_BULK_3, 
DD_INSPECTIONLOTNO_BULK_1, 
DIM_PARTID_BULK_1, 
DIM_BATCHID_BULK_1, 
DD_DATEUSERSTATUSQCCO_BULK_1, 
DD_DATEUSERSTATUSSAMR_BULK_1, 
DIM_DATEIDINSPECTIONSTART_BULK_1, 
DIM_DATEIDUSAGEDECISIONMADE_BULK_1, 
DIM_DATEIDACTUALRELEASE_BULK_1, 
DIM_DATEIDACTUALSTART_BULK_1, 
DIM_DATEIDACTUALHEADERFINISH_BULK_1, 
CT_TARGETMASTERRECIPE_BULK_1, 
DIM_DATEUSERSTATUSSAMR_BULK_1, 
DIM_DATEUSERSTATUSQCCO_BULK_1, 
CT_MINRELEASEDSTOCKDAYS_BULK_1, 
CT_LOTSIZEDAYS_BULK_1, 
CT_FLOATBEFOREPRODUCTION_BULK_1, 
DD_INSPECTIONLOTNO_ANTIGEN_1, 
DIM_PARTID_ANTIGEN_1, 
DIM_BATCHID_ANTIGEN_1, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_1, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_1, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_1, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_1, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_1, 
DIM_DATEIDACTUALSTART_ANTIGEN_1, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_1, 
CT_TARGETMASTERRECIPE_ANTIGEN_1, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_1, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_1, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_1, 
CT_LOTSIZEDAYS_ANTIGEN_1, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_1, 
DD_INSPECTIONLOTNO_RAW_1, 
DIM_PARTID_RAW_1, 
DIM_BATCHID_RAW_1, 
DD_DATEUSERSTATUSQCCO_RAW_1, 
DD_DATEUSERSTATUSSAMR_RAW_1, 
DIM_DATEIDINSPECTIONSTART_RAW_1, 
DIM_DATEIDUSAGEDECISIONMADE_RAW_1, 
DIM_DATEIDACTUALRELEASE_RAW_1, 
DIM_DATEIDACTUALSTART_RAW_1, 
DIM_DATEIDACTUALHEADERFINISH_RAW_1, 
CT_TARGETMASTERRECIPE_RAW_1, 
DIM_DATEUSERSTATUSSAMR_RAW_1, 
DIM_DATEUSERSTATUSQCCO_RAW_1, 
CT_MINRELEASEDSTOCKDAYS_RAW_1, 
CT_LOTSIZEDAYS_RAW_1, 
CT_FLOATBEFOREPRODUCTION_RAW_1, 
DD_ORDERNUMBER_BULK_1_BULK_2, 
DD_INSPECTIONLOTNO_ANTIGEN_4, 
DIM_PARTID_ANTIGEN_4, 
DIM_BATCHID_ANTIGEN_4, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_4, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_4, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_4, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_4, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_4, 
DIM_DATEIDACTUALSTART_ANTIGEN_4, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_4, 
CT_TARGETMASTERRECIPE_ANTIGEN_4, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_4, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_4, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_4, 
CT_LOTSIZEDAYS_ANTIGEN_4, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_4, 
DD_INSPECTIONLOTNO_ANTIGEN_3, 
DIM_PARTID_ANTIGEN_3, 
DIM_BATCHID_ANTIGEN_3, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_3, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_3, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_3, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_3, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_3, 
DIM_DATEIDACTUALSTART_ANTIGEN_3, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_3, 
CT_TARGETMASTERRECIPE_ANTIGEN_3, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_3, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_3, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_3, 
CT_LOTSIZEDAYS_ANTIGEN_3, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_3, 
DD_ORDERNUMBER_RAW_1_BULK_3, 
DD_ORDERNUMBER_RAW_1_BULK_1, 
DD_INSPECTIONLOTNO_FPU_4, 
DIM_PARTID_FPU_4, 
DIM_BATCHID_FPU_4, 
DD_DATEUSERSTATUSQCCO_FPU_4, 
DD_DATEUSERSTATUSSAMR_FPU_4, 
DIM_DATEIDINSPECTIONSTART_FPU_4, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_4, 
DIM_DATEIDACTUALRELEASE_FPU_4, 
DIM_DATEIDACTUALSTART_FPU_4, 
DIM_DATEIDACTUALHEADERFINISH_FPU_4, 
CT_TARGETMASTERRECIPE_FPU_4, 
DIM_DATEUSERSTATUSSAMR_FPU_4, 
DIM_DATEUSERSTATUSQCCO_FPU_4, 
CT_MINRELEASEDSTOCKDAYS_FPU_4, 
CT_LOTSIZEDAYS_FPU_4, 
CT_FLOATBEFOREPRODUCTION_FPU_4, 
DD_ORDERNUMBER_FPU_1_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_1_ANTIGEN_2, 
DD_ORDERNUMBER_ANTIGEN_1_ANTIGEN_3, 
DD_ORDERNUMBER_ANTIGEN_1_ANTIGEN_4, 
DD_ORDERNUMBER_ANTIGEN_1_ANTIGEN_5, 
DD_ORDERNUMBER_ANTIGEN_1_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_1_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_1_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_1_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_1_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_1_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_5, 
DD_ORDERNUMBER_ANTIGEN_2_ANTIGEN_3, 
DD_ORDERNUMBER_ANTIGEN_2_ANTIGEN_4, 
DD_ORDERNUMBER_ANTIGEN_2_ANTIGEN_5, 
DD_ORDERNUMBER_ANTIGEN_2_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_2_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_2_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_2_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_2_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_2_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_5, 
DD_ORDERNUMBER_ANTIGEN_3_ANTIGEN_4, 
DD_ORDERNUMBER_ANTIGEN_3_ANTIGEN_5, 
DD_ORDERNUMBER_ANTIGEN_3_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_3_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_3_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_3_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_3_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_3_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_5, 
DD_ORDERNUMBER_ANTIGEN_4_ANTIGEN_5, 
DD_ORDERNUMBER_ANTIGEN_4_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_4_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_4_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_4_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_4_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_4_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_5, 
DD_ORDERNUMBER_ANTIGEN_5_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_5_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_5_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_5_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_5_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_5_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_5, 
DD_ORDERNUMBER_BULK_1_FPP_2, 
DD_ORDERNUMBER_BULK_1_FPP_3, 
DD_ORDERNUMBER_BULK_1_FPU_2, 
DD_ORDERNUMBER_BULK_1_FPU_3, 
DD_ORDERNUMBER_BULK_1_FPU_4, 
DD_ORDERNUMBER_BULK_1_FPU_5, 
DD_ORDERNUMBER_BULK_2_FPP_2, 
DD_ORDERNUMBER_BULK_2_FPP_3, 
DD_ORDERNUMBER_BULK_2_FPU_2, 
DD_ORDERNUMBER_BULK_2_FPU_3, 
DD_ORDERNUMBER_BULK_2_FPU_4, 
DD_ORDERNUMBER_BULK_2_FPU_5, 
DD_ORDERNUMBER_BULK_3_FPP_2, 
DD_ORDERNUMBER_BULK_3_FPP_3, 
DD_ORDERNUMBER_BULK_3_FPU_2, 
DD_ORDERNUMBER_BULK_3_FPU_3, 
DD_ORDERNUMBER_BULK_3_FPU_4, 
DD_ORDERNUMBER_BULK_3_FPU_5, 
DD_ORDERNUMBER_FPP_1_FPP_2, 
DD_ORDERNUMBER_FPP_1_FPP_3, 
DD_ORDERNUMBER_FPP_2_FPP_3, 
DD_ORDERNUMBER_FPU_1_FPP_2, 
DD_ORDERNUMBER_FPU_1_FPP_3, 
DD_ORDERNUMBER_FPU_1_FPU_2, 
DD_ORDERNUMBER_FPU_1_FPU_3, 
DD_ORDERNUMBER_FPU_1_FPU_4, 
DD_ORDERNUMBER_FPU_1_FPU_5, 
DD_ORDERNUMBER_FPU_2_FPP_1, 
DD_ORDERNUMBER_FPU_2_FPP_2, 
DD_ORDERNUMBER_FPU_2_FPP_3, 
DD_ORDERNUMBER_FPU_2_FPU_3, 
DD_ORDERNUMBER_FPU_2_FPU_4, 
DD_ORDERNUMBER_FPU_2_FPU_5, 
DD_ORDERNUMBER_FPU_3_FPP_1, 
DD_ORDERNUMBER_FPU_3_FPP_2, 
DD_ORDERNUMBER_FPU_3_FPP_3, 
DD_ORDERNUMBER_FPU_3_FPU_4, 
DD_ORDERNUMBER_FPU_3_FPU_5, 
DD_ORDERNUMBER_FPU_4_FPP_1, 
DD_ORDERNUMBER_FPU_4_FPP_2, 
DD_ORDERNUMBER_FPU_4_FPP_3, 
DD_ORDERNUMBER_FPU_4_FPU_5, 
DD_ORDERNUMBER_FPU_5_FPP_1, 
DD_ORDERNUMBER_FPU_5_FPP_2, 
DD_ORDERNUMBER_FPU_5_FPP_3, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_1, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_2, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_3, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_4, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_5, 
DD_ORDERNUMBER_RAW_1_FPP_2, 
DD_ORDERNUMBER_RAW_1_FPP_3, 
DD_ORDERNUMBER_RAW_1_FPU_2, 
DD_ORDERNUMBER_RAW_1_FPU_3, 
DD_ORDERNUMBER_RAW_1_FPU_4, 
DD_ORDERNUMBER_RAW_1_FPU_5, 
DIM_BATCHID_FPP_FINAL, 
DIM_PARTID_FPP_FINAL,
DIM_PLANTID_FPP_FINAL,
DIM_PLANTID_RAW_1,
DIM_PLANTID_ANTIGEN_1,
DIM_PLANTID_ANTIGEN_2,
DIM_PLANTID_ANTIGEN_3,
DIM_PLANTID_ANTIGEN_4,
DIM_PLANTID_ANTIGEN_5,
DIM_PLANTID_BULK_1,
DIM_PLANTID_BULK_2,
DIM_PLANTID_BULK_3,
DIM_PLANTID_FPU_1,
DIM_PLANTID_FPU_2,
DIM_PLANTID_FPU_3,
DIM_PLANTID_FPU_4,
DIM_PLANTID_FPU_5,
DIM_PLANTID_FPP_1,
DIM_PLANTID_FPP_2,
DIM_PLANTID_FPP_3,
DD_INCLUDEE2ELT,
ct_targetE2E_LT_antigen,
ct_targetE2E_LT_bulk,
ct_targetE2E_LT_fpu,
ct_actualLTAntigen,
ct_actualLTBulk,
ct_actualLTFPU,
ct_actualLTFPP,
ct_actualLTRaw,
ct_targetLTAntigen,
ct_targetLTBulk,
ct_targetLTRaw,
ct_targetLTFPU,
ct_targetLTFPP,
ct_actualLTRaw1,
ct_targetLTRaw1,
ct_actualLTAntigen1,
ct_actualLTAntigen2,
ct_actualLTAntigen3,
ct_actualLTAntigen4,
ct_actualLTAntigen5,
ct_targetLTAntigen1,
ct_targetLTAntigen2,
ct_targetLTAntigen3,
ct_targetLTAntigen4,
ct_targetLTAntigen5,
ct_actualLTBulk1,
ct_actualLTBulk2,
ct_actualLTBulk3,
ct_targetLTBULK1,
ct_targetLTBULK2,
ct_targetLTBULK3,
ct_actualLTFpu1,
ct_actualLTFpu2,
ct_actualLTFpu3,
ct_actualLTFpu4,
ct_actualLTFpu5,
ct_targetLTFPU1,
ct_targetLTFPU2,
ct_targetLTFPU3,
ct_targetLTFPU4,
ct_targetLTFPU5,
ct_actualLTFpp1,
ct_actualLTFpp2,
ct_actualLTFpp3,
ct_targetLTfpp1,
ct_targetLTfpp2,
ct_targetLTfpp3,
gpf_fpp_final,
dd_plantcode_agg,	
dd_plantcode_comops_agg,
dd_partnumber_agg,
dd_ProductFamily_Merck_agg,
dd_monthyear_agg,
ct_actual_raw1_qc,
ct_actual_raw1_rl,
ct_actual_antigen1_qc,
ct_actual_antigen1_rl,
ct_actual_antigen1_prd,
ct_actual_antigen2_qc,
ct_actual_antigen2_rl,
ct_actual_antigen2_prd,
ct_actual_antigen3_qc,
ct_actual_antigen3_rl,
ct_actual_antigen3_prd,
ct_actual_antigen4_qc,
ct_actual_antigen4_rl,
ct_actual_antigen4_prd,
ct_actual_antigen5_qc,
ct_actual_antigen5_rl,
ct_actual_antigen5_prd,
ct_actual_bulk1_qc,
ct_actual_bulk1_rl,
ct_actual_bulk1_prd,
ct_actual_bulk2_qc,
ct_actual_bulk2_rl,
ct_actual_bulk2_prd,
ct_actual_bulk3_qc,
ct_actual_bulk3_rl,
ct_actual_bulk3_prd,
ct_actual_fpu1_qc,
ct_actual_fpu1_rl,
ct_actual_fpu1_prd,
ct_actual_fpu2_qc,
ct_actual_fpu2_rl,
ct_actual_fpu2_prd,
ct_actual_fpu3_qc,
ct_actual_fpu3_rl,
ct_actual_fpu3_prd,
ct_actual_fpu4_qc,
ct_actual_fpu4_rl,
ct_actual_fpu4_prd,
ct_actual_fpu5_qc,
ct_actual_fpu5_rl,
ct_actual_fpu5_prd,
ct_actual_fpp1_qc,
ct_actual_fpp1_prd,
ct_actual_fpp2_qc,
ct_actual_fpp2_prd,
ct_actual_fpp3_qc,
ct_actual_fpp3_prd,
ct_target_raw1_qc,
ct_target_raw1_rl,
ct_target_antigen1_qc,
ct_target_antigen1_rl,
ct_target_antigen1_prd,
ct_target_antigen2_qc,
ct_target_antigen2_rl,
ct_target_antigen2_prd,
ct_target_antigen3_qc,
ct_target_antigen3_rl,
ct_target_antigen3_prd,
ct_target_antigen4_qc,
ct_target_antigen4_rl,
ct_target_antigen4_prd,
ct_target_antigen5_qc,
ct_target_antigen5_rl,
ct_target_antigen5_prd,
ct_target_bulk1_qc,
ct_target_bulk1_rl,
ct_target_bulk1_prd,
ct_target_bulk2_qc,
ct_target_bulk2_rl,
ct_target_bulk2_prd,
ct_target_bulk3_qc,
ct_target_bulk3_rl,
ct_target_bulk3_prd,
ct_target_fpu1_qc,
ct_target_fpu1_rl,
ct_target_fpu1_prd,
ct_target_fpu2_qc,
ct_target_fpu2_rl,
ct_target_fpu2_prd,
ct_target_fpu3_qc,
ct_target_fpu3_rl,
ct_target_fpu3_prd,
ct_target_fpu4_qc,
ct_target_fpu4_rl,
ct_target_fpu4_prd,
ct_target_fpu5_qc,
ct_target_fpu5_rl,
ct_target_fpu5_prd,
ct_target_fpp1_qc,
ct_target_fpp1_prd,
ct_target_fpp2_qc,
ct_target_fpp2_prd,
ct_target_fpp3_qc,
ct_target_fpp3_prd,
ct_hitormisse2e_lt_antigen1,
ct_hitormisse2e_lt_antigen2,
ct_hitormisse2e_lt_antigen3,
ct_hitormisse2e_lt_antigen4,
ct_hitormisse2e_lt_antigen5,
ct_hitormisse2e_lt_bulk1,
ct_hitormisse2e_lt_bulk2,
ct_hitormisse2e_lt_bulk3,
ct_hitormisse2e_lt_fpp1,
ct_hitormisse2e_lt_fpp2,
ct_hitormisse2e_lt_fpp3,
ct_hitormisse2e_lt_fpu1,
ct_hitormisse2e_lt_fpu2,
ct_hitormisse2e_lt_fpu3,
ct_hitormisse2e_lt_fpu4,
ct_hitormisse2e_lt_fpu5,
ct_hitormisse2e_lt_raw1,
ct_hitormisse2e_lt_antigen1_prd,
ct_hitormisse2e_lt_antigen1_qc ,
ct_hitormisse2e_lt_antigen1_rl ,
ct_hitormisse2e_lt_antigen2_prd ,
ct_hitormisse2e_lt_antigen2_qc ,
ct_hitormisse2e_lt_antigen2_rl ,
ct_hitormisse2e_lt_antigen3_prd ,
ct_hitormisse2e_lt_antigen3_qc ,
ct_hitormisse2e_lt_antigen3_rl ,
ct_hitormisse2e_lt_antigen4_prd ,
ct_hitormisse2e_lt_antigen4_qc ,
ct_hitormisse2e_lt_antigen4_rl ,
ct_hitormisse2e_lt_antigen5_prd ,
ct_hitormisse2e_lt_antigen5_qc ,
ct_hitormisse2e_lt_antigen5_rl ,
ct_hitormisse2e_lt_bulk1_prd ,
ct_hitormisse2e_lt_bulk1_qc ,
ct_hitormisse2e_lt_bulk1_rl ,
ct_hitormisse2e_lt_bulk2_prd ,
ct_hitormisse2e_lt_bulk2_qc ,
ct_hitormisse2e_lt_bulk2_rl ,
ct_hitormisse2e_lt_bulk3_prd ,
ct_hitormisse2e_lt_bulk3_qc ,
ct_hitormisse2e_lt_bulk3_rl ,
ct_hitormisse2e_lt_fpp1_prd ,
ct_hitormisse2e_lt_fpp1_qc ,
ct_hitormisse2e_lt_fpp2_prd ,
ct_hitormisse2e_lt_fpp2_qc ,
ct_hitormisse2e_lt_fpp3_prd ,
ct_hitormisse2e_lt_fpp3_qc ,
ct_hitormisse2e_lt_fpu1_prd ,
ct_hitormisse2e_lt_fpu1_qc ,
ct_hitormisse2e_lt_fpu1_rl ,
ct_hitormisse2e_lt_fpu2_prd ,
ct_hitormisse2e_lt_fpu2_qc ,
ct_hitormisse2e_lt_fpu2_rl ,
ct_hitormisse2e_lt_fpu3_prd ,
ct_hitormisse2e_lt_fpu3_qc ,
ct_hitormisse2e_lt_fpu3_rl ,
ct_hitormisse2e_lt_fpu4_prd ,
ct_hitormisse2e_lt_fpu4_qc ,
ct_hitormisse2e_lt_fpu4_rl ,
ct_hitormisse2e_lt_fpu5_prd ,
ct_hitormisse2e_lt_fpu5_qc ,
ct_hitormisse2e_lt_fpu5_rl ,
ct_hitormisse2e_lt_raw1_qc ,
ct_hitormisse2e_lt_raw1_rl,
ct_hitormisse2e_lt_raw,
ct_hitormisse2e_lt_antigen,
ct_hitormisse2e_lt_bulk,
ct_hitormisse2e_lt_fpu,
ct_hitormisse2e_lt_fpp 
)
select 
FACT_MMPRODHIERARCHYID, 
DIM_PLANTID, 
DD_INSPECTIONLOTNOT_RAW, 
DIM_RAWPARTID, 
DIM_RAWBATCHID, 
DD_PODOCUMENTNO, 
DD_PODOCUMENTITEMNO, 
DD_SCHEDULENO, 
DIM_DATEIDSCHEDORDER, 
DIM_DATEIDCREATE, 
DIM_DATEIDRAWPOSTINGDATE, 
DD_RAWDATEUSERSTATUSQCCO, 
DIM_DATEIDRAWINSPECTIONSTART, 
DD_RAWDATEUSERSTATUSSAMR, 
DIM_DATEIDRAWUSAGEDECISIONMADE, 
DD_ORDERNUMBERRAWBULK, 
DIM_BULKPARTID, 
DIM_BULKBATCHIDPROD, 
DIM_DATEIDBULKACTUALRELEASE, 
DIM_DATEIDBULKACTUALSTART, 
DIM_DATEIDBULKACTUALHEADERFINISH, 
DD_INSPECTIONLOTNO_BULK, 
DD_DATEUSERSTATUSQCCO_BULK, 
DIM_DATEIDINSPECTIONSTART_BULK, 
DD_DATEUSERSTATUSSAMR_BULK, 
DIM_DATEIDUSAGEDECISIONMADE_BULK, 
DD_ORDERNUMBER_BULK_FPU, 
DIM_FPUPARTID, 
DIM_FPUBATCHIDPROD, 
DIM_DATEIDFPUACTUALRELEASE, 
DIM_DATEIDFPUACTUALSTART, 
DIM_DATEIDFPUACTUALHEADERFINISH, 
DD_INSPECTIONLOTNO_FPU, 
DD_DATEUSERSTATUSSAMR_FPU, 
DD_DATEUSERSTATUSQCCO_FPU, 
DIM_DATEIDFPUINSPECTIONSTART, 
DIM_DATEIDFPUUSAGEDECISIONMADE, 
DD_ORDERNUMBER_FPU_FPP, 
DIM_FPPPARTID, 
DIM_FPPBATCHIDPROD, 
DIM_DATEIDFPPACTUALRELEASE, 
DIM_DATEIDFPPACTUALSTART, 
DIM_DATEIDFPPACTUALHEADERFINISH, 
DD_INSPECTIONLOTNO_FPP, 
DD_DATEUSERSTATUSSAMR_FPP, 
DD_DATEUSERSTATUSQCCO_FPP, 
DIM_DATEIDFPPINSPECTIONSTART, 
DIM_DATEIDFPPUSAGEDECISIONMADE, 
DIM_PLANTID_COMOPS, 
DD_SALESDLVRDOCNO_COMOPS, 
DD_SALESDLVRITEMNO_COMOPS, 
DIM_DATEIDDLVRDOCCREATED_COMOPS, 
DIM_DATEIDACTUALGOODSISSUE_COMOPS, 
DIM_DATEIDACTUALRECEIPT_COMOPS, 
DD_INSPECTIONLOTNO_COMOPS, 
DD_DATEUSERSTATUSSAMR_COMOPS, 
DIM_DATEIDINSPECTIONSTART_COMOPS, 
DD_DATEUSERSTATUSQCCO_COMOPS, 
DIM_DATEIDUSAGEDECISIONMADE_COMOPS, 
DD_SALESDLVRDOCNO_CUSTOMER, 
DD_SALESDLVRITEMNO_CUSTOMER, 
DIM_DATEIDDLVRDOCCREATED_CUSTOMER, 
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER, 
CT_QTYDELIVERED, 
DD_SALESDOCNO_CUSTOMER, 
DD_SALESITEMNO_CUSTOMER, 
AMT_ORDERAMOUNTSALES, 
AMT_EXCHANGERATE, 
AMT_EXCHANGERATE_GBL, 
DIM_FPPPARTID_COMOPS, 
DIM_CURRENCYID, 
DIM_CUSTOMERSHIPCOMOPS, 
DIM_CUSTOMERSOLDCOMOPS, 
DIM_CUSTOMERSHIPCUST, 
DIM_CUSTOMERSOLDCUST, 
DD_SALESDOCNO_COMOPS, 
DD_SALESITEMNO_COMOPS, 
AMT_ORDERAMOUNTSALESFPP, 
DIM_CURRENCYID_TRA, 
CT_ORDERAMOUNTSALESFPP, 
CT_ORDERAMOUNTSALESFPP2, 
CT_ORDERAMOUNTSALESFPP_2, 
CT_TARGETMASTERRECIPEFPP, 
CT_TARGETMASTERRECIPERAW, 
CT_TARGETMASTERRECIPEFPU, 
CT_TARGETMASTERRECIPEBULK, 
CT_TARGETMASTERRECIPEFPP_COMOP, 
DIM_RAWDATEUSERSTATUSSAMR, 
DIM_RAWDATEUSERSTATUSQCCO, 
DIM_DATEUSERSTATUSSAMR_BULK, 
DIM_DATEUSERSTATUSQCCO_BULK, 
DIM_DATEUSERSTATUSSAMR_FPU, 
DIM_DATEUSERSTATUSQCCO_FPU, 
DIM_DATEUSERSTATUSSAMR_FPP, 
DIM_DATEUSERSTATUSQCCO_FPP, 
CT_MINRELEASEDSTOCKDAYSRAW, 
CT_MINRELEASEDSTOCKDAYSBULK, 
CT_MINRELEASEDSTOCKDAYSFPU, 
CT_MINRELEASEDSTOCKDAYSFPPSITE, 
CT_MINRELEASEDSTOCKDAYSFPPCOMOP, 
CT_LOTSIZEDAYSRAW, 
CT_LOTSIZEDAYSBULK, 
CT_LOTSIZEDAYSFPU, 
CT_LOTSIZEDAYSFPPSITE, 
CT_LOTSIZEDAYSFPPCOMOP, 
CT_FLOATBEFOREPRODUCTIONRAW, 
CT_FLOATBEFOREPRODUCTIONFPU, 
CT_FLOATBEFOREPRODUCTIONFPP, 
CT_FLOATBEFOREPRODUCTIONFPPCOMOP, 
CT_FLOATBEFOREPRODUCTIONBULK, 
CT_HITORMISSE2E_LT, 
CT_HITVALUE, 
CT_ACTUALE2E_LT, 
CT_TARGETE2E_LT, 
CT_OVERALLSUPPLYHEALTHSCORE, 
CT_WEIGHTEDACTUALLT, 
CT_WEIGHTEDTARGETLT, 
DIM_BULKPARTID_2, 
DIM_BULKBATCHIDPROD_2, 
DD_ORDERNUMBERBULK_BULK, 
DIM_DATEIDBULK_2ACTUALRELEASE, 
DIM_DATEIDBULK_2ACTUALSTART, 
DIM_DATEIDBULK_2ACTUALHEADERFINISH, 
DD_ORDERNUMBER_BULK_2_FPU, 
DD_ORDERNUMBER_BULK_2_FPP, 
CT_ACTUALLTSALES, 
CT_TARGETLTSALES, 
DIM_BULKPARTID_3, 
DIM_BULKBATCHIDPROD_3, 
DD_ORDERNUMBERBULK_2_BULK_3, 
DIM_DATEIDBULK_3ACTUALRELEASE, 
DIM_DATEIDBULK_3ACTUALSTART, 
DIM_DATEIDBULK_3ACTUALHEADERFINISH, 
DD_ORDERNUMBER_BULK_3_FPU, 
DD_ORDERNUMBER_BULK_3_FPP, 
DD_ORDERNUMBER_BULK_3_FPU_1, 
DD_ORDERNUMBER_BULK_3_FPP_1, 
DD_ORDERNUMBER_BULK_2_FPP_1, 
DD_ORDERNUMBER_BULK_2_FPU_1, 
DD_ORDERNUMBER_BULK_1_FPP_1, 
DD_ORDERNUMBER_BULK_1_FPU_1, 
DD_DOCUMENTNO, 
DD_DOCUMENTITEMNO, 
DIM_DATEIDPOSTINGDATE, 
DIM_PARTID_COMOPS, 
CT_TARGETMASTERRECIPE_COMOPS, 
CT_MINRELEASEDSTOCKDAYS_COMOPS, 
CT_LOTSIZEDAYS_COMOPS, 
CT_FLOATBEFOREPRODUCTION_COMOPS, 
DD_INSPECTIONLOTNO_BULK_2, 
DIM_PARTID_BULK_2, 
DIM_BATCHID_BULK_2, 
DD_DATEUSERSTATUSQCCO_BULK_2, 
DD_DATEUSERSTATUSSAMR_BULK_2, 
DIM_DATEIDINSPECTIONSTART_BULK_2, 
DIM_DATEIDUSAGEDECISIONMADE_BULK_2, 
DIM_DATEIDACTUALRELEASE_BULK_2, 
DIM_DATEIDACTUALSTART_BULK_2, 
DIM_DATEIDACTUALHEADERFINISH_BULK_2, 
CT_TARGETMASTERRECIPE_BULK_2, 
DIM_DATEUSERSTATUSSAMR_BULK_2, 
DIM_DATEUSERSTATUSQCCO_BULK_2, 
CT_MINRELEASEDSTOCKDAYS_BULK_2, 
CT_LOTSIZEDAYS_BULK_2, 
CT_FLOATBEFOREPRODUCTION_BULK_2, 
DD_INSPECTIONLOTNO_ANTIGEN_2, 
DIM_PARTID_ANTIGEN_2, 
DIM_BATCHID_ANTIGEN_2, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_2, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_2, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_2, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_2, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_2, 
DIM_DATEIDACTUALSTART_ANTIGEN_2, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_2, 
CT_TARGETMASTERRECIPE_ANTIGEN_2, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_2, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_2, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_2, 
CT_LOTSIZEDAYS_ANTIGEN_2, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_2, 
DD_ORDERNUMBER_BULK_2_BULK_3, 
DD_ORDERNUMBER_BULK_1_BULK_3, 
DD_INSPECTIONLOTNO_ANTIGEN_5, 
DIM_PARTID_ANTIGEN_5, 
DIM_BATCHID_ANTIGEN_5, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_5, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_5, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_5, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_5, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_5, 
DIM_DATEIDACTUALSTART_ANTIGEN_5, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_5, 
CT_TARGETMASTERRECIPE_ANTIGEN_5, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_5, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_5, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_5, 
CT_LOTSIZEDAYS_ANTIGEN_5, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_5, 
DD_ORDERNUMBER_RAW_1_BULK_2, 
DD_INSPECTIONLOTNO_FPP_3, 
DIM_PARTID_FPP_3, 
DIM_BATCHID_FPP_3, 
DD_DATEUSERSTATUSQCCO_FPP_3, 
DD_DATEUSERSTATUSSAMR_FPP_3, 
DIM_DATEIDINSPECTIONSTART_FPP_3, 
DIM_DATEIDUSAGEDECISIONMADE_FPP_3, 
DIM_DATEIDACTUALRELEASE_FPP_3, 
DIM_DATEIDACTUALSTART_FPP_3, 
DIM_DATEIDACTUALHEADERFINISH_FPP_3, 
CT_TARGETMASTERRECIPE_FPP_3, 
DIM_DATEUSERSTATUSSAMR_FPP_3, 
DIM_DATEUSERSTATUSQCCO_FPP_3, 
CT_MINRELEASEDSTOCKDAYS_FPP_3, 
CT_LOTSIZEDAYS_FPP_3, 
CT_FLOATBEFOREPRODUCTION_FPP_3, 
DD_INSPECTIONLOTNO_FPP_2, 
DIM_PARTID_FPP_2, 
DIM_BATCHID_FPP_2, 
DD_DATEUSERSTATUSQCCO_FPP_2, 
DD_DATEUSERSTATUSSAMR_FPP_2, 
DIM_DATEIDINSPECTIONSTART_FPP_2, 
DIM_DATEIDUSAGEDECISIONMADE_FPP_2, 
DIM_DATEIDACTUALRELEASE_FPP_2, 
DIM_DATEIDACTUALSTART_FPP_2, 
DIM_DATEIDACTUALHEADERFINISH_FPP_2, 
CT_TARGETMASTERRECIPE_FPP_2, 
DIM_DATEUSERSTATUSSAMR_FPP_2, 
DIM_DATEUSERSTATUSQCCO_FPP_2, 
CT_MINRELEASEDSTOCKDAYS_FPP_2, 
CT_LOTSIZEDAYS_FPP_2, 
CT_FLOATBEFOREPRODUCTION_FPP_2, 
DD_ORDERNUMBER_RAW_1_FPP_1, 
DD_INSPECTIONLOTNO_FPP_1, 
DIM_PARTID_FPP_1, 
DIM_BATCHID_FPP_1, 
DD_DATEUSERSTATUSQCCO_FPP_1, 
DD_DATEUSERSTATUSSAMR_FPP_1, 
DIM_DATEIDINSPECTIONSTART_FPP_1, 
DIM_DATEIDUSAGEDECISIONMADE_FPP_1, 
DIM_DATEIDACTUALRELEASE_FPP_1, 
DIM_DATEIDACTUALSTART_FPP_1, 
DIM_DATEIDACTUALHEADERFINISH_FPP_1, 
CT_TARGETMASTERRECIPE_FPP_1, 
DIM_DATEUSERSTATUSSAMR_FPP_1, 
DIM_DATEUSERSTATUSQCCO_FPP_1, 
CT_MINRELEASEDSTOCKDAYS_FPP_1, 
CT_LOTSIZEDAYS_FPP_1, 
CT_FLOATBEFOREPRODUCTION_FPP_1, 
DD_INSPECTIONLOTNO_FPU_2, 
DIM_PARTID_FPU_2, 
DIM_BATCHID_FPU_2, 
DD_DATEUSERSTATUSQCCO_FPU_2, 
DD_DATEUSERSTATUSSAMR_FPU_2, 
DIM_DATEIDINSPECTIONSTART_FPU_2, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_2, 
DIM_DATEIDACTUALRELEASE_FPU_2, 
DIM_DATEIDACTUALSTART_FPU_2, 
DIM_DATEIDACTUALHEADERFINISH_FPU_2, 
CT_TARGETMASTERRECIPE_FPU_2, 
DIM_DATEUSERSTATUSSAMR_FPU_2, 
DIM_DATEUSERSTATUSQCCO_FPU_2, 
CT_MINRELEASEDSTOCKDAYS_FPU_2, 
CT_LOTSIZEDAYS_FPU_2, 
CT_FLOATBEFOREPRODUCTION_FPU_2, 
DD_INSPECTIONLOTNO_FPU_3, 
DIM_PARTID_FPU_3, 
DIM_BATCHID_FPU_3, 
DD_DATEUSERSTATUSQCCO_FPU_3, 
DD_DATEUSERSTATUSSAMR_FPU_3, 
DIM_DATEIDINSPECTIONSTART_FPU_3, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_3, 
DIM_DATEIDACTUALRELEASE_FPU_3, 
DIM_DATEIDACTUALSTART_FPU_3, 
DIM_DATEIDACTUALHEADERFINISH_FPU_3, 
CT_TARGETMASTERRECIPE_FPU_3, 
DIM_DATEUSERSTATUSSAMR_FPU_3, 
DIM_DATEUSERSTATUSQCCO_FPU_3, 
CT_MINRELEASEDSTOCKDAYS_FPU_3, 
CT_LOTSIZEDAYS_FPU_3, 
CT_FLOATBEFOREPRODUCTION_FPU_3, 
DD_INSPECTIONLOTNO_FPU_5, 
DIM_PARTID_FPU_5, 
DIM_BATCHID_FPU_5, 
DD_DATEUSERSTATUSQCCO_FPU_5, 
DD_DATEUSERSTATUSSAMR_FPU_5, 
DIM_DATEIDINSPECTIONSTART_FPU_5, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_5, 
DIM_DATEIDACTUALRELEASE_FPU_5, 
DIM_DATEIDACTUALSTART_FPU_5, 
DIM_DATEIDACTUALHEADERFINISH_FPU_5, 
CT_TARGETMASTERRECIPE_FPU_5, 
DIM_DATEUSERSTATUSSAMR_FPU_5, 
DIM_DATEUSERSTATUSQCCO_FPU_5, 
CT_MINRELEASEDSTOCKDAYS_FPU_5, 
CT_LOTSIZEDAYS_FPU_5, 
CT_FLOATBEFOREPRODUCTION_FPU_5, 
DD_ORDERNUMBER_RAW_1_FPU_1, 
DD_INSPECTIONLOTNO_FPU_1, 
DIM_PARTID_FPU_1, 
DIM_BATCHID_FPU_1, 
DD_DATEUSERSTATUSQCCO_FPU_1, 
DD_DATEUSERSTATUSSAMR_FPU_1, 
DIM_DATEIDINSPECTIONSTART_FPU_1, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_1, 
DIM_DATEIDACTUALRELEASE_FPU_1, 
DIM_DATEIDACTUALSTART_FPU_1, 
DIM_DATEIDACTUALHEADERFINISH_FPU_1, 
CT_TARGETMASTERRECIPE_FPU_1, 
DIM_DATEUSERSTATUSSAMR_FPU_1, 
DIM_DATEUSERSTATUSQCCO_FPU_1, 
CT_MINRELEASEDSTOCKDAYS_FPU_1, 
CT_LOTSIZEDAYS_FPU_1, 
CT_FLOATBEFOREPRODUCTION_FPU_1, 
DD_INSPECTIONLOTNO_BULK_3, 
DIM_PARTID_BULK_3, 
DIM_BATCHID_BULK_3, 
DD_DATEUSERSTATUSQCCO_BULK_3, 
DD_DATEUSERSTATUSSAMR_BULK_3, 
DIM_DATEIDINSPECTIONSTART_BULK_3, 
DIM_DATEIDUSAGEDECISIONMADE_BULK_3, 
DIM_DATEIDACTUALRELEASE_BULK_3, 
DIM_DATEIDACTUALSTART_BULK_3, 
DIM_DATEIDACTUALHEADERFINISH_BULK_3, 
CT_TARGETMASTERRECIPE_BULK_3, 
DIM_DATEUSERSTATUSSAMR_BULK_3, 
DIM_DATEUSERSTATUSQCCO_BULK_3, 
CT_MINRELEASEDSTOCKDAYS_BULK_3, 
CT_LOTSIZEDAYS_BULK_3, 
CT_FLOATBEFOREPRODUCTION_BULK_3, 
DD_INSPECTIONLOTNO_BULK_1, 
DIM_PARTID_BULK_1, 
DIM_BATCHID_BULK_1, 
DD_DATEUSERSTATUSQCCO_BULK_1, 
DD_DATEUSERSTATUSSAMR_BULK_1, 
DIM_DATEIDINSPECTIONSTART_BULK_1, 
DIM_DATEIDUSAGEDECISIONMADE_BULK_1, 
DIM_DATEIDACTUALRELEASE_BULK_1, 
DIM_DATEIDACTUALSTART_BULK_1, 
DIM_DATEIDACTUALHEADERFINISH_BULK_1, 
CT_TARGETMASTERRECIPE_BULK_1, 
DIM_DATEUSERSTATUSSAMR_BULK_1, 
DIM_DATEUSERSTATUSQCCO_BULK_1, 
CT_MINRELEASEDSTOCKDAYS_BULK_1, 
CT_LOTSIZEDAYS_BULK_1, 
CT_FLOATBEFOREPRODUCTION_BULK_1, 
DD_INSPECTIONLOTNO_ANTIGEN_1, 
DIM_PARTID_ANTIGEN_1, 
DIM_BATCHID_ANTIGEN_1, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_1, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_1, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_1, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_1, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_1, 
DIM_DATEIDACTUALSTART_ANTIGEN_1, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_1, 
CT_TARGETMASTERRECIPE_ANTIGEN_1, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_1, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_1, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_1, 
CT_LOTSIZEDAYS_ANTIGEN_1, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_1, 
DD_INSPECTIONLOTNO_RAW_1, 
DIM_PARTID_RAW_1, 
DIM_BATCHID_RAW_1, 
DD_DATEUSERSTATUSQCCO_RAW_1, 
DD_DATEUSERSTATUSSAMR_RAW_1, 
DIM_DATEIDINSPECTIONSTART_RAW_1, 
DIM_DATEIDUSAGEDECISIONMADE_RAW_1, 
DIM_DATEIDACTUALRELEASE_RAW_1, 
DIM_DATEIDACTUALSTART_RAW_1, 
DIM_DATEIDACTUALHEADERFINISH_RAW_1, 
CT_TARGETMASTERRECIPE_RAW_1, 
DIM_DATEUSERSTATUSSAMR_RAW_1, 
DIM_DATEUSERSTATUSQCCO_RAW_1, 
CT_MINRELEASEDSTOCKDAYS_RAW_1, 
CT_LOTSIZEDAYS_RAW_1, 
CT_FLOATBEFOREPRODUCTION_RAW_1, 
DD_ORDERNUMBER_BULK_1_BULK_2, 
DD_INSPECTIONLOTNO_ANTIGEN_4, 
DIM_PARTID_ANTIGEN_4, 
DIM_BATCHID_ANTIGEN_4, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_4, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_4, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_4, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_4, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_4, 
DIM_DATEIDACTUALSTART_ANTIGEN_4, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_4, 
CT_TARGETMASTERRECIPE_ANTIGEN_4, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_4, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_4, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_4, 
CT_LOTSIZEDAYS_ANTIGEN_4, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_4, 
DD_INSPECTIONLOTNO_ANTIGEN_3, 
DIM_PARTID_ANTIGEN_3, 
DIM_BATCHID_ANTIGEN_3, 
DD_DATEUSERSTATUSQCCO_ANTIGEN_3, 
DD_DATEUSERSTATUSSAMR_ANTIGEN_3, 
DIM_DATEIDINSPECTIONSTART_ANTIGEN_3, 
DIM_DATEIDUSAGEDECISIONMADE_ANTIGEN_3, 
DIM_DATEIDACTUALRELEASE_ANTIGEN_3, 
DIM_DATEIDACTUALSTART_ANTIGEN_3, 
DIM_DATEIDACTUALHEADERFINISH_ANTIGEN_3, 
CT_TARGETMASTERRECIPE_ANTIGEN_3, 
DIM_DATEUSERSTATUSSAMR_ANTIGEN_3, 
DIM_DATEUSERSTATUSQCCO_ANTIGEN_3, 
CT_MINRELEASEDSTOCKDAYS_ANTIGEN_3, 
CT_LOTSIZEDAYS_ANTIGEN_3, 
CT_FLOATBEFOREPRODUCTION_ANTIGEN_3, 
DD_ORDERNUMBER_RAW_1_BULK_3, 
DD_ORDERNUMBER_RAW_1_BULK_1, 
DD_INSPECTIONLOTNO_FPU_4, 
DIM_PARTID_FPU_4, 
DIM_BATCHID_FPU_4, 
DD_DATEUSERSTATUSQCCO_FPU_4, 
DD_DATEUSERSTATUSSAMR_FPU_4, 
DIM_DATEIDINSPECTIONSTART_FPU_4, 
DIM_DATEIDUSAGEDECISIONMADE_FPU_4, 
DIM_DATEIDACTUALRELEASE_FPU_4, 
DIM_DATEIDACTUALSTART_FPU_4, 
DIM_DATEIDACTUALHEADERFINISH_FPU_4, 
CT_TARGETMASTERRECIPE_FPU_4, 
DIM_DATEUSERSTATUSSAMR_FPU_4, 
DIM_DATEUSERSTATUSQCCO_FPU_4, 
CT_MINRELEASEDSTOCKDAYS_FPU_4, 
CT_LOTSIZEDAYS_FPU_4, 
CT_FLOATBEFOREPRODUCTION_FPU_4, 
DD_ORDERNUMBER_FPU_1_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_1_ANTIGEN_2, 
DD_ORDERNUMBER_ANTIGEN_1_ANTIGEN_3, 
DD_ORDERNUMBER_ANTIGEN_1_ANTIGEN_4, 
DD_ORDERNUMBER_ANTIGEN_1_ANTIGEN_5, 
DD_ORDERNUMBER_ANTIGEN_1_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_1_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_1_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_1_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_1_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_1_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_1_FPU_5, 
DD_ORDERNUMBER_ANTIGEN_2_ANTIGEN_3, 
DD_ORDERNUMBER_ANTIGEN_2_ANTIGEN_4, 
DD_ORDERNUMBER_ANTIGEN_2_ANTIGEN_5, 
DD_ORDERNUMBER_ANTIGEN_2_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_2_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_2_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_2_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_2_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_2_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_2_FPU_5, 
DD_ORDERNUMBER_ANTIGEN_3_ANTIGEN_4, 
DD_ORDERNUMBER_ANTIGEN_3_ANTIGEN_5, 
DD_ORDERNUMBER_ANTIGEN_3_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_3_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_3_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_3_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_3_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_3_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_3_FPU_5, 
DD_ORDERNUMBER_ANTIGEN_4_ANTIGEN_5, 
DD_ORDERNUMBER_ANTIGEN_4_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_4_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_4_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_4_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_4_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_4_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_4_FPU_5, 
DD_ORDERNUMBER_ANTIGEN_5_BULK_1, 
DD_ORDERNUMBER_ANTIGEN_5_BULK_2, 
DD_ORDERNUMBER_ANTIGEN_5_BULK_3, 
DD_ORDERNUMBER_ANTIGEN_5_FPP_1, 
DD_ORDERNUMBER_ANTIGEN_5_FPP_2, 
DD_ORDERNUMBER_ANTIGEN_5_FPP_3, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_1, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_2, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_3, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_4, 
DD_ORDERNUMBER_ANTIGEN_5_FPU_5, 
DD_ORDERNUMBER_BULK_1_FPP_2, 
DD_ORDERNUMBER_BULK_1_FPP_3, 
DD_ORDERNUMBER_BULK_1_FPU_2, 
DD_ORDERNUMBER_BULK_1_FPU_3, 
DD_ORDERNUMBER_BULK_1_FPU_4, 
DD_ORDERNUMBER_BULK_1_FPU_5, 
DD_ORDERNUMBER_BULK_2_FPP_2, 
DD_ORDERNUMBER_BULK_2_FPP_3, 
DD_ORDERNUMBER_BULK_2_FPU_2, 
DD_ORDERNUMBER_BULK_2_FPU_3, 
DD_ORDERNUMBER_BULK_2_FPU_4, 
DD_ORDERNUMBER_BULK_2_FPU_5, 
DD_ORDERNUMBER_BULK_3_FPP_2, 
DD_ORDERNUMBER_BULK_3_FPP_3, 
DD_ORDERNUMBER_BULK_3_FPU_2, 
DD_ORDERNUMBER_BULK_3_FPU_3, 
DD_ORDERNUMBER_BULK_3_FPU_4, 
DD_ORDERNUMBER_BULK_3_FPU_5, 
DD_ORDERNUMBER_FPP_1_FPP_2, 
DD_ORDERNUMBER_FPP_1_FPP_3, 
DD_ORDERNUMBER_FPP_2_FPP_3, 
DD_ORDERNUMBER_FPU_1_FPP_2, 
DD_ORDERNUMBER_FPU_1_FPP_3, 
DD_ORDERNUMBER_FPU_1_FPU_2, 
DD_ORDERNUMBER_FPU_1_FPU_3, 
DD_ORDERNUMBER_FPU_1_FPU_4, 
DD_ORDERNUMBER_FPU_1_FPU_5, 
DD_ORDERNUMBER_FPU_2_FPP_1, 
DD_ORDERNUMBER_FPU_2_FPP_2, 
DD_ORDERNUMBER_FPU_2_FPP_3, 
DD_ORDERNUMBER_FPU_2_FPU_3, 
DD_ORDERNUMBER_FPU_2_FPU_4, 
DD_ORDERNUMBER_FPU_2_FPU_5, 
DD_ORDERNUMBER_FPU_3_FPP_1, 
DD_ORDERNUMBER_FPU_3_FPP_2, 
DD_ORDERNUMBER_FPU_3_FPP_3, 
DD_ORDERNUMBER_FPU_3_FPU_4, 
DD_ORDERNUMBER_FPU_3_FPU_5, 
DD_ORDERNUMBER_FPU_4_FPP_1, 
DD_ORDERNUMBER_FPU_4_FPP_2, 
DD_ORDERNUMBER_FPU_4_FPP_3, 
DD_ORDERNUMBER_FPU_4_FPU_5, 
DD_ORDERNUMBER_FPU_5_FPP_1, 
DD_ORDERNUMBER_FPU_5_FPP_2, 
DD_ORDERNUMBER_FPU_5_FPP_3, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_1, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_2, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_3, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_4, 
DD_ORDERNUMBER_RAW_1_ANTIGEN_5, 
DD_ORDERNUMBER_RAW_1_FPP_2, 
DD_ORDERNUMBER_RAW_1_FPP_3, 
DD_ORDERNUMBER_RAW_1_FPU_2, 
DD_ORDERNUMBER_RAW_1_FPU_3, 
DD_ORDERNUMBER_RAW_1_FPU_4, 
DD_ORDERNUMBER_RAW_1_FPU_5, 
DIM_BATCHID_FPP_FINAL, 
DIM_PARTID_FPP_FINAL,
DIM_PLANTID_FPP_FINAL,
DIM_PLANTID_RAW_1,
DIM_PLANTID_ANTIGEN_1,
DIM_PLANTID_ANTIGEN_2,
DIM_PLANTID_ANTIGEN_3,
DIM_PLANTID_ANTIGEN_4,
DIM_PLANTID_ANTIGEN_5,
DIM_PLANTID_BULK_1,
DIM_PLANTID_BULK_2,
DIM_PLANTID_BULK_3,
DIM_PLANTID_FPU_1,
DIM_PLANTID_FPU_2,
DIM_PLANTID_FPU_3,
DIM_PLANTID_FPU_4,
DIM_PLANTID_FPU_5,
DIM_PLANTID_FPP_1,
DIM_PLANTID_FPP_2,
DIM_PLANTID_FPP_3,
DD_INCLUDEE2ELT,
ct_targetE2E_LT_antigen,
ct_targetE2E_LT_bulk,
ct_targetE2E_LT_fpu,
ct_actualLTAntigen,
ct_actualLTBulk,
ct_actualLTFPU,
ct_actualLTFPP,
ct_actualLTRaw,
ct_targetLTAntigen,
ct_targetLTBulk,
ct_targetLTRaw,
ct_targetLTFPU,
ct_targetLTFPP,
ct_actualLTRaw1,
ct_targetLTRaw1,
ct_actualLTAntigen1,
ct_actualLTAntigen2,
ct_actualLTAntigen3,
ct_actualLTAntigen4,
ct_actualLTAntigen5,
ct_targetLTAntigen1,
ct_targetLTAntigen2,
ct_targetLTAntigen3,
ct_targetLTAntigen4,
ct_targetLTAntigen5,
ct_actualLTBulk1,
ct_actualLTBulk2,
ct_actualLTBulk3,
ct_targetLTBULK1,
ct_targetLTBULK2,
ct_targetLTBULK3,
ct_actualLTFpu1,
ct_actualLTFpu2,
ct_actualLTFpu3,
ct_actualLTFpu4,
ct_actualLTFpu5,
ct_targetLTFPU1,
ct_targetLTFPU2,
ct_targetLTFPU3,
ct_targetLTFPU4,
ct_targetLTFPU5,
ct_actualLTFpp1,
ct_actualLTFpp2,
ct_actualLTFpp3,
ct_targetLTfpp1,
ct_targetLTfpp2,
ct_targetLTfpp3,
gpf_fpp_final,
dd_plantcode_agg,	
dd_plantcode_comops_agg,
dd_partnumber_agg,
dd_ProductFamily_Merck_agg,
dd_monthyear_agg,
ct_actual_raw1_qc,
ct_actual_raw1_rl,
ct_actual_antigen1_qc,
ct_actual_antigen1_rl,
ct_actual_antigen1_prd,
ct_actual_antigen2_qc,
ct_actual_antigen2_rl,
ct_actual_antigen2_prd,
ct_actual_antigen3_qc,
ct_actual_antigen3_rl,
ct_actual_antigen3_prd,
ct_actual_antigen4_qc,
ct_actual_antigen4_rl,
ct_actual_antigen4_prd,
ct_actual_antigen5_qc,
ct_actual_antigen5_rl,
ct_actual_antigen5_prd,
ct_actual_bulk1_qc,
ct_actual_bulk1_rl,
ct_actual_bulk1_prd,
ct_actual_bulk2_qc,
ct_actual_bulk2_rl,
ct_actual_bulk2_prd,
ct_actual_bulk3_qc,
ct_actual_bulk3_rl,
ct_actual_bulk3_prd,
ct_actual_fpu1_qc,
ct_actual_fpu1_rl,
ct_actual_fpu1_prd,
ct_actual_fpu2_qc,
ct_actual_fpu2_rl,
ct_actual_fpu2_prd,
ct_actual_fpu3_qc,
ct_actual_fpu3_rl,
ct_actual_fpu3_prd,
ct_actual_fpu4_qc,
ct_actual_fpu4_rl,
ct_actual_fpu4_prd,
ct_actual_fpu5_qc,
ct_actual_fpu5_rl,
ct_actual_fpu5_prd,
ct_actual_fpp1_qc,
ct_actual_fpp1_prd,
ct_actual_fpp2_qc,
ct_actual_fpp2_prd,
ct_actual_fpp3_qc,
ct_actual_fpp3_prd,
ct_target_raw1_qc,
ct_target_raw1_rl,
ct_target_antigen1_qc,
ct_target_antigen1_rl,
ct_target_antigen1_prd,
ct_target_antigen2_qc,
ct_target_antigen2_rl,
ct_target_antigen2_prd,
ct_target_antigen3_qc,
ct_target_antigen3_rl,
ct_target_antigen3_prd,
ct_target_antigen4_qc,
ct_target_antigen4_rl,
ct_target_antigen4_prd,
ct_target_antigen5_qc,
ct_target_antigen5_rl,
ct_target_antigen5_prd,
ct_target_bulk1_qc,
ct_target_bulk1_rl,
ct_target_bulk1_prd,
ct_target_bulk2_qc,
ct_target_bulk2_rl,
ct_target_bulk2_prd,
ct_target_bulk3_qc,
ct_target_bulk3_rl,
ct_target_bulk3_prd,
ct_target_fpu1_qc,
ct_target_fpu1_rl,
ct_target_fpu1_prd,
ct_target_fpu2_qc,
ct_target_fpu2_rl,
ct_target_fpu2_prd,
ct_target_fpu3_qc,
ct_target_fpu3_rl,
ct_target_fpu3_prd,
ct_target_fpu4_qc,
ct_target_fpu4_rl,
ct_target_fpu4_prd,
ct_target_fpu5_qc,
ct_target_fpu5_rl,
ct_target_fpu5_prd,
ct_target_fpp1_qc,
ct_target_fpp1_prd,
ct_target_fpp2_qc,
ct_target_fpp2_prd,
ct_target_fpp3_qc,
ct_target_fpp3_prd,
ct_hitormisse2e_lt_antigen1,
ct_hitormisse2e_lt_antigen2,
ct_hitormisse2e_lt_antigen3,
ct_hitormisse2e_lt_antigen4,
ct_hitormisse2e_lt_antigen5,
ct_hitormisse2e_lt_bulk1,
ct_hitormisse2e_lt_bulk2,
ct_hitormisse2e_lt_bulk3,
ct_hitormisse2e_lt_fpp1,
ct_hitormisse2e_lt_fpp2,
ct_hitormisse2e_lt_fpp3,
ct_hitormisse2e_lt_fpu1,
ct_hitormisse2e_lt_fpu2,
ct_hitormisse2e_lt_fpu3,
ct_hitormisse2e_lt_fpu4,
ct_hitormisse2e_lt_fpu5,
ct_hitormisse2e_lt_raw1,
ct_hitormisse2e_lt_antigen1_prd,
ct_hitormisse2e_lt_antigen1_qc ,
ct_hitormisse2e_lt_antigen1_rl ,
ct_hitormisse2e_lt_antigen2_prd ,
ct_hitormisse2e_lt_antigen2_qc ,
ct_hitormisse2e_lt_antigen2_rl ,
ct_hitormisse2e_lt_antigen3_prd ,
ct_hitormisse2e_lt_antigen3_qc ,
ct_hitormisse2e_lt_antigen3_rl ,
ct_hitormisse2e_lt_antigen4_prd ,
ct_hitormisse2e_lt_antigen4_qc ,
ct_hitormisse2e_lt_antigen4_rl ,
ct_hitormisse2e_lt_antigen5_prd ,
ct_hitormisse2e_lt_antigen5_qc ,
ct_hitormisse2e_lt_antigen5_rl ,
ct_hitormisse2e_lt_bulk1_prd ,
ct_hitormisse2e_lt_bulk1_qc ,
ct_hitormisse2e_lt_bulk1_rl ,
ct_hitormisse2e_lt_bulk2_prd ,
ct_hitormisse2e_lt_bulk2_qc ,
ct_hitormisse2e_lt_bulk2_rl ,
ct_hitormisse2e_lt_bulk3_prd ,
ct_hitormisse2e_lt_bulk3_qc ,
ct_hitormisse2e_lt_bulk3_rl ,
ct_hitormisse2e_lt_fpp1_prd ,
ct_hitormisse2e_lt_fpp1_qc ,
ct_hitormisse2e_lt_fpp2_prd ,
ct_hitormisse2e_lt_fpp2_qc ,
ct_hitormisse2e_lt_fpp3_prd ,
ct_hitormisse2e_lt_fpp3_qc ,
ct_hitormisse2e_lt_fpu1_prd ,
ct_hitormisse2e_lt_fpu1_qc ,
ct_hitormisse2e_lt_fpu1_rl ,
ct_hitormisse2e_lt_fpu2_prd ,
ct_hitormisse2e_lt_fpu2_qc ,
ct_hitormisse2e_lt_fpu2_rl ,
ct_hitormisse2e_lt_fpu3_prd ,
ct_hitormisse2e_lt_fpu3_qc ,
ct_hitormisse2e_lt_fpu3_rl ,
ct_hitormisse2e_lt_fpu4_prd ,
ct_hitormisse2e_lt_fpu4_qc ,
ct_hitormisse2e_lt_fpu4_rl ,
ct_hitormisse2e_lt_fpu5_prd ,
ct_hitormisse2e_lt_fpu5_qc ,
ct_hitormisse2e_lt_fpu5_rl ,
ct_hitormisse2e_lt_raw1_qc ,
ct_hitormisse2e_lt_raw1_rl,
ct_hitormisse2e_lt_raw,
ct_hitormisse2e_lt_antigen,
ct_hitormisse2e_lt_bulk,
ct_hitormisse2e_lt_fpu,
ct_hitormisse2e_lt_fpp 
from fact_scm;

/*Andrei R - APP-9751 */
drop table if exists tmp_resultsrecording_plwcenter;
create table tmp_resultsrecording_plwcenter as select distinct pl.planttitle_merck as plant , f.dim_batchid,
 f.dd_inspectionlotno, 0 as rowno
from fact_resultsrecording f inner join dim_workcenter w on f.dim_workcenterid = w.dim_workcenterid 
inner join dim_plant pl on pl.PlantCode = w.plant;

merge into tmp_resultsrecording_plwcenter f
using ( select distinct plant , dim_batchid, dd_inspectionlotno, 
		ROW_NUMBER() OVER (PARTITION BY  dim_batchid, dd_inspectionlotno
 ORDER BY  dim_batchid, dd_inspectionlotno) as rowno
from tmp_resultsrecording_plwcenter ) t
on t.plant = f.plant
and t.dim_batchid = f.dim_batchid
and t.dd_inspectionlotno = f.dd_inspectionlotno
when matched then update set f.rowno = t.rowno;

drop table if exists tmp_resultsrecording_plwcenter2;
create table tmp_resultsrecording_plwcenter2 as select distinct dim_batchid, dd_inspectionlotno,
 GROUP_CONCAT(plant ORDER BY dd_inspectionlotno separator ' - ') as plantconcat from tmp_resultsrecording_plwcenter
group by 1, 2; 

update fact_resultsrecording f
set f.dd_plantworkcenter = ifnull(t.plantconcat,'Not Set')
from fact_resultsrecording f, tmp_resultsrecording_plwcenter2 t 
where   f.dim_batchid = t.dim_batchid
and f.dd_inspectionlotno = t.dd_inspectionlotno
and f.dd_plantworkcenter <> ifnull(t.plantconcat,'Not Set'); 

merge into fact_mmprodhierarchy f
using ( select distinct DD_INSPECTIONLOTNO_ANTIGEN_1,
                        dim_batchid_antigen_1,
                       w.dd_plantworkcenter
from fact_mmprodhierarchy f inner join  fact_resultsrecording w on f.dim_batchid_antigen_1 = w.dim_batchid
and DD_INSPECTIONLOTNO_ANTIGEN_1 = cast (w.dd_inspectionlotno as varchar(12))
) t
on f.DD_INSPECTIONLOTNO_ANTIGEN_1 = t.DD_INSPECTIONLOTNO_ANTIGEN_1
and f.dim_batchid_antigen_1 = t.dim_batchid_antigen_1
when matched then update set f.dd_workcenterplant_antigen_1 = ifnull(t.dd_plantworkcenter,'Not Set')
where f.dd_workcenterplant_antigen_1 <> ifnull(t.dd_plantworkcenter,'Not Set');

merge into fact_mmprodhierarchy f
using ( select distinct DD_INSPECTIONLOTNO_FPU_1,
                        dim_batchid_fpu_1,
                       w.dd_plantworkcenter
from fact_mmprodhierarchy f inner join  fact_resultsrecording w on f.dim_batchid_fpu_1 = w.dim_batchid
and DD_INSPECTIONLOTNO_FPU_1 = cast (w.dd_inspectionlotno as varchar(12))
) t
on f.DD_INSPECTIONLOTNO_FPU_1 = t.DD_INSPECTIONLOTNO_FPU_1
and f.dim_batchid_fpu_1 = t.dim_batchid_fpu_1
when matched then update set f.dd_workcenterplant_fpu_1 = ifnull(t.dd_plantworkcenter,'Not Set')
where f.dd_workcenterplant_fpu_1 <> ifnull(t.dd_plantworkcenter,'Not Set');

