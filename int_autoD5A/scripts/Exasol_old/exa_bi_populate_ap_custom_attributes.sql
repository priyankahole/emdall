/* Yogini App-8849 13 Aug 2018 */
/* Level 1,2,3, LOB and Geographic - all added in dimension GSM Taxonomy Level */



/* Yogini APP-8849 15 June 2018 - Level 1,2,3 logic changes */
/* created tmp tables and inserted all values and there respective description for each level seperately */

/*APP-9411 Alin 6 June 2018 custom attributes implemented here due to length error in UI*/


/*GSM TAXONOMY LEVEL 1*/
/*
merge into fact_accountspayable f
using(
select distinct fact_accountspayableid, 
	 CASE WHEN MaterialGroupCode='10000000' THEN  'Live Plant and Anima'
ELSE CASE WHEN MaterialGroupCode='10100000' THEN  ' LIVE ANIMALS'
ELSE CASE WHEN MaterialGroupCode='10111301' THEN  'Pet toys'
ELSE CASE WHEN MaterialGroupCode='10120000' THEN  ' ANIMAL FEED'
ELSE CASE WHEN MaterialGroupCode='10130000' THEN  ' ANIMAL CONTAINMENT AND HABITATS'
ELSE CASE WHEN MaterialGroupCode='10131601' THEN  'Cages or its accesso'
ELSE CASE WHEN MaterialGroupCode='10160000' THEN  ' FLORICULTURE AND SILVICULTURE PRODUCTS'
ELSE CASE WHEN MaterialGroupCode='12000000' THEN  'Chemicals including Bio Chemicals and Gas Materials'
ELSE CASE WHEN MaterialGroupCode='12140000' THEN  ' ELEMENTS AND GASES'
ELSE CASE WHEN MaterialGroupCode='12142100' THEN  ' INDUSTRIAL USE GASES'
ELSE CASE WHEN MaterialGroupCode='12352106' THEN  'Organic acids or its'
ELSE CASE WHEN MaterialGroupCode='13000000' THEN  'Resin and Rosin and'
ELSE CASE WHEN MaterialGroupCode='13111000' THEN  ' RESINS'
ELSE CASE WHEN MaterialGroupCode='14000000' THEN  'Paper Materials and'
ELSE CASE WHEN MaterialGroupCode='14111500' THEN  ' PRINTING AND WRITING PAPER'
ELSE CASE WHEN MaterialGroupCode='14111604' THEN  'Business cards'
ELSE CASE WHEN MaterialGroupCode='15100000' THEN  'Fuels'
ELSE CASE WHEN MaterialGroupCode='15101500' THEN  ' PETROLEUM AND DISTILLATES'
ELSE CASE WHEN MaterialGroupCode='15101506' THEN  'Gasoline or Petrol '
ELSE CASE WHEN MaterialGroupCode='15101600' THEN  'Solid and gel fuels'
ELSE CASE WHEN MaterialGroupCode='15101700' THEN  'Fuel Oils'
ELSE CASE WHEN MaterialGroupCode='15111500' THEN  'Gaseous fuels'
ELSE CASE WHEN MaterialGroupCode='15120000' THEN  ' LUBRICANTS AND OILS AND GREASES AND ANTI CORROSIVES'
ELSE CASE WHEN MaterialGroupCode='15121500' THEN  ' LUBRICATING PREPARATIONS'
ELSE CASE WHEN MaterialGroupCode='15121520' THEN  'General purpose lubr'
ELSE CASE WHEN MaterialGroupCode='20000000' THEN  'Mining and Well Dril'
ELSE CASE WHEN MaterialGroupCode='20121904' THEN  'Flow measurement equipment'
ELSE CASE WHEN MaterialGroupCode='21101906' THEN  'Egg inspection or collecting equipment'
ELSE CASE WHEN MaterialGroupCode='23000000' THEN  'Industrial Manufactu'
ELSE CASE WHEN MaterialGroupCode='23150000' THEN  ' INDUSTRIAL PROCESS MACHINERY AND EQUIPMENT AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='23151800' THEN  ' PHARMACEUTICAL INDUSTRY MACHINERY AND EQUIPMENT AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='23151804' THEN  'Reactors or fermente'
ELSE CASE WHEN MaterialGroupCode='23151807' THEN  'Freezedryers or lyop'
ELSE CASE WHEN MaterialGroupCode='23151810' THEN  'Sterile or aseptic p'
ELSE CASE WHEN MaterialGroupCode='23151814' THEN  'Vaccine production e'
ELSE CASE WHEN MaterialGroupCode='23151819' THEN  'Filter integrity tes'
ELSE CASE WHEN MaterialGroupCode='23152100' THEN  ' SEPARATION MACHINERY AND EQUIPMENT'
ELSE CASE WHEN MaterialGroupCode='23152104' THEN  'Centrifugal separati'
ELSE CASE WHEN MaterialGroupCode='23152900' THEN  ' PACKAGING MACHINERY'
ELSE CASE WHEN MaterialGroupCode='23152907' THEN  'Packaging machinery'
ELSE CASE WHEN MaterialGroupCode='23153100' THEN  ' INDUSTRIAL MACHINERY COMPONENTS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='23181803' THEN  'Ice making machines'
ELSE CASE WHEN MaterialGroupCode='23270000' THEN  ' WELDING, SOLDERING, BRAZING MACHINERY, ACCES. AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='23271400' THEN  ' WELDING MACHINERY'
ELSE CASE WHEN MaterialGroupCode='24000000' THEN  'Material Handling an'
ELSE CASE WHEN MaterialGroupCode='24100000' THEN  ' MATERIAL HANDLING MACHINERY AND EQUIPMENT'
ELSE CASE WHEN MaterialGroupCode='24101601' THEN  'Elevators'
ELSE CASE WHEN MaterialGroupCode='24102004' THEN  'Storage racks or she'
ELSE CASE WHEN MaterialGroupCode='24102107' THEN  'Cartoning machinery'
ELSE CASE WHEN MaterialGroupCode='24110000' THEN  ' CONTAINERS AND STORAGE'
ELSE CASE WHEN MaterialGroupCode='24111500' THEN  'Bags '
ELSE CASE WHEN MaterialGroupCode='24111800' THEN  ' TANKS AND CYLINDERS AND THEIR ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='24111802' THEN  'Air or gas tanks or'
ELSE CASE WHEN MaterialGroupCode='24112100' THEN  ' CASKS AND BARRELS AND DRUMS'
ELSE CASE WHEN MaterialGroupCode='24112500' THEN  'Corrugated and other supplies for distribution'
ELSE CASE WHEN MaterialGroupCode='24112505' THEN  'Pallets'
ELSE CASE WHEN MaterialGroupCode='24120000' THEN  ' PACKAGING MATERIALS'
ELSE CASE WHEN MaterialGroupCode='24140000' THEN  ' PACKING SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='24141704' THEN  'Printed inserts or instructions'
ELSE CASE WHEN MaterialGroupCode='25000000' THEN  'Commercial and Milit'
ELSE CASE WHEN MaterialGroupCode='25101503' THEN  'Fleet Repair and manteinance'
ELSE CASE WHEN MaterialGroupCode='25101506' THEN  'Limousines'
ELSE CASE WHEN MaterialGroupCode='26000000' THEN  'Power Generation and'
ELSE CASE WHEN MaterialGroupCode='26101100' THEN  ' ELECTRIC ALTERNATING CURRENT AC MOTORS'
ELSE CASE WHEN MaterialGroupCode='26101200' THEN  ' ELECTRIC DIRECT CURRENT DC MOTORS'
ELSE CASE WHEN MaterialGroupCode='26110000' THEN  ' BATTERIES AND GENERATORS AND KINETIC POWER TRANSMISSION'
ELSE CASE WHEN MaterialGroupCode='26111700' THEN  ' BATTERIES AND CELLS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='26121600' THEN  ' ELECTRICAL CABLE AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='26121616' THEN  'Telecommunications c'
ELSE CASE WHEN MaterialGroupCode='27000000' THEN  'Tools and General Ma'
ELSE CASE WHEN MaterialGroupCode='27110000' THEN  ' HAND TOOLS'
ELSE CASE WHEN MaterialGroupCode='27112700' THEN  'Power tools '
ELSE CASE WHEN MaterialGroupCode='30000000' THEN  'Structures and Build'
ELSE CASE WHEN MaterialGroupCode='30100000' THEN  ' STRUCTURAL MATERIALS AND BASIC SHAPES'
ELSE CASE WHEN MaterialGroupCode='30110000' THEN  'Concrete and cement'
ELSE CASE WHEN MaterialGroupCode='30140000' THEN  ' INSULATION'
ELSE CASE WHEN MaterialGroupCode='30161503' THEN  'Drywall'
ELSE CASE WHEN MaterialGroupCode='30180000' THEN  ' PLUMBING FIXTURES'
ELSE CASE WHEN MaterialGroupCode='30181500' THEN  ' SANITARY WARE'
ELSE CASE WHEN MaterialGroupCode='30222309' THEN  'Research or Testing'
ELSE CASE WHEN MaterialGroupCode='31000000' THEN  'Manufacturing Compon'
ELSE CASE WHEN MaterialGroupCode='31160000' THEN  ' HARDWARE'
ELSE CASE WHEN MaterialGroupCode='31162702' THEN  'Wheels'
ELSE CASE WHEN MaterialGroupCode='31180000' THEN  ' GASKETS AND SEALS'
ELSE CASE WHEN MaterialGroupCode='31181506' THEN  'O ring gaskets '
ELSE CASE WHEN MaterialGroupCode='31201500' THEN  ' TAPE'
ELSE CASE WHEN MaterialGroupCode='31201600' THEN  ' OTHER ADHESIVES AND SEALANTS'
ELSE CASE WHEN MaterialGroupCode='31210000' THEN  ' PAINTS AND PRIMERS AND FINISHES'
ELSE CASE WHEN MaterialGroupCode='31231300' THEN  'Tube and tubing '
ELSE CASE WHEN MaterialGroupCode='32000000' THEN  'Electronic Component'
ELSE CASE WHEN MaterialGroupCode='32101522' THEN  'Isolators'
ELSE CASE WHEN MaterialGroupCode='32150000' THEN  ' AUTOMATION CONTROL DEVICES AND COMPONENTS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='39000000' THEN  'Electrical Systems a'
ELSE CASE WHEN MaterialGroupCode='39101600' THEN  ' LAMPS AND LIGHTBULBS'
ELSE CASE WHEN MaterialGroupCode='39120000' THEN  ' ELECTRICAL EQUIPMENT AND COMPONENTS AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='39121000' THEN  ' POWER CONDITIONING EQUIPMENT'
ELSE CASE WHEN MaterialGroupCode='39121300' THEN  ' ELECTRICAL BOXES AND ENCLOSURES AND FITTINGS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='39121400' THEN  ' ELECTRICAL LUGS PLUGS AND CONNECTORS'
ELSE CASE WHEN MaterialGroupCode='39121500' THEN  ' SWITCHES AND CONTROLS AND RELAYS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='39121600' THEN  ' CIRCUIT PROTECTION DEVICES AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='39121700' THEN  ' ELECTRICAL HARDWARE AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='39121900' THEN  ' ELECTRICAL SAFETY DEVICES AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='40000000' THEN  'Distribution and Con'
ELSE CASE WHEN MaterialGroupCode='40100000' THEN  ' HEATING AND VENTILATION AND AIR CIRCULATION'
ELSE CASE WHEN MaterialGroupCode='40101600' THEN  ' AIR CIRCULATION AND PARTS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='40101800' THEN  ' HEATING EQUIPMENT AND PARTS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='40141600' THEN  ' VALVES'
ELSE CASE WHEN MaterialGroupCode='40141604' THEN  'Safety valves'
ELSE CASE WHEN MaterialGroupCode='40141609' THEN  'Control valves'
ELSE CASE WHEN MaterialGroupCode='40142300' THEN  'Pipe fittings '
ELSE CASE WHEN MaterialGroupCode='40142500' THEN  'Traps and strainers '
ELSE CASE WHEN MaterialGroupCode='40142600' THEN  'Tube fittings '
ELSE CASE WHEN MaterialGroupCode='40150000' THEN  ' INDUSTRIAL PUMPS AND COMPRESSORS'
ELSE CASE WHEN MaterialGroupCode='40151500' THEN  ' PUMPS'
ELSE CASE WHEN MaterialGroupCode='40151601' THEN  'Air compressors '
ELSE CASE WHEN MaterialGroupCode='40151700' THEN  ' PUMP PARTS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='40160000' THEN  ' INDUSTRIAL FILTERING AND PURIFICATION'
ELSE CASE WHEN MaterialGroupCode='40161505' THEN  'Air Filters'
ELSE CASE WHEN MaterialGroupCode='41000000' THEN  'Laboratory and Measuring and Observing and Testing Equipment '
ELSE CASE WHEN MaterialGroupCode='41100000' THEN  'Laboratory and scientific equipment  '
ELSE CASE WHEN MaterialGroupCode='41101500' THEN  'Laboratory blending and dispersing and homogenizing equipment and supplies'
ELSE CASE WHEN MaterialGroupCode='41101504' THEN  'Homogenizers'
ELSE CASE WHEN MaterialGroupCode='41101518' THEN  'Laboratory blenders or emulsifiers '
ELSE CASE WHEN MaterialGroupCode='41102400' THEN  'Laboratory heating and drying equipment '
ELSE CASE WHEN MaterialGroupCode='41102600' THEN  ' ANIMAL LABORATORY EQUIPMENT AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='41103000' THEN  'Laboratory cooling equipment '
ELSE CASE WHEN MaterialGroupCode='41103021' THEN  'Laboratory plate freezers '
ELSE CASE WHEN MaterialGroupCode='41103022' THEN  'Cool transport or storage'
ELSE CASE WHEN MaterialGroupCode='41103023' THEN  'Laboratory chillers '
ELSE CASE WHEN MaterialGroupCode='41103200' THEN  'Laboratory washing and cleaning equipment '
ELSE CASE WHEN MaterialGroupCode='41103206' THEN  'Laboratory washing detergents '
ELSE CASE WHEN MaterialGroupCode='41103207' THEN  'Microplate washers'
ELSE CASE WHEN MaterialGroupCode='41103300' THEN  ' FLUID MECHANICS EQUIPMENT'
ELSE CASE WHEN MaterialGroupCode='41103502' THEN  'Fume hoods or cupboa'
ELSE CASE WHEN MaterialGroupCode='41103504' THEN  'Laminar flow cabinets or stations'
ELSE CASE WHEN MaterialGroupCode='41103700' THEN  'Laboratory baths '
ELSE CASE WHEN MaterialGroupCode='41103715' THEN  'Laboratory bath accessories or supplies '
ELSE CASE WHEN MaterialGroupCode='41103800' THEN  ' LABORATORY MIXING AND STIRRING AND SHAKING EQUIP. AND SUP.'
ELSE CASE WHEN MaterialGroupCode='41103801' THEN  'Laboratory mixers '
ELSE CASE WHEN MaterialGroupCode='41103900' THEN  'Laboratory centrifuges and accessories '
ELSE CASE WHEN MaterialGroupCode='41104200' THEN  'Laboratory water purification equipment and supplies '
ELSE CASE WHEN MaterialGroupCode='41104300' THEN  ' FERMENTATION EQUIPMENT'
ELSE CASE WHEN MaterialGroupCode='41104400' THEN  'Laboratory incubating equipment '
ELSE CASE WHEN MaterialGroupCode='41104500' THEN  'Laboratory ovens and accessories '
ELSE CASE WHEN MaterialGroupCode='41104700' THEN  'Laboratory freeze dryers and lyopholizers and accessories '
ELSE CASE WHEN MaterialGroupCode='41104803' THEN  'Laboratory evaporators '
ELSE CASE WHEN MaterialGroupCode='41104900' THEN  'Laboratory filtering equipment and supplies '
ELSE CASE WHEN MaterialGroupCode='41104903' THEN  'Ultra filtration equ'
ELSE CASE WHEN MaterialGroupCode='41105100' THEN  ' LABORATORY PUMPS AND TUBING'
ELSE CASE WHEN MaterialGroupCode='41105101' THEN  'Laboratory vacuum pumps '
ELSE CASE WHEN MaterialGroupCode='41105300' THEN  ' LABORATORY ELECTROPHORESIS AND BLOTTING SYSTEM AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='41110000' THEN  ' MEASURING AND OBSERVING AND TESTING INSTRUMENTS'
ELSE CASE WHEN MaterialGroupCode='41111500' THEN  ' WEIGHT MEASURING INSTRUMENTS'
ELSE CASE WHEN MaterialGroupCode='41111502' THEN  'Laboratory balances'
ELSE CASE WHEN MaterialGroupCode='41111600' THEN  ' LENGTH AND THICKNESS AND DISTANCE MEASURING INSTRUMENTS'
ELSE CASE WHEN MaterialGroupCode='41111700' THEN  ' VIEWING AND OBSERVING INSTRUMENTS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='41111740' THEN  'Automated optical inspection system'
ELSE CASE WHEN MaterialGroupCode='41111900' THEN  ' INDICATING AND RECORDING INSTRUMENTS'
ELSE CASE WHEN MaterialGroupCode='41112100' THEN  ' TRANSDUCERS'
ELSE CASE WHEN MaterialGroupCode='41112200' THEN  ' TEMPERATURE AND HEAT MEASURING INSTRUMENTS'
ELSE CASE WHEN MaterialGroupCode='41112201' THEN  'Temperature and heat'
ELSE CASE WHEN MaterialGroupCode='41112400' THEN  ' PRESSURE MEASURING AND CONTROL INSTRUMENTS'
ELSE CASE WHEN MaterialGroupCode='41112500' THEN  ' LIQUID AND GAS FLOW MEASURING AND OBSERVING INSTRUMENTS'
ELSE CASE WHEN MaterialGroupCode='41113037' THEN  'Microplate readers'
ELSE CASE WHEN MaterialGroupCode='41113100' THEN  ' GAS ANALYZERS AND MONITORS'
ELSE CASE WHEN MaterialGroupCode='41115300' THEN  ' LIGHT AND WAVE GENERATING AND MEASURING EQUIPMENT'
ELSE CASE WHEN MaterialGroupCode='41115600' THEN  ' ELECTROCHEMICAL MEASURING INSTRUMENTS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='41115604' THEN  'pH electrodes'
ELSE CASE WHEN MaterialGroupCode='41115700' THEN  ' CHROMATOGRAPHIC MEASURING INSTRUMENTS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='41115707' THEN  'High pressure liquid chromatograph chromatography'
ELSE CASE WHEN MaterialGroupCode='41115800' THEN  'Clinical and diagnostic analyzers and accessories and supplies'
ELSE CASE WHEN MaterialGroupCode='41116000' THEN  ' CLINICAL AND DIAGNOSTIC ANALYZER REAGENTS'
ELSE CASE WHEN MaterialGroupCode='41116100' THEN  ' MANUAL TEST KITS, QUALITY CONTROLS, CALIBRATORS, STANDARDS'
ELSE CASE WHEN MaterialGroupCode='41116128' THEN  'Immunology/serology'
ELSE CASE WHEN MaterialGroupCode='41120000' THEN  ' LABORATORY SUPPLIES AND FIXTURES'
ELSE CASE WHEN MaterialGroupCode='41121500' THEN  ' PIPETTES AND LIQUID HANDLING EQUIPMENT AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='41121800' THEN  ' GENERAL LABORATORY GLASSWARE AND PLASTICWARE AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='41122000' THEN  ' LABORATORY OR SAMPLING SYRINGES'
ELSE CASE WHEN MaterialGroupCode='41122003' THEN  'Syringe adapters or'
ELSE CASE WHEN MaterialGroupCode='41122502' THEN  'Laboratory stoppers'
ELSE CASE WHEN MaterialGroupCode='41122800' THEN  'Laboratory stands and racks and trays '
ELSE CASE WHEN MaterialGroupCode='41123003' THEN  'Desiccants'
ELSE CASE WHEN MaterialGroupCode='42000000' THEN  'Medical Equipment an'
ELSE CASE WHEN MaterialGroupCode='42120000' THEN  ' VETERINARY EQUIPMENT AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='42132200' THEN  ' MEDICAL GLOVES AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='42140000' THEN  ' PATIENT CARE AND TREATMENT PRODUCTS AND SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='42142600' THEN  ' SYRINGES AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='42172001' THEN  'Emergency medical se'
ELSE CASE WHEN MaterialGroupCode='42281500' THEN  ' AUTOCLAVE AND STERILIZER EQUIPMENT AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='42281502' THEN  'Dry heat or hot air'
ELSE CASE WHEN MaterialGroupCode='42281508' THEN  'Steam autoclaves or'
ELSE CASE WHEN MaterialGroupCode='43000000' THEN  'Information Technolo'
ELSE CASE WHEN MaterialGroupCode='43191502' THEN  'Pagers '
ELSE CASE WHEN MaterialGroupCode='43200000' THEN  ' COMPONENTS FOR INFORM. TECH., BROADCASTING, TELECOMM.'
ELSE CASE WHEN MaterialGroupCode='43201409' THEN  'Wireless network int'
ELSE CASE WHEN MaterialGroupCode='43210000' THEN  ' COMPUTER EQUIPMENT AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='43211500' THEN  'Computers '
ELSE CASE WHEN MaterialGroupCode='43211501' THEN  'Computer servers '
ELSE CASE WHEN MaterialGroupCode='43211503' THEN  'Notebook computers '
ELSE CASE WHEN MaterialGroupCode='43211507' THEN  'Desktop computers '
ELSE CASE WHEN MaterialGroupCode='43211512' THEN  'Mainframe computers '
ELSE CASE WHEN MaterialGroupCode='43211600' THEN  'Computer accessories '
ELSE CASE WHEN MaterialGroupCode='43211902' THEN  'Liquid crystal displ'
ELSE CASE WHEN MaterialGroupCode='43212100' THEN  'Computer printers '
ELSE CASE WHEN MaterialGroupCode='43220000' THEN  'Data Voice or Multimedia Network Equipment or Platforms and Accessories '
ELSE CASE WHEN MaterialGroupCode='43223108' THEN  'WLAN wireless access network equipment and components'
ELSE CASE WHEN MaterialGroupCode='43230000' THEN  'Software '
ELSE CASE WHEN MaterialGroupCode='43231100' THEN  'Software Delivered v'
ELSE CASE WHEN MaterialGroupCode='43231200' THEN  'Software Delivered E'
ELSE CASE WHEN MaterialGroupCode='43231270' THEN  'Software Usage Fee'
ELSE CASE WHEN MaterialGroupCode='43231280' THEN  'Business Use Softwar'
ELSE CASE WHEN MaterialGroupCode='43233202' THEN  'Canned Software Lice'
ELSE CASE WHEN MaterialGroupCode='44000000' THEN  'Office Equipment & A'
ELSE CASE WHEN MaterialGroupCode='44100000' THEN  ' OFFICE MACHINES AND THEIR SUPPLIES AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='44101501' THEN  'Photocopiers'
ELSE CASE WHEN MaterialGroupCode='44101503' THEN  'Multifunction machin'
ELSE CASE WHEN MaterialGroupCode='44101700' THEN  ' PRINTER AND PHOTOCOPIER AND FACSIMILE ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='44102400' THEN  ' LABELING MACHINES'
ELSE CASE WHEN MaterialGroupCode='44102605' THEN  'Dictation machines'
ELSE CASE WHEN MaterialGroupCode='44103100' THEN  ' PRINTER AND FACSIMILE AND PHOTOCOPIER SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='44120000' THEN  ' OFFICE SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='44121700' THEN  ' WRITING INSTRUMENTS'
ELSE CASE WHEN MaterialGroupCode='45000000' THEN  'Printing and Photogr'
ELSE CASE WHEN MaterialGroupCode='45101900' THEN  ' PRINTING LABORATORY EQUIPMENT AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='45110000' THEN  'Audio and visual presentation and composing equipment'
ELSE CASE WHEN MaterialGroupCode='45111900' THEN  'Phone and video conference equipment and hardware and controllers'
ELSE CASE WHEN MaterialGroupCode='45130000' THEN  ' PHOTOGRAPHIC AND RECORDING MEDIA'
ELSE CASE WHEN MaterialGroupCode='46000000' THEN  'Defense and Law Enfo'
ELSE CASE WHEN MaterialGroupCode='46180000' THEN  ' PERSONAL SAFETY AND PROTECTION'
ELSE CASE WHEN MaterialGroupCode='46181500' THEN  ' SAFETY APPAREL'
ELSE CASE WHEN MaterialGroupCode='46181600' THEN  ' SAFETY FOOTWEAR'
ELSE CASE WHEN MaterialGroupCode='46181605' THEN  'Safety shoes'
ELSE CASE WHEN MaterialGroupCode='46181700' THEN  ' FACE AND HEAD PROTECTION'
ELSE CASE WHEN MaterialGroupCode='46181800' THEN  ' VISION PROTECTION AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='46181900' THEN  ' HEARING PROTECTORS'
ELSE CASE WHEN MaterialGroupCode='46182000' THEN  ' RESPIRATORY PROTECTION'
ELSE CASE WHEN MaterialGroupCode='46182200' THEN  ' ERGONOMIC SUPPORT AIDS'
ELSE CASE WHEN MaterialGroupCode='46190000' THEN  ' FIRE PROTECTION'
ELSE CASE WHEN MaterialGroupCode='47000000' THEN  'Cleaning Equipment a'
ELSE CASE WHEN MaterialGroupCode='47100000' THEN  ' WATER AND WASTEWATER TREATMENT SUPPLY AND DISPOSAL'
ELSE CASE WHEN MaterialGroupCode='47101500' THEN  ' WATER TREATMENT AND SUPPLY EQUIPMENT'
ELSE CASE WHEN MaterialGroupCode='47101600' THEN  ' WATER TREATMENT CONSUMABLES'
ELSE CASE WHEN MaterialGroupCode='47121700' THEN  ' WASTE CONTAINERS AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='47130000' THEN  ' CLEANING AND JANITORIAL SUPPLIES'
ELSE CASE WHEN MaterialGroupCode='47131800' THEN  ' CLEANING AND DISINFECTING SOLUTIONS'
ELSE CASE WHEN MaterialGroupCode='48000000' THEN  'Service Industry Mac'
ELSE CASE WHEN MaterialGroupCode='48100000' THEN  ' INSTITUTIONAL FOOD SERVICES EQUIPMENT'
ELSE CASE WHEN MaterialGroupCode='49000000' THEN  'Sports and Recreatio'
ELSE CASE WHEN MaterialGroupCode='50000000' THEN  'Food Beverage and To'
ELSE CASE WHEN MaterialGroupCode='51000000' THEN  'Drugs and Pharmaceut'
ELSE CASE WHEN MaterialGroupCode='52000000' THEN  'Domestic Appliances'
ELSE CASE WHEN MaterialGroupCode='52130000' THEN  'Window treatments '
ELSE CASE WHEN MaterialGroupCode='52161600' THEN  ' AUDIO VISUAL EQUIPMENT ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='53000000' THEN  'Apparel and Luggage'
ELSE CASE WHEN MaterialGroupCode='53100000' THEN  ' CLOTHING'
ELSE CASE WHEN MaterialGroupCode='54000000' THEN  'Timepieces and Jewel'
ELSE CASE WHEN MaterialGroupCode='55000000' THEN  'Published Products'
ELSE CASE WHEN MaterialGroupCode='55120000' THEN  ' SIGNAGE AND ACCESSORIES'
ELSE CASE WHEN MaterialGroupCode='55121600' THEN  'Labels'
ELSE CASE WHEN MaterialGroupCode='55121609' THEN  'Packaging labels'
ELSE CASE WHEN MaterialGroupCode='55121700' THEN  ' SIGNAGE'
ELSE CASE WHEN MaterialGroupCode='56000000' THEN  'Furniture and Furnishings  '
ELSE CASE WHEN MaterialGroupCode='56111700' THEN  ' CASEGOOD AND NON MODULAR SYSTEMS'
ELSE CASE WHEN MaterialGroupCode='56112000' THEN  ' COMPUTER SUPPORT FURNITURE'
ELSE CASE WHEN MaterialGroupCode='56122000' THEN  ' LABORATORY FURNITURE'
ELSE CASE WHEN MaterialGroupCode='60000000' THEN  'Mus Instr and Games'
ELSE CASE WHEN MaterialGroupCode='70000000' THEN  'Farming and Fishing'
ELSE CASE WHEN MaterialGroupCode='70111601' THEN  'Planting services '
ELSE CASE WHEN MaterialGroupCode='70120000' THEN  ' LIVESTOCK SERVICES'
ELSE CASE WHEN MaterialGroupCode='70122000' THEN  'Animal health '
ELSE CASE WHEN MaterialGroupCode='70122010' THEN  'Animal health inform'
ELSE CASE WHEN MaterialGroupCode='70130000' THEN  ' LAND AND SOIL PREPARATION AND MANAGEMENT AND PROTECTION'
ELSE CASE WHEN MaterialGroupCode='71000000' THEN  'Mining and Oil and G'
ELSE CASE WHEN MaterialGroupCode='72000000' THEN  ' BUILDING CONSTRUCTION, SUPPORT, MAINTENANCE AND REPAIR SERV.'
ELSE CASE WHEN MaterialGroupCode='72000001' THEN  'Building and Constru'
ELSE CASE WHEN MaterialGroupCode='72100000' THEN  ' BUILDING CONSTRUCTION, SUPPORT, MAINTENANCE AND REPAIR SERV.'
ELSE CASE WHEN MaterialGroupCode='72101901' THEN  'Interior design or d'
ELSE CASE WHEN MaterialGroupCode='72102100' THEN  'Pest control '
ELSE CASE WHEN MaterialGroupCode='72102200' THEN  'Electrical services '
ELSE CASE WHEN MaterialGroupCode='72102205' THEN  'Telecom equipment maintenance or support'
ELSE CASE WHEN MaterialGroupCode='72102300' THEN  ' PLUMBING AND HEATING AND AIR CONDITIONING'
ELSE CASE WHEN MaterialGroupCode='72102302' THEN  'Heating systems inst'
ELSE CASE WHEN MaterialGroupCode='72102307' THEN  'Pressure controller'
ELSE CASE WHEN MaterialGroupCode='72102400' THEN  'Painting and paper h'
ELSE CASE WHEN MaterialGroupCode='72102702' THEN  'Floor covering insta'
ELSE CASE WHEN MaterialGroupCode='72102900' THEN  'Facility maintenance'
ELSE CASE WHEN MaterialGroupCode='72102901' THEN  'Snow removal service'
ELSE CASE WHEN MaterialGroupCode='72102902' THEN  'Landscaping services'
ELSE CASE WHEN MaterialGroupCode='72130000' THEN  ' GENERAL BUILDING CONSTRUCTION'
ELSE CASE WHEN MaterialGroupCode='72131600' THEN  ' COMMERCIAL OR INDUSTRIAL CONSTRUCTION'
ELSE CASE WHEN MaterialGroupCode='73000000' THEN  'Industrial Productio'
ELSE CASE WHEN MaterialGroupCode='73101604' THEN  'Organic chemical pro'
ELSE CASE WHEN MaterialGroupCode='73101701' THEN  'Drug or Medicine Pro'
ELSE CASE WHEN MaterialGroupCode='73101702' THEN  'Vaccines or sera or'
ELSE CASE WHEN MaterialGroupCode='73150000' THEN  ' MANUFACTURING SUPPORT SERVICES'
ELSE CASE WHEN MaterialGroupCode='73151600' THEN  ' PACKAGING SERVICES'
ELSE CASE WHEN MaterialGroupCode='73152100' THEN  ' MANUFACTURING EQUIPMENT MAINTENANCE AND REPAIR SERVICES'
ELSE CASE WHEN MaterialGroupCode='73152101' THEN  'Manufacturing equipm'
ELSE CASE WHEN MaterialGroupCode='73161510' THEN  'Chemical or pharmace'
ELSE CASE WHEN MaterialGroupCode='73161515' THEN  'Office machinery or'
ELSE CASE WHEN MaterialGroupCode='76000000' THEN  'Industrial Cleaning'
ELSE CASE WHEN MaterialGroupCode='76101500' THEN  ' DISINFECTION'
ELSE CASE WHEN MaterialGroupCode='76110000' THEN  ' CLEANING AND JANITORIAL SERVICES'
ELSE CASE WHEN MaterialGroupCode='76111500' THEN  ' GENERAL BUILDING, OFFICE CLEANING, MAINTENANCE SERVICES'
ELSE CASE WHEN MaterialGroupCode='76111504' THEN  'Window or window bli'
ELSE CASE WHEN MaterialGroupCode='76111506' THEN  'Interior plant lands'
ELSE CASE WHEN MaterialGroupCode='76111604' THEN  'Floor waxing or carp'
ELSE CASE WHEN MaterialGroupCode='76120000' THEN  ' REFUSE DISPOSAL AND TREATMENT'
ELSE CASE WHEN MaterialGroupCode='76121600' THEN  ' NONHAZARDOUS WASTE DISPOSAL'
ELSE CASE WHEN MaterialGroupCode='76121900' THEN  ' HAZARDOUS WASTE DISPOSAL'
ELSE CASE WHEN MaterialGroupCode='76122300' THEN  'Recycling services '
ELSE CASE WHEN MaterialGroupCode='77000000' THEN  'Environmental Servic'
ELSE CASE WHEN MaterialGroupCode='77100000' THEN  ' ENVIRONMENTAL MANAGEMENT'
ELSE CASE WHEN MaterialGroupCode='78000000' THEN  'Transportation and S'
ELSE CASE WHEN MaterialGroupCode='78100000' THEN  ' MAIL AND CARGO TRANSPORT'
ELSE CASE WHEN MaterialGroupCode='78101500' THEN  'Air cargo transport'
ELSE CASE WHEN MaterialGroupCode='78101600' THEN  ' RAIL CARGO TRANSPORT'
ELSE CASE WHEN MaterialGroupCode='78101700' THEN  ' MARINE CARGO TRANSPORT'
ELSE CASE WHEN MaterialGroupCode='78101800' THEN  ' ROAD CARGO TRANSPORT'
ELSE CASE WHEN MaterialGroupCode='78101804' THEN  'Relocation services'
ELSE CASE WHEN MaterialGroupCode='78101900' THEN  ' INTERMODAL CARGO TRANSPORT'
ELSE CASE WHEN MaterialGroupCode='78102200' THEN  ' POSTAL AND SMALL PARCEL AND COURIER SERVICES'
ELSE CASE WHEN MaterialGroupCode='78102201' THEN  'National Postal Deli'
ELSE CASE WHEN MaterialGroupCode='78102203' THEN  'Mailing or mail pick'
ELSE CASE WHEN MaterialGroupCode='78111502' THEN  'Commercial airplane travel'
ELSE CASE WHEN MaterialGroupCode='78111808' THEN  'Vehicle rental '
ELSE CASE WHEN MaterialGroupCode='78111809' THEN  'Fleet leasing services'
ELSE CASE WHEN MaterialGroupCode='78131800' THEN  ' SPECIALIZED WAREHOUSING AND STORAGE'
ELSE CASE WHEN MaterialGroupCode='78141500' THEN  ' TRANSPORT ARRANGEMENTS'
ELSE CASE WHEN MaterialGroupCode='78141502' THEN  'Customs brokerage services'
ELSE CASE WHEN MaterialGroupCode='80000000' THEN  'Management, Business Professionals, Administrative Services'
ELSE CASE WHEN MaterialGroupCode='80101500' THEN  ' BUSINESS AND CORPORATE MANAGEMENT CONSULTATION SERVICES'
ELSE CASE WHEN MaterialGroupCode='80101504' THEN  'Strategic planning c'
ELSE CASE WHEN MaterialGroupCode='80101507' THEN  'Information technology consultation services'
ELSE CASE WHEN MaterialGroupCode='80101508' THEN  'Business intelligenc'
ELSE CASE WHEN MaterialGroupCode='80101702' THEN  'Productivity or efficiency studies or implementation'
ELSE CASE WHEN MaterialGroupCode='80110000' THEN  ' HUMAN RESOURCES SERVICES'
ELSE CASE WHEN MaterialGroupCode='80111507' THEN  'Outplacement service'
ELSE CASE WHEN MaterialGroupCode='80111600' THEN  ' TEMPORARY PERSONNEL SERVICES'
ELSE CASE WHEN MaterialGroupCode='80111700' THEN  ' PERSONNEL RECRUITMENT'
ELSE CASE WHEN MaterialGroupCode='80111707' THEN  'Permanent technical'
ELSE CASE WHEN MaterialGroupCode='80120000' THEN  ' LEGAL SERVICES'
ELSE CASE WHEN MaterialGroupCode='80131500' THEN  ' LEASE AND RENTAL OF PROPERTY OR BUILDING'
ELSE CASE WHEN MaterialGroupCode='80141500' THEN  ' MARKET RESEARCH'
ELSE CASE WHEN MaterialGroupCode='80141501' THEN  'Marketing analysis'
ELSE CASE WHEN MaterialGroupCode='80141504' THEN  'Preparation of commo'
ELSE CASE WHEN MaterialGroupCode='80141507' THEN  'Consumer based resea'
ELSE CASE WHEN MaterialGroupCode='80141509' THEN  'Market intelligence'
ELSE CASE WHEN MaterialGroupCode='80141600' THEN  ' SALES AND BUSINESS PROMOTION ACTIVITIES'
ELSE CASE WHEN MaterialGroupCode='80141601' THEN  'Sales promotion serv'
ELSE CASE WHEN MaterialGroupCode='80141604' THEN  'Branding of product'
ELSE CASE WHEN MaterialGroupCode='80141605' THEN  'Promotional merchand'
ELSE CASE WHEN MaterialGroupCode='80141606' THEN  'Direct marketing ful'
ELSE CASE WHEN MaterialGroupCode='80141607' THEN  'Events management'
ELSE CASE WHEN MaterialGroupCode='80141609' THEN  'Sponsorship of event or celebrity'
ELSE CASE WHEN MaterialGroupCode='80141611' THEN  'Product or gift pers'
ELSE CASE WHEN MaterialGroupCode='80141612' THEN  'Sales or marketing p'
ELSE CASE WHEN MaterialGroupCode='80141614' THEN  'Public relations pro'
ELSE CASE WHEN MaterialGroupCode='80141618' THEN  'Sales marketing agen'
ELSE CASE WHEN MaterialGroupCode='80141619' THEN  'Customer relationship center CRC management services'
ELSE CASE WHEN MaterialGroupCode='80141700' THEN  ' DISTRIBUTION'
ELSE CASE WHEN MaterialGroupCode='80141701' THEN  'Direct sales service'
ELSE CASE WHEN MaterialGroupCode='80141703' THEN  'Retail distribution'
ELSE CASE WHEN MaterialGroupCode='80141800' THEN  ' MAILING SERVICES'
ELSE CASE WHEN MaterialGroupCode='80141900' THEN  ' TRADE SHOWS AND EXHIBITS'
ELSE CASE WHEN MaterialGroupCode='80141901' THEN  'Auto shows or other'
ELSE CASE WHEN MaterialGroupCode='80141902' THEN  'Meetings events'
ELSE CASE WHEN MaterialGroupCode='80161502' THEN  'Meeting planning services'
ELSE CASE WHEN MaterialGroupCode='80161505' THEN  'Fleet management services'
ELSE CASE WHEN MaterialGroupCode='80161507' THEN  'Audio visual service'
ELSE CASE WHEN MaterialGroupCode='80161602' THEN  'Receiving or invento'
ELSE CASE WHEN MaterialGroupCode='81000000' THEN  'Engineering and Rese'
ELSE CASE WHEN MaterialGroupCode='81100000' THEN  ' PROFESSIONAL ENGINEERING SERVICES'
ELSE CASE WHEN MaterialGroupCode='81101513' THEN  'Building constructio'
ELSE CASE WHEN MaterialGroupCode='81101703' THEN  'Engineering testing'
ELSE CASE WHEN MaterialGroupCode='81101706' THEN  'Laboratory equipment maintenance '
ELSE CASE WHEN MaterialGroupCode='81111509' THEN  'Internet or intranet client application development services'
ELSE CASE WHEN MaterialGroupCode='81111811' THEN  'Technical support or help desk services '
ELSE CASE WHEN MaterialGroupCode='81111812' THEN  'Computer hardware maintenance or support'
ELSE CASE WHEN MaterialGroupCode='81111819' THEN  'Quality assurance se'
ELSE CASE WHEN MaterialGroupCode='81112002' THEN  'Data processing or preparation services'
ELSE CASE WHEN MaterialGroupCode='81112100' THEN  ' INTERNET SERVICES'
ELSE CASE WHEN MaterialGroupCode='81112106' THEN  'Business Use Application Service Providers'
ELSE CASE WHEN MaterialGroupCode='81112200' THEN  'Software maintenance and support '
ELSE CASE WHEN MaterialGroupCode='81141601' THEN  'Logistics'
ELSE CASE WHEN MaterialGroupCode='81141800' THEN  'Facility Management'
ELSE CASE WHEN MaterialGroupCode='81160000' THEN  ' INFORMATION TECHNOLOGY SERVICE DELIVERY'
ELSE CASE WHEN MaterialGroupCode='81161700' THEN  ' TELECOMMUNICATION SERVICES'
ELSE CASE WHEN MaterialGroupCode='82000000' THEN  'Editorial and Design'
ELSE CASE WHEN MaterialGroupCode='82100000' THEN  ' ADVERTISING'
ELSE CASE WHEN MaterialGroupCode='82101500' THEN  ' PRINT ADVERTISING'
ELSE CASE WHEN MaterialGroupCode='82101503' THEN  'Magazine advertising'
ELSE CASE WHEN MaterialGroupCode='82101505' THEN  'Handbill or coupon a'
ELSE CASE WHEN MaterialGroupCode='82101602' THEN  'Television advertisi'
ELSE CASE WHEN MaterialGroupCode='82101603' THEN  'Internet advertising'
ELSE CASE WHEN MaterialGroupCode='82101800' THEN  ' ADVERTISING AGENCY SERVICES'
ELSE CASE WHEN MaterialGroupCode='82101801' THEN  'Advertising campaign'
ELSE CASE WHEN MaterialGroupCode='82101900' THEN  ' MEDIA PLACEMENT AND FULFILLMENT'
ELSE CASE WHEN MaterialGroupCode='82110000' THEN  ' WRITING AND TRANSLATIONS'
ELSE CASE WHEN MaterialGroupCode='82111503' THEN  'Academic or scientif'
ELSE CASE WHEN MaterialGroupCode='82111800' THEN  ' EDITORIAL AND SUPPORT SERVICES'
ELSE CASE WHEN MaterialGroupCode='82111804' THEN  'Written translation'
ELSE CASE WHEN MaterialGroupCode='82121500' THEN  ' PRINTING'
ELSE CASE WHEN MaterialGroupCode='82121503' THEN  'Digital printing'
ELSE CASE WHEN MaterialGroupCode='82121504' THEN  'Letterpress or scree'
ELSE CASE WHEN MaterialGroupCode='82121505' THEN  'Promotional or advertising printing'
ELSE CASE WHEN MaterialGroupCode='82121507' THEN  'Stationery or busine'
ELSE CASE WHEN MaterialGroupCode='82121508' THEN  'Wrap or tag or label'
ELSE CASE WHEN MaterialGroupCode='82121509' THEN  'Security or financia'
ELSE CASE WHEN MaterialGroupCode='82121801' THEN  'Textbook or research'
ELSE CASE WHEN MaterialGroupCode='82130000' THEN  ' PHOTOGRAPHIC SERVICES'
ELSE CASE WHEN MaterialGroupCode='82131603' THEN  'Video production ser'
ELSE CASE WHEN MaterialGroupCode='82140000' THEN  ' GRAPHIC DESIGN'
ELSE CASE WHEN MaterialGroupCode='82141502' THEN  'Art design or graphi'
ELSE CASE WHEN MaterialGroupCode='82141506' THEN  'Package design servi'
ELSE CASE WHEN MaterialGroupCode='82150001' THEN  'Web development'
ELSE CASE WHEN MaterialGroupCode='82150002' THEN  'Mobile app development'
ELSE CASE WHEN MaterialGroupCode='82150004' THEN  'Digital media – paid'
ELSE CASE WHEN MaterialGroupCode='82150006' THEN  'On-line monitoring/l'
ELSE CASE WHEN MaterialGroupCode='82150007' THEN  'Social media campaig'
ELSE CASE WHEN MaterialGroupCode='83000000' THEN  'Public Utilities and'
ELSE CASE WHEN MaterialGroupCode='83100000' THEN  'Utilities'
ELSE CASE WHEN MaterialGroupCode='83101500' THEN  ' WATER AND SEWER UTILITIES'
ELSE CASE WHEN MaterialGroupCode='83101501' THEN  'Supply of water'
ELSE CASE WHEN MaterialGroupCode='83101601' THEN  'Supply of natural ga'
ELSE CASE WHEN MaterialGroupCode='83101602' THEN  'Supply of fuel oil'
ELSE CASE WHEN MaterialGroupCode='83101800' THEN  ' ELECTRIC UTILITIES'
ELSE CASE WHEN MaterialGroupCode='83101801' THEN  'Supply of single pha'
ELSE CASE WHEN MaterialGroupCode='83111603' THEN  'Cellular telephone s'
ELSE CASE WHEN MaterialGroupCode='83121700' THEN  ' MASS COMMUNICATION SERVICES'
ELSE CASE WHEN MaterialGroupCode='84000000' THEN  'Financial and Insurance Services'
ELSE CASE WHEN MaterialGroupCode='84111505' THEN  'Payroll accounting s'
ELSE CASE WHEN MaterialGroupCode='84111600' THEN  ' AUDIT SERVICES'
ELSE CASE WHEN MaterialGroupCode='84121607' THEN  'Operating lease fina'
ELSE CASE WHEN MaterialGroupCode='84130000' THEN  ' INSURANCE AND RETIREMENT SERVICES'
ELSE CASE WHEN MaterialGroupCode='84140010' THEN  'Credit Reporting Ser'
ELSE CASE WHEN MaterialGroupCode='85000000' THEN  'Healthcare Services '
ELSE CASE WHEN MaterialGroupCode='85100000' THEN  ' COMPREHENSIVE HEALTH SERVICES'
ELSE CASE WHEN MaterialGroupCode='85101700' THEN  ' HEALTH ADMINISTRATION SERVICES'
ELSE CASE WHEN MaterialGroupCode='85121800' THEN  ' MEDICAL LABORATORIES'
ELSE CASE WHEN MaterialGroupCode='85130000' THEN  ' MEDICAL SCIENCE RESEARCH AND EXPERIMENTATION'
ELSE CASE WHEN MaterialGroupCode='85131503' THEN  'Animal experimentation'
ELSE CASE WHEN MaterialGroupCode='85131701' THEN  'Pharmaceutical research services'
ELSE CASE WHEN MaterialGroupCode='85131708' THEN  'Epidemiology Researc'
ELSE CASE WHEN MaterialGroupCode='86000000' THEN  'Education and Training Services'
ELSE CASE WHEN MaterialGroupCode='86120000' THEN  ' EDUCATIONAL INSTITUTIONS'
ELSE CASE WHEN MaterialGroupCode='86121700' THEN  ' UNIVERSITY AND COLLEGES'
ELSE CASE WHEN MaterialGroupCode='86141704' THEN  'Library or documenta'
ELSE CASE WHEN MaterialGroupCode='90000000' THEN  'Travel and Food and Lodging and Entertainment Services'
ELSE CASE WHEN MaterialGroupCode='90101603' THEN  'Catering services'
ELSE CASE WHEN MaterialGroupCode='90101700' THEN  'Cafeteria services '
ELSE CASE WHEN MaterialGroupCode='90110000' THEN  ' HOTELS AND LODGING AND MEETING FACILITIES'
ELSE CASE WHEN MaterialGroupCode='90121502' THEN  'Travel agencies '
ELSE CASE WHEN MaterialGroupCode='91000000' THEN  'Personal and Domesti'
ELSE CASE WHEN MaterialGroupCode='91111502' THEN  'Laundry services '
ELSE CASE WHEN MaterialGroupCode='92000000' THEN  'National Defense and'
ELSE CASE WHEN MaterialGroupCode='92121504' THEN  'Security guard servi'
ELSE CASE WHEN MaterialGroupCode='93141802' THEN  'Recruitment Services'
ELSE CASE WHEN MaterialGroupCode='94000000' THEN  'Organizations and Cl'
ELSE CASE WHEN MaterialGroupCode='00HE00000' THEN  'HERMES interface'
ELSE CASE WHEN MaterialGroupCode='CTRLSUB' THEN  'Controlled Substance'
ELSE CASE WHEN MaterialGroupCode='EMP' THEN  'Employees'
ELSE CASE WHEN MaterialGroupCode='GOVT' THEN  'Government Agencies'
ELSE CASE WHEN MaterialGroupCode='Mktg0001' THEN  'Speakers Bureau/Engagement'
ELSE CASE WHEN MaterialGroupCode='NDF99IFM' THEN  '99 NotDefined - IFM'
ELSE CASE WHEN MaterialGroupCode='NDF99IND' THEN  '99 NotDefined - Indirect'
ELSE CASE WHEN MaterialGroupCode='NDF99LSE' THEN  '99 NotDefined - Lab Supplies and Equipment'
ELSE CASE WHEN MaterialGroupCode='NDF99LSU' THEN  '99 NotDefined - Lab_Supplies'
ELSE CASE WHEN MaterialGroupCode='NDF99PRC' THEN  '99 NotDefined - Prod_Chem_and_Mfg_Svcs.'
ELSE CASE WHEN MaterialGroupCode='PREC002' THEN  'Analytical & Stability Testing'
ELSE CASE WHEN MaterialGroupCode='PREC003' THEN  'Assay Development & Release Testing'
ELSE CASE WHEN MaterialGroupCode='SPEAKER' THEN  'Speaker'
ELSE CASE WHEN MaterialGroupCode='STOXINS' THEN  'Select Toxins'
ELSE CASE WHEN MaterialGroupCode='NDF99Pre' THEN  '99_Not_Defined_-_Profressional_Services'
ELSE CASE WHEN IndustryStandardDescription ='NDF99DIR' THEN  '99_NotDefined_-_Direct'
ELSE CASE WHEN IndustryStandardDescription ='NDF99CMO' THEN  '99_NotDefined_-_CM'
ELSE CASE WHEN IndustryStandardDescription ='NDF99MDE' THEN  '99_NotDefined_-_Medical_Devices'
ELSE CASE WHEN IndustryStandardDescription ='NDF99PAC' THEN  '99_NotDefined_-_Packaging'
ELSE CASE WHEN IndustryStandardDescription ='NDF99PCC' THEN  '99_NotDefined_-_Product_Contact_Component'
ELSE CASE WHEN IndustryStandardDescription ='NDF99RAM' THEN  '99_NotDefined_-_Raw_Materials'
ELSE CASE WHEN IndustryStandardDescription ='NDF99API' THEN  '99_NotDefined_-_APIs_and_Intermediates'
ELSE CASE WHEN IndustryStandardDescription ='NDF99APL' THEN  '99_NotDefined_-_Applicator'
ELSE CASE WHEN IndustryStandardDescription ='NDF99BST' THEN  '99_NotDefined_-_Bio_Sterile'
ELSE CASE WHEN IndustryStandardDescription ='NDF99CCM' THEN  '99_NotDefined_-_Cell_Culture'
ELSE CASE WHEN IndustryStandardDescription ='NDF99EXC' THEN  '99_NotDefined_-_Excipients'
ELSE CASE WHEN IndustryStandardDescription ='NDF99MDS' THEN  '99_NotDefined_-_Medical_Devices_Marketing_Services'
ELSE CASE WHEN IndustryStandardDescription ='NDF99OCH' THEN  '99_NotDefined_-_Other_Chemicals'
ELSE CASE WHEN IndustryStandardDescription ='NDF99PHD' THEN  '99_NotDefined_-_Pharma_Distribution_Contact'
ELSE CASE WHEN IndustryStandardDescription ='NDF99PPP' THEN  '99_NotDefined_-_Primary_Packaging'
ELSE CASE WHEN IndustryStandardDescription ='NDF99PSE' THEN  '99_NotDefined_-_Secondary_Packaging'
ELSE CASE WHEN IndustryStandardDescription ='NDF99PTE' THEN  '99_NotDefined_-_Tertiary_Packaging'
ELSE CASE WHEN IndustryStandardDescription ='NDF99REA' THEN  '99_NotDefined_-_Reagents'
ELSE CASE WHEN IndustryStandardDescription ='NDF99RMG' THEN  '99_NotDefined_-_Gases'
ELSE CASE WHEN IndustryStandardDescription ='11101800' THEN  ' PRECIOUS METALS'
ELSE CASE WHEN IndustryStandardDescription ='12160000' THEN  ' ADDITIVES'
ELSE CASE WHEN IndustryStandardDescription ='12161700' THEN  ' BUFFERS'
ELSE CASE WHEN IndustryStandardDescription ='12170000' THEN  ' COLORANTS'
ELSE CASE WHEN IndustryStandardDescription ='12190000' THEN  ' SOLVENTS'
ELSE CASE WHEN IndustryStandardDescription ='12352200' THEN  ' BIOCHEMICALS'
ELSE CASE WHEN IndustryStandardDescription ='12352204' THEN  'Enzymes'
ELSE CASE WHEN IndustryStandardDescription ='24112110' THEN  'Intermediate bulk containers'
ELSE CASE WHEN IndustryStandardDescription ='24121502' THEN  'Packaging pouches or bags'
ELSE CASE WHEN IndustryStandardDescription ='24121503' THEN  'Packaging boxes'
ELSE CASE WHEN IndustryStandardDescription ='24122000' THEN  'Glass Bottles / Vials'
ELSE CASE WHEN IndustryStandardDescription ='24122002' THEN  'Plastic Bottles'
ELSE CASE WHEN IndustryStandardDescription ='24122003' THEN  'Glass bottles'
ELSE CASE WHEN IndustryStandardDescription ='24122006' THEN  'Applicators'
ELSE CASE WHEN IndustryStandardDescription ='24141514' THEN  'Packaging films'
ELSE CASE WHEN IndustryStandardDescription ='30102006' THEN  'Aluminum foil'
ELSE CASE WHEN IndustryStandardDescription ='31251500' THEN  'Actuators '
ELSE CASE WHEN IndustryStandardDescription ='40161500' THEN  ' FILTERS'
ELSE CASE WHEN IndustryStandardDescription ='40182200' THEN  'Seamless Aluminum Tube'
ELSE CASE WHEN IndustryStandardDescription ='41116155' THEN  'Molecular biology and cell culture growth media'
ELSE CASE WHEN IndustryStandardDescription ='41121821' THEN  'Vial closure cap or seal or stopper'
ELSE CASE WHEN IndustryStandardDescription ='41123400' THEN  ' DOSING DEVICES'
ELSE CASE WHEN IndustryStandardDescription ='42121500' THEN  ' VETERINARY EQUIPMENT'
ELSE CASE WHEN IndustryStandardDescription ='42281516' THEN  'Sterilization Filters'
ELSE CASE WHEN IndustryStandardDescription ='50131600' THEN  ' EGGS AND EGG SUBSTITUTES'
ELSE CASE WHEN IndustryStandardDescription ='73101700' THEN  'PHARMACEUTICAL PRODUCTION'
ELSE CASE WHEN IndustryStandardDescription ='API0001' THEN  'APIs and Intermediates'
ELSE CASE WHEN IndustryStandardDescription ='API0002' THEN  'Antigen'
ELSE CASE WHEN IndustryStandardDescription ='CMOBIDI1' THEN  'BIO PRODUCTION'
ELSE CASE WHEN IndustryStandardDescription ='CMOBINS1' THEN  'Bio Non Sterile Formulation/Filling of Liquid vials'
ELSE CASE WHEN IndustryStandardDescription ='CMOBINS2' THEN  'Bio Non Sterile Formulation filling injectables'
ELSE CASE WHEN IndustryStandardDescription ='CMOBIPK1' THEN  'Bio Contract Packaging Services'
ELSE CASE WHEN IndustryStandardDescription ='CMOBIST1' THEN  'Bio Sterile Formulation/Filling of Liquid vials'
ELSE CASE WHEN IndustryStandardDescription ='CMOBIST2' THEN  'Bio Sterile Formulation filling injectables'
ELSE CASE WHEN IndustryStandardDescription ='CMOBITM1' THEN  'MANUFACTURING SUPPORT SERVICES BIO'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHA01' THEN  'Pharma MANUFACTURING SUPPORT SERVICES'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHNS1' THEN  'Tablet Manufacturing'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHNS2' THEN  'Lotions, Ointments, Creams Manufacturing'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHNS3' THEN  'Containment tablet Manufacturing'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHNS4' THEN  'Non-sterile - Specialty Manufacturing'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHNS5' THEN  'Liquid Manufacturing'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHPK1' THEN  'Pharma Contract Packaging Services'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHST1' THEN  'Pharma Formulation/Filling of Liquid vials'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHST2' THEN  'Formulation/Filling into Blow Fill Seal containers'
ELSE CASE WHEN IndustryStandardDescription ='CMOPHST3' THEN  'Sterile - Specialty Manufacturing'
ELSE CASE WHEN IndustryStandardDescription ='DICACA01' THEN  'Cleaning Agents'
ELSE CASE WHEN IndustryStandardDescription ='DIPASP01' THEN  'Insert'
ELSE CASE WHEN IndustryStandardDescription ='DIPCCRB1' THEN  'Roller Bottles'
ELSE CASE WHEN IndustryStandardDescription ='DIRMOC01' THEN  'Other Chemicals'
ELSE CASE WHEN IndustryStandardDescription ='DIRMREA1' THEN  'Reagents'
ELSE CASE WHEN IndustryStandardDescription ='DIRMSU01' THEN  'Sugars'
ELSE CASE WHEN IndustryStandardDescription ='EgBi011' THEN  'SPF Birds'
ELSE CASE WHEN IndustryStandardDescription ='EgBi021' THEN  'Duck Eggs'
ELSE CASE WHEN IndustryStandardDescription ='EgBi022' THEN  'SPF Eggs'
ELSE CASE WHEN IndustryStandardDescription ='EgBi023' THEN  'Commercial Eggs'
ELSE CASE WHEN IndustryStandardDescription ='EgBi031' THEN  'Liver cells'
ELSE CASE WHEN IndustryStandardDescription ='EgBi032' THEN  'Bursa Cells'
ELSE CASE WHEN IndustryStandardDescription ='EgBi033' THEN  'CEF'
ELSE CASE WHEN IndustryStandardDescription ='EgBi034' THEN  'Other'
ELSE CASE WHEN IndustryStandardDescription ='InAd004' THEN  'Adjuvant'
ELSE CASE WHEN IndustryStandardDescription ='INCCM001' THEN  'Read-to-Use Media'
ELSE CASE WHEN IndustryStandardDescription ='INCCM002' THEN  'Cell_Culture_Media_Components'
ELSE CASE WHEN IndustryStandardDescription ='INEX003' THEN  'Excipients'
ELSE CASE WHEN IndustryStandardDescription ='InPR001' THEN  'Purification Resins'
ELSE CASE WHEN IndustryStandardDescription ='INPR002' THEN  'Beads'
ELSE CASE WHEN IndustryStandardDescription ='InRe001' THEN  'Reagents'
ELSE CASE WHEN IndustryStandardDescription ='INSE0001' THEN  'Fetal Bovine Serum'
ELSE CASE WHEN IndustryStandardDescription ='INSE0002' THEN  'Other Serum'
ELSE CASE WHEN IndustryStandardDescription ='InSp001' THEN  'Specialty Chemicals'
ELSE CASE WHEN IndustryStandardDescription ='PACKPP01' THEN  'Plastic Bottles / Vials'
ELSE CASE WHEN IndustryStandardDescription ='PACKPP02' THEN  'Caps or tops'
ELSE CASE WHEN IndustryStandardDescription ='PACKSP01' THEN  'Labels.'
ELSE CASE WHEN IndustryStandardDescription ='PaPr001' THEN  'Syringes / Injectors / Droppers'
ELSE CASE WHEN IndustryStandardDescription ='PaPr002' THEN  'Media Bottles / Vials'
ELSE CASE WHEN IndustryStandardDescription ='PaPr003' THEN  'Rubber Stoppers'
ELSE CASE WHEN IndustryStandardDescription ='PaPr004' THEN  'Cartrige / Adaptor / Accessory'
ELSE CASE WHEN IndustryStandardDescription ='PaSe001' THEN  'Tray / collation carton'
ELSE CASE WHEN IndustryStandardDescription ='PaSe002' THEN  'Label / Leaflet combination'
ELSE CASE WHEN IndustryStandardDescription ='PaTe001' THEN  'Shipper / Corrugated'
ELSE CASE WHEN IndustryStandardDescription ='PCCBC001' THEN  'Single Use Bioreactor'
ELSE CASE WHEN IndustryStandardDescription ='PCCBC002' THEN  'Bioprocess Container'
ELSE CASE WHEN IndustryStandardDescription ='PCCM0001' THEN  'PCC Miscellaneous'
ELSE CASE WHEN IndustryStandardDescription ='PCCM0002' THEN  'Cleaning Agent'
ELSE CASE WHEN IndustryStandardDescription ='PRCCC02' THEN  'PCC Miscellaneous'
ELSE CASE WHEN IndustryStandardDescription ='PRDCC01' THEN  'Sterile Assemblies'
ELSE CASE WHEN IndustryStandardDescription ='PRDCC02' THEN  'Tubing'
ELSE CASE WHEN IndustryStandardDescription ='PRDCC03' THEN  'Connectors'
ELSE CASE WHEN IndustryStandardDescription ='RMGAS001' THEN  'Gases'
ELSE CASE WHEN IndustryStandardDescription ='RMVIAM01' THEN  'Vitamin, Amino Acids'
ELSE 'Undefined'
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
end

as tax_level1
from  fact_accountspayable f, dim_materialgroup d, dim_part dp
where f.dim_materialgroupid = d.dim_materialgroupid
and f.dim_popartid = dp.dim_partid) t
on t.fact_accountspayableid = f.fact_accountspayableid
when matched then update
set dd_gsm_taxonomy_level1 = t.tax_level1
*/

/*
merge into fact_accountspayable f
using(

Select distinct f.fact_accountspayableid,
-- dm.materialgroupcode, dp.industrystandarddescription,c1.description MGC,c2.description IND,

COALESCE(case when industrystandarddescription = c2.val then c2.description END,
		case when materialgroupcode = c1.val then c1.description  END, 'Undefined'      
) as tax_level1

from  fact_accountspayable f
inner join dim_materialgroup dm on  f.dim_materialgroupid = dm.dim_materialgroupid
Inner join dim_part dp on f.dim_popartid = dp.dim_partid
LEFT join tmp_level1 c1 on c1.val = dm.materialgroupcode
LEFT join tmp_level1 c2 on c2.val = dp.industrystandarddescription
) t
on t.fact_accountspayableid = f.fact_accountspayableid
when matched then update
set dd_gsm_taxonomy_level1 = t.tax_level1
*/





/*GSM TAXONOMY LEVEL 2*/

/*
merge into fact_accountspayable f
using(
select distinct fact_accountspayableid, 
CASE WHEN MaterialGroupCode in('PREC002', 'PREC003') 
THEN 'Analytical_Testing'

ELSE CASE WHEN MaterialGroupCode in('85131503', '85131701') 
THEN 'Animal_Studies_and_Tests'

ELSE CASE WHEN MaterialGroupCode in('10111301', '10120000', '10130000', '10131601', '70000000', '70120000', '70122000', '70130000') 
THEN 'Animal_Supplies'

ELSE CASE WHEN MaterialGroupCode in('10000000', '10100000') 
THEN 'Animals'

ELSE CASE WHEN MaterialGroupCode in('23150000', '23151804', '23151807', '23152100', '23152104', '32101522', '41104903') 
THEN 'Biological_Equipment'

ELSE CASE WHEN MaterialGroupCode in('70122010', '73151600', '80000000', '80101500', '80101504', '80101508', '80101702', '80120000', '80141504', '80141507', '80141607', '80141703', '81000000', '81111819', '82110000', '82111503', '82111804', '82121509', '84000000', '84111600', '84121607', '84140010', '86141704', '94000000', 'GOVT', 'SPEAKER') 
THEN 'Business_Services'

ELSE CASE WHEN MaterialGroupCode in('72000001', '73000000') 
THEN 'Civil_Maintenance_and_Repair'

ELSE CASE WHEN MaterialGroupCode in('56111700', '72130000', '81100000') 
THEN 'Construction'

ELSE CASE WHEN MaterialGroupCode in('12140000', '12142100', '15100000', '15101500', '15101600', '15101700', '15111500' , '24111802', '39121000', '47100000', '47101500', '47101600', '71000000', '83000000', '83100000', '83101500', '83101501', '83101601', '83101602', '83101800', '83101801') 
THEN 'Energy_and_Utilities'

ELSE CASE WHEN MaterialGroupCode in('80141900', '80141901') 
THEN 'Exhibitions'
ELSE CASE WHEN MaterialGroupCode in('15101506', '25101503', '78111809', '80161505') THEN 'Fleet'

ELSE CASE WHEN MaterialGroupCode in('23000000', '23151819', '23152900', '23270000', '23271400', '41111600', '42281500', '42281502', '42281508', '48000000') 
THEN 'General'

ELSE CASE WHEN MaterialGroupCode in('80110000', '80111507', '80111600', '80111700', '80111707', '84111505', '84130000', '85100000', '85101700', '86000000', '86120000', '86121700', '91000000', '93141802', 'EMP') 
THEN 'HR_Services'

ELSE CASE WHEN MaterialGroupCode in('31160000', '39121300', '39121500', '39121600', '43000000', '43200000', '43201409', '43210000', '43211500', '43211501', '43211503', '43211507', '43211512', '43211600', '43211902', '43212100', '43220000', '43223108', '44101503', '44101700', '44103100', '45000000', '45111900', '52161600', '56112000') 
THEN 'ICT_Hardware'

ELSE CASE WHEN MaterialGroupCode in('26121616', '72102205', '80101507', '81111509', '81111811', '81111812', '81112002', '81112100', '81112106', '81160000', '82000000') 
THEN 'ICT_Professional_Services'

ELSE CASE WHEN MaterialGroupCode in('43230000', '43231100', '43231200', '43231270', '43231280', '43233202', '81112200', '82150002') 
THEN 'ICT_Software'

ELSE CASE WHEN MaterialGroupCode in('81161700') 
THEN 'ICT_Telecom_Services'

ELSE CASE WHEN MaterialGroupCode in('10160000', '24101601', '30181500', '31210000', '47000000', '47130000', '47131800', '50000000', '53000000', '53100000', '70111601', '72100000', '72101901', '72102100', '72102400', '72102702', '72102900', '72102901', '72102902', '72131600', '76000000', '76110000', '76111500', '76111504', '76111506', '76111604', '78102201', '78102203', '81101513', '81141800', '90101603', '90101700', '91111502', 'NDF99IFM') 
THEN 'IFM'

ELSE CASE WHEN MaterialGroupCode in('12000000', '12352106', '13000000', '13111000', '41116000', 'CTRLSUB', 'STOXINS')
 THEN 'Lab_Chemicals'

ELSE CASE WHEN MaterialGroupCode in('20121904', '21101906', '23181803', '41000000', '41100000', '41101518', '41102400', '41102600', '41103000', '41103021', '41103023', '41103200', '41103207', '41103300', '41103502', '41103504', '41103700', '41103715', '41103800', '41103801', '41103900', '41104200', '41104300', '41104400', '41104500', '41104700', '41104803', '41104900', '41105100', '41105101', '41105300', '41111500', '41111502', '41111700', '41111740', '41113037', '41113100', '41115300', '41115600', '41115700', '41115707', '41115800', '41121500', '41122000', '45101900')
 THEN 'Lab_Equipment'

ELSE CASE WHEN MaterialGroupCode in('56122000') 
THEN 'Lab_Furniture'

ELSE CASE WHEN MaterialGroupCode in('81101706') 
THEN 'Lab_Maintenance'

ELSE CASE WHEN MaterialGroupCode in('31231300', '41101500', '41103022', '41103206', '41112500', '41115604', '41116100', '41116128', '41120000', '41121800', '41122003', '41122502', '41122800' , '42142600', 'NDF99LSU') 
THEN 'Lab_Supplies'

ELSE CASE WHEN MaterialGroupCode in('30222309', '73101701', '73101702', '73161510', '85121800', '85130000', '85131708')
 THEN 'Level 2 CRO'

ELSE CASE WHEN MaterialGroupCode in('55000000', '80141500', '80141509', '82150006') 
THEN 'Market_Research'

ELSE CASE WHEN MaterialGroupCode in('24141704', '42120000', '42140000', '45130000', '80141501', '80141600', '80141601', '80141604', '80141605', '80141606', '80141611', '80141612', '80141614', '80141618', '80141701', '80141800', '82100000', '82101500', '82101503', '82101505', '82101602', '82101603', '82101800', '82101801', '82101900', '82111800', '82121500', '82121503', '82121504', '82121505', '82121801', '82130000', '82131603', '82140000', '82141502', '82141506', '82150001', '82150004', '82150007', '83121700', 'Mktg0001') 
THEN 'Marketing Services'

ELSE CASE WHEN MaterialGroupCode in('20000000', '23152907', '23153100', '24111500', '24111800', '26000000', '26101100', '26101200', '26110000', '26121600', '27110000', '27112700', '30180000', '31000000', '31162702', '31180000', '31181506', '32000000', '39000000', '39101600', '39120000', '39121400', '39121700', '39121900', '40141600', '40141604', '40141609', '40142300', '40142500', '40142600', '40151601', '40151700', '40161505', '72102300', '72102307', '73152100', '73152101', '15120000', '15121500', '15121520') 
THEN 'MRO'
ELSE CASE WHEN MaterialGroupCode in('14000000', '14111500', '14111604', '31201500', '43191502', '44000000', '44100000', '44101501', '44102605', '44120000', '44121700', '54000000', '56000000', '73161515', '82121507', '82121508') 
THEN 'Office_Supplies'

ELSE CASE WHEN MaterialGroupCode in('42132200', '46180000', '46181600', '46181605', '46181700', '46181800', '46181900', '46182000', '46182200')
 THEN 'Personal_Protection_Materials'

ELSE CASE WHEN MaterialGroupCode in('23151800', '41101504') 
THEN 'Pharmaceutical_Equipment'

ELSE CASE WHEN MaterialGroupCode in('23151810') 
THEN 'Primary_Packaging'

ELSE CASE WHEN MaterialGroupCode in('31201600', '41123003', '51000000', '73101604', '73150000' , 'NDF99PRC') 
THEN 'Prod_Chem_and_Mfg_Svcs'

ELSE CASE WHEN MaterialGroupCode in('24112100', '24112505', '24120000', '24140000', '55121600', '55121609') 
THEN 'Prod_Packaging'

ELSE CASE WHEN MaterialGroupCode in('80131500') 
THEN 'Real_Estate'

ELSE CASE WHEN MaterialGroupCode in('46181500')
 THEN 'Safety_Materials'

ELSE CASE WHEN MaterialGroupCode in('24102107', '44102400') 
THEN 'Secondary_Packaging'

ELSE CASE WHEN MaterialGroupCode in('42172001', '46000000', '46190000', '92000000', '92121504') 
THEN 'Security'

ELSE CASE WHEN MaterialGroupCode in('32150000', '42000000', '48100000', '52000000', '55120000', '60000000', '76101500', '83111603', '30000000', '30100000', '30110000', '30140000', '30161503', '40000000', '40100000', '40101800', '45110000', '52130000', '55121700', '72000000', '72102200', '72102302', '78101804', '80161507', '80161602', '81101703', '00HE00000') THEN 'Site_Services'
ELSE CASE WHEN MaterialGroupCode in('23151814', '27000000', '40101600', '40150000', '40151500', '40160000', '41110000', '41111900', '41112100', '41112200', '41112201', '41112400') 
THEN 'Small_Equipment'

ELSE CASE WHEN MaterialGroupCode in('78000000', '78100000', '78101500', '78101600', '78101700', '78101800', '78101900', '78102200', '78141500', '78141502', '81141601') 
THEN 'Transportation'

ELSE CASE WHEN MaterialGroupCode in('25000000', '25101506', '78111502', '78111808', '80141609', '80141902', '80161502', '85000000', '90000000', '90110000', '90121502')
 THEN 'Travel_Cards_and_Meetings'

ELSE CASE WHEN MaterialGroupCode in('24000000', '24100000', '24102004', '24110000', '24112500', '26111700', '49000000', '78131800', '80141619', '80141700')
 THEN 'warehouse_and_distribution'

ELSE CASE WHEN MaterialGroupCode in('47121700', '76120000', '76121600', '76121900', '76122300', '77000000', '77100000')
 THEN 'Waste'

ELSE CASE WHEN MaterialGroupCode in('NDF99IND') 
THEN '99_NotDefined_-_Indirect'

ELSE CASE WHEN MaterialGroupCode in('NDF99LSE')
THEN '99_NotDefined_-_Lab_Supplies_and_Equipment'

ELSE CASE WHEN MaterialGroupCode in('NDF99Mkt')
THEN '99_Not_Defined_-_Mktg_and_Advertising'

ELSE CASE WHEN MaterialGroupCode in('NDF99Pre')
THEN '99_Not_Defined_-_Profressional_Services'

ELSE CASE WHEN  IndustryStandardDescription IN ('API0002')
THEN 'ABI'

ELSE CASE WHEN IndustryStandardDescription IN ('InAd004')
THEN 'Adjuvant'

ELSE CASE WHEN IndustryStandardDescription IN ('API0001')
THEN 'API_and_Intermediates'

ELSE CASE WHEN IndustryStandardDescription IN  ('NDF99API')
THEN ' APIs_and_IntermediateS'

ELSE CASE WHEN  IndustryStandardDescription IN  ('24122006' , 'NDF99API' )
THEN 'Applicator'

ELSE CASE WHEN IndustryStandardDescription IN  ('CMOBIPK1')
THEN 'Bio_Contract_Packaging'

ELSE CASE WHEN IndustryStandardDescription IN  ('CMOBIDI1')
THEN'Bio_Distribution_Contract'

ELSE CASE WHEN IndustryStandardDescription IN ('CMOBINS1' , 'CMOBINS2')
THEN 'Bio_Non_Sterile'

ELSE CASE WHEN IndustryStandardDescription IN ('CMOBIST1' , 'CMOBIST2' , 'NDF99BST')
THEN 'Bio_Sterile'

ELSE CASE WHEN IndustryStandardDescription IN ('CMOBITM1')
THEN 'Bio_Toll_Manufacturing'

ELSE CASE WHEN IndustryStandardDescription IN ('PCCBC001' , 'PCCBC002')
THEN 'Bioprocess_Containers'

ELSE CASE WHEN IndustryStandardDescription IN ('EgBi011')
THEN 'Birds'

ELSE CASE WHEN IndustryStandardDescription IN ('12161700')
THEN ' Buffer_Solutions'

ELSE CASE WHEN IndustryStandardDescription IN ('NDF99CCM' )
THEN 'Cell_Culture_Media'

ELSE CASE WHEN IndustryStandardDescription IN ('INCCM002')
THEN ' Cell_Culture_Media_Components'

ELSE CASE WHEN IndustryStandardDescription IN ('DICACA01')
THEN ' Cleaning_Agents_Level_2'

ELSE CASE WHEN IndustryStandardDescription IN ('12170000')
THEN 'Colorants'

ELSE CASE WHEN IndustryStandardDescription IN ('31251500' , '41123400')
THEN 'Devices'

ELSE CASE WHEN IndustryStandardDescription IN ('50131600' , 'EgBi021' , 'EgBi022' , 'EgBi023')
THEN 'Eggs'

ELSE CASE WHEN IndustryStandardDescription IN ( 'EgBi031' , 'EgBi032' , 'EgBi033' , 'EgBi034')
THEN 'Embryo_Derived_Product'

ELSE CASE WHEN IndustryStandardDescription IN ('12352204')
THEN 'Enzymes'

ELSE CASE WHEN IndustryStandardDescription IN ('INEX003' , 'NDF99EXC')
THEN 'Excipients'

ELSE CASE WHEN IndustryStandardDescription IN ('NDF99RMG' , 'RMGAS001')
THEN 'Gases'

ELSE CASE WHEN IndustryStandardDescription IN ('12352200' , '41116155')
THEN 'Growth_Media_or_Sera'

ELSE CASE WHEN IndustryStandardDescription IN ('42121500' , 'NDF99MDS')
THEN 'Medical_Devices_Marketing_Services'

ELSE CASE WHEN IndustryStandardDescription IN ('DIRMOC01' , 'NDF99OCH')
THEN ' Other_Chemicals'

ELSE CASE WHEN IndustryStandardDescription IN ('PRCCC02' , 'PCCM0001' , 'PCCM0002')
THEN 'PCC_Miscellaneous'

ELSE CASE WHEN IndustryStandardDescription IN ('CMOPHPK1')
THEN 'Pharma_Contract_Packing_Services'

ELSE CASE WHEN IndustryStandardDescription IN ('73101700' , 'NDF99PHD')
THEN 'Pharma_Distribution_Contract'

ELSE CASE WHEN IndustryStandardDescription IN ('CMOPHNS1' , 'CMOPHNS2' , 'CMOPHNS3' , 'CMOPHNS4' , 'CMOPHNS5')
THEN 'Pharma_Non_sterile'

ELSE CASE WHEN IndustryStandardDescription IN ('CMOPHST1' , 'CMOPHST2' , 'CMOPHST3')
THEN 'Pharma_Sterile' 

ELSE CASE WHEN IndustryStandardDescription IN ('CMOPHA01')
THEN 'Pharma_Toll_Manufacturing'

ELSE CASE WHEN IndustryStandardDescription IN ('11101800')
THEN 'Precious Metals'

ELSE CASE WHEN IndustryStandardDescription IN ('24112110' , '24121502' , '24122000' , '24122002' , '24122003' , '24141514' , '30102006', '41121821' , 'PaPr001' , 'PaPr002' , 'PaPr003' , 'PaPr004' , 'PACKPP01' , 'PACKPP02' , '40182200' , 'NDF99PPP')
THEN 'Primary_Packaging'

ELSE CASE WHEN IndustryStandardDescription IN ('40161500' , '42281516')
THEN 'Process_Filters' 

ELSE CASE WHEN IndustryStandardDescription IN ('INPR002' , 'InPR001')
THEN 'Purification_Resins_and_Beads'

ELSE CASE WHEN IndustryStandardDescription IN ('INCCM001')
THEN 'Ready_to_use_media'

ELSE CASE WHEN IndustryStandardDescription IN ('InRe001' , 'DIRMREA1' , 'NDF99REA')
THEN 'Reagents'

ELSE CASE WHEN IndustryStandardDescription IN ('DIPCCRB1')
THEN 'Roller_Bottles'

ELSE CASE WHEN IndustryStandardDescription IN ('24121503' , 'PaSe001' , 'PaSe002' , 'PACKSP01' , 'DIPCCRB1' ,  'NDF99PSE')
THEN 'Secondary_Packaging'

ELSE CASE WHEN IndustryStandardDescription IN ('INSE0001' , 'INSE0002')
THEN 'Sera'

ELSE CASE WHEN IndustryStandardDescription IN ('12160000' , '12190000')
THEN 'Solvents'

ELSE CASE WHEN IndustryStandardDescription IN ('InSp001')
THEN 'Specialty_Chemicals'

ELSE CASE WHEN IndustryStandardDescription IN ('PRDCC01' , 'PRDCC02' , 'PRDCC03')
THEN 'Sterile_Assemblies_Tubing_Connectors'

ELSE CASE WHEN IndustryStandardDescription IN ('DIRMSU01')
THEN 'Sugars'

ELSE CASE WHEN IndustryStandardDescription IN ('PaTe001' , 'NDF99PTE')
THEN 'Tertiary_Packaging'

ELSE CASE WHEN IndustryStandardDescription IN ('RMVIAM01')
THEN 'Vitamin_Amino_Acids'

ELSE CASE WHEN IndustryStandardDescription IN ('NDF99DIR')
THEN '99_NotDefined_-_Direct'

ELSE CASE WHEN IndustryStandardDescription IN ('NDF99CMO')
THEN '99_NotDefined_-_Contract_Manufacturing'

ELSE CASE WHEN IndustryStandardDescription IN ('NDF99MDE')
THEN '99_NotDefined_-_Medical_Devices'

ELSE CASE WHEN IndustryStandardDescription IN ('NDF99PAC')
THEN '99_NotDefined_-_Packaging'

ELSE CASE WHEN IndustryStandardDescription IN ('NDF99PCC')
THEN '99_NotDefined_-_Product_Contact_Component'

ELSE CASE WHEN IndustryStandardDescription IN ('NDF99RAM')
THEN '99_NotDefined_-_Raw_Materials'
ELSE 'Undefined'

END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
END
as tax_level2
from  fact_accountspayable f, dim_materialgroup d, dim_part dp
where f.dim_materialgroupid = d.dim_materialgroupid
and f.dim_popartid = dp.dim_partid) t
on t.fact_accountspayableid = f.fact_accountspayableid
when matched then update
set dd_gsm_taxonomy_level2 = t.tax_level2
*/

/*
merge into fact_accountspayable f
using(

Select distinct  f.fact_accountspayableid, 
 --dm.materialgroupcode, dp.industrystandarddescription,c1.description MGC,c2.description IND,

COALESCE(case when industrystandarddescription = c2.val then c2.description END,
		case when materialgroupcode = c1.val then c1.description  END, 'Undefined'      
) as tax_level2


from  fact_accountspayable f
inner join dim_materialgroup dm on  f.dim_materialgroupid = dm.dim_materialgroupid
Inner join dim_part dp on f.dim_popartid = dp.dim_partid
LEFT join tmp_level2 c1 on c1.val = dm.materialgroupcode
LEFT join tmp_level2 c2 on c2.val = dp.industrystandarddescription
) t
on t.fact_accountspayableid = f.fact_accountspayableid
when matched then update
set dd_gsm_taxonomy_level2 = t.tax_level2

*/






/*GSM TAXONOMY LEVEL 3*/
/*
merge into fact_accountspayable f
using(
	select distinct fact_accountspayableid, 
CASE WHEN MaterialGroupCode in ('PREC002', 'PREC003')	
THEN 'Analytical_Services'														
ELSE CASE WHEN MaterialGroupCode in ('10000000', '10100000', '10111301', '10120000', '10130000', '10131601', '70000000', '70120000', '70122000', '70130000')
THEN 'Animals_and_Supplies'																						
ELSE CASE WHEN MaterialGroupCode in ('56111700', '72130000', '81100000')
THEN 'Construction'												
ELSE CASE WHEN MaterialGroupCode in ('85131503', '85131701', '30222309', '73101701', '73101702', '73161510', '85121800', '85130000', '85131708')			
THEN 'CRO'		
										
ELSE CASE WHEN MaterialGroupCode in ('10160000', '24101601', '30181500', '31210000', '47000000', '47130000', '47131800', '50000000', '53000000', '53100000', '70111601', '72100000', '72101901', '72102100', '72102400', '72102702', '72102900', '72102901', '72102902', '72131600', '76000000', '76110000', '76111500', '76111504', '76111506', '78102201', '78102203', '81101513', '81141800', '90101603', '90101700', '91111502', '42172001', '46000000', '46190000' , '92000000', '92121504', '47121700', '76120000', '76121600', '76121900', '76122300', '77000000', '77100000', '14000000', '14111500', '54000000', '56000000', '73161515', '82121507', '82121508', '80131500', '32150000', '42000000', '48100000', '52000000', '55120000', '60000000', '76101500', '83111603', '12142100', '15100000', '15101500', '15101600', '15101700', '15111500',  '24111802', '39121000', '47100000', '47101500', '47101600', '12140000', '14111604', '31201500', '43191502', '44000000', '44100000', '44101501', '44102605', '44120000', '44121700', '71000000', '76111604', '80131500' , '83000000', '83100000', '83101501', '83101601', '83101602', '83101800', '83101801', 'NDF99IFM', '83101500')									
THEN 'Facility_Management'		
								
ELSE CASE WHEN MaterialGroupCode in ('15101506','25101503','78111809','80161505')							
THEN 'Fleet'												

ELSE CASE WHEN MaterialGroupCode in ('78000000', '78100000', '78101500', '78101600', '78101700', '78101800', '78101900', '78102200', '78141500', '78141502', '81141601', '24000000', '24100000', '24102004', '24110000', '24112500', '26111700', '49000000', '78131800', '80141619', '80141700')								
THEN 'Integrated_Logistics'	
									
ELSE CASE WHEN MaterialGroupCode in ('12000000', '12352106', '13000000', '13111000', '41116000', 'CTRLSUB', 'STOXINS', '20121904', '21101906', '23181803', '41000000', '41100000', '41101518', '41102400', '41102600', '41103000', '41103021', '41103023', '41103200', '41103207', '41103300', '41103502', '41103504', '41103700', '41103715', '41103800', '41103801', '41103900', '41104200', '41104300', '41104400', '41104500', '41104700', '41104803', '41104900', '41105100', '41105101', '41105300', '41111500', '41111502', '41111700', '41111740', '41113037', '41113100', '41115300', '41115600', '41115700', '41115707', '41115800', '41121500', '41122000', '45101900', '56122000', '81101706', '31231300', '41101500', '41103022', '41103206', '41112500', '41115604', '41116100', '41116128', '41120000', '41121800', '41122003', '41122502', '41122800', '42142600', '42132200', '46180000', '46181600', '46181605', '46181700', '46181800', '46181900', '46182000', '46182200', '46181500', 'NDF99LSE', 'NDF99LSU')			
THEN 'Lab_Supplies_and_Equipment'									

ELSE CASE WHEN MaterialGroupCode in ('23000000', '23151819', '23152900', '23270000', '23271400', '41111600', '42281500', '42281502', '42281508', '48000000', '23150000', '23151804', '23151807', '23152100', '23152104', '32101522', '41104903', '23151800', '41101504', '23151814', '27000000', '40101600', '40150000', '40151500', '40160000', '41110000', '41111900', '41112100', '41112200', '41112201', '41112400', '72000001', '73000000', '23151810', '24102107', '44102400')									
THEN 'Machines_and_Equipment'																						
ELSE CASE WHEN MaterialGroupCode in ('20000000', '23152907', '23153100', '24111500', '24111800', '26000000', '26101100', '26101200', '26110000', '26121600', '27110000', '27112700', '30180000', '31000000', '31162702', '31180000', '31181506', '32000000', '39000000', '39101600', '39120000', '39121400', '39121700', '39121900', '40141600', '40141604', '40141609', '40142300', '40142500', '40142600', '40151601', '40151700', '40161505', '72102300', '72102307', '73152100', '73152101', '30000000', '30100000', '30110000', '30140000', '30161503', '40000000', '40100000', '40101800', '45110000', '52130000', '55121700', '72000000', '72102200', '72102302', '78101804', '80161507', '80161602', '81101703', '00HE00000', '15120000', '15121500', '15121520')	
THEN 'Maintenance'																							
ELSE CASE WHEN MaterialGroupCode in ('80141900', '80141901', '24141704', '55000000', '80141500', '80141509', '82150006', '42120000', '42140000', '45130000', '80141501', '80141600', '80141601', '80141604', '80141605', '80141606', '80141611', '80141612', '80141614', '80141618', '80141701', '80141800', '82100000', '82101500', '82101503', '82101505', '82101602', '82101603', '82101800', '82101801', '82101900', '82111800', '82121500', '82121503', '82121504', '82121505', '82121801', '82130000', '82131603', '82140000', '82141502', '82141506', '82150001', '82150004', '82150007', '83121700','Mktg0001' , 'NDF99Mkt')			
THEN 'Mktg_and_Advertising'																							
ELSE CASE WHEN MaterialGroupCode in ('31201600', '41123003', '51000000', '73101604', '73150000' , 'NDF99PRC')						
THEN 'Prod_Chem_and_Mfg_Svcs'																						
ELSE CASE WHEN MaterialGroupCode in ('24112100', '24112505', '24120000', '24140000', '55121600','55121609')								
THEN 'Prod_Packaging'	
										
ELSE CASE WHEN MaterialGroupCode in ('70122010', '73151600', '80000000', '80101500', '80101504', '80101508', '80101702', '80120000', '80141504', '80141507', '80141607', '80141703', '81000000', '81111819', '82110000', '82111503', '82111804', '82121509', '84000000', '84111600', '84121607', '84140010', '86141704', '94000000', 'GOVT', 'SPEAKER', '80110000', '80111507', '80111600', '80111700', '80111707', '84111505', '84130000', '85100000', '85101700', '86000000', '86120000', '86121700', '91000000', '93141802', 'EMP', 'NDF99Pre')	
THEN 'Professional_Services'										
										
ELSE CASE WHEN MaterialGroupCode in ('31160000', '39121300', '39121500', '39121600', '43000000', '43200000', '43201409', '43210000', '43211500', '43211501', '43211503', '43211507', '43211512', '43211600', '43211902', '43212100', '43220000', '43223108', '44101503', '44101700', '44103100', '45000000', '45111900', '52161600', '56112000', '26121616', '72102205', '80101507', '81111509', '81111811', '81111812', '81112002', '81112100', '81112106', '81160000', '82000000', '43230000', '43231100', '43231200', '43231270', '43231280', '43233202', '81112200', '82150002', '81161700')								
THEN 'Software_Hardware_and_Services'																					
ELSE CASE WHEN MaterialGroupCode in ('25000000', '25101506', '78111502', '78111808', '80141609', '80141902', '80161502', '85000000', '90000000', '90110000','90121502')
THEN 'Travel_Cards_and_Meetings'			
						
ELSE CASE WHEN MaterialGroupCode in ('NDF99IND')	
THEN '99_NotDefined_-_Indirect'									

													
ELSE CASE WHEN IndustryStandardDescription  IN ('API0001')					
THEN 'API'		
	
ELSE CASE WHEN IndustryStandardDescription  IN  ('API0002' , 'NDF99API')			
THEN 'API_and_ABI'											

ELSE CASE WHEN IndustryStandardDescription  IN ('DICACA01')				
THEN 'Cleaning_Agents'							
				
ELSE CASE WHEN IndustryStandardDescription  IN ( '73101700', 'CMOPHPK1', 'CMOPHST1', 'CMOPHST2' , 'CMOPHST3', 'CMOPHNS1', 'CMOPHNS2', 'CMOPHNS3', 'CMOPHNS4', 'CMOPHNS5', 'CMOBIST1', 'CMOBIST2', 'CMOBINS1', 'CMOBINS2', 'CMOBIPK1', 'CMOBIDI1', 'CMOBITM1', 'CMOPHA01', 'NDF99CMO', 'NDF99BST' , 'NDF99PHD')						
THEN 'Contract_Manufacturing'											
ELSE CASE WHEN IndustryStandardDescription  IN ('EgBi011', '50131600', 'EgBi021', 'EgBi022', 'EgBi023', 'EgBi031', 'EgBi032', 'EgBi033', 'EgBi034')						
THEN 'Eggs_and_Birds'																							
ELSE CASE WHEN IndustryStandardDescription  IN ('24122006', '31251500', '41123400', '42121500', 'NDF99MDE', 'NDF99APL', 'NDF99MDS')							
THEN 'Medical_Devices'																							
ELSE CASE WHEN IndustryStandardDescription  IN ('24112110', '24121502', '24122000', '24122002', '24122003', '24141514', '30102006', '41121821', 'PaPr001', 'PaPr002', 'PaPr003', 'PaPr004', 'PACKPP01', 'PACKPP02', '40182200', '24121503', 'PaSe001', 'PaSe002', 'PACKSP01' , 'PaTe001' , 'NDF99PAC' , 'NDF99PPP' , 'NDF99PSE' , 'NDF99PTE')							
THEN 'Packaging'		

ELSE CASE WHEN IndustryStandardDescription  IN ('PCCBC001', 'PCCBC002', 'PRCCC02', 'PCCM0001', 'PCCM0002', '40161500', '42281516', 'PRDCC01', 'PRDCC02', 'PRDCC03', 'DIPCCRB1', 'NDF99PCC')
THEN 'ProductContactComponent'						
			
ELSE CASE WHEN IndustryStandardDescription  IN ('11101800', '12160000', '12161700', '12170000', '12190000', '12352200', '12352204', '41116155', 'DIRMOC01', 'DIRMREA1', 'DIRMSU01', 'InAd004', 'INCCM001', 'INCCM002', 'INEX003', 'InPR001', 'INPR002', 'InRe001', 'INSE0001', 'INSE0002', 'InSp001', 'RMGAS001', 'RMVIAM01', 'NDF99RAM', 'NDF99CCM', 'NDF99EXC', 'NDF99OCH', 'NDF99REA', 'NDF99RMG')										
THEN 'Raw_Materials'																							
ELSE CASE WHEN IndustryStandardDescription  IN ('NDF99DIR')				
THEN '99_NotDefined_-_Direct'									
																							
ELSE CASE WHEN DD_GLACCOUNT IN ('0002700001' , '0002701000' , '0002705000' , '0002706000' , '0002845000', '0002899000')	
THEN 'Accounts Payable - Trade'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002004003')	
THEN 'Accounts Receivable - Trade'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002838047' , '0002838048' , '0002899015')	
THEN 'Accrued Capital Taxes'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002838047' , '0002838048' , '0002899015')	
THEN 'Accrued Capital Taxes'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002900000')	
THEN 'Accrued Federal Income Tax Payable'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002838044')	
THEN 'Accrued Real Property Taxes'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002836001' , '0002836006' , '0002836017' , '0002836018' , '0002836019' , '0002837000')	
THEN 'Accrued Sales Tax Payable'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002900004')	
THEN 'Accrued State Income Tax Payable'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002004000')	
THEN 'Allowance for Returns'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004505000' , '0004505003' , '0004505004' , '0004505005' , '0004506000')	
THEN 'Animals_and_Supplies'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002510034' , '0002520033' , '0002530034' , '0002530038' , '0002532001' , '0002532003' , '0002532035')	
THEN 'Citibank Operating Account'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004505000' , '0004505003' , '0004505004' , '0004505005' , '0004506000')	
THEN 'Animals_and_Supplies'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002510034' , '0002520033' , '0002530034' , '0002530038' , '0002532001' , '0002532003' , '0002532035')	
THEN 'Citibank Operating Account'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004830000')	
THEN 'CLINICAL GRANTS - OTHER'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004520017' , '0004903002')	
THEN 'Clinical Mfg Inventory Expense'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004520000', '0009198000')	
THEN 'Construction'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004132000' , '0004507002' , '0004812002')	
THEN 'CRO'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002842000')	
THEN 'Cur  Accrued Commissions'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002829300' , '0002831000' , '0002831029' , '0002831030' , '0002831034' , '0002831035' , '0002831036' , '0002831037' , '0002831038' , '0002831039' , '0002831040' , '0002831041' , '0002831042' , '0002831045' , '0002838008' , '0002899004' , '0002899011' , '0002899014')	
THEN 'Cur Accrued Benefits - Medical'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002828001' , '0002839000')	
THEN 'Cur Accrued Pensions'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002839010')	
THEN 'Cur Accrued Pensions   Defined Contribution'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002834000' , '0002845002' , '0002899003' , '0002899008' , '0002899021')	
THEN 'Current Accrued Moving and Relocation Expenses'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002899007')	
THEN 'Current Accrued Payroll Interface Clearing'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002843000')	
THEN 'Current Accrued Product Liability'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002835000')	
THEN 'Current Accrued Royalties'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004509600')	
THEN 'Deductible Contributions - Cash: Comm Relations'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0007000004')	
THEN 'Direct Cost of Goods Sold'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002201000' , '0002211000' , '0002211003')	
THEN 'Dividends Receivables'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009197000')	
THEN 'F/A Retirement - Gain on Sale/Disposal'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004203000' , '0004204000' , '0004205000' , '0004300004' , '0004300008' , '0004501000' , '0004504004', '0004200000' , '0004201000' , '0004201001' , '0004202000' , '0004206000')	
THEN 'Facility_Management'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004513002')	
THEN 'Fees - Bank/Finance'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004209002' , '0004511000' , '0004511001' , '0004512000')	
THEN 'Fees - Trade Associations'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004855000' , '0004855003')	
THEN 'Fees - Regulatory'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004351000' , '0004351001' , '0004351005' , '0004351007' , '0004352006' , '0004352007' , '0004352008')	
THEN 'Fleet'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009128000' , '0009129000')	
THEN 'Gain/Loss on FX - Realized Gain'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004992013')	
THEN 'Headquarters Functions'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002702000' , '0002703000' , '0002802000')	
THEN 'I/C Payables - MSPJV Conversion Balances'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002102000')	
THEN 'I/C Receivables - MSPJV Conversion Balances'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004000032' , '0004000047')	
THEN 'I/P - Other'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002204000')	
THEN 'Input VAT/GST on Imports'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004253000')	
THEN 'Insurance Expenses - Other'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004251000')	
THEN 'Insurance Premiums - Product Liability'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004250000')	
THEN 'Insurance Premiums - Property'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0001120021')	
THEN 'Intangible Assets - Licenses'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004210001' , '0004300007' , '0004903000' , '0004903001' , '0004903004' , '0004903005' , '0004903006' , '0004903008' , '0004903010' , '0004904000')	
THEN 'Integrated_Logistics'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009301000')	
THEN 'Interest Expense - 3rd Party Loans'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004513000')	
THEN 'Interest Expense - Bank Fees'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0003520000')	
THEN 'Inventory - In Transit'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0003100002')	
THEN 'Inventory - Packaging Components'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0003800000')	
THEN 'Inventory Reserves - Semi-Finished Goods'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004507000' , '0004507001' , '0004507003', '0004507004', '0004507005', '0004522000' , '0004812000')	
	
THEN 'Lab_Supplies_and_Equipment'	

ELSE CASE WHEN DD_GLACCOUNT IN ('0004300000' , '0004300001' , '0004300002' , '0004300005' , '0004300006' , '0004300009' , '0004300011' , '0004300013' , '0004300014' , '0004508000')	
	
THEN 'Maintenance'	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004502000' , '0004520001' , '0009190000' , '0009190002')	
THEN 'Miscellaneous Operating Expenses - 100% Deductible'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004510000' , '0004510001' , '0004510002' , '0004510004' , '0004514000' , '0004514001' , '0004840000' , '0004850000' , '0004850001' , '0004850002' , '0004850003' , '0004851000' , '0004852000' , '0004852001' , '0004853000' , '0004853001' , '0004853002' , '0004856000' , '0004856001' , '0004856003' , '0004856004' , '0004856006' , '0004856007' , '0004856008' , '0004856009' , '0004856011' , '0004856012' , '0004856013' , '0004856014' , '0004856018' , '0004856020' , '0004856023' , '0004856024' , '0004856025' , '0004504006' , '0004509900')	
THEN 'Mktg_and_Advertising'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002839004' , '0001702000' , '0001702010')	
THEN 'N/C Accrued Pension Plans - Salaried'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0001710020')	
THEN 'N/C Accrued Post Retiree Health & Welfare'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004516000')	
THEN 'Other Assessment'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004000049' , '0004000060' , '0004002022' , '0004002024' , '0004002052' , '0004150000' , '0004150001' , '0004151000' , '0004154000' , '0004154004')	
THEN 'Other Benefits - Company Sponsored'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009190013')	
THEN 'Other Expense - External Reporting'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0001306000')	
THEN 'Other N/C Expsd - Deferred Pension Expenses'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002209000' , '0002209005')	
THEN 'Other Receivables - Employee Receivables'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009135002')	
THEN 'Other Than Income Taxes - Franchise & License'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004154009' , '0004209001')	
THEN 'Other Than Income Taxes - Other'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004209000')	
THEN 'Other Than Income Taxes - Property'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004992011')	
THEN 'Other Unallocated Overhead'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004504002')	
THEN 'Outside Services - Audit Fees - Global'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004992011')	
THEN 'Other Unallocated Overhead'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004504008')	
THEN 'Outside Services - Audit Fees - Local'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004131000' , '0004154003' , '0004207000' , '0004207002' , '0004207003' , '0004300003' , '0004520005' , '0004520018' , '0004856022' , '0004900000' , '0004906001' , '0004906003' , '0004906004')	
THEN 'Outside Services - Other'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004155001')	
THEN 'Outside Services - Temporaries, Project-Based'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002838001' , '0002838002' , '0002838020' , '0002838025' , '0002838049' , '0002838051' , '0002838053' , '0002838065')	
THEN 'Payroll Ded - Social Security Employer'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004002013')	
THEN 'Payroll Taxes - FUTA Tax Expense'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004002001' , '0004002036' , '0004002044' , '0004002053' , '0004520003')	
THEN 'Payroll Taxes - Social Security'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004001000' , '0004001002')	
THEN 'Pension Costs-Defined Benefit (Svc, Int & RoA)'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004001024')	
THEN 'Pension Costs-Defined Contribution Plans'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004403000')	
THEN 'Postage'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004106000' , '0004997000' , '0009020000' , '0009022000' , '0009022001' , '0009023000' , '0009024000' , '0009025000')	
THEN 'PPV - 3rd Party - Local Costs'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002210900' , '0002211007' , '0002211011' , '0002211012' , '0002211014' , '0002211015' , '0002211016')	
THEN 'Prepaid Exp - Advances'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009009000')	
THEN 'PPV - Intercompany - Exchange - AH'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009019000')	
THEN 'PPV - Intercompany Billing Price - AH'	
		
ELSE CASE WHEN DD_GLACCOUNT IN ('0001301000')	
THEN 'Prepaid Exp - Deposits'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002300000' , '0002302000' , '0002302001' , '0002302002')	
THEN 'Prepaid Exp - Insurance - General'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002304000' , '0002306000')	
THEN 'Prepaid Exp - Miscellaneous'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002306007')	
THEN 'Prepaid Exp - Sales & Marketing'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002400000')	
THEN 'Prepaid Exp - Trade Tax'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004155000' , '0004156000' , '0004400001' , '0004504000' , '0004504003' , '0004504009' , '0004520004')	
THEN 'Professional_Services'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004992004')	
THEN 'Production Variances - Non-Commercial'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009402000' , '0009402010')	
THEN 'Provision for Taxes - Federal Income - Current'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004152000')	
THEN 'Relocation Expenses'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004208001')	
THEN 'Rental - Space'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004208002', '0004208009')	
THEN 'Rental - Machinery & Equipment'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004208000' , '0004208006' , '0004208007')	
THEN 'Rentals - Total'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004350017')	
THEN 'Rep Facilitated Meetings-PartiallyDeductible(Meal)'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004902000')	
THEN 'Royalty Expense - 3rd Party'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004000000' , '0004000003' , '0004000004' , '0004000016' , '0004000031')	
THEN 'Salaries - Professional'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004450002' , '0004451000' , '0004452001' , '0004401000' , '0004401001' , '0004452000')	
THEN 'Software_Hardware_and_Services'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0009040000')	
THEN 'Scrap Assessment'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004117000')	
THEN 'Supplies & Non-Cap Equip - Clinical Study Material'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004121000')	
THEN 'Supplies & Non-Capital Equipment - Laboratory'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004400000' , '0004402000')	
THEN 'Supplies & Non-Capital Equip - Office ex-Computer'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004404000')	
THEN 'Supplies & Non-Capital Equipment - Computer'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004121000')	
THEN 'Supplies & Non-Capital Equipment - Laboratory'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004119000' , '0004120000' , '0004120050' , '0004205001' , '0004302000' , '0004501001')	
THEN 'Supplies & Non-Capital Equipment - Low Value Asset'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004001010')	
THEN 'Termination - Professional'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004520007')	
THEN 'Supplies & Non-Capital Equipment - Manufacturing'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004153000' , '0004153001' , '0004153003' , '0004504001')	
THEN 'Training Tuition Costs - Other'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004350000' , '0004350001' , '0004350002' , '0004350003' , '0004350004' , '0004350005' , '0004350008' , '0004350009' , '0004350010' , '0004350011' , '0004350013' , '0004350014' , '0004350016' , '0004350023' , '0004350024' , '0004350025' , '0004350026' , '0004350027' , '0004352000' , '0004352001' , '0004352003' , '0004353000' , '0004520014')	
THEN 'Travel_Cards_and_Meetings'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0002203000' , '0002203017' , '0002203025' , '0002203027' , '0002203028' , '0002203032' , '0002203039' , '0002837001')	
THEN 'VAT - Balance Receivable'	
	
ELSE CASE WHEN DD_GLACCOUNT IN ('0004002008' , '0004002023' , '0004002027')	
THEN 'Workers Compensation Insurance - Professional'	
ELSE 'undefined'	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	
END	

as tax_level3
from  fact_accountspayable f, dim_materialgroup d, dim_part dp
where f.dim_materialgroupid = d.dim_materialgroupid
and f.dim_popartid = dp.dim_partid)t
on t.fact_accountspayableid = f.fact_accountspayableid
when matched then update
set dd_gsm_taxonomy_level3 = t.tax_level3

*/

/*
merge into fact_accountspayable f
using(

Select distinct  f.fact_accountspayableid, 
-- dm.materialgroupcode, dp.industrystandarddescription,c2.description MGC,c3.description IND, dd_glaccount, c1.description,

COALESCE(CASE WHEN dp.industrystandarddescription = c3.val THEN c3.description END,
						CASE WHEN dm.materialgroupcode = c2.val THEN c2.description END,
						 CASE WHEN dd_glaccount = c1.val THEN c1.description END,  'Undefined')
as
tax_level3


from  fact_accountspayable f
inner join dim_materialgroup dm on  f.dim_materialgroupid = dm.dim_materialgroupid
Inner join dim_part dp on f.dim_popartid = dp.dim_partid
LEFT join tmp_level3 c1 on c1.val = f.dd_glaccount
LEFT join tmp_level3 c2 on c2.val = dm.materialgroupcode
LEFT join tmp_level3 c3 on c3.val = dp.industrystandarddescription
) t
on t.fact_accountspayableid = f.fact_accountspayableid
when matched then update
set dd_gsm_taxonomy_level3 = t.tax_level3
*/





/* GSM Taxonomy Level 5 */

/* APP-8849 GSM Taxonomy Level 5 (Segment) */
/* level 5 is created as dimension and used in both AP and Purchase SA */

delete from dim_gsmlevel5;


INSERT INTO dim_gsmlevel5(
	dim_gsmlevel5id,
	apid,
	purchid,
	ponumber,
	materialgroupcode,
	industrystandarddescription,
	glaccount,
	plant,
	companycode,
	level5,
	level3,
	geographic,
	lob,
	level1,
	level2
)
SELECT 1, 1, 1, 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set', 'Not Set'
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_gsmlevel5
               WHERE dim_gsmlevel5id = 1);

DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'dim_gsmlevel5';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_gsmlevel5', ifnull(max(dim_gsmlevel5id), 0)
FROM dim_gsmlevel5;

-- 1. from ap
drop table if exists tmp_from_ap;
create table tmp_from_ap
as
select distinct 
f.fact_accountspayableid as apid,
1 as purchid,
f.DD_PURCH_ORDER_NUMBER as ponumber,
dm.materialgroupcode as materialgroupcode,
dp.industrystandarddescription as industrystandarddescription,
f.dd_glaccount as glaccount,
pl.plantcode as plant,
cc.companycode as companycode,
'Not Set' as level5,
'Not Set' as level3,
'Not Set' as geographic,
'Not Set' as lob,
'Not Set' as level1,
'Not Set' as level2
from fact_accountspayable f
inner join dim_materialgroup dm on f.dim_materialgroupid = dm.dim_materialgroupid
inner join dim_part dp on f.dim_popartid = dp.dim_partid
inner join dim_plant pl on f.dim_poplantid = pl.dim_plantid
inner join dim_company cc on f.dim_companyid = cc.dim_companyid;

INSERT INTO dim_gsmlevel5(
	dim_gsmlevel5id,
	apid,
	purchid,
	ponumber,
	materialgroupcode,
	industrystandarddescription,
	glaccount,
	plant,
	companycode,
	level5,
	level3,
	geographic,
	lob,
	level1,
	level2
)
select distinct (SELECT max_id
             FROM NUMBER_FOUNTAIN
            WHERE table_name = 'dim_gsmlevel5')
          + row_number()
             over(order by '') as dim_gsmlevel5id,
ifnull(apid, 1),
ifnull(purchid, 1),
ifnull(ponumber, 'Not Set'),
ifnull(materialgroupcode, 'Not Set'),
ifnull(industrystandarddescription, 'Not Set'),
ifnull(glaccount, 'Not Set'),
ifnull(plant, 'Not Set'),
ifnull(companycode, 'Not Set'),
ifnull(level5, 'Not Set'),
ifnull(level3, 'Not Set'),
ifnull(geographic, 'Not Set'),
ifnull(lob, 'Not Set'),
ifnull(level1, 'Not Set'),
ifnull(level2, 'Not Set')
from tmp_from_ap f
where not exists(select 1
        from dim_gsmlevel5 x
        where x.apid = f.apid
		and x.purchid = f.purchid
		and x.ponumber = f.ponumber
        and x.materialgroupcode = f.materialgroupcode
        and x.industrystandarddescription = f.industrystandarddescription
        and x.glaccount = f.glaccount
		and x.plant = f.plant
		and x.companycode = f.companycode);



-- 2. from purchase
drop table if exists tmp_from_purchase;
create table tmp_from_purchase
as
select distinct 
1 as apid,
f.fact_purchaseid as purchid,
f.dd_DocumentNo as ponumber,
dm.materialgroupcode as materialgroupcode,
dp.industrystandarddescription as industrystandarddescription,
f.dd_glaccountno as glaccount,
pl.plantcode as plant,
cc.companycode as companycode,
'Not Set' as level5,
'Not Set' as level3,
'Not Set' as geographic,
'Not Set' as lob,
'Not Set' as level1,
'Not Set' as level2
from fact_purchase f
inner join dim_materialgroup dm on f.dim_materialgroupid = dm.dim_materialgroupid
inner join dim_part dp on f.dim_partid = dp.dim_partid
inner join dim_plant pl on f.dim_plantidordering = pl.dim_plantid
inner join dim_company cc on f.dim_companyid = cc.dim_companyid;


DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'dim_gsmlevel5';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'dim_gsmlevel5', ifnull(max(dim_gsmlevel5id), 0)
FROM dim_gsmlevel5;


insert into dim_gsmlevel5
(
	dim_gsmlevel5id,
	apid,
	purchid,
	ponumber,
	materialgroupcode,
	industrystandarddescription,
	glaccount,
	plant,
	companycode,
	level5,
	level3,
	geographic,
	lob,
	level1,
	level2
)
select distinct (SELECT max_id
             FROM NUMBER_FOUNTAIN
            WHERE table_name = 'dim_gsmlevel5')
          + row_number()
             over(order by '') as dim_gsmlevel5id,
ifnull(apid, 1),
ifnull(purchid, 1),
ifnull(ponumber, 'Not Set'),
ifnull(materialgroupcode, 'Not Set'),
ifnull(industrystandarddescription, 'Not Set'),
ifnull(glaccount, 'Not Set'),
ifnull(plant, 'Not Set'),
ifnull(companycode, 'Not Set'),
ifnull(level5, 'Not Set'),
ifnull(level3, 'Not Set'),
ifnull(geographic, 'Not Set'),
ifnull(lob, 'Not Set'),
ifnull(level1, 'Not Set'),
ifnull(level2, 'Not Set')
from tmp_from_purchase f
where not exists(select 1
        from dim_gsmlevel5 x
        where x.apid = f.apid
		and x.purchid = f.purchid
		and x.ponumber = f.ponumber
        and x.materialgroupcode = f.materialgroupcode
        and x.industrystandarddescription = f.industrystandarddescription
        and x.glaccount = f.glaccount
		and x.plant = f.plant
		and x.companycode = f.companycode);

/* Temp table tmp_level5 is created with 2 cols - val and description */
/* All the values and their corresponding descriptions (Direct, Indirect, Undefined) are stored in this temp table */
/* updating level5 field from dim using temp table  */

merge into dim_gsmlevel5 f
using
(
select distinct dim_gsmlevel5id, COALESCE(CASE WHEN industrystandarddescription = t3.val THEN t3.description END,
						CASE WHEN materialgroupcode = t2.val THEN t2.description END,
						 CASE WHEN glaccount = t1.val THEN t1.description END,  'Undefined')
as
tax_level5
from dim_gsmlevel5 f
left join tmp_level5 t1 on t1.val = f.glaccount
left join tmp_level5 t2 on t2.val = f.materialgroupcode
left join tmp_level5 t3 on t3.val = f.industrystandarddescription 

)x
on f.dim_gsmlevel5id = x.dim_gsmlevel5id
when matched then update set f.level5 = x.tax_level5;




/* Temp table tmp_level3 is created with 2 cols - val and description */

merge into dim_gsmlevel5 f
using
(
select distinct dim_gsmlevel5id, COALESCE(CASE WHEN industrystandarddescription = t3.val THEN t3.description END,
						CASE WHEN materialgroupcode = t2.val THEN t2.description END,
						 CASE WHEN glaccount = t1.val THEN t1.description END,  'Undefined')
as
tax_level3
from dim_gsmlevel5 f
left join tmp_level3 t1 on t1.val = f.glaccount
left join tmp_level3 t2 on t2.val = f.materialgroupcode
left join tmp_level3 t3 on t3.val = f.industrystandarddescription 

)x
on f.dim_gsmlevel5id = x.dim_gsmlevel5id
when matched then update set f.level3 = x.tax_level3;




/* Temp table tmp_level1 is created with 2 cols - val and description */

merge into dim_gsmlevel5 f
using(

Select distinct  f.dim_gsmlevel5id, 
 --dm.materialgroupcode, dp.industrystandarddescription,c1.description MGC,c2.description IND,

COALESCE(case when industrystandarddescription = c2.val then c2.description END,
		case when materialgroupcode = c1.val then c1.description  END, 'Undefined'      
) as tax_level1


from  dim_gsmlevel5 f
LEFT join tmp_level1 c1 on c1.val = f.materialgroupcode
LEFT join tmp_level1 c2 on c2.val = f.industrystandarddescription
) t
on t.dim_gsmlevel5id = f.dim_gsmlevel5id
when matched then update
set level1 = t.tax_level1;




/* Temp table tmp_level2 is created with 2 cols - val and description */

merge into dim_gsmlevel5 f
using(

Select distinct  f.dim_gsmlevel5id, 
 --dm.materialgroupcode, dp.industrystandarddescription,c1.description MGC,c2.description IND,

COALESCE(case when industrystandarddescription = c2.val then c2.description END,
		case when materialgroupcode = c1.val then c1.description  END, 'Undefined'      
) as tax_level2


from  dim_gsmlevel5 f
LEFT join tmp_level2 c1 on c1.val = f.materialgroupcode
LEFT join tmp_level2 c2 on c2.val = f.industrystandarddescription
) t
on t.dim_gsmlevel5id = f.dim_gsmlevel5id
when matched then update
set level2 = t.tax_level2;



/* Temp table tmp_geographic is created with 2 cols - val and description */

merge into dim_gsmlevel5 f
using
(
select distinct dim_gsmlevel5id, COALESCE(CASE WHEN plant = t2.val THEN t2.description END,
						CASE WHEN companycode = t1.val THEN t1.description END, 'Undefined')
as
geographic
from dim_gsmlevel5 f
left join tmp_geographic t1 on t1.val = f.companycode
left join tmp_geographic t2 on t2.val = f.plant
)x
on f.dim_gsmlevel5id = x.dim_gsmlevel5id
when matched then update set f.geographic = x.geographic;



/* Temp table tmp_lob is created with 2 cols - val and description */

merge into dim_gsmlevel5 f
using
(
select distinct dim_gsmlevel5id, COALESCE(CASE WHEN plant = t2.val THEN t2.description END,
						CASE WHEN companycode = t1.val THEN t1.description END, 'Undefined')
as
lob
from dim_gsmlevel5 f
left join tmp_lob t1 on t1.val = f.companycode
left join tmp_lob t2 on t2.val = f.plant
)x
on f.dim_gsmlevel5id = x.dim_gsmlevel5id
when matched then update set f.lob = x.lob;









/* update dim_gsmlevel5id  AP SA */
/*
update fact_accountspayable f
set f.dim_gsmlevel5id = g.dim_gsmlevel5id
from fact_accountspayable f, dim_gsmlevel5 g, dim_materialgroup dm, dim_part dp
where f.DD_PURCH_ORDER_NUMBER = g.ponumber
and f.dim_materialgroupid = dm.dim_materialgroupid
and dm.materialgroupcode = g.materialgroupcode
and f.dim_popartid = dp.dim_partid
and dp.industrystandarddescription = g.industrystandarddescription
and f.dd_glaccount = g.glaccount
*/

merge into fact_accountspayable f
using
(
	select distinct f.fact_accountspayableid,  g.dim_gsmlevel5id
	from fact_accountspayable f, dim_gsmlevel5 g , dim_materialgroup dm, dim_part dp , dim_plant pl, dim_company cc
	where f.fact_accountspayableid = g.apid
	and f.DD_PURCH_ORDER_NUMBER = g.ponumber
	and f.dim_materialgroupid = dm.dim_materialgroupid
	and dm.materialgroupcode = g.materialgroupcode
	and f.dim_popartid = dp.dim_partid
	and dp.industrystandarddescription = g.industrystandarddescription
	and f.dd_glaccount = g.glaccount

	and f.dim_poplantid = pl.dim_plantid
	and pl.plantcode = g.plant
	and f.dim_companyid = cc.dim_companyid 
	and cc.companycode = g.companycode
)x
on f.fact_accountspayableid = x.fact_accountspayableid
when matched then update set f.dim_gsmlevel5id = x.dim_gsmlevel5id;


/* update dim_gsmlevel5id  Purchase SA */
/*
merge into fact_purchase f
using
(
select distinct f.fact_purchaseid,  g.dim_gsmlevel5id
from fact_purchase f, dim_gsmlevel5 g, dim_materialgroup dm, dim_part dp
where f.dd_DocumentNo = g.ponumber
and f.dim_materialgroupid = dm.dim_materialgroupid
and dm.materialgroupcode = g.materialgroupcode
and f.dim_partid = dp.dim_partid
and dp.industrystandarddescription = g.industrystandarddescription
and f.dd_glaccountno = g.glaccount
)x
on f.fact_purchaseid = x.fact_purchaseid
when matched then update set f.dim_gsmlevel5id = x.dim_gsmlevel5id
*/

merge into fact_purchase f
using
(
	select distinct f.fact_purchaseid, g.dim_gsmlevel5id
	from fact_purchase f, dim_gsmlevel5 g , dim_materialgroup dm, dim_part dp , dim_plant pl, dim_company cc
	where f.fact_purchaseid = g.purchid
	and f.dd_DocumentNo = g.ponumber
	and f.dim_materialgroupid = dm.dim_materialgroupid
	and dm.materialgroupcode = g.materialgroupcode
	and f.dim_partid = dp.dim_partid
	and dp.industrystandarddescription = g.industrystandarddescription
	and ifnull(f.dd_glaccountno, 'Not Set') = ifnull(g.glaccount, 'Not Set')

	and f.dim_plantidordering = pl.dim_plantid
	and pl.plantcode = g.plant
	and f.dim_companyid = cc.dim_companyid 
	and cc.companycode = g.companycode
)x
on f.fact_purchaseid = x.fact_purchaseid
when matched then update set f.dim_gsmlevel5id = x.dim_gsmlevel5id;


