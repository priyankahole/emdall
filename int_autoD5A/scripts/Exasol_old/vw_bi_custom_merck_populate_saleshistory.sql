
Drop table if exists max_holder_saleshist_merck;
Create table max_holder_saleshist_merck
as
Select ifnull(max(fact_saleshistory_merckid),0) as maxid
from fact_saleshistory_merck;

INSERT INTO fact_saleshistory_merck(
fact_saleshistory_merckid,
dd_currentdate,
Dim_DateIdSnapshot,
dd_SnapShotYear,
dd_SnapshotWeekID,
dd_SnapshotMonthID,
dd_salesdocno,
dd_salesitemno,
dd_scheduleno,
ct_scheduleqtysalesunit,
ct_confirmedqty,
ct_correctedqty,
ct_deliveredqty,
ct_onhandcoveredqty,
amt_unitprice,
ct_priceunit,
amt_scheduletotal,
amt_stdcost,
amt_targetvalue,
ct_targetqty,
amt_exchangerate,
ct_overdlvrtolerance,
ct_underdlvrtolerance,
dim_dateidsalesordercreated,
dim_dateidscheddeliveryreq,
dim_dateidscheddlvrreqprev,
dim_dateidscheddelivery,
dim_dateidgoodsissue,
dim_dateidmtrlavail,
dim_dateidloading,
dim_dateidtransport,
dim_currencyid,
dim_producthierarchyid,
dim_plantid,
dim_companyid,
dim_storagelocationid,
dim_salesdivisionid,
dim_shipreceivepointid,
dim_documentcategoryid,
dim_salesdocumenttypeid,
dim_salesorgid,
dim_customerid,
dim_dateidvalidfrom,
dim_dateidvalidto,
dim_salesgroupid,
dim_costcenterid,
dim_controllingareaid,
dim_billingblockid,
dim_transactiongroupid,
dim_salesorderrejectreasonid,
dim_partid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_customergroup1id,
dim_customergroup2id,
dim_salesorderitemcategoryid,
amt_exchangerate_gbl,
ct_fillqty,
ct_onhandqty,
dim_dateidfirstdate,
dim_schedulelinecategoryid,
dim_salesmiscid,
dd_itemrelfordelv,
dim_dateidshipmentdelivery,
dim_dateidactualgi,
dim_dateidshipdlvrfill,
dim_dateidactualgifill,
dim_profitcenterid,
dim_customeridshipto,
dim_dateidguaranteedate,
dim_unitofmeasureid,
dim_distributionchannelid,
dd_batchno,
dd_createdby,
dim_custompartnerfunctionid,
dim_dateidloadingfill,
dim_payerpartnerfunctionid,
dim_billtopartypartnerfunctionid,
ct_shippedagnstorderqty,
ct_cmlqtyreceived,
dim_custompartnerfunctionid1,
dim_custompartnerfunctionid2,
dim_customergroupid,
dim_salesofficeid,
dim_highercustomerid,
dim_highersalesorgid,
dim_higherdistchannelid,
dim_highersalesdivid,
dd_soldtohierarchylevel,
dd_hierarchytype,
dim_toplevelcustomerid,
dim_topleveldistchannelid,
dim_toplevelsalesdivid,
dim_toplevelsalesorgid,
ct_cumconfirmedqty,
ct_cumorderqty,
ct_shippedorbilledqty,
amt_shippedorbilled,
dim_creditrepresentativeid,
dd_customerpono,
ct_incompleteqty,
amt_incomplete,
dim_deliveryblockid,
amt_baseprice,
dim_materialpricegroup1id,
amt_creditholdvalue,
amt_deliveryblockvalue,
dim_dateiddlvrdoccreated,
amt_customerexpectedprice,
dim_overallstatuscreditcheckid,
dim_salesdistrictid,
dim_accountassignmentgroupid,
dim_materialgroupid,
dim_salesdocorderreasonid,
amt_subtotal3,
amt_subtotal4,
amt_dicountaccrualnetprice,
dim_purchaseordertypeid,
dim_dateidquotationvalidfrom,
dim_dateidpurchaseorder,
dim_dateidquotationvalidto,
dim_dateidsocreated,
dim_dateidsodocument,
dd_subsequentdocno,
dd_subsdocitemno,
dd_subsscheduleno,
dim_subsdoccategoryid,
dim_dateidaslastdate,
dd_referencedocumentno,
dd_requirementtype,
amt_stdprice,
dim_dateidnextdate,
amt_tax,
dim_dateidearliestsocanceldate,
dim_dateidlatestcustpoagi,
dd_businesscustomerpono,
dim_routeid,
dim_billingdateid,
dim_customerpaymenttermsid,
dim_salesriskcategoryid,
dd_creditrep,
dd_creditlimit,
dim_customerriskcategoryid,
ct_fillqty_crd,
ct_fillqty_pdd,
dim_h1customerid,
dim_dateidsoitemchangedon,
dim_shippingconditionid,
dim_dateidrejection,
dd_customermaterialno,
dim_baseuomid,
dim_salesuomid,
amt_unitpriceuom,
amt_subtotal3_orderqty,
amt_subtotal3incustconfig_billing,
ct_otifqty_merck,
dd_soheaderotif_merck,
dd_solineotif_merck,
dim_customermastersalesid,
dim_partsalesid,
dim_dateidfixedvalue,
dim_paymenttermsid,
dim_dateidnetduedate,
dim_materialpricegroup4id,
dim_materialpricegroup5id,
ct_backorderedqty_merck,
ct_headerbackorderedqty_merck,
dd_salesorderblocked,
ct_billingblockcnt_merck,
ct_deliveryblockcnt_merck,
ct_creditblockcnt_merck,
dd_otifcalcflag_merck,
dd_socreatetime,
dd_reqdeliverytime,
dd_solinecreatetime,
dd_deliverytime,
dd_plannedgitime,
dd_infullflag_merck,
dd_ontimeflag_merck,
dim_dateidexpgi_merck,
dd_backorderflag_merck,
dim_dateidarposting,
dim_customercategoryid,
dim_currencyid_tra,
dim_currencyid_gbl,
dim_currencyid_stat,
amt_exchangerate_stat,
amt_billingcustconfigsubtotal3unitprice,
dd_prodorderno,
dd_prodorderitemno,
dim_customergroup4id,
dim_scheduledeliveryblockid,
dim_customerconditiongroups1id,
dim_customerconditiongroups2id,
dim_customerconditiongroups3id,
dd_openconfirmationsexists,
dd_otifenabledflag,
dim_dateidadjsocreated_merck,
dd_infullflaghdr_merck,
dd_ontimeflaghdr_merck,
dd_sdlinecreatetime_merck,
dim_dateidadjdroptowh_merck,
dd_conditionno,
dim_chargebackcontractid,
amt_subtotal1,
amt_subtotal2,
amt_subtotal5,
amt_subtotal6,
dd_ro_materialdesc,
dd_documentconditionno,
dd_highlevelitem,
dim_customeridcbowner,
dd_creditmgr,
dd_clearedblockedsts,
ct_committedqty,
dim_dateidlatestactualgi_merck,
dim_plantidordering_merck,
dim_dateidreqdelivline,
dim_materialpricinggroupid,
dim_customersla_merckid,
dd_deliveryindicator,
dd_invqtystatus_merck,
dd_billing_no,
dim_dateidbillingcreated,
dim_agreementsid,
/*21 apr 2015 new BaseUoM and IRU cols */
ct_ScheduleQtyBaseUoM,
ct_ShippedAgnstOrderQtyBaseUoM,
ct_CmlQtyReceivedBaseUoM,
ct_ConfirmedQtyBaseUoM,
ct_ShippedOrderQtyBaseUoM,
ct_ScheduleQtyBaseUoMIRU,
ct_ShippedAgnstOrderQtyBaseUoMIRU,
ct_CmlQtyReceivedBaseUoMIRU,
ct_ConfirmedQtyBaseUoMIRU,
ct_ShippedOrderQtyBaseUoMIRU,
dd_DeliveryNumber,
dd_SalesDlvrItemNo,
dd_SalesDlvrDocNo,
std_exchangerate_dateid)
SELECT max_holder_saleshist_merck.maxid + row_number() over(order by '') fact_saleshistory_merckid,
current_date as dd_currentdate,
1 Dim_DateIdSnapshot, /*ifnull((SELECT dim_DateID FROM dim_date WHERE datevalue = CURRENT_DATE AND companycode = dc.companycode),1) Dim_DateIdSnapshot,*/
       year(current_date) dd_SnapShotYear,
       0 as dd_SnapshotWeekID,
       0 as dd_SnapshotMonthID,
dd_salesdocno,
dd_salesitemno,
dd_scheduleno,
ct_scheduleqtysalesunit,
ct_confirmedqty,
ct_correctedqty,
ct_deliveredqty,
ct_onhandcoveredqty,
amt_unitprice,
ct_priceunit,
amt_scheduletotal,
amt_stdcost,
amt_targetvalue,
ct_targetqty,
amt_exchangerate,
ct_overdlvrtolerance,
ct_underdlvrtolerance,
ifnull(dim_dateidsalesordercreated,1) as DIM_DATEIDSALESORDERCREATED,
ifnull(dim_dateidscheddeliveryreq,1) as DIM_DATEIDSCHEDDELIVERYREQ,
ifnull(dim_dateidscheddlvrreqprev,1) as DIM_DATEIDSCHEDDLVRREQPREV,
ifnull(dim_dateidscheddelivery,1) as DIM_DATEIDSCHEDDELIVERY,
ifnull(dim_dateidgoodsissue,1) as DIM_DATEIDGOODSISSUE,
ifnull(dim_dateidmtrlavail,1) as DIM_DATEIDMTRLAVAIL,
ifnull(dim_dateidloading,1) as DIM_DATEIDLOADING,
ifnull(dim_dateidtransport,1) as DIM_DATEIDTRANSPORT,
ifnull(dim_currencyid,1) as DIM_CURRENCYID,
ifnull(dim_producthierarchyid,1) as DIM_PRODUCTHIERARCHYID,
ifnull(dim_plantid,1) as DIM_PLANTID,
ifnull(f.dim_companyid,1) as DIM_COMPANYID,
ifnull(dim_storagelocationid,1),
ifnull(dim_salesdivisionid,1),
ifnull(dim_shipreceivepointid,1),
ifnull(dim_documentcategoryid,1),
ifnull(dim_salesdocumenttypeid,1),
ifnull(dim_salesorgid,1) as DIM_SALESORGID,
ifnull(dim_customerid,1) as DIM_CUSTOMERID,
ifnull(dim_dateidvalidfrom,1) as DIM_DATEIDVALIDFROM,
ifnull(dim_dateidvalidto,1) as DIM_DATEIDVALIDTO,
ifnull(dim_salesgroupid,1) as DIM_SALESGROUPID,
ifnull(dim_costcenterid,1) as DIM_COSTCENTERID,
ifnull(dim_controllingareaid,1) as DIM_CONTROLLINGAREAID,
ifnull(dim_billingblockid,1) as DIM_BILLINGBLOCKID,
ifnull(dim_transactiongroupid,1) as DIM_TRANSACTIONGROUPID,
ifnull(dim_salesorderrejectreasonid,1) as DIM_SALESORDERREJECTREASONID,
ifnull(dim_partid,1) as DIM_PARTID,
ifnull(dim_salesorderheaderstatusid,1) as DIM_SALESORDERHEADERSTATUSID,
ifnull(dim_salesorderitemstatusid,1) as DIM_SALESORDERITEMSTATUSID,
ifnull(dim_customergroup1id,1) as DIM_CUSTOMERGROUP1ID,
ifnull(dim_customergroup2id,1) as DIM_CUSTOMERGROUP2ID,
ifnull(dim_salesorderitemcategoryid,1) as DIM_SALESORDERITEMCATEGORYID,
amt_exchangerate_gbl,
ct_fillqty,
ct_onhandqty,
ifnull(dim_dateidfirstdate,1) as DIM_DATEIDFIRSTDATE,
ifnull(dim_schedulelinecategoryid,1) as DIM_SCHEDULELINECATEGORYID,
ifnull(dim_salesmiscid,1) as DIM_SALESMISCID,
ifnull(dd_itemrelfordelv,'Not Set') as DD_ITEMRELFORDELV,
ifnull(dim_dateidshipmentdelivery,1) as DIM_DATEIDSHIPMENTDELIVERY,
ifnull(dim_dateidactualgi,1) as DIM_DATEIDACTUALGI,
ifnull(dim_dateidshipdlvrfill,1) as DIM_DATEIDSHIPDLVRFILL,
ifnull(dim_dateidactualgifill,1) as DIM_DATEIDACTUALGIFILL,
ifnull(dim_profitcenterid,1) as DIM_PROFITCENTERID,
ifnull(dim_customeridshipto,1) as DIM_CUSTOMERIDSHIPTO,
ifnull(dim_dateidguaranteedate,1) as DIM_DATEIDGUARANTEEDATE,
ifnull(dim_unitofmeasureid,1) as DIM_UNITOFMEASUREID,
ifnull(dim_distributionchannelid,1) as DIM_DISTRIBUTIONCHANNELID,
ifnull(dd_batchno,'Not Set') as dd_batchno,
ifnull(dd_createdby,'Not Set') as dd_createdby,
ifnull(dim_custompartnerfunctionid,1) as DIM_CUSTOMPARTNERFUNCTIONID,
ifnull(dim_dateidloadingfill,1) as DIM_DATEIDLOADINGFILL,
ifnull(dim_payerpartnerfunctionid,1) as DIM_PAYERPARTNERFUNCTIONID,
ifnull(dim_billtopartypartnerfunctionid,1) as DIM_BILLTOPARTYPARTNERFUNCTIONID,
ifnull(ct_shippedagnstorderqty,0),
ifnull(ct_cmlqtyreceived,0),
ifnull(dim_custompartnerfunctionid1,1) as DIM_CUSTOMPARTNERFUNCTIONID1,
ifnull(dim_custompartnerfunctionid2,1) as DIM_CUSTOMPARTNERFUNCTIONID2,
ifnull(dim_customergroupid,1) as DIM_CUSTOMERGROUPID,
ifnull(dim_salesofficeid,1) as DIM_SALESOFFICEID,
ifnull(dim_highercustomerid,1) as DIM_HIGHERCUSTOMERID,
ifnull(dim_highersalesorgid,1) as DIM_HIGHERSALESORGID,
ifnull(dim_higherdistchannelid,1) as DIM_HIGHERDISTCHANNELID,
ifnull(dim_highersalesdivid,1) as DIM_HIGHERSALESDIVID,
ifnull(dd_soldtohierarchylevel,'Not Set') as dd_soldtohierarchylevel,
ifnull(dd_hierarchytype,'Not Set') as dd_hierarchytype,
ifnull(dim_toplevelcustomerid,1) as DIM_TOPLEVELCUSTOMERID,
ifnull(dim_topleveldistchannelid,1) as DIM_TOPLEVELDISTCHANNELID,
ifnull(dim_toplevelsalesdivid,1) as DIM_TOPLEVELSALESDIVID,
ifnull(dim_toplevelsalesorgid,1) as DIM_TOPLEVELSALESORGID,
ct_cumconfirmedqty,
ct_cumorderqty,
ct_shippedorbilledqty,
amt_shippedorbilled,
ifnull(dim_creditrepresentativeid,1) as DIM_CREDITREPRESENTATIVEID,
dd_customerpono,
ct_incompleteqty,
amt_incomplete,
ifnull(dim_deliveryblockid,1) as DIM_DELIVERYBLOCKID,
amt_baseprice,
ifnull(dim_materialpricegroup1id,1) as DIM_MATERIALPRICEGROUP1ID,
amt_creditholdvalue,
amt_deliveryblockvalue,
ifnull(dim_dateiddlvrdoccreated,1) as DIM_DATEIDDLVRDOCCREATED,
amt_customerexpectedprice,
ifnull(dim_overallstatuscreditcheckid,1) as DIM_OVERALLSTATUSCREDITCHECKID,
ifnull(dim_salesdistrictid,1) as DIM_SALESDISTRICTID,
ifnull(dim_accountassignmentgroupid,1) as DIM_ACCOUNTASSIGNMENTGROUPID,
ifnull(dim_materialgroupid,1) as DIM_MATERIALGROUPID,
ifnull(dim_salesdocorderreasonid,1) as DIM_SALESDOCORDERREASONID,
amt_subtotal3,
amt_subtotal4,
amt_dicountaccrualnetprice,
ifnull(dim_purchaseordertypeid,1) as DIM_PURCHASEORDERTYPEID,
ifnull(dim_dateidquotationvalidfrom,1) as DIM_DATEIDQUOTATIONVALIDFROM,
ifnull(dim_dateidpurchaseorder,1) as DIM_DATEIDPURCHASEORDER,
ifnull(dim_dateidquotationvalidto,1) as DIM_DATEIDQUOTATIONVALIDTO,
ifnull(dim_dateidsocreated,1) as DIM_DATEIDSOCREATED,
ifnull(dim_dateidsodocument,1) as DIM_DATEIDSODOCUMENT,
ifnull(dd_subsequentdocno,'Not Set') as dd_subsequentdocno,
ifnull(dd_subsdocitemno,'Not Set') as dd_subsdocitemno,
ifnull(dd_subsscheduleno,'Not Set') as dd_subsscheduleno,
ifnull(dim_subsdoccategoryid,1) as DIM_SUBSDOCCATEGORYID,
ifnull(dim_dateidaslastdate,1) as DIM_DATEIDASLASTDATE,
ifnull(dd_referencedocumentno,'Not Set') as dd_referencedocumentno,
ifnull(dd_requirementtype,'Not Set') as dd_requirementtype,
amt_stdprice,
ifnull(dim_dateidnextdate,1) as DIM_DATEIDNEXTDATE,
amt_tax,
ifnull(dim_dateidearliestsocanceldate,1) as DIM_DATEIDEARLIESTSOCANCELDATE,
ifnull(dim_dateidlatestcustpoagi,1) as DIM_DATEIDLATESTCUSTPOAGI,
ifnull(left(dd_businesscustomerpono,36),'Not Set') as dd_businesscustomerpono,
ifnull(dim_routeid,1) as DIM_ROUTEID,
ifnull(dim_billingdateid,1) as DIM_BILLINGDATEID,
ifnull(dim_customerpaymenttermsid,1) as DIM_CUSTOMERPAYMENTTERMSID,
ifnull(dim_salesriskcategoryid,1) as DIM_SALESRISKCATEGORYID,
ifnull(dd_creditrep,'Not Set') as dd_creditrep,
ifnull(dd_creditlimit,'Not Set') as dd_creditlimit,
dim_customerriskcategoryid,
ct_fillqty_crd,
ct_fillqty_pdd,
ifnull(dim_h1customerid,1) as DIM_H1CUSTOMERID,
ifnull(dim_dateidsoitemchangedon,1) as DIM_DATEIDSOITEMCHANGEDON,
ifnull(dim_shippingconditionid,1) as DIM_SHIPPINGCONDITIONID,
ifnull(dim_dateidrejection,1) as DIM_DATEIDREJECTION,
dd_customermaterialno,
ifnull(dim_baseuomid,1) as DIM_BASEUOMID,
ifnull(dim_salesuomid,1) as DIM_SALESUOMID,
amt_unitpriceuom,
amt_subtotal3_orderqty,
amt_subtotal3incustconfig_billing,
ct_otifqty_merck,
ifnull(dd_soheaderotif_merck,'Not Set') as dd_soheaderotif_merck,
ifnull(dd_solineotif_merck,'Not Set') as dd_solineotif_merck,
ifnull(dim_customermastersalesid,1),
ifnull(dim_partsalesid,1),
ifnull(dim_dateidfixedvalue,1),
ifnull(dim_paymenttermsid,1),
ifnull(dim_dateidnetduedate,1),
ifnull(dim_materialpricegroup4id,1),
ifnull(dim_materialpricegroup5id,1),
ifnull(ct_backorderedqty_merck,0),
ifnull(ct_headerbackorderedqty_merck,0),
ifnull(dd_salesorderblocked,'Not Set') as dd_salesorderblocked,
ifnull(ct_billingblockcnt_merck,0),
ifnull(ct_deliveryblockcnt_merck,0),
ifnull(ct_creditblockcnt_merck,0),
ifnull(dd_otifcalcflag_merck,'Not Set') as dd_otifcalcflag_merck,
ifnull(dd_socreatetime,'Not Set') as dd_socreatetime,
ifnull(dd_reqdeliverytime,'Not Set') as dd_reqdeliverytime,
ifnull(dd_solinecreatetime,'Not Set'),
ifnull(dd_deliverytime,'Not Set'),
ifnull(dd_plannedgitime,'Not Set'),
ifnull(dd_infullflag_merck,'Not Set'),
ifnull(dd_ontimeflag_merck,'Not Set'),
ifnull(dim_dateidexpgi_merck,1),
ifnull(dd_backorderflag_merck,'Not Set'),
ifnull(dim_dateidarposting,1),
ifnull(dim_customercategoryid,1),
ifnull(dim_currencyid_tra,1),
ifnull(dim_currencyid_gbl,1),
ifnull(dim_currencyid_stat,1),
ifnull(amt_exchangerate_stat,0),
ifnull(amt_billingcustconfigsubtotal3unitprice,0),
ifnull(dd_prodorderno,'Not Set'),
ifnull(dd_prodorderitemno,'Not Set'),
ifnull(dim_customergroup4id,1),
ifnull(dim_scheduledeliveryblockid,1),
ifnull(dim_customerconditiongroups1id,1),
ifnull(dim_customerconditiongroups2id,1),
ifnull(dim_customerconditiongroups3id,1),
ifnull(dd_openconfirmationsexists,'Not Set'),
ifnull(dd_otifenabledflag,'Not Set'),
ifnull(dim_dateidadjsocreated_merck,1),
ifnull(dd_infullflaghdr_merck,'Not Set'),
ifnull(dd_ontimeflaghdr_merck,'Not Set'),
ifnull(dd_sdlinecreatetime_merck,'Not Set'),
ifnull(dim_dateidadjdroptowh_merck,1),
ifnull(dd_conditionno,'Not Set'),
ifnull(dim_chargebackcontractid,1),
ifnull(amt_subtotal1,0),
ifnull(amt_subtotal2,0),
ifnull(amt_subtotal5,0),
ifnull(amt_subtotal6,0),
'Not Set' as dd_ro_materialdesc,
ifnull(dd_documentconditionno,'Not Set'),
ifnull(dd_highlevelitem,'Not Set'),
ifnull(dim_customeridcbowner,1),
ifnull(dd_creditmgr,'Not Set'),
ifnull(dd_clearedblockedsts,'Not Set'),
ct_committedqty,
ifnull(dim_dateidlatestactualgi_merck,1),
ifnull(dim_plantidordering_merck,1),
ifnull(dim_dateidreqdelivline,1) as DIM_DATEIDREQDELIVLINE,
ifnull(dim_materialpricinggroupid,1) as DIM_MATERIALPRICINGGROUPID,
ifnull(dim_customersla_merckid,1) as DIM_CUSTOMERSLA_MERCKID,
ifnull(dd_deliveryindicator,'Not Set'),
ifnull(dd_invqtystatus_merck,'Not Set'),
'Not Set' as dd_billing_no,
1 as dim_dateidbillingcreated,
ifnull(dim_agreementsid,1) as DIM_AGREEMENTSID,
/*21 apr 2015 new BaseUoM and IRU cols */
ct_ScheduleQtyBaseUoM,
ct_ShippedAgnstOrderQtyBaseUoM,
ct_CmlQtyReceivedBaseUoM,
ct_ConfirmedQtyBaseUoM,
ct_ShippedOrderQtyBaseUoM,
ct_ScheduleQtyBaseUoMIRU,
ct_ShippedAgnstOrderQtyBaseUoMIRU,
ct_CmlQtyReceivedBaseUoMIRU,
ct_ConfirmedQtyBaseUoMIRU,
ct_ShippedOrderQtyBaseUoMIRU,
ifnull(dd_DeliveryNumber,'Not Set') as DD_DELIVERYNUMBER,
ifnull(dd_SalesDlvrItemNo,0) as DD_SALESDLVRITEMNO,
ifnull(dd_SalesDlvrDocNo,'Not Set'),
f.std_exchangerate_dateid as std_exchangerate_dateid
  FROM fact_salesorder f, dim_company dc,max_holder_saleshist_merck
where f.dim_companyid = dc.dim_companyid;


merge into fact_saleshistory_merck f
using (select distinct f.fact_saleshistory_merckid, d.dim_dateid
from fact_saleshistory_merck f
INNER JOIN dim_company dc on f.dim_companyid = dc.dim_companyid
INNER JOIN dim_plant p ON f.dim_plantid = p.dim_plantid
INNER JOIN dim_date d on datevalue = f.dd_currentdate AND d.companycode = dc.companycode AND d.plantcode_factory = p.plantcode
where f.dd_currentdate=current_date) a on a.fact_saleshistory_merckid=f.fact_saleshistory_merckid
when matched then update set Dim_DateIdSnapshot =ifnull(a.dim_dateid,1)
where Dim_DateIdSnapshot <> ifnull(a.dim_dateid,1);

/* Merck new dim's - BI-495 */

/*Georgiana Changes 29 Aug 2016: optimizing updates from dim's*/
/*update
fact_saleshistory_merck f
set dd_billingblockcode = ifnull(b.billingblockcode,'Not Set')
from dim_date d, dim_billingblock b, fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and b.dim_billingblockid = f.dim_billingblockid
and d.datevalue = current_date
and dd_billingblockcode <> ifnull(b.billingblockcode,'Not Set')*/

/*update
fact_saleshistory_merck f
set dd_billingblockdesc = ifnull(b.description,'Not Set')
from dim_date d, dim_billingblock b, fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and b.dim_billingblockid = f.dim_billingblockid
and d.datevalue = current_date
and dd_billingblockdesc <> ifnull(b.description,'Not Set')*/

merge into fact_saleshistory_merck f
using ( select distinct f.fact_saleshistory_merckid, b.billingblockcode, b.description
from  dim_billingblock b, fact_saleshistory_merck f
where ifnull(b.dim_billingblockid,1) = ifnull(f.dim_billingblockid,1)
and f.dd_currentdate = current_date) t
on t.fact_saleshistory_merckid=f.fact_saleshistory_merckid
when matched then update set dd_billingblockcode = ifnull(t.billingblockcode,'Not Set'),
dd_billingblockdesc = ifnull(t.description,'Not Set');


/*update
fact_saleshistory_merck f
set dd_overallstatuscreditcheck = ifnull(o.overallstatusforcreditcheck,'Not Set')
from dim_date d, dim_overallstatusforcreditcheck o, fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and o.dim_overallstatusforcreditcheckid = f.Dim_OverallStatusCreditCheckId
and d.datevalue = current_date
and dd_overallstatuscreditcheck <> ifnull(o.overallstatusforcreditcheck,'Not Set')*/

/*update
fact_saleshistory_merck f
set dd_overallstatuscreditcheckdesc = ifnull(o.description,'Not Set')
from dim_date d, dim_overallstatusforcreditcheck o, fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and o.dim_overallstatusforcreditcheckid = f.Dim_OverallStatusCreditCheckId
and d.datevalue = current_date
and dd_overallstatuscreditcheckdesc <> ifnull(o.description,'Not Set')*/

merge into fact_saleshistory_merck f
using ( select distinct f.fact_saleshistory_merckid, o.overallstatusforcreditcheck, o.description
from  dim_overallstatusforcreditcheck o, fact_saleshistory_merck f
where  ifnull(o.dim_overallstatusforcreditcheckid,1) = ifnull(f.Dim_OverallStatusCreditCheckId,1)
and f.dd_currentdate = current_date) t
on t.fact_saleshistory_merckid=f.fact_saleshistory_merckid
when matched then update set dd_overallstatuscreditcheck = ifnull(t.overallstatusforcreditcheck,'Not Set'),
dd_overallstatuscreditcheckdesc = ifnull(t.description,'Not Set');

/*update
fact_saleshistory_merck f
set dd_billingstatus = ifnull(h.billingstatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_billingstatus <> ifnull(h.billingstatus,'Not Set')

update
fact_saleshistory_merck f
set dd_BillingStatusOfOrdRelBillDoc = ifnull(h.BillingStatusOfOrdRelBillDoc,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_BillingStatusOfOrdRelBillDoc <> ifnull(h.BillingStatusOfOrdRelBillDoc,'Not Set')

update
fact_saleshistory_merck f
set dd_ConfirmationStatus = ifnull(h.ConfirmationStatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_ConfirmationStatus <> ifnull(h.ConfirmationStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_deliveryblockprocessingstatus = ifnull(h.deliveryblockprocessingstatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_deliveryblockprocessingstatus <> ifnull(h.deliveryblockprocessingstatus,'Not Set')

update
fact_saleshistory_merck f
set dd_overalldlvrblkstatuscode = ifnull(h.overalldlvrblkstatuscode,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_overalldlvrblkstatuscode <> ifnull(h.overalldlvrblkstatuscode,'Not Set')

update
fact_saleshistory_merck f
set dd_DeliveryStatusheader = ifnull(h.DeliveryStatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_DeliveryStatusheader <> ifnull(h.DeliveryStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_GeneralIncompleteStatusItem = ifnull(h.GeneralIncompleteStatusItem,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_GeneralIncompleteStatusItem <> ifnull(h.GeneralIncompleteStatusItem,'Not Set')

update
fact_saleshistory_merck f
set dd_generalincompletionprocessingstatusofheader = ifnull(h.generalincompletionprocessingstatusofheader,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_generalincompletionprocessingstatusofheader <> ifnull(h.generalincompletionprocessingstatusofheader,'Not Set')

update
fact_saleshistory_merck f
set dd_headerincompletionstautusconcerndlvry = ifnull(h.headerincompletionstautusconcerndlvry,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_headerincompletionstautusconcerndlvry <> ifnull(h.headerincompletionstautusconcerndlvry,'Not Set')

update
fact_saleshistory_merck f
set dd_overallblockedprocessingstatus = ifnull(h.overallblockedprocessingstatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_overallblockedprocessingstatus <> ifnull(h.overallblockedprocessingstatus,'Not Set')

update
fact_saleshistory_merck f
set dd_overallblkdstatus = ifnull(h.overallblkdstatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_overallblkdstatus <> ifnull(h.overallblkdstatus,'Not Set')

update
fact_saleshistory_merck f
set dd_overallblkdstatuscode = ifnull(h.overallblkdstatuscode,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_overallblkdstatuscode <> ifnull(h.overallblkdstatuscode,'Not Set')

update
fact_saleshistory_merck f
set dd_overallcreditstatus = ifnull(h.overallcreditstatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_overallcreditstatus <> ifnull(h.overallcreditstatus,'Not Set')

update
fact_saleshistory_merck f
set dd_overallcreditstatuscode = ifnull(h.overallcreditstatuscode,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_overallcreditstatuscode <> ifnull(h.overallcreditstatuscode,'Not Set')

update
fact_saleshistory_merck f
set dd_OverallDlvrStatusOfItem = ifnull(h.OverallDlvrStatusOfItem,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_OverallDlvrStatusOfItem <> ifnull(h.OverallDlvrStatusOfItem,'Not Set')

update
fact_saleshistory_merck f
set dd_OverallPickingStatus = ifnull(h.OverallPickingStatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_OverallPickingStatus <> ifnull(h.OverallPickingStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_OverallProcessStatusItem = ifnull(h.OverallProcessStatusItem,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_OverallProcessStatusItem <> ifnull(h.OverallProcessStatusItem,'Not Set')


update
fact_saleshistory_merck f
set dd_overallrejectionprocessingstatus = ifnull(h.overallrejectionprocessingstatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_overallrejectionprocessingstatus <> ifnull(h.overallrejectionprocessingstatus,'Not Set')

update
fact_saleshistory_merck f
set dd_overallprocessingstatusofcreditchecks = ifnull(h.overallprocessingstatusofcreditchecks,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_overallprocessingstatusofcreditchecks <> ifnull(h.overallprocessingstatusofcreditchecks,'Not Set')

update
fact_saleshistory_merck f
set dd_RefStatusAllItems = ifnull(h.RefStatusAllItems,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_RefStatusAllItems <> ifnull(h.RefStatusAllItems,'Not Set')

update
fact_saleshistory_merck f
set dd_RejectionStatus = ifnull(h.RejectionStatus,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_RejectionStatus <> ifnull(h.RejectionStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_rejectionstatuscode = ifnull(h.rejectionstatuscode,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_rejectionstatuscode <> ifnull(h.rejectionstatuscode,'Not Set')

update
fact_saleshistory_merck f
set dd_TotalIncompleteStatusAllItems = ifnull(h.TotalIncompleteStatusAllItems,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_TotalIncompleteStatusAllItems <> ifnull(h.TotalIncompleteStatusAllItems,'Not Set')

update
fact_saleshistory_merck f
set dd_TotalIncompleteStatusAllItemsCode = ifnull(h.TotalIncompleteStatusAllItemsCode,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_TotalIncompleteStatusAllItemsCode <> ifnull(h.TotalIncompleteStatusAllItemsCode,'Not Set')

update
fact_saleshistory_merck f
set dd_totalincompletionprocessingstatusallitems = ifnull(h.totalincompletionprocessingstatusallitems,'Not Set')
from dim_date d, dim_salesorderheaderstatus h,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and h.dim_salesorderheaderstatusid = f.dim_salesorderheaderstatusId
and d.datevalue = current_date
and dd_totalincompletionprocessingstatusallitems <> ifnull(h.totalincompletionprocessingstatusallitems,'Not Set')*/



merge into fact_saleshistory_merck f
using ( select distinct f.fact_saleshistory_merckid,
h.billingstatus,
h.BillingStatusOfOrdRelBillDoc,
h.ConfirmationStatus,
h.deliveryblockprocessingstatus,
h.overalldlvrblkstatuscode,
h.DeliveryStatus,
h.GeneralIncompleteStatusItem,
h.generalincompletionprocessingstatusofheader,
h.headerincompletionstautusconcerndlvry,
h.overallblockedprocessingstatus,
h.overallblkdstatus,
h.overallblkdstatuscode,
h.overallcreditstatus,
h.overallcreditstatuscode,
h.OverallDlvrStatusOfItem,
h.OverallPickingStatus,
h.OverallProcessStatusItem,
h.overallrejectionprocessingstatus,
h.overallprocessingstatusofcreditchecks,
h.RefStatusAllItems,
h.RejectionStatus,
h.rejectionstatuscode,
h.TotalIncompleteStatusAllItems,
h.TotalIncompleteStatusAllItemsCode,
h.totalincompletionprocessingstatusallitems
from  dim_salesorderheaderstatus h,fact_saleshistory_merck f
where  ifnull(h.dim_salesorderheaderstatusid,1) = ifnull(f.dim_salesorderheaderstatusId,1)
and f.dd_currentdate = current_date) t
on t.fact_saleshistory_merckid=f.fact_saleshistory_merckid
when matched then update set 
dd_billingstatus = ifnull(t.billingstatus,'Not Set'),
dd_BillingStatusOfOrdRelBillDoc = ifnull(t.BillingStatusOfOrdRelBillDoc,'Not Set'),
dd_ConfirmationStatus = ifnull(t.ConfirmationStatus,'Not Set'),
dd_deliveryblockprocessingstatus = ifnull(t.deliveryblockprocessingstatus,'Not Set'),
dd_overalldlvrblkstatuscode = ifnull(t.overalldlvrblkstatuscode,'Not Set'),
dd_DeliveryStatusheader = ifnull(t.DeliveryStatus,'Not Set'),
dd_GeneralIncompleteStatusItem = ifnull(t.GeneralIncompleteStatusItem,'Not Set'),
dd_generalincompletionprocessingstatusofheader = ifnull(t.generalincompletionprocessingstatusofheader,'Not Set'),
dd_headerincompletionstautusconcerndlvry = ifnull(t.headerincompletionstautusconcerndlvry,'Not Set'),
dd_overallblockedprocessingstatus = ifnull(t.overallblockedprocessingstatus,'Not Set'),
dd_overallblkdstatus = ifnull(t.overallblkdstatus,'Not Set'),
dd_overallblkdstatuscode = ifnull(t.overallblkdstatuscode,'Not Set'),
dd_overallcreditstatus = ifnull(t.overallcreditstatus,'Not Set'),
dd_overallcreditstatuscode = ifnull(t.overallcreditstatuscode,'Not Set'),
dd_OverallDlvrStatusOfItem = ifnull(t.OverallDlvrStatusOfItem,'Not Set'),
dd_OverallPickingStatus = ifnull(t.OverallPickingStatus,'Not Set'),
dd_OverallProcessStatusItem = ifnull(t.OverallProcessStatusItem,'Not Set'),
dd_overallrejectionprocessingstatus = ifnull(t.overallrejectionprocessingstatus,'Not Set'),
dd_overallprocessingstatusofcreditchecks = ifnull(t.overallprocessingstatusofcreditchecks,'Not Set'),
dd_RefStatusAllItems = ifnull(t.RefStatusAllItems,'Not Set'),
dd_RejectionStatus = ifnull(t.RejectionStatus,'Not Set'),
dd_rejectionstatuscode = ifnull(t.rejectionstatuscode,'Not Set'),
dd_TotalIncompleteStatusAllItems = ifnull(t.TotalIncompleteStatusAllItems,'Not Set'),
dd_TotalIncompleteStatusAllItemsCode = ifnull(t.TotalIncompleteStatusAllItemsCode,'Not Set'),
dd_totalincompletionprocessingstatusallitems = ifnull(t.totalincompletionprocessingstatusallitems,'Not Set');
 
 
/* 
update
fact_saleshistory_merck f
set dd_BillingStatusDeliveryRelated = ifnull(i.BillingStatusDeliveryRelated,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_BillingStatusDeliveryRelated <> ifnull(i.BillingStatusDeliveryRelated,'Not Set')

update
fact_saleshistory_merck f
set dd_BillingStatusOrderRelated = ifnull(i.BillingStatusOrderRelated,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_BillingStatusOrderRelated <> ifnull(i.BillingStatusOrderRelated,'Not Set')

update
fact_saleshistory_merck f
set dd_ConfirmationStatusitem = ifnull(i.ConfirmationStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_ConfirmationStatusitem <> ifnull(i.ConfirmationStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_DelayStatus = ifnull(i.DelayStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_DelayStatus <> ifnull(i.DelayStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_DeliveryStatusitem = ifnull(i.DeliveryStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_DeliveryStatusitem <> ifnull(i.DeliveryStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_GeneralIncompletionStatusitem = ifnull(i.GeneralIncompletionStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_GeneralIncompletionStatusitem <> ifnull(i.GeneralIncompletionStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_generalincompletionstatuscode = ifnull(i.generalincompletionstatuscode,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_generalincompletionstatuscode <> ifnull(i.generalincompletionstatuscode,'Not Set')

update
fact_saleshistory_merck f
set dd_generalincompletionprocessingstatusofitem = ifnull(i.generalincompletionprocessingstatusofitem,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_generalincompletionprocessingstatusofitem <> ifnull(i.generalincompletionprocessingstatusofitem,'Not Set')

update
fact_saleshistory_merck f
set dd_GoodsMovementStatus = ifnull(i.GoodsMovementStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_GoodsMovementStatus <> ifnull(i.GoodsMovementStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_ItemDeliveryBlockStatus = ifnull(i.ItemDeliveryBlockStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_ItemDeliveryBlockStatus <> ifnull(i.ItemDeliveryBlockStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_ItemRejectionStatus = ifnull(i.ItemRejectionStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_ItemRejectionStatus <> ifnull(i.ItemRejectionStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_OverallDeliveryStatus = ifnull(i.OverallDeliveryStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_OverallDeliveryStatus <> ifnull(i.OverallDeliveryStatus,'Not Set')


update
fact_saleshistory_merck f
set dd_OverallProcessingStatus = ifnull(i.OverallProcessingStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_OverallProcessingStatus <> ifnull(i.OverallProcessingStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_OverallReferenceStatus = ifnull(i.OverallReferenceStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_OverallReferenceStatus <> ifnull(i.OverallReferenceStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_PickingPutawayStatus = ifnull(i.PickingPutawayStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_PickingPutawayStatus <> ifnull(i.PickingPutawayStatus,'Not Set')

update
fact_saleshistory_merck f
set dd_PODStatus = ifnull(i.PODStatus,'Not Set')
from dim_date d, dim_salesorderitemstatus i,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and i.dim_salesorderitemstatusid = f.dim_salesorderitemstatusId
and d.datevalue = current_date
and dd_PODStatus <> ifnull(i.PODStatus,'Not Set')*/

merge into fact_saleshistory_merck f
using ( select distinct f.fact_saleshistory_merckid,
i.BillingStatusDeliveryRelated,
i.BillingStatusOrderRelated,
i.ConfirmationStatus,
i.DelayStatus,
i.DeliveryStatus,
i.GeneralIncompletionStatus,
i.generalincompletionstatuscode,
i.generalincompletionprocessingstatusofitem,
i.GoodsMovementStatus,
i.ItemDeliveryBlockStatus,
i.ItemRejectionStatus,
i.OverallDeliveryStatus,
i.OverallProcessingStatus,
i.OverallReferenceStatus,
i.PickingPutawayStatus,
i.PODStatus
from  dim_salesorderitemstatus i,fact_saleshistory_merck f
where  ifnull(i.dim_salesorderitemstatusid,1) = ifnull(f.dim_salesorderitemstatusId,1)
and f.dd_currentdate = current_date) t
on t.fact_saleshistory_merckid=f.fact_saleshistory_merckid
when matched then update set 
dd_BillingStatusDeliveryRelated = ifnull(t.BillingStatusDeliveryRelated,'Not Set'),
dd_BillingStatusOrderRelated = ifnull(t.BillingStatusOrderRelated,'Not Set'),
dd_ConfirmationStatusitem = ifnull(t.ConfirmationStatus,'Not Set'),
dd_DelayStatus = ifnull(t.DelayStatus,'Not Set'),
dd_DeliveryStatusitem = ifnull(t.DeliveryStatus,'Not Set'),
dd_GeneralIncompletionStatusitem = ifnull(t.GeneralIncompletionStatus,'Not Set'),
dd_generalincompletionstatuscode = ifnull(t.generalincompletionstatuscode,'Not Set'),
dd_generalincompletionprocessingstatusofitem = ifnull(t.generalincompletionprocessingstatusofitem,'Not Set'),
dd_GoodsMovementStatus = ifnull(t.GoodsMovementStatus,'Not Set'),
dd_ItemDeliveryBlockStatus = ifnull(t.ItemDeliveryBlockStatus,'Not Set'),
dd_ItemRejectionStatus = ifnull(t.ItemRejectionStatus,'Not Set'),
dd_OverallDeliveryStatus = ifnull(t.OverallDeliveryStatus,'Not Set'),
dd_OverallProcessingStatus = ifnull(t.OverallProcessingStatus,'Not Set'),
dd_OverallReferenceStatus = ifnull(t.OverallReferenceStatus,'Not Set'),
dd_PickingPutawayStatus = ifnull(t.PickingPutawayStatus,'Not Set'),
dd_PODStatus = ifnull(t.PODStatus,'Not Set');

/*update
fact_saleshistory_merck f
set dd_rejectreasoncode = ifnull(r.rejectreasoncode,'Not Set')
from dim_date d, dim_salesorderrejectreason r,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and r.dim_salesorderrejectreasonid = f.dim_salesorderrejectreasonid
and d.datevalue = current_date
and dd_rejectreasoncode <> ifnull(r.rejectreasoncode,'Not Set')

update
fact_saleshistory_merck f
set dd_rejectreasondesc = ifnull(r.description,'Not Set')
from dim_date d, dim_salesorderrejectreason r,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and r.dim_salesorderrejectreasonid = f.dim_salesorderrejectreasonid
and d.datevalue = current_date
and dd_rejectreasondesc <> ifnull(r.description,'Not Set')*/

merge into fact_saleshistory_merck f
using ( select distinct f.fact_saleshistory_merckid,
r.rejectreasoncode,
r.description
from  dim_salesorderrejectreason r,fact_saleshistory_merck f
where  ifnull(r.dim_salesorderrejectreasonid,1) = ifnull(f.dim_salesorderrejectreasonid,1)
and f.dd_currentdate = current_date) t
on t.fact_saleshistory_merckid=f.fact_saleshistory_merckid
when matched then update set 
dd_rejectreasoncode = ifnull(t.rejectreasoncode,'Not Set'),
dd_rejectreasondesc = ifnull(t.description,'Not Set');

/*update
fact_saleshistory_merck f
set dd_salesriskcategorycode = ifnull(r.salesriskcategory,'Not Set')
from dim_date d, dim_salesriskcategory r,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and r.dim_salesriskcategoryid = f.dim_salesriskcategoryid
and d.datevalue = current_date
and dd_salesriskcategorycode <> ifnull(r.salesriskcategory,'Not Set')

update
fact_saleshistory_merck f
set dd_salesriskcategorydesc = ifnull(r.salesriskcategorydescription,'Not Set')
from dim_date d, dim_salesriskcategory r,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and r.dim_salesriskcategoryid = f.dim_salesriskcategoryid
and d.datevalue = current_date
and dd_salesriskcategorydesc <> ifnull(r.salesriskcategorydescription,'Not Set')*/

merge into fact_saleshistory_merck f
using ( select distinct f.fact_saleshistory_merckid,
r.salesriskcategory,
r.salesriskcategorydescription
from  dim_salesriskcategory r,fact_saleshistory_merck f
where  ifnull(r.dim_salesriskcategoryid,1) = ifnull(f.dim_salesriskcategoryid,1)
and f.dd_currentdate = current_date) t
on t.fact_saleshistory_merckid=f.fact_saleshistory_merckid
when matched then update set 
dd_salesriskcategorycode = ifnull(t.salesriskcategory,'Not Set'),
dd_salesriskcategorydesc = ifnull(t.salesriskcategorydescription,'Not Set');

/*update
fact_saleshistory_merck f
set dd_deliveryblock = ifnull(db.deliveryblock,'Not Set')
from dim_date d, dim_deliveryblock db,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and db.dim_deliveryblockid = f.dim_deliveryblockid
and d.datevalue = current_date
and dd_deliveryblock <> ifnull(db.deliveryblock,'Not Set')

update
fact_saleshistory_merck f
set dd_deliveryblockdesc = ifnull(db.description,'Not Set')
from dim_date d, dim_deliveryblock db,fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and db.dim_deliveryblockid = f.dim_deliveryblockid
and d.datevalue = current_date*/

merge into fact_saleshistory_merck f
using ( select distinct f.fact_saleshistory_merckid,
db.deliveryblock,
db.description
from  dim_deliveryblock db,fact_saleshistory_merck f
where  ifnull(db.dim_deliveryblockid,1) = ifnull(f.dim_deliveryblockid,1)
and f.dd_currentdate = current_date) t
on t.fact_saleshistory_merckid=f.fact_saleshistory_merckid
when matched then update set 
dd_deliveryblock = ifnull(t.deliveryblock,'Not Set'),
dd_deliveryblockdesc = ifnull(t.description,'Not Set');
/*29 Aug End of  Changes*/

/*Andrian - based on Bi-1675*/
drop table if exists tmp01_actualtransittimecalendardays;
create table tmp01_actualtransittimecalendardays as
select dd_salesdocno,dd_salesitemno,dd_scheduleno,avg(art.transittimeincalendardays) as actualtransittimecalendardays from  	fact_salesorderdelivery a
,dim_route art
where ifnull(a.dim_actualrouteid,1) = ifnull(art.dim_routeid,1)
group by dd_salesdocno,dd_salesitemno,dd_scheduleno;


update fact_saleshistory_merck f
set ct_actualtransittimecalendardays = ifnull(h.actualtransittimecalendardays,0)
from dim_date d, tmp01_actualtransittimecalendardays h, fact_saleshistory_merck f
where d.dim_dateid = f.dim_Dateidsnapshot
and d.datevalue = current_date
and f.dd_salesdocno = h.dd_salesdocno
and f.dd_salesitemno = h.dd_salesitemno
and f.dd_scheduleno = h.dd_scheduleno
and ct_actualtransittimecalendardays <> ifnull(h.actualtransittimecalendardays,0);

drop table if exists tmp01_actualtransittimecalendardays;