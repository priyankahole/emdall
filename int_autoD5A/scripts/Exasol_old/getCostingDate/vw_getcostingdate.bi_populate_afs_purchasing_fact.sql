/* START OF CODE BLOCKS - tmp_getcostingdate would have the final the data    */

DELETE FROM tmp_getcostingdate
WHERE fact_script_name = 'bi_populate_afs_purchasing_fact';

INSERT INTO tmp_getcostingdate
(pCompanyCode,
pPlant,             
pMaterialNo,            
pFiYear,     
pPeriod, 
fact_script_name
)       
SELECT DISTINCT EKPO_BUKRS pCompanyCode, EKPO_WERKS pPlant, EKPO_MATNR pMaterialNo, dt.FinancialYear pFiYear, dt.FinancialMonthNumber pPeriod, 
	 'bi_populate_afs_purchasing_fact' fact_script_name
FROM ekko_ekpo_eket, dim_date dt,fact_purchase fp
WHERE     fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket x
				 where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
AND dt.CompanyCode = EKPO_BUKRS
AND fp.amt_StdUnitPrice = 0;



