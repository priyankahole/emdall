set -o errexit
holdout_date=$1
#echo"holdout date"holdout_date
echo $1


USER1=$(whoami)
echo $USER1
echo -e "\e[31;43m***** User trying to run is $USER1 *****\e[0m"

echo -e "\e[31;43m***** TEST SCRIPT FOR MAH *****\e[0m"
HOST=10.102.6.19
#status=$(ssh -o BatchMode=yes -o ConnectTimeout=5 fusionops@$HOST echo ok 2>&1)
status=ok
if [[ $status == ok ]]; then
        echo SSH passed to target server, moving on...
        ssh -o StrictHostKeyChecking=no fusionops@$HOST '/home/fusionops/merck_forecast_automation_3MHO.sh'


elif [[ $status == "Permission denied"* ]] ; then
    echo I am unable to communicate to target host
fi
