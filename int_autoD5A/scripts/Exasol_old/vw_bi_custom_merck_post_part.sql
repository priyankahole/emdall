
/* Madalina 27 Oct 2016 - Reset field for deleted AUSP or CAWNT records - BI-4519 */
/* UPDATE dim_part dp
  SET ItemType_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp 
FROM AUSP a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 14
  AND a.AUSP_KLART = 1
  AND ItemType_Merck <> AUSP_ATWRT */

merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
 from dim_part dp
	left join ausp a
  		on dp.PartNumber = a.AUSP_OBJEK
		  AND a.AUSP_ATINN = 14
		  AND a.AUSP_KLART = 1  
where dp.RowIsCurrent = 1
	 AND ItemType_Merck <> ifnull(AUSP_ATWRT, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update	 
set ItemType_Merck = ifnull(AUSP_ATWRT, 'Not Set') 
  ,dw_update_date = current_timestamp;  

/* UPDATE dim_part dp
  SET ItemTypeDescription_Merck = c.CAWNT_ATWTB
  ,dw_update_date = current_timestamp 
FROM AUSP a, CAWNT c
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = c.CAWN_ATINN
  AND a.AUSP_ATWRT = c.CAWN_ATWRT
  AND a.AUSP_ATINN = 14
  AND a.AUSP_KLART = 1
  AND ItemTypeDescription_Merck <> c.CAWNT_ATWTB */
  
merge into dim_part dp using
(select distinct dim_partid,  ifnull(c.CAWNT_ATWTB, 'Not Set') as CAWNT_ATWTB
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		AND dp.RowIsCurrent = 1
		AND a.AUSP_ATINN = 14
		AND a.AUSP_KLART = 1
	left join CAWNT c
		on a.AUSP_ATINN = c.CAWN_ATINN
		AND a.AUSP_ATWRT = c.CAWN_ATWRT
where ItemTypeDescription_Merck <> ifnull(c.CAWNT_ATWTB, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set ItemTypeDescription_Merck = upd.CAWNT_ATWTB
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET ItemSubType_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp 
FROM AUSP a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 13
  AND a.AUSP_KLART = 1
  AND ItemSubType_Merck <> AUSP_ATWRT */
  
 merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
 from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		  AND dp.RowIsCurrent = 1
		  AND a.AUSP_ATINN = 13
		  AND a.AUSP_KLART = 1
where ItemSubType_Merck <>  ifnull(AUSP_ATWRT, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update	
set ItemSubType_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET ItemSubTypeDescription_Merck = c.CAWNT_ATWTB
  ,dw_update_date = current_timestamp 
FROM AUSP a, CAWNT c
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = c.CAWN_ATINN
  AND a.AUSP_ATWRT = c.CAWN_ATWRT
  AND a.AUSP_ATINN = 13
  AND a.AUSP_KLART = 1
  AND ItemSubTypeDescription_Merck <> c.CAWNT_ATWTB */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(c.CAWNT_ATWTB, 'Not Set') as CAWNT_ATWTB
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		AND dp.RowIsCurrent = 1
		AND a.AUSP_ATINN = 13
		AND a.AUSP_KLART = 1
	left join CAWNT c
		on a.AUSP_ATINN = c.CAWN_ATINN
		AND a.AUSP_ATWRT = c.CAWN_ATWRT
where ItemSubTypeDescription_Merck <> ifnull(c.CAWNT_ATWTB, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update	
set ItemSubTypeDescription_Merck = upd.CAWNT_ATWTB
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET ProductFamily_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp 
FROM AUSP a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 16
  AND a.AUSP_KLART = 1
  AND ProductFamily_Merck <> AUSP_ATWRT */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
 from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		  AND dp.RowIsCurrent = 1
		  AND a.AUSP_ATINN = 16
		  AND a.AUSP_KLART = 1
where ProductFamily_Merck <> ifnull(AUSP_ATWRT, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update	
set ProductFamily_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp;

UPDATE dim_part dp
  SET ProductFamilyDescription_Merck = mkt.MARA_WRKST
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (SELECT DISTINCT MARA_MATNR,MARA_WRKST FROM MARA_MARC_MAKT) mkt
, dim_part dp
  WHERE dp.PartNumber = mkt.MARA_MATNR
  AND dp.RowIsCurrent = 1
  AND ProductFamilyDescription_Merck <> mkt.MARA_WRKST;

/* UPDATE dim_part dp
  SET ProductGroup_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp 
FROM AUSP a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 129
  AND a.AUSP_KLART = 1
  AND ProductGroup_Merck <> AUSP_ATWRT */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		  AND dp.RowIsCurrent = 1
		  AND a.AUSP_ATINN = 129
		  AND a.AUSP_KLART = 1
where ProductGroup_Merck <> ifnull(AUSP_ATWRT, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set ProductGroup_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET ProductGroupDescription_Merck = c.CAWNT_ATWTB
  ,dw_update_date = current_timestamp 
FROM AUSP a, CAWNT c
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = c.CAWN_ATINN
  AND a.AUSP_ATWRT = c.CAWN_ATWRT
  AND a.AUSP_ATINN = 129
  AND a.AUSP_KLART = 1
  AND ProductGroupDescription_Merck <> c.CAWNT_ATWTB */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(c.CAWNT_ATWTB, 'Not Set') as CAWNT_ATWTB 
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		AND dp.RowIsCurrent = 1
		AND a.AUSP_ATINN = 129
		AND a.AUSP_KLART = 1
	left join CAWNT c
		on a.AUSP_ATINN = c.CAWN_ATINN
		AND a.AUSP_ATWRT = c.CAWN_ATWRT
where ProductGroupDescription_Merck <> ifnull(c.CAWNT_ATWTB, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set ProductGroupDescription_Merck = upd.CAWNT_ATWTB
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET DominantSpecies_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp 
FROM AUSP a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 128
  AND a.AUSP_KLART = 1
  AND  DominantSpecies_Merck <> AUSP_ATWRT */
 
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		  AND dp.RowIsCurrent = 1
		  AND a.AUSP_ATINN = 128
		  AND a.AUSP_KLART = 1
where DominantSpecies_Merck <> ifnull(AUSP_ATWRT, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set DominantSpecies_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET DominantSpeciesDescription_Merck = c.CAWNT_ATWTB
  ,dw_update_date = current_timestamp 
FROM AUSP a, CAWNT c
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = c.CAWN_ATINN
  AND a.AUSP_ATWRT = c.CAWN_ATWRT
  AND a.AUSP_ATINN = 128
  AND a.AUSP_KLART = 1
  AND DominantSpeciesDescription_Merck <> c.CAWNT_ATWTB */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(c.CAWNT_ATWTB, 'Not Set') as CAWNT_ATWTB
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		AND dp.RowIsCurrent = 1
		AND a.AUSP_ATINN = 128
		AND a.AUSP_KLART = 1
	left join CAWNT c
		on a.AUSP_ATINN = c.CAWN_ATINN
		AND a.AUSP_ATWRT = c.CAWN_ATWRT
where  DominantSpeciesDescription_Merck <> ifnull(c.CAWNT_ATWTB, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set DominantSpeciesDescription_Merck = upd.CAWNT_ATWTB
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET ProductType_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp 
FROM AUSP a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 127
  AND a.AUSP_KLART = 1
  AND ProductType_Merck <> AUSP_ATWRT */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		  AND dp.RowIsCurrent = 1
		  AND a.AUSP_ATINN = 127
		  AND a.AUSP_KLART = 1
where ProductType_Merck <> ifnull(AUSP_ATWRT, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set ProductType_Merck = AUSP_ATWRT
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET ProductTypeDescription_Merck = c.CAWNT_ATWTB
  ,dw_update_date = current_timestamp 
FROM AUSP a, CAWNT c
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = c.CAWN_ATINN
  AND a.AUSP_ATWRT = c.CAWN_ATWRT
  AND a.AUSP_ATINN = 127
  AND a.AUSP_KLART = 1
  AND ProductTypeDescription_Merck <> c.CAWNT_ATWTB */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(c.CAWNT_ATWTB, 'Not Set') as CAWNT_ATWTB
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		AND dp.RowIsCurrent = 1
		AND a.AUSP_ATINN = 127
		AND a.AUSP_KLART = 1
	left join CAWNT c
		on a.AUSP_ATINN = c.CAWN_ATINN
		AND a.AUSP_ATWRT = c.CAWN_ATWRT
where ProductTypeDescription_Merck <> ifnull(c.CAWNT_ATWTB, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set ProductTypeDescription_Merck = upd.CAWNT_ATWTB
  ,dw_update_date = current_timestamp;
  
/* END BI-4519 */

UPDATE dim_part
SET ItemType_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ItemType_Merck IS NULL;

UPDATE dim_part
SET ItemTypeDescription_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ItemTypeDescription_Merck IS NULL;

UPDATE dim_part
SET ItemSubType_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ItemSubType_Merck IS NULL;

UPDATE dim_part
SET ItemSubTypeDescription_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ItemSubTypeDescription_Merck IS NULL;

UPDATE dim_part
SET ProductGroup_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ProductGroup_Merck IS NULL;

UPDATE dim_part
SET ProductGroupDescription_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ProductGroupDescription_Merck IS NULL;

UPDATE dim_part
SET ProductFamily_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ProductFamily_Merck IS NULL;

UPDATE dim_part
SET ProductFamilyDescription_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ProductFamilyDescription_Merck IS NULL;

UPDATE dim_part
SET DominantSpecies_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE DominantSpecies_Merck IS NULL;

UPDATE dim_part
SET DominantSpeciesDescription_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE DominantSpeciesDescription_Merck IS NULL;

UPDATE dim_part dp
SET ProductType_Merck = 1
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ProductType_Merck IS NULL;

UPDATE dim_part
SET ProductTypeDescription_Merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ProductTypeDescription_Merck IS NULL;

UPDATE dim_part
SET prodfamilydescription_merck = trim(substring(productfamilydescription_merck,6,42))
WHERE productfamilydescription_merck <> 'Not Set';

/*Andrei - APP-10656 */
UPDATE dim_part
SET prodfamilydescription_merck = 'Not Set'
WHERE productfamilydescription_merck = 'Not Set';
/* end Andrei - APP-10656 */

UPDATE dim_part
SET prodfamilydescription_merck = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE prodfamilydescription_merck IS NULL;

/*16 Feb 2017 Georgiana Changes according to BI-5499*/
merge into dim_part dp
using
( select distinct first_value(prodfamilydescription_merck) over (partition by ProductFamily_Merck order by xcount desc) as gpf_consolidated ,ProductFamily_Merck
from
(select ProductFamily_Merck,prodfamilydescription_merck, count(*) as xcount
from dim_part
where
prodfamilydescription_merck <> 'Not Set'

group by ProductFamily_Merck,prodfamilydescription_merck) t) src
on dp.ProductFamily_Merck=src.ProductFamily_Merck
when matched then update set prodfamilydescription_merck_consolidated=src.gpf_consolidated;
/* 16 Feb 2017 End of changes*/



update dim_part dp
set dp.productpriorityint_merck = convert(decimal (5,2),0.00)
from dim_part dp
where not exists ( select 1
from merck_import_top75rank_gpf m
where dp.productfamily_Merck = lpad(m.gpf,4,'0')
)
and dp.productpriorityint_merck <> convert(decimal (5,2),0.00);


update dim_part dp
set dp.productpriorityint_merck = convert(decimal (7,2),m.top75rank)
from (SELECT gpf,MAX(top75rank) AS top75rank from merck_import_top75rank_gpf group by gpf) m
, dim_part dp
where dp.productfamily_Merck = lpad(m.gpf,4,'0')
and dp.productpriorityint_merck <> convert(decimal (5,2),m.top75rank);
update dim_part dp set productpriorityint_merck = 999 where productpriorityint_merck = 0.00;

/* Madalina 26 Oct 2016 - Reset field for deleted AUSP records - BI-4519 */
/* UPDATE dim_part dp
  SET primarysite = AUSP_ATWRT
  ,dw_update_date = current_timestamp 
FROM AUSP a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 438
  AND a.AUSP_KLART = 1
  AND primarysite <> AUSP_ATWRT */
  

/* APP-10532 the primary site is not correctly updated */
merge into dim_part dp using 
(select  distinct dim_partid, ifnull(AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
 from  dim_part dp
	left join ausp a
  		on  dp.PartNumber = a.AUSP_OBJEK
		  AND a.AUSP_ATINN = 438
		  AND a.AUSP_KLART = 1
where primarysite <> ifnull(AUSP_ATWRT, 'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set primarysite = ifnull(AUSP_ATWRT, 'Not Set')
 ,dw_update_date = current_timestamp;

UPDATE dim_part
SET primarysite = 'Not Set'
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE primarysite IS NULL;

update dim_part
set PrimarySiteTitle = 'Not Set'
where  PrimarySiteTitle <> 'Not Set';

UPDATE dim_part dp
  SET dp.PrimarySiteTitle = ifnull(mpt.primarysettitle,'Not Set')
FROM tmp_plantattributes mpt
, dim_part dp
  WHERE dp.primarysite = mpt.plantcode
and dp.PrimarySiteTitle <> ifnull(mpt.primarysettitle,'Not Set');

/* UPDATE dim_part dp
  SET dp.concentration = IFNULL(ausp_atflv,'0.00')
  ,dw_update_date = current_timestamp 
FROM ausp a
, dim_part dp
  WHERE dp.partnumber = a.ausp_objek
  AND dp.rowiscurrent = 1
  AND a.ausp_atinn = 6
  AND a.ausp_klart = '001'
  AND dp.concentration <> IFNULL(ausp_atflv,'0.00') */
  
  /*12 May 2017 Georgiana according to APP 6205 removed the ifnull statement in order to be able to identify zeroes from nulls*/
  /*12 Sep 2017 Reverting back the changes according to APP-7473*/
merge into dim_part dp using 
(select distinct dim_partid, ifnull(ausp_atflv,'0.00') as ausp_atflv
from dim_part dp
	left join ausp a
  		on dp.partnumber = a.ausp_objek
			 and a.ausp_atinn = 6 
			 and a.ausp_klart = '001'
where dp.rowiscurrent = 1
  /*AND dp.concentration <> ifnull(ausp_atflv,'0.00')*/) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set dp.concentration = ifnull(ausp_atflv,'0.00'),
 dw_update_date = current_timestamp ;
 /*12 May 2017 End*/

/* UPDATE dim_part dp
  SET dp.ConcentrationUOM = IFNULL(ausp_atwrt,'Not Set')
  ,dw_update_date = current_timestamp 
FROM ausp a
, dim_part dp
  WHERE dp.Partnumber = a.ausp_objek
  AND dp.rowiscurrent = 1
  AND a.ausp_atinn = 7
  AND a.ausp_klart = '001'
  AND dp.ConcentrationUOM <> IFNULL(ausp_atwrt,'Not Set') */
  
merge into dim_part dp using 
(select distinct dim_partid,  IFNULL(ausp_atwrt,'Not Set') as ausp_atwrt
from dim_part dp
	left join ausp a
		on dp.Partnumber = a.ausp_objek
		  AND a.ausp_atinn = 7
		  AND a.ausp_klart = '001'
where dp.rowiscurrent = 1
	and dp.ConcentrationUOM <> IFNULL(ausp_atwrt,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set dp.ConcentrationUOM = IFNULL(ausp_atwrt,'Not Set')
  ,dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET dp.contents = IFNULL(ausp_atflv,'0.00')
  ,dw_update_date = current_timestamp 
FROM ausp a
, dim_part dp
  WHERE dp.partnumber = a.ausp_objek
  AND dp.rowiscurrent = 1
  AND a.ausp_atinn = 8
  AND a.ausp_klart = '001'
  AND dp.contents <> IFNULL(ausp_atflv,'0.00') */
  
merge into dim_part dp using
(select distinct dim_partid, IFNULL(ausp_atflv,'0.00') as ausp_atflv
from dim_part dp
	left join ausp a
		on dp.partnumber = a.ausp_objek
		  AND a.ausp_atinn = 8
		  AND a.ausp_klart = '001'
where dp.rowiscurrent = 1	
	and dp.contents <> IFNULL(ausp_atflv,'0.00')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set dp.contents = IFNULL(ausp_atflv,'0.00')
 ,dw_update_date = current_timestamp;
	
/* UPDATE dim_part dp
  SET dp.contentsuom = IFNULL(ausp_atwrt,'Not Set')
  ,dw_update_date = current_timestamp 
FROM ausp a
, dim_part dp
  WHERE dp.partnumber = a.ausp_objek
  AND dp.rowiscurrent = 1
  AND a.ausp_atinn = 9
  AND a.ausp_klart = '001'
  AND dp.contentsuom <> IFNULL(ausp_atwrt,'Not Set') */
  
 merge into dim_part dp using
(select distinct dim_partid, IFNULL(ausp_atwrt,'Not Set') as ausp_atwrt
from dim_part dp
	left join ausp a
		on dp.partnumber = a.ausp_objek
		  AND a.ausp_atinn = 9
		  AND a.ausp_klart = '001'
where dp.rowiscurrent = 1
	and dp.contentsuom <> IFNULL(ausp_atwrt,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set dp.contentsuom = IFNULL(ausp_atwrt,'Not Set')
  ,dw_update_date = current_timestamp;

/* END BI-4519 */
  
/* 3 dec 2014 */
/* 12 Dec 2016 Georgiana Change according to BI 4934 changing hte logic of batchclass_merck in order to be like batchclass_klah*/
/*merge into dim_part d
using (select distinct d.dim_partid, b.class, row_number() over (partition by d.dim_partid order by d.dim_partid) as rowseqno
 from kssk_inob a, klah b
 , dim_part d
 where obtab = 'MARA'
 and a.clint = b.clint
 and TRIM(LEADING '0' FROM a.inob_objek) = TRIM(LEADING '0' FROM d.partnumber)
 and KLART= '023') t on d.dim_partid=t.dim_partid
when matched then update set batchclass_merck = t.class
where
rowseqno=1
AND d.batchclass_merck <> t.class*/

/*Madalina 20170612 - For STG env only: do not reset BatchClass material attribute - APP-5685*/
/* update dim_part set batchclass_merck = 'Not Set' */

drop table if exists tmp_kssk_for_batchclass_merck;
create table tmp_kssk_for_batchclass_merck as
select * from  kssk a
where a.kssk_objek REGEXP_LIKE '[0-9]*' = 'TRUE';

drop table if exists tmp_upd_batchclass_merck;
create table tmp_upd_batchclass_merck as
select distinct dim_partid, FIRST_VALUE(b.class) over (partition by dim_partid order by dim_partid) as class
from tmp_kssk_for_batchclass_merck a, klah b, inob i,dim_part d
where
	i.inob_objek = d.partnumber
	and i.inob_obtab = 'MARA'
	and i.inob_cuobj = a.kssk_objek
	and a.kssk_KLART = '023'
	and a.kssk_clint = b.clint
	and b.klart = '023';

update dim_part d
set batchclass_merck = class,
	dw_update_date = CURRENT_TIMESTAMP
from tmp_upd_batchclass_merck t, dim_part d
where d.dim_partid = t.dim_partid
	/*and rn = 1*/
	and batchclass_merck <> class;
	
drop table if exists tmp_kssk_for_batchclass_merck;
drop table if exists tmp_upd_batchclass_merck;

/*merge into dim_part d
using (select distinct d.dim_partid,  c.kschl, row_number() over (partition by d.dim_partid order by d.dim_partid) as rowseqno
from kssk_inob a,  swor c
, dim_part d
where obtab = 'MARA'
and a.clint = c.clint
and TRIM(LEADING '0' FROM a.inob_objek) = TRIM(LEADING '0' FROM d.partnumber)
and spras = 'E' and klpos =1) t on t.dim_partid=d.dim_partid
when matched then update set batchclassdescription_merck =  t.kschl
where rowseqno=1
and batchclassdescription_merck <>  t.kschl*/

update dim_part set batchclassdescription_merck = 'Not Set';

drop table if exists tmp_kssk_for_batchclassdescription_merck;
create table tmp_kssk_for_batchclassdescription_merck as
select * from  kssk a
where a.kssk_objek REGEXP_LIKE '[0-9]*' = 'TRUE';

drop table if exists tmp_upd_batchclass_merck;
create table tmp_upd_batchclass_merck as
select distinct dim_partid, FIRST_VALUE(c.kschl) over (partition by dim_partid order by dim_partid) as kschl
from tmp_kssk_for_batchclassdescription_merck a, klah b, inob i,dim_part d,swor c
where
	i.inob_objek = d.partnumber
	and i.inob_obtab = 'MARA'
	and i.inob_cuobj = a.kssk_objek
	and a.kssk_KLART = '023'
	and a.kssk_clint = b.clint
    and a.kssk_clint = c.clint
	and b.klart = '023'
    and spras = 'E' and klpos =1;

update dim_part d
set batchclassdescription_merck = kschl,
	dw_update_date = CURRENT_TIMESTAMP
from tmp_upd_batchclass_merck t, dim_part d
where d.dim_partid = t.dim_partid
	/*and rn = 1*/
	and batchclassdescription_merck <> kschl;
/* End of 12 Dec 2016 Changes*/

/*Alin 18 0ct 2017 fix APP-7791*/
update dim_part
set procureplant = 'Not Set';

update dim_part pt
set pt.procureplant = ifnull(t460a_wrk02,'Not Set')
from t460a a, t460t t
, dim_part pt
where t460a_werks = t460t_werks
and t460a_sobsl = t460t_sobsl
and pt.SpecialProcurement = t460t_sobsl
and pt.Plant = t460t_werks;

/*Alin APP-7812 30 Oct 2017*/
update dim_part
set procure_from_country = 'Not Set';

update dim_part pt
set pt.procure_from_country = ifnull(z.LAND1, 'Not Set')
from t460a a, t460t t, dim_part pt, T001W_NOSPRAS z
where t460a_werks = t460t_werks
and t460a_sobsl = t460t_sobsl
and pt.SpecialProcurement = t460t_sobsl
and pt.Plant = t460t_werks
and pt.procureplant = ifnull(t460a_wrk02,'Not Set')
and z.werks = t460a_wrk02;

/* updates from Material Master file - 2 feb 2015 */

 /*update dim_part pt
   set pt.reportingcompanycode_merck = m.reportingcompanycode
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*from materialmaster_merck m
, dim_part pt
   where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.reportingcompanycode_merck <> m.reportingcompanycode
*/

drop table if exists upd_part_materialmaster_merck;
create table upd_part_materialmaster_merck as
select max(reportingcompanycode) as reportingcompanycode,max(ispgcode ) as ispgcode,
max(ispg) as ispg, max(fppsuperfamilycode) as fppsuperfamilycode,
max(fppsuperfamily) as fppsuperfamily, max(packingunit) as packingunit,
max(therapeuticalclasscode) as therapeuticalclasscode,
max(therapeuticalclass) as therapeuticalclass, max(gemtollername) as gemtollername,
max(healthplatform) as healthplatform,max(manufacturingsitecode) as manufacturingsitecode,
 uin, primarysitecode   from materialmaster_merck
group by  uin, primarysitecode;

update dim_part pt
   set pt.reportingcompanycode_merck = m.reportingcompanycode
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

from upd_part_materialmaster_merck m
, dim_part pt
   where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.reportingcompanycode_merck <> m.reportingcompanycode;

/* Madalina 27 Oct 2016 - Reset field for deleted AUSP records - BI-4519 */
/* ispgcode_merck  changed to extract data from SAP table */
/* update dim_part pt
set pt.ispgcode_merck     = a.AUSP_ATWRT
,dw_update_date     = current_timestamp
from ausp a
, dim_part pt
where     pt.partnumber   = a.AUSP_OBJEK
    and A.AUSP_ATINN  = '0000000132'
    and pt.ispgcode_merck <> a.AUSP_ATWRT */
	
merge into dim_part pt using
(select distinct dim_partid, ifnull(a.AUSP_ATWRT, 'Not Set') as AUSP_ATWRT
from dim_part pt
	left join ausp a
		on pt.partnumber   = a.AUSP_OBJEK
		and A.AUSP_ATINN  = '0000000132'
where pt.ispgcode_merck <> ifnull(a.AUSP_ATWRT, 'Not Set') ) upd
on pt.dim_partid = upd.dim_partid
when matched then update
set pt.ispgcode_merck     = upd.AUSP_ATWRT
,dw_update_date     = current_timestamp;
	
/* end update ispgcode_merck*/
/* END BI-4519 */

  update dim_part pt
set pt.ispg_merck = m.ispg
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   from /*materialmaster_merck*/
upd_part_materialmaster_merck   m, dim_part  pt
where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.ispg_merck <> m.ispg;

  update dim_part pt
set pt.fppsuperfamilycode_merck = m.fppsuperfamilycode
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
    from /*materialmaster_merck*/
upd_part_materialmaster_merck   m, dim_part pt
where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.fppsuperfamilycode_merck <> m.fppsuperfamilycode;

  update dim_part pt
set pt.fppsuperfamily_merck = m.fppsuperfamily
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   from /*materialmaster_merck*/
upd_part_materialmaster_merck   m, dim_part pt
where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.fppsuperfamily_merck <> m.fppsuperfamily;

/*  Madalina 19 Jul 2016 - packingunit_merck should have the same values as packingunit
update dim_part pt
set pt.packingunit_merck = m.packingunit
,dw_update_date = current_timestamp Added automatically by update_dw_update_date.pl
   from materialmaster_merck
upd_part_materialmaster_merck   m, dim_part pt
where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.packingunit_merck <> m.packingunit */

update dim_part pt 
set pt.packingunit_merck = pt.packingunit  
,dw_update_date = current_timestamp 
where pt.packingunit_merck <> pt.packingunit;
/* END 19 Jul 2016 */

  update dim_part pt
set pt.therapeuticalclasscode_merck = m.therapeuticalclasscode
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
    from /*materialmaster_merck*/
upd_part_materialmaster_merck   m, dim_part pt
where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.therapeuticalclasscode_merck <> m.therapeuticalclasscode;


  update dim_part pt
set pt.therapeuticalclass_merck = m.therapeuticalclass
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
    from /*materialmaster_merck*/
upd_part_materialmaster_merck   m, dim_part pt
where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.therapeuticalclass_merck <> m.therapeuticalclass;


  update dim_part pt
set pt.gemtollername_merck = m.gemtollername
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
    from /*materialmaster_merck*/
upd_part_materialmaster_merck   m, dim_part pt
where lpad(uin,6,'0') = pt.partnumber and pt.plant = primarysitecode
and pt.gemtollername_merck <> m.gemtollername;


 update dim_part pt
set pt.healthplatform_merck = m.healthplatform
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
    from /*materialmaster_merck*/
upd_part_materialmaster_merck   m, dim_part pt
where lpad(uin,6,'0') = pt.partnumber AND pt.plant = primarysitecode
and pt.healthplatform_merck <> m.healthplatform;

 update dim_part pt
   set pt.manufacturingsitecode_merck = m.manufacturingsitecode_merck
from manufacturingsitecode m
, dim_part pt
   where m.partnumber = pt.partnumber
and pt.manufacturingsitecode_merck <> m.manufacturingsitecode_merck;


 /* end updates from Material Master file - 2 feb 2015 */

 /* 13 feb 2015 GPF Priority Consolidated */

update dim_part dp
set dp.prodpriorityconsolidated_merck = 'Not Set';

/*update dim_part dp
set dp.prodpriorityconsolidated_merck = m.prodpriorityconsolidated
from merck_import_gpf_consolidated m
, dim_part dp
where dp.productfamily_merck = lpad(m.gpfcode,4,'0')
and dp.ispgcode_merck = m.ispgcode
and  dp.prodpriorityconsolidated_merck <> m.prodpriorityconsolidated
*/
drop table if exists upd_part_merck_import_gpf_consolidated;
create table upd_part_merck_import_gpf_consolidated as
select gpfcode,ispgcode, max(prodpriorityconsolidated) as
prodpriorityconsolidated
from merck_import_gpf_consolidated
group by gpfcode,ispgcode;

update dim_part dp
set dp.prodpriorityconsolidated_merck = m.prodpriorityconsolidated
from upd_part_merck_import_gpf_consolidated m
, dim_part dp
where dp.productfamily_merck = lpad(m.gpfcode,4,'0')
and dp.ispgcode_merck = m.ispgcode
and  dp.prodpriorityconsolidated_merck <> m.prodpriorityconsolidated;




 /* end 13 feb 2015 GPF Priority Consolidated */

/* 20 feb 2015 */

 update dim_part pt
set safetystockiru_merck = safetystock/marm_umrez
from  MARM m
, dim_part pt
where m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and safetystockiru_merck <> safetystock/marm_umrez;

 update dim_part pt
set fixedlotsizeiru_merck = fixedlotsize/marm_umrez
from  MARM m
, dim_part pt
where m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and fixedlotsizeiru_merck <> fixedlotsize/marm_umrez;

 update dim_part pt
set minimumlotsizeiru_merck = minimumlotsize/marm_umrez
from  MARM m
, dim_part pt
where m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and minimumlotsizeiru_merck <> minimumlotsize/marm_umrez;

 update dim_part pt
set maxlotsizeiru_merck = maxlotsize/marm_umrez
from  MARM m
, dim_part pt
where m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and maxlotsizeiru_merck <> maxlotsize/marm_umrez;
/* end 20 feb 2015 */


update
dim_part pt set grproctimecaldays_merck = ifnull(case when pl.tacticalring_merck = 'ComOps' then
convert(decimal (18,0), convert(decimal (18,2),grprocessingtime)/5*7) + ifnull(leadtime,0)
else convert(decimal (18,0), convert(decimal (18,2),grprocessingtime)/5*7) end,0)
from dim_plant pl, dim_part pt
where pt.plant = pl.plantcode
and grproctimecaldays_merck <> ifnull(case when pl.tacticalring_merck = 'ComOps' then
convert(decimal (18,0), convert(decimal (18,2),grprocessingtime)/5*7) + ifnull(leadtime,0)
else convert(decimal (18,0), convert(decimal (18,2),grprocessingtime)/5*7) end,0);

update dim_part
set MRPControllerPlantCode = ifnull(concat(plant,'/',MRPController),'Not Set')
where MRPControllerPlantCode <> ifnull(concat(plant,'/',MRPController),'Not Set');

/* Octavian : Every angle migration */
/* Madalina 27 Oct 2016 - Reset field for deleted AUSP or CAWNT records - BI-4519 */
/* UPDATE dim_part dp
  SET additionalspec = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 5 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND additionalspec <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 5 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND additionalspec <> ifnull(AUSP_ATWRT,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set additionalspec = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET itemspecification = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 12 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND itemspecification <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 12 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND itemspecification <> ifnull(AUSP_ATWRT,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update  
set itemspecification = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET containertype = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 19 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND containertype <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 19 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND containertype <> ifnull(AUSP_ATWRT,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update  
set containertype = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET externalshelflife = ifnull(cast(AUSP_ATFLV AS INTEGER),0),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
WHERE AUSP_ATINN = 27 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND externalshelflife <> ifnull(cast(AUSP_ATFLV AS INTEGER),0) */
 
merge into dim_part dp using
(select distinct dim_partid, ifnull(cast(AUSP_ATFLV AS INTEGER),0) as AUSP_ATFLV
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
				WHERE AUSP_ATINN = 27 AND AUSP_KLART = 1) a
		on  dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND externalshelflife <> ifnull(cast(AUSP_ATFLV AS INTEGER),0) ) upd
on dp.dim_partid = upd.dim_partid
when matched then update  
set externalshelflife = ifnull(cast(AUSP_ATFLV AS INTEGER),0),
	dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET internalshelflife = ifnull(cast(AUSP_ATFLV AS INTEGER),0),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
WHERE AUSP_ATINN = 28 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND internalshelflife <> ifnull(cast(AUSP_ATFLV AS INTEGER),0) */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(cast(AUSP_ATFLV AS INTEGER),0) as AUSP_ATFLV
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
				WHERE AUSP_ATINN = 28 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND internalshelflife <> ifnull(cast(AUSP_ATFLV AS INTEGER),0) ) upd
on dp.dim_partid = upd.dim_partid
when matched then update  
set internalshelflife = ifnull(cast(AUSP_ATFLV AS INTEGER),0),
	dw_update_date = current_timestamp;

  /* Madalina Herghelegiu 16 May 2016. Populate textcode column using CAWNT_ATWTB instead of AUSP_ATWRT.  BI-2853 */
/*UPDATE dim_part dp
  SET textcode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM AUSP a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 78
  AND a.AUSP_KLART = 1
  AND textcode <> ifnull(AUSP_ATWRT,'Not Set')   */

/* UPDATE dim_part dp
SET textcode = ifnull(CAWNT_ATWTB,'Not Set'),
dw_update_date = current_timestamp
FROM AUSP a, CAWNT c, dim_part dp
WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 78
  AND a.AUSP_KLART = 1
  AND a.AUSP_ATINN = c.CAWN_ATINN
  AND a.AUSP_ATWRT = c.CAWN_ATWRT
  AND textcode <> ifnull(CAWNT_ATWTB,'Not Set')*/

  /* End  Madalina Herghelegiu */

merge into dim_part dp using
(select distinct dim_partid, ifnull(CAWNT_ATWTB,'Not Set') as CAWNT_ATWTB
from dim_part dp
	left join ausp a
		on dp.PartNumber = a.AUSP_OBJEK
		AND dp.RowIsCurrent = 1
		AND a.AUSP_ATINN = 78
		AND a.AUSP_KLART = 1
	left join CAWNT c
		on a.AUSP_ATINN = c.CAWN_ATINN
		AND a.AUSP_ATWRT = c.CAWN_ATWRT
where textcode <> ifnull(CAWNT_ATWTB,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set textcode = ifnull(CAWNT_ATWTB,'Not Set'),
	dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET irudescription = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 133 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND irudescription <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 133 AND AUSP_KLART = 1) a 
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND irudescription <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set irudescription = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET masterseedbatchnumber = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 415 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND masterseedbatchnumber <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 415 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND masterseedbatchnumber <> ifnull(AUSP_ATWRT,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set masterseedbatchnumber = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;  

/* UPDATE dim_part dp
  SET itemspecifadmin = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 439 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND itemspecifadmin <> ifnull(AUSP_ATWRT,'Not Set') */
  
 merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 439 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND itemspecifadmin <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set itemspecifadmin = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET species = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 475 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND species <> ifnull(AUSP_ATWRT,'Not Set') */
  
 merge into dim_part dp using
(select distinct dim_partid,  ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 475 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND species <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set species = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET tissue = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 476 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND tissue <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid,  ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 476 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND tissue <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set tissue = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET origingroup = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 478 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND origingroup <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 478 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND origingroup <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set origingroup = ifnull(AUSP_ATWRT,'Not Set'),
	dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET therapeuticclass = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 130 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND therapeuticclass <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 130 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND therapeuticclass <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set therapeuticclass = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET mastercellmatdescr = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 466 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND mastercellmatdescr <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 466 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND mastercellmatdescr <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set mastercellmatdescr = ifnull(AUSP_ATWRT,'Not Set'),
	dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET colorplatecodematerials = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 3 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND colorplatecodematerials <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 3 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND colorplatecodematerials <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set colorplatecodematerials = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET electronicvisualcode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 4 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND electronicvisualcode <> ifnull(AUSP_ATWRT,'Not Set') */

merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 4 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND electronicvisualcode <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set electronicvisualcode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp; 

/* UPDATE dim_part dp
  SET dosage = ifnull(AUSP_ATFLV,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
WHERE AUSP_ATINN = 10 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND dosage <> ifnull(AUSP_ATFLV,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATFLV,'0.00') as AUSP_ATFLV
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
				WHERE AUSP_ATINN = 10 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND dosage <> ifnull(AUSP_ATFLV,'0.00') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set dosage = ifnull(AUSP_ATFLV,'0.00'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET dosageuom = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 11 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND dosageuom <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 11 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND dosageuom <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set dosageuom = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET packingunit = ifnull(convert(varchar(20),AUSP_ATFLV),'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
WHERE AUSP_ATINN = 15 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND packingunit <> ifnull(convert(varchar(20),AUSP_ATFLV),'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(convert(varchar(20),AUSP_ATFLV),'Not Set') as AUSP_ATFLV
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
				WHERE AUSP_ATINN = 15 AND AUSP_KLART = 1) a  
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND packingunit <> ifnull(convert(varchar(20),AUSP_ATFLV),'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set packingunit = ifnull(convert(varchar(20),AUSP_ATFLV),'Not Set'),
dw_update_date = current_timestamp;  

/* UPDATE dim_part dp
  SET usdaproductcode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 18 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND usdaproductcode <> ifnull(AUSP_ATWRT,'Not Set') */

merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 18 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND usdaproductcode <> ifnull(AUSP_ATWRT,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set usdaproductcode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;
  
/* UPDATE dim_part dp
  SET keycomponentsofaproduct = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 73 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND keycomponentsofaproduct <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 73 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND keycomponentsofaproduct <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set keycomponentsofaproduct = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET administration = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 131 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND administration <> ifnull(AUSP_ATWRT,'Not Set') */

merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 131 AND AUSP_KLART = 1) a 
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND administration <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set administration = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET dosageform = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 199 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND dosageform <> ifnull(AUSP_ATWRT,'Not Set') */

merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 199 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND dosageform <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set dosageform = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;  

/* UPDATE dim_part dp
  SET activecomponents = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 200 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND activecomponents <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 200 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND activecomponents <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set activecomponents = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;  

/* UPDATE dim_part dp
  SET componentsofcombipack = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 201 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND componentsofcombipack <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 201 AND AUSP_KLART = 1) a  
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND componentsofcombipack <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set componentsofcombipack = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;  

/* UPDATE dim_part dp
  SET formatcode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 270 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND formatcode <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT 
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 270 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND formatcode <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set formatcode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;  

/* UPDATE dim_part dp
  SET veterindustria = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 271 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND veterindustria <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
			   WHERE AUSP_ATINN = 271 AND AUSP_KLART = 1) a
	on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND veterindustria <> ifnull(AUSP_ATWRT,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set veterindustria = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET registrynumber = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 322 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND registrynumber <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 322 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND registrynumber <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set registrynumber = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;  

/* UPDATE dim_part dp
  SET masterseedlastinspdate = ifnull(CONVERT(DATE,CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2))),'0001-01-01'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
WHERE AUSP_ATINN = 467 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND masterseedlastinspdate <> ifnull(CONVERT(DATE,CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2))),'0001-01-01') */
  
merge into dim_part dp using
(select distinct dim_partid,
CONCAT(IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'0001'),1,4),'0001'),'-',IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'01'),5,2),'01'),'-',IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'01'),7,2),'01')) AS AUSP_ATFLV
from dim_part dp
left join (SELECT DISTINCT AUSP_OBJEK,
FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
WHERE AUSP_ATINN = 467 AND AUSP_KLART = 1) a
on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1 AND masterseedlastinspdate <> CONCAT(IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'0001'),1,4),'0001'),'-',IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'01'),5,2),'01'),'-',IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'01'),7,2),'01'))) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set masterseedlastinspdate = AUSP_ATFLV,
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET NDCCode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 514 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND NDCCode <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid, ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 514 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
  AND NDCCode <> ifnull(AUSP_ATWRT,'Not Set') ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set NDCCode = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;

/* UPDATE dim_part dp
  SET PlanDateArtworkApproval = ifnull(CONVERT(DATE,CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2))),'0001-01-01'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
WHERE AUSP_ATINN = 517 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND PlanDateArtworkApproval <> ifnull(CONVERT(DATE,CONCAT(SUBSTRING(AUSP_ATFLV,1,4),'-',SUBSTRING(AUSP_ATFLV,5,2),'-',SUBSTRING(AUSP_ATFLV,7,2))),'0001-01-01') */
  
merge into dim_part dp using
(select distinct dim_partid, CONCAT(IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'0001'),1,4),'0001'),'-',IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'01'),5,2),'01'),'-',IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'01'),7,2),'01')) AS AUSP_ATFLV
from dim_part dp
left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATFLV) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATFLV FROM AUSP
WHERE AUSP_ATINN = 517 AND AUSP_KLART = 1) a
on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
AND SUBSTRING(AUSP_ATFLV,1,4) <> '0'
AND PlanDateArtworkApproval <> CONCAT(IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'0001'),1,4),'0001'),'-',IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'01'),5,2),'01'),'-',IFNULL(SUBSTRING(ifnull(AUSP_ATFLV,'01'),7,2),'01')) ) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set PlanDateArtworkApproval = AUSP_ATFLV,
dw_update_date = current_timestamp;
  
/* UPDATE dim_part dp
  SET RegulatoryControls = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp
FROM (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
WHERE AUSP_ATINN = 612 AND AUSP_KLART = 1) a
, dim_part dp
  WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND RegulatoryControls <> ifnull(AUSP_ATWRT,'Not Set') */
  
merge into dim_part dp using
(select distinct dim_partid,  ifnull(AUSP_ATWRT,'Not Set') as AUSP_ATWRT
from dim_part dp
	left join (SELECT DISTINCT AUSP_OBJEK, FIRST_VALUE(AUSP_ATWRT) OVER (PARTITION BY AUSP_OBJEK ORDER BY AUSP_ATZHL asc) AUSP_ATWRT FROM AUSP
				WHERE AUSP_ATINN = 612 AND AUSP_KLART = 1) a
		on dp.PartNumber = a.AUSP_OBJEK
where dp.RowIsCurrent = 1
AND RegulatoryControls <> ifnull(AUSP_ATWRT,'Not Set')) upd
on dp.dim_partid = upd.dim_partid
when matched then update
set RegulatoryControls = ifnull(AUSP_ATWRT,'Not Set'),
dw_update_date = current_timestamp;
  
/* END BI-4519*/
 /* Octavian : Every Angle migration */
 /* 25 Oct 2017 Georgiana added changes according to APP-4115*/
 insert into tmp_uin_gpf_spf a
select distinct
UIN,
UIN_DESC,
GPF_CODE,
GLOBAL_PRODUCT_FAMILY,
SUB_PRODUCT_FAMILY_CODE,
SUB_PRODUCT_FAMILY,
SUPERFAMILY_CODE,
SUPERFAMILY_DESC,
HealthPlatformCode,
healthplatform,
PRODUCTSEGEMENTCODE,
PRODUCTSEGEMENTDESC
from pmra_master_data b
where not exists (select 1 from tmp_uin_gpf_spf a
where a.UIN=b.UIN);

/* Andrei R - 4155 - updates if some documents are changed in PMRA */
update tmp_uin_gpf_spf t
set t.UIN_DESC = ifnull(p.UIN_DESC,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.UIN_DESC,'xx') <> ifnull(p.UIN_DESC,'Not Set');

update tmp_uin_gpf_spf t
set t.GPF_CODE = ifnull(p.GPF_CODE,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.GPF_CODE,'xx') <> ifnull(p.GPF_CODE,'Not Set');

update tmp_uin_gpf_spf t
set t.GLOBAL_PRODUCT_FAMILY = ifnull(p.GLOBAL_PRODUCT_FAMILY,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.GLOBAL_PRODUCT_FAMILY,'xx') <> ifnull(p.GLOBAL_PRODUCT_FAMILY,'Not Set');

update tmp_uin_gpf_spf t
set t.SUB_PRODUCT_FAMILY_CODE = ifnull(p.SUB_PRODUCT_FAMILY_CODE,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.SUB_PRODUCT_FAMILY_CODE,'xx') <> ifnull(p.SUB_PRODUCT_FAMILY_CODE,'Not Set');

update tmp_uin_gpf_spf t
set t.SUB_PRODUCT_FAMILY = ifnull(p.SUB_PRODUCT_FAMILY,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.SUB_PRODUCT_FAMILY,'xx') <> ifnull(p.SUB_PRODUCT_FAMILY,'Not Set');

update tmp_uin_gpf_spf t
set t.SUPERFAMILY_CODE = ifnull(p.SUPERFAMILY_CODE,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.SUPERFAMILY_CODE,'xx') <> ifnull(p.SUPERFAMILY_CODE,'Not Set');

update tmp_uin_gpf_spf t
set t.SUPERFAMILY_DESC = ifnull(p.SUPERFAMILY_DESC,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.SUPERFAMILY_DESC,'xx') <> ifnull(p.SUPERFAMILY_DESC,'Not Set');

update tmp_uin_gpf_spf t
set t.HealthPlatformCode = ifnull(p.HealthPlatformCode,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.HealthPlatformCode,'xx') <> ifnull(p.HealthPlatformCode,'Not Set');

update tmp_uin_gpf_spf t
set t.heatlhplatform = ifnull(p.healthplatform,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.heatlhplatform,'xx') <> ifnull(p.healthplatform,'Not Set');

update tmp_uin_gpf_spf t
set t.PRODUCTSEGEMENTCODE = ifnull(p.PRODUCTSEGEMENTCODE,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.PRODUCTSEGEMENTCODE,'xx') <> ifnull(p.PRODUCTSEGEMENTCODE,'Not Set');

update tmp_uin_gpf_spf t
set t.PRODUCTSEGEMENTDESC = ifnull(p.PRODUCTSEGEMENTDESC,'Not Set')
from tmp_uin_gpf_spf t, pmra_master_data p
where t.UIN = p.UIN
and ifnull(t.PRODUCTSEGEMENTDESC,'xx') <> ifnull(p.PRODUCTSEGEMENTDESC,'Not Set');

/* END Andrei R - 4155 - updates if some documents are changed in PMRA */

 /*Begin Andrian - new 5 columns from excel file provided from merck*/
update dim_part a set a.uin_desc_pma = b.uin_desc
from tmp_uin_gpf_spf b
, dim_part a where a.partnumber = b.uin
and ifnull(a.uin_desc_pma,'Not Set') <> b.uin_desc;

update dim_part a set a.gpf_code_pma = b.gpf_code
from tmp_uin_gpf_spf b
, dim_part a where a.partnumber = b.uin
and ifnull(a.gpf_code_pma,'Not Set') <> b.gpf_code;

update dim_part a set a.global_product_family_pma = b.global_product_family
from tmp_uin_gpf_spf b
, dim_part a where a.partnumber = b.uin
and ifnull(a.global_product_family_pma,'Not Set') <> b.global_product_family;

update dim_part a set a.sub_product_family_code_pma = b.sub_product_family_code
from tmp_uin_gpf_spf b
, dim_part a where a.partnumber = b.uin
and ifnull(a.sub_product_family_code_pma,'Not Set') <> b.sub_product_family_code;

update dim_part a set a.sub_product_family_pma = b.sub_product_family
from tmp_uin_gpf_spf b
, dim_part a where a.partnumber = b.uin
and ifnull(a.sub_product_family_pma,'Not Set') <> b.sub_product_family;
/*End Andrian*/

/*Begin Alin - new 2 columns from excel file provided from merck - BI-4155*/
update dim_part a set a.superfamily_code_pma = b.superfamily_code
from tmp_uin_gpf_spf b
, dim_part a where a.partnumber = b.uin
and ifnull(a.superfamily_code_pma,'Not Set') <> b.superfamily_code;

update dim_part a set a.superfamily_desc_pma = b.superfamily_desc
from tmp_uin_gpf_spf b
, dim_part a where a.partnumber = b.uin
and ifnull(a.superfamily_desc_pma,'Not Set') <> b.superfamily_desc;
/* End Alin - BI-4155*/

/*Begin Andrei - new 2 columns from excel file provided from merck - APP-4155*/
update dim_part a
set a.PRODUCTSEGEMENTCODE = ifnull(b.PRODUCTSEGEMENTCODE, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_uin_gpf_spf b, dim_part a where a.partnumber = b.uin
and ifnull(a.PRODUCTSEGEMENTCODE,'xx') <> ifnull(b.PRODUCTSEGEMENTCODE,'Not Set');

 update dim_part a
set a.PRODUCTSEGEMENTDESC = ifnull(b.PRODUCTSEGEMENTDESC, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_uin_gpf_spf b, dim_part a where a.partnumber = b.uin
and ifnull(a.PRODUCTSEGEMENTDESC,'xx') <> ifnull(b.PRODUCTSEGEMENTDESC,'Not Set');
/* End Andrei - APP-4155*/

 update dim_part a
set a.healthplatform_merck = ifnull(b.heatlhplatform, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_uin_gpf_spf b, dim_part a where a.partnumber = b.uin
and ifnull(a.healthplatform_merck,'Not Set') <> ifnull(b.heatlhplatform,'Not Set');

merge into dim_part a
using (select distinct dim_partid ,a.partnumber, ifnull(b.HealthPlatformCode,'Not Set') as HealthPlatformCode
from tmp_uin_gpf_spf b
, dim_part a where a.partnumber = b.uin
and ifnull(a.HealthPlatformCode,'Not Set') <> ifnull(b.HealthPlatformCode,'Not Set')
---having count(*)>1
) t
on a.dim_partid=t.dim_partid
when matched then update set a.HealthPlatformCode = t.HealthPlatformCode;

/*Andrian 7 January 2016 Based on BI-1740 manually CSV file import*/
update dim_part
set vcm_initiative = 'No'
where vcm_initiative <> 'No';

update dim_part a set a.vcm_initiative = b.VCM_Initiative
from (SELECT DISTINCT PLANT,GPF_CODE,VCM_Initiative FROM VCM_initiative) b
, dim_part a where a.plant = b.plant
and a.ProductFamily_Merck = b.gpf_code
AND a.vcm_initiative <> b.VCM_Initiative;


/*Andrian-based on BI-1794*/
update dim_part pt
set counter_merck = marm_umrez
from  MARM m
, dim_part pt
where m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and counter_merck <> marm_umrez;

update dim_part dp
set dp.bbc = (CASE WHEN (cast(dp.dosage AS decimal (18,4))) > 0 THEN (cast(dp.dosage AS decimal (18,4))) ELSE decode( (cast(dp.contents AS decimal (18,4))) ,0,1.0000, (cast(dp.contents AS decimal (18,4))) )*decode( (dp.contentsuom) ,'KG',1000.0000,'ML',0.0010,1) END
) * (dp.counter_merck)
where ifnull(dp.bbc,-9999) <> (CASE WHEN (cast(dp.dosage AS decimal (18,4))) > 0 THEN (cast(dp.dosage AS decimal (18,4))) ELSE decode( (cast(dp.contents AS decimal (18,4))) ,0,1.0000, (cast(dp.contents AS decimal (18,4))) )*decode( (dp.contentsuom) ,'KG',1000.0000,'ML',0.0010,1) END
) * (dp.counter_merck);

/* Octavian: Every Angle */
/* Madalina 26 Jul 2016 - update Batch Class (KLAH) - BI-3555*/
/*merge into dim_part d
using (select distinct dim_partid,b.class
from kssk a, klah b
, dim_part d
where a.kssk_clint = b.clint
and TRIM(LEADING '0' FROM a.kssk_objek) = TRIM(LEADING '0' FROM d.partnumber)
and kssk_KLART= '023') t on t.dim_partid=d.dim_partid
when matched then update set batchclass_klah =  t.class,
dw_update_date = CURRENT_TIMESTAMP
where batchclass_klah <>  t.class*/

/* Madalina 16 Aug 2016 - correct field batchclass_klah - BI-3555 */
update dim_part set batchclass_klah = 'Not Set';

drop table if exists tmp_kssk_for_batchclass_klah;
create table tmp_kssk_for_batchclass_klah as
select * from  kssk a
where a.kssk_objek REGEXP_LIKE '[0-9]*' = 'TRUE';

/* avoid Unable to get a stable set of rows in the source tables error */
/*
update dim_part d
set batchclass_klah =  b.class,
dw_update_date = CURRENT_TIMESTAMP */
/* from kssk a, klah b, inob i,dim_part d */
/* from tmp_kssk_for_batchclass_klah a, klah b, inob i,dim_part d
where
	i.inob_objek = d.partnumber
	and i.inob_obtab = 'MARA' */
	/* and TO_CHAR(i.inob_cuobj) = a.kssk_objek */
/*	and i.inob_cuobj = a.kssk_objek
	and a.kssk_KLART = '023'
	and a.kssk_clint = b.clint
	and b.klart = '023'
	and batchclass_klah <>  b.class */
	
drop table if exists tmp_upd_batchclass_klah;
create table tmp_upd_batchclass_klah as
select distinct dim_partid, FIRST_VALUE(b.class) over (partition by dim_partid order by dim_partid) as class
from tmp_kssk_for_batchclass_klah a, klah b, inob i,dim_part d
where
	i.inob_objek = d.partnumber
	and i.inob_obtab = 'MARA'
	and i.inob_cuobj = a.kssk_objek
	and a.kssk_KLART = '023'
	and a.kssk_clint = b.clint
	and b.klart = '023';

update dim_part d
set batchclass_klah = class,
	dw_update_date = CURRENT_TIMESTAMP
from tmp_upd_batchclass_klah t, dim_part d
where d.dim_partid = t.dim_partid
	/*and rn = 1*/
	and batchclass_klah <> class;
	
drop table if exists tmp_kssk_for_batchclass_klah;
drop table if exists tmp_upd_batchclass_klah;
/* END 16 Aug 2016 */	

/* END 26 Jul 2016 */

merge into dim_part d
using (select distinct dim_partid, b.STATU
from kssk a, klah b
, dim_part d
where a.kssk_clint = b.clint
and TRIM(LEADING '0' FROM a.kssk_objek) = TRIM(LEADING '0' FROM d.partnumber)
and KLART= '023')t on t.dim_partid=d.dim_partid
when matched then update set batchstatus_klah =  t.STATU,
dw_update_date = CURRENT_TIMESTAMP
where batchstatus_klah <> t.STATU;

/*
update dim_part d
set materialclass_klah =  b.class,
dw_update_date = CURRENT_TIMESTAMP
from kssk a, klah b
, dim_part d
where a.kssk_clint = b.clint
and TRIM(LEADING '0' FROM a.kssk_objek) = TRIM(LEADING '0' FROM d.partnumber)
and KLART= '001'
and materialclass_klah <>  b.class */

drop table if exists update_materialclass_klah;
create table update_materialclass_klah as
select d.partnumber,
group_concat(distinct b.class ORDER BY b.class asc SEPARATOR ', ') materialclass_klah_comb
from dim_part d
,kssk a, klah b
where a.kssk_clint = b.clint
and TRIM(LEADING '0' FROM a.kssk_objek) = TRIM(LEADING '0' FROM d.partnumber)
and KLART= '001'
GROUP BY d.partnumber;

UPDATE dim_part d
SET d.materialclass_klah = k.materialclass_klah_comb,
dw_update_date = CURRENT_TIMESTAMP
FROM update_materialclass_klah k
, dim_part d
WHERE
d.partnumber = k.partnumber AND
d.materialclass_klah <> k.materialclass_klah_comb;

drop table if exists update_materialclass_klah;


MERGE INTO dim_part d
USING (
SELECT distinct d.dim_partid, FIRST_VALUE(b.statu) OVER (PARTITION BY b.clint ORDER BY b.clint) as statu
from kssk a, klah b
, dim_part d
where a.kssk_clint = b.clint
and TRIM(LEADING '0' FROM a.kssk_objek) = TRIM(LEADING '0' FROM d.partnumber)
and KLART= '001'
and materialstatus_klah <>  b.STATU) src
ON d.dim_partid = src.dim_partid
WHEN MATCHED THEN UPDATE
SET d.materialstatus_klah = src.statu;
/* Octavian : Every Angle */

/*Andrei R - APP-10656 */
UPDATE DIM_PART p
SET p.ispgcodedescription_merck = 'Not Set';
/* END Andrei R - APP-10656 */

/* Start change 31 Mar by Andrei Postolache - BI-2467 */
UPDATE DIM_PART p
SET p.ispgcodedescription_merck = IFNULL(icd.ispgcodedescription,'Not Set')
FROM ispgcodedescriptions_merck icd
, DIM_PART p
WHERE p.ispgcode_merck = icd.ispgcode
  AND p.ispgcodedescription_merck <> IFNULL(icd.ispgcodedescription,'Not Set');
/* End change 31 Mar by Andrei Postolache - BI-2467 */

/* Octavian: add a new field */
drop table if exists tmp_poscountriesorigin_rem_dupl;
create table tmp_poscountriesorigin_rem_dupl as
select distinct dp.partnumber,t.t005t_landx from dim_part dp
,AUSP a,T005T t
WHERE dp.PartNumber = a.AUSP_OBJEK
  AND dp.RowIsCurrent = 1
  AND a.AUSP_ATINN = 477
  AND a.AUSP_KLART = 1
  AND t.t005t_land1 = a.AUSP_ATWRT;


drop table if exists tmp_poscountriesorigin;
create table tmp_poscountriesorigin as
select t.partnumber,
group_concat(distinct t.t005t_landx ORDER BY t.t005t_landx asc SEPARATOR ', ') poscountriesorigin
FROM tmp_poscountriesorigin_rem_dupl t
GROUP BY t.partnumber;

update dim_part dp
SET dp.poscountriesorigin = t.poscountriesorigin,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_poscountriesorigin t
, dim_part dp
WHERE dp.partnumber = t.partnumber
AND dp.poscountriesorigin <> t.poscountriesorigin;

drop table if exists tmp_poscountriesorigin;
/* Octavian: add a new field */

drop table if exists dim_part_unitofmeasure;
create table dim_part_unitofmeasure AS
SELECT distinct partnumber,UnitOfMeasure from dim_part
WHERE UnitOfMeasure <> 'Not Set';

UPDATE dim_part dp
SET dp.UnitOfMeasure = uom.UnitOfMeasure
FROM dim_part_unitofmeasure uom
, dim_part dp
WHERE dp.UnitOfMeasure <> uom.UnitOfMeasure
and dp.partnumber = uom.partnumber;

drop table if exists dim_part_unitofmeasure;

/* Andrei Robescu BI - 5854 */

update dim_part set packingunit = '0' where packingunit = 'Not Set';

/* Andrei Robescu - APP-6333 - Merck AH - Currency values coming up as "Not Set"  materials in SA Material master - Update Currency in dim_part */

/* Andrei APP-7785 */
UPDATE dim_part dp
set dp.spendtype = case when dp.PartNumber = 'Not Set' then  'indirect' ELSE 'direct' END
from dim_part dp;
/* end APP-7785 */

drop table if exists tmp_for_upd_currency;
create table tmp_for_upd_currency as
select distinct t.werks,c.waers   FROM T001W t, t001k k,t001 c
 WHERE t.bwkey = k.bwkey   and k.bukrs=c.bukrs;

merge into dim_part p
using (select  distinct dim_partid,waers from dim_part, tmp_for_upd_currency 
where plant=werks) t
on t.dim_partid =p.dim_partid
when matched then update set p.currency=t.waers;

/*Georgiana Added changes for Forecast Selected Editable field form PF SA*/
update dim_part 
set dd_partnumber_description= Concat(PartNumber_NoLeadZero,' - ',PartDescription)
where 
dd_partnumber_description<> Concat(PartNumber_NoLeadZero,' - ',PartDescription);

/* New Changes 5 April 2017 - Yogini 28 Feb 2017 New field segmentation according to BI-5612 */
update dim_part set segmentation = 'Not Set';

update dim_part
set segmentation = tmp.segmentation
from dim_part dp, tmp_segmentation tmp
where dp.ProductFamily_Merck = tmp.gpf
and upper(dp.DominantSpeciesDescription_Merck) = upper(tmp.dom_spec);
/* END new changes - END Yogini 28 Feb 2017 BI-5612 */