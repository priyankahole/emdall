/* ################################################################################################################## */
/* */
/*   Script         : exa_mtp_3d_machineresource */
/*   Author         : Madalina */
/*   Created On     : 13 Sep 2018 */
/*   Overview       : MTP Skill - Machine Resource Capacity Supporting Table 3D  */
/*                Source tables:  mtp_1b_capacityRequirements, mtp_1c_master_aggregate and mtp_3a_Production_order*/ 
/*  Changes         : Create a fact containing a UNION between the 3d Machine resource + 3d Labor resource        */
/*                    Add new column - DescriptionofTheCapacityCategory - to differentiate the components         */
/*                    The final fact - Machine & Labor resource - won't use the 3d temp tables, but the fact created based on both of them */
/* ################################################################################################################## */

/* this fact is not completely re-created, only new monthly rows are added */
delete from number_fountain m where m.table_name = 'fact_mtp_3d_machineLaborResource';

insert into number_fountain
select 'fact_mtp_3d_machineLaborResource',
ifnull(max(f.fact_mtp_3d_machineLaborResourceid),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mtp_3d_machineLaborResource f;

drop table if exists mtp_machine_time_interval;

-- Get 37 months data
create table mtp_machine_time_interval as
select distinct trunc(datevalue, 'MM') as datevalue
from dim_date
where trunc(datevalue, 'MM') between  (trunc(current_date, 'MM') + interval '-12' MONTH) and  (trunc(current_date, 'MM') + interval '24' MONTH);

/* Machine resource inserts*/
insert into fact_mtp_3d_machineLaborResource (fact_mtp_3d_machineLaborResourceid,dd_Resource, dd_datevalue, dd_MonthOfYear, dd_DescriptionofTheCapacityCategory,dd_sourceOfData)
select ( SELECT IFNULL( m.max_id, 1 )FROM number_fountain m WHERE m.table_name = 'fact_mtp_3d_machineLaborResource' ) + ROW_NUMBER() OVER(ORDER BY '' ) AS fact_mtp_3d_machineLaborResourceid, 
		dd_Resource, 
		dd_datevalue,
		dd_MonthOfYear,
		dd_DescriptionofTheCapacityCategory,
        'template' as dd_sourceOfData
from (select distinct 
                mtp_1c.dd_Resource as dd_Resource,
				mtp_time.datevalue as dd_datevalue, 
				to_char(mtp_time.datevalue,'MON YYYY') as dd_MonthOfYear,
				mtp_1b.dd_DescriptionofTheCapacityCategory as dd_DescriptionofTheCapacityCategory
from fact_mtp_1c_master_aggregate mtp_1c 
	inner join fact_mtp_1b_capacityRequirements mtp_1b on mtp_1b.dd_materialnumber = mtp_1c.dd_materialnumber  and mtp_1b.dd_capacityid = mtp_1c.dd_capacityid
	cross join mtp_machine_time_interval mtp_time
where mtp_1b.dd_DescriptionofTheCapacityCategory = 'Processing unit'
and not exists 
	(select 1 from fact_mtp_3d_machineLaborResource mtp_3d where mtp_3d.dd_Resource = mtp_1c.dd_Resource and mtp_3d.dd_datevalue = mtp_time.datevalue )) t;

delete from number_fountain m where m.table_name = 'fact_mtp_3d_machineLaborResource';

insert into number_fountain
select 'fact_mtp_3d_machineLaborResource',
ifnull(max(f.fact_mtp_3d_machineLaborResourceid),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mtp_3d_machineLaborResource f;
	
/* Labor resource inserts*/
insert into fact_mtp_3d_machineLaborResource (fact_mtp_3d_machineLaborResourceid,dd_Resource, dd_datevalue, dd_MonthOfYear, dd_DescriptionofTheCapacityCategory,dd_sourceOfData)
select ( SELECT IFNULL( m.max_id, 1 )FROM number_fountain m WHERE m.table_name = 'fact_mtp_3d_machineLaborResource' ) + ROW_NUMBER() OVER(ORDER BY '' ) AS fact_mtp_3d_machineLaborResourceid, 
		dd_Resource, 
		dd_datevalue,
		dd_MonthOfYear,
		dd_DescriptionofTheCapacityCategory,
        'template' as dd_sourceOfData
from (select distinct 
                mtp_1c.dd_Resource as dd_Resource,
				mtp_time.datevalue as dd_datevalue, 
				to_char(mtp_time.datevalue,'MON YYYY') as dd_MonthOfYear,
				mtp_1b.dd_DescriptionofTheCapacityCategory as dd_DescriptionofTheCapacityCategory
from fact_mtp_1c_master_aggregate mtp_1c 
	inner join fact_mtp_1b_capacityRequirements mtp_1b on mtp_1b.dd_materialnumber = mtp_1c.dd_materialnumber  and mtp_1b.dd_capacityid = mtp_1c.dd_capacityid
	cross join mtp_machine_time_interval mtp_time
where mtp_1b.dd_DescriptionofTheCapacityCategory = 'Person'
and not exists 
	(select 1 from fact_mtp_3d_machineLaborResource mtp_3d where mtp_3d.dd_Resource = mtp_1c.dd_Resource and mtp_3d.dd_datevalue = mtp_time.datevalue)) t;

/*machine resource updates*/
update fact_mtp_3d_machineLaborResource 
set ct_NetAvailableCapacity = ct_GrossCapacity - (ct_Holidays + ct_PlannedMaintenance + ct_MediaSimulation + ct_NoProductionPlanned + ct_nonSAPproduction)
where dd_DescriptionofTheCapacityCategory = 'Processing unit';

merge into fact_mtp_3d_machineLaborResource mtp_3d using
( select dd_Resource,
		dd_MonthOfYear,
		sum(ct_OperationHours_GRQuantity) as ActualOutput
  from fact_mtp_3a_Production_order
  group by dd_Resource,
		dd_MonthOfYear
)  mtp_3a
on mtp_3d.dd_Resource = mtp_3a.dd_Resource
   and mtp_3d.dd_MonthOfYear = mtp_3a.dd_MonthOfYear
when matched then update
set mtp_3d.ct_ActualOutput = mtp_3a.ActualOutput
where dd_DescriptionofTheCapacityCategory = 'Processing unit';

/*labor resource updates*/
update fact_mtp_3d_machineLaborResource
set ct_GrossFTEHours =  ct_GrossFTE * ct_WorkingHours
where dd_DescriptionofTheCapacityCategory = 'Person';

update fact_mtp_3d_machineLaborResource
set ct_NetAvailableCapacity = ct_GrossFTEHours - (ct_Vacation + ct_Sickness + ct_nonSAPProduction)
where dd_DescriptionofTheCapacityCategory = 'Person';

merge into fact_mtp_3d_machineLaborResource mtp_3d using
( select dd_Resource,
		dd_MonthOfYear,
		sum(ct_LaborHours_GRQuantity) as ct_ActualOutput
  from fact_mtp_3a_Production_order
  group by dd_Resource,
		dd_MonthOfYear
)  mtp_3a
on mtp_3d.dd_Resource = mtp_3a.dd_Resource
   and mtp_3d.dd_MonthOfYear = mtp_3a.dd_MonthOfYear
when matched then update
set mtp_3d.ct_ActualOutput = mtp_3a.ct_ActualOutput
where dd_DescriptionofTheCapacityCategory = 'Person';

/*common columns updates*/
update fact_mtp_3d_machineLaborResource 
set ct_DemonstratedEfficiency = case when ct_NetAvailableCapacity <> 0 
									then ct_ActualOutput / ct_NetAvailableCapacity 
									else 0 end, 
    ct_NetEfficientCapacity = ct_PlannedEfficiency * ct_NetAvailableCapacity
where dd_DescriptionofTheCapacityCategory = 'Person';

/*update new added month (current date + 24 months), for new added resources, with the values from the next year*/
update fact_mtp_3d_machineLaborResource mtp_3d_new
set 
mtp_3d_new.CT_GROSSCAPACITY = mtp_3d_old.CT_GROSSCAPACITY,
mtp_3d_new.CT_HOLIDAYS = mtp_3d_old.CT_HOLIDAYS,
mtp_3d_new.CT_PLANNEDMAINTENANCE = mtp_3d_old.CT_PLANNEDMAINTENANCE,
mtp_3d_new.CT_MEDIASIMULATION = mtp_3d_old.CT_MEDIASIMULATION,
mtp_3d_new.CT_NOPRODUCTIONPLANNED = mtp_3d_old.CT_NOPRODUCTIONPLANNED,
mtp_3d_new.CT_GROSSFTE = mtp_3d_old.CT_GROSSFTE,
mtp_3d_new.CT_WORKINGHOURS = mtp_3d_old.CT_WORKINGHOURS,
mtp_3d_new.CT_GROSSFTEHOURS = mtp_3d_old.CT_GROSSFTEHOURS,
mtp_3d_new.CT_VACATION = mtp_3d_old.CT_VACATION,
mtp_3d_new.CT_SICKNESS = mtp_3d_old.CT_SICKNESS,
mtp_3d_new.CT_NONSAPPRODUCTION = mtp_3d_old.CT_NONSAPPRODUCTION,
mtp_3d_new.CT_NETAVAILABLECAPACITY = mtp_3d_old.CT_NETAVAILABLECAPACITY,
mtp_3d_new.CT_ACTUALOUTPUT = mtp_3d_old.CT_ACTUALOUTPUT,
mtp_3d_new.CT_DEMONSTRATEDEFFICIENCY = mtp_3d_old.CT_DEMONSTRATEDEFFICIENCY,
mtp_3d_new.CT_PLANNEDEFFICIENCY = mtp_3d_old.CT_PLANNEDEFFICIENCY,
mtp_3d_new.CT_NETEFFICIENTCAPACITY  = mtp_3d_old.CT_NETEFFICIENTCAPACITY
from fact_mtp_3d_machineLaborResource mtp_3d_new
	inner join fact_mtp_3d_machineLaborResource mtp_3d_old on mtp_3d_new.dd_resource = mtp_3d_old.dd_resource
where mtp_3d_old.dd_datevalue = (trunc(current_date, 'MM') + interval '12' MONTH)  /* get the values from next year, same month */
and  mtp_3d_new.dd_datevalue = (trunc(current_date, 'MM') + interval '24' MONTH)  /* and place them in the same month, next 2 years */
and  mtp_3d_new.dd_resource in (select dd_resource from fact_mtp_3d_machineLaborResource where trunc(dw_insert_date,'DD') = current_date); /*new added resource*/
     
/*APP-10674 - adding rows from extraction for 3d*/

update mtp_3d_machineLaborResource 
set dd_monthofyear = to_char(dd_datevalue,'MON YYYY');

delete from fact_mtp_3d_machineLaborResource fact_3d
where exists ( select 1 from  mtp_3d_machineLaborResource mtp_3d
				where fact_3d.dd_resource = mtp_3d.dd_resource
					and fact_3d.dd_datevalue = mtp_3d.dd_datevalue)
and fact_3d.dd_SourceOfData in ( 'template', 'extraction');

delete from number_fountain m where m.table_name = 'fact_mtp_3d_machineLaborResource';

insert into number_fountain
select 'fact_mtp_3d_machineLaborResource',
ifnull(max(f.fact_mtp_3d_machineLaborResourceid),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mtp_3d_machineLaborResource f;


insert into fact_mtp_3d_machineLaborResource 
(
	FACT_MTP_3D_MACHINELABORRESOURCEID,
	DD_RESOURCE,
	DD_DATEVALUE,
	DD_MONTHOFYEAR,
	CT_GROSSCAPACITY,
	CT_HOLIDAYS,
	CT_PLANNEDMAINTENANCE,
	CT_MEDIASIMULATION,
	CT_NOPRODUCTIONPLANNED,
	CT_GROSSFTE,
	CT_WORKINGHOURS,
	CT_GROSSFTEHOURS,
	CT_VACATION,
	CT_SICKNESS,
	CT_NONSAPPRODUCTION,
	CT_NETAVAILABLECAPACITY,
	CT_ACTUALOUTPUT,
	CT_DEMONSTRATEDEFFICIENCY,
	CT_PLANNEDEFFICIENCY,
	CT_NETEFFICIENTCAPACITY,
	DD_DESCRIPTIONOFTHECAPACITYCATEGORY,
	DW_INSERT_DATE,
    dd_SourceOfData
)
select 
	 ( SELECT IFNULL( m.max_id, 1 )FROM number_fountain m WHERE m.table_name = 'fact_mtp_3d_machineLaborResource' ) + ROW_NUMBER() OVER(ORDER BY '' ) AS fact_mtp_3d_machineLaborResourceid, 
	DD_RESOURCE,
	DD_DATEVALUE,
	DD_MONTHOFYEAR,
	CT_GROSSCAPACITY,
	CT_HOLIDAYS,
	CT_PLANNEDMAINTENANCE,
	CT_MEDIASIMULATION,
	CT_NOPRODUCTIONPLANNED,
	CT_GROSSFTE,
	CT_WORKINGHOURS,
	CT_GROSSFTEHOURS,
	CT_VACATION,
	CT_SICKNESS,
	CT_NONSAPPRODUCTION,
	CT_NETAVAILABLECAPACITY,
	CT_ACTUALOUTPUT,
	CT_DEMONSTRATEDEFFICIENCY,
	CT_PLANNEDEFFICIENCY,
	CT_NETEFFICIENTCAPACITY,
	DD_DESCRIPTIONOFTHECAPACITYCATEGORY,
	current_timestamp,
    'extraction' 
from mtp_3d_machineLaborResource mtp_3d
 where not exists ( select 1 from  fact_mtp_3d_machineLaborResource fact_3d
            where fact_3d.dd_resource = mtp_3d.dd_resource
                and fact_3d.dd_datevalue = mtp_3d.dd_datevalue)
     ;     


