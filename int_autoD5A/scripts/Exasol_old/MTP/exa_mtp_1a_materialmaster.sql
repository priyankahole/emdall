/* ################################################################################################################## */
/* */
/*   Script         : exa_mtp_1a_materialmaster */
/*   Author         : Madalina */
/*   Created On     : 10 Sep 2018 */
/*   Overview       : MTP Skill - Master Data Supporting Table 1A */
/*                    Source table: fact Material Master          */ 
/* ################################################################################################################## */


--drop table if exists mtp_1a_materialmaster
--create table mtp_1a_materialmaster as

/*create table Fact_MTP_1A_MATERIALMASTER
(
Fact_MTP_1A_MATERIALMASTER_ID decimal(36,0),
dd_MATERIALNUMBER VARCHAR(22) Default 'Not Set',
dd_MATERIALDESCRIPTION VARCHAR(60) Default 'Not Set',
dd_ITEMSUBTYPE VARCHAR(30) Default 'Not Set',
dd_GPFCODE VARCHAR(7) Default 'Not Set',
dd_GPFDESC VARCHAR(48) Default 'Not Set',
dd_BASEUOM VARCHAR(50) Default 'Not Set',
ct_CONTENTS DECIMAL(18,2) Default 0,
dd_CONTENTSUOM VARCHAR(30) Default 'Not Set',
ct_CONCENTRATION DECIMAL(18,4) Default 0,
dd_CONCENTRATIONUOM VARCHAR(30) Default 'Not Set',
ct_DOSAGE DECIMAL(18,0) Default 0,
dd_DOSAGEUOM VARCHAR(30) Default 'Not Set',
ct_FUTUREDEMANDQTY DECIMAL(28,2) Default 0,
dd_MRPLOTSIZECODE VARCHAR(10) Default 'Not Set',
dd_LOTSIZEINDICATOR VARCHAR(7) Default 'Not Set',
CT_MRPFIXEDLOTSIZE DECIMAL(13,3) Default 0,
CT_ROUNDINGVALUE DECIMAL(13,3) Default 0,
CT_MINIMUMLOTSIZE DECIMAL(13,3) Default 0,
CT_MAXLOTSIZE DECIMAL(18,4) Default 0,
CT_COSTINGLOTSIZE DECIMAL(13,3) Default 0,
dd_SCHEDULINGMARGINKEY VARCHAR(7) Default 'Not Set',
CT_SAFETYSTOCK DECIMAL(13,3) Default 0,
CT_MINIMUMSAFETYSTOCK DECIMAL(13,3) Default 0,
CT_SAFETYTIME DECIMAL(18,0) Default 0,
dd_RANGEOFCOVERAGE VARCHAR(7) Default 'Not Set',
CT_GRPROCESSINGTIME DECIMAL(18,0) Default 0,
CT_ASSEMBLYSCRAP DECIMAL(5,2) Default 0,
CT_PLANNINGTIMEFENCE DECIMAL(18,0) Default 0,
dd_CROSSPLANTMATERIALSTATUS VARCHAR(7) Default 'Not Set',
dd_PLANTMATERIALSTATUSCODE VARCHAR(7) Default 'Not Set',
dd_MRPTYPECODE VARCHAR(20) Default 'Not Set',
dd_MRPCONTROLLERCODE VARCHAR(50) Default 'Not Set',
CT_LOTSIZE DECIMAL(13,3) Default 0
)*/

truncate table  Fact_MTP_1A_MATERIALMASTER;

delete from number_fountain m where m.table_name = 'Fact_MTP_1A_MATERIALMASTER';

insert into number_fountain
select 'Fact_MTP_1A_MATERIALMASTER',
ifnull(max(f.Fact_MTP_1A_MATERIALMASTER_ID),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Fact_MTP_1A_MATERIALMASTER f;



Insert into Fact_MTP_1A_MATERIALMASTER

(
Fact_MTP_1A_MATERIALMASTER_ID ,
dd_MATERIALNUMBER  ,
dd_MATERIALDESCRIPTION  ,
dd_ITEMSUBTYPE  ,
dd_GPFCODE  ,
dd_GPFDESC  ,
dd_BASEUOM  ,
ct_CONTENTS ,
dd_CONTENTSUOM  ,
ct_CONCENTRATION ,
dd_CONCENTRATIONUOM  ,
ct_DOSAGE ,
dd_DOSAGEUOM  ,
ct_FUTUREDEMANDQTY ,
dd_MRPLOTSIZECODE  ,
dd_LOTSIZEINDICATOR  ,
CT_MRPFIXEDLOTSIZE ,
CT_ROUNDINGVALUE ,
CT_MINIMUMLOTSIZE ,
CT_MAXLOTSIZE ,
CT_COSTINGLOTSIZE ,
dd_SCHEDULINGMARGINKEY  ,
CT_SAFETYSTOCK ,
CT_MINIMUMSAFETYSTOCK ,
CT_SAFETYTIME ,
dd_RANGEOFCOVERAGE  ,
CT_GRPROCESSINGTIME ,
CT_ASSEMBLYSCRAP ,
CT_PLANNINGTIMEFENCE ,
dd_CROSSPLANTMATERIALSTATUS  ,
dd_PLANTMATERIALSTATUSCODE  ,
dd_MRPTYPECODE  ,
dd_MRPCONTROLLERCODE  ,
CT_LOTSIZE,
dd_plant
)

select (select ifnull(m.max_id,1) from number_fountain m where m.table_name = 'Fact_MTP_1A_MATERIALMASTER') + row_number() over(order by '') as Fact_MTP_1A_MATERIALMASTER_ID,
        A.* 
		from (
SELECT m.PartNumber dd_MATERIALNUMBER,   -- m.PartNumber_NoLeadZero
       m.PartDescription dd_MATERIALDESCRIPTION,
       m.ItemSubType_Merck dd_ITEMSUBTYPE,
       m.ProductFamily_Merck dd_GPFCODE,
       m.prodfamilydescription_merck dd_GPFDESC,
       m.UnitOfMeasure dd_BASEUOM,
       case when cast(m.contents AS decimal (18,2)) = '0.0000' THEN '' else cast(m.contents AS decimal (18,2)) END ct_CONTENTS,
       m.contentsuom dd_CONTENTSUOM,
       cast(m.concentration AS decimal (18,4)) ct_CONCENTRATION,
	   m.concentrationuom dd_CONCENTRATIONUOM,
	   cast(m.dosage AS integer) ct_DOSAGE,
       m.dosageuom dd_DOSAGEUOM,
	   ROUND(SUM(m.future_demandqty),2) ct_FUTUREDEMANDQTY, 
       m.MRPLotSize dd_MRPLOTSIZECODE,
       m.lotsizeindicator dd_LOTSIZEINDICATOR,
       m.fixedlotsize CT_MRPFIXEDLOTSIZE,
       m.RoundingValue CT_ROUNDINGVALUE,
       m.MinimumLotSize CT_MINIMUMLOTSIZE,
       m.maxlotsize CT_MAXLOTSIZE,
       m.Lotsizeprodcosting CT_COSTINGLOTSIZE,
       m.Schedmarginkey dd_SCHEDULINGMARGINKEY,
       m.SafetyStock CT_SAFETYSTOCK,
       m.minimumsafetystock CT_MINIMUMSAFETYSTOCK,
       m.safetytime CT_SAFETYTIME, 
       m.RangeOfCoverage dd_RANGEOFCOVERAGE,
       m.GRProcessingTime CT_GRPROCESSINGTIME,
       m.assemblyscrap CT_ASSEMBLYSCRAP,
       m.PlanningTimeFence CT_PLANNINGTIMEFENCE,
       m.Crossmatplantsts dd_CROSSPLANTMATERIALSTATUS,
       m.Plantmaterialstatus as dd_PLANTMATERIALSTATUSCODE,
       m.MRPType as dd_MRPTYPECODE,
       m.MRPController as dd_MRPCONTROLLERCODE,
	   case when Mrplotsize = 'FX' then Fixedlotsize
			when Mrplotsize = 'FS' then Roundingvalue 
            when Mrplotsize = 'EX' then Lotsizeprodcosting 
       else null end as CT_LOTSIZE,
       m.plant  /*APP-10674 - Madalina - adding Plant, so we can use it as join with 1c*/

FROM fact_materialmaster AS f_mmi 
INNER JOIN Dim_Part AS m ON f_mmi.dim_materialmasterid = m.Dim_Partid  
/* INNER JOIN dim_plant as pl on f_mmi.dim_plantmasterid = pl.dim_plantid */
GROUP BY  
		m.PartNumber,   -- m.PartNumber_NoLeadZero
       m.PartDescription,
       m.ItemSubType_Merck,
       m.ProductFamily_Merck,
       m.prodfamilydescription_merck,
       m.UnitOfMeasure,
       case when cast(m.contents AS decimal (18,2)) = '0.0000' THEN '' else cast(m.contents AS decimal (18,2)) END,
       m.contentsuom,
       cast(m.concentration AS decimal (18,4)),
	   m.concentrationuom,
	   cast(m.dosage AS integer),
       m.dosageuom,
       m.MRPLotSize,
       m.lotsizeindicator,
       m.fixedlotsize,
       m.RoundingValue,
       m.MinimumLotSize,
       m.maxlotsize,
       m.Lotsizeprodcosting,
       m.Schedmarginkey,
       m.SafetyStock,
       m.minimumsafetystock,
       m.safetytime, 
       m.RangeOfCoverage,
       m.GRProcessingTime,
       m.assemblyscrap,
       m.PlanningTimeFence,
       m.Crossmatplantsts,
       m.Plantmaterialstatus,
       m.MRPType,
       m.MRPController,
	   case when Mrplotsize = 'FX' then Fixedlotsize
			when Mrplotsize = 'FS' then Roundingvalue 
            when Mrplotsize = 'EX' then Lotsizeprodcosting 
       else null end,
       m.plant 
)A;
