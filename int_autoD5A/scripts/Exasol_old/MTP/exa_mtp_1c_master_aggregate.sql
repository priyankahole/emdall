/* ################################################################################################################## */
/* */
/*   Script         : exa_mtp_1c_master_aggregate */
/*   Author         : Madalina */
/*   Created On     : 10 Sep 2018 */
/*   Overview       : MTP Skill - Master aggregate 1c  */
/*                Source tables: mtp_master_aggregate  ( created as extraction from provided csv file ) +
                          1a (Material Master) and 1b (Capacity) tables*/ 
/* ################################################################################################################## */

DELETE FROM number_fountain m WHERE m.table_name = 'fact_mtp_1c_master_aggregate';
INSERT INTO number_fountain
SELECT 'fact_mtp_1c_master_aggregate', 1;

DROP TABLE if exists mtp_1c_master_aggregate;

CREATE TABLE mtp_1c_master_aggregate AS
SELECT 
(SELECT IFNULL(m.max_id,1) FROM number_fountain m WHERE m.table_name = 'fact_mtp_1c_master_aggregate') + ROW_NUMBER() OVER(ORDER BY'') AS fact_mtp_1c_master_aggregateid,
        mtp_ma.material_number     AS dd_MaterialNumber,
        mtp_ma.plant_code          AS dd_PlantCode,
        mtp_ma.current_aggregation AS dd_Resource,
        mtp_ma.capacity_id         AS dd_CapacityID,
        mtp_ma.planner             AS dd_Planner,
        cast (1 AS bigint)         AS dim_partid,
        avg(CASE WHEN mtp_1a.ct_LotSize <> 0 
                 THEN mtp_1a.ct_LotSize 
                 ELSE NULL 
            END)                   AS ct_LotSize,
        avg(CASE WHEN mtp_1a.ct_Assemblyscrap <> 0 
                 THEN mtp_1a.ct_Assemblyscrap/100
                 ELSE NULL
            END)                   AS ct_Assemblyscrap,
        avg(CASE WHEN mtp_1a.ct_LotSize <> 0 
                 THEN mtp_1a.ct_LotSize*(1/(1+mtp_1a.ct_AssemblyScrap))
                 ELSE NULL
            END)                   AS ct_StandardLotSize,
        /*avg(CASE WHEN mtp_1b.ct_Operation_hours_quantity <> 0 
                 THEN mtp_1b.ct_Operation_hours_quantity
                 ELSE NULL
            END)       */
        cast (null as decimal(18,4)) AS ct_OperationHours_StandardLotSize,
        avg(CASE WHEN mtp_1b.ct_Operation_hours_quantity <> 0 
                 THEN mtp_1b.ct_Operation_hours_quantity / 
                       CASE WHEN mtp_1a.ct_LotSize*(1/(1+mtp_1a.ct_AssemblyScrap)) = 0 
                            THEN 1 
                            ELSE mtp_1a.ct_LotSize*(1/(1+mtp_1a.ct_AssemblyScrap))
                       END
                 ELSE NULL
            END)                   AS ct_OperationHours_BUoM,
        avg(CASE WHEN mtp_1b.ct_Labor_hours_quantity <> 0 
                 THEN mtp_1b.ct_Labor_hours_quantity
                 ELSE NULL
            END)                   AS ct_LaborHours_StandardLotSize,
        avg(CASE WHEN mtp_1b.ct_Labor_hours_quantity <> 0 
                 THEN mtp_1b.ct_Labor_hours_quantity /
                       CASE WHEN mtp_1a.ct_LotSize*(1/(1+mtp_1a.ct_AssemblyScrap)) = 0
                            THEN 1 
                            ELSE  mtp_1a.ct_LotSize*(1/(1+mtp_1a.ct_AssemblyScrap)) 
                       END
                 ELSE NULL
            END)                   AS ct_LaborHours_BUoM,
        (cast (0 AS decimal)) AS ct_TTI_BUoM
   FROM mtp_master_aggregate mtp_ma
  INNER JOIN fact_mtp_1a_materialmaster mtp_1a 
     ON mtp_1a.dd_materialnumber = mtp_ma.material_number 
     and mtp_1a.dd_plant = mtp_ma.plant_code /*APP-10674 - add plant join to correct the measures  */
   LEFT JOIN fact_mtp_1b_capacityRequirements mtp_1b 
     ON mtp_1b.dd_materialnumber = mtp_ma.material_number 
    AND mtp_1b.dd_plantcode = mtp_ma.plant_code
  GROUP BY mtp_ma.material_number,
           mtp_ma.plant_code,
           mtp_ma.current_aggregation,
           mtp_ma.capacity_id,
           mtp_ma.planner;

alter table  mtp_1c_master_aggregate add column dd_capacityidflag varchar(10);
-- APP-10498 : update the master data with a flag to track the capacity ids to be used to calculate the  hours per day
update mtp_1c_master_aggregate f
set dd_capacityidflag='Y' 
where dd_capacityid in (select distinct capacityid from tmp_capacityid);

--APP-10498: Get the average hours per day at resource level and set it to default = 8 if the capacity id is not flagged to Y 
drop table if exists tmp_wc_getavghrsperday;
create table tmp_wc_getavghrsperday as
select  dd_resource, ifnull(avg((DD_FINISHTIMEINSECONDS - DD_STARTTIMEINSECODS)/(60*60)),8) as avghoursperday
from  fact_workcenter wc
right join fact_mtp_1c_master_aggregate master
on wc.dd_capacityid=master.dd_capacityid and dd_capacityidflag='Y' 
group by  dd_resource;
               
UPDATE mtp_1c_master_aggregate m
   SET m.dim_partid = pr.dim_partid
  FROM mtp_1c_master_aggregate m
 INNER JOIN dim_part pr 
    ON m.dd_MaterialNumber = pr.partnumber 
   AND m.dd_plantcode=pr.plant;
  
/*Madalina - APP-10609 - Change logic for OperationHours_StandardLotSize*/
merge into mtp_1c_master_aggregate mtp_1c using
(select mtp_1b.dd_materialnumber,  mtp_1b.dd_capacityid,
		 avg(CASE WHEN mtp_1b.ct_Operation_hours_quantity <> 0 
		                 THEN mtp_1b.ct_Operation_hours_quantity
		                 ELSE NULL
		            END) as ct_OperationHours_StandardLotSize
from fact_mtp_1b_capacityRequirements mtp_1b 
	inner join fact_mtp_1c_master_aggregate mtp_1c on mtp_1b.dd_capacityid = mtp_1c.dd_capacityid
												 and mtp_1b.dd_materialnumber = mtp_1c.dd_materialnumber
where mtp_1c.dd_capacityidflag = 'Y'
group by mtp_1b.dd_materialnumber, mtp_1b.dd_capacityid) upd
on upd.dd_capacityid = mtp_1c.dd_capacityid
 and upd.dd_materialnumber = mtp_1c.dd_materialnumber
when matched then update 
set mtp_1c.ct_OperationHours_StandardLotSize = upd.ct_OperationHours_StandardLotSize;
               
/*master aggregate file does not contain all resources*/
delete from fact_mtp_1c_master_aggregate mtp_1c
where exists ( select 1 from mtp_1c_master_aggregate tmp_1c
              	where tmp_1c.dd_Resource = mtp_1c.dd_Resource
              		and tmp_1c.dd_MaterialNumber = mtp_1c.dd_MaterialNumber
              		and tmp_1c.dd_PlantCode = mtp_1c.dd_PlantCode
              		and tmp_1c.dd_CapacityID = mtp_1c.dd_CapacityID);
               
insert into fact_mtp_1c_master_aggregate
               (
                 FACT_MTP_1C_MASTER_AGGREGATEID,
                DD_MATERIALNUMBER,
                DD_PLANTCODE,
                DD_RESOURCE,
                DD_CAPACITYID,
                DD_PLANNER,
                DIM_PARTID,
                CT_LOTSIZE,
                CT_ASSEMBLYSCRAP,
                CT_STANDARDLOTSIZE,
                CT_OPERATIONHOURS_STANDARDLOTSIZE,
                CT_OPERATIONHOURS_BUOM,
                CT_LABORHOURS_STANDARDLOTSIZE,
                CT_LABORHOURS_BUOM,
                CT_TTI_BUOM,
                DD_CAPACITYIDFLAG
               )
select
               FACT_MTP_1C_MASTER_AGGREGATEID,
                DD_MATERIALNUMBER,
                DD_PLANTCODE,
                DD_RESOURCE,
                DD_CAPACITYID,
                DD_PLANNER,
                DIM_PARTID,
                CT_LOTSIZE,
                CT_ASSEMBLYSCRAP,
                CT_STANDARDLOTSIZE,
                CT_OPERATIONHOURS_STANDARDLOTSIZE,
                CT_OPERATIONHOURS_BUOM,
                CT_LABORHOURS_STANDARDLOTSIZE,
                CT_LABORHOURS_BUOM,
                CT_TTI_BUOM,
                DD_CAPACITYIDFLAG           
from mtp_1c_master_aggregate;

/* DROP TABLE if exists fact_mtp_1c_master_aggregate
rename mtp_1c_master_aggregate to fact_mtp_1c_master_aggregate */
