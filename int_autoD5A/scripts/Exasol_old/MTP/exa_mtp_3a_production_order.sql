/* ################################################################################################################## */
/* */
/*   Script         : exa_mtp_3a_Production_order */
/*   Author         : Madalina, Piyush */
/*   Created On     : 12 Sep 2018 */
/*   Overview       : MTP Skill - Production Order Supporting Table 3A  */
/*                Source tables:  Production Order and mtp_1c_master_aggregate */
/* ################################################################################################################## */
/*Change logic for mtp_3a_Production_order*/
DELETE
FROM number_fountain m
WHERE m.table_name = 'fact_mtp_3a_Production_order';

INSERT INTO number_fountain SELECT 'fact_mtp_3a_Production_order', 1;

/* re-create fact, generate IDs starting with 1 */
DROP TABLE IF EXISTS mtp_3a_Production_order;

CREATE TABLE mtp_3a_Production_order AS 
SELECT ( SELECT IFNULL( m.max_id, 1 )FROM number_fountain m WHERE m.table_name = 'fact_mtp_3a_Production_order' ) + ROW_NUMBER() OVER(ORDER BY '' ) AS fact_mtp_3a_Production_orderid, 
       f_prord.dd_ordernumber         AS dd_OrderNumber, 
       ip.PartNumber                  AS dd_MaterialNumber, 
       ip.PartDescription             AS dd_MaterialDescription, 
       ip.ProductFamily_Merck         AS dd_GPFcode, 
       ip.prodfamilydescription_merck AS dd_GPFdesc, 
       btch.batchnumber               AS dd_BatchNumber, 
       CASE TO_CHAR( aifd.DateValue, 'DD MON yyyy' )
            WHEN '01 Jan 0001' THEN ' '
            ELSE TO_CHAR( aifd.DateValue, 'DD MON yyyy' )
       END                            AS dd_Actual_Finish_Date, 
       ROUND( SUM( f_prord.ct_TotalOrderQty ), 4 ) AS ct_TotalOrderQty, 
       ROUND( SUM( f_prord.ct_GRQty ), 4 )         AS ct_GR_Quantity, 
       hp.UnitOfMeasure                            AS dd_Base_UoM, 
       SUM( CAST( btch.activityfactorkg AS DECIMAL ( 11, 4 ))) AS dd_Activity_Factor_KG_FPE, 
       SUM( CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 )))  AS ct_Proportion_Unit, 
       ROUND( SUM( CASE WHEN ( f_prord.ct_TotalOrderQty ) = 0.00 
                        THEN 0.00 
                        ELSE ( f_prord.ct_GRQty ) / ( f_prord.ct_TotalOrderQty ) * 100 END ), 4 )  AS ct_Perc_Production_Yield,
       hp.assemblyscrap AS ct_Assembly_scrap_Perc, 
       MF1c.dd_Resource    AS dd_Resource, 
       MF1c.dd_CapacityID  AS dd_CapacityID, 
       MF1c.dd_Planner     AS dd_Planner, 
       TO_CHAR( aifd.DateValue, 'MON YYYY' ) AS dd_MonthOfYear, 
       SUM( CASE WHEN CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 )) = 0.000 
                 THEN ROUND(( f_prord.ct_GRQty ), 4 ) 
                 ELSE CASE WHEN CAST( btch.activityfactorkg AS DECIMAL ( 11, 4 )) = 0.0000 
                           THEN ROUND(( f_prord.ct_GRQty ), 4 )
                           ELSE ROUND(( f_prord.ct_GRQty* ( CAST( btch.activityfactorkg AS DECIMAL ( 11, 4 )) / CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 )) ) ), 4 ) 
                      END 
             END )                 AS ct_Corrected_GR_Quantity, 
       AVG( MF1c.ct_LotSize )         AS ct_LotSize, 
       AVG( MF1c.ct_StandardLotSize ) AS ct_StandardLotSize, 
       AVG( MF1c.ct_OperationHours_StandardLotSize ) AS ct_OperationHours_StandardLotSize, 
       AVG( MF1c.ct_LaborHours_StandardLotSize )     AS ct_LaborHours_StandardLotSize, 
       AVG( MF1c.ct_OperationHours_BUoM )            AS ct_OperationHours_BUoM, 
       ROUND( SUM( MF1c.ct_OperationHours_BUoM * f_prord.ct_TotalOrderQty ), 4 ) AS ct_OperationHours_TotalQuantity, 
       ROUND( SUM( MF1c.ct_OperationHours_BUoM * f_prord.ct_GRQty ), 4 )         AS ct_OperationHours_GRQuantity, 
       AVG( MF1c.ct_LaborHours_BUoM )                                            AS ctd_LaborHours_BUoM, 
       ROUND( SUM( MF1c.ct_LaborHours_BUoM * f_prord.ct_TotalOrderQty ), 4 )     AS ct_LaborHours_TotalQuantity, 
       ROUND( SUM( MF1c.ct_LaborHours_BUoM * f_prord.ct_GRQty ), 4 )             AS ct_LaborHours_GRQuantity, 
       CASE CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 ))
            WHEN 0 
            THEN 'No'
            ELSE 'Yes'
       END                 AS dd_FPE_revelant, 
       CASE WHEN CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 )) = 0
            THEN 0.00
            ELSE CAST( btch.activityfactorkg AS DECIMAL ( 11, 4 ))/ CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 ))
       END AS ct_Potency_Variance, 
       SUM( CAST( 1 -( hp.assemblyscrap / 100 ) AS FLOAT )) AS ct_ExpectedYield, 
       SUM( CASE CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 )) 
                 WHEN 0 
                 THEN ROUND(( f_prord.ct_GRQty ), 4 ) 
                 ELSE CASE 
                          WHEN CAST( btch.activityfactorkg AS DECIMAL ( 11, 4 )) = 0 
                          THEN ROUND(( f_prord.ct_GRQty ), 4 ) 
                          ELSE CAST( btch.activityfactorkg AS DECIMAL ( 11, 4 )) 
                      END 
            END )/ ROUND( SUM( f_prord.ct_TotalOrderQty ), 4 ) AS ct_GRQntycorrectedPotencyVsTotalQnty, 
       ROUND( SUM( f_prord.ct_GRQty ), 4 )/ ROUND( SUM( f_prord.ct_TotalOrderQty ), 4 ) AS ct_GRQntyVsTotalQnty, 
       pl.dim_plantid       AS dim_plantid, 
       hp.dim_partid        AS dim_partid, 
       CAST ( 1 AS BIGINT ) AS dim_dateid
 FROM fact_productionorder         AS f_prord
INNER JOIN Dim_Date                AS bfd 
   ON f_prord.Dim_DateidBasicFinish = bfd.Dim_Dateid
INNER JOIN Dim_Part                AS ip 
   ON f_prord.Dim_PartidItem = ip.Dim_Partid
INNER JOIN Dim_Plant               AS pl 
   ON f_prord.Dim_Plantid = pl.Dim_Plantid
INNER JOIN Dim_Part                AS hp 
   ON f_prord.Dim_PartidHeader = hp.Dim_Partid
INNER JOIN dim_batch               AS btch 
   ON f_prord.dim_batchid = btch.dim_batchid
INNER JOIN Dim_Date                AS aifd 
   ON f_prord.Dim_DateidActualItemFinish = aifd.Dim_Dateid
/*APP-10674 - correct join between Prod Order and Master aggregate*/
INNER JOIN dim_workcenter dw on f_prord.dim_workcenterid = dw.dim_workcenterid
INNER JOIN fact_mtp_1c_master_aggregate AS MF1c 
   ON ip.PartNumber = MF1c.dd_MaterialNumber
  AND pl.Plantcode = MF1c.dd_Plantcode
  /* and dw.capacityid = MF1c.dd_capacityid */
WHERE f_prord.ct_GRQty <> 0
  AND ( bfd.DateValue BETWEEN ( NOW() - INTERVAL '1' YEAR ) AND NOW())
  AND ( LOWER( CASE WHEN f_prord.dd_deletionflag = 'Y' 
                    THEN ( CASE WHEN dd_delivered = 'X' 
                                THEN 'Closed' 
                                ELSE ( CASE WHEN dd_technicallycompleted = 'X' 
                                            THEN 'Closed' 
                                            ELSE ( CASE WHEN dd_closed = 'X' 
                                                        THEN 'Closed' 
                                                        ELSE 'Cancelled' 
                                                   END ) 
                                       END ) 
                                END ) 
                    ELSE ( CASE WHEN dd_delivered = 'X' 
                                THEN 'Closed' 
                                ELSE ( CASE WHEN dd_technicallycompleted = 'X' 
                                            THEN 'Closed' 
                                            ELSE ( CASE WHEN ct_GRQty = 0 
                                                        THEN 'Open' 
                                                        ELSE ( CASE WHEN ( ct_OrderItemQty - ct_GRQty ) <= 0 
                                                                    THEN 'Closed' 
                                                                    ELSE 'Partially Open' 
                                                               END ) 
                                                   END ) 
                                       END ) 
                           END ) 
                END ) = LOWER( 'Closed' ))
  AND (( ct_GRQty != 0 ))
GROUP BY f_prord.dd_ordernumber, 
         ip.PartNumber, 
         ip.PartDescription, 
         ip.ProductFamily_Merck, 
         ip.prodfamilydescription_merck, 
         btch.batchnumber, 
         TO_CHAR( aifd.DateValue, 'DD MON yyyy' ), 
         hp.UnitOfMeasure, 
         hp.assemblyscrap, 
         MF1c.dd_Resource, MF1c.dd_CapacityID, 
         MF1c.dd_Planner, TO_CHAR( aifd.DateValue, 'MON YYYY' ), 
         CASE CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 ))
        WHEN 0 
        THEN 'No'
        ELSE 'Yes'
     END, 
     CASE
      WHEN CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 )) = 0 
      THEN 0.00
      ELSE CAST( btch.activityfactorkg AS DECIMAL ( 11, 4 ))/ CAST( ip.proportionunitqty AS DECIMAL ( 18, 3 ))
     END, 
     pl.dim_plantid, 
     hp.dim_partid;

 DROP TABLE IF EXISTS fact_mtp_3a_Production_order;

 RENAME mtp_3a_Production_order TO fact_mtp_3a_Production_order;