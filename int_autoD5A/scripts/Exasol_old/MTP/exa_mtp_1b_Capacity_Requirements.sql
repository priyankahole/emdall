/* ################################################################################################################## */
/* */
/*   Script         : exa_mtp_1b_capacity */
/*   Author         : Madalina */
/*   Created On     : 24 Sep 2018 */
/*   Overview       : MTP Skill - Capacity Supporting Table 1B  */
/*                Source tables: Capacity */ 
/* ################################################################################################################## */


delete from number_fountain m where m.table_name = 'fact_mtp_1b_capacityRequirements';

insert into number_fountain
select 'fact_mtp_1b_capacityRequirements', 1;

drop table if exists mtp_1b_capacityRequirements;
create table mtp_1b_capacityRequirements as
SELECT 
(select ifnull(m.max_id,1) from number_fountain m where m.table_name = 'fact_mtp_1b_capacityRequirements') + row_number() over(order by '') as fact_mtp_1b_capacityRequirementsid,
pl.PlantCode dd_PlantCode,
wck.productionsupplyarea dd_productionsupplyarea,
dd_capacityid,
cc.description dd_descriptionofTheCapacityCategory,
wck.workcenter dd_Workcenter,
wck.ktext_description dd_WorkCenterDescription,
f_cap.dd_keyfortlgroup dd_KeyforTaskListGroup,
f_cap.dd_opnumber dd_opnumber,
dp.PartNumber dd_Materialnumber,
dp.PartDescription dd_MaterialDescription,
ROUND(AVG(ct_operation),3) ct_OperationQuantity,
dp.UnitOfMeasure dd_BaseUoM,
ROUND(AVG(ct_schedcaprequirprocesing),16) ct_schedcaprequirprocessing,
cruomq.UOM  dd_UOM,
Case cc.description when 'Person' then '' else ROUND(AVG(ct_schedcaprequirprocesing),16) end ct_Operation_hours_quantity,
Case cc.description when 'Person' then ROUND(AVG(ct_schedcaprequirprocesing),16) else '' end ct_Labor_hours_quantity,
f_cap.dim_plantid dim_plantid,
f_cap.dim_partid dim_partid
FROM fact_capacity AS f_cap 
INNER JOIN Dim_Date AS dsh 
ON f_cap.dim_dateidscheduledfinish = dsh.Dim_Dateid   
INNER JOIN Dim_Part AS dp ON f_cap.dim_partid = dp.Dim_Partid    
INNER JOIN Dim_Plant AS pl ON f_cap.dim_plantid = pl.Dim_Plantid  
INNER JOIN dim_workcenter AS wck ON f_cap.dim_workcenterid = wck.dim_workcenterid  
INNER JOIN dim_capacitycategory AS cc ON f_cap.dim_capcategoryid = cc.dim_capacitycategoryid  
INNER JOIN dim_unitofmeasure AS cruomq ON f_cap.dim_caprequnitofmeasureid = cruomq.dim_unitofmeasureid  
WHERE (f_cap.dd_longtermplanscen = 0)  AND (dsh.DateValue  BETWEEN now() AND now() + 729) 
GROUP BY  
pl.PlantCode, 
wck.productionsupplyarea,
dd_capacityid,
cc.description,
wck.workcenter,
wck.ktext_description,
f_cap.dd_keyfortlgroup,
f_cap.dd_opnumber,
dp.PartNumber,
dp.PartDescription, 
dp.UnitOfMeasure ,
cruomq.UOM,
f_cap.dim_plantid,
f_cap.dim_partid;

drop table if exists fact_mtp_1b_capacityRequirements;
rename mtp_1b_capacityRequirements to fact_mtp_1b_capacityRequirements;



