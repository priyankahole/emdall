DROP TABLE IF EXISTS tmp_GRDates_For_POSchedules;


/* Madalina 21 Jul 2016 - Move the updates from dim_part into post fact_materialmovement script - Check logic - Material Master: Date of Last Goods Movement and Type */
/* Octavian: Movement Type and Date of last movement type per material and plant */
DROP TABLE IF EXISTS dim_part_dateoflastgoodsmvmttype;
CREATE TABLE dim_part_dateoflastgoodsmvmttype AS
SELECT  dim_partid, PostingDate, movementtype 
FROM 
(
    select
f.dim_partid,dt.datevalue AS PostingDate,concat(mt.movementtype,' - ',mt.description) AS movementtype,
row_number() over (partition by f.dim_partid order by dt.datevalue desc) as rn
from fact_materialmovement f, dim_movementtype mt, dim_date dt, dim_part dp
where ifnull(f.dim_movementtypeid,1) = ifnull(mt.dim_movementtypeid,1)
and ifnull(dt.dim_dateid, 1) = ifnull(f.dim_dateidpostingdate, 1)
and ifnull(dp.dim_partid,1) = ifnull(f.dim_partid,1)
/* AND  ((dp.lastmovementtype <> concat(mt.movementtype,' - ',mt.description)) OR (dp.dateoflastgoods <> dt.datevalue)) */
) t
WHERE t.rn = 1;

UPDATE dim_part dp
SET dp.lastmovementtype = mm.movementtype,
dw_update_date = CURRENT_TIMESTAMP
FROM    dim_part_dateoflastgoodsmvmttype mm, dim_part dp
WHERE ifnull(dp.dim_partid, 1) = ifnull(mm.dim_partid, 1)
AND dp.lastmovementtype <> mm.movementtype;

UPDATE dim_part dp
SET dp.dateoflastgoods = mm.PostingDate,
dw_update_date = CURRENT_TIMESTAMP
FROM    dim_part_dateoflastgoodsmvmttype mm, dim_part dp
WHERE ifnull(dp.dim_partid,1) = ifnull(mm.dim_partid,1)
AND dp.dateoflastgoods <> mm.PostingDate;

DROP TABLE IF EXISTS dim_part_dateoflastgoodsmvmttype;
/* Octavian: Movement Type and Date of last movement type per material and plant */
/* END 21 Jul 2016 */

/* Georgiana 04 Aug 2016 Adding the update for dd_executionstatusschedule in this script in order to have the accurate data for this status column*/
/*Georgiana 30 May 2016 adding dd_executionstatusschedule according to BI -2372*/
drop table if exists tmp_for_upd_executionstatus;
create table tmp_for_upd_executionstatus as 
select distinct  f_deliv.dd_DocumentNo,f_deliv.dd_DocumentItemNo,f_deliv.dd_ScheduleNo,
CASE WHEN dd_deletionindicator = 'L' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) = 0 THEN 'Cancelled'

WHEN dd_deletionindicator= 'L' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) <> 0 THEN 'Closed'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete = 'X' AND atrb.itemreturn = 'X' THEN 'Closed'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete = 'X' AND atrb.itemreturn <> 'X'  AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) = 0 THEN 'Cancelled'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete = 'X' AND atrb.itemreturn <> 'X' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) <> 0 THEN 'Closed'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete <> 'X' AND atrb.itemreturn = 'X' THEN 'Open'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete <> 'X' AND atrb.itemreturn <> 'X' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) = 0 and (case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 then 0.0000 else (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) end) = 0 AND ct_ItemQty = 0 THEN 'Open'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete <> 'X' AND atrb.itemreturn <> 'X' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) = 0  AND (case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 then 0.0000 else (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) end) = 0 AND ct_ItemQty <> 0 AND dd_InvStatus='Not Yet' THEN 'Open'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete <> 'X' AND atrb.itemreturn <> 'X' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) = 0  AND (case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 then 0.0000 else (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) end) = 0 AND ct_ItemQty <> 0 AND dd_InvStatus <>'Not Yet' THEN 'Closed'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete <> 'X' AND atrb.itemreturn <> 'X' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) = 0  AND (case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 then 0.0000 else (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) end) <> 0  THEN 'Open'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete <> 'X' AND atrb.itemreturn <> 'X' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) <> 0  AND (case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 then 0.0000 else (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) end) = 0 THEN 'Closed'

WHEN dd_deletionindicator <> 'L' AND atrb.ItemDeliveryComplete <> 'X' AND atrb.itemreturn <> 'X' AND (CASE WHEN (f_deliv.dim_DateidFirstGR > 1 OR f_deliv.dim_DateidLastGR > 1) THEN f_deliv.ct_ReceivedQty ELSE 0.0000 END) <> 0 AND (case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 then 0.0000 else (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) end) <> 0 THEN 'Partially open'

END as executionstatusschedule
from fact_purchase  f_deliv, dim_purchasemisc atrb
 WHERE     f_deliv.dim_purchasemiscid = atrb.dim_purchasemiscid;
 
 
MERGE INTO fact_purchase fact
   USING (SELECT f_deliv.fact_purchaseid, t.executionstatusschedule
 AS dd_executionstatusschedule
         from tmp_for_upd_executionstatus t
, fact_purchase f_deliv
         where  f_deliv.dd_DocumentNo = t.dd_DocumentNo
and f_deliv.dd_DocumentItemNo=t.dd_DocumentItemNo
and f_deliv.dd_ScheduleNo=t.dd_ScheduleNo
and f_deliv.dd_executionstatusschedule <> t.executionstatusschedule ) src
   ON fact.fact_purchaseid = src.fact_purchaseid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_executionstatusschedule  = src.dd_executionstatusschedule
 WHERE ifnull(fact.dd_executionstatusschedule ,-1) <> ifnull(src.dd_executionstatusschedule ,-2);


