
/* Populate Relationship table fact_scn_rawmaterial_customer for supply chain navigation */

DROP TABLE IF EXISTS tmp_scn_rmc_fact_purchase_part;
CREATE TABLE tmp_scn_rmc_fact_purchase_part AS
SELECT 	distinct fp.dim_partid
from fact_purchase fp 
inner join dim_date d on fp.dim_dateidcreate = d.dim_dateid
inner join dim_vendor dv on fp.dim_vendorid = dv.dim_vendorid and dv.scnenabled = 1
inner join dim_part dp on fp.dim_partid = dp.dim_partid and dp.scnenabled = 1
WHERE d.datevalue > current_date-450;


DROP TABLE IF EXISTS tmp_scn_rmc_fact_salesorder_part;
CREATE TABLE tmp_scn_rmc_fact_salesorder_part AS
SELECT 	distinct f_so.dim_partid
from fact_salesorder f_so 
inner join dim_date agidt on f_so.dim_dateidactualgi = agidt.dim_dateid
inner join dim_customer dc on dc.dim_customerid = f_so.dim_customerid and dc.scnenabled = 1
inner join dim_part dp on dp.dim_partid = f_so.dim_partid and dp.scnenabled = 1
WHERE agidt.datevalue > current_date-450;

-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_rmc_fmm_prodorder_issue;   
create table tmp_scn_rmc_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, f.dd_productionordernumber, 'Issue' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_rmc_fmm_prodorder_receive;   
create table tmp_scn_rmc_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_rmc_fmm_prodorder_link;   
create table tmp_scn_rmc_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber
from tmp_scn_rmc_fmm_prodorder_receive por, tmp_scn_rmc_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;


-- Step 4 - Get BOM Root and Component
DROP TABLE IF EXISTS tmp_scn_rmc_bom;
CREATE TABLE tmp_scn_rmc_bom AS
SELECT DISTINCT f_bom.dd_ComponentNumber, f_bom.dd_RootPart
FROM fact_bom f_bom, dim_part cp, dim_part rp, tmp_scn_rmc_fact_purchase_part fp, tmp_scn_rmc_fact_salesorder_part fso
WHERE f_bom.dim_bomcomponentid = cp.dim_partid AND cp.scnenabled = 1
AND f_bom.dim_rootpartid = rp.dim_partid AND rp.scnenabled = 1
AND f_bom.dim_bomcomponentid = fp.dim_partid
AND f_bom.dim_rootpartid = fso.dim_partid;

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_rmc_bom (dd_RootPart,dd_ComponentNumber) 
select dd_rootpart, dd_ComponentNumber
from tmp_scn_rmc_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_rmc_bom b
where b.dd_ComponentNumber = m.dd_ComponentNumber and b.dd_RootPart = m.dd_rootpart);

DROP TABLE IF EXISTS tmp_scn_rmc_so_openordcount;
CREATE TABLE tmp_scn_rmc_so_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_part p, dim_customer c
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;


DROP TABLE IF EXISTS tmp_scn_rawmaterial_customer;
CREATE TABLE tmp_scn_rawmaterial_customer as
SELECT 	dpcomp.dim_partid dim_partid_rawmaterial, 
		dpcomp.partnumber dd_partnumber_rawmaterial, 
		dpcomp.partdescription dd_partdescription_rawmaterial,
		dpcomp.plant dd_plant_rawmaterial,
		dc.dim_customerid dim_customerid_customer, 
		dc.customernumber dd_customernumber_customer, 
		dc.name dd_customername_customer, 
		dc.city dd_customercity_customer, 
		dc.country dd_customercountry_customer,
		count(distinct f_bom.dd_RootPart) ct_productcount_rawmaterialcustomer, 
		sum(f_so.ct_DeliveredQty) ct_productqty_rawmaterialcustomer,
		sum( f_so.amt_exchangerate_gbl * f_so.ct_DeliveredQty * f_so.amt_UnitPriceUoM/case when f_so.ct_PriceUnit = 0 then 1.0000 else f_so.ct_PriceUnit end) amt_netitemvalueproduct_rawmaterialcustomer,
		COUNT(distinct f_so.dd_salesdocno) ct_socount_rawmaterialcustomer,
		COUNT(DISTINCT op.dd_salesdocno) ct_openordercount_rawmaterialcustomer

FROM dim_part dpcomp, tmp_scn_rmc_bom f_bom, dim_part product, fact_salesorder f_so
LEFT OUTER JOIN tmp_scn_rmc_so_openordcount op ON op.dd_salesdocno = f_so.dd_salesdocno,
dim_customer dc
WHERE f_bom.dd_ComponentNumber = dpcomp.partnumber and dpcomp.scnenabled = 1
AND f_bom.dd_RootPart = product.partnumber AND product.scnenabled = 1
AND  product.dim_partid = f_so.dim_partid 
AND f_so.dim_customerid = dc.dim_customerid AND dc.scnenabled = 1
GROUP BY 
dpcomp.dim_partid, dpcomp.partnumber, dpcomp.partdescription, dpcomp.plant,
dc.dim_customerid, dc.customernumber, dc.name, dc.city, dc.country;




/* Insert Into Fact Table */

truncate TABLE fact_scn_rawmaterial_customer;

delete from number_fountain m where m.table_name = 'fact_scn_rawmaterial_customer';

insert into number_fountain
select  'fact_scn_rawmaterial_customer',
        ifnull(max(f.fact_scn_rawmaterial_customerid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s ),1))
from fact_scn_rawmaterial_customer f
where f.fact_scn_rawmaterial_customerid <> 1;


insert into fact_scn_rawmaterial_customer 
( 
	fact_scn_rawmaterial_customerid, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ct_productcount_rawmaterialcustomer, 
	ct_productqty_rawmaterialcustomer, 
	amt_netitemvalueproduct_rawmaterialcustomer, 
	ct_socount_rawmaterialcustomer, 
	ct_openordercount_rawmaterialcustomer
)
select 
  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_rawmaterial_customer') + 
      row_number() over(order by dim_partid_rawmaterial, dim_customerid_customer) 	fact_scn_rawmaterial_customerid, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ct_productcount_rawmaterialcustomer, 
	ct_productqty_rawmaterialcustomer, 
	amt_netitemvalueproduct_rawmaterialcustomer, 
	ct_socount_rawmaterialcustomer, 
	ct_openordercount_rawmaterialcustomer
from tmp_scn_rawmaterial_customer s;



/* Copy To Mirror Table */
truncate table fact_scn_customer_rawmaterial;

insert into fact_scn_customer_rawmaterial
(
	fact_scn_customer_rawmaterialid, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ct_productcount_rawmaterialcustomer, 
	ct_productqty_rawmaterialcustomer, 
	amt_netitemvalueproduct_rawmaterialcustomer, 
	ct_socount_rawmaterialcustomer, 
	ct_openordercount_rawmaterialcustomer
)
select
	fact_scn_rawmaterial_customerid, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_customerid_customer, 
	dd_customernumber_customer, 
	dd_customername_customer, 
	dd_customercity_customer, 
	dd_customercountry_customer, 
	ct_productcount_rawmaterialcustomer, 
	ct_productqty_rawmaterialcustomer, 
	amt_netitemvalueproduct_rawmaterialcustomer, 
	ct_socount_rawmaterialcustomer, 
	ct_openordercount_rawmaterialcustomer
from fact_scn_rawmaterial_customer;



/* Drop Temp Tables */
DROP TABLE IF EXISTS tmp_scn_rmc_fact_purchase_part;
DROP TABLE IF EXISTS tmp_scn_rmc_fact_salesorder_part;
DROP TABLE IF EXISTS tmp_scn_rmc_bom;
DROP TABLE IF EXISTS tmp_scn_rmc_so_openordcount;
DROP TABLE IF EXISTS tmp_scn_rawmaterial_customer;

DROP TABLE IF EXISTS tmp_scn_rmc_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_rmc_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_rmc_fmm_prodorder_link;







