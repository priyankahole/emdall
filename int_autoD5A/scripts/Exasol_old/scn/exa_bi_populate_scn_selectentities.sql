/*
The following script is used to select data for the supply chain navigation model. 
*/


/* Step 1 - Identify the Products with Active BOM */
drop table if exists tmp_scn_productswithbom;
create table tmp_scn_productswithbom as 
select distinct rp.dim_partid, rp.partnumber as toplevelpart, rp.plant as toplevelplant, rp.parttype as toplevelparttype, rp.parttypedescription as toplevelparttypedescription
from fact_bom f, dim_part cp, dim_part pp, dim_part rp, dim_date vf, dim_date vt,
	 dim_bomstatus 	boms, dim_bomcategory 	bc, dim_billofmaterialmisc 	bomis, dim_bomitemcategory bicat
where f.dim_rootpartid <> 1 and f.dim_bomcomponentid <> 1 
and f.dim_bomcomponentid = cp.dim_partid and f.dim_partid = pp.dim_partid 
and f.dim_rootpartid = rp.dim_partid and f.dim_validfromdateid = vf.dim_dateid
and f.dim_dateidvalidto = vt.dim_dateid and f.dim_bomstatusid = boms.dim_bomstatusid	
and f.dim_bomcategoryid = bc.dim_bomcategoryid
and f.dim_billofmaterialmiscid = bomis.dim_billofmaterialmiscid
and f.dim_bomitemcategoryid = bicat.dim_bomitemcategoryid
and boms.description = 'Active' ; --and vt.datevalue > current_date--6692

drop table if exists tmp_scn_prodorder_receive_mm;
create table  tmp_scn_prodorder_receive_mm
as
select 
dp.dim_partid, dp.partnumber as toplevelpart, dp.plant as toplevelplant, dp.parttype as toplevelparttype,
 dp.parttypedescription as toplevelparttypedescription,
 f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  
and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
--and dp.scnenabled = 1
group by dp.dim_partid, dp.partnumber , dp.plant , dp.parttype ,dp.parttypedescription, f.dd_productionordernumber
having sum(f.ct_quantity) > 0;

insert into tmp_scn_productswithbom
(dim_partid, toplevelpart, toplevelplant, toplevelparttype,toplevelparttypedescription)
select 
dim_partid, toplevelpart, toplevelplant, toplevelparttype,toplevelparttypedescription
from tmp_scn_prodorder_receive_mm mm
where not exists (select 1 from tmp_scn_productswithbom bom where bom.dim_partid = mm.dim_partid);

/* Compare Part/Plant for Part Type */
/*
drop table if exists tmp_ca_parttypeswithbom
create table tmp_ca_parttypeswithbom as 
select rp.parttype, rp.parttypedescription, count(*) as rowcount
-- cp.parttype, cp.parttypedescription, count(*) as rowcount
from fact_bom f, dim_part cp, dim_part pp, dim_part rp, dim_date vf, dim_date vt,
dim_bomstatus 	boms, dim_bomcategory 	bc, dim_billofmaterialmisc 	bomis, dim_bomitemcategory bicat
where f.dim_rootpartid <> 1 and f.dim_bomcomponentid <> 1 
and f.dim_bomcomponentid = cp.dim_partid and f.dim_partid = pp.dim_partid 
and f.dim_rootpartid = rp.dim_partid and f.dim_validfromdateid = vf.dim_dateid
and f.dim_dateidvalidto = vt.dim_dateid and f.dim_bomstatusid = boms.dim_bomstatusid	
and f.dim_bomcategoryid = bc.dim_bomcategoryid
and f.dim_billofmaterialmiscid = bomis.dim_billofmaterialmiscid
and f.dim_bomitemcategoryid = bicat.dim_bomitemcategoryid
and boms.description = 'Active' and vt.datevalue > current_date
group by rp.parttype, rp.parttypedescription
--group by cp.parttype, cp.parttypedescription
*/


/* Step 2 - Get Sales Order Amt for the Products with Active BOM */
drop table if exists tmp_scn_fpbom_sales;
create table tmp_scn_fpbom_sales as 
select t.*, sum(CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) orderamt
from tmp_scn_productswithbom t, fact_salesorder f, dim_date soc, dim_salesmisc dsi
where --parttype = 'FG' and 
t.dim_partid = f.dim_partid
and f.dim_dateidsalesordercreated = soc.dim_dateid
and f.dim_salesmiscid =	dsi.dim_salesmiscid
--and soc.datevalue >= '2014-01-01'
group by t.*;
--having sum(CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) > 0; --1829/1934/2229

/* select count(distinct dim_partid ) from tmp_scn_productswithbom t
where exists(select 1 from  fact_salesorder f where t.dim_partid = f.dim_partid)
*/

/* Step 3 - Get Component Part No. from Material Movement */
-- Step 3a - Get Issue Parts For a Production Order
drop table if exists tmp_scn_fp_fmm_prodorder_issue;   
create table tmp_scn_fp_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, dp.dim_partid dim_BOMcomponentid,dp.plant dd_componentplant, f.dd_productionordernumber, 
'Issue' as dd_activity, (-1 * sum(f.ct_quantity)) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
group by dp.partnumber, f.dd_productionordernumber, dp.dim_partid, dp.plant
having sum(f.ct_quantity) < 0;

-- Step 3b - Get Receive Parts For a Production Order
drop table if exists tmp_scn_fp_fmm_prodorder_receive;   
create table tmp_scn_fp_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, dp.dim_partid dim_rootpartid, dp.plant dd_rootplant, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
group by dp.partnumber, f.dd_productionordernumber, dp.dim_partid, dp.plant
having sum(f.ct_quantity) > 0;

-- Step 3c - Join the above queries on Production Order
drop table if exists tmp_scn_fp_fmm_prodorder_link;   
create table tmp_scn_fp_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber, por.dim_rootpartid, por.dd_rootplant, poi.dim_BOMcomponentid, poi.dd_componentplant,
poi.ct_quantity ct_componentqty, 1 as dd_level
from tmp_scn_fp_fmm_prodorder_receive por, tmp_scn_fp_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;

-- Step 3d - Get BOM Root and Component
DROP TABLE IF EXISTS tmp_scn_fp_bom;
CREATE TABLE tmp_scn_fp_bom AS
SELECT distinct f_bom.dim_rootpartid, f_bom.dd_RootPart, f_bom.dd_ComponentNumber, f_bom.dim_bomcomponentid
FROM fact_bom f_bom, dim_part dpcomp
WHERE f_bom.dd_ComponentNumber = dpcomp.partnumber; -- AND dpcomp.scnenabled = 1

-- Step 3e - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_fp_bom (dd_RootPart,dd_ComponentNumber,dim_rootpartid, dim_bomcomponentid ) 
select dd_rootpart, dd_ComponentNumber, dim_rootpartid, dim_BOMcomponentid
from tmp_scn_fp_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_fp_bom b
where b.dim_BOMcomponentid = m.dim_BOMcomponentid and b.dim_rootpartid = m.dim_rootpartid);

/* Step 4 - Identify all components of the Products with Active BOM */
drop table if exists tmp_scn_bomrc;
create table tmp_scn_bomrc as 
select distinct f.dd_rootpart, f.dd_componentnumber,1 as dd_level
from tmp_scn_fpbom_sales t, tmp_scn_fp_bom f
where t.dim_partid = f.dim_rootpartid;


/* Step 5 - Identify Dim_Part Records for Update */
drop table if exists tmp_scn_partselect;
create table tmp_scn_partselect as 
select distinct p.dim_partid, p.partnumber, p.partdescription, p.parttype, p.parttypedescription, p.plant  
from dim_part p inner join 
( select distinct partnumber
from (
select distinct dd_rootpart as partnumber from tmp_scn_bomrc union all
select  distinct dd_componentnumber as partnumber from tmp_scn_bomrc ) p
) f on p.partnumber = f.partnumber; --19558


-- Check Data Queries
/*
select parttype, parttypedescription, count(*) selectcount
from tmp_scn_partselect
group by parttype, parttypedescription

select plant, count(*) selectcount
from tmp_scn_partselect
group by plant
*/


/* Final Step */

/* Update Dim_Part */

-- reset for new data
update dim_part p
set p.scnenabled = 0;

-- set for scn 
update dim_part p
set p.scnenabled = 1
from tmp_scn_partselect t, dim_part p
where p.dim_partid = t.dim_partid;  --19558


/* Update Dim_Vendor */

drop table if exists tmp_scn_vendor;
create table tmp_scn_vendor as 
select distinct f.dim_vendorid
from fact_purchase f, dim_part p, dim_vendor v
where f.dim_partid <> 1 and f.dim_partid = p.dim_partid and p.scnenabled = 1 and v.dim_vendorid = f.dim_vendorid;

-- reset for new data
update dim_vendor v
set v.scnenabled = 0;

-- set for scn
update dim_vendor v
set v.scnenabled = 1
from tmp_scn_vendor t, dim_vendor v
where v.dim_vendorid = t.dim_vendorid; --599


/* Update Dim_Customer */

drop table if exists tmp_scn_customer;
create table tmp_scn_customer as 
select distinct f.dim_customerid
from fact_salesorder f, dim_part p, dim_customer c
where f.dim_partid <> 1 and f.dim_partid = p.dim_partid and p.scnenabled = 1 and c.dim_customerid = f.dim_customerid;

-- reset for new data
update dim_customer c
set c.scnenabled = 0;

-- set for scn
update dim_customer c
set c.scnenabled = 1
from tmp_scn_customer t, dim_customer c
where c.dim_customerid = t.dim_customerid; --13380


/* Update Dim_Plant */
drop table if exists tmp_scn_plant;
create table tmp_scn_plant as 
select p.dim_plantid, p.plantcode, p.projectsourceid
from dim_plant p inner join 
(
select distinct dim_plantid
from
(
select distinct dim_plantidsupplying as dim_plantid from fact_purchase where dim_plantidsupplying <> 1 and dim_vendorid in (select dim_vendorid from dim_vendor where scnenabled = 1)
union all
select distinct dim_plantidsupplying as dim_plantid from fact_purchase where dim_plantidsupplying <> 1 and dim_vendorid in (select dim_vendorid from dim_vendor where scnenabled = 1)
union all
select distinct dim_plantid from fact_salesorder where dim_plantid <> 1 and dim_customerid in (select dim_customerid from dim_customer where scnenabled = 1)
union all
select dim_plantid from dim_plant where  dim_plantid <> 1 and plantcode in (select distinct plant from dim_part where scnenabled = 1)
) x
) f on p.dim_plantid = f.dim_plantid
 and  p.dim_plantid <> 1;

-- reset for new data
update dim_plant pl
set pl.scnenabled = 0;

-- set for scn
update dim_plant p
set p.scnenabled = 1
from tmp_scn_plant t, dim_plant p
where p.dim_plantid = t.dim_plantid
and p.dim_plantid <> 1; --29



/* Drop Temp Tables */
drop table if exists tmp_scn_productswithbom;
drop table if exists tmp_scn_fpbom_sales;
drop table if exists tmp_scn_bomrc;
drop table if exists tmp_scn_partselect;
drop table if exists tmp_scn_vendor;
drop table if exists tmp_scn_customer;

