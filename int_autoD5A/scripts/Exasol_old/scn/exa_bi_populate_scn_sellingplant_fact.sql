
/* Populate Relationship table fact_scn_sellingplant for supply chain navigation */


/* Direct Measures */

DROP TABLE IF EXISTS tmp_scn_spl_openordcount;
CREATE TABLE tmp_scn_spl_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_part p, dim_customer c
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;

DROP TABLE IF EXISTS tmp_scn_sellingplant;
CREATE TABLE tmp_scn_sellingplant as
select 
dp.dim_plantid dim_plantid_sellingplant, dp.plantcode dd_plantcode_sellingplant, dp.name dd_plantname_sellingplant, dp.city dd_plantcity_sellingplant, dp.country dd_plantcountry_sellingplant,

/* Related measures */
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_itemnetvalue_sellingplant,

count(distinct f_so.dim_partid) ct_productcount_sellingplant,
count(distinct f_so.dim_customerid) ct_customercount_sellingplant,
count(distinct f_so.dd_salesdocno) ct_socount_sellingplant,
count(DISTINCT op.dd_salesdocno) ct_openordercount_sellingplant,

(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_sellingplant,
/* Indirect measures */
cast(0 as decimal(36,0)) ct_rawmaterialcount_sellingplant,
cast(0 as decimal(36,0)) ct_poplantcount_sellingplant,
cast(0 as decimal(36,0)) ct_suppliercount_sellingplant

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_spl_openordcount op on f_so.dd_salesdocno = op.dd_salesdocno,
dim_part prt, dim_customer c, dim_plant dp, dim_salesdocumenttype sdt, dim_date agidt, dim_date pgid --, tmp_lastyeardate_scn t
WHERE f_so.dim_partid = prt.dim_partid AND prt.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.dim_plantid = dp.dim_plantid
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid
-- AND agidt.datevalue  >= t.dt_prevyear
GROUP BY dp.dim_plantid, dp.plantcode, dp.name,dp.city,dp.country;


INSERT INTO tmp_scn_sellingplant
(	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	amt_itemnetvalue_sellingplant, 
	ct_productcount_sellingplant, 
	ct_customercount_sellingplant, 
	ct_socount_sellingplant, 
	ct_openordercount_sellingplant, 
	ct_otif_sellingplant, 
	ct_rawmaterialcount_sellingplant, 
	ct_poplantcount_sellingplant, 
	ct_suppliercount_sellingplant
)
SELECT
0 dim_plantid_sellingplant, 'ALL' dd_plantcode_sellingplant, 'ALL' dd_plantname_sellingplant,'ALL' dd_plantcity_sellingplant, 'ALL' dd_plantcountry_sellingplant,
/* Related measures */
sum((f_so.ct_ConfirmedQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_itemnetvalue_sellingplant,
count(distinct f_so.dim_partid) ct_productcount_sellingplant,
count(distinct f_so.dim_customerid) ct_customercount_sellingplant,
count(distinct f_so.dd_salesdocno) ct_socount_sellingplant,
count(DISTINCT op.dd_salesdocno) ct_openordercount_sellingplant,

(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_sellingplant,
/* Indirect measures */
0 ct_rawmaterialcount_sellingplant,
0 ct_poplantcount_sellingplant,
0 ct_suppliercount_sellingplant

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_spl_openordcount op on f_so.dd_salesdocno = op.dd_salesdocno,
dim_part prt, dim_customer c, dim_plant dp, dim_salesdocumenttype sdt, dim_date agidt, dim_date pgid --, tmp_lastyeardate_scn t
WHERE f_so.dim_partid = prt.dim_partid AND prt.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.dim_plantid = dp.dim_plantid
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid
--AND agidt.datevalue  >= t.dt_prevyear
;


-- Indirect Measures

/* To Reduce Rows Use Last Year SO Activity Data */
/*
DROP TABLE IF EXISTS tmp_scn_so_recentdata
CREATE TABLE tmp_scn_so_recentdata AS
SELECT current_date - 365 as dd_recentdate
*/

-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_spl_fmm_prodorder_issue;   
create table tmp_scn_spl_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, f.dd_productionordernumber, 'Issue' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_spl_fmm_prodorder_receive;   
create table tmp_scn_spl_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_spl_fmm_prodorder_link;   
create table tmp_scn_spl_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber
from tmp_scn_spl_fmm_prodorder_receive por,  tmp_scn_spl_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;


-- Step 4 - Get BOM Root and Component
DROP TABLE IF EXISTS tmp_scn_spl_bom;
CREATE TABLE tmp_scn_spl_bom AS
SELECT DISTINCT f_bom.dd_ComponentNumber,f_bom.dd_RootPart
FROM fact_bom f_bom, dim_part cp, dim_part rp
WHERE f_bom.dim_bomcomponentid = cp.dim_partid AND cp.scnenabled = 1
AND f_bom.dim_rootpartid = rp.dim_partid AND rp.scnenabled = 1;

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_spl_bom (dd_RootPart,dd_ComponentNumber) 
select dd_rootpart, dd_ComponentNumber
from tmp_scn_spl_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_spl_bom b
where b.dd_ComponentNumber = m.dd_ComponentNumber and b.dd_RootPart = m.dd_rootpart);

DROP TABLE IF EXISTS tmp_scn_spl_sales_bom;
CREATE TABLE tmp_scn_spl_sales_bom AS
SELECT distinct f_so.dim_plantid, bom.dd_RootPart
FROM  fact_salesorder f_so 
      inner join dim_part dp on f_so.dim_partid = dp.dim_partid and dp.scnenabled = 1
      inner join dim_customer c on f_so.dim_customerid = c.dim_customerid and c.scnenabled = 1
      left outer join tmp_scn_spl_bom bom on dp.partnumber = bom.dd_RootPart;


DROP TABLE IF EXISTS tmp_scn_spl_purchase_bom;
CREATE TABLE tmp_scn_spl_purchase_bom AS
SELECT distinct f.dim_vendorid, f.dim_plantidordering, bom.dd_ComponentNumber
FROM  fact_purchase f 
      inner join dim_part dp on f.dim_partid = dp.dim_partid and dp.scnenabled = 1
      inner join dim_vendor v on f.dim_vendorid = v.dim_vendorid and v.scnenabled = 1
      left outer join tmp_scn_spl_bom bom on dp.partnumber = bom.dd_ComponentNumber;


/* Update indirect measures */
DROP TABLE IF EXISTS tmp_scn_sellingplant_indirect;
CREATE TABLE tmp_scn_sellingplant_indirect AS
SELECT f_so.dim_plantid dim_plantid_sellingplant,
count(distinct f_bom.dd_ComponentNumber) ct_rawmaterialcount_sellingplant,
count(distinct f.dim_plantidordering) ct_poplantcount_sellingplant,
count(distinct f.dim_vendorid) ct_suppliercount_sellingplant
FROM tmp_scn_spl_bom f_bom, tmp_scn_spl_purchase_bom f, tmp_scn_spl_sales_bom f_so
WHERE f_bom.dd_RootPart = f_so.dd_RootPart
AND f_bom.dd_ComponentNumber = f.dd_ComponentNumber
GROUP BY f_so.dim_plantid;


INSERT INTO tmp_scn_sellingplant_indirect
SELECT 0 dim_plantid_sellingplant,
count(distinct f_bom.dd_ComponentNumber) ct_rawmaterialcount_sellingplant,
count(distinct f.dim_plantidordering) ct_poplantcount_sellingplant,
count(distinct f.dim_vendorid) ct_suppliercount_sellingplant
FROM tmp_scn_spl_bom f_bom, tmp_scn_spl_purchase_bom f, tmp_scn_spl_sales_bom f_so
WHERE f_bom.dd_RootPart = f_so.dd_RootPart
AND f_bom.dd_ComponentNumber = f.dd_ComponentNumber;


UPDATE tmp_scn_sellingplant s
SET s.ct_rawmaterialcount_sellingplant = t.ct_rawmaterialcount_sellingplant,
	s.ct_poplantcount_sellingplant = t.ct_poplantcount_sellingplant,
	s.ct_suppliercount_sellingplant = t.ct_suppliercount_sellingplant
FROM tmp_scn_sellingplant_indirect t, tmp_scn_sellingplant s
WHERE s.dim_plantid_sellingplant = t.dim_plantid_sellingplant;



/* Insert Into Fact Table */

truncate TABLE fact_scn_sellingplant;

delete from number_fountain m where m.table_name = 'fact_scn_sellingplant';

insert into number_fountain
select  'fact_scn_sellingplant',
        ifnull(max(f.fact_scn_sellingplantid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_scn_sellingplant f
where f.fact_scn_sellingplantid <> 1;


insert into fact_scn_sellingplant 
( 
	fact_scn_sellingplantid, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	amt_itemnetvalue_sellingplant, 
	ct_productcount_sellingplant, 
	ct_customercount_sellingplant, 
	ct_socount_sellingplant, 
	ct_openordercount_sellingplant, 
	ct_otif_sellingplant, 
	ct_rawmaterialcount_sellingplant, 
	ct_poplantcount_sellingplant, 
	ct_suppliercount_sellingplant
)
select 
  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_sellingplant') + 
      row_number() over(order by dim_plantid_sellingplant) 	fact_scn_sellingplantid, 
	dim_plantid_sellingplant, 
	dd_plantcode_sellingplant, 
	dd_plantname_sellingplant, 
	dd_plantcity_sellingplant, 
	dd_plantcountry_sellingplant, 
	ifnull(amt_itemnetvalue_sellingplant,0) amt_itemnetvalue_sellingplant, 
	ct_productcount_sellingplant, 
	ct_customercount_sellingplant, 
	ct_socount_sellingplant, 
	ct_openordercount_sellingplant, 
	ct_otif_sellingplant, 
	ct_rawmaterialcount_sellingplant, 
	ct_poplantcount_sellingplant, 
	ct_suppliercount_sellingplant
from tmp_scn_sellingplant s;




/* Drop Temp Tables */

DROP TABLE IF EXISTS tmp_scn_spl_openordcount;
DROP TABLE IF EXISTS tmp_scn_spl_bom;
DROP TABLE IF EXISTS tmp_scn_spl_sales_bom;
DROP TABLE IF EXISTS tmp_scn_spl_purchase_bom;

DROP TABLE IF EXISTS tmp_scn_sellingplant_indirect;
DROP TABLE IF EXISTS tmp_scn_sellingplant;

DROP TABLE IF EXISTS tmp_scn_spl_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_spl_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_spl_fmm_prodorder_link;

