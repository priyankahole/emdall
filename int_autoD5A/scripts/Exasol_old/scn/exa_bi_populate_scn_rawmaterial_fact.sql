
/* Populate Relationship table fact_scn_rawmaterial for supply chain navigation */


-- Direct Measures

DROP TABLE IF EXISTS tmp_scn_rawmaterial_1;
CREATE TABLE tmp_scn_rawmaterial_1 as
select 
--dp.dim_partid dim_partid_rawmaterial, dp.partnumber dd_partnumber_rawmaterial, dp.partdescription dd_partdescription_rawmaterial, dp.plant dd_plant_rawmaterial,
dp.partnumber dd_partnumber_rawmaterial,
sum(amt_ItemNetValue * amt_exchangerate_gbl) amt_itemnetvalue_rawmaterial,
count(distinct f.dim_plantidordering) ct_plantcount_rawmaterial,
count(distinct f.dim_vendorid) ct_suppliercount_rawmaterial,
/*count(distinct rm_prod.dim_partid_product) ct_productcount_rawmaterial,*/
cast(0 as decimal(36,0)) ct_productcount_rawmaterial,
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END)/CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_rawmaterial,
count(DISTINCT f.dd_DocumentNo) ct_pocount_rawmaterial,
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_rawmaterial,
cast(0 as decimal(36,2)) ct_supplierperformancescore_rawmaterial,
sum(ct_ReceivedQty) ct_poqty_rawmaterial,
/* Indirect measures */
cast(0 as decimal(36,0)) ct_customercount_rawmaterial,
cast(0 as decimal(36,0)) ct_salesordercount_rawmaterial,
cast(0 as decimal(36,0)) ct_opensalesordercount_rawmaterial,
cast(0 as decimal(36,0)) ct_sellingplantcount_rawmaterial

FROM fact_purchase f, dim_date d, dim_vendor dv, dim_part dp, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid 
AND f.dim_vendorid = dv.dim_vendorid AND dv.scnenabled = 1
AND f.dim_partid = dp.dim_partid AND dp.scnenabled = 1
AND f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_vendorid <> 1 AND f.dim_partid <> 1 AND ct_DeliveryQty > 0	/* Some sanitization */
GROUP BY 
--dp.dim_partid,dp.partnumber,dp.partdescription,dp.plant
dp.partnumber;

DROP TABLE IF EXISTS tmp_scn_rawmaterial;
CREATE TABLE tmp_scn_rawmaterial AS
SELECT s.*,dp.dim_partid dim_partid_rawmaterial, dp.partdescription dd_partdescription_rawmaterial, dp.plant dd_plant_rawmaterial
FROM tmp_scn_rawmaterial_1 s, dim_part dp
WHERE s.dd_partnumber_rawmaterial = dp.partnumber;


/* Add 1 row that corresponds to all parts */
DROP TABLE IF EXISTS tmp_scn_rawmaterial_all;
CREATE TABLE tmp_scn_rawmaterial_all as
select 
sum(amt_ItemNetValue * amt_exchangerate_gbl) amt_itemnetvalue_rawmaterial,
count(distinct f.dim_plantidordering) ct_plantcount_rawmaterial,
count(distinct f.dim_vendorid) ct_suppliercount_rawmaterial,
/* count(distinct rm_prod.dim_partid_product) ct_productcount_rawmaterial, */
cast(0 as int) ct_productcount_rawmaterial,
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END)/CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_rawmaterial,
count(DISTINCT f.dd_DocumentNo) ct_pocount_rawmaterial,
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_rawmaterial,
cast(0 as decimal(36,2)) ct_supplierperformancescore_rawmaterial,
sum(ct_ReceivedQty) ct_poqty_rawmaterial,
/* Indirect measures */
cast(0 as decimal(36,0)) ct_customercount_rawmaterial,
cast(0 as decimal(36,0)) ct_salesordercount_rawmaterial,
cast(0 as decimal(36,0)) ct_opensalesordercount_rawmaterial,
cast(0 as decimal(36,0)) ct_sellingplantcount_rawmaterial

FROM fact_purchase f, dim_date d, dim_vendor dv, dim_part dp, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid 
AND f.dim_vendorid = dv.dim_vendorid AND dv.scnenabled = 1
AND f.dim_partid = dp.dim_partid AND dp.scnenabled = 1
AND f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_vendorid <> 1 AND f.dim_partid <> 1 AND ct_DeliveryQty > 0;

insert into tmp_scn_rawmaterial
(  
  dim_partid_rawmaterial, 
  dd_partnumber_rawmaterial, 
  dd_partdescription_rawmaterial, 
  dd_plant_rawmaterial, 
  amt_itemnetvalue_rawmaterial, 
  ct_plantcount_rawmaterial, 
  ct_suppliercount_rawmaterial, 
  ct_productcount_rawmaterial, 
  ct_otif_rawmaterial, 
  ct_pocount_rawmaterial, 
  ct_openordercount_rawmaterial, 
  ct_supplierperformancescore_rawmaterial, 
  ct_poqty_rawmaterial, 
  ct_customercount_rawmaterial, 
  ct_salesordercount_rawmaterial, 
  ct_opensalesordercount_rawmaterial, 
  ct_sellingplantcount_rawmaterial
)
select
  0 as dim_partid_rawmaterial, 
  'ALL' as dd_partnumber_rawmaterial, 
  'ALL' as dd_partdescription_rawmaterial, 
  'ALL' as dd_plant_rawmaterial, 
  amt_itemnetvalue_rawmaterial, 
  ct_plantcount_rawmaterial, 
  ct_suppliercount_rawmaterial, 
  ct_productcount_rawmaterial, 
  ct_otif_rawmaterial, 
  ct_pocount_rawmaterial, 
  ct_openordercount_rawmaterial, 
  ct_supplierperformancescore_rawmaterial, 
  ct_poqty_rawmaterial, 
  ct_customercount_rawmaterial, 
  ct_salesordercount_rawmaterial, 
  ct_opensalesordercount_rawmaterial, 
  ct_sellingplantcount_rawmaterial
from tmp_scn_rawmaterial_all;



/* Update indirect measures */
DROP TABLE IF EXISTS tmp_scn_rw_openordcount;
CREATE TABLE tmp_scn_rw_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_customer c, dim_part p
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;

-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_rw_fmm_prodorder_issue;   
create table tmp_scn_rw_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, f.dd_productionordernumber, 'Issue' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_rw_fmm_prodorder_receive;   
create table tmp_scn_rw_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_rw_fmm_prodorder_link;   
create table tmp_scn_rw_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber
from tmp_scn_rw_fmm_prodorder_receive por, tmp_scn_rw_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;


-- Step 4 - Get BOM Root and Component
drop table if exists tmp_scn_rw_bom;
CREATE TABLE tmp_scn_rw_bom AS
SELECT DISTINCT f_bom.dd_ComponentNumber,f_bom.dd_RootPart 
from fact_bom f_bom, dim_part cp, dim_part rp 
WHERE f_bom.dim_bomcomponentid = cp.dim_partid AND cp.scnenabled = 1
AND f_bom.dim_rootpartid = rp.dim_partid AND rp.scnenabled = 1;

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_rw_bom (dd_RootPart,dd_ComponentNumber) 
select dd_rootpart, dd_ComponentNumber
from tmp_scn_rw_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_rw_bom b
where b.dd_ComponentNumber = m.dd_ComponentNumber and b.dd_RootPart = m.dd_rootpart);

/* To Reduce Rows Use Last Year SO Activity Data */
/*
DROP TABLE IF EXISTS tmp_scn_so_recentdata
CREATE TABLE tmp_scn_so_recentdata AS
SELECT current_date - 365 as dd_recentdate
*/

/*
DROP TABLE IF EXISTS tmp_scn_rw_sales_bom
CREATE TABLE tmp_scn_rw_sales_bom AS
SELECT distinct f_so.dim_customerid, f_so.dim_plantid, f_so.dd_salesdocno, op.dd_salesdocno opensalesdocno, bom.dd_rootpart
FROM  fact_salesorder f_so 
      inner join dim_part dp on f_so.dim_partid = dp.dim_partid
      left outer join tmp_openordcount_scn op ON op.dd_salesdocno = f_so.dd_salesdocno
      left outer join tmp_bom_scn bom on dp.partnumber = bom.dd_RootPart
*/


DROP TABLE IF EXISTS tmp_scn_rawmaterial_indirect;
CREATE TABLE tmp_scn_rawmaterial_indirect AS
SELECT dpcomp.dim_partid dim_partid_rawmaterial,
COUNT(DISTINCT bom.dd_RootPart) ct_productcount_rawmaterial,
count(distinct f_so.dim_customerid) ct_customercount_rawmaterial,
count(distinct f_so.dd_salesdocno) ct_salesordercount_rawmaterial,
count(DISTINCT op.dd_salesdocno) ct_opensalesordercount_rawmaterial,
count(distinct f_so.dim_plantid)  ct_sellingplantcount_rawmaterial
FROM fact_salesorder f_so 
      inner join dim_date agi on f_so.dim_dateidactualgi = agi.dim_dateid and agi.datevalue > current_date-365
      inner join dim_part dp on f_so.dim_partid = dp.dim_partid AND dp.scnenabled = 1
      inner join dim_customer c on f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
      LEFT OUTER JOIN tmp_scn_rw_openordcount op ON op.dd_salesdocno = f_so.dd_salesdocno
      inner join tmp_scn_rw_bom bom on dp.partnumber = bom.dd_RootPart
      inner join dim_part dpcomp on bom.dd_ComponentNumber = dpcomp.partnumber AND dpcomp.scnenabled = 1
-- WHERE  
group by dpcomp.dim_partid;

DROP TABLE IF EXISTS tmp_scn_rawmaterial_indirect_all;
CREATE TABLE tmp_scn_rawmaterial_indirect_all AS
SELECT 
COUNT(DISTINCT bom.dd_RootPart) ct_productcount_rawmaterial,
count(distinct f_so.dim_customerid) ct_customercount_rawmaterial,
count(distinct f_so.dd_salesdocno) ct_salesordercount_rawmaterial,
count(DISTINCT op.dd_salesdocno) ct_opensalesordercount_rawmaterial,
count(distinct f_so.dim_plantid)  ct_sellingplantcount_rawmaterial
FROM fact_salesorder f_so 
      inner join dim_date agi on f_so.dim_dateidactualgi = agi.dim_dateid and agi.datevalue > current_date-365
      inner join dim_part dp on f_so.dim_partid = dp.dim_partid AND dp.scnenabled = 1
      inner join dim_customer c on f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
      LEFT OUTER JOIN tmp_scn_rw_openordcount op ON op.dd_salesdocno = f_so.dd_salesdocno
      inner join tmp_scn_rw_bom bom on dp.partnumber = bom.dd_RootPart
      inner join dim_part dpcomp on bom.dd_ComponentNumber = dpcomp.partnumber AND dpcomp.scnenabled = 1
-- WHERE  
;

insert into tmp_scn_rawmaterial_indirect
( dim_partid_rawmaterial,
  ct_productcount_rawmaterial,
  ct_customercount_rawmaterial,
  ct_salesordercount_rawmaterial,
  ct_opensalesordercount_rawmaterial,
  ct_sellingplantcount_rawmaterial
)
select
0 dim_partid_rawmaterial,
ct_productcount_rawmaterial,
ct_customercount_rawmaterial,
ct_salesordercount_rawmaterial,
ct_opensalesordercount_rawmaterial,
ct_sellingplantcount_rawmaterial
from tmp_scn_rawmaterial_indirect_all;


UPDATE tmp_scn_rawmaterial rm
SET  rm.ct_productcount_rawmaterial = rm_so.ct_productcount_rawmaterial,
	   rm.ct_customercount_rawmaterial = rm_so.ct_customercount_rawmaterial,
	   rm.ct_salesordercount_rawmaterial = rm_so.ct_salesordercount_rawmaterial,
	   rm.ct_opensalesordercount_rawmaterial = rm_so.ct_opensalesordercount_rawmaterial,
	   rm.ct_sellingplantcount_rawmaterial = rm_so.ct_sellingplantcount_rawmaterial
FROM tmp_scn_rawmaterial_indirect rm_so, tmp_scn_rawmaterial rm     
WHERE rm.dim_partid_rawmaterial = rm_so.dim_partid_rawmaterial;



/* Insert Into Fact Table */

truncate TABLE fact_scn_rawmaterial;

delete from number_fountain m where m.table_name = 'fact_scn_rawmaterial';

insert into number_fountain
select  'fact_scn_rawmaterial',
        ifnull(max(f.fact_scn_rawmaterialid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_scn_rawmaterial f
where f.fact_scn_rawmaterialid <> 1;


insert into fact_scn_rawmaterial 
( 
  fact_scn_rawmaterialid, 
  dim_partid_rawmaterial, 
  dd_partnumber_rawmaterial, 
  dd_partdescription_rawmaterial, 
  dd_plant_rawmaterial, 
  amt_itemnetvalue_rawmaterial, 
  ct_plantcount_rawmaterial, 
  ct_suppliercount_rawmaterial, 
  ct_productcount_rawmaterial, 
  ct_otif_rawmaterial, 
  ct_pocount_rawmaterial, 
  ct_openordercount_rawmaterial, 
  ct_supplierperformancescore_rawmaterial, 
  ct_poqty_rawmaterial, 
  ct_customercount_rawmaterial, 
  ct_salesordercount_rawmaterial, 
  ct_opensalesordercount_rawmaterial, 
  ct_sellingplantcount_rawmaterial 
  -- amt_openorder
)
select 
  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_rawmaterial') + 
      row_number() over(order by dim_partid_rawmaterial) fact_scn_rawmaterialid, 
  dim_partid_rawmaterial, 
  dd_partnumber_rawmaterial, 
  dd_partdescription_rawmaterial, 
  dd_plant_rawmaterial, 
  amt_itemnetvalue_rawmaterial, 
  ct_plantcount_rawmaterial, 
  ct_suppliercount_rawmaterial, 
  ct_productcount_rawmaterial, 
  ct_otif_rawmaterial, 
  ct_pocount_rawmaterial, 
  ct_openordercount_rawmaterial, 
  ct_supplierperformancescore_rawmaterial, 
  ct_poqty_rawmaterial, 
  ct_customercount_rawmaterial, 
  ct_salesordercount_rawmaterial, 
  ct_opensalesordercount_rawmaterial, 
  ct_sellingplantcount_rawmaterial 
  -- amt_openorder
from tmp_scn_rawmaterial s;



/* Drop Temp Tables */
DROP TABLE IF EXISTS tmp_scn_rw_openordcount;
drop table if exists tmp_scn_rw_bom;

DROP TABLE IF EXISTS tmp_scn_rawmaterial_1;
DROP TABLE IF EXISTS tmp_scn_rawmaterial;
DROP TABLE IF EXISTS tmp_scn_rawmaterial_all;

DROP TABLE IF EXISTS tmp_scn_so_recentdata;
DROP TABLE IF EXISTS tmp_scn_rawmaterial_indirect;
DROP TABLE IF EXISTS tmp_scn_rawmaterial_indirect_all;

DROP TABLE IF EXISTS tmp_scn_rw_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_rw_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_rw_fmm_prodorder_link;






