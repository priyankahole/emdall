drop table if exists tmp_exch_rem_duplicates_inventoryatlas;
drop table if exists tmp_exch_globalcur_factinventoryatlas;

create table tmp_exch_globalcur_factinventoryatlas as
select ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),'USD') as GlobalCurrency;

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_custom_merck_populate_inventoryatlas_fact';


create table tmp_exch_rem_duplicates_inventoryatlas as
select distinct all_currs.* from 
(
select 
distinct
dc.CurrencyCode pFromCurrency,current_date as pDate, tmp.GlobalCurrency pToCurrency, nullif(1,1) as exchangeRate,
'vw_bi_custom_merck_populate_inventoryatlas_fact' as fact_script_name
FROM dim_part dp 
inner join dim_plant dpl 
	on dp.plant = dpl.PlantCode
inner join dim_company c 
	on dpl.CompanyCode = c.CompanyCode
inner join dim_Currency dc
	on dc.CurrencyCode = c.Currency	,
tmp_exch_globalcur_factinventoryatlas tmp

union all

select
distinct 
dc.CurrencyCode as pFromCurrency,current_date as pDate, tmp.GlobalCurrency pToCurrency, nullif(1,1) as exchangeRate,
'vw_bi_custom_merck_populate_inventoryatlas_fact' as fact_script_name
from Z1PP_INVBW a
inner join dim_plant dpl
	on a.Z1PP_INVBW_PLWRK=dpl.PlantCode
inner join 	dim_company c
	on c.CompanyCode = dpl.CompanyCode
inner join dim_Currency dc
	on dc.CurrencyCode = c.Currency,
tmp_exch_globalcur_factinventoryatlas tmp
) all_currs;




insert into tmp_getExchangeRate1 (pFromCurrency,pDate,pToCurrency,exchangeRate,fact_script_name)
select pFromCurrency,pDate,pToCurrency,exchangeRate,fact_script_name from tmp_exch_rem_duplicates_inventoryatlas;


drop table if exists tmp_exch_rem_duplicates_inventoryatlas;
drop table if exists tmp_exch_globalcur_factinventoryatlas;



