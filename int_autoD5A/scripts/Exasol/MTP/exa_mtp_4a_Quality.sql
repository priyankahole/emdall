/*Change logic for mtp_4a_Quality*/
DELETE
FROM number_fountain m
WHERE m.table_name = 'fact_mtp_4a_Quality';
INSERT INTO number_fountain SELECT 'fact_mtp_4a_Quality', 1;

DROP TABLE IF EXISTS mtp_4a_Quality;

CREATE TABLE mtp_4a_Quality AS
SELECT ( SELECT IFNULL( m.max_id, 1 )FROM number_fountain m WHERE m.table_name = 'fact_mtp_4a_Quality' ) + ROW_NUMBER() OVER(ORDER BY '' ) AS fact_mtp_4a_Qualityid, 
       prt.primarysite                 AS dd_primarysite, 
       f_il.dd_batchno                 AS dd_BatchNumber, 
       CASE WHEN f_il.dd_inspectionlotno = 'Not Available' 
            THEN 0
            ELSE f_il.dd_inspectionlotno
       END                             AS dd_inspectionlotno, 
       f_il.dd_OrderNo                 AS dd_OrderNo, 
       prt.ProductFamily_Merck         AS dd_GPFcode, 
       prt.prodfamilydescription_merck AS dd_GPFdesc,  
       prt.PartNumber                  AS dd_Material, 
       prt.PartDescription             AS dd_MaterialDescription, 
       prt.ItemSubType_Merck           AS dd_ItemSubType, 
       ud.Description                  AS dd_Code, 
       CASE TO_CHAR( dmd.DateValue, 'dd mon yyyy' )
            WHEN '01 Jan 0001' 
            THEN ' '
            ELSE TO_CHAR( dmd.DateValue, 'dd mon yyyy' )
       END                             AS dd_UsageDecisionMadeDate, 
       ROUND( SUM( CASE WHEN ( dim_dateidusagedecisionmade = 1 OR dim_lastgrdate_q_id = 1 ) 
                        THEN NULL 
                        ELSE CASE WHEN dlgrq.isapublicholiday = 0 
                                  THEN dmd.BusinessDaysSeqNo - dlgrq.BusinessDaysSeqNo - ct_diffpublicholiday 
                                  ELSE dmd.BusinessDaysSeqNo - nxtgrq.BusinessDaysSeqNo - ct_diffpublicholiday 
                             END 
                   END ), 0 )                  AS ct_ActualQualityReleaseDuration_hrs, 
       ROUND( SUM( prt.GRProcessingTime ), 2 ) AS ct_GRprocessingtime, 
       ROUND( SUM( CASE WHEN f_il.dim_dateidusagedecisionmade = 1 
                        THEN 0.0000 
                        ELSE ( CASE WHEN CAST(( CASE WHEN dlgrq.isapublicholiday = 0 
                                                     THEN dmd.BusinessDaysSeqNo - dlgrq.BusinessDaysSeqNo - ct_diffpublicholiday 
                                                     ELSE dmd.BusinessDaysSeqNo - nxtgrq.BusinessDaysSeqNo - ct_diffpublicholiday 
                                                END ) AS DECIMAL ) <= CAST(( CASE WHEN ilo.inspectionlotorigincode = '01' 
                                                                                  THEN ct_grprocessingtime_purchaseord 
                                                                                  WHEN ilo.inspectionlotorigincode = '04' 
                                                                                  THEN ct_grprocessingtime_prodord 
                                                                                  ELSE prt.GRProcessingTime 
                                                                             END ) AS DECIMAL ) 
                                   THEN 1.0000 
                                   ELSE 0.0000 
                               END ) END ), 2 ) AS ct_ReleasedonTime, 
       ROUND( SUM( f_il.ct_actuallotqty ), 3 )  AS ct_ActualLotQuantity, 
       CAST( prt.dosage AS INT )                AS ct_dosage, 
       prt.dosageuom                            AS dd_dosageuom, 
       prt.contents                             AS ct_contents, 
       prt.contentsuom                          AS dd_contentsuom, 
       prt.concentration                        AS dd_concentration, 
       prt.concentrationuom                     AS dd_concentrationuom, 
       CASE TO_CHAR( dmd.DateValue, 'dd mon yyyy' )
            WHEN '01 Jan 0001' 
            THEN ' '
            ELSE TO_CHAR( dmd.DateValue, 'MON YYYY' ) /*keep the format consistent*/
       END                                AS dd_MonthofYear, 
       MF1c.dd_Resource                   AS dd_Resource, 
       MF1c.dd_CapacityID                 AS dd_CapacityID, 
       MF1c.dd_Planner                    AS dd_Planner, 
       AVG( MF1c.ct_OperationHours_BUoM ) AS ct_OperationHours_BUoM, 
       ROUND( SUM( MF1c.ct_OperationHours_BUoM * f_il.ct_actuallotqty ), 3 ) AS ct_GRQuantityOperationhrs, 
       AVG( MF1c.ct_LaborHours_BUoM )                                        AS ct_LaborHours_BUoM, 
       ROUND( SUM( MF1c.ct_LaborHours_BUoM * f_il.ct_actuallotqty ), 3 )     AS ct_GRQuantityLaborhrs,
       f_il.dim_partid      AS Dim_Partid,
       f_il.dim_plantid     AS Dim_Plantid,
       CAST ( 1 AS BIGINT ) AS Dim_dateid
  FROM fact_inspectionlot                AS f_il
 INNER JOIN Dim_Date                     AS dmd 
    ON f_il.dim_dateidusagedecisionmade = dmd.Dim_Dateid
 INNER JOIN Dim_Part                     AS prt 
    ON f_il.Dim_Partid = prt.Dim_Partid
 INNER JOIN dim_codetext                 AS ud 
    ON f_il.Dim_InspUsageDecisionId = ud.dim_codetextid
 INNER JOIN dim_inspectionlotorigin      AS ilo 
    ON f_il.dim_inspectionlotoriginid = ilo.dim_inspectionlotoriginid
 INNER JOIN dim_inspectionlotstatus      AS ils 
    ON f_il.Dim_InspectionLotStatusId = ils.dim_inspectionlotstatusid
 INNER JOIN dim_inspectioncatalogcodes   AS ficg 
    ON f_il.dim_inspectioncatalogcodesid = ficg.dim_inspectioncatalogcodesid
 INNER JOIN Dim_Date                     AS dlgrq 
    ON f_il.dim_lastgrdate_q_id = dlgrq.Dim_Dateid
 INNER JOIN Dim_Date                     AS nxtgrq 
    ON f_il.dim_nextbusday_lastgrdate_q_id = nxtgrq.Dim_Dateid
 INNER JOIN fact_mtp_1c_master_aggregate AS MF1c 
    ON prt.PartNumber = MF1c.dd_MaterialNumber
 WHERE dmd.DateValue BETWEEN ( NOW() - INTERVAL '1' YEAR ) AND NOW()
   AND ilo.inspectionlotOriginCode = '04'
   AND LOWER( CASE WHEN ils.LotCancelled = 'X' 
                   THEN 'Yes' 
                   ELSE 'No' 
              END ) = LOWER( 'No' )
 GROUP BY prt.primarysite, 
          f_il.dd_batchno, 
          CASE WHEN f_il.dd_inspectionlotno = 'Not Available' 
               THEN 0
			   ELSE f_il.dd_inspectionlotno
		  END, 
		  f_il.dd_OrderNo, 
		  prt.ProductFamily_Merck, 
		  prt.prodfamilydescription_merck, 
		  prt.PartNumber, 
		  prt.PartDescription, 
		  prt.ItemSubType_Merck, 
		  ud.Description, 
		  TO_CHAR( dmd.DateValue, 'dd mon yyyy' ), 
		  dmd.DateValue, 
		  CAST( prt.dosage AS INT ), 
		  prt.dosageuom, 
		  prt.contents, 
		  prt.contentsuom, 
		  prt.concentration, 
		  prt.concentrationuom, 
		  MF1c.dd_Resource, 
		  MF1c.dd_CapacityID, 
		  MF1c.dd_Planner,
		  f_il.Dim_Partid,
		  f_il.Dim_Plantid;
		 
DROP TABLE IF EXISTS fact_mtp_4a_Quality;
RENAME mtp_4a_Quality TO fact_mtp_4a_Quality;