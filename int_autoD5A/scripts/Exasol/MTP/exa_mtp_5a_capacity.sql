/* ################################################################################################################## */
/* */
/*   Script         : exa_mtp_5a_capacity */
/*   Author         : Madalina */
/*   Created On     : 14 Sep 2018 */
/*   Overview       : MTP Skill - Planned/Process Order Supporting Table 5A  */
/*                Source tables: Planned Order, Production Order and mtp_1c_master_aggregate */ 
/*                                                                                                                    */
/* ################################################################################################################## */

/* drop table if exists mtp_5a_planOrder_productionOrder
create table mtp_5a_planOrder_productionOrder as
select  f_po.dd_PlanOrderNo as PlannedOrder,
	case when attr.FirmingIndicator= 'X' then 'Yes' else 'No' end FirmingIndicator,
	prt.PartNumber MaterialNumber,
	prt.PartDescription MaterialDescription,
	fd.DateValue FinishDate,
	ROUND(SUM(f_po.ct_QtyTotal),4) TotalQuantity,
	prt.UnitOfMeasure BaseUoM,
	sum(prt.assemblyscrap) Assemblyscrap_perc,
	to_char(fd.DateValue,'MON YYYY') MonthOfYear,
	mtp_1c.Resource,
	mtp_1c.CapacityID,
	mtp_1c.Planner,
	ROUND(SUM(f_po.ct_QtyTotal),4) * (1 / (1 + sum(prt.assemblyscrap) / 100)) NetLotSize,
	sum(mtp_1c.StandardLotSize) StandardLotSize,
	sum(mtp_1c.OperationHours_StandardLotSize) as OperationHours_StandardLotSize,
	sum(mtp_1c.LaborHours_StandardLotSize)  as LaborHours_StandardLotSize,
	cast(0 as decimal(18,4))  OperationHoursPerNetLotSize,
	cast(0 as decimal(18,4))  LaborHoursPerNetLotSize,
	sum(mtp_1c.TTI_BUoM) TTI_BUoM,
	cast(0 as decimal(18,4)) as TTIperNetLotSize,
	cast(0 as decimal(18,4)) as TTIOperationDay,
	cast(0 as decimal(18,4)) as TTILaborDay
from fact_planorder as f_po 
	inner join Dim_Date as fd on f_po.Dim_dateidFinish = fd.Dim_Dateid  
	inner join Dim_Part as prt on f_po.Dim_Partid = prt.Dim_Partid  
	inner join Dim_Plant as pl on f_po.Dim_Plantid = pl.Dim_Plantid  
	inner join dim_planordermisc as attr on f_po.Dim_PlanOrderMiscid = attr.dim_planordermiscid  
	inner join mtp_1c_master_aggregate as mtp_1c on mtp_1c.materialnumber = prt.PartNumber
group by  f_po.dd_PlanOrderNo,
	case when attr.FirmingIndicator= 'X' then 'Yes' else 'No' end,
	prt.PartNumber, 
	prt.PartDescription,
	fd.DateValue, 
	prt.UnitOfMeasure, 
	to_char(fd.DateValue,'MON YYYY'),
	mtp_1c.Resource,
	mtp_1c.CapacityID,
	mtp_1c.Planner

union all

select f_prord.dd_ordernumber dd_ordernumber,
		pl.PlantCode as FirmingIndicator,
		ip.PartNumber as MaterialNumber,
		ip.PartDescription as MaterialDescription,
		bfd.DateValue as FinishDate,
		ROUND(SUM(f_prord.ct_TotalOrderQty),4) TotalQuantity,
		ip.UnitOfMeasure BaseUoM,
		sum(ip.assemblyscrap) Assemblyscrap_perc,
		to_char(bfd.DateValue,'MON YYYY') MonthOfYear,
		mtp_1c.Resource,
		mtp_1c.CapacityID,
		mtp_1c.Planner,
		ROUND(SUM(f_prord.ct_TotalOrderQty),4) * (1 / (1 + sum(ip.assemblyscrap) / 100)) NetLotSize,
		sum(mtp_1c.StandardLotSize) StandardLotSize,
		sum(mtp_1c.OperationHours_StandardLotSize) as OperationHours_StandardLotSize,
		sum(mtp_1c.LaborHours_StandardLotSize)  as LaborHours_StandardLotSize,
		cast(0 as decimal(18,4))  OperationHoursPerNetLotSize,
		cast(0 as decimal(18,4))  LaborHoursPerNetLotSize,
		sum(mtp_1c.TTI_BUoM) TTI_BUoM,
		cast(0 as decimal(18,4)) as TTIperNetLotSize,
		cast(0 as decimal(18,4)) as TTIOperationDay,
		cast(0 as decimal(18,4)) as TTILaborDay
from fact_productionorder AS f_prord 
		inner join Dim_Plant AS pl ON f_prord.Dim_Plantid = pl.Dim_Plantid
		inner join Dim_Part AS ip ON f_prord.Dim_PartidItem = ip.Dim_Partid
		inner join Dim_Date AS bfd ON f_prord.Dim_DateidBasicFinish = bfd.Dim_Dateid 
		inner join mtp_1c_master_aggregate as mtp_1c on mtp_1c.materialnumber = ip.PartNumber
group by f_prord.dd_ordernumber,
		pl.PlantCode,
		ip.PartNumber,
		ip.PartDescription,
		bfd.DateValue,
		ip.UnitOfMeasure,
		to_char(bfd.DateValue,'MON YYYY'),
		mtp_1c.Resource,
		mtp_1c.CapacityID,
		mtp_1c.Planner

update mtp_5a_planOrder_productionOrder
set OperationHoursPerNetLotSize = case when StandardLotSize <> 0 then OperationHours_StandardLotSize * NetLotSize / StandardLotSize else null end,
	LaborHoursPerNetLotSize =  case when StandardLotSize <> 0 then LaborHours_StandardLotSize * NetLotSize / StandardLotSize else null end

update  mtp_5a_planOrder_productionOrder
set TTIperNetLotSize = TTI_BUoM * NetLotSize,
	TTIOperationDay = TTI_BUoM *  OperationHoursPerNetLotSize,
	TTILaborDay = TTI_BUoM * LaborHoursPerNetLotSize
*/

/*Change logic for 5a - use Capacity as source*/
delete from number_fountain m where m.table_name = 'fact_mtp_5a_Capacity';

insert into number_fountain
select 'fact_mtp_5a_Capacity', 1; /* re-create fact, generate IDs starting with 1 */

drop table if exists mtp_5a_Capacity;
create table mtp_5a_Capacity as
select 
	(select ifnull(m.max_id,1) from number_fountain m where m.table_name = 'fact_mtp_5a_Capacity') + row_number() over(order by '') as fact_mtp_5a_Capacityid,
	pl.PlantCode dd_PlantCode,
	wck.productionsupplyarea dd_productionsupplyarea,
	cc.description dd_DescriptionofTheCapacityCategory,
	f_cap.dd_capacityid as dd_CapacityID,
	wck.workcenter dd_Workcenter,
	wck.ktext_description dd_WorkCenterDescription,
	f_cap.dd_keyfortlgroup dd_KeyforTaskListGroup,
	f_cap.dd_opnumber dd_OperationActivityNumber,
	dp.PartNumber dd_Materialnumber,
	dp.PartDescription dd_MaterialDescription,
	dd_plannedorder as dd_Plannedorder,        
	dd_productionorder as dd_ProductionOrder,
	ROUND(AVG(ct_operation),3) ct_OperationQuantity,
	dp.UnitOfMeasure dd_BaseUoM,
	ROUND(AVG(ct_schedcaprequirprocesing),16) ct_schedcaprequirprocessing,
	cruomq.UOM  dd_UOM,
	dim_dateidscheduledfinish as dim_dateidscheduledfinish, 
	to_char(dsh.DateValue, 'MON YYYY') dd_MonthofYear,
	mtp_1c.dd_Resource as dd_Resource,
	cast (1 as bigint) as dim_plantid
from fact_capacity AS f_cap 
	inner join Dim_Date AS dsh ON f_cap.dim_dateidscheduledfinish = dsh.Dim_Dateid   
	inner join Dim_Part AS dp ON f_cap.dim_partid = dp.Dim_Partid    
	inner join Dim_Plant AS pl ON f_cap.dim_plantid = pl.Dim_Plantid  
	inner join dim_workcenter AS wck ON f_cap.dim_workcenterid = wck.dim_workcenterid  
	inner join dim_capacitycategory AS cc ON f_cap.dim_capcategoryid = cc.dim_capacitycategoryid  
	inner join dim_unitofmeasure AS cruomq ON f_cap.dim_caprequnitofmeasureid = cruomq.dim_unitofmeasureid  
	inner join fact_mtp_1c_master_aggregate mtp_1c on f_cap.dd_capacityid = mtp_1c.dd_CapacityID and dp.PartNumber = mtp_1c.dd_MaterialNumber
where f_cap.dd_longtermplanscen = 0 
	and dsh.DateValue  BETWEEN now() AND now() + 729
	and cruomq.uom <> 'Not Set'
group by 
	pl.PlantCode, 
	wck.productionsupplyarea,
	f_cap.dd_capacityid,
	cc.description,
	wck.workcenter,
	wck.ktext_description,
	f_cap.dd_keyfortlgroup,
	f_cap.dd_opnumber,
	dp.PartNumber,
	dp.PartDescription, 
	f_cap.dd_plannedorder,
	f_cap.dd_productionorder,
	dp.UnitOfMeasure,
	cruomq.UOM,
	dim_dateidscheduledfinish, 
	to_char(dsh.DateValue, 'MON YYYY'),
	mtp_1c.dd_Resource;

update mtp_5a_Capacity f
set f.dim_plantid = pl.dim_plantid
from mtp_5a_Capacity f 
inner join dim_plant pl on f.dd_plantcode = pl.plantcode;

drop table if exists fact_mtp_5a_Capacity;
rename mtp_5a_Capacity to fact_mtp_5a_Capacity;


