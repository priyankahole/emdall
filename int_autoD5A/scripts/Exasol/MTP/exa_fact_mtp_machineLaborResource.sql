/* ################################################################################################################## */
/* */
/*   Script         : exa_fact_mtp_machineLaborResource */
/*   Author         : Madalina */
/*   Created On     : 18 Sep 2018 */
/*   Overview       : MTP Skill - Fact table */
/*   Changes		: Use fact tables instead of temp tables*/
/* ################################################################################################################## */

/* Machine resource temp table */
drop table if exists tmp_fact_machineresource;
create table tmp_fact_machineresource as
select mtp_1c.dd_PlantCode as dd_Site,
	mtp_1c.dd_Resource as dd_Resource,
	mtp_3d.dd_MonthOfYear,
	mtp_3d.dd_datevalue,
    cast(0 as decimal (18,4)) as ct_ActualProduction_OperationHours,    /*mtp_3a.OperationHours_GRQuantity*/
	cast(0 as decimal(18,4)) as ct_ProductionLoad_OperationHours,
	cast(0 as decimal(18,4)) as ct_ProductionLoad,   /*mtp_5a.OperationHoursperNetLotSize*/
    /* cast(0 as decimal(18,4)) as OldProductionLoad,*/  /*mtp_5a.OperationHoursperNetLotSize for current month*/
	cast(0 as decimal(18,4)) as ct_NonSAPLoad_w_TTI,  /*mtp_5b.OperationHours for GenerateTTI = 'Yes'*/
    cast(0 as decimal(18,4)) as ct_NonSAPLoad_w_o_TTI,   /*mtp_5b.OperationHours for GenerateTTI = 'No'*/
	avg(mtp_3d.ct_NetEfficientCapacity) as ct_NetEfficientCapacity_OperationHours, /* Krutika : Take Avg instead of Sum*/
	avg(mtp_3d.ct_NetAvailableCapacity) as ct_NetAvailableCapacity_OperationHours,/* Krutika  :Take Avg instead of Sum*/
	cast(0 as decimal(18,4)) as ct_Utilization,
	cast(0 as decimal(18,4)) as ct_RejectionRate,  /*uses mtp_4a.GRQuantityOperationhrs*/
	'Processing unit' as dd_resource_type,
                       cast(0 as decimal(18,0)) as ct_avghoursperday /* Krutika: Add dynamic hours per day using the work center fact  */
from fact_mtp_1c_master_aggregate mtp_1c	
	inner join fact_mtp_3d_machinelaborresource mtp_3d on mtp_3d.dd_Resource = mtp_1c.dd_resource  /* multiplies all the resources by the number of months. */
	--inner join mtp_machine_time_interval mtp_time on to_char(mtp_time.datevalue,'MON YYYY') = mtp_3d.dd_MonthOfYear 
where mtp_3d.dd_DescriptionofTheCapacityCategory = 'Processing unit'
  and trunc(mtp_3d.dd_datevalue, 'MM') between  (trunc(current_date, 'MM') + interval '-12' MONTH) and  (trunc(current_date, 'MM') + interval '24' MONTH) /*only get the 37 months data*/
group by mtp_1c.dd_PlantCode ,
	mtp_1c.dd_Resource,
	mtp_3d.dd_MonthOfYear,
	mtp_3d.dd_datevalue;

                                         

/*Krutika: Add average hours per day */
merge into tmp_fact_machineresource f using
(
    select dd_resource, avghoursperday
	from  tmp_wc_getavghrsperday
) tmp
 on f.dd_resource = tmp.dd_resource
when matched then update
set f.ct_avghoursperday = tmp.avghoursperday;

                                         
/*3a measures */
merge into tmp_fact_machineresource f using
(
    select mtp_3a.dd_Resource, mtp_3a.dd_MonthOfYear,
	      sum(mtp_3a.ct_OperationHours_GRQuantity) ct_ActualProduction_OperationHours
	from fact_mtp_3a_Production_order mtp_3a
 	group by mtp_3a.dd_Resource, mtp_3a.dd_MonthOfYear
) mtp_3a
 on f.dd_resource = mtp_3a.dd_resource
and f.dd_MonthOfYear = mtp_3a.dd_MonthOfYear
when matched then update
set f.ct_ActualProduction_OperationHours = mtp_3a.ct_ActualProduction_OperationHours;

/*4a measures*/
merge into tmp_fact_machineresource f using
(
    select mtp_4a.dd_Resource,  upper(mtp_4a.dd_MonthOfYear) as dd_MonthOfYear,
	      sum(mtp_4a.ct_GRQuantityOperationhrs) as ct_GRQuantityOperationhrs
	from fact_mtp_4a_Quality mtp_4a
  	where mtp_4a.dd_code = 'Rejected'
 	group by mtp_4a.dd_Resource, mtp_4a.dd_MonthOfYear
) mtp_4a
 on f.dd_resource = mtp_4a.dd_resource
and f.dd_MonthOfYear = mtp_4a.dd_MonthOfYear
when matched then update
set f.ct_RejectionRate = case when ifnull(ct_ActualProduction_OperationHours, 0) <> 0 
							then mtp_4a.ct_GRQuantityOperationhrs / ct_ActualProduction_OperationHours * 100
							else 0 end;


/*5a measures*/
merge into tmp_fact_machineresource f using
(
    select mtp_5a.dd_Resource, mtp_5a.dd_MonthOfYear,
     sum(CT_SCHEDCAPREQUIRPROCESSING) as ct_ProductionLoad /*Krutika: Use CT_SCHEDCAPREQUIRPROCESSING instead of ct_OperationQuantity*/
	    --  sum(ct_OperationQuantity) as ct_ProductionLoad ,
	     /* sum(case when mtp_5a.MonthOfYear = to_char(  (trunc(current_date, 'MM') ) , 'MON YYYY') 
			then OperationHoursperNetLotSize
	        else 0 end) as OldProductionLoad */
	from fact_mtp_5a_capacity mtp_5a
 	group by mtp_5a.dd_Resource, mtp_5a.dd_MonthOfYear
) mtp_5a
 on f.dd_resource = mtp_5a.dd_resource
and f.dd_MonthOfYear = mtp_5a.dd_MonthOfYear
when matched then update
set ct_ProductionLoad = mtp_5a.ct_ProductionLoad;

/*5b measures*/
/* merge into tmp_fact_machineresource f using
(
    select mtp_5b.dd_Resource, mtp_5b.dd_MonthOfYear,
	    sum( case when mtp_5b.dd_GenerateTTI = 'Yes' then ct_OperationHours else 0 end) as ct_NonSAPLoad_w_TTI,
		sum( case when mtp_5b.dd_GenerateTTI = 'No' then ct_OperationHours else 0 end) as ct_NonSAPLoad_w_o_TTI
	from fact_mtp_5b_NonSAPProductionLoad mtp_5b
 	group by mtp_5b.dd_Resource, mtp_5b.dd_MonthOfYear
) mtp_5b
 on f.dd_resource = mtp_5b.dd_resource
and f.dd_MonthOfYear = mtp_5b.dd_MonthOfYear
when matched then update
set f.ct_NonSAPLoad_w_TTI = mtp_5b.ct_NonSAPLoad_w_TTI,
	f.ct_NonSAPLoad_w_o_TTI = mtp_5b.ct_NonSAPLoad_w_o_TTI */

update tmp_fact_machineresource
set ct_ProductionLoad_OperationHours = ct_ProductionLoad + ct_NonSAPLoad_w_TTI + ct_NonSAPLoad_w_o_TTI;

update tmp_fact_machineresource
set ct_Utilization = case when  ct_NetEfficientCapacity_OperationHours <> 0 then ct_ProductionLoad_OperationHours / ct_NetEfficientCapacity_OperationHours else 0 end;

/* Labor resource temp table */
drop table if exists tmp_fact_LaborResource;
create table tmp_fact_LaborResource as
select mtp_1c.dd_PlantCode as dd_Site,
	mtp_1c.dd_Resource as dd_Resource,
	mtp_3d.dd_MonthOfYear,
	mtp_3d.dd_datevalue,
    cast(0 as decimal (18,4)) as ct_ActualProduction_OperationHours,   /*mtp_3a.LaborHours_GRQuantity*/
	cast(0 as decimal(18,4)) as ct_ProductionLoad_OperationHours,
	cast(0 as decimal(18,4)) as ct_ProductionLoad,    /*mtp_5a.LaborHoursperNetLotSize*/
    /*cast(0 as decimal(18,4)) as OldProductionLoad, */ /*mtp_5a.LaborHoursperNetLotSize for current month*/
	cast(0 as decimal(18,4)) as ct_NonSAPLoad_w_TTI,     /*mtp_5b.LaborHours for GenerateTTI = 'Yes' */
    cast(0 as decimal(18,4)) as ct_NonSAPLoad_w_o_TTI,   /*mtp_5b.LaborHours for GenerateTTI = 'No' */
	sum(mtp_3d.ct_NetEfficientCapacity) as ct_NetEfficientCapacity_OperationHours,
	sum(mtp_3d.ct_NetAvailableCapacity) as ct_NetAvailableCapacity_OperationHours,
	cast(0 as decimal(18,4)) as ct_Utilization,
	cast(0 as decimal(18,4)) as ct_RejectionRate,  /*based on mtp_4a.GRQuantityLaborhrs*/
	'Person' as dd_resource_type,                
    cast(0 as decimal(18,0)) as ct_avghoursperday /* Krutika: Add dynamic hours per day using the work center fact  */
from fact_mtp_1c_master_aggregate mtp_1c	
	inner join fact_mtp_3d_machinelaborresource mtp_3d on mtp_3d.dd_Resource = mtp_1c.dd_resource  /* multiplies all the resources by the number of months. Already has embedded the  machine type - Person */
	inner join mtp_machine_time_interval mtp_time on to_char(mtp_time.datevalue,'MON YYYY') = mtp_3d.dd_MonthOfYear /*only get the 37 months*/
where mtp_3d.dd_DescriptionofTheCapacityCategory = 'Person'
group by mtp_1c.dd_PlantCode,
	mtp_1c.dd_Resource,
	mtp_3d.dd_MonthOfYear,
	mtp_3d.dd_datevalue;

                      

/*Krutika: Add average hours per day */
merge into tmp_fact_LaborResource f using
(
    select dd_resource, avghoursperday
	from  tmp_wc_getavghrsperday
) tmp
 on f.dd_resource = tmp.dd_resource
when matched then update
set f.ct_avghoursperday = tmp.avghoursperday;

                      
/*3a measures */
merge into tmp_fact_LaborResource f using
(
    select mtp_3a.dd_Resource, mtp_3a.dd_MonthOfYear,
	      sum(mtp_3a.ct_LaborHours_GRQuantity) ct_ActualProduction_OperationHours
	from fact_mtp_3a_Production_order mtp_3a
 	group by mtp_3a.dd_Resource, mtp_3a.dd_MonthOfYear
) mtp_3a
 on f.dd_resource = mtp_3a.dd_resource
and f.dd_MonthOfYear = mtp_3a.dd_MonthOfYear
when matched then update
set ct_ActualProduction_OperationHours = mtp_3a.ct_ActualProduction_OperationHours;

/*4a measures*/
merge into tmp_fact_LaborResource f using
(
    select mtp_4a.dd_Resource, upper(mtp_4a.dd_MonthOfYear) as dd_MonthOfYear,
	      sum(mtp_4a.ct_GRQuantityLaborhrs) as ct_GRQuantityLaborhrs
	from fact_mtp_4a_Quality mtp_4a
    where mtp_4a.dd_code = 'Rejected'
 	group by mtp_4a.dd_Resource, mtp_4a.dd_MonthOfYear
) mtp_4a
 on f.dd_resource = mtp_4a.dd_resource
and f.dd_MonthOfYear = mtp_4a.dd_MonthOfYear
when matched then update
set f.ct_RejectionRate = case when ifnull(ct_ActualProduction_OperationHours,0) <> 0 
							then mtp_4a.ct_GRQuantityLaborhrs / ct_ActualProduction_OperationHours * 100
							else 0 end;

/*5a measures */
merge into tmp_fact_LaborResource f using
(
    select mtp_5a.dd_Resource, mtp_5a.dd_MonthOfYear,
	      sum(ct_OperationQuantity) as ct_ProductionLoad /* ,
	      sum(case when mtp_5a.MonthOfYear = to_char(  (trunc(current_date, 'MM') ) , 'MON YYYY') 
			then LaborHoursperNetLotSize
	        else 0 end) as OldProductionLoad*/
	from fact_mtp_5a_capacity mtp_5a
 	group by mtp_5a.dd_Resource, mtp_5a.dd_MonthOfYear
) mtp_5a
 on f.dd_resource = mtp_5a.dd_resource
and f.dd_MonthOfYear = mtp_5a.dd_MonthOfYear
when matched then update
set f.ct_ProductionLoad = mtp_5a.ct_ProductionLoad;

/* merge into tmp_fact_LaborResource f using
(
    select mtp_5b.dd_Resource, mtp_5b.dd_MonthOfYear,
	    sum( case when mtp_5b.dd_GenerateTTI = 'Yes' then ct_OperationHours else 0 end) as ct_NonSAPLoad_w_TTI,
		sum( case when mtp_5b.dd_GenerateTTI = 'No' then ct_OperationHours else 0 end) as ct_NonSAPLoad_w_o_TTI
	from fact_mtp_5b_NonSAPProductionLoad mtp_5b
 	group by mtp_5b.dd_Resource, mtp_5b.dd_MonthOfYear
) mtp_5b
 on f.dd_resource = mtp_5b.dd_resource
and f.dd_MonthOfYear = mtp_5b.dd_MonthOfYear
when matched then update
set f.ct_NonSAPLoad_w_TTI = mtp_5b.ct_NonSAPLoad_w_TTI,
	f.ct_NonSAPLoad_w_o_TTI = mtp_5b.ct_NonSAPLoad_w_o_TTI */

update tmp_fact_LaborResource
set ct_ProductionLoad_OperationHours = ct_ProductionLoad + ct_NonSAPLoad_w_TTI + ct_NonSAPLoad_w_o_TTI;

update tmp_fact_LaborResource
set ct_Utilization = case when  ct_NetEfficientCapacity_OperationHours <> 0 then ct_ProductionLoad_OperationHours / ct_NetEfficientCapacity_OperationHours else null end;

/* re-create the fact */
--truncate table fact_mtp_machineLaborResource
delete from fact_mtp_machineLaborResource 
	  where  dd_insert_date = /*trunc(current_date,'MM')*/
                      case when extract (DAY from current_date) between 1 and 4 
 									then trunc(current_date,'MM') - Interval '1' MONTH 
								else trunc(current_date,'MM')
								end    /*delete for current month when current date after 5 and from previous month when current_date between [1,4]*/
                      ;

delete from number_fountain m where m.table_name = 'fact_mtp_machineLaborResource';

insert into number_fountain
select 'fact_mtp_machineLaborResource',
ifnull(max(f.fact_mtp_machineLaborResourceid),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mtp_machineLaborResource f;

--alter table fact_mtp_machineLaborResource add column ct_productionloaddays decimal(18,4) default 0 not null enable

insert into fact_mtp_machineLaborResource
(
	fact_mtp_machineLaborResourceid,
	dd_site,
	dd_resource,
	dd_monthofyear,
	dd_datevalue,
	ct_actualproduction_operationhours,
    ct_actualproduction_operationdays,
	ct_productionload_operationhours,
	ct_productionload_operationdays,
	ct_productionload,
    ct_productionloaddays, /*Krutika: Add Produciton Load in Days*/
	/*ct_oldproductionload,*/
	ct_nonsapload_w_tti,
	ct_nonsapload_w_o_tti,
	ct_netefficientcapacity_operationhours,
	ct_netefficientcapacity_operationdays,
	ct_netavailablecapacity_operationhours,
	ct_netavailablecapacity_operationdays,
	ct_utilization,
	ct_rejectionrate,
	dd_resource_type,
	dw_insert_date,
	dd_insert_date
)
select (select ifnull(m.max_id,1) from number_fountain m where m.table_name = 'fact_mtp_machineLaborResource') + row_number() over(order by '') as fact_mtp_machineLaborResourceid,
        machine_and_labor.* 
		from (
			select 
				dd_site,
				dd_resource,
				dd_monthofyear,
				dd_datevalue,
				ct_actualproduction_operationhours,
          		/*Madalina - handling division by 0*/
                case when ct_avghoursperday <> 0 then ct_actualproduction_operationhours/ct_avghoursperday else 0 end actualproduction_operationdays,/*Krutika: replace 8 with the hours per day from WC fact*/
				ct_productionload_operationhours,
                case when ct_avghoursperday <> 0 then ct_productionload_operationhours/ct_avghoursperday else 0 end productionload_operationdays,/*Krutika: replace 8 with the hours per day from WC fact*/
				ct_productionload,
				case when ct_avghoursperday <> 0 then ct_productionload/ct_avghoursperday else 0 end productionload_days,  /*Krutika: Add Produciton Load in Days*/
				/*oldproductionload,*/
				ct_nonsapload_w_tti,
				ct_nonsapload_w_o_tti,
				ct_netefficientcapacity_operationhours,
                case when ct_avghoursperday <> 0 then ct_netefficientcapacity_operationhours/ct_avghoursperday else 0 end netefficientcapacity_operationdays,/*Krutika: replace 8 with the hours per day from WC fact*/
				ct_netavailablecapacity_operationhours,
                case when ct_avghoursperday <> 0 then ct_netavailablecapacity_operationhours/ct_avghoursperday else 0 end netavailablecapacity_operationdays,/*Krutika: replace 8 with the hours per day from WC fact*/
				ct_utilization,
				ct_rejectionrate,
				dd_resource_type,
				current_timestamp as dw_insert_date,
				/*trunc(current_date,'MM') */
          		case when extract (DAY from current_date) between 1 and 4 
 									then trunc(current_date,'MM') - Interval '1' MONTH 
								else trunc(current_date,'MM')
								end as dd_insert_date
			from tmp_fact_machineresource
			
			union all
			
			select 
				dd_site,
				dd_resource,
				dd_monthofyear,
				dd_datevalue,
				ct_actualproduction_operationhours,
                case when ct_avghoursperday <> 0 then ct_actualproduction_operationhours/ct_avghoursperday else 0 end actualproduction_operationdays,/*Krutika: replace 8 with the hours per day from WC fact*/
				ct_productionload_operationhours,
                case when ct_avghoursperday <> 0 then ct_productionload_operationhours/ct_avghoursperday else 0 end productionload_operationdays,/*Krutika: replace 8 with the hours per day from WC fact*/
				ct_productionload,
				case when ct_avghoursperday <> 0 then ct_productionload/ct_avghoursperday else 0 end productionload_days,  /*Krutika: Add Produciton Load in Days*//*Krutika: replace 8 with the hours per day from WC fact*/
				/*oldproductionload,*/
				ct_nonsapload_w_tti,
				ct_nonsapload_w_o_tti,
				ct_netefficientcapacity_operationhours,
                case when ct_avghoursperday <> 0 then ct_netefficientcapacity_operationhours/ct_avghoursperday else 0 end netefficientcapacity_operationdays,/*Krutika: replace 8 with the hours per day from WC fact*/
				ct_netavailablecapacity_operationhours,
                case when ct_avghoursperday <> 0 then ct_netavailablecapacity_operationhours/ct_avghoursperday else 0 end netavailablecapacity_operationdays,/*Krutika: replace 8 with the hours per day from WC fact*/
				ct_utilization,
				ct_rejectionrate,
				dd_resource_type,
				current_timestamp as dw_insert_date,
				/*trunc(current_date,'MM')*/
          		case when extract (DAY from current_date) between 1 and 4 
 									then trunc(current_date,'MM') - Interval '1' MONTH 
								else trunc(current_date,'MM')
								end as dd_insert_date
			from tmp_fact_laborresource
			) machine_and_labor;

update fact_mtp_machineLaborResource f
set f.dim_plantid = pl.dim_plantid
from  fact_mtp_machineLaborResource f 
	inner join dim_plant pl on pl.plantcode = f.dd_site;

merge into fact_mtp_machineLaborResource f using
( select distinct fact_mtp_machineLaborResourceid, d.dim_dateid
	from  fact_mtp_machineLaborResource f 
	inner join dim_plant p on f.dd_site = p.plantcode
	inner join dim_date d on d.datevalue = f.dd_datevalue and d.plantcode_factory = f.dd_site and p.companycode = d.companycode) upd
on f.fact_mtp_machineLaborResourceid = upd.fact_mtp_machineLaborResourceid 
when matched then update
set f.dim_dateid = upd.dim_dateid;
