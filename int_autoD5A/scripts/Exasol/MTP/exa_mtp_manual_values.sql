/* ################################################################################################################## */
/* */
/*   Script         : exa_mtp_manual_values */
/*   Author         : Octavian S */
/*   Created On     : 15 Oct 2018 */
/*   Overview       : MTP Skill - Manual Values */
/*                    Source table: fact_mtp_machineLaborResource */ 
/* ################################################################################################################## */

INSERT INTO fact_mtp_manualvalues(
  dd_resource,
  dd_site,
  dd_monthyear,
  ct_manualvalueA,
  ct_manualvalueB,
  ct_manualvalueC,
  dd_insert_date,
  dd_datevalue)
SELECT m.DD_RESOURCE, 
       m.DD_SITE,
       m.DD_MONTHOFYEAR, 
       0 AS ct_manualvalueA, 
       0 AS ct_manualvalueB, 
       0 AS ct_manualvalueC,
       m.DD_INSERT_DATE,
       m.DD_DATEVALUE
 FROM FACT_MTP_MACHINELABORRESOURCE m
WHERE m.DD_DATEVALUE >= trunc(current_date, 'MM')
  AND NOT EXISTS (SELECT 1 
                    FROM fact_mtp_manualvalues f
                   WHERE f.DD_RESOURCE=m.DD_RESOURCE
                     AND f.DD_SITE=m.DD_SITE
                     AND f.DD_INSERT_DATE = trunc(current_date, 'MM'))
ORDER BY m.DD_RESOURCE,m.DD_DATEVALUE;