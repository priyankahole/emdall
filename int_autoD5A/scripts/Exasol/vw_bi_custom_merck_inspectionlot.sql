
/* Madalina 29 Dec 2016 - plantcode_factory additional condition - BI-5085*/ 

MERGE INTO fact_inspectionlot fact
   USING (SELECT f_il.fact_inspectionlotid, dd.dim_dateid AS dim_ScheduledStartDateOrderid
         from CAUFV cf,dim_date dd,dim_plant dp, fact_inspectionlot f_il
         where
f_il.dd_orderno = ifnull(cf.CAUFV_AUFNR,'Not Set')
and f_il.dim_plantid = dp.dim_plantid
and dp.CompanyCode = dd.CompanyCode
and dp.plantcode = dd.plantcode_factory  /* plantcode_factory additional condition- BI-5085 */
and ifnull(cf.CAUFV_GSTRS,'0001-01-01') = dd.DateValue
and ifnull(f_il.dim_ScheduledStartDateOrderid,-1) <> dd.dim_dateid ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_ScheduledStartDateOrderid  = src.dim_ScheduledStartDateOrderid
 WHERE ifnull(fact.dim_ScheduledStartDateOrderid ,-1) <> ifnull(src.dim_ScheduledStartDateOrderid ,-2);


MERGE INTO fact_inspectionlot fact
   USING (SELECT f_il.fact_inspectionlotid, dd.dim_dateid AS dim_ScheduledEndDateOrderid
         from CAUFV cf,dim_date dd,dim_plant dp, fact_inspectionlot f_il
         where
f_il.dd_orderno=ifnull(cf.CAUFV_AUFNR,'Not Set')
and f_il.dim_plantid=dp.dim_plantid
and dp.CompanyCode=dd.CompanyCode
and dp.plantcode = dd.plantcode_factory  /* plantcode_factory additional condition- BI-5085 */
and ifnull(cf.CAUFV_GLTRS,'0001-01-01') = dd.DateValue
and ifnull(f_il.dim_ScheduledEndDateOrderid,-1) <> dd.dim_dateid ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_ScheduledEndDateOrderid  = src.dim_ScheduledEndDateOrderid
 WHERE ifnull(fact.dim_ScheduledEndDateOrderid ,-1) <> ifnull(src.dim_ScheduledEndDateOrderid ,-2);

MERGE INTO fact_inspectionlot fact
   USING (SELECT f_il.fact_inspectionlotid, ifnull(qm.QMAT_MPDAU,0) AS ct_AverageInspectionDuration
         from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
         where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and ifnull(f_il.ct_AverageInspectionDuration,-1) <> ifnull(qm.QMAT_MPDAU,0) ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_AverageInspectionDuration  = src.ct_AverageInspectionDuration
 WHERE ifnull(fact.ct_AverageInspectionDuration ,-1) <> ifnull(src.ct_AverageInspectionDuration ,-2);

/* 10.03.2015 new QMAT cols */
MERGE INTO fact_inspectionlot fact
   USING (SELECT f_il.fact_inspectionlotid, ifnull(qm.QMAT_INSMK,'Not Set') AS dd_posttoinspstock
         from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
         where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_posttoinspstock <> ifnull(qm.QMAT_INSMK,'Not Set') ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_posttoinspstock  = src.dd_posttoinspstock
 WHERE ifnull(fact.dd_posttoinspstock ,-1) <> ifnull(src.dd_posttoinspstock ,-2);

MERGE INTO fact_inspectionlot fact
   USING (SELECT f_il.fact_inspectionlotid, ifnull(qm.QMAT_DYN,'Not Set') AS dd_skipsallowed
         from QMAT qm, dim_part dpa, dim_plant dp, dim_inspectiontype dit, fact_inspectionlot f_il
         where
f_il.dim_partid = dpa.dim_partid
and f_il.dim_plantid = dp.dim_plantid
and f_il.dim_inspectiontypeid = dit.dim_inspectiontypeid
and dpa.Partnumber = qm.QMAT_MATNR
and dp.PlantCode = qm.QMAT_WERKS
and dit.inspectiontypecode = qm.QMAT_ART
and f_il.dd_skipsallowed <> ifnull(qm.QMAT_DYN,'Not Set') ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_skipsallowed  = src.dd_skipsallowed
 WHERE ifnull(fact.dd_skipsallowed ,-1) <> ifnull(src.dd_skipsallowed ,-2);
/* end 10.03.2015 new QMAT cols */


drop table if exists tmp_mch1_001;

create table tmp_mch1_001 as
select mch1_charg,mch1_matnr,mch1_lwedt,
row_number() over (partition by mch1_charg,mch1_matnr order by ifnull(mch1_lwedt,'0001-01-01') desc) as rnk
from mch1;

delete from tmp_mch1_001 where rnk>1;

MERGE INTO fact_inspectionlot fact
   USING (SELECT f_il.fact_inspectionlotid, dd.dim_dateid
     AS dim_LastGRdateid
         from tmp_mch1_001 mc,dim_date dd,dim_plant dp,dim_part dpa
, fact_inspectionlot f_il
         where
f_il.dim_partid = dpa.dim_partid
and dpa.partnumber = ifnull(mc.mch1_matnr,'Not Set')
and f_il.dd_batchno = ifnull(mc.mch1_charg,'Not Set')
and f_il.dim_plantid = dp.dim_plantid
and dp.CompanyCode = dd.CompanyCode
and dd.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
and dd.datevalue = ifnull(mc.mch1_lwedt,'0001-01-01')
and ifnull(f_il.dim_LastGRdateid,-1) <> dd.dim_dateid ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_LastGRdateid  = src.dim_LastGRdateid
 WHERE ifnull(fact.dim_LastGRdateid ,-1) <> ifnull(src.dim_LastGRdateid ,-2);

drop table if exists tmp_QAMB_001;

create table tmp_QAMB_001 as
select
QAMB_PRUEFLOS,QAMB_CPUDT,row_number() over (partition by QAMB_PRUEFLOS order by QAMB_ZAEHLER desc) as rwn
from QAMB
where QAMB_TYP=1;

delete from tmp_QAMB_001 where rwn<>1;


MERGE INTO fact_inspectionlot fact
   USING (SELECT f_il.fact_inspectionlotid, dd.dim_dateid AS dim_LastGRdate_Q_id
         from tmp_QAMB_001 qb,dim_date dd,dim_plant dp, fact_inspectionlot f_il
         where
f_il.dd_inspectionlotno = ifnull(cast (qb.QAMB_PRUEFLOS as varchar(20)),'Not Set')
and f_il.dim_plantid = dp.dim_plantid
and dp.CompanyCode = dd.CompanyCode
and dd.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
and dd.datevalue = ifnull(qb.QAMB_CPUDT,'0001-01-01')
and f_il.dim_LastGRdate_Q_id <> dd.dim_dateid ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_LastGRdate_Q_id  = src.dim_LastGRdate_Q_id
 WHERE ifnull(fact.dim_LastGRdate_Q_id ,-1) <> ifnull(src.dim_LastGRdate_Q_id ,-2);

DROP TABLE IF EXISTS tmp_dim_date_update_workingdays_inspectionlot;
CREATE TABLE tmp_dim_date_update_workingdays_inspectionlot
AS
SELECT dt.*,ROW_NUMBER() OVER (ORDER BY companycode,plantcode_factory,datevalue ) sr_no
FROM dim_date dt
WHERE isapublicholiday = 0 AND isaweekendday = 0
ORDER BY  companycode,plantcode_factory,datevalue ;    /* plantcode_factory additional condition- BI-5085 */

/* Create another table with sr_no, but with all rows from dim_date */
DROP TABLE IF EXISTS tmp_dim_date_update_workingdays_inspectionlot_allddrows;
CREATE TABLE tmp_dim_date_update_workingdays_inspectionlot_allddrows
AS
SELECT dt.*,CAST(-1 as BIGINT) sr_no
FROM dim_date dt;


/* Update sr_no for non-holidays */
UPDATE tmp_dim_date_update_workingdays_inspectionlot_allddrows a
SET a.sr_no = b.sr_no
FROM tmp_dim_date_update_workingdays_inspectionlot b, tmp_dim_date_update_workingdays_inspectionlot_allddrows a
WHERE a.dim_dateid = b.dim_dateid;

/* Madalina 2 Nov 2016 - BI-4489 */
/* For a holiday or weekend, get the next minimum date for a working day */
/*DROP TABLE IF EXISTS tmp_dim_date_updsr_no1
CREATE TABLE tmp_dim_date_updsr_no1
AS
SELECT a.companycode,a.datevalue, min(b.datevalue) next_min_datevalue
FROM tmp_dim_date_update_workingdays_inspectionlot_allddrows a, tmp_dim_date_update_workingdays_inspectionlot b
WHERE a.companycode = b.companycode
AND a.sr_no = -1
AND a.datevalue < b.datevalue
GROUP BY a.companycode,a.datevalue*/

drop table if exists tmp_dim_date_srno_holiday;
create table tmp_dim_date_srno_holiday as
select * from  tmp_dim_date_update_workingdays_inspectionlot_allddrows a
where a.sr_no = -1;

DROP TABLE IF EXISTS tmp_dim_date_updsr_no1;
CREATE TABLE tmp_dim_date_updsr_no1
AS
SELECT a.companycode,a.datevalue, a.plantcode_factory, min(b.datevalue) next_min_datevalue
FROM  tmp_dim_date_srno_holiday a, tmp_dim_date_update_workingdays_inspectionlot b
WHERE a.companycode = b.companycode
and a.plantcode_factory = b.plantcode_factory /* plantcode_factory additional condition- BI-5085 */
AND a.datevalue < b.datevalue
GROUP BY a.companycode,a.plantcode_factory,a.datevalue;
/* End BI-4489 */

/* Update sr no of the previous max working date */
DROP TABLE IF EXISTS tmp_dim_date_updsr_no;
CREATE TABLE tmp_dim_date_updsr_no
AS
SELECT a.companycode,a.datevalue,  a.plantcode_factory, b.sr_no prev_max_sr_no
FROM tmp_dim_date_updsr_no1 a, tmp_dim_date_update_workingdays_inspectionlot b
WHERE a.companycode = b.companycode
and a.plantcode_factory = b.plantcode_factory /* plantcode_factory additional condition- BI-5085 */
AND a.next_min_datevalue = b.datevalue;

UPDATE tmp_dim_date_update_workingdays_inspectionlot_allddrows a
SET a.sr_no = b.prev_max_sr_no
FROM tmp_dim_date_updsr_no b, tmp_dim_date_update_workingdays_inspectionlot_allddrows a
WHERE a.sr_no = -1    /* This corresponds to isapublicholiday = 1 OR isaweekendday = 1*/
AND a.companycode = b.companycode
and a.plantcode_factory = b.plantcode_factory /* plantcode_factory additional condition- BI-5085 */
AND a.datevalue = b.datevalue;

/* Instead of direct update do this for performance */
/* Using _allddrows here as dim_LastGRdate_Q_id ( as per the current issue ) can also be on a weekend */
/*
DROP TABLE IF EXISTS tmp_upd_fact_inspectionlot
CREATE TABLE tmp_upd_fact_inspectionlot
AS
SELECT f_il.*,cast((CASE WHEN ilo.inspectionlotorigincode = '01' THEN f_il.ct_grprocessingtime_purchaseord 
WHEN ilo.inspectionlotorigincode = '04' THEN f_il.ct_grprocessingtime_prodord 
ELSE dpa.GRProcessingTime END) as bigint) grprocessingtime,convert(varchar(10),dp.CompanyCode) CompanyCode,dd1.sr_no,
cast((dd1.sr_no + (CASE WHEN ilo.inspectionlotorigincode = '01' THEN f_il.ct_grprocessingtime_purchaseord 
WHEN ilo.inspectionlotorigincode = '04' THEN f_il.ct_grprocessingtime_prodord 
ELSE dpa.GRProcessingTime END)) as bigint) new_srno, plantcode_factory /* plantcode_factory additional condition- BI-5085 */
/*
FROM fact_inspectionlot f_il, dim_part dpa,tmp_dim_date_update_workingdays_inspectionlot_allddrows dd1, dim_plant dp,
	dim_inspectionlotorigin ilo
where f_il.dim_partid = dpa.dim_partid
and f_il.dim_LastGRdate_Q_id = dd1.dim_dateid
and f_il.dim_plantid=dp.dim_plantid
and f_il.dim_inspectionlotoriginid = ilo.dim_inspectionlotoriginid
AND ifnull(f_il.dim_LastGRdate_Q_id,-1) <> 1

/* Update on this tmp table based on companycode + sr_no, which is unique */
/*
UPDATE tmp_upd_fact_inspectionlot f_il
SET f_il.dim_TargetReleaseDateid = dd2.dim_dateid
FROM  tmp_dim_date_update_workingdays_inspectionlot dd2, tmp_upd_fact_inspectionlot f_il /* Not using _allddrows here as we only want workdays to be updated from TargetReleaseDate */
/*WHERE convert(varchar(10),f_il.CompanyCode)= convert(varchar(10),dd2.CompanyCode)
and f_il.plantcode_factory = dd2.plantcode_factory /* plantcode_factory additional condition- BI-5085 */
/*AND f_il.new_srno = dd2.sr_no
and ifnull(f_il.dim_TargetReleaseDateid,-1) <> dd2.dim_dateid
/*
UPDATE fact_inspectionlot f_il
SET f_il.dim_TargetReleaseDateid = f_il2.dim_TargetReleaseDateid
FROM tmp_upd_fact_inspectionlot f_il2,  fact_inspectionlot f_il
WHERE f_il.fact_inspectionlotid = f_il2.fact_inspectionlotid
AND f_il.dim_TargetReleaseDateid <> f_il2.dim_TargetReleaseDateid
*/

/*Alin APP-7621 target */
/*8th of June 2018 - logic change ticket APP - 9766*/
drop table if exists tmp_new_seq;
create table tmp_new_seq as 
select fact_inspectionlotid, f.dim_lastgrdate_q_id, dt.datevalue,
/*dd_inspectionlotno, d.batchnumber, dt.plantcode, dt.companycode, 
d.lastgoodreceipt, grprocessingtime, dt.businessdaysseqno old_seq*/
  dt.plantcode_factory, dt.companycode, 
case when ilo.INSPECTIONLOTORIGINCODE='01' then ct_grprocessingtime_purchaseord + dt.businessdaysseqno
when ilo.INSPECTIONLOTORIGINCODE='04' then ct_grprocessingtime_prodord + dt.businessdaysseqno
 else grprocessingtime + dt.businessdaysseqno end as new_seq 
from
fact_inspectionlot f 
inner join dim_batch d
on f.dim_batchid = d.dim_batchid
inner join dim_part dp
on f.dim_partid = dp.dim_partid
inner join dim_plant dpl
on f.dim_plantid = dpl.dim_plantid
inner join dim_date dt
on f.dim_lastgrdate_q_id = dt.dim_dateid
inner join dim_inspectionlotorigin ilo on ilo.dim_inspectionlotoriginid=f.dim_inspectionlotoriginid
WHERE --dt.datevalue = '0001-01-01'
--and dim_targetreleasedateid = 1
--and 
dpl.plantcode not in (select distinct plantcode from newiteminstockbrinit);


merge into fact_inspectionlot f
using(
select distinct sq.fact_inspectionlotid, sq.datevalue,
case when sq.datevalue = '0001-01-01' then 1
else
min(dt.dim_dateid) end as dim_dateidtargetreleasedatenew
/*there are multiple dates that match the same businessdaysseqno because public holidays have the same businessdaysseqno
as the previous working day*/
from tmp_new_seq sq 
inner join dim_date dt
on dt.plantcode_factory = sq.plantcode_factory
and dt.companycode = sq.companycode
and dt.businessdaysseqno = sq.new_seq
--where fact_inspectionlotid = '1086013'
group by sq.fact_inspectionlotid, sq.datevalue
) t
on t.fact_inspectionlotid = f.fact_inspectionlotid
when matched then update set
f.dim_targetreleasedateid = t.dim_dateidtargetreleasedatenew;

drop table if exists tmp_new_seq;

/* Madalina - 29 Dec 2016 - No need to update dim_TargetReleaseDateid from TFACD_THOC - BI-5085 */
/* Madalina- Use Factory Calendar for Target Release Date- BI-4319 */
/* do not run the following select without rebuilding dim_TargetReleaseDateid first */
/* drop table if exists tmp_upd_fact_holidays
create table tmp_upd_fact_holidays as
select distinct fact_inspectionlotid, fi.dim_plantid, count(d.datevalue) as ctfactdays
from fact_inspectionlot fi, dim_date dd, dim_date grd, dim_plant dp, TFACD_THOC TF, dim_date d
where fi.dim_TargetReleaseDateid = dd.dim_dateid
and fi.dim_LastGRdate_Q_id = grd.dim_dateid
and dp.dim_plantid = fi.dim_plantid
and dp.plantcode = dp.plantcode
and dp.companycode = dp.companycode
and TF.TFACD_IDENT = dp.FactoryCalendarKey
AND dp.companycode = d.companycode
and dp.plantcode = d.plantcode_factory 
AND TF.THOC_DATUM = d.datevalue
AND d.IsAWeekendday = 0
AND d.isapublicholiday = 0 
and d.datevalue between grd.datevalue and dd.datevalue
group by fact_inspectionlotid, fi.dim_plantid

update fact_inspectionlot fi
set fi.dim_TargetReleaseDateid = final_.dim_dateid,
	dw_update_date=current_date
from fact_inspectionlot fi, dim_date final_, dim_date actual, tmp_upd_fact_holidays tmph
where final_.businessdaysseqno = actual.businessdaysseqno + ctfactdays
and tmph.fact_inspectionlotid = fi.fact_inspectionlotid
and tmph.dim_plantid = fi.dim_plantid 
and actual.dim_dateid = dim_TargetReleaseDateid
and final_.companycode = actual.companycode
and final_.plantcode_factory = actual.plantcode_factory 
and final_.IsAWeekendday = 0 
and final_.isapublicholiday = 0
and fi.dim_TargetReleaseDateid <> final_.dim_dateid
/* End BI-4319 */

/* Madalina - 29 Dec 2016 - businessdaysseqno now contains all the not working days, ct_diffpublicholiday is not needed - BI-5085 */
/*Alin 09 Dec 2016 - Use Factory Calendar for Actual Quality Release Duration (days)- BI-4983*/
/* drop table if exists tmp_upd_fact_holidays_lrot
create table tmp_upd_fact_holidays_lrot as
select distinct fact_inspectionlotid, fi.dim_plantid, count(d.datevalue) as ctfactdays
from fact_inspectionlot fi, dim_date dd, dim_date grd, dim_plant dp, TFACD_THOC TF, dim_date d
where fi.dim_dateidusagedecisionmade = dd.dim_dateid
and fi.dim_LastGRdate_Q_id = grd.dim_dateid
and dp.dim_plantid = fi.dim_plantid
and dp.plantcode = dp.plantcode
and dp.companycode = dp.companycode
and TF.TFACD_IDENT = dp.FactoryCalendarKey
AND dp.companycode = d.companycode
AND TF.THOC_DATUM = d.datevalue
AND d.IsAWeekendday = 0
AND d.isapublicholiday = 0 
and d.datevalue between grd.datevalue and dd.datevalue
group by fact_inspectionlotid, fi.dim_plantid


update fact_inspectionlot fi
set fi.ct_diffpublicholiday = tmph.ctfactdays
from fact_inspectionlot fi, dim_date final_, dim_date actual, tmp_upd_fact_holidays_lrot tmph
where final_.businessdaysseqno = actual.businessdaysseqno + ctfactdays
and tmph.fact_inspectionlotid = fi.fact_inspectionlotid
and tmph.dim_plantid = fi.dim_plantid 
and actual.dim_dateid = dim_TargetReleaseDateid
and final_.companycode = actual.companycode
and final_.IsAWeekendday = 0 
and final_.isapublicholiday = 0
and fi.dim_TargetReleaseDateid <> final_.dim_dateid */

/*end BI-4983*/

/* ct_diffpublicholiday is used in UI formulas for different measures */
update fact_inspectionlot fi
set fi.ct_diffpublicholiday = 0;

/* END BI-5085 */
/*
update fact_inspectionlot f_il
set f_il.dim_TargetReleaseDateid = 1
where f_il.dim_LastGRdate_Q_id = 1
*/
/* Madalina 28 Apr 2017 - do not reset the Target Release Date for the dummy BRINIT rows-  APP-6076 */
/*and dd_lrotcommentmanual_flag <> 1

/* Yogini 30 Nov 2016 BI-4590 */

DROP TABLE IF EXISTS tmp_upd_fact_inspectionlot1;
CREATE TABLE tmp_upd_fact_inspectionlot1
AS
SELECT f_il.*,cast((CASE WHEN ilo.inspectionlotorigincode = '01' THEN f_il.ct_grprocessingtime_purchaseord 
WHEN ilo.inspectionlotorigincode = '04' THEN f_il.ct_grprocessingtime_prodord 
ELSE dpa.GRProcessingTime END) as bigint) grprocessingtime,convert(varchar(10),dp.CompanyCode) CompanyCode,dd1.sr_no,
cast((dd1.sr_no + (CASE WHEN ilo.inspectionlotorigincode = '01' THEN f_il.ct_grprocessingtime_purchaseord 
WHEN ilo.inspectionlotorigincode = '04' THEN f_il.ct_grprocessingtime_prodord 
ELSE dpa.GRProcessingTime END)) as bigint) new_srno,
plantcode_factory /* plantcode_factory additional condition- BI-5085 */
FROM fact_inspectionlot f_il, dim_part dpa, tmp_dim_date_update_workingdays_inspectionlot_allddrows dd1, dim_plant dp,
	dim_inspectionlotorigin ilo
where f_il.dim_partid = dpa.dim_partid
and f_il.dim_LastGRdate_Q_id = dd1.dim_dateid
and f_il.dim_plantid=dp.dim_plantid
and f_il.dim_inspectionlotoriginid = ilo.dim_inspectionlotoriginid
AND ifnull(f_il.dim_LastGRdate_Q_id,-1) <> 1;

/* Update on this tmp table based on companycode + sr_no, which is unique */
UPDATE tmp_upd_fact_inspectionlot1 f_il
SET f_il.dim_TargetReleaseDateOrderid = dd2.dim_dateid
FROM  tmp_dim_date_update_workingdays_inspectionlot dd2, tmp_upd_fact_inspectionlot1 f_il /* Not using _allddrows here as we only want workdays to be updated from TargetReleaseDate */
WHERE convert(varchar(10),f_il.CompanyCode)= convert(varchar(10),dd2.CompanyCode)
and f_il.plantcode_factory = dd2.plantcode_factory /* plantcode_factory additional condition- BI-5085 */
AND f_il.new_srno = dd2.sr_no
and ifnull(f_il.dim_TargetReleaseDateOrderid,-1) <> dd2.dim_dateid;

UPDATE fact_inspectionlot f_il
SET f_il.dim_TargetReleaseDateOrderid = f_il2.dim_TargetReleaseDateOrderid
FROM tmp_upd_fact_inspectionlot1 f_il2,  fact_inspectionlot f_il
WHERE f_il.fact_inspectionlotid = f_il2.fact_inspectionlotid
AND f_il.dim_TargetReleaseDateOrderid <> f_il2.dim_TargetReleaseDateOrderid;

 /* Madalina - 29 Dec 2016 - No need to update dim_TargetReleaseDateid from TFACD_THOC - BI-5085 */
/*
drop table if exists tmp_upd_fact_holidays1
create table tmp_upd_fact_holidays1 as
select distinct fact_inspectionlotid, fi.dim_plantid, count(d.datevalue) as ctfactdays
from fact_inspectionlot fi, dim_date dd, dim_date grd, dim_plant dp, TFACD_THOC TF, dim_date d
where fi.dim_TargetReleaseDateOrderid = dd.dim_dateid
and fi.dim_LastGRdate_Q_id = grd.dim_dateid
and dp.dim_plantid = fi.dim_plantid
and dp.plantcode = dp.plantcode
and dp.companycode = dp.companycode
and TF.TFACD_IDENT = dp.FactoryCalendarKey
AND dp.companycode = d.companycode
AND TF.THOC_DATUM = d.datevalue
AND d.IsAWeekendday = 0
AND d.isapublicholiday = 0 
and d.datevalue between grd.datevalue and dd.datevalue
group by fact_inspectionlotid, fi.dim_plantid

update fact_inspectionlot fi
set fi.dim_TargetReleaseDateOrderid = final_.dim_dateid,
	dw_update_date=current_date
from fact_inspectionlot fi, dim_date final_, dim_date actual, tmp_upd_fact_holidays1 tmph
where final_.businessdaysseqno = actual.businessdaysseqno + ctfactdays
and tmph.fact_inspectionlotid = fi.fact_inspectionlotid
and tmph.dim_plantid = fi.dim_plantid 
and actual.dim_dateid = dim_TargetReleaseDateid
and final_.companycode = actual.companycode
and final_.IsAWeekendday = 0 
and final_.isapublicholiday = 0
and fi.dim_TargetReleaseDateOrderid <> final_.dim_dateid
*/


update fact_inspectionlot f_il
set f_il.dim_TargetReleaseDateOrderid = 1
where f_il.dim_LastGRdate_Q_id = 1;

/* End 30 Nov 2016 BI-4590 */

/* 15.04.2015 add movement type, site/movement type */

 MERGE INTO fact_inspectionlot fact
   USING (SELECT il.fact_inspectionlotid, ifnull(QALS_BWART,'Not Set') AS dd_movementtype
         FROM
       qals q,
       dim_plant dp,
       dim_part dpa
   , fact_inspectionlot il
         WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
     and dd_movementtype <> ifnull(QALS_BWART, 'Not Set') ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.dd_movementtype  = src.dd_movementtype;

UPDATE fact_inspectionlot il
     SET dd_sitemovementtype = ifnull(QALS_WERK||'/'||QALS_BWART, 'Not Set')
  ,il.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       qals q,
       dim_plant dp,
       dim_part dpa,fact_inspectionlot il
WHERE     dp.PlantCode = q.QALS_WERK
       AND dp.RowIsCurrent = 1
       AND dpa.PartNumber = QALS_MATNR
       AND dpa.Plant = dp.PlantCode
       AND dpa.RowIsCurrent = 1
       AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
     and dd_sitemovementtype <> ifnull(QALS_WERK||'/'||QALS_BWART, 'Not Set');

MERGE INTO fact_inspectionlot fact
   USING (SELECT f.fact_inspectionlotid, ifnull(grproctimecaldays_merck,0) AS ct_grproctime
         from dim_part pt, fact_inspectionlot f
         where f.dim_partid = pt.dim_partid
and ct_grproctime  <> ifnull(grproctimecaldays_merck,0) ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_grproctime  = src.ct_grproctime ;


MERGE INTO fact_inspectionlot fact
   USING (SELECT f.fact_inspectionlotid, ifnull(leadtime,0) AS ct_planneddelivtime
         from dim_part pt
, fact_inspectionlot f
         where f.dim_partid = pt.dim_partid
and  ct_planneddelivtime  <> ifnull(leadtime,0) ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_planneddelivtime = src.ct_planneddelivtime;

/* Update next business day - BI-121 */
DROP TABLE IF EXISTS tmp_upd_nextbusday_lastgrdateq;
CREATE TABLE tmp_upd_nextbusday_lastgrdateq
AS
SELECT DISTINCT f_il.dim_LastGRdate_Q_id,d2.dim_dateid  min_next_dim_dateid
FROM fact_inspectionlot f_il,dim_date d, dim_date d2,
( SELECT businessdaysseqno,companycode,plantcode_factory,min(datevalue) min_datevalue from dim_date group by businessdaysseqno,companycode,plantcode_factory) t
WHERE f_il.dim_LastGRdate_Q_id = d.dim_dateid
AND d.companycode = d2.companycode
and d.plantcode_factory = d2.plantcode_factory /* plantcode_factory additional condition- BI-5085 */
AND d2.businessdaysseqno = d.businessdaysseqno + 1
AND d2.businessdaysseqno = t.businessdaysseqno AND d2.companycode = t.companycode 
and d2.plantcode_factory = t.plantcode_factory 
AND d2.datevalue = t.min_datevalue;

MERGE INTO fact_inspectionlot fact
   USING (SELECT f_il.fact_inspectionlotid, t.min_next_dim_dateid AS dim_nextbusday_lastgrdate_q_id
         FROM tmp_upd_nextbusday_lastgrdateq t
, fact_inspectionlot f_il
         WHERE f_il.dim_LastGRdate_Q_id = t.dim_LastGRdate_Q_id
AND f_il.dim_nextbusday_lastgrdate_q_id <> t.min_next_dim_dateid ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.dim_nextbusday_lastgrdate_q_id = src.dim_nextbusday_lastgrdate_q_id;

DROP TABLE IF EXISTS tmp_upd_nextbusday_lastgrdateq;


DROP TABLE IF EXISTS tmp_dim_date_update_workingdays_inspectionlot;
DROP TABLE IF EXISTS tmp_upd_fact_inspectionlot;
DROP TABLE IF EXISTS tmp_dim_date_update_workingdays_inspectionlot_allddrows;
DROP TABLE IF EXISTS tmp_dim_date_updsr_no1;
DROP TABLE IF EXISTS tmp_dim_date_updsr_no;

/*Andrian 20/10/2015 - New Fact Dimension relation based on request BI-1400*/
  MERGE INTO fact_inspectionlot fact
   USING (SELECT il.fact_inspectionlotid, pot.Dim_productionordertypeid AS Dim_productionordertypeid
         FROM AFKO_AFPO_AUFK mhi,
      Dim_productionordertype pot
   , fact_inspectionlot il
         WHERE il.dd_orderno = mhi.AFKO_AUFNR
       AND pot.typecode = AUFK_AUART
       AND pot.RowIsCurrent = 1
       AND il.Dim_productionordertypeid <> pot.Dim_productionordertypeid ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.Dim_productionordertypeid  = src.Dim_productionordertypeid
 WHERE ifnull(fact.Dim_productionordertypeid ,-1) <> ifnull(src.Dim_productionordertypeid ,-2);

/*Andrian 02/12/2015 - new 6 measures based on provided excel and new column first inspection based on BI-1485 */
update fact_inspectionlot
set ct_act_lt_med = 0,
      ct_act_lt_10p = 0,
    ct_act_lt_25p = 0,
    ct_act_lt_75p = 0,
    ct_act_lt_90p = 0,
    ct_n_batches = 0;

MERGE INTO fact_inspectionlot fact
   USING (SELECT a.fact_inspectionlotid, 
		c.act_lt_med AS ct_act_lt_med,
		c.act_lt_10p AS ct_act_lt_10p,
		c.act_lt_25p as ct_act_lt_25p,
		c.act_lt_75p as ct_act_lt_75p,
		c.act_lt_90p as ct_act_lt_90p,
		c.n_batches  as ct_n_batches
         from dim_part b, tmp_quality_analysed c, dim_plant d
, fact_inspectionlot a
         where a.dim_partid = b.dim_partid
and  b.partnumber = lpad(c.partnumber,6,'0')
and a.dim_plantid = d.dim_plantid
and d.plantcode = c.plant_code ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET  fact.ct_act_lt_med = src.ct_act_lt_med,
		fact.ct_act_lt_10p = src.ct_act_lt_10p,
		fact.ct_act_lt_25p = src.ct_act_lt_25p,
		fact.ct_act_lt_75p = src.ct_act_lt_75p,
		fact.ct_act_lt_90p = src.ct_act_lt_90p,
		fact.ct_n_batches  = src.ct_n_batches
 WHERE ifnull(fact.ct_act_lt_med ,-1) <> ifnull(src.ct_act_lt_med ,-2)
 and ifnull(fact.ct_act_lt_10p ,-1) <> ifnull(src.ct_act_lt_10p ,-2)
 and ifnull(fact.ct_act_lt_25p ,-1) <> ifnull(src.ct_act_lt_25p ,-2)
 and ifnull(fact.ct_act_lt_75p ,-1) <> ifnull(src.ct_act_lt_75p ,-2)
 and ifnull(fact.ct_act_lt_90p ,-1) <> ifnull(src.ct_act_lt_90p ,-2)
 and ifnull(fact.ct_n_batches  ,-1) <> ifnull(src.ct_n_batches  ,-2);


drop table if exists tmp_first_inspection01;

create table tmp_first_inspection01 as
select a.fact_inspectionlotid, min(c.datevalue) over (partition by b.PartDescription) as firstinspection
from fact_inspectionlot a, dim_part b, dim_date c
where a.dim_partid = b.dim_partid
and a.dim_dateidinspectionstart = c.dim_dateid;

update fact_inspectionlot
set dim_dateidfirstinspection = 1
where dim_dateidfirstinspection <> 1;

MERGE INTO fact_inspectionlot fact
   USING (SELECT a.fact_inspectionlotid, c.dim_dateid AS dim_dateidfirstinspection
         from tmp_first_inspection01 b,dim_date c, fact_inspectionlot a, dim_plant pl
         where b.firstinspection = c.datevalue
and a.fact_inspectionlotid = b.fact_inspectionlotid
and c.companycode = pl.companycode
and c.plantcode_factory = pl.plantcode /* plantcode_factory additional condition- BI-5085 */
and pl.dim_plantid = a.dim_plantid
and a.dim_dateidfirstinspection <> c.dim_dateid
) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE
   SET fact.dim_dateidfirstinspection  = src.dim_dateidfirstinspection
WHERE ifnull(fact.dim_dateidfirstinspection ,-1) <> ifnull(src.dim_dateidfirstinspection ,-2);

/*Andrian - According to basecamp thread BI-1485*/
drop table if exists tmp_ood_st_price;
create table tmp_ood_st_price as
select  b.partnumber,c.plantcode,sum(ct_future_demandqty * amt_stdpricepmra_merck) as oodstprice from fact_inventoryatlas a,
dim_part b,
dim_plant c
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
group by b.partnumber,c.plantcode ;

drop table if exists tmp_fact_inspectionlot;
create table tmp_fact_inspectionlot as
select fact_inspectionlotid,b.partnumber,c.plantcode,
row_number() over (partition by b.partnumber,c.plantcode order by fact_inspectionlotid)
as RN
  from fact_inspectionlot a,
                        dim_part b,
                        dim_plant c
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid;

update fact_inspectionlot
set ct_oodstprice = 0
where ct_oodstprice <> 0;

MERGE INTO fact_inspectionlot fact
   USING (SELECT a.fact_inspectionlotid, b.oodstprice AS ct_oodstprice
         from tmp_ood_st_price b,tmp_fact_inspectionlot c
, fact_inspectionlot a
         where rn = 1
and a.fact_inspectionlotid = c.fact_inspectionlotid
and b.partnumber = c.partnumber
and b.plantcode = c.plantcode
and a.ct_oodstprice <> b.oodstprice ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_oodstprice  = src.ct_oodstprice
 WHERE ifnull(fact.ct_oodstprice ,-1) <> ifnull(src.ct_oodstprice ,-2);

update fact_inspectionlot
set ct_netpottentionopp = 0
where ct_netpottentionopp <> 0;


MERGE INTO fact_inspectionlot fact
   USING (SELECT a.fact_inspectionlotid, -1*( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4))) /5*7 / 365 * cast(ct_oodstprice as decimal (18,4)) AS ct_netpottentionopp
         from dim_part prt, fact_inspectionlot a
         where a.dim_partid = prt.dim_partid
and ct_netpottentionopp <> -1*( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4))) /5*7 / 365 * cast(ct_oodstprice as decimal (18,4)) ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_netpottentionopp = src.ct_netpottentionopp;

update fact_inspectionlot
set ct_negoppchangeinlt = 0
where ct_negoppchangeinlt <> 0;

MERGE INTO fact_inspectionlot fact
   USING (SELECT a.fact_inspectionlotid, -1*CASE WHEN  ( (( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4)) )) /5*7) / 365 * cast(ct_oodstprice as decimal (18,4)) >0 THEN 0 ELSE ( (( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4)) )) /5*7) / 365 *cast(ct_oodstprice as decimal (18,4)) END AS ct_negoppchangeinlt
         from dim_part prt, fact_inspectionlot a
         where a.dim_partid = prt.dim_partid
and ct_negoppchangeinlt <> -1*CASE WHEN ( (( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as DECIMAL (18,4)) )) /5*7) / 365 * cast(ct_oodstprice as decimal (18,4)) >0 THEN 0 ELSE ( (( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4)) )) /5*7) / 365 *cast(ct_oodstprice as decimal (18,4)) END ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_negoppchangeinlt  = src.ct_negoppchangeinlt;

update fact_inspectionlot
set ct_posoppchangeinlt = 0
where ct_posoppchangeinlt <> 0;

MERGE INTO fact_inspectionlot fact
   USING (SELECT a.fact_inspectionlotid, -1*CASE WHEN ( (( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4)) )) /5*7) / 365 * cast(ct_oodstprice as decimal (18,4)) <=0 THEN 0 ELSE ( (( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4)) )) /5*7) / 365 * cast(ct_oodstprice as decimal (18,4)) END AS ct_posoppchangeinlt
         from dim_part prt
, fact_inspectionlot a
         where a.dim_partid = prt.dim_partid
and ct_posoppchangeinlt <> -1*CASE WHEN ( (( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4)) )) /5*7) / 365 * cast(ct_oodstprice as decimal (18,4)) <=0 THEN 0 ELSE ( (( cast(prt.GRProcessingTime as decimal (18,4)) - cast(ct_act_lt_med as decimal (18,4)) )) /5*7) / 365 * cast(ct_oodstprice as decimal (18,4)) END ) src
   ON fact.fact_inspectionlotid = src.fact_inspectionlotid
 WHEN MATCHED THEN UPDATE
   SET fact.ct_posoppchangeinlt  = src.ct_posoppchangeinlt ;
/* 07 Jul 2016 Georgiana Adding  Profile Fields  according to BI-3414*/

/*Profile Columns*/
/*ZQM00001*/
/* Madalina 14 Sep 2016 - correct conditions - BI-3414*/


/* Madalina 30 Nov 2016 - Re-write the update stmts */
/* drop table if exists tmp_forZQM00001
create table tmp_forZQM00001 as 
SELECT DISTINCT il.fact_inspectionlotid, FIRST_VALUE(ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')) 
						OVER (PARTITION BY TJ30T_STSMA ORDER BY TJ30T_STSMA desc) AS dd_IntervetStdInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00001'

update fact_inspectionlot il 
set il.dd_IntervetStdInsplotP = t.dd_IntervetStdInsplotP
from  fact_inspectionlot il ,tmp_forZQM00001 t
where il.fact_inspectionlotid=t.fact_inspectionlotid
and il.dd_IntervetStdInsplotP <> t.dd_IntervetStdInsplotP
*/

drop table if exists tmp_upd_IntervetStdInsplotP;
create table tmp_upd_IntervetStdInsplotP as
SELECT DISTINCT il.fact_inspectionlotid, ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set') as dd_IntervetStdInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00001';

merge into fact_inspectionlot il 
using (
select distinct fact_inspectionlotid, 
	first_value(dd_IntervetStdInsplotP) over (partition by fact_inspectionlotid order by '') as dd_IntervetStdInsplotP
from tmp_upd_IntervetStdInsplotP) src 
ON il.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE
SET il.dd_IntervetStdInsplotP = src.dd_IntervetStdInsplotP;
/* END 30 Nov 2016 */

/*ZQM00002*/
/* Madalina 30 Nov 2016 - Re-write the update stmts */
/* drop table if exists tmp_forZQM00002
create table tmp_forZQM00002 as 
SELECT DISTINCT il.fact_inspectionlotid, FIRST_VALUE(ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')) 
						OVER (PARTITION BY TJ30T_STSMA ORDER BY TJ30T_STSMA desc) AS dd_IntervetaUDInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00002'

update fact_inspectionlot il 
SET il.dd_IntervetaUDInsplotP = t.dd_IntervetaUDInsplotP
from  fact_inspectionlot il ,tmp_forZQM00002 t
where il.fact_inspectionlotid=t.fact_inspectionlotid
and il.dd_IntervetaUDInsplotP <> t.dd_IntervetaUDInsplotP
*/

drop table if exists tmp_forZQM00002;
create table tmp_forZQM00002 as 
SELECT DISTINCT il.fact_inspectionlotid, ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set') as dd_IntervetaUDInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00002';

merge into fact_inspectionlot il 
using (
select distinct fact_inspectionlotid, 
	first_value(dd_IntervetaUDInsplotP) over (partition by fact_inspectionlotid order by '') as dd_IntervetaUDInsplotP
from tmp_forZQM00002) src 
ON il.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE
SET il.dd_IntervetaUDInsplotP = src.dd_IntervetaUDInsplotP;
/* END 30 Nov 2016 */

/*ZQM00003*/

/* Madalina 30 Nov 2016 - Re-write the update stmts */
/* drop table if exists tmp_forZQM00003
create table tmp_forZQM00003 as
SELECT DISTINCT il.fact_inspectionlotid, FIRST_VALUE(ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')) 
						OVER (PARTITION BY TJ30T_STSMA ORDER BY TJ30T_STSMA desc) AS dd_IntervetSchInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00003'

update fact_inspectionlot il 
SET il.dd_IntervetSchInsplotP = t.dd_IntervetSchInsplotP
from  fact_inspectionlot il ,tmp_forZQM00003 t
where il.fact_inspectionlotid=t.fact_inspectionlotid
and il.dd_IntervetSchInsplotP <> t.dd_IntervetSchInsplotP */

drop table if exists tmp_forZQM00003;
create table tmp_forZQM00003 as
SELECT DISTINCT il.fact_inspectionlotid, ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set') AS dd_IntervetSchInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00003';

merge into fact_inspectionlot il 
using (
select distinct fact_inspectionlotid, 
	first_value(dd_IntervetSchInsplotP) over (partition by fact_inspectionlotid order by '') as dd_IntervetSchInsplotP
from tmp_forZQM00003) src
on il.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE
SET il.dd_IntervetSchInsplotP = src.dd_IntervetSchInsplotP;
/* END 30 Nov 2016 */

/* I0211 Additional column */
UPDATE fact_inspectionlot il
SET  il.dd_inspinstructionprinted = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0211' and j.JEST_INACT is NULL
AND  il.dd_inspinstructionprinted <> 'X';

/*Date Columns*/

drop table if exists tmp_distinct_jcds;
Create table tmp_distinct_jcds as 
select t.*,row_number() over (partition by JCDS_OBJNR,QALS_STSMA order by JCDS_UDATE desc) as rn from (
select distinct jq.JCDS_OBJNR, jq.JCDS_stat, jq.JCDS_UDATE,qals_stsma, max (jcds_chgnr) as jcds_chgnr
FROM JCDS_QALS jq, JEST_QALS j,tj30t t
WHERE  jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND j.JEST_OBJNR = jq.JCDS_OBJNR
and jq.JCDS_stat=j.jest_stat
and qals_stsma in ('ZQM00001','ZQM00002','ZQM00003')
Group BY jq.JCDS_OBJNR, jq.JCDS_stat, jq.JCDS_UDATE,qals_stsma) t;


/*ZQM00001*/
Update fact_inspectionlot il
SET dim_dateidintervetstd = dt.dim_dateid
FROM  tmp_distinct_jcds jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE dt.DateValue = jq.JCDS_UDATE
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND il.dim_plantid = dp.dim_plantid
AND jq.JCDS_OBJNR = il.dd_objectnumber
AND qals_stsma='ZQM00001'
AND rn = 1
AND dim_dateidintervetstd <> dt.dim_dateid;

/*ZQM00002*/
Update fact_inspectionlot il
SET dim_dateidintervetaud = dt.dim_dateid
FROM  tmp_distinct_jcds jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE dt.DateValue = jq.JCDS_UDATE
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND il.dim_plantid = dp.dim_plantid
AND jq.JCDS_OBJNR = il.dd_objectnumber
AND qals_stsma='ZQM00002'
AND rn = 1
AND dim_dateidintervetaud <> dt.dim_dateid;

/*ZQM00003*/
Update fact_inspectionlot il
SET dim_dateidintervetsch = dt.dim_dateid
FROM  tmp_distinct_jcds jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE dt.DateValue = jq.JCDS_UDATE
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND il.dim_plantid = dp.dim_plantid
AND jq.JCDS_OBJNR = il.dd_objectnumber
AND qals_stsma='ZQM00003'
AND rn = 1
AND dim_dateidintervetsch <> dt.dim_dateid;

/*I0211*/
drop table if exists tmp_distinct_jcds;
Create table tmp_distinct_jcds as
select t.*, row_number() over (partition by JCDS_OBJNR order by JCDS_UDATE desc) as rn from ( 
select distinct jq.JCDS_OBJNR, jq.JCDS_stat, jq.JCDS_UDATE, max (jcds_chgnr) as jcds_chgnr
FROM JCDS_QALS jq, JEST_QALS j
WHERE  
j.JEST_OBJNR = jq.JCDS_OBJNR
and jq.JCDS_stat=j.jest_stat
and jq.JCDS_stat='I0211'
Group BY jq.JCDS_OBJNR, jq.JCDS_stat, jq.JCDS_UDATE) t;

Update fact_inspectionlot il
SET dim_dateidinspintructionprinted = dt.dim_dateid
FROM  tmp_distinct_jcds jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE dt.DateValue = jq.JCDS_UDATE
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND il.dim_plantid = dp.dim_plantid
AND jq.JCDS_OBJNR = il.dd_objectnumber
AND jq.JCDS_stat='I0211'
AND rn = 1
AND dim_dateidinspintructionprinted <> dt.dim_dateid;

/* start Alin Gh - BI-5819 23 mar 2017*/
drop table if exists tmp_avg_inspectiondur;
create table tmp_avg_inspectiondur as
select f_il.fact_inspectionlotid, f_il.dd_inspectionlotno, plantcode, f_il.dim_plantid, 
round(((AVG(f_il.ct_averageinspectionduration) OVER (PARTITION BY F_IL.DD_INSPECTIONLOTNO, PL.PLANTCODE ORDER BY F_IL.DD_INSPECTIONLOTNO, PL.PLANTCODE))/5*7)-14) as avg_inspdur
from fact_inspectionlot f_il, dim_plant pl
where pl.dim_plantid = f_il.dim_plantid;

drop table if exists tmp_avg_inspectiondur2;
create table tmp_avg_inspectiondur2 as
select distinct f_il.fact_inspectionlotid, dt.datevalue, tmp.avg_inspdur, 
case when dt.datevalue = '0001-01-01' then dt.datevalue else add_days(dt.datevalue, tmp.avg_inspdur) end date_avg_inspdur
from fact_inspectionlot f_il, dim_date dt, tmp_avg_inspectiondur tmp
where dt.dim_dateid = f_il.dim_scheduledstartdateorderid
and tmp.fact_inspectionlotid = f_il.fact_inspectionlotid;

Update fact_inspectionlot il
SET dim_dateidinspectiondur = dt.dim_dateid
FROM  tmp_avg_inspectiondur2 jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE jq.fact_inspectionlotid = il.fact_inspectionlotid
AND il.dim_plantid = dp.dim_plantid
and dt.DateValue = jq.date_avg_inspdur
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode /* plantcode_factory additional condition- BI-5085 */
AND dim_dateidinspectiondur <> dt.dim_dateid;

drop table if exists tmp_avg_inspectiondur;
drop table if exists tmp_avg_inspectiondur2;
/* end Alin Gh - BI-5819 23 mar 2017*/

/*Andrei APP-9336 target */
drop table if exists tmp_new_seq_ud_date;
create table tmp_new_seq_ud_date as 
select fact_inspectionlotid, f.dim_dateidinspectionstart, dt.datevalue,
/*dd_inspectionlotno, d.batchnumber, dt.plantcode, dt.companycode, 
d.lastgoodreceipt, grprocessingtime, dt.businessdaysseqno old_seq*/
  dt.plantcode_factory, dt.companycode, grprocessingtime + dt.businessdaysseqno as new_seq
from
fact_inspectionlot f 
inner join dim_batch d
on f.dim_batchid = d.dim_batchid
inner join dim_part dp
on f.dim_partid = dp.dim_partid
inner join dim_plant dpl
on f.dim_plantid = dpl.dim_plantid
inner join dim_date dt
on f.dim_dateidinspectionstart = dt.dim_dateid
WHERE dpl.plantcode not in (select distinct plantcode from newiteminstockbrinit);

merge into fact_inspectionlot f
using(
select distinct sq.fact_inspectionlotid, sq.datevalue,
case when sq.datevalue = '0001-01-01' then 1
else
min(dt.dim_dateid) end as dim_targetuddateid
/*there are multiple dates that match the same businessdaysseqno because public holidays have the same businessdaysseqno
as the previous working day*/
from tmp_new_seq_ud_date sq 
inner join dim_date dt
on dt.plantcode_factory = sq.plantcode_factory
and dt.companycode = sq.companycode
and dt.businessdaysseqno = sq.new_seq
group by sq.fact_inspectionlotid, sq.datevalue
) t
on t.fact_inspectionlotid = f.fact_inspectionlotid
when matched then update set
f.dim_targetuddateid = t.dim_targetuddateid;

drop table if exists tmp_new_seq_ud_date;

drop table if exists tmp_new_seq_qcco_date;
create table tmp_new_seq_qcco_date as 
select fact_inspectionlotid, f.dim_dateidinspectionstart, dt.datevalue,
/*dd_inspectionlotno, d.batchnumber, dt.plantcode, dt.companycode, 
d.lastgoodreceipt, grprocessingtime, dt.businessdaysseqno old_seq*/
  dt.plantcode_factory, dt.companycode, grprocessingtime + (dt.businessdaysseqno - 5) as new_seq
from
fact_inspectionlot f 
inner join dim_batch d
on f.dim_batchid = d.dim_batchid
inner join dim_part dp
on f.dim_partid = dp.dim_partid
inner join dim_plant dpl
on f.dim_plantid = dpl.dim_plantid
inner join dim_date dt
on f.dim_dateidinspectionstart = dt.dim_dateid
WHERE dpl.plantcode not in (select distinct plantcode from newiteminstockbrinit);

merge into fact_inspectionlot f
using(
select distinct sq.fact_inspectionlotid, sq.datevalue,
case when sq.datevalue = '0001-01-01' then 1
else
min(dt.dim_dateid) end as dim_targetqccodateid
/*there are multiple dates that match the same businessdaysseqno because public holidays have the same businessdaysseqno
as the previous working day*/
from tmp_new_seq_qcco_date sq 
inner join dim_date dt
on dt.plantcode_factory = sq.plantcode_factory
and dt.companycode = sq.companycode
and dt.businessdaysseqno = sq.new_seq
group by sq.fact_inspectionlotid, sq.datevalue
) t
on t.fact_inspectionlotid = f.fact_inspectionlotid
when matched then update set
f.dim_targetqccodateid = t.dim_targetqccodateid;

drop table if exists tmp_new_seq_qcco_date;

/*Andrei R APP-9225 */
update fact_inspectionlot f
set f.dd_WarehouseSpecialMovInd = ifnull(m.MLGN_BSSKZ,'Not Set')
from fact_inspectionlot f, MLGN m, QALS q
where f.dd_inspectionlotno = q.QALS_PRUEFLOS
and q.QALS_MATNR = m.MLGN_MATNR
and q.QALS_LGNUM = m.MLGN_LGNUM
and f.dd_WarehouseSpecialMovInd <> ifnull(m.MLGN_BSSKZ,'Not Set');
/* end Andrei R APP-9225 */

drop table if exists tmp_forZQM00001;
drop table if exists tmp_forZQM00002;
drop table if exists tmp_forZQM00003;


 
 /*19 Apr 2018 Georgiana Changes according to APP-8617 creating a default logic based on Plant.Plant Title and Target Release Date.Date
additional filters for this will be: equals with Year to Date and not equal with Current Month*/

/*05 June 2018 Georgiana - revert default logic changes*/
drop table if exists  tmp_for_default_LROT_ATLAS;
create table tmp_for_default_LROT_ATLAS as
select distinct calendarmonthid,planttitle_merck,DD_LROTCOMMENTMANUAL,f.dim_plantid,f.dim_targetreleasedateid from fact_inspectionlot f, dim_date d, dim_plant pl
where dim_targetreleasedateid=dim_dateid
and f.dim_plantid=pl.dim_plantid
and year(datevalue)=year(current_date)
and calendarmonthid = concat( year(current_date), case when length(month(current_date)-1)=1 then concat('0', month(current_date)-1) else month(current_Date)-1 end)
and DD_LROTCOMMENTMANUAL <>'No comments'
order by 2,1;

merge into fact_inspectionlot f
using ( select distinct fact_inspectionlotid,t.DD_LROTCOMMENTMANUAL
from fact_inspectionlot f,tmp_for_default_LROT_ATLAS t,dim_date d1, dim_date d2
where f.dim_plantid = t.dim_plantid
and f.dim_targetreleasedateid = d1.dim_dateid
and t.dim_targetreleasedateid = d2.dim_dateid
and d1.calendarmonthid=d2.calendarmonthid
and d1.plantcode_factory=d2.plantcode_factory
and d1.companycode=d2.companycode
and f.DD_LROTCOMMENTMANUAL='No comments') t
on f.fact_inspectionlotid=t.fact_inspectionlotid
when matched then update set f.DD_LROTCOMMENTMANUAL=t.DD_LROTCOMMENTMANUAL;


/*19 Apr 2018 End of changes*/

drop table if exists  tmp_for_default_LROT_ATLAS;

/* Andrei R APP-10360 */
drop table if exists tmp_new_seq_trd_date;
create table tmp_new_seq_trd_date as 
select fact_inspectionlotid, f.dim_lastgrdate_q_id, dt.datevalue,
/*dd_inspectionlotno, d.batchnumber, dt.plantcode, dt.companycode, 
d.lastgoodreceipt, grprocessingtime, dt.businessdaysseqno old_seq*/
  dt.plantcode_factory, dt.companycode,  (dt.businessdaysseqno + f.ct_averageinspectionduration) as new_seq
from
fact_inspectionlot f 
inner join dim_batch d
on f.dim_batchid = d.dim_batchid
inner join dim_part dp
on f.dim_partid = dp.dim_partid
inner join dim_plant dpl
on f.dim_plantid = dpl.dim_plantid
inner join dim_date dt
on f.dim_lastgrdate_q_id = dt.dim_dateid
WHERE dpl.plantcode not in (select distinct plantcode from newiteminstockbrinit);

merge into fact_inspectionlot f
using(
select distinct sq.fact_inspectionlotid, sq.datevalue,
case when sq.datevalue = '0001-01-01' then 1
else
min(dt.dim_dateid) end as dim_targetrdaiddateid
/*there are multiple dates that match the same businessdaysseqno because public holidays have the same businessdaysseqno
as the previous working day*/
from tmp_new_seq_trd_date sq 
inner join dim_date dt
on dt.plantcode_factory = sq.plantcode_factory
and dt.companycode = sq.companycode
and dt.businessdaysseqno = sq.new_seq
group by sq.fact_inspectionlotid, sq.datevalue
) t
on t.fact_inspectionlotid = f.fact_inspectionlotid
when matched then update set
f.dim_targetrdaiddateid = t.dim_targetrdaiddateid;