/* #########################################################################################################################################	*/
/* */
/*   Script         : create_tmp_getLeadTime.sql */
/*   Author         : Lokesh */
/*   Created On     : 22 Jul 2013 */
/*  */
/*  */
/*   Description    : Create table tmp_getLeadTime  */
/* */
/*   Change History */
/*   Date            By        Version            Desc 																											*/
/*   22-Jul-2013     Lokesh      1.0               Create table definition for tmp_getLeadTime. To be used by VW scripts corresponding to getLeadTime mysql function 	*/
/* ##########################################################################################################################################					*/

DROP TABLE IF EXISTS tmp_getLeadTime;

CREATE TABLE tmp_getLeadTime
(
	 pPart                 varchar(256),
	 pPlant                varchar(256),
	 pVendor               varchar(256),
	 DocumentNumber        varchar(256),
	 DocumentLineNumber    int,
	 DocumentType          varchar(10),
	 v_leadTime		  bigint,
	 fact_script_name varchar(60),
	 processed_flag varchar(1),
	 idone			 integer null
);



