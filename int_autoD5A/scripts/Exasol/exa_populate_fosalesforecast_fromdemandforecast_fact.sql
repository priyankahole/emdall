/*31 Mar 2017*/
/*As per Merck request they want that these 3 fields that are coming from Demand forecast Sa tot be updated on a daily basis*/

drop table if exists fact_atlaspharmlogiforecast_merck_for_salesforecast;
create table fact_atlaspharmlogiforecast_merck_for_salesforecast
as select fd.*,
(case when fd.dim_plantid in (91,124) and fd.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
dp.partnumber as dd_partnumber, 
(CASE WHEN dp.plant = 'NL10' THEN 'XX20' ELSE dp.plant END) as dd_plant_upd,
d.MonthEndDate as reporting_monthenddate,(CASE WHEN d.plantcode_factory = 'NL10' THEN 'XX20' ELSE d.plantcode_factory END) as reporting_plantcode_factory_upd,
d.companycode as reporting_companycode
from fact_atlaspharmlogiforecast_merck fd, dim_date d, dim_part dp
where fd.dim_dateidreporting = d.dim_dateid
AND fd.dim_partid = dp.dim_partid
and dd_version = 'DTA';

/* Update customer forecast */

/*09 Feb 2017 Georgiana changes according to BI-5459*/
/* Horizon Forecast Sample*/
drop table if exists tmp_for_upd_customerquantity;
create table tmp_for_upd_customerquantity as
select distinct fact_fosalesforecastid,dim_dateidforecast,f.dd_reportingdate,d.calendarmonthid as focalendarmonthid,dd_country_destination_code,
year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) as repcalendarmonthid,
f.DD_MARKET_GROUPING,f.dd_partnumber,f.dd_plantcode,f.dd_REPORTING_COMPANY_CODE
FROM fact_fosalesforecast f
--inner join tmp_maxrptdate r on TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
inner join dim_date d on f.dim_dateidforecast = d.dim_dateid
where dd_forecastsample='Horizon';

/*This will include only NL10 plant*/
/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
AND f.dd_plantcode='NL10'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='NL10';

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from 
fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;


/*XX20 separate update*/
/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = case when t.PLANT_CODE ='NL10' then 'XX20' end AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and f.dd_plantcode='XX20'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='XX20';

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from 
fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;


/*This update is for USA0 plants*/

/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = 'USA'
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(f.dd_reportingdate,'DD MON YYYY')) || lpad( month(to_date(f.dd_reportingdate,'DD MON YYYY')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code in ( 'USA0')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE /* AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE */
AND  f.DD_MARKET_GROUPING = 'USA' and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='USA0';

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from 
fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;


drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode not in ( 'NL10','USA0');

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;

drop table if exists tmp_for_upd_customerquantity2;

/*Train and Test Forecast sample*/


/*Train and Test Forecast sample*/
/*This will include NL10 plant*/

drop table if exists tmp_for_upd_customerquantity;
create table tmp_for_upd_customerquantity as
select distinct fact_fosalesforecastid,dim_dateidforecast,f.dd_reportingdate,d.calendarmonthid as focalendarmonthid,dd_country_destination_code,
year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) as repcalendarmonthid,
f.DD_MARKET_GROUPING,f.dd_partnumber,f.dd_plantcode,f.dd_REPORTING_COMPANY_CODE
FROM fact_fosalesforecast f
--inner join tmp_maxrptdate r on TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
inner join dim_date d on f.dim_dateidforecast = d.dim_dateid
where dd_forecastsample in ('Train','Test');

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='NL10';

update fact_fosalesforecast f
set f.ct_forecastquantity_customer = t.PRA_QUANTITY 
from tmp_for_upd_customerquantity2 t,fact_fosalesforecast f
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;

/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid  
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and dd_forecastsample in ('Train','Test')
and f.dd_plantcode = 'NL10'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/


/*This update is for USA10 plants*/

/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = 'USA'
AND f.dim_dateidforecast = d.dim_dateid 
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code in ( 'USA0')
and dd_forecastsample in ('Train','Test')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/


drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE /* AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE */
AND  f.DD_MARKET_GROUPING = 'USA' and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode='USA0';

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from 
fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;



/*This update is for the rest of plants*/

/*UPDATE fact_fosalesforecast f
SET f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid 
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code not in ( 'NL10','USA0')
and dd_forecastsample in ('Train','Test')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate*/


/*merge into fact_fosalesforecast f
using (select distinct f.fact_fosalesforecastid,t.PRA_QUANTITY
FROM fact_fosalesforecast f,dim_date d,tmp_maxrptdate r,atlas_forecast_pra_forecasts_merck_DC t
WHERE f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
/*AND f.dd_COUNTRY_DESTINATION_CODE = t.COUNTRY_DESTINATION_CODE 
/*AND f.DD_HEI_CODE = t.HEI_CODE AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING
AND f.dim_dateidforecast = d.dim_dateid 
AND year(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(f.dd_forecastdate),'YYYYMMDD')),2,0) = t.PRA_REPORTING_PERIOD
AND d.calendarmonthid = t.PRA_FORECASTING_PERIOD
and t.plant_code not in ( 'NL10','USA0')
and dd_forecastsample in ('Train','Test')
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate) t
on t.fact_fosalesforecastid=f.fact_fosalesforecastid
when matched then update set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY*/

drop table if exists tmp_for_upd_customerquantity2;
create table tmp_for_upd_customerquantity2 as
select distinct fact_fosalesforecastid, t.PRA_QUANTITY
from tmp_for_upd_customerquantity f,atlas_forecast_pra_forecasts_merck_DC t
where 
f.dd_partnumber = t.PRA_UIN AND f.dd_plantcode = t.PLANT_CODE AND f.dd_REPORTING_COMPANY_CODE = t.REPORTING_COMPANY_CODE
AND f.DD_MARKET_GROUPING = t.MARKET_GROUPING and f.repcalendarmonthid=t.PRA_REPORTING_PERIOD and f.focalendarmonthid=t.pra_forecasting_period
and dd_plantcode not in ( 'NL10','USA0');

update fact_fosalesforecast f
set f.CT_FORECASTQUANTITY_CUSTOMER = t.PRA_QUANTITY
from fact_fosalesforecast f, tmp_for_upd_customerquantity2 t
where f.fact_fosalesforecastid=t.fact_fosalesforecastid;

drop table if exists tmp_for_upd_customerquantity2;


/*09 Feb 2017 End of changes*/

/*Update the manual records*/

merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,dd_reportingdate,dd_forecastdate,
avg(CT_LATESTFORECASTONMONTH) as CT_LATESTFORECASTONMONTH,
dt.calendarmonthid 
from fact_atlaspharmlogiforecast_merck_for_salesforecast f, fact_fosalesforecast fos,dim_date dt,dim_part dp
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(f.country_destination_code_upd,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
/*and dp.partnumber ='005195' and dp.plant='CY20'
and dt.calendarmonthid like '2017%'*/
and dd_version='DTA'
and dd_reportingdate in ('08 Mar 2017','09 Mar 2017')
and dd_forecasttype='Manual'
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 	fos.CT_FORECASTQUANTITY_CUSTOMER=t.CT_LATESTFORECASTONMONTH;

/*Forecast Quantity Customer End*/

/*Sales Amount*/
drop table if exists tmp_for_upd_salesdeliveredmonth;
create table tmp_for_upd_salesdeliveredmonth as
select f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
CASE WHEN 
MAX( (f.dd_reportavailable) ) = 'Yes' 
THEN 
SUM((f.ct_salesmonth1) * (f.ct_nasp)) 
ELSE 
NULL 
END as col1,
sum(f.ct_salesmonth1) as col2,sum(f.ct_nasp) as col3 from fact_atlaspharmlogiforecast_merck f,dim_part dp,dim_Date dt
where f.dim_partid=dp.dim_partid 
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dt.datevalue='2016-02-01'*/
and dim_dateidreporting=dt.dim_dateid
and  dd_version='SFA'
group by f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end);


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
sum(f.col1) as col1,
dt.calendarmonthid 
from tmp_for_upd_salesdeliveredmonth f, fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(f.country_destination_code_upd,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
AND TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dd_version='SFA'
and dt.calendarmonthid like '2016%'
and dd_reportingdate='08 Feb 2017'
and dd_forecastrank='1'
and dd_forecastdate='20160229'*/
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 
fos.ct_salesdeliveredmonth = ifnull(t.col1,0);

/*For Manual Inserts*/

drop table if exists tmp_for_upd_salesdeliveredmonth;
create table tmp_for_upd_salesdeliveredmonth as
select f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end) as country_destination_code_upd,
CASE WHEN 
MAX( (f.dd_reportavailable) ) = 'Yes' 
THEN 
SUM((f.ct_salesmonth1) * (f.ct_nasp)) 
ELSE 
NULL 
END as col1,
sum(f.ct_salesmonth1) as col2,sum(f.ct_nasp) as col3 from fact_atlaspharmlogiforecast_merck f,dim_part dp,dim_Date dt
where f.dim_partid=dp.dim_partid 
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dt.datevalue='2016-02-01'*/
and dim_dateidreporting=dt.dim_dateid
and  dd_version='SFA'
group by f.dim_partid,dt.datevalue,reporting_company_code,dim_dateidreporting,dd_version,(case when (f.dd_version = 'SFA' and f.dim_plantid in (91,124)) or f.dd_version = 'DTA' then country_destination_code else 'Not Set' end);


merge into fact_fosalesforecast fos
using (
select distinct fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
sum(f.col1) as col1,
dt.calendarmonthid 
from tmp_for_upd_salesdeliveredmonth f, fact_fosalesforecast fos,dim_date dt,dim_part dp,tmp_maxrptdate r
where f.dim_partid = dp.dim_partid
and f.dim_partid=fos.dim_partid
AND f.dim_dateidreporting = dt.dim_dateid  
AND year(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')) || lpad( month(to_date(to_char(fos.dd_forecastdate),'YYYYMMDD')),2,0) =dt.calendarmonthid
AND ifnull(f.country_destination_code_upd,'Not Set') = ifnull(fos.dd_COUNTRY_DESTINATION_CODE,'Not Set')
and ifnull(f.reporting_company_code,'Not Set')= ifnull(fos.dd_REPORTING_COMPANY_CODE,'Not Set')
AND TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*and dp.partnumber ='141332' and dp.plant='ZA20'
and dd_version='SFA'
and dt.calendarmonthid like '2016%'
and dd_reportingdate='08 Feb 2017'
and dd_forecastrank='1'
and dd_forecastdate='20160229'*/
--and dd_reportingdate in ('08 Mar 2017','09 Mar 2017')
and dd_forecasttype='Manual'
group by fact_fosalesforecastid,dd_forecasttype,dp.partnumber,dp.plant,dd_forecastrank,fos.dd_reportingdate,dd_forecastdate,
dt.calendarmonthid) t
on t.fact_fosalesforecastid=fos.fact_fosalesforecastid
when matched then update set 
fos.ct_salesdeliveredmonth = ifnull(t.col1,0);

/*Sales Amount End*/

/*MAPE - Customer Forecast*/
DROP TABLE IF EXISTS merck.tmp_custmapes_fosp;
CREATE TABLE merck.tmp_custmapes_fosp
AS
SELECT f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype,
100 * avg(abs((ct_forecastquantity_customer-ct_salesquantity)/ct_salesquantity)) ct_mape_customerfcst
FROM merck.fact_fosalesforecast f,tmp_maxrptdate r
WHERE f.DD_FORECASTSAMPLE = 'Test'
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
GROUP BY f.dd_reportingdate,dd_partnumber,dd_plantcode,f.dd_REPORTING_COMPANY_CODE,f.dd_COUNTRY_DESTINATION_CODE,f.DD_HEI_CODE,f.DD_MARKET_GROUPING,dd_forecasttype;


UPDATE merck.fact_fosalesforecast f
SET f.ct_mape_customerfcst = t.ct_mape_customerfcst
FROM merck.fact_fosalesforecast f,merck.tmp_custmapes_fosp t
WHERE f.dd_reportingdate = t.dd_reportingdate AND f.dd_partnumber = t.dd_partnumber AND f.dd_plantcode = t.dd_plantcode
AND f.dd_REPORTING_COMPANY_CODE = t.dd_REPORTING_COMPANY_CODE AND f.dd_COUNTRY_DESTINATION_CODE = t.dd_COUNTRY_DESTINATION_CODE
AND f.DD_HEI_CODE = t.DD_HEI_CODE AND f.DD_MARKET_GROUPING = t.DD_MARKET_GROUPING
AND f.dd_forecasttype = t.dd_forecasttype;


/*FO MAPE and Current MAPE - BI-5969 - Sales Forecast: custom dimensions MAPE not working */


merge into fact_fosalesforecast f
using(
select distinct f.fact_fosalesforecastid ,f.dim_partid, f.DD_PLANTCODEMERCK, f.DD_FORECASTRANK, f.dd_reportingdate, dt.monthyear,
AVG(CT_MAPE) as FOMAPE, AVG(CT_MAPE_CUSTOMERFCST)  AS CURRENTMAPE FROM  fact_fosalesforecast f, dim_date dt,tmp_maxrptdate r
where  f.dim_dateidforecast = dt.dim_dateid
AND TO_DATE(f.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
group by f.fact_fosalesforecastid,f.dim_partid, f.DD_PLANTCODEMERCK, f.DD_FORECASTRANK, f.dd_reportingdate, dt.monthyear) t 
on t.fact_fosalesforecastid = f.fact_fosalesforecastid
when matched then update set
f.DD_FOMAPE = ifnull(t.FOMAPE,0), f.DD_CURRENTMAPE = ifnull(t.CURRENTMAPE,0);

