
/* Populate Relationship table scn_plant_rawmaterial for supply chain navigation */

DROP TABLE IF EXISTS tmp_scn_plant_rawmaterial;
CREATE TABLE tmp_scn_plant_rawmaterial
as
select DISTINCT dp.dim_plantid dim_plantid_plant, dp.plantcode dd_plantcode_plant, dp.name dd_plantname_plant, dp.city dd_plantcity_plant, dp.country dd_plantcountry_plant,
f.dim_partid dim_partid_rawmaterial, prt.partnumber dd_partnumber_rawmaterial, prt.partdescription dd_partdescription_rawmaterial, prt.plant dd_plant_rawmaterial,

/* Related measures */
sum(ct_ReceivedQty) ct_poqty_plantrawmaterial,
count(distinct f.dim_vendorid) ct_suppliercount_plantrawmaterial,
sum(amt_ItemNetValue * f.amt_exchangerate_gbl) amt_itemnetvalue_plantrawmaterial,
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_plantrawmaterial,
count(DISTINCT f.dd_DocumentNo) ct_pocount_plantrawmaterial,
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END)/CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_plantrawmaterial,
cast(0 as decimal(18,4)) ct_supplierperformancescore_plantrawmaterial

FROM fact_purchase f, dim_date d, dim_part prt, dim_plant dp, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid AND f.dim_plantidordering = dp.dim_plantid AND f.dim_partid = prt.dim_partid AND f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_partid <> 1 AND f.dim_plantidordering <> 1 
AND prt.scnenabled = 1
GROUP BY dp.dim_plantid, dp.plantcode, dp.name, dp.city, dp.country, f.dim_partid, prt.partnumber, prt.partdescription, prt.plant;


truncate TABLE fact_scn_plant_rawmaterial;

delete from number_fountain m where m.table_name = 'fact_scn_plant_rawmaterial';

insert into number_fountain
select  'fact_scn_plant_rawmaterial',
        ifnull(max(f.fact_scn_plant_rawmaterialid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s ),1))
from fact_scn_plant_rawmaterial f
where f.fact_scn_plant_rawmaterialid <> 1;


insert into fact_scn_plant_rawmaterial 
(	fact_scn_plant_rawmaterialid, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	ct_poqty_plantrawmaterial, 
	ct_suppliercount_plantrawmaterial, 
	amt_itemnetvalue_plantrawmaterial, 
	ct_openordercount_plantrawmaterial, 
	ct_pocount_plantrawmaterial, 
	ct_otif_plantrawmaterial, 
	ct_supplierperformancescore_plantrawmaterial
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_plant_rawmaterial') + 
			row_number() over(order by dim_plantid_plant, dim_partid_rawmaterial) 	fact_scn_plant_rawmaterialid, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	ct_poqty_plantrawmaterial, 
	ct_suppliercount_plantrawmaterial, 
	amt_itemnetvalue_plantrawmaterial, 
	ct_openordercount_plantrawmaterial, 
	ct_pocount_plantrawmaterial, 
	ct_otif_plantrawmaterial, 
	ct_supplierperformancescore_plantrawmaterial
from tmp_scn_plant_rawmaterial s;


/* Copy Data Into Mirror Table */

truncate table fact_scn_rawmaterial_plant;

insert into fact_scn_rawmaterial_plant
(	
	fact_scn_rawmaterial_plantid, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	ct_poqty_plantrawmaterial, 
	ct_suppliercount_plantrawmaterial, 
	amt_itemnetvalue_plantrawmaterial, 
	ct_openordercount_plantrawmaterial, 
	ct_pocount_plantrawmaterial, 
	ct_otif_plantrawmaterial, 
	ct_supplierperformancescore_plantrawmaterial
)
select 
	fact_scn_plant_rawmaterialid, 
	dim_plantid_plant, 
	dd_plantcode_plant, 
	dd_plantname_plant, 
	dd_plantcity_plant, 
	dd_plantcountry_plant, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	ct_poqty_plantrawmaterial, 
	ct_suppliercount_plantrawmaterial, 
	amt_itemnetvalue_plantrawmaterial, 
	ct_openordercount_plantrawmaterial, 
	ct_pocount_plantrawmaterial, 
	ct_otif_plantrawmaterial, 
	ct_supplierperformancescore_plantrawmaterial
from fact_scn_plant_rawmaterial;



DROP TABLE IF EXISTS tmp_scn_plant_rawmaterial;








