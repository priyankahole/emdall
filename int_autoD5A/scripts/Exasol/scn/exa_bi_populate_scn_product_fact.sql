

/* Populate entity table fact_scn_product for supply chain navigation */


/* Direct Measures */

DROP TABLE IF EXISTS tmp_scn_fp_openordcount;
CREATE TABLE tmp_scn_fp_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_part p, dim_customer c
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;


DROP TABLE IF EXISTS tmp_scn_product;
CREATE TABLE tmp_scn_product as
select DISTINCT f_so.dim_partid dim_partid_product, prt.partnumber dd_partnumber_product, prt.partdescription dd_partdescription_product, prt.plant dd_plant_product,

/* Related measures */
sum((ifnull(f_so.ct_ConfirmedQty,0)*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_itemnetvalue_product,
COUNT(DISTINCT f_so.dim_plantid) ct_sellingplantcount_product,
COUNT(DISTINCT f_so.dim_customerid) ct_customercount_product,
(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_product,
count(distinct f_so.dd_salesdocno) ct_socount_product,
count(DISTINCT op.dd_salesdocno) ct_openordercount_product,
sum(f_so.ct_ConfirmedQty) ct_salesqty_product,
cast(0 as decimal(36,0)) ct_rawmaterialcount_product,
cast(0 as decimal(36,0)) ct_plantcount_product,
cast(0 as decimal(36,0)) ct_suppliercount_product

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_fp_openordcount op on f_so.dd_salesdocno = op.dd_salesdocno,
dim_part prt, dim_customer c, dim_plant dp, dim_salesdocumenttype sdt, dim_date agidt, dim_date pgid
WHERE f_so.dim_partid = prt.dim_partid AND prt.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.dim_plantid = dp.dim_plantid
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid
GROUP BY f_so.dim_partid, prt.partnumber, prt.partdescription, prt.plant;


INSERT INTO tmp_scn_product
(	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product,
	amt_itemnetvalue_product, 
	ct_sellingplantcount_product, 
	ct_customercount_product, 
	ct_otif_product, 
	ct_socount_product, 
	ct_openordercount_product, 
	ct_salesqty_product, 
	ct_rawmaterialcount_product, 
	ct_plantcount_product, 
	ct_suppliercount_product
)
select 0 dim_partid_product, 'ALL' dd_partnumber_product, 'ALL' dd_partdescription_product, 'ALL' dd_plant_product,
/* Related measures */
sum((ifnull(f_so.ct_ConfirmedQty,0)*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) amt_itemnetvalue_product,
COUNT(DISTINCT f_so.dim_plantid) ct_sellingplantcount_product,
COUNT(DISTINCT f_so.dim_customerid) ct_customercount_product,
(SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000
when ( f_so.Dim_DateidActualGI = 1 OR f_so.Dim_DateidGoodsIssue = 1) THEN 0
when (agidt.DateValue <= pgid.DateValue) AND (f_so.ct_DeliveredQty >= f_so.ct_ConfirmedQty) then 1.0000
else 0 end) /(CASE WHEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) <> 0 THEN SUM(case when f_so.ct_DeliveredQty = 0 then 0.0000 else 1.0000 end) ELSE 1.0000 END)) * 100 ct_otif_product,
count(distinct f_so.dd_salesdocno) ct_socount_product,
count(DISTINCT op.dd_salesdocno) ct_openordercount_product,
sum(f_so.ct_ConfirmedQty) ct_salesqty_product,
cast(0 as decimal(36,0)) ct_rawmaterialcount_product,
cast(0 as decimal(36,0)) ct_plantcount_product,
cast(0 as decimal(36,0)) ct_suppliercount_product

FROM fact_salesorder f_so LEFT OUTER JOIN tmp_scn_fp_openordcount op on f_so.dd_salesdocno = op.dd_salesdocno,
dim_part prt, dim_customer c, dim_plant dp, dim_salesdocumenttype sdt, dim_date agidt, dim_date pgid
WHERE f_so.dim_partid = prt.dim_partid AND prt.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.dim_plantid = dp.dim_plantid
AND f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
AND f_so.dim_dateidactualgi = agidt.dim_dateid AND f_so.dim_dateidgoodsissue = pgid.dim_dateid;




/* Update indirect measures */
/*
DROP TABLE IF EXISTS tmp_scn_product_indirect
CREATE TABLE tmp_scn_product_indirect AS
SELECT f_bom.dim_rootpartid dim_partid_product,
count(distinct f_bom.dd_ComponentNumber) ct_rawmaterialcount_product,
count(distinct f.dim_plantidordering) ct_plantcount_product,
count(distinct f.dim_vendorid) ct_suppliercount_product
FROM fact_bom f_bom, dim_part dpcomp, fact_purchase f, dim_vendor v --, dim_date d
WHERE f_bom.dd_ComponentNumber = dpcomp.partnumber AND dpcomp.scnenabled = 1
AND dpcomp.dim_partid = f.dim_partid
AND f.dim_vendorid = v.dim_vendorid AND v.scnenabled = 1
-- AND f.dim_dateidcreate = d.dim_dateid -- AND d.datevalue >= current_date-365
GROUP BY f_bom.dim_rootpartid
*/


-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_fp_fmm_prodorder_issue;   
create table tmp_scn_fp_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, dp.dim_partid dim_BOMcomponentid,dp.plant dd_componentplant, f.dd_productionordernumber, 
'Issue' as dd_activity, (-1 * sum(f.ct_quantity)) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber, dp.dim_partid, dp.plant
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_fp_fmm_prodorder_receive;   
create table tmp_scn_fp_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, dp.dim_partid dim_rootpartid, dp.plant dd_rootplant, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber, dp.dim_partid, dp.plant
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_fp_fmm_prodorder_link;   
create table tmp_scn_fp_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber, por.dim_rootpartid, por.dd_rootplant, poi.dim_BOMcomponentid, poi.dd_componentplant,
poi.ct_quantity ct_componentqty, 1 as dd_level
from tmp_scn_fp_fmm_prodorder_receive por, tmp_scn_fp_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;

-- Step 4 - Get BOM Root and Component
DROP TABLE IF EXISTS tmp_scn_fp_bom;
CREATE TABLE tmp_scn_fp_bom AS
SELECT distinct f_bom.dim_rootpartid, f_bom.dd_RootPart, f_bom.dd_ComponentNumber, f_bom.dim_bomcomponentid
FROM fact_bom f_bom, dim_part dpcomp
WHERE f_bom.dd_ComponentNumber = dpcomp.partnumber AND dpcomp.scnenabled = 1;

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_fp_bom (dd_RootPart,dd_ComponentNumber,dim_rootpartid, dim_bomcomponentid ) 
select dd_rootpart, dd_ComponentNumber, dim_rootpartid, dim_BOMcomponentid
from tmp_scn_fp_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_fp_bom b
where b.dim_BOMcomponentid = m.dim_BOMcomponentid and b.dim_rootpartid = m.dim_rootpartid);


DROP TABLE IF EXISTS tmp_scn_fp_purchase;
CREATE TABLE tmp_scn_fp_purchase AS
select distinct f.dim_partid, f.dim_plantidordering, f.dim_vendorid, dpcomp.partnumber
from fact_purchase f, dim_vendor v, dim_part dpcomp
where f.dim_vendorid = v.dim_vendorid AND v.scnenabled = 1
and f.dim_partid = dpcomp.dim_partid and dpcomp.scnenabled = 1;

DROP TABLE IF EXISTS tmp_scn_product_indirect;
CREATE TABLE tmp_scn_product_indirect AS
SELECT f_bom.dim_rootpartid dim_partid_product,
count(distinct f_bom.dd_ComponentNumber) ct_rawmaterialcount_product,
count(distinct f_pur.dim_plantidordering) ct_plantcount_product,
count(distinct f_pur.dim_vendorid) ct_suppliercount_product
FROM tmp_scn_fp_bom f_bom, tmp_scn_fp_purchase f_pur
WHERE f_bom.dd_ComponentNumber = f_pur.partnumber 
GROUP BY f_bom.dim_rootpartid;



INSERT INTO tmp_scn_product_indirect
SELECT 0 dim_partid_product,
count(distinct f_bom.dd_ComponentNumber) ct_rawmaterialcount_product,
count(distinct f_pur.dim_plantidordering) ct_plantcount_product,
count(distinct f_pur.dim_vendorid) ct_suppliercount_product
FROM tmp_scn_fp_bom f_bom, tmp_scn_fp_purchase f_pur
WHERE f_bom.dd_ComponentNumber = f_pur.partnumber ;


UPDATE tmp_scn_product s
SET s.ct_rawmaterialcount_product = t.ct_rawmaterialcount_product,
	s.ct_plantcount_product = t.ct_plantcount_product,
	s.ct_suppliercount_product = t.ct_suppliercount_product
FROM tmp_scn_product_indirect t, tmp_scn_product s	
WHERE s.dim_partid_product = t.dim_partid_product;





/* Insert Into Fact Table */

truncate TABLE fact_scn_product;

delete from number_fountain m where m.table_name = 'fact_scn_product';

insert into number_fountain
select  'fact_scn_product',
        ifnull(max(f.fact_scn_productid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_scn_product f
where f.fact_scn_productid <> 1;


insert into fact_scn_product 
( 
	fact_scn_productid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	amt_itemnetvalue_product, 
	ct_sellingplantcount_product, 
	ct_customercount_product, 
	ct_otif_product, 
	ct_socount_product, 
	ct_openordercount_product, 
	ct_salesqty_product, 
	ct_rawmaterialcount_product, 
	ct_plantcount_product, 
	ct_suppliercount_product
)
select 
  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_product') + 
      row_number() over(order by dim_partid_product) fact_scn_productid, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ifnull(amt_itemnetvalue_product,0), 	
	ct_sellingplantcount_product, 
	ct_customercount_product, 
	ct_otif_product, 
	ct_socount_product, 
	ct_openordercount_product, 
	ifnull(ct_salesqty_product,0), 
	ct_rawmaterialcount_product, 
	ct_plantcount_product, 
	ct_suppliercount_product
from tmp_scn_product s;




/* Drop Temp Tables */

DROP TABLE IF EXISTS tmp_scn_fp_openordcount;
DROP TABLE IF EXISTS tmp_scn_product;
DROP TABLE IF EXISTS tmp_scn_product_indirect;

DROP TABLE IF EXISTS tmp_scn_fp_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_fp_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_fp_bom;
