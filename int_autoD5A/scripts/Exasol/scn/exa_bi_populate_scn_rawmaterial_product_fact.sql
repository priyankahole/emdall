
/* Populate Relationship table fact_scn_rawmaterial_product for supply chain navigation */


DROP TABLE IF EXISTS tmp_scn_rm;
CREATE TABLE tmp_scn_rm AS
SELECT DISTINCT f.dim_partid from fact_purchase f, dim_part p where f.dim_partid = p.dim_partid and p.scnenabled = 1;

DROP TABLE IF EXISTS tmp_scn_prod;
CREATE TABLE tmp_scn_prod as
select distinct f.dim_partid from fact_salesorder f, dim_part p where f.dim_partid = p.dim_partid and p.scnenabled = 1;

-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_rm_fmm_prodorder_issue;   
create table tmp_scn_rm_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, dp.dim_partid dim_BOMcomponentid,dp.plant dd_componentplant, f.dd_productionordernumber, 
'Issue' as dd_activity, (-1 * sum(f.ct_quantity)) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber, dp.dim_partid, dp.plant
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_rm_fmm_prodorder_receive;   
create table tmp_scn_rm_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, dp.dim_partid dim_rootpartid, dp.plant dd_rootplant, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber, dp.dim_partid, dp.plant
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_rm_fmm_prodorder_link;   
create table tmp_scn_rm_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber, por.dim_rootpartid, por.dd_rootplant, poi.dim_BOMcomponentid, poi.dd_componentplant,
poi.ct_quantity ct_componentqty, 1 as dd_level
from tmp_scn_rm_fmm_prodorder_receive por, tmp_scn_rm_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;

-- Step 4 - Get BOM Root and Component
DROP TABLE IF EXISTS tmp_scn_rm_bom;
CREATE TABLE tmp_scn_rm_bom AS
SELECT DISTINCT f_bom.dd_ComponentNumber, f_bom.dd_RootPart, 
cp.dim_partid dim_BOMcomponentid, rp.dim_partid dim_rootpartid, 
cp.plant dd_componentplant, rp.plant dd_rootplant ,
sum(f_bom.ct_componentqty) ct_componentqty,
max(f_bom.dd_level) dd_level
FROM fact_bom f_bom, dim_part cp, dim_part rp
WHERE f_bom.dim_bomcomponentid = cp.dim_partid AND cp.scnenabled = 1
AND f_bom.dim_rootpartid = rp.dim_partid AND rp.scnenabled = 1
GROUP BY f_bom.dd_ComponentNumber, f_bom.dd_RootPart, 
cp.dim_partid , rp.dim_partid , 
cp.plant , rp.plant ;

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_rm_bom (dd_RootPart,dd_ComponentNumber,dim_rootpartid, dim_BOMcomponentid, 
dd_componentplant, dd_rootplant, ct_componentqty, dd_level) 
select dd_rootpart, dd_ComponentNumber, dim_rootpartid, dim_BOMcomponentid, 
dd_componentplant, dd_rootplant, ct_componentqty, dd_level
from tmp_scn_rm_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_rm_bom b
where b.dim_BOMcomponentid = m.dim_BOMcomponentid and b.dim_rootpartid = m.dim_rootpartid);


DROP TABLE IF EXISTS tmp_scn_rawmaterial_product_1;
CREATE TABLE tmp_scn_rawmaterial_product_1 AS
SELECT DISTINCT
f_bom.Dim_BOMComponentId dim_partid_rawmaterial, (f_bom.dd_ComponentNumber) dd_partnumber_rawmaterial, (dp_comp.partdescription) dd_partdescription_rawmaterial,
dp_comp.plant dd_plant_rawmaterial,
f_bom.dim_rootpartid dim_partid_product, (f_bom.dd_RootPart) dd_partnumber_product, (dp_prod.partdescription) dd_partdescription_product, dp_prod.plant dd_plant_product,
sum(f_bom.ct_componentqty) ct_qty_rawmaterialproduct,
max(f_bom.dd_level) ct_depth_rawmaterialproduct
FROM tmp_scn_rm_bom f_bom, dim_part dp_comp, dim_part dp_prod, tmp_scn_rm rm, tmp_scn_prod prod
WHERE f_bom.Dim_BOMComponentId = dp_comp.dim_partid AND dp_comp.dim_partid = rm.dim_partid
AND f_bom.dim_rootpartid = dp_prod.dim_partid AND dp_prod.dim_partid = prod.dim_partid
/*AND dp_prod.parttype = 'FERT'*/	 /*Indicates Finished Products */
AND f_bom.Dim_BOMComponentId <> f_bom.dim_rootpartid		/* Ignore rows where component is same as root. e.g level 0 rows */
AND f_bom.Dim_BOMComponentId <> 1 AND f_bom.dim_rootpartid <> 1
-- AND db_comp.scnenabled = 1 AND db_prod.scnenabled = 1
group by f_bom.Dim_BOMComponentId, f_bom.dd_ComponentNumber, dp_comp.plant, f_bom.dim_rootpartid, (f_bom.dd_RootPart), dp_prod.plant, (dp_comp.partdescription), 
(dp_prod.partdescription);


DROP TABLE IF EXISTS tmp_scn_rawmaterial_product;
CREATE TABLE tmp_scn_rawmaterial_product
as
select s.*, dpl_comp.dim_plantid dim_plantid_rawmaterial, dpl_prod.dim_plantid dim_plantid_product
FROM tmp_scn_rawmaterial_product_1 s, dim_plant dpl_comp, dim_plant dpl_prod
WHERE s.dd_plant_rawmaterial = dpl_comp.plantcode and s.dd_plant_product = dpl_prod.plantcode;



truncate TABLE fact_scn_rawmaterial_product;

delete from number_fountain m where m.table_name = 'fact_scn_rawmaterial_product';

insert into number_fountain
select  'fact_scn_rawmaterial_product',
        ifnull(max(f.fact_scn_rawmaterial_productid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s ),1))
from fact_scn_rawmaterial_product f
where f.fact_scn_rawmaterial_productid <> 1;


insert into fact_scn_rawmaterial_product 
(	fact_scn_rawmaterial_productid, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ct_qty_rawmaterialproduct, 
	ct_depth_rawmaterialproduct, 
	dim_plantid_rawmaterial, 
	dim_plantid_product
)
select 
	(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_rawmaterial_product') + 
			row_number() over(order by dim_partid_rawmaterial, dim_partid_product) fact_scn_rawmaterial_productid, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ct_qty_rawmaterialproduct, 
	ct_depth_rawmaterialproduct, 
	dim_plantid_rawmaterial, 
	dim_plantid_product
from tmp_scn_rawmaterial_product s;


/* Copy Data To Mirror Table */

truncate table fact_scn_product_rawmaterial;

insert into fact_scn_product_rawmaterial
(	
	fact_scn_product_rawmaterialid, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ct_qty_rawmaterialproduct, 
	ct_depth_rawmaterialproduct, 
	dim_plantid_rawmaterial, 
	dim_plantid_product
)
select
	fact_scn_rawmaterial_productid, 
	dim_partid_rawmaterial, 
	dd_partnumber_rawmaterial, 
	dd_partdescription_rawmaterial, 
	dd_plant_rawmaterial, 
	dim_partid_product, 
	dd_partnumber_product, 
	dd_partdescription_product, 
	dd_plant_product, 
	ct_qty_rawmaterialproduct, 
	ct_depth_rawmaterialproduct, 
	dim_plantid_rawmaterial, 
	dim_plantid_product
from fact_scn_rawmaterial_product;





DROP TABLE IF EXISTS tmp_scn_rm;
DROP TABLE IF EXISTS tmp_scn_prod;
DROP TABLE IF EXISTS tmp_scn_rawmaterial_product_1;
DROP TABLE IF EXISTS tmp_scn_rawmaterial_product;

DROP TABLE IF EXISTS tmp_scn_rm_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_rm_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_rm_fmm_prodorder_link;


