
/* Populate entity table fact_scn_supplier for supply chain navigation */


/* Direct Measures */

DROP TABLE IF EXISTS tmp_scn_supplier;
CREATE TABLE tmp_scn_supplier as
select f.dim_vendorid dim_vendorid_supplier, dv.vendornumber dd_vendornumber_supplier, 
dv.vendorname dd_vendorname_supplier, dv.city dd_vendorcity_supplier, dv.country dd_vendorcountry_supplier,

count(distinct f.dim_partid) ct_materialcount_supplier,
count(distinct f.dim_plantidordering) ct_plantcount_supplier,
sum(amt_ItemNetValue * amt_exchangerate_gbl) amt_itemnetvalue_supplier, 
count(DISTINCT f.dd_DocumentNo) ct_pocount_supplier,
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_supplier,
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END)/CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_supplier,

/* Related Measures(Derived) */
cast(0 as decimal(36,2)) ct_supplierperformancescore_supplier,

/* Indirect measures */
cast(0 as decimal(36,0)) ct_customercount_supplier,
cast(0 as decimal(36,0)) ct_sellingplantcount_supplier,
cast(0 as decimal(36,0)) ct_salesordercount_supplier,
cast(0 as decimal(36,0)) ct_opensalesordercount_supplier,
cast(0 as decimal(36,0)) ct_productcount_supplier

FROM fact_purchase f, dim_date d, dim_vendor dv, dim_part dp, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid 
AND f.dim_vendorid = dv.dim_vendorid AND dv.scnenabled = 1 
AND f.dim_partid = dp.dim_partid AND dp.scnenabled = 1
and f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_vendorid <> 1 AND f.dim_partid <> 1 AND ct_DeliveryQty > 0
GROUP BY f.dim_vendorid, dv.vendornumber, dv.vendorname, dv.city, dv.country;


/* Add 1 row that corresponds to all vendors */
DROP TABLE IF EXISTS tmp_scn_supplier_all;
CREATE TABLE tmp_scn_supplier_all as
SELECT 
count(distinct f.dim_partid) ct_materialcount_supplier,
count(distinct f.dim_plantidordering) ct_plantcount_supplier,
sum(amt_ItemNetValue * amt_exchangerate_gbl) amt_itemnetvalue_supplier,
count(DISTINCT f.dd_DocumentNo) ct_pocount_supplier,
count(DISTINCT CASE WHEN (CASE WHEN atrb.itemdeliverycomplete = 'X' or atrb.ItemGRIndicator = 'Not Set' THEN 0
                                ELSE CASE WHEN f.ct_ReceivedQty > f.ct_DeliveryQty THEN 0
                                  ELSE (f.ct_DeliveryQty - f.ct_ReceivedQty) END END)>0
                            THEN f.dd_DocumentNo ELSE null END) ct_openordercount_supplier,
100 * (SUM(CASE WHEN f.dd_ontime_delv = 'Y' AND f.dd_qtyinfull = 'Y' THEN 1.0000 ELSE 0.0000 END)/CASE WHEN SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) = 0.0000 THEN 1.0000 ELSE SUM(CASE WHEN f.dd_ontime_delv = 'Not Set' THEN 0.0000 ELSE 1.0000 END) END) ct_otif_supplier

FROM fact_purchase f, dim_date d, dim_vendor dv, dim_part dp, dim_purchasemisc atrb
WHERE f.dim_dateidcreate = d.dim_dateid AND f.dim_vendorid = dv.dim_vendorid AND f.dim_partid = dp.dim_partid and f.dim_purchasemiscid = atrb.dim_purchasemiscid
AND f.dim_vendorid <> 1 AND f.dim_partid <> 1 AND ct_DeliveryQty > 0;


insert into tmp_scn_supplier
( 
  dim_vendorid_supplier, 
  dd_vendornumber_supplier, 
  dd_vendorname_supplier, 
  dd_vendorcity_supplier, 
  dd_vendorcountry_supplier, 
  ct_materialcount_supplier, 
  ct_plantcount_supplier, 
  amt_itemnetvalue_supplier, 
  ct_pocount_supplier, 
  ct_openordercount_supplier, 
  ct_otif_supplier, 
  ct_supplierperformancescore_supplier, 
  ct_customercount_supplier, 
  ct_sellingplantcount_supplier, 
  ct_salesordercount_supplier, 
  ct_opensalesordercount_supplier, 
  ct_productcount_supplier
)  
select
  0 dim_vendorid_supplier, 
  'ALL' dd_vendornumber_supplier, 
  'ALL' dd_vendorname_supplier, 
  'ALL' dd_vendorcity_supplier, 
  'ALL' dd_vendorcountry_supplier, 
  ct_materialcount_supplier, 
  ct_plantcount_supplier, 
  amt_itemnetvalue_supplier, 
  ct_pocount_supplier, 
  ct_openordercount_supplier, 
  ct_otif_supplier, 
  cast(0 as decimal(36,2)) ct_supplierperformancescore_supplier, 
  cast(0 as decimal(36,0)) ct_customercount_supplier, 
  cast(0 as decimal(36,0)) ct_sellingplantcount_supplier, 
  cast(0 as decimal(36,0)) ct_salesordercount_supplier, 
  cast(0 as decimal(36,0)) ct_opensalesordercount_supplier, 
  cast(0 as decimal(36,0)) ct_productcount_supplier
from tmp_scn_supplier_all;



/* In Direct Measures */

/* Get distinct bom. Ensure that root part is in sales order */

-- Step 1 - Get Issue Parts For a Production Order
drop table if exists tmp_scn_sup_fmm_prodorder_issue;   
create table tmp_scn_sup_fmm_prodorder_issue as
select dp.partnumber dd_ComponentNumber, f.dd_productionordernumber, 'Issue' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' 
and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('261','262')
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) < 0;

-- Step 2 - Get Receive Parts For a Production Order
drop table if exists tmp_scn_sup_fmm_prodorder_receive;   
create table tmp_scn_sup_fmm_prodorder_receive as
select dp.partnumber dd_rootpart, f.dd_productionordernumber, 'Receipt' as dd_activity, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1
and mt.movementtype in ('101','102')  and mt.movementindicator = 'F'
and dp.scnenabled = 1
group by dp.partnumber, f.dd_productionordernumber
having sum(f.ct_quantity) > 0;

-- Step 3 - Join the above queries on Production Order
drop table if exists tmp_scn_sup_fmm_prodorder_link;   
create table tmp_scn_sup_fmm_prodorder_link as
select distinct dd_rootpart, dd_ComponentNumber
from tmp_scn_sup_fmm_prodorder_receive por, tmp_scn_sup_fmm_prodorder_issue poi
where por.dd_productionordernumber = poi.dd_productionordernumber;


-- Step 4 - Get BOM Root and Component
drop table if exists tmp_scn_sup_bom;
CREATE TABLE tmp_scn_sup_bom AS
SELECT DISTINCT f_bom.dd_ComponentNumber, f_bom.dd_RootPart 
from fact_bom f_bom 
inner join fact_salesorder f on f.dim_partid = f_bom.DIM_ROOTPARTID
inner join dim_part dp on dp.dim_partid = f.dim_partid and dp.scnenabled = 1
inner join dim_customer c on c.dim_customerid = f.dim_customerid and c.scnenabled = 1
WHERE f_bom.dd_ComponentNumber <> f_bom.dd_RootPart
AND dp.parttype = 'FG';

-- Step 5 - Insert New Root and Component Combinations from Material Movements Using Production Order
insert into tmp_scn_sup_bom (dd_RootPart,dd_ComponentNumber) 
select dd_rootpart, dd_ComponentNumber
from tmp_scn_sup_fmm_prodorder_link m
where not exists 
(select 1
from tmp_scn_sup_bom b
where b.dd_ComponentNumber = m.dd_ComponentNumber and b.dd_RootPart = m.dd_rootpart);

DROP TABLE IF EXISTS tmp_scn_sup_openordcount;
CREATE TABLE tmp_scn_sup_openordcount as
SELECT DISTINCT dd_salesdocno
FROM fact_salesorder f_so, dim_salesdocumenttype sdt, dim_part p, dim_customer c
WHERE f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid AND f_so.dim_partid = p.dim_partid AND p.scnenabled = 1
AND f_so.dim_customerid = c.dim_customerid AND c.scnenabled = 1
AND f_so.Dim_SalesOrderRejectReasonid = 1
AND f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty > 0;

DROP TABLE IF EXISTS tmp_scn_sup_purchase_bom;
CREATE TABLE tmp_scn_sup_purchase_bom as
select distinct fp.dim_vendorid, bom.dd_ComponentNumber, bom.dd_RootPart
from fact_purchase fp 
inner join dim_part dp on fp.dim_partid = dp.dim_partid and dp.scnenabled = 1
inner join dim_vendor v on fp.dim_vendorid = v.dim_vendorid and v.scnenabled = 1
inner join tmp_scn_sup_bom bom on bom.dd_ComponentNumber = dp.partnumber;

DROP TABLE IF EXISTS tmp_scn_sup_sales_bom;
CREATE TABLE tmp_scn_sup_sales_bom AS
SELECT distinct f_so.dim_customerid, f_so.dim_plantid, f_so.dd_salesdocno, op.dd_salesdocno opensalesdocno, bom.dd_rootpart
FROM  fact_salesorder f_so 
inner join dim_part dp on f_so.dim_partid = dp.dim_partid and dp.scnenabled = 1
inner join dim_customer c on c.dim_customerid = f_so.dim_customerid and c.scnenabled = 1
left outer join tmp_scn_sup_openordcount op ON op.dd_salesdocno = f_so.dd_salesdocno
left outer join tmp_scn_sup_bom bom on dp.partnumber = bom.dd_RootPart;


DROP TABLE IF EXISTS tmp_scn_supplierindirectmeasures;
CREATE TABLE tmp_scn_supplierindirectmeasures AS
SELECT f_po.dim_vendorid dim_vendorid_supplier, 
count(distinct f_so.dim_customerid) ct_customercount_supplier,
count(distinct f_so.dim_plantid) ct_sellingplantcount_supplier,
count(distinct f_so.dd_salesdocno) ct_salesordercount_supplier,
count(DISTINCT f_so.opensalesdocno) ct_opensalesordercount_supplier,
count(DISTINCT f_so.dd_rootpart) ct_productcount_supplier
FROM tmp_scn_sup_purchase_bom f_po left outer join tmp_scn_sup_sales_bom f_so on f_po.dd_rootpart = f_so.dd_RootPart
GROUP BY f_po.dim_vendorid;

INSERT INTO tmp_scn_supplierindirectmeasures
SELECT 0 dim_vendorid_supplier, 
count(distinct f_so.dim_customerid) ct_customercount_supplier,
count(distinct f_so.dim_plantid) ct_sellingplantcount_supplier,
count(distinct f_so.dd_salesdocno) ct_salesordercount_supplier,
count(DISTINCT f_so.opensalesdocno) ct_opensalesordercount_supplier,
count(DISTINCT f_so.dd_rootpart) ct_productcount_supplier
FROM tmp_scn_sup_purchase_bom f_po left outer join tmp_scn_sup_sales_bom f_so on f_po.dd_rootpart = f_so.dd_RootPart;


UPDATE tmp_scn_supplier s
SET s.ct_customercount_supplier = t.ct_customercount_supplier,
	s.ct_sellingplantcount_supplier = t.ct_sellingplantcount_supplier,
	s.ct_salesordercount_supplier = t.ct_salesordercount_supplier,
	s.ct_opensalesordercount_supplier = t.ct_opensalesordercount_supplier,
	s.ct_productcount_supplier = t.ct_productcount_supplier
FROM tmp_scn_supplierindirectmeasures t, tmp_scn_supplier s
where s.dim_vendorid_supplier = t.dim_vendorid_supplier;




/* Insert Into Fact Table */

truncate TABLE fact_scn_supplier;

delete from number_fountain m where m.table_name = 'fact_scn_supplier';

insert into number_fountain
select  'fact_scn_supplier',
        ifnull(max(f.fact_scn_supplierid),
        ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_scn_supplier f
where f.fact_scn_supplierid <> 1;


insert into fact_scn_supplier 
( 
  fact_scn_supplierid, 
  dim_vendorid_supplier, 
  dd_vendornumber_supplier, 
  dd_vendorname_supplier, 
  dd_vendorcity_supplier, 
  dd_vendorcountry_supplier, 
  ct_materialcount_supplier, 
  ct_plantcount_supplier, 
  amt_itemnetvalue_supplier, 
  ct_pocount_supplier, 
  -- ct_latedeliverycount_last3months, 
  -- ct_totaldeliverycount_last3months, 
  ct_openordercount_supplier, 
  ct_otif_supplier, 
  ct_supplierperformancescore_supplier, 
  ct_customercount_supplier, 
  ct_sellingplantcount_supplier, 
  ct_salesordercount_supplier, 
  ct_opensalesordercount_supplier, 
  ct_productcount_supplier
  -- amt_openorder
)
select 
  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_scn_supplier') + 
      row_number() over(order by dim_vendorid_supplier) fact_scn_supplierid, 
  dim_vendorid_supplier, 
  dd_vendornumber_supplier, 
  dd_vendorname_supplier, 
  dd_vendorcity_supplier, 
  dd_vendorcountry_supplier, 
  ct_materialcount_supplier, 
  ct_plantcount_supplier, 
  amt_itemnetvalue_supplier, 
  ct_pocount_supplier, 
  -- ct_latedeliverycount_last3months, 
  -- ct_totaldeliverycount_last3months, 
  ct_openordercount_supplier, 
  ct_otif_supplier, 
  ct_supplierperformancescore_supplier, 
  ct_customercount_supplier, 
  ct_sellingplantcount_supplier, 
  ct_salesordercount_supplier, 
  ct_opensalesordercount_supplier, 
  ct_productcount_supplier
  -- amt_openorder
from tmp_scn_supplier s;




/* Drop Temp Tables */


DROP TABLE IF EXISTS tmp_scn_sup_sales_bom;
DROP TABLE IF EXISTS tmp_scn_sup_purchase_bom;
DROP TABLE IF EXISTS tmp_scn_sup_openordcount;
drop table if exists tmp_scn_sup_bom;

DROP TABLE IF EXISTS tmp_scn_supplierindirectmeasures;
DROP TABLE IF EXISTS tmp_scn_supplier_all;
DROP TABLE IF EXISTS tmp_scn_supplier;

DROP TABLE IF EXISTS tmp_scn_sup_fmm_prodorder_issue;
DROP TABLE IF EXISTS tmp_scn_sup_fmm_prodorder_receive;
DROP TABLE IF EXISTS tmp_scn_sup_fmm_prodorder_link;




