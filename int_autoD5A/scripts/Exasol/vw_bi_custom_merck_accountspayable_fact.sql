/*   Script         :    */
/*   Author         : George */
/*   Created On     : 07 Oct 2014 */
/*   Description    : Custom Accounts Payable script  */

/*UPDATE fact_accountspayable fap
from dim_vendor dv, dim_term dt
set dd_MaxTerms_merck = ifnull((case when dt.termbucket_merck = '90' then 'YES'
	 when dt.termbucket_merck = '60' and dv.countryname in ('France','Spain') then 'YES'
	 else 'NO' end),'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*where fap.dim_vendorid = dv.dim_vendorid
and fap.Dim_CustomerPaymentTermsid = dt.dim_termid
and dd_MaxTerms_merck <> ifnull((case when dt.termbucket_merck = '90' then 'YES'
	 when dt.termbucket_merck = '60' and dv.countryname in ('France','Spain') then 'YES'
	 else 'NO' end),'Not Set')
*/
	 
	 
UPDATE fact_accountspayable fap
set dd_MaxTerms_merck = ifnull((case when dt.termbucket_merck = '90' then 'YES'
	 when dt.termbucket_merck = '60' and dc.countryname in ('France','Spain') then 'YES'
	 else 'NO' end),'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from dim_company dc, dim_term dt, fact_accountspayable fap
where fap.dim_companyid = dc.dim_companyid
and fap.Dim_CustomerPaymentTermsid = dt.dim_termid
and dd_MaxTerms_merck <> ifnull((case when dt.termbucket_merck = '90' then 'YES'
	 when dt.termbucket_merck = '60' and dc.countryname in ('France','Spain') then 'YES'
	 else 'NO' end),'Not Set');
