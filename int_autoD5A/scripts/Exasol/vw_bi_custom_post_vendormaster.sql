drop table if exists tmp_upd_diversitybyvendor_merck_v3;
create table tmp_upd_diversitybyvendor_merck_v3
as select
vendornumber, max(diverse) as diverse, max(small) as small, max(SDB) as SDB,
max(VBE) as VBE, max(MBE) as MBE, max(WBE) as WBE, max(mwbe) as mwbe,
max(dvbe) as dvbe, max(glbt) as glbt, max(disabled) as disabled
,max(dbe) as dbe,max(sme_small) as sme_small,max(sme_micro) as sme_micro,max(sme_medium) as  sme_medium
from diversitybyvendor_merck_v4
group by vendornumber;

update dim_vendormaster v
set v.diverse_merck = ifnull(u.diverse,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.diverse_merck <> ifnull(u.diverse,'Not Set');

update dim_vendormaster v
set v.small_merck = ifnull(u.small,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.small_merck <> ifnull(u.small,'Not Set');

/* DBE = SDB */
update dim_vendormaster v
set v.sdb_merck = ifnull(u.dbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.sdb_merck <> ifnull(u.dbe,'Not Set');

update dim_vendormaster v
set v.vbe_merck= ifnull(u.vbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.vbe_merck <> ifnull(u.vbe,'Not Set');

update dim_vendormaster v
set v.mbe_merck = ifnull(u.mbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.mbe_merck <> ifnull(u.mbe,'Not Set');

update dim_vendormaster v
set v.wbe_merck = ifnull(u.wbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.wbe_merck <> ifnull(u.wbe,'Not Set');


update dim_vendormaster v
set v.mwbe_merck = ifnull(u.mwbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.mwbe_merck <> ifnull(u.mwbe,'Not Set');

update dim_vendormaster v
set v.dvbe_merck = ifnull(u.dvbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.dvbe_merck <> ifnull(u.dvbe,'Not Set');

update dim_vendormaster v
set v.glbt_merck = ifnull(u.glbt,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.glbt_merck <> ifnull(u.glbt,'Not Set');

update dim_vendormaster v
set v.disabled_merck = ifnull(u.disabled,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.disabled_merck <> ifnull(u.disabled,'Not Set');

update dim_vendormaster v
set v.sme_small = ifnull(u.sme_small,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.sme_small <> ifnull(u.sme_small,'Not Set');

update dim_vendormaster v
set v.sme_micro = ifnull(u.sme_micro,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.sme_micro <> ifnull(u.sme_micro,'Not Set');

update dim_vendormaster v
set v.sme_medium = ifnull(u.sme_medium,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendormaster v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.sme_medium <> ifnull(u.sme_medium,'Not Set');



/* 05.03.2015 */
update  dim_vendormaster d
set d.functionalgroup_merck = ifnull(v.functionalgroup,'Not Set')
from (SELECT DISTINCT VENDORNUMBER,FIRST_VALUE(FUNCTIONALGROUP) OVER (PARTITION BY VENDORNUMBER ORDER BY FUNCTIONALGROUP DESC) AS FUNCTIONALGROUP FROM vendorfunctionalgroup_merck) v, dim_vendormaster d
where d.vendornumber = lpad(v.vendornumber,10,'0')
and d.functionalgroup_merck <> ifnull(v.functionalgroup,'Not Set');