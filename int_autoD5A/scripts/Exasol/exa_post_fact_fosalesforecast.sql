
/*Calculations for Skills*/

drop table if exists tmp_quantities_forrank1;
create table tmp_quantities_forrank1 as
select DD_COUNTRY_DESTINATION_CODE,
DD_SALES_COCD,
DD_REPORTING_COMPANY_CODE,
DD_HEI_CODE,
DD_PARTNUMBER,
fos.DD_REPORTINGDATE,
DD_FORECASTDATE,
DD_MARKET_GROUPING, CT_FORECASTQUANTITY,CT_FORECASTQUANTITY_customer,CT_SALESQUANTITY
from fact_fosalesforecast fos,
tmp_maxrptdate r
where dd_forecastrank=1
and  TO_DATE(fos.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate;

merge into fact_fosalesforecast f
using (select distinct fact_fosalesforecastid,avg(t1.CT_FORECASTQUANTITY) as CT_FORECASTQUANTITY,avg(t1.CT_FORECASTQUANTITY_customer)  as CT_FORECASTQUANTITY_customer
from tmp_quantities_forrank1 t1,fact_fosalesforecast f
where 
t1.DD_COUNTRY_DESTINATION_CODE=f.DD_COUNTRY_DESTINATION_CODE
and t1.DD_SALES_COCD=f.DD_SALES_COCD
and t1.DD_REPORTING_COMPANY_CODE=f.DD_REPORTING_COMPANY_CODE
and t1.DD_HEI_CODE=f.DD_HEI_CODE
and t1.DD_PARTNUMBER=f.DD_PARTNUMBER
and t1.DD_REPORTINGDATE=f.DD_REPORTINGDATE
and t1.DD_FORECASTDATE=f.DD_FORECASTDATE
and t1.DD_MARKET_GROUPING=f.DD_MARKET_GROUPING
group by fact_fosalesforecastid) t
on f.fact_fosalesforecastid=t.fact_fosalesforecastid
when matched then update set CT_FORECASTQUANTITYrank1=t.CT_FORECASTQUANTITY,
CT_FORECASTQUANTITY_customer_rank1=t.CT_FORECASTQUANTITY_customer;

/* Identify Top 25% of sales Materials*/

drop table if exists tmp_forTOP5;
create table tmp_forTOP5 as
select distinct dd_partnumber,dd_plantcode,b.dd_reportingdate,dd_market_grouping,dd_reporting_company_code,dd_country_destination_code, dd_hei_code,
sum(ct_forecastquantity_customer*ct_nasp) as amt_top5
from fact_fosalesforecast b,tmp_maxrptdate r
where  TO_DATE(b.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
GROUP BY  dd_partnumber,dd_plantcode,b.dd_reportingdate,dd_market_grouping,dd_reporting_company_code,dd_country_destination_code, dd_hei_code;

drop table if exists tmp_percentage25;
create table tmp_percentage25 as 
select 0.25*sum(amt_top5) as toppercentage from tmp_forTOP5;

delete from tmp_forTOP5_v2;
insert into tmp_forTOP5_v2 (dd_partnumber,amt_top5,id)
select distinct dd_partnumber,amt_top5,  row_number() over (partition by '' order by amt_top5 desc) as id
from tmp_forTOP5;

drop table if exists tmp_forTOP5_v3;
create table tmp_forTOP5_v3 as 
SELECT id, amt_top5, SUM(amt_top5) OVER(ORDER BY id ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW ) AS cml
FROM tmp_forTOP5_v2
ORDER BY id asc;

drop table if exists tmp_fortopyes;
create table tmp_fortopyes as
select * from tmp_forTOP5_v3, tmp_percentage25
where cml<=toppercentage
order by id asc;

update  tmp_forTOP5_v2
set dd_topyesno='Yes'
from tmp_forTOP5_v2 a ,tmp_fortopyes b
where a.id=b.id;

/*Insert Creation Audit Logs*/

insert into fact_fosalesforecast_audittrail 
(usermainid,loginid, dim_plantid, dim_partid,dim_dateidreporting, audit_message, forecast_rank,last_entry)
select distinct 
1 as userid,'Aera Technology',fso.dim_plantid,fso.dim_partid, fso.dim_dateidreporting,
concat('Forecast ran on ', fso.dd_reportingdate),
dd_forecastselected, 'Y'
from fact_fosalesforecast fso 
where dd_latestreporting = 'Yes'
and not exists (select 1 from fact_fosalesforecast_audittrail fso_at
where fso.dim_plantid = fso_at.dim_plantid
and fso.dim_partid = fso_at.dim_partid
and fso.dim_dateidreporting = fso_at.dim_dateidreporting);

/*Insert Audit log inly for 25% Top Sales Materials*/
insert into fact_fosalesforecast_audittrail 
(usermainid,loginid, dim_plantid, dim_partid,dim_dateidreporting, audit_message, forecast_rank,last_entry)
select distinct 1 as userid,'Aera Technology',fso.dim_plantid,fso.dim_partid, fso.dim_dateidreporting, 
'This Material is one of your Top 25% Materials' as audit_message, dd_forecastselected ,'N'
from fact_fosalesforecast fso , tmp_forTOP5_v2 a ,tmp_forTOP5 b 
where a.dd_partnumber=b.dd_partnumber 
and a.dd_topyesno='Yes'
 and b.dd_partnumber=fso.dd_partnumber 
and b.dd_plantcode=fso.dd_plantcode 
and b.dd_reportingdate=fso.dd_reportingdate 
and b.dd_market_grouping=fso.dd_market_grouping 
and b.dd_reporting_company_code=fso.dd_reporting_company_code 
and b.dd_country_destination_code=fso.dd_country_destination_code 
and b.dd_hei_code=fso.dd_hei_code 
and fso.dd_latestreporting = 'Yes'
and not exists (select 1 from fact_fosalesforecast_audittrail fso_at
where fso.dim_plantid = fso_at.dim_plantid
and fso.dim_partid = fso_at.dim_partid
and fso.dim_dateidreporting = fso_at.dim_dateidreporting
and fso_at.audit_message='This Material is one of your Top 25% Materials');

/*Insert Audit log only for Manual Forecast Types*/

insert into fact_fosalesforecast_audittrail 
(usermainid,loginid, dim_plantid, dim_partid,dim_dateidreporting, audit_message, forecast_rank,last_entry)
select distinct 
1 as userid,'Aera Technology',fso.dim_plantid,fso.dim_partid, fso.dim_dateidreporting,
'Forecast Type: Manual' as audit_message,
dd_forecastselected,'N'
from fact_fosalesforecast fso 
where dd_latestreporting = 'Yes'
and not exists (select 1 from fact_fosalesforecast_audittrail fso_at
where fso.dim_plantid = fso_at.dim_plantid
and fso.dim_partid = fso_at.dim_partid
and fso.dim_dateidreporting = fso_at.dim_dateidreporting
and  audit_message='Forecast Type: Manual');

/*Create temp table for filters*/
drop table if exists tmp_for_plants;
create table tmp_for_plants as select  distinct DD_PLANTTITLEMERCK 
			  from fact_fosalesforecast;

/* 15 Nov 2018 We need to filter out the ones with the corresponding SAP flags*/
drop table if exists tmp_for_GPF;
create table tmp_for_GPF as 
select distinct prodfamilydescription_merck,DD_PLANTTITLEMERCK,DominantSpeciesDescription_Merck,therapeuticalclass_merck,ProductGroupDescription_Merck  from 
fact_fosalesforecast fso inner join dim_part fosfpa
on fso.dim_partid = fosfpa.dim_partid
where dd_latestreporting = 'Yes'
 AND ((fosfpa.PartTypeDescription) IN (('Finished products'),('Trading goods')) ) 
     AND ((fosfpa.PlantMaterialStatus) IN (('Not Set'),('Z1'),('Z2'),('Z3'),('Z4'),('Z5')) )  AND ((fosfpa.PartNumber_NoLeadZero) NOT IN (('163333'),('189746'),('179195'),('194452')) )  
     AND (lower(fosfpa.deletionflag) = lower('Not Set'))  AND ((fosfpa.crossmatplantsts) IN (('Not Set'),('Z3'),('00')) )  
and dd_holdout='3'
order by 1 asc;

update fact_fosalesforecast_audittrail a
set last_entry='N'
from fact_fosalesforecast_audittrail;

update fact_fosalesforecast_audittrail a
set last_entry='Y'
from fact_fosalesforecast_audittrail a,tmp_maxrptdate r,dim_date d
where a.dim_dateidreporting=d.dim_dateid
and d.datevalue=r.dd_reportingdate
and audit_message not in ('Forecast Type: Manual','This Material is one of your Top 25% Materials');

DROP TABLE IF EXISTS tmp_maxrptdate;

