/* Staging table preparation */

	/* drop table if exists t001w__xbi_hash_stg
	create table t001w__xbi_hash_stg */

	truncate table t001w__xbi_hash_stg;
	insert into t001w__xbi_hash_stg
	 (
		T001W_WERKS_HSH,
		T001W_WERKS_LDTS,
		T001W_WERKS_RSRC,
		T001W_WERKS_TEXT_HSH_DIFF,
		T001W_WERKS,
		T001W_IWERK,
		T001W_VKORG,
		T001W_ADRNR,
		T001W_CHAZV,
		T001W_FABKL,
		T001W_KUNNR,
		T001W_LIFNR,
		T001W_NAME2,
		T001W_PFACH,
		T001W_REGIO,
		T001W_STRAS,
		T001W_BWKEY,
		T001W_EKORG,
		T001W_LAND1,
		T001W_NAME1,
		T001W_ORT01,
		T001W_PSTLZ,
		T001W_SPRAS)
	select 	cast(UPPER(HASH_MD5(T001W_WERKS)) as varchar(32)) as T001W_WERKS_HSH,
	       	current_timestamp as T001W_WERKS_LDTS,
	       	cast('ECC: T001W' as varchar(50)) as T001W_WERKS_RSRC,
			cast(UPPER(HASH_MD5(upper(T001W_WERKS ||'^'||
										IFNULL(T001W_IWERK,'') ||'^'||
										IFNULL(T001W_VKORG,'') ||'^'||
										IFNULL(T001W_ADRNR,'') ||'^'||
										IFNULL(T001W_CHAZV,'') ||'^'||
										IFNULL(T001W_FABKL,'') ||'^'||
										IFNULL(T001W_KUNNR,'') ||'^'||
										IFNULL(T001W_LIFNR,'') ||'^'||
										IFNULL(T001W_NAME2,'') ||'^'||
										IFNULL(T001W_PFACH,'') ||'^'||
										IFNULL(T001W_REGIO,'') ||'^'||
										IFNULL(T001W_STRAS,'') ||'^'||
										IFNULL(T001W_BWKEY,'') ||'^'||
										IFNULL(T001W_EKORG,'') ||'^'||
										IFNULL(T001W_LAND1,'') ||'^'||
										IFNULL(T001W_NAME1,'') ||'^'||
										IFNULL(T001W_ORT01,'') ||'^'||
										IFNULL(T001W_PSTLZ,'') ||'^'||
										IFNULL(T001W_SPRAS,'')
				)))as varchar(32)) as T001W_WERKS_TEXT_HSH_DIFF,
			T001W_WERKS,
			T001W_IWERK,
			T001W_VKORG,
			T001W_ADRNR,
			T001W_CHAZV,
			T001W_FABKL,
			T001W_KUNNR,
			T001W_LIFNR,
			T001W_NAME2,
			T001W_PFACH,
			T001W_REGIO,
			T001W_STRAS,
			T001W_BWKEY,
			T001W_EKORG,
			T001W_LAND1,
			T001W_NAME1,
			T001W_ORT01,
			T001W_PSTLZ,
			T001W_SPRAS
	from T001W__XBI;
