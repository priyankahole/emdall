/* HUB Customer table populating */

	/* create table hub_customer (KNA1_KUNNR_HSH varchar(32),
							   KNA1_KUNNR_LDTS timestamp,
							   KNA1_KUNNR_RSRC varchar(50),
							   KNA1_KUNNR varchar(10)
							  ) */

	insert into hub_customer (KNA1_KUNNR_HSH, KNA1_KUNNR_LDTS, KNA1_KUNNR_RSRC, KNA1_KUNNR)
	select distinct st1.KNA1_KUNNR_HSH,
	       st1.KNA1_KUNNR_LDTS,
	       st1.KNA1_KUNNR_RSRC,
	       st1.KNA1_KUNNR
	from KNA1__xbi_hash_stg as st1
	where st1.KNA1_KUNNR_HSH not in (
                                   	 select KNA1_KUNNR_HSH
                                   	 from hub_customer
                                    );
