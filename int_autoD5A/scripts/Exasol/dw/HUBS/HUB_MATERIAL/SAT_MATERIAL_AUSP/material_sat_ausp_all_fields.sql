/* ---------------------------------------------------------------------------- */

/* SAT AUSP ALL table population */


	/* create table SAT_MATERIAL_AUSP_ALL (
	AUSP_OBJEK_HSH          VARCHAR(32) UTF8,
    AUSP_OBJEK_LDTS         TIMESTAMP WITH LOCAL TIME ZONE,
	AUSP_OBJEK_LEDTS        TIMESTAMP WITH LOCAL TIME ZONE,
    AUSP_OBJEK_RSRC         VARCHAR(50) UTF8,
    AUSP_OBJEK_ALL_HSH_DIFF VARCHAR(32) UTF8,
    CONCENTRATION           VARCHAR(100) UTF8,
    CONCENTRATIONUOM        VARCHAR(30) UTF8,
    CONTENTS                VARCHAR(100) UTF8,
    CONTENTSUOM             VARCHAR(30) UTF8,
    DOMINANTSPECIES         VARCHAR(30) UTF8,
    ITEMSUBTYPE             VARCHAR(30) UTF8,
    ITEMTYPE                VARCHAR(30) UTF8,
    PRIMARYSITE             VARCHAR(30) UTF8,
    PRODUCTGROUP            VARCHAR(30) UTF8,
    PRODUCTTYPE             VARCHAR(30) UTF8,
    STRENGTH                VARCHAR(30) UTF8,
    STRENGTH_UOM            VARCHAR(30) UTF8,
    FILL_QTY                DECIMAL(18,4),
    FILL_QTY_UOM            VARCHAR(30) UTF8
			) */

	/* insert new records and changed ones */
	insert into SAT_MATERIAL_AUSP_ALL
					   (AUSP_OBJEK_HSH,
                        AUSP_OBJEK_LDTS,
                        AUSP_OBJEK_RSRC,
                        AUSP_OBJEK_ALL_HSH_DIFF,
                        CONCENTRATION,
                        CONCENTRATIONUOM,
                        CONTENTS,
                        CONTENTSUOM,
                        DOMINANTSPECIES,
                        ITEMSUBTYPE,
                        ITEMTYPE,
                        PRIMARYSITE,
                        PRODUCTGROUP,
                        PRODUCTTYPE,
                        STRENGTH,
                        STRENGTH_UOM,
                        FILL_QTY,
                        FILL_QTY_UOM)
	select 	st1.AUSP_OBJEK_HSH,
            st1.AUSP_OBJEK_LDTS,
            st1.AUSP_OBJEK_RSRC,
            st1.AUSP_OBJEK_ALL_HSH_DIFF,
            st1.CONCENTRATION,
            st1.CONCENTRATIONUOM,
            st1.CONTENTS,
            st1.CONTENTSUOM,
            st1.DOMINANTSPECIES,
            st1.ITEMSUBTYPE,
            st1.ITEMTYPE,
            st1.PRIMARYSITE,
            st1.PRODUCTGROUP,
            st1.PRODUCTTYPE,
            st1.STRENGTH,
            st1.STRENGTH_UOM,
            st1.FILL_QTY,
            st1.FILL_QTY_UOM
	from AUSP__XBI_HASH_STG as st1
	      left outer join SAT_MATERIAL_AUSP_ALL as s1
	                   on st1.AUSP_OBJEK_HSH = s1.AUSP_OBJEK_HSH
	                      and s1.AUSP_OBJEK_LEDTS is NULL
	where (
	        s1.AUSP_OBJEK_HSH is null OR
	        (st1.AUSP_OBJEK_ALL_HSH_DIFF != s1.AUSP_OBJEK_ALL_HSH_DIFF
	          AND s1.AUSP_OBJEK_LDTS = (
                                        select max(z.AUSP_OBJEK_LDTS)
                                        from SAT_MATERIAL_AUSP_ALL  z
                                        where s1.AUSP_OBJEK_HSH = z.AUSP_OBJEK_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s22
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s22;
	insert into upd_sat_end_date_loop_s22 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.AUSP_OBJEK_HSH,
	       s1.AUSP_OBJEK_LDTS
	from SAT_MATERIAL_AUSP_ALL as s1
	where (
	            s1.AUSP_OBJEK_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.AUSP_OBJEK_HSH)
	                     from SAT_MATERIAL_AUSP_ALL as y
	                     where   y.AUSP_OBJEK_HSH = s1.AUSP_OBJEK_HSH
	                         AND y.AUSP_OBJEK_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s22
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s22;
	insert into upd_sat_end_date_cross_s22 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s22 t1
					inner join upd_sat_end_date_loop_s22 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update SAT_MATERIAL_AUSP_ALL dst
	   set dst.AUSP_OBJEK_LEDTS = src.BUSKEY_LEDTS
	  from SAT_MATERIAL_AUSP_ALL dst
				inner join upd_sat_end_date_cross_s22 src on dst.AUSP_OBJEK_HSH = src.BUSKEY_HSH
	      												and dst.AUSP_OBJEK_LDTS = src.BUSKEY_LDTS;
