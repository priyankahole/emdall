/* Staging table preparation */

	/* 	CREATE TABLE AUSP__XBI (
	    AUSP_ADZHL     DECIMAL(18,0),
	    AUSP_ATAWE     DECIMAL(18,4),
	    AUSP_ATFLV     VARCHAR(100) UTF8,
	    AUSP_ATWRT     VARCHAR(30) UTF8,
	    AUSP_KLART     VARCHAR(3) UTF8,
	    AUSP_OBJEK     VARCHAR(50) UTF8,
	    AUSP_ATAW1     DECIMAL(18,4),
	    AUSP_ATFLB     VARCHAR(100) UTF8,
	    AUSP_ATINN     DECIMAL(18,0),
	    AUSP_ATZHL     DECIMAL(18,0),
	    AUSP_MAFID     VARCHAR(1) UTF8,
	    AUSP_ATFLV_NUM DECIMAL(18,4),
	    AUSP_ATFLB_NUM DECIMAL(18,4)
	)

	drop table if exists AUSP__XBI_L2
	create table AUSP__XBI_L2 as
	*/

truncate table AUSP__XBI_L2;
insert into AUSP__XBI_L2
select AUSP_OBJEK,
	   max(case when tt.AUSP_ATINN = 6 then tt.AUSP_ATFLV else NULL end) as Concentration,
	   max(case when tt.AUSP_ATINN = 7 then tt.AUSP_ATWRT else NULL end) as ConcentrationUOM,
	   max(case when tt.AUSP_ATINN = 8 then tt.AUSP_ATFLV else NULL end) as Contents,
	   max(case when tt.AUSP_ATINN = 9 then tt.AUSP_ATWRT else NULL end) as ContentsUOM,
	   max(case when tt.AUSP_ATINN = 128 then tt.AUSP_ATWRT else NULL end) as DominantSpecies,
	   max(case when tt.AUSP_ATINN = 13 then tt.AUSP_ATWRT else NULL end) as ItemSubType,
	   max(case when tt.AUSP_ATINN = 14 then tt.AUSP_ATWRT else NULL end) as ItemType,
	   max(case when tt.AUSP_ATINN = 438 then tt.AUSP_ATWRT else NULL end) as PrimarySite,
	   max(case when tt.AUSP_ATINN = 129 then tt.AUSP_ATWRT else NULL end) as ProductGroup,
	   max(case when tt.AUSP_ATINN = 127 then tt.AUSP_ATWRT else NULL end) as ProductType,
	   max(case when tt.AUSP_ATINN = 285 then tt.AUSP_ATWRT else NULL end) as Strength,
	   max(case when tt.AUSP_ATINN = 287 then tt.AUSP_ATWRT else NULL end) as Strength_uom,
	   max(case when tt.AUSP_ATINN = 267 then convert(decimal (18,4), tt.ausp_atflv/10000000000000000) else NULL end) as Fill_qty,
	   max(case when tt.AUSP_ATINN = 268 then tt.AUSP_ATWRT else NULL end) as Fill_qty_uom
from (
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 6 -- Concentration = ifnull(a.AUSP_ATFLV,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 7 -- ConcentrationUOM = ifnull(a.AUSP_ATWRT,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 8 -- Contents = ifnull(a.AUSP_ATFLV,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 9 -- ContentsUOM = ifnull(a.AUSP_ATWRT,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 128 -- DominantSpecies = ifnull(a.AUSP_ATWRT,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 13	-- ItemSubType = ifnull(a.AUSP_ATWRT,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 14	-- ItemType = ifnull(a.AUSP_ATWRT,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 438 -- PrimarySite = ifnull(a.AUSP_ATWRT,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 129 -- ProductGroup = ifnull(a.AUSP_ATWRT,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.AUSP_ATINN = 127 -- ProductType = ifnull(a.AUSP_ATWRT,'Not Set')
		union all
		select * from AUSP__XBI a
		where a.ausp_atinn = 285 -- a.strength = b.ausp_atwrt
		union all
		select * from AUSP__XBI a
		where a.ausp_atinn = 287 -- a.strength_uom = b.ausp_atwrt
		union all
		select * from AUSP__XBI a
		where a.ausp_atinn = 267 -- a.fill_qty = convert(decimal (18,4), b.ausp_atflv/10000000000000000)
		union all
		select * from AUSP__XBI a
		where a.ausp_atinn = 268 -- a.fill_qty_uom = b.ausp_atwrt
	) tt
group by AUSP_OBJEK;

/* drop table if exists ausp__xbi_hash_stg as
	create table ausp__xbi_hash_stg */

	truncate table ausp__xbi_hash_stg;
	insert into ausp__xbi_hash_stg	(
		AUSP_OBJEK_HSH,
		AUSP_OBJEK_LDTS,
		AUSP_OBJEK_RSRC,
		AUSP_OBJEK_ALL_HSH_DIFF,
		AUSP_OBJEK,
		CONCENTRATION,
		CONCENTRATIONUOM,
		CONTENTS,
		CONTENTSUOM,
		DOMINANTSPECIES,
		ITEMSUBTYPE,
		ITEMTYPE,
		PRIMARYSITE,
		PRODUCTGROUP,
		PRODUCTTYPE,
		STRENGTH,
		STRENGTH_UOM,
		FILL_QTY,
		FILL_QTY_UOM)
	select 	cast(UPPER(HASH_MD5(AUSP_OBJEK)) as varchar(32)) as AUSP_OBJEK_HSH,
			current_timestamp as AUSP_OBJEK_LDTS,
		    cast('ECC: AUSP' as varchar(50)) as AUSP_OBJEK_RSRC,
			cast(UPPER(HASH_MD5(upper(AUSP_OBJEK || '|^|' ||
								IFNULL(CONCENTRATION,'') || '|^|' ||
								IFNULL(CONCENTRATIONUOM,'') || '|^|' ||
								IFNULL(CONTENTS,'') || '|^|' ||
								IFNULL(CONTENTSUOM,'') || '|^|' ||
								IFNULL(DOMINANTSPECIES,'') || '|^|' ||
								IFNULL(ITEMSUBTYPE,'') || '|^|' ||
								IFNULL(ITEMTYPE,'') || '|^|' ||
								IFNULL(PRIMARYSITE,'') || '|^|' ||
								IFNULL(PRODUCTGROUP,'') || '|^|' ||
								IFNULL(PRODUCTTYPE,'') || '|^|' ||
								IFNULL(STRENGTH,'') || '|^|' ||
								IFNULL(STRENGTH_UOM,'') || '|^|' ||
								IFNULL(FILL_QTY,0.00) || '|^|' ||
								IFNULL(FILL_QTY_UOM,'')
				)))as varchar(32)) as AUSP_OBJEK_ALL_HSH_DIFF,
			AUSP_OBJEK,
			CONCENTRATION,
			CONCENTRATIONUOM,
			CONTENTS,
			CONTENTSUOM,
			DOMINANTSPECIES,
			ITEMSUBTYPE,
			ITEMTYPE,
			PRIMARYSITE,
			PRODUCTGROUP,
			PRODUCTTYPE,
			STRENGTH,
			STRENGTH_UOM,
			FILL_QTY,
			FILL_QTY_UOM
	from AUSP__XBI_L2;
