/* Staging table preparation */

	/* drop table if exists mara__xbi_hash_stg
	create table mara__xbi_hash_stg */

	truncate table mara__xbi_hash_stg;
	insert into mara__xbi_hash_stg
	 (
		MARA_MATNR_HSH,
		MARA_MATNR_LDTS,
		MARA_MATNR_RSRC,
		MARA_MATNR_ALL_HSH_DIFF,
		MARA_MATNR_DATE_HSH_DIFF,
		MARA_MATNR_NUMBER_HSH_DIFF,
		MARA_MATNR_TEXT_HSH_DIFF,
		MARA_MATNR,
		MARA_BRGEW,
		MARA_EAN11,
		MARA_ERNAM,
		MARA_EXTWG,
		MARA_GROES,
		MARA_KZREV,
		MARA_LAEDA,
		MARA_MATKL,
		MARA_MEINS,
		MARA_MFRPN,
		MARA_MHDRZ,
		MARA_MSTAV,
		MARA_MTART,
		MARA_NTGEW,
		MARA_PSTAT,
		MARA_RAUBE,
		MARA_SLED_BBD,
		MARA_STOFF,
		MARA_TRAGR,
		MARA_VPSTA,
		MARA_WRKST,
		MARA_BISMT,
		MARA_BSTME,
		MARA_EKWSL,
		MARA_ERSDA,
		MARA_GEWEI,
		MARA_IPRKZ,
		MARA_LABOR,
		MARA_LVORM,
		MARA_MFRNR,
		MARA_MHDHB,
		MARA_MSTAE,
		MARA_MSTDE,
		MARA_PRDHA,
		MARA_QMPUR,
		MARA_RDMHD,
		MARA_SPART,
		MARA_TEMPB,
		MARA_VOLUM,
		MARA_WESCH,
		MARA_MTPOS_MARA)
	select 	cast(UPPER(HASH_MD5(MARA_MATNR)) as varchar(32)) as MARA_MATNR_HSH,
	       	current_timestamp as MARA_MATNR_LDTS,
	       	cast('ECC: MARA' as varchar(50)) as MARA_MATNR_RSRC,
			cast(UPPER(HASH_MD5(upper(MARA_MATNR || '|^|' ||
										IFNULL(MARA_BRGEW,0.00) || '|^|' ||
			                            IFNULL(MARA_EAN11,'') || '|^|' ||
			                            IFNULL(MARA_ERNAM,'') || '|^|' ||
			                            IFNULL(MARA_EXTWG,'') || '|^|' ||
			                            IFNULL(MARA_GROES,'') || '|^|' ||
			                            IFNULL(MARA_KZREV,'') || '|^|' ||
			                            IFNULL(MARA_LAEDA,'0001-01-01') || '|^|' ||
			                            IFNULL(MARA_MATKL,'') || '|^|' ||
			                            IFNULL(MARA_MEINS,'') || '|^|' ||
			                            IFNULL(MARA_MFRPN,'') || '|^|' ||
			                            IFNULL(MARA_MHDRZ,0.00) || '|^|' ||
			                            IFNULL(MARA_MSTAV,'') || '|^|' ||
			                            IFNULL(MARA_MTART,'') || '|^|' ||
			                            IFNULL(MARA_NTGEW,0.00) || '|^|' ||
			                            IFNULL(MARA_PSTAT,'') || '|^|' ||
			                            IFNULL(MARA_RAUBE,'') || '|^|' ||
			                            IFNULL(MARA_SLED_BBD,'') || '|^|' ||
			                            IFNULL(MARA_STOFF,'') || '|^|' ||
			                            IFNULL(MARA_TRAGR,'') || '|^|' ||
			                            IFNULL(MARA_VPSTA,'') || '|^|' ||
			                            IFNULL(MARA_WRKST,'') || '|^|' ||
			                            IFNULL(MARA_BISMT,'') || '|^|' ||
			                            IFNULL(MARA_BSTME,'') || '|^|' ||
			                            IFNULL(MARA_EKWSL,'') || '|^|' ||
			                            IFNULL(MARA_ERSDA,'0001-01-01') || '|^|' ||
			                            IFNULL(MARA_GEWEI,'') || '|^|' ||
			                            IFNULL(MARA_IPRKZ,'') || '|^|' ||
			                            IFNULL(MARA_LABOR,'') || '|^|' ||
			                            IFNULL(MARA_LVORM,'') || '|^|' ||
			                            IFNULL(MARA_MFRNR,'') || '|^|' ||
			                            IFNULL(MARA_MHDHB,0.00) || '|^|' ||
			                            IFNULL(MARA_MSTAE,'') || '|^|' ||
			                            IFNULL(MARA_MSTDE,'0001-01-01') || '|^|' ||
			                            IFNULL(MARA_PRDHA,'') || '|^|' ||
			                            IFNULL(MARA_QMPUR,'') || '|^|' ||
			                            IFNULL(MARA_RDMHD,'') || '|^|' ||
			                            IFNULL(MARA_SPART,'') || '|^|' ||
			                            IFNULL(MARA_TEMPB,'') || '|^|' ||
			                            IFNULL(MARA_VOLUM,0.00) || '|^|' ||
			                            IFNULL(MARA_WESCH,0.00) || '|^|' ||
			                            IFNULL(MARA_MTPOS_MARA,'')
				)))as varchar(32)) as MARA_MATNR_ALL_HSH_DIFF,
			cast(UPPER(HASH_MD5(upper(MARA_MATNR || '|^|' ||
									 	IFNULL(MARA_LAEDA,'0001-01-01') || '|^|' ||
										IFNULL(MARA_ERSDA,'0001-01-01') || '|^|' ||
										IFNULL(MARA_MSTDE,'0001-01-01')
				)))as varchar(32)) as MARA_MATNR_DATE_HSH_DIFF,
			cast(UPPER(HASH_MD5(upper(MARA_MATNR || '|^|' ||
										IFNULL(MARA_BRGEW,0.00) || '|^|' ||
										IFNULL(MARA_MHDRZ,0.00) || '|^|' ||
										IFNULL(MARA_NTGEW,0.00) || '|^|' ||
										IFNULL(MARA_MHDHB,0.00) || '|^|' ||
										IFNULL(MARA_VOLUM,0.00) || '|^|' ||
										IFNULL(MARA_WESCH,0.00)
				)))as varchar(32)) as MARA_MATNR_NUMBER_HSH_DIFF,
			cast(UPPER(HASH_MD5(upper(MARA_MATNR || '|^|' ||
			                            IFNULL(MARA_EAN11,'') || '|^|' ||
			                            IFNULL(MARA_ERNAM,'') || '|^|' ||
			                            IFNULL(MARA_EXTWG,'') || '|^|' ||
			                            IFNULL(MARA_GROES,'') || '|^|' ||
			                            IFNULL(MARA_KZREV,'') || '|^|' ||
			                            IFNULL(MARA_MATKL,'') || '|^|' ||
			                            IFNULL(MARA_MEINS,'') || '|^|' ||
			                            IFNULL(MARA_MFRPN,'') || '|^|' ||
			                            IFNULL(MARA_MSTAV,'') || '|^|' ||
			                            IFNULL(MARA_MTART,'') || '|^|' ||
			                            IFNULL(MARA_PSTAT,'') || '|^|' ||
			                            IFNULL(MARA_RAUBE,'') || '|^|' ||
			                            IFNULL(MARA_SLED_BBD,'') || '|^|' ||
			                            IFNULL(MARA_STOFF,'') || '|^|' ||
			                            IFNULL(MARA_TRAGR,'') || '|^|' ||
			                            IFNULL(MARA_VPSTA,'') || '|^|' ||
			                            IFNULL(MARA_WRKST,'') || '|^|' ||
			                            IFNULL(MARA_BISMT,'') || '|^|' ||
			                            IFNULL(MARA_BSTME,'') || '|^|' ||
			                            IFNULL(MARA_EKWSL,'') || '|^|' ||
			                            IFNULL(MARA_GEWEI,'') || '|^|' ||
			                            IFNULL(MARA_IPRKZ,'') || '|^|' ||
			                            IFNULL(MARA_LABOR,'') || '|^|' ||
			                            IFNULL(MARA_LVORM,'') || '|^|' ||
			                            IFNULL(MARA_MFRNR,'') || '|^|' ||
			                            IFNULL(MARA_MSTAE,'') || '|^|' ||
			                            IFNULL(MARA_PRDHA,'') || '|^|' ||
			                            IFNULL(MARA_QMPUR,'') || '|^|' ||
			                            IFNULL(MARA_RDMHD,'') || '|^|' ||
			                            IFNULL(MARA_SPART,'') || '|^|' ||
			                            IFNULL(MARA_TEMPB,'') || '|^|' ||
			                            IFNULL(MARA_MTPOS_MARA,'')
				)))as varchar(32)) as MARA_MATNR_TEXT_HSH_DIFF,
	        MARA_MATNR,
	        MARA_BRGEW,
	        MARA_EAN11,
	        MARA_ERNAM,
	        MARA_EXTWG,
	        MARA_GROES,
	        MARA_KZREV,
	        MARA_LAEDA,
	        MARA_MATKL,
	        MARA_MEINS,
	        MARA_MFRPN,
	        MARA_MHDRZ,
	        MARA_MSTAV,
	        MARA_MTART,
	        MARA_NTGEW,
	        MARA_PSTAT,
	        MARA_RAUBE,
	        MARA_SLED_BBD,
	        MARA_STOFF,
	        MARA_TRAGR,
	        MARA_VPSTA,
	        MARA_WRKST,
	        MARA_BISMT,
	        MARA_BSTME,
	        MARA_EKWSL,
	        MARA_ERSDA,
	        MARA_GEWEI,
	        MARA_IPRKZ,
	        MARA_LABOR,
	        MARA_LVORM,
	        MARA_MFRNR,
	        MARA_MHDHB,
	        MARA_MSTAE,
	        MARA_MSTDE,
	        MARA_PRDHA,
	        MARA_QMPUR,
	        MARA_RDMHD,
	        MARA_SPART,
	        MARA_TEMPB,
	        MARA_VOLUM,
	        MARA_WESCH,
	        MARA_MTPOS_MARA
	from mara__xbi;
