/* HUB Material table populating */

	/* create table hub_material (MARA_MATNR_HSH varchar(32),
							   MARA_MATNR_LDTS timestamp,
							   MARA_MATNR_RSRC varchar(50),
							   MARA_MATNR varchar(18)
							  ) */

	insert into hub_material (MARA_MATNR_HSH, MARA_MATNR_LDTS, MARA_MATNR_RSRC, MARA_MATNR)
	select DISTINCT st1.MARA_MATNR_HSH,
	       st1.MARA_MATNR_LDTS,
	       st1.MARA_MATNR_RSRC,
	       st1.MARA_MATNR
	from mara__xbi_hash_stg as st1
	where st1.MARA_MATNR_HSH not in (
                                   	 select MARA_MATNR_HSH
                                   	 from hub_material
                                    );
