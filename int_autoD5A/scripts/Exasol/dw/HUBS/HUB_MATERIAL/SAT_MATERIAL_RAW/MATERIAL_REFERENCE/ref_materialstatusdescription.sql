/* 	Reference table for MARA_MSTAE based on T141T table
	SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
    /*
    DROP TABLE IF EXISTS ref_materialstatusdescription
    CREATE TABLE ref_materialstatusdescription
    (
        MARA_MSTAE VARCHAR(2) -- Cross Plant Material Status
        ,T141T_SPRAS VARCHAR(1)
        ,T141T_MTSTB VARCHAR(25) -- Description of Material Status
    ) */

    MERGE INTO ref_materialstatusdescription dst
    USING (SELECT T141T_MMSTA, T141T_SPRAS, T141T_MTSTB FROM T141T__XBI) src
    ON ( dst.MARA_MSTAE = src.T141T_MMSTA
            AND dst.T141T_SPRAS = src.T141T_SPRAS)
    WHEN MATCHED THEN  UPDATE SET dst.T141T_MTSTB = src.T141T_MTSTB
    WHEN NOT MATCHED THEN INSERT (MARA_MSTAE, T141T_SPRAS, T141T_MTSTB) VALUES (src.T141T_MMSTA, src.T141T_SPRAS, src.T141T_MTSTB);
