/* 	Reference table for MARA_PRDHA based on T179T table
	SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
    /*
    DROP TABLE IF EXISTS ref_producthierarchydescription
    CREATE TABLE ref_producthierarchydescription
    (
        MARA_PRDHA VARCHAR(18) -- Product Hierarchy
        ,T179T_SPRAS VARCHAR(1)
        ,T179T_VTEXT VARCHAR(40) -- Description
    ) */

    MERGE INTO ref_producthierarchydescription dst
    USING (SELECT T179T_PRODH, T179T_SPRAS, T179T_VTEXT FROM T179T__XBI) src
    ON ( dst.MARA_PRDHA = src.T179T_PRODH
            AND dst.T179T_SPRAS = src.T179T_SPRAS)
    WHEN MATCHED THEN UPDATE SET dst.T179T_VTEXT = src.T179T_VTEXT
    WHEN NOT MATCHED THEN INSERT (MARA_PRDHA, T179T_SPRAS, T179T_VTEXT) VALUES (src.T179T_PRODH, src.T179T_SPRAS, src.T179T_VTEXT);
