/* 	Reference table for MARA_TEMPB based on T143T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_descriptiontemperatureconditions
   CREATE TABLE ref_descriptiontemperatureconditions
   (
       MARA_TEMPB VARCHAR(4), --  Code
       T143T_TBTXT VARCHAR(20) UTF8, -- Desc
       T143T_SPRAS VARCHAR(1) UTF8

   ) */

   MERGE INTO ref_descriptiontemperatureconditions dst
   USING (SELECT T143T_TEMPB, T143T_TBTXT, T143T_SPRAS FROM T143T__XBI WHERE T143T_TEMPB IS NOT NULL) src
   ON ( dst.MARA_TEMPB = src.T143T_TEMPB
        AND dst.T143T_SPRAS = src.T143T_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.T143T_TBTXT = src.T143T_TBTXT
   WHEN NOT MATCHED THEN INSERT (MARA_TEMPB, T143T_TBTXT, T143T_SPRAS) VALUES (src.T143T_TEMPB, src.T143T_TBTXT, src.T143T_SPRAS);
