/* 	Reference table for MARA_MSTAV based on TVMST table
	SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
  /*
  DROP TABLE IF EXISTS ref_crossdmaterialstatusdescription
  CREATE TABLE ref_crossdmaterialstatusdescription
  (
      MARA_MSTAV VARCHAR(2) -- Cross-distribution-chain material status
      ,TVMST_SPRAS VARCHAR(1)
      ,TVMST_VMSTB VARCHAR(20) -- Description
  ) */

  MERGE INTO ref_crossdmaterialstatusdescription dst
  USING (SELECT TVMST_VMSTA, TVMST_SPRAS, TVMST_VMSTB FROM TVMST__XBI) src
  ON ( dst.MARA_MSTAV = src.TVMST_VMSTA
          AND dst.TVMST_SPRAS = src.TVMST_SPRAS)
  WHEN MATCHED THEN  UPDATE SET dst.TVMST_VMSTB = src.TVMST_VMSTB
  WHEN NOT MATCHED THEN INSERT (MARA_MSTAV, TVMST_SPRAS, TVMST_VMSTB) VALUES (src.TVMST_VMSTA, src.TVMST_SPRAS, src.TVMST_VMSTB);
