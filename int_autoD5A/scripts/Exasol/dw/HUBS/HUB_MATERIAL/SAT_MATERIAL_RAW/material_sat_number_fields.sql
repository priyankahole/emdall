/* ---------------------------------------------------------------------------- */

/* SAT Material Number table population */

	/*
	create table sat_material_number (
			MARA_MATNR_HSH varchar(32),
			MARA_MATNR_LDTS timestamp,
			MARA_MATNR_LEDTS timestamp,
			MARA_MATNR_RSRC varchar(50),
			MARA_MATNR_NUMBER_HSH_DIFF varchar(32),
			MARA_BRGEW decimal(18,5),
			MARA_MHDRZ decimal(18,5),
			MARA_NTGEW decimal(18,5),
			MARA_MHDHB decimal(18,5),
			MARA_VOLUM decimal(18,5),
			MARA_WESCH decimal(18,5)
			) */

	/* insert new records and changed ones */
	insert into sat_material_number (MARA_MATNR_HSH, MARA_MATNR_LDTS, MARA_MATNR_RSRC, MARA_MATNR_NUMBER_HSH_DIFF, MARA_BRGEW, MARA_MHDRZ, MARA_NTGEW, MARA_MHDHB, MARA_VOLUM, MARA_WESCH)
	select st1.MARA_MATNR_HSH,
	       st1.MARA_MATNR_LDTS,
	       st1.MARA_MATNR_RSRC,
	       st1.MARA_MATNR_NUMBER_HSH_DIFF,
	       st1.MARA_BRGEW,
	       st1.MARA_MHDRZ,
	       st1.MARA_NTGEW,
	       st1.MARA_MHDHB,
	       st1.MARA_VOLUM,
	       st1.MARA_WESCH
	from mara__xbi_hash_stg as st1
	      left outer join sat_material_number as s1
	                   on     st1.MARA_MATNR_HSH = s1.MARA_MATNR_HSH
	                      and s1.MARA_MATNR_LEDTS is NULL
	where (
	        s1.MARA_MATNR_HSH is null OR
	        (
	              st1.MARA_MATNR_NUMBER_HSH_DIFF != s1.MARA_MATNR_NUMBER_HSH_DIFF
	          AND s1.MARA_MATNR_LDTS = (
                                        select max(z.MARA_MATNR_LDTS)
                                        from sat_material_number z
                                        where s1.MARA_MATNR_HSH = z.MARA_MATNR_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s2
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s2;
	insert into upd_sat_end_date_loop_s2 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.MARA_MATNR_HSH,
	       s1.MARA_MATNR_LDTS
	from sat_material_number as s1
	where (
	            s1.MARA_MATNR_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.MARA_MATNR_HSH)
	                     from sat_material_number as y
	                     where   y.MARA_MATNR_HSH = s1.MARA_MATNR_HSH
	                         AND y.MARA_MATNR_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s2
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s2;
	insert into upd_sat_end_date_cross_s2 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s2 t1
					inner join upd_sat_end_date_loop_s2 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_material_number dst
	   set dst.MARA_MATNR_LEDTS = src.BUSKEY_LEDTS
	  from sat_material_number dst
				inner join upd_sat_end_date_cross_s2 src on dst.MARA_MATNR_HSH = src.BUSKEY_HSH
	      												and dst.MARA_MATNR_LDTS = src.BUSKEY_LDTS;
