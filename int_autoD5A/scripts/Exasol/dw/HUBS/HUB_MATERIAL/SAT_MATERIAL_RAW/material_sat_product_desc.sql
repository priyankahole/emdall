/* SAT Material Description table population */
	/*
	create table sat_material_description (
    MAKT_MATNR_HSH VARCHAR(32) UTF8,
    MAKT_MATNR_LDTS TIMESTAMP,
    MAKT_MATNR_LEDTS TIMESTAMP,
    MAKT_MATNR_RSRC varchar(50) UTF8,
    MAKT_MATNR_SPRAS_HSH varchar(32) UTF8,
    MAKT_MATNR_ALL_HSH_DIFF varchar(32) UTF8,
    MAKT_SPRAS VARCHAR(1) UTF8,
    MAKT_MAKTX VARCHAR(80) UTF8,
    MAKT_MAKTG VARCHAR(80) UTF8
)*/
	/* insert new records and changed ones */
insert into sat_material_description (
    MAKT_MATNR_HSH,
    MAKT_MATNR_LDTS,
    MAKT_MATNR_RSRC,
    MAKT_MATNR_SPRAS_HSH,
    MAKT_MATNR_ALL_HSH_DIFF,
    MAKT_SPRAS,
    MAKT_MAKTX,
    MAKT_MAKTG)
select
    st1.MAKT_MATNR_HSH,
    st1.MAKT_MATNR_LDTS,
    st1.MAKT_MATNR_RSRC,
    st1.MAKT_MATNR_SPRAS_HSH,
    st1.MAKT_MATNR_ALL_HSH_DIFF,
    st1.MAKT_SPRAS,
    st1.MAKT_MAKTX,
    st1.MAKT_MAKTG
	from makt__xbi_hash_stg as st1
	      left outer join sat_material_description as s1
	                   on     st1.MAKT_MATNR_SPRAS_HSH = s1.MAKT_MATNR_SPRAS_HSH
	                      and s1.MAKT_MATNR_LEDTS is NULL
	where (
	        s1.MAKT_MATNR_SPRAS_HSH is null OR
	        (
	              st1.MAKT_MATNR_ALL_HSH_DIFF != s1.MAKT_MATNR_ALL_HSH_DIFF
	          AND s1.MAKT_MATNR_LDTS = (
                                        select max(z.MAKT_MATNR_LDTS)
                                        from sat_material_description z
                                        where s1.MAKT_MATNR_SPRAS_HSH = z.MAKT_MATNR_SPRAS_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s6
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s6;
	insert into upd_sat_end_date_loop_s6 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.MAKT_MATNR_SPRAS_HSH,
	       s1.MAKT_MATNR_LDTS
	from sat_material_description as s1
	where (
	            s1.MAKT_MATNR_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.MAKT_MATNR_SPRAS_HSH)
	                     from sat_material_description as y
	                     where   y.MAKT_MATNR_SPRAS_HSH = s1.MAKT_MATNR_SPRAS_HSH
	                         AND y.MAKT_MATNR_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s6
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s6;
	insert into upd_sat_end_date_cross_s6 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s6 t1
					inner join upd_sat_end_date_loop_s6 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_material_description dst
	   set dst.MAKT_MATNR_LEDTS = src.BUSKEY_LEDTS
	  from sat_material_description dst
				inner join upd_sat_end_date_cross_s6 src on dst.MAKT_MATNR_SPRAS_HSH = src.BUSKEY_HSH
	      												and dst.MAKT_MATNR_LDTS = src.BUSKEY_LDTS;
