/* create a placeholder for grouping related objects */

create or replace view fact_dw_holder_material_plant as
	select cast(1 as bigint) as dim_dw_link_material_plantid,
				 cast(1 as bigint) as DIM_DW_SAT_MATERIAL_PLANT_TEXTID,
				 cast(1 as bigint) as DIM_DW_SAT_MATERIAL_PLANT_NUMBERID,
				 cast(1 as bigint) as dim_dw_ref_consumptionmodedescrid,
				 cast(1 as bigint) as dim_dw_ref_materialdiscontinuationflagdescrid,
				 cast(1 as bigint) as dim_dw_ref_mrpcontrollerdescrid,
				 cast(1 as bigint) as dim_dw_ref_mrpgroupdescrid,
				 cast(1 as bigint) as dim_dw_ref_mrplotsizedescrid,
				 cast(1 as bigint) as dim_dw_ref_mrplotsizestextsid,
				 cast(1 as bigint) as dim_dw_ref_mrpprofiledescrid,
				 cast(1 as bigint) as dim_dw_ref_mrptypedescrid,
				 cast(1 as bigint) as dim_dw_ref_plantmaterialstatusdescrid,
				 cast(1 as bigint) as dim_dw_ref_procurementtypedescrid,
				 cast(1 as bigint) as dim_dw_ref_profitcenternameid,
				 cast(1 as bigint) as dim_dw_ref_purchasegroupdescrid,
				 cast(1 as bigint) as dim_dw_ref_rangeofcoverageprofileid,
				 cast(1 as bigint) as dim_dw_ref_specialprocurementdescrid,
				 cast(1 as bigint) as dim_dw_ref_unitofissuecommercialid,
				 cast(1 as bigint) as dim_material_plant__xbiid;
