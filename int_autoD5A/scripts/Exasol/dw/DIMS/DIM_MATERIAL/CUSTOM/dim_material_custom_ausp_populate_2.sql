/* adding AUSP fields in material dimension
	alter table dim_material__xbi drop column DD_CONCENTRATION
	alter table dim_material__xbi drop column DD_CONCENTRATIONUOM 
	alter table dim_material__xbi drop column DD_CONTENTS
	alter table dim_material__xbi drop column DD_CONTENTSUOM
	alter table dim_material__xbi drop column DD_DOMINANTSPECIES
	alter table dim_material__xbi drop column DD_ITEMSUBTYPE
	alter table dim_material__xbi drop column DD_ITEMTYPE
	alter table dim_material__xbi drop column DD_PRIMARYSITE
	alter table dim_material__xbi drop column DD_PRODUCTGROUP
	alter table dim_material__xbi drop column DD_PRODUCTTYPE
	alter table dim_material__xbi drop column DD_STRENGTH
	alter table dim_material__xbi drop column DD_STRENGTH_UOM
	alter table dim_material__xbi drop column CT_FILL_QTY
	alter table dim_material__xbi drop column DD_FILL_QTY_UOM
	
	alter table dim_material__xbi add column DD_CONCENTRATION VARCHAR(100) default 'Not Set'
	alter table dim_material__xbi add column DD_CONCENTRATIONUOM VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_CONTENTS VARCHAR(100) default 'Not Set'
	alter table dim_material__xbi add column DD_CONTENTSUOM VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_DOMINANTSPECIES VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_ITEMSUBTYPE VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_ITEMTYPE VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_PRIMARYSITE VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_PRODUCTGROUP VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_PRODUCTTYPE VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_STRENGTH VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column DD_STRENGTH_UOM VARCHAR(30) default 'Not Set'
	alter table dim_material__xbi add column CT_FILL_QTY DECIMAL(18,4) default 0.00
	alter table dim_material__xbi add column DD_FILL_QTY_UOM VARCHAR(30) default 'Not Set'
*/

merge into dim_material__xbi trg
using (select *
	   from SAT_MATERIAL_AUSP_ALL
	   where AUSP_OBJEK_LEDTS is null) src on trg.dim_material__xbiid = src.AUSP_OBJEK_HSH
when matched then update set 
						trg.dd_CONCENTRATION 	= ifnull(src.CONCENTRATION, 'Not Set'),
						trg.dd_CONCENTRATIONUOM = ifnull(src.CONCENTRATIONUOM, 'Not Set'), 
						trg.dd_CONTENTS 		= ifnull(src.CONTENTS, 'Not Set'),
						trg.dd_CONTENTSUOM 		= ifnull(src.CONTENTSUOM, 'Not Set'),
						trg.dd_DOMINANTSPECIES 	= ifnull(src.DOMINANTSPECIES, 'Not Set'),
						trg.dd_ITEMSUBTYPE 		= ifnull(src.ITEMSUBTYPE, 'Not Set'),
						trg.dd_ITEMTYPE 		= ifnull(src.ITEMTYPE, 'Not Set'),
						trg.dd_PRIMARYSITE 		= ifnull(src.PRIMARYSITE, 'Not Set'),
						trg.dd_PRODUCTGROUP 	= ifnull(src.PRODUCTGROUP, 'Not Set'),
						trg.dd_PRODUCTTYPE 		= ifnull(src.PRODUCTTYPE, 'Not Set'),
						trg.dd_STRENGTH 		= ifnull(src.STRENGTH, 'Not Set'),
						trg.dd_STRENGTH_UOM 	= ifnull(src.STRENGTH_UOM, 'Not Set'),
						trg.ct_fill_qty 		= ifnull(src.FILL_QTY, 0.00),
						trg.dd_FILL_QTY_UOM 	= ifnull(src.FILL_QTY_UOM, 'Not Set');
