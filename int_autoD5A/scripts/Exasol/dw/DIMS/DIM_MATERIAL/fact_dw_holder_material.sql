/* create a placeholder for grouping related objects */

create or replace view fact_dw_holder_material as
	select 	cast(1 as bigint) as dim_dw_hub_materialid,
					cast(1 as bigint) as dim_dw_ref_crossdmaterialstatusdescriptionid,
					cast(1 as bigint) as dim_dw_ref_divisionid,
					cast(1 as bigint) as dim_dw_ref_externalmaterialgroupid,
					cast(1 as bigint) as dim_dw_ref_itemsubtypedescriptionid,
					cast(1 as bigint) as dim_dw_ref_itemtypedescriptionid,
					cast(1 as bigint) as dim_dw_ref_materialgroupdescriptionid,
					cast(1 as bigint) as dim_dw_ref_materialstatusdescriptionid,
					cast(1 as bigint) as dim_dw_ref_parttypedescriptionid,
					cast(1 as bigint) as dim_dw_ref_producthierarchydescriptionid,
					cast(1 as bigint) as dim_dw_ref_storageconditionsid,
					cast(1 as bigint) as dim_dw_ref_descriptiontemperatureconditionsid,
					cast(1 as bigint) as dim_dw_sat_material_ausp_allid,
					cast(1 as bigint) as dim_dw_sat_material_dateid,
					cast(1 as bigint) as dim_dw_sat_material_descriptionid,
					cast(1 as bigint) as dim_dw_sat_material_numberid,
					cast(1 as bigint) as dim_dw_sat_material_textid,
					cast(1 as bigint) as dim_material__xbiid;
