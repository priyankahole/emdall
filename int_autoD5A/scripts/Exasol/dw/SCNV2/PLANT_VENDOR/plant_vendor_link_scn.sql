drop table if exists stg_scn_plant_vendor_purchase1;
create table stg_scn_plant_vendor_purchase1 as
SELECT 
prt.plant,
dv.vendornumber
,ROUND(SUM((case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 
when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 
then 0.0000 else f_deliv.amt_UnitPrice*(f_deliv.ct_DeliveryQty-f_deliv.ct_ReceivedQty) end) * 
f_deliv.amt_ExchangeRate_GBL),2) ct_open_deliv_amt
FROM fact_purchase AS f_deliv 
INNER JOIN Dim_Date AS dtdel ON f_deliv.Dim_DateidDelivery = dtdel.Dim_Dateid  
                             AND (dtdel.DateValue BETWEEN current_date AND current_date + 6)  
INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid  AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))  
INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid 
INNER JOIN dim_vendor as dv on f_deliv.dim_vendorid = dv.dim_vendorid
group by prt.plant, dv.vendornumber
;


drop table if exists stg_scn_plant_vendor_purchase2;
create table stg_scn_plant_vendor_purchase2 as
SELECT 
prt.plant,
dv.vendornumber,
ROUND(SUM((case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 
when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 
then 0.0000 else f_deliv.amt_UnitPrice*(f_deliv.ct_DeliveryQty-f_deliv.ct_ReceivedQty) end) * f_deliv.amt_ExchangeRate_GBL),2) ct_late_amt
FROM fact_purchase AS f_deliv 
INNER JOIN Dim_Part AS prt ON f_deliv.Dim_Partid = prt.Dim_Partid  AND (lower(prt.PartNumber_NoLeadZero) != lower('Not Set'))  
INNER JOIN dim_purchasemisc AS atrb ON f_deliv.Dim_PurchaseMiscid = atrb.dim_purchasemiscid  
INNER JOIN Dim_Date AS dtdel ON f_deliv.Dim_DateidDelivery = dtdel.Dim_Dateid  
INNER JOIN dim_vendor as dv on f_deliv.dim_vendorid = dv.dim_vendorid
WHERE  ((case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' 
then 0.0000 when (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) < 0 then 0.0000 else (f_deliv.ct_DeliveryQty - f_deliv.ct_ReceivedQty) end>=1))  
AND (((CASE WHEN f_deliv.Dim_DateidDelivery > 1 THEN  (dtdel.DateValue - current_date)  ELSE 0.0000  END)<=-1)) 
group by prt.plant, dv.vendornumber
;




drop table if exists stg_scn_plant_vendor_unique;
			create table stg_scn_plant_vendor_unique as
			select PLANT, VENDORNUMBER from stg_scn_plant_vendor_purchase1
			union
			select PLANT, VENDORNUMBER from stg_scn_plant_vendor_purchase2;

			drop table if exists stg_scn_plant_vendor;
			create table stg_scn_plant_vendor as
			select  t0.PLANT, t0.VENDORNUMBER,
					cast(ifnull(t1.ct_open_deliv_amt, 0.00) as decimal(18,5)) as ct_open_deliv_amt,
					cast(ifnull(t2.ct_late_amt, 0.00) as decimal(18,5)) as ct_late_amt
			from stg_scn_plant_vendor_unique t0
					left join stg_scn_plant_vendor_purchase1 t1 on t0.plant = t1.plant and t0.VENDORNUMBER = t1.VENDORNUMBER
					left join stg_scn_plant_vendor_purchase2 t2 on t0.plant = t2.plant and t0.VENDORNUMBER = t2.VENDORNUMBER;

			drop table if exists stg_scn_plant_vendor_hash;
			create table stg_scn_plant_vendor_hash as
			select 	cast(UPPER(HASH_MD5(PLANT || '|^|' || VENDORNUMBER)) as varchar(32)) 	as PLANT_VENDOR_HSH,
					cast(UPPER(HASH_MD5(PLANT)) as varchar(32)) 							as PLANT_HSH,
					cast(UPPER(HASH_MD5(VENDORNUMBER)) as varchar(32)) 							as VENDOR_HSH,
					current_timestamp 				 				 							as PLANT_VENDOR_LDTS,
					cast('AERA BV: Plant-Vendor Purchase' as varchar(50))  	 				as PLANT_VENDOR_RSRC,
					cast(UPPER(HASH_MD5(upper(PLANT 								|| '|^|' ||
											  VENDORNUMBER								|| '|^|' ||
											  IFNULL(CT_OPEN_DELIV_AMT, 0.0) 	|| '|^|' ||
											  IFNULL(CT_LATE_AMT, 0.0)
							)))as varchar(32)) 						 							as PLANT_VENDOR_SCN_HSH_DIFF,
					PLANT,
					VENDORNUMBER,
					CT_OPEN_DELIV_AMT,
					CT_LATE_AMT
			from stg_scn_plant_vendor;

	/* ********************* END: Unifying measures for Plant-Vendor ********************* */

-- >>

/* ********************* START: Plant-Vendor SCN measures populating ********************* */

	/* LINK Plant-Vendor table populating */

	/* drop table if exists link_plant_vendor
	   create table link_plant_vendor ( PLANT_VENDOR_HSH varchar(32),
								   			 PLANT_VENDOR_LDTS timestamp,
								   			 PLANT_VENDOR_RSRC varchar(50),
								   			 PLANT_HSH varchar(32),
											 VENDOR_HSH 	varchar(32)
								  ) */

	insert into link_plant_vendor (PLANT_VENDOR_HSH, PLANT_VENDOR_LDTS, PLANT_VENDOR_RSRC, PLANT_HSH, VENDOR_HSH)
	select distinct st1.PLANT_VENDOR_HSH,
	       st1.PLANT_VENDOR_LDTS,
	       st1.PLANT_VENDOR_RSRC,
	       st1.PLANT_HSH,
		   st1.VENDOR_HSH
	from stg_scn_plant_vendor_hash as st1
	where not exists (
                      select 1
                      from link_plant_vendor l1
					  where     st1.PLANT_HSH = l1.PLANT_HSH
							and st1.VENDOR_HSH = l1.VENDOR_HSH
                      );


	/* SAT Plant-Vendor SCN table population */

	 	/* drop table if exists sat_plant_vendor_scn
		create table sat_plant_vendor_scn (
												PLANT_VENDOR_HSH 		varchar(32),
												PLANT_VENDOR_LDTS 		timestamp,
												PLANT_VENDOR_LEDTS 	timestamp,
												PLANT_VENDOR_RSRC 		varchar(50),
												PLANT_VENDOR_SCN_HSH_DIFF varchar(32),
												CT_OPEN_DELIV_AMT decimal(18,5),
												CT_LATE_AMT decimal(18,5)
												) */

	/* insert into sat dummy record in order to avoid left joins in future */ 
	insert into sat_plant_vendor_scn (PLANT_VENDOR_HSH, PLANT_VENDOR_LDTS, PLANT_VENDOR_LEDTS, PLANT_VENDOR_RSRC, PLANT_VENDOR_SCN_HSH_DIFF, 
										 CT_OPEN_DELIV_AMT, CT_LATE_AMT)
	select  '00000000000000000000000000000000' as PLANT_VENDOR_HSH,
			'0001-01-01' as PLANT_VENDOR_LDTS,
			'9999-12-31' as PLANT_VENDOR_LEDTS,
			'SYSTEM'	 as PLANT_VENDOR_RSRC,
			'00000000000000000000000000000000' as PLANT_VENDOR_SCN_HSH_DIFF,
			0.00 as CT_OPEN_DELIV_AMT, 
			0.00 as CT_LATE_AMT
	from dual
	where not exists (select 1 from sat_plant_vendor_scn where PLANT_VENDOR_HSH = '00000000000000000000000000000000');

	/* insert new records and changed ones */
	insert into sat_plant_vendor_scn (PLANT_VENDOR_HSH, PLANT_VENDOR_LDTS, PLANT_VENDOR_RSRC, PLANT_VENDOR_SCN_HSH_DIFF, 
										 CT_OPEN_DELIV_AMT, CT_LATE_AMT)
	select 	st1.PLANT_VENDOR_HSH, st1.PLANT_VENDOR_LDTS, st1.PLANT_VENDOR_RSRC, st1.PLANT_VENDOR_SCN_HSH_DIFF, 
			st1.CT_OPEN_DELIV_AMT, st1.CT_LATE_AMT
	from stg_scn_plant_vendor_hash as st1
	      left outer join sat_plant_vendor_scn as s1
	                   on     st1.PLANT_VENDOR_HSH = s1.PLANT_VENDOR_HSH
	                      and s1.PLANT_VENDOR_LEDTS is NULL
	where (
	        s1.PLANT_VENDOR_HSH is null OR
	        (
	              st1.PLANT_VENDOR_SCN_HSH_DIFF != s1.PLANT_VENDOR_SCN_HSH_DIFF
	          AND s1.PLANT_VENDOR_LDTS = (
                                        select max(z.PLANT_VENDOR_LDTS)
                                        from sat_plant_vendor_scn z
                                        where s1.PLANT_VENDOR_HSH = z.PLANT_VENDOR_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s202
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s202;
	insert into upd_sat_end_date_loop_s202 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.PLANT_VENDOR_HSH,
	       s1.PLANT_VENDOR_LDTS
	from sat_plant_vendor_scn as s1
	where (
	            s1.PLANT_VENDOR_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.PLANT_VENDOR_HSH)
	                     from sat_plant_vendor_scn as y
	                     where   y.PLANT_VENDOR_HSH = s1.PLANT_VENDOR_HSH
	                         AND y.PLANT_VENDOR_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s202
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s202;
	insert into upd_sat_end_date_cross_s202 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s202 t1
					inner join upd_sat_end_date_loop_s202 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_plant_vendor_scn dst
	   set dst.PLANT_VENDOR_LEDTS = src.BUSKEY_LEDTS
	  from sat_plant_vendor_scn dst
				inner join upd_sat_end_date_cross_s202 src on dst.PLANT_VENDOR_HSH = src.BUSKEY_HSH
	      												and dst.PLANT_VENDOR_LDTS = src.BUSKEY_LDTS;	

