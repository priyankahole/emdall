/* Build SCN for Plant-Vendor */
		create or replace view fact_scn_plant_vendor as
		select 	t.PLANT_VENDOR_HSH 	as fact_scn_plant_vendorid,
				t.PLANT_VENDOR_RSRC 	as dd_scn_plant_vendor_recordsource,
				t.PLANT_HSH 			as dim_plant__xbiid,
				t.VENDOR_HSH 				as dim_vendor__xbiid,
				CT_OPEN_DELIV_AMT,
				CT_LATE_AMT
		from link_plant_vendor t
				inner join sat_plant_vendor_scn x on t.PLANT_VENDOR_HSH = x.PLANT_VENDOR_HSH
		where x.PLANT_VENDOR_LEDTS is null;