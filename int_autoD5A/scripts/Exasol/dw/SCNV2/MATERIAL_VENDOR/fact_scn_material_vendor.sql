/* Build SCN for Material-Vendor */
		create or replace view fact_scn_material_vendor as
		select 	t.PARTNUMBER_VENDOR_HSH 	as fact_scn_material_vendorid,
				t.PARTNUMBER_VENDOR_RSRC 	as dd_scn_material_vendor_recordsource,
				t.PARTNUMBER_HSH 			as dim_material__xbiid,
				t.VENDOR_HSH 				as dim_vendor__xbiid,
				CT_OPENDELIV_AMT_NEXT7DAYS,
				CT_OPENDELIVSCHEDULE_COUNT,
				CT_OPENDELIV_AMT
		from link_material_vendor t
				inner join sat_material_vendor_scn x on t.PARTNUMBER_VENDOR_HSH = x.PARTNUMBER_VENDOR_HSH
		where x.PARTNUMBER_VENDOR_LEDTS is null;

		/* will use PRODUCT_HSH to join to dim_material__xbi dimension 
			and  VENDOR_HSH to join to dim_vendor__xbi dimension */