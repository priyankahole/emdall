/* Staging tables preparation */

	/* ********************* START: Customer-Plant ********************* */

			-- 1 source Sales1
			drop table if exists stg_scn_customer_plant_sales1;
			create table stg_scn_customer_plant_sales1 as
			SELECT plt.plantcode, cust.customernumber,
			       ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			                       WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
			                       WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
			                                  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                       ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                  ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
			                            END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                  END) * f_so.amt_ExchangeRate_GBL), 0) ct_past_due_amount_cust_plant
			FROM fact_salesorder AS f_so
					INNER JOIN Dim_Date AS sdrdp ON f_so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid
													AND ((sdrdp.DateValue) BETWEEN (add_months(trunc(CURRENT_DATE - INTERVAL '9' MONTH, 'MM'), 1)-1 + INTERVAL '1' DAY) AND (add_months(trunc(CURRENT_DATE, 'MM'), 1)-1))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ItemSubType_Merck) = lower('FPP'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid AND (lower(dch.DistributionChannelName) != lower('Intercompany sales'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
					INNER JOIN dim_overallstatusforcreditcheck AS oscc ON f_so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckid
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
					INNER JOIN dim_salesorderitemcategory AS ic ON f_so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
					INNER JOIN dim_customer AS cust ON f_so.dim_customerid = cust.dim_customerid
					INNER JOIN dim_plant AS plt ON f_so.dim_plantid = plt.dim_plantid
			WHERE (lower((CASE WHEN dc.DocumentCategory ='C'
			                       AND sdt.DocumentType NOT IN ('KA','KB')
			                       AND oscc.OverallStatusforCreditCheck <> 'B'
			                       AND sois.OverallProcessingStatus <> 'Completely processed'
			                       AND sois.OverallDeliveryStatus <>'Completely processed'
			                       AND sorr.RejectReasonCode = 'Not Set'
			                       AND soegidt.DateValue < CURRENT_DATE THEN 'X'
			                  ELSE 'Not Set' END)) = lower('X'))
			  	AND ((CASE  WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
			            	WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
			            	WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC','ZLZC')
								THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                               ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			            	ELSE (CASE 	WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                      		ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END)
			       	  END>=0.1))
			GROUP BY cust.customernumber, plt.plantcode;


			-- 2 source Sales2
			drop table if exists stg_scn_customer_plant_sales2;
			create table stg_scn_customer_plant_sales2 as
			SELECT plt.plantcode, cust.customernumber,
			       ROUND(SUM((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
			                       WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
										THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                                       ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
			                                  END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                      ELSE (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			                                 ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
			                            END * f_so.amt_UnitPriceUoM / (CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE 1 END))
			                  END) * f_so.amt_ExchangeRate_GBL),2) ct_open_orders_amounts_cust_plant
			FROM fact_salesorder AS f_so
					INNER JOIN Dim_Date AS soegidt ON   f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid 
													AND (   soegidt.DateValue BETWEEN (trunc(CURRENT_DATE) - INTERVAL '1' YEAR) AND trunc(CURRENT_DATE)
			     										 OR soegidt.DateValue BETWEEN trunc(CURRENT_DATE,'YEAR') AND trunc(CURRENT_DATE)
													     OR soegidt.DateValue BETWEEN CURRENT_DATE AND (CURRENT_DATE + 364))
					INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid AND (lower(sorr.RejectReasonCode) = lower('Not Set'))
					INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
															AND ((CASE WHEN sdt.DocumentType = 'AF' THEN 'IN'
			          												  WHEN sdt.DocumentType = 'AG' THEN 'QT'
															          WHEN sdt.DocumentType = 'AU' THEN 'SI'
															          WHEN sdt.DocumentType = 'G2' THEN 'CR'
															          WHEN sdt.DocumentType = 'KL' THEN 'FD'
															          WHEN sdt.DocumentType = 'KM' THEN 'CQ'
															          WHEN sdt.DocumentType = 'KN' THEN 'SD'
															          WHEN sdt.DocumentType = 'L2' THEN 'DR'
															          WHEN sdt.DocumentType = 'LP' THEN 'DS'
															          WHEN sdt.DocumentType = 'TA' THEN 'OR'
			          											  ELSE sdt.DocumentType END) NOT IN (('KA'),('KB')))
					INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid AND (lower(dc.DocumentCategory) = lower('C'))
					INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid AND (lower(dch.DistributionChannelCode) != lower('20'))
					INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
																	AND (lower(sois.OverallProcessingStatus) != lower('Completely processed'))
																	AND (lower(sois.OverallDeliveryStatus) != lower('Completely processed'))
					INNER JOIN dim_customer AS cust ON f_so.dim_customerid = cust.dim_customerid
					INNER JOIN dim_plant AS plt ON f_so.dim_plantid = plt.dim_plantid
			WHERE ((CASE WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
			             WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') AND f_so.dd_ItemRelForDelv = 'X' 
							THEN (CASE WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
			                           ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) END)
			             WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
			             ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) END>0 ))
			GROUP BY cust.customernumber, plt.plantcode;


			-- 3 source Sales3
			drop table if exists stg_scn_customer_plant_sales3;
			create table stg_scn_customer_plant_sales3 as
			SELECT plt.plantcode, cust.customernumber,
			       ROUND(((COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' AND f_so.dd_solineotif_merck = 'Y' THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo)
			                                   ELSE 'Not Set' END) 
										- CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' AND f_so.dd_solineotif_merck = 'Y' THEN 0.0000
			                                                          ELSE 1.0000 END) > 0.0000 THEN 1.0000
			                                   ELSE 0.0000 END) 
								/ (CASE WHEN (COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo)
			                                                      ELSE 'Not Set' END)
														 - CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
			                                                                           ELSE 1.0000 END) > 0.0000 THEN 1.0000
			                                                    ELSE 0.0000 END) = 0.0000 THEN 1.0000
			                            ELSE (COUNT(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo)
			                                                      ELSE 'Not Set' END) 
														- CASE WHEN SUM(DISTINCT CASE WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
			                                                                          ELSE 1.0000 END) > 0.0000 THEN 1.0000
			                                                   ELSE 0.0000 END) END) ) * 100, 2) ct_so_lines_perc_cust_plant
			FROM fact_salesorder AS f_so
					INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
													AND (soegidt.DateValue BETWEEN to_date(CONCAT(YEAR(trunc(CURRENT_DATE)),'-01-01'),'YYYY-MM-DD') AND trunc(CURRENT_DATE))
					INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ProductHierarchy) = lower('Not Set'))
					INNER JOIN Dim_Date AS sdd ON f_so.Dim_DateidSchedDelivery = sdd.Dim_Dateid
					INNER JOIN dim_plant AS plt ON f_so.dim_plantid = plt.dim_plantid
					INNER JOIN dim_customer AS cust ON f_so.dim_customerid = cust.dim_customerid
			WHERE (lower(f_so.dd_ItemRelForDelv) = lower('X')) AND (lower(f_so.dd_otifenabledflag) = lower('Y'))
			GROUP BY cust.customernumber, plt.plantcode;

	/* ********************* END: Customer-Plant ********************* */

-- >>

	/* ********************* START: Unifying measures for Customer-Plant ********************* */
			
			drop table if exists stg_scn_customer_plant_unique;
			create table stg_scn_customer_plant_unique as
			select CUSTOMERNUMBER, PLANTCODE from stg_scn_customer_plant_sales1
			union
			select CUSTOMERNUMBER, PLANTCODE from stg_scn_customer_plant_sales2
			union
			select CUSTOMERNUMBER, PLANTCODE from stg_scn_customer_plant_sales3;

			drop table if exists stg_scn_customer_plant;
			create table stg_scn_customer_plant as
			select  t0.PLANTCODE, t0.CUSTOMERNUMBER,
					cast(ifnull(t1.CT_PAST_DUE_AMOUNT_CUST_PLANT, 0.00)     as decimal(18,5)) 	as CT_PAST_DUE_AMOUNT_CUST_PLANT,
					cast(ifnull(t2.CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT, 0.00) as decimal(18,5)) 	as CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT,
					cast(ifnull(t3.CT_SO_LINES_PERC_CUST_PLANT, 0.00)       as decimal(18,5)) 	as CT_SO_LINES_PERC_CUST_PLANT
			from stg_scn_customer_plant_unique t0
					left join stg_scn_customer_plant_sales1 t1 on t0.PLANTCODE = t1.PLANTCODE and t0.CUSTOMERNUMBER = t1.CUSTOMERNUMBER
					left join stg_scn_customer_plant_sales2 t2 on t0.PLANTCODE = t2.PLANTCODE and t0.CUSTOMERNUMBER = t2.CUSTOMERNUMBER
					left join stg_scn_customer_plant_sales3 t3 on t0.PLANTCODE = t3.PLANTCODE and t0.CUSTOMERNUMBER = t3.CUSTOMERNUMBER;

			drop table if exists stg_scn_customer_plant_hash;
			create table stg_scn_customer_plant_hash as
			select 	cast(UPPER(HASH_MD5(CUSTOMERNUMBER || '|^|' || PLANTCODE)) as varchar(32))  as CUSTOMER_PLANT_HSH,
					cast(UPPER(HASH_MD5(PLANTCODE)) as varchar(32)) 							as PLANT_HSH,
					cast(UPPER(HASH_MD5(CUSTOMERNUMBER)) as varchar(32)) 						as CUSTOMER_HSH,
					current_timestamp 				 				 							as CUSTOMER_PLANT_LDTS,
					cast('AERA BV: SCN - Customer-Plant Sales' as varchar(50))  	 			as CUSTOMER_PLANT_RSRC,
					cast(UPPER(HASH_MD5(upper(CUSTOMERNUMBER 								 || '|^|' ||
											  PLANTCODE										 || '|^|' ||
											  IFNULL(CT_PAST_DUE_AMOUNT_CUST_PLANT, 0.0) 	 || '|^|' ||
											  IFNULL(CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT, 0.0) || '|^|' ||
											  IFNULL(CT_SO_LINES_PERC_CUST_PLANT, 0.0) 
							)))as varchar(32)) 						 							as CUSTOMER_PLANT_SCN_HSH_DIFF,
					CUSTOMERNUMBER,
					PLANTCODE,
					CT_PAST_DUE_AMOUNT_CUST_PLANT,
					CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT,
					CT_SO_LINES_PERC_CUST_PLANT
			from stg_scn_customer_plant;

	/* ********************* END: Unifying measures for Material-Customer ********************* */

-- >>

/* ********************* START: Material-Customer SCN measures populating ********************* */

	/* LINK Material-Customer table populating */

	/* drop table if exists link_customer_plant
	   create table link_customer_plant (CUSTOMER_PLANT_HSH 		varchar(32),
								   		 CUSTOMER_PLANT_LDTS 	timestamp,
								   		 CUSTOMER_PLANT_RSRC 	varchar(50),
								   		 CUSTOMER_HSH 	varchar(32),
										 PLANT_HSH 		varchar(32)
								  ) */


	insert into link_customer_plant (CUSTOMER_PLANT_HSH, CUSTOMER_PLANT_LDTS, CUSTOMER_PLANT_RSRC, CUSTOMER_HSH, PLANT_HSH)
	select distinct st1.CUSTOMER_PLANT_HSH,
					st1.CUSTOMER_PLANT_LDTS,
					st1.CUSTOMER_PLANT_RSRC,
					st1.CUSTOMER_HSH,
					st1.PLANT_HSH
	from stg_scn_customer_plant_hash as st1
	where not exists (
                      select 1
                      from link_customer_plant l1
					  where     st1.CUSTOMER_HSH = l1.CUSTOMER_HSH
							and st1.PLANT_HSH = l1.PLANT_HSH
                      );


	/* SAT Material-Customer SCN table population */

	/* 	drop table if exists sat_customer_plant_scn
		create table sat_customer_plant_scn (
												CUSTOMER_PLANT_HSH 			varchar(32),
												CUSTOMER_PLANT_LDTS 		timestamp,
												CUSTOMER_PLANT_LEDTS 		timestamp,
												CUSTOMER_PLANT_RSRC 		varchar(50),
												CUSTOMER_PLANT_SCN_HSH_DIFF varchar(32),
												CT_PAST_DUE_AMOUNT_CUST_PLANT 		decimal(18,5),
												CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT 	decimal(18,5),
												CT_SO_LINES_PERC_CUST_PLANT 		decimal(18,5)
												) */

	/* insert into sat dummy record in order to avoid left joins in future */
	insert into sat_customer_plant_scn (CUSTOMER_PLANT_HSH, CUSTOMER_PLANT_LDTS, CUSTOMER_PLANT_LEDTS, CUSTOMER_PLANT_RSRC, CUSTOMER_PLANT_SCN_HSH_DIFF, 
										CT_PAST_DUE_AMOUNT_CUST_PLANT, CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT, CT_SO_LINES_PERC_CUST_PLANT)
	select  '00000000000000000000000000000000' as CUSTOMER_PLANT_HSH,
			'0001-01-01' as CUSTOMER_PLANT_LDTS,
			'9999-12-31' as CUSTOMER_PLANT_LEDTS,
			'SYSTEM'	 as CUSTOMER_PLANT_RSRC,
			'00000000000000000000000000000000' as CUSTOMER_PLANT_SCN_HSH_DIFF,
			0.00 as CT_PAST_DUE_AMOUNT_CUST_PLANT, 
			0.00 as CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT, 
			0.00 as CT_SO_LINES_PERC_CUST_PLANT
	from dual
	where not exists (select 1 from sat_customer_plant_scn where CUSTOMER_PLANT_HSH = '00000000000000000000000000000000');

	/* insert new records and changed ones */
	insert into sat_customer_plant_scn (CUSTOMER_PLANT_HSH, CUSTOMER_PLANT_LDTS, CUSTOMER_PLANT_RSRC, CUSTOMER_PLANT_SCN_HSH_DIFF, 
										CT_PAST_DUE_AMOUNT_CUST_PLANT, CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT, CT_SO_LINES_PERC_CUST_PLANT)
	select 	st1.CUSTOMER_PLANT_HSH,
			st1.CUSTOMER_PLANT_LDTS,
			st1.CUSTOMER_PLANT_RSRC,
			st1.CUSTOMER_PLANT_SCN_HSH_DIFF,
			st1.CT_PAST_DUE_AMOUNT_CUST_PLANT,
			st1.CT_OPEN_ORDERS_AMOUNTS_CUST_PLANT,
			st1.CT_SO_LINES_PERC_CUST_PLANT
	from stg_scn_customer_plant_hash as st1
	      left outer join sat_customer_plant_scn as s1
	                   on     st1.CUSTOMER_PLANT_HSH = s1.CUSTOMER_PLANT_HSH
	                      and s1.CUSTOMER_PLANT_LEDTS is NULL
	where (
	        s1.CUSTOMER_PLANT_HSH is null OR
	        (
	              st1.CUSTOMER_PLANT_SCN_HSH_DIFF != s1.CUSTOMER_PLANT_SCN_HSH_DIFF
	          AND s1.CUSTOMER_PLANT_LDTS = (
                                        select max(z.CUSTOMER_PLANT_LDTS)
                                        from sat_customer_plant_scn z
                                        where s1.CUSTOMER_PLANT_HSH = z.CUSTOMER_PLANT_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s105
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s105;
	insert into upd_sat_end_date_loop_s105 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.CUSTOMER_PLANT_HSH,
	       s1.CUSTOMER_PLANT_LDTS
	from sat_customer_plant_scn as s1
	where (
	            s1.CUSTOMER_PLANT_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.CUSTOMER_PLANT_HSH)
	                     from sat_customer_plant_scn as y
	                     where   y.CUSTOMER_PLANT_HSH = s1.CUSTOMER_PLANT_HSH
	                         AND y.CUSTOMER_PLANT_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s105
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s105;
	insert into upd_sat_end_date_cross_s105 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s105 t1
					inner join upd_sat_end_date_loop_s105 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_customer_plant_scn dst
	   set dst.CUSTOMER_PLANT_LEDTS = src.BUSKEY_LEDTS
	  from sat_customer_plant_scn dst
				inner join upd_sat_end_date_cross_s105 src on dst.CUSTOMER_PLANT_HSH = src.BUSKEY_HSH
	      												and dst.CUSTOMER_PLANT_LDTS = src.BUSKEY_LDTS;	