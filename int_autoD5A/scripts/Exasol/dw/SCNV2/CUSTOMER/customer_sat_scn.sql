/* Staging tables preparation */

	/* SCN - Past Due Report - Overview */
		drop table if exists stg_scn_customer_sales1;
		create table stg_scn_customer_sales1 as
		SELECT cust.customernumber,
       	ROUND(SUM((CASE
                      WHEN f_so.Dim_SalesOrderRejectReasonid <> 1
                           OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
                      WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
                      WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') THEN (CASE
                                                                                                                WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
                                                                                                                ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
                                                                                                            END * f_so.amt_UnitPriceUoM/(CASE
                                                                                                                                             WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit
                                                                                                                                             ELSE 1
                                                                                                                                         END))
                      ELSE (CASE
                                WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
                                ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
                            END * f_so.amt_UnitPriceUoM/(CASE
                                                             WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit
                                                             ELSE 1
                                                         END))
                  END) * f_so.amt_ExchangeRate_GBL),0) ct_past_due_amount_cust
		FROM fact_salesorder AS f_so
				INNER JOIN Dim_Date AS sdrdp ON f_so.Dim_DateidSchedDlvrReqPrev = sdrdp.Dim_Dateid
											AND ((sdrdp.DateValue) BETWEEN    (add_months(trunc(CURRENT_DATE - INTERVAL '9' MONTH, 'MM'), 1)-1 + INTERVAL '1' DAY) 
																		  AND (add_months(trunc(CURRENT_DATE, 'MM'), 1)-1))
				INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid AND (lower(prt.ItemSubType_Merck) = lower('FPP'))
				INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid AND (lower(dch.DistributionChannelName) != lower('Intercompany sales'))
				INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
				INNER JOIN dim_overallstatusforcreditcheck AS oscc ON f_so.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckid
				INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
				INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
				INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
				INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
				INNER JOIN dim_salesorderitemcategory AS ic ON f_so.dim_salesorderitemcategoryid = ic.dim_salesorderitemcategoryid
				INNER JOIN dim_customer AS cust ON f_so.dim_customerid = cust.dim_customerid
		WHERE (lower((CASE
		                  WHEN dc.DocumentCategory ='C'
		                       AND sdt.DocumentType NOT IN ('KA','KB')
		                       AND oscc.OverallStatusforCreditCheck <> 'B'
		                       AND sois.OverallProcessingStatus <> 'Completely processed'
		                       AND sois.OverallDeliveryStatus <>'Completely processed'
		                       AND sorr.RejectReasonCode = 'Not Set'
		                       AND soegidt.DateValue < CURRENT_DATE THEN 'X'
		                  ELSE 'Not Set'
		              END)) = lower('X'))
		  AND ((CASE
		            WHEN f_so.Dim_SalesOrderRejectReasonid <> 1
		                 OR ic.SalesOrderItemCategory = 'TAS' THEN 0.0000
		            WHEN soegidt.DateValue > CURRENT_DATE THEN 0.0000
		            WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC') 
														THEN (CASE
		                                                        WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
		                                                        ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
		                                                    END)
		            ELSE (CASE
		                      WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
		                      ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
		                  END)
		        END >= 0.1))
			and cust.CUSTOMERNUMBER <> 'Not Set'
		GROUP BY cust.customernumber;


	/* SCN - Open Orders - Overview */
		drop table if exists stg_scn_customer_sales2;
		create table stg_scn_customer_sales2 as
		SELECT cust.customernumber,
		       ROUND(SUM((CASE
		                      WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
		                      WHEN sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')
		                           AND f_so.dd_ItemRelForDelv = 'X' THEN (CASE
		                                                                      WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
		                                                                      ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
		                                                                  END * f_so.amt_UnitPriceUoM/(CASE
		                                                                                                   WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit
		                                                                                                   ELSE 1
		                                                                                               END))
		                      ELSE (CASE
		                                WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
		                                ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
		                            END * f_so.amt_UnitPriceUoM/(CASE
		                                                             WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit
		                                                             ELSE 1
		                                                         END))
		                  END) * f_so.amt_ExchangeRate_GBL),2) ct_open_orders_amounts_cust
		FROM fact_salesorder AS f_so
				INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
												AND (soegidt.DateValue BETWEEN (trunc(CURRENT_DATE) - INTERVAL '1' YEAR) AND trunc(CURRENT_DATE)
												     OR soegidt.DateValue BETWEEN trunc(CURRENT_DATE,'YEAR') AND trunc(CURRENT_DATE)
												     OR soegidt.DateValue BETWEEN CURRENT_DATE AND (CURRENT_DATE + 364))
				INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
																AND (lower(sorr.RejectReasonCode) = lower('Not Set'))
				INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
														AND ((CASE
														          WHEN sdt.DocumentType = 'AF' THEN 'IN'
														          WHEN sdt.DocumentType = 'AG' THEN 'QT'
														          WHEN sdt.DocumentType = 'AU' THEN 'SI'
														          WHEN sdt.DocumentType = 'G2' THEN 'CR'
														          WHEN sdt.DocumentType = 'KL' THEN 'FD'
														          WHEN sdt.DocumentType = 'KM' THEN 'CQ'
														          WHEN sdt.DocumentType = 'KN' THEN 'SD'
														          WHEN sdt.DocumentType = 'L2' THEN 'DR'
														          WHEN sdt.DocumentType = 'LP' THEN 'DS'
														          WHEN sdt.DocumentType = 'TA' THEN 'OR'
														          ELSE sdt.DocumentType
														      END) NOT IN (('KA'),('KB')))
				INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
														AND (lower(dc.DocumentCategory) = lower('C'))
				INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
															AND (lower(dch.DistributionChannelCode) != lower('20'))
				INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
															AND (lower(sois.OverallProcessingStatus) != lower('Completely processed'))
															AND (lower(sois.OverallDeliveryStatus) != lower('Completely processed'))
				INNER JOIN dim_customer AS cust ON f_so.dim_customerid = cust.dim_customerid
		WHERE ((CASE
		            WHEN f_so.Dim_SalesOrderRejectReasonid <> 1 THEN 0.0000
		            WHEN sdt.DocumentType IN ('LP', 'LK', 'ZLK', 'LZM', 'LZ', 'ZLZ', 'ZPL', 'ZMZC', 'ZLZC')
		                 AND f_so.dd_ItemRelForDelv = 'X' THEN (CASE
		                                                            WHEN (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 THEN 0.0000
		                                                            ELSE (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived))
		                                                        END)
		            WHEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 THEN 0.0000
		            ELSE (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty)
		        END>0))
			and cust.CUSTOMERNUMBER <> 'Not Set'
		GROUP BY cust.customernumber ;



	/*  AHR0061- Returns Summary - Return by summary */
		drop table if exists stg_scn_customer_sales3;
		create table stg_scn_customer_sales3 as
		SELECT cust.customernumber,
		       ROUND(SUM(CASE
		                     WHEN (dc.DocumentCategory) ='H'
		                          AND (sorr.RejectReasonCode) = 'Not Set' THEN f_so.amt_ScheduleTotal*amt_exchangerate_gbl
		                     ELSE 0.0000
		                 END),2) ct_returns_only_cust
		FROM fact_salesorder AS f_so
				INNER JOIN Dim_Date AS soc ON f_so.Dim_DateidSalesOrderCreated = soc.Dim_Dateid
											AND (soc.CalendarYear BETWEEN year(trunc(CURRENT_DATE) - INTERVAL '2' YEAR) AND YEAR(trunc(CURRENT_DATE)))
				INNER JOIN dim_distributionchannel AS dch ON f_so.Dim_DistributionChannelId = dch.dim_distributionchannelid
															AND (lower(dch.DistributionChannelName) != lower('Intercompany sales'))
				INNER JOIN dim_salesdocumenttype AS sdt ON f_so.Dim_SalesDocumentTypeid = sdt.dim_salesdocumenttypeid
															AND ((CASE
															          WHEN sdt.DocumentType = 'AF' THEN 'IN'
															          WHEN sdt.DocumentType = 'AG' THEN 'QT'
															          WHEN sdt.DocumentType = 'AU' THEN 'SI'
															          WHEN sdt.DocumentType = 'G2' THEN 'CR'
															          WHEN sdt.DocumentType = 'KL' THEN 'FD'
															          WHEN sdt.DocumentType = 'KM' THEN 'CQ'
															          WHEN sdt.DocumentType = 'KN' THEN 'SD'
															          WHEN sdt.DocumentType = 'L2' THEN 'DR'
															          WHEN sdt.DocumentType = 'LP' THEN 'DS'
															          WHEN sdt.DocumentType = 'TA' THEN 'OR'
															          ELSE sdt.DocumentType
															      END) NOT IN (('KA'),('KE')))
				INNER JOIN dim_Customer AS c ON f_so.dim_CustomerID = c.dim_Customerid
				INNER JOIN dim_documentcategory AS dc ON f_so.Dim_DocumentCategoryid = dc.dim_documentcategoryid
				INNER JOIN dim_salesorderrejectreason AS sorr ON f_so.Dim_SalesOrderRejectReasonid = sorr.dim_salesorderrejectreasonid
				INNER JOIN dim_salesmisc AS dsi ON f_so.Dim_SalesMiscId = dsi.dim_salesmiscid
				INNER JOIN dim_salesorderitemstatus AS sois ON f_so.Dim_SalesOrderItemStatusid = sois.dim_salesorderitemstatusid
				INNER JOIN dim_customer AS cust ON f_so.dim_customerid = cust.dim_customerid
		where c.CUSTOMERNUMBER <> 'Not Set'
		GROUP BY cust.customernumber;


	/* ct_sales_order_perc_otif_cust */
		drop table if exists stg_scn_customer_sales4;
		create table stg_scn_customer_sales4 as
		SELECT c.customernumber,
		       ROUND(((COUNT(DISTINCT CASE
		                                  WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y'
		                                       AND f_so.dd_soheaderotif_merck = 'Y' THEN f_so.dd_SalesDocNo
		                                  ELSE 'Not Set'
		                              END) - CASE
		                                         WHEN SUM(DISTINCT CASE
		                                                               WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y'
		                                                                    AND f_so.dd_soheaderotif_merck = 'Y' THEN 0.0000
		                                                               ELSE 1.0000
		                                                           END) > 0 THEN 1.0000
		                                         ELSE 0.0000
		                                     END) / (CASE
		                                                 WHEN (COUNT(DISTINCT CASE
		                                                                          WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN f_so.dd_SalesDocNo
		                                                                          ELSE 'Not Set'
		                                                                      END) - CASE
		                                                                                 WHEN SUM(DISTINCT CASE
		                                                                                                       WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
		                                                                                                       ELSE 1.0000
		                                                                                                   END) > 0 THEN 1.0000
		                                                                                 ELSE 0.0000
		                                                                             END) = 0.0000 THEN 1.0000
		                                                 ELSE (COUNT(DISTINCT CASE
		                                                                          WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN f_so.dd_SalesDocNo
		                                                                          ELSE 'Not Set'
		                                                                      END) - CASE
		                                                                                 WHEN SUM(DISTINCT CASE
		                                                                                                       WHEN f_so.dd_OTIFCalcFlag_Merck = 'Y' THEN 0.0000
		                                                                                                       ELSE 1.0000
		                                                                                                   END) > 0 THEN 1.0000
		                                                                                 ELSE 0.0000
		                                                                             END)
		                                             END)) * 100,2) ct_sales_order_perc_otif_cust
		FROM fact_salesorder AS f_so
				INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid
												AND (soegidt.DateValue BETWEEN to_date(CONCAT(YEAR(trunc(CURRENT_DATE)),'-01-01'),'YYYY-MM-DD') AND trunc(CURRENT_DATE))
				INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid
											AND (lower(prt.ProductHierarchy) = lower('Not Set'))
				INNER JOIN dim_Customer AS c ON f_so.dim_CustomerID = c.dim_Customerid
				INNER JOIN Dim_Date AS sdd ON f_so.Dim_DateidSchedDelivery = sdd.Dim_Dateid
		WHERE (lower(f_so.dd_ItemRelForDelv) = lower('X'))
		  AND (lower(f_so.dd_otifenabledflag) = lower('Y'))
		  and c.CUSTOMERNUMBER <> 'Not Set'
		GROUP BY c.customernumber;

	/* ********************* END: Aggregations for Customer ********************* */

-- >>

	/* ********************* START: Unifying measures for Customer ********************* */

  		drop table if exists stg_scn_customer_unique;
		create table stg_scn_customer_unique as
		select customernumber from stg_scn_customer_sales1
		union
		select customernumber from stg_scn_customer_sales2
		union
		select customernumber from stg_scn_customer_sales3
		union
		select customernumber from stg_scn_customer_sales4;

		drop table if exists stg_scn_customer;
		create table stg_scn_customer as
		select  t0.customernumber,
			    cast(ifnull(t1.ct_past_due_amount_cust, 0.0) as decimal(18,5)) 		 as ct_past_due_amount_cust,
				cast(ifnull(t2.ct_open_orders_amounts_cust, 0.0) as decimal(18,5)) 	 as ct_open_orders_amounts_cust,
				cast(ifnull(t3.ct_returns_only_cust, 0.0) as decimal(18,5)) 		 as ct_returns_only_cust,
				cast(ifnull(t4.ct_sales_order_perc_otif_cust, 0.0) as decimal(18,5)) as ct_sales_order_perc_otif_cust
		from stg_scn_customer_unique t0
				left join stg_scn_customer_sales1 t1 on t0.customernumber = t1.customernumber
				left join stg_scn_customer_sales2 t2 on t0.customernumber = t2.customernumber
				left join stg_scn_customer_sales3 t3 on t0.customernumber = t3.customernumber
				left join stg_scn_customer_sales4 t4 on t0.customernumber = t4.customernumber;

		drop table if exists stg_scn_customer_hash;
		create table stg_scn_customer_hash as
		select 	cast(UPPER(HASH_MD5(CUSTOMERNUMBER)) as varchar(32)) 			as CUSTOMERNUMBER_HSH,
				current_timestamp 				 				 				as CUSTOMERNUMBER_LDTS,
				cast('AERA BV: SCN - Customer from Multiple SA' as varchar(50)) as CUSTOMERNUMBER_RSRC,
				cast(UPPER(HASH_MD5(upper(customernumber 							|| '|^|' ||
										  IFNULL(ct_past_due_amount_cust, 0.0) 		|| '|^|' ||
										  IFNULL(ct_open_orders_amounts_cust, 0.0) 	|| '|^|' ||
										  IFNULL(ct_returns_only_cust, 0.0) 		|| '|^|' ||
										  IFNULL(ct_sales_order_perc_otif_cust, 0.0)
						)))as varchar(32)) 										as CUSTOMERNUMBER_SCN_HSH_DIFF,
				CUSTOMERNUMBER,
				ct_past_due_amount_cust,
				ct_open_orders_amounts_cust,
				ct_returns_only_cust,
				ct_sales_order_perc_otif_cust
		from stg_scn_customer;

	/* ********************* END: Unifying measures for Customer ********************* */

-- >>

	/* ********************* START: Customerial SCN measures populating ********************* */

	/* SAT Customer SCN table population */

	 /*	drop table if exists sat_customer_scn
		create table sat_customer_scn (
				customernumber_HSH 			varchar(32),
				customernumber_LDTS 		timestamp,
				customernumber_LEDTS 		timestamp,
				customernumber_RSRC 		varchar(50),
				customernumber_SCN_HSH_DIFF varchar(32),
				ct_past_due_amount_cust 				decimal(18,5),
				ct_open_orders_amounts_cust		decimal(18,5),
				ct_returns_only_cust 				decimal(18,5),
                ct_sales_order_perc_otif_cust decimal(18,5)
				) */

	/* insert into sat dummy record in order to avoid left joins in future */
	insert into sat_customer_scn (customernumber_HSH, customernumber_LDTS, customernumber_LEDTS, customernumber_RSRC, customernumber_SCN_HSH_DIFF,
								  ct_past_due_amount_cust, ct_open_orders_amounts_cust, ct_returns_only_cust, ct_sales_order_perc_otif_cust
								 )
	select  '00000000000000000000000000000000' as customernumber_HSH,
			'0001-01-01' as customernumber_LDTS,
			'9999-12-31' as customernumber_LEDTS,
			'SYSTEM'	 as customernumber_RSRC,
			'00000000000000000000000000000000' as customernumber_SCN_HSH_DIFF,
			0.00 as ct_past_due_amount_cust, 
			0.00 as ct_open_orders_amounts_cust, 
			0.00 as ct_returns_only_cust, 
			0.00 as ct_sales_order_perc_otif_cust
	from dual
	where not exists (select 1 from sat_customer_scn where customernumber_HSH = '00000000000000000000000000000000');

	/* insert new records and changed ones */
	insert into sat_customer_scn (customernumber_HSH, customernumber_LDTS, customernumber_RSRC, customernumber_SCN_HSH_DIFF,
								  ct_past_due_amount_cust, ct_open_orders_amounts_cust, ct_returns_only_cust, ct_sales_order_perc_otif_cust)
	select 	st1.customernumber_HSH, st1.customernumber_LDTS, st1.customernumber_RSRC, st1.customernumber_SCN_HSH_DIFF,
			st1.ct_past_due_amount_cust, st1.ct_open_orders_amounts_cust, st1.ct_returns_only_cust, st1.ct_sales_order_perc_otif_cust
	from stg_scn_customer_hash as st1
	      left outer join sat_customer_scn as s1
	                   on     st1.customernumber_HSH = s1.customernumber_HSH
	                      and s1.customernumber_LEDTS is NULL
	where (
	        s1.customernumber_HSH is null OR
	        (
	              st1.customernumber_SCN_HSH_DIFF != s1.customernumber_SCN_HSH_DIFF
	          AND s1.customernumber_LDTS = (
                                        select max(z.customernumber_LDTS)
                                        from sat_customer_scn z
                                        where s1.customernumber_HSH = z.customernumber_HSH
                                       )
	        )
	      );

	/* update Load End Date */

	 /* create table upd_sat_end_date_loop_s201
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  ) */

	truncate table upd_sat_end_date_loop_s201;
	insert into upd_sat_end_date_loop_s201 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.customernumber_HSH,
	       s1.customernumber_LDTS
	from sat_customer_scn as s1
	where (
	            s1.customernumber_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.customernumber_HSH)
	                     from sat_customer_scn as y
	                     where   y.customernumber_HSH = s1.customernumber_HSH
	                         AND y.customernumber_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s201
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  ) */

	truncate table upd_sat_end_date_cross_s201;
	insert into upd_sat_end_date_cross_s201 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s201 t1
					inner join upd_sat_end_date_loop_s201 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_customer_scn dst
	   set dst.customernumber_LEDTS = src.BUSKEY_LEDTS
	  from sat_customer_scn dst
				inner join upd_sat_end_date_cross_s201 src on dst.customernumber_HSH = src.BUSKEY_HSH
	      												and dst.customernumber_LDTS = src.BUSKEY_LDTS;
