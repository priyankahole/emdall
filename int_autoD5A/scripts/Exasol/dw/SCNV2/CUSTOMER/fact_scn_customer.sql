/* Build SCN for Material */
		create or replace view fact_scn_customer 	as
		select  row_number()over(order by '') 		as fact_scn_customerid,
				KNA1_KUNNR_HSH 						as dim_customer__xbiid,
				KNA1_KUNNR_RSRC						as dd_customer_recordsource,
				CUSTOMERNUMBER_RSRC 				as dd_scn_customer_recordsource,
				CT_PAST_DUE_AMOUNT_CUST,
				CT_OPEN_ORDERS_AMOUNTS_CUST,
				CT_RETURNS_ONLY_CUST,
				CT_SALES_ORDER_PERC_OTIF_CUST
		from HUB_CUSTOMER h1
				inner join sat_customer_scn s1 on h1.KNA1_KUNNR_HSH = s1.CUSTOMERNUMBER_HSH
		where s1.CUSTOMERNUMBER_LEDTS is null;
		
		/* will use CUSTOMER_HSH to join to dim_customer__xbi dimension */
