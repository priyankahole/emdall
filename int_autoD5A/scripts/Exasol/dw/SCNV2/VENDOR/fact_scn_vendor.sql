/* create fact vendor */
	create or replace view fact_scn_vendor 	as
		select  row_number()over(order by '') 		as fact_scn_vendorid,
				LFA1_LIFNR_HSH						as dim_vendor__xbiid,
				LFA1_LIFNR_RSRC						as dd_vendor_recordsource
		from HUB_VENDOR h1
				;

