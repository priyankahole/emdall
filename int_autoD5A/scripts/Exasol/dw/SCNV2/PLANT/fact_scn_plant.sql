/* Build SCN for Plant */
		create or replace view fact_scn_plant 	as
		select  row_number()over(order by '') 		as fact_scn_plantid,
				T001W_WERKS_HSH 						as dim_plant__xbiid,
				T001W_WERKS_RSRC						as dd_plant_recordsource,
				PLANTCODE_RSRC 					as dd_scn_plant_recordsource,
				CT_OPENORDER_AMOUNT_PL,
                CT_PASTDUEORDER_AMOUNT_ORD_PL,
                CT_TOTAL_SALES_ORDERS,
                CT_TOTAL_SALES_ORDERS_OTIF,
                CT_SALES_ORDERS_OTIF_PERC,
                CT_SALES_ORDERS_LINES_OTIF_PERC,
                CT_PURCHASE_DOC_COUNT_ORD_PL,
                CT_DELIV_SCHED_COUNT_ORD_PL,
                CT_IOTIF_DOCLVL_ORD_PL,
                CT_IOTIF_SCHEDLVL_ORD_PL,
                CT_IOTIFCOUNT_DOCLVL_ORD_PL,
                CT_IOTIFCOUNT_SCHEDLVL_ORD_PL,
                CT_PURCHASE_DOC_COUNT_SUPPLY_PL,
                CT_DELIV_SCHED_COUNT_SUPPLY_PL,
                CT_IOTIF_DOCLVL_SUPPLY_PL,
                CT_IOTIF_SCHEDLVL_SUPPLY_PL,
                CT_IOTIFCOUNT_DOCLVL_SUPPLY_PL,
                CT_IOTIFCOUNT_SCHEDLVL_SUPPLY_PL,
                CT_ACTYTD_ENDMONTH_PRD_PL,
                CT_SA_LAST_MONTH_PROD_PL,
                CT_LROT_YTD_INSP_PL,
                CT_LROT_LASTMONTH_INSP_PL
		from HUB_PLANT h1
				inner join sat_plant_scn s1 on h1.T001W_WERKS_HSH = s1.PLANTCODE_HSH
		where s1.PLANTCODE_LEDTS is null;
		
		/* will use PLANTCODE_HSH to join to dim_plant__xbi dimension */