/* LINK Material-Plant table populating */

	/* create table link_material_plant (MARC_MATNR_WERKS_HSH varchar(32),
							   			 MARC_MATNR_WERKS_LDTS timestamp,
							   			 MARC_MATNR_WERKS_RSRC varchar(50),
							   			 MARC_MATNR varchar(18),
										 MARC_WERKS
							  ) */

	insert into link_material_plant (MARC_MATNR_WERKS_HSH, MARC_MATNR_WERKS_LDTS, MARC_MATNR_WERKS_RSRC, MARC_MATNR_HSH, MARC_WERKS_HSH)
	select distinct st1.MARC_MATNR_WERKS_HSH,
	       st1.MARC_MATNR_WERKS_LDTS,
	       st1.MARC_MATNR_WERKS_RSRC,
	       st1.MARC_MATNR_HSH,
		   st1.MARC_WERKS_HSH
	from marc__xbi_hash_stg as st1
	where not exists (
                      select 1
                      from link_material_plant l1
					  where     st1.MARC_MATNR_HSH = l1.MARC_MATNR_HSH
							and st1.MARC_WERKS_HSH = l1.MARC_WERKS_HSH
                      );
