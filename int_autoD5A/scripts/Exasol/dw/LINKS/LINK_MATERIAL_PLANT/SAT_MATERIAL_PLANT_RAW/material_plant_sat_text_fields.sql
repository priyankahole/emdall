/* ---------------------------------------------------------------------------- */

/* SAT Material-Plant TEXT table population */

	/* drop table if exists sat_material_plant_text
	create table sat_material_plant_text (
			MARC_MATNR_WERKS_HSH varchar(32),
			MARC_MATNR_WERKS_LDTS timestamp,
			MARC_MATNR_WERKS_LEDTS timestamp,
			MARC_MATNR_WERKS_RSRC varchar(50),
			MARC_MATNR_WERKS_TEXT_HSH_DIFF varchar(32),
			MARC_UEETK VARCHAR(1),
			MARC_LADGR VARCHAR(4),
			MARC_LVORM VARCHAR(1),
			MARC_ALTSL VARCHAR(1),
			MARC_DISGR VARCHAR(4),
			MARC_DISMM VARCHAR(2),
			MARC_DISPR VARCHAR(4),
			MARC_EKGRP VARCHAR(3),
			MARC_FHORI VARCHAR(3),
			MARC_FVIDK VARCHAR(4),
			MARC_INSMK VARCHAR(1),
			MARC_KZAUS VARCHAR(1),
			MARC_KZKRI VARCHAR(1),
			MARC_LGFSB VARCHAR(4),
			MARC_MAABC VARCHAR(1),
			MARC_MTVFP VARCHAR(2),
			MARC_PERIV VARCHAR(2),
			MARC_PRENC VARCHAR(1),
			MARC_PRENO VARCHAR(8),
			MARC_PSTAT VARCHAR(15),
			MARC_QMATV VARCHAR(1),
			MARC_RGEKZ VARCHAR(1),
			MARC_SBDKZ VARCHAR(1),
			MARC_SFCPF VARCHAR(6),
			MARC_SHFLG VARCHAR(1),
			MARC_SOBSK VARCHAR(2),
			MARC_SSQSS VARCHAR(8),
			MARC_STRGR VARCHAR(2),
			MARC_USEQU VARCHAR(1),
			MARC_VINT1 VARCHAR(3),
			MARC_VRMOD VARCHAR(1),
			MARC_XCHAR VARCHAR(1),
			MARC_AUSME VARCHAR(3),
			MARC_AWSLS VARCHAR(6),
			MARC_BESKZ VARCHAR(1),
			MARC_BWSCL VARCHAR(1),
			MARC_DISLS VARCHAR(2),
			MARC_DISPO VARCHAR(3),
			MARC_FEVOR VARCHAR(3),
			MARC_FRTME VARCHAR(3),
			MARC_FXHOR VARCHAR(3),
			MARC_HERKL VARCHAR(3),
			MARC_KAUTB VARCHAR(1),
			MARC_KZDKZ VARCHAR(1),
			MARC_LGPRO VARCHAR(4),
			MARC_MMSTA VARCHAR(2),
			MARC_NCOST VARCHAR(1),
			MARC_PERKZ VARCHAR(1),
			MARC_PRCTR VARCHAR(10),
			MARC_QMATA VARCHAR(6),
			MARC_QZGTP VARCHAR(4),
			MARC_RWPRO VARCHAR(3),
			MARC_SCHGT VARCHAR(1),
			MARC_SFEPR VARCHAR(4),
			MARC_SHZET VARCHAR(2),
			MARC_SOBSL VARCHAR(2),
			MARC_STAWN VARCHAR(17),
			MARC_VERKZ VARCHAR(1),
			MARC_VINT2 VARCHAR(3),
			MARC_XCHPF VARCHAR(1)
			) */

	/* insert new records and changed ones */
	insert into sat_material_plant_text (MARC_MATNR_WERKS_HSH, MARC_MATNR_WERKS_LDTS, MARC_MATNR_WERKS_RSRC, MARC_MATNR_WERKS_TEXT_HSH_DIFF,
										MARC_UEETK,MARC_LADGR,MARC_LVORM,MARC_ALTSL,MARC_DISGR,MARC_DISMM,MARC_DISPR,MARC_EKGRP,MARC_FHORI,MARC_FVIDK,MARC_INSMK,MARC_KZAUS,MARC_KZKRI,MARC_LGFSB,
										MARC_MAABC,MARC_MTVFP,MARC_PERIV,MARC_PRENC,MARC_PRENO,MARC_PSTAT,MARC_QMATV,MARC_RGEKZ,MARC_SBDKZ,MARC_SFCPF,MARC_SHFLG,MARC_SOBSK,MARC_SSQSS,MARC_STRGR,
										MARC_USEQU,MARC_VINT1,MARC_VRMOD,MARC_XCHAR,MARC_AUSME,MARC_AWSLS,MARC_BESKZ,MARC_BWSCL,MARC_DISLS,MARC_DISPO,MARC_FEVOR,MARC_FRTME,MARC_FXHOR,MARC_HERKL,
										MARC_KAUTB,MARC_KZDKZ,MARC_LGPRO,MARC_MMSTA,MARC_NCOST,MARC_PERKZ,MARC_PRCTR,MARC_QMATA,MARC_QZGTP,MARC_RWPRO,MARC_SCHGT,MARC_SFEPR,MARC_SHZET,MARC_SOBSL,
										MARC_STAWN,MARC_VERKZ,MARC_VINT2,MARC_XCHPF)
	select 	st1.MARC_MATNR_WERKS_HSH,
		   	st1.MARC_MATNR_WERKS_LDTS,
		   	st1.MARC_MATNR_WERKS_RSRC,
		   	st1.MARC_MATNR_WERKS_TEXT_HSH_DIFF,
	       	st1.MARC_UEETK, st1.MARC_LADGR, st1.MARC_LVORM, st1.MARC_ALTSL, st1.MARC_DISGR, st1.MARC_DISMM, st1.MARC_DISPR, st1.MARC_EKGRP, st1.MARC_FHORI, st1.MARC_FVIDK, st1.MARC_INSMK, st1.MARC_KZAUS,
			st1.MARC_KZKRI, st1.MARC_LGFSB, st1.MARC_MAABC, st1.MARC_MTVFP, st1.MARC_PERIV, st1.MARC_PRENC, st1.MARC_PRENO, st1.MARC_PSTAT, st1.MARC_QMATV, st1.MARC_RGEKZ, st1.MARC_SBDKZ, st1.MARC_SFCPF,
			st1.MARC_SHFLG, st1.MARC_SOBSK, st1.MARC_SSQSS, st1.MARC_STRGR, st1.MARC_USEQU, st1.MARC_VINT1, st1.MARC_VRMOD, st1.MARC_XCHAR, st1.MARC_AUSME, st1.MARC_AWSLS, st1.MARC_BESKZ, st1.MARC_BWSCL,
			st1.MARC_DISLS, st1.MARC_DISPO, st1.MARC_FEVOR, st1.MARC_FRTME, st1.MARC_FXHOR, st1.MARC_HERKL, st1.MARC_KAUTB, st1.MARC_KZDKZ, st1.MARC_LGPRO, st1.MARC_MMSTA, st1.MARC_NCOST, st1.MARC_PERKZ,
			st1.MARC_PRCTR, st1.MARC_QMATA, st1.MARC_QZGTP, st1.MARC_RWPRO, st1.MARC_SCHGT, st1.MARC_SFEPR, st1.MARC_SHZET, st1.MARC_SOBSL, st1.MARC_STAWN, st1.MARC_VERKZ, st1.MARC_VINT2, st1.MARC_XCHPF
	from marc__xbi_hash_stg as st1
	      left outer join sat_material_plant_text as s1
	                   on     st1.MARC_MATNR_WERKS_HSH = s1.MARC_MATNR_WERKS_HSH
	                      and s1.MARC_MATNR_WERKS_LEDTS is NULL
	where (
	        s1.MARC_MATNR_WERKS_HSH is null OR
	        (
	              st1.MARC_MATNR_WERKS_TEXT_HSH_DIFF != s1.MARC_MATNR_WERKS_TEXT_HSH_DIFF
	          AND s1.MARC_MATNR_WERKS_LDTS = (
			                                        select max(z.MARC_MATNR_WERKS_LDTS)
			                                        from sat_material_plant_text z
			                                        where s1.MARC_MATNR_WERKS_HSH = z.MARC_MATNR_WERKS_HSH
			                                       )
	        )
	      );

	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s10
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  )
	select * from exa_all_tables where lower(table_name) like 'upd_sat_end_date_loop%'
	*/

	truncate table upd_sat_end_date_loop_s10;
	insert into upd_sat_end_date_loop_s10 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.MARC_MATNR_WERKS_HSH,
	       s1.MARC_MATNR_WERKS_LDTS
	from sat_material_plant_text as s1
	where (
	            s1.MARC_MATNR_WERKS_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.MARC_MATNR_WERKS_HSH)
	                     from sat_material_plant_text as y
	                     where   y.MARC_MATNR_WERKS_HSH = s1.MARC_MATNR_WERKS_HSH
	                         AND y.MARC_MATNR_WERKS_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s10
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  )
	*/

	truncate table upd_sat_end_date_cross_s10;
	insert into upd_sat_end_date_cross_s10 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s10 t1
					inner join upd_sat_end_date_loop_s10 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_material_plant_text dst
	   set dst.MARC_MATNR_WERKS_LEDTS = src.BUSKEY_LEDTS
	  from sat_material_plant_text dst
				inner join upd_sat_end_date_cross_s10 src on dst.MARC_MATNR_WERKS_HSH = src.BUSKEY_HSH
	      												 and dst.MARC_MATNR_WERKS_LDTS = src.BUSKEY_LDTS;
                                 
