/* 	Reference table for MARC_DISMM based on T438T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_mrptypedescr
   CREATE TABLE ref_mrptypedescr
   (
       MARC_DISMM VARCHAR(4), -- MRP Type Code
       T438T_DIBEZ VARCHAR(30) UTF8, -- MRP Type Description
       T438T_SPRAS VARCHAR(1) UTF8
   )
*/

   MERGE INTO ref_mrptypedescr dst
   USING (SELECT T438T_DISMM, T438T_SPRAS, T438T_DIBEZ FROM T438T__XBI) src
   ON ( dst.MARC_DISMM = src.T438T_DISMM
           AND dst.T438T_SPRAS = src.T438T_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.T438T_DIBEZ = src.T438T_DIBEZ
   WHEN NOT MATCHED THEN INSERT (MARC_DISMM, T438T_SPRAS, T438T_DIBEZ) VALUES (src.T438T_DISMM, src.T438T_SPRAS, src.T438T_DIBEZ);
