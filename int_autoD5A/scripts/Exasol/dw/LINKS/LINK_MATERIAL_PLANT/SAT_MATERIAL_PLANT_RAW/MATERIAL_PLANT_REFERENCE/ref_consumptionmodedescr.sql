/* 	Reference table for MARC_VRMOD based on DD07T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_consumptionmodedescription
   CREATE TABLE ref_consumptionmodedescription
   (
       MARC_VRMOD VARCHAR(10), -- Code
       DD07T_DDTEXT     VARCHAR(60) UTF8,
       DD07T_DDLANGUAGE VARCHAR(1) UTF8 -- Desc
   ) */


   MERGE INTO ref_consumptionmodedescription dst
   USING (SELECT DD07T_DOMVALUE_L, DD07T_DDLANGUAGE, DD07T_DDTEXT FROM DD07T__XBI WHERE DD07T_DOMNAME = 'VRMOD') src
   ON ( dst.MARC_VRMOD = src.DD07T_DOMVALUE_L
           AND dst.DD07T_DDLANGUAGE = src.DD07T_DDLANGUAGE)
   WHEN MATCHED THEN UPDATE SET dst.DD07T_DDTEXT = src.DD07T_DDTEXT
   WHEN NOT MATCHED THEN INSERT (MARC_VRMOD, DD07T_DDLANGUAGE, DD07T_DDTEXT) VALUES (src.DD07T_DOMVALUE_L, src.DD07T_DDLANGUAGE, src.DD07T_DDTEXT);
