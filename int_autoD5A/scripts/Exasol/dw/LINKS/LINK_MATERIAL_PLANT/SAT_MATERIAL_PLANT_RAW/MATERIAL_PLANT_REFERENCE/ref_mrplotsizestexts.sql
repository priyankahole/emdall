/* 	Reference table for MARC_DISLS based on T439A table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_mrplotsizestexts
   CREATE TABLE ref_mrplotsizestexts
   (
       MARC_DISLS VARCHAR(4), --  Code
       T439A_LOSKZ VARCHAR(1) UTF8, --LOTSIZEINDICATOR
       T439A_PERAZ DECIMAL(18,4) --NUMBEROFPERIODS
   ) */

   MERGE INTO ref_mrplotsizestexts dst
   USING (SELECT T439A_DISLS, T439A_LOSKZ, T439A_PERAZ FROM T439A__XBI WHERE T439A_DISLS IS NOT NULL) src
   ON ( dst.MARC_DISLS = src.T439A_DISLS)
   WHEN MATCHED THEN UPDATE SET dst.T439A_LOSKZ = src.T439A_LOSKZ,
                                dst.T439A_PERAZ = src.T439A_PERAZ
   WHEN NOT MATCHED THEN INSERT (MARC_DISLS, T439A_LOSKZ, T439A_PERAZ) VALUES (src.T439A_DISLS, src.T439A_LOSKZ, src.T439A_PERAZ);
