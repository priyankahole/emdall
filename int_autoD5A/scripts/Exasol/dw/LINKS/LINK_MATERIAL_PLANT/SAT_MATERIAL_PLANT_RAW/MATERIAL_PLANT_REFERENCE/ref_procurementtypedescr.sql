/* 	Reference table for MARC_BESKZ based on DD07T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_procurementtypedescription
   CREATE TABLE ref_procurementtypedescription
   (
       MARC_BESKZ VARCHAR(10), -- Procurement Type Code
       DD07T_DDTEXT     VARCHAR(60) UTF8,
       DD07T_DDLANGUAGE VARCHAR(1) UTF8 -- Procurement Type Desc
   ) */

   MERGE INTO ref_procurementtypedescription dst
   USING (SELECT DD07T_DOMVALUE_L, DD07T_DDLANGUAGE, DD07T_DDTEXT FROM DD07T__XBI WHERE DD07T_DOMNAME = 'BESKZ' AND DD07T_DOMVALUE_L IS NOT NULL) src
   ON ( dst.MARC_BESKZ = src.DD07T_DOMVALUE_L
           AND dst.DD07T_DDLANGUAGE = src.DD07T_DDLANGUAGE)
   WHEN MATCHED THEN UPDATE SET dst.DD07T_DDTEXT = src.DD07T_DDTEXT
   WHEN NOT MATCHED THEN INSERT (MARC_BESKZ, DD07T_DDLANGUAGE, DD07T_DDTEXT) VALUES (src.DD07T_DOMVALUE_L, src.DD07T_DDLANGUAGE, src.DD07T_DDTEXT);
