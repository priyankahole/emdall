/* 	Reference table for MARC_DISPO based on T024D table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_mrpcontrollerdescription
   CREATE TABLE ref_mrpcontrollerdescription
   (
       MARC_DISPO VARCHAR(50), -- Code
       MARC_WERKS VARCHAR(50) UTF8, --
       T024D_DSNAM VARCHAR(256) UTF8, -- Descr
       T024D_DSTEL VARCHAR(256) UTF8 -- Telephone
   )
*/

   MERGE INTO ref_mrpcontrollerdescription dst
   USING (SELECT T024D_DISPO,T024D_WERKS,T024D_DSNAM,T024D_DSTEL FROM T024D__XBI WHERE T024D_DISPO IS NOT NULL AND T024D_WERKS IS NOT NULL) src
   ON ( dst.MARC_DISPO = src.T024D_DISPO
           AND dst.MARC_WERKS = src.T024D_WERKS)
   WHEN MATCHED THEN UPDATE SET dst.T024D_DSNAM = src.T024D_DSNAM,
                                dst.T024D_DSTEL = src.T024D_DSTEL
   WHEN NOT MATCHED THEN INSERT (MARC_DISPO, MARC_WERKS, T024D_DSNAM,T024D_DSTEL) VALUES (src.T024D_DISPO, src.T024D_WERKS, src.T024D_DSNAM,src.T024D_DSTEL);
