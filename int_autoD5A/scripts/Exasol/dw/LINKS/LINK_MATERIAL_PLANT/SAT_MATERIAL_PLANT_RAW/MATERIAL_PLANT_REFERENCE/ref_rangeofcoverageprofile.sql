/* 	Reference table for MARC_RWPRO based on T438R table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_rangeofcoverageprofile
   CREATE TABLE ref_rangeofcoverageprofile
   (
       MARC_RWPRO VARCHAR(4), --  Code
       MARC_WERKS VARCHAR(4) UTF8, -- Code
       T438R_RWART VARCHAR(1) UTF8, --TYPEPERIODLENGTH
       T438R_RW1TG VARCHAR(2) UTF8, -- TARGETCOVERAGE1
       T438R_RWPER VARCHAR(1) UTF8 -- PERIODINDICATOR

   ) */

   MERGE INTO ref_rangeofcoverageprofile dst
   USING (SELECT T438R_RWPRO, T438R_WERKS, T438R_RWART, T438R_RW1TG, T438R_RWPER FROM T438R__XBI WHERE T438R_RWPRO IS NOT NULL) src
   ON ( dst.MARC_RWPRO = src.T438R_RWPRO AND dst.MARC_WERKS = src.T438R_WERKS)
   WHEN MATCHED THEN UPDATE SET dst.T438R_RWART = src.T438R_RWART,
                                dst.T438R_RW1TG = src.T438R_RW1TG,
                                dst.T438R_RWPER = src.T438R_RWPER
   WHEN NOT MATCHED THEN INSERT (MARC_RWPRO, MARC_WERKS, T438R_RWART, T438R_RW1TG, T438R_RWPER) VALUES (src.T438R_RWPRO, src.T438R_WERKS, src.T438R_RWART, src.T438R_RW1TG, src.T438R_RWPER);
