/* 	Reference table for MARC_AUSME based on T006A table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_unitofissuecommercial
   CREATE TABLE ref_unitofissuecommercial
   (
       MARC_AUSME VARCHAR(3), -- Code
       T006A_SPRAS VARCHAR(1), -- Language
       T006A_MSEH3 VARCHAR(4), -- Unit of Issue Commercial
       T006A_MSEHT VARCHAR(10), --Unit of Measurement Text (Maximum 10 Characters)
       T006A_MSEHL VARCHAR(30)  --Unit of Measurement Text (Maximum 30 Characters)
   )
*/


   MERGE INTO ref_unitofissuecommercial dst
   USING (SELECT T006A_MSEHI,T006A_SPRAS, T006A_MSEH3,T006A_MSEHT,T006A_MSEHL FROM T006A__XBI WHERE T006A_MSEHI IS NOT NULL) src
   ON ( dst.MARC_AUSME= src.T006A_MSEHI and dst.T006A_SPRAS = src.T006A_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.T006A_MSEH3 = src.T006A_MSEH3
   WHEN NOT MATCHED THEN INSERT (MARC_AUSME, T006A_SPRAS,T006A_MSEH3,T006A_MSEHT,T006A_MSEHL) VALUES (src.T006A_MSEHI,T006A_SPRAS, src.T006A_MSEH3,T006A_MSEHT,T006A_MSEHL);
