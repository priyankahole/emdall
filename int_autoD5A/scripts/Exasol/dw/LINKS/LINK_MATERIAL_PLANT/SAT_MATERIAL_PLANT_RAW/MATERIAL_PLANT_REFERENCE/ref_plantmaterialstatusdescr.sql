/* 	Reference table for MARC_MMSTA based on T141T table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_plantmaterialstatusdescription
   CREATE TABLE ref_plantmaterialstatusdescription
   (
       MARC_MMSTA VARCHAR(4), -- Code
       T141T_MTSTB VARCHAR(25) UTF8, -- Desc
       T141T_SPRAS VARCHAR(1) UTF8
   )
*/


   MERGE INTO ref_plantmaterialstatusdescription dst
   USING (SELECT T141T_MMSTA, T141T_MTSTB, T141T_SPRAS FROM T141T__XBI) src
   ON ( dst.MARC_MMSTA= src.T141T_MMSTA
        AND dst.T141T_SPRAS = src.T141T_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.T141T_MTSTB = src.T141T_MTSTB
   WHEN NOT MATCHED THEN INSERT (MARC_MMSTA, T141T_MTSTB, T141T_SPRAS) VALUES (src.T141T_MMSTA, src.T141T_MTSTB, src.T141T_SPRAS);
