/* 	Reference table for MARC_PRCTR based on CEPCT table
 SPRAS (Language) is part of key - even if in filter is set to 'E'
*/
   /*
   DROP TABLE IF EXISTS ref_profitcentername
   CREATE TABLE ref_profitcentername
   (
       MARC_PRCTR VARCHAR(10), -- Code
       CEPCT_KTEXT VARCHAR(30) UTF8, -- Desc
       CEPCT_SPRAS VARCHAR(7) UTF8

   )
*/

   MERGE INTO ref_profitcentername dst
   USING (SELECT CEPCT_PRCTR, CEPCT_KTEXT, CEPCT_SPRAS FROM CEPCT__XBI WHERE CEPCT_DATBI > current_date) src
   ON ( dst.MARC_PRCTR= src.CEPCT_PRCTR
        AND dst.CEPCT_SPRAS = src.CEPCT_SPRAS)
   WHEN MATCHED THEN UPDATE SET dst.CEPCT_KTEXT = src.CEPCT_KTEXT
   WHEN NOT MATCHED THEN INSERT (MARC_PRCTR, CEPCT_KTEXT, CEPCT_SPRAS) VALUES (src.CEPCT_PRCTR, src.CEPCT_KTEXT, src.CEPCT_SPRAS);
