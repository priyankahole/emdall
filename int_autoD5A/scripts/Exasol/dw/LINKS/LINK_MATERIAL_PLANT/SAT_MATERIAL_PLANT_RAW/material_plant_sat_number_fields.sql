/* SAT Material-Plant NUMBER table population */

	/* drop table if exists sat_material_plant_number
	create table sat_material_plant_number (
			MARC_MATNR_WERKS_HSH varchar(32),
			MARC_MATNR_WERKS_LDTS timestamp,
			MARC_MATNR_WERKS_LEDTS timestamp,
			MARC_MATNR_WERKS_RSRC varchar(50),
			MARC_MATNR_WERKS_NUMBER_HSH_DIFF varchar(32),
			MARC_PREND date,
			MARC_BSTMI DECIMAL(13,3),
			MARC_BWESB DECIMAL(18,4),
			MARC_GLGMG DECIMAL(18,4),
			MARC_UMLMC DECIMAL(18,4),
			MARC_TRAME DECIMAL(18,4),
			MARC_UEETO DECIMAL(18,4),
			MARC_UNETO DECIMAL(18,4),
			MARC_AUSSS DECIMAL(18,4),
			MARC_BEARZ DECIMAL(18,4),
			MARC_BSTFE DECIMAL(18,4),
			MARC_EISBE DECIMAL(18,4),
			MARC_LOSGR DECIMAL(18,4),
			MARC_MINBE DECIMAL(18,4),
			MARC_PLIFZ DECIMAL(18,4),
			MARC_BSTMA DECIMAL(18,4),
			MARC_BSTRF DECIMAL(18,4),
			MARC_DZEIT DECIMAL(18,4),
			MARC_EISLO DECIMAL(18,4),
			MARC_MABST DECIMAL(18,4),
			MARC_MAXLZ DECIMAL(18,4),
			MARC_PRFRQ DECIMAL(18,4),
			MARC_WEBAZ DECIMAL(18,4),
			MARC_WZEIT DECIMAL(18,4)
			) */

	/* insert new records and changed ones */
	insert into sat_material_plant_number ( MARC_MATNR_WERKS_HSH, MARC_MATNR_WERKS_LDTS, MARC_MATNR_WERKS_RSRC, MARC_MATNR_WERKS_NUMBER_HSH_DIFF,
											MARC_PREND, MARC_BSTMI, MARC_BWESB, MARC_GLGMG, MARC_UMLMC, MARC_TRAME, MARC_UEETO, MARC_UNETO, MARC_AUSSS, MARC_BEARZ, MARC_BSTFE, MARC_EISBE,
											MARC_LOSGR, MARC_MINBE, MARC_PLIFZ, MARC_BSTMA, MARC_BSTRF, MARC_DZEIT, MARC_EISLO, MARC_MABST, MARC_MAXLZ, MARC_PRFRQ, MARC_WEBAZ, MARC_WZEIT)
	select st1.MARC_MATNR_WERKS_HSH,
		   st1.MARC_MATNR_WERKS_LDTS,
		   st1.MARC_MATNR_WERKS_RSRC,
		   st1.MARC_MATNR_WERKS_NUMBER_HSH_DIFF,
	       st1.MARC_PREND, st1.MARC_BSTMI, st1.MARC_BWESB, st1.MARC_GLGMG, st1.MARC_UMLMC, st1.MARC_TRAME, st1.MARC_UEETO, st1.MARC_UNETO, st1.MARC_AUSSS, st1.MARC_BEARZ, st1.MARC_BSTFE, st1.MARC_EISBE,
		   st1.MARC_LOSGR, st1.MARC_MINBE, st1.MARC_PLIFZ, st1.MARC_BSTMA, st1.MARC_BSTRF, st1.MARC_DZEIT, st1.MARC_EISLO, st1.MARC_MABST, st1.MARC_MAXLZ, st1.MARC_PRFRQ, st1.MARC_WEBAZ, st1.MARC_WZEIT
	from marc__xbi_hash_stg as st1
	      left outer join sat_material_plant_number as s1
	                   on     st1.MARC_MATNR_WERKS_HSH = s1.MARC_MATNR_WERKS_HSH
	                      and s1.MARC_MATNR_WERKS_LEDTS is NULL
	where (
	        s1.MARC_MATNR_WERKS_HSH is null OR
	        (
	              st1.MARC_MATNR_WERKS_NUMBER_HSH_DIFF != s1.MARC_MATNR_WERKS_NUMBER_HSH_DIFF
	          AND s1.MARC_MATNR_WERKS_LDTS = (
			                                        select max(z.MARC_MATNR_WERKS_LDTS)
			                                        from sat_material_plant_number z
			                                        where s1.MARC_MATNR_WERKS_HSH = z.MARC_MATNR_WERKS_HSH
			                                       )
	        )
	      );


	/* update Load End Date */

	/* create table upd_sat_end_date_loop_s9
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp
	  )
	select * from exa_all_tables where lower(table_name) like 'upd_sat_end_date_loop%'
	*/

	truncate table upd_sat_end_date_loop_s9;
	insert into upd_sat_end_date_loop_s9 (BUSKEY_HSH, BUSKEY_LDTS)
	select s1.MARC_MATNR_WERKS_HSH,
	       s1.MARC_MATNR_WERKS_LDTS
	from sat_material_plant_number as s1
	where (
	            s1.MARC_MATNR_WERKS_LEDTS is null
	        and (
	              2 <= (
	                     select count(y.MARC_MATNR_WERKS_HSH)
	                     from sat_material_plant_number as y
	                     where   y.MARC_MATNR_WERKS_HSH = s1.MARC_MATNR_WERKS_HSH
	                         AND y.MARC_MATNR_WERKS_LEDTS is null
	                   )
	            )
	      );

	/* get min load end date based on a partial cross join */

	/* create table upd_sat_end_date_cross_s9
	  (
	    BUSKEY_HSH varchar(32),
	    BUSKEY_LDTS timestamp ,
	    BUSKEY_LEDTS timestamp
	  )
	*/

	truncate table upd_sat_end_date_cross_s9;
	insert into upd_sat_end_date_cross_s9 (BUSKEY_HSH, BUSKEY_LDTS, BUSKEY_LEDTS)
	select BUSKEY_HSH, BUSKEY_LDTS, min(LEDTS) - interval '0.001' SECOND as BUSKEY_LEDTS
	from (
			select t1.BUSKEY_HSH, t1.BUSKEY_LDTS, t2.BUSKEY_LDTS AS LEDTS
			from upd_sat_end_date_loop_s9 t1
					inner join upd_sat_end_date_loop_s9 t2 on t1.BUSKEY_HSH = t2.BUSKEY_HSH
			and t1.BUSKEY_LDTS < t2.BUSKEY_LDTS
		) tt
	group by BUSKEY_HSH, BUSKEY_LDTS;

	/* update SAT Load End Date */
	update sat_material_plant_number dst
	   set dst.MARC_MATNR_WERKS_LEDTS = src.BUSKEY_LEDTS
	  from sat_material_plant_number dst
				inner join upd_sat_end_date_cross_s9 src on dst.MARC_MATNR_WERKS_HSH = src.BUSKEY_HSH
	      												and dst.MARC_MATNR_WERKS_LDTS = src.BUSKEY_LDTS;
