
/**************************************************************************************************************/
/*   Script         : vw_bi_custom_post_vendor.sql                   		       		 */
/*   Author         : Nicoleta                                                                          	*/
/*   Created On     : Feb 2015                                                                                */
/*   Description    : Post Vendor script for Merck                      			*/

/*********************************************Change History*******************************************************/
/*  Date             By         Version	    Desc                                                            */
/*                                                                                                         */
/******************************************************************************************************************/





drop table if exists tmp_upd_diversitybyvendor_merck_v3;
create table tmp_upd_diversitybyvendor_merck_v3
as select
vendornumber, max(diverse) as diverse, max(small) as small, max(SDB) as SDB,
max(VBE) as VBE, max(MBE) as MBE, max(WBE) as WBE, max(mwbe) as mwbe,
max(dvbe) as dvbe, max(glbt) as glbt, max(disabled) as disabled
,max(dbe) as dbe,max(sme_small) as sme_small,max(sme_micro) as sme_micro,max(sme_medium) as  sme_medium
from diversitybyvendor_merck_v4
group by vendornumber;

update dim_vendor
set diverse_merck = 'Not Set'
where diverse_merck <> 'Not Set';

update dim_vendor v
set v.diverse_merck = ifnull(u.diverse,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.diverse_merck <> ifnull(u.diverse,'Not Set');

update dim_vendor
set small_merck = 'Not Set'
where small_merck <> 'Not Set';


update dim_vendor v
set v.small_merck = ifnull(u.small,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.small_merck <> ifnull(u.small,'Not Set');

update dim_vendor
set sdb_merck = 'Not Set'
where sdb_merck <> 'Not Set';

/* DBE = SDB */
update dim_vendor v
set v.sdb_merck = ifnull(u.dbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.sdb_merck <> ifnull(u.dbe,'Not Set');

update dim_vendor
set vbe_merck = 'Not Set'
where vbe_merck <> 'Not Set';

update dim_vendor v
set v.vbe_merck= ifnull(u.vbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.vbe_merck <> ifnull(u.vbe,'Not Set');

update dim_vendor
set mbe_merck = 'Not Set'
where mbe_merck <> 'Not Set';

update dim_vendor v
set v.mbe_merck = ifnull(u.mbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.mbe_merck <> ifnull(u.mbe,'Not Set');

update dim_vendor
set wbe_merck = 'Not Set'
where wbe_merck <> 'Not Set';

update dim_vendor v
set v.wbe_merck = ifnull(u.wbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.wbe_merck <> ifnull(u.wbe,'Not Set');


update dim_vendor
set mwbe_merck = 'Not Set'
where mwbe_merck <> 'Not Set';


update dim_vendor v
set v.mwbe_merck = ifnull(u.mwbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.mwbe_merck <> ifnull(u.mwbe,'Not Set');

update dim_vendor
set dvbe_merck = 'Not Set'
where dvbe_merck <> 'Not Set';

update dim_vendor v
set v.dvbe_merck = ifnull(u.dvbe,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.dvbe_merck <> ifnull(u.dvbe,'Not Set');

update dim_vendor
set glbt_merck = 'Not Set'
where glbt_merck <> 'Not Set';

update dim_vendor v
set v.glbt_merck = ifnull(u.glbt,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.glbt_merck <> ifnull(u.glbt,'Not Set');

update dim_vendor
set disabled_merck = 'Not Set'
where disabled_merck <> 'Not Set';

update dim_vendor v
set v.disabled_merck = ifnull(u.disabled,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.disabled_merck <> ifnull(u.disabled,'Not Set');

update dim_vendor
set sme_small = 'Not Set'
where sme_small <> 'Not Set';

update dim_vendor v
set v.sme_small = ifnull(u.sme_small,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.sme_small <> ifnull(u.sme_small,'Not Set');

update dim_vendor
set sme_micro = 'Not Set'
where sme_micro <> 'Not Set';

update dim_vendor v
set v.sme_micro = ifnull(u.sme_micro,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.sme_micro <> ifnull(u.sme_micro,'Not Set');

update dim_vendor
set sme_medium = 'Not Set'
where sme_medium <> 'Not Set';

update dim_vendor v
set v.sme_medium = ifnull(u.sme_medium,'Not Set')
from tmp_upd_diversitybyvendor_merck_v3 u, dim_vendor v
where v.vendornumber = lpad(u.vendornumber,10,'0')
and v.sme_medium <> ifnull(u.sme_medium,'Not Set');

update dim_vendor
set functionalgroup_merck = 'Not Set'
where functionalgroup_merck <> 'Not Set';


/* 05.03.2015 */
update  dim_vendor d
set d.functionalgroup_merck = ifnull(v.functionalgroup,'Not Set')
from (SELECT DISTINCT VENDORNUMBER,FIRST_VALUE(FUNCTIONALGROUP) OVER (PARTITION BY VENDORNUMBER ORDER BY FUNCTIONALGROUP DESC) AS FUNCTIONALGROUP FROM vendorfunctionalgroup_merck) v, dim_vendor d
where d.vendornumber = lpad(v.vendornumber,10,'0')
and d.functionalgroup_merck <> ifnull(v.functionalgroup,'Not Set');

/*Alin APP-7276 start 25 sept*/
update dim_vendor
set DBE_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'DBE'
and DBE_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set DIVERSE_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'DIVERSE'
and DIVERSE_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set DVBE_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'DVBE'
and DVBE_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set LGBT_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'LGBT'
and LGBT_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set MBE_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'MBE'
and MBE_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set MWBE_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'MWBE'
and MWBE_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set SMALL_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'SMALL'
and SMALL_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set SME_MEDIUM_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'SME_MEDIUM'
and SME_MEDIUM_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set SME_MICRO_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'SME_MICRO'
and SME_MICRO_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set SME_SMALL_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'SME_SMALL'
and SME_SMALL_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set VBE_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'VBE'
and VBE_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set WBE_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'WBE'
and WBE_merck_v2 <> ifnull('X', 'Not Set');

update dim_vendor
set WBE_NONUS_merck_v2 = ifnull('X', 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
and z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE = 'WBE_NONUS'
and WBE_NONUS_merck_v2 <> ifnull('X', 'Not Set');

/*
update dim_vendor v
set minority_certificate = ifnull(z.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE, 'Not Set') 
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR
*/

/*start - replace the problematic extraction Z1MM_FO_MINCERT*/
merge into Z1MM_FO_MINCERT_CODE z
using
(select distinct Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_CODE,
row_number() over(order by Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_CODE) as rn
from Z1MM_FO_MINCERT_CODE
) t
on z.Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_CODE = t.Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_CODE
when matched then update set
z.rn = t.rn;

merge into Z1MM_FO_MINCERT_DESCR z
using
(select distinct Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_DESCR,
row_number() over(order by Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_DESCR) as rn
from Z1MM_FO_MINCERT_DESCR
) t
on z.Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_DESCR = t.Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_DESCR
when matched then update set
z.rn = t.rn;

delete from Z1MM_FO_MINCERT;

INSERT INTO Z1MM_FO_MINCERT(Z1MM_FO_MINCERT_MINORITY_CERTIFICATE,Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_DESCR)
select a.Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_code, b.Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_DESCR from Z1MM_FO_MINCERT_CODE a, Z1MM_FO_MINCERT_descr b
where a.rn = b.rn;

/*end - replace the problematic extraction Z1MM_FO_MINCERT*/

/*update dim_vendor v
set minority_certificate_descr = ifnull(z1.Z1MM_FO_MINCERT_MINORITY_CERTIFICATE_DESCR, 'Not Set') 
from dim_vendor v, LFA1 l, Z1MM_FO_MINCERT z1, Z1MM_FO_DIVERS Z2
where
z1.Z1MM_FO_MINCERT_MINORITY_CERTIFICATE = Z2.Z1MM_FO_DIVERS_MINORITY_CERTIFICATE
and l.LIFNR = z2.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = l.LIFNR*/

update dim_vendor v
set func_group = ifnull(z.Z1MM_FO_OWNERLST_FUNC_GROUP, 'Not Set') 
from dim_vendor v, LFA1 l, Z1MM_FO_OWNERLST z
where l.LIFNR = z.Z1MM_FO_OWNERLST_LIFNR
and v.vendornumber = l.LIFNR;

update dim_vendor v
set func_group_descr = ifnull(z1.Z1MM_FO_FUNCGRP_FUNC_GROUP_DESCR, 'Not Set') 
from dim_vendor v, LFA1 l, Z1MM_FO_FUNCGRP z1, Z1MM_FO_OWNERLST Z2
where
Z1MM_FO_OWNERLST_FUNC_GROUP = Z1MM_FO_FUNCGRP_FUNC_GROUP
and l.LIFNR = z2.Z1MM_FO_OWNERLST_LIFNR
and v.vendornumber = l.LIFNR;

/*Alin 2 June 2018 APP-7276*/
update dim_vendor
set hvn_parent = ifnull(Z1MM_FO_HVN_HVN_PARENT, 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_HVN z
where l.LIFNR = z.Z1MM_FO_HVN_LIFNR
and v.vendornumber = ifnull(l.LIFNR, 'Not Set')
and hvn_parent <> ifnull(Z1MM_FO_HVN_HVN_PARENT, 'Not Set');

update dim_vendor
set hvn_child = ifnull(Z1MM_FO_HVN_HVN_CHILD, 'Not Set')
from dim_vendor v, LFA1 l, Z1MM_FO_HVN z
where l.LIFNR = z.Z1MM_FO_HVN_LIFNR
and v.vendornumber = ifnull(l.LIFNR, 'Not Set')
and hvn_child <> ifnull(Z1MM_FO_HVN_HVN_CHILD, 'Not Set');

merge into dim_vendor d
using(
select distinct dim_vendorid, z.Z1MM_FO_DIVERS_REGION
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and v.vendornumber = ifnull(l.LIFNR, 'Not Set')
)t
on d.dim_vendorid = t.dim_vendorid
when matched then update
set d.region = ifnull(t.Z1MM_FO_DIVERS_REGION, 'Not Set')
where d.region <> ifnull(t.Z1MM_FO_DIVERS_REGION, 'Not Set');

merge into dim_vendor d
using(
select distinct dim_vendorid, Z1MM_FO_REGION_REGION_DESCR
from dim_vendor v, LFA1 l, Z1MM_FO_DIVERS z, Z1MM_FO_REGION R
where l.LIFNR = z.Z1MM_FO_DIVERS_LIFNR
and z.Z1MM_FO_DIVERS_REGION = r.Z1MM_FO_REGION_REGION
and v.vendornumber = ifnull(l.LIFNR, 'Not Set')
)t
on d.dim_vendorid = t.dim_vendorid
when matched then update
set region_descr = ifnull(Z1MM_FO_REGION_REGION_DESCR, 'Not Set')
where region_descr <> ifnull(Z1MM_FO_REGION_REGION_DESCR, 'Not Set');