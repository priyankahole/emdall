
/*---------------------------------------Custom proc starts here---------------------------*/

/* Run this just before populating fact_planorder statement  */
/* ( for another proc, create a similar custom proc and run that before the actual proc ) */


/* Populate tmp_getStdPrice - This will change for each proc where this function is required*/

DELETE FROM tmp_getStdPrice
WHERE fact_script_name = 'bi_populate_salesorder_fact';

INSERT INTO tmp_getStdPrice
( 
pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
  select DISTINCT
        pl.CompanyCode,
        pl.PlantCode,
        VBAP_MATNR,
        dd.FinancialYear,
		dd.FinancialMonthNumber, 
        CASE WHEN VBAP_UMVKZ = 0 THEN 1 ELSE VBAP_UMVKZ END,
        CASE WHEN VBAP_UMVKN = 0 THEN 1 ELSE VBAP_UMVKN END,
        dd_SalesDocNo PoNumber,
        /*0 punitprice,*/
		ifnull((VBAP_NETPR / (CASE WHEN ifnull(VBAP_KPEIN, 0) = 0 THEN 1 ELSE VBAP_KPEIN END)),0) punitprice,
	'bi_populate_salesorder_fact' fact_script_name
FROM fact_salesorder so, 
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	dim_date dd
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
	and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
	AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
	and dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode;
	
UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE fact_script_name = 'bi_populate_salesorder_fact';
	
	
	