
/* Custom proc starts here */

/* Run this just before bi_populate_shortage_fact  */
/* ( for another proc, create a similar custom proc and run that before the actual proc ) */

DELETE FROM tmp_getStdPrice
WHERE fact_script_name = 'bi_populate_shortage_fact';
      
/* Populate tmp_getStdPrice  This will change for each proc where this function is required*/

DROP TABLE IF EXISTS tmp_resb_for_shortagefact;
create table tmp_resb_for_shortagefact
as
select RESB_RSNUM,RESB_RSPOS,min(rb.RESB_BDTER) as min_RESB_BDTER
FROM RESB rb
GROUP BY RESB_RSNUM,RESB_RSPOS;

DROP TABLE IF EXISTS tmp_resb_for_shortagefact_2;
CREATE TABLE tmp_resb_for_shortagefact_2
AS
SELECT distinct rb.*,rb2.min_RESB_BDTER
FROM RESB rb,tmp_resb_for_shortagefact rb2
WHERE rb.RESB_RSNUM = rb2.RESB_RSNUM
AND rb.RESB_RSPOS = rb2.RESB_RSPOS;



INSERT INTO tmp_getStdPrice
(
 pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
SELECT DISTINCT
pl.CompanyCode,
pl.PlantCode,
r.RESB_MATNR,
dt.FinancialYear,
dt.FinancialMonthNumber, RESB_UMREZ, RESB_UMREN, NULL,
ifnull((CASE WHEN ifnull(RESB_PEINH, 0) = 0 THEN 1 ELSE RESB_PEINH END),0),
'bi_populate_shortage_fact'
FROM 
fact_shortage_temp s ,
       dim_plant pl,
       tmp_resb_for_shortagefact_2 r,
       dim_date dt
WHERE     s.dd_ReservationNo <> 0 
       AND s.dd_ReservationItemNo <> 0 
       AND s.dd_ReservationNo = r.RESB_RSNUM
       AND s.dd_ReservationItemNo = r.RESB_RSPOS
	   AND s.dd_RecordType = ifnull(r.RESB_RSART, 'Not Set')
       AND dt.DateValue = r.min_RESB_BDTER
       AND dt.CompanyCode = pl.CompanyCode
       AND pl.PlantCode = r.RESB_WERKS
       AND pl.RowIsCurrent = 1;	   


UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE fact_script_name = 'bi_populate_shortage_fact' ;

DROP TABLE IF EXISTS tmp_resb_for_shortagefact;
DROP TABLE IF EXISTS tmp_resb_for_shortagefact_2;
