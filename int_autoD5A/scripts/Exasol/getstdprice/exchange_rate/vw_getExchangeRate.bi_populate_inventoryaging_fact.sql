/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_inventoryaging_fact';



INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP) as pDate, 'USD' pToCurrency, NULL exchangeRate,'bi_populate_inventoryaging_fact'
FROM MARD a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MARD_WERKS = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP) as pDate, 'USD' pToCurrency, NULL  exchangeRate,'bi_populate_inventoryaging_fact'
FROM MCHB a, dim_plant p,dim_company c,dim_Currency dc
WHERE a.MCHB_WERKS = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name)
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP) as pDate, 'USD' pToCurrency, NULL  exchangeRate,'bi_populate_inventoryaging_fact'
FROM MSLB a, dim_plant p,dim_company c,dim_Currency dc
WHERE a.MSLB_WERKS = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP) as pDate, 'USD' pToCurrency, NULL  exchangeRate,'bi_populate_inventoryaging_fact'
FROM MSKU a, dim_plant p,dim_company c,dim_Currency dc
WHERE a.MSKU_WERKS = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP) as pDate, 'USD' pToCurrency, NULL exchangeRate,'bi_populate_invaging_fact_salesorderstock'
FROM MSKA a,dim_plant p,dim_company c,dim_Currency dc
WHERE a.MSKA_WERKS = p.PlantCode
AND c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
AND dc.CurrencyCode = c.Currency;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name )
SELECT DISTINCT dc.CurrencyCode as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP) as pDate, 'USD' pToCurrency, NULL exchangeRate,'bi_populate_inventoryaging_fact'
FROM dim_Currency dc;




UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_inventoryaging_fact';


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_inventoryaging;
create table tmp_getExchangeRate1_nodups_inventoryaging
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_inventoryaging_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_inventoryaging_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_inventoryaging;

drop table tmp_getExchangeRate1_nodups_inventoryaging;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');

