/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_materialtransaction_fact';

Drop table if exists tmp_globalcur;

Declare global temporary table tmp_globalcur(
	pGlobalCurrency)
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD'),varchar(3)) ON COMMIT PRESERVE ROWS;



INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT co.Currency pFromCurrency,ansidate(LOCAL_TIMESTAMP) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_materialtransaction_fact'
FROM dim_part p 
inner join dim_plant pl on p.plant = pl.PlantCode
inner join dim_company co on pl.CompanyCode = co.CompanyCode,
tmp_globalcur;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT co.Currency pFromCurrency,ansidate(LOCAL_TIMESTAMP) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_materialtransaction_fact'
FROM dim_partsales p 
inner join dim_salesorg so on p.SalesOrgCode = so.SalesOrgCode
inner join dim_company co on so.CompanyCode = co.CompanyCode,
tmp_globalcur;

drop table if exists tmp_getExchangeRate1_nodups_sof;
create table tmp_getExchangeRate1_nodups_sof
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_materialtransaction_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_materialtransaction_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_sof;

drop table tmp_getExchangeRate1_nodups_sof;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
