
/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_prodorder_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT AUFK_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,AUFK_AEDAT pDate,NULL exchangeRate,'bi_populate_prodorder_fact'
FROM AFKO_AFPO_AUFK mhi;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT AUFK_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,ansidate(LOCAL_TIMESTAMP) pDate,NULL exchangeRate,'bi_populate_prodorder_fact'
FROM AFKO_AFPO_AUFK mhi;



UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_prodorder_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT AUFK_WAERS,dc.Currency,NULL,AUFK_AEDAT,NULL exchangeRate,'bi_populate_prodorder_fact'
FROM AFKO_AFPO_AUFK mhi,dim_company dc
WHERE dc.CompanyCode = mhi.AUFK_BUKRS;


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups;
create table tmp_getExchangeRate1_nodups
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_prodorder_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_prodorder_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups;

drop table tmp_getExchangeRate1_nodups;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');