

/* ##################################################################################################################
  
     Script         : gw_getExchangeRate.bi_populate_afs_salesorder_fact.sql
     Author         : Lokesh
     Created On     : 7-Jul-2013
  
  
     Description    : Exchange rate script for afs_salesorder_fact - after changes for parallel processing to previous script
  
     Change History
     Date            By        Version           Desc
     9-Sep-2013      Lokesh    1.2		 Chgs corresponding to Currency and Exchange rate changes in vw_bi_populate_afs_salesorder_fact script
     7-Jul-2013      Lokesh    1.1               Brought in sync with latest bi_populate_afs_salesorder_fact script
     7-Jul-2013      Lokesh    1.0               Modified the previous afs sof exchange rate script to use fact_script_name flag. 
							Removed drop-create. Renamed ( removed _custom from name )
#################################################################################################################### */

/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

Drop table if exists tmp_globalcur_afs_sof;

Declare global temporary table tmp_globalcur_afs_sof(
        pGlobalCurrency)
AS
Select  CAST(ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD'),varchar(3)) ON COMMIT PRESERVE ROWS;


DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_salesorder_fact';

/* This section populates tmp_getExchangeRate1. Only this will change for different procs, remaining code will remain the same */


/* Tran->local,gbl,stat curr from VBAK_VBAP_VBEP*/
INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, co.currency pToCurrency, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

/* Tran ->Global curr from VBAK_VBAP_VBEP*/
INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur_afs_sof
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur_afs_sof
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

/* Tran -> Statistical Currency from VBAK_VBAP_VBEP. Note that this has vbap_stcur as pFromExchangeRate */
/* pFromExchangeRate vbap_stcur is not the correct rate for vbap_waerk --> vbak_stwae . This is still populating tmp_getexchangerate1, but not used anywhere
in the fact script */

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

/* Added this to handle statistic currency rate as above was giving incorrect results */

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, a.VBAK_STWAE pToCurrency,NULL pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;



/* Tran->local,gbl,stat curr from VBAK_VBAP */

/* Tran->local curr from VBAK_VBAP_VBEP*/
INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT)  as pDate, co.currency pToCurrency,NULL pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;


/* Tran->gbl curr from VBAK_VBAP_VBEP*/
INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT)  as pDate, pGlobalCurrency pToCurrency,NULL pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur_afs_sof
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ANSIDATE(LOCAL_TIMESTAMP)  as pDate, pGlobalCurrency pToCurrency,NULL pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur_afs_sof
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;



/* Tran->stat curr from VBAK_VBAP_VBEP. Note that this has vbap_stcur as pFromExchangeRate*/

/* pFromExchangeRate vbap_stcur is not the correct rate for vbap_waerk --> vbak_stwae . This is still populating tmp_getexchangerate1, but not used anywhere
in the fact script */


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT)  as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT)  as pDate, a.VBAK_STWAE pToCurrency,NULL pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;



/* Not sure if PRSDT is to be used for stat curr or not. So, populating it with VBAK_AUDAT as well */


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,VBAK_AUDAT as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,VBAK_AUDAT as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'bi_populate_afs_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode;

/* Added this query as we have only 2 cases : 0 and nonzero. No use of NULL from exchgrate, it was creating dups as seen in merck issue*/
UPDATE tmp_getExchangeRate1
SET pFromExchangeRate = 0 where pFromExchangeRate is null AND fact_script_name = 'bi_populate_afs_salesorder_fact';


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_afs_sof;
create table tmp_getExchangeRate1_nodups_afs_sof
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_salesorder_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_afs_salesorder_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_afs_sof;

drop table if exists tmp_getExchangeRate1_nodups_afs_sof;

CALL VECTORWISE( COMBINE 'tmp_getExchangeRate1');
