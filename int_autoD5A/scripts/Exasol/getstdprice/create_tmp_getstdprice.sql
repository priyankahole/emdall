
/* Table definition for tmp_getstdprice */ 

DROP TABLE IF EXISTS tmp_getstdprice;

CREATE TABLE tmp_getstdprice
(
pcompanycode                     varchar(4),
pplant                           varchar(4),
pmaterialno                      varchar(18),
pfiyear                          int,
pperiod                          int,
vumrez                           int,
vumren                           int,
ponumber                         varchar(20),
punitprice                       decimal(22, 5),
rppvprice                        decimal(22, 5),
pprevfiyear                      int,
pprevperiod                      int,
standardprice                    decimal(22, 5),
pumrez_umren                     decimal(22, 5),
pprevdates                       varchar(45),
pprevfromdate                    ansidate,                  
pprevtodate                      ansidate,                  
flag_upd                         varchar(20),
pfiyear_pperiod                  bigint,
fact_script_name                 varchar(60),
processed_flag                   varchar(1)
);
