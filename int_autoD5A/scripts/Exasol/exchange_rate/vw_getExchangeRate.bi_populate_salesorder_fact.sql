/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

/* update from item data if it exists (posnr <> 0), otherwise update from header data (posnr = 0) */
  UPDATE vbak_vbap_vbep p
  SET p.PRSDT = k.VBKD_PRSDT
  FROM vbak_vbap_vbkd k,vbak_vbap_vbep p
  where p.vbak_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0
  and ifnull(p.PRSDT,'1900-01-01') <> ifnull(k.VBKD_PRSDT,'1900-01-01');

  UPDATE vbak_vbap p
  SET p.PRSDT = k.VBKD_PRSDT
  FROM vbak_vbap_vbkd k,vbak_vbap p
  where p.vbap_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0
  AND ifnull(p.PRSDT,'1900-01-01') <> ifnull(k.VBKD_PRSDT,'1900-01-01');

/* Update item data if it exists. So this will overwrite updates from header, where a match is found */


  UPDATE vbak_vbap_vbep p
  SET p.PRSDT = k.VBKD_PRSDT
  FROM vbak_vbap_vbkd k, vbak_vbap_vbep p
  where p.vbak_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
  AND ifnull(p.PRSDT,'1900-01-01') <> ifnull(k.VBKD_PRSDT,'1900-01-01');

  UPDATE vbak_vbap p
  SET p.PRSDT = k.VBKD_PRSDT
  FROM vbak_vbap_vbkd k,vbak_vbap p
  where p.vbap_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
  AND ifnull(p.PRSDT,'1900-01-01') <> ifnull(k.VBKD_PRSDT,'1900-01-01');



DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

Drop table if exists tmp_globalcur_fact_salesorder;

create table tmp_globalcur_fact_salesorder AS
Select  convert(varchar(3), ifnull((SELECT property_value
									 FROM systemproperty
									WHERE property = 'customer.global.currency'),
								  'USD')) pGlobalCurrency;



INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, co.currency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur_fact_salesorder
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,current_date as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co,tmp_globalcur_fact_salesorder
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;



INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, a.VBAK_STWAE pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, co.currency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co, tmp_globalcur_fact_salesorder
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency, current_date as pDate, pGlobalCurrency pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co, tmp_globalcur_fact_salesorder
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,ifnull(a.PRSDT,VBAK_AUDAT) as pDate, a.VBAK_STWAE pToCurrency, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;

/* pFromExchangeRate vbap_stcur is not the correct rate for vbap_waerk --> vbak_stwae . This is still populating tmp_getexchangerate1, but not used anywhere in the fact script */


INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency,pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,VBAK_AUDAT as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP_VBEP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;

INSERT INTO tmp_getExchangeRate1 ( pFromCurrency, pDate, pToCurrency, pFromExchangeRate, exchangeRate,fact_script_name  )
SELECT DISTINCT VBAP_WAERK as pFromCurrency,VBAK_AUDAT as pDate, a.VBAK_STWAE pToCurrency,vbap_stcur pFromExchangeRate, NULL exchangeRate,'bi_populate_salesorder_fact'
FROM VBAK_VBAP a,
     Dim_Plant pl,
     Dim_Company co
WHERE pl.PlantCode = ifnull(VBAP_WERKS,'Not Set') AND co.CompanyCode = pl.CompanyCode;

/* For all from-to curr combinations, add rows with fromexchangerate = 0 as well */
INSERT INTO tmp_getExchangeRate1( pFromCurrency, pDate, pToCurrency, exchangeRate, pFromExchangeRate,fact_script_name)
SELECT pFromCurrency, pDate, pToCurrency, exchangeRate,0,'bi_populate_salesorder_fact'
FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

/* Added this query as we have only 2 cases : 0 and nonzero. No use of NULL from exchgrate, it was creating dups as seen in merck issue*/
UPDATE tmp_getExchangeRate1
SET pFromExchangeRate = 0 where pFromExchangeRate is null AND fact_script_name = 'bi_populate_salesorder_fact';

drop table if exists tmp_getExchangeRate1_nodups_sof;
create table tmp_getExchangeRate1_nodups_sof
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_salesorder_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_sof;

drop table tmp_getExchangeRate1_nodups_sof;
