/* Start of custom exchange rate proc. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchase_requisition_fact';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT EBAN_WAERS ,'USD' pToCurrency, NULL pFromExchangeRate, EBAN_BADAT pDate, NULL exchangeRate,'bi_populate_purchase_requisition_fact'
from EBAN;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT EBAN_WAERS,'USD' pToCurrency, NULL pFromExchangeRate,current_date pDate, NULL exchangeRate,'bi_populate_purchase_requisition_fact'
from EBAN;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_purchase_requisition_fact';


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_fact_purchase_requisition_nodups;
create table tmp_getExchangeRate1_fact_purchase_requisition_nodups
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchase_requisition_fact';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_purchase_requisition_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_fact_purchase_requisition_nodups;

drop table if exists tmp_getExchangeRate1_fact_purchase_requisition_nodups;
