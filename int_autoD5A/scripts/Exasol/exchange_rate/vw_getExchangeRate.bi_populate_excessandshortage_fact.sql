
/* Start of custom exchange rate proc for bi_populate_mrp_fact. This contains the step for population of tmp_getExchangeRate1 and will change for each proc */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_excessandshortage_fact';

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT EBAN_WAERS pFromCurrency,'USD' pToCurrency,null pFromExchangeRate,EBAN_BADAT pDate,'bi_populate_excessandshortage_fact' fact_script_name
FROM eban;

UPDATE tmp_getExchangeRate1 t
SET t.pToCurrency = ifnull(s.property_value,'USD')
FROM tmp_getExchangeRate1 t CROSS JOIN (select property_value from systemproperty where property = 'customer.global.currency')  s
WHERE t.fact_script_name = 'bi_populate_excessandshortage_fact' ;

UPDATE tmp_getExchangeRate1
set exchangeRate = NULL
WHERE fact_script_name = 'bi_populate_excessandshortage_fact';

