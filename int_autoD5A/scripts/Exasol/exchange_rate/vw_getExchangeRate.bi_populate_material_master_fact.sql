


/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Andrei */
/*   Created On     : 15 May 2017 */
/*   Description    : Current version  Exasol syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                               */
                          
/******************************************************************************************************************/


/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_materialmaster';


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT p.currency ,'USD',null pFromExchangeRate,current_date,'vw_bi_populate_materialmaster'
FROM fact_materialmaster f INNER JOIN dim_part p ON f.dim_materialmasterid = p.dim_partid;

drop table if exists dates_for_materialmaster_exchangerate;
create table dates_for_materialmaster_exchangerate as
select concat(t_year.v_year,'-',t_month.v_month,'-',t_day.v_day) as v_date
from 
(select '01' as v_day) t_day
cross join
(
select '01' as v_month union select '06' union
select '07' union select '08' union select '09' union
select '12') t_month
cross join(
select '2016' as v_year union select '2017' union select '2018' ) t_year
order by 1;


INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT p.currency ,'USD',null pFromExchangeRate,dd.v_date,'vw_bi_populate_materialmaster'
FROM fact_materialmaster f INNER JOIN dim_part p ON f.dim_materialmasterid = p.dim_partid
CROSS JOIN dates_for_materialmaster_exchangerate dd;

drop table if exists dates_for_materialmaster_exchangerate;
/*
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT p.currency ,'USD',null pFromExchangeRate,'2017-08-01','vw_bi_populate_materialmaster'
FROM fact_materialmaster f INNER JOIN dim_part p ON f.dim_materialmasterid = p.dim_partid */


UPDATE tmp_getExchangeRate1 t
   SET t.pToCurrency = ifnull(sys.property_value, 'USD')
  FROM systemproperty sys, tmp_getExchangeRate1 t
 WHERE t.fact_script_name = 'vw_bi_populate_materialmaster'
   AND sys.property = 'customer.global.currency';



/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_materialmaster;
create table tmp_getExchangeRate1_nodups_materialmaster
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_materialmaster';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_materialmaster';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_materialmaster;

drop table tmp_getExchangeRate1_nodups_materialmaster;

