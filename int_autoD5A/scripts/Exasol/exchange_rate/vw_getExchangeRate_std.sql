/**************************************************************************************************************/
/*   Script         : vw_getExchangeRate_std   */
/*   Author         : Hiten */
/*   Created On     : 29 Jul 2013 */
/*   Description    : Current version  Vectorwise syntax   */
/*   Last Update    : 01 Jan 2014 */
/******************************************************************************************************************/

insert into tmp_getexchangerate1(pfromcurrency,ptocurrency,pfromexchangerate,pdate,vtype,fact_script_name,curr_eur_miss)
select tousd.pfromcurrency,'EUR' as ptocurrency,0 as pfromexchangerate,tousd.pdate,tousd.vtype,tousd.fact_script_name,'inserted' as curr_eur_miss from
(select * from tmp_getexchangerate1
where ptocurrency = 'USD' and pfromcurrency <> 'USD'
and pdate is not null) tousd
left join 
(select * from tmp_getexchangerate1
where ptocurrency = 'EUR' and pfromcurrency <> 'USD') toeur
on tousd.pfromcurrency = toeur.pfromcurrency
and tousd.fact_script_name = toeur.fact_script_name
and tousd.vtype = toeur.vtype
and tousd.pdate = toeur.pdate
where toeur.pfromcurrency is null;

UPDATE tmp_getExchangeRate1
set vReverse = 'N',
	updflag = 'N',
	processed_flag = 'Y',
	vFromCurr = pFromCurrency,
	vToCurr = pToCurrency,
	vFTfact = 0,
	vffact = 0,
	vtfact = 0
WHERE processed_flag IS NULL;      

UPDATE tmp_getExchangeRate1
SET exchangeRate = 0
WHERE exchangeRate is null
	AND processed_flag = 'Y' 
	AND updflag = 'N';    

UPDATE tmp_getExchangeRate1
SET pFromExchangeRate = 0
WHERE pFromExchangeRate is null
	AND processed_flag = 'Y' 
	AND updflag = 'N';    

UPDATE tmp_getExchangeRate1
SET exchangeRate = 1,
	updflag = 'Y'
WHERE pFromCurrency = pToCurrency
	AND processed_flag = 'Y' 
	AND updflag = 'N';


UPDATE tmp_getExchangeRate1
SET vReverse = 'Y',
	pFromExchangeRate = -1 * pFromExchangeRate,
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND pFromExchangeRate < 0
	AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 x
SET vReverse = 'Y',
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND NOT EXISTS ( SELECT tcurr_fcurr FROM tcurr y where y.tcurr_fcurr = x.pFromCurrency AND y.tcurr_tcurr = x.pToCurrency )
	AND EXISTS ( SELECT tcurr_fcurr FROM tcurr z where z.tcurr_fcurr = x.pToCurrency AND z.tcurr_tcurr = x.pFromCurrency )
	AND processed_flag = 'Y'
	AND vRefCur is null
	AND ifnull(pFromExchangeRate,0) = 0; /* added 01/24/2014 to fix varian PO 6512990 */ 


UPDATE  tmp_getExchangeRate1
SET vType = ifnull((select TCURV_KURST from tcurv ),'M'),
	vRefCur = (select TCURV_BWAER from tcurv ),
	vInvAllow = ifnull((select 'Y' from tcurv where TCURV_XINVR is not null),'N'),
	vTCURFExists = ifnull((select distinct 1 from TCURF ),0)
WHERE updflag = 'N'
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET vRefCur = null
WHERE updflag = 'N' 
	AND vRefCur = pToCurrency
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET vReverse = 'Y',
	pFromExchangeRate = -1 * pFromExchangeRate,
	vToCurr = pFromCurrency,
	vFromCurr = pToCurrency,
	vRefCur = null
WHERE updflag = 'N' 
	AND vReverse = 'N'
	AND vRefCur = pFromCurrency
	AND processed_flag = 'Y'	
	AND ifnull(pFromExchangeRate,0) = 0; /* added 01/24/2014 to fix varian PO 6512990 */ 
	
UPDATE tmp_getExchangeRate1
SET pFromCurrency = vFromCurr,
	pToCurrency = vToCurr
WHERE updflag = 'N' 
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1
SET vToCurr = vRefCur
WHERE updflag = 'N' 
	AND vRefCur is not null
	AND vRefCur <> vToCurr
	AND processed_flag = 'Y';


/***** Prepare and use TCURF *****/

drop table if exists tmp_tcurf;
CREATE table tmp_tcurf
AS
SELECT  TCURF_FCURR,TCURF_TCURR,TCURF_KURST,tcurf_gdatu,TCURF_FFACT,TCURF_TFACT,
cast(left((99999999 - tcurf_gdatu),4) || '-'|| left(right((99999999 - tcurf_gdatu),4),2) || '-'|| right((99999999 - tcurf_gdatu),2) AS date) cast_tcurf_gdatu
FROM TCURF;

/* Denormalize to get date ranges */

drop table if exists tmp_tcurf_denorm;
CREATE table tmp_tcurf_denorm
AS
SELECT t.*,cast_tcurf_gdatu as cast_tcurf_gdatu_to
FROM tmp_tcurf t;

DROP TABLE IF EXISTS TMP_UPDT_TCURF;
CREATE TABLE TMP_UPDT_TCURF
AS
SELECT X.ROWID RID, MIN(Y.cast_tcurf_gdatu) cast_tcurf_gdatu
FROM tmp_tcurf_denorm x,
	 tmp_tcurf y
WHERE 	    x.TCURF_FCURR = y.TCURF_FCURR
		AND x.TCURF_TCURR = y.TCURF_TCURR
		AND x.TCURF_KURST = y.TCURF_KURST
		AND y.cast_tcurf_gdatu > x.cast_tcurf_gdatu
GROUP BY X.ROWID;

UPDATE tmp_tcurf_denorm x
SET x.cast_tcurf_gdatu_to = y.cast_tcurf_gdatu
FROM tmp_tcurf_denorm x
	LEFT JOIN TMP_UPDT_TCURF y ON x.rowid = y.rid;
														
/* Now update the latest row with 31 Dec 9999 */

UPDATE tmp_tcurf_denorm x
SET cast_tcurf_gdatu_to = '9999-12-31'
WHERE cast_tcurf_gdatu_to IS NULL;

/**********/

/***** Prepare and use TCURR *****/

drop table if exists tmp_tcurr;
CREATE table tmp_tcurr
AS
SELECT  tcurr_fcurr,tcurr_tcurr,tcurr_kurst,tcurr_gdatu,tcurr_ffact,tcurr_tfact,tcurr_ukurs,
cast(left((99999999 - tcurr_gdatu),4) || '-' || left(right((99999999 - tcurr_gdatu),4),2) || '-' || right((99999999 - tcurr_gdatu),2) AS date) cast_tcurr_gdatu
FROM tcurr;

/* Denormalize to get date ranges */

drop table if exists tmp_tcurr_denorm;
CREATE table tmp_tcurr_denorm
AS
SELECT t.*,cast_tcurr_gdatu as cast_tcurr_gdatu_to
FROM tmp_tcurr t;

DROP TABLE IF EXISTS TMP_UPDT_TCURR;
CREATE TABLE TMP_UPDT_TCURR
AS
SELECT X.ROWID RID, MIN(Y.cast_tcurr_gdatu) cast_tcurr_gdatu
FROM tmp_tcurr_denorm x,
	 tmp_tcurr y
WHERE 	    x.tcurr_fcurr = y.tcurr_fcurr
		AND x.tcurr_tcurr = y.tcurr_tcurr
		AND x.tcurr_kurst = y.tcurr_kurst
		AND y.cast_tcurr_gdatu > x.cast_tcurr_gdatu
GROUP BY X.ROWID;

UPDATE tmp_tcurr_denorm x1
SET X1.cast_tcurr_gdatu_to = y1.cast_tcurr_gdatu
FROM tmp_tcurr_denorm x1
	LEFT JOIN TMP_UPDT_TCURR y1 ON x1.ROWID = y1.rid;

/* Now update the latest row with 31 Dec 9999 */
 
UPDATE tmp_tcurr_denorm x
SET cast_tcurr_gdatu_to = '9999-12-31'
WHERE cast_tcurr_gdatu_to IS NULL;

/**********/
/*
UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_tcurf_denorm t2, tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.processed_flag = 'Y' */


 merge into tmp_getExchangeRate1 g 
 using
 (select distinct g.pFromCurrency, t2.TCURF_FFACT, t2.TCURF_TFACT, g.vtocurr
 FROM tmp_getExchangeRate1 g,tmp_tcurf_denorm t2
 WHERE g.pFromCurrency = t2.TCURF_FCURR 
 AND g.pToCurrency = t2.TCURF_TCURR 
 AND g.vType = t2.TCURF_KURST
 AND g.pDate >= t2.cast_tcurf_gdatu 
 AND g.pDate < t2.cast_tcurf_gdatu_to
 AND g.updflag = 'N' 
 AND pFromExchangeRate > 0
 AND g.processed_flag = 'Y'
 group by g.pFromCurrency,g.pToCurrency, g.vType, g.pDate, g.updflag, g.processed_flag, t2.TCURF_FFACT, t2.TCURF_TFACT, g.vtocurr
 )t2
 on (
 g.pfromcurrency = t2.pfromcurrency and 
 g.vtocurr = t2.vtocurr
 )
 when matched then update set g.vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
 g.vffact = t2.TCURF_FFACT,
 g.vtfact = t2.TCURF_TFACT;



UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_tcurf_denorm t2, tmp_getExchangeRate1 g 
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND CURRENT_DATE >= t2.cast_tcurf_gdatu AND  CURRENT_DATE < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
FROM tmp_tcurf_denorm t2, tmp_getExchangeRate1 g 
WHERE g.vFromCurr = t2.TCURF_FCURR AND g.vToCurr = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.updflag = 'N' 
	AND g.pFromExchangeRate = 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
from tmp_getExchangeRate1 g,tmp_tcurf_denorm t2
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.pDate >= t2.cast_tcurf_gdatu AND  g.pDate < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
from tmp_getExchangeRate1 g,tmp_tcurf_denorm t2
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND CURRENT_DATE >= t2.cast_tcurf_gdatu AND  CURRENT_DATE < t2.cast_tcurf_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURF_FFACT * t2.TCURF_TFACT,
	vffact = t2.TCURF_FFACT,
	vtfact = t2.TCURF_TFACT
from tmp_getExchangeRate1 g,tmp_tcurf_denorm t2
WHERE g.pFromCurrency = t2.TCURF_FCURR AND g.pToCurrency = t2.TCURF_TCURR AND g.vType = t2.TCURF_KURST
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

	drop table if exists tmp_tcurf_denorm_tmp;
	
	
UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURR_FFACT * t2.TCURR_TFACT,
	vffact = t2.TCURR_FFACT,
	vtfact = t2.TCURR_TFACT
from tmp_getExchangeRate1 g,tmp_tcurr_denorm t2
WHERE g.pFromCurrency = t2.TCURR_FCURR AND g.pToCurrency = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET vFTfact = t2.TCURR_FFACT * t2.TCURR_TFACT,
	vffact = t2.TCURR_FFACT,
	vtfact = t2.TCURR_TFACT
from tmp_getExchangeRate1 g,tmp_tcurr_denorm t2
WHERE g.pFromCurrency = t2.TCURR_FCURR AND g.pToCurrency = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2.cast_tcurr_gdatu AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND pFromExchangeRate > 0
	AND g.vFTfact = 0
	AND g.processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g
SET exchangeRate = case vReverse 
			when 'Y' then round(pFromExchangeRate * case vFTfact when 0 then 1.00000000 else vFTfact end,6)
			else round(pFromExchangeRate / case vFTfact when 0 then 1.00000000 else vFTfact end,6)
		   end,
	updflag = 'A'
WHERE pFromExchangeRate > 0
	AND updflag = 'N' 
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = pFromExchangeRate,
	updflag = 'A'
WHERE pFromExchangeRate > 0
	AND updflag = 'A'
	AND exchangeRate = 0
	AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = round(tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6),
	updflag = 'B'
FROM tmp_tcurr_denorm t2, tmp_getExchangeRate1 g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND vReverse = 'Y'
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = round(tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6),
	updflag = 'B'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g 
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2.cast_tcurr_gdatu AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND vReverse = 'Y'
	AND g.processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = CASE WHEN tcurr_ukurs = 0 THEN 1
			WHEN tcurr_ukurs > 0
                        THEN round(tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6)
			ELSE round(-1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end),6)
		   END,
	updflag = 'B'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = CASE WHEN tcurr_ukurs = 0 THEN 1
			WHEN tcurr_ukurs > 0
                        THEN round(tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6)
			ELSE round(-1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end),6)
		   END,
	updflag = 'B'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2.cast_tcurr_gdatu AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'N' 
	AND g.processed_flag = 'Y';



UPDATE tmp_getExchangeRate1
SET vFromCurr = pToCurrency, updflag = 'C'
WHERE updflag = 'B' 
	AND vRefCur is not null
	AND vRefCur = vToCurr
	AND vRefCur <> pToCurrency
	AND processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = exchangeRate / CASE WHEN tcurr_ukurs = 0 THEN 1
					WHEN tcurr_ukurs > 0
					THEN round(tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6)
					ELSE round(-1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end),6)
				   END,
	updflag = 'D'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND g.pDate >= t2.cast_tcurr_gdatu AND  g.pDate < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'C' 
	AND g.processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g 
SET exchangeRate = exchangeRate / CASE WHEN tcurr_ukurs = 0 THEN 1
					WHEN tcurr_ukurs > 0
					THEN round(tcurr_ukurs / case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end,6)
					ELSE round(-1.00000000 / (tcurr_ukurs * case vFTfact when 0 then case (tcurr_ffact * tcurr_tfact) when 0 then 1.00000000 else (tcurr_ffact * tcurr_tfact) end else vFTfact end),6)
				   END,
	updflag = 'D'
FROM tmp_tcurr_denorm t2,  tmp_getExchangeRate1 g
WHERE g.vFromCurr = t2.TCURR_FCURR AND g.vToCurr = t2.TCURR_TCURR AND g.vType = t2.TCURR_KURST
	AND CURRENT_DATE >= t2.cast_tcurr_gdatu AND  CURRENT_DATE < t2.cast_tcurr_gdatu_to
	AND g.updflag = 'C' 
	AND g.processed_flag = 'Y';


UPDATE tmp_getExchangeRate1 g
SET exchangeRate = -1 * exchangeRate
WHERE updflag <> 'N'
	AND vReverse = 'Y'
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = 1, updflag = 'E'
WHERE exchangeRate = 0
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET exchangeRate = round(-1/exchangeRate,6), updflag = 'E'
WHERE exchangeRate < 0
	AND processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET vFromCurr = pFromCurrency,
	vToCurr = pToCurrency
WHERE processed_flag = 'Y';

UPDATE tmp_getExchangeRate1 g
SET pFromCurrency = vToCurr,
	pToCurrency = vFromCurr,
	pfromexchangerate = -1 * pfromexchangerate
WHERE processed_flag = 'Y'
	AND vReverse = 'Y';

UPDATE tmp_getExchangeRate1
SET processed_flag = 'D'
WHERE processed_flag = 'Y';

/* BI-5524 - Octavian - Change exchangerate link from <<currency code>> - USD to be <currency code>> - EUR - USD */
create table tmp_getexchangerate1_unique
as select distinct * from tmp_getexchangerate1;

drop table if exists tmp_getexchangerate1;
rename tmp_getexchangerate1_unique to tmp_getexchangerate1;

drop table if exists tmp_getexchangerate1_update_currency_eur_usd;
create table tmp_getexchangerate1_update_currency_eur_usd as
select distinct curr_eur.pfromcurrency,eur_usd.ptocurrency,curr_eur.exchangerate * eur_usd.exchangerate as exchangerate_through_eur,curr_usd.pdate as curr_usd_pdate ,curr_eur.pdate as curr_eur_pdate,
curr_usd.vtype,curr_usd.fact_script_name,curr_usd.pfromexchangerate
from

(select * from tmp_getexchangerate1
where ptocurrency = 'USD' and exchangerate <> 1
and pfromexchangerate = 0
) curr_usd
INNER JOIN
(select * from tmp_getexchangerate1
where ptocurrency = 'EUR'  and exchangerate <> 1
and pfromexchangerate = 0
) curr_eur
ON curr_usd.vtype = curr_eur.vtype
AND curr_usd.fact_script_name = curr_eur.fact_script_name
AND curr_usd.pfromcurrency = curr_eur.pfromcurrency
AND curr_usd.pdate = curr_eur.pdate
INNER JOIN
(select * from tmp_getexchangerate1
where pfromcurrency = 'EUR'
and ptocurrency = 'USD') eur_usd
ON curr_eur.fact_script_name = eur_usd.fact_script_name
AND curr_eur.vtype = eur_usd.vtype
AND curr_eur.ptocurrency = eur_usd.pfromcurrency
AND curr_usd.ptocurrency = eur_usd.ptocurrency
AND curr_usd.pdate = eur_usd.pdate;

/*
update tmp_getexchangerate1 t1
set t1.exchangerate = t2.exchangerate_through_eur
from tmp_getexchangerate1 t1,tmp_getexchangerate1_update_currency_eur_usd t2
where t1.pfromcurrency = t2.pfromcurrency
and t1.ptocurrency = t2.ptocurrency
and t1.vtype = t2.vtype
and t1.fact_script_name = t2.fact_script_name
and t2.curr_usd_pdate = t1.pdate
and t1.exchangerate <> 1
and t1.pfromexchangerate = 0
and t1.exchangerate <> t2.exchangerate_through_eur
*/


merge into tmp_getexchangerate1 t1
using 
(
select t2.pfromcurrency,t2.ptocurrency,t2.vtype,t2.fact_script_name,max(t2.exchangerate_through_eur) as exchangerate_through_eur
from tmp_getexchangerate1 t1,tmp_getexchangerate1_update_currency_eur_usd t2
 where t1.pfromcurrency = t2.pfromcurrency
 and t1.ptocurrency = t2.ptocurrency
 and t1.vtype = t2.vtype
 and t1.fact_script_name = t2.fact_script_name
 and t2.curr_usd_pdate = t1.pdate
 and t1.exchangerate <> 1
 and t1.pfromexchangerate = 0
 and t1.exchangerate <> t2.exchangerate_through_eur
group by t2.pfromcurrency,t2.ptocurrency,t2.vtype,t2.fact_script_name
)x
on (
t1.pfromcurrency = x.pfromcurrency and 
t1.ptocurrency = x.ptocurrency and 
t1.vtype = x.vtype and 
t1.fact_script_name = x.fact_script_name
)
when matched then update set 
t1.exchangerate = x.exchangerate_through_eur
where t1.exchangerate <> x.exchangerate_through_eur; 

/*20 Nov 2017 Georgiana Changes according to APP-8202 Added this additional merge for the cases where we have equal dates between the two tables*/
merge into tmp_getexchangerate1 t1
using 
(
select t2.pfromcurrency,t2.ptocurrency,t2.vtype,t2.fact_script_name,t2.curr_usd_pdate,max(t2.exchangerate_through_eur) as exchangerate_through_eur
from tmp_getexchangerate1 t1,tmp_getexchangerate1_update_currency_eur_usd t2
 where t1.pfromcurrency = t2.pfromcurrency
 and t1.ptocurrency = t2.ptocurrency
 and t1.vtype = t2.vtype
 and t1.fact_script_name = t2.fact_script_name
 and t2.curr_usd_pdate = t1.pdate
 and t1.exchangerate <> 1
 and t1.pfromexchangerate = 0
group by t2.pfromcurrency,t2.ptocurrency,t2.vtype,t2.fact_script_name,t2.curr_usd_pdate
)x
on (
t1.pfromcurrency = x.pfromcurrency and 
t1.ptocurrency = x.ptocurrency and 
t1.vtype = x.vtype and 
t1.fact_script_name = x.fact_script_name and
t1.pdate=x.curr_usd_pdate
)
when matched then update set 
t1.exchangerate = x.exchangerate_through_eur
where t1.exchangerate <> x.exchangerate_through_eur; 

delete from tmp_getexchangerate1
where curr_eur_miss = 'inserted';

drop table if exists tmp_getexchangerate1_update_currency_eur_usd;
drop table if exists tmp_getexchangerate1_update_currency_eur_usd_datesaprox;