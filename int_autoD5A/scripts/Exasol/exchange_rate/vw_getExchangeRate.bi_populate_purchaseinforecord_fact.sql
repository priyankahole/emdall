


/**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Georgiana */
/*   Created On     : 12 May 2017 */
/*   Description    : Current version  Exasol syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                               */

/******************************************************************************************************************/


/* START OF CODE BLOCKS - tmp_getExchangeRate1 would have the final the data    */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_purchaseinforecord';


/* pFromExchangeRate to be used only for local curr */
INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT EINE_WAERS,'USD',NULL,EINE_ERDAT,'vw_bi_populate_purchaseinforecord'
FROM EINE;

UPDATE tmp_getExchangeRate1 t
   SET t.pToCurrency = ifnull(sys.property_value, 'USD')
  FROM systemproperty sys, tmp_getExchangeRate1 t
 WHERE t.fact_script_name = 'vw_bi_populate_purchaseinforecord'
   AND sys.property = 'customer.global.currency';

/* Where explicit currency other than global currency is populated */

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,fact_script_name)
SELECT DISTINCT EINE_WAERS,dc.Currency,null pFromExchangeRate,EINE_ERDAT,'vw_bi_populate_purchaseinforecord'
FROM EINE INNER JOIN dim_plant pl on pl.plantcode=ifnull(WERKS,'Not Set')
 INNER JOIN dim_company dc ON dc.CompanyCode =pl.companycode AND dc.RowIsCurrent = 1 ;


/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_nodups_purchaseinforecord;
create table tmp_getExchangeRate1_nodups_purchaseinforecord
as
select distinct * from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_purchaseinforecord';

delete from tmp_getExchangeRate1
WHERE fact_script_name = 'vw_bi_populate_purchaseinforecord';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_nodups_purchaseinforecord;

drop table tmp_getExchangeRate1_nodups_purchaseinforecord;

/* BI-5524 - Octavian - Change exchangerate link from <<currency code>> - USD to be <currency code>> - EUR - USD */

drop table if exists tmp_getexchangerate1_purchaseinforecord;
create table tmp_getexchangerate1_purchaseinforecord
as select * from tmp_getexchangerate1
where fact_script_name = 'vw_bi_populate_purchaseinforecord';

drop table if exists tmp_getexchangerate1_update_currency_eur_usd;
create table tmp_getexchangerate1_update_currency_eur_usd as
select distinct curr_eur.pfromcurrency,eur_usd.ptocurrency,curr_eur.exchangerate * eur_usd.exchangerate as exchangerate_through_eur,curr_usd.pdate as curr_usd_pdate ,curr_eur.pdate as curr_eur_pdate,
curr_usd.vtype,curr_usd.fact_script_name
, first_value(curr_eur.pdate) over (partition by curr_eur.pfromcurrency,eur_usd.ptocurrency,curr_usd.pdate,curr_usd.vtype,curr_usd.fact_script_name order by abs(curr_eur.pdate - curr_usd.pdate) asc) as curr_eur_aprox_pdate
from

(select * from tmp_getexchangerate1_purchaseinforecord
where ptocurrency = 'USD') curr_usd
INNER JOIN
(select * from tmp_getexchangerate1_purchaseinforecord
where ptocurrency = 'EUR') curr_eur
ON curr_usd.vtype = curr_eur.vtype
AND curr_usd.fact_script_name = curr_eur.fact_script_name
AND curr_usd.pfromcurrency = curr_eur.pfromcurrency
INNER JOIN
(select * from tmp_getexchangerate1_purchaseinforecord
where pfromcurrency = 'EUR'
and ptocurrency = 'USD') eur_usd
ON curr_eur.fact_script_name = eur_usd.fact_script_name
AND curr_eur.vtype = eur_usd.vtype
AND curr_eur.ptocurrency = eur_usd.pfromcurrency
AND curr_usd.ptocurrency = eur_usd.ptocurrency
AND curr_usd.pdate = eur_usd.pdate;

drop table if exists tmp_getexchangerate1_update_currency_eur_usd_datesaprox_fact_purchaseinforecord;
create table tmp_getexchangerate1_update_currency_eur_usd_datesaprox_fact_purchaseinforecord
as select * from tmp_getexchangerate1_update_currency_eur_usd
where curr_eur_aprox_pdate = curr_eur_pdate;

update tmp_getexchangerate1 t1
set t1.exchangerate = t2.exchangerate_through_eur
from tmp_getexchangerate1 t1,tmp_getexchangerate1_update_currency_eur_usd_datesaprox t2
where t1.pfromcurrency = t2.pfromcurrency
and t1.ptocurrency = t2.ptocurrency
and t1.vtype = t2.vtype
and t1.fact_script_name = t2.fact_script_name
and t2.curr_usd_pdate = t1.pdate
and t1.exchangerate <> t2.exchangerate_through_eur;

drop table if exists tmp_getexchangerate1_update_currency_eur_usd;
drop table if exists tmp_getexchangerate1_update_currency_eur_usd_datesaprox_fact_purchaseinforecord;
