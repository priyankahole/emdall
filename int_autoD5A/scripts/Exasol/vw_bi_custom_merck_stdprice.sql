/***************************************************************************************************** 
Creation Date : May 08, 2014
Created By : Nicoleta C
*****************************************************************************************************/
DROP TABLE IF EXISTS tmp_ccpdatags_upd_mbew;
create table tmp_ccpdatags_upd_mbew as
 select distinct plantcode, convert(varchar(20),uin) as uin, cplocalstdcostprice
from ccpdatagscost
where uin not like 'INV%' and cplocalstdcostprice <> 0;


UPDATE MBEW m 
   SET STPRS = cplocalstdcostprice
FROM tmp_ccpdatags_upd_mbew u, dim_part dp, dim_plant p,  MBEW m 
WHERE   m.MATNR = u.uin
        and m.MATNR = dp.PartNumber
		AND m.BWKEY = p.ValuationArea
		AND dp.Plant = p.PlantCode
		AND u.plantcode = p.plantcode
       /*AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON */
       AND STPRS <> cplocalstdcostprice;
	   
UPDATE MBEW m  
   SET VERPR = cplocalstdcostprice 
FROM tmp_ccpdatags_upd_mbew u, dim_part dp, dim_plant p,  MBEW m  
WHERE   m.MATNR = u.uin
        and m.MATNR = dp.PartNumber
		AND m.BWKEY = p.ValuationArea
		AND dp.Plant = p.PlantCode
		AND u.plantcode = p.plantcode
       /*AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON */
       AND VERPR <> cplocalstdcostprice;
 
UPDATE MBEW m 
   SET SALK3 = cplocalstdcostprice * m.MBEW_LBKUM
FROM tmp_ccpdatags_upd_mbew u, dim_part dp, dim_plant p,  MBEW m  
 WHERE    m.MATNR = u.uin
        and m.MATNR = dp.PartNumber
		AND m.BWKEY = p.ValuationArea
		AND dp.Plant = p.PlantCode
		AND u.plantcode = p.plantcode
       /*AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON */
       AND SALK3 <> cplocalstdcostprice * m.MBEW_LBKUM;
	   
/* not in mbew */	 
insert into mbew(bwkey, matnr, vprsv, verpr, stprs, lfgja, lfmon, peinh)
select distinct plantcode as bwkey, 
				lpad(uin,6,'0') as matnr, 
				'S' as vprsv,
				cplocalstdcostprice as verpr, 
				cplocalstdcostprice as strps,
				(SELECT EXTRACT (YEAR FROM CURRENT_DATE)) as lfgja, 
				1 as lfmon, 
				1.000 peinh
from ccpdatagscost c
where not exists
	(select 1 from mbew m where m.matnr = c.uin and m.bwkey = c.plantcode);


UPDATE MBEWH m 
   SET STPRS = cplocalstdcostprice
FROM tmp_ccpdatags_upd_mbew u, dim_part dp, dim_plant p, MBEWH m 
WHERE   m.MATNR = u.uin
        and m.MATNR = dp.PartNumber
		AND m.BWKEY = p.ValuationArea
		AND dp.Plant = p.PlantCode
		AND u.plantcode = p.plantcode
       /*AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON */
       AND STPRS <> cplocalstdcostprice;
	   
UPDATE MBEWH m  
   SET VERPR = cplocalstdcostprice 
FROM tmp_ccpdatags_upd_mbew u, dim_part dp, dim_plant p, MBEWH m 
WHERE   m.MATNR = u.uin
        and m.MATNR = dp.PartNumber
		AND m.BWKEY = p.ValuationArea
		AND dp.Plant = p.PlantCode
		AND u.plantcode = p.plantcode
       /*AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON */
       AND VERPR <> cplocalstdcostprice;
 
UPDATE MBEWH m 
   SET SALK3 = cplocalstdcostprice * m.MBEWH_LBKUM
 FROM tmp_ccpdatags_upd_mbew u, dim_part dp, dim_plant p, MBEWH m 
 WHERE    m.MATNR = u.uin
        and m.MATNR = dp.PartNumber
		AND m.BWKEY = p.ValuationArea
		AND dp.Plant = p.PlantCode
		AND u.plantcode = p.plantcode
       /*AND m.LFGJA = mmm.MBEW_LFGJA
       AND ifnull(m.BWTAR,'Not Set') = ifnull(mmm.MBEW_BWTAR,'Not Set')
       AND m.LFMON = mmm.MBEW_LFMON */
       AND SALK3 <> cplocalstdcostprice * m.MBEWH_LBKUM;
	   
/* not in mbewh */	 
insert into mbewh(bwkey, matnr, vprsv, verpr, stprs, lfgja, lfmon, peinh)
select distinct plantcode as bwkey, 
				lpad(uin,6,'0') as matnr, 
				'S' as vprsv,
				cplocalstdcostprice as verpr, 
				cplocalstdcostprice as strps,
				(SELECT EXTRACT (YEAR FROM CURRENT_DATE)) as lfgja, 
				1 as lfmon, 
				1.000 peinh
from ccpdatagscost c
where not exists
	(select 1 from mbewh m where m.matnr = c.uin and m.bwkey = c.plantcode);


Delete from mbew_no_bwtar;
/*CALL vectorwise(combine 'mbew_no_bwtar-mbew_no_bwtar')*/

DROP TABLE IF EXISTS SALK3_000;

CREATE TABLE SALK3_000
AS
   SELECT a.MATNR,
          a.BWKEY,
          ((a.LFGJA * 100) + a.LFMON) LFGJA_LFMON,
          max(a.SALK3) MAXSALKS,
          max(a.MBEW_KALNR) MAXMBEW_KALNR
     FROM mbew a
    WHERE a.BWTAR IS NULL
   GROUP BY a.MATNR, a.BWKEY, ((a.LFGJA * 100) + a.LFMON);

INSERT INTO mbew_no_bwtar(BWKEY,
                          MATNR,
                          LVORM,
                          VPRSV,
                          VERPR,
                          STPRS,
                          LFGJA,
                          LFMON,
                          PEINH,
                          SALK3,
                          BWTAR,
                          MBEW_LBKUM,
                          MBEW_VMPEI,
                          MBEW_VMSTP,
                          MBEW_VJSTP,
                          MBEW_VJPEI,
                          MBEW_ZPLD1,
                          MBEW_KALN1,
                          MBEW_VMVER,
                          MBEW_STPRV,
                          MBEW_LAEPR,
                          MBEW_BWPRH,
                          MBEW_ZPLP1,
                          MBEW_ZPLD2,
                          MBEW_VPLPR,
                          MBEW_LPLPR,
                          MBEW_BWVA2,
                          MBEW_KALNR,
                          MARA_LAEDA,
                          MBEW_ZPLPR,
                          MBEW_PDATL,
                          MBEW_PPRDL)
   SELECT DISTINCT BWKEY,
                   MATNR,
                   LVORM,
                   VPRSV,
                   VERPR,
                   STPRS,
                   LFGJA,
                   LFMON,
                   PEINH,
                   SALK3,
                   BWTAR,
                   MBEW_LBKUM,
                   MBEW_VMPEI,
                   MBEW_VMSTP,
                   MBEW_VJSTP,
                   MBEW_VJPEI,
                   NULL MBEW_ZPLD1,
                   MBEW_KALN1,
                   MBEW_VMVER,
                   MBEW_STPRV,
                   NULL MBEW_LAEPR,
                   MBEW_BWPRH,
                   MBEW_ZPLP1,
                   NULL MBEW_ZPLD2,
                   MBEW_VPLPR,
                   MBEW_LPLPR,
                   MBEW_BWVA2,
                   MBEW_KALNR,
                   NULL MARA_LAEDA,
                   MBEW_ZPLPR,
                   MBEW_PDATL,
                   MBEW_PPRDL
     FROM mbew m
    WHERE BWTAR IS NULL
          AND SALK3 IN
                 (SELECT MAXSALKS
                    FROM SALK3_000 a
                   WHERE     a.MATNR = m.MATNR
                         AND a.BWKEY = m.BWKEY
                         AND LFGJA_LFMON = ((m.LFGJA * 100) + m.LFMON))
          AND MBEW_KALNR IN
                 (SELECT MAXMBEW_KALNR
                    FROM SALK3_000 a
                   WHERE     a.MATNR = m.MATNR
                         AND a.BWKEY = m.BWKEY
                         AND LFGJA_LFMON = ((m.LFGJA * 100) + m.LFMON));

DROP TABLE IF EXISTS SALK3_000;


delete from mbewh_no_bwtar;

INSERT INTO mbewh_no_bwtar(MATNR,
                           BWKEY,
                           LFGJA,
                           LFMON,
                           VPRSV,
                           VERPR,
                           STPRS,
                           PEINH,
                           SALK3,
                           BWTAR,
                           MBEWH_LBKUM)
   SELECT distinct MATNR,
          BWKEY,
          LFGJA,
          LFMON,
          VPRSV,
          VERPR,
          STPRS,
          PEINH,
          SALK3,
          BWTAR,
          MBEWH_LBKUM
     FROM mbewh
     WHERE BWTAR is null;

DROP TABLE IF EXISTS tmp_ccpdatags_upd_mbew;
