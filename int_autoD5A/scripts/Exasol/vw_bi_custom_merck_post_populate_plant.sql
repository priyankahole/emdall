/************************************************************************************************************************/

/*   Script         : vw_bi_custom_merck_post_populate_plant.sql                                                  	    */

/*   Author         : Octavian Z                                                                    			 	    */

/*   Created On     : January 2015	                                                                       		        */

/*   Description    : vw_bi_custom_merck_post_populate_plant - Script for post populating dim_plant	with custom values  */



/*********************************************Change History*************************************************************/

/*  Date             By         Version	    Desc                                                  			            */
/*  05 Mar 2015	     Octavian Z	1.2	    	Requested updated values for BR40,PH30 and USA0							*/
/*  03 Feb 2015	     Octavian Z	1.1	    	Requested updated values for AT30 and DE50							*/
/*  05 Jan 2015	     Octavian Z	1.0	    	Script for post populating plant - initial draft							*/

/************************************************************************************************************************/

/*
update dim_plant set planttitle_merck='Arab Emirates DC' where plantcode='AE20'
update dim_plant set planttitle_merck='West Balkan',region_merck='Europe 2' where plantcode='AT30'
update dim_plant set name='Intervet GesmbH',tacticalring_merck='ComOps',planttitle_merck='Austria',region_merck='Europe 1',region='Austria ComOps' where plantcode='AT40'
update dim_plant set tacticalring_merck='ComOps',region_merck='Latin America',planttitle_merck='Brasil Coopers' where plantcode='BR40'
update dim_plant set tacticalring_merck='Not Used' where plantcode='CH10'
update dim_plant set tacticalring_merck='ComOps',planttitle_merck='France',region_merck='Europe 2',region='France ComOps' where plantcode='FR30'
update dim_plant set region_merck='Europe 1' where plantcode='SE20'
update dim_plant set planttitle_merck='Austria' where plantcode='DE50'
update dim_plant set planttitle_merck='Philippines Intervet' where plantcode='PH30'
update dim_plant set planttitle_merck='US/Omaha' where plantcode='USA0'
update dim_plant set planttitle_merck='France' where plantcode='FR60'*/

/*Begin Andrian based on excel file*/
    update dim_plant a
  set a.tacticalring_merck = ifnull(b.tacticalring,'Not Set')
  from tmp_plantattributes b,  dim_plant a
  where a.plantcode = b.plantcode
  and ifnull(a.tacticalring_merck,'Not Set') <> ifnull(b.tacticalring,'Not Set');

   update dim_plant a
  set a.region = ifnull(b.primarysettitle,'Not Set')
  from tmp_plantattributes b, dim_plant a
  where a.plantcode = b.plantcode
  and ifnull(a.region,'Not Set') <> ifnull(b.primarysettitle,'Not Set');

   update dim_plant a
  set a.planttitle_merck = ifnull(b.planttitle,'Not Set')
  from tmp_plantattributes b, dim_plant a
  where a.plantcode = b.plantcode
  and ifnull(a.planttitle_merck,'Not Set') <> ifnull(b.planttitle,'Not Set');

     update dim_plant a
  set a.region_merck = ifnull(b.plngregion,'Not Set')
  from tmp_plantattributes b, dim_plant a
  where a.plantcode = b.plantcode
  and ifnull(a.region_merck,'Not Set') <> ifnull(b.plngregion,'Not Set');
/*End Andrian based on excel file*/

update dim_plant
set regiongbl_merck = 'Global'
where regiongbl_merck <> 'Global';

/* BI-309 */
UPDATE dim_plant pl
SET pl.flag_atlaslite_plant = 'ATLAS LITE'
FROM Z1SD_ISD_INSCOPE z, dim_plant pl
WHERE pl.plantcode = z.Z1SD_ISD_INSCOPE_WERKS
AND z.Z1SD_ISD_INSCOPE_INS_SALES = 'L'
AND pl.flag_atlaslite_plant <> 'ATLAS LITE';
