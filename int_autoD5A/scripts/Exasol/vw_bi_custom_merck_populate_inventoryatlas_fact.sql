/*   Script         :    */
/*   Author         : Radu */
/*   Created On     : 21 Feb 2014 */
/*   Description    : Populating script for Merck table fact_inventoryatlas   */

/*** MBEW NO BWTAR values ***/

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x 
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY)
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR AS
SELECT DISTINCT m.MATNR,
       m.BWKEY,
       m.LFGJA,
       m.LFMON,
       m.VPRSV,
       m.VERPR,
       m.STPRS,
       m.PEINH,
       m.SALK3,
       m.BWTAR,
       m.MBEW_LBKUM,
       ((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, 
       m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;	

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  cast((b.STPRS / b.PEINH) as decimal(18,5))
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  cast((b.VERPR / b.PEINH) as decimal(18,5))
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

/******/

/*  Liviu Ionescu App-6059 */
drop table if exists Z1PP_ENH_MRP_TMP;
create table Z1PP_ENH_MRP_TMP as
select Z1PP_ENH_MRP_PLWRK,
	Z1PP_ENH_MRP_MATNR,
	sum(Z1PP_ENH_MRP_AQST1M) Z1PP_ENH_MRP_AQST1M,
	sum(Z1PP_ENH_MRP_AQST2M) Z1PP_ENH_MRP_AQST2M,
	sum(Z1PP_ENH_MRP_AQST3M) Z1PP_ENH_MRP_AQST3M,
	sum(Z1PP_ENH_MRP_AQST6M) Z1PP_ENH_MRP_AQST6M,
	sum(Z1PP_ENH_MRP_AQST9M) Z1PP_ENH_MRP_AQST9M,
	sum(Z1PP_ENH_MRP_AQST12M) Z1PP_ENH_MRP_AQST12M,
	sum(Z1PP_ENH_MRP_AQST15M) Z1PP_ENH_MRP_AQST15M,
	sum(Z1PP_ENH_MRP_AQST18M) Z1PP_ENH_MRP_AQST18M
from Z1PP_ENH_MRP group by Z1PP_ENH_MRP_PLWRK,Z1PP_ENH_MRP_MATNR;

drop table if exists tmp_fact_inventoryatlas_ins_upd;

create table tmp_fact_inventoryatlas_ins_upd as
(
select 
dp.dim_partid,
dpl.dim_plantid,
ifnull(dpld.dim_plantid,1) as dim_plantdeliveryid,
ifnull(dd.dim_dateid,1) as dim_mprdateid,
ifnull(du.dim_unitofmeasureid,1) as dim_unitofmeasureid,
ifnull(dd2.dim_dateid,1) as dim_lastexecutiondateid,
ifnull(src.Z1PP_INVBW_EXTIM,'000000') as dd_execusion_time,
concat(
cast(ifnull(src.Z1PP_INVBW_EXDAT,'0001-01-01') as varchar(20)),' ',
substr(ifnull(src.Z1PP_INVBW_EXTIM,'000000'),1,2),':',
substr(ifnull(src.Z1PP_INVBW_EXTIM,'000000'),3,2),':',
substr(ifnull(src.Z1PP_INVBW_EXTIM,'000000'),5,2)) as dd_lastexecution_timestamp,
ifnull(src.Z1PP_INVBW_AQSTC,0) as ct_available_qi_stock_coverage,
ifnull(src.Z1PP_INVBW_AQSTQ,0) as ct_available_qi_stockqty,
ifnull(src.Z1PP_INVBW_ASTC,0) as ct_available_stock_coverage,
ifnull(src.Z1PP_INVBW_ASTQ,0) as ct_available_stockqty,
ifnull(src.Z1PP_INVBW_SAST,0) as ct_safety_workdays,
ifnull(src.Z1PP_INVBW_SASTQT,0) as ct_safety_workdays_GRprocessing_time,
ifnull(src.Z1PP_INVBW_SEGWD,0) as ct_safety_EoqEotFx_Workdays_GR_time,
ifnull(src.Z1PP_INVBW_YDEMAND,0) as ct_future_demandqty,
ifnull(src.Z1PP_INVBW_INTRANSITSTOCK,0) as ct_intransitstockqty,
ifnull(b.multiplier,0) StdPrice,
/*  Liviu Ionescu App-6059 */
ifnull(zsrc.Z1PP_ENH_MRP_AQST1M,0) AvailableQIStockRSL1Month,
ifnull(zsrc.Z1PP_ENH_MRP_AQST2M,0) AvailableQIStockRSL2Month,
ifnull(zsrc.Z1PP_ENH_MRP_AQST3M,0) AvailableQIStockRSL3Month,
ifnull(zsrc.Z1PP_ENH_MRP_AQST6M,0) AvailableQIStockRSL6Month,
ifnull(zsrc.Z1PP_ENH_MRP_AQST9M,0) AvailableQIStockRSL9Month,
ifnull(zsrc.Z1PP_ENH_MRP_AQST12M,0) AvailableQIStockRSL12Month,
ifnull(zsrc.Z1PP_ENH_MRP_AQST15M,0) AvailableQIStockRSL15Month,
ifnull(zsrc.Z1PP_ENH_MRP_AQST18M,0) AvailableQIStockRSL18Month,
ifnull(src.Z1PP_INVBW_CUSTST,0) as ct_custorder_segm_stock--APP-8409
from Z1PP_INVBW src
inner join dim_part dp
	on src.Z1PP_INVBW_MATNR=dp.partnumber
	and src.Z1PP_INVBW_PLWRK=dp.plant
inner join dim_plant dpl
	on src.Z1PP_INVBW_PLWRK=dpl.plantcode
LEFT JOIN tmp2_MBEW_NO_BWTAR b 
	ON b.MATNR = src.Z1PP_INVBW_MATNR AND b.BWKEY = dpl.ValuationArea
left outer join dim_plant dpld
	on src.Z1PP_INVBW_DELPS=dpld.plantcode
left outer join dim_date dd
	on dpl.companycode=dd.companycode
	and src.Z1PP_INVBW_DSDAT=dd.datevalue and dpl.plantcode=dd.plantcode_factory
left outer join dim_unitofmeasure du
	on 	du.uom=src.Z1PP_INVBW_DUOM
left outer join dim_date dd2
	on dd2.companycode=dpl.companycode
	and src.Z1PP_INVBW_EXDAT=dd2.datevalue and dpl.plantcode=dd2.plantcode_factory
/*  Liviu Ionescu App-6059 */
left join Z1PP_ENH_MRP_TMP zsrc on zsrc.Z1PP_ENH_MRP_PLWRK = src.Z1PP_INVBW_PLWRK
						and zsrc.Z1PP_ENH_MRP_MATNR = src.Z1PP_INVBW_MATNR
);


drop table if exists tmp_fact_inventoryatlas_001;
create table tmp_fact_inventoryatlas_001 like fact_inventoryatlas including defaults including identity;

delete from number_fountain m where m.table_name = 'tmp_fact_inventoryatlas_001';
INSERT INTO NUMBER_FOUNTAIN
select 	'tmp_fact_inventoryatlas_001',
		ifnull(max(f.fact_inventoryatlasid), 
		       ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_inventoryatlas_001 f;


insert into tmp_fact_inventoryatlas_001 (
							fact_inventoryatlasid,                                                                                                                                                                                                                                 
							dim_partid,                                                                                                                                                                                                                                                     
							dim_plantid,                                                                                                                                                                                                                                                     
							dim_plantdeliveryid,                                                                                                                                                                                                                                             
							dim_mprdateid,                                                                                                                                                                                                                                                   
							dim_unitofmeasureid,                                                                                                                                                                                                                                             
							dim_lastexecutiondateid,                                                                                                                                                                                                                                         
							dd_execusion_time,                                                                                                                                                                                                                                               
							dd_lastexecution_timestamp,                                                                                                                                                                                                                                      
							ct_available_qi_stock_coverage,                                                                                                                                                                                                                                  
							ct_available_qi_stockqty,                                                                                                                                                                                                                                        
							ct_available_stock_coverage,                                                                                                                                                                                                                                     
							ct_available_stockqty,                                                                                                                                                                                                                                           
							ct_safety_workdays,                                                                                                                                                                                                                                              
							ct_safety_workdays_grprocessing_time,                                                                                                                                                                                                                            
							ct_safety_eoqeotfx_workdays_gr_time,                                                                                                                                                                                                                             
							ct_future_demandqty,
							ct_intransitstockqty,
							amt_PartStdPrice,
							AvailableQIStockRSL1Month,
							AvailableQIStockRSL2Month,
							AvailableQIStockRSL3Month,
							AvailableQIStockRSL6Month,
							AvailableQIStockRSL9Month,
							AvailableQIStockRSL12Month,
							AvailableQIStockRSL15Month,
							AvailableQIStockRSL18Month,
							ct_custorder_segm_stock--app-8409
									)
select
		(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_inventoryatlas_001') + row_number() over (order by ''),
		tmp.dim_partid,                                                                                                                                                                                                                                                     
		tmp.dim_plantid,                                                                                                                                                                                                                                                     
		tmp.dim_plantdeliveryid,                                                                                                                                                                                                                                             
		tmp.dim_mprdateid,                                                                                                                                                                                                                                                   
		tmp.dim_unitofmeasureid,                                                                                                                                                                                                                                             
		tmp.dim_lastexecutiondateid,                                                                                                                                                                                                                                         
		tmp.dd_execusion_time,                                                                                                                                                                                                                                               
		tmp.dd_lastexecution_timestamp,                                                                                                                                                                                                                                      
		tmp.ct_available_qi_stock_coverage,                                                                                                                                                                                                                                  
		tmp.ct_available_qi_stockqty,                                                                                                                                                                                                                                        
		tmp.ct_available_stock_coverage,                                                                                                                                                                                                                                     
		tmp.ct_available_stockqty,                                                                                                                                                                                                                                           
		tmp.ct_safety_workdays,                                                                                                                                                                                                                                              
		tmp.ct_safety_workdays_grprocessing_time,                                                                                                                                                                                                                            
		tmp.ct_safety_eoqeotfx_workdays_gr_time,                                                                                                                                                                                                                             
		tmp.ct_future_demandqty,
		tmp.ct_intransitstockqty,
		tmp.StdPrice,
/*  Liviu Ionescu App-6059 */
		tmp.AvailableQIStockRSL1Month,
		tmp.AvailableQIStockRSL2Month,
		tmp.AvailableQIStockRSL3Month,
		tmp.AvailableQIStockRSL6Month,
		tmp.AvailableQIStockRSL9Month,
		tmp.AvailableQIStockRSL12Month,
		tmp.AvailableQIStockRSL15Month,
		tmp.AvailableQIStockRSL18Month,
		tmp.ct_custorder_segm_stock--app-8409
from
	tmp_fact_inventoryatlas_ins_upd tmp;
 			
drop table if exists tmp_fact_inventoryatlas_globalcurrency;

create table tmp_fact_inventoryatlas_globalcurrency as
select ifnull( (SELECT property_value
				FROM systemproperty
				WHERE property = 'customer.global.currency'),'USD') pGlobalCurrency;


drop table if exists tmp_fact_inventoryatlas_ins_upd2;

create table tmp_fact_inventoryatlas_ins_upd2 as
select
f.dim_partid
,f.dim_plantid
,convert(bigint,1) as dim_currencyid
,convert(bigint,1) as dim_currencyid_Tra
,convert(bigint,1) as dim_currencyid_GBL
,convert(decimal (18,6),1) as amt_exchangerate
,convert(decimal (18,6),1) as amt_Exchangerate_gbl
,tmp.pGlobalCurrency
from tmp_fact_inventoryatlas_001 f
left outer join dim_plant dpl
	on f.dim_plantid=dpl.dim_plantid
left outer join dim_company dc
	on dpl.companycode=dc.companycode
,tmp_fact_inventoryatlas_globalcurrency tmp;

update tmp_fact_inventoryatlas_ins_upd2 f
set f.dim_currencyid = dcr.dim_currencyid
from tmp_fact_inventoryatlas_ins_upd2 f, 
                          dim_plant dpl, 
                         dim_company dc, 
                       dim_currency dcr
where f.dim_plantid=dpl.dim_plantid
and dpl.companycode=dc.companycode
and dc.currency=dcr.currencycode 
and f.dim_currencyid <> dcr.dim_currencyid;

update tmp_fact_inventoryatlas_ins_upd2 f
set f.dim_currencyid_tra = dcr.dim_currencyid
from tmp_fact_inventoryatlas_ins_upd2 f, 
                          dim_plant dpl, 
                         dim_company dc, 
                       dim_currency dcr
where f.dim_plantid=dpl.dim_plantid
and dpl.companycode=dc.companycode
and dc.currency=dcr.currencycode 
and f.dim_currencyid_tra <> dcr.dim_currencyid;

update tmp_fact_inventoryatlas_ins_upd2 f
set f.dim_currencyid_GBL = dcr.dim_currencyid
from tmp_fact_inventoryatlas_ins_upd2 f, 
                          dim_plant dpl, 
                         dim_company dc, 
                       dim_currency dcr
where f.dim_plantid=dpl.dim_plantid
and dpl.companycode=dc.companycode
and dc.currency=dcr.currencycode 
and dcr.currencycode = f.pGlobalCurrency 
and f.dim_currencyid_GBL <> dcr.dim_currencyid;

update tmp_fact_inventoryatlas_ins_upd2 f
set f.amt_Exchangerate_gbl = Exch.exchangeRate
from tmp_fact_inventoryatlas_ins_upd2 f, 
                          dim_plant dpl, 
                         dim_company dc, 
                       tmp_getExchangeRate1 Exch
where f.dim_plantid=dpl.dim_plantid
and dpl.companycode=dc.companycode
and Exch.pFromCurrency = dc.currency
		and Exch.fact_script_name = 'vw_bi_custom_merck_populate_inventoryatlas_fact' 
		and Exch.pToCurrency = f.pGlobalCurrency
		and Exch.pDate = current_date
and f.amt_Exchangerate_gbl <> Exch.exchangeRate;

update tmp_fact_inventoryatlas_001 trg
set trg.dim_currencyid=tmp2.dim_currencyid
    ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_fact_inventoryatlas_ins_upd2 tmp2,tmp_fact_inventoryatlas_001 trg
where tmp2.dim_partid=trg.dim_partid
and ifnull(trg.dim_currencyid,1) <> ifnull(tmp2.dim_currencyid,1);

update tmp_fact_inventoryatlas_001 trg set trg.dim_currencyid=1 where trg.dim_currencyid is null;


update tmp_fact_inventoryatlas_001 trg
set trg.dim_currencyid_Tra=tmp2.dim_currencyid_Tra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_fact_inventoryatlas_ins_upd2 tmp2,tmp_fact_inventoryatlas_001 trg
where tmp2.dim_partid=trg.dim_partid
and ifnull(trg.dim_currencyid_Tra,1) <> ifnull(tmp2.dim_currencyid_Tra,1);

update tmp_fact_inventoryatlas_001 trg set trg.dim_currencyid_Tra=1 where trg.dim_currencyid_Tra is null;


update tmp_fact_inventoryatlas_001 trg
set trg.dim_currencyid_GBL=tmp2.dim_currencyid_GBL
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_fact_inventoryatlas_ins_upd2 tmp2,tmp_fact_inventoryatlas_001 trg
where tmp2.dim_partid=trg.dim_partid
and ifnull(trg.dim_currencyid_GBL,1) <> ifnull(tmp2.dim_currencyid_GBL,1);

update tmp_fact_inventoryatlas_001 trg 
set trg.dim_currencyid_GBL=1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where trg.dim_currencyid_GBL is null;

update tmp_fact_inventoryatlas_001 trg
set trg.amt_Exchangerate_gbl=tmp2.amt_Exchangerate_gbl
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_fact_inventoryatlas_ins_upd2 tmp2,tmp_fact_inventoryatlas_001 trg
where tmp2.dim_partid=trg.dim_partid
and ifnull(trg.amt_Exchangerate_gbl,1) <> ifnull(tmp2.amt_Exchangerate_gbl,1);

update tmp_fact_inventoryatlas_001 trg 
set trg.amt_Exchangerate_gbl=1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where trg.amt_Exchangerate_gbl is null;

update tmp_fact_inventoryatlas_001 trg 
set trg.amt_exchangerate=cast(1 as decimal(18,6)) 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where trg.amt_exchangerate is null;

update tmp_fact_inventoryatlas_001 f
set
f.dd_inventory_kpi=ifnull(case
								when f.ct_available_qi_stock_coverage  > (f.ct_safety_eoqeotfx_workdays_gr_time * 1.25) then 'Above Max'
								when f.ct_available_qi_stock_coverage < (f.ct_safety_workdays_grprocessing_time * 0.75) then 'Below Min'
								else 'In Bandwidth'
	   						end,'Not Set'),
f.dd_vmi_indicator=ifnull(case
								when upper(substr(dp.mrpcontroller,1,1)) = 'J' then 'VMI'
								else 'PO Driven' 
	   						end,'Not Set')
	,dw_update_date = current_timestamp
from dim_part dp, tmp_fact_inventoryatlas_001 f
where
f.dim_partid=dp.dim_partid;

TRUNCATE TABLE fact_inventoryatlas;
INSERT INTO fact_inventoryatlas
SELECT * FROM tmp_fact_inventoryatlas_001;

drop table if exists tmp_fact_inventoryatlas_001;

delete from number_fountain m where m.table_name = 'fact_inventoryatlas';
INSERT INTO NUMBER_FOUNTAIN
select 	'fact_inventoryatlas',
		ifnull(max(f.fact_inventoryatlasid), 
		       ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inventoryatlas f;

insert into fact_inventoryatlas (fact_inventoryatlasid,dim_partid,dim_plantid,ct_totalstockiru)
select (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryatlas') + row_number() over (order by ''),
 b.dim_partid,c.dim_plantid,ifnull(a.stockqty,0) from newiteminstockbrinit a, dim_part b, dim_plant c
where a.uin = b.partnumber 
and a.plantcode = b.plant
and a.plantcode = c.plantcode
and not exists (select 1 from fact_inventoryatlas d
where b.dim_partid = d.dim_partid
and c.dim_plantid = d.dim_plantid);

update fact_inventoryatlas f
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from  
(select plantcode,partnumber,max(ccpbudgetunitprice) as ccpbudgetunitprice from ccpstpricefile201602
group by plantcode,partnumber) u, dim_part dp, dim_plant pl, 
fact_inventoryatlas f 
WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcode = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode =pl.plantcode
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice;
		
	
		
		
/*update fact_inventoryatlas f
from  
ccpdatagscost_new_2015 u, dim_part dp, dim_plant pl
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcountry = substr(pl.plantcode,1,2)
		and f.dim_plantid = pl.dim_plantid
		and  dp.plant =pl.plantcode
		and u.plantcode<>pl.plantcode
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
*/

/*
drop table if exists tmp_upd_gblStdPrice_Merck2
create table tmp_upd_gblStdPrice_Merck2 as
select   max(u.ccpbudgetunitprice) as ccpbudgetunitprice,  f.dim_plantid, 
f.dim_partid
from fact_inventoryatlas f,
ccpdatagscost_new_2015 u, dim_part dp, dim_plant pl
WHERE lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
and f.dim_partid = dp.dim_partid
AND u.plantcountry = substr(pl.plantcode,1,2)
and f.dim_plantid = pl.dim_plantid
and u.plantcode<>pl.plantcode
and not exists ( select 1 from ccpdatagscost_new_2015 u2
where u2.plantcode = dp.plant
	and lpad(dp.partnumber,8,'0') = lpad(u2.partnumber,8,'0'))
	group by f.dim_plantid, 
f.dim_partid

update fact_inventoryatlas f
from 
tmp_upd_gblStdPrice_Merck2 u
set f.amt_gblStdPrice_Merck = u.ccpbudgetunitprice
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*WHERE f.dim_partid = u.dim_partid
and f.dim_plantid = u.dim_plantid
and ifnull(f.amt_gblStdPrice_Merck,-99) <> u.ccpbudgetunitprice

/* update only for XX20 */		
/*update fact_inventoryatlas f
from  
ccpdatagscost_new_2015 u, dim_part dp 
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid and  dp.plant = 'XX20'
		/*AND u.plantcode = /*dp.plant*//* dp.primarysite
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		*/
				
/* Nicoleta 14.05.2014 */

/* 28.08.2015 New CCP */


/* Plant Country Code and Part Number */

drop table if exists tmp_ccpdatagscost_upd1;
create table tmp_ccpdatagscost_upd1 as	
select fact_inventoryatlasid, max(ccpbudgetunitprice) as ccpbudgetunitprice
from
fact_inventoryatlas f,ccpstpricefile201602 u, dim_part dp, dim_plant pl 
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid and  dp.plant = pl.plantcode
		AND u.plantcountry = /*dp.plant*/ pl.country
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0
		group by fact_inventoryatlasid;
		
update fact_inventoryatlas f
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_ccpdatagscost_upd1 u,fact_inventoryatlas f
WHERE  f.fact_inventoryatlasid = u.fact_inventoryatlasid
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0;
		
drop table if exists tmp_ccpdatagscost_upd1;

/* Primary Site Code and Part Number	*/	
			
update fact_inventoryatlas f
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from  
(SELECT u.partnumber,u.plantcode,max(ccpbudgetunitprice) AS ccpbudgetunitprice FROM ccpstpricefile201602 u 
GROUP BY u.partnumber,u.plantcode) u, 
dim_part dp , fact_inventoryatlas f
--cpstpricefile201602 u, dim_part dp,fact_inventoryatlas f
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid /*and  dp.plant = 'XX20'*/
		AND u.plantcode = /*dp.plant*/ dp.primarysite
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0;


		
/* Primary Site Country Code and Part Number */

		
drop table if exists tmp_ccpdatagscost_upd2;
create table tmp_ccpdatagscost_upd2 as	
select fact_inventoryatlasid, max(ccpbudgetunitprice) as ccpbudgetunitprice
from
fact_inventoryatlas f, 
ccpstpricefile201602 u, dim_part dp, dim_plant pl 
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid 
		AND u.plantcountry = /*dp.plant*/ pl.country
		and  dp.primarysite = pl.plantcode
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0
group by fact_inventoryatlasid;

 update fact_inventoryatlas f
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from  
tmp_ccpdatagscost_upd2 u ,fact_inventoryatlas f
WHERE  f.fact_inventoryatlasid = u.fact_inventoryatlasid
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0;
		
drop table if exists tmp_ccpdatagscost_upd2;		
		
		
/*	BI-4693 */
/*Part Number*/

drop table if exists tmp_ccpdatagscost_upd3;
create table tmp_ccpdatagscost_upd3 as	
select fact_inventoryatlasid, avg(ccpbudgetunitprice) as ccpbudgetunitprice
from
fact_inventoryatlas f, 
ccpstpricefile201602 u, dim_part dp, dim_plant pl 
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid=dp.dim_partid
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0
group by fact_inventoryatlasid;	

 update fact_inventoryatlas f
set amt_gblStdPrice_Merck = ccpbudgetunitprice
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from  
tmp_ccpdatagscost_upd3 u ,fact_inventoryatlas f
WHERE  f.fact_inventoryatlasid = u.fact_inventoryatlasid
		and amt_gblStdPrice_Merck <> ccpbudgetunitprice
		and amt_gblStdPrice_Merck = 0;

drop table if exists tmp_ccpdatagscost_upd3;
/*BI-4693 End*/

/* 28 oct 2014 StdPricePMRA change */
/*update
fact_inventoryatlas f
from stdprice_pmra_merck u, dim_part dp, dim_plant pl
set f.amt_StdPricePMRA_Merck = u.stdprice_pmra
WHERE    lpad(dp.partnumber,8,'0') =  lpad(u.uin,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plant = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plant =pl.plantcode
		and ifnull(f.amt_StdPricePMRA_Merck,-99) <> u.stdprice_pmra
		*/
update fact_inventoryatlas f
set f.amt_StdPricePMRA_Merck = a.stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from (select fact_inventoryatlasid,avg(stdprice_pmra) as stdprice_pmra from  
ccpstpricefile201602 u, dim_part dp, dim_plant pl,fact_inventoryatlas f
where  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcode = dp.plant
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode =pl.plantcode 
group by fact_inventoryatlasid) a, fact_inventoryatlas f
WHERE   a.fact_inventoryatlasid = f.fact_inventoryatlasid
and  ifnull(f.amt_StdPricePMRA_Merck,-99) <> a.stdprice_pmra;

/*update fact_inventoryatlas f
from  
stdprice_pmra_merck_new_2015 u, dim_part dp, dim_plant pl
set f.amt_StdPricePMRA_Merck = u.stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*WHERE   lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid
		AND u.plantcountry = substr(pl.plantcode,1,2)
		and f.dim_plantid = pl.dim_plantid
		and u.plantcode<>pl.plantcode
		and ifnull(f.amt_StdPricePMRA_Merck,-99) <> u.stdprice_pmra
*/

/*drop table if exists tmp_upd_StdPricePMRA_Merck2
create table tmp_upd_StdPricePMRA_Merck2 as
select   max(u.stdprice_pmra) as stdprice_pmra,  f.dim_plantid, 
f.dim_partid
from fact_inventoryatlas f,
stdprice_pmra_merck_new_2015 u, dim_part dp, dim_plant pl
WHERE lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
and f.dim_partid = dp.dim_partid
AND u.plantcountry = substr(pl.plantcode,1,2)
and f.dim_plantid = pl.dim_plantid
and u.plantcode<>pl.plantcode
and not exists ( select 1 from stdprice_pmra_merck_new_2015 u2
where u2.plantcode = dp.plant
	and lpad(dp.partnumber,8,'0') = lpad(u2.partnumber,8,'0'))
	group by f.dim_plantid, 
f.dim_partid*/

/*update fact_inventoryatlas f
from 
tmp_upd_StdPricePMRA_Merck2 u
set f.amt_StdPricePMRA_Merck = u.stdprice_pmra
,dw_update_date = current_timestamp 
WHERE f.dim_partid = u.dim_partid
and f.dim_plantid = u.dim_plantid
and ifnull(f.amt_StdPricePMRA_Merck,-99) <> u.stdprice_pmra*/

/* 11.09.2015 Andrian New Std Price PMRA  */

/* Plant Country Code and Part Number */

drop table if exists tmp_upd_StdPricePMRA_Merck1;
create table tmp_upd_StdPricePMRA_Merck1 as	
select fact_inventoryatlasid, avg(stdprice_pmra) as stdprice_pmra
from
fact_inventoryatlas f,ccpstpricefile201602 u, dim_part dp, dim_plant pl 
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid and  dp.plant = pl.plantcode
		AND u.plantcountry = /*dp.plant*/ pl.country
		and amt_StdPricePMRA_Merck <> stdprice_pmra
		and amt_StdPricePMRA_Merck = 0
		group by fact_inventoryatlasid;
		
update fact_inventoryatlas f
set amt_StdPricePMRA_Merck = stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_upd_StdPricePMRA_Merck1 u, fact_inventoryatlas f
WHERE  f.fact_inventoryatlasid = u.fact_inventoryatlasid
		and amt_StdPricePMRA_Merck <> stdprice_pmra
		and amt_StdPricePMRA_Merck = 0;
		
drop table if exists tmp_upd_StdPricePMRA_Merck1;

/* Primary Site Code and Part Number	*/	
			
/* update fact_inventoryatlas f
set amt_StdPricePMRA_Merck = stdprice_pmra
	,dw_update_date = current_timestamp 
from  
ccpstpricefile201602 u, dim_part dp ,fact_inventoryatlas f
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid 
		AND u.plantcode =  dp.primarysite
		and amt_StdPricePMRA_Merck <> stdprice_pmra
		and amt_StdPricePMRA_Merck = 0 */

merge into fact_inventoryatlas f
    using ( select distinct fact_inventoryatlasid, avg(stdprice_pmra) as stdprice_pmra
        from 
        ccpstpricefile201602 u, dim_part dp ,fact_inventoryatlas f
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
       and f.dim_partid = dp.dim_partid /*and  dp.plant = 'XX20'*/
       AND u.plantcode = /*dp.plant*/ dp.primarysite
       and amt_StdPricePMRA_Merck <> stdprice_pmra
       and amt_StdPricePMRA_Merck = 0
       group by fact_inventoryatlasid
        ) upd
    on upd.fact_inventoryatlasid = f.fact_inventoryatlasid
    when matched then update set f.amt_StdPricePMRA_Merck = upd.stdprice_pmra
    ,dw_update_date = current_timestamp;
		
/* Primary Site Country Code and Part Number */

		
drop table if exists tmp_upd_StdPricePMRA_Merck2;
create table tmp_upd_StdPricePMRA_Merck2 as	
select fact_inventoryatlasid, avg(stdprice_pmra) as stdprice_pmra
from
fact_inventoryatlas f, 
ccpstpricefile201602 u, dim_part dp, dim_plant pl 
WHERE  lpad(dp.partnumber,8,'0') = lpad(u.partnumber,8,'0')
        and f.dim_partid = dp.dim_partid 
		AND u.plantcountry = /*dp.plant*/ pl.country
		and  dp.primarysite = pl.plantcode
		and amt_StdPricePMRA_Merck <> stdprice_pmra
		and amt_StdPricePMRA_Merck = 0
group by fact_inventoryatlasid;

 update fact_inventoryatlas f
set amt_StdPricePMRA_Merck = stdprice_pmra
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from  
tmp_upd_StdPricePMRA_Merck2 u ,fact_inventoryatlas f
WHERE  f.fact_inventoryatlasid = u.fact_inventoryatlasid
		and amt_StdPricePMRA_Merck <> stdprice_pmra
		and amt_StdPricePMRA_Merck = 0;
		
drop table if exists tmp_upd_StdPricePMRA_Merck2;


/* Sap Stock view update - 9 feb 2015 */

drop table if exists upd_fact_atlas_inv_tmp;
create table upd_fact_atlas_inv_tmp as
select MATNR material,WERKS plant,'Not Set' batchmanaged,SLABS unrestricted ,0 stockintransfer,
SINSM inqinsp,SEINM restricted,SSPEM "blocked",cast(0 as integer) "returns", 0 stockintransit from mkol
union all
select MARC_MATNR material,MARC_WERKS plant,'Not Set' batchmanaged,0 unrestricted,
MARC_UMLMC stockintrasfer, 0  inqinsp,0  restricted,0 "blocked" ,0 "returns",MARC_TRAME stockintransit  
from MARC;

insert into upd_fact_atlas_inv_tmp
select mara_matnr material,marc_werks plant, ifnull(marc_xchpf,'Not Set') batchmanaged ,
0 unrestricted, 0 stockintrasfer ,0  inqinsp,0  restricted ,0 "blocked" ,0 "returns"
,0 stockintransit
from MARA_MARC_MAKT;



insert into upd_fact_atlas_inv_tmp
select mska_MATNR material,mska_WERKS plant,'Not Set' batchmanaged,mska_KALAB unrestricted,0 stockintransfer,
mska_KAINS inqinsp,mska_KAEIN restricted,mska_KASPE "blocked", 0 "returns", 0 stockintransit
from mska;


insert into upd_fact_atlas_inv_tmp
select msku_MATNR material,msku_WERKS plant,'Not Set' batchmanaged,msku_KULAB unrestricted,0 stockintransfer,
msku_KUINS inqinsp,msku_KUEIN restricted,0  "blocked", 0 "returns", 0 stockintransit
	from msku;

insert into upd_fact_atlas_inv_tmp
select mslb_MATNR material,mslb_WERKS plant,'Not Set' batchmanaged,mslb_LBLAB unrestricted,
0 stockintransfer,mslb_LBINS inqinsp,mslb_LBEIN restricted,0 "blocked", 0 "returns", 0 stockintransit
 from mslb;
	

insert into upd_fact_atlas_inv_tmp
select mard_MATNR material,mard_WERKS plant,'Not Set' batchmanaged,mard_LABST unrestricted,
mard_UMLME stockintransfer, mard_INSME inqinsp,mard_EINME restricted,mard_SPEME "blocked",
mard_RETME "returns", 0 stockintransit from mard;


drop table if exists upd_fact_atlas_inv_tmp1;
create table upd_fact_atlas_inv_tmp1 as
select  material,plant,max(batchmanaged) batchmanaged,sum(unrestricted) unrestricted,
sum(stockintransfer) stockintransfer, sum(inqinsp) inqinsp,sum(restricted) restricted,
sum("blocked") "blocked", sum("returns") "returns", sum(stockintransit) stockintransit
from upd_fact_atlas_inv_tmp
group by material,plant;


update fact_inventoryatlas f
set dd_isbatchmanaged_merck = ifnull(u.batchmanaged,'Not Set')
,ct_Unr = ifnull(u.unrestricted,0)/marm_umrez 
,ct_StockInTransfer_merck = ifnull(u.stockintransfer,0)/marm_umrez 
,ct_StockInQInsp_merck = ifnull(u.inqinsp,0)/marm_umrez 
,ct_TotalRestrictedStock_merck = ifnull(u.restricted,0)/marm_umrez 
,ct_BlockedStock_merck = ifnull(u."blocked",0)/marm_umrez 
,ct_BlockedStockReturns_merck = ifnull(u."returns",0)/marm_umrez 
,ct_StockInTransit_merck = ifnull(u.stockintransit,0)/marm_umrez 
from upd_fact_atlas_inv_tmp1 u, dim_part dp, MARM m,fact_inventoryatlas f
WHERE   dp.partnumber = u.material
        and f.dim_partid = dp.dim_partid
		and m.marm_matnr =dp.partnumber
		and marm_meinh = 'IRU'
		AND u.plant = dp.plant;

/* end Sap Stock view update - 9 feb 2015 */

 /* Indep Req - 24.10.2014 */
 
 drop table if exists upd_fact_inventoryatlas_indepreqqty;
/*create table upd_fact_inventoryatlas_indepreqqty as
select b.pbim_werks,b.pbim_matnr,sum(a.pbed_plnmg) as pbed_plnmg
 from pbed a, pbim b
where a.pbed_bdzei = b.pbim_bdzei
and a.pbed_perxx in (select distinct calendarweekid from dim_date 
where datevalue between current_date 
and current_date + 7*52
group by  b.pbim_werks,b.pbim_matnr*/

/* dim_Date calendarweek difference QA vs Prod */
create table upd_fact_inventoryatlas_indepreqqty as
select b.pbim_werks,b.pbim_matnr,sum(a.pbed_plnmg) as pbed_plnmg
 from pbed a, pbim b
where a.pbed_bdzei = b.pbim_bdzei
and a.PBED_PDATU  between current_date and current_date+365
group by  b.pbim_werks,b.pbim_matnr;


update fact_inventoryatlas f
set ct_indepreqqty = pbed_plnmg/marm_umrez 
from upd_fact_inventoryatlas_indepreqqty u, dim_part pt,MARM m,fact_inventoryatlas f
where pt.dim_partid = f.dim_partid and pt.partnumber = u.pbim_matnr
and pt.plant = pbim_werks and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and ifnull(ct_indepreqqty,-1) <> pbed_plnmg/marm_umrez ;



/* end Indep Req - 24.10.2014 */


/* 20 feb 2015 */
update fact_inventoryatlas f
set dd_itemkeptonstock_merck = ifnull(
case 
when cast(safetystockiru_merck as decimal (18,0))<>0 then 'Yes' 
when safetytimeind <> 'Not Set' then 'Yes'
when RangeOfCoverage <> 'Not Set' then 'Yes'
when ItemSubType_Merck = 'RAW' then 'Yes'
when MRPLotSize = 'EX' then 'Yes' 
when ct_future_demandqty=0 then 'Yes'
else 'Yes' end, 'Not Set')
from dim_part pt,fact_inventoryatlas f
where f.dim_partid = pt.dim_partid
and dd_itemkeptonstock_merck <> ifnull(
case 
when cast(safetystockiru_merck as decimal (18,0))<>0 then 'Yes' 
when safetytimeind <> 'Not Set' then 'Yes'
when RangeOfCoverage <> 'Not Set' then 'Yes'
when ItemSubType_Merck = 'RAW' then 'Yes'
when MRPLotSize = 'EX' then 'Yes' 
when ct_future_demandqty=0 then 'Yes'
else 'Yes' end, 'Not Set');



/* new Lot size 16.04.2015 */
update fact_inventoryatlas f 
set ct_lotsize_merck =
case when MRPLotSize ='FX' OR MRPLotSize ='EX' OR MRPLotSize ='FS' 
THEN cast(greatest(fixedlotsizeiru_merck,minimumlotsizeiru_merck,maxlotsizeiru_merck) as decimal(18,0))
when MRPLotSize ='ZY' THEN 30 
when lotsizeindicator ='W' THEN ((cast(numberofperiods as decimal(18,0)) * 7)/365) *ct_future_demandqty 
when lotsizeindicator ='M' THEN ((cast(numberofperiods as decimal(18,0)) * 30)/365) *ct_future_demandqty 
when lotsizeindicator ='T' THEN (cast(numberofperiods as decimal(18,0))/365) *ct_future_demandqty  
ELSE 0 
END
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_lotsize_merck <>
case when MRPLotSize ='FX' OR MRPLotSize ='EX' OR MRPLotSize ='FS' 
THEN cast(greatest(fixedlotsizeiru_merck,minimumlotsizeiru_merck,maxlotsizeiru_merck) as decimal(18,0))
when MRPLotSize ='ZY' THEN 30 
when lotsizeindicator ='W' THEN ((cast(numberofperiods as decimal(18,0)) * 7)/365) *ct_future_demandqty 
when lotsizeindicator ='M' THEN ((cast(numberofperiods as decimal(18,0)) * 30)/365) *ct_future_demandqty 
when lotsizeindicator ='T' THEN (cast(numberofperiods as decimal(18,0))/365) *ct_future_demandqty  
ELSE 0  
END;


/* 12.03.2015 Site Code for MoS*/
update
fact_inventoryatlas f
set dd_sitecodeformos = ifnull(case when pl.tacticalring_merck <> 'ComOps' then pl.plantcode
else pt.primarysite end,'Not Set')
from dim_part pt, dim_plant pl,fact_inventoryatlas f
where f.dim_partid = pt.dim_partid
and pl.dim_plantid = f.dim_plantid
and dd_sitecodeformos <> ifnull(case when pl.tacticalring_merck <> 'ComOps' then pl.plantcode
else pt.primarysite end,'Not Set');

/*Georgiana 04 Nov Moving this part at the end of the script*/
/*28 June 2016 Andrian based on BI-3134*/
/*update fact_inventoryatlas f
set dd_sitecodeformos = 'CMO'
where  dd_sitecodeformos in ('AU90','DEC0','ES90','FR90','GB90','IE90','IT30','JP10','NL90','NZ90','XX90')*/

/*update fact_inventoryatlas f
set dd_sitecodeformos = 'FR60'
where  dd_sitecodeformos in ('FR40')*/
/*28 June 2016 Andrian based on BI-3134*/

update
fact_inventoryatlas f
set ct_sitedemandqty  = ifnull(case when pl.tacticalring_merck <> 'ComOps' then f.ct_future_demandqty
else 0 end, 0)
from  dim_plant pl,fact_inventoryatlas f
where  pl.dim_plantid = f.dim_plantid
and  ct_sitedemandqty  <> ifnull(case when pl.tacticalring_merck <> 'ComOps' then f.ct_future_demandqty
else 0 end, 0);
/* end 12.03.2015 Site Code for MoS*/

update fact_inventoryatlas f 
set ct_lotsizesite_merck =
case when MRPLotSize ='FX' OR MRPLotSize ='EX' OR MRPLotSize ='FS' 
THEN cast(greatest(fixedlotsizeiru_merck,minimumlotsizeiru_merck,maxlotsizeiru_merck) as decimal(18,0))
when MRPLotSize ='ZY' THEN 30 
when lotsizeindicator ='W' THEN ((cast(numberofperiods as decimal(18,0)) * 7)/365) *ct_sitedemandqty
when lotsizeindicator ='M' THEN ((cast(numberofperiods as decimal(18,0)) * 30)/365) *ct_sitedemandqty 
when lotsizeindicator ='T' THEN (cast(numberofperiods as decimal(18,0))/365) *ct_sitedemandqty  
ELSE 0 
END
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_lotsizesite_merck <>
case when MRPLotSize ='FX' OR MRPLotSize ='EX' OR MRPLotSize ='FS' 
THEN cast(greatest(fixedlotsizeiru_merck,minimumlotsizeiru_merck,maxlotsizeiru_merck) as decimal(18,0))
when MRPLotSize ='ZY' THEN 30 
when lotsizeindicator ='W' THEN ((cast(numberofperiods as decimal(18,0)) * 7)/365) *ct_sitedemandqty
when lotsizeindicator ='M' THEN ((cast(numberofperiods as decimal(18,0)) * 30)/365) *ct_sitedemandqty 
when lotsizeindicator ='T' THEN (cast(numberofperiods as decimal(18,0))/365) *ct_sitedemandqty  
ELSE 0  
END;

/* END new Lot size 16.04.2015 */

/*
update fact_inventoryatlas f 
from dim_part pt
set ct_lotsizeindays_merck =
case when ct_future_demandqty=0 THEN 0
when MRPLotSize ='FX' OR MRPLotSize ='EX'
THEN round(decimal(greatest(fixedlotsizeiru_merck,minimumlotsizeiru_merck,maxlotsizeiru_merck),2)/ct_future_demandqty *365,0)
when MRPLotSize ='ZY' THEN round(decimal(30,2)/ct_future_demandqty*365,0)
when lotsizeindicator ='W' THEN round(decimal((numberofperiods * 7),2)/ct_future_demandqty*365,0)
when lotsizeindicator ='M' THEN round(decimal((numberofperiods * 30),2)/ct_future_demandqty*365,0) 
when lotsizeindicator ='T' THEN round(decimal(numberofperiods,2) /ct_future_demandqty*365,0) 
ELSE 0 
END
where f.dim_partid = pt.dim_partid
and ct_lotsizeindays_merck <>
case when ct_future_demandqty=0 THEN 0
when MRPLotSize ='FX' OR MRPLotSize ='EX'
THEN round(decimal(greatest(fixedlotsizeiru_merck,minimumlotsizeiru_merck,maxlotsizeiru_merck),2)/ct_future_demandqty *365,0)
when MRPLotSize ='ZY' THEN round(decimal(30,2)/ct_future_demandqty*365,0)
when lotsizeindicator ='W' THEN round(decimal((numberofperiods * 7),2)/ct_future_demandqty*365,0)
when lotsizeindicator ='M' THEN round(decimal((numberofperiods * 30),2)/ct_future_demandqty*365,0) 
when lotsizeindicator ='T' THEN round(decimal(numberofperiods,2) /ct_future_demandqty*365,0) 
ELSE 0 
END
*/
update fact_inventoryatlas f 
set ct_lotsizeindays_merck =
case when ct_future_demandqty=0 THEN 0
ELSE (ct_lotsize_merck /ct_future_demandqty)*365
END
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_lotsizeindays_merck <>
case when ct_future_demandqty=0 THEN 0
ELSE (ct_lotsize_merck /ct_future_demandqty)*365
END;


/* end 20 feb 2015 */

/* 23 feb 2015 */
/*Madalina - 6 Jun 2018 - including DaysPerPeriod in the formula - APP-9645*/
update fact_inventoryatlas f 
set ct_minreleasedstockindays_merck = round(case when ct_future_demandqty=0 THEN 0
else (cast(safetystockiru_merck as decimal(18,4))/ct_future_demandqty)*365 + 
(case when safetytimeind = 'Not Set' then 0 else ((cast(safetytime as decimal(18,4))/5)*7) end) +
(case when RangeOfCoverage ='Not Set' THEN 0 
when periodindicator ='M' AND typeperiodlength=2 THEN targetcoverage1 
when periodindicator ='M' AND typeperiodlength=3 THEN targetcoverage1 / (case when daysperperiod = 0 or daysperperiod is null then 1 else daysperperiod end) * 30 
when periodindicator ='W' AND typeperiodlength=1 THEN round(cast(targetcoverage1 as decimal(18,4)) / 5 * 7,0) 
when periodindicator ='W' AND typeperiodlength=3 THEN targetcoverage1 *30
END)
end,0)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_minreleasedstockindays_merck <> round(case when ct_future_demandqty=0 THEN 0
else (cast(safetystockiru_merck as decimal(18,4))/ct_future_demandqty)*365 + 
(case when safetytimeind = 'Not Set' then 0 else ((cast(safetytime as decimal(18,4))/5)*7) end) +
(case when RangeOfCoverage ='Not Set' THEN 0 
when periodindicator ='M' AND typeperiodlength=2 THEN targetcoverage1 
when periodindicator ='M' AND typeperiodlength=3 THEN targetcoverage1 / (case when daysperperiod = 0 or daysperperiod is null then 1 else daysperperiod end) * 30 
when periodindicator ='W' AND typeperiodlength=1 THEN round(cast(targetcoverage1 as decimal(18,4))/ 5 * 7,0) 
when periodindicator ='W' AND typeperiodlength=3 THEN targetcoverage1 *30
END)
end,0);

update fact_inventoryatlas f 
set ct_mintotalstockindays_merck  = round(case when ct_future_demandqty=0 THEN 0
else ct_minreleasedstockindays_merck + pt.grproctimecaldays_merck end,0)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_mintotalstockindays_merck  <> round(case when ct_future_demandqty=0 THEN 0
else ct_minreleasedstockindays_merck + pt.grproctimecaldays_merck end,0);


update fact_inventoryatlas f 
set ct_mthsofsupplytargetindays_merck = round(case when ct_future_demandqty=0 THEN 0
when dd_itemkeptonstock_merck = 'No' then ct_mintotalstockindays_merck  
else ct_mintotalstockindays_merck  + cast(ct_lotsizeindays_merck as decimal(18,4))/2 end,0)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_mthsofsupplytargetindays_merck <> round(case when ct_future_demandqty=0 THEN 0
when dd_itemkeptonstock_merck = 'No' then ct_mintotalstockindays_merck  
else ct_mintotalstockindays_merck  + cast(ct_lotsizeindays_merck as decimal(18,4))/2 end,0);

update fact_inventoryatlas f 
set ct_mthsofsupplytarget_merck  = round((cast(ct_mthsofsupplytargetindays_merck as decimal(18,4))/365)*12,2)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_mthsofsupplytarget_merck  <> round((cast(ct_mthsofsupplytargetindays_merck as decimal(18,4))/365)*12,2);

update fact_inventoryatlas f 
set ct_maxstocklevelindays_merck = round(case when ct_future_demandqty=0 THEN 0
when dd_itemkeptonstock_merck = 'No' then ct_mintotalstockindays_merck  
else ct_mintotalstockindays_merck  + ct_lotsizeindays_merck end,0)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_maxstocklevelindays_merck <> round(case when ct_future_demandqty=0 THEN 0
when dd_itemkeptonstock_merck = 'No' then ct_mintotalstockindays_merck  
else ct_mintotalstockindays_merck  + ct_lotsizeindays_merck end,0);
/* end 23 feb 2015 */



/* 09.03.2015 update ct_interplantdemandqty from Purchase Req */
drop table if exists tmp_fact_inventoryatlas_frompurch_upd;
create table tmp_fact_inventoryatlas_frompurch_upd as 
select pt.partnumber,f.dim_supplplantstocktranspordid as dim_plantid,
sum(ct_purchasereq) as ct_interplantdemandqty
from fact_purchase_requisition f,  dim_part pt, dim_plant p,
dim_date d
where f.dim_materialid = pt.dim_partid and 
f.dim_plantid = p.dim_plantid
and d.dim_dateid = f.dim_datepurchasereqreleaseid
and dim_supplplantstocktranspordid<>1
and tacticalring_merck in
('Large Molecule' , 'Small Molecule' , 'External manufacturing')
and pt.ItemSubType_Merck <> 'FPP'
and dd_purchasereqclosed <> 'X' and dd_deletionindicator <> 'X'
and d.datevalue <= current_date+365
AND  d.datevalue >= current_date-90
and ct_orderedagainstpurchase = 0
group by pt.partnumber,f.dim_supplplantstocktranspordid;

update fact_inventoryatlas f 
set f.ct_interplantdemandqty = ifnull(u.ct_interplantdemandqty/marm_umrez,0)
from tmp_fact_inventoryatlas_frompurch_upd u, dim_part pt, dim_plant pl, MARM m,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid and f.dim_plantid = u.dim_plantid
and u.partnumber = pt.partnumber and u.dim_plantid = pl.dim_plantid
and pt.plant= pl.plantcode and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and f.ct_interplantdemandqty <> ifnull(u.ct_interplantdemandqty/marm_umrez,0);

/* end 09.03.2015 insert ct_interplantdemandqty from Purchase Req */

/* 19.03.2015 insert ct_interplantdemandqtypurchdoc from Purchase */

drop table if exists tmp_fact_inventoryatlas_frompurch_upd2;
create table tmp_fact_inventoryatlas_frompurch_upd2 as
select partnumber,f.dim_plantidsupplying as dim_plantid,
sum(
(case when atrb.itemdeliverycomplete = 'X' then 0.0000 
when atrb.ItemGRIndicator = 'Not Set' then 0.0000 
when (f.ct_DeliveryQty - f.ct_ReceivedQty) < 0 then 0.0000 
else (f.ct_DeliveryQty - f.ct_ReceivedQty) end
) * cast(PriceConversion_EqualTo as decimal(18,4))/cast(case when PriceConversion_Denom =0 then 1
else PriceConversion_Denom end as decimal(18,4))
) as ct_openqty
from fact_purchase f,  dim_part pt, dim_date d, dim_purchasemisc atrb
where f.dim_partid = pt.dim_partid 
and d.dim_dateid = f.dim_dateiddelivery
and atrb.dim_purchasemiscid = f.dim_purchasemiscid
and pt.ItemSubType_Merck <> 'FPP'
and f.dim_plantidsupplying <>1
and d.datevalue <= current_date+365
AND  d.datevalue >= current_date-90
--and PriceConversion_EqualTo=1 and PriceConversion_Denom =18
group by partnumber,f.dim_plantidsupplying;

update fact_inventoryatlas f 
set f.ct_interplantdemandqtypurchdoc = ifnull(u.ct_openqty/marm_umrez,0)
from tmp_fact_inventoryatlas_frompurch_upd2 u, dim_part pt, dim_plant pl, MARM m,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid and f.dim_plantid = u.dim_plantid
and u.partnumber = pt.partnumber and u.dim_plantid = pl.dim_plantid
and pt.plant= pl.plantcode and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and f.ct_interplantdemandqtypurchdoc <> ifnull(u.ct_openqty/marm_umrez,0);

/* end 19.03.2015 insert ct_interplantdemandqtypurchdoc from Purchase */


/* 15.04.2015 */
update
fact_inventoryatlas f
set dim_plantidsitefore2e = ifnull(site.dim_plantid ,1)
from  dim_part pt, dim_plant pl,dim_plant site,fact_inventoryatlas f
where f.dim_partid = pt.dim_partid
and f.dim_plantid = pl.dim_plantid
and site.plantcode = ifnull(case when pl.tacticalring_merck <> 'ComOps' then pl.plantcode
else pt.primarysite end,'Not Set')
and dim_plantidsitefore2e <> ifnull(site.dim_plantid ,1);

/* 13.03.2015 Manufacturing Site new cols */
update fact_inventoryatlas f
set dd_itemkeptonstocksite_merck = ifnull(
case 
when cast(safetystockiru_merck as decimal (18,0))<>0 then 'Yes' 
when safetytimeind <> 'Not Set' then 'Yes'
when RangeOfCoverage <> 'Not Set' then 'Yes'
when ItemSubType_Merck = 'RAW' then 'Yes'
when MRPLotSize = 'EX' then 'No' 
when ct_sitedemandqty =0 then 'Yes'
else 'Yes' end, 'Not Set')
from dim_part pt,fact_inventoryatlas f
where f.dim_partid = pt.dim_partid
and dd_itemkeptonstocksite_merck <> ifnull(
case 
when cast(safetystockiru_merck as decimal (18,0))<>0 then 'Yes' 
when safetytimeind <> 'Not Set' then 'Yes'
when RangeOfCoverage <> 'Not Set' then 'Yes'
when ItemSubType_Merck = 'RAW' then 'Yes'
when MRPLotSize = 'EX' then 'No' 
when ct_sitedemandqty =0 then 'Yes'
else 'Yes' end, 'Not Set');

/*update fact_inventoryatlas f 
from dim_part pt
set ct_lotsizeindayssite_merck =
case when ct_sitedemandqty =0 THEN 0
when MRPLotSize ='FX' OR MRPLotSize ='EX'
THEN  greatest(fixedlotsizeiru_merck,minimumlotsizeiru_merck,maxlotsizeiru_merck)/ct_sitedemandqty *365
when MRPLotSize ='ZY' THEN (30/ct_sitedemandqty)*365
when lotsizeindicator ='W' THEN (numberofperiods * 7)/ct_sitedemandqty *365
when lotsizeindicator ='M' THEN (numberofperiods * 30)/ct_sitedemandqty *365
when lotsizeindicator ='T' THEN numberofperiods /ct_sitedemandqty *365
ELSE 0 
END
where f.dim_partid = pt.dim_partid
and ct_lotsizeindayssite_merck <>
case when ct_sitedemandqty =0 THEN 0
when MRPLotSize ='FX' OR MRPLotSize ='EX'
THEN  greatest(fixedlotsizeiru_merck,minimumlotsizeiru_merck,maxlotsizeiru_merck)/ct_sitedemandqty *365
when MRPLotSize ='ZY' THEN (30/ct_sitedemandqty)*365
when lotsizeindicator ='W' THEN (numberofperiods * 7)/ct_sitedemandqty *365
when lotsizeindicator ='M' THEN (numberofperiods * 30)/ct_sitedemandqty *365
when lotsizeindicator ='T' THEN numberofperiods /ct_sitedemandqty *365
ELSE 0 
END
*/
update fact_inventoryatlas f 
set ct_lotsizeindayssite_merck =
case when ct_sitedemandqty =0 THEN 0
ELSE (ct_lotsizesite_merck/ct_sitedemandqty) *365  
END
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_lotsizeindayssite_merck <>
case when ct_sitedemandqty =0 THEN 0
ELSE (ct_lotsizesite_merck/ct_sitedemandqty) *365  
END;

/*Madalina - 6 Jun 2018 - including DaysPerPeriod in the formula - APP-9645*/
update fact_inventoryatlas f 
set ct_minreleasedstockindayssite_merck = round(case when ct_sitedemandqty=0 THEN 0
else (cast(safetystockiru_merck as decimal(18,4))/ct_sitedemandqty)*365 + 
(case when safetytimeind = 'Not Set' then 0 else ((cast(safetytime as decimal(18,4))/5)*7) end) +
(case when RangeOfCoverage ='Not Set' THEN 0 
when periodindicator ='M' AND typeperiodlength=2 THEN targetcoverage1 
when periodindicator ='M' AND typeperiodlength=3 THEN targetcoverage1 / (case when daysperperiod = 0 or daysperperiod is null then 1 else daysperperiod end) * 30 
when periodindicator ='W' AND typeperiodlength=1 THEN round(cast(targetcoverage1 as decimal(18,4)) / 5 * 7,0) 
when periodindicator ='W' AND typeperiodlength=3 THEN targetcoverage1 *30
END)
end,0)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_minreleasedstockindayssite_merck <> round(case when ct_sitedemandqty=0 THEN 0
else (cast(safetystockiru_merck as decimal(18,4))/ct_sitedemandqty)*365 + 
(case when safetytimeind = 'Not Set' then 0 else ((cast(safetytime as decimal(18,4))/5)*7) end) +
(case when RangeOfCoverage ='Not Set' THEN 0 
when periodindicator ='M' AND typeperiodlength=2 THEN targetcoverage1 
when periodindicator ='M' AND typeperiodlength=3 THEN targetcoverage1 / (case when daysperperiod = 0 or daysperperiod is null then 1 else daysperperiod end) * 30 
when periodindicator ='W' AND typeperiodlength=1 THEN round(cast(targetcoverage1 as decimal(18,4)) / 5 * 7,0) 
when periodindicator ='W' AND typeperiodlength=3 THEN targetcoverage1 *30
END)
end,0);

/*Alin 7 Dec 2017 logic change request APP-8355*/
update fact_inventoryatlas f 
set ct_mintotalstockindayssite_merck  =  round(case when ct_sitedemandqty=0 THEN 0
else ct_minreleasedstockindayssite_merck + pt.grproctimecaldays_merck +
(case when ProcurementType = 'F' and  SpecialProcurement <> 'Not Set'
then LeadTime else 0 end)
end,0)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_mintotalstockindayssite_merck  <> round(case when ct_sitedemandqty=0 THEN 0
else ct_minreleasedstockindayssite_merck + pt.grproctimecaldays_merck +
(case when ProcurementType = 'F' and  SpecialProcurement <> 'Not Set'
then LeadTime else 0 end)
end,0);

update fact_inventoryatlas f 
set ct_mthsofsupplytargetindayssite_merck = round(case when ct_sitedemandqty=0 THEN 0
when dd_itemkeptonstocksite_merck = 'No' then ct_mintotalstockindayssite_merck  
else ct_mintotalstockindayssite_merck  + ct_lotsizeindayssite_merck/2 end,0)
from dim_part pt,fact_inventoryatlas f
where f.dim_partid = pt.dim_partid
and ct_mthsofsupplytargetindayssite_merck <> round(case when ct_sitedemandqty=0 THEN 0
when dd_itemkeptonstocksite_merck = 'No' then ct_mintotalstockindayssite_merck  
else ct_mintotalstockindayssite_merck  + ct_lotsizeindayssite_merck/2 end,0);

update fact_inventoryatlas f 
set ct_mthsofsupplytargetsite_merck  = round((ct_mthsofsupplytargetindayssite_merck/365)*12,2)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_mthsofsupplytargetsite_merck  <> round((ct_mthsofsupplytargetindayssite_merck/365)*12,2);

update fact_inventoryatlas f 
set ct_maxstocklevelindayssite_merck = round(case when ct_sitedemandqty=0 THEN 0
when dd_itemkeptonstocksite_merck = 'No' then ct_mintotalstockindayssite_merck  
else ct_mintotalstockindayssite_merck  + ct_lotsizeindayssite_merck end,0)
from dim_part pt,fact_inventoryatlas f 
where f.dim_partid = pt.dim_partid
and ct_maxstocklevelindayssite_merck <> round(case when ct_sitedemandqty=0 THEN 0
when dd_itemkeptonstocksite_merck = 'No' then ct_mintotalstockindayssite_merck  
else ct_mintotalstockindayssite_merck  + ct_lotsizeindayssite_merck end,0);

/* end 13.03.2015 Manufacturing Site new cols */

/* 20-Mar-2015 Octavian: Adding dd_sitecodeformostitle from dim_plant */
update fact_inventoryatlas f
set dd_sitecodeformostitle = ifnull(planttitle_merck,'Not Set')
from dim_plant pl,fact_inventoryatlas f
where pl.plantcode = f.dd_sitecodeformos
and dd_sitecodeformostitle <> ifnull(planttitle_merck,'Not Set');
/* 20-Mar-2015 Octavian: END OF CHANGES */


/* 29.04.2015 MoS Fixed Target */
 update fact_inventoryatlas f
 set f.ct_mosfixedtarget_merck  = t.mosfixedtarget 
 from  dim_plant pl,  inventoryfixedtargets_merck t,fact_inventoryatlas f
  where f.dim_plantidsitefore2e = pl.dim_plantid and pl.plantcode = t.plantcode
  and f.ct_mosfixedtarget_merck  <> t.mosfixedtarget;

/* Octavian: Every Angle Transition Addons */  
update fact_inventoryatlas f 
SET amt_grossweight_marm = ifnull(MARM_BRGEW,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_part dp,MARM m,dim_unitofmeasure uom,fact_inventoryatlas f 
where m.MARM_MATNR = dp.partnumber
and dp.dim_partid = f.dim_partid 
and m.MARM_MEINH = uom.uom
and f.dim_unitofmeasureid = uom.dim_unitofmeasureid
and f.amt_grossweight_marm <> ifnull(MARM_BRGEW,0);
/* Octavian: Every Angle Transition Addons */ 

drop table if exists upd_fact_inventoryatlas_indepreqqty;
create table upd_fact_inventoryatlas_indepreqqty as
select b.pbim_werks,b.pbim_matnr,sum(a.pbed_plnmg) as pbed_plnmg,
sum(case when  a.PBED_PDATU>=current_date and a.PBED_PDATU<=PREPROCESS.LAST_DAY(current_date) then  a.pbed_plnmg else 0 end) pbed_plnmgrestofmonth,
sum(case when  a.PBED_PDATU>=PREPROCESS.LAST_DAY(current_date)+1 and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,1)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth1,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,1) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,2)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth2,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,2) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,3)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth3,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,3) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,4)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth4,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,4) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,5)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth5,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,5) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,6)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth6,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,6) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,7)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth7,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,7) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,8)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth8,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,8) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,9)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth9,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,9) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,10)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth10,
sum(case when  a.PBED_PDATU>=add_months(PREPROCESS.LAST_DAY(current_date)+1,10) and a.PBED_PDATU<=PREPROCESS.LAST_DAY(add_months(current_date,11)) then  a.pbed_plnmg else 0 end) pbed_plnmgmonth11
 from pbed a, pbim b
where a.pbed_bdzei = b.pbim_bdzei
and a.PBED_PDATU  between current_date and current_date+365
group by  b.pbim_werks,b.pbim_matnr;


update fact_inventoryatlas f
set ct_indepreqqty = pbed_plnmg/marm_umrez,
    ct_inderestof =  pbed_plnmgrestofmonth/marm_umrez,
    ct_inde1 = pbed_plnmgmonth1/marm_umrez,
    ct_inde2 = pbed_plnmgmonth2/marm_umrez,
    ct_inde3 = pbed_plnmgmonth3/marm_umrez,
    ct_inde4 = pbed_plnmgmonth4/marm_umrez,
    ct_inde5 = pbed_plnmgmonth5/marm_umrez,
    ct_inde6 = pbed_plnmgmonth6/marm_umrez,
    ct_inde7 = pbed_plnmgmonth7/marm_umrez,
    ct_inde8 = pbed_plnmgmonth8/marm_umrez,
    ct_inde9 = pbed_plnmgmonth9/marm_umrez,
    ct_inde10 = pbed_plnmgmonth10/marm_umrez,
    ct_inde11 = pbed_plnmgmonth11/marm_umrez
from upd_fact_inventoryatlas_indepreqqty u, dim_part pt,MARM m,fact_inventoryatlas f
where pt.dim_partid = f.dim_partid and pt.partnumber = u.pbim_matnr
and pt.plant = pbim_werks and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU' ;

/*Andrian-Based on ticket BI-1546*/
drop table if exists tmp01_onhandqtyiru;
create table tmp01_onhandqtyiru as
select  a.fact_inventoryatlasid,sum(d.ct_onhandqtyiru) as ct_onhandqtyiru
 from fact_inventoryatlas a, 
                 dim_part b, 
                 dim_plant c, 
       fact_inventoryaging d, 
                  dim_part e,
                  dim_plant f
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
and d.dim_partid = e.dim_partid
and d.dim_plantidstockatsite = f.dim_plantid
and b.partnumber = e.partnumber 
and c.plantcode = f.plantcode
and d.ct_onhandqtyiru <> 0
group by a.fact_inventoryatlasid;

update fact_inventoryatlas a
set a.ct_onhandqtyiru = b.ct_onhandqtyiru
 from tmp01_onhandqtyiru b,fact_inventoryatlas a
where a.fact_inventoryatlasid = b.fact_inventoryatlasid
and a.ct_onhandqtyiru <> b.ct_onhandqtyiru;
 
 /* 19 May 2016 Georgiana  EA Changes according to BI-2896*/
 
drop table if exists upd_fact_inventoryatlas_indepreqqty;
create table upd_fact_inventoryatlas_indepreqqty as
select b.pbim_werks,b.pbim_matnr,sum(a.pbed_plnmg) as pbed_plnmg
 from pbed a, pbim b
where a.pbed_bdzei = b.pbim_bdzei
and year(a.PBED_PDATU)=year(current_date)+1
group by  b.pbim_werks,b.pbim_matnr;


update fact_inventoryatlas f
set ct_openorderdemandnextcalyear = (case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end) 
from upd_fact_inventoryatlas_indepreqqty u, dim_part pt,MARM m,fact_inventoryatlas f
where pt.dim_partid = f.dim_partid and pt.partnumber = u.pbim_matnr
and pt.plant = pbim_werks and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and ifnull(ct_openorderdemandnextcalyear,-1) <> (case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end) ;

/*Update ct_indepreqqty in dim_part*/
update dim_part dp
set dp.indepreqqty = f.ct_indepreqqty 
from fact_inventoryatlas f,dim_part dp
where dp.dim_partid=f.dim_partid
and ifnull(dp.indepreqqty,-1) <> f.ct_indepreqqty; 

/*Update ct_openorderdemandnextcalyear in Dim_part*/

update dim_part dp
set dp.openorderdemandnextcalyear= f.ct_openorderdemandnextcalyear 
from fact_inventoryatlas f,dim_part dp
where dp.dim_partid=f.dim_partid
and ifnull(dp.openorderdemandnextcalyear,-1) <> f.ct_openorderdemandnextcalyear;

/*End of changes 19 May 2016*/

drop table if exists tmp_fact_inventoryatlas_frompurch_upd;
drop table if exists tmp_fact_inventoryatlas_ins_upd;
drop table if exists tmp_fact_inventoryatlas_globalcurrency;
drop table if exists tmp_fact_inventoryatlas_ins_upd2;
drop table if exists tmp_fact_inventoryatlas_ins_upd3;
drop table if exists upd_fact_inventoryatlas_indepreqqty;

drop table if exists tmp_futuredemandqty_001;
create table tmp_futuredemandqty_001 as
select distinct dim_Partid,
ct_future_demandqty
from fact_inventoryatlas;

/*03 Mar 2017 Georgiana Changes BI-5653 Reset future_quantity to 0 in Part Dimension as there might be materials that are deleted from Z1PP_INVBW table and future_demandqty should be 0 in those cases*/
update dim_part 
set future_demandqty=0;  
/*03 Mar 2017 END*/

update dim_part a 
set a.future_demandqty = b.ct_future_demandqty
from tmp_futuredemandqty_001 b,dim_part a
where a.dim_partid = b.dim_partid;

drop table if exists tmp_futuredemandqty_001;

/* MOVED DOWN - Alin Gheorghe 18 Nov 2016*/
/*
update fact_inventoryatlas
set ct_baselinevalue = 0
where ct_baselinevalue <> 0

update fact_inventoryatlas
set ct_baselinemos = 0
where ct_baselinemos <> 0

update fact_inventoryatlas a 
set a.ct_baselinevalue = ifnull(c.baselinevalue,0)
from sitebaselines c,fact_inventoryatlas a, dim_part dp 
where a.dd_sitecodeformos = c.sitee2e
/* WS3: change link logic of 3 fields regarding Baseline - START*/
/*
AND DP.DIM_PARTID = A.DIM_PARTID
and dp.partnumber = 'XXXXXX'
/* WS3: change link logic of 3 fields regarding Baseline - END*/
/*
and a.ct_baselinevalue <> ifnull(c.baselinevalue,0)

update fact_inventoryatlas a 
set a.ct_baselinemos = ifnull(c.baselinemos,0)
from sitebaselines c,fact_inventoryatlas a, dim_part dp
where a.dd_sitecodeformos = c.sitee2e
/* WS3: change link logic of 3 fields regarding Baseline - START*/
/*
AND DP.DIM_PARTID = A.DIM_PARTID
and dp.partnumber = 'XXXXXX'
/* WS3: change link logic of 3 fields regarding Baseline - END*/
/*
and a.ct_baselinemos <> ifnull(c.baselinemos,0)

update fact_inventoryatlas
set ct_sitebaselinedemandccp = 0
where ct_sitebaselinedemandccp <> 0

update fact_inventoryatlas a 
set a.ct_sitebaselinedemandccp = ifnull(c.sitebaselinedemandccp,0)
from  sitebaselines c, fact_inventoryatlas a, dim_part dp 
 where a.dd_sitecodeformos = c.sitee2e
 /* WS3: change link logic of 3 fields regarding Baseline - START*/
 /*
 AND DP.DIM_PARTID = A.DIM_PARTID
and dp.partnumber = 'XXXXXX'
/* WS3: change link logic of 3 fields regarding Baseline - END*/
/*
and a.ct_sitebaselinedemandccp <> ifnull(c.sitebaselinedemandccp,0)

/*end MOVED DOWN - Alin Gheorghe 18 Nov 2016*/

update fact_inventoryatlas
set ct_baselinematerialmos = 0
where ct_baselinematerialmos <> 0;

update fact_inventoryatlas
set ct_anticipationstock = 0
where ct_anticipationstock <> 0;

update fact_inventoryatlas a 
set a.ct_baselinematerialmos = ifnull(d.baselinematerialmos,0)
from dim_part b, dim_plant c, (select partnumber,plantcode,max(baselinematerialmos) as baselinematerialmos  from materialbaselinesc
group by partnumber,plantcode) d,fact_inventoryatlas a 
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
and b.partnumber = d.partnumber
and c.plantcode = d.plantcode
and a.ct_baselinematerialmos <> ifnull(d.baselinematerialmos,0);

update fact_inventoryatlas a 
set a.ct_anticipationstock = ifnull(d.anticipationstock,0)
from dim_part b, dim_plant c, materialbaselinesc d,fact_inventoryatlas a 
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
and b.partnumber = d.partnumber
and c.plantcode = d.plantcode
and a.ct_anticipationstock <> ifnull(d.anticipationstock,0);

update fact_inventoryaging
set dd_availableinminmax = 'Not Set';

update fact_inventoryaging a
set a.dd_availableinminmax = 'X'
where exists (select 1 from fact_inventoryatlas b
where a.dim_partid = b.dim_partid);

update fact_inventoryatlas
set ct_totalstockccp = (ct_unr + ct_stockinqinsp_merck +ct_totalrestrictedstock_merck 
+ct_stockintransfer_merck +ct_intransitstockqty) * amt_gblstdprice_merck
where ct_totalstockccp <> (ct_unr + ct_stockinqinsp_merck +ct_totalrestrictedstock_merck 
+ct_stockintransfer_merck +ct_intransitstockqty) * amt_gblstdprice_merck;

update fact_inventoryatlas
SET ct_totalstockiru = ct_unr + ct_stockinqinsp_merck +ct_totalrestrictedstock_merck +ct_stockintransfer_merck +ct_intransitstockqty
WHERE ct_totalstockiru <> ct_unr + ct_stockinqinsp_merck +ct_totalrestrictedstock_merck +ct_stockintransfer_merck +ct_intransitstockqty;

update fact_inventoryatlas
set ct_totalstockccp = ct_totalstockiru * amt_gblstdprice_merck
where ct_totalstockccp <> ct_totalstockiru * amt_gblstdprice_merck;



update fact_inventoryatlas d
SET d.ct_totalstockiru = a.stockqty
FROM fact_inventoryatlas d,newiteminstockbrinit a, dim_part b, dim_plant c
WHERE a.uin = b.partnumber 
and a.plantcode = b.plant
and a.plantcode = c.plantcode
and b.dim_partid = d.dim_partid
and c.dim_plantid = d.dim_plantid
and d.ct_totalstockiru <> a.stockqty;

update fact_inventoryatlas
set ct_totalstockccp = ct_totalstockiru * amt_gblstdprice_merck
where ct_totalstockccp <> ct_totalstockiru * amt_gblstdprice_merck;

drop table if exists tmp001_inventoryatlas;
create table tmp001_inventoryatlas as
select sum(ct_onhandqtyIRU * amt_gblstdprice_merck) + (select sum(ct_BlockedStock_merck*amt_gblstdprice_merck) from fact_inventoryatlas) as onhandamountnotavailableinMinMax 
from fact_inventoryaging
where dd_availableinminmax = 'Not Set';

delete from number_fountain m where m.table_name = 'fact_inventoryatlas';
INSERT INTO NUMBER_FOUNTAIN
select 	'fact_inventoryatlas',
		ifnull(max(f.fact_inventoryatlasid), 
		       ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inventoryatlas f;

insert into fact_inventoryatlas (fact_inventoryatlasid,dd_sitecodeformos,ct_totalstockccp)
select (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryatlas') + row_number() over (order by ''),
'Other Non SAP MRP',onhandamountnotavailableinMinMax from tmp001_inventoryatlas;


/* Lines from Inventory not in MinMax */

/* One time only 
delete from number_fountain m where m.table_name = 'dim_part'

insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1

insert into dim_part(dim_partid,partnumber,	PartNumber_NoLeadZero)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') + row_number() over(order by ''),
'XXXXXX','XXXXXX' */

delete from number_fountain m where m.table_name = 'fact_inventoryatlas';

insert into number_fountain
select 	'fact_inventoryatlas',
	ifnull(max(d.fact_inventoryatlasid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inventoryatlas d
where d.fact_inventoryatlasid<> 1;

delete from fact_inventoryatlas
where dim_partid in (select dim_partid from dim_part where partnumber = 'XXXXXX');
insert into fact_inventoryatlas(fact_inventoryatlasid,dd_sitecodeformos,dd_sitecodeformostitle,dim_partid,ct_othernonsapmrpstockvalueccp)
SELECT  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_inventoryatlas') + row_number() over(order by '') as fact_inventoryatlasid,
pl.plantcode,ifnull(pl.planttitle_merck,'Not Set') dd_sitecodeformostitle, (select dim_partid from dim_part where partnumber = 'XXXXXX') as dim_partid,sum(ct_onhandqtyIRU * amt_gblstdprice_merck)
from fact_inventoryaging ia INNER JOIN dim_plant pl
ON ia.dim_plantid = pl.dim_plantid
where dd_availableinminmax = 'Not Set'
group by 2,3,4;

/*Georgiana 04 Nov 2016 changed the formula: using f.dd_sitecodeformos = pl.plantcode instead the one with the case*/
update fact_inventoryatlas f
set dim_plantidsitefore2e = ifnull(site.dim_plantid ,1)
from  dim_part pt, dim_plant pl,dim_plant site,fact_inventoryatlas f
where f.dim_partid = pt.dim_partid
/*and site.plantcode = ifnull(case when pl.tacticalring_merck <> 'ComOps' then pl.plantcode
else pt.primarysite end,'Not Set')*/
and f.dd_sitecodeformos = pl.plantcode
and site.plantcode = pl.plantcode
AND pt.partnumber = 'XXXXXX'
and dim_plantidsitefore2e <> ifnull(site.dim_plantid ,1);


 /*Alin-Based on ticket BI-4669*/
drop table if exists tmp01_onhandqty;
create table tmp01_onhandqty as
select  a.fact_inventoryatlasid,sum(d.ct_onhandqty) as ct_onhandqty
 from fact_inventoryatlas a, 
                 dim_part b, 
                 dim_plant c, 
       fact_inventoryaging d, 
                  dim_part e,
                  dim_plant f
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
and d.dim_partid = e.dim_partid
and d.dim_plantidstockatsite = f.dim_plantid
and b.partnumber = e.partnumber 
and c.plantcode = f.plantcode
and d.ct_onhandqty <> 0
group by a.fact_inventoryatlasid;

update fact_inventoryatlas a
set a.ct_onhandqty = b.ct_onhandqty
 from tmp01_onhandqty b,fact_inventoryatlas a
where a.fact_inventoryatlasid = b.fact_inventoryatlasid
and a.ct_onhandqty <> b.ct_onhandqty;

/* row deleted according to ticket  BI-3134*/
delete from fact_inventoryatlas
where dim_partid in (select dim_partid from dim_part where partnumber = 'Not Set' and primarysite = 'Not Set')
and  dd_sitecodeformos like '%Other Non SAP MRP%';

/*BI-4598 - MinMax: change logic MoS - Std Price Alin Gheorghe ----logic restored to old version*/
/*
drop table if exists temp_std_mos
create table temp_std_mos as
select 
fact_inventoryatlasid, b.dim_partid, c.dim_plantid, b.partnumber, b.primarysite, c.plantcode, b.plant, ct_indepreqqty, amt_stdpricepmra_merck, ct_future_demandqty, ct_totalstockiru, 
CASE WHEN ct_indepreqqty*amt_stdpricepmra_merck = 0 AND (ct_future_demandqty * amt_stdpricepmra_merck = 0 AND ct_totalstockiru * amt_stdpricepmra_merck > 0) then 9999.99
WHEN ct_indepreqqty*amt_stdpricepmra_merck = 0 THEN 0 
ELSE 12*SUM( (ct_totalstockiru) * amt_stdpricepmra_merck ) / SUM ( ct_indepreqqty*amt_stdpricepmra_merck  )
END as ct_mos_std
from fact_inventoryatlas f, dim_part b, dim_plant c
where f.dim_partid = b.dim_partid
and f.dim_plantid = c.dim_plantid
and B.plant= c.plantcode
group by fact_inventoryatlasid, b.dim_partid, c.dim_plantid, b.partnumber, b.primarysite, c.plantcode, b.plant, ct_indepreqqty, amt_stdpricepmra_merck, ct_future_demandqty, ct_totalstockiru

UPDATE FACT_INVENTORYATLAS
SET CT_MOS_std  = 0


update fact_inventoryatlas d
SET d.ct_mos_std = a.ct_mos_std
FROM fact_inventoryatlas d,temp_std_mos a
WHERE d.fact_inventoryatlasid = a.fact_inventoryatlasid
*/

/*BI-4598 - MinMax: change logic MoS - UIN Alin Gheorghe*/
drop table if exists temp_std_UIN;
create table temp_std_UIN as
select 
fact_inventoryatlasid,ct_future_demandqty, ct_totalstockiru,b.partnumber,b.dim_partid,c.plantcode,
case 
when (ct_future_demandqty)=0 and ct_totalstockiru > 0 then 9999.99
when (ct_future_demandqty)=0 and ct_totalstockiru = 0 then 0
else (ct_totalstockiru)/(ct_future_demandqty) *12
end as ct_mos_uin
from fact_inventoryatlas f, dim_part b, dim_plant c
where f.dim_partid = b.dim_partid
and f.dim_plantid = c.dim_plantid
and B.PLANT = c.plantcode;


UPDATE FACT_INVENTORYATLAS
SET CT_MOS_UIN  = 0;


update fact_inventoryatlas d
SET d.ct_mos_uin = a.ct_mos_uin
FROM fact_inventoryatlas d,temp_std_UIN a
WHERE d.fact_inventoryatlasid = a.fact_inventoryatlasid;

/* INSERT ACCORDING TO WS3: change link logic of 3 fields regarding Baseline */

/*22 Nov 2016 Georgiana One time insert*/
/* One time only 
delete from number_fountain m where m.table_name = 'dim_plant'

insert into number_fountain
select 	'dim_plant',
	ifnull(max(d.dim_plantid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_plant d
where d.dim_plantid<> 1

insert into dim_plant(dim_plantid,plantcode,	Name)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_plant') + row_number() over(order by ''),
'CMO','CMO' */
delete from number_fountain m where m.table_name = 'fact_inventoryatlas';

insert into number_fountain
select 	'fact_inventoryatlas',
	ifnull(max(d.fact_inventoryatlasid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inventoryatlas d
where d.fact_inventoryatlasid<> 1;

insert into fact_inventoryatlas(fact_inventoryatlasid,dd_sitecodeformos,dd_sitecodeformostitle,dim_partid, dim_plantidsitefore2e, CT_BASELINEMOS, CT_BASELINEVALUE, ct_sitebaselinedemandccp)
SELECT  (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_inventoryatlas') + row_number() over(order by '') as fact_inventoryatlasid,
pl.plantcode, ifnull(pl.planttitle_merck,'Not Set') dd_sitecodeformostitle, (select dim_partid from dim_part where partnumber = 'XXXXXX') as dim_partid, dim_plantid, BASELINEMOS, BASELINEVALUE, SITEBASELINEDEMANDCCP
from SITEBASELINES ia INNER JOIN dim_plant pl
on ia.sitee2e = pl.plantcode
where not exists (select 1 from fact_inventoryatlas f, dim_part dp where pL.PLANTCODE = f.DD_SITECODEFORMOS and f.dim_partid = dp.dim_partid and dp.partnumber = 'XXXXXX');

/* MOVED DOWN - Alin Gheorghe 18 Nov 2016*/

update fact_inventoryatlas
set ct_baselinevalue = 0
where ct_baselinevalue <> 0;

update fact_inventoryatlas
set ct_baselinemos = 0
where ct_baselinemos <> 0;

update fact_inventoryatlas a 
set a.ct_baselinevalue = ifnull(c.baselinevalue,0)
from sitebaselines c,fact_inventoryatlas a, dim_part dp 
where a.dd_sitecodeformos = c.sitee2e
/* WS3: change link logic of 3 fields regarding Baseline - START*/
AND DP.DIM_PARTID = A.DIM_PARTID
and dp.partnumber = 'XXXXXX'
/* WS3: change link logic of 3 fields regarding Baseline - END*/
and a.ct_baselinevalue <> ifnull(c.baselinevalue,0);

update fact_inventoryatlas a 
set a.ct_baselinemos = ifnull(c.baselinemos,0)
from sitebaselines c,fact_inventoryatlas a, dim_part dp
where a.dd_sitecodeformos = c.sitee2e
/* WS3: change link logic of 3 fields regarding Baseline - START*/
AND DP.DIM_PARTID = A.DIM_PARTID
and dp.partnumber = 'XXXXXX'
/* WS3: change link logic of 3 fields regarding Baseline - END*/
and a.ct_baselinemos <> ifnull(c.baselinemos,0);

update fact_inventoryatlas
set ct_sitebaselinedemandccp = 0
where ct_sitebaselinedemandccp <> 0;

update fact_inventoryatlas a 
set a.ct_sitebaselinedemandccp = ifnull(c.sitebaselinedemandccp,0)
from  sitebaselines c, fact_inventoryatlas a, dim_part dp 
 where a.dd_sitecodeformos = c.sitee2e
 /* WS3: change link logic of 3 fields regarding Baseline - START*/
 AND DP.DIM_PARTID = A.DIM_PARTID
and dp.partnumber = 'XXXXXX'
/* WS3: change link logic of 3 fields regarding Baseline - END*/
and a.ct_sitebaselinedemandccp <> ifnull(c.sitebaselinedemandccp,0);
/*end MOVED DOWN - Alin Gheorghe 18 Nov 2016*/


/*Georgiana 04 Nov Moving this part at the end of the script- this part should always be at the end of the script*/
/*28 June 2016 Andrian based on BI-3134*/
update fact_inventoryatlas f
set dd_sitecodeformos = 'CMO'
where  dd_sitecodeformos in ('AU90','DEC0','ES90','FR90','GB90','IE90','IT30','JP10','NL90','NZ90','XX90', 'MX90');

update fact_inventoryatlas f
set dd_sitecodeformos = 'FR60'
where  dd_sitecodeformos in ('FR40');
/*28 June 2016 Andrian based on BI-3134*/

/*28 June 2016 Andrian based on BI-3134*/
update fact_inventoryatlas f
set dd_sitecodeformostitle = 'CMO'
where  dd_sitecodeformos = 'CMO';

 /* 19 May 2016 Georgiana  EA Changes according to BI-2896*/
 
/*drop table if exists upd_fact_inventoryatlas_indepreqqty
create table upd_fact_inventoryatlas_indepreqqty as
select b.pbim_werks,b.pbim_matnr,sum(a.pbed_plnmg) as pbed_plnmg
 from pbed a, pbim b
where a.pbed_bdzei = b.pbim_bdzei
and year(a.PBED_PDATU)=year(current_date)+1
group by  b.pbim_werks,b.pbim_matnr*/


/*update fact_inventoryatlas f
set ct_openorderdemandnextcalyear = (case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end) 
from upd_fact_inventoryatlas_indepreqqty u, dim_part pt,MARM m,fact_inventoryatlas f
where pt.dim_partid = f.dim_partid and pt.partnumber = u.pbim_matnr
and pt.plant = pbim_werks and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and ifnull(ct_openorderdemandnextcalyear,-1) <> (case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end) */

/*18 Aug 2016 Georgiana changing the logic BI-2896*/
/* 07 dec 2016 Correction BI-2896*/


drop table if exists upd_fact_inventoryatlas_indepreqqty1;
create table upd_fact_inventoryatlas_indepreqqty1 as
select b.pbim_matnr,b.pbim_werks,sum(a.pbed_plnmg) as pbed_plnmg
 from pbed a, pbim b
where a.pbed_bdzei = b.pbim_bdzei
and (a.PBED_PDATU  BETWEEN current_date AND (current_date + 364))
group by  b.pbim_werks,b.pbim_matnr;


drop table if exists upd_fact_inventoryatlas_indepreqqty2;
create table upd_fact_inventoryatlas_indepreqqty2 as
select resb_matnr,resb_werks, sum(resb_bdmng) as resb_bdmng from resb where 
(RESB_BDTER BETWEEN current_date AND (current_date + 364))
and resb_xloek is null 
and resb_kzear is null
group by resb_matnr,resb_werks;

drop table if exists upd_fact_inventoryatlas_indepreqqty3;
create table upd_fact_inventoryatlas_indepreqqty3 as
select EBAN_MATNR,EBAN_RESWK,sum(EBAN_MENGE) as EBAN_MENGE from eban where
(EBAN_FRGDT BETWEEN current_date AND (current_date + 364))
and ifnull(EBAN_EBELN, 'Not Set') = 'Not Set'
group by EBAN_MATNR,EBAN_RESWK;

drop table if exists upd_fact_inventoryatlas_indepreqqty4;
create table upd_fact_inventoryatlas_indepreqqty4 as
select VBAP_MATNR,VBAP_WERKS, sum(case when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f_so.dd_ItemRelForDelv = 'X' then (case when (f_so.ct_ScheduleQtyBaseUoM - (f_so.ct_ShippedAgnstOrderQtyBaseUoM - f_so.ct_CmlQtyReceivedBaseUoM )) < 0 then 0.0000 else (f_so.ct_ScheduleQtyBaseUoM - (f_so.ct_ShippedAgnstOrderQtyBaseUoM - f_so.ct_CmlQtyReceivedBaseUoM )) end) when (f_so.ct_ScheduleQtyBaseUoM - f_so.ct_ShippedAgnstOrderQtyBaseUoM ) < 0 then 0.0000 else (f_so.ct_ScheduleQtyBaseUoM - f_so.ct_ShippedAgnstOrderQtyBaseUoM ) end) as OpenOrderQuantity 
 from vbak_vbap_vbep 
inner join fact_salesorder f_so on VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
inner join dim_salesdocumenttype sdt on f_so.dim_salesdocumenttypeid = sdt.dim_salesdocumenttypeid
where 
(VBEP_MBDAT BETWEEN current_date AND (current_date + 364))
group by VBAP_MATNR,VBAP_WERKS;

drop table if exists upd_fact_inventoryatlas_indepreqqty5;
create table upd_fact_inventoryatlas_indepreqqty5 as
select EKPO_MATNR,EKPO_WERKS, sum((EKET_WEMNG-EKPO_MENGE)*(case when EKPO_UMREN = 0 then 1 else EKPO_UMREN end)) as quantity
from ekko_ekpo_eket_ekbe
where (EKET_EINDT  BETWEEN current_date AND (current_date + 364)) and
EKPO_RETPO='X'
group by EKPO_MATNR,EKPO_WERKS;


drop table if exists tmp_for_upd_ct_openorderdemandnextcalyear;
create table tmp_for_upd_ct_openorderdemandnextcalyear as
select * from (
(select * from  upd_fact_inventoryatlas_indepreqqty1)
union 
(select * from upd_fact_inventoryatlas_indepreqqty2)
union
(select * from  upd_fact_inventoryatlas_indepreqqty3)
union 
(select * from upd_fact_inventoryatlas_indepreqqty4)
union 
(select * from upd_fact_inventoryatlas_indepreqqty5)) t
group by t.pbim_matnr,t.pbim_werks, t.pbed_plnmg;

/*
merge into fact_inventoryatlas f
using (select distinct f.fact_inventoryatlasid,max(case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end)as openorderdemandnextcalyear
from tmp_for_upd_ct_openorderdemandnextcalyear u, dim_part pt,MARM m,fact_inventoryatlas f
where pt.dim_partid = f.dim_partid and pt.partnumber = u.pbim_matnr
and pt.plant = pbim_werks and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and ifnull(ct_openorderdemandnextcalyear,-1) <> (case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end)
group by f.fact_inventoryatlasid ) t
on t.fact_inventoryatlasid=f.fact_inventoryatlasid
when matched then update set ct_openorderdemandnextcalyear=t.openorderdemandnextcalyear*/
/*Update ct_indepreqqty in dim_part*/
update dim_part dp
set dp.indepreqqty = f.ct_indepreqqty 
from fact_inventoryatlas f,dim_part dp
where dp.dim_partid=f.dim_partid
and ifnull(dp.indepreqqty,-1) <> f.ct_indepreqqty; 

/*Update ct_openorderdemandnextcalyear in Dim_part*/

/*merge into dim_part dp
using (Select distinct dp.dim_partid,f.ct_openorderdemandnextcalyear
from fact_inventoryatlas f,dim_part dp
where dp.dim_partid=f.dim_partid
and ifnull(dp.openorderdemandnextcalyear,-1) <> f.ct_openorderdemandnextcalyear) t
on t.dim_partid = dp.dim_partid
when matched then update set dp.openorderdemandnextcalyear=t.ct_openorderdemandnextcalyear*/
/*End of changes 19 May 2016*/
merge into dim_part dp
using (Select distinct pt.dim_partid,max(case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end)as openorderdemandnextcalyear
from tmp_for_upd_ct_openorderdemandnextcalyear f,dim_part pt,marm m
where pt.partnumber = f.pbim_matnr
and pt.plant = f.pbim_werks
and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
group by pt.dim_partid) t
on t.dim_partid = dp.dim_partid
when matched then update set dp.openorderdemandnextcalyear=t.openorderdemandnextcalyear;

/* Octavian: 12/12/2016 Add Traffic Light BI-4745 */
merge into fact_inventoryatlas f
using (
select
dim_partid,
 CASE WHEN  SUM( (ct_mos_uin) ) > ( (SUM( (f_invatlas.ct_safety_workdays_grprocessing_time)  - 0.25* (f_invatlas.ct_safety_workdays) )
)  / 365 * 12)  
          AND SUM(  (ct_mos_uin) ) <  ( (SUM(  (ct_maxstocklevelindays_merck)  + 0.25 *  (ct_minreleasedstockindays_merck) ))  /365 * 12)    
          THEN '1'
 WHEN  SUM( (ct_mos_uin) ) >  ( (SUM( (f_invatlas.ct_safety_workdays_grprocessing_time)  - 0.75* (f_invatlas.ct_safety_workdays) ))  / 365 * 12)  
      AND SUM( (ct_mos_uin) ) < ( (SUM( (f_invatlas.ct_safety_workdays_grprocessing_time)  - 0.25* (f_invatlas.ct_safety_workdays) )
)  / 365 * 12) 
      THEN  '2'
 WHEN SUM(  (ct_mos_uin) ) >  ( (SUM(  (ct_maxstocklevelindays_merck)  + 0.25 *  (ct_minreleasedstockindays_merck) ))  /365 * 12) 
     AND SUM( (ct_mos_uin) ) < ( (SUM( (f_invatlas.ct_safety_eoqeotfx_workdays_gr_time)  + 0.75* (f_invatlas.ct_safety_workdays) ))  / 365 * 12) 
    THEN '2'
ELSE '3'
END as dd_trafficlight
from fact_inventoryatlas f_invatlas 
group by dim_partid) t
on f.dim_partid = t.dim_partid
when matched then update 
set f.dd_trafficlight = t.dd_trafficlight
where f.dd_trafficlight <> t.dd_trafficlight;

/*BI-5286 Alin Gheorghe 26.01.2017 */

delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryatlas';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryatlas',ifnull(max(fact_inventoryatlasid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
FROM fact_inventoryatlas;


INSERT INTO fact_inventoryatlas(fact_inventoryatlasid,
dim_plantid,
dim_partid,
DD_IQVRISKCODE ,
DD_IQVRISKDESCRIPTION,
DD_IQVREASONCODE,
DD_IQVREASONDESCRIPTION,
DD_IQVREASONCOMMENT,
dd_iqvriskcatcode,
dd_iqvriskcatdescription,
--AMT_IQVPROVISION, 
--CT_IQVPROVISION,
CT_IQVFIXED,
/*new cols*/
dd_update_mode,
dd_batch_per_plant,
dd_Base_Unit,
dd_Responsible_Area,
dd_IQV_Action_Code,
dd_IQV_Action_Description,
dd_Comment_IQV_Action,
dd_Last_Changed_by,
dim_First_IQV_Review_Dateid,
dim_Last_IQV_Review_Dateid,
dim_IQV_Due_Dateid,
dim_Risk_Creation_Dateid
/*new cols*/
)
SELECT 
(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryatlas') + row_number() over (order by ''),
pl.dim_plantid, dp.dim_partid, 
ifnull(iqv_risk_code, 'Not Set') AS iqv_risk_code,
ifnull(iqv_risk_description , 'Not Set') AS iqv_risk_description,
ifnull(iqv_reason_code , 'Not Set') AS iqv_reason_code,
ifnull(iqv_reason_description , 'Not Set') AS iqv_reason_description,
ifnull(iqv_comment_reason, 'Not Set') AS iqv_comment_reason,
ifnull(IQV_RISKCATCODE, 'Not Set') AS IQV_RISKCATCODE,
ifnull(IQV_RISKCATDESC, 'Not Set') AS IQV_RISKCATDESC,
--iqv_provision_amount ,
--iqv_provsion_qty ,
ifnull(iqv_fixed_qty, 0) AS iqv_fixed_qty,
/*new cols*/
ifnull(update_mode, 'Not Set') AS update_mode,
ifnull(batch_per_plant, 'Not Set') AS batch_per_plant,
ifnull(Base_Unit, 'Not Set') AS Base_Unit,
ifnull(Responsible_Area, 'Not Set') AS Responsible_Area,
ifnull(IQV_Action_Code, 'Not Set') AS IQV_Action_Code,
ifnull(IQV_Action_Description, 'Not Set') AS IQV_Action_Description,
ifnull(Comment_IQV_Action, 'Not Set') AS Comment_IQV_Action,
ifnull(Last_Changed_by, 'Not Set') AS Last_Changed_by,
1,
1,
1,
1
/*new cols*/
FROM IQV i, dim_part dp, dim_plant pl
where i.plant = pl.plantcode and 
i.plant = dp.plant and
i.material = dp.partnumber and
dp.plant = pl.plantcode 
and not exists(
select 1 from fact_inventoryatlas f  where
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL);


merge into fact_inventoryatlas f
using(
select fact_inventoryatlasid,
ifnull(min(iqv_risk_code), 'Not Set') as iqv_risk_code,
ifnull(min(iqv_risk_description), 'Not Set') as iqv_risk_description,
ifnull(min(iqv_reason_code), 'Not Set') as iqv_reason_code,
ifnull(min(iqv_reason_description), 'Not Set') as iqv_reason_description,
ifnull(min(iqv_comment_reason), 'Not Set') as iqv_comment_reason,
ifnull(min(IQV_RISKCATCODE), 'Not Set') as IQV_RISKCATCODE,
ifnull(min(IQV_RISKCATDESC), 'Not Set') as IQV_RISKCATDESC,
/*new cols*/
ifnull(min(update_mode), 'Not Set') as update_mode,
ifnull(min(batch_per_plant), 'Not Set') as batch_per_plant,
ifnull(min(Base_Unit), 'Not Set') as Base_Unit,
ifnull(min(Responsible_Area), 'Not Set') as Responsible_Area,
ifnull(min(IQV_Action_Code), 'Not Set') as IQV_Action_Code,
ifnull(min(IQV_Action_Description), 'Not Set') as IQV_Action_Description,
ifnull(min(Comment_IQV_Action), 'Not Set') as Comment_IQV_Action,
ifnull(min(Last_Changed_by), 'Not Set') as Last_Changed_by


from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlas f
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
DP.PARTNUMBER = I.MATERIAL AND
PL.PLANTCODE = I.PLANT
GROUP BY fact_inventoryatlasid
)t
on t.fact_inventoryatlasid = f.fact_inventoryatlasid
when matched then 
update set
DD_IQVRISKCODE = iqv_risk_code ,
DD_IQVRISKDESCRIPTION = iqv_risk_description,
DD_IQVREASONCODE =iqv_reason_code,
DD_IQVREASONDESCRIPTION =iqv_reason_description,
DD_IQVREASONCOMMENT = iqv_comment_reason,
DD_IQVRISKCATCODE = IQV_RISKCATCODE,
DD_IQVRISKCATDESCRIPTION = IQV_RISKCATDESC,
/*new cols*/
dd_update_mode = update_mode,
dd_batch_per_plant = batch_per_plant,
dd_Base_Unit = Base_Unit,
dd_Responsible_Area = Responsible_Area,
dd_IQV_Action_Code = IQV_Action_Code,
dd_IQV_Action_Description = IQV_Action_Description,
dd_Comment_IQV_Action = Comment_IQV_Action,
dd_Last_Changed_by = Last_Changed_by;

DROP TABLE IF EXISTS temp_test_invatl;
create table temp_test_invatl as
select  f.fact_inventoryatlasid, f.dim_partid,f.dim_plantid, 
--iqv_provision_amount as iqv_provision_amount,
--iqv_provsion_qty as iqv_provsion_qty,
iqv_fixed_qty as iqv_fixed_qty
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlas f
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL;

merge into fact_inventoryatlas f
using(
select  Z.fact_inventoryatlasid, Z.DIM_PARTID, Z.DIM_PLANTID,
--SUM(iqv_provision_amount) AS iqv_provision_amount ,
--SUM(iqv_provsion_qty) AS iqv_provsion_qty,
SUM(iqv_fixed_qty) AS iqv_fixed_qty
from temp_test_invatl Z
GROUP BY Z.fact_inventoryatlasid,  Z.DIM_PARTID, Z.DIM_PLANTID
) t
on  f.fact_inventoryatlasid = t.fact_inventoryatlasid
when matched then 
update set
--AMT_IQVPROVISION = iqv_provision_amount, 
--CT_IQVPROVISION = iqv_provsion_qty,
CT_IQVFIXED = iqv_fixed_qty;

DROP TABLE IF EXISTS temp_test_invatl;

/*NEW COLS DIM_DATES*/
MERGE INTO fact_inventoryatlas ST
USING
	(
SELECT DISTINCT first_value(f.fact_inventoryatlasid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryatlasid, 
first_value(dd.dim_dateid)  over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlas f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL AND
f.DD_BATCH_PER_PLANT = I.IQV_BATCH
AND dd.datevalue =  cast(ifnull(i.First_IQV_Review_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryatlasid = SRC.fact_inventoryatlasid
WHEN MATCHED THEN UPDATE
	SET ST.dim_First_IQV_Review_Dateid = SRC.dim_dateid
WHERE ST.dim_First_IQV_Review_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryatlas ST
USING
	(
SELECT DISTINCT first_value(f.fact_inventoryatlasid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryatlasid, 
first_value(dd.dim_dateid)  over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlas f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL AND
f.DD_BATCH_PER_PLANT = I.IQV_BATCH
AND dd.datevalue =  cast(ifnull(i.Last_IQV_Review_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryatlasid = SRC.fact_inventoryatlasid
WHEN MATCHED THEN UPDATE
	SET ST.dim_Last_IQV_Review_Dateid = SRC.dim_dateid
WHERE ST.dim_Last_IQV_Review_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryatlas ST
USING
	(
SELECT DISTINCT first_value(f.fact_inventoryatlasid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryatlasid, 
first_value(dd.dim_dateid)  over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlas f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL AND
f.DD_BATCH_PER_PLANT = I.IQV_BATCH
AND dd.datevalue =  cast(ifnull(i.IQV_Due_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryatlasid = SRC.fact_inventoryatlasid
WHEN MATCHED THEN UPDATE
	SET ST.dim_IQV_Due_Dateid = SRC.dim_dateid
WHERE ST.dim_IQV_Due_Dateid <> SRC.dim_dateid;


MERGE INTO fact_inventoryatlas ST
USING
	(
SELECT DISTINCT first_value(f.fact_inventoryatlasid) over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as fact_inventoryatlasid, 
first_value(dd.dim_dateid)  over (partition by i.plant, i.material, i.iqv_batch order by IQV_RISKCATCODE desc) as dim_dateid
from IQV i, dim_part dp, dim_plant pl, fact_inventoryatlas f, dim_date dd
where 
f.dim_partid = dp.dim_partid and 
f.dim_plantid = pl.dim_plantid and 
dp.plant = i.plant and
PL.PLANTCODE = I.PLANT AND
DP.PARTNUMBER = I.MATERIAL AND
f.DD_BATCH_PER_PLANT = I.IQV_BATCH
AND dd.datevalue =  cast(ifnull(i.Risk_Creation_Date, '0001-01-01') as date)
and pl.plantcode = dd.plantcode_factory
and dd.companycode = pl.companycode
	) SRC ON ST.fact_inventoryatlasid = SRC.fact_inventoryatlasid
WHEN MATCHED THEN UPDATE
	SET ST.dim_Risk_Creation_Dateid = SRC.dim_dateid
WHERE ST.dim_Risk_Creation_Dateid <> SRC.dim_dateid;






update fact_inventoryatlas set ct_totaliqvamountCCP=0;

drop table if exists tmp_for_upd_ct_totaliqvamountCCP;
create table tmp_for_upd_ct_totaliqvamountCCP as
select distinct f.dim_partid, f.dim_plantid,
AVG(CASE WHEN DD_IQVRISKCODE='1' THEN case when prt.uomiru = 0 then (CT_IQVPROVISION*amt_gblstdprice_merck) else ((CT_IQVPROVISION*amt_gblstdprice_merck) / prt.uomiru) end  
WHEN DD_IQVRISKCODE='2' THEN ct_onhandqtyIRU * amt_gblstdprice_merck
WHEN DD_IQVRISKCODE='3' THEN ct_onhandqtyIRU * amt_gblstdprice_merck
ELSE 0 END) as ct_totaliqvamountCCP
From fact_inventoryatlas f, dim_part prt
where f.dim_partid=prt.dim_partid
group by f.dim_partid,f.dim_plantid;

merge into fact_inventoryatlas f
using (select distinct f.fact_inventoryatlasid, t.ct_totaliqvamountCCP
from fact_inventoryatlas f,tmp_for_upd_ct_totaliqvamountCCP t
where
f.dim_partid=t.dim_partid 
and f.dim_plantid=t.dim_plantid 
) s
on f.fact_inventoryatlasid=s.fact_inventoryatlasid
when matched then update set f.ct_totaliqvamountCCP=s.ct_totaliqvamountCCP;

/*BI-5894*/
merge into fact_inventoryatlas fi
using (
select f.dim_partid,f.dim_plantid,sum((case when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000 
when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X' 
then (case when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 
then 0.0000 else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) end) 
when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000 
else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) end)) as ct_backorderquantityBUoM
 from fact_salesorder f, dim_salesdocumenttype sdt,dim_plant pl,dim_date dt,dim_part dp
where
f.dim_salesdocumenttypeid=sdt.dim_salesdocumenttypeid
and f.dim_partid=dp.dim_partid
and pl.dim_plantid=f.dim_plantid
and f.dim_orderduedate=dt.dim_dateid
and dt.datevalue between current_date -90 and current_date-1
AND (CASE WHEN sdt.DocumentType = 'AF' THEN 'IN' WHEN sdt.DocumentType = 'AG' THEN 'QT' WHEN sdt.DocumentType = 'AU' THEN 'SI' WHEN sdt.DocumentType = 'G2' THEN 'CR' WHEN sdt.DocumentType = 'KL' THEN 'FD' WHEN sdt.DocumentType = 'KM' THEN 'CQ' WHEN sdt.DocumentType = 'KN' THEN 'SD' WHEN sdt.DocumentType = 'L2' THEN 'DR' WHEN sdt.DocumentType = 'LP' THEN 'DS' WHEN sdt.DocumentType = 'TA' THEN 'OR' ELSE sdt.DocumentType END
 IN ('Z1O','OR','ZOR','ZSI','ZSO','ZSW') ) 
and f.dd_executionstatusschedule in ('Open','Partially Open')
and pl.tacticalring_merck='ComOps'
and (case when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000 
when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X' 
then (case when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 
then 0.0000 else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) end) 
when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000 
else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) end)>0
group by  f.dim_partid,f.dim_plantid) t
on t.dim_partid=fi.dim_partid
and t.dim_plantid=fi.dim_plantid
when matched then update set ct_backorderquantityBUoM=ifnull(t.ct_backorderquantityBUoM,0);


/* Update std_exchangerate_dateid by AROBESCU on 05 January 2016  */

UPDATE fact_inventoryatlas am
SET am.std_exchangerate_dateid = dt.dim_dateid
FROM fact_inventoryatlas am INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode and p.plantcode=dt.plantcode_factory
WHERE dt.datevalue = current_date
AND am.std_exchangerate_dateid <> dt.dim_dateid;

/*start Alin APP-6326 24 MAY 2017*/
merge into fact_inventoryatlas dp
using
(select fact_inventoryatlasid, dp.dim_partid, CONCAT(PRODFAMILYDESCRIPTION_MERCK_CONSOLIDATED,' - ', CAST(concentration AS DECIMAL(18,4)), ' - ', 
(case when ProductGroup_Merck = 'B' then 'DS'
WHEN contentsuom = 'KG' THEN 'GR'
WHEN contentsuom = 'ML' THEN 'L'
WHEN contentsuom = 'NA' THEN 'DS' 
ELSE contentsuom END)) as concat_GPF_Conc_Bulk_UOM
from dim_part dp, fact_inventoryatlas f
where f.dim_partid = dp.dim_partid
 ) t
on t.fact_inventoryatlasid = dp.fact_inventoryatlasid
when matched then update
set dp.dd_concat_GPF_Conc_Bulk_UOM = t.concat_GPF_Conc_Bulk_UOM
where dp.dd_concat_GPF_Conc_Bulk_UOM <> t.concat_GPF_Conc_Bulk_UOM;

/*Alin 2 june 2017 APP-6090*/
drop table if exists tmp01_iqvnoprovision_v2;
create table tmp01_iqvnoprovision_v2 as
select  a.fact_inventoryatlasid, sum(d.ct_iqvnoprovision_v2) as ct_iqvnoprovision_v2
 from fact_inventoryatlas a, 
                 dim_part b, 
                 dim_plant c, 
       fact_inventoryaging d, 
                  dim_part e,
                  dim_plant f
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
and d.dim_partid = e.dim_partid
and d.dim_plantid = f.dim_plantid
and b.partnumber = e.partnumber 
and c.plantcode = f.plantcode
group by a.fact_inventoryatlasid;

update fact_inventoryatlas a
set a.ct_iqvnoprovision_v2 = b.ct_iqvnoprovision_v2
 from tmp01_iqvnoprovision_v2 b,fact_inventoryatlas a
where a.fact_inventoryatlasid = b.fact_inventoryatlasid
and a.ct_iqvnoprovision_v2 <> b.ct_iqvnoprovision_v2;

drop table if exists tmp01_iqvnoprovision_v2;


drop table if exists tmp01_iqvprovision_v2;
create table tmp01_iqvprovision_v2 as
select  a.fact_inventoryatlasid, sum(d.ct_iqvprovision_v2) as ct_iqvprovision_v2
 from fact_inventoryatlas a, 
                 dim_part b, 
                 dim_plant c, 
       fact_inventoryaging d, 
                  dim_part e,
                  dim_plant f
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
and d.dim_partid = e.dim_partid
and d.dim_plantid = f.dim_plantid
and b.partnumber = e.partnumber 
and c.plantcode = f.plantcode
group by a.fact_inventoryatlasid;


update fact_inventoryatlas a
set a.ct_iqvprovision_v2 = b.ct_iqvprovision_v2
from tmp01_iqvprovision_v2 b,fact_inventoryatlas a
where a.fact_inventoryatlasid = b.fact_inventoryatlasid
and a.ct_iqvprovision_v2 <> b.ct_iqvprovision_v2;

drop table if exists tmp01_iqvprovision_v2;

/* Andrei - APP-6210 - Add MRP REQUIRED LATEST */
drop table if exists tmp_mrp_required;
create table tmp_mrp_required as 
select  f.dd_sitecodeformostitle,
case when sum((ct_indepreqqty+ct_interplantdemandqty+ct_interplantdemandqtypurchdoc)*amt_gblstdprice_merck) = 0 then NULL else (sum((ct_mthsofsupplytargetindayssite_merck/365)*ct_sitedemandqty*amt_gblstdprice_merck)
+
(sum(case when dpl.tacticalring_merck = 'ComOps' then (cast(ct_mthsofsupplytargetindays_merck as decimal(18))/365)* ct_future_demandqty*amt_gblstdprice_merck else 0 end)) + sum(ct_anticipationstock))/sum((ct_indepreqqty+ct_interplantdemandqty+ct_interplantdemandqtypurchdoc)*amt_gblstdprice_merck)*12 END
as ct_mrprequired
from fact_inventoryatlas f inner join dim_plant dpl 
on f.dim_plantid = dpl.dim_plantid
group by f.dd_sitecodeformostitle;

update fact_inventoryatlashistory h
set h.ct_mrprequiredmos = ifnull(f.ct_mrprequired,0)
from fact_inventoryatlashistory h, tmp_mrp_required f
where f.dd_sitecodeformostitle = h.dd_sitecodeformostitle
and h.ct_mrprequiredmos <> ifnull(f.ct_mrprequired,0);

/*Alin 22 Nov 2107 APP-8124*/
--ct_avg_overunderstock
drop table if exists tmp_for_average_over_understock;
create table tmp_for_average_over_understock as 
select distinct dp.partnumber,dp.primarysite, dp.MRPCONTROLLER, dt.monthyear, CALENDARMONTHID,
ifnull(
CASE WHEN (SUM(( (cast((ct_mthsofsupplytargetindayssite_merck/365)*ct_sitedemandqty*amt_gblstdprice_merck as decimal (30,15))) + (CASE WHEN (dpl.tacticalring_merck) = 'ComOps' THEN (((ct_mthsofsupplytargetindays_merck)/365)* ct_future_demandqty*amt_gblstdprice_merck) ELSE 0 END) + (ct_anticipationstock) )/
(CASE WHEN (ct_sitedemandqty*amt_gblstdprice_merck) = 0 THEN
(CASE WHEN (ct_future_demandqty*amt_gblstdprice_merck) = 0 THEN 1 ELSE (ct_future_demandqty*amt_gblstdprice_merck) END)
ELSE (ct_sitedemandqty*amt_gblstdprice_merck) END)*12)) = 0 THEN NULL ELSE (( (CASE WHEN SUM( (amt_gblstdprice_merck*ct_future_demandqty) ) = 0 and ( (SUM( (ct_totalstockccp) ) +SUM( (CT_OTHERNONSAPMRPSTOCKVALUECCP) ) +SUM( (ct_BlockedStock_merck*amt_gblstdprice_merck) )) - (AVG(ct_iqvprovision_v2)) ) > 0 THEN '9999.99'
WHEN SUM( (amt_gblstdprice_merck*ct_future_demandqty) ) = 0 and ( (SUM( (ct_totalstockccp) ) +SUM( (CT_OTHERNONSAPMRPSTOCKVALUECCP) ) +SUM( (ct_BlockedStock_merck*amt_gblstdprice_merck) )) - (AVG(ct_iqvprovision_v2)) ) = 0 THEN 0
ELSE CASE WHEN SUM( (amt_gblstdprice_merck*ct_future_demandqty) ) = 0 THEN NULL ELSE ( ( (SUM( (ct_totalstockccp) ) +SUM( (CT_OTHERNONSAPMRPSTOCKVALUECCP) ) +SUM( (ct_BlockedStock_merck*amt_gblstdprice_merck) )) - (AVG(ct_iqvprovision_v2)) ) / SUM( (amt_gblstdprice_merck*ct_future_demandqty) ))*12
END 
END) - (SUM(( (cast((ct_mthsofsupplytargetindayssite_merck/365)*ct_sitedemandqty*amt_gblstdprice_merck as decimal (30,15))) + (CASE WHEN (dpl.tacticalring_merck) = 'ComOps' THEN (((ct_mthsofsupplytargetindays_merck)/365)* ct_future_demandqty*amt_gblstdprice_merck) ELSE 0 END) + (ct_anticipationstock) )/
(CASE WHEN (ct_sitedemandqty*amt_gblstdprice_merck) = 0 THEN
(CASE WHEN (ct_future_demandqty*amt_gblstdprice_merck) = 0 THEN 1 ELSE (ct_future_demandqty*amt_gblstdprice_merck) END)
ELSE (ct_sitedemandqty*amt_gblstdprice_merck) END)*12)) )/ CASE WHEN (SUM(( (cast((ct_mthsofsupplytargetindayssite_merck/365)*ct_sitedemandqty*amt_gblstdprice_merck as decimal (30,15))) + (CASE WHEN (dpl.tacticalring_merck) = 'ComOps' THEN (((ct_mthsofsupplytargetindays_merck)/365)* ct_future_demandqty*amt_gblstdprice_merck) ELSE 0 END) + (ct_anticipationstock) )/
(CASE WHEN (ct_sitedemandqty*amt_gblstdprice_merck) = 0 THEN
(CASE WHEN (ct_future_demandqty*amt_gblstdprice_merck) = 0 THEN 1 ELSE (ct_future_demandqty*amt_gblstdprice_merck) END)
ELSE (ct_sitedemandqty*amt_gblstdprice_merck) END)*12)) = 0 THEN NULL ELSE (SUM(( (cast((ct_mthsofsupplytargetindayssite_merck/365)*ct_sitedemandqty*amt_gblstdprice_merck as decimal (30,15))) + (CASE WHEN (dpl.tacticalring_merck) = 'ComOps' THEN (((ct_mthsofsupplytargetindays_merck)/365)* ct_future_demandqty*amt_gblstdprice_merck) ELSE 0 END) + (ct_anticipationstock) )/
(CASE WHEN (ct_sitedemandqty*amt_gblstdprice_merck) = 0 THEN
(CASE WHEN (ct_future_demandqty*amt_gblstdprice_merck) = 0 THEN 1 ELSE (ct_future_demandqty*amt_gblstdprice_merck) END)
ELSE (ct_sitedemandqty*amt_gblstdprice_merck) END)*12)) END )*100 END, 0)
 as over_under_stock
from fact_inventoryatlashistory f, dim_part dp, dim_date dt, dim_plant dpl
where f.dim_partid = dp.dim_partid
and dt.dim_dateid = dim_snapshotdateid
and dpl.dim_plantid = f.dim_plantidsitefore2e

/*
and dp.partnumber = '000011'
and dp.productfamily_merck = '5933'
and dp.primarysite = 'NL10'
and dp.MRPController = '10A'
*/
and dt.monthyear in (

select distinct monthyear
from dim_date
where datevalue between  (select distinct datevalue from dim_Date where datevalue = current_date - interval '11' month) 

and (select distinct datevalue from dim_date where datevalue = current_date)
)

group by --fact_inventoryatlashistoryid, 
dp.partnumber, 
dp.primarysite, dp.MRPCONTROLLER, dt.monthyear, CALENDARMONTHID

ORDER BY DP.PARTNUMBER, DP.PRIMARYSITE, DP.MRPCONTROLLER, CALENDARMONTHID;



drop table if exists tmp_ct_avg_overunderstock;
create table tmp_ct_avg_overunderstock as
SELECT  distinct t.partnumber, t.mrpcontroller, t.primarysite,
avg(over_under_stock) as ct_avg_overunderstock
FROM FACT_inventoryatlashistory f, tmp_for_average_over_understock t,  dim_part dp, dim_date dt, dim_plant dpl
where f.dim_partid = dp.dim_partid
and dt.dim_dateid = dim_snapshotdateid
and dpl.dim_plantid = f.dim_plantidsitefore2e
and t.partnumber = dp.partnumber
and t.mrpcontroller = dp.mrpcontroller
and t.calendarmonthid = dt.calendarmonthid
group by t.partnumber, t.mrpcontroller, t.primarysite;

update FACT_inventoryatlashistory f
set f.ct_avg_overunderstock = t.ct_avg_overunderstock
FROM FACT_inventoryatlashistory f, tmp_ct_avg_overunderstock t,  dim_part dp, dim_date dt, dim_plant dpl
where f.dim_partid = dp.dim_partid
and dt.dim_dateid = dim_snapshotdateid
and dpl.dim_plantid = f.dim_plantidsitefore2e
and t.partnumber = dp.partnumber
and t.mrpcontroller = dp.mrpcontroller
and f.ct_avg_overunderstock <> t.ct_avg_overunderstock;

drop table if exists tmp_ct_avg_overunderstock;
drop table if exists tmp_for_average_over_understock;


--dd_avg_overunderstock

merge into FACT_inventoryatlashistory f
using(
select distinct fact_inventoryatlashistoryid, 
dp.partnumber, dp.mrpcontroller, dp.primarysite, --dt.monthyear,
--dp.dim_partid,
 avg(ct_avg_overunderstock) as ct_avg_overunderstock
from fact_inventoryatlashistory f, dim_part dp, dim_date dt, dim_plant dpl
where f.dim_partid = dp.dim_partid
and dt.dim_dateid = dim_snapshotdateid
and dpl.dim_plantid = f.dim_plantidsitefore2e
and dpl.plantcode = dp.plant
--and dp.partnumber = '000011'
--and mrpcontroller = '10A'
and dt.monthyear in (
select distinct monthyear
from dim_date
where datevalue between  (select distinct datevalue from dim_Date where datevalue = current_date - interval '11' month) 
and (select distinct datevalue from dim_date where datevalue = current_date))
--group by dp.dim_partid
group by fact_inventoryatlashistoryid, dp.partnumber, dp.mrpcontroller, dp.primarysite--, dt.monthyear
)t
on t.fact_inventoryatlashistoryid = f.fact_inventoryatlashistoryid
when matched then update 
set f.dd_avg_overunderstock = ifnull(t.ct_avg_overunderstock, 0);
