
UPDATE fact_purchase p
   SET p.ct_ScheduledQty_Merck = EKET_MENGE * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM EKKO_EKPO_EKET e
, fact_purchase p
   WHERE p.dd_DocumentNo = e.EKPO_EBELN
AND p.dd_DocumentItemNo = e.EKPO_EBELP
AND p.dd_scheduleno = e.EKET_ETENR
and p.ct_ScheduledQty_Merck <> EKET_MENGE * CASE EKPO_RETPO WHEN 'X' THEN -1 ELSE 1 END;

/* iOTIF/iLIFR changes */
UPDATE fact_purchase
SET ct_lineotif_merck = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_lineotif_merck <> 0;

UPDATE fact_purchase
SET ct_docotif_merck = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_docotif_merck <> 0;

/* FOr a given item, if all schedules have dd_ontime_delv = 'Y' AND dd_qtyinfull = 'Y', then ct_lineotif_merck = 1. Else 0 for all rows of that item */
DROP TABLE IF EXISTS tmp_fp_otif1_item;
CREATE TABLE tmp_fp_otif1_item
AS
SELECT distinct dd_documentno, dd_documentitemno
FROM fact_purchase
WHERE dd_ontime_delv = 'Y' AND dd_qtyinfull = 'Y';

DROP TABLE IF EXISTS tmp_fp_otif2_item;
CREATE TABLE tmp_fp_otif2_item
AS
SELECT distinct dd_documentno, dd_documentitemno
FROM fact_purchase
WHERE ( dd_ontime_delv <> 'Y' OR dd_qtyinfull <> 'Y' );


MERGE INTO tmp_fp_otif1_item f1
USING tmp_fp_otif2_item f2 ON (f1.dd_documentno = f2.dd_documentno AND f1.dd_documentitemno = f2.dd_documentitemno)
WHEN MATCHED THEN DELETE;

UPDATE fact_purchase fp
SET ct_lineotif_merck = 1
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_fp_otif1_item o
, fact_purchase fp
WHERE fp.dd_documentno = o.dd_documentno
AND fp.dd_documentitemno = o.dd_documentitemno
AND ct_lineotif_merck <> 1;

DROP TABLE IF EXISTS tmp_fp_otif1_item;
DROP TABLE IF EXISTS tmp_fp_otif2_item;


/* FOr a given document, if all items have ct_lineotif_merck = 1, then ct_docotif_merck = 1. Else 0 for all rows of that document */
DROP TABLE IF EXISTS tmp_fp_otif1;
CREATE TABLE tmp_fp_otif1
AS
SELECT distinct dd_documentno
FROM fact_purchase
WHERE ct_lineotif_merck = 1;

DROP TABLE IF EXISTS tmp_fp_otif2;
CREATE TABLE tmp_fp_otif2
AS
SELECT distinct dd_documentno
FROM fact_purchase
WHERE ct_lineotif_merck = 0;

MERGE INTO tmp_fp_otif1 f1
USING tmp_fp_otif2 f2 ON (f1.dd_documentno = f2.dd_documentno)
WHEN MATCHED THEN DELETE;

UPDATE fact_purchase fp
SET ct_docotif_merck = 1
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_fp_otif1 o
, fact_purchase fp
WHERE fp.dd_documentno = o.dd_documentno
AND ct_docotif_merck <> 1;


DROP TABLE IF EXISTS tmp_fp_otif1;
DROP TABLE IF EXISTS tmp_fp_otif2;

/* Suchithra - 03Nov2014 - Added for updating Top Supplier Flag (fact_purchase.dd_topSupplierflag_merck) for Merck */

update fact_purchase
set dd_topSupplierflag_merck = 'Not Set'
where dd_topSupplierflag_merck <> 'Not Set';
/* Added automatically by update_dw_update_date.pl*/


DROP TABLE IF EXISTS upd_dd_topSupplierflag_merck;
CREATE TABLE upd_dd_topSupplierflag_merck AS
SELECT DISTINCT fpr.dim_plantidordering,fpr.dim_vendorid, CASE WHEN ifnull(tsp.topsupplier,'Not Set') ='Y' THEN 'X' ELSE 'Not Set' END AS dd_topSupplierflag_merck
FROM  dim_plant dpl,
  dim_vendor dvn,
  tmp_merck_topSupplier tsp
,  fact_purchase fpr
WHERE   tsp.orderingplantcode = dpl.plantcode
AND   tsp.vendornumber = TRIM(LEADING '0' FROM dvn.vendornumber)
AND   fpr.dim_plantidordering = dpl.dim_plantid
AND   fpr.dim_vendorid = dvn.dim_vendorid
and  fpr.dd_topSupplierflag_merck <> CASE WHEN ifnull(tsp.topsupplier,'Not Set') ='Y' THEN 'X' ELSE 'Not Set' END;

UPDATE fact_purchase fpr
SET fpr.dd_topSupplierflag_merck = tmp.dd_topSupplierflag_merck
FROM fact_purchase fpr, upd_dd_topSupplierflag_merck tmp
WHERE fpr.dim_plantidordering = tmp.dim_plantidordering
AND fpr.dim_vendorid = tmp.dim_vendorid;

/* Supplying Plant 22.04.2015 */
drop table if exists tmp_EKKO_Z1SD_SR_ACTUAL_upd;
create table tmp_EKKO_Z1SD_SR_ACTUAL_upd as
select e.EKPO_EBELN,e.EKPO_EBELP,e.EKET_ETENR,e.EKKO_UNSEZ,
e.EKPO_WERKS,e.EKPO_BUKRS,z.Z1SD_SR_ACTUAL_PRWRK,
z.Z1SD_SR_ACTUAL_SRNUM,z.Z1SD_SR_ACTUAL_VBELN
 from EKKO_EKPO_EKET e, Z1SD_SR_ACTUAL z
WHERE Z1SD_SR_ACTUAL_EBELN = e.EKPO_EBELN
and EKKO_UNSEZ = Z1SD_SR_ACTUAL_SRNUM;

UPDATE fact_purchase p
set Dim_PlantidSupplying = dps.Dim_Plantid,
    dw_update_date = current_timestamp
from (select distinct VBAK_BSTNK,VBAP_MATNR,VBAP_WERKS,vbap_pstyv from VBAK_VBAP_VBEP) v, dim_part pt, Dim_Plant dps, fact_purchase p
where v.VBAK_BSTNK = p.dd_DocumentNo
and v.VBAP_MATNR = pt.partnumber
and p.dim_partid = pt.dim_partid
and dps.plantcode = v.VBAP_WERKS
and v.vbap_pstyv = 'TAN'
AND Dim_PlantidSupplying <> dps.Dim_Plantid;

UPDATE fact_purchase p
   SET dd_scheduledreceiptno_merck = ifnull(Z1SD_SR_ACTUAL_SRNUM,'Not Set')
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_EKKO_Z1SD_SR_ACTUAL_upd e
, fact_purchase p
   WHERE p.dd_DocumentNo = e.EKPO_EBELN
AND p.dd_DocumentItemNo = e.EKPO_EBELP
AND p.dd_scheduleno = e.EKET_ETENR
and dd_scheduledreceiptno_merck <> ifnull(Z1SD_SR_ACTUAL_SRNUM,'Not Set');

UPDATE fact_purchase p
   SET dd_salesdocno_merck = ifnull(Z1SD_SR_ACTUAL_VBELN,'Not Set')
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_EKKO_Z1SD_SR_ACTUAL_upd e
, fact_purchase p
   WHERE p.dd_DocumentNo = e.EKPO_EBELN
AND p.dd_DocumentItemNo = e.EKPO_EBELP
AND p.dd_scheduleno = e.EKET_ETENR
and dd_salesdocno_merck <> ifnull(Z1SD_SR_ACTUAL_VBELN,'Not Set');
/* end Supplying Plant 22.04.2015 */

update fact_purchase p
set dd_lpa_merck = CASE WHEN doctyp.type = 'LPA' and dtcr.calendaryear <= YEAR(current_date) -2 THEN 'Exclude LPA' ELSE 'Not Set' END
from dim_documenttype doctyp, dim_date dtcr, fact_purchase p
where p.dim_documenttypeid = doctyp.dim_documenttypeid
and p.dim_dateidcreate = dtcr.dim_dateid
and dd_lpa_merck <> CASE WHEN doctyp.type = 'LPA' and dtcr.calendaryear <= YEAR(current_date) -2 THEN 'Exclude LPA' ELSE 'Not Set' END;


/*Georgiana Every Angle Changes 18 Feb 2015*/
/*KONV columns*/
/*Purchase*/
/*take PB00 over PBXX when both are available*/

drop table if exists tmp_forcheck;
create table tmp_forcheck as
select  conditionitemnumber,documentconditionnumber,
group_concat(DISTINCT Conditiontypecode ORDER BY Conditiontypecode ASC SEPARATOR ',')   as Conditiontypecode2
from dim_transactionpricecondition where Conditiontypecode in ('PB00','PBXX')
group by conditionitemnumber,documentconditionnumber;

delete  from dim_transactionpricecondition dt
where
exists (select 1 from tmp_forcheck t
        where dt.conditionitemnumber=t.conditionitemnumber
        and dt.documentconditionnumber =t.documentconditionnumber
        and dt.Conditiontypecode='PBXX'
        and Conditiontypecode2 like '%PB00%PBXX');

DROP TABLE IF EXISTS tmp_konv_col_pf;
CREATE TABLE tmp_konv_col_pf
AS
SELECT documentconditionnumber as KONV_KNUMV,
conditionitemnumber as KONV_KPOSN, avg(conditionrate) as avg_KONV_KBETR,  sum(ConditionValue) sum_KONV_KWERT,
Conditiontypecode as KONV_KSCHL
FROM dim_transactionpricecondition
where documentconditionnumber in (select distinct EKKO_KNUMV from EKKO_EKPO_EKET)
and sourceflag ='PURCHASE'
GROUP BY documentconditionnumber,conditionitemnumber, conditiontypecode;

UPDATE fact_purchase fpr
SET amt_grossprice = k1.avg_KONV_KBETR
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,tmp_konv_col_pf k1, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PB00'
AND amt_grossprice <> k1.avg_KONV_KBETR;

UPDATE fact_purchase fpr
SET amt_grossprice = k1.avg_KONV_KBETR
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,tmp_konv_col_pf k1, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PBXX'
AND amt_grossprice <> k1.avg_KONV_KBETR;



UPDATE fact_purchase fpr
SET amt_netprice = k1.avg_KONV_KBETR
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,tmp_konv_col_pf k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PN00'
AND amt_netprice <> k1.avg_KONV_KBETR;

UPDATE fact_purchase fpr
SET amt_price = k1.avg_KONV_KBETR
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,tmp_konv_col_pf k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PR00'
AND amt_price <> k1.avg_KONV_KBETR;

UPDATE fact_purchase fpr
SET amt_freightcharged = k1.avg_KONV_KBETR
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,tmp_konv_col_pf k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'ZTRS'
AND amt_freightcharged <> k1.avg_KONV_KBETR;


UPDATE fact_purchase fpr
SET amt_conditionpricevalue = k1.sum_KONV_KWERT
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,tmp_konv_col_pf k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PR00'
AND amt_conditionpricevalue <> k1.sum_KONV_KWERT;

/*KONP columns */


DROP TABLE IF EXISTS TMP_col_KONP;
CREATE TABLE TMP_col_KONP
AS
SELECT documentconditionnumber KONV_KNUMV,conditionitemnumber KONV_KPOSN,conditiontype_konp KONV_KSCHL,conditionunit,AVG(conditionrate_konp) avg_baseprice,avg(conditionpriceunit) avg_conditionpriceunit
FROM dim_transactionpricecondition
WHERE conditiontype_konp IN ('PN00','PR00','ZCMX','ZTRS','PB00','PI01','ZFR1','ZFR2','ZFR3','ZFR4')
and sourceflag ='PURCHASE'
GROUP BY documentconditionnumber,conditionitemnumber,conditiontype_konp,conditionunit;


UPDATE fact_purchase fpr
SET fpr.amt_IntercompanyPrice  = k1.avg_baseprice
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PI01'
AND amt_IntercompanyPrice <> k1.avg_baseprice;



UPDATE fact_purchase fpr
SET fpr.amt_stdFrghtandInsurnceprice  = k1.avg_baseprice
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'ZFR1'
AND amt_stdFrghtandInsurnceprice <> k1.avg_baseprice;

UPDATE fact_purchase fpr
SET fpr.amt_stdQualityCntrlprice  = k1.avg_baseprice
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'ZFR2'
AND amt_stdQualityCntrlprice <> k1.avg_baseprice;


UPDATE fact_purchase fpr
SET fpr.amt_stdCustomDutiesprice  = k1.avg_baseprice
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'ZFR3'
AND fpr.amt_stdCustomDutiesprice <> k1.avg_baseprice;

UPDATE fact_purchase fpr
SET fpr.amt_stdOthrLandedCost  = k1.avg_baseprice
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'ZFR4'
AND fpr.amt_stdOthrLandedCost <> k1.avg_baseprice;

MERGE INTO FACT_PURCHASE fact
USING (SELECT DISTINCT fpr.fact_purchaseid,FIRST_VALUE(k1.conditionunit) OVER (PARTITION BY KONV_KNUMV,KONV_KPOSN,KONV_KSCHL ORDER BY KONV_KSCHL DESC) AS dd_conditionunitforgrossprice
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PB00'
AND fpr.dd_conditionunitforgrossprice <> k1.conditionunit) src
ON fact.fact_purchaseid = src.fact_purchaseid
WHEN MATCHED THEN UPDATE 
SET fact.dd_conditionunitforgrossprice = src.dd_conditionunitforgrossprice;


UPDATE fact_purchase fpr
SET fpr.dd_conditionunitforintercompanyprice  = k1.conditionunit
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PI01'
AND fpr.dd_conditionunitforintercompanyprice <> k1.conditionunit;


UPDATE fact_purchase fpr
SET fpr.dd_conditionunitforintercompanyprice  = k1.conditionunit
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PI01'
AND fpr.dd_conditionunitforintercompanyprice <> k1.conditionunit;
/*
UPDATE fact_purchase fpr
SET fpr.amt_conditiongrossprice  = k1.avg_conditionpriceunit
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PB00'
AND fpr.amt_conditiongrossprice <> k1.avg_conditionpriceunit */

MERGE INTO FACT_PURCHASE fact
USING (SELECT DISTINCT fpr.fact_purchaseid,FIRST_VALUE(k1.avg_conditionpriceunit) OVER (PARTITION BY KONV_KNUMV,KONV_KPOSN,KONV_KSCHL ORDER BY KONV_KSCHL DESC) AS amt_conditiongrossprice
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PB00'
AND fpr.amt_conditiongrossprice <> k1.avg_conditionpriceunit) src
ON fact.fact_purchaseid = src.fact_purchaseid
WHEN MATCHED THEN UPDATE 
SET fact.amt_conditiongrossprice = src.amt_conditiongrossprice;


UPDATE fact_purchase fpr
SET fpr.amt_conditionIntercompanyPrice  = k1.avg_conditionpriceunit
FROM ekko_ekpo_eket dm_di_ds,
dim_plant pl,
dim_company dc
 ,TMP_col_KONP k1
, fact_purchase fpr
WHERE pl.PlantCode = EKPO_WERKS
AND dc.companycode = EKPO_BUKRS
AND fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.KONV_KNUMV AND EKPO_EBELP = k1.KONV_KPOSN AND k1.KONV_KSCHL = 'PI01'
AND fpr.amt_conditionIntercompanyPrice <> k1.avg_conditionpriceunit;



/*Folowing tables are used to check if there are two currencies for the same combination of KONV_KNUMV,KONV_KPOSN*/
drop table if exists tmp_tmp_konh1;
create table tmp_tmp_konh1 as
select KONV_KNUMV,KONV_KPOSN,
GROUP_CONCAT (KONP_KONWA order by KONP_KONWA) as rateunit
from KONV_SALES_PURCHASE_tmp k, KONH_KONP p
where k.konv_knumh = p.konh_knumh
and k.konv_kopos = p.konp_kopos
GROUP BY  KONV_KNUMV,KONV_KPOSN
order by KONV_KNUMV,KONV_KPOSN;

drop table if exists tmp_for_upd_rateunit;
create table tmp_for_upd_rateunit as
select * from tmp_tmp_konh1
where left(rateunit,3) <> right(rateunit,3);

/*
merge into fact_purchase fpr
using (select distinct fpr.fact_purchaseid, k1.rateunit
 FROM ekko_ekpo_eket dm_di_ds
,dim_transactionpricecondition k1
, fact_purchase fpr
WHERE fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND EKKO_KNUMV = k1.documentconditionnumber AND EKPO_EBELP = k1.conditionitemnumber AND k1.conditiontype_konp = 'PB00'
AND k1.documentconditionnumber not in (select KONV_KNUMV from tmp_for_upd_rateunit)
AND k1.conditionitemnumber not in (select KONV_KPOSN from tmp_for_upd_rateunit)) t
on t.fact_purchaseid=fpr.fact_purchaseid
when matched then update set fpr.rateunit = t.rateunit
where fpr.rateunit <> t.rateunit
*/

merge into fact_purchase fpr
using (select distinct fpr.fact_purchaseid, MAX(k1.rateunit) as rateunit
 FROM ekko_ekpo_eket dm_di_ds
,dim_transactionpricecondition k1
, fact_purchase fpr
WHERE fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
AND EKKO_KNUMV = k1.documentconditionnumber AND EKPO_EBELP = k1.conditionitemnumber 
AND k1.conditiontype_konp = 'PB00'
AND k1.documentconditionnumber not in (select KONV_KNUMV from tmp_for_upd_rateunit)
AND k1.conditionitemnumber not in (select KONV_KPOSN from tmp_for_upd_rateunit)
GROUP BY fpr.fact_purchaseid) t
on t.fact_purchaseid=fpr.fact_purchaseid
when matched then update set fpr.rateunit = t.rateunit
where fpr.rateunit <> t.rateunit;



MERGE INTO fact_purchase fact 
USING (SELECT DISTINCT fpr.fact_purchaseid,'USD' AS rateunit
FROM ekko_ekpo_eket dm_di_ds
 ,dim_transactionpricecondition k1
, fact_purchase fpr
WHERE
fpr.dd_DocumentNo = EKPO_EBELN
AND fpr.dd_DocumentItemNo = EKPO_EBELP
AND fpr.dd_ScheduleNo = EKET_ETENR
  AND EKKO_KNUMV = k1.documentconditionnumber AND EKPO_EBELP = k1.conditionitemnumber AND k1.conditiontype_konp = 'PB00'
  AND k1.documentconditionnumber  in (select KONV_KNUMV  from tmp_for_upd_rateunit)
AND k1.conditionitemnumber in (select KONV_KPOSN from tmp_for_upd_rateunit)) src
ON fact.fact_purchaseid = src.fact_purchaseid
WHEN MATCHED THEN UPDATE 
SET fact.rateunit = src.rateunit;

/*START Alin Changes - 25/11/2016 -   */
merge into fact_purchase fp
using (
select distinct 
fpr.fact_purchaseid,
first_value(dt.dim_dateid) over (partition by fpr.fact_purchaseid order by fpr.fact_purchaseid) as dim_dateidmatlavail
from 
 EKBE_FPURCHASE ef, fact_purchase fpr,fact_salesorderdelivery f,dim_date dt
where fpr.dd_DocumentNo = f.dd_SalesDocNo
 AND fpr.dd_DocumentItemNo=f.dd_SalesItemNo
and EKBE_BELNR =f.dd_SalesDlvrDocNo 
and EKBE_VGABE = 8
and EKBE_BEWTP = 'L'
and f.dim_dateidmatlavail=dt.dim_dateid
)t
on fp.fact_purchaseid=t.fact_purchaseid
when matched then update
set  fp.dim_dateidmatavdate = t.dim_dateidmatlavail
WHERE   fp.dim_dateidmatavdate <> t.dim_dateidmatlavail;
/*END Alin Changes - 25/11/2016 -   */
