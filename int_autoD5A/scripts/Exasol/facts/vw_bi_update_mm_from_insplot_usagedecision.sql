
/* Pick up the max fil.dim_inspusagedecisionid if more than 1 row exists in fil for same doc-item-year */

DROP TABLE IF EXISTS tmp_upd_max_inspdecision_insplot;
CREATE TABLE tmp_upd_max_inspdecision_insplot AS
SELECT fil.dd_MaterialDocItemNo,fil.dd_MaterialDocNo,fil.dd_MaterialDocYear,max(fil.dim_inspusagedecisionid) dim_inspusagedecisionid
FROM fact_inspectionlot fil
GROUP BY fil.dd_MaterialDocItemNo,fil.dd_MaterialDocNo,fil.dd_MaterialDocYear;

UPDATE fact_materialmovement ms
SET ms.dim_inspusagedecisionid = ifnull(fil.dim_inspusagedecisionid,1)
FROM tmp_upd_max_inspdecision_insplot fil, fact_materialmovement ms
WHERE ms.dd_MaterialDocItemNo = fil.dd_MaterialDocItemNo
      AND ms.dd_MaterialDocNo = fil.dd_MaterialDocNo
      AND ms.dd_MaterialDocYear = fil.dd_MaterialDocYear
      AND ms.dim_inspusagedecisionid <> ifnull(fil.dim_inspusagedecisionid,1);

DROP TABLE IF EXISTS tmp_upd_max_inspdecision_insplot;

