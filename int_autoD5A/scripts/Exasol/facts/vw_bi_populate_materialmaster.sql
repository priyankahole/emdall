/* ################################################################################################################## */
/* */
/*   Author         : Octavian */
/*   Created On     : 08 Apr 2016 */
/*  */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */


/*   08 Apr 2016      Octavian	1.0  		  First draft 			 											  */
/* 	 20 May 2016      Madalina H              Add a new measure, amt_materialvalue - BI-2897                      */
/* 	 15 Jun 2016      Madalina H              Add a new quantity, Where used count in active BOM, ct_usedactiveBOM - BI-3179 */
/*	 11 Jul 2016         Liviu I                       Add past demand cts BI-3409 */
/*   02 May 2017       Yogini                 APP-6085                                                            */
/******************************************************************************************************************/


/*
drop table if exists tmp_fact_materialmaster
create table tmp_fact_materialmaster
LIKE fact_materialmaster INCLUDING DEFAULTS INCLUDING IDENTITY

delete from number_fountain m where m.table_name = 'tmp_fact_materialmaster'
   
insert into number_fountain
select 	'tmp_fact_materialmaster',
	ifnull(max(d.fact_materialmasterid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_materialmaster d
where d.fact_materialmasterid <> 1

insert into tmp_fact_materialmaster
(fact_materialmasterid,dim_materialmasterid,dim_plantmasterid)
select 
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'tmp_fact_materialmaster') + row_number() over(ORDER BY ''),
ifnull(dp.dim_partid,1) AS dim_materialmasterid,
ifnull(pl.dim_plantid,1) AS dim_plantmasterid
FROM dim_part dp LEFT OUTER JOIN dim_plant pl ON dp.plant = pl.plantcode
where dp.dim_partid <> 1

TRUNCATE TABLE fact_materialmaster
INSERT INTO fact_materialmaster
SELECT * FROM  tmp_fact_materialmaster

drop table if exists tmp_fact_materialmaster
*/

/* Yogini 2nd May 2017 APP-6085 avoid truncating fact */
drop table if exists tmp_fact_materialmaster;
create table tmp_fact_materialmaster
LIKE fact_materialmaster INCLUDING DEFAULTS INCLUDING IDENTITY;

/* Add entry in number_fountain table for fact_materialmaster */
delete from number_fountain m where m.table_name = 'fact_materialmaster';
   
insert into number_fountain
select 	'fact_materialmaster',
	ifnull(max(d.fact_materialmasterid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_materialmaster d
where d.fact_materialmasterid <> 1;

/* get all part-plant combination which is not present in fact_materialmaster */
insert into tmp_fact_materialmaster
(fact_materialmasterid,dim_materialmasterid,dim_plantmasterid)
select 
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_materialmaster') + row_number() over(ORDER BY ''),
ifnull(dp.dim_partid,1) AS dim_materialmasterid,
ifnull(pl.dim_plantid,1) AS dim_plantmasterid
FROM dim_part dp LEFT OUTER JOIN dim_plant pl ON dp.plant = pl.plantcode
where dp.dim_partid <> 1
and not exists (
	select 1 from fact_materialmaster f
	where f.dim_materialmasterid = dp.dim_partid
);

insert into fact_materialmaster
(fact_materialmasterid,dim_materialmasterid,dim_plantmasterid)
select fact_materialmasterid,dim_materialmasterid,dim_plantmasterid 
from tmp_fact_materialmaster;

drop table if exists tmp_fact_materialmaster;

/*Alin 17 Nov update missing the plantids*/
update fact_materialmaster f
set f.dim_plantmasterid = pl.dim_plantid
from fact_materialmaster f 
inner join dim_part dp on dp.dim_partid = f.dim_materialmasterid
LEFT OUTER JOIN dim_plant pl ON dp.plant = pl.plantcode
where f.dim_plantmasterid <> pl.dim_plantid;

/* END Yogini 2nd May 2017  APP-6085 avoid truncating fact */


/* Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016 . */
		
UPDATE fact_materialmaster am
SET am.std_exchangerate_dateid = dt.dim_dateid
FROM dim_plant p
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode AND dt.plantcode_factory = p.plantcode
		 INNER JOIN fact_materialmaster am ON am.Dim_PlantMasterId = p.dim_plantid

WHERE am.Dim_PlantMasterId = p.dim_plantid
AND   dt.datevalue = current_date
AND   am.std_exchangerate_dateid <> dt.dim_dateid;

/*END Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016 . */

/* Octavian: Adding fields */
update fact_materialmaster f_mm
SET f_mm.ct_proportionunit = (m.MARM_UMREN/m.MARM_UMREZ) * (t.T006_NENNR/t.T006_ZAEHL),
dw_update_date = CURRENT_TIMESTAMP
FROM dim_part dp, MARM m, T006 t,fact_materialmaster f_mm
WHERE f_mm.dim_materialmasterid = dp.dim_partid
AND dp.UnitOfMeasure = ifnull(MARM_MEINH, 'Not Set')
and dp.PartNumber = ifnull(MARM_MATNR, 'Not Set')
AND m.MARM_MSEHI = t.T006_MSEHI
AND (m.MARM_ATINN <> '' or m.MARM_ATINN <> 0)
AND f_mm.ct_proportionunit <> (m.MARM_UMREN/m.MARM_UMREZ) * (t.T006_NENNR/t.T006_ZAEHL);

/* Octavian: Adding fields */

/* Andrian: Adding fields Stock Days Supply*/
update fact_materialmaster a 
 set ct_stockdayssupply = d.mdkp_berw1
 from dim_part b,dim_plant c, (select mdkp_matnr,mdkp_plwrk,max(mdkp_berw1) as mdkp_berw1  from MDKP
                                    group by mdkp_matnr,mdkp_plwrk) d,fact_materialmaster a 
 where a.dim_materialmasterid = b.dim_partid
 and a.dim_plantmasterid = c.dim_plantid
 and b.partnumber = d.mdkp_matnr
 and c.plantcode = d.mdkp_plwrk
 and ct_stockdayssupply <> d.mdkp_berw1;
/* Andrian: Adding fields Stock Days Supply*/

/* Madalina Herghelegiu 20 May 2016 - add a new measure, amt_materialvalue - BI-2897 */
DROP TABLE IF EXISTS tmp_prices; 
CREATE TABLE tmp_prices AS 
	select  dim_partid, 
					CASE WHEN AVG(CAST(priceunit AS  DECIMAL (18,4))) = 0 THEN avg(0)
						 WHEN max(pricecontrol) = 'S' THEN AVG(CAST (standardprice_mbew AS  DECIMAL (18,4))) / AVG(CAST(priceunit AS  DECIMAL (18,4)))
						 WHEN max(pricecontrol) = 'V' THEN AVG(CAST (movingprice AS  DECIMAL (18,4))) / AVG(CAST(priceunit AS  DECIMAL (18,4)))
						 ELSE avg(0)
					END AS amt_materialvalue
	from dim_part
	group by dim_partid;

update fact_materialmaster fm
set fm.amt_materialvalue = tmpp.amt_materialvalue,
	dw_update_date = current_timestamp 
from tmp_prices tmpp,fact_materialmaster fm
where fm.dim_materialmasterid = tmpp.dim_partid
	  and fm.amt_materialvalue <> ifnull (tmpp.amt_materialvalue, 0);
	  
DROP TABLE IF EXISTS tmp_prices; 	  
/* END Madalina Herghelegiu 20 May 2016 */

/* Madalina 15 Jun 2016 - Add a new quantity, Where used count in active BOM, ct_usedactiveBOM - BI-3179 */
/*Georgiana 22 Dec 2016 adding dd_deletionindicator<>'X' condition according to BI-5087*/
drop table if exists tmp_bom_distinct;	
create table tmp_bom_distinct as 
select distinct b.dd_bomnumber dd_bomnumber, b.dd_alternative dd_alternative, b.dd_bomHeaderCounter dd_bomHeaderCounter, p.partnumber partnumber, p.plant plant
from fact_bom b, dim_part p
where p.dim_partid = b.dim_bomcomponentid
	and b.dd_activeItem = 'Active'
	and b.dd_activeHeader = 'Active'
	and dd_BomItemNo <> 'Not Set'
	and dd_deletionindicator<>'X';
	
drop table if exists tmp_bom_ct;	
create table tmp_bom_ct as
select partnumber, plant, count(1) ctdistinct
from tmp_bom_distinct
group by  partnumber, plant;
	
update fact_materialmaster mm
set ct_usedactiveBOM = ifnull(t.ctdistinct, 0)
from dim_part p, dim_plant pl, tmp_bom_ct t,fact_materialmaster mm
where mm.dim_materialmasterid = p.dim_partid
	and mm.dim_plantmasterid = pl.dim_plantid
	and t.partnumber = p.partnumber
	and t.plant = pl.plantcode
	and ct_usedactiveBOM <> ifnull(t.ctdistinct, 0);



/*Andrian based on BI-3177*/
drop table if exists tmp01_tcurr_alltypes;
create table tmp01_tcurr_alltypes as 
select tcurr_kurst,
       tcurr_fcurr,
       tcurr_tcurr,
       cast(left((99999999 - tcurr_gdatu),4) || '-' || left(right((99999999 - tcurr_gdatu),4),2) || '-' || right((99999999 - tcurr_gdatu),2) AS date)
       as tcurr_gdate,
       tcurr_ffact,
       tcurr_tfact,
       tcurr_ukurs from TCURR_ALLTYPES
where tcurr_kurst in ('M','P');

drop table if exists tmp02_tcurr_alltypes;
create table tmp02_tcurr_alltypes as
select tcurr_kurst,
       tcurr_fcurr,
       tcurr_tcurr,
       tcurr_gdate,
       tcurr_ukurs,
       row_number() over (partition by tcurr_kurst,tcurr_fcurr,tcurr_tcurr order by tcurr_gdate desc)
       as RN
         from tmp01_tcurr_alltypes
where tcurr_fcurr = 'USD' and tcurr_kurst in ('P');


update fact_materialmaster
set ct_monthlyaverageexchangerate = 0
where ct_monthlyaverageexchangerate <> 0;

update fact_materialmaster
set ct_profitplantexchangerate = 0
where ct_profitplantexchangerate <> 0;

  update fact_materialmaster a
set ct_profitplantexchangerate = -1*tcurr_ukurs
from dim_part b,tmp02_tcurr_alltypes c, fact_materialmaster a
where a.dim_materialmasterid = b.dim_partid
  and c.tcurr_tcurr = b.currency
  and c.tcurr_kurst = 'P'
  and c.RN = 1
  and ct_profitplantexchangerate <> -1*tcurr_ukurs; 


drop table if exists tmp03_tcurr_alltypes;
create table tmp03_tcurr_alltypes as
select a.*, row_number() over (partition by tcurr_kurst,tcurr_fcurr,tcurr_tcurr order by tcurr_gdate desc) as RN from
(select tcurr_kurst,
       tcurr_fcurr,
       tcurr_tcurr,
       tcurr_gdate,
       tcurr_ukurs
         from tmp01_tcurr_alltypes
where tcurr_fcurr = 'USD' and tcurr_kurst in ('M') and tcurr_tcurr = 'EUR'
) a;



update fact_materialmaster a

set ct_monthlyaverageexchangerate = -1*tcurr_ukurs
from dim_part b,tmp03_tcurr_alltypes c, fact_materialmaster a
where a.dim_materialmasterid = b.dim_partid
  and c.tcurr_tcurr = b.currency
  and c.tcurr_kurst = 'M'
  and c.RN = 1
  and b.currency = 'EUR'
  and ct_monthlyaverageexchangerate <> -1*tcurr_ukurs;


update  fact_materialmaster a
set ct_monthlyaverageexchangerate = 1,
    ct_profitplantexchangerate = 1
from dim_part b, fact_materialmaster a
where a.dim_materialmasterid = b.dim_partid
  and b.currency = 'USD';  
  
  
drop table if exists tmp04_tcurr_alltypes;
create table tmp04_tcurr_alltypes as
select b.* from
(select a.*, row_number() over (partition by tcurr_kurst,tcurr_fcurr,tcurr_tcurr order by tcurr_gdate desc) as RN from
(select tcurr_kurst,
       tcurr_fcurr,
       tcurr_tcurr,
       tcurr_gdate,
       tcurr_ukurs
         from tmp01_tcurr_alltypes
where tcurr_kurst in ('M') and tcurr_tcurr = 'EUR'
) a ) b
where b.rn = 1;  

drop table if exists tmp05_tcurr_alltypes;
create table tmp05_tcurr_alltypes as
select a.*,(select tcurr_ukurs from tmp04_tcurr_alltypes
where tcurr_fcurr = 'USD') as tcurr_ukurs_usd  from tmp04_tcurr_alltypes a
where tcurr_fcurr <> 'USD';

update fact_materialmaster a
set ct_monthlyaverageexchangerate = (1/tcurr_ukurs * tcurr_ukurs_usd)
from dim_part b,tmp05_tcurr_alltypes c, fact_materialmaster a
where a.dim_materialmasterid = b.dim_partid
  and c.tcurr_fcurr = b.currency
  and c.tcurr_kurst = 'M'
  and c.RN = 1
  and b.currency <> 'EUR'
  and ct_monthlyaverageexchangerate <> (1/tcurr_ukurs * tcurr_ukurs_usd);
  
drop table if exists tmp04_tcurr_alltypes;
create table tmp04_tcurr_alltypes as
select b.* from
(select a.*, row_number() over (partition by tcurr_kurst,tcurr_fcurr,tcurr_tcurr order by tcurr_gdate desc) as RN from
(select tcurr_kurst,
       tcurr_fcurr,
       tcurr_tcurr,
       tcurr_gdate,
       tcurr_ukurs
         from tmp01_tcurr_alltypes
where tcurr_kurst in ('P') and tcurr_tcurr = 'EUR'
) a ) b
where b.rn = 1;  

drop table if exists tmp05_tcurr_alltypes;
create table tmp05_tcurr_alltypes as
select a.*,(select tcurr_ukurs from tmp04_tcurr_alltypes
where tcurr_fcurr = 'USD') as tcurr_ukurs_usd  from tmp04_tcurr_alltypes a
where tcurr_fcurr <> 'USD';

update fact_materialmaster a
set ct_profitplantexchangerate = (1/tcurr_ukurs * tcurr_ukurs_usd)
from dim_part b,tmp05_tcurr_alltypes c, fact_materialmaster a
where a.dim_materialmasterid = b.dim_partid
  and c.tcurr_fcurr = b.currency
  and c.tcurr_kurst = 'P'
  and c.RN = 1
  and b.currency <> 'EUR'
  and ct_profitplantexchangerate <> (1/tcurr_ukurs * tcurr_ukurs_usd);  
  
drop table if exists tmp1_volume2017;
create table tmp1_volume2017 as
select resb_matnr,resb_werks,sum(resb_bdmng) as resb_bdmng from RESB
where resb_bdter >= '2019-01-01' and resb_bdter < '2020-01-01'
group by resb_matnr,resb_werks;

update fact_materialmaster a
set ct_requirementsquantity2017 = c.resb_bdmng
from dim_part b,tmp1_volume2017 c, fact_materialmaster a
where a.dim_materialmasterid = b.dim_partid
and b.partnumber = c.resb_matnr
and b.plant = c.resb_werks
and ct_requirementsquantity2017 <> c.resb_bdmng;  

drop table if exists upd_fact_materialmaster_indepreqqty;
create table upd_fact_materialmaster_indepreqqty as
select b.pbim_werks,b.pbim_matnr,sum(a.pbed_plnmg) as pbed_plnmg
 from pbed a, pbim b
where a.pbed_bdzei = b.pbim_bdzei
and year(a.PBED_PDATU)=year(current_date)+1
group by  b.pbim_werks,b.pbim_matnr;


update fact_materialmaster f
set ct_openorderdemandnextcalyear = (case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end) 
from upd_fact_materialmaster_indepreqqty u, dim_part pt,MARM m, fact_materialmaster f
where pt.dim_partid = f.dim_materialmasterid and pt.partnumber = u.pbim_matnr
and pt.plant = pbim_werks and m.marm_matnr = pt.partnumber
and marm_meinh = 'IRU'
and ifnull(ct_openorderdemandnextcalyear,-1) <> (case when marm_umrez=0 then pbed_plnmg else pbed_plnmg/marm_umrez end) ;

/* Begin Liviu Ionescu BI-3409 */
drop table if exists tmp_materialmasterdemand_upd;
create table tmp_materialmasterdemand_upd as
select  so.dim_partid
		,so.dim_plantid
		,sum(case when (dt.datevalue >= (current_date - INTERVAL '365' DAY (3))) then so.ct_ScheduleQtySalesUnit else 0 end) as ct_DemandLastYear
		,sum(case when (dt.datevalue >= (current_date - INTERVAL '92' DAY)) then so.ct_ScheduleQtySalesUnit else 0 end) as ct_DemandLastQuarter
		,sum(case when (dt.datevalue >= (current_date - INTERVAL '31' DAY)) then so.ct_ScheduleQtySalesUnit else 0 end) as ct_DemandLastMonth
	from fact_salesorder so
		inner join dim_part dp on dp.dim_partid = so.dim_partid
		inner join dim_plant pl on pl.dim_plantid = so.dim_plantid
		inner join dim_date dt on dt.dim_dateid = so.dim_orderduedate
		where so.dd_executionstatusschedule = 'Closed'
	group by so.dim_partid,so.dim_plantid;

update fact_materialmaster tmp
set tmp.ct_DemandLastYear = ifnull(upd.ct_DemandLastYear,0)
from tmp_materialmasterdemand_upd upd, fact_materialmaster tmp
where tmp.dim_materialmasterid = upd.dim_partid 
and tmp.dim_plantmasterid = upd.dim_plantid
and tmp.ct_DemandLastYear <> ifnull(upd.ct_DemandLastYear,0);

update fact_materialmaster tmp
set tmp.ct_DemandLastQuarter = ifnull(upd.ct_DemandLastQuarter,0)
from tmp_materialmasterdemand_upd upd, fact_materialmaster tmp
where tmp.dim_materialmasterid = upd.dim_partid 
and tmp.dim_plantmasterid = upd.dim_plantid
and tmp.ct_DemandLastQuarter <> ifnull(upd.ct_DemandLastQuarter,0);

update fact_materialmaster tmp
set tmp.ct_DemandLastMonth = ifnull(upd.ct_DemandLastMonth,0)
from tmp_materialmasterdemand_upd upd, fact_materialmaster tmp
where tmp.dim_materialmasterid = upd.dim_partid 
and tmp.dim_plantmasterid = upd.dim_plantid
and tmp.ct_DemandLastMonth <> ifnull(upd.ct_DemandLastMonth,0);

drop table if exists tmp_materialmasterdemand_upd;

/*START BI-5163 Alin*/
merge into fact_materialmaster f using
(
select  fact_materialmasterid, dd.dim_dateid
from dim_date dd, fact_materialmaster f_mmi, dim_plant p
where 
p.dim_plantid = Dim_PlantMasterId
and dd.datevalue = cast(case when f_mmi.ct_stockdayssupply = 999.9 then '9999-12-31' else current_date+cast(round(f_mmi.ct_stockdayssupply ,0) as integer) end as date)
and dd.plantcode_factory = p.plantcode
and dd.companycode = 'Not Set'
) t
on f.fact_materialmasterid = t.fact_materialmasterid
when matched then 
update set f.dim_dateidtestdt = t.dim_dateid
where f.dim_dateidtestdt <> t.dim_dateid;
/*END BI-5163 Alin*/

/* Octavian : Add FX Rate columns */
merge into fact_materialmaster f_mm
using (
select distinct fpr.dim_partid,
first_value(dcr.dim_currencyid) over (partition by fpr.dim_partid order by dt.datevalue desc)
as dim_pocurrencyid
from fact_purchase fpr 
	inner join dim_date dt on fpr.dim_dateiddelivery = dt.dim_dateid
	inner join dim_currency dcr on fpr.dim_pocurrencyid = dcr.dim_currencyid
) fpr on f_mm.dim_materialmasterid = fpr.dim_partid
when matched then update
set f_mm.dim_pocurrencyid = fpr.dim_pocurrencyid;

/* Liviu Ionescu - add ratio for currency units to exchangerate for APP-7205 */ 

/*
DROP TABLE IF EXISTS tcurr_fxrate
CREATE TABLE tcurr_fxrate as 
SELECT t.*, concat(substr((99999999 - tcurr_gdatu),0,4),'',substr((99999999 - tcurr_gdatu),5,2),'',substr((99999999 - tcurr_gdatu),7,2)) tcurr_gdatu_date, -1 / tcurr_ukurs exchangerate
FROM TCURR_ALLTYPES t
WHERE t.TCURR_KURST = 'M'
*/

DROP TABLE IF EXISTS tcurr_fxrate;
CREATE TABLE tcurr_fxrate as 
SELECT 
t.TCURR_KURST
,t.TCURR_FCURR
,t.TCURR_TCURR
,t.TCURR_GDATU
,t.TCURR_FFACT
,t.TCURR_TFACT
,convert(decimal (18, 6), t.TCURR_UKURS * ifnull((case when ifnull(f.TCURF_FFACT,0) = 0 then 1 else ifnull(f.TCURF_FFACT,1) end)/(case when ifnull(f.TCURF_TFACT,0) = 0 then 1 else ifnull(f.TCURF_TFACT,1) end) ,1)) TCURR_UKURS
, concat(substr((99999999 - tcurr_gdatu),0,4),'',substr((99999999 - tcurr_gdatu),5,2),'',substr((99999999 - tcurr_gdatu),7,2)) tcurr_gdatu_date, 
(-1 / tcurr_ukurs)*ifnull((case when ifnull(f.TCURF_FFACT,0) = 0 then 1 else ifnull(f.TCURF_FFACT,1) end)/(case when ifnull(f.TCURF_TFACT,0) = 0 then 1 else ifnull(f.TCURF_TFACT,1) end) ,1) exchangerate
FROM TCURR_ALLTYPES t
	left join (select distinct TCURF_KURST,TCURF_FCURR, TCURF_TCURR, TCURF_FFACT, TCURF_TFACT from TCURF_ALLTYPES) f 
						on t.TCURR_KURST = f.TCURF_KURST and
						t.TCURR_FCURR = f.TCURF_FCURR and
						t.TCURR_TCURR = f.TCURF_TCURR 
WHERE t.TCURR_KURST = 'M' ;

/* Column 1: 2018 Plant Currency FX Rate  
(Plant Currency  Global Object Currency (EUR) conversion rate)
*/

merge into fact_materialmaster m 
using (
select distinct fact_materialmasterid,first_value(case when tc.tcurr_ukurs is null then 1 when tc.tcurr_ukurs < 0 then -1 * tc.tcurr_ukurs else tc.tcurr_ukurs end) over (partition by m.dim_materialmasterid order by TCURR_GDATU_DATE desc) as exchangerate
from fact_materialmaster m
inner join dim_part dp on m.dim_materialmasterid = dp.dim_partid 
left join tcurr_fxrate tc on tc.TCURR_FCURR = dp.currency and tc.TCURR_TCURR = 'EUR' and tc.TCURR_GDATU_DATE  <= '20180901' /* select concat(year(current_date),lpad(month(current_date),2,0),'01') */
/* where dp.partnumber = '115401' */) t
on m.fact_materialmasterid = t.fact_materialmasterid
when matched then update
set m.ct_plantcurr2018_fxrate = t.exchangerate
where m.ct_plantcurr2018_fxrate <> t.exchangerate;


/* Column 2: 2017 Purchase Currency FX Rate  
(Plant Currency  Global Object Currency (EUR) conversion rate) 1st of August
*/

merge into fact_materialmaster m 
using (
select fact_materialmasterid,min(case when tc.tcurr_ukurs is null then 1 when tc.tcurr_ukurs < 0 then -1 * tc.tcurr_ukurs else tc.tcurr_ukurs end) as exchangerate
from fact_materialmaster m
inner join dim_part dp on m.dim_materialmasterid = dp.dim_partid 
left join tcurr_fxrate tc on tc.TCURR_FCURR = dp.currency and tc.TCURR_TCURR = 'EUR' and tc.TCURR_GDATU_DATE  = '20170801'
group by fact_materialmasterid
/* where dp.partnumber = '115401' */) t
on m.fact_materialmasterid = t.fact_materialmasterid
when matched then update
set m.ct_purchcurr2017_fxrate = t.exchangerate
where m.ct_purchcurr2017_fxrate <> t.exchangerate;


/* Column 3 =2018 Global Obj. to Purchase currency. 
In this column we fetch conversion factor of global object currency i.e. EUR to Purchase Currency i.e. CHF for our example.
*/

merge into fact_materialmaster m 
using (
select distinct fact_materialmasterid,first_value(1/(case when tc.tcurr_ukurs is null then 1 when tc.tcurr_ukurs < 0 then -1 * tc.tcurr_ukurs else tc.tcurr_ukurs end)) over (partition by m.dim_materialmasterid order by TCURR_GDATU_DATE desc) as exchangerate
from fact_materialmaster m
inner join dim_part dp on m.dim_materialmasterid = dp.dim_partid 
inner join dim_currency dc on m.dim_pocurrencyid = dc.dim_currencyid
left join tcurr_fxrate tc on tc.TCURR_FCURR = dc.currencycode and tc.TCURR_TCURR = 'EUR' and tc.TCURR_GDATU_DATE  <= '20180901' 
/* where dp.partnumber = '115401' */) t
on m.fact_materialmasterid = t.fact_materialmasterid
when matched then update
set m.ct_glbtopurchase2018_fxrate = t.exchangerate
where m.ct_glbtopurchase2018_fxrate <> t.exchangerate;


/* Column 4=2017 Global Obj. to Purchase currency. */

merge into fact_materialmaster m 
using (
select distinct fact_materialmasterid,1/(case when tc.tcurr_ukurs is null then 1 when tc.tcurr_ukurs < 0 then -1 * tc.tcurr_ukurs else tc.tcurr_ukurs end) as exchangerate
from fact_materialmaster m
inner join dim_part dp on m.dim_materialmasterid = dp.dim_partid 
inner join dim_currency dc on m.dim_pocurrencyid = dc.dim_currencyid
left join tcurr_fxrate tc on tc.TCURR_FCURR = dc.currencycode and tc.TCURR_TCURR = 'EUR'  and tc.TCURR_FCURR= 'USD' and tc.TCURR_GDATU_DATE  = '20170801'
/*where dp.partnumber = '115401' */) t
on m.fact_materialmasterid = t.fact_materialmasterid
when matched then update
set m.ct_glbtopurchase2017_fxrate = t.exchangerate
where m.ct_glbtopurchase2017_fxrate <> t.exchangerate;

 merge into fact_materialmaster f
using (select fact_materialmasterid,max(z.exchangeRate) as exchangeRate
from dim_part p, tmp_getexchangerate1 z,fact_materialmaster f
 WHERE   f.dim_materialmasterid = p.dim_partid
AND z.pFromCurrency  = p.currency
AND z.pToCurrency = 'USD' AND z.pDate  = '2017-01-01'
and z.fact_script_name = 'vw_bi_populate_materialmaster'
group by fact_materialmasterid) t
on t.fact_materialmasterid=f.fact_materialmasterid
when matched then update set f.amt_ExchangeRate_gbl =  t.exchangeRate;

/*08 Jun 2017 Georgiana Changes updating dim_currencyid based on Material Dimension*/

merge into fact_materialmaster f
using (select f.fact_materialmasterid,dc.dim_currencyid from fact_materialmaster f, dim_currency dc, dim_part dp
where f.DIM_MATERIALMASTERID = dp.dim_partid
and dc.currencycode=dp.currency
and f.dim_currencyid<>dc.dim_currencyid) t
on f.fact_materialmasterid=t.fact_materialmasterid
when matched then update set dim_currencyid = t.dim_currencyid;

/* 08 Jun 2017 End of Changes*/

/*Alin 2 june 2017 APP-6416*/
update fact_materialmaster f
set f.dd_sourcelist = dp.sourcelist
from fact_materialmaster f, dim_part dp, dim_plant dpl
where f.DIM_PLANTMASTERID = dpl.dim_plantid
and f.DIM_MATERIALMASTERID = dp.dim_partid
and dpl.plantcode = dp.plant
and f.dd_sourcelist <> dp.sourcelist;

/*Alin 22 sept APP-7578*/
update fact_materialmaster f
set dd_capacityscore = dp.capacity_score
from fact_materialmaster f, dim_part dp
where f.dim_materialmasterid = dp.dim_partid
and dd_capacityscore <> dp.capacity_score;

/*ALin 6 dec 2017 APP-8059*/
merge into fact_materialmaster f
using(
select distinct f.fact_materialmasterid, ifnull(MLAN_TAXM1, 'Not Set') as taxm1, ifnull(TSKMT_VTEXT, 'Not Set') as vtext
from 
TVKWZ_T001W_MLAN_v2 t, 
fact_materialmaster f, dim_part dp, dim_plant dpl, TSKMT tt
where f.DIM_PLANTMASTERID = dpl.dim_plantid
and f.DIM_MATERIALMASTERID = dp.dim_partid
and dpl.plantcode = dp.plant
and TSKMT_SPRAS = 'E'
and TSKMT_TATYP = 'MWST'
AND TSKMT_TAXKM = MLAN_TAXM1
and dp.partnumber = t.MLAN_MATNR
and dpl.plantcode = t.MARC_WERKS
) t
on f.fact_materialmasterid = t.fact_materialmasterid
when matched then update set
f.DD_TAX_CLASSIF_MATERIAL = t.taxm1,
f.DD_description_tskmt = t.vtext;
