drop table if exists BOMlevels_basis;
create table BOMlevels_basis as

select M.MAST_MATNR as MaterialNumber,
m.MAST_WERKS as PlantCode,
m.MAST_STLAL as AlternativeBOM,
m.MAST_STLNR as BOMNumber,
m.MAST_STLAN as BOMUsage,
SP.STPO_POSNR as Item,
SP.STPO_IDNRK as Component,
SP.STPO_POTX1 as ComponentDescription,
SP.STPO_STLTY as BOMCategory,
SP.STPO_STLKN as BOMItemNodeNumber,
SP.STPO_DATUV as ValidFromDate,
SP.STPO_MENGE as ComponentQuantity,
sp.STPO_STPOZ as BomItemCounter
from
STPO SP
inner JOIN STKO SK ON SP.STPO_STLTY = SK.STKO_STLTY AND SP.STPO_STLNR = SK.STKO_STLNR
inner JOIN STAS S ON S.STAS_STLNR = SK.STKO_STLNR AND S.STAS_STLTY = SP.STPO_STLTY
AND S.STAS_STLKN = SP.STPO_STLKN /*AND S.STAS_DATUV = SP.STPO_DATUV*/ AND
S.STAS_STLAL = SK.STKO_STLAL
inner JOIN MAST M  ON M.MAST_STLNR = SK.STKO_STLNR AND SK.STKO_STLAL = M.MAST_STLAL
WHERE SK.STKO_LKENZ is null AND SK.STKO_LOEKZ is null and S.STAS_LKENZ is null
AND SK.STKO_STLST in ('10','15','20','25','30');

DROP TABLE IF EXISTS BOMLevels_selectminalternative;
CREATE TABLE BOMLevels_selectminalternative
AS
SELECT DISTINCT mkal.mkal_matnr,mkal.mkal_werks,mkal.mkal_stlal
from MKAL,
(
SELECT mkal_matnr,mkal_werks,min(mkal_verid) mkal_verid
FROM mkal where current_date >= mkal_adatu AND current_date <= mkal_bdatu AND IFNULL(MKAL_MKSP,-1) <> 1
GROUP BY mkal_matnr,mkal_werks
) t,Marc_prodorder m
WHERE mkal.mkal_matnr = t.mkal_matnr AND mkal.mkal_werks = t.mkal_werks AND mkal.mkal_verid = t.mkal_verid 
AND current_date >= mkal.mkal_adatu AND current_date <= mkal.mkal_bdatu AND IFNULL(mkal.MKAL_MKSP,-1) <> 1
and m.marc_matnr=t.mkal_matnr and m.marc_werks=t.mkal_werks and marc_altsl=3;

drop table if exists BOMLevels_minalternative_neq3;
create table BOMLevels_minalternative_neq3 as
select distinct materialnumber,plantcode, min(alternativebom) as alternativebom 
from BOMlevels_basis,marc_prodorder
where marc_matnr=materialnumber and marc_werks=plantcode and marc_altsl <> 3
group by materialnumber,plantcode;

insert into BOMLevels_selectminalternative select * from BOMLevels_minalternative_neq3;

/*Create level 1*/
drop table if exists BOMLevels_1;
create table BOMLevels_1 as
select distinct t0.materialnumber as rootpartnumber,t0.alternativebom as rootalternativebom,t0.bomnumber as rootbomnumber,t0.*, convert(varchar(2),'1') as BomLevel 
from BOMlevels_basis t0/*,BOMLevels_selectminalternative
where t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks
AND ALTERNATIVEBOM = mkal_stlal*/;

/* Check the components that have active BOM's for Level 2*/
/* If a record for the material (component/Plant) exists in table MAST, it means it has a BOM*/


drop table if exists BOMLevels_2_mast_filter;
create table BOMLevels_2_mast_filter as
select distinct t0.* from BOMLevels_1 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 2 (for the above components)*/

drop table if exists BOMLevels_2;
create table BOMLevels_2 as
select distinct t2.rootpartnumber,t2.rootalternativebom,t2.rootbomnumber,t0.*, convert(varchar(2),'2') as BomLevel 
from BOMlevels_basis t0, BOMLevels_2_mast_filter t2, BOMLevels_selectminalternative 
where
  t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t2.component
and t0.plantcode=T2.plantcode
and t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 3*/
/* If a record for the material (component/Plant) exists in table MAST, it means it has a BOM*/

drop table if exists BOMLevels_mast_filter_3;
create table BOMLevels_mast_filter_3 as
select distinct t0.* from BOMLevels_2 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 3*/

drop table if exists BOMLevels_3;
create table BOMLevels_3 as
select distinct t3.rootpartnumber,t3.rootalternativebom,t3.rootbomnumber,t0.*, convert(varchar(2),'3') as BomLevel 
from BOMlevels_basis t0, BOMLevels_mast_filter_3 t3, BOMLevels_selectminalternative 
where
  t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t3.component
and t0.plantcode=T3.plantcode
and t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 4*/

drop table if exists BOMLevels_mast_filter_4;
create table BOMLevels_mast_filter_4 as
select distinct t0.* from BOMLevels_3 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 4*/

drop table if exists BOMLevels_4;
create table BOMLevels_4 as
select distinct t4.rootpartnumber,t4.rootalternativebom,t4.rootbomnumber ,t0.*, convert(varchar(2),'4') as BomLevel 
from BOMlevels_basis t0, BOMLevels_mast_filter_4 t4, BOMLevels_selectminalternative 
where
t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t4.component
and t0.plantcode=T4.plantcode
and t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 5*/
drop table if exists BOMLevels_mast_filter_5;
create table BOMLevels_mast_filter_5 as
select distinct t0.* from BOMLevels_4 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 5*/

drop table if exists BOMLevels_5;
create table BOMLevels_5 as
select distinct t5.rootpartnumber,t5.rootalternativebom,t5.rootbomnumber ,t0.*, convert(varchar(2),'5') as BomLevel
from BOMlevels_basis t0, BOMLevels_mast_filter_5 t5, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t5.component
and t0.plantcode=T5.plantcode
and t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 6*/
drop table if exists BOMLevels_mast_filter_6;
create table BOMLevels_mast_filter_6 as
select distinct t0.* from BOMLevels_5 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 6*/

drop table if exists BOMLevels_6;
create table BOMLevels_6 as
select distinct t6.rootpartnumber,t6.rootalternativebom,t6.rootbomnumber ,t0.*, convert(varchar(2),'6') as BomLevel 
from BOMlevels_basis t0, BOMLevels_mast_filter_6 t6, BOMLevels_selectminalternative 
where
t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t6.component
and t0.plantcode=T6.plantcode
and t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 7*/
drop table if exists BOMLevels_mast_filter_7;
create table BOMLevels_mast_filter_7 as
select distinct t0.* from BOMLevels_6 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 7*/

drop table if exists BOMLevels_7;
create table BOMLevels_7 as
select distinct t7.rootpartnumber,t7.rootalternativebom,t7.rootbomnumber ,t0.*, convert(varchar(2),'7') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_7 t7, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t7.component
and t0.plantcode=T7.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 8*/
drop table if exists BOMLevels_mast_filter_8;
create table BOMLevels_mast_filter_8 as
select distinct t0.* from BOMLevels_7 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 8*/

drop table if exists BOMLevels_8;
create table BOMLevels_8 as
select distinct t8.rootpartnumber,t8.rootalternativebom,t8.rootbomnumber ,t0.*, convert(varchar(2),'8') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_8 t8, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t8.component
and t0.plantcode=T8.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 9*/
drop table if exists BOMLevels_mast_filter_9;
create table BOMLevels_mast_filter_9 as
select distinct t0.* from BOMLevels_8 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 9*/

drop table if exists BOMLevels_9;
create table BOMLevels_9 as
select distinct t9.rootpartnumber,t9.rootalternativebom,t9.rootbomnumber ,t0.*, convert(varchar(2),'9') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_9 t9, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t9.component
and t0.plantcode=T9.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 10*/
drop table if exists BOMLevels_mast_filter_10;
create table BOMLevels_mast_filter_10 as
select distinct t0.* from BOMLevels_9 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 10*/

drop table if exists BOMLevels_10;
create table BOMLevels_10 as
select distinct t10.rootpartnumber,t10.rootalternativebom,t10.rootbomnumber ,t0.*, convert(varchar(2),'10') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_10 t10, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t10.component
and t0.plantcode=T10.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 11*/
drop table if exists BOMLevels_mast_filter_11;
create table BOMLevels_mast_filter_11 as
select distinct t0.* from BOMLevels_10 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 11*/

drop table if exists BOMLevels_11;
create table BOMLevels_11 as
select distinct t11.rootpartnumber,t11.rootalternativebom,t11.rootbomnumber ,t0.*, convert(varchar(2),'11') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_11 t11, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t11.component
and t0.plantcode=T11.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 12*/
drop table if exists BOMLevels_mast_filter_12;
create table BOMLevels_mast_filter_12 as
select distinct t0.* from BOMLevels_11 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 12*/

drop table if exists BOMLevels_12;
create table BOMLevels_12 as
select distinct t12.rootpartnumber,t12.rootalternativebom,t12.rootbomnumber ,t0.*, convert(varchar(2),'12') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_12 t12, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t12.component
and t0.plantcode=T12.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 13*/
drop table if exists BOMLevels_mast_filter_13;
create table BOMLevels_mast_filter_13 as
select distinct t0.* from BOMLevels_12 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 13*/

drop table if exists BOMLevels_13;
create table BOMLevels_13 as
select distinct t13.rootpartnumber,t13.rootalternativebom,t13.rootbomnumber ,t0.*, convert(varchar(2),'13') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_13 t13, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t13.component
and t0.plantcode=T13.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 14*/
drop table if exists BOMLevels_mast_filter_14;
create table BOMLevels_mast_filter_14 as
select distinct t0.* from BOMLevels_13 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 14*/

drop table if exists BOMLevels_14;
create table BOMLevels_14 as
select distinct t14.rootpartnumber,t14.rootalternativebom,t14.rootbomnumber ,t0.*, convert(varchar(2),'14') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_14 t14, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t14.component
and t0.plantcode=T14.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 15*/
drop table if exists BOMLevels_mast_filter_15;
create table BOMLevels_mast_filter_15 as
select distinct t0.* from BOMLevels_14 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 15*/

drop table if exists BOMLevels_15;
create table BOMLevels_15 as
select distinct t15.rootpartnumber,t15.rootalternativebom,t15.rootbomnumber ,t0.*, convert(varchar(2),'15') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_15 t15, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t15.component
and t0.plantcode=T15.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 16*/
drop table if exists BOMLevels_mast_filter_16;
create table BOMLevels_mast_filter_16 as
select distinct t0.* from BOMLevels_15 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 16*/

drop table if exists BOMLevels_16;
create table BOMLevels_16 as
select distinct t16.rootpartnumber,t16.rootalternativebom,t16.rootbomnumber ,t0.*, convert(varchar(2),'16') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_16 t16, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t16.component
and t0.plantcode=T16.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 17*/
drop table if exists BOMLevels_mast_filter_17;
create table BOMLevels_mast_filter_17 as
select distinct t0.* from BOMLevels_16 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 17*/

drop table if exists BOMLevels_17;
create table BOMLevels_17 as
select distinct t17.rootpartnumber,t17.rootalternativebom,t17.rootbomnumber ,t0.*, convert(varchar(2),'17') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_17 t17, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t17.component
and t0.plantcode=T17.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 18*/
drop table if exists BOMLevels_mast_filter_18;
create table BOMLevels_mast_filter_18 as
select distinct t0.* from BOMLevels_17 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 18*/

drop table if exists BOMLevels_18;
create table BOMLevels_18 as
select distinct t18.rootpartnumber,t18.rootalternativebom,t18.rootbomnumber ,t0.*, convert(varchar(2),'18') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_18 t18, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t18.component
and t0.plantcode=T18.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 19*/
drop table if exists BOMLevels_mast_filter_19;
create table BOMLevels_mast_filter_19 as
select distinct t0.* from BOMLevels_18 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 19*/

drop table if exists BOMLevels_19;
create table BOMLevels_19 as
select distinct t19.rootpartnumber,t19.rootalternativebom,t19.rootbomnumber ,t0.*, convert(varchar(2),'19') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_19 t19, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t19.component
and t0.plantcode=T19.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 20*/
drop table if exists BOMLevels_mast_filter_20;
create table BOMLevels_mast_filter_20 as
select distinct t0.* from BOMLevels_19 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 20*/

drop table if exists BOMLevels_20;
create table BOMLevels_20 as
select distinct t20.rootpartnumber,t20.rootalternativebom,t20.rootbomnumber ,t0.*, convert(varchar(2),'20') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_20 t20, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t20.component
and t0.plantcode=T20.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 21*/
drop table if exists BOMLevels_mast_filter_21;
create table BOMLevels_mast_filter_21 as
select distinct t0.* from BOMLevels_20 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 21*/

drop table if exists BOMLevels_21;
create table BOMLevels_21 as
select distinct t21.rootpartnumber,t21.rootalternativebom,t21.rootbomnumber ,t0.*, convert(varchar(2),'21') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_21 t21, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t21.component
and t0.plantcode=T21.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 22*/
drop table if exists BOMLevels_mast_filter_22;
create table BOMLevels_mast_filter_22 as
select distinct t0.* from BOMLevels_21 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 22*/

drop table if exists BOMLevels_22;
create table BOMLevels_22 as
select distinct t22.rootpartnumber,t22.rootalternativebom,t22.rootbomnumber ,t0.*, convert(varchar(2),'22') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_22 t22, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t22.component
and t0.plantcode=T22.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 23*/
drop table if exists BOMLevels_mast_filter_23;
create table BOMLevels_mast_filter_23 as
select distinct t0.* from BOMLevels_22 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 23*/

drop table if exists BOMLevels_23;
create table BOMLevels_23 as
select distinct t23.rootpartnumber,t23.rootalternativebom,t23.rootbomnumber ,t0.*, convert(varchar(2),'23') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_23 t23, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t23.component
and t0.plantcode=T23.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 24*/
drop table if exists BOMLevels_mast_filter_24;
create table BOMLevels_mast_filter_24 as
select distinct t0.* from BOMLevels_23 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 24*/

drop table if exists BOMLevels_24;
create table BOMLevels_24 as
select distinct t24.rootpartnumber,t24.rootalternativebom,t24.rootbomnumber ,t0.*, convert(varchar(2),'24') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_24 t24, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t24.component
and t0.plantcode=T24.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 25*/
drop table if exists BOMLevels_mast_filter_25;
create table BOMLevels_mast_filter_25 as
select distinct t0.* from BOMLevels_24 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 25*/

drop table if exists BOMLevels_25;
create table BOMLevels_25 as
select distinct t25.rootpartnumber,t25.rootalternativebom,t25.rootbomnumber ,t0.*, convert(varchar(2),'25') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_25 t25, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t25.component
and t0.plantcode=T25.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 26*/
drop table if exists BOMLevels_mast_filter_26;
create table BOMLevels_mast_filter_26 as
select distinct t0.* from BOMLevels_25 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 26*/

drop table if exists BOMLevels_26;
create table BOMLevels_26 as
select distinct t26.rootpartnumber,t26.rootalternativebom,t26.rootbomnumber ,t0.*, convert(varchar(2),'26') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_26 t26, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t26.component
and t0.plantcode=T26.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 27*/
drop table if exists BOMLevels_mast_filter_27;
create table BOMLevels_mast_filter_27 as
select distinct t0.* from BOMLevels_26 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 27*/

drop table if exists BOMLevels_27;
create table BOMLevels_27 as
select distinct t27.rootpartnumber,t27.rootalternativebom,t27.rootbomnumber ,t0.*, convert(varchar(2),'27') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_27 t27, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t27.component
and t0.plantcode=T27.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 28*/
drop table if exists BOMLevels_mast_filter_28;
create table BOMLevels_mast_filter_28 as
select distinct t0.* from BOMLevels_27 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 28*/

drop table if exists BOMLevels_28;
create table BOMLevels_28 as
select distinct t28.rootpartnumber,t28.rootalternativebom,t28.rootbomnumber ,t0.*, convert(varchar(2),'28') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_28 t28, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t28.component
and t0.plantcode=T28.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 29*/
drop table if exists BOMLevels_mast_filter_29;
create table BOMLevels_mast_filter_29 as
select distinct t0.* from BOMLevels_28 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 29*/

drop table if exists BOMLevels_29;
create table BOMLevels_29 as
select distinct t29.rootpartnumber,t29.rootalternativebom,t29.rootbomnumber ,t0.*, convert(varchar(2),'29') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_29 t29, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t29.component
and t0.plantcode=T29.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 30*/
drop table if exists BOMLevels_mast_filter_30;
create table BOMLevels_mast_filter_30 as
select distinct t0.* from BOMLevels_29 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 30*/

drop table if exists BOMLevels_30;
create table BOMLevels_30 as
select distinct t30.rootpartnumber,t30.rootalternativebom,t30.rootbomnumber ,t0.*, convert(varchar(2),'30') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_30 t30, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t30.component
and t0.plantcode=T30.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 31*/
drop table if exists BOMLevels_mast_filter_31;
create table BOMLevels_mast_filter_31 as
select distinct t0.* from BOMLevels_30 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 31*/

drop table if exists BOMLevels_31;
create table BOMLevels_31 as
select distinct t31.rootpartnumber,t31.rootalternativebom,t31.rootbomnumber ,t0.*, convert(varchar(2),'31') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_31 t31, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t31.component
and t0.plantcode=T31.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 32*/
drop table if exists BOMLevels_mast_filter_32;
create table BOMLevels_mast_filter_32 as
select distinct t0.* from BOMLevels_31 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 32*/

drop table if exists BOMLevels_32;
create table BOMLevels_32 as
select distinct t32.rootpartnumber,t32.rootalternativebom,t32.rootbomnumber ,t0.*, convert(varchar(2),'32') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_32 t32, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t32.component
and t0.plantcode=T32.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 33*/
drop table if exists BOMLevels_mast_filter_33;
create table BOMLevels_mast_filter_33 as
select distinct t0.* from BOMLevels_32 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 33*/

drop table if exists BOMLevels_33;
create table BOMLevels_33 as
select distinct t33.rootpartnumber,t33.rootalternativebom,t33.rootbomnumber ,t0.*, convert(varchar(2),'33') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_33 t33, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t33.component
and t0.plantcode=T33.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 34*/
drop table if exists BOMLevels_mast_filter_34;
create table BOMLevels_mast_filter_34 as
select distinct t0.* from BOMLevels_33 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 34*/

drop table if exists BOMLevels_34;
create table BOMLevels_34 as
select distinct t34.rootpartnumber,t34.rootalternativebom,t34.rootbomnumber ,t0.*, convert(varchar(2),'34') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_34 t34, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t34.component
and t0.plantcode=T34.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 35*/
drop table if exists BOMLevels_mast_filter_35;
create table BOMLevels_mast_filter_35 as
select distinct t0.* from BOMLevels_34 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 35*/

drop table if exists BOMLevels_35;
create table BOMLevels_35 as
select distinct t35.rootpartnumber,t35.rootalternativebom,t35.rootbomnumber ,t0.*, convert(varchar(2),'35') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_35 t35, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t35.component
and t0.plantcode=T35.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 36*/
drop table if exists BOMLevels_mast_filter_36;
create table BOMLevels_mast_filter_36 as
select distinct t0.* from BOMLevels_35 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 36*/

drop table if exists BOMLevels_36;
create table BOMLevels_36 as
select distinct t36.rootpartnumber,t36.rootalternativebom,t36.rootbomnumber ,t0.*, convert(varchar(2),'36') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_36 t36, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t36.component
and t0.plantcode=T36.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 37*/
drop table if exists BOMLevels_mast_filter_37;
create table BOMLevels_mast_filter_37 as
select distinct t0.* from BOMLevels_36 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 37*/

drop table if exists BOMLevels_37;
create table BOMLevels_37 as
select distinct t37.rootpartnumber,t37.rootalternativebom,t37.rootbomnumber ,t0.*, convert(varchar(2),'37') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_37 t37, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t37.component
and t0.plantcode=T37.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 38*/
drop table if exists BOMLevels_mast_filter_38;
create table BOMLevels_mast_filter_38 as
select distinct t0.* from BOMLevels_37 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 38*/

drop table if exists BOMLevels_38;
create table BOMLevels_38 as
select distinct t38.rootpartnumber,t38.rootalternativebom,t38.rootbomnumber ,t0.*, convert(varchar(2),'38') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_38 t38, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t38.component
and t0.plantcode=T38.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 39*/
drop table if exists BOMLevels_mast_filter_39;
create table BOMLevels_mast_filter_39 as
select distinct t0.* from BOMLevels_38 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 39*/

drop table if exists BOMLevels_39;
create table BOMLevels_39 as
select distinct t39.rootpartnumber,t39.rootalternativebom,t39.rootbomnumber ,t0.*, convert(varchar(2),'39') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_39 t39, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t39.component
and t0.plantcode=T39.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 40*/
drop table if exists BOMLevels_mast_filter_40;
create table BOMLevels_mast_filter_40 as
select distinct t0.* from BOMLevels_39 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 40*/

drop table if exists BOMLevels_40;
create table BOMLevels_40 as
select distinct t40.rootpartnumber,t40.rootalternativebom,t40.rootbomnumber ,t0.*, convert(varchar(2),'40') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_40 t40, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t40.component
and t0.plantcode=T40.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 41*/
drop table if exists BOMLevels_mast_filter_41;
create table BOMLevels_mast_filter_41 as
select distinct t0.* from BOMLevels_40 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 41*/

drop table if exists BOMLevels_41;
create table BOMLevels_41 as
select distinct t41.rootpartnumber,t41.rootalternativebom,t41.rootbomnumber ,t0.*, convert(varchar(2),'41') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_41 t41, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t41.component
and t0.plantcode=T41.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 42*/
drop table if exists BOMLevels_mast_filter_42;
create table BOMLevels_mast_filter_42 as
select distinct t0.* from BOMLevels_41 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 42*/

drop table if exists BOMLevels_42;
create table BOMLevels_42 as
select distinct t42.rootpartnumber,t42.rootalternativebom,t42.rootbomnumber ,t0.*, convert(varchar(2),'42') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_42 t42, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t42.component
and t0.plantcode=T42.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 43*/
drop table if exists BOMLevels_mast_filter_43;
create table BOMLevels_mast_filter_43 as
select distinct t0.* from BOMLevels_42 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 43*/

drop table if exists BOMLevels_43;
create table BOMLevels_43 as
select distinct t43.rootpartnumber,t43.rootalternativebom,t43.rootbomnumber ,t0.*, convert(varchar(2),'43') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_43 t43, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t43.component
and t0.plantcode=T43.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 44*/
drop table if exists BOMLevels_mast_filter_44;
create table BOMLevels_mast_filter_44 as
select distinct t0.* from BOMLevels_43 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 44*/

drop table if exists BOMLevels_44;
create table BOMLevels_44 as
select distinct t44.rootpartnumber,t44.rootalternativebom,t44.rootbomnumber ,t0.*, convert(varchar(2),'44') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_44 t44, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t44.component
and t0.plantcode=T44.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 45*/
drop table if exists BOMLevels_mast_filter_45;
create table BOMLevels_mast_filter_45 as
select distinct t0.* from BOMLevels_44 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 45*/

drop table if exists BOMLevels_45;
create table BOMLevels_45 as
select distinct t45.rootpartnumber,t45.rootalternativebom,t45.rootbomnumber ,t0.*, convert(varchar(2),'45') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_45 t45, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t45.component
and t0.plantcode=T45.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 46*/
drop table if exists BOMLevels_mast_filter_46;
create table BOMLevels_mast_filter_46 as
select distinct t0.* from BOMLevels_45 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 46*/

drop table if exists BOMLevels_46;
create table BOMLevels_46 as
select distinct t46.rootpartnumber,t46.rootalternativebom,t46.rootbomnumber ,t0.*, convert(varchar(2),'46') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_46 t46, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t46.component
and t0.plantcode=T46.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 47*/
drop table if exists BOMLevels_mast_filter_47;
create table BOMLevels_mast_filter_47 as
select distinct t0.* from BOMLevels_46 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 47*/

drop table if exists BOMLevels_47;
create table BOMLevels_47 as
select distinct t47.rootpartnumber,t47.rootalternativebom,t47.rootbomnumber ,t0.*, convert(varchar(2),'47') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_47 t47, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t47.component
and t0.plantcode=T47.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 48*/
drop table if exists BOMLevels_mast_filter_48;
create table BOMLevels_mast_filter_48 as
select distinct t0.* from BOMLevels_47 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 48*/

drop table if exists BOMLevels_48;
create table BOMLevels_48 as
select distinct t48.rootpartnumber,t48.rootalternativebom,t48.rootbomnumber ,t0.*, convert(varchar(2),'48') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_48 t48, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t48.component
and t0.plantcode=T48.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 49*/
drop table if exists BOMLevels_mast_filter_49;
create table BOMLevels_mast_filter_49 as
select distinct t0.* from BOMLevels_48 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 49*/

drop table if exists BOMLevels_49;
create table BOMLevels_49 as
select distinct t49.rootpartnumber,t49.rootalternativebom,t49.rootbomnumber ,t0.*, convert(varchar(2),'49') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_49 t49, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t49.component
and t0.plantcode=T49.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 50*/
drop table if exists BOMLevels_mast_filter_50;
create table BOMLevels_mast_filter_50 as
select distinct t0.* from BOMLevels_49 t0, mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null;

/*Create level 50*/

drop table if exists BOMLevels_50;
create table BOMLevels_50 as
select distinct t50.rootpartnumber,t50.rootalternativebom,t50.rootbomnumber ,t0.*, convert(varchar(2),'50') as BomLevel from BOMlevels_basis t0, BOMLevels_mast_filter_50 t50, BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t50.component
and t0.plantcode=T50.plantcode
and  t0.alternativebom=mkal_stlal;



drop table if exists BOMLevels;
create table BOMLevels as
select * from BOMLevels_1
union all
select * from BOMLevels_2
union all
select * from BOMLevels_3
union all
select * from BOMLevels_4
union all
select * from BOMLevels_5
union all
select * from BOMLevels_6
union all
select * from BOMLevels_7
union all
select * from BOMLevels_8
union all
select * from BOMLevels_9
union all
select * from BOMLevels_10
union all
select * from BOMLevels_11
union all
select * from BOMLevels_12
union all
select * from BOMLevels_13
union all
select * from BOMLevels_14
union all
select * from BOMLevels_15
union all
select * from BOMLevels_16
union all
select * from BOMLevels_17
union all
select * from BOMLevels_18
union all
select * from BOMLevels_19
union all
select * from BOMLevels_20
union all
select * from BOMLevels_21
union all
select * from BOMLevels_22
union all
select * from BOMLevels_23
union all
select * from BOMLevels_24
union all
select * from BOMLevels_25
union all
select * from BOMLevels_26
union all
select * from BOMLevels_27
union all
select * from BOMLevels_28
union all
select * from BOMLevels_29
union all
select * from BOMLevels_30
union all
select * from BOMLevels_31
union all
select * from BOMLevels_32
union all
select * from BOMLevels_33
union all
select * from BOMLevels_34
union all
select * from BOMLevels_35
union all
select * from BOMLevels_36
union all
select * from BOMLevels_37
union all
select * from BOMLevels_38
union all
select * from BOMLevels_39
union all
select * from BOMLevels_40
union all
select * from BOMLevels_41
union all
select * from BOMLevels_42
union all
select * from BOMLevels_43
union all
select * from BOMLevels_44
union all
select * from BOMLevels_45
union all
select * from BOMLevels_46
union all
select * from BOMLevels_47
union all
select * from BOMLevels_48
union all
select * from BOMLevels_49
union all
select * from BOMLevels_50;


DROP TABLE IF EXISTS tmp_pGlobalCurrency_bom;
CREATE TABLE tmp_pGlobalCurrency_bom ( pGlobalCurrency VARCHAR(3) NULL);

INSERT INTO tmp_pGlobalCurrency_bom VALUES ( 'USD' );

update tmp_pGlobalCurrency_bom
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

/* MODIFY fact_bom_tmp_populate TO TRUNCATED */

DROP TABLE IF EXISTS tmp_fact_bom_populate;
CREATE TABLE tmp_fact_bom_populate
LIKE fact_bom INCLUDING DEFAULTS INCLUDING IDENTITY;

alter table tmp_fact_bom_populate add column dd_plant varchar(7) default 'Not Set' not null enable;

delete from NUMBER_FOUNTAIN where table_name = 'tmp_fact_bom_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'tmp_fact_bom_populate',ifnull(max(fact_bomid),0)
FROM tmp_fact_bom_populate;

insert into tmp_fact_bom_populate(
dd_rootpart,
dd_partnumber,
dd_immediateparentpart,
dd_Alternative,
dd_alternative_orig,
dd_level,
dd_BomItemNo,
dd_BOMItemNodeNo,
dd_BomNumber,
dd_ComponentNumber,
Dim_BomCategoryId,
dd_plant,
dd_BomItemCounter,
dd_bomusage,
fact_bomid)
select 
rootpartnumber as dd_rootpart,
rootpartnumber as dd_partnumber,
materialnumber as dd_immediateparentpart,
rootalternativebom as dd_Alternative ,
alternativebom as dd_alternative_orig,
BomLevel as dd_level,
ifnull(item,'Not Set') as dd_BomItemNo,
BomItemNodeNumber as dd_BOMItemNodeNo,
rootbomnumber as dd_BomNumber,
Component as dd_ComponentNumber,
ifnull(bc.Dim_BomCategoryId, 1) as Dim_BomCategoryId,
ifnull(plantcode,'Not Set') as dd_plant,
BomItemCounter as dd_BomItemCounter,
BOMUsage as dd_bomusage,
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_bom_populate') + row_number() over (order by '') as fact_bomid

FROM
BOMLevels
  INNER JOIN dim_bomcategory bc
             ON bc.Category = 'M' AND bc.RowIsCurrent = 1;

merge into tmp_fact_bom_populate f
using (select distinct t.fact_bomid, s.STPO_FMENG,s.STPO_LKENZ,s.STPO_POTX1,s.STPO_SORTF, s.STPO_CSSTR,s.STPO_MENGE,
s.STPO_AUSCH, s.STPO_NLFZT, s.STPO_PREIS, s.STPO_PEINH,s.STPO_VGKNT,s.STPO_AENAM, s.STPO_ANNAM,STPO_STPOZ,STPO_AVOAU
from tmp_fact_bom_populate t,dim_bomcategory bc, STPO s
where
t.dim_bomcategoryid=bc.dim_bomcategoryid and
s.STPO_STLTY=bc.Category and
s.STPO_STLNR =t.dd_BomNumber and
s.STPO_STLKN = t.dd_BOMItemNodeNo and
s.STPO_STPOZ = dd_BomItemCounter) t1
on t1.fact_bomid=f.fact_bomid
when matched then update set
             dd_fixedqty=   ifnull(t1.STPO_FMENG, 'Not Set') ,
              dd_deletionindicator_stpo=  ifnull(t1.STPO_LKENZ, 'Not Set') ,
              dd_bomitemtext1=  ifnull(t1.STPO_POTX1, 'Not Set') ,
              dd_sortstring =  ifnull(t1.STPO_SORTF, 'Not Set'),
              ct_AvgMatPurityinPercent= ifnull(t1.STPO_CSSTR,0),
			  ct_ComponentQty = ifnull(t1.STPO_MENGE,0),
           ct_ComponentScrapinPercent = ifnull(t1.STPO_AUSCH,0) ,
          ct_LeadTimeOffset = ifnull(t1.STPO_NLFZT,0) ,
          amt_Price = ifnull(t1.STPO_PREIS,0) ,
          amt_PriceUnit= ifnull(t1.STPO_PEINH,1),
         dd_BOMPredecessorNodeNo = ifnull(t1.STPO_VGKNT, 0) ,
         dd_ChangedBy = ifnull(t1.STPO_AENAM, 'Not Set') ,
         dd_CreatedBy= ifnull(t1.STPO_ANNAM, 'Not Set'),
         ct_operationsscrap = ifnull(t1.STPO_AVOAU, 0); /*APP-10354 - adding Operations Scrap*/


merge into tmp_fact_bom_populate fa
USING ( SELECT
                t.fact_bomid, ifnull(uom.Dim_UnitOfMeasureid,1) AS Dim_ComponentUOMId
                FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                 LEFT JOIN dim_unitofmeasure uom ON uom.UOM = s.STPO_MEINS AND uom.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_ComponentUOMId = SRC.Dim_ComponentUOMId
WHERE FA.Dim_ComponentUOMId <> SRC.Dim_ComponentUOMId;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T1.fact_bomid, ifnull(pitem.Dim_Partid,1) AS Dim_BOMComponentId
                FROM tmp_fact_bom_populate T1       
                Inner JOIN dim_part pitem ON pitem.PartNumber = T1.dd_ComponentNumber AND pitem.Plant = t1.dd_plant
                     AND pitem.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_BOMComponentId = SRC.Dim_BOMComponentId
WHERE FA.Dim_BOMComponentId <> SRC.Dim_BOMComponentId;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T1.fact_bomid, ifnull(pitem.Dim_Partid,1) AS dim_rootpartid
                FROM tmp_fact_bom_populate T1
                                INNER JOIN dim_part pitem ON pitem.PartNumber = T1.dd_rootpart AND pitem.Plant = t1.dd_plant AND pitem.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.dim_rootpartid = SRC.dim_rootpartid
WHERE FA.dim_rootpartid <> SRC.dim_rootpartid;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T1.fact_bomid, ifnull(pitem.Dim_Partid,1) AS dim_partidimmediateparent
                FROM tmp_fact_bom_populate T1
                                INNER JOIN dim_part pitem ON pitem.PartNumber = T1.dd_immediateparentpart AND pitem.Plant = t1.dd_plant AND pitem.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.dim_partidimmediateparent = SRC.dim_partidimmediateparent
WHERE FA.dim_partidimmediateparent <> SRC.dim_partidimmediateparent;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(be.dim_dateid,1) AS Dim_ChangedOnDateid
               FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                    Inner join dim_date be ON be.DateValue = ifnull(s.STPO_AEDAT,'0001-01-01') AND 'Not Set' = be.CompanyCode and be.plantcode_factory = ifnull(S.STPO_PSWRK, 'Not Set') 
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_ChangedOnDateid = SRC.Dim_ChangedOnDateid
WHERE FA.Dim_ChangedOnDateid <> SRC.Dim_ChangedOnDateid;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(dr.dim_dateid,1) AS Dim_CreatedOnDateid
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_date dr ON dr.DateValue = ifnull(STPO_ANDAT,'0001-01-01') AND 'Not Set' = dr.CompanyCode and dr.plantcode_factory = ifnull(s.STPO_PSWRK, 'Not Set')
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_CreatedOnDateid = SRC.Dim_CreatedOnDateid
WHERE FA.Dim_CreatedOnDateid <> SRC.Dim_CreatedOnDateid;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(cur.Dim_Currencyid,1) AS Dim_Currencyid_TRA
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                  inner join dim_currency cur ON cur.CurrencyCode = ifnull(s.STPO_WAERS,'Not Set')
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid_TRA = SRC.Dim_Currencyid_TRA
WHERE FA.Dim_Currencyid_TRA <> SRC.Dim_Currencyid_TRA;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(dp.Dim_PlantId,1) AS Dim_IssuingPlantId
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_plant dp ON dp.PlantCode = ifnull(s.STPO_PSWRK,'Not Set') AND dp.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_IssuingPlantId = SRC.Dim_IssuingPlantId
WHERE FA.Dim_IssuingPlantId <> SRC.Dim_IssuingPlantId;


MERGE INTO tmp_fact_bom_populate  FA
USING ( SELECT
                T.fact_bomid, ifnull(dp.Dim_PlantId,1) AS Dim_MaterialPlantId
                FROM tmp_fact_bom_populate  T
                                inner join dim_plant dp ON dp.PlantCode = t.dd_plant AND dp.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_MaterialPlantId = SRC.Dim_MaterialPlantId
WHERE FA.Dim_MaterialPlantId <> SRC.Dim_MaterialPlantId;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(porg.Dim_PurchaseOrgId,1) AS Dim_PurchasingOrgId
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join Dim_PurchaseOrg porg ON porg.PurchaseOrgCode = ifnull(s.STPO_EKORG,'Not Set') AND porg.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_PurchasingOrgId = SRC.Dim_PurchasingOrgId
WHERE FA.Dim_PurchasingOrgId <> SRC.Dim_PurchasingOrgId;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(sl.Dim_StorageLocationid,1) AS Dim_StorageLocationid
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_storagelocation sl ON sl.LocationCode = ifnull(s.STPO_LGORT,'Not Set') AND sl.Plant = t.dd_plant AND sl.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_StorageLocationid = SRC.Dim_StorageLocationid
WHERE FA.Dim_StorageLocationid <> SRC.Dim_StorageLocationid;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(pg.Dim_PurchaseGroupId,1) AS Dim_PurchasingGroupId
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join Dim_PurchaseGroup pg ON pg.PurchaseGroup = ifnull(s.STPO_EKGRP,'Not Set') AND pg.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_PurchasingGroupId = SRC.Dim_PurchasingGroupId
WHERE FA.Dim_PurchasingGroupId <> SRC.Dim_PurchasingGroupId;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(mg.Dim_MaterialGroupid,1) AS Dim_MaterialGroupid
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join Dim_MaterialGroup mg ON mg.MaterialGroupCode = s.STPO_MATKL AND mg.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_MaterialGroupid = SRC.Dim_MaterialGroupid
WHERE FA.Dim_MaterialGroupid <> SRC.Dim_MaterialGroupid;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(ls.dim_dateid,1) AS Dim_ValidFromDateid
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_date ls ON ls.DateValue = ifnull(s.STPO_DATUV,'0001-01-01') AND 'Not Set' = ls.CompanyCode and ls.plantcode_factory = ifnull(S.STPO_PSWRK, 'Not Set')
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_ValidFromDateid = SRC.Dim_ValidFromDateid
WHERE FA.Dim_ValidFromDateid <> SRC.Dim_ValidFromDateid;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(bic.Dim_BomItemCategoryId,1) AS Dim_BomItemCategoryId
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_bomitemcategory bic ON bic.ItemCategory = s.STPO_POSTP AND bic.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_BomItemCategoryId = SRC.Dim_BomItemCategoryId
WHERE FA.Dim_BomItemCategoryId <> SRC.Dim_BomItemCategoryId;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(dv.Dim_VendorId,1) AS Dim_VendorId
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_Vendor dv ON dv.VendorNumber = s.STPO_LIFNR AND dv.RowIsCurrent = 1
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_VendorId = SRC.Dim_VendorId
WHERE FA.Dim_VendorId <> SRC.Dim_VendorId;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, ifnull(cur.Dim_Currencyid,1) AS Dim_Currencyid_GBL
                FROM tmp_fact_bom_populate T
                                ,dim_currency cur, tmp_pGlobalCurrency_bom
                                WHERE  cur.CurrencyCode = pGlobalCurrency
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid_GBL = SRC.Dim_Currencyid_GBL
WHERE FA.Dim_Currencyid_GBL <> SRC.Dim_Currencyid_GBL;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, z.exchangeRate AS amt_ExchangeRate_GBL
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                INNER JOIN tmp_getExchangeRate1 z ON ifnull(z.pFromCurrency,'x') = ifnull(s.STPO_WAERS,'x') AND z.fact_script_name = 'bi_populate_billofmaterials_fact' AND z.pDate = CURRENT_DATE
                                INNER JOIN tmp_pGlobalCurrency_bom ON z.pToCurrency = pGlobalCurrency
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.amt_ExchangeRate_GBL = SRC.amt_ExchangeRate_GBL
WHERE FA.amt_ExchangeRate_GBL <> SRC.amt_ExchangeRate_GBL;


MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, cur.Dim_Currencyid AS Dim_Currencyid
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                 inner join dim_plant dp on dp.PlantCode = s.STPO_PSWRK AND dp.RowIsCurrent = 1
                 inner join  dim_company c on dp.companycode = c.companycode
                inner join dim_currency cur on c.currency = cur.CurrencyCode

) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid = SRC.Dim_Currencyid
WHERE FA.Dim_Currencyid <> SRC.Dim_Currencyid;

MERGE INTO tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomid, z.exchangeRate AS amt_ExchangeRate
                  FROM tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                  inner join tmp_getExchangeRate1 z on z.pFromCurrency  = STPO_WAERS and z.fact_script_name = 'bi_populate_billofmaterials_fact'
				  inner join dim_plant dp on dp.PlantCode = STPO_PSWRK AND dp.RowIsCurrent = 1
                  inner join  dim_company c on dp.companycode = c.companycode and z.pToCurrency = c.currency AND z.pDate = s.STPO_AEDAT
) SRC
ON FA.fact_bomid = SRC.fact_bomid
WHEN MATCHED THEN UPDATE SET FA.amt_ExchangeRate = SRC.amt_ExchangeRate
WHERE FA.amt_ExchangeRate <> SRC.amt_ExchangeRate;




update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_bomid),0) from tmp_fact_bom_populate)
where table_name = 'tmp_fact_bom_populate';


UPDATE tmp_fact_bom_populate b
   SET dim_parentpartprodhierarchyid = dim_producthierarchyid
from dim_producthierarchy dph, dim_part dpr, tmp_fact_bom_populate b
WHERE dph.ProductHierarchy = dpr.ProductHierarchy
       AND dpr.Dim_Partid = b.Dim_partId
       AND dim_parentpartprodhierarchyid <> dim_producthierarchyid;

UPDATE tmp_fact_bom_populate
   SET dim_parentpartprodhierarchyid = 1
 WHERE dim_parentpartprodhierarchyid IS NULL;
 
 
DROP TABLE IF EXISTS tmp_distinct_stko_stlst;
CREATE TABLE tmp_distinct_stko_stlst
AS
SELECT DISTINCT STKO_STLNR,STKO_STLST, STKO_STLAL,STKO_ANDAT,STKO_ANNAM,STKO_DATUV,STKO_LKENZ,STKO_STKTX, STKO_WRKAN
FROM STKO
WHERE (RTRIM(STKO_LKENZ) = '' OR STKO_LKENZ IS NULL)
AND STKO_STLTY = 'M' and STKO_STLST in ('10','15','20','25','30');

UPDATE tmp_fact_bom_populate b
SET b.Dim_bomstatusid = bs.Dim_bomstatusid
from tmp_distinct_stko_stlst stko, dim_bomstatus bs, tmp_fact_bom_populate b
WHERE b.dd_BomNumber = stko.stko_stlnr
AND b.dd_alternative_orig = stko.stko_stlal
AND bs.BOMStatusCode = ifnull(stko.STKO_STLST, 'Not Set')
AND b.Dim_bomstatusid <> bs.Dim_bomstatusid;

UPDATE tmp_fact_bom_populate b
SET b.dim_creaedondateid_stko = dt.dim_dateid
from tmp_distinct_stko_stlst stko, dim_date dt, tmp_fact_bom_populate b
WHERE b.dd_BomNumber = stko.stko_stlnr
AND b.dd_alternative_orig = stko.stko_stlal
AND dt.datevalue= stko.STKO_ANDAT
AND 'Not Set' = dt.CompanyCode
and dt.plantcode_factory = ifnull(stko.STKO_WRKAN, 'Not Set')
AND b.dim_creaedondateid_stko <> dt.dim_dateid;

UPDATE tmp_fact_bom_populate b
SET b.dd_creationuser = ifnull(stko.STKO_ANNAM, 'Not Set')
from tmp_distinct_stko_stlst stko, tmp_fact_bom_populate b
WHERE b.dd_BomNumber = stko.stko_stlnr
AND b.dd_alternative_orig = stko.stko_stlal
AND b.dd_creationuser <> ifnull(stko.STKO_ANNAM, 'Not Set');

UPDATE tmp_fact_bom_populate b
SET b.dim_dateidvalidfrom = dt.dim_dateid
from tmp_distinct_stko_stlst stko, dim_date dt, tmp_fact_bom_populate b
WHERE b.dd_BomNumber = stko.stko_stlnr
AND b.dd_alternative_orig = stko.stko_stlal
AND dt.datevalue= stko.STKO_DATUV
AND 'Not Set' = dt.CompanyCode
and dt.plantcode_factory = ifnull(stko.STKO_WRKAN, 'Not Set')
AND b.dim_dateidvalidfrom <> dt.dim_dateid;

UPDATE tmp_fact_bom_populate b
SET b.dd_deletionindicator_stko = ifnull(stko.STKO_LKENZ, 'Not Set')
from tmp_distinct_stko_stlst stko, tmp_fact_bom_populate b
WHERE b.dd_BomNumber = stko.stko_stlnr
AND b.dd_alternative_orig = stko.stko_stlal
AND b.dd_deletionindicator_stko <> ifnull(stko.STKO_LKENZ, 'Not Set');

UPDATE tmp_fact_bom_populate b
SET b.dd_alternativebomtext = ifnull(stko.STKO_STKTX, 'Not Set')
from tmp_distinct_stko_stlst stko, tmp_fact_bom_populate b
WHERE b.dd_BomNumber = stko.stko_stlnr
AND b.dd_alternative_orig = stko.stko_stlal
AND b.dd_alternativebomtext <> ifnull(stko.STKO_STKTX, 'Not Set');

UPDATE tmp_fact_bom_populate b
SET b.dd_indicator_alt = ifnull(s.STZU_ALTST,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM STZU s, tmp_fact_bom_populate b
WHERE b.dd_BomNumber = s.STZU_STLNR
AND s.STZU_STLTY = 'M'
AND b.dd_indicator_alt <> ifnull(s.STZU_ALTST,'Not Set');

UPDATE tmp_fact_bom_populate b
SET b.dd_deletionindicator = ifnull(STKO_LOEKZ,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM STKO, tmp_fact_bom_populate b
WHERE (RTRIM(STKO_LKENZ) = '' OR STKO_LKENZ IS NULL)
and b.dd_BomNumber = stko.stko_stlnr
AND b.dd_Alternative = stko.stko_stlal
AND b.dd_deletionindicator <> ifnull(STKO_LOEKZ,'Not Set');


	/* Liviu Ionescu Fix missing IDs: BI-3558 */
		UPDATE tmp_fact_bom_populate bom
			SET Dim_PartId = 1
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc, tmp_fact_bom_populate bom			
WHERE m.MAST_STLNR = bom.dd_BomNumber
			AND m.MAST_STLAL = bom.dd_Alternative
			AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
			AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
			AND bc.Category = 'M'
			AND bom.Dim_PartId <> 1;


			UPDATE tmp_fact_bom_populate bom
			SET Dim_PartId = pt.Dim_PartId
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc, tmp_fact_bom_populate bom
			 ,dim_part pt			
WHERE m.MAST_STLNR = bom.dd_BomNumber
			AND m.MAST_STLAL = bom.dd_Alternative
			AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
			AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
			AND bc.Category = 'M'
			 AND pt.PartNumber = m.MAST_MATNR AND pt.Plant = m.MAST_WERKS AND pt.RowIsCurrent = 1
			AND bom.Dim_PartId <> pt.Dim_PartId;

			/* Update Dim_PartId */
			UPDATE    tmp_fact_bom_populate bom 
			SET bom.Dim_PartId  =  1
FROM PRST p,dim_bomusage bu,dim_bomcategory bc, tmp_fact_bom_populate bom 
			WHERE p.PRST_STLNR = bom.dd_BomNumber
			AND p.PRST_STLAL = bom.dd_Alternative
			AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
			AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
			AND bc.Category = 'P';	

			UPDATE    tmp_fact_bom_populate bom 
			SET bom.Dim_PartId  = pt.Dim_PartId
FROM PRST p,dim_bomusage bu,dim_bomcategory bc
			,dim_part pt, tmp_fact_bom_populate bom 			
			WHERE p.PRST_STLNR = bom.dd_BomNumber
			AND p.PRST_STLAL = bom.dd_Alternative
			AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
			AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
			AND bc.Category = 'P'
			AND pt.PartNumber = p.PRST_MATNR
			AND pt.Plant = p.PRST_WERKS
			AND pt.RowIsCurrent = 1
			AND bom.Dim_PartId  <> pt.Dim_PartId;

			UPDATE tmp_fact_bom_populate bom
			SET bom.Dim_BomUsageId = bu.Dim_bomusageid
FROM  MAST m, dim_bomusage bu, dim_bomcategory bc, tmp_fact_bom_populate bom
			WHERE m.MAST_STLNR = bom.dd_BomNumber
			 AND m.MAST_STLAL = bom.dd_Alternative
			 AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
			 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
			 AND bc.Category = 'M'
			AND bom.Dim_BomUsageId <> bu.Dim_bomusageid;

			UPDATE    tmp_fact_bom_populate bom
			SET bom.Dim_BomUsageId = bu.Dim_bomusageid
FROM PRST p,dim_bomusage bu,dim_bomcategory bc, tmp_fact_bom_populate bom
			WHERE p.PRST_STLNR = bom.dd_BomNumber
			 AND p.PRST_STLAL = bom.dd_Alternative
			 AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
			 AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
			 AND bc.Category = 'P'
			AND bom.Dim_BomUsageId <> bu.Dim_bomusageid;
/*End LI:BI-3558 */

  UPDATE tmp_fact_bom_populate
    SET std_exchangerate_dateid = Dim_ChangedOnDateid
        ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE std_exchangerate_dateid <> Dim_ChangedOnDateid;
  
alter table  tmp_fact_bom_populate drop column dd_plant;

delete from fact_bom;
insert into fact_bom 
select * from tmp_fact_bom_populate;

update fact_bom b
set b.dd_activeItem =
        case when d.datevalue <= current_date and p.deletionflag <> 'X' then 'Active'
                else 'Not Active'
        end
from dim_part p, dim_date d, fact_bom b
where  p.dim_partid = b.dim_bomcomponentid
           and b.dim_validfromdateid = d.dim_dateid
           and  b.dd_activeItem <>
                                                case when d.datevalue <= current_date and p.deletionflag <> 'X' then 'Active'
                                                        else 'Not Active'
                                                end;

update fact_bom b
set dd_activeHeader =
                case when dt.datevalue <= current_date and p.deletionflag <> 'X' and dp.deletionflag <> 'X' then 'Active'
                        else 'Not Active'
                end
from dim_date dt, dim_part dp, dim_part p, fact_bom b
where b.dim_dateidvalidfrom = dt.dim_dateid
        and dp.dim_partid = b.dim_rootpartid
        and  p.dim_partid = b.dim_bomcomponentid
        and dd_activeHeader <>
                case when dt.datevalue <= current_date and p.deletionflag <> 'X' and dp.deletionflag <> 'X' then 'Active'
                        else 'Not Active'
                end;
/* End 15 Jun 2016 */
/* Madalina 23 Jun 2016 - add BOM Header Counter and update Base Quantity - BI-3178 */
update fact_bom b
set b.dd_bomHeaderCounter = ifnull(STKO_STKOZ, 0)
from STKO s, dim_bomcategory c, fact_bom b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STKO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STKO_STLNR, 'Not Set')
        and b.dd_alternative = ifnull(STKO_STLAL, 'Not Set')
        and b.dd_bomHeaderCounter <> ifnull(STKO_STKOZ, 0);

update fact_bom b
set b.ct_BaseQty = ifnull(s.STKO_BMENG, 0)
from STKO s, dim_bomcategory c, fact_bom b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STKO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STKO_STLNR, 'Not Set')
        and b.dd_alternative = ifnull(STKO_STLAL, 'Not Set')
        and b.dd_bomHeaderCounter = ifnull(STKO_STKOZ, 0)
        and b.ct_BaseQty <> ifnull(s.STKO_BMENG, 0);

/* End 23 Jun 2016 */


/* Andrei Merck AH - Bill of Material (BOM): Fields UOM and UOM Code not set */

UPDATE fact_bom f
set f.dim_baseuomid = d.dim_unitofmeasureid
from fact_bom f, dim_unitofmeasure d, STKO s, dim_bomcategory b
where   f.dim_bomcategoryid = b.dim_bomcategoryid
        and b.category = ifnull(s.STKO_STLTY, 'Not Set')
        and f.dd_bomnumber = ifnull(s.STKO_STLNR, 'Not Set')
        and f.dd_alternative = ifnull(s.STKO_STLAL, 'Not Set')
        and f.dd_bomHeaderCounter = ifnull(s.STKO_STKOZ, 0)
        and d.uom = ifnull(s.STKO_BMEIN, 'Not Set')
and  f.dim_baseuomid <> d.dim_unitofmeasureid;

/* Andrei Merck AH - Bill of Material (BOM): Fields UOM and UOM Code not set */

/* Ana Rusu Start - APP-8638 - Doc description new field */

merge into fact_bom f
using (select distinct t.fact_bomid, s.stpo_guidx
from fact_bom t,dim_bomcategory bc, STPO s, drad_drat dr
where
t.dim_bomcategoryid=bc.dim_bomcategoryid and
s.STPO_STLTY=bc.Category and
s.STPO_STLNR =t.dd_BomNumber and
s.STPO_STLKN = t.dd_BOMItemNodeNo and
s.STPO_STPOZ = dd_BomItemCounter and
s.stpo_guidx = dr.drad_objky and 
s.stpo_sortf in ('IM1', 'IM2')) t1
on t1.fact_bomid=f.fact_bomid
when matched then update set  f.dd_objectkey = ifnull(t1.stpo_guidx, 'Not Set')
;

/* End - APP-8638 - Doc description new field */


DROP TABLE IF EXISTS tmp_FactBOMValidToDate;

/*UPDATE fact_bom f
FROM fact_bom fp
SET f.dd_Alternative = fp.dd_Alternative
WHERE f.dd_rootpart = fp.dd_ComponentNumber
AND f.dd_level > 0
AND fp.dd_level = 0
AND f.dd_Alternative <> fp.dd_Alternative*/

/* These 2 procs are called after this */
/* CALL bi_populate_bom_level() */
/* CALL bi_process_bom_fact() */

DROP TABLE IF EXISTS tmp_pGlobalCurrency_bom;
DROP TABLE IF EXISTS TMP1_STKO;
DROP TABLE IF EXISTS TMP2_STKO;
DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_1;
DROP TABLE IF EXISTS TMP_DEL_fact_bom_tmp_populate_2;

/*Madalina - 4 Oct 2018- Adding single and duplicated view of the routes - APP-10354*/
/*APP-10354 - Adding BOM Factor*/
/*Madalina  - APP-11048 and APP-10928 - changes for duplicate view and filtering*/
update fact_bom 
set ct_BomFactor = 
	case when db.Net = '' 
		then f_bom.ct_ComponentQty/ f_bom.ct_BaseQty * (1 + rp.assemblyscrap ) * (1 + ct_ComponentScrapinPercent)
		else f_bom.ct_ComponentQty/ f_bom.ct_BaseQty * (1 + ct_operationsscrap ) * (1 + ct_ComponentScrapinPercent)
	end
from fact_bom AS f_bom 
inner join Dim_Part rp ON f_bom.dim_partidimmediateparent = rp.Dim_Partid   
inner join Dim_Part  bomc ON f_bom.Dim_BOMComponentId = bomc.Dim_Partid
inner join dim_billofmaterialmisc db on db.dim_billofmaterialmiscid = f_bom.dim_billofmaterialmiscid;

/* BOM rows that can enter in a hierarchy*/
drop table if exists tmp_bom_MTP;
create table tmp_bom_MTP as
select distinct  rp.PartNumber parent_part,rp.Plant parent_plant,rp.ItemSubType_Merck parent_itemSubType,
bomc.PartNumber component_part, bomc.Plant component_plant,bomc.ItemSubType_Merck component_itemSubType,
/* f_bom.dd_Level dd_level, */
 cast('fact_bom'as varchar(15)) as flag, avg(ct_BomFactor) as ct_BomFactor
from fact_bom as f_bom 
inner join Dim_Part as rp on f_bom.Dim_RootPartId = rp.Dim_Partid and ((rp.crossmatplantsts) not in (('00'),('97'),('98'),('99')) )  
inner join Dim_Part as bomc on f_bom.Dim_BOMComponentId = bomc.Dim_Partid  and ((bomc.ItemType_Merck)  in (('VCP'),('BASIC')) )  
inner join Dim_Part as immedpp on f_bom.dim_partidimmediateparent = immedpp.Dim_Partid 
inner join dim_unitofmeasure AS cuom on f_bom.Dim_ComponentUOMId = cuom.dim_unitofmeasureid   
where f_bom.dd_Level = 1
and rp.ItemSubType_Merck not in ('MEDIA','SEED','RAW') /* no need to add these Item Sub Types as parents */
/*exclude the materials marked for deletion*/
and rp.DeletionFlag <> 'X'
and bomc.DeletionFlag <> 'X'
group  by rp.PartNumber,rp.Plant,rp.ItemSubType_Merck,
bomc.PartNumber, bomc.Plant,bomc.ItemSubType_Merck,
f_bom.dd_Level, 'fact_bom' ;

/*Special procurements table*/
drop table if exists tmp_materialMaster_SPK;
create table tmp_materialMaster_SPK as
select m.PartNumber as parent_part, pl.PlantCode as parent_plant, m.ItemSubType_Merck as parent_itemSubType,  /*parent info*/
 m.PartNumber as component_part,   /* material component info*/
/* substr( m.SpecialProcurementDescription, INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1, len( m.SpecialProcurementDescription) - INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1),   plant component info*/
case when m.procureplant <> 'Not Set'  then m.procureplant 
	else substr( m.SpecialProcurementDescription, INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1, len( m.SpecialProcurementDescription) - INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1)
	end as component_plant, /*plant component info*/
m.ItemSubType_Merck as component_itemSubType, 
'fact_mm' as flag,
1 as ct_BomFactor
from fact_materialmaster AS f_mmi 
inner join Dim_Part AS m on f_mmi.dim_materialmasterid = m.Dim_Partid  
inner join Dim_Plant AS pl on f_mmi.dim_plantmasterid = pl.Dim_Plantid  
inner join dim_plant as pl_check on pl_check.plantcode = case when m.procureplant <> 'Not Set'  then m.procureplant 
													else substr( m.SpecialProcurementDescription, INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1, len( m.SpecialProcurementDescription) - INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1)
													end
where m.ItemSubType_Merck not in ('MEDIA','SEED','RAW')
/*exclude the materials marked for deletion*/
and m.DeletionFlag <> 'X';

/* Materials in Independent Requirements - */
drop table if exists tmp_indepReq_root_parent_list;
CREATE TABLE tmp_indepReq_root_parent_list AS
	select distinct concat(dp.PartNumber, '-', pl.plantcode) parent_part_plant,
	dp.PartNumber as parent_part,
	pl.PlantCode as parent_plant,
	dp.ItemSubType_Merck as parent_itemSubType
FROM fact_independentrequirements AS f_ir 
	INNER JOIN Dim_Date AS dfd ON f_ir.dim_deliveryfinishdate = dfd.Dim_Dateid   AND dfd.DateValue >= current_date  and  dfd.DateValue >= current_date + 729
	--INNER JOIN Dim_Date AS drd ON f_ir.dim_requirementsdateid = drd.Dim_Dateid  AND (drd.DateValue  BETWEEN current_date and current_date + 364)
	INNER JOIN Dim_Part AS dp ON f_ir.dim_partid = dp.Dim_Partid  AND dp.ItemSubType_Merck = 'FPP' and lower(dp.ItemType_Merck) = lower('VCP')
							AND (dp.PlantMaterialStatus) NOT IN (('00'),('97'),('98'),('99'))   AND (dp.crossmatplantsts) NOT IN (('00'),('97'),('99'),('98')) 
	INNER JOIN Dim_Plant AS pl ON f_ir.dim_plantid = pl.Dim_Plantid and lower(pl.tacticalring_merck) = lower('ComOps')
WHERE (lower(f_ir.dd_versionactive) = lower('X'))
and dp.ItemSubType_Merck not in ('MEDIA','SEED','RAW')
/*exclude the materials marked for deletion*/
and dp.DeletionFlag <> 'X';


/*Insert the Indep Req - these are the TOP level UINs*/
insert into tmp_bom_MTP bomm  (parent_part,parent_plant,parent_itemSubType,component_part, component_plant, component_itemSubType, flag, ct_BomFactor)
select distinct ireq.parent_part, ireq.parent_plant, ireq.parent_itemSubType,
		bom.parent_part as component_part, bom.parent_plant as component_plant, bom.parent_itemSubType as component_itemSubType, 'fact_ireq', 1 as ct_BomFactor
from tmp_indepReq_root_parent_list ireq
inner join tmp_bom_MTP bom on ireq.parent_part = bom.parent_part
where not exists ( select 1 from tmp_indepReq_root_parent_list ireq
				where ireq.parent_part = bom.parent_part
					and ireq.parent_plant = bom.parent_plant
				and bom.component_part = bom.component_part and bom.component_plant = bom.component_plant)
and bom.parent_itemSubType not in ('MEDIA','SEED','RAW')
/*only insert the special procurements - APP-10928*/
and exists (select 1 from tmp_materialMaster_SPK SPK
			where ireq.parent_part = spk.parent_part and ireq.parent_plant = spk.parent_plant
					and bom.parent_part = spk.component_part and bom.parent_plant = spk.component_plant);
				
/*Include SPK - special procurement keys (interplants)*/
/* insert into  tmp_bom_MTP (parent_part,parent_plant,parent_itemSubType,component_part, component_plant, component_itemSubType, flag, ct_BomFactor)
select m.PartNumber, pl.PlantCode, m.ItemSubType_Merck, -- parent info
 m.PartNumber,   -- material component info
-- substr( m.SpecialProcurementDescription, INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1, len( m.SpecialProcurementDescription) - INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1),   plant component info
m.procureplant, --plant component info
 m.ItemSubType_Merck, 
'fact_mm' as flag,
1 as ct_BomFactor
from fact_materialmaster AS f_mmi 
inner join Dim_Part AS m on f_mmi.dim_materialmasterid = m.Dim_Partid  
inner join Dim_Plant AS pl on f_mmi.dim_plantmasterid = pl.Dim_Plantid  
inner join dim_plant as pl_check on pl_check.plantcode = substr( m.SpecialProcurementDescription, INSTR( m.SpecialProcurementDescription,' ',2, 2) +1, len( m.SpecialProcurementDescription) - INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1)
where 
not exists ( select 1 from tmp_bom_MTP t
					 where  t.parent_part = m.PartNumber and t.parent_plant = pl.plantcode and t.component_part = m.PartNumber and t.component_plant = pl_check.plantcode ) 
					-- condition excluded for component - if we already have a transition, don't add a new one. Specific case - 022758-IE20 - where this is also coming from Ireq on level 1 - don't need MM on level 1 on a TOP view
and exists ( -- the new SPK comoponent has to be a parent in tmp_bom
			select 1 from tmp_bom_MTP t			
			where t.parent_part = m.PartNumber
				and t.parent_plant = substr( m.SpecialProcurementDescription, INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1, len( m.SpecialProcurementDescription) - INSTR( m.SpecialProcurementDescription,' ',2, 2) + 1)  -- plant component info
and m.ItemSubType_Merck not in ('MEDIA','SEED','RAW')
		) */

/*insert SPKs from table previously created*/						
insert into  tmp_bom_MTP (parent_part,parent_plant,parent_itemSubType,component_part, component_plant, component_itemSubType, flag, ct_BomFactor)
select parent_part,parent_plant,parent_itemSubType,component_part, component_plant, component_itemSubType, flag, ct_BomFactor
from tmp_materialMaster_SPK spk
where not exists ( select 1 from tmp_bom_MTP t
					 where  t.parent_part = spk.parent_part and t.parent_plant = spk.parent_plant and t.component_part = spk.component_part and t.component_plant = spk.component_plant ) 
and exists ( -- the new SPK comoponent has to be a parent in tmp_bom
			select 1 from tmp_bom_MTP t			
			where t.parent_part = spk.component_part
				and t.parent_plant = spk.component_plant ); /* plant component info */

/* create all the possible hierarchies*/
drop table if exists tmp_bom_MTP_hierarchies;
create table tmp_bom_MTP_hierarchies as
select mtp.*, 'Y' as single_view, cast('Not Set' as varchar(30)) as root_part_duplicate_view, 'Not Set' as duplicate_view, 
CONNECT_BY_ROOT concat (parent_part, '-', parent_plant) ROOT_PART_single_view
 ,SYS_CONNECT_BY_PATH(concat(parent_part, '-', parent_plant), '/') PATHS, level as levels,
CONNECT_BY_ISLEAF isleaf
from tmp_bom_MTP mtp
CONNECT by nocycle PRIOR component_part = parent_part and PRIOR component_plant = parent_plant;

-- START WITH parent_part  works with an IN (list of materials) but not working with a subselect
/*END APP-10354*/
alter table tmp_bom_MTP_hierarchies add column flag_topUIN varchar(10);

update tmp_bom_MTP_hierarchies 
set flag_topUIN = 'top'
from tmp_bom_MTP_hierarchies 
where ROOT_PART_single_view  in (select distinct parent_part_plant from tmp_indepReq_root_parent_list);

update tmp_bom_MTP_hierarchies 
set root_part_duplicate_view = root_part_single_view
where  flag_topUIN = 'top';

/* get all the single views that are related to the TOP hierarchies*/
drop table if exists tmp_single_views_of_tops;
create table tmp_single_views_of_tops as
select concat(component_part, '-', component_plant) as single_views_of_tops, root_part_single_view as root_part_duplicate_view, levels as top_levels
from tmp_bom_MTP_hierarchies h 
where  flag_topUIN  = 'top' and isleaf = 0;

/*two inserts: one with the single, one with the duplicate views*/ 
delete from tmp_dim_bom_MTP_view;

/*the TOP single views*/
insert into tmp_dim_bom_MTP_view  (root_part_duplicate_view,parent_part,parent_plant,parent_itemsubtype,component_part,component_plant,component_itemsubtype,flag,
	ct_bomfactor,single_view,duplicate_view, root_part_single_view,paths,levels,isleaf,top_levels,flag_topUIN)
select root_part_duplicate_view,parent_part,parent_plant,parent_itemsubtype,component_part,component_plant,component_itemsubtype,flag,
	ct_bomfactor,single_view,duplicate_view, root_part_single_view,paths,levels,isleaf, 0 as top_levels,flag_topUIN
from tmp_bom_MTP_hierarchies 
where  flag_topUIN  = 'top'; 

 /*each single NON-TOP single view is duplicated by all TOP views it is component of*/
insert into tmp_dim_bom_MTP_view 
(root_part_duplicate_view,parent_part,parent_plant,parent_itemsubtype,component_part,component_plant,component_itemsubtype,flag,
	ct_bomfactor,single_view,duplicate_view, root_part_single_view,paths,levels,isleaf,top_levels)
select distinct s.root_part_duplicate_view, h.parent_part, h.parent_plant, h.parent_itemsubtype, h.component_part, h.component_plant, h.component_itemsubtype,
				h.flag, 0 as ct_bomfactor, h.single_view, 'Y' as duplicate_view,
				h.root_part_single_view, h.paths, h.levels, h.isleaf,  s.top_levels
from tmp_bom_MTP_hierarchies h
	inner join tmp_single_views_of_tops s on h.root_part_single_view = s.single_views_of_tops
order by s.root_part_duplicate_view, levels;  

/* generate the links - the join condition column - of the dimension with BOM -> for those situations where the TOP level UIN is from Indep Req*/
/* Take the first parent of a hierarchy (from the top level) that exists in BOM, and use it as a link*/
merge into tmp_dim_bom_MTP_view h using
(
	
	select distinct tmin.root_part_duplicate_view , concat (parent_part, '-', parent_plant) as root_part_single_view_bom_link,
			row_number() over (partition by tmin.root_part_duplicate_view order by tmin.root_part_duplicate_view)  as rn
	from tmp_dim_bom_MTP_view t
	inner join
		(select distinct root_part_duplicate_view,
			min(levels) min_level
		from tmp_dim_bom_MTP_view
		where flag = 'fact_bom'
			and top_levels = 0
		group by root_part_duplicate_view) tmin on t.root_part_duplicate_view = tmin.root_part_duplicate_view 
												and  t.levels = tmin.min_level
												and t.top_levels = 0
    group by tmin.root_part_duplicate_view, concat (parent_part, '-', parent_plant)
) upd
on h.root_part_duplicate_view = upd.root_part_duplicate_view
	and upd.rn = 1
when matched then update
set h.root_part_single_view_bom_link = upd.root_part_single_view_bom_link;

/* Some of the hierarchies has 2 items on the first level (2 Indep Req) -> resulting 2 items on level 2. E.g: '022758-IE20' and 126464-USA0' */
/* The previuous merge creates a link between the dimension and the BOM area based on the first Parent -Plant combination from Level 2*/
/* So, for the other Parent - Plant we need to duplicate the data, with a different root_part_single_view_bom_link */
/* This way, when user filters on any of the Parent - Plant, he won't loose the data*/
/* these are the hierarchies that has to be duplicated, with a different root_part_single_view_bom_link*/
drop table if exists  tmp_bom_distinct_links;
create table tmp_bom_distinct_links as
select distinct tmin.root_part_duplicate_view , count(distinct concat (parent_part, '-', parent_plant)) distinct_links
	from tmp_dim_bom_MTP_view t
	inner join
		(select distinct root_part_duplicate_view,
			min(levels) min_level
		from tmp_dim_bom_MTP_view
		where flag = 'fact_bom'
			and top_levels = 0
		group by root_part_duplicate_view) tmin on t.root_part_duplicate_view = tmin.root_part_duplicate_view 
												and  t.levels = tmin.min_level
												and t.top_levels = 0
    group by tmin.root_part_duplicate_view
having count(distinct concat (parent_part, '-', parent_plant)) > 1;

/* duplicate the rows in dimension - those having different items on the second level - about 6000 rows*/
insert into tmp_dim_bom_MTP_view
(root_part_duplicate_view,parent_part,parent_plant,parent_itemsubtype,component_part,component_plant,component_itemsubtype,flag,
	ct_bomfactor,single_view,duplicate_view, root_part_single_view,paths,levels,isleaf,top_levels,flag_topUIN)
select root_part_duplicate_view,parent_part,parent_plant,parent_itemsubtype,component_part,component_plant,component_itemsubtype,flag,
	ct_bomfactor,single_view,duplicate_view, root_part_single_view,paths,levels,isleaf,top_levels,flag_topUIN
from tmp_dim_bom_MTP_view
where root_part_duplicate_view in (select root_part_duplicate_view from tmp_bom_distinct_links);

/* update the link for the rows previously inserted*/
merge into tmp_dim_bom_MTP_view h using
(	
	select distinct tmin.root_part_duplicate_view , concat (parent_part, '-', parent_plant) as root_part_single_view_bom_link,
			row_number() over (partition by tmin.root_part_duplicate_view order by tmin.root_part_duplicate_view)  as rn
	from tmp_dim_bom_MTP_view t
	inner join
		(select distinct root_part_duplicate_view,
			min(levels) min_level
		from tmp_dim_bom_MTP_view
		where flag = 'fact_bom'
			and top_levels = 0
		group by root_part_duplicate_view) tmin on t.root_part_duplicate_view = tmin.root_part_duplicate_view 
												and  t.levels = tmin.min_level
												and t.top_levels = 0
    where root_part_single_view_bom_link  = 'Not Set'
    group by tmin.root_part_duplicate_view, concat (parent_part, '-', parent_plant)
) upd
on h.root_part_duplicate_view = upd.root_part_duplicate_view
  and upd.rn = 2
and h.root_part_single_view_bom_link  = 'Not Set'
when matched then update
set h.root_part_single_view_bom_link = upd.root_part_single_view_bom_link;

/*insert into the BOM duplicate view dimension*/
delete from dim_bom_MTP_view;

delete from number_fountain m where m.table_name = 'dim_bom_MTP_view';

insert into number_fountain
select 'dim_bom_MTP_view', 1;  /*always recreate the dimension*/

/*final insert into the dimension*/
insert into dim_bom_MTP_view 
(		
		dim_bom_MTP_viewid,
		parent_part,
		parent_plant,
		parent_itemsubtype,
		component_part,
		component_plant,
		component_itemsubtype,
		flag,
		root_part_single_view,
		single_view,
		root_part_duplicate_view,
		duplicate_view,
		paths,
		ddorder,
		/* bomfactor_duplicate_view, */
		bomfactor,
		levels,
		isleaf,
		top_levels,
		root_part_single_view_bom_link,
		flag_topUIN
		)
select 
	(select ifnull(m.max_id,1) from number_fountain m where m.table_name = 'dim_bom_MTP_view') + row_number() over(order by '') as dim_bom_MTP_viewid,
	t.* from
	(select distinct 
		parent_part,
		parent_plant,
		parent_itemsubtype,
		component_part,
		component_plant,
		component_itemsubtype,
		flag,
		root_part_single_view,
		single_view,
		root_part_duplicate_view,
		duplicate_view,
		paths,
		row_number() over (partition by root_part_duplicate_view order by root_part_duplicate_view, top_levels, levels) ddorder,
		/* bomfactor_duplicate_view, */
		ct_bomfactor,
		levels,
		isleaf,
		top_levels,
		root_part_single_view_bom_link,
		flag_topUIN
	from tmp_dim_bom_MTP_view
	order by root_part_duplicate_view, top_levels, levels) t;


update  dim_bom_MTP_view mtp_d
set root_part_single_view_bom_link_material = substring(mtp_d.root_part_single_view_bom_link, 0, instr(mtp_d.root_part_single_view_bom_link, '-') - 1 );

update  dim_bom_MTP_view mtp_d
set root_part_single_view_bom_link_plant = substring(mtp_d.root_part_single_view_bom_link, instr(mtp_d.root_part_single_view_bom_link, '-') + 1,  length(mtp_d.root_part_single_view_bom_link) - instr(mtp_d.root_part_single_view_bom_link, '-') + 1);

update dim_bom_MTP_view 
set single_view =  case when flag_topUIN = 'top' then 'Y' else 'N' end;
