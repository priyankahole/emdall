/*insert 1 -records with no active FPP*/  
insert into fact_scm m   
         (  
         /* fact_mmprodhierarchyid, */ 
         dim_plantid_raw_1,dim_partid_raw_1, dim_batchid_raw_1, dd_documentno, dd_DocumentItemNo, dd_scheduleno, 
                                        dim_dateidschedorder,dim_dateidcreate,dim_dateidpostingdate,dd_dateuserstatussamr_raw_1,dd_dateuserstatusqcco_raw_1,  
                                        dim_dateidinspectionstart_raw_1, 
                                         dim_dateidusagedecisionmade_raw_1,dd_inspectionlotno_raw_1, dim_dateidactualrelease_raw_1 
                                         ,dd_ordernumber_raw_1_antigen_1,dim_plantid_antigen_1,dim_partid_antigen_1,dim_batchid_antigen_1,dim_dateidactualrelease_antigen_1,  
                     dim_dateidactualstart_antigen_1,dim_dateidactualheaderfinish_antigen_1,dd_inspectionlotno_antigen_1,dd_dateuserstatusqcco_antigen_1, 
                     dim_dateidinspectionstart_antigen_1,dd_dateuserstatussamr_antigen_1,dim_dateidusagedecisionmade_antigen_1 
                     ,dd_ordernumber_antigen_1_antigen_2,dim_plantid_antigen_2,dim_partid_antigen_2,dim_batchid_antigen_2,dim_dateidactualrelease_antigen_2,  
                     dim_dateidactualstart_antigen_2,dim_dateidactualheaderfinish_antigen_2,dd_inspectionlotno_antigen_2,dd_dateuserstatusqcco_antigen_2, 
                     dim_dateidinspectionstart_antigen_2,dd_dateuserstatussamr_antigen_2,dim_dateidusagedecisionmade_antigen_2 
                     ,dd_ordernumber_antigen_2_antigen_3,dim_plantid_antigen_3,dim_partid_antigen_3,dim_batchid_antigen_3,dim_dateidactualrelease_antigen_3,  
                     dim_dateidactualstart_antigen_3,dim_dateidactualheaderfinish_antigen_3,dd_inspectionlotno_antigen_3,dd_dateuserstatusqcco_antigen_3, 
                     dim_dateidinspectionstart_antigen_3,dd_dateuserstatussamr_antigen_3,dim_dateidusagedecisionmade_antigen_3 
                     ,dd_ordernumber_antigen_3_antigen_4,dim_plantid_antigen_4,dim_partid_antigen_4,dim_batchid_antigen_4,dim_dateidactualrelease_antigen_4,  
                     dim_dateidactualstart_antigen_4,dim_dateidactualheaderfinish_antigen_4,dd_inspectionlotno_antigen_4,dd_dateuserstatusqcco_antigen_4, 
                     dim_dateidinspectionstart_antigen_4,dd_dateuserstatussamr_antigen_4,dim_dateidusagedecisionmade_antigen_4 
                     ,dd_ordernumber_antigen_4_antigen_5,dim_plantid_antigen_5,dim_partid_antigen_5,dim_batchid_antigen_5,dim_dateidactualrelease_antigen_5,  
                     dim_dateidactualstart_antigen_5,dim_dateidactualheaderfinish_antigen_5,dd_inspectionlotno_antigen_5,dd_dateuserstatusqcco_antigen_5, 
                     dim_dateidinspectionstart_antigen_5,dd_dateuserstatussamr_antigen_5,dim_dateidusagedecisionmade_antigen_5 
                     ,dd_ordernumber_antigen_5_bulk_1,dim_plantid_bulk_1,dim_partid_bulk_1,dim_batchid_bulk_1,dim_dateidactualrelease_bulk_1,  
                     dim_dateidactualstart_bulk_1,dim_dateidactualheaderfinish_bulk_1,dd_inspectionlotno_bulk_1,dd_dateuserstatusqcco_bulk_1, 
                     dim_dateidinspectionstart_bulk_1,dd_dateuserstatussamr_bulk_1,dim_dateidusagedecisionmade_bulk_1 
                     ,dd_ordernumber_bulk_1_bulk_2,dim_plantid_bulk_2,dim_partid_bulk_2,dim_batchid_bulk_2,dim_dateidactualrelease_bulk_2,  
                     dim_dateidactualstart_bulk_2,dim_dateidactualheaderfinish_bulk_2,dd_inspectionlotno_bulk_2,dd_dateuserstatusqcco_bulk_2, 
                     dim_dateidinspectionstart_bulk_2,dd_dateuserstatussamr_bulk_2,dim_dateidusagedecisionmade_bulk_2 
                     ,dd_ordernumber_bulk_2_bulk_3,dim_plantid_bulk_3,dim_partid_bulk_3,dim_batchid_bulk_3,dim_dateidactualrelease_bulk_3,  
                     dim_dateidactualstart_bulk_3,dim_dateidactualheaderfinish_bulk_3,dd_inspectionlotno_bulk_3,dd_dateuserstatusqcco_bulk_3, 
                     dim_dateidinspectionstart_bulk_3,dd_dateuserstatussamr_bulk_3,dim_dateidusagedecisionmade_bulk_3 
                     ,dd_ordernumber_bulk_3_fpu_1,dim_plantid_fpu_1,dim_partid_fpu_1,dim_batchid_fpu_1,dim_dateidactualrelease_fpu_1,  
                     dim_dateidactualstart_fpu_1,dim_dateidactualheaderfinish_fpu_1,dd_inspectionlotno_fpu_1,dd_dateuserstatusqcco_fpu_1, 
                     dim_dateidinspectionstart_fpu_1,dd_dateuserstatussamr_fpu_1,dim_dateidusagedecisionmade_fpu_1 
                     ,dd_ordernumber_fpu_1_fpu_2,dim_plantid_fpu_2,dim_partid_fpu_2,dim_batchid_fpu_2,dim_dateidactualrelease_fpu_2,  
                     dim_dateidactualstart_fpu_2,dim_dateidactualheaderfinish_fpu_2,dd_inspectionlotno_fpu_2,dd_dateuserstatusqcco_fpu_2, 
                     dim_dateidinspectionstart_fpu_2,dd_dateuserstatussamr_fpu_2,dim_dateidusagedecisionmade_fpu_2 
                     ,dd_ordernumber_fpu_2_fpu_3,dim_plantid_fpu_3,dim_partid_fpu_3,dim_batchid_fpu_3,dim_dateidactualrelease_fpu_3,  
                     dim_dateidactualstart_fpu_3,dim_dateidactualheaderfinish_fpu_3,dd_inspectionlotno_fpu_3,dd_dateuserstatusqcco_fpu_3, 
                     dim_dateidinspectionstart_fpu_3,dd_dateuserstatussamr_fpu_3,dim_dateidusagedecisionmade_fpu_3 
                     ,dd_ordernumber_fpu_3_fpu_4,dim_plantid_fpu_4,dim_partid_fpu_4,dim_batchid_fpu_4,dim_dateidactualrelease_fpu_4,  
                     dim_dateidactualstart_fpu_4,dim_dateidactualheaderfinish_fpu_4,dd_inspectionlotno_fpu_4,dd_dateuserstatusqcco_fpu_4, 
                     dim_dateidinspectionstart_fpu_4,dd_dateuserstatussamr_fpu_4,dim_dateidusagedecisionmade_fpu_4 
                     ,dd_ordernumber_fpu_4_fpu_5,dim_plantid_fpu_5,dim_partid_fpu_5,dim_batchid_fpu_5,dim_dateidactualrelease_fpu_5,  
                     dim_dateidactualstart_fpu_5,dim_dateidactualheaderfinish_fpu_5,dd_inspectionlotno_fpu_5,dd_dateuserstatusqcco_fpu_5, 
                     dim_dateidinspectionstart_fpu_5,dd_dateuserstatussamr_fpu_5,dim_dateidusagedecisionmade_fpu_5 
                     ,dd_ordernumber_fpu_5_fpp_1,dim_plantid_fpp_1,dim_partid_fpp_1,dim_batchid_fpp_1,dim_dateidactualrelease_fpp_1,  
                     dim_dateidactualstart_fpp_1,dim_dateidactualheaderfinish_fpp_1,dd_inspectionlotno_fpp_1,dd_dateuserstatusqcco_fpp_1, 
                     dim_dateidinspectionstart_fpp_1,dd_dateuserstatussamr_fpp_1,dim_dateidusagedecisionmade_fpp_1 
                     ,dd_ordernumber_fpp_1_fpp_2,dim_plantid_fpp_2,dim_partid_fpp_2,dim_batchid_fpp_2,dim_dateidactualrelease_fpp_2,  
                     dim_dateidactualstart_fpp_2,dim_dateidactualheaderfinish_fpp_2,dd_inspectionlotno_fpp_2,dd_dateuserstatusqcco_fpp_2, 
                     dim_dateidinspectionstart_fpp_2,dd_dateuserstatussamr_fpp_2,dim_dateidusagedecisionmade_fpp_2 
                     ,dd_ordernumber_fpp_2_fpp_3,dim_plantid_fpp_3,dim_partid_fpp_3,dim_batchid_fpp_3,dim_dateidactualrelease_fpp_3,  
                     dim_dateidactualstart_fpp_3,dim_dateidactualheaderfinish_fpp_3,dd_inspectionlotno_fpp_3,dd_dateuserstatusqcco_fpp_3, 
                     dim_dateidinspectionstart_fpp_3,dd_dateuserstatussamr_fpp_3,dim_dateidusagedecisionmade_fpp_3 
                     ,ct_TargetMasterRecipe_raw_1,  
                                         dim_dateuserstatussamr_raw_1,  
                                         dim_dateuserstatusqcco_raw_1,  
                                         ct_minReleasedStockDays_raw_1,  
                                         ct_lotsizedays_raw_1,  
                                         ct_floatBeforeProduction_raw_1,  
                                         ct_TargetMasterRecipe_comops,  
                                         ct_floatBeforeProduction_comops  
                                         ,ct_TargetMasterRecipe_antigen_1,  
                                                 dim_dateuserstatussamr_antigen_1,  
                                                 dim_dateuserstatusqcco_antigen_1,  
                                                 ct_minReleasedStockDays_antigen_1,  
                                                 ct_lotsizedays_antigen_1,  
                                                 ct_floatBeforeProduction_antigen_1  
                                                 ,ct_TargetMasterRecipe_antigen_2,  
                                                 dim_dateuserstatussamr_antigen_2,  
                                                 dim_dateuserstatusqcco_antigen_2,  
                                                 ct_minReleasedStockDays_antigen_2,  
                                                 ct_lotsizedays_antigen_2,  
                                                 ct_floatBeforeProduction_antigen_2  
                                                 ,ct_TargetMasterRecipe_antigen_3,  
                                                 dim_dateuserstatussamr_antigen_3,  
                                                 dim_dateuserstatusqcco_antigen_3,  
                                                 ct_minReleasedStockDays_antigen_3,  
                                                 ct_lotsizedays_antigen_3,  
                                                 ct_floatBeforeProduction_antigen_3  
                                                 ,ct_TargetMasterRecipe_antigen_4,  
                                                 dim_dateuserstatussamr_antigen_4,  
                                                 dim_dateuserstatusqcco_antigen_4,  
                                                 ct_minReleasedStockDays_antigen_4,  
                                                 ct_lotsizedays_antigen_4,  
                                                 ct_floatBeforeProduction_antigen_4  
                                                 ,ct_TargetMasterRecipe_antigen_5,  
                                                 dim_dateuserstatussamr_antigen_5,  
                                                 dim_dateuserstatusqcco_antigen_5,  
                                                 ct_minReleasedStockDays_antigen_5,  
                                                 ct_lotsizedays_antigen_5,  
                                                 ct_floatBeforeProduction_antigen_5  
                                                 ,ct_TargetMasterRecipe_bulk_1,  
                                                 dim_dateuserstatussamr_bulk_1,  
                                                 dim_dateuserstatusqcco_bulk_1,  
                                                 ct_minReleasedStockDays_bulk_1,  
                                                 ct_lotsizedays_bulk_1,  
                                                 ct_floatBeforeProduction_bulk_1  
                                                 ,ct_TargetMasterRecipe_bulk_2,  
                                                 dim_dateuserstatussamr_bulk_2,  
                                                 dim_dateuserstatusqcco_bulk_2,  
                                                 ct_minReleasedStockDays_bulk_2,  
                                                 ct_lotsizedays_bulk_2,  
                                                 ct_floatBeforeProduction_bulk_2  
                                                 ,ct_TargetMasterRecipe_bulk_3,  
                                                 dim_dateuserstatussamr_bulk_3,  
                                                 dim_dateuserstatusqcco_bulk_3,  
                                                 ct_minReleasedStockDays_bulk_3,  
                                                 ct_lotsizedays_bulk_3,  
                                                 ct_floatBeforeProduction_bulk_3  
                                                 ,ct_TargetMasterRecipe_fpu_1,  
                                                 dim_dateuserstatussamr_fpu_1,  
                                                 dim_dateuserstatusqcco_fpu_1,  
                                                 ct_minReleasedStockDays_fpu_1,  
                                                 ct_lotsizedays_fpu_1,  
                                                 ct_floatBeforeProduction_fpu_1  
                                                 ,ct_TargetMasterRecipe_fpu_2,  
                                                 dim_dateuserstatussamr_fpu_2,  
                                                 dim_dateuserstatusqcco_fpu_2,  
                                                 ct_minReleasedStockDays_fpu_2,  
                                                 ct_lotsizedays_fpu_2,  
                                                 ct_floatBeforeProduction_fpu_2  
                                                 ,ct_TargetMasterRecipe_fpu_3,  
                                                 dim_dateuserstatussamr_fpu_3,  
                                                 dim_dateuserstatusqcco_fpu_3,  
                                                 ct_minReleasedStockDays_fpu_3,  
                                                 ct_lotsizedays_fpu_3,  
                                                 ct_floatBeforeProduction_fpu_3  
                                                 ,ct_TargetMasterRecipe_fpu_4,  
                                                 dim_dateuserstatussamr_fpu_4,  
                                                 dim_dateuserstatusqcco_fpu_4,  
                                                 ct_minReleasedStockDays_fpu_4,  
                                                 ct_lotsizedays_fpu_4,  
                                                 ct_floatBeforeProduction_fpu_4  
                                                 ,ct_TargetMasterRecipe_fpu_5,  
                                                 dim_dateuserstatussamr_fpu_5,  
                                                 dim_dateuserstatusqcco_fpu_5,  
                                                 ct_minReleasedStockDays_fpu_5,  
                                                 ct_lotsizedays_fpu_5,  
                                                 ct_floatBeforeProduction_fpu_5  
                                                 ,ct_TargetMasterRecipe_fpp_1,  
                                                 dim_dateuserstatussamr_fpp_1,  
                                                 dim_dateuserstatusqcco_fpp_1,  
                                                 ct_minReleasedStockDays_fpp_1,  
                                                 ct_lotsizedays_fpp_1,  
                                                 ct_floatBeforeProduction_fpp_1  
                                                 ,ct_TargetMasterRecipe_fpp_2,  
                                                 dim_dateuserstatussamr_fpp_2,  
                                                 dim_dateuserstatusqcco_fpp_2,  
                                                 ct_minReleasedStockDays_fpp_2,  
                                                 ct_lotsizedays_fpp_2,  
                                                 ct_floatBeforeProduction_fpp_2  
                                                 ,ct_TargetMasterRecipe_fpp_3,  
                                                 dim_dateuserstatussamr_fpp_3,  
                                                 dim_dateuserstatusqcco_fpp_3,  
                                                 ct_minReleasedStockDays_fpp_3,  
                                                 ct_lotsizedays_fpp_3,  
                                                 ct_floatBeforeProduction_fpp_3  
                                                 ,dd_ordernumber_raw_1_bulk_1,dd_ordernumber_antigen_1_fpu_1,dd_ordernumber_antigen_1_fpp_1,dd_ordernumber_fpu_1_fpp_1,dd_ordernumber_antigen_1_bulk_1,dd_ordernumber_bulk_1_fpp_1,dd_ordernumber_bulk_1_fpu_1,dd_ordernumber_raw_1_fpu_1,dd_ordernumber_raw_1_fpp_1 )  
         select   
             /* (select max_id from number_fountain where table_name = 'fact_scm') + row_number() over(ORDER BY '') as fact_mmprodhierarchyid,   
 */             dim_plantid_raw_1,dim_partid_raw_1, dim_batchid_raw_1, dd_documentno, dd_DocumentItemNo, dd_scheduleno, 
                                        dim_dateidschedorder,dim_dateidcreate,dim_dateidpostingdate,dd_dateuserstatussamr_raw_1,dd_dateuserstatusqcco_raw_1,  
                                        dim_dateidinspectionstart_raw_1, 
                                         dim_dateidusagedecisionmade_raw_1,dd_inspectionlotno_raw_1, dim_dateidactualrelease_raw_1 
                                         ,dd_ordernumber_raw_1_antigen_1,dim_plantid_antigen_1,dim_partid_antigen_1,dim_batchid_antigen_1,dim_dateidactualrelease_antigen_1,  
                     dim_dateidactualstart_antigen_1,dim_dateidactualheaderfinish_antigen_1,dd_inspectionlotno_antigen_1,dd_dateuserstatusqcco_antigen_1, 
                     dim_dateidinspectionstart_antigen_1,dd_dateuserstatussamr_antigen_1,dim_dateidusagedecisionmade_antigen_1 
                     ,dd_ordernumber_antigen_1_antigen_2,dim_plantid_antigen_2,dim_partid_antigen_2,dim_batchid_antigen_2,dim_dateidactualrelease_antigen_2,  
                     dim_dateidactualstart_antigen_2,dim_dateidactualheaderfinish_antigen_2,dd_inspectionlotno_antigen_2,dd_dateuserstatusqcco_antigen_2, 
                     dim_dateidinspectionstart_antigen_2,dd_dateuserstatussamr_antigen_2,dim_dateidusagedecisionmade_antigen_2 
                     ,dd_ordernumber_antigen_2_antigen_3,dim_plantid_antigen_3,dim_partid_antigen_3,dim_batchid_antigen_3,dim_dateidactualrelease_antigen_3,  
                     dim_dateidactualstart_antigen_3,dim_dateidactualheaderfinish_antigen_3,dd_inspectionlotno_antigen_3,dd_dateuserstatusqcco_antigen_3, 
                     dim_dateidinspectionstart_antigen_3,dd_dateuserstatussamr_antigen_3,dim_dateidusagedecisionmade_antigen_3 
                     ,dd_ordernumber_antigen_3_antigen_4,dim_plantid_antigen_4,dim_partid_antigen_4,dim_batchid_antigen_4,dim_dateidactualrelease_antigen_4,  
                     dim_dateidactualstart_antigen_4,dim_dateidactualheaderfinish_antigen_4,dd_inspectionlotno_antigen_4,dd_dateuserstatusqcco_antigen_4, 
                     dim_dateidinspectionstart_antigen_4,dd_dateuserstatussamr_antigen_4,dim_dateidusagedecisionmade_antigen_4 
                     ,dd_ordernumber_antigen_4_antigen_5,dim_plantid_antigen_5,dim_partid_antigen_5,dim_batchid_antigen_5,dim_dateidactualrelease_antigen_5,  
                     dim_dateidactualstart_antigen_5,dim_dateidactualheaderfinish_antigen_5,dd_inspectionlotno_antigen_5,dd_dateuserstatusqcco_antigen_5, 
                     dim_dateidinspectionstart_antigen_5,dd_dateuserstatussamr_antigen_5,dim_dateidusagedecisionmade_antigen_5 
                     ,dd_ordernumber_antigen_5_bulk_1,dim_plantid_bulk_1,dim_partid_bulk_1,dim_batchid_bulk_1,dim_dateidactualrelease_bulk_1,  
                     dim_dateidactualstart_bulk_1,dim_dateidactualheaderfinish_bulk_1,dd_inspectionlotno_bulk_1,dd_dateuserstatusqcco_bulk_1, 
                     dim_dateidinspectionstart_bulk_1,dd_dateuserstatussamr_bulk_1,dim_dateidusagedecisionmade_bulk_1 
                     ,dd_ordernumber_bulk_1_bulk_2,dim_plantid_bulk_2,dim_partid_bulk_2,dim_batchid_bulk_2,dim_dateidactualrelease_bulk_2,  
                     dim_dateidactualstart_bulk_2,dim_dateidactualheaderfinish_bulk_2,dd_inspectionlotno_bulk_2,dd_dateuserstatusqcco_bulk_2, 
                     dim_dateidinspectionstart_bulk_2,dd_dateuserstatussamr_bulk_2,dim_dateidusagedecisionmade_bulk_2 
                     ,dd_ordernumber_bulk_2_bulk_3,dim_plantid_bulk_3,dim_partid_bulk_3,dim_batchid_bulk_3,dim_dateidactualrelease_bulk_3,  
                     dim_dateidactualstart_bulk_3,dim_dateidactualheaderfinish_bulk_3,dd_inspectionlotno_bulk_3,dd_dateuserstatusqcco_bulk_3, 
                     dim_dateidinspectionstart_bulk_3,dd_dateuserstatussamr_bulk_3,dim_dateidusagedecisionmade_bulk_3 
                     ,dd_ordernumber_bulk_3_fpu_1,dim_plantid_fpu_1,dim_partid_fpu_1,dim_batchid_fpu_1,dim_dateidactualrelease_fpu_1,  
                     dim_dateidactualstart_fpu_1,dim_dateidactualheaderfinish_fpu_1,dd_inspectionlotno_fpu_1,dd_dateuserstatusqcco_fpu_1, 
                     dim_dateidinspectionstart_fpu_1,dd_dateuserstatussamr_fpu_1,dim_dateidusagedecisionmade_fpu_1 
                     ,dd_ordernumber_fpu_1_fpu_2,dim_plantid_fpu_2,dim_partid_fpu_2,dim_batchid_fpu_2,dim_dateidactualrelease_fpu_2,  
                     dim_dateidactualstart_fpu_2,dim_dateidactualheaderfinish_fpu_2,dd_inspectionlotno_fpu_2,dd_dateuserstatusqcco_fpu_2, 
                     dim_dateidinspectionstart_fpu_2,dd_dateuserstatussamr_fpu_2,dim_dateidusagedecisionmade_fpu_2 
                     ,dd_ordernumber_fpu_2_fpu_3,dim_plantid_fpu_3,dim_partid_fpu_3,dim_batchid_fpu_3,dim_dateidactualrelease_fpu_3,  
                     dim_dateidactualstart_fpu_3,dim_dateidactualheaderfinish_fpu_3,dd_inspectionlotno_fpu_3,dd_dateuserstatusqcco_fpu_3, 
                     dim_dateidinspectionstart_fpu_3,dd_dateuserstatussamr_fpu_3,dim_dateidusagedecisionmade_fpu_3 
                     ,dd_ordernumber_fpu_3_fpu_4,dim_plantid_fpu_4,dim_partid_fpu_4,dim_batchid_fpu_4,dim_dateidactualrelease_fpu_4,  
                     dim_dateidactualstart_fpu_4,dim_dateidactualheaderfinish_fpu_4,dd_inspectionlotno_fpu_4,dd_dateuserstatusqcco_fpu_4, 
                     dim_dateidinspectionstart_fpu_4,dd_dateuserstatussamr_fpu_4,dim_dateidusagedecisionmade_fpu_4 
                     ,dd_ordernumber_fpu_4_fpu_5,dim_plantid_fpu_5,dim_partid_fpu_5,dim_batchid_fpu_5,dim_dateidactualrelease_fpu_5,  
                     dim_dateidactualstart_fpu_5,dim_dateidactualheaderfinish_fpu_5,dd_inspectionlotno_fpu_5,dd_dateuserstatusqcco_fpu_5, 
                     dim_dateidinspectionstart_fpu_5,dd_dateuserstatussamr_fpu_5,dim_dateidusagedecisionmade_fpu_5 
                     ,dd_ordernumber_fpu_5_fpp_1,dim_plantid_fpp_1,dim_partid_fpp_1,dim_batchid_fpp_1,dim_dateidactualrelease_fpp_1,  
                     dim_dateidactualstart_fpp_1,dim_dateidactualheaderfinish_fpp_1,dd_inspectionlotno_fpp_1,dd_dateuserstatusqcco_fpp_1, 
                     dim_dateidinspectionstart_fpp_1,dd_dateuserstatussamr_fpp_1,dim_dateidusagedecisionmade_fpp_1 
                     ,dd_ordernumber_fpp_1_fpp_2,dim_plantid_fpp_2,dim_partid_fpp_2,dim_batchid_fpp_2,dim_dateidactualrelease_fpp_2,  
                     dim_dateidactualstart_fpp_2,dim_dateidactualheaderfinish_fpp_2,dd_inspectionlotno_fpp_2,dd_dateuserstatusqcco_fpp_2, 
                     dim_dateidinspectionstart_fpp_2,dd_dateuserstatussamr_fpp_2,dim_dateidusagedecisionmade_fpp_2 
                     ,dd_ordernumber_fpp_2_fpp_3,dim_plantid_fpp_3,dim_partid_fpp_3,dim_batchid_fpp_3,dim_dateidactualrelease_fpp_3,  
                     dim_dateidactualstart_fpp_3,dim_dateidactualheaderfinish_fpp_3,dd_inspectionlotno_fpp_3,dd_dateuserstatusqcco_fpp_3, 
                     dim_dateidinspectionstart_fpp_3,dd_dateuserstatussamr_fpp_3,dim_dateidusagedecisionmade_fpp_3 
                     ,ct_TargetMasterRecipe_raw_1,  
                                         dim_dateuserstatussamr_raw_1,  
                                         dim_dateuserstatusqcco_raw_1,  
                                         ct_minReleasedStockDays_raw_1,  
                                         ct_lotsizedays_raw_1,  
                                         ct_floatBeforeProduction_raw_1,  
                                         ct_TargetMasterRecipe_comops,  
                                         ct_floatBeforeProduction_comops  
                                         ,ct_TargetMasterRecipe_antigen_1,  
                                                 dim_dateuserstatussamr_antigen_1,  
                                                 dim_dateuserstatusqcco_antigen_1,  
                                                 ct_minReleasedStockDays_antigen_1,  
                                                 ct_lotsizedays_antigen_1,  
                                                 ct_floatBeforeProduction_antigen_1  
                                                 ,ct_TargetMasterRecipe_antigen_2,  
                                                 dim_dateuserstatussamr_antigen_2,  
                                                 dim_dateuserstatusqcco_antigen_2,  
                                                 ct_minReleasedStockDays_antigen_2,  
                                                 ct_lotsizedays_antigen_2,  
                                                 ct_floatBeforeProduction_antigen_2  
                                                 ,ct_TargetMasterRecipe_antigen_3,  
                                                 dim_dateuserstatussamr_antigen_3,  
                                                 dim_dateuserstatusqcco_antigen_3,  
                                                 ct_minReleasedStockDays_antigen_3,  
                                                 ct_lotsizedays_antigen_3,  
                                                 ct_floatBeforeProduction_antigen_3  
                                                 ,ct_TargetMasterRecipe_antigen_4,  
                                                 dim_dateuserstatussamr_antigen_4,  
                                                 dim_dateuserstatusqcco_antigen_4,  
                                                 ct_minReleasedStockDays_antigen_4,  
                                                 ct_lotsizedays_antigen_4,  
                                                 ct_floatBeforeProduction_antigen_4  
                                                 ,ct_TargetMasterRecipe_antigen_5,  
                                                 dim_dateuserstatussamr_antigen_5,  
                                                 dim_dateuserstatusqcco_antigen_5,  
                                                 ct_minReleasedStockDays_antigen_5,  
                                                 ct_lotsizedays_antigen_5,  
                                                 ct_floatBeforeProduction_antigen_5  
                                                 ,ct_TargetMasterRecipe_bulk_1,  
                                                 dim_dateuserstatussamr_bulk_1,  
                                                 dim_dateuserstatusqcco_bulk_1,  
                                                 ct_minReleasedStockDays_bulk_1,  
                                                 ct_lotsizedays_bulk_1,  
                                                 ct_floatBeforeProduction_bulk_1  
                                                 ,ct_TargetMasterRecipe_bulk_2,  
                                                 dim_dateuserstatussamr_bulk_2,  
                                                 dim_dateuserstatusqcco_bulk_2,  
                                                 ct_minReleasedStockDays_bulk_2,  
                                                 ct_lotsizedays_bulk_2,  
                                                 ct_floatBeforeProduction_bulk_2  
                                                 ,ct_TargetMasterRecipe_bulk_3,  
                                                 dim_dateuserstatussamr_bulk_3,  
                                                 dim_dateuserstatusqcco_bulk_3,  
                                                 ct_minReleasedStockDays_bulk_3,  
                                                 ct_lotsizedays_bulk_3,  
                                                 ct_floatBeforeProduction_bulk_3  
                                                 ,ct_TargetMasterRecipe_fpu_1,  
                                                 dim_dateuserstatussamr_fpu_1,  
                                                 dim_dateuserstatusqcco_fpu_1,  
                                                 ct_minReleasedStockDays_fpu_1,  
                                                 ct_lotsizedays_fpu_1,  
                                                 ct_floatBeforeProduction_fpu_1  
                                                 ,ct_TargetMasterRecipe_fpu_2,  
                                                 dim_dateuserstatussamr_fpu_2,  
                                                 dim_dateuserstatusqcco_fpu_2,  
                                                 ct_minReleasedStockDays_fpu_2,  
                                                 ct_lotsizedays_fpu_2,  
                                                 ct_floatBeforeProduction_fpu_2  
                                                 ,ct_TargetMasterRecipe_fpu_3,  
                                                 dim_dateuserstatussamr_fpu_3,  
                                                 dim_dateuserstatusqcco_fpu_3,  
                                                 ct_minReleasedStockDays_fpu_3,  
                                                 ct_lotsizedays_fpu_3,  
                                                 ct_floatBeforeProduction_fpu_3  
                                                 ,ct_TargetMasterRecipe_fpu_4,  
                                                 dim_dateuserstatussamr_fpu_4,  
                                                 dim_dateuserstatusqcco_fpu_4,  
                                                 ct_minReleasedStockDays_fpu_4,  
                                                 ct_lotsizedays_fpu_4,  
                                                 ct_floatBeforeProduction_fpu_4  
                                                 ,ct_TargetMasterRecipe_fpu_5,  
                                                 dim_dateuserstatussamr_fpu_5,  
                                                 dim_dateuserstatusqcco_fpu_5,  
                                                 ct_minReleasedStockDays_fpu_5,  
                                                 ct_lotsizedays_fpu_5,  
                                                 ct_floatBeforeProduction_fpu_5  
                                                 ,ct_TargetMasterRecipe_fpp_1,  
                                                 dim_dateuserstatussamr_fpp_1,  
                                                 dim_dateuserstatusqcco_fpp_1,  
                                                 ct_minReleasedStockDays_fpp_1,  
                                                 ct_lotsizedays_fpp_1,  
                                                 ct_floatBeforeProduction_fpp_1  
                                                 ,ct_TargetMasterRecipe_fpp_2,  
                                                 dim_dateuserstatussamr_fpp_2,  
                                                 dim_dateuserstatusqcco_fpp_2,  
                                                 ct_minReleasedStockDays_fpp_2,  
                                                 ct_lotsizedays_fpp_2,  
                                                 ct_floatBeforeProduction_fpp_2  
                                                 ,ct_TargetMasterRecipe_fpp_3,  
                                                 dim_dateuserstatussamr_fpp_3,  
                                                 dim_dateuserstatusqcco_fpp_3,  
                                                 ct_minReleasedStockDays_fpp_3,  
                                                 ct_lotsizedays_fpp_3,  
                                                 ct_floatBeforeProduction_fpp_3  
                                                 ,dd_ordernumber_raw_1_bulk_1,dd_ordernumber_antigen_1_fpu_1,dd_ordernumber_antigen_1_fpp_1,dd_ordernumber_fpu_1_fpp_1,dd_ordernumber_antigen_1_bulk_1,dd_ordernumber_bulk_1_fpp_1,dd_ordernumber_bulk_1_fpu_1,dd_ordernumber_raw_1_fpu_1,dd_ordernumber_raw_1_fpp_1   
             from fact_scm_test a   
             where dim_batchid_FPP_final = 1 and dim_partid_FPP_final = 1 
         ;
 drop table if exists fact_scm_test_deleted; 
create table fact_scm_test_deleted as select * from fact_scm_test where dim_batchid_FPP_final = 1 and dim_partid_FPP_final = 1; 
delete from fact_scm_test where dim_batchid_FPP_final = 1 and dim_partid_FPP_final = 1; 
/*insert 2 -records with active FPP + Shipping*/ 
/* fact insert */ 
/* alter table fact_scm_test add column hash_value_plantPartBatch varchar(200) */ 
 alter table tmp004_insplot_salesorderdelivery002 add column hash_value_plantPartBatch varchar(200);  
update fact_scm_test 
     set hash_value_plantPartBatch = hash_md5 (dim_plantid_FPP_final, pafpp.partnumber, bafpp.batchnumber) 
          from fact_scm_test a   
          inner join dim_batch bafpp on a.dim_batchid_FPP_final = bafpp.dim_batchid   
          inner join dim_part pafpp on a.dim_partid_FPP_final = pafpp.dim_partid;    
 update tmp004_insplot_salesorderdelivery002 b 
     set hash_value_plantPartBatch = hash_md5 (dim_plantid, pasdpartnumber, basdbatchnumber) 
     from tmp004_insplot_salesorderdelivery002 b; 
 insert into fact_scm m   
         (         /* fact_mmprodhierarchyid, */         dim_plantid_raw_1,dim_partid_raw_1, dim_batchid_raw_1, dd_documentno, dd_DocumentItemNo, dd_scheduleno, 
                                        dim_dateidschedorder,dim_dateidcreate,dim_dateidpostingdate,dd_dateuserstatussamr_raw_1,dd_dateuserstatusqcco_raw_1,  
                                        dim_dateidinspectionstart_raw_1, 
                                         dim_dateidusagedecisionmade_raw_1,dd_inspectionlotno_raw_1, dim_dateidactualrelease_raw_1 
                                         ,dd_ordernumber_raw_1_antigen_1,dim_plantid_antigen_1,dim_partid_antigen_1,dim_batchid_antigen_1,dim_dateidactualrelease_antigen_1,  
                     dim_dateidactualstart_antigen_1,dim_dateidactualheaderfinish_antigen_1,dd_inspectionlotno_antigen_1,dd_dateuserstatusqcco_antigen_1, 
                     dim_dateidinspectionstart_antigen_1,dd_dateuserstatussamr_antigen_1,dim_dateidusagedecisionmade_antigen_1 
                     ,dd_ordernumber_antigen_1_antigen_2,dim_plantid_antigen_2,dim_partid_antigen_2,dim_batchid_antigen_2,dim_dateidactualrelease_antigen_2,  
                     dim_dateidactualstart_antigen_2,dim_dateidactualheaderfinish_antigen_2,dd_inspectionlotno_antigen_2,dd_dateuserstatusqcco_antigen_2, 
                     dim_dateidinspectionstart_antigen_2,dd_dateuserstatussamr_antigen_2,dim_dateidusagedecisionmade_antigen_2 
                     ,dd_ordernumber_antigen_2_antigen_3,dim_plantid_antigen_3,dim_partid_antigen_3,dim_batchid_antigen_3,dim_dateidactualrelease_antigen_3,  
                     dim_dateidactualstart_antigen_3,dim_dateidactualheaderfinish_antigen_3,dd_inspectionlotno_antigen_3,dd_dateuserstatusqcco_antigen_3, 
                     dim_dateidinspectionstart_antigen_3,dd_dateuserstatussamr_antigen_3,dim_dateidusagedecisionmade_antigen_3 
                     ,dd_ordernumber_antigen_3_antigen_4,dim_plantid_antigen_4,dim_partid_antigen_4,dim_batchid_antigen_4,dim_dateidactualrelease_antigen_4,  
                     dim_dateidactualstart_antigen_4,dim_dateidactualheaderfinish_antigen_4,dd_inspectionlotno_antigen_4,dd_dateuserstatusqcco_antigen_4, 
                     dim_dateidinspectionstart_antigen_4,dd_dateuserstatussamr_antigen_4,dim_dateidusagedecisionmade_antigen_4 
                     ,dd_ordernumber_antigen_4_antigen_5,dim_plantid_antigen_5,dim_partid_antigen_5,dim_batchid_antigen_5,dim_dateidactualrelease_antigen_5,  
                     dim_dateidactualstart_antigen_5,dim_dateidactualheaderfinish_antigen_5,dd_inspectionlotno_antigen_5,dd_dateuserstatusqcco_antigen_5, 
                     dim_dateidinspectionstart_antigen_5,dd_dateuserstatussamr_antigen_5,dim_dateidusagedecisionmade_antigen_5 
                     ,dd_ordernumber_antigen_5_bulk_1,dim_plantid_bulk_1,dim_partid_bulk_1,dim_batchid_bulk_1,dim_dateidactualrelease_bulk_1,  
                     dim_dateidactualstart_bulk_1,dim_dateidactualheaderfinish_bulk_1,dd_inspectionlotno_bulk_1,dd_dateuserstatusqcco_bulk_1, 
                     dim_dateidinspectionstart_bulk_1,dd_dateuserstatussamr_bulk_1,dim_dateidusagedecisionmade_bulk_1 
                     ,dd_ordernumber_bulk_1_bulk_2,dim_plantid_bulk_2,dim_partid_bulk_2,dim_batchid_bulk_2,dim_dateidactualrelease_bulk_2,  
                     dim_dateidactualstart_bulk_2,dim_dateidactualheaderfinish_bulk_2,dd_inspectionlotno_bulk_2,dd_dateuserstatusqcco_bulk_2, 
                     dim_dateidinspectionstart_bulk_2,dd_dateuserstatussamr_bulk_2,dim_dateidusagedecisionmade_bulk_2 
                     ,dd_ordernumber_bulk_2_bulk_3,dim_plantid_bulk_3,dim_partid_bulk_3,dim_batchid_bulk_3,dim_dateidactualrelease_bulk_3,  
                     dim_dateidactualstart_bulk_3,dim_dateidactualheaderfinish_bulk_3,dd_inspectionlotno_bulk_3,dd_dateuserstatusqcco_bulk_3, 
                     dim_dateidinspectionstart_bulk_3,dd_dateuserstatussamr_bulk_3,dim_dateidusagedecisionmade_bulk_3 
                     ,dd_ordernumber_bulk_3_fpu_1,dim_plantid_fpu_1,dim_partid_fpu_1,dim_batchid_fpu_1,dim_dateidactualrelease_fpu_1,  
                     dim_dateidactualstart_fpu_1,dim_dateidactualheaderfinish_fpu_1,dd_inspectionlotno_fpu_1,dd_dateuserstatusqcco_fpu_1, 
                     dim_dateidinspectionstart_fpu_1,dd_dateuserstatussamr_fpu_1,dim_dateidusagedecisionmade_fpu_1 
                     ,dd_ordernumber_fpu_1_fpu_2,dim_plantid_fpu_2,dim_partid_fpu_2,dim_batchid_fpu_2,dim_dateidactualrelease_fpu_2,  
                     dim_dateidactualstart_fpu_2,dim_dateidactualheaderfinish_fpu_2,dd_inspectionlotno_fpu_2,dd_dateuserstatusqcco_fpu_2, 
                     dim_dateidinspectionstart_fpu_2,dd_dateuserstatussamr_fpu_2,dim_dateidusagedecisionmade_fpu_2 
                     ,dd_ordernumber_fpu_2_fpu_3,dim_plantid_fpu_3,dim_partid_fpu_3,dim_batchid_fpu_3,dim_dateidactualrelease_fpu_3,  
                     dim_dateidactualstart_fpu_3,dim_dateidactualheaderfinish_fpu_3,dd_inspectionlotno_fpu_3,dd_dateuserstatusqcco_fpu_3, 
                     dim_dateidinspectionstart_fpu_3,dd_dateuserstatussamr_fpu_3,dim_dateidusagedecisionmade_fpu_3 
                     ,dd_ordernumber_fpu_3_fpu_4,dim_plantid_fpu_4,dim_partid_fpu_4,dim_batchid_fpu_4,dim_dateidactualrelease_fpu_4,  
                     dim_dateidactualstart_fpu_4,dim_dateidactualheaderfinish_fpu_4,dd_inspectionlotno_fpu_4,dd_dateuserstatusqcco_fpu_4, 
                     dim_dateidinspectionstart_fpu_4,dd_dateuserstatussamr_fpu_4,dim_dateidusagedecisionmade_fpu_4 
                     ,dd_ordernumber_fpu_4_fpu_5,dim_plantid_fpu_5,dim_partid_fpu_5,dim_batchid_fpu_5,dim_dateidactualrelease_fpu_5,  
                     dim_dateidactualstart_fpu_5,dim_dateidactualheaderfinish_fpu_5,dd_inspectionlotno_fpu_5,dd_dateuserstatusqcco_fpu_5, 
                     dim_dateidinspectionstart_fpu_5,dd_dateuserstatussamr_fpu_5,dim_dateidusagedecisionmade_fpu_5 
                     ,dd_ordernumber_fpu_5_fpp_1,dim_plantid_fpp_1,dim_partid_fpp_1,dim_batchid_fpp_1,dim_dateidactualrelease_fpp_1,  
                     dim_dateidactualstart_fpp_1,dim_dateidactualheaderfinish_fpp_1,dd_inspectionlotno_fpp_1,dd_dateuserstatusqcco_fpp_1, 
                     dim_dateidinspectionstart_fpp_1,dd_dateuserstatussamr_fpp_1,dim_dateidusagedecisionmade_fpp_1 
                     ,dd_ordernumber_fpp_1_fpp_2,dim_plantid_fpp_2,dim_partid_fpp_2,dim_batchid_fpp_2,dim_dateidactualrelease_fpp_2,  
                     dim_dateidactualstart_fpp_2,dim_dateidactualheaderfinish_fpp_2,dd_inspectionlotno_fpp_2,dd_dateuserstatusqcco_fpp_2, 
                     dim_dateidinspectionstart_fpp_2,dd_dateuserstatussamr_fpp_2,dim_dateidusagedecisionmade_fpp_2 
                     ,dd_ordernumber_fpp_2_fpp_3,dim_plantid_fpp_3,dim_partid_fpp_3,dim_batchid_fpp_3,dim_dateidactualrelease_fpp_3,  
                     dim_dateidactualstart_fpp_3,dim_dateidactualheaderfinish_fpp_3,dd_inspectionlotno_fpp_3,dd_dateuserstatusqcco_fpp_3, 
                     dim_dateidinspectionstart_fpp_3,dd_dateuserstatussamr_fpp_3,dim_dateidusagedecisionmade_fpp_3 
                     ,ct_TargetMasterRecipe_raw_1,  
                                         dim_dateuserstatussamr_raw_1,  
                                         dim_dateuserstatusqcco_raw_1,  
                                         ct_minReleasedStockDays_raw_1,  
                                         ct_lotsizedays_raw_1,  
                                         ct_floatBeforeProduction_raw_1,  
                                         ct_TargetMasterRecipe_comops,  
                                         ct_floatBeforeProduction_comops  
                                         ,ct_TargetMasterRecipe_antigen_1,  
                                                 dim_dateuserstatussamr_antigen_1,  
                                                 dim_dateuserstatusqcco_antigen_1,  
                                                 ct_minReleasedStockDays_antigen_1,  
                                                 ct_lotsizedays_antigen_1,  
                                                 ct_floatBeforeProduction_antigen_1  
                                                 ,ct_TargetMasterRecipe_antigen_2,  
                                                 dim_dateuserstatussamr_antigen_2,  
                                                 dim_dateuserstatusqcco_antigen_2,  
                                                 ct_minReleasedStockDays_antigen_2,  
                                                 ct_lotsizedays_antigen_2,  
                                                 ct_floatBeforeProduction_antigen_2  
                                                 ,ct_TargetMasterRecipe_antigen_3,  
                                                 dim_dateuserstatussamr_antigen_3,  
                                                 dim_dateuserstatusqcco_antigen_3,  
                                                 ct_minReleasedStockDays_antigen_3,  
                                                 ct_lotsizedays_antigen_3,  
                                                 ct_floatBeforeProduction_antigen_3  
                                                 ,ct_TargetMasterRecipe_antigen_4,  
                                                 dim_dateuserstatussamr_antigen_4,  
                                                 dim_dateuserstatusqcco_antigen_4,  
                                                 ct_minReleasedStockDays_antigen_4,  
                                                 ct_lotsizedays_antigen_4,  
                                                 ct_floatBeforeProduction_antigen_4  
                                                 ,ct_TargetMasterRecipe_antigen_5,  
                                                 dim_dateuserstatussamr_antigen_5,  
                                                 dim_dateuserstatusqcco_antigen_5,  
                                                 ct_minReleasedStockDays_antigen_5,  
                                                 ct_lotsizedays_antigen_5,  
                                                 ct_floatBeforeProduction_antigen_5  
                                                 ,ct_TargetMasterRecipe_bulk_1,  
                                                 dim_dateuserstatussamr_bulk_1,  
                                                 dim_dateuserstatusqcco_bulk_1,  
                                                 ct_minReleasedStockDays_bulk_1,  
                                                 ct_lotsizedays_bulk_1,  
                                                 ct_floatBeforeProduction_bulk_1  
                                                 ,ct_TargetMasterRecipe_bulk_2,  
                                                 dim_dateuserstatussamr_bulk_2,  
                                                 dim_dateuserstatusqcco_bulk_2,  
                                                 ct_minReleasedStockDays_bulk_2,  
                                                 ct_lotsizedays_bulk_2,  
                                                 ct_floatBeforeProduction_bulk_2  
                                                 ,ct_TargetMasterRecipe_bulk_3,  
                                                 dim_dateuserstatussamr_bulk_3,  
                                                 dim_dateuserstatusqcco_bulk_3,  
                                                 ct_minReleasedStockDays_bulk_3,  
                                                 ct_lotsizedays_bulk_3,  
                                                 ct_floatBeforeProduction_bulk_3  
                                                 ,ct_TargetMasterRecipe_fpu_1,  
                                                 dim_dateuserstatussamr_fpu_1,  
                                                 dim_dateuserstatusqcco_fpu_1,  
                                                 ct_minReleasedStockDays_fpu_1,  
                                                 ct_lotsizedays_fpu_1,  
                                                 ct_floatBeforeProduction_fpu_1  
                                                 ,ct_TargetMasterRecipe_fpu_2,  
                                                 dim_dateuserstatussamr_fpu_2,  
                                                 dim_dateuserstatusqcco_fpu_2,  
                                                 ct_minReleasedStockDays_fpu_2,  
                                                 ct_lotsizedays_fpu_2,  
                                                 ct_floatBeforeProduction_fpu_2  
                                                 ,ct_TargetMasterRecipe_fpu_3,  
                                                 dim_dateuserstatussamr_fpu_3,  
                                                 dim_dateuserstatusqcco_fpu_3,  
                                                 ct_minReleasedStockDays_fpu_3,  
                                                 ct_lotsizedays_fpu_3,  
                                                 ct_floatBeforeProduction_fpu_3  
                                                 ,ct_TargetMasterRecipe_fpu_4,  
                                                 dim_dateuserstatussamr_fpu_4,  
                                                 dim_dateuserstatusqcco_fpu_4,  
                                                 ct_minReleasedStockDays_fpu_4,  
                                                 ct_lotsizedays_fpu_4,  
                                                 ct_floatBeforeProduction_fpu_4  
                                                 ,ct_TargetMasterRecipe_fpu_5,  
                                                 dim_dateuserstatussamr_fpu_5,  
                                                 dim_dateuserstatusqcco_fpu_5,  
                                                 ct_minReleasedStockDays_fpu_5,  
                                                 ct_lotsizedays_fpu_5,  
                                                 ct_floatBeforeProduction_fpu_5  
                                                 ,ct_TargetMasterRecipe_fpp_1,  
                                                 dim_dateuserstatussamr_fpp_1,  
                                                 dim_dateuserstatusqcco_fpp_1,  
                                                 ct_minReleasedStockDays_fpp_1,  
                                                 ct_lotsizedays_fpp_1,  
                                                 ct_floatBeforeProduction_fpp_1  
                                                 ,ct_TargetMasterRecipe_fpp_2,  
                                                 dim_dateuserstatussamr_fpp_2,  
                                                 dim_dateuserstatusqcco_fpp_2,  
                                                 ct_minReleasedStockDays_fpp_2,  
                                                 ct_lotsizedays_fpp_2,  
                                                 ct_floatBeforeProduction_fpp_2  
                                                 ,ct_TargetMasterRecipe_fpp_3,  
                                                 dim_dateuserstatussamr_fpp_3,  
                                                 dim_dateuserstatusqcco_fpp_3,  
                                                 ct_minReleasedStockDays_fpp_3,  
                                                 ct_lotsizedays_fpp_3,  
                                                 ct_floatBeforeProduction_fpp_3  
                                                 ,dd_ordernumber_raw_1_bulk_1,dd_ordernumber_antigen_1_fpu_1,dd_ordernumber_antigen_1_fpp_1,dd_ordernumber_fpu_1_fpp_1,dd_ordernumber_antigen_1_bulk_1,dd_ordernumber_bulk_1_fpp_1,dd_ordernumber_bulk_1_fpu_1,dd_ordernumber_raw_1_fpu_1,dd_ordernumber_raw_1_fpp_1 ,dim_plantid_comops,   
                 dd_salesdlvrdocno_comops,   
                 dd_salesdlvritemno_comops,   
                 dim_dateiddlvrdoccreated_comops,   
                 dim_dateidactualgoodsissue_comops,   
                 dim_dateidactualreceipt_comops,   
                 dd_inspectionlotno_comops,   
                 dd_dateuserstatussamr_comops,   
                 dim_dateidinspectionstart_comops,   
                 dd_dateuserstatusqcco_comops,   
                 dim_Dateidusagedecisionmade_comops,   
                 dd_salesdlvrdocno_customer,   
                 dd_salesdlvritemno_customer,   
                 dim_dateiddlvrdoccreated_customer,   
                 dim_dateidactualgoodsissue_customer,   
                 ct_qtydelivered,   
                 dd_SalesDocNo_customer,  
                 dd_SalesItemNo_customer, 
                 dd_SalesDocNo_comops, 
                 dd_SalesItemNo_comops, 
                 amt_orderAmountSales, 
                 amt_ExchangeRate, 
                 amt_ExchangeRate_GBL, 
                 dim_partid_comops, 
                 dim_customerShipCust, 
                 dim_customerSoldCust, 
                 dim_customerShipComops, 
                 dim_customerSoldComops 
                     )  
         select   
            /*  (select max_id from number_fountain where table_name = 'fact_scm') + row_number() over(ORDER BY '') as fact_mmprodhierarchyid,   
  */             a.dim_plantid_raw_1,dim_partid_raw_1, dim_batchid_raw_1, dd_documentno, dd_DocumentItemNo, dd_scheduleno, 
                                        dim_dateidschedorder,dim_dateidcreate,dim_dateidpostingdate,dd_dateuserstatussamr_raw_1,dd_dateuserstatusqcco_raw_1,  
                                        dim_dateidinspectionstart_raw_1, 
                                         dim_dateidusagedecisionmade_raw_1,dd_inspectionlotno_raw_1, dim_dateidactualrelease_raw_1 
                                         ,dd_ordernumber_raw_1_antigen_1,dim_plantid_antigen_1,dim_partid_antigen_1,dim_batchid_antigen_1,dim_dateidactualrelease_antigen_1,  
                     dim_dateidactualstart_antigen_1,dim_dateidactualheaderfinish_antigen_1,dd_inspectionlotno_antigen_1,dd_dateuserstatusqcco_antigen_1, 
                     dim_dateidinspectionstart_antigen_1,dd_dateuserstatussamr_antigen_1,dim_dateidusagedecisionmade_antigen_1 
                     ,dd_ordernumber_antigen_1_antigen_2,dim_plantid_antigen_2,dim_partid_antigen_2,dim_batchid_antigen_2,dim_dateidactualrelease_antigen_2,  
                     dim_dateidactualstart_antigen_2,dim_dateidactualheaderfinish_antigen_2,dd_inspectionlotno_antigen_2,dd_dateuserstatusqcco_antigen_2, 
                     dim_dateidinspectionstart_antigen_2,dd_dateuserstatussamr_antigen_2,dim_dateidusagedecisionmade_antigen_2 
                     ,dd_ordernumber_antigen_2_antigen_3,dim_plantid_antigen_3,dim_partid_antigen_3,dim_batchid_antigen_3,dim_dateidactualrelease_antigen_3,  
                     dim_dateidactualstart_antigen_3,dim_dateidactualheaderfinish_antigen_3,dd_inspectionlotno_antigen_3,dd_dateuserstatusqcco_antigen_3, 
                     dim_dateidinspectionstart_antigen_3,dd_dateuserstatussamr_antigen_3,dim_dateidusagedecisionmade_antigen_3 
                     ,dd_ordernumber_antigen_3_antigen_4,dim_plantid_antigen_4,dim_partid_antigen_4,dim_batchid_antigen_4,dim_dateidactualrelease_antigen_4,  
                     dim_dateidactualstart_antigen_4,dim_dateidactualheaderfinish_antigen_4,dd_inspectionlotno_antigen_4,dd_dateuserstatusqcco_antigen_4, 
                     dim_dateidinspectionstart_antigen_4,dd_dateuserstatussamr_antigen_4,dim_dateidusagedecisionmade_antigen_4 
                     ,dd_ordernumber_antigen_4_antigen_5,dim_plantid_antigen_5,dim_partid_antigen_5,dim_batchid_antigen_5,dim_dateidactualrelease_antigen_5,  
                     dim_dateidactualstart_antigen_5,dim_dateidactualheaderfinish_antigen_5,dd_inspectionlotno_antigen_5,dd_dateuserstatusqcco_antigen_5, 
                     dim_dateidinspectionstart_antigen_5,dd_dateuserstatussamr_antigen_5,dim_dateidusagedecisionmade_antigen_5 
                     ,dd_ordernumber_antigen_5_bulk_1,dim_plantid_bulk_1,dim_partid_bulk_1,dim_batchid_bulk_1,dim_dateidactualrelease_bulk_1,  
                     dim_dateidactualstart_bulk_1,dim_dateidactualheaderfinish_bulk_1,dd_inspectionlotno_bulk_1,dd_dateuserstatusqcco_bulk_1, 
                     dim_dateidinspectionstart_bulk_1,dd_dateuserstatussamr_bulk_1,dim_dateidusagedecisionmade_bulk_1 
                     ,dd_ordernumber_bulk_1_bulk_2,dim_plantid_bulk_2,dim_partid_bulk_2,dim_batchid_bulk_2,dim_dateidactualrelease_bulk_2,  
                     dim_dateidactualstart_bulk_2,dim_dateidactualheaderfinish_bulk_2,dd_inspectionlotno_bulk_2,dd_dateuserstatusqcco_bulk_2, 
                     dim_dateidinspectionstart_bulk_2,dd_dateuserstatussamr_bulk_2,dim_dateidusagedecisionmade_bulk_2 
                     ,dd_ordernumber_bulk_2_bulk_3,dim_plantid_bulk_3,dim_partid_bulk_3,dim_batchid_bulk_3,dim_dateidactualrelease_bulk_3,  
                     dim_dateidactualstart_bulk_3,dim_dateidactualheaderfinish_bulk_3,dd_inspectionlotno_bulk_3,dd_dateuserstatusqcco_bulk_3, 
                     dim_dateidinspectionstart_bulk_3,dd_dateuserstatussamr_bulk_3,dim_dateidusagedecisionmade_bulk_3 
                     ,dd_ordernumber_bulk_3_fpu_1,dim_plantid_fpu_1,dim_partid_fpu_1,dim_batchid_fpu_1,dim_dateidactualrelease_fpu_1,  
                     dim_dateidactualstart_fpu_1,dim_dateidactualheaderfinish_fpu_1,dd_inspectionlotno_fpu_1,dd_dateuserstatusqcco_fpu_1, 
                     dim_dateidinspectionstart_fpu_1,dd_dateuserstatussamr_fpu_1,dim_dateidusagedecisionmade_fpu_1 
                     ,dd_ordernumber_fpu_1_fpu_2,dim_plantid_fpu_2,dim_partid_fpu_2,dim_batchid_fpu_2,dim_dateidactualrelease_fpu_2,  
                     dim_dateidactualstart_fpu_2,dim_dateidactualheaderfinish_fpu_2,dd_inspectionlotno_fpu_2,dd_dateuserstatusqcco_fpu_2, 
                     dim_dateidinspectionstart_fpu_2,dd_dateuserstatussamr_fpu_2,dim_dateidusagedecisionmade_fpu_2 
                     ,dd_ordernumber_fpu_2_fpu_3,dim_plantid_fpu_3,dim_partid_fpu_3,dim_batchid_fpu_3,dim_dateidactualrelease_fpu_3,  
                     dim_dateidactualstart_fpu_3,dim_dateidactualheaderfinish_fpu_3,dd_inspectionlotno_fpu_3,dd_dateuserstatusqcco_fpu_3, 
                     dim_dateidinspectionstart_fpu_3,dd_dateuserstatussamr_fpu_3,dim_dateidusagedecisionmade_fpu_3 
                     ,dd_ordernumber_fpu_3_fpu_4,dim_plantid_fpu_4,dim_partid_fpu_4,dim_batchid_fpu_4,dim_dateidactualrelease_fpu_4,  
                     dim_dateidactualstart_fpu_4,dim_dateidactualheaderfinish_fpu_4,dd_inspectionlotno_fpu_4,dd_dateuserstatusqcco_fpu_4, 
                     dim_dateidinspectionstart_fpu_4,dd_dateuserstatussamr_fpu_4,dim_dateidusagedecisionmade_fpu_4 
                     ,dd_ordernumber_fpu_4_fpu_5,dim_plantid_fpu_5,dim_partid_fpu_5,dim_batchid_fpu_5,dim_dateidactualrelease_fpu_5,  
                     dim_dateidactualstart_fpu_5,dim_dateidactualheaderfinish_fpu_5,dd_inspectionlotno_fpu_5,dd_dateuserstatusqcco_fpu_5, 
                     dim_dateidinspectionstart_fpu_5,dd_dateuserstatussamr_fpu_5,dim_dateidusagedecisionmade_fpu_5 
                     ,dd_ordernumber_fpu_5_fpp_1,dim_plantid_fpp_1,dim_partid_fpp_1,dim_batchid_fpp_1,dim_dateidactualrelease_fpp_1,  
                     dim_dateidactualstart_fpp_1,dim_dateidactualheaderfinish_fpp_1,dd_inspectionlotno_fpp_1,dd_dateuserstatusqcco_fpp_1, 
                     dim_dateidinspectionstart_fpp_1,dd_dateuserstatussamr_fpp_1,dim_dateidusagedecisionmade_fpp_1 
                     ,dd_ordernumber_fpp_1_fpp_2,dim_plantid_fpp_2,dim_partid_fpp_2,dim_batchid_fpp_2,dim_dateidactualrelease_fpp_2,  
                     dim_dateidactualstart_fpp_2,dim_dateidactualheaderfinish_fpp_2,dd_inspectionlotno_fpp_2,dd_dateuserstatusqcco_fpp_2, 
                     dim_dateidinspectionstart_fpp_2,dd_dateuserstatussamr_fpp_2,dim_dateidusagedecisionmade_fpp_2 
                     ,dd_ordernumber_fpp_2_fpp_3,dim_plantid_fpp_3,dim_partid_fpp_3,dim_batchid_fpp_3,dim_dateidactualrelease_fpp_3,  
                     dim_dateidactualstart_fpp_3,dim_dateidactualheaderfinish_fpp_3,dd_inspectionlotno_fpp_3,dd_dateuserstatusqcco_fpp_3, 
                     dim_dateidinspectionstart_fpp_3,dd_dateuserstatussamr_fpp_3,dim_dateidusagedecisionmade_fpp_3 
                     ,ct_TargetMasterRecipe_raw_1,  
                                         dim_dateuserstatussamr_raw_1,  
                                         dim_dateuserstatusqcco_raw_1,  
                                         ct_minReleasedStockDays_raw_1,  
                                         ct_lotsizedays_raw_1,  
                                         ct_floatBeforeProduction_raw_1,  
                                         ct_TargetMasterRecipe_comops,  
                                         ct_floatBeforeProduction_comops  
                                         ,ct_TargetMasterRecipe_antigen_1,  
                                                 dim_dateuserstatussamr_antigen_1,  
                                                 dim_dateuserstatusqcco_antigen_1,  
                                                 ct_minReleasedStockDays_antigen_1,  
                                                 ct_lotsizedays_antigen_1,  
                                                 ct_floatBeforeProduction_antigen_1  
                                                 ,ct_TargetMasterRecipe_antigen_2,  
                                                 dim_dateuserstatussamr_antigen_2,  
                                                 dim_dateuserstatusqcco_antigen_2,  
                                                 ct_minReleasedStockDays_antigen_2,  
                                                 ct_lotsizedays_antigen_2,  
                                                 ct_floatBeforeProduction_antigen_2  
                                                 ,ct_TargetMasterRecipe_antigen_3,  
                                                 dim_dateuserstatussamr_antigen_3,  
                                                 dim_dateuserstatusqcco_antigen_3,  
                                                 ct_minReleasedStockDays_antigen_3,  
                                                 ct_lotsizedays_antigen_3,  
                                                 ct_floatBeforeProduction_antigen_3  
                                                 ,ct_TargetMasterRecipe_antigen_4,  
                                                 dim_dateuserstatussamr_antigen_4,  
                                                 dim_dateuserstatusqcco_antigen_4,  
                                                 ct_minReleasedStockDays_antigen_4,  
                                                 ct_lotsizedays_antigen_4,  
                                                 ct_floatBeforeProduction_antigen_4  
                                                 ,ct_TargetMasterRecipe_antigen_5,  
                                                 dim_dateuserstatussamr_antigen_5,  
                                                 dim_dateuserstatusqcco_antigen_5,  
                                                 ct_minReleasedStockDays_antigen_5,  
                                                 ct_lotsizedays_antigen_5,  
                                                 ct_floatBeforeProduction_antigen_5  
                                                 ,ct_TargetMasterRecipe_bulk_1,  
                                                 dim_dateuserstatussamr_bulk_1,  
                                                 dim_dateuserstatusqcco_bulk_1,  
                                                 ct_minReleasedStockDays_bulk_1,  
                                                 ct_lotsizedays_bulk_1,  
                                                 ct_floatBeforeProduction_bulk_1  
                                                 ,ct_TargetMasterRecipe_bulk_2,  
                                                 dim_dateuserstatussamr_bulk_2,  
                                                 dim_dateuserstatusqcco_bulk_2,  
                                                 ct_minReleasedStockDays_bulk_2,  
                                                 ct_lotsizedays_bulk_2,  
                                                 ct_floatBeforeProduction_bulk_2  
                                                 ,ct_TargetMasterRecipe_bulk_3,  
                                                 dim_dateuserstatussamr_bulk_3,  
                                                 dim_dateuserstatusqcco_bulk_3,  
                                                 ct_minReleasedStockDays_bulk_3,  
                                                 ct_lotsizedays_bulk_3,  
                                                 ct_floatBeforeProduction_bulk_3  
                                                 ,ct_TargetMasterRecipe_fpu_1,  
                                                 dim_dateuserstatussamr_fpu_1,  
                                                 dim_dateuserstatusqcco_fpu_1,  
                                                 ct_minReleasedStockDays_fpu_1,  
                                                 ct_lotsizedays_fpu_1,  
                                                 ct_floatBeforeProduction_fpu_1  
                                                 ,ct_TargetMasterRecipe_fpu_2,  
                                                 dim_dateuserstatussamr_fpu_2,  
                                                 dim_dateuserstatusqcco_fpu_2,  
                                                 ct_minReleasedStockDays_fpu_2,  
                                                 ct_lotsizedays_fpu_2,  
                                                 ct_floatBeforeProduction_fpu_2  
                                                 ,ct_TargetMasterRecipe_fpu_3,  
                                                 dim_dateuserstatussamr_fpu_3,  
                                                 dim_dateuserstatusqcco_fpu_3,  
                                                 ct_minReleasedStockDays_fpu_3,  
                                                 ct_lotsizedays_fpu_3,  
                                                 ct_floatBeforeProduction_fpu_3  
                                                 ,ct_TargetMasterRecipe_fpu_4,  
                                                 dim_dateuserstatussamr_fpu_4,  
                                                 dim_dateuserstatusqcco_fpu_4,  
                                                 ct_minReleasedStockDays_fpu_4,  
                                                 ct_lotsizedays_fpu_4,  
                                                 ct_floatBeforeProduction_fpu_4  
                                                 ,ct_TargetMasterRecipe_fpu_5,  
                                                 dim_dateuserstatussamr_fpu_5,  
                                                 dim_dateuserstatusqcco_fpu_5,  
                                                 ct_minReleasedStockDays_fpu_5,  
                                                 ct_lotsizedays_fpu_5,  
                                                 ct_floatBeforeProduction_fpu_5  
                                                 ,ct_TargetMasterRecipe_fpp_1,  
                                                 dim_dateuserstatussamr_fpp_1,  
                                                 dim_dateuserstatusqcco_fpp_1,  
                                                 ct_minReleasedStockDays_fpp_1,  
                                                 ct_lotsizedays_fpp_1,  
                                                 ct_floatBeforeProduction_fpp_1  
                                                 ,ct_TargetMasterRecipe_fpp_2,  
                                                 dim_dateuserstatussamr_fpp_2,  
                                                 dim_dateuserstatusqcco_fpp_2,  
                                                 ct_minReleasedStockDays_fpp_2,  
                                                 ct_lotsizedays_fpp_2,  
                                                 ct_floatBeforeProduction_fpp_2  
                                                 ,ct_TargetMasterRecipe_fpp_3,  
                                                 dim_dateuserstatussamr_fpp_3,  
                                                 dim_dateuserstatusqcco_fpp_3,  
                                                 ct_minReleasedStockDays_fpp_3,  
                                                 ct_lotsizedays_fpp_3,  
                                                 ct_floatBeforeProduction_fpp_3  
                                                 ,dd_ordernumber_raw_1_bulk_1,dd_ordernumber_antigen_1_fpu_1,dd_ordernumber_antigen_1_fpp_1,dd_ordernumber_fpu_1_fpp_1,dd_ordernumber_antigen_1_bulk_1,dd_ordernumber_bulk_1_fpp_1,dd_ordernumber_bulk_1_fpu_1,dd_ordernumber_raw_1_fpu_1,dd_ordernumber_raw_1_fpp_1,   
             ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,  
             ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,  
             ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,  
             ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,  
             ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,  
             ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,  
             ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,  
             ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,  
             ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,  
             ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,  
             ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,  
             ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,  
             ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,  
             ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,  
             ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,  
             ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,  
             ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,  
             ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer,   
             ifnull(b.dd_SalesDocNo_comops, 'Not Set') as dd_SalesDocNo_comops, 
             ifnull(b.dd_SalesItemNo_comops, 0) as dd_SalesItemNo_comops, 
             ifnull(b.amt_orderAmountSales, 0) as amt_orderAmountSales, 
             ifnull(b.amt_ExchangeRate, 1) as amt_ExchangeRate, 
             ifnull(b.amt_ExchangeRate_GBL, 1) as amt_ExchangeRate_GBL, 
             ifnull(b.dim_partid_comops, 1) as dim_partid_comops, 
             ifnull(b.dim_customerShipCust,1) as dim_customerShipCust, 
             ifnull(b.dim_customerSoldCust,1) as dim_customerSoldCust, 
             ifnull(b.dim_customerShipComops,1) as dim_customerShipComops, 
             ifnull(b.dim_customerSoldComops,1) as dim_customerSoldComops 
         from fact_scm_test a   
        /* inner join dim_batch bafpp on a.dim_batchid_FPP_final = bafpp.dim_batchid  
         inner join dim_part pafpp on a.dim_partid_FPP_final = pafpp.dim_partid  */ 
         inner join tmp004_insplot_salesorderdelivery002 b 
             on a.hash_value_plantPartBatch = b.hash_value_plantPartBatch 
                /* on a.dim_plantid = b.dim_plantid    
                 and bafpp.batchnumber = b.basdbatchnumber   
                 and pafpp.partnumber = b.pasdpartnumber */ 
         ;
update fact_scm 
         set fact_mmprodhierarchyid = fact_id_identity;  
 update fact_scm 
     set dim_partid_FPP_final = 
         case when dim_partid_FPP_3 <> 1 then dim_partid_FPP_3 
                 else case when dim_partid_FPP_2 <> 1 then dim_partid_FPP_2 
                             else case when dim_partid_FPP_1 <> 1 then dim_partid_FPP_1  
                                     else 1 end end end; 
update fact_scm 
         set dim_partid_FPP_final = 
                  case when dim_partid_FPP_3 <> 1 then dim_partid_FPP_3 
                       else case when dim_partid_FPP_2 <> 1 then dim_partid_FPP_2 
                               else case when dim_partid_FPP_1 <> 1 then dim_partid_FPP_1  
                                        else 1 end end end; 
update fact_scm 
         set dim_plantid_FPP_final = 
                  case when dim_plantid_FPP_3 <> 1 then dim_plantid_FPP_3 
                         else case when dim_plantid_FPP_2 <> 1 then dim_plantid_FPP_2 
                                  else case when dim_plantid_FPP_1 <> 1 then dim_plantid_FPP_1  
                                     else 1 end end end; 
update fact_scm 
          set dim_plantid = dim_plantid_FPP_final; 

 
 /* Min released stock in days and Lot size in days to ComOps    */ 
 merge into fact_scm fmm using 
             (select fmm.fact_mmprodhierarchyid, 
                     comops.FACT_INVENTORYATLASID comops_FACT_INVENTORYATLASID, 
                     comops.ct_minreleasedstockindays_merck comops_ct_minreleasedstockindays_merck, 
                     comops.ct_lotsizeindays_merck comops_ct_lotsizedays_merck 
             from fact_scm fmm 
             inner join fact_inventoryatlas comops on fmm.dim_plantid_comops = comops.dim_plantid and fmm.dim_partid_comops = comops.dim_partid 
             ) upd 
             on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
             when matched then update 
             set fmm.ct_minReleasedStockDays_comops = ifnull(upd.comops_ct_minreleasedstockindays_merck, 0) 
             , fmm.ct_lotsizedays_comops = ifnull(upd.comops_ct_lotsizedays_merck, 0); 
