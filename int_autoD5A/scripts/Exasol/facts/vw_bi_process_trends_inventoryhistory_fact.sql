/**********************************************************************************/
/*  28 Aug 2013   1.56        Shanthi    New Field WIP Qty        */
/*  15 Oct 2013   1.81        Issam    Added material movement measures     */
/*  21  Oct 2013  1.82        Shanthi New Fields for prod orders  */
/*  20 Dec 2013   1.82        Issam    Update optimization            */
/**********************************************************************************/


drop table if exists fact_inventoryhistory_delete;
create table fact_inventoryhistory_delete as
select * from fact_inventoryhistory
where SnapshotDate = current_date;

MERGE INTO fact_inventoryhistory t
USING
( select fact_inventoryhistoryid
  from fact_inventoryhistory_delete ) del
ON (t.fact_inventoryhistoryid = del.fact_inventoryhistoryid)
WHEN MATCHED THEN DELETE;

drop table if exists fact_inventoryhistory_delete;

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_inventoryhistory';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_inventoryhistory', ifnull(max(fact_inventoryhistoryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
     FROM fact_inventoryhistory;

INSERT INTO fact_inventoryhistory(ct_POOpenQty, /* Issam change 24th Jun */
                                  ct_SOOpenQty, /* Issam change 24th Jun */
                                  amt_BlockedStockAmt,
                                  amt_BlockedStockAmt_GBL,
                                  amt_CommericalPrice1,
                                  amt_CurPlannedPrice,
                                  amt_MovingAvgPrice,
                                  amt_MtlDlvrCost,
                                  amt_OtherCost,
                                  amt_OverheadCost,
                                  amt_PlannedPrice1,
                                  amt_PreviousPrice,
                                  amt_PrevPlannedPrice,
                                  amt_StdUnitPrice,
                                  amt_StdUnitPrice_GBL,
                                  amt_StockInQInspAmt,
                                  amt_StockInQInspAmt_GBL,
                                  amt_StockInTransferAmt,
                                  amt_StockInTransferAmt_GBL,
                                  amt_StockInTransitAmt,
                                  amt_StockInTransitAmt_GBL,
                                  amt_StockValueAmt,
                                  amt_StockValueAmt_GBL,
                                  amt_UnrestrictedConsgnStockAmt,
                                  amt_UnrestrictedConsgnStockAmt_GBL,
                                  amt_WIPBalance,
          ct_WIPQty,
                                  ct_BlockedConsgnStock,
                                  ct_BlockedStock,
                                  ct_BlockedStockReturns,
                                  ct_ConsgnStockInQInsp,
                                  ct_LastReceivedQty,
                                  ct_RestrictedConsgnStock,
                                  ct_StockInQInsp,
                                  ct_StockInTransfer,
                                  ct_StockInTransit,
                                  ct_StockQty,
                                  ct_TotalRestrictedStock,
                                  ct_UnrestrictedConsgnStock,
                                  ct_WIPAging,
                                  dd_BatchNo,
                                  dd_DocumentItemNo,
                                  dd_DocumentNo,
                                  dd_MovementType,
                                  dd_ValuationType,
                                  dim_Companyid,
                                  Dim_ConsumptionTypeid,
                                  dim_costcenterid,
                                  dim_Currencyid,
                                  Dim_DateIdLastChangedPrice,
                                  Dim_DateIdPlannedPrice2,
                                  Dim_DocumentStatusid,
                                  Dim_DocumentTypeid,
                                  Dim_IncoTermid,
                                  Dim_ItemCategoryid,
                                  Dim_ItemStatusid,
                                  dim_LastReceivedDateid,
                                  Dim_MovementIndicatorid,
                                  dim_Partid,
                                  dim_Plantid,
          Dim_ProfitCenterId,
                                  dim_producthierarchyid,
                                  Dim_PurchaseGroupid,
                                  Dim_PurchaseMiscid,
                                  Dim_PurchaseOrgid,
                                  dim_specialstockid,
                                  dim_stockcategoryid,
                                  dim_StockTypeid,
                                  dim_StorageLocationid,
                                  dim_StorageLocEntryDateid,
                                  Dim_SupplyingPlantId,
                                  Dim_Termid,
                                  Dim_UnitOfMeasureid,
                                  dim_Vendorid,
                  Dim_VendorMasterid,
                                  SnapshotDate,
                                  Dim_DateidSnapshot,
                                  fact_inventoryhistoryid,
          amt_ExchangeRate_GBL,
          amt_ExchangeRate,
          dim_Currencyid_TRA,
          dim_Currencyid_GBL,
/* Begin 15 Oct 2013 changes */
          ct_GRQty_Late30,
          ct_GRQty_31_60,
          ct_GRQty_61_90,
          ct_GRQty_91_180,
          ct_GIQty_Late30,
          ct_GIQty_31_60,
          ct_GIQty_61_90,
          ct_GIQty_91_180,
/* End 15 Oct 2013 changes */
/* 18.05.2015 */
          amt_gblStdPrice_Merck,
          amt_StdPricePMRA_Merck,
          ct_StockQtyIRU,
          ct_TotalRestrictedStockIRU,
          ct_StockInTransferIRU,
          ct_StockInQInspIRU,
          ct_BlockedStockIRU,
          ct_StockInTransitIRU,
          ct_onhandqtyIRU,
          ct_intransitstockqty,
          ct_globalonhandamt_merck,
          dd_sitecodefore2e,
          dd_sitecodefore2etitle,
          dim_plantidsitefore2e,
          dim_dateidexpirydate,
/* end 18.05.2015 */
          dd_prodordernumber,
          dd_prodorderitemno,
          dim_productionorderstatusid,
          dim_productionordertypeid,
          dim_partsalesid,
          dim_batchid,
          dd_deletionflag,
          dd_stockdeletionflag,
          dd_storagebin,
          dd_inventcorrfactor,
          dd_status,
          amt_grossweight_marm,
		  AMT_ONHAND,
AMT_ONHAND_GBL,
CT_GLOBALEXTTOTALCOST_MERCK,
CT_LOCALEXTTOTALCOST_MERCK,
CT_LOCALONHANDAMT_MERCK,
AMT_PRICEUNIT,
CT_TOTVALSTKQTY,
CT_RESERVEDQTY,
CT_RESERVEDQTYIRU,
CT_CATEGORYQTY,
CT_STOCKDAYSSUPPLY,
AMT_STDCOSTLOC_MERCK,
AMT_VALUEOFVALUATEDSTOCK,
--AMT_IQVPROVISION,
--CT_IQVPROVISION,
CT_IQVFIXED,
CT_ONHANDQTY,
CT_TOTALIQVAMOUNTCCP,
DIM_CUSTOMERID,
DIM_DATEIDTRANSACTION,
DIM_DATEIDACTUALRELEASE,
DIM_DATEIDACTUALSTART,
DD_PLANNEDORDERNO,
DD_STOCKTYPE,
DD_SLOCMRPINDICATOR,
DD_STOCKSTATUS,
DIM_PLANTIDSTOCKATSITE,
DD_AVAILABLEINMINMAX,
DD_IQVRISKDESCRIPTION,
DD_IQVREASONCODE,
DD_IQVREASONDESCRIPTION,
DD_IQVREASONCOMMENT,
DD_IQVRISKCODE,
DD_IQVRISKCATCODE,
DD_IQVRISKCATDESCRIPTION,
/*new cols*/
dd_update_mode,
dd_batch_per_plant,
dd_Base_Unit,
dd_Responsible_Area,
dd_IQV_Action_Code,
dd_IQV_Action_Description,
dd_Comment_IQV_Action,
dd_Last_Changed_by,
dim_First_IQV_Review_Dateid,
dim_Last_IQV_Review_Dateid,
dim_IQV_Due_Dateid,
dim_Risk_Creation_Dateid
/*new cols*/
          )
   SELECT ifnull(ct_POOpenQty, 0),/* Issam change 24th Jun */
          ifnull(ct_SOOpenQty, 0), /* Issam change 24th Jun */
          amt_BlockedStockAmt,
          amt_BlockedStockAmt_GBL,
          amt_CommericalPrice1,
          amt_CurPlannedPrice,
          amt_MovingAvgPrice,
          amt_MtlDlvrCost,
          amt_OtherCost,
          amt_OverheadCost,
          amt_PlannedPrice1,
          amt_PreviousPrice,
          amt_PrevPlannedPrice,
          amt_StdUnitPrice,
          amt_StdUnitPrice_GBL,
          amt_StockInQInspAmt,
          amt_StockInQInspAmt_GBL,
          amt_StockInTransferAmt,
          amt_StockInTransferAmt_GBL,
          amt_StockInTransitAmt,
          amt_StockInTransitAmt_GBL,
          amt_StockValueAmt,
          amt_StockValueAmt_GBL,
          amt_UnrestrictedConsgnStockAmt,
          amt_UnrestrictedConsgnStockAmt_GBL,
          amt_WIPBalance,
    ct_WIPQty,
          ct_BlockedConsgnStock,
          ct_BlockedStock,
          ct_BlockedStockReturns,
          ct_ConsgnStockInQInsp,
          ct_LastReceivedQty,
          ct_RestrictedConsgnStock,
          ct_StockInQInsp,
          ct_StockInTransfer,
          ct_StockInTransit,
          ct_StockQty,
          ct_TotalRestrictedStock,
          ct_UnrestrictedConsgnStock,
          ct_WIPAging,
          ifnull(dd_BatchNo, 'Not Set') as dd_BatchNo,
          ifnull(dd_DocumentItemNo, 0) as dd_DocumentItemNo,
          ifnull(dd_DocumentNo, 'Not Set') as dd_DocumentNo,
          ifnull(dd_MovementType, 'Not Set') as dd_MovementType,
          ifnull(dd_ValuationType, 'Not Set') as dd_ValuationType,
          ifnull(iag.dim_Companyid, 1) as dim_Companyid,
          IFNULL(Dim_ConsumptionTypeid,        1) as Dim_ConsumptionTypeid,   
          IFNULL(dim_costcenterid, 1) as dim_costcenterid,
          IFNULL(dim_Currencyid, 1) as dim_Currencyid,
          IFNULL(Dim_DateIdLastChangedPrice, 1) as Dim_DateIdLastChangedPrice,
          IFNULL(Dim_DateIdPlannedPrice2, 1) as Dim_DateIdPlannedPrice2,
          IFNULL(Dim_DocumentStatusid, 1) as Dim_DocumentStatusid,
          IFNULL(Dim_DocumentTypeid, 1) as Dim_DocumentTypeid,
          IFNULL(Dim_IncoTermid, 1) as Dim_IncoTermid,
          IFNULL(Dim_ItemCategoryid, 1) as Dim_ItemCategoryid,
          IFNULL(Dim_ItemStatusid, 1) as Dim_ItemStatusid,
          IFNULL(dim_LastReceivedDateid, 1) as dim_LastReceivedDateid,
          IFNULL(Dim_MovementIndicatorid, 1) as Dim_MovementIndicatorid,
          IFNULL(dim_Partid, 1) as dim_Partid,
          IFNULL(dim_Plantid, 1) as dim_Plantid,
    Dim_ProfitCenterId,
          ifnull(dim_producthierarchyid, 1) as dim_producthierarchyid,
          ifnull(Dim_PurchaseGroupid, 1)   as Dim_PurchaseGroupid,
          ifnull(Dim_PurchaseMiscid, 1)   as Dim_PurchaseMiscid,
          ifnull(Dim_PurchaseOrgid, 1)  as Dim_PurchaseOrgid,
          ifnull(dim_specialstockid, 1)   as dim_specialstockid,
          ifnull(dim_stockcategoryid, 1)   as dim_stockcategoryid,
          ifnull(dim_StockTypeid, 1)   as dim_StockTypeid,
          ifnull(dim_StorageLocationid, 1)   as dim_StorageLocationid,
          ifnull(dim_StorageLocEntryDateid, 1)   as dim_StorageLocEntryDateid,
          ifnull(Dim_SupplyingPlantId, 1)   as Dim_SupplyingPlantId,
          ifnull(Dim_Termid, 1)   as Dim_Termid,
          ifnull(Dim_UnitOfMeasureid, 1)   as Dim_UnitOfMeasureid,
          ifnull(dim_Vendorid, 1)   as dim_Vendorid,
          ifnull(Dim_VendorMasterid, 1) as Dim_VendorMasterid,
          CURRENT_DATE SnapshotDate,
          convert(bigint,1) Dim_DateidSnapshot,
          (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryhistory') + row_number() over (ORDER BY ''),
    amt_ExchangeRate_GBL,
    amt_ExchangeRate,
    dim_Currencyid_TRA,
    dim_Currencyid_GBL,
/* Begin 15 Oct 2013 changes */
   ct_GRQty_Late30,
   ct_GRQty_31_60,
   ct_GRQty_61_90,
   ct_GRQty_91_180,
   ct_GIQty_Late30,
   ct_GIQty_31_60,
   ct_GIQty_61_90,
   ct_GIQty_91_180,
/* End 15 Oct 2013 changes */
/* 18.05.2015 */
   amt_gblStdPrice_Merck,
   amt_StdPricePMRA_Merck,
   ct_StockQtyIRU,
   ct_TotalRestrictedStockIRU,
   ct_StockInTransferIRU,
   ct_StockInQInspIRU,
   ct_BlockedStockIRU,
   ct_StockInTransitIRU,
   ct_onhandqtyIRU,
   ct_intransitstockqty,
   ct_globalonhandamt_merck,
   ifnull(dd_sitecodefore2e,'Not Set') as dd_sitecodefore2e, 
   ifnull(dd_sitecodefore2etitle,'Not Set') as dd_sitecodefore2etitle,
   ifnull(dim_plantidsitefore2e,1) as dim_plantidsitefore2e,
   ifnull(dim_dateidexpirydate, 1) as dim_dateidexpirydate,
/* end 18.05.2015 */
    ifnull(dd_prodordernumber,'Not Set') as dd_prodordernumber,
    ifnull(dd_prodorderitemno,0) as dd_prodorderitemno,
    ifnull(dim_productionorderstatusid,1) as dim_productionorderstatusid,
    ifnull(dim_productionordertypeid,1) as dim_productionordertypeid,
    ifnull(dim_partsalesid,1) as dim_partsalesid,
    ifnull(dim_batchid,1) as dim_batchid,
    dd_deletionflag,
    dd_stockdeletionflag,
    dd_storagebin,
    dd_inventcorrfactor,
    dd_status,
    amt_grossweight_marm,
	AMT_ONHAND,
AMT_ONHAND_GBL,
CT_GLOBALEXTTOTALCOST_MERCK,
CT_LOCALEXTTOTALCOST_MERCK,
CT_LOCALONHANDAMT_MERCK,
AMT_PRICEUNIT,
CT_TOTVALSTKQTY,
CT_RESERVEDQTY,
CT_RESERVEDQTYIRU,
CT_CATEGORYQTY,
CT_STOCKDAYSSUPPLY,
AMT_STDCOSTLOC_MERCK,
AMT_VALUEOFVALUATEDSTOCK,
--AMT_IQVPROVISION,
--CT_IQVPROVISION,
ifnull(CT_IQVFIXED,0),
CT_ONHANDQTY,
CT_TOTALIQVAMOUNTCCP,
ifnull(DIM_CUSTOMERID, 1) as DIM_CUSTOMERID,
ifnull(DIM_DATEIDTRANSACTION, 1) as DIM_DATEIDTRANSACTION,
ifnull(DIM_DATEIDACTUALRELEASE, 1) as DIM_DATEIDACTUALRELEASE,
ifnull(DIM_DATEIDACTUALSTART, 1) as DIM_DATEIDACTUALSTART,
ifnull(DD_PLANNEDORDERNO, 'Not Set') as DD_PLANNEDORDERNO,
ifnull(DD_STOCKTYPE, 'Not Set') as DD_STOCKTYPE,
ifnull(DD_SLOCMRPINDICATOR, 'Not Set') as DD_SLOCMRPINDICATOR,
ifnull(DD_STOCKSTATUS, 'Not Set') as DD_STOCKSTATUS,
ifnull(DIM_PLANTIDSTOCKATSITE, 'Not Set') AS DIM_PLANTIDSTOCKATSITE,
ifnull(DD_AVAILABLEINMINMAX, 'Not Set') as DD_AVAILABLEINMINMAX,
ifnull(DD_IQVRISKDESCRIPTION, 	'Not Set') as 	DD_IQVRISKDESCRIPTION, 
ifnull(DD_IQVREASONCODE,        'Not Set') as  	DD_IQVREASONCODE,
ifnull(DD_IQVREASONDESCRIPTION, 'Not Set') as  	DD_IQVREASONDESCRIPTION,
ifnull(DD_IQVREASONCOMMENT,     'Not Set') as  	DD_IQVREASONCOMMENT,
ifnull(DD_IQVRISKCODE,          'Not Set') as  	DD_IQVRISKCODE,
ifnull(DD_IQVRISKCATCODE,       'Not Set') as  	DD_IQVRISKCATCODE,
ifnull(DD_IQVRISKCATDESCRIPTION,'Not Set') as  	DD_IQVRISKCATDESCRIPTION,
/*new cols*/
ifnull(dd_update_mode,				'Not Set') as	dd_update_mode,						
ifnull(dd_batch_per_plant,          'Not Set') as   dd_batch_per_plant,         
ifnull(dd_Base_Unit,                'Not Set') as   dd_Base_Unit,               
ifnull(dd_Responsible_Area,         'Not Set') as   dd_Responsible_Area,        
ifnull(dd_IQV_Action_Code,          'Not Set') as   dd_IQV_Action_Code,         
ifnull(dd_IQV_Action_Description,   'Not Set') as   dd_IQV_Action_Description,  
ifnull(dd_Comment_IQV_Action,       'Not Set') as   dd_Comment_IQV_Action,      
ifnull(dd_Last_Changed_by,			'Not Set') as   dd_Last_Changed_by,			
ifnull(dim_First_IQV_Review_Dateid, 1) 		   as   dim_First_IQV_Review_Dateid,
ifnull(dim_Last_IQV_Review_Dateid,  1) 		   as   dim_Last_IQV_Review_Dateid, 
ifnull(dim_IQV_Due_Dateid,          1) 		   as   dim_IQV_Due_Dateid,         
ifnull(dim_Risk_Creation_Dateid,     1) 		   as   dim_Risk_Creation_Dateid  
     FROM    facT_inventoryaging iag
          INNER JOIN
             dim_Company dc
          ON ifnull(iag.dim_companyid, 1) = ifnull(dc.dim_Companyid, 1);


UPDATE fact_inventoryhistory t0
SET t0.Dim_DateidSnapshot = IFNULL(dt.dim_dateid, 1)
FROM
fact_inventoryhistory t0,
dim_Company dc,
dim_date dt
WHERE       t0.dim_companyid = IFNULL(dc.dim_Companyid, 1)
  AND dt.DateValue = current_date
  AND dt.companycode = dc.companycode
  AND dt.plantcode_factory = 'Not Set'
  
  AND t0.Dim_DateidSnapshot = 1;

UPDATE NUMBER_FOUNTAIN
SET max_id =
(
select  ifnull(max(f.fact_inventoryhistoryid),
               ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inventoryhistory f
)
WHERE table_name = 'fact_inventoryhistory';



DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'processinglog', ifnull(max(processinglogid), 0) FROM processinglog;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          CURRENT_DATE,
          'Start of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

delete from fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_StorageLocationid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock,
         /* 18.05.2015 */
                                      ct_StockQtyIRU,
              ct_TotalRestrictedStockIRU,
              ct_StockInTransferIRU,
              ct_StockInQInspIRU,
              ct_BlockedStockIRU,
              ct_StockInTransitIRU,
              ct_onhandqtyIRU,
              ct_intransitstockqty
        /* end 18.05.2015 */
)
   SELECT ifnull(dim_Partid, 1) as dim_Partid,
          ifnull(dim_Plantid, 1) as dim_Plantid,
          ifnull(dim_StorageLocationid, 1) as dim_StorageLocationid,
          ifnull(dim_stockcategoryid, 1) as dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock),
/* 18.05.2015 */
          SUM(ct_StockQtyIRU),
    SUM(ct_TotalRestrictedStockIRU),
    SUM(ct_StockInTransferIRU),
    SUM(ct_StockInQInspIRU),
          SUM(ct_BlockedStockIRU),
    SUM(ct_StockInTransitIRU),
    SUM(ct_onhandqtyIRU),
    SUM(ct_intransitstockqty)
/* end 18.05.2015 */
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid IN (2, 3)
          AND SnapshotDate IN
                 (CURRENT_DATE,
                  (CURRENT_DATE - 1),
                  (CURRENT_DATE - 7),
                  CURRENT_DATE - (INTERVAL '1' MONTH),
                  CURRENT_DATE - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_StorageLocationid,
            dim_stockcategoryid,
            SnapshotDate;



UPDATE    fact_inventoryhistory ih1
  SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1DayChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1DayChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1DayChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1DayChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1DayChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1DayChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1DayChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1DayChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
and ih1.SnapshotDate = CURRENT_DATE
AND ih1.dim_stockcategoryid IN (2, 3)
and ih1.SnapshotDate - (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1WeekChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1WeekChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1WeekChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1WeekChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1WeekChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1WeekChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1WeekChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1WeekChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.SnapshotDate = CURRENT_DATE
AND ih1.dim_stockcategoryid IN (2, 3)
AND ih1.SnapshotDate - ( INTERVAL '7' DAY) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1MonthChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1MonthChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1MonthChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1MonthChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1MonthChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1MonthChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1MonthChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1MonthChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.SnapshotDate = CURRENT_DATE
AND ih1.dim_stockcategoryid IN (2, 3)
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
 /* 18.05.2015 */
        ih1.ct_StockQtyIRU_1QuarterChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1QuarterChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1QuarterChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1QuarterChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1QuarterChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1QuarterChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1QuarterChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1QuarterChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.SnapshotDate = CURRENT_DATE
AND ih1.dim_stockcategoryid IN (2, 3)
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange,
 /* 18.05.2015 */
       ih1.ct_StockQtyIRU_1DayChange = ih2.ct_StockQtyIRU_1DayChange,
       ih1.ct_TotalRestrictedStockIRU_1DayChange = ih2.ct_TotalRestrictedStockIRU_1DayChange,
       ih1.ct_StockInTransferIRU_1DayChange = ih2.ct_StockInTransferIRU_1DayChange,
       ih1.ct_StockInQInspIRU_1DayChange = ih2.ct_StockInQInspIRU_1DayChange,
       ih1.ct_BlockedStockIRU_1DayChange = ih2.ct_BlockedStockIRU_1DayChange,
       ih1.ct_StockInTransitIRU_1DayChange = ih2.ct_StockInTransitIRU_1DayChange,
       ih1.ct_onhandqtyIRU_1DayChange = ih2.ct_onhandqtyIRU_1DayChange,
       ih1.ct_intransitstockqty_1DayChange = ih2.ct_intransitstockqty_1DayChange,
       ih1.ct_StockQtyIRU_1WeekChange = ih2.ct_StockQtyIRU_1WeekChange,
       ih1.ct_TotalRestrictedStockIRU_1WeekChange = ih2.ct_TotalRestrictedStockIRU_1WeekChange,
       ih1.ct_StockInTransferIRU_1WeekChange = ih2.ct_StockInTransferIRU_1WeekChange,
       ih1.ct_StockInQInspIRU_1WeekChange = ih2.ct_StockInQInspIRU_1WeekChange,
       ih1.ct_BlockedStockIRU_1WeekChange = ih2.ct_BlockedStockIRU_1WeekChange,
       ih1.ct_StockInTransitIRU_1WeekChange = ih2.ct_StockInTransitIRU_1WeekChange,
       ih1.ct_onhandqtyIRU_1WeekChange = ih2.ct_onhandqtyIRU_1WeekChange,
       ih1.ct_intransitstockqty_1WeekChange = ih2.ct_intransitstockqty_1WeekChange,
       ih1.ct_StockQtyIRU_1MonthChange = ih2.ct_StockQtyIRU_1MonthChange,
       ih1.ct_TotalRestrictedStockIRU_1MonthChange = ih2.ct_TotalRestrictedStockIRU_1MonthChange,
       ih1.ct_StockInTransferIRU_1MonthChange = ih2.ct_StockInTransferIRU_1MonthChange,
       ih1.ct_StockInQInspIRU_1MonthChange = ih2.ct_StockInQInspIRU_1MonthChange,
       ih1.ct_BlockedStockIRU_1MonthChange = ih2.ct_BlockedStockIRU_1MonthChange,
       ih1.ct_StockInTransitIRU_1MonthChange = ih2.ct_StockInTransitIRU_1MonthChange,
       ih1.ct_onhandqtyIRU_1MonthChange = ih2.ct_onhandqtyIRU_1MonthChange,
       ih1.ct_intransitstockqty_1MonthChange = ih2.ct_intransitstockqty_1MonthChange,
       ih1.ct_StockQtyIRU_1QuarterChange = ih2.ct_StockQtyIRU_1QuarterChange,
       ih1.ct_TotalRestrictedStockIRU_1QuarterChange = ih2.ct_TotalRestrictedStockIRU_1QuarterChange,
       ih1.ct_StockInTransferIRU_1QuarterChange = ih2.ct_StockInTransferIRU_1QuarterChange,
       ih1.ct_StockInQInspIRU_1QuarterChange = ih2.ct_StockInQInspIRU_1QuarterChange,
       ih1.ct_BlockedStockIRU_1QuarterChange = ih2.ct_BlockedStockIRU_1QuarterChange,
       ih1.ct_StockInTransitIRU_1QuarterChange = ih2.ct_StockInTransitIRU_1QuarterChange,
       ih1.ct_onhandqtyIRU_1QuarterChange = ih2.ct_onhandqtyIRU_1QuarterChange,
       ih1.ct_intransitstockqty_1QuarterChange = ih2.ct_intransitstockqty_1QuarterChange
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid in (2,3)
AND ih1.SnapshotDate = CURRENT_DATE;

/* DELETE FROM fact_inventoryhistory_tmp */

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_Vendorid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock,
                   /* 18.05.2015 */
                          ct_StockQtyIRU,
                      ct_TotalRestrictedStockIRU,
                        ct_StockInTransferIRU,
                          ct_StockInQInspIRU,
                          ct_BlockedStockIRU,
                        ct_StockInTransitIRU,
                        ct_onhandqtyIRU,
                      ct_intransitstockqty
                      /* end 18.05.2015 */)
   SELECT ifnull(dim_Partid, 1) as dim_Partid,
          ifnull(dim_Plantid, 1)          as dim_Plantid,
          ifnull(dim_Vendorid, 1)as dim_Vendorid,
          ifnull(dim_stockcategoryid, 1) as dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock),
/* 18.05.2015 */
          SUM(ct_StockQtyIRU),
    SUM(ct_TotalRestrictedStockIRU),
    SUM(ct_StockInTransferIRU),
    SUM(ct_StockInQInspIRU),
          SUM(ct_BlockedStockIRU),
    SUM(ct_StockInTransitIRU),
    SUM(ct_onhandqtyIRU),
    SUM(ct_intransitstockqty)
/* end 18.05.2015 */
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 4
          AND SnapshotDate IN
                 (CURRENT_DATE,
                  CURRENT_DATE - (INTERVAL '1' DAY),
                  CURRENT_DATE - (INTERVAL '7' DAY),
                  CURRENT_DATE - (INTERVAL '1' MONTH),
                  CURRENT_DATE - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_Vendorid,
            dim_stockcategoryid,
            SnapshotDate;

UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1DayChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1DayChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1DayChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1DayChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1DayChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1DayChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1DayChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1DayChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1DayChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
          fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.SnapshotDate = CURRENT_DATE
AND ifnull(ih1.dim_Vendorid, 1) = ifnull(ih2.dim_Vendorid, 1)
AND ih1.dim_stockcategoryid = 4
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
        ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1WeekChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1WeekChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1WeekChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1WeekChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1WeekChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1WeekChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1WeekChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1WeekChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ifnull(ih1.dim_Vendorid, 1) = ifnull(ih2.dim_Vendorid, 1)
AND ih1.dim_stockcategoryid = 4
AND ih1.SnapshotDate = CURRENT_DATE
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;


UPDATE fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1MonthChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1MonthChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1MonthChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1MonthChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1MonthChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1MonthChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1MonthChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1MonthChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory ih1, fact_inventoryhistory_tmp ih2
WHERE  ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Vendorid, 1) = ifnull(ih2.dim_Vendorid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.dim_stockcategoryid = 4
AND ih1.SnapshotDate = CURRENT_DATE
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
  SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1QuarterChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1QuarterChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1QuarterChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1QuarterChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1QuarterChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1QuarterChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1QuarterChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1QuarterChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory ih1,fact_inventoryhistory_tmp ih2
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ifnull(ih1.dim_Vendorid, 1) = ifnull(ih2.dim_Vendorid, 1)
AND ih1.dim_stockcategoryid = 4
and ih1.SnapshotDate = CURRENT_DATE
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange,
 /* 18.05.2015 */
       ih1.ct_StockQtyIRU_1DayChange = ih2.ct_StockQtyIRU_1DayChange,
       ih1.ct_TotalRestrictedStockIRU_1DayChange = ih2.ct_TotalRestrictedStockIRU_1DayChange,
       ih1.ct_StockInTransferIRU_1DayChange = ih2.ct_StockInTransferIRU_1DayChange,
       ih1.ct_StockInQInspIRU_1DayChange = ih2.ct_StockInQInspIRU_1DayChange,
       ih1.ct_BlockedStockIRU_1DayChange = ih2.ct_BlockedStockIRU_1DayChange,
       ih1.ct_StockInTransitIRU_1DayChange = ih2.ct_StockInTransitIRU_1DayChange,
       ih1.ct_onhandqtyIRU_1DayChange = ih2.ct_onhandqtyIRU_1DayChange,
       ih1.ct_intransitstockqty_1DayChange = ih2.ct_intransitstockqty_1DayChange,
       ih1.ct_StockQtyIRU_1WeekChange = ih2.ct_StockQtyIRU_1WeekChange,
       ih1.ct_TotalRestrictedStockIRU_1WeekChange = ih2.ct_TotalRestrictedStockIRU_1WeekChange,
       ih1.ct_StockInTransferIRU_1WeekChange = ih2.ct_StockInTransferIRU_1WeekChange,
       ih1.ct_StockInQInspIRU_1WeekChange = ih2.ct_StockInQInspIRU_1WeekChange,
       ih1.ct_BlockedStockIRU_1WeekChange = ih2.ct_BlockedStockIRU_1WeekChange,
       ih1.ct_StockInTransitIRU_1WeekChange = ih2.ct_StockInTransitIRU_1WeekChange,
       ih1.ct_onhandqtyIRU_1WeekChange = ih2.ct_onhandqtyIRU_1WeekChange,
       ih1.ct_intransitstockqty_1WeekChange = ih2.ct_intransitstockqty_1WeekChange,
       ih1.ct_StockQtyIRU_1MonthChange = ih2.ct_StockQtyIRU_1MonthChange,
       ih1.ct_TotalRestrictedStockIRU_1MonthChange = ih2.ct_TotalRestrictedStockIRU_1MonthChange,
       ih1.ct_StockInTransferIRU_1MonthChange = ih2.ct_StockInTransferIRU_1MonthChange,
       ih1.ct_StockInQInspIRU_1MonthChange = ih2.ct_StockInQInspIRU_1MonthChange,
       ih1.ct_BlockedStockIRU_1MonthChange = ih2.ct_BlockedStockIRU_1MonthChange,
       ih1.ct_StockInTransitIRU_1MonthChange = ih2.ct_StockInTransitIRU_1MonthChange,
       ih1.ct_onhandqtyIRU_1MonthChange = ih2.ct_onhandqtyIRU_1MonthChange,
       ih1.ct_intransitstockqty_1MonthChange = ih2.ct_intransitstockqty_1MonthChange,
       ih1.ct_StockQtyIRU_1QuarterChange = ih2.ct_StockQtyIRU_1QuarterChange,
       ih1.ct_TotalRestrictedStockIRU_1QuarterChange = ih2.ct_TotalRestrictedStockIRU_1QuarterChange,
       ih1.ct_StockInTransferIRU_1QuarterChange = ih2.ct_StockInTransferIRU_1QuarterChange,
       ih1.ct_StockInQInspIRU_1QuarterChange = ih2.ct_StockInQInspIRU_1QuarterChange,
       ih1.ct_BlockedStockIRU_1QuarterChange = ih2.ct_BlockedStockIRU_1QuarterChange,
       ih1.ct_StockInTransitIRU_1QuarterChange = ih2.ct_StockInTransitIRU_1QuarterChange,
       ih1.ct_onhandqtyIRU_1QuarterChange = ih2.ct_onhandqtyIRU_1QuarterChange,
       ih1.ct_intransitstockqty_1QuarterChange = ih2.ct_intransitstockqty_1QuarterChange
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM   fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_Storagelocationid, 1) = ifnull(ih2.dim_storagelocationid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ifnull(ih1.dim_Vendorid, 1) = ifnull(ih2.dim_Vendorid, 1)
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.dim_stockcategoryid = 4
AND ih1.SnapshotDate = CURRENT_DATE;

DELETE FROM fact_inventoryhistory_tmp;

INSERT INTO fact_inventoryhistory_tmp(dim_Partid,
                                      dim_Plantid,
                                      dim_stockcategoryid,
                                      SnapshotDate,
                                      amt_BlockedStockAmt,
                                      amt_StockInQInspAmt,
                                      amt_StockInTransferAmt,
                                      amt_StockValueAmt,
                                      ct_BlockedStock,
                                      ct_StockInQInsp,
                                      ct_StockInTransfer,
                                      ct_StockInTransit,
                                      ct_StockQty,
                                      ct_TotalRestrictedStock,
                    /* 18.05.2015 */
                          ct_StockQtyIRU,
                      ct_TotalRestrictedStockIRU,
                        ct_StockInTransferIRU,
                          ct_StockInQInspIRU,
                          ct_BlockedStockIRU,
                        ct_StockInTransitIRU,
                        ct_onhandqtyIRU,
                      ct_intransitstockqty
                      /* end 18.05.2015 */)
   SELECT ifnull(dim_Partid, 1) as dim_Partid,
          ifnull(dim_Plantid, 1) as dim_Plantid,
          ifnull(dim_stockcategoryid, 1) as dim_stockcategoryid,
          SnapshotDate,
          SUM(amt_BlockedStockAmt),
          SUM(amt_StockInQInspAmt),
          SUM(amt_StockInTransferAmt),
          SUM(amt_StockValueAmt),
          SUM(ct_BlockedStock),
          SUM(ct_StockInQInsp),
          SUM(ct_StockInTransfer),
          SUM(ct_StockInTransit),
          SUM(ct_StockQty),
          SUM(ct_TotalRestrictedStock),
/* 18.05.2015 */
          SUM(ct_StockQtyIRU),
    SUM(ct_TotalRestrictedStockIRU),
    SUM(ct_StockInTransferIRU),
    SUM(ct_StockInQInspIRU),
          SUM(ct_BlockedStockIRU),
    SUM(ct_StockInTransitIRU),
    SUM(ct_onhandqtyIRU),
    SUM(ct_intransitstockqty)
/* end 18.05.2015 */
     FROM fact_inventoryhistory
    WHERE dim_stockcategoryid = 5
          AND SnapshotDate IN
                 (CURRENT_DATE,
                  CURRENT_DATE - (INTERVAL '1' DAY),
                  CURRENT_DATE - (INTERVAL '7' DAY),
                  CURRENT_DATE - (INTERVAL '1' MONTH),
                  CURRENT_DATE - (INTERVAL '3' MONTH))
   GROUP BY dim_Partid,
            dim_Plantid,
            dim_stockcategoryid,
            SnapshotDate;


UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1DayChange =
          ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1DayChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1DayChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1DayChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1DayChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1DayChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1DayChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1DayChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1DayChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1DayChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1DayChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1DayChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1DayChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1DayChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1DayChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1DayChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1DayChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1DayChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1DayChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ih1.SnapshotDate = CURRENT_DATE
       AND     ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.dim_stockcategoryid = 5
AND ih1.SnapshotDate -  (INTERVAL '1' DAY) = ih2.SnapshotDate;


UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1WeekChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1WeekChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1WeekChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1WeekChange = ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1WeekChange =
          ih1.ct_TotalRestrictedStock - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1WeekChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1WeekChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1WeekChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
        ih1.amt_StockInTransitAmt_1WeekChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1WeekChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1WeekChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1WeekChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1WeekChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1WeekChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1WeekChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1WeekChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1WeekChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1WeekChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1WeekChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ih1.SnapshotDate = CURRENT_DATE
AND     ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.dim_stockcategoryid = 5
AND ih1.SnapshotDate -  (INTERVAL '7' DAY) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1MonthChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1MonthChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1MonthChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1MonthChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1MonthChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1MonthChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1MonthChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1MonthChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1MonthChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1MonthChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1MonthChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1MonthChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1MonthChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1MonthChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1MonthChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1MonthChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1MonthChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1MonthChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1MonthChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ih1.SnapshotDate = CURRENT_DATE
AND     ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.dim_stockcategoryid = 5
AND ih1.SnapshotDate -  (INTERVAL '1' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1QuarterChange = ih1.ct_StockInQInsp - ih2.ct_StockInQInsp,
       ih1.ct_StockInTransfer_1QuarterChange =
          ih1.ct_StockInTransfer - ih2.ct_StockInTransfer,
       ih1.ct_StockInTransit_1QuarterChange =
          ih1.ct_StockInTransit - ih2.ct_StockInTransit,
       ih1.ct_StockQty_1QuarterChange =
          ih1.ct_StockQty - ih2.ct_StockQty,
       ih1.ct_TotalRestrictedStock_1QuarterChange =
          ih1.ct_TotalRestrictedStock
          - ih2.ct_TotalRestrictedStock,
       ih1.ct_BlockedStock_1QuarterChange =
          ih1.ct_BlockedStock - ih2.ct_BlockedStock,
       ih1.amt_StockValueAmt_1QuarterChange =
          ih1.amt_StockValueAmt - ih2.amt_StockValueAmt,
       ih1.amt_StockInTransferAmt_1QuarterChange =
          ih1.amt_StockInTransferAmt - ih2.amt_StockInTransferAmt,
       ih1.amt_StockInTransitAmt_1QuarterChange =
          ih1.amt_StockInTransitAmt - ih2.amt_StockInTransitAmt,
       ih1.amt_StockInQInspAmt_1QuarterChange =
          ih1.amt_StockInQInspAmt - ih2.amt_StockInQInspAmt,
       ih1.amt_BlockedStockAmt_1QuarterChange =
          ih1.amt_BlockedStockAmt - ih2.amt_BlockedStockAmt,
/* 18.05.2015 */
        ih1.ct_StockQtyIRU_1QuarterChange = ih1.ct_StockQtyIRU - ih2.ct_StockQtyIRU,
  ih1.ct_TotalRestrictedStockIRU_1QuarterChange = ih1.ct_TotalRestrictedStockIRU - ih2.ct_TotalRestrictedStockIRU,
  ih1.ct_StockInTransferIRU_1QuarterChange = ih1.ct_StockInTransferIRU - ih2.ct_StockInTransferIRU,
  ih1.ct_StockInQInspIRU_1QuarterChange = ih1.ct_StockInQInspIRU - ih2.ct_StockInQInspIRU,
  ih1.ct_BlockedStockIRU_1QuarterChange = ih1.ct_BlockedStockIRU - ih2.ct_BlockedStockIRU,
  ih1.ct_StockInTransitIRU_1QuarterChange = ih1.ct_StockInTransitIRU - ih2.ct_StockInTransitIRU,
  ih1.ct_onhandqtyIRU_1QuarterChange = ih1.ct_onhandqtyIRU - ih2.ct_onhandqtyIRU,
  ih1.ct_intransitstockqty_1QuarterChange = ih1.ct_intransitstockqty - ih2.ct_intransitstockqty
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ih1.SnapshotDate = CURRENT_DATE
AND     ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.dim_stockcategoryid = 5
AND ih1.SnapshotDate -  (INTERVAL '3' MONTH) = ih2.SnapshotDate;

UPDATE    fact_inventoryhistory ih1
   SET ih1.ct_StockInQInsp_1DayChange = ih2.ct_StockInQInsp_1DayChange,
       ih1.ct_StockInTransfer_1DayChange = ih2.ct_StockInTransfer_1DayChange,
       ih1.ct_StockInTransit_1DayChange = ih2.ct_StockInTransit_1DayChange ,
       ih1.ct_StockQty_1DayChange = ih2.ct_StockQty_1DayChange,
       ih1.ct_TotalRestrictedStock_1DayChange = ih2.ct_TotalRestrictedStock_1DayChange,
       ih1.ct_BlockedStock_1DayChange = ih2.ct_BlockedStock_1DayChange,
       ih1.amt_StockValueAmt_1DayChange = ih2.amt_StockValueAmt_1DayChange,
       ih1.amt_StockInTransferAmt_1DayChange = ih2.amt_StockInTransferAmt_1DayChange,
       ih1.amt_StockInTransitAmt_1DayChange = ih2.amt_StockInTransitAmt_1DayChange,
       ih1.amt_StockInQInspAmt_1DayChange = ih2.amt_StockInQInspAmt_1DayChange,
       ih1.amt_BlockedStockAmt_1DayChange = ih2.amt_BlockedStockAmt_1DayChange,
       ih1.ct_StockInQInsp_1WeekChange = ih2.ct_StockInQInsp_1WeekChange,
       ih1.ct_StockInTransfer_1WeekChange = ih2.ct_StockInTransfer_1WeekChange,
       ih1.ct_StockInTransit_1WeekChange = ih2.ct_StockInTransit_1WeekChange,
       ih1.ct_StockQty_1WeekChange = ih2.ct_StockQty_1WeekChange,
       ih1.ct_TotalRestrictedStock_1WeekChange = ih2.ct_TotalRestrictedStock_1WeekChange,
       ih1.ct_BlockedStock_1WeekChange = ih2.ct_BlockedStock_1WeekChange,
       ih1.amt_StockValueAmt_1WeekChange = ih2.amt_StockValueAmt_1WeekChange,
       ih1.amt_StockInTransferAmt_1WeekChange = ih2.amt_StockInTransferAmt_1WeekChange,
       ih1.amt_StockInTransitAmt_1WeekChange = ih2.amt_StockInTransitAmt_1WeekChange,
       ih1.amt_StockInQInspAmt_1WeekChange = ih2.amt_StockInQInspAmt_1WeekChange,
       ih1.amt_BlockedStockAmt_1WeekChange = ih2.amt_BlockedStockAmt_1WeekChange,
       ih1.ct_StockInQInsp_1MonthChange = ih2.ct_StockInQInsp_1MonthChange,
       ih1.ct_StockInTransfer_1MonthChange = ih2.ct_StockInTransfer_1MonthChange,
       ih1.ct_StockInTransit_1MonthChange = ih2.ct_StockInTransit_1MonthChange,
       ih1.ct_StockQty_1MonthChange = ih2.ct_StockQty_1MonthChange,
       ih1.ct_TotalRestrictedStock_1MonthChange = ih2.ct_TotalRestrictedStock_1MonthChange,
       ih1.ct_BlockedStock_1MonthChange = ih2.ct_BlockedStock_1MonthChange,
       ih1.amt_StockValueAmt_1MonthChange = ih2.amt_StockValueAmt_1MonthChange,
       ih1.amt_StockInTransferAmt_1MonthChange = ih2.amt_StockInTransferAmt_1MonthChange,
       ih1.amt_StockInTransitAmt_1MonthChange = ih2.amt_StockInTransitAmt_1MonthChange,
       ih1.amt_StockInQInspAmt_1MonthChange = ih2.amt_StockInQInspAmt_1MonthChange,
       ih1.amt_BlockedStockAmt_1MonthChange = ih2.amt_BlockedStockAmt_1MonthChange,
       ih1.ct_StockInQInsp_1QuarterChange = ih2.ct_StockInQInsp_1QuarterChange,
       ih1.ct_StockInTransfer_1QuarterChange = ih2.ct_StockInTransfer_1QuarterChange,
       ih1.ct_StockInTransit_1QuarterChange = ih2.ct_StockInTransit_1QuarterChange,
       ih1.ct_StockQty_1QuarterChange = ih2.ct_StockQty_1QuarterChange,
       ih1.ct_TotalRestrictedStock_1QuarterChange = ih2.ct_TotalRestrictedStock_1QuarterChange,
       ih1.ct_BlockedStock_1QuarterChange = ih2.ct_BlockedStock_1QuarterChange,
       ih1.amt_StockValueAmt_1QuarterChange = ih2.amt_StockValueAmt_1QuarterChange,
       ih1.amt_StockInTransferAmt_1QuarterChange = ih2.amt_StockInTransferAmt_1QuarterChange,
       ih1.amt_StockInTransitAmt_1QuarterChange = ih2.amt_StockInTransitAmt_1QuarterChange,
       ih1.amt_StockInQInspAmt_1QuarterChange = ih2.amt_StockInQInspAmt_1QuarterChange,
       ih1.amt_BlockedStockAmt_1QuarterChange = ih2.amt_BlockedStockAmt_1QuarterChange,
/* 18.05.2015 */
       ih1.ct_StockQtyIRU_1DayChange = ih2.ct_StockQtyIRU_1DayChange,
       ih1.ct_TotalRestrictedStockIRU_1DayChange = ih2.ct_TotalRestrictedStockIRU_1DayChange,
       ih1.ct_StockInTransferIRU_1DayChange = ih2.ct_StockInTransferIRU_1DayChange,
       ih1.ct_StockInQInspIRU_1DayChange = ih2.ct_StockInQInspIRU_1DayChange,
       ih1.ct_BlockedStockIRU_1DayChange = ih2.ct_BlockedStockIRU_1DayChange,
       ih1.ct_StockInTransitIRU_1DayChange = ih2.ct_StockInTransitIRU_1DayChange,
       ih1.ct_onhandqtyIRU_1DayChange = ih2.ct_onhandqtyIRU_1DayChange,
       ih1.ct_intransitstockqty_1DayChange = ih2.ct_intransitstockqty_1DayChange,
       ih1.ct_StockQtyIRU_1WeekChange = ih2.ct_StockQtyIRU_1WeekChange,
       ih1.ct_TotalRestrictedStockIRU_1WeekChange = ih2.ct_TotalRestrictedStockIRU_1WeekChange,
       ih1.ct_StockInTransferIRU_1WeekChange = ih2.ct_StockInTransferIRU_1WeekChange,
       ih1.ct_StockInQInspIRU_1WeekChange = ih2.ct_StockInQInspIRU_1WeekChange,
       ih1.ct_BlockedStockIRU_1WeekChange = ih2.ct_BlockedStockIRU_1WeekChange,
       ih1.ct_StockInTransitIRU_1WeekChange = ih2.ct_StockInTransitIRU_1WeekChange,
       ih1.ct_onhandqtyIRU_1WeekChange = ih2.ct_onhandqtyIRU_1WeekChange,
       ih1.ct_intransitstockqty_1WeekChange = ih2.ct_intransitstockqty_1WeekChange,
       ih1.ct_StockQtyIRU_1MonthChange = ih2.ct_StockQtyIRU_1MonthChange,
       ih1.ct_TotalRestrictedStockIRU_1MonthChange = ih2.ct_TotalRestrictedStockIRU_1MonthChange,
       ih1.ct_StockInTransferIRU_1MonthChange = ih2.ct_StockInTransferIRU_1MonthChange,
       ih1.ct_StockInQInspIRU_1MonthChange = ih2.ct_StockInQInspIRU_1MonthChange,
       ih1.ct_BlockedStockIRU_1MonthChange = ih2.ct_BlockedStockIRU_1MonthChange,
       ih1.ct_StockInTransitIRU_1MonthChange = ih2.ct_StockInTransitIRU_1MonthChange,
       ih1.ct_onhandqtyIRU_1MonthChange = ih2.ct_onhandqtyIRU_1MonthChange,
       ih1.ct_intransitstockqty_1MonthChange = ih2.ct_intransitstockqty_1MonthChange,
       ih1.ct_StockQtyIRU_1QuarterChange = ih2.ct_StockQtyIRU_1QuarterChange,
       ih1.ct_TotalRestrictedStockIRU_1QuarterChange = ih2.ct_TotalRestrictedStockIRU_1QuarterChange,
       ih1.ct_StockInTransferIRU_1QuarterChange = ih2.ct_StockInTransferIRU_1QuarterChange,
       ih1.ct_StockInQInspIRU_1QuarterChange = ih2.ct_StockInQInspIRU_1QuarterChange,
       ih1.ct_BlockedStockIRU_1QuarterChange = ih2.ct_BlockedStockIRU_1QuarterChange,
       ih1.ct_StockInTransitIRU_1QuarterChange = ih2.ct_StockInTransitIRU_1QuarterChange,
       ih1.ct_onhandqtyIRU_1QuarterChange = ih2.ct_onhandqtyIRU_1QuarterChange,
       ih1.ct_intransitstockqty_1QuarterChange = ih2.ct_intransitstockqty_1QuarterChange
 /* end 18.05.2015 */
  ,ih1.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inventoryhistory_tmp ih2, fact_inventoryhistory ih1
WHERE ih1.dim_stockcategoryid = 5
AND ifnull(ih1.dim_partid, 1) = ifnull(ih2.dim_partid, 1)
AND ifnull(ih1.dim_plantid, 1) = ifnull(ih2.dim_plantid, 1)
AND ifnull(ih1.dim_stockcategoryid, 1) = ifnull(ih2.dim_stockcategoryid, 1)
AND ih1.SnapshotDate = ih2.Snapshotdate
AND ih1.SnapshotDate = CURRENT_DATE;

INSERT INTO processinglog(processinglogid,
                          referencename,
                          startdate,
                          description)
   SELECT max_id + 1,
          'bi_process_trends_inventoryhistory_fact',
          current_date,
          'End of proc'
     FROM NUMBER_FOUNTAIN
    WHERE table_name = 'processinglog';

UPDATE NUMBER_FOUNTAIN
   SET max_id = max_id + 1
 WHERE table_name = 'processinglog';

truncate table fact_inventoryhistory_tmp;

/* Begin 15 Oct 2013 changes */
/* Begin 20 Dec 2013 changes */
UPDATE fact_inventoryhistory
SET ct_GRQty_Late30 = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_GRQty_Late30 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_31_60 = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_GRQty_31_60 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_61_90 = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_GRQty_61_90 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GRQty_91_180 = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_GRQty_91_180 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_Late30 = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_GIQty_Late30 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_31_60 = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_GIQty_31_60 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_61_90 = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_GIQty_61_90 IS NULL;

UPDATE fact_inventoryhistory
SET ct_GIQty_91_180 = 0
  ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE ct_GIQty_91_180 IS NULL;

/* Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */

UPDATE fact_inventoryhistory am

SET am.std_exchangerate_dateid = ifnull(dt.dim_dateid, 1)
FROM fact_inventoryhistory am
INNER JOIN dim_plant p ON ifnull(am.dim_plantid, 1) = ifnull(p.dim_plantid, 1)
INNER JOIN dim_date dt ON dt.companycode = p.companycode AND p.plantcode = dt.plantcode_factory
WHERE dt.datevalue = current_date
AND   am.std_exchangerate_dateid <> ifnull(dt.dim_dateid, 1);

/*END Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */

/* Octavian: EA changes */
update fact_inventoryhistory f_ih
set f_ih.dim_batchid = ifnull(b.dim_batchid,1),
f_ih.dw_update_date = current_timestamp
from dim_batch b, dim_part dp, fact_inventoryhistory f_ih
where ifnull(f_ih.dim_partid, 1) = ifnull(dp.dim_partid, 1) and
ifnull(f_ih.dd_batchno, 'Not Set') = ifnull(b.batchnumber, 'Not Set') and
ifnull(dp.partnumber, 'Not Set') = ifnull(b.partnumber, 'Not Set')
and ifnull(dp.plant, 'Not Set') = ifnull(b.plantcode, 'Not Set')
and f_ih.SnapshotDate = CURRENT_DATE
and f_ih.dim_batchid <> ifnull(b.dim_batchid, 1);
/* Octavian: EA changes */

/*Georgiana According to App-6119*/

update fact_inventoryhistory
set dd_monthlyflag=case when datevalue like '%-%-01' then 'Yes' Else 'No' end ,
dd_quaterlyflag=case when datevalue like '%-01-01' or datevalue like '%-04-01' or datevalue like '%-07-01' or datevalue like '%-10-01'    then 'Yes' Else 'No' end,
dd_yearlyflag = case when datevalue like '%-01-01' then 'Yes' else 'No' end 
 from fact_inventoryhistory, dim_date where ifnull(dim_dateidsnapshot, 1) = ifnull(dim_dateid, 1);