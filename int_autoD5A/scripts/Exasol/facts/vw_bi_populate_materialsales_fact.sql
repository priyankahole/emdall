/* ################################################################################################################## */
/* */
/*   Author         : Alin */
/*   Created On     : 28 Sept 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   28 Sept 2016      Alin		1.0  		  First draft */

/******************************************************************************************************************/


delete from number_fountain m where m.table_name = 'fact_materialsales';
insert into number_fountain
select 'fact_materialsales',
ifnull(max(f.fact_materialsalesid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_materialsales f;

DELETE FROM fact_materialsales f
WHERE EXISTS (
select 1 from fact_materialsales f, dim_countrytaxclassif ctx
where CTX.SalesOrganization = DD_MATERIALNUMBER 
AND CTX.DistributionChannel = DD_SALESORGANIZATION
AND CTX.MaterialNumber = DD_DISTRIBUTIONCHANNEL
);

insert into fact_materialsales
(
FACT_MATERIALSALESID,
DD_MATERIALNUMBER,
DIM_PARTID,
DD_SALESORGANIZATION,
DIM_SALESORGID,	
DD_DISTRIBUTIONCHANNEL,
DIM_DISTRIBUTIONCHANNELID,
DD_MATERIALSTATUS,
DD_materialstatusdescription,
DIM_SDMATERIALSTATUSID, 	
DIM_DATEIDMAT, 	
CT_MINORDERINBASEUOM,
CT_MINDELIVERNOTEPROCESSING,
CT_DELIVERYUNIT,
DIM_UNITOFMEASUREID,
DIM_UOMIDSALESUNIT,
DIM_UOMIDDELIVUNIT,
DD_ITEMCATGROUP,				
DIM_PLANTIDMAT, 				
DIM_PRODUCTHIERARCHYID, 
DD_ACCOUNTASSIGNMGR,
DD_ACCOUNTASSIGNMGRCODE,
dim_materialpricinggroupid,
DIM_MATERIALPRICEGROUP1ID, 	
DIM_MATERIALPRICEGROUP2ID, 	
DIM_MATERIALPRICEGROUP3ID, 	
DIM_MATERIALPRICEGROUP4ID, 	
DIM_MATERIALPRICEGROUP5ID,
dim_countrytaxclassifid,
DD_ROUNDINGPROFILE,
dw_insert_date,
DW_UPDATE_DATE)
SELECT
(select max_id   from number_fountain   where table_name = 'fact_materialsales') + row_number() over(order by '') AS fact_materialsalesid,
IFNULL(MV.MVKE_MATNR, 'Not Set'),
1 as dim_partid,
IFNULL(MV.MVKE_VKORG, 'Not Set'),
1 as dim_salesorgid,
IFNULL(MV.MVKE_VTWEG, 'Not Set'),
1 as dim_distributionchannelid,
IFNULL(MV.MVKE_VMSTA, 'Not Set'),
'Not Set' as DD_materialstatusdescription,
1 as dim_sdmaterialstatusid,
1 as dim_dateidmat,
IFNULL(MV.MVKE_AUMNG, 0),
IFNULL(MV.MVKE_LFMNG, 0),
IFNULL(MV.MVKE_SCMNG, 0),
1 as dim_unitofmeasureid,
1 AS DIM_UOMIDSALESUNIT,
1 AS DIM_UOMIDDELIVUNIT,
IFNULL(MV.MVKE_MTPOS, 'Not Set'),
1 as dim_plantidmat,				
1 as dim_producthierarchyid,	
'Not Set' as DD_ACCOUNTASSIGNMGR,
'Not Set' AS DD_ACCOUNTASSIGNMGRCODE,
1 as dim_materialpricinggroupid,
1 as DIM_MATERIALPRICEGROUP1ID, 	
1 as DIM_MATERIALPRICEGROUP2ID, 	
1 as DIM_MATERIALPRICEGROUP3ID, 	
1 as DIM_MATERIALPRICEGROUP4ID, 	
1 as DIM_MATERIALPRICEGROUP5ID,
IFNULL(dim_countrytaxclassifid,1),
IFNULL(MV.MVKE_RDPRF, 'Not Set'),
current_date,
current_date
FROM
MVKE_MARA MV INNER JOIN MARA MA
ON MA.MARA_MATNR = MV.MVKE_MATNR
LEFT JOIN dim_countrytaxclassif CTX
ON CTX.SalesOrganization = IFNULL(MV.MVKE_VKORG, 'Not Set')
AND CTX.DistributionChannel = IFNULL(MV.MVKE_VTWEG, 'Not Set')
AND CTX.MaterialNumber = IFNULL(MV.MVKE_MATNR, 'Not Set')
WHERE not exists (select 1 from fact_materialsales
                      where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set'));

 --dim_partid						   
 merge into fact_materialsales f
using (
select f.fact_materialsalesid, ds.dim_partid
FROM MVKE_MARA MV,fact_materialsales f, dim_part ds
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
and ds.partnumber = dd_materialnumber
and ds.plant = IFNULL(MV.MVKE_DWERK, 'Not Set')
and f.dim_partid<>ds.dim_partid
) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.dim_partid = t.dim_partid,
 dw_update_date=current_date;
 
 --DIM_SALESORGID
 merge into fact_materialsales f
using (
select distinct f.fact_materialsalesid, ds.dim_salesorgid
FROM MVKE_MARA MV,fact_materialsales f, dim_salesorg ds
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
and ds.salesorgcode = DD_SALESORGANIZATION
and f.dim_salesorgid<>ds.dim_salesorgid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.dim_salesorgid = t.dim_salesorgid,
 dw_update_date=current_date;
 
 --DIM_DISTRIBUTIONCHANNELID,
 merge into fact_materialsales f
using (
select distinct f.fact_materialsalesid, ds.dim_distributionchannelid
FROM MVKE_MARA MV,fact_materialsales f, dim_distributionchannel ds
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
and ds.distributionchannelcode = DD_DISTRIBUTIONCHANNEL
and f.dim_distributionchannelid<>ds.dim_distributionchannelid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.dim_distributionchannelid = t.dim_distributionchannelid,
 dw_update_date=current_date;
 


 --DIM_SDMATERIALSTATUSID
merge into fact_materialsales f
using (select distinct f.fact_materialsalesid, FIRST_VALUE(MS.DIM_SDMATERIALSTATUSID) OVER (PARTITION BY 
MS.SDMATERIALSTATUS ORDER BY MS.DIM_SDMATERIALSTATUSID ASC) AS DIM_SDMATERIALSTATUSID
FROM MVKE_MARA mv,fact_materialsales f, DIM_SDMATERIALSTATUS MS
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
     and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
 and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
and DD_MATERIALSTATUS = MS.SDMATERIALSTATUS
and f.DIM_SDMATERIALSTATUSID<>ms.DIM_SDMATERIALSTATUSID) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set
f.DIM_SDMATERIALSTATUSID = t.DIM_SDMATERIALSTATUSID,
dw_update_date=current_date;

--DIM_DATEIDMAT
merge into fact_materialsales f
using (select distinct f.fact_materialsalesid, dt.dim_dateid as dim_dateid
FROM MVKE_MARA MV,fact_materialsales f, dim_date dt 
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
and dt.datevalue=ifnull(MV.MVKE_VMSTD,'0001-01-01')
and dt.companycode = 'Not Set'
and dt.plantcode_factory = 'Not Set'
and f.dim_dateidmat <> dt.dim_dateid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set  
f.dim_dateidmat = t.dim_dateid,
 dw_update_date=current_date;
 
 --DIM_UNITOFMEASUREID
 merge into fact_materialsales f
using (select distinct f.fact_materialsalesid, uom.dim_unitofmeasureid
FROM MVKE_MARA mv,fact_materialsales f, dim_unitofmeasure uom
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	uom.uom=ifnull(mv.MARA_MEINS,'Not Set')		   
and f.dim_unitofmeasureid<>uom.dim_unitofmeasureid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.dim_unitofmeasureid = t.dim_unitofmeasureid,
 dw_update_date=current_date;

--DIM_PLANTIDMAT 	

merge into fact_materialsales f
using (select distinct f.fact_materialsalesid, dp.dim_plantid
FROM MVKE_MARA mv,fact_materialsales f, dim_plant dp
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	dp.plantcode=ifnull(MV.MVKE_DWERK,'Not Set')		   
and f.dim_plantidmat<>dp.dim_plantid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.dim_plantidmat = t.dim_plantid,
 dw_update_date=current_date;
 
--DIM_PRODUCTHIERARCHYID, 
merge into fact_materialsales f
using (select distinct f.fact_materialsalesid, prd.DIM_PRODUCTHIERARCHYid
FROM MVKE_MARA mv,fact_materialsales f, DIM_PRODUCTHIERARCHY prd
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	PRD.PRODUCTHIERARCHY=ifnull(mv.MVKE_PRODH,'Not Set')		   
and f.DIM_PRODUCTHIERARCHYid<>PRD.DIM_PRODUCTHIERARCHYid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.DIM_PRODUCTHIERARCHYid = t.DIM_PRODUCTHIERARCHYid,
 dw_update_date=current_date;

 
 --1
 merge into fact_materialsales f
using (
select distinct f.fact_materialsalesid, ds.DIM_MATERIALPRICEGROUP1ID
FROM MVKE_MARA MV,fact_materialsales f, DIM_MATERIALPRICEGROUP1 ds
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	DS.MATERIALPRICEGROUP1=ifnull(mv.MVKE_MVGR1,'Not Set')
and f.DIM_MATERIALPRICEGROUP1ID<>ds.DIM_MATERIALPRICEGROUP1ID) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.DIM_MATERIALPRICEGROUP1ID = t.DIM_MATERIALPRICEGROUP1ID,
 dw_update_date=current_date;
 
 --2
  merge into fact_materialsales f
using (
select distinct f.fact_materialsalesid, ds.DIM_MATERIALPRICEGROUP2ID
FROM MVKE_MARA MV,fact_materialsales f, DIM_MATERIALPRICEGROUP2 ds
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	DS.MATERIALPRICEGROUP2=ifnull(mv.MVKE_MVGR2,'Not Set')
and f.DIM_MATERIALPRICEGROUP2ID<>ds.DIM_MATERIALPRICEGROUP2ID) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.DIM_MATERIALPRICEGROUP2ID = t.DIM_MATERIALPRICEGROUP2ID,
 dw_update_date=current_date;
 
 --3
  merge into fact_materialsales f
using (
select distinct f.fact_materialsalesid, ds.DIM_MATERIALPRICEGROUP3ID
FROM MVKE_MARA MV,fact_materialsales f, DIM_MATERIALPRICEGROUP3 ds
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	DS.MATERIALPRICEGROUP3=ifnull(mv.MVKE_MVGR3,'Not Set')  						 
and f.DIM_MATERIALPRICEGROUP3ID<>ds.DIM_MATERIALPRICEGROUP3ID) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.DIM_MATERIALPRICEGROUP3ID = t.DIM_MATERIALPRICEGROUP3ID,
 dw_update_date=current_date;
 
 --4
  merge into fact_materialsales f
using (
select distinct f.fact_materialsalesid, ds.DIM_MATERIALPRICEGROUP4ID
FROM MVKE_MARA MV,fact_materialsales f, DIM_MATERIALPRICEGROUP4 ds
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	DS.MATERIALPRICEGROUP4=ifnull(mv.MVKE_MVGR4,'Not Set')
and f.DIM_MATERIALPRICEGROUP4ID<>ds.DIM_MATERIALPRICEGROUP4ID) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.DIM_MATERIALPRICEGROUP4ID = t.DIM_MATERIALPRICEGROUP4ID,
 dw_update_date=current_date;
 
 --5
  merge into fact_materialsales f
using (
select distinct f.fact_materialsalesid, ds.DIM_MATERIALPRICEGROUP5ID
FROM MVKE_MARA MV,fact_materialsales f, DIM_MATERIALPRICEGROUP5 ds
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	DS.MATERIALPRICEGROUP5=ifnull(mv.MVKE_MVGR5,'Not Set')
and f.DIM_MATERIALPRICEGROUP5ID<>ds.DIM_MATERIALPRICEGROUP5ID) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.DIM_MATERIALPRICEGROUP5ID = t.DIM_MATERIALPRICEGROUP5ID,
 dw_update_date=current_date;
 
 
 ----DIM_UOMIDSALESUNIT,
  merge into fact_materialsales f
using (select distinct f.fact_materialsalesid, uom.dim_unitofmeasureid
FROM MVKE_MARA mv,fact_materialsales f, dim_unitofmeasure uom
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	uom.uom=ifnull(mv.MVKE_VRKME,'Not Set')		   
and f.DIM_UOMIDSALESUNIT<>uom.dim_unitofmeasureid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.DIM_UOMIDSALESUNIT = t.dim_unitofmeasureid,
 dw_update_date=current_date;
 
--DIM_UOMIDDELIVUNIT,
  merge into fact_materialsales f
using (select distinct f.fact_materialsalesid, uom.dim_unitofmeasureid
FROM MVKE_MARA mv,fact_materialsales f, dim_unitofmeasure uom
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	uom.uom=ifnull(mv.MVKE_SCHME,'Not Set')		   
and f.DIM_UOMIDDELIVUNIT<>uom.dim_unitofmeasureid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.DIM_UOMIDDELIVUNIT = t.dim_unitofmeasureid,
 dw_update_date=current_date;
 
 --dim_materialpricinggroupid,
  merge into fact_materialsales f
using (select distinct f.fact_materialsalesid, FIRST_VALUE(mpg.dim_materialpricinggroupid) OVER (PARTITION BY mpg.MATERIALPRICINGGROUPCODE ORDER BY mpg.dim_materialpricinggroupid ASC) AS dim_materialpricinggroupid
FROM MVKE_MARA mv,fact_materialsales f, dim_materialpricinggroup mpg
where DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
					       and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
						   and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
			and 	mpg.MATERIALPRICINGGROUPCODE=ifnull(mv.MVKE_KONDM,'Not Set')		   
and f.dim_materialpricinggroupid<>MPG.dim_materialpricinggroupid) t
on t.fact_materialsalesid=f.fact_materialsalesid
when matched then update set 
 f.dim_materialpricinggroupid = t.dim_materialpricinggroupid,
 dw_update_date=current_date;
 
 
--DD_ACCOUNTASSIGNMGRCODE
  UPDATE fact_materialsales ms
  SET ms.DD_ACCOUNTASSIGNMGRCODE = ifnull(MV.MVKE_KTGRM, 'Not Set'),
dw_update_date=current_date
FROM fact_materialsales ms,  MVKE_MARA mv
  WHERE DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
AND ms.DD_ACCOUNTASSIGNMGRCODE <> ifnull(MV.MVKE_KTGRM, 'Not Set');

 --DD_ACCOUNTASSIGNMGR
 UPDATE fact_materialsales ms
  SET ms.DD_ACCOUNTASSIGNMGR = ifnull(tv.TVKMT_VTEXT, 'Not Set'),
dw_update_date=current_date
FROM fact_materialsales ms, TVKMT tv, MVKE_MARA mv
  WHERE DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set')
and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set')
and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set')
and TV.TVKMT_KTGRM = MV.MVKE_KTGRM
AND ms.DD_ACCOUNTASSIGNMGR <> ifnull(tv.TVKMT_VTEXT, 'Not Set');

--DD_materialstatusdescription
merge into fact_materialsales ms 
using (select distinct ms.fact_materialsalesid, tv.TVMST_VMSTB 
FROM fact_materialsales ms, TVMST tv, MVKE_MARA mv, DIM_MATERIALSTATUS MatS 
WHERE DD_MATERIALNUMBER = IFNULL(MV.MVKE_MATNR, 'Not Set') 
and DD_SALESORGANIZATION = IFNULL(MV.MVKE_VKORG, 'Not Set') 
and DD_DISTRIBUTIONCHANNEL = IFNULL(MV.MVKE_VTWEG, 'Not Set') 
and DD_MATERIALSTATUS = MatS.MATERIALSTATUS 
and TV.TVMST_VMSTA = MV.MVKE_VMSTA 
AND ms.DD_materialstatusdescription <> ifnull(tv.TVMST_VMSTB, 'Not Set')) t
on ms.fact_materialsalesid=t.fact_materialsalesid
when matched then update set ms.DD_materialstatusdescription = ifnull(t.TVMST_VMSTB, 'Not Set');


 
 
 
