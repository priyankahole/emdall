/*Georgiana 01 Nov 2016*/
/*Preprocess for imsert data from CSV files*/

/*Insert from Forecast*/

 insert into atlas_forecast_pra_forecasts_merck_DC
 select plantcode as plant_code,
 uin as pra_uin,
 reportingperiod as pra_reporting_period,
 forecastperiod as pra_forecasting_period,
 salesforecastqty as pra_quantity,
 reportingcompanycode as reporting_company_code,
 heicode as hei_code,
 countrydestinationcode as country_destination_code,
 marketgrouping as market_grouping,
 salesforecastqtyfpu as pra_quantityfpu,
 salesforecastqtybulk as pra_quantitybulk,
current_timestamp as dw_insert_date
from 
PMRA_FORECAST_QTY f
where not exists (
select 1 from atlas_forecast_pra_forecasts_merck_DC dc
where  f.reportingperiod=dc.pra_reporting_period and 
f.forecastperiod = dc.pra_forecasting_period and
f.plantcode=dc.plant_code and 
f.uin=dc.pra_uin and
f.countrydestinationcode = dc.country_destination_code);

/*Update from Forecast*/
merge into atlas_forecast_pra_forecasts_merck_DC dc
using (select  f1.reportingperiod,f1.forecastperiod,f1.plantcode,f1.countrydestinationcode,f1.uin,f1.salesforecastqty,f1.salesforecastqtyfpu,f1.salesforecastqtybulk,
f1.heicode,f1.marketgrouping,f1.reportingcompanycode
from PMRA_FORECAST_QTY f1) f
on
f.reportingperiod=dc.pra_reporting_period and 
f.forecastperiod = dc.pra_forecasting_period and
f.plantcode=dc.plant_code and 
f.uin=dc.pra_uin and
f.countrydestinationcode = dc.country_destination_code
when matched then update set
dc.pra_quantity=f.salesforecastqty,
dc.pra_quantityfpu=f.salesforecastqtyfpu,
dc.pra_quantitybulk=f.salesforecastqtybulk,
dc.hei_code=f.heicode,
dc.market_grouping=f.marketgrouping,
dc.reporting_company_code=f.reportingcompanycode;


/*Delete the existing data from sales*/
delete  from atlas_forecast_sales_merck_DC sdc
where exists (select 1 from  PMRA_SALES_QTY f where 
f.period=sdc.sales_reporting_period
and f.plantcode=sdc.sales_cocd);

/*Insert from Sales*/
insert into atlas_forecast_sales_merck_DC 
select uin as sales_uin,
period as sales_reporting_period,
plantcode as sales_cocd,
salesactualqty as buom_quantity,
reportingcompanycode as reporting_company_code,
heicode as hei_code,
countrydestinationcode as country_destination_code,
marketgrouping as market_grouping,
salesactualqtyfpu as buom_quantityfpu,
salesactualqtybulk as buom_quantitybulk,
'Not Set' as region,
current_timestamp as dw_insert_date
 from PMRA_SALES_QTY f
where not exists (select 1 from atlas_forecast_sales_merck_DC sdc
where f.period=sdc.sales_reporting_period and f.uin=sdc.sales_uin
and f.plantcode=sdc.sales_cocd and f.countrydestinationcode=sdc.country_destination_code);

/*Update from Sales*/
merge into atlas_forecast_sales_merck_DC sdc
using (select uin,period,plantcode,salesactualqty,reportingcompanycode,heicode,countrydestinationcode,marketgrouping,salesactualqtyfpu,salesactualqtybulk
 from PMRA_SALES_QTY f) f1
on f1.period=sdc.sales_reporting_period
 and f1.uin=sdc.sales_uin
and f1.plantcode=sdc.sales_cocd 
and f1.countrydestinationcode=sdc.country_destination_code
when matched then update set 
sdc.buom_quantity= f1.salesactualqty,
sdc.reporting_company_code= f1.reportingcompanycode ,
sdc.hei_code= f1.heicode,
sdc.market_grouping= f1.marketgrouping,
sdc.buom_quantityfpu=f1.salesactualqtyfpu,
sdc.buom_quantitybulk=f1.salesactualqtybulk;

/*Update region market grouping information until we will receive the column in the Sales file*/
merge into atlas_forecast_sales_merck_DC sdc
using (select distinct f.market_grouping,f.region
 from region_marketing_grouping f
) f1
on f1.market_grouping=sdc.market_grouping
when matched then update set 
sdc.region= f1.region;

/*According to Ralf's comment from 06 Feb 2017 this insert is not validated yet so we will use the file from Basecamp*/
/*Insert from Nasp*/
/*insert into atlas_forecast_nasp_merck_DC ndc
select 
'Not Set' as marketing_grouping,
reportingcompanycode as reporting_company_code,
uin as uin,
'NULL' as hei_code,
countrydestinationcode as country_destination_code,
nasp as nasp,
naspused as nasp_used,
from PMRA_NASP f
where not exists (select 1 from atlas_forecast_nasp_merck_DC ndc
where 
f.reportingcompanycode = ndc.reporting_company_code and
f.uin=ndc.uin and 
f.countrydestinationcode=ndc.country_destination_code)

/*Update from Nasp*/
/*merge into atlas_forecast_nasp_merck_DC ndc
using (select reportingcompanycode,uin,countrydestinationcode,nasp,naspused
from PMRA_NASP )f
on f.reportingcompanycode = ndc.reporting_company_code and
f.uin=ndc.uin and 
f.countrydestinationcode=ndc.country_destination_code
when matched then update set
ndc.nasp = f.nasp,
ndc.nasp_used=f.naspused*/

/*Process SupplyOutlook File*/

insert into tmp_Supply_Outlook a 
select * from pmra_supply_outlook b where
not exists (select 1 from tmp_Supply_Outlook a
where a.reporting_period =b.reporting_period
and a.ProductFamily=b.ProductFamily
and a.region=b.region);

/* 17 Oct 2018 Georgiana changes according to APP-10381 - This tables needs to be created for have a better performance on the New Insert of Materials in PF*/
/*Adding it in DF script as it is using a PMRA table*/
drop table if exists get_allMaterialMasterPArts;
create table get_allMaterialMasterPArts as 
select distinct partnumber,plant,planttitle_merck,reporting_company_code as reportingcompanycode
from  dim_part p
inner join dim_plant pl on p.plant=pl.plantcode
inner join (select distinct plant_code,reporting_company_code from atlas_forecast_pra_forecasts_merck_DC ) t on t.plant_code=pl.plantcode;


