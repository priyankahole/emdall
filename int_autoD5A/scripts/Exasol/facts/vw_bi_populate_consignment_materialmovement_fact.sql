

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   22 Dec 2015      Alex D.   1.0               Script in exasol */
/******************************************************************************************************************/

drop table if exists pGlobalCurrency_tbl_consg;
create table pGlobalCurrency_tbl_consg ( pGlobalCurrency varchar(3));

Insert into pGlobalCurrency_tbl_consg values('USD');

Update pGlobalCurrency_tbl_consg
SET pGlobalCurrency =
        ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');
				
DELETE FROM fact_materialmovement
WHERE EXISTS (SELECT 1
		FROM MKPF_MSEG_RKWA m
		WHERE dd_MaterialDocNo = m.MSEG_MBLNR
                         AND dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = dd_MaterialDocYear);



Drop table if exists max_holder_789;

CREATE TABLE max_holder_789 AS
SELECT ifnull(max(fact_materialmovementid), 
	   ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
FROM fact_materialmovement;


drop table if exists fact_materialmovement_789tmp;
create table fact_materialmovement_789tmp as 
--select max_holder_789.maxid + row_number() over() rowno, bb.* from max_holder_789,
SELECT 
          m.MSEG_MBLNR dd_MaterialDocNo,
          m.MSEG_ZEILE dd_MaterialDocItemNo,
          m.MSEG_MJAHR dd_MaterialDocYear,
          m.MSEG_EBELN dd_DocumentNo,
          m.MSEG_EBELP dd_DocumentItemNo,
          m.MSEG_KDAUF dd_SalesOrderNo,
          m.MSEG_KDPOS dd_SalesOrderItemNo,
          m.MSEG_KDEIN dd_SalesOrderDlvrNo,
          m.MSEG_SAKTO dd_GLAccountNo,
          CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          m.MSEG_AUFNR dd_productionordernumber, 
          m.MSEG_AUFPS dd_productionorderitemno,
          m.MSEG_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG_CHARG, 'Not Set') dd_BatchNumber,
          m.MSEG_BWTAR dd_ValuationType,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_DMBTR amt_LocalCurrAmt,
          m.MSEG_DMBTR / (case when m.MSEG_ERFMG =0 then null else  m.MSEG_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BUALT amt_AltPriceControl,
          m.MSEG_SALK3 amt_OnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_MENGE ct_Quantity,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BSTMG ct_QtyGROrdUnit,
          m.MSEG_LBKUM ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN 1 ELSE -1 END) * m.MSEG_BPMNG ct_QtyOrdPriceUnit,
          cast(1 as bigint) as  dim_MovementTypeid,
          cast(1 as bigint) as  dim_Companyid,
          cast(1 as bigint) as  dim_Currencyid,
          cast(1 as bigint) as  dim_Currencyid_TRA,
          cast(1 as bigint) as  Dim_Currencyid_GBL,				   
	      cast(1 as decimal (18,4)) as amt_ExchangeRate,
	      cast(1 as decimal (18,4)) as amt_ExchangeRate_GBL,				   
		  cast(1 as bigint)  as Dim_Partid,
          dp.Dim_Plantid as Dim_Plantid,
          cast(1 as bigint) as Dim_StorageLocationid,
          cast(1 as bigint) as Dim_Vendorid,
          cast(1 as bigint) as dim_DateIDMaterialDocDate,
          cast(1 as bigint) as dim_DateIDPostingDate,
          cast(1 as bigint) as dim_producthierarchyid,
          cast(1 as bigint) as dim_MovementIndicatorid,
          cast(1 as bigint) as dim_Customerid,
          cast(1 as bigint) as dim_CostCenterid,
          cast(1 as bigint) as dim_specialstockid,
          cast(1 as bigint) as dim_unitofmeasureid,
          cast(1 as bigint) as dim_controllingareaid,
          cast(1 as bigint) as dim_consumptiontypeid,
          cast(1 as bigint) as dim_stocktypeid,
	      cast(1 as bigint) as Dim_PurchaseGroupid,
          cast(1 as bigint) as Dim_materialgroupid,
          cast(1 as bigint) as dd_ConsignmentFlag,
          m.MSEG_BWART, -- used in Dim_MovementTypeid
		  m.MSEG_KZVBR, -- used in Dim_MovementTypeid
		  m.MSEG_KZBEW, -- used in Dim_MovementTypeid,dim_MovementIndicatorid
		  m.MSEG_KZZUG, -- used in Dim_MovementTypeid
		  m.MSEG_SOBKZ, -- used in Dim_MovementTypeid,dim_specialstockid
		  m.MSEG_BUKRS, -- used in dim_Companyid,Dim_Currencyid,dim_MaterialDocDate,dim_PostingDate
		  m.MSEG_WAERS, -- used in dim_Currencyid_TRA,Dim_Currencyid_GBL,amt_ExchangeRate,amt_ExchangeRate_GBL
		  m.MKPF_BUDAT, -- used in amt_ExchangeRate,dim_PostingDate
		  m.MSEG_WERKS, -- used in Dim_Partid,Dim_StorageLocationid,dim_producthierarchyid,Dim_PurchaseGroupid,Dim_materialgroupid
		  m.MSEG_MATNR, -- used in Dim_Partid,dim_producthierarchyid,Dim_PurchaseGroupid,Dim_materialgroupid
		  m.MSEG_LGORT, -- used in Dim_StorageLocationid
		  m.MSEG_LIFNR, -- used in Dim_Vendorid
		  m.MSEG_KUNNR, -- used in dim_Customerid
		  m.MSEG_MEINS, -- used in dim_unitofmeasureid
		  m.MSEG_KOKRS, -- used in dim_controllingareaid
		  m.MSEG_INSMK, -- used in dim_stocktypeid
		  m.MKPF_BLDAT, -- used in dim_MaterialDocDate
		  p.pGlobalCurrency,
		  'Not Set' as dc_Currency  -- used in amt_ExchangeRate
		  
     FROM MKPF_MSEG_RKWA m
          INNER JOIN dim_plant dp ON m.MSEG_WERKS = dp.PlantCode 
		                         AND dp.RowIsCurrent = 1
		  CROSS JOIN pGlobalCurrency_tbl_consg p
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear);
                    
UPDATE fact_materialmovement_789tmp m
SET m.dim_MovementTypeid = ifnull(mt.Dim_MovementTypeid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_movementtype mt 
     ON mt.MovementType = m.MSEG_BWART
     AND mt.ConsumptionIndicator = ifnull(m.MSEG_KZVBR, 'Not Set')
     AND mt.MovementIndicator = ifnull(m.MSEG_KZBEW, 'Not Set')
     AND mt.ReceiptIndicator = ifnull(m.MSEG_KZZUG, 'Not Set')
     AND mt.SpecialStockIndicator = ifnull(m.MSEG_SOBKZ, 'Not Set')
     AND mt.RowIsCurrent = 1;
	 
UPDATE fact_materialmovement_789tmp m	 
SET  m.dim_Companyid = IFNULL(dc.dim_Companyid,1),
     m.dc_Currency = IFNULL(dc.Currency,'Not Set')
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_company dc ON dc.CompanyCode = m.MSEG_BUKRS 
                         AND dc.RowIsCurrent = 1;
	 

UPDATE fact_materialmovement_789tmp m	 
SET  m.dim_Currencyid = IFNULL(dcr.Dim_Currencyid,1)
FROM   fact_materialmovement_789tmp m
INNER JOIN dim_company dc ON ifnull(m.dim_Companyid,1) = ifnull(dc.dim_Companyid,1)
LEFT JOIN dim_currency dcr ON dcr.CurrencyCode = dc.currency;
	 
	 
UPDATE fact_materialmovement_789tmp m	 
SET  m.dim_Currencyid_TRA = IFNULL(dcr.Dim_Currencyid,1)
FROM   fact_materialmovement_789tmp m
LEFT JOIN dim_currency dcr ON dcr.CurrencyCode = m.MSEG_WAERS;

UPDATE fact_materialmovement_789tmp m	 
SET  m.Dim_Currencyid_GBL = IFNULL(dcr.Dim_Currencyid,1)
FROM   fact_materialmovement_789tmp m
LEFT JOIN dim_currency dcr ON dcr.CurrencyCode = m.pGlobalCurrency;

UPDATE fact_materialmovement_789tmp m	
SET  m.amt_ExchangeRate	= IFNULL(z.exchangeRate,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN tmp_getExchangeRate1 z ON  z.pFromCurrency  = m.MSEG_WAERS 
	                              AND z.fact_script_name = 'bi_populate_materialmovement_fact' 
	                              AND z.pToCurrency = m.dc_Currency 
	                              AND z.pDate = m.MKPF_BUDAT;
	


UPDATE fact_materialmovement_789tmp m	
SET m.amt_ExchangeRate_GBL =  CASE WHEN MSEG_WAERS = pGlobalCurrency 
                              THEN 1.00 
							  ELSE z.exchangeRate 
							  END
FROM fact_materialmovement_789tmp m,tmp_getExchangeRate1 z 
WHERE    z.pFromCurrency  = m.MSEG_WAERS 
     AND z.fact_script_name = 'bi_populate_materialmovement_fact' 
	 AND z.pToCurrency = pGlobalCurrency 
	 AND z.pDate = current_date;


UPDATE fact_materialmovement_789tmp m	
SET m.Dim_Partid = IFNULL(dpr.Dim_Partid,1) 
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_part dpr ON     dpr.PartNumber = m.MSEG_MATNR
                          AND dpr.Plant = m.MSEG_WERKS
                          AND dpr.RowIsCurrent = 1;
     
    
	
UPDATE fact_materialmovement_789tmp m
SET m.Dim_StorageLocationid = IFNULL(Dim_StorageLocationid,1) 
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_storagelocation dsl ON dsl.LocationCode = m.MSEG_LGORT
                                  AND dsl.Plant = m.MSEG_WERKS
                                  AND dsl.RowIsCurrent = 1;	 
					
					
UPDATE fact_materialmovement_789tmp m
SET m.Dim_Vendorid = IFNULL(dv.Dim_Vendorid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_vendor dv ON dv.VendorNumber = m.MSEG_LIFNR
                        AND dv.RowIsCurrent = 1;


UPDATE fact_materialmovement_789tmp m
SET m.dim_DateIDMaterialDocDate = IFNULL(mdd.Dim_Dateid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_date mdd ON mdd.CompanyCode = m.MSEG_BUKRS 
                       AND m.MKPF_BLDAT = mdd.DateValue
		       AND M.MSEG_WERKS = mdd.plantcode_factory;
   
UPDATE fact_materialmovement_789tmp m
SET m.dim_DateIDPostingDate = IFNULL(pd.Dim_Dateid,1)
FROM  fact_materialmovement_789tmp m
LEFT JOIN dim_date pd ON pd.DateValue = m.MKPF_BUDAT 
                      AND pd.CompanyCode = m.MSEG_BUKRS
		      AND M.MSEG_WERKS = pd.plantcode_factory;


UPDATE fact_materialmovement_789tmp m
SET m.dim_producthierarchyid = IFNULL(dim_producthierarchyid,1)
FROM fact_materialmovement_789tmp m
INNER JOIN dim_part dpr ON ifnull(m.Dim_Partid, 1) = ifnull(dpr.Dim_Partid,1)
LEFT JOIN dim_producthierarchy dph  ON dph.ProductHierarchy = dpr.ProductHierarchy
                                    AND dph.RowIsCurrent = 1;
                                                   

						  
UPDATE fact_materialmovement_789tmp m
SET m.dim_MovementIndicatorid = IFNULL(dmi.dim_MovementIndicatorid,1)
FROM fact_materialmovement_789tmp m 
LEFT JOIN  dim_movementindicator dmi ON dmi.TypeCode = m.MSEG_KZBEW
                                     AND dmi.RowIsCurrent = 1;			


UPDATE fact_materialmovement_789tmp m
SET m.dim_Customerid = 	IFNULL(dcu.dim_Customerid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_customer dcu ON dcu.CustomerNumber = m.MSEG_KUNNR 
                           AND dcu.RowIsCurrent = 1;
					  
						
UPDATE fact_materialmovement_789tmp m
SET m.dim_specialstockid = IFNULL(spt.dim_specialstockid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_specialstock spt ON spt.specialstockindicator = MSEG_SOBKZ
                               AND spt.RowIsCurrent = 1;


UPDATE fact_materialmovement_789tmp m
SET m.dim_unitofmeasureid =  IFNULL(uom.Dim_UnitOfMeasureid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_unitofmeasure uom ON uom.UOM = MSEG_MEINS 
                                AND uom.RowIsCurrent = 1;
	 
				   
UPDATE fact_materialmovement_789tmp m
SET dim_controllingareaid = IFNULL(ca.dim_controllingareaid,1)
FROM  fact_materialmovement_789tmp m
LEFT JOIN  dim_controllingarea ca ON ca.ControllingAreaCode = MSEG_KOKRS;


UPDATE fact_materialmovement_789tmp m
SET m.dim_consumptiontypeid = IFNULL(ct.Dim_ConsumptionTypeid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_consumptiontype ct ON ct.ConsumptionCode = MSEG_KZVBR 
                                 AND ct.RowIsCurrent = 1;

				   
UPDATE fact_materialmovement_789tmp m
SET m.dim_stocktypeid = IFNULL(st.Dim_StockTypeid,1)
FROM fact_materialmovement_789tmp m
LEFT JOIN dim_stocktype st ON st.TypeCode = MSEG_INSMK 
                           AND st.RowIsCurrent = 1;
	 

UPDATE fact_materialmovement_789tmp m
SET m.Dim_PurchaseGroupid = IFNULL(pg.Dim_PurchaseGroupid,1)
FROM fact_materialmovement_789tmp m
INNER JOIN dim_part dpr ON ifnull(m.Dim_Partid,1) = ifnull(dpr.Dim_Partid,1)
LEFT JOIN dim_purchasegroup pg ON dpr.PurchaseGroupCode = pg.PurchaseGroup;	
				

						

UPDATE fact_materialmovement_789tmp m
SET m.Dim_materialgroupid =  IFNULL(mg.Dim_materialgroupid,1)
FROM fact_materialmovement_789tmp m
INNER JOIN dim_part dpr ON ifnull(m.Dim_Partid,1) = ifnull(dpr.Dim_Partid,1)
LEFT JOIN  dim_materialgroup mg ON mg.MaterialGroupCode = dpr.MaterialGroup 
                                AND mg.RowIsCurrent = 1;








drop table if exists dim_costcenter_789;
create table dim_costcenter_789 as select * from dim_costcenter;


MERGE INTO fact_materialmovement_789tmp fact
	USING (  SELECT fmt.dd_MaterialDocNo, fmt.dd_MaterialDocItemNo, fmt.dd_MaterialDocYear,  
                   ifnull(dcc.dim_CostCenterid,1) AS dim_CostCenterid
			  FROM fact_materialmovement_789tmp fmt
             INNER JOIN MKPF_MSEG_RKWA m
                ON fmt.dd_MaterialDocNo = m.MSEG_MBLNR
               AND fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
               AND fmt.dd_MaterialDocYear = m.MSEG_MJAHR
             INNER JOIN dim_plant dp
                ON m.MSEG_WERKS = dp.PlantCode 
               AND dp.RowIsCurrent = 1 		  
			  LEFT JOIN dim_costcenter_789 dcc
                ON dcc.Code = m.MSEG_KOSTL
               AND dcc.validTo >= m.MKPF_BUDAT
               AND m.MSEG_KOKRS = dcc.ControllingArea
               AND dcc.RowIsCurrent = 1) src
   ON fact.dd_MaterialDocNo = src.dd_MaterialDocNo
  AND fact.dd_MaterialDocItemNo = src.dd_MaterialDocItemNo
  AND fact.dd_MaterialDocYear = src.dd_MaterialDocYear
 WHEN MATCHED THEN UPDATE
  SET fact.dim_CostCenterid = ifnull(src.dim_CostCenterid, 1);


DROP TABLE IF EXISTS dim_costcenter_789;

INSERT INTO fact_materialmovement(fact_materialmovementid,
  			                   dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
							   Dim_Currencyid_TRA,
							   Dim_Currencyid_GBL,
							   amt_exchangerate,
							   amt_exchangerate_GBL,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               dd_ConsignmentFlag)
SELECT  max_holder_789.maxid + row_number() over(order by '') fact_materialmovementid, x.* FROM 
(SELECT DISTINCT 
dd_MaterialDocNo,
dd_MaterialDocItemNo,
dd_MaterialDocYear,
dd_DocumentNo,
dd_DocumentItemNo,
dd_SalesOrderNo,
dd_SalesOrderItemNo,
dd_SalesOrderDlvrNo,
dd_GLAccountNo,
ifnull(dd_debitcreditid, 'Not Set') as dd_debitcreditid,
dd_productionordernumber,
dd_productionorderitemno,
ifnull(dd_GoodsMoveReason, 0) as dd_GoodsMoveReason,
dd_BatchNumber,
dd_ValuationType,
amt_LocalCurrAmt,
amt_StdUnitPrice,
amt_DeliveryCost,
amt_AltPriceControl,
amt_OnHand_ValStock,
ct_Quantity,
ct_QtyEntryUOM,
ct_QtyGROrdUnit,
ct_QtyOnHand_ValStock,
ct_QtyOrdPriceUnit,
ifnull(Dim_MovementTypeid, 1) as Dim_MovementTypeid,
ifnull(dim_Companyid, 1) as dim_Companyid,
ifnull(Dim_Currencyid,1) as Dim_Currencyid,
Dim_Currencyid_TRA,
Dim_Currencyid_GBL,
amt_exchangerate,
amt_exchangerate_GBL,
ifnull(Dim_Partid, 1) as Dim_Partid,
ifnull(Dim_Plantid, 1) as Dim_Plantid,
ifnull(Dim_StorageLocationid, 1) as Dim_StorageLocationid,
ifnull(Dim_Vendorid, 1) as Dim_Vendorid,
ifnull(dim_DateIDMaterialDocDate, 1) as dim_DateIDMaterialDocDate,
ifnull(dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
ifnull(dim_producthierarchyid, 1) as dim_producthierarchyid,
ifnull(dim_MovementIndicatorid, 1) as dim_MovementIndicatorid,
ifnull(dim_Customerid, 1) as dim_Customerid,
ifnull(dim_CostCenterid, 1) as dim_CostCenterid,
ifnull(dim_specialstockid, 1) as dim_specialstockid,
ifnull(dim_unitofmeasureid, 1) as dim_unitofmeasureid,
ifnull(dim_controllingareaid, 1) as dim_controllingareaid,
ifnull(dim_consumptiontypeid, 1) as dim_consumptiontypeid,
ifnull(dim_stocktypeid, 1) as dim_stocktypeid,
ifnull(Dim_PurchaseGroupid, 1) as Dim_PurchaseGroupid,
ifnull(dim_materialgroupid, 1) as dim_materialgroupid,
ifnull(dd_ConsignmentFlag, 0) as dd_ConsignmentFlag
FROM fact_materialmovement_789tmp) x
CROSS JOIN max_holder_789;
                               
drop table if exists max_holder_789;
DROP TABLE IF EXISTS fact_materialmovement_789tmp;


MERGE INTO fact_materialmovement fact 
USING (	
SELECT DISTINCT ms.fact_materialmovementid, ifnull(po.dim_purchasegroupid,1) AS dim_purchasegroupid
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE  ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	 AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
     AND ifnull(ms.Dim_Plantid,1) = ifnull(pl.Dim_Plantid,1)
	 AND  po.dim_purchasegroupid <>1 
	 AND  ms.dim_purchasegroupid <> ifnull(po.dim_purchasegroupid, 1)) src
ON fact.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE 
SET fact.dim_purchasegroupid = src.dim_purchasegroupid;

MERGE INTO fact_materialmovement fact 
USING (	
SELECT DISTINCT ms.fact_materialmovementid,ifnull(po.dim_purchaseorgid, 1) AS dim_purchaseorgid
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
      AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
      AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
      AND ifnull(ms.Dim_Plantid, 1) = ifnull(pl.Dim_Plantid, 1)
      AND  ms.dim_purchaseorgid <> ifnull(po.dim_purchaseorgid, 1) ) src
ON fact.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
SET fact.dim_purchaseorgid = src.dim_purchaseorgid;


MERGE INTO fact_materialmovement fact 
USING (	
SELECT DISTINCT ms.fact_materialmovementid,ifnull(po.dim_itemcategoryid,1) AS dim_itemcategoryid
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
     AND ifnull(ms.Dim_Plantid, 1) = ifnull(pl.Dim_Plantid, 1)
     AND IFNULL(ms.dim_itemcategoryid, 1) <> ifnull(po.dim_itemcategoryid, 1)) src
ON fact.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
SET fact.dim_itemcategoryid = src.dim_itemcategoryid;



MERGE INTO fact_materialmovement fact 
USING (	
SELECT DISTINCT ms.fact_materialmovementid,po.dim_Termid AS dim_Termid
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
     AND ifnull(ms.Dim_Plantid,1) = ifnull(pl.Dim_Plantid,1)
     AND  ms.dim_Termid <> po.dim_Termid) src
ON fact.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
SET fact.dim_Termid = src.dim_Termid;


MERGE INTO fact_materialmovement fact 
USING (	
SELECT DISTINCT ms.fact_materialmovementid,IFNULL(po.dim_documenttypeid, 1)  AS dim_documenttypeid
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
      AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
      AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
      AND ifnull(ms.Dim_Plantid, 1) = ifnull(pl.Dim_Plantid,1)
      AND  ms.dim_documenttypeid <> IFNULL(po.dim_documenttypeid, 1) ) src
ON fact.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
SET fact.dim_documenttypeid = src.dim_documenttypeid;

MERGE INTO fact_materialmovement fact 
USING (	
SELECT DISTINCT ms.fact_materialmovementid,ifnull(po.dim_accountcategoryid, 1) AS dim_accountcategoryid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
     AND ifnull(ms.Dim_Plantid, 1) = ifnull(pl.Dim_Plantid,1)
     AND  ms.dim_accountcategoryid <>  ifnull(po.dim_accountcategoryid, 1)) src
ON fact.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
SET fact.dim_accountcategoryid = src.dim_accountcategoryid;

MERGE INTO fact_materialmovement fact 
USING (	
SELECT DISTINCT ms.fact_materialmovementid, ifnull(po.dim_itemstatusid, 1) AS dim_itemstatusid
FROM  fact_materialmovement ms,fact_purchase po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
     AND ifnull(ms.Dim_Plantid, 1) = ifnull(pl.Dim_Plantid, 1)
     AND  ms.dim_itemstatusid <>  ifnull(po.dim_itemstatusid, 1)) src
ON fact.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
SET fact.dim_itemstatusid = src.dim_itemstatusid;

DROP TABLE IF EXISTS tmp_upd_po_to_mm;
CREATE TABLE tmp_upd_po_to_mm
AS
SELECT po.*
FROM fact_purchase po,
(SELECT fp.dd_DocumentNo,fp.dd_DocumentItemNo,min(dd_scheduleno) dd_scheduleno
FROM fact_purchase fp GROUP BY fp.dd_DocumentNo,fp.dd_DocumentItemNo) t
WHERE po.dd_DocumentNo = t.dd_DocumentNo
AND po.dd_DocumentItemNo = t.dd_DocumentItemNo
AND po.dd_scheduleno = t.dd_scheduleno;

MERGE INTO fact_materialmovement ms
USING 
(SELECT distinct ms.fact_materialmovementid, FIRST_VALUE(po.Dim_DateidCreate) OVER (PARTITION BY ms.fact_materialmovementid) AS Dim_DateIDDocCreation
FROM fact_materialmovement ms,tmp_upd_po_to_mm po,dim_vendor v, dim_plant pl
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
AND ifnull(ms.Dim_Plantid, 1) = ifnull(pl.Dim_Plantid,1)
AND ifnull(ms.Dim_DateIDDocCreation, 1) <> ifnull(po.Dim_DateidCreate,1) ) src
ON ms.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
SET ms.Dim_DateIDDocCreation = ifnull(src.Dim_DateIDDocCreation, 1);

MERGE INTO fact_materialmovement ms
USING (SELECT DISTINCT ms.fact_materialmovementid, FIRST_VALUE(po.Dim_DateidOrder) OVER (PARTITION BY ms.fact_materialmovementid ORDER BY ms.fact_materialmovementid) AS Dim_DateIDOrdered
FROM  fact_materialmovement ms,tmp_upd_po_to_mm po,dim_vendor v, dim_plant pl
WHERE  ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
     AND ifnull(ms.Dim_Plantid, 1) = ifnull(pl.Dim_Plantid, 1)
     AND ifnull(ms.Dim_DateIDOrdered, 1) <> ifnull(po.Dim_DateidOrder, 1) ) src
ON ms.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE 
SET ms.Dim_DateIDOrdered = ifnull(src.Dim_DateIDOrdered, 1);

MERGE INTO fact_materialmovement ms
USING (SELECT DISTINCT ms.fact_materialmovementid, FIRST_VALUE(po.Dim_MaterialGroupid) OVER (PARTITION BY ms.fact_materialmovementid ORDER BY ms.fact_materialmovementid) AS Dim_MaterialGroupid
FROM  fact_materialmovement ms,tmp_upd_po_to_mm po,dim_vendor v, dim_plant pl
WHERE  ms.dd_DocumentNo = po.dd_DocumentNo
     AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
     AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
     AND ifnull(ms.Dim_Plantid, 1) = ifnull(pl.Dim_Plantid, 0)
     AND  ms.Dim_MaterialGroupid <> po.Dim_MaterialGroupid) src
ON ms.fact_materialmovementid = src.fact_materialmovementid
WHEN MATCHED THEN UPDATE
SET ms.Dim_MaterialGroupid = src.Dim_MaterialGroupid;

DROP TABLE IF EXISTS tmp_upd_po_to_mm;

DROP TABLE IF EXISTS tmp_upd_so_to_mm;
CREATE TABLE tmp_upd_so_to_mm
AS
SELECT po.*
FROM fact_salesorder po,
(SELECT fp.dd_SalesDocNo,fp.dd_SalesItemNo,min(dd_scheduleno) dd_scheduleno
FROM fact_salesorder fp GROUP BY fp.dd_SalesDocNo,fp.dd_SalesItemNo) t
WHERE po.dd_SalesDocNo = t.dd_SalesDocNo
AND po.dd_SalesItemNo = t.dd_SalesItemNo
AND po.dd_scheduleno = t.dd_scheduleno;
  
UPDATE fact_materialmovement ms
SET ms.dim_salesorderheaderstatusid = so.dim_salesorderheaderstatusid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesorderheaderstatusid <> so.dim_salesorderheaderstatusid;



UPDATE fact_materialmovement ms
SET  ms.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
	 ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesorderitemstatusid <> so.dim_salesorderitemstatusid;

UPDATE fact_materialmovement ms
SET ms.dim_salesdocumenttypeid = so.dim_salesdocumenttypeid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesdocumenttypeid <> so.dim_salesdocumenttypeid;

UPDATE fact_materialmovement ms
SET ms.dim_salesorderrejectreasonid = so.dim_salesorderrejectreasonid
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND ms.dim_salesorderrejectreasonid <> so.dim_salesorderrejectreasonid;

UPDATE fact_materialmovement ms
SET ms.dim_salesgroupid = so.dim_salesgroupid
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesgroupid <> so.dim_salesgroupid;

UPDATE fact_materialmovement ms
SET ms.dim_salesorgid = so.dim_salesorgid
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND  ms.dim_salesorgid <> so.dim_salesorgid;

UPDATE fact_materialmovement ms
SET ms.dim_customergroup1id = ifnull(so.dim_customergroup1id, 1)
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo;

UPDATE fact_materialmovement ms
SET ms.dim_documentcategoryid = ifnull(so.dim_documentcategoryid, 1)
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
    AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
    AND ms.dim_documentcategoryid <>  ifnull(so.dim_documentcategoryid, 1);

UPDATE fact_materialmovement ms
SET ms.Dim_DateIDDocCreation = ifnull(so.Dim_DateidSalesOrderCreated, 1)
	,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND ms.Dim_DateIDDocCreation <> ifnull(so.Dim_DateidSalesOrderCreated, 1);

UPDATE fact_materialmovement ms
SET ms.Dim_DateIDOrdered = ifnull(so.Dim_DateidSalesOrderCreated, 1)
   ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,tmp_upd_so_to_mm so
WHERE  ms.dd_SalesOrderNo = so.dd_SalesDocNo
     AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
     AND ms.Dim_DateIDOrdered <> ifnull(so.Dim_DateidSalesOrderCreated, 1);
  
UPDATE fact_materialmovement ms
SET ms.dim_productionorderstatusid = po.dim_productionorderstatusid
   ,ms.dw_update_date = current_timestamp
FROM   fact_materialmovement ms,fact_productionorder po
WHERE  ms.dd_productionordernumber = po.dd_ordernumber
     AND ms.dd_productionorderitemno = po.dd_orderitemno
     AND ms.dim_productionorderstatusid<>  po.dim_productionorderstatusid;

UPDATE fact_materialmovement ms
SET  ms.dim_productionordertypeid = ifnull(po.Dim_ordertypeid, 1)
    ,ms.dw_update_date = current_timestamp
FROM  fact_materialmovement ms,fact_productionorder po
WHERE  ms.dd_productionordernumber = po.dd_ordernumber
     AND ms.dd_productionorderitemno = po.dd_orderitemno
     AND  ms.dim_productionordertypeid <> ifnull(po.Dim_ordertypeid, 1);

 