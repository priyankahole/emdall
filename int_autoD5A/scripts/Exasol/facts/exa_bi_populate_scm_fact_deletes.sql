
 /* prepare fact deletes */ 
 merge into fact_scm_test mmp using 
         (select distinct mmp.fact_mmprodhierarchyid 
         from fact_scm_test mmp 
          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_raw_1 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_raw_1 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) raw_1 on mmp.fact_mmprodhierarchyid = raw_1.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_antigen_1 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_antigen_1 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) antigen_1 on mmp.fact_mmprodhierarchyid = antigen_1.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_antigen_2 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_antigen_2 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) antigen_2 on mmp.fact_mmprodhierarchyid = antigen_2.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_antigen_3 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_antigen_3 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) antigen_3 on mmp.fact_mmprodhierarchyid = antigen_3.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_antigen_4 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_antigen_4 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) antigen_4 on mmp.fact_mmprodhierarchyid = antigen_4.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_antigen_5 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_antigen_5 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) antigen_5 on mmp.fact_mmprodhierarchyid = antigen_5.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_bulk_1 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_bulk_1 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) bulk_1 on mmp.fact_mmprodhierarchyid = bulk_1.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_bulk_2 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_bulk_2 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) bulk_2 on mmp.fact_mmprodhierarchyid = bulk_2.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_bulk_3 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_bulk_3 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) bulk_3 on mmp.fact_mmprodhierarchyid = bulk_3.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_fpu_1 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_fpu_1 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) fpu_1 on mmp.fact_mmprodhierarchyid = fpu_1.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_fpu_2 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_fpu_2 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) fpu_2 on mmp.fact_mmprodhierarchyid = fpu_2.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_fpu_3 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_fpu_3 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) fpu_3 on mmp.fact_mmprodhierarchyid = fpu_3.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_fpu_4 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_fpu_4 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) fpu_4 on mmp.fact_mmprodhierarchyid = fpu_4.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_fpu_5 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_fpu_5 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) fpu_5 on mmp.fact_mmprodhierarchyid = fpu_5.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_fpp_1 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_fpp_1 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) fpp_1 on mmp.fact_mmprodhierarchyid = fpp_1.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_fpp_2 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_fpp_2 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) fpp_2 on mmp.fact_mmprodhierarchyid = fpp_2.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_fpp_3 <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_fpp_3 = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) fpp_3 on mmp.fact_mmprodhierarchyid = fpp_3.fact_mmprodhierarchyid 
                          left join (select distinct tmp.fact_mmprodhierarchyid from fact_scm_test tmp  
                         inner join fact_inspectionlot fi on tmp.DD_INSPECTIONLOTNO_comops <> 'Not Set' 
                                                             and tmp.DD_INSPECTIONLOTNO_comops = fi.dd_inspectionlotno 
                         inner join dim_inspectiontype dit on  fi.dim_inspectiontypeid = dit.dim_inspectiontypeid  
                                                         and dit.InspectionTypeCode not like '01%'  
                                                         and dit.InspectionTypeCode not like '04%'  
                                                         and dit.InspectionTypeCode not like '03%' 
                         ) comops on mmp.fact_mmprodhierarchyid = comops.fact_mmprodhierarchyid 
                          where raw_1.fact_mmprodhierarchyid is not null 
  or antigen_1.fact_mmprodhierarchyid is not null 
  or antigen_2.fact_mmprodhierarchyid is not null 
  or antigen_3.fact_mmprodhierarchyid is not null 
  or antigen_4.fact_mmprodhierarchyid is not null 
  or antigen_5.fact_mmprodhierarchyid is not null 
  or bulk_1.fact_mmprodhierarchyid is not null 
  or bulk_2.fact_mmprodhierarchyid is not null 
  or bulk_3.fact_mmprodhierarchyid is not null 
  or fpu_1.fact_mmprodhierarchyid is not null 
  or fpu_2.fact_mmprodhierarchyid is not null 
  or fpu_3.fact_mmprodhierarchyid is not null 
  or fpu_4.fact_mmprodhierarchyid is not null 
  or fpu_5.fact_mmprodhierarchyid is not null 
  or fpp_1.fact_mmprodhierarchyid is not null 
  or fpp_2.fact_mmprodhierarchyid is not null 
  or fpp_3.fact_mmprodhierarchyid is not null 
  or comops.fact_mmprodhierarchyid is not null 
 ) del 
         on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid 
         when matched then delete; 
