
/* Madalina 10 Feb 2017 - re-write the join with Inspection lot, from inner join to left join, to avoid losing not inspected materials BI-5352 */
drop table if exists tmp001_mat101_102_insplot_purchase;
create table tmp001_mat101_102_insplot_purchase as
select
distinct f_mm.dim_plantid,
         f_mm.dim_partid,
         f_mm.dim_batchid,
         ifnull(f_ins.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno,
         f_mm.dd_DocumentNo,
         f_mm.dd_DocumentItemNo,
		 f_pur.dd_scheduleno,
         f_pur.dim_dateidschedorder,
         f_pur.dim_dateidcreate,
         f_mm.dim_dateidpostingdate,
         ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr,
         ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco,
         ifnull(f_ins.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart,
         ifnull(f_ins.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade
         from fact_materialmovement f_mm
			  inner join fact_purchase f_pur on  f_mm.dim_plantid = f_pur.dim_plantidordering
											  and f_mm.dim_partid = f_pur.dim_partid
											  and f_pur.dd_documentno = f_mm.dd_documentno
											  and f_pur.dd_documentitemno = f_mm.dd_documentitemno
              left join fact_inspectionlot f_ins on  f_mm.dim_plantid = f_ins.dim_plantid
												  and f_mm.dim_partid = f_ins.dim_partid
												  and f_mm.dim_batchid = f_ins.dim_batchid
			  /*BI-5352 - exclude date conditions*/
              /* inner join dim_date dd1 on f_mm.dim_dateidpostingdate = dd1.dim_dateid
              inner join dim_date dd2 on f_ins.dim_dateidlotcreated = dd2.dim_dateid */
              inner join dim_movementtype mt on f_mm.dim_movementtypeid = mt.dim_movementtypeid
              inner join dim_part dp on f_mm.dim_partid = dp.dim_partid
where /* dd1.datevalue = ifnull(dd2.datevalue, '0001-01-01') */
  mt.movementtype in ('101','102')
  and f_mm.dim_dateidpostingdate <> 1 
  and f_mm.dim_plantid <> 1
  and f_mm.dim_partid <> 1
  and f_mm.dim_batchid <> 1
  and dp.ItemSubType_Merck = 'RAW';

drop table if exists tmp002_mat261_262_prodorder;
create table tmp002_mat261_262_prodorder as
select distinct f_mm.dim_plantid,
                f_mm.dim_partid,
                f_mm.dim_batchid,
                f_prodord.dd_ordernumber,
                f_prodord.dim_dateidactualheaderfinish,
                f_prodord.dim_dateidactualrelease,
                f_prodord.dim_dateidactualstart,
                f_prodord.dim_partidheader,
                f_prodord.dim_batchid as dim_batchidprod
 from fact_materialmovement f_mm,
      fact_productionorder f_prodord,
      dim_movementtype mt,
	  dim_part dp,
	  dim_part dph
where f_mm.dim_movementtypeid = mt.dim_movementtypeid
 and  mt.movementtype in ('261','262')
 and f_mm.dim_plantid = f_prodord.dim_plantid
 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
 and dp.dim_partid = f_mm.dim_partid
 and dph.dim_partid = f_prodord.dim_partidheader
 and dp.ItemSubType_Merck = 'RAW'
 and dph.ItemSubType_Merck = 'BULK';

/* Madalina 10 Feb 2017 - re-write the join with Inspection lot, from inner join to left join, to avoid losing not inspected materials BI-5352 */
drop table if exists tmp003_insplot_prodorder;
create table tmp003_insplot_prodorder as
select distinct f_prodorder.dim_batchid,
                f_prodorder.dim_partidheader as dim_partid,
                f_prodorder.dim_plantid,
                ifnull(f_insp.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno,
                f_prodorder.dd_ordernumber,
                ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr,
                ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco,
                ifnull(f_insp.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart,
                ifnull(f_insp.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade
from fact_productionorder f_prodorder 
		left join fact_inspectionlot f_insp on f_insp.dim_batchid = f_prodorder.dim_batchid
										  and f_insp.dim_partid = f_prodorder.dim_partidheader
										  and f_insp.dim_plantid = f_prodorder.dim_plantid
		inner join dim_part dp on f_prodorder.dim_partidheader = dp.dim_partid
where dp.ItemSubType_Merck = 'BULK';

/*--------------------------------------------------------------*/
/* Adding one more bulk step */
drop table if exists tmp002_mat261_262_prodorder_BULK_BULK;
create table tmp002_mat261_262_prodorder_BULK_BULK as
select distinct f_mm.dim_plantid,
                f_mm.dim_partid,
                f_mm.dim_batchid,
                f_prodord.dd_ordernumber,
                f_prodord.dim_dateidactualheaderfinish,
                f_prodord.dim_dateidactualrelease,
                f_prodord.dim_dateidactualstart,
                f_prodord.dim_partidheader,
                f_prodord.dim_batchid as dim_batchidprod
 from fact_materialmovement f_mm,
      fact_productionorder f_prodord,
      dim_movementtype mt,
	  dim_part dp,
	  dim_part dph
where f_mm.dim_movementtypeid = mt.dim_movementtypeid
 and  mt.movementtype in ('261','262')
 and f_mm.dim_plantid = f_prodord.dim_plantid
 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
 and dp.dim_partid = f_mm.dim_partid
 and dph.dim_partid = f_prodord.dim_partidheader
 and dp.ItemSubType_Merck = 'BULK'
 and dph.ItemSubType_Merck = 'BULK';
 /*--------------------------------------------------------------*/

drop table if exists tmp004_mat261_262_prodorder;
create table tmp004_mat261_262_prodorder as
select distinct f_mm.dim_plantid,
                f_mm.dim_partid,
                f_mm.dim_batchid,
                f_prodord.dd_ordernumber,
                f_prodord.dim_dateidactualheaderfinish,
                f_prodord.dim_dateidactualrelease,
                f_prodord.dim_dateidactualstart,
                f_prodord.dim_partidheader,
                f_prodord.dim_batchid as dim_batchidprod
 from fact_materialmovement f_mm,
      fact_productionorder f_prodord,
      dim_movementtype mt,
	  dim_part dp,
	  dim_part dph
where f_mm.dim_movementtypeid = mt.dim_movementtypeid
 and  mt.movementtype in ('261','262')
 and f_mm.dim_plantid = f_prodord.dim_plantid
 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
 and dp.dim_partid = f_mm.dim_partid
 and dph.dim_partid = f_prodord.dim_partidheader
 and dp.ItemSubType_Merck = 'BULK'
 and dph.ItemSubType_Merck = 'FPU';

/* Madalina 10 Feb 2017 - re-write the join with Inspection lot, from inner join to left join, to avoid losing not inspected materials BI-5352 */
drop table if exists tmp005_insplot_prodorder;
create table tmp005_insplot_prodorder as
select distinct f_prodorder.dim_batchid,
                f_prodorder.dim_partidheader as dim_partid,
                f_prodorder.dim_plantid,
                ifnull(f_insp.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno,
                f_prodorder.dd_ordernumber,
                ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr,
                ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco,
                ifnull(f_insp.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart,
                ifnull(f_insp.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade
from fact_productionorder f_prodorder 
	left join fact_inspectionlot f_insp on f_insp.dim_batchid = f_prodorder.dim_batchid
										  and f_insp.dim_partid = f_prodorder.dim_partidheader
										  and f_insp.dim_plantid = f_prodorder.dim_plantid
    inner join dim_part dp on f_prodorder.dim_partidheader = dp.dim_partid
where dp.ItemSubType_Merck = 'FPU';

drop table if exists tmp006_mat261_262_prodorder;
create table tmp006_mat261_262_prodorder as
select distinct f_mm.dim_plantid,
                f_mm.dim_partid,
                f_mm.dim_batchid,
                f_prodord.dd_ordernumber,
                f_prodord.dim_dateidactualheaderfinish,
                f_prodord.dim_dateidactualrelease,
                f_prodord.dim_dateidactualstart,
                f_prodord.dim_partidheader,
                f_prodord.dim_batchid as dim_batchidprod
 from fact_materialmovement f_mm,
      fact_productionorder f_prodord,
      dim_movementtype mt,
	  dim_part dp,
	  dim_part dph
where f_mm.dim_movementtypeid = mt.dim_movementtypeid
 and  mt.movementtype in ('261','262')
 and f_mm.dim_plantid = f_prodord.dim_plantid
 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
 and dp.dim_partid = f_mm.dim_partid
 and dph.dim_partid = f_prodord.dim_partidheader
 and dp.ItemSubType_Merck = 'FPU'
 and dph.ItemSubType_Merck = 'FPP';

/* Madalina 10 Feb 2017 - re-write the join with Inspection lot, from inner join to left join, to avoid losing not inspected materials BI-5352 */
drop table if exists tmp007_insplot_prodorder;
create table tmp007_insplot_prodorder as
select distinct f_prodorder.dim_batchid,
                f_prodorder.dim_partidheader as dim_partid,
                f_prodorder.dim_plantid,
                ifnull(f_insp.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno,
                f_prodorder.dd_ordernumber,
                ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr,
                ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco,
                ifnull(f_insp.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart,
                ifnull(f_insp.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade
from fact_productionorder f_prodorder
		left join fact_inspectionlot f_insp on 	f_insp.dim_batchid = f_prodorder.dim_batchid
											  and f_insp.dim_partid = f_prodorder.dim_partidheader
											  and f_insp.dim_plantid = f_prodorder.dim_plantid
		inner join dim_part dp on  f_prodorder.dim_partidheader = dp.dim_partid
where dp.ItemSubType_Merck = 'FPP';


drop table if exists tmp008_mat261_262_prodorder;
create table tmp008_mat261_262_prodorder as
select distinct f_mm.dim_plantid,
                f_mm.dim_partid,
                f_mm.dim_batchid,
                f_prodord.dd_ordernumber,
                f_prodord.dim_dateidactualheaderfinish,
                f_prodord.dim_dateidactualrelease,
                f_prodord.dim_dateidactualstart,
                f_prodord.dim_partidheader,
                f_prodord.dim_batchid as dim_batchidprod
 from fact_materialmovement f_mm,
      fact_productionorder f_prodord,
      dim_movementtype mt,
    dim_part dp,
    dim_part dph
where f_mm.dim_movementtypeid = mt.dim_movementtypeid
 and  mt.movementtype in ('261','262')
 and f_mm.dim_plantid = f_prodord.dim_plantid
 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
 and dp.dim_partid = f_mm.dim_partid
 and dph.dim_partid = f_prodord.dim_partidheader
 and dp.ItemSubType_Merck = 'RAW'
 and dph.ItemSubType_Merck = 'FPU';


drop table if exists tmp009_mat261_262_prodorder;
create table tmp009_mat261_262_prodorder as
select distinct f_mm.dim_plantid,
                f_mm.dim_partid,
                f_mm.dim_batchid,
                f_prodord.dd_ordernumber,
                f_prodord.dim_dateidactualheaderfinish,
                f_prodord.dim_dateidactualrelease,
                f_prodord.dim_dateidactualstart,
                f_prodord.dim_partidheader,
                f_prodord.dim_batchid as dim_batchidprod
 from fact_materialmovement f_mm,
      fact_productionorder f_prodord,
      dim_movementtype mt,
    dim_part dp,
    dim_part dph
where f_mm.dim_movementtypeid = mt.dim_movementtypeid
 and  mt.movementtype in ('261','262')
 and f_mm.dim_plantid = f_prodord.dim_plantid
 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
 and dp.dim_partid = f_mm.dim_partid
 and dph.dim_partid = f_prodord.dim_partidheader
 and dp.ItemSubType_Merck = 'RAW'
 and dph.ItemSubType_Merck = 'FPP';


drop table if exists tmp0010_mat261_262_prodorder;
create table tmp0010_mat261_262_prodorder as
select distinct f_mm.dim_plantid,
                f_mm.dim_partid,
                f_mm.dim_batchid,
                f_prodord.dd_ordernumber,
                f_prodord.dim_dateidactualheaderfinish,
                f_prodord.dim_dateidactualrelease,
                f_prodord.dim_dateidactualstart,
                f_prodord.dim_partidheader,
                f_prodord.dim_batchid as dim_batchidprod
 from fact_materialmovement f_mm,
      fact_productionorder f_prodord,
      dim_movementtype mt,
    dim_part dp,
    dim_part dph
where f_mm.dim_movementtypeid = mt.dim_movementtypeid
 and  mt.movementtype in ('261','262')
 and f_mm.dim_plantid = f_prodord.dim_plantid
 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
 and dp.dim_partid = f_mm.dim_partid
 and dph.dim_partid = f_prodord.dim_partidheader
 and dp.ItemSubType_Merck = 'BULK'
 and dph.ItemSubType_Merck = 'FPP';

/* drop table if exists tmp004_insplot_salesorderdelivery001
create table tmp004_insplot_salesorderdelivery001 as
select distinct f_sdeliv.dim_plantid,
                f_sdeliv.dim_plantidordering,
                f_sdeliv.dim_customeridshipto,
                f_sdeliv.dd_SalesDlvrDocNo,
                f_sdeliv.dd_SalesDlvrItemNo,
                f_sdeliv.dd_SalesDocNo,
                f_sdeliv.dd_SalesItemNo,
                f_sdeliv.dim_partid,
                f_sdeliv.dim_batchid,
                f_sdeliv.ct_QtyDelivered,
                f_sdeliv.dim_dateiddlvrdoccreated,
                f_sdeliv.dim_dateidactualgoodsissue,
                f_sdeliv.dim_dateidactualreceipt,
                f_ins.dd_inspectionlotno,
                dd_dateuserstatussamr,
                f_ins.dim_dateidinspectionstart,
               dd_dateuserstatusqcco,
                f_ins.dim_dateidusagedecisionmade,
                f_ins.ct_actuallotqty,
                 padeliv.partnumber,
                 badeliv.batchnumber,
                 pldeliv.plantcode,
                 plorddeliv.plantcode as plantcodeOrder
 from fact_inspectionlot f_ins,
      fact_salesorderdelivery f_sdeliv,
      dim_part padeliv,
      dim_batch badeliv,
      dim_plant pldeliv,
      dim_plant plorddeliv
where f_ins.dim_batchid = f_sdeliv.dim_batchid
  and f_ins.dim_partid = f_sdeliv.dim_partid
  and f_ins.dim_plantid = f_sdeliv.dim_plantid
  and padeliv.dim_partid = f_sdeliv.dim_partid
  and badeliv.dim_batchid = f_sdeliv.dim_batchid
  and pldeliv.dim_plantid = f_sdeliv.dim_plantid
  and plorddeliv.dim_plantid = f_sdeliv.dim_plantidordering */
  

drop table if exists tmp004_insplot_salesorderdelivery001;
create table tmp004_insplot_salesorderdelivery001 as
/* select distinct 
	dim_plantid,
    dim_plantidordering,
    dim_customeridshipto,
    dd_SalesDlvrDocNo,
    dd_SalesDlvrItemNo,
    dd_SalesDocNo,
    dd_SalesItemNo,
    dim_partid,
    dim_batchid,
    ct_QtyDelivered,
    dim_dateiddlvrdoccreated,
    dim_dateidactualgoodsissue,
    dim_dateidactualreceipt,
    dd_inspectionlotno,
    dd_dateuserstatussamr,
    dim_dateidinspectionstart,
    dd_dateuserstatusqcco,
    dim_dateidusagedecisionmade,
    ct_actuallotqty,
    partnumber,
    batchnumber,
    plantcode,
    plantcodeOrder

from 
( */
/* shipcomop */
select distinct f_sdeliv.dim_plantid,
                f_sdeliv.dim_plantidordering,
                f_sdeliv.dim_customeridshipto,
                f_sdeliv.dd_SalesDlvrDocNo,
                f_sdeliv.dd_SalesDlvrItemNo,
                f_sdeliv.dd_SalesDocNo,
                f_sdeliv.dd_SalesItemNo,
                f_sdeliv.dim_partid,
                f_sdeliv.dim_batchid,
                f_sdeliv.ct_QtyDelivered,
                f_sdeliv.dim_dateiddlvrdoccreated,
                f_sdeliv.dim_dateidactualgoodsissue,
                f_sdeliv.dim_dateidactualreceipt,
                f_ins.dd_inspectionlotno,
                dd_dateuserstatussamr,
                f_ins.dim_dateidinspectionstart,
               dd_dateuserstatusqcco,
                f_ins.dim_dateidusagedecisionmade,
                f_ins.ct_actuallotqty,
                 padeliv.partnumber,
                 badeliv.batchnumber,
                 pldeliv.plantcode,
                 plorddeliv.plantcode as plantcodeOrder
 from fact_inspectionlot f_ins,
      fact_salesorderdelivery f_sdeliv,
      dim_part padeliv,
	  dim_part painsp,
      dim_batch badeliv,
	  dim_batch bainsp,
      dim_plant pldeliv,
      dim_plant plorddeliv
where bainsp.batchnumber = badeliv.batchnumber 
  and painsp.partnumber = padeliv.partnumber 
  and f_ins.dim_plantid = f_sdeliv.dim_plantidordering
  and padeliv.dim_partid = f_sdeliv.dim_partid
  and painsp.dim_partid = f_ins.dim_partid
  and badeliv.dim_batchid = f_sdeliv.dim_batchid
  and bainsp.dim_batchid = f_ins.dim_batchid
  and pldeliv.dim_plantid = f_sdeliv.dim_plantid
  and plorddeliv.dim_plantid = f_sdeliv.dim_plantidordering;
  
/*BI-5540 - Shippings with a set Delivery plant (f_sdeliv.dim_plantidordering <> 1) might also be un-inspected. 
Couldn't resolve with left join to Inspection lot because of  bainsp.batchnumber = badeliv.batchnumber and painsp.partnumber = padeliv.partnumber conditions */
  /* union all */
  
  /*shipcust + un-inspected shipcomops*/
 insert into tmp004_insplot_salesorderdelivery001
(	
	dim_plantid,
    dim_plantidordering,
    dim_customeridshipto,
    dd_SalesDlvrDocNo,
    dd_SalesDlvrItemNo,
    dd_SalesDocNo,
    dd_SalesItemNo,
    dim_partid,
    dim_batchid,
    ct_QtyDelivered,
    dim_dateiddlvrdoccreated,
    dim_dateidactualgoodsissue,
    dim_dateidactualreceipt,
    dd_inspectionlotno,
    dd_dateuserstatussamr,
    dim_dateidinspectionstart,
    dd_dateuserstatusqcco,
    dim_dateidusagedecisionmade,
    ct_actuallotqty,
    partnumber,
    batchnumber,
    plantcode,
    plantcodeOrder
)
select distinct f_sdeliv.dim_plantid,
			f_sdeliv.dim_plantidordering,
			f_sdeliv.dim_customeridshipto,
			f_sdeliv.dd_SalesDlvrDocNo,
			f_sdeliv.dd_SalesDlvrItemNo,
			f_sdeliv.dd_SalesDocNo,
			f_sdeliv.dd_SalesItemNo,
			f_sdeliv.dim_partid,
			f_sdeliv.dim_batchid,
			f_sdeliv.ct_QtyDelivered,
			f_sdeliv.dim_dateiddlvrdoccreated,
			f_sdeliv.dim_dateidactualgoodsissue,
			f_sdeliv.dim_dateidactualreceipt,
			convert(varchar(12), 'Not Set') as dd_inspectionlotno,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatussamr,
			convert(integer, 1) as dim_dateidinspectionstart,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatusqcco,
			convert(integer, 1) as dim_dateidusagedecisionmade,
			convert(decimal(18,4), 0) as ct_actuallotqty,
			 padeliv.partnumber,
			 badeliv.batchnumber,
			 pldeliv.plantcode,
			 plorddeliv.plantcode as plantcodeOrder
from fact_salesorderdelivery f_sdeliv,
  dim_part padeliv,
  dim_batch badeliv,
  dim_plant pldeliv,
  dim_plant plorddeliv
where 
  padeliv.dim_partid = f_sdeliv.dim_partid
  and badeliv.dim_batchid = f_sdeliv.dim_batchid
  and pldeliv.dim_plantid = f_sdeliv.dim_plantid
  and plorddeliv.dim_plantid = f_sdeliv.dim_plantidordering
 /* and f_sdeliv.dim_plantidordering = 1 */
  and not exists
	(select 1 from tmp004_insplot_salesorderdelivery001 tmp
		where tmp.dim_plantid = f_sdeliv.dim_plantid
				and tmp.dim_plantidordering = f_sdeliv.dim_plantidordering
				and tmp.dim_partid = f_sdeliv.dim_partid
				and tmp.dim_batchid = f_sdeliv.dim_batchid
	);
  
  
  
  /* Lite shipcomops - not working properly, some records are lost in the following left join for 002, but the fact triples its size */
 /* union all
 select distinct f_sdeliv.dim_plantid,
			f_sdeliv.dim_plantidordering,
			f_sdeliv.dim_customeridshipto,
			f_sdeliv.dd_SalesDlvrDocNo,
			f_sdeliv.dd_SalesDlvrItemNo,
			f_sdeliv.dd_SalesDocNo,
			f_sdeliv.dd_SalesItemNo,
			f_sdeliv.dim_partid,
			f_sdeliv.dim_batchid,
			f_sdeliv.ct_QtyDelivered,
			f_sdeliv.dim_dateiddlvrdoccreated,
			f_sdeliv.dim_dateidactualgoodsissue,
			f_sdeliv.dim_dateidactualreceipt,
			convert(varchar(12), 'Not Set') as dd_inspectionlotno,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatussamr,
			convert(integer, 1) as dim_dateidinspectionstart,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatusqcco,
			convert(integer, 1) as dim_dateidusagedecisionmade,
			convert(decimal(18,4), 0) as ct_actuallotqty,
			 padeliv.partnumber,
			 badeliv.batchnumber,
			 pldeliv.plantcode,
			 plorddeliv.plantcode as plantcodeOrder
from fact_salesorderdelivery f_sdeliv,
  dim_part padeliv,
  dim_batch badeliv,
  dim_plant pldeliv,
  dim_plant plorddeliv,
  dim_deliverytype dt
where 
  padeliv.dim_partid = f_sdeliv.dim_partid
  and badeliv.dim_batchid = f_sdeliv.dim_batchid
  and pldeliv.dim_plantid = f_sdeliv.dim_plantid
  and plorddeliv.dim_plantid = f_sdeliv.dim_plantidordering
  and f_sdeliv.dim_plantidordering <> 1
  and f_sdeliv.dim_deliverytypeid = dt.dim_deliverytypeid
  and dt.DeliveryType = 'LF' 
  
  ) a */

drop table if exists tmp004_insplot_salesorderdelivery002;
create table tmp004_insplot_salesorderdelivery002 as
select  distinct case when shipcomop.dim_plantidordering is null then shipcust.dim_plantid
        else shipcomop.dim_plantid end as dim_plantid,
        ifnull(shipcomop.dim_plantidordering,1) as dim_plantid_comops,
        shipcust.dim_partid,
        shipcust.dim_batchid,
        shipcust.dim_customeridshipto,
        ifnull(shipcomop.dd_salesdlvrdocno,'Not Set') as dd_salesdlvrdocno_comops,
        ifnull(shipcomop.dd_salesdlvritemno,0) as dd_salesdlvritemno_comops,
        ifnull(shipcomop.dim_dateiddlvrdoccreated,1) as dim_dateiddlvrdoccreated_comops,
        ifnull(shipcomop.dim_dateidactualgoodsissue,1) as dim_dateidactualgoodsissue_comops,
        ifnull(shipcomop.dim_dateidactualreceipt,1) as dim_dateidactualreceipt_comops,
        case when shipcomop.dim_plantidordering is null then 'Not Set' else shipcomop.dd_inspectionlotno end as dd_inspectionlotno_comops,
        case when shipcomop.dim_plantidordering is null then '0001-01-01' else shipcomop.dd_dateuserstatussamr end as dd_dateuserstatussamr_comops,
        case when shipcomop.dim_plantidordering is null then 1 else shipcomop.dim_dateidinspectionstart end as dim_dateidinspectionstart_comops,
        case when shipcomop.dim_plantidordering is null then '0001-01-01' else shipcomop.dd_dateuserstatusqcco end as dd_dateuserstatusqcco_comops,
        case when shipcomop.dim_plantidordering is null then 1 else shipcomop.dim_dateidusagedecisionmade end as dim_dateidusagedecisionmade_comops,
        shipcust.dd_salesdlvrdocno as dd_salesdlvrdocno_customer,
        shipcust.dd_salesdlvritemno as dd_salesdlvritemno_customer,
        shipcust.dim_dateiddlvrdoccreated as dim_dateiddlvrdoccreated_customer,
        shipcust.dim_dateidactualgoodsissue as dim_dateidactualgoodsissue_customer,
        --ifnull(shipcomop.ct_qtydelivered,0) as  ct_qtydelivered, 
		ifnull(shipcust.ct_qtydelivered,0) as  ct_qtydelivered,
		/* Madalina 23 Sept 2016 - Adding Sales Document Number and Item, and Order Amount from the Sales Area*/ 
		shipcust.dd_SalesDocNo as dd_SalesDocNo_customer,
		shipcust.dd_SalesItemNo as dd_SalesItemNo_customer,
		shipcomop.dd_SalesDocNo as dd_SalesDocNo_comops,
		shipcomop.dd_SalesItemNo as dd_SalesItemNo_comops, 
		convert(varchar(10), 'Not Set') as basdbatchnumber,
		convert(varchar(22), 'Not Set') as pasdpartnumber
        from tmp004_insplot_salesorderdelivery001 shipcust
       left join
        (select dim_plantidordering,dim_plantid,dd_salesdlvrdocno,dd_salesdlvritemno,dim_dateiddlvrdoccreated,
         dim_dateidactualgoodsissue,dim_dateidactualreceipt,ct_qtydelivered,partnumber,batchnumber,plantcodeOrder, dd_SalesDocNo, dd_SalesItemNo,
		dd_inspectionlotno, dd_dateuserstatussamr, dim_dateidinspectionstart, dd_dateuserstatusqcco, dim_dateidusagedecisionmade
         from tmp004_insplot_salesorderdelivery001
where dim_plantidordering <> 1) shipcomop
  on shipcust.partnumber = shipcomop.partnumber
  and shipcust.batchnumber = shipcomop.batchnumber
  and shipcust.plantcode = shipcomop.plantcodeOrder
where shipcust.dim_plantidordering = 1;

update tmp004_insplot_salesorderdelivery002 b
set basdbatchnumber = basd.batchnumber
from tmp004_insplot_salesorderdelivery002 b 
	inner join dim_batch basd on b.dim_batchid = basd.dim_batchid;  

update tmp004_insplot_salesorderdelivery002 b
set pasdpartnumber = pasd.partnumber
from tmp004_insplot_salesorderdelivery002 b 
	inner join dim_part pasd on  b.dim_partid = pasd.dim_partid;

/* revert to old logic - BI-4376 */
/* drop table if exists tmp004_insplot_salesorderdelivery002
create table tmp004_insplot_salesorderdelivery002 as
select distinct dim_plantid,
			dim_plantid_comops,
			dim_partid,
			dim_batchid,
			dim_customeridshipto,
			dd_salesdlvrdocno_comops,
			dd_salesdlvritemno_comops,
			dim_dateiddlvrdoccreated_comops,
			dim_dateidactualgoodsissue_comops,
			dim_dateidactualreceipt_comops,
			dd_inspectionlotno_comops,
			dd_dateuserstatussamr_comops,
			dim_dateidinspectionstart_comops,
			dd_dateuserstatusqcco_comops,
			dim_dateidusagedecisionmade_comops,
			dd_salesdlvrdocno_customer,
			dd_salesdlvritemno_customer,
			dim_dateiddlvrdoccreated_customer,
			dim_dateidactualgoodsissue_customer,
			ct_qtydelivered,
			dd_SalesDocNo_customer,
			dd_SalesItemNo_customer,
			dd_SalesDocNo_comops,
			dd_SalesItemNo_comops
from
(
select  distinct convert( integer, case when shipcomop.dim_plantidordering is null then shipcust.dim_plantid
        else shipcomop.dim_plantid end) as dim_plantid,
        ifnull(shipcomop.dim_plantidordering,1) as dim_plantid_comops,
        shipcust.dim_partid,
        shipcust.dim_batchid,
        shipcust.dim_customeridshipto,
        ifnull(shipcomop.dd_salesdlvrdocno,'Not Set') as dd_salesdlvrdocno_comops,
        ifnull(shipcomop.dd_salesdlvritemno,0) as dd_salesdlvritemno_comops,
        ifnull(shipcomop.dim_dateiddlvrdoccreated,1) as dim_dateiddlvrdoccreated_comops,
        ifnull(shipcomop.dim_dateidactualgoodsissue,1) as dim_dateidactualgoodsissue_comops,
        ifnull(shipcomop.dim_dateidactualreceipt,1) as dim_dateidactualreceipt_comops,
        convert(varchar(12), case when shipcomop.dim_plantidordering is null then 'Not Set' else shipcomop.dd_inspectionlotno end) as dd_inspectionlotno_comops,
        to_date(case when shipcomop.dim_plantidordering is null then '0001-01-01' else shipcomop.dd_dateuserstatussamr end,  'YYYY-MM-DD') as dd_dateuserstatussamr_comops,
        convert(integer, case when shipcomop.dim_plantidordering is null then 1 else shipcomop.dim_dateidinspectionstart end) as dim_dateidinspectionstart_comops,
        to_date(case when shipcomop.dim_plantidordering is null then '0001-01-01' else shipcomop.dd_dateuserstatusqcco end, 'YYYY-MM-DD') as dd_dateuserstatusqcco_comops,
        convert(integer, case when shipcomop.dim_plantidordering is null then 1 else shipcomop.dim_dateidusagedecisionmade end) as dim_dateidusagedecisionmade_comops,
        shipcust.dd_salesdlvrdocno as dd_salesdlvrdocno_customer,
        shipcust.dd_salesdlvritemno as dd_salesdlvritemno_customer,
        shipcust.dim_dateiddlvrdoccreated as dim_dateiddlvrdoccreated_customer,
        shipcust.dim_dateidactualgoodsissue as dim_dateidactualgoodsissue_customer,
        ifnull(shipcomop.ct_qtydelivered,0) as  ct_qtydelivered, */
		/* Madalina 23 Sept 2016 - Adding Sales Document Number and Item, and Order Amount from the Sales Area*/ 
		/* shipcust.dd_SalesDocNo as dd_SalesDocNo_customer,
		shipcust.dd_SalesItemNo as dd_SalesItemNo_customer,
		shipcomop.dd_SalesDocNo as dd_SalesDocNo_comops,
		shipcomop.dd_SalesItemNo as dd_SalesItemNo_comops
        from tmp004_insplot_salesorderdelivery001 shipcust
       left join
        (select dim_plantidordering,dim_plantid,dd_salesdlvrdocno,dd_salesdlvritemno,dim_dateiddlvrdoccreated,
         dim_dateidactualgoodsissue,dim_dateidactualreceipt,ct_qtydelivered,partnumber,batchnumber,plantcodeOrder, dd_SalesDocNo, dd_SalesItemNo,
		dd_inspectionlotno, dd_dateuserstatussamr, dim_dateidinspectionstart, dd_dateuserstatusqcco, dim_dateidusagedecisionmade
         from tmp004_insplot_salesorderdelivery001
where dim_plantidordering <> 1) shipcomop
  on shipcust.partnumber = shipcomop.partnumber
  and shipcust.batchnumber = shipcomop.batchnumber
  and shipcust.plantcode = shipcomop.plantcodeOrder
where shipcust.dim_plantidordering = 1

union all

select distinct f_sdeliv.dim_plantid as dim_plantid,
			f_sdeliv.dim_plantidordering as dim_plantid_comops,
			f_sdeliv.dim_partid,
			f_sdeliv.dim_batchid,
			f_sdeliv.dim_customeridshipto,
			ifnull(f_sdeliv.dd_SalesDlvrDocNo, 'Not Set') as dd_salesdlvrdocno_comops,
			ifnull(f_sdeliv.dd_SalesDlvrItemNo, 0) as dd_salesdlvritemno_comops,
			ifnull(f_sdeliv.dim_dateiddlvrdoccreated, 1) as dim_dateiddlvrdoccreated_comops,
			ifnull(f_sdeliv.dim_dateidactualgoodsissue, 1) as dim_dateidactualgoodsissue_comops,
			ifnull(f_sdeliv.dim_dateidactualreceipt, 1) as dim_dateidactualreceipt_comops,
			convert(varchar(12), 'Not Set') as dd_inspectionlotno_comops,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatussamr_comops,
			convert(integer, 1) as dim_dateidinspectionstart_comops,
			to_date('0001-01-01', 'YYYY-MM-DD') as dd_dateuserstatusqcco_comops,
			convert(integer, 1) as dim_dateidusagedecisionmade_comops,
			convert(varchar(10), 'Not Set') as dd_salesdlvrdocno_customer,
			convert(integer, 0) as dd_salesdlvritemno_customer,
			convert(integer, 1) as dim_dateiddlvrdoccreated_customer,
			convert(integer, 1) as dim_dateidactualgoodsissue_customer,
			ifnull(f_sdeliv.ct_QtyDelivered,0) as  ct_qtydelivered,
			convert(varchar(10), 'Not Set') as dd_SalesDocNo_customer,
			convert(integer, 0) as dd_SalesItemNo_customer,
			f_sdeliv.dd_SalesDocNo as dd_SalesDocNo_comops,
			f_sdeliv.dd_SalesItemNo as dd_SalesItemNo_comops
from fact_salesorderdelivery f_sdeliv,
  dim_deliverytype dt
where 
  f_sdeliv.dim_plantidordering <> 1
  and f_sdeliv.dim_deliverytypeid = dt.dim_deliverytypeid
  and dt.DeliveryType = 'LF'
) a */

/* Combination of 2 */

/* RAW -- RAW-BULK -- BULK */
drop table if exists tmp001_002_003;
create table tmp001_002_003 as
select distinct a.dim_plantid,
       a.dd_inspectionlotno as dd_inspectionlotnot_raw,
       a.dim_partid as dim_rawpartid,
       a.dim_batchid as dim_rawbatchid,
       a.dd_DocumentNo as dd_podocumentno,
       a.dd_DocumentItemNo as dd_podocumentitemno,
	   a.dd_scheduleno,
       a.dim_dateidschedorder,
       a.dim_dateidcreate,
       a.dim_dateidpostingdate as dim_dateidrawpostingdate,
       a.dd_dateuserstatusqcco as dd_rawdateuserstatusqcco,
       a.dim_dateidinspectionstart as dim_dateidrawinspectionstart,
       a.dd_dateuserstatussamr as dd_rawdateuserstatussamr,
       a.dim_dateidusagedecisionmade as dim_dateidrawusagedecisionmade,
       b.dd_ordernumber as dd_ordernumberrawbulk,
       b.dim_partidheader as dim_bulkpartid,
       b.dim_batchidprod as dim_bulkbatchidprod,
       b.dim_dateidactualrelease as dim_dateidbulkactualrelease,
       b.dim_dateidactualstart as dim_dateidbulkactualstart,
       b.dim_dateidactualheaderfinish as dim_dateidbulkactualheaderfinish,
       c.dd_inspectionlotno as dd_inspectionlotno_bulk,
       c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_bulk,
       c.dim_dateidinspectionstart as dim_dateidinspectionstart_bulk,
       c.dd_dateuserstatussamr as dd_dateuserstatussamr_bulk,
       c.dim_dateidusagedecisionmade as dim_dateidusagedecisionmade_bulk
      from tmp001_mat101_102_insplot_purchase a
      INNER JOIN tmp002_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_partid = b.dim_partid
                                          and a.dim_batchid = b.dim_batchid
      INNER JOIN tmp003_insplot_prodorder c ON b.dim_partidheader = c.dim_partid
                                          and b.dim_plantid = c.dim_plantid
                                          and b.dim_batchidprod = c.dim_batchid
                                          and b.dd_ordernumber = c.dd_ordernumber; /* RAW - BULK */

/* Adding one more bulk step */
/* BULK -- BULK-BULK -- BULK */
drop table if exists tmp002_003_BULK_BULK;
create table tmp002_003_BULK_BULK as
select distinct a.dim_plantid,
       a.dd_inspectionlotno as dd_inspectionlotno_bulk,  /* dd_inspectionlotnot_raw */
       a.dim_partid as dim_bulkpartid, /*  dim_rawpartid */
       a.dim_batchid as dim_bulkbatchidprod, /*  dim_rawbatchid */
       /* a.dd_DocumentNo as dd_podocumentno,
       a.dd_DocumentItemNo as dd_podocumentitemno,
	   a.dd_scheduleno,
       a.dim_dateidschedorder,
       a.dim_dateidcreate,
       a.dim_dateidpostingdate as dim_dateidrawpostingdate, */
       a.dd_dateuserstatusqcco as dd_dateuserstatusqcco_bulk, /* dd_bulkdateuserstatusqcco, -- dd_rawdateuserstatusqcco, */
       a.dim_dateidinspectionstart as dim_dateidinspectionstart_bulk,  /*dim_dateidrawinspectionstart, */
       a.dd_dateuserstatussamr as dd_dateuserstatussamr_bulk, /* dd_rawdateuserstatussamr, */
       a.dim_dateidusagedecisionmade as dim_dateidusagedecisionmade_bulk, /* dim_dateidrawusagedecisionmade, */
       b.dd_ordernumber as dd_ordernumberbulk_bulk, /* dd_ordernumberrawbulk, */
       b.dim_partidheader as dim_bulkpartid_2,
       b.dim_batchidprod as dim_bulkbatchidprod_2,
       b.dim_dateidactualrelease as dim_dateidbulkactualrelease,
       b.dim_dateidactualstart as dim_dateidbulkactualstart,
       b.dim_dateidactualheaderfinish as dim_dateidbulkactualheaderfinish,
       c.dd_inspectionlotno as dd_inspectionlotno_bulk_2,
       c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_bulk_2,
       c.dim_dateidinspectionstart as dim_dateidinspectionstart_bulk_2,
       c.dd_dateuserstatussamr as dd_dateuserstatussamr_bulk_2,
       c.dim_dateidusagedecisionmade as dim_dateidusagedecisionmade_bulk_2
      from tmp003_insplot_prodorder a
      INNER JOIN tmp002_mat261_262_prodorder_BULK_BULK b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_partid = b.dim_partid
                                          and a.dim_batchid = b.dim_batchid
      INNER JOIN tmp003_insplot_prodorder c ON b.dim_partidheader = c.dim_partid
                                          and b.dim_plantid = c.dim_plantid
                                          and b.dim_batchidprod = c.dim_batchid
                                          and b.dd_ordernumber = c.dd_ordernumber; /* BULK - BULK */

/* RAW -- RAW-FPU -- FPU */
drop table if exists tmp001_008_005;
create table tmp001_008_005 as
select distinct a.dim_plantid,
       a.dd_inspectionlotno as dd_inspectionlotnot_raw,
       a.dim_partid as dim_rawpartid,
       a.dim_batchid as dim_rawbatchid,
       a.dd_DocumentNo as dd_podocumentno,
       a.dd_DocumentItemNo as dd_podocumentitemno,
     a.dd_scheduleno,
       a.dim_dateidschedorder,
       a.dim_dateidcreate,
       a.dim_dateidpostingdate as dim_dateidrawpostingdate,
       a.dd_dateuserstatusqcco as dd_rawdateuserstatusqcco,
       a.dim_dateidinspectionstart as dim_dateidrawinspectionstart,
       a.dd_dateuserstatussamr as dd_rawdateuserstatussamr,
       a.dim_dateidusagedecisionmade as dim_dateidrawusagedecisionmade,
       b.dd_ordernumber as dd_ordernumberrawfpu,
       b.dim_partidheader as dim_fpupartid,
       b.dim_batchidprod as dim_fpubatchidprod,
       b.dim_dateidactualrelease as dim_dateidfpuactualrelease,
       b.dim_dateidactualstart as dim_dateidfpuactualstart,
       b.dim_dateidactualheaderfinish as dim_dateidfpuactualheaderfinish,
       c.dd_inspectionlotno as dd_inspectionlotno_fpu,
       c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpu,
       c.dim_dateidinspectionstart as dim_dateidfpuinspectionstart,
       c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpu,
       c.dim_dateidusagedecisionmade as dim_dateidfpuusagedecisionmade
      from tmp001_mat101_102_insplot_purchase a
      INNER JOIN tmp008_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_partid = b.dim_partid
                                          and a.dim_batchid = b.dim_batchid
      INNER JOIN tmp005_insplot_prodorder c ON b.dim_partidheader = c.dim_partid
                                          and b.dim_plantid = c.dim_plantid
                                          and b.dim_batchidprod = c.dim_batchid
                                          and b.dd_ordernumber = c.dd_ordernumber; /* RAW - FPU */


/* RAW -- RAW-FPP -- FPP */
drop table if exists tmp001_009_007;
create table tmp001_009_007 as
select distinct a.dim_plantid,
       a.dd_inspectionlotno as dd_inspectionlotnot_raw,
       a.dim_partid as dim_rawpartid,
       a.dim_batchid as dim_rawbatchid,
       a.dd_DocumentNo as dd_podocumentno,
       a.dd_DocumentItemNo as dd_podocumentitemno,
     a.dd_scheduleno,
       a.dim_dateidschedorder,
       a.dim_dateidcreate,
       a.dim_dateidpostingdate as dim_dateidrawpostingdate,
       a.dd_dateuserstatusqcco as dd_rawdateuserstatusqcco,
       a.dim_dateidinspectionstart as dim_dateidrawinspectionstart,
       a.dd_dateuserstatussamr as dd_rawdateuserstatussamr,
       a.dim_dateidusagedecisionmade as dim_dateidrawusagedecisionmade,
       b.dd_ordernumber as dd_ordernumberrawfpp,
       b.dim_partidheader as dim_fpppartid,
       b.dim_batchidprod as dim_fppbatchidprod,
       b.dim_dateidactualrelease as dim_dateidfppactualrelease,
       b.dim_dateidactualstart as dim_dateidfppactualstart,
       b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
       c.dd_inspectionlotno as dd_inspectionlotno_fpp,
       c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
       c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
       c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
       c.dim_dateidusagedecisionmade as dim_dateidfppusagedecisionmade
      from tmp001_mat101_102_insplot_purchase a
      INNER JOIN tmp009_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_partid = b.dim_partid
                                          and a.dim_batchid = b.dim_batchid
      INNER JOIN tmp007_insplot_prodorder c ON b.dim_partidheader = c.dim_partid
                                          and b.dim_plantid = c.dim_plantid
                                          and b.dim_batchidprod = c.dim_batchid
                                          and b.dd_ordernumber = c.dd_ordernumber; /* RAW - FPP */


/* BULK -- BULK-FPP -- FPP */
drop table if exists tmp003_007_0010;
create table tmp003_007_0010 as
select distinct a.dim_plantid,
       a.dd_inspectionlotno as dd_inspectionlotnot_bulk,
       a.dim_partid as dim_bulkpartid,
       a.dim_batchid as dim_bulkbatchid,
       /* 'Not Set' as dd_podocumentno,
       0 as dd_podocumentitemno,
       0 AS dd_scheduleno,
       1 AS dim_dateidschedorder,
       1 AS dim_dateidcreate,
       1 as dim_dateidrawpostingdate, */
       a.dd_dateuserstatusqcco as dd_bulkdateuserstatusqcco,
       a.dim_dateidinspectionstart as dim_dateidbulkinspectionstart,
       a.dd_dateuserstatussamr as dd_bulkdateuserstatussamr,
       a.dim_dateidusagedecisionmade as dim_dateidbulkusagedecisionmade,
       b.dd_ordernumber as dd_ordernumber_bulk_fpp,
       b.dim_partidheader as dim_fpppartid,
       b.dim_batchidprod as dim_fppbatchidprod,
       b.dim_dateidactualrelease as dim_dateidfppactualrelease,
       b.dim_dateidactualstart as dim_dateidfppactualstart,
       b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
       c.dd_inspectionlotno as dd_inspectionlotno_fpp,
       c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
       c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
       c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
       c.dim_dateidusagedecisionmade as dim_dateidfppusagedecisionmade
      from tmp003_insplot_prodorder a
      INNER JOIN tmp0010_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_partid = b.dim_partid
                                          and a.dim_batchid = b.dim_batchid
      INNER JOIN tmp007_insplot_prodorder c ON b.dim_partidheader = c.dim_partid
                                          and b.dim_plantid = c.dim_plantid
                                          and b.dim_batchidprod = c.dim_batchid
                                          and b.dd_ordernumber = c.dd_ordernumber; /* BULK - FPP */

/* BULK -- BULK-FPU -- FPU */
drop table if exists tmp003_004_005;
create table tmp003_004_005 as
select distinct a.dim_plantid,
       a.dd_inspectionlotno as dd_inspectionlotnot_bulk,
       a.dim_partid as dim_bulkpartid,
       a.dim_batchid as dim_bulkbatchid,
       /*'Not Set' as dd_podocumentno,
       0 as dd_podocumentitemno,
       0 AS dd_scheduleno,
       1 AS dim_dateidschedorder,
       1 AS dim_dateidcreate,
       1 as dim_dateidrawpostingdate, */
       a.dd_dateuserstatusqcco as dd_bulkdateuserstatusqcco,
       a.dim_dateidinspectionstart as dim_dateidbulkinspectionstart,
       a.dd_dateuserstatussamr as dd_bulkdateuserstatussamr,
       a.dim_dateidusagedecisionmade as dim_dateidbulkusagedecisionmade,
       b.dd_ordernumber as dd_ordernumber_bulk_fpu,
       b.dim_partidheader as dim_fpupartid,
       b.dim_batchidprod as dim_fpubatchidprod,
       b.dim_dateidactualrelease as dim_dateidfpuactualrelease,
       b.dim_dateidactualstart as dim_dateidfpuactualstart,
       b.dim_dateidactualheaderfinish as dim_dateidfpuactualheaderfinish,
       c.dd_inspectionlotno as dd_inspectionlotno_fpu,
       c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpu,
       c.dim_dateidinspectionstart as dim_dateidfpuinspectionstart,
       c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpu,
       c.dim_dateidusagedecisionmade as dim_dateidfpuusagedecisionmade
      from tmp003_insplot_prodorder a
      INNER JOIN tmp004_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_partid = b.dim_partid
                                          and a.dim_batchid = b.dim_batchid
      INNER JOIN tmp005_insplot_prodorder c ON b.dim_partidheader = c.dim_partid
                                          and b.dim_plantid = c.dim_plantid
                                          and b.dim_batchidprod = c.dim_batchid
                                          and b.dd_ordernumber = c.dd_ordernumber; /* BULK - FPU */


/* FPU -- FPU-FPP -- FPP */
drop table if exists tmp005_006_007;
create table tmp005_006_007 as
select distinct a.dim_plantid,
       a.dd_inspectionlotno as dd_inspectionlotnot_fpu,
       a.dim_partid as dim_fpupartid,
       a.dim_batchid as dim_fpubatchid,
       /* 'Not Set' as dd_podocumentno,
       0 as dd_podocumentitemno,
       0 AS dd_scheduleno,
       1 AS dim_dateidschedorder,
       1 AS dim_dateidcreate,
       1 as dim_dateidrawpostingdate, */
       a.dd_dateuserstatusqcco as dd_fpudateuserstatusqcco,
       a.dim_dateidinspectionstart as dim_dateidfpuinspectionstart,
       a.dd_dateuserstatussamr as dd_fpudateuserstatussamr,
       a.dim_dateidusagedecisionmade as dim_dateidfpuusagedecisionmade,
       b.dd_ordernumber as dd_ordernumber_fpu_fpp,
       b.dim_partidheader as dim_fpppartid,
       b.dim_batchidprod as dim_fppbatchidprod,
       b.dim_dateidactualrelease as dim_dateidfppactualrelease,
       b.dim_dateidactualstart as dim_dateidfppactualstart,
       b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
       c.dd_inspectionlotno as dd_inspectionlotno_fpp,
       c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
       c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
       c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
       c.dim_dateidusagedecisionmade as dim_dateidfppusagedecisionmade
      from tmp005_insplot_prodorder a
      INNER JOIN tmp006_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_partid = b.dim_partid
                                          and a.dim_batchid = b.dim_batchid
      INNER JOIN tmp007_insplot_prodorder c ON b.dim_partidheader = c.dim_partid
                                          and b.dim_plantid = c.dim_plantid
                                          and b.dim_batchidprod = c.dim_batchid
                                          and b.dd_ordernumber = c.dd_ordernumber; /* FPU - FPP */


/* Combination of 3 */

/* (RAW -- RAW-BULK -- BULK) -- BULK-FPU -- FPU */
drop table if exists tmp001_002_003_004_005;
create table tmp001_002_003_004_005 as
select distinct a.*,
                b.dd_ordernumber as dd_ordernumber_bulk_fpu,
                b.dim_partidheader as dim_fpupartid,
                b.dim_batchidprod as dim_fpubatchidprod,
                b.dim_dateidactualrelease as dim_dateidfpuactualrelease,
                b.dim_dateidactualstart as dim_dateidfpuactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfpuactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpu,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpu,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpu,
                c.dim_dateidinspectionstart as dim_dateidfpuinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfpuusagedecisionmade
               from  tmp001_002_003 a
               INNER JOIN tmp004_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_bulkpartid = b.dim_partid
                                          and a.dim_bulkbatchidprod = b.dim_batchid
               INNER JOIN tmp005_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* RAW - BULK - FPU */

/*---------------------------------------------------------*/
/* Adding one more bulk step */										  
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK */
drop table if exists tmp001_002_003_BULK_BULK;
create table tmp001_002_003_BULK_BULK as
select distinct a.*,
                b.dd_ordernumber as dd_ordernumberbulk_bulk,
                b.dim_partidheader as dim_bulkpartid_2,
                b.dim_batchidprod as dim_bulkbatchidprod_2,
                b.dim_dateidactualrelease as dim_dateidbulk_2actualrelease, 
                b.dim_dateidactualstart as dim_dateidbulk_2actualstart,
                b.dim_dateidactualheaderfinish as dim_dateidbulk_2actualheaderfinish, 
                c.dd_inspectionlotno as dd_inspectionlotno_bulk_2,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_bulk_2,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_bulk_2,
                c.dim_dateidinspectionstart as dim_dateidinspectionstart_bulk_2,
                c.dim_Dateidusagedecisionmade as dim_dateidusagedecisionmade_bulk_2
               from  tmp001_002_003 a
               INNER JOIN tmp002_mat261_262_prodorder_BULK_BULK b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_bulkpartid = b.dim_partid
                                          and a.dim_bulkbatchidprod = b.dim_batchid
               INNER JOIN tmp003_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* RAW - BULK - BULK */	

/* Adding one more bulk step */	
/* (BULK -- BULK-BULK -- BULK) -- BULK-FPU --FPU */ 
drop table if exists tmp002_003_004_005_BULK_BULK;
create table tmp002_003_004_005_BULK_BULK as
select distinct a.*,
                b.dd_ordernumber as dd_ordernumber_bulk_2_fpu,
                b.dim_partidheader as dim_fpupartid,
                b.dim_batchidprod as dim_fpubatchidprod,
                b.dim_dateidactualrelease as dim_dateidfpuactualrelease,
                b.dim_dateidactualstart as dim_dateidfpuactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfpuactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpu,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpu,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpu,
                c.dim_dateidinspectionstart as dim_dateidfpuinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfpuusagedecisionmade
               from  tmp002_003_BULK_BULK a
               INNER JOIN tmp004_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_bulkpartid_2 = b.dim_partid
                                          and a.dim_bulkbatchidprod_2 = b.dim_batchid
               INNER JOIN tmp005_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* BULK - BULK - FPU */		

/* Adding one more bulk step */	
/* (BULK -- BULK-BULK -- BULK) -- BULK-FPP --FPP */  	
drop table if exists tmp002_003_007_010_BULK_BULK;
create table tmp002_003_007_010_BULK_BULK as
select distinct a.*,
                b.dd_ordernumber as dd_ordernumber_bulk_2_fpp,
                b.dim_partidheader as dim_fpppartid,
                b.dim_batchidprod as dim_fppbatchidprod,
                b.dim_dateidactualrelease as dim_dateidfppactualrelease,
                b.dim_dateidactualstart as dim_dateidfppactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpp,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
                c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
               from  tmp002_003_BULK_BULK a
               INNER JOIN tmp0010_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_bulkpartid_2 = b.dim_partid
                                          and a.dim_bulkbatchidprod_2 = b.dim_batchid
               INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* BULK - BULK - FPP */							  
/*---------------------------------------------------------*/

/* Adding the third bulk step - BI-5623*/
drop table if exists tmp002_003_BULK_BULK_BULK;
create table tmp002_003_BULK_BULK_BULK as
select distinct a.*,
				b.dd_ordernumber as dd_ordernumberbulk_2_bulk_3,
                b.dim_partidheader as dim_bulkpartid_3,
                b.dim_batchidprod as dim_bulkbatchidprod_3,
                b.dim_dateidactualrelease as dim_dateidbulk_3actualrelease,
                b.dim_dateidactualstart as dim_dateidbulk_3actualstart,
                b.dim_dateidactualheaderfinish as dim_dateidbulk_3actualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_bulk_3,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_bulk_3,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_bulk_3,
                c.dim_dateidinspectionstart as dim_dateidinspectionstart_bulk_3,
                c.dim_Dateidusagedecisionmade as dim_dateidusagedecisionmade_bulk_3
			   from  tmp002_003_BULK_BULK a
               INNER JOIN tmp002_mat261_262_prodorder_BULK_BULK b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_bulkpartid_2 = b.dim_partid
                                          and a.dim_bulkbatchidprod_2 = b.dim_batchid
               INNER JOIN tmp003_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* BULK - BULK - BULK */	
/*-----------------------------------------------------------*/

/* (RAW -- RAW-BULK -- BULK) -- BULK-FPP -- FPP */
drop table if exists tmp001_002_003_004_007;
create table tmp001_002_003_004_007 as
select distinct a.*,
                b.dd_ordernumber as dd_ordernumber_bulk_fpp,
                b.dim_partidheader as dim_fpppartid,
                b.dim_batchidprod as dim_fppbatchidprod,
                b.dim_dateidactualrelease as dim_dateidfppactualrelease,
                b.dim_dateidactualstart as dim_dateidfppactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpp,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
                c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
               from  tmp001_002_003 a
			   /*8 Febr correction: Since the link is BULK-FPP, use tmp0010_mat261_262_prodorder instead of tmp004_mat261_262_prodorder*/
               /*INNER JOIN tmp004_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid */   
				INNER JOIN tmp0010_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid 			   
                                          and a.dim_bulkpartid = b.dim_partid
                                          and a.dim_bulkbatchidprod = b.dim_batchid
               INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* RAW - BULK - FPP */

/* (RAW -- RAW-FPU -- FPU) -- FPU-FPP -- FPP */
drop table if exists tmp008_006_007;
create table tmp008_006_007 as
select distinct a.*,
                b.dd_ordernumber as dd_ordernumber_fpu_fpp,
                b.dim_partidheader as dim_fpppartid,
                b.dim_batchidprod as dim_fppbatchidprod,
                b.dim_dateidactualrelease as dim_dateidfppactualrelease,
                b.dim_dateidactualstart as dim_dateidfppactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpp,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
                c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
               from  tmp001_008_005 a
               INNER JOIN tmp006_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_fpupartid = b.dim_partid
                                          and a.dim_fpubatchidprod = b.dim_batchid
               INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* RAW - FPU - FPP */


/* (BULK -- BULK-FPU -- FPU) -- FPU-FPP -- FPP */
drop table if exists tmp003_006_007;
create table tmp003_006_007 as
select distinct a.*,
                b.dd_ordernumber as dd_ordernumber_fpu_fpp,
                b.dim_partidheader as dim_fpppartid,
                b.dim_batchidprod as dim_fppbatchidprod,
                b.dim_dateidactualrelease as dim_dateidfppactualrelease,
                b.dim_dateidactualstart as dim_dateidfppactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpp,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
                c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
               from  tmp003_004_005 a
               INNER JOIN tmp006_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_fpupartid = b.dim_partid
                                          and a.dim_fpubatchidprod = b.dim_batchid
               INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* BULK - FPU - FPP */



/* Combination of 4 */
/* (RAW -- RAW-BULK -- BULK) -- BULK-FPU -- FPU  ---(FPU-FPP)  --FPP*/
drop table if exists tmp001_002_003_004_005_006_007;
create table tmp001_002_003_004_005_006_007 as
select  distinct a.*,
             b.dd_ordernumber as dd_ordernumber_fpu_fpp,
                b.dim_partidheader as dim_fpppartid,
                b.dim_batchidprod as dim_fppbatchidprod,
                b.dim_dateidactualrelease as dim_dateidfppactualrelease,
                b.dim_dateidactualstart as dim_dateidfppactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpp,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
                c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
             from  tmp001_002_003_004_005 a
             INNER JOIN tmp006_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_fpupartid = b.dim_partid
                                          and a.dim_fpubatchidprod = b.dim_batchid
             INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* RAW - BULK - FPU - FPP */

/*---------------------------------------------------------------------------------------------------*/ 
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-FPU)  --FPU*/  
/* Adding one more bulk step */
drop table if exists tmp001_002_003_004_005_BULK_BULK;
create table tmp001_002_003_004_005_BULK_BULK as
select  distinct a.*,
             b.dd_ordernumber as dd_ordernumber_bulk_2_fpu,
                b.dim_partidheader as dim_fpupartid,
                b.dim_batchidprod as dim_fpubatchidprod,
                b.dim_dateidactualrelease as dim_dateidfpuactualrelease,
                b.dim_dateidactualstart as dim_dateidfpuactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfpuactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpu,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpu,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpu,
                c.dim_dateidinspectionstart as dim_dateidfpuinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfpuusagedecisionmade
             from  tmp001_002_003_BULK_BULK a
             INNER JOIN tmp004_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_bulkpartid_2 = b.dim_partid
                                          and a.dim_bulkbatchidprod_2 = b.dim_batchid
             INNER JOIN tmp005_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* RAW - BULK - BULK - FPU */

/* ( BULK --  BULK-BULK -- BULK) -- BULK-FPU -- FPU  ---(FPU-FPP)  --FPP*/  
/* Adding one more bulk step */
drop table if exists tmp002_003_004_005_006_007_BULK_BULK;
create table tmp002_003_004_005_006_007_BULK_BULK as
select  distinct a.*,
				b.dd_ordernumber as dd_ordernumber_fpu_fpp,
                b.dim_partidheader as dim_fpppartid,
                b.dim_batchidprod as dim_fppbatchidprod,
                b.dim_dateidactualrelease as dim_dateidfppactualrelease,
                b.dim_dateidactualstart as dim_dateidfppactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpp,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
                c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
             from  tmp002_003_004_005_BULK_BULK a
             INNER JOIN tmp006_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_fpupartid = b.dim_partid
                                          and a.dim_fpubatchidprod = b.dim_batchid
             INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* BULK - BULK - FPU - FPP*/

/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-FPP)  --FPP*/  
/* Adding one more bulk step */
drop table if exists tmp001_002_003_007_010_BULK_BULK;
create table tmp001_002_003_007_010_BULK_BULK as
select  distinct a.*,
             b.dd_ordernumber as dd_ordernumber_bulk_2_fpp,
                b.dim_partidheader as dim_fpppartid,
                b.dim_batchidprod as dim_fppbatchidprod,
                b.dim_dateidactualrelease as dim_dateidfppactualrelease,
                b.dim_dateidactualstart as dim_dateidfppactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpp,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
                c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
             from  tmp001_002_003_BULK_BULK a
             INNER JOIN tmp0010_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_bulkpartid_2 = b.dim_partid
                                          and a.dim_bulkbatchidprod_2 = b.dim_batchid
             INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* RAW - BULK - BULK - FPP */										  
/*---------------------------------------------------------------------------------------------------*/ 
/* Adding the third bulk step - BI-5623*/
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-BULK)  --BULK*/  	
drop table if exists tmp001_002_003_BULK_BULK_BULK;
create table tmp001_002_003_BULK_BULK_BULK as
select  distinct a.*,
			b.dd_ordernumber as dd_ordernumberbulk_2_bulk_3,
			b.dim_partidheader as dim_bulkpartid_3,
			b.dim_batchidprod as dim_bulkbatchidprod_3,
			b.dim_dateidactualrelease as dim_dateidbulk_3actualrelease,
			b.dim_dateidactualstart as dim_dateidbulk_3actualstart,
			b.dim_dateidactualheaderfinish as dim_dateidbulk_3actualheaderfinish,
			c.dd_inspectionlotno as dd_inspectionlotno_bulk_3,
			c.dd_dateuserstatussamr as dd_dateuserstatussamr_bulk_3,
			c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_bulk_3,
			c.dim_dateidinspectionstart as dim_dateidinspectionstart_bulk_3,
			c.dim_Dateidusagedecisionmade as dim_dateidusagedecisionmade_bulk_3
		 from  tmp001_002_003_BULK_BULK a
		 INNER JOIN tmp002_mat261_262_prodorder_BULK_BULK b ON a.dim_plantid = b.dim_plantid
									  and a.dim_bulkpartid_2 = b.dim_partid
									  and a.dim_bulkbatchidprod_2 = b.dim_batchid
		 INNER JOIN tmp003_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
									  and c.dim_batchid = b.dim_batchidprod
									  and c.dim_plantid = a.dim_plantid
									  and c.dd_ordernumber = b.dd_ordernumber; /* RAW - BULK - BULK - BULK */
									  
/* (BULK -- BULK-BULK -- BULK) -- BULK-BULK --BULK --(BULK-FPU) - FPU */  
drop table if exists tmp002_003_004_005_BULK_BULK_BULK;
create table tmp002_003_004_005_BULK_BULK_BULK as
select  distinct a.*,
			b.dd_ordernumber as dd_ordernumber_bulk_3_fpu,
			b.dim_partidheader as dim_fpupartid,
			b.dim_batchidprod as dim_fpubatchidprod,
			b.dim_dateidactualrelease as dim_dateidfpuactualrelease,
			b.dim_dateidactualstart as dim_dateidfpuactualstart,
			b.dim_dateidactualheaderfinish as dim_dateidfpuactualheaderfinish,
			c.dd_inspectionlotno as dd_inspectionlotno_fpu,
			c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpu,
			c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpu,
			c.dim_dateidinspectionstart as dim_dateidfpuinspectionstart,
			c.dim_Dateidusagedecisionmade as dim_Dateidfpuusagedecisionmade
		 from  tmp002_003_BULK_BULK_BULK a
		 INNER JOIN tmp004_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
									  and a.dim_bulkpartid_3 = b.dim_partid
									  and a.dim_bulkbatchidprod_3 = b.dim_batchid
		 INNER JOIN tmp005_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
									  and c.dim_batchid = b.dim_batchidprod
									  and c.dim_plantid = a.dim_plantid
									  and c.dd_ordernumber = b.dd_ordernumber;	/*  bulk - bulk - bulk - FPU */		

/* (BULK -- BULK-BULK -- BULK) -- BULK-BULK --BULK --(BULK-FPP) - FPP */  
drop table if exists tmp002_003_007_010_BULK_BULK_BULK;
create table tmp002_003_007_010_BULK_BULK_BULK as
select  distinct a.*,
			b.dd_ordernumber as dd_ordernumber_bulk_3_fpp,
			b.dim_partidheader as dim_fpppartid,
			b.dim_batchidprod as dim_fppbatchidprod,
			b.dim_dateidactualrelease as dim_dateidfppactualrelease,
			b.dim_dateidactualstart as dim_dateidfppactualstart,
			b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
			c.dd_inspectionlotno as dd_inspectionlotno_fpp,
			c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
			c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
			c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
			c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
		 from  tmp002_003_BULK_BULK_BULK a
		 INNER JOIN tmp0010_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
									  and a.dim_bulkpartid_3 = b.dim_partid
									  and a.dim_bulkbatchidprod_3 = b.dim_batchid
		 INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
									  and c.dim_batchid = b.dim_batchidprod
									  and c.dim_plantid = a.dim_plantid
									  and c.dd_ordernumber = b.dd_ordernumber;	/*  bulk - bulk - bulk - FPP */		 


/*--------------------------------------*/

/* Combination of 5 */

/* (RAW -- RAW-BULK -- BULK) -- (BULK-BULK) -- BULK  ---(BULK-FPU)  --FPU - (FPU-FPP) - FPP*/ 
/* Adding one more bulk step */
drop table if exists tmp001_002_003_004_005_006_007_BULK_BULK;
create table tmp001_002_003_004_005_006_007_BULK_BULK as
select  distinct a.*,
				b.dd_ordernumber as dd_ordernumber_fpu_fpp,
                b.dim_partidheader as dim_fpppartid,
                b.dim_batchidprod as dim_fppbatchidprod,
                b.dim_dateidactualrelease as dim_dateidfppactualrelease,
                b.dim_dateidactualstart as dim_dateidfppactualstart,
                b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
                c.dd_inspectionlotno as dd_inspectionlotno_fpp,
                c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
                c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
                c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
                c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
             from  tmp001_002_003_004_005_BULK_BULK a
             INNER JOIN tmp006_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
                                          and a.dim_fpupartid = b.dim_partid
                                          and a.dim_fpubatchidprod = b.dim_batchid
             INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
                                          and c.dim_batchid = b.dim_batchidprod
                                          and c.dim_plantid = a.dim_plantid
                                          and c.dd_ordernumber = b.dd_ordernumber; /* RAW -BULK - BULK - FPU - FPP*/
										  										  
/* Adding the third bulk step - BI-5623*/										  	
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-BULK)  --BULK -(BULK-FPU) - FPU*/ 
drop table if exists tmp001_002_003_004_005_BULK_BULK_BULK;
create table tmp001_002_003_004_005_BULK_BULK_BULK as
select  distinct a.*,
				b.dd_ordernumber as dd_ordernumber_bulk_3_fpu,
				b.dim_partidheader as dim_fpupartid,
				b.dim_batchidprod as dim_fpubatchidprod,
				b.dim_dateidactualrelease as dim_dateidfpuactualrelease,
				b.dim_dateidactualstart as dim_dateidfpuactualstart,
				b.dim_dateidactualheaderfinish as dim_dateidfpuactualheaderfinish,
				c.dd_inspectionlotno as dd_inspectionlotno_fpu,
				c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpu,
				c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpu,
				c.dim_dateidinspectionstart as dim_dateidfpuinspectionstart,
				c.dim_Dateidusagedecisionmade as dim_Dateidfpuusagedecisionmade
	 from  tmp001_002_003_BULK_BULK_BULK a
	 INNER JOIN tmp004_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
								  and a.dim_bulkpartid_3 = b.dim_partid
								  and a.dim_bulkbatchidprod_3 = b.dim_batchid
	 INNER JOIN tmp005_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
								  and c.dim_batchid = b.dim_batchidprod
								  and c.dim_plantid = a.dim_plantid
								  and c.dd_ordernumber = b.dd_ordernumber; /* RAW-BULK-BULK-BULK-FPU */
								  							
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-BULK)  --BULK -(BULK-FPP) - FPP*/ 
drop table if exists tmp001_002_003_007_010_BULK_BULK_BULK;
create table tmp001_002_003_007_010_BULK_BULK_BULK as
select  distinct a.*,
		b.dd_ordernumber as dd_ordernumber_bulk_3_fpp,
		b.dim_partidheader as dim_fpppartid,
		b.dim_batchidprod as dim_fppbatchidprod,
		b.dim_dateidactualrelease as dim_dateidfppactualrelease,
		b.dim_dateidactualstart as dim_dateidfppactualstart,
		b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
		c.dd_inspectionlotno as dd_inspectionlotno_fpp,
		c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
		c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
		c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
		c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
	 from  tmp001_002_003_BULK_BULK_BULK a
		 INNER JOIN tmp0010_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
									  and a.dim_bulkpartid_3 = b.dim_partid
									  and a.dim_bulkbatchidprod_3 = b.dim_batchid
		 INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
									  and c.dim_batchid = b.dim_batchidprod
									  and c.dim_plantid = a.dim_plantid
									  and c.dd_ordernumber = b.dd_ordernumber;	/* RAW-BULK-BULK-BULK-FPP */

/* (BULK -- BULK-BULK -- BULK) -- BULK-BULK --BULK --(BULK-FPU) - FPU -- (FPU-FPP) - FPP*/ 	
drop table if exists tmp002_003_004_005_006_007_BULK_BULK_BULK;
create table tmp002_003_004_005_006_007_BULK_BULK_BULK as
select  distinct a.*,
		b.dd_ordernumber as dd_ordernumber_fpu_fpp,
		b.dim_partidheader as dim_fpppartid,
		b.dim_batchidprod as dim_fppbatchidprod,
		b.dim_dateidactualrelease as dim_dateidfppactualrelease,
		b.dim_dateidactualstart as dim_dateidfppactualstart,
		b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
		c.dd_inspectionlotno as dd_inspectionlotno_fpp,
		c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
		c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
		c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
		c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
	 from  tmp002_003_004_005_BULK_BULK_BULK a
		 INNER JOIN tmp006_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
									  and a.dim_fpupartid = b.dim_partid
									  and a.dim_fpubatchidprod = b.dim_batchid
		 INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
									  and c.dim_batchid = b.dim_batchidprod
									  and c.dim_plantid = a.dim_plantid
									  and c.dd_ordernumber = b.dd_ordernumber;     /* BULK-BULK-BULK-FPU-FPP */

	
/*---------------------------------------------------------------------------------------------------*/ 

/* Combination of 6 */

/* Adding the third bulk step - BI-5623*/
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-BULK)  --BULK -(BULK-FPU) - FPU - (FPU-FPP) - FPP*/ 
drop table if exists tmp001_002_003_004_005_006_007_BULK_BULK_BULK;
create table tmp001_002_003_004_005_006_007_BULK_BULK_BULK as
select  distinct a.*,
		b.dd_ordernumber as dd_ordernumber_fpu_fpp,
		b.dim_partidheader as dim_fpppartid,
		b.dim_batchidprod as dim_fppbatchidprod,
		b.dim_dateidactualrelease as dim_dateidfppactualrelease,
		b.dim_dateidactualstart as dim_dateidfppactualstart,
		b.dim_dateidactualheaderfinish as dim_dateidfppactualheaderfinish,
		c.dd_inspectionlotno as dd_inspectionlotno_fpp,
		c.dd_dateuserstatussamr as dd_dateuserstatussamr_fpp,
		c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_fpp,
		c.dim_dateidinspectionstart as dim_dateidfppinspectionstart,
		c.dim_Dateidusagedecisionmade as dim_Dateidfppusagedecisionmade
from tmp001_002_003_004_005_BULK_BULK_BULK a
	INNER JOIN tmp006_mat261_262_prodorder b ON a.dim_plantid = b.dim_plantid
									  and a.dim_fpupartid = b.dim_partid
									  and a.dim_fpubatchidprod = b.dim_batchid
	INNER JOIN tmp007_insplot_prodorder c ON c.dim_partid =  b.dim_partidheader
									  and c.dim_batchid = b.dim_batchidprod
									  and c.dim_plantid = a.dim_plantid
									  and c.dd_ordernumber = b.dd_ordernumber;  /* RAW-BULK-BULK-BULK-FPU-FPP */
/*---------------------------------------------------------------------------------------------------*/ 


/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------begin the inserts into temp fact ----------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/* drop table if exists fact_mmprodhierarchy */

/* Use a temporary table and rename it to the fact in the end */
/* truncate table fact_mmprodhierarchy */

DROP TABLE IF EXISTS fact_mmprodhierarchy_tmp_populate;
create table fact_mmprodhierarchy_tmp_populate 
like fact_mmprodhierarchy INCLUDING DEFAULTS INCLUDING IDENTITY;


/*---------------------------------------------------------------------------------------------------*/ 
/* Combination of 6 */

/* Adding the third bulk step - BI-5623*/
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-BULK)  --BULK -(BULK-FPU) - FPU - (FPU-FPP) - FPP*/ 
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULK_2ACTUALRELEASE,
	DIM_DATEIDBULK_2ACTUALSTART,
	DIM_DATEIDBULK_2ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
	DD_ORDERNUMBER_BULK_3_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE,
	DD_ORDERNUMBER_FPU_FPP,
	DIM_FPPPARTID,
	DIM_FPPBATCHIDPROD,
	DIM_DATEIDFPPACTUALRELEASE,
	DIM_DATEIDFPPACTUALSTART,
	DIM_DATEIDFPPACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPP,
	DD_DATEUSERSTATUSSAMR_FPP,
	DD_DATEUSERSTATUSQCCO_FPP,
	DIM_DATEIDFPPINSPECTIONSTART,
	DIM_DATEIDFPPUSAGEDECISIONMADE,
	dim_plantid_comops,
	dd_salesdlvrdocno_comops,
	dd_salesdlvritemno_comops,
	dim_dateiddlvrdoccreated_comops,
	dim_dateidactualgoodsissue_comops,
	dim_dateidactualreceipt_comops,
	dd_inspectionlotno_comops,
	dd_dateuserstatussamr_comops,
	dim_dateidinspectionstart_comops,
	dd_dateuserstatusqcco_comops,
	dim_Dateidusagedecisionmade_comops,
	dd_salesdlvrdocno_customer,
	dd_salesdlvritemno_customer,
	dim_dateiddlvrdoccreated_customer,
	dim_dateidactualgoodsissue_customer,
	ct_qtydelivered,
	dd_SalesDocNo_customer,
	dd_SalesItemNo_customer
)
SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
	a.*,	
	ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
	ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
	ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
	ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
	ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
	ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
	ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
	ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
	ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
	ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
	ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
	ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
	ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
	ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
	ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
	ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
	ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
	ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer
from tmp001_002_003_004_005_006_007_BULK_BULK_BULK a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
														and bafpp.batchnumber = b.basdbatchnumber 
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod
AND m.dim_bulkpartid_3 = a.dim_bulkpartid_3
AND m.dim_bulkbatchidprod_3 = a.dim_bulkbatchidprod_3);   /* RAW-BULK-BULK-BULK-FPU-FPP */
	


/*--------------------------------------------------------------------------------------------*/
/* Combination of 5 */

/* Adding one more bulk step */
/* (RAW -- RAW-BULK -- BULK) -- (BULK-BULK) -- BULK  ---(BULK-FPU)  --FPU - (FPU-FPP) - FPP*/ 
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULK_2ACTUALRELEASE,
	DIM_DATEIDBULK_2ACTUALSTART,
	DIM_DATEIDBULK_2ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBER_BULK_2_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE,
	DD_ORDERNUMBER_FPU_FPP,
	DIM_FPPPARTID,
	DIM_FPPBATCHIDPROD,
	DIM_DATEIDFPPACTUALRELEASE,
	DIM_DATEIDFPPACTUALSTART,
	DIM_DATEIDFPPACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPP,
	DD_DATEUSERSTATUSSAMR_FPP,
	DD_DATEUSERSTATUSQCCO_FPP,
	DIM_DATEIDFPPINSPECTIONSTART,
	DIM_DATEIDFPPUSAGEDECISIONMADE,
	dim_plantid_comops,
	dd_salesdlvrdocno_comops,
	dd_salesdlvritemno_comops,
	dim_dateiddlvrdoccreated_comops,
	dim_dateidactualgoodsissue_comops,
	dim_dateidactualreceipt_comops,
	dd_inspectionlotno_comops,
	dd_dateuserstatussamr_comops,
	dim_dateidinspectionstart_comops,
	dd_dateuserstatusqcco_comops,
	dim_Dateidusagedecisionmade_comops,
	dd_salesdlvrdocno_customer,
	dd_salesdlvritemno_customer,
	dim_dateiddlvrdoccreated_customer,
	dim_dateidactualgoodsissue_customer,
	ct_qtydelivered,
	dd_SalesDocNo_customer,
	dd_SalesItemNo_customer
)
SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
	a.*,	
	ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
	ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
	ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
	ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
	ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
	ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
	ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
	ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
	ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
	ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
	ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
	ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
	ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
	ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
	ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
	ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
	ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
	ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer
from tmp001_002_003_004_005_006_007_BULK_BULK a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
														and bafpp.batchnumber = b.basdbatchnumber 
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);  /*RAW - BULK - BULK - FPU - FPP*/

/* Adding the third bulk step - BI-5623*/										  	
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-BULK)  --BULK -(BULK-FPU) - FPU*/ 
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULK_2ACTUALRELEASE,
	DIM_DATEIDBULK_2ACTUALSTART,
	DIM_DATEIDBULK_2ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
	DD_ORDERNUMBER_BULK_3_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE
)
SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULK_2ACTUALRELEASE,
	DIM_DATEIDBULK_2ACTUALSTART,
	DIM_DATEIDBULK_2ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
	DD_ORDERNUMBER_BULK_3_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE
from tmp001_002_003_004_005_BULK_BULK_BULK a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_3 = a.dim_bulkpartid_3
AND m.dim_bulkbatchidprod_3 = a.dim_bulkbatchidprod_3
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod); /*raw-bulk-bulk-bulk-fpu*/


/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-BULK)  --BULK -(BULK-FPP) - FPP*/ 
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULK_2ACTUALRELEASE,
	DIM_DATEIDBULK_2ACTUALSTART,
	DIM_DATEIDBULK_2ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
	DD_ORDERNUMBER_BULK_3_FPP,
	DIM_FPPPARTID,
	DIM_FPPBATCHIDPROD,
	DIM_DATEIDFPPACTUALRELEASE,
	DIM_DATEIDFPPACTUALSTART,
	DIM_DATEIDFPPACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPP,
	DD_DATEUSERSTATUSSAMR_FPP,
	DD_DATEUSERSTATUSQCCO_FPP,
	DIM_DATEIDFPPINSPECTIONSTART,
	DIM_DATEIDFPPUSAGEDECISIONMADE,
	dim_plantid_comops,
	dd_salesdlvrdocno_comops,
	dd_salesdlvritemno_comops,
	dim_dateiddlvrdoccreated_comops,
	dim_dateidactualgoodsissue_comops,
	dim_dateidactualreceipt_comops,
	dd_inspectionlotno_comops,
	dd_dateuserstatussamr_comops,
	dim_dateidinspectionstart_comops,
	dd_dateuserstatusqcco_comops,
	dim_Dateidusagedecisionmade_comops,
	dd_salesdlvrdocno_customer,
	dd_salesdlvritemno_customer,
	dim_dateiddlvrdoccreated_customer,
	dim_dateidactualgoodsissue_customer,
	ct_qtydelivered,
	dd_SalesDocNo_customer,
	dd_SalesItemNo_customer
)
SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
	a.*,	
	ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
	ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
	ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
	ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
	ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
	ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
	ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
	ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
	ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
	ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
	ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
	ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
	ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
	ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
	ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
	ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
	ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
	ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer
from tmp001_002_003_007_010_BULK_BULK_BULK a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
														and bafpp.batchnumber = b.basdbatchnumber 
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2
AND m.dim_bulkpartid_3 = a.dim_bulkpartid_3
AND m.dim_bulkbatchidprod_3 = a.dim_bulkbatchidprod_3
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);  	 /*RAW-BULK-BULK-BULK-FPP*/
	
/* (BULK -- BULK-BULK -- BULK) -- BULK-BULK --BULK --(BULK-FPU) - FPU -- (FPU-FPP) - FPP*/ 	
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNO_BULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
	DD_ORDERNUMBER_BULK_3_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE,
	DD_ORDERNUMBER_FPU_FPP,
	DIM_FPPPARTID,
	DIM_FPPBATCHIDPROD,
	DIM_DATEIDFPPACTUALRELEASE,
	DIM_DATEIDFPPACTUALSTART,
	DIM_DATEIDFPPACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPP,
	DD_DATEUSERSTATUSSAMR_FPP,
	DD_DATEUSERSTATUSQCCO_FPP,
	DIM_DATEIDFPPINSPECTIONSTART,
	DIM_DATEIDFPPUSAGEDECISIONMADE,
	dim_plantid_comops,
	dd_salesdlvrdocno_comops,
	dd_salesdlvritemno_comops,
	dim_dateiddlvrdoccreated_comops,
	dim_dateidactualgoodsissue_comops,
	dim_dateidactualreceipt_comops,
	dd_inspectionlotno_comops,
	dd_dateuserstatussamr_comops,
	dim_dateidinspectionstart_comops,
	dd_dateuserstatusqcco_comops,
	dim_Dateidusagedecisionmade_comops,
	dd_salesdlvrdocno_customer,
	dd_salesdlvritemno_customer,
	dim_dateiddlvrdoccreated_customer,
	dim_dateidactualgoodsissue_customer,
	ct_qtydelivered,
	dd_SalesDocNo_customer,
	dd_SalesItemNo_customer
)
SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
	a.*,	
	ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
	ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
	ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
	ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
	ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
	ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
	ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
	ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
	ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
	ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
	ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
	ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
	ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
	ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
	ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
	ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
	ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
	ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer
from tmp002_003_004_005_006_007_BULK_BULK_BULK a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
														and bafpp.batchnumber = b.basdbatchnumber 
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2
AND m.dim_bulkpartid_3 = a.dim_bulkpartid_3
AND m.dim_bulkbatchidprod_3 = a.dim_bulkbatchidprod_3
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);  	 /*BULK-BULK-BULK-FPU-FPP*/

/*--------------------------------------------------------------------------------------------*/

/* Combination of 4 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

/* (RAW -- RAW-BULK -- BULK) -- BULK-FPU -- FPU  ---(FPU-FPP)  --FPP*/
insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBER_BULK_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE,
	DD_ORDERNUMBER_FPU_FPP,
	DIM_FPPPARTID,
	DIM_FPPBATCHIDPROD,
	DIM_DATEIDFPPACTUALRELEASE,
	DIM_DATEIDFPPACTUALSTART,
	DIM_DATEIDFPPACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPP,
	DD_DATEUSERSTATUSSAMR_FPP,
	DD_DATEUSERSTATUSQCCO_FPP,
	DIM_DATEIDFPPINSPECTIONSTART,
	DIM_DATEIDFPPUSAGEDECISIONMADE,
	dim_plantid_comops,
	dd_salesdlvrdocno_comops,
	dd_salesdlvritemno_comops,
	dim_dateiddlvrdoccreated_comops,
	dim_dateidactualgoodsissue_comops,
	dim_dateidactualreceipt_comops,
	dd_inspectionlotno_comops,
	dd_dateuserstatussamr_comops,
	dim_dateidinspectionstart_comops,
	dd_dateuserstatusqcco_comops,
	dim_Dateidusagedecisionmade_comops,
	dd_salesdlvrdocno_customer,
	dd_salesdlvritemno_customer,
	dim_dateiddlvrdoccreated_customer,
	dim_dateidactualgoodsissue_customer,
	ct_qtydelivered,
	dd_SalesDocNo_customer,
	dd_SalesItemNo_customer,
	amt_orderAmountSales,
	AMT_EXCHANGERATE,
	AMT_EXCHANGERATE_GBL,
	dim_fpppartid_comops,
	dim_currencyid,
	dim_customerShipComops,
	dim_customerSoldComops,
	dim_customerShipCust,
	dim_customerSoldCust,
	dd_SalesDocNo_comops,
	dd_SalesItemNo_comops,
	amt_orderamountsalesfpp,
	DIM_CURRENCYID_TRA,
	ct_orderAmountSalesFPP,
	ct_orderAmountSalesFPP2,
	ct_orderAmountSalesFPP_2,
	ct_TargetMasterRecipeFPP,
	ct_TargetMasterRecipeRAW,
	ct_TargetMasterRecipeFPU,
	ct_TargetMasterRecipeBULK,
	ct_TargetMasterRecipeFPP_ComOp,
	dim_rawdateuserstatussamr,
	dim_rawdateuserstatusqcco,
	dim_dateuserstatussamr_bulk,
	dim_dateuserstatusqcco_bulk,
	dim_dateuserstatussamr_fpu,
	dim_dateuserstatusqcco_fpu,
	dim_dateuserstatussamr_fpp,
	dim_dateuserstatusqcco_fpp
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
         a.*,
         ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
         ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
         ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
         ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
         ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
         ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
         ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
         ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
         ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
         ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
         ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
         ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
         ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
         ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
         ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
         ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
		 ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
		 ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer, 
		 0 as amt_orderAmountSales,
		 1 as AMT_EXCHANGERATE,
		 1 as AMT_EXCHANGERATE_GBL,
		 1 as dim_fpppartid_comops,
		 1 as dim_currencyid,
		 1 as dim_customerShipComops,
		 1 as dim_customerSoldComops,
		 1 as dim_customerShipCust,
		 1 as dim_customerSoldCust,
		 ifnull(dd_SalesDocNo_comops, 'Not Set') as dd_SalesDocNo_comops,
		 ifnull(dd_SalesItemNo_comops, 0) as dd_SalesItemNo_comops,
		 0 as amt_orderamountsalesfpp,
		 1 as DIM_CURRENCYID_TRA,
		 0 as ct_orderAmountSalesFPP,
		 0 as ct_orderAmountSalesFPP2,
		 0 as ct_orderAmountSalesFPP_2, 
		 0 as ct_TargetMasterRecipeFPP,
		 0 as ct_TargetMasterRecipeRAW,
		 0 as ct_TargetMasterRecipeFPU,
		 0 as ct_TargetMasterRecipeBULK,
		 0 as ct_TargetMasterRecipeFPP_ComOp,
		1 as dim_rawdateuserstatussamr, 
		1 as dim_rawdateuserstatusqcco, 
		1 as dim_dateuserstatussamr_bulk,
		1 as dim_dateuserstatusqcco_bulk,
		1 as dim_dateuserstatussamr_fpu, 
		1 as dim_dateuserstatusqcco_fpu, 
		1 as dim_dateuserstatussamr_fpp, 
		1 as dim_dateuserstatusqcco_fpp
from tmp001_002_003_004_005_006_007 a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
														and bafpp.batchnumber = b.basdbatchnumber 
														and pafpp.partnumber = b.pasdpartnumber
/*Additional condition needed, since there is more than one combination of 4 added for the rest of material steps + combination of 5,6... */
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);  /*RAW - BULK - FPU - FPP*/
														
/*---------------------------------------------------------------------------------------------------*/ 
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-FPU)  --FPU*/  
/* Adding one more bulk step */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULK_2ACTUALRELEASE,
DIM_DATEIDBULK_2ACTUALSTART,
DIM_DATEIDBULK_2ACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DD_ORDERNUMBER_BULK_2_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE
)
SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULK_2ACTUALRELEASE,
DIM_DATEIDBULK_2ACTUALSTART,
DIM_DATEIDBULK_2ACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DD_ORDERNUMBER_BULK_2_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE
from tmp001_002_003_004_005_BULK_BULK a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2);  /*RAW- BULK - BULK - FPU */
 
/* ( BULK --  BULK-BULK -- BULK) -- BULK-FPU -- FPU  ---(FPU-FPP)  --FPP*/  
 /* Adding one more bulk step */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNO_BULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBER_BULK_2_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE,
	DD_ORDERNUMBER_FPU_FPP,
	DIM_FPPPARTID,
	DIM_FPPBATCHIDPROD,
	DIM_DATEIDFPPACTUALRELEASE,
	DIM_DATEIDFPPACTUALSTART,
	DIM_DATEIDFPPACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPP,
	DD_DATEUSERSTATUSSAMR_FPP,
	DD_DATEUSERSTATUSQCCO_FPP,
	DIM_DATEIDFPPINSPECTIONSTART,
	DIM_DATEIDFPPUSAGEDECISIONMADE,
	dim_plantid_comops,
	dd_salesdlvrdocno_comops,
	dd_salesdlvritemno_comops,
	dim_dateiddlvrdoccreated_comops,
	dim_dateidactualgoodsissue_comops,
	dim_dateidactualreceipt_comops,
	dd_inspectionlotno_comops,
	dd_dateuserstatussamr_comops,
	dim_dateidinspectionstart_comops,
	dd_dateuserstatusqcco_comops,
	dim_Dateidusagedecisionmade_comops,
	dd_salesdlvrdocno_customer,
	dd_salesdlvritemno_customer,
	dim_dateiddlvrdoccreated_customer,
	dim_dateidactualgoodsissue_customer,
	ct_qtydelivered,
	dd_SalesDocNo_customer,
	dd_SalesItemNo_customer
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
         a.*,
         ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
         ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
         ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
         ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
         ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
         ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
         ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
         ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
         ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
         ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
         ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
         ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
         ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
         ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
         ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
         ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
		 ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
		 ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer 
from tmp002_003_004_005_006_007_BULK_BULK a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
														and bafpp.batchnumber = b.basdbatchnumber 
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);  /*BULK - BULK - FPU - FPP*/


/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-FPP)  --FPP*/  
 /* Adding one more bulk step */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULK_2ACTUALRELEASE,
	DIM_DATEIDBULK_2ACTUALSTART,
	DIM_DATEIDBULK_2ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBER_BULK_2_FPP,
	DIM_FPPPARTID,
	DIM_FPPBATCHIDPROD,
	DIM_DATEIDFPPACTUALRELEASE,
	DIM_DATEIDFPPACTUALSTART,
	DIM_DATEIDFPPACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPP,
	DD_DATEUSERSTATUSSAMR_FPP,
	DD_DATEUSERSTATUSQCCO_FPP,
	DIM_DATEIDFPPINSPECTIONSTART,
	DIM_DATEIDFPPUSAGEDECISIONMADE,
	dim_plantid_comops,
	dd_salesdlvrdocno_comops,
	dd_salesdlvritemno_comops,
	dim_dateiddlvrdoccreated_comops,
	dim_dateidactualgoodsissue_comops,
	dim_dateidactualreceipt_comops,
	dd_inspectionlotno_comops,
	dd_dateuserstatussamr_comops,
	dim_dateidinspectionstart_comops,
	dd_dateuserstatusqcco_comops,
	dim_Dateidusagedecisionmade_comops,
	dd_salesdlvrdocno_customer,
	dd_salesdlvritemno_customer,
	dim_dateiddlvrdoccreated_customer,
	dim_dateidactualgoodsissue_customer,
	ct_qtydelivered,
	dd_SalesDocNo_customer,
	dd_SalesItemNo_customer
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
         a.*,
		 ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
         ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
         ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
         ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
         ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
         ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
         ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
         ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
         ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
         ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
         ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
         ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
         ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
         ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
         ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
         ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
		 ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
		 ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer 
 from tmp001_002_003_007_010_BULK_BULK a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
														and bafpp.batchnumber = b.basdbatchnumber 
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);  /* RAW BULK BULK FPP */
 
 /*---------------------------------------------------------------------------------------------------*/ 
/* Adding the third bulk step - BI-5623*/
/* (RAW -- RAW-BULK -- BULK) -- BULK-BULK -- BULK  ---(BULK-BULK)  --BULK*/  	
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULK_2ACTUALRELEASE,
	DIM_DATEIDBULK_2ACTUALSTART,
	DIM_DATEIDBULK_2ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3
)
select (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNOT_RAW,
	DIM_RAWPARTID,
	DIM_RAWBATCHID,
	DD_PODOCUMENTNO,
	DD_PODOCUMENTITEMNO,
	DD_SCHEDULENO,
	DIM_DATEIDSCHEDORDER,
	DIM_DATEIDCREATE,
	DIM_DATEIDRAWPOSTINGDATE,
	DD_RAWDATEUSERSTATUSQCCO,
	DIM_DATEIDRAWINSPECTIONSTART,
	DD_RAWDATEUSERSTATUSSAMR,
	DIM_DATEIDRAWUSAGEDECISIONMADE,
	DD_ORDERNUMBERRAWBULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULK_2ACTUALRELEASE,
	DIM_DATEIDBULK_2ACTUALSTART,
	DIM_DATEIDBULK_2ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3
from tmp001_002_003_BULK_BULK_BULK a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_3 = a.dim_bulkpartid_3
AND m.dim_bulkbatchidprod_3 = a.dim_bulkbatchidprod_3
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2);   /* raw bulk bulk BULK */

/* (BULK -- BULK-BULK -- BULK) -- BULK-BULK --BULK --(BULK-FPU) - FPU */  
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNO_BULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
	DD_ORDERNUMBER_BULK_3_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE
)	
select (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,		
	DIM_PLANTID,
	DD_INSPECTIONLOTNO_BULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
	DD_ORDERNUMBER_BULK_3_FPU,
	DIM_FPUPARTID,
	DIM_FPUBATCHIDPROD,
	DIM_DATEIDFPUACTUALRELEASE,
	DIM_DATEIDFPUACTUALSTART,
	DIM_DATEIDFPUACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPU,
	DD_DATEUSERSTATUSSAMR_FPU,
	DD_DATEUSERSTATUSQCCO_FPU,
	DIM_DATEIDFPUINSPECTIONSTART,
	DIM_DATEIDFPUUSAGEDECISIONMADE
from tmp002_003_004_005_BULK_BULK_BULK a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.DIM_BULKPARTID_3 = a.DIM_BULKPARTID_3
AND m.DIM_BULKBATCHIDPROD_3 = a.DIM_BULKBATCHIDPROD_3
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2);  /*bulk bulk bulk FPU*/

/* (BULK -- BULK-BULK -- BULK) -- BULK-BULK --BULK --(BULK-FPP) - FPP */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate
(
	fact_mmprodhierarchyid,
	DIM_PLANTID,
	DD_INSPECTIONLOTNO_BULK,
	DIM_BULKPARTID,
	DIM_BULKBATCHIDPROD,
	DD_DATEUSERSTATUSQCCO_BULK,
	DIM_DATEIDINSPECTIONSTART_BULK,
	DD_DATEUSERSTATUSSAMR_BULK,
	DIM_DATEIDUSAGEDECISIONMADE_BULK,
	DD_ORDERNUMBERBULK_BULK,
	DIM_BULKPARTID_2,
	DIM_BULKBATCHIDPROD_2,
	DIM_DATEIDBULKACTUALRELEASE,
	DIM_DATEIDBULKACTUALSTART,
	DIM_DATEIDBULKACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_2,
	DD_DATEUSERSTATUSQCCO_BULK_2,
	DIM_DATEIDINSPECTIONSTART_BULK_2,
	DD_DATEUSERSTATUSSAMR_BULK_2,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
	DD_ORDERNUMBERBULK_2_BULK_3,
	DIM_BULKPARTID_3,
	DIM_BULKBATCHIDPROD_3,
	DIM_DATEIDBULK_3ACTUALRELEASE,
	DIM_DATEIDBULK_3ACTUALSTART,
	DIM_DATEIDBULK_3ACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_BULK_3,
	DD_DATEUSERSTATUSSAMR_BULK_3,
	DD_DATEUSERSTATUSQCCO_BULK_3,
	DIM_DATEIDINSPECTIONSTART_BULK_3,
	DIM_DATEIDUSAGEDECISIONMADE_BULK_3,
	DD_ORDERNUMBER_BULK_3_FPP,
	DIM_FPPPARTID,
	DIM_FPPBATCHIDPROD,
	DIM_DATEIDFPPACTUALRELEASE,
	DIM_DATEIDFPPACTUALSTART,
	DIM_DATEIDFPPACTUALHEADERFINISH,
	DD_INSPECTIONLOTNO_FPP,
	DD_DATEUSERSTATUSSAMR_FPP,
	DD_DATEUSERSTATUSQCCO_FPP,
	DIM_DATEIDFPPINSPECTIONSTART,
	DIM_DATEIDFPPUSAGEDECISIONMADE,
	dim_plantid_comops,
	dd_salesdlvrdocno_comops,
	dd_salesdlvritemno_comops,
	dim_dateiddlvrdoccreated_comops,
	dim_dateidactualgoodsissue_comops,
	dim_dateidactualreceipt_comops,
	dd_inspectionlotno_comops,
	dd_dateuserstatussamr_comops,
	dim_dateidinspectionstart_comops,
	dd_dateuserstatusqcco_comops,
	dim_Dateidusagedecisionmade_comops,
	dd_salesdlvrdocno_customer,
	dd_salesdlvritemno_customer,
	dim_dateiddlvrdoccreated_customer,
	dim_dateidactualgoodsissue_customer,
	ct_qtydelivered,
	dd_SalesDocNo_customer,
	dd_SalesItemNo_customer
)
SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
         a.*,
		 ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
         ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
         ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
         ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
         ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
         ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
         ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
         ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
         ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
         ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
         ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
         ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
         ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
         ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
         ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
         ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
		 ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
		 ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer 
from tmp002_003_007_010_BULK_BULK_BULK a
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
														and bafpp.batchnumber = b.basdbatchnumber 
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2
AND m.dim_bulkpartid_3 = a.dim_bulkpartid_3
AND m.dim_bulkbatchidprod_3 = a.dim_bulkbatchidprod_3
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);   /*bulk bulk bulk FPP*/

/*---------------------------------------------------------------------------------------------------*/ 

/* Combination of 3 */
/* RAW - BULK - FPU */
/* Rows affected: 113471 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBER_BULK_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBER_BULK_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE
from tmp001_002_003_004_005 a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.dim_fpubatchidprod);

/*--------------------------------------------------------------------------------------------*/
/* RAW - BULK - BULK */
/* Adding one more bulk step */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULK_2ACTUALRELEASE,
DIM_DATEIDBULK_2ACTUALSTART,
DIM_DATEIDBULK_2ACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
dim_dateidinspectionstart_bulk_2,
dim_dateidusagedecisionmade_bulk_2
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULK_2ACTUALRELEASE,
DIM_DATEIDBULK_2ACTUALSTART,
DIM_DATEIDBULK_2ACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
dim_dateidinspectionstart_bulk_2,
dim_dateidusagedecisionmade_bulk_2
from tmp001_002_003_BULK_BULK a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2);


/* BULK - BULK - FPU */
/* Adding one more bulk step */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNO_BULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DD_ORDERNUMBER_BULK_2_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNO_BULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DD_ORDERNUMBER_BULK_2_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE
from tmp002_003_004_005_BULK_BULK a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_fpupartid = a.dim_fpupartid
AND m.DIM_FPUBATCHIDPROD = a.DIM_FPUBATCHIDPROD
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2);

/* BULK - BULK - FPP */
/* Adding one more bulk step */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNO_BULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DD_ORDERNUMBER_BULK_2_FPP,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNO_BULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DD_ORDERNUMBER_BULK_2_FPP,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE
from tmp002_003_007_010_BULK_BULK a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_fpppartid = a.dim_fpppartid
AND m.DIM_FPPBATCHIDPROD = a.DIM_FPPBATCHIDPROD
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2);

/*--------------------------------------------------------------------------------------------*/

/* Adding the third bulk step - BI-5623*/
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNO_BULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DD_ORDERNUMBERBULK_2_BULK_3,
DIM_BULKPARTID_3,
DIM_BULKBATCHIDPROD_3,
DIM_DATEIDBULK_3ACTUALRELEASE,
DIM_DATEIDBULK_3ACTUALSTART,
DIM_DATEIDBULK_3ACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_3,
DD_DATEUSERSTATUSSAMR_BULK_3,
DD_DATEUSERSTATUSQCCO_BULK_3,
DIM_DATEIDINSPECTIONSTART_BULK_3,
DIM_DATEIDUSAGEDECISIONMADE_BULK_3
)
 SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNO_BULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2,
DD_ORDERNUMBERBULK_2_BULK_3,
DIM_BULKPARTID_3,
DIM_BULKBATCHIDPROD_3,
DIM_DATEIDBULK_3ACTUALRELEASE,
DIM_DATEIDBULK_3ACTUALSTART,
DIM_DATEIDBULK_3ACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_3,
DD_DATEUSERSTATUSSAMR_BULK_3,
DD_DATEUSERSTATUSQCCO_BULK_3,
DIM_DATEIDINSPECTIONSTART_BULK_3,
DIM_DATEIDUSAGEDECISIONMADE_BULK_3
from tmp002_003_BULK_BULK_BULK a
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.DIM_BULKPARTID_3 = a.DIM_BULKPARTID_3
AND m.DIM_BULKBATCHIDPROD_3 = a.DIM_BULKBATCHIDPROD_3
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_bulkpartid_2 = a.dim_bulkpartid_2
AND m.dim_bulkbatchidprod_2 = a.dim_bulkbatchidprod_2); /* BULK - BULK - BULK */	
/*-----------------------------------------------------------*/


/* RAW - BULK - FPP */
/* Rows affected: 0 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
DD_SALESDOCNO_COMOPS,
DD_SALESITEMNO_COMOPS
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
A.DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
ifnull(DD_SALESDOCNO_COMOPS, 'Not Set'),
ifnull(DD_SALESITEMNO_COMOPS, 0)
from tmp001_002_003_004_007 a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid
														and bafpp.batchnumber = b.basdbatchnumber
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.DIM_BULKBATCHIDPROD
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);



/* RAW - FPU - FPP */
/* Rows affected: 352929 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE,
DD_ORDERNUMBER_FPU_FPP,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
DD_SALESDOCNO_COMOPS,
DD_SALESITEMNO_COMOPS
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
a.DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE,
DD_ORDERNUMBER_FPU_FPP,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
ifnull(DD_SALESDOCNO_COMOPS, 'Not Set'),
ifnull(DD_SALESITEMNO_COMOPS, 0)
from      tmp008_006_007 a 
		inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
		inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
		inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid 
														and bafpp.batchnumber = b.basdbatchnumber
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.DIM_fpuBATCHIDPROD
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);


/* BULK - FPU - FPP */
/* Rows affected: 2035944 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBER_BULK_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE,
DD_ORDERNUMBER_FPU_FPP,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
DD_SALESDOCNO_COMOPS,
DD_SALESITEMNO_COMOPS
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_BULKPARTID,
dim_bulkbatchid,
1 AS DIM_DATEIDBULKACTUALRELEASE,
1 AS DIM_DATEIDBULKACTUALSTART,
1 AS DIM_DATEIDBULKACTUALHEADERFINISH,
'Not Set' AS DD_INSPECTIONLOTNO_BULK,
'0001-01-01' AS DD_DATEUSERSTATUSQCCO_BULK,
1 AS DIM_DATEIDINSPECTIONSTART_BULK,
'0001-01-01' AS DD_DATEUSERSTATUSSAMR_BULK,
1 DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBER_BULK_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE,
DD_ORDERNUMBER_FPU_FPP,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
ifnull(DD_SALESDOCNO_COMOPS, 'Not Set'),
ifnull(DD_SALESITEMNO_COMOPS, 0)
from      tmp003_006_007 a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
	inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid
														and bafpp.batchnumber = b.basdbatchnumber
														and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.dim_bulkbatchidprod = a.dim_bulkbatchid
AND m.dim_fpupartid = a.dim_fpupartid
AND m.dim_fpubatchidprod = a.DIM_fpuBATCHIDPROD
AND m.dim_fpppartid = a.dim_fpppartid
AND m.dim_fppbatchidprod = a.dim_fppbatchidprod);


/* Combination of 2 */

/* RAW - BULK */
/* Rows affected: 39861 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DD_ORDERNUMBERRAWBULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK
from      tmp001_002_003 a

 where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.DIM_BULKBATCHIDPROD = a.DIM_BULKBATCHIDPROD);


/* ---------------------------------------------------- */
/* Adding one more bulk step */
/* BULK - BULK */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNO_BULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNO_BULK,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBERBULK_BULK,
DIM_BULKPARTID_2,
DIM_BULKBATCHIDPROD_2,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK_2,
DD_DATEUSERSTATUSQCCO_BULK_2,
DIM_DATEIDINSPECTIONSTART_BULK_2,
DD_DATEUSERSTATUSSAMR_BULK_2,
DIM_DATEIDUSAGEDECISIONMADE_BULK_2
from      tmp002_003_BULK_BULK a

 where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.DIM_BULKPARTID_2 = a.DIM_BULKPARTID_2
AND m.DIM_BULKBATCHIDPROD_2 = a.DIM_BULKBATCHIDPROD_2
AND m.dim_bulkpartid = a.dim_bulkpartid
AND m.DIM_BULKBATCHIDPROD = a.DIM_BULKBATCHIDPROD);
/*------------------------------------------------------*/


/* RAW - FPU */
/* Rows affected: 2728 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE
from      tmp001_008_005 a

 where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.DIM_FPUPARTID = a.DIM_FPUPARTID
AND m.DIM_FPUBATCHIDPROD = a.DIM_FPUBATCHIDPROD);



/* RAW - FPP */
/* Rows affected: 5860 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
DD_SALESDOCNO_COMOPS,
DD_SALESITEMNO_COMOPS

)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
A.DIM_PLANTID,
DD_INSPECTIONLOTNOT_RAW,
DIM_RAWPARTID,
DIM_RAWBATCHID,
DD_PODOCUMENTNO,
DD_PODOCUMENTITEMNO,
DD_SCHEDULENO,
DIM_DATEIDSCHEDORDER,
DIM_DATEIDCREATE,
DIM_DATEIDRAWPOSTINGDATE,
DD_RAWDATEUSERSTATUSQCCO,
DIM_DATEIDRAWINSPECTIONSTART,
DD_RAWDATEUSERSTATUSSAMR,
DIM_DATEIDRAWUSAGEDECISIONMADE,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
ifnull(DD_SALESDOCNO_COMOPS, 'Not Set'),
ifnull(DD_SALESITEMNO_COMOPS, 0)

from tmp001_009_007 a 
		inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
		inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
        inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid 
														 and bafpp.batchnumber = b.basdbatchnumber
														 and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.dim_rawpartid = a.dim_rawpartid
AND m.dim_rawbatchid = a.dim_rawbatchid
AND m.DIM_FPPPARTID = a.DIM_FPPPARTID
AND m.DIM_FPPBATCHIDPROD = a.DIM_FPPBATCHIDPROD);




/* BULK - FPP */
/* Rows affected: 712550 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
DD_SALESDOCNO_COMOPS,
DD_SALESITEMNO_COMOPS

)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_BULKPARTID,
DIM_BULKBATCHID,
1 AS DIM_DATEIDBULKACTUALRELEASE,
1 AS DIM_DATEIDBULKACTUALSTART,
1 AS DIM_DATEIDBULKACTUALHEADERFINISH,
'Not Set' AS DD_INSPECTIONLOTNO_BULK,
'0001-01-01' AS DD_DATEUSERSTATUSQCCO_BULK,
1 AS DIM_DATEIDINSPECTIONSTART_BULK,
'0001-01-01' AS DD_DATEUSERSTATUSSAMR_BULK,
1 AS DIM_DATEIDUSAGEDECISIONMADE_BULK,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
ifnull(DD_SALESDOCNO_COMOPS, 'Not Set'),
ifnull(DD_SALESITEMNO_COMOPS, 0)
from      tmp003_007_0010 a
			inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
			inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
            inner join tmp004_insplot_salesorderdelivery002 b on  a.dim_plantid = b.dim_plantid
															 and bafpp.batchnumber = b.basdbatchnumber
															 and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.DIM_BULKPARTID = a.DIM_BULKPARTID
AND m.DIM_BULKBATCHIDPROD = a.DIM_BULKBATCHID
AND m.DIM_FPPPARTID = a.DIM_FPPPARTID
AND m.DIM_FPPBATCHIDPROD = a.DIM_FPPBATCHIDPROD);



/* BULK - FPU */
/* Rows affected: 11602 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_BULKPARTID,
DIM_BULKBATCHIDPROD,
DIM_DATEIDBULKACTUALRELEASE,
DIM_DATEIDBULKACTUALSTART,
DIM_DATEIDBULKACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_BULK,
DD_DATEUSERSTATUSQCCO_BULK,
DIM_DATEIDINSPECTIONSTART_BULK,
DD_DATEUSERSTATUSSAMR_BULK,
DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBER_BULK_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE

)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_BULKPARTID,
DIM_BULKBATCHID,
1 AS DIM_DATEIDBULKACTUALRELEASE,
1 AS DIM_DATEIDBULKACTUALSTART,
1 AS DIM_DATEIDBULKACTUALHEADERFINISH,
'Not Set' AS DD_INSPECTIONLOTNO_BULK,
'0001-01-01' AS DD_DATEUSERSTATUSQCCO_BULK,
1 AS DIM_DATEIDINSPECTIONSTART_BULK,
'0001-01-01' AS DD_DATEUSERSTATUSSAMR_BULK,
1 AS DIM_DATEIDUSAGEDECISIONMADE_BULK,
DD_ORDERNUMBER_BULK_FPU,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPUACTUALRELEASE,
DIM_DATEIDFPUACTUALSTART,
DIM_DATEIDFPUACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPU,
DD_DATEUSERSTATUSSAMR_FPU,
DD_DATEUSERSTATUSQCCO_FPU,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE

from      tmp003_004_005 a
 where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.DIM_BULKPARTID = a.DIM_BULKPARTID
AND m.DIM_BULKBATCHIDPROD = a.DIM_BULKBATCHID
AND m.DIM_FPUPARTID = a.DIM_FPUPARTID
AND m.DIM_FPUBATCHIDPROD = a.DIM_FPUBATCHIDPROD);




/* FPU - FPP */
/* Rows affected: 2264298 */
delete from number_fountain m where m.table_name = 'fact_mmprodhierarchy_tmp_populate';
insert into number_fountain
select 'fact_mmprodhierarchy_tmp_populate',
ifnull(max(f.fact_mmprodhierarchyid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_mmprodhierarchy_tmp_populate f;

insert into fact_mmprodhierarchy_tmp_populate(
FACT_MMPRODHIERARCHYID,
DIM_FPUPARTID,
DIM_FPUBATCHIDPROD,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE,
DD_ORDERNUMBER_FPU_FPP,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
DD_SALESDOCNO_COMOPS,
DD_SALESITEMNO_COMOPS
)
  SELECT (select max_id   from number_fountain   where table_name = 'fact_mmprodhierarchy_tmp_populate') + row_number() over(ORDER BY '') AS fact_mmprodhierarchyid,
DIM_FPUPARTID,
DIM_FPUBATCHID,
DIM_DATEIDFPPACTUALRELEASE,
DIM_DATEIDFPPACTUALSTART,
DIM_DATEIDFPPACTUALHEADERFINISH,
DD_INSPECTIONLOTNO_FPP,
DD_DATEUSERSTATUSSAMR_FPP,
DD_DATEUSERSTATUSQCCO_FPP,
DIM_DATEIDFPUINSPECTIONSTART,
DIM_DATEIDFPUUSAGEDECISIONMADE,
DD_ORDERNUMBER_FPU_FPP,
DIM_FPPPARTID,
DIM_FPPBATCHIDPROD,
DIM_DATEIDFPPINSPECTIONSTART,
DIM_DATEIDFPPUSAGEDECISIONMADE,
DIM_PLANTID_COMOPS,
DD_SALESDLVRDOCNO_COMOPS,
DD_SALESDLVRITEMNO_COMOPS,
DIM_DATEIDDLVRDOCCREATED_COMOPS,
DIM_DATEIDACTUALGOODSISSUE_COMOPS,
DIM_DATEIDACTUALRECEIPT_COMOPS,
DD_INSPECTIONLOTNO_COMOPS,
DD_DATEUSERSTATUSSAMR_COMOPS,
DIM_DATEIDINSPECTIONSTART_COMOPS,
DD_DATEUSERSTATUSQCCO_COMOPS,
DIM_DATEIDUSAGEDECISIONMADE_COMOPS,
DD_SALESDLVRDOCNO_CUSTOMER,
DD_SALESDLVRITEMNO_CUSTOMER,
DIM_DATEIDDLVRDOCCREATED_CUSTOMER,
DIM_DATEIDACTUALGOODSISSUE_CUSTOMER,
CT_QTYDELIVERED,
DD_SALESDOCNO_CUSTOMER,
DD_SALESITEMNO_CUSTOMER,
ifnull(DD_SALESDOCNO_COMOPS, 'Not Set'),
ifnull(DD_SALESITEMNO_COMOPS, 0)
from      tmp005_006_007 a 
	inner join dim_batch bafpp on a.dim_fppbatchidprod = bafpp.dim_batchid
	inner join dim_part pafpp on a.dim_fpppartid = pafpp.dim_partid
    inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid 
													 and bafpp.batchnumber = b.basdbatchnumber
													 and pafpp.partnumber = b.pasdpartnumber
where not exists
(select 1 from fact_mmprodhierarchy_tmp_populate m
where m.dim_plantid = a.dim_plantid
AND m.DIM_FPUPARTID = a.DIM_FPUPARTID
AND m.DIM_FPUBATCHIDPROD = a.DIM_FPUBATCHID
AND m.DIM_FPPPARTID = a.DIM_FPPPARTID
AND m.DIM_FPPBATCHIDPROD = a.DIM_FPPBATCHIDPROD);

/* Only include inspections starting with 1, 3 or 4 - BI-4358 */
/* delete from fact_mmprodhierarchy_tmp_populate where DD_INSPECTIONLOTNOT_RAW like '8%' OR DD_INSPECTIONLOTNO_BULK like '8%' OR DD_INSPECTIONLOTNO_FPU like '8%'
OR DD_INSPECTIONLOTNO_COMOPS like '8%'OR DD_INSPECTIONLOTNO_FPP like '8%' */

/* delete from fact_mmprodhierarchy_tmp_populate where
not (DD_INSPECTIONLOTNOT_RAW like '1%' OR DD_INSPECTIONLOTNO_BULK like '1%' OR DD_INSPECTIONLOTNO_FPU like '1%' OR DD_INSPECTIONLOTNO_COMOPS like '1%' OR DD_INSPECTIONLOTNO_FPP like '1%') 
and not (DD_INSPECTIONLOTNOT_RAW like '3%' OR DD_INSPECTIONLOTNO_BULK like '3%' OR DD_INSPECTIONLOTNO_FPU like '3%' OR DD_INSPECTIONLOTNO_COMOPS like '3%' OR DD_INSPECTIONLOTNO_FPP like '3%')
and not (DD_INSPECTIONLOTNOT_RAW like '4%' OR DD_INSPECTIONLOTNO_BULK like '4%' OR DD_INSPECTIONLOTNO_FPU like '4%' OR DD_INSPECTIONLOTNO_COMOPS like '4%' OR DD_INSPECTIONLOTNO_FPP like '4%') */

merge into fact_mmprodhierarchy_tmp_populate mmp using
(select mmp.fact_mmprodhierarchyid 
from fact_mmprodhierarchy_tmp_populate mmp,
	 fact_inspectionlot iraw, dim_inspectiontype draw
where mmp.DD_INSPECTIONLOTNOT_RAW = iraw.dd_inspectionlotno and iraw.dim_inspectiontypeid = draw.dim_inspectiontypeid
	and draw.InspectionTypeCode not like '01%' and draw.InspectionTypeCode not like '04%' and draw.InspectionTypeCode not like '03%') del
on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
when matched then delete;

merge into fact_mmprodhierarchy_tmp_populate mmp using
(select mmp.fact_mmprodhierarchyid
from fact_mmprodhierarchy_tmp_populate mmp,
	 fact_inspectionlot ibulk, dim_inspectiontype dbulk
where mmp.DD_INSPECTIONLOTNO_BULK = ibulk.dd_inspectionlotno and ibulk.dim_inspectiontypeid = dbulk.dim_inspectiontypeid
	and dbulk.InspectionTypeCode not like '01%' and dbulk.InspectionTypeCode not like '04%' and dbulk.InspectionTypeCode not like '03%') del
on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
when matched then delete;

merge into fact_mmprodhierarchy_tmp_populate mmp using
(select mmp.fact_mmprodhierarchyid
from fact_mmprodhierarchy_tmp_populate mmp,
	 fact_inspectionlot ifpu, dim_inspectiontype dfpu
where mmp.DD_INSPECTIONLOTNO_FPU = ifpu.dd_inspectionlotno and ifpu.dim_inspectiontypeid = dfpu.dim_inspectiontypeid
	and dfpu.InspectionTypeCode not like '01%' and dfpu.InspectionTypeCode not like '04%' and dfpu.InspectionTypeCode not like '03%') del
on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
when matched then delete;

merge into fact_mmprodhierarchy_tmp_populate mmp using
(select mmp.fact_mmprodhierarchyid
from fact_mmprodhierarchy_tmp_populate mmp,
	 fact_inspectionlot ifpp, dim_inspectiontype dfpp
where mmp.DD_INSPECTIONLOTNO_FPP = ifpp.dd_inspectionlotno and ifpp.dim_inspectiontypeid = dfpp.dim_inspectiontypeid
	and dfpp.InspectionTypeCode not like '01%' and dfpp.InspectionTypeCode not like '04%' and dfpp.InspectionTypeCode not like '03%') del
on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
when matched then delete;

merge into fact_mmprodhierarchy_tmp_populate mmp using
(select mmp.fact_mmprodhierarchyid
from fact_mmprodhierarchy_tmp_populate mmp,
	 fact_inspectionlot icomops, dim_inspectiontype dcomops
where mmp.DD_INSPECTIONLOTNO_COMOPS = icomops.dd_inspectionlotno and icomops.dim_inspectiontypeid = dcomops.dim_inspectiontypeid
	and dcomops.InspectionTypeCode not like '01%' and dcomops.InspectionTypeCode not like '04%' and dcomops.InspectionTypeCode not like '03%') del
on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
when matched then delete;
/* END BI-4358 */

/* BI-4287 - delete returns */
merge into fact_mmprodhierarchy_tmp_populate mmp
using ( select distinct mmp.fact_mmprodhierarchyid from fact_mmprodhierarchy_tmp_populate mmp, fact_salesorder so
	where dd_ReturnsItem = 'X'
	and mmp.dd_salesdocno_customer = so.dd_salesdocno
	and mmp.dd_salesitemno_customer = so.dd_salesitemno ) del
on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
when matched then delete;

update fact_mmprodhierarchy_tmp_populate mmp
set DIM_CURRENCYID_TRA = c.dim_currencyid
from fact_mmprodhierarchy_tmp_populate mmp, dim_currency c
where currencycode = 'TRA'
and DIM_CURRENCYID_TRA <> c.dim_currencyid;

/* Adding Sales Order Amount */
merge into  fact_mmprodhierarchy_tmp_populate mmp
using ( select distinct fact_mmprodhierarchyid, 
		first_value((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) ELSE f_so.amt_ScheduleTotal END)) over (partition by fact_mmprodhierarchyid order by '') as amt_orderAmountSales
		from fact_salesorder f_so, fact_mmprodhierarchy_tmp_populate mmp, dim_salesmisc dsi
		where mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
			and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo
			and f_so.dim_salesmiscid = dsi.dim_salesmiscid ) upd
on mmp.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update
set mmp.amt_orderAmountSales = upd.amt_orderAmountSales;

/* Add amt_ExchangeRate, based on the related on from Sales */
merge into fact_mmprodhierarchy_tmp_populate mmp
using ( select distinct dd_SalesDocNo, dd_SalesItemNo, f_so.amt_ExchangeRate
	from  fact_salesorder f_so ) f_so
on  mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
	and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo 
when matched then update
set  mmp.amt_ExchangeRate = f_so.amt_ExchangeRate;

/* Add amt_ExchangeRate_GBL, based on the related on from Sales */
merge into fact_mmprodhierarchy_tmp_populate mmp
using ( select distinct dd_SalesDocNo, dd_SalesItemNo, /* f_so.amt_ExchangeRate_GBL - Correct unstable set of rows*/
	first_value(f_so.amt_ExchangeRate_GBL) over (partition by dd_SalesDocNo, dd_SalesItemNo order by '') as amt_ExchangeRate_GBL
	from  fact_salesorder f_so ) f_so
on  mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
	and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo 
when matched then update
set  mmp.amt_ExchangeRate_GBL = f_so.amt_ExchangeRate_GBL;

/* Set Order Sales Amount as Quantity, always expressed in global conversion */
update fact_mmprodhierarchy_tmp_populate
set amt_orderAmountSales = amt_orderAmountSales * amt_ExchangeRate_GBL;


/* BI-4227 Adding Material FPP COMOPS field */
update fact_mmprodhierarchy_tmp_populate mmp
set mmp.dim_fpppartid_comops = fpp_comops.dim_partid
from fact_mmprodhierarchy_tmp_populate mmp, dim_part fpp, dim_part fpp_comops, dim_plant p_comops
where mmp.dim_fpppartid = fpp.dim_partid 
	and mmp.dim_plantid_comops = p_comops.dim_plantid
	and fpp_comops.partnumber = fpp.partnumber
	and fpp_comops.plant = p_comops.plantcode
	and mmp.dim_fpppartid_comops <> fpp_comops.dim_partid;
	
/* Add Customer fields */
merge into fact_mmprodhierarchy_tmp_populate mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridShipTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_comops = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_comops = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_comops = upd.dd_salesdocno
	and mmp.dd_salesitemno_comops = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerShipComops = upd.Dim_CustomeridShipTo;

merge into fact_mmprodhierarchy_tmp_populate mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridSoldTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_comops = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_comops = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_comops = upd.dd_salesdocno
	and mmp.dd_salesitemno_comops = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerSoldComops = upd.Dim_CustomeridSoldTo;

merge into fact_mmprodhierarchy_tmp_populate mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridShipTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_customer = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_customer = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_customer = upd.dd_salesdocno
	and mmp.dd_salesitemno_customer = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerShipCust = upd.Dim_CustomeridShipTo;

merge into fact_mmprodhierarchy_tmp_populate mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridSoldTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_customer = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_customer = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_customer = upd.dd_salesdocno
	and mmp.dd_salesitemno_customer = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerSoldCust = upd.Dim_CustomeridSoldTo;

/* Add Sales Order Amount based on GPF Code - Material FPP Site */
merge into fact_mmprodhierarchy_tmp_populate mmp
using (
select fact_mmprodhierarchyid, y.sum_fpp from 
(select ProductFamily_Merck, sum(amt_orderAmountSales) sum_fpp from 
	(select mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, ProductFamily_Merck, amt_orderAmountSales,
		row_number() over ( partition by mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, ProductFamily_Merck order by '') rn
		from fact_mmprodhierarchy_tmp_populate mmp, dim_part p, dim_date dd
		where mmp.dim_fpppartid = p.dim_partid
		and mmp.dim_dateidactualgoodsissue_customer = dd.dim_dateid
		and dd.datevalue >= add_years(current_date, -2)) x
	where x.rn = 1
	group by  ProductFamily_Merck) y, fact_mmprodhierarchy_tmp_populate mmp, dim_part dp
where mmp.dim_fpppartid = dp.dim_partid 
	and y.ProductFamily_Merck = dp.ProductFamily_Merck) upd
on mmp.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update
set amt_orderAmountSalesFPP = upd.sum_fpp;

/* Add Sales Order Amount based on GPF Code - Material FPP Site - as quantity, with default Global currency */
merge into fact_mmprodhierarchy_tmp_populate mmp
using (
select fact_mmprodhierarchyid, y.sum_fpp from 
(select ProductFamily_Merck, sum(amt_orderAmountSales) sum_fpp from 
	/* (select mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, ProductFamily_Merck, amt_orderAmountSales * amt_ExchangeRate_GBL as amt_orderAmountSales, */
	(select mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, ProductFamily_Merck, amt_orderAmountSales as amt_orderAmountSales,
		row_number() over ( partition by mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, ProductFamily_Merck order by '') rn
		from fact_mmprodhierarchy_tmp_populate mmp, dim_part p, dim_date dd
		where mmp.dim_fpppartid = p.dim_partid
		and mmp.dim_dateidactualgoodsissue_customer = dd.dim_dateid
		and dd.datevalue >= add_years(current_date, -2)
		) x
	where x.rn = 1
	group by  ProductFamily_Merck) y, fact_mmprodhierarchy_tmp_populate mmp, dim_part dp
where mmp.dim_fpppartid = dp.dim_partid 
	and y.ProductFamily_Merck = dp.ProductFamily_Merck) upd
on mmp.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update
set mmp.ct_orderAmountSalesFPP = upd.sum_fpp;

/* create temp table based on Planned Order */
drop table if exists tmp_PlanOrder_TargetPerPart;
create table tmp_PlanOrder_TargetPerPart as
select partnumber, 
	round(case when avg(df.businessdaysseqno - ds.businessdaysseqno) < '0.5' then 1
		else avg(df.businessdaysseqno - ds.businessdaysseqno) end) as targerMasterRecipe
from fact_planorder fp, dim_date ds, dim_date df, dim_part p
where fp.dd_PlannScenario = '0'
	and fp.dim_dateidstart = ds.dim_dateid
	and ds.datevalue > current_date
	and fp.dim_dateidfinish = df.dim_dateid
	and fp.dim_partid = p.dim_partid
group by partnumber;

/* Update the Target Master Recipe Days BI-4755*/
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_TargetMasterRecipeRAW = ifnull(tmp.targerMasterRecipe, 0)
from fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, tmp_PlanOrder_TargetPerPart tmp
where fmm.dim_rawpartid = dp.dim_partid
	and tmp.partnumber = dp.partnumber
	and fmm.ct_TargetMasterRecipeRAW <> ifnull(tmp.targerMasterRecipe, 0);

update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_TargetMasterRecipeFPP = ifnull(tmp.targerMasterRecipe, 0)
from fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, tmp_PlanOrder_TargetPerPart tmp
where fmm.dim_fpppartid = dp.dim_partid
	and tmp.partnumber = dp.partnumber
	and fmm.ct_TargetMasterRecipeFPP <> ifnull(tmp.targerMasterRecipe, 0);

update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_TargetMasterRecipeFPU = ifnull(tmp.targerMasterRecipe, 0)
from fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, tmp_PlanOrder_TargetPerPart tmp
where fmm.dim_fpupartid = dp.dim_partid
	and tmp.partnumber = dp.partnumber
	and fmm.ct_TargetMasterRecipeFPU <> ifnull(tmp.targerMasterRecipe, 0);

update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_TargetMasterRecipeBULK = ifnull(tmp.targerMasterRecipe, 0)
from fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, tmp_PlanOrder_TargetPerPart tmp
where fmm.dim_bulkpartid = dp.dim_partid
	and tmp.partnumber = dp.partnumber
	and fmm.ct_TargetMasterRecipeBULK <> ifnull(tmp.targerMasterRecipe, 0);

update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_TargetMasterRecipeFPP_ComOp = ifnull(tmp.targerMasterRecipe, 0)
from fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, tmp_PlanOrder_TargetPerPart tmp
where fmm.dim_fpppartid_comops = dp.dim_partid
	and tmp.partnumber = dp.partnumber
	and fmm.ct_TargetMasterRecipeFPP_ComOp <> ifnull(tmp.targerMasterRecipe, 0);
	
/* Use dim instead of dd for Date User Status SAMR was last set RAW - BI-4772  */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.dim_rawdateuserstatussamr = dd.dim_dateid
from fact_mmprodhierarchy_tmp_populate fmm, dim_date dd, dim_plant pl
where pl.dim_plantid = fmm.dim_plantid
and pl.companycode = dd.companycode
and fmm.dd_rawdateuserstatussamr = dd.datevalue
and pl.plantcode = dd.plantcode_factory  /* Additional condition for dim_date factory plant BI-5085 */
and fmm.dim_rawdateuserstatussamr <> dd.dim_dateid;
 
/* BI-4824 - dd_rawdateuserstatusqcco - Date User Status QCCO was last set RAW  */
	update fact_mmprodhierarchy_tmp_populate fmm
	set fmm.dim_rawdateuserstatusqcco = dd.dim_dateid
	from fact_mmprodhierarchy_tmp_populate fmm, dim_date dd, dim_plant pl
	where pl.dim_plantid = fmm.dim_plantid
	and pl.companycode = dd.companycode
	and fmm.dd_rawdateuserstatusqcco = dd.datevalue
	and pl.plantcode = dd.plantcode_factory  /* Additional condition for dim_date factory plant BI-5085 */
	and fmm.dim_rawdateuserstatusqcco <> dd.dim_dateid;

/* BI-4824 - dd_dateuserstatussamr_bulk - Date User Status SAMR was last set BULK */
	update fact_mmprodhierarchy_tmp_populate fmm
	set fmm.dim_dateuserstatussamr_bulk = dd.dim_dateid
	from fact_mmprodhierarchy_tmp_populate fmm, dim_date dd, dim_plant pl
	where pl.dim_plantid = fmm.dim_plantid
	and pl.companycode = dd.companycode
	and fmm.dd_dateuserstatussamr_bulk = dd.datevalue
	and pl.plantcode = dd.plantcode_factory  /* Additional condition for dim_date factory plant BI-5085 */
	and fmm.dim_dateuserstatussamr_bulk <> dd.dim_dateid;
 
/* BI-4824 - dd_dateuserstatusqcco_bulk - Date User Status QCCO was last set BULK */
	update fact_mmprodhierarchy_tmp_populate fmm
	set fmm.dim_dateuserstatusqcco_bulk = dd.dim_dateid
	from fact_mmprodhierarchy_tmp_populate fmm, dim_date dd, dim_plant pl
	where pl.dim_plantid = fmm.dim_plantid
	and pl.companycode = dd.companycode
	and fmm.dd_dateuserstatusqcco_bulk = dd.datevalue
	and pl.plantcode = dd.plantcode_factory  /* Additional condition for dim_date factory plant BI-5085 */
	and fmm.dim_dateuserstatusqcco_bulk <> dd.dim_dateid;

/* BI-4824 - dd_dateuserstatussamr_fpu - Date User Status SAMR was last set FPU */
	update fact_mmprodhierarchy_tmp_populate fmm
	set fmm.dim_dateuserstatussamr_fpu = dd.dim_dateid
	from fact_mmprodhierarchy_tmp_populate fmm, dim_date dd, dim_plant pl
	where pl.dim_plantid = fmm.dim_plantid
	and pl.companycode = dd.companycode
	and fmm.dd_dateuserstatussamr_fpu = dd.datevalue
	and pl.plantcode = dd.plantcode_factory  /* Additional condition for dim_date factory plant BI-5085 */
	and fmm.dim_dateuserstatussamr_fpu <> dd.dim_dateid;

/* BI-4824 - dd_dateuserstatusqcco_fpu - Date User Status QCCO was last set FPU */
	update fact_mmprodhierarchy_tmp_populate fmm
	set fmm.dim_dateuserstatusqcco_fpu = dd.dim_dateid
	from fact_mmprodhierarchy_tmp_populate fmm, dim_date dd, dim_plant pl
	where pl.dim_plantid = fmm.dim_plantid
	and pl.companycode = dd.companycode
	and fmm.dd_dateuserstatusqcco_fpu = dd.datevalue
	and pl.plantcode = dd.plantcode_factory  /* Additional condition for dim_date factory plant BI-5085 */
	and fmm.dim_dateuserstatusqcco_fpu <> dd.dim_dateid;

/* BI-4824 - dd_dateuserstatussamr_fpp - Date User Status SAMR was last set FPP */
	update fact_mmprodhierarchy_tmp_populate fmm
	set fmm.dim_dateuserstatussamr_fpp = dd.dim_dateid
	from fact_mmprodhierarchy_tmp_populate fmm, dim_date dd, dim_plant pl
	where pl.dim_plantid = fmm.dim_plantid
	and pl.companycode = dd.companycode
	and fmm.dd_dateuserstatussamr_fpp = dd.datevalue
	and pl.plantcode = dd.plantcode_factory  /* Additional condition for dim_date factory plant BI-5085 */
	and fmm.dim_dateuserstatussamr_fpp <> dd.dim_dateid;

/* BI-4824 - dd_dateuserstatusqcco_fpp - Date User Status QCCO was last set FPP */
	update fact_mmprodhierarchy_tmp_populate fmm
	set fmm.dim_dateuserstatusqcco_fpp = dd.dim_dateid
	from fact_mmprodhierarchy_tmp_populate fmm, dim_date dd, dim_plant pl
	where pl.dim_plantid = fmm.dim_plantid
	and pl.companycode = dd.companycode
	and fmm.dd_dateuserstatusqcco_fpp = dd.datevalue
	and pl.plantcode = dd.plantcode_factory  /* Additional condition for dim_date factory plant BI-5085 */
	and fmm.dim_dateuserstatusqcco_fpp <> dd.dim_dateid;

/* Add fields from Min Max - BI-4868 */
/* Min released stock in days RAW */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_minReleasedStockDaysRAW = fi.ct_minreleasedstockindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_rawpartid = fi.dim_partid
	and fmm.dim_plantid = fi.dim_plantid
	and fmm.ct_minReleasedStockDaysRAW <> fi.ct_minreleasedstockindays_merck;

/* Min released stock in days BULK */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_minReleasedStockDaysBULK = fi.ct_minreleasedstockindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_bulkpartid = fi.dim_partid
	and fmm.dim_plantid = fi.dim_plantid
	and fmm.ct_minReleasedStockDaysBULK <> fi.ct_minreleasedstockindays_merck;

/* Min released stock in days FPU */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_minReleasedStockDaysFPU = fi.ct_minreleasedstockindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_fpupartid = fi.dim_partid
	and fmm.dim_plantid = fi.dim_plantid
	and fmm.ct_minReleasedStockDaysFPU <> fi.ct_minreleasedstockindays_merck;

/* Min released stock in days FPPSite */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_minReleasedStockDaysFPPSite = fi.ct_minreleasedstockindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_fpppartid = fi.dim_partid
	and fmm.dim_plantid = fi.dim_plantid
	and fmm.ct_minReleasedStockDaysFPPSite <> fi.ct_minreleasedstockindays_merck;

/* Min released stock in days FPPComOp */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_minReleasedStockDaysFPPComOp = fi.ct_minreleasedstockindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_fpppartid_comops = fi.dim_partid
	and fmm.dim_plantid_comops = fi.dim_plantid
	and fmm.ct_minReleasedStockDaysFPPComOp <> fi.ct_minreleasedstockindays_merck;

/* Lot size in days RAW */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_lotSizeDaysRAW = fi.ct_lotsizeindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_rawpartid = fi.dim_partid
	and fmm.dim_plantid = fi.dim_plantid
	and fmm.ct_lotSizeDaysRAW <> fi.ct_lotsizeindays_merck;

/* Lot size in days BULK */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_lotSizeDaysBULK = fi.ct_lotsizeindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_bulkpartid = fi.dim_partid
	and fmm.dim_plantid = fi.dim_plantid
	and fmm.ct_lotSizeDaysBULK <> fi.ct_lotsizeindays_merck;

/* Lot size in days FPU */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_lotSizeDaysFPU = fi.ct_lotsizeindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_fpupartid = fi.dim_partid
	and fmm.dim_plantid = fi.dim_plantid
	and fmm.ct_lotSizeDaysFPU <> fi.ct_lotsizeindays_merck;

/* Lot size in days FPPSite */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_lotSizeDaysFPPSite = fi.ct_lotsizeindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_fpppartid = fi.dim_partid
	and fmm.dim_plantid = fi.dim_plantid
	and fmm.ct_lotSizeDaysFPPSite <> fi.ct_lotsizeindays_merck;

/* Lot size in days FPPComOp */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_lotSizeDaysFPPComOp = fi.ct_lotsizeindays_merck
from  fact_mmprodhierarchy_tmp_populate fmm, fact_inventoryatlas fi
where fmm.dim_fpppartid_comops = fi.dim_partid
	and fmm.dim_plantid_comops = fi.dim_plantid
	and fmm.ct_lotSizeDaysFPPComOp <> fi.ct_lotsizeindays_merck;
/* END BI-4868 */
	
	
/* add 5 new fields Float before production (in days) - BI-4886	*/
	/* Float before production (in days) - RAW */
update fact_mmprodhierarchy_tmp_populate fmm
set ct_floatBeforeProductionRAW = ifnull(T436A_VORGZ, 0)
from  fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, T436A
where fmm.dim_rawpartid =  dp.dim_partid
	and dp.schedmarginkey = ifnull(T436A_FHORI, 0)
	and dp.plant = ifnull(T436A_WERKS, 'Not Set')
	and ct_floatBeforeProductionRAW <> ifnull(T436A_VORGZ, 0);

/* Float before production (in days) - BULK */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_floatBeforeProductionBULK = ifnull(T436A_VORGZ, 0)
from  fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, T436A
where fmm.dim_bulkpartid =  dp.dim_partid
	and dp.schedmarginkey = ifnull(T436A_FHORI, 0)
	and dp.plant = ifnull(T436A_WERKS, 'Not Set')
	and fmm.ct_floatBeforeProductionBULK <> ifnull(T436A_VORGZ, 0);

/* Float before production (in days) - FPU */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_floatBeforeProductionFPU = ifnull(T436A_VORGZ, 0)
from  fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, T436A
where fmm.dim_fpupartid =  dp.dim_partid
	and dp.schedmarginkey = ifnull(T436A_FHORI, 0)
	and dp.plant = ifnull(T436A_WERKS, 'Not Set')
	and fmm.ct_floatBeforeProductionFPU <> ifnull(T436A_VORGZ, 0);

/* Float before production (in days) - FPP */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_floatBeforeProductionFPP = ifnull(T436A_VORGZ, 0)
from  fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, T436A
where fmm.dim_fpppartid =  dp.dim_partid
	and dp.schedmarginkey = ifnull(T436A_FHORI, 0)
	and dp.plant = ifnull(T436A_WERKS, 'Not Set')
	and fmm.ct_floatBeforeProductionFPP <> ifnull(T436A_VORGZ, 0);

/* Float before production (in days) - FPPComOp */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_floatBeforeProductionFPPComOp = ifnull(T436A_VORGZ, 0)
from  fact_mmprodhierarchy_tmp_populate fmm, dim_part dp, T436A
where fmm.dim_fpppartid_comops =  dp.dim_partid
	and dp.schedmarginkey = ifnull(T436A_FHORI, 0)
	and dp.plant = ifnull(T436A_WERKS, 'Not Set')
	and fmm.ct_floatBeforeProductionFPPComOp <> ifnull(T436A_VORGZ, 0);
/* END BI-4886 */

/* update the dim dates earlier in the script */
/* Update Actual receipt date to ComOps, according to the relation with Inspection Lot and EKBE - BI-4288 */
update fact_mmprodhierarchy_tmp_populate mm
set mm.dim_dateidactualreceipt_comops = dd.dim_dateid
from fact_inspectionlot f
	inner join dim_batch b ON f.dim_batchid = b.dim_batchid
	inner join EKBE_deliv d ON f.dd_materialdocno = d.EKBE_BELNR 
							AND f.dd_MaterialDocItemNo = d.EKBE_BUZEI
							AND d.EKBE_CHARG = b.batchnumber 
							AND d.EKBE_MATNR = b.partnumber 
							AND d.EKBE_WERKS = b.plantcode
	inner join fact_mmprodhierarchy_tmp_populate mm ON f.dd_inspectionlotno = mm.DD_INSPECTIONLOTNO_COMOPS
	inner join dim_plant p ON mm.dim_plantid_comops = p.dim_plantid
	inner join dim_date dd ON dd.datevalue = ifnull(d.EKBE_BUDAT,'Not Set')
							AND dd.plantcode_factory = p.plantcode
							AND dd.companycode = p.companycode
where mm.dim_dateidactualreceipt_comops <> dd.dim_dateid;

/* BI-4288 - Point Usage Decision Made Date FPP to Plant */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.dim_dateidfppusagedecisionmade = danew.dim_dateid
from fact_mmprodhierarchy_tmp_populate fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateidfppusagedecisionmade = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateidfppusagedecisionmade <> danew.dim_dateid;
	
/* Point Shipment Create Date to ComOps to Plant */  
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.dim_dateiddlvrdoccreated_comops = danew.dim_dateid
from fact_mmprodhierarchy_tmp_populate fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateiddlvrdoccreated_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateiddlvrdoccreated_comops <> danew.dim_dateid;
	
/* Point Actual Goods Issue Date to ComOps to Plant */  
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.dim_dateidactualgoodsissue_comops = danew.dim_dateid
from fact_mmprodhierarchy_tmp_populate fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid = pl.dim_plantid
	and fmm.dim_dateidactualgoodsissue_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateidactualgoodsissue_comops <> danew.dim_dateid;

/* BI-4288 - Point Actual Receipt Date to ComOps and Usage Decision Made Date ComOps to the same Plant */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.dim_dateidactualreceipt_comops = danew.dim_dateid
from fact_mmprodhierarchy_tmp_populate fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidactualreceipt_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode /* Additional condition for dim_date factory plant BI-5085 */
	and dim_dateidactualreceipt_comops <> danew.dim_dateid;

update fact_mmprodhierarchy_tmp_populate fmm
set fmm.dim_dateidusagedecisionmade_comops = danew.dim_dateid
from fact_mmprodhierarchy_tmp_populate fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidusagedecisionmade_comops = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode /* Additional condition for dim_date factory plant BI-5085 */
	and dim_dateidusagedecisionmade_comops <> danew.dim_dateid;
	
/* Point Shipment Create Date to Customer to PlantComOps */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.dim_dateiddlvrdoccreated_customer = danew.dim_dateid
from fact_mmprodhierarchy_tmp_populate fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateiddlvrdoccreated_customer = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateiddlvrdoccreated_customer <> danew.dim_dateid;

/* Point Actual Goods Issue Date to Customer to PlantComops */
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.dim_dateidactualgoodsissue_customer = danew.dim_dateid
from fact_mmprodhierarchy_tmp_populate fmm, dim_date da, dim_date danew, dim_plant pl
where fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_dateidactualgoodsissue_customer = da.dim_dateid
	and da.datevalue = danew.datevalue
	and danew.companycode = pl.companycode
	and danew.plantcode_factory = pl.plantcode 
	and dim_dateidactualgoodsissue_customer <> danew.dim_dateid;

/* Add Hit or miss (E2E LT) and Sales order hit- BI-4857 */
/* BI-5265 - update formulas - include ACTIVE_PROP_UNIT in addition to ACTIVE */
/* Correction: dont't use ma.batchclass_merck in grouping */
drop table if exists tmp_upd_hitormissE2E_LT;
create table tmp_upd_hitormissE2E_LT as
select dd_SalesDocNo_customer, dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode as PlantCodeSite,  	mfp.PartNumber, agidcc.MonthYear,
	CASE WHEN (CASE WHEN (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) +

						MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) +
						MAX( (CT_TARGETMASTERRECIPEBULK) ) +
						MAX( (mab.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) +
						MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) +

						MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) +
						MAX( (CT_TARGETMASTERRECIPEFPU) ) + 
						MAX( (mf.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) +

						MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) +
						MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
						MAX( (mfp.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) +

						MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
						MAX( (mfpc.GRProcessingTime) )+
						MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2) 


						) =0
					THEN 999
					ELSE ( (( (MAX( (agidcc.DateValue) - (grdr.DateValue) )*5/7) - (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) +

						MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) +
						MAX( (CT_TARGETMASTERRECIPEBULK) ) +
						MAX( (mab.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) +
						MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) +

						MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) +
						MAX( (CT_TARGETMASTERRECIPEFPU) ) + 
						MAX( (mf.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) +

						MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) +
						MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
						MAX( (mfp.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) +

						MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
						MAX( (mfpc.GRProcessingTime) )+
						MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2) 


						) )) / (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) +
						MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) +
						
						MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) +
						MAX( (CT_TARGETMASTERRECIPEBULK) ) +
						MAX( (mab.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) +
						MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) +

						MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) +
						MAX( (CT_TARGETMASTERRECIPEFPU) ) + 
						MAX( (mf.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) +

						MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) +
						MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
						MAX( (mfp.GRProcessingTime) ) +
						MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) +

						MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
						MAX( (mfpc.GRProcessingTime) )+
						MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) +
						MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2) 


						) )*100
					END) > 25
			THEN '0'
			WHEN (CASE WHEN (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) +

					MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) +
					MAX( (CT_TARGETMASTERRECIPEBULK) ) +
					MAX( (mab.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) +
					MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) +

					MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) +
					MAX( (CT_TARGETMASTERRECIPEFPU) ) + 
					MAX( (mf.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) +

					MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) +
					MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
					MAX( (mfp.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) +

					MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
					MAX( (mfpc.GRProcessingTime) )+
					MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2) 


					) =0
				THEN 999
				ELSE ( (( (MAX( (agidcc.DateValue) - (grdr.DateValue) )*5/7) - (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) +

					MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) +
					MAX( (CT_TARGETMASTERRECIPEBULK) ) +
					MAX( (mab.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) +
					MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) +

					MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) +
					MAX( (CT_TARGETMASTERRECIPEFPU) ) + 
					MAX( (mf.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) +

					MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) +
					MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
					MAX( (mfp.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) +

					MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
					MAX( (mfpc.GRProcessingTime) )+
					MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2) 


					) )) / (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) +
					MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) +

					MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) +
					MAX( (CT_TARGETMASTERRECIPEBULK) ) +
					MAX( (mab.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) +
					MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) +

					MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) +
					MAX( (CT_TARGETMASTERRECIPEFPU) ) + 
					MAX( (mf.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) +

					MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) +
					MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
					MAX( (mfp.GRProcessingTime) ) +
					MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) +

					MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
					MAX( (mfpc.GRProcessingTime) )+
					MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) +
					MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2) 


					) )*100
			END) < -25
	THEN '0'
	ELSE '100'
	END as ct_hitormissE2E_LT,
/* Add Actual E2E LT and Target E2E LT - for Weighted av. Actual LT and Weighted av. Target LT - BI-4857 */
/* Actual E2E LT */	
	MAX( (agidcc.DateValue) - (grdr.DateValue) ) *5/7 as ct_actualE2E_LT,
/* Target E2E LT */
	MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) +
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) +

	MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) +
	MAX( (CT_TARGETMASTERRECIPEBULK) ) +
	MAX( (mab.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) +
	MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) +

	MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) +
	MAX( (CT_TARGETMASTERRECIPEFPU) ) + 
	MAX( (mf.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) +
	MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) +

	MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) +
	MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
	MAX( (mfp.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) +
	MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) +

	MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
	MAX( (mfpc.GRProcessingTime) )+
	MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) +
	MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2)  as ct_targetE2E_LT,
/* Madalina 13 Feb 2017 - BI-4857 */
/* Create new measures in order to obtain different values for Total Weighted av. Actual LT and Total Weighted av. Target LT*/ 
/* Actual LT * Sales - backend and Target LT * Sales - backend */
	avg(AMT_ORDERAMOUNTSALES) * MAX( (agidcc.DateValue) - (grdr.DateValue) ) *5/7 as ct_actualLTSales,
	avg(AMT_ORDERAMOUNTSALES) * (MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) +
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) +
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) +

	MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) +
	MAX( (CT_TARGETMASTERRECIPEBULK) ) +
	MAX( (mab.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) +
	MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) +

	MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) +
	MAX( (CT_TARGETMASTERRECIPEFPU) ) + 
	MAX( (mf.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) +
	MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) +

	MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) +
	MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
	MAX( (mfp.GRProcessingTime) ) +
	MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) +
	MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) +

	MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
	MAX( (mfpc.GRProcessingTime) )+
	MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) +
	MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2) ) as ct_TargetLTSales

from fact_mmprodhierarchy_tmp_populate fmm, dim_part mab, dim_part mfpc, dim_part mfp, dim_part mf, dim_part ma, dim_date grdr, dim_date agidcc, dim_plant pl, dim_plant plsite
where fmm.dim_bulkpartid = mab.dim_partid
	and fmm.dim_fpppartid_comops = mfpc.dim_partid
	and fmm.dim_fpppartid =	mfp.dim_partid
	and fmm.dim_fpupartid = mf.dim_partid
	and fmm.dim_rawpartid = ma.dim_partid
	and fmm.dim_dateidrawpostingdate = grdr.dim_dateid
	and fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
	and fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_plantid = plsite.dim_plantid
	and agidcc.datevalue >= add_years(current_date, -2)
	and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')
group by dd_SalesDocNo_customer, dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode, mfp.PartNumber, agidcc.MonthYear;

update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_hitormissE2E_LT = tmp.ct_hitormissE2E_LT,
	fmm.ct_actualE2E_LT = tmp.ct_actualE2E_LT,
	fmm.ct_targetE2E_LT = tmp.ct_targetE2E_LT,
	fmm.ct_actualLTSales = tmp.ct_actualLTSales,
	fmm.ct_TargetLTSales = tmp.ct_TargetLTSales
from fact_mmprodhierarchy_tmp_populate fmm, tmp_upd_hitormissE2E_LT tmp, dim_part ma, dim_date agidcc, dim_plant pl, dim_plant plsite, dim_part mfp
where fmm.dd_SalesDocNo_customer = tmp.dd_SalesDocNo_customer
	and fmm.dd_SalesItemNo_customer = tmp.dd_SalesItemNo_customer
	and fmm.dim_rawpartid = ma.dim_partid
	and fmm.dim_fpppartid =	mfp.dim_partid
	--and ma.batchclass_merck = tmp.batchclass_merck
	and fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
	and fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_plantid = plsite.dim_plantid
	and agidcc.MonthYear = tmp.MonthYear
	and mfp.PartNumber = tmp.PartNumber
	and plsite.PlantCode = tmp.PlantCodeSite
	and pl.PlantCode = tmp.PlantCode
	and mfp.ProductFamily_Merck = tmp.ProductFamily_Merck;
	/* and fmm.ct_hitormissE2E_LT <> tmp.ct_hitormissE2E_LT */
	
update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_hitvalue = (case when (CT_HITORMISSE2E_LT) = 0 then 0 else (AMT_ORDERAMOUNTSALES) end);	

/* End BI-4857 */

/* Reset all the values from the Sales aggregation dimension - BI-3287 */
/* BI-5265 - include ACTIVE_PROP_UNIT in addition to ACTIVE */
delete from dim_aggregationMMProdOrd;

delete from number_fountain m where m.table_name = 'dim_aggregationMMProdOrd';
insert into number_fountain
select 'dim_aggregationMMProdOrd',
ifnull(max(da.dim_aggregationMMProdOrdId),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_aggregationMMProdOrd da;

insert into dim_aggregationMMProdOrd
	(	
		dim_aggregationMMProdOrdId,
		ProductFamily_Merck,
		PlantCode,
		PlantCodeSite,
		PartNumber,
		MonthYear,
		sum_fpp,
		salesOrderHit_fpp,
		totalSalesOrderValueHitUSD_fpp,
		TotalActualLTSales_fpp,
		TotalTargetLTSales_fpp
	)
select (select max_id  from number_fountain   where table_name = 'dim_aggregationMMProdOrd') + row_number() over(ORDER BY '') AS dim_aggregationMMProdOrdId,
	x.ProductFamily_Merck, 
	x.PlantCode, 
	x.PlantCodeSite,
	x.PartNumber,	
	x.MonthYear,
    sum(amt_orderAmountSales) sum_fpp,
	avg(ct_hitormissE2E_LT) as salesOrderHit_fpp,
	sum(ct_hitvalue) as totalSalesOrderValueHitUSD_fpp,
	sum(ct_actualLTSales) as TotalActualLTSales_fpp,
	sum(ct_TargetLTSales) as TotalTargetLTSales_fpp
	/* avg(amt_orderAmountSales)/ ( case when sum(amt_orderAmountSales) <> 0 then  sum(amt_orderAmountSales) else 1 end) * max(ct_actualE2E_LT) as weightedActualLT_fpp,
	avg(amt_orderAmountSales)/ ( case when sum(amt_orderAmountSales) <> 0 then  sum(amt_orderAmountSales) else 1 end) * max(ct_targetE2E_LT) as weightedTargetLT_fpp */
from 
	(select mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, p.ProductFamily_Merck, pl.PlantCode, 
		p.PartNumber, plsite.PlantCode as PlantCodeSite, dd.MonthYear,
		/* amt_orderAmountSales * amt_ExchangeRate_GBL as amt_orderAmountSales, */
		amt_orderAmountSales as amt_orderAmountSales,
		ct_hitormissE2E_LT as ct_hitormissE2E_LT,
		ct_hitvalue as ct_hitvalue,
		ct_actualLTSales as ct_actualLTSales,
		ct_TargetLTSales as ct_TargetLTSales,
		row_number() over ( partition by mmp.dd_SalesDocNo_customer, mmp.dd_SalesItemNo_customer, p.ProductFamily_Merck, pl.PlantCode,
							p.PartNumber, plsite.PlantCode, dd.MonthYear order by '') rn
		from fact_mmprodhierarchy_tmp_populate mmp, dim_part p, dim_date dd, dim_plant pl, dim_plant plsite, dim_part ma
		where mmp.dim_fpppartid = p.dim_partid
		and mmp.dim_plantid_comops = pl.dim_plantid
		and mmp.dim_plantid = plsite.dim_plantid
		and mmp.dim_dateidactualgoodsissue_customer = dd.dim_dateid
		and dd.datevalue >= add_years(current_date, -2)
		and mmp.dim_rawpartid = ma.dim_partid
		and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')
		) x
	where x.rn = 1
	group by CUBE (ProductFamily_Merck, PlantCode, PlantCodeSite, PartNumber, MonthYear);  

/* Madalina 13 Feb 2017 - Change the calculation for Total Weighted av. Actual LT -backend and Total Weighted av. Target LT -backend BI-4857 */	
update dim_aggregationMMProdOrd dimagg
set dimagg.totalWeightedActualLT = TotalActualLTSales_fpp/ case when sum_fpp = 0 then 1 else sum_fpp end,
	dimagg.totalWeightedTargetLT = TotalTargetLTSales_fpp/ case when sum_fpp = 0 then 1 else sum_fpp end;

/* 26 jan 2017 Georgiana Changes - according to BI 5295: Insert all GPF codes in dim_aggregationMMProdOrd*/
delete from number_fountain m where m.table_name = 'dim_aggregationMMProdOrd';
insert into number_fountain
select 'dim_aggregationMMProdOrd',
ifnull(max(da.dim_aggregationMMProdOrdId),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_aggregationMMProdOrd da;

insert into dim_aggregationMMProdOrd (dim_aggregationMMProdOrdid,ProductFamily_Merck)
select (select max_id  from number_fountain   where table_name = 'dim_aggregationMMProdOrd') + row_number() over(ORDER BY '') AS dim_aggregationMMProdOrdId,
t.ProductFamily_Merck
from (
select distinct ProductFamily_Merck from dim_part dp
where not exists (select 1 from dim_aggregationMMProdOrd agg where dp.ProductFamily_Merck=agg.ProductFamily_Merck)) t;
/*End of changes 26 Jan 2017*/

update dim_aggregationMMProdOrd
set plantcode = 'ZZZZ' where Plantcode is null;

update dim_aggregationMMProdOrd
set ProductFamily_Merck = 'ZZZZ' where ProductFamily_Merck is null;

update dim_aggregationMMProdOrd
set PlantCodeSite = 'ZZZZ' where PlantCodeSite is null;

update dim_aggregationMMProdOrd
set PartNumber = 'ZZZZ' where PartNumber is null;

update dim_aggregationMMProdOrd
set MonthYear = 'ZZZZ' where MonthYear is null;
	
/* Avoid table recreation */
/* drop table if exists fact_mmprodhierarchy
rename table fact_mmprodhierarchy_tmp_populate to fact_mmprodhierarchy */

/*Georgiana Changes 23 Jan 2017 adding new field ct_overallsupplyhealthscore, according to BI-5295*/

drop table if exists tmp_for_upd_SupplyHealthScore;
create table tmp_for_upd_SupplyHealthScore as 
SELECT x.*  from       ( 
	SELECT 
	DFA.ProductFamilyDescription_Merck , 
	DFA.ct_avg_hit_miss AS  HitOrMissFPPComop, 
	MinMax.CT_HITORMISS AS GrobalHitLast2YDFA , 
	salesorder.ct_avg_hit_miss_lifr AS GrobalHitLast2YLIFR , 
	materialmaster.CAPACITY_SCORE AS CapacityScore,
   ifnull(mmprod.SalesORderValueHit,0) as SalesORderValueHit	
FROM (SELECT aplfprt.ProductFamilyDescription_Merck,ROUND(AVG(ct_avg_hit_miss),2) as ct_avg_hit_miss 
					FROM fact_atlaspharmlogiforecast_merck AS f_iaplf 
					INNER JOIN Dim_Part AS aplfprt ON f_iaplf.dim_partid = aplfprt.Dim_Partid  
					INNER JOIN Dim_Date AS repdt ON f_iaplf.dim_dateidreporting = repdt.Dim_Dateid  
			WHERE (repdt.DateValue  BETWEEN '2015-01-01' AND '2016-12-31')  
			AND f_iaplf.dd_version = 'SFA'  
			AND f_iaplf.dd_regiondestionation NOT IN ('Head Quarters','NOT APPLICABLE')  
	        GROUP BY  aplfprt.ProductFamilyDescription_Merck
	) DFA  
LEFT  JOIN (SELECT dp.ProductFamilyDescription_Merck,ROUND(AVG(CT_HITORMISS),2) as CT_HITORMISS 
                   FROM fact_inventoryatlashistory AS f_invatlashist 
                   INNER JOIN Dim_Date AS dd3 ON f_invatlashist.dim_snapshotdateid = dd3.Dim_Dateid  
                   INNER JOIN Dim_Part AS dp ON f_invatlashist.dim_partid = dp.Dim_Partid 
                   INNER JOIN Dim_Plant AS dpl ON f_invatlashist.dim_plantid = dpl.Dim_Plantid  
              WHERE  dd3.WeekDayAbbreviation  = 'Mon'
              AND dp.ItemSubType_Merck = 'FPP'
              AND dpl.tacticalring_merck = 'ComOps'
              GROUP BY  dp.ProductFamilyDescription_Merck
	        ) MinMax ON (  DFA.ProductFamilyDescription_Merck = MinMax.ProductFamilyDescription_Merck ) 
LEFT  JOIN  (SELECT prt.ProductFamilyDescription_Merck,ROUND(AVG(ct_avg_hit_miss_lifr),0) as ct_avg_hit_miss_lifr 
                   FROM fact_salesorder AS f_so 
                   INNER JOIN Dim_Part AS prt ON f_so.Dim_Partid = prt.Dim_Partid  
                   INNER JOIN Dim_Date AS soegidt ON f_so.dim_DateidExpGI_Merck = soegidt.Dim_Dateid  
            WHERE (soegidt.DateValue  != current_date) AND (soegidt.DateValue  BETWEEN '2015-01-01' AND '2016-12-31')  AND prt.ProductHierarchy = 'Not Set' AND f_so.dd_ItemRelForDelv = 'X' 
 GROUP BY  prt.ProductFamilyDescription_Merck
             ) salesorder ON (  DFA.ProductFamilyDescription_Merck = salesorder.ProductFamilyDescription_Merck ) 
 INNER JOIN (SELECT m.ProductFamilyDescription_Merck,ROUND(AVG( (m.CAPACITY_SCORE) ),0) as CAPACITY_SCORE 
                   FROM fact_materialmaster AS f_mmi 
                   INNER JOIN Dim_Part AS m ON f_mmi.dim_materialmasterid = m.Dim_Partid  
                   GROUP BY  m.ProductFamilyDescription_Merck
			)  materialmaster  ON (  DFA.ProductFamilyDescription_Merck = materialmaster.ProductFamilyDescription_Merck )
LEFT  JOIN (SELECT mfp.ProductFamilyDescription_Merck,AVG(100 * agg3.totalSalesOrderValueHitUSD_fpp / case when agg3.SUM_FPP=0 then 1 else agg3.SUM_FPP end) as  SalesORderValueHit 
 FROM fact_mmprodhierarchy_tmp_populate AS f_mmprodhier 
 INNER JOIN Dim_Part AS mfp ON f_mmprodhier.DIM_FPPPARTID = mfp.Dim_Partid 
 INNER JOIN Dim_Date AS agidcc ON f_mmprodhier.DIM_DATEIDACTUALGOODSISSUE_CUSTOMER = agidcc.Dim_Dateid  
 INNER JOIN Dim_Plant AS pc ON f_mmprodhier.DIM_PLANTID_COMOPS = pc.Dim_Plantid  
 INNER JOIN Dim_Plant AS pl ON f_mmprodhier.DIM_PLANTID = pl.Dim_Plantid  
 INNER JOIN Dim_Part AS mab ON f_mmprodhier.DIM_BULKPARTID = mab.Dim_Partid  
 INNER JOIN Dim_Part AS mfpc ON f_mmprodhier.DIM_FPPPARTID_COMOPS = mfpc.Dim_Partid  
 INNER JOIN Dim_Part AS mf ON f_mmprodhier.DIM_FPUPARTID = mf.Dim_Partid  
 INNER JOIN Dim_Part AS ma ON f_mmprodhier.DIM_RAWPARTID = ma.Dim_Partid  
 INNER JOIN Dim_Date AS posl ON f_mmprodhier.DIM_DATEIDSCHEDORDER = posl.Dim_Dateid  
 INNER JOIN dim_aggregationMMProdOrd AS agg3 ON agg3.PlantCode = (case when 'ALL' = 'ALL' then 'ZZZZ' else pc.plantcode end) 
 and agg3.ProductFamily_Merck = (case when 'ZZZZ' = 'ALL' then 'ZZZZ' else mfp.ProductFamily_Merck end) 
 and agg3.PlantCodeSite = (case when 'ALL' = 'ALL' then 'ZZZZ' else pl.PlantCode end) 
 and agg3.PartNumber = (case when 'ALL' = 'ALL' then 'ZZZZ' else mfp.PartNumber end) and 
 agg3.MonthYear = (case when 'ALL' = 'ALL' then 'ZZZZ' else agidcc.MonthYear end)   
 GROUP BY  mfp.ProductFamilyDescription_Merck) mmprod
 ON (  DFA.ProductFamilyDescription_Merck = mmprod.ProductFamilyDescription_Merck )
) x order by ProductFamilyDescription_Merck ;

merge into fact_mmprodhierarchy_tmp_populate f_mmp
using (select distinct fact_mmprodhierarchyid,
0.15 *( case when SalesORderValueHit > 80 then 3 when SalesORderValueHit > 60 then 2 else 1 end)
+ 0.15 * (case when tmp.GrobalHitLast2YDFA > 80 then 3 when tmp.GrobalHitLast2YDFA > 60 then 2 else 1 end)
+ 0.15* ( case when tmp.HitOrMissFPPComop > 80 then 3 when tmp.HitOrMissFPPComop > 60 then 2 else 1 end)
+ 0.3 * tmp.CapacityScore
+ 0.25* ( case when tmp.GrobalHitLast2YLIFR > 80 then 3 else 1 end) as overallsupplyhealthscore
 from fact_mmprodhierarchy_tmp_populate f_mmp,tmp_for_upd_SupplyHealthScore tmp,dim_part dp 
where tmp.ProductFamilyDescription_Merck=dp.ProductFamilyDescription_Merck
and  f_mmp.dim_fpppartid=dp.dim_partid
/*and dp.ProductFamilyDescription_Merck='6075 Rotavec Corona'*/) t
on t.fact_mmprodhierarchyid=f_mmp.fact_mmprodhierarchyid
when matched then update set f_mmp.ct_overallsupplyhealthscore=t.overallsupplyhealthscore;


/*Georgiana 23 Jan 2017 End*/

/* Madalina 13 Feb 2017 - Change the calculation for Total Weighted av. Actual LT -backend and Total Weighted av. Target LT -backend, but keep the old code for further reference BI-4857 */
/* Madalina 30 Jan 2017 - BI-4857 */
/* Add Weighted av. Actual LT and Weighted av. Target LT*/ 
/* drop table if exists tmp_upd_weighted_LT
create table tmp_upd_weighted_LT as
select 
	dd_SalesDocNo_customer, dd_SalesItemNo_customer, 	mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode as PlantCodeSite,  	mfp.PartNumber, agidcc.MonthYear,
	AVG( AMT_ORDERAMOUNTSALES ) / case when SUM( agg3.SUM_FPP ) = 0 then 1 else SUM( agg3.SUM_FPP ) end * max((agidcc.DateValue) - (grdr.DateValue))*5/7 as ct_WeightedActualLT,
	
	( AVG( (AMT_ORDERAMOUNTSALES) ) / case when SUM( agg3.SUM_FPP ) = 0 then 1 else SUM( agg3.SUM_FPP ) end ) * 
	( MAX( CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (ma.GRProcessingTime) ELSE 0 END) + 
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_MINRELEASEDSTOCKDAYSRAW) *5/7 ELSE 0 END) + 
	MAX(CASE WHEN (ma.batchclass_merck) = 'ACTIVE' or (ma.batchclass_merck) = 'ACTIVE_PROP_UNIT' THEN (CT_LOTSIZEDAYSRAW) *5/7 /2 ELSE 0 END) + 
	MAX( (CT_FLOATBEFOREPRODUCTIONBULK) ) + MAX( (CT_TARGETMASTERRECIPEBULK) ) + MAX( (mab.GRProcessingTime) ) + MAX( (CT_MINRELEASEDSTOCKDAYSBULK) *5/7) + 
	MAX( (CT_LOTSIZEDAYSBULK) *5/7 /2 ) + MAX( (CT_FLOATBEFOREPRODUCTIONFPU) ) + MAX( (CT_TARGETMASTERRECIPEFPU) ) + MAX( (mf.GRProcessingTime) ) + 
	MAX( (CT_MINRELEASEDSTOCKDAYSFPU) *5/7) + MAX( (CT_LOTSIZEDAYSFPU) *5/7 /2) + MAX( (CT_FLOATBEFOREPRODUCTIONFPP) ) + MAX( (CT_TARGETMASTERRECIPEFPP) ) + 
	MAX( (mfp.GRProcessingTime) ) + MAX( (CT_MINRELEASEDSTOCKDAYSFPPSITE) *5/7) + MAX( (CT_LOTSIZEDAYSFPPSITE) *5/7 /2) + MAX( (ifnull(mfpc.LeadTime,0)) *5/7) + 
	MAX( (mfpc.GRProcessingTime) )+ MAX( (CT_MINRELEASEDSTOCKDAYSFPPCOMOP) *5/7) + MAX( (CT_LOTSIZEDAYSFPPCOMOP) *5/7 /2) ) as ct_WeightedTargetLT
from fact_mmprodhierarchy_tmp_populate fmm
	inner join dim_part mab on fmm.dim_bulkpartid = mab.dim_partid
	inner join dim_part mfpc on fmm.dim_fpppartid_comops = mfpc.dim_partid
	inner join dim_part mfp on fmm.dim_fpppartid =	mfp.dim_partid
	inner join dim_part mf on fmm.dim_fpupartid = mf.dim_partid
	inner join dim_part ma on fmm.dim_rawpartid = ma.dim_partid
	inner join dim_date grdr on fmm.dim_dateidrawpostingdate = grdr.dim_dateid
	inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid 
	inner join dim_plant pl on fmm.dim_plantid_comops = pl.dim_plantid
	inner join dim_plant plsite on fmm.dim_plantid = plsite.dim_plantid
	INNER JOIN dim_aggregationMMProdOrd AS agg3 ON agg3.PlantCode = (case when 'ALL' = 'ALL' then 'ZZZZ' else pl.plantcode end) 
		 and agg3.ProductFamily_Merck = (case when 'ZZZZ' = 'ALL' then 'ZZZZ' else mfp.ProductFamily_Merck end) 
		 and agg3.PlantCodeSite = (case when 'ALL' = 'ALL' then 'ZZZZ' else plsite.PlantCode end) 
		 and agg3.PartNumber = (case when 'ALL' = 'ALL' then 'ZZZZ' else mfp.PartNumber end)  
		 and agg3.MonthYear = (case when 'ALL' = 'ALL' then 'ZZZZ' else agidcc.MonthYear end) 
where 
    agidcc.datevalue >= add_years(current_date, -2)
	and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT')
group by dd_SalesDocNo_customer, dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode, mfp.PartNumber, agidcc.MonthYear

update fact_mmprodhierarchy_tmp_populate fmm
set fmm.ct_WeightedActualLT = tmp.ct_WeightedActualLT,
	fmm.ct_WeightedTargetLT = tmp.ct_WeightedTargetLT
from fact_mmprodhierarchy_tmp_populate fmm, tmp_upd_weighted_LT tmp, dim_part ma, dim_date agidcc, dim_plant pl, dim_plant plsite, dim_part mfp
where fmm.dd_SalesDocNo_customer = tmp.dd_SalesDocNo_customer
	and fmm.dd_SalesItemNo_customer = tmp.dd_SalesItemNo_customer
	and fmm.dim_rawpartid = ma.dim_partid
	and fmm.dim_fpppartid =	mfp.dim_partid
	--and ma.batchclass_merck = tmp.batchclass_merck
	and fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid
	and fmm.dim_plantid_comops = pl.dim_plantid
	and fmm.dim_plantid = plsite.dim_plantid
	and agidcc.MonthYear = tmp.MonthYear
	and mfp.PartNumber = tmp.PartNumber
	and plsite.PlantCode = tmp.PlantCodeSite
	and pl.PlantCode = tmp.PlantCode
	and mfp.ProductFamily_Merck = tmp.ProductFamily_Merck */

/* Add Total Weighted av. Actual LT and Total Weighted av. Target LT*/ 
/* drop table if exists tmp_upd_dimaggregation
create table tmp_upd_dimaggregation as
select x.ProductFamily_Merck, x.PlantCode, x.PlantCodeSite, x.PartNumber, x.MonthYear,
	sum(x.ct_WeightedActualLT) as totalWeightedActualLT,
	sum(x.ct_WeightedTargetLT) as totalWeightedTargetLT
	from 
	( 
		select fmm.dd_SalesDocNo_customer, fmm.dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode, plsite.PlantCode as PlantCodeSite, mfp.PartNumber, agidcc.MonthYear,
				fmm.ct_WeightedActualLT,
				fmm.ct_WeightedTargetLT,
				row_number() over ( partition by fmm.dd_SalesDocNo_customer, fmm.dd_SalesItemNo_customer, mfp.ProductFamily_Merck, pl.PlantCode,
								mfp.PartNumber, plsite.PlantCode, agidcc.MonthYear order by '') rn
		from fact_mmprodhierarchy_tmp_populate fmm
			inner join dim_part mfp on fmm.dim_fpppartid =	mfp.dim_partid
			inner join dim_date agidcc on fmm.dim_dateidactualgoodsissue_customer = agidcc.dim_dateid 
			inner join dim_plant pl on fmm.dim_plantid_comops = pl.dim_plantid
			inner join dim_plant plsite on fmm.dim_plantid = plsite.dim_plantid
			inner join dim_part ma on fmm.dim_rawpartid = ma.dim_partid
		where agidcc.datevalue >= add_years(current_date, -2)
		and ma.batchclass_merck IN ('ACTIVE', 'ACTIVE_PROP_UNIT') ) x
	where x.rn = 1
 group by CUBE (x.ProductFamily_Merck, x.PlantCode, x.PlantCodeSite, x.PartNumber, x.MonthYear)

update tmp_upd_dimaggregation
set plantcode = 'ZZZZ' where Plantcode is null
update tmp_upd_dimaggregation
set ProductFamily_Merck = 'ZZZZ' where ProductFamily_Merck is null
update tmp_upd_dimaggregation
set PlantCodeSite = 'ZZZZ' where PlantCodeSite is null
update tmp_upd_dimaggregation
set PartNumber = 'ZZZZ' where PartNumber is null
update tmp_upd_dimaggregation
set MonthYear = 'ZZZZ' where MonthYear is null

update dim_aggregationMMProdOrd dimagg
set dimagg.totalWeightedActualLT = tmpagg.totalWeightedActualLT,
	dimagg.totalWeightedTargetLT = tmpagg.totalWeightedTargetLT
from tmp_upd_dimaggregation tmpagg, dim_aggregationMMProdOrd dimagg
where  dimagg.ProductFamily_Merck = tmpagg.ProductFamily_Merck
	and dimagg.PlantCode = tmpagg.PlantCode
	and dimagg.PlantCodeSite = tmpagg.PlantCodeSite
	and dimagg.PartNumber = tmpagg.PartNumber
	and dimagg.MonthYear = tmpagg.MonthYear */
/* END BI-4857 */


truncate table fact_mmprodhierarchy;
insert into fact_mmprodhierarchy
select * from fact_mmprodhierarchy_tmp_populate;