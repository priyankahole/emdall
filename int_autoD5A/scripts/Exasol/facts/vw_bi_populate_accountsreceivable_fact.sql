
/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 10 May 2013 */
/*   Description    : Stored Proc bi_populate_accountsreceivable_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   16 Oct 2014      Amar    1.18            Added Dim_ChartofAccountsIdARDocGL for HKONT                         */
/*   05 Sep 2013      Lokesh	1.1  		  Exchange rate changes for transaction,local and global currencies */
/*   10 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/

/* Refreshing relevant tables */

/*
mysqldump --default-character-set=utf8 -uoctet -poctet --skip-triggers --routines --single-transaction columbia275 --tables
bsad bsid but000 but050 cvi_cust_link dim_accountreceivablestatus dim_accountsreceivabledocstatus dim_blockingpaymentreason
dim_businessarea dim_chartofaccounts dim_company dim_creditcontrolarea dim_currency dim_customer dim_customerpaymentterms
dim_date dim_documenttypetext dim_paymentmethod dim_paymentreason dim_postingkey dim_riskclass dim_specialglindicator dim_specialgltransactiontype
fact_accountsreceivable fact_billing fact_disputemanagement fact_salesorder fdm_dcproc scmg_t_case_attr systemproperty ukmbp_cms ukmbp_cms_sgm
| mysql -uoctet -poctet --host=192.168.200.125  -C columbia99 &

*/

/*********************************************START****************************************************************/

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

insert into number_fountain
select 'processinglog',ifnull(max(f.processinglogid ), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from processinglog f;

Drop table if exists tmp_far_variable_holder;

create table tmp_far_variable_holder
AS
Select  CONVERT(varchar(7), ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD')) AS pGlobalCurrency;

DROP TABLE IF EXISTS tmp_fact_accountsreceivable;

CREATE TABLE tmp_fact_accountsreceivable  like  fact_accountsreceivable INCLUDING DEFAULTS INCLUDING IDENTity;

delete from NUMBER_FOUNTAIN where table_name = 'tmp_fact_accountsreceivable';

insert into number_fountain
select 'tmp_fact_accountsreceivable',ifnull(max(f.fact_accountsreceivableid ), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_accountsreceivable f;


INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'bi_populate_accountsreceivable_fact START',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


UPDATE BSID SET BSID_ZUONR = ifnull(BSID_ZUONR,'Not Set'), BSID_BUKRS = ifnull(BSID_BUKRS,'Not Set'), BSID_KUNNR = ifnull(BSID_KUNNR,'Not Set');
UPDATE BSAD SET BSAD_ZUONR = ifnull(BSAD_ZUONR,'Not Set'), BSAD_BUKRS = ifnull(BSAD_BUKRS,'Not Set'), BSAD_KUNNR = ifnull(BSAD_KUNNR,'Not Set');

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE  tmp_fact_accountsreceivable far',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

/* Update 1 - Part 1
UPDATE tmp_fact_accountsreceivable far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET     ct_CashDiscountDays1 = BSID_ZBD1P,
          amt_CashDiscountDocCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_WSKTO,
          amt_CashDiscountLocalCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_SKNTO,
          amt_InLocalCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_DMBTR,
          amt_TaxInDocCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_WMWST,
          amt_TaxInLocalCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END) * BSID_MWSTS,
          amt_InDocCurrency = (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WRBTR,
          dd_AccountingDocItemNo = BSID_BUZEI,
          dd_AccountingDocNo = BSID_BELNR,
          dd_AssignmentNumber = ifnull(BSID_ZUONR,'Not Set')
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR


UPDATE tmp_fact_accountsreceivable far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET
          Dim_ClearedFlagId = ifnull((SELECT Dim_AccountReceivableStatusId
                                        FROM Dim_AccountReceivableStatus ars
                                       WHERE ars.Status = CASE WHEN BSID_REBZG IS NOT NULL AND BSID_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END), 1),
          Dim_DateIdAccDocDateEntered = ifnull((SELECT dim_dateid
                                                 FROM dim_date dt
                                                WHERE dt.DateValue = BSID_CPUDT AND dt.CompanyCode = arc.BSID_BUKRS), 1) ,
          Dim_DateIdBaseDateForDueDateCalc = ifnull((SELECT dim_dateid
                                                       FROM dim_date dt
                                                      WHERE dt.DateValue = BSID_ZFBDT AND dt.CompanyCode = arc.BSID_BUKRS), 1) ,
          Dim_DateIdCreated = ifnull((SELECT dim_dateid
                                        FROM dim_date dt
                                       WHERE dt.DateValue = BSID_BLDAT AND  dt.CompanyCode = arc.BSID_BUKRS), 1) ,
          Dim_DateIdPosting = ifnull((SELECT dim_dateid
                                        FROM dim_date dt
                                       WHERE dt.DateValue = BSID_BUDAT AND dt.CompanyCode = arc.BSID_BUKRS), 1)
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR


UPDATE tmp_fact_accountsreceivable far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET
          dd_debitcreditid = (CASE WHEN BSID_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END),
          dd_ClearingDocumentNo = ifnull(BSID_AUGBL,'Not Set') ,
          Dim_AccountsReceivableDocStatusId = ifnull((SELECT Dim_AccountsReceivableDocStatusId
                                                        FROM Dim_AccountsReceivableDocStatus ards
                                                       WHERE ards.Status = BSID_BSTAT), 1) ,
          dd_FixedPaymentTerms = BSID_ZBFIX,
          dd_InvoiceNumberTransBelongTo = ifnull(BSID_REBZG,'Not Set'),
          dd_NetPaymentTermsPeriod = BSID_ZBD3T,
          Dim_BlockingPaymentReasonId = ifnull((SELECT Dim_BlockingPaymentReasonId
                                                  FROM Dim_BlockingPaymentReason bpr
                                                 WHERE bpr.BlockingKeyPayment = BSID_ZLSPR), 1),
          Dim_BusinessAreaId = ifnull((SELECT Dim_BusinessAreaId
                                       FROM Dim_BusinessArea ba
                                       WHERE ba.BusinessArea = BSID_GSBER), 1)
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR

UPDATE tmp_fact_accountsreceivable far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET
          Dim_ChartOfAccountsId = ifnull((SELECT Dim_ChartOfAccountsId
                                            FROM Dim_ChartOfAccounts coa INNER JOIN dim_company dc ON dc.ChartOfAccounts = coa.CharOfAccounts
                                           WHERE dc.CompanyCode = BSID_BUKRS AND coa.GLAccountNumber = BSID_SAKNR), 1) ,
          far.dim_companyid = dcm.Dim_CompanyId,
          Dim_CreditControlAreaId = ifnull((SELECT Dim_CreditControlAreaid
                                              FROM dim_creditcontrolarea cca
                                             WHERE cca.CreditControlAreaCode = BSID_KKBER), 1) ,
          Dim_Currencyid = ifnull((SELECT dim_currencyid
                                     FROM dim_currency c
                                    WHERE c.CurrencyCode = dcm.Currency),1)  ,
          far.Dim_CustomerID = dc.Dim_CustomerID,
          Dim_DateIdClearing = ifnull((SELECT dim_dateid
                                         FROM dim_date dt
                                        WHERE dt.DateValue = BSID_AUGDT AND dt.CompanyCode = arc.BSID_BUKRS), 1) ,
          Dim_DocumentTypeId = ifnull((SELECT dim_documenttypetextid
                                         FROM dim_documenttypetext dtt
                                        WHERE dtt.type = BSID_BLART), 1)

WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR

UPDATE tmp_fact_accountsreceivable far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET

          Dim_PaymentReasonId = ifnull((SELECT Dim_PaymentReasonId
                                         FROM Dim_PaymentReason pr
                                        WHERE pr.PaymentReasonCode = BSID_RSTGR AND pr.CompanyCode = BSID_BUKRS), 1) ,
          Dim_PostingKeyId = ifnull((SELECT Dim_PostingKeyId
                                        FROM Dim_PostingKey pk
                                        WHERE pk.PostingKey= BSID_BSCHL AND pk.SpecialGLIndicator = ifnull(BSID_UMSKZ,'Not Set')), 1) ,
          Dim_SpecialGLIndicatorId = ifnull((SELECT Dim_SpecialGLIndicatorId
                                                FROM Dim_SpecialGLIndicator sgl
                                                WHERE sgl.SpecialGLIndicator = BSID_UMSKZ AND sgl.AccountType =  'D'), 1) ,
          Dim_TargetSpecialGLIndicatorId = ifnull((SELECT Dim_SpecialGLIndicatorId
                                                    FROM Dim_SpecialGLIndicator sgl
                                                    WHERE sgl.SpecialGLIndicator = BSID_ZUMSK AND sgl.AccountType =  'D'), 1) ,
          dd_SalesDocNo = ifnull(BSID_VBEL2, 'Not Set'),
          Dim_SpecialGlTransactionTypeId = ifnull ((SELECT Dim_SpecialGlTransactionTypeId
                                                      FROM Dim_SpecialGlTransactionType sgt
                                                     WHERE sgt.SpecialGlTransactionTypeId = BSID_UMSKS) , 1)


WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR

UPDATE tmp_fact_accountsreceivable far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET

          dd_SalesItemNo = BSID_POSN2,
          dd_SalesScheduleNo = BSID_ETEN2,
          dd_CashDiscountPercentage1 = BSID_ZBD1P,
          dd_BillingNo = ifnull(BSID_VBELN,'Not Set'),
          dd_CashDiscountPercentage2 = BSID_ZBD2P,
          dd_ProductionOrderNo = ifnull(BSID_AUFNR,'Not Set'),
          Dim_PaymentTermsId = ifnull ((SELECT Dim_CustomerPaymentTermsid
                                          FROM Dim_CustomerPaymentTerms pt
                                         WHERE pt.PaymentTermCode = BSID_ZTERM) , 1),
          dd_DunningLevel = BSID_MANST,
          Dim_PaymentMethodId = ifnull ((SELECT Dim_PaymentMethodId
                                          FROM Dim_PaymentMethod pm
                                          WHERE pm.PaymentMethod = BSID_ZLSCH AND pm.Country = dc.Country) , 1)


WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR


UPDATE tmp_fact_accountsreceivable far
FROM  dim_company dcm,
	  dim_customer dc,
	  BSID arc
SET
          Dim_RiskClassId = ifnull((select dr.Dim_RiskClassId
                                  from UKMBP_CMS a
                                        inner join BUT000 b on a.PARTNER = b.PARTNER and TIMESTAMP(LOCAL_TIMESTAMP) <= ifnull(a.RATING_VAL_DATE,TIMESTAMP(LOCAL_TIMESTAMP))
                                        inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
                                        inner join dim_riskclass dr on dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1
                                  where c.CUSTOMER = dc.CustomerNumber),1)
WHERE	 dcm.Dim_CompanyId = far.Dim_CompanyId
AND		 dc.Dim_CustomerId = far.Dim_CustomerId
AND		 far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR

 End of Update 1 - Split into 7 parts */


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'INSERT INTO tmp_fact_accountsreceivable(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';


/* Insert 1 */

INSERT INTO tmp_fact_accountsreceivable(amt_CashDiscountDocCurrency,
                                    amt_CashDiscountLocalCurrency,
                                    amt_InDocCurrency,
                                    amt_InLocalCurrency,
                                    amt_TaxInDocCurrency,
                                    amt_TaxInLocalCurrency,
                                    ct_CashDiscountDays1,
                                    dd_AccountingDocItemNo,
                                    dd_AccountingDocNo,
                                    dd_AssignmentNumber,
                                    Dim_ClearedFlagId,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdCreated,
                                    Dim_DateIdPosting,
                                    dd_debitcreditid,
                                    dd_ClearingDocumentNo,
                                    dd_FiscalPeriod,
                                    dd_FiscalYear,
                                    dd_FixedPaymentTerms,
                                    dd_InvoiceNumberTransBelongTo,
                                    dd_NetPaymentTermsPeriod,
                                    Dim_BlockingPaymentReasonId,
                                    Dim_BusinessAreaId,
                                    Dim_ChartOfAccountsId,
                                    Dim_CompanyId,
                                    Dim_CreditControlAreaId,
									/* Local currency */
                                    Dim_CurrencyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    Dim_DocumentTypeId,
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    Dim_SpecialGLIndicatorId,
                                    Dim_TargetSpecialGLIndicatorId,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    dd_BillingNo,
                                    dd_CashDiscountPercentage1,
                                    dd_CashDiscountPercentage2,
                                    dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    Dim_PaymentTermsId,
                                    dd_DunningLevel,
                                    Dim_PaymentMethodId,
                                    dim_RiskClassId,
                                    dd_CreditRep,
/* Begin 06 Nov 2013 */
                                    dd_CreditMgr,
/* End 06 Nov 2013 */
                                    Dim_DateidSONextDate,
				    fact_accountsreceivableid,
									/* Transaction and Global currencies */
									dim_Currencyid_TRA,
									dim_Currencyid_GBL,
									amt_exchangerate,
									amt_exchangerate_GBL,
									dim_salesdivisionid,
                  Dim_ChartOfAccountsIdARDocGL)
   SELECT (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WSKTO amt_CashDiscountDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_SKNTO amt_CashDiscountLocalCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WRBTR amt_InDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSID_SHKZG = 'H' THEN -1 ELSE 1 END)*BSID_MWSTS amt_TaxInLocalCurrency,
          BSID_ZBD1P ct_CashDiscountDays1,
          BSID_BUZEI dd_AccountingDocItemNo,
          BSID_BELNR dd_AccountingDocNo,
          ifnull(BSID_ZUONR,'Not Set') dd_AssignmentNumber,
          1 Dim_ClearedFlagId,
          1 Dim_DateIdAccDocDateEntered,
          1 Dim_DateIdBaseDateForDueDateCalc,
          1 Dim_DateIdCreated,
          1 Dim_DateIdPosting,
          (CASE WHEN BSID_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END) dd_debitcreditid,
          ifnull(BSID_AUGBL,'Not Set') dd_ClearingDocumentNo,
          ifnull(BSID_MONAT,0) dd_FiscalPeriod,
          ifnull(BSID_GJAHR,0) dd_FiscalYear,
          ifnull(BSID_ZBFIX,'Not Set') dd_FixedPaymentTerms,
          ifnull(BSID_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
          ifnull(BSID_ZBD3T,0) dd_NetPaymentTermsPeriod,
          1 Dim_BlockingPaymentReasonId,
          1 Dim_BusinessAreaId,
          1 Dim_ChartOfAccountsId,
          dc.Dim_CompanyId,
          1 Dim_CreditControlAreaId,
		/* dim_currencyid ( local currency ) is populated from dim_company */
          1 Dim_Currencyid,
          cm.Dim_CustomerId Dim_CustomerID,
          1 Dim_DateIdClearing,
          1 Dim_DocumentTypeId,
          1 Dim_PaymentReasonId,
          1 Dim_PostingKeyId,
          1 Dim_SpecialGLIndicatorId,
          1 Dim_TargetSpecialGLIndicatorId,
          ifnull(BSID_VBEL2, 'Not Set') dd_SalesDocNo,
          BSID_POSN2 dd_SalesItemNo,
          BSID_ETEN2 dd_SalesScheduleNo,
          ifnull(BSID_VBELN,'Not Set') dd_BillingNo,
          BSID_ZBD1P dd_CashDiscountPercentage1,
          BSID_ZBD2P dd_CashDiscountPercentage2,
          ifnull(BSID_AUFNR,'Not Set') dd_ProductionOrderNo,
          1 Dim_SpecialGlTransactionTypeId ,
          1 Dim_AccountsReceivableDocStatusId,
          1 Dim_PaymentTermsId,
          1 Dim_PaymentMethodId,
          BSID_MANST,
          0 dim_RiskClassId,
          'Not Set' dd_CreditRep,
/* Begin 06 Nov 2013 */									
           'Not Set' dd_CreditMgr,
/* End 06 Nov 2013 */				  
          1 Dim_DateidSONextDate,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_accountsreceivable') + row_number() over (order by ''),
		  /* dim_Currencyid_TRA ( local currency ) is populated from BSID_WAERS */
          1  dim_Currencyid_TRA,
          1 dim_Currencyid_GBL,
		  1 amt_exchangerate,	/*Get local rate from dmbtr and wrbtr columns*/
		  1 amt_exchangerate_GBL,  /*Get global rate from exchange rate script */
  		  1 dim_salesdivisionid,
	   1 AS Dim_ChartOfAccountsIdARDocGL
    FROM tmp_far_variable_holder,BSID arc
      INNER JOIN dim_company dc ON dc.CompanyCode = arc.BSID_BUKRS
      INNER JOIN dim_customer cm ON cm.CustomerNumber = arc.BSID_KUNNR
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM tmp_fact_accountsreceivable ar
                   WHERE     ifnull(ar.dd_AccountingDocNo,'xxx') = ifnull(arc.BSID_BELNR,'yyy')
                         AND ifnull(ar.dd_AccountingDocItemNo,-1) = ifnull(arc.BSID_BUZEI,-2)
                         AND ifnull(ar.dd_AssignmentNumber,'xxx') = ifnull(arc.BSID_ZUONR,'yyy')
                         AND ifnull(ar.dd_fiscalyear,-1) = ifnull(arc.BSID_GJAHR,-2)
                         AND ifnull(ar.dd_FiscalPeriod,-1)  = ifnull(arc.BSID_MONAT,-2)
                         AND ar.Dim_CompanyId = dc.Dim_CompanyId
                         AND ar.Dim_CustomerId = cm.Dim_CustomerId);


						 
						 
drop table if exists frcv_upd_1;
create table frcv_upd_1 as 
select distinct ar.fact_accountsreceivableid , BSID_REBZG,BSID_REBZJ,BSID_BUKRS,BSID_CPUDT,BSID_ZFBDT,BSID_BLDAT,BSID_BUDAT,BSID_ZLSPR,BSID_GSBER,BSID_SAKNR,BSID_KKBER,BSID_AUGDT
,BSID_BLART,BSID_RSTGR,BSID_BSCHL,BSID_UMSKZ,BSID_ZUMSK,BSID_UMSKS,BSID_BSTAT,BSID_ZTERM,BSID_ZLSCH,BSID_VBEL2,BSID_WAERS,BSID_XREF3,BSID_HKONT
from 
tmp_fact_accountsreceivable ar, BSID arc, dim_company dc,dim_customer cm
where ar.dd_AccountingDocNo = arc.BSID_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
AND ar.dd_fiscalyear = arc.BSID_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
AND dc.CompanyCode = arc.BSID_BUKRS
AND cm.CustomerNumber = arc.BSID_KUNNR 
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId    
;


/*
update tmp_fact_accountsreceivable
set Dim_ChartOfAccountsIdARDocGL = coa.Dim_ChartOfAccountsId 
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u, dim_company dc,
Dim_ChartOfAccounts coa
where 
dc.ChartOfAccounts = coa.CharOfAccounts AND dc.CompanyCode = BSID_BUKRS
AND coa.GLAccountNumber = BSID_HKONT
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and ar.Dim_ChartOfAccountsIdARDocGL <> coa.Dim_ChartOfAccountsId
*/

 merge into tmp_fact_accountsreceivable f
using(
select distinct ar.fact_accountsreceivableid, coa.Dim_ChartOfAccountsId
from  fact_accountsreceivable ar,frcv_upd_1 u, dim_company dc,
Dim_ChartOfAccounts coa
where 
dc.ChartOfAccounts = coa.CharOfAccounts AND dc.CompanyCode = BSID_BUKRS
AND coa.GLAccountNumber = BSID_HKONT
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid)t
on t.fact_accountsreceivableid = f.fact_accountsreceivableid
when matched then update 
set f.Dim_ChartOfAccountsIdARDocGL = t.Dim_ChartOfAccountsId
where f.Dim_ChartOfAccountsIdARDocGL <> t.Dim_ChartOfAccountsId;


update tmp_fact_accountsreceivable
set Dim_ClearedFlagId = ars.Dim_AccountReceivableStatusId 
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_AccountReceivableStatus ars
where 
ars.Status = CASE WHEN u.BSID_REBZG IS NOT NULL AND u.BSID_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and Dim_ClearedFlagId <> ars.Dim_AccountReceivableStatusId
;

update tmp_fact_accountsreceivable
set Dim_DateIdAccDocDateEntered = dt.dim_dateid  
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_date dt
where 
dt.DateValue = u.BSID_CPUDT AND dt.CompanyCode = u.BSID_BUKRS
and dt.plantcode_factory = 'Not Set'
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and Dim_DateIdAccDocDateEntered <> dt.dim_dateid 
;

update tmp_fact_accountsreceivable
set Dim_DateIdBaseDateForDueDateCalc = dt.dim_dateid 
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_date dt
where 
dt.DateValue = u.BSID_ZFBDT
              AND dt.CompanyCode = u.BSID_BUKRS    
	      and dt.plantcode_factory = 'Not Set'
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                 
and ar.Dim_DateIdBaseDateForDueDateCalc <> dt.dim_dateid
;


update tmp_fact_accountsreceivable
set Dim_DateIdCreated = dt.dim_dateid 
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_date dt
where 
dt.DateValue = BSID_BLDAT
             AND  dt.CompanyCode = u.BSID_BUKRS
	     and dt.plantcode_factory = 'Not Set'
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.Dim_DateIdCreated <> dt.dim_dateid
;


     
update tmp_fact_accountsreceivable
set Dim_DateIdPosting = dt.dim_dateid 
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_date dt
where 
dt.DateValue = BSID_BUDAT
              AND dt.CompanyCode = u.BSID_BUKRS
	      and dt.plantcode_factory = 'Not Set'
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_DateIdPosting <> dt.dim_dateid
;

 
update tmp_fact_accountsreceivable
set Dim_BlockingPaymentReasonId = bpr.Dim_BlockingPaymentReasonId 
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_BlockingPaymentReason bpr
where 
bpr.BlockingKeyPayment = u.BSID_ZLSPR
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId
;


update tmp_fact_accountsreceivable
set Dim_BusinessAreaId = ba.Dim_BusinessAreaId 
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_BusinessArea ba
where 
ba.BusinessArea = BSID_GSBER
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                       
and ar.Dim_BusinessAreaId <> ba.Dim_BusinessAreaId
;
		

/*ambiguous update using tmp*/
update tmp_fact_accountsreceivable
set Dim_ChartOfAccountsId = coa.Dim_ChartOfAccountsId 
from  tmp_fact_accountsreceivable ar, BSID arc,dim_company dc,dim_customer cm,
Dim_ChartOfAccounts coa
where 
dc.ChartOfAccounts = coa.CharOfAccounts
AND coa.GLAccountNumber = BSID_SAKNR
and ar.dd_AccountingDocNo = arc.BSID_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
AND ar.dd_fiscalyear = arc.BSID_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
AND dc.CompanyCode = arc.BSID_BUKRS
AND cm.CustomerNumber = arc.BSID_KUNNR 
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                        
and ar.Dim_ChartOfAccountsId <> coa.Dim_ChartOfAccountsId
;		

update tmp_fact_accountsreceivable
set dim_currencyid = c.dim_currencyid
from  tmp_fact_accountsreceivable ar, BSID arc,dim_company dc,dim_customer cm,
dim_currency c
where c.CurrencyCode = dc.Currency
and ar.dd_AccountingDocNo = arc.BSID_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
AND ar.dd_fiscalyear = arc.BSID_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
AND dc.CompanyCode = arc.BSID_BUKRS
AND cm.CustomerNumber = arc.BSID_KUNNR 
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                        
and ar.dim_currencyid <> c.dim_currencyid
;

/*ambiguous update using tmp end */

update tmp_fact_accountsreceivable
set Dim_CreditControlAreaId = cca.Dim_CreditControlAreaId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_creditcontrolarea cca
where cca.CreditControlAreaCode = u.BSID_KKBER
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                            
and ar.Dim_CreditControlAreaId <> cca.Dim_CreditControlAreaId
;


update tmp_fact_accountsreceivable
set Dim_DateIdClearing = dt.dim_dateid
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_date dt
where dt.DateValue = BSID_AUGDT
                  AND dt.CompanyCode = u.BSID_BUKRS
		  and dt.plantcode_factory = 'Not Set'
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_DateIdClearing <> dt.dim_dateid
;



update tmp_fact_accountsreceivable
set Dim_DocumentTypeId = dtt.dim_documenttypetextid
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_documenttypetext dtt
where  dtt.type = BSID_BLART
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_DocumentTypeId <> dtt.dim_documenttypetextid
;


update tmp_fact_accountsreceivable
set Dim_PaymentReasonId = pr.Dim_PaymentReasonId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_PaymentReason pr
where  pr.PaymentReasonCode = u.BSID_RSTGR
       AND pr.CompanyCode = u.BSID_BUKRS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                          
and ar.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId
;


update tmp_fact_accountsreceivable
set Dim_PostingKeyId = pk.Dim_PostingKeyId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_PostingKey pk
where  pk.PostingKey= BSID_BSCHL
             AND pk.SpecialGLIndicator = ifnull(BSID_UMSKZ,'Not Set')
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_PostingKeyId <> pk.Dim_PostingKeyId
;


update tmp_fact_accountsreceivable
set Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_SpecialGLIndicator sgl
where  sgl.SpecialGLIndicator = BSID_UMSKZ
              AND sgl.AccountType =  'D' 
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
;

update tmp_fact_accountsreceivable
set Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_SpecialGLIndicator sgl
where  sgl.SpecialGLIndicator = BSID_ZUMSK
              AND sgl.AccountType =  'D' 
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
;


update tmp_fact_accountsreceivable
set Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_SpecialGlTransactionType sgt
where  sgt.SpecialGlTransactionTypeId = BSID_UMSKS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId
;


update tmp_fact_accountsreceivable
set Dim_AccountsReceivableDocStatusId = ards.Dim_AccountsReceivableDocStatusId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_AccountsReceivableDocStatus ards
where  ards.Status = BSID_BSTAT
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_AccountsReceivableDocStatusId <> ards.Dim_AccountsReceivableDocStatusId
;


update tmp_fact_accountsreceivable
set Dim_PaymentTermsId = pt.Dim_CustomerPaymentTermsid
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
Dim_CustomerPaymentTerms pt
where  pt.PaymentTermCode = BSID_ZTERM
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_PaymentTermsId <> pt.Dim_CustomerPaymentTermsid
;


update tmp_fact_accountsreceivable
set Dim_PaymentMethodId = pm.Dim_PaymentMethodId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,dim_customer cm,
 Dim_PaymentMethod pm
where pm.PaymentMethod = BSID_ZLSCH
      AND pm.Country = cm.Country
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_PaymentMethodId <> pm.Dim_PaymentMethodId
;


update tmp_fact_accountsreceivable
set dim_RiskClassId = dr.Dim_RiskClassId
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,dim_customer cm,
 dim_riskclass dr,UKMBP_CMS a,BUT000 b,cvi_cust_link c
where  a.PARTNER = b.PARTNER and systimestamp <= ifnull(a.RATING_VAL_DATE,systimestamp)
       and c.PARTNER_GUID = b.PARTNER_GUID 
       and dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1
       and c.CUSTOMER = cm.CustomerNumber
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.dim_RiskClassId <> dr.Dim_RiskClassId
;


drop table if exists tmp_so_updt;
create table tmp_so_updt
as
select distinct so.dd_SalesDocNo,so.dim_dateidnextdate from fact_salesorder so;

update tmp_fact_accountsreceivable
set Dim_DateidSONextDate = so.dim_dateidnextdate
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
 tmp_so_updt so
where  so.dd_SalesDocNo = BSID_VBEL2
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                            
and ar.Dim_DateidSONextDate <> so.dim_dateidnextdate
;

drop table if exists tmp_so_updt;


update tmp_fact_accountsreceivable
set dim_Currencyid_TRA = c.dim_currencyid
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_currency c
where  c.CurrencyCode = u.BSID_WAERS
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                   
and ar.dim_Currencyid_TRA <> c.dim_currencyid
;



update tmp_fact_accountsreceivable
set dim_Currencyid_GBL = c.dim_currencyid
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_currency c,tmp_far_variable_holder 
where  c.CurrencyCode = tmp_far_variable_holder.pGlobalCurrency
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.dim_Currencyid_GBL <> c.dim_currencyid
;



update tmp_fact_accountsreceivable
set amt_exchangerate_GBL = z.exchangeRate
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
tmp_getExchangeRate1 z,tmp_far_variable_holder
where  z.pFromCurrency  = BSID_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact' 
	   and z.pToCurrency = tmp_far_variable_holder.pGlobalCurrency AND z.pFromExchangeRate = 0 AND z.pDate = systimestamp		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and amt_exchangerate_GBL <> z.exchangeRate
;



update tmp_fact_accountsreceivable
set dim_salesdivisionid = dsdi.dim_salesdivisionid
from  tmp_fact_accountsreceivable ar,frcv_upd_1 u,
dim_salesdivision dsdi 
where  dsdi.divisioncode = BSID_XREF3			  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                       
and ar.dim_salesdivisionid <> dsdi.dim_salesdivisionid
;

update tmp_fact_accountsreceivable
set amt_exchangerate = (arc.BSID_DMBTR/case when ifnull(arc.BSID_WRBTR,0) <> 0 then arc.BSID_WRBTR ELSE (CASE WHEN arc.BSID_DMBTR <> 0 THEN arc.BSID_DMBTR ELSE 1 END) END )
from  tmp_fact_accountsreceivable ar, BSID arc,dim_company dc,dim_customer cm
where  
 ar.dd_AccountingDocNo = arc.BSID_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND ar.dd_AssignmentNumber = arc.BSID_ZUONR
AND ar.dd_fiscalyear = arc.BSID_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSID_MONAT
AND dc.CompanyCode = arc.BSID_BUKRS
AND cm.CustomerNumber = arc.BSID_KUNNR 
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                      
and cast(ar.amt_exchangerate as decimal (19,6)) <> cast(arc.BSID_DMBTR/(case when ifnull(arc.BSID_WRBTR,0) <> 0 then arc.BSID_WRBTR ELSE (CASE WHEN arc.BSID_DMBTR <> 0 THEN arc.BSID_DMBTR ELSE 1 END) END ) as decimal (19,6));

drop table if exists frcv_upd_1;

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_accountsreceivableid),0) from tmp_fact_accountsreceivable)
where table_name = 'tmp_fact_accountsreceivable';
					 

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'INSERT INTO tmp_fact_accountsreceivable(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable farINNER JOIN BSAD arc ON far.dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable farINNER JOIN BSAD arc ON far.dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'INSERT INTO tmp_fact_accountsreceivable(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';


/* Insert 2 */

INSERT INTO tmp_fact_accountsreceivable(amt_CashDiscountDocCurrency,
                                    amt_CashDiscountLocalCurrency,
                                    amt_InDocCurrency,
                                    amt_InLocalCurrency,
                                    amt_TaxInDocCurrency,
                                    amt_TaxInLocalCurrency,
                                    ct_CashDiscountDays1,
                                    dd_AccountingDocItemNo,
                                    dd_AccountingDocNo,
                                    dd_AssignmentNumber,
                                    Dim_ClearedFlagId,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdCreated,
                                    Dim_DateIdPosting,
                                    dd_debitcreditid,
                                    dd_ClearingDocumentNo,
                                    dd_FiscalPeriod,
                                    dd_FiscalYear,
                                    dd_FixedPaymentTerms,
                                    dd_InvoiceNumberTransBelongTo,
                                    dd_NetPaymentTermsPeriod,
                                    Dim_BlockingPaymentReasonId,
                                    Dim_BusinessAreaId,
                                    Dim_ChartOfAccountsId,
                                    Dim_CompanyId,
                                    Dim_CreditControlAreaId,
									/* Local curr */
                                    Dim_CurrencyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    Dim_DocumentTypeId,
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    Dim_SpecialGLIndicatorId,
                                    Dim_TargetSpecialGLIndicatorId,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    dd_BillingNo,
                                    dd_CashDiscountPercentage1,
                                    dd_CashDiscountPercentage2,
                                    dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    Dim_PaymentTermsId,
                                    dd_DunningLevel,
                                    Dim_PaymentMethodId,
                                    dim_RiskClassId,
                                    dd_CreditRep,
/* Begin 06 Nov 2013 */									
									dd_CreditMgr,
/* End 06 Nov 2013 */				  									
                                    Dim_DateidSONextDate,fact_accountsreceivableid,
										/* Transaction and Global currencies */
									dim_Currencyid_TRA,
									dim_Currencyid_GBL,
									amt_exchangerate,
									amt_exchangerate_GBL,
									dim_salesdivisionid,
									Dim_ChartOfAccountsIdARDocGL)
   SELECT (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WSKTO amt_CashDiscountDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_SKNTO amt_CashDiscountLocalCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WRBTR amt_InDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_DMBTR amt_InLocalCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_WMWST amt_TaxInDocCurrency,
          (CASE WHEN BSAD_SHKZG = 'H' THEN -1 ELSE 1 END)*BSAD_MWSTS amt_TaxInLocalCurrency,
       
        BSAD_ZBD1P ct_CashDiscountDays1,
          BSAD_BUZEI dd_AccountingDocItemNo,
          BSAD_BELNR dd_AccountingDocNo,
          ifnull(BSAD_ZUONR, 'Not Set') dd_AssignmentNumber,
          1 Dim_ClearedFlagId,
          1 Dim_DateIdAccDocDateEntered,
          1 Dim_DateIdBaseDateForDueDateCalc,
          1 Dim_DateIdCreated,
          1 Dim_DateIdPosting,
          (CASE WHEN BSAD_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)  dd_debitcreditid,
          ifnull(BSAD_AUGBL,'Not Set') dd_ClearingDocumentNo,
          BSAD_MONAT dd_FiscalPeriod,
          BSAD_GJAHR dd_FiscalYear,
          ifnull(BSAD_ZBFIX,'Not Set') dd_FixedPaymentTerms,
          ifnull(BSAD_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
          BSAD_ZBD3T dd_NetPaymentTermsPeriod,
          1 Dim_BlockingPaymentReasonId,
          1 Dim_BusinessAreaId,
          1 Dim_ChartOfAccountsId,
          dc.Dim_CompanyId,
          1 Dim_CreditControlAreaId,
          1 Dim_Currencyid,
          cm.Dim_CustomerId Dim_CustomerID,
          1 Dim_DateIdClearing,
          1 Dim_DocumentTypeId,
          1 Dim_PaymentReasonId,
          1 Dim_PostingKeyId,
          1 Dim_SpecialGLIndicatorId,
          1 Dim_TargetSpecialGLIndicatorId,
          ifnull(BSAD_VBEL2, 'Not Set') dd_SalesDocNo,
          BSAD_POSN2 dd_SalesItemNo,
          BSAD_ETEN2 dd_SalesScheduleNo,
          ifnull(BSAD_VBELN,'Not Set') dd_BillingNo,
          BSAD_ZBD1P dd_CashDiscountPercentage1,
          BSAD_ZBD2P dd_CashDiscountPercentage2,
          ifnull(BSAD_AUFNR,'Not Set') dd_ProductionOrderNo,
          1 Dim_SpecialGlTransactionTypeId,
          1 Dim_AccountsReceivableDocStatusId,
          1 Dim_PaymentTermsId,
          BSAD_MANST,
          1 Dim_PaymentMethodId,
          0 dim_RiskClassId,
          'Not Set' dd_CreditRep,
/* Begin 06 Nov 2013 */									
           'Not Set' dd_CreditMgr,
/* End 06 Nov 2013 */					  
          1 Dim_DateidSONextDate,
		  (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_accountsreceivable') + row_number() over (order by ''),
		  		  /* dim_Currencyid_TRA ( local currency ) is populated from BSAD_WAERS */
          1 dim_Currencyid_TRA,
          1 dim_Currencyid_GBL,
		  1 amt_exchangerate,			
		  1 amt_exchangerate_GBL,
		  1 dim_salesdivisionid,
		  1 Dim_ChartOfAccountsIdARDocGL	 
    FROM tmp_far_variable_holder,BSAD arc
      INNER JOIN dim_company dc ON dc.CompanyCode = BSAD_BUKRS
      INNER JOIN dim_customer cm ON cm.CustomerNumber = BSAD_KUNNR
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM tmp_fact_accountsreceivable ar
                   WHERE     ifnull(ar.dd_AccountingDocNo,'xxx') = ifnull(arc.BSAD_BELNR,'yyy')
                         AND ifnull(ar.dd_AccountingDocItemNo,-1) = ifnull(arc.BSAD_BUZEI,-2)
                         AND ifnull(ar.dd_AssignmentNumber,'xxx') = ifnull(arc.BSAD_ZUONR,'yyy')
                         AND ifnull(ar.dd_fiscalyear,-1) = ifnull(arc.BSAD_GJAHR,-2)
                         AND ifnull(ar.dd_FiscalPeriod,-1)  = ifnull(arc.BSAD_MONAT,-2)
                         AND ar.Dim_CompanyId = dc.Dim_CompanyId
                         AND ar.Dim_CustomerId = cm.Dim_CustomerId);

drop table if exists frcv_upd_2;
create table frcv_upd_2 as 
select distinct ar.fact_accountsreceivableid , BSAD_REBZG,BSAD_REBZJ,BSAD_CPUDT,BSAD_BUKRS,BSAD_ZFBDT,BSAD_BLDAT,BSAD_BUDAT,BSAD_ZLSPR,BSAD_GSBER,
BSAD_SAKNR,BSAD_KKBER,BSAD_AUGDT,BSAD_BLART,BSAD_RSTGR,BSAD_BSCHL,BSAD_UMSKZ,BSAD_ZUMSK,BSAD_UMSKS,BSAD_BSTAT,BSAD_ZTERM,BSAD_ZLSCH,BSAD_VBEL2,BSAD_WAERS,BSAD_DMBTR,BSAD_WRBTR,BSAD_XREF3,BSAD_HKONT
from 
tmp_fact_accountsreceivable ar, BSAD arc, dim_company dc,dim_customer cm
where  dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId    
;

update tmp_fact_accountsreceivable
set Dim_ChartOfAccountsIdARDocGL = coa.Dim_ChartOfAccountsId 
from  tmp_fact_accountsreceivable ar,frcv_upd_2 u, dim_company dc,
Dim_ChartOfAccounts coa
where 
dc.ChartOfAccounts = coa.CharOfAccounts AND dc.CompanyCode = BSAD_BUKRS
AND coa.GLAccountNumber = BSAD_HKONT
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and ar.Dim_ChartOfAccountsIdARDocGL <> coa.Dim_ChartOfAccountsId
;

update tmp_fact_accountsreceivable
set dim_salesdivisionid = dsdi.dim_salesdivisionid 
from  tmp_fact_accountsreceivable ar,frcv_upd_2 u, dim_salesdivision dsdi
where 
dsdi.divisioncode = BSAD_XREF3
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and ar.dim_salesdivisionid <> dsdi.dim_salesdivisionid
;
  

update tmp_fact_accountsreceivable
set Dim_ClearedFlagId = ars.Dim_AccountReceivableStatusId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_AccountReceivableStatus ars
where  ars.Status = CASE WHEN BSAD_REBZG IS NOT NULL AND BSAD_REBZJ <> 0 THEN 'F - Partial' ELSE 'C - Cleared' END	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and ar.Dim_ClearedFlagId <> ars.Dim_AccountReceivableStatusId
;

update tmp_fact_accountsreceivable
set Dim_DateIdAccDocDateEntered = dt.dim_dateid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
dim_date dt
where  dt.DateValue = BSAD_CPUDT
                  AND dt.CompanyCode = BSAD_BUKRS	 
and dt.plantcode_factory = 'Not Set'				  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                  
and ar.Dim_DateIdAccDocDateEntered <> dt.dim_dateid
;

update tmp_fact_accountsreceivable
set Dim_DateIdBaseDateForDueDateCalc = dt.dim_dateid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
dim_date dt
where  dt.DateValue = BSAD_ZFBDT
                  AND dt.CompanyCode = BSAD_BUKRS	
and dt.plantcode_factory = 'Not Set'				  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_DateIdBaseDateForDueDateCalc <> dt.dim_dateid
;


update tmp_fact_accountsreceivable
set Dim_DateIdCreated = dt.dim_dateid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
dim_date dt
where dt.DateValue = BSAD_BLDAT
                  AND dt.CompanyCode = BSAD_BUKRS	
and dt.plantcode_factory = 'Not Set'				  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid      
and ar.Dim_DateIdCreated <> dt.dim_dateid
;



update tmp_fact_accountsreceivable
set Dim_DateIdPosting = dt.dim_dateid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
dim_date dt
where dt.DateValue = BSAD_BUDAT
                  AND dt.CompanyCode = BSAD_BUKRS		
and dt.plantcode_factory = 'Not Set'				  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_DateIdPosting <> dt.dim_dateid
;



update tmp_fact_accountsreceivable
set Dim_BlockingPaymentReasonId = bpr.Dim_BlockingPaymentReasonId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_BlockingPaymentReason bpr
where bpr.BlockingKeyPayment = BSAD_ZLSPR		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                        
and ar.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId
;

update tmp_fact_accountsreceivable
set Dim_BusinessAreaId = ba.Dim_BusinessAreaId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_BusinessArea ba
where  ba.BusinessArea = BSAD_GSBER		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                         
and ar.Dim_BusinessAreaId <>  ba.Dim_BusinessAreaId
;


update tmp_fact_accountsreceivable
set Dim_ChartOfAccountsId = coa.Dim_ChartOfAccountsId
from  tmp_fact_accountsreceivable ar, BSAD arc,dim_company dc,dim_customer cm,
Dim_ChartOfAccounts coa
where  dc.ChartOfAccounts = coa.CharOfAccounts
        AND coa.GLAccountNumber = BSAD_SAKNR		  
and dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                    
and ar.Dim_ChartOfAccountsId <> coa.Dim_ChartOfAccountsId
;


update tmp_fact_accountsreceivable
set Dim_CreditControlAreaId = cca.Dim_CreditControlAreaId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
dim_creditcontrolarea cca
where  cca.CreditControlAreaCode = BSAD_KKBER	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                     
and ar.Dim_CreditControlAreaId <> cca.Dim_CreditControlAreaId
;


update tmp_fact_accountsreceivable
set Dim_Currencyid = c.Dim_Currencyid
from  tmp_fact_accountsreceivable ar, BSAD arc,dim_company dc,dim_customer cm,
dim_currency c
where  c.CurrencyCode = dc.Currency	  
and dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId                    
and ar.Dim_Currencyid <> c.Dim_Currencyid
;


update tmp_fact_accountsreceivable
set Dim_DateIdClearing = dt.dim_dateid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
dim_date dt
where  dt.DateValue = BSAD_AUGDT
AND dt.CompanyCode = BSAD_BUKRS		  
and dt.plantcode_factory = 'Not Set'
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_DateIdClearing <> dt.dim_dateid
;


update tmp_fact_accountsreceivable
set Dim_DocumentTypeId = dtt.dim_documenttypetextid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
dim_documenttypetext dtt
where  dtt.type = BSAD_BLART		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_DocumentTypeId <> dtt.dim_documenttypetextid
;


update tmp_fact_accountsreceivable
set Dim_PaymentReasonId = pr.Dim_PaymentReasonId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_PaymentReason pr
where  pr.PaymentReasonCode = BSAD_RSTGR
               AND pr.CompanyCode = BSAD_BUKRS		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                 
and ar.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId
;


update tmp_fact_accountsreceivable
set Dim_PostingKeyId = pk.Dim_PostingKeyId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_PostingKey pk
where  pk.PostingKey= BSAD_BSCHL
            AND pk.SpecialGLIndicator = ifnull(BSAD_UMSKZ,'Not Set')		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                   
and ar.Dim_PostingKeyId <> pk.Dim_PostingKeyId
;


update tmp_fact_accountsreceivable
set Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_SpecialGLIndicator sgl
where sgl.SpecialGLIndicator = BSAD_UMSKZ
              AND sgl.AccountType =  'D'	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                    
and ar.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
;



update tmp_fact_accountsreceivable
set Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_SpecialGLIndicator sgl
where sgl.SpecialGLIndicator = BSAD_ZUMSK
              AND sgl.AccountType =  'D'		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                    
and ar.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId
;



update tmp_fact_accountsreceivable
set Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_SpecialGlTransactionType sgt
where sgt.SpecialGlTransactionTypeId = BSAD_UMSKS		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                      
and ar.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId
;


update tmp_fact_accountsreceivable
set Dim_AccountsReceivableDocStatusId = ards.Dim_AccountsReceivableDocStatusId
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_AccountsReceivableDocStatus ards
where ards.Status = BSAD_BSTAT		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid                     
and ar.Dim_AccountsReceivableDocStatusId <> ards.Dim_AccountsReceivableDocStatusId
;



update tmp_fact_accountsreceivable
set Dim_PaymentTermsId = pt.Dim_CustomerPaymentTermsid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
Dim_CustomerPaymentTerms pt
where pt.PaymentTermCode = BSAD_ZTERM		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid     
and ar.Dim_PaymentTermsId <> pt.Dim_CustomerPaymentTermsid
;



update tmp_fact_accountsreceivable
set Dim_PaymentMethodId = pm.Dim_PaymentMethodId
from  tmp_fact_accountsreceivable ar, BSAD arc,dim_company dc,dim_customer cm,
Dim_PaymentMethod pm
where  pm.PaymentMethod = BSAD_ZLSCH
              AND pm.Country = cm.Country		  
and dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId 
and ar.Dim_PaymentMethodId <> pm.Dim_PaymentMethodId
;



update tmp_fact_accountsreceivable
set dim_RiskClassId = dr.Dim_RiskClassId
from  tmp_fact_accountsreceivable ar, BSAD arc,dim_company dc,dim_customer cm,
dim_riskclass dr,UKMBP_CMS a,BUT000 b,cvi_cust_link c 
where  a.PARTNER = b.PARTNER and systimestamp <= ifnull(a.RATING_VAL_DATE,systimestamp) and
	   c.PARTNER_GUID = b.PARTNER_GUID  and
	   dr.riskclass = a.RISK_CLASS and dr.rowiscurrent = 1 and
	   c.CUSTOMER = cm.CustomerNumber	  
and dc.CompanyCode = BSAD_BUKRS
and cm.CustomerNumber = BSAD_KUNNR
and ar.dd_AccountingDocNo = arc.BSAD_BELNR
AND ar.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND ar.dd_AssignmentNumber = arc.BSAD_ZUONR
AND ar.dd_fiscalyear = arc.BSAD_GJAHR
AND ar.dd_FiscalPeriod  = arc.BSAD_MONAT
AND ar.Dim_CompanyId = dc.Dim_CompanyId
AND ar.Dim_CustomerId = cm.Dim_CustomerId 
and ar.dim_RiskClassId <> dr.Dim_RiskClassId
;


drop table if exists tmp_so_upd2;
create table tmp_so_upd2 as 
select distinct so.dd_SalesDocNo, so.dim_dateidnextdate from fact_salesorder so
;


update tmp_fact_accountsreceivable
set Dim_DateidSONextDate = so.dim_dateidnextdate
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
tmp_so_upd2 so
where  so.dd_SalesDocNo = BSAD_VBEL2		  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.Dim_DateidSONextDate <> so.dim_dateidnextdate
;
drop table if exists tmp_so_upd2;




update tmp_fact_accountsreceivable
set dim_Currencyid_TRA = c.dim_currencyid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u ,
dim_currency c
where  c.CurrencyCode = u.BSAD_WAERS	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.dim_Currencyid_TRA <> c.dim_currencyid
;



update tmp_fact_accountsreceivable
set dim_Currencyid_GBL = c.dim_currencyid
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u,
dim_currency c,tmp_far_variable_holder
where  c.CurrencyCode = tmp_far_variable_holder.pGlobalCurrency	  
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.dim_Currencyid_GBL <> c.dim_currencyid
;


update tmp_fact_accountsreceivable
set amt_exchangerate = (BSAD_DMBTR/case when ifnull(BSAD_WRBTR,0) <> 0 then BSAD_WRBTR ELSE (CASE WHEN BSAD_DMBTR <> 0 THEN BSAD_DMBTR ELSE 1 END) END )
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u
where  ar.fact_accountsreceivableid = u.fact_accountsreceivableid
and cast(ar.amt_exchangerate as decimal (19,6)) <> cast((BSAD_DMBTR/case when ifnull(BSAD_WRBTR,0) <> 0 then BSAD_WRBTR ELSE (CASE WHEN BSAD_DMBTR <> 0 THEN BSAD_DMBTR ELSE 1 END) END ) as  decimal (19,6))
;


update tmp_fact_accountsreceivable
set amt_exchangerate_GBL = z.exchangeRate
from  tmp_fact_accountsreceivable ar, frcv_upd_2 u,
tmp_getExchangeRate1 z, tmp_far_variable_holder
where z.pFromCurrency  = BSAD_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
and  z.pToCurrency = pGlobalCurrency AND z.pFromExchangeRate = 0 AND z.pDate = systimestamp
and ar.fact_accountsreceivableid = u.fact_accountsreceivableid  
and ar.amt_exchangerate_GBL <> z.exchangeRate
;


drop table if exists frcv_upd_2;
						 
						 
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_accountsreceivableid),0) from tmp_fact_accountsreceivable)
where table_name = 'tmp_fact_accountsreceivable';
					 
		 
					 
INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'INSERT INTO tmp_fact_accountsreceivable(amt_CashDiscountDocCurrency',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
				 
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable INNER JOIN BSID arc ON dd_AccountingDocNo = arc.BSID_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 

/* START OF Update 3*/

/* Update 3 could be broken up like this if required. But that will increase the size of code by a lot. But might be required if it causes performance problems */

/*
DROP TABLE IF EXISTS TMP_BSID_accountsreceivable
CREATE TABLE TMP_BSID_accountsreceivable
AS
SELECT b.*,(CASE WHEN BSID_ZFBDT IS NULL THEN BSID_BLDAT ELSE BSID_ZFBDT END ) as datevalue_upd,'N' as update_flag
FROM BSID b

UPDATE TMP_BSID_accountsreceivable
SET 	update_flag = 'Y'
WHERE  BSID_REBZG IS NULL AND BSID_SHKZG = 'H'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD3T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD3T IS NOT NULL AND BSID_ZBD3T <> 0
AND update_flag = 'N'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD2T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD2T IS NOT NULL AND BSID_ZBD2T <> 0
AND update_flag = 'N'

UPDATE TMP_BSID_accountsreceivable
SET   datevalue_upd = datevalue_upd + cast(BSID_ZBD1T,integer),
	  update_flag = 'Y'
WHERE  BSID_ZBD1T IS NOT NULL AND BSID_ZBD1T <> 0
AND update_flag = 'N'

*/

/* Update 3 - Part 1*/

UPDATE tmp_fact_accountsreceivable far
SET 
far.Dim_NetDueDateId = ifnull(DIM_DATEID,1)
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc, 
     tmp_fact_accountsreceivable far ,
     DIM_DATE dt
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR
AND dt.CompanyCode = arc.BSID_BUKRS
and dt.plantcode_factory = 'Not Set'
and far.Dim_NetDueDateId <> ifnull(DIM_DATEID,1)
and dt.DateValue = ( CASE WHEN BSID_ZFBDT IS NULL THEN BSID_BLDAT
                                             ELSE BSID_ZFBDT END) + 
												  CAST((
												  CASE WHEN (BSID_REBZG IS NULL AND BSID_SHKZG = 'H') THEN 0 
												  ELSE ( CASE WHEN BSID_ZBD3T IS NOT NULL AND BSID_ZBD3T <> 0 THEN BSID_ZBD3T
														WHEN BSID_ZBD2T IS NOT NULL AND BSID_ZBD2T <> 0 THEN BSID_ZBD2T
														WHEN BSID_ZBD1T IS NOT NULL AND BSID_ZBD1T <> 0 THEN BSID_ZBD1T  ELSE 0 END) END) as integer)
;

UPDATE tmp_fact_accountsreceivable far
SET 
far.Dim_NetDueDateWrtCashDiscountTerms1 = 
				CASE WHEN (BSID_REBZG IS NULL AND (BSID_SHKZG = 'H'))  
						THEN Dim_NetDueDateId
				ELSE Dim_NetDueDateId  END,
far.Dim_NetDueDateWrtCashDiscountTerms2 = 
				CASE WHEN (BSID_REBZG IS NULL AND (BSID_SHKZG = 'H')) 
						THEN Dim_NetDueDateId
                ELSE Dim_NetDueDateId  END	
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc, 
	 tmp_fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR
;	 
	 


/* Update 3 - Part 2*/

UPDATE tmp_fact_accountsreceivable far
SET far.Dim_NetDueDateWrtCashDiscountTerms1 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 dim_date dt,
	 tmp_fact_accountsreceivable far	
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR	
AND BSID_ZBD1T IS NOT NULL AND dt.DateValue = CAST((BSID_ZFBDT + ifnull(CAST(BSID_ZBD1T as INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSID_BUKRS and dt.plantcode_factory = 'Not Set'
and far.Dim_NetDueDateWrtCashDiscountTerms1 <> dt.DIM_DATEID
; 


	 
/* Update 3 - Part 3*/	 
	 
UPDATE tmp_fact_accountsreceivable far
SET far.Dim_NetDueDateWrtCashDiscountTerms2 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 dim_date dt,
     tmp_fact_accountsreceivable far	 
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = BSID_BUKRS
AND dc.CustomerNumber = BSID_KUNNR	
/* Join to update Dim_NetDueDateWrtCashDiscountTerms2 not updated in part 1 */
AND BSID_ZBD2T IS NOT NULL AND dt.DateValue = CAST((BSID_ZFBDT + ifnull(CAST(BSID_ZBD2T as INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSID_BUKRS and dt.plantcode_factory = 'Not Set';		 



INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable INNER JOIN BSID arc ON dd_AccountingDocNo = arc.BSID_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 

/* END OF Update 3 */

/* START OF Update 4 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable INNER JOIN BSAD arc ON dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

/* Update 4 - Part 1*/
/* Fixed data exception - datetime field overflow issue - OSTOIAN - March 9 */
/*
UPDATE tmp_fact_accountsreceivable far
SET 
far.Dim_NetDueDateId = ifnull(dt.DIM_DATEID,1)
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 tmp_fact_accountsreceivable far,
	 DIM_DATE dt 
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
and dt.DateValue = ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
                                             ELSE BSAD_ZFBDT END) + 
												  CAST((
												  CASE WHEN (BSAD_REBZG IS NULL AND BSAD_SHKZG = 'H') THEN 0 
												  ELSE ( CASE WHEN BSAD_ZBD3T IS NOT NULL AND BSAD_ZBD3T <> 0 THEN BSAD_ZBD3T
														WHEN BSAD_ZBD2T IS NOT NULL AND BSAD_ZBD2T <> 0 THEN BSAD_ZBD2T
														WHEN BSAD_ZBD1T IS NOT NULL AND BSAD_ZBD1T <> 0 THEN BSAD_ZBD1T  ELSE 0 END) END) as integer)
AND dt.CompanyCode = arc.BSAD_BUKRS 
and dt.plantcode_factory = 'Not Set'
and far.Dim_NetDueDateId <> ifnull(dt.DIM_DATEID,1)
*/

UPDATE tmp_fact_accountsreceivable far
SET
far.Dim_NetDueDateId = ifnull(dt.DIM_DATEID,1)
FROM dim_company dcm,
dim_customer dc,
BSAD arc,
tmp_fact_accountsreceivable far,
DIM_DATE dt
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
and dt.DateValue = case when ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END)='9999-12-31' then ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END) else ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END) +
CAST((
CASE WHEN (BSAD_REBZG IS NULL AND BSAD_SHKZG = 'H') THEN 0
ELSE ( CASE WHEN BSAD_ZBD3T IS NOT NULL AND BSAD_ZBD3T <> 0 THEN BSAD_ZBD3T
WHEN BSAD_ZBD2T IS NOT NULL AND BSAD_ZBD2T <> 0 THEN BSAD_ZBD2T
WHEN BSAD_ZBD1T IS NOT NULL AND BSAD_ZBD1T <> 0 THEN BSAD_ZBD1T ELSE 0 END) END) as integer) end
AND dt.CompanyCode = arc.BSAD_BUKRS
and dt.plantcode_factory = 'Not Set'
and far.Dim_NetDueDateId <> ifnull(dt.DIM_DATEID,1); 

/* End of changes - OSTOIAN - March 9 */

UPDATE tmp_fact_accountsreceivable far
SET 
far.Dim_NetDueDateWrtCashDiscountTerms1 = 
				CASE WHEN (BSAD_REBZG IS NULL AND (BSAD_SHKZG = 'H'))  
						THEN Dim_NetDueDateId
				ELSE Dim_NetDueDateId  END,
far.Dim_NetDueDateWrtCashDiscountTerms2 = 
				CASE WHEN (BSAD_REBZG IS NULL AND (BSAD_SHKZG = 'H')) 
						THEN Dim_NetDueDateId
                ELSE Dim_NetDueDateId  END	
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 tmp_fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR 
;	 


/* Update 4 - Part 2*/

/*
UPDATE tmp_fact_accountsreceivable far
SET far.Dim_NetDueDateWrtCashDiscountTerms1 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 dim_date dt,
	 tmp_fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
*//* Join to update Dim_NetDueDateWrtCashDiscountTerms1 not updated in part 1 *//*
AND BSAD_ZBD1T IS NOT NULL AND dt.DateValue = CAST((BSAD_ZFBDT + ifnull(CAST(BSAD_ZBD1T as INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSAD_BUKRS 	 and dt.plantcode_factory = 'Not Set'
and  far.Dim_NetDueDateWrtCashDiscountTerms1 <> dt.DIM_DATEID
*/

UPDATE tmp_fact_accountsreceivable far
SET far.Dim_NetDueDateWrtCashDiscountTerms1 = dt.DIM_DATEID
FROM dim_company dcm,
dim_customer dc,
BSAD arc,
dim_date dt,
tmp_fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
/* Join to update Dim_NetDueDateWrtCashDiscountTerms1 not updated in part 1 */
AND BSAD_ZBD1T IS NOT NULL AND dt.DateValue = case when ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END)='9999-12-31' then ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END) else ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END) +
CAST((
CASE WHEN (BSAD_REBZG IS NULL AND BSAD_SHKZG = 'H') THEN 0
ELSE ( CASE WHEN BSAD_ZBD3T IS NOT NULL AND BSAD_ZBD3T <> 0 THEN BSAD_ZBD3T
WHEN BSAD_ZBD2T IS NOT NULL AND BSAD_ZBD2T <> 0 THEN BSAD_ZBD2T
WHEN BSAD_ZBD1T IS NOT NULL AND BSAD_ZBD1T <> 0 THEN BSAD_ZBD1T ELSE 0 END) END) as integer) end
 AND dt.CompanyCode = arc.BSAD_BUKRS
 and dt.plantcode_factory = 'Not Set'
and far.Dim_NetDueDateWrtCashDiscountTerms1 <> dt.DIM_DATEID;

/* End of changes - OSTOIAN - March 9 */
	 
/* Update 4 - Part 3*/	 
/*	 
UPDATE tmp_fact_accountsreceivable far	 
SET far.Dim_NetDueDateWrtCashDiscountTerms2 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 dim_date dt,
	 tmp_fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
*//* Join to update Dim_NetDueDateWrtCashDiscountTerms2 not updated in part 1 *//*
AND BSAD_ZBD2T IS NOT NULL AND dt.DateValue = CAST((BSAD_ZFBDT + ifnull(CAST(BSAD_ZBD2T as INTEGER),0) ) AS DATE) AND dt.CompanyCode = arc.BSAD_BUKRS 	and dt.plantcode_factory = 'Not Set'	 
and far.Dim_NetDueDateWrtCashDiscountTerms2 <> dt.DIM_DATEID
*/

UPDATE tmp_fact_accountsreceivable far	 
SET far.Dim_NetDueDateWrtCashDiscountTerms2 = dt.DIM_DATEID
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc,
	 dim_date dt,
	 tmp_fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND   far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
/* Join to update Dim_NetDueDateWrtCashDiscountTerms2 not updated in part 1 */
AND BSAD_ZBD2T IS NOT NULL AND dt.DateValue = case when ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END)='9999-12-31' then ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END) else ( CASE WHEN BSAD_ZFBDT IS NULL THEN BSAD_BLDAT
ELSE BSAD_ZFBDT END) +
CAST((
CASE WHEN (BSAD_REBZG IS NULL AND BSAD_SHKZG = 'H') THEN 0
ELSE ( CASE WHEN BSAD_ZBD3T IS NOT NULL AND BSAD_ZBD3T <> 0 THEN BSAD_ZBD3T
WHEN BSAD_ZBD2T IS NOT NULL AND BSAD_ZBD2T <> 0 THEN BSAD_ZBD2T
WHEN BSAD_ZBD1T IS NOT NULL AND BSAD_ZBD1T <> 0 THEN BSAD_ZBD1T ELSE 0 END) END) as integer) end
 AND dt.CompanyCode = arc.BSAD_BUKRS
and dt.plantcode_factory = 'Not Set'	 
and far.Dim_NetDueDateWrtCashDiscountTerms2 <> dt.DIM_DATEID;


/* End of changes - OSTOIAN - March 9 */


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable INNER JOIN BSAD arc ON dd_AccountingDocNo = arc.BSAD_BELNR',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		 




/* START OF Update 5 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far INNER JOIN  VBRK_VBRP',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE tmp_fact_accountsreceivable far
SET amt_ExchangeRate_GBL = z.exchangeRate
FROM dim_currency tra,tmp_getExchangeRate1 z, tmp_far_variable_holder, tmp_fact_accountsreceivable far
WHERE far.dim_currencyid_tra = tra.dim_currencyid
AND z.pFromCurrency = tra.currencycode
AND z.pToCurrency = pGlobalCurrency
AND z.pFromExchangeRate = 0
AND z.pDate = systimestamp
AND z.fact_script_name = 'bi_populate_accountsreceivable_fact'
AND far.amt_ExchangeRate_GBL <> z.exchangeRate;

UPDATE tmp_fact_accountsreceivable far
SET amt_ExchangeRate = ifnull((BSID_DMBTR/case when ifnull(BSID_WRBTR,0) <> 0 then BSID_WRBTR ELSE (CASE WHEN BSID_DMBTR <> 0 THEN BSID_DMBTR ELSE 1 END) END ),1)
FROM BSID arc, dim_customer dc,dim_company dcm,tmp_fact_accountsreceivable far
WHERE far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND far.dd_billingno = arc.BSID_VBELN
AND dcm.dim_companyid = far.dim_companyid AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.Dim_CustomerId = far.Dim_CustomerId AND dc.CustomerNumber = arc.BSID_KUNNR;


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far INNER JOIN  VBRK_VBRP',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

/* END OF Update 5 */  
  
/* START OF Update 6 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far INNER JOIN  VBRK_VBRP part2',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE tmp_fact_accountsreceivable far
SET amt_ExchangeRate_GBL = z.exchangeRate
FROM dim_currency tra,tmp_getExchangeRate1 z, tmp_far_variable_holder, tmp_fact_accountsreceivable far
WHERE far.dim_currencyid_tra = tra.dim_currencyid
AND z.pFromCurrency = tra.currencycode
AND z.pToCurrency = pGlobalCurrency
AND z.pFromExchangeRate = 0
AND z.pDate = systimestamp
AND z.fact_script_name = 'bi_populate_accountsreceivable_fact'
AND far.amt_ExchangeRate_GBL <> z.exchangeRate;

UPDATE tmp_fact_accountsreceivable far
SET amt_ExchangeRate = ifnull((BSAD_DMBTR/case when ifnull(BSAD_WRBTR,0) <> 0 then BSAD_WRBTR ELSE (CASE WHEN BSAD_DMBTR <> 0 THEN BSAD_DMBTR ELSE 1 END) END ),1)
FROM BSAD arc, dim_customer dc,dim_company dcm, tmp_fact_accountsreceivable far
WHERE far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND far.dd_billingno = arc.BSAD_VBELN
AND dcm.dim_companyid = far.dim_companyid AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.Dim_CustomerId = far.Dim_CustomerId AND dc.CustomerNumber = arc.BSAD_KUNNR;
  

Drop table if exists tmp_distinct_forupd;
/*End Changes 02 Jul 2015*/
/* END OF Update 6 */

/*Updating the sums*/
Drop table if exists tmp_distinct_forupd;
create table tmp_distinct_forupd as
select distinct
ifnull((SELECT SUM(f_bill.ct_BillingQtySalesUOM) FROM fact_billing f_bill where far.dd_BillingNo = f_bill.dd_billing_no),0) ct_bill_QtySalesUOM,
ifnull((SELECT SUM(f_bill.ct_BillingQtyStockUOM) FROM fact_billing f_bill where far.dd_BillingNo = f_bill.dd_billing_no),0) ct_bill_QtyStockUOM,
fb.dd_billing_no
from
fact_billing fb,
tmp_fact_accountsreceivable far
WHERE fb.dd_billing_no = far.dd_billingno;

UPDATE tmp_fact_accountsreceivable far
SET far.ct_bill_QtySalesUOM =fb.ct_bill_QtySalesUOM ,
 far.ct_bill_QtyStockUOM =fb.ct_bill_QtyStockUOM
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_distinct_forupd fb, tmp_fact_accountsreceivable far
WHERE fb.dd_billing_no = far.dd_billingno;

/*Updating partid and taking the first one since below partid is updating after accountingdocno*/
Drop table if exists tmp_distinct_forupd;
create table tmp_distinct_forupd as select distinct  fb.Dim_PartId, fb.dd_billing_no, row_number() over (partition by fb.dd_billing_no order by '') as rn
from
fact_billing fb,
tmp_fact_accountsreceivable far
WHERE fb.dd_billing_no = far.dd_billingno;

UPDATE tmp_fact_accountsreceivable far
set far.Dim_PartId = ifnull(fb.Dim_PartId,1)
FROM tmp_distinct_forupd fb, tmp_fact_accountsreceivable far
WHERE fb.dd_billing_no = far.dd_billingno
AND far.Dim_PartId <> ifnull(fb.Dim_PartId,1)
and rn=1;

/*Updating plantid and taking the first one and adding also an update for plantid  after accountingdocno same as it were for partid*/

Drop table if exists tmp_distinct_forupd;
create table tmp_distinct_forupd as select distinct fb.dim_plantid,
fb.dd_billing_no,
 row_number() over (partition by fb.dd_billing_no order by '') as rn
from
fact_billing fb,
tmp_fact_accountsreceivable far
WHERE fb.dd_billing_no = far.dd_billingno;

UPDATE tmp_fact_accountsreceivable far
set far.Dim_PlantId = ifnull(fb.dim_plantid,1)
FROM tmp_distinct_forupd fb, tmp_fact_accountsreceivable far
WHERE fb.dd_billing_no = far.dd_billingno
AND far.Dim_PlantId <> ifnull(fb.dim_plantid,1)
and rn = 1;

/*customerponumber*/
Drop table if exists tmp_distinct_forupd;
create table tmp_distinct_forupd as
select distinct fb.dd_CustomerPONumber,
fb.dd_billing_no
from
fact_billing fb,
tmp_fact_accountsreceivable far
WHERE fb.dd_billing_no = far.dd_billingno
AND far.dd_CustomerPONumber <> fb.dd_CustomerPONumber;

UPDATE tmp_fact_accountsreceivable far
SET
 far.dd_CustomerPONumber = fb.dd_CustomerPONumber
 -----far.dd_salesdocno = fb.dd_salesdocno
FROM tmp_distinct_forupd fb, tmp_fact_accountsreceivable far
WHERE fb.dd_billing_no = far.dd_billingno;

/*Updating salesdocno taking the first value*/

/*End Changes 02 Jul 2015*/

/* Update 8 */
INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far INNER JOIN  VBRK_VBRP part2',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

/* Update 8 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far INNER JOIN BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';

drop table if exists rcv_so_upd;

create table rcv_so_upd as 
select distinct so.dim_dateidnextdate, so.dd_SalesDocNo from fact_salesorder so 
;

UPDATE tmp_fact_accountsreceivable far
SET	 amt_EligibleForDiscount = BSID_SKFBT, 
     far.Dim_DateidSONextDate = ifnull(so.dim_dateidnextdate , 1)
FROM dim_company dcm,
	 dim_customer dc,
	 BSID arc,
	 rcv_so_upd so,tmp_fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSID_BELNR
AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
AND far.dd_AssignmentNumber = arc.BSID_ZUONR
AND far.dd_fiscalyear = arc.BSID_GJAHR
AND far.dd_FiscalPeriod  = arc.BSID_MONAT
AND dcm.CompanyCode = arc.BSID_BUKRS
AND dc.CustomerNumber = arc.BSID_KUNNR
AND so.dd_SalesDocNo = far.dd_salesdocno 
;
	 
drop table if exists rcv_so_upd;


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far INNER JOIN BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';

/* Update 9 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far INNER JOIN BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	

UPDATE tmp_fact_accountsreceivable far
SET	 far.Dim_DateidSONextDate =so.dim_dateidnextdate
     
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc, tmp_fact_accountsreceivable far, fact_salesorder so
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
AND so.dd_SalesDocNo = far.dd_salesdocno
AND far.Dim_DateidSONextDate <> so.dim_dateidnextdate;


UPDATE tmp_fact_accountsreceivable far
SET	 amt_EligibleForDiscount = BSAD_SKFBT /* * amt_ExchangeRate,*/
     
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_company dcm,
	 dim_customer dc,
	 BSAD arc, tmp_fact_accountsreceivable far
WHERE dcm.Dim_CompanyId = far.Dim_CompanyId
AND   dc.Dim_CustomerId = far.Dim_CustomerId
AND far.dd_AccountingDocNo = arc.BSAD_BELNR
AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
AND far.dd_fiscalyear = arc.BSAD_GJAHR
AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
AND dcm.CompanyCode = arc.BSAD_BUKRS
AND dc.CustomerNumber = arc.BSAD_KUNNR
AND amt_EligibleForDiscount <> BSAD_SKFBT;


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far INNER JOIN BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';


/* Update 10 */

INSERT INTO processinglog (referencename, startdate, description,processinglogid)
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far, Dim_ARMiscellaneous armisc, BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';

merge into tmp_fact_accountsreceivable far
using (select distinct far.fact_accountsreceivableid, armisc.Dim_ARMiscellaneousId
 FROM tmp_fact_accountsreceivable far,Dim_ARMiscellaneous armisc,
 BSID arc
 WHERE far.dd_AccountingDocNo = arc.BSID_BELNR
 AND far.dd_AccountingDocItemNo = arc.BSID_BUZEI
 AND far.dd_AssignmentNumber = arc.BSID_ZUONR
 AND far.dd_fiscalyear = arc.BSID_GJAHR
 AND far.dd_FiscalPeriod = arc.BSID_MONAT
 AND armisc.CustomerItemsClearingReversed = ifnull(arc.BSID_XRAGL,'Not Set')
 AND armisc.CustomerItemsDocumentPostedYet = ifnull(arc.BSID_XNETB,'Not Set')
 AND armisc.NegativePosting = ifnull(arc.BSID_XNEGP,'Not Set')) t on t.fact_accountsreceivableid=far.fact_accountsreceivableid
when matched then update set 
far.Dim_ARMiscellaneousId = t.Dim_ARMiscellaneousId
 ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where far.Dim_ARMiscellaneousId <> t.Dim_ARMiscellaneousId;


INSERT INTO processinglog (referencename, enddate, description,processinglogid)
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far, Dim_ARMiscellaneous armisc, BSID arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';
/* Update 11 */

/*Georgiana Changes 02 Jul 2015*/

/*UPDATE tmp_fact_accountsreceivable far
FROM   Dim_ARMiscellaneous armisc,
       BSAD arc
SET far.Dim_ARMiscellaneousId = armisc.Dim_ARMiscellaneousId
	,dw_update_date = current_timestamp
 WHERE       far.dd_AccountingDocNo = arc.BSAD_BELNR
       AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
       AND far.dd_fiscalyear = arc.BSAD_GJAHR
       AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
       AND armisc.CustomerItemsClearingReversed = ifnull(arc.BSAD_XRAGL,'Not Set')
       AND armisc.CustomerItemsDocumentPostedYet = ifnull(arc.BSAD_XNETB,'Not Set')
       AND armisc.NegativePosting = ifnull(arc.BSAD_XNEGP,'Not Set')*/

Drop table if exists tmp_distinct_forupd;
Create table tmp_distinct_forupd as select distinct
    armisc.Dim_ARMiscellaneousId, arc.BSAD_BELNR, arc.BSAD_BUZEI, arc.BSAD_ZUONR, arc.BSAD_GJAHR, arc.BSAD_MONAT, BSAD_XRAGL, BSAD_XNETB, BSAD_XNEGP,
    row_number() over(partition by arc.BSAD_BELNR, arc.BSAD_BUZEI, arc.BSAD_ZUONR, arc.BSAD_GJAHR, arc.BSAD_MONAT order by '') as rn
FROM  Dim_ARMiscellaneous armisc,
BSAD arc,
tmp_fact_accountsreceivable far
WHERE far.dd_AccountingDocNo = arc.BSAD_BELNR
     AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
     AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
     AND far.dd_fiscalyear = arc.BSAD_GJAHR
     AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
     AND armisc.CustomerItemsClearingReversed = ifnull(arc.BSAD_XRAGL,'Not Set')
     AND armisc.CustomerItemsDocumentPostedYet = ifnull(arc.BSAD_XNETB,'Not Set')
    AND armisc.NegativePosting = ifnull(arc.BSAD_XNEGP,'Not Set')
	AND far.Dim_ARMiscellaneousId <> armisc.Dim_ARMiscellaneousId;

UPDATE tmp_fact_accountsreceivable far
SET far.Dim_ARMiscellaneousId = armisc.Dim_ARMiscellaneousId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM   tmp_fact_accountsreceivable far,Dim_ARMiscellaneous armisc,
       tmp_distinct_forupd arc
WHERE       far.dd_AccountingDocNo = arc.BSAD_BELNR
       AND far.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND far.dd_AssignmentNumber = arc.BSAD_ZUONR
       AND far.dd_fiscalyear = arc.BSAD_GJAHR
       AND far.dd_FiscalPeriod  = arc.BSAD_MONAT
       AND armisc.CustomerItemsClearingReversed = ifnull(arc.BSAD_XRAGL,'Not Set')
       AND armisc.CustomerItemsDocumentPostedYet = ifnull(arc.BSAD_XNETB,'Not Set')
       AND armisc.NegativePosting = ifnull(arc.BSAD_XNEGP,'Not Set')
       and arc.rn=1;


Drop table if exists tmp_distinct_forupd;
/*End Changes 02 Jul 2015*/

/* Update 12 */

UPDATE    tmp_fact_accountsreceivable far
   SET far.dd_ARAge =
          (CASE
              WHEN current_date - ndt.DateValue BETWEEN 0 AND 30
              THEN
                 '1 - 30'
              WHEN current_date - ndt.DateValue BETWEEN 31 AND 60
              THEN
                 '31 - 60'
              WHEN current_date - ndt.DateValue BETWEEN 61 AND 90
              THEN
                 '61 - 90'
              WHEN current_date - ndt.DateValue > 90
              THEN
                 ' > 90 '
              WHEN current_date - ndt.DateValue < 0
              THEN
                 'Future'
              ELSE
                 'Not Set'
           END)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  dim_date ndt, tmp_fact_accountsreceivable far
 WHERE far.dim_clearedflagid IN (2, 4)
 AND far.Dim_NetDueDateId = ndt.dim_dateid;


merge into tmp_fact_accountsreceivable ar 
using (select distinct ar.fact_accountsreceivableid,sod.Dim_DateIdActualGI_Original
FROM fact_billing b, fact_salesorderdelivery sod, tmp_fact_accountsreceivable ar 
WHERE ar.dd_accountingdocno = b.dd_billing_no 
AND b.dd_Salesdlvrdocno = sod.dd_salesdlvrdocno
 AND b.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo 
AND ar.dd_InvoiceNumberTransBelongTo = 'Not Set') t
on t.fact_accountsreceivableid=ar.fact_accountsreceivableid
when matched then update set ar.Dim_DateIdActualGIDate = t.Dim_DateIdActualGI_Original 
where ar.Dim_DateIdActualGIDate <> t.Dim_DateIdActualGI_Original;


update tmp_fact_accountsreceivable ar
set ar.Dim_DateIdActualGIDate = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where ar.Dim_DateIdActualGIDate IS NULL;


merge into tmp_fact_accountsreceivable ar 
using (select distinct ar.fact_accountsreceivableid, first_value(b.dim_partid) over (partition by  ar.fact_accountsreceivableid order by  ar.fact_accountsreceivableid) as dim_partid
FROM fact_billing b, tmp_fact_accountsreceivable ar
WHERE ar.dd_accountingdocno = b.dd_billing_no
AND ar.dim_partid = 1
AND  ar.Dim_partid <> b.dim_partid) t
on ar.fact_accountsreceivableid=t.fact_accountsreceivableid
when matched then update set ar.Dim_partid = t.dim_partid
where ar.Dim_partid <> t.dim_partid;

/*Georgiana Changes 02 Jul 2015- adding this because in Update 7 we are updating for plantid the first value*/
merge into tmp_fact_accountsreceivable ar 
using (select distinct ar.fact_accountsreceivableid,  b.dim_plantid
FROM fact_billing b, tmp_fact_accountsreceivable ar
WHERE ar.dd_accountingdocno = b.dd_billing_no
AND ar.dim_plantid = 1
AND  ar.Dim_plantid <> b.dim_plantid) t
on ar.fact_accountsreceivableid=t.fact_accountsreceivableid
when matched then update set ar.Dim_plantid = t.dim_plantid
where ar.Dim_plantid <> t.dim_plantid;
/*End Changes 02 Jul 2015*/


merge into tmp_fact_accountsreceivable ar 
using (select distinct ar.fact_accountsreceivableid,  b.dim_Salesorgid
FROM fact_billing b, tmp_fact_accountsreceivable ar
WHERE ar.dd_accountingdocno = b.dd_billing_no
AND ar.dim_Salesorgid = 1
AND  ar.dim_Salesorgid <> b.dim_Salesorgid) t
on ar.fact_accountsreceivableid=t.fact_accountsreceivableid
when matched then update set ar.Dim_Salesorgid = t.dim_Salesorgid
where ar.Dim_Salesorgid <> t.dim_Salesorgid;

DROP TABLE IF EXISTS tmp_CustomerCreditLimit;

CREATE TABLE tmp_CustomerCreditLimit as
select distinct c.CUSTOMER,ifnull(a.CREDIT_LIMIT,0) as Credit_Limit,a.LIMIT_VALID_DATE,RANK() OVER (PARTITION BY c.CUSTOMER ORDER BY a.LIMIT_VALID_DATE ASC) 
  AS Rank from UKMBP_CMS_SGM a 
  inner join BUT000 b on a.PARTNER = b.PARTNER and systimestamp <= a.LIMIT_VALID_DATE
  inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID;

DELETE FROM tmp_CustomerCreditLimit WHERE Rank <> 1;

UPDATE tmp_fact_accountsreceivable far
SET     dd_CreditLimit = t.Credit_Limit
FROM    dim_customer dc,
        tmp_CustomerCreditLimit  t,tmp_fact_accountsreceivable far
WHERE	 dc.Dim_CustomerId = far.Dim_CustomerId
AND dc.CustomerNumber = t.customer;

UPDATE tmp_fact_accountsreceivable far
SET dd_CreditLimit = 0
WHERE dd_CreditLimit IS NULL;
                                  
DROP TABLE IF EXISTS tmp_CustomerCreditLimit; 





/* Begin 06 Nov 2013 */
DROP TABLE IF EXISTS tmp_upd_702;

Create table tmp_upd_702 as Select  
(case when b.NAME_FIRST is null then b.NAME_LAST else b.NAME_FIRST || ' ' || ifnull(b.NAME_LAST,'') end) updcol1,d.BUT050_PARTNER2 updVBAK_KUNNR
from cvi_cust_link c inner join but000 b1 on c.PARTNER_GUID = b1.PARTNER_GUID
	inner join BUT050 d on b1.PARTNER = d.BUT050_PARTNER2 AND d.BUT050_RELTYP = 'UKMSB0'
	inner join but000 b on b.PARTNER = d.BUT050_PARTNER1
where  sysdate between to_date(d.BUT050_DATE_FROM) and to_date(d.BUT050_DATE_TO)
and (b.NAME_FIRST is not null or b.NAME_LAST is not null and b.BU_GROUP = 'CRED')
order by c.customer ;

update tmp_fact_accountsreceivable so
Set dd_CreditRep = t.updcol1
from   tmp_upd_702 t,
       Dim_Customer c,
       tmp_fact_accountsreceivable so
Where so.dim_Customerid = c.dim_customerid
  and t.updVBAK_KUNNR = c.CustomerNumber;

DROP TABLE IF EXISTS tmp_upd_702;

/* End 06 Nov 2013 */


INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'UPDATE tmp_fact_accountsreceivable far, Dim_ARMiscellaneous armisc, BSAD arc',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		
 

/* Update 13 */

/* Need to break up this query, as the original update with just changes for concat results in Rewrite Error in VW */

/* This is required otherwise the tmp_dim.. creation query fails with error Invalid qualifier 'f' .. */
drop table if exists tmp_fact_accountsreceivable1;
create table tmp_fact_accountsreceivable1
as
select f.*,cast(f.dd_AccountingDocNo as varchar(100)) as char_dd_AccountingDocNo,
          cast(f.dd_fiscalyear as varchar(100)) as char_dd_fiscalyear,
        lpad(rtrim(cast(f.dd_AccountingDocItemNo as varchar(100))),3,'0') char_dd_AccountingDocItemNo, 
        cast(dcm.CompanyCode as varchar(100)) as char_CompanyCode
FROM tmp_fact_accountsreceivable f,dim_company dcm
WHERE   dcm.Dim_CompanyId = f.Dim_CompanyId;




DROP TABLE IF EXISTS tmp_dim_company_fact_accountsreceivable;
CREATE TABLE tmp_dim_company_fact_accountsreceivable
AS
SELECT f.*,
	   (f.char_CompanyCode || f.char_dd_AccountingDocNo || f.char_dd_fiscalyear || f.char_dd_AccountingDocItemNo) as FDM_DCPROC_OBJ_KEY
FROM 	 tmp_fact_accountsreceivable1 f;

/* Added explicit commits as they were not working earlier with wrapper script */

drop table if exists tmp_scmg_t_case_attr;
create table tmp_scmg_t_case_attr
as
select a.ext_key,b.fdm_dcproc_obj_key
from scmg_t_case_attr a inner join fdm_dcproc b on a.case_guid = b.fdm_dcproc_case_guid_loc
where b.fdm_dcproc_obj_type = 'BSEG' and b.fdm_dcproc_new_class = 'DISP_RES' ;

update tmp_dim_company_fact_accountsreceivable f 
  set dd_disputecaseid = ifnull(a.ext_key,'Not Set')
                                from tmp_dim_company_fact_accountsreceivable f, tmp_scmg_t_case_attr a 
where a.fdm_dcproc_obj_key = f.fdm_dcproc_obj_key
and dd_disputecaseid <> ifnull(a.ext_key,'Not Set')
;


/* Update 14 */			

DROP TABLE IF EXISTS tmp_SCMG_T_CASE_ATTR2;
CREATE TABLE tmp_SCMG_T_CASE_ATTR2
AS
SELECT a.EXT_KEY,b.FDM_DCPROC_OBJ_KEY
from SCMG_T_CASE_ATTR a inner join FDM_DCPROC b on a.CASE_GUID = b.FDM_DCPROC_CASE_GUID_LOC
where b.FDM_DCPROC_OBJ_TYPE = 'BSEG' and b.FDM_DCPROC_NEW_CLASS in ('DISP_INV','DISP_PAY');


UPDATE tmp_dim_company_fact_accountsreceivable f 
  SET dd_DisputeCaseId = ifnull( a.EXT_KEY,'Not Set')
    from tmp_dim_company_fact_accountsreceivable f , tmp_SCMG_T_CASE_ATTR2 a 
WHERE dd_DisputeCaseId = 'Not Set'
  and a.FDM_DCPROC_OBJ_KEY = f.FDM_DCPROC_OBJ_KEY;


/* Andra changes 14 Jan 2014: Add column dd_ReasonCode*/

drop table if exists tmp_scmg_t_case_attr3;
create table tmp_scmg_t_case_attr3 as
select a.reason_code,b.fdm_dcproc_obj_key
from scmg_t_case_attr a 
inner join fdm_dcproc b 
on a.case_guid = b.fdm_dcproc_case_guid_loc;


update tmp_dim_company_fact_accountsreceivable f 
  set dd_reasoncode = ifnull(a.reason_code,'Not Set')
from tmp_dim_company_fact_accountsreceivable f, tmp_scmg_t_case_attr3 a 
where  a.fdm_dcproc_obj_key = f.fdm_dcproc_obj_key
and  dd_reasoncode <> ifnull(a.reason_code,'Not Set');

drop table if exists tmp_scmg_t_case_attr3;

/* END Andra changes 14 Jan 2014 */			

DROP TABLE IF EXISTS tmp_d;
CREATE TABLE tmp_d
AS
select a.EXT_KEY, b.FDM_DCPROC_OBJ_KEY,
                          SUM(FDM_DCPROC_DELTA_CREDITED) SUM_FDM_DCPROC_DELTA_CREDITED,
                          SUM(FDM_DCPROC_DELTA_DISPUTED) SUM_FDM_DCPROC_DELTA_DISPUTED,
                          SUM(FDM_DCPROC_DELTA_NOT_SOLVED) SUM_FDM_DCPROC_DELTA_NOT_SOLVED,
                          SUM(FDM_DCPROC_DELTA_ORIGINAL) SUM_FDM_DCPROC_DELTA_ORIGINAL,
                          SUM(FDM_DCPROC_DELTA_PAID) SUM_FDM_DCPROC_DELTA_PAID,
                          SUM(FDM_DCPROC_DELTA_WRITE_OFF) SUM_FDM_DCPROC_DELTA_WRITE_OFF
                  from SCMG_T_CASE_ATTR a inner join FDM_DCPROC b on a.CASE_GUID = b.FDM_DCPROC_CASE_GUID_LOC
                  where b.FDM_DCPROC_OBJ_TYPE = 'BSEG' and b.FDM_DCPROC_NEW_CLASS in ('DISP_INV','DISP_PAY','DISP_RES')
                  group by a.EXT_KEY, b.FDM_DCPROC_OBJ_KEY;

/*DROP TABLE tmp_dim_company_fact_accountsreceivable*/
DROP TABLE if exists tmp_fact_accountsreceivable1;
DROP TABLE if exists tmp_SCMG_T_CASE_ATTR2;
DROP TABLE if exists tmp_SCMG_T_CASE_ATTR;

 
/* Update 15 - Final Update */	

UPDATE tmp_dim_company_fact_accountsreceivable f 
SET amt_OriginalAmtDisputed = SUM_FDM_DCPROC_DELTA_ORIGINAL,
    amt_DisputedAmt = SUM_FDM_DCPROC_DELTA_DISPUTED,
    amt_DisputeAmtPaid = SUM_FDM_DCPROC_DELTA_PAID,
    amt_DisputeAmtCredited = SUM_FDM_DCPROC_DELTA_CREDITED,
    amt_DisputeAmtCleared = SUM_FDM_DCPROC_DELTA_WRITE_OFF,
    amt_DisputeAmtWrittenOff = SUM_FDM_DCPROC_DELTA_NOT_SOLVED
FROM tmp_d d, tmp_dim_company_fact_accountsreceivable f 
WHERE f.FDM_DCPROC_OBJ_KEY = d.FDM_DCPROC_OBJ_KEY
AND EXT_KEY = f.dd_DisputeCaseId;

								
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_AccountingDocNo RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_fiscalyear RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_dd_AccountingDocItemNo RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column char_CompanyCode RESTRICT;
ALTER TABLE tmp_dim_company_fact_accountsreceivable
DROP column FDM_DCPROC_OBJ_KEY RESTRICT;


truncate table fact_accountsreceivable
;

insert into fact_accountsreceivable
(
FACT_ACCOUNTSRECEIVABLEID,
DIM_COMPANYID,
DIM_CUSTOMERID,
DIM_SPECIALGLINDICATORID,
DIM_DATEIDCLEARING,
DD_CLEARINGDOCUMENTNO,
DD_ASSIGNMENTNUMBER,
DD_FISCALYEAR,
DD_ACCOUNTINGDOCNO,
DD_ACCOUNTINGDOCITEMNO,
DIM_DATEIDPOSTING,
DIM_DATEIDCREATED,
DIM_BUSINESSAREAID,
DD_DEBITCREDITID,
DIM_CREDITCONTROLAREAID,
DIM_DOCUMENTTYPEID,
DD_FISCALPERIOD,
DIM_CHARTOFACCOUNTSID,
DIM_CURRENCYID,
DIM_BLOCKINGPAYMENTREASONID,
DIM_PAYMENTREASONID,
DD_FIXEDPAYMENTTERMS,
DD_NETPAYMENTTERMSPERIOD,
DIM_POSTINGKEYID,
DIM_TARGETSPECIALGLINDICATORID,
AMT_CASHDISCOUNTLOCALCURRENCY,
AMT_CASHDISCOUNTDOCCURRENCY,
AMT_TAXINLOCALCURRENCY,
AMT_TAXINDOCCURRENCY,
AMT_INLOCALCURRENCY,
AMT_INDOCCURRENCY,
DD_INVOICENUMBERTRANSBELONGTO,
DIM_DATEIDBASEDATEFORDUEDATECALC,
DIM_DATEIDACCDOCDATEENTERED,
DIM_CLEAREDFLAGID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DD_SALESDOCNO,
DD_SALESITEMNO,
DD_SALESSCHEDULENO,
DIM_NETDUEDATEID,
DIM_NETDUEDATEWRTCASHDISCOUNTTERMS1,
DIM_NETDUEDATEWRTCASHDISCOUNTTERMS2,
DD_BILLINGNO,
DD_PRODUCTIONORDERNO,
DD_CASHDISCOUNTPERCENTAGE1,
DD_CASHDISCOUNTPERCENTAGE2,
DIM_SPECIALGLTRANSACTIONTYPEID,
AMT_ELIGIBLEFORDISCOUNT,
DIM_ACCOUNTSRECEIVABLEDOCSTATUSID,
DD_RELEVANTINVOICEFISCALYEAR,
DIM_ARMISCELLANEOUSID,
CT_BILL_QTYSALESUOM,
CT_BILL_QTYSTOCKUOM,
DIM_PARTID,
DIM_PLANTID,
DIM_PAYMENTMETHODID,
DIM_PAYMENTTERMSID,
DD_DUNNINGLEVEL,
CT_CASHDISCOUNTDAYS1,
DD_CUSTOMERPONUMBER,
DD_CREDITREP,
DD_CREDITLIMIT,
DIM_RISKCLASSID,
DIM_DATEIDSONEXTDATE,
DD_DISPUTECASEID,
AMT_ORIGINALAMTDISPUTED,
AMT_DISPUTEDAMT,
AMT_DISPUTEAMTPAID,
AMT_DISPUTEAMTCREDITED,
AMT_DISPUTEAMTCLEARED,
AMT_DISPUTEAMTWRITTENOFF,
DIM_SALESORGID,
DIM_DISTRIBUTIONCHANNELID,
DIM_ACCOUNTASSIGNMENTGROUPID,
DIM_SALESDOCUMENTTYPEID,
AMT_SALESORDERNETVALUE,
AMT_SALESORDERTAXAMT,
AMT_SALESORDEROPENAMTWTAX,
DIM_DATEIDSOCREATED,
DIM_DATEIDSOSCHEDDLVRREQ,
DIM_DATEIDSOSCHEDDLVR,
DD_ARAGE,
DIM_DATEIDACTUALGIDATE,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DD_CREDITMGR,
DIM_CHARTOFACCOUNTSIDARDOCGL,
DD_REASONCODE,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DIM_PROJECTSOURCEID,
DIM_SALESDIVISIONID

)
(
select distinct 
FACT_ACCOUNTSRECEIVABLEID,
DIM_COMPANYID,
DIM_CUSTOMERID,
DIM_SPECIALGLINDICATORID,
DIM_DATEIDCLEARING,
DD_CLEARINGDOCUMENTNO,
DD_ASSIGNMENTNUMBER,
DD_FISCALYEAR,
DD_ACCOUNTINGDOCNO,
DD_ACCOUNTINGDOCITEMNO,
DIM_DATEIDPOSTING,
DIM_DATEIDCREATED,
DIM_BUSINESSAREAID,
DD_DEBITCREDITID,
DIM_CREDITCONTROLAREAID,
DIM_DOCUMENTTYPEID,
DD_FISCALPERIOD,
DIM_CHARTOFACCOUNTSID,
DIM_CURRENCYID,
DIM_BLOCKINGPAYMENTREASONID,
DIM_PAYMENTREASONID,
DD_FIXEDPAYMENTTERMS,
DD_NETPAYMENTTERMSPERIOD,
DIM_POSTINGKEYID,
DIM_TARGETSPECIALGLINDICATORID,
AMT_CASHDISCOUNTLOCALCURRENCY,
AMT_CASHDISCOUNTDOCCURRENCY,
AMT_TAXINLOCALCURRENCY,
AMT_TAXINDOCCURRENCY,
AMT_INLOCALCURRENCY,
AMT_INDOCCURRENCY,
DD_INVOICENUMBERTRANSBELONGTO,
DIM_DATEIDBASEDATEFORDUEDATECALC,
DIM_DATEIDACCDOCDATEENTERED,
DIM_CLEAREDFLAGID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DD_SALESDOCNO,
DD_SALESITEMNO,
DD_SALESSCHEDULENO,
ifnull(DIM_NETDUEDATEID,1),
ifnull(DIM_NETDUEDATEWRTCASHDISCOUNTTERMS1,1),
ifnull(DIM_NETDUEDATEWRTCASHDISCOUNTTERMS2,1),
DD_BILLINGNO,
DD_PRODUCTIONORDERNO,
DD_CASHDISCOUNTPERCENTAGE1,
DD_CASHDISCOUNTPERCENTAGE2,
DIM_SPECIALGLTRANSACTIONTYPEID,
ifnull(AMT_ELIGIBLEFORDISCOUNT,0),
DIM_ACCOUNTSRECEIVABLEDOCSTATUSID,
ifnull(DD_RELEVANTINVOICEFISCALYEAR, 0),
ifnull(DIM_ARMISCELLANEOUSID,1),
ifnull(CT_BILL_QTYSALESUOM,0),
ifnull(CT_BILL_QTYSTOCKUOM,0),
ifnull(DIM_PARTID,1),
ifnull(DIM_PLANTID,1),
ifnull(DIM_PAYMENTMETHODID,1),
ifnull(DIM_PAYMENTTERMSID,1),
DD_DUNNINGLEVEL,
CT_CASHDISCOUNTDAYS1,
ifnull(DD_CUSTOMERPONUMBER, 'Not Set'),
DD_CREDITREP,
DD_CREDITLIMIT,
ifnull(DIM_RISKCLASSID,1),
ifnull(DIM_DATEIDSONEXTDATE,1),
ifnull(DD_DISPUTECASEID,1),
ifnull(AMT_ORIGINALAMTDISPUTED,0),
ifnull(AMT_DISPUTEDAMT,0),
ifnull(AMT_DISPUTEAMTPAID,0),
ifnull(AMT_DISPUTEAMTCREDITED,0),
ifnull(AMT_DISPUTEAMTCLEARED,0),
ifnull(AMT_DISPUTEAMTWRITTENOFF,0),
ifnull(DIM_SALESORGID,1),
ifnull(DIM_DISTRIBUTIONCHANNELID,1),
ifnull(DIM_ACCOUNTASSIGNMENTGROUPID,1),
ifnull(DIM_SALESDOCUMENTTYPEID,1),
ifnull(AMT_SALESORDERNETVALUE,0),
ifnull(AMT_SALESORDERTAXAMT,0),
ifnull(AMT_SALESORDEROPENAMTWTAX,0),
ifnull(DIM_DATEIDSOCREATED,1),
ifnull(DIM_DATEIDSOSCHEDDLVRREQ,1),
ifnull(DIM_DATEIDSOSCHEDDLVR,1),
ifnull(DD_ARAGE,'Not Set'),
ifnull(DIM_DATEIDACTUALGIDATE,1),
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DD_CREDITMGR,
ifnull(DIM_CHARTOFACCOUNTSIDARDOCGL,1),
ifnull(DD_REASONCODE, 'Not Set'),
current_timestamp,
ifnull(DW_UPDATE_DATE,current_timestamp),
ifnull(DIM_PROJECTSOURCEID,1),
ifnull(DIM_SALESDIVISIONID,1)
from tmp_dim_company_fact_accountsreceivable
);



/* Exchange rates : Posting dates are used for both global and local rate */
/* Populate the exchange rates for BSID data */
/* Local exchange rate */
DROP TABLE IF EXISTS TMP_UPDT_BSID;
CREATE TABLE TMP_UPDT_BSID
AS
SELECT BSID_BELNR,BSID_BUZEI,BSID_ZUONR,BSID_GJAHR,BSID_MONAT,BSID_BUKRS,BSID_WAERS,BSID_BUDAT, ROW_NUMBER() OVER(PARTITION BY BSID_BELNR,BSID_BUZEI,BSID_ZUONR,BSID_GJAHR,BSID_MONAT ORDER BY BSID_BUDAT desc) rono
FROM BSID;


UPDATE fact_accountsreceivable fap
 SET amt_ExchangeRate =  z.exchangeRate
 from TMP_UPDT_BSID arc, dim_Company dcm, tmp_getexchangerate1 z,fact_accountsreceivable fap
 WHERE     fap.dd_AccountingDocNo = arc.BSID_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSID_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSID_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSID_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSID_MONAT
       AND dcm.CompanyCode = arc.BSID_BUKRS AND dcm.RowIsCurrent = 1
        AND z.pFromCurrency  = BSID_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
        AND z.pToCurrency = dcm.currency AND z.pDate = BSID_BUDAT
        AND arc.rono = 1
        AND fap.amt_ExchangeRate <> z.exchangeRate;

DROP TABLE IF EXISTS TMP_UPDT_BSID;

/* Global rate */
   drop table if exists tmp_getexchangerate1_upd2;
    create table tmp_getexchangerate1_upd2 as    
    /* Georgiana Chnages 30 Dec 2016 BI-5031 Exchange Rate fix select distinct  pFromCurrency, fact_script_name, pToCurrency, pDate, exchangeRate,  row_number() over (order by '') rownr*/
	    select distinct  pFromCurrency, fact_script_name, pToCurrency, pDate, exchangeRate,  row_number()  over (partition by pFromCurrency,fact_script_name,pToCurrency, pDate,exchangeRate order by '') rownr
    from tmp_getexchangerate1;
	
/*Georgiana 30 Dec 2016 added additional conditions for companycode and customernumber for BSAD and BSID*/	
UPDATE fact_accountsreceivable fap 
   SET amt_ExchangeRate_gbl =  z.exchangeRate
  from BSID arc, tmp_getexchangerate1_upd2 z, tmp_gblcurr_fap, fact_accountsreceivable fap,dim_company dc,dim_customer cm
 WHERE     fap.dd_AccountingDocNo = arc.BSID_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSID_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSID_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSID_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSID_MONAT
        AND z.pFromCurrency  = BSID_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
        AND z.pToCurrency = pGlobalCurrency AND z.pDate = BSID_BUDAT
		AND dc.CompanyCode = arc.BSID_BUKRS
        AND cm.CustomerNumber = arc.BSID_KUNNR 
        AND fap.Dim_CompanyId = dc.Dim_CompanyId
        AND fap.Dim_CustomerId = cm.Dim_CustomerId 
        AND fap.amt_ExchangeRate_GBL <> z.exchangeRate
        and rownr=1;


/* Populate the exchange rates for BSAD data*/
    
	UPDATE fact_accountsreceivable fap 
   SET amt_ExchangeRate =  z.exchangeRate
  from BSAD arc, dim_Company dcm, tmp_getexchangerate1_upd2 z, fact_accountsreceivable fap ,dim_company dc,dim_customer cm
 WHERE     fap.dd_AccountingDocNo = arc.BSAD_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSAD_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSAD_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSAD_MONAT
       AND dcm.CompanyCode = arc.BSAD_BUKRS AND dcm.RowIsCurrent = 1
        AND z.pFromCurrency  = BSAD_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
        AND z.pToCurrency = dcm.currency AND z.pDate = BSAD_BUDAT
		AND dc.CompanyCode = arc.BSAD_BUKRS
        AND cm.CustomerNumber = arc.BSAD_KUNNR 
        AND fap.Dim_CompanyId = dc.Dim_CompanyId
        AND fap.Dim_CustomerId = cm.Dim_CustomerId 
        AND fap.amt_ExchangeRate <> z.exchangeRate
        and  rownr=1
        ;

 UPDATE fact_accountsreceivable fap 
 SET amt_ExchangeRate_gbl =  z.exchangeRate
 from BSAD arc, tmp_getexchangerate1_upd2 z, tmp_gblcurr_fap, fact_accountsreceivable fap,dim_company dc,dim_customer cm
 WHERE     fap.dd_AccountingDocNo = arc.BSAD_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSAD_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSAD_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSAD_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSAD_MONAT
       AND z.pFromCurrency  = BSAD_WAERS and z.fact_script_name = 'bi_populate_accountsreceivable_fact'
       AND z.pToCurrency = pGlobalCurrency AND z.pDate = BSAD_BUDAT
		AND dc.CompanyCode = arc.BSAD_BUKRS
        AND cm.CustomerNumber = arc.BSAD_KUNNR 
        AND fap.Dim_CompanyId = dc.Dim_CompanyId
        AND fap.Dim_CustomerId = cm.Dim_CustomerId 
       AND fap.amt_ExchangeRate_gbl <> z.exchangeRate
       and  rownr=1 
        ;

drop table if exists tmp_getexchangerate1_upd2;

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'bi_populate_accountsreceivable_fact',systimestamp, 'bi_populate_accountsreceivable_fact END',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';		
