
/*
create table PMRA_SALES_QTY_hist
like PMRA_SALES_QTY INCLUDING DEFAULTS INCLUDING IDENTITY

create table PMRA_FORECAST_QTY_hist
like PMRA_FORECAST_QTY INCLUDING DEFAULTS INCLUDING IDENTITY */

--delete from PMRA_SALES_QTY_hist
--where period = TO_CHAR(CURRENT_DATE-INTERVAL '1' MONTH,'YYYYMM');

--insert into PMRA_SALES_QTY_HIST
--SELECT * FROM PMRA_SALES_QTY
--WHERE PERIOD = TO_CHAR(CURRENT_DATE-INTERVAL '1' MONTH,'YYYYMM');

--delete from PMRA_FORECAST_QTY_hist
--where reportingperiod = TO_CHAR(CURRENT_DATE,'YYYYMM');

--insert into PMRA_FORECAST_QTY_HIST
--SELECT * FROM PMRA_FORECAST_QTY
--WHERE reportingperiod = TO_CHAR(CURRENT_DATE,'YYYYMM');


/*First script 1_final22102015_script_SFA_DTA_with_DC_v2*/
/* DTA */
/*21 Nov 2017 Move the DF statementd from dim_part in order to optimive the process of DF*/

drop table if exists tmp_all_atlasforecast_material;
create table tmp_all_atlasforecast_material as
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_DC
union
select distinct sales_uin,sales_cocd from atlas_forecast_sales_merck_DC
union
select distinct uin,cocd from atlas_forecast_budget_merck_DC
union
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2
union 
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1
UNION 
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1
UNION
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2
UNION
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1
UNION
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1
UNION
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2
UNION
select distinct pra_uin,plant_code from atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1;


delete from tmp_all_atlasforecast_material
where pra_uin is null;

delete from number_fountain m where m.table_name = 'dim_part';

insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1;


insert into dim_part
(dim_partid,PartNumber,Plant,Flag_Demandforecast,rowiscurrent)
select
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') + row_number() over(order by ''),
pra_uin,
plant_code,
'Y' as Flag_Demandforecast,
1 from tmp_all_atlasforecast_material a
where
plant_code not in ('XX20','NL10')
and not exists (
select 1 from dim_part b
where b.partnumber = a.pra_uin
and b.plant = a.plant_code);

delete from number_fountain m where m.table_name = 'dim_part';

insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_part d
where d.dim_partid <> 1;

insert into dim_part
(dim_partid,PartNumber,Plant,Flag_Demandforecast,rowiscurrent)
select
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') + row_number() over(order by ''),
pra_uin,
plant_code,
'Y' as Flag_Demandforecast,
1 from tmp_all_atlasforecast_material a
where
plant_code = 'NL10'
and not exists (
select 1 from dim_part b
where b.partnumber = a.pra_uin
and b.plant in ('NL10','XX20'));

/* 01 Nov 2016 Georgiana BI-4374 Moving below inserts at the begining of the script in order to run automatically*/

/*Georgiana 23 May 2017 creating a temporary table for avoid data being unavailable during daily refresh*/ 
delete from tmp_fact_atlaspharmlogiforecast_merck;
insert into tmp_fact_atlaspharmlogiforecast_merck select * from fact_atlaspharmlogiforecast_merck;
delete from fact_atlaspharmlogiforecast_merck_DC;

Drop table if exists max_holder_atlasph;
Create table max_holder_atlasph
as
Select ifnull(max(fact_atlaspharmlogiforecast_merckid),0) as maxid
from fact_atlaspharmlogiforecast_merck_DC;


insert into fact_atlaspharmlogiforecast_merck_DC(	
fact_atlaspharmlogiforecast_merckid,
dim_plantid,
dim_partid,	
dim_companyid,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_dateidforecast,	
dd_version,	
dim_unitofmeasureidbase,	
ct_salesdelivered,	
ct_salesrequested,	
ct_salesforecast,	
ct_promotion,	
ct_adjforlogistics,	
ct_totaladj,	
ct_adjsalesplan,
ct_adjsalesplanfpu,	
ct_adjsalesplanbulk,
ct_salesreqmod,	
ct_adjsalesreq,	
ct_salesbudget,
dd_source,
dim_dateidsnapshot
)
select 
max_holder_atlasph.maxid + row_number() over(ORDER BY '') fact_atlaspharmlogiforecast_merckid,
pl.dim_plantid,
pt.dim_partid,
dc.dim_companyid,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dt.dim_dateid dim_dateidforecast,
0	dd_version,
1 dim_unitofmeasureidbase,
0  ct_salesdelivered,	       
0  ct_salesrequested,	     
0  ct_salesforecast,	       
0  ct_promotion,             
0  ct_adjforlogistics,
0  ct_totaladj,
m.pra_quantity  ct_adjsalesplan,
m.pra_quantityfpu  ct_adjsalesplanfpu,
m.pra_quantitybulk  ct_adjsalesplanbulk,
0  ct_salesreqmod,
0  ct_adjsalesreq,  
0  ct_salesbudget,
'PRA' dd_source	,
dt2.dim_dateid dim_dateidsnapshot
from atlas_forecast_pra_forecasts_merck_DC m,
dim_plant pl, dim_part pt, dim_company dc,
dim_date dt,dim_date dt2, max_holder_atlasph 
where pl.plantcode = m.plant_code and pt.plant = pl.plantcode
and pt.partnumber = m.pra_uin and dt.datevalue = to_date(case when pra_forecasting_period is null then '00010101' else  pra_forecasting_period||'01' end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and dt2.datevalue = to_date(case when pra_reporting_period is null then '00010101' else pra_reporting_period||'01' end ,'YYYYMMDD') and pl.plantcode = dt2.plantcode_factory
and dc.companycode = dt.companycode and pl.companycode = dc.companycode
and dc.companycode = dt2.companycode;

--insert for 09/2016 for stagging tables NL10
 insert into fact_atlaspharmlogiforecast_merck_DC(	
fact_atlaspharmlogiforecast_merckid,
dim_plantid,
dim_partid,	
dim_companyid,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_dateidforecast,	
dd_version,	
dim_unitofmeasureidbase,	
ct_salesdelivered,	
ct_salesrequested,	
ct_salesforecast,	
ct_promotion,	
ct_adjforlogistics,	
ct_totaladj,	
ct_adjsalesplan,
ct_adjsalesplanfpu,	
ct_adjsalesplanbulk,	
ct_salesreqmod,	
ct_adjsalesreq,	
ct_salesbudget,
dd_source,
dim_dateidsnapshot
)
 select 
max_holder_atlasph.maxid + row_number() over(ORDER BY '') fact_atlaspharmlogiforecast_merckid,
pl.dim_plantid,
pt.dim_partid,
dc.dim_companyid,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dt.dim_dateid dim_dateidforecast,
0	dd_version,
1 dim_unitofmeasureidbase,
0  ct_salesdelivered,	       
0  ct_salesrequested,	     
0  ct_salesforecast,	       
0  ct_promotion,             
0  ct_adjforlogistics,
0  ct_totaladj,
m.pra_quantity  ct_adjsalesplan,
m.pra_quantityfpu  ct_adjsalesplanfpu,
m.pra_quantitybulk  ct_adjsalesplanbulk,
0  ct_salesreqmod,
0  ct_adjsalesreq,  
0  ct_salesbudget,
'PRA' dd_source	,
dt2.dim_dateid dim_dateidsnapshot
from atlas_forecast_pra_forecasts_merck_DC m,
dim_plant pl, dim_part pt, dim_company dc,
dim_date dt,dim_date dt2, max_holder_atlasph 
where pl.plantcode = case when  m.plant_code = 'NL10' then 'XX20' else m.plant_code end and pt.plant = pl.plantcode
and pt.partnumber = m.pra_uin and dt.datevalue = to_date(case when pra_forecasting_period is null then '00010101' else pra_forecasting_period||'01' end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and dt2.datevalue = to_date(case when pra_reporting_period is null then '00010101' else pra_reporting_period||'01' end,'YYYYMMDD') and pl.plantcode = dt2.plantcode_factory
and dc.companycode = dt.companycode and pl.companycode = dc.companycode
and dc.companycode = dt2.companycode
 and m.plant_code = 'NL10';
 
 /*01 Nov 2016 End of changes*/

delete from  fact_atlaspharmlogiforecast_merck_snp_DC;
insert into fact_atlaspharmlogiforecast_merck_snp_DC
(dim_dateidreporting,
dim_plantid,
dim_partid,
dim_plantidsfa,
dd_source,
dd_version,
dim_companyid,
reporting_company_code,
hei_code,
country_destination_code,
market_grouping,
dim_unitofmeasureidbase,
ct_salesdelivered,
ct_salesdeliveredfpu,
ct_salesdeliveredbulk,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_Salesbudgetfpu,
ct_Salesbudgetbulk,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
foremonth,
foreyear,
snapmonth,
snapyear)
select distinct
dim_Dateidforecast as dim_dateidreporting,
dim_plantid,
dim_partid,
convert (bigint,1) as dim_plantidsfa,
dd_source,
dd_version,
dim_companyid,
reporting_company_code,
hei_code,
country_destination_code,
market_grouping,
dim_unitofmeasureidbase,
CAST(0 AS DECIMAL (18,4)) as ct_salesdelivered,
CAST(0 AS DECIMAL (18,4))  as ct_salesdeliveredfpu,
CAST(0 AS DECIMAL (18,4))  as ct_salesdeliveredbulk,
CAST(0 AS DECIMAL (18,4)) as ct_salesrequested,
CAST(0 AS DECIMAL (18,4)) as ct_salesforecast,
CAST(0 AS DECIMAL (18,4))  as ct_promotion,
CAST(0 AS DECIMAL (18,4))  as ct_adjforlogistics,
CAST(0 AS DECIMAL (18,4))  as ct_totaladj,
CAST(0 AS DECIMAL (18,4))  as ct_salesreqmod,
CAST(0 AS DECIMAL (18,4))  as ct_adjsalesreq,
CAST(0 AS DECIMAL (18,4))  as ct_Salesbudget,
CAST(0 AS DECIMAL (18,4))  as ct_Salesbudgetfpu,
CAST(0 AS DECIMAL (18,4))  as ct_Salesbudgetbulk,
CAST(0 AS DECIMAL (18,8))  as ct_salesmonth1,
CAST(0 AS DECIMAL (18,8))  as ct_salesmonth2,
CAST(0 AS DECIMAL (18,4))  as ct_salesmonth3,
CAST(0 as integer) foremonth,
CAST(0 as integer) foreyear,
CAST(0 as integer) snapmonth,
CAST(0 as integer) snapyear
from fact_atlaspharmlogiforecast_merck_DC
where fact_atlaspharmlogiforecast_merckid <> 1;


/*08 May 2017 Georgiana Insert records that are present in RF2 and not in RF1 in order to be able to calculate statistical measures like next12 months, last months etc*/
drop table if exists tmp_for_insert_budget;
create table tmp_for_insert_budget as 
select distinct plant_code,pra_uin,'201701' as period,0 as budgetqty ,rf.reporting_company_code,rf.hei_code,rf.country_destination_code,rf.market_grouping,0 as budgetqtyfpu,0 as budgetqtybulk from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 rf
where not exists (select 1 from atlas_forecast_budget_merck_DC b
where 
b.cocd=rf.plant_code and
b.uin=rf.pra_uin
and b.period=rf.pra_period and
rf.reporting_company_code =b.reporting_company_code
and rf.country_destination_code=b.country_destination_code
)
---and pra_uin='146342' and plant_code='DE40'
and pra_period  not like '2018%'
union all

select distinct plant_code,pra_uin,'201702' as period,0 as budgetqty ,rf.reporting_company_code,rf.hei_code,rf.country_destination_code,rf.market_grouping,0 as budgetqtyfpu,0 as budgetqtybulk from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 rf
where not exists (select 1 from atlas_forecast_budget_merck_DC b
where 
b.cocd=rf.plant_code and
b.uin=rf.pra_uin
and b.period=rf.pra_period and
rf.reporting_company_code =b.reporting_company_code
and rf.country_destination_code=b.country_destination_code
)
---and pra_uin='146342' and plant_code='DE40'
and pra_period  not like '2018%'
union all 
select distinct plant_code,pra_uin,'201703' as period,0 as budgetqty ,rf.reporting_company_code,rf.hei_code,rf.country_destination_code,rf.market_grouping,0 as budgetqtyfpu,0 as budgetqtybulk from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 rf
where not exists (select 1 from atlas_forecast_budget_merck_DC b
where 
b.cocd=rf.plant_code and
b.uin=rf.pra_uin
and b.period=rf.pra_period and
rf.reporting_company_code =b.reporting_company_code
and rf.country_destination_code=b.country_destination_code
)
---and pra_uin='146342' and plant_code='DE40'
and pra_period  not like '2018%';

insert into atlas_forecast_budget_merck_DC 
select * from tmp_for_insert_budget rf
where not exists (select 1 from atlas_forecast_budget_merck_DC b
where 
b.cocd=rf.plant_code and
b.uin=rf.pra_uin
and b.period=rf.period and
rf.reporting_company_code =b.reporting_company_code
and rf.country_destination_code=b.country_destination_code);

drop table if exists tmp_for_insert_budget;

/*08 May 2017 End*/

drop table if exists atlas_forecast_budget_upd_DC;

create table atlas_forecast_budget_upd_DC as
select dim_partid, dim_plantid,reporting_company_code,
country_destination_code, d.dim_dateid, ifnull(sum(a.budgetqty),0) budgetqty,
 ifnull(sum(a.budgetqtyfpu),0) budgetqtyfpu,
  ifnull(sum(a.budgetqtybulk),0) budgetqtybulk,
  max(market_grouping) as market_grouping
from atlas_forecast_budget_merck_DC a, dim_part pt, dim_plant pl,
dim_date d
where pt.partnumber = uin and pt.plant = cocd and pl.plantcode = cocd
and d.datevalue = to_date(case when period is null then '00010101' else concat(period,'01') end,'YYYYMMDD')  and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
group by dim_partid, dim_plantid,reporting_company_code,
country_destination_code, d.dim_dateid;

insert into fact_atlaspharmlogiforecast_merck_snp_DC(dim_dateidreporting, dim_plantid, dim_partid,
dd_source,dd_version, dim_companyid, dim_plantidsfa,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_unitofmeasureidbase, ct_salesdelivered,
ct_salesdeliveredfpu,
ct_salesdeliveredbulk,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_Salesbudgetfpu,
ct_Salesbudgetbulk,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,foremonth, foreyear, snapmonth,snapyear)
select distinct d.dim_dateid as dim_dateidreporting, dim_plantid, dim_partid,
'S991' dd_source,
'A00' as dd_version, dim_companyid, convert(bigint,1) as dim_plantidsfa,
reporting_company_code,hei_code,country_destination_code,market_grouping,
convert(bigint,1) dim_unitofmeasureidbase,
s.buom_quantity as ct_salesdelivered,
s.buom_quantityfpu as ct_salesdeliveredfpu,
s.buom_quantitybulk as ct_salesdeliveredbulk,
cast(0 as decimal (18,4)) as ct_salesrequested,
cast(0 as decimal (18,4)) as ct_salesforecast,
cast(0 as decimal (18,4)) as ct_promotion,
cast(0 as decimal (18,4)) as ct_adjforlogistics,
cast(0 as decimal (18,4)) as ct_totaladj,
cast(0 as decimal (18,4)) as ct_salesreqmod,
cast(0 as decimal (18,4)) as ct_adjsalesreq,
cast(0 as decimal (18,4)) as ct_Salesbudget,
cast(0 as decimal (18,4)) as ct_Salesbudgetfpu,
cast(0 as decimal (18,4)) as ct_Salesbudgetbulk,
cast(0 as decimal (18,8)) as ct_salesmonth1,
cast(0 as decimal (18,8)) as ct_salesmonth2,
cast(0 as decimal (18,4)) as ct_salesmonth3,
cast(0 as integer) foremonth,
cast(0 as integer) foreyear,
cast(0 as integer) snapmonth,
cast(0 as integer) snapyear
from atlas_forecast_sales_merck_DC  s,
dim_part pt, dim_plant pl, dim_date d, dim_company dc
where lpad(s.sales_uin,6,'0') = pt.partnumber and pt.plant = s.sales_cocd
and s.sales_cocd = pl.plantcode and d.datevalue = to_date(case when sales_reporting_period is null then '00010101' else concat(sales_reporting_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode and dc.companycode = pl.companycode
and not exists
(select 1 from fact_atlaspharmlogiforecast_merck_snp_DC f
where d.dim_dateid = f.dim_dateidreporting and pt.dim_partid = f.dim_partid
and f.reporting_company_code = s.reporting_company_code and
f.country_destination_code = s.country_destination_code
and pl.dim_plantid = f.dim_plantid and dd_version = 'A00')
union
select distinct d.dim_dateid as dim_dateidreporting, dim_plantid, dim_partid,
'S991' dd_source,
'A00' as dd_version, dim_companyid, convert(bigint,1) as dim_plantidsfa,
reporting_company_code,hei_code,country_destination_code,market_grouping,
convert(bigint,1) dim_unitofmeasureidbase,
s.buom_quantity as ct_salesdelivered,
s.buom_quantityfpu as ct_salesdeliveredfpu,
s.buom_quantitybulk as ct_salesdeliveredbulk,
cast(0 as decimal (18,4)) as ct_salesrequested,
cast(0 as decimal (18,4)) as ct_salesforecast,
cast(0 as decimal (18,4)) as ct_promotion,
cast(0 as decimal (18,4)) as ct_adjforlogistics,
cast(0 as decimal (18,4)) as ct_totaladj,
cast(0 as decimal (18,4)) as ct_salesreqmod,
cast(0 as decimal (18,4)) as ct_adjsalesreq,
cast(0 as decimal (18,4)) as ct_Salesbudget,
cast(0 as decimal (18,4)) as ct_Salesbudgetfpu,
cast(0 as decimal (18,4)) as ct_Salesbudgetbulk,
cast(0 as decimal (18,8)) as ct_salesmonth1,
cast(0 as decimal (18,8)) as ct_salesmonth2,
cast(0 as decimal (18,4)) as ct_salesmonth3,
cast(0 as integer) foremonth,
cast(0 as integer) foreyear,
cast(0 as integer) snapmonth,
cast(0 as integer) snapyear
from atlas_forecast_sales_merck_DC  s,
dim_part pt, dim_plant pl, dim_date d, dim_company dc
where lpad(s.sales_uin,6,'0') = pt.partnumber and pt.plant = 'XX20'
and s.sales_cocd = 'NL10' and pl.plantcode='XX20' and d.datevalue = to_date(case when sales_reporting_period is null then '00010101' else concat(sales_reporting_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode and dc.companycode = pl.companycode
union
select distinct f.dim_dateid dim_dateidreporting,f.dim_plantid,dim_partid,
'S991' dd_source,
'A00' as dd_version,
dim_companyid,
convert(bigint,1) as dim_plantidsfa,
reporting_company_code,
null as hei_code,
country_destination_code,
market_grouping,
convert(bigint,1) dim_unitofmeasureidbase,
cast(0 as decimal (18,4)) as ct_salesdelivered,
cast(0 as decimal (18,4)) as ct_salesdeliveredfpu,
cast(0 as decimal (18,4)) as ct_salesdeliveredbulk,
cast(0 as decimal (18,4)) as ct_salesrequested,
cast(0 as decimal (18,4)) as ct_salesforecast,
cast(0 as decimal (18,4)) as ct_promotion,
cast(0 as decimal (18,4)) as ct_adjforlogistics,
cast(0 as decimal (18,4)) as ct_totaladj,
cast(0 as decimal (18,4)) as ct_salesreqmod,
cast(0 as decimal (18,4)) as ct_adjsalesreq,
budgetqty as ct_Salesbudget,
budgetqtyfpu as ct_Salesbudgetfpu,
budgetqtybulk as ct_Salesbudgetbulk,
cast(0 as decimal (18,4)) as ct_salesmonth1,
cast(0 as decimal (18,4)) as ct_salesmonth2,
cast(0 as decimal (18,4)) as ct_salesmonth3,
cast(0 as integer) foremonth,
cast(0 as integer) foreyear,
cast(0 as integer) snapmonth,
cast(0 as integer) snapyear
 from atlas_forecast_budget_upd_DC f,dim_plant pl0,dim_company dc0
where
f.dim_plantid = pl0.dim_plantid and dc0.companycode = pl0.companycode
and not exists (select 1 from atlas_forecast_sales_merck_DC  s,
dim_part pt, dim_plant pl, dim_date d, dim_company dc
where lpad(s.sales_uin,6,'0') = pt.partnumber and pt.plant = s.sales_cocd
and s.sales_cocd = pl.plantcode and d.datevalue = to_date(case when sales_reporting_period is null then '00010101' else concat(sales_reporting_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode and dc.companycode = pl.companycode
and f.dim_partid = pt.dim_partid and f.dim_plantid = pl.dim_plantid
and f.dim_dateid = d.dim_dateid and f.reporting_company_code = s.reporting_company_code and
f.country_destination_code = s.country_destination_code
)
and   not exists (select 1 from atlas_forecast_pra_forecasts_merck_DC  s1,
dim_part pt1, dim_plant pl1, dim_date d1, dim_company dc1
where s1.pra_uin = pt1.partnumber and pt1.plant = s1.plant_code
and s1.plant_code = pl1.plantcode and d1.datevalue = to_date(case when pra_reporting_period is null then '00010101' else concat(pra_reporting_period,'01') end,'YYYYMMDD') and pl1.plantcode = d1.plantcode_factory
and d1.companycode = pl1.companycode and dc1.companycode = pl1.companycode
and f.dim_partid = pt1.dim_partid and f.dim_plantid = pl1.dim_plantid
and f.dim_dateid = d1.dim_dateid and f.reporting_company_code = s1.reporting_company_code and
f.country_destination_code = s1.country_destination_code
);

insert into fact_atlaspharmlogiforecast_merck_snp_DC(dim_dateidreporting, dim_plantid, dim_partid,
dd_source,dd_version, dim_companyid, dim_plantidsfa,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_unitofmeasureidbase, ct_salesdelivered,
ct_salesdeliveredfpu,
ct_salesdeliveredbulk,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_Salesbudgetfpu,
ct_Salesbudgetbulk,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,foremonth, foreyear, snapmonth,snapyear)
select distinct dim_dateidsnapshot,f.dim_plantid,dim_partid,
'S991' dd_source,
'A00' as dd_version,
dim_companyid,
convert(bigint,1) as dim_plantidsfa,
reporting_company_code,
null as hei_code,
country_destination_code,
market_grouping,
convert(bigint,1) dim_unitofmeasureidbase,
cast(0 as decimal (18,4)) as ct_salesdelivered,
cast(0 as decimal (18,4)) as ct_salesdeliveredfpu,
cast(0 as decimal (18,4)) as ct_salesdeliveredbulk,
cast(0 as decimal (18,4)) as ct_salesrequested,
cast(0 as decimal (18,4)) as ct_salesforecast,
cast(0 as decimal (18,4)) as ct_promotion,
cast(0 as decimal (18,4)) as ct_adjforlogistics,
cast(0 as decimal (18,4)) as ct_totaladj,
cast(0 as decimal (18,4)) as ct_salesreqmod,
cast(0 as decimal (18,4)) as ct_adjsalesreq,
cast(0 as decimal (18,4)) as ct_Salesbudget,
cast(0 as decimal (18,4)) as ct_Salesbudgetfpu,
cast(0 as decimal (18,4)) as ct_Salesbudgetbulk,
cast(0 as decimal (18,4)) as ct_salesmonth1,
cast(0 as decimal (18,4)) as ct_salesmonth2,
cast(0 as decimal (18,4)) as ct_salesmonth3,
cast(0 as integer) foremonth,
cast(0 as integer) foreyear,
cast(0 as integer) snapmonth,
cast(0 as integer) snapyear
from fact_atlaspharmlogiforecast_merck_DC f, dim_date dt1
where  f.dim_dateidsnapshot = dt1.dim_dateid and not exists
(select 1 from fact_atlaspharmlogiforecast_merck_snp_DC s, dim_date ds1
where ds1.datevalue = dt1.datevalue and s.dim_partid = f.dim_partid
and f.reporting_company_code = s.reporting_company_code and
f.country_destination_code = s.country_destination_code
and s.dim_plantid = f.dim_plantid and s.dd_version = 'A00'
and ds1.dim_dateid = s.dim_dateidreporting);



/* INSERT just NL10 */
insert into fact_atlaspharmlogiforecast_merck_snp_DC(dim_dateidreporting, dim_plantid, dim_partid,
dd_source,dd_version, dim_companyid,dim_plantidsfa,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_unitofmeasureidbase, ct_salesdelivered,
 ct_salesdeliveredfpu,
  ct_salesdeliveredbulk,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_Salesbudgetfpu,
ct_Salesbudgetbulk,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,ct_adjsalesplan, foremonth, foreyear, snapmonth,snapyear)
select distinct dt2.dim_dateid as dim_dateidreporting, dim_plantid, dim_partid,
'PRA' dd_source,
'0' as dd_version, dim_companyid,convert(bigint,1) as dim_plantidsfa,
reporting_company_code,hei_code,country_destination_code,market_grouping,
convert(bigint,1) dim_unitofmeasureidbase,
cast(0 as decimal (18,4)) as ct_salesdelivered,
cast(0 as decimal (18,4)) as ct_salesdeliveredfpu,
cast(0 as decimal (18,4)) as ct_salesdeliveredbulk,
cast(0 as decimal (18,4)) as ct_salesrequested,
cast(0 as decimal (18,4)) as ct_salesforecast,
cast(0 as decimal (18,4)) as ct_promotion,
cast(0 as decimal (18,4)) as ct_adjforlogistics,
cast(0 as decimal (18,4)) as ct_totaladj,
cast(0 as decimal (18,4)) as ct_salesreqmod,
cast(0 as decimal (18,4)) as ct_adjsalesreq,
cast(0 as decimal (18,4)) as ct_Salesbudget,
cast(0 as decimal (18,4)) as ct_Salesbudgetfpu,
cast(0 as decimal (18,4)) as ct_Salesbudgetbulk,
cast(0 as decimal (18,8)) as ct_salesmonth1,
cast(0 as decimal (18,8)) as ct_salesmonth2,
cast(0 as decimal (18,4)) as ct_salesmonth3,
m.pra_quantity  ct_adjsalesplan,
cast(0 as integer) foremonth,
cast(0 as integer) foreyear,
cast(0 as integer) snapmonth,
cast(0 as integer) snapyear
from atlas_forecast_pra_forecasts_merck_DC m,
dim_plant pl, dim_part pt, dim_company dc,
dim_date dt,dim_date dt2, max_holder_atlasph
where  pt.plant = pl.plantcode
and pt.partnumber = m.pra_uin and dt.datevalue = to_date(case when pra_forecasting_period is null then '00010101' else concat(pra_forecasting_period,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and dt2.datevalue = to_date(case when pra_reporting_period is null then '00010101' else concat(pra_reporting_period,'01') end,'YYYYMMDD') and pl.plantcode = dt2.plantcode_factory
and dc.companycode = dt.companycode and pl.companycode = dc.companycode
and dc.companycode = dt2.companycode
and pt.plant = 'XX20' and m.plant_code = 'NL10'
and not exists (select 1 from
dim_part pt2
where pra_uin = pt2.partnumber  and plant_code = pt2.plant
and plant_code = 'NL10');

/* insert blank */
drop table if exists tmp_insert_dates;
create table tmp_insert_dates as
select distinct
to_char(datevalue,'YYYYMM') as rep_per from dim_date
where datevalue between  current_date-INTERVAL '24' MONTH
and  current_date + INTERVAL '24' MONTH;

/*drop table if exists tmp_insert_forecasts*/
/*create table tmp_insert_forecasts as
select distinct partnumber,plant,
reporting_company_code,*//*market_grouping,*//*country_destination_code,
to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') as rep_date
from fact_atlaspharmlogiforecast_merck_snp_DC f, dim_part pt,dim_plant pl,
tmp_insert_dates d, dim_date dt
where f.dim_partid = pt.dim_partid and f.dim_plantid = pl.dim_plantid
and pl.companycode = dt.companycode and dt.datevalue =to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory

union
select distinct sales_uin as partnumber,
sales_cocd as plant,reporting_company_code,*//*market_grouping,*/
/*country_destination_code,
to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') as rep_date
 from
atlas_forecast_sales_merck_DC,tmp_insert_dates d*/

drop table if exists tmp_for_lastrepdate_1;
create table tmp_for_lastrepdate_1 as select distinct pra_uin,plant_code,reporting_company_code,country_destination_code, max(pra_reporting_period) as maxrepperiod
from atlas_forecast_pra_forecasts_merck_DC
group by pra_uin,plant_code,reporting_company_code,country_destination_code;

drop table if exists tmp_for_lastrepdate_2;
create table tmp_for_lastrepdate_2 as select distinct sales_uin,sales_cocd,reporting_company_code, max(sales_reporting_period) as maxrepperiod
from atlas_forecast_sales_merck_DC
 group by sales_uin,sales_cocd,reporting_company_code;


/*drop table if exists tmp_insert_forecasts
create table tmp_insert_forecasts as
select distinct partnumber,plant,
f.reporting_company_code,/*market_grouping,f.country_destination_code,
to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') as rep_date
from fact_atlaspharmlogiforecast_merck_snp_DC f, dim_part pt,dim_plant pl,
tmp_insert_dates d, dim_date dt,dim_date dt2,tmp_for_lastrepdate_1 m
where f.dim_partid = pt.dim_partid and f.dim_plantid = pl.dim_plantid
and pl.companycode = dt.companycode and dt.datevalue =to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and dim_dateidreporting=dt2.dim_dateid
and dt2.datevalue=to_date(case when m.maxrepperiod is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory and pl.companycode = dt2.companycode
and pt.partnumber=m.pra_uin and pt.plant=m.plant_code and f.reporting_company_code=m.reporting_company_code and f.country_destination_code=m.country_destination_code
union
select distinct dc.sales_uin as partnumber,
dc.sales_cocd as plant,dc.reporting_company_code,/*market_grouping,
dc.country_destination_code,
to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') as rep_date
 from
atlas_forecast_sales_merck_DC dc,tmp_insert_dates d,tmp_for_lastrepdate_2 m
where dc.sales_uin=m.sales_uin and dc.sales_cocd=m.sales_cocd and dc.reporting_company_code=m.reporting_company_code 
and dc.sales_reporting_period=m.maxrepperiod*/

drop table if exists tmp_insert_forecasts;
create table tmp_insert_forecasts as
select distinct partnumber,plant,
f.reporting_company_code,/*market_grouping,*/f.country_destination_code,
to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') as rep_date
from fact_atlaspharmlogiforecast_merck_snp_DC f, dim_part pt,dim_plant pl,
tmp_insert_dates d, dim_date dt,dim_date dt2,tmp_for_lastrepdate_1 m
where f.dim_partid = pt.dim_partid and f.dim_plantid = pl.dim_plantid
and pl.companycode = dt.companycode and dt.datevalue =to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and dim_dateidreporting=dt2.dim_dateid
and dt2.datevalue=to_date(case when m.maxrepperiod is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') and pl.plantcode = dt2.plantcode_factory and pl.companycode = dt2.companycode
and pt.partnumber=m.pra_uin and pt.plant=m.plant_code and f.reporting_company_code=m.reporting_company_code and f.country_destination_code=m.country_destination_code
/*and pt.partnumber='000840' and pt.plant='NL20'*/
union
select distinct partnumber,plant,
f.reporting_company_code,/*market_grouping,*/f.country_destination_code,
to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') as rep_date
 from fact_atlaspharmlogiforecast_merck_snp_DC f, dim_part pt,dim_plant pl,
tmp_insert_dates d, dim_date dt,dim_date dt2,tmp_for_lastrepdate_2 m
where f.dim_partid = pt.dim_partid and f.dim_plantid = pl.dim_plantid
and pl.companycode = dt.companycode and dt.datevalue =to_date(case when d.rep_per is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and dim_dateidreporting=dt2.dim_dateid
and dt2.datevalue=to_date(case when m.maxrepperiod is null then '00010101' else concat(d.rep_per,'01') end,'YYYYMMDD') and pl.plantcode = dt2.plantcode_factory and pl.companycode = dt2.companycode
and pt.partnumber=m.sales_uin and pt.plant=m.sales_cocd and f.reporting_company_code=m.reporting_company_code /*and f.country_destination_code=m.country_destination_code*/
/*and pt.partnumber='000840' and pt.plant='NL20'*/;

drop table if exists tmp_insert_forecasts2;
create table tmp_insert_forecasts2 LIKE tmp_insert_forecasts INCLUDING DEFAULTS INCLUDING IDENTITY;

insert into tmp_insert_forecasts2
select distinct partnumber,plant,
reporting_company_code,country_destination_code,/*market_grouping,*/ d.datevalue  from
fact_atlaspharmlogiforecast_merck_snp_DC s, dim_date d,
dim_part pt,dim_plant pl
where s.dim_partid = pt.dim_partid and s.dim_plantid = pl.dim_plantid
and d.dim_dateid = s.dim_dateidreporting and s.dd_version = 'A00';

/*call vectorwise(combine 'tmp_insert_forecasts - tmp_insert_forecasts2')*/
rename tmp_insert_forecasts to tmp_insert_forecasts_1;
create table tmp_insert_forecasts as
(select * from tmp_insert_forecasts_1) minus (Select * from tmp_insert_forecasts2);
drop table tmp_insert_forecasts_1;

insert into fact_atlaspharmlogiforecast_merck_snp_DC
(dim_dateidreporting, dim_plantid, dim_partid,
reporting_company_code,country_destination_code, /*market_grouping,*/
dim_unitofmeasureidbase,ct_salesdelivered,ct_salesdeliveredfpu,ct_salesdeliveredbulk,dd_source,
dd_version, dim_companyid, dim_plantidsfa,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_Salesbudgetfpu,
ct_Salesbudgetbulk,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
foremonth,
foreyear,
snapmonth,
snapyear)
select
d.dim_dateid as dim_dateidreporting, dim_plantid, dim_partid,
reporting_company_code,country_destination_code, /*market_grouping,*/
 convert(bigint,1) as dim_unitofmeasureidbase,cast(0 as decimal (18,4)) as ct_salesdelivered,
 cast(0 as decimal (18,4)) as ct_salesdeliveredfpu,
 cast(0 as decimal (18,4)) as ct_salesdeliveredbulk,'PRA' as dd_source,
'A00' as dd_version, dc.dim_companyid as dim_companyid, convert(bigint,1) as dim_plantidsfa,
cast(0 as decimal (18,4)) as ct_salesrequested,
cast(0 as decimal (18,4)) as ct_salesforecast,
cast(0 as decimal (18,4)) as ct_promotion,
cast(0 as decimal (18,4)) as ct_adjforlogistics,
cast(0 as decimal (18,4)) as ct_totaladj,
cast(0 as decimal (18,4)) as ct_adjsalesplan,
cast(0 as decimal (18,4)) as ct_salesreqmod,
cast(0 as decimal (18,4)) as ct_adjsalesreq,
cast(0 as decimal (18,4)) as ct_Salesbudget,
cast(0 as decimal (18,4)) as ct_Salesbudgetfpu,
cast(0 as decimal (18,4)) as ct_Salesbudgetbulk,
cast(0 as decimal (18,8)) as ct_salesmonth1,
cast(0 as decimal (18,8)) as ct_salesmonth2,
cast(0 as decimal (18,4)) as ct_salesmonth3,
month(d.datevalue) as foremonth,
year(d.datevalue) as foreyear,
month(d.datevalue + interval '1' month) as snapmonth,
year(d.datevalue + interval '1' month) as snapyear
from tmp_insert_forecasts t, dim_part pt, dim_plant pl, dim_Date d,
dim_company dc
where t.partnumber = pt.partnumber and pt.plant = t.plant
and pl.plantcode = t.plant and pl.companycode =d.companycode
and dc.companycode = pl.companycode
and d.datevalue = rep_date and pl.plantcode = d.plantcode_factory;

/*NL10 Part*/

insert into fact_atlaspharmlogiforecast_merck_snp_DC
(dim_dateidreporting, dim_plantid, dim_partid,
reporting_company_code,country_destination_code, /*market_grouping,*/
dim_unitofmeasureidbase,ct_salesdelivered,ct_salesdeliveredfpu,ct_salesdeliveredbulk,dd_source,
dd_version, dim_companyid, dim_plantidsfa,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_Salesbudgetfpu,
ct_Salesbudgetbulk,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
foremonth,
foreyear,
snapmonth,
snapyear)
select
d.dim_dateid as dim_dateidreporting, dim_plantid, dim_partid,
reporting_company_code,country_destination_code, /*market_grouping,*/
 convert(bigint,1) as dim_unitofmeasureidbase,cast(0 as decimal (18,4)) as ct_salesdelivered,
 cast(0 as decimal (18,4)) as ct_salesdeliveredfpu,
 cast(0 as decimal (18,4)) as ct_salesdeliveredbulk,'PRA' as dd_source,
'A00' as dd_version, dc.dim_companyid as dim_companyid, convert(bigint,1) as dim_plantidsfa,
cast(0 as decimal (18,4)) as ct_salesrequested,
cast(0 as decimal (18,4)) as ct_salesforecast,
cast(0 as decimal (18,4)) as ct_promotion,
cast(0 as decimal (18,4)) as ct_adjforlogistics,
cast(0 as decimal (18,4)) as ct_totaladj,
cast(0 as decimal (18,4)) as ct_adjsalesplan,
cast(0 as decimal (18,4)) as ct_salesreqmod,
cast(0 as decimal (18,4)) as ct_adjsalesreq,
cast(0 as decimal (18,4)) as ct_Salesbudget,
cast(0 as decimal (18,4)) as ct_Salesbudgetfpu,
cast(0 as decimal (18,4)) as ct_Salesbudgetbulk,
cast(0 as decimal (18,8)) as ct_salesmonth1,
cast(0 as decimal (18,8)) as ct_salesmonth2,
cast(0 as decimal (18,4)) as ct_salesmonth3,
month(d.datevalue) as foremonth,
year(d.datevalue) as foreyear,
month(d.datevalue + interval '1' month) as snapmonth,
year(d.datevalue + interval '1' month) as snapyear
from tmp_insert_forecasts t, dim_part pt, dim_plant pl, dim_Date d,
dim_company dc
where t.partnumber = pt.partnumber /*and pt.plant = t.plant*/
and pl.plantcode = t.plant and pt.plant='XX20' and t.plant='NL10' and pl.companycode =d.companycode
and dc.companycode = pl.companycode
and d.datevalue = rep_date and pl.plantcode = d.plantcode_factory;
/*NL10 inset end*/

drop table if exists tmp_insert_forecasts;
drop table if exists tmp_insert_forecasts2;
/* insert blank */


update fact_atlaspharmlogiforecast_merck_snp_DC a
set foreyear = year(d.datevalue),
foremonth = month(d.datevalue),
snapyear = year(d.datevalue + interval '1' month),
snapmonth = month(d.datevalue + interval '1' month)
from dim_Date d,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_dateidreporting = d.dim_dateid;

drop table if exists tmp_atlas_forecast_SalesUpd_DC;
create table tmp_atlas_forecast_SalesUpd_DC as
select distinct pt.dim_partid, pl.dim_plantid,
reporting_company_code,
country_destination_code,
month(dt.datevalue) foremonth, year(dt.datevalue) foreyear,
dt.dim_dateid dim_dateidreporting, BUoM_Quantity ct_salesdelivered,
BUoM_Quantityfpu ct_salesdeliveredfpu,
BUoM_Quantitybulk ct_salesdeliveredbulk
from atlas_forecast_sales_merck_DC s,
dim_plant pl, dim_part pt, dim_date dt
where pl.plantcode = s.sales_cocd and pt.plant = pl.plantcode
and lpad(pt.partnumber,6,'0') = lpad(s.sales_uin,6,'0')
and dt.datevalue = to_date(case when sales_reporting_period is null then '00010101' else concat(sales_reporting_period,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and pl.companycode = dt.companycode;

/*Ambiguous replace fix*/
update fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_salesdelivered =  b.ct_salesdelivered ,
    a.ct_salesdeliveredfpu =  b.ct_salesdeliveredfpu,
	a.ct_salesdeliveredbulk = b.ct_salesdeliveredbulk
from (select distinct b.dim_plantid,b.dim_partid,b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,b.dim_dateidreporting,max(b.ct_salesdelivered) ct_salesdelivered, max(b.ct_salesdeliveredfpu) ct_salesdeliveredfpu,max(b.ct_salesdeliveredbulk) ct_salesdeliveredbulk from tmp_atlas_forecast_SalesUpd_DC b
group by b.dim_plantid,b.dim_partid,b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,b.dim_dateidreporting) b, fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
and a.foremonth = b.foremonth and a.foreyear = b.foreyear
and a.dim_dateidreporting = b.dim_dateidreporting
and ifnull(a.ct_salesdelivered,-1.999) <> b.ct_salesdelivered;
drop table if exists tmp_atlas_forecast_SalesUpd_DC;

/* NEW just for NL10 */
drop table if exists tmp_atlas_forecast_SalesUpd_DC2;
create table tmp_atlas_forecast_SalesUpd_DC2 as
select distinct pt.dim_partid, pl.dim_plantid,
reporting_company_code,
country_destination_code,
month(dt.datevalue) foremonth, year(dt.datevalue) foreyear,
dt.dim_dateid dim_dateidreporting,case when BUoM_Quantity <0 then 0 else BUoM_Quantity end  ct_salesdelivered
from atlas_forecast_sales_merck_DC s,
dim_plant pl, dim_part pt, dim_date dt
where  pt.plant = pl.plantcode
and lpad(pt.partnumber,8,'0') = lpad(s.sales_uin,8,'0')
and dt.datevalue = to_date(case when sales_reporting_period is null then '00010101' else concat(sales_reporting_period,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and pl.companycode = dt.companycode
and pt.plant = 'XX20' and s.sales_cocd = 'NL10'
and not exists (select 1 from
dim_part pt2
where lpad(pt2.partnumber,8,'0') = lpad(s.sales_uin,8,'0')  and sales_cocd = pt2.plant
and sales_cocd = 'NL10');

update fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_salesdelivered = b.ct_salesdelivered
from tmp_atlas_forecast_SalesUpd_DC2 b, fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
and a.foremonth = b.foremonth and a.foreyear = b.foreyear
and a.dim_dateidreporting = b.dim_dateidreporting
and ifnull(a.ct_salesdelivered,-1.999) <> b.ct_salesdelivered;
drop table if exists tmp_atlas_forecast_SalesUpd_DC2;

drop table if exists atlas_forecast_budget_upd_DC;

create table atlas_forecast_budget_upd_DC as
select dim_partid, dim_plantid,reporting_company_code,
country_destination_code, d.dim_dateid, sum(a.budgetqty) budgetqty,
sum(a.budgetqtyfpu) budgetqtyfpu,sum(a.budgetqtybulk) budgetqtybulk
from atlas_forecast_budget_merck_DC a, dim_part pt, dim_plant pl,
dim_date d
where pt.partnumber = uin and pt.plant = cocd and pl.plantcode = cocd
and d.datevalue = to_date(case when period is null then '00010101' else concat(period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
group by dim_partid, dim_plantid,reporting_company_code,
country_destination_code, d.dim_dateid;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_Salesbudget = budgetqty,
ct_Salesbudgetfpu = budgetqtyfpu,
ct_Salesbudgetbulk = budgetqtybulk
from
atlas_forecast_budget_upd_DC u,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_plantid = u.dim_plantid and f.dim_partid = u.dim_partid
and u.reporting_company_code = f.reporting_company_code
and u.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid;



/* NEW just for NL10 */
drop table if exists atlas_forecast_budget_upd_DC2;
create table atlas_forecast_budget_upd_DC2 as
select dim_partid, dim_plantid,reporting_company_code,
country_destination_code, d.dim_dateid, sum(a.budgetqty) budgetqty,
sum(a.budgetqtyfpu) budgetqtyfpu,sum(a.budgetqtybulk) budgetqtybulk
from atlas_forecast_budget_merck_DC a, dim_part pt, dim_plant pl,
dim_date d
where pt.partnumber = uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when period is null then '00010101' else concat(period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  cocd = 'NL10'
and not exists (select 1 from
dim_part pt2
where lpad(pt2.partnumber,8,'0') = lpad( uin,8,'0')  and  cocd = pt2.plant
and  cocd = 'NL10')
group by dim_partid, dim_plantid,reporting_company_code,
country_destination_code, d.dim_dateid;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_Salesbudget = budgetqty,
ct_Salesbudgetfpu = budgetqtyfpu,
ct_Salesbudgetbulk = budgetqtybulk
from
atlas_forecast_budget_upd_DC2 u,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_plantid = u.dim_plantid and f.dim_partid = u.dim_partid
and u.reporting_company_code = f.reporting_company_code
and u.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid;
drop table if exists atlas_forecast_budget_upd_DC2;

drop table if exists tmp_NL10partsinsert;
create table tmp_NL10partsinsert as
select   distinct partnumber from atlas_forecast_pra_forecasts_merck_DC f,
dim_part pt
where pra_uin = partnumber --and plant_code = plant
and plant = 'XX20' and plant_code = 'NL10'
and not exists (select 1 from
dim_part pt2
where pra_uin = pt2.partnumber  and plant_code = pt2.plant
and plant_code = 'NL10');

/* NEW Plant for SFA */
update tmp_fact_atlaspharmlogiforecast_merck f
set dim_plantidsfa = (select dim_plantid from dim_plant where plantcode = 'NL10')
from tmp_NL10partsinsert t, dim_part pt,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_partid = pt.dim_partid and t.partnumber = pt.partnumber
and pt.plant = 'XX20' and dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set  dim_plantidsfa = dim_plantid
where dd_version = 'DTA'
and dim_plantidsfa = 1;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dim_plantidsfa = (select dim_plantid from dim_plant where plantcode = 'NL10')
from tmp_NL10partsinsert t, dim_part pt,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_partid = pt.dim_partid and t.partnumber = pt.partnumber
and pt.plant = 'XX20';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set  dim_plantidsfa = dim_plantid
where  dim_plantidsfa = 1;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dim_plantidsfa = (select dim_plantid from dim_plant where plantcode = 'NL10')
from tmp_NL10partsinsert t, dim_part pt,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_partid = pt.dim_partid and t.partnumber = pt.partnumber
and pt.plant = 'XX20' and dd_version = 'DTA';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set  dim_plantidsfa = dim_plantid
where dd_version = 'DTA'
and dim_plantidsfa = 1;

/*Andrian change 15 December 2015*/
update fact_atlaspharmlogiforecast_merck_snp_DC a
set a.dim_plantidsfa = 91
from dim_plant b, dim_plant c,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_plantid = b.dim_plantid
and b.plantcode = 'XX20'
and a.dim_plantidsfa = c.dim_plantid
and c.plantcode = 'XX20';


drop table if exists fact_atlaspharmlogiforecast_merck_upd1_DC;
create table fact_atlaspharmlogiforecast_merck_upd1_DC as
select  distinct snapmonth,  snapyear, dim_partid, dim_plantid,
reporting_company_code,country_destination_code,
dim_Dateidreporting,ct_salesdelivered from
fact_atlaspharmlogiforecast_merck_snp_DC b;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_SalesMonth1 = b.ct_salesdelivered
 from fact_atlaspharmlogiforecast_merck_upd1_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  a.dim_Dateidreporting = b.dim_Dateidreporting
--	and b.snapmonth = a.snapmonth and b.snapyear = a.snapyear
	and ifnull(a.ct_SalesMonth1,-1.999) <> b.ct_salesdelivered;

drop table if exists fact_atlaspharmlogiforecast_merck_upd1_DC;


drop table if exists fact_atlaspharmlogiforecast_merck_upd2_DC;
create table fact_atlaspharmlogiforecast_merck_upd2_DC as
select distinct snapmonth,  snapyear, dim_partid, dim_plantid,
reporting_company_code,country_destination_code,
dim_Dateidreporting,ct_salesdelivered,
case when b.foremonth+1<=12 then b.foremonth+1 else b.foremonth+1-12 end foremonth,
case when b.foremonth+1<=12 then b.foreyear else b.foreyear+1 end foreyear
 from
fact_atlaspharmlogiforecast_merck_snp_DC b;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_SalesMonth2 = b.ct_salesdelivered
 from fact_atlaspharmlogiforecast_merck_upd2_DC b ,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.foremonth = a.foremonth
	 and b.foreyear = a.foreyear --and b.snapmonth = a.snapmonth
	-- and b.snapyear = a.snapyear
		and ifnull(a.ct_SalesMonth2,-1.999) <> b.ct_salesdelivered;

drop table if exists fact_atlaspharmlogiforecast_merck_upd2_DC;


drop table if exists fact_atlaspharmlogiforecast_merck_upd3_DC;
create table fact_atlaspharmlogiforecast_merck_upd3_DC as
select distinct snapmonth, snapyear, dim_partid, dim_plantid,
reporting_company_code,country_destination_code,
dim_Dateidreporting,ct_salesdelivered,
case when b.foremonth+2<=12 then b.foremonth+2 else b.foremonth+2-12 end foremonth,
case when b.foremonth+2<=12 then b.foreyear else b.foreyear+1 end foreyear
 from
fact_atlaspharmlogiforecast_merck_snp_DC b;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_SalesMonth3 = b.ct_salesdelivered
 from fact_atlaspharmlogiforecast_merck_upd3_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
   and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 --and b.snapmonth = a.snapmonth
--	 and b.snapyear = a.snapyear
	and b.foremonth = a.foremonth
	 and b.foreyear = a.foreyear
and ifnull(a.ct_SalesMonth3,-1.999) <> b.ct_salesdelivered;

drop table if exists fact_atlaspharmlogiforecast_merck_upd3_DC;

/* Forecasts */


update  fact_atlaspharmlogiforecast_merck_snp_DC a
set fore_minus_4_2_month = (month(x.datevalue) - 1)
from dim_date x,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = x.dim_Dateid
and ifnull(fore_minus_4_2_month,99) <> (month(x.datevalue) - 1);

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set fore_minus_4_2_year = year(x.datevalue)
from dim_date x,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = x.dim_Dateid
and ifnull(fore_minus_4_2_year,0) <> year(x.datevalue);

UPDATE  fact_atlaspharmlogiforecast_merck_snp_DC
SET fore_minus_4_2_year  = fore_minus_4_2_year - 1 where fore_minus_4_2_month <= 0;

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET fore_minus_4_2_month  = fore_minus_4_2_month + 12 where fore_minus_4_2_month <= 0;


update  fact_atlaspharmlogiforecast_merck_snp_DC a
set snap_minus_4_month = month(y.datevalue) - 4
from dim_date y,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = y.dim_Dateid
and ifnull(snap_minus_4_month,99) <> month(y.datevalue) - 4;

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set snap_minus_4_year = year(y.datevalue)
from dim_date y,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = y.dim_Dateid
and ifnull(snap_minus_4_year,0) <> year(y.datevalue);

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET snap_minus_4_year  = snap_minus_4_year - 1 where snap_minus_4_month <= 0;

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET snap_minus_4_month  = snap_minus_4_month + 12 where snap_minus_4_month <= 0;

/* End SFA - 4 */


update  fact_atlaspharmlogiforecast_merck_snp_DC a
set
snap_minus_9_month = month(y.datevalue) - 9
from dim_date y,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = y.dim_Dateid
and ifnull(snap_minus_9_month,-99) <> month(y.datevalue) - 9;

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set
snap_minus_9_year = year(y.datevalue)
from dim_date y,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = y.dim_Dateid
and ifnull(snap_minus_9_year,0) <> year(y.datevalue);

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set
fore_minus_2_month = (month(x.datevalue) - 1)
from dim_date x,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = x.dim_Dateid
and ifnull(fore_minus_2_month,-99) <> (month(x.datevalue) - 1);

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set
fore_minus_2_year = year(x.datevalue)
from dim_date x,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = x.dim_Dateid
and ifnull(fore_minus_2_year,0) <> year(x.datevalue);

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET snap_minus_9_year  = snap_minus_9_year - 1 where snap_minus_9_month <= 0;

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET snap_minus_9_month  = snap_minus_9_month + 12 where snap_minus_9_month <= 0;

UPDATE  fact_atlaspharmlogiforecast_merck_snp_DC
SET fore_minus_2_year  = fore_minus_2_year - 1 where fore_minus_2_month <= 0;

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET fore_minus_2_month  = fore_minus_2_month + 12 where fore_minus_2_month <= 0;


update  fact_atlaspharmlogiforecast_merck_snp_DC a
set fore_minus_6_2_month = (month(x.datevalue) - 1)
from dim_date x,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = x.dim_Dateid
and ifnull(fore_minus_6_2_month,99) <> (month(x.datevalue) - 1);

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set fore_minus_6_2_year = year(x.datevalue)
from dim_date x,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = x.dim_Dateid
and ifnull(fore_minus_6_2_year,0) <> year(x.datevalue);

UPDATE  fact_atlaspharmlogiforecast_merck_snp_DC
SET fore_minus_6_2_year  = fore_minus_6_2_year - 1 where fore_minus_6_2_month <= 0;

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET fore_minus_6_2_month  = fore_minus_6_2_month + 12 where fore_minus_6_2_month <= 0;


update  fact_atlaspharmlogiforecast_merck_snp_DC a
set snap_minus_3_month = month(y.datevalue) - 3
from dim_date y,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = y.dim_Dateid
and ifnull(snap_minus_3_month,99) <> month(y.datevalue) - 3;

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set snap_minus_3_year = year(y.datevalue)
from dim_date y, fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = y.dim_Dateid
and ifnull(snap_minus_3_year,0) <> year(y.datevalue);


UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET snap_minus_3_year  = snap_minus_3_year - 1 where snap_minus_3_month <= 0;

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET snap_minus_3_month  = snap_minus_3_month + 12 where snap_minus_3_month <= 0;


update  fact_atlaspharmlogiforecast_merck_snp_DC a
set snap_minus_6_month = month(y.datevalue) - 6
from dim_date y, fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = y.dim_Dateid
and ifnull(snap_minus_6_month,99) <> month(y.datevalue) - 6;

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set snap_minus_6_year = year(y.datevalue)
from dim_date y,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = y.dim_Dateid
and ifnull(snap_minus_6_year,0) <> year(y.datevalue);

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET snap_minus_6_year  = snap_minus_6_year - 1 where snap_minus_6_month <= 0;

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET snap_minus_6_month  = snap_minus_6_month + 12 where snap_minus_6_month <= 0;

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set fore_minus_9_3_month = (month(x.datevalue) - 2)
from dim_date x,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = x.dim_Dateid
and ifnull(fore_minus_9_3_month,99) <> (month(x.datevalue) - 1);

update  fact_atlaspharmlogiforecast_merck_snp_DC a
set fore_minus_9_3_year = year(x.datevalue)
from dim_date x,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_Dateidreporting = x.dim_Dateid
and ifnull(fore_minus_9_3_year,0) <> year(x.datevalue);

UPDATE  fact_atlaspharmlogiforecast_merck_snp_DC
SET fore_minus_9_3_year  = fore_minus_9_3_year - 1 where fore_minus_9_3_month <= 0;

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC
SET fore_minus_9_3_month  = fore_minus_9_3_month + 12 where fore_minus_9_3_month <= 0;

 	/*
	Forecast 9_2
	*/
DROP TABLE IF EXISTS tmp_upd_fact_atlaspharmlogiforecast_merck_DC;
CREATE TABLE tmp_upd_fact_atlaspharmlogiforecast_merck_DC
AS
SELECT DISTINCT dim_plantid,dim_partid,reporting_company_code,country_destination_code,
year(p.datevalue) updforecastyear,
month(p.datevalue) updforecastmonth,
year(q.datevalue) updsnpyear,
month(q.datevalue) updsnpmonth,
ct_adjsalesplan
FROM fact_atlaspharmlogiforecast_merck_DC b, dim_date p, dim_date q
WHERE b.dim_Dateidforecast = p.dim_dateid AND b.dim_dateidsnapshot = q.dim_dateid
and fact_atlaspharmlogiforecast_merckid <> 1 and dd_source = 'PRA';


UPDATE fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_Forecast9Mth2 = b.ct_adjsalesplan
FROM  tmp_upd_fact_atlaspharmlogiforecast_merck_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
WHERE  a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
AND b.updsnpyear = a.snap_minus_9_year
AND b.updsnpmonth = a.snap_minus_9_month
AND b.updforecastyear = a.fore_minus_2_year
AND b.updforecastmonth = a.fore_minus_2_month
AND ifnull(a.ct_Forecast9Mth2,-99) <> b.ct_adjsalesplan;

 	/*
	Forecast 9_3
	*/

DROP TABLE IF EXISTS tmp_upd_fact_atlaspharmlogiforecast_merck_DC;
CREATE TABLE tmp_upd_fact_atlaspharmlogiforecast_merck_DC
AS
SELECT DISTINCT dim_plantid,dim_partid,reporting_company_code,country_destination_code,
year(p.datevalue) updforecastyear,
month(p.datevalue) updforecastmonth,
year(q.datevalue) updsnpyear,
month(q.datevalue) updsnpmonth,
ct_adjsalesplan
FROM fact_atlaspharmlogiforecast_merck_DC b, dim_date p, dim_date q
WHERE b.dim_Dateidforecast = p.dim_dateid AND b.dim_dateidsnapshot = q.dim_dateid
and fact_atlaspharmlogiforecast_merckid <> 1 and dd_source = 'PRA';


UPDATE fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_Forecast9Mth3 = b.ct_adjsalesplan
FROM  tmp_upd_fact_atlaspharmlogiforecast_merck_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
WHERE  a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
AND b.updsnpyear = a.snap_minus_9_year
AND b.updsnpmonth = a.snap_minus_9_month
AND b.updforecastyear = a.fore_minus_9_3_year
AND b.updforecastmonth = a.fore_minus_9_3_month
AND ifnull(a.ct_Forecast9Mth3,-1.999) <> b.ct_adjsalesplan;

 		/*
	Forecast 6_2
	*/

DROP TABLE IF EXISTS tmp_upd_fact_atlaspharmlogiforecast_merck_DC;
CREATE TABLE tmp_upd_fact_atlaspharmlogiforecast_merck_DC
AS
SELECT DISTINCT dim_plantid,dim_partid,reporting_company_code,country_destination_code,
year(p.datevalue) updforecastyear,
month(p.datevalue) updforecastmonth,
year(q.datevalue) updsnpyear,
month(q.datevalue) updsnpmonth,
ct_adjsalesplan
FROM fact_atlaspharmlogiforecast_merck_DC b, dim_date p, dim_date q
WHERE b.dim_Dateidforecast = p.dim_dateid AND b.dim_dateidsnapshot = q.dim_dateid
and fact_atlaspharmlogiforecast_merckid <> 1 and dd_source = 'PRA';


UPDATE fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_Forecast6Mth2 = b.ct_adjsalesplan
FROM  tmp_upd_fact_atlaspharmlogiforecast_merck_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
WHERE  a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
AND b.updsnpyear = a.snap_minus_6_year
AND b.updsnpmonth = a.snap_minus_6_month
AND b.updforecastyear = a.fore_minus_6_2_year
AND b.updforecastmonth = a.fore_minus_6_2_month
AND ifnull(a.ct_Forecast6Mth2,-1.999) <> b.ct_adjsalesplan;

 /*
Forecast 6 1
*/

DROP TABLE IF EXISTS tmp_upd_fact_atlaspharmlogiforecast_merck_DC;
CREATE TABLE tmp_upd_fact_atlaspharmlogiforecast_merck_DC
AS
SELECT DISTINCT dim_plantid,dim_partid,reporting_company_code,country_destination_code,
year(p.datevalue) updforecastyear,
month(p.datevalue) updforecastmonth,
year(q.datevalue) updsnpyear,
month(q.datevalue) updsnpmonth,
ct_adjsalesplan
FROM fact_atlaspharmlogiforecast_merck_DC b, dim_date p, dim_date q
WHERE b.dim_Dateidforecast = p.dim_dateid AND b.dim_dateidsnapshot = q.dim_dateid
and fact_atlaspharmlogiforecast_merckid <> 1 and dd_source = 'PRA';

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_Forecast6Mth1 = b.ct_adjsalesplan
FROM  tmp_upd_fact_atlaspharmlogiforecast_merck_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
WHERE  a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
AND b.updsnpyear = a.snap_minus_6_year
AND b.updsnpmonth = a.snap_minus_6_month
AND b.updforecastyear = a.foreyear
AND b.updforecastmonth = a.foremonth
AND ifnull(a.ct_Forecast6Mth1,-1.999) <> b.ct_adjsalesplan;

/*
Forecast 3 1
*/

DROP TABLE IF EXISTS tmp_upd_fact_atlaspharmlogiforecast_merck_DC;
CREATE TABLE tmp_upd_fact_atlaspharmlogiforecast_merck_DC
AS
SELECT DISTINCT dim_plantid,dim_partid,reporting_company_code,country_destination_code,
year(p.datevalue) updforecastyear,
month(p.datevalue) updforecastmonth,
year(q.datevalue) updsnpyear,
month(q.datevalue) updsnpmonth,
ct_adjsalesplan
FROM fact_atlaspharmlogiforecast_merck_DC b, dim_date p, dim_date q
WHERE b.dim_Dateidforecast = p.dim_dateid AND b.dim_dateidsnapshot = q.dim_dateid
and fact_atlaspharmlogiforecast_merckid <> 1 and dd_source = 'PRA';

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_Forecast3Mth = b.ct_adjsalesplan
FROM  tmp_upd_fact_atlaspharmlogiforecast_merck_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
WHERE  a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
AND b.updsnpyear = a.snap_minus_3_year
AND b.updsnpmonth = a.snap_minus_3_month
AND b.updforecastyear = a.foreyear
AND b.updforecastmonth = a.foremonth
AND ifnull(a.ct_Forecast3Mth,-99) <> b.ct_adjsalesplan;

 	/*
Forecast 9 1
*/

DROP TABLE IF EXISTS tmp_upd_fact_atlaspharmlogiforecast_merck_DC;
CREATE TABLE tmp_upd_fact_atlaspharmlogiforecast_merck_DC
AS
SELECT DISTINCT dim_plantid,dim_partid,reporting_company_code,country_destination_code,
year(p.datevalue) updforecastyear,
month(p.datevalue) updforecastmonth,
year(q.datevalue) updsnpyear,
month(q.datevalue) updsnpmonth,
ct_adjsalesplan
FROM fact_atlaspharmlogiforecast_merck_DC b, dim_date p, dim_date q
WHERE b.dim_Dateidforecast = p.dim_dateid AND b.dim_dateidsnapshot = q.dim_dateid
and fact_atlaspharmlogiforecast_merckid <> 1 and dd_source = 'PRA';


UPDATE fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_Forecast9Mth1 = b.ct_adjsalesplan
FROM  tmp_upd_fact_atlaspharmlogiforecast_merck_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
WHERE  a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
AND b.updsnpyear = a.snap_minus_9_year
AND b.updsnpmonth = a.snap_minus_9_month
AND b.updforecastyear = a.foreyear
AND b.updforecastmonth = a.foremonth
AND ifnull(a.ct_Forecast9Mth1,-1.999) <> b.ct_adjsalesplan;

		/*
	Forecast 4_2
	*/

DROP TABLE IF EXISTS tmp_upd_fact_atlaspharmlogiforecast_merck_DC;
CREATE TABLE tmp_upd_fact_atlaspharmlogiforecast_merck_DC
AS
SELECT DISTINCT dim_plantid,dim_partid,reporting_company_code,country_destination_code,
year(p.datevalue) updforecastyear,
month(p.datevalue) updforecastmonth,
year(q.datevalue) updsnpyear,
month(q.datevalue) updsnpmonth,
ct_adjsalesplan,
ct_adjsalesplanbulk
FROM fact_atlaspharmlogiforecast_merck_DC b, dim_date p, dim_date q
WHERE b.dim_Dateidforecast = p.dim_dateid AND b.dim_dateidsnapshot = q.dim_dateid
and fact_atlaspharmlogiforecast_merckid <> 1 and dd_source = 'PRA';


UPDATE fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_Forecast4Mth2 = b.ct_adjsalesplan,
    a.ct_forecast4bulkmth2 = b.ct_adjsalesplanbulk
FROM  tmp_upd_fact_atlaspharmlogiforecast_merck_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
WHERE  a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
AND b.updsnpyear = a.snap_minus_4_year
AND b.updsnpmonth = a.snap_minus_4_month
AND b.updforecastyear = a.fore_minus_4_2_year
AND b.updforecastmonth = a.fore_minus_4_2_month
AND ifnull(a.ct_Forecast4Mth2,-1.999) <> b.ct_adjsalesplan;

 /*
Forecast 4 1
*/

DROP TABLE IF EXISTS tmp_upd_fact_atlaspharmlogiforecast_merck_DC;
CREATE TABLE tmp_upd_fact_atlaspharmlogiforecast_merck_DC
AS
SELECT DISTINCT dim_plantid,dim_partid,reporting_company_code,country_destination_code,
year(p.datevalue) updforecastyear,
month(p.datevalue) updforecastmonth,
year(q.datevalue) updsnpyear,
month(q.datevalue) updsnpmonth,
ct_adjsalesplan,ct_adjsalesplanbulk
FROM fact_atlaspharmlogiforecast_merck_DC b, dim_date p, dim_date q
WHERE b.dim_Dateidforecast = p.dim_dateid AND b.dim_dateidsnapshot = q.dim_dateid
and fact_atlaspharmlogiforecast_merckid <> 1 and dd_source = 'PRA';

UPDATE fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_Forecast4Mth1 = b.ct_adjsalesplan,
    a.ct_forecast4bulkmth1 = b.ct_adjsalesplanbulk
FROM  tmp_upd_fact_atlaspharmlogiforecast_merck_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
WHERE  a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
AND b.updsnpyear = a.snap_minus_4_year
AND b.updsnpmonth = a.snap_minus_4_month
AND b.updforecastyear = a.foreyear
AND b.updforecastmonth = a.foremonth
AND ifnull(a.ct_Forecast4Mth1,-1.999) <> b.ct_adjsalesplan;


/*
DTA measures
*/

update fact_atlaspharmlogiforecast_merck_DC a
set foreyear = year(d.datevalue)
from dim_Date d,fact_atlaspharmlogiforecast_merck_DC a
where a.dim_dateidforecast = d.dim_dateid
and ifnull(foreyear,-1.999) <> year(d.datevalue);

update fact_atlaspharmlogiforecast_merck_DC a
set foremonth = month(d.datevalue)
from dim_Date d,fact_atlaspharmlogiforecast_merck_DC a
where a.dim_dateidforecast = d.dim_dateid
and ifnull(foremonth,-1.999) <> month(d.datevalue);

update fact_atlaspharmlogiforecast_merck_DC a
set snapyear = year(d.datevalue)
from dim_Date d,fact_atlaspharmlogiforecast_merck_DC a
where a.dim_dateidsnapshot = d.dim_dateid
and ifnull(snapyear,-1.999) <> year(d.datevalue);

update fact_atlaspharmlogiforecast_merck_DC a
set snapmonth = month(d.datevalue)
from dim_Date d,fact_atlaspharmlogiforecast_merck_DC a
where a.dim_dateidsnapshot = d.dim_dateid
and ifnull(snapmonth,-1.999) <> month(d.datevalue);

/*call vectorwise(combine 'fact_atlaspharmlogiforecast_merck_snp_DC')*/

/*
update fact_atlaspharmlogiforecast_merck_snp_DC b
 from (
select distinct dim_plantid, dim_partid,reporting_company_code,country_destination_code, foreyear,
foremonth,dd_version, ct_adjsalesplan,snapmonth,snapyear
   from fact_atlaspharmlogiforecast_merck_DC a
 where  dd_source = 'PRA'
  and fact_atlaspharmlogiforecast_merckid <> 1) a
   set b.ct_adjsalesplan = a.ct_adjsalesplan
   where a.dim_plantid =b.dim_plantid and a.dim_partid=b.dim_partid
     and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
   and b.foreyear=a.foreyear and a.foremonth=b.foremonth
   and a.snapmonth = case when b.foremonth=12 then 1 else b.foremonth+1 end
   and a.snapyear = case when b.foremonth=12 then b.foreyear+1 else b.foreyear end
   and a.dd_version = b.dd_version and ifnull(b.ct_adjsalesplan,-1.999)<>a.ct_adjsalesplan
 */
 
 
/*Andrian - for SFA using <0 for DTA no*/
drop table if exists tmp_atlas_forecast_SalesUpd_DC;
create table tmp_atlas_forecast_SalesUpd_DC as
select distinct pt.dim_partid, pl.dim_plantid,
reporting_company_code,
country_destination_code,
month(dt.datevalue) foremonth, year(dt.datevalue) foreyear,
dt.datevalue , case when BUoM_Quantity <0 then 0 else BUoM_Quantity end ct_salesdelivered,
case when BUoM_Quantityfpu <0 then 0 else BUoM_Quantityfpu end  ct_salesdeliveredfpu,
case when BUoM_Quantitybulk <0 then 0 else BUoM_Quantitybulk end  ct_salesdeliveredbulk
from atlas_forecast_sales_merck_DC s,
dim_plant pl, dim_part pt, dim_date dt
where pl.plantcode = s.sales_cocd and pt.plant = pl.plantcode
and lpad(pt.partnumber,6,'0') = lpad(s.sales_uin,6,'0')
and dt.datevalue = to_date(case when sales_reporting_period is null then '00010101' else concat(sales_reporting_period,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and pl.companycode = dt.companycode;

/*Ambiguous Replace fix*/

update fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_salesdelivered =  b.ct_salesdelivered ,
    a.ct_salesdeliveredfpu =  b.ct_salesdeliveredfpu,
	a.ct_salesdeliveredbulk = b.ct_salesdeliveredbulk
from  (select distinct b.dim_plantid,b.dim_partid,b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,max(b.ct_salesdelivered) ct_salesdelivered,max(b.ct_salesdeliveredfpu) ct_salesdeliveredfpu,max(b.ct_salesdeliveredbulk) ct_salesdeliveredbulk
from  tmp_atlas_forecast_SalesUpd_DC b
group by  b.dim_plantid,b.dim_partid,b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear ) b
,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
and a.foremonth = b.foremonth and a.foreyear = b.foreyear
and ifnull(a.ct_salesdelivered,-1.999) <> b.ct_salesdelivered;

drop table if exists tmp_atlas_forecast_SalesUpd_DC;

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta1_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta1_DC as
select dim_partid, dim_plantid, foreyear, dd_version,reporting_company_code,country_destination_code,
sum(ct_Salesdelivered)  ct_Salesdelivered,sum(ct_Salesdeliveredfpu) ct_Salesdeliveredfpu,
sum(ct_Salesdeliveredbulk) ct_Salesdeliveredbulk
from (
select distinct dim_partid, dim_plantid, snapmonth, snapyear,
foremonth, foreyear, dd_version, reporting_company_code,country_destination_code,
ct_Salesdelivered,ct_Salesdeliveredfpu,ct_Salesdeliveredbulk
from fact_atlaspharmlogiforecast_merck_snp_DC
) f
group by dim_partid, dim_plantid,  foreyear, dd_version, reporting_company_code,
country_destination_code;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salesdelivprevyear = b.ct_salesdelivered,
     a.ct_salesdelivprevyearfpu = b.ct_salesdeliveredfpu,
	 a.ct_salesdelivprevyearbulk = b.ct_salesdeliveredbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta1_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear-1
and (ifnull(a.ct_salesdelivprevyear,-1.999) <> b.ct_salesdelivered
  or ifnull(a.ct_salesdelivprevyearfpu,-1.999) <> b.ct_salesdeliveredfpu
  or ifnull(a.ct_salesdelivprevyearbulk,-1.999) <> b.ct_salesdeliveredbulk);



/* upd 2*/

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta2_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta2_DC as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
sum(f1.ct_Salesdelivered)  ct_Salesdelivered,
sum(f1.ct_Salesdeliveredfpu)  ct_Salesdeliveredfpu,
sum(f1.ct_Salesdeliveredbulk)  ct_Salesdeliveredbulk
from (
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_Salesdelivered
from fact_atlaspharmlogiforecast_merck_snp_DC
) f,
(
select distinct dim_partid, dim_plantid, snapmonth, snapyear,
foremonth, foreyear, dd_version, ct_Salesdelivered,ct_Salesdeliveredfpu,ct_Salesdeliveredbulk,reporting_company_code,country_destination_code
from fact_atlaspharmlogiforecast_merck_snp_DC
) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
  and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 and f.dd_version = f1.dd_version   and ((
 f1.foreyear = f.foreyear and f1.foremonth <= f.foremonth) OR
(f1.foremonth >= case when f.foremonth=12 then 1 else f.foremonth+1 end
and f1.foreyear =  case when f.foremonth=12 then f.foreyear else f.foreyear-1 end))
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;



update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salesdelivmoving = b.ct_salesdelivered,
     a.ct_salesdelivmovingfpu  = b.ct_Salesdeliveredfpu,
	 a.ct_salesdelivmovingbulk = b.ct_Salesdeliveredbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta2_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
   and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and (ifnull(a.ct_salesdelivmoving,-1.999) <> b.ct_salesdelivered or
ifnull(a.ct_salesdelivmovingfpu,-1.999) <> b.ct_salesdeliveredfpu or
ifnull(a.ct_salesdelivmovingbulk,-1.999) <> b.ct_salesdeliveredbulk);

/* upd 3 */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta3_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta3_DC as
select distinct f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
sum(f1.ct_Salesdelivered)  ct_Salesdelivered,
sum(f1.ct_Salesdeliveredfpu)  ct_Salesdeliveredfpu,
sum(f1.ct_Salesdeliveredbulk)  ct_Salesdeliveredbulk
from (
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_Salesdelivered
from fact_atlaspharmlogiforecast_merck_snp_DC
) f,
(
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_Salesdelivered,ct_Salesdeliveredfpu,ct_Salesdeliveredbulk
from fact_atlaspharmlogiforecast_merck_snp_DC
) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
  and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 and f.dd_version = f1.dd_version  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;


update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_Salesdelivactualytd = b.ct_salesdelivered,
     a.ct_Salesdelivactualytdfpu = b.ct_salesdeliveredfpu,
	 a.ct_Salesdelivactualytdbulk = b.ct_salesdeliveredbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta3_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and (ifnull(a.ct_Salesdelivactualytd,-1.999) <> b.ct_salesdelivered or
ifnull(a.ct_Salesdelivactualytdfpu,-1.999) <> b.ct_salesdeliveredfpu or
ifnull(a.ct_Salesdelivactualytdbulk,-1.999) <> b.ct_salesdeliveredbulk);

/* upd 3.1 develop 4-Nov-2015 based on Ralf request */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta3_1_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta3_1_DC as
select distinct f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
sum(f1.ct_Salesdelivered)  ct_Salesdelivered,
sum(f1.ct_Salesdeliveredfpu)  ct_Salesdeliveredfpu,
sum(f1.ct_Salesdeliveredbulk)  ct_Salesdeliveredbulk
from (
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_Salesdelivered
from fact_atlaspharmlogiforecast_merck_snp_DC
) f,
(
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_Salesdelivered,ct_Salesdeliveredfpu,ct_Salesdeliveredbulk
from fact_atlaspharmlogiforecast_merck_snp_DC
) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
  and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 and f.dd_version = f1.dd_version  and (
 f1.foreyear = f.foreyear-1 and f1.foremonth >= 1
 and f1.foremonth <= (select case when to_char(max(b.datevalue),'MM') = '01' then 13
            when to_char(max(b.datevalue),'MM') = '02' then 12
            else to_char(max(b.datevalue),'MM')-2 end as MONREPOR from fact_atlaspharmlogiforecast_merck_DC a,dim_date b
where a.dim_dateidsnapshot = b.dim_dateid))
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;


update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_Salesdelivactualprevytd = b.ct_salesdelivered,
     a.ct_Salesdelivactualprevytdfpu = b.ct_salesdeliveredfpu,
	 a.ct_Salesdelivactualprevytdbulk = b.ct_salesdeliveredbulk

 from fact_atlaspharmlogiforecast_merck_snp_upddta3_1_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and (ifnull(a.ct_Salesdelivactualprevytd,-1.999) <> b.ct_salesdelivered or
ifnull(a.ct_Salesdelivactualprevytdfpu,-1.999) <> b.ct_salesdeliveredfpu or
ifnull(a.ct_Salesdelivactualprevytdbulk,-1.999) <> b.ct_salesdeliveredbulk) ;


/* upd 4 */
drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta4_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta4_DC as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.dd_version,x.reporting_company_code,x.country_destination_code,
 sum( x.ct_Salesbudget) ct_Salesbudget, sum( x.ct_Salesbudgetfpu) ct_Salesbudgetfpu,sum( x.ct_Salesbudgetbulk) ct_Salesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
  f1.ct_Salesbudget,f1.ct_Salesbudgetfpu,f1.ct_Salesbudgetbulk,f1.foremonth
from fact_atlaspharmlogiforecast_merck_snp_DC f,
fact_atlaspharmlogiforecast_merck_snp_DC f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
/* and f.dd_version = f1.dd_version */  and
 f1.foreyear = f.foreyear  ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.dd_version,x.reporting_company_code,x.country_destination_code;


update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_budgetyear = b.ct_Salesbudget,
     a.ct_budgetyearfpu = b.ct_Salesbudgetfpu,
	 a.ct_budgetyearbulk = b.ct_Salesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta4_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear
 and (ifnull(a.ct_budgetyear,-1.999) <> b.ct_Salesbudget
 or ifnull(a.ct_budgetyearfpu,-1.999) <> b.ct_Salesbudgetfpu
 or ifnull(a.ct_budgetyearbulk,-1.999) <> b.ct_Salesbudgetbulk);

  /* upd 5 */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth, x.dd_version,reporting_company_code,country_destination_code,
 sum( x.ct_Salesbudget) ct_Salesbudget, sum( x.ct_Salesbudgetfpu) ct_Salesbudgetfpu,sum( x.ct_Salesbudgetbulk) ct_Salesbudgetbulk
  from ( 
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth, f.dd_version,
f.reporting_company_code,f.country_destination_code,
 f1.ct_Salesbudget,f1.ct_Salesbudgetfpu,f1.ct_Salesbudgetbulk, f1.foreyear as foreyear1,f1.foremonth as foremonth1
from fact_atlaspharmlogiforecast_merck_snp_DC f, 
fact_atlaspharmlogiforecast_merck_snp_DC f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code 
 and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth, x.dd_version,reporting_company_code,country_destination_code;


update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_budgetytd = b.ct_Salesbudget,
     a.ct_budgetytdfpu = b.ct_Salesbudgetfpu,
	 a.ct_budgetytdbulk = b.ct_Salesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC b, fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and ifnull(a.ct_budgetytd,-1.999) <> b.ct_Salesbudget;

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta1_DC;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta2_DC;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta3_DC;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta4_DC;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC;


/* upd 6 */

/* 06 Sep 2017 Georgiana changes according to APP-7408, APP-5237, Next 12 Months should be calculated taking into account reporting month and the sum of forecast quantity received betwee reporting month+12 months, so for example if we check jan 2017 as reporting month we should see the sum of Jan2017-Dec 2017 period from jan 2017 file */
/*
drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta6_DC
create table fact_atlaspharmlogiforecast_merck_snp_upddta6_DC as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
sum(f1.ct_adjsalesplan)  ct_adjsalesplan,
sum(f1.ct_adjsalesplanfpu)  ct_adjsalesplanfpu,
sum(f1.ct_adjsalesplanbulk)  ct_adjsalesplanbulk
from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear, foremonth,reporting_company_code,country_destination_code,
snapmonth,snapyear, dd_version, ct_adjsalesplan,ct_adjsalesplanfpu,ct_adjsalesplanbulk
   from fact_atlaspharmlogiforecast_merck_DC a
 where  fact_atlaspharmlogiforecast_merckid <> 1
 and dd_source = 'PRA' and dd_version = '0' ) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version   and ((
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear =  case when f.foremonth+1<=12 then f.foreyear else f.foreyear+1 end
and f1.snapmonth = case when f.foremonth+1<=12 then f.foremonth+1 else f.foremonth+1-12 end )
 group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version*/

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta6_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta6_DC as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
sum(f1.ct_adjsalesplan)  ct_adjsalesplan,
sum(f1.ct_adjsalesplanfpu)  ct_adjsalesplanfpu,
sum(f1.ct_adjsalesplanbulk)  ct_adjsalesplanbulk
from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear, foremonth,reporting_company_code,country_destination_code,
snapmonth,snapyear, dd_version, ct_adjsalesplan,ct_adjsalesplanfpu,ct_adjsalesplanbulk
   from fact_atlaspharmlogiforecast_merck_DC a
 where  fact_atlaspharmlogiforecast_merckid <> 1
 and dd_source = 'PRA' and dd_version = '0' 
) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/   and ((
 f1.foreyear = f.foreyear and f1.foremonth > f.foremonth) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear = case when f.foremonth+1<=12 then f.foreyear else f.foreyear+1 end
and f1.snapmonth = case when f.foremonth+1<=12 then f.foremonth+1 else f.foremonth+1-12 end )
/*and dp.dim_partid=f.dim_partid
and dp.partnumber='005195'
and dp.plant='AE20'
and f.foreyear='2017' and f.foremonth='1'
and f.dd_version='A00'*/
 group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;


update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_forecastnext12mths = b.ct_adjsalesplan,
 a.ct_forecastnext12mthsfpu = b.ct_adjsalesplanfpu,
 a.ct_forecastnext12mthsbulk = b.ct_adjsalesplanbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta6_DC b ,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapmonth = b.snapmonth and a.snapyear = b.snapyear;

update fact_atlaspharmlogiforecast_merck_snp_DC
set  ct_forecastnext12mths =0 where ct_forecastnext12mths is null;

update fact_atlaspharmlogiforecast_merck_snp_DC
set  ct_forecastnext12mthsfpu =0 where ct_forecastnext12mthsfpu is null;

update fact_atlaspharmlogiforecast_merck_snp_DC
set  ct_forecastnext12mthsbulk =0 where ct_forecastnext12mthsbulk is null;

/* upd 6.1 */
/* 06 Sep 2017 Georgiana changes according to APP-7408, APP-5237, Next 12 Months previous month should be calculated taking into account previous reporting month and the sum of forecast quantity received between reporting month+12 months, so for ecample if we want to see for rep month jan 2017 we should search in Dec 2016 file  the sum of jan 2017-dec 2017 period */
 /*drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta61_DC
create table fact_atlaspharmlogiforecast_merck_snp_upddta61_DC as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
sum(f1.ct_adjsalesplan)  ct_adjsalesplan
from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear, foremonth,reporting_company_code,country_destination_code,
snapmonth,snapyear, dd_version, ct_adjsalesplan,ct_adjsalesplanfpu,ct_adjsalesplanbulk
   from fact_atlaspharmlogiforecast_merck_DC a
 where  fact_atlaspharmlogiforecast_merckid <> 1
 and dd_source = 'PRA' and dd_version = '0' ) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/  /* and ((
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and f1.snapyear = f.foreyear
and f1.snapmonth = f.foremonth*/
/*
and (f1.snapyear =  case when f.foremonth+1<=12 then f.foreyear else f.foreyear+1 end
and f1.snapmonth = case when f.foremonth+1<=12 then f.foremonth+1 else f.foremonth+1-12 end )
*/
/*group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version*/

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta61_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta61_DC as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
 f.foremonth,  f.foreyear, f.dd_version,
/*f1.snapmonth,f1.snapyear,f1.foremonth,f1.foreyear*/
sum(f1.ct_adjsalesplan)  ct_adjsalesplan
from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid,  foreyear, foremonth,reporting_company_code,country_destination_code,
snapmonth,snapyear, dd_version, ct_adjsalesplan,ct_adjsalesplanfpu,ct_adjsalesplanbulk
   from fact_atlaspharmlogiforecast_merck_DC a
 where  fact_atlaspharmlogiforecast_merckid <> 1
 and dd_source = 'PRA' and dd_version = '0' ) f1/*,dim_part dp*/
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/  and ((
 f1.foreyear = f.foreyear and f1.foremonth > f.foremonth) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear =  case when f.foremonth-1<=0 then f.foreyear-1 else f.foreyear end
and f1.snapmonth = case when f.foremonth-1<=0 then f.foremonth+11 else f.foremonth-1 end )
/*and f.dim_partid=dp.dim_partid
and dp.partnumber='000791'
and dp.plant='ES20'
and f.foreyear='2017' and f.foremonth='4'
and f.dd_version='A00'*/
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;


update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_forecastnext12mths_prevmonth = b.ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_upddta61_DC b ,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapmonth = b.snapmonth and a.snapyear = b.snapyear
and (ifnull(a.ct_forecastnext12mths_prevmonth,-1.999) <> b.ct_adjsalesplan);

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta61_DC;

/*end update 6.1*/



/* upd 6_1 */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta6_1_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta6_1_DC as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
sum(f1.ct_adjsalesplan)  ct_adjsalesplan,
sum(f1.ct_adjsalesplanfpu)  ct_adjsalesplanfpu,
sum(f1.ct_adjsalesplanbulk)  ct_adjsalesplanbulk
from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear, foremonth,reporting_company_code,country_destination_code,
snapmonth,snapyear, dd_version, ct_adjsalesplan,ct_adjsalesplanfpu,ct_adjsalesplanbulk
   from fact_atlaspharmlogiforecast_merck_DC a
 where  fact_atlaspharmlogiforecast_merckid <> 1
 and dd_source = 'PRA' and dd_version = '0' ) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/   and  f1.foreyear =  f.foreyear+1
 and (f1.snapyear =  case when f.foremonth+1<=12 then f.foreyear else f.foreyear+1 end
and f1.snapmonth = case when f.foremonth+1<=12 then f.foremonth+1 else f.foremonth+1-12 end )
 group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;



update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_forecastnextcalendaryear = b.ct_adjsalesplan,
 a.ct_forecastnextcalendaryearfpu = b.ct_adjsalesplanfpu,
 a.ct_forecastnextcalendaryearbulk = b.ct_adjsalesplanbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta6_1_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapmonth = b.snapmonth and a.snapyear = b.snapyear
and (ifnull(a.ct_forecastnextcalendaryear,-1.999) <> b.ct_adjsalesplan
OR ifnull(a.ct_forecastnextcalendaryearfpu,-1.999) <> b.ct_adjsalesplanfpu
OR ifnull(a.ct_forecastnextcalendaryearbulk,-1.999) <> b.ct_adjsalesplanbulk);

drop table if exists atlas_forecast_budget_upd_DCnextyear;
create table atlas_forecast_budget_upd_DCnextyear as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.budgetqty,0)) as budgetqtynextyear,
sum(ifnull(a2.budgetqtyfpu,0)) as budgetqtyfpunextyear,
sum(ifnull(a2.budgetqtybulk,0)) as budgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,year(b.datevalue) as Yyear,
month(b.datevalue) as mmonth,
budgetqty,
budgetqtyfpu,
budgetqtybulk
 from atlas_forecast_budget_upd_DC a, dim_date b
where a.dim_dateid = b.dim_dateid ) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,year(b.datevalue) as Yyear,
month(b.datevalue) as mmonth,
budgetqty,
budgetqtyfpu,
budgetqtybulk
 from atlas_forecast_budget_upd_DC a, dim_date b
where a.dim_dateid = b.dim_dateid ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;



update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_budgetnextcalendaryear = b.budgetqtynextyear,
 a.ct_budgetnextcalendaryearfpu = b.budgetqtyfpunextyear,
 a.ct_budgetnextcalendaryearbulk = b.budgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyear b ,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
and (ifnull(a.ct_budgetnextcalendaryear,-1.999) <> b.budgetqtynextyear
OR ifnull(a.ct_budgetnextcalendaryearfpu,-1.999) <> b.budgetqtyfpunextyear
OR ifnull(a.ct_budgetnextcalendaryearbulk,-1.999) <> b.budgetqtybulknextyear);


/* upd 7 */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta7_DC;
create table fact_atlaspharmlogiforecast_merck_snp_upddta7_DC as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
sum(f1.ct_adjsalesplan)  ct_adjsalesplan,
sum(f1.ct_adjsalesplanfpu)  ct_adjsalesplanfpu,
sum(f1.ct_adjsalesplanbulk)  ct_adjsalesplanbulk
from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear, foremonth,reporting_company_code,country_destination_code,
snapmonth,snapyear, dd_version, ct_adjsalesplan,ct_adjsalesplanfpu,ct_adjsalesplanbulk
   from fact_atlaspharmlogiforecast_merck_DC a
 where  fact_atlaspharmlogiforecast_merckid <> 1
 and dd_source = 'PRA' and dd_version = '0') f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/  and
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1
 and (f1.snapyear =  f.foreyear and f1.snapmonth = f.foremonth+1 )
 group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;


update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_forecastremaining = b.ct_adjsalesplan,
     a.ct_forecastremainingfpu = b.ct_adjsalesplanfpu,
	 a.ct_forecastremainingbulk = b.ct_adjsalesplanbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta7_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapmonth = b.snapmonth and a.snapyear = b.snapyear
and (ifnull(a.ct_forecastremaining,-1.999) <> b.ct_adjsalesplan
or ifnull(a.ct_forecastremainingfpu,-1.999) <> b.ct_adjsalesplanfpu
or ifnull(a.ct_forecastremainingbulk,-1.999) <> b.ct_adjsalesplanbulk);


/* upd 8*/

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_ytdplusforecast = ifnull(a.ct_Salesdelivactualytd,0) + ifnull(a.ct_forecastremaining,0),
     a.ct_ytdplusforecastfpu = ifnull(a.ct_Salesdelivactualytdfpu,0) + ifnull(a.ct_forecastremainingfpu,0),
	 a.ct_ytdplusforecastbulk = ifnull(a.ct_Salesdelivactualytdbulk,0) + ifnull(a.ct_forecastremainingbulk,0)
 where (ifnull(a.ct_ytdplusforecast,-1.999) <> ifnull(a.ct_Salesdelivactualytd,0) + ifnull(a.ct_forecastremaining,0)
 or ifnull(a.ct_ytdplusforecastfpu,-1.999) <> ifnull(a.ct_Salesdelivactualytdfpu,0) + ifnull(a.ct_forecastremainingfpu,0)
 or ifnull(a.ct_ytdplusforecastbulk,-1.999) <> ifnull(a.ct_Salesdelivactualytdbulk,0) + ifnull(a.ct_forecastremainingbulk,0));
 


 drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta6_DC;
 drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta7_DC;


/*
sales consistency
*/
/*call vectorwise(combine 'fact_atlaspharmlogiforecast_merck_snp_DC')*/

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updfc;
create table fact_atlaspharmlogiforecast_merck_snp_DC_updfc as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version, f.reporting_company_code, f.country_destination_code,
sum(f1.ct_adjsalesplan) ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear,
foremonth,dd_version, ct_adjsalesplan,snapmonth,snapyear,reporting_company_code,country_destination_code
   from fact_atlaspharmlogiforecast_merck_DC a
 where  dd_source = 'PRA'
  and fact_atlaspharmlogiforecast_merckid <> 1) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/   and ((
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear = case when f.foremonth-9>0 then f.foreyear else f.foreyear-1 end
and f1.snapmonth = case when f.foremonth-9>0 then f.foremonth-9 else f.foremonth+3 end)
/*and f.foremonth = 08 and f.dim_partid = 49775 and f.foreyear = 2014*/
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version, f.reporting_company_code, f.country_destination_code;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salesfc9 = b.ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC_updfc b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapyear = b.snapyear and a.snapmonth = b.snapmonth
and ifnull(a.ct_salesfc9 ,-1.999) <> b.ct_adjsalesplan;


/* 8 */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updfc;
create table fact_atlaspharmlogiforecast_merck_snp_DC_updfc as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
sum(f1.ct_adjsalesplan) ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear,
foremonth,dd_version, ct_adjsalesplan,snapmonth,snapyear,reporting_company_code,country_destination_code
   from fact_atlaspharmlogiforecast_merck_DC a
 where  dd_source = 'PRA'
  and fact_atlaspharmlogiforecast_merckid <> 1) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/   and ((
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear = case when f.foremonth-8>0 then f.foreyear else f.foreyear-1 end
and f1.snapmonth = case when f.foremonth-8>0 then f.foremonth-8 else f.foremonth+4 end)
/*and f.foremonth = 08 and f.dim_partid = 49775 and f.foreyear = 2014*/
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version, f.reporting_company_code, f.country_destination_code;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salesfc8 = b.ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC_updfc b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid
 and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapyear = b.snapyear and a.snapmonth = b.snapmonth
and ifnull(a.ct_salesfc8 ,-1.999) <> b.ct_adjsalesplan;

/* 7 */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updfc;
create table fact_atlaspharmlogiforecast_merck_snp_DC_updfc as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
sum(f1.ct_adjsalesplan) ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear,
foremonth,dd_version, ct_adjsalesplan,snapmonth,snapyear,reporting_company_code,country_destination_code
   from fact_atlaspharmlogiforecast_merck_DC a
 where  dd_source = 'PRA'
  and fact_atlaspharmlogiforecast_merckid <> 1) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version*/   and ((
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear = case when f.foremonth-7>0 then f.foreyear else f.foreyear-1 end
and f1.snapmonth = case when f.foremonth-7>0 then f.foremonth-7 else f.foremonth+5 end)
/*and f.foremonth = 08 and f.dim_partid = 49775 and f.foreyear = 2014*/
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version, f.reporting_company_code, f.country_destination_code;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salesfc7 = b.ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC_updfc b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapyear = b.snapyear and a.snapmonth = b.snapmonth
and ifnull(a.ct_salesfc7 ,-1.999) <> b.ct_adjsalesplan;

/* 2 */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updfc;
create table fact_atlaspharmlogiforecast_merck_snp_DC_updfc as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
sum(f1.ct_adjsalesplan) ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear,
foremonth,dd_version, ct_adjsalesplan,snapmonth,snapyear,reporting_company_code,country_destination_code
   from fact_atlaspharmlogiforecast_merck_DC a
 where  dd_source = 'PRA'
  and fact_atlaspharmlogiforecast_merckid <> 1) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version */  and ((
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear = case when f.foremonth-2>0 then f.foreyear else f.foreyear-1 end
and f1.snapmonth = case when f.foremonth-2>0 then f.foremonth-2 else f.foremonth+10 end)
/*and f.foremonth = 08 and f.dim_partid = 49775 and f.foreyear = 2014*/
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version, f.reporting_company_code, f.country_destination_code;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salesfc2 = b.ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC_updfc b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapyear = b.snapyear and a.snapmonth = b.snapmonth
and ifnull(a.ct_salesfc2 ,-1.999) <> b.ct_adjsalesplan;

/* 1 */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updfc;
create table fact_atlaspharmlogiforecast_merck_snp_DC_updfc as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
sum(f1.ct_adjsalesplan) ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear,
foremonth,dd_version, ct_adjsalesplan,snapmonth,snapyear,reporting_company_code,country_destination_code
   from fact_atlaspharmlogiforecast_merck_DC a
 where  dd_source = 'PRA'
  and fact_atlaspharmlogiforecast_merckid <> 1) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
and f.country_destination_code = f1.country_destination_code
 /*and f.dd_version = f1.dd_version */  and ((
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear = case when f.foremonth-1>0 then f.foreyear else f.foreyear-1 end
and f1.snapmonth = case when f.foremonth-1>0 then f.foremonth-1 else f.foremonth+11 end)
/*and f.foremonth = 08 and f.dim_partid = 49775 and f.foreyear = 2014*/
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version, f.reporting_company_code, f.country_destination_code;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salesfc1 = b.ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC_updfc b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapyear = b.snapyear and a.snapmonth = b.snapmonth
and ifnull(a.ct_salesfc1 ,-1.999) <> b.ct_adjsalesplan;


/* */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updfc;
create table fact_atlaspharmlogiforecast_merck_snp_DC_updfc as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
sum(f1.ct_adjsalesplan) ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC f,
(
select distinct dim_plantid, dim_partid, foreyear,
foremonth,dd_version, ct_adjsalesplan,snapmonth,snapyear,reporting_company_code,country_destination_code
   from fact_atlaspharmlogiforecast_merck_DC a
 where  dd_source = 'PRA'
  and fact_atlaspharmlogiforecast_merckid <> 1) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
and f.country_destination_code = f1.country_destination_code
/*and f.dd_version = f1.dd_version */  and ((
 f1.foreyear = f.foreyear and f1.foremonth >= f.foremonth+1) OR
(f1.foremonth <= f.foremonth and f1.foreyear =  f.foreyear+1))
and (f1.snapyear = f.foreyear and f1.snapmonth = f.foremonth)
/*and f.foremonth = 08 and f.dim_partid = 49775 and f.foreyear = 2014*/
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version, f.reporting_company_code, f.country_destination_code;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salesfc = b.ct_adjsalesplan
 from fact_atlaspharmlogiforecast_merck_snp_DC_updfc b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapyear = b.snapyear and a.snapmonth = b.snapmonth
and ifnull(a.ct_salesfc ,-1.999) <> b.ct_adjsalesplan;


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updfc;

/*call vectorwise(combine 'fact_atlaspharmlogiforecast_merck_snp_DC')*/

 /* new NASP */

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_DC u, dim_part pt,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and ifnull(ct_nasp,-1.999)<> nasp;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = 0
from  dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01' and dd.datevalue < '2016-07-01';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01' and dd.datevalue < '2016-07-01'
and ifnull(ct_nasp,-1.999)<> nasp;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = 0
from  dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue < '2016-10-01';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd, fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue < '2016-10-01'
and ifnull(ct_nasp,-1.999)<> nasp;

/* 09 Nov 2016 Georgiana Changes Adding RF4 Data*/

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = 0
from  dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <= '2016-12-01';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd, fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <= '2016-12-01'
and ifnull(ct_nasp,-1.999)<> nasp;


update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_DC u, dim_part pt,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and dd_naspused <> nasp_used;


update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = 'Not Set'
from  dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01' and dd.datevalue < '2016-07-01';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01' and dd.datevalue < '2016-07-01'
and ifnull(dd_naspused,'Not Set')<> nasp_used;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = 'Not Set'
from  dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue < '2016-10-01';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue < '2016-10-01'
and ifnull(dd_naspused,'Not Set')<> nasp_used;

/*09 Nov 2016 Georgiana Changes added RF4 Data*/

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = 'Not Set'
from  dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <= '2016-12-01';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <= '2016-12-01'
and ifnull(dd_naspused,'Not Set')<> nasp_used;
/*9 Nov 2016 End*/


/* IN TESTING */


/*

SFA Contribution

*/
/*call vectorwise(combine 'fact_atlaspharmlogiforecast_merck_snp_DC')*/


update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_salesdeliv2mthsIRU = (ct_Salesmonth1+ct_Salesmonth2);

update fact_atlaspharmlogiforecast_merck_snp_DC f
set productfamily_merck = pt.productfamily_merck
from   dim_part pt,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_partid = pt.dim_partid
and ifnull(f.productfamily_merck,'X') <> pt.productfamily_merck;

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr1;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,
sum(ct_salesdeliv2mthsiru * ct_nasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsiru,productfamily_merck ,ct_nasp
from fact_atlaspharmlogiforecast_merck_snp_DC f
  where   dd_source <> 'PRA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck;


update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salescontribgpf  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsIRU*ct_nasp/ct_salesdeliv2mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where /*a.dim_plantid = b.dim_plantid and */
 a.productfamily_merck = b.productfamily_merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribgpf <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsIRU*ct_nasp/ct_salesdeliv2mthsIRUtot end;



drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dim_plantidsfa,
sum(ct_salesdeliv2mthsiru*ct_nasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ f.dim_plantidsfa, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsiru ,ct_nasp
from fact_atlaspharmlogiforecast_merck_snp_DC f
  where   dd_source <> 'PRA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dim_plantidsfa;




 /*call vectorwise(combine 'fact_atlaspharmlogiforecast_merck_snp_DC')*/




update fact_atlaspharmlogiforecast_merck_snp_DC f
set f.DominantSpeciesDescription_Merck = pt.DominantSpeciesDescription_Merck
from   dim_part pt,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_partid = pt.dim_partid
and ifnull(f.DominantSpeciesDescription_Merck,'X') <> pt.DominantSpeciesDescription_Merck;



drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr3;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck ,
sum(ct_salesdeliv2mthsiru*ct_nasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsiru, f.DominantSpeciesDescription_Merck ,ct_nasp
from fact_atlaspharmlogiforecast_merck_snp_DC f
  where   dd_source <> 'PRA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck;




update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_salescontribspecies  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsIRU*ct_nasp/ct_salesdeliv2mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr3 b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where /*a.dim_plantid = b.dim_plantid and */
 a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribspecies   <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsIRU*ct_nasp/ct_salesdeliv2mthsIRUtot end;

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr1;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr3;



delete from tmp_fact_atlaspharmlogiforecast_merck
where dd_version = 'DTA';


insert into tmp_fact_atlaspharmlogiforecast_merck
(fact_atlaspharmlogiforecast_merckid,dim_dateidforecast, dim_dateidreporting, dim_plantid, dim_partid,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_unitofmeasureidbase,ct_salesdelivered,dd_source, dd_version, dim_companyid,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
ct_forecast9mth1,
ct_forecast9mth2,
ct_forecast9mth3,
ct_forecast6mth1,
ct_forecast6mth2,
ct_forecast3mth,
ct_forecast4mth1,
ct_forecast4mth2,
ct_forecast4bulkmth1,
ct_forecast4bulkmth2,
ct_salesdelivprevyear,
ct_salesdelivprevyearfpu,
ct_salesdelivprevyearbulk,
ct_salesdelivmoving,
ct_salesdelivmovingfpu,
ct_salesdelivmovingbulk,
ct_Salesdelivactualytd,
ct_Salesdelivactualytdfpu,
ct_Salesdelivactualytdbulk,
ct_Salesdelivactualprevytd,
ct_Salesdelivactualprevytdfpu,
ct_Salesdelivactualprevytdbulk,
ct_budgetyear,
ct_budgetyearfpu,
ct_budgetyearbulk,
ct_budgetytd,
ct_budgetytdfpu,
ct_budgetytdbulk,
ct_ytdplusforecast,
ct_ytdplusforecastfpu,
ct_ytdplusforecastbulk,
ct_forecastnext12mths,
ct_forecastnext12mths_prevmonth,
ct_forecastnext12mthsfpu,
ct_forecastnext12mthsbulk,
ct_forecastnext18mths, /* 18 months*/
ct_forecastnext18mthsfpu, /* 18 months*/
ct_forecastnext18mthsbulk, /* 18 months*/
ct_forecastnextcalendaryear,
ct_forecastnextcalendaryearfpu,
ct_forecastnextcalendaryearbulk,
ct_budgetnextcalendaryear,
ct_budgetnextcalendaryearfpu,
ct_budgetnextcalendaryearbulk,
ct_forecastremaining,
ct_forecastremainingfpu,
ct_forecastremainingbulk,
productfamily_merck,
ct_salescontribgpf,
ct_salescontribgpf_partial,
ct_salesdeliv2mthsIRU,
ct_salescontribplant,
ct_salescontribplant_partial,
ct_salescontribspecies,
ct_nasp,
dd_naspused,
ct_salesfc9,
ct_salesfc8,
ct_salesfc7,
ct_salesfc2,
ct_salesfc1,
ct_salesfc,
foremonth,
foreyear,
snapmonth,
snapyear)
select
convert(bigint,1) as fact_atlaspharmlogiforecast_merckid,
dim_dateidreporting as dim_dateidforecast,
dim_dateidreporting, dim_plantid, dim_partid,
ifnull(reporting_company_code,'Not Set') as reporting_company_code,
ifnull(hei_code,'Not Set') as hei_code,
ifnull(country_destination_code,'Not Set') as country_destination_code,
ifnull(market_grouping,'Not Set') as market_grouping,
dim_unitofmeasureidbase,ct_salesdelivered,dd_source,
'DTA' as dd_version, dim_companyid,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ifnull(ct_adjsalesplan,0),
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
ct_forecast9mth1,
ct_forecast9mth2,
ct_forecast9mth3,
ct_forecast6mth1,
ct_forecast6mth2,
ct_forecast3mth,
ct_forecast4mth1,
ct_forecast4mth2,
ct_forecast4bulkmth1,
ct_forecast4bulkmth2,
ifnull(ct_salesdelivprevyear,0),
ifnull(ct_salesdelivprevyearfpu,0),
ifnull(ct_salesdelivprevyearbulk,0),
ifnull(ct_salesdelivmoving,0),
ifnull(ct_salesdelivmovingfpu,0),
ifnull(ct_salesdelivmovingbulk,0),
ifnull(ct_Salesdelivactualytd,0),
ifnull(ct_Salesdelivactualytdfpu,0),
ifnull(ct_Salesdelivactualytdbulk,0),
ifnull(ct_Salesdelivactualprevytd,0),
ifnull(ct_Salesdelivactualprevytdfpu,0),
ifnull(ct_Salesdelivactualprevytdbulk,0),
ifnull(ct_budgetyear,0),
ifnull(ct_budgetyearfpu,0),
ifnull(ct_budgetyearbulk,0),
ifnull(ct_budgetytd,0),
ifnull(ct_budgetytdfpu,0),
ifnull(ct_budgetytdbulk,0),
ifnull(ct_ytdplusforecast,0),
ifnull(ct_ytdplusforecastfpu,0),
ifnull(ct_ytdplusforecastbulk,0),
ifnull(ct_forecastnext12mths,0),
ifnull(ct_forecastnext12mths_prevmonth,0),
ifnull(ct_forecastnext12mthsfpu,0),
ifnull(ct_forecastnext12mthsbulk,0),
ifnull(ct_forecastnext18mths,0), /*18 months*/
ifnull(ct_forecastnext18mthsfpu,0), /*18 months*/
ifnull(ct_forecastnext18mthsbulk,0), /*18 months*/
ifnull(ct_forecastnextcalendaryear,0),
ifnull(ct_forecastnextcalendaryearfpu,0),
ifnull(ct_forecastnextcalendaryearbulk,0),
ifnull(ct_budgetnextcalendaryear,0),
ifnull(ct_budgetnextcalendaryearfpu,0),
ifnull(ct_budgetnextcalendaryearbulk,0),
ifnull(ct_forecastremaining,0),
ifnull(ct_forecastremainingfpu,0),
ifnull(ct_forecastremainingbulk,0),
productfamily_merck,
ifnull(ct_salescontribgpf,0),
ifnull(ct_salescontribgpf_partial,0),
ifnull(ct_salesdeliv2mthsIRU,0),
ifnull(ct_salescontribplant,0),
ifnull(ct_salescontribplant_partial,0),
ifnull(ct_salescontribspecies,0),
ifnull(ct_nasp,0),
dd_naspused,
ifnull(ct_salesfc9,0),
ifnull(ct_salesfc8,0),
ifnull(ct_salesfc7,0),
ifnull(ct_salesfc2,0),
ifnull(ct_salesfc1,0),
ifnull(ct_salesfc,0),
foremonth,
foreyear,
snapmonth,
snapyear
from fact_atlaspharmlogiforecast_merck_snp_DC
where dd_version = 'A00';

drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_allrf_files_allyears;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_allrf_files_allyears as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union /*RF4 Data*/
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
/*RF2 2017 Data*/
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
/*RF3 2017 Data*/
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
UNION
/*RF4 2017 Data*/
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode

union
/*RF2 2018 Data*/
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
/*RF3 2018 Data*/
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
/*RF4 2018 Data*/
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and b.plant = c.plantcode
and a.plant_code = 'NL10'
and b.plant = 'XX20'
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode;



insert into tmp_fact_atlaspharmlogiforecast_merck
(dim_dateidreporting,
  country_destination_code,
  dim_partid,
  dim_plantid,
  dd_version,
  reporting_company_code,
  foremonth,
  foreyear)
select distinct a.dim_dateid as dim_dateidreporting,
                a.country_destination_code,
                a.dim_partid,
                a.dim_plantid,
                'DTA' as dd_version,
				reporting_company_code,
				foremonth,
                foreyear
from tmp_atlas_forecast_pra_forecasts_merck_dc_allrf_files_allyears a
where not exists (
select 1 from tmp_fact_atlaspharmlogiforecast_merck b
where a.dim_dateid = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code
and a.dim_partid = b.dim_partid
and a.dim_plantid = b.dim_plantid
and b.dd_version = 'DTA');


/*This above insert is not treating the NL10 part for which we have version 0 and source PRA*/
/*Added a new insert statement in order to have all materials available for the coresponding reporting dates*/

insert into tmp_fact_atlaspharmlogiforecast_merck
(fact_atlaspharmlogiforecast_merckid,dim_dateidforecast, dim_dateidreporting, dim_plantid, dim_partid,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_unitofmeasureidbase,ct_salesdelivered,dd_source, dd_version, dim_companyid,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
ct_forecast9mth1,
ct_forecast9mth2,
ct_forecast9mth3,
ct_forecast6mth1,
ct_forecast6mth2,
ct_forecast3mth,
ct_forecast4mth1,
ct_forecast4mth2,
ct_forecast4bulkmth1,
ct_forecast4bulkmth2,
ct_salesdelivprevyear,
ct_salesdelivprevyearfpu,
ct_salesdelivprevyearbulk,
ct_salesdelivmoving,
ct_salesdelivmovingfpu,
ct_salesdelivmovingbulk,
ct_Salesdelivactualytd,
ct_Salesdelivactualytdfpu,
ct_Salesdelivactualytdbulk,
ct_Salesdelivactualprevytd,
ct_Salesdelivactualprevytdfpu,
ct_Salesdelivactualprevytdbulk,
ct_budgetyear,
ct_budgetyearfpu,
ct_budgetyearbulk,
ct_budgetytd,
ct_budgetytdfpu,
ct_budgetytdbulk,
ct_ytdplusforecast,
ct_ytdplusforecastfpu,
ct_ytdplusforecastbulk,
ct_forecastnext12mths,
ct_forecastnext12mths_prevmonth,
ct_forecastnext12mthsfpu,
ct_forecastnext12mthsbulk,
ct_forecastnext18mths, /* 18 months*/
ct_forecastnext18mthsfpu, /* 18 months*/
ct_forecastnext18mthsbulk, /* 18 months*/
ct_forecastnextcalendaryear,
ct_forecastnextcalendaryearfpu,
ct_forecastnextcalendaryearbulk,
ct_budgetnextcalendaryear,
ct_budgetnextcalendaryearfpu,
ct_budgetnextcalendaryearbulk,
ct_forecastremaining,
ct_forecastremainingfpu,
ct_forecastremainingbulk,
productfamily_merck,
ct_salescontribgpf,
ct_salescontribgpf_partial,
ct_salesdeliv2mthsIRU,
ct_salescontribplant,
ct_salescontribplant_partial,
ct_salescontribspecies,
ct_nasp,
dd_naspused,
ct_salesfc9,
ct_salesfc8,
ct_salesfc7,
ct_salesfc2,
ct_salesfc1,
ct_salesfc,
foremonth,
foreyear,
snapmonth,
snapyear)
select 
convert(bigint,1) as fact_atlaspharmlogiforecast_merckid,
dim_dateidreporting as dim_dateidforecast,
dim_dateidreporting, dim_plantid, dim_partid,
ifnull(reporting_company_code,'Not Set') as reporting_company_code,
ifnull(hei_code,'Not Set') as hei_code,
ifnull(country_destination_code,'Not Set') as country_destination_code,
ifnull(market_grouping,'Not Set') as market_grouping,
dim_unitofmeasureidbase,ct_salesdelivered,dd_source,
'DTA' as dd_version, dim_companyid,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ifnull(ct_adjsalesplan,0),
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
ct_forecast9mth1,
ct_forecast9mth2,
ct_forecast9mth3,
ct_forecast6mth1,
ct_forecast6mth2,
ct_forecast3mth,
ct_forecast4mth1,
ct_forecast4mth2,
ct_forecast4bulkmth1,
ct_forecast4bulkmth2,
ifnull(ct_salesdelivprevyear,0),
ifnull(ct_salesdelivprevyearfpu,0),
ifnull(ct_salesdelivprevyearbulk,0),
ifnull(ct_salesdelivmoving,0),
ifnull(ct_salesdelivmovingfpu,0),
ifnull(ct_salesdelivmovingbulk,0),
ifnull(ct_Salesdelivactualytd,0),
ifnull(ct_Salesdelivactualytdfpu,0),
ifnull(ct_Salesdelivactualytdbulk,0),
ifnull(ct_Salesdelivactualprevytd,0),
ifnull(ct_Salesdelivactualprevytdfpu,0),
ifnull(ct_Salesdelivactualprevytdbulk,0),
ifnull(ct_budgetyear,0),
ifnull(ct_budgetyearfpu,0),
ifnull(ct_budgetyearbulk,0),
ifnull(ct_budgetytd,0),
ifnull(ct_budgetytdfpu,0),
ifnull(ct_budgetytdbulk,0),
ifnull(ct_ytdplusforecast,0),
ifnull(ct_ytdplusforecastfpu,0),
ifnull(ct_ytdplusforecastbulk,0),
ifnull(ct_forecastnext12mths,0),
ifnull(ct_forecastnext12mths_prevmonth,0),
ifnull(ct_forecastnext12mthsfpu,0),
ifnull(ct_forecastnext12mthsbulk,0),
ifnull(ct_forecastnext18mths,0), /*18 months*/
ifnull(ct_forecastnext18mthsfpu,0), /*18 months*/
ifnull(ct_forecastnext18mthsbulk,0), /*18 months*/
ifnull(ct_forecastnextcalendaryear,0),
ifnull(ct_forecastnextcalendaryearfpu,0),
ifnull(ct_forecastnextcalendaryearbulk,0),
ifnull(ct_budgetnextcalendaryear,0),
ifnull(ct_budgetnextcalendaryearfpu,0),
ifnull(ct_budgetnextcalendaryearbulk,0),
ifnull(ct_forecastremaining,0),
ifnull(ct_forecastremainingfpu,0),
ifnull(ct_forecastremainingbulk,0),
productfamily_merck,
ifnull(ct_salescontribgpf,0),
ifnull(ct_salescontribgpf_partial,0),
ifnull(ct_salesdeliv2mthsIRU,0),
ifnull(ct_salescontribplant,0),
ifnull(ct_salescontribplant_partial,0),
ifnull(ct_salescontribspecies,0),
ifnull(ct_nasp,0),
dd_naspused,
ifnull(ct_salesfc9,0),
ifnull(ct_salesfc8,0),
ifnull(ct_salesfc7,0),
ifnull(ct_salesfc2,0),
ifnull(ct_salesfc1,0),
ifnull(ct_salesfc,0),
foremonth,
foreyear,
snapmonth,
snapyear
from fact_atlaspharmlogiforecast_merck_snp_DC a
where dd_version = '0' and dd_source='PRA' 
and not exists ( select 1 from tmp_fact_atlaspharmlogiforecast_merck b
where a.dim_partid=b.dim_partid
and a.dim_plantid=b.dim_plantid
and a.dim_dateidreporting=b.dim_dateidreporting
/*and a.reporting_company_code=b.reporting_company_code*/
and a.country_destination_code=b.country_destination_code
and b.dd_version='DTA');

update tmp_fact_atlaspharmlogiforecast_merck a
set
dim_dateidsnapshot = ifnull(dim_Dateid,1)
from tmp_fact_atlaspharmlogiforecast_merck a
Inner join dim_company dc on dc.dim_companyid = a.dim_companyid
inner join dim_plant pl on pl.dim_plantid=a.dim_plantid
inner join dim_date d on  dc.companycode = d.companycode and pl.plantcode = d.plantcode_factory
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and month(d.datevalue) = snapmonth and year(d.datevalue) = snapyear
and dayofmonth=1;


/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/

update tmp_fact_atlaspharmlogiforecast_merck  set amt_exchangerate_gbl =1
where  amt_exchangerate_gbl <>1;

update tmp_fact_atlaspharmlogiforecast_merck  set amt_exchangerate =1
where amt_exchangerate <>1;



update tmp_fact_atlaspharmlogiforecast_merck
set dd_forecast9mthisnull = 1
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast9mth1 is null and ct_forecast9mth2 is null
and ct_forecast9mth3 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set dd_forecast6mthisnull = 1
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast6mth1 is null and ct_forecast6mth2 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set dd_forecast3mthisnull = 1
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast3mth is null;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_forecast9mth1 = 0
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast9mth1 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_forecast9mth2 = 0
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast9mth2 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_forecast9mth3 = 0
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast9mth3 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_forecast6mth1 = 0
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast6mth1 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_forecast6mth2 = 0
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast6mth2 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_forecast3mth = 0
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast3mth is null;

update tmp_fact_atlaspharmlogiforecast_merck
set dd_forecast4mthisnull = 1
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast4mth1 is null and ct_forecast4mth2 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set dd_forecast4mthis1null = 1
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast4mth1 is null;

update tmp_fact_atlaspharmlogiforecast_merck
set dd_forecast4mthis2null = 1
where fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and ct_forecast4mth2 is null;


---------


drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,
sum(ct_salescontribgpf_part) ct_salescontribgpf_part from
(
select distinct  f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and dd_forecast9mthisnull =1 then 0
else ct_salescontribgpf
end as ct_salescontribgpf_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'DTA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribgpf_partial  = ct_salescontribgpf_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  a.productfamily_merck = b.productfamily_merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = 'DTA' /*and dd_source <> 'PRA'*/
and a.ct_salescontribgpf_partial   <> ct_salescontribgpf_part;



/* Plant partial contrib */

drop table if exists fact_atlaspharmlogiforecast_merck_updContr4;
 create table fact_atlaspharmlogiforecast_merck_updContr4 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dim_plantidsfa,
sum(ct_salescontribplant_part) ct_salescontribplant_part from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ f.dim_plantidsfa,  f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and dd_forecast9mthisnull =1 then 0
else ct_salescontribplant
end as ct_salescontribplant_part ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'DTA' and dd_source <> 'PRA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dim_plantidsfa;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partial  = ct_salescontribplant_part
 from fact_atlaspharmlogiforecast_merck_updContr4 b,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'DTA'
and a.ct_salescontribplant_partial <> ct_salescontribplant_part;


 drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
drop table if exists fact_atlaspharmlogiforecast_merck_updContr4;



-----------------
/* NEW contrib */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr1;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 as
select   f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid,*/ dim_plantidsfa, productfamily_merck,
sum(ct_salesdeliv2mthsiru * ct_nasp) ct_salesdelivgpftotal from
tmp_fact_atlaspharmlogiforecast_merck f1
  where  dd_version = 'DTA' and
fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 group by  f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid,*/ dim_plantidsfa, productfamily_merck;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttogpf  = case when b.ct_salesdelivgpftotal=0 then 0.00 else
 (100*ct_salesdeliv2mthsiru * ct_nasp)/b.ct_salesdelivgpftotal end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 b,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */ a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear
 and a.foremonth = b.foremonth
and a.productfamily_merck =b.productfamily_merck
and a.dd_version = b.dd_version and  a.dd_version = 'DTA'
and a.ct_salescontribplanttogpf  <>
case when b.ct_salesdelivgpftotal=0 then 0.00 else
 (100*ct_salesdeliv2mthsiru * ct_nasp)/b.ct_salesdelivgpftotal end;
 /*
call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/


drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck, /*dim_plantid,*/ dim_plantidsfa, dd_version,
sum(ct_salescontribplanttogpf_part) ct_salescontribplanttogpf_part from
(
select distinct  f.dim_partid, /*f.dim_plantid,*/ f.dim_plantidsfa, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and
dd_forecast9mthisnull =1  and
dd_forecast9mthisnull =1 and
(  (CASE WHEN (ct_forecast3mth=0) AND (ct_salesmonth1=0) THEN (1.00)
WHEN (ct_forecast3mth=0) THEN (0.00)
WHEN (1-cast(abs(ct_forecast3mth-ct_salesmonth1)/case when ct_forecast3mth=0 then 1 else ct_forecast3mth end as DECIMAL (18,4)))>1 THEN (0.00)
WHEN (1-cast(abs(ct_forecast3mth-ct_salesmonth1)/case when ct_forecast3mth=0 then 1 else ct_forecast3mth end as DECIMAL (18,4)))<0 THEN (0.00)
ELSE (1-cast(abs(ct_forecast3mth-ct_salesmonth1)/case when ct_forecast3mth=0 then 1 else ct_forecast3mth end as DECIMAL (18,4))) END)+
(CASE WHEN (ct_forecast6mth1+ct_forecast6mth2=0 AND ct_salesmonth1+ct_salesmonth2=0) THEN (1.00)
WHEN (ct_forecast6mth1+ct_forecast6mth2=0) THEN (0.00) WHEN (1-
cast(abs(ct_forecast6mth1+ct_forecast6mth2-ct_salesmonth1-ct_salesmonth2)/(case when ct_forecast6mth2+
ct_forecast6mth1 =0 then 1 else ct_forecast6mth2+
ct_forecast6mth1 end
) as DECIMAL (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(ct_forecast6mth1+ct_forecast6mth2-ct_salesmonth1-
ct_salesmonth2)/(case when ct_forecast6mth2+
ct_forecast6mth1 =0 then 1 else ct_forecast6mth2+
ct_forecast6mth1 end) as DECIMAL (18,4))) <0 THEN (0.00) ELSE (1-cast(abs(
ct_forecast6mth1+ct_forecast6mth2-ct_salesmonth1-ct_salesmonth2)/(case when ct_forecast6mth2+
ct_forecast6mth1 =0 then 1 else ct_forecast6mth2+
ct_forecast6mth1 end
) as DECIMAL (18,4))) END)
+
(CASE WHEN (ct_forecast9mth1+ct_forecast9mth2+ct_forecast9mth3)=0 AND (ct_salesmonth1+
ct_salesmonth2+ct_salesmonth3)=0 THEN (1.00) WHEN (ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3=0) THEN (0.00) WHEN (1-cast(abs(ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3-ct_salesmonth1-ct_salesmonth2-ct_salesmonth3)/(case when ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3 =0 then 1 else ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3 end) as DECIMAL (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3-ct_salesmonth1-ct_salesmonth2-ct_salesmonth3)/(case when ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3 =0 then 1 else ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3 end) as DECIMAL (18,4))) <0 THEN (0.00) ELSE (1-cast(abs(ct_forecast9mth1+
ct_forecast9mth2+ct_forecast9mth3-ct_salesmonth1-ct_salesmonth2-ct_salesmonth3)/(
case when ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3 =0 then 1 else ct_forecast9mth1+ct_forecast9mth2+
ct_forecast9mth3 end) as DECIMAL (18,4))) END)
) = 0
then 0
else ct_salescontribplanttogpf
end as ct_salescontribplanttogpf_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'DTA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,/*dim_plantid,*/ dim_plantidsfa, dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttogpf_partial  = ct_salescontribplanttogpf_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  a.productfamily_merck = b.productfamily_merck and
  /*a.dim_plantid = b.dim_plantid */ a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and a.dd_version = 'DTA'
and ct_salescontribplanttogpf_partial   <> ct_salescontribplanttogpf_part;

/* Contribution Global */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr1;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,
sum(ct_salesdeliv2mthsiru * ct_nasp) ct_salesdelivglobal_tot from
(
select distinct  f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsiru,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_source <> 'PRA' and  dd_version = 'DTA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribglobal  =
 cast(100*ct_salesdeliv2mthsiru * ct_nasp/case when b.ct_salesdelivglobal_tot =0 then 1.00000 else b.ct_salesdelivglobal_tot end as decimal(18,5))
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  a.dd_version = 'DTA'
and a.ct_salescontribglobal  <>
  cast(100*ct_salesdeliv2mthsiru * ct_nasp/case when b.ct_salesdelivglobal_tot =0 then 1.00000 else b.ct_salesdelivglobal_tot end as decimal(18,5));



drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, dd_version,
sum(ct_salescontribglobal_part) ct_salescontribglobal_part from
(
select distinct  f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and dd_forecast9mthisnull =1 then 0
else ct_salescontribglobal
end as ct_salescontribglobal_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'DTA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,f1.foremonth, f1.foreyear,dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribglobal_partial  = ct_salescontribglobal_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and  a.dd_version = 'DTA'
and ct_salescontribglobal_partial   <> ct_salescontribglobal_part;


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_upddta7;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_upddta6;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updcontr3;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updcontr2;
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updcontr1;
drop table if exists fact_atlaspharmlogiforecast_merck_updcontr3;

 update tmp_fact_atlaspharmlogiforecast_merck f
 set f.ct_sfatarget = s.sfatarget
 from  dim_plant pl,  sfaplanttargets_merck s,tmp_fact_atlaspharmlogiforecast_merck f
  where fact_atlaspharmlogiforecast_merckid =1 and  dd_version = 'DTA'
  and f.dim_plantid = pl.dim_plantid and pl.plantcode = s.plant
  and f.ct_sfatarget <> s.sfatarget;

/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/


/*Andrian - for SFA using <0 for DTA no*/
/*drop table if exists tmp_atlas_forecast_SalesUpd_DC
create table tmp_atlas_forecast_SalesUpd_DC as
select distinct pt.dim_partid, pl.dim_plantid,
reporting_company_code,
country_destination_code,
month(dt.datevalue) foremonth, year(dt.datevalue) foreyear,
dt.datevalue , case when BUoM_Quantity <0 then 0 else BUoM_Quantity end ct_salesdelivered,
case when BUoM_Quantityfpu <0 then 0 else BUoM_Quantityfpu end  ct_salesdeliveredfpu,
case when BUoM_Quantitybulk <0 then 0 else BUoM_Quantitybulk end  ct_salesdeliveredbulk
from atlas_forecast_sales_merck_DC s,
dim_plant pl, dim_part pt, dim_date dt
where pl.plantcode = s.sales_cocd and pt.plant = pl.plantcode
and lpad(pt.partnumber,6,'0') = lpad(s.sales_uin,6,'0')
and dt.datevalue = to_date(case when sales_reporting_period is null then '00010101' else concat(sales_reporting_period,'01') end,'YYYYMMDD') and pl.plantcode = dt.plantcode_factory
and pl.companycode = dt.companycode*/

/*Ambiguous Replace fix*/
/*
update fact_atlaspharmlogiforecast_merck_snp_DC a
set a.ct_salesdelivered =  b.ct_salesdelivered ,
    a.ct_salesdeliveredfpu =  b.ct_salesdeliveredfpu,
	a.ct_salesdeliveredbulk = b.ct_salesdeliveredbulk
from  (select distinct b.dim_plantid,b.dim_partid,b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,max(b.ct_salesdelivered) ct_salesdelivered,max(b.ct_salesdeliveredfpu) ct_salesdeliveredfpu,max(b.ct_salesdeliveredbulk) ct_salesdeliveredbulk
from  tmp_atlas_forecast_SalesUpd_DC b
group by  b.dim_plantid,b.dim_partid,b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear ) b
,fact_atlaspharmlogiforecast_merck_snp_DC a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
and a.foremonth = b.foremonth and a.foreyear = b.foreyear
and ifnull(a.ct_salesdelivered,-1.999) <> b.ct_salesdelivered*/

/*drop table if exists tmp_atlas_forecast_SalesUpd_DC*/

update fact_atlaspharmlogiforecast_merck_snp_DC
set ct_nasp = 0
where ct_nasp <> 0;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_DC u, dim_part pt,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and ifnull(ct_nasp,-1.999)<> nasp;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = 0
from  dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue <'2016-10-01';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue <'2016-10-01'
and ifnull(ct_nasp,-1.999)<> nasp;

/*09 Nov 2016 Added RF4 Data*/

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = 0
from  dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <= '2016-12-01';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <= '2016-12-01'
and ifnull(ct_nasp,-1.999)<> nasp;

/* 09 Nov 2016 End*/

update fact_atlaspharmlogiforecast_merck_snp_DC
set dd_naspused = 'Not Set'
where dd_naspused <> 'Not Set';

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_DC u, dim_part pt,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and dd_naspused <> nasp_used;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue < '2016-10-01'
and ifnull(dd_naspused,'Not Set')<> nasp_used;

/*RF4 Data*/

update fact_atlaspharmlogiforecast_merck_snp_DC f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd,fact_atlaspharmlogiforecast_merck_snp_DC f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <= '2016-12-01'
and ifnull(dd_naspused,'Not Set')<> nasp_used;



drop table if exists fact_atlaspharmlogiforecast_merck_upd1_DC;
create table fact_atlaspharmlogiforecast_merck_upd1_DC as
select  distinct snapmonth,  snapyear, dim_partid, dim_plantid,
reporting_company_code,country_destination_code,
a.datevalue,ct_salesdelivered,foreyear,foremonth from
fact_atlaspharmlogiforecast_merck_snp_DC b, dim_date a
where a.dim_dateid = b.dim_dateidreporting;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_SalesMonth1 = b.ct_salesdelivered
 from fact_atlaspharmlogiforecast_merck_upd1_DC b, fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.foremonth = a.foremonth
	 and b.foreyear = a.foreyear
--	and b.snapmonth = a.snapmonth and b.snapyear = a.snapyear
	and ifnull(a.ct_SalesMonth1,-1.999) <> b.ct_salesdelivered;

drop table if exists fact_atlaspharmlogiforecast_merck_upd1_DC;


drop table if exists fact_atlaspharmlogiforecast_merck_upd2_DC;
create table fact_atlaspharmlogiforecast_merck_upd2_DC as
select distinct snapmonth,  snapyear, dim_partid, dim_plantid,
reporting_company_code,country_destination_code,
a.datevalue,ct_salesdelivered,
case when b.foremonth+1<=12 then b.foremonth+1 else b.foremonth+1-12 end foremonth,
case when b.foremonth+1<=12 then b.foreyear else b.foreyear+1 end foreyear
 from
fact_atlaspharmlogiforecast_merck_snp_DC b, dim_date a
where a.dim_dateid = b.dim_dateidreporting;

update fact_atlaspharmlogiforecast_merck_snp_DC a
 set a.ct_SalesMonth2 = b.ct_salesdelivered
 from fact_atlaspharmlogiforecast_merck_upd2_DC b,fact_atlaspharmlogiforecast_merck_snp_DC a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
  and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.foremonth = a.foremonth
	 and b.foreyear = a.foreyear --and b.snapmonth = a.snapmonth
	-- and b.snapyear = a.snapyear
		and ifnull(a.ct_SalesMonth2,-1.999) <> b.ct_salesdelivered;

drop table if exists fact_atlaspharmlogiforecast_merck_upd2_DC;

update fact_atlaspharmlogiforecast_merck_snp_DC f
set ct_salesdeliv2mthsIRU = (ct_Salesmonth1+ct_Salesmonth2);



/* SFA */


delete from tmp_fact_atlaspharmlogiforecast_merck
where dd_version = 'SFA';


insert into tmp_fact_atlaspharmlogiforecast_merck
(fact_atlaspharmlogiforecast_merckid,dim_dateidforecast, dim_dateidreporting, dim_plantid, dim_partid,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_unitofmeasureidbase,ct_salesdelivered,dd_source, dd_version, dim_companyid, dim_plantidsfa,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
ct_forecast9mth1,
ct_forecast9mth2,
ct_forecast9mth3,
ct_forecast6mth1,
ct_forecast6mth2,
ct_forecast4mth1,
ct_forecast4mth2,
ct_forecast3mth,
ct_salesdelivprevyear,
ct_salesdelivprevyearfpu,
ct_salesdelivprevyearbulk,
ct_salesdelivmoving,
ct_salesdelivmovingfpu,
ct_salesdelivmovingbulk,
ct_Salesdelivactualytd,
ct_Salesdelivactualytdfpu,
ct_Salesdelivactualytdbulk,
ct_Salesdelivactualprevytd,
ct_Salesdelivactualprevytdfpu,
ct_Salesdelivactualprevytdbulk,
ct_budgetyear,
ct_budgetyearfpu,
ct_budgetyearbulk,
ct_budgetytd,
ct_budgetytdfpu,
ct_budgetytdbulk,
ct_ytdplusforecast,
ct_ytdplusforecastfpu,
ct_ytdplusforecastbulk,
ct_forecastnext12mths,
ct_forecastnext12mths_prevmonth,
ct_forecastnext12mthsfpu,
ct_forecastnext12mthsbulk,
ct_forecastnext18mths, /* 18 months*/
ct_forecastnext18mthsfpu, /* 18 months*/
ct_forecastnext18mthsbulk, /* 18 months*/
ct_forecastnextcalendaryear,
ct_forecastnextcalendaryearfpu,
ct_forecastnextcalendaryearbulk,
ct_budgetnextcalendaryear,
ct_budgetnextcalendaryearfpu,
ct_budgetnextcalendaryearbulk,
ct_forecastremaining,
ct_forecastremainingfpu,
ct_forecastremainingbulk,
productfamily_merck,
ct_salescontribgpf,
ct_salescontribgpf_partial,
ct_salesdeliv2mthsIRU,
ct_salescontribplant,
ct_salescontribplant_partial,
ct_salescontribspecies,
ct_nasp,
dd_naspused,
ct_salesfc9,
ct_salesfc8,
ct_salesfc7,
ct_salesfc2,
ct_salesfc1,
ct_salesfc,
dd_forecast9mthisnull,
dd_forecast6mthisnull,
dd_forecast4mthisnull,
dd_forecast4mthis1null,
dd_forecast4mthis2null,
dd_forecast3mthisnull,
DominantSpeciesDescription_Merck,
ct_salesdeliv2mthsnasp,
foremonth,
foreyear,
snapmonth,
snapyear)
select
convert(bigint,1) as fact_atlaspharmlogiforecast_merckid,
dim_dateidreporting as dim_dateidforecast,
dim_dateidreporting, dim_plantid, dim_partid,
ifnull(reporting_company_code,'Not Set'),
'Not Set' as hei_code,
ifnull(country_destination_code,'Not Set') as country_destination_code,
'Not Set' as market_grouping,
dim_unitofmeasureidbase,sum(ct_salesdelivered),dd_source,
'SFA' as dd_version, dim_companyid, dim_plantidsfa,
sum(ct_salesrequested),
sum(ct_salesforecast),
sum(ct_promotion),
sum(ct_adjforlogistics),
sum(ct_totaladj),
sum(ifnull(ct_adjsalesplan,0)),
sum(ct_salesreqmod),
sum(ct_adjsalesreq),
sum(ifnull(ct_Salesbudget,0)),
sum(ifnull(ct_salesmonth1,0)),
sum(ifnull(ct_salesmonth2,0)),
sum(ifnull(ct_salesmonth3,0)),
max(ifnull(ct_forecast9mth1,0)),
max(ifnull(ct_forecast9mth2,0)),
max(ifnull(ct_forecast9mth3,0)),
max(ifnull(ct_forecast6mth1,0)),
max(ifnull(ct_forecast6mth2,0)),
max(ifnull(ct_forecast4mth1,0)),
max(ifnull(ct_forecast4mth2,0)),
max(ifnull(ct_forecast3mth,0)),
sum(ifnull(ct_salesdelivprevyear,0)),
sum(ifnull(ct_salesdelivprevyearfpu,0)),
sum(ifnull(ct_salesdelivprevyearbulk,0)),
sum(ifnull(ct_salesdelivmoving,0)),
sum(ifnull(ct_salesdelivmovingfpu,0)),
sum(ifnull(ct_salesdelivmovingbulk,0)),
sum(ifnull(ct_Salesdelivactualytd,0)),
sum(ifnull(ct_Salesdelivactualytdfpu,0)),
sum(ifnull(ct_Salesdelivactualytdbulk,0)),
sum(ifnull(ct_Salesdelivactualprevytd,0)),
sum(ifnull(ct_Salesdelivactualprevytdfpu,0)),
sum(ifnull(ct_Salesdelivactualprevytdbulk,0)),
sum(ifnull(ct_budgetyear,0)),
sum(ifnull(ct_budgetyearfpu,0)),
sum(ifnull(ct_budgetyearbulk,0)),
sum(ifnull(ct_budgetytd,0)),
sum(ifnull(ct_budgetytdfpu,0)),
sum(ifnull(ct_budgetytdbulk,0)),
sum(ifnull(ct_ytdplusforecast,0)),
sum(ifnull(ct_ytdplusforecastfpu,0)),
sum(ifnull(ct_ytdplusforecastbulk,0)),
sum(ifnull(ct_forecastnext12mths,0)),
sum(ifnull(ct_forecastnext12mths_prevmonth,0)),
sum(ifnull(ct_forecastnext12mthsfpu,0)),
sum(ifnull(ct_forecastnext12mthsbulk,0)),
sum(ifnull(ct_forecastnext18mths,0)), /* 18 months*/
sum(ifnull(ct_forecastnext18mthsfpu,0)), /* 18 months*/
sum(ifnull(ct_forecastnext18mthsbulk,0)), /* 18 months*/
sum(ifnull(ct_forecastnextcalendaryear,0)),
sum(ifnull(ct_forecastnextcalendaryearfpu,0)),
sum(ifnull(ct_forecastnextcalendaryearbulk,0)),
sum(ifnull(ct_budgetnextcalendaryear,0)),
sum(ifnull(ct_budgetnextcalendaryearfpu,0)),
sum(ifnull(ct_budgetnextcalendaryearbulk,0)),
sum(ifnull(ct_forecastremaining,0)),
sum(ifnull(ct_forecastremainingfpu,0)),
sum(ifnull(ct_forecastremainingbulk,0)),
productfamily_merck,
sum(ifnull(ct_salescontribgpf,0)),
avg(ifnull(ct_salescontribgpf_partial,0)),
sum(ifnull(ct_salesdeliv2mthsIRU,0)),
sum(ifnull(ct_salescontribplant,0)),
avg(ifnull(ct_salescontribplant_partial,0)),
sum(ifnull(ct_salescontribspecies,0)),
avg(ifnull(ct_nasp,0)),
max(dd_naspused) as dd_naspused,
sum(ifnull(ct_salesfc9,0)),
sum(ifnull(ct_salesfc8,0)),
sum(ifnull(ct_salesfc7,0)),
sum(ifnull(ct_salesfc2,0)),
sum(ifnull(ct_salesfc1,0)),
sum(ifnull(ct_salesfc,0)),
min(case when ct_forecast9mth1+ct_forecast9mth2+ct_forecast9mth3 is null then 1 else 0 end) as dd_forecast9mthisnull,
min(case when ct_forecast6mth1+ct_forecast6mth2 is null then 1 else 0 end) as dd_forecast6mthisnull,
min(case when ct_forecast4mth1+ct_forecast4mth2 is null then 1 else 0 end) as dd_forecast4mthisnull,
min(case when ct_forecast4mth1 is null then 1 else 0 end) as dd_forecast4mthis1null,
min(case when ct_forecast4mth2 is null then 1 else 0 end) as dd_forecast4mthis2null,
min(case when ct_forecast3mth is null then 1 else 0 end) as  dd_forecast3mthisnull,
DominantSpeciesDescription_Merck,
sum(ifnull(ct_salesdeliv2mthsIRU*ct_nasp,0)) as ct_salesdeliv2mthsnasp,
foremonth,
foreyear,
snapmonth,
snapyear
from fact_atlaspharmlogiforecast_merck_snp_DC
where dd_version = 'A00' and dd_source = 'S991'
and dim_plantid not in  (select dim_plantid from dim_plant where  plantcode = 'NL10')
group by dim_dateidreporting, dim_plantid, dim_partid,dim_plantidsfa,
foremonth, foreyear,snapmonth,snapyear,
productfamily_merck,dim_unitofmeasureidbase,dd_source,
dim_companyid,DominantSpeciesDescription_Merck,
ifnull(reporting_company_code,'Not Set'),
ifnull(hei_code,'Not Set'),
ifnull(country_destination_code,'Not Set'),
ifnull(market_grouping,'Not Set');

--insert NL10



insert into tmp_fact_atlaspharmlogiforecast_merck
(fact_atlaspharmlogiforecast_merckid,dim_dateidforecast, dim_dateidreporting, dim_plantid, dim_partid,
reporting_company_code,hei_code,country_destination_code,market_grouping,
dim_unitofmeasureidbase,ct_salesdelivered,dd_source, dd_version, dim_companyid, dim_plantidsfa,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
ct_forecast9mth1,
ct_forecast9mth2,
ct_forecast9mth3,
ct_forecast6mth1,
ct_forecast6mth2,
ct_forecast4mth1,
ct_forecast4mth2,
ct_forecast4bulkmth1,
ct_forecast4bulkmth2,
ct_forecast3mth,
ct_salesdelivprevyear,
ct_salesdelivprevyearfpu,
ct_salesdelivprevyearbulk,
ct_salesdelivmoving,
ct_salesdelivmovingfpu,
ct_salesdelivmovingbulk,
ct_Salesdelivactualytd,
ct_Salesdelivactualytdfpu,
ct_Salesdelivactualytdbulk,
ct_Salesdelivactualprevytd,
ct_Salesdelivactualprevytdfpu,
ct_Salesdelivactualprevytdbulk,
ct_budgetyear,
ct_budgetyearfpu,
ct_budgetyearbulk,
ct_budgetytd,
ct_budgetytdfpu,
ct_budgetytdbulk,
ct_ytdplusforecast,
ct_ytdplusforecastfpu,
ct_ytdplusforecastbulk,
ct_forecastnext12mths,
ct_forecastnext12mths_prevmonth,
ct_forecastnext12mthsfpu,
ct_forecastnext12mthsbulk,
ct_forecastnext18mths, /* 18 months*/
ct_forecastnext18mthsfpu, /* 18 months*/
ct_forecastnext18mthsbulk, /* 18 months*/
ct_forecastnextcalendaryear,
ct_forecastnextcalendaryearfpu,
ct_forecastnextcalendaryearbulk,
ct_budgetnextcalendaryear,
ct_budgetnextcalendaryearfpu,
ct_budgetnextcalendaryearbulk,
ct_forecastremaining,
ct_forecastremainingfpu,
ct_forecastremainingbulk,
productfamily_merck,
ct_salescontribgpf,
ct_salescontribgpf_partial,
ct_salesdeliv2mthsIRU,
ct_salescontribplant,
ct_salescontribplant_partial,
ct_salescontribspecies,
ct_nasp,
dd_naspused,
ct_salesfc9,
ct_salesfc8,
ct_salesfc7,
ct_salesfc2,
ct_salesfc1,
ct_salesfc,
DominantSpeciesDescription_Merck,
dd_forecast9mthisnull,
dd_forecast6mthisnull,
dd_forecast4mthisnull,
dd_forecast4mthis1null,
dd_forecast4mthis2null,
dd_forecast3mthisnull,
ct_salesdeliv2mthsnasp,
foremonth,
foreyear,
snapmonth,
snapyear)
select
convert(bigint,1) as fact_atlaspharmlogiforecast_merckid,
dim_dateidreporting as dim_dateidforecast,
dim_dateidreporting, dim_plantid, dim_partid,
ifnull(reporting_company_code,'Not Set'),
'Not Set' as hei_code,
ifnull(country_destination_code,'Not Set'),
ifnull(market_grouping,'Not Set'),
dim_unitofmeasureidbase,ct_salesdelivered,dd_source,
'SFA' as dd_version, dim_companyid, dim_plantidsfa,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ifnull(ct_adjsalesplan,0),
ct_salesreqmod,
ct_adjsalesreq,
ct_Salesbudget,
ct_salesmonth1,
ct_salesmonth2,
ct_salesmonth3,
ct_forecast9mth1,
ct_forecast9mth2,
ct_forecast9mth3,
ct_forecast6mth1,
ct_forecast6mth2,
ct_forecast4mth1,
ct_forecast4mth2,
ct_forecast4bulkmth1,
ct_forecast4bulkmth2,
ct_forecast3mth,
ifnull(ct_salesdelivprevyear,0),
ifnull(ct_salesdelivprevyearfpu,0),
ifnull(ct_salesdelivprevyearbulk,0),
ifnull(ct_salesdelivmoving,0),
ifnull(ct_salesdelivmovingfpu,0),
ifnull(ct_salesdelivmovingbulk,0),
ifnull(ct_Salesdelivactualytd,0),
ifnull(ct_Salesdelivactualytdfpu,0),
ifnull(ct_Salesdelivactualytdbulk,0),
ifnull(ct_Salesdelivactualprevytd,0),
ifnull(ct_Salesdelivactualprevytdfpu,0),
ifnull(ct_Salesdelivactualprevytdbulk,0),
ifnull(ct_budgetyear,0),
ifnull(ct_budgetyearfpu,0),
ifnull(ct_budgetyearbulk,0),
ifnull(ct_budgetytd,0),
ifnull(ct_budgetytdfpu,0),
ifnull(ct_budgetytdbulk,0),
ifnull(ct_ytdplusforecast,0),
ifnull(ct_ytdplusforecastfpu,0),
ifnull(ct_ytdplusforecastbulk,0),
ifnull(ct_forecastnext12mths,0),
ifnull(ct_forecastnext12mths_prevmonth,0),
ifnull(ct_forecastnext12mthsfpu,0),
ifnull(ct_forecastnext12mthsbulk,0),
ifnull(ct_forecastnext18mths,0), /*18 months*/
ifnull(ct_forecastnext18mthsfpu,0), /*18 months*/
ifnull(ct_forecastnext18mthsbulk,0), /*18 months*/
ifnull(ct_forecastnextcalendaryear,0),
ifnull(ct_forecastnextcalendaryearfpu,0),
ifnull(ct_forecastnextcalendaryearbulk,0),
ifnull(ct_budgetnextcalendaryear,0),
ifnull(ct_budgetnextcalendaryearfpu,0),
ifnull(ct_budgetnextcalendaryearbulk,0),
ifnull(ct_forecastremaining,0),
ifnull(ct_forecastremainingfpu,0),
ifnull(ct_forecastremainingbulk,0),
productfamily_merck,
ifnull(ct_salescontribgpf,0),
ifnull(ct_salescontribgpf_partial,0),
ifnull(ct_salesdeliv2mthsIRU,0),
ifnull(ct_salescontribplant,0),
ifnull(ct_salescontribplant_partial,0),
ifnull(ct_salescontribspecies,0),
ifnull(ct_nasp,0),
dd_naspused,
ifnull(ct_salesfc9,0),
ifnull(ct_salesfc8,0),
ifnull(ct_salesfc7,0),
ifnull(ct_salesfc2,0),
ifnull(ct_salesfc1,0),
ifnull(ct_salesfc,0),
DominantSpeciesDescription_Merck,
(case when ct_forecast9mth1+ct_forecast9mth2+ct_forecast9mth3 is null then 1 else 0 end) as dd_forecast9mthisnull,
(case when ct_forecast6mth1+ct_forecast6mth2 is null then 1 else 0 end) as dd_forecast6mthisnull,
(case when ct_forecast4mth1+ct_forecast4mth2 is null then 1 else 0 end) as dd_forecast4mthisnull,
(case when ct_forecast4mth1 is null then 1 else 0 end) as dd_forecast4mthis1null,
(case when ct_forecast4mth2 is null then 1 else 0 end) as dd_forecast4mthis2null,
(case when ct_forecast3mth is null then 1 else 0 end) as  dd_forecast3mthisnull,
ifnull(ct_salesdeliv2mthsIRU*ct_nasp,0) as ct_salesdeliv2mthsnasp,
foremonth,
foreyear,
snapmonth,
snapyear
from fact_atlaspharmlogiforecast_merck_snp_DC
where dd_version = 'A00'
and dim_plantid in  (select dim_plantid from dim_plant where  plantcode = 'NL10')
and dd_source = 'S991';



update tmp_fact_atlaspharmlogiforecast_merck
set ct_salesdeliv2mthsnasp = ct_salesdeliv2mthsiru*ct_nasp
where dd_version  ='SFA'
and dim_plantid in  (select dim_plantid from dim_plant where  plantcode = 'NL10')
and ct_salesdeliv2mthsnasp <> ct_salesdeliv2mthsiru*ct_nasp;





/* Plant contrib */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dim_plantidsfa,
sum(ct_salesdeliv2mthsnasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ f.dim_plantidsfa, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsnasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dim_plantidsfa;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsnasp/ct_salesdeliv2mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2 b,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and dd_version = 'SFA'
and a.ct_salescontribplant  <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsnasp/ct_salesdeliv2mthsIRUtot end;




 drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
drop table if exists fact_atlaspharmlogiforecast_merck_updContr4;





-----------------
/* NEW contrib */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr1;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 as
select   f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid, */ dim_plantidsfa, productfamily_merck,
sum(ct_salesdeliv2mthsnasp) ct_salesdelivgpftotal from
tmp_fact_atlaspharmlogiforecast_merck f1
  where  dd_version = 'SFA' and
fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 group by  f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid, */ dim_plantidsfa, productfamily_merck;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttogpf  = case when b.ct_salesdelivgpftotal=0 then 0.00 else
 (100*ct_salesdeliv2mthsnasp )/b.ct_salesdelivgpftotal end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 b,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear
 and a.foremonth = b.foremonth
and a.productfamily_merck =b.productfamily_merck
and a.dd_version = b.dd_version and  a.dd_version = 'SFA'
and a.ct_salescontribplanttogpf  <>
case when b.ct_salesdelivgpftotal=0 then 0.00 else
 (100*ct_salesdeliv2mthsnasp )/b.ct_salesdelivgpftotal end;
/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/

/* Madalina 12 Oct 2016 - Add field Contribution to Species within Plant BI-4115*/
/* NEW contrib for Species*/

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr5;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr5 as
select   f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid,*/ dim_plantidsfa, DominantSpeciesDescription_Merck,
sum(ct_salesdeliv2mthsiru * ct_nasp) ct_salesdelivspeciestotal from
tmp_fact_atlaspharmlogiforecast_merck f1
  where dd_version = 'SFA' and 
fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 group by  f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid,*/ dim_plantidsfa, DominantSpeciesDescription_Merck;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttospecies  = case when b.ct_salesdelivspeciestotal=0 then 0.00 else
 (100*ct_salesdeliv2mthsiru * ct_nasp)/b.ct_salesdelivspeciestotal end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr5 b,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */ a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear
 and a.foremonth = b.foremonth
and a.DominantSpeciesDescription_Merck =b.DominantSpeciesDescription_Merck
and a.dd_version = b.dd_version  and  a.dd_version = 'SFA' 
and a.ct_salescontribplanttospecies  <>
case when b.ct_salesdelivspeciestotal=0 then 0.00 else
 (100*ct_salesdeliv2mthsiru * ct_nasp)/b.ct_salesdelivspeciestotal end;

/* END BI-4115*/



/* Contribution Global */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr1;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,
sum(ct_salesdeliv2mthsnasp ) ct_salesdelivglobal_tot from
(
select distinct  f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsnasp,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_source <> 'PRA' and  dd_version = 'SFA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribglobal  =
 cast(100*ct_salesdeliv2mthsnasp /case when b.ct_salesdelivglobal_tot =0 then 1.00000 else b.ct_salesdelivglobal_tot end as decimal (18,5))
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  a.dd_version = 'SFA'
and a.ct_salescontribglobal  <>
  cast(100*ct_salesdeliv2mthsnasp /case when b.ct_salesdelivglobal_tot =0 then 1.00000 else b.ct_salesdelivglobal_tot end as decimal (18,5));





/* Species Contrib */

update tmp_fact_atlaspharmlogiforecast_merck f
set f.DominantSpeciesDescription_Merck = pt.DominantSpeciesDescription_Merck
from   dim_part pt,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_partid = pt.dim_partid and   dd_version = 'SFA'
and ifnull(f.DominantSpeciesDescription_Merck,'X') <> pt.DominantSpeciesDescription_Merck;


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr3;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck ,
sum(ct_salesdeliv2mthsnasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsnasp, f.DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck  f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck;


update tmp_fact_atlaspharmlogiforecast_merck  a
 set a.ct_salescontribspecies  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsnasp/ct_salesdeliv2mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr3 b ,tmp_fact_atlaspharmlogiforecast_merck  a
 where  dd_version = 'SFA' and
 a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribspecies   <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsnasp/ct_salesdeliv2mthsIRUtot end;






   update tmp_fact_atlaspharmlogiforecast_merck f
 set f.ct_sfatarget = s.sfatarget
 from  dim_plant pl,  sfaplanttargets_merck s,tmp_fact_atlaspharmlogiforecast_merck f
  where fact_atlaspharmlogiforecast_merckid =1 and  dd_version = 'SFA'
  and f.dim_plantid = pl.dim_plantid and pl.plantcode = s.plant
  and f.ct_sfatarget <> s.sfatarget;

/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/
/*14 Feb 2017 Georgiana Changes moving the update on market_grouping here as we need to have it correct in the join on dd_region column*/
drop table if exists tmp_market_group_upd;
create table tmp_market_group_upd as
select distinct dim_partid,country_destination_code,market_grouping from tmp_fact_atlaspharmlogiforecast_merck
where (dd_version = 'DTA' or dim_plantid = 124)
and market_grouping <> 'Not Set'
union
select distinct dim_partid,country_destination_code,market_grouping
 from tmp_atlas_forecast_pra_forecasts_merck_dc_allrf_files_allyears
 where market_grouping <> 'Not Set';


/*Ambiguous Replace fix*/

merge into tmp_fact_atlaspharmlogiforecast_merck a
using (
select distinct b.country_destination_code,b.dim_partid, min(b.market_grouping) over (partition by b.country_destination_code,b.dim_partid) as market_grouping
from tmp_market_group_upd b,tmp_fact_atlaspharmlogiforecast_merck a
where a.country_destination_code = b.country_destination_code
and a.market_grouping = 'Not Set'
and (dd_version = 'DTA' or dim_plantid = 124)
and a.dim_partid = b.dim_partid) t on a.country_destination_code = t.country_destination_code and a.dim_partid = t.dim_partid
when matched then update set a.market_grouping = t.market_grouping;
/*14 Feb 2017 END*/
/* 8 Dec 2016 Georgiana changes according to BI-4374 adding dd_region column for market grouping*/
merge into tmp_fact_atlaspharmlogiforecast_merck a
using(
select distinct s.region,s.market_grouping
from region_marketing_grouping s
) t
on t.market_grouping=a.market_grouping
when matched then update set a.dd_region=ifnull(t.region,'Not Set')
where a.dd_region<>ifnull(t.region,'Not Set');


update tmp_fact_atlaspharmlogiforecast_merck
set dd_plantcode = 'Not Set'
where dd_version = 'SFA'
and dd_plantcode <> 'Not Set';

update tmp_fact_atlaspharmlogiforecast_merck
set dd_planttitlemerck = 'Not Set'
where dd_version = 'SFA'
and dd_planttitlemerck <> 'Not Set';

update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_plantcode = b.plantcode
from dim_plant b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantidsfa = b.dim_plantid
and dd_version = 'SFA'
and  a.dd_plantcode <> b.plantcode;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_planttitlemerck = b.planttitle_merck
from dim_plant b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantidsfa = b.dim_plantid
and dd_version = 'SFA'
and  a.dd_planttitlemerck <> b.planttitle_merck;

 update tmp_fact_atlaspharmlogiforecast_merck a
 set a.dd_plantcode = b.plantfordfa_code,
     a.dd_planttitlemerck = b.plantfordfa_title
 from tmp_plantfordfa b,tmp_fact_atlaspharmlogiforecast_merck a
 where dd_plantcode in ('NL10','XX20')
 and a.country_destination_code = b.country_destination
 and a.dd_version = 'SFA';

/*End of first script*/

 /* Andrei - 28 Apr 2017 APP-6116 */

update tmp_fact_atlaspharmlogiforecast_merck a
set a.DD_REGIONFORDFA = b.REGIONFORDFA_REGION
FROM tmp_fact_atlaspharmlogiforecast_merck a, TMP_REGIONFORDFA b
where b.REGIONFORDFA_PLANT = a.DD_PLANTTITLEMERCK
and a.DD_REGIONFORDFA <> b.REGIONFORDFA_REGION;

 /* END Andrei - 28 Apr 2017 APP-6116 */


/* Alin - 09 Jan 2017 - changes according to BI-5039 */

update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_countrydestionation = b.country_destination , a.dd_regiondestionation = b.region_destination
from tmp_country_dest b,tmp_fact_atlaspharmlogiforecast_merck a
where a.country_destination_code = b.country_destination_code
and a.dd_version = 'DTA';

/* 8 Dec 2016 Georgiana changes according to BI-4374 adding dd_region column for market grouping*/
merge into tmp_fact_atlaspharmlogiforecast_merck a
using(
select distinct s.region,s.market_grouping
from region_marketing_grouping s
) t
on t.market_grouping=a.market_grouping
when matched then update set a.dd_region=ifnull(t.region,'Not Set')
where a.dd_version = 'DTA' and a.dd_region<>ifnull(t.region,'Not Set');


update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_regiondestionation = b.region_destination
from tmp_country_dest b, dim_plant c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantidsfa = c.dim_plantid
and substr(c.plantcode,1,2) = b.country_destination_code
and a.dd_version = 'SFA'
and c.plantcode <> 'NL10';

/* 8 Dec 2016 Georgiana changes according to BI-4374 adding dd_region column for market grouping*/
merge into tmp_fact_atlaspharmlogiforecast_merck a
using(
select distinct s.region,s.market_grouping
from region_marketing_grouping s,dim_plant c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantidsfa = c.dim_plantid
and a.dd_version = 'SFA'
and c.plantcode <> 'NL10'
) t
on t.market_grouping=a.market_grouping
when matched then update set a.dd_region=ifnull(t.region,'Not Set')
where a.dd_region<>ifnull(t.region,'Not Set');

update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_regiondestionation = b.region_destination
from tmp_country_dest b, dim_plant c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantidsfa = c.dim_plantid
and a.country_destination_code = b.country_destination_code
and c.plantcode = 'NL10';

/* 8 Dec 2016 Georgiana changes according to BI-4374 adding dd_region column for market grouping*/
merge into tmp_fact_atlaspharmlogiforecast_merck a
using(
select distinct s.region,s.market_grouping
from region_marketing_grouping s,dim_plant c,tmp_fact_atlaspharmlogiforecast_merck a
where  a.dim_plantidsfa = c.dim_plantid
and c.plantcode = 'NL10'
) t
on t.market_grouping=a.market_grouping
when matched then update set a.dd_region=ifnull(t.region,'Not Set')
where a.dd_region<>ifnull(t.region,'Not Set');

update tmp_fact_atlaspharmlogiforecast_merck a
set dd_regiondestionation = 'Europe 2'
from dim_plant b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantidsfa = b.dim_plantid
and b.plantcode = 'AT30'
and a.dd_version = 'SFA';

update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_countrydestionation = b.country_destination
from tmp_country_dest b, dim_plant c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantidsfa = c.dim_plantid
and substr(c.plantcode,1,2) = b.country_destination_code
and a.dd_version = 'SFA'
and c.plantcode <> 'NL10';

update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_countrydestionation = b.country_destination
from tmp_country_dest b, dim_plant c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantidsfa = c.dim_plantid
and a.country_destination_code = b.country_destination_code
and c.plantcode = 'NL10';

drop table if exists tmp_rowndfa_04;

create table tmp_rowndfa_04 as
select a.dim_plantid,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
min(dd_forecast4mthisnull) over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,c.plantcode)
as MININUM0,
min(dd_forecast4mthis1null) over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,c.plantcode)
as MININUM1,
min(dd_forecast4mthis2null) over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,c.plantcode)
as MININUM2 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_plant c, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_plantidsfa = c.dim_plantid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and c.plantcode <> 'NL10'
and a.dim_dateidreporting <> 1;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_forecast4mthisnull = b.MININUM0,
a.dd_forecast4mthis1null = b.MININUM1,
a.dd_forecast4mthis2null = b.MININUM2
from  tmp_rowndfa_04 b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dim_plantid <> 91
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;
  
 
/* Andrei 5.05.2017 - APP6116 */
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,DD_REGIONFORDFA ,
sum(ct_salesdeliv2mthsiru*ct_nasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid,f.dim_plantid, DD_REGIONFORDFA, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsiru ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,DD_REGIONFORDFA ;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribregion  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsIRU*ct_nasp/ct_salesdeliv2mthsIRUtot end
  from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  a.DD_REGIONFORDFA = b.DD_REGIONFORDFA
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribregion  <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0
 else 100*a.ct_salesdeliv2mthsIRU*ct_nasp/ct_salesdeliv2mthsIRUtot end;
  
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611;

/*  end Andrei 5.05.2017 -APP6116 */
 
/* Andrei 5.05.2017 -APP6116 */


drop table if exists fact_atlaspharmlogiforecast_merck_updContr26111;
 create table fact_atlaspharmlogiforecast_merck_updContr26111 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DD_REGIONFORDFA,
sum(ct_salescontribregion_part) ct_salescontribregion_part from
(
select distinct   f.dim_partid,f.dim_plantid, DD_REGIONFORDFA , f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull =1  then 0
else ct_salescontribregion
end as ct_salescontribregion_part ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
 where  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DD_REGIONFORDFA;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribregion_partial  = ct_salescontribregion_part
 from fact_atlaspharmlogiforecast_merck_updContr26111 b ,tmp_fact_atlaspharmlogiforecast_merck a
 where a.DD_REGIONFORDFA = b.DD_REGIONFORDFA
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and dd_version = 'SFA'
and a.ct_salescontribregion_partial <> ct_salescontribregion_part;

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr26111;

 /*  end Andrei 5.05.2017 -APP6116 */

 /* Species Contrib Partial */
 drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck ,
sum(ct_salescontribspecies_part) ct_salescontribspecies_part from
(
select distinct  f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull =1 then 0
else ct_salescontribspecies
end as ct_salescontribspecies_part, DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribspecies_partial  = ct_salescontribspecies_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b ,tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck and  a.dd_version = 'SFA'
and ct_salescontribspecies_partial   <> ct_salescontribspecies_part;

 drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr3;

 /* Plant partial contrib */

drop table if exists fact_atlaspharmlogiforecast_merck_updContr4;
 create table fact_atlaspharmlogiforecast_merck_updContr4 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid, */ f1.dim_plantidsfa,
sum(ct_salescontribplant_part) ct_salescontribplant_part from
(
select distinct  f.dim_partid,  /*f.dim_plantid, */ f.dim_plantidsfa, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull =1 then 0
else ct_salescontribplant
end as ct_salescontribplant_part ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA' and dd_source <> 'PRA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,  /*f1.dim_plantid, */ f1.dim_plantidsfa;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partial  = ct_salescontribplant_part
 from fact_atlaspharmlogiforecast_merck_updContr4 b,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'SFA'
and a.ct_salescontribplant_partial <> ct_salescontribplant_part;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, dd_version,
sum(ct_salescontribglobal_part) ct_salescontribglobal_part from
(
select distinct  f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull =1 then 0
else ct_salescontribglobal
end as ct_salescontribglobal_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,f1.foremonth, f1.foreyear,dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribglobal_partial  = ct_salescontribglobal_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and  a.dd_version = 'SFA'
and ct_salescontribglobal_partial   <> ct_salescontribglobal_part;


/* SFA Contrib */


drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,
sum(ct_salescontribgpf_part) ct_salescontribgpf_part from
(
select distinct  f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull =1  then 0
else ct_salescontribgpf
end as ct_salescontribgpf_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribgpf_partial  = ct_salescontribgpf_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  a.productfamily_merck = b.productfamily_merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = 'SFA' /*and dd_source <> 'PRA'*/
and a.ct_salescontribgpf_partial   <> ct_salescontribgpf_part;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck, /*dim_plantid, */ dim_plantidsfa, dd_version,
sum(ct_salescontribplanttogpf_part) ct_salescontribplanttogpf_part from
(
select distinct  f.dim_partid, /*f.dim_plantid, */ f.dim_plantidsfa, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull =1
then 0
else ct_salescontribplanttogpf
end as ct_salescontribplanttogpf_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,/*dim_plantid, */ dim_plantidsfa, dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttogpf_partial  = ct_salescontribplanttogpf_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b, tmp_fact_atlaspharmlogiforecast_merck a
 where  a.productfamily_merck = b.productfamily_merck and
 /*a.dim_plantid = b.dim_plantid */  a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and a.dd_version = 'SFA'
and ct_salescontribplanttogpf_partial   <> ct_salescontribplanttogpf_part;

/* Madalina 12 Oct 2016 - Add field Species within Plant Partial - BI-4115 */
drop table if exists fact_atlaspharmlogiforecast_merck_updContr5_2;
 create table fact_atlaspharmlogiforecast_merck_updContr5_2 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DominantSpeciesDescription_Merck, /*dim_plantid, */ dim_plantidsfa, dd_version,
sum(ct_salescontribplanttospecies_part) ct_salescontribplanttospecies_part from
(
select distinct  f.dim_partid, /*f.dim_plantid, */ f.dim_plantidsfa, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull =1
then 0
else ct_salescontribplanttospecies
end as ct_salescontribplanttospecies_part, DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
   and  dd_version = 'SFA' 
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DominantSpeciesDescription_Merck,/*dim_plantid, */ dim_plantidsfa, dd_version;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttospecies_partial  = ct_salescontribplanttospecies_part
 from fact_atlaspharmlogiforecast_merck_updContr5_2 b, tmp_fact_atlaspharmlogiforecast_merck a
 where  a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck and
 /*a.dim_plantid = b.dim_plantid */  a.dim_plantidsfa = b.dim_plantidsfa
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version  and a.dd_version = 'SFA' 
and ct_salescontribplanttospecies_partial   <> ct_salescontribplanttospecies_part;
/* END BI-4115*/

drop table if exists tmp_market_group_upd;
create table tmp_market_group_upd as
select distinct dim_partid,country_destination_code,market_grouping from tmp_fact_atlaspharmlogiforecast_merck
where (dd_version = 'DTA' or dim_plantid = 124)
and market_grouping <> 'Not Set'
union
select distinct dim_partid,country_destination_code,market_grouping
 from tmp_atlas_forecast_pra_forecasts_merck_dc_allrf_files_allyears
 where market_grouping <> 'Not Set';


/*Ambiguous Replace fix*/

merge into tmp_fact_atlaspharmlogiforecast_merck a
using (
select distinct b.country_destination_code,b.dim_partid, min(b.market_grouping) over (partition by b.country_destination_code,b.dim_partid) as market_grouping
from tmp_market_group_upd b,tmp_fact_atlaspharmlogiforecast_merck a
where a.country_destination_code = b.country_destination_code
and a.market_grouping = 'Not Set'
and (dd_version = 'DTA' or dim_plantid = 124)
and a.dim_partid = b.dim_partid) t on a.country_destination_code = t.country_destination_code and a.dim_partid = t.dim_partid
when matched then update set a.market_grouping = t.market_grouping;

/*Second script: 2_additionalinsertXX20notused*/
drop table if exists tmp_e1;
create table tmp_e1 as
select fact_atlaspharmlogiforecast_merckid,d.plantcode,country_destination_code,c.ProductFamily_Merck,c.prodfamilydescription_merck,c.partnumber,
count(partnumber) over (partition by country_destination_code,c.ProductFamily_Merck,c.prodfamilydescription_merck,c.partnumber) as CN
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b,dim_part c,dim_plant d
where a.dim_dateidreporting = b.dim_Dateid
and a.dd_version = 'DTA'
and a.dim_plantid in (91,124)
and a.dim_partid = c.dim_partid
and a.dim_plantid = d.dim_plantid;

drop table if exists tmp_delete;
create table tmp_delete as
select fact_atlaspharmlogiforecast_merckid,
a1.dim_dateidreporting,
a1.dim_plantid,
a1.dim_partid,
country_destination_code  from tmp_fact_atlaspharmlogiforecast_merck a1 , dim_date b1,dim_part c1,dim_plant d1
where a1.dim_dateidreporting = b1.dim_Dateid
and a1.dd_version = 'DTA'
and a1.dim_plantid in (91,124)
and a1.dim_partid = c1.dim_partid
and a1.dim_plantid = d1.dim_plantid
and exists (
select 1 from tmp_e1 e1
where e1.cn>1
and e1.plantcode = 'XX20'
and e1.fact_atlaspharmlogiforecast_merckid = a1.fact_atlaspharmlogiforecast_merckid
and e1.plantcode = d1.plantcode
and e1.country_destination_code = a1.country_destination_code
and e1.ProductFamily_Merck = c1.ProductFamily_Merck
and e1.prodfamilydescription_merck = c1.prodfamilydescription_merck
and e1.partnumber = c1.partnumber);

/*Georgiana 05 Mar 2018 Removing this delete as it is deleting also important xx20 rows-the ones that have only XX20*/
/*delete from tmp_fact_atlaspharmlogiforecast_merck a
where exists (select 1 from
tmp_delete b
where a.dim_dateidreporting = b.dim_Dateidreporting
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_partid
and a.country_destination_code = b.country_destination_code)
and a.dd_version = 'DTA'*/

update  tmp_fact_atlaspharmlogiforecast_merck a
set fact_atlaspharmlogiforecast_merckid =0
from dim_plant b,dim_part c,dim_plant d, dim_date e,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid
and a.dim_plantidsfa = d.dim_plantid
and a.dim_partid = c.dim_partid
and a.dim_dateidreporting = e.dim_dateid
and a.dd_version = 'SFA'
and b.plantcode = 'XX20'
and d.plantcode = 'NL10'
and a.dim_dateidreporting <> 1
and exists  (select 1 from tmp_fact_atlaspharmlogiforecast_merck a1, dim_plant b1,dim_part c1,dim_plant d1, dim_date e1
where a1.dim_plantid = b1.dim_plantid
and a1.dim_plantidsfa = d1.dim_plantid
and a1.dim_partid = c1.dim_partid
and a1.dim_dateidreporting = e1.dim_dateid
and a1.dd_version = 'SFA'
and b1.plantcode = 'NL10'
and d1.plantcode = 'NL10'
and e.datevalue = e1.datevalue
and a1.dim_dateidreporting <> 1
and c.partnumber = c1.partnumber);

delete  from tmp_fact_atlaspharmlogiforecast_merck
where fact_atlaspharmlogiforecast_merckid =0;

/*Georgiana 11 Jan 2018 Added a delete for NL10,XX20 cases which are not cought in the first dele for DTA version*/

drop table if exists tmp_delete2;
create table tmp_delete2 as 
select distinct
fact_atlaspharmlogiforecast_merckid,
a1.dim_dateidreporting,
a1.dim_plantid,
a1.dim_partid,
country_destination_code,
a1.reporting_company_code
 from tmp_fact_atlaspharmlogiforecast_merck a1 
inner join dim_part dp on a1.dim_partid=dp.dim_partid and dp.plant='XX20'
inner join dim_part dp2 on  dp.partnumber=dp2.partnumber and  dp2.plant='NL10'
where
 DIM_PLANTID = '91'
and dd_version='DTA';

delete from tmp_fact_atlaspharmlogiforecast_merck a
where exists (select 1 from
tmp_delete2 b
where a.dim_dateidreporting = b.dim_Dateidreporting
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_partid
and a.country_destination_code = b.country_destination_code
and a.reporting_company_code=b.reporting_company_code)
and a.dd_version = 'DTA';
/*11 Jan 2018 End of Changes*/

/* 21 Mar 2018 Reconciliation Changes We still found NL10 and XX20 combinations in DF which are found with NL10 only in PMRA, so we are deletin also XX20 additional records for which dim_plantid='124'*/

drop table if exists tmp_delete2;
create table tmp_delete2 as 
select distinct
fact_atlaspharmlogiforecast_merckid,
a1.dim_dateidreporting,
a1.dim_plantid,
a1.dim_partid,
country_destination_code,
a1.reporting_company_code
 from tmp_fact_atlaspharmlogiforecast_merck a1 
inner join dim_part dp on a1.dim_partid=dp.dim_partid and dp.plant='XX20'
inner join dim_part dp2 on  dp.partnumber=dp2.partnumber and  dp2.plant='NL10'
where
 DIM_PLANTID = '124'
and dd_version='DTA';


delete from tmp_fact_atlaspharmlogiforecast_merck a
where exists (select 1 from
tmp_delete2 b
where a.dim_dateidreporting = b.dim_Dateidreporting
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_partid
and a.country_destination_code = b.country_destination_code
and a.reporting_company_code=b.reporting_company_code)
and a.dd_version = 'DTA';
/*21 MAr 2018 End of changes*/

/*End of second script*/

/*Third Script:3_additional tmp_dfa4*/

drop table if exists tmp_first_sales01;

create table tmp_first_sales01 as
select c.datevalue,c.companycode,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.country_destination_code, min(case when ct_salesdelivered > 0 then c.datevalue else null end) over (partition by b.partnumber,dd_plantcode) as firstsales
from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date c
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting <> 1
and a.dd_version in ('SFA')
and a.dim_dateidreporting = c.dim_dateid;

update tmp_fact_atlaspharmlogiforecast_merck
set dim_dateidfirstsales = 1
where dim_dateidfirstsales<>1;

/*Ambiguous replace fix*/
merge into tmp_fact_atlaspharmlogiforecast_merck a
using ( select distinct d.dim_dateid,e.dim_partid,b.dd_plantcode,b.dd_planttitlemerck,b.country_destination_code, max(c.dim_dateid) as dim_dateidupd
from tmp_first_sales01 b,dim_date c,dim_date d, dim_part e,tmp_fact_atlaspharmlogiforecast_merck a
where b.firstsales = c.datevalue
and a.dim_dateidreporting = d.dim_dateid
and a.dim_partid = e.dim_partid
and b.partnumber = e.partnumber
and b.ProductFamily_Merck = e.ProductFamily_Merck
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and b.datevalue = d.datevalue
and b.companycode = d.companycode
and b.companycode = c.companycode
and a.country_destination_code = b.country_destination_code
and  a.dd_version in ('SFA')
group by d.dim_dateid,e.dim_partid,b.dd_plantcode,b.dd_planttitlemerck,b.country_destination_code) t on
a.dim_dateidreporting=t.dim_dateid and a.dim_partid=t.dim_partid and a.dd_plantcode = t.dd_plantcode and a.dd_planttitlemerck = t.dd_planttitlemerck and a.country_destination_code = t.country_destination_code
when matched then update set a.dim_dateidfirstsales = t.dim_dateidupd
where a.dim_dateidfirstsales <> t.dim_dateidupd;




drop table if exists tmp_first_salesgpf01;

create table tmp_first_salesgpf01 as
select c.datevalue,c.companycode,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.country_destination_code, min(case when ct_salesdelivered > 0 then c.datevalue else null end) over (partition by b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck) as firstsales
from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date c
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting <> 1
and a.dd_version in ('SFA')
and a.dim_dateidreporting = c.dim_dateid;

update tmp_fact_atlaspharmlogiforecast_merck
set dim_dateidfirstsalesgpf = 1
where dim_dateidfirstsalesgpf<>1;


/*Ambiguous replace fix*/
/*merge into tmp_fact_atlaspharmlogiforecast_merck a
using (select distinct d.dim_dateid,e.dim_partid,b.dd_plantcode,b.dd_planttitlemerck,b.country_destination_code, min(c.dim_dateid) as dim_dateidupd
from tmp_first_salesgpf01 b,dim_date c,dim_date d, dim_part e, tmp_fact_atlaspharmlogiforecast_merck a
where b.firstsales = c.datevalue
and a.dim_dateidreporting = d.dim_dateid
and a.dim_partid = e.dim_partid
and b.partnumber = e.partnumber
and b.ProductFamily_Merck = e.ProductFamily_Merck
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and b.datevalue = d.datevalue
and b.companycode = d.companycode
and b.companycode = c.companycode
and a.country_destination_code = b.country_destination_code
and  a.dd_version in ('SFA')
group by d.dim_dateid,e.dim_partid,b.dd_plantcode,b.dd_planttitlemerck,b.country_destination_code ) t
on a.dim_dateidreporting = t.dim_dateid
and a.dim_partid = t.dim_partid
and a.dd_plantcode = t.dd_plantcode
and a.dd_planttitlemerck = t.dd_planttitlemerck
and a.country_destination_code = t.country_destination_code
when matched then update set a.dim_dateidfirstsalesgpf = t.dim_dateidupd
where a.dim_dateidfirstsalesgpf <> t.dim_dateidupd*/

merge into tmp_fact_atlaspharmlogiforecast_merck a
using (select distinct d.dim_dateid,e.dim_partid,b.dd_plantcode,b.dd_planttitlemerck,b.country_destination_code, min(c.dim_dateid) as dim_dateidupd
from  tmp_fact_atlaspharmlogiforecast_merck a
inner join dim_part e on a.dim_partid = e.dim_partid
inner join tmp_first_salesgpf01 b on b.partnumber = e.partnumber
and b.ProductFamily_Merck = e.ProductFamily_Merck
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.country_destination_code = b.country_destination_code
inner join dim_date c on  b.firstsales = c.datevalue and  b.companycode = c.companycode
inner join dim_date d on  a.dim_dateidreporting = d.dim_dateid and b.datevalue = d.datevalue and b.companycode = d.companycode  and b.dd_plantcode = d.plantcode_factory
where a.dd_version in ('SFA')
group by d.dim_dateid,e.dim_partid,b.dd_plantcode,b.dd_planttitlemerck,b.country_destination_code ) t
on a.dim_dateidreporting = t.dim_dateid
and a.dim_partid = t.dim_partid
and a.dd_plantcode = t.dd_plantcode
and a.dd_planttitlemerck = t.dd_planttitlemerck
and a.country_destination_code = t.country_destination_code
when matched then update set a.dim_dateidfirstsalesgpf = t.dim_dateidupd
where a.dim_dateidfirstsalesgpf <> t.dim_dateidupd;

drop table if exists tmp_sfaavg_01;

create table tmp_sfaavg_01 as
select d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end as country_destination_code,
abs(case when avg(ct_salescontribplant_partialavg)=0 then 0.00000 else cast(sum(ct_salescontribplantavg)/avg(ct_salescontribplant_partialavg) as decimal (18,5)) end)*
(CASE WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is not null THEN ( (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) )/2
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is not null THEN ( (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) )/2
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is null THEN ( (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) )/2
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is null THEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END)
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is null THEN (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END)
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is not null THEN (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END)
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is not null THEN ( (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) )/3
ELSE NULL
END)
as plant_sfaavg_4
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end;

drop table if exists tmp_rowndfa_02;

create table tmp_rowndfa_02 as
select a.dd_plantcode,a.dd_planttitlemerck,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
row_number() over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,a.dd_plantcode,a.dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
a.country_destination_code
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4 = 0
where rn_dfa4 <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4 = b.RNDFA
from  tmp_rowndfa_02 b,tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;

/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/

/*update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from dim_part b, dim_date d,
tmp_sfaavg_01 e,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code*/

update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from  tmp_fact_atlaspharmlogiforecast_merck a
inner join dim_part b on a.dim_partid = b.dim_partid
inner join dim_date d on a.dim_dateidreporting = d.dim_dateid
inner join tmp_sfaavg_01 e on d.datevalue = e.datevalue /*and e.dd_plantcode = d.plantcode_factory*/
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code
where a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;



drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ dd_plantcode,dd_planttitlemerck,
sum(ct_salesdeliv3mthsIRU*ct_nasp) ct_salesdeliv3mthsIRUtot from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3) as ct_salesdeliv3mthsIRU ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplantavg  = case when ifnull( ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/b.ct_salesdeliv3mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg b ,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dd_plantcode = b.dd_plantcode
 and a.dd_planttitlemerck = b.dd_planttitlemerck and b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribplantavg  <> case when ifnull(ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/ct_salesdeliv3mthsIRUtot end;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr4avg;
 create table fact_atlaspharmlogiforecast_merck_updContr4avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salescontribplant_partavg) ct_salescontribplant_partavg from
(
select distinct f.dim_partid,  /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and dd_forecast9mthisnull =1 then 0
else ct_salescontribplantavg
end as ct_salescontribplant_partavg
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA' and dd_source <> 'PRA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,  /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partialavg  = ct_salescontribplant_partavg
 from fact_atlaspharmlogiforecast_merck_updContr4avg b, tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode
 and a.dd_planttitlemerck = b.dd_planttitlemerck and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'SFA'
and a.ct_salescontribplant_partialavg <> ct_salescontribplant_partavg;



drop table if exists tmp_sfaavg_01;

create table tmp_sfaavg_01 as
select d.datevalue,b.partnumber,b.ProductFamily_Merck,a.dd_plantcode,a.dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code,
abs(case when avg(ct_salescontribplant_partialavg)=0 then 0.00000 else cast(sum(ct_salescontribplantavg)/avg(ct_salescontribplant_partialavg) as decimal (18,5)) end)*
(CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND      SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00)
      WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00)
      WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,8))) >1 THEN (0.00)
      WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,8))) <0 THEN (0.00)
      ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,8))) END)
as plant_sfaavg_4
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.partnumber,b.ProductFamily_Merck,a.dd_plantcode,a.dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code;


drop table if exists tmp_rowndfa_02;

create table tmp_rowndfa_02 as
select a.dd_plantcode,a.dd_planttitlemerck,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
row_number() over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,a.dd_plantcode,a.dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4 = 0
where rn_dfa4 <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4 = b.RNDFA
from  tmp_rowndfa_02 b,tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;

/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/

update tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_sfa_avg = 0
where ct_plant_sfa_avg <> 0 ;

/*update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from dim_part b, dim_date d,
tmp_sfaavg_01 e,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code*/

update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
 from tmp_fact_atlaspharmlogiforecast_merck a
inner join dim_part b on a.dim_partid = b.dim_partid
inner join dim_date d on a.dim_dateidreporting = d.dim_dateid
inner join tmp_sfaavg_01 e on d.datevalue = e.datevalue /*and e.dd_plantcode = d.plantcode_factory*/
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code
where a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

/*End of third script*/

/* 4th script:4_additional2*/

update tmp_fact_atlaspharmlogiforecast_merck
set dd_supplyissuecurrentmonth = null
where dd_supplyissuecurrentmonth is not null;

/*Ambiguous replace fix*/
merge into  tmp_fact_atlaspharmlogiforecast_merck a
using (
select distinct a.dd_version,
a.dim_dateidreporting,
a.country_destination_code,
a.dim_partid,
a.dim_plantid
from dim_date b,tmp_Supply_Outlook c,dim_part d,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid
and a.dim_partid = d.dim_partid
and b.datevalue =  to_date(case when c.reporting_period is null then '00010101' else concat(c.reporting_period,'01') end,'YYYYMMDD')
and c.productfamily = d.productfamily_merck
and a.dd_REGIONFORDFA = case when c.region in ('Europe 1','Europe 2') then 'EURAM' else c.region end) t
on t.dd_version=a.dd_version
and t.dim_dateidreporting=a.dim_dateidreporting
and t.country_destination_code=a.country_destination_code
and t.dim_partid=a.dim_partid
and t.dim_plantid=a.dim_plantid
when matched then update set  dd_supplyissuecurrentmonth = 'X';

update tmp_fact_atlaspharmlogiforecast_merck
set dd_supplyissuelastmonth = null
where dd_supplyissuelastmonth is not null;

/*Ambiguous replace fix*/
merge into  tmp_fact_atlaspharmlogiforecast_merck a
using (
select distinct a.dd_version,
a.dim_dateidreporting,
a.country_destination_code,
a.dim_partid,
a.dim_plantid
from dim_date b,tmp_Supply_Outlook c,dim_part d,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid
and a.dim_partid = d.dim_partid
and CASE WHEN b.datevalue = '0001-01-01' THEN b.datevalue ELSE add_Months(b.datevalue,-1) END =  to_date(case when c.reporting_period is null then '00010101' else concat(c.reporting_period,'01') end,'YYYYMMDD')
and c.productfamily = d.productfamily_merck
and a.dd_REGIONFORDFA = case when c.region in ('Europe 1','Europe 2') then 'EURAM' else c.region end) t
on t.dd_version=a.dd_version
and t.dim_dateidreporting=a.dim_dateidreporting
and t.country_destination_code=a.country_destination_code
and t.dim_partid=a.dim_partid
and t.dim_plantid=a.dim_plantid
when matched then update set  dd_supplyissuelastmonth = 'X';

update tmp_fact_atlaspharmlogiforecast_merck
set dd_supplyissue2monthsago = null
where dd_supplyissue2monthsago is not null;

/*Ambiguous replace fix*/
merge into  tmp_fact_atlaspharmlogiforecast_merck a
using (
select distinct a.dd_version,
a.dim_dateidreporting,
a.country_destination_code,
a.dim_partid,
a.dim_plantid
from dim_date b,tmp_Supply_Outlook c,dim_part d,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid
and a.dim_partid = d.dim_partid
and CASE WHEN b.datevalue = '0001-01-01' THEN b.datevalue ELSE add_Months(b.datevalue,-2) END =  to_date(case when c.reporting_period is null then '00010101' else concat(c.reporting_period,'01') end,'YYYYMMDD')
and c.productfamily = d.productfamily_merck
and a.dd_REGIONFORDFA = case when c.region in ('Europe 1','Europe 2') then 'EURAM' else c.region end) t
on t.dd_version=a.dd_version
and t.dim_dateidreporting=a.dim_dateidreporting
and t.country_destination_code=a.country_destination_code
and t.dim_partid=a.dim_partid
and t.dim_plantid=a.dim_plantid
when matched then update set  dd_supplyissue2monthsago = 'X';

update tmp_fact_atlaspharmlogiforecast_merck
set dd_firstsalesyoungerforyear = null
where dd_firstsalesyoungerforyear is not null;

update tmp_fact_atlaspharmlogiforecast_merck a
set dd_firstsalesyoungerforyear = 'X'
from dim_date b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid
and a.dim_dateidfirstsalesgpf = c.dim_dateid
and b.datevalue - c.datevalue < 365
and a.dim_dateidreporting <> 1
and a.dim_dateidfirstsalesgpf <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set dd_firstsalesyoungerforyear = 'X'
where dim_dateidfirstsalesgpf = 1
and dd_firstsalesyoungerforyear <> 'X';

update tmp_fact_atlaspharmlogiforecast_merck
set  	ct_salesdeliv2mthsnasp = 0, ct_salesdeliv2mthsiru = 0
where dd_version = 'SFA'
and (dd_firstsalesyoungerforyear = 'X' or dd_supplyissue2monthsago = 'X' or dd_supplyissuelastmonth = 'X'
or dd_supplyissuecurrentmonth = 'X');

drop table if exists tmp_rowndfa_02_subprodlev_forecast_4_mth;
create table tmp_rowndfa_02_subprodlev_forecast_4_mth as
select country_destination_code,a.dd_plantcode,dd_planttitlemerck,sub_product_family_code_pma,b.dim_partid,a.dim_dateidreporting,
sum(case when dd_forecast4mthisnull=0 then 1 else 0 end) over (partition by d.datevalue,b.sub_product_family_code_pma,b.ProductFamily_Merck,a.dd_plantcode,a.dd_planttitlemerck) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set dd_forecast4mthisnull_subprodlev =  null
where dd_version = 'SFA'
and dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.dd_forecast4mthisnull_subprodlev = case when b.RNDFA>=1 then 0 else 1 end
from  tmp_rowndfa_02_subprodlev_forecast_4_mth b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;



/* Plant contrib */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salesdeliv2mthsnasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsnasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2 b , tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and dd_version = 'SFA'
and a.ct_salescontribplant  <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8))  end;

 drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2_1;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2_1 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,
sum(ct_salesdeliv2mthsiru * ct_nasp) ct_salesdeliv2mthsIRUtot from
(
select  distinct  f.dim_partid, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsiru,productfamily_merck ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
 ) f1
 group by f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribgpf  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2_1 b , tmp_fact_atlaspharmlogiforecast_merck a
 where a.productfamily_merck = b.productfamily_merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and dd_version = 'SFA'
and a.ct_salescontribgpf  <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8))  end;

 drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salesdeliv2mthsnasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  dpt.sub_product_family_code_pma, /*f.dim_plantid*/ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsnasp
from tmp_fact_atlaspharmlogiforecast_merck f,dim_part dpt
where f.dim_partid = dpt.dim_partid
  and   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_subprodlev  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2_subprodlev b, tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and dd_version = 'SFA'
and a.ct_salescontribplant_subprodlev  <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8))  end;



 drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
drop table if exists fact_atlaspharmlogiforecast_merck_updContr4;





-----------------
/* NEW contrib */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr1;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 as
select   f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid, */ dd_plantcode,dd_planttitlemerck, productfamily_merck,
sum(ct_salesdeliv2mthsnasp) ct_salesdelivgpftotal from
tmp_fact_atlaspharmlogiforecast_merck f1
  where  dd_version = 'SFA' and
fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 group by  f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid, */ dd_plantcode,dd_planttitlemerck, productfamily_merck;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttogpf  = case when b.ct_salesdelivgpftotal=0 then 0.00000000 else
 (100.00000000*cast(ct_salesdeliv2mthsnasp as decimal (36,8)))/cast(b.ct_salesdelivgpftotal as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 b, tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear
 and a.foremonth = b.foremonth
and a.productfamily_merck =b.productfamily_merck
and a.dd_version = b.dd_version and  a.dd_version = 'SFA'
and a.ct_salescontribplanttogpf  <>
case when b.ct_salesdelivgpftotal=0 then 0.00000000 else
 (100.00000000*cast(ct_salesdeliv2mthsnasp as decimal (36,8)))/cast(b.ct_salesdelivgpftotal as decimal (36,8)) end;

/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/

/* Madalina 12 Oct 2016 - Add field Contribution to Species within Plant BI-4115*/
/* NEW contrib for Species*/
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr5_5;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr5_5 as
select   f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid, */ dd_plantcode,dd_planttitlemerck, DominantSpeciesDescription_Merck,
sum(ct_salesdeliv2mthsnasp) ct_salesdelivspeciestotal from
tmp_fact_atlaspharmlogiforecast_merck f1
  where  dd_version = 'SFA' and
fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 group by  f1.snapmonth, f1.snapyear,f1.dd_version,
f1.foremonth, f1.foreyear, /*dim_plantid, */ dd_plantcode,dd_planttitlemerck, DominantSpeciesDescription_Merck;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttospecies  = case when b.ct_salesdelivspeciestotal=0 then 0.00000000 else
 (100.00000000*cast(ct_salesdeliv2mthsnasp as decimal (36,8)))/cast(b.ct_salesdelivspeciestotal as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr5_5 b, tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear
 and a.foremonth = b.foremonth
and a.DominantSpeciesDescription_Merck =b.DominantSpeciesDescription_Merck
and a.dd_version = b.dd_version and  a.dd_version = 'SFA'
and a.ct_salescontribplanttospecies <>
case when b.ct_salesdelivspeciestotal=0 then 0.00000000 else
 (100.00000000*cast(ct_salesdeliv2mthsnasp as decimal (36,8)))/cast(b.ct_salesdelivspeciestotal as decimal (36,8)) end;

 /* END BI-4115 */



/* Contribution Global */


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr1;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,
sum(ct_salesdeliv2mthsnasp ) ct_salesdelivglobal_tot from
(
select distinct  f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsnasp,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_source <> 'PRA' and  dd_version = 'SFA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribglobal  =
 cast(100.00000000*cast(ct_salesdeliv2mthsnasp as decimal (36,8))/case when cast(b.ct_salesdelivglobal_tot as decimal (36,8)) =0 then 1.00000 else cast(b.ct_salesdelivglobal_tot as decimal (36,8)) end as decimal (18,8))
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr1 b, tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  a.dd_version = 'SFA'
and a.ct_salescontribglobal  <>
  cast(100.00000000*cast(ct_salesdeliv2mthsnasp as decimal (36,8)) /case when cast(b.ct_salesdelivglobal_tot as decimal (36,8)) =0 then 1.00000 else cast(b.ct_salesdelivglobal_tot as decimal (36,8))  end as decimal (18,8));



/* Species Contrib */

update tmp_fact_atlaspharmlogiforecast_merck f
set f.DominantSpeciesDescription_Merck = pt.DominantSpeciesDescription_Merck
from   dim_part pt, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_partid = pt.dim_partid and   dd_version = 'SFA'
and ifnull(f.DominantSpeciesDescription_Merck,'X') <> pt.DominantSpeciesDescription_Merck;


drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr3;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck ,
sum(ct_salesdeliv2mthsnasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsnasp, f.DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck  f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck;


update tmp_fact_atlaspharmlogiforecast_merck  a
 set a.ct_salescontribspecies  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr3 b , tmp_fact_atlaspharmlogiforecast_merck  a
 where  dd_version = 'SFA' and
 a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribspecies   <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end;

 drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr3_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr3_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck ,
sum(ct_salesdeliv2mthsnasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  dpt.sub_product_family_code_pma, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsnasp, f.DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck  f, dim_part dpt
  where   dd_version = 'SFA'
  and f.dim_partid = dpt.dim_partid
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck;


update tmp_fact_atlaspharmlogiforecast_merck  a
 set a.ct_salescontribspecies_subprodlev  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr3_subprodlev b, tmp_fact_atlaspharmlogiforecast_merck  a
 where  dd_version = 'SFA' and
 a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribspecies_subprodlev   <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsnasp as decimal (36,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end;







   update tmp_fact_atlaspharmlogiforecast_merck f
 set f.ct_sfatarget = s.sfatarget
 from    sfaplanttargets_merck s, tmp_fact_atlaspharmlogiforecast_merck f
  where fact_atlaspharmlogiforecast_merckid =1 and  dd_version = 'SFA'
   and f.dd_plantcode = s.plant
  and f.ct_sfatarget <> s.sfatarget;

/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/

 /*   Andrei 5.05.2017 -APP6116 */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,dd_REGIONFORDFA ,
sum(ct_salesdeliv2mthsiru*ct_nasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  f.dim_partid,f.dd_plantcode,dd_planttitlemerck, DD_REGIONFORDFA, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsiru ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,DD_REGIONFORDFA ;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribregion  = case when ifnull(cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)),0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsIRU as decimal (18,8))*cast(ct_nasp as decimal (18,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611 b, tmp_fact_atlaspharmlogiforecast_merck a
 where  a.DD_REGIONFORDFA = b.DD_REGIONFORDFA
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribregion  <> case when ifnull(cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)),0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsIRU as decimal (18,8))*cast(ct_nasp as decimal (18,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end;
 
drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611;

  /*  end Andrei 5.05.2017 -APP6116 */

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,DD_REGIONFORDFA ,
sum(ct_salesdeliv2mthsiru*ct_nasp) ct_salesdeliv2mthsIRUtot from
(
select distinct  dpt.sub_product_family_code_pma,f.dd_plantcode,dd_planttitlemerck, DD_REGIONFORDFA, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, ct_salesdeliv2mthsiru ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dpt
  where  dd_version = 'SFA'
  and f.dim_partid = dpt.dim_partid
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,DD_REGIONFORDFA ;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribregion_subprodlev  = case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsIRU as decimal (18,8))*cast(ct_nasp as decimal (18,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611_subprodlev b, tmp_fact_atlaspharmlogiforecast_merck a
 where  a.DD_REGIONFORDFA = b.DD_REGIONFORDFA
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribregion_subprodlev  <> case when ifnull(ct_salesdeliv2mthsIRUtot,0)=0 then 0.00000000
 else 100.00000000*cast(a.ct_salesdeliv2mthsIRU as decimal (18,8))*cast(ct_nasp as decimal (18,8))/cast(ct_salesdeliv2mthsIRUtot as decimal (36,8)) end;

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2611_subprodlev;


 /*   Andrei 5.05.2017 -APP6116 */

drop table if exists fact_atlaspharmlogiforecast_merck_updContr26111;
 create table fact_atlaspharmlogiforecast_merck_updContr26111 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DD_REGIONFORDFA,
sum(ct_salescontribregion_part) ct_salescontribregion_part from
(
select distinct   f.dim_partid,f.dd_plantcode,dd_planttitlemerck, DD_REGIONFORDFA , f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull =1  then 0
else ct_salescontribregion
end as ct_salescontribregion_part ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
 where  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DD_REGIONFORDFA;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribregion_partial  = ct_salescontribregion_part
 from fact_atlaspharmlogiforecast_merck_updContr26111 b, tmp_fact_atlaspharmlogiforecast_merck a
 where a.DD_REGIONFORDFA = b.DD_REGIONFORDFA
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and dd_version = 'SFA'
and a.ct_salescontribregion_partial <> ct_salescontribregion_part;

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr26111_subprodlev;

 /*  end Andrei 5.05.2017 -APP6116 */

drop table if exists fact_atlaspharmlogiforecast_merck_updContr26111_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_updContr26111_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DD_REGIONFORDFA,
sum(ct_salescontribregion_part) ct_salescontribregion_part from
(
select distinct   dpt.sub_product_family_code_pma,f.dd_plantcode,dd_planttitlemerck, DD_REGIONFORDFA , f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull_subprodlev =1  then 0
else ct_salescontribregion_subprodlev
end as ct_salescontribregion_part ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dpt
where f.dim_partid = dpt.dim_partid
 and  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DD_REGIONFORDFA;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribregion_partial_subprodlev  = ct_salescontribregion_part
 from fact_atlaspharmlogiforecast_merck_updContr26111_subprodlev b,tmp_fact_atlaspharmlogiforecast_merck a
 where a.DD_REGIONFORDFA = b.DD_REGIONFORDFA
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and dd_version = 'SFA'
and a.ct_salescontribregion_partial_subprodlev <> ct_salescontribregion_part;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr26111_subprodlev;

 /* Species Contrib Partial */
 drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck ,
sum(ct_salescontribspecies_part) ct_salescontribspecies_part from
(
select distinct  f.dim_partid, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull =1 then 0
else ct_salescontribspecies
end as ct_salescontribspecies_part, DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribspecies_partial  = ct_salescontribspecies_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck and  a.dd_version = 'SFA'
and ct_salescontribspecies_partial   <> ct_salescontribspecies_part;

 drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr3_subprodlev;

  drop table if exists fact_atlaspharmlogiforecast_merck_updContr3_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_updContr3_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck ,
sum(ct_salescontribspecies_part) ct_salescontribspecies_part from
(
select distinct  dpt.sub_product_family_code_pma, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull_subprodlev =1 then 0
else ct_salescontribspecies_subprodlev
end as ct_salescontribspecies_part, f.DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dpt
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA' and f.dim_partid = dpt.dim_partid
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, f1.DominantSpeciesDescription_Merck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribspecies_partial_subprodlev  = ct_salescontribspecies_part
 from fact_atlaspharmlogiforecast_merck_updContr3_subprodlev b , tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck and  a.dd_version = 'SFA'
and ct_salescontribspecies_partial_subprodlev   <> ct_salescontribspecies_part;

 drop table if exists fact_atlaspharmlogiforecast_merck_updContr3_subprodlev;

 /* Plant partial contrib */

drop table if exists fact_atlaspharmlogiforecast_merck_updContr4;
 create table fact_atlaspharmlogiforecast_merck_updContr4 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salescontribplant_part) ct_salescontribplant_part from
(
select distinct  f.dim_partid,  /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull =1 then 0
else ct_salescontribplant
end as ct_salescontribplant_part ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA' and dd_source <> 'PRA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,  /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partial  = ct_salescontribplant_part
 from fact_atlaspharmlogiforecast_merck_updContr4 b , tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'SFA'
and a.ct_salescontribplant_partial <> ct_salescontribplant_part;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr4_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_updContr4_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salescontribplant_part) ct_salescontribplant_part from
(
select distinct  dpt.sub_product_family_code_pma,  /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull_subprodlev =1 then 0
else ct_salescontribplant_subprodlev
end as ct_salescontribplant_part ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f,dim_part dpt
  where   dd_version = 'SFA' and dd_source <> 'PRA' and f.dim_partid = dpt.dim_partid
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,  /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partial_subprodlev  = ct_salescontribplant_part
 from fact_atlaspharmlogiforecast_merck_updContr4_subprodlev b , tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'SFA'
and a.ct_salescontribplant_partial_subprodlev <> ct_salescontribplant_part;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, dd_version,
sum(ct_salescontribglobal_part) ct_salescontribglobal_part from
(
select distinct  f.dim_partid, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull =1 then 0
else ct_salescontribglobal
end as ct_salescontribglobal_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,f1.foremonth, f1.foreyear,dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribglobal_partial  = ct_salescontribglobal_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b, tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and  a.dd_version = 'SFA'
and ct_salescontribglobal_partial   <> ct_salescontribglobal_part;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr3_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_updContr3_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, dd_version,
sum(ct_salescontribglobal_part) ct_salescontribglobal_part from
(
select distinct  dpt.sub_product_family_code_pma, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull_subprodlev =1 then 0
else ct_salescontribglobal
end as ct_salescontribglobal_part, f.productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dpt
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA' and f.dim_partid = dpt.dim_partid
 ) f1
 group by  f1.snapmonth, f1.snapyear,f1.foremonth, f1.foreyear,dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribglobal_partial_subprodlev  = ct_salescontribglobal_part
 from fact_atlaspharmlogiforecast_merck_updContr3_subprodlev b , tmp_fact_atlaspharmlogiforecast_merck a
 where  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and  a.dd_version = 'SFA'
and ct_salescontribglobal_partial_subprodlev   <> ct_salescontribglobal_part;


/* SFA Contrib */


drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,
sum(ct_salescontribgpf_part) ct_salescontribgpf_part from
(
select distinct  f.dim_partid, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull =1  then 0
else ct_salescontribgpf
end as ct_salescontribgpf_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribgpf_partial  = ct_salescontribgpf_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b,tmp_fact_atlaspharmlogiforecast_merck a
 where  a.productfamily_merck = b.productfamily_merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = 'SFA' /*and dd_source <> 'PRA'*/
and a.ct_salescontribgpf_partial   <> ct_salescontribgpf_part;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr3_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_updContr3_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,
sum(ct_salescontribgpf_part) ct_salescontribgpf_part from
(
select distinct  dpt.sub_product_family_code_pma, f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast4mthisnull_subprodlev =1  then 0
else ct_salescontribgpf
end as ct_salescontribgpf_part, f.productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dpt
  where   dd_version = 'SFA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
and f.dim_partid = dpt.dim_partid
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribgpf_partial_subprodlev  = ct_salescontribgpf_part
 from fact_atlaspharmlogiforecast_merck_updContr3_subprodlev b,tmp_fact_atlaspharmlogiforecast_merck a
 where  a.productfamily_merck = b.productfamily_merck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = 'SFA' /*and dd_source <> 'PRA'*/
and a.ct_salescontribgpf_partial_subprodlev   <> ct_salescontribgpf_part;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr3;
 create table fact_atlaspharmlogiforecast_merck_updContr3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck, /*dim_plantid, */ dd_plantcode,dd_planttitlemerck, dd_version,
sum(ct_salescontribplanttogpf_part) ct_salescontribplanttogpf_part from
(
select distinct  f.dim_partid, /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull =1
then 0
else ct_salescontribplanttogpf
end as ct_salescontribplanttogpf_part, productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,/*dim_plantid, */ dd_plantcode,dd_planttitlemerck, dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttogpf_partial  = ct_salescontribplanttogpf_part
 from fact_atlaspharmlogiforecast_merck_updContr3 b, tmp_fact_atlaspharmlogiforecast_merck a
 where  a.productfamily_merck = b.productfamily_merck and
 /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and a.dd_version = 'SFA'
and ct_salescontribplanttogpf_partial   <> ct_salescontribplanttogpf_part;

/* Madalina 12 Oct 2016 - Add field Species within Plant Partial- BI-4115 */
drop table if exists fact_atlaspharmlogiforecast_merck_updContr5_3;
 create table fact_atlaspharmlogiforecast_merck_updContr5_3 as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DominantSpeciesDescription_Merck, /*dim_plantid, */ dd_plantcode,dd_planttitlemerck, dd_version,
sum(ct_salescontribplanttospecies_part) ct_salescontribplanttospecies_part from
(
select distinct  f.dim_partid, /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull =1
then 0
else ct_salescontribplanttospecies
end as ct_salescontribplanttospecies_part, DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck f
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
   and  dd_version = 'SFA' 
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DominantSpeciesDescription_Merck,/*dim_plantid, */ dd_plantcode,dd_planttitlemerck, dd_version;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttospecies_partial  = ct_salescontribplanttospecies_part
 from fact_atlaspharmlogiforecast_merck_updContr5_3 b, tmp_fact_atlaspharmlogiforecast_merck a
 where  a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck and
 /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and a.dd_version = 'SFA'  
and ct_salescontribplanttospecies_partial   <> ct_salescontribplanttospecies_part;
/* END BI-4115 */

drop table if exists fact_atlaspharmlogiforecast_merck_updContr3_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_updContr3_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck, /*dim_plantid, */ dd_plantcode,dd_planttitlemerck, dd_version,
sum(ct_salescontribplanttogpf_part) ct_salescontribplanttogpf_part from
(
select distinct  dpt.sub_product_family_code_pma, /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull_subprodlev =1
then 0
else ct_salescontribplanttogpf
end as ct_salescontribplanttogpf_part, f.productfamily_merck
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dpt
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA' and f.dim_partid = dpt.dim_partid
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, productfamily_merck,/*dim_plantid, */ dd_plantcode,dd_planttitlemerck, dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttogpf_partial_subprodlev  = ct_salescontribplanttogpf_part
 from fact_atlaspharmlogiforecast_merck_updContr3_subprodlev b, tmp_fact_atlaspharmlogiforecast_merck a
 where  a.productfamily_merck = b.productfamily_merck and
 /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and a.dd_version = 'SFA'
and ct_salescontribplanttogpf_partial_subprodlev   <> ct_salescontribplanttogpf_part;

/* Madalina - 12 Oct 2016 - Add ct_salescontribplanttospecies_partial_subprodlev, which will be used for ct_specieswithinplant_dfa_4_subprodlev calculation - BI-4115 */
drop table if exists fact_atlaspharmlogiforecast_merck_updContr5_subprodlev;
 create table fact_atlaspharmlogiforecast_merck_updContr5_subprodlev as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DominantSpeciesDescription_Merck, /*dim_plantid, */ dd_plantcode,dd_planttitlemerck, dd_version,
sum(ct_salescontribplanttospecies_part) ct_salescontribplanttospecies_part from
(
select distinct  dpt.sub_product_family_code_pma, /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, dd_version,
case when dd_forecast4mthisnull_subprodlev =1
then 0
else ct_salescontribplanttospecies
end as ct_salescontribplanttospecies_part, f.DominantSpeciesDescription_Merck
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dpt
  where   fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
  and  dd_version = 'SFA' and f.dim_partid = dpt.dim_partid
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, DominantSpeciesDescription_Merck,/*dim_plantid, */ dd_plantcode,dd_planttitlemerck, dd_version;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplanttospecies_partial_subprodlev  = ct_salescontribplanttospecies_part
 from fact_atlaspharmlogiforecast_merck_updContr5_subprodlev b, tmp_fact_atlaspharmlogiforecast_merck a
 where  a.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck and
 /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.dd_version = b.dd_version and a.dd_version = 'SFA'
and ct_salescontribplanttospecies_partial_subprodlev   <> ct_salescontribplanttospecies_part;
/* END BI-4115 */

drop table if exists tmp_dfa_01;

create table tmp_dfa_01 as
select d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end as country_destination_code,
abs(case when avg(ct_salescontribspecies_partial)=0 then 0.00000 else cast(sum(ct_salescontribspecies)/avg(ct_salescontribspecies_partial) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as species_dfa_4,
abs(case when avg(ct_salescontribplant_partial)=0 then 0.00000 else cast(sum(ct_salescontribplant)/avg(ct_salescontribplant_partial) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as plant_dfa_4,
abs(case when avg(ct_salescontribglobal_partial)=0 then 0.00000 else cast (sum(ct_salescontribglobal)/avg(ct_salescontribglobal_partial) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,4))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,4))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as global_dfa_4,
abs(case when avg(ct_salescontribgpf_partial)=0 then 0.00000 else cast(sum(ct_salescontribgpf)/avg(ct_salescontribgpf_partial) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as gpf_dfa_4,
abs(case when avg(ct_salescontribplanttogpf_partial)=0 then 0.00000 else cast(sum(ct_salescontribplanttogpf)/avg(ct_salescontribplanttogpf_partial) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,4))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,4))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as gpfwithinplant_dfa_4,
abs(case when avg(ct_salescontribregion_partial)=0 then 0.00000 else cast(sum(ct_salescontribregion)/avg(ct_salescontribregion_partial) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,4))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,4))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as region_dfa_4
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end;




drop table if exists tmp_rowndfa_02;

create table tmp_rowndfa_02 as
select a.dd_plantcode,dd_planttitlemerck,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
row_number() over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4 = 0
where rn_dfa4 <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4 = b.RNDFA
from  tmp_rowndfa_02 b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;

/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/

update tmp_fact_atlaspharmlogiforecast_merck
set ct_species_dfa_4 = 0
where ct_species_dfa_4 <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_dfa_4 = 0
where ct_plant_dfa_4 <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_global_dfa_4 = 0
where ct_global_dfa_4 <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_gpf_dfa_4 = 0
where ct_gpf_dfa_4 <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_gpfwithinplant_dfa_4 = 0
where ct_gpfwithinplant_dfa_4 <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_region_dfa_4 = 0
where ct_region_dfa_4 <> 0;


/*update tmp_fact_atlaspharmlogiforecast_merck a
set ct_species_dfa_4 = species_dfa_4,
    ct_plant_dfa_4 = plant_dfa_4,
    ct_global_dfa_4 = global_dfa_4,
    ct_gpf_dfa_4 = gpf_dfa_4,
    ct_gpfwithinplant_dfa_4 = gpfwithinplant_dfa_4,
    ct_region_dfa_4 = region_dfa_4
from dim_part b, dim_date d,
tmp_dfa_01 e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and case when a.dd_plantcode =  'NL10' then a.country_destination_code else 'Not Set' end = e.country_destination_code*/

update tmp_fact_atlaspharmlogiforecast_merck a 
set ct_species_dfa_4 = species_dfa_4,
 ct_plant_dfa_4 = plant_dfa_4,
 ct_global_dfa_4 = global_dfa_4,
 ct_gpf_dfa_4 = gpf_dfa_4,
 ct_gpfwithinplant_dfa_4 = gpfwithinplant_dfa_4
 /*ct_region_dfa_4 = region_dfa_4*/
from tmp_fact_atlaspharmlogiforecast_merck a 
inner join dim_part b on a.dim_partid = b.dim_partid 
inner join dim_date d on a.dim_dateidreporting = d.dim_dateid 
inner join  tmp_dfa_01 e 
on  a.dd_plantcode = e.dd_plantcode /*and e.dd_plantcode = d.plantcode_factory*/
and a.dd_planttitlemerck = e.dd_planttitlemerck 
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck 
and a.dd_regionfordfa = e.dd_regionfordfa 
and case when a.dd_plantcode = 'NL10' then a.country_destination_code else 'Not Set' end = e.country_destination_code
and b.partnumber = e.partnumber 
and b.productfamily_merck = e.productfamily_merck 
and d.datevalue = e.datevalue 
where a.rn_dfa4 = 1 and a.dd_version = 'SFA' 
and a.dim_dateidreporting <> 1 ;


update tmp_fact_atlaspharmlogiforecast_merck
set ct_region_dfa_4 = 0
where ct_region_dfa_4 <> 0;

drop table if exists tmp_rowndfa_02;

create table tmp_rowndfa_02 as
select a.dd_plantcode,dd_planttitlemerck,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
row_number() over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4 = 0
where rn_dfa4 <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4 = b.RNDFA
from  tmp_rowndfa_02 b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;

drop table if exists tmp_dfa_01_2;

create table tmp_dfa_01_2 as
select d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end as country_destination_code,
abs(case when avg(ct_salescontribregion_partial)=0 then 0.00000 else cast(sum(ct_salescontribregion)/avg(ct_salescontribregion_partial) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,4))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,4))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as region_dfa_4
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end;

update tmp_fact_atlaspharmlogiforecast_merck a 
set 
 ct_region_dfa_4 = region_dfa_4
from tmp_fact_atlaspharmlogiforecast_merck a 
inner join dim_part b on a.dim_partid = b.dim_partid 
inner join dim_date d on a.dim_dateidreporting = d.dim_dateid 
inner join  tmp_dfa_01_2 e 
on  a.dd_plantcode = e.dd_plantcode /*and e.dd_plantcode = d.plantcode_factory*/
and a.dd_planttitlemerck = e.dd_planttitlemerck 
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck 
and a.dd_regionfordfa = e.dd_regionfordfa
and case when a.dd_plantcode = 'NL10' then a.country_destination_code else 'Not Set' end = e.country_destination_code
and b.partnumber = e.partnumber 
and b.productfamily_merck = e.productfamily_merck 
and d.datevalue = e.datevalue 
where a.rn_dfa4 = 1 and a.dd_version = 'SFA' 
and a.dim_dateidreporting <> 1 ;

drop table if exists tmp_dfa_01_2;

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salesdeliv3mthsIRU*ct_nasp) ct_salesdeliv3mthsIRUtot from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3) as ct_salesdeliv3mthsIRU ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplantavg  = case when ifnull( ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/b.ct_salesdeliv3mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg b , tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dd_plantcode = b.dd_plantcode  and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribplantavg  <> case when ifnull(ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/ct_salesdeliv3mthsIRUtot end;


drop table if exists fact_atlaspharmlogiforecast_merck_updContr4avg;
 create table fact_atlaspharmlogiforecast_merck_updContr4avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid, */ f1.dd_plantcode,f1.dd_planttitlemerck,
sum(ct_salescontribplant_partavg) ct_salescontribplant_partavg from
(
select distinct f.dim_partid,  /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and dd_forecast9mthisnull =1 then 0
else ct_salescontribplantavg
end as ct_salescontribplant_partavg
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA' and dd_source <> 'PRA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,  /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partialavg  = ct_salescontribplant_partavg
 from fact_atlaspharmlogiforecast_merck_updContr4avg b,  tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'SFA'
and a.ct_salescontribplant_partialavg <> ct_salescontribplant_partavg;


drop table if exists tmp_sfaavg_01;

create table tmp_sfaavg_01 as
select d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code,
abs(case when avg(ct_salescontribplant_partialavg)=0 then 0.00000 else cast(sum(ct_salescontribplantavg)/avg(ct_salescontribplant_partialavg) as decimal (18,5)) end)*
(CASE WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is not null THEN ( (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) )/2
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is not null THEN ( (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) )/2
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is null THEN ( (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) )/2
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is null THEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END)
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is null THEN (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END)
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is not null THEN (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END)
WHEN (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) is not null and (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) is not null THEN ( (CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00) WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00) WHEN (SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) >1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) <0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast6mth1)+SUM(ct_forecast6mth2)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1)=0 then 1 else SUM(ct_forecast6mth2)+SUM(ct_forecast6mth1) end) as decimal (18,4))) END) + (CASE WHEN (SUM(ct_forecast3mth)=0) AND (SUM(ct_salesmonth1)=0) THEN (100.00) WHEN (SUM(ct_forecast3mth)=0) THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))>1 THEN (0.00) WHEN (1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4)))<0 THEN (0.00) ELSE 100*(1-cast(abs(SUM(ct_forecast3mth)-SUM(ct_salesmonth1))/case when SUM(ct_forecast3mth)=0 then 1 else SUM(ct_forecast3mth) end as decimal (18,4))) END) )/3
ELSE NULL
END)
as plant_sfaavg_4
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code;


drop table if exists tmp_rowndfa_02;

create table tmp_rowndfa_02 as
select a.dd_plantcode,a.dd_planttitlemerck,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
row_number() over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4 = 0
where rn_dfa4 <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4 = b.RNDFA
from  tmp_rowndfa_02 b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;

/*call vectorwise(combine 'tmp_fact_atlaspharmlogiforecast_merck')*/

update tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_sfa_avg = 0
where ct_plant_sfa_avg <> 0 ;

/*update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from dim_part b, dim_date d,
tmp_sfaavg_01 e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code*/

update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from  tmp_fact_atlaspharmlogiforecast_merck a
inner join dim_part b on a.dim_partid = b.dim_partid
inner join dim_date d on a.dim_dateidreporting = d.dim_dateid
inner join tmp_sfaavg_01 e on 
a.dd_plantcode = e.dd_plantcode /*and e.dd_plantcode = d.plantcode_factory*/
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code
 and b.partnumber = e.partnumber and d.datevalue = e.datevalue
and b.productfamily_merck = e.productfamily_merck
where 
 a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;


drop table if exists tmp_dfa_01_subprodlev;

create table tmp_dfa_01_subprodlev as
select d.datevalue,b.sub_product_family_code_pma,b.ProductFamily_Merck,b.prodfamilydescription_merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end as country_destination_code,
abs(case when avg(ct_salescontribspecies_partial_subprodlev)=0 then 0.00000 else cast(sum(ct_salescontribspecies_subprodlev)/avg(ct_salescontribspecies_partial_subprodlev) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as species_dfa_4_subprodlev,
abs(case when avg(ct_salescontribplant_partial_subprodlev)=0 then 0.00000 else cast(sum(ct_salescontribplant_subprodlev)/avg(ct_salescontribplant_partial_subprodlev) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as plant_dfa_4_subprodlev,
abs(case when avg(ct_salescontribglobal_partial_subprodlev)=0 then 0.00000 else cast(sum(ct_salescontribglobal)/avg(ct_salescontribglobal_partial_subprodlev) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as global_dfa_4_subprodlev,
abs(case when avg(ct_salescontribgpf_partial_subprodlev)=0 then 0.00000 else cast(sum(ct_salescontribgpf)/avg(ct_salescontribgpf_partial_subprodlev) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as gpf_dfa_4_subprodlev,
abs(case when avg(ct_salescontribplanttogpf_partial_subprodlev)=0 then 0.00000 else cast(sum(ct_salescontribplanttogpf)/avg(ct_salescontribplanttogpf_partial_subprodlev) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as gpfwithinplant_dfa_4_subprodlev,
abs(case when avg(ct_salescontribregion_partial_subprodlev)=0 then 0.00000 else cast(sum(ct_salescontribregion_subprodlev)/avg(ct_salescontribregion_partial_subprodlev) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as region_dfa_4_subprodlev,
/* Madalina 12 Oct 2016 - Add field DFA Species within Plant-4 Subproduct Level BI-4115 */
abs(case when avg(ct_salescontribplanttospecies_partial_subprodlev)=0 then 0.00000 else cast(sum(ct_salescontribplanttospecies)/avg(ct_salescontribplanttospecies_partial_subprodlev) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as specieswithinplant_dfa_4_subprodlev
/* BI-4115 */
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.sub_product_family_code_pma,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,b.prodfamilydescription_merck,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end;


/*call vectorwise (combine 'tmp_fact_atlaspharmlogiforecast_merck')*/


drop table if exists tmp_rowndfa_02_subprodlev;

create table tmp_rowndfa_02_subprodlev as
select a.dd_plantcode,dd_planttitlemerck,b.dim_partid,a.dim_dateidreporting,country_destination_code,
row_number() over (partition by d.datevalue,b.sub_product_family_code_pma,b.ProductFamily_Merck,b.prodfamilydescription_merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4_subprodlev = 0
where rn_dfa4_subprodlev <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4_subprodlev = b.RNDFA
from  tmp_rowndfa_02_subprodlev b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;



update tmp_fact_atlaspharmlogiforecast_merck
set ct_species_dfa_4_subprodlev = 0
where ct_species_dfa_4_subprodlev <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_dfa_4_subprodlev = 0
where ct_plant_dfa_4_subprodlev <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_global_dfa_4_subprodlev = 0
where ct_global_dfa_4_subprodlev <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_gpf_dfa_4_subprodlev = 0
where ct_gpf_dfa_4_subprodlev <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_gpfwithinplant_dfa_4_subprodlev = 0
where ct_gpfwithinplant_dfa_4_subprodlev <> 0;

/* Madalina 12 Oct 2016 - Add field DFA Species within Plant-4 Subproduct Level*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_specieswithinplant_dfa_4_subprodlev = 0
where ct_specieswithinplant_dfa_4_subprodlev <> 0;
/* END BI-4115 */

update tmp_fact_atlaspharmlogiforecast_merck
set ct_region_dfa_4_subprodlev = 0
where ct_region_dfa_4_subprodlev <> 0;


update tmp_fact_atlaspharmlogiforecast_merck a
set ct_species_dfa_4_subprodlev = species_dfa_4_subprodlev,
    ct_plant_dfa_4_subprodlev = plant_dfa_4_subprodlev,
    ct_global_dfa_4_subprodlev = global_dfa_4_subprodlev,
    ct_gpf_dfa_4_subprodlev = gpf_dfa_4_subprodlev,
    ct_gpfwithinplant_dfa_4_subprodlev = gpfwithinplant_dfa_4_subprodlev,
    /*ct_region_dfa_4_subprodlev = region_dfa_4_subprodlev,*/
	ct_specieswithinplant_dfa_4_subprodlev = ifnull(specieswithinplant_dfa_4_subprodlev,0)  /* Madalina BI-4115 */
from dim_part b, dim_date d,
tmp_dfa_01_subprodlev e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4_subprodlev = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue /*and e.dd_plantcode = d.plantcode_factory*/
and ifnull(b.sub_product_family_code_pma,'Not Set') = ifnull(e.sub_product_family_code_pma,'Not Set')
and ifnull(b.productfamily_merck,'Not Set') = ifnull(e.productfamily_merck,'Not Set')
and ifnull(b.prodfamilydescription_merck,'Not Set') = ifnull(e.prodfamilydescription_merck,'Not Set')
and ifnull(a.dd_plantcode,'Not Set') = ifnull(e.dd_plantcode,'Not Set')
and ifnull(a.dd_planttitlemerck,'Not Set') = ifnull(e.dd_planttitlemerck,'Not Set')
and ifnull(a.DominantSpeciesDescription_Merck,'Not Set') = ifnull(e.DominantSpeciesDescription_Merck,'Not Set')
and ifnull(a.dd_regionfordfa,'Not Set') = ifnull(e.dd_regionfordfa,'Not Set')
and case when a.dim_plantid in (91,124) then a.country_destination_code else 'Not Set' end = e.country_destination_code;

 /*   Andrei 5.05.2017 -APP6116 */

drop table if exists tmp_dfa_01_subprodlev_2;


create table tmp_dfa_01_subprodlev_2 as
select d.datevalue,b.sub_product_family_code_pma,b.ProductFamily_Merck,b.prodfamilydescription_merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.DD_REGIONFORDFA,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end as country_destination_code,
abs(case when avg(ct_salescontribregion_partial_subprodlev)=0 then 0.00000 else cast(sum(ct_salescontribregion_subprodlev)/avg(ct_salescontribregion_partial_subprodlev) as decimal (18,8)) end)*(CASE WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0 AND SUM(ct_salesmonth1)+SUM(ct_salesmonth2)=0) THEN (100.00)
WHEN (SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)=0) THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) >1 THEN (0.00)
WHEN (1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1
else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) <0 THEN (0.00)
ELSE 100*(1-cast(abs(SUM(ct_forecast4mth1)+SUM(ct_forecast4mth2)
-SUM(ct_salesmonth1)-SUM(ct_salesmonth2))
/(case when SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1)=0 then 1 else SUM(ct_forecast4mth2)+SUM(ct_forecast4mth1) end) as decimal (18,8))) END)
as region_dfa_4_subprodlev
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.sub_product_family_code_pma,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.DD_REGIONFORDFA,b.prodfamilydescription_merck,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end;


/*call vectorwise (combine 'tmp_fact_atlaspharmlogiforecast_merck')*/


drop table if exists tmp_rowndfa_02_subprodlev;

 /*  end Andrei 5.05.2017 -APP6116 */

create table tmp_rowndfa_02_subprodlev as
select a.dd_plantcode,dd_planttitlemerck,b.dim_partid,a.dim_dateidreporting,country_destination_code,
row_number() over (partition by d.datevalue,b.sub_product_family_code_pma,b.ProductFamily_Merck,b.prodfamilydescription_merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.DD_REGIONFORDFA,
case when a.dim_plantid in (91,124) then country_destination_code else 'Not Set' end
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4_subprodlev = 0
where rn_dfa4_subprodlev <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4_subprodlev = b.RNDFA
from  tmp_rowndfa_02_subprodlev b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_region_dfa_4_subprodlev = 0
where ct_region_dfa_4_subprodlev <> 0;

 /*  end Andrei 5.05.2017 -APP6116 */
 
update tmp_fact_atlaspharmlogiforecast_merck a
set 
    ct_region_dfa_4_subprodlev = region_dfa_4_subprodlev

from dim_part b, dim_date d,
tmp_dfa_01_subprodlev_2 e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4_subprodlev = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue /*and e.dd_plantcode = d.plantcode_factory*/
and ifnull(b.sub_product_family_code_pma,'Not Set') = ifnull(e.sub_product_family_code_pma,'Not Set')
and ifnull(b.productfamily_merck,'Not Set') = ifnull(e.productfamily_merck,'Not Set')
and ifnull(b.prodfamilydescription_merck,'Not Set') = ifnull(e.prodfamilydescription_merck,'Not Set')
and ifnull(a.dd_plantcode,'Not Set') = ifnull(e.dd_plantcode,'Not Set')
and ifnull(a.dd_planttitlemerck,'Not Set') = ifnull(e.dd_planttitlemerck,'Not Set')
and ifnull(a.DominantSpeciesDescription_Merck,'Not Set') = ifnull(e.DominantSpeciesDescription_Merck,'Not Set')
and ifnull(a.DD_REGIONFORDFA,'Not Set') = ifnull(e.DD_REGIONFORDFA,'Not Set')
and case when a.dim_plantid in (91,124) then a.country_destination_code else 'Not Set' end = e.country_destination_code;

 /*  end Andrei 5.05.2017 -APP6116 */
 
 
/*Ambiguous replace fix: adding distinct in the creation statement*/
/*old logic disabled- BI-5583
drop table if exists tmp_latestforecastforrpmonth
create table tmp_latestforecastforrpmonth as
select a.* from
(select distinct  dense_rank() over (partition by a.dim_plantid,a.dim_partid,
reporting_company_code,country_destination_code order by d.datevalue desc) as RN,
d.datevalue as Snapshott,c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code,country_destination_code,
ct_adjsalesplan from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
--and b.partnumber = '000039'
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid
--and d.datevalue = '2015-12-01')
)a
where rn=1

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_latestforecastforrpmonth = b.ct_adjsalesplan
from tmp_latestforecastforrpmonth b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = c.dim_dateid
and c.datevalue = b.forecast
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_Partid
and a.dd_Version = 'DTA'
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
*/

/*start NEW LOGIC IMPLEMENTED BI-5583 - Alin GH*/
drop table if exists tmp_latestforecastforrpmonth_newlogic;
create table tmp_latestforecastforrpmonth_newlogic as

select distinct  
last_value(ct_adjsalesplan) over (partition by a.dim_plantid,a.dim_partid,
country_destination_code,c.CalendarMonthID  order by c.datevalue desc) as ct_adjsalesplan,
row_number() over ( partition by a.dim_plantid,a.dim_partid,country_destination_code,c.CalendarMonthID  order by d.datevalue desc) as row_num, 
d.datevalue as Snapshott,c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code,country_destination_code--,

from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_latestforecastforrpmonth = b.ct_adjsalesplan
from tmp_latestforecastforrpmonth_newlogic b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = c.dim_dateid
and c.datevalue = b.forecast
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_Partid
and a.dd_Version = 'DTA'
and row_num=1
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
and a.ct_latestforecastforrpmonth <> b.ct_adjsalesplan;

drop table if exists tmp_latestforecastforrpmonth_newlogic;
/*end NEW LOGIC IMPLEMENTED BI-5583 - Alin GH*/

update tmp_fact_atlaspharmlogiforecast_merck
set ct_budgetnext12month = 0
where ct_budgetnext12month <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_budgetnext12monthfpu = 0
where ct_budgetnext12monthfpu <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_budgetnext12monthbulk = 0
where ct_budgetnext12monthbulk <> 0;

/* 18 months*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_budgetnext18month = 0
where ct_budgetnext18month <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_budgetnext18monthfpu = 0
where ct_budgetnext18monthfpu <> 0;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_budgetnext18monthbulk = 0
where ct_budgetnext18monthbulk <> 0;

drop table if exists atlas_forecast_budget_upd_DCnext12month;
create table atlas_forecast_budget_upd_DCnext12month as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.budgetqty,0)) as budgetqtynext12month,
sum(ifnull(a2.budgetqtyfpu,0)) as budgetqtyfpunext12month,
sum(ifnull(a2.budgetqtybulk,0)) as budgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,year(b.datevalue) as Yyear,
month(b.datevalue) as mmonth,
b.datevalue as ddate,
budgetqty,
budgetqtyfpu,
budgetqtybulk
 from atlas_forecast_budget_upd_DC a, dim_date b
where a.dim_dateid = b.dim_dateid ) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,year(b.datevalue) as Yyear,
month(b.datevalue) as mmonth,
b.datevalue as ddate,
budgetqty,
budgetqtyfpu,
budgetqtybulk
 from atlas_forecast_budget_upd_DC a, dim_date b
where a.dim_dateid = b.dim_dateid ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_budgetnext12month = b.budgetqtynext12month,
 a.ct_budgetnext12monthfpu = b.budgetqtyfpunext12month,
 a.ct_budgetnext12monthbulk = b.budgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12month b, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
and (ifnull(a.ct_budgetnext12month,-1.999) <> b.budgetqtynext12month
OR ifnull(a.ct_budgetnext12monthfpu,-1.999) <> b.budgetqtyfpunext12month
OR ifnull(a.ct_budgetnext12monthbulk,-1.999) <> b.budgetqtybulknext12month);

/* budget 18 months*/
drop table if exists atlas_forecast_budget_upd_DCnext18month;
create table atlas_forecast_budget_upd_DCnext18month as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.budgetqty,0)) as budgetqtynext18month,
sum(ifnull(a2.budgetqtyfpu,0)) as budgetqtyfpunext18month,
sum(ifnull(a2.budgetqtybulk,0)) as budgetqtybulknext18month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,year(b.datevalue) as Yyear,
month(b.datevalue) as mmonth,
b.datevalue as ddate,
budgetqty,
budgetqtyfpu,
budgetqtybulk
 from atlas_forecast_budget_upd_DC a, dim_date b
where a.dim_dateid = b.dim_dateid ) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,year(b.datevalue) as Yyear,
month(b.datevalue) as mmonth,
b.datevalue as ddate,
budgetqty,
budgetqtyfpu,
budgetqtybulk
 from atlas_forecast_budget_upd_DC a, dim_date b
where a.dim_dateid = b.dim_dateid ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,18)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_budgetnext18month = b.budgetqtynext18month,
 a.ct_budgetnext18monthfpu = b.budgetqtyfpunext18month,
 a.ct_budgetnext18monthbulk = b.budgetqtybulknext18month
from atlas_forecast_budget_upd_DCnext18month b, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
and (ifnull(a.ct_budgetnext18month,-1.999) <> b.budgetqtynext18month
OR ifnull(a.ct_budgetnext18monthfpu,-1.999) <> b.budgetqtyfpunext18month
OR ifnull(a.ct_budgetnext18monthbulk,-1.999) <> b.budgetqtybulknext18month);

/* budget 18 months RF*/

/*2016 Budget RF Start */
drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v28;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v28 as
select distinct b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,
--cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(pra_period,1,4)),
case when 
(substr(pra_period,1,4)) < 10 
then concat('0',(substr(pra_period,5,2))) 
else (substr(pra_period,5,2)) end,'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,
--cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(pra_period,1,4)),
case when (substr(pra_period,1,4)) < 10 
then concat('0',(substr(pra_period,5,2))) else (substr(pra_period,5,2)) end,'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
--and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD')
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v28;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v28 as
select distinct dim_partid,dim_plantid,foreyear,foremonth,
--ddate,
date1,
reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v28
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
--cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(period,1,4)), substr(period,5,2),'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk 
from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and period is not null
and a.cocd = b.plant
and a.cocd = c.plantcode
and cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date)<='2016-03-01';



drop table if exists atlas_forecast_budget_upd_DCnext18monthrf;
create table atlas_forecast_budget_upd_DCnext18monthrf as
select distinct a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.foreyear,
a1.foremonth,
a1.date1,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext18month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext18month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext18month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear,
foremonth,
date1,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v28 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear, 
foremonth ,
date1,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v28 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
/*
and a1.yyear+1 = a2.yyear*/
and ((
 a2.date1 > to_date(concat(a1.foreyear,a1.foremonth,'01'),'YYYYMMDD') and
a2.date1 <=  add_months(to_date(concat(a1.foreyear,a1.foremonth  ,'01'),'YYYYMMDD'),18)))
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.foreyear,a1.foremonth,a1.date1;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnext18month = b.rfbudgetqtynext18month
 from atlas_forecast_budget_upd_DCnext18monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.foreyear = a.foreyear and b.foremonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue <= '2016-06-01'
and ifnull(a.ct_rfbudgetnext18month,-1.999) <> b.rfbudgetqtynext18month;

/*end budget 18 months RF*/

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salesdeliv3mthsIRU*ct_nasp) ct_salesdeliv3mthsIRUtot from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3) as ct_salesdeliv3mthsIRU ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplantavg  = case when ifnull( ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/b.ct_salesdeliv3mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg b ,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribplantavg  <> case when ifnull(ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/ct_salesdeliv3mthsIRUtot end;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr4avg;
 create table fact_atlaspharmlogiforecast_merck_updContr4avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salescontribplant_partavg) ct_salescontribplant_partavg from
(
select distinct f.dim_partid,  /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and dd_forecast9mthisnull =1 then 0
else ct_salescontribplantavg
end as ct_salescontribplant_partavg
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA' and dd_source <> 'PRA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,  /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partialavg  = ct_salescontribplant_partavg
 from fact_atlaspharmlogiforecast_merck_updContr4avg b , tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'SFA'
and a.ct_salescontribplant_partialavg <> ct_salescontribplant_partavg;



drop table if exists tmp_sfaavg_01;

create table tmp_sfaavg_01 as
select d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code,
abs(case when avg(ct_salescontribplant_partialavg)=0 then 0.00000 else  cast(sum(ct_salescontribplantavg)/avg(ct_salescontribplant_partialavg)as decimal (18,5)) end)*
(CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND      SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00)
      WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00)
      WHEN (1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) >1 THEN (0.00)
      WHEN (1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) <0 THEN (0.00)
      ELSE 100*(1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) END)
as plant_sfaavg_4
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code;


drop table if exists tmp_rowndfa_02;

create table tmp_rowndfa_02 as
select a.dd_plantcode,dd_planttitlemerck,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
row_number() over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,a.dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4 = 0
where rn_dfa4 <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4 = b.RNDFA
from  tmp_rowndfa_02 b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;


update tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_sfa_avg = 0
where ct_plant_sfa_avg <> 0 ;

/*update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from dim_part b, dim_date d,
tmp_sfaavg_01 e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code*/
update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from tmp_fact_atlaspharmlogiforecast_merck a
inner join dim_part b on a.dim_partid = b.dim_partid
inner join dim_date d on a.dim_dateidreporting = d.dim_dateid
inner join tmp_sfaavg_01 e on 
 a.dd_plantcode = e.dd_plantcode /*and e.dd_plantcode = d.plantcode_factory*/
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code and b.partnumber = e.partnumber and  b.productfamily_merck = e.productfamily_merck and d.datevalue = e.datevalue
where a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_dfa_4_subprodlev = 0
where ct_plant_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_global_dfa_4_subprodlev = 0
where ct_global_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpfwithinplant_dfa_4_subprodlev = 0
where ct_gpfwithinplant_dfa_4_subprodlev is null
and dd_version = 'SFA';

/* Madalina - BI-4115 */
update  tmp_fact_atlaspharmlogiforecast_merck
set ct_specieswithinplant_dfa_4_subprodlev = 0
where ct_specieswithinplant_dfa_4_subprodlev is null
and dd_version = 'SFA';
/* END BI-4115 */

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpf_dfa_4_subprodlev = 0
where ct_gpf_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpf_dfa_4_subprodlev = 0
where ct_gpf_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_region_dfa_4_subprodlev = 0
where ct_region_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_species_dfa_4_subprodlev = 0
where ct_species_dfa_4_subprodlev is null
and dd_version = 'SFA';

/*End of 4th script*/

/*5th script: 5 executed on 9June*/


update atlas_forecast_nasp_merck_dc_rf2_2016_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf2_2016_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf2_2016_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf2_2016_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf2_2016_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf2_2016_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf2_2016_v1
set nasp_global = 0
where nasp_global is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2
set pra_quantitybulk = 0
where pra_quantitybulk is null;

update atlas_forecast_nasp_merck_dc_rf3_2016_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf3_2016_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf3_2016_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf3_2016_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf3_2016_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf3_2016_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf3_2016_v1
set nasp_global = 0
where nasp_global is null;


update atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1
set pra_quantitybulk = 0
where pra_quantitybulk is null;

/*RF4*/

update atlas_forecast_nasp_merck_dc_rf4_2016_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf4_2016_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf4_2016_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf4_2016_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf4_2016_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf4_2016_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf4_2016_v1
set nasp_global = 0
where nasp_global is null;


update atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1
set pra_quantitybulk = 0
where pra_quantitybulk is null;


drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2016-03-01';

 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue <= '2016-06-01'
and (ifnull(a.ct_rfbudgetnextcalendaryear,-1.999) <> b.rfbudgetqtynextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearfpu,-1.999) <> b.rfbudgetqtyfpunextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearbulk,-1.999) <> b.rfbudgetqtybulknextyear);

drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/* 17 Feb 2017 Georgiana Changes */

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
  and  c.datevalue >= '2016-01-01'
and (ifnull(a.ct_rfbudgetnext12month,-1.999) <> b.rfbudgetqtynext12month
OR ifnull(a.ct_rfbudgetnext12monthfpu,-1.999) <> b.rfbudgetqtyfpunext12month
OR ifnull(a.ct_rfbudgetnext12monthbulk,-1.999) <> b.rfbudgetqtybulknext12month);

/*RF3*/

drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2016-03-01';

 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2016-07-01'
and (ifnull(a.ct_rfbudgetnextcalendaryear,-1.999) <> b.rfbudgetqtynextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearfpu,-1.999) <> b.rfbudgetqtyfpunextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearbulk,-1.999) <> b.rfbudgetqtybulknextyear);

drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/* 17 Feb 2018 Georgiana Changes */


/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
   and c.datevalue >= '2016-07-01' and c.datevalue <= '2016-09-01';
/*RF3 End*/

/*RF4*/


drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2016-03-01';

 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2016-10-01' and c.datevalue <= '2016-12-01'
and (ifnull(a.ct_rfbudgetnextcalendaryear,-1.999) <> b.rfbudgetqtynextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearfpu,-1.999) <> b.rfbudgetqtyfpunextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearbulk,-1.999) <> b.rfbudgetqtybulknextyear);

/*RF4 End */


drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2016-03-01';



drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
  and  c.datevalue >= '2016-10-01' and c.datevalue <='2016-12-01'
and (ifnull(a.ct_rfbudgetnext12month,-1.999) <> b.rfbudgetqtynext12month
OR ifnull(a.ct_rfbudgetnext12monthfpu,-1.999) <> b.rfbudgetqtyfpunext12month
OR ifnull(a.ct_rfbudgetnext12monthbulk,-1.999) <> b.rfbudgetqtybulknext12month);


update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasprf = 0
where ct_nasprf <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01'
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasprf = 0
where ct_nasprf <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01'
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;

/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasprf = 0
where ct_nasprf <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01'
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;


update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_cy = 0
where ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_cy = 0
where ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;

/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_cy = 0
where ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;


update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_ny = 0
where ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_ny = 0
where ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;

/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_ny = 0
where ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_cy = 0
where ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_cy = 0
where ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_cy = 0
where ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_py = 0
where ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;


update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_py = 0
where ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;

/*RF4*/

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_py = 0
where ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_global = 0
where ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_global = 0
where ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;

/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_global = 0
where ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;


update tmp_fact_atlaspharmlogiforecast_merck a
set dd_usedrfbudget = case when b.datevalue <'2014-01-01' then '2013'
                           when b.datevalue <'2015-01-01' then '2014'
                           when b.datevalue <'2016-01-01' then '2015'
                           when b.datevalue <'2016-04-01' then 'Budget 2016'
                           when b.datevalue <'2016-07-01' then'RF2 2016'
						   when b.datevalue <'2016-10-01' then 'RF3 2016'
						   when b.datevalue <'2017-01-01' then 'RF4 2016'
						   when b.datevalue <'2017-04-01' then 'Budget 2017'
						   else 'RF2 2017' end
from dim_date b,  tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid
and dd_version in ('DTA','SFA');


update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue<='2016-06-01';



drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2016-03-01';



update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue>='2016-07-01' and u.datevalue<='2016-09-01';

/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue>='2016-10-01' and u.datevalue<='2016-12-01';


drop table if exists tmp_atlas_forecast_SalesUpd_DC;
create table tmp_atlas_forecast_SalesUpd_DC as
select distinct pt.dim_partid, pl.dim_plantid,
reporting_company_code,
country_destination_code,
month(dt.datevalue) foremonth, year(dt.datevalue) foreyear,
dt.dim_dateid dim_dateidreporting, BUoM_Quantity ct_salesdelivered,
BUoM_Quantityfpu  ct_salesdeliveredfpu,
BUoM_Quantitybulk  ct_salesdeliveredbulk
from atlas_forecast_sales_merck_DC s,
dim_plant pl, dim_part pt, dim_date dt
where pl.plantcode = s.sales_cocd and pt.plant = pl.plantcode
and lpad(pt.partnumber,6,'0') = lpad(s.sales_uin,6,'0')
and dt.datevalue = to_date(case when sales_reporting_period is null then '00010101' else  concat(sales_reporting_period,'01') end,'YYYYMMDD')
and pl.companycode = dt.companycode;

/*Ambiguous replace fix: taking the mas value*/

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_salesdeliveredbulk = b.ct_salesdeliveredbulk
from ( select distinct b.dim_plantid,b.dim_partid, b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,b.dim_dateidreporting,max(b.ct_salesdeliveredbulk) as ct_salesdeliveredbulk
from tmp_atlas_forecast_SalesUpd_DC b
group by b.dim_plantid,b.dim_partid, b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,b.dim_dateidreporting)b,
tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
and a.foremonth = b.foremonth and a.foreyear = b.foreyear
and a.dim_dateidreporting = b.dim_dateidreporting
and ifnull(a.ct_salesdeliveredbulk,-1.999) <> b.ct_salesdeliveredbulk;

/*drop table if exists tmp_latestforecastforrpmonth
create table tmp_latestforecastforrpmonth as
select a.* from
(select dense_rank() over (partition by a.dim_plantid,a.dim_partid,
reporting_company_code,country_destination_code order by d.datevalue desc) as RN,
d.datevalue as Snapshott,c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code,country_destination_code,
ct_adjsalesplanbulk from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
--and b.partnumber = '000039'
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid
--and d.datevalue = '2015-12-01')
)a
where rn=1*/

drop table if exists tmp_latestforecastforrpmonth;
create table tmp_latestforecastforrpmonth as
select a.* from
(select distinct dense_rank() over (partition by a.dim_plantid,a.dim_partid,
reporting_company_code,country_destination_code order by d.datevalue desc) as RN,
d.datevalue as Snapshott,c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code,country_destination_code,
ct_adjsalesplanbulk from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
--and b.partnumber = '000039'
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid
--and d.datevalue = '2015-12-01')
)a
where rn=1;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_latestforecastforrpmonthbulk = b.ct_adjsalesplanbulk
from tmp_latestforecastforrpmonth b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = c.dim_dateid
and c.datevalue = b.forecast
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_Partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code;


/* 12 Apr 2017 Georgiana Changes: changed the logic for dd_reportavailable in order to be automatically updated to 'Yes' after each 9th of the month, according to BI-4374*/
/*update  tmp_fact_atlaspharmlogiforecast_merck a
set dd_reportavailable = case when b.datevalue<='2017-03-01' then 'Yes' else 'No' end
from dim_date b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid*/

update  tmp_fact_atlaspharmlogiforecast_merck a
set dd_reportavailable = case when b.datevalue <=(case when current_date >= to_date(to_char(concat(year(current_date),case when len(month(current_date))=1 then concat('0',month(current_date)) else month(current_date) end,'08')), 'YYYYMMDD') then add_months(current_date,-1) else add_months(current_date,-2) end) then 'Yes' else 'No' end
from dim_date b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid;

/* 12 Apr 2017 End*/

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
from  dim_date dd,  tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01' and dd.datevalue <'2016-07-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01' and dd.datevalue <'2016-07-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;


update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
 from  dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue <'2016-10-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01' and dd.datevalue <'2016-10-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;

/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
 from  dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <='2016-12-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01' and dd.datevalue <='2016-12-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;



drop table if exists tmp1_sales001;
create table tmp1_sales001 as
select b.dim_partid,c.dim_plantid,
substr(sales_reporting_period,1,4) as foreyear,
substr(sales_reporting_period,5,2) as foremonth,
cast(substr(sales_reporting_period,1,4)||'-'||substr(sales_reporting_period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,buom_quantity as pra_quantity,buom_quantityfpu as pra_quantityfpu,buom_quantitybulk as pra_quantitybulk
 from atlas_forecast_sales_merck_DC a, dim_part b, dim_plant c
where a.sales_uin = b.partnumber
and a.sales_cocd = b.plant
and a.sales_cocd = c.plantcode
union
select b.dim_partid,c.dim_plantid,
substr(sales_reporting_period,1,4) as foreyear,
substr(sales_reporting_period,5,2) as foremonth,
cast(substr(sales_reporting_period,1,4)||'-'||substr(sales_reporting_period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,buom_quantity as pra_quantity,buom_quantityfpu as pra_quantityfpu,buom_quantitybulk as pra_quantitybulk
 from atlas_forecast_sales_merck_DC a, dim_part b, dim_plant c
where a.sales_uin = b.partnumber
and a.sales_cocd = 'NL10'
and b.plant = 'XX20'
and b.plant = c.plantcode;



drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 as
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty as pra_quantity,budgetqtyfpu as pra_quantityfpu,budgetqtybulk as pra_quantitybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode;


drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth,reporting_company_code,country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth,
f.reporting_company_code,f.country_destination_code,
 ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth,reporting_company_code,country_destination_code;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetytd = b.ct_rfSalesbudget,
     a.ct_rfbudgetytdfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetytdbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2016-01-01' and c.datevalue<='2016-03-01'
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;


 drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear,x.reporting_company_code,x.country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear,f.reporting_company_code,f.country_destination_code,
ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk, f1.foremonth
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
  and f1.foreyear = f.foreyear  ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear,x.reporting_company_code,x.country_destination_code;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetyear = b.ct_rfSalesbudget,
     a.ct_rfbudgetyearfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetyearbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf b , dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and  b.foreyear = a.foreyear
  and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2016-01-01' and c.datevalue<='2016-03-01'
 and (ifnull(a.ct_rfbudgetyear,-1.999) <> b.ct_rfSalesbudget
 or ifnull(a.ct_rfbudgetyearfpu,-1.999) <> b.ct_rfSalesbudgetfpu
 or ifnull(a.ct_rfbudgetyearbulk,-1.999) <> b.ct_rfSalesbudgetbulk);


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 as
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty as pra_quantity,budgetqtyfpu as pra_quantityfpu,budgetqtybulk as pra_quantitybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode;





drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth,reporting_company_code,country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth,
f.reporting_company_code,f.country_destination_code,
 ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth,reporting_company_code,country_destination_code;
 

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetytd = b.ct_rfSalesbudget,
     a.ct_rfbudgetytdfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetytdbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2016-04-01' and c.datevalue <='2016-12-01'
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;

 
   drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v3 as
 select a.* from tmp1_sales001 a
where ddate<='2018-02-01'
union
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 a
where ddate>='2018-03-01';

drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2016_v2 b
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2016-04-01';

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2016-04-01' and c.datevalue<='2016-06-01'
  and a.dd_version = 'DTA';


drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2016_v1 b
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2016-04-01';

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2016-07-01' and c.datevalue<'2016-10-01'
  and a.dd_version = 'DTA';

  /*RF4*/
  
  
drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2016_v1 b 
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2016-04-01';

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2016-10-01' and c.datevalue <= '2016-12-01'
  and a.dd_version = 'DTA';
  
  /*RF4 END*/
  
 update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_DC u, dim_part pt,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and dd_naspused <> nasp_used;

update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01'
and f.dd_version = 'DTA';


update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf2_2016_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-04-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;

update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01'
and f.dd_version = 'DTA';


update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf3_2016_v1 u, dim_part pt, dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-07-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;

/* RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01'
and f.dd_version = 'DTA';


update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf4_2016_v1 u, dim_part pt, dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2016-10-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;

/*2016 Budget RF END*/

/*2017 Budget RF START*/
drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v28;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v28 as
select distinct b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,
--cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(pra_period,1,4)),
case when 
(substr(pra_period,1,4)) < 10 
then concat('0',(substr(pra_period,5,2))) 
else (substr(pra_period,5,2)) end,'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2    a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,
--cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(pra_period,1,4)),
case when (substr(pra_period,1,4)) < 10 
then concat('0',(substr(pra_period,5,2))) else (substr(pra_period,5,2)) end,'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2   a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
--and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD')
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v28;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v28 as
select distinct dim_partid,dim_plantid,foreyear,foremonth,
--ddate,
date1,
reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v28
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
--cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(period,1,4)), substr(period,5,2),'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk 
from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and period is not null
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) >='2017-01-01' and cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date)<='2017-12-01');



drop table if exists atlas_forecast_budget_upd_DCnext18monthrf;
create table atlas_forecast_budget_upd_DCnext18monthrf as
select distinct a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.foreyear,
a1.foremonth,
a1.date1,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext18month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext18month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext18month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear,
foremonth,
date1,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v28 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear, 
foremonth ,
date1,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v28 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
/*
and a1.yyear+1 = a2.yyear*/
and ((
 a2.date1 > to_date(concat(a1.foreyear,a1.foremonth,'01'),'YYYYMMDD') and
a2.date1 <=  add_months(to_date(concat(a1.foreyear,a1.foremonth  ,'01'),'YYYYMMDD'),18)))
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.foreyear,a1.foremonth,a1.date1;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnext18month = b.rfbudgetqtynext18month
 from atlas_forecast_budget_upd_DCnext18monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.foreyear = a.foreyear and b.foremonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2017-04-01'
and ifnull(a.ct_rfbudgetnext18month,-1.999) <> b.rfbudgetqtynext18month;

/*end budget 18 months RF*/

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salesdeliv3mthsIRU*ct_nasp) ct_salesdeliv3mthsIRUtot from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3) as ct_salesdeliv3mthsIRU ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplantavg  = case when ifnull( ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/b.ct_salesdeliv3mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg b ,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribplantavg  <> case when ifnull(ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/ct_salesdeliv3mthsIRUtot end;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr4avg;
 create table fact_atlaspharmlogiforecast_merck_updContr4avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salescontribplant_partavg) ct_salescontribplant_partavg from
(
select distinct f.dim_partid,  /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and dd_forecast9mthisnull =1 then 0
else ct_salescontribplantavg
end as ct_salescontribplant_partavg
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA' and dd_source <> 'PRA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,  /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partialavg  = ct_salescontribplant_partavg
 from fact_atlaspharmlogiforecast_merck_updContr4avg b , tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'SFA'
and a.ct_salescontribplant_partialavg <> ct_salescontribplant_partavg;



drop table if exists tmp_sfaavg_01;

create table tmp_sfaavg_01 as
select d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code,
abs(case when avg(ct_salescontribplant_partialavg)=0 then 0.00000 else  cast(sum(ct_salescontribplantavg)/avg(ct_salescontribplant_partialavg)as decimal (18,5)) end)*
(CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND      SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00)
      WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00)
      WHEN (1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) >1 THEN (0.00)
      WHEN (1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) <0 THEN (0.00)
      ELSE 100*(1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) END)
as plant_sfaavg_4
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code;


drop table if exists tmp_rowndfa_02;

create table tmp_rowndfa_02 as
select a.dd_plantcode,dd_planttitlemerck,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
row_number() over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,a.dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4 = 0
where rn_dfa4 <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4 = b.RNDFA
from  tmp_rowndfa_02 b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;


update tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_sfa_avg = 0
where ct_plant_sfa_avg <> 0 ;

/*update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from dim_part b, dim_date d,
tmp_sfaavg_01 e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code*/
update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from tmp_fact_atlaspharmlogiforecast_merck a
inner join dim_part b on a.dim_partid = b.dim_partid
inner join dim_date d on a.dim_dateidreporting = d.dim_dateid
inner join tmp_sfaavg_01 e on 
 a.dd_plantcode = e.dd_plantcode /*and e.dd_plantcode = d.plantcode_factory*/
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code and b.partnumber = e.partnumber and  b.productfamily_merck = e.productfamily_merck and d.datevalue = e.datevalue
where a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_dfa_4_subprodlev = 0
where ct_plant_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_global_dfa_4_subprodlev = 0
where ct_global_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpfwithinplant_dfa_4_subprodlev = 0
where ct_gpfwithinplant_dfa_4_subprodlev is null
and dd_version = 'SFA';

/* Madalina - BI-4115 */
update  tmp_fact_atlaspharmlogiforecast_merck
set ct_specieswithinplant_dfa_4_subprodlev = 0
where ct_specieswithinplant_dfa_4_subprodlev is null
and dd_version = 'SFA';
/* END BI-4115 */

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpf_dfa_4_subprodlev = 0
where ct_gpf_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpf_dfa_4_subprodlev = 0
where ct_gpf_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_region_dfa_4_subprodlev = 0
where ct_region_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_species_dfa_4_subprodlev = 0
where ct_species_dfa_4_subprodlev is null
and dd_version = 'SFA';

/*End of 4th script*/

/*5th script: 5 executed on 9June*/


update atlas_forecast_nasp_merck_dc_rf2_2017_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf2_2017_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf2_2017_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf2_2017_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf2_2017_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf2_2017_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf2_2017_v1
set nasp_global = 0
where nasp_global is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2  
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2  
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2  
set pra_quantitybulk = 0
where pra_quantitybulk is null;

update atlas_forecast_nasp_merck_dc_rf3_2017_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf3_2017_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf3_2017_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf3_2017_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf3_2017_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf3_2017_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf3_2017_v1
set nasp_global = 0
where nasp_global is null;


update atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1
set pra_quantitybulk = 0
where pra_quantitybulk is null;

/*RF4*/

update atlas_forecast_nasp_merck_dc_rf4_2017_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf4_2017_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf4_2017_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf4_2017_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf4_2017_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf4_2017_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf4_2017_v1
set nasp_global = 0
where nasp_global is null;


update atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1
set pra_quantitybulk = 0
where pra_quantitybulk is null;


drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2   a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2   a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (
cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2017-01-01' and
cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2017-03-01');


 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/*This will be used after we will receive the FR2 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2017-01-01' /*this will change after we will receive the RF3 file*/
and (ifnull(a.ct_rfbudgetnextcalendaryear,-1.999) <> b.rfbudgetqtynextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearfpu,-1.999) <> b.rfbudgetqtyfpunextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearbulk,-1.999) <> b.rfbudgetqtybulknextyear);


drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/* 17 Feb 2017 Georgiana Changes */

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
  and  c.datevalue >= '2017-01-01'
and (ifnull(a.ct_rfbudgetnext12month,-1.999) <> b.rfbudgetqtynext12month
OR ifnull(a.ct_rfbudgetnext12monthfpu,-1.999) <> b.rfbudgetqtyfpunext12month
OR ifnull(a.ct_rfbudgetnext12monthbulk,-1.999) <> b.rfbudgetqtybulknext12month);


/*RF3*/

drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2017-01-01'
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2017-12-01');

drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';

drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2017-01-01'
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2017-03-01');


 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/*This will be used after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2017-07-01' and c.datevalue <= '2017-09-01'
and (ifnull(a.ct_rfbudgetnextcalendaryear,-1.999) <> b.rfbudgetqtynextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearfpu,-1.999) <> b.rfbudgetqtyfpunextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearbulk,-1.999) <> b.rfbudgetqtybulknextyear);

drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/* 17 Feb 2018 Georgiana Changes */


/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
   and c.datevalue >= '2017-07-01' and c.datevalue <= '2017-09-01';
/*RF3 End*/

/*RF4*/


drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2017-01-01'
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2017-03-01');

drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';

drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2017-01-01'
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2017-03-01');


 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/*This will be used after we will receive RF4 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2017-10-01'
and (ifnull(a.ct_rfbudgetnextcalendaryear,-1.999) <> b.rfbudgetqtynextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearfpu,-1.999) <> b.rfbudgetqtyfpunextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearbulk,-1.999) <> b.rfbudgetqtybulknextyear);

/*RF4 End */






drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/* 17 Feb 2017 Georgiana Changes */


/*This will be use after we will receive RF4 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
  and  c.datevalue >= '2017-10-01'
and (ifnull(a.ct_rfbudgetnext12month,-1.999) <> b.rfbudgetqtynext12month
OR ifnull(a.ct_rfbudgetnext12monthfpu,-1.999) <> b.rfbudgetqtyfpunext12month
OR ifnull(a.ct_rfbudgetnext12monthbulk,-1.999) <> b.rfbudgetqtybulknext12month);

/*This will be used after we will receive RF2 file*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01'
and ct_nasprf <> 0;


update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf2_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01' and dd.datevalue<'2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasprf = 0
where ct_nasprf <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf3_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01' and dd.datevalue<='2017-09-01'
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;

/*RF4*/

/*This will be use after we will receive RF4 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasprf = 0
where ct_nasprf <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf4_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;

/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_fc_cy = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01'
and ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf2_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01' and dd.datevalue <'2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_cy = 0
where ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf3_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01' and dd.datevalue<='2017-09-01' 
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;


/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_cy = 0
where ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf4_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;


/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_fc_ny = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01'
and ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf2_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01' and dd.datevalue< '2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_ny = 0
where ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf3_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01' and dd.datevalue<='2017-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_ny = 0
where ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf4_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;


/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_cy = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01'
and ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf2_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01' and dd.datevalue<'2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_cy = 0
where ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf3_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01' and dd.datevalue<='2017-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_cy = 0
where ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf4_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_py = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01'
and ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf2_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01' and dd.datevalue< '2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_py = 0
where ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf3_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01' and dd.datevalue<='2017-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;

/*This will be use after we will receive RF4 File*/
/*RF4*/

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_py = 0
where ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf4_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;

/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_global = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01'
and ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf2_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01' and dd.datevalue<'2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_global = 0
where ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf3_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01' and dd.datevalue<='2017-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_global = 0
where ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf4_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;


update tmp_fact_atlaspharmlogiforecast_merck a
set dd_usedrfbudget = case when b.datevalue <'2014-01-01' then '2013'
                           when b.datevalue <'2015-01-01' then '2014'
                           when b.datevalue <'2016-01-01' then '2015'
                           when b.datevalue <'2016-04-01' then 'Budget 2016'
                           when b.datevalue <'2016-07-01' then'RF2 2016'
						   when b.datevalue <'2016-10-01' then 'RF3 2016'
						   when b.datevalue <'2017-01-01' then 'RF4 2016'
						   when b.datevalue <'2017-04-01' then 'Budget 2017'
						   when b.datevalue <'2017-07-01' then 'RF2 2017'
						   when b.datevalue <'2017-10-01' then 'RF3 2017'
						   else 'RF4 2017' end
from dim_date b,  tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid
and dd_version in ('DTA','SFA');


/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue >='2017-01-01' and u.datevalue<'2017-10-01'; /*This will change after we will receive RF3 file + added <condition in order to treat the materials that are not in RF3*/



/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue>='2017-07-01' and u.datevalue<='2017-09-01';

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue>='2017-10-01';


drop table if exists tmp_atlas_forecast_SalesUpd_DC;
create table tmp_atlas_forecast_SalesUpd_DC as
select distinct pt.dim_partid, pl.dim_plantid,
reporting_company_code,
country_destination_code,
month(dt.datevalue) foremonth, year(dt.datevalue) foreyear,
dt.dim_dateid dim_dateidreporting, BUoM_Quantity ct_salesdelivered,
BUoM_Quantityfpu  ct_salesdeliveredfpu,
BUoM_Quantitybulk  ct_salesdeliveredbulk
from atlas_forecast_sales_merck_DC s,
dim_plant pl, dim_part pt, dim_date dt
where pl.plantcode = s.sales_cocd and pt.plant = pl.plantcode
and lpad(pt.partnumber,6,'0') = lpad(s.sales_uin,6,'0')
and dt.datevalue = to_date(case when sales_reporting_period is null then '00010101' else  concat(sales_reporting_period,'01') end,'YYYYMMDD')
and pl.companycode = dt.companycode;

/*Ambiguous replace fix: taking the mas value*/

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_salesdeliveredbulk = b.ct_salesdeliveredbulk
from ( select distinct b.dim_plantid,b.dim_partid, b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,b.dim_dateidreporting,max(b.ct_salesdeliveredbulk) as ct_salesdeliveredbulk
from tmp_atlas_forecast_SalesUpd_DC b
group by b.dim_plantid,b.dim_partid, b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,b.dim_dateidreporting)b,
tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
and a.foremonth = b.foremonth and a.foreyear = b.foreyear
and a.dim_dateidreporting = b.dim_dateidreporting
and ifnull(a.ct_salesdeliveredbulk,-1.999) <> b.ct_salesdeliveredbulk;

/*drop table if exists tmp_latestforecastforrpmonth
create table tmp_latestforecastforrpmonth as
select a.* from
(select dense_rank() over (partition by a.dim_plantid,a.dim_partid,
reporting_company_code,country_destination_code order by d.datevalue desc) as RN,
d.datevalue as Snapshott,c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code,country_destination_code,
ct_adjsalesplanbulk from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
--and b.partnumber = '000039'
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid
--and d.datevalue = '2015-12-01')
)a
where rn=1*/

drop table if exists tmp_latestforecastforrpmonth;
create table tmp_latestforecastforrpmonth as
select a.* from
(select distinct dense_rank() over (partition by a.dim_plantid,a.dim_partid,
reporting_company_code,country_destination_code order by d.datevalue desc) as RN,
d.datevalue as Snapshott,c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code,country_destination_code,
ct_adjsalesplanbulk from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
--and b.partnumber = '000039'
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid
--and d.datevalue = '2015-12-01')
)a
where rn=1;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_latestforecastforrpmonthbulk = b.ct_adjsalesplanbulk
from tmp_latestforecastforrpmonth b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = c.dim_dateid
and c.datevalue = b.forecast
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_Partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code;


/* 12 Apr 2017 Georgiana Changes: changed the logic for dd_reportavailable in order to be automatically updated to 'Yes' after each 9th of the month, according to BI-4374*/
/*update  tmp_fact_atlaspharmlogiforecast_merck a
set dd_reportavailable = case when b.datevalue<='2017-03-01' then 'Yes' else 'No' end
from dim_date b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid*/

update  tmp_fact_atlaspharmlogiforecast_merck a
set dd_reportavailable = case when b.datevalue <=(case when current_date >= to_date(to_char(concat(year(current_date),case when len(month(current_date))=1 then concat('0',month(current_date)) else month(current_date) end,'08')), 'YYYYMMDD') then add_months(current_date,-1) else add_months(current_date,-2) end) then 'Yes' else 'No' end
from dim_date b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid;

/* 12 Apr 2017 End*/


/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
from  dim_date dd,  tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf2_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01' and dd.datevalue<'2017-10-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
 from  dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf3_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01' and dd.datevalue<='2017-09-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
 from  dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf4_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;



drop table if exists tmp1_sales001;
create table tmp1_sales001 as
select b.dim_partid,c.dim_plantid,
substr(sales_reporting_period,1,4) as foreyear,
substr(sales_reporting_period,5,2) as foremonth,
cast(substr(sales_reporting_period,1,4)||'-'||substr(sales_reporting_period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,buom_quantity as pra_quantity,buom_quantityfpu as pra_quantityfpu,buom_quantitybulk as pra_quantitybulk
 from atlas_forecast_sales_merck_DC a, dim_part b, dim_plant c
where a.sales_uin = b.partnumber
and a.sales_cocd = b.plant
and a.sales_cocd = c.plantcode
union
select b.dim_partid,c.dim_plantid,
substr(sales_reporting_period,1,4) as foreyear,
substr(sales_reporting_period,5,2) as foremonth,
cast(substr(sales_reporting_period,1,4)||'-'||substr(sales_reporting_period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,buom_quantity as pra_quantity,buom_quantityfpu as pra_quantityfpu,buom_quantitybulk as pra_quantitybulk
 from atlas_forecast_sales_merck_DC a, dim_part b, dim_plant c
where a.sales_uin = b.partnumber
and a.sales_cocd = 'NL10'
and b.plant = 'XX20'
and b.plant = c.plantcode;



drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 as
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty as pra_quantity,budgetqtyfpu as pra_quantityfpu,budgetqtybulk as pra_quantitybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode;


drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth,reporting_company_code,country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth,
f.reporting_company_code,f.country_destination_code,
 ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth,reporting_company_code,country_destination_code;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetytd = b.ct_rfSalesbudget,
     a.ct_rfbudgetytdfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetytdbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2017-01-01' and c.datevalue<='2017-03-01' /*'2017-03-01'*/
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;


 drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear,x.reporting_company_code,x.country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear,f.reporting_company_code,f.country_destination_code,
ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk, f1.foremonth
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
  and f1.foreyear = f.foreyear  ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear,x.reporting_company_code,x.country_destination_code;

 
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetyear = b.ct_rfSalesbudget,
     a.ct_rfbudgetyearfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetyearbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf b , dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and  b.foreyear = a.foreyear
  and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2017-01-01' and c.datevalue<='2017-03-01' /*'2017-03-01'*/
 and (ifnull(a.ct_rfbudgetyear,-1.999) <> b.ct_rfSalesbudget
 or ifnull(a.ct_rfbudgetyearfpu,-1.999) <> b.ct_rfSalesbudgetfpu
 or ifnull(a.ct_rfbudgetyearbulk,-1.999) <> b.ct_rfSalesbudgetbulk);



drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 as
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty as pra_quantity,budgetqtyfpu as pra_quantityfpu,budgetqtybulk as pra_quantitybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode;



drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth,reporting_company_code,country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth,
f.reporting_company_code,f.country_destination_code,
 ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth,reporting_company_code,country_destination_code;

/* This wil be used wne we will receive rf2 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetytd = b.ct_rfSalesbudget,
     a.ct_rfbudgetytdfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetytdbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2017-04-01'
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;

drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v3 as
 select a.* from tmp1_sales001 a
where ddate<='2018-02-01'
union
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 a
where ddate>='2018-03-01';

drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2017_v2 b
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2017-04-01';

 /* This wil be used wne we will receive rf2 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2017-04-01' and c.datevalue<='2017-12-01' /*2017-06-01*/
  and a.dd_version = 'DTA';


drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2017_v1 b
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2017-04-01';

 /* This wil be used wne we will receive rf3 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2017-07-01'
  and a.dd_version = 'DTA';

  /*RF4*/
  
  
drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2017_v1 b 
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2017-04-01';
 
/* This wil be used wne we will receive rf4 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2017-10-01'
  and a.dd_version = 'DTA';
  
  /*RF4 END*/
  
 update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_DC u, dim_part pt,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and dd_naspused <> nasp_used;

/* This wil be used wne we will receive rf2 file*/
update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01'
and f.dd_version = 'DTA';


update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf2_2017_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-04-01' and dd.datevalue<'2017-10-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;


/* This wil be used wne we will receive rf3 file*/
update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01' and dd.datevalue<='2017-09-01'
and f.dd_version = 'DTA';


update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf3_2017_v1 u, dim_part pt, dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-07-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;

/* This wil be used wne we will receive rf4 file*/
/* RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
and f.dd_version = 'DTA';


update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf4_2017_v1 u, dim_part pt, dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2017-10-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;
/*2017 Budget RF END*/


/*2018 Budget RF START*/
drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v28;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v28 as
select distinct b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,
--cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(pra_period,1,4)),
case when 
(substr(pra_period,1,4)) < 10 
then concat('0',(substr(pra_period,5,2))) 
else (substr(pra_period,5,2)) end,'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2    a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,
--cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(pra_period,1,4)),
case when (substr(pra_period,1,4)) < 10 
then concat('0',(substr(pra_period,5,2))) else (substr(pra_period,5,2)) end,'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2   a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
--and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD')
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v28;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v28 as
select distinct dim_partid,dim_plantid,foreyear,foremonth,
--ddate,
date1,
reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v28
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
--cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
to_date(concat((substr(period,1,4)), substr(period,5,2),'01'),'YYYYMMDD') as date1,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk 
from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and period is not null
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) >='2018-01-01' and cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date)<='2018-12-01');



drop table if exists atlas_forecast_budget_upd_DCnext18monthrf;
create table atlas_forecast_budget_upd_DCnext18monthrf as
select distinct a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.foreyear,
a1.foremonth,
a1.date1,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext18month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext18month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext18month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear,
foremonth,
date1,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v28 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear, 
foremonth ,
date1,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v28 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
/*
and a1.yyear+1 = a2.yyear*/
and ((
 a2.date1 > to_date(concat(a1.foreyear,a1.foremonth,'01'),'YYYYMMDD') and
a2.date1 <=  add_months(to_date(concat(a1.foreyear,a1.foremonth  ,'01'),'YYYYMMDD'),18)))
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.foreyear,a1.foremonth,a1.date1;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnext18month = b.rfbudgetqtynext18month
 from atlas_forecast_budget_upd_DCnext18monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.foreyear = a.foreyear and b.foremonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2018-04-01'
and ifnull(a.ct_rfbudgetnext18month,-1.999) <> b.rfbudgetqtynext18month;

/*end budget 18 months RF*/

drop table if exists fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg;
 create table fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salesdeliv3mthsIRU*ct_nasp) ct_salesdeliv3mthsIRUtot from
(
select distinct  f.dim_partid, /*f.dim_plantid*/ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3) as ct_salesdeliv3mthsIRU ,ct_nasp
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA'
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid*/ f1.dd_plantcode,dd_planttitlemerck;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplantavg  = case when ifnull( ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/b.ct_salesdeliv3mthsIRUtot end
 from fact_atlaspharmlogiforecast_merck_snp_DC_updContr2avg b ,tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid*/ a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.ct_salescontribplantavg  <> case when ifnull(ct_salesdeliv3mthsIRUtot,0)=0 then 0
 else 100* (ct_Salesmonth1+ct_Salesmonth2+ct_salesmonth3)*ct_nasp/ct_salesdeliv3mthsIRUtot end;

drop table if exists fact_atlaspharmlogiforecast_merck_updContr4avg;
 create table fact_atlaspharmlogiforecast_merck_updContr4avg as
select   f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear, /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck,
sum(ct_salescontribplant_partavg) ct_salescontribplant_partavg from
(
select distinct f.dim_partid,  /*f.dim_plantid, */ f.dd_plantcode,dd_planttitlemerck, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear,
case when dd_forecast3mthisnull =1 and dd_forecast6mthisnull =1 and dd_forecast9mthisnull =1 then 0
else ct_salescontribplantavg
end as ct_salescontribplant_partavg
from tmp_fact_atlaspharmlogiforecast_merck f
  where   dd_version = 'SFA' and dd_source <> 'PRA'
and fact_atlaspharmlogiforecast_merckid =1 and dim_dateidreporting <> 1
 ) f1
 group by  f1.snapmonth, f1.snapyear,
f1.foremonth, f1.foreyear,  /*f1.dim_plantid, */ f1.dd_plantcode,dd_planttitlemerck;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salescontribplant_partialavg  = ct_salescontribplant_partavg
 from fact_atlaspharmlogiforecast_merck_updContr4avg b , tmp_fact_atlaspharmlogiforecast_merck a
 where /*a.dim_plantid = b.dim_plantid */  a.dd_plantcode = b.dd_plantcode and a.dd_planttitlemerck = b.dd_planttitlemerck
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and  dd_version = 'SFA'
and a.ct_salescontribplant_partialavg <> ct_salescontribplant_partavg;



drop table if exists tmp_sfaavg_01;

create table tmp_sfaavg_01 as
select d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,
a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code,
abs(case when avg(ct_salescontribplant_partialavg)=0 then 0.00000 else  cast(sum(ct_salescontribplantavg)/avg(ct_salescontribplant_partialavg)as decimal (18,5)) end)*
(CASE WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 AND      SUM(ct_salesmonth1)+SUM(ct_salesmonth2)+SUM(ct_salesmonth3)=0) THEN (100.00)
      WHEN (SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0) THEN (0.00)
      WHEN (1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) >1 THEN (0.00)
      WHEN (1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) <0 THEN (0.00)
      ELSE 100*(1- cast(abs(SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)-SUM(ct_salesmonth1)-SUM(ct_salesmonth2)-SUM(ct_salesmonth3))/(case when SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3)=0 then 1 else SUM(ct_forecast9mth1)+SUM(ct_forecast9mth2)+SUM(ct_forecast9mth3) end)as decimal (18,8))) END)
as plant_sfaavg_4
 from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
group by d.datevalue,b.partnumber,b.ProductFamily_Merck,dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code;


drop table if exists tmp_rowndfa_02;

create table tmp_rowndfa_02 as
select a.dd_plantcode,dd_planttitlemerck,a.dim_partid,a.dim_dateidreporting,a.country_destination_code,
row_number() over (partition by d.datevalue,b.partnumber,b.ProductFamily_Merck,a.dd_plantcode,dd_planttitlemerck,a.DominantSpeciesDescription_Merck,a.dd_regionfordfa,
country_destination_code
order by d.datevalue) as RNDFA from tmp_fact_atlaspharmlogiforecast_merck a, dim_part b, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update tmp_fact_atlaspharmlogiforecast_merck
set rn_dfa4 = 0
where rn_dfa4 <> 0 ;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.rn_dfa4 = b.RNDFA
from  tmp_rowndfa_02 b, tmp_fact_atlaspharmlogiforecast_merck a
where  a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and a.dd_plantcode = b.dd_plantcode
and a.dd_planttitlemerck = b.dd_planttitlemerck
and a.dim_partid = b.dim_partid
and a.dim_dateidreporting = b.dim_dateidreporting
and a.country_destination_code = b.country_destination_code;


update tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_sfa_avg = 0
where ct_plant_sfa_avg <> 0 ;

/*update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from dim_part b, dim_date d,
tmp_sfaavg_01 e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1
and d.datevalue = e.datevalue
and b.partnumber = e.partnumber
and b.productfamily_merck = e.productfamily_merck
and a.dd_plantcode = e.dd_plantcode
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code*/
update tmp_fact_atlaspharmlogiforecast_merck a
set ct_plant_sfa_avg = plant_sfaavg_4
from tmp_fact_atlaspharmlogiforecast_merck a
inner join dim_part b on a.dim_partid = b.dim_partid
inner join dim_date d on a.dim_dateidreporting = d.dim_dateid
inner join tmp_sfaavg_01 e on 
 a.dd_plantcode = e.dd_plantcode /*and e.dd_plantcode = d.plantcode_factory*/
and a.dd_planttitlemerck = e.dd_planttitlemerck
and a.DominantSpeciesDescription_Merck = e.DominantSpeciesDescription_Merck
and a.dd_regionfordfa = e.dd_regionfordfa
and a.country_destination_code = e.country_destination_code and b.partnumber = e.partnumber and  b.productfamily_merck = e.productfamily_merck and d.datevalue = e.datevalue
where a.rn_dfa4 = 1
and a.dd_version = 'SFA'
and a.dim_dateidreporting <> 1;

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_plant_dfa_4_subprodlev = 0
where ct_plant_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_global_dfa_4_subprodlev = 0
where ct_global_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpfwithinplant_dfa_4_subprodlev = 0
where ct_gpfwithinplant_dfa_4_subprodlev is null
and dd_version = 'SFA';

/* Madalina - BI-4115 */
update  tmp_fact_atlaspharmlogiforecast_merck
set ct_specieswithinplant_dfa_4_subprodlev = 0
where ct_specieswithinplant_dfa_4_subprodlev is null
and dd_version = 'SFA';
/* END BI-4115 */

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpf_dfa_4_subprodlev = 0
where ct_gpf_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_gpf_dfa_4_subprodlev = 0
where ct_gpf_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_region_dfa_4_subprodlev = 0
where ct_region_dfa_4_subprodlev is null
and dd_version = 'SFA';

update  tmp_fact_atlaspharmlogiforecast_merck
set ct_species_dfa_4_subprodlev = 0
where ct_species_dfa_4_subprodlev is null
and dd_version = 'SFA';

/*End of 4th script*/

/*5th script: 5 executed on 9June*/


update atlas_forecast_nasp_merck_dc_rf2_2018_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf2_2018_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf2_2018_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf2_2018_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf2_2018_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf2_2018_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf2_2018_v1
set nasp_global = 0
where nasp_global is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2  
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2  
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2  
set pra_quantitybulk = 0
where pra_quantitybulk is null;

update atlas_forecast_nasp_merck_dc_rf3_2018_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf3_2018_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf3_2018_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf3_2018_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf3_2018_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf3_2018_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf3_2018_v1
set nasp_global = 0
where nasp_global is null;


update atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1
set pra_quantitybulk = 0
where pra_quantitybulk is null;

/*RF4*/

update atlas_forecast_nasp_merck_dc_rf4_2018_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf4_2018_v1
set nasp = 0
where nasp is null;

update atlas_forecast_nasp_merck_dc_rf4_2018_v1
set nasp_fc_cy = 0
where nasp_fc_cy is null;

update atlas_forecast_nasp_merck_dc_rf4_2018_v1
set nasp_fc_ny = 0
where nasp_fc_ny is null;

update atlas_forecast_nasp_merck_dc_rf4_2018_v1
set nasp_cy = 0
where nasp_cy is null;

update atlas_forecast_nasp_merck_dc_rf4_2018_v1
set nasp_py = 0
where nasp_py is null;

update atlas_forecast_nasp_merck_dc_rf4_2018_v1
set nasp_global = 0
where nasp_global is null;


update atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1
set pra_quantity = 0
where pra_quantity is null;

update atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1
set pra_quantityfpu = 0
where pra_quantityfpu is null;

update atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1
set pra_quantitybulk = 0
where pra_quantitybulk is null;


drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2   a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2   a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (
cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2018-01-01' and
cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2018-03-01'); /*THis will be changes when we will receive RF2 File*/


 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/*This will be used after we will receive the FR2 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2018-04-01' and c.datevalue <= '2018-06-01';

 /*this will change after we will receive the RF3 file
and (ifnull(a.ct_rfbudgetnextcalendaryear,-1.999) <> b.rfbudgetqtynextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearfpu,-1.999) <> b.rfbudgetqtyfpunextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearbulk,-1.999) <> b.rfbudgetqtybulknextyear)*/


drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/* 17 Feb 2018 Georgiana Changes */

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
  and  c.datevalue >= '2018-01-01'
and (ifnull(a.ct_rfbudgetnext12month,-1.999) <> b.rfbudgetqtynext12month
OR ifnull(a.ct_rfbudgetnext12monthfpu,-1.999) <> b.rfbudgetqtyfpunext12month
OR ifnull(a.ct_rfbudgetnext12monthbulk,-1.999) <> b.rfbudgetqtybulknext12month);


/*RF3*/

drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2018-01-01'
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2018-12-01');

drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';

drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2018-01-01'
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2018-03-01');


 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/*This will be used after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2018-07-01' and c.datevalue <= '2018-09-01'
/*and (ifnull(a.ct_rfbudgetnextcalendaryear,-1.999) <> b.rfbudgetqtynextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearfpu,-1.999) <> b.rfbudgetqtyfpunextyear
OR ifnull(a.ct_rfbudgetnextcalendaryearbulk,-1.999) <> b.rfbudgetqtybulknextyear)*/;

drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/* 17 Feb 2018 Georgiana Changes */


/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
   and c.datevalue >= '2018-07-01' and c.datevalue <= '2018-09-01';
/*RF3 End*/

/*RF4*/


drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v2;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v2 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';


drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v2;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v2 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk
from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v2
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2018-01-01'
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2018-03-01');

drop table if exists tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1;
create table tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 as
select b.dim_partid,c.dim_plantid,market_grouping,substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
 from atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a, dim_part b, dim_plant c, dim_date d
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode
and cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) = d.datevalue and c.plantcode = d.plantcode_factory
and c.companycode = d.companycode
union
select pt.dim_partid,pl.dim_plantid,a.market_grouping,substr(a.pra_period,1,4) as foreyear,
substr(a.pra_period,5,2) as foremonth,d.dim_dateid,cast(substr(a.pra_period,1,4)||'-'||substr(a.pra_period,5,2)||'-01' as date) as ddate,reporting_company_code,country_destination_code,
pra_quantity,pra_quantityfpu,pra_quantitybulk
from atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a,
 dim_part pt,
 dim_plant pl,
dim_date d
where pt.partnumber = a.pra_uin   and pl.plantcode = pt.plant
and d.datevalue = to_date(case when pra_period is null then '00010101' else concat(pra_period,'01') end,'YYYYMMDD') and pl.plantcode = d.plantcode_factory
and d.companycode = pl.companycode
and pt.plant = 'XX20' and  a.plant_code = 'NL10';

drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 as
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1
union
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty,budgetqtyfpu,budgetqtybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode
and (cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)>='2018-01-01'
and cast(substr(case when period is null then '000101' else period end,1,4)||'-'||substr(case when period is null then '000101' else period end,5,2)||'-01' as date)<='2018-03-01');


 drop table if exists atlas_forecast_budget_upd_DCnextyearrf;
create table atlas_forecast_budget_upd_DCnextyearrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynextyear,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunextyear,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknextyear from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as yyear,
foremonth as mmonth,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a1.yyear+1 = a2.yyear
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/*This will be used after we will receive RF4 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetnextcalendaryear = b.rfbudgetqtynextyear,
 a.ct_rfbudgetnextcalendaryearfpu = b.rfbudgetqtyfpunextyear,
 a.ct_rfbudgetnextcalendaryearbulk = b.rfbudgetqtybulknextyear
 from atlas_forecast_budget_upd_DCnextyearrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
 and dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue >= '2018-10-01';


/*RF4 End */






drop table if exists atlas_forecast_budget_upd_DCnext12monthrf;
create table atlas_forecast_budget_upd_DCnext12monthrf as
select a1.dim_partid,
a1.dim_plantid,
a1.reporting_company_code,
a1.country_destination_code,
a1.yyear,
a1.mmonth,
sum(ifnull(a2.pra_quantity,0)) as rfbudgetqtynext12month,
sum(ifnull(a2.pra_quantityfpu,0)) as rfbudgetqtyfpunext12month,
sum(ifnull(a2.pra_quantitybulk,0)) as rfbudgetqtybulknext12month from
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a) a1
left join
(select a.dim_partid,a.dim_plantid,reporting_company_code,
country_destination_code,foreyear as Yyear,
foremonth as mmonth,
ddate,
ifnull(pra_quantity,0) as pra_quantity,
ifnull(pra_quantityfpu,0) as pra_quantityfpu,
ifnull(pra_quantitybulk,0) as pra_quantitybulk
 from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a ) a2
on a1.dim_partid = a2.dim_partid
and a1.dim_plantid = a2.dim_plantid
and a1.reporting_company_code = a2.reporting_company_code
and a1.country_destination_code = a2.country_destination_code
and a2.ddate between add_months(a1.ddate,1) and add_months(a1.ddate,12)
group by a1.dim_partid,a1.dim_plantid,a1.reporting_company_code,a1.country_destination_code,a1.yyear,a1.mmonth;

/* 17 Feb 2018 Georgiana Changes */


/*This will be use after we will receive RF4 File*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetnext12month = b.rfbudgetqtynext12month,
 a.ct_rfbudgetnext12monthfpu = b.rfbudgetqtyfpunext12month,
 a.ct_rfbudgetnext12monthbulk = b.rfbudgetqtybulknext12month
from atlas_forecast_budget_upd_DCnext12monthrf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and  b.yyear = a.foreyear and b.mmonth = a.foremonth
  and dd_version = 'DTA'
  and a.dim_dateidreporting = c.dim_dateid
  and  c.datevalue >= '2018-10-01';

/*This will be used after we will receive RF2 file*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01'
and ct_nasprf <> 0;


update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf2_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01' and dd.datevalue < '2018-07-01'
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasprf = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01'
and ct_nasprf <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf3_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' /*and dd.datevalue<='2018-09-01'*/
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;

/*RF4*/

/*This will be use after we will receive RF4 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasprf = 0
where ct_nasprf <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasprf = nasp
from
atlas_forecast_nasp_merck_dc_rf4_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
--and f.dd_version = 'DTA'
and ifnull(ct_nasprf,-1.999)<> nasp;

/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_fc_cy = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01'
and ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf2_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01' and dd.datevalue <'2018-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_cy = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
and ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf3_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;


/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_cy = 0
where ct_nasp_fc_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_cy = u.nasp_fc_cy
from
atlas_forecast_nasp_merck_dc_rf4_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_cy,-1.999)<> u.nasp_fc_cy;


/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_fc_ny = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01'
and ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf2_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01' and dd.datevalue< '2018-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_ny = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
and ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf3_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_fc_ny = 0
where ct_nasp_fc_ny <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_fc_ny = u.nasp_fc_ny
from
atlas_forecast_nasp_merck_dc_rf4_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_fc_ny,-1.999)<> u.nasp_fc_ny;


/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_cy = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01'
and ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf2_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01' and dd.datevalue<'2018-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_cy = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
and ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf3_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_cy = 0
where ct_nasp_cy <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_cy = u.nasp_cy
from
atlas_forecast_nasp_merck_dc_rf4_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_cy,-1.999)<> u.nasp_cy;

/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_py = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01'
and ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf2_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01' and dd.datevalue< '2018-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_py = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue >='2018-07-01' and dd.datevalue <='2018-09-01'
and ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf3_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;

/*This will be use after we will receive RF4 File*/
/*RF4*/

update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_py = 0
where ct_nasp_py <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_py = u.nasp_py
from
atlas_forecast_nasp_merck_dc_rf4_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_py,-1.999)<> u.nasp_py;

/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp_global = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01'
and ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf2_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01' and dd.datevalue<'2018-07-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_global = 0
from dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
and ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf3_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck
set ct_nasp_global = 0
where ct_nasp_global <> 0;

update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_nasp_global = u.nasp_global
from
atlas_forecast_nasp_merck_dc_rf4_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
--and f.dd_version = 'DTA'
and ifnull(f.ct_nasp_global,-1.999)<> u.nasp_global;


update tmp_fact_atlaspharmlogiforecast_merck a
set dd_usedrfbudget = case when b.datevalue <'2014-01-01' then '2013'
                           when b.datevalue <'2015-01-01' then '2014'
                           when b.datevalue <'2016-01-01' then '2015'
                           when b.datevalue <'2016-04-01' then 'Budget 2016'
                           when b.datevalue <'2016-07-01' then'RF2 2016'
						   when b.datevalue <'2016-10-01' then 'RF3 2016'
						   when b.datevalue <'2017-01-01' then 'RF4 2016'
						   when b.datevalue <'2017-04-01' then 'Budget 2017'
						   when b.datevalue <'2017-07-01' then 'RF2 2017'
						   when b.datevalue <'2017-10-01' then 'RF3 2017'
						   when b.datevalue <'2018-01-01' then 'RF4 2017'
						   when b.datevalue <'2018-04-01' then 'Budget 2018'
						   when b.datevalue <'2018-07-01' then 'RF2 2018'
						   when b.datevalue <'2018-10-01' then 'RF3 2018'
						   else 'RF4 2018' end
from dim_date b,  tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid
and dd_version in ('DTA','SFA');


/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue >='2018-04-01' and u.datevalue<'2018-07-01'; /*This will change after we will receive RF3 file + added <condition in order to treat the materials that are not in RF3*/



/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue>='2018-07-01' and u.datevalue<='2018-09-01';

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_Salesrfbudget = pra_quantity,
ct_Salesrfbudgetfpu = pra_quantityfpu,
ct_Salesrfbudgetbulk = pra_quantitybulk
from
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 b,
dim_date u, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_plantid = b.dim_plantid and f.dim_partid = b.dim_partid
and b.reporting_company_code = f.reporting_company_code
and b.country_destination_code = f.country_destination_code
and f.dim_dateidreporting = u.dim_dateid
and b.ddate = u.datevalue
and u.datevalue>='2018-10-01';


drop table if exists tmp_atlas_forecast_SalesUpd_DC;
create table tmp_atlas_forecast_SalesUpd_DC as
select distinct pt.dim_partid, pl.dim_plantid,
reporting_company_code,
country_destination_code,
month(dt.datevalue) foremonth, year(dt.datevalue) foreyear,
dt.dim_dateid dim_dateidreporting, BUoM_Quantity ct_salesdelivered,
BUoM_Quantityfpu  ct_salesdeliveredfpu,
BUoM_Quantitybulk  ct_salesdeliveredbulk
from atlas_forecast_sales_merck_DC s,
dim_plant pl, dim_part pt, dim_date dt
where pl.plantcode = s.sales_cocd and pt.plant = pl.plantcode
and lpad(pt.partnumber,6,'0') = lpad(s.sales_uin,6,'0')
and dt.datevalue = to_date(case when sales_reporting_period is null then '00010101' else  concat(sales_reporting_period,'01') end,'YYYYMMDD')
and pl.companycode = dt.companycode;

/*Ambiguous replace fix: taking the mas value*/

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_salesdeliveredbulk = b.ct_salesdeliveredbulk
from ( select distinct b.dim_plantid,b.dim_partid, b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,b.dim_dateidreporting,max(b.ct_salesdeliveredbulk) as ct_salesdeliveredbulk
from tmp_atlas_forecast_SalesUpd_DC b
group by b.dim_plantid,b.dim_partid, b.reporting_company_code,b.country_destination_code,b.foremonth,b.foreyear,b.dim_dateidreporting)b,
tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code
and a.foremonth = b.foremonth and a.foreyear = b.foreyear
and a.dim_dateidreporting = b.dim_dateidreporting
and ifnull(a.ct_salesdeliveredbulk,-1.999) <> b.ct_salesdeliveredbulk;

/*drop table if exists tmp_latestforecastforrpmonth
create table tmp_latestforecastforrpmonth as
select a.* from
(select dense_rank() over (partition by a.dim_plantid,a.dim_partid,
reporting_company_code,country_destination_code order by d.datevalue desc) as RN,
d.datevalue as Snapshott,c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code,country_destination_code,
ct_adjsalesplanbulk from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
--and b.partnumber = '000039'
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid
--and d.datevalue = '2015-12-01')
)a
where rn=1*/

drop table if exists tmp_latestforecastforrpmonth;
create table tmp_latestforecastforrpmonth as
select a.* from
(select distinct dense_rank() over (partition by a.dim_plantid,a.dim_partid,
reporting_company_code,country_destination_code order by d.datevalue desc) as RN,
d.datevalue as Snapshott,c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code,country_destination_code,
ct_adjsalesplanbulk from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
--and b.partnumber = '000039'
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid
--and d.datevalue = '2015-12-01')
)a
where rn=1;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_latestforecastforrpmonthbulk = b.ct_adjsalesplanbulk
from tmp_latestforecastforrpmonth b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = c.dim_dateid
and c.datevalue = b.forecast
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_Partid
and a.reporting_company_code = b.reporting_company_code
and a.country_destination_code = b.country_destination_code;


/* 12 Apr 2018 Georgiana Changes: changed the logic for dd_reportavailable in order to be automatically updated to 'Yes' after each 9th of the month, according to BI-4374*/
/*update  tmp_fact_atlaspharmlogiforecast_merck a
set dd_reportavailable = case when b.datevalue<='2018-03-01' then 'Yes' else 'No' end
from dim_date b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid*/

update  tmp_fact_atlaspharmlogiforecast_merck a
set dd_reportavailable = case when b.datevalue <=(case when current_date >= to_date(to_char(concat(year(current_date),case when len(month(current_date))=1 then concat('0',month(current_date)) else month(current_date) end,'08')), 'YYYYMMDD') then add_months(current_date,-1) else add_months(current_date,-2) end) then 'Yes' else 'No' end
from dim_date b,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = b.dim_dateid;

/* 12 Apr 2018 End*/


/*This will be use after we will receive RF2 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
from  dim_date dd,  tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf2_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01' and dd.datevalue<'2018-07-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;

/*This will be use after we will receive RF3 File*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
 from  dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf3_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;

/*This will be use after we will receive RF4 File*/
/*RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = 0
 from  dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set ct_nasp = nasp
from
atlas_forecast_nasp_merck_dc_rf4_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = u.uin
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
and f.dd_version = 'DTA'
and ifnull(ct_nasp,-1.999)<> nasp;



drop table if exists tmp1_sales001;
create table tmp1_sales001 as
select b.dim_partid,c.dim_plantid,
substr(sales_reporting_period,1,4) as foreyear,
substr(sales_reporting_period,5,2) as foremonth,
cast(substr(sales_reporting_period,1,4)||'-'||substr(sales_reporting_period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,buom_quantity as pra_quantity,buom_quantityfpu as pra_quantityfpu,buom_quantitybulk as pra_quantitybulk
 from atlas_forecast_sales_merck_DC a, dim_part b, dim_plant c
where a.sales_uin = b.partnumber
and a.sales_cocd = b.plant
and a.sales_cocd = c.plantcode
union
select b.dim_partid,c.dim_plantid,
substr(sales_reporting_period,1,4) as foreyear,
substr(sales_reporting_period,5,2) as foremonth,
cast(substr(sales_reporting_period,1,4)||'-'||substr(sales_reporting_period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,buom_quantity as pra_quantity,buom_quantityfpu as pra_quantityfpu,buom_quantitybulk as pra_quantitybulk
 from atlas_forecast_sales_merck_DC a, dim_part b, dim_plant c
where a.sales_uin = b.partnumber
and a.sales_cocd = 'NL10'
and b.plant = 'XX20'
and b.plant = c.plantcode;



drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 as
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty as pra_quantity,budgetqtyfpu as pra_quantityfpu,budgetqtybulk as pra_quantitybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode;


drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth,reporting_company_code,country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth,
f.reporting_company_code,f.country_destination_code,
 ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth,reporting_company_code,country_destination_code;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetytd = b.ct_rfSalesbudget,
     a.ct_rfbudgetytdfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetytdbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2018-01-01' and c.datevalue<='2018-03-01' /*this will be active once we will receive RF2 file /*'2018-03-01'*/
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;


 drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear,x.reporting_company_code,x.country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear,f.reporting_company_code,f.country_destination_code,
ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk, f1.foremonth
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
  and f1.foreyear = f.foreyear  ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear,x.reporting_company_code,x.country_destination_code;

 
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetyear = b.ct_rfSalesbudget,
     a.ct_rfbudgetyearfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetyearbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta4_DC_rf b , dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and  b.foreyear = a.foreyear
  and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2018-01-01' and c.datevalue<='2018-03-01'/* this will be active once we will receive RF2 file /*'2018-03-01'*/
 /*and (ifnull(a.ct_rfbudgetyear,-1.999) <> b.ct_rfSalesbudget
 or ifnull(a.ct_rfbudgetyearfpu,-1.999) <> b.ct_rfSalesbudgetfpu
 or ifnull(a.ct_rfbudgetyearbulk,-1.999) <> b.ct_rfSalesbudgetbulk)*/;



drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 as
select b.dim_partid,c.dim_plantid,
substr(period,1,4) as foreyear,
substr(period,5,2) as foremonth,
cast(substr(period,1,4)||'-'||substr(period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
budgetqty as pra_quantity,budgetqtyfpu as pra_quantityfpu,budgetqtybulk as pra_quantitybulk from atlas_forecast_budget_merck_DC a, dim_part b ,dim_plant c
where a.uin = b.partnumber
and a.cocd = b.plant
and a.cocd = c.plantcode;



drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth,reporting_company_code,country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth,
f.reporting_company_code,f.country_destination_code,
 ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth,reporting_company_code,country_destination_code;

/* This wil be used wne we will receive rf2 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetytd = b.ct_rfSalesbudget,
     a.ct_rfbudgetytdfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetytdbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2018-04-01' and c.datevalue<'2018-07-01'
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;
 
/* 22 Oct 2018 Georgiana Changes according to APP-10401*/ 
/*Adding missing update for RF3*/
drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v3 as
select b.dim_partid,c.dim_plantid,
substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,
cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
pra_quantity as pra_quantity,pra_quantityfpu as pra_quantityfpu,pra_quantitybulk as pra_quantitybulk from atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 a, dim_part b ,dim_plant c
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode;


 drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth,reporting_company_code,country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth,
f.reporting_company_code,f.country_destination_code,
 ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth,reporting_company_code,country_destination_code;

/* This wil be used wne we will receive rf3 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetytd = b.ct_rfSalesbudget,
     a.ct_rfbudgetytdfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetytdbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2018-07-01' and c.datevalue<'2018-10-01'
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;
 
 /*Adding missing update for RF4*/
drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v3 as
select b.dim_partid,c.dim_plantid,
substr(pra_period,1,4) as foreyear,
substr(pra_period,5,2) as foremonth,
cast(substr(pra_period,1,4)||'-'||substr(pra_period,5,2)||'-01' as date) as ddate,
reporting_company_code,country_destination_code,
pra_quantity as pra_quantity,pra_quantityfpu as pra_quantityfpu,pra_quantitybulk as pra_quantitybulk from atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a, dim_part b ,dim_plant c
where a.pra_uin = b.partnumber
and a.plant_code = b.plant
and a.plant_code = c.plantcode;


 drop table if exists fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf;
create table fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf as
select x.dim_partid, x.dim_plantid,  x.foreyear, x.foremonth,reporting_company_code,country_destination_code,
 sum( x.pra_quantity) ct_rfSalesbudget, sum( x.pra_quantityfpu) ct_rfSalesbudgetfpu,sum( x.pra_quantitybulk) ct_rfSalesbudgetbulk
  from (
select distinct f.dim_partid, f.dim_plantid,  f.foreyear, f.foremonth,
f.reporting_company_code,f.country_destination_code,
 ifnull(f1.pra_quantity,0) as pra_quantity,ifnull(f1.pra_quantityfpu,0) as pra_quantityfpu,ifnull(f1.pra_quantitybulk,0) as pra_quantitybulk
from tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v3 f,
tmp2_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v3 f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth)   ) x
 group by x.dim_partid, x.dim_plantid, x.foreyear, x.foremonth,reporting_company_code,country_destination_code;

/* This wil be used once we will receive rf4 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_rfbudgetytd = b.ct_rfSalesbudget,
     a.ct_rfbudgetytdfpu = b.ct_rfSalesbudgetfpu,
	 a.ct_rfbudgetytdbulk = b.ct_rfSalesbudgetbulk
 from fact_atlaspharmlogiforecast_merck_snp_upddta5_DC_rf b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = 'DTA'
 and a.dim_dateidreporting = c.dim_dateid
 and c.datevalue>='2018-10-01' /*and c.datevalue<='2018-12-01'*/
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;
 
/*End 22 Oct 2018*/

drop table if exists tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3;
create table tmp2_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v3 as
 select a.* from tmp1_sales001 a
where ddate<='2018-12-01'
union
select dim_partid,dim_plantid,foreyear,foremonth,ddate,reporting_company_code,country_destination_code,pra_quantity,pra_quantityfpu,pra_quantitybulk from tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 a
where ddate>='2019-01-01';

drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf2_2018_v2 b
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2018-04-01';

 /* This wil be used wne we will receive rf2 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2018-04-01'and c.datevalue<='2018-07-01'
  and a.dd_version = 'DTA';


drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf3_2018_v1 b
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2018-04-01';

 /* This wil be used wne we will receive rf3 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2018-07-01' and c.datevalue<='2018-09-01'
  and a.dd_version = 'DTA';

  /*RF4*/
  
  
drop table if exists tmp1_dates;
create table tmp1_dates as
select distinct b.datevalue as dt from tmp_fact_atlaspharmlogiforecast_merck a, dim_date b
where a.dim_dateidreporting = b.dim_dateid
and a.dd_version = 'DTA';

drop table if exists tmp_sales_cummulativ;
create table tmp_sales_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(pra_quantity) as pra_quantity,sum(pra_quantityfpu) as pra_quantityfpu, sum(pra_quantitybulk) as pra_quantitybulk from
(select c.dt,a.dim_partid,a.dim_plantid,a.reporting_company_code,a.country_destination_code,a.pra_quantity as pra_quantity,
a.pra_quantityfpu as pra_quantityfpu, a.pra_quantitybulk  from tmp1_sales001 a
,tmp1_dates c
where  a.ddate<=c.dt
and a.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rf2_cummulativ;
create table tmp_rf2_cummulativ as
select d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code,sum(rfpra_quantity) as rfpra_quantity,sum(rfpra_quantityfpu) as rfpra_quantityfpu,
sum(rfpra_quantitybulk) as rfpra_quantitybulk from
(select c.dt,b.dim_partid,b.dim_plantid,b.reporting_company_code,b.country_destination_code,b.pra_quantity as rfpra_quantity,
b.pra_quantityfpu as rfpra_quantityfpu,
b.pra_quantitybulk as rfpra_quantitybulk  from  tmp_atlas_forecast_pra_forecasts_merck_dc_rf4_2018_v1 b 
,tmp1_dates c
where   b.ddate>c.dt
and  b.foreyear = year(c.dt)) d
group by d.dt,d.dim_partid,d.dim_plantid,d.reporting_company_code,d.country_destination_code;

drop table if exists tmp_rfbudgetyear;
create table tmp_rfbudgetyear as
select a.dim_partid,a.dim_plantid,a.country_destination_code,a.reporting_company_code,a.dim_dateidreporting,
ifnull(b.pra_quantity,0)+ifnull(c.rfpra_quantity,0) as ct_rfbudgetyear ,
ifnull(b.pra_quantityfpu,0)+ifnull(c.rfpra_quantityfpu,0) as ct_rfbudgetyearfpu,
ifnull(b.pra_quantitybulk,0)+ifnull(c.rfpra_quantitybulk,0) as ct_rfbudgetyearbulk from tmp_fact_atlaspharmlogiforecast_merck a
  inner join dim_date a1
  on a.dim_dateidreporting = a1.dim_dateid
 inner join tmp_sales_cummulativ b
 on a.dim_partid = b.dim_partid
        and a.dim_plantid = b.dim_plantid
        and a.reporting_company_code = b.reporting_company_code
        and a.country_destination_code = b.country_destination_code
        and a1.datevalue = b.dt
 left join tmp_rf2_cummulativ c
  on a.dim_partid = c.dim_partid
        and a.dim_plantid = c.dim_plantid
        and a.reporting_company_code = c.reporting_company_code
        and a.country_destination_code = c.country_destination_code
        and a1.datevalue = c.dt
 where a.dd_version = 'DTA'
 and a1.datevalue>='2018-04-01';
 
/* This wil be used wne we will receive rf4 file*/
update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_rfbudgetyear = b.ct_rfbudgetyear,
    a.ct_rfbudgetyearfpu = b.ct_rfbudgetyearfpu,
    a.ct_rfbudgetyearbulk = b.ct_rfbudgetyearbulk
from tmp_rfbudgetyear b, dim_date c,tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
  and a.dim_plantid = b.dim_plantid
  and a.country_destination_code = b.country_destination_code
  and a.reporting_company_code = b.reporting_company_code
  and a.dim_dateidreporting = b.dim_dateidreporting
  and a.dim_dateidreporting = c.dim_dateid
  and c.datevalue>='2018-10-01'
  and a.dd_version = 'DTA';
  
  /*RF4 END*/
  
 update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_DC u, dim_part pt,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and dd_naspused <> nasp_used;

/* This wil be used wne we will receive rf2 file*/
update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01'
and f.dd_version = 'DTA';


update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf2_2018_v1 u, dim_part pt, dim_date dd, tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-04-01' and dd.datevalue<'2018-07-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;


/* This wil be used wne we will receive rf3 file*/
update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' /*and dd.datevalue<='2018-09-01'*/
and f.dd_version = 'DTA';


update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf3_2018_v1 u, dim_part pt, dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-07-01' and dd.datevalue<='2018-09-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;

/* This wil be used wne we will receive rf4 file*/
/* RF4*/
update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = 'Not Set'
from  dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
and f.dd_version = 'DTA';

update tmp_fact_atlaspharmlogiforecast_merck f
set dd_naspused = nasp_used
from
atlas_forecast_nasp_merck_dc_rf4_2018_v1 u, dim_part pt, dim_date dd,tmp_fact_atlaspharmlogiforecast_merck f
where  f.dim_partid = pt.dim_partid and pt.partnumber = lpad(u.uin,6,'0')
and f.reporting_company_code = u.reporting_company_code
and f.country_destination_code = u.country_destination_code
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue>='2018-10-01'
and f.dd_version = 'DTA'
and ifnull(dd_naspused,'Not Set')<> nasp_used;
/*2018 Budget RF END*/

/* Begin insert from S991 and S992 Sap Table Only A00 and 000 are added */
delete from tmp_fact_atlaspharmlogiforecast_merck
where dd_version IN ('A00','000');

Drop table if exists max_holder_atlasph;
Create table max_holder_atlasph
as
Select ifnull(max(fact_atlaspharmlogiforecast_merckid),0) as maxid
from tmp_fact_atlaspharmlogiforecast_merck;

insert into tmp_fact_atlaspharmlogiforecast_merck(
fact_atlaspharmlogiforecast_merckid,
dim_plantid,
dim_partid,
dim_companyid,
dim_dateidforecast,
dd_version,
dim_unitofmeasureidbase,
ct_salesdelivered,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_salesbudget,
dd_source,
dim_dateidsnapshot
)
select
max_holder_atlasph.maxid + row_number() over(order by '') fact_atlaspharmlogiforecast_merckid,
pl.dim_plantid,
pt.dim_partid,
dc.dim_companyid,
dt.dim_dateid dim_dateidforecast,
VRSIO	dd_version,
um.dim_unitofmeasureid dim_unitofmeasureidbase,
Z1PP_SALD  ct_salesdelivered,
Z1PP_SALR  ct_salesrequested,
Z1PP_FCST  ct_salesforecast,
Z1PP_PROM  ct_promotion,
Z1PP_ADJL  ct_adjforlogistics,
Z1PP_TADJ  ct_totaladj,
Z1PP_ADJSP  ct_adjsalesplan,
Z1PP_SALRM  ct_salesreqmod,
Z1PP_SALRA  ct_adjsalesreq,
Z1PP_BUDGT  ct_salesbudget,
'S991' dd_source,
crt.dim_dateid dim_Dateidsnapshot
from S991 s
inner join dim_plant pl on ifnull(pl.plantcode,'Not Set')= ifnull(WENUX,'Not Set')
inner join dim_part pt on ifnull(pt.partnumber,'Not Set') = ifnull(PMNUX,'Not Set') and ifnull(pt.plant,'Not Set') = ifnull(pl.plantcode,'Not Set')
inner join dim_company dc on ifnull(pl.companycode,'Not Set') = ifnull(dc.companycode,'Not Set')
inner join dim_date crt on ifnull(dc.companycode,'Not Set') = ifnull(crt.companycode,'Not Set') and ifnull(crt.datevalue,'00010101') = current_date and ifnull(pl.plantcode,'Not Set') = ifnull(crt.plantcode_factory,'Not Set')
inner join dim_date dt on ifnull(dt.datevalue,'00010101') = to_date(case when spmon=0 then '00010101' else concat(spmon,'01') end,'YYYYMMDD') and ifnull(pl.plantcode,'Not Set') = ifnull(dt.plantcode_factory,'Not Set')
and ifnull(dc.companycode,'Not Set') = ifnull(dt.companycode,'Not Set')
inner join dim_unitofmeasure um on ifnull(um.uom,'Not Set') = ifnull(BASME,'Not Set')
cross join  max_holder_atlasph;


Drop table if exists max_holder_atlasph;
Create table max_holder_atlasph
as
Select ifnull(max(fact_atlaspharmlogiforecast_merckid),0) as maxid
from tmp_fact_atlaspharmlogiforecast_merck;

insert into tmp_fact_atlaspharmlogiforecast_merck(
fact_atlaspharmlogiforecast_merckid,
dim_plantid,
dim_partid,
dim_companyid,
dim_dateidforecast,
dd_version,
dim_unitofmeasureidbase,
ct_salesdelivered,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_salesbudget,
dd_source,
dim_dateidsnapshot
)
select
max_holder_atlasph.maxid + row_number() over(order by '') fact_atlaspharmlogiforecast_merckid,
pl.dim_plantid,
pt.dim_partid,
dc.dim_companyid,
dt.dim_dateid dim_dateidforecast,
VRSIO	dd_version,
um.dim_unitofmeasureid dim_unitofmeasureidbase,
Z1PP_SALD  ct_salesdelivered,
Z1PP_SALR  ct_salesrequested,
Z1PP_FCST  ct_salesforecast,
Z1PP_PROM  ct_promotion,
Z1PP_ADJL  ct_adjforlogistics,
Z1PP_TADJ  ct_totaladj,
Z1PP_ADJSP  ct_adjsalesplan,
Z1PP_SALRM  ct_salesreqmod,
Z1PP_SALRA  ct_adjsalesreq,
Z1PP_BUDGT  ct_salesbudget,
'S992' dd_source,
crt.dim_dateid dim_Dateidsnapshot
from S992 s
inner join dim_plant pl on  ifnull(pl.plantcode,'Not Set') = ifnull(WENUX,'Not Set')
inner join dim_part pt on ifnull(pt.partnumber,'Not Set') = ifnull(PMNUX,'Not Set')  and ifnull(pt.plant,'Not Set') = ifnull(pl.plantcode,'Not Set')
inner join dim_company dc on  ifnull(pl.companycode,'Not Set') = ifnull(dc.companycode,'Not Set')
inner join  dim_date crt on  ifnull(dc.companycode,'Not Set') = ifnull(crt.companycode,'Not Set') and ifnull(crt.datevalue,'0001-01-01') = current_date and ifnull(pl.plantcode,'Not Set') = ifnull(crt.plantcode_factory,'Not Set')
inner join dim_date dt on ifnull(dt.datevalue,'0001-01-01') = to_date(case when spmon=0 then '00010101' else concat(spmon,'01') end,'YYYYMMDD') and ifnull(pl.plantcode,'Not Set') = ifnull(dt.plantcode_factory,'Not Set')
and ifnull(dc.companycode,'Not Set') = ifnull(dt.companycode,'Not Set')
inner join dim_unitofmeasure um on um.uom =ifnull(BASME,'Not Set')
cross join  max_holder_atlasph;


/* End insert from S991 and S992 Sap Table Only A00 and 000 are added */

/*START Update all sources with null for DTA and SFA version in order for not remaining S991 and S992*/
update tmp_fact_atlaspharmlogiforecast_merck
set dd_source = null
where dd_version in ('DTA','SFA');
/*END Update all sources with null for DTA and SFA version  in order for not remaining S991 and S992*/
------------------------------------------
drop table if exists demandforecat_averagesalesq_base;
create table demandforecat_averagesalesq_base as
select select_rn.*,
SUM(ifnull(salesdelivered,0)) OVER (PARTITION BY SUB_PRODUCT_FAMILY_CODE_PMA, dd_plantcode ORDER BY calendarmonthid ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as incrementalsum
from
(
select SUB_PRODUCT_FAMILY_CODE_PMA,dd_plantcode,calendarmonthid,monthyear,salesdelivered,row_number() over (partition by SUB_PRODUCT_FAMILY_CODE_PMA, dd_plantcode order by calendarmonthid) rn
from 
(
select dp.SUB_PRODUCT_FAMILY_CODE_PMA, f.dd_plantcode, dd.CalendarMonthID,dd.MonthYear, SUM(ct_salesdelivered) salesdelivered
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dp, dim_date dd
where dp.dim_partid = f.dim_partid
and dd.dim_dateid = f.dim_dateidreporting
group by dp.SUB_PRODUCT_FAMILY_CODE_PMA,f.dd_plantcode,dd.CalendarMonthID,dd.MonthYear
order by 1,2
) select_sum
) select_rn;

-------3 months
drop table if exists demandforecat_averagesalesq_base_3months;
create table demandforecat_averagesalesq_base_3months as
select SUB_PRODUCT_FAMILY_CODE_PMA,dd_plantcode,monthyear,calendarmonthid, salesdelivered, max(prevsalesdeliv3months) as prevsalesdeliv3months from (
select b1.SUB_PRODUCT_FAMILY_CODE_PMA,b1.dd_plantcode, b1.calendarmonthid, b1.monthyear,b1.salesdelivered,(SUM(b2.salesdelivered) OVER (PARTITION BY b1.SUB_PRODUCT_FAMILY_CODE_PMA, b1.dd_plantcode, b1.rn ORDER BY b1.rn))/3 as prevsalesdeliv3months
from 
demandforecat_averagesalesq_base b1 INNER JOIN demandforecat_averagesalesq_base b2
ON b1.SUB_PRODUCT_FAMILY_CODE_PMA = b2.SUB_PRODUCT_FAMILY_CODE_PMA AND b1.dd_plantcode = b2.dd_plantcode and b2.rn between b1.rn-2 and b1.rn
order by 1,2,4) t
group by SUB_PRODUCT_FAMILY_CODE_PMA,dd_plantcode,monthyear,calendarmonthid, salesdelivered
order by 1,2,4;

update tmp_fact_atlaspharmlogiforecast_merck f
SET CT_PREVSALESDELIV3MONTHS = prevsalesdeliv3months
FROM tmp_fact_atlaspharmlogiforecast_merck f, demandforecat_averagesalesq_base_3months t,dim_part dp, dim_date dd
WHERE f.dim_partid = dp.dim_partid
AND t.SUB_PRODUCT_FAMILY_CODE_PMA = dp.SUB_PRODUCT_FAMILY_CODE_PMA AND t.dd_plantcode = f.dd_plantcode
AND f.dim_dateidreporting = dd.dim_dateid
AND dd.monthyear = t.monthyear
AND CT_PREVSALESDELIV3MONTHS <> prevsalesdeliv3months;
------3 months

-------6 months
drop table if exists demandforecat_averagesalesq_base_6months;
create table demandforecat_averagesalesq_base_6months as
select SUB_PRODUCT_FAMILY_CODE_PMA,dd_plantcode,monthyear,calendarmonthid, salesdelivered, max(prevsalesdeliv6months) as prevsalesdeliv6months from (
select b1.SUB_PRODUCT_FAMILY_CODE_PMA,b1.dd_plantcode,b1.calendarmonthid, b1.monthyear,b1.salesdelivered,(SUM(b2.salesdelivered) OVER (PARTITION BY b1.SUB_PRODUCT_FAMILY_CODE_PMA, b1.dd_plantcode, b1.rn ORDER BY b1.rn))/6 as prevsalesdeliv6months
from 
demandforecat_averagesalesq_base b1 INNER JOIN demandforecat_averagesalesq_base b2
ON b1.SUB_PRODUCT_FAMILY_CODE_PMA = b2.SUB_PRODUCT_FAMILY_CODE_PMA AND b1.dd_plantcode = b2.dd_plantcode and b2.rn between b1.rn-5 and b1.rn
order by 1,2,4) t
group by SUB_PRODUCT_FAMILY_CODE_PMA,dd_plantcode,monthyear,calendarmonthid, salesdelivered
order by 1,2,4;

update tmp_fact_atlaspharmlogiforecast_merck f
SET CT_PREVSALESDELIV6MONTHS = prevsalesdeliv6months
FROM tmp_fact_atlaspharmlogiforecast_merck f, demandforecat_averagesalesq_base_6months t,dim_part dp, dim_date dd
WHERE f.dim_partid = dp.dim_partid
AND t.SUB_PRODUCT_FAMILY_CODE_PMA = dp.SUB_PRODUCT_FAMILY_CODE_PMA AND t.dd_plantcode = f.dd_plantcode
AND f.dim_dateidreporting = dd.dim_dateid
AND dd.monthyear = t.monthyear
AND CT_PREVSALESDELIV6MONTHS <> prevsalesdeliv6months;
-------6 months

-------12 months
drop table if exists demandforecat_averagesalesq_base_12months;
create table demandforecat_averagesalesq_base_12months as
select SUB_PRODUCT_FAMILY_CODE_PMA,dd_plantcode,monthyear,calendarmonthid,max(prevsalesdeliv12months) as prevsalesdeliv12months from (
select b1.SUB_PRODUCT_FAMILY_CODE_PMA,b1.dd_plantcode,b1.calendarmonthid, b1.monthyear,b1.salesdelivered,(SUM(b2.salesdelivered) OVER (PARTITION BY b1.SUB_PRODUCT_FAMILY_CODE_PMA, b1.dd_plantcode, b1.rn ORDER BY b1.rn))/12 as prevsalesdeliv12months
from 
demandforecat_averagesalesq_base b1 INNER JOIN demandforecat_averagesalesq_base b2
ON b1.SUB_PRODUCT_FAMILY_CODE_PMA = b2.SUB_PRODUCT_FAMILY_CODE_PMA AND b1.dd_plantcode = b2.dd_plantcode and b2.rn between b1.rn-11 and b1.rn
order by 1,2,4) t
group by SUB_PRODUCT_FAMILY_CODE_PMA,dd_plantcode,monthyear,calendarmonthid
order by 1,2,4;

update tmp_fact_atlaspharmlogiforecast_merck f
SET CT_PREVSALESDELIV12MONTHS = prevsalesdeliv12months
FROM tmp_fact_atlaspharmlogiforecast_merck f, demandforecat_averagesalesq_base_12months t,dim_part dp, dim_date dd
WHERE f.dim_partid = dp.dim_partid
AND t.SUB_PRODUCT_FAMILY_CODE_PMA = dp.SUB_PRODUCT_FAMILY_CODE_PMA AND t.dd_plantcode = f.dd_plantcode
AND f.dim_dateidreporting = dd.dim_dateid
AND dd.monthyear = t.monthyear
AND CT_PREVSALESDELIV12MONTHS <> prevsalesdeliv12months;


/*Georgiana Changes 21 Sept 2016 according to BI- 4153*/

drop table if exists tmp_latestforecastonmonth;
create table tmp_latestforecastonmonth as
select  
distinct last_value(ct_adjsalesplan) over (partition by a.dim_plantid,a.dim_partid,
country_destination_code,c.CalendarMonthID  order by c.datevalue desc) as ct,
row_number() over ( partition by a.dim_plantid,a.dim_partid,country_destination_code,c.CalendarMonthID  order by d.datevalue desc) as row_num, 
country_destination_code,
c.datevalue  as Forecast,a.dim_plantid,
a.dim_partid,reporting_company_code
from fact_atlaspharmlogiforecast_merck_DC a, dim_part b, dim_date c, dim_date d
where a.dim_partid = b.dim_partid
and a.dim_dateidforecast = c.dim_dateid
and a.dim_dateidsnapshot = d.dim_dateid
order by 3 ;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_latestforecastonmonth  = b.ct
from tmp_latestforecastonmonth b, dim_date c, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_dateidreporting = c.dim_dateid
and c.datevalue = b.forecast
and a.country_destination_code=b.country_destination_code
and a.dim_plantid = b.dim_plantid
and a.dim_partid = b.dim_Partid
and row_num=1
and a.ct_latestforecastonmonth  <> b.ct;

drop table if exists tmp_latestforecastonmonth;

drop table if exists tmp_for_previousyear;
create table tmp_for_previousyear as 
select dp.partnumber, dp.plant, dd.CalendarMonthID,dd.MonthYear,
f.country_destination_code,avg(ct_salesdelivered) as salesdelivered,
ifnull(lag(avg(ct_salesdelivered),12) over (partition by dp.partnumber,plant,f.country_destination_code order by dd.CalendarMonthID asc),0) as sumPY,
row_number() over (partition by dp.partnumber,plant,f.country_destination_code order by dd.CalendarMonthID asc) as rn
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dp, dim_date dd
where dp.dim_partid = f.dim_partid
and dd.dim_dateid = f.dim_dateidreporting
group by dp.partnumber, dp.plant, dd.CalendarMonthID,dd.MonthYear,f.country_destination_code
order by 1,2,5,3;

update tmp_fact_atlaspharmlogiforecast_merck f
SET ct_salesdeliveredPY = sumPY
FROM tmp_fact_atlaspharmlogiforecast_merck f, tmp_for_previousyear t,dim_part dp, dim_date dd
WHERE f.dim_partid = dp.dim_partid
AND t.partnumber = dp.partnumber AND t.plant = dp.plant
AND f.dim_dateidreporting = dd.dim_dateid
AND dd.monthyear = t.monthyear
and f.country_destination_code=t.country_destination_code
AND ct_salesdeliveredPY <> sumPY;


drop table if exists tmp_for_previous2years;
create table tmp_for_previous2years as 
select dp.partnumber, dp.plant, dd.CalendarMonthID,dd.MonthYear,
f.country_destination_code,avg(ct_salesdelivered) as salesdelivered,
ifnull(lag(avg(ct_salesdelivered),24) over (partition by dp.partnumber,plant,f.country_destination_code order by dd.CalendarMonthID asc),0) as sum2Y,
row_number() over (partition by dp.partnumber,plant,f.country_destination_code order by dd.CalendarMonthID asc) as rn
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part dp, dim_date dd
where dp.dim_partid = f.dim_partid
and dd.dim_dateid = f.dim_dateidreporting
group by dp.partnumber, dp.plant, dd.CalendarMonthID,dd.MonthYear,f.country_destination_code
order by 1,2,5,3;

update tmp_fact_atlaspharmlogiforecast_merck f
SET ct_salesdelivered2Y = sum2Y
FROM tmp_fact_atlaspharmlogiforecast_merck f, tmp_for_previous2years t,dim_part dp, dim_date dd
WHERE f.dim_partid = dp.dim_partid
AND t.partnumber = dp.partnumber AND t.plant = dp.plant
AND f.dim_dateidreporting = dd.dim_dateid
AND dd.monthyear = t.monthyear
and f.country_destination_code=t.country_destination_code
AND ct_salesdelivered2Y <> sum2Y;

drop table if exists fact_atlaspharmlogiforecast_merck_latestforecastytd;
create table fact_atlaspharmlogiforecast_merck_latestforecastytd as
select distinct f.dim_partid, f.dim_plantid,
 f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,sum( f1.ct_latestforecastonmonth ) as latestforecastytd
from (
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_latestforecastonmonth  
from tmp_fact_atlaspharmlogiforecast_merck
) f,
(
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_latestforecastonmonth  
from tmp_fact_atlaspharmlogiforecast_merck
) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
  and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 and f.dd_version = f1.dd_version  and (
 f1.foreyear = f.foreyear and f1.foremonth >= 1
 and f1.foremonth <= f.foremonth) 
---and f.foreyear='2016' and f.foremonth='9'
----and f1.dim_partid in ('115770','93248','46374','43465','116281','42753')
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;


update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_latestforecastytd = b.latestforecastytd
 from fact_atlaspharmlogiforecast_merck_latestforecastytd b,tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapmonth=b.snapmonth and  a.snapyear=b.snapyear
and ifnull(a.ct_latestforecastytd,-1.999) <> b.latestforecastytd;

/*Georgiana End of changes*/
drop table if exists tmp_for_previousyear;
drop table if exists tmp_for_previous2years;
drop table if exists fact_atlaspharmlogiforecast_merck_latestforecastytd;


/* Alin - 21 Oct 2016 - changes according to BI-4472 */
drop table if exists tmp_for_4_subprodlev1;
create table tmp_for_4_subprodlev1 as
select distinct t.ProductFamily_Merck,t.dd_plantcode,t.calendarmonthid, t.calendaryear,
AVG(ifnull(t.ct_gpfwithinplant_dfa_4_subprodlev_bymonth,0)) OVER (PARTITION BY t.ProductFamily_Merck,t.dd_plantcode,t.calendaryear order by rn)as cumulativesum
from (
select  distinct  d.ProductFamily_Merck,f.dd_plantcode,dd.calendarmonthid,calendaryear,
SUM(ifnull(ct_gpfwithinplant_dfa_4_subprodlev,0)) as ct_gpfwithinplant_dfa_4_subprodlev_bymonth,
row_number() over (PARTITION BY d.ProductFamily_Merck order by dd.datevalue asc) as rn
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part d, dim_date dd, dim_plant dp
where f.dim_partid = d.dim_partid
and f.dim_dateidreporting = dd.dim_dateid
and dp.dim_plantid = f.dim_plantidsfa
and f.dd_version = 'SFA'
and dd.datevalue <= current_date - interval '1'  month
group by d.ProductFamily_Merck,f.dd_plantcode,dd.calendarmonthid,dd.datevalue,calendaryear
order by calendarmonthid asc) t;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_gpfwithinplant_dfa_4_subprodlevytd=e.cumulativesum
from dim_part b, dim_date d,
tmp_for_4_subprodlev1 e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and e.calendarmonthid=d.calendarmonthid
and a.dd_version = 'SFA'
and e.dd_plantcode=a.dd_plantcode
and ifnull(b.productfamily_merck,'Not Set') = ifnull(e.productfamily_merck,'Not Set');

drop table if exists tmp_for_4_subprodlev1;


/* Alin End of Changes BI-4472*/

/* 8 Dec 2016 Georgiana changes according to BI-4374 adding dd_region column for market grouping*/
/*merge into tmp_fact_atlaspharmlogiforecast_merck a
using(
select distinct s.region,s.market_grouping
from region_marketing_grouping s
) t
on t.market_grouping=a.market_grouping
when matched then update set a.dd_region=ifnull(t.region,'Not Set')
where a.dd_region<>ifnull(t.region,'Not Set')*/

/* Alin - 09 Jan 2017 - changes according to BI-5039 */
drop table if exists temp_avg_hit_miss;
create table temp_avg_hit_miss as
select distinct t.ProductFamily_Merck, t.CalendarMonthID,t.monthyear, sum_ct_gpf_dfa_4,
AVG(ifnull(t.sum_ct_gpf_dfa_4,0)) OVER (PARTITION BY t.ProductFamily_Merck) as avg_hit_miss
from (
select  distinct  d.ProductFamily_Merck, dd.CalendarMonthID, dd.monthyear,
CASE WHEN SUM( (f.ct_gpf_dfa_4_subprodlev) ) > 64.5 THEN  100 ELSE  0 END as sum_ct_gpf_dfa_4
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part d, dim_date dd
where f.dim_partid = d.dim_partid
and f.dim_dateidreporting = dd.dim_dateid
and dd.datevalue >= current_date - interval '25'  month
and dd.datevalue <= current_date -interval '1' month
group by d.ProductFamily_Merck, dd.CalendarMonthID, dd.monthyear, dd.datevalue
order by dd.CalendarMonthID desc) t;


update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_avg_hit_miss = e.avg_hit_miss
from dim_part b, dim_date d, temp_avg_hit_miss e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and d.monthyear = e.monthyear
and d.datevalue >= current_date - interval '25'  month
and d.datevalue <= current_date - interval '1' month
and ifnull(b.productfamily_merck,'Not Set') = ifnull(e.productfamily_merck,'Not Set')
and a.ct_avg_hit_miss <> e.avg_hit_miss;

drop table if exists temp_avg_hit_miss;
/* Alin End of Changes BI-5039*/

/* Yogini - 09 March 2017 BI-5692 */

UPDATE tmp_fact_atlaspharmlogiforecast_merck f
SET f.dim_currentdateid = dt.dim_dateid
FROM tmp_fact_atlaspharmlogiforecast_merck f 
INNER JOIN dim_plant p ON f.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode AND dt.plantcode_factory = p.plantcode and dt.datevalue = (add_months(current_date, -1) - 8)
WHERE f.dim_currentdateid <> dt.dim_dateid;

/* End Yogini - 09 March 2017 BI-5692 */

/*06 Jun 2017 Georgiana adding ct_scheduleqtybaseuom from sales according to APP-6420*/

drop table if exists tmp_for_upd_from_sales;
create table tmp_for_upd_from_sales as 
select distinct s.dim_partid,s.dim_plantid,d1.calendarmonthid,dc.Country,sum(s.ct_scheduleqtybaseuom) as ct_scheduleqtybaseuom
from fact_salesorder  s
inner join dim_date d1 on s.dim_dateidactualgi=d1.dim_dateid
inner join dim_customer dc on dc.dim_customerid=s.dim_customerid
group by s.dim_partid,s.dim_plantid,d1.calendarmonthid,dc.Country;

merge into tmp_fact_atlaspharmlogiforecast_merck f
using ( select distinct f.dim_partid,f.dim_plantid,f.dim_dateidreporting ,dd_version,country_destination_code,reporting_company_code,
sum(s.ct_scheduleqtybaseuom) as ct_scheduleqtybaseuom
from   tmp_fact_atlaspharmlogiforecast_merck f
inner join dim_date d2 on f.dim_dateidreporting=d2.dim_dateid
inner join tmp_for_upd_from_sales s on s.dim_partid=f.dim_partid and s.dim_plantid=f.dim_plantid 
 and s.calendarmonthid=d2.calendarmonthid and s.country=country_destination_code
--and f.dim_partid='135182'
---and d2.datevalue='2017-01-01'
---and dd_version='DTA'
group by f.dim_partid,f.dim_plantid,f.dim_dateidreporting,dd_version,country_destination_code,reporting_company_code) t
on t.dim_partid=f.dim_partid
and t.dim_plantid=f.dim_plantid
and t.dim_dateidreporting=f.dim_dateidreporting
and t.dd_version=f.dd_version
and t.country_destination_code=f.country_destination_code
and t.reporting_company_code=f.reporting_company_code
when matched then update set f.ct_scheduleqtybaseuom=t.ct_scheduleqtybaseuom;
drop table if exists tmp_for_upd_from_sales;
/* 06 Jun 2017 End of changes*/

/*Alin 12 Jun 2017 APP-6472 */
drop table if exists tmp_for_4_subprodlev1_subspecies;
create table tmp_for_4_subprodlev1_subspecies as
select distinct t.dd_planttitlemerck, 
t.dd_plantcode,t.calendarmonthid, t.calendaryear,t.DominantSpeciesDescription_Merck, CT_SPECIESWITHINPLANT_DFA_4_SUBPRODLEV_bymonth,
AVG(ifnull(t.CT_SPECIESWITHINPLANT_DFA_4_SUBPRODLEV_bymonth,0)) OVER (PARTITION BY t.dd_planttitlemerck, t.DominantSpeciesDescription_Merck, t.dd_plantcode,t.calendaryear order by rn)as cumulativesum
from (
select  distinct  f.dd_planttitlemerck,
f.dd_plantcode,dd.calendarmonthid,calendaryear,d.DominantSpeciesDescription_Merck,
SUM(ifnull(CT_SPECIESWITHINPLANT_DFA_4_SUBPRODLEV,0)) as CT_SPECIESWITHINPLANT_DFA_4_SUBPRODLEV_bymonth,
row_number() over (PARTITION BY f.dd_planttitlemerck order by dd.datevalue asc) as rn
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part d, dim_date dd, dim_plant dp
where f.dim_partid = d.dim_partid
and f.dim_dateidreporting = dd.dim_dateid
and dp.dim_plantid = f.dim_plantidsfa
and f.dd_version = 'SFA'
and dd.datevalue <= current_date 
group by f.dd_planttitlemerck, 
f.dd_plantcode,dd.calendarmonthid,dd.datevalue,calendaryear,d.DominantSpeciesDescription_Merck
order by calendarmonthid asc
) t;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_gpfwithinplant_dfa_4_subprodlevytd_subspecies = e.cumulativesum
from dim_part b, dim_date d,
tmp_for_4_subprodlev1_subspecies e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and e.calendarmonthid=d.calendarmonthid
and a.dd_version = 'SFA'
and e.dd_plantcode=a.dd_plantcode
and e.DominantSpeciesDescription_Merck = b.DominantSpeciesDescription_Merck
and ifnull(e.dd_planttitlemerck,'Not Set') = ifnull(a.dd_planttitlemerck,'Not Set');

drop table if exists tmp_for_4_subprodlev1_subspecies;

/*Alin 16 Jun 2017 APP-6472 */
drop table if exists tmp_for_4_subprodfamilylev1;
create table tmp_for_4_subprodfamilylev1 as

select distinct t.dd_planttitlemerck, 
t.dd_plantcode,t.calendarmonthid, t.calendaryear,
CT_SPECIESWITHINPLANT_DFA_4_SUBPRODLEV_bymonth,
AVG(ifnull(t.CT_SPECIESWITHINPLANT_DFA_4_SUBPRODLEV_bymonth,0)) OVER (PARTITION BY t.dd_planttitlemerck, 
t.dd_plantcode,t.calendaryear order by rn)as cumulativesum
from (
select  distinct  f.dd_planttitlemerck,
f.dd_plantcode,dd.calendarmonthid,calendaryear,
SUM(ifnull(ct_plant_dfa_4_subprodlev,0)) as CT_SPECIESWITHINPLANT_DFA_4_SUBPRODLEV_bymonth,
row_number() over (PARTITION BY f.dd_planttitlemerck order by dd.datevalue asc) as rn
from tmp_fact_atlaspharmlogiforecast_merck f, dim_part d, dim_date dd, dim_plant dp
where f.dim_partid = d.dim_partid
and f.dim_dateidreporting = dd.dim_dateid
and dp.dim_plantid = f.dim_plantidsfa
and f.dd_version = 'SFA'
and dd.datevalue <= current_date - interval '1'  month
group by f.dd_planttitlemerck, 
f.dd_plantcode,dd.calendarmonthid,dd.datevalue,calendaryear
order by calendarmonthid asc
) t;

update tmp_fact_atlaspharmlogiforecast_merck a
set a.ct_gpfwithinplant_dfa_4_subprodfamilylevytd = e.cumulativesum
from dim_part b, dim_date d,
tmp_for_4_subprodfamilylev1 e, tmp_fact_atlaspharmlogiforecast_merck a
where a.dim_partid = b.dim_partid
and a.dim_dateidreporting = d.dim_dateid
and e.calendarmonthid=d.calendarmonthid
and a.dd_version = 'SFA'
and e.dd_plantcode=a.dd_plantcode
and ifnull(e.dd_planttitlemerck,'Not Set') = ifnull(a.dd_planttitlemerck,'Not Set')
and a.ct_gpfwithinplant_dfa_4_subprodfamilylevytd <> e.cumulativesum;

drop table if exists tmp_for_4_subprodfamilylev1;

/* Georgiana Changes 23 May 2017*/
/*Georgiana 26 Jun 2017 added  two new fields according to App-6420*/

drop table if exists tmp_fact_atlaspharmlogiforecast_merck_for_movingbaseunits;
create table tmp_fact_atlaspharmlogiforecast_merck_for_movingbaseunits as
select f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
sum(f1.ct_scheduleqtybaseuom)  ct_scheduleqtybaseuom
from (
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_scheduleqtybaseuom
from tmp_fact_atlaspharmlogiforecast_merck
) f,
(
select distinct dim_partid, dim_plantid, snapmonth, snapyear,
foremonth, foreyear, dd_version, ct_scheduleqtybaseuom,reporting_company_code,country_destination_code
from tmp_fact_atlaspharmlogiforecast_merck
) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
  and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 and f.dd_version = f1.dd_version   and ((
 f1.foreyear = f.foreyear and f1.foremonth <= f.foremonth) OR
(f1.foremonth >= case when f.foremonth=12 then 1 else f.foremonth+1 end
and f1.foreyear =  case when f.foremonth=12 then f.foreyear else f.foreyear-1 end))
group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_salesdelivmovingbaseunits = b.ct_scheduleqtybaseuom
 from tmp_fact_atlaspharmlogiforecast_merck_for_movingbaseunits b,tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
   and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth;


drop table if exists tmp_for_scheduleqtybaseuom_previousmonths;
create table tmp_for_scheduleqtybaseuom_previousmonths as
select f.dim_partid, f.dim_plantid,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
sum(f1.ct_scheduleqtybaseuom)  ct_scheduleqtybaseuompreviousmonths
from (
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_scheduleqtybaseuom
from tmp_fact_atlaspharmlogiforecast_merck
) f,
(
select distinct dim_partid, dim_plantid, snapmonth, snapyear,
foremonth, foreyear, dd_version, ct_scheduleqtybaseuom,reporting_company_code,country_destination_code
from tmp_fact_atlaspharmlogiforecast_merck
) f1
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
  and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 and f.dd_version = f1.dd_version   and (
 f1.foreyear = f.foreyear and f1.foremonth <= f.foremonth)
/*and f.dim_partid='135182'
and f.country_destination_code='PR'
and f.dd_version='DTA'
and f.foreyear='2017'*/
group by f.dim_partid, f.dim_plantid, f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;


drop table if exists tmp_for_scheduleqtybaseuom_nextrfbudgetmonths;
create table tmp_for_scheduleqtybaseuom_nextrfbudgetmonths as
select f.dim_partid, f.dim_plantid,
f.foremonth, f.foreyear, f.dd_version,f.reporting_company_code,f.country_destination_code,
    SUM( (case when prt.packingunit = 'Not Set' then 0 else prt.packingunit end) * (f1.ct_salesrfbudget) )  as ct_salesrfbudgetnextrfbudgetmonths
from (
select distinct dim_partid, dim_plantid, snapmonth, snapyear,reporting_company_code,country_destination_code,
foremonth, foreyear, dd_version, ct_salesrfbudget
from tmp_fact_atlaspharmlogiforecast_merck
) f,
(
select distinct dim_partid, dim_plantid, snapmonth, snapyear,
foremonth, foreyear, dd_version, ct_salesrfbudget,reporting_company_code,country_destination_code
from tmp_fact_atlaspharmlogiforecast_merck
) f1,
dim_part prt
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid
  and f.reporting_company_code = f1.reporting_company_code
 and f.country_destination_code = f1.country_destination_code
 and f.dd_version = f1.dd_version   and (
 f1.foreyear = f.foreyear and f1.foremonth > f.foremonth)
and f.dim_partid=prt.dim_partid
/*and f.dim_partid='135182'
and f.country_destination_code='US'
and f.dd_version='DTA'
and f.foreyear='2017'*/
group by f.dim_partid, f.dim_plantid, f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;



update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_actualsalesuomandnextrfbudget = b.ct_scheduleqtybaseuompreviousmonths+c.ct_salesrfbudgetnextrfbudgetmonths
 from tmp_fact_atlaspharmlogiforecast_merck a,tmp_for_scheduleqtybaseuom_previousmonths b,tmp_for_scheduleqtybaseuom_nextrfbudgetmonths c
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
   and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and a.dim_plantid = c.dim_plantid and a.dim_partid = c.dim_partid
   and a.reporting_company_code = c.reporting_company_code
 and a.country_destination_code = c.country_destination_code
 and a.dd_version = c.dd_version
 and  a.foreyear = c.foreyear and a.foremonth = c.foremonth
/*and a.dim_partid='135182'
and a.country_destination_code='US'
and a.dd_version='DTA'
and a.foreyear='2017'*/
and a.ct_actualsalesuomandnextrfbudget <> b.ct_scheduleqtybaseuompreviousmonths+c.ct_salesrfbudgetnextrfbudgetmonths;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_actualsalesuomandnextrfbudget = b.ct_scheduleqtybaseuompreviousmonths
from tmp_fact_atlaspharmlogiforecast_merck a,tmp_for_scheduleqtybaseuom_previousmonths b,dim_date d
 where
a.dim_dateidreporting=d.dim_dateid and datevalue like '%12-01' and a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
   and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
and ct_actualsalesuomandnextrfbudget=0;

/* 26 Jun 2017 End of changes*/
   

/*Alin 26 Jun 2017 APP-6766 */
drop table if exists tmp_indep1;
create table tmp_indep1 as
select distinct
x.calendarmonthid, b.partnumber,b.ProductFamilyDescription_Merck, c.plantcode,
sum(a.ct_plannedquantity) as plannedquantity
from fact_independentrequirements a, 
                 dim_part b, 
                 dim_plant c,
                 dim_date x
where a.dim_partid = b.dim_partid
and a.dim_plantid = c.dim_plantid
and a.dim_deliveryfinishdate = x.dim_dateid
and a.dd_versionindepreqmts = 'ZI'
group by x.calendarmonthid, b.partnumber,b.ProductFamilyDescription_Merck, c.plantcode;

drop table if exists tmp_atlas1;
create table tmp_atlas1 as
select distinct  y.calendarmonthid, f.plantcode, e.partnumber,e.ProductFamilyDescription_Merck, d.country_destination_code 
from tmp_fact_atlaspharmlogiforecast_merck d, 
                  dim_part e,
                  dim_plant f,
                  dim_date y
where  d.dim_partid = e.dim_partid
and d.dim_plantid = f.dim_plantid
and d.dim_dateidreporting = y.dim_dateid;


drop table if exists tmp_indep_atlas1;
create table tmp_indep_atlas1 as
select a.*, i.plannedquantity
from tmp_indep1 i, tmp_atlas1 a
where i.partnumber = a.partnumber 
and i.ProductFamilyDescription_Merck=a.ProductFamilyDescription_Merck
and i.plantcode = (
case when a.plantcode ='XX20' or (a.plantcode = 'NL10'and a.country_destination_code <> 'CN') THEN 'XX20' 
WHEN (a.plantcode = 'NL10' AND a.country_destination_code = 'CN') then 'CN20'
ELSE A.PLANTCODE END)
and i.calendarmonthid = a.calendarmonthid;

drop table if exists tmppppp;
create table tmppppp as 
select  DISTINCT
partnumber, plantcode, calendarmonthid,ProductFamilyDescription_Merck,
country_destination_code, plannedquantity,
first_value(country_destination_code) over (partition by calendarmonthid,plantcode, partnumber 
order by  calendarmonthid,plantcode, partnumber asc) as first_cc,
case when country_destination_code = substr(plantcode, 1,2) then plannedquantity
when plantcode = 'NL10' and country_destination_code <> 'NL'
then first_value(plannedquantity) over (partition by calendarmonthid,plantcode, partnumber 
order by  calendarmonthid,plantcode, partnumber, country_destination_code asc)
else 0 end as plannedquantity_new
from tmp_indep_atlas1;

update tmppppp
set plannedquantity_new = 0
from tmppppp
where plantcode = 'NL10' and country_destination_code <> 'NL'
and country_destination_code <> first_cc;


update tmp_fact_atlaspharmlogiforecast_merck f
set f.ct_plannedquantity = t.plannedquantity_new
from tmp_fact_atlaspharmlogiforecast_merck f, tmppppp t, dim_part d, dim_plant p, dim_date dd
where f.dim_partid = d.dim_partid
and f.dim_plantid = p.dim_plantid
and f.dim_dateidreporting = dd.dim_dateid
and t.calendarmonthid = dd.calendarmonthid
and t.partnumber = d.partnumber
and t.plantcode = p.plantcode
and f.country_destination_code = t.country_destination_code
and t.ProductFamilyDescription_Merck=d.ProductFamilyDescription_Merck;


drop table if exists tmp_indep1;
drop table if exists tmp_atlas1;
drop table if exists tmp_indep_atlas1;
drop table if exists tmppppp;

/*Alin APP-6766 29 Jun 2017*/
drop table  if exists temp1_12months_plannedqty;
 create table temp1_12months_plannedqty as  
select  f.dim_partid, f.dim_plantid, --f.snapmonth, f.snapyear, 
f.reporting_company_code,f.country_destination_code,
 f.foremonth, f.foreyear, f.dd_version,
 f1.ct_plannedquantity  
from tmp_fact_atlaspharmlogiforecast_merck f, 
( select  dim_plantid, dim_partid, foreyear, foremonth, 
to_date(concat(foreyear,case when foremonth < 10 then concat('0',foremonth) else foremonth end,'01'),'YYYYMMDD') as date1, 
reporting_company_code,country_destination_code, --snapmonth,snapyear, 
dd_version, ct_plannedquantity  
from tmp_fact_atlaspharmlogiforecast_merck a  
where foremonth is not null
 ) f1 
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid and f.reporting_company_code = f1.reporting_company_code  
and f.country_destination_code = f1.country_destination_code  and f.dd_version = f1.dd_version   and ((
  f1.date1 > to_date(concat(f.foreyear,case when f.foremonth <10 then concat('0',f.foremonth) else f.foremonth end,'01'),'YYYYMMDD') 
and f1.date1 <=  add_months(to_date(concat(f.foreyear,case when f.foremonth <10 then concat('0',f.foremonth) else f.foremonth end ,'01'),'YYYYMMDD'),12)));

drop table  if exists temp2_12months_plannedqty;
create table temp2_12months_plannedqty as 
select distinct f.dim_partid, f.dim_plantid, --f.snapmonth, f.snapyear,
f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
sum(f.ct_plannedquantity) as ct_planneedqty12months
from temp1_12months_plannedqty f
 group by f.dim_partid, f.dim_plantid, --f.snapmonth, f.snapyear,
 f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;

update tmp_fact_atlaspharmlogiforecast_merck a
 set a.ct_planneedqty12months = b.ct_planneedqty12months
 from temp2_12months_plannedqty b,tmp_fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 --and a.snapmonth = b.snapmonth and a.snapyear = b.snapyear
and ifnull(a.ct_planneedqty12months,-1.999) <> b.ct_planneedqty12months;



 update tmp_fact_atlaspharmlogiforecast_merck
set  ct_ytdplusforecast  =0 where ct_ytdplusforecast  is null;

update tmp_fact_atlaspharmlogiforecast_merck
set  ct_ytdplusforecastfpu  =0 where ct_ytdplusforecastfpu  is null;

update tmp_fact_atlaspharmlogiforecast_merck
set  ct_ytdplusforecastbulk  =0 where ct_ytdplusforecastbulk  is null;

delete from fact_atlaspharmlogiforecast_merck;
insert into fact_atlaspharmlogiforecast_merck select * from tmp_fact_atlaspharmlogiforecast_merck;

delete from tmp_fact_atlaspharmlogiforecast_merck;



/* 07 Nov 2017 Georgiana CHanges according to APP-8012 optimizing temp1_18months*/
/*upd 9 - 18 months feature*/
drop table  if exists temp1_18months;
 create table temp1_18months as  
select distinct f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear, f.reporting_company_code,f.country_destination_code,
 f.foremonth, f.foreyear, f.dd_version,
 ---f1.date1,
 f1.ct_adjsalesplan  
from fact_atlaspharmlogiforecast_merck f, 
( select distinct dim_plantid, dim_partid, foreyear, foremonth, 
to_date(concat(foreyear,case when foremonth < 10 then concat('0',foremonth) else foremonth end,'01'),'YYYYMMDD') as date1, 
reporting_company_code,country_destination_code, snapmonth,snapyear, dd_version, ct_adjsalesplan  
from fact_atlaspharmlogiforecast_merck a  
where foremonth is not null
 ) f1 
where f.dim_plantid = f1.dim_plantid and f.dim_partid = f1.dim_partid and f.reporting_company_code = f1.reporting_company_code  
and f.country_destination_code = f1.country_destination_code  and f.dd_version = f1.dd_version   and ((
  f1.date1 > to_date(concat(f.foreyear,case when f.foremonth <10 then concat('0',f.foremonth) else f.foremonth end,'01'),'YYYYMMDD') 
and f1.date1 <=  add_months(to_date(concat(f.foreyear,case when f.foremonth <10 then concat('0',f.foremonth) else f.foremonth end ,'01'),'YYYYMMDD'),18)));

drop table  if exists temp2_18months;
create table temp2_18months as 
select distinct f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version,
---f1.date1,
sum(f.ct_adjsalesplan) as ct_forecastnext18months
from temp1_18months f
 group by f.dim_partid, f.dim_plantid, f.snapmonth, f.snapyear,f.reporting_company_code,f.country_destination_code,
f.foremonth, f.foreyear, f.dd_version;

update fact_atlaspharmlogiforecast_merck a
 set a.ct_forecastnext18mths = b.ct_forecastnext18months
 from temp2_18months b,fact_atlaspharmlogiforecast_merck a
 where a.dim_plantid = b.dim_plantid and a.dim_partid = b.dim_partid
 and a.reporting_company_code = b.reporting_company_code
 and a.country_destination_code = b.country_destination_code
 and a.dd_version = b.dd_version
 and  b.foreyear = a.foreyear and a.foremonth = b.foremonth
 and a.snapmonth = b.snapmonth and a.snapyear = b.snapyear
and ifnull(a.ct_forecastnext18mths,-1.999) <> b.ct_forecastnext18months;

/*28 Feb 2018 Georgiana adding a new measure calculated only for the records that are in the last forecast file that we have received*/

drop table if exists tmp_for_last_reporting ;
create table tmp_for_last_reporting as 
select max( pra_reporting_period) as pra_reporting_period from atlas_forecast_pra_forecasts_merck_DC;

/*Not NL10*/
update fact_atlaspharmlogiforecast_merck f
set ct_latestforecastforlastrpmonthfile=h.pra_quantity
/*select distinct pra_uin */
from fact_atlaspharmlogiforecast_merck f,atlas_forecast_pra_forecasts_merck_DC h, tmp_for_last_reporting d,dim_part dp,dim_date d2
where f.dim_partid=dp.dim_partid
and h.pra_uin=dp.partnumber
and h.plant_code=dp.plant
and f.reporting_company_code=h.reporting_company_code
and f.country_destination_code=h.country_destination_code
and  h.pra_reporting_period=d.pra_reporting_period
and d2.dim_dateid=f.dim_dateidreporting
and d2.calendarmonthid=pra_forecasting_period;

/*NL10 part*/


update fact_atlaspharmlogiforecast_merck f
set ct_latestforecastforlastrpmonthfile=h.pra_quantity
from fact_atlaspharmlogiforecast_merck f,atlas_forecast_pra_forecasts_merck_DC h, tmp_for_last_reporting d,dim_part dp,dim_date d2
where f.dim_partid=dp.dim_partid
and h.pra_uin=dp.partnumber
and case when h.plant_code='NL10' then 'XX20' else h.plant_code end =dp.plant
and f.reporting_company_code=h.reporting_company_code
and f.country_destination_code=h.country_destination_code
and  h.pra_reporting_period=d.pra_reporting_period
and d2.dim_dateid=f.dim_dateidreporting
and d2.calendarmonthid=pra_forecasting_period
and h.plant_code='NL10';

/*Added changes according to APP-9833, SAles and Df are joined using material Number,Plant,Billing Date , Country and Version=DFA*/
/*Changing it from Amt to Qty according with latest updates*/

/*Georgiana 11 Oct 2018 Adding latest changes based on issues found during validation ( duplication of peace measures due to country destination code and use sales instead of forecast when shipped qty from Sales is missing)*/
update fact_atlaspharmlogiforecast_merck
set dd_country_destination_code_upd = case when (dd_version = 'SFA' and dim_plantid in (91,124)) or dd_version = 'DTA' then country_destination_code else 'Not Set' end;

drop table if exists tmp_for_upd_from_sales;
create table tmp_for_upd_from_sales as 
select distinct s.dim_partid,s.dim_plantid,d1.calendarmonthid,dc.Country,sum(ct_DeliveredQty) as ct_DeliveredQty,sum((s.ct_DeliveredQty*s.amt_UnitPriceUoM/(CASE WHEN s.ct_PriceUnit <> 0 THEN s.ct_PriceUnit ELSE NULL END))*amt_exchangerate_gbl) as Ct_shippedAmountGBL
from fact_salesorder  s
inner join dim_date d1 on s.dim_billingdateid=d1.dim_dateid
inner join dim_customer dc on dc.dim_customerid=s.dim_customerid
group by s.dim_partid,s.dim_plantid,d1.calendarmonthid,dc.Country;

merge into fact_atlaspharmlogiforecast_merck f
using ( select distinct f.dim_partid,f.dim_plantid,f.dim_dateidreporting ,dd_version,country_destination_code,reporting_company_code,
ifnull(sum(s.ct_DeliveredQty), sum(ct_salesmonth1))  as ct_shippedQty
from   fact_atlaspharmlogiforecast_merck f
inner join dim_date d2 on f.dim_dateidreporting=d2.dim_dateid
left join tmp_for_upd_from_sales s on s.dim_partid=f.dim_partid and s.dim_plantid=f.dim_plantid 
 and s.calendarmonthid=d2.calendarmonthid and s.country=country_destination_code
/*and f.dim_partid='122290'
and d2.calendarmonthid='201806'*/
and dd_version='SFA'
group by f.dim_partid,f.dim_plantid,f.dim_dateidreporting,dd_version,country_destination_code,reporting_company_code) t
on t.dim_partid=f.dim_partid
and t.dim_plantid=f.dim_plantid
and t.dim_dateidreporting=f.dim_dateidreporting
and t.dd_version=f.dd_version
and t.country_destination_code=f.country_destination_code
and t.reporting_company_code=f.reporting_company_code
when matched then update set f.Ct_shippedQty=t.ct_shippedQty;

/*According to APP-9833 adding Current Peace and Peace measures*/
/*They are calculated only for the current month of sales. Current Pace is the % we are through the calendar month, for example if we hare at the half of june this measure will be 50%. Peace is Shipped QTY/( Latest forecast for Reporting Month FPP * Current Peace) */

drop table if exists tmp_for_current_peace;
create table tmp_for_current_peace as
select distinct (dayofmonth/daysinmonth)*100 as currentpeace from dim_date
where datevalue=current_date;

/*update fact_atlaspharmlogiforecast_merck
set ct_currentpeace=currentpeace
from fact_atlaspharmlogiforecast_merck,tmp_for_current_peace, dim_date d
where dim_dateidreporting=dim_dateid
and calendarmonthid= concat(year(current_date),case when length(month(current_date))=1 then concat('0',month(current_date)) else month(current_date) end)*/
merge into fact_atlaspharmlogiforecast_merck f
using( select distinct f.dim_partid,f.dim_plantid,f.dim_dateidreporting,f.dd_country_destination_code_upd,f.reporting_company_code,f.dd_version,currentpeace
from fact_atlaspharmlogiforecast_merck f,tmp_for_current_peace, dim_date d
where dim_dateidreporting=dim_dateid
and calendarmonthid= concat(year(current_date),case when length(month(current_date))=1 then concat('0',month(current_date)) else month(current_date) end)) t
on t.dim_partid=f.dim_partid
and t.dim_plantid=f.dim_plantid
and t.dim_dateidreporting=f.dim_dateidreporting
and t.dd_version=f.dd_version
and t.dd_country_destination_code_upd=f.dd_country_destination_code_upd
and t.reporting_company_code=f.reporting_company_code
when matched then update set ct_currentpeace=currentpeace;

/*update fact_atlaspharmlogiforecast_merck
set ct_peace= 100*Ct_shippedQty /case when (CT_LATESTFORECASTONMONTH * ct_currentpeace/100)=0 then null else (CT_LATESTFORECASTONMONTH * ct_currentpeace/100) end
from fact_atlaspharmlogiforecast_merck,dim_date d
where dim_dateidreporting=dim_dateid
and calendarmonthid= concat(year(current_date),case when length(month(current_date))=1 then concat('0',month(current_date)) else month(current_date) end)*/

merge into fact_atlaspharmlogiforecast_merck f
using(select distinct f.dim_partid,f.dim_plantid,f.dim_dateidreporting,f.dd_country_destination_code_upd,f.reporting_company_code,f.dd_version,
sum(case when (CT_LATESTFORECASTONMONTH * ct_currentpeace/100)=0 then 0 else 100*Ct_shippedQty / (CT_LATESTFORECASTONMONTH * ct_currentpeace/100) end) as peace
from fact_atlaspharmlogiforecast_merck f,tmp_for_current_peace, dim_date d
where dim_dateidreporting=dim_dateid
and calendarmonthid= concat(year(current_date),case when length(month(current_date))=1 then concat('0',month(current_date)) else month(current_date) end)
group by f.dim_partid,f.dim_plantid,f.dim_dateidreporting,f.dd_country_destination_code_upd,f.reporting_company_code,f.dd_version) t
on t.dim_partid=f.dim_partid
and t.dim_plantid=f.dim_plantid
and t.dim_dateidreporting=f.dim_dateidreporting
and t.dd_version=f.dd_version
and t.dd_country_destination_code_upd=f.dd_country_destination_code_upd
and t.reporting_company_code=f.reporting_company_code
when matched then update set ct_peace=peace;


/* 24 Sep 2018 Georgiana Changes: adding forecast bias calculations according to APP-10373*/

/*Forecast Bias is a custom measure and we need to calculate it also in the script*/
/*Grain usd: Dim_partid,dim_plantid,dim_dateidreporting,dd_version*/

drop table if exists tmp_forforecastbias;
create table tmp_forforecastbias as
select distinct dim_partid,dim_plantid,dim_dateidreporting,dd_version,d.datevalue,
round(case
when sum( (case when dd_forecast4mthis2null = 1 then null else ct_forecast4mth2 end ) ) + sum( (case when dd_forecast4mthis1null = 1 then null else ct_forecast4mth1 end ) ) = 0
then 0
else
(sum( (ct_salesmonth2) ) + sum( (ct_salesmonth1) )) / (sum( (case when dd_forecast4mthis2null = 1 then null else ct_forecast4mth2 end ) ) + sum( (case when dd_forecast4mthis1null = 1 then null else ct_forecast4mth1 end ) ))*100 - 100
end,2) as forecast_bias
from fact_atlaspharmlogiforecast_merck f, dim_date d
where f.dim_dateidreporting=d.dim_dateid
group by dim_partid,dim_plantid,dim_dateidreporting,dd_version,d.datevalue;

/*Temp tabe to calculate YTD values*/
drop table if exists tmp_forforecastbiasYTD;
create table tmp_forforecastbiasYTD as 
select t.dim_partid,t.dim_plantid,f.dim_dateidreporting,f.dd_version,d2.calendarmonthid, round(AVG(forecast_bias),2) as forecastbiasYTD from 
fact_atlaspharmlogiforecast_merck f, tmp_forforecastbias t, dim_date d2
where f.dim_partid=t.dim_partid
and f.dim_plantid=t.dim_plantid
and t.dd_version=f.dd_version
and f.dim_dateidreporting=d2.dim_dateid
and year(t.datevalue)=year(d2.datevalue)
and t.datevalue<=d2.datevalue
group by  t.dim_partid,t.dim_plantid,f.dim_dateidreporting,f.dd_version,d2.calendarmonthid;

/*Temp table to calculate Last 12 Months values*/
drop table if exists tmp_forforecastbiasLast12Months;
create table tmp_forforecastbiasLast12Months as 
select t.dim_partid,t.dim_plantid,f.dim_dateidreporting,f.dd_version,d2.calendarmonthid, round(AVG(forecast_bias),2) as forecastbiasLast12 from 
fact_atlaspharmlogiforecast_merck f, tmp_forforecastbias t, dim_date d2
where f.dim_partid=t.dim_partid
and f.dim_plantid=t.dim_plantid
and t.dd_version=f.dd_version
and f.dim_dateidreporting=d2.dim_dateid
and ((t.datevalue<=  case when d2.datevalue= '0001-01-01' then null else d2.datevalue end) and (t.datevalue > (case when d2.datevalue= '0001-01-01' then null else  add_months(d2.datevalue,-12) end)))
group by  t.dim_partid,t.dim_plantid,f.dim_dateidreporting,f.dd_version,d2.calendarmonthid;


merge into fact_atlaspharmlogiforecast_merck f
using (select distinct f.dim_partid,f.dim_plantid, f.dim_dateidreporting,f.reporting_company_code,f.country_destination_code,f.dd_version, t.forecastbiasYTD
from fact_atlaspharmlogiforecast_merck f, tmp_forforecastbiasYTD t
where t.dim_partid=f.dim_partid
and t.dim_plantid=f.dim_plantid
and t.dim_dateidreporting=f.dim_dateidreporting
and t.dd_version = f.dd_version) t
on t.dim_partid=f.dim_partid
and t.dim_plantid=f.dim_plantid
and t.dim_dateidreporting=f.dim_dateidreporting
and t.reporting_company_code=f.reporting_company_code
and t.country_destination_code=f.country_destination_code
and t.dd_version = f.dd_version
when matched then update set f.ct_forecastbiasYTD= t.forecastbiasYTD;

merge into fact_atlaspharmlogiforecast_merck f
using (select distinct f.dim_partid,f.dim_plantid, f.dim_dateidreporting,f.reporting_company_code,f.country_destination_code,f.dd_version, t.forecastbiasLast12
from fact_atlaspharmlogiforecast_merck f, tmp_forforecastbiasLast12Months t
where t.dim_partid=f.dim_partid
and t.dim_plantid=f.dim_plantid
and t.dim_dateidreporting=f.dim_dateidreporting
and t.dd_version = f.dd_version) t
on t.dim_partid=f.dim_partid
and t.dim_plantid=f.dim_plantid
and t.dim_dateidreporting=f.dim_dateidreporting
and t.reporting_company_code=f.reporting_company_code
and t.country_destination_code=f.country_destination_code
and t.dd_version = f.dd_version
when matched then update set f.ct_forecastbiasLast12Months= t.forecastbiasLast12;
/* 24 Sep APP-10373 End*/
drop table if exists tmp_forforecastbias;
drop table if exists tmp_forforecastbiasYTD;
drop table if exists tmp_forforecastbiasLast12Months;

/*28 Sep 2018 APP-10373 Calculating 2 attributes for current month averages*/
merge into fact_atlaspharmlogiforecast_merck f
using (select distinct dim_partid, dim_plantid,dd_version,avg(CT_FORECASTBIASYTD) as FORECASTBIASYTD,avg(CT_FORECASTBIASLAST12MONTHS) as FORECASTBIASLAST12MONTHS  
from fact_atlaspharmlogiforecast_merck f, dim_date d
where f.dim_dateidreporting=d.dim_dateid
and d.calendarmonthid = concat(year(current_date),case when length(month(current_date))=1 then concat('0',month(current_date)) else month(current_date) end )
group by dim_partid, dim_plantid,dd_version) t
on t.dim_partid=f.dim_partid and t.dim_plantid=f.dim_plantid and t.dd_version=f.dd_version
when matched then update set DD_FORECASTBIASYTD = FORECASTBIASYTD,
DD_FORECASTBIASlast12MOnths = FORECASTBIASLAST12MONTHS;
