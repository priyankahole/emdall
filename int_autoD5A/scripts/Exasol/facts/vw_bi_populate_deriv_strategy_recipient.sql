
/* ################################################################################################################## */
/* */
/*   Author         : Octavian */
/*   Created On     : 11 Feb 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   16 Dec 2015      Octavian	1.0  		  First draft */
/* 	 24 Feb 2015 	  Octavian  1.1		  Added KOTR901 */	
/******************************************************************************************************************/

drop table if exists tmp_fact_derivationstrategy_recipient;
CREATE TABLE tmp_fact_derivationstrategy_recipient LIKE fact_derivationstrategy_recipient INCLUDING DEFAULTS INCLUDING IDENTITY;

ALTER TABLE tmp_fact_derivationstrategy_recipient ADD PRIMARY KEY (fact_derivationstrategy_recipientid);


delete from number_fountain m where m.table_name = 'tmp_fact_derivationstrategy_recipient';
insert into number_fountain
select 'tmp_fact_derivationstrategy_recipient',
ifnull(max(f.fact_derivationstrategy_recipientid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_derivationstrategy_recipient f;

insert into tmp_fact_derivationstrategy_recipient(
fact_derivationstrategy_recipientid,
dd_conditionno,
dd_seqno,
dd_attrderived,
dd_norecur,
dd_rband,
dd_rbfalse,
dd_rbor,
dd_rbtrue,
dd_rmintrscn,
dd_rmunion,
dd_rnmax,
dd_rnmin,
dd_deletionind,
dd_source,
dd_derivindsendernotempty,
dd_derivindnoteverwrite,
dd_calcofaverage,
dd_customerexit1,
dd_customerexit2,
dd_numberoflevelsinbatch)

SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_derivationstrategy_recipient') + row_number() over(order by '') AS fact_derivationstrategy_recipientid, 
ifnull(k.KONDRPR_KNUMH,'Not Set') AS dd_conditionno,
ifnull(k.KONDRPR_KPOSN,'Not Set') AS dd_seqno,
ifnull(k.KONDRPR_DRVFL,'Not Set') AS dd_attrderived,
ifnull(k.KONDRPR_NORECUR,'Not Set') AS dd_norecur,
ifnull(k.KONDRPR_RBAND,'Not Set') AS dd_rband,
ifnull(k.KONDRPR_RBFALSE,'Not Set') AS dd_rbfalse,
ifnull(k.KONDRPR_RBOR,'Not Set') AS dd_rbor,
ifnull(k.KONDRPR_RBTRUE,'Not Set') AS dd_rbtrue,
ifnull(k.KONDRPR_RMINTRSCN,'Not Set') AS dd_rmintrscn,
ifnull(k.KONDRPR_RMUNION,'Not Set') AS dd_rmunion,
ifnull(k.KONDRPR_RNMAX,'Not Set') AS dd_rnmax,
ifnull(k.KONDRPR_RNMIN,'Not Set') AS dd_rnmin,
ifnull(kx.KONDR_LOEVM_KO,'Not Set') AS dd_deletionind,
'RECEIVER MATERIAL',
ifnull(KONDRPR_NOTNULL,'Not Set') as dd_derivindsendernotempty,
ifnull(KONDRPR_NOOVRWRT,'Not Set') as dd_derivindnoteverwrite,
ifnull(KONDRPR_RNAVG,'Not Set') as dd_calcofaverage,
ifnull(KONDRPR_EXIT1,'Not Set') as dd_customerexit1,
ifnull(KONDRPR_EXIT2,'Not Set') as dd_customerexit2,
ifnull(CASE WHEN KONDRPR_DRVLVL < 99 THEN KONDRPR_DRVLVL / 2 ELSE KONDRPR_DRVLVL END,0) as dd_numberoflevelsinbatch
FROM 
KONDRPR k 
INNER JOIN KONDR kx ON k.KONDRPR_KNUMH = kx.KONDR_KNUMH;


MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, ifnull(k9.KOTR901_DRVEV,'Not Set')
 AS dd_derivationevent 
 				 from KONDRPR k , KOTR901 k9
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k.KONDRPR_KNUMH,'Not Set') = dd_conditionno
and ifnull(k.KONDRPR_KPOSN,'Not Set') = dd_seqno
and k.KONDRPR_KNUMH = k9.KOTR901_KNUMH
and dd_derivationevent <> ifnull(k9.KOTR901_DRVEV,'Not Set')) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dd_derivationevent = src.dd_derivationevent;



MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, FIRST_VALUE(dp.dim_partid) OVER (PARTITION BY dp.partnumber ORDER BY dp.dim_partid desc)
 AS dim_partid  
 				 from KONDRPR k , KOTR901 k9, dim_part dp
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k.KONDRPR_KNUMH,'Not Set') = dd_conditionno
and ifnull(k.KONDRPR_KPOSN,'Not Set') = dd_seqno
and k.KONDRPR_KNUMH = k9.KOTR901_KNUMH
and dp.partnumber = k9.KOTR901_R_MATNR
and t.dim_partid <> dp.dim_partid ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dim_partid  = src.dim_partid;




/* Georgiana 28 Mar 2016 Adding fields from KONDRPS and KOTR910 tables*/

delete from number_fountain m where m.table_name = 'tmp_fact_derivationstrategy_recipient';
insert into number_fountain
select 'tmp_fact_derivationstrategy_recipient',
ifnull(max(f.fact_derivationstrategy_recipientid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_derivationstrategy_recipient f;

insert into tmp_fact_derivationstrategy_recipient(
fact_derivationstrategy_recipientid,
dd_conditionno,
dd_seqno,
dd_attrderivedsender,
dd_indicatorattrsender,
dd_controlfield1,
dd_controlfield2,
dd_source)

SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_derivationstrategy_recipient') + row_number() over(ORDER BY '') AS fact_derivationstrategy_recipientid, 
ifnull(k.KONDRPS_KNUMH,'Not Set') AS dd_conditionno,
ifnull(k.KONDRPS_KPOSN,'Not Set') AS dd_seqno,
ifnull(k.KONDRPS_DRVFL, 'Not Set') AS dd_attrderivedsender,
ifnull(k.KONDRPS_DRVCF, 'Not Set') AS dd_indicatorattrsender,
ifnull(k.KONDRPS_EXIT1, 'Not Set') AS dd_controlfield1,
ifnull(k.KONDRPS_EXIT2, 'Not Set') AS dd_controlfield2,
'SENDER MATERIAL'
FROM KONDRPS k 
where not exists (  select 1 from tmp_fact_derivationstrategy_recipient t
where ifnull(k.KONDRPS_KNUMH,'Not Set') = t.dd_conditionno and
ifnull(k.KONDRPS_KPOSN,'Not Set') = t.dd_seqno);

MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, ifnull(k9.KOTR910_KAPPL,'Not Set') AS dd_application  
 				 from KONDRPS k , KOTR910 k9
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k.KONDRPS_KNUMH,'Not Set') = dd_conditionno
and ifnull(k.KONDRPS_KPOSN,'Not Set') = dd_seqno
and k.KONDRPS_KNUMH = k9.KOTR910_KNUMH
and  t.dd_application <> ifnull(k9.KOTR910_KAPPL, 'Not Set') ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dd_application  = src.dd_application  
 WHERE ifnull(fact.dd_application ,'Not Set') <> ifnull(src.dd_application ,'Not Set');


MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, ifnull (k9.KOTR910_DRVEV,'Not Set') AS dd_derivationeventsender  
 				 from KONDRPS k , KOTR910 k9
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k.KONDRPS_KNUMH,'Not Set') = dd_conditionno
and ifnull(k.KONDRPS_KPOSN,'Not Set') = dd_seqno
and k.KONDRPS_KNUMH = k9.KOTR910_KNUMH
and  t.dd_derivationeventsender <> ifnull (k9.KOTR910_DRVEV,'Not Set') ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dd_derivationeventsender  = src.dd_derivationeventsender;

MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, FIRST_VALUE(dp.dim_partid) OVER (PARTITION BY dp.partnumber ORDER BY dp.dim_partid desc)
 AS dim_partidsender  
 				 from KONDRPS k , KOTR910 k9, dim_part dp
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k.KONDRPS_KNUMH,'Not Set') = dd_conditionno
and ifnull(k.KONDRPS_KPOSN,'Not Set') = dd_seqno
and k.KONDRPS_KNUMH = k9.KOTR910_KNUMH
and dp.partnumber = k9.KOTR910_S_MATNR
and t.dim_partidsender <> dp.dim_partid ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dim_partidsender  = src.dim_partidsender;

MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, ifnull (k9.KOTR910_KSCHL,'Not Set') AS dd_searchstrategytype  
 				 from KONDRPS k , KOTR910 k9
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k.KONDRPS_KNUMH,'Not Set') = dd_conditionno
and ifnull(k.KONDRPS_KPOSN,'Not Set') = dd_seqno
and k.KONDRPS_KNUMH = k9.KOTR910_KNUMH
and  t.dd_searchstrategytype <> ifnull (k9.KOTR910_KSCHL, 'Not Set') ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dd_searchstrategytype  = src.dd_searchstrategytype;

MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, FIRST_VALUE(ifnull(k9.KOTR910_DATBI,'Not Set')) OVER (PARTITION BY k9.KOTR910_KNUMH ORDER BY k9.KOTR910_DATBI desc) AS dd_validitydate  
 				 from KONDRPS k , KOTR910 k9
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k.KONDRPS_KNUMH,'Not Set') = dd_conditionno
and ifnull(k.KONDRPS_KPOSN,'Not Set') = dd_seqno
and k.KONDRPS_KNUMH = k9.KOTR910_KNUMH
and  t.dd_validitydate <> ifnull(k9.KOTR910_DATBI,'0001-01-01') ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dd_validitydate  = src.dd_validitydate;


MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT fd.fact_derivationstrategy_recipientid, dt.dim_dateid
 AS dim_dateidvaliditystart  
 				 FROM dim_date dt, KOTR910 k9,dim_part dp, dim_plant pl
, tmp_fact_derivationstrategy_recipient fd 
 				 WHERE
ifnull(k9.KOTR910_KAPPL, 'Not Set') = fd.dd_application
AND ifnull (k9.KOTR910_DRVEV,'Not Set') = fd.dd_derivationeventsender
AND ifnull (k9.KOTR910_KSCHL, 'Not Set') = fd.dd_searchstrategytype
AND ifnull (k9.KOTR910_DATBI, '0001-01-01') = fd.dd_validitydate
and  fd.dim_partid=dp.dim_partid
AND dt.datevalue = k9.KOTR910_DATAB    
AND dp.plant = pl.plantcode
AND pl.plantcode = dt.plantcode_factory
and pl.companycode=dt.companycode
AND K9.kotr910_s_matnr = dp.partnumber
AND fd.dim_dateidvaliditystart <> dt.dim_dateid ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dim_dateidvaliditystart  = src.dim_dateidvaliditystart;

MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT fd.fact_derivationstrategy_recipientid, dt.dim_dateid
 AS dim_dateidvalidityend  
 				 FROM dim_date dt, KOTR910 k9,dim_part dp, dim_plant pl
, tmp_fact_derivationstrategy_recipient fd 
 				 WHERE
ifnull(k9.KOTR910_KAPPL, 'Not Set') = fd.dd_application
AND ifnull (k9.KOTR910_DRVEV,'Not Set') = fd.dd_derivationeventsender
AND ifnull (k9.KOTR910_KSCHL, 'Not Set') = fd.dd_searchstrategytype
AND ifnull (k9.KOTR910_DATBI, '0001-01-01') = fd.dd_validitydate
and  fd.dim_partid=dp.dim_partid
AND dt.datevalue = k9.KOTR910_DATBI    
AND dp.plant = pl.plantcode
AND pl.plantcode = dt.plantcode_factory
and pl.companycode=dt.companycode
AND K9.kotr910_s_matnr = dp.partnumber
AND fd.dim_dateidvaliditystart <> dt.dim_dateid ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dim_dateidvalidityend  = src.dim_dateidvalidityend;

/*KOTR912 Updates*/
MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, FIRST_VALUE(dp.dim_partid) OVER (PARTITION BY dp.partnumber ORDER BY dp.dim_partid desc)
 AS dim_partid  
 				 from  KOTR912 k9, dim_part dp, dim_plant pl,dim_date dt
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k9.KOTR912_KNUMH,'Not Set') = dd_conditionno
and ifnull(k9.KOTR912_R_MATNR, 'Not Set') = dp.partnumber  
AND dp.plant = pl.plantcode
AND pl.plantcode = dt.plantcode_factory
and pl.companycode=dt.companycode
AND dt.datevalue = k9.KOTR912_DATBI
and  t.dim_partid <> ifnull(k9.KOTR912_R_MATNR, 'Not Set') ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dim_partid  = src.dim_partid;

MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT t.fact_derivationstrategy_recipientid, FIRST_VALUE(dp.dim_partid) OVER (PARTITION BY dp.partnumber ORDER BY dp.dim_partid desc)
 AS dim_partidsender  
 				 from  KOTR912 k9, dim_part dp, dim_plant pl,dim_date dt
, tmp_fact_derivationstrategy_recipient t 
 				 where ifnull(k9.KOTR912_KNUMH,'Not Set') = dd_conditionno
AND dp.plant = pl.plantcode
and pl.companycode=dt.companycode
AND pl.plantcode = dt.plantcode_factory
AND dt.datevalue = k9.KOTR912_DATBI
and dp.partnumber =  ifnull(k9.KOTR912_S_MATNR, 'Not Set')
and  t.dim_partidsender <> dp.dim_partid ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.dim_partidsender  = src.dim_partidsender;

/*28 Mar End of Changes*/

/* Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */
MERGE INTO tmp_fact_derivationstrategy_recipient fact 
 	 USING (SELECT DISTINCT am.fact_derivationstrategy_recipientid, dt.dim_dateid
 AS std_exchangerate_dateid  
 				 FROM dim_date dt
, tmp_fact_derivationstrategy_recipient am 
 				 WHERE dt.companycode = 'Not Set' AND dt.plantcode_factory = 'Not Set'
AND dt.datevalue = current_date
AND am.std_exchangerate_dateid <> dt.dim_dateid ) src 
 	 ON fact.fact_derivationstrategy_recipientid = src.fact_derivationstrategy_recipientid 
 WHEN MATCHED THEN UPDATE 
 	 SET fact.std_exchangerate_dateid  = src.std_exchangerate_dateid  
 WHERE ifnull(fact.std_exchangerate_dateid ,-1) <> ifnull(src.std_exchangerate_dateid ,-2);
/* END Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

TRUNCATE TABLE fact_derivationstrategy_recipient;
INSERT INTO fact_derivationstrategy_recipient
SELECT * FROM tmp_fact_derivationstrategy_recipient;

DROP TABLE IF EXISTS tmp_fact_derivationstrategy_recipient;
