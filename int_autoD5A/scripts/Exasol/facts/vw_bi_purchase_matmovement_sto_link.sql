/*********************************************Change History*******************************************************/
/*Date             By             Version         Desc                                                            */
/*   03 Mar 2015   Liviu Ionescu                  Add Combine for table var_pToleranceDays, fact_materialmovement */
/******************************************************************************************************************/

DROP TABLE IF EXISTS var_pToleranceDays;
CREATE TABLE var_pToleranceDays
(
pType  VARCHAR(10),
pToleranceDays INTEGER
);


INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('EARLY',0);
INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('LATE',0);

UPDATE var_pToleranceDays
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
WHERE property = 'purchasing.deliverytolerance.early'),
              0)
WHERE pType ='EARLY';
			  
UPDATE var_pToleranceDays			  
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
WHERE property = 'purchasing.deliverytolerance.late'),
              0)
WHERE pType ='LATE';          
                    
DROP TABLE IF EXISTS tmp_Purchase_uomvalues;
CREATE TABLE tmp_Purchase_uomvalues AS 
SELECT distinct p.dd_documentno,p.dd_documentitemno,duom.uom
from fact_purchase p 
inner join dim_unitofmeasure duom
on ifnull(p.dim_unitofmeasureid, 1) = ifnull(duom.dim_unitofmeasureid, 1);

drop table if exists fact_mm_tmp_dlvrlink;
create table fact_mm_tmp_dlvrlink as 
SELECT f.Fact_materialmovementid,
		f.dd_MaterialDocNo,
		f.dd_MaterialDocItemNo,
		f.dd_MaterialDocYear,
		f.dd_DocumentNo,
		f.dd_DocumentItemNo,
		-1*f.ct_Quantity*(ifnull(uc.marm_umrez,1))/ifnull(uc.marm_umren,1)*(ifnull(tpuc.marm_umren,1)/ifnull(tpuc.marm_umrez,1)) ct_Quantity,
		-1*f.ct_QtyEntryUOM*(ifnull(buc.marm_umrez,1))/ifnull(buc.marm_umren,1)*(ifnull(tpuc.marm_umren,1)/ifnull(tpuc.marm_umrez,1)) ct_QtyEntryUOM,
		ifnull(f.Dim_MovementTypeid, 1) as Dim_MovementTypeid,
		f.dim_DateIDPostingDate,
		f.dd_debitcreditid,
		dt.datevalue PostingDate,
		ROW_NUMBER() OVER (PARTITION BY f.dd_DocumentNo, f.dd_DocumentItemNo ORDER BY dt.datevalue, f.dd_MaterialDocNo, f.dd_MaterialDocItemNo) "rownum"
FROM fact_materialmovement f 
INNER JOIN dim_unitofmeasure uom ON ifnull(f.dim_unitofmeasureorderunitid, 1) = ifnull(uom.dim_unitofmeasureid, 1)
INNER JOIN dim_unitofmeasure buom ON ifnull(f.dim_unitofmeasureid, 1) = ifnull(buom.dim_unitofmeasureid, 1)
		inner join dim_movementtype mt on f.Dim_MovementTypeid = ifnull(mt.Dim_MovementTypeid,1)
		inner join dim_date dt on f.dim_DateIDPostingDate = dt.dim_dateid
		 INNER JOIN dim_Part dp1 ON ifnull(f.dim_Partid, 1) = ifnull(dp1.dim_Partid,1)
			LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
			LEFT JOIN MARM buc ON buc.marm_matnr = dp1.partnumber and buc.marm_meinh = buom.uom
	LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
	LEFT JOIN tmp_Purchase_uomvalues t1 on t1.dd_DocumentNo = f.dd_DocumentNo and t1.dd_DocumentItemNo = f.dd_DocumentItemNo
	LEFT JOIN MARM tpuc on tpuc.marm_matnr = dp1.partnumber and tpuc.marm_meinh = t1.uom
WHERE mt.MovementType in ('641','643') 
        and exists (select 1 from fact_materialmovement f1 
                    where f.dd_DocumentNo = f1.dd_DocumentNo and f.dd_DocumentItemNo = f1.dd_DocumentItemNo
                        and f1.Dim_DateIdDelivery = 1)
        and exists (select 1 from fact_purchase fp 
                    where f.dd_DocumentNo = fp.dd_DocumentNo and f.dd_DocumentItemNo = fp.dd_DocumentItemNo);

    
drop table if exists fact_mm_tmp_dlvrlink_01;
create table fact_mm_tmp_dlvrlink_01 as 
SELECT a.Fact_materialmovementid,
		a.dd_MaterialDocNo,
		a.dd_MaterialDocItemNo,
		a.dd_MaterialDocYear,
		a.dd_DocumentNo,
		a.dd_DocumentItemNo,
		a.ct_Quantity,
		a.ct_QtyEntryUOM,
		ifnull(a.Dim_MovementTypeid, 1) as Dim_MovementTypeid,
		ifnull(a.dim_DateIDPostingDate, 1) as dim_DateIDPostingDate, 
		a.dd_debitcreditid,
		a.PostingDate,
		a."rownum",
		SUM(b.ct_QtyEntryUOM) DlvrQtyCUMM
FROM fact_mm_tmp_dlvrlink a 
	 inner join fact_mm_tmp_dlvrlink b on a.dd_DocumentNo = b.dd_DocumentNo and a.dd_DocumentItemNo = b.dd_DocumentItemNo
WHERE a."rownum" >= b."rownum"
GROUP BY a.Fact_materialmovementid,
		a.dd_MaterialDocNo,
		a.dd_MaterialDocItemNo,
		a.dd_MaterialDocYear,
		a.dd_DocumentNo,
		a.dd_DocumentItemNo,
		a.ct_Quantity,
		a.ct_QtyEntryUOM,
		a.Dim_MovementTypeid,
		a.dim_DateIDPostingDate,
		a.dd_debitcreditid,
		a.PostingDate,
		a."rownum";
		
		
drop table if exists fact_po_mm_tmp;
create table fact_po_mm_tmp as
select dd_DocumentNo,
		dd_DocumentItemNo,
		dd_ScheduleNo,
		Dim_DateidDelivery,
		Dim_DateidStatDelivery,
		ct_QtyIssued,
		dp1.DateValue DeliveryDate,
		ROW_NUMBER() OVER (PARTITION BY dd_DocumentNo, dd_DocumentItemNo ORDER BY dp1.DateValue, dd_ScheduleNo) "rownum"
from fact_purchase fp inner join dim_date dp1 on dp1.Dim_Dateid = fp.Dim_DateidDelivery		
where exists (select 1 from fact_mm_tmp_dlvrlink t 
			  where t.dd_DocumentNo = fp.dd_DocumentNo and t.dd_DocumentItemNo = fp.dd_DocumentItemNo)
		and fp.ct_QtyIssued > 0;
			  		

drop table if exists fact_po_mm_tmp_01;
create table fact_po_mm_tmp_01 as
select a.dd_DocumentNo,
		a.dd_DocumentItemNo,
		a.dd_ScheduleNo,
		a.Dim_DateidDelivery,
		a.Dim_DateidStatDelivery,
		a.ct_QtyIssued,
		a.DeliveryDate,
		a."rownum",
		SUM(b.ct_QtyIssued) QtyIssuedCUMM
from fact_po_mm_tmp a 
	 inner join fact_po_mm_tmp b on a.dd_DocumentNo = b.dd_DocumentNo and a.dd_DocumentItemNo = b.dd_DocumentItemNo
where a."rownum" >= b."rownum"
group by a.dd_DocumentNo,
		a.dd_DocumentItemNo,
		a.dd_ScheduleNo,
		a.Dim_DateidDelivery,
		a.Dim_DateidStatDelivery,
		a.ct_QtyIssued,
		a.DeliveryDate,
		a."rownum";
		
drop table if exists fact_mm_tmp_dlvrlink;
create table fact_mm_tmp_dlvrlink as 
SELECT a.Fact_materialmovementid,
		b.dd_ScheduleNo,
		b.Dim_DateidDelivery,
		b.Dim_DateidStatDelivery,
		b.ct_QtyIssued
FROM fact_mm_tmp_dlvrlink_01 a 
	 inner join fact_po_mm_tmp_01 b on a.dd_DocumentNo = b.dd_DocumentNo and a.dd_DocumentItemNo = b.dd_DocumentItemNo
WHERE a.DlvrQtyCUMM <= b.QtyIssuedCUMM
	 and a.DlvrQtyCUMM > (b.QtyIssuedCUMM - b.ct_QtyIssued);
		
		
update fact_materialmovement f
set f.Dim_DateIdDelivery = ifnull(b.Dim_DateidDelivery, 1)
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_mm_tmp_dlvrlink b,fact_materialmovement f
where f.Fact_materialmovementid = b.Fact_materialmovementid
AND f.Dim_DateIdDelivery <> ifnull(b.Dim_DateidDelivery, 1);

update fact_materialmovement f
set f.Dim_DateIdStatDelivery = ifnull(b.Dim_DateidStatDelivery, 1)
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_mm_tmp_dlvrlink b, fact_materialmovement f
where f.Fact_materialmovementid = b.Fact_materialmovementid
      AND f.Dim_DateIdStatDelivery <> ifnull(b.Dim_DateidStatDelivery, 1);

update fact_materialmovement f
set f.ct_OrderQuantity = b.ct_QtyIssued
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_mm_tmp_dlvrlink b,fact_materialmovement f
where f.Fact_materialmovementid = b.Fact_materialmovementid
AND f.ct_OrderQuantity <> b.ct_QtyIssued;
		
		
drop table if exists fact_mm_tmp_dlvrlink;
drop table if exists fact_mm_tmp_dlvrlink_01;
drop table if exists fact_po_mm_tmp;
drop table if exists fact_po_mm_tmp_01;

       
update fact_materialmovement f 
     set f.Dim_GRStatusid = case when dp.DateValue > (to_date(ds.DateValue) + (INTERVAL '1' DAY)*L.pToleranceDays ) then 3 
                                 when dp.DateValue < (to_date(ds.DateValue) - (INTERVAL '1' DAY)*E.pToleranceDays ) then 4 
                                 else 5 end
	,f.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	  FROM dim_date dp, dim_date ds, var_pToleranceDays E,var_pToleranceDays L,fact_materialmovement f 
   where f.Dim_DateIdStatDelivery <> 1
     and ifnull(dp.Dim_Dateid, 1) = ifnull(f.dim_DateIDPostingDate, 1)
     and ifnull(ds.Dim_Dateid, 1) = ifnull(f.Dim_DateIdStatDelivery, 1)
     and E.pType = 'EARLY'
     and L.pType = 'LATE';