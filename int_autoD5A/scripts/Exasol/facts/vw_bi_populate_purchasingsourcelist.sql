/* */
/*   Author         : Octavian */
/*   Created On     : 05 APR 2016 */
/*  */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   05 APR 2015      Octavian	1.0  		  First draft 														  */
/******************************************************************************************************************/

/* pre-process the dates that go beyond 2090 to default to 9999-12-31 */
update EORD_nofilter
SET EORD_VDATU = '9999-12-31'
where EORD_VDATU >= '2090-01-01';

update EORD_nofilter
SET EORD_BDATU = '9999-12-31'
where EORD_BDATU >= '2090-01-01';
/* pre-process the dates that go beyond 2090 to default to 9999-12-31 */

drop table if exists tmp_fact_purchasingsourcelist;
create table tmp_fact_purchasingsourcelist
LIKE fact_purchasingsourcelist INCLUDING DEFAULTS INCLUDING IDENTITY;


delete from number_fountain m where m.table_name = 'tmp_fact_purchasingsourcelist';
insert into number_fountain
select 'tmp_fact_purchasingsourcelist',
ifnull(max(f.fact_purchasingsourcelistid ),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_purchasingsourcelist f;

insert into tmp_fact_purchasingsourcelist(
fact_purchasingsourcelistid,
dd_pk_partnumber,
dd_pk_plantcode,
dd_slrno,
dim_partid,
dim_plantid,
dim_dateidrecordcreated,
dd_recordcreator,
dim_dateidvalidfrom,
dim_dateidvalidto,
dim_vendorid,
dd_fixedvendor,
dd_agreementno,
dd_agreementitem,
dd_fixedagreementitem,
dim_plantidproc,
dd_fixedissueingplant,
dd_matnomanufacturer,
dd_blockedsupplysource,
dim_purchaseorgid,
dd_purchasingdoccatg,
dd_catgslr,
dd_slusageinmtpl,
dim_purchaseuomid,
dd_logicalsystem,
dim_specialstockid
)

SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_purchasingsourcelist') + row_number() over(order by '') AS fact_purchasingsourcelistid,
ifnull(e.eord_matnr,'Not Set') AS dd_pk_partnumber,
ifnull(e.eord_werks,'Not Set') AS dd_pk_plantcode,
ifnull(e.eord_zeord,0) AS dd_slrno,
/* ifnull((SELECT dp.dim_partid FROM dim_part dp
WHERE ifnull(e.eord_matnr,'Not Set') = dp.partnumber
and ifnull(e.eord_werks,'Not S') = dp.plant),1) */ CONVERT(BIGINT,1) AS dim_partid,
/* ifnull((SELECT p.dim_plantid FROM dim_plant p
WHERE ifnull(e.eord_werks,'Not Set') = p.plantcode),1) */ CONVERT(BIGINT,1) AS dim_plantid,
/* ifnull((SELECT dt.dim_dateid FROM dim_date dt, dim_plant pl
WHERE ifnull(e.eord_erdat,'0001-01-01') = dt.datevalue AND dt.companycode = pl.companycode
and pl.plantcode = ifnull(e.eord_werks,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_dateidrecordcreated,
ifnull(e.eord_ernam,'Not Set') AS dd_recordcreator,
/* ifnull((SELECT dt.dim_dateid FROM dim_date dt, dim_plant pl
WHERE ifnull(e.eord_vdatu,'0001-01-01') = dt.datevalue AND dt.companycode = pl.companycode
and pl.plantcode = ifnull(e.eord_werks,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_dateidvalidfrom,
/* ifnull((SELECT dt.dim_dateid FROM dim_date dt, dim_plant pl
WHERE ifnull(e.eord_bdatu,'0001-01-01') = dt.datevalue AND dt.companycode = pl.companycode
and pl.plantcode = ifnull(e.eord_werks,'Not Set')),1) */ CONVERT(BIGINT,1) AS dim_dateidvalidto,
/* ifnull((SELECT v.dim_vendorid FROM dim_vendor v
WHERE ifnull(e.eord_lifnr,'Not Set') = v.vendornumber),1) */ CONVERT(BIGINT,1) AS dim_vendorid,
ifnull(e.eord_flifn,'Not Set') AS dd_fixedvendor,
ifnull(e.eord_ebeln,'Not Set') AS dd_agreementno,
ifnull(e.eord_ebelp,0) AS dd_agreementitem,
ifnull(e.eord_febel,'Not Set') AS dd_fixedagreementitem,
/* ifnull((SELECT p.dim_plantid FROM dim_plant p
WHERE ifnull(e.eord_reswk,'Not Set') = p.plantcode),1) */ CONVERT(BIGINT,1) AS dim_plantidproc,
ifnull(e.eord_fresw,'Not Set') AS dd_fixedissueingplant,
ifnull(e.eord_ematn,'Not Set') AS dd_matnomanufacturer,
ifnull(e.eord_notkz,'Not Set') AS dd_blockedsupplysource,
/* ifnull((SELECT po.dim_purchaseorgid FROM dim_purchaseorg po
WHERE ifnull(e.eord_ekorg,'Not Set') = po.purchaseorgcode),1) */ CONVERT(BIGINT,1) AS dim_purchaseorgid,
ifnull(e.eord_vrtyp,'Not Set') AS dd_purchasingdoccatg,
ifnull(e.eord_eortp,'Not Set') AS dd_catgslr,
ifnull(e.eord_autet,'Not Set') AS dd_slusageinmtpl,
/* ifnull((SELECT uom.dim_unitofmeasureid FROM dim_unitofmeasure uom
WHERE ifnull(e.eord_meins,'Not Set') = uom.uom),1) */ CONVERT(BIGINT,1) AS dim_purchaseuomid,
ifnull(e.eord_logsy,'Not Set') AS dd_logicalsystem,
/* ifnull((SELECT ss.dim_specialstockid FROM dim_specialstock ss
WHERE ifnull(e.eord_sobkz,'Not Set') = ss.specialstockindicator),1) */ CONVERT(BIGINT,1) AS dim_specialstockid
FROM EORD_nofilter e;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_partid = dp.dim_partid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_part dp
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ps.dd_pk_partnumber = dp.partnumber AND ps.dd_pk_plantcode = dp.plant
AND ps.dim_partid <> dp.dim_partid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_plantid = pl.dim_plantid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_plant pl
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ps.dd_pk_plantcode = pl.plantcode
AND ps.dim_plantid <> pl.dim_plantid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_dateidrecordcreated = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_date dt, dim_plant pl
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ifnull(e.eord_erdat,'0001-01-01') = dt.datevalue AND dt.companycode = pl.companycode
AND dt.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
AND pl.plantcode = ifnull(e.eord_werks,'Not Set')
AND ps.dim_dateidrecordcreated <> dt.dim_dateid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_dateidvalidfrom = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_date dt, dim_plant pl
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ifnull(e.eord_vdatu,'0001-01-01') = dt.datevalue AND dt.companycode = pl.companycode
AND dt.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
AND pl.plantcode = ifnull(e.eord_werks,'Not Set')
AND ps.dim_dateidvalidfrom <> dt.dim_dateid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_dateidvalidto = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_date dt, dim_plant pl
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ifnull(e.eord_bdatu,'0001-01-01') = dt.datevalue AND dt.companycode = pl.companycode
AND dt.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
AND pl.plantcode = ifnull(e.eord_werks,'Not Set')
AND ps.dim_dateidvalidto <> dt.dim_dateid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_vendorid = v.dim_vendorid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_vendor v
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ifnull(e.eord_lifnr,'Not Set') = v.vendornumber
AND ps.dim_vendorid <> v.dim_vendorid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_plantidproc = p.dim_plantid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_plant p
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ifnull(e.eord_reswk,'Not Set') = p.plantcode
AND ps.dim_plantidproc <> p.dim_plantid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_purchaseorgid = po.dim_purchaseorgid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_purchaseorg po
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ifnull(e.eord_ekorg,'Not Set') = po.purchaseorgcode
AND ps.dim_purchaseorgid <> po.dim_purchaseorgid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_plantidproc = uom.dim_unitofmeasureid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_unitofmeasure uom
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ifnull(e.eord_meins,'Not Set') = uom.uom
AND ps.dim_plantidproc <> uom.dim_unitofmeasureid;

UPDATE tmp_fact_purchasingsourcelist ps
SET ps.dim_specialstockid = ss.dim_specialstockid,
dw_update_date = CURRENT_TIMESTAMP
FROM tmp_fact_purchasingsourcelist ps, EORD_nofilter e, dim_specialstock ss
WHERE ps.dd_pk_partnumber = e.eord_matnr AND ps.dd_pk_plantcode = e.eord_werks AND ps.dd_slrno = e.eord_zeord
AND ifnull(e.eord_sobkz,'Not Set') = ss.specialstockindicator
AND ps.dim_specialstockid <> ss.dim_specialstockid;

TRUNCATE TABLE fact_purchasingsourcelist;
INSERT INTO fact_purchasingsourcelist
SELECT * FROM tmp_fact_purchasingsourcelist;

DROP TABLE IF EXISTS tmp_fact_purchasingsourcelist;