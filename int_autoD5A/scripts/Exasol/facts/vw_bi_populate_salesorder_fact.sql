/* ################################################################################################################## */
/* */
/*   Script         : bi_populate_salesorder_fact */
/*   Author         : Ashu */
/*   Created On     : 17 Jan 2013 */
/*  */
/*  */
/*   Description    : Stored Proc bi_populate_salesorder_fact from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* 24 Jun 2016       Madalina  	 		Add new dimension - BI-3300 */
/*	 10 Jun 2016	 Madalina H 1.65			 Shipment Doc No, Shipment Doc Item and Actual Goods Issue Date are Not Set for some items BI-3042 */
/*   19 May 2016	 Madalina H 1.64			 Add  Billing Date (Invoice) and Invoice Number (F2), based on the related columns from fact_billing. BI-2795 */
/*   13 May 2016     Madalina H 1.63             Add new column, representing the difference between Order Purchase Date and Transit time in Calendar Days. BI - 2821   */
/*   18 Mar 2016     Georgiana  1.62             Add dd_deliverynumber: EKES-VBELN -BI-2348 */
/*   16 Mar 2016     Georgiana  1.61             Adding dim_partsalesid */
/*	 25 Jan 2016     Octavian   1.6			     20+ fields added as part of the Every Angle transition */
/*	 29 Jun 2015     Nicu       1.51             Removed call vectorwise(combine 'fact_salesorder') calls and left only the final call.*/
/*   02 Mar 2015     L.Ionescu  1.50             Add Combine for  fact_salesorder_tmptbl, fact_salesorder, Dim_CostCenter_first1_701, fact_salesorderdelivery */
/*   28 Apr 2014     Lokesh	1.49		 Optimized script	*/
/*   18 Mar 2014     Lokesh	1.48	         Added ct_CumOrderQty population */
/*   14 Feb 2014     George     1.47             Added Dim_CustomerGroup4id */
/*   12 Feb 2014     George     1.46             Added Dim_ScheduleDeliveryBlockid */
/*   03 Feb 2014     George     1.45		 Added: dim_CustomerConditionGroups1id, dim_CustomerConditionGroups2id, dim_CustomerConditionGroups3id  */
/*   29 Nov 2013    Lokesh	1.40             Changed amt_ScheduleTotal calculation to use VBAP_NETWR when vbep_wmeng = VBAP_KWMENG */
/* 						As discussed with Issam, fixed the issue with vbak_vbap_vbep and vbak_vbap updates */
/*						update from item data if it exists (posnr <> 0), otherwise update from header data (posnr = 0) */
/*	 26 Sep 2013     Issam      1.33             Added fields dd_SOCreateTime dd_ReqDeliveryTime, dd_SOLineCreateTime,
												 dd_DeliveryTime, dd_PlannedGITime  								  */
/*   07 Sep 2013     Lokesh     1.25             Exchange Rate and Currency changes. Merged prev versions                               */
/*   29 Aug 2013     Lokesh     1.22             Changes done for amt_Subtotal3inCustConfig_Billing and amt_Subtotal3_OrderQty.  */
/*   29 Aug 2013     Shanthi    1.21		 	 Made fixes related to the Textron    */
/*   20 Aug 2013     Shanthi    1.20               added new fields */
/*   11 Mar 2013     Lokesh     1.3              Sync with Shanthi's latest changes in mysql   */
/*   25 Mar 2013     Lokesh     1.2				 Changes to fix Dim_BillToPartyPartnerFunctionId discrepancies - add order by cpf.PartnerCounter desc limit 1 */
/*   20 Mar 2013     Lokesh     1.1              Minor changes, sync with current prod/qa version. */
/*   17 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise > */
/* #################################################################################################################### */

/* Refresh all tables in VW from MySQL */
/* cd /home/fusionops/ispring/db/schema_migration/bin	*/
/* */

DROP TABLE IF EXISTS variable_holder_701;
CREATE TABLE variable_holder_701
AS
SELECT CONVERT(VARCHAR(3),ifnull((SELECT property_value
                                  FROM systemproperty
                                  WHERE property = 'customer.global.currency'), 'USD')) pGlobalCurrency,
       CONVERT(VARCHAR(10), ifnull((SELECT property_value
                                    FROM systemproperty
                                    WHERE property = 'custom.partnerfunction.key'), 'Not Set')) pCustomPartnerFunctionKey,
       CONVERT(VARCHAR(10), ifnull((SELECT property_value
                                    FROM systemproperty
                                    WHERE property = 'custom.partnerfunction.key1'), 'Not Set')) pCustomPartnerFunctionKey1,
       CONVERT(VARCHAR(10), ifnull((SELECT property_value
                                    FROM systemproperty
                                    WHERE property = 'custom.partnerfunction.key2'), 'Not Set')) pCustomPartnerFunctionKey2,
       CONVERT(VARCHAR(5),'RE') pBillToPartyPartnerFunction,
       CONVERT(VARCHAR(5),'RG') pPayerPartnerFunction;


/*
  UPDATE vbak_vbap_vbep p
   FROM vbak_vbap_vbkd k
  SET p.VBAP_STCUR = k.VBKD_KURSK, p.PRSDT = k.VBKD_PRSDT
  where p.vbak_vbeln = k.VBKD_VBELN

  UPDATE vbak_vbap p
   FROM vbak_vbap_vbkd k
  SET p.VBAP_STCUR = k.VBKD_KURSK, p.PRSDT = k.VBKD_PRSDT
  where p.vbap_vbeln = k.VBKD_VBELN
*/
/* update from item data if it exists (posnr <> 0), otherwise update from header data (posnr = 0) */
  UPDATE vbak_vbap_vbep p
  SET p.VBAP_STCUR = k.VBKD_KURSK
FROM vbak_vbap_vbkd k,
     vbak_vbap_vbep p
  where p.vbak_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0
  and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);


UPDATE vbak_vbap_vbep p
SET p.PRSDT = k.VBKD_PRSDT
FROM vbak_vbap_vbkd k,
     vbak_vbap_vbep p
WHERE p.vbak_vbeln = k.VBKD_VBELN
      AND k.VBKD_POSNR = 0
      AND IFNULL(p.PRSDT,to_date('01011900','DDMMYYYY')) <> IFNULL(k.VBKD_PRSDT,to_date('01011900','DDMMYYYY'));

UPDATE vbak_vbap p
SET p.VBAP_STCUR = k.VBKD_KURSK
FROM vbak_vbap_vbkd k,
     vbak_vbap p
WHERE p.vbap_vbeln = k.VBKD_VBELN
      AND VBKD_POSNR = 0
      AND IFNULL(p.VBAP_STCUR,-1) <> IFNULL(k.VBKD_KURSK,-1);

UPDATE vbak_vbap p
SET p.PRSDT = k.VBKD_PRSDT
FROM vbak_vbap_vbkd k,
     vbak_vbap p
WHERE p.vbap_vbeln = k.VBKD_VBELN
      AND k.VBKD_POSNR = 0
      AND IFNULL(p.PRSDT,to_date('01011900','DDMMYYYY')) <> IFNULL(k.VBKD_PRSDT,to_date('01011900','DDMMYYYY'));

/* Update item data if it exists. So this will overwrite updates from header, where a match is found */

UPDATE vbak_vbap_vbep p
SET p.VBAP_STCUR = k.VBKD_KURSK
FROM vbak_vbap_vbkd k,
     vbak_vbap_vbep p
WHERE p.vbak_vbeln = k.VBKD_VBELN
      AND p.vbap_posnr = k.VBKD_POSNR
      AND IFNULL(p.VBAP_STCUR,-1) <> IFNULL(k.VBKD_KURSK,-1);

UPDATE vbak_vbap_vbep p
SET p.PRSDT = k.VBKD_PRSDT
FROM vbak_vbap_vbkd k,
     vbak_vbap_vbep p
WHERE p.vbak_vbeln = k.VBKD_VBELN
      AND p.vbap_posnr = k.VBKD_POSNR
      AND IFNULL(p.PRSDT,to_date('01011900','DDMMYYYY')) <> IFNULL(k.VBKD_PRSDT,to_date('01011900','DDMMYYYY'));


UPDATE vbak_vbap p
SET p.VBAP_STCUR = k.VBKD_KURSK
FROM vbak_vbap_vbkd k,
     vbak_vbap p
WHERE p.vbap_vbeln = k.VBKD_VBELN
      AND p.vbap_posnr = k.VBKD_POSNR
      AND IFNULL(p.VBAP_STCUR,-1) <> IFNULL(k.VBKD_KURSK,-1);

UPDATE vbak_vbap p
SET p.PRSDT = k.VBKD_PRSDT
FROM vbak_vbap_vbkd k,
     vbak_vbap p
WHERE p.vbap_vbeln = k.VBKD_VBELN
      AND p.vbap_posnr = k.VBKD_POSNR
      AND IFNULL(p.PRSDT,to_date('01011900', 'ddmmyyyy')) <> IFNULL(k.VBKD_PRSDT,to_date('01011900', 'ddmmyyyy'));


/* Backup rows for tracking before they are DELETEd */
INSERT INTO tmp_fact_salesorder_deleted
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       current_timestamp,
       fact_salesorderid,
       'D'
FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE a.CDPOS_OBJECTID = dd_SalesDocNo
                    AND a.CDPOS_TABNAME = 'VBAK'
                    AND a.CDPOS_CHNGIND  =  'D');

INSERT INTO tmp_fact_salesorder_deleted
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       current_timestamp,
       fact_salesorderid,
       'I'
FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,4,10)) = dd_SalesDocNo
                    AND TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,14,6))  =  dd_SalesItemNo
                    AND a.CDPOS_TABNAME  =  'VBAP'
                    AND a.CDPOS_CHNGIND  =  'D' )
      AND NOT EXISTS (SELECT 1
                      FROM VBAK_VBAP_VBEP
                      WHERE VBAK_VBELN = dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo);

INSERT INTO tmp_fact_salesorder_deleted
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       current_timestamp,
       fact_salesorderid,
       'S'
FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,4,10)) = dd_SalesDocNo
                    AND TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,14,6))  =  dd_SalesItemNo
                    AND TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,20,4)) =  dd_scheduleno
                    AND a.CDPOS_TABNAME  =  'VBEP'
                    AND a.CDPOS_CHNGIND  =  'D' )
      AND NOT EXISTS (SELECT 1
                      FROM VBAK_VBAP_VBEP
                      WHERE VBAK_VBELN  =  dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo
                            AND VBEP_ETENR  =  dd_ScheduleNo);

/* DELETE based on docno */
DELETE FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
	          WHERE a.CDPOS_OBJECTID  =  dd_SalesDocNo
                    AND a.CDPOS_TABNAME  =  'VBAK'
                    AND a.CDPOS_CHNGIND  =  'D');

/* DELETE based on itemno */
DELETE FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,4,10)) = dd_SalesDocNo
                    AND TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,14,6))  =  dd_SalesItemNo
                    AND a.CDPOS_TABNAME  =  'VBAP'
                    AND  a.CDPOS_CHNGIND  =  'D' )
      AND NOT EXISTS (SELECT 1
                      FROM VBAK_VBAP_VBEP
                      WHERE VBAK_VBELN  =  dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo);

/* DELETE based on schedule */
DELETE FROM fact_salesorder
WHERE EXISTS (SELECT 1
              FROM CDPOS_VBAK a
              WHERE TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,4,10)) = dd_SalesDocNo
                    AND TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,14,6))  =  dd_SalesItemNo
                    AND TRIM(LEADING '0' FROM SUBSTRING(a.CDPOS_TABKEY,20,4)) =  dd_scheduleno
                    AND a.CDPOS_TABNAME  =  'VBEP'
                    AND  a.CDPOS_CHNGIND  =  'D' )
      AND NOT EXISTS (SELECT 1
                      FROM VBAK_VBAP_VBEP
                      WHERE VBAK_VBELN  =  dd_SalesDocNo
                            AND VBAP_POSNR  =  dd_SalesItemNo
                            AND VBEP_ETENR  =  dd_ScheduleNo);
			    
drop table if exists Dim_CostCenter_701;
create table Dim_CostCenter_701 as Select * from Dim_CostCenter ORDER BY ValidTo DESC;
drop table if exists dim_profitcenter_701;
create table dim_profitcenter_701 as Select  * from dim_profitcenter  ORDER BY ValidTo ASC;			    

/* ################################### 12 Apr changes - sync with mysql changes	by Shanthi part 2 starts ###################################	*/
/* Create temporary tables to avoide multiple joins for updating dd_CreditLimit, Dim_CustomerRiskCategoryId and dd_CreditRep */

/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/

DROP TABLE IF EXISTS TMP_UPD_dd_CreditRep1;
CREATE TABLE TMP_UPD_dd_CreditRep1
AS
SELECT CASE
         WHEN b.NAME_FIRST IS NULL THEN b.NAME_LAST
		 ELSE b.NAME_FIRST || ' ' || IFNULL(b.NAME_LAST,'')
	   END upd_dd_CreditRep1,
       d.BUT050_PARTNER2
FROM cvi_cust_link c
     INNER JOIN but000 b1 ON c.PARTNER_GUID = b1.PARTNER_GUID
     INNER JOIN BUT050 d ON b1.PARTNER = d.BUT050_PARTNER2
     INNER JOIN but000 b ON b.PARTNER = d.BUT050_PARTNER1
WHERE current_date BETWEEN d.BUT050_DATE_FROM AND d.BUT050_DATE_TO
      AND b.NAME_FIRST IS NOT NULL
	  OR b.NAME_LAST IS NOT NULL
	  AND b.BU_GROUP = 'CRED';


/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/
DROP TABLE IF EXISTS TMP_UPD_dd_CreditLimit;
CREATE TABLE TMP_UPD_dd_CreditLimit
AS
SELECT c.CUSTOMER,
       a.CREDIT_LIMIT
FROM UKMBP_CMS_SGM a
     INNER JOIN BUT000 b ON a.PARTNER = b.PARTNER AND current_date <=  a.LIMIT_VALID_DATE
     INNER JOIN cvi_cust_link c ON c.PARTNER_GUID  =  b.PARTNER_GUID;

/* Note that this also had  order by c.customer limit 1 in the original mysql query, but ignored that as its not useful in getting unique recs	*/
DROP TABLE IF EXISTS TMP_UPD_Dim_CustomerRiskCategoryId;
CREATE TABLE TMP_UPD_Dim_CustomerRiskCategoryId
AS
SELECT c.CUSTOMER,
       src.CreditControlArea,
       src.dim_salesriskcategoryid
FROM ukmbp_cms a
     INNER JOIN BUT000 b ON a.PARTNER = b.PARTNER
     INNER JOIN cvi_cust_link c ON c.PARTNER_GUID = b.PARTNER_GUID
     INNER JOIN dim_salesriskcategory src ON src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent  =  1;

/*###################################End of 12 Apr tmp table creation ###################################*/

DROP TABLE IF EXISTS STAGING_UPDATE_701;
DROP TABLE IF EXISTS VBAK_VBAP_VBEP_701;

CREATE TABLE VBAK_VBAP_VBEP_701
AS
SELECT MIN(x.VBEP_ETENR) v_SalesSchedNo,
       x.VBAK_VBELN v_SaleseDocNo,
	   x.VBAP_POSNR v_SalesItemNo
FROM VBAK_VBAP_VBEP x
GROUP BY x.VBAK_VBELN,
         x.VBAP_POSNR;

CREATE TABLE staging_update_701
AS
SELECT vbep_wmeng as ct_ScheduleQtySalesUnit,
	   vbep_bmeng as ct_ConfirmedQty,
       vbep_cmeng as ct_CorrectedQty,
	   ifnull(convert(numeric(18,4), vbap_netpr), 0) as amt_UnitPrice,	--LK: Removed multiplication with stat exchg rate
       vbap_kpein as ct_PriceUnit,
	   convert(numeric(18,4),CASE
	             WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			       THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end )),2), 0)
			     WHEN vbep_wmeng  =  VBAP_KWMENG
				   THEN VBAP_NETWR
			     ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end )), 0)
		      END) as amt_ScheduleTotal,
       convert(numeric(18,4),CASE
	              WHEN VBEP_ETENR  =  y.v_SalesSchedNo THEN vbap_wavwr
			      ELSE 0
		        END) as amt_StdCost,
	   convert(numeric(18,4),CASE
	              WHEN VBEP_ETENR  =  y.v_SalesSchedNo THEN vbap_zwert
		          ELSE 0
		        END) as amt_TargetValue,
       ifnull(convert(numeric(18,4),((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0)  =  0 THEN 1
						WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
								WHEN 0 THEN 1
								ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							END
						ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
					        END)* vbep_bmeng)), 0) amt_Tax,
       CASE
          WHEN VBEP_ETENR  =  y.v_SalesSchedNo THEN vbap_zmeng
			  ELSE 0
		    END  ct_TargetQty,
       convert(numeric(18,4), 1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =   co.Currency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate,
       convert(numeric(18,4), 1) as amt_ExchangeRate_GBL, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate_GBL,
           vbap_uebto ct_OverDlvrTolerance,
           vbap_untto ct_UnderDlvrTolerance,
       convert(bigint, 1) as Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint, 1) as Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
	   dd_SalesDocNo,
	   dd_SalesItemNo,
	   dd_ScheduleNo,
	   convert(bigint, 1) as Dim_UnitOfMeasureId, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_kmein AND uom.RowIsCurrent  =  1), 1) Dim_UnitOfMeasureId,
	   convert(bigint, 1) as Dim_BaseUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_meins AND uom.RowIsCurrent  =  1), 1) Dim_BaseUoMid,
	   convert(bigint, 1) as Dim_SalesUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_vrkme AND uom.RowIsCurrent  =  1), 1) Dim_SalesUoMid
	   convert(numeric(18,4),CASE
	            WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			      THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			    ELSE ifnull(vbap_netpr, 0)
		       END) amt_UnitPriceUoM,	/*LK: 8 Sep 2013: Removed multiplication by stat exch rate*/
	/* LK: 8 Sep 2013: Added 4 new columns */
       convert(bigint, 1) as Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  VBAP_WAERK),1) Dim_Currencyid_TRA,
       convert(bigint, 1) as Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  co.Currency),1) Dim_Currencyid,
       convert(bigint, 1) as dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pGlobalCurrency),1) dim_Currencyid_GBL,
       convert(bigint, 1) as dim_currencyid_STAT, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  vbak_stwae),1) dim_currencyid_STAT,
       convert(numeric(18,4), 1) as amt_exchangerate_STAT, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate  =  0 AND z.pToCurrency  =  vbak_stwae),1) amt_exchangerate_STAT,
       ifnull(VBAP_KWMENG,0) ct_CumOrderQty,
	   VBAP_WAERK,
       PRSDT,
       vbak_audat,
       vbap_erdat,
       VBAP_STADAT,
	   pl.CompanyCode as CompanyCode,
       vbap_kmein,
       vbap_meins,
       vbap_vrkme,
       co.Currency as Currency,
       pGlobalCurrency,
       vbak_stwae,
	   VBAP_WERKS
from fact_salesorder so,
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	 VBAK_VBAP_VBEP_701 y,
	 variable_holder_701
WHERE pl.PlantCode  =  VBAP_WERKS
      AND co.CompanyCode  =  pl.CompanyCode
	  and VBAK_VBELN  =  dd_SalesDocNo
	  and VBAP_POSNR  =  dd_SalesItemNo
	  and VBEP_ETENR  =  dd_ScheduleNo
	  and VBAK_VBELN  =  y.v_SaleseDocNo
	  and VBAP_POSNR  =  y.v_SalesItemNo
	  AND VBEP_ETENR  =  y.v_SalesSchedNo
	  AND soic.SalesOrderItemCategory  =  VBAP_PSTYV
	  AND soic.RowIsCurrent  =  1;

INSERT INTO staging_update_701(
ct_ScheduleQtySalesUnit,
ct_ConfirmedQty,
ct_CorrectedQty,
amt_UnitPrice,
ct_PriceUnit,
amt_ScheduleTotal,
amt_StdCost,
amt_TargetValue,
amt_Tax,
ct_TargetQty,
amt_ExchangeRate,
amt_ExchangeRate_GBL,
ct_OverDlvrTolerance,
ct_UnderDlvrTolerance,
Dim_DateidSalesOrderCreated,
Dim_DateidFirstDate,
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo,
Dim_UnitOfMeasureId,
Dim_BaseUoMid,
Dim_SalesUoMid,
amt_UnitPriceUoM,
dim_Currencyid_TRA,
dim_Currencyid,
dim_Currencyid_GBL,
dim_currencyid_STAT,
amt_exchangerate_STAT,
ct_CumOrderQty,
VBAP_WAERK,
PRSDT,
vbak_audat,
vbap_erdat,
VBAP_STADAT,
CompanyCode,
vbap_kmein,
vbap_meins,
vbap_vrkme,
Currency,
pGlobalCurrency,
vbak_stwae,VBAP_WERKS
)
SELECT vbep_wmeng as ct_ScheduleQtySalesUnit,
	   vbep_bmeng as ct_ConfirmedQty,
       vbep_cmeng as ct_CorrectedQty,
	   ifnull(convert(numeric(18,4),vbap_netpr), 0) as amt_UnitPrice,
           vbap_kpein ct_PriceUnit,
       convert(numeric(18,4), CASE
                                WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0)
			WHEN vbep_wmeng = VBAP_KWMENG THEN VBAP_NETWR
			ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0)
		                      END) as amt_ScheduleTotal,
       convert(numeric(18,4), 0) as amt_StdCost,
       convert(numeric(18,4), 0) as amt_TargetValue,
	   ifnull(convert(numeric(18,4),((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0)  =  0 THEN 1
						WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						THEN ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
						ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),2),1)
					 END)
			* (vbep_bmeng))), 0) amt_Tax,
           0 ct_TargetQty,
       convert(numeric(18,4),1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =   co.Currency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate,
       convert(numeric(18,4),1) as amt_ExchangeRate_GBL, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate_GBL,
       vbap_uebto as ct_OverDlvrTolerance,
       vbap_untto as ct_UnderDlvrTolerance,
       convert(bigint,1) as Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint,1) as Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo,
	   convert(bigint,1) as Dim_UnitOfMeasureId, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_kmein AND uom.RowIsCurrent  =  1), 1) Dim_UnitOfMeasureId,
	   convert(bigint,1) as Dim_BaseUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_meins AND uom.RowIsCurrent  =  1), 1) Dim_BaseUoMid,
	   convert(bigint,1) as Dim_SalesUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_vrkme AND uom.RowIsCurrent  =  1), 1) Dim_SalesUoMid,
	   convert(numeric(18,4),CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END) amt_UnitPriceUoM,
       convert(bigint,1) as Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  VBAP_WAERK),1) Dim_Currencyid_TRA,
       convert(bigint,1) as Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  co.currency ),1) Dim_Currencyid,
	   convert(bigint,1) as dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pGlobalCurrency),1) dim_Currencyid_GBL,
       convert(bigint,1) as dim_currencyid_STAT, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  vbak_stwae),1) dim_currencyid_STAT,
       convert(numeric(18,4),1) as amt_exchangerate_STAT, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate  =  0 AND z.pToCurrency  =  vbak_stwae),1) amt_exchangerate_STAT,
       ifnull(VBAP_KWMENG,0) ct_CumOrderQty,
	   VBAP_WAERK,
       PRSDT,
       vbak_audat,
       vbap_erdat,
       VBAP_STADAT,
	   pl.CompanyCode as CompanyCode,
       vbap_kmein,
       vbap_meins,
       vbap_vrkme,
       co.Currency as Currency,
       pGlobalCurrency,
       vbak_stwae, VBAP_WERKS
FROM fact_salesorder so,
	 VBAK_VBAP_VBEP,
	 Dim_Plant pl,
	 Dim_Company co,
	 Dim_SalesOrderItemCategory soic,
	 variable_holder_701
WHERE pl.PlantCode  =  VBAP_WERKS
      AND co.CompanyCode  =  pl.CompanyCode
      AND VBAK_VBELN  =  dd_SalesDocNo
	  AND VBAP_POSNR  =  dd_SalesItemNo
	  AND VBEP_ETENR  =  dd_ScheduleNo
      AND soic.SalesOrderItemCategory  =  VBAP_PSTYV
	  AND soic.RowIsCurrent  =  1
      AND NOT EXISTS (SELECT 1
	                  FROM VBAK_VBAP_VBEP_701 y
			          WHERE VBAK_VBELN = y.v_SaleseDocNo
					        and VBAP_POSNR = y.v_SalesItemNo
							AND VBEP_ETENR = y.v_SalesSchedNo);

UPDATE staging_UPDATE_701 sut
SET sut.amt_ExchangeRate = ifnull(z.exchangeRate, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' and pFromExchangeRate = 0) z
	     ON z.pFromCurrency = sut.VBAP_WAERK AND z.pDate = ifnull(sut.PRSDT,sut.vbak_audat) AND z.pToCurrency = sut.Currency
WHERE sut.amt_ExchangeRate <> ifnull(z.exchangeRate, 1);

UPDATE staging_UPDATE_701 sut
SET sut.amt_ExchangeRate_GBL = ifnull(z.exchangerate, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' and pFromExchangeRate = 0) z
	     ON z.pFromCurrency = sut.VBAP_WAERK AND z.pDate = ifnull(sut.PRSDT,sut.vbak_audat) AND z.pToCurrency = sut.pGlobalCurrency
WHERE sut.amt_ExchangeRate_GBL <> ifnull(z.exchangerate, 1);

merge into staging_UPDATE_701 sut
using (select distinct dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,dd.dim_dateid
FROM staging_UPDATE_701 sut
     INNER JOIN dim_plant pl on pl.PlantCode  =  VBAP_WERKS and pl.companycode=sut.companycode
     LEFT JOIN dim_date dd ON dd.datevalue = sut.vbap_erdat AND dd.companycode = sut.companycode and dd.plantcode_factory=pl.plantcode
WHERE sut.Dim_DateidSalesOrderCreated <> ifnull(dd.dim_dateid, 1)) t
on
t.dd_SalesDocNo=sut.dd_SalesDocNo
and t.dd_SalesItemNo=sut.dd_SalesItemNo 
and t.dd_ScheduleNo=sut.dd_ScheduleNo
when matched then update set sut.Dim_DateidSalesOrderCreated = ifnull(t.dim_dateid, 1);

merge into staging_UPDATE_701 sut
using (select distinct dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,dd.dim_dateid
FROM staging_UPDATE_701 sut
     INNER JOIN dim_plant pl on pl.PlantCode  =  VBAP_WERKS and pl.companycode=sut.companycode
     LEFT JOIN dim_date dd ON dd.datevalue = sut.VBAP_STADAT AND dd.companycode = sut.companycode  and dd.plantcode_factory=pl.plantcode
WHERE sut.Dim_DateidFirstDate <> ifnull(dd.dim_dateid, 1)) t
on
t.dd_SalesDocNo=sut.dd_SalesDocNo
and t.dd_SalesItemNo=sut.dd_SalesItemNo 
and t.dd_ScheduleNo=sut.dd_ScheduleNo
when matched then update set sut.Dim_DateidFirstDate = ifnull(t.dim_dateid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_UnitOfMeasureId = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom
	     ON uom.UOM = sut.vbap_kmein
WHERE sut.Dim_UnitOfMeasureId <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_BaseUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom
	     ON uom.UOM = sut.vbap_meins
WHERE sut.Dim_BaseUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_SalesUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom
	     ON uom.UOM = sut.vbap_vrkme
WHERE sut.Dim_SalesUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_Currencyid_TRA = ifnull(cur.dim_currencyid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_currency cur ON cur.CurrencyCode = sut.VBAP_WAERK
WHERE sut.Dim_Currencyid_TRA <> ifnull(cur.dim_currencyid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.Dim_Currencyid = ifnull(cur.dim_currencyid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_currency cur ON cur.CurrencyCode = sut.currency
WHERE sut.Dim_Currencyid <> ifnull(cur.dim_currencyid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.dim_Currencyid_GBL = ifnull(cur.dim_currencyid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_currency cur ON cur.CurrencyCode = sut.pGlobalCurrency
WHERE sut.dim_Currencyid_GBL <> ifnull(cur.dim_currencyid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.dim_currencyid_STAT = ifnull(cur.dim_currencyid, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN dim_currency cur ON cur.CurrencyCode = sut.vbak_stwae
WHERE sut.dim_currencyid_STAT <> ifnull(cur.dim_currencyid, 1);

UPDATE staging_UPDATE_701 sut
SET sut.amt_exchangerate_STAT = ifnull(z.exchangeRate, 1)
FROM staging_UPDATE_701 sut
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name  =  'bi_populate_salesorder_fact' AND pFromExchangeRate = 0) z
	     ON z.pFromCurrency = sut.VBAP_WAERK AND z.pDate = ifnull(sut.PRSDT,sut.vbak_audat) AND z.pToCurrency = sut.vbak_stwae
WHERE sut.amt_exchangerate_STAT <> ifnull(z.exchangeRate, 1);

drop table if exists VBAK_VBAP_VBEP_701;

MERGE INTO fact_salesorder so
USING staging_UPDATE_701 sut ON so.dd_SalesDocNo = sut.dd_SalesDocNo AND so.dd_SalesItemNo = sut.dd_SalesItemNo AND so.dd_ScheduleNo = sut.dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET
	so.ct_ScheduleQtySalesUnit = sut.ct_ScheduleQtySalesUnit
	,so.ct_ConfirmedQty = sut.ct_ConfirmedQty
	,so.ct_CorrectedQty = sut.ct_CorrectedQty
	,so.amt_UnitPrice = sut.amt_UnitPrice
	,so.amt_UnitPriceUoM = sut.amt_UnitPriceUoM
	,so.ct_PriceUnit = sut.ct_PriceUnit
	,so.amt_ScheduleTotal = sut.amt_ScheduleTotal
	,so.amt_StdCost = sut.amt_StdCost
	,so.amt_TargetValue  =  sut.amt_TargetValue
	,so.amt_Tax  =  sut.amt_Tax
	,so.ct_TargetQty  =  sut.ct_TargetQty
	,so.amt_ExchangeRate  =  sut.amt_ExchangeRate
	,so.amt_ExchangeRate_GBL = sut.amt_ExchangeRate_GBL
	,so.ct_OverDlvrTolerance = sut.ct_OverDlvrTolerance
	,so.ct_UnderDlvrTolerance = sut.ct_UnderDlvrTolerance
	,so.Dim_DateidSalesOrderCreated = sut.Dim_DateidSalesOrderCreated
	,so.Dim_DateidFirstDate = sut.Dim_DateidFirstDate
	,so.Dim_UnitOfMeasureId  =  sut.Dim_UnitOfMeasureId
	,so.Dim_BaseUoMid  =  sut.Dim_BaseUoMid
	,so.Dim_SalesUoMid  =  sut.Dim_SalesUoMid
	,so.dim_Currencyid_TRA  =  sut.dim_Currencyid_TRA
	,so.dim_Currencyid_GBL  =  sut.dim_Currencyid_GBL
	,so.dim_currencyid_STAT  =  sut.dim_currencyid_STAT
	,so.amt_exchangerate_STAT  =  sut.amt_exchangerate_STAT
	,so.dim_Currencyid  =  sut.dim_Currencyid
	,so.ct_CumOrderQty  =  sut.ct_CumOrderQty;

DROP TABLE IF EXISTS staging_UPDATE_701;

/* ashu temporary code split to handle stringparse issue - Actian is working on to fix it-- */
/*create table staging_update_701 as
Select  ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_edatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDelivery,
         ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGoodsIssue,
         ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMtrlAvail,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoading,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbep_tddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidTransport,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) Dim_Currencyid,
           ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid Dim_Plantid,
           co.Dim_Companyid Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort and sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          ifnull((SELECT Dim_ScheduleLineCategoryId
                  FROM Dim_ScheduleLineCategory slc
                  WHERE slc.ScheduleLineCategory = VBEP_ETTYP),1) Dim_ScheduleLineCategoryId,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
          1 Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((select Dim_SalesOrderHeaderStatusid
                    from Dim_SalesOrderHeaderStatus sohs
                    where sohs.SalesDocumentNumber = VBAK_VBELN),1) Dim_SalesOrderHeaderStatusid,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo
from fact_salesorder so,
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	(select min(x.VBEP_ETENR) _SalesSchedNo, x.VBAK_VBELN _SaleseDocNo, x.VBAP_POSNR _SalesItemNo
	 from VBAK_VBAP_VBEP x group by x.VBAK_VBELN, x.VBAP_POSNR) y
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y._SaleseDocNo and VBAP_POSNR = y._SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
*/

CREATE TABLE staging_update_701(
        dim_dateidscheddeliveryreq integer default 1 not null,
        dim_dateidscheddelivery integer default 1 not null,
        dim_dateidgoodsissue integer default 1 not null,
        dim_dateidmtrlavail integer default 1 not null,
        dim_dateidloading integer default 1 not null,
        dim_dateidguaranteedate integer default 1 not null,
        dim_dateidtransport integer default 1 not null,
        dim_currencyid smallint default 1 not null,
		dim_currencyid_TRA smallint default 1 not null,
		dim_currencyid_GBL smallint default 1 not null,
		dim_currencyid_STAT smallint default 1 not null,
        dim_producthierarchyid integer default 1 not null,
        dim_plantid smallint default 1 not null,
        dim_companyid smallint default 1 not null,
        dim_storagelocationid smallint default 1 not null,
        dim_salesdivisionid integer default 1 not null,
        dim_shipreceivepointid integer default 1 not null,
        dim_documentcategoryid integer default 1 not null,
        dim_salesdocumenttypeid integer default 1 not null,
        dim_salesorgid integer default 1 not null,
        dim_customerid integer default 1 not null,
        dim_schedulelinecategoryid smallint default 1 not null,
        dim_dateidvalidfrom integer default 1 not null,
        dim_dateidvalidto integer default 1 not null,
        dim_salesgroupid integer default 1 not null,
        dim_costcenterid smallint default 1 not null,
        dim_controllingareaid integer default 1 not null,
        dim_billingblockid integer default 1 not null,
        dim_transactiongroupid integer default 1 not null,
        dim_salesorderrejectreasonid integer default 1 not null,
        dim_partid integer default 1 not null,
        dim_salesorderheaderstatusid integer default 1 not null,
        dd_salesdocno varchar(10) default 'Not Set' null,
        dd_salesitemno integer default 0 null,
        dd_scheduleno integer default 0 null
);

drop table if exists VBAK_VBAP_VBEP_701;
create table VBAK_VBAP_VBEP_701 as
select min(x.VBEP_ETENR) v_SalesSchedNo, x.VBAK_VBELN v_SaleseDocNo, x.VBAP_POSNR v_SalesItemNo
          from VBAK_VBAP_VBEP x group by x.VBAK_VBELN, x.VBAP_POSNR;



Insert into staging_update_701 (
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo
)
select
dd_SalesDocNo,
dd_SalesItemNo,
dd_ScheduleNo
from fact_salesorder so,
	VBAK_VBAP_VBEP,
	Dim_Plant pl,
	Dim_Company co,
	Dim_SalesOrderItemCategory soic,
	VBAK_VBAP_VBEP_701 y
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y.v_SaleseDocNo and VBAP_POSNR = y.v_SalesItemNo
AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

update staging_update_701 sg
Set Dim_DateidSchedDeliveryReq = dd.Dim_Dateid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Date dd, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode and dd.plantcode_factory=pl.plantcode
AND sg.Dim_DateidSchedDeliveryReq <> dd.Dim_Dateid;


update staging_update_701 sg
Set Dim_DateidSchedDelivery = dd.Dim_Dateid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Date dd, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND dd.DateValue  =  vbep_edatu AND dd.CompanyCode  =  pl.CompanyCode and dd.plantcode_factory=pl.plantcode
AND sg.Dim_DateidSchedDelivery <> dd.Dim_Dateid;

update staging_update_701 sg
Set Dim_DateidGoodsIssue = dd.Dim_Dateid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Date dd,staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND dd.DateValue  =  vbep_wadat AND dd.CompanyCode  =  pl.CompanyCode and dd.plantcode_factory=pl.plantcode
AND sg.Dim_DateidGoodsIssue <> dd.Dim_Dateid;

update staging_update_701 sg
Set Dim_DateidMtrlAvail = dd.Dim_Dateid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Date dd, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND dd.DateValue  =  vbep_mbdat AND dd.CompanyCode  =  pl.CompanyCode and dd.plantcode_factory=pl.plantcode
AND sg.Dim_DateidMtrlAvail <> dd.Dim_Dateid;


update staging_update_701 sg
Set Dim_DateidLoading = dd.Dim_Dateid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Date dd, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND dd.DateValue  =  vbep_lddat AND dd.CompanyCode  =  pl.CompanyCode and dd.plantcode_factory=pl.plantcode
AND sg.Dim_DateidLoading <> dd.Dim_Dateid;


update staging_update_701 sg
Set Dim_DateidGuaranteedate = dd.Dim_Dateid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Date dd, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode and dd.plantcode_factory=pl.plantcode
AND sg.Dim_DateidGuaranteedate <> dd.Dim_Dateid;



update staging_update_701 sg
Set Dim_DateidTransport = dd.Dim_Dateid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Date dd,staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND dd.DateValue  =  vbep_tddat AND dd.CompanyCode  =  pl.CompanyCode and dd.plantcode_factory=pl.plantcode
AND sg.Dim_DateidTransport <> dd.Dim_Dateid;



update staging_update_701 sg
Set Dim_Currencyid_TRA = cur.Dim_Currencyid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Currency cur, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND cur.CurrencyCode  =  VBAP_WAERK
AND sg.Dim_Currencyid_TRA <> cur.Dim_Currencyid;


update staging_update_701 sg
Set sg.Dim_ProductHierarchyid = ph.Dim_ProductHierarchyid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_ProductHierarchy ph, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND ph.ProductHierarchy  =  vbap_prodh
AND sg.Dim_ProductHierarchyid <> ph.Dim_ProductHierarchyid;


update staging_update_701 sg
Set Dim_Currencyid_STAT = cur.Dim_Currencyid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Currency cur,staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND cur.CurrencyCode  =  vbak_stwae
AND sg.Dim_Currencyid_STAT <> cur.Dim_Currencyid;

update staging_update_701 sg
Set Dim_Currencyid = cur.Dim_Currencyid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Currency cur, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND cur.CurrencyCode  =  co.currency
AND sg.Dim_Currencyid <> cur.Dim_Currencyid;


update staging_update_701 sg
Set Dim_Currencyid_GBL = cur.Dim_Currencyid
from fact_salesorder so,
        VBAK_VBAP_VBEP,
        Dim_Plant pl,
        Dim_Company co,
        Dim_SalesOrderItemCategory soic,
        VBAK_VBAP_VBEP_701 y,Dim_Currency cur, variable_holder_701,  staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND cur.CurrencyCode  =  pGlobalCurrency
AND sg.Dim_Currencyid_GBL <> cur.Dim_Currencyid;



update staging_update_701 sg
Set       Dim_Plantid  =  pl.Dim_Plantid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
        Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND sg.Dim_Plantid <> pl.Dim_Plantid;


update staging_update_701 sg
Set       sg.Dim_Companyid  =  co.Dim_Companyid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
        Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND sg.Dim_Companyid <> co.Dim_Companyid;


update staging_update_701 sg
SET Dim_StorageLocationid = sl.Dim_StorageLocationid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
        Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y, Dim_StorageLocation sl, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND sl.LocationCode  =  vbap_lgort and sl.plant  =  vbap_werks
AND sg.Dim_StorageLocationid <> sl.Dim_StorageLocationid;


update staging_update_701 sg
SET Dim_SalesDivisionid = sd.Dim_SalesDivisionid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
        Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y, Dim_SalesDivision sd, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND sd.DivisionCode  =  vbap_spart
AND sg.Dim_SalesDivisionid <> sd.Dim_SalesDivisionid;


update staging_update_701 sg
Set     Dim_ShipReceivePointid = srp.Dim_ShipReceivePointid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_ShipReceivePoint srp, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND srp.ShipReceivePointCode  =  vbap_vstel
AND sg.Dim_ShipReceivePointid <> srp.Dim_ShipReceivePointid;


update staging_update_701 sg
Set     Dim_DocumentCategoryid = dc.Dim_DocumentCategoryid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_DocumentCategory dc, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND dc.DocumentCategory  =  vbak_vbtyp
AND sg.Dim_DocumentCategoryid <> dc.Dim_DocumentCategoryid;

update staging_update_701 sg
Set     sg.Dim_SalesDocumentTypeid = sdt.Dim_SalesDocumentTypeid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_SalesDocumentType sdt,staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND sdt.DocumentType  =  vbak_auart
AND sg.Dim_SalesDocumentTypeid <> sdt.Dim_SalesDocumentTypeid;


update staging_update_701 sg
Set     sg.Dim_SalesOrgid = sog.Dim_SalesOrgid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_SalesOrg sog, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo  =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo  =  so.dd_ScheduleNo
AND sog.SalesOrgCode  =  vbak_vkorg
AND sg.Dim_SalesOrgid <> sog.Dim_SalesOrgid;



update staging_update_701 sg
Set       Dim_CustomerID =    dim.Dim_CustomerID
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
         VBAK_VBAP_VBEP_701 y,Dim_Customer dim, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND dim.CustomerNumber  =  vbak_kunnr
AND sg.Dim_CustomerID <> dim.Dim_CustomerID;


update staging_update_701 sg
Set       Dim_ScheduleLineCategoryId =    dim.Dim_ScheduleLineCategoryId
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,Dim_ScheduleLineCategory dim,
         VBAK_VBAP_VBEP_701 y, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND dim.ScheduleLineCategory  =  VBEP_ETTYP
AND sg.Dim_ScheduleLineCategoryId <> dim.Dim_ScheduleLineCategoryId;

update staging_update_701 sg
Set       Dim_DateidValidFrom =    vf.Dim_Dateid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,Dim_Date vf,
         VBAK_VBAP_VBEP_701 y,staging_update_701 sg
Where pl.PlantCode  =  ifnull(VBAP_WERKS,'Not Set') and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode and vf.plantcode_factory=pl.plantcode
AND sg.Dim_DateidValidFrom <> vf.Dim_Dateid;

update staging_update_701 sg
Set       Dim_DateidValidTo =    vt.Dim_Dateid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,Dim_Date vt,
         VBAK_VBAP_VBEP_701 y,staging_update_701 sg
Where pl.PlantCode  =  ifnull(VBAP_WERKS,'Not Set') and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode and vt.plantcode_factory=pl.plantcode
AND sg.Dim_DateidValidTo <> vt.Dim_Dateid;


update staging_update_701 stg
Set      Dim_SalesGroupid  = sg.Dim_SalesGroupid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_SalesGroup sg  , staging_update_701 stg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND stg.dd_SalesDocNo =  so.dd_SalesDocNo and stg.dd_SalesItemNo  =  so.dd_SalesItemNo and stg.dd_ScheduleNo =  so.dd_ScheduleNo
AND sg.SalesGroupCode  =  vbak_vkgrp
AND stg.Dim_SalesGroupid  <> sg.Dim_SalesGroupid;

update staging_update_701 stg
Set      stg.Dim_CostCenterid  = 1
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,staging_update_701 stg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND stg.dd_SalesDocNo =  so.dd_SalesDocNo and stg.dd_SalesItemNo  =  so.dd_SalesItemNo and stg.dd_ScheduleNo =  so.dd_ScheduleNo
AND stg.Dim_CostCenterid  <> 1;

update staging_update_701 stg
Set      Dim_ControllingAreaid  = ca.Dim_ControllingAreaid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_ControllingArea ca ,staging_update_701 stg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND stg.dd_SalesDocNo =  so.dd_SalesDocNo and stg.dd_SalesItemNo  =  so.dd_SalesItemNo and stg.dd_ScheduleNo =  so.dd_ScheduleNo
AND ca.ControllingAreaCode  =  vbak_kokrs
AND stg.dim_ControllingAreaid  <> ca.Dim_ControllingAreaid;


update staging_update_701 stg
Set      Dim_BillingBlockid  = bb.Dim_BillingBlockid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_BillingBlock bb ,staging_update_701 stg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND stg.dd_SalesDocNo =  so.dd_SalesDocNo and stg.dd_SalesItemNo  =  so.dd_SalesItemNo and stg.dd_ScheduleNo =  so.dd_ScheduleNo
AND bb.BillingBlockCode  =  vbap_faksp
AND stg.Dim_BillingBlockid  <> bb.Dim_BillingBlockid  ;


update staging_update_701 sg
Set       sg.Dim_TransactionGroupid  = tg.Dim_TransactionGroupid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_TransactionGroup tg ,staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND tg.TransactionGroup  =  vbak_trvog
AND sg.Dim_TransactionGroupid  <> tg.Dim_TransactionGroupid;


update staging_update_701 sg
Set       sg.Dim_SalesOrderRejectReasonid  = sorr.Dim_SalesOrderRejectReasonid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_SalesOrderRejectReason sorr ,staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND sorr.RejectReasonCode  =  vbap_abgru
AND sg.Dim_SalesOrderRejectReasonid  <> sorr.Dim_SalesOrderRejectReasonid;



update staging_update_701 sg
Set       sg.Dim_Partid  = dp.dim_partid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,dim_part dp ,staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS
AND ifnull(sg.Dim_Partid,-1)  <> ifnull(dp.dim_partid,-2);


update staging_update_701 sg
Set       sg.Dim_SalesOrderHeaderStatusid  = sohs.Dim_SalesOrderHeaderStatusid
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
                Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701 y,Dim_SalesOrderHeaderStatus sohs, staging_update_701 sg
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  so.dd_SalesDocNo and VBAP_POSNR  =  so.dd_SalesItemNo and VBEP_ETENR  =  so.dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND sg.dd_SalesDocNo =  so.dd_SalesDocNo and sg.dd_SalesItemNo  =  so.dd_SalesItemNo and sg.dd_ScheduleNo =  so.dd_ScheduleNo
AND sohs.SalesDocumentNumber  =  VBAK_VBELN
AND sg.Dim_SalesOrderHeaderStatusid  <> sohs.Dim_SalesOrderHeaderStatusid;


/*End of temporary code split */


/*update staging_update_701 sut
set Dim_CostCenterid  = cc.Dim_CostCenterid
from  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
           VBAK_VBAP_VBEP_701  y,Dim_CostCenter_701 cc,staging_update_701 sut
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  dd_SalesDocNo and VBAP_POSNR  =  dd_SalesItemNo and VBEP_ETENR  =  dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND cc.Code  =  vbak_kostl and cc.ControllingArea  =  vbak_kokrs and cc.RowIsCurrent  =  1
AND sut.Dim_CostCenterid <> cc.Dim_CostCenterid*/

drop table if exists tmp_for_upd_costcenter;
create table tmp_for_upd_costcenter as
select distinct cc.Dim_CostCenterid,VBAK_VBELN,VBAP_POSNR, VBEP_ETENR, row_number() over (partition by VBAK_VBELN,VBAP_POSNR, VBEP_ETENR order by VBAK_VBELN,VBAP_POSNR, VBEP_ETENR) as counter
from  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
           VBAK_VBAP_VBEP_701  y,Dim_CostCenter_701 cc,staging_update_701 sut
Where pl.PlantCode  =  VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  dd_SalesDocNo and VBAP_POSNR  =  dd_SalesItemNo and VBEP_ETENR  =  dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND cc.Code  =  vbak_kostl and cc.ControllingArea  =  vbak_kokrs and cc.RowIsCurrent  =  1
AND sut.Dim_CostCenterid <> cc.Dim_CostCenterid;

update staging_update_701 sut
set sut.Dim_CostCenterid  = cc.Dim_CostCenterid
from tmp_for_upd_costcenter cc, staging_update_701 sut
where VBAK_VBELN  =  dd_SalesDocNo and VBAP_POSNR  =  dd_SalesItemNo and VBEP_ETENR  =  dd_ScheduleNo
and counter=1
AND sut.Dim_CostCenterid <> cc.Dim_CostCenterid;

/* Madalina 21 Oct 2016 - Merge all the following updates from staging_update_701 into one single statement - BI-4486*/
merge into fact_salesorder so using
	( select
		sut.dd_SalesDocNo,
		sut.dd_SalesItemNo,
		sut.dd_ScheduleNo,
		sut.Dim_DateidSchedDeliveryReq,
		sut.Dim_DateidSchedDelivery,
		sut.Dim_DateidGoodsIssue,
		sut.Dim_DateidMtrlAvail,
		sut.Dim_DateidLoading,
		sut.Dim_DateidGuaranteedate,
		sut.Dim_DateidTransport,
		sut.Dim_Currencyid,
		sut.Dim_ProductHierarchyid,
		sut.Dim_Plantid,
		sut.Dim_Companyid,
		sut.Dim_StorageLocationid,
		sut.Dim_SalesDivisionid,
		sut.Dim_ShipReceivePointid,
		sut.Dim_DocumentCategoryid,
		sut.Dim_SalesDocumentTypeid,
		sut.Dim_SalesOrgid,
		sut.Dim_CustomerID,
		sut.Dim_ScheduleLineCategoryId,
		sut.Dim_DateidValidFrom,
		sut.Dim_DateidValidTo,
		sut.Dim_SalesGroupid,
		sut.Dim_CostCenterid,
		sut.Dim_ControllingAreaid,
		sut.Dim_BillingBlockid,
		sut.Dim_TransactionGroupid,
		sut.Dim_SalesOrderRejectReasonid,
		sut.Dim_Partid,
		sut.Dim_SalesOrderHeaderStatusid
	from staging_update_701 sut ) sut
on so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
when matched then update
set
	so.Dim_DateidSchedDeliveryReq = sut.Dim_DateidSchedDeliveryReq,
	so.Dim_DateidSchedDelivery = sut.Dim_DateidSchedDelivery,
	so.Dim_DateidGoodsIssue = sut.Dim_DateidGoodsIssue,
	so.Dim_DateidMtrlAvail = sut.Dim_DateidMtrlAvail,
	so.Dim_DateidLoading = sut.Dim_DateidLoading,
	so.Dim_DateidGuaranteedate = sut.Dim_DateidGuaranteedate,
	so.Dim_DateidTransport = sut.Dim_DateidTransport,
	so.Dim_Currencyid = sut.Dim_Currencyid,
	so.Dim_ProductHierarchyid = sut.Dim_ProductHierarchyid,
	so.Dim_Plantid = sut.Dim_Plantid,
	so.Dim_Companyid = sut.Dim_Companyid,
	so.Dim_StorageLocationid = sut.Dim_StorageLocationid,
	so.Dim_SalesDivisionid = sut.Dim_SalesDivisionid,
	so.Dim_ShipReceivePointid = sut.Dim_ShipReceivePointid,
	so.Dim_DocumentCategoryid = sut.Dim_DocumentCategoryid,
	so.Dim_SalesDocumentTypeid = sut.Dim_SalesDocumentTypeid,
	so.Dim_SalesOrgid = sut.Dim_SalesOrgid,
	so.Dim_CustomerID = sut.Dim_CustomerID,
	so.Dim_ScheduleLineCategoryId = sut.Dim_ScheduleLineCategoryId,
	so.Dim_DateidValidFrom = sut.Dim_DateidValidFrom,
	so.Dim_DateidValidTo = sut.Dim_DateidValidTo,
	so.Dim_SalesGroupid = sut.Dim_SalesGroupid,
	so.Dim_CostCenterid = sut.Dim_CostCenterid,
	so.Dim_ControllingAreaid = sut.Dim_ControllingAreaid,
	so.Dim_BillingBlockid = sut.Dim_BillingBlockid,
	so.Dim_TransactionGroupid = sut.Dim_TransactionGroupid,
	so.Dim_SalesOrderRejectReasonid = sut.Dim_SalesOrderRejectReasonid,
	so.Dim_Partid = sut.Dim_Partid,
	so.Dim_SalesOrderHeaderStatusid = sut.Dim_SalesOrderHeaderStatusid;
/*
update fact_salesorder so
SET
     so.Dim_DateidSchedDeliveryReq = sut.Dim_DateidSchedDeliveryReq
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND      so.Dim_DateidSchedDeliveryReq <> sut.Dim_DateidSchedDeliveryReq

update fact_salesorder so
SET so.Dim_DateidSchedDelivery = sut.Dim_DateidSchedDelivery
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DateidSchedDelivery <> sut.Dim_DateidSchedDelivery

update fact_salesorder so
SET so.Dim_DateidGoodsIssue = sut.Dim_DateidGoodsIssue
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DateidGoodsIssue <> sut.Dim_DateidGoodsIssue


update fact_salesorder so
SET so.Dim_DateidMtrlAvail = sut.Dim_DateidMtrlAvail
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DateidMtrlAvail <> sut.Dim_DateidMtrlAvail

update fact_salesorder so
SET
     so.Dim_DateidLoading = sut.Dim_DateidLoading
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND      so.Dim_DateidLoading <> sut.Dim_DateidLoading

update fact_salesorder so
SET so.Dim_DateidGuaranteedate = sut.Dim_DateidGuaranteedate
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DateidGuaranteedate <> sut.Dim_DateidGuaranteedate

update fact_salesorder so
SET so.Dim_DateidTransport = sut.Dim_DateidTransport
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DateidTransport <> sut.Dim_DateidTransport

update fact_salesorder so
SET so.Dim_Currencyid = sut.Dim_Currencyid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_Currencyid <> sut.Dim_Currencyid



update fact_salesorder so
SET
     so.Dim_ProductHierarchyid = sut.Dim_ProductHierarchyid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND      so.Dim_ProductHierarchyid <> sut.Dim_ProductHierarchyid

update fact_salesorder so
SET so.Dim_Plantid = sut.Dim_Plantid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_Plantid <> sut.Dim_Plantid

update fact_salesorder so
SET so.Dim_Companyid = sut.Dim_Companyid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_Companyid <> sut.Dim_Companyid

update fact_salesorder so
SET so.Dim_StorageLocationid = sut.Dim_StorageLocationid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_StorageLocationid <> sut.Dim_StorageLocationid


update fact_salesorder so
SET
     so.Dim_SalesDivisionid = sut.Dim_SalesDivisionid
	 from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND      so.Dim_SalesDivisionid <> sut.Dim_SalesDivisionid

update fact_salesorder so
SET so.Dim_ShipReceivePointid = sut.Dim_ShipReceivePointid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_ShipReceivePointid <> sut.Dim_ShipReceivePointid

update fact_salesorder so
SET so.Dim_DocumentCategoryid = sut.Dim_DocumentCategoryid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DocumentCategoryid <> sut.Dim_DocumentCategoryid

update fact_salesorder so
SET so.Dim_SalesDocumentTypeid = sut.Dim_SalesDocumentTypeid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_SalesDocumentTypeid <> sut.Dim_SalesDocumentTypeid


update fact_salesorder so
SET
     so.Dim_SalesOrgid = sut.Dim_SalesOrgid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND      so.Dim_SalesOrgid <> sut.Dim_SalesOrgid

update fact_salesorder so
SET so.Dim_CustomerID = sut.Dim_CustomerID
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_CustomerID <> sut.Dim_CustomerID

update fact_salesorder so
SET so.Dim_ScheduleLineCategoryId = sut.Dim_ScheduleLineCategoryId
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_ScheduleLineCategoryId <> sut.Dim_ScheduleLineCategoryId

update fact_salesorder so
SET so.Dim_DateidValidFrom = sut.Dim_DateidValidFrom
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DateidValidFrom <> sut.Dim_DateidValidFrom


update fact_salesorder so
SET
     so.Dim_DateidValidTo = sut.Dim_DateidValidTo
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND      so.Dim_DateidValidTo <> sut.Dim_DateidValidTo

update fact_salesorder so
SET so.Dim_SalesGroupid = sut.Dim_SalesGroupid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_SalesGroupid <> sut.Dim_SalesGroupid

update fact_salesorder so
SET so.Dim_CostCenterid = sut.Dim_CostCenterid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_CostCenterid <> sut.Dim_CostCenterid

update fact_salesorder so
SET so.Dim_ControllingAreaid = sut.Dim_ControllingAreaid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_ControllingAreaid <> sut.Dim_ControllingAreaid

update fact_salesorder so
SET
     so.Dim_BillingBlockid = sut.Dim_BillingBlockid
	 from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND      so.Dim_BillingBlockid <> sut.Dim_BillingBlockid

update fact_salesorder so
SET so.Dim_TransactionGroupid = sut.Dim_TransactionGroupid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_TransactionGroupid <> sut.Dim_TransactionGroupid

update fact_salesorder so
SET so.Dim_SalesOrderRejectReasonid = sut.Dim_SalesOrderRejectReasonid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_SalesOrderRejectReasonid <> sut.Dim_SalesOrderRejectReasonid

update fact_salesorder so
SET so.Dim_Partid = sut.Dim_Partid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_Partid <> sut.Dim_Partid

update fact_salesorder so
SET so.Dim_SalesOrderHeaderStatusid = sut.Dim_SalesOrderHeaderStatusid
from staging_update_701 sut,fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_SalesOrderHeaderStatusid <> sut.Dim_SalesOrderHeaderStatusid

 END 21 Oct 2016 */

drop table if exists staging_update_701;

CREATE TABLE staging_update_701
AS
SELECT convert(bigint, 1) as Dim_SalesOrderItemStatusid, --ifnull((select sois.Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber  =  VBAK_VBELN and sois.SalesItemNumber  =  VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
       convert(bigint, 1) as Dim_CustomerGroup1id, --ifnull((select cg1.Dim_CustomerGroup1id from Dim_CustomerGroup1 cg1 where cg1.CustomerGroup  =  VBAK_KVGR1),1) Dim_CustomerGroup1id,
       convert(bigint, 1) as Dim_CustomerGroup2id, --ifnull((select cg2.Dim_CustomerGroup2id from Dim_CustomerGroup2 cg2 where cg2.CustomerGroup  =  VBAK_KVGR2),1) Dim_CustomerGroup2id,
	   soic.Dim_SalesOrderItemCategoryid Dim_salesorderitemcategoryid,
       ifnull(vbep_lfrel, 'Not Set') dd_ItemRelForDelv,
	   convert(bigint, 1) as Dim_ProfitCenterId,
       convert(bigint, 1) as Dim_DistributionChannelId, --ifnull((select dc.Dim_DistributionChannelid from dim_DistributionChannel dc where   dc.DistributionChannelCode  =  VBAK_VTWEG AND dc.RowIsCurrent  =  1), 1) Dim_DistributionChannelId,
	   convert(bigint, 1) as Dim_UnitOfMeasureId, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_kmein AND uom.RowIsCurrent  =  1), 1) Dim_UnitOfMeasureId,
	   convert(bigint, 1) as Dim_BaseUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_meins AND uom.RowIsCurrent  =  1), 1) Dim_BaseUoMid,
	   convert(bigint, 1) as Dim_SalesUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_vrkme AND uom.RowIsCurrent  =  1), 1) Dim_SalesUoMid,
       ifnull(VBAP_CHARG, 'Not Set') dd_BatchNo,
       ifnull(VBAK_ERNAM, 'Not Set') dd_CreatedBy,
       convert(bigint, 1) as Dim_DateidNextDate, --ifnull((SELECT nd.Dim_Dateid FROM Dim_Date nd WHERE nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidNextDate,
	   convert(bigint, 1) as Dim_RouteId, --ifnull((SELECT r.dim_routeid from dim_route r where r.RouteCode  =  VBAP_ROUTE and r.RowIsCurrent  =  1),1) Dim_RouteId,

	dd_SalesDocNo,
	dd_SalesItemNo,
	   dd_ScheduleNo,
	   VBAK_KVGR1,
	   VBAK_KVGR2,
	   VBAK_VTWEG,
	   vbap_kmein,
	   vbap_meins,
	   vbap_vrkme,
	   VBAK_CMNGV,
	   pl.CompanyCode as companycode,
	   VBAP_ROUTE,
	   VBAP_WERKS
from fact_salesorder so, VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
	Dim_SalesOrderItemCategory soic,
          VBAK_VBAP_VBEP_701  y
Where pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
      and VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
      and VBAK_VBELN = y.v_SaleseDocNo and VBAP_POSNR = y.v_SalesItemNo
	AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

UPDATE staging_update_701 sut
SET sut.Dim_SalesOrderItemStatusid = IFNULL(sois.Dim_SalesOrderItemStatusid, 1)
FROM staging_update_701 sut
     LEFT JOIN Dim_SalesOrderItemStatus sois on sois.SalesDocumentNumber = dd_SalesDocNo and sois.SalesItemNumber  =  dd_SalesItemNo
WHERE sut.Dim_SalesOrderItemStatusid <> IFNULL(sois.Dim_SalesOrderItemStatusid, 1);

UPDATE staging_update_701 sut
SET sut.Dim_CustomerGroup1id = IFNULL(cg1.Dim_CustomerGroup1id, 1)
FROM staging_update_701 sut
     LEFT JOIN Dim_CustomerGroup1 cg1 on cg1.CustomerGroup  =  VBAK_KVGR1
WHERE sut.Dim_CustomerGroup1id <> IFNULL(cg1.Dim_CustomerGroup1id, 1);

UPDATE staging_update_701 sut
SET sut.Dim_CustomerGroup2id = IFNULL(cg2.Dim_CustomerGroup2id, 1)
FROM staging_update_701 sut
     LEFT JOIN Dim_CustomerGroup2 cg2 on cg2.CustomerGroup  =  VBAK_KVGR2
WHERE sut.Dim_CustomerGroup2id <> IFNULL(cg2.Dim_CustomerGroup2id, 1);

UPDATE staging_update_701 sut
SET sut.Dim_DistributionChannelId = IFNULL(dc.Dim_DistributionChannelid, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_DistributionChannel WHERE RowIsCurrent  =  1) dc on dc.DistributionChannelCode  =  VBAK_VTWEG
WHERE sut.Dim_DistributionChannelId <> IFNULL(dc.Dim_DistributionChannelid, 1);

UPDATE staging_update_701 sut
SET sut.Dim_UnitOfMeasureId = IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE RowIsCurrent  =  1) uom on uom.UOM  =  vbap_kmein
WHERE sut.Dim_UnitOfMeasureId <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_update_701 sut
SET sut.Dim_BaseUoMid = IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE RowIsCurrent  =  1) uom on uom.UOM  =  vbap_meins
WHERE sut.Dim_BaseUoMid <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

UPDATE staging_update_701 sut
SET sut.Dim_SalesUoMid = IFNULL(uom.Dim_UnitOfMeasureId, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE RowIsCurrent  =  1) uom on uom.UOM  =  vbap_vrkme
WHERE sut.Dim_SalesUoMid <> IFNULL(uom.Dim_UnitOfMeasureId, 1);

merge into  staging_update_701 sut
using (select distinct  dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,nd.Dim_Dateid 
FROM staging_update_701 sut
     INNER JOIN dim_plant pl on pl.PlantCode = VBAP_WERKS and sut.CompanyCode = pl.CompanyCode
     LEFT JOIN Dim_Date nd on nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode = sut.CompanyCode AND nd.plantcode_factory=pl.plantcode
WHERE sut.Dim_DateidNextDate <> IFNULL(nd.Dim_Dateid, 1))t
on t.dd_SalesDocNo=sut.dd_SalesDocNo and t.dd_SalesItemNo=sut.dd_SalesItemNo and t.dd_ScheduleNo=sut.dd_ScheduleNo
when matched then update set sut.Dim_DateidNextDate = IFNULL(t.Dim_Dateid, 1);

UPDATE staging_update_701 sut
SET sut.Dim_RouteId = IFNULL(r.dim_routeid, 1)
FROM staging_update_701 sut
     LEFT JOIN (SELECT * FROM dim_route where rowiscurrent = 1) r on r.RouteCode  =  VBAP_ROUTE
WHERE sut.Dim_RouteId <> IFNULL(r.dim_routeid, 1);


update staging_update_701 sut
set Dim_ProfitCenterId  =  pc.dim_profitcenterid
from  VBAK_VBAP_VBEP v,
          Dim_Plant pl,
          Dim_Company co,
        Dim_SalesOrderItemCategory soic,
           VBAK_VBAP_VBEP_701 y, dim_profitcenter_701 pc, staging_update_701 sut
Where pl.PlantCode  =  v.VBAP_WERKS and co.CompanyCode  =  pl.CompanyCode
      and VBAK_VBELN  =  dd_SalesDocNo and VBAP_POSNR  =  dd_SalesItemNo and VBEP_ETENR  =  dd_ScheduleNo
      and VBAK_VBELN  =  y.v_SaleseDocNo and VBAP_POSNR  =  y.v_SalesItemNo
      AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND  pc.ProfitCenterCode  =  VBAP_PRCTR AND pc.ControllingArea  =  VBAK_KOKRS AND pc.ValidTo >=  VBAK_ERDAT AND pc.RowIsCurrent  =  1
AND sut.Dim_ProfitCenterId <> pc.dim_profitcenterid;

/* Madalina 24 Oct 2016 - Merge all the following updates from staging_update_701 into one single statement - BI-4486*/
merge into fact_salesorder so using
	( select
		sut.dd_SalesDocNo,
		sut.dd_SalesItemNo,
		sut.dd_ScheduleNo,
		sut.Dim_SalesOrderItemStatusid,
		sut.Dim_CustomerGroup1id,
		sut.Dim_CustomerGroup2id,
		sut.Dim_salesorderitemcategoryid,
		sut.dd_ItemRelForDelv,
		sut.Dim_ProfitCenterId,
		sut.Dim_DistributionChannelId,
		sut.Dim_UnitOfMeasureId,
		sut.Dim_BaseUoMid,
		sut.Dim_SalesUoMid,
		sut.dd_BatchNo,
		sut.dd_CreatedBy,
		sut.Dim_DateidNextDate,
		sut.Dim_RouteId
	from staging_update_701 sut ) sut
on so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
when matched then update
set	
	so.Dim_SalesOrderItemStatusid = sut.Dim_SalesOrderItemStatusid,
	so.Dim_CustomerGroup1id = sut.Dim_CustomerGroup1id,
	so.Dim_CustomerGroup2id = sut.Dim_CustomerGroup2id,
	so.Dim_salesorderitemcategoryid = sut.Dim_salesorderitemcategoryid,
	so.dd_ItemRelForDelv = sut.dd_ItemRelForDelv,
	so.Dim_ProfitCenterId = sut.Dim_ProfitCenterId,
	so.Dim_DistributionChannelId = sut.Dim_DistributionChannelId,
	so.Dim_UnitOfMeasureId = sut.Dim_UnitOfMeasureId,
	so.Dim_BaseUoMid = sut.Dim_BaseUoMid,
	so.Dim_SalesUoMid = sut.Dim_SalesUoMid,
	so.dd_BatchNo = sut.dd_BatchNo,
	so.dd_CreatedBy = sut.dd_CreatedBy,
	so.Dim_DateidNextDate = sut.Dim_DateidNextDate,
	so.Dim_RouteId = sut.Dim_RouteId;
	
/*	
update fact_salesorder so
SET
  so.Dim_SalesOrderItemStatusid = sut.Dim_SalesOrderItemStatusid
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND   so.Dim_SalesOrderItemStatusid <> sut.Dim_SalesOrderItemStatusid

update fact_salesorder so
SET so.Dim_CustomerGroup1id = sut.Dim_CustomerGroup1id
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_CustomerGroup1id <> sut.Dim_CustomerGroup1id

update fact_salesorder so
SET so.Dim_CustomerGroup2id = sut.Dim_CustomerGroup2id
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_CustomerGroup2id <> sut.Dim_CustomerGroup2id

update fact_salesorder so
SET so.Dim_salesorderitemcategoryid = sut.Dim_salesorderitemcategoryid
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_salesorderitemcategoryid <> sut.Dim_salesorderitemcategoryid

update fact_salesorder so
SET so.dd_ItemRelForDelv = sut.dd_ItemRelForDelv
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.dd_ItemRelForDelv <> sut.dd_ItemRelForDelv

update fact_salesorder so
SET
  so.Dim_ProfitCenterId = sut.Dim_ProfitCenterId
 from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND   so.Dim_ProfitCenterId <> sut.Dim_ProfitCenterId

update fact_salesorder so
SET so.Dim_DistributionChannelId = sut.Dim_DistributionChannelId
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DistributionChannelId <> sut.Dim_DistributionChannelId

update fact_salesorder so
SET so.Dim_UnitOfMeasureId = sut.Dim_UnitOfMeasureId
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_UnitOfMeasureId <> sut.Dim_UnitOfMeasureId

update fact_salesorder so
SET so.Dim_BaseUoMid  =  sut.Dim_BaseUoMid
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_BaseUoMid  <>  sut.Dim_BaseUoMid


update fact_salesorder so
SET so.Dim_SalesUoMid  =  sut.Dim_SalesUoMid
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_SalesUoMid  <>  sut.Dim_SalesUoMid

update fact_salesorder so
SET so.dd_BatchNo = sut.dd_BatchNo
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.dd_BatchNo <> sut.dd_BatchNo

update fact_salesorder so
SET so.dd_CreatedBy = sut.dd_CreatedBy
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.dd_CreatedBy <> sut.dd_CreatedBy

update fact_salesorder so
SET so.Dim_DateidNextDate = sut.Dim_DateidNextDate
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_DateidNextDate <> sut.Dim_DateidNextDate

update fact_salesorder so
SET so.Dim_RouteId = sut.Dim_RouteId
from staging_update_701 sut, fact_salesorder so
where so.dd_SalesDocNo = sut.dd_SalesDocNo and so.dd_SalesItemNo = sut.dd_SalesItemNo and so.dd_ScheduleNo = sut.dd_ScheduleNo
AND so.Dim_RouteId <> sut.Dim_RouteId
*/
/* END 24 Oct 2016 */

drop table if exists vvv_min_i20_1;
create table vvv_min_i20_1 as SELECT min(x.VBEP_ETENR) v_SalesSchedNo, x.VBAK_VBELN v_SaleseDocNo, x.VBAP_POSNR v_SalesItemNo
FROM VBAK_VBAP_VBEP x GROUP BY x.VBAK_VBELN, x.VBAP_POSNR;


update fact_salesorder so
SET  Dim_SalesRiskCategoryId  = src.Dim_SalesRiskCategoryId
FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
          Dim_SalesOrderItemCategory soic,
	  vvv_min_i20_1 y, Dim_SalesRiskCategory src,fact_salesorder so
  WHERE pl.PlantCode  =  VBAP_WERKS AND co.CompanyCode  =  pl.CompanyCode
      AND VBAK_VBELN  =  dd_SalesDocNo AND VBAP_POSNR  =  dd_SalesItemNo AND VBEP_ETENR  =  dd_ScheduleNo
      AND VBAK_VBELN  =  y.v_SaleseDocNo AND VBAP_POSNR  =  y.v_SalesItemNo
        AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER and src.RowIsCurrent  =  1
AND so.Dim_SalesRiskCategoryId <> src.Dim_SalesRiskCategoryId;

update fact_salesorder so
SET  dd_CreditLimit  = ifnull(c.CREDIT_LIMIT,0)
FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
          Dim_SalesOrderItemCategory soic,
		vvv_min_i20_1 y,
		  TMP_UPD_dd_CreditLimit c,fact_salesorder so
  WHERE pl.PlantCode  =  VBAP_WERKS AND co.CompanyCode  =  pl.CompanyCode
      AND VBAK_VBELN  =  dd_SalesDocNo AND VBAP_POSNR  =  dd_SalesItemNo AND VBEP_ETENR  =  dd_ScheduleNo
      AND VBAK_VBELN  =  y.v_SaleseDocNo AND VBAP_POSNR  =  y.v_SalesItemNo
        AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND c.CUSTOMER  =  VBAK_KUNNR
AND so.dd_CreditLimit <> ifnull(c.CREDIT_LIMIT,0);


update fact_salesorder so
SET  Dim_CustomerRiskCategoryId  = src.dim_salesriskcategoryid
FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
          Dim_SalesOrderItemCategory soic,
	  vvv_min_i20_1 y,
		  TMP_UPD_Dim_CustomerRiskCategoryId src,fact_salesorder so
  WHERE pl.PlantCode  =  VBAP_WERKS AND co.CompanyCode  =  pl.CompanyCode
      AND VBAK_VBELN  =  dd_SalesDocNo AND VBAP_POSNR  =  dd_SalesItemNo AND VBEP_ETENR  =  dd_ScheduleNo
      AND VBAK_VBELN  =  y.v_SaleseDocNo AND VBAP_POSNR  =  y.v_SalesItemNo
        AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND src.CUSTOMER  =  VBAK_KUNNR and src.CreditControlArea  =  VBAK_KKBER
AND so.Dim_CustomerRiskCategoryId <> src.dim_salesriskcategoryid;

update fact_salesorder so
SET  dd_CreditRep  = b.upd_dd_CreditRep1
FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co,
          Dim_SalesOrderItemCategory soic,
	vvv_min_i20_1 y, TMP_UPD_dd_CreditRep1 b, fact_salesorder so
  WHERE pl.PlantCode  =  VBAP_WERKS AND co.CompanyCode  =  pl.CompanyCode
      AND VBAK_VBELN  =  dd_SalesDocNo AND VBAP_POSNR  =  dd_SalesItemNo AND VBEP_ETENR  =  dd_ScheduleNo
      AND VBAK_VBELN  =  y.v_SaleseDocNo AND VBAP_POSNR  =  y.v_SalesItemNo
        AND soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
AND  b.BUT050_PARTNER2  =  VBAK_KUNNR
AND so.dd_CreditRep <> b.upd_dd_CreditRep1;


drop table if exists vvv_min_i20_1;


/* 12 Apr changes - sync with mysql changes	by Shanthi part 2 ends	*/

/* OZ: Remove dim_costcenter update as that only brings null values that don't get updated
DROP TABLE IF EXISTS staging_update_701

DROP TABLE IF EXISTS Dim_CostCenter_first1_701
CREATE TABLE Dim_CostCenter_first1_701
AS
SELECT code,
       ControllingArea,
	   RowIsCurrent,
	   MAX(Validto) validto,
	   CONVERT(BIGINT, NULL) dim_costcenterid
FROM Dim_CostCenter
GROUP BY code,
         ControllingArea,
		 RowIsCurrent

UPDATE Dim_CostCenter_first1_701 b
SET b.dim_costcenterid = a.dim_costcenterid
FROM Dim_CostCenter_first1_701 b
     INNER JOIN Dim_CostCenter_701  a ON b.code = a.code and b.ControllingArea = a.ControllingArea and b.RowIsCurrent = a.RowIsCurrent and b.validto = a.validto

OZ: Remove dim_costcenter update as that only brings null values that don't get updated */



DROP TABLE IF EXISTS max_holder_701;
CREATE TABLE max_holder_701
AS
SELECT ifnull(max(fact_salesorderid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) maxid
FROM fact_salesorder;

drop table if exists fact_salesorder_tmptbl;
create table fact_salesorder_tmptbl  like fact_salesorder INCLUDING DEFAULTS INCLUDING IDENTITY ;

DROP TABLE IF EXISTS fact_salesorder_useinsub;
CREATE TABLE fact_salesorder_useinsub
AS
SELECT dd_SalesDocNo,
       dd_SalesItemNo,
	   dd_ScheduleNo
FROM fact_salesorder;


drop table if exists vvv_di90;
create table vvv_di90 as select * from VBAK_VBAP_VBEP where exists (select 1 from dim_date mdt where mdt.DateValue = vbap_erdat and mdt.Dim_Dateid > 1);



DROP TABLE IF EXISTS tmp_pop_fact_salesorder_tmptbl;
CREATE TABLE tmp_pop_fact_salesorder_tmptbl
AS
SELECT v.*,
       pl.PlantCode,
       pl.CompanyCode,
       pl.dim_plantid,
       soic.Dim_SalesOrderItemCategoryid,
       co.Dim_Companyid,
       y.v_SalesSchedNo,
       co.Currency
FROM vvv_di90 v
      inner join Dim_Plant pl on pl.PlantCode = VBAP_WERKS
      inner join Dim_Company co on co.CompanyCode = pl.CompanyCode
	INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
      inner join VBAK_VBAP_VBEP_701 y
              on VBAK_VBELN = y.v_SaleseDocNo and VBAP_POSNR = y.v_SalesItemNo
  WHERE not exists (select 1 from fact_salesorder_useinsub f
                    where f.dd_SalesDocNo = VBAK_VBELN and f.dd_SalesItemNo = VBAP_POSNR and f.dd_ScheduleNo = VBEP_ETENR);

drop table if exists vvv_di90;

  INSERT
    INTO fact_salesorder_tmptbl(
	    fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
	    Dim_RouteId,
	    Dim_SalesRiskCategoryId,
            dd_CreditRep,
            dd_CreditLimit,
            Dim_CustomerRiskCategoryId,
	        amt_UnitPriceUoM,
	        dim_Currencyid_TRA,
	        dim_Currencyid_GBL,
	        dim_currencyid_STAT,
	        amt_exchangerate_STAT)
SELECT max_holder_701.maxid + row_number() over(order by ''),
          vbak_vbeln dd_SalesDocNo,
          vbap_posnr dd_SalesItemNo,
          vbep_etenr dd_ScheduleNo,
          vbep_wmeng ct_ScheduleQtySalesUnit,
          vbep_bmeng ct_ConfirmedQty,
          vbep_cmeng ct_CorrectedQty,
       ifnull(convert(numeric(18,4), vbap_netpr),0) amt_UnitPrice,
          vbap_kpein ct_PriceUnit,
       convert(numeric(18,4), CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0)
			WHEN vbep_wmeng = VBAP_KWMENG THEN VBAP_NETWR
			ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0)
		   END) amt_ScheduleTotal,
       convert(numeric(18,4), 0) as amt_StdCost,
       convert(numeric(18,4), 0) as amt_TargetValue,
	   ifnull(convert(numeric(18,4), CASE ifnull(vbap_mwsbp,0) WHEN 0 THEN 0
				ELSE
				(vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0) = 0 THEN 1
						WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
						THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1.000000 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							WHEN 0 THEN 1
							ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1.000000 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							END
						ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1.000000 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
					 END)
		END * (vbep_bmeng)), 0) amt_Tax,
       convert(numeric(18,4), 0) as ct_TargetQty,
       convert(bigint, 1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  pl.Currency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate,
	   convert(bigint, 1) as amt_ExchangeRate_GBL, --ifnull( ( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate_GBL ,
       vbap_uebto ct_OverDlvrTolerance,
       vbap_untto ct_UnderDlvrTolerance,
       convert(bigint, 1) as Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint, 1) as Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
       convert(bigint, 1) as Dim_DateidSchedDeliveryReq, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
       convert(bigint, 1) as Dim_DateidSchedDlvrReqPrev, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
       convert(bigint, 1) as Dim_DateidSchedDelivery, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_edatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDelivery,
       convert(bigint, 1) as Dim_DateidGoodsIssue, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_wadat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGoodsIssue,
       convert(bigint, 1) as Dim_DateidMtrlAvail, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_mbdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidMtrlAvail,
       convert(bigint, 1) as Dim_DateidLoading, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_lddat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidLoading,
       convert(bigint, 1) as Dim_DateidTransport, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbep_tddat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidTransport,
       convert(bigint, 1) as Dim_DateidGuaranteedate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGuaranteedate,
       convert(bigint, 1) as Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pl.currency ),1) Dim_Currencyid,
       convert(bigint, 1) as Dim_ProductHierarchyid, --ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy  =  vbap_prodh),1) Dim_ProductHierarchyid,
       pl.Dim_Plantid,
       pl.Dim_Companyid,
       convert(bigint, 1) as Dim_StorageLocationid, --ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode  =  vbap_lgort and sl.plant  =  vbap_werks),1) Dim_StorageLocationid,
       convert(bigint, 1) as Dim_SalesDivisionid, --ifnull((SELECT Dim_SalesDivisionid FROM Dim_SalesDivision sd WHERE sd.DivisionCode  =  vbap_spart),1) Dim_SalesDivisionid,
       convert(bigint, 1) as Dim_ShipReceivePointid, --ifnull((SELECT Dim_ShipReceivePointid FROM Dim_ShipReceivePoint srp WHERE srp.ShipReceivePointCode  =  vbap_vstel),1) Dim_ShipReceivePointid,
       convert(bigint, 1) as Dim_DocumentCategoryid, --ifnull((SELECT Dim_DocumentCategoryid FROM Dim_DocumentCategory dc WHERE dc.DocumentCategory  =  vbak_vbtyp),1) Dim_DocumentCategoryid,
       convert(bigint, 1) as Dim_SalesDocumentTypeid, --ifnull((SELECT Dim_SalesDocumentTypeid FROM Dim_SalesDocumentType sdt WHERE sdt.DocumentType  =  vbak_auart),1) Dim_SalesDocumentTypeid,
       convert(bigint, 1) as Dim_SalesOrgid, --ifnull((SELECT Dim_SalesOrgid FROM Dim_SalesOrg so WHERE so.SalesOrgCode  =  vbak_vkorg),1) Dim_SalesOrgid,
       convert(bigint, 1) as Dim_CustomerID, --ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber  =  vbak_kunnr),1) Dim_CustomerID,
       convert(bigint, 1) as Dim_DateidValidFrom, --ifnull((SELECT Dim_Dateid FROM Dim_Date vf WHERE vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidFrom,
       convert(bigint, 1) as Dim_DateidValidTo, --ifnull((SELECT Dim_Dateid FROM Dim_Date vt WHERE vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidTo,
       convert(bigint, 1) as Dim_SalesGroupid, --ifnull((SELECT Dim_SalesGroupid FROM Dim_SalesGroup sg WHERE sg.SalesGroupCode  =  vbak_vkgrp),1) Dim_SalesGroupid,
       convert(bigint, 1) as Dim_CostCenterid, --ifnull((SELECT Dim_CostCenterid FROM Dim_CostCenter_first1_701 cc WHERE cc.Code  =  vbak_kostl and cc.ControllingArea  =  vbak_kokrs and cc.RowIsCurrent  =  1),1) Dim_CostCenterid,
       convert(bigint, 1) as Dim_ControllingAreaid, --ifnull((SELECT Dim_ControllingAreaid FROM Dim_ControllingArea ca WHERE ca.ControllingAreaCode  =  vbak_kokrs),1) Dim_ControllingAreaid,
       convert(bigint, 1) as Dim_BillingBlockid, --ifnull((SELECT Dim_BillingBlockid FROM Dim_BillingBlock bb WHERE bb.BillingBlockCode  =  vbap_faksp),1) Dim_BillingBlockid,
       convert(bigint, 1) as Dim_TransactionGroupid, --ifnull((SELECT Dim_TransactionGroupid FROM Dim_TransactionGroup tg WHERE tg.TransactionGroup  =  vbak_trvog),1) Dim_TransactionGroupid,
       convert(bigint, 1) as Dim_SalesOrderRejectReasonid, --ifnull((SELECT Dim_SalesOrderRejectReasonid FROM Dim_SalesOrderRejectReason sorr WHERE sorr.RejectReasonCode  =  vbap_abgru),1) Dim_SalesOrderRejectReasonid,
       convert(bigint, 1) as Dim_Partid, --ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS),1) Dim_Partid,
       convert(bigint, 1) as Dim_SalesOrderHeaderStatusid, --ifnull((select Dim_SalesOrderHeaderStatusid from Dim_SalesOrderHeaderStatus sohs where sohs.SalesDocumentNumber  =  VBAK_VBELN),1) Dim_SalesOrderHeaderStatusid,
       convert(bigint, 1) as Dim_SalesOrderItemStatusid, --ifnull((select Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber  =  VBAK_VBELN and sois.SalesItemNumber  =  VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
       convert(bigint, 1) as Dim_CustomerGroup1id, --ifnull((select Dim_CustomerGroup1id from Dim_CustomerGroup1 cg1 where cg1.CustomerGroup  =  VBAK_KVGR1),1) Dim_CustomerGroup1id,
       convert(bigint, 1) as Dim_CustomerGroup2id, --ifnull((select Dim_CustomerGroup2id from Dim_CustomerGroup2 cg2 where cg2.CustomerGroup  =  VBAK_KVGR2),1) Dim_CustomerGroup2id,
	   ifnull((pl.Dim_SalesOrderItemCategoryid),1)  Dim_SalesOrderItemCategoryid,
       convert(bigint, 1) as Dim_ScheduleLineCategoryId, --ifnull((SELECT slc.Dim_ScheduleLineCategoryId FROM Dim_ScheduleLineCategory slc WHERE slc.ScheduleLineCategory  =  VBEP_ETTYP),1) Dim_ScheduleLineCategoryId,
       ifnull(vbep_lfrel,'Not Set') dd_ItemRelForDelv,
	   convert(bigint, 1) as Dim_ProfitCenterId,
       convert(bigint, 1) as Dim_DistributionChannelId, --ifnull((select dc.Dim_DistributionChannelid from dim_DistributionChannel dc where   dc.DistributionChannelCode  =  VBAK_VTWEG AND dc.RowIsCurrent  =  1), 1) Dim_DistributionChannelId,
       ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
       ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
       convert(bigint, 1) as Dim_DateidNextDate, --ifnull((SELECT nd.Dim_Dateid FROM Dim_Date nd WHERE nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidNextDate,
	   convert(bigint, 1) as Dim_RouteId, --ifnull((SELECT r.dim_routeid from dim_route r where r.RouteCode  =  VBAP_ROUTE and r.RowIsCurrent  =  1),1),
       convert(bigint, 1) as Dim_SalesRiskCategoryId, --ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER and src.RowIsCurrent  =  1),1),
       'Not Set' as dd_CreditRep, --ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditRep1,
       convert(numeric(18,4), 0) as dd_CreditLimit, --ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER  =  VBAK_KUNNR ),0) dd_CreditLimit,
       convert(bigint, 1) Dim_CustomerRiskCategoryId,
       convert(numeric(18,4), CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END) amt_UnitPriceUoM,
       convert(bigint, 1) Dim_Currencyid_TRA,
	   convert(bigint, 1) dim_Currencyid_GBL,
	   convert(bigint, 1) dim_currencyid_STAT,
	   convert(numeric(18,4), 1) amt_exchangerate_STAT
FROM max_holder_701,
     tmp_pop_fact_salesorder_tmptbl pl,
	 variable_holder_701;


UPDATE fact_salesorder_tmptbl tmp
SET tmp.amt_ExchangeRate = IFNULL(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' and pFromExchangeRate  =  0) z
         ON z.pFromCurrency  = VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  pl.Currency
WHERE tmp.amt_ExchangeRate <> IFNULL(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.amt_ExchangeRate_GBL = IFNULL(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN (SELECT x.*, pGlobalCurrency FROM tmp_pop_fact_salesorder_tmptbl x,variable_holder_701) pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' and pFromExchangeRate  =  0 ) z
         ON z.pFromCurrency  = VBAP_WAERK AND z.pDate = ifnull(pl.PRSDT,pl.vbak_audat) AND z.pToCurrency  =  pl.Currency AND z.pToCurrency  =  pGlobalCurrency
WHERE tmp.amt_ExchangeRate_GBL <> IFNULL(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidSalesOrderCreated = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidSalesOrderCreated <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidFirstDate = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidFirstDate <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidSchedDeliveryReq = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidSchedDeliveryReq <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidSchedDlvrReqPrev = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidSchedDlvrReqPrev <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidSchedDelivery = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_edatu AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidSchedDelivery <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidGoodsIssue = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_wadat AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidGoodsIssue <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidMtrlAvail = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_mbdat AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidMtrlAvail <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidLoading = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_lddat AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidLoading <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidTransport = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbep_tddat AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidTransport <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidGuaranteedate = IFNULL(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_date dd ON dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode AND dd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidGuaranteedate <> IFNULL(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_Currencyid = IFNULL(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  pl.currency
WHERE tmp.Dim_Currencyid <> IFNULL(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ProductHierarchyid = IFNULL(ph.Dim_ProductHierarchyid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_ProductHierarchy ph ON ph.ProductHierarchy  =  vbap_prodh
WHERE tmp.Dim_ProductHierarchyid <> IFNULL(ph.Dim_ProductHierarchyid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_StorageLocationid = IFNULL(sl.Dim_StorageLocationid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_StorageLocation sl ON sl.LocationCode  =  vbap_lgort and sl.plant  =  vbap_werks
WHERE tmp.Dim_StorageLocationid <> IFNULL(sl.Dim_StorageLocationid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesDivisionid = IFNULL(sd.Dim_SalesDivisionid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesDivision sd ON sd.DivisionCode  =  vbap_spart
WHERE tmp.Dim_SalesDivisionid <> IFNULL(sd.Dim_SalesDivisionid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ShipReceivePointid = IFNULL(srp.Dim_ShipReceivePointid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_ShipReceivePoint srp ON srp.ShipReceivePointCode  =  vbap_vstel
WHERE tmp.Dim_ShipReceivePointid <> IFNULL(srp.Dim_ShipReceivePointid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DocumentCategoryid = IFNULL(dc.Dim_DocumentCategoryid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_DocumentCategory dc ON dc.DocumentCategory  =  vbak_vbtyp
WHERE tmp.Dim_DocumentCategoryid <> IFNULL(dc.Dim_DocumentCategoryid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesDocumentTypeid = IFNULL(sdt.Dim_SalesDocumentTypeid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesDocumentType sdt ON sdt.DocumentType  =  vbak_auart
WHERE tmp.Dim_SalesDocumentTypeid <> IFNULL(sdt.Dim_SalesDocumentTypeid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesOrgid = IFNULL(so.Dim_SalesOrgid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesOrg so ON so.SalesOrgCode  =  vbak_vkorg
WHERE tmp.Dim_SalesOrgid <> IFNULL(so.Dim_SalesOrgid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_CustomerID = IFNULL(cust.Dim_CustomerID, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Customer cust ON cust.CustomerNumber  =  vbak_kunnr
WHERE tmp.Dim_CustomerID <> IFNULL(cust.Dim_CustomerID, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidValidFrom = IFNULL(vf.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Date vf ON vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode AND vf.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidValidFrom <> IFNULL(vf.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidValidTo = IFNULL(vt.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Date vt ON vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode AND vt.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidValidTo <> IFNULL(vt.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesGroupid = IFNULL(sg.Dim_SalesGroupid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesGroup sg ON sg.SalesGroupCode  =  vbak_vkgrp
WHERE tmp.Dim_SalesGroupid <> IFNULL(sg.Dim_SalesGroupid, 1);


/* OZ: Remove dim_costcenter update as that only brings null values that don't get updated

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_CostCenterid = IFNULL(cc.Dim_CostCenterid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT * FROM Dim_CostCenter_first1_701 WHERE  RowIsCurrent = 1) cc ON cc.Code  =  vbak_kostl and cc.ControllingArea  =  vbak_kokrs
WHERE tmp.Dim_CostCenterid <> IFNULL(cc.Dim_CostCenterid, 1)

OZ: Remove dim_costcenter update as that only brings null values that don't get updated */


UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ControllingAreaid = IFNULL(ca.Dim_ControllingAreaid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_ControllingArea ca ON ca.ControllingAreaCode  =  vbak_kokrs
WHERE tmp.Dim_ControllingAreaid <> IFNULL(ca.Dim_ControllingAreaid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_BillingBlockid = IFNULL(bb.Dim_BillingBlockid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_BillingBlock bb ON bb.BillingBlockCode  =  vbap_faksp
WHERE tmp.Dim_BillingBlockid <> IFNULL(bb.Dim_BillingBlockid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_TransactionGroupid = IFNULL(tg.Dim_TransactionGroupid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_TransactionGroup tg ON tg.TransactionGroup  =  vbak_trvog
WHERE tmp.Dim_TransactionGroupid <> IFNULL(tg.Dim_TransactionGroupid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesOrderRejectReasonid = IFNULL(sorr.Dim_SalesOrderRejectReasonid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesOrderRejectReason sorr ON sorr.RejectReasonCode  =  vbap_abgru
WHERE tmp.Dim_SalesOrderRejectReasonid <> IFNULL(sorr.Dim_SalesOrderRejectReasonid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_Partid = IFNULL(dp.dim_partid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN dim_part dp ON dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS
WHERE tmp.Dim_Partid <> IFNULL(dp.dim_partid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesOrderHeaderStatusid = IFNULL(sohs.Dim_SalesOrderHeaderStatusid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesOrderHeaderStatus sohs ON sohs.SalesDocumentNumber  =  VBAK_VBELN
WHERE tmp.Dim_SalesOrderHeaderStatusid <> IFNULL(sohs.Dim_SalesOrderHeaderStatusid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesOrderItemStatusid = IFNULL(sois.Dim_SalesOrderItemStatusid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_SalesOrderItemStatus sois ON sois.SalesDocumentNumber  =  VBAK_VBELN and sois.SalesItemNumber  =  VBAP_POSNR
WHERE tmp.Dim_SalesOrderItemStatusid <> IFNULL(sois.Dim_SalesOrderItemStatusid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_CustomerGroup1id = IFNULL(cg1.Dim_CustomerGroup1id, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_CustomerGroup1 cg1 ON cg1.CustomerGroup  =  VBAK_KVGR1
WHERE tmp.Dim_CustomerGroup1id <> IFNULL(cg1.Dim_CustomerGroup1id, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_CustomerGroup2id = IFNULL(cg2.Dim_CustomerGroup2id, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_CustomerGroup2 cg2 ON cg2.CustomerGroup  =  VBAK_KVGR2
WHERE tmp.Dim_CustomerGroup2id <> IFNULL(cg2.Dim_CustomerGroup2id, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ScheduleLineCategoryId = IFNULL(slc.Dim_ScheduleLineCategoryId, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_ScheduleLineCategory slc ON slc.ScheduleLineCategory  =  VBEP_ETTYP
WHERE tmp.Dim_ScheduleLineCategoryId <> IFNULL(slc.Dim_ScheduleLineCategoryId, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DistributionChannelId = IFNULL(dc.Dim_DistributionChannelid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT * FROM dim_DistributionChannel WHERE rowiscurrent = 1) dc ON dc.DistributionChannelCode  =  VBAK_VTWEG
WHERE tmp.Dim_DistributionChannelId <> IFNULL(dc.Dim_DistributionChannelid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_DateidNextDate = IFNULL(nd.dim_dateid, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN Dim_Date nd ON nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode AND nd.plantcode_factory=pl.plantcode
WHERE tmp.Dim_DateidNextDate <> IFNULL(nd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_RouteId = IFNULL(r.Dim_RouteId, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT * FROM dim_route WHERE rowiscurrent = 1) r ON r.RouteCode  =  VBAP_ROUTE
WHERE tmp.Dim_RouteId <> IFNULL(r.Dim_RouteId, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_SalesRiskCategoryId = IFNULL(src.Dim_SalesRiskCategoryId, 1)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN (SELECT * FROM Dim_SalesRiskCategory WHERE rowiscurrent = 1) src ON src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER
WHERE tmp.Dim_SalesRiskCategoryId <> IFNULL(src.Dim_SalesRiskCategoryId, 1);

UPDATE fact_salesorder_tmptbl tmp
SET tmp.dd_CreditRep = IFNULL(b.upd_dd_CreditRep1, 'Not Set')
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN TMP_UPD_dd_CreditRep1 b ON b.BUT050_PARTNER2  =  VBAK_KUNNR
WHERE tmp.dd_CreditRep <> IFNULL(b.upd_dd_CreditRep1, 'Not Set');



UPDATE fact_salesorder_tmptbl tmp
SET tmp.dd_CreditLimit = IFNULL(c.CREDIT_LIMIT, 0)
FROM fact_salesorder_tmptbl tmp
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON vbak_vbeln = dd_SalesDocNo AND vbap_posnr = dd_SalesItemNo AND vbep_etenr = dd_ScheduleNo
	 LEFT JOIN TMP_UPD_dd_CreditLimit c ON c.CUSTOMER  =  VBAK_KUNNR
WHERE tmp.dd_CreditLimit <> IFNULL(c.CREDIT_LIMIT, 0);

UPDATE fact_salesorder_tmptbl f
SET Dim_Currencyid_TRA = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON f.dd_salesdocno = pl.vbak_vbeln AND f.dd_salesitemno = pl.vbap_posnr AND f.dd_scheduleno= pl.vbep_etenr
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  VBAP_WAERK
WHERE f.Dim_Currencyid_TRA <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl f
SET dim_Currencyid_GBL = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN (SELECT x.*,pGlobalCurrency FROM tmp_pop_fact_salesorder_tmptbl x, variable_holder_701) pl ON f.dd_salesdocno = pl.vbak_vbeln AND f.dd_salesitemno = pl.vbap_posnr AND f.dd_scheduleno= pl.vbep_etenr
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  pGlobalCurrency
WHERE f.dim_Currencyid_GBL <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl f
SET dim_currencyid_STAT = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON f.dd_salesdocno = pl.vbak_vbeln AND f.dd_salesitemno = pl.vbap_posnr AND f.dd_scheduleno= pl.vbep_etenr
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  vbak_stwae
WHERE f.dim_currencyid_STAT <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl f
SET amt_exchangerate_STAT = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl pl ON f.dd_salesdocno = pl.vbak_vbeln AND f.dd_salesitemno = pl.vbap_posnr AND f.dd_scheduleno= pl.vbep_etenr
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' AND pFromExchangeRate = 0) z ON z.pFromCurrency = VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  vbak_stwae
WHERE amt_exchangerate_STAT <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl f
SET amt_StdCost  =  ifnull(convert(numeric(18,4), vbap_wavwr),0)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl vbk ON vbk.VBAK_VBELN  =  f.dd_SalesDocNo AND vbk.VBAP_POSNR  =  f.dd_SalesItemNo AND vbk.VBEP_ETENR  =  f.dd_ScheduleNo AND f.dd_ScheduleNo  =  vbk.v_SalesSchedNo
WHERE amt_StdCost  <>  ifnull(convert(numeric(18,4), vbap_wavwr),0);

UPDATE fact_salesorder_tmptbl f
SET amt_TargetValue  =  ifnull(convert(numeric(18,4), vbap_zwert),0)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl vbk ON vbk.VBAK_VBELN  =  f.dd_SalesDocNo AND vbk.VBAP_POSNR  =  f.dd_SalesItemNo AND vbk.VBEP_ETENR  =  f.dd_ScheduleNo AND f.dd_ScheduleNo  =  vbk.v_SalesSchedNo
WHERE amt_TargetValue  <>  ifnull(convert(numeric(18,4), vbap_zwert),0);

UPDATE fact_salesorder_tmptbl f
SET ct_TargetQty  =  ifnull(vbap_zmeng,0)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl vbk ON vbk.VBAK_VBELN  =  f.dd_SalesDocNo AND vbk.VBAP_POSNR  =  f.dd_SalesItemNo AND vbk.VBEP_ETENR  =  f.dd_ScheduleNo AND f.dd_ScheduleNo  =  vbk.v_SalesSchedNo
WHERE ct_TargetQty  <>  ifnull(vbap_zmeng,0);

UPDATE fact_salesorder_tmptbl f
SET f.ct_CumOrderQty  =  ifnull(vbk.VBAP_KWMENG, 0)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl vbk ON vbk.VBAK_VBELN  =  f.dd_SalesDocNo AND vbk.VBAP_POSNR  =  f.dd_SalesItemNo AND vbk.VBEP_ETENR  =  f.dd_ScheduleNo
WHERE f.ct_CumOrderQty <> ifnull(vbk.VBAP_KWMENG, 0);



/* Update this column separately to avoid the previous insert from failing */

UPDATE fact_salesorder_tmptbl f
SET Dim_CustomerRiskCategoryId  = ifnull(c.dim_salesriskcategoryid , 1)
FROM fact_salesorder_tmptbl f
     INNER JOIN tmp_pop_fact_salesorder_tmptbl ft ON ft.VBEP_ETENR  =  f.dd_ScheduleNo AND ft.VBAP_POSNR  =  f.dd_SalesItemNo AND ft.VBAK_VBELN  =  f.dd_SalesDocNo
	 LEFT JOIN TMP_UPD_Dim_CustomerRiskCategoryId c ON c.CUSTOMER  =  VBAK_KUNNR and c.CreditControlArea  =  VBAK_KKBER
WHERE f.Dim_CustomerRiskCategoryId <> ifnull(c.dim_salesriskcategoryid , 1);

UPDATE fact_salesorder_tmptbl fo
SET Dim_UnitOfMeasureId  =  uom.Dim_UnitOfMeasureId
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE VBAP_KMEIN IS NOT NULL) ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom ON uom.UOM  =  vbap_kmein
WHERE fo.Dim_UnitOfMeasureId <> uom.Dim_UnitOfMeasureId;

UPDATE fact_salesorder_tmptbl fo
SET Dim_UnitOfMeasureId  =  1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE VBAP_KMEIN IS NULL) ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
WHERE fo.Dim_UnitOfMeasureId <> 1;

UPDATE fact_salesorder_tmptbl fo
SET Dim_BaseUoMid  =   uom.Dim_UnitOfMeasureId
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_meins IS NOT NULL) ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom ON uom.UOM  =  vbap_meins
WHERE fo.Dim_BaseUoMid <> uom.Dim_UnitOfMeasureId;

UPDATE fact_salesorder_tmptbl fo
SET Dim_BaseUoMid  =   1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_meins IS NULL) ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
WHERE fo.Dim_BaseUoMid <> 1;

UPDATE fact_salesorder_tmptbl fo
SET Dim_SalesUoMid  =  uom.Dim_UnitOfMeasureId
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_vrkme IS NOT NULL) ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent = 1) uom ON uom.UOM  =  vbap_vrkme
WHERE fo.Dim_SalesUoMid <> uom.Dim_UnitOfMeasureId;

UPDATE fact_salesorder_tmptbl fo
SET Dim_SalesUoMid  =  1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_vrkme IS NULL) ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
WHERE fo.Dim_SalesUoMid <> 1;


DROP TABLE IF EXISTS dim_profitcenter_upd_701;

CREATE TABLE dim_profitcenter_upd_701
AS
SELECT max_holder_701.maxid+row_number() over(order by '') iid,
       VBAP_PRCTR,
	   VBAK_KOKRS,
	   VBAK_ERDAT,
	   convert(bigint, 1) dim_profitcenterid
FROM max_holder_701,
     VBAK_VBAP_VBEP
     INNER JOIN Dim_Plant pl ON pl.PlantCode  =  VBAP_WERKS
     INNER JOIN Dim_Company co ON co.CompanyCode  =  pl.CompanyCode
	 INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory  =  VBAP_PSTYV AND soic.RowIsCurrent  =  1
WHERE  NOT EXISTS (SELECT 1
                   FROM fact_salesorder_useinsub f
                   WHERE f.dd_SalesDocNo  =  VBAK_VBELN
				         AND f.dd_SalesItemNo  =  VBAP_POSNR
						 AND f.dd_ScheduleNo  =  VBEP_ETENR)
       AND EXISTS (SELECT 1
	               FROM dim_date mdt
				   WHERE mdt.DateValue  =  vbap_erdat
				         AND mdt.Dim_Dateid > 1);

DROP TABLE IF EXISTS fact_salesorder_useinsub;


DROP TABLE IF EXISTS TMP_minvalidto_sopc;
CREATE TABLE TMP_minvalidto_sopc
AS
SELECT pc.ProfitCenterCode,
       pc.ControllingArea,
	   VBAK_ERDAT,
	   MIN(pc.ValidTo) ValidTo
FROM dim_profitcenter_upd_701 u,
     dim_profitcenter pc
WHERE pc.ProfitCenterCode  =  VBAP_PRCTR
      AND pc.ControllingArea  =  VBAK_KOKRS
      AND pc.ValidTo >=  VBAK_ERDAT
      AND pc.RowIsCurrent  =  1
GROUP BY pc.ProfitCenterCode,
         pc.ControllingArea,
		 VBAK_ERDAT;

DROP TABLE IF EXISTS TMP_minvalidto_sopc2;
CREATE TABLE TMP_minvalidto_sopc2
AS
SELECT a.*,pc.dim_profitcenterid
FROM TMP_minvalidto_sopc a,
     dim_profitcenter pc
WHERE a.ProfitCenterCode = pc.ProfitCenterCode
      AND a.ControllingArea = pc.ControllingArea
      AND a.ValidTo = pc.ValidTo;


UPDATE dim_profitcenter_upd_701 u
SET dim_profitcenterid  = pc.dim_profitcenterid
FROM dim_profitcenter_upd_701 u
     INNER JOIN TMP_minvalidto_sopc2 pc ON pc.ProfitCenterCode  =  VBAP_PRCTR AND pc.ControllingArea = VBAK_KOKRS AND pc.VBAK_ERDAT = u.VBAK_ERDAT
WHERE u.dim_profitcenterid <> pc.dim_profitcenterid ;

UPDATE fact_salesorder_tmptbl tmp
SET tmp.Dim_ProfitCenterId  = ud.Dim_ProfitCenterId
FROM fact_salesorder_tmptbl tmp
     INNER JOIN dim_profitcenter_upd_701 ud ON tmp.fact_salesorderid  =  ud.iid
WHERE tmp.Dim_ProfitCenterId <> ud.Dim_ProfitCenterId;

DROP TABLE IF EXISTS dim_profitcenter_upd_701;

/*call vectorwise (combine 'fact_salesorder+fact_salesorder_tmptbl')*/

alter table fact_salesorder_tmptbl add constraint primary key (dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo);

INSERT INTO fact_salesorder
(
       fact_salesorderid,
       dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       ct_ScheduleQtySalesUnit,
       ct_ConfirmedQty,
       ct_CorrectedQty,
       amt_UnitPrice,
       ct_PriceUnit,
       amt_ScheduleTotal,
       amt_StdCost,
       amt_TargetValue,
       amt_Tax,
       ct_TargetQty,
       amt_ExchangeRate,
       amt_ExchangeRate_GBL,
       ct_OverDlvrTolerance,
       ct_UnderDlvrTolerance,
       Dim_DateidSalesOrderCreated,
       Dim_DateidFirstDate,
       Dim_DateidSchedDeliveryReq,
       Dim_DateidSchedDlvrReqPrev,
       Dim_DateidSchedDelivery,
       Dim_DateidGoodsIssue,
       Dim_DateidMtrlAvail,
       Dim_DateidLoading,
       Dim_DateidTransport,
       Dim_DateidGuaranteedate,
       Dim_Currencyid,
       Dim_ProductHierarchyid,
       Dim_Plantid,
       Dim_Companyid,
       Dim_StorageLocationid,
       Dim_SalesDivisionid,
       Dim_ShipReceivePointid,
       Dim_DocumentCategoryid,
       dim_SalesDocumentTypeid,
       Dim_SalesOrgid,
       Dim_CustomerID,
       Dim_DateidValidFrom,
       Dim_DateidValidTo,
       Dim_SalesGroupid,
       Dim_CostCenterid,
       Dim_ControllingAreaid,
       Dim_BillingBlockid,
       Dim_TransactionGroupid,
       Dim_SalesOrderRejectReasonid,
       Dim_Partid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       Dim_CustomerGroup1id,
       Dim_CustomerGroup2id,
       Dim_SalesOrderItemCategoryid,
       Dim_ScheduleLineCategoryId,
       dd_ItemRelForDelv,
       Dim_ProfitCenterId,
       Dim_DistributionChannelId,
       dd_BatchNo,
       dd_CreatedBy,
       Dim_DateidNextDate,
	   Dim_RouteId,
	   Dim_SalesRiskCategoryId,
       dd_CreditRep,
       dd_CreditLimit,
       Dim_CustomerRiskCategoryId,
	   amt_UnitPriceUoM,
	   dim_Currencyid_TRA,
	   dim_Currencyid_GBL,
	   dim_currencyid_STAT,
	   amt_exchangerate_STAT
)
SELECT fact_salesorderid,
       dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       ct_ScheduleQtySalesUnit,
       ct_ConfirmedQty,
       ct_CorrectedQty,
       amt_UnitPrice,
       ct_PriceUnit,
       amt_ScheduleTotal,
       amt_StdCost,
       amt_TargetValue,
       amt_Tax,
       ct_TargetQty,
       amt_ExchangeRate,
       amt_ExchangeRate_GBL,
       ct_OverDlvrTolerance,
       ct_UnderDlvrTolerance,
       Dim_DateidSalesOrderCreated,
       Dim_DateidFirstDate,
       Dim_DateidSchedDeliveryReq,
       Dim_DateidSchedDlvrReqPrev,
       Dim_DateidSchedDelivery,
       Dim_DateidGoodsIssue,
       Dim_DateidMtrlAvail,
       Dim_DateidLoading,
       Dim_DateidTransport,
       Dim_DateidGuaranteedate,
       Dim_Currencyid,
       Dim_ProductHierarchyid,
       Dim_Plantid,
       Dim_Companyid,
       Dim_StorageLocationid,
       Dim_SalesDivisionid,
       Dim_ShipReceivePointid,
       Dim_DocumentCategoryid,
       dim_SalesDocumentTypeid,
       Dim_SalesOrgid,
       Dim_CustomerID,
       Dim_DateidValidFrom,
       Dim_DateidValidTo,
       Dim_SalesGroupid,
       Dim_CostCenterid,
       Dim_ControllingAreaid,
       Dim_BillingBlockid,
       Dim_TransactionGroupid,
       Dim_SalesOrderRejectReasonid,
       Dim_Partid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       Dim_CustomerGroup1id,
       Dim_CustomerGroup2id,
       Dim_SalesOrderItemCategoryid,
       Dim_ScheduleLineCategoryId,
       dd_ItemRelForDelv,
       Dim_ProfitCenterId,
       Dim_DistributionChannelId,
       dd_BatchNo,
       dd_CreatedBy,
       Dim_DateidNextDate,
	   Dim_RouteId,
	   Dim_SalesRiskCategoryId,
       dd_CreditRep,
       dd_CreditLimit,
       Dim_CustomerRiskCategoryId,
	   amt_UnitPriceUoM,
	   dim_Currencyid_TRA,
	   dim_Currencyid_GBL,
	   dim_currencyid_STAT,
	   amt_exchangerate_STAT
FROM fact_salesorder_tmptbl t
WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder f WHERE f.dd_SalesDocNo = t.dd_SalesDocNo AND f.dd_SalesItemNo = t.dd_SalesItemNo AND f.dd_ScheduleNo = t.dd_ScheduleNo);


DROP TABLE IF EXISTS Dim_CostCenterid_2nd_701;
DROP TABLE IF EXISTS fact_salesorder_tmptbl;

CREATE TABLE Dim_CostCenterid_2nd_701
AS
SELECT Dim_CostCenterid,
       cc.Code,
	   cc.ControllingArea,
	   cc.RowIsCurrent,
	   row_number() over (partition by cc.Code,cc.ControllingArea,cc.RowIsCurrent order by cc.ValidTo DESC) tt
FROM Dim_CostCenter cc  ;

CREATE TABLE fact_salesorder_tmptbl
AS
SELECT vbap_kwmeng ct_ScheduleQtySalesUnit,
	   ifnull(VBAP_KWMENG,0) ct_CumOrderQty,
       vbap_kbmeng ct_ConfirmedQty,
       convert(numeric(18,4), 0) as ct_CorrectedQty,
       ifnull(convert(numeric(18,4), vbap_netpr), 0) amt_UnitPrice,
       vbap_kpein ct_PriceUnit,
       convert(numeric(18,4), vbap_netwr) amt_ScheduleTotal,
       ifnull(convert(numeric(18,4), vbap_wavwr),0) amt_StdCost,
       ifnull(convert(numeric(18,4), vbap_zwert),0) amt_TargetValue,
       ifnull(vbap_mwsbp, 0) amt_Tax,	/* Shanthi changes 12 Apr */
       vbap_zmeng ct_TargetQty,
       convert(numeric(18,4), 1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  co.Currency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate,
       convert(numeric(18,4), 1) as amt_ExchangeRate_GBL, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate_GBL,
       vbap_uebto ct_OverDlvrTolerance,
       vbap_untto ct_UnderDlvrTolerance,
       convert(bigint, 1) as Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint, 1) as Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
       convert(bigint, 1) as Dim_DateidSchedDeliveryReq, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
       convert(bigint, 1) as Dim_DateidSchedDelivery,
       convert(bigint, 1) as Dim_DateidGoodsIssue,
       convert(bigint, 1) as Dim_DateidMtrlAvail,
       convert(bigint, 1) as Dim_DateidLoading,
       convert(bigint, 1) as Dim_DateidGuaranteedate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGuaranteedate,
       convert(bigint, 1) as Dim_DateidTransport,
       convert(bigint, 1) as Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  co.currency ),1) Dim_Currencyid,
       convert(bigint, 1) as Dim_ProductHierarchyid, --ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy  =  vbap_prodh),1) Dim_ProductHierarchyid,
       pl.Dim_Plantid Dim_Plantid,
       co.Dim_Companyid  Dim_Companyid,
       convert(bigint, 1) as Dim_StorageLocationid, --ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode  =  vbap_lgort AND sl.plant  =  vbap_werks),1) Dim_StorageLocationid,
       convert(bigint, 1) as Dim_SalesDivisionid, --ifnull((SELECT Dim_SalesDivisionid FROM Dim_SalesDivision sd WHERE sd.DivisionCode  =  vbap_spart),1) Dim_SalesDivisionid,
       convert(bigint, 1) as Dim_ShipReceivePointid, --ifnull((SELECT Dim_ShipReceivePointid FROM Dim_ShipReceivePoint srp WHERE srp.ShipReceivePointCode  =  vbap_vstel),1) Dim_ShipReceivePointid,
       convert(bigint, 1) as Dim_DocumentCategoryid, --ifnull((SELECT Dim_DocumentCategoryid FROM Dim_DocumentCategory dc WHERE dc.DocumentCategory  =  vbak_vbtyp),1) Dim_DocumentCategoryid,
       convert(bigint, 1) as Dim_SalesDocumentTypeid, --ifnull((SELECT Dim_SalesDocumentTypeid FROM Dim_SalesDocumentType sdt WHERE sdt.DocumentType  =  vbak_auart),1) Dim_SalesDocumentTypeid,
       convert(bigint, 1) as Dim_SalesOrgid, --ifnull((SELECT Dim_SalesOrgid FROM Dim_SalesOrg so WHERE so.SalesOrgCode  =  vbak_vkorg),1) Dim_SalesOrgid,
       convert(bigint, 1) as Dim_CustomerID, --ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber  =  vbak_kunnr),1) Dim_CustomerID,
       convert(bigint, 1) as Dim_ScheduleLineCategoryId,
       convert(bigint, 1) as Dim_DateidValidFrom, --ifnull((SELECT Dim_Dateid FROM Dim_Date vf WHERE vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidFrom,
       convert(bigint, 1) as Dim_DateidValidTo, --ifnull((SELECT Dim_Dateid FROM Dim_Date vt WHERE vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidTo,
       convert(bigint, 1) as Dim_SalesGroupid, --ifnull((SELECT Dim_SalesGroupid FROM Dim_SalesGroup sg WHERE sg.SalesGroupCode  =  vbak_vkgrp),1) Dim_SalesGroupid ,
       convert(bigint, 1) as Dim_CostCenterid, --ifnull((SELECT Dim_CostCenterid FROM Dim_CostCenterid_2nd_701 cc WHERE cc.Code  =  vbak_kostl AND cc.ControllingArea  =  vbak_kokrs AND cc.RowIsCurrent  =  1 and tt = 1),1) Dim_CostCenterid,
       convert(bigint, 1) as Dim_ControllingAreaid, --ifnull((SELECT Dim_ControllingAreaid FROM Dim_ControllingArea ca WHERE ca.ControllingAreaCode  =  vbak_kokrs),1) Dim_ControllingAreaid,
       convert(bigint, 1) as Dim_BillingBlockid, --ifnull((SELECT Dim_BillingBlockid FROM Dim_BillingBlock bb WHERE bb.BillingBlockCode  =  vbap_faksp),1) Dim_BillingBlockid,
       convert(bigint, 1) as Dim_TransactionGroupid, --ifnull((SELECT Dim_TransactionGroupid FROM Dim_TransactionGroup tg WHERE tg.TransactionGroup  =  vbak_trvog),1) Dim_TransactionGroupid,
       convert(bigint, 1) as Dim_SalesOrderRejectReasonid, --ifnull((SELECT Dim_SalesOrderRejectReasonid FROM Dim_SalesOrderRejectReason sorr WHERE sorr.RejectReasonCode  =  vbap_abgru),1) Dim_SalesOrderRejectReasonid,
       convert(bigint, 1) as Dim_Partid, --ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS),1) Dim_Partid,
       convert(bigint, 1) as Dim_SalesOrderHeaderStatusid, --ifnull((SELECT Dim_SalesOrderHeaderStatusid FROM Dim_SalesOrderHeaderStatus sohs WHERE sohs.SalesDocumentNumber  =  VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
       convert(bigint, 1) as Dim_SalesOrderItemStatusid, --ifnull((SELECT sois.Dim_SalesOrderItemStatusid FROM Dim_SalesOrderItemStatus sois WHERE sois.SalesDocumentNumber  =  VBAP_VBELN AND sois.SalesItemNumber  =  VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
       convert(bigint, 1) as Dim_CustomerGroup1id, --ifnull((SELECT cg1.Dim_CustomerGroup1id FROM Dim_CustomerGroup1 cg1 WHERE cg1.CustomerGroup  =  VBAK_KVGR1),1) Dim_CustomerGroup1id,
       convert(bigint, 1) as Dim_CustomerGroup2id, --ifnull((SELECT cg2.Dim_CustomerGroup2id FROM Dim_CustomerGroup2 cg2 WHERE cg2.CustomerGroup  =  VBAK_KVGR2),1) Dim_CustomerGroup2id,
       ifnull((soic.Dim_SalesOrderItemCategoryid),1) Dim_salesorderitemcategoryid,
       'Not Set' dd_ItemRelForDelv,
       convert(bigint, 1) as Dim_ProfitCenterId,
       convert(bigint, 1) as Dim_DistributionChannelId, --ifnull((SELECT dc.Dim_DistributionChannelid FROM dim_DistributionChannel dc WHERE   dc.DistributionChannelCode  =  VBAK_VTWEG AND dc.RowIsCurrent  =  1), 1) Dim_DistributionChannelId,
	   convert(bigint, 1) as Dim_UnitOfMeasureId, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_kmein AND uom.RowIsCurrent  =  1), 1) Dim_UnitOfMeasureId,
	   convert(bigint, 1) as Dim_BaseUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_meins AND uom.RowIsCurrent  =  1), 1) Dim_BaseUoMid,
	   convert(bigint, 1) as Dim_SalesUoMid, --ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM  =  vbap_vrkme AND uom.RowIsCurrent  =  1), 1) Dim_SalesUoMid,
       ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
       ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
       convert(bigint, 1) as Dim_DateidNextDate, --ifnull((SELECT nd.Dim_Dateid FROM Dim_Date nd WHERE nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidNextDate,
       convert(bigint, 1) as Dim_RouteId, --ifnull((SELECT r.dim_routeid from dim_route r where r.RouteCode  =  VBAP_ROUTE and r.RowIsCurrent  =  1),1) Dim_RouteId,
	   dd_SalesDocNo,
	   dd_SalesItemNo,
	   dd_ScheduleNo,
	   convert(bigint, 1) as Dim_SalesRiskCategoryId, --ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER and src.RowIsCurrent  =  1),1) Dim_SalesRiskCategoryId,
	   'Not Set' as dd_CreditRep, --ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditRep,
	   convert(numeric(18,4), 0) as dd_CreditLimit, --ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER  =  VBAK_KUNNR ),0) dd_CreditLimit,
       convert(bigint, 1) as Dim_CustomerRiskCategoryId, --ifnull((select c.dim_salesriskcategoryid from TMP_UPD_Dim_CustomerRiskCategoryId c where c.CUSTOMER  =  VBAK_KUNNR and c.CreditControlArea  =  VBAK_KKBER),1) Dim_CustomerRiskCategoryId,
	   convert(numeric(18,4), CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END) amt_UnitPriceUoM,
       convert(bigint, 1) as Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid WHERE cur.CurrencyCode  =  VBAP_WAERK),1) Dim_Currencyid_TRA,
       convert(bigint, 1) as dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pGlobalCurrency),1) dim_Currencyid_GBL,
       convert(bigint, 1) as dim_currencyid_STAT, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  vbak_stwae),1) dim_currencyid_STAT,
       convert(numeric(18,4), 1) as amt_exchangerate_STAT, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate  =  0 AND z.pToCurrency  =  vbak_stwae),1) amt_exchangerate_STAT
	   VBAP_WAERK,
	   PRSDT,
	   vbak_audat,
	   pGlobalCurrency,
	   co.Currency as currency,
	   vbap_erdat,
	   pl.CompanyCode as companycode,
	   VBAP_STADAT,
	   vbak_vdatu,
	   vbak_gwldt,
	   vbap_prodh,
	   vbap_lgort,
	   vbap_werks,
	   vbap_spart,
	   vbap_vstel,
	   vbak_vbtyp,
	   vbak_auart,
	   vbak_vkorg,
	   vbak_kunnr,
	   vbak_guebg,
	   vbak_gueen,
	   vbak_vkgrp,
	   vbak_kostl,
	   vbak_kokrs,
	   vbap_faksp,
	   vbak_trvog,
	   vbap_abgru,
	   VBAP_MATNR,
	   VBAK_KVGR1,
	   VBAK_KVGR2,
	   VBAK_VTWEG,
	   vbap_kmein,
	   vbap_meins,
	   vbap_vrkme,
	   VBAK_CMNGV,
	   VBAP_ROUTE,
	   VBAK_CTLPC,
	   VBAK_KKBER,
	   vbak_stwae,
	   VBAK_ERDAT,
	   VBAP_PRCTR
FROM fact_salesorder so,
     VBAK_VBAP,
     dim_SalesOrderItemCategory soic,
     Dim_Plant pl,
     Dim_Company co,
	 variable_holder_701
WHERE pl.PlantCode = VBAP_WERKS
      AND co.CompanyCode = pl.CompanyCode
      AND VBAP_VBELN = dd_SalesDocNo
	  AND VBAP_POSNR = dd_SalesItemNo
	  AND soic.SalesOrderItemCategory = VBAP_PSTYV
	  AND soic.RowIsCurrent = 1
	  AND dd_ScheduleNo = 0;	/* Shanthi changes - sync on 12 Apr */


UPDATE fact_salesorder_tmptbl so
SET so.Dim_ProfitCenterId = ifnull(pc.dim_profitcenterid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_profitcenter WHERE RowIsCurrent = 1) pc
         ON pc.ProfitCenterCode  =  so.VBAP_PRCTR AND pc.ControllingArea  =  so.VBAK_KOKRS AND pc.ValidTo >=  so.VBAK_ERDAT
WHERE  so.Dim_ProfitCenterId <> ifnull(pc.dim_profitcenterid, 1);


UPDATE fact_salesorder_tmptbl so
SET so.amt_ExchangeRate = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' AND pFromExchangeRate = 0) z
	     ON z.pFromCurrency  = so.VBAP_WAERK AND z.pDate = ifnull(so.PRSDT,so.vbak_audat) AND z.pToCurrency = so.Currency
WHERE so.amt_ExchangeRate <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl so
SET so.amt_ExchangeRate_GBL = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE fact_script_name = 'bi_populate_salesorder_fact' AND pFromExchangeRate = 0) z
	     ON z.pFromCurrency  = so.VBAP_WAERK AND z.pToCurrency = so.pGlobalCurrency AND z.pDate = ifnull(so.PRSDT,so.vbak_audat)
WHERE so.amt_ExchangeRate_GBL <> ifnull(z.exchangeRate, 1);

merge into fact_salesorder_tmptbl so
using (select distinct dd_SalesDocNo,dd_SalesItemNo,dd.dim_dateid
FROM fact_salesorder_tmptbl so
     INNER JOIN dim_plant pl on pl.dim_plantid=so.dim_plantid
     LEFT JOIN dim_date dd ON dd.DateValue = so.vbap_erdat AND dd.CompanyCode = so.CompanyCode and dd.plantcode_factory=pl.plantcode
WHERE so.Dim_DateidSalesOrderCreated <> ifnull(dd.dim_dateid, 1)) t
on t.dd_SalesDocNo=so.dd_SalesDocNo and t.dd_SalesItemNo=so.dd_SalesItemNo
when matched then update set so.Dim_DateidSalesOrderCreated = ifnull(t.dim_dateid, 1);

merge into fact_salesorder_tmptbl so
using (select distinct dd_SalesDocNo,dd_SalesItemNo,dd.dim_dateid
FROM fact_salesorder_tmptbl so
     INNER JOIN dim_plant pl on pl.dim_plantid=so.dim_plantid
     LEFT JOIN dim_date dd ON dd.DateValue = so.VBAP_STADAT AND dd.CompanyCode = so.CompanyCode and dd.plantcode_factory=pl.plantcode
WHERE so.Dim_DateidFirstDate <> ifnull(dd.dim_dateid, 1)) t
on t.dd_SalesDocNo=so.dd_SalesDocNo and t.dd_SalesItemNo=so.dd_SalesItemNo
when matched then update set so.Dim_DateidFirstDate = ifnull(t.dim_dateid, 1);


merge into fact_salesorder_tmptbl so
using (select distinct dd_SalesDocNo,dd_SalesItemNo,dd.dim_dateid
FROM fact_salesorder_tmptbl so
INNER JOIN dim_plant pl on pl.dim_plantid=so.dim_plantid
     LEFT JOIN dim_date dd ON dd.DateValue = so.vbak_vdatu AND dd.CompanyCode = so.CompanyCode and dd.plantcode_factory=pl.plantcode
WHERE so.Dim_DateidSchedDeliveryReq <> ifnull(dd.dim_dateid, 1)) t
on t.dd_SalesDocNo=so.dd_SalesDocNo and t.dd_SalesItemNo=so.dd_SalesItemNo
when matched then update set so.Dim_DateidSchedDeliveryReq = ifnull(t.dim_dateid, 1);

merge into fact_salesorder_tmptbl so
using (select distinct dd_SalesDocNo,dd_SalesItemNo,dd.dim_dateid
FROM fact_salesorder_tmptbl so
INNER JOIN dim_plant pl on pl.dim_plantid=so.dim_plantid
     LEFT JOIN dim_date dd ON dd.DateValue = so.vbak_gwldt AND dd.CompanyCode = so.CompanyCode and dd.plantcode_factory=pl.plantcode
WHERE so.Dim_DateidGuaranteedate <> ifnull(dd.dim_dateid, 1)) t
on t.dd_SalesDocNo=so.dd_SalesDocNo and t.dd_SalesItemNo=so.dd_SalesItemNo
when matched then update set so.Dim_DateidGuaranteedate = ifnull(t.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_Currencyid = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  so.currency
WHERE so.Dim_Currencyid <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_ProductHierarchy ph ON ph.ProductHierarchy  =  vbap_prodh
WHERE so.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_StorageLocation sl ON sl.LocationCode  =  vbap_lgort AND sl.plant  =  vbap_werks
WHERE so.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesDivisionid = ifnull(sd.Dim_SalesDivisionid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesDivision sd ON sd.DivisionCode  =  vbap_spart
WHERE so.Dim_SalesDivisionid <> ifnull(sd.Dim_SalesDivisionid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_ShipReceivePointid = ifnull(srp.Dim_ShipReceivePointid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_ShipReceivePoint srp ON srp.ShipReceivePointCode  =  vbap_vstel
WHERE so.Dim_ShipReceivePointid <> ifnull(srp.Dim_ShipReceivePointid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DocumentCategoryid = ifnull(dc.Dim_DocumentCategoryid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_DocumentCategory dc ON dc.DocumentCategory  =  vbak_vbtyp
WHERE so.Dim_DocumentCategoryid <> ifnull(dc.Dim_DocumentCategoryid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesDocumentTypeid = ifnull(sdt.Dim_SalesDocumentTypeid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesDocumentType sdt ON sdt.DocumentType  =  vbak_auart
WHERE so.Dim_SalesDocumentTypeid <> ifnull(sdt.Dim_SalesDocumentTypeid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesOrgid = ifnull(sorg.Dim_SalesOrgid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesOrg sorg ON sorg.SalesOrgCode  =  vbak_vkorg
WHERE so.Dim_SalesOrgid <> ifnull(sorg.Dim_SalesOrgid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CustomerID = ifnull(cust.Dim_CustomerID, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Customer cust ON cust.CustomerNumber  =  vbak_kunnr
WHERE so.Dim_CustomerID <> ifnull(cust.Dim_CustomerID, 1);

merge into fact_salesorder_tmptbl so
using (select distinct dd_SalesDocNo,dd_SalesItemNo,vf.dim_dateid
FROM fact_salesorder_tmptbl so
INNER JOIN dim_plant pl on pl.dim_plantid=so.dim_plantid
     LEFT JOIN Dim_Date vf ON vf.DateValue  =  vbak_guebg AND vf.CompanyCode  = so.CompanyCode and vf.plantcode_factory=pl.plantcode
WHERE so.Dim_DateidValidFrom <> ifnull(vf.Dim_Dateid, 1)) t
on t.dd_SalesDocNo=so.dd_SalesDocNo and t.dd_SalesItemNo=so.dd_SalesItemNo
when matched then update set so.Dim_DateidValidFrom = ifnull(t.dim_dateid, 1);

merge into fact_salesorder_tmptbl so
using (select distinct dd_SalesDocNo,dd_SalesItemNo,vt.dim_dateid
FROM fact_salesorder_tmptbl so
INNER JOIN dim_plant pl on pl.dim_plantid=so.dim_plantid
     LEFT JOIN Dim_Date vt ON vt.DateValue  =  vbak_gueen AND vt.CompanyCode  = so.CompanyCode and vt.plantcode_factory=pl.plantcode
WHERE so.Dim_DateidValidTo <> ifnull(vt.Dim_Dateid, 1)) t
on t.dd_SalesDocNo=so.dd_SalesDocNo and t.dd_SalesItemNo=so.dd_SalesItemNo
when matched then update set so.Dim_DateidValidTo = ifnull(t.dim_dateid, 1);


UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesGroupid = ifnull(sg.Dim_SalesGroupid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesGroup sg ON sg.SalesGroupCode  =  vbak_vkgrp
WHERE so.Dim_SalesGroupid <> ifnull(sg.Dim_SalesGroupid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CostCenterid = ifnull(cc.Dim_CostCenterid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM Dim_CostCenterid_2nd_701 WHERE RowIsCurrent = 1 and tt = 1)cc
	     ON cc.Code  =  vbak_kostl AND cc.ControllingArea  =  vbak_kokrs
WHERE so.Dim_CostCenterid <> ifnull(cc.Dim_CostCenterid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_ControllingAreaid = ifnull(ca.Dim_ControllingAreaid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_ControllingArea ca ON ca.ControllingAreaCode  =  vbak_kokrs
WHERE so.Dim_ControllingAreaid <> ifnull(ca.Dim_ControllingAreaid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_BillingBlockid = ifnull(bb.Dim_BillingBlockid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_BillingBlock bb ON bb.BillingBlockCode  =  vbap_faksp
WHERE so.Dim_BillingBlockid <> ifnull(bb.Dim_BillingBlockid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_TransactionGroupid = ifnull(tg.Dim_TransactionGroupid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_TransactionGroup tg ON tg.TransactionGroup  =  vbak_trvog
WHERE so.Dim_TransactionGroupid <> ifnull(tg.Dim_TransactionGroupid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesOrderRejectReasonid = ifnull(sorr.Dim_SalesOrderRejectReasonid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesOrderRejectReason sorr ON sorr.RejectReasonCode  =  vbap_abgru
WHERE so.Dim_SalesOrderRejectReasonid <> ifnull(sorr.Dim_SalesOrderRejectReasonid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_Partid = ifnull(dp.dim_partid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN dim_part dp ON dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS
WHERE so.Dim_Partid <> ifnull(dp.dim_partid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesOrderHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesOrderHeaderStatus sohs ON sohs.SalesDocumentNumber  = dd_salesdocno
WHERE so.Dim_SalesOrderHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesOrderItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_SalesOrderItemStatus sois ON sois.SalesDocumentNumber  =  dd_salesdocno AND sois.SalesItemNumber  =  dd_salesitemno
WHERE so.Dim_SalesOrderItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CustomerGroup1id = ifnull(cg1.Dim_CustomerGroup1id, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_CustomerGroup1 cg1 ON cg1.CustomerGroup  =  VBAK_KVGR1
WHERE so.Dim_CustomerGroup1id <> ifnull(cg1.Dim_CustomerGroup1id, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CustomerGroup2id = ifnull(cg2.Dim_CustomerGroup2id, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_CustomerGroup2 cg2 ON cg2.CustomerGroup  =  VBAK_KVGR2
WHERE so.Dim_CustomerGroup2id <> ifnull(cg2.Dim_CustomerGroup2id, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_DistributionChannel WHERE RowIsCurrent  =  1) dc
	     ON dc.DistributionChannelCode  =  VBAK_VTWEG
WHERE so.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_UnitOfMeasureId = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE rowiscurrent = 1) uom
	     ON uom.UOM  =  vbap_kmein
WHERE so.Dim_UnitOfMeasureId <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_BaseUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE rowiscurrent = 1) uom
	     ON uom.UOM  =  vbap_meins
WHERE so.Dim_BaseUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_unitofmeasure WHERE rowiscurrent = 1) uom
	     ON uom.UOM  =  vbap_vrkme
WHERE so.Dim_SalesUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

merge into fact_salesorder_tmptbl so
using (select distinct dd_SalesDocNo,dd_SalesItemNo,nd.dim_dateid
FROM fact_salesorder_tmptbl so
inner join dim_plant pl on pl.dim_plantid=so.dim_plantid
     LEFT JOIN Dim_Date nd ON nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = so.CompanyCode and nd.plantcode_factory=pl.plantcode
WHERE so.Dim_DateidNextDate <> ifnull(nd.Dim_Dateid, 1)) t
on t.dd_SalesDocNo=so.dd_SalesDocNo and t.dd_SalesItemNo=so.dd_SalesItemNo
when matched then update set so.Dim_DateidNextDate = ifnull(t.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_RouteId = ifnull(r.dim_routeid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM dim_route WHERE rowiscurrent = 1) r
	     ON r.RouteCode  =  VBAP_ROUTE
WHERE so.Dim_RouteId <> ifnull(r.dim_routeid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_SalesRiskCategoryId = ifnull(src.Dim_SalesRiskCategoryId, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT * FROM Dim_SalesRiskCategory WHERE rowiscurrent = 1) src
	     ON src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea = VBAK_KKBER
WHERE so.Dim_SalesRiskCategoryId <> ifnull(src.Dim_SalesRiskCategoryId, 1);

UPDATE fact_salesorder_tmptbl so
SET so.dd_CreditRep = ifnull(b.upd_dd_CreditRep1, 'Not Set')
FROM fact_salesorder_tmptbl so
     LEFT JOIN TMP_UPD_dd_CreditRep1 b ON b.BUT050_PARTNER2 = VBAK_KUNNR
WHERE so.dd_CreditRep <> ifnull(b.upd_dd_CreditRep1, 'Not Set');

UPDATE fact_salesorder_tmptbl so
SET so.dd_CreditLimit = ifnull(c.CREDIT_LIMIT, 0)
FROM fact_salesorder_tmptbl so
     LEFT JOIN TMP_UPD_dd_CreditLimit c ON c.CUSTOMER = VBAK_KUNNR
WHERE so.dd_CreditLimit <> ifnull(c.CREDIT_LIMIT, 0);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_CustomerRiskCategoryId = ifnull(c.dim_salesriskcategoryid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN TMP_UPD_Dim_CustomerRiskCategoryId c ON c.CUSTOMER = VBAK_KUNNR and c.CreditControlArea = VBAK_KKBER
WHERE so.Dim_CustomerRiskCategoryId <> ifnull(c.dim_salesriskcategoryid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.Dim_Currencyid_TRA = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  VBAP_WAERK
WHERE so.Dim_Currencyid_TRA <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.dim_Currencyid_GBL = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  pGlobalCurrency
WHERE so.dim_Currencyid_GBL <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.dim_currencyid_STAT = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  vbak_stwae
WHERE so.dim_currencyid_STAT <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl so
SET so.amt_exchangerate_STAT = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl so
     LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE pFromExchangeRate  =  0 AND fact_script_name  =  'bi_populate_salesorder_fact') z
	     ON z.pFromCurrency   =  VBAP_WAERK AND z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  vbak_stwae
WHERE so.amt_exchangerate_STAT <> ifnull(z.exchangeRate, 1);

/* Madalina 21 Oct 2016 - Merge all the following updates from fact_salesorder_tmptbl into one single statement -BI-4486*/
merge into fact_salesorder so using
 ( select sot.dd_SalesDocNo,
		sot.dd_SalesItemNo,
		sot.dd_ScheduleNo,
		sot.ct_ScheduleQtySalesUnit,
		sot.ct_ConfirmedQty,
		sot.ct_CorrectedQty,
		sot.amt_UnitPrice,
		sot.amt_UnitPriceUoM,
		sot.ct_PriceUnit,
		sot.amt_ScheduleTotal,
		sot.amt_StdCost,
		sot.amt_TargetValue,
		sot.amt_Tax,
		sot.ct_TargetQty,
		sot.amt_ExchangeRate,
		sot.amt_ExchangeRate_GBL,
		sot.ct_OverDlvrTolerance,
		sot.ct_UnderDlvrTolerance,
		sot.Dim_DateidSalesOrderCreated,
		sot.Dim_DateidFirstDate,
		sot.Dim_DateidSchedDeliveryReq,
		sot.Dim_DateidSchedDelivery,
		sot.Dim_DateidGoodsIssue,
		sot.Dim_DateidMtrlAvail,
		sot.Dim_DateidLoading,
		sot.Dim_DateidGuaranteedate,
		sot.Dim_DateidTransport,
		sot.Dim_Currencyid,
		sot.Dim_ProductHierarchyid,
		sot.Dim_Plantid,
		sot.Dim_Companyid,
		sot.Dim_StorageLocationid,
		sot.Dim_SalesDivisionid,
		sot.Dim_ShipReceivePointid,
		sot.Dim_DocumentCategoryid,
		sot.Dim_SalesDocumentTypeid,
		sot.Dim_SalesOrgid,
		sot.Dim_CustomerID,
		sot.Dim_ScheduleLineCategoryId,
		sot.Dim_DateidValidFrom,
		sot.Dim_DateidValidTo,
		sot.Dim_SalesGroupid,
		sot.Dim_CostCenterid,
		sot.Dim_ControllingAreaid,
		sot.Dim_BillingBlockid,
		sot.Dim_TransactionGroupid,
		sot.Dim_SalesOrderRejectReasonid,
		sot.Dim_Partid,
		sot.Dim_SalesOrderHeaderStatusid,
		sot.Dim_SalesOrderItemStatusid,
		sot.Dim_CustomerGroup1id,
		sot.Dim_CustomerGroup2id,
		sot.Dim_salesorderitemcategoryid,
		sot.dd_ItemRelForDelv,
		sot.Dim_ProfitCenterId,
		sot.Dim_DistributionChannelId,
		sot.Dim_UnitOfMeasureId,
		sot.Dim_BaseUoMid,
		sot.Dim_SalesUoMid,
		sot.dd_BatchNo,
		sot.dd_CreatedBy,
		sot.Dim_DateidNextDate,
		sot.Dim_RouteId,
		sot.Dim_SalesRiskCategoryId,
		sot.dd_CreditRep,
		sot.dd_CreditLimit,
		sot.Dim_CustomerRiskCategoryId,
		sot.ct_CumOrderQty,
		sot.dim_Currencyid_TRA,
		sot.dim_Currencyid_GBL,
		sot.dim_currencyid_STAT,
		sot.amt_exchangerate_STAT	
	from fact_salesorder_tmptbl sot ) sot
on so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
when matched then update
set		
	so.ct_ScheduleQtySalesUnit =  sot.ct_ScheduleQtySalesUnit,
	so.ct_ConfirmedQty =  sot.ct_ConfirmedQty,
	so.ct_CorrectedQty =  sot.ct_CorrectedQty,
	so.amt_UnitPrice =  sot.amt_UnitPrice,
	so.amt_UnitPriceUoM =  sot.amt_UnitPriceUoM,
	so.ct_PriceUnit =  sot.ct_PriceUnit,
	so.amt_ScheduleTotal =  sot.amt_ScheduleTotal,
	so.amt_StdCost =  sot.amt_StdCost,
	so.amt_TargetValue =  sot.amt_TargetValue,
	so.amt_Tax =  sot.amt_Tax,
	so.ct_TargetQty =  sot.ct_TargetQty,
	so.amt_ExchangeRate =  sot.amt_ExchangeRate,
	so.amt_ExchangeRate_GBL =  sot.amt_ExchangeRate_GBL,
	so.ct_OverDlvrTolerance =  sot.ct_OverDlvrTolerance,
	so.ct_UnderDlvrTolerance =  sot.ct_UnderDlvrTolerance,
	so.Dim_DateidSalesOrderCreated =  sot.Dim_DateidSalesOrderCreated,
	so.Dim_DateidFirstDate =  sot.Dim_DateidFirstDate,
	so.Dim_DateidSchedDeliveryReq =  sot.Dim_DateidSchedDeliveryReq,
	so.Dim_DateidSchedDelivery =  sot.Dim_DateidSchedDelivery,
	so.Dim_DateidGoodsIssue =  sot.Dim_DateidGoodsIssue,
	so.Dim_DateidMtrlAvail =  sot.Dim_DateidMtrlAvail,
	so.Dim_DateidLoading =  sot.Dim_DateidLoading,
	so.Dim_DateidGuaranteedate =  sot.Dim_DateidGuaranteedate,
	so.Dim_DateidTransport =  sot.Dim_DateidTransport,
	so.Dim_Currencyid =  sot.Dim_Currencyid,
	so.Dim_ProductHierarchyid =  sot.Dim_ProductHierarchyid,
	so.Dim_Plantid =  sot.Dim_Plantid,
	so.Dim_Companyid =  sot.Dim_Companyid,
	so.Dim_StorageLocationid =  sot.Dim_StorageLocationid,
	so.Dim_SalesDivisionid =  sot.Dim_SalesDivisionid,
	so.Dim_ShipReceivePointid =  sot.Dim_ShipReceivePointid,
	so.Dim_DocumentCategoryid =  sot.Dim_DocumentCategoryid,
	so.Dim_SalesDocumentTypeid =  sot.Dim_SalesDocumentTypeid,
	so.Dim_SalesOrgid =  sot.Dim_SalesOrgid,
	so.Dim_CustomerID =  sot.Dim_CustomerID,
	so.Dim_ScheduleLineCategoryId =  sot.Dim_ScheduleLineCategoryId,
	so.Dim_DateidValidFrom =  sot.Dim_DateidValidFrom,
	so.Dim_DateidValidTo =  sot.Dim_DateidValidTo,
	so.Dim_SalesGroupid =  sot.Dim_SalesGroupid,
	so.Dim_CostCenterid =  sot.Dim_CostCenterid,
	so.Dim_ControllingAreaid =  sot.Dim_ControllingAreaid,
	so.Dim_BillingBlockid =  sot.Dim_BillingBlockid,
	so.Dim_TransactionGroupid =  sot.Dim_TransactionGroupid,
	so.Dim_SalesOrderRejectReasonid =  sot.Dim_SalesOrderRejectReasonid,
	so.Dim_Partid =  sot.Dim_Partid,
	so.Dim_SalesOrderHeaderStatusid =  sot.Dim_SalesOrderHeaderStatusid,
	so.Dim_SalesOrderItemStatusid =  sot.Dim_SalesOrderItemStatusid,
	so.Dim_CustomerGroup1id =  sot.Dim_CustomerGroup1id,
	so.Dim_CustomerGroup2id =  sot.Dim_CustomerGroup2id,
	so.Dim_salesorderitemcategoryid =  sot.Dim_salesorderitemcategoryid,
	so.dd_ItemRelForDelv =  sot.dd_ItemRelForDelv,
	so.Dim_ProfitCenterId =  sot.Dim_ProfitCenterId,
	so.Dim_DistributionChannelId =  sot.Dim_DistributionChannelId,
	so.Dim_UnitOfMeasureId =  sot.Dim_UnitOfMeasureId,
	so.Dim_BaseUoMid =  sot.Dim_BaseUoMid,
	so.Dim_SalesUoMid =  sot.Dim_SalesUoMid,
	so.dd_BatchNo =  sot.dd_BatchNo,
	so.dd_CreatedBy =  sot.dd_CreatedBy,
	so.Dim_DateidNextDate =  sot.Dim_DateidNextDate,
	so.Dim_RouteId =  sot.Dim_RouteId,
	so.Dim_SalesRiskCategoryId =  sot.Dim_SalesRiskCategoryId,
	so.dd_CreditRep =  sot.dd_CreditRep,
	so.dd_CreditLimit =  sot.dd_CreditLimit,
	so.Dim_CustomerRiskCategoryId =  sot.Dim_CustomerRiskCategoryId,
	so.ct_CumOrderQty =  sot.ct_CumOrderQty,
	so.dim_Currencyid_TRA =  sot.dim_Currencyid_TRA,
	so.dim_Currencyid_GBL =  sot.dim_Currencyid_GBL,
	so.dim_currencyid_STAT =  sot.dim_currencyid_STAT,
	so.amt_exchangerate_STAT =  sot.amt_exchangerate_STAT;


/*
Update fact_salesorder so

SET
  so.ct_ScheduleQtySalesUnit    =  sot.ct_ScheduleQtySalesUnit
 From fact_salesorder_tmptbl sot ,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND   so.ct_ScheduleQtySalesUnit    <>  sot.ct_ScheduleQtySalesUnit          

Update fact_salesorder so
SET so.ct_ConfirmedQty        =  sot.ct_ConfirmedQty

From fact_salesorder_tmptbl sot, fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.ct_ConfirmedQty        <>  sot.ct_ConfirmedQty

Update fact_salesorder so
SET so.ct_CorrectedQty        =  sot.ct_CorrectedQty
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.ct_CorrectedQty        <>  sot.ct_CorrectedQty

Update fact_salesorder so
SET
 so.amt_UnitPrice      =  sot.amt_UnitPrice
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND  so.amt_UnitPrice      <>  sot.amt_UnitPrice

Update fact_salesorder so
SET so.amt_UnitPriceUoM       =  sot.amt_UnitPriceUoM
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.amt_UnitPriceUoM       <>  sot.amt_UnitPriceUoM

Update fact_salesorder so
SET so.ct_PriceUnit   =  sot.ct_PriceUnit
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.ct_PriceUnit   <>  sot.ct_PriceUnit

Update fact_salesorder so
SET so.amt_ScheduleTotal      =  sot.amt_ScheduleTotal
From fact_salesorder_tmptbl sot, fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.amt_ScheduleTotal      <>  sot.amt_ScheduleTotal


Update fact_salesorder so
SET
 so.amt_StdCost        =  sot.amt_StdCost
From fact_salesorder_tmptbl sot, fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND  so.amt_StdCost        <>  sot.amt_StdCost

Update fact_salesorder so
SET so.amt_TargetValue        =  sot.amt_TargetValue
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.amt_TargetValue        <>  sot.amt_TargetValue

Update fact_salesorder so
SET so.amt_Tax        =  sot.amt_Tax
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.amt_Tax        <>  sot.amt_Tax

Update fact_salesorder so
SET
 so.ct_TargetQty       =  sot.ct_TargetQty
 From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND  so.ct_TargetQty       <>  sot.ct_TargetQty

Update fact_salesorder so
SET so.amt_ExchangeRate       =  sot.amt_ExchangeRate
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.amt_ExchangeRate       <>  sot.amt_ExchangeRate


Update fact_salesorder so
SET so.amt_ExchangeRate_GBL   =  sot.amt_ExchangeRate_GBL
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.amt_ExchangeRate_GBL   <>  sot.amt_ExchangeRate_GBL

Update fact_salesorder so
SET
 so.ct_OverDlvrTolerance       =  sot.ct_OverDlvrTolerance
 From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND  so.ct_OverDlvrTolerance       <>  sot.ct_OverDlvrTolerance

Update fact_salesorder so
SET so.ct_UnderDlvrTolerance          =  sot.ct_UnderDlvrTolerance
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.ct_UnderDlvrTolerance          <>  sot.ct_UnderDlvrTolerance

Update fact_salesorder so
SET so.Dim_DateidSalesOrderCreated    =  sot.Dim_DateidSalesOrderCreated
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidSalesOrderCreated    <>  sot.Dim_DateidSalesOrderCreated


Update fact_salesorder so
SET
 so.Dim_DateidFirstDate        =  sot.Dim_DateidFirstDate
 From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND  so.Dim_DateidFirstDate        <>  sot.Dim_DateidFirstDate 

Update fact_salesorder so
SET so.Dim_DateidSchedDeliveryReq     =  sot.Dim_DateidSchedDeliveryReq
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidSchedDeliveryReq     <>  sot.Dim_DateidSchedDeliveryReq

Update fact_salesorder so
SET so.Dim_DateidSchedDelivery        =  sot.Dim_DateidSchedDelivery
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidSchedDelivery        <>  sot.Dim_DateidSchedDelivery

Update fact_salesorder so
SET so.Dim_DateidGoodsIssue       =  sot.Dim_DateidGoodsIssue
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidGoodsIssue       <>  sot.Dim_DateidGoodsIssue 

Update fact_salesorder so
SET so.Dim_DateidMtrlAvail    =  sot.Dim_DateidMtrlAvail
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidMtrlAvail    <>  sot.Dim_DateidMtrlAvail 

Update fact_salesorder so
SET so.Dim_DateidLoading      =  sot.Dim_DateidLoading
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidLoading      <>  sot.Dim_DateidLoading

Update fact_salesorder so
SET so.Dim_DateidGuaranteedate    =  sot.Dim_DateidGuaranteedate
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidGuaranteedate    <>  sot.Dim_DateidGuaranteedate

Update fact_salesorder so
SET so.Dim_DateidTransport    =  sot.Dim_DateidTransport
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidTransport    <>  sot.Dim_DateidTransport 

Update fact_salesorder so
SET so.Dim_Currencyid         =  sot.Dim_Currencyid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_Currencyid         <>  sot.Dim_Currencyid



Update fact_salesorder so
SET so.Dim_ProductHierarchyid     =  sot.Dim_ProductHierarchyid
 From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND  so.Dim_ProductHierarchyid     <>  sot.Dim_ProductHierarchyid

Update fact_salesorder so
SET so.Dim_Plantid    =  sot.Dim_Plantid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_Plantid    <>  sot.Dim_Plantid

Update fact_salesorder so
SET so.Dim_Companyid          =  sot.Dim_Companyid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_Companyid          <>  sot.Dim_Companyid

Update fact_salesorder so
SET so.Dim_StorageLocationid      =  sot.Dim_StorageLocationid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_StorageLocationid      <>  sot.Dim_StorageLocationid

Update fact_salesorder so
SET so.Dim_SalesDivisionid    =  sot.Dim_SalesDivisionid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_SalesDivisionid    <>  sot.Dim_SalesDivisionid

Update fact_salesorder so
SET so.Dim_ShipReceivePointid         =  sot.Dim_ShipReceivePointid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_ShipReceivePointid         <>  sot.Dim_ShipReceivePointid

Update fact_salesorder so
SET so.Dim_DocumentCategoryid     =  sot.Dim_DocumentCategoryid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DocumentCategoryid     <>  sot.Dim_DocumentCategoryid

Update fact_salesorder so
SET so.Dim_SalesDocumentTypeid        =  sot.Dim_SalesDocumentTypeid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_SalesDocumentTypeid        <>  sot.Dim_SalesDocumentTypeid 

Update fact_salesorder so
SET so.Dim_SalesOrgid         =  sot.Dim_SalesOrgid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_SalesOrgid         <>  sot.Dim_SalesOrgid

Update fact_salesorder so
SET so.Dim_CustomerID     =  sot.Dim_CustomerID
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_CustomerID     <>  sot.Dim_CustomerID 

Update fact_salesorder so
SET so.Dim_ScheduleLineCategoryId     =  sot.Dim_ScheduleLineCategoryId
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_ScheduleLineCategoryId     <>  sot.Dim_ScheduleLineCategoryId 

Update fact_salesorder so
SET so.Dim_DateidValidFrom    =  sot.Dim_DateidValidFrom
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidValidFrom    <>  sot.Dim_DateidValidFrom

Update fact_salesorder so
SET so.Dim_DateidValidTo          =  sot.Dim_DateidValidTo
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidValidTo          <>  sot.Dim_DateidValidTo

Update fact_salesorder so
SET so.Dim_SalesGroupid       =  sot.Dim_SalesGroupid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_SalesGroupid       <>  sot.Dim_SalesGroupid 

Update fact_salesorder so
SET so.Dim_CostCenterid       =  sot.Dim_CostCenterid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_CostCenterid       <>  sot.Dim_CostCenterid


Update fact_salesorder so
SET so.Dim_ControllingAreaid      =  sot.Dim_ControllingAreaid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_ControllingAreaid      <>  sot.Dim_ControllingAreaid 

Update fact_salesorder so
SET so.Dim_BillingBlockid     =  sot.Dim_BillingBlockid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_BillingBlockid     <>  sot.Dim_BillingBlockid 

Update fact_salesorder so
SET so.Dim_TransactionGroupid         =  sot.Dim_TransactionGroupid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_TransactionGroupid         <>  sot.Dim_TransactionGroupid

Update fact_salesorder so
SET so.Dim_SalesOrderRejectReasonid       =  sot.Dim_SalesOrderRejectReasonid
 From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND  so.Dim_SalesOrderRejectReasonid       <>  sot.Dim_SalesOrderRejectReasonid 

Update fact_salesorder so
SET so.Dim_Partid     =  sot.Dim_Partid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_Partid     <>  sot.Dim_Partid 

Update fact_salesorder so
SET so.Dim_SalesOrderHeaderStatusid           =  sot.Dim_SalesOrderHeaderStatusid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_SalesOrderHeaderStatusid           <>  sot.Dim_SalesOrderHeaderStatusid

Update fact_salesorder so
SET so.Dim_SalesOrderItemStatusid         =  sot.Dim_SalesOrderItemStatusid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_SalesOrderItemStatusid         <>  sot.Dim_SalesOrderItemStatusid 

Update fact_salesorder so
SET so.Dim_CustomerGroup1id           =  sot.Dim_CustomerGroup1id
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_CustomerGroup1id           <>  sot.Dim_CustomerGroup1id 

Update fact_salesorder so
SET so.Dim_CustomerGroup2id           =  sot.Dim_CustomerGroup2id
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_CustomerGroup2id           <>  sot.Dim_CustomerGroup2id

Update fact_salesorder so
SET so.Dim_salesorderitemcategoryid       =  sot.Dim_salesorderitemcategoryid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_salesorderitemcategoryid       <>  sot.Dim_salesorderitemcategoryid 

Update fact_salesorder so
SET so.dd_ItemRelForDelv      =  sot.dd_ItemRelForDelv
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.dd_ItemRelForDelv      <>  sot.dd_ItemRelForDelv 

Update fact_salesorder so
SET so.Dim_ProfitCenterId     =  sot.Dim_ProfitCenterId
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_ProfitCenterId     <>  sot.Dim_ProfitCenterId

Update fact_salesorder so
SET so.Dim_DistributionChannelId          =  sot.Dim_DistributionChannelId
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DistributionChannelId          <>  sot.Dim_DistributionChannelId 

Update fact_salesorder so
SET so.Dim_UnitOfMeasureId  =  sot.Dim_UnitOfMeasureId
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_UnitOfMeasureId  <>  sot.Dim_UnitOfMeasureId 

Update fact_salesorder so
SET so.Dim_BaseUoMid  =  sot.Dim_BaseUoMid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_BaseUoMid  <>  sot.Dim_BaseUoMid

Update fact_salesorder so
SET so.Dim_SalesUoMid  =  sot.Dim_SalesUoMid
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_SalesUoMid  <>  sot.Dim_SalesUoMid

Update fact_salesorder so
SET so.dd_BatchNo  =  sot.dd_BatchNo
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.dd_BatchNo  <>  sot.dd_BatchNo

Update fact_salesorder so
SET so.dd_CreatedBy       =  sot.dd_CreatedBy
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.dd_CreatedBy       <>  sot.dd_CreatedBy 

Update fact_salesorder so
SET so.Dim_DateidNextDate     =  sot.Dim_DateidNextDate
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_DateidNextDate     <>  sot.Dim_DateidNextDate 

Update fact_salesorder so
SET so.Dim_RouteId    =  sot.Dim_RouteId
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_RouteId    <>  sot.Dim_RouteId
*/

/* Shanthi changes - 12 Apr sync part X starts	*/

/*
Update fact_salesorder so
SET so.Dim_SalesRiskCategoryId    =  sot.Dim_SalesRiskCategoryId
 From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND  so.Dim_SalesRiskCategoryId    <>  sot.Dim_SalesRiskCategoryId 

Update fact_salesorder so
SET so.dd_CreditRep           =  sot.dd_CreditRep
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.dd_CreditRep           <>  sot.dd_CreditRep 
Update fact_salesorder so
SET so.dd_CreditLimit         =  sot.dd_CreditLimit
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.dd_CreditLimit         <>  sot.dd_CreditLimit 

Update fact_salesorder so
SET so.Dim_CustomerRiskCategoryId  =  sot.Dim_CustomerRiskCategoryId
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo = sot.dd_SalesDocNo and so.dd_SalesItemNo = sot.dd_SalesItemNo and so.dd_ScheduleNo = sot.dd_ScheduleNo
AND so.Dim_CustomerRiskCategoryId  <>  sot.Dim_CustomerRiskCategoryId


Update fact_salesorder so
Set so.ct_CumOrderQty  = sot.ct_CumOrderQty
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo
AND so.ct_CumOrderQty <> sot.ct_CumOrderQty
*/

/* Shanthi changes - 12 Apr sync part X ends	*/

/* LK: 8 Sep : 4 new columns */

/*
Update fact_salesorder so
Set so.dim_Currencyid_TRA   = sot.dim_Currencyid_TRA
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo
AND so.dim_Currencyid_TRA <> sot.dim_Currencyid_TRA

Update fact_salesorder so
Set so.dim_Currencyid_GBL   = sot.dim_Currencyid_GBL
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo
AND so.dim_Currencyid_GBL <> sot.dim_Currencyid_GBL

Update fact_salesorder so
Set so.dim_currencyid_STAT   = sot.dim_currencyid_STAT
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo
AND so.dim_currencyid_STAT <> sot.dim_currencyid_STAT

Update fact_salesorder so
Set so.amt_exchangerate_STAT   = sot.amt_exchangerate_STAT
From fact_salesorder_tmptbl sot,fact_salesorder so
where so.dd_SalesDocNo=sot.dd_SalesDocNo and so.dd_SalesItemNo=sot.dd_SalesItemNo and so.dd_ScheduleNo=sot.dd_ScheduleNo
AND so.amt_exchangerate_STAT <> sot.amt_exchangerate_STAT
*/

/* LK - 8 Sep - End of 4 new columns */

/* END 21 Oct 2016 */

DROP TABLE IF EXISTS max_holder_701;
CREATE TABLE max_holder_701
AS
SELECT IFNULL(max(fact_salesorderid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0)) AS maxid
FROM fact_salesorder;


drop table if exists fact_salesorder_tmptbl;
create table fact_salesorder_tmptbl  like fact_salesorder INCLUDING DEFAULTS INCLUDING IDENTITY;
drop table if exists fact_salesorder_useinsub;

create table fact_salesorder_useinsub as Select dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
from fact_salesorder;


 INSERT
    INTO fact_salesorder_tmptbl(
 fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
            Dim_routeid,
	    Dim_SalesRiskCategoryId,
            dd_CreditRep,
            dd_CreditLimit,
            Dim_CustomerRiskCategoryId,
	        amt_UnitPriceUoM,
		    dim_Currencyid_TRA,
		    dim_Currencyid_GBL,
		    dim_currencyid_STAT,
		    amt_exchangerate_STAT,
		    ct_CumOrderQty
)
SELECT max_holder_701.maxid + row_number() over(order by ''),
       vbap_vbeln AS dd_SalesDocNo,
       vbap_posnr AS dd_SalesItemNo,
       0 AS dd_ScheduleNo,
       vbap_kwmeng AS ct_ScheduleQtySalesUnit,
       vbap_kbmeng AS ct_ConfirmedQty,
       convert(numeric(18,4), 0) AS ct_CorrectedQty,
       ifnull(convert(numeric(18,4), vbap_netpr),0) AS amt_UnitPrice,
       vbap_kpein AS ct_PriceUnit,
       convert(numeric(18,4), vbap_netwr) AS amt_ScheduleTotal,
       ifnull(convert(numeric(18,4), vbap_wavwr),0) AS amt_StdCost,
       ifnull(convert(numeric(18,4), vbap_zwert),0) AS amt_TargetValue,
       ifnull(vbap_mwsbp ,0) AS amt_Tax,
       vbap_zmeng AS ct_TargetQty,
       convert(numeric(18,4), 1) as amt_ExchangeRate, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  co.Currency and z.pFromExchangeRate  =  0 ),1) amt_ExchangeRate,
	   convert(numeric(18,4), 1) as amt_ExchangeRate_GBL, --ifnull( ( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency  =  pGlobalCurrency and z.pFromExchangeRate  =  0 ),1)  amt_ExchangeRate_GBL ,
       vbap_uebto AS ct_OverDlvrTolerance,
       vbap_untto AS ct_UnderDlvrTolerance,
       convert(bigint, 1) AS Dim_DateidSalesOrderCreated, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbap_erdat AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
       convert(bigint, 1) AS Dim_DateidFirstDate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  VBAP_STADAT AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidFirstDate,
       convert(bigint, 1) AS Dim_DateidSchedDeliveryReq, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
       convert(bigint, 1) AS Dim_DateidSchedDlvrReqPrev, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_vdatu AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
       convert(bigint, 1) AS Dim_DateidSchedDelivery,
       convert(bigint, 1) AS Dim_DateidGoodsIssue,
       convert(bigint, 1) AS Dim_DateidMtrlAvail,
       convert(bigint, 1) AS Dim_DateidLoading,
       convert(bigint, 1) AS Dim_DateidTransport,
       convert(bigint, 1) AS Dim_DateidGuaranteedate, --ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue  =  vbak_gwldt AND dd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidGuaranteedate,
       convert(bigint, 1) AS Dim_Currencyid, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  co.currency ),1) Dim_Currencyid,
       convert(bigint, 1) AS Dim_ProductHierarchyid, --ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy  =  vbap_prodh),1) Dim_ProductHierarchyid,
       pl.Dim_Plantid,
       co.Dim_Companyid,
       convert(bigint, 1) AS Dim_StorageLocationid, --ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode  =  vbap_lgort AND sl.plant  =  vbap_werks),1) Dim_StorageLocationid,
       convert(bigint, 1) AS Dim_SalesDivisionid, --ifnull((SELECT Dim_SalesDivisionid FROM Dim_SalesDivision sd WHERE sd.DivisionCode  =  vbap_spart),1) Dim_SalesDivisionid,
       convert(bigint, 1) AS Dim_ShipReceivePointid, --ifnull((SELECT Dim_ShipReceivePointid FROM Dim_ShipReceivePoint srp WHERE srp.ShipReceivePointCode  =  vbap_vstel),1) Dim_ShipReceivePointid,
       convert(bigint, 1) AS Dim_DocumentCategoryid, --ifnull((SELECT Dim_DocumentCategoryid FROM Dim_DocumentCategory dc WHERE dc.DocumentCategory  =  vbak_vbtyp),1) Dim_DocumentCategoryid,
       convert(bigint, 1) AS Dim_SalesDocumentTypeid, --ifnull((SELECT Dim_SalesDocumentTypeid FROM Dim_SalesDocumentType sdt WHERE sdt.DocumentType  =  vbak_auart),1) Dim_SalesDocumentTypeid,
       convert(bigint, 1) AS Dim_SalesOrgid, --ifnull((SELECT Dim_SalesOrgid FROM Dim_SalesOrg so WHERE so.SalesOrgCode  =  vbak_vkorg),1) Dim_SalesOrgid,
       convert(bigint, 1) AS Dim_CustomerID, --ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber  =  vbak_kunnr),1) Dim_CustomerID,
       convert(bigint, 1) AS Dim_DateidValidFrom, --ifnull((SELECT Dim_Dateid FROM Dim_Date vf WHERE vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidFrom,
       convert(bigint, 1) AS Dim_DateidValidTo, --ifnull((SELECT Dim_Dateid FROM Dim_Date vt WHERE vt.DateValue  =  vbak_gueen AND vt.CompanyCode  =  pl.CompanyCode),1) Dim_DateidValidTo,
       convert(bigint, 1) AS Dim_SalesGroupid, --ifnull((SELECT Dim_SalesGroupid FROM Dim_SalesGroup sg WHERE sg.SalesGroupCode  =  vbak_vkgrp),1) Dim_SalesGroupid,
	   convert(bigint, 1) AS Dim_CostCenterid,
       convert(bigint, 1) AS Dim_ControllingAreaid, --ifnull((SELECT Dim_ControllingAreaid FROM Dim_ControllingArea ca WHERE ca.ControllingAreaCode  =  vbak_kokrs),1) Dim_ControllingAreaid,
       convert(bigint, 1) AS Dim_BillingBlockid, --ifnull((SELECT Dim_BillingBlockid FROM Dim_BillingBlock bb WHERE bb.BillingBlockCode  =  vbap_faksp),1) Dim_BillingBlockid,
       convert(bigint, 1) AS Dim_TransactionGroupid, --ifnull((SELECT Dim_TransactionGroupid FROM Dim_TransactionGroup tg WHERE tg.TransactionGroup  =  vbak_trvog),1) Dim_TransactionGroupid,
       convert(bigint, 1) AS Dim_SalesOrderRejectReasonid, --ifnull((SELECT Dim_SalesOrderRejectReasonid FROM Dim_SalesOrderRejectReason sorr WHERE sorr.RejectReasonCode  =  vbap_abgru),1) Dim_SalesOrderRejectReasonid,
       convert(bigint, 1) AS Dim_Partid, --ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS),1) Dim_Partid,
       convert(bigint, 1) AS Dim_SalesOrderHeaderStatusid, --ifnull((SELECT Dim_SalesOrderHeaderStatusid FROM Dim_SalesOrderHeaderStatus sohs WHERE sohs.SalesDocumentNumber  =  VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
       convert(bigint, 1) AS Dim_SalesOrderItemStatusid, --ifnull((SELECT Dim_SalesOrderItemStatusid FROM Dim_SalesOrderItemStatus sois WHERE sois.SalesDocumentNumber  =  VBAP_VBELN AND sois.SalesItemNumber  =  VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
       convert(bigint, 1) AS Dim_CustomerGroup1id, --ifnull((SELECT Dim_CustomerGroup1id FROM Dim_CustomerGroup1 cg1 WHERE cg1.CustomerGroup  =  VBAK_KVGR1),1) Dim_CustomerGroup1id,
       convert(bigint, 1) AS Dim_CustomerGroup2id, --ifnull((SELECT Dim_CustomerGroup2id FROM Dim_CustomerGroup2 cg2 WHERE cg2.CustomerGroup  =  VBAK_KVGR2),1) Dim_CustomerGroup2id,
       ifnull((soic.Dim_SalesOrderItemCategoryid),1) AS Dim_SalesOrderItemCategoryid,
       convert(bigint, 1) AS Dim_ScheduleLineCategoryId,
       'Not Set' AS dd_ItemRelForDelv,
	   convert(bigint, 1) AS Dim_ProfitCenterId,
       convert(bigint, 1) AS Dim_DistributionChannelId, --ifnull((SELECT dc.Dim_DistributionChannelid FROM dim_DistributionChannel dc WHERE   dc.DistributionChannelCode  =  VBAK_VTWEG AND dc.RowIsCurrent  =  1), 1) Dim_DistributionChannelId,
       ifnull(VBAP_CHARG,'Not Set') AS dd_BatchNo,
       ifnull(VBAK_ERNAM,'Not Set') AS dd_CreatedBy,
       convert(bigint, 1) AS Dim_DateidNextDate, --ifnull((SELECT nd.Dim_Dateid FROM Dim_Date nd WHERE nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  pl.CompanyCode),1) Dim_DateidNextDate,
       convert(bigint, 1) AS Dim_routeid, --ifnull((SELECT r.dim_routeid from dim_route r where r.RouteCode  =  VBAP_ROUTE and r.RowIsCurrent  =  1),1),
	   convert(bigint, 1) AS Dim_SalesRiskCategoryId, --ifnull((SELECT src.Dim_SalesRiskCategoryId from Dim_SalesRiskCategory src where src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER and src.RowIsCurrent  =  1),1),
       'Not Set' AS dd_CreditRep, --ifnull((select upd_dd_CreditRep1 FROM TMP_UPD_dd_CreditRep1 b WHERE  b.BUT050_PARTNER2  =  VBAK_KUNNR ),'Not Set') dd_CreditRep,
 
       convert(numeric(18,4), 0) AS dd_CreditLimit, --ifnull((select CREDIT_LIMIT FROM TMP_UPD_dd_CreditLimit c WHERE c.CUSTOMER  =  VBAK_KUNNR ),0) dd_CreditLimit,
       convert(bigint, 1) AS Dim_CustomerRiskCategoryId, --ifnull((select c.dim_salesriskcategoryid from TMP_UPD_Dim_CustomerRiskCategoryId c where c.CUSTOMER  =  VBAK_KUNNR and c.CreditControlArea  =  VBAK_KKBER),1) Dim_CustomerRiskCategoryId,
	   convert(numeric(18,4), CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END) amt_UnitPriceUoM,
       convert(bigint, 1) AS Dim_Currencyid_TRA, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  VBAP_WAERK),1) Dim_Currencyid_TRA,
       convert(bigint, 1) AS dim_Currencyid_GBL, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  pGlobalCurrency),1) dim_Currencyid_GBL,
       convert(bigint, 1) AS dim_currencyid_STAT, --ifnull((SELECT Dim_Currencyid FROM Dim_Currency cur WHERE cur.CurrencyCode  =  vbak_stwae),1) dim_currencyid_STAT,
       convert(numeric(18,4), 1) as amt_exchangerate_STAT, --ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency   =  VBAP_WAERK and z.fact_script_name  =  'bi_populate_salesorder_fact' and z.pDate  =  ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate  =  0 AND z.pToCurrency  =  vbak_stwae),1) amt_exchangerate_STAT,
	   ifnull(VBAP_KWMENG, 0) ct_CumOrderQty
FROM VBAK_VBAP
/* Madalina 30 Jun 2016 - add ifnull for VBAP_WERKS and VBAP_PSTYV from join conditions, to avoid losing data - BI-2952 */
     INNER JOIN Dim_Plant pl ON pl.PlantCode  =  ifnull(VBAP_WERKS, 'Not Set')
     INNER JOIN Dim_Company co ON co.CompanyCode  =  pl.CompanyCode
     INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory  =  ifnull(VBAP_PSTYV, 'Not Set') AND soic.RowIsCurrent  =  1
	 CROSS JOIN variable_holder_701
	 CROSS JOIN max_holder_701
WHERE NOT EXISTS (SELECT 1
                  FROM fact_salesorder_useinsub f
                  WHERE f.dd_SalesDocNo  =  VBAP_VBELN
				        AND f.dd_SalesItemNo  =  VBAP_POSNR
						AND f.dd_ScheduleNo  =  0)
      AND EXISTS (SELECT 1
	              FROM dim_date mdt
				  WHERE mdt.DateValue = vbap_erdat
				        AND mdt.Dim_Dateid > 1)
	  AND NOT EXISTS (SELECT 1
	                  FROM vbak_vbap_vbep v
					  WHERE vbak_vbap.VBAP_VBELN = v.VBAK_VBELN
                            AND vbak_vbap.VBAP_POSNR  =  v.VBAP_POSNR);

UPDATE fact_salesorder_tmptbl fo
SET fo.amt_ExchangeRate = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        co.Currency as currency
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
                      INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE pFromExchangeRate  =  0 AND fact_script_name  =  'bi_populate_salesorder_fact') z
	     ON z.pFromCurrency  = VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency  = v.Currency
WHERE fo.amt_ExchangeRate <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.amt_ExchangeRate_GBL = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,pGlobalCurrency FROM VBAK_VBAP x, variable_holder_701) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE pFromExchangeRate  =  0 AND fact_script_name  =  'bi_populate_salesorder_fact') z
	     ON z.pToCurrency  =  pGlobalCurrency AND z.pFromCurrency   =  VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat)
WHERE fo.amt_ExchangeRate_GBL <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidSalesOrderCreated = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = vbap_erdat AND dd.CompanyCode = v.CompanyCode and dd.plantcode_factory=v.VBAP_WERKS
WHERE fo.Dim_DateidSalesOrderCreated <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidFirstDate = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = VBAP_STADAT AND dd.CompanyCode = v.CompanyCode and dd.plantcode_factory=v.VBAP_WERKS 
WHERE fo.Dim_DateidFirstDate <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidSchedDeliveryReq = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = vbak_vdatu AND dd.CompanyCode = v.CompanyCode and dd.plantcode_factory=v.VBAP_WERKS 
WHERE fo.Dim_DateidSchedDeliveryReq <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidSchedDlvrReqPrev = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = vbak_vdatu AND dd.CompanyCode = v.CompanyCode and dd.plantcode_factory=v.VBAP_WERKS 
WHERE fo.Dim_DateidSchedDlvrReqPrev <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidGuaranteedate = ifnull(dd.dim_dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date dd ON dd.DateValue = vbak_gwldt AND dd.CompanyCode = v.CompanyCode and dd.plantcode_factory=v.VBAP_WERKS 
WHERE fo.Dim_DateidGuaranteedate <> ifnull(dd.dim_dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_Currencyid = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        co.Currency as currency
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
                      INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  v.currency
WHERE fo.Dim_Currencyid <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_ProductHierarchy ph ON ph.ProductHierarchy  =  vbap_prodh
WHERE fo.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_StorageLocation sl ON sl.LocationCode  =  vbap_lgort AND sl.plant  =  vbap_werks
WHERE fo.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesDivisionid = ifnull(sd.Dim_SalesDivisionid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesDivision sd ON sd.DivisionCode  =  vbap_spart
WHERE fo.Dim_SalesDivisionid <> ifnull(sd.Dim_SalesDivisionid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_ShipReceivePointid = ifnull(srp.Dim_ShipReceivePointid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_ShipReceivePoint srp ON srp.ShipReceivePointCode  =  vbap_vstel
WHERE fo.Dim_ShipReceivePointid <> ifnull(srp.Dim_ShipReceivePointid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DocumentCategoryid = ifnull(dc.Dim_DocumentCategoryid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_DocumentCategory dc ON dc.DocumentCategory  =  vbak_vbtyp
WHERE fo.Dim_DocumentCategoryid <> ifnull(dc.Dim_DocumentCategoryid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesDocumentTypeid = ifnull(sdt.Dim_SalesDocumentTypeid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesDocumentType sdt ON sdt.DocumentType  =  vbak_auart
WHERE fo.Dim_SalesDocumentTypeid <> ifnull(sdt.Dim_SalesDocumentTypeid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesOrgid = ifnull(so.Dim_SalesOrgid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesOrg so ON so.SalesOrgCode  =  vbak_vkorg
WHERE fo.Dim_SalesOrgid <> ifnull(so.Dim_SalesOrgid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CustomerID = ifnull(cust.Dim_CustomerID, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Customer cust ON cust.CustomerNumber  =  vbak_kunnr
WHERE fo.Dim_CustomerID <> ifnull(cust.Dim_CustomerID, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidValidFrom = ifnull(vf.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = ifnull(VBAP_WERKS,'Not Set')) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date vf ON vf.DateValue  =  vbak_guebg AND vf.CompanyCode  =  v.CompanyCode and vf.plantcode_factory=v.VBAP_WERKS 
WHERE fo.Dim_DateidValidFrom <> ifnull(vf.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidValidTo = ifnull(vt.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = ifnull(VBAP_WERKS,'Not Set')) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date vt ON vt.DateValue  =  vbak_gueen AND vt.CompanyCode  = v.CompanyCode and vt.plantcode_factory=v.VBAP_WERKS 
WHERE fo.Dim_DateidValidTo <> ifnull(vt.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesGroupid = ifnull(sg.Dim_SalesGroupid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesGroup sg ON sg.SalesGroupCode  =  vbak_vkgrp
WHERE fo.Dim_SalesGroupid <> ifnull(sg.Dim_SalesGroupid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_ControllingAreaid = ifnull(ca.Dim_ControllingAreaid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_ControllingArea ca ON ca.ControllingAreaCode  =  vbak_kokrs
WHERE fo.Dim_ControllingAreaid <> ifnull(ca.Dim_ControllingAreaid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_BillingBlockid = ifnull(bb.Dim_BillingBlockid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_BillingBlock bb ON bb.BillingBlockCode  =  vbap_faksp
WHERE fo.Dim_BillingBlockid <> ifnull(bb.Dim_BillingBlockid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_TransactionGroupid = ifnull(tg.Dim_TransactionGroupid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_TransactionGroup tg ON tg.TransactionGroup  =  vbak_trvog
WHERE fo.Dim_TransactionGroupid <> ifnull(tg.Dim_TransactionGroupid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesOrderRejectReasonid = ifnull(sorr.Dim_SalesOrderRejectReasonid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesOrderRejectReason sorr ON sorr.RejectReasonCode  =  vbap_abgru
WHERE fo.Dim_SalesOrderRejectReasonid <> ifnull(sorr.Dim_SalesOrderRejectReasonid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_Partid = ifnull(dp.dim_partid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN dim_part dp ON dp.PartNumber  =  VBAP_MATNR AND dp.Plant  =  VBAP_WERKS
WHERE fo.Dim_Partid <> ifnull(dp.dim_partid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesOrderHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesOrderHeaderStatus sohs ON sohs.SalesDocumentNumber  =  VBAP_VBELN
WHERE fo.Dim_SalesOrderHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesOrderItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_SalesOrderItemStatus sois ON sois.SalesDocumentNumber  =  VBAP_VBELN AND sois.SalesItemNumber  =  VBAP_POSNR
WHERE fo.Dim_SalesOrderItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CustomerGroup1id = ifnull(cg1.Dim_CustomerGroup1id, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_CustomerGroup1 cg1 ON cg1.CustomerGroup  =  VBAK_KVGR1
WHERE fo.Dim_CustomerGroup1id <> ifnull(cg1.Dim_CustomerGroup1id, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CustomerGroup2id = ifnull(cg2.Dim_CustomerGroup2id, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_CustomerGroup2 cg2 ON cg2.CustomerGroup  =  VBAK_KVGR2
WHERE fo.Dim_CustomerGroup2id <> ifnull(cg2.Dim_CustomerGroup2id, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM dim_DistributionChannel WHERE RowIsCurrent  =  1) dc
	     ON dc.DistributionChannelCode  =  VBAK_VTWEG
WHERE fo.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_DateidNextDate = ifnull(nd.Dim_Dateid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,
                        pl.CompanyCode as CompanyCode
                 FROM VBAK_VBAP x
                      INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Date nd ON nd.DateValue  =  VBAK_CMNGV AND nd.CompanyCode  =  v.CompanyCode and nd.plantcode_factory=v.VBAP_WERKS 
WHERE fo.Dim_DateidNextDate <> ifnull(nd.Dim_Dateid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_routeid = ifnull(r.dim_routeid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM dim_route WHERE rowiscurrent = 1) r
	     ON r.RouteCode  =  VBAP_ROUTE
WHERE fo.Dim_routeid <> ifnull(r.dim_routeid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesRiskCategoryId = ifnull(src.Dim_SalesRiskCategoryId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_SalesRiskCategory WHERE rowiscurrent = 1) src
	     ON src.SalesRiskCategory  =  VBAK_CTLPC and src.CreditControlArea  =  VBAK_KKBER
WHERE fo.Dim_SalesRiskCategoryId <> ifnull(src.Dim_SalesRiskCategoryId, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.dd_CreditRep = ifnull(b.upd_dd_CreditRep1, 'Not Set')
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN TMP_UPD_dd_CreditRep1 b ON b.BUT050_PARTNER2  =  VBAK_KUNNR
WHERE fo.dd_CreditRep <> ifnull(b.upd_dd_CreditRep1, 'Not Set');



UPDATE fact_salesorder_tmptbl fo
SET fo.dd_CreditLimit = ifnull(c.CREDIT_LIMIT, 0)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN TMP_UPD_dd_CreditLimit c ON c.CUSTOMER  =  VBAK_KUNNR
WHERE fo.dd_CreditLimit <> ifnull(c.CREDIT_LIMIT, 0);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CustomerRiskCategoryId = ifnull(c.dim_salesriskcategoryid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN TMP_UPD_Dim_CustomerRiskCategoryId c ON c.CUSTOMER  =  VBAK_KUNNR and c.CreditControlArea  =  VBAK_KKBER
WHERE fo.Dim_CustomerRiskCategoryId <> ifnull(c.dim_salesriskcategoryid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_Currencyid_TRA = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  VBAP_WAERK
WHERE fo.Dim_Currencyid_TRA <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.dim_Currencyid_GBL = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT x.*,pGlobalCurrency FROM VBAK_VBAP x, variable_holder_701) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  pGlobalCurrency
WHERE fo.dim_Currencyid_GBL <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.dim_currencyid_STAT = ifnull(cur.Dim_Currencyid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN Dim_Currency cur ON cur.CurrencyCode  =  vbak_stwae
WHERE fo.dim_currencyid_STAT <> ifnull(cur.Dim_Currencyid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.amt_exchangerate_STAT = ifnull(z.exchangeRate, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT distinct * FROM tmp_getExchangeRate1 WHERE pFromExchangeRate  =  0 AND fact_script_name  =  'bi_populate_salesorder_fact') z
	     ON z.pFromCurrency = VBAP_WAERK AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency  =  vbak_stwae
WHERE fo.amt_exchangerate_STAT <> ifnull(z.exchangeRate, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_CostCenterid = ifnull(cc.Dim_CostCenterid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_CostCenter WHERE RowIsCurrent = 1 and current_date <= VALIDTO) cc
	     ON cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs
WHERE fo.Dim_CostCenterid <> ifnull(cc.Dim_CostCenterid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_ProfitCenterId  =  ifnull(pc.dim_profitcenterid, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN VBAK_VBAP v ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM dim_profitcenter WHERE RowIsCurrent  =  1) pc
         ON pc.ProfitCenterCode  =  VBAP_PRCTR AND pc.ControllingArea  =  VBAK_KOKRS AND pc.ValidTo >=  VBAK_ERDAT
WHERE fo.Dim_ProfitCenterId <> ifnull(pc.dim_profitcenterid, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_UnitOfMeasureId  =  ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE VBAP_KMEIN IS NOT NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent  =  1) uom
	     ON uom.UOM  =  vbap_kmein
WHERE fo.Dim_UnitOfMeasureId <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_UnitOfMeasureId  =  1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE VBAP_KMEIN IS NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0;

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_BaseUoMid = ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_meins IS NOT NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent  =  1) uom
	     ON uom.UOM  =  vbap_meins
WHERE fo.Dim_BaseUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_BaseUoMid  =   1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_meins IS NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0;

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesUoMid  =  ifnull(uom.Dim_UnitOfMeasureId, 1)
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_vrkme IS NOT NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0
	 LEFT JOIN (SELECT * FROM Dim_UnitOfMeasure WHERE RowIsCurrent  =  1) uom
	     ON uom.UOM  =  vbap_vrkme
WHERE fo.Dim_SalesUoMid <> ifnull(uom.Dim_UnitOfMeasureId, 1);

UPDATE fact_salesorder_tmptbl fo
SET fo.Dim_SalesUoMid  =  1
FROM fact_salesorder_tmptbl fo
     INNER JOIN (SELECT * FROM VBAK_VBAP WHERE vbap_vrkme IS NULL) v
	      ON fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR AND fo.dd_ScheduleNo = 0;


/*call vectorwise (combine 'fact_salesorder+fact_salesorder_tmptbl')*/

alter table fact_salesorder_tmptbl add constraint primary key (dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo);


INSERT INTO fact_salesorder
(
            fact_salesorderid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
            Dim_routeid,
	        Dim_SalesRiskCategoryId,
            dd_CreditRep,
            dd_CreditLimit,
            Dim_CustomerRiskCategoryId,
	        amt_UnitPriceUoM,
		    dim_Currencyid_TRA,
		    dim_Currencyid_GBL,
		    dim_currencyid_STAT,
		    amt_exchangerate_STAT,
		    ct_CumOrderQty
)
SELECT fact_salesorderid,
       dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       ct_ScheduleQtySalesUnit,
       ct_ConfirmedQty,
       ct_CorrectedQty,
       amt_UnitPrice,
       ct_PriceUnit,
       amt_ScheduleTotal,
       amt_StdCost,
       amt_TargetValue,
       amt_Tax,
       ct_TargetQty,
       amt_ExchangeRate,
       amt_ExchangeRate_GBL,
       ct_OverDlvrTolerance,
       ct_UnderDlvrTolerance,
       Dim_DateidSalesOrderCreated,
       Dim_DateidFirstDate,
       Dim_DateidSchedDeliveryReq,
       Dim_DateidSchedDlvrReqPrev,
       dim_DateidSchedDelivery,
       Dim_DateidGoodsIssue,
       Dim_DateidMtrlAvail,
       Dim_DateidLoading,
       Dim_DateidTransport,
       Dim_DateidGuaranteedate,
       Dim_Currencyid,
       Dim_ProductHierarchyid,
       Dim_Plantid,
       Dim_Companyid,
       Dim_StorageLocationid,
       Dim_SalesDivisionid,
       Dim_ShipReceivePointid,
       Dim_DocumentCategoryid,
       Dim_SalesDocumentTypeid,
       Dim_SalesOrgid,
       Dim_CustomerID,
       Dim_DateidValidFrom,
       Dim_DateidValidTo,
       Dim_SalesGroupid,
       Dim_CostCenterid,
       Dim_ControllingAreaid,
       Dim_BillingBlockid,
       Dim_TransactionGroupid,
       Dim_SalesOrderRejectReasonid,
       Dim_Partid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       Dim_CustomerGroup1id,
       Dim_CustomerGroup2id,
       Dim_SalesOrderItemCategoryid,
       Dim_ScheduleLineCategoryId,
       dd_ItemRelForDelv,
       Dim_ProfitCenterId,
       Dim_DistributionChannelId,
       dd_BatchNo,
       dd_CreatedBy,
       Dim_DateidNextDate,
       Dim_routeid,
	   Dim_SalesRiskCategoryId,
       dd_CreditRep,
	   
       dd_CreditLimit,
       Dim_CustomerRiskCategoryId,
	   amt_UnitPriceUoM,
       dim_Currencyid_TRA,
	   dim_Currencyid_GBL,
	   dim_currencyid_STAT,
	   amt_exchangerate_STAT,
	   ct_CumOrderQty
FROM fact_salesorder_tmptbl t
WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder f WHERE f.dd_SalesDocNo = t.dd_SalesDocNo AND f.dd_SalesItemNo = t.dd_SalesItemNo AND f.dd_ScheduleNo = t.dd_ScheduleNo);


DROP TABLE IF EXISTS fact_salesorder_useinsub;

UPDATE fact_salesorder fso
SET fso.Dim_SalesMiscId = IFNULL(smisc.Dim_SalesMiscId, 1)
FROM fact_salesorder fso
	INNER JOIN VBAK_VBAP vkp ON fso.dd_SalesDocNo = vkp.VBAP_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = 0
	LEFT JOIN Dim_SalesMisc smisc
		ON smisc.DeliveryDateQuantityFixed  =  ifnull(VBAP_FIXMG,'Not Set')
			AND smisc.FixedQuantity  =  ifnull(VBAP_FMENG, 'Not Set')
			AND smisc.OverDeliveryAllowed  =  ifnull(VBAP_UEBTK, 'Not Set')
			AND smisc.CashDiscountIndicator  =  ifnull(VBAP_SKTOF, 'Not Set')
			AND smisc.ReturnsItem  =  ifnull(VBAP_SHKZG, 'Not Set')
			AND smisc.PricingOk  =  ifnull(VBAP_PRSOK, 'Not Set')
			AND smisc.CustomerNotPostedGoodsReceipt  =  ifnull(VBAP_NACHL, 'Not Set')
			AND smisc.ItemRelevantForDelivery  =  ifnull(VBAP_LFREL, 'Not Set')
			AND smisc.ScheduleConfirmStatus  =  'Not Set'
			AND smisc.InvoiceReceiptIndicator  =  'Not Set'
WHERE fso.Dim_SalesMiscId <> smisc.Dim_SalesMiscId;

UPDATE fact_salesorder fso
SET fso.Dim_SalesMiscId = ifnull(smisc.Dim_SalesMiscId, 1)
FROM fact_salesorder fso
     INNER JOIN VBAK_VBAP_VBEP vkp ON fso.dd_SalesDocNo = vkp.VBAK_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN Dim_SalesMisc smisc
	     ON smisc.DeliveryDateQuantityFixed = ifnull(VBAP_FIXMG,'Not Set')
            AND smisc.FixedQuantity = ifnull(VBAP_FMENG, 'Not Set')
            AND smisc.OverDeliveryAllowed = ifnull(VBAP_UEBTK, 'Not Set')
            AND smisc.CashDiscountIndicator = ifnull(VBAP_SKTOF, 'Not Set')
            AND smisc.ReturnsItem = ifnull(VBAP_SHKZG, 'Not Set')
            AND smisc.PricingOk = ifnull(VBAP_PRSOK, 'Not Set')
            AND smisc.CustomerNotPostedGoodsReceipt = ifnull(VBAP_NACHL, 'Not Set')
            AND smisc.ItemRelevantForDelivery = ifnull(VBAP_LFREL, 'Not Set')
            AND smisc.ScheduleConfirmStatus = ifnull(VBEP_WEPOS, 'Not Set')
            AND smisc.InvoiceReceiptIndicator = ifnull(VBEP_REPOS, 'Not Set')
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_SalesMiscId <> ifnull(smisc.Dim_SalesMiscId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = ifnull(so.Dim_SalesOfficeId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT * FROM VBAK_VBAP_VBEP WHERE vbak_vkbur IS NOT NULL) vkp
	      ON fso.dd_SalesDocNo = vkp.VBAK_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN (SELECT * FROM dim_SalesOffice WHERE RowIsCurrent = 1) so
	     ON so.SalesOfficeCode = vbak_vkbur
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_SalesOfficeId <> ifnull(so.Dim_SalesOfficeId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = ifnull(so.Dim_SalesOfficeId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT * FROM vbak_vbap WHERE vbak_vkbur IS NOT NULL) vkp
	      ON fso.dd_SalesDocNo = vkp.VBAP_VBELN AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
	 LEFT JOIN (SELECT * FROM dim_SalesOffice WHERE RowIsCurrent = 1) so
	     ON so.SalesOfficeCode = vbak_vkbur
WHERE fso.dd_ScheduleNo = 0
      AND fso.Dim_SalesOfficeId <> ifnull(so.Dim_SalesOfficeId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_CustomerGroupId = ifnull(cg.Dim_CustomerGroupId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_kdgrp
	             FROM vbak_vbap_vbep x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAK_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN (SELECT * FROM dim_CustomerGroup WHERE RowIsCurrent = 1) cg
	     ON cg.CustomerGroup = vkp.knvv_kdgrp
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_CustomerGroupId <> ifnull(cg.Dim_CustomerGroupId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = ifnull(so.Dim_SalesOfficeId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_vkbur
	             FROM vbak_vbap_vbep x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAK_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN (SELECT * FROM dim_SalesOffice WHERE RowIsCurrent = 1) so
         ON so.SalesOfficeCode = vkp.knvv_vkbur
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_SalesOfficeId <> ifnull(so.Dim_SalesOfficeId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_CustomerPaymentTermsId = ifnull(cpt.dim_Customerpaymenttermsid, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_zterm
	             FROM vbak_vbap_vbep x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAK_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
     LEFT JOIN (SELECT * FROM dim_customerpaymentterms WHERE rowiscurrent = 1) cpt
	     ON cpt.PaymentTermCode = vkp.knvv_zterm
WHERE fso.dd_ScheduleNo <> 0
      AND fso.Dim_CustomerPaymentTermsId <> ifnull(cpt.dim_Customerpaymenttermsid, 1);

UPDATE fact_salesorder fso
SET fso.Dim_CustomerGroupId = ifnull(cg.Dim_CustomerGroupId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_kdgrp
	             FROM vbak_vbap x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM dim_CustomerGroup where rowiscurrent = 1) cg
	     ON cg.CustomerGroup = vkp.knvv_kdgrp
WHERE fso.Dim_CustomerGroupId <> ifnull(cg.Dim_CustomerGroupId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = ifnull(so.Dim_SalesOfficeId, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_vkbur
	             FROM vbak_vbap x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM dim_SalesOffice where rowiscurrent = 1) so
	     ON so.SalesOfficeCode  =  vkp.knvv_vkbur
WHERE fso.Dim_SalesOfficeId <> ifnull(so.Dim_SalesOfficeId, 1);

UPDATE fact_salesorder fso
SET fso.Dim_CustomerPaymentTermsId = ifnull(cpt.dim_Customerpaymenttermsid, 1)
FROM fact_salesorder fso
     INNER JOIN (SELECT x.*, k.knvv_zterm
	             FROM vbak_vbap x,
                      KNVV k
                 WHERE k.knvv_VTWEG = x.VBAK_VTWEG
                       AND k.knvv_SPART = x.VBAK_SPART
                       AND k.knvv_vkorg = x.VBAK_VKORG
                       AND k.knvv_KUNNR = x.VBAK_KUNNR) vkp
          ON fso.dd_SalesDocNo = vkp.VBAP_VBELN
             AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
             AND fso.dd_ScheduleNo = 0
     LEFT JOIN (SELECT * FROM dim_customerpaymentterms where rowiscurrent = 1) cpt
         ON cpt.PaymentTermCode = vkp.knvv_zterm
WHERE fso.Dim_CustomerPaymentTermsId <> ifnull(cpt.dim_Customerpaymenttermsid, 1);

UPDATE fact_salesorder fso
SET fso.Dim_SalesOfficeId = 1
WHERE fso.Dim_SalesOfficeId IS NULL;


UPDATE fact_salesorder so
SET so.Dim_OverallStatusCreditCheckId = ifnull(oscc.dim_overallstatusforcreditcheckID, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT DISTINCT VBUK_VBELN,VBUK_CMGST FROM vbuk) v
          ON so.dd_SalesDocNo = v.VBUK_VBELN
	 LEFT JOIN (SELECT overallstatusforcreditcheck, MIN(dim_overallstatusforcreditcheckid) dim_overallstatusforcreditcheckid FROM dim_overallstatusforcreditcheck WHERE RowIsCurrent = 1 GROUP BY overallstatusforcreditcheck) oscc
	     ON oscc.overallstatusforcreditcheck  =  ifnull(v.VBUK_CMGST, 'Not Set')
WHERE so.Dim_OverallStatusCreditCheckId <> ifnull(oscc.dim_overallstatusforcreditcheckID, 1);

/*tmp table to handle Dim_BillToPartyPartnerFunctionId updates */

DROP TABLE IF EXISTS tmp1_sof_Dim_CustomerPartnerFunctions;
CREATE TABLE tmp1_sof_Dim_CustomerPartnerFunctions
AS
SELECT cpf.CustomerNumber1,
       cpf.SalesOrgCode,
       cpf.DivisionCode,
       cpf.DistributionChannelCode,
       cpf.PartnerFunction,
       cpf.RowIsCurrent,
       max(PartnerCounter) max_PartnerCounter
FROM Dim_CustomerPartnerFunctions cpf
GROUP BY cpf.CustomerNumber1,
         cpf.SalesOrgCode,
         cpf.DivisionCode,
         cpf.DistributionChannelCode,
         cpf.PartnerFunction,
         cpf.RowIsCurrent;

DROP TABLE IF EXISTS tmp_sof_Dim_CustomerPartnerFunctions;
CREATE TABLE tmp_sof_Dim_CustomerPartnerFunctions
AS
SELECT cpf.CustomerNumber1,
       cpf.SalesOrgCode,
       cpf.DivisionCode,
       cpf.DistributionChannelCode,
       cpf.PartnerFunction,
       cpf.RowIsCurrent,
       Dim_CustomerPartnerFunctionsID
FROM Dim_CustomerPartnerFunctions cpf,
     tmp1_sof_Dim_CustomerPartnerFunctions t
WHERE cpf.CustomerNumber1   =  t.CustomerNumber1
      AND cpf.SalesOrgCode  =  t.SalesOrgCode
      AND cpf.DivisionCode  =  t.DivisionCode
      AND cpf.DistributionChannelCode  =  t.DistributionChannelCode
      AND cpf.PartnerFunction  =  t.PartnerFunction
      AND cpf.RowIsCurrent  =  t.RowIsCurrent
      AND cpf.PartnerCounter  =  t.max_PartnerCounter;

DROP TABLE IF EXISTS tmp1_sof_Dim_CustomerPartnerFunctions;

/*tmp table to handle Dim_BillToPartyPartnerFunctionId updates */

UPDATE fact_salesorder so
SET so.Dim_BillToPartyPartnerFunctionId = cpf.Dim_CustomerPartnerFunctionsID
FROM fact_salesorder so
     INNER JOIN (
SELECT distinct x.vbak_vbeln, x.vbap_posnr, x.vbep_etenr,x.vbak_kunnr,x.vbak_vkorg,x.vbap_spart,x.VBAK_VTWEG
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pBillToPartyPartnerFunction) shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM tmp_sof_Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pBillToPartyPartnerFunction
				      AND x.RowIsCurrent = 1) cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_BillToPartyPartnerFunctionId <> cpf.Dim_CustomerPartnerFunctionsID;

UPDATE fact_salesorder so
SET so.Dim_BillToPartyPartnerFunctionId = cpf.Dim_CustomerPartnerFunctionsID
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pBillToPartyPartnerFunction) shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM tmp_sof_Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pBillToPartyPartnerFunction
				      AND x.RowIsCurrent = 1) cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_BillToPartyPartnerFunctionId <> cpf.Dim_CustomerPartnerFunctionsID;

UPDATE fact_salesorder so
SET so.Dim_PayerPartnerFunctionId = cpf.Dim_CustomerPartnerFunctionsID
FROM fact_salesorder so
     INNER JOIN (SELECT  distinct x.vbak_vbeln, x.vbap_posnr, x.vbep_etenr,x.vbak_kunnr, x.vbak_vkorg, x.vbap_spart,x.VBAK_VTWEG
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pPayerPartnerFunction) shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT distinct x.CustomerNumber1, x.SalesOrgCode, x.DivisionCode, x.DistributionChannelCode, x.Dim_CustomerPartnerFunctionsID
                ,row_number()over(partition by  x.CustomerNumber1, x.SalesOrgCode, x.DivisionCode, x.DistributionChannelCode order by '') rowno
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pPayerPartnerFunction
				      AND x.RowIsCurrent = 1) cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
            AND cpf.rowno = 1
WHERE so.dd_scheduleno <> 0
      AND so.Dim_PayerPartnerFunctionId <> cpf.Dim_CustomerPartnerFunctionsID;

UPDATE fact_salesorder so
SET so.Dim_PayerPartnerFunctionId = cpf.Dim_CustomerPartnerFunctionsID
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pPayerPartnerFunction) shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pPayerPartnerFunction
				      AND x.RowIsCurrent = 1) cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_PayerPartnerFunctionId <> cpf.Dim_CustomerPartnerFunctionsID;

drop table if exists tmp_for_Dim_CustomerPartnerFunctionsID
;
create table tmp_for_Dim_CustomerPartnerFunctionsID as
select  cpf.Dim_CustomerPartnerFunctionsID , so.fact_salesorderid , row_number() over(partition by so.fact_salesorderid order by so.fact_salesorderid) as row_nr
FROM  fact_salesorder so
     INNER JOIN (SELECT distinct x.vbak_vbeln, x.vbap_posnr, x.vbep_etenr, x.vbak_kunnr, x.vbap_spart, x.vbak_vkorg,x.VBAK_VTWEG
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey
					   AND vb.pCustomPartnerFunctionKey <> 'Not Set')  shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT distinct x.CustomerNumber1, x.SalesOrgCode, x.DivisionCode, x.DistributionChannelCode,x.Dim_CustomerPartnerFunctionsID
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId <> cpf.Dim_CustomerPartnerFunctionsID
;


UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId = t.Dim_CustomerPartnerFunctionsID
FROM tmp_for_Dim_CustomerPartnerFunctionsID t,fact_salesorder so  where
t.fact_salesorderid = so.fact_salesorderid
and t.row_nr = 1
;

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId1 = cpf.Dim_CustomerPartnerFunctionsID
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey1
					   AND vb.pCustomPartnerFunctionKey1 <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey1
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey1 <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId1 <> cpf.Dim_CustomerPartnerFunctionsID;

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId1 = cpf.Dim_CustomerPartnerFunctionsID
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey1
					   AND vb.pCustomPartnerFunctionKey1 <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey1
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey1 <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId1 <> cpf.Dim_CustomerPartnerFunctionsID;

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId2 = cpf.Dim_CustomerPartnerFunctionsID
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey2
					   AND vb.pCustomPartnerFunctionKey2 <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey2
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey2 <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId2 <> cpf.Dim_CustomerPartnerFunctionsID;

UPDATE fact_salesorder so
SET so.Dim_CustomPartnerFunctionId2 = cpf.Dim_CustomerPartnerFunctionsID
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x,
				      vbpa sdp,
					  variable_holder_701 vb
			     WHERE sdp.vbpa_vbeln = x.vbak_vbeln
				       AND sdp.vbpa_posnr = x.vbap_posnr
                       AND sdp.vbpa_parvw = pCustomPartnerFunctionKey2
					   AND vb.pCustomPartnerFunctionKey2 <> 'Not Set') shi
          ON so.dd_SalesDocNo = shi.vbak_vbeln AND so.dd_SalesItemNo = shi.vbap_posnr AND so.dd_scheduleno = shi.vbep_etenr
	 LEFT JOIN (SELECT x.*
	            FROM Dim_CustomerPartnerFunctions x,
				     variable_holder_701 vb
				WHERE x.PartnerFunction = vb.pCustomPartnerFunctionKey2
				      AND x.RowIsCurrent = 1
					  AND vb.pCustomPartnerFunctionKey2 <> 'Not Set') cpf
		 ON cpf.CustomerNumber1 = shi.vbak_kunnr
            AND cpf.SalesOrgCode = shi.vbak_vkorg
            AND cpf.DivisionCode = shi.vbap_spart
            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CustomPartnerFunctionId2 <> cpf.Dim_CustomerPartnerFunctionsID;

UPDATE fact_salesorder so
SET so.dd_CustomerPONo  =  ifnull(vbk.VBAK_BSTNK,'Not Set')
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
WHERE so.dd_scheduleno <> 0
      AND so.dd_CustomerPONo <> ifnull(vbk.VBAK_BSTNK,'Not Set');

UPDATE fact_salesorder so
SET so.Dim_CreditRepresentativeId  = ifnull(Dim_CreditRepresentativegroupId, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM dim_creditrepresentativegroup WHERE RowIsCurrent  =  1) cg
	     ON cg.CreditRepresentativeGroup  =  vbk.VBAK_SBGRP
            AND cg.CreditControlArea  =  vbk.VBAK_KKBER
WHERE so.dd_scheduleno <> 0
      AND so.Dim_CreditRepresentativeId <> ifnull(Dim_CreditRepresentativegroupId, 1);

UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup1Id  = ifnull(mpg.Dim_MaterialPriceGroup1Id, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM Dim_MaterialPriceGroup1 WHERE rowiscurrent = 1) mpg
         ON mpg.MaterialPriceGroup1  =  ifnull(vbk.VBAP_MVGR1,'Not Set')
WHERE so.dd_ScheduleNo <> 0
      AND so.Dim_MaterialPriceGroup1Id <> ifnull(mpg.Dim_MaterialPriceGroup1Id, 1);

UPDATE fact_salesorder so
SET so.Dim_DeliveryBlockId  = ifnull(db.Dim_DeliveryBlockId, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM Dim_DeliveryBlock WHERE RowIsCurrent  =  1 ) db
         ON db.DeliveryBlock  =  vbk.VBAK_LIFSK
WHERE so.dd_ScheduleNo <> 0
      AND so.Dim_DeliveryBlockId <> ifnull(db.Dim_DeliveryBlockId, 1);

UPDATE fact_salesorder so
SET so.Dim_SalesDocOrderReasonId  = ifnull(sor.dim_salesdocorderreasonid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM dim_salesdocorderreason WHERE rowiscurrent = 1) sor
         ON sor.ReasonCode  =  vbk.VBAK_AUGRU
WHERE so.dd_ScheduleNo <> 0
      AND so.Dim_SalesDocOrderReasonId <> ifnull(sor.dim_salesdocorderreasonid, 1);

UPDATE fact_salesorder so
SET so.Dim_MaterialGroupId  = ifnull(mg.dim_materialgroupid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap_vbep  vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
     LEFT JOIN (SELECT * FROM dim_materialgroup where rowiscurrent = 1) mg
         ON mg.MaterialGroupCode  =  vbk.VBAP_MATKL
WHERE so.dd_ScheduleNo <> 0
      AND so.Dim_MaterialGroupId <> ifnull(mg.dim_materialgroupid, 1);

UPDATE fact_salesorder so
SET so.amt_SubTotal3  =  ifnull((vbk.VBAP_KZWI3 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)),1)
													   END)
								* vbep_bmeng , 0),
    so.amt_SubTotal4  =  ifnull((vbk.VBAP_KZWI4 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)),1)
													   END)
										* vbep_bmeng , 0)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x
				 WHERE x.vbap_netpr <> 0
                       AND x.vbap_netwr <> 0) vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
WHERE so.dd_ScheduleNo <> 0;

/* LK : 29 Aug changes - amt_Subtotal3_OrderQty */
UPDATE fact_salesorder so
SET so.amt_Subtotal3_OrderQty  =  ifnull((vbk.VBAP_KZWI3 / CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),4)
														THEN CASE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
																WHEN 0 THEN 1
																ELSE ifnull(Round((VBAP_NETWR / ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein = 0 then 1 else vbap_kpein end ))),4),1)
															END
														ELSE ifnull(VBAP_NETWR / (vbap_netpr / (case when vbap_kpein = 0 then 1 else vbap_kpein end)),1)
													   END)
								* vbep_wmeng , 0)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*
	             FROM vbak_vbap_vbep x
				 WHERE x.vbap_netpr <> 0
                       AND x.vbap_netwr <> 0) vbk
	      ON vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
WHERE so.dd_ScheduleNo <> 0;

/* LK : 29 Aug changes - End of amt_Subtotal3_OrderQty changes */

UPDATE fact_salesorder so
SET so.Dim_PurchaseOrderTypeId = ifnull(cpt.dim_customerpurchaseordertypeid, 1)
FROM fact_salesorder so
     INNER JOIN vbak_vbap vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN (SELECT CustomerPOType,max(dim_customerpurchaseordertypeid) as dim_customerpurchaseordertypeid FROM dim_customerpurchaseordertype WHERE rowiscurrent = 1
                   group by CustomerPOType) cpt
         ON cpt.CustomerPOType  =  vbk.VBAK_BSARK
WHERE so.dd_ScheduleNo  <> 0
      AND so.Dim_PurchaseOrderTypeId <> ifnull(cpt.dim_customerpurchaseordertypeid, 1);


UPDATE fact_salesorder so
SET so.Dim_DateIdPurchaseOrder = ifnull(dt.Dim_Dateid, 1)
FROM fact_salesorder so
     INNER JOIN (SELECT x.*, pl.CompanyCode
	             FROM vbak_vbap x,
                      dim_plant pl
			     WHERE pl.plantcode = x.VBAP_WERKS
				       AND pl.rowiscurrent  =  1) vbk
	      ON vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
	 LEFT JOIN dim_Date dt ON dt.DateValue  =  vbk.VBAK_BSTDK AND dt.CompanyCode  =  vbk.CompanyCode and dt.plantcode_factory=vbk.VBAP_WERKS
WHERE so.dd_ScheduleNo  <>  0
      AND so.Dim_DateIdPurchaseOrder <> ifnull(dt.Dim_Dateid, 1);

UPDATE          fact_salesorder so
   SET so.Dim_DateIdQuotationValidFrom = dt.Dim_Dateid
   FROM vbak_vbap_vbep vbk, dim_plant pl, dim_Date dt, fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0
AND dt.DateValue = vbk.VBAK_ANGDT AND dt.CompanyCode = pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdQuotationValidFrom <> dt.Dim_Dateid;


UPDATE          fact_salesorder so
   SET so.Dim_DateIdQuotationValidTo = dt.Dim_Dateid
FROM vbak_vbap_vbep vbk, dim_plant pl, dim_Date dt, fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0
AND dt.DateValue = vbk.VBAK_BNDDT  AND dt.CompanyCode = pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdQuotationValidTo <> dt.Dim_Dateid;


UPDATE          fact_salesorder so
   SET so.Dim_DateIdSOCreated = dt.Dim_Dateid
FROM vbak_vbap_vbep vbk, dim_plant pl, dim_Date dt, fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <> 0
AND dt.DateValue = vbk.VBAK_ERDAT AND dt.CompanyCode = pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdSOCreated <> dt.Dim_Dateid;



UPDATE          fact_salesorder so
   SET so.Dim_DateIdSODocument = dt.Dim_Dateid
FROM vbak_vbap_vbep vbk, dim_plant pl, dim_Date dt, fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <> 0
AND dt.DateValue = vbk.VBAK_AUDAT AND dt.CompanyCode = pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdSODocument <> dt.Dim_Dateid;


UPDATE          fact_salesorder so
       SET so.dd_ReferenceDocumentNo = ifnull(vbk.VBAP_VGBEL,'Not Set')
FROM vbak_vbap_vbep vbk, dim_plant pl, fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <> 0
AND so.dd_ReferenceDocumentNo <> ifnull(vbk.VBAP_VGBEL,'Not Set');


UPDATE          fact_salesorder so
   SET so.dd_CustomerPONo = ifnull(vbk.VBAK_BSTNK,'Not Set')
FROM vbak_vbap vbk, dim_plant pl, fact_salesorder so
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0
AND so.dd_CustomerPONo <> ifnull(vbk.VBAK_BSTNK,'Not Set');



UPDATE          fact_salesorder so
      SET so.Dim_CreditRepresentativeId = cg.Dim_CreditRepresentativegroupId
FROM vbak_vbap vbk, dim_plant pl,dim_creditrepresentativegroup cg, fact_salesorder so
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0
AND cg.CreditRepresentativeGroup = vbk.VBAK_SBGRP AND cg.CreditControlArea = vbk.VBAK_KKBER AND cg.RowIsCurrent = 1
AND so.Dim_CreditRepresentativeId <> cg.Dim_CreditRepresentativegroupId;


UPDATE          fact_salesorder so
   SET so.Dim_MaterialPriceGroup1Id = mpg.Dim_MaterialPriceGroup1Id
FROM vbak_vbap vbk, dim_plant pl, Dim_MaterialPriceGroup1 mpg, fact_salesorder so
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0
AND mpg.MaterialPriceGroup1 = vbk.VBAP_MVGR1 AND mpg.RowIsCurrent = 1
AND so.Dim_MaterialPriceGroup1Id <> mpg.Dim_MaterialPriceGroup1Id;



UPDATE          fact_salesorder so
   SET so.Dim_DeliveryBlockId = db.Dim_DeliveryBlockId
FROM vbak_vbap vbk, dim_plant pl, Dim_DeliveryBlock db, fact_salesorder so
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0
AND db.DeliveryBlock = ifnull(vbk.VBAK_LIFSK,'Not Set') AND db.RowIsCurrent = 1
AND so.Dim_DeliveryBlockId <> db.Dim_DeliveryBlockId;


UPDATE          fact_salesorder so
   SET so.Dim_SalesDocOrderReasonId = sor.dim_salesdocorderreasonid
FROM vbak_vbap vbk, dim_plant pl, dim_salesdocorderreason sor, fact_salesorder so
where vbk.VBAP_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0
AND sor.ReasonCode = vbk.VBAK_AUGRU AND sor.RowIsCurrent = 1
AND so.Dim_SalesDocOrderReasonId <> sor.dim_salesdocorderreasonid;


UPDATE          fact_salesorder so
   SET so.Dim_MaterialGroupId  = mg.dim_materialgroupid
FROM vbak_vbap vbk, dim_plant pl, dim_materialgroup mg, fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND mg.MaterialGroupCode  =  vbk.VBAP_MATKL AND mg.RowIsCurrent  =  1
AND so.Dim_MaterialGroupId <> mg.dim_materialgroupid;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
from vbak_vbap_vbep, dim_materialpricegroup4 mg4, fact_salesorder so
WHERE so.dd_SalesDocNo = VBAK_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = VBEP_ETENR
AND VBAP_MVGR4 is not null
AND VBAP_MVGR4 = mg4.MaterialPriceGroup4
AND mg4.RowIsCurrent = 1
AND so.dim_materialpricegroup4id <> mg4.dim_materialpricegroup4id;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
from vbak_vbap, dim_materialpricegroup4 mg4, fact_salesorder so
WHERE so.dd_SalesDocNo = VBAP_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = 0
AND VBAP_MVGR4 is not null
AND VBAP_MVGR4 = mg4.MaterialPriceGroup4
AND mg4.RowIsCurrent = 1
AND so.dim_materialpricegroup4id <> mg4.dim_materialpricegroup4id;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id = 1
WHERE so.dim_materialpricegroup4id IS NULL;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
from vbak_vbap_vbep, dim_materialpricegroup5 mg5, fact_salesorder so
WHERE so.dd_SalesDocNo = VBAK_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = VBEP_ETENR
AND VBAP_MVGR5 is not null
AND VBAP_MVGR5 = mg5.MaterialPriceGroup5
AND mg5.RowIsCurrent = 1
AND so.dim_materialpricegroup5id <> mg5.dim_materialpricegroup5id;;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
from vbak_vbap, dim_materialpricegroup5 mg5, fact_salesorder so
WHERE so.dd_SalesDocNo = VBAP_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = 0
AND VBAP_MVGR5 is not null
AND VBAP_MVGR5 = mg5.MaterialPriceGroup5
AND mg5.RowIsCurrent = 1
AND so.dim_materialpricegroup5id <> mg5.dim_materialpricegroup5id;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id = 1
WHERE so.dim_materialpricegroup5id IS NULL;


UPDATE          fact_salesorder so
SET so.amt_SubTotal3  =  ifnull(vbk.VBAP_KZWI3, 0)
FROM vbak_vbap vbk, dim_plant pl, fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND so.amt_SubTotal3 <> ifnull(vbk.VBAP_KZWI3, 0);

UPDATE          fact_salesorder so
SET so.amt_SubTotal4  =  ifnull(vbk.VBAP_KZWI4, 0)
FROM vbak_vbap vbk, dim_plant pl,fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND so.amt_SubTotal4 <> ifnull(vbk.VBAP_KZWI4, 0);

/* LK : 29 Aug change - Update new column amt_Subtotal3_OrderQty */

/* No need to split when there are no schedules */

UPDATE fact_salesorder so
   SET so.amt_Subtotal3_OrderQty = amt_SubTotal3
WHERE so.dd_ScheduleNo = 0;

/* End of 29 Aug change - Update new column amt_Subtotal3_OrderQty */


UPDATE          fact_salesorder so
SET so.Dim_PurchaseOrderTypeId = cpt.dim_customerpurchaseordertypeid
FROM vbak_vbap vbk, dim_plant pl,dim_customerpurchaseordertype cpt,fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND cpt.CustomerPOType  =  vbk.VBAK_BSARK AND cpt.RowIsCurrent  =  1
AND so.Dim_PurchaseOrderTypeId <> cpt.dim_customerpurchaseordertypeid;


UPDATE          fact_salesorder so
SET so.Dim_DateIdPurchaseOrder = dt.Dim_Dateid
FROM vbak_vbap vbk, dim_plant pl,dim_Date dt, fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND dt.DateValue  =  vbk.VBAK_BSTDK AND dt.CompanyCode  =  pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdPurchaseOrder <> dt.Dim_Dateid ;



UPDATE          fact_salesorder so
SET so.Dim_DateIdQuotationValidFrom = dt.Dim_Dateid
FROM vbak_vbap vbk, dim_plant pl,dim_Date dt, fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND dt.DateValue  =  vbk.VBAK_ANGDT AND dt.CompanyCode  =  pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdQuotationValidFrom <> dt.Dim_Dateid ;


UPDATE          fact_salesorder so
SET so.Dim_DateIdQuotationValidTo = dt.Dim_Dateid
FROM vbak_vbap vbk, dim_plant pl,dim_Date dt, fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND dt.DateValue  =  vbk.VBAK_BNDDT AND dt.CompanyCode  =  pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdQuotationValidTo <> dt.Dim_Dateid ;



UPDATE          fact_salesorder so
SET so.Dim_DateIdSOCreated = dt.Dim_Dateid
FROM vbak_vbap vbk, dim_plant pl,dim_Date dt, fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND dt.DateValue  =  vbk.VBAK_ERDAT AND dt.CompanyCode  =  pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdSOCreated <> dt.Dim_Dateid ;



UPDATE          fact_salesorder so
SET so.Dim_DateIdSODocument = dt.Dim_Dateid
FROM vbak_vbap vbk, dim_plant pl,dim_Date dt, fact_salesorder so
where vbk.VBAP_VBELN  =  so.dd_SalesDocNo
AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo  = 0
AND dt.DateValue  =  vbk.VBAK_AUDAT AND dt.CompanyCode  =  pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.Dim_DateIdSODocument <> dt.Dim_Dateid ;


UPDATE    fact_salesorder so
SET so.Dim_SalesDistrictId  =  sd.Dim_SalesDistrictid
FROM vbak_vbap_vbkd vkd, dim_salesdistrict sd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN AND vkd.VBKD_POSNR = 0
AND vkd.VBKD_BZIRK IS NOT NULL
AND sd.SalesDistrict  =  vkd.VBKD_BZIRK
AND sd.RowIsCurrent  =  1
AND so.Dim_SalesDistrictId <> sd.Dim_SalesDistrictid;

UPDATE    fact_salesorder so
SET so.Dim_SalesDistrictId  =  sd.Dim_SalesDistrictid
FROM vbak_vbap_vbkd vkd, dim_salesdistrict sd,  fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND vkd.VBKD_BZIRK IS NOT NULL
AND sd.SalesDistrict  =  vkd.VBKD_BZIRK
AND sd.RowIsCurrent  =  1
AND so.Dim_SalesDistrictId <> sd.Dim_SalesDistrictid;


UPDATE    fact_salesorder so
SET so.Dim_AccountAssignmentGroupId  =  aag.Dim_AccountAssignmentGroupId
FROM vbak_vbap_vbkd vkd, dim_accountassignmentgroup aag, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN AND vkd.VBKD_POSNR = 0
AND vkd.VBKD_KTGRD IS NOT NULL
AND aag.AccountAssignmentGroup  =  vkd.VBKD_KTGRD
AND aag.RowIsCurrent  =  1
AND so.Dim_AccountAssignmentGroupId <> aag.Dim_AccountAssignmentGroupId;

UPDATE    fact_salesorder so
SET so.Dim_AccountAssignmentGroupId  =  aag.Dim_AccountAssignmentGroupId
FROM vbak_vbap_vbkd vkd, dim_accountassignmentgroup aag, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND vkd.VBKD_KTGRD IS NOT NULL
AND aag.AccountAssignmentGroup  =  vkd.VBKD_KTGRD
AND aag.RowIsCurrent  =  1
AND so.Dim_AccountAssignmentGroupId <> aag.Dim_AccountAssignmentGroupId;

UPDATE    fact_salesorder so
SET so.dd_BusinessCustomerPONo  =  ifnull(vkd.VBKD_BSTKD,'Not Set')
FROM vbak_vbap_vbkd vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND vkd.VBKD_POSNR = 0
AND so.dd_BusinessCustomerPONo <> ifnull(vkd.VBKD_BSTKD,'Not Set');

UPDATE    fact_salesorder so
SET so.dd_BusinessCustomerPONo  =  ifnull(vkd.VBKD_BSTKD,'Not Set')
FROM vbak_vbap_vbkd vkd,fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.dd_BusinessCustomerPONo <> ifnull(vkd.VBKD_BSTKD,'Not Set');

UPDATE    fact_salesorder so
SET so.Dim_BillingDateId  =  dt.dim_dateid
FROM vbak_vbap_vbkd vkd, dim_date dt, dim_company dc, fact_salesorder so, dim_plant pl
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN AND vkd.VBKD_POSNR = 0 /* 28 APr : Only posnr = 0 to be applied as default which will be overridden with item level dt*/
AND so.dim_companyid  =  dc.dim_companyid
and so.dim_plantid = pl.dim_plantid
AND dt.datevalue  =  VBKD_FKDAT
AND dt.companycode  =  dc.CompanyCode and dt.plantcode_factory=pl.plantcode
AND VBKD_FKDAT IS NOT NULL
AND so.Dim_BillingDateId <> dt.dim_dateid;

UPDATE    fact_salesorder so
SET so.Dim_BillingDateId  =  dt.dim_dateid
FROM vbak_vbap_vbkd vkd, dim_date dt, dim_company dc, fact_salesorder so, dim_plant pl
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.dim_companyid  =  dc.dim_companyid
and so.dim_plantid = pl.dim_plantid
AND dt.datevalue  =  VBKD_FKDAT
AND dt.companycode  =  dc.CompanyCode  and dt.plantcode_factory=pl.plantcode
AND VBKD_FKDAT IS NOT NULL
AND so.Dim_BillingDateId <> dt.dim_dateid;

/*NN Oct 30 2015 update dim_dateidoriginalrequestdate*/

UPDATE    fact_salesorder so
SET so.dim_dateidorginalrequestdate  =  dt.dim_dateid
	,so.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbkd vkd, dim_date dt, dim_company dc, fact_salesorder so, dim_plant pl
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.dim_companyid  =  dc.dim_companyid
and so.dim_plantid = pl.dim_plantid
AND dt.datevalue  =  vkd.VBKD_BSTDK_E
AND dt.companycode  =  dc.CompanyCode and dt.plantcode_factory=pl.plantcode
AND vkd.VBKD_BSTDK_E IS NOT NULL
AND so.dim_dateidorginalrequestdate <> dt.dim_dateid;

UPDATE fact_salesorder so
SET so.dim_dateidorginalrequestdate = so.Dim_DateidSchedDelivery
WHERE dim_dateidorginalrequestdate = 1 and Dim_DateidSchedDelivery <> 1;

/*NN Oct 30 2015 update dim_dateidoriginalrequestdate*/

/* Liviu Ionescu - add VBKD-BSTDK_E as a separate field - BI-3411*/
UPDATE    fact_salesorder so
SET so.dim_ShiptoPartyPODate  =  dt.dim_dateid
	,so.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbkd vkd, dim_date dt, dim_company dc, fact_salesorder so,dim_plant pl
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR 
AND so.dim_companyid  =  dc.dim_companyid
and so.dim_plantid=pl.dim_plantid
AND dt.datevalue  =  vkd.VBKD_BSTDK_E
AND dt.companycode  =  dc.CompanyCode and dt.plantcode_factory=pl.plantcode
AND vkd.VBKD_BSTDK_E IS NOT NULL
AND so.dim_ShiptoPartyPODate <> dt.dim_dateid;

/* End Liviu Ionescu */
UPDATE          fact_salesorder so
SET so.Dim_DateIdSOItemChangedOn  = dt.Dim_Dateid
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt, fact_salesorder so
Where vbk.VBAK_VBELN  =  so.dd_SalesDocNo
             AND vbk.VBAP_POSNR  =  so.dd_SalesItemNo
             AND vbk.VBEP_ETENR  =  so.dd_ScheduleNo
AND pl.plantcode  =  vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND  dt.DateValue  =  vbk.VBAP_AEDAT AND dt.CompanyCode  =  pl.CompanyCode and dt.plantcode_factory=pl.plantcode  AND so.dd_scheduleno <> 0
AND vbk.VBAP_AEDAT IS NOT NULL
AND so.Dim_DateIdSOItemChangedOn <> dt.Dim_Dateid;


drop table if exists tmp_salesord_VBFA_upd;
create table tmp_salesord_VBFA_upd as
select f.VBFA_VBELV,f.VBFA_POSNV,VBFA_RFMNG,VBFA_VBELN,VBFA_POSNN,VBFA_VBTYP_N,
ROW_NUMBER() OVER(PARTITION BY dd_SalesDocNo,dd_SalesItemNo
				order by vbfa_posnn desc)  RowSeqNo
from
fact_salesorder so
,vbfa_vbak_vbap f
WHERE f.VBFA_VBTYP_N = 'J'
AND f.VBFA_VBELV = so.dd_SalesDocNo
AND f.VBFA_POSNV = so.dd_SalesItemNo;

UPDATE    fact_salesorder so
SET so.ct_AfsTotalDrawn  =  ifnull(f.VBFA_RFMNG,0)
FROM tmp_salesord_VBFA_upd f, fact_salesorder so
WHERE f.VBFA_VBTYP_N  =  'J'
AND  f.VBFA_VBELV  =  so.dd_SalesDocNo
AND f.VBFA_POSNV  =  so.dd_SalesItemNo
and f.rowseqno = 1
AND so.ct_AfsTotalDrawn <> ifnull(f.VBFA_RFMNG,0);


/* Madalina 29 Jun 2016 */
/* Filter for vbfa_vbak_vbap was modified to VBTYP_N in ('J','T','M'), so dd_SubsequentDocNo ( Follow on doc) needs to be calculated based on all of them. BI-3161 */
/*UPDATE    fact_salesorder so
FROM vbfa_vbak_vbap f
SET so.dd_SubsequentDocNo  =  ifnull(f.VBFA_VBELN, 'Not Set')
WHERE f.VBFA_VBTYP_N  =  'J'
AND  f.VBFA_VBELV  =  so.dd_SalesDocNo
AND f.VBFA_POSNV  =  so.dd_SalesItemNo
AND so.dd_SubsequentDocNo <> ifnull(f.VBFA_VBELN, 'Not Set')
*/
drop table if exists tmpFollowOnDoc_vbfa_vbak_vbap;

create table tmpFollowOnDoc_vbfa_vbak_vbap as
select VBFA_VBELN, VBFA_VBTYP_N, VBFA_POSNV, VBFA_VBELV
from vbfa_vbak_vbap f, fact_salesorder so
where  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	and f.VBFA_VBTYP_N  =  'J'
	and VBFA_VBELN is not null;

insert into tmpFollowOnDoc_vbfa_vbak_vbap
select VBFA_VBELN, VBFA_VBTYP_N, VBFA_POSNV, VBFA_VBELV  
from vbfa_vbak_vbap f, fact_salesorder so
where  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	and f.VBFA_VBTYP_N  =  'T'
	and VBFA_VBELN is not null
	and not exists (select 1 from tmpFollowOnDoc_vbfa_vbak_vbap tmp where tmp.VBFA_VBELV = f.VBFA_VBELV and tmp.VBFA_POSNV = f.VBFA_POSNV);
	
insert into tmpFollowOnDoc_vbfa_vbak_vbap
select VBFA_VBELN, VBFA_VBTYP_N, VBFA_POSNV, VBFA_VBELV  
from vbfa_vbak_vbap f, fact_salesorder so
where  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	and f.VBFA_VBTYP_N  =  'M'
	and VBFA_VBELN is not null
	and not exists (select 1 from tmpFollowOnDoc_vbfa_vbak_vbap tmp where tmp.VBFA_VBELV = f.VBFA_VBELV and tmp.VBFA_POSNV = f.VBFA_POSNV);
	
/* Madalina 30 Jun 2016 - Filter for vbfa_vbak_vbap was modified to VBTYP_N in ('J','T','M','O','P'), so dd_SubsequentDocNo ( Follow on doc) needs to be calculated based on all of them - BI-3161 */
insert into tmpFollowOnDoc_vbfa_vbak_vbap
select VBFA_VBELN, VBFA_VBTYP_N, VBFA_POSNV, VBFA_VBELV  
from vbfa_vbak_vbap f, fact_salesorder so
where  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	and f.VBFA_VBTYP_N  =  'O'
	and VBFA_VBELN is not null
	and not exists (select 1 from tmpFollowOnDoc_vbfa_vbak_vbap tmp where tmp.VBFA_VBELV = f.VBFA_VBELV and tmp.VBFA_POSNV = f.VBFA_POSNV);
	
insert into tmpFollowOnDoc_vbfa_vbak_vbap
select VBFA_VBELN, VBFA_VBTYP_N, VBFA_POSNV, VBFA_VBELV  
from vbfa_vbak_vbap f, fact_salesorder so
where  f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	and f.VBFA_VBTYP_N  =  'P'
	and VBFA_VBELN is not null
	and not exists (select 1 from tmpFollowOnDoc_vbfa_vbak_vbap tmp where tmp.VBFA_VBELV = f.VBFA_VBELV and tmp.VBFA_POSNV = f.VBFA_POSNV);
/* End  Madalina 30 Jun 2016 */

UPDATE fact_salesorder so
SET so.dd_SubsequentDocNo  =  ifnull(f.VBFA_VBELN, 'Not Set')
FROM (select distinct VBFA_VBELV,VBFA_POSNV, FIRST_VALUE(VBFA_VBELN) OVER (PARTITION BY VBFA_VBELV,VBFA_POSNV ORDER BY VBFA_VBELN DESC) AS VBFA_VBELN FROM tmpFollowOnDoc_vbfa_vbak_vbap) f, fact_salesorder so
WHERE f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	AND so.dd_SubsequentDocNo <> ifnull(f.VBFA_VBELN, 'Not Set');

/* New field added - dd_categSubsequentDoc - the corresponding type of the Follow On Document BI-3161 */
UPDATE fact_salesorder so
SET so.dd_categSubsequentDoc  =  ifnull(f.VBFA_VBTYP_N, 'Not Set')
FROM (select distinct VBFA_VBELV,VBFA_POSNV, FIRST_VALUE(VBFA_VBTYP_N) OVER (PARTITION BY VBFA_VBELV,VBFA_POSNV ORDER BY VBFA_VBTYP_N DESC) AS VBFA_VBTYP_N FROM tmpFollowOnDoc_vbfa_vbak_vbap) f, fact_salesorder so
WHERE f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	AND so.dd_categSubsequentDoc <> ifnull(f.VBFA_VBTYP_N, 'Not Set');
/* Madalina 30 Jun 2016 - If a Doc category of subsequent document does not exist, the reset its corresponding Follow On Doc  BI-3161 */
update fact_salesorder so
set so.dd_SubsequentDocNo = 'Not Set' where
	ifnull(so.dd_categSubsequentDoc, 'Not Set') = 'Not Set'
	and so.dd_SubsequentDocNo <> 'Not Set';
/* End Madalina 30 Jun 2016 */
	
/* New field added - ct_QuantityClosed - sum of VBFA_RFMNG quantity BI-3161 */
drop table if exists tmp_ct_QuantityClosed;
create table tmp_ct_QuantityClosed as
select dd_SalesDocNo, dd_SalesItemNo, dd_ScheduleNo, dd_SubsequentDocNo, dd_categSubsequentDoc, sum(ifnull(f.VBFA_RFMNG,0)) as ct_QuantityClosed
FROM vbfa_vbak_vbap f, fact_salesorder so
where f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	and so.dd_categSubsequentDoc  =  ifnull(f.VBFA_VBTYP_N, 'Not Set')     /* Madalina 13 Jul 2016 - Additional conditions and grouping fields - BI-3161*/
	and so.dd_categSubsequentDoc <> 'Not Set'
group by dd_SalesDocNo, dd_SalesItemNo, dd_ScheduleNo, dd_SubsequentDocNo, dd_categSubsequentDoc;


update fact_salesorder fso
set fso.ct_QuantityClosed = ifnull(qc.ct_QuantityClosed, 0)
from tmp_ct_QuantityClosed qc, fact_salesorder fso
where fso.dd_SalesDocNo = qc.dd_SalesDocNo
	and fso.dd_SalesItemNo = qc.dd_SalesItemNo
	and fso.dd_ScheduleNo = qc.dd_ScheduleNo
	and fso.dd_SubsequentDocNo = qc.dd_SubsequentDocNo
	and fso.dd_categSubsequentDoc = qc.dd_categSubsequentDoc
	and fso.ct_QuantityClosed <> ifnull(qc.ct_QuantityClosed, 0);
	
/* Madalina 14 Jul 2016 - additional conditions for Quantity Closed - BI - 3161*/	
drop table if exists tmp_ct_QuantityClosed;
create table tmp_ct_QuantityClosed as
select dd_SalesDocNo, dd_SalesItemNo, dd_ScheduleNo, dd_SubsequentDocNo, dd_categSubsequentDoc, sum(ifnull(f.VBFA_RFMNG,0)) as ct_QuantityClosed
FROM vbfa_vbak_vbap f, fact_salesorder so
where f.VBFA_VBELV  =  so.dd_SalesDocNo
	AND f.VBFA_POSNV  =  so.dd_SalesItemNo
	and so.dd_categSubsequentDoc = 'Not Set'
group by dd_SalesDocNo, dd_SalesItemNo, dd_ScheduleNo, dd_SubsequentDocNo, dd_categSubsequentDoc;


update fact_salesorder fso
set fso.ct_QuantityClosed = ifnull(qc.ct_QuantityClosed, 0)
from tmp_ct_QuantityClosed qc, fact_salesorder fso
where fso.dd_SalesDocNo = qc.dd_SalesDocNo
	and fso.dd_SalesItemNo = qc.dd_SalesItemNo
	and fso.dd_ScheduleNo = qc.dd_ScheduleNo
	and fso.dd_SubsequentDocNo = qc.dd_SubsequentDocNo
	and fso.dd_categSubsequentDoc = qc.dd_categSubsequentDoc
	and fso.ct_QuantityClosed <> ifnull(qc.ct_QuantityClosed, 0);


/* Madalina 02 Aug 2016 - Update dim_dateidactualgi earlier, so that can be used for Quantity closed - BI -3161 */		
/*Ambiguous Replace fix- Taking the max value*/
merge into fact_salesorder fso
using (select distinct fso.fact_salesorderid,max(f.dim_dateidactualgi) as dim_dateidactualgi
    from fact_salesorder f,fact_salesorder fso
	where f.dd_SalesDocNo = fso.dd_SalesDocNo
		and f.dd_SalesItemNo = fso.dd_SalesItemNo
		and f.dim_dateidactualgi <> '1'
		and fso.dim_dateidactualgi = '1'
group by fso.fact_salesorderid) t on t.fact_salesorderid=fso.fact_salesorderid
when matched then update set fso.dim_dateidactualgi = t.dim_dateidactualgi
where fso.dim_dateidactualgi <> t.dim_dateidactualgi;	
	
/* Madalina 13 Jul 2016 - reset ct_QuantityClosed (Quantity Closed) for Actual Goods Issue Date = Not Set and Item Status.Delivery Status = Not Set - BI-3161 */
update fact_salesorder fso
set fso.ct_QuantityClosed = 0 
from dim_salesorderitemstatus i, fact_salesorder fso
where fso.dim_dateidactualgi = 1 	/*fs.dim_dateidshipmentdelivery = 1 */
	and fso.dim_salesorderitemstatusid =  i.dim_salesorderitemstatusid
	and i.DeliveryStatus <> 'Not Set' 
 	and fso.ct_QuantityClosed <> 0;
/* END 13 Jul 2016  */


/* New field Execution Status Sales Schedule - BI-3128*/	
/* Madalina 02 Aug 2016 - new formulas for dd_executionstatusschedule and dd_executionstatusitem - BI-3128 */
/*
drop table if exists tmp_for_executionstatus
create table tmp_for_executionstatus as 
select distinct f_so.dd_SalesDocNo, f_so.dd_SalesItemNo,f_so.dd_ScheduleNo, 
CASE WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Not yet processed' THEN 'Open'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Partially processed' AND soh.RejectionStatus = 'Partially processed' AND soi.ItemRejectionStatus = 'Not yet processed' THEN 'Open'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Partially processed' AND soh.RejectionStatus = 'Partially processed' AND NOT(soi.ItemRejectionStatus = 'Not yet processed') THEN 'Cancelled'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Partially processed' AND NOT(soh.RejectionStatus = 'Partially processed') THEN 'Open'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Completely processed' AND NOT(f_so.dd_SubsequentDocNo = 'Not Set') AND soi.DeliveryStatus = 'Completely processed' THEN 'Open'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Completely processed' AND NOT(f_so.dd_SubsequentDocNo = 'Not Set') AND NOT(soi.DeliveryStatus= 'Completely processed') THEN 'Cancelled'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Completely processed' AND f_so.dd_SubsequentDocNo = 'Not Set' AND soi.OverallProcessingStatus = 'Not yet processed' THEN 'Open'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Completely processed' AND f_so.dd_SubsequentDocNo = 'Not Set' AND NOT(soi.OverallProcessingStatus = 'Not yet processed') THEN 'Cancelled'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Not Set' AND soh.RejectionStatus = 'Not yet processed' THEN 'N/a (Not applicable)'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Not Set' AND NOT(soh.RejectionStatus = 'Not yet processed') AND soi.ItemRejectionStatus = 'Not yet processed'  THEN 'N/a (Not applicable)'
WHEN ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Not Set' AND NOT(soh.RejectionStatus = 'Not yet processed') AND NOT(soi.ItemRejectionStatus = 'Not yet processed')  THEN 'Cancelled'
WHEN ct_QuantityClosed <> 0  THEN 'Closed'
END as dd_executionstatusschedule
from fact_salesorder f_so, dim_salesorderheaderstatus soh,dim_salesorderitemstatus soi
where f_so.dim_salesorderitemstatusid=soi.dim_salesorderitemstatusid
and f_so.dim_salesorderheaderstatusid=soh.dim_salesorderheaderstatusid

update fact_salesorder f_so
from tmp_for_executionstatus t
set f_so.dd_executionstatusschedule = ifnull(t.dd_executionstatusschedule, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/ /*
where f_so.dd_SalesDocNo =t.dd_SalesDocNo
and  f_so.dd_SalesItemNo=t.dd_SalesItemNo
and f_so.dd_ScheduleNo=t.dd_ScheduleNo
and f_so.dd_executionstatusschedule <> ifnull(t.dd_executionstatusschedule, 'Not Set')  */

/* END Madalina 29 Jun 2016 */

/* Madalina 30 Jun 2016 - Add fields Execution Status Sales Item (dd_executionstatusitem) and Execution Status Sales Header (dd_executionstatusheader) BI-3361 */
/*drop table if exists tmp_for_schedule
create table tmp_for_schedule as 
select so.dd_SalesDocNo, so.dd_SalesItemNo, listagg(distinct dd_executionstatusschedule, ',') WITHIN GROUP(ORDER BY dd_executionstatusschedule asc) dd_schedulestatus 
from fact_salesorder so
group by so.dd_SalesDocNo, so.dd_SalesItemNo
 
drop table if exists tmp_for_upd_statusitem
create table tmp_for_upd_statusitem as
select distinct so.dd_SalesDocNo, so.dd_SalesItemNo,
	case when dd_schedulestatus like '%Closed%' then
				( case when dd_schedulestatus like '%Open%' then 'Partially Open' else 'Closed' end) 
	else ( case when dd_schedulestatus like '%Open%' then 'Open'
			else (case when dd_schedulestatus like '%Cancelled%' then 'Cancelled' else 'N/a (Not Applicable)' end)
 		   end )
 	end as dd_schedulestatus1
from tmp_for_schedule so
 
update fact_salesorder fo
from tmp_for_upd_statusitem t
set fo.dd_executionstatusitem = ifnull(t.dd_schedulestatus1, 'Not Set')
where fo.dd_SalesDocNo = t.dd_SalesDocNo
	and fo.dd_SalesItemNo = t.dd_SalesItemNo
	and fo.dd_executionstatusitem <> ifnull(t.dd_schedulestatus1, 'Not Set')
*/

drop table if exists tmp_for_executionstatus;
create table tmp_for_executionstatus as 
select distinct f_so.dd_SalesDocNo, f_so.dd_SalesItemNo,f_so.dd_ScheduleNo, 
	case when ct_QuantityClosed = 0 AND soh.OverallProcessStatusItem = 'Not yet processed' THEN 'Open'
		when ct_QuantityClosed <> 0 AND soh.OverallProcessStatusItem = 'Not yet processed' THEN 'Closed'
		when soh.OverallProcessStatusItem = 'Not Set' AND NOT(soi.ItemRejectionStatus = 'Completely processed') THEN 'N/a (Not applicable)'
		when soh.OverallProcessStatusItem = 'Not Set' AND soi.ItemRejectionStatus = 'Completely processed' THEN 'Cancelled'
		when soh.OverallProcessStatusItem = 'Partially processed' AND soi.OverallProcessingStatus = 'Not Set' THEN 'Open'
		when soh.OverallProcessStatusItem = 'Partially processed' AND soi.OverallProcessingStatus = 'Not yet processed' THEN
			( case when dd_SubsequentDocNo = 'Not Set' THEN 'Open' else 'Closed' end)
		when soh.OverallProcessStatusItem = 'Partially processed' AND soi.OverallProcessingStatus = 'Partially processed' THEN 
			( case when ct_QuantityClosed = 0 THEN 'Open' else 'Partially Open' end)
		when soh.OverallProcessStatusItem = 'Partially processed' AND soi.OverallProcessingStatus = 'Completely processed' THEN
			(case when soi.ItemRejectionStatus = 'Completely processed' THEN  
				(case when ct_QuantityClosed = 0 THEN 'Cancelled' else 'Closed' end)
			 else 
				(case when soi.DeliveryStatus = 'Not Set' THEN 'Closed' 
				 else
					(case when dd_SubsequentDocNo = 'Not Set' THEN 'Cancelled'
					else
						(case when ct_QuantityClosed = 0 THEN 'Open'
						else
							(case when ct_QuantityClosed < ct_scheduleqtybaseuom THEN 'Partially Open' ELSE 'Closed' end)
						end
						)
					end
					)
				end
				 )
			end
			)
		when soh.OverallProcessStatusItem = 'Completely processed' and dd_SubsequentDocNo = 'Not Set' then
			(case when soi.OverallProcessingStatus = 'Not yet processed' THEN 'Open' else 'Cancelled' end)
		when soh.OverallProcessStatusItem = 'Completely processed' and not(dd_SubsequentDocNo = 'Not Set') then
			(case when ct_QuantityClosed = 0 then
				(case when soi.DeliveryStatus = 'Not yet processed' then 'Cancelled' else 'Open' end)
			else
				(case when ct_QuantityClosed < ct_scheduleqtybaseuom then 
			(case when soi.OverallProcessingStatus = 'Completely processed' then 
			'Closed' else 'Partially Open' end)		
				ELSE 'Closed' end)
			end
			)
	end as dd_executionstatusschedule
from fact_salesorder f_so, dim_salesorderheaderstatus soh,dim_salesorderitemstatus soi
where f_so.dim_salesorderitemstatusid=soi.dim_salesorderitemstatusid
and f_so.dim_salesorderheaderstatusid=soh.dim_salesorderheaderstatusid;

update fact_salesorder f_so
set f_so.dd_executionstatusschedule = ifnull(t.dd_executionstatusschedule, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_for_executionstatus t, fact_salesorder f_so
where f_so.dd_SalesDocNo =t.dd_SalesDocNo
and  f_so.dd_SalesItemNo=t.dd_SalesItemNo
and f_so.dd_ScheduleNo=t.dd_ScheduleNo
and f_so.dd_executionstatusschedule <> ifnull(t.dd_executionstatusschedule, 'Not Set');	

update fact_salesorder f_so
set f_so.dd_executionstatusitem = ifnull(f_so.dd_executionstatusschedule,'Not Set')
where f_so.dd_executionstatusitem <> f_so.dd_executionstatusschedule;
/* END 02 Aug 2016 */

drop table if exists tmp_for_header;
create table tmp_for_header as
select so.dd_SalesDocNo, group_concat(distinct dd_executionstatusitem ORDER BY dd_executionstatusitem asc SEPARATOR ',') dd_headerstatus
from fact_salesorder so
group by so.dd_SalesDocNo;

drop table if exists tmp_for_upd_statusheader;
create table tmp_for_upd_statusheader as
select distinct so.dd_SalesDocNo,
	case when dd_headerstatus like '%Partially%Open%' then 'Partially Open' 
	else (case when dd_headerstatus like '%Closed%' then ( case when dd_headerstatus like '%Open%' then 'Partially Open' else 'Closed' end) 
		  else ( case when dd_headerstatus like '%Open%' then 'Open' 
		  			else (case when dd_headerstatus like '%Cancelled%' then 'Cancelled' else 'N/a (Not Applicable)' end) 
		  		end)
		  end)
	end as dd_headerstatus1
from tmp_for_header so;

update fact_salesorder so
set so.dd_executionstatusheader = ifnull(t.dd_headerstatus1, 'Not Set')
from tmp_for_upd_statusheader t, fact_salesorder so
where so.dd_SalesDocNo = t.dd_SalesDocNo
and so.dd_executionstatusheader <> ifnull(t.dd_headerstatus1, 'Not Set');	

/* END Madalina 30 Jun 2016 */

UPDATE    fact_salesorder so
SET so.dd_SubsDocItemNo  =  ifnull(f.VBFA_POSNN,0)
FROM tmp_salesord_VBFA_upd f, fact_salesorder so
WHERE f.VBFA_VBTYP_N  =  'J'
AND  f.VBFA_VBELV  =  so.dd_SalesDocNo
AND f.VBFA_POSNV  =  so.dd_SalesItemNo
and f.rowseqno = 1
AND so.dd_SubsDocItemNo <> ifnull(f.VBFA_POSNN,0);


UPDATE    fact_salesorder so
SET so.Dim_SubsDocCategoryId  =  dc.Dim_DocumentCategoryid
FROM tmp_salesord_VBFA_upd f,Dim_DocumentCategory dc,fact_salesorder so
WHERE f.VBFA_VBTYP_N  =  'J'
AND  f.VBFA_VBELV  =  so.dd_SalesDocNo
AND f.VBFA_POSNV  =  so.dd_SalesItemNo
AND dc.DocumentCategory  =  f.VBFA_VBTYP_N
and f.rowseqno = 1
AND so.Dim_SubsDocCategoryId <> dc.Dim_DocumentCategoryid;

drop table if exists tmp_salesord_VBFA_upd;


/* LK : 29 Aug changes - amt_Subtotal3inCustConfig_Billing */

/* First sum amt_customerconfigsubtotal3 over docno+itemno */
DROP TABLE IF EXISTS tmp_sof_fb_amt_CustomerConfigSubtotal3;
CREATE TABLE tmp_sof_fb_amt_CustomerConfigSubtotal3
AS
SELECT dd_salesdocno, dd_salesitemno, sum(amt_customerconfigsubtotal3) sum_amt_customerconfigsubtotal3
FROM fact_billing fb, dim_billingdocumenttype bdt, dim_billingmisc bm
where fb.dim_documenttypeid = bdt.dim_billingdocumenttypeid
and fb.dim_billingmiscid = bm.dim_billingmiscid
and bm.cancelflag <> 'X'
AND bdt.type IN ('F1','F2','ZF2','ZF2C')
GROUP BY dd_salesdocno, dd_salesitemno;

/* Update amt_Subtotal3inCustConfig_Billing in sof for dd_salesdlvrdocno, dd_salesdlvritemno with minimum schedule no */
DROP TABLE IF EXISTS tmp_sof_sof_minsche;
CREATE TABLE tmp_sof_sof_minsche
AS
SELECT dd_salesdocno,dd_salesitemno,min(dd_scheduleno) min_dd_scheduleno
FROM fact_salesorder
GROUP BY dd_salesdocno,dd_salesitemno;

DROP TABLE IF EXISTS tmp_sof_amt_CustomerConfigSubtotal3_upd;
CREATE TABLE tmp_sof_amt_CustomerConfigSubtotal3_upd
AS
SELECT so.dd_salesdocno,so.dd_salesitemno,so.min_dd_scheduleno,fb.sum_amt_customerconfigsubtotal3
FROM tmp_sof_fb_amt_CustomerConfigSubtotal3 fb,tmp_sof_sof_minsche so
WHERE so.dd_salesdocno = fb.dd_salesdocno
AND so.dd_salesitemno = fb.dd_salesitemno;

UPDATE fact_salesorder so
SET amt_Subtotal3inCustConfig_Billing = 0
WHERE amt_Subtotal3inCustConfig_Billing <> 0;

UPDATE fact_salesorder so
SET so.amt_Subtotal3inCustConfig_Billing = ifnull(t.sum_amt_customerconfigsubtotal3, 0)
FROM tmp_sof_amt_CustomerConfigSubtotal3_upd t,fact_salesorder so
WHERE so.dd_salesdocno = t.dd_salesdocno
AND so.dd_salesitemno = t.dd_salesitemno
AND so.dd_scheduleno = t.min_dd_scheduleno
AND so.amt_Subtotal3inCustConfig_Billing <> ifnull(t.sum_amt_customerconfigsubtotal3, 0);

UPDATE fact_salesorder so
SET so.dd_SalesOrderBlocked = 'Not Set';

UPDATE fact_salesorder so
SET so.dd_SalesOrderBlocked = 'X'
   FROM dim_overallstatusforcreditcheck os,fact_salesorder so
WHERE so.Dim_OverallStatusCreditCheckId = os.dim_overallstatusforcreditcheckid
AND ( so.Dim_DeliveryBlockId <> 1 OR so.Dim_BillingBlockid <> 1
OR os.overallstatusforcreditcheck = 'B')
AND so.dd_SalesOrderBlocked <> 'X';



/* End of 29 Aug changes - amt_Subtotal3inCustConfig_Billing */

/* Delete 1 */

/* Delete 2 */

/* Delete 3 */

  DELETE FROM fact_salesorder
  WHERE dd_ScheduleNo = 0
        AND EXISTS
                (SELECT 1
                  FROM VBAK_VBAP
                  WHERE VBAP_VBELN = dd_SalesDocNo)
        AND NOT EXISTS
                    (SELECT 1
                      FROM VBAK_VBAP
                      WHERE VBAP_VBELN = dd_SalesDocNo
                            AND VBAP_POSNR = dd_SalesItemNo);


/* Delete 4	*/

DROP Table if exists tmp_MinSalesSchedules_0;
CREATE table tmp_MinSalesSchedules_0
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,min(dd_ScheduleNo) as MinScheduleNo
from facT_salesorder
where dd_ScheduleNo <> 0
group by dd_SalesDocNo,dd_SalesItemNo;

drop table if exists deletepart_702;
Create table deletepart_702
AS
Select *  FROM fact_salesorder f
  WHERE dd_ScheduleNo = 0
        AND EXISTS
                (SELECT 1 FROM tmp_MinSalesSchedules_0 t WHERE t.dd_SalesDocNo = f.dd_SalesDocNo
                  AND t.dd_SalesItemNo = f.dd_SalesItemNo);

/*call vectorwise ( combine 'fact_salesorder-deletepart_702')*/

MERGE INTO fact_salesorder i
USING deletepart_702 d ON i.fact_salesorderid = d.fact_salesorderid
WHEN MATCHED THEN DELETE;


merge into  fact_salesorder so
using (select distinct so.fact_salesorderid, first_value(cms.dim_Customermastersalesid) over (partition by  so.fact_salesorderid order by  so.fact_salesorderid) as dim_Customermastersalesid
    FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm, fact_salesorder so
 WHERE     so.dim_salesorgid = sorg.dim_salesorgid
       AND so.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = so.dim_salesdivisionid
       AND cm.dim_customerid = so.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber) t
on t.fact_salesorderid = so.fact_salesorderid
when matched then update set so.dim_Customermastersalesid = t.dim_Customermastersalesid
where so.dim_Customermastersalesid <> t.dim_Customermastersalesid;

/*
UPDATE fact_salesorder so
	SET so.dd_ProdOrderNo = ifnull(po.dd_OrderNumber,'Not Set')
FROM  fact_productionorder po,fact_salesorder so
WHERE  SO.dd_SalesDocNo = PO.dd_SalesOrderNo AND
	SO.dd_SalesItemNo = PO.dd_SalesOrderItemNo
AND ifnull(so.dd_ProdOrderNo,'X') <> ifnull(po.dd_OrderNumber,'Not Set')

UPDATE fact_salesorder so
SET so.dd_ProdOrderItemNo = ifnull(po.dd_OrderItemNo,0)
FROM  fact_productionorder po,fact_salesorder so
WHERE  SO.dd_SalesDocNo = PO.dd_SalesOrderNo AND
        SO.dd_SalesItemNo = PO.dd_SalesOrderItemNo
AND ifnull(so.dd_ProdOrderItemNo,-1) <>  ifnull(po.dd_OrderItemNo,0) */

DROP TABLE IF EXISTS tmp_fact_productionorder;
CREATE TABLE tmp_fact_productionorder AS 
select  PO.dd_OrderNumber,po.dd_OrderItemNo,PO.dd_SalesOrderNo,PO.dd_SalesOrderItemNo,PO.dd_SalesOrderDeliveryScheduleNo,
row_number () over (partition by PO.dd_SalesOrderNo,PO.dd_SalesOrderItemNo,PO.dd_SalesOrderDeliveryScheduleNo order by '') rowno
from fact_productionorder po;

UPDATE fact_salesorder so
SET so.dd_ProdOrderNo  =  ifnull(po.dd_OrderNumber,'Not Set')
FROM  fact_salesorder so, tmp_fact_productionorder po
WHERE  SO.dd_SalesDocNo  =  PO.dd_SalesOrderNo AND
	SO.dd_SalesItemNo  =  PO.dd_SalesOrderItemNo AND
	SO.dd_ScheduleNo  =  PO.dd_SalesOrderDeliveryScheduleNo AND
    PO.rowno = 1
AND so.dd_ProdOrderNo <> ifnull(po.dd_OrderNumber,'Not Set');


UPDATE fact_salesorder so
SET so.dd_ProdOrderItemNo  =  ifnull(po.dd_OrderItemNo,0)
FROM  fact_salesorder so, tmp_fact_productionorder po
WHERE  SO.dd_SalesDocNo  =  PO.dd_SalesOrderNo AND
        SO.dd_SalesItemNo  =  PO.dd_SalesOrderItemNo AND
        SO.dd_ScheduleNo  =  PO.dd_SalesOrderDeliveryScheduleNo AND
        PO.rowno = 1
AND so.dd_ProdOrderItemNo <> ifnull(po.dd_OrderItemNo,0);



UPDATE          fact_salesorder so
SET so.dd_CustomerMaterialNo = ifnull(vbk.VBAP_KDMAT,'Not Set')
FROM vbak_vbap_vbep vbk ,fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND  so.dd_scheduleno <> 0
AND  vbk.VBAP_KDMAT IS NOT NULL
AND so.dd_CustomerMaterialNo <> ifnull(vbk.VBAP_KDMAT,'Not Set');

UPDATE          fact_salesorder so
SET so.dd_CustomerMaterialNo = ifnull(vbk.VBAP_KDMAT,'Not Set')
FROM vbak_vbap vbk ,fact_salesorder so
Where vbk.VBAP_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND  so.dd_scheduleno = 0
AND  vbk.VBAP_KDMAT IS NOT NULL
AND so.dd_CustomerMaterialNo <> ifnull(vbk.VBAP_KDMAT,'Not Set');

UPDATE          fact_salesorder so
SET so.dd_CustomerMaterialNo = 'Not Set'
Where so.dd_CustomerMaterialNo IS NULL;




/* Start of the final update	*/
/*Georgiana 04 Oct 2016 -adding also an update for amt_UnitPriceUoM column according to BI-4332*/
update fact_salesorder 
set amt_UnitPriceUoM =CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END
From fact_salesorder ,vbak_vbap_vbep
where dd_SalesDocNo=VBAK_VBELN
and dd_SalesItemNo=VBAP_POSNR
and dd_ScheduleNo=VBEP_ETENR
and amt_UnitPriceUoM <> CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') and VBAP_NETPR > 0 and VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0)
			ELSE ifnull(vbap_netpr, 0)
		   END;

/* 04 Oct 2016 Georgiana End of changes*/

/*13 Jan 2017 Georgiana : Transforming all updates for fact_salesorderdelvery, the one without distinct condition, into one merge statement */
/*UPDATE fact_salesorderdelivery sd
  /*  SET sd.amt_Cost =
            Decimal(((CASE
                  WHEN f.amt_ExchangeRate < 0
                  THEN
                      (1 / (-1 * f.amt_ExchangeRate))
                  ELSE
                      f.amt_ExchangeRate
                END) * sd.amt_Cost_DocCurr),18,4) , */
/*	SET sd.amt_Cost = sd.amt_Cost_DocCurr,			--Not multiplying by local exchg rate. Stored as it is in doc/tran curr
        sd.ct_PriceUnit = f.ct_PriceUnit,
        sd.amt_UnitPrice = f.amt_UnitPrice,
        sd.amt_UnitPriceUoM = f.amt_UnitPriceUoM
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/

/*UPDATE fact_salesorderdelivery sd
    SET sd.amt_ExchangeRate = f.amt_ExchangeRate,
        sd.amt_ExchangeRate_GBL = f.amt_ExchangeRate_GBL
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET
        sd.Dim_DateidSalesOrderCreated = f.Dim_DateidSalesOrderCreated,
        sd.Dim_DateidSchedDeliveryReq = f.Dim_DateidSchedDeliveryReq
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_DateidSchedDlvrReqPrev = f.Dim_DateidSchedDlvrReqPrev,
        sd.Dim_DateidMatlAvailOriginal = f.Dim_DateidMtrlAvail
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
        WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_DateidFirstDate = f.Dim_DateidFirstDate,
        sd.Dim_Currencyid = f.Dim_Currencyid
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_TRA = f.Dim_Currencyid_TRA
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_GBL = f.Dim_Currencyid_GBL
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_STAT = f.Dim_Currencyid_STAT
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.amt_ExchangeRate_STAT = f.amt_ExchangeRate_STAT
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/

/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Companyid = f.Dim_Companyid,
        sd.Dim_SalesDivisionid = f.Dim_SalesDivisionid
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_ShipReceivePointid = f.Dim_ShipReceivePointid,
        sd.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_SalesDocumentTypeid = f.Dim_SalesDocumentTypeid,
        sd.Dim_SalesOrgid = f.Dim_SalesOrgid
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_SalesGroupid = f.Dim_SalesGroupid,
        sd.Dim_CostCenterid = f.Dim_CostCenterid
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/


/*UPDATE fact_salesorderdelivery sd
    SET sd.Dim_BillingBlockid = f.Dim_BillingBlockid,
        sd.Dim_TransactionGroupid = f.Dim_TransactionGroupid
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/

/*UPDATE fact_salesorderdelivery sd
    SET sd.dim_purchaseordertypeid = f.dim_purchaseordertypeid
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo*/

merge into fact_salesorderdelivery sd
using (select distinct fact_salesorderdeliveryid,sd.amt_Cost_DocCurr,f.ct_PriceUnit,f.amt_UnitPrice,f.amt_UnitPriceUoM,f.amt_ExchangeRate,f.amt_ExchangeRate_GBL,f.Dim_DateidSalesOrderCreated,
f.Dim_DateidSchedDeliveryReq,f.Dim_DateidSchedDlvrReqPrev,f.Dim_DateidMtrlAvail,f.Dim_DateidFirstDate,f.Dim_Currencyid,f.Dim_Currencyid_TRA,f.Dim_Currencyid_GBL,f.Dim_Currencyid_STAT,
f.amt_ExchangeRate_STAT,f.Dim_Companyid,f.Dim_SalesDivisionid,f.Dim_ShipReceivePointid,f.Dim_DocumentCategoryid,f.Dim_SalesDocumentTypeid,f.Dim_SalesOrgid,f.Dim_SalesGroupid,f.Dim_CostCenterid,
f.Dim_BillingBlockid,f.Dim_TransactionGroupid,f.dim_purchaseordertypeid,f.Dim_CustomerGroup1id
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo) f
on sd.fact_salesorderdeliveryid=f.fact_salesorderdeliveryid
when matched then update set
sd.amt_Cost = f.amt_Cost_DocCurr,		
sd.ct_PriceUnit = f.ct_PriceUnit,
sd.amt_UnitPrice = f.amt_UnitPrice,
sd.amt_UnitPriceUoM = f.amt_UnitPriceUoM,
sd.amt_ExchangeRate = f.amt_ExchangeRate,
sd.amt_ExchangeRate_GBL = f.amt_ExchangeRate_GBL,
sd.Dim_DateidSalesOrderCreated = f.Dim_DateidSalesOrderCreated,
sd.Dim_DateidSchedDeliveryReq = f.Dim_DateidSchedDeliveryReq,
sd.Dim_DateidSchedDlvrReqPrev = f.Dim_DateidSchedDlvrReqPrev,
sd.Dim_DateidMatlAvailOriginal = f.Dim_DateidMtrlAvail,
sd.Dim_DateidFirstDate = f.Dim_DateidFirstDate,
sd.Dim_Currencyid = f.Dim_Currencyid,
sd.Dim_Currencyid_TRA = f.Dim_Currencyid_TRA,
sd.Dim_Currencyid_GBL = f.Dim_Currencyid_GBL,
sd.Dim_Currencyid_STAT = f.Dim_Currencyid_STAT,
sd.amt_ExchangeRate_STAT = f.amt_ExchangeRate_STAT,
sd.Dim_Companyid = f.Dim_Companyid,
sd.Dim_SalesDivisionid = f.Dim_SalesDivisionid,
sd.Dim_ShipReceivePointid = f.Dim_ShipReceivePointid,
sd.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid,
sd.Dim_SalesDocumentTypeid = f.Dim_SalesDocumentTypeid,
sd.Dim_SalesOrgid = f.Dim_SalesOrgid,
sd.Dim_SalesGroupid = f.Dim_SalesGroupid,
sd.Dim_CostCenterid = f.Dim_CostCenterid,
sd.Dim_BillingBlockid = f.Dim_BillingBlockid,
sd.Dim_TransactionGroupid = f.Dim_TransactionGroupid,
sd.dim_purchaseordertypeid = f.dim_purchaseordertypeid,
sd.Dim_CustomerGroup1id = f.Dim_CustomerGroup1id;

/*13 Jan 2017 End of Changes*/


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_CustomeridSoldTo =
            CASE sd.Dim_CustomeridSoldTo
              WHEN 1 THEN f.Dim_CustomerID
              ELSE sd.Dim_CustomeridSoldTo
            END
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
		AND sd.Dim_CustomeridSoldTo <>
            CASE sd.Dim_CustomeridSoldTo
              WHEN 1 THEN f.Dim_CustomerID
              ELSE sd.Dim_CustomeridSoldTo
            END;


update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup4Id = so.Dim_MaterialPriceGroup4Id
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup4Id,1) <> so.Dim_MaterialPriceGroup4Id;

update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup5Id = so.Dim_MaterialPriceGroup5Id
 from fact_salesorder so,fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup5Id,1) <> so.Dim_MaterialPriceGroup5Id;

UPDATE fact_salesorderdelivery sd
    SET sd.Dim_CustomerGroup2id = f.Dim_CustomerGroup2id,
        sd.dim_salesorderitemcategoryid = f.dim_salesorderitemcategoryid,
        sd.Dim_ScheduleLineCategoryId = f.Dim_ScheduleLineCategoryId
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
  WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


/* Start Changes 03 Feb 2014 */

UPDATE fact_salesorder fso
SET dim_CustomerConditionGroups1id = cg.dim_CustomerConditionGroupsid
FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg,fact_salesorder fso
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND fso.dd_SalesItemNo = vp.VBKD_POSNR
AND VBKD_KDKG1 IS NOT NULL
AND cg.CustomerCondGrp = VBKD_KDKG1
AND ifnull(fso.dim_CustomerConditionGroups1id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);
update fact_salesorder set dim_CustomerConditionGroups1id = 1 where dim_CustomerConditionGroups1id is NULL;

UPDATE fact_salesorder fso
SET dim_CustomerConditionGroups2id = cg.dim_CustomerConditionGroupsid
FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg, fact_salesorder fso
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND fso.dd_SalesItemNo = vp.VBKD_POSNR
AND VBKD_KDKG2 IS NOT NULL
AND cg.CustomerCondGrp = VBKD_KDKG2
AND ifnull(fso.dim_CustomerConditionGroups2id,-1) <> ifnull(cg.dim_CustomerConditionGroupsid,1);

update fact_salesorder set dim_CustomerConditionGroups2id = 1 where dim_CustomerConditionGroups2id is NULL;

UPDATE fact_salesorder fso
SET dim_CustomerConditionGroups3id = cg.dim_CustomerConditionGroupsid
FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg, fact_salesorder fso
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
AND fso.dd_SalesItemNo = vp.VBKD_POSNR
AND VBKD_KDKG3 IS NOT NULL
AND cg.CustomerCondGrp = VBKD_KDKG3
AND fso.dim_CustomerConditionGroups3id <> ifnull(cg.dim_CustomerConditionGroupsid,1);

update fact_salesorder set dim_CustomerConditionGroups3id = 1 where dim_CustomerConditionGroups3id is NULL;

/* End Changes 03 Feb 2014 */



/* Start Changes 12 Feb 2014 */

UPDATE fact_salesorder so
SET so.dim_scheduledeliveryblockid =  db.Dim_DeliveryBlockId
FROM vbak_vbap_vbep vbk, Dim_DeliveryBlock db, fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND vbk.VBEP_LIFSP IS NOT NULL
AND db.DeliveryBlock = vbk.VBEP_LIFSP
AND db.RowIsCurrent = 1
and ifnull(so.dim_scheduledeliveryblockid,-1) <> ifnull(db.Dim_DeliveryBlockId,-2);

UPDATE fact_salesorder so SET so.dim_scheduledeliveryblockid = 1 WHERE so.dim_scheduledeliveryblockid is NULL;

/* End Changes 12 Feb 2014 */

/* Update all exchange rates */

/*Update local , gbl and stat rate from vbak_vbap_vbep*/


merge into fact_salesorder f
using (select f.fact_salesorderid, max(z.exchangeRate) AS exchangeRate
FROM VBAK_VBAP_VBEP, tmp_getexchangerate1 z, dim_currency tra, dim_currency loc, fact_salesorder f
WHERE VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
AND f.dim_currencyid_tra = tra.dim_currencyid
AND f.dim_currencyid = loc.dim_currencyid
AND z.pfromcurrency = tra.currencycode AND z.pfromcurrency = VBAP_WAERK and z.ptocurrency = loc.currencycode AND z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0
/*AND f.dim_currencyid_tra <> f.dim_currencyid*/
AND tra.dim_currencyid <> loc.dim_currencyid
AND f.amt_exchangerate = 1
group by f.fact_salesorderid) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set amt_exchangerate = t.exchangeRate
where  f.amt_exchangerate <> t.exchangeRate;


merge into fact_salesorder f
using (select f.fact_salesorderid, max(z.exchangeRate) AS exchangeRate
FROM VBAK_VBAP_VBEP, tmp_getexchangerate1 z, dim_currency gbl, fact_salesorder f
WHERE VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
AND f.dim_currencyid_gbl = gbl.dim_currencyid
AND z.pfromcurrency = VBAP_WAERK AND z.ptocurrency = gbl.currencycode AND z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0
AND gbl.currencycode <> VBAP_WAERK
AND f.amt_exchangerate_gbl = 1
group by f.fact_salesorderid) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set amt_exchangerate_gbl = t.exchangeRate
where  f.amt_exchangerate_gbl <> t.exchangeRate;

merge into fact_salesorder f
using (select f.fact_salesorderid, max(z.exchangeRate) AS exchangeRate
FROM VBAK_VBAP_VBEP, tmp_getexchangerate1 z, dim_currency tra, dim_currency stat,fact_salesorder f
WHERE VBAK_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo and VBEP_ETENR = dd_ScheduleNo
AND f.dim_currencyid_tra = tra.dim_currencyid
AND f.dim_currencyid_stat = stat.dim_currencyid
AND z.pfromcurrency = tra.currencycode AND  z.pfromcurrency = VBAP_WAERK AND z.ptocurrency = stat.currencycode AND z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0
/*AND f.dim_currencyid_tra <> f.dim_currencyid_stat*/
AND tra.dim_currencyid <> stat.dim_currencyid
AND f.amt_exchangerate_stat = 1
group by f.fact_salesorderid) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set amt_exchangerate_stat = t.exchangeRate
where  f.amt_exchangerate_stat <> t.exchangeRate;

/*Update local and gbl rate from vbak_vbap*/

merge into fact_salesorder f
using (select f.fact_salesorderid, max(z.exchangeRate) exchangeRate
FROM VBAK_VBAP, tmp_getexchangerate1 z, dim_currency tra, dim_currency loc, fact_salesorder f
WHERE VBAP_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo
AND f.dim_currencyid_tra = tra.dim_currencyid
AND f.dim_currencyid = loc.dim_currencyid
AND z.pfromcurrency = tra.currencycode AND z.pfromcurrency = VBAP_WAERK AND z.ptocurrency = loc.currencycode AND z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0
/*AND f.dim_currencyid_tra <> f.dim_currencyid*/
AND tra.dim_currencyid <> loc.dim_currencyid
AND f.amt_exchangerate = 1
group by f.fact_salesorderid
) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set amt_exchangerate = t.exchangeRate
where  f.amt_exchangerate <> t.exchangeRate;

merge into fact_salesorder f
using (select f.fact_salesorderid, max(z.exchangeRate) exchangeRate
FROM VBAK_VBAP, tmp_getexchangerate1 z, dim_currency gbl,fact_salesorder f
WHERE VBAP_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo
AND f.dim_currencyid_gbl = gbl.dim_currencyid
AND z.pfromcurrency = VBAP_WAERK AND z.ptocurrency = gbl.currencycode AND z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0
AND gbl.currencycode <> VBAP_WAERK
AND f.amt_exchangerate_gbl = 1
group by  f.fact_salesorderid
) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set amt_exchangerate_gbl = t.exchangeRate
where  f.amt_exchangerate_gbl <> t.exchangeRate;

merge into fact_salesorder f
using (select f.fact_salesorderid, max(z.exchangeRate) exchangeRate
FROM VBAK_VBAP, tmp_getexchangerate1 z, dim_currency stat, fact_salesorder f
WHERE VBAP_VBELN = dd_SalesDocNo and VBAP_POSNR = dd_SalesItemNo
AND f.dim_currencyid_stat = stat.dim_currencyid
AND z.pfromcurrency = VBAP_WAERK AND z.ptocurrency = stat.currencycode AND z.pDate = ifnull(PRSDT,VBAP_ERDAT) AND z.pFromExchangeRate = 0
AND stat.currencycode <> VBAP_WAERK
AND f.amt_exchangerate_stat = 1
group by  f.fact_salesorderid
) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set amt_exchangerate_stat = t.exchangeRate
where  f.amt_exchangerate_stat <> t.exchangeRate;


/* Take care of any remaining cases where exchange rates were not updated. If there are no issues in previous queries, this should update 0 rows*/

UPDATE fact_salesorder f
SET amt_exchangerate = z.exchangeRate
FROM tmp_getexchangerate1 z, dim_currency loc, dim_date dt, dim_currency tra, fact_salesorder f
WHERE f.dim_currencyid = loc.dim_currencyid
AND f.dim_currencyid_tra = tra.dim_currencyid
AND f.dim_dateidsalesordercreated = dt.dim_dateid
AND z.pfromcurrency = tra.currencycode AND z.ptocurrency = loc.currencycode AND z.pDate = dt.datevalue AND z.pFromExchangeRate = 0 and fact_script_name like '%bi_populate_salesorder_fact%'
AND f.amt_exchangerate = 1
AND loc.dim_currencyid <> tra.dim_currencyid
AND f.amt_exchangerate <> z.exchangeRate;


merge into fact_salesorder f
using (select distinct f.fact_salesorderid, z.exchangeRate
FROM tmp_getexchangerate1 z, dim_currency gbl, dim_date dt, dim_currency tra, fact_salesorder f,dim_plant pl
WHERE f.dim_currencyid_gbl = gbl.dim_currencyid
AND f.dim_currencyid_tra = tra.dim_currencyid
AND f.dim_dateidsalesordercreated = dt.dim_dateid
and f.dim_plantid=pl.dim_plantid
AND z.pfromcurrency = tra.currencycode AND z.ptocurrency = gbl.currencycode AND z.pDate = dt.datevalue 
and dt.plantcode_factory=pl.plantcode 
AND z.pFromExchangeRate = 0 and fact_script_name like '%bi_populate_salesorder_fact%'
AND f.amt_exchangerate_gbl = 1
AND gbl.dim_currencyid <> tra.dim_currencyid
AND f.amt_exchangerate_gbl <> z.exchangeRate) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set f.amt_exchangerate_gbl = t.exchangeRate;


merge into fact_salesorder f
using (select distinct f.fact_salesorderid, z.exchangeRate
FROM tmp_getexchangerate1 z, dim_currency stat,dim_date dt, dim_currency tra, fact_salesorder f
WHERE f.dim_currencyid_stat = stat.dim_currencyid
AND f.dim_currencyid_tra = tra.dim_currencyid
AND f.dim_dateidsalesordercreated = dt.dim_dateid
AND z.pfromcurrency = tra.currencycode AND z.ptocurrency = stat.currencycode AND z.pDate = dt.datevalue AND z.pFromExchangeRate = 0 and fact_script_name like '%bi_populate_salesorder_fact%'
AND f.amt_exchangerate_stat = 1
/*AND f.dim_currencyid_tra <> f.dim_currencyid_stat*/
AND stat.dim_currencyid <> tra.dim_currencyid
AND amt_exchangerate_stat <> z.exchangeRate) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set f.amt_exchangerate_stat = t.exchangeRate;


/* Start Changes 14 Feb 2014 */

UPDATE fact_salesorder so
set so.Dim_CustomerGroup4id = ifnull(cg4.Dim_CustomerGroup4id,1)
FROM vbak_vbap_vbep vbk, Dim_CustomerGroup4 cg4, fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND cg4.CustomerGroup = VBAK_KVGR4
and ifnull(so.Dim_CustomerGroup4id,-1) <> ifnull(cg4.Dim_CustomerGroup4id,-2);

update fact_salesorder set Dim_CustomerGroup4id = 1 where Dim_CustomerGroup4id is NULL;

/* End Changes 14 Feb 2014 */

/* Ensure subsequent items have the correct Dim_CustomeridShipTo*/
/*Georgiana 12 Jul 2016 DROP TABLE IF EXISTS fact_salesorder_tmp_Dim_CustomeridShipToFix
CREATE TABLE fact_salesorder_tmp_Dim_CustomeridShipToFix
AS
SELECT DISTINCT f1.dd_salesdocno,f1.dd_salesitemno,f1.Dim_CustomeridShipTo,f2.Dim_CustomeridShipTo new_Dim_CustomeridShipTo
FROM fact_salesorder f1, fact_salesorder f2
WHERE f1.dd_salesdocno = f2.dd_salesdocno
AND f1.Dim_CustomeridShipTo = 1
AND f2.Dim_CustomeridShipTo <> 1

UPDATE fact_salesorder f
FROM fact_salesorder_tmp_Dim_CustomeridShipToFix b
SET f.Dim_CustomeridShipTo =  b.new_Dim_CustomeridShipTo
WHERE f.dd_salesdocno = b.dd_salesdocno
AND f.dd_salesitemno = b.dd_salesitemno
AND f.Dim_CustomeridShipTo = 1
AND  b.new_Dim_CustomeridShipTo <> 1
AND f.Dim_CustomeridShipTo <> b.new_Dim_CustomeridShipTo*/

/*Begin 26-Oct-2015 Andrian Added new Expiry Date according BI-1267*/
update fact_salesorder
SET dim_DateidExpiryDate = 1
WHERE dim_DateidExpiryDate <> 1;

drop table if exists tmp_fact_salesorder_upd_expdate;
create table tmp_fact_salesorder_upd_expdate as
	select  dt1.dim_dateid,
	(case when m.mch1_vfdat='9999-12-31' then '0001-01-01' else m.mch1_vfdat end) mch1_vfdat ,
	i.dim_Partid,
	i.dd_BatchNo,
	i.dim_companyid,
	ROW_NUMBER() OVER(PARTITION BY i.dim_Partid,i.dd_BatchNo,i.dim_companyid
				order by m.mch1_vfdat desc)  RowSeqNo
	from
	fact_salesorder i, mch1 m,
	dim_Part dp1,
	dim_company dc,
	dim_date dt1
where i.dim_Partid = dp1.dim_Partid
	and dp1.partnumber =  m.mch1_matnr
	and i.dd_BatchNo = m.mch1_charg
	and dt1.datevalue = m.mch1_vfdat and dt1.plantcode_factory=dp1.plant
	and dt1.CompanyCode = dc.CompanyCode
	and i.dim_companyid = dc.dim_companyid;

update fact_salesorder i
set dim_DateidExpiryDate = u.dim_dateid
	,i.dw_update_date = current_timestamp
from	tmp_fact_salesorder_upd_expdate u, fact_salesorder i
where i.dim_Partid = u.dim_Partid
	and i.dd_BatchNo = u.dd_BatchNo
	and i.dim_companyid = u.dim_companyid
	and u.RowSeqNo = 1
	and dim_DateidExpiryDate <> u.dim_dateid;

/*End 26-Oct-2015 Andrian Added new Expiry Date according BI-1267*/

/* Octavian: Every Angle Transition Addons */
update fact_salesorder f
SET amt_grossweight_marm = ifnull(MARM_BRGEW,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_part dp,MARM m,dim_unitofmeasure uom,fact_salesorder f
where m.MARM_MATNR = dp.partnumber
and dp.dim_partid = f.dim_partid
and m.MARM_MEINH = uom.uom
and f.dim_unitofmeasureid = uom.dim_unitofmeasureid
and f.amt_grossweight_marm <> ifnull(MARM_BRGEW,0);

update fact_salesorder f
SET f.dd_batchno = ifnull(v.VBAP_CHARG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbep v, fact_salesorder f
WHERE f.dd_SalesDocNo = v.VBAK_VBELN
AND f.dd_SalesItemNo = v.VBAP_POSNR
AND f.dd_ScheduleNo = v.VBEP_ETENR
AND f.dd_batchno <> ifnull(v.VBAP_CHARG,'Not Set');

/* This is to ensure that if the value is changed to null, it will be updated for dim_batchid as well */
/* Madalina 19 Aug 2016 - Reset of dim_batchid is handled in dim_batch dimension script - BI-3822 */
/*update fact_salesorder f
SET dim_batchid = 1
WHERE dim_batchid <> 1 */

update fact_salesorder ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp, fact_salesorder ia
where ia.dim_partid = dp.dim_partid and
ia.dd_batchno = b.batchnumber and
dp.partnumber = b.partnumber and
dp.plant = b.plantcode
and ia.dim_batchid <> b.dim_batchid;

/*Ambiguous replace fix*/
/*UPDATE fact_salesorder s
SET s.dim_materiallistingid = ml.dim_materiallistingid,
dw_update_date = current_timestamp
FROM dim_part p, dim_customer c , dim_salesorg o , dim_DistributionChannel dc, dim_materiallisting ml, fact_salesorder s
where s.dim_partid=p.dim_partid
and ml.materialnumber=p.partnumber
and s.dim_customerid=c.dim_customerid
and ml.customer=c.customernumber
and ml.matlisting=c.CustAccountGroup
and s.dim_salesorgid=o.dim_salesorgid
and ml.salesorg=o.salesorgcode
and s.dim_DistributionChannelid = dc.dim_DistributionChannelid
and ml.distributionchannel=dc.distributionchannelcode
and s.dim_materiallistingid <> ml.dim_materiallistingid*/

drop table if exists tmp_for_upd_matlistid;
create table tmp_for_upd_matlistid as
select distinct s.fact_salesorderid, ml.dim_materiallistingid, row_number() over (partition by s.fact_salesorderid order by s.fact_salesorderid) as rowseqno
FROM dim_part p, dim_customer c , dim_salesorg o , dim_DistributionChannel dc, dim_materiallisting ml, fact_salesorder s
where s.dim_partid=p.dim_partid
and ml.materialnumber=p.partnumber
and s.dim_customerid=c.dim_customerid
and ml.customer=c.customernumber
and ml.matlisting=c.CustAccountGroup
and s.dim_salesorgid=o.dim_salesorgid
and ml.salesorg=o.salesorgcode
and s.dim_DistributionChannelid = dc.dim_DistributionChannelid
and ml.distributionchannel=dc.distributionchannelcode
and s.dim_materiallistingid <> ml.dim_materiallistingid;


UPDATE fact_salesorder s
SET s.dim_materiallistingid = ml.dim_materiallistingid,
dw_update_date = current_timestamp
FROM tmp_for_upd_matlistid ml, fact_salesorder s
where s.fact_salesorderid=ml.fact_salesorderid
and rowseqno=1;

/* Madalina 14 Sep 2016 - Adding ifnulls for werks conditions - BI-3996 */
UPDATE fact_salesorder so
SET so.dd_numerator = ifnull(vbk.VBAP_UMVKZ,0),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbep vbk, dim_plant pl, fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = ifnull(vbk.VBAP_WERKS, 'Not Set') AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0
AND so.dd_numerator <> ifnull(vbk.VBAP_UMVKZ,0);

/* Madalina 8 Sep 2016 - update Numerator field using VBAK_VBAP - BI-3996 */
UPDATE fact_salesorder so
SET so.dd_numerator = ifnull(vbk.VBAP_UMVKZ,0),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap vbk, dim_plant pl, fact_salesorder so
where vbk.VBAP_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = ifnull(vbk.VBAP_WERKS, 'Not Set') AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo = 0
AND so.dd_numerator <> ifnull(vbk.VBAP_UMVKZ,0);
/* END BI-3996 */

UPDATE    fact_salesorder so
SET so.dim_dateidpricing  =  dt.dim_dateid
	,so.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbkd vkd, dim_date dt, dim_company dc, fact_salesorder so, dim_plant pl
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.dim_companyid  =  dc.dim_companyid
and so.dim_plantid=pl.dim_plantid
AND dt.datevalue  =  ifnull(vkd.VBKD_PRSDT,'0001-01-01')
AND dt.companycode  =  dc.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.dim_dateidorginalrequestdate <> dt.dim_dateid;

UPDATE fact_salesorder so
SET so.dim_customerpurchaseordertypeid = cpt.dim_customerpurchaseordertypeid,
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd, dim_customerpurchaseordertype cpt, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND cpt.CustomerPOType = ifnull(vkd.VBKD_BSARK,'Not Set')
AND so.dim_customerpurchaseordertypeid <> cpt.dim_customerpurchaseordertypeid;

update fact_salesorder so
SET so.dim_datecustomerpurchaseordertypeid = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
from vbak_vbap_vbkd vkd, dim_date dt, dim_company dc, fact_salesorder so, dim_plant pl
WHERE so.dd_SalesDocNo = vkd.VBKD_VBELN
AND so.dd_SalesItemNo = vkd.VBKD_POSNR
AND dt.datevalue = ifnull(vkd.vbkd_BSTDK,'0001-01-01')
AND so.dim_companyid = dc.dim_companyid
and so.dim_plantid=pl.dim_plantid
AND dt.companycode = dc.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.dim_datecustomerpurchaseordertypeid <> dt.dim_dateid;

UPDATE fact_salesorder so
SET so.dd_purchordernumbershiptoparty = ifnull(vkd.vbkd_bstkd_e,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd,  fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.dd_purchordernumbershiptoparty <> ifnull(vkd.vbkd_bstkd_e,'Not Set');

/* Andrei R - APP-9349 */
UPDATE fact_salesorder so
SET so.dd_purchordernumbershiptoparty = ifnull(vkd.vbkd_bstkd_e,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd,  fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
and   vkd.VBKD_POSNR = '000000'
and so.dd_purchordernumbershiptoparty = 'Not Set'
AND so.dd_purchordernumbershiptoparty <> ifnull(vkd.vbkd_bstkd_e,'Not Set');
/* end Andrei R - APP-9349 */


UPDATE fact_salesorder so
SET so.dd_urreference = ifnull(vkd.vbkd_ihrez,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.dd_urreference <> ifnull(vkd.vbkd_ihrez,'Not Set');

/*Alin 20 nov 2017 - logic change acc to APP-8019*/
UPDATE fact_salesorder so
SET so.dd_urreference = ifnull(vkd.vbkd_ihrez,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM VBAK_VBAP_VBKD vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  ifnull(vkd.VBKD_VBELN, 'Not Set')
AND so.dd_SalesItemNo  =  0
and vbkd_posnr = '000000'
AND so.dd_urreference <> ifnull(vkd.vbkd_ihrez,'Not Set');

UPDATE fact_salesorder so
SET so.dd_urreference = ifnull(vkd.vbkd_ihrez,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM VBAK_VBAP_VBKD vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  ifnull(vkd.VBKD_VBELN, 'Not Set')
AND so.dd_SalesItemNo  =  0
and vbkd_posnr = '000010'
AND so.dd_urreference <> ifnull(vkd.vbkd_ihrez,'Not Set');

UPDATE fact_salesorder so
SET so.dd_urreference = ifnull(vkd.vbkd_ihrez,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM VBAK_VBAP_VBKD vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  ifnull(vkd.VBKD_VBELN, 'Not Set')
AND so.dd_SalesItemNo  =  10
and vbkd_posnr = '000000'
AND so.dd_urreference <> ifnull(vkd.vbkd_ihrez,'Not Set');

UPDATE fact_salesorder so
SET so.dd_urreference = ifnull(vkd.vbkd_ihrez,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM VBAK_VBAP_VBKD vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  ifnull(vkd.VBKD_VBELN, 'Not Set')
AND so.dd_SalesItemNo  =  10
and vbkd_posnr = '000010'
AND so.dd_urreference <> ifnull(vkd.vbkd_ihrez,'Not Set');
UPDATE fact_salesorder so
SET so.dd_incoterms2 = ifnull(vkd.vbkd_inco2,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.dd_incoterms2 <> ifnull(vkd.vbkd_inco2,'Not Set');

/* Madalina 21 Jul 2016 - Add Incoterms (Part 2) (Sales Document Header) - dd_incoterms2Header, based on dd_SalesDocNo only - BI-3360*/
UPDATE fact_salesorder so
SET so.dd_incoterms2Header = ifnull(vkd.vbkd_inco2,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND vkd.VBKD_POSNR  =  0
AND so.dd_incoterms2Header <> ifnull(vkd.vbkd_inco2,'Not Set');
/* END 21 Jul 2016 */

UPDATE fact_salesorder so
SET so.dim_CustomerGroup_vbkd_ID = cg.Dim_CustomerGroupId,
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd,dim_CustomerGroup cg, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND cg.CustomerGroup = ifnull(vkd.vbkd_kdgrp,'Not Set')
AND so.dim_CustomerGroup_vbkd_ID <> cg.Dim_CustomerGroupId;

UPDATE fact_salesorder so
SET so.ct_additionaldays = ifnull(vkd.vbkd_valtg,0),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.ct_additionaldays <> ifnull(vkd.vbkd_valtg,0);

UPDATE fact_salesorder so
SET so.dim_tranportbyshippingtype_vbkd_id = tst.dim_tranportbyshippingtypeid,
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd,dim_tranportbyshippingtype tst, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND tst.shippingtype = ifnull(vkd.vbkd_vsart,'Not Set')
AND so.dim_tranportbyshippingtype_vbkd_id <> tst.dim_tranportbyshippingtypeid;

UPDATE fact_salesorder so
SET so.dim_tranportbyshippingtype_vbkd_id = tst.dim_tranportbyshippingtypeid,
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd,dim_tranportbyshippingtype tst, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND VBKD_POSNR = 0
AND tst.shippingtype = ifnull(vkd.vbkd_vsart,'Not Set')
AND so.dim_tranportbyshippingtype_vbkd_id = 1
AND vkd.vbkd_vsart is not null
AND so.dim_tranportbyshippingtype_vbkd_id <> tst.dim_tranportbyshippingtypeid;


UPDATE fact_salesorder so
SET so.Dim_CustomerPaymentTerms_vbkd_Id  = cpt.dim_Customerpaymenttermsid,
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd,dim_customerpaymentterms cpt, fact_salesorder so
WHERE
so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND cpt.PaymentTermCode  =  ifnull(vkd.VBKD_ZTERM,'Not Set')
AND so.Dim_CustomerPaymentTerms_vbkd_Id  <> cpt.dim_Customerpaymenttermsid;

UPDATE fact_salesorder fso
SET fso.dd_term_product_proposal = ifnull(vkp.vbak_ktext,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
AND fso.dd_term_product_proposal <> ifnull(vkp.vbak_ktext,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_term_product_proposal = ifnull(vkp.vbak_ktext,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
AND fso.dd_term_product_proposal <> ifnull(vkp.vbak_ktext,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_referencedocno_xblnr = ifnull(vkp.vbak_xblnr,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
AND fso.dd_referencedocno_xblnr <> ifnull(vkp.vbak_xblnr,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_referencedocno_xblnr = ifnull(vkp.vbak_xblnr,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
AND fso.dd_referencedocno_xblnr <> ifnull(vkp.vbak_xblnr,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_shorttext = ifnull(vkp.vbap_arktx,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
AND fso.dd_shorttext <> ifnull(vkp.vbap_arktx,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_shorttext = ifnull(vkp.vbap_arktx,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
AND fso.dd_shorttext <> ifnull(vkp.vbap_arktx,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_requirementstypes = ifnull(vkp.vbap_bedae,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
AND fso.dd_requirementstypes <> ifnull(vkp.vbap_bedae,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_requirementstypes = ifnull(vkp.vbap_bedae,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
AND fso.dd_requirementstypes <> ifnull(vkp.vbap_bedae,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_originatingitem = ifnull(vkp.vbap_posnv,0),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
AND fso.dd_originatingitem <> ifnull(vkp.vbap_posnv,0);

UPDATE fact_salesorder fso
SET fso.dd_originatingitem = ifnull(vkp.vbap_posnv,0),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
AND fso.dd_originatingitem <> ifnull(vkp.vbap_posnv,0);

UPDATE fact_salesorder fso
SET fso.dd_originatingdoc = ifnull(vkp.vbap_vbelv,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
AND fso.dd_originatingdoc <> ifnull(vkp.vbap_vbelv,'Not Set');

UPDATE fact_salesorder fso
SET fso.dd_originatingdoc = ifnull(vkp.vbap_vbelv,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
AND fso.dd_originatingdoc <> ifnull(vkp.vbap_vbelv,'Not Set');

UPDATE fact_salesorder fso
SET fso.dim_original_req_dateid = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, dim_plant pl, dim_Date dt, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
  AND dt.datevalue = ifnull(vkp.vbap_zz1edatu,'0001-01-01')
  AND pl.plantcode = ifnull(vkp.VBAP_WERKS,'Not Set')
  AND dt.CompanyCode = pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND fso.dim_original_req_dateid <> dt.dim_dateid;

UPDATE fact_salesorder fso
SET fso.dim_original_req_dateid = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, dim_plant pl, dim_Date dt, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
  AND dt.datevalue = ifnull(vkp.vbap_zz1edatu,'0001-01-01')
  AND pl.plantcode = ifnull(vkp.VBAP_WERKS,'Not Set')
  AND dt.CompanyCode = pl.CompanyCode
  AND dt.plantcode_factory=pl.plantcode
AND fso.dim_original_req_dateid <> dt.dim_dateid;


UPDATE fact_salesorder fso
SET fso.ct_original_req = ifnull(vkp.vbap_zz1wmeng,0),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
AND fso.ct_original_req <> ifnull(vkp.vbap_zz1wmeng,0);

UPDATE fact_salesorder fso
SET fso.ct_original_req = ifnull(vkp.vbap_zz1wmeng,0),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
AND fso.ct_original_req <> ifnull(vkp.vbap_zz1wmeng,0);

UPDATE fact_salesorder fso
SET fso.dim_first_promised_dateid = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, dim_plant pl, dim_Date dt, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
  AND dt.datevalue = ifnull(vkp.vbap_zz2edatu,'0001-01-01')
  AND pl.plantcode = ifnull(vkp.VBAP_WERKS,'Not Set')
  AND dt.CompanyCode = pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND fso.dim_first_promised_dateid <> dt.dim_dateid;

/* Update 15 FEBRUARY 2016 - OSTOIAN - UPDATE FOR VBEP_MEINS */

Update fact_salesorder fo
   SET  dim_baseunitofmeasureid  =   uom.Dim_UnitOfMeasureId,
   dw_update_date = CURRENT_TIMESTAMP
  FROM VBAK_VBAP_VBEP, Dim_UnitOfMeasure uom, fact_salesorder fo
where   fo.dd_SalesDocNo  =  VBAK_VBELN
    AND fo.dd_SalesItemNo  =  VBAP_POSNR
    AND fo.dd_ScheduleNo  =  VBEP_ETENR
    AND vbap_meins IS NOT NULL
    AND uom.UOM  =  vbap_meins
    AND uom.RowIsCurrent  =  1
AND dim_baseunitofmeasureid <> uom.Dim_UnitOfMeasureId;

/* END OF CHANGES 15 FEBRUARY 2016 OSTOIAN */


/* Liviu I - change dim_original_req_dateid to dim_first_promised_dateid - BI-3417 */
UPDATE fact_salesorder fso
SET fso.dim_first_promised_dateid = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, dim_plant pl, dim_Date dt, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
  AND dt.datevalue = ifnull(vkp.vbap_zz2edatu,'0001-01-01')
  AND pl.plantcode = ifnull(vkp.VBAP_WERKS,'Not Set')
  AND dt.CompanyCode = pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND fso.dim_first_promised_dateid <> dt.dim_dateid;

UPDATE fact_salesorder fso
SET fso.ct_first_promised__req = ifnull(vkp.vbap_zz2wmeng,0),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  0
AND fso.ct_first_promised__req <> ifnull(vkp.vbap_zz2wmeng,0);

UPDATE fact_salesorder fso
SET fso.ct_first_promised__req = ifnull(vkp.vbap_zz2wmeng,0),
dw_update_date = CURRENT_TIMESTAMP
  FROM vbak_vbap_vbep vkp, fact_salesorder fso
WHERE fso.dd_SalesDocNo  =  vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo  =  vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo  =  vkp.VBEP_ETENR
  AND fso.dd_ScheduleNo <> 0
AND fso.ct_first_promised__req <> ifnull(vkp.vbap_zz2wmeng,0);

/*
update fact_salesorder so
SET so.dim_dateidvalidto_leadtime = dd.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
from Z1SD_LEADTIME z,
dim_customer c, dim_salesorg sorg, dim_part dp, dim_date dd, dim_plant dpl, fact_salesorder so
where z.Z1SD_LEADTIME_KUNNR = c.customernumber
and so.dim_customerid = c.dim_customerid
and z.Z1SD_LEADTIME_MATNR = dp.partnumber
and so.dim_partid = dp.dim_partid
and z.Z1SD_LEADTIME_VKORG = sorg.salesorgcode
and so.dim_salesorgid = sorg.dim_salesorgid
and dd.datevalue = ifnull(Z1SD_LEADTIME_DATBI,'0001-01-01')
and dd.companycode = dpl.companycode and dd.plantcode_factory=dpl.plantcode
and dpl.plantcode = dp.plant
and so.dim_dateidvalidto_leadtime <> dd.dim_dateid
*/

merge into fact_salesorder so
using
(
	select distinct fact_salesorderid, dd.dim_dateid
	from Z1SD_LEADTIME z,
	dim_customer c, dim_salesorg sorg, dim_part dp, dim_date dd, dim_plant dpl, fact_salesorder so
	where z.Z1SD_LEADTIME_KUNNR = c.customernumber
	and so.dim_customerid = c.dim_customerid
	and z.Z1SD_LEADTIME_MATNR = dp.partnumber
	and so.dim_partid = dp.dim_partid
	and z.Z1SD_LEADTIME_VKORG = sorg.salesorgcode
	and so.dim_salesorgid = sorg.dim_salesorgid
	and dd.datevalue = ifnull(Z1SD_LEADTIME_DATBI,'0001-01-01')
	and dd.companycode = dpl.companycode and dd.plantcode_factory=dpl.plantcode
	and dpl.plantcode = dp.plant
	and so.dim_dateidvalidto_leadtime <> dd.dim_dateid
)x
on so.fact_salesorderid = x.fact_salesorderid
when matched then update set so.dim_dateidvalidto_leadtime = x.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

drop table if exists KOTG992_filterDATE;
create table KOTG992_filterDATE
as select distinct * from KOTG992
where kotg992_datbi >= current_date AND kotg992_datab <=current_date;

update fact_salesorder fso
SET fso.dim_dateidvalidstartdateid_kotg992 = dd.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
from KOTG992_filterDATE k, dim_part dp, dim_salesorg so, dim_DistributionChannel dc, dim_CustomerGroup cg,
dim_date dd, fact_salesorder fso,dim_plant pl
WHERE
dp.partnumber = ifnull(k.KOTG992_MATNR,'Not Set') AND
dp.dim_partid = fso.dim_partid AND
so.SalesOrgCode = ifnull(k.KOTG992_VKORG,'Not Set') AND
so.dim_salesorgid = fso.dim_salesorgid AND
dc.DistributionChannelCode = ifnull(k.KOTG992_VTWEG,'Not Set') AND
dc.Dim_DistributionChannelId = fso.Dim_DistributionChannelId AND
cg.CustomerGroup  =  ifnull(k.KOTG992_KDGRP,'Not Set') AND
cg.Dim_CustomerGroupId = fso.Dim_CustomerGroupId AND
dd.datevalue = ifnull(k.KOTG992_DATAB,'0001-01-01') AND
fso.dim_plantid=pl.dim_plantid and
dd.companycode = pl.companycode AND dd.plantcode_factory=pl.plantcode AND
fso.dim_dateidvalidstartdateid_kotg992 <> dd.dim_dateid;

update fact_salesorder fso
SET fso.dim_dateidvalidenddateid_kotg992 = dd.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
from KOTG992_filterDATE k, dim_part dp, dim_salesorg so, dim_DistributionChannel dc, dim_CustomerGroup cg,
dim_date dd,fact_salesorder fso,dim_plant pl
WHERE
dp.partnumber = ifnull(k.KOTG992_MATNR,'Not Set') AND
dp.dim_partid = fso.dim_partid AND
so.SalesOrgCode = ifnull(k.KOTG992_VKORG,'Not Set') AND
so.dim_salesorgid = fso.dim_salesorgid AND
dc.DistributionChannelCode = ifnull(k.KOTG992_VTWEG,'Not Set') AND
dc.Dim_DistributionChannelId = fso.Dim_DistributionChannelId AND
cg.CustomerGroup  =  ifnull(k.KOTG992_KDGRP,'Not Set') AND
cg.Dim_CustomerGroupId = fso.Dim_CustomerGroupId AND
fso.dim_plantid=pl.dim_plantid and
dd.datevalue = ifnull(k.KOTG992_DATBI,'0001-01-01') AND
dd.companycode = pl.companycode AND dd.plantcode_factory=pl.plantcode AND
fso.dim_dateidvalidenddateid_kotg992 <> dd.dim_dateid;

update fact_salesorder fso
SET fso.dim_customergroupid_kotg992 = cg.dim_customergroupid,
dw_update_date = CURRENT_TIMESTAMP
from KOTG992_filterDATE k, dim_part dp, dim_salesorg so, dim_DistributionChannel dc, dim_CustomerGroup cg,fact_salesorder fso
WHERE
dp.partnumber = ifnull(k.KOTG992_MATNR,'Not Set') AND
dp.dim_partid = fso.dim_partid AND
so.SalesOrgCode = ifnull(k.KOTG992_VKORG,'Not Set') AND
so.dim_salesorgid = fso.dim_salesorgid AND
dc.DistributionChannelCode = ifnull(k.KOTG992_VTWEG,'Not Set') AND
dc.Dim_DistributionChannelId = fso.Dim_DistributionChannelId AND
cg.CustomerGroup  =  ifnull(k.KOTG992_KDGRP,'Not Set') AND
cg.Dim_CustomerGroupId = fso.Dim_CustomerGroupId AND
fso.dim_customergroupid_kotg992 <> cg.dim_customergroupid;


update fact_salesorder fso
SET fso.dim_distributionchanelcodeid_kotg992 = dc.dim_distributionchannelid,
dw_update_date = CURRENT_TIMESTAMP
from KOTG992_filterDATE k, dim_part dp, dim_salesorg so, dim_DistributionChannel dc, dim_CustomerGroup cg,fact_salesorder fso
WHERE
dp.partnumber = ifnull(k.KOTG992_MATNR,'Not Set') AND
dp.dim_partid = fso.dim_partid AND
so.SalesOrgCode = ifnull(k.KOTG992_VKORG,'Not Set') AND
so.dim_salesorgid = fso.dim_salesorgid AND
dc.DistributionChannelCode = ifnull(k.KOTG992_VTWEG,'Not Set') AND
dc.Dim_DistributionChannelId = fso.Dim_DistributionChannelId AND
cg.CustomerGroup  =  ifnull(k.KOTG992_KDGRP,'Not Set') AND
cg.Dim_CustomerGroupId = fso.Dim_CustomerGroupId AND
fso.dim_distributionchanelcodeid_kotg992 <> dc.dim_distributionchannelid;


drop table if exists KOTG992_filterDATE;

drop table if exists a055_datefiltered;
create table a055_datefiltered
as select *
from a055
where a055_datbi >= current_date AND a055_datab <=current_date;

update fact_salesorder fso
set dim_salesorgid_a055 = sg.dim_salesorgid
from a055_datefiltered a, dim_salesorg sg, dim_plant pl, dim_part dp,dim_company dc,fact_salesorder fso
where
sg.SalesOrgCode = ifnull(a.A055_VKORGAU, 'Not Set')
and sg.dim_salesorgid = fso.dim_salesorgid
and pl.PlantCode  =  ifnull(A055_WERKS, 'Not set')
and dc.CompanyCode  =  pl.CompanyCode
and fso.dim_plantid=pl.dim_plantid
and dp.partnumber = ifnull(a.a055_MATNR,'Not Set')
and dp.dim_partid = fso.dim_partid;

drop table if exists a055_datefiltered;


update fact_salesorder fso
SET fso.dd_matnrbycustomer = ifnull(k.KNMT_KDMAT,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from KNMT k, dim_part dp, dim_salesorg so, dim_DistributionChannel dc, dim_customer c,  fact_salesorder fso
WHERE
dp.partnumber = ifnull(k.KNMT_MATNR,'Not Set') AND
dp.dim_partid = fso.dim_partid AND
so.SalesOrgCode = ifnull(k.KNMT_VKORG,'Not Set') AND
so.dim_salesorgid = fso.dim_salesorgid AND
dc.DistributionChannelCode = ifnull(k.KNMT_VTWEG,'Not Set') AND
dc.Dim_DistributionChannelId = fso.Dim_DistributionChannelId AND
c.customernumber = ifnull(k.KNMT_KUNNR,'Not Set') AND
c.dim_customerid = fso.dim_customerid AND
fso.dd_matnrbycustomer <> ifnull(k.KNMT_KDMAT,'Not Set');

update fact_salesorder fso
SET fso.dd_matnrdescripbycustomer = ifnull(k.KNMT_POSTX,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from KNMT k, dim_part dp, dim_salesorg so, dim_DistributionChannel dc, dim_customer c, fact_salesorder fso
WHERE
dp.partnumber = ifnull(k.KNMT_MATNR,'Not Set') AND
dp.dim_partid = fso.dim_partid AND
so.SalesOrgCode = ifnull(k.KNMT_VKORG,'Not Set') AND
so.dim_salesorgid = fso.dim_salesorgid AND
dc.DistributionChannelCode = ifnull(k.KNMT_VTWEG,'Not Set') AND
dc.Dim_DistributionChannelId = fso.Dim_DistributionChannelId AND
c.customernumber = ifnull(k.KNMT_KUNNR,'Not Set') AND
c.dim_customerid = fso.dim_customerid AND
fso.dd_matnrdescripbycustomer <> ifnull(k.KNMT_POSTX,'Not Set');
/* Octavian: Every Angle Transition Addons */

/*Georgiana 16 Mar 2016 Adding dim_partsalesid*/
/*UPDATE fact_salesorder fso
SET fso.Dim_PartSalesId = sprt.Dim_PartSalesId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl
FROM VBAK_VBAP vp, Dim_PartSales sprt, fact_salesorder fso
where   fso.dd_SalesDocNo = vp.VBAP_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = 0
AND VBAK_VKORG IS NOT NULL
AND sprt.SalesOrgCode = VBAK_VKORG
AND sprt.PartNumber = VBAP_MATNR
AND sprt.RowIsCurrent = 1
AND fso.Dim_PartSalesId <> sprt.Dim_PartSalesId*/

/*/Ambiguous Replace fix: Adding also distibution channel code in join*/

drop table if exists tmp_for_upd_Dim_PartSalesId;
create table tmp_for_upd_Dim_PartSalesId as
select distinct sprt.Dim_PartSalesId, fso.dd_SalesDocNo, fso.dd_SalesItemNo
FROM VBAK_VBAP vp, Dim_PartSales sprt, fact_salesorder fso, dim_DistributionChannel dc
where   fso.dd_SalesDocNo = vp.VBAP_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = 0
AND VBAK_VKORG IS NOT NULL
AND sprt.SalesOrgCode = VBAK_VKORG
AND sprt.PartNumber = VBAP_MATNR
and dc.DistributionChannelCode = sprt.distributionchannelcode and
dc.Dim_DistributionChannelId = fso.Dim_DistributionChannelId
AND sprt.RowIsCurrent = 1
AND fso.Dim_PartSalesId <> sprt.Dim_PartSalesId;

UPDATE fact_salesorder fso
SET fso.Dim_PartSalesId = sprt.Dim_PartSalesId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_for_upd_Dim_PartSalesId sprt,  fact_salesorder fso
where   fso.dd_SalesDocNo = sprt.dd_SalesDocNo
AND fso.dd_SalesItemNo = sprt.dd_SalesItemNo;

drop table if exists tmp_for_upd_Dim_PartSalesId;

UPDATE fact_salesorder fso
SET Dim_PartSalesId = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM VBAK_VBAP vp, fact_salesorder fso
where   fso.dd_SalesDocNo = vp.VBAP_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = 0
AND VBAK_VKORG IS NULL
AND Dim_PartSalesId <> 1;

/*Georgiana Abiguous Replace fix: adding dim_distributionchannel in join*/
UPDATE fact_salesorder fso
SET fso.Dim_PartSalesId = sprt.Dim_PartSalesId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM VBAK_VBAP_VBEP vp, Dim_PartSales sprt, fact_salesorder fso, dim_distributionchannel dc
where   fso.dd_SalesDocNo = vp.VBAK_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = vp.VBEP_ETENR
AND VBAK_VKORG IS NOT NULL
AND sprt.SalesOrgCode = VBAK_VKORG
AND sprt.PartNumber = VBAP_MATNR
and dc.DistributionChannelCode = sprt.distributionchannelcode and
dc.Dim_DistributionChannelId = fso.Dim_DistributionChannelId
AND sprt.RowIsCurrent = 1
AND fso.Dim_PartSalesId <> sprt.Dim_PartSalesId;

UPDATE fact_salesorder fso
SET Dim_PartSalesId = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM VBAK_VBAP_VBEP vp, fact_salesorder fso
where   fso.dd_SalesDocNo = vp.VBAK_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = vp.VBEP_ETENR
AND VBAK_VKORG IS NULL
AND Dim_PartSalesId <> 1;

UPDATE fact_salesorder fso SET Dim_PartSalesId = 1 where Dim_PartSalesId IS NULL;
/*End Changes 16 Mar 2016*/

/* Georgiana Changes 18 Mar 2016 adding dd_deliverynumber: EKES-VBELN -BI-2348 : Adding also rowseqno column in the temporary table to avoid duplicates*/

drop table if exists tmp_deliverynumber;
Create table tmp_deliverynumber as
select distinct a.fact_salesorderid,ifnull(b.dd_DeliveryNumber,'Not Set') as dd_DeliveryNumber, row_number() over ( partition by a.fact_salesorderid order by a.fact_salesorderid) as rowseqno
from fact_salesorder a, fact_purchase b
where
a.dd_ourreference = b.dd_ourreference
and b.dd_ourreference<>'Not Set';

update fact_salesorder fso
set fso.dd_DeliveryNumber = t.dd_DeliveryNumber
from tmp_deliverynumber t, fact_salesorder fso
where fso.fact_salesorderid = t.fact_salesorderid
and rowseqno=1
and fso.dd_DeliveryNumber <> t.dd_DeliveryNumber;

/* 18 Mar End of Changes*/

/* Georgiana 21 mar 2016 aading dd_SalesDlvrDocNo, dd_SalesDlvrItemNo according to BI-2371*/
/*Alin BI-5191*/
update fact_salesorder  f
set f.dd_SalesDlvrDocNo = 'Not Set'
from fact_salesorder f
where f.dd_SalesDlvrDocNo not in (select distinct fd.dd_SalesDlvrDocNo from fact_salesorderdelivery fd)
and f.dd_SalesDlvrDocNo <> 'Not Set';
/*Alin BI-5191*/

merge into fact_salesorder so
using ( select distinct fact_salesorderid, max(sod.dd_SalesDlvrDocNo) as dd_SalesDlvrDocNo
 from fact_salesorderdelivery sod, fact_salesorder so
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
 AND sod.dd_SalesItemNo = so.dd_SalesItemNo
 AND sod.dd_ScheduleNo = so.dd_ScheduleNo
GROUP by so.fact_salesorderid) t on t.fact_salesorderid=so.fact_salesorderid
when matched then update set so.dd_SalesDlvrDocNo = ifnull(t.dd_SalesDlvrDocNo,'Not Set')
where so.dd_SalesDlvrDocNo <> ifnull(t.dd_SalesDlvrDocNo,'Not Set');



merge into fact_salesorder so
using ( select distinct fact_salesorderid, max(sod.dd_SalesDlvrItemNo) as dd_SalesDlvrItemNo
 from fact_salesorderdelivery sod, fact_salesorder so
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
 AND sod.dd_SalesItemNo = so.dd_SalesItemNo
 AND sod.dd_ScheduleNo = so.dd_ScheduleNo
GROUP by so.fact_salesorderid) t on t.fact_salesorderid=so.fact_salesorderid
when matched then update set so.dd_SalesDlvrItemNo = ifnull(t.dd_SalesDlvrItemNo,0)
where so.dd_SalesDlvrItemNo <> ifnull(t.dd_SalesDlvrItemNo,0);

/*End of Changes 21 Mar 2016*/

/* Madalina 10 Jun 2016 - Shipment Doc No, Shipment Doc Item and Actual Goods Issue Date are Not Set for some items BI-3042 */
merge into fact_salesorder fso 
using (select distinct fso.fact_salesorderid, max(f.dd_SalesDlvrDocNo) as dd_SalesDlvrDocNo
    from fact_salesorder f, fact_salesorder fso
	where f.dd_SalesDocNo = fso.dd_SalesDocNo
		and f.dd_SalesItemNo = fso.dd_SalesItemNo
		and f.dd_SalesDlvrDocNo <> 'Not Set'
		and fso.dd_SalesDlvrDocNo = 'Not Set'
GROUP by fso.fact_salesorderid) t on t.fact_salesorderid=fso.fact_salesorderid
when matched then update set fso.dd_SalesDlvrDocNo = ifnull(t.dd_SalesDlvrDocNo,'Not Set')
where fso.dd_SalesDlvrDocNo <> ifnull(t.dd_SalesDlvrDocNo,'Not Set');


merge into fact_salesorder fso 
using (select distinct fso.fact_salesorderid, max(f.dd_salesdlvritemno) as dd_salesdlvritemno
    from fact_salesorder f, fact_salesorder fso
	where f.dd_SalesDocNo = fso.dd_SalesDocNo
		and f.dd_SalesItemNo = fso.dd_SalesItemNo
		and f.dd_salesdlvritemno <> '0'
		and fso.dd_salesdlvritemno = '0'
GROUP by fso.fact_salesorderid) t on t.fact_salesorderid=fso.fact_salesorderid
when matched then update set fso.dd_salesdlvritemno = ifnull(t.dd_salesdlvritemno,0)
where fso.dd_salesdlvritemno <> ifnull(t.dd_salesdlvritemno,0);

/*merge into fact_salesorder fso
using (select distinct fso.fact_salesorderid,max(f.dim_dateidactualgi) as dim_dateidactualgi
    from fact_salesorder f,fact_salesorder fso
	where f.dd_SalesDocNo = fso.dd_SalesDocNo
		and f.dd_SalesItemNo = fso.dd_SalesItemNo
		and f.dim_dateidactualgi <> '1'
		and fso.dim_dateidactualgi = '1'
group by fso.fact_salesorderid) t on t.fact_salesorderid=fso.fact_salesorderid
when matched then update set fso.dim_dateidactualgi = t.dim_dateidactualgi
where fso.dim_dateidactualgi <> t.dim_dateidactualgi*/
/*End of Changes 10 Jun 2016 */
/* LI Start Changes 02 Mar 2015 */

 /* Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */

UPDATE fact_salesorder f
SET f.std_exchangerate_dateid = f.Dim_DateIdSODocument
WHERE   f.std_exchangerate_dateid <> f.Dim_DateIdSODocument;

/* This date was required and it should only contain the first not null date */
drop table if exists vbak_vbap_vbep_creditdate;
create table vbak_vbap_vbep_creditdate as
select vbak_vbeln,vbap_posnr,vbep_etenr,vbap_werks,vbak_cmfre from vbak_vbap_vbep
where 1=0 ;

insert into vbak_vbap_vbep_creditdate
select vbak_vbeln,vbap_posnr,vbep_etenr,vbap_werks,vbak_cmfre from vbak_vbap_vbep stg
where not exists (select 1 from vbak_vbap_vbep_creditdate cd
where cd.vbak_vbeln = stg.vbak_vbeln
and cd.vbap_posnr = stg.vbap_posnr
and cd.vbep_etenr = stg.vbep_etenr);

UPDATE fact_salesorder so
   SET so.dim_dateidcreditrelease = dt.Dim_Dateid,
   dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbep_creditdate vbk, dim_plant pl, dim_Date dt, fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
             AND vbk.VBAP_POSNR = so.dd_SalesItemNo
             AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = ifnull(vbk.VBAP_WERKS,'Not Set') AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <>0
AND dt.DateValue = ifnull(vbk.VBAK_CMFRE,'0001-01-01') AND dt.CompanyCode = pl.CompanyCode and dt.plantcode_factory=pl.plantcode
AND so.dim_dateidcreditrelease <> dt.Dim_Dateid;
/*Octavian: EA changes END */

/* Madalina 17 Oct 2016 - Change formula for Ex Works Date - BI-4442 */
/*  Madalina Herghelegiu - 13 May 2016 - Add new column, representing the difference between Order Purchase Date and Transit time in Calendar Days. BI - 2821 */
/* UPDATE fact_salesorder so
 SET so.dim_dateidworksdate = ewd.dim_dateid,
 dw_update_date = CURRENT_TIMESTAMP
 FROM dim_date dd, dim_route r, dim_date ewd, dim_company dc, fact_salesorder so
 WHERE
 so.dim_dateidorginalrequestdate = dd.dim_dateid
 and so.dim_routeid = r.dim_routeid
 and ewd.datevalue = case when dd.datevalue <> '0001-01-01'  then dd.datevalue - convert(integer, r.transittimeincalendardays) else '0001-01-01' end
 and ewd.companycode = dc.companycode
 and so.dim_companyid = dc.dim_companyid
 and so.dim_dateidworksdate <> ewd.dim_dateid */

 
 /* Madalina 28 Oct 2016 - Move the update in the end of the script, to catch all dim_routeid updates - BI-4442 */
 /* UPDATE fact_salesorder so
 SET so.dim_dateidworksdate = ewd.dim_dateid,
 dw_update_date = CURRENT_TIMESTAMP
 FROM dim_date dd, dim_date pd, dim_route r, dim_date ewd, dim_company dc, fact_salesorder so
 WHERE
 so.dim_shiptopartypodate = dd.dim_dateid
 and so.dim_dateidscheddelivery = pd.dim_dateid
 and so.dim_routeid = r.dim_routeid
 and ewd.datevalue = 
	(case when dd.datevalue = '0001-01-01' 
		 then (case when pd.datevalue <> '0001-01-01' then pd.datevalue - convert(integer, r.transittimeincalendardays) else '0001-01-01' end)
		 else dd.datevalue - convert(integer, r.transittimeincalendardays) 
    end)
 and ewd.companycode = dc.companycode
 and so.dim_companyid = dc.dim_companyid
 and so.dim_dateidworksdate <> ewd.dim_dateid */
 
/* END - BI-4442 */

/* END */

/*  Madalina Herghelegiu - 19 May 2016 - Add  Billing Date (Invoice) and Invoice Number (F2), based on the related columns from fact_billing. BI-2795 */
drop table if exists tmp_billingdate_in_sales;
create table tmp_billingdate_in_sales as
	select fb.dd_salesdocno, fb.dd_salesItemNo, dd.datevalue, fb.dim_dateidbilling,
		   fb.dd_billing_no,
		   row_number() over (partition by fb.dd_salesdocno,fb.dd_salesitemno order by dd.datevalue desc) rn
    from fact_billing fb, dim_date dd, dim_billingdocumenttype bdt
    where dd.dim_dateid = fb.dim_dateidbilling
    /* Madalina 7 May 2018 - APP-9565 */
    /* and bdt.type = 'F2' */  
    /* for invoice number */
    and fb.dim_documenttypeid = bdt.dim_billingdocumenttypeid;

update fact_salesorder fso
	set fso.dim_dateidbillingdate = tmpb.dim_dateidbilling,
		dw_update_date = current_timestamp
	from tmp_billingdate_in_sales tmpb, fact_salesorder fso
	where tmpb.dd_salesdocno = fso.dd_SalesDocNo
		and tmpb.dd_salesitemno = fso.dd_SalesItemNo
		and rn = 1
		and fso.dim_dateidbillingdate <> tmpb.dim_dateidbilling;


update fact_salesorder fso
	set fso.dd_invoicenumberf2 = ifnull(tmpb.dd_billing_no,'Not Set'),
		dw_update_date = current_timestamp
from tmp_billingdate_in_sales tmpb, fact_salesorder fso
	where tmpb.dd_salesdocno = fso.dd_SalesDocNo
		and tmpb.dd_salesitemno = fso.dd_SalesItemNo
		and rn = 1
		and fso.dd_invoicenumberf2 <> ifnull(tmpb.dd_billing_no,'Not Set');
/*  END Madalina Herghelegiu */
/* Madalina 24 Jun 2016 - add new dimension - BI-3300 */
update fact_salesorder f_so
	
	set dim_OrderDueDate = 
  		(CASE WHEN f_so.dd_ScheduleNo <> 0 AND f_so.dd_ItemRelForDelv <> 'X' 
				THEN mavd.dim_dateid 
			WHEN f_so.dd_ScheduleNo <> 0 AND f_so.dd_ItemRelForDelv = 'X'
				THEN pgid.dim_dateid 
			WHEN f_so.dd_ScheduleNo = 0 AND spdd.DateValue > '1900-01-01'
				THEN soc.dim_dateid 
			END)
from dim_date mavd, dim_date pgid, dim_date soc, dim_date spdd, fact_salesorder f_so	
where 
		dim_dateidmtrlavail = mavd.dim_dateid
		and dim_dateidgoodsissue = pgid.dim_dateid
		and dim_dateidsalesordercreated = soc.dim_dateid
		and dim_dateidshipmentdelivery = spdd.dim_dateid
		and dim_OrderDueDate <>
			(CASE WHEN f_so.dd_ScheduleNo <> 0 AND f_so.dd_ItemRelForDelv <> 'X' 
					THEN mavd.dim_dateid 
				WHEN f_so.dd_ScheduleNo <> 0 AND f_so.dd_ItemRelForDelv = 'X'
					THEN pgid.dim_dateid 
				WHEN f_so.dd_ScheduleNo = 0 AND spdd.DateValue > '1900-01-01'
					THEN soc.dim_dateid 
				END);

/*Georgiana added  vbpa_posnr in join*/
drop table if exists tmp_distinct_kunnr;
create table tmp_distinct_kunnr as
select distinct VBPA_VBELN,VBPA_KUNNR, vbpa_posnr from VBPA
where VBPA_PARVW in ('WE','AG');

update fact_salesorder a
set a.dd_customershsp = ifnull(b.VBPA_KUNNR,'Not Set')
from tmp_distinct_kunnr b, fact_salesorder a
where a.dd_salesdocno = b.VBPA_VBELN
and a.dd_salesitemno = b.vbpa_posnr
and a.dd_customershsp <> ifnull(b.VBPA_KUNNR,'Not Set');
drop table if exists tmp_distinct_kunnr;


/*Georgiana 10 May 2016 EA Changes: Adding Material Price Group 2,3,4,5 according to BI-2706*/

UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup2Id = mpg.Dim_MaterialPriceGroup2Id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk,Dim_MaterialPriceGroup2 mpg,fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND  mpg.MaterialPriceGroup2 = ifnull(vbk.VBAP_MVGR2,'Not Set')
AND mpg.RowIsCurrent = 1
AND so.Dim_MaterialPriceGroup2Id <> ifnull( mpg.Dim_MaterialPriceGroup2Id,1);

UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup2Id = mpg.Dim_MaterialPriceGroup2Id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk,Dim_MaterialPriceGroup2 mpg, fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND so.dd_ScheduleNo = 0
AND  mpg.MaterialPriceGroup2 = ifnull(vbk.VBAP_MVGR2,'Not Set')
AND mpg.RowIsCurrent = 1
AND so.Dim_MaterialPriceGroup2Id <> ifnull( mpg.Dim_MaterialPriceGroup2Id,1);

UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup2Id = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE so.Dim_MaterialPriceGroup2Id IS NULL;

UPDATE fact_salesorder so
SET Dim_MaterialPriceGroup3Id = mpg3.Dim_MaterialPriceGroup3Id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk,Dim_MaterialPriceGroup3 mpg3,fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND  mpg3.MaterialPriceGroup3 = ifnull(vbk.VBAP_MVGR3,'Not Set')
AND mpg3.RowIsCurrent = 1
AND so.Dim_MaterialPriceGroup3Id <> ifnull( mpg3.Dim_MaterialPriceGroup3Id,1);

UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup3Id = mpg3.Dim_MaterialPriceGroup3Id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk,Dim_MaterialPriceGroup3 mpg3,fact_salesorder so
Where vbk.VBAK_VBELN = so.dd_SalesDocNo
AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND so.dd_ScheduleNo = 0
AND  mpg3.MaterialPriceGroup3 = ifnull(vbk.VBAP_MVGR3,'Not Set')
AND mpg3.RowIsCurrent = 1
AND so.Dim_MaterialPriceGroup3Id <> ifnull( mpg3.Dim_MaterialPriceGroup3Id,1);

UPDATE fact_salesorder so
SET so.Dim_MaterialPriceGroup3Id = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE so.Dim_MaterialPriceGroup3Id IS NULL;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from vbak_vbap_vbep, dim_materialpricegroup4 mg4,fact_salesorder so
WHERE so.dd_SalesDocNo = VBAK_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = VBEP_ETENR
AND ifnull(VBAP_MVGR4,'Not Set') = mg4.MaterialPriceGroup4
AND mg4.RowIsCurrent = 1
AND ifnull(so.dim_materialpricegroup4id,-1) <> ifnull(mg4.dim_materialpricegroup4id,-2);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from vbak_vbap, dim_materialpricegroup4 mg4,fact_salesorder so
WHERE so.dd_SalesDocNo = VBAP_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = 0
AND ifnull(VBAP_MVGR4,'Not Set') = mg4.MaterialPriceGroup4
AND mg4.RowIsCurrent = 1
AND ifnull(so.dim_materialpricegroup4id,-1) <> ifnull(mg4.dim_materialpricegroup4id,-2);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup4id = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE so.dim_materialpricegroup4id IS NULL;

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from vbak_vbap_vbep, dim_materialpricegroup5 mg5,fact_salesorder so
WHERE so.dd_SalesDocNo = VBAK_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = VBEP_ETENR
AND ifnull(VBAP_MVGR5,'Not Set') = mg5.MaterialPriceGroup5
AND mg5.RowIsCurrent = 1
AND ifnull(so.dim_materialpricegroup5id,-1) <> ifnull(mg5.dim_materialpricegroup5id,-2);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from vbak_vbap, dim_materialpricegroup5 mg5,fact_salesorder so
WHERE so.dd_SalesDocNo = VBAP_VBELN
AND so.dd_SalesItemNo = VBAP_POSNR
AND so.dd_ScheduleNo = 0
AND ifnull(VBAP_MVGR5,'Not Set') = mg5.MaterialPriceGroup5
AND mg5.RowIsCurrent = 1
AND ifnull(so.dim_materialpricegroup5id,-1) <> ifnull(mg5.dim_materialpricegroup5id,-2);

UPDATE fact_salesorder so
SET so.dim_materialpricegroup5id = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE so.dim_materialpricegroup5id IS NULL;

UPDATE fact_salesorder so
SET so.dim_MaterialPricingGroupId  = mpg.dim_MaterialPricingGroupId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap_vbep vbk, dim_plant pl, 
(select MaterialPricingGroupCode, min(dim_MaterialPricingGroupId) dim_MaterialPricingGroupId 
from dim_MaterialPricingGroup where RowIsCurrent  =  1  
group by MaterialPricingGroupCode) mpg, 
fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
  AND vbk.VBAP_POSNR = so.dd_SalesItemNo
  AND vbk.VBEP_ETENR = so.dd_ScheduleNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
AND so.dd_ScheduleNo <> 0
AND mpg.MaterialPricingGroupCode = vbk.VBAP_KONDM
AND ifnull(so.dim_MaterialPricingGroupId,-1) <> ifnull(mpg.dim_MaterialPricingGroupId,-2);

UPDATE fact_salesorder so
SET so.dim_MaterialPricingGroupId = mpg.dim_MaterialPricingGroupId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM vbak_vbap vbk, dim_plant pl, dim_MaterialPricingGroup mpg,fact_salesorder so
where vbk.VBAP_VBELN = so.dd_SalesDocNo
  AND vbk.VBAP_POSNR = so.dd_SalesItemNo
AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent  =  1
AND so.dd_ScheduleNo = 0
AND mpg.MaterialPricingGroupCode = vbk.VBAP_KONDM AND mpg.RowIsCurrent  =  1
AND ifnull(so.dim_MaterialPricingGroupId,-1) <> ifnull(mpg.dim_MaterialPricingGroupId,-2);

UPDATE fact_salesorder so set so.dim_MaterialPricingGroupId = 1 where dim_MaterialPricingGroupId is NULL;

/* Madalina - 28 Jul 2016 - add new dd for material, without a link on plant - BI-2952 */
update fact_salesorder so
set dd_partnumber = ifnull(VBAP_MATNR, 'Not Set')
from vbak_vbap_vbep vbk, fact_salesorder so
where vbk.VBAK_VBELN = so.dd_SalesDocNo
  and vbk.VBAP_POSNR = so.dd_SalesItemNo
  and vbk.VBEP_ETENR = so.dd_ScheduleNo
  and so.dd_ScheduleNo <> 0
  and dd_partnumber <> ifnull(VBAP_MATNR, 'Not Set');

update fact_salesorder so
set dd_partnumber = ifnull(VBAP_MATNR, 'Not Set')
from vbak_vbap vbk,fact_salesorder so 
where vbk.VBAP_VBELN = so.dd_SalesDocNo
  and vbk.VBAP_POSNR = so.dd_SalesItemNo
  and so.dd_ScheduleNo = 0
  and dd_partnumber <> ifnull(VBAP_MATNR, 'Not Set');

/* END 28 Jul 2016 */

/*10 May 2016 End of changes*/

/*  CALL bi_populate_customer_hierarchy() */

/*
drop table if exists deletepart_701
drop table if exists Dim_CostCenter_701
drop table if exists Dim_CostCenter_first1_701
drop table if exists dim_profitcenter_701
drop table if exists dim_profitcenteri_701
drop table if exists dim_profitcenter_upd_701
drop table if exists fact_salesorder_tmptbl
drop table if exists staging_update_701
drop table if exists max_holder_701
drop table if exists variable_holder_701
drop table if exists VBAK_VBAP_VBEP_701
drop table if exists tmp_sof_Dim_CustomerPartnerFunctions
select 'Process Complete'
select 'sales order count ',count(*) from fact_salesorder
DROP TABLE IF EXISTS TMP_UPD_dd_CreditRep1
*/

DROP TABLE IF EXISTS tmp_sof_fb_amt_CustomerConfigSubtotal3;
DROP TABLE IF EXISTS tmp_sof_sof_minsche;
DROP TABLE IF EXISTS tmp_sof_amt_CustomerConfigSubtotal3_upd;
drop table if exists dim_profitcenter_upd_701;
DROP TABLE IF EXISTS fact_salesorder_tmp_Dim_CustomeridShipToFix;

/*25 May 2016 Georgiana EA Changes adding dim_SalesrepresentativeId according to BI-2929*/

drop table if exists tmp_Dim_dim_SalesrepresentativeId_01 ;
create table tmp_Dim_dim_SalesrepresentativeId_01  as
select distinct fact_salesorderid,dc.dim_customerid
from
fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZR'
	AND so.dd_ScheduleNo <> 0;

/*
update fact_salesorder so
set so.dim_SalesrepresentativeId = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_01 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and ifnull(dim_SalesrepresentativeId,-1) <> ifnull(t.dim_customerid,-2)
*/

merge into fact_salesorder f
using(
select distinct so.fact_salesorderid, max(ifnull(t.dim_customerid,1)) as dim_customerid
from tmp_Dim_dim_SalesrepresentativeId_01 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
group by so.fact_salesorderid
) z
on z.fact_salesorderid = f.fact_salesorderid
when matched then update
set f.dim_SalesrepresentativeId = z.dim_customerid
where f.dim_SalesrepresentativeId <> z.dim_customerid;


drop table if exists tmp_Dim_dim_SalesrepresentativeId_02;
create table tmp_Dim_dim_SalesrepresentativeId_02 as
select distinct fact_salesorderid,max (dc.dim_customerid) as dim_customerid
from 
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZR'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeId,1) = 1
group by  fact_salesorderid;

update fact_salesorder so
set so.dim_SalesrepresentativeId = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_02 t,fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and ifnull(so.dim_SalesrepresentativeId,-1) <> ifnull(t.dim_customerid,-2);

/*25 May 2016 End Of Changes*/
drop table if exists tmp_Dim_dim_SalesrepresentativeId_01;
drop table if exists tmp_Dim_dim_SalesrepresentativeId_02;



/* 4 Jul 2016 Madalina - add new fields, dim_SalesrepresentativeCA and dim_SalesrepresentativeRU - BI-3360 */
drop table if exists tmp_Dim_dim_SalesrepresentativeId_03 ;
create table tmp_Dim_dim_SalesrepresentativeId_03  as
select distinct fact_salesorderid,dc.dim_customerid
from fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZZ'
	AND so.dd_ScheduleNo <> 0;

update fact_salesorder so 
set so.dim_SalesrepresentativeCA = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_03 t, fact_salesorder so 
where so.fact_salesorderid = t.fact_salesorderid
  and ifnull(dim_SalesrepresentativeCA,-1) <> ifnull(t.dim_customerid,-2);
  
drop table if exists tmp_Dim_dim_SalesrepresentativeId_04;
create table tmp_Dim_dim_SalesrepresentativeId_04 as
select distinct fact_salesorderid,dc.dim_customerid
from
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZZ'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeId,1) = 1;

update fact_salesorder so
set so.dim_SalesrepresentativeCA = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_04 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and ifnull(so.dim_SalesrepresentativeCA,-1) <> ifnull(t.dim_customerid,-2);

drop table if exists tmp_Dim_dim_SalesrepresentativeId_03;
drop table if exists tmp_Dim_dim_SalesrepresentativeId_04;

/* ************************** */

drop table if exists tmp_Dim_dim_SalesrepresentativeId_05 ;
create table tmp_Dim_dim_SalesrepresentativeId_05  as
select distinct fact_salesorderid,dc.dim_customerid
from fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZU'
	AND so.dd_ScheduleNo <> 0;

update fact_salesorder so 
set so.dim_SalesrepresentativeRU = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_05 t, fact_salesorder so 
where so.fact_salesorderid = t.fact_salesorderid
  and ifnull(dim_SalesrepresentativeRU,-1) <> ifnull(t.dim_customerid,-2);
  
drop table if exists tmp_Dim_dim_SalesrepresentativeId_06;
create table tmp_Dim_dim_SalesrepresentativeId_06 as
select distinct fact_salesorderid,dc.dim_customerid
from
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZU'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeId,1) = 1;

update fact_salesorder so
set so.dim_SalesrepresentativeRU = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_06 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and ifnull(so.dim_SalesrepresentativeRU,-1) <> ifnull(t.dim_customerid,-2);

drop table if exists tmp_Dim_dim_SalesrepresentativeId_05;
drop table if exists tmp_Dim_dim_SalesrepresentativeId_06;

/* Madalina 22 Aug 2016 - add six more Sales Representative fields, according to BI-3523 */

/* Sales Representative LS - Partner Function = ZY */
drop table if exists tmp_Dim_dim_SalesrepresentativeId_07 ;
create table tmp_Dim_dim_SalesrepresentativeId_07  as
select distinct fact_salesorderid,dc.dim_customerid
from fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZY'
	AND so.dd_ScheduleNo <> 0;

update fact_salesorder so 
set so.dim_SalesrepresentativeZY = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_07 t, fact_salesorder so 
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZY <> ifnull(t.dim_customerid,1);
  
drop table if exists tmp_Dim_dim_SalesrepresentativeId_08;
create table tmp_Dim_dim_SalesrepresentativeId_08 as
select distinct fact_salesorderid,dc.dim_customerid
from
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZY'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeZY,1) = 1;

update fact_salesorder so
set so.dim_SalesrepresentativeZY = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_08 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZY <> ifnull(t.dim_customerid,1);

drop table if exists tmp_Dim_dim_SalesrepresentativeId_07;
drop table if exists tmp_Dim_dim_SalesrepresentativeId_08;

/* Sales Representative PO - Partner Function = ZX */
drop table if exists tmp_Dim_dim_SalesrepresentativeId_09 ;
create table tmp_Dim_dim_SalesrepresentativeId_09  as
select distinct fact_salesorderid,dc.dim_customerid
from fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZX'
	AND so.dd_ScheduleNo <> 0;

update fact_salesorder so 
set so.dim_SalesrepresentativeZX = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_09 t, fact_salesorder so 
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZX <> ifnull(t.dim_customerid,1);
  
drop table if exists tmp_Dim_dim_SalesrepresentativeId_10;
create table tmp_Dim_dim_SalesrepresentativeId_10 as
select distinct fact_salesorderid,dc.dim_customerid
from
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZX'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeZX,1) = 1;

update fact_salesorder so
set so.dim_SalesrepresentativeZX = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_10 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZX <> ifnull(t.dim_customerid,1);

drop table if exists tmp_Dim_dim_SalesrepresentativeId_09;
drop table if exists tmp_Dim_dim_SalesrepresentativeId_10;

/* Responsible Person - Partner Function = ZC */
drop table if exists tmp_Dim_dim_SalesrepresentativeId_11 ;
create table tmp_Dim_dim_SalesrepresentativeId_11  as
select distinct fact_salesorderid,dc.dim_customerid
from fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZC'
	AND so.dd_ScheduleNo <> 0;

update fact_salesorder so 
set so.dim_SalesrepresentativeZC = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_11 t, fact_salesorder so 
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZC <> ifnull(t.dim_customerid,1);
  
drop table if exists tmp_Dim_dim_SalesrepresentativeId_12;
create table tmp_Dim_dim_SalesrepresentativeId_12 as
select distinct fact_salesorderid,dc.dim_customerid
from
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZC'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeZC,1) = 1;

update fact_salesorder so
set so.dim_SalesrepresentativeZC = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_12 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZC <> ifnull(t.dim_customerid,1);

drop table if exists tmp_Dim_dim_SalesrepresentativeId_11;
drop table if exists tmp_Dim_dim_SalesrepresentativeId_12;

/* Agent Code - Partner Function = ZW - Madalina 25 Aug 2016 - Resolve Unable to get a stable set of rows in the source tables error */
drop table if exists tmp_Dim_dim_SalesrepresentativeId_13 ;
create table tmp_Dim_dim_SalesrepresentativeId_13  as
select distinct fact_salesorderid,
	first_value(dc.dim_customerid) over ( partition by fact_salesorderid order by '') as dim_customerid 
from fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZW'
	AND so.dd_ScheduleNo <> 0;

update fact_salesorder so 
set so.dim_SalesrepresentativeZW = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_13 t, fact_salesorder so 
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZW <> ifnull(t.dim_customerid,1);
  
drop table if exists tmp_Dim_dim_SalesrepresentativeId_14;
create table tmp_Dim_dim_SalesrepresentativeId_14 as
select distinct fact_salesorderid,dc.dim_customerid
from
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZW'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeZW,1) = 1;

update fact_salesorder so
set so.dim_SalesrepresentativeZW = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_14 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZW <> ifnull(t.dim_customerid,1);

drop table if exists tmp_Dim_dim_SalesrepresentativeId_13;
drop table if exists tmp_Dim_dim_SalesrepresentativeId_14;

/* Sales Representative PG - Partner Function = ZV */
drop table if exists tmp_Dim_dim_SalesrepresentativeId_15 ;
create table tmp_Dim_dim_SalesrepresentativeId_15  as
select distinct fact_salesorderid,dc.dim_customerid
from fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZV'
	AND so.dd_ScheduleNo <> 0;

update fact_salesorder so 
set so.dim_SalesrepresentativeZV = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_15 t, fact_salesorder so 
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZV <> ifnull(t.dim_customerid,1);
  
drop table if exists tmp_Dim_dim_SalesrepresentativeId_16;
create table tmp_Dim_dim_SalesrepresentativeId_16 as
select distinct fact_salesorderid,dc.dim_customerid
from
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.customernumber = VBPA_KUNNR
	AND sdp.vbpa_parvw = 'ZV'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeZV,1) = 1;

update fact_salesorder so
set so.dim_SalesrepresentativeZV = ifnull(t.dim_customerid,1)
from tmp_Dim_dim_SalesrepresentativeId_16 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
  and so.dim_SalesrepresentativeZV <> ifnull(t.dim_customerid,1);

drop table if exists tmp_Dim_dim_SalesrepresentativeId_15;
drop table if exists tmp_Dim_dim_SalesrepresentativeId_16;

/* Fwding agent-Carrier - Partner Function = SP */
drop table if exists tmp_Dim_dim_SalesrepresentativeId_17 ;
create table tmp_Dim_dim_SalesrepresentativeId_17  as
select distinct fact_salesorderid,dc.dim_vendorid,
	row_number() over (partition by fact_salesorderid order by '') as rn
from fact_salesorder so, vbak_vbap_vbep shi, vbpa sdp, dim_vendor dc
where so.dd_SalesDocNo = shi.vbak_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND so.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln
	AND dc.VendorNumber = VBPA_LIFNR
	AND sdp.vbpa_parvw = 'SP'
	AND so.dd_ScheduleNo <> 0;

update fact_salesorder so 
set so.dim_SalesrepresentativeSP = ifnull(t.dim_vendorid,1)
from tmp_Dim_dim_SalesrepresentativeId_17 t, fact_salesorder so 
where so.fact_salesorderid = t.fact_salesorderid
  and rn = 1
  and so.dim_SalesrepresentativeSP <> ifnull(t.dim_vendorid,1);
  
drop table if exists tmp_Dim_dim_SalesrepresentativeId_18;
create table tmp_Dim_dim_SalesrepresentativeId_18 as
select distinct fact_salesorderid,dc.dim_vendorid,
	row_number() over (partition by fact_salesorderid order by '') as rn
from
fact_salesorder so, vbak_vbap shi, vbpa sdp, dim_vendor dc
where so.dd_SalesDocNo = shi.vbap_vbeln
    AND so.dd_SalesItemNo = shi.vbap_posnr
    AND  sdp.vbpa_vbeln = shi.vbap_vbeln
	AND dc.VendorNumber = VBPA_LIFNR
	AND sdp.vbpa_parvw = 'SP'
	AND so.dd_ScheduleNo = 0
	AND ifnull(so.dim_SalesrepresentativeSP,1) = 1;

update fact_salesorder so
set so.dim_SalesrepresentativeSP = ifnull(t.dim_vendorid,1)
from tmp_Dim_dim_SalesrepresentativeId_18 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
	and rn = 1
  and so.dim_SalesrepresentativeSP <> ifnull(t.dim_vendorid,1);

drop table if exists tmp_Dim_dim_SalesrepresentativeId_17;
 /* End 22 Aug 2016 */
drop table if exists tmp_Dim_dim_SalesrepresentativeId_18;

/* Madalina 23 Aug 2016 - add two new fields, according to BI-3554 */
/* Denominator for Conversion of Sales qty */
update fact_salesorder so
set so.dd_DenomConvSalesQty = ifnull(VBAP_UMZIN, 0)
from fact_salesorder so, vbak_vbap_vbep vbk
where so.dd_SalesDocNo = vbk.vbak_vbeln
    and so.dd_SalesItemNo = vbk.vbap_posnr
    and so.dd_scheduleno = vbk.vbep_etenr
	and so.dd_DenomConvSalesQty <> ifnull(VBAP_UMZIN, 0);
	
update fact_salesorder so
set so.dd_DenomConvSalesQty = ifnull(VBAP_UMZIN, 0)
from fact_salesorder so, vbak_vbap vbk
where so.dd_SalesDocNo = vbk.vbap_vbeln
    and so.dd_SalesItemNo = vbk.vbap_posnr
    and so.dd_scheduleno = 0
	and so.dd_DenomConvSalesQty <> ifnull(VBAP_UMZIN, 0);

/* Returns Item */
update fact_salesorder so
set so.dd_ReturnsItem =  ifnull(VBAP_SHKZG, 'Not Set')
from fact_salesorder so, vbak_vbap_vbep vbk
where so.dd_SalesDocNo = vbk.vbak_vbeln
    and so.dd_SalesItemNo = vbk.vbap_posnr
    and so.dd_scheduleno = vbk.vbep_etenr	
	and so.dd_ReturnsItem <>  ifnull(VBAP_SHKZG, 'Not Set');

update fact_salesorder so
set so.dd_ReturnsItem =   ifnull(VBAP_SHKZG, 'Not Set')
from fact_salesorder so, vbak_vbap vbk
where so.dd_SalesDocNo = vbk.vbap_vbeln
    and so.dd_SalesItemNo = vbk.vbap_posnr
    and so.dd_scheduleno = 0
	and so.dd_ReturnsItem <>  ifnull(VBAP_SHKZG, 'Not Set');

/* Madalina 23 Aug 2016 - add Follow-On Document Date and Realized Finish Date */
/* Follow-On Document Date */
drop table if exists tmp_sales_followondocdate;
create table tmp_sales_followondocdate as
select distinct so.dd_SalesDocNo, so.dd_SalesItemNo, so.dd_ScheduleNo,
	first_value(ifnull(d.dim_dateid, 1)) over ( partition by so.dd_SalesDocNo, so.dd_SalesItemNo order by ifnull(d.dim_dateid, 1) desc) as  dim_dateid
from fact_salesorder so, VBFA_VBAK_VBAP f, dim_date d, dim_plant pl
where f.VBFA_VBELV  =  so.dd_SalesDocNo
	and f.VBFA_POSNV  =  so.dd_SalesItemNo
	and d.datevalue = ifnull(VBFA_ERDAT, '1900-01-01')
	and d.companycode = pl.CompanyCode
	and pl.plantcode = ifnull(f.VBAP_WERKS,'Not Set')
	and d.plantcode_factory=pl.plantcode
	and so.dd_SubsequentDocNo = ifnull(f.VBFA_VBELN,'Not Set')
	and so.dd_categSubsequentDoc  = ifnull(f.VBFA_VBTYP_N, 'Not Set');

update fact_salesorder so
set so.dim_followondocdate = ifnull(tmp.dim_dateid, 1)
from fact_salesorder so, tmp_sales_followondocdate tmp
where so.dd_SalesDocNo = tmp.dd_SalesDocNo
	and  so.dd_SalesItemNo = tmp.dd_SalesItemNo
	and so.dd_ScheduleNo = tmp.dd_ScheduleNo
 	and so.dim_followondocdate <> ifnull(tmp.dim_dateid, 1);
	
drop table if exists tmp_sales_followondocdate;
	
/* Realized Finish Date */
drop table if exists tmp_sales_realizedFinishDate;
create table tmp_sales_realizedFinishDate as
select distinct so.dd_SalesDocNo, so.dd_SalesItemNo, so.dd_ScheduleNo,
	case when i.DeliveryStatus <> 'Not Set' then so.dim_dateidactualgi
		else so.dim_followondocdate
	end as dim_realizedFinishDate
from fact_salesorder so, dim_date gi, dim_date fd, dim_salesorderitemstatus i
where so.dim_dateidactualgi = gi.dim_dateid
	and so.dim_followondocdate = fd.dim_dateid
	and so.dim_salesorderitemstatusid = i.dim_salesorderitemstatusid;

update fact_salesorder so
set so.dim_realizedFinishDate = ifnull(tmp.dim_realizedFinishDate, 1)
from fact_salesorder so, tmp_sales_realizedFinishDate tmp
where so.dd_SalesDocNo = tmp.dd_SalesDocNo
	and so.dd_SalesItemNo = tmp.dd_SalesItemNo
	and so.dd_ScheduleNo = tmp.dd_ScheduleNo
	and so.dim_realizedFinishDate <> ifnull(tmp.dim_realizedFinishDate, 1);

/* End 23 Aug 2016 */
drop table if exists tmp_sales_realizedFinishDate;

/* Madalina 24 Aug 2016 - add Intervet COPS Profile fields - BI - 3804 */
/* Intervet COPS Profile (TJ30T-TXT04) */
/*drop table if exists tmp_JCDS_VBAP_for_update
create table tmp_JCDS_VBAP_for_update as
select distinct VBAP_VBELN, VBAP_POSNR, TJ30T_TXT04, TJ30T_TXT30, JCDS_UDATE, JCDS_OBJNR,
	first_value(JCDS_UDATE) over (partition by JCDS_OBJNR order by JCDS_UDATE desc) as JCDS_UDATE_max
from JCDS_VBAP j

drop table if exists tmp_JCDS_VBAP_for_update2
create table tmp_JCDS_VBAP_for_update2 as
select distinct VBAP_VBELN, VBAP_POSNR, 
	first_value(TJ30T_TXT04) over (partition by VBAP_VBELN, VBAP_POSNR order by TJ30T_TXT04 ) as TJ30T_TXT04,
	first_value(TJ30T_TXT30) over (partition by VBAP_VBELN, VBAP_POSNR order by TJ30T_TXT30 ) as TJ30T_TXT30
from tmp_JCDS_VBAP_for_update
where JCDS_UDATE = JCDS_UDATE_max*/

/* 15 Dec 2016 Georgiana changes according to BI-5038 taking satus values by last value of JCDS_STAT*/
drop table if exists tmp_JCDS_VBAP_for_update;
create table tmp_JCDS_VBAP_for_update as
select distinct VBAP_VBELN, VBAP_POSNR, JCDS_OBJNR,
	first_value(JCDS_STAT) over (partition by JCDS_UDATE order by JCDS_UDATE,JCDS_STAT desc) as JCDS_STAT_last
from JCDS_VBAP j;

drop table if exists tmp_JCDS_VBAP_for_update2;
create table tmp_JCDS_VBAP_for_update2 as
select distinct a.VBAP_VBELN, a.VBAP_POSNR, 
	first_value(j.TJ30T_TXT04) over (partition by a.VBAP_VBELN, a.VBAP_POSNR order by a.JCDS_STAT_last desc) as TJ30T_TXT04,
	first_value(TJ30T_TXT30) over (partition by a.VBAP_VBELN, a.VBAP_POSNR order by a.JCDS_STAT_last desc) as TJ30T_TXT30
from tmp_JCDS_VBAP_for_update a,JCDS_VBAP j
where a.VBAP_VBELN=j.VBAP_VBELN and a.VBAP_POSNR=j.VBAP_POSNR and a.JCDS_OBJNR=j.JCDS_OBJNR and a.JCDS_STAT_last=j.JCDS_STAT;

/* 15 Dec 2016 End of changes*/

update fact_salesorder so
set so.dd_intervetCOPSProfile = ifnull(TJ30T_TXT04, 'Not Set') 
from fact_salesorder so, tmp_JCDS_VBAP_for_update2 j
where so.dd_SalesDocNo = j.VBAP_VBELN
	and so.dd_SalesItemNo = j.VBAP_POSNR
	and so.dd_intervetCOPSProfile <> ifnull(TJ30T_TXT04, 'Not Set');

/* Intervet COPS Profile Description (TJ30T-TXT30)*/
update fact_salesorder so
set so.dd_intervetCOPSProfileDesc = ifnull(TJ30T_TXT30, 'Not Set') 
from fact_salesorder so, tmp_JCDS_VBAP_for_update2 j
where so.dd_SalesDocNo = j.VBAP_VBELN
	and so.dd_SalesItemNo = j.VBAP_POSNR
	and so.dd_intervetCOPSProfile <> ifnull(TJ30T_TXT30, 'Not Set');

/* Intervet COPS Profile date (JCDS-UDATE) */
drop table if exists tmp_JCDS_VBAP_for_update3;
create table tmp_JCDS_VBAP_for_update3 as
select distinct VBAP_VBELN, VBAP_POSNR, VBAP_WERKS,
	first_value(JCDS_UDATE) over (partition by JCDS_OBJNR order by JCDS_UDATE desc) as JCDS_UDATE
from JCDS_VBAP j;

update fact_salesorder so
set so.dim_intervetCOPSProfileDate = d.dim_dateid
from fact_salesorder so, dim_date d, dim_plant pl, tmp_JCDS_VBAP_for_update3 j
where so.dd_SalesDocNo = j.VBAP_VBELN
	and so.dd_SalesItemNo = j.VBAP_POSNR
	and ifnull(j.JCDS_UDATE, '1900-01-01') = d.datevalue
	and d.companycode = pl.CompanyCode
	and pl.plantcode = ifnull(j.VBAP_WERKS,'Not Set')
	and d.plantcode_factory=pl.plantcode
	and dim_intervetCOPSProfileDate <> d.dim_dateid;

/* 28 Sept 2016 Georgiana addind Material value Sales according to BI-4239*/
drop table if exists tmp_for_materialvaluesales;
create table tmp_for_materialvaluesales as
select
distinct f_so.fact_salesorderid,
CASE WHEN ct_scheduleqtybaseuom <> 0
THEN ct_scheduleqtybaseuom
ELSE (f_so.ct_TargetQty* (CASE WHEN (f_so.dd_numerator) = 0 THEN 1 ELSE f_so.dd_numerator END)/(CASE WHEN f_so.DD_DENOMCONVSALESQTY = 0 THEN 1 ELSE f_so.DD_DENOMCONVSALESQTY END))END
* 
((CASE WHEN prt.pricecontrol = 'S' THEN prt.standardprice_mbew ELSE prt.movingprice END)/(CASE WHEN (prt.priceunit) = 0 THEN 1 ELSE prt.priceunit END)*(CASE WHEN f_so.DD_RETURNSITEM = 'X' THEN -1 ELSE 1 END)) as materialvaluesales
from fact_salesorder f_so, dim_part prt
where f_so.dim_partid=prt.dim_partid;

update fact_salesorder f
set amt_materialvaluesales = t.materialvaluesales
from fact_salesorder f,tmp_for_materialvaluesales t
where f.fact_salesorderid=t.fact_salesorderid;

drop table if exists tmp_for_materialvaluesales;

/*End of changes*/	
/* Update BI-4260 - Sales: Route Code often Not Set*/
/*06 Oct 2016 Georgiana Changes- Ambuguous Replace fix*/
/*UPDATE fact_salesorder A
SET A.Dim_routeid = ifnull(DR.dim_routeid, 1)
from fact_salesorder A inner join vbak_vbap B
on A.dd_SalesDocNo = b.VBAP_VBELN  AND A.dd_SalesItemNo = VBAP_POSNR AND a.dd_ScheduleNo <> 0
INNER JOIN DIM_ROUTE DR 
	     ON dr.RouteCode  =  VBAP_ROUTE
WHERE a.Dim_routeid <> ifnull(dr.dim_routeid, 1)*/

merge into  fact_salesorder A
using ( select distinct fact_salesorderid, first_value(dr.dim_routeid) over (partition by RouteCode order by rowiscurrent desc) as dim_routeid
from fact_salesorder A inner join vbak_vbap B
on A.dd_SalesDocNo = b.VBAP_VBELN AND A.dd_SalesItemNo = VBAP_POSNR AND a.dd_ScheduleNo <> 0
INNER JOIN DIM_ROUTE DR
ON dr.RouteCode = VBAP_ROUTE
WHERE a.Dim_routeid <> ifnull(dr.dim_routeid, 1)
) t
on t.fact_salesorderid=a.fact_salesorderid
when matched then update set A.Dim_routeid = ifnull(t.dim_routeid, 1);

/*Georgiana End of changes*/

drop table if exists tmp_JCDS_VBAP_for_update;
drop table if exists tmp_JCDS_VBAP_for_update2;
/* End 24 Aug 2016 */

 /* Madalina 28 Oct 2016 - Move the update in the end of the script, to catch all dim_routeid updates - BI-4442 */
 UPDATE fact_salesorder so
 SET so.dim_dateidworksdate = ewd.dim_dateid,
 dw_update_date = CURRENT_TIMESTAMP
 FROM dim_date dd, dim_date pd, dim_route r, dim_date ewd, dim_company dc, fact_salesorder so, dim_plant pl
 WHERE
 so.dim_shiptopartypodate = dd.dim_dateid
 and so.dim_dateidscheddelivery = pd.dim_dateid
 and so.dim_routeid = r.dim_routeid
 and so.dim_plantid=pl.dim_plantid
 and ewd.datevalue = 
	(case when dd.datevalue = '0001-01-01' 
		 then (case when pd.datevalue <> '0001-01-01' then pd.datevalue - convert(integer, r.transittimeincalendardays) else '0001-01-01' end)
		 else dd.datevalue - convert(integer, r.transittimeincalendardays) 
    end)
 and ewd.companycode = dc.companycode
 and ewd.plantcode_factory=pl.plantcode
 and so.dim_companyid = dc.dim_companyid
 and so.dim_dateidworksdate <> ewd.dim_dateid;
/* END - BI-4442 */

/* Georgiana Changes 05 Oct 2016 adding amt_netordervalue according to BI-4316*/
update fact_salesorder
set amt_netordervalue= ifnull(vbap_NETWR,0)
From fact_salesorder ,vbak_vbap_vbep
where dd_SalesDocNo=VBAK_VBELN
and dd_SalesItemNo=VBAP_POSNR
and dd_ScheduleNo=VBEP_ETENR;
/*05 Oct 2016 End of changes*/

/* 24 Oct 2016 Georgiana Changes adding bellow update according to BI-4505 Sales UOM not always populated*/
merge into fact_salesorder fo
using( select distinct fact_salesorderid, uom.dim_unitofmeasureid
from VBAK_VBAP v, fact_salesorder fo, dim_unitofmeasure uom
where fo.dd_SalesDocNo = VBAP_VBELN AND fo.dd_SalesItemNo = VBAP_POSNR
and uom.uom=ifnull(vbap_vrkme,'Not Set')
AND fo.Dim_SalesUoMid  <>  uom.dim_unitofmeasureid) t
on t.fact_salesorderid =fo.fact_salesorderid
when matched then update set fo.Dim_SalesUoMid=t.dim_unitofmeasureid;

/*24 Oct 2016 End of changes*/

/* Alin Changes 09 Nov 2016 BI-4634 - Sales: Reason Code of the Change & Description*/
update fact_salesorder
set DD_REASONCODEOFTHECHANGE = ifnull(VBAP_ZZRSC, 'Not Set')
From fact_salesorder,vbak_vbap
where dd_SalesDocNo=VBAP_VBELN
and dd_SalesItemNo=VBAP_POSNR
and DD_REASONCODEOFTHECHANGE <> ifnull(VBAP_ZZRSC, 'Not Set');


update fact_salesorder
set DD_REASONCODEOFTHECHANGEDESC = ifnull(z1.REASON, 'Not Set')
From fact_salesorder f,vbak_vbap vv, Z1SD_SR_REASON z1
where F.dd_SalesDocNo=VV.VBAP_VBELN
and F.dd_SalesItemNo=VV.VBAP_POSNR
AND VV.VBAK_VKORG = Z1.VKORG
AND VV.VBAK_VTWEG = Z1.VTWEG
and VV.VBAP_ZZRSC = Z1.ZZRSC
and DD_REASONCODEOFTHECHANGEDESC <> ifnull(z1.REASON, 'Not Set');

/*09 Nov 2016 End of changes*/

/* Yogini 21 Nov 2016 BI-4668 - add column ct_ontime for '3P-%On TIME -Sales' measure*/
drop table if exists tmp_ct_ontime;
create table tmp_ct_ontime
as
select fso.dd_salesdocno, fso.dd_salesitemno, fso.dd_scheduleno,
		(CASE WHEN ( (agidt.DateValue) - (dewd.DateValue) ) >4 AND  (pl.PlantCode) <>'ES10' THEN 0 
			WHEN ( (agidt.DateValue) - (dewd.DateValue) ) < -7 AND (pl.PlantCode) <>'ES10' THEN 0 
			WHEN ( (agidt.DateValue) - (dewd.DateValue) ) >0 AND  (pl.PlantCode)='ES10' THEN 0 
			WHEN ( (agidt.DateValue) - (dewd.DateValue) ) < -12 AND  (pl.PlantCode)='ES10' THEN 0 
		ELSE 100 END) as x
from fact_salesorder fso,
	dim_date dewd,
	dim_date agidt,
	dim_plant pl
where fso.dim_plantid = pl.dim_plantid
and fso.dim_dateidworksdate = dewd.dim_dateid
and fso.dim_dateidactualgi = agidt.dim_dateid
and ct_ontime <> (CASE WHEN ( (agidt.DateValue) - (dewd.DateValue) ) >4 AND  (pl.PlantCode) <>'ES10' THEN 0 
			WHEN ( (agidt.DateValue) - (dewd.DateValue) ) < -7 AND (pl.PlantCode) <>'ES10' THEN 0 
			WHEN ( (agidt.DateValue) - (dewd.DateValue) ) >0 AND  (pl.PlantCode)='ES10' THEN 0 
			WHEN ( (agidt.DateValue) - (dewd.DateValue) ) < -12 AND  (pl.PlantCode)='ES10' THEN 0 
		ELSE 100 END);


merge into fact_salesorder fso using
	(select fact_salesorderid, x 
	 from tmp_ct_ontime t, fact_salesorder fso
	where fso.dd_salesdocno = t.dd_salesdocno
	and fso.dd_salesitemno = t.dd_salesitemno
	and fso.dd_scheduleno = t.dd_scheduleno) upd
on upd.fact_salesorderid = fso.fact_salesorderid
when matched then update
set ct_ontime = upd.x;

drop table if exists tmp_ct_ontime;
/* 21 Nov 2016 End of changes*/

/* Yogini 24 Nov 2016 BI-4668 - add column ct_countmiss for '3P- Count Miss' measure*/
drop table if exists tmp_countmiss;
create table tmp_countmiss
as
select fso.dd_salesdocno, fso.dd_salesitemno, fso.dd_scheduleno,
		(case when substr ( (CASE 
		
		WHEN  ((agidt.DateValue) - (dewd.DateValue))  >4 AND  (pl.PlantCode) <>'ES10' THEN 0 
		WHEN  ((agidt.DateValue) - (dewd.DateValue) ) < -7 AND (pl.PlantCode)<>'ES10' THEN 0 
		
		WHEN ((agidt.DateValue) - (dewd.DateValue) ) >0 AND  (pl.PlantCode) ='ES10' THEN 0 
		WHEN ((agidt.DateValue) - (dewd.DateValue) ) < -12 AND  (pl.PlantCode) ='ES10' THEN 0 
		
		ELSE 100 END) ,1,1) = 0 then 1 else 0 end) as x
from fact_salesorder fso,
	dim_date dewd,
	dim_date agidt,
	dim_plant pl
where fso.dim_plantid = pl.dim_plantid
and fso.dim_dateidworksdate = dewd.dim_dateid
and fso.dim_dateidactualgi = agidt.dim_dateid
and ct_ontime <> (case when substr ( (CASE 
		WHEN  ((agidt.DateValue) - (dewd.DateValue))  >4 AND  (pl.PlantCode) <>'ES10' THEN 0 
		WHEN  ((agidt.DateValue) - (dewd.DateValue) ) < -7 AND (pl.PlantCode)<>'ES10' THEN 0 
		WHEN ((agidt.DateValue) - (dewd.DateValue) ) >0 AND  (pl.PlantCode) ='ES10' THEN 0 
		WHEN ((agidt.DateValue) - (dewd.DateValue) ) < -12 AND  (pl.PlantCode) ='ES10' THEN 0 
		ELSE 100 END) ,1,1) = 0 then 1 else 0 end);

merge into fact_salesorder fso using
	(select fact_salesorderid, x 
	 from tmp_countmiss t, fact_salesorder fso
	where fso.dd_salesdocno = t.dd_salesdocno
	and fso.dd_salesitemno = t.dd_salesitemno
	and fso.dd_scheduleno = t.dd_scheduleno) upd
on upd.fact_salesorderid = fso.fact_salesorderid
when matched then update
set ct_countmiss = upd.x;

drop table if exists tmp_countmiss;
/* 24 Nov 2016 End of changes*/

 /* 24 Nov 2016 Georgiana Changes according to BI-4798*/
  
drop table if exists tmp_Dim_specialstockind_17 ;
create table tmp_Dim_specialstockind_17  as
select distinct fact_salesorderid,dc.dim_customerid,
row_number() over (partition by fact_salesorderid order by '') as rn
from fact_salesorder so, VBAK_VBAP_VBEP shi, vbpa sdp, dim_customer dc
where so.dd_SalesDocNo = shi.vbak_vbeln
   AND so.dd_SalesItemNo = shi.vbap_posnr
   AND so.dd_scheduleno = shi.vbep_etenr
AND  sdp.vbpa_vbeln = shi.vbak_vbeln
AND dc.customernumber = VBPA_KUNNR
AND sdp.vbpa_parvw = 'SB'
AND so.dd_ScheduleNo <> 0;


update fact_salesorder so
set so.DIM_SpecialStockPartnerid = ifnull(t.dim_customerid,1)
from tmp_Dim_specialstockind_17 t, fact_salesorder so
where so.fact_salesorderid = t.fact_salesorderid
and rn=1
 and so.dim_SalesrepresentativeSP <> ifnull(t.dim_customerid,1);
 drop table if exists tmp_Dim_specialstockind_17 ;
/*24 Nov End of changes*/

/*24 Nov 2016 Georgiana chnages according to BI-4823*/

update fact_salesorder 
set ct_countopenorderamount=0;
/*
drop table if exists tmp_count_for_3P_Lines
create table tmp_count_for_3P_Lines as
select distinct f_so.dd_salesdocno, f_so.dd_salesitemno, f_so.dd_scheduleno,
count(case when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when sdt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC')  AND f_so.dd_ItemRelForDelv = 'X' then (case when (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 then 0.0000 else (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) end * f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)) else (case when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000 else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) end * f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)) end) as Count_OOA
 from fact_salesorder f_so,dim_salesdocumenttype sdt
where ct_ScheduleQtySalesUnit>0
group by f_so.dd_salesdocno, f_so.dd_salesitemno, f_so.dd_scheduleno

merge into fact_salesorder f
using (select distinct f.fact_salesorderid, p.Count_OOA
from fact_salesorder f,tmp_count_for_3P_Lines p
where 
f.dd_salesdocno=p.dd_salesdocno 
and f.dd_salesitemno=p.dd_salesitemno
and f.dd_scheduleno=p.dd_scheduleno
and f.ct_countopenorderamount <> p.Count_OOA) t
on f.fact_salesorderid=t.fact_salesorderid
when matched then update set f.ct_countopenorderamount = t.Count_OOA*/

/*12 Sep 2017 Georgiana - Rewrite the above statement in order to optimize it as is taking 1h to complete after AWS migration*/

drop table if exists tmp_count_for_3P_Lines_a;
create table tmp_count_for_3P_Lines_a as
select distinct f_so.dd_salesdocno, f_so.dd_salesitemno, f_so.dd_scheduleno,
 count(case when f_so.Dim_SalesOrderRejectReasonid <> 1 then 0.0000 when f_so.dd_ItemRelForDelv = 'X' then (case when (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) < 0 then 0.0000 else (f_so.ct_ScheduleQtySalesUnit - (f_so.ct_ShippedAgnstOrderQty - f_so.ct_CmlQtyReceived)) end * f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)) else (case when (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) < 0 then 0.0000 else (f_so.ct_ScheduleQtySalesUnit - f_so.ct_ShippedAgnstOrderQty) end * f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END)) end) as Count_OOA
from fact_salesorder f_so
where ct_ScheduleQtySalesUnit>0
group by f_so.dd_salesdocno, f_so.dd_salesitemno, f_so.dd_scheduleno;


drop table if exists tmp_count_for_3P_Lines_b;
create table tmp_count_for_3P_Lines_b as 
select count(*) as Count_OOB from dim_salesdocumenttype sdt;

merge into fact_salesorder f
using (
select distinct f.fact_salesorderid, a.Count_OOA*b.Count_OOB as count_ab
from fact_salesorder f,tmp_count_for_3P_Lines_a a,tmp_count_for_3P_Lines_b b
where 
f.dd_salesdocno=a.dd_salesdocno 
and f.dd_salesitemno=a.dd_salesitemno
and f.dd_scheduleno=a.dd_scheduleno
and f.ct_countopenorderamount <> a.Count_OOA*b.Count_OOB) t
on f.fact_salesorderid=t.fact_salesorderid
when matched then update set f.ct_countopenorderamount=t.count_ab;


/*24 Nov 2016 End of changes*/


/*08 Dec 2016 Alin Changes according to BI-2104 */
drop table if exists tmp_CreditRelDate1;
create table tmp_CreditRelDate1 as
select SUBSTR(cr.CDPOS_TABKEY, 4, 10) SalesDocNo, CDPOS_OBJECTID, MIN(Cdpos_ChangeNr) Cdpos_ChangeNr
from cdpos_rejdate cr
group by SUBSTR(cr.CDPOS_TABKEY, 4, 10), CDPOS_OBJECTID;

drop table if exists tmp_CreditRelDate2;
create table tmp_CreditRelDate2 as
select cr1.SalesDocNo, cr1.Cdpos_ChangeNr, cv.CDHDR_UDATE, cr1.CDPOS_OBJECTID
from tmp_CreditRelDate1 cr1 inner join cdhdr_vbak cv 
on cr1.cdpos_changenr = cv.cdhdr_changenr;

merge into fact_salesorder so
using
(
select distinct fact_salesorderid, 
first_value(dd.dim_dateid) over (partition by dd.datevalue order by dd.datevalue) as dim_dateid
from fact_salesorder so, tmp_CreditRelDate2 cr, dim_date dd, dim_company dc,dim_plant pl
where  
--so.dd_SalesDocNo = cr.SalesDocNo 
SO.dd_SalesDocNo = cr.CDPOS_OBJECTID
and dd.datevalue=  ifnull(cr.CDHDR_UDATE,'0001-01-01')
and dc.dim_companyid = so.dim_companyid
and dc.companycode = dd.companycode
and so.dim_plantid=pl.dim_plantid and dd.plantcode_factory=pl.plantcode
) t
on so.fact_salesorderid = t.fact_salesorderid
when matched then update 
set so.dim_dateidcreditreleasedt = t.dim_dateid
where so.dim_dateidcreditreleasedt <> t.dim_dateid;

drop table if exists tmp_CreditRelDate1;
drop table if exists tmp_CreditRelDate2;
/*08 Dec 2016 Alin Changes according to BI-2104 */

/* Alin - 12 Jan 2017 - changes according to BI-5039 */
drop table if exists temp_avg_hit_miss_lifr; 

create table temp_avg_hit_miss_lifr as
select distinct t.ProductFamily_Merck, t.CalendarMonthID,
AVG(ifnull(t.sum_ct_lifr_last_2y,0)) OVER (PARTITION BY t.ProductFamily_Merck) as avg_hit_miss_lifr
from (
select  distinct  d.ProductFamily_Merck, dd.CalendarMonthID, 
CASE WHEN  (((COUNT(distinct case when f_so.dd_OTIFCalcFlag_Merck = 'Y' 
and f_so.dd_solineotif_merck = 'Y' then concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo) else 'Not Set' end) 
- CASE WHEN SUM(distinct case when f_so.dd_OTIFCalcFlag_Merck = 'Y' and f_so.dd_solineotif_merck = 'Y' 
then 0.0000 else 1.0000 end) > 0.0000 THEN 1.0000 ELSE 0.0000 END)
/ (CASE WHEN (COUNT(distinct case when f_so.dd_OTIFCalcFlag_Merck = 'Y' 
then concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo) else 'Not Set' end) 
		- CASE WHEN SUM(distinct case when f_so.dd_OTIFCalcFlag_Merck = 'Y' 
		then 0.0000 else 1.0000 end) > 0.0000 THEN 1.0000 ELSE 0.0000 END) = 0.0000 THEN 1.0000
	ELSE (COUNT(distinct case when f_so.dd_OTIFCalcFlag_Merck = 'Y' 
	then concat(f_so.dd_SalesDocNo,f_so.dd_SalesItemNo) else 'Not Set' end) 
	- CASE WHEN SUM(distinct case when f_so.dd_OTIFCalcFlag_Merck = 'Y' then 
	0.0000 else 1.0000 end) > 0.0000 THEN 1.0000 ELSE 0.0000 END) END)) * 100)  > 95
     THEN 100
ELSE 0 END as sum_ct_lifr_last_2y
from fact_salesorder f_so, dim_part d, dim_date dd
where f_so.dim_partid = d.dim_partid
and f_so.dim_dateidexpgi_merck = dd.dim_dateid
and dd.datevalue >= current_date - interval '24'  month
and dd.datevalue <= current_date - interval '1'  month
group by d.ProductFamily_Merck, dd.CalendarMonthID
order by dd.CalendarMonthID desc
) t;

update fact_salesorder a
set a.ct_avg_hit_miss_lifr = round(e.avg_hit_miss_lifr)
from dim_part b, dim_date d, temp_avg_hit_miss_lifr e, fact_salesorder a
where a.dim_partid = b.dim_partid
and a.dim_dateidexpgi_merck = d.dim_dateid
and d.CalendarMonthID = e.CalendarMonthID
and d.datevalue >= current_date - interval '24'  month
and d.datevalue <= current_date - interval '1'  month
and ifnull(b.productfamily_merck,'Not Set') = ifnull(e.productfamily_merck,'Not Set')
and a.ct_avg_hit_miss_lifr <> round(e.avg_hit_miss_lifr);

drop table if exists temp_avg_hit_miss_lifr; 
/* Alin End of Changes BI-5039*/

/*06 Feb 2017 Georgiana Changes according to BI-5446*/

UPDATE fact_salesorder so
SET so.dd_purchaseordertype = ifnull(vkd.vbkd_bsark_e,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM vbak_vbap_vbkd vkd, fact_salesorder so
WHERE so.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND so.dd_SalesItemNo  =  vkd.VBKD_POSNR
AND so.dd_purchaseordertype <> ifnull(vkd.vbkd_bsark_e,'Not Set');

/* 06 Feb 2017 End of changes*/

drop table if exists tmp_JCDS_VBAP_for_update3;

/* 21 Jul 2017 Georgiana Changes according to APP-6977 adding an extra update statement in order to avoid refresh issues */

merge into fact_salesorder f
using (select distinct fact_salesorderid,dim_dateid
from fact_salesorder f,dim_date dt ,vbak_vbap, dim_plant pl
where vbap_vbeln=dd_SalesDocNo
and vbap_posnr=dd_SalesItemNo
and f.dim_plantid=pl.dim_plantid
and pl.plantcode=dt.plantcode_factory
and pl.companycode=dt.companycode
and dt.datevalue=vbak_gueen
and f.Dim_DateidValidTo<>dt.dim_dateid) t
on t.fact_salesorderid=f.fact_salesorderid
when matched then update set Dim_DateidValidTo=t.dim_dateid;

/* 21 Jul 2017 End of changes*/

/* Add APP-7184 - Sales: add new field name of orderer */
update fact_salesorder fso
	set fso.dd_NameOfOrderer = ifnull(vvv.VBAK_BNAME,'Not Set')
From fact_salesorder fso,vbak_vbap_vbep vvv
	where fso.dd_SalesDocNo = vvv.VBAK_VBELN
		and fso.dd_SalesItemNo = vvv.VBAP_POSNR
		and fso.dd_ScheduleNo = vvv.VBEP_ETENR
		and fso.dd_NameOfOrderer <> ifnull(vvv.VBAK_BNAME,'Not Set');

drop table if exists fact_salesorder_tmptbl;

/*Octavian S 15-FEB-2018 APP-6837 Add new attributes*/
--Info for Customer / i2i
 MERGE INTO fact_salesorder so
 USING (
        SELECT DISTINCT so.dd_salesdocno,so.dd_salesitemno,shorttext
          FROM (SELECT DISTINCT stxh_tdname, 
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                   FROM  stxh s 
                  WHERE stxh_tdobject LIKE '%VBBP%' 
                    AND stxh_tdid     = 'Z009'
                  GROUP BY stxh_tdname) s
                 ,fact_salesorder       so
          WHERE TRIM(substr(stxh_tdname,1,10))                   = so.dd_salesdocno 
            AND TRIM(leading '0' FROM substr(stxh_tdname,11,20)) = so.dd_salesitemno
       )t
   ON t.dd_salesdocno  = so.dd_salesdocno 
  AND t.dd_salesitemno = so.dd_salesitemno
 WHEN MATCHED 
 THEN UPDATE SET dd_lONgtext_stxh = IFNULL(t.shorttext,'Not Set');

--Info for COP (NP) 
 MERGE INTO fact_salesorder so
 USING (
        SELECT DISTINCT so.dd_salesdocno,so.dd_salesitemno,shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%VBBP%' 
                   AND stxh_tdid     = 'Z007'
                 GROUP BY stxh_tdname)      s
                ,fact_salesorder            so
         WHERE TRIM(substr(stxh_tdname,1,10))                   = so.dd_salesdocno 
           AND TRIM(LEADING '0' FROM substr(stxh_tdname,11,20)) = so.dd_salesitemno
        )t
   ON t.dd_salesdocno  = so.dd_salesdocno 
  AND t.dd_salesitemno = so.dd_salesitemno
 WHEN MATCHED 
 THEN UPDATE SET dd_lONgtext_stxh_info_COP = IFNULL(t.shorttext,'Not Set');

--Gen cust. info. for COP (NP)
 MERGE INTO fact_salesorder so
 USING (
        SELECT DISTINCT so.dd_salesdocno,so.dd_salesitemno,shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%VBBP%' 
                   AND stxh_tdid     = 'Z011'
                 GROUP BY stxh_tdname)      s
                 ,fact_salesorder           so
         WHERE TRIM(substr(stxh_tdname,1,10))                   = so.dd_salesdocno 
           AND TRIM(LEADING '0' FROM substr(stxh_tdname,11,20)) = so.dd_salesitemno
       )t
   ON t.dd_salesdocno  = so.dd_salesdocno 
  AND t.dd_salesitemno = so.dd_salesitemno
 WHEN MATCHED 
 THEN UPDATE SET dd_lONgtext_stxh_gen_info_COP = IFNULL(t.shorttext,'Not Set');

--Item Proposal Information
 MERGE INTO fact_salesorder so
 USING (
        SELECT DISTINCT so.dd_salesdocno,so.dd_salesitemno,shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%VBBP%' 
                   AND stxh_tdid     = 'Z012'
                 GROUP BY stxh_tdname)      s
                 ,fact_salesorder           so
         WHERE TRIM(substr(stxh_tdname,1,10))                   = so.dd_salesdocno 
           AND TRIM(LEADING '0' FROM substr(stxh_tdname,11,20)) = so.dd_salesitemno
       )t
   ON t.dd_salesdocno  = so.dd_salesdocno 
  AND t.dd_salesitemno = so.dd_salesitemno
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_stxh_item_prop_info = IFNULL(t.shorttext,'Not Set');

--Info for Delivery Note (NL)
 MERGE INTO fact_salesorder so
 USING (
        SELECT DISTINCT so.dd_salesdocno,so.dd_salesitemno,shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%VBBP%' 
                   AND stxh_tdid     = 'Z003'
                 GROUP BY stxh_tdname)      s
                 ,fact_salesorder           so
         WHERE TRIM(substr(stxh_tdname,1,10))                   = so.dd_salesdocno 
           AND TRIM(LEADING '0' FROM substr(stxh_tdname,11,20)) = so.dd_salesitemno
       )t
   ON t.dd_salesdocno  = so.dd_salesdocno 
  AND t.dd_salesitemno = so.dd_salesitemno
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_stxh_info_deliv_note = IFNULL(t.shorttext,'Not Set');

--Info for Invoice (NL)
 MERGE INTO fact_salesorder so
 USING (
        SELECT DISTINCT so.dd_salesdocno,so.dd_salesitemno,shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%VBBP%' 
                   AND stxh_tdid     = 'Z004'
                 GROUP BY stxh_tdname)      s
                 ,fact_salesorder          so
         WHERE TRIM(substr(stxh_tdname,1,10))                   = so.dd_salesdocno 
           AND TRIM(LEADING '0' FROM substr(stxh_tdname,11,20)) = so.dd_salesitemno
      )t
   ON t.dd_salesdocno  = so.dd_salesdocno 
  AND t.dd_salesitemno = so.dd_salesitemno
  WHEN MATCHED
 THEN UPDATE SET dd_longtext_stxh_info_invoice = IFNULL(t.shorttext,'Not Set');

 /*Alin 21 feb 2018 APP-8910*/
merge into fact_salesorder f
using(
select distinct fact_salesorderid,  ifnull(m.marc_dispo, 'Not Set') as mrp_controller
from fact_salesorder f
inner join dim_part dp
on f.dim_partid = dp.dim_partid
inner join dim_customer c
on f.dim_customerid = c.dim_customerid
inner join vbak_vbap_vbep v
on ifnull(v.vbak_kunnr, 'Not Set') =  c.customernumber
and ifnull(VBAK_VBELN, 'Not Set') = dd_SalesDocNo
AND ifnull(VBAP_POSNR, 0)  =  dd_SalesItemNo
and ifnull(VBEP_ETENR, 0)  =  dd_ScheduleNo
inner join kna1 k
on v.vbak_kunnr = k.kna1_kunnr
inner join t001w t
 on t.kunnr = k.kna1_kunnr
and (case when k.KNA1_KUNNR like '%2000480'  then t.pstlz = k.kna1_pstlz else 1=1 end)
inner join mara_marc_makt m
on t.werks = m.marc_werks
and dp.partnumber = ifnull(m.mara_matnr, 'Not Set')
) t
on t.fact_salesorderid = f.fact_salesorderid
when matched then update
set f.DD_MRPCONTROLLER = t.mrp_controller
where f.DD_MRPCONTROLLER <> t.mrp_controller;