/* ################################################################################################################## */
/* */
/*   Script         : exa_bi_populate_ceesa_fact */ 
/*   Author         : Andrei R */
/*   Created On     : 20 Apr 2017 */
/*  */
/* ################################################################################################################## */

DROP TABLE IF EXISTS tmp_gblcurr_fap;

Create table tmp_gblcurr_ces(
pGlobalCurrency varchar(3) null);

Insert into tmp_gblcurr_ces(pGlobalCurrency) values(null);

Update tmp_gblcurr_ces
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');



DELETE FROM CEESA;
 

IMPORT INTO CEESA FROM CSV
AT DC1STGC1INT01CONNECTION
FILE '/home/fusionops/ispring_clustered_storage/configurations/MerckDE6/files/Ceesa/ceesa_20170419.csv'
COLUMN SEPARATOR = '|' 
ROW SEPARATOR = 'CRLF'
ENCODING = 'ISO-8859-1'
SKIP = 1;

delete from number_fountain m where m.table_name = 'FACT_CEESA';
insert into number_fountain
select 'FACT_CEESA',
ifnull(max(f.FACT_CEESAID),
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from FACT_CEESA f;


/*In order to have the data multiplicated for each value of dd_yearsforcagr column we have created a misc table which has only one column with the three values requeste by Violi 2,3,4 in this way we are avoiding multiple insert statements in ther future*/

INSERT INTO FACT_CEESA (fact_ceesaid,
  dd_year,
  dd_quarter,
  dd_country,
  dd_region,
  dd_subregion,
  dd_company,
  dd_companycode,
  dd_global_product_name,
  dd_productcode,
  dd_originalceesacode,
  dd_ceesaclasslevel1maintc,
  dd_ceesaclasslevel2species,
  dd_singlespeciesname,
  dd_species,
  dd_speciescode,
  dd_currencyloc,
  amt_saleslocquarter,
  amt_saleslocytd,
  amt_saleslocmat,
  ct_salesusdquarter,
  amt_salesusdquarter2,
  ct_salesusdytd,
  amt_salesusdytd2,
  ct_salesusdmat,
  amt_salesusdmat2,
  dd_yearsforcagr,
DW_INSERT_DATE)
SELECT  (SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'FACT_CEESA') + row_number() over(order by '') 
 as FACT_CEESAID,
 IFNULL(finyear,'0000') AS dd_year,
 IFNULL(quarter,0) AS dd_quarter,
 IFNULL(country,'Not Set') AS dd_country,
 IFNULL(region,'Not Set') AS dd_region,
 IFNULL(subregion,'Not Set') AS dd_subregion,
 IFNULL(company,'Not Set') AS dd_company,
 IFNULL(companycode,'Not Set') AS dd_companycode,
 IFNULL(global_product_name,'Not Set') AS dd_global_product_name,
 IFNULL(productcode,'Not Set') AS dd_productcode,
 IFNULL(originalceesacode,'Not Set') AS dd_originalceesacode,
 IFNULL(ceesaclasslevel1maintc,'Not Set') AS dd_ceesaclasslevel1maintc,
 IFNULL(ceesaclasslevel2species,'Not Set') AS dd_ceesaclasslevel2species,
 IFNULL(singlespeciesname,'Not Set') AS dd_singlespeciesname,
 IFNULL(species,'Not Set') AS dd_species,
 IFNULL(speciescode,'Not Set') AS dd_speciescode,
 IFNULL(currencyloc,'Not Set') AS dd_currencyloc,
 IFNULL(saleslocquarter,0) AS amt_saleslocquarter,
 IFNULL(saleslocytd,0) AS amt_saleslocytd,
 IFNULL(saleslocmat,0) AS amt_saleslocmat,
 IFNULL(salesusdquarter,0) AS ct_salesusdquarter,
 IFNULL(salesusdquarter2,0) AS amt_salesusdquarter2,
 IFNULL(salesusdytd,0) AS ct_salesusdytd,
 IFNULL(salesusdytd2,0) AS amt_salesusdytd2,
 IFNULL(salesusdmat,0) AS ct_salesusdmat,
 IFNULL(salesusdmat2,0) AS amt_salesusdmat2,
 t.yearsforcagr as dd_yearsforcagr,
 current_timestamp AS DW_INSERT_DATE
 from CEESA C
,yearsforcagr_misc t
 WHERE NOT EXISTS(SELECT 1  FROM FACT_CEESA F where 
 IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') ) ;

 
 merge into fact_ceesa f
using(
select distinct a.fact_ceesaid,b.dim_dateid from fact_ceesa a, dim_date b
where a.dd_year=b.calendaryear
and a.dd_quarter=b.calendarquarter
and b.companycode='Not Set'
and plantcode_factory='Not Set'
and b.datevalue=to_date(to_char(concat(dd_year, case when dd_quarter='1' 
then '-01-01' else case when dd_quarter='2' 
then '-04-01' else case when dd_quarter='3' then '-07-01' else 
case when dd_quarter='4' then '-10-01' end end end end)),'YYYY-MM-DD')) t 
on f.fact_ceesaid = t.fact_ceesaid
when matched then update set f.dim_dateid_period  = t.dim_dateid;


update fact_ceesa f
set f.amt_saleslocquarter = ifnull(c.saleslocquarter,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.amt_saleslocquarter <> ifnull(c.saleslocquarter,0);
 
 
 update fact_ceesa f
set f.amt_saleslocytd = ifnull(c.saleslocytd,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.amt_saleslocytd <> ifnull(c.saleslocytd,0);
 
 
 
 update fact_ceesa f
set f.amt_saleslocmat = ifnull(c.saleslocmat,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.amt_saleslocmat <> ifnull(c.saleslocmat,0);
 
 
 update fact_ceesa f
set f.ct_salesusdquarter = ifnull(c.salesusdquarter,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.ct_salesusdquarter <> ifnull(c.salesusdquarter,0);
 
 
 
 update fact_ceesa f
set f.amt_salesusdquarter2 = ifnull(c.salesusdquarter2,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.amt_salesusdquarter2 <> ifnull(c.salesusdquarter2,0);
 
 
 
 update fact_ceesa f
set f.ct_salesusdytd = ifnull(c.salesusdytd,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.ct_salesusdytd <> ifnull(c.salesusdytd,0);
 
 
 
 update fact_ceesa f
set f.amt_salesusdytd2 = ifnull(c.salesusdytd2,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.amt_salesusdytd2 <> ifnull(c.salesusdytd2,0);
 
 
 
 
 update fact_ceesa f
set f.ct_salesusdmat = ifnull(c.salesusdmat,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.ct_salesusdmat <> ifnull(c.salesusdmat,0);
 
 
 
 
 update fact_ceesa f
set f.amt_salesusdmat2 = ifnull(c.salesusdmat2,0)
from fact_ceesa f,ceesa c
where
IFNULL(finyear,'0000') = ifnull(f.dd_year,'0000') and
 IFNULL(quarter,0) = ifnull(f.dd_quarter,0) and
 IFNULL(country,'Not Set') = ifnull(f.dd_country,'Not Set') and
 IFNULL(region,'Not Set') = ifnull(f.dd_region,'Not Set') and
 IFNULL(subregion,'Not Set') = ifnull(f.dd_subregion,'Not Set') and
 IFNULL(company,'Not Set') = ifnull(f.dd_company,'Not Set') and
 IFNULL(companycode,'Not Set') = ifnull(f.dd_companycode,'Not Set') and
 IFNULL(global_product_name,'Not Set') = ifnull(f.dd_global_product_name,'Not Set') and
 IFNULL(productcode,'Not Set') = ifnull(f.dd_productcode,'Not Set') and
 IFNULL(originalceesacode,'Not Set') = ifnull(f.dd_originalceesacode,'Not Set') and
 IFNULL(ceesaclasslevel1maintc,'Not Set') = ifnull(f.dd_ceesaclasslevel1maintc,'Not Set') and
 IFNULL(ceesaclasslevel2species,'Not Set') = ifnull(f.dd_ceesaclasslevel2species,'Not Set') and
 IFNULL(singlespeciesname,'Not Set') = ifnull(f.dd_singlespeciesname,'Not Set') and
 IFNULL(species,'Not Set') = ifnull(f.dd_species,'Not Set') and
 IFNULL(speciescode,'Not Set') = ifnull(f.dd_speciescode,'Not Set') and
 IFNULL(currencyloc,'Not Set') = ifnull(f.dd_currencyloc,'Not Set') and
 f.amt_salesusdmat2 <> ifnull(c.salesusdmat2,0);
 
/* Andrei Robescu 2 Jun 2017 APP-6379 Add Compound Annual Growth */

drop table if exists tmp_ctsalescalculated;
create table tmp_ctsalescalculated as 

select distinct dd_company, dd_year, dd_quarter, dd_yearsforcagr , dd_region,  sum(ct_salesusdquarter) as ct_salesusdquarterinitial
from fact_ceesa
group by dd_company, dd_year, dd_quarter, dd_yearsforcagr, dd_region ;



drop table if exists tmp_compound;
create table tmp_compound as

 select distinct dd_company, dd_year, dd_quarter, dd_yearsforcagr , dd_region, ct_salesusdquarterinitial, 
lag(ct_salesusdquarterinitial,2) over (partition by dd_company, dd_region, dd_quarter, dd_yearsforcagr
 order by  dd_year ) as ct_salesusdquarter2
from  tmp_ctsalescalculated 
where dd_yearsforcagr = 2;


drop table if exists tmp_compound2;
create table tmp_compound2 as

 select distinct dd_company, dd_year, dd_quarter, dd_yearsforcagr , dd_region, ct_salesusdquarterinitial, 
lag(ct_salesusdquarterinitial,3) over (partition by dd_company, dd_region, dd_quarter, dd_yearsforcagr
 order by  dd_year ) as ct_salesusdquarter3
from  tmp_ctsalescalculated 
where dd_yearsforcagr = 3;


drop table if exists tmp_compound3;
create table tmp_compound3 as

 select distinct dd_company, dd_year, dd_quarter, dd_yearsforcagr , dd_region, ct_salesusdquarterinitial, 
lag(ct_salesusdquarterinitial,4) over (partition by dd_company, dd_region, dd_quarter, dd_yearsforcagr
 order by  dd_year ) as ct_salesusdquarter4
from  tmp_ctsalescalculated 
where dd_yearsforcagr = 4;



drop table if exists tmp_ctfinal;
create table tmp_ctfinal as 

select distinct dd_company, dd_year, dd_quarter, dd_yearsforcagr , dd_region, ct_salesusdquarterinitial, ct_salesusdquarter2, 
case when ct_salesusdquarter2 is null then 0
else 
100*(power((ct_salesusdquarterinitial/ct_salesusdquarter2),(1/dd_yearsforcagr))-1) end as CT_COMPOUNDANNUAL
from  tmp_compound;


drop table if exists tmp_ctfinal2;
create table tmp_ctfinal2 as 

select distinct dd_company, dd_year, dd_quarter, dd_yearsforcagr , dd_region, ct_salesusdquarterinitial, ct_salesusdquarter3, 
case when ct_salesusdquarter3 is null then 0
else 
100*(power((ct_salesusdquarterinitial/ct_salesusdquarter3),(1/dd_yearsforcagr))-1) end as CT_COMPOUNDANNUAL
from  tmp_compound2;



drop table if exists tmp_ctfinal3;
create table tmp_ctfinal3 as 

select distinct dd_company, dd_year, dd_quarter, dd_yearsforcagr , dd_region, ct_salesusdquarterinitial, ct_salesusdquarter4, 
case when ct_salesusdquarter4 is null then 0
else 
100*(power((ct_salesusdquarterinitial/ct_salesusdquarter4),(1/dd_yearsforcagr))-1) end as CT_COMPOUNDANNUAL
from  tmp_compound3;



update fact_ceesa f
set f.CT_COMPOUNDANNUAL = t.CT_COMPOUNDANNUAL
from fact_ceesa f, tmp_ctfinal t 
where 
f.dd_company = t.dd_company 
and f.dd_year = t.dd_year
and f.dd_quarter = t.dd_quarter
and f.dd_yearsforcagr = t.dd_yearsforcagr
and f.dd_region = t.dd_region
and f.CT_COMPOUNDANNUAL <> t.CT_COMPOUNDANNUAL;


update fact_ceesa f
set f.CT_COMPOUNDANNUAL = t.CT_COMPOUNDANNUAL
from fact_ceesa f, tmp_ctfinal2 t 
where 
f.dd_company = t.dd_company 
and f.dd_year = t.dd_year
and f.dd_quarter = t.dd_quarter
and f.dd_yearsforcagr = t.dd_yearsforcagr
and f.dd_region = t.dd_region
and f.CT_COMPOUNDANNUAL <> t.CT_COMPOUNDANNUAL;



update fact_ceesa f
set f.CT_COMPOUNDANNUAL = t.CT_COMPOUNDANNUAL
from fact_ceesa f, tmp_ctfinal3 t 
where 
f.dd_company = t.dd_company 
and f.dd_year = t.dd_year
and f.dd_quarter = t.dd_quarter
and f.dd_yearsforcagr = t.dd_yearsforcagr
and f.dd_region = t.dd_region
and f.CT_COMPOUNDANNUAL <> t.CT_COMPOUNDANNUAL;





/*  END  Andrei Robescu 2 Jun 2017 APP-6379 Add Compound Annual Growth */


/*Alin APP-6381 14 Jun 2017*/
/*Sales USD Quarter (Without exch. rate effect) PY*/
merge into fact_ceesa f 
using (
select  dd_year + 1 as prevyear, dd_quarter,dd_yearsforcagr,dd_company,dd_region, dd_subregion, dd_country, dd_ceesaclasslevel1maintc,
dd_ceesaclasslevel2species, dd_singlespeciesname, dd_global_product_name, dd_originalceesacode, dd_productcode, sum(CT_SALESUSDQUARTER) as marksalusd
from fact_ceesa f
group by dd_year, dd_quarter, dd_yearsforcagr,dd_company,dd_region, dd_subregion, dd_country, dd_ceesaclasslevel1maintc,
dd_ceesaclasslevel2species, dd_singlespeciesname, dd_global_product_name, dd_originalceesacode, dd_productcode) t
on f.dd_year = t.prevyear and f.dd_quarter=t.dd_quarter and f.dd_yearsforcagr = t.dd_yearsforcagr and f.dd_company = t.dd_company
and f.dd_region = t.dd_region and f.dd_subregion = t.dd_subregion and f.dd_country = t.dd_country 
and f.dd_ceesaclasslevel1maintc = t.dd_ceesaclasslevel1maintc and f.dd_ceesaclasslevel2species = t.dd_ceesaclasslevel2species 
and f.dd_singlespeciesname = t.dd_singlespeciesname and f.dd_global_product_name = t.dd_global_product_name and f.dd_originalceesacode = t.dd_originalceesacode and f.dd_productcode = t.dd_productcode
when matched then update
set f.ct_marksalusd_py = t.marksalusd
where f.ct_marksalusd_py <> t.marksalusd;

/*Market Sales USD Quarter (Without exch. rate effect)*/
merge into fact_ceesa f 
using (
select  dd_year, dd_quarter,dd_yearsforcagr,dd_company,dd_region, dd_subregion, dd_country, dd_ceesaclasslevel1maintc,
dd_ceesaclasslevel2species, dd_singlespeciesname, dd_global_product_name, dd_originalceesacode, dd_productcode, sum(CT_SALESUSDQUARTER) as marksalusd
from fact_ceesa f
group by dd_year, dd_quarter, dd_yearsforcagr,dd_company,dd_region, dd_subregion, dd_country, dd_ceesaclasslevel1maintc,
dd_ceesaclasslevel2species, dd_singlespeciesname, dd_global_product_name, dd_originalceesacode, dd_productcode) t
on f.dd_year = t.dd_year and f.dd_quarter=t.dd_quarter and f.dd_yearsforcagr = t.dd_yearsforcagr and f.dd_company = t.dd_company
and f.dd_region = t.dd_region and f.dd_subregion = t.dd_subregion and f.dd_country = t.dd_country 
and f.dd_ceesaclasslevel1maintc = t.dd_ceesaclasslevel1maintc and f.dd_ceesaclasslevel2species = t.dd_ceesaclasslevel2species 
and f.dd_singlespeciesname = t.dd_singlespeciesname and f.dd_global_product_name = t.dd_global_product_name and f.dd_originalceesacode = t.dd_originalceesacode and f.dd_productcode = t.dd_productcode
when matched then update
set f.ct_marksalusd = t.marksalusd
where f.ct_marksalusd <> t.marksalusd;

/*Market Sales USD Quarter (Without exch. rate effect) PY*/
merge into fact_ceesa f 
using (
select  dd_year + 1 as prevyear, dd_quarter,dd_yearsforcagr,dd_company,dd_region, dd_subregion, dd_country, dd_ceesaclasslevel1maintc,
dd_ceesaclasslevel2species, dd_singlespeciesname, dd_global_product_name, dd_originalceesacode, dd_productcode, sum(CT_SALESUSDQUARTER) as marksalusd
from fact_ceesa f
group by dd_year, dd_quarter, dd_yearsforcagr,dd_company,dd_region, dd_subregion, dd_country, dd_ceesaclasslevel1maintc,
dd_ceesaclasslevel2species, dd_singlespeciesname, dd_global_product_name, dd_originalceesacode, dd_productcode) t
on f.dd_year = t.prevyear and f.dd_quarter=t.dd_quarter and f.dd_yearsforcagr = t.dd_yearsforcagr and f.dd_company = t.dd_company
and f.dd_region = t.dd_region and f.dd_subregion = t.dd_subregion and f.dd_country = t.dd_country 
and f.dd_ceesaclasslevel1maintc = t.dd_ceesaclasslevel1maintc and f.dd_ceesaclasslevel2species = t.dd_ceesaclasslevel2species 
and f.dd_singlespeciesname = t.dd_singlespeciesname and f.dd_global_product_name = t.dd_global_product_name and f.dd_originalceesacode = t.dd_originalceesacode and f.dd_productcode = t.dd_productcode
when matched then update
set f.ct_py_usdquarter = t.marksalusd
where f.ct_py_usdquarter <> t.marksalusd;

/* Liviu Ionescu - add exchange rates */

UPDATE fact_ceesa f
SET f.std_exchangerate_dateid = dt.dim_dateid
FROM fact_ceesa f
     INNER JOIN dim_date dt ON dt.companycode = 'Not Set'
						and plantcode_factory = 'Not Set'
						and dt.datevalue = to_date(to_char(concat(f.dd_year, case when f.dd_quarter='1' 
											then '-01-01' else case when f.dd_quarter='2' 
											then '-04-01' else case when f.dd_quarter='3' then '-07-01' else 
											case when f.dd_quarter='4' then '-10-01' end end end end)),'YYYY-MM-DD')
		where f.std_exchangerate_dateid <> dt.dim_dateid;

UPDATE fact_ceesa f
	set f.amt_exchangerate_GBL = z.exchangeRate
from fact_ceesa f, tmp_getExchangeRate1 z, tmp_gblcurr_ces ces
	where f.dd_CurrencyLoc = z.pFromCurrency
		and z.fact_script_name = 'bi_populate_ceesa_fact'
		and z.pDate = to_date(to_char(concat(f.dd_year, case when f.dd_quarter='1' 
											then '-01-01' else case when f.dd_quarter='2' 
											then '-04-01' else case when f.dd_quarter='3' then '-07-01' else 
											case when f.dd_quarter='4' then '-10-01' end end end end)),'YYYY-MM-DD')
		and z.pToCurrency = ces.pGlobalCurrency 
		and f.amt_exchangerate_GBL <> z.exchangeRate;

UPDATE fact_ceesa f
	set f.amt_exchangerate = z.exchangeRate
from fact_ceesa f, tmp_getExchangeRate1 z, dim_company dc
	where z.pFromCurrency =  f.dd_CurrencyLoc
		and z.fact_script_name = 'bi_populate_ceesa_fact'
		and z.pDate = to_date(to_char(concat(f.dd_year, case when f.dd_quarter='1' 
											then '-01-01' else case when f.dd_quarter='2' 
											then '-04-01' else case when f.dd_quarter='3' then '-07-01' else 
											case when f.dd_quarter='4' then '-10-01' end end end end)),'YYYY-MM-DD')
		
		and dc.CompanyCode = f.dd_companycode /* there is no data available to get the company currency */
		and z.pToCurrency =  dc.Currency
		and f.amt_exchangerate <> z.exchangeRate;
/*Added Liviu Changes related to currency conversion */
update FACT_CEESA fc
set fc.DIM_CURRENCYID = dc.dim_currencyid
from  FACT_CEESA fc, dim_currency dc
	where dc.currencycode = fc.DD_CURRENCYLOC
		and fc.DIM_CURRENCYID <> dc.dim_currencyid;

update FACT_CEESA fc
set fc.dim_currencyid_tra = dc.dim_currencyid
from  FACT_CEESA fc, dim_currency dc
	where dc.currencycode = fc.DD_CURRENCYLOC
		and fc.dim_currencyid_tra <> dc.dim_currencyid;
		
/* 07 sept 2017 Georgiana Changes  according to APP-6381 adding a duplicate of dd_company for the Market Overview tab*/
update fact_ceesa set dd_company2=dd_company
where dd_company2<>dd_company;
 
 /*18 Oct 2017 Georgiana Changes according to APP-6381 adding an aggregation for ct_marksalusd_py at year,quarter and yearsforcagr level for the EI measure*/
 merge into fact_ceesa f
using (select distinct dd_year,dd_quarter,dd_yearsforcagr,SUM(ct_marksalusd_py) as ct
from fact_ceesa
group by  dd_year,dd_quarter,dd_yearsforcagr) t
on t.dd_year=f.dd_year
and t.dd_quarter=f.dd_quarter
and t.dd_yearsforcagr=f.dd_yearsforcagr
when matched then update set ct_marketsalesUSDwexr_py_MO =t.ct;