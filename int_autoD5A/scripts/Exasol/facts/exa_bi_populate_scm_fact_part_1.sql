/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_scm_fact.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 17 Mar 2017 */
/*  */
/*  */
/*   Description    : Populate SCM Fact */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */


/* instead of drop the temporary fact and recreating -> compare the describes and add the missing columns - this part is moved to exa_bi_populate_scm_create_basic_temp_tables.sql */
/* DROP TABLE IF EXISTS fact_scm_test
create table fact_scm_test 
like fact_mmprodhierarchy INCLUDING DEFAULTS INCLUDING IDENTITY


DROP TABLE IF EXISTS fact_scm
create table fact_scm
like fact_mmprodhierarchy INCLUDING DEFAULTS INCLUDING IDENTITY */

/* ******************************************************************************************************************************************************************************** */
/* ****************************************first generate the temp tables containing the basic material states, along with the basic states transitions *************************** */
/* Source tables for this first part: Material movement, Purchasing, Inspection Lot, Production Order */
/* ******************************************************************************************************************************************************************************** */

drop table if exists tmp_basic_mat_states;
create table tmp_basic_mat_states as
select 'X' as raw, 'X' as antigen, 'X' as bulk, 'X' as fpu, 'X' as fpp,  1 as ct_1;

delete from number_fountain m where m.table_name = 'tmp_basic_mat_states_combinations';
insert into number_fountain
select 'tmp_basic_mat_states_combinations', 1;  /*always recreate from the first rowid*/

drop table if exists tmp_basic_mat_states_combinations;
create table tmp_basic_mat_states_combinations as
select (select max_id  from number_fountain   where table_name = 'tmp_basic_mat_states_combinations') + row_number() over(ORDER BY '') AS tmp_basic_mat_states_combinationsId,
	 	 raw, antigen, bulk, fpu, fpp,  sum(ct_1) as ct_1
	from tmp_basic_mat_states
	group by cube (raw, antigen, bulk, fpu, fpp);
	
/*next script: create_basic_temp_tables.sql*/	