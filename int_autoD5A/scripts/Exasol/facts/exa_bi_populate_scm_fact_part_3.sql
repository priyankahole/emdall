/* ******************************************************************************************************************************************************************************** */
/* ****************************************third step: fact deletions  ************************************************************************************************************ */
/* ******************************************************************************************************************************************************************************** */

/* Only include inspections starting with 1, 3 or 4 - BI-4358 */
/*tmp_transpose will always keep a list of material states, no matter if they were active or not in a specific combination*/

/* at this point, fact_deletes.sql is already run*/

/*Madalina 2 Oct 2017 - fact optimizations - apply the deletion on the temp Shippping (002) table - part_2 script*/
/* BI-4287 - delete returns */
/* merge into fact_scm mmp
using ( select distinct mmp.fact_mmprodhierarchyid from fact_scm mmp, fact_salesorder so
	where dd_ReturnsItem = 'X'
	and mmp.dd_salesdocno_customer = so.dd_salesdocno
	and mmp.dd_salesitemno_customer = so.dd_salesitemno ) del
on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
when matched then delete */

/* ******************************************************************************************************************************************************************************** */
/* ****************************************fourth step: fact updates ************************************************************************************************************** */
/* ******************************************************************************************************************************************************************************** */

update fact_scm_test mmp
set DIM_CURRENCYID_TRA = c.dim_currencyid
from fact_scm_test mmp, dim_currency c
where currencycode = 'TRA'
and DIM_CURRENCYID_TRA <> c.dim_currencyid;

/*Madalina 2 Oct 2017 - fact optimizations - move calculation to Shipping (002) table level, in part_2 script */
/* Adding Sales Order Amount */
/* merge into  fact_scm mmp
using ( select distinct fact_mmprodhierarchyid, 
		first_value((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) ELSE f_so.amt_ScheduleTotal END)) over (partition by fact_mmprodhierarchyid order by '') as amt_orderAmountSales
		from fact_salesorder f_so, fact_scm mmp, dim_salesmisc dsi
		where mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
			and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo
			and f_so.dim_salesmiscid = dsi.dim_salesmiscid ) upd
on mmp.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update
set mmp.amt_orderAmountSales = upd.amt_orderAmountSales */

/* Add amt_ExchangeRate, based on the related on from Sales */
/* merge into fact_scm mmp
using ( select distinct dd_SalesDocNo, dd_SalesItemNo, -- f_so.amt_ExchangeRate - Correct unstable set of rows
	first_value(f_so.amt_ExchangeRate) over (partition by dd_SalesDocNo, dd_SalesItemNo order by '') as amt_ExchangeRate
	from  fact_salesorder f_so ) f_so
on  mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
	and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo 
when matched then update
set  mmp.amt_ExchangeRate = f_so.amt_ExchangeRate */

/* Add amt_ExchangeRate_GBL, based on the related on from Sales */
/* merge into fact_scm mmp
using ( select distinct dd_SalesDocNo, dd_SalesItemNo, -- f_so.amt_ExchangeRate_GBL - Correct unstable set of rows
	first_value(f_so.amt_ExchangeRate_GBL) over (partition by dd_SalesDocNo, dd_SalesItemNo order by '') as amt_ExchangeRate_GBL
	from  fact_salesorder f_so ) f_so
on  mmp.dd_SalesDocNo_customer = f_so.dd_SalesDocNo
	and mmp.dd_SalesItemNo_customer = f_so.dd_SalesItemNo 
when matched then update
set  mmp.amt_ExchangeRate_GBL = f_so.amt_ExchangeRate_GBL */

/* Set Order Sales Amount as Quantity, always expressed in global conversion */
/*update fact_scm
set amt_orderAmountSales = amt_orderAmountSales * amt_ExchangeRate_GBL */

/* BI-4227 Adding Material FPP COMOPS field */
/* update fact_scm mmp
set mmp.dim_partid_comops = fpp_comops.dim_partid
from fact_scm mmp, dim_part fpp, dim_part fpp_comops, dim_plant p_comops
where mmp.dim_partid_fpp_final = fpp.dim_partid 
	and mmp.dim_plantid_comops = p_comops.dim_plantid
	and fpp_comops.partnumber = fpp.partnumber
	and fpp_comops.plant = p_comops.plantcode
	and mmp.dim_partid_comops <> fpp_comops.dim_partid */

/* Add Customer fields */
/* merge into fact_scm mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridShipTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_comops = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_comops = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_comops = upd.dd_salesdocno
	and mmp.dd_salesitemno_comops = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerShipComops = upd.Dim_CustomeridShipTo

merge into fact_scm mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridSoldTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_comops = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_comops = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_comops = upd.dd_salesdocno
	and mmp.dd_salesitemno_comops = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerSoldComops = upd.Dim_CustomeridSoldTo

merge into fact_scm mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridShipTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_customer = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_customer = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_customer = upd.dd_salesdocno
	and mmp.dd_salesitemno_customer = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerShipCust = upd.Dim_CustomeridShipTo

merge into fact_scm mmp
using ( select distinct dd_salesdlvritemno, dd_salesdlvrdocno, dd_salesdocno, dd_salesitemno, Dim_CustomeridSoldTo
		from fact_salesorderdelivery f_sod
		 ) upd
on mmp.dd_salesdlvritemno_customer = upd.dd_salesdlvritemno
	and mmp.dd_salesdlvrdocno_customer = upd.dd_salesdlvrdocno
	and mmp.dd_salesdocno_customer = upd.dd_salesdocno
	and mmp.dd_salesitemno_customer = upd.dd_salesitemno
when matched then update 
set mmp.dim_customerSoldCust = upd.Dim_CustomeridSoldTo */

merge into fact_scm_test fmm using
(select distinct fact_mmprodhierarchyid, 
				danew_usage.dim_dateid as dim_dateidusagedecisionmade_fpp_1
		from fact_scm_test fmm 
				inner join dim_plant pl_usage on fmm.dim_plantid = pl_usage.dim_plantid
				inner join dim_date da_usage on  fmm.dim_dateidusagedecisionmade_fpp_1 = da_usage.dim_dateid
				inner join dim_date danew_usage on da_usage.datevalue = danew_usage.datevalue
							and danew_usage.companycode = pl_usage.companycode
							and danew_usage.plantcode_factory = pl_usage.plantcode 
		) upd
on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid
when matched then update 
set
fmm.dim_dateidusagedecisionmade_fpp_1 = upd.dim_dateidusagedecisionmade_fpp_1;

/* create temp table based on Planned Order */
drop table if exists tmp_PlanOrder_TargetPerPart;
create table tmp_PlanOrder_TargetPerPart as
select partnumber, 
	round(case when avg(df.businessdaysseqno - ds.businessdaysseqno) < '0.5' then 1
		else avg(df.businessdaysseqno - ds.businessdaysseqno) end) as targerMasterRecipe
from fact_planorder fp, dim_date ds, dim_date df, dim_part p
where fp.dd_PlannScenario = '0'
	and fp.dim_dateidstart = ds.dim_dateid
	and ds.datevalue > current_date
	and fp.dim_dateidfinish = df.dim_dateid
	and fp.dim_partid = p.dim_partid
group by partnumber;