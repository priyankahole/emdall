drop table if exists tmp_agent_test;
create table tmp_agent_test
LIKE agent_test INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO tmp_agent_test
SELECT * FROM  agent_test;