/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 20 May 2013 */
/*   Description    : Stored Proc bi_populate_billing_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc  
/*   18 Mar 2016     Georgiana   1.8        Add dd_DeliveryNumber according to BI-2348  */                                                        
/*   4 Mar 2015      Liviu Ionescu  1.7     Add combine for tables fact_salesorderdelivery, tmp_pGlobalCurrency_fact_billing */
/*   24 Feb 2014      Cornelia          Added: Dim_partsalesid */
/*   14 Feb 2014      George    1.5     Added: Dim_CustomerGroup4id */ 
/*   03 Feb 2014      George    1.4     Added: dim_materialpricegroup4id, dim_materialpricegroup5id,dim_CustomerConditionGroups1id,dim_CustomerConditionGroups2id,dim_CustomerConditionGroups3id */
/*   21 Nov 2013      Cornelia  1.4               Enable Material Pricing Group ->Dim_MaterialPricingGroupId				  */
/*   09 Sep 2013      Lokesh	1.3		  Currency and Exchange rate changes. Store tran amts and store Tran->loc and tran->gbl rates */
/*   31 May 2013      Lokesh    1.2       Converted multi-column updates to single-column 						  */
/*   23 May 2013      Lokesh	1.1		  Merged Shanthi's changes from vw_bi_populate_billing_sales_shipment_attributes.sql  */
/*   20 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/*   13 Aug 2015	  Mirela               Update Dim_BillingMiscId*/
/******************************************************************************************************************/



/* ALTER TABLE fact_billing DISABLE KEYS */
/* SET FOREIGN_KEY_CHECKS = 0 */


DROP TABLE IF EXISTS tmp_pGlobalCurrency_fact_billing;
CREATE TABLE tmp_pGlobalCurrency_fact_billing ( pGlobalCurrency VARCHAR(3) NULL);

INSERT INTO tmp_pGlobalCurrency_fact_billing VALUES ( 'USD' );

update tmp_pGlobalCurrency_fact_billing 
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

/* Update 1 - Part 1 - Need to handle getExchangeRate */


/* Update 1 column 1 - ct_BillingQtySalesUOM */

UPDATE fact_billing fb
SET 
ct_BillingQtySalesUOM = VBRP_FKLMG * (VBRP_UMVKN / VBRP_UMVKZ)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode;

/* Update 1 column 2 - ct_BillingQtyStockUOM - NULLABLE column ( hence the ifnull with <> condition ) */

UPDATE fact_billing fb
SET 
       ct_BillingQtyStockUOM = VBRP_FKLMG
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(ct_BillingQtyStockUOM,-1) <> IFNULL(VBRP_FKLMG,-2);


/* Update currency IDs */
/* replaced update with merge to prevent unable to get a stable set of rows error */

merge into fact_billing fbill
using (select distinct fact_billingid, ifnull(cur.dim_CurrencyID,1) as dim_CurrencyID
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,
VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,fact_billing fb 
WHERE dc.CompanyCode = vkp.VBRK_BUKRS 
AND dc.RowIsCurrent = 1 AND cur.CurrencyCode = dc.currency AND dp.PlantCode = vkp.VBRP_WERKS 
AND dp.RowIsCurrent = 1 AND so.SalesOrgCode = vkp.VBRK_VKORG AND pitem.PartNumber = vkp.VBRP_MATNR 
AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS AND ca.ControllingAreaCode = vkp.VBRP_KOKRS 
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR 
AND fb.dim_CurrencyId <> ifnull(cur.dim_CurrencyID,1)
) t
on t.fact_billingid = fbill.fact_billingid
when matched then 
update set fbill.dim_CurrencyId = t.dim_CurrencyID;

/*replaced update with merge for unable to get stable set of rows*/
merge into fact_billing fbill
using (select distinct fact_billingid, ifnull(cur.dim_currencyid,1) as dim_CurrencyID
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,fact_billing fb 
 WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND cur.currencycode = VBRK_WAERK
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
) t
on t.fact_billingid = fbill.fact_billingid
when matched then 
update set fbill.dim_CurrencyId_TRA = t.dim_CurrencyID;


UPDATE fact_billing fb
SET fb.dim_CurrencyId_GBL = ifnull(cur.dim_currencyid,1)	
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch, fact_billing fb	  
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND cur.currencycode = pGlobalCurrency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

/* Update 1 column 3 - amt_ExchangeRate */

/* Update the default value first */
UPDATE fact_billing fb
SET amt_ExchangeRate = 1		
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch, fact_billing fb  
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND amt_ExchangeRate <> 1;


/* Now update the value where a match is found */
/* LK: 8 Sep: amt_ExchangeRate should have tran->local rate. Replaced z.pToCurrency = dc.currency ( from globalcurr ) */
UPDATE fact_billing fb
SET amt_ExchangeRate = z.exchangeRate
		   /* getExchangeRate(VBRK_WAERK, pGlobalCurrency,VBRK_KURRF,   VBRK_FKDAT), */	
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
tmp_getExchangeRate1 z,fact_billing fb	  
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND z.pFromCurrency  = VBRK_WAERK and z.pToCurrency = dc.currency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT
and z.fact_script_name = 'bi_populate_billing_fact';



/* Update 1 column 4 - amt_ExchangeRate_GBL */

/* Update the default value first */

UPDATE fact_billing fb
SET
 amt_ExchangeRate_GBL = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
SET
 amt_ExchangeRate_GBL = z.exchangeRate   
        /*  getExchangeRate(dc.currency,pGlobalCurrency,VBRK_KURRF,VBRK_FKDAT),	*/
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
	 tmp_getExchangeRate1 z, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND z.pFromCurrency  = VBRK_WAERK and z.pToCurrency = t_exch.pGlobalCurrency AND ifnull(z.pDate,'0001-01-01') = ifnull(VBRK_FKDAT,'0001-01-01') AND pFromExchangeRate = 0
and z.fact_script_name = 'bi_populate_billing_fact';



/* Update 1 column 5 - amt_CashDiscount */

UPDATE fact_billing fb
set
   amt_CashDiscount = VBRP_SKFBP
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND amt_CashDiscount <> VBRP_SKFBP;



/* Update 1 column 6 - dd_CancelledDocumentNo */

UPDATE fact_billing fb
SET
  dd_CancelledDocumentNo = ifnull(VBRK_SFAKN,'Not Set')
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,  fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(dd_CancelledDocumentNo,'xx') <> ifnull(VBRK_SFAKN,'yy');


/* Update 1 column 7 - dd_SalesDlvrItemNo */

UPDATE fact_billing fb
SET
 dd_SalesDlvrItemNo = VBRP_VGPOS
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(dd_SalesDlvrItemNo,-1) = IFNULL(VBRP_VGPOS,-2 );

/* Update 1 column 8 - dd_SalesDlvrDocNo */

UPDATE fact_billing fb
SET
 dd_SalesDlvrDocNo = ifnull(VBRP_VGBEL, 'Not Set')
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(dd_SalesDlvrDocNo,'xx') <> ifnull(VBRP_VGBEL,'yy');

/* Update 1 column 9 - Dim_DateidBilling */

/* Update default value first - e.g for rows which don't have a match for dt.DateValue = VBRK_FKDAT AND dt.CompanyCode = VBRK_BUKRS */

UPDATE fact_billing fb
SET
Dim_DateidBilling = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS 
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS 
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR 
AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
/* Madalina BI-5480 - Billing: Creation Date not set */
/*AND dc.CompanyCode = dp.CompanyCode */
AND Dim_DateidBilling <> 1;


UPDATE fact_billing fb
SET
Dim_DateidBilling = ifnull(dt.dim_dateid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
dim_date dt,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS 
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS 
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR 
AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
/* Madalina BI-5480 - Billing: Creation Date not set */
/*AND dc.CompanyCode = dp.CompanyCode
AND dt.DateValue = VBRK_FKDAT AND dt.CompanyCode = VBRK_BUKRS AND DT.PLANTCODE_FACTORY = vkp.VBRP_WERKS */
AND dt.DateValue = VBRK_FKDAT AND dt.CompanyCode = VBRK_BUKRS 
--AND DT.PLANTCODE_FACTORY = dp.plantcode
--APP-9177fix
AND dt.plantcode_factory = 'Not Set'
AND Dim_DateidBilling <> ifnull(dt.dim_dateid,1);

/* Update 1 column 10 - Dim_DateidCreated */

/* Update the default value first - for rows which don't match for the join condition on dt */

UPDATE fact_billing fb
SET
       Dim_DateidCreated = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS 
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS 
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR 
AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
/* Madalina BI-5480 - Billing: Creation Date not set */
/* AND dc.CompanyCode = dp.CompanyCode */
AND  Dim_DateidCreated <> 1;

UPDATE fact_billing fb
SET
       Dim_DateidCreated = ifnull(dt.dim_dateid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
dim_date dt,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS 
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS 
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR  
AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
/* Madalina BI-5480 - Billing: Creation Date not set */
/* AND dc.CompanyCode = dp.CompanyCode 
AND dt.DateValue = VBRK_ERDAT AND dt.CompanyCode = VBRK_BUKRS AND DT.PLANTCODE_FACTORY = vkp.VBRP_WERKS */
AND dt.DateValue = VBRK_ERDAT AND dt.CompanyCode = VBRK_BUKRS 
--AND DT.PLANTCODE_FACTORY = dp.plantcode
--APP-9177fix
AND dt.plantcode_factory = 'Not Set'
AND Dim_DateidCreated <> ifnull(dt.dim_dateid,1);


/* Update 1 column 11 - Dim_DistributionChannelId */

UPDATE fact_billing fb
SET
	   Dim_DistributionChannelId = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND  Dim_DistributionChannelId <> 1;

UPDATE fact_billing fb
SET
	   Dim_DistributionChannelId = ifnull(ddc.Dim_DistributionChannelid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
dim_distributionchannel ddc,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND ddc.DistributionChannelCode = VBRK_VTWEG AND ddc.RowIsCurrent = 1
AND fb.Dim_DistributionChannelId <> ifnull(ddc.Dim_DistributionChannelid,1);



/* Update 1 - Part 2 column 12 onwards - Need to handle WHERE binary dc1.DocumentCategory = VBRK_VBTYP
/* Also need to handle - ORDER BY pc.ValidTo ASC LIMIT 1 */


/* 2a */	

drop table if exists dim_profitcenter_fact_billing;
create table dim_profitcenter_fact_billing as Select * from dim_profitcenter  ORDER BY ValidTo ASC;


/* Default values first - for rows not matching join with dc1. Those that match, would be updated by the next query */

/* Update 1 - column 12 */

UPDATE fact_billing fb
SET        
       Dim_DocumentCategoryid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND  Dim_DocumentCategoryid <> 1;	

UPDATE fact_billing fb
SET        
       Dim_DocumentCategoryid = ifnull(dc1.Dim_documentcategoryid,1)   
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,dim_documentcategory dc1,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND ltrim(rtrim(dc1.DocumentCategory )) = ltrim(rtrim(VBRK_VBTYP)) AND dc1.RowIsCurrent = 1
and fb.Dim_DocumentCategoryid <> ifnull(dc1.Dim_documentcategoryid,1) ;

	
/* 2b */	

/* Update 1 - column 13 */	

UPDATE fact_billing fb
SET  	
       Dim_DocumentTypeid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND fb.Dim_DocumentTypeid <> 1;

UPDATE fact_billing fb
SET  	
       Dim_DocumentTypeid = ifnull(bdt.dim_billingdocumenttypeid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
dim_billingdocumenttype bdt,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND bdt.Type = VBRK_FKART AND bdt.RowIsCurrent = 1
AND Dim_DocumentTypeid <> ifnull(bdt.dim_billingdocumenttypeid,1);

/* Update 1 - column 14 */	
UPDATE fact_billing fb
SET  	
       Dim_IncoTermid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_IncoTermid,-1) <> 1;

UPDATE fact_billing fb
SET  	
       Dim_IncoTermid = it.dim_IncoTermId
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	dim_incoterm it,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND it.IncoTermCode = VBRK_INCO1 AND it.RowIsCurrent = 1
AND IFNULL(fb.Dim_IncoTermid,-1) <> IFNULL( it.dim_IncoTermId,-2) ;

/* Update 1 - column 15 */	
UPDATE fact_billing fb
SET  	
       Dim_MaterialGroupid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_MaterialGroupid,-1) <> 1;

UPDATE fact_billing fb
SET  	
       Dim_MaterialGroupid = mg.dim_materialgroupid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	dim_materialgroup mg, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND mg.MaterialGroupCode = VBRP_MATKL AND mg.RowIsCurrent = 1
AND IFNULL(fb.Dim_MaterialGroupid,-1) <> IFNULL(mg.dim_materialgroupid,-2) ;

/* Update 1 - column 16 */	
UPDATE fact_billing fb
SET  	
       fb.Dim_Partid = 1	   
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_Partid,-1) <> 1;

UPDATE fact_billing fb
SET  	
       fb.Dim_Partid = p.dim_partid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	dim_part p,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND p.PartNumber = VBRP_MATNR AND p.Plant = VBRP_WERKS AND p.RowIsCurrent = 1
AND IFNULL(fb.Dim_Partid,-1) <> IFNULL(p.dim_partid,-2);

/* Update 1 - column 17 */	
UPDATE fact_billing fb
SET  	
 fb.Dim_Plantid = dp.dim_plantid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL( fb.Dim_Plantid,-1 ) <> IFNULL(dp.dim_plantid,-2);

/* Update 1 - column 18 */	
UPDATE fact_billing fb
SET  	
	   Dim_ProductHierarchyid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_ProductHierarchyid,-1) <> 1 ;


UPDATE fact_billing fb
SET  	
	   Dim_ProductHierarchyid = ph.dim_producthierarchyid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,
	dim_producthierarchy ph, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND ph.ProductHierarchy = VBRP_PRODH AND ph.RowIsCurrent = 1
AND IFNULL(fb.Dim_ProductHierarchyid,-1) <> IFNULL(ph.dim_producthierarchyid,-2);


	

/* 2c */	
	
/* Update 1 - column 19 */		
	
UPDATE fact_billing fb
SET  	
       fb.Dim_ProfitCenterid = ifnull(pc.dim_profitcenterid,1)

FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,fact_billing fb,VBRK_VBRP vkp
LEFT JOIN dim_profitcenter_fact_billing pc
                ON pc.ProfitCenterCode = vkp.VBRP_PRCTR
               AND pc.ControllingArea = vkp.VBRP_KOKRS
			   
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
 AND pc.ValidTo >= VBRK_FKDAT
                          AND pc.RowIsCurrent = 1
and fb.Dim_ProfitCenterid <> ifnull(pc.dim_profitcenterid,1);


/* End of Update 1 - Part 2 */

/* Update 1 - Part 3 : Tested */

/* Update 1 column 20 */

UPDATE fact_billing fb
SET fb.Dim_SalesDivisionid = 1
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_SalesDivisionid,-1) <> 1;

UPDATE fact_billing fb
SET Dim_SalesDivisionid = ifnull(sd.dim_salesdivisionid,1)
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_salesdivision sd,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND sd.DivisionCode = VBRK_SPART   
AND IFNULL(fb.Dim_SalesDivisionid,-1) <> IFNULL(sd.dim_salesdivisionid,-2);

/* Update 1 column 21 - Dim_SalesGroupid */

UPDATE fact_billing fb
SET Dim_SalesGroupid = 1
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_SalesGroupid,-1) <> 1;


UPDATE fact_billing fb
SET Dim_SalesGroupid = sg.dim_salesgroupid
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_salesgroup sg,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND sg.SalesGroupCode = VBRP_VKGRP 
AND IFNULL(fb.Dim_SalesGroupid,-1) <> IFNULL(sg.dim_salesgroupid,-2);


/* Update 1 column 22 - Dim_SalesOfficeid */

UPDATE fact_billing fb
SET Dim_SalesOfficeid = 1
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_SalesOfficeid,-1) <> 1;


UPDATE fact_billing fb
SET Dim_SalesOfficeid = so1.dim_salesofficeid
 FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_salesoffice so1,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND so1.SalesOfficeCode = VBRP_VKBUR 
AND IFNULL(fb.Dim_SalesOfficeid,-1) <> IFNULL(so1.dim_salesofficeid,-2);


/* Update 1 column 23 - Dim_SalesOrgid */
UPDATE fact_billing fb
SET fb.Dim_SalesOrgid = ifnull(so.dim_salesorgid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
 and IFNULL(fb.Dim_SalesOrgid,-1) <> ifnull(so.dim_salesorgid,1);
 
 
/* Update 1 column 24 - Dim_ControllingAreaid - NOT NULL column so IFNULL not used with the <> condition */ 
 
UPDATE fact_billing fb
SET fb.Dim_ControllingAreaid = ifnull(ca.dim_controllingareaid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND fb.Dim_ControllingAreaid <>  ifnull(ca.dim_controllingareaid,1);


/* Update 1 column 25 - Dim_StorageLocationid */ 

/* Update column Dim_StorageLocationid */

UPDATE fact_billing fb
SET Dim_StorageLocationid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_StorageLocationid,-1) <> 1;


UPDATE fact_billing fb
SET Dim_StorageLocationid = sl.dim_storagelocationid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_storagelocation sl,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND  sl.LocationCode = VBRP_LGORT
                     AND sl.plant = VBRP_WERKS
                     AND sl.RowIsCurrent = 1
AND IFNULL(fb.Dim_StorageLocationid,-1) <> IFNULL(sl.dim_storagelocationid,-2);


/* Update column Dim_AccountingTransferStatusid */

UPDATE fact_billing fb
SET Dim_AccountingTransferStatusid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_AccountingTransferStatusid,-1) <> 1;


UPDATE fact_billing fb
SET Dim_AccountingTransferStatusid = ats.dim_accountingtransferstatusid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_accountingtransferstatus ats,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND ats.AccountingTransferStatusCode = VBRK_RFBSK
                     AND ats.RowIsCurrent = 1
AND IFNULL(fb.Dim_AccountingTransferStatusid,-1) <> IFNULL(ats.dim_accountingtransferstatusid,-2);


/* Update column fb.Dim_ConditionProcedureid */

UPDATE fact_billing fb
SET fb.Dim_ConditionProcedureid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_ConditionProcedureid,-1) <> 1;

UPDATE fact_billing fb
SET fb.Dim_ConditionProcedureid = dc1.dim_conditionprocedureid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_conditionprocedure dc1,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND dc1.ProcedureCode = VBRK_KALSM
                     AND dc1.RowIsCurrent = 1
AND IFNULL(fb.Dim_ConditionProcedureid,-1) <> IFNULL(dc1.dim_conditionprocedureid,-2);


/* Update column Dim_CreditControlAreaid */

UPDATE fact_billing fb
SET Dim_CreditControlAreaid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_CreditControlAreaid,-1) <> 1;


UPDATE fact_billing fb
SET Dim_CreditControlAreaid = ifnull(cca.dim_creditcontrolareaid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_creditcontrolarea cca,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND cca.CreditControlAreaName = VBRP_KOKRS
                     AND cca.RowIsCurrent = 1
AND IFNULL(fb.Dim_CreditControlAreaid,-1) <> IFNULL(cca.dim_creditcontrolareaid,-2);


/* Update column Dim_CustomerID */

UPDATE fact_billing fb
SET Dim_CustomerID = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_CustomerID,-1) <> 1;


UPDATE fact_billing fb
SET Dim_CustomerID = ifnull(c.dim_customerid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_customer c,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND c.CustomerNumber = VBRK_KUNAG AND c.RowIsCurrent = 1
AND IFNULL(fb.Dim_CustomerID,-1) <> IFNULL(c.dim_customerid,-2);


/* Update column Dim_CustomerGroup1id */

UPDATE fact_billing fb
SET Dim_CustomerGroup1id = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_CustomerGroup1id,-1) <> 1;


UPDATE fact_billing fb
SET Dim_CustomerGroup1id = cg1.dim_customergroup1id
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_customergroup1 cg1,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND cg1.CustomerGroup = VBRK_KDGRP
AND IFNULL(fb.Dim_CustomerGroup1id,-1) <> IFNULL(cg1.dim_customergroup1id,-2);

/* Update column Dim_CustomerPaymentTermsid */

UPDATE fact_billing fb
SET Dim_CustomerPaymentTermsid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_CustomerPaymentTermsid,-1) <> 1;


UPDATE fact_billing fb
SET Dim_CustomerPaymentTermsid = cpt.dim_customerpaymenttermsid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_customerpaymentterms cpt,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND cpt.PaymentTermCode = VBRK_ZTERM
AND cpt.RowIsCurrent = 1
AND IFNULL(fb.Dim_CustomerPaymentTermsid,-1) <> IFNULL(cpt.dim_customerpaymenttermsid,-2);


/* Update column fb.Dim_ShipReceivePointid */

UPDATE fact_billing fb
SET fb.Dim_ShipReceivePointid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_ShipReceivePointid,-1) <> 1;


UPDATE fact_billing fb
SET fb.Dim_ShipReceivePointid = srp.dim_shipreceivepointid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_shipreceivepoint srp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND srp.ShipReceivePointCode = VBRP_VSTEL
AND IFNULL(fb.Dim_ShipReceivePointid,-1) <> IFNULL(srp.dim_shipreceivepointid,-2);


/* Update column fb.Dim_UnitOfMeasureId */

UPDATE fact_billing fb
SET fb.Dim_UnitOfMeasureId = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_UnitOfMeasureId,-1) <> 1;


UPDATE fact_billing fb
SET fb.Dim_UnitOfMeasureId = um.dim_unitofmeasureid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_unitofmeasure um,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND um.UOM = VBRP_MEINS
AND IFNULL(fb.Dim_UnitOfMeasureId,-1) <> IFNULL(um.dim_unitofmeasureid,-2);


/* Update 1 - Part 4 : tested */
UPDATE fact_billing fb
SET
  fb.Dim_Companyid = ifnull(dc.Dim_Companyid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND   fb.Dim_Companyid <> ifnull(dc.Dim_Companyid,1);

UPDATE fact_billing fb
SET fb.dd_fiscalyear = VBRK_GJAHR
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.dd_fiscalyear,-1) <> ifnull(VBRK_GJAHR,-2);

UPDATE fact_billing fb
SET fb.dd_postingperiod = VBRK_POPER
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.dd_postingperiod,-1) <> ifnull(VBRK_POPER,-2);

UPDATE fact_billing fb
SET fb.dd_salesitemno = VBRP_AUPOS
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.dd_salesitemno,-1) <> ifnull(VBRP_AUPOS,-2);

UPDATE fact_billing fb
SET amt_cost_InDocCurrency = VBRP_WAVWR
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.amt_cost_InDocCurrency,-1) <> ifnull(VBRP_WAVWR,-2);

UPDATE fact_billing fb
SET amt_NetValueHeader_InDocCurrency = VBRK_NETWR
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
AND ifnull(fb.amt_NetValueHeader_InDocCurrency,-1) <> ifnull(VBRK_NETWR,-2);
 
UPDATE fact_billing fb
SET fb.dd_salesdocno = ifnull(VBRP_AUBEL, 'Not Set')
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;	   
 
 
/* Update column fb.dim_salesorderitemcategoryid */

UPDATE fact_billing fb
SET fb.dim_salesorderitemcategoryid = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.dim_salesorderitemcategoryid,-1) <> 1;


UPDATE fact_billing fb
SET fb.dim_salesorderitemcategoryid = soic.dim_salesorderitemcategoryid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_salesorderitemcategory soic, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
 AND soic.SalesOrderItemCategory = vkp.VBRP_PSTYV AND soic.RowIsCurrent = 1
AND IFNULL(fb.dim_salesorderitemcategoryid,-1) <> IFNULL(soic.dim_salesorderitemcategoryid,-2);


/* Update column fb.Dim_BillingCategoryId */

UPDATE fact_billing fb
SET fb.Dim_BillingCategoryId = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_BillingCategoryId,-1) <> 1;


UPDATE fact_billing fb
SET fb.Dim_BillingCategoryId = bc.dim_billingcategoryid
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_billingcategory bc, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
 AND bc.Category = ifnull(VBRK_FKTYP,'Not Set')
                   AND bc.RowIsCurrent = 1
AND IFNULL(fb.Dim_BillingCategoryId,-1) <> IFNULL(bc.dim_billingcategoryid,-2);


/* Update column fb.Dim_RevenueRecognitionCategoryId */

UPDATE fact_billing fb
SET fb.Dim_RevenueRecognitionCategoryId = 1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(fb.Dim_RevenueRecognitionCategoryId,-1) <> 1;


UPDATE fact_billing fb
SET fb.Dim_RevenueRecognitionCategoryId = ifnull(rrc.dim_RevenueRecognitioncategoryid,1)
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp
 ,dim_RevenueRecognitioncategory rrc,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND rrc.Category = ifnull(VBRP_RRREL,'Not Set')
                   AND rrc.RowIsCurrent = 1
AND IFNULL(fb.Dim_RevenueRecognitionCategoryId,-1) <> IFNULL(rrc.dim_RevenueRecognitioncategoryid,-2);
 


/* Update 1 - Part 5 : tested*/

UPDATE fact_billing fb
SET
        amt_NetValueItem_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_NETWR
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
SET amt_CustomerConfigSubtotal1_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
SET amt_CustomerConfigSubtotal2_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,  fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
SET amt_CustomerConfigSubtotal3_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,  fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
SET amt_CustomerConfigSubtotal4_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
SET amt_CustomerConfigSubtotal5_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
SET amt_CustomerConfigSubtotal6_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode;

UPDATE fact_billing fb
SET
dd_CustomerPONumber = ifnull(VBRK_BSTNK_VF,'Not Set')
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,  fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
 AND ifnull(fb.dd_CustomerPONumber,'xx') <> ifnull(VBRK_BSTNK_VF,'Not Set');

UPDATE fact_billing fb
SET
dd_CreatedBy = ifnull(VBRK_ERNAM,'Not Set')
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp,  fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
  AND ifnull(fb.dd_CreatedBy,'xx') <> ifnull(VBRK_ERNAM,'Not Set');
 
/*Georgiana EA Tansition Addons*/ 
  UPDATE fact_billing fb
SET
fb.dd_salesunit = ifnull(vkp.VBRP_VRKME,'Not Set')
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND cur.CurrencyCode = dc.currency
 AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
 AND so.SalesOrgCode = vkp.VBRK_VKORG
 AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
 AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND dc.CompanyCode = dp.CompanyCode
 AND ifnull(fb.dd_salesunit,'Not Set') <> ifnull(vkp.VBRP_VRKME,'Not Set');
 
 UPDATE fact_billing fb
SET 
       ct_actualinvoiceqty = vkp.VBRP_FKIMG
FROM dim_company dc,dim_currency cur,dim_plant dp,dim_salesorg so,dim_part pitem,dim_controllingarea ca,VBRK_VBRP vkp, fact_billing fb
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND so.SalesOrgCode = vkp.VBRK_VKORG
AND pitem.PartNumber = vkp.VBRP_MATNR AND pitem.RowIsCurrent = 1 AND pitem.Plant = vkp.VBRP_WERKS
AND  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc.CompanyCode = dp.CompanyCode
AND IFNULL(ct_actualinvoiceqty,0) <> IFNULL(vkp.VBRP_FKIMG,0);

/*Georgiana End Of Changes*/
  

/* End of Update 1 */


/* Insert 1 */

/*End of Conversion*/
DROP TABLE IF EXISTS tmp_db_fb1 ;
CREATE TABLE tmp_db_fb1
AS
SELECT pc.ProfitCenterCode,pc.ControllingArea,VBRK_FKDAT,min(pc.ValidTo) as min_ValidTo
FROM dim_profitcenter pc,VBRK_VBRP v
WHERE   pc.ProfitCenterCode = VBRP_PRCTR
AND pc.ControllingArea = VBRP_KOKRS
AND pc.ValidTo >= VBRK_FKDAT
AND pc.RowIsCurrent = 1
GROUP BY pc.ProfitCenterCode,pc.ControllingArea,VBRK_FKDAT;


DROP TABLE IF EXISTS tmp_db_fb2;
CREATE TABLE tmp_db_fb2
AS
SELECT pc.*,t.VBRK_FKDAT,t.min_ValidTo
FROM dim_profitcenter_fact_billing pc,tmp_db_fb1 t
WHERE pc.ProfitCenterCode = t.ProfitCenterCode
AND  pc.ControllingArea = t.ControllingArea
AND pc.ValidTo = t.min_ValidTo;


delete from NUMBER_FOUNTAIN where table_name = 'fact_billing';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_billing',ifnull(max(fact_billingid),0)
FROM fact_billing;



DROP TABLE IF EXISTS tmp_ins_fact_billing;
CREATE TABLE tmp_ins_fact_billing
AS
SELECT VBRK_VBELN as dd_billing_no, VBRP_POSNR as dd_billing_item_no
     FROM    VBRK_VBRP
         INNER JOIN dim_plant dp ON dp.PlantCode = VBRP_WERKS AND dp.RowIsCurrent = 1
     INNER JOIN dim_company dc ON dc.CompanyCode = VBRK_BUKRS;

		  
/* Need exactly the same table structure for combine to work */		  
DROP TABLE IF EXISTS tmp_del_fact_billing;
CREATE TABLE tmp_del_fact_billing LIKE tmp_ins_fact_billing INCLUDING DEFAULTS INCLUDING IDENTITY;


INSERT INTO tmp_del_fact_billing
SELECT b.dd_billing_no, b.dd_billing_item_no 	
FROM fact_billing b;


rename tmp_ins_fact_billing to tmp_ins_fact_billing_1;
create table tmp_ins_fact_billing as
(select * from tmp_ins_fact_billing_1) minus (Select * from tmp_del_fact_billing);
drop table tmp_ins_fact_billing_1;
/*call vectorwise(combine 'tmp_ins_fact_billing - tmp_del_fact_billing')*/

DROP TABLE IF EXISTS tmp_fact_billing_VBRK_VBRP;
CREATE TABLE tmp_fact_billing_VBRK_VBRP
AS
SELECT VBRK_VBRP.*
FROM VBRK_VBRP,tmp_ins_fact_billing b
WHERE b.dd_billing_no = VBRK_VBELN
AND b.dd_billing_item_no = VBRP_POSNR;


INSERT INTO fact_billing(ct_BillingQtySalesUOM,
                         amt_ExchangeRate,
                         amt_ExchangeRate_GBL,
                         amt_CashDiscount,
                         ct_BillingQtyStockUOM,
                         dd_billing_no,
                         dd_billing_item_no,
                         dd_CancelledDocumentNo,
                         dd_SalesDlvrItemNo,
                         dd_SalesDlvrDocNo,
                         dd_fiscalyear,
                         dd_postingperiod,
                         Dim_AccountingTransferStatusid,
                         Dim_Companyid,
                         Dim_ConditionProcedureid,
                         Dim_ControllingAreaid,
                         Dim_CreditControlAreaid,
                         Dim_Currencyid,
                         Dim_CustomerGroup1id,
                         Dim_CustomerID,
                         Dim_CustomerPaymentTermsid,
                         Dim_DateidBilling,
                         Dim_DateidCreated,
                         Dim_DistributionChannelId,
                         Dim_DocumentCategoryid,
                         Dim_DocumentTypeid,
                         Dim_IncoTermid,
                         Dim_MaterialGroupid,
                         Dim_Partid,
                         Dim_Plantid,
                         Dim_ProductHierarchyid,
                         Dim_ProfitCenterid,
                         Dim_SalesDivisionid,
                         Dim_SalesGroupid,
                         Dim_SalesOfficeid,
                         Dim_SalesOrgid,
                         Dim_ShipReceivePointid,
                         Dim_StorageLocationid,
                         Dim_UnitOfMeasureId,
                         Dim_SalesOrderItemCategoryid,
                         dd_salesdocno,
                         dd_salesitemno,
                         Dim_BillingCategoryId,
                         Dim_RevenueRecognitionCategoryId,
                         amt_cost_InDocCurrency,
                         amt_NetValueHeader_InDocCurrency,
                         amt_NetValueItem_InDocCurrency,
                         amt_CustomerConfigSubtotal1_InDocCurrency,
                         amt_CustomerConfigSubtotal2_InDocCurrency,
                         amt_CustomerConfigSubtotal3_InDocCurrency,
                         amt_CustomerConfigSubtotal4_InDocCurrency,
                         amt_CustomerConfigSubtotal5_InDocCurrency,
                         amt_CustomerConfigSubtotal6_InDocCurrency,
                         dd_CustomerPONumber,
                         dd_CreatedBy,fact_billingid,
			 Dim_Currencyid_TRA,
			 Dim_Currencyid_GBL,
			 Dim_MaterialPricingGroupId,
			 dim_partsalesid,
			 /*Georgiana Ea Changes 21 Jan 2016*/
			 ct_actualinvoiceqty,
			 dd_salesunit)
   SELECT VBRP_FKLMG * VBRP_UMVKN / VBRP_UMVKZ,
          1 amt_ExchangeRate, /*ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = VBRK_WAERK and z.fact_script_name = 'bi_populate_billing_fact' and z.pToCurrency = dc.Currency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT ),1) amt_ExchangeRate ,*/
          1 amt_ExchangeRate_GBL, /*ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z where z.pFromCurrency  = VBRK_WAERK and z.fact_script_name = 'bi_populate_billing_fact' and z.pToCurrency = pGlobalCurrency AND z.pDate = VBRK_FKDAT AND z.pFromExchangeRate = 0 ),1) amt_ExchangeRate_GBL ,*/
          VBRP_SKFBP amt_CashDiscount,
          VBRP_FKLMG ct_BillingQtyStockUOM,
          VBRK_VBELN dd_billing_no,
          VBRP_POSNR dd_billing_item_no,
          ifnull(VBRK_SFAKN,'Not Set') dd_CancelledDocumentNo,
          VBRP_VGPOS dd_SalesDlvrItemNo,
          ifnull(VBRP_VGBEL, 'Not Set') dd_SalesDlvrDocNo,
          VBRK_GJAHR dd_fiscalyear,
          VBRK_POPER dd_postingperiod,
          1 Dim_AccountingTransferStatusid, /*ifnull((SELECT dim_accountingtransferstatusid FROM dim_accountingtransferstatus ats WHERE ats.AccountingTransferStatusCode = ifnull(VBRK_RFBSK, 'Not Set') AND ats.RowIsCurrent = 1), 1) Dim_AccountingTransferStatusid,*/
          1 Dim_Companyid, /*ifnull((SELECT dim_companyid FROM dim_company c WHERE c.companycode = VBRK_BUKRS AND c.RowIsCurrent = 1),1) Dim_Companyid,*/
          1 Dim_ConditionProcedureid, /*ifnull((SELECT Dim_ConditionProcedureid FROM dim_conditionprocedure dc WHERE dc.Dim_ConditionProcedureid = ifnull(VBRK_KALSM, 'Not Set') AND dc.RowIsCurrent = 1),1) Dim_ConditionProcedureid,*/
          1 Dim_ControllingAreaid, /*ifnull((SELECT Dim_ControllingAreaid FROM dim_controllingarea ca WHERE ca.Dim_ControllingAreaid = ifnull(VBRP_KOKRS, 'Not Set')), 1) Dim_ControllingAreaid,*/
          1 Dim_CreditControlAreaid, /*ifnull((SELECT Dim_CreditControlAreaid FROM dim_creditcontrolarea cca WHERE cca.CreditControlAreaName = VBRP_KOKRS AND cca.RowIsCurrent = 1), 1) Dim_CreditControlAreaid,*/
          1 Dim_Currencyid, /* ifnull( ( SELECT dim_currencyid FROM dim_currency c WHERE c.CurrencyCode = dc.Currency), 1) Dim_Currencyid,*/
          1 Dim_CustomerGroup1id, /*ifnull((SELECT dim_customergroup1id FROM dim_customergroup1 cg1 WHERE cg1.CustomerGroup = ifnull(VBRK_KDGRP, 'Not Set')), 1) Dim_CustomerGroup1id,*/
          1 Dim_CustomerID,  /*ifnull((SELECT dim_customerid FROM dim_customer c WHERE c.CustomerNumber = ifnull(VBRK_KUNAG, 'Not Set') AND c.RowIsCurrent = 1), 1) Dim_CustomerID,*/
          1 Dim_CustomerPaymentTermsid, /*ifnull((SELECT Dim_CustomerPaymentTermsid FROM dim_customerpaymentterms cpt WHERE cpt.PaymentTermCode = ifnull(VBRK_ZTERM, 'Not Set') AND cpt.RowIsCurrent = 1),1) Dim_CustomerPaymentTermsid,*/
          1 Dim_DateidBilling, /*ifnull((SELECT dim_dateid FROM dim_date dt WHERE dt.DateValue = ifnull(VBRK_FKDAT, '31 Dec 9999') AND dt.CompanyCode = ifnull(VBRK_BUKRS, 'Not Set')),1) Dim_DateidBilling,*/
          1 Dim_DateIdCreated, /*ifnull((SELECT dim_dateid FROM dim_date dt WHERE dt.DateValue = VBRK_ERDAT AND dt.CompanyCode = VBRK_BUKRS),1) Dim_DateIdCreated,*/
          1 Dim_DistributionChannelId, /*ifnull((SELECT Dim_DistributionChannelid FROM dim_distributionchannel dc WHERE dc.DistributionChannelCode =ifnull(VBRK_VTWEG, 'Not Set')AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,*/
          1 Dim_DocumentCategoryid, /*ifnull((SELECT Dim_documentcategoryid FROM dim_documentcategory dc1 WHERE ltrim(rtrim(dc1.DocumentCategory)) = ltrim(rtrim(ifnull(VBRK_VBTYP, 'Not Set'))) AND dc1.RowIsCurrent = 1), 1) Dim_DocumentCategoryid,*/
          1 Dim_DocumentTypeid, /*ifnull((SELECT dim_billingdocumenttypeid FROM dim_billingdocumenttype bdt  WHERE bdt.Type = VBRK_FKART AND bdt.RowIsCurrent = 1),1) Dim_DocumentTypeid,*/
          1 Dim_IncoTermid, /*ifnull((SELECT dim_IncoTermId FROM dim_incoterm it WHERE it.IncoTermCode = VBRK_INCO1 AND it.RowIsCurrent = 1),1) Dim_IncoTermid,*/
          1 Dim_MaterialGroupid, /*ifnull((SELECT dim_materialgroupid FROM dim_materialgroup mg WHERE mg.MaterialGroupCode = VBRP_MATKL AND mg.RowIsCurrent = 1),1) Dim_MaterialGroupid,*/
          1 Dim_Partid, /*ifnull( (SELECT dim_partid FROM dim_part p WHERE     p.PartNumber = VBRP_MATNR AND p.Plant = VBRP_WERKS AND p.RowIsCurrent = 1), 1) Dim_Partid,*/
          ifnull(dim_Plantid,1) dim_Plantid,
          1 dim_producthierarchyid, /*ifnull((SELECT dim_producthierarchyid FROM dim_producthierarchy ph  WHERE ph.ProductHierarchy = VBRP_PRODH  AND ph.RowIsCurrent = 1),1) dim_producthierarchyid,*/
          1 Dim_ProfitCenterid,/* ifnull((SELECT pc.dim_profitcenterid FROM tmp_db_fb2 pc WHERE   pc.ProfitCenterCode = VBRP_PRCTR  AND pc.ControllingArea = VBRP_KOKRS AND pc.VBRK_FKDAT = VBRK_FKDAT AND pc.RowIsCurrent = 1 ),1) Dim_ProfitCenterid,*/
          1 Dim_SalesDivisionid, /*ifnull((SELECT dim_salesdivisionid  FROM dim_salesdivision sd  WHERE sd.DivisionCode = VBRK_SPART),1)  Dim_SalesDivisionid,*/
          1 Dim_SalesGroupid, /*ifnull((SELECT dim_salesgroupid  FROM dim_salesgroup sg WHERE sg.SalesGroupCode = VBRP_VKGRP),1) Dim_SalesGroupid,*/
          1 Dim_SalesOfficeid, /*ifnull((SELECT dim_salesofficeid FROM dim_salesoffice so WHERE so.SalesOfficeCode = VBRP_VKBUR),1) Dim_SalesOfficeid,*/
          1 Dim_SalesOrgid, /*ifnull((SELECT dim_salesorgid FROM dim_salesorg so WHERE so.SalesOrgCode = VBRK_VKORG),1) Dim_SalesOrgid,*/
          1 Dim_ShipReceivePointid, /*ifnull((SELECT dim_shipreceivepointid FROM dim_shipreceivepoint srp WHERE srp.ShipReceivePointCode = VBRP_VSTEL),1) Dim_ShipReceivePointid,*/
          1 Dim_StorageLocationid, /*ifnull((SELECT dim_storagelocationid FROM dim_storagelocation sl WHERE     sl.LocationCode = VBRP_LGORT AND sl.plant = VBRP_WERKS AND sl.RowIsCurrent = 1),  1) Dim_StorageLocationid,*/
          1 Dim_UnitOfMeasureId, /*ifnull((SELECT dim_unitofmeasureid FROM dim_unitofmeasure um WHERE um.UOM = VBRP_MEINS  AND um.RowIsCurrent = 1),1)  Dim_UnitOfMeasureId,*/
          1 Dim_SalesOrderItemCategoryid , /*ifnull((SELECT soic.Dim_SalesOrderItemCategoryid  FROM dim_salesorderitemcategory soic  WHERE soic.SalesOrderItemCategory = VBRP_PSTYV    AND soic.RowIsCurrent = 1),1) Dim_SalesOrderItemCategoryid,*/
          ifnull(VBRP_AUBEL, 'Not Set') dd_salesdocno,
          VBRP_AUPOS dd_salesitemno,
          1 Dim_BillingCategoryId, /*ifnull((SELECT Dim_BillingCategoryId  FROM dim_billingcategory bc WHERE bc.Category = VBRK_FKTYP  AND bc.RowIsCurrent = 1 ),    1) Dim_BillingCategoryId ,*/
         1 Dim_RevenueRecognitionCategoryId, /*ifnull((SELECT Dim_RevenueRecognitionCategoryId  FROM dim_RevenueRecognitioncategory rrc   WHERE rrc.Category = VBRP_RRREL  AND rrc.RowIsCurrent = 1 ), 1) Dim_RevenueRecognitionCategoryId,*/
       VBRP_WAVWR amt_cost_InDocCurrency,
       VBRK_NETWR amt_NetValueHeader_InDocCurrency, 
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_NETWR amt_CustomerConfigSubtotal1_InDocCurrency,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1 amt_CustomerConfigSubtotal2_InDocCurrency,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2 amt_CustomerConfigSubtotal3_InDocCurrency,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3 amt_CustomerConfigSubtotal3_InDocCurrency,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4 amt_CustomerConfigSubtotal4_InDocCurrency,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5 amt_CustomerConfigSubtotal5_InDocCurrency,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6 amt_CustomerConfigSubtotal6_InDocCurrency,
       ifnull(VBRK_BSTNK_VF,'Not Set') dd_CustomerPONumber,
       ifnull(VBRK_ERNAM,'Not Set') dd_CreatedBy,
	    (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_billing') + row_number() over ( order by '') fact_billingid,
        1 dim_currencyid_TRA,  /*    ifnull( ( SELECT dim_currencyid FROM dim_currency c WHERE c.CurrencyCode = VBRK_WAERK), 1) dim_currencyid_TRA,*/
         1 dim_currencyid_GBL,/*  ifnull( ( SELECT dim_currencyid FROM dim_currency c WHERE c.CurrencyCode = pGlobalCurrency), 1) dim_currencyid_GBL,*/
	    1 Dim_MaterialPricingGroupId,
	    1 Dim_PartSalesId,
	    /*Georgiana Ea Changes 21 Jan 2016*/
		ifnull(VBRP_FKIMG,0) ct_actualinvoiceqty,
		ifnull(VBRP_VRKME, 'Not Set') dd_salesunit
     FROM    tmp_fact_billing_VBRK_VBRP
	 INNER JOIN dim_plant dp ON dp.PlantCode = VBRP_WERKS AND dp.RowIsCurrent = 1
     INNER JOIN dim_company dc ON dc.CompanyCode = VBRK_BUKRS, 
	 tmp_pGlobalCurrency_fact_billing;
	 		  
/*amt_ExchangeRate_GBL,*/
UPDATE fact_billing fb
SET fb.amt_ExchangeRate_GBL = z.exchangeRate
FROM fact_billing fb,tmp_pGlobalCurrency_fact_billing, tmp_fact_billing_VBRK_VBRP vkp, tmp_getExchangeRate1 z 
WHERE z.pFromCurrency  = VBRK_WAERK  and z.pToCurrency = pGlobalCurrency AND ifnull(z.pDate,'0001-01-01') = ifnull(VBRK_FKDAT,'0001-01-01') and
z.fact_script_name = 'bi_populate_billing_fact'
AND z.pFromExchangeRate = 0  
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.amt_ExchangeRate_GBL <> z.exchangeRate;		

/*Dim_AccountingTransferStatusid*/
UPDATE fact_billing fb
SET fb.dim_accountingtransferstatusid = ifnull(ats.dim_accountingtransferstatusid,1)
FROM dim_accountingtransferstatus ats, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE ats.AccountingTransferStatusCode = ifnull(VBRK_RFBSK, 'Not Set') 
AND ats.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_accountingtransferstatusid <> ifnull(ats.dim_accountingtransferstatusid,1);
		  
/*Dim_Companyid*/ 
/*replaced update with merge and put min(c.dim_companyid)*/
merge into fact_billing fbill
using (select distinct fact_billingid, ifnull(min(c.dim_companyid),1) as dim_companyid
FROM dim_company c, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb        
WHERE c.companycode = VBRK_BUKRS AND c.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_Companyid <> ifnull(c.Dim_Companyid,1)
group by fact_billingid
)t
on t.fact_billingid = fbill.fact_billingid
when matched then 
update set fbill.dim_companyid = t.dim_companyid;


/*amt_ExchangeRate*/
UPDATE fact_billing fb
SET fb.amt_ExchangeRate = z.exchangeRate
FROM fact_billing fb,tmp_pGlobalCurrency_fact_billing, tmp_fact_billing_VBRK_VBRP vkp,dim_company dc, tmp_getExchangeRate1 z 
WHERE z.pFromCurrency  = VBRK_WAERK 
and z.fact_script_name = 'bi_populate_billing_fact' 
and z.pToCurrency = dc.Currency 
AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT 
AND dc.CompanyCode = VBRK_BUKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.amt_ExchangeRate <> z.exchangeRate;
   
/*Dim_ConditionProcedureid*/
UPDATE fact_billing fb
SET fb.Dim_ConditionProcedureid = ifnull(dc.Dim_ConditionProcedureid,1)
FROM dim_conditionprocedure dc, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE dc.ProcedureCode = ifnull(VBRK_KALSM, 'Not Set')
AND dc.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_ConditionProcedureid <> ifnull(dc.Dim_ConditionProcedureid,1);

/* Dim_ControllingAreaid*/
UPDATE fact_billing fb
SET fb.Dim_ControllingAreaid = ifnull(ca.Dim_ControllingAreaid,1)
FROM dim_controllingarea ca, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE ca.ControllingAreaCode = ifnull(vkp.VBRP_KOKRS,'Not Set')
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_ControllingAreaid <> ifnull(ca.Dim_ControllingAreaid,1);

/*Dim_CreditControlAreaid*/		  
UPDATE fact_billing fb
SET fb.Dim_CreditControlAreaid = ifnull(cca.Dim_CreditControlAreaid,1)
FROM dim_creditcontrolarea cca, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE cca.CreditControlAreaName = VBRP_KOKRS 
AND cca.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_CreditControlAreaid <> ifnull(cca.Dim_CreditControlAreaid,1);

/*Dim_Currencyid*/
UPDATE fact_billing fb
SET fb.Dim_Currencyid = ifnull(c.Dim_Currencyid,1)
FROM dim_currency c, dim_company dc, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE c.CurrencyCode = dc.Currency
AND dc.CompanyCode = VBRK_BUKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_Currencyid <> ifnull(c.Dim_Currencyid,1);
		  
/*Dim_CustomerGroup1id*/
UPDATE fact_billing fb
SET fb.dim_customergroup1id = ifnull(cg1.dim_customergroup1id,1)
FROM dim_customergroup1 cg1, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE cg1.CustomerGroup = ifnull(VBRK_KDGRP, 'Not Set')
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_customergroup1id <> ifnull(cg1.dim_customergroup1id,1);

/*Dim_CustomerID*/
		  
UPDATE fact_billing fb
SET fb.dim_customerid = ifnull(c.dim_customerid,1)
FROM dim_customer c, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE c.CustomerNumber = ifnull(VBRK_KUNAG, 'Not Set') AND c.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_customerid <> ifnull(c.dim_customerid,1);

/*Dim_CustomerPaymentTermsid*/	  
UPDATE fact_billing fb
SET fb.Dim_CustomerPaymentTermsid = ifnull(cpt.Dim_CustomerPaymentTermsid,1)
FROM dim_customerpaymentterms cpt, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE cpt.PaymentTermCode = ifnull(VBRK_ZTERM, 'Not Set') AND cpt.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_CustomerPaymentTermsid <> ifnull(cpt.Dim_CustomerPaymentTermsid,1);

/*Dim_DateidBilling*/
UPDATE fact_billing fb
SET fb.Dim_DateidBilling = ifnull(dt.dim_dateid,1)
FROM dim_date dt, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE dt.DateValue = ifnull(VBRK_FKDAT, '0001-01-01') AND dt.CompanyCode = ifnull(VBRK_BUKRS, 'Not Set') AND DT.PLANTCODE_FACTORY = ifnull(VBRP_WERKS, 'Not Set')	
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_DateidBilling <> ifnull(dt.dim_dateid,1);		  
	  
/*Dim_DateIdCreated*/
UPDATE fact_billing fb
SET fb.Dim_DateIdCreated = ifnull(dt.dim_dateid,1)
FROM dim_date dt, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE dt.DateValue = ifnull(VBRK_ERDAT, '0001-01-01') AND dt.CompanyCode = ifnull(VBRK_BUKRS, 'Not Set') AND DT.PLANTCODE_FACTORY = ifnull(VBRP_WERKS, 'Not Set')	
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_DateIdCreated <> ifnull(dt.dim_dateid,1);
	
/*Dim_DistributionChannelId*/	  
UPDATE fact_billing fb
SET fb.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelId,1)
FROM dim_distributionchannel dc, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE dc.DistributionChannelCode =ifnull(VBRK_VTWEG, 'Not Set')AND dc.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelId,1);

/*Dim_DocumentCategoryid*/	  
UPDATE fact_billing fb
SET fb.Dim_DocumentCategoryid = ifnull(dc1.Dim_documentcategoryid,1)
FROM dim_documentcategory dc1, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE ltrim(rtrim(dc1.DocumentCategory)) = ltrim(rtrim(ifnull(VBRK_VBTYP, 'Not Set'))) AND dc1.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_DocumentCategoryid <> ifnull(dc1.Dim_documentcategoryid,1);

/*Dim_DocumentTypeid*/	
/* Madalina 05 Sept 2016 - Update Dim_DocumentTypeid field instead of Dim_DocumentCategoryid - BI-4019 */	  
/*UPDATE fact_billing fb
SET fb.Dim_DocumentCategoryid = ifnull(bdt.dim_billingdocumenttypeid,1)
FROM dim_billingdocumenttype bdt, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  bdt.Type = ifnull(VBRK_FKART, 'Not Set') AND bdt.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_DocumentCategoryid <> ifnull(bdt.dim_billingdocumenttypeid,1) */

UPDATE fact_billing fb
SET fb.Dim_DocumentTypeid = ifnull(bdt.dim_billingdocumenttypeid,1)
FROM dim_billingdocumenttype bdt, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  bdt.Type = ifnull(VBRK_FKART, 'Not Set') AND bdt.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_DocumentTypeid <> ifnull(bdt.dim_billingdocumenttypeid,1);
/* END BI-4019 */

/*Dim_IncoTermid*/
UPDATE fact_billing fb
SET fb.Dim_IncoTermid = ifnull(it.Dim_IncoTermid,1)
FROM dim_incoterm it, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE it.IncoTermCode = ifnull(VBRK_INCO1, 'Not Set') AND it.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_IncoTermid <> ifnull(it.Dim_IncoTermid,1);	  

/*Dim_MaterialGroupid*/		  
UPDATE fact_billing fb
SET fb.Dim_MaterialGroupid = ifnull(mg.dim_materialgroupid,1)
FROM dim_materialgroup mg, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  mg.MaterialGroupCode = ifnull(VBRP_MATKL, 'Not Set') AND mg.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_MaterialGroupid <> ifnull(mg.dim_materialgroupid,1);

/*Dim_Partid*/		  
UPDATE fact_billing fb
SET fb.dim_partid = ifnull(p.dim_partid,1)
FROM  dim_part p, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE   p.PartNumber = ifnull(VBRP_MATNR, 'Not Set') AND p.Plant = ifnull(VBRP_WERKS, 'Not Set') AND p.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_partid <> ifnull(p.dim_partid,1);

/*dim_producthierarchyid*/		  
UPDATE fact_billing fb
SET fb.dim_producthierarchyid = ifnull(ph.dim_producthierarchyid,1)
FROM  dim_producthierarchy ph, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE   ph.ProductHierarchy = ifnull(VBRP_PRODH,'Not Set')  AND ph.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_producthierarchyid <> ifnull(ph.dim_producthierarchyid,1);

/*Dim_ProfitCenterid*/	 
UPDATE fact_billing fb
SET fb.dim_profitcenterid = ifnull(pc.dim_profitcenterid,1)
FROM  tmp_db_fb2 pc, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE   pc.ProfitCenterCode = ifnull(VBRP_PRCTR, 'Not Set')  AND pc.ControllingArea = ifnull(VBRP_KOKRS, 'Not Set') AND pc.VBRK_FKDAT =vkp.VBRK_FKDAT AND pc.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_profitcenterid <> ifnull(pc.dim_profitcenterid,1);


/* Dim_SalesDivisionid*/
UPDATE fact_billing fb
SET fb.Dim_SalesDivisionid = ifnull(sd.dim_salesdivisionid,1)
FROM  dim_salesdivision sd, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  sd.DivisionCode = ifnull(VBRK_SPART, 'Not Set')
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_SalesDivisionid <> ifnull(sd.dim_salesdivisionid,1);

/* Dim_SalesGroupid*/	  
UPDATE fact_billing fb
SET fb.Dim_SalesGroupid = ifnull(sg.dim_salesgroupid,1)
FROM  dim_salesgroup sg, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  sg.SalesGroupCode = ifnull(VBRP_VKGRP, 'Not Set')
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_SalesGroupid <> ifnull(sg.dim_salesgroupid,1);

/*Dim_SalesOfficeid*/
UPDATE fact_billing fb
SET fb.Dim_SalesOfficeid = ifnull(so.dim_salesofficeid,1)
FROM  dim_salesoffice so, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  so.SalesOfficeCode = ifnull(VBRP_VKBUR, 'Not Set')
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_SalesOfficeid <> ifnull(so.dim_salesofficeid,1);		  
		  
/* Dim_SalesOrgid*/
UPDATE fact_billing fb
SET fb.Dim_SalesOrgid = ifnull(so.Dim_SalesOrgid,1)
FROM  dim_salesorg so, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  so.SalesOrgCode = ifnull(VBRK_VKORG, 'Not Set')
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_SalesOrgid <> ifnull(so.Dim_SalesOrgid,1);		  
		  		  		  
/*Dim_ShipReceivePointid*/
UPDATE fact_billing fb
SET fb.Dim_ShipReceivePointid = ifnull(srp.dim_shipreceivepointid,1)
FROM  dim_shipreceivepoint srp, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  srp.ShipReceivePointCode = ifnull(VBRP_VSTEL, 'Not Set')
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_ShipReceivePointid <> ifnull(srp.dim_shipreceivepointid,1);		  
		  		  		  	  
/*Dim_StorageLocationid*/
UPDATE fact_billing fb
SET fb.Dim_StorageLocationid = ifnull(sl.dim_storagelocationid,1)
FROM  dim_storagelocation sl, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE sl.LocationCode = ifnull(VBRP_LGORT,'Not Set') AND sl.plant = ifnull(VBRP_WERKS, 'Not Set') AND sl.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_StorageLocationid <> ifnull(sl.dim_storagelocationid,1);		  
		  		  		  	  
/*Dim_UnitOfMeasureId*/
UPDATE fact_billing fb
SET fb.Dim_UnitOfMeasureId = ifnull(um.dim_unitofmeasureid,1)
FROM  dim_unitofmeasure um, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE um.UOM = ifnull(VBRP_MEINS, 'Not Set')  AND um.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_UnitOfMeasureId <> ifnull(um.dim_unitofmeasureid,1);		 		  
		  
/*Dim_SalesOrderItemCategoryid*/
UPDATE fact_billing fb
SET fb.Dim_SalesOrderItemCategoryid = ifnull(soic.Dim_SalesOrderItemCategoryid,1)
FROM  dim_salesorderitemcategory soic, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  soic.SalesOrderItemCategory = ifnull(VBRP_PSTYV,'Not Set')    AND soic.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_SalesOrderItemCategoryid <> ifnull(soic.Dim_SalesOrderItemCategoryid,1);		 			  
		  
/*Dim_BillingCategoryId*/
UPDATE fact_billing fb
SET fb.Dim_BillingCategoryId = ifnull(bc.Dim_BillingCategoryId,1)
FROM  dim_billingcategory bc, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  bc.Category = ifnull(VBRK_FKTYP,'Not Set')  AND bc.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_BillingCategoryId <> ifnull(bc.Dim_BillingCategoryId,1);		 
	 
/*Dim_RevenueRecognitionCategoryId*/
UPDATE fact_billing fb
SET fb.Dim_RevenueRecognitionCategoryId = ifnull(rrc.Dim_RevenueRecognitionCategoryId,1)
FROM  dim_RevenueRecognitioncategory rrc , tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE  rrc.Category = ifnull(VBRP_RRREL,'Not Set')  AND rrc.RowIsCurrent = 1
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_RevenueRecognitionCategoryId <> ifnull(rrc.Dim_RevenueRecognitionCategoryId,1);		 
	 		 
/*dim_currencyid_TRA*/	
UPDATE fact_billing fb
SET fb.dim_currencyid_TRA = ifnull(c.Dim_Currencyid,1)
FROM dim_currency c, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb			  
WHERE c.CurrencyCode = ifnull(VBRK_WAERK, 'Not Set')
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_currencyid_TRA <> ifnull(c.Dim_Currencyid,1);
		  
/*dim_currencyid_GBL*/
UPDATE fact_billing fb
SET fb.dim_currencyid_GBL = ifnull(c.Dim_Currencyid,1)
FROM dim_currency c, tmp_fact_billing_VBRK_VBRP vkp,fact_billing fb,tmp_pGlobalCurrency_fact_billing			  
WHERE c.CurrencyCode = pGlobalCurrency
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_currencyid_GBL <> ifnull(c.Dim_Currencyid,1);

						 
update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_billingid),0) from fact_billing)
where table_name = 'fact_billing';						 

/* Update 2  - RECURSIVE not required to be removed */

/* Update 3 */ 

UPDATE    fact_billing fb
SET
 amt_cost = VBRP_WAVWR * amt_ExchangeRate
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
SET amt_NetValueHeader = VBRK_NETWR /* * amt_ExchangeRate */
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
SET amt_NetValueItem = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_NETWR /** amt_ExchangeRate*/
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
SET amt_CustomerConfigSubtotal1 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1  /** amt_ExchangeRate*/
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
SET amt_CustomerConfigSubtotal2 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2 /** amt_ExchangeRate*/
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
SET amt_CustomerConfigSubtotal3 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3 /** amt_ExchangeRate*/
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
SET amt_CustomerConfigSubtotal4 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4 /** amt_ExchangeRate*/
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
SET amt_CustomerConfigSubtotal5 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5 /** amt_ExchangeRate*/
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
SET amt_CustomerConfigSubtotal6 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6 /** amt_ExchangeRate*/
FROM vbrk_vbrp vkp,fact_billing fb
WHERE fb.dd_billing_no = vkp.VBRK_VBELN

 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
 /* Update column Dim_MaterialPricingGroupId */

UPDATE fact_billing fb
SET fb.Dim_MaterialPricingGroupId = 1
WHERE IFNULL(fb.Dim_MaterialPricingGroupId,-1) <> 1;

MERGE INTO fact_billing fb
USING (SELECT fb.fact_billingid, MAX(ifnull(mpg.dim_MaterialPricingGroupId, 1)) as dim_MaterialPricingGroupId
       FROM VBRK_VBRP vkp,dim_MaterialPricingGroup mpg,fact_billing fb
       WHERE fb.dd_billing_no = vkp.VBRK_VBELN
             AND fb.dd_billing_item_no = vkp.VBRP_POSNR
             AND mpg.materialpricinggroupcode = ifnull(VBRP_KONDM,'Not Set')
             AND mpg.RowIsCurrent = 1
             AND IFNULL(fb.Dim_MaterialPricingGroupId,-1) <> IFNULL(mpg.dim_MaterialPricingGroupId,-2)
       GROUP BY fb.fact_billingid) x
ON (fb.fact_billingid = x.fact_billingid)
WHEN MATCHED THEN UPDATE
SET fb.Dim_MaterialPricingGroupId = x.Dim_MaterialPricingGroupId;


/*UPDATE fact_Billing fb
  FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm
   SET fb.dim_Customermastersalesid = cms.dim_Customermastersalesid
 WHERE     fb.dim_salesorgid = sorg.dim_salesorgid
       AND fb.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = fb.dim_salesdivisionid
       AND cm.dim_customerid = fb.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber*/

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid;
CREATE TABLE tmp_perf_fb_dim_Customermastersalesid
AS
SELECT DISTINCT fb.dim_salesorgid,fb.dim_distributionchannelid,fb.dim_salesdivisionid,fb.dim_customerid, convert(bigint,1) as dim_Customermastersalesid
FROM fact_billing fb;

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid_2;
CREATE TABLE tmp_perf_fb_dim_Customermastersalesid_2
AS	   
SELECT  DISTINCT fb.dim_salesorgid,fb.dim_distributionchannelid,fb.dim_salesdivisionid,fb.dim_customerid,  cms.dim_Customermastersalesid dim_Customermastersalesid
FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm,
	   tmp_perf_fb_dim_Customermastersalesid fb
 WHERE     fb.dim_salesorgid = sorg.dim_salesorgid
       AND fb.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = fb.dim_salesdivisionid
       AND cm.dim_customerid = fb.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber;	   
	   
merge into fact_Billing fb
using (select distinct fb.fact_billingid,first_value(upd.dim_Customermastersalesid) over(partition by fb.fact_billingid order by fb.fact_billingid) as dim_Customermastersalesid
   FROM tmp_perf_fb_dim_Customermastersalesid_2 upd, fact_Billing fb
 WHERE     fb.dim_salesorgid =  upd.dim_salesorgid
       AND fb.dim_distributionchannelid = upd.dim_distributionchannelid
       AND fb.dim_salesdivisionid = upd.dim_salesdivisionid
       AND fb.dim_customerid = upd.dim_customerid) t
on t.fact_billingid=fb.fact_billingid
when matched then update set fb.dim_Customermastersalesid = t.dim_Customermastersalesid
where fb.dim_Customermastersalesid <> t.dim_Customermastersalesid;

UPDATE fact_Billing fb
SET fb.dim_Customermastersalesid = 1
WHERE fb.dim_Customermastersalesid IS NULL;

 
/* Update 4 */

/* Update 4 - Part 1 */   
/* Shanthi's queries from here */
/* Andrian remove join with vbrk_vbrp according BI-2300*/

merge into fact_billing fb
using (select distinct fb.fact_billingid,fso.dim_salesorderitemstatusid
  FROM fact_salesorder fso, fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
 AND fb.dd_salesitemno = fso.dd_SalesItemNo) t on t.fact_billingid=fb.fact_billingid
when matched then update set fb.Dim_SalesDocumentItemStatusId = ifnull(t.dim_salesorderitemstatusid,1) 
where fb.Dim_SalesDocumentItemStatusId <> ifnull(t.dim_salesorderitemstatusid,1); 


/*Georgiana Changes Ambiguous replace fix*/
/*merge into fact_billing fb
using (select distinct fb.fact_billingid,fso.Dim_CostCenterid
  FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo) t
on t.fact_billingid=fb.fact_billingid
when matched then update set fb.Dim_so_CostCenterid = t.Dim_CostCenterid
where IFNULL(fb.Dim_so_CostCenterid,-1) <> IFNULL(t.Dim_CostCenterid,-2)*/

merge into fact_billing fb
using (select distinct fb.fact_billingid,max(fso.Dim_CostCenterid) as Dim_CostCenterid
 FROM fact_salesorder fso, fact_billing fb
WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
AND fb.dd_salesitemno = fso.dd_SalesItemNo
group by fb.fact_billingid ) t
on t.fact_billingid=fb.fact_billingid
when matched then update set fb.Dim_so_CostCenterid = t.Dim_CostCenterid
where IFNULL(fb.Dim_so_CostCenterid,-1) <> IFNULL(t.Dim_CostCenterid,-2);

merge into fact_billing fb
using (select distinct fb.fact_billingid,fso.Dim_SalesDocumentTypeid
  FROM fact_salesorder fso, fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
 AND fb.dd_salesitemno = fso.dd_SalesItemNo
 AND IFNULL(fb.Dim_so_SalesDocumentTypeid,-1) <> ifnull(fso.Dim_SalesDocumentTypeid,-2)) t on t.fact_billingid=fb.fact_billingid
when matched then update set fb.Dim_so_SalesDocumentTypeid = ifnull(t.Dim_SalesDocumentTypeid,1) 
where IFNULL(fb.Dim_so_SalesDocumentTypeid,-1) <> ifnull(t.Dim_SalesDocumentTypeid,-2); 



MERGE INTO fact_billing fb
USING
(select distinct fso.dd_SalesDocNo,fso.dd_SalesItemNo,max(fso.Dim_DateidSchedDelivery) as Dim_DateidSchedDelivery
FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND IFNULL(fb.Dim_so_DateidSchedDelivery,-1) <> IFNULL(fso.Dim_DateidSchedDelivery,-2)
GROUP BY fso.dd_SalesDocNo,fso.dd_SalesItemNo) t on fb.dd_salesdocno = t.dd_SalesDocNo
       AND fb.dd_salesitemno = t.dd_SalesItemNo
WHEN MATCHED THEN UPDATE SET fb.Dim_so_DateidSchedDelivery = t.Dim_DateidSchedDelivery;



merge into fact_billing fb
using (select distinct fb.fact_billingid,fso.Dim_DateidSalesOrderCreated
  FROM fact_salesorder fso, fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
 AND fb.dd_salesitemno = fso.dd_SalesItemNo) t on t.fact_billingid=fb.fact_billingid
when matched then update set fb.Dim_so_SalesOrderCreatedId = ifnull(t.Dim_DateidSalesOrderCreated,1)
where fb.Dim_so_SalesOrderCreatedId <> ifnull(t.Dim_DateidSalesOrderCreated,1);


	   
UPDATE fact_billing fb
SET    fb.Dim_CustomPartnerFunctionId = ifnull(fso.Dim_CustomPartnerFunctionId,1)
FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND fb.Dim_CustomPartnerFunctionId <> ifnull(fso.Dim_CustomPartnerFunctionId,1);


MERGE INTO fact_billing fb
USING
(select distinct fso.dd_SalesDocNo,fso.dd_SalesItemNo,max(fso.Dim_PayerPartnerFunctionId) as Dim_PayerPartnerFunctionId
FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND fb.Dim_PayerPartnerFunctionId <> fso.Dim_PayerPartnerFunctionId
GROUP BY fso.dd_SalesDocNo,fso.dd_SalesItemNo) t on fb.dd_salesdocno = t.dd_SalesDocNo
       AND fb.dd_salesitemno = t.dd_SalesItemNo
WHEN MATCHED THEN UPDATE SET  fb.Dim_PayerPartnerFunctionId = ifnull(t.Dim_PayerPartnerFunctionId,1);


merge into fact_billing fb
using (select distinct fact_billingid,first_value(fso.Dim_BillToPartyPartnerFunctionId) over (partition by fb.fact_billingid order by '') as Dim_BillToPartyPartnerFunctionId
FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo) t on t.fact_billingid=fb.fact_billingid
when matched then update set fb.Dim_BillToPartyPartnerFunctionId = ifnull(t.Dim_BillToPartyPartnerFunctionId,1)
where fb.Dim_BillToPartyPartnerFunctionId <> ifnull(t.Dim_BillToPartyPartnerFunctionId,1);



merge into fact_billing fb
using (select distinct fb.fact_billingid,
first_value(fso.Dim_CustomerGroupId) over (partition by fb.fact_billingid order by '') as Dim_CustomerGroupId
--fso.Dim_CustomerGroupId
FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo) t on t.fact_billingid=fb.fact_billingid
when matched then update set  fb.Dim_CustomerGroupId = t.Dim_CustomerGroupId
where  fb.Dim_CustomerGroupId <> t.Dim_CustomerGroupId;



UPDATE fact_billing fb
SET    fb.Dim_CustomPartnerFunctionId1 = fso.Dim_CustomPartnerFunctionId1
FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       and fb.Dim_CustomPartnerFunctionId1 <> fso.Dim_CustomPartnerFunctionId1;

UPDATE fact_billing fb
SET    fb.Dim_CustomPartnerFunctionId2 = fso.Dim_CustomPartnerFunctionId2
FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       and fb.Dim_CustomPartnerFunctionId2 <> fso.Dim_CustomPartnerFunctionId2;


merge into fact_billing fb
using (select distinct fb.fact_billingid,
	first_value(fso.Dim_CustomeridShipTo) over (partition by fb.fact_billingid order by '') as Dim_CustomeridShipTo  /* Madalina 17 Oct 2016- Unstable set */
FROM  fact_salesorder fso,  fact_billing fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo) t on t.fact_billingid=fb.fact_billingid
when matched then update set  fb.Dim_CustomeridShipTo = ifnull(t.Dim_CustomeridShipTo,1)
where  fb.Dim_CustomeridShipTo <> ifnull(t.Dim_CustomeridShipTo,1);

       
/*Georgiana 18 Mar 2016 adding dd_DeliveryNumber according to BI-2348*/
/* Madalina 20 Sept 2016 - Unstable set of rows */
MERGE INTO fact_billing fb
USING
(select distinct fso.dd_SalesDocNo,fso.dd_SalesItemNo,  /*  fso.dd_DeliveryNumber */
	first_value (fso.dd_DeliveryNumber) over ( partition by fso.dd_SalesDocNo,fso.dd_SalesItemNo order by '') as dd_DeliveryNumber
 FROM fact_salesorder fso, fact_billing fb
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND fb.dd_DeliveryNumber <> ifnull(fso.dd_DeliveryNumber,'Not Set')) t
on fb.dd_salesdocno = t.dd_SalesDocNo
       AND fb.dd_salesitemno = t.dd_SalesItemNo
WHEN MATCHED THEN UPDATE SET fb.dd_DeliveryNumber = ifnull(t.dd_DeliveryNumber,'Not Set');

/*18 Mar 2016 End of Changes*/

DROP TABLE IF EXISTS tmp_fb_fact_salesorder;
create table tmp_fb_fact_salesorder
as
select f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,SUM(f_so.ct_ConfirmedQty) sum_ct_ConfirmedQty,SUM(f_so.ct_DeliveredQty) sum_ct_DeliveredQty,
SUM(f_so.ct_ScheduleQtySalesUnit) - SUM(f_so.ct_DeliveredQty ) as diff_sched_dlvrqty,
SUM( f_so.ct_ConfirmedQty * (f_so.amt_UnitPrice / (CASE WHEN f_so.ct_PriceUnit = 0 THEN NULL ELSE f_so.ct_PriceUnit END))) upd_amt_so_confirmed
FROM fact_salesorder f_so
group by f_so.dd_SalesDocNo,f_so.dd_SalesItemNo;

/* tmp_fb_fact_salesorder_a stores aggregate value diff_sched_dlvrqty grouped by doc,item. But it also has an additinoal column Dim_DocumentCategoryid
If there are no doc,item combinations in fso having different documentcategoryid, then rowcount in tmp_fb_fact_salesorder_a will be same as tmp_fb_fact_salesorder */
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_a;
create table tmp_fb_fact_salesorder_a
as
select DISTINCT fso.dd_SalesDocNo,fso.dd_SalesItemNo,fso.Dim_DocumentCategoryid,f_so.diff_sched_dlvrqty,f_so.upd_amt_so_confirmed
FROM tmp_fb_fact_salesorder f_so, fact_salesorder fso
WHERE fso.dd_SalesDocNo = f_so.dd_SalesDocNo
AND fso.dd_SalesItemNo = f_so.dd_SalesItemNo;


DROP TABLE IF EXISTS tmp_fb_amt_updates;
CREATE TABLE tmp_fb_amt_updates
AS
SELECT distinct fb.*
FROM fact_billing fb,dim_billingmisc bm,dim_billingdocumenttype bdt,vbrk_vbrp vkp
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_billingmiscid = bm.dim_billingmiscid
AND bm.CancelFlag = 'Not Set'
AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
AND bdt.type IN ('F1','F2','ZF2','ZF2C');

/* This is required for using combine*/
ALTER TABLE tmp_fb_amt_updates
ADD PRIMARY KEY (fact_billingid);


UPDATE tmp_fb_amt_updates fb
SET fb.ct_so_ConfirmedQty = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_so_UnitPrice = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.ct_so_DeliveredQty = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_so_Returned = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.ct_so_OpenOrderQty = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.ct_so_ReturnedQty = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_so_confirmed = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_sd_Cost = 0;

UPDATE tmp_fb_amt_updates fb 
SET    fb.ct_so_ConfirmedQty = fso.sum_ct_ConfirmedQty 
FROM tmp_fb_fact_salesorder fso, tmp_fb_amt_updates fb 
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo;


MERGE into tmp_fb_amt_updates fb
USING (select distinct fso.amt_UnitPrice, fso.dd_SalesDocNo, fso.dd_SalesItemNo
  FROM fact_salesorder fso,tmp_fb_amt_updates fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo) t
on fb.dd_salesdocno = t.dd_SalesDocNo
       AND fb.dd_salesitemno = t.dd_SalesItemNo
WHEN MATCHED THEN UPDATE SET fb.amt_so_UnitPrice = t.amt_UnitPrice;

UPDATE tmp_fb_amt_updates fb
SET       fb.ct_so_DeliveredQty = fso.sum_ct_DeliveredQty
FROM tmp_fb_fact_salesorder fso, tmp_fb_amt_updates fb 
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo;



DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2;
create table tmp_fb_fact_salesorder_2
as
/* select f_so.dd_SalesDocNo,f_so.dd_SalesItemNo, SUM(f_so.amt_ScheduleTotal) sum_amt_ScheduleTotal, SUM(f_so.ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit, SUM(f_so.ct_ScheduleQtySalesUnit) - SUM(f_so.ct_DeliveredQty ) as diff_sched_dlvrqt */
select f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,SUM(f_so.ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit, SUM(f_so.amt_ScheduleTotal) sum_amt_ScheduleTotal
FROM fact_salesorder f_so, Dim_documentcategory dc 
WHERE dc.Dim_DocumentCategoryid = f_so.Dim_DocumentCategoryid
AND dc.RowIsCurrent = 1
AND dc.DocumentCategory IN ('H', 'K')
AND f_so.Dim_SalesOrderRejectReasonid <> 1
group by f_so.dd_SalesDocNo,f_so.dd_SalesItemNo;


DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2a;
create table tmp_fb_fact_salesorder_2a
as
SELECT DISTINCT fso.dd_SalesDocNo,fso.dd_SalesItemNo,fso.Dim_DocumentCategoryid,f_so.sum_ct_ScheduleQtySalesUnit
FROM tmp_fb_fact_salesorder_2 f_so, fact_salesorder fso
WHERE fso.dd_SalesDocNo = f_so.dd_SalesDocNo
AND fso.dd_SalesItemNo = f_so.dd_SalesItemNo;


UPDATE tmp_fb_amt_updates fb
SET       fb.amt_so_Returned = fso.sum_amt_ScheduleTotal
FROM  tmp_fb_fact_salesorder_2 fso,  tmp_fb_amt_updates fb
WHERE fso.dd_SalesDocNo = fb.dd_salesdocno
AND fso.dd_SalesItemNo = fb.dd_salesitemno;     


   
UPDATE tmp_fb_amt_updates fb 
 SET      fb.ct_so_OpenOrderQty = fso.diff_sched_dlvrqty
 FROM
       tmp_fb_fact_salesorder_a fso,Dim_documentcategory dc, tmp_fb_amt_updates fb 
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
AND dc.RowIsCurrent = 1;


UPDATE tmp_fb_amt_updates fb 
SET    fb.ct_so_ReturnedQty = fso.sum_ct_ScheduleQtySalesUnit
FROM
	tmp_fb_fact_salesorder_2 fso,tmp_fb_amt_updates fb 
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
AND fb.dd_salesitemno = fso.dd_SalesItemNo;


DROP TABLE IF EXISTS TMP_UPDT_SLS;
CREATE TABLE TMP_UPDT_SLS
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,MAX(Dim_DocumentCategoryid) Dim_DocumentCategoryid FROM fact_salesorder GROUP BY dd_SalesDocNo,dd_SalesItemNo;

UPDATE tmp_fb_amt_updates fb
   SET fb.amt_revenue =
          (CASE dc.DocumentCategory
           WHEN 'U' THEN 0
           WHEN '5' THEN 0
           WHEN '6' THEN 0
           WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
           WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
           ELSE fb.amt_NetValueItem
           END) 
  FROM TMP_UPDT_SLS fso,Dim_documentcategory dc,tmp_fb_amt_updates fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
   AND fb.dd_salesitemno = fso.dd_SalesItemNo
   AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
   AND dc.RowIsCurrent = 1;

UPDATE tmp_fb_amt_updates fb 
SET    fb.amt_Revenue_InDocCurrency = (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
      WHEN '5' THEN 0
      WHEN '6' THEN 0
      WHEN 'N' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
      WHEN 'O' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
              ELSE fb.amt_NetValueItem_InDocCurrency
           END)
FROM
        TMP_UPDT_SLS fso,Dim_documentcategory dc,tmp_fb_amt_updates fb 

 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1;

UPDATE tmp_fb_amt_updates fb
 SET   fb.amt_so_confirmed = fso.upd_amt_so_confirmed
  FROM
       tmp_fb_fact_salesorder_a fso,
       Dim_documentcategory dc, tmp_fb_amt_updates fb
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1;


DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;

CREATE TABLE tmp_openorder_from_Sales_to_billing AS
SELECT f_so.dd_SalesDocNo, f_so.dd_salesitemno,SUM(f_so.ct_ScheduleQtySalesUnit) as orderqty,SUM(f_so.ct_DeliveredQty) as DlvrQty, AVG(f_so.amt_UnitPrice) as TotalUnitPrice, ifnull(avg(f_so.ct_PriceUnit),0) as PriceUnit from fact_salesorder f_so
group by f_so.dd_SalesDocNo,f_so.dd_salesitemno;

UPDATE tmp_fb_amt_updates fb 
SET       fb.amt_so_openorder =
          ((t.orderqty - t.DlvrQty)* t.TotalUnitPrice /(CASE WHEN t.PriceUnit = 0 THEN 1 ELSE t.PriceUnit END))
FROM 
           tmp_openorder_from_Sales_to_billing t, tmp_fb_amt_updates fb 
 WHERE t.dd_salesDocNo = fb.dd_Salesdocno
       AND t.dd_SalesItemNo = fb.dd_SalesItemNo;

DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;


DROP TABLE IF EXISTS tmp_fb_fact_salesorderdelivery;
CREATE TABLE tmp_fb_fact_salesorderdelivery
AS
SELECT f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo,sum(f_sod.amt_Cost) as sum_amt_Cost
FROM fact_salesorderdelivery f_sod
GROUP BY f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_sd_Cost = f_sod.sum_amt_Cost
 from tmp_fb_fact_salesorderdelivery f_sod, tmp_fb_amt_updates fb
WHERE f_sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
  AND f_sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo;

DROP TABLE IF EXISTS TMP_UPDT_BILLING_SOD;
CREATE TABLE TMP_UPDT_BILLING_SOD
AS
SELECT sod.dd_SalesDlvrDocNo,sod.dd_SalesDlvrItemNo,max(sod.Dim_DateidActualGoodsIssue) Dim_DateidActualGoodsIssue FROM fact_salesorderdelivery sod GROUP BY sod.dd_SalesDlvrDocNo,sod.dd_SalesDlvrItemNo;
   
UPDATE tmp_fb_amt_updates fb
   SET fb.Dim_sd_DateidActualGoodsIssue = sod.Dim_DateidActualGoodsIssue
  FROM TMP_UPDT_BILLING_SOD sod,tmp_fb_amt_updates fb
 WHERE sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
   AND sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo
   AND ifnull(fb.Dim_sd_DateidActualGoodsIssue,-1) <> sod.Dim_DateidActualGoodsIssue;
   
DROP TABLE IF EXISTS TMP_UPDT_BILLING_SOD;

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;

CREATE TABLE tmp_openorder_from_Shipment_to_billing AS
SELECT f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo,SUM(f_sod.ct_QtyDelivered) as dlvrqty,AVG(f_sod.amt_UnitPrice) as AmtUnitPrice,ifnull(avg(f_sod.ct_PriceUnit),0) as PriceUnit from fact_salesorderdelivery f_sod
group by f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo;



  UPDATE tmp_fb_amt_updates fb
    SET fb.amt_sd_Shipped = (t.dlvrqty * t.AmtUnitPrice/(case when t.PriceUnit = 0 then null else t.PriceUnit end ))
  from tmp_openorder_from_Shipment_to_billing t, tmp_fb_amt_updates fb
  WHERE     fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo;
   
   
  UPDATE tmp_fb_amt_updates fb
    SET fb.ct_sd_QtyDelivered = t.dlvrqty
  from tmp_openorder_from_Shipment_to_billing t, tmp_fb_amt_updates fb
  WHERE     fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo;

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;

DROP table if exists tmp_SalesOrderAmtForBilling;

/*PK on any 1 column is mandatory*/
/*call vectorwise(combine 'fact_billing-tmp_fb_amt_updates+tmp_fb_amt_updates')*/

MERGE INTO fact_billing i 
USING tmp_fb_amt_updates d
   ON i.dd_billing_no = d.dd_billing_no
  AND i.dd_billing_item_no = d.dd_billing_item_no
WHEN MATCHED THEN DELETE;

INSERT INTO fact_billing i
SELECT d.* 
  FROM tmp_fb_amt_updates d;


  CREATE TABLE tmp_SalesOrderAmtForBilling
  AS
  SELECT fb.dd_billing_no,dd_billing_item_no,dt.datevalue as billingdate,so.dd_SalesDocNo,so.dd_salesItemno
  ,AVG(fb.ct_sd_QtyDelivered) as sd_QtyDelivered,AVG(fb.amt_sd_Shipped) as sd_shipped,
  sum(so.ct_ScheduleQtySalesUnit) as totalorderqty,sum(so.amt_ScheduleTotal) as totalorderamt,
  /*000000000000000000.0000 as acttotalqty, 000000000000000000.0000 as acttotalamt*/
  convert(decimal (18,4), 0) as acttotalqty, convert(decimal (18,4), 0) as acttotalamt
  FROM fact_billing fb, fact_salesorder so,dim_date dt, dim_billingdocumenttype bdt,
         dim_billingmisc bm
   WHERE fb.dd_SalesDocNo = so.dd_salesdocno
  and fb.dd_salesitemno = so.dd_SalesItemNo
  and fb.Dim_DateidBilling = dt.dim_dateid
  and fb.dd_Salesdocno <> 'Not Set'
  AND fb.dim_billingmiscid = bm.dim_billingmiscid
  AND bm.CancelFlag = 'Not Set'
  AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
  AND bdt.type IN ('F1','F2','ZF2','ZF2C')
  group by fb.dd_billing_no,dd_billing_item_no,dt.datevalue,so.dd_SalesDocNo,so.dd_salesItemno;

  UPDATE tmp_SalesOrderAmtForBilling
  set acttotalamt =  sd_Shipped,
    acttotalqty =  sd_QtyDelivered
  where totalorderamt > sd_Shipped;

  DROP table if exists tmp_SalesOrderAmtForBilling_100;

  CREATE TABLE tmp_SalesOrderAmtForBilling_100
  AS SELECT dd_salesDocNo,dd_salesitemno, max(billingdate) as billdate,sum(acttotalamt) as ActualAmt,sum(acttotalqty) as ActualQty from
  tmp_SalesOrderAmtForBilling group by dd_salesDocNo,dd_salesitemno;

  UPDATE tmp_SalesOrderAmtForBilling t1 
  set t1.acttotalamt = t1.acttotalamt + (t1.TotalOrderamt - t2.ActualAmt),
      t1.acttotalqty = t1.acttotalqty + (t1.Totalorderqty - t2.actualqty)
FROM
    tmp_SalesOrderAmtForBilling_100 t2,  tmp_SalesOrderAmtForBilling t1
  where t1.dd_Salesdocno = t2.dd_salesdocno
    AND t1.dd_salesitemno = t2.dd_Salesitemno
    and t1.billingdate = t2.billdate
    and t1.totalorderamt > t2.ActualAmt;

  DROP table if exists tmp_SalesOrderAmtForBilling_100;

  UPDATE fact_billing fb
  set fb.amt_so_ScheduleTotal = tmp.acttotalamt
  from tmp_SalesOrderAmtForBilling tmp, fact_billing fb
  WHERE fb.dd_billing_no = tmp.dd_billing_no
  and fb.dd_billing_item_no = tmp.dd_billing_item_no
  AND fb.amt_so_ScheduleTotal <> tmp.acttotalamt
  AND IFNULL(fb.amt_so_ScheduleTotal,-2) = IFNULL(tmp.acttotalamt,-2);
  
  UPDATE fact_billing fb
  set 
      fb.ct_so_ScheduleQtySalesUnit = tmp.acttotalqty
  from tmp_SalesOrderAmtForBilling tmp, fact_billing fb
  WHERE fb.dd_billing_no = tmp.dd_billing_no
  and fb.dd_billing_item_no = tmp.dd_billing_item_no
  AND IFNULL(fb.ct_so_ScheduleQtySalesUnit,-1) <> IFNULL(tmp.acttotalqty,-2);  

  DROP table if exists tmp_SalesOrderAmtForBilling;
  
DROP TABLE IF EXISTS tmp_sod_for_billing_no;
CREATE TABLE tmp_sod_for_billing_no
AS
SELECT DISTINCT  sod.fact_salesorderdeliveryid, fb.dd_billing_no, row_number() over (partition by fb.dd_SalesDlvrDocNo,fb.dd_SalesDlvrItemNo order by fb.dd_SalesDlvrDocNo,fb.dd_SalesDlvrItemNo) as rowseqno
from fact_salesorderdelivery sod
, fact_billing fb,
 dim_billingdocumenttype bdt,
 dim_billingmisc bm
 WHERE  
 fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
 AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
 AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
 AND bdt.type IN ('F1','F2','ZF2','ZF2C')
 AND bm.dim_billingmiscid = fb.dim_billingmiscid
 AND bm.CancelFlag = 'Not Set'
 AND sod.dd_billing_no <> fb.dd_billing_no;
 
 
  UPDATE fact_salesorderdelivery sod
  SET sod.dd_billing_no = fb.dd_billing_no
    	,sod.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM tmp_sod_for_billing_no fb, fact_salesorderdelivery sod
  WHERE  fb.fact_salesorderdeliveryid = sod.fact_salesorderdeliveryid
  AND rowseqno=1
   AND sod.dd_billing_no <> fb.dd_billing_no;
  DROP TABLE IF EXISTS tmp_sod_for_billing_no; 
  
DROP TABLE IF EXISTS tmp_sod_for_billing_item_no;
CREATE TABLE tmp_sod_for_billing_item_no
AS
SELECT DISTINCT  sod.fact_salesorderdeliveryid, ifnull(fb.dd_billing_item_no,0) as dd_billing_item_no, row_number() over (partition by fb.dd_SalesDlvrDocNo,fb.dd_SalesDlvrItemNo order by fb.dd_SalesDlvrDocNo,fb.dd_SalesDlvrItemNo) as rowseqno
from fact_salesorderdelivery sod
, fact_billing fb,
 dim_billingdocumenttype bdt,
 dim_billingmisc bm 
WHERE     
fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
AND bdt.type IN ('F1','F2','ZF2','ZF2C')
AND bm.dim_billingmiscid = fb.dim_billingmiscid
AND bm.CancelFlag = 'Not Set'
AND ifnull(sod.dd_billingitem_no,0) <> ifnull(fb.dd_billing_item_no,0);
 
  UPDATE fact_salesorderdelivery sod
 SET sod.dd_billingitem_no = ifnull(fb.dd_billing_item_no,0)
    	,sod.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM tmp_sod_for_billing_item_no fb, fact_salesorderdelivery sod
  WHERE  fb.fact_salesorderdeliveryid = sod.fact_salesorderdeliveryid
  AND rowseqno=1
  AND ifnull(sod.dd_billingitem_no,0) <> ifnull(fb.dd_billing_item_no,0);
  DROP TABLE IF EXISTS tmp_sod_for_billing_item_no;

 

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid;
DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid_2;
DROP TABLE IF EXISTS tmp_pGlobalCurrency_fact_billing;
drop table if exists dim_profitcenter_fact_billing;
DROP TABLE IF EXISTS tmp_db_fb1 ;
DROP TABLE IF EXISTS tmp_db_fb2;
DROP TABLE IF EXISTS tmp_ins_fact_billing;
DROP TABLE IF EXISTS tmp_del_fact_billing;
DROP TABLE IF EXISTS tmp_fact_billing_VBRK_VBRP;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_a;
DROP TABLE IF EXISTS tmp_fb_amt_updates;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2a;
DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;
DROP TABLE IF EXISTS tmp_fb_fact_salesorderdelivery;
DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;
DROP table if exists tmp_SalesOrderAmtForBilling;
  DROP table if exists tmp_SalesOrderAmtForBilling_100;

/* Start Changes 03 Feb 2014 */

UPDATE fact_billing fb
SET fb.dim_materialpricegroup4id= ifnull(fso.dim_materialpricegroup4id,1)
 FROM
       fact_salesorder fso, fact_billing fb
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_materialpricegroup4id <> ifnull(fso.dim_materialpricegroup4id,1);
update fact_billing set dim_materialpricegroup4id = 1 where dim_materialpricegroup4id is NULL;

/*06 Aug 2018 Georgiana Unable to get a stabe source of rows fix*/
/*UPDATE fact_billing fb
SET fb.dim_materialpricegroup5id= ifnull(fso.dim_materialpricegroup5id,1) 
FROM
       fact_salesorder fso, fact_billing fb
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_materialpricegroup5id <> ifnull(fso.dim_materialpricegroup5id,1)*/
	   
merge into fact_billing  fb
using ( select distinct fb.fact_billingid,ifnull(fso.dim_materialpricegroup5id,1) as dim_materialpricegroup5id
from  fact_salesorder fso, fact_billing fb
 WHERE fb.dd_salesdocno = fso.dd_SalesDocNo
 AND fb.dd_salesitemno = fso.dd_SalesItemNo
 AND fb.dim_materialpricegroup5id <> ifnull(fso.dim_materialpricegroup5id,1)) t
on t.fact_billingid=fb.fact_billingid
when matched then update set fb.dim_materialpricegroup5id= t.dim_materialpricegroup5id;
	   
update fact_billing set dim_materialpricegroup5id = 1 where dim_materialpricegroup5id is NULL;


UPDATE fact_billing fb
SET fb.dim_CustomerConditionGroups1id = ifnull(fso.dim_CustomerConditionGroups1id,1)
 FROM
       fact_salesorder fso, fact_billing fb
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_CustomerConditionGroups1id <> ifnull(fso.dim_CustomerConditionGroups1id,1);
update fact_billing set dim_CustomerConditionGroups1id = 1 where dim_CustomerConditionGroups1id is NULL;

UPDATE fact_billing fb
SET  fb.dim_CustomerConditionGroups2id= ifnull(fso.dim_CustomerConditionGroups2id,1)
 FROM
       fact_salesorder fso, fact_billing fb
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_CustomerConditionGroups2id <> ifnull(fso.dim_CustomerConditionGroups2id,1);
update fact_billing set dim_CustomerConditionGroups2id = 1 where dim_CustomerConditionGroups2id is NULL;

UPDATE fact_billing fb
SET  fb.dim_CustomerConditionGroups3id= ifnull(fso.dim_CustomerConditionGroups3id,1)
 FROM
       fact_salesorder fso, fact_billing fb
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_CustomerConditionGroups3id <> ifnull(fso.dim_CustomerConditionGroups3id,1);
update fact_billing set dim_CustomerConditionGroups3id = 1 where dim_CustomerConditionGroups3id is NULL;

/* End Changes 03 Feb 2014 */

/* Start Changes 14 Feb 2014 */

UPDATE fact_billing fb
SET fb.Dim_CustomerGroup4id= ifnull(fso.Dim_CustomerGroup4id,1)
 FROM
       (select distinct dd_SalesDocNo,dd_SalesItemNo,Dim_CustomerGroup4id from fact_salesorder) fso, fact_billing fb
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND ifnull(fb.Dim_CustomerGroup4id,-1) <> ifnull(fso.Dim_CustomerGroup4id,-2);
update fact_billing set Dim_CustomerGroup4id = 1 where Dim_CustomerGroup4id is NULL;

/* END Changes 14 Feb 2014 */

/* Update dim_partsalesid */

UPDATE fact_billing fb
SET fb.dim_partsalesid = ps.dim_partsalesid
FROM dim_partsales ps, dim_part dp, dim_salesorg so, dim_distributionchannel dc, fact_billing fb
WHERE fb.dim_partid = dp.dim_partid and dp.partnumber = ps.partnumber
	AND fb.dim_salesorgid = so.dim_salesorgid and so.salesorgcode = ps.salesorgcode
	AND fb.dim_distributionchannelid = dc.dim_distributionchannelid and dc.distributionchannelcode = ps.distributionchannelcode
AND IFNULL(fb.dim_partsalesid,1) <> ps.dim_partsalesid;

update fact_billing set dim_partsalesid = 1 where dim_partsalesid is NULL;
/* End Update dim_partsalesid */

/* 13 Aug 2015 Update Dim_BillingMiscId */
 UPDATE fact_billing fb
    SET  fb.Dim_BillingMiscId = ifnull(bmisc.Dim_BillingMiscId,1)
  FROM  Dim_BillingMisc bmisc,
        VBRK_VBRP vrp, fact_billing fb
  WHERE     fb.dd_Billing_no = vrp.VBRK_VBELN
        AND fb.dd_billing_item_no = vrp.VBRP_POSNR
        AND bmisc.CancelFlag = ifnull(vrp.VBRK_FKSTO,'Not Set')
        AND bmisc.CashDiscountIndicator = ifnull( VBRP_SKTOF,'Not Set')
		AND fb.Dim_BillingMiscId <> ifnull(bmisc.Dim_BillingMiscId,1);
/* End Update Dim_BillingMiscId */


/* Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */

UPDATE fact_billing fap
SET fap.std_exchangerate_dateid = fap.Dim_DateidBilling
WHERE  fap.std_exchangerate_dateid <> fap.Dim_DateidBilling;

/* END Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */

/*Georgiana Changes 10 May 2016 Add dd_referencedocnumber according to BI-2790*/
UPDATE fact_billing fb
SET  fb.dd_referencedocnumber = ifnull(vkp.VBRK_XBLNR,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM VBRK_VBRP vkp, fact_billing fb
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND IFNULL(dd_referencedocnumber,'xx') <> ifnull(vkp.VBRK_XBLNR,'yy');

/* Madalina 12 Aug 2016 - dd used for replace subselect in measures formula */
update fact_billing
set dd_BusinessDaysSeqNo_cd = ifnull(d.BusinessDaysSeqNo,0)
from fact_billing AS f_bill INNER JOIN Dim_Date AS cd ON f_bill.Dim_DateidCreated = cd.Dim_Dateid 
INNER JOIN Dim_Date AS sdagi ON f_bill.Dim_sd_DateidActualGoodsIssue = sdagi.Dim_Dateid
inner join  dim_date d  on d.datevalue =   (sdagi.DateValue) and d.companycode = cd.companycode and d.PLANTCODE_FACTORY = cd.PLANTCODE_FACTORY
where dd_BusinessDaysSeqNo_cd <> ifnull(d.BusinessDaysSeqNo,0);

update fact_billing
set dd_BusinessDaysSeqNo_bd = ifnull(d.BusinessDaysSeqNo,0)
FROM fact_billing AS f_bill INNER JOIN Dim_Date AS bd ON f_bill.Dim_DateidBilling = bd.Dim_Dateid  
INNER JOIN Dim_Date AS sdagi ON f_bill.Dim_sd_DateidActualGoodsIssue = sdagi.Dim_Dateid
inner join dim_date d on d.datevalue =   (sdagi.DateValue) and d.companycode = bd.companycode and d.PLANTCODE_FACTORY = bd.PLANTCODE_FACTORY
where dd_BusinessDaysSeqNo_bd <> ifnull(d.BusinessDaysSeqNo,0);

	   
/*Alin Changes according to BI-5061*/
UPDATE fact_billing fb
SET  fb.dd_AccountAssignmentGroupCode = ifnull(vkp.VBRK_KTGRD,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM VBRK_VBRP vkp, fact_billing fb
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND IFNULL(dd_AccountAssignmentGroupCode,'xx') <> ifnull(vkp.VBRK_KTGRD,'yy');

UPDATE fact_billing fb
SET  fb.dd_AccountAssignmentGroupDescription = ifnull(TV.TVKTT_VTEXT,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM VBRK_VBRP vkp, fact_billing fb, TVKTT tv
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN 
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND tv.TVKTT_KTGRD = vkp.VBRK_KTGRD
 AND IFNULL(dd_AccountAssignmentGroupDescription,'xx') <> ifnull(tv.TVKTT_VTEXT,'yy');