/* ################################################################################################################## */
/* */
/*   Author         : Octavian */
/*   Created On     : 16 Dec 2015 */
/*  */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   16 Dec 2015      Octavian	1.0  		  First draft */
/*    07 Jul 2016      Liviu I      1.1       Add fields for BI-3408 */
/******************************************************************************************************************/
/* drop table if exists fact_capacity
create table fact_capacity
LIKE fact_capacity INCLUDING DEFAULTS INCLUDING IDENTITY */

delete from number_fountain m where m.table_name = 'fact_capacity';
insert into number_fountain
select 'fact_capacity',
ifnull(max(f.fact_capacityid ), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_capacity f;

insert into fact_capacity(
fact_capacityid,
dd_capacityid,
dd_capacityid_requirements,
dd_internalcounter,
dd_countercaprqmtsrec)

SELECT
(select max_id   from number_fountain   where table_name = 'fact_capacity') + row_number() over(ORDER BY '') AS fact_capacityid,
kk.KAKO_KAPID,
kbe.KBED_BEDID,
kbe.KBED_BEDZL,
kbe.KBED_CANUM
FROM 
KAKO kk 
INNER JOIN KBED kbe ON kk.KAKO_KAPID = kbe.KBED_KAPID
INNER JOIN KBKO kbk ON kbk.KBKO_BEDID = kbe.KBED_BEDID
WHERE not exists (
	SELECT 1 from fact_capacity cap
	WHERE cap.dd_capacityid = kk.KAKO_KAPID
	AND cap.dd_capacityid_requirements = kbe.KBED_BEDID
	AND cap.dd_internalcounter = kbe.KBED_BEDZL
	AND cap.dd_countercaprqmtsrec = kbe.KBED_CANUM
		);


UPDATE fact_capacity f
SET f.ct_nocapacities = ifnull(kk.KAKO_AZNOR,0),
    f.dw_update_date = current_timestamp
FROM KAKO kk, fact_capacity f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND f.ct_nocapacities <> ifnull(kk.KAKO_AZNOR,0);

UPDATE fact_capacity f
SET f.dd_starttimesec = ifnull(kk.KAKO_BEGZT,0),
    f.dw_update_date = current_timestamp
FROM KAKO kk,fact_capacity f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND f.dd_starttimesec <> ifnull(kk.KAKO_BEGZT,0);

UPDATE fact_capacity f
SET f.dd_finishtimesec = ifnull(kk.KAKO_ENDZT,0),
    f.dw_update_date = current_timestamp
FROM KAKO kk, fact_capacity f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND f.dd_finishtimesec <> ifnull(kk.KAKO_ENDZT,0);

UPDATE fact_capacity f
SET f.dim_capcategoryid = c.dim_capacitycategoryid,
    f.dw_update_date = current_timestamp
FROM KAKO kk, dim_capacitycategory c,fact_capacity f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND c.capcategory = ifnull(kk.KAKO_KAPAR,'Not Set')
AND f.dim_capcategoryid <> c.dim_capacitycategoryid;

UPDATE fact_capacity f
SET f.dim_baseunitofmeasureid = uom.dim_unitofmeasureid,
    f.dw_update_date = current_timestamp
FROM KAKO kk, dim_unitofmeasure uom, fact_capacity f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND uom.uom = ifnull(kk.KAKO_MEINS,'Not Set')
AND f.dim_baseunitofmeasureid <> uom.dim_unitofmeasureid;

UPDATE fact_capacity f
SET f.ct_caputilrate = ifnull(kk.KAKO_NGRAD,0),
    f.dw_update_date = current_timestamp
FROM KAKO kk,fact_capacity f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND f.ct_caputilrate <> ifnull(kk.KAKO_NGRAD,0);

UPDATE fact_capacity f
SET f.dim_capacityplannergroupid = c.dim_capacityplannergroupid,
    f.dw_update_date = current_timestamp
FROM KAKO kk,dim_capacityplannergroup c,fact_capacity f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND capplannergroup = ifnull(kk.KAKO_PLANR,'Not Set')
AND f.dim_capacityplannergroupid <> c.dim_capacityplannergroupid;

UPDATE fact_capacity f
SET f.dim_plantid = p.dim_plantid,
    f.dw_update_date = current_timestamp
FROM KAKO kk, dim_plant p, fact_capacity f
WHERE f.dd_capacityid = kk.KAKO_KAPID
AND p.plantcode = ifnull(kk.KAKO_WERKS,'Not Set')
AND f.dim_plantid <> p.dim_plantid;

UPDATE fact_capacity f
SET f.dd_groupcounter = ifnull(kbk.KBKO_PLNAL,'Not Set'),
    f.dw_update_date = current_timestamp
FROM KBKO kbk,fact_capacity f
WHERE f.dd_capacityid_requirements = kbk.KBKO_BEDID
AND f.dd_groupcounter <> ifnull(kbk.KBKO_PLNAL,'Not Set');

UPDATE fact_capacity f
SET f.dd_keyfortlgroup = ifnull(kbk.KBKO_PLNNR,'Not Set'),
    f.dw_update_date = current_timestamp
FROM KBKO kbk,fact_capacity f
WHERE f.dd_capacityid_requirements = kbk.KBKO_BEDID
AND f.dd_keyfortlgroup <> ifnull(kbk.KBKO_PLNNR,'Not Set');

UPDATE fact_capacity f
SET f.dim_tasklisttypeid = tl.dim_tasklisttypeid,
    f.dw_update_date = current_timestamp
FROM KBKO kbk, dim_tasklisttype tl, fact_capacity f
WHERE f.dd_capacityid_requirements = kbk.KBKO_BEDID
AND tl.tasklisttypecode = ifnull(kbk.KBKO_PLNTY,'Not Set')
AND f.dim_tasklisttypeid <> tl.dim_tasklisttypeid;

/* MERGE INTO fact_capacity fact
USING (SELECT f.fact_capacityid, dd.dim_dateid
		FROM KAKO kk, KBKO kbk, dim_date dd, dim_plant pl, fact_capacity f
		WHERE f.dd_capacityid_requirements = kbk.KBKO_BEDID
		AND dd.datevalue = ifnull(kbk.KBKO_GLTRS,'0001-01-01')
		AND dd.companycode = pl.companycode
		AND pl.plantcode = dd.plantcode_factory
		AND pl.dim_plantid = f.dim_plantid
		AND pl.plantcode = ifnull(kk.KAKO_WERKS,'Not Set')
		AND f.dd_capacityid = kk.KAKO_KAPID
		AND f.dim_dateidscheduledfinish <> dd.dim_dateid) src
ON fact.fact_capacityid = src.fact_capacityid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidscheduledfinish = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP */

MERGE INTO fact_capacity fact
USING (SELECT f.fact_capacityid, dd.dim_dateid
	from fact_capacity f
		inner join KAKO kk on f.dd_capacityid = kk.KAKO_KAPID
		inner join KBKO kbk on f.dd_capacityid_requirements = kbk.KBKO_BEDID
		inner join dim_plant pl on pl.dim_plantid = f.dim_plantid AND pl.plantcode = ifnull(kk.KAKO_WERKS,'Not Set')
		inner join dim_date dd on dd.datevalue = ifnull(kbk.KBKO_GLTRS,'0001-01-01')
								and dd.companycode = pl.companycode
								and ifnull(dd.plantcode_factory,'Not Set') = ifnull(pl.plantcode,'Not Set')
		where f.dim_dateidscheduledfinish <> dd.dim_dateid) src
ON fact.fact_capacityid = src.fact_capacityid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidscheduledfinish = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

MERGE INTO fact_capacity fact
USING (SELECT f.fact_capacityid, dd.dim_dateid
		FROM KAKO kk, KBKO kbk, dim_date dd, dim_plant pl, fact_capacity f
		WHERE f.dd_capacityid_requirements = kbk.KBKO_BEDID
		AND dd.datevalue = ifnull(kbk.KBKO_GSTRS,'0001-01-01')
		AND dd.companycode = pl.companycode
		AND ifnull(pl.plantcode,'Not Set') = ifnull(dd.plantcode_factory,'Not Set')
		AND pl.dim_plantid = f.dim_plantid
		AND ifnull(pl.plantcode,'Not Set') = ifnull(kk.KAKO_WERKS,'Not Set')
		AND f.dd_capacityid = kk.KAKO_KAPID
		AND f.dim_dateidscheduledstart <> dd.dim_dateid) src
ON fact.fact_capacityid = src.fact_capacityid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidscheduledstart = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

/* OZ: LINK to workcenter only on OBJECT ID, there is no OBJECT TYPE in KAKO,KBKO,KBED - first value taken of the last entry in dim_workcenter */
MERGE INTO fact_capacity fact
USING (SELECT DISTINCT f.fact_capacityid, FIRST_VALUE(wc.dim_workcenterid) OVER (PARTITION BY wc.objectid,wc.objecttype ORDER BY wc.objectid DESC) AS dim_workcenterid
		FROM KBED kbe, dim_workcenter wc,fact_capacity f
		WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
		AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
		AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
		AND wc.objectid = ifnull(CONVERT(VARCHAR(8),kbe.KBED_ARBID),'Not Set')
		AND f.dim_workcenterid <> wc.dim_workcenterid) src
ON fact.fact_capacityid = src.fact_capacityid
WHEN MATCHED THEN UPDATE
SET fact.dim_workcenterid = src.dim_workcenterid,
dw_update_date = CURRENT_TIMESTAMP;

MERGE INTO fact_capacity fact
USING (SELECT f.fact_capacityid, dd.dim_dateid
		FROM KAKO kk, KBED kbe, dim_date dd, dim_plant pl,fact_capacity f
		WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
		AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
		AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
		AND dd.datevalue = ifnull(kbe.KBED_IENDD,'0001-01-01')
		AND dd.companycode = pl.companycode
		AND ifnull(pl.plantcode,'Not Set') = ifnull(dd.plantcode_factory, 'Not Set')
		AND pl.dim_plantid = f.dim_plantid		
		AND ifnull(pl.plantcode,'Not Set') = ifnull(kk.KAKO_WERKS,'Not Set')
		AND f.dd_capacityid = kk.KAKO_KAPID
		AND f.dim_dateidactualfinishdate <> dd.dim_dateid) src
ON fact.fact_capacityid = src.fact_capacityid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidactualfinishdate = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

UPDATE fact_capacity f
SET f.ct_schedcaprequirteardown = ifnull(kbe.KBED_KABRSOLL,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe,fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.ct_schedcaprequirteardown <> ifnull(kbe.KBED_KABRSOLL,0);

UPDATE fact_capacity f
SET f.ct_remainingcaprequirteardown = ifnull(kbe.KBED_KBEAREST,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe,fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.ct_remainingcaprequirteardown <> ifnull(kbe.KBED_KBEAREST,0);

UPDATE fact_capacity f
SET f.ct_schedcaprequirprocesing = ifnull(kbe.KBED_KBEASOLL,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe,fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.ct_schedcaprequirprocesing <> ifnull(kbe.KBED_KBEASOLL,0);

UPDATE fact_capacity f
SET f.dim_caprequnitofmeasureid = uom.dim_unitofmeasureid,
    f.dw_update_date = current_timestamp
FROM KBED kbe, dim_unitofmeasure uom, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND uom.uom = ifnull(kbe.KBED_KEINH,'Not Set')
AND f.dim_caprequnitofmeasureid <> uom.dim_unitofmeasureid;

UPDATE fact_capacity f
SET f.ct_schedcaprequirsetup = ifnull(kbe.KBED_KRUESOLL,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.ct_schedcaprequirsetup <> ifnull(kbe.KBED_KRUESOLL,0);

UPDATE fact_capacity f
SET f.dim_activityuomid = uom.dim_unitofmeasureid,
    f.dw_update_date = current_timestamp
FROM KBED kbe, dim_unitofmeasure uom, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND uom.uom = ifnull(kbe.KBED_MEINH,'Not Set')
AND f.dim_activityuomid <> uom.dim_unitofmeasureid;

UPDATE fact_capacity f
SET f.ct_operation = ifnull(kbe.KBED_MGVRG,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe,fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.ct_operation <> ifnull(kbe.KBED_MGVRG,0);

MERGE INTO fact_capacity fact
USING (SELECT f.fact_capacityid, dd.dim_dateid
	   FROM KAKO kk, KBED kbe, dim_date dd, dim_plant pl, fact_capacity f
		WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
		AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
		AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
		AND dd.datevalue = ifnull(kbe.KBED_SENDD,'0001-01-01')
		AND dd.companycode = pl.companycode
		AND ifnull(pl.plantcode,'Not Set') = ifnull(dd.plantcode_factory,'Not Set')
		AND pl.dim_plantid = f.dim_plantid
		AND ifnull(pl.plantcode,'Not Set') = ifnull(kk.KAKO_WERKS,'Not Set')
		AND f.dd_capacityid = kk.KAKO_KAPID
		AND f.dim_dateidlatestfinishdate <> dd.dim_dateid) src
ON fact.fact_capacityid = src.fact_capacityid
WHEN MATCHED THEN UPDATE
SET fact.dim_dateidlatestfinishdate = src.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP;

UPDATE fact_capacity f
SET dd_catobjcauseload = ifnull(kbe.KBED_TYPKZ,'Not Set'),
    f.dw_update_date = current_timestamp
FROM KBED kbe,fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_catobjcauseload <> ifnull(kbe.KBED_TYPKZ,'Not Set');

UPDATE fact_capacity f
SET f.dd_opnumber = ifnull(kbe.KBED_VORNR,'Not Set'),
    f.dw_update_date = current_timestamp
FROM KBED kbe,fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_opnumber <> ifnull(kbe.KBED_VORNR,'Not Set');

/* Start Liviu Ionescu BI-3408 */

UPDATE fact_capacity f

SET f.dd_OperationNumber = ifnull(kbe.KBED_AUFPL,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE f.dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_OperationNumber <> ifnull(kbe.KBED_AUFPL,0);

UPDATE fact_capacity f

SET f.dd_OperationCounter = ifnull(kbe.KBED_APLZL,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE f.dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_OperationCounter <> ifnull(kbe.KBED_APLZL,0);

UPDATE fact_capacity f
SET f.dd_OperationActivity = ifnull(aa.AFVC_VORNR,'Not Set'),
    f.dw_update_date = current_timestamp
FROM AFVC aa, fact_capacity f
WHERE f.dd_OperationNumber = ifnull(aa.AFVC_AUFPL,0)
AND f.dd_OperationCounter = ifnull(aa.AFVC_APLZL,0)
AND f.dd_OperationActivity <> ifnull(aa.AFVC_VORNR,'Not Set');

UPDATE fact_capacity f
SET f.dd_OperationShortText = ifnull(aa.AFVC_LTXA1,'Not Set'),
    f.dw_update_date = current_timestamp
FROM AFVC aa, fact_capacity f
WHERE f.dd_OperationNumber = ifnull(aa.AFVC_AUFPL,0)
AND f.dd_OperationCounter = ifnull(aa.AFVC_APLZL,0)
AND f.dd_OperationShortText <> ifnull(aa.AFVC_LTXA1,'Not Set');

UPDATE fact_capacity f
SET f.dd_OrderCategory = ifnull(kbe.KBED_TYPKZ,'Not Set'),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE f.dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_OrderCategory <> ifnull(kbe.KBED_TYPKZ,'Not Set');

UPDATE fact_capacity f
SET f.dd_ProductionOrder = ifnull(aaa.AFKO_AUFNR,'Not Set'),
    f.dw_update_date = current_timestamp
FROM AFKO_AFPO_AUFK aaa, fact_capacity f
WHERE f.dd_OperationNumber = ifnull(aaa.AFKO_AUFPL,0)
AND f.dd_ProductionOrder <> ifnull(aaa.AFKO_AUFNR,'Not Set');

UPDATE fact_capacity f
SET f.dim_partid = ifnull(dp.dim_partid,1),
    f.dw_update_date = current_timestamp
FROM AFKO_AFPO_AUFK aaa, dim_part dp, dim_plant pl, fact_capacity f
WHERE f.dd_OperationNumber = ifnull(aaa.AFKO_AUFPL,0)
AND ifnull(f.dim_plantid,1) = ifnull(pl.dim_plantid,1)
AND ifnull(aaa.AFKO_PLNBEZ,'Not Set') = ifnull(dp.partnumber,'Not Set')
AND ifnull(pl.plantcode,'Not Set') = ifnull(dp.plant,'Not Set')
AND f.dim_partid <> ifnull(dp.dim_partid,1);

/* Use  PLAF_MATNR when AFKO_PLNBEZ not set */
/* UPDATE fact_capacity f
SET f.dim_partid = ifnull(dp.dim_partid,1),
    f.dw_update_date = current_timestamp
FROM fact_capacity f, KBED kbe, PLAF pf, dim_plant pl, dim_part dp
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND ifnull(f.dim_plantid,1) = ifnull(pl.dim_plantid,1)
AND ifnull(kbe.KBED_PLNUM,'Not Set') = ifnull(pf.PLAF_PLNUM,'Not Set')
AND ifnull(pf.PLAF_MATNR,'Not Set') = ifnull(dp.partnumber,'Not Set')
AND ifnull(pl.plantcode,'Not Set') = ifnull(dp.plant,'Not Set') 
AND f.dim_partid = ifnull(f.dim_partid, 1)
AND f.dim_partid <> ifnull(dp.dim_partid,1) */

UPDATE fact_capacity f
SET f.dim_partid = ifnull(dp.dim_partid,1),
    f.dw_update_date = current_timestamp
FROM fact_capacity f
	inner join KBED kbe on dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
			AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
			AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
	inner join PLAF pf on  ifnull(kbe.KBED_PLNUM,'Not Set') = ifnull(pf.PLAF_PLNUM,'Not Set')
	inner join dim_plant pl on ifnull(f.dim_plantid,1) = ifnull(pl.dim_plantid,1)
			AND ifnull(kbe.KBED_PLNUM,'Not Set') = ifnull(pf.PLAF_PLNUM,'Not Set')
	inner join dim_part dp on ifnull(pf.PLAF_MATNR,'Not Set') = ifnull(dp.partnumber,'Not Set')
			AND ifnull(pl.plantcode,'Not Set') = ifnull(dp.plant,'Not Set')
	where f.dim_partid <> ifnull(dp.dim_partid,1);


UPDATE fact_capacity f
SET f.dd_TaskListNode = ifnull(kbe.KBED_PLNKN,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_TaskListNode <> ifnull(kbe.KBED_PLNKN,0);

UPDATE fact_capacity f
SET f.dd_TaskListCounter  = ifnull(kbe.KBED_ZAEHL,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_TaskListCounter  <> ifnull(kbe.KBED_ZAEHL,0);

UPDATE fact_capacity f
SET f.dd_PlannedOrder  = ifnull(kbe.KBED_PLNUM,'Not Set'),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_PlannedOrder  <> ifnull(kbe.KBED_PLNUM,'Not Set');

UPDATE fact_capacity f
SET f.ct_RemainingCapReqOpSegmentProcessing = ifnull(kbe.KBED_KBEAREST,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.ct_RemainingCapReqOpSegmentProcessing <> ifnull(kbe.KBED_KBEAREST,0);


UPDATE fact_capacity f
SET f.dim_orderDueDateid = ifnull(dd.dim_dateid,1),
    f.dw_update_date = current_timestamp
FROM AFVC_AFVV aa, dim_date dd, dim_plant pl, fact_capacity f
WHERE  f.dd_OperationNumber = ifnull(aa.AFVC_AUFPL,0)
AND f.dd_OperationCounter = ifnull(aa.AFVC_APLZL,0)
AND ifnull(f.dim_plantid,1) = ifnull(pl.dim_plantid,1)
AND ifnull(dd.companycode,'Not Set') = ifnull(pl.companycode,'Not Set')
AND dd.datevalue = ifnull(aa.AFVV_SSEDD,'0001-01-01')
AND ifnull(pl.plantcode,'Not Set') = ifnull(dd.plantcode_factory,'Not Set')
AND f.dim_orderDueDateid <> ifnull(dd.dim_dateid,1);


UPDATE fact_capacity f
SET f.dd_LongTermPlanScen = ifnull(kbe.KBED_PLSCN,0),
    f.dw_update_date = current_timestamp
FROM KBED kbe, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND f.dd_LongTermPlanScen <> ifnull(kbe.KBED_PLSCN,0);

/* UPDATE fact_capacity f
SET f.dim_planmatid = ifnull(dp.dim_partid,1),
    f.dw_update_date = current_timestamp
FROM KBED kbe, PLAF pf, dim_plant pl, dim_part dp, fact_capacity f
WHERE dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
AND ifnull(f.dim_plantid,1) = ifnull(pl.dim_plantid,1)
AND ifnull(kbe.KBED_PLNUM,'Not Set') = ifnull(pf.PLAF_PLNUM,'Not Set')
AND ifnull(pf.PLAF_MATNR,'Not Set') = ifnull(dp.partnumber,'Not Set')
AND ifnull(pl.plantcode,'Not Set') = ifnull(dp.plant,'Not Set')
AND f.dim_planmatid <> ifnull(dp.dim_partid,1) */

UPDATE fact_capacity f
SET f.dim_planmatid = ifnull(dp.dim_partid,1),
    f.dw_update_date = current_timestamp
FROM fact_capacity f
	inner join KBED kbe on dd_capacityid_requirements = ifnull(kbe.KBED_BEDID,0)
			AND f.dd_internalcounter = ifnull(kbe.KBED_BEDZL,0)
			AND f.dd_countercaprqmtsrec = ifnull(kbe.KBED_CANUM,0)
	inner join PLAF pf on  ifnull(kbe.KBED_PLNUM,'Not Set') = ifnull(pf.PLAF_PLNUM,'Not Set')
	inner join dim_plant pl on ifnull(f.dim_plantid,1) = ifnull(pl.dim_plantid,1)
			AND ifnull(kbe.KBED_PLNUM,'Not Set') = ifnull(pf.PLAF_PLNUM,'Not Set')
	inner join dim_part dp on ifnull(pf.PLAF_MATNR,'Not Set') = ifnull(dp.partnumber,'Not Set')
			AND ifnull(pl.plantcode,'Not Set') = ifnull(dp.plant,'Not Set')
	where f.dim_planmatid <> ifnull(dp.dim_partid,1);
	
/* End Liviu Ionescu BI-3408 */

/* Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */
UPDATE fact_capacity am
SET am.std_exchangerate_dateid = dt.dim_dateid 
FROM fact_capacity am,dim_date dt, dim_plant pl 
WHERE
dt.companycode = pl.companycode 
AND dt.datevalue = current_date 
AND ifnull(dt.plantcode_factory,'Not Set') = ifnull(pl.plantcode,'Not Set')
AND am.dim_plantid = pl.dim_plantid
AND am.std_exchangerate_dateid <> dt.dim_dateid;


DELETE FROM fact_capacity cap
WHERE NOT EXISTS (SELECT 1 
FROM 
KAKO kk 
INNER JOIN KBED kbe ON kk.KAKO_KAPID = kbe.KBED_KAPID
INNER JOIN KBKO kbk ON kbk.KBKO_BEDID = kbe.KBED_BEDID
WHERE cap.dd_capacityid = kk.KAKO_KAPID
	AND cap.dd_capacityid_requirements = kbe.KBED_BEDID
	AND cap.dd_internalcounter = kbe.KBED_BEDZL
	AND cap.dd_countercaprqmtsrec = kbe.KBED_CANUM
	);
	
	
/* OZ - TTI factor */
merge into fact_capacity fc 
using (
select distinct wc.dim_workcenterid, c.CKIS_ARBID,
-1 * first_value(c.CKIS_TPREIFX) over (partition by CKIS_ARBID order by CKIS_KADKY desc) ct_ttifactor
from CKIS c, KEKO k, dim_workcenter wc
where c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CASE WHEN wc.objectid = 'Not Set' then 0 else wc.objectid END = c.CKIS_ARBID
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY <= current_date) t
on fc.dim_workcenterid = t.dim_workcenterid
when matched then update
set fc.ct_ttifactor = t.ct_ttifactor
where fc.ct_ttifactor <> t.ct_ttifactor;	


merge into fact_capacity fc 
using (
select distinct wc.dim_workcenterid, c.CKIS_ARBID,
-1 * first_value(c.CKIS_TPREIFX) over (partition by CKIS_ARBID order by CKIS_KADKY desc) ct_ttifactor2
from CKIS c, KEKO k, dim_workcenter wc
where c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CASE WHEN wc.objectid = 'Not Set' then 0 else wc.objectid END = c.CKIS_ARBID
and wc.costcenteractivitytype = ifnull(c.CKIS_LSTAR,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY <= current_date) t
on fc.dim_workcenterid = t.dim_workcenterid
when matched then update
set fc.ct_ttifactor2 = t.ct_ttifactor2
where fc.ct_ttifactor2 <> t.ct_ttifactor2;		
