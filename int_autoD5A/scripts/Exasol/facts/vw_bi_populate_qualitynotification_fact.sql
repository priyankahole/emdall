/*************************************************************************************************/
/* Author : Shanthi Chillara									 */
/* Moved everything in interfaces to the script							 */
/*************************************************************************************************/
/*************************************************************************************************/


DELETE FROM jest
      WHERE NOT EXISTS (SELECT 1
                          FROM qmel
                         WHERE qmel_objnr = JEST_OBJNR)
            AND NOT EXISTS (SELECT 1
                              FROM fact_qualitynotification qn
                             WHERE qn.dd_ObjectNumber = JEST_OBJNR);

DELETE FROM number_fountain m 
WHERE m.table_name = 'fact_qualitynotification';

INSERT INTO number_fountain
SELECT 'fact_qualitynotification',
       ifnull(MAX(fq.fact_qualitynotificationid), ifnull((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_qualitynotification fq;

DROP TABLE IF EXISTS tmp_fact_qualitynotification;

CREATE TABLE tmp_fact_qualitynotification
AS
   SELECT qn.fact_qualitynotificationid fact_qualitynotificationid,   
	ifnull(QMEL_RKMNG, 0) ct_complaintQty,
	ifnull(QMEL_MGFRD, 0) ct_ExternalDefectiveQty,
	ifnull(QMEL_MGEIG, 0) ct_InternalDefectiveQty,
	ifnull(QMEL_RGMNG, 0) ct_returndeliveryqty,
	ifnull(QMEL_MGFRD, 0) ct_QtyRejected,
	convert(integer, 1) ct_LotRejected,
	ifnull(QMEL_BUNAME, 'Not Set') dd_author,
	ifnull(QMEL_CHARG, 'Not Set') dd_batchno,
	ifnull(QMEL_AENAM, 'Not Set') dd_changedby,
	ifnull(QMEL_ERNAM, 'Not Set') dd_createdby,
	ifnull(QMEL_EBELP, 0) dd_documentitemno,
	ifnull(QMEL_EBELN, 'Not Set') dd_documentno,
	ifnull(QMEL_PRUEFLOS, 'Not Set') dd_InspectionLotNo,
	ifnull(QMEL_MBLPO, 0) dd_MaterialDocItemNo,
	ifnull(QMEL_MBLNR, 'Not Set') dd_MaterialDocNo,
	ifnull(QMEL_MJAHR, 0) dd_MaterialDocYear,
	ifnull(q1.QMEL_QMNUM, 'Not Set') dd_notificationo,
	ifnull(QMEL_AUFNR, 'Not Set') dd_orderno,
	ifnull(QMEL_FERTAUFNR, 'Not Set') dd_productionorderno,
	ifnull(QMEL_FERTAUFPL, 0) dd_productionorderplanno,
	ifnull(QMEL_COAUFNR, 'Not Set') dd_qmorderno,
	ifnull(QMEL_AEDAT, '0001-01-01') dd_RecordChanged,
	ifnull(QMEL_ERDAT, '0001-01-01') dd_RecordCreated,
	ifnull(QMEL_QWRNUM, 'Not Set') dd_referencenotificationo,
	ifnull(QMEL_QMNAM, 'Not Set') dd_reportedby,
	ifnull(QMEL_LS_POSNR, 0) dd_SalesOrderDeliveryItemNo,
	ifnull(QMEL_LS_VBELN, 'Not Set') dd_salesorderdeliveryno,
	ifnull(QMEL_LS_KDPOS, 0) dd_SalesOrderItemNo,
	ifnull(QMEL_LS_KDAUF, 'Not Set') dd_SalesOrderNo,
	ifnull(QMEL_VBELN, 'Not Set') dd_SalesOrderNo2,
	ifnull(QMEL_SERIALNR, 'Not Set') dd_serialno,
	ifnull(QMEL_OBJNR, 'Not Set') dd_objectnumber,
	convert(bigint, 1) dim_batchstoragelocationid,
	convert(bigint, 1)  dim_catalogprofileid,
	convert(bigint, 1)  dim_currencyid,
	convert(bigint, 1)  dim_customerid,
	convert(bigint, 1) dim_dateidnotificationcompletion,
	convert(bigint, 1)  dim_dateidnotificationdate,
	convert(bigint, 1)  dim_dateidreturndate,
	convert(bigint, 1)  Dim_dateidrecordcreated,
	convert(bigint, 1)  dim_defectreporttypeid,
	convert(bigint, 1)  dim_distributionchannelid,
	convert(bigint, 1)  dim_inspectioncatalogtypeid,
	convert(bigint, 1)  dim_inspectionlotstoragelocationid,
	convert(bigint, 1)  dim_MaterialGroupid,
	convert(bigint, 1)  dim_notificationoriginid,
	convert(bigint, 1)  dim_notificationphaseid,
	convert(bigint, 1)  dim_notificationpriorityid,
	convert(bigint, 1)  dim_notificationtypeid,
	convert(bigint, 1)  dim_partid,
	convert(bigint, 1) dim_plantid,
	convert(bigint, 1) dim_producthierarchyid,
	convert(bigint, 1) dim_purchasegroupid,
	convert(bigint, 1) dim_purchaseorgid,
	convert(bigint, 1) Dim_SalesDivisionid,
	convert(bigint, 1) dim_SalesGroupid,
	convert(bigint, 1) dim_SalesOfficeid,
	convert(bigint, 1) dim_SalesOrgid,
	convert(bigint, 1)  Dim_UnitOfMeasureid,
	convert(bigint, 1) dim_vendorid,
	convert(bigint, 1) dim_workplantid,
	convert(bigint, 1) dim_qualitynotificationmiscid,
	ifnull(MNCOD, 'Not Set') dd_taskcode,
	ifnull(PARNRKU, 'Not Set') dd_Coordinator,
	ifnull(PARNRZV, 'Not Set') dd_Dispositioner,
	convert(bigint, 1) dim_dateidtaskcreated,
	convert(bigint, 1) dim_dateiddispositionerassigned,
	convert(bigint, 1) dim_dateidtaskcompleted,
	ifnull(ERNAM, 'Not Set') dd_taskcreatedby,
	ifnull(ERLNAM, 'Not Set') dd_taskcompletedby,
	(CASE WHEN QMEL_QMDAB IS NULL THEN 2 ELSE 3 END) Dim_ActionStateid,
	convert(varchar(7), 'Not Set') dd_ZmrbFlag,
	convert(bigint, 1) dim_dateidrequestedstart
	,QMEL_OBJNR --ct_LotRejected
	,QMEL_LGORTCHARG --dim_batchstoragelocationid
	,QMEL_MAWERK --dim_batchstoragelocationid dim_inspectionlotstoragelocationid dim_partid dim_plantid
	,QMEL_RBNR --dim_catalogprofileid
	,QMEL_WAERS --dim_currencyid
	,QMEL_KUNUM --dim_customerid
	,QMEL_QMDAB --dim_dateidnotificationcompletion
	,QMEL_QMDAT --dim_dateidnotificationdate
	,QMEL_RKDAT --dim_dateidreturndate
	,QMEL_ERDAT --Dim_dateidrecordcreated
	,QMEL_FEART --dim_defectreporttypeid
	,QMEL_VTWEG --dim_distributionchannelid
	,QMEL_QMKAT --dim_inspectioncatalogtypeid
	,QMEL_LGORTVORG --dim_inspectionlotstoragelocationid
	,QMEL_MATKL --dim_MaterialGroupid
	,QMEL_HERKZ --dim_notificationoriginid
	,QMEL_PHASE --dim_notificationphaseid
	,QMEL_PRIOK --dim_notificationpriorityid
	,QMEL_ARTPR --dim_notificationpriorityid
	,QMEL_QMART --dim_notificationtypeid
	,QMEL_MATNR --dim_partid
	,QMEL_PRDHA --dim_producthierarchyid
	,QMEL_BKGRP --dim_purchasegroupid
	,QMEL_EKORG --dim_purchaseorgid
	,QMEL_SPART --Dim_SalesDivisionid
	,QMEL_VKGRP --dim_SalesGroupid
	,QMEL_VKBUR --dim_SalesOfficeid
	,QMEL_VKORG --dim_SalesOrgid
	,QMEL_MGEIN --Dim_UnitOfMeasureid
	,QMEL_LIFNUM --dim_vendorid
	,QMEL_ARBPLWERK --dim_workplantid
	,QMEL_MAKNZ --dim_qualitynotificationmiscid
	,QMEL_KZLOESCH --dim_qualitynotificationmiscid
	,QMEL_KZKRI --dim_qualitynotificationmiscid
	,QMEL_KZDKZ --dim_qualitynotificationmiscid
	,QMEL_FEKNZ --dim_qualitynotificationmiscid
	,ERDAT --dim_dateidtaskcreated
	,ERDATZV --dim_dateiddispositionerassigned
	,ERLDAT --dim_dateidtaskcompleted
	,QMEL_STRMN --dim_dateidrequestedstart
FROM qmel q1
	INNER JOIN fact_qualitynotification qn ON qn.dd_notificationo = q1.QMEL_QMNUM
UNION
SELECT 
	ifnull(nf.max_id, 1) + row_number() over (order by '') fact_qualitynotificationid,	
	ifnull(QMEL_RKMNG, 0) ct_complaintQty,
	ifnull(QMEL_MGFRD, 0) ct_ExternalDefectiveQty,
	ifnull(QMEL_MGEIG, 0) ct_InternalDefectiveQty,
	ifnull(QMEL_RGMNG, 0) ct_returndeliveryqty,
	ifnull(QMEL_MGFRD, 0) ct_QtyRejected,
	convert(integer, 1) ct_LotRejected,
	ifnull(QMEL_BUNAME, 'Not Set') dd_author,
	ifnull(QMEL_CHARG, 'Not Set') dd_batchno,
	ifnull(QMEL_AENAM, 'Not Set') dd_changedby,
	ifnull(QMEL_ERNAM, 'Not Set') dd_createdby,
	ifnull(QMEL_EBELP, 0) dd_documentitemno,
	ifnull(QMEL_EBELN, 'Not Set') dd_documentno,
	ifnull(QMEL_PRUEFLOS, 'Not Set') dd_InspectionLotNo,
	ifnull(QMEL_MBLPO, 0) dd_MaterialDocItemNo,
	ifnull(QMEL_MBLNR, 'Not Set') dd_MaterialDocNo,
	ifnull(QMEL_MJAHR, 0) dd_MaterialDocYear,
	ifnull(q1.QMEL_QMNUM, 'Not Set') dd_notificationo,
	ifnull(QMEL_AUFNR, 'Not Set') dd_orderno,
	ifnull(QMEL_FERTAUFNR, 'Not Set') dd_productionorderno,
	ifnull(QMEL_FERTAUFPL, 0) dd_productionorderplanno,
	ifnull(QMEL_COAUFNR, 'Not Set') dd_qmorderno,
	ifnull(QMEL_AEDAT, '0001-01-01') dd_RecordChanged,
	ifnull(QMEL_ERDAT, '0001-01-01') dd_RecordCreated,
	ifnull(QMEL_QWRNUM, 'Not Set') dd_referencenotificationo,
	ifnull(QMEL_QMNAM, 'Not Set') dd_reportedby,
	ifnull(QMEL_LS_POSNR, 0) dd_SalesOrderDeliveryItemNo,
	ifnull(QMEL_LS_VBELN, 'Not Set') dd_salesorderdeliveryno,
	ifnull(QMEL_LS_KDPOS, 0) dd_SalesOrderItemNo,
	ifnull(QMEL_LS_KDAUF, 'Not Set') dd_SalesOrderNo,
	ifnull(QMEL_VBELN, 'Not Set') dd_SalesOrderNo2,
	ifnull(QMEL_SERIALNR, 'Not Set') dd_serialno,
	ifnull(QMEL_OBJNR, 'Not Set') dd_objectnumber,
	convert(bigint, 1) dim_batchstoragelocationid,
	convert(bigint, 1)  dim_catalogprofileid,
	convert(bigint, 1)  dim_currencyid,
	convert(bigint, 1)  dim_customerid,
	convert(bigint, 1) dim_dateidnotificationcompletion,
	convert(bigint, 1)  dim_dateidnotificationdate,
	convert(bigint, 1)  dim_dateidreturndate,
	convert(bigint, 1)  Dim_dateidrecordcreated,
	convert(bigint, 1)  dim_defectreporttypeid,
	convert(bigint, 1)  dim_distributionchannelid,
	convert(bigint, 1)  dim_inspectioncatalogtypeid,
	convert(bigint, 1)  dim_inspectionlotstoragelocationid,
	convert(bigint, 1)  dim_MaterialGroupid,
	convert(bigint, 1)  dim_notificationoriginid,
	convert(bigint, 1)  dim_notificationphaseid,
	convert(bigint, 1)  dim_notificationpriorityid,
	convert(bigint, 1)  dim_notificationtypeid,
	convert(bigint, 1)  dim_partid,
	convert(bigint, 1) dim_plantid,
	convert(bigint, 1) dim_producthierarchyid,
	convert(bigint, 1) dim_purchasegroupid,
	convert(bigint, 1) dim_purchaseorgid,
	convert(bigint, 1) Dim_SalesDivisionid,
	convert(bigint, 1) dim_SalesGroupid,
	convert(bigint, 1) dim_SalesOfficeid,
	convert(bigint, 1) dim_SalesOrgid,
	convert(bigint, 1)  Dim_UnitOfMeasureid,
	convert(bigint, 1) dim_vendorid,
	convert(bigint, 1) dim_workplantid,
	convert(bigint, 1) dim_qualitynotificationmiscid,
	ifnull(MNCOD, 'Not Set') dd_taskcode,
	ifnull(PARNRKU, 'Not Set') dd_Coordinator,
	ifnull(PARNRZV, 'Not Set') dd_Dispositioner,
	convert(bigint, 1) dim_dateidtaskcreated,
	convert(bigint, 1) dim_dateiddispositionerassigned,
	convert(bigint, 1) dim_dateidtaskcompleted,
	ifnull(ERNAM, 'Not Set') dd_taskcreatedby,
	ifnull(ERLNAM, 'Not Set') dd_taskcompletedby,
	(CASE WHEN QMEL_QMDAB IS NULL THEN 2 ELSE 3 END) Dim_ActionStateid,
	convert(varchar(7), 'Not Set') dd_ZmrbFlag,
	convert(bigint, 1) dim_dateidrequestedstart
	,QMEL_OBJNR --ct_LotRejected
	,QMEL_LGORTCHARG --dim_batchstoragelocationid
	,QMEL_MAWERK --dim_batchstoragelocationid dim_inspectionlotstoragelocationid dim_partid dim_plantid
	,QMEL_RBNR --dim_catalogprofileid
	,QMEL_WAERS --dim_currencyid
	,QMEL_KUNUM --dim_customerid
	,QMEL_QMDAB --dim_dateidnotificationcompletion
	,QMEL_QMDAT --dim_dateidnotificationdate
	,QMEL_RKDAT --dim_dateidreturndate
	,QMEL_ERDAT --Dim_dateidrecordcreated
	,QMEL_FEART --dim_defectreporttypeid
	,QMEL_VTWEG --dim_distributionchannelid
	,QMEL_QMKAT --dim_inspectioncatalogtypeid
	,QMEL_LGORTVORG --dim_inspectionlotstoragelocationid
	,QMEL_MATKL --dim_MaterialGroupid
	,QMEL_HERKZ --dim_notificationoriginid
	,QMEL_PHASE --dim_notificationphaseid
	,QMEL_PRIOK --dim_notificationpriorityid
	,QMEL_ARTPR --dim_notificationpriorityid
	,QMEL_QMART --dim_notificationtypeid
	,QMEL_MATNR --dim_partid
	,QMEL_PRDHA --dim_producthierarchyid
	,QMEL_BKGRP --dim_purchasegroupid
	,QMEL_EKORG --dim_purchaseorgid
	,QMEL_SPART --Dim_SalesDivisionid
	,QMEL_VKGRP --dim_SalesGroupid
	,QMEL_VKBUR --dim_SalesOfficeid
	,QMEL_VKORG --dim_SalesOrgid
	,QMEL_MGEIN --Dim_UnitOfMeasureid
	,QMEL_LIFNUM --dim_vendorid
	,QMEL_ARBPLWERK --dim_workplantid
	,QMEL_MAKNZ --dim_qualitynotificationmiscid
	,QMEL_KZLOESCH --dim_qualitynotificationmiscid
	,QMEL_KZKRI --dim_qualitynotificationmiscid
	,QMEL_KZDKZ --dim_qualitynotificationmiscid
	,QMEL_FEKNZ --dim_qualitynotificationmiscid
	,ERDAT --dim_dateidtaskcreated
	,ERDATZV --dim_dateiddispositionerassigned
	,ERLDAT --dim_dateidtaskcompleted
	,QMEL_STRMN --dim_dateidrequestedstart
FROM QMEL q1
	INNER JOIN number_fountain nf ON nf.table_name = 'fact_qualitynotification'
WHERE q1.QMEL_QMNUM NOT IN (SELECT DISTINCT dd_notificationo FROM fact_qualitynotification);


UPDATE tmp_fact_qualitynotification f
SET f.dim_plantid = pl.dim_plantid
FROM tmp_fact_qualitynotification f, dim_plant pl
WHERE pl.PlantCode = f.QMEL_MAWERK 
	AND pl.RowIsCurrent = 1
	AND f.dim_plantid <> pl.dim_plantid;
	
update tmp_fact_qualitynotification tmp
set tmp.ct_LotRejected = 0
where tmp.QMEL_OBJNR in ( SELECT DISTINCT j.JEST_OBJNR 
			FROM jest j 
			WHERE j.JEST_STAT IN ('I0076', 'I0072')
	 		 AND j.JEST_INACT IS NULL)
	AND QMEL_OBJNR IS NOT NULL
	AND tmp.ct_LotRejected <> 0;

update tmp_fact_qualitynotification f
set f.dim_batchstoragelocationid = sl1.Dim_StorageLocationid
from dim_storagelocation sl1, tmp_fact_qualitynotification f
where sl1.LocationCode = QMEL_LGORTCHARG
        AND sl1.RowIsCurrent = 1
        AND sl1.Plant = ifnull(f.QMEL_MAWERK, 'Not Set')
	AND QMEL_LGORTCHARG IS NOT NULL
	AND f.dim_batchstoragelocationid <> sl1.Dim_StorageLocationid;

update tmp_fact_qualitynotification f
set f.dim_catalogprofileid = cp.Dim_CatalogProfileid
from dim_catalogprofile cp, tmp_fact_qualitynotification f
where cp.CatalogProfileCode = ifnull(f.QMEL_RBNR, 'Not Set')
	AND cp.RowIsCurrent = 1
	AND f.dim_catalogprofileid <> cp.Dim_CatalogProfileid;

update tmp_fact_qualitynotification f
set f.dim_currencyid = dc.Dim_Currencyid
from dim_currency dc, tmp_fact_qualitynotification f
where dc.CurrencyCode = ifnull(f.QMEL_WAERS, 'Not Set')
	and f.dim_currencyid <> dc.Dim_Currencyid;
		  
update tmp_fact_qualitynotification f
set f.dim_customerid = c.dim_customerid
from dim_customer c, tmp_fact_qualitynotification f
where c.CustomerNumber = ifnull(f.QMEL_KUNUM, 'Not Set')
          AND c.RowIsCurrent = 1
	  AND f.dim_customerid <> c.dim_customerid;

update tmp_fact_qualitynotification f
set f.dim_dateidnotificationcompletion = dnc.dim_dateid
FROM dim_date dnc, dim_plant pl, tmp_fact_qualitynotification f
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
    AND dnc.DateValue = f.QMEL_QMDAB
    AND dnc.CompanyCode = pl.CompanyCode
	AND dnc.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
    AND QMEL_QMDAB IS NOT NULL
    AND f.dim_dateidnotificationcompletion <> dnc.dim_dateid;

update tmp_fact_qualitynotification f
set f.dim_dateidnotificationdate = nd.dim_dateid
FROM dim_date nd, dim_plant pl, tmp_fact_qualitynotification f
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
      AND nd.DateValue = QMEL_QMDAT
      AND nd.CompanyCode = pl.CompanyCode
	  AND nd.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
      AND QMEL_QMDAT IS NOT NULL
      AND f.dim_dateidnotificationdate <> nd.dim_dateid;

update tmp_fact_qualitynotification f
set f.dim_dateidreturndate = rd.dim_dateid
FROM dim_date rd, dim_plant pl, tmp_fact_qualitynotification f
where pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND rd.DateValue = QMEL_RKDAT
        AND rd.CompanyCode = pl.CompanyCode
	AND rd.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
	AND QMEL_RKDAT IS NOT NULL
	AND f.dim_dateidreturndate <> rd.dim_dateid;

update tmp_fact_qualitynotification f
set f.Dim_dateidrecordcreated = rd.dim_dateid
FROM dim_date rd, dim_plant pl, tmp_fact_qualitynotification f
where pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1
	AND rd.DateValue = QMEL_ERDAT
        AND rd.CompanyCode = pl.CompanyCode
	AND rd.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
	AND QMEL_ERDAT IS NOT NULL
	AND f.Dim_dateidrecordcreated <> rd.dim_dateid;
	
update tmp_fact_qualitynotification f
SET f.dim_defectreporttypeid = drt.dim_defectreporttypeid
from dim_defectreporttype drt, tmp_fact_qualitynotification f
where drt.DefectReportTypeCode = ifnull(QMEL_FEART, 'Not Set')
	AND drt.RowIsCurrent = 1
	AND f.dim_defectreporttypeid <> drt.dim_defectreporttypeid;

update tmp_fact_qualitynotification f
set f.dim_distributionchannelid = dch.dim_distributionchannelid
from dim_distributionchannel dch, tmp_fact_qualitynotification f
where dch.DistributionChannelCode = ifnull(QMEL_VTWEG, 'Not Set')
	AND dch.RowIsCurrent = 1
	AND f.dim_distributionchannelid <> dch.dim_distributionchannelid;
 
update tmp_fact_qualitynotification f
set f.dim_inspectioncatalogtypeid = ict.dim_inspectioncatalogtypeid
from dim_inspectioncatalogtype ict, tmp_fact_qualitynotification f
where ict.InspectionCatalogTypeCode = ifnull(QMEL_QMKAT, 'Not Set')
	AND ict.RowIsCurrent = 1
 	AND f.dim_inspectioncatalogtypeid <> ict.dim_inspectioncatalogtypeid;
 
update tmp_fact_qualitynotification f
set f.dim_inspectionlotstoragelocationid = sl2.dim_storagelocationid
FROM dim_storagelocation sl2, tmp_fact_qualitynotification f
WHERE  sl2.LocationCode = QMEL_LGORTVORG
         AND sl2.RowIsCurrent = 1
         AND sl2.Plant = ifnull(QMEL_MAWERK, 'Not Set')
	 AND QMEL_LGORTVORG IS NOT NULL
	 AND f.dim_inspectionlotstoragelocationid <> sl2.dim_storagelocationid;

update tmp_fact_qualitynotification f
set f.dim_MaterialGroupid = mg.dim_MaterialGroupid
FROM dim_MaterialGroup mg, tmp_fact_qualitynotification f
WHERE mg.MaterialGroupCode = ifnull(QMEL_MATKL, 'Not Set')
  	AND mg.RowIsCurrent = 1
	AND f.dim_MaterialGroupid <> mg.dim_MaterialGroupid;
		  
update tmp_fact_qualitynotification f
set f.dim_notificationoriginid = nf.dim_notificationoriginid
FROM dim_notificationorigin nf, tmp_fact_qualitynotification f
WHERE nf.NotificationOriginCode = ifnull(QMEL_HERKZ, 'Not Set')
  	AND nf.RowIsCurrent = 1	
	AND f.dim_notificationoriginid <> nf.dim_notificationoriginid;

update tmp_fact_qualitynotification f
set f.dim_notificationphaseid = np.dim_notificationphaseid
from dim_notificationphase np, tmp_fact_qualitynotification f
WHERE np.NotificationPhaseCode = ifnull(QMEL_PHASE, 'Not Set')
  	AND np.RowIsCurrent = 1
	AND f.dim_notificationphaseid <> np.dim_notificationphaseid;
	
update tmp_fact_qualitynotification f
set f.dim_notificationpriorityid = np.dim_notificationpriorityid
FROM dim_notificationpriority np, tmp_fact_qualitynotification f
WHERE np.NotificationPriorityCode = ifnull(QMEL_PRIOK, 'Not Set')
  	AND np.NotificationPriorityType = ifnull(QMEL_ARTPR, 'Not Set')
  	AND np.RowIsCurrent = 1
	AND f.dim_notificationpriorityid <> np.dim_notificationpriorityid;
	
update tmp_fact_qualitynotification f
set f.dim_notificationtypeid = nt.dim_notificationtypeid
FROM dim_notificationtype nt, tmp_fact_qualitynotification f
WHERE nt.NotificationTypeCode = ifnull(QMEL_QMART, 'Not Set')
  	AND nt.RowIsCurrent = 1
	AND f.dim_notificationtypeid <> nt.dim_notificationtypeid;
 
update tmp_fact_qualitynotification f
set f.dim_partid = dp.dim_partid
FROM dim_part dp, tmp_fact_qualitynotification f
WHERE dp.PartNumber = ifnull(QMEL_MATNR, 'Not Set')
     AND dp.Plant = ifnull(QMEL_MAWERK, 'Not Set')
     AND dp.RowIsCurrent = 1 
     AND f.dim_partid <> dp.dim_partid;
         
update tmp_fact_qualitynotification f
set f.dim_producthierarchyid = ph.dim_producthierarchyid
FROM dim_producthierarchy ph, tmp_fact_qualitynotification f
WHERE ph.ProductHierarchy = ifnull(QMEL_PRDHA, 'Not Set')
  	AND ph.RowIsCurrent = 1  
	AND f.dim_producthierarchyid <> ph.dim_producthierarchyid;
  
update tmp_fact_qualitynotification f
set f.dim_purchasegroupid = pg.dim_purchasegroupid
FROM dim_purchasegroup pg, tmp_fact_qualitynotification f
WHERE pg.PurchaseGroup = ifnull(QMEL_BKGRP, 'Not Set')
  	AND pg.RowIsCurrent = 1  
	AND f.dim_purchasegroupid <> pg.dim_purchasegroupid;
    	  
update tmp_fact_qualitynotification f
set f.dim_purchaseorgid = po.dim_purchaseorgid
FROM dim_purchaseorg po, tmp_fact_qualitynotification f
WHERE po.PurchaseOrgCode = ifnull(QMEL_EKORG, 'Not Set')
  	AND po.RowIsCurrent = 1
	AND f.dim_purchaseorgid <> po.dim_purchaseorgid;

update tmp_fact_qualitynotification f
set f.Dim_SalesDivisionid = sd.Dim_SalesDivisionid
FROM Dim_SalesDivision sd, tmp_fact_qualitynotification f
WHERE sd.DivisionCode = ifnull(QMEL_SPART, 'Not Set')
	AND f.Dim_SalesDivisionid <> sd.Dim_SalesDivisionid;
	
update tmp_fact_qualitynotification f
set f.dim_SalesGroupid = sg.dim_SalesGroupid
FROM dim_SalesGroup sg, tmp_fact_qualitynotification f
WHERE sg.SalesGroupCode = ifnull(QMEL_VKGRP, 'Not Set')
	AND f.dim_SalesGroupid <> sg.dim_SalesGroupid;

update tmp_fact_qualitynotification f
set f.dim_SalesOfficeid = so.dim_SalesOfficeid
FROM dim_SalesOffice so, tmp_fact_qualitynotification f
WHERE so.SalesOfficeCode = ifnull(QMEL_VKBUR, 'Not Set')
  	AND so.RowIsCurrent = 1
	AND f.dim_SalesOfficeid <> so.dim_SalesOfficeid;
		  
update tmp_fact_qualitynotification f
set f.dim_SalesOrgid = sog.dim_SalesOrgid
FROM dim_SalesOrg sog, tmp_fact_qualitynotification f
WHERE sog.SalesOrgCode = ifnull(QMEL_VKORG, 'Not Set') 
	AND f.dim_SalesOrgid <> sog.dim_SalesOrgid;     
      
update tmp_fact_qualitynotification f
set f.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid
FROM Dim_UnitOfMeasure uom, tmp_fact_qualitynotification f
WHERE uom.UOM = ifnull(QMEL_MGEIN, 'Not Set')
  	AND uom.RowIsCurrent = 1   
	AND f.Dim_UnitOfMeasureid <> uom.Dim_UnitOfMeasureid;
 
update tmp_fact_qualitynotification f
set f.dim_vendorid = v.dim_vendorid
FROM dim_vendor v, tmp_fact_qualitynotification f
WHERE v.VendorNumber = ifnull(QMEL_LIFNUM, 'Not Set')
	AND v.RowIsCurrent = 1
	AND f.dim_vendorid <> v.dim_vendorid;
	
update tmp_fact_qualitynotification f
set f.dim_workplantid = wp.dim_plantid
FROM dim_plant wp, tmp_fact_qualitynotification f
WHERE wp.PlantCode = ifnull(QMEL_ARBPLWERK, 'Not Set')
  	AND wp.RowIsCurrent = 1
	AND f.dim_workplantid <> wp.dim_plantid;
   
update tmp_fact_qualitynotification f
set f.dim_qualitynotificationmiscid = qnm.dim_qualitynotificationmiscid
FROM dim_qualitynotificationmisc qnm, tmp_fact_qualitynotification f
WHERE     TaskRecordsExist = ifnull(QMEL_MAKNZ, 'Not Set')
	  AND DeleteDataRecord = ifnull(QMEL_KZLOESCH, 'Not Set')
	  AND CriticalPart = ifnull(QMEL_KZKRI, 'Not Set')
	  AND DocumentationRequired = ifnull(QMEL_KZDKZ, 'Not Set')
	  AND ErrorRecordsExist = ifnull(QMEL_FEKNZ, 'Not Set')
	  AND f.dim_qualitynotificationmiscid <> qnm.dim_qualitynotificationmiscid;
		  
update tmp_fact_qualitynotification f
set f.dim_dateidtaskcreated = dnc.dim_dateid
FROM dim_date dnc, dim_plant pl, tmp_fact_qualitynotification f
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1 
	AND dnc.DateValue = ERDAT
 	AND dnc.CompanyCode = pl.CompanyCode
	AND dnc.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
	AND ERDAT IS NOT NULL
	AND f.dim_dateidtaskcreated <> dnc.dim_dateid;

update tmp_fact_qualitynotification f
set f.dim_dateiddispositionerassigned = dnc.dim_dateid
FROM dim_date dnc, dim_plant pl, tmp_fact_qualitynotification f
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1 
	AND dnc.DateValue = ERDATZV
 	AND dnc.CompanyCode = pl.CompanyCode
	AND dnc.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
	AND ERDATZV IS NOT NULL
	AND f.dim_dateiddispositionerassigned <> dnc.dim_dateid;
						 
update tmp_fact_qualitynotification f
set f.dim_dateidtaskcompleted = dnc.dim_dateid
FROM dim_date dnc, dim_plant pl, tmp_fact_qualitynotification f
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1 
	AND dnc.DateValue = ERLDAT
 	AND dnc.CompanyCode = pl.CompanyCode
	AND dnc.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
	AND ERLDAT IS NOT NULL
	AND f.dim_dateidtaskcompleted <> dnc.dim_dateid;
						 
update tmp_fact_qualitynotification f
set f.dd_ZmrbFlag = 'X'
FROM QMSM q, tmp_fact_qualitynotification f
WHERE q.QMSM_MNCOD = 'ZMRB' AND q.QMSM_QMNUM = f.dd_notificationo 
	AND q.QMSM_KZLOESCH IS NULL
	AND f.dd_ZmrbFlag <> 'X';
       
update tmp_fact_qualitynotification f
set f.dim_dateidrequestedstart = dnc.dim_dateid
FROM dim_date dnc, dim_plant pl, tmp_fact_qualitynotification f
WHERE pl.PlantCode = f.QMEL_MAWERK AND pl.RowIsCurrent = 1 
	AND dnc.DateValue = QMEL_STRMN
        AND dnc.CompanyCode = pl.CompanyCode
	AND dnc.plantcode_factory = pl.plantcode  /* plantcode_factory additional condition- BI-5085 */
	AND QMEL_STRMN IS NOT NULL
	AND f.dim_dateidrequestedstart <> dnc.dim_dateid;


MERGE INTO fact_qualitynotification f
USING tmp_fact_qualitynotification t ON f.fact_qualitynotificationid = t.fact_qualitynotificationid
WHEN MATCHED THEN UPDATE SET 
	f.ct_complaintQty = t.ct_complaintQty
	,f.ct_ExternalDefectiveQty = t.ct_ExternalDefectiveQty
	,f.ct_InternalDefectiveQty = t.ct_InternalDefectiveQty
	,f.ct_returndeliveryqty = t.ct_returndeliveryqty
	,f.ct_QtyRejected = t.ct_QtyRejected
	,f.ct_LotRejected = t.ct_LotRejected
	,f.dd_author = t.dd_author
	,f.dd_batchno = t.dd_batchno
	,f.dd_changedby = t.dd_changedby
	,f.dd_createdby = t.dd_createdby
	,f.dd_documentitemno = t.dd_documentitemno
	,f.dd_documentno = t.dd_documentno
	,f.dd_InspectionLotNo = t.dd_InspectionLotNo
	,f.dd_MaterialDocItemNo = t.dd_MaterialDocItemNo
	,f.dd_MaterialDocNo = t.dd_MaterialDocNo
	,f.dd_MaterialDocYear = t.dd_MaterialDocYear
	,f.dd_notificationo = t.dd_notificationo
	,f.dd_orderno = t.dd_orderno
	,f.dd_productionorderno = t.dd_productionorderno
	,f.dd_productionorderplanno = t.dd_productionorderplanno
	,f.dd_qmorderno = t.dd_qmorderno
	,f.dd_RecordChanged = t.dd_RecordChanged
	,f.dd_RecordCreated = t.dd_RecordCreated
	,f.dd_referencenotificationo = t.dd_referencenotificationo
	,f.dd_reportedby = t.dd_reportedby
	,f.dd_SalesOrderDeliveryItemNo = t.dd_SalesOrderDeliveryItemNo
	,f.dd_salesorderdeliveryno = t.dd_salesorderdeliveryno
	,f.dd_SalesOrderItemNo = t.dd_SalesOrderItemNo
	,f.dd_SalesOrderNo = t.dd_SalesOrderNo
	,f.dd_SalesOrderNo2 = t.dd_SalesOrderNo2
	,f.dd_serialno = t.dd_serialno
	,f.dd_objectnumber = t.dd_objectnumber
	,f.dim_batchstoragelocationid = t.dim_batchstoragelocationid
	,f.dim_catalogprofileid = t.dim_catalogprofileid
	,f.dim_currencyid = t.dim_currencyid
	,f.dim_customerid = t.dim_customerid
	,f.dim_dateidnotificationcompletion = t.dim_dateidnotificationcompletion
	,f.dim_dateidnotificationdate = t.dim_dateidnotificationdate
	,f.dim_dateidreturndate = t.dim_dateidreturndate
	,f.Dim_dateidrecordcreated = t.Dim_dateidrecordcreated
	,f.dim_defectreporttypeid = t.dim_defectreporttypeid
	,f.dim_distributionchannelid = t.dim_distributionchannelid
	,f.dim_inspectioncatalogtypeid = t.dim_inspectioncatalogtypeid
	,f.dim_inspectionlotstoragelocationid = t.dim_inspectionlotstoragelocationid
	,f.dim_MaterialGroupid = t.dim_MaterialGroupid
	,f.dim_notificationoriginid = t.dim_notificationoriginid
	,f.dim_notificationphaseid = t.dim_notificationphaseid
	,f.dim_notificationpriorityid = t.dim_notificationpriorityid
	,f.dim_notificationtypeid = t.dim_notificationtypeid
	,f.dim_partid = t.dim_partid
	,f.dim_plantid = t.dim_plantid
	,f.dim_producthierarchyid = t.dim_producthierarchyid
	,f.dim_purchasegroupid = t.dim_purchasegroupid
	,f.dim_purchaseorgid = t.dim_purchaseorgid
	,f.Dim_SalesDivisionid = t.Dim_SalesDivisionid
	,f.dim_SalesGroupid = t.dim_SalesGroupid
	,f.dim_SalesOfficeid = t.dim_SalesOfficeid
	,f.dim_SalesOrgid = t.dim_SalesOrgid
	,f.Dim_UnitOfMeasureid = t.Dim_UnitOfMeasureid
	,f.dim_vendorid = t.dim_vendorid
	,f.dim_workplantid = t.dim_workplantid
	,f.dim_qualitynotificationmiscid = t.dim_qualitynotificationmiscid
	,f.dd_taskcode = t.dd_taskcode
	,f.dd_Coordinator = t.dd_Coordinator
	,f.dd_Dispositioner = t.dd_Dispositioner
	,f.dim_dateidtaskcreated = t.dim_dateidtaskcreated
	,f.dim_dateiddispositionerassigned = t.dim_dateiddispositionerassigned
	,f.dim_dateidtaskcompleted = t.dim_dateidtaskcompleted
	,f.dd_taskcreatedby = t.dd_taskcreatedby
	,f.dd_taskcompletedby = t.dd_taskcompletedby
	,f.Dim_ActionStateid = t.Dim_ActionStateid
	,f.dd_ZmrbFlag = t.dd_ZmrbFlag
	,f.dim_dateidrequestedstart = t.dim_dateidrequestedstart;								        	    	  	     	     

INSERT INTO fact_qualitynotification(fact_qualitynotificationid,
                                     ct_complaintQty,
                                     ct_ExternalDefectiveQty,
                                     ct_InternalDefectiveQty,
                                     ct_returndeliveryqty,
                                     ct_QtyRejected,
                                     ct_LotRejected,
                                     dd_author,
                                     dd_batchno,
                                     dd_changedby,
                                     dd_createdby,
                                     dd_documentitemno,
                                     dd_documentno,
                                     dd_InspectionLotNo,
                                     dd_MaterialDocItemNo,
                                     dd_MaterialDocNo,
                                     dd_MaterialDocYear,
                                     dd_notificationo,
                                     dd_orderno,
                                     dd_productionorderno,
                                     dd_productionorderplanno,
                                     dd_qmorderno,
                                     dd_RecordChanged,
                                     dd_RecordCreated,
                                     dd_referencenotificationo,
                                     dd_reportedby,
                                     dd_SalesOrderDeliveryItemNo,
                                     dd_salesorderdeliveryno,
                                     dd_SalesOrderItemNo,
                                     dd_SalesOrderNo,
                                     dd_SalesOrderNo2,
                                     dd_serialno,
                                     dd_objectnumber,
                                     dim_batchstoragelocationid,
                                     dim_catalogprofileid,
                                     dim_currencyid,
                                     dim_customerid,
                                     dim_dateidnotificationcompletion,
                                     dim_dateidnotificationdate,
                                     dim_dateidreturndate,
                                     Dim_dateidrecordcreated,
                                     dim_defectreporttypeid,
                                     dim_distributionchannelid,
                                     dim_inspectioncatalogtypeid,
                                     dim_inspectionlotstoragelocationid,
                                     dim_MaterialGroupid,
                                     dim_notificationoriginid,
                                     dim_notificationphaseid,
                                     dim_notificationpriorityid,
                                     dim_notificationtypeid,
                                     dim_partid,
                                     dim_plantid,
                                     dim_producthierarchyid,
                                     dim_purchasegroupid,
                                     dim_purchaseorgid,
                                     Dim_SalesDivisionid,
                                     dim_SalesGroupid,
                                     dim_SalesOfficeid,
                                     dim_SalesOrgid,
                                     Dim_UnitOfMeasureid,
                                     dim_vendorid,
                                     dim_workplantid,
                                     dim_qualitynotificationmiscid,
                                     dd_taskcode,
                                     dd_Coordinator,
                                     dd_Dispositioner,
                                     dim_dateidtaskcreated,
                                     dim_dateiddispositionerassigned,
                                     dim_dateidtaskcompleted,
                                     dd_taskcreatedby,
                                     dd_taskcompletedby,
                                     Dim_ActionStateid,
                                     dd_ZmrbFlag,
                                     dim_dateidrequestedstart)
SELECT
	fact_qualitynotificationid   
	,ct_complaintQty
	,ct_ExternalDefectiveQty
	,ct_InternalDefectiveQty
	,ct_returndeliveryqty
	,ct_QtyRejected
    ,ct_LotRejected
	,dd_author
	,dd_batchno
	,dd_changedby
	,dd_createdby
	,dd_documentitemno
	,dd_documentno
	,dd_InspectionLotNo
	,dd_MaterialDocItemNo
	,dd_MaterialDocNo
	,dd_MaterialDocYear
	,dd_notificationo
	,dd_orderno
	,dd_productionorderno
	,dd_productionorderplanno
	,dd_qmorderno
	,dd_RecordChanged
	,dd_RecordCreated
	,dd_referencenotificationo
	,dd_reportedby
	,dd_SalesOrderDeliveryItemNo
	,dd_salesorderdeliveryno
	,dd_SalesOrderItemNo
	,dd_SalesOrderNo
	,dd_SalesOrderNo2
	,dd_serialno
	,dd_objectnumber
	,dim_batchstoragelocationid
	,dim_catalogprofileid
	,dim_currencyid
	,dim_customerid
	,dim_dateidnotificationcompletion
	,dim_dateidnotificationdate
	,dim_dateidreturndate
	,Dim_dateidrecordcreated
	,dim_defectreporttypeid
	,dim_distributionchannelid
	,dim_inspectioncatalogtypeid
	,dim_inspectionlotstoragelocationid
	,dim_MaterialGroupid
    ,dim_notificationoriginid
    ,dim_notificationphaseid
    ,dim_notificationpriorityid
	,dim_notificationtypeid
    ,dim_partid
	,dim_plantid
    ,dim_producthierarchyid
    ,dim_purchasegroupid
    ,dim_purchaseorgid
    ,Dim_SalesDivisionid
    ,dim_SalesGroupid
    ,dim_SalesOfficeid
    ,dim_SalesOrgid
    ,Dim_UnitOfMeasureid
    ,dim_vendorid
    ,dim_workplantid
    ,dim_qualitynotificationmiscid
	,dd_taskcode
	,dd_Coordinator
	,dd_Dispositioner
	,dim_dateidtaskcreated
    ,dim_dateiddispositionerassigned
	,dim_dateidtaskcompleted
	,dd_taskcreatedby
	,dd_taskcompletedby
	,Dim_ActionStateid
	,dd_ZmrbFlag
	,dim_dateidrequestedstart
FROM tmp_fact_qualitynotification t
WHERE NOT EXISTS
             (SELECT 1
                FROM fact_qualitynotification qn
               WHERE qn.dd_notificationo = t.dd_notificationo);

			   
DROP TABLE IF EXISTS tmp_dim_qualnotificationstatus;
CREATE TABLE  tmp_dim_qualnotificationstatus AS 
SELECT distinct j1.JEST_OBJNR AS dd_ObjectNumber,
       'Not Set' AS Outstanding,
       'Not Set' AS Postponed,
       'Not Set' AS InProcess,
       'Not Set' AS Completed,
       'Not Set' as DeletionFlag FROM JEST j1;

UPDATE   tmp_dim_qualnotificationstatus
SET Outstanding = 'X'
FROM JEST j, tmp_dim_qualnotificationstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0068' 
AND j.JEST_INACT is NULL; 

UPDATE   tmp_dim_qualnotificationstatus
SET Postponed = 'X'
FROM JEST j, tmp_dim_qualnotificationstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0069' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_qualnotificationstatus
SET InProcess = 'X'
FROM JEST j, tmp_dim_qualnotificationstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0070' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_qualnotificationstatus
SET Completed = 'X'
FROM JEST j, tmp_dim_qualnotificationstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0072' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_qualnotificationstatus
SET DeletionFlag = 'X'
FROM JEST j, tmp_dim_qualnotificationstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0076' 
AND j.JEST_INACT is NULL;

UPDATE fact_qualitynotification qn
SET qn.dim_notificationstatusid = ns.dim_notificationstatusid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM
       dim_notificationstatus ns,
       QMEL q1,
       tmp_dim_qualnotificationstatus t1,
       fact_qualitynotification qn
	
 WHERE  qn.dd_notificationo = q1.QMEL_QMNUM
       AND ns.Outstanding = t1.Outstanding
       AND ns.Postponed = t1.Postponed 
       AND ns.InProcess = t1.InProcess 
       AND ns.Completed = t1.Completed
       AND ns.DeletionFlag = t1.DeletionFlag
       ANd qn.dd_ObjectNumber = t1.dd_ObjectNumber
       AND qn.dim_notificationstatusid <> ns.dim_notificationstatusid;

DROP TABLE IF EXISTS tmp_dim_qualnotificationstatus;
DROP TABLE IF EXISTS tmp_updt_QtyRejectedExternalAmt;
CREATE TABLE tmp_updt_QtyRejectedExternalAmt
AS
SELECT 
	ct_QtyRejected
	,ct_InternalDefectiveQty
	,dim_notificationtypeid
	,dim_notificationstatusid
	,dd_MaterialDocItemNo
	,dd_MaterialDocYear
	,dd_MaterialDocNo
	,row_number() over(partition by dd_MaterialDocItemNo,dd_MaterialDocYear,dd_MaterialDocNo order by dd_MaterialDocYear) rownumber
FROM fact_qualitynotification
WHERE dim_dateidnotificationcompletion <> 1;

DELETE FROM tmp_updt_QtyRejectedExternalAmt WHERE rownumber <> 1;

UPDATE fact_materialmovement f_mm  --PRODUS cartezian
SET f_mm.QtyRejectedExternalAmt =

          IFNULL(
             (CASE
                 WHEN nt.QualityNotification = 1 THEN qn.ct_QtyRejected
                 ELSE 0
              END),
             0),
       f_mm.QtyRejectedInternalAmt =

          IFNULL(
             (CASE
                 WHEN (nt.NotificationTypeCode = 'H1'
                       AND ns.DeletionFlag <> 'X')
                 THEN
                    qn.ct_InternalDefectiveQty
                 ELSE
                    0
              END),
             0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_materialmovement f_mm,
	dim_notificationtype nt,
	dim_notificationstatus ns,



	tmp_updt_QtyRejectedExternalAmt qn
WHERE qn.dim_notificationtypeid = nt.Dim_NotificationTypeid
	AND qn.dim_notificationstatusid = ns.dim_notificationstatusid
	AND qn.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo
	AND qn.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
	AND qn.dd_MaterialDocNo = f_mm.dd_MaterialDocNo;
	
DROP TABLE IF EXISTS tmp_updt_QtyRejectedExternalAmt;
UPDATE facT_qualitynotification
SET dim_notificationoriginid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_notificationoriginid IS NULL;

UPDATE facT_qualitynotification
SET dim_vendorid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_vendorid IS NULL;

UPDATE facT_qualitynotification
SET dim_materialgroupid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_materialgroupid IS NULL;

UPDATE facT_qualitynotification
SET dim_unitofmeasureid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_unitofmeasureid IS NULL;

UPDATE facT_qualitynotification
SET dim_notificationtypeid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_notificationtypeid IS NULL;

UPDATE facT_qualitynotification
SET dim_inspectioncatalogtypeid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_inspectioncatalogtypeid IS NULL;

UPDATE facT_qualitynotification
SET dim_catalogprofileid = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_catalogprofileid IS NULL;

DROP TABLE IF EXISTS tmp_updt_dd_VendorMaterialNo;
CREATE TABLE tmp_updt_dd_VendorMaterialNo
AS
SELECT dd_DocumentNo, dd_documentitemno, dd_VendorMaterialNo, ROW_NUMBER() OVER(PARTITION BY dd_DocumentNo, dd_documentitemno ORDER BY '') rownumber
FROM fact_purchase;

DELETE FROM tmp_updt_dd_VendorMaterialNo WHERE rownumber <> 1;

UPDATE fact_qualitynotification fq  --produs cartezian
SET fq.dd_VendorMaterialNo = IFNULL(fp.dd_VendorMaterialNo,'Not Set')
	,dw_update_date = CURRENT_TIMESTAMP /* Added automatically by update_dw_update_date.pl*/
FROM fact_qualitynotification fq, tmp_updt_dd_VendorMaterialNo fp
WHERE fp.dd_DocumentNo = fq.dd_documentno
  AND fp.dd_documentitemno = fq.dd_documentitemno
  AND fq.dd_VendorMaterialNo <> IFNULL(fp.dd_VendorMaterialNo,'Not Set');
  
DROP TABLE IF EXISTS tmp_updt_dd_VendorMaterialNo;

update fact_qualitynotification 
set dd_VendorMaterialNo = 'Not Set' 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dd_VendorMaterialNo is null;

update fact_qualitynotification fq
set fq.Dim_codetextCausesid = ifnull(fi.Dim_codetextCausesid,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_Qualitynotificationitem fi, fact_qualitynotification fq
where fq.dd_notificationo = fi.dd_NotificationNo
	and fq.Dim_codetextCausesid <> ifnull(fi.Dim_codetextCausesid,1);

update fact_qualitynotification 
set Dim_codetextCausesid = 1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where Dim_codetextCausesid is null;


drop table if exists tmp_updt_dd_NumberOfDefects;
create table tmp_updt_dd_NumberOfDefects 
as 
select dd_NumberOfDefects, dd_NotificationNo, row_number() over ( partition by dd_NotificationNo order by '') rownumber
from fact_Qualitynotificationitem;

DELETE FROM tmp_updt_dd_NumberOfDefects WHERE rownumber <> 1;

update fact_qualitynotification fq
set fq.dd_NumberOfDefects = ifnull(fi.dd_NumberOfDefects,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from tmp_updt_dd_NumberOfDefects fi, fact_qualitynotification fq
where fq.dd_notificationo = fi.dd_NotificationNo
	and fq.dd_NumberOfDefects <> ifnull(fi.dd_NumberOfDefects,0);

update fact_qualitynotification 
set dd_NumberOfDefects = 0 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dd_NumberOfDefects is null;

update fact_qualitynotification fq
set fq.dd_defectnumber = ifnull(fi.dd_defectnumber,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_Qualitynotificationitem fi, fact_qualitynotification fq
where fq.dd_notificationo = fi.dd_NotificationNo
	and fq.dd_defectnumber <> ifnull(fi.dd_defectnumber,0);

update fact_qualitynotification 
set dd_defectnumber = 0 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dd_defectnumber is null;

update fact_qualitynotification ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp, fact_qualitynotification ia
where ia.dim_partid = dp.dim_partid and
ia.dd_batchno = b.batchnumber and
dp.partnumber = b.partnumber
and dp.plant = b.plantcode
and ia.dim_batchid <> b.dim_batchid;

 /* Update std_exchangerate_dateid by FPOPESCU on 05 January 2016  */

UPDATE fact_qualitynotification f
SET f.std_exchangerate_dateid = dt.dim_dateid
FROM fact_qualitynotification f inner join dim_plant p on f.dim_plantid = p.dim_plantid
	 INNER JOIN dim_date dt ON dt.companycode = p.companycode AND dt.plantcode_factory = p.plantcode
AND dt.datevalue = current_date
WHERE   f.std_exchangerate_dateid <> dt.dim_dateid;



