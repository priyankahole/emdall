	/* Octavian: 13/10 : Insert missing combinations in dim_part for NL10 plant */
	delete from number_fountain m 
	where m.table_name = 'dim_part';

	insert into number_fountain
	select 'dim_part', ifnull(max(d.dim_partid ),1)
	from dim_part d;

	insert into dim_part(dim_partid,partnumber,plant)
	select 
	(select max_id from number_fountain where table_name = 'dim_part') + row_number() over(order by '') as dim_partid,t.*
	from
	(select distinct Z1CA_SCM_TRSHEET_MATNR,'NL10' as plant
	from Z1CA_SCM_TRSHEET a inner join dim_part b 
	on a.Z1CA_SCM_TRSHEET_MATNR =  b.partnumber 
	and  Z1CA_SCM_TRSHEET_PLANT = 'NL10' 
	and b.plant <> 'NL10' where not exists (select 1 from dim_part dp
	where dp.partnumber = a.Z1CA_SCM_TRSHEET_MATNR and dp.plant = 'NL10')) t;
	
	
	merge into dim_part dp
	using
	(select distinct dp.dim_partid, ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set') as PartType
	from dim_part dp,Z1CA_SCM_TRSHEET b
	where dp.partnumber = b.Z1CA_SCM_TRSHEET_MATNR
	and dp.plant = b.Z1CA_SCM_TRSHEET_PLANT
	and dp.plant = 'NL10'
	and dp.parttype = 'Not Set'
	and PartType <> ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')) t
	on dp.dim_partid = t.dim_partid
	when matched then update
	set dp.parttype = t.parttype;
	
	
	merge into dim_part dp
	using
(select DISTINCT dim_partid,max(ifnull(b.Z1CA_SCM_TRSHEET_GPF, 'Not Set')) as productfamily_merck
	from dim_part dp,Z1CA_SCM_TRSHEET b
	where dp.partnumber = b.Z1CA_SCM_TRSHEET_MATNR
	and dp.plant = b.Z1CA_SCM_TRSHEET_PLANT
	and dp.plant = 'NL10'
	and dp.productfamily_merck = 'Not Set'
and productfamily_merck <> ifnull(b.Z1CA_SCM_TRSHEET_GPF, 'Not Set')
group by dim_partid) t
	on dp.dim_partid = t.dim_partid
	when matched then update
	set dp.productfamily_merck = t.productfamily_merck;
	
/*	merge into dim_part dp
	using
	(select distinct dp.dim_partid, ifnull(b.Z1CA_SCM_TRSHEET_GPF, 'Not Set') as productfamily_merck
	from dim_part dp,Z1CA_SCM_TRSHEET b
	where dp.partnumber = b.Z1CA_SCM_TRSHEET_MATNR
	and dp.plant = b.Z1CA_SCM_TRSHEET_PLANT
	and dp.plant = 'NL10'
	and dp.productfamily_merck = 'Not Set'
	and productfamily_merck <> ifnull(b.Z1CA_SCM_TRSHEET_GPF, 'Not Set')) t
	on dp.dim_partid = t.dim_partid
	when matched then update
	set dp.productfamily_merck = t.productfamily_merck
	
	 */
	
	/* Octavian: 13/10 : End changes */
	
	delete from number_fountain m 
	where m.table_name = 'fact_materialworkflow';

	insert into number_fountain
	   select 'fact_materialworkflow', ifnull(max(f.fact_materialworkflowid ),1)
	   from fact_materialworkflow f;
	
	/* Update to take TRANSFER_PRICE2 from the second extraction */
	
	update Z1CA_SCM_TRSHEET a 
	set a.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2 = b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2
	from Z1CA_SCM_TRSHEET_FOR_TRANSFER b, Z1CA_SCM_TRSHEET a 
	where  ifnull(a.Z1CA_SCM_TRSHEET_GPF,'Not Set')=  ifnull(b.Z1CA_SCM_TRSHEET_GPF,'Not Set')
		   and ifnull(a.Z1CA_SCM_TRSHEET_MATNR,'Not Set') =ifnull(b.Z1CA_SCM_TRSHEET_MATNR,'Not Set')
		   and ifnull(a.Z1CA_SCM_TRSHEET_ML,'Not Set')=ifnull(b.Z1CA_SCM_TRSHEET_ML,'Not Set')
		   and ifnull(a.Z1CA_SCM_TRSHEET_MTART,'Not Set')=ifnull(b.Z1CA_SCM_TRSHEET_MTART,'Not Set')
		   and ifnull(a.Z1CA_SCM_TRSHEET_PROJECT,'Not Set')=ifnull(b.Z1CA_SCM_TRSHEET_PROJECT,'Not Set')
		   and ifnull(a.Z1CA_SCM_TRSHEET_WI_ID,'Not Set')=ifnull(b.Z1CA_SCM_TRSHEET_WI_ID,'Not Set')
		   and ifnull(a.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2,'0001-01-01') <> ifnull(b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2,'0001-01-01');   
		   
	drop table if exists tmp_ins_trsheet;

	create table tmp_ins_trsheet
	as
	select 
		ifnull(a.Z1CA_SCM_TRSHEET_WI_PRIO,'Not Set') Z1CA_SCM_TRSHEET_WI_PRIO,
		a.Z1CA_SCM_TRSHEET_WI_ID,
		ifnull(a.Z1CA_SCM_TRSHEET_SCENARIO,'Not Set') Z1CA_SCM_TRSHEET_SCENARIO,
		ifnull(a.Z1CA_SCM_TRSHEET_WI_ICON,'Not Set') Z1CA_SCM_TRSHEET_WI_ICON,
		ifnull(a.Z1CA_SCM_TRSHEET_WI_AAGENT,'Not Set') Z1CA_SCM_TRSHEET_WI_AAGENT,
		ifnull(a.Z1CA_SCM_TRSHEET_INTCOM,'Not Set')  Z1CA_SCM_TRSHEET_INTCOM,
		ifnull(a.Z1CA_SCM_TRSHEET_ISACODE,'Not Set')   Z1CA_SCM_TRSHEET_ISACODE,
		ifnull(convert(varchar(10), a.Z1CA_SCM_TRSHEET_IP_NUMBER),'Not Set')  Z1CA_SCM_TRSHEET_IP_NUMBER,
		ifnull(a.Z1CA_SCM_TRSHEET_IP_DESCRIPTION,'Not Set')  Z1CA_SCM_TRSHEET_IP_DESCRIPTION,
		ifnull(a.Z1CA_SCM_TRSHEET_IP_ITEMTXT,'Not Set')  Z1CA_SCM_TRSHEET_IP_ITEMTXT,
		ifnull(a.Z1CA_SCM_TRSHEET_IP_STATUS,'Not Set')   Z1CA_SCM_TRSHEET_IP_STATUS,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSWI,'Not Set')  Z1CA_SCM_TRSHEET_STATUSWI,
		ifnull(a.Z1CA_SCM_TRSHEET_LIGHT_SCM,'Not Set')  Z1CA_SCM_TRSHEET_LIGHT_SCM,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSSCM,'Not Set')  Z1CA_SCM_TRSHEET_STATUSSCM,
		ifnull(a.Z1CA_SCM_TRSHEET_LIGHT_IPS,'Not Set') 	Z1CA_SCM_TRSHEET_LIGHT_IPS,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSIPS,'Not Set')	Z1CA_SCM_TRSHEET_STATUSIPS,
		ifnull(a.Z1CA_SCM_TRSHEET_LIGHT_IMO,'Not Set')	Z1CA_SCM_TRSHEET_LIGHT_IMO,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSIMO,'Not Set')	Z1CA_SCM_TRSHEET_STATUSIMO,
		ifnull(a.Z1CA_SCM_TRSHEET_LSC_SELECTED,'Not Set')	Z1CA_SCM_TRSHEET_LSC_SELECTED,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSPUR,'Not Set')	Z1CA_SCM_TRSHEET_STATUSPUR,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSPLA,'Not Set')	Z1CA_SCM_TRSHEET_STATUSPLA,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSWAR,'Not Set')	Z1CA_SCM_TRSHEET_STATUSWAR,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSWA2,'Not Set')	Z1CA_SCM_TRSHEET_STATUSWA2,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSQM,'Not Set')	Z1CA_SCM_TRSHEET_STATUSQM,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSFIC,'Not Set')	Z1CA_SCM_TRSHEET_STATUSFIC,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSSAL,'Not Set')	Z1CA_SCM_TRSHEET_STATUSSAL,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSPRO,'Not Set')	Z1CA_SCM_TRSHEET_STATUSPRO,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSQC,'Not Set')	Z1CA_SCM_TRSHEET_STATUSQC,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSNEW,'Not Set')	Z1CA_SCM_TRSHEET_STATUSNEW,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSOCC,'Not Set')	Z1CA_SCM_TRSHEET_STATUSOCC,
		ifnull(a.Z1CA_SCM_TRSHEET_STATUSSER,'Not Set')	Z1CA_SCM_TRSHEET_STATUSSER,
		ifnull(convert(varchar(8), a.Z1CA_SCM_TRSHEET_STLNR),'Not Set')	Z1CA_SCM_TRSHEET_STLNR,
		ifnull(convert(varchar(7), a.Z1CA_SCM_TRSHEET_STLAL),'Not Set')	Z1CA_SCM_TRSHEET_STLAL,
		ifnull(convert(varchar(10), a.Z1CA_SCM_TRSHEET_BOM_STATUS),'Not Set')	Z1CA_SCM_TRSHEET_BOM_STATUS,
		ifnull(a.Z1CA_SCM_TRSHEET_PPI_RELEASE,'Not Set')	Z1CA_SCM_TRSHEET_PPI_RELEASE,
		ifnull(convert(varchar(10), a.Z1CA_SCM_TRSHEET_VBELN),'Not Set')	Z1CA_SCM_TRSHEET_VBELN,
		ifnull(a.Z1CA_SCM_TRSHEET_PV_ICON,'Not Set')	Z1CA_SCM_TRSHEET_PV_ICON,
		IFNULL(a.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') Z1CA_SCM_TRSHEET_PROJECT,
		IFNULL(a.Z1CA_SCM_TRSHEET_ML, 'Not Set') Z1CA_SCM_TRSHEET_ML,
		IFNULL(a.Z1CA_SCM_TRSHEET_ML_DESCR, 'Not Set') Z1CA_SCM_TRSHEET_ML_DESCR,
		IFNULL(a.Z1CA_SCM_TRSHEET_ML_REQNR, 'Not Set') Z1CA_SCM_TRSHEET_ML_REQNR,
		IFNULL(a.Z1CA_SCM_TRSHEET_MATNR, 'Not Set') Z1CA_SCM_TRSHEET_MATNR,
		IFNULL(a.Z1CA_SCM_TRSHEET_PLANT, 'Not Set') Z1CA_SCM_TRSHEET_PLANT 
	from Z1CA_SCM_TRSHEET a where not exists 
	 								(select 1 from fact_materialworkflow  x 
											  where x.dd_workflowid = ifnull(a.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set')
												and x.dd_projectcode = ifnull(a.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set')
												and x.dd_ml = ifnull(a.Z1CA_SCM_TRSHEET_ML, 'Not Set')
												and x.dd_trsheet_matnr = IFNULL(a.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
												and x.dd_trsheet_plant = IFNULL(a.Z1CA_SCM_TRSHEET_PLANT, 'Not Set'));	   

	insert into fact_materialworkflow
	(fact_materialworkflowid,
	dd_priority,
	dd_workflowid,
	dd_workflowscenario,
	dd_workflowlog,
	dd_initiatorofworkflowinstance,
	dim_partid,
	dd_internalcomment,
	dim_plantid,
	dd_isacode,
	dd_ipnumber,
	dd_ipdescription,
	dd_infointroduction,
	dd_ipstatus,
	dd_mainprocessstatus,
	dd_scmintro,
	dd_scmintrostatus,
	dd_ips,
	dd_ipsstatus,
	dd_imosite,
	dd_imositestatus,
	dd_matlconfigurelocalsalescompany,
	dd_purchasingstatus,
	dd_planningstatus,
	dd_warehousestatus,
	dd_warehhazmatstatus,
	dd_qmstatus,
	dd_ficostatus,
	dd_salesstatus,
	dd_productionstatus,
	dd_qcstatus,
	dd_qregnew,
	dd_qregocc,
	dd_qregser,
	dd_billofmaterial,
	dd_alternativebom,
	dd_bomstatus,
	dd_ppirelease,
	dd_salesdocument,
	dd_productversion,
	ct_economicorderquantity,
	dd_projectcode,
	dd_ml,
	dd_mldescription,
	dd_mlreqnr,
	dd_trsheet_matnr,
	dd_trsheet_plant,
	dw_insert_date,
	dw_update_date)
	select 
	(select max_id 
			from number_fountain 
			where table_name = 'fact_materialworkflow') + row_number() over(order by '') AS fact_materialworkflowid,
	ifnull(convert(varchar(7), a.Z1CA_SCM_TRSHEET_WI_PRIO),'Not Set') dd_priority,
	ifnull(convert(varchar(10), a.Z1CA_SCM_TRSHEET_WI_ID),'Not Set') dd_workflowid,
	ifnull(a.Z1CA_SCM_TRSHEET_SCENARIO,'Not Set') dd_workflowscenario,
	ifnull(a.Z1CA_SCM_TRSHEET_WI_ICON,'Not Set') dd_workflowlog,
	ifnull(a.Z1CA_SCM_TRSHEET_WI_AAGENT,'Not Set') dd_initiatorofworkflowinstance,
	ifnull(b.dim_partid,1)  dim_partid,
	ifnull(a.Z1CA_SCM_TRSHEET_INTCOM,'Not Set')  dd_internalcomment,
	ifnull(c.dim_plantid,1)  dim_plantid,
	ifnull(a.Z1CA_SCM_TRSHEET_ISACODE,'Not Set')   dd_isacode,
	ifnull(convert(varchar(10), a.Z1CA_SCM_TRSHEET_IP_NUMBER),'Not Set')  dd_ipnumber,
	ifnull(a.Z1CA_SCM_TRSHEET_IP_DESCRIPTION,'Not Set')  dd_ipdescription,
	ifnull(a.Z1CA_SCM_TRSHEET_IP_ITEMTXT,'Not Set')  dd_infointroduction,
	ifnull(a.Z1CA_SCM_TRSHEET_IP_STATUS,'Not Set')   dd_ipstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSWI,'Not Set')  dd_mainprocessstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_LIGHT_SCM,'Not Set')  dd_scmintro,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSSCM,'Not Set')  dd_scmintrostatus,
	ifnull(a.Z1CA_SCM_TRSHEET_LIGHT_IPS,'Not Set') 	dd_ips,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSIPS,'Not Set')	dd_ipsstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_LIGHT_IMO,'Not Set')	dd_imosite,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSIMO,'Not Set')	dd_imositestatus,
	ifnull(a.Z1CA_SCM_TRSHEET_LSC_SELECTED,'Not Set')	dd_matlconfigurelocalsalescompany,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSPUR,'Not Set')	dd_purchasingstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSPLA,'Not Set')	dd_planningstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSWAR,'Not Set')	dd_warehousestatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSWA2,'Not Set')	dd_warehhazmatstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSQM,'Not Set')	dd_qmstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSFIC,'Not Set')	dd_ficostatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSSAL,'Not Set')	dd_salesstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSPRO,'Not Set')	dd_productionstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSQC,'Not Set')	dd_qcstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSNEW,'Not Set')	dd_qregnew,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSOCC,'Not Set')	dd_qregocc,
	ifnull(a.Z1CA_SCM_TRSHEET_STATUSSER,'Not Set')	dd_qregser,
	ifnull(convert(varchar(8), a.Z1CA_SCM_TRSHEET_STLNR),'Not Set')	dd_billofmaterial,
	ifnull(convert(varchar(7), a.Z1CA_SCM_TRSHEET_STLAL),'Not Set')	dd_alternativebom,
	ifnull(convert(varchar(10), a.Z1CA_SCM_TRSHEET_BOM_STATUS),'Not Set')	dd_bomstatus,
	ifnull(a.Z1CA_SCM_TRSHEET_PPI_RELEASE,'Not Set')	dd_ppirelease,
	ifnull(convert(varchar(10), a.Z1CA_SCM_TRSHEET_VBELN),'Not Set')	dd_salesdocument,
	ifnull(a.Z1CA_SCM_TRSHEET_PV_ICON,'Not Set')	dd_productversion,
	0 ct_economicorderquantity,
	IFNULL(a.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') dd_projectcode,
	IFNULL(a.Z1CA_SCM_TRSHEET_ML, 'Not Set') dd_ml,
	IFNULL(a.Z1CA_SCM_TRSHEET_ML_DESCR, 'Not Set') dd_mldescription,
	IFNULL(a.Z1CA_SCM_TRSHEET_ML_REQNR, 'Not Set') dd_mlreqnr,
	IFNULL(a.Z1CA_SCM_TRSHEET_MATNR, 'Not Set') dd_trsheet_matnr,
	IFNULL(a.Z1CA_SCM_TRSHEET_PLANT, 'Not Set') dd_trsheet_plant,
	current_timestamp as dw_insert_date,
	current_timestamp as dw_update_date                                 
	from tmp_ins_trsheet a
	left join dim_part b
		 on a.Z1CA_SCM_TRSHEET_MATNR =  b.partnumber and a.Z1CA_SCM_TRSHEET_PLANT = b.plant
	left join dim_plant c
		 on a.Z1CA_SCM_TRSHEET_PLANT = c.plantcode
	where not exists (select 1 from fact_materialworkflow  x 
					  where x.dd_workflowid = a.Z1CA_SCM_TRSHEET_WI_ID
							and x.dd_projectcode = ifnull(a.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set')
							and x.dd_ml = ifnull(a.Z1CA_SCM_TRSHEET_ML, 'Not Set')
							and x.dd_trsheet_matnr = IFNULL(a.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
							and x.dd_trsheet_plant = IFNULL(a.Z1CA_SCM_TRSHEET_PLANT, 'Not Set')
							);
/*06 Jan 2017 Georgiana according to BI-5173 moving these updates after the insert statement*/
    update fact_materialworkflow a 
	set a.dim_partid = b.dim_partid
	from dim_part b,  fact_materialworkflow a 
	where a.dd_trsheet_matnr = b.partnumber
	and a.dd_trsheet_plant = b.plant
	and a.dim_partid <> b.dim_partid;

	update fact_materialworkflow a 
	set a.dim_plantid = b.dim_plantid
	from dim_plant b, fact_materialworkflow a 
	where a.dd_trsheet_plant = b.plantcode
	and a.dim_plantid <> b.dim_plantid;
/*06 Jan 2017 End*/	

    MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, 
		     ifnull(convert(varchar(7), b.Z1CA_SCM_TRSHEET_WI_PRIO),'Not Set') dd_priority,	 
             ifnull(b.Z1CA_SCM_TRSHEET_SCENARIO,'Not Set') dd_workflowscenario,
             ifnull(b.Z1CA_SCM_TRSHEET_WI_ICON,'Not Set') dd_workflowlog, 
             ifnull(b.Z1CA_SCM_TRSHEET_WI_AAGENT,'Not Set') dd_initiatorofworkflowinstance,	
             ifnull(b.Z1CA_SCM_TRSHEET_INTCOM,'Not Set')  dd_internalcomment,	
             ifnull(b.Z1CA_SCM_TRSHEET_ISACODE,'Not Set')   dd_isacode,
			 ifnull(convert(varchar(10), b.Z1CA_SCM_TRSHEET_IP_NUMBER),'Not Set')  dd_ipnumber,
	         ifnull(b.Z1CA_SCM_TRSHEET_IP_DESCRIPTION,'Not Set')  dd_ipdescription,
			ifnull(b.Z1CA_SCM_TRSHEET_IP_ITEMTXT,'Not Set')  dd_infointroduction,
			ifnull(b.Z1CA_SCM_TRSHEET_IP_STATUS,'Not Set')   dd_ipstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSWI,'Not Set')  dd_mainprocessstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_LIGHT_SCM,'Not Set')  dd_scmintro,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSSCM,'Not Set')  dd_scmintrostatus,
			ifnull(b.Z1CA_SCM_TRSHEET_LIGHT_IPS,'Not Set') 	dd_ips,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSIPS,'Not Set')	dd_ipsstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_LIGHT_IMO,'Not Set')	dd_imosite,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSIMO,'Not Set')	dd_imositestatus,
			ifnull(b.Z1CA_SCM_TRSHEET_LSC_SELECTED,'Not Set')	dd_matlconfigurelocalsalescompany,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSPUR,'Not Set')	dd_purchasingstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSPLA,'Not Set')	dd_planningstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSWAR,'Not Set')	dd_warehousestatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSWA2,'Not Set')	dd_warehhazmatstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSQM,'Not Set')	dd_qmstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSFIC,'Not Set')	dd_ficostatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSSAL,'Not Set')	dd_salesstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSPRO,'Not Set')	dd_productionstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSQC,'Not Set')	dd_qcstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSNEW,'Not Set')	dd_qregnew,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSOCC,'Not Set')	dd_qregocc,
			ifnull(b.Z1CA_SCM_TRSHEET_STATUSSER,'Not Set')	dd_qregser,
			ifnull(convert(varchar(8), b.Z1CA_SCM_TRSHEET_STLNR),'Not Set')	dd_billofmaterial,
			ifnull(convert(varchar(7), b.Z1CA_SCM_TRSHEET_STLAL),'Not Set')	dd_alternativebom,
			ifnull(convert(varchar(10), b.Z1CA_SCM_TRSHEET_BOM_STATUS),'Not Set')	dd_bomstatus,
			ifnull(b.Z1CA_SCM_TRSHEET_PPI_RELEASE,'Not Set')	dd_ppirelease,
			ifnull(convert(varchar(10), b.Z1CA_SCM_TRSHEET_VBELN),'Not Set')	dd_salesdocument,
			ifnull(b.Z1CA_SCM_TRSHEET_PV_ICON,'Not Set')	dd_productversion,
			IFNULL(b.Z1CA_SCM_TRSHEET_ML_DESCR, 'Not Set') dd_mldescription,
			IFNULL(b.Z1CA_SCM_TRSHEET_ML_REQNR, 'Not Set') dd_mlreqnr
    from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dd_priority = s.dd_priority,
	                             t.dd_workflowscenario = s.dd_workflowscenario,
	                             t.dd_workflowlog = s.dd_workflowlog,
	                             t.dd_initiatorofworkflowinstance = s.dd_initiatorofworkflowinstance,
	                             t.dd_internalcomment = s.dd_internalcomment,
	                             t.dd_isacode = s.dd_isacode,
	                             t.dd_ipnumber = s.dd_ipnumber,
	                             t.dd_ipdescription = s.dd_ipdescription,
	                             t.dd_infointroduction = s.dd_infointroduction,
	                             t.dd_ipstatus = s.dd_ipstatus,
	                             t.dd_mainprocessstatus = s.dd_mainprocessstatus,
	                             t.dd_scmintro = s.dd_scmintro,
	                             t.dd_scmintrostatus = s.dd_scmintrostatus,
	                             t.dd_ips = s.dd_ips,
	                             t.dd_ipsstatus = s.dd_ipsstatus,
	                             t.dd_imosite = s.dd_imosite,
	                             t.dd_imositestatus = s.dd_imositestatus,
	                             t.dd_matlconfigurelocalsalescompany = s.dd_matlconfigurelocalsalescompany,
	                             t.dd_purchasingstatus = s.dd_purchasingstatus,
	                             t.dd_planningstatus = s.dd_planningstatus,
	                             t.dd_warehousestatus = s.dd_warehousestatus,
	                             t.dd_warehhazmatstatus = s.dd_warehhazmatstatus,
	                             t.dd_qmstatus = s.dd_qmstatus,
	                             t.dd_ficostatus = s.dd_ficostatus,
	                             t.dd_salesstatus = s.dd_salesstatus,
	                             t.dd_productionstatus = s.dd_productionstatus,
	                             t.dd_qcstatus = s.dd_qcstatus,
	                             t.dd_qregnew = s.dd_qregnew,
	                             t.dd_qregocc = s.dd_qregocc,
	                             t.dd_qregser = s.dd_qregser,
	                             t.dd_billofmaterial = s.dd_billofmaterial,
	                             t.dd_alternativebom = s.dd_alternativebom,
	                             t.dd_bomstatus = s.dd_bomstatus,
	                             t.dd_ppirelease = s.dd_ppirelease,
	                             t.dd_salesdocument = s.dd_salesdocument,
	                             t.dd_productversion = s.dd_productversion,
	                             t.dd_mldescription = s.dd_mldescription,
	                             t.dd_mlreqnr = s.dd_mlreqnr ;			
		 
	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_vendorid,1) dim_vendorid
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')  
					and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set') 
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_vendor d on IFNULL(b.Z1CA_SCM_TRSHEET_LIFNR, 'Not Set') = d.VendorNumber 
		  /* condition added to remove ambiguous replace*/
		  where Z1CA_SCM_TRSHEET_LIFNR is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_vendorid = s.dim_vendorid;
	
/*Georgiana 09 MAr 2015 Part of EA changes: adding dim_vendormasterid*/
		MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_vendormasterid,1) dim_vendormasterid
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')  
					and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set') 
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl ON a.dim_plantidshipto = pl.dim_plantid
                    and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = pl.plantcode
		  left join dim_vendormaster d on IFNULL(b.Z1CA_SCM_TRSHEET_LIFNR, 'Not Set') = d.VendorNumber
		  and ifnull(pl.companycode, 'Not Set') = ifnull(d.CompanyCode, 'Not Set')
		  /* condition added to remove ambiguous replace*/
		  where Z1CA_SCM_TRSHEET_LIFNR is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_vendormasterid = s.dim_vendormasterid;
/*09 Mar 2016 End of Changes*/

/*Georgiana Changes 21 Sept 2016- BI-4196 changed the logic in order to use dd_ml instead of customernumber and removed dd_ml not like '001%' condition*/

MERGE INTO fact_materialworkflow t
USING (
select distinct fact_materialworkflowid, ifnull(d.dim_customermastersalesid,1) dim_customermastersalesid
from fact_materialworkflow a
left join (select  max(dim_customermastersalesid) as dim_customermastersalesid ,ItemProposal from
dim_customermastersales
WHERE 
ItemProposal<>'Not Set'
AND SalesOrg in ('NL10','US10')
group by ItemProposal ) d
on IFNULL(a.dd_ipnumber, 'Not Set') = d.ItemProposal

) s
ON t.fact_materialworkflowid = s.fact_materialworkflowid
WHEN MATCHED THEN UPDATE SET t.dim_customermastersalesid = s.dim_customermastersalesid;

	 	 
		 
	DROP TABLE IF EXISTS tmp_fact_materialworkflow_001;

	CREATE TABLE tmp_fact_materialworkflow_001
	AS
	select f.fact_materialworkflowid, d.customernumber, d.SalesOrg, d.DistributionChannel, dp.partnumber,f.dd_ml
	from fact_materialworkflow f 
			inner join dim_customermastersales d on f.dim_customermastersalesid = d.dim_customermastersalesid
			inner join dim_part dp on f.dim_partid = dp.dim_partid;

	DROP TABLE IF EXISTS tmp_fact_materialworkflow_002;

	CREATE TABLE tmp_fact_materialworkflow_002
	AS		
	SELECT KOTG004_KUNNR CustomerNumber, KOTG004_VKORG SalesOrg,  KOTG004_VTWEG DistributionChannel, KOTG004_MATNR PartNumber,
		   MAX(KOTG004_DATBI) dd_validto, MAX(KOTG004_DATAB) dd_validfrom,
		   convert(bigint, 1) as dim_dateidvalidto, convert(bigint, 1) as dim_dateidvalidfrom 
	FROM KOTG004 a 
	GROUP BY KOTG004_KUNNR, KOTG004_VKORG, KOTG004_MATNR, KOTG004_VTWEG;	

	UPDATE tmp_fact_materialworkflow_002 a
	SET a.dim_dateidvalidto = b.dim_dateid
	FROM dim_date b, tmp_fact_materialworkflow_002 a
	WHERE a.dd_validto = b.datevalue AND b.CompanyCode = 'Not Set' AND b.plantcode_factory = 'Not Set';

	UPDATE tmp_fact_materialworkflow_002 a
	SET a.dim_dateidvalidfrom = b.dim_dateid
	FROM dim_date b, tmp_fact_materialworkflow_002 a
	WHERE a.dd_validfrom = b.datevalue AND b.CompanyCode = 'Not Set' AND b.plantcode_factory = 'Not Set';

	MERGE INTO fact_materialworkflow t
	USING (SELECT a.fact_materialworkflowid, IFNULL(b.dim_dateidvalidto,1) dim_dateidvalidto, IFNULL(b.dim_dateidvalidfrom, 1) dim_dateidvalidfrom
		   FROM tmp_fact_materialworkflow_001 a 
				LEFT JOIN tmp_fact_materialworkflow_002 b
				ON a.dd_ml = b.CustomerNumber and a.SalesOrg = b.SalesOrg 
				and a.DistributionChannel = b.DistributionChannel and a.PartNumber = b.PartNumber
		   ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidvalidto = s.dim_dateidvalidto, t.dim_dateidvalidfrom = s.dim_dateidvalidfrom;		
/* Georgiana End of Changes*/	
	DROP TABLE IF EXISTS tmp_fact_materialworkflow_001;

	CREATE TABLE tmp_fact_materialworkflow_001
	AS
	select f.fact_materialworkflowid, d.customernumber, dp.partnumber
	from fact_materialworkflow f 
			inner join dim_customermastersales d on f.dim_customermastersalesid = d.dim_customermastersalesid
			inner join dim_part dp on f.dim_partid = dp.dim_partid;
			
	DROP TABLE IF EXISTS tmp_fact_materialworkflow_002;

	CREATE TABLE tmp_fact_materialworkflow_002
	AS			
	select Z1SD_SR_SHIPTO_KUNAG customernumber, Z1SD_SR_SHIPTO_MATNR partnumber, Z1SD_SR_SHIPTO_WERKS plantcode, convert(bigint,1) dim_plantidshipto
	from Z1SD_SR_SHIPTO;

	UPDATE tmp_fact_materialworkflow_002 a	
	SET a.dim_plantidshipto = b.dim_plantid
	FROM dim_plant b, tmp_fact_materialworkflow_002 a
	WHERE a.plantcode = b.plantcode;

	MERGE INTO fact_materialworkflow t
	USING (SELECT a.fact_materialworkflowid, IFNULL(b.dim_plantidshipto, 1) dim_plantidshipto
		   FROM tmp_fact_materialworkflow_001 a 
				LEFT JOIN tmp_fact_materialworkflow_002 b
				ON a.CustomerNumber = b.CustomerNumber and a.PartNumber = b.PartNumber
		   ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_plantidshipto = s.dim_plantidshipto;	

	DROP TABLE IF EXISTS tmp_fact_materialworkflow_001;

	CREATE TABLE tmp_fact_materialworkflow_001
	AS
	select f.fact_materialworkflowid, d.customernumber
	from fact_materialworkflow f 
			inner join dim_customermastersales d on f.dim_customermastersalesid = d.dim_customermastersalesid
	WHERE dim_plantidshipto = 1;
			
	DROP TABLE IF EXISTS tmp_fact_materialworkflow_002;

	CREATE TABLE tmp_fact_materialworkflow_002
	AS			
	select Z1SD_SR_SHIPTO_KUNAG customernumber, Z1SD_SR_SHIPTO_WERKS plantcode, cast(1 as bigint) dim_plantidshipto
	from Z1SD_SR_SHIPTO
	where Z1SD_SR_SHIPTO_MATNR is null;

	UPDATE tmp_fact_materialworkflow_002 a
	SET a.dim_plantidshipto = b.dim_plantid
	FROM dim_plant b, tmp_fact_materialworkflow_002 a
	WHERE a.plantcode = b.plantcode;

	MERGE INTO fact_materialworkflow t
	USING (SELECT a.fact_materialworkflowid, MAX(IFNULL(b.dim_plantidshipto, 1)) dim_plantidshipto
		   FROM tmp_fact_materialworkflow_001 a 
				LEFT JOIN tmp_fact_materialworkflow_002 b
				ON a.CustomerNumber = b.CustomerNumber 
		   GROUP BY a.fact_materialworkflowid	
		   ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_plantidshipto = s.dim_plantidshipto;	

	DROP TABLE IF EXISTS tmp_fact_materialworkflow_001;
	DROP TABLE IF EXISTS tmp_fact_materialworkflow_002;
	
	DROP TABLE IF EXISTS tmp_upd_partshipto;
	
	CREATE TABLE tmp_upd_partshipto
	AS
	SELECT a.fact_materialworkflowid, b.plantcode plant, c.partnumber
	FROM fact_materialworkflow a 
					INNER JOIN dim_plant b
					ON a.dim_plantidshipto = b.dim_plantid
			   		INNER JOIN dim_part c
			   		ON a.dim_partid = c.dim_partid
	WHERE dim_plantidshipto<>1;
		
	
	MERGE INTO fact_materialworkflow t
	USING (SELECT a.fact_materialworkflowid, b.dim_partid dim_partidshipto
		   FROM tmp_upd_partshipto a 
		   		INNER JOIN dim_part b
		   		ON a.partnumber = b.partnumber
		  		AND a.plant = b.plant
		   ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_partidshipto = s.dim_partidshipto;
	
	DROP TABLE IF EXISTS tmp_upd_partshipto;
	
	merge into fact_materialworkflow t
	using (select f.fact_materialworkflowid, dc.dim_customerid 
		   from fact_materialworkflow f
				inner join dim_customer dc
				on dc.customernumber = f.dd_ml) s
	on t.fact_materialworkflowid = s.fact_materialworkflowid
	when matched then update set t.dim_customerid = s.dim_customerid;


	/* This column comes in the form of 05.12.2015 so we update it to 2015-12-05 */
	update Z1CA_SCM_TRSHEET
	set Z1CA_SCM_TRSHEET_EXPDATEARTAPPR = concat(right(Z1CA_SCM_TRSHEET_EXPDATEARTAPPR,4), '-', substr(Z1CA_SCM_TRSHEET_EXPDATEARTAPPR,4,2), '-', left(Z1CA_SCM_TRSHEET_EXPDATEARTAPPR,2))
	where Z1CA_SCM_TRSHEET_EXPDATEARTAPPR is not null and Z1CA_SCM_TRSHEET_EXPDATEARTAPPR like '%.%';	

	/* This column comes in the form of 05.12.2015 so we update it to 2015-12-05 */
		 
	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidblockremovaldate
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set') 					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_BLKREMDAT00 = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_BLKREMDAT00 end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidblockremovaldate = s.dim_dateidblockremovaldate;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidcreatedon
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set') 
					and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_ERSDA = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_ERSDA end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidcreatedon = s.dim_dateidcreatedon;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidfirstdeliv
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set') 
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_LAUNCH_DATE = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_LAUNCH_DATE end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidfirstdeliv = s.dim_dateidfirstdeliv;


	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidpifstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_PIF_STARTDATE = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_PIF_STARTDATE end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidpifstart = s.dim_dateidpifstart;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidmainprocessstatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEWI = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEWI end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidmainprocessstatusstart = s.dim_dateidmainprocessstatusstart;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidmainprocessstatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set') 
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEWI = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEWI end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_COMPLDATEWI is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidmainprocessstatuscompleted = s.dim_dateidmainprocessstatuscompleted;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidscmintrostart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATESCM = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATESCM end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_STARTDATESCM is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidscmintrostart = s.dim_dateidscmintrostart;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidscmintrocompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
					and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant					
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATESCM = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATESCM end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_COMPLDATESCM is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidscmintrocompleted = s.dim_dateidscmintrocompleted;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidipsstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')  
					and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEIPS = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEIPS end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  WHERE b.Z1CA_SCM_TRSHEET_STARTDATEIPS is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidipsstart = s.dim_dateidipsstart;  

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidipscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEIPS = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEIPS end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_COMPLDATEIPS is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidipscompleted = s.dim_dateidipscompleted;    

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidimositestart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEIMO = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEIMO end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidimositestart = s.dim_dateidimositestart;    

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidimositecompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEIMO = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEIMO end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_COMPLDATEIMO is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidimositecompleted = s.dim_dateidimositecompleted;  

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidpurcasingstatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEPUR = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEPUR end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidpurcasingstatusstart = s.dim_dateidpurcasingstatusstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidpurcasingstatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
					and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant					
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEPUR = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEPUR end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidpurcasingstatuscompleted = s.dim_dateidpurcasingstatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidplanningstatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEPLA = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEPLA end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidplanningstatusstart = s.dim_dateidplanningstatusstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidplanningstatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEPLA = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEPLA end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidplanningstatuscompleted = s.dim_dateidplanningstatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidwarehousestatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEWAR = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEWAR end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidwarehousestatusstart = s.dim_dateidwarehousestatusstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidwarehousestatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEWAR = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEWAR end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidwarehousestatuscompleted = s.dim_dateidwarehousestatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidwarehhazmatstatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant  
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEWA2 = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEWA2 end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidwarehhazmatstatusstart = s.dim_dateidwarehhazmatstatusstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidwarehhazmatstatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant  
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEWA2 = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEWA2 end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidwarehhazmatstatuscompleted = s.dim_dateidwarehhazmatstatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqmstatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant 
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEQM = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEQM end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqmstatusstart = s.dim_dateidqmstatusstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqmstatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEQM = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEQM end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqmstatuscompleted = s.dim_dateidqmstatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidficostatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set') 
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEFIC = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEFIC end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidficostatusstart = s.dim_dateidficostatusstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidficostatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set') 
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEFIC = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEFIC end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_COMPLDATEFIC is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidficostatuscompleted = s.dim_dateidficostatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidsalesstatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set') 
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATESAL = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATESAL end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidsalesstatusstart = s.dim_dateidsalesstatusstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidsalesstatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set') 
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATESAL = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATESAL end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidsalesstatuscompleted = s.dim_dateidsalesstatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidproductionstatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEPRO = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEPRO end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidproductionstatusstart = s.dim_dateidproductionstatusstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidproductionstatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEPRO = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEPRO end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidproductionstatuscompleted = s.dim_dateidproductionstatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqcstatusstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEQC = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEQC end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqcstatusstart = s.dim_dateidqcstatusstart;   

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqcstatuscompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEQC = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEQC end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqcstatuscompleted = s.dim_dateidqcstatuscompleted; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqregnewstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATENEW = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATENEW end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_STARTDATENEW is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqregnewstart = s.dim_dateidqregnewstart; 

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqregnewcompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATENEW = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATENEW end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_COMPLDATENEW is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqregnewcompleted = s.dim_dateidqregnewcompleted;  

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqregoccstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATEOCC = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATEOCC end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqregoccstart = s.dim_dateidqregoccstart;    

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqregocccompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATEOCC = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATEOCC end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqregocccompleted = s.dim_dateidqregocccompleted;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqregserstart
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_STARTDATESER = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_STARTDATESER end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqregserstart = s.dim_dateidqregserstart;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidqregsercompleted
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant 
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COMPLDATESER = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COMPLDATESER end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidqregsercompleted = s.dim_dateidqregsercompleted;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) 
			dim_dateidexpectedartworkapprovaldate
			from fact_materialworkflow a
			left join dim_part dp on a.dim_partid = dp.dim_partid
			left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
			and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
			and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
			and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
			and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
			and dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')	
			and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
			and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
			left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
			left join dim_date d on 
				convert(date, case when b.Z1CA_SCM_TRSHEET_EXPDATEARTAPPR = '1900-01-01' then null 
							else
								case when Z1CA_SCM_TRSHEET_EXPDATEARTAPPR = '0000-00-00' then '0001-01-01' 
								else Z1CA_SCM_TRSHEET_EXPDATEARTAPPR 
							end 
						end) = d.datevalue 
			and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
			where b.Z1CA_SCM_TRSHEET_EXPDATEARTAPPR is not null
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidexpectedartworkapprovaldate = s.dim_dateidexpectedartworkapprovaldate;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidcostprice
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
					 and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_COST_PRICE = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_COST_PRICE end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidcostprice = s.dim_dateidcostprice;

   MERGE INTO fact_materialworkflow t
	USING (
	select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidtransferprice1
	from fact_materialworkflow a
	left join dim_part dp on a.dim_partid = dp.dim_partid
	left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
	and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
	and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
	and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
	and dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')	
	and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
	and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
	left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
	left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE1 = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE1 end) = d.datevalue 
	and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
	) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidtransferprice1 = s.dim_dateidtransferprice1;
	
	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, max(ifnull(d.dim_dateid,1)) dim_dateidtransferprice2
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2 = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2 end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		  where b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2 is not null
		  group by fact_materialworkflowid
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidtransferprice2 = s.dim_dateidtransferprice2;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidtransferprice2
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2 = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_TRANSFER_PRICE2 end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidtransferprice2 = s.dim_dateidtransferprice2;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidmaterialfinalrelease
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_MATERIAL_FINAL_RELEASE = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_MATERIAL_FINAL_RELEASE end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidmaterialfinalrelease = s.dim_dateidmaterialfinalrelease;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateiddocdatestorder
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_DOCDATE_1STORD = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_DOCDATE_1STORD end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateiddocdatestorder = s.dim_dateiddocdatestorder;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidfirstgoodsissued
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_FIRST_GOODS_ISSUED = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_FIRST_GOODS_ISSUED end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidfirstgoodsissued = s.dim_dateidfirstgoodsissued;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidfirstgoodsreceipt
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_FIRST_GOODS_RECEIPT = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_FIRST_GOODS_RECEIPT end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidfirstgoodsreceipt = s.dim_dateidfirstgoodsreceipt;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidpirvalidto
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_PIR_VALID_TO = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_PIR_VALID_TO end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidpirvalidto = s.dim_dateidpirvalidto;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidpirvalidfrom
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_PIR_VALID_FROM = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_PIR_VALID_FROM end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidpirvalidfrom = s.dim_dateidpirvalidfrom;

	MERGE INTO fact_materialworkflow t
	USING (
		  select distinct fact_materialworkflowid, ifnull(d.dim_dateid,1) dim_dateidslcreated
		  from  fact_materialworkflow a
		  left join dim_part dp on a.dim_partid = dp.dim_partid
		  left join Z1CA_SCM_TRSHEET b on a.dd_workflowid = ifnull(b.Z1CA_SCM_TRSHEET_WI_ID, 'Not Set') 
					and a.dd_projectcode = ifnull(b.Z1CA_SCM_TRSHEET_PROJECT, 'Not Set') 
					and a.dd_ml = ifnull(b.Z1CA_SCM_TRSHEET_ML, 'Not Set')
					and dp.partnumber = IFNULL(b.Z1CA_SCM_TRSHEET_MATNR, 'Not Set')
					and dp.productfamily_merck = ifnull(b.z1ca_scm_trsheet_gpf,'Not Set')
                    and  dp.parttype = ifnull(b.Z1CA_SCM_TRSHEET_MTART, 'Not Set')					
					and ifnull(b.Z1CA_SCM_TRSHEET_PLANT,'Not Set') = dp.plant 
		  left join dim_plant pl on ifnull(dp.plant,'Not Set') = pl.plantcode
		  left join dim_date d on convert(date, case when b.Z1CA_SCM_TRSHEET_SL_CREATE_DATE = '1900-01-01' then null else b.Z1CA_SCM_TRSHEET_SL_CREATE_DATE end) = d.datevalue 
		  and d.companycode=pl.companycode and d.plantcode_factory = pl.plantcode
		 ) s
	ON t.fact_materialworkflowid = s.fact_materialworkflowid
	WHEN MATCHED THEN UPDATE SET t.dim_dateidslcreated = s.dim_dateidslcreated;
	
	
	update fact_materialworkflow
	set dd_allmilestones = 'N';
	
	drop table if exists tmp_milestones;

	create table tmp_milestones
	as
	select fact_materialworkflowid, 
		   dim_partid,
		   dim_dateidipsstart,
		   dim_dateidvalidto,
		   dim_dateidtransferprice2,
		   dim_dateidmaterialfinalrelease,
		   dim_dateidslcreated,
		   dim_dateidmainprocessstatusstart,
		   dim_customermastersalesid,
		   dd_scmintrostatus,
		   dd_bomstatus,
		   dd_workflowscenario,
		   dd_ppirelease,
		   dd_ipsstatus,
		   dd_mainprocessstatus,
		   dd_ipdescription,
		   dd_mldescription,
		   convert (varchar(1), 'N') milestone1, 
		   convert (varchar(1), 'N') milestone2, 
		   convert (varchar(1), 'N') milestone3, 
		   convert (varchar(1), 'N') milestone4, 
		   convert (varchar(1), 'N') milestone5, 
		   convert (varchar(1), 'N') milestone6, 
		   convert (varchar(1), 'N') milestone7
	from fact_materialworkflow;

	update tmp_milestones
	set milestone1 = case when (dd_scmintrostatus = 'COMPLETED' or dd_scmintrostatus = 'Resubmission UD') then 'Y' else 'N' end
	where milestone1 <> case when (dd_scmintrostatus = 'COMPLETED' or dd_scmintrostatus = 'Resubmission UD')  then 'Y' else 'N' end;



	update tmp_milestones t
	set t.milestone2 = case when t.dd_workflowscenario = 'Phase out' 
							 then 'Y'
							 else case when d_dp.MaterialStatus='Not Set' 
									   then case when t.dd_bomstatus = '30' 
												 then 'Y'
												 else case when t.dd_workflowscenario='Existing FPP item'
														   then 'Y'
														   else 'N'
													  end
											end
										else 'N'
								   end	
					   end
   	from dim_part d_dp, tmp_milestones t
	where t.dim_partid = d_dp.dim_partid
	and t.milestone2 <> case when t.dd_workflowscenario = 'Phase out' 
							 then 'Y'
							 else case when d_dp.MaterialStatus='Not Set' 
									   then case when t.dd_bomstatus = '30' 
												 then 'Y'
												 else case when t.dd_workflowscenario='Existing FPP item'
														   then 'Y'
														   else 'N'
													  end
											end
										else 'N'
								  end	
						end;
										
	update tmp_milestones t
	set t.milestone3 = case when t.dd_workflowscenario = 'Phase out' 
									  then 'Y'
								 when t.dd_ipsstatus = 'NOT SELECTED' or t.dd_ipsstatus = 'Not Set' or t.dd_ipsstatus = 'No Workflow found' 
									  then 'Y'
								 when t.dd_ppirelease ='Yes' and t.dd_workflowscenario = 'FPP Introduction' 
									  then 'Y'
								 when t.dd_ppirelease <>'Yes' and t.dd_workflowscenario = 'FPP Introduction' 
									  then 'N'
								 when t.dd_workflowscenario ='Existing FPP item' and t.dd_ipsstatus = 'COMPLETED' 
									  then 'Y'
								 when t.dd_workflowscenario ='Existing FPP item' and t.dd_ipsstatus <> 'COMPLETED' 
									  then 'N'
								else 'Y'
						   	   End
	where t.milestone3 <> case when t.dd_workflowscenario = 'Phase out' 
									  then 'Y'
								 when t.dd_ipsstatus = 'NOT SELECTED' or t.dd_ipsstatus = 'Not Set' or t.dd_ipsstatus = 'No Workflow found' 
									  then 'Y'
								 when t.dd_ppirelease ='Yes' and t.dd_workflowscenario = 'FPP Introduction' 
									  then 'Y'
								 when t.dd_ppirelease <>'Yes' and t.dd_workflowscenario = 'FPP Introduction' 
									  then 'N'
								 when t.dd_workflowscenario ='Existing FPP item' and t.dd_ipsstatus = 'COMPLETED' 
									  then 'Y'
								 when t.dd_workflowscenario ='Existing FPP item' and t.dd_ipsstatus <> 'COMPLETED' 
									  then 'N'
								else 'Y'
							  	End;  
					   
	update tmp_milestones t	
	set t.milestone4 = case when t.dd_workflowscenario = 'Phase out'
							 then 'Y'
							 else case when d_mvt.DateValue > current_date
										then 'Y'
										else case when t.dd_mainprocessstatus='COMPLETED' 
												  then 'Y'
												  else 'N'
											 end
								  end
					   end
	from dim_date d_mvt, tmp_milestones t
	where t.dim_dateidvalidto = d_mvt.dim_dateid
	and t.milestone4 <> case when t.dd_workflowscenario = 'Phase out'
							 then 'Y'
							 else case when d_mvt.DateValue > current_date
										then 'Y'
										else case when t.dd_mainprocessstatus='COMPLETED' 
												  then 'Y'
												  else 'N'
											 end
								  end
						end;			    

	update tmp_milestones t
	set t.milestone5 = case when t.dd_workflowscenario = 'Phase out'
							 then 'Y'
							 else case when d_transtwo.DateValue <> '0001-01-01'
										then 'Y'
										else case when (d_cms.DistributionChannel in ('Not Set','50')
										 or left(t.dd_ipdescription,2)='US')	
												  then 'Y'
												  else 'N'
											 end
								   end
					   end	
   	from dim_date d_transtwo, dim_customermastersales d_cms, tmp_milestones t	   
	where d_cms.dim_customermastersalesid = t.dim_customermastersalesid and d_transtwo.dim_dateid = t.dim_dateidtransferprice2
	and t.milestone5 <> case when t.dd_workflowscenario = 'Phase out'
							 then 'Y'
							 else case when d_transtwo.DateValue <> '0001-01-01'
										then 'Y'
										else case when (d_cms.DistributionChannel in ('Not Set','50')
										 or left(t.dd_ipdescription,2)='US')
										                  then 'Y'
												  else 'N'
											 end
								   end
						end;

	update tmp_milestones t
	set t.milestone6 = case when d_mfr.DateValue = '0001-01-01'
							then 'N'
							else 'Y'
					   end
   	from dim_date d_mfr, tmp_milestones t
	where t.dim_dateidmaterialfinalrelease = d_mfr.dim_dateid
	and t.milestone6 <> case when d_mfr.DateValue = '0001-01-01'
							then 'N'
							else 'Y'
						end;			   

	update tmp_milestones t
	set t.milestone7 = case when t.dd_workflowscenario = 'Phase out'
							 then 'Y'
							 else case when d_mpss.DateValue < '2016-02-01'
									   then 'Y'
					         else case when left(dd_mldescription, 11) = 'ML customer' then 'Y'				   
									   else case when d_sld.DateValue <> '0001-01-01' 	
													then 'Y'
													else case when d_cms.DistributionChannel in ('Not Set','50') 
															  then 'Y'
															  else 'N'
														 end
											   end
								  end	
						end	
					end	
	from dim_date d_mpss, dim_date d_sld, dim_customermastersales d_cms, tmp_milestones t			   
	where d_cms.dim_customermastersalesid = t.dim_customermastersalesid and d_sld.dim_dateid = t.dim_dateidslcreated
	and t.dim_dateidmainprocessstatusstart = d_mpss.dim_dateid
	and t.milestone7 <> case when t.dd_workflowscenario = 'Phase out'
							 then 'Y'
							 else case when d_mpss.DateValue < '2016-02-01'
									   then 'Y'
					         else case when left(dd_mldescription, 11) = 'ML customer' then 'Y'				   
									   else case when d_sld.DateValue <> '0001-01-01' 	
													then 'Y'
													else case when d_cms.DistributionChannel in ('Not Set','50') 
															  then 'Y'
															  else 'N'
														 end
											   end
								  end	
						end
					end;		
					   
	merge into fact_materialworkflow t
	using (				   
			select fact_materialworkflowid,
				   case when milestone1 = 'Y' and milestone2 = 'Y' and milestone3 = 'Y' and milestone4 = 'Y' 
							 and milestone5 = 'Y' and milestone6 = 'Y' and milestone7 = 'Y'	
						then 'Y'
						else 'N'
				   end dd_allmilestones
			from tmp_milestones	
		   ) s
	on t.fact_materialworkflowid = s.fact_materialworkflowid
	when matched then update set t.dd_allmilestones = s.dd_allmilestones;	

	drop table if exists tmp_milestones;  	
	
	update fact_materialworkflow
	set ct_onhandqtyiru_shipto_ia=0,
		ct_onhandqtyiru_ia = 0;

	drop table if exists tmp_stocks;

	create table tmp_stocks
	as
	select dim_partid, sum(ct_onhandqtyiru) ct_onhandqtyiru
	from fact_inventoryaging
	group by dim_partid;

	update fact_materialworkflow f
	set f.ct_onhandqtyiru_ia = t.ct_onhandqtyiru
	from tmp_stocks t, fact_materialworkflow f
	where f.dim_partid = t.dim_partid;
		  
	update fact_materialworkflow f
	set f.ct_onhandqtyiru_shipto_ia = t.ct_onhandqtyiru
	from tmp_stocks t, fact_materialworkflow f
	where f.dim_partidshipto = t.dim_partid;
	
	drop table if exists tmp_stocks;

/* Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

UPDATE fact_materialworkflow am
SET am.std_exchangerate_dateid = dt.dim_dateid
	FROM fact_materialworkflow am inner join dim_plant p on am.dim_plantid = p.dim_plantid
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory = p.plantcode
WHERE dt.datevalue = current_date
AND am.std_exchangerate_dateid <> dt.dim_dateid;

/* END Added std_exchangerate_dateid by FPOPESCU on 22 March 2016 */

/*06 Jan 2017 Georgiana according to BI-5173 moving these updates after the insert statement*/
    /*update fact_materialworkflow a 
	set a.dim_partid = b.dim_partid
	from dim_part b,  fact_materialworkflow a 
	where a.dd_trsheet_matnr = b.partnumber
	and a.dd_trsheet_plant = b.plant
	and a.dim_partid <> b.dim_partid

	update fact_materialworkflow a 
	set a.dim_plantid = b.dim_plantid
	from dim_plant b, fact_materialworkflow a 
	where a.dd_trsheet_plant = b.plantcode
	and a.dim_plantid <> b.dim_plantid;
/*06 Jan 2017 End*/	
	 update fact_materialworkflow
     set dd_custmergroup_NL10_50_10 = 'Not Set'
     where dd_custmergroup_NL10_50_10 <> 'Not Set';

merge into fact_materialworkflow a
using (select distinct a.fact_materialworkflowid, c.customergroup
     from dim_customer b, dim_customermastersales c, fact_materialworkflow a
     where a.dim_customerid = b.dim_customerid
     and b.customernumber = c.customernumber 
     and c.salesorg = 'NL10'
     and c.distributionchannel = 50
     and c.divisioncode = 10
and IFNULL(a.dd_ipnumber, 'Not Set') = c.ItemProposal) t
on t.fact_materialworkflowid=a.fact_materialworkflowid
when matched then update set  a.dd_custmergroup_NL10_50_10 = t.customergroup
where a.dd_custmergroup_NL10_50_10 <> t.customergroup;

	 
     drop table if exists tmp_kotg992_latest;
     create table tmp_kotg992_latest as
     select KOTG992_KDGRP,
        KOTG992_MATNR,
        max(kotg992_datab) as kotg992_datab,
        max(kotg992_datbi) as kotg992_datbi from KOTG992
     group by KOTG992_KDGRP,
              KOTG992_MATNR;
          
     update fact_materialworkflow a 

     set a.dim_dateidvalidfrom = d.dim_dateid
     from tmp_kotg992_latest b,dim_date d, fact_materialworkflow a 
     where dd_custmergroup_NL10_50_10 <> 'Not Set'
     and a.dd_trsheet_matnr = b.KOTG992_MATNR
     and a.dd_custmergroup_NL10_50_10 = b.KOTG992_KDGRP
     and b.kotg992_datab = d.datevalue
     and d.CompanyCode = 'Not Set'
     and d.plantcode_factory = 'Not Set'
     and a.dim_dateidvalidfrom <> d.dim_dateid;
 
     update fact_materialworkflow a 

     set a.dim_dateidvalidto = d.dim_dateid
     from tmp_kotg992_latest b,dim_date d, fact_materialworkflow a 
     where dd_custmergroup_NL10_50_10 <> 'Not Set'
      and a.dd_trsheet_matnr = b.KOTG992_MATNR
      and a.dd_custmergroup_NL10_50_10 = b.KOTG992_KDGRP
      and b.kotg992_datbi = d.datevalue
      and d.CompanyCode = 'Not Set'
      and d.plantcode_factory = 'Not Set'
      and a.dim_dateidvalidto <> d.dim_dateid;
 
      drop table if exists tmp_kotg992_latest;
		 