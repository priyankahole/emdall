
delete from fact_atlaspharmlogiforecast_merck
where dd_version IN ('A00','000');

Drop table if exists max_holder_atlasph;
Create table max_holder_atlasph
as
Select ifnull(max(fact_atlaspharmlogiforecast_merckid),0) as maxid
from fact_atlaspharmlogiforecast_merck;

insert into fact_atlaspharmlogiforecast_merck(
fact_atlaspharmlogiforecast_merckid,
dim_plantid,
dim_partid,
dim_companyid,
dim_dateidforecast,
dd_version,
dim_unitofmeasureidbase,
ct_salesdelivered,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_salesbudget,
dd_source,
dim_dateidsnapshot
)
select
max_holder_atlasph.maxid + row_number() over(order by '') fact_atlaspharmlogiforecast_merckid,
pl.dim_plantid,
pt.dim_partid,
dc.dim_companyid,
dt.dim_dateid dim_dateidforecast,
VRSIO	dd_version,
um.dim_unitofmeasureid dim_unitofmeasureidbase,
Z1PP_SALD  ct_salesdelivered,
Z1PP_SALR  ct_salesrequested,
Z1PP_FCST  ct_salesforecast,
Z1PP_PROM  ct_promotion,
Z1PP_ADJL  ct_adjforlogistics,
Z1PP_TADJ  ct_totaladj,
Z1PP_ADJSP  ct_adjsalesplan,
Z1PP_SALRM  ct_salesreqmod,
Z1PP_SALRA  ct_adjsalesreq,
Z1PP_BUDGT  ct_salesbudget,
'S991' dd_source,
crt.dim_dateid dim_Dateidsnapshot
from S991 s, dim_plant pl, dim_part pt, dim_company dc, dim_date crt,
dim_date dt, dim_unitofmeasure um, max_holder_atlasph
where pl.plantcode = WENUX and pt.plant = pl.plantcode
and pt.partnumber = PMNUX and  dt.datevalue = to_date(case when spmon=0 then '00010101' else concat(spmon,'01') end,'YYYYMMDD')
and dc.companycode = dt.companycode and pl.companycode = dc.companycode and pl.plantcode=dt.plantcode_factory
and dc.companycode = crt.companycode and crt.datevalue = current_date and pl.plantcode=crt.plantcode_factory
and um.uom = BASME;

Drop table if exists max_holder_atlasph;
Create table max_holder_atlasph
as
Select ifnull(max(fact_atlaspharmlogiforecast_merckid),0) as maxid
from fact_atlaspharmlogiforecast_merck;

insert into fact_atlaspharmlogiforecast_merck(
fact_atlaspharmlogiforecast_merckid,
dim_plantid,
dim_partid,
dim_companyid,
dim_dateidforecast,
dd_version,
dim_unitofmeasureidbase,
ct_salesdelivered,
ct_salesrequested,
ct_salesforecast,
ct_promotion,
ct_adjforlogistics,
ct_totaladj,
ct_adjsalesplan,
ct_salesreqmod,
ct_adjsalesreq,
ct_salesbudget,
dd_source,
dim_dateidsnapshot
)
select
max_holder_atlasph.maxid + row_number() over(order by '') fact_atlaspharmlogiforecast_merckid,
pl.dim_plantid,
pt.dim_partid,
dc.dim_companyid,
dt.dim_dateid dim_dateidforecast,
VRSIO	dd_version,
um.dim_unitofmeasureid dim_unitofmeasureidbase,
Z1PP_SALD  ct_salesdelivered,
Z1PP_SALR  ct_salesrequested,
Z1PP_FCST  ct_salesforecast,
Z1PP_PROM  ct_promotion,
Z1PP_ADJL  ct_adjforlogistics,
Z1PP_TADJ  ct_totaladj,
Z1PP_ADJSP  ct_adjsalesplan,
Z1PP_SALRM  ct_salesreqmod,
Z1PP_SALRA  ct_adjsalesreq,
Z1PP_BUDGT  ct_salesbudget,
'S992' dd_source,
crt.dim_dateid dim_Dateidsnapshot
from S992 s, dim_plant pl, dim_part pt, dim_company dc, dim_date crt,
dim_date dt, dim_unitofmeasure um, max_holder_atlasph
where pl.plantcode = WENUX and pt.plant = pl.plantcode
and pt.partnumber = PMNUX and  dt.datevalue = to_date(case when spmon=0 then '00010101' else concat(spmon,'01') end,'YYYYMMDD')
and dc.companycode = dt.companycode and pl.companycode = dc.companycode and pl.plantcode=dt.plantcode_factory
and dc.companycode = crt.companycode and crt.datevalue = current_date and pl.plantcode=crt.plantcode_factory
and um.uom = BASME;