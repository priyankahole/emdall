DROP TABLE IF EXISTS mdtb_pi00;

CREATE TABLE mdtb_pi00 AS 
SELECT MDTB_DELNR, MDTB_DTNUM, IFNULL(MDTB_UMDAT, MDTB_DAT01) DATEvalue_upd
FROM mdtb 
WHERE IFNULL(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL;

DROP TABLE IF EXISTS plaf_pi00;

Create table plaf_pi00 as 
Select PLAF_UMREZ,
	PLAF_UMREN,
	PLAF_PLNUM,
	ifnull(PLAF_EMLIF, 'Not Set') PLAF_EMLIF, 
	ifnull(PLAF_FLIEF, 'Not Set') PLAF_FLIEF,
	ifnull(PLAF_KZVBR, 'Not Set') PLAF_KZVBR
from plaf
where ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL;

MERGE INTO fact_excessandshortage fact
USING(SELECT m.fact_excessandshortageid, IFNULL(z.StandardPrice,0) StandardPrice
	  FROM fact_excessandshortage m 
	   INNER JOIN plaf_pi00 po ON m.dd_DocumentNo = po.PLAF_PLNUM
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR 
	   INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
	   INNER JOIN dim_part p ON p.Dim_Partid = m.Dim_Partid
	   INNER JOIN dim_plant pl ON pl.PlantCode = p.Plant
	   INNER JOIN Dim_Date dd ON to_date(dd.DateValue) = to_date(t.datevalue_upd) AND dd.Dim_Dateid <> 1
			 AND dd.CompanyCode = pl.CompanyCode 
			 AND pl.plantcode = dd.plantcode_factory
       LEFT JOIN tmp_getStdPrice z
			            ON  z.pCompanyCode = pl.CompanyCode
						AND z.pPlant = pl.PlantCode
						AND z.pMaterialNo =  k.MDKP_MATNR
						AND z.pFiYear = dd.CalendarYear
						AND z.pPeriod =  dd.FinancialMonthNumber
						AND z.vUMREZ = PLAF_UMREZ
						AND z.vUMREN =   PLAF_UMREN
						AND z.fact_script_name = 'fact_excessandshortage'
						AND z.PONumber IS NULL
                        AND m.Dim_ActionStateid = 2
			           AND z.pUnitPrice =  0) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.StandardPrice;
	

drop table if exists mdtb_pi00;
drop table if exists plaf_pi00;

/* Do not split it single column updates */
MERGE INTO fact_excessandshortage fact
USING (SELECT m.fact_excessandshortageid,
			  p.Dim_ItemCategoryid,
			  p.Dim_Vendorid,
			  p.Dim_DocumentTypeid,
			  ifnull((p.amt_DeliveryTotal / (case when p.ct_BaseUOMQty = 0 
			                                      then null 
												  else p.ct_BaseUOMQty 
										     end ))
				     ,0) as amt_UnitPrice,
			  ifnull((p.amt_DeliveryTotal / (case when p.ct_BaseUOMQty = 0 
			                                      then null 
												  else p.ct_BaseUOMQty 
										     end))
					 ,0) * p.amt_ExchangeRate_GBL as amt_UnitPrice_GBL,
			  (ct_DeliveryQty - ct_ReceivedQty) as ct_QtyOpenOrder,
			   p.Dim_ConsumptionTypeid 
	   FROM  fact_excessandshortage m
	         INNER JOIN fact_purchase p ON m.dd_DocumentNo = p.dd_DocumentNo
										AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
										AND m.dd_ScheduleNo = p.dd_ScheduleNo
			 INNER JOIN dim_vendor v ON v.Dim_Vendorid = p.Dim_Vendorid
             INNER JOIN dim_date dt ON p.Dim_DateidDelivery = dt.Dim_Dateid
		WHERE  m.Dim_ActionStateid = 2
               AND dt.Dim_Dateid <> 1) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_ItemCategoryid = src.Dim_ItemCategoryid,
	fact.Dim_Vendorid = src.Dim_Vendorid,
	fact.Dim_DocumentTypeid = src.Dim_DocumentTypeid,
	fact.amt_UnitPrice = src.amt_UnitPrice,
	fact.amt_UnitPrice_GBL = src.amt_UnitPrice_GBL,
	fact.ct_QtyOpenOrder = src.ct_QtyOpenOrder,
	fact.dim_consumptiontypeid = src.Dim_ConsumptionTypeid;	



UPDATE fact_excessandshortage m
SET m.Dim_SalesDocumentTypeid = p.Dim_SalesDocumentTypeid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_SalesDocumentTypeid <> p.Dim_SalesDocumentTypeid;



UPDATE fact_excessandshortage m
SET      m.Dim_CustomerGroup1id = p.Dim_CustomerGroup1id
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_CustomerGroup1id <> p.Dim_CustomerGroup1id;



    UPDATE fact_excessandshortage m
SET    m.Dim_CustomerID = p.Dim_CustomerID
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p,  fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_CustomerID  <> p.Dim_CustomerID;



    UPDATE fact_excessandshortage m
SET    m.Dim_SalesOrderRejectReasonid = p.Dim_SalesOrderRejectReasonid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_SalesOrderRejectReasonid  <>  p.Dim_SalesOrderRejectReasonid;



    UPDATE fact_excessandshortage m
SET    m.Dim_SalesOrderItemStatusid = p.Dim_SalesOrderItemStatusid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.Dim_SalesOrderItemStatusid <> p.Dim_SalesOrderItemStatusid;



    UPDATE fact_excessandshortage m
SET    m.Dim_SalesOrderHeaderStatusid = p.Dim_SalesOrderHeaderStatusid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_SalesOrderHeaderStatusid <> p.Dim_SalesOrderHeaderStatusid;



    UPDATE fact_excessandshortage m
SET    m.Dim_DocumentCategoryid = p.Dim_DocumentCategoryid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.Dim_DocumentCategoryid <> p.Dim_DocumentCategoryid;



    UPDATE fact_excessandshortage m
SET    m.amt_UnitPrice = ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end)
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND  m.amt_UnitPrice  <> ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end);



    UPDATE fact_excessandshortage m
SET    m.amt_UnitPrice_GBL = ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end) * p.amt_ExchangeRate_GBL
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND m.amt_UnitPrice_GBL <> ifnull(p.amt_UnitPrice, 0)/(case when p.ct_PriceUnit = 0 then null else p.ct_PriceUnit end) * p.amt_ExchangeRate_GBL;



    UPDATE fact_excessandshortage m
SET    ct_QtyOpenOrder =
          CASE
             WHEN (ct_ScheduleQtySalesUnit - ct_DeliveredQty) < 0 THEN 0
             ELSE (ct_ScheduleQtySalesUnit - ct_DeliveredQty)
          END
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder p, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_SalesDocNo
       AND m.dd_DocumentItemNo = p.dd_SalesItemNo
       AND m.dd_ScheduleNo = p.dd_ScheduleNo
	AND ct_QtyOpenOrder <> CASE
             WHEN (ct_ScheduleQtySalesUnit - ct_DeliveredQty) < 0 THEN 0
             ELSE (ct_ScheduleQtySalesUnit - ct_DeliveredQty)
          END ;



UPDATE fact_excessandshortage m
SET m.amt_UnitPrice = ifnull((p.amt_estimatedTotalCost/(case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0)
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  fact_productionorder p, fact_excessandshortage m
WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_ordernumber
       AND m.dd_DocumentItemNo = p.dd_orderitemno
	AND m.amt_UnitPrice  <> ifnull((p.amt_estimatedTotalCost/(case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0);
   

UPDATE fact_excessandshortage m
SET m.amt_UnitPrice_GBL = ifnull((p.amt_estimatedTotalCost/(Case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0) * p.amt_ExchangeRate_GBL
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  fact_productionorder p, fact_excessandshortage m
WHERE     m.Dim_ActionStateid = 2
       AND m.dd_DocumentNo = p.dd_ordernumber
       AND m.dd_DocumentItemNo = p.dd_orderitemno
	AND m.amt_UnitPrice_GBL <>  ifnull((p.amt_estimatedTotalCost/(Case when p.ct_TotalOrderQty = 0 then null else p.ct_TotalOrderQty end)), 0) * p.amt_ExchangeRate_GBL;


drop table if exists mdtb_pi00;

create table mdtb_pi00 as 
Select MDTB_DELNR,MDTB_DELPS,MDTB_DTNUM from mdtb 
where ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL and MDTB_DAT02 IS NOT NULL;

MERGE INTO fact_excessandshortage fact
USING (SELECT distinct ic.Dim_ItemCategoryid, m.fact_excessandshortageid 
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
	    WHERE m.Dim_ActionStateid = 2 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_ItemCategoryid = src.Dim_ItemCategoryid
WHERE fact.Dim_ItemCategoryid <> src.Dim_ItemCategoryid;
      

UPDATE fact_excessandshortage m
   SET        m.Dim_Vendorid = v.Dim_Vendorid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.EBAN_BANFN
       AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
       AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND m.Dim_Vendorid <> v.Dim_Vendorid;



 MERGE INTO fact_excessandshortage fact
USING (select distinct m.fact_excessandshortageid,fv.Dim_Vendorid From eban pr,
 mdkp k,
 mdtb_pi00 t,
 dim_vendor v,
 dim_itemcategory ic,
 dim_documenttype dt,
 dim_consumptiontype ct,
 dim_part dp,
 dim_vendor fv, fact_excessandshortage m
 WHERE m.Dim_ActionStateid = 2
 AND k.MDKP_DTNUM = t.MDTB_DTNUM
 AND m.dd_DocumentNo = pr.EBAN_BANFN
 AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
 AND m.dd_DocumentNo = t.MDTB_DELNR
 AND m.dd_DocumentItemNo = t.MDTB_DELPS
 AND dp.Dim_Partid = m.Dim_Partid
 AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
 AND v.RowIsCurrent = 1
 AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
 AND fv.RowIsCurrent = 1
 AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
 AND ic.RowIsCurrent = 1
 AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
 AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
 AND dt.RowIsCurrent = 1
 AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
 AND ct.RowIsCurrent = 1
 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
 AND m.Dim_FixedVendorid <> fv.Dim_Vendorid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_FixedVendorid = src.Dim_Vendorid
WHERE fact.Dim_FixedVendorid <> src.Dim_Vendorid;



 MERGE INTO fact_excessandshortage fact
USING (select distinct m.fact_excessandshortageid,dt.Dim_DocumentTypeid From eban pr,
 mdkp k,
 mdtb_pi00 t,
 dim_vendor v,
 dim_itemcategory ic,
 dim_documenttype dt,
 dim_consumptiontype ct,
 dim_part dp,
 dim_vendor fv, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.EBAN_BANFN
       AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
       AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND  m.Dim_DocumentTypeid <>  dt.Dim_DocumentTypeid) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.Dim_DocumentTypeid = src.Dim_DocumentTypeid
WHERE fact.Dim_DocumentTypeid <> src.Dim_DocumentTypeid;



       UPDATE fact_excessandshortage m
   SET m.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.EBAN_BANFN
       AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
       AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND  m.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;



MERGE INTO fact_excessandshortage fact
USING (select distinct m.fact_excessandshortageid,ifnull(EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 then null else EBAN_PEINH end), 1), 0) as amt_UnitPrice
 From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.EBAN_BANFN
       AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
       AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
       AND dt.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice = src.amt_UnitPrice
WHERE fact.amt_UnitPrice <> src.amt_UnitPrice;

	
	
MERGE INTO fact_excessandshortage fact
USING (SELECT distinct m.fact_excessandshortageid,
	   ifnull(EBAN_PREIS / ifnull((case when EBAN_PEINH = 0 
										then null 
										else EBAN_PEINH 
										end
								   ), 1), 0) * IFNULL(ex.exchangeRate,1) as amt_UnitPrice_GBL
	   FROM   fact_excessandshortage m 
	   INNER JOIN eban pr ON m.dd_DocumentNo = pr.eban_BANFN
						  AND m.dd_DocumentItemNo = pr.eban_BNFPO
	   INNER JOIN mdtb_pi00 t ON m.dd_DocumentNo = t.MDTB_DELNR
							  AND m.dd_DocumentItemNo = t.MDTB_DELPS				  
       INNER JOIN mdkp k ON k.MDKP_DTNUM = t.MDTB_DTNUM
       INNER JOIN dim_vendor v ON v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
							   AND v.RowIsCurrent = 1
       INNER JOIN dim_itemcategory ic ON ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
									  AND ic.RowIsCurrent = 1
       INNER JOIN dim_documenttype dt ON dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
								      AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
								      AND dt.RowIsCurrent = 1
       INNER JOIN dim_consumptiontype ct ON ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
										 AND ct.RowIsCurrent = 1
       INNER JOIN dim_part dp ON dp.Dim_Partid = m.Dim_Partid
       INNER JOIN dim_vendor fv ON fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
								AND fv.RowIsCurrent = 1
	   LEFT JOIN (SELECT exchangeRate,pFromCurrency, pDate
				  FROM tmp_getExchangeRate1 t1 CROSS JOIN systemproperty sp
				  WHERE t1.pFromExchangeRate is null
						and t1.fact_script_name = 'bi_populate_excessandshortage_fact'
						and t1.pToCurrency = IFNULL(sp.property_value,'USD')
						and sp.property = 'customer.global.currency') ex  
			ON ex.pFromCurrency = pr.EBAN_WAERS and ex.pDate = pr.eban_BADAT
       WHERE m.Dim_ActionStateid = 2 
			 AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
								) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.amt_UnitPrice_GBL = src.amt_UnitPrice_GBL
WHERE fact.amt_UnitPrice_GBL <> src.amt_UnitPrice_GBL;


drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY)
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR AS
SELECT DISTINCT m.MATNR,
       m.BWKEY,
       m.LFGJA,
       m.LFMON,
       m.VPRSV,
       m.VERPR,
       m.STPRS,
       m.PEINH,
       m.SALK3,
       m.BWTAR,
       m.MBEW_LBKUM,
       ((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON,
       m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

UPDATE fact_excessandshortage m
   SET m.amt_UnitPrice = (b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end))
	,m.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From  tmp2_MBEW_NO_BWTAR b,
       dim_plant p,
       dim_part dp, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND dp.Dim_Partid = m.Dim_Partid
       AND b.MATNR = dp.PartNumber
       AND p.Dim_Plantid = m.Dim_Plantid
       AND b.BWKEY = p.ValuationArea
       AND b.BWTAR IS NULL
       AND b.MBEW_LBKUM > 0
       AND ifnull(m.amt_UnitPrice, 0) = 0
	AND m.amt_UnitPrice <> (b.SALK3 / (case when b.MBEW_LBKUM = 0 then null else b.MBEW_LBKUM end));

/* Update std_exchangerate_dateid,dim_requisitiondateid by FPOPESCU on 05 January 2016  */
/*	
UPDATE fact_excessandshortage m
   SET dim_requisition_dateid = dte.dim_dateid
	,m.dw_update_date = current_timestamp 
From   eban pr,
       mdkp k,
       mdtb_pi00 t,
       dim_vendor v,
	   dim_plant pl,
       dim_itemcategory ic,
       dim_documenttype dt,
       dim_consumptiontype ct,
       dim_part dp,
       dim_vendor fv,
	   dim_date dte, fact_excessandshortage m
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.EBAN_BANFN
       AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND dp.Dim_Partid = m.Dim_Partid
	   AND pl.dim_plantid = m.dim_plantid
       AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
       AND ic.RowIsCurrent = 1
       AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
       AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
	   AND dt.RowIsCurrent = 1
	   AND dte.datevalue = ifnull(pr.BADAT,'0001-01-01')
	   AND dte.companycode = pl.companycode
	   AND dte.plantcode_factory = pl.plantcode
       AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	AND  dim_requisition_dateid <> dte.dim_dateid
*/
MERGE INTO fact_excessandshortage fact
USING (select max(dte.dim_dateid) dim_requisition_dateid,m.fact_excessandshortageid
 From eban pr,
                        mdkp k,
                        mdtb_pi00 t,
                        dim_vendor v,
                dim_plant pl,
                        dim_itemcategory ic,
                        dim_documenttype dt,
                        dim_consumptiontype ct,
                        dim_part dp,
                        dim_vendor fv,
                dim_date dte,
                fact_excessandshortage m
WHERE m.Dim_ActionStateid = 2
AND k.MDKP_DTNUM = t.MDTB_DTNUM
AND m.dd_DocumentNo = pr.EBAN_BANFN
AND m.dd_DocumentItemNo = pr.EBAN_BNFPO
AND m.dd_DocumentNo = t.MDTB_DELNR
AND m.dd_DocumentItemNo = t.MDTB_DELPS
AND dp.Dim_Partid = m.Dim_Partid
AND pl.dim_plantid = m.dim_plantid
AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
AND v.RowIsCurrent = 1
AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
AND fv.RowIsCurrent = 1
AND ic.CategoryCode = ifnull(pr.EBAN_PSTYP, 'Not Set')
AND ic.RowIsCurrent = 1
AND dt.Type = ifnull(pr.EBAN_BSART, 'Not Set')
AND dt.Category = ifnull(pr.EBAN_BSTYP, 'Not Set')
AND dt.RowIsCurrent = 1
AND dte.datevalue = ifnull(pr.BADAT,'0001-01-01')
AND dte.companycode = pl.companycode
AND dte.plantcode_factory = pl.plantcode
AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
AND ct.RowIsCurrent = 1
AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
AND dim_requisition_dateid <> dte.dim_dateid
 group by fact_excessandshortageid
 ) src
ON fact.fact_excessandshortageid = src.fact_excessandshortageid
WHEN MATCHED THEN UPDATE SET fact.dim_requisition_dateid = src.dim_requisition_dateid
WHERE fact.dim_requisition_dateid <> src.dim_requisition_dateid;
	

	
UPDATE fact_excessandshortage f
SET f.std_exchangerate_dateid = f.dim_requisition_dateid
WHERE f.std_exchangerate_dateid <> f.dim_requisition_dateid;

/* END Update std_exchangerate_dateid,dim_requisitiondateid by FPOPESCU on 05 January 2016  */	

Drop table if exists pGlobalCurrency_po_41;
Drop table if exists mdtb_pi00;
drop table if exists tmp2_MBEW_NO_BWTAR;
drop table if exists tmp_MBEW_NO_BWTAR;
