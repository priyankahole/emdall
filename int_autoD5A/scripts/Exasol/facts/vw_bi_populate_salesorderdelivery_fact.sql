/* ################################################################################################################## */
/* */
/*   Script         : bi_populate_salesorderdelivery_fact */
/*   Author         : Ashu */
/*   Created On     : 18 Feb 2013 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_salesorderdelivery_fact from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*    29 Jul 2016                 Liviu I     1.25     Change LIPS joins to left BI-3605 */
/*	 10 Jun 2016	 Madalina H 1.24			 Shipment Doc No, Shipment Doc Item and Actual Goods Issue Date are Not Set for some items BI-3042 */
/*   18 Mar 2016     Georgiana 1.23              Add dd_deliverynumber: EKES-VBELN  BI-2348*/
/*   17 Mar 2016     Georgiana 1.22              Add dim_partsalesid, dim_partsalesid_vbap, amt_NetValueItem  and ct_actualinvoiceqty: BI-2357 */
/*   02 Mar 2015     L.Ionescu 1.21              Add combine for table fact_salesorder */
/*   17 Nov 2014     Suchithra 1.20		 Added Update Statement for dim_salesdocorderreasonid to fix issue in Basecamp	*/
/*   2  May 2014     George    1.17              Added dd_BusinessCustomerPONo */
/*   14 Feb 2014     George    1.16              Added Dim_CustomerGroup4id      */
/*   12 Feb 2014     George    1.15              Added Dim_ScheduleDeliveryBlockid                                   */
/*	 26 Sep 2013     Issam     1.14              Added fields dd_SDCreateTime dd_DeliveryTime, dd_PickingTime,
												 dd_GITime, dd_SDLineCreateTime											*/
/*   08 Sep 2013     Lokesh    1.10              Currency and exchange rate changes */
/*   13 Aug 2013     Issam     1.9               Added Sales District */
/*   29 Apr 2013     Hiten     1.2               Revised population logic */
/*   24 Feb 2013     Lokesh    1.1		 Add part 2 + while loop logic  */
/*   18 Feb 2013     Ashu      1.0               Existing code migrated to Vectorwise */
/* #################################################################################################################### */

/*Refresh the required tables from corresponding mysql db first ( for testing ) */
/*cd /home/fusionops/ispring/db/schema_migration/bin */
/*./refresh_vw_from_mysql_sameserver.sh_lk albea albea dim_billingdocumenttype dim_controllingarea dim_customer dim_date dim_distributionchannel dim_part dim_plant dim_producthierarchy dim_salesorderheaderstatus dim_salesorderitemstatus dim_storagelocation fact_salesorder fact_salesorderdelivery likp_lips systemproperty */

Drop table if exists flag_holder_722;
Drop table if exists cursor_table1_722;

/*
DROP TABLE IF EXISTS NUMBER_FOUNTAIN
CREATE TABLE NUMBER_FOUNTAIN
(
table_name      varchar(40) NOT NULL,
max_id          int     NOT NULL
)
*/

DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'fact_salesorderdelivery';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_salesorderdelivery',ifnull(max(fact_salesorderdeliveryid),0)
FROM fact_salesorderdelivery;
/* Remove hard-deleted delivery orders */
DELETE FROM fact_salesorderdelivery
WHERE exists (select 1 from CDPOS_LIKP a where a.CDPOS_OBJECTID = dd_SalesDlvrDocNo AND a.CDPOS_CHNGIND = 'D' AND a.CDPOS_TABNAME = 'LIKP');

Create table cursor_table1_722(
v_iid		  Integer,
v_DlvrDocNo       VARCHAR(50) null,
v_plantcode       VARCHAR(50) null,
v_DlvrItemNo      INTEGER null,
v_SalesDocNo      VARCHAR(50) null,
v_SalesItemNo     INTEGER null,
v_DeliveryQty     DECIMAL(18,4) null,
v_AGI_Date        DATE null,
v_PGI_Date        DATE null,
v_GIDate          DATE null,
v_DlvrCost        DECIMAL(18,4) null,
v_SchedQty        DECIMAL(18,4) null,
v_SchedNo         INTEGER null,
v_ControllingArea VARCHAR(7) null, /* VARCHAR(4) */
v_ProfitCenter    VARCHAR(10) null,
v_Billingdate     date null,
v_BillingType     VARCHAR(7) null,
v_DistChannel     VARCHAR(7) null,
v_DlvrRowNo	   INTEGER null,
v_DlvrRowNoMax	   INTEGER null,
v_DeliveryQtyCUMM  DECIMAL(18,4) null);

Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingDate,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select row_number() over(order by ''),
	LIKP_VBELN v_DlvrDocNo,
	LIPS_POSNR v_DlvrItemNo,
	LIPS_VGBEL v_SalesDocNo,
	LIPS_VGPOS v_SalesItemNo,
	LIPS_LFIMG v_DeliveryQty,
	ifnull(LIKP_WADAT_IST,LIKP_WADAT) v_AGI_Date,
	LIKP_WADAT v_PGI_Date,
	LIPS_WAVWR v_DlvrCost,
	LIPS_WERKS v_plantcode,
	LIPS_KOKRS v_ControllingArea,
	LIPS_PRCTR v_ProfitCenter,
	LIKP_FKDAT v_BillingDate,
	LIKP_FKARV v_BillingType,
	LIKP_VTWIV v_DistChannel,
	 ROW_NUMBER() OVER (PARTITION BY LIPS_VGBEL,LIPS_VGPOS ORDER BY LIKP_WADAT_IST,LIKP_WADAT,LIKP_VBELN,LIPS_POSNR) AS v_DlvrRowNo,
	 COUNT(1) OVER (PARTITION BY LIPS_VGBEL,LIPS_VGPOS) v_DlvrRowNoMax
from LIKP_LIPS
where exists (select 1 from fact_salesorder f1
              where f1.dd_SalesDocNo = LIPS_VGBEL and f1.dd_SalesItemNo = LIPS_VGPOS and f1.dd_ItemRelForDelv = 'X')
      and LIPS_LFIMG > 0
order by LIKP_VBELN, v_AGI_Date, LIPS_POSNR;

Create table flag_holder_722 as
         SELECT ifnull(property_value,'true') pDeltaChangesFlag
                   FROM systemproperty
                  WHERE property = 'process.delta.salesorderdelivery';

/*** Remove deleted lines ***/

/* don't delete inbound records missing from fact_salesorder if t- BI-3605 */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE exists (select 1 from LIKP_LIPS a where dd_SalesDlvrDocNo = LIKP_VBELN)
AND not exists ( SELECT 1 from LIKP_LIPS where dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrItemNo = LIPS_POSNR)
/* don't delete inbound records missing from fact_salesorder if t- BI-3605 */
			    and ifnull(fact_salesorderdelivery.DD_SCHEDULENO,0) <> 0
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.* FROM fact_salesorderdelivery,flag_holder_722
WHERE exists (select 1 from LIKP_LIPS a where dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrItemNo = LIPS_POSNR)
/* don't delete inbound records missing from fact_salesorder if t- BI-3605 */
/*			    and ifnull(fact_salesorderdelivery.dd_SalesDocNo,'Not Set') <> 'Not Set' */
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE not exists (select 1 from fact_salesorder f
                      where fact_salesorderdelivery.dd_SalesDocNo = f.dd_SalesDocNo
                            and fact_salesorderdelivery.dd_SalesItemNo = f.dd_SalesItemNo
                            and fact_salesorderdelivery.dd_ScheduleNo = f.dd_ScheduleNo)
/* don't delete inbound records missing from fact_salesorder if t- BI-3605 */
			    and ifnull(fact_salesorderdelivery.DD_SCHEDULENO,0) <> 0
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE dd_SalesDlvrDocNo = 'Not Set'
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
where  pDeltaChangesFlag <> 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

/*** All shipment lines not in LIKP_LIPS - 10/16/2013 ***/

DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'cursor_table1_722';

INSERT INTO NUMBER_FOUNTAIN
select 'cursor_table1_722',ifnull(max(v_iid),0)
FROM cursor_table1_722;

Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingDate,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select (select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'cursor_table1_722' ) + row_number() over (order by ''),
	dd_SalesDlvrDocNo v_DlvrDocNo,
	dd_SalesDlvrItemNo v_DlvrItemNo,
	dd_SalesDocNo v_SalesDocNo,
	dd_SalesItemNo v_SalesItemNo,
	ct_QtyDelivered v_DeliveryQty,
	case when f.Dim_DateidActualGoodsIssue = 1 then agi.datevalue else pgi.datevalue end v_AGI_Date,
	pgi.datevalue v_PGI_Date,
	amt_Cost v_DlvrCost,
	pl.plantcode v_plantcode,
	ctr.ControllingAreaCode v_ControllingArea,
	pc.ProfitCenterCode v_ProfitCenter,
	bdt.datevalue v_BillingDate,
	btp.Type v_BillingType,
	dcn.DistributionChannelCode v_DistChannel,
	 CONVERT(BIGINT,1) v_DlvrRowNo,
	 CONVERT(BIGINT,1) v_DlvrRowNoMax
from fact_salesorderdelivery f
	inner join dim_date agi on f.Dim_DateidActualGoodsIssue = agi.dim_dateid
	inner join dim_date pgi on f.Dim_DateidPlannedGoodsIssue = pgi.dim_dateid
	inner join dim_plant pl on pl.dim_plantid = f.dim_plantid
	inner join Dim_ControllingArea ctr on ctr.Dim_ControllingAreaId = f.Dim_ControllingAreaId
	inner join Dim_ProfitCenter pc on pc.Dim_ProfitCenterid = f.Dim_ProfitCenterid
	inner join dim_date bdt on f.Dim_DateidBillingDate = bdt.dim_dateid
	inner join dim_billingdocumenttype btp on f.dim_billingdocumenttypeid = btp.dim_billingdocumenttypeid
	inner join dim_distributionchannel dcn on dcn.dim_distributionchannelid = f.dim_distributionchannelid
where exists (select 1 from LIKP_LIPS where f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS)
	and not exists (select 1 from LIKP_LIPS where f.dd_SalesDlvrDocNo = LIKP_VBELN and f.dd_SalesDlvrItemNo = LIPS_POSNR);

Drop table if exists cursor_table1_722_tmp;
Create table cursor_table1_722_tmp As
select v_iid,
	v_DlvrDocNo,
	v_DlvrItemNo,
	v_SalesDocNo,
	v_SalesItemNo,
	v_DeliveryQty,
	v_AGI_Date,
	v_PGI_Date,
	v_DlvrCost,
	v_plantcode,
	v_ControllingArea,
	v_ProfitCenter,
	v_BillingDate,
	v_BillingType,
	v_DistChannel,
	 ROW_NUMBER() OVER (PARTITION BY v_SalesDocNo,v_SalesItemNo ORDER BY v_AGI_Date,v_PGI_Date,v_DlvrDocNo,v_DlvrItemNo) AS v_DlvrRowNo,
	 COUNT(1) OVER (PARTITION BY v_SalesDocNo,v_SalesItemNo) v_DlvrRowNoMax
from cursor_table1_722;

delete from cursor_table1_722;
Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingDate,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select v_iid,
	v_DlvrDocNo,
	v_DlvrItemNo,
	v_SalesDocNo,
	v_SalesItemNo,
	v_DeliveryQty,
	v_AGI_Date,
	v_PGI_Date,
	v_DlvrCost,
	v_plantcode,
	v_ControllingArea,
	v_ProfitCenter,
	v_BillingDate,
	v_BillingType,
	v_DistChannel,
	v_DlvrRowNo,
	v_DlvrRowNoMax
from cursor_table1_722_tmp;

Drop table if exists cursor_table1_722_tmp;

/******/

/*** Update cummulative delivery qty ***/

Drop table if exists cursor_table1_723;
Create table cursor_table1_723 as
select  a.v_iid,
	a.v_DlvrDocNo,
	a.v_plantcode,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_DlvrCost,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_Billingdate,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_DeliveryQty) v_DeliveryQtyCUMM
from cursor_table1_722 a inner join cursor_table1_722 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
group by a.v_iid,
	a.v_DlvrDocNo,
	a.v_plantcode,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_DlvrCost,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_Billingdate,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;

Drop table if exists salesorder_table_101;
Create table salesorder_table_101 as
select f.dd_SalesDocNo v_SalesDocNo,
	f.dd_SalesItemNo v_SalesItemNo,
	f.dd_ScheduleNo v_SchedNo,
	gi.DateValue v_GIDate,
	f.ct_ConfirmedQty v_SchedQty,
	ROW_NUMBER() OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo ORDER BY gi.DateValue,f.dd_ScheduleNo) AS v_DlvrRowNo,
	COUNT(1) OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo) v_DlvrRowNoMax
From fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
where f.dd_ItemRelForDelv = 'X' and f.ct_ConfirmedQty > 0
	and exists (select 1 from cursor_table1_722 where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo);

Drop table if exists cursor_table1_724;
Create table cursor_table1_724 as
select a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_SchedQty) v_SchedQtyCUMM
from salesorder_table_101 a inner join salesorder_table_101 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
group by a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;
Drop table if exists salesorder_table_101;

/*** Get final values with schedule link ***/

Drop table if exists cursor_table1_722;
Create table cursor_table1_722 as
select a.v_iid,
	a.v_DlvrDocNo,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_plantcode,
	a.v_AGI_Date,
	a.v_PGI_Date,
	b.v_GIDate,
	a.v_DlvrCost,
	b.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_Billingdate,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	a.v_DeliveryQtyCUMM,
	case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM
	    then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax and b.v_SchedQtyCUMM <= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then 0
	    		else
	    		case when b.v_SchedQty > a.v_DeliveryQty
			      	then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty then a.v_DeliveryQty
					  when b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty - (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM)
					  else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	else case when b.v_DlvrRowNo = b.v_DlvrRowNoMax
			      	  		then b.v_SchedQty
			      	  		else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  				when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
			      	  					then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 		else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  				when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  			  	else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  		end
			 end
		 end
	    else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty
		      	then case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then a.v_DeliveryQty + (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM) else a.v_DeliveryQty end
		      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then b.v_SchedQty
		      	  	else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM))
		      	   end
		 end
	end v_SchedQty,
      case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM
            then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax
            			then case when (b.v_SchedQtyCUMM - b.v_SchedQty) > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
            						then a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)
            				 	else a.v_DeliveryQty end
		      else
		    	case when b.v_SchedQty > a.v_DeliveryQty
	                      then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty or b.v_DlvrRowNo = b.v_DlvrRowNoMax
	                                then a.v_DeliveryQty
	                                else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty
	                                          else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
	                           end
	                      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty
	                      			else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  						when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
			      	  							then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 			else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
	                      					when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  		  				else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	   end
	                 end
	              end
            else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty then a.v_DeliveryQty
                      else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM))
                 end
      end v_DeliveryQty,
      ROW_NUMBER() OVER (PARTITION BY a.v_DlvrDocNo,a.v_DlvrItemNo ORDER BY b.v_DlvrRowNo) AS v_DlvrRowSeq
from cursor_table1_723 a
	inner join cursor_table1_724 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DeliveryQtyCUMM > (b.v_SchedQtyCUMM - b.v_SchedQty)
	and ((b.v_DlvrRowNo < b.v_DlvrRowNoMax and b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)) or b.v_DlvrRowNo = b.v_DlvrRowNoMax);

Drop table if exists cursor_table1_723;
Drop table if exists cursor_table1_724;


/*****/

/* loop table */
drop table if exists loop_tbl_722;
create table loop_tbl_722 as
select * from cursor_table1_722 where v_DlvrRowNoMax > 0 and v_DeliveryQty > 0;

/* This intermediate table is used to handle the order by asc that was used in the insert query ( in mysql proc ) */

DROP TABLE IF EXISTS tmp4a_fs_dimpc ;
CREATE TABLE  tmp4a_fs_dimpc
AS
select pc.ProfitCenterCode,pc.ControllingArea,v_AGI_Date,min(pc.ValidTo) as ValidTo
FROM loop_tbl_722, Dim_ProfitCenter pc
WHERE pc.ProfitCenterCode = v_ProfitCenter
AND pc.ControllingArea = v_ControllingArea
AND pc.ValidTo >= v_AGI_Date
AND pc.RowIsCurrent = 1
GROUP BY pc.ProfitCenterCode,pc.ControllingArea,v_AGI_Date;


DROP TABLE IF EXISTS tmp4_fs_dimpc ;
CREATE TABLE  tmp4_fs_dimpc
AS
select a.ProfitCenterCode,a.ControllingArea,v_AGI_Date,a.ValidTo,pc.dim_profitcenterid
FROM tmp4a_fs_dimpc a , Dim_ProfitCenter pc
WHERE pc.ProfitCenterCode = a.ProfitCenterCode
AND pc.ControllingArea = a.ControllingArea
AND pc.RowIsCurrent = 1
AND pc.ValidTo = a.ValidTo;


drop table if exists tmp_fsod_t001t;
create table tmp_fsod_t001t as
SELECT ifnull(LIPS_VGBEL,'Not Set') dd_SalesDocNo,
              LIPS_VGPOS dd_SalesItemNo,
              f.dd_ScheduleNo,
              LIKP_VBELN dd_SalesDlvrDocNo,
              LIPS_POSNR dd_SalesDlvrItemNo,
              ifnull(lips_bwart ,'Not Set') dd_MovementType,
              IFNULL(lt.v_DeliveryQty,0) ct_QtyDelivered,
              case when v_DlvrRowSeq = 1 then lt.v_DlvrCost else 0 end amt_Cost_DocCurr,
              case when v_DlvrRowSeq = 1 then lt.v_DlvrCost else 0 end amt_Cost,	/* LK: 8 Sep 2013 */
              /*case when v_DlvrRowSeq = 1 then (lt.v_DlvrCost * (case when f.amt_ExchangeRate < 0 then (1/(-1 * f.amt_ExchangeRate)) else f.amt_ExchangeRate end)) else 0 end amt_Cost,*/
              lips_vbeaf ct_FixedProcessDays,
              lips_vbeav ct_ShipProcessDays,
              lt.v_SchedQty ct_ScheduleQtySalesUnit,
              lt.v_SchedQty ct_ConfirmedQty,
              ifnull(f.ct_PriceUnit,1) ct_PriceUnit,
              ifnull(f.amt_UnitPrice,0) amt_UnitPrice,
              ifnull(f.amt_ExchangeRate,1) amt_ExchangeRate,
              ifnull(f.amt_ExchangeRate_GBL,1) amt_ExchangeRate_GBL,
              convert(bigint, 1) as Dim_DateidPlannedGoodsIssue, 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPlannedGoodsIssue,
              convert(bigint, 1) as Dim_DateidActualGoodsIssue, 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGoodsIssue,
			  convert(bigint, 1) as Dim_DateidDeliveryDate, 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_lfdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDeliveryDate,
			  convert(bigint, 1) as Dim_DateidLoadingDate, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoadingDate,
              convert(bigint, 1) as Dim_DateidPickingDate, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_kodat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPickingDate,
			  convert(bigint, 1) as Dim_DateidDlvrDocCreated, 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDlvrDocCreated,
              convert(bigint, 1) as Dim_DateidMatlAvail, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = lips_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMatlAvail,
			  convert(bigint, 1) as Dim_CustomeridSoldTo, 				-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = likp_kunag),f.Dim_CustomerID) Dim_CustomeridSoldTo,
              convert(bigint, 1) as Dim_CustomeridShipTo, 				-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = likp_kunnr),1) Dim_CustomeridShipTo,
              convert(bigint, 1) as Dim_Partid, 						-- ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber = lips_matnr AND dp.Plant = lips_werks),1) Dim_Partid,
              pl.Dim_Plantid,
              convert(bigint, 1) as Dim_StorageLocationid, 				-- ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode = lips_lgort and sl.plant = lips_werks),1) Dim_StorageLocationid,
			  convert(bigint, 1) as Dim_ProductHierarchyid,				-- ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy = lips_prodh),1) Dim_ProductHierarchyid,
              convert(bigint, 1) as Dim_DeliveryHeaderStatusid, 		-- ifnull((select Dim_SalesOrderHeaderStatusid from Dim_SalesOrderHeaderStatus sohs where sohs.SalesDocumentNumber = LIKP_VBELN),1) Dim_DeliveryHeaderStatusid,
              convert(bigint, 1) as Dim_DeliveryItemStatusid, 			-- ifnull((select Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber = LIKP_VBELN and sois.SalesItemNumber = LIPS_POSNR),1) Dim_DeliveryItemStatusid,
              ifnull(f.Dim_DateidSalesOrderCreated,1) Dim_DateidSalesOrderCreated,
              ifnull(f.Dim_DateidSchedDeliveryReq,1) Dim_DateidSchedDeliveryReq,
              ifnull(f.Dim_DateidSchedDlvrReqPrev,1) Dim_DateidSchedDlvrReqPrev,
              ifnull(f.Dim_DateidMtrlAvail,1) as Dim_DateidMatlAvailOriginal,
              ifnull(f.Dim_Currencyid,1) Dim_Currencyid,
              ifnull(f.Dim_Companyid,1) Dim_Companyid,
              ifnull(f.Dim_SalesDivisionid,1) Dim_SalesDivisionid,
              ifnull(f.Dim_ShipReceivePointid,1) Dim_ShipReceivePointid,
              ifnull(f.Dim_DocumentCategoryid,1) Dim_DocumentCategoryid,
              ifnull(f.Dim_SalesDocumentTypeid,1) Dim_SalesDocumentTypeid,
              ifnull(f.Dim_SalesOrgid,1) Dim_SalesOrgid,
              ifnull(f.Dim_SalesGroupid,1) Dim_SalesGroupid,
              ifnull(f.Dim_CostCenterid,1) Dim_CostCenterid,
              ifnull(f.Dim_BillingBlockid,1) Dim_BillingBlockid,
              ifnull(f.Dim_TransactionGroupid,1) Dim_TransactionGroupid,
              ifnull(f.Dim_CustomerGroup1id,1) Dim_CustomerGroup1id,
              ifnull(f.Dim_CustomerGroup2id,1) Dim_CustomerGroup2id,
              ifnull(f.dim_salesorderitemcategoryid,1) dim_salesorderitemcategoryid,
              ifnull(f.Dim_ScheduleLineCategoryId,1) Dim_ScheduleLineCategoryId,
			  convert(bigint, 1) as Dim_ProfitCenterid, 	-- ifnull((select pc.Dim_ProfitCenterid from tmp4_fs_dimpc pc where  pc.ProfitCenterCode = v_ProfitCenter AND pc.ControllingArea = v_ControllingArea AND pc.ValidTo >= v_AGI_Date ),1) Dim_ProfitCenterid,
              convert(bigint, 1) as Dim_ControllingAreaId, 	-- ifnull((select ca.Dim_ControllingAreaid from Dim_ControllingArea ca where  ca.ControllingAreaCode = v_ControllingArea),1) Dim_ControllingAreaId,
              convert(bigint, 1) as Dim_DateIdBillingDate, --- ifnull((SELECT dt.Dim_DateID FROM dim_Date dt WHERE dt.DateValue = v_BillingDate AND dt.CompanyCode = pl.CompanyCode ), 1) Dim_DateIdBillingDate,
			  convert(bigint, 1) as Dim_BillingDocumentTypeid, -- ifnull((SELECT dim_billingdocumenttypeid FROM dim_billingdocumenttype bdt WHERE bdt.Type = v_BillingType AND bdt.RowIsCurrent= 1), 1),
			  convert(bigint, 1) as Dim_DistributionChannelId, -- ifnull((SELECT Dim_DistributionChannelId FROM dim_distributionchannel dc WHERE dc.DistributionChannelCode = v_DistChannel AND dc.RowIsCurrent = 1),f.Dim_DistributionChannelId),
              convert(bigint, 1) as Dim_DateidActualGI_Original, -- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGI_Original,
              ifnull(f.Dim_PurchaseOrderTypeId,1) Dim_PurchaseOrderTypeId,
		(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by '') rid,
		ifnull(f.dim_Currencyid_TRA,1) dim_Currencyid_TRA,
		ifnull(f.dim_Currencyid_GBL,1) dim_Currencyid_GBL,
		ifnull(f.dim_currencyid_STAT,1) dim_currencyid_STAT,
		ifnull(f.amt_exchangerate_STAT,1) amt_exchangerate_STAT,
	/* changes 26 Sep 2013 */
		ifnull(LIKP_ERZET, '000000') as dd_SDCreateTime,
		ifnull(LIKP_LFUHR, '000000') as dd_DeliveryTime,
		ifnull(LIKP_KOUHR, '000000') as dd_PickingTime,
		ifnull(LIKP_WAUHR, '000000') as dd_GITime,
		ifnull(LIPS_ERZET, '000000') as dd_SDLineCreateTime,
		ifnull(likp_bolnr,'Not Set') as dd_BillofLading,
		ifnull(LIPS_CHARG,'Not Set') as dd_batch,
        convert(bigint,1) dim_actualrouteid, ---ifnull((SELECT dim_Routeid FROM dim_Route r WHERE r.RouteCode = LIKP_ROUTE AND r.RowIsCurrent = 1),1) dim_actualrouteid,
		ifnull(LIKP_GRULG, 'Not Set') dd_weightgroup,
		ifnull(LIKP_TRAGR, 'Not Set') dd_transportationgroup,
		ifnull(LIKP_ANZPK, 0) dd_PackageCount,
		convert(bigint,1) dim_unitofmeasureidtotalweight, ---ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM = LIKP_GEWEI AND uom.RowIsCurrent = 1), 1) dim_unitofmeasureidtotalweight,
		convert(bigint,1) dim_unitofmeasureidvolum, ---ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM = LIKP_VOLEH AND uom.RowIsCurrent = 1), 1) dim_unitofmeasureidvolum,
	 	convert(bigint,1) dim_unitofmeasureidvolumline, ---ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM = LIPS_VOLEH AND uom.RowIsCurrent = 1), 1) dim_unitofmeasureidvolumline,
		convert(bigint,1) dim_actualshippingconditionid, ----ifnull( (select dim_shippingconditionid from dim_shippingcondition s where s.shippingconditioncode = LIKP_VSBED and s.rowiscurrent = 1), 1) dim_actualshippingconditionid,
		convert(bigint, 1) dim_defaultShipReceivePointid, ---ifnull((SELECT Dim_ShipReceivePointid FROM Dim_ShipReceivePoint srp WHERE srp.ShipReceivePointCode = likp_vstel),1) dim_defaultShipReceivePointid,
		convert(bigint,1) dim_warehousenumberid, ---ifnull((SELECT dim_warehousenumberid FROM dim_warehousenumber wn WHERE wn.WarehouseCode = ifnull(LIKP_LGNUM, 'Not Set') AND wn.RowIsCurrent = 1),1) dim_warehousenumberid,
		ifnull(LIKP_ABLAD, 'Not Set') dd_unloadingpoint,
		ifnull(LIKP_BTGEW, 0) ct_totalweightheader,
		ifnull(LIPS_BRGEW, 0) ct_grossweight,
		ifnull(LIKP_VOLUM, 0) ct_volumheader,
		convert(bigint,1) dim_unitofmeasureidtotalweightline, ----ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM = LIPS_GEWEI AND uom.RowIsCurrent = 1), 1)  dim_unitofmeasureidtotalweightline,
		ifnull(LIPS_VOLUM, 0) ct_volumline,
		/*Georgiana EA Changes 21 Jan 2016*/
        ifnull(LIKP_AENAM, 'Not Set') dd_changeuser,
        ifnull(LIKP_ERNAM,'Not Set') dd_creationuser,
        Ifnull(LIKP_LIFEX, 'Not Set') dd_externalidentification,
        ifnull(LIKP_VSTEL, 'Not Set') dd_shippingpoint,
        ifnull(LIPS_PSTYV, 'Not Set') dd_dlvritemcategory,
        ifnull(LIPS_SHKZG, 'Not Set') dd_itemreturns,
        convert(bigint,1) dim_dateidchangedon, ----- ifnull((SELECT dt.Dim_DateID FROM dim_Date dt WHERE dt.DateValue = LIKP_AEDAT AND dt.CompanyCode = pl.CompanyCode ), 1)  dim_dateidchangedon,
        convert(bigint,1) dim_dateiditemdlvrcreated, ----ifnull((SELECT dt.Dim_DateID FROM dim_Date dt WHERE dt.DateValue = LIPS_ERDAT AND dt.CompanyCode = pl.CompanyCode ), 1) dim_dateiditemdlvrcreated,
        convert(bigint,1) dim_dateidexpirationdate,  ---ifnull((SELECT dt.Dim_DateID FROM dim_Date dt WHERE dt.DateValue = LIPS_VFDAT AND dt.CompanyCode = pl.CompanyCode ), 1) dim_dateidexpirationdate
		/*End of changes 21 Jan 2016*/
		       likp_wadat, 			-- Dim_DateidPlannedGoodsIssue,
        likp_wadat_ist, 		-- Dim_DateidActualGoodsIssue,
        likp_lfdat, 			-- Dim_DateidDeliveryDate,
        likp_lddat,				-- Dim_DateidLoadingDate,
        likp_kodat,				-- Dim_DateidPickingDate,
        likp_erdat, 			-- Dim_DateidDlvrDocCreated,
        lips_mbdat, 			-- Dim_DateidMatlAvail,
        likp_kunag, 			-- Dim_CustomeridSoldTo,
        likp_kunnr, 			-- Dim_CustomeridShipTo,
        lips_matnr, lips_werks, -- Dim_Partid,
        lips_lgort, 			-- Dim_StorageLocationid,
        lips_prodh, 			-- Dim_ProductHierarchyid,
        v_ProfitCenter, v_ControllingArea, v_AGI_Date, -- Dim_ProfitCenterid, Dim_ControllingAreaId
        v_BillingType,			-- Dim_BillingDocumentTypeid,
        v_DistChannel, f.Dim_DistributionChannelId as Dim_DistributionChannelId_f, -- Dim_DistributionChannelId,
		pl.CompanyCode, f.Dim_CustomerID,v_BillingDate,LIKP_ROUTE,LIKP_GEWEI,LIKP_VOLEH,
		LIKP_VSBED,likp_vstel,LIKP_LGNUM,LIPS_GEWEI,LIKP_AEDAT,LIPS_ERDAT,LIPS_VFDAT,
	        LIPS_VOLEH
	FROM	loop_tbl_722 lt
		inner join LIKP_LIPS on LIKP_VBELN = lt.v_DlvrDocNo and LIPS_POSNR = lt.v_DlvrItemNo
		inner join fact_salesorder f on f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS and f.dd_ScheduleNo = lt.v_SchedNo
		inner join Dim_Plant pl on pl.plantcode = LIPS_WERKS;

			/* Dim_DateidPlannedGoodsIssue */
	update tmp_fsod_t001t f
	set f.Dim_DateidPlannedGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_wadat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidPlannedGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidActualGoodsIssue */
	update tmp_fsod_t001t f
	set f.Dim_DateidActualGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidActualGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDeliveryDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidDeliveryDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_lfdat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidDeliveryDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidLoadingDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidLoadingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_lddat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidLoadingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidPickingDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidPickingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_kodat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidPickingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDlvrDocCreated */
	update tmp_fsod_t001t f
	set f.Dim_DateidDlvrDocCreated = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_erdat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidDlvrDocCreated <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidMatlAvail */
	update tmp_fsod_t001t f
	set f.Dim_DateidMatlAvail = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.lips_mbdat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidMatlAvail <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_CustomeridSoldTo */
	update tmp_fsod_t001t f
	set f.Dim_CustomeridSoldTo = ifnull(cust.Dim_CustomerID, f.Dim_CustomerID)
	from tmp_fsod_t001t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunag
	where f.Dim_CustomeridSoldTo <> ifnull(cust.Dim_CustomerID, f.Dim_CustomerID);

	/* Dim_CustomeridShipTo */
	update tmp_fsod_t001t f
	set f.Dim_CustomeridShipTo = ifnull(cust.Dim_CustomerID, 1)
	from tmp_fsod_t001t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunnr
	where f.Dim_CustomeridShipTo <> ifnull(cust.Dim_CustomerID, 1);

	/* Dim_Partid */
	update tmp_fsod_t001t f
	set f.Dim_Partid = ifnull(dp.dim_partid, 1)
	from tmp_fsod_t001t f
			left join dim_part dp on dp.PartNumber = f.lips_matnr AND dp.Plant = f.lips_werks
	where f.Dim_Partid <> ifnull(dp.dim_partid, 1);

	/* Dim_StorageLocationid */
	update tmp_fsod_t001t f
	set f.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
	from tmp_fsod_t001t f
			left join Dim_StorageLocation sl on sl.LocationCode = f.lips_lgort and sl.plant = f.lips_werks
	where f.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

	/* Dim_ProductHierarchyid */
	update tmp_fsod_t001t f
	set f.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
	from tmp_fsod_t001t f
			left join Dim_ProductHierarchy ph on ph.ProductHierarchy = lips_prodh
	where f.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

	/* Dim_DeliveryHeaderStatusid */
	update tmp_fsod_t001t f
	set f.Dim_DeliveryHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
	from tmp_fsod_t001t f
			left join Dim_SalesOrderHeaderStatus sohs on sohs.SalesDocumentNumber = f.dd_SalesDlvrDocNo
	where f.Dim_DeliveryHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

	/* Dim_DeliveryItemStatusid */
	update tmp_fsod_t001t f
	set f.Dim_DeliveryItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
	from tmp_fsod_t001t f
			left join (select a.SalesDocumentNumber,a.SalesItemNumber,max(a.Dim_SalesOrderItemStatusid) Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus a group by a.SalesDocumentNumber,a.SalesItemNumber) sois
				on sois.SalesDocumentNumber = f.dd_SalesDlvrDocNo and sois.SalesItemNumber = f.dd_SalesDlvrItemNo
	where f.Dim_DeliveryItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

	/* Dim_ProfitCenterid */
	merge into tmp_fsod_t001t fa
	using (select t0.rid, ifnull(t1.Dim_ProfitCenterid, 1) Dim_ProfitCenterid
		   from tmp_fsod_t001t t0
					left join (select distinct f.rid, pc.Dim_ProfitCenterid
							   from tmp_fsod_t001t f
										inner join tmp4_fs_dimpc pc on    pc.ProfitCenterCode = f.v_ProfitCenter
										   						      AND pc.ControllingArea = f.v_ControllingArea
							   where pc.ValidTo >= f.v_AGI_Date) t1 on t0.rid = t1.rid
		  ) src
	on fa.rid = src.rid
	when matched then update set fa.Dim_ProfitCenterid = src.Dim_ProfitCenterid
	where fa.Dim_ProfitCenterid <> src.Dim_ProfitCenterid;

	/* Dim_ControllingAreaId */
	update tmp_fsod_t001t f
	set f.Dim_ControllingAreaId = ifnull(ca.Dim_ControllingAreaid, 1)
	from tmp_fsod_t001t f
			left join Dim_ControllingArea ca on ca.ControllingAreaCode = f.v_ControllingArea
	where f.Dim_ControllingAreaId <> ifnull(ca.Dim_ControllingAreaid, 1);

	/*Dim_DateIdBillingDate*/
	update tmp_fsod_t001t f
	set f.Dim_DateIdBillingDate = ifnull(dt.Dim_DateID, 1)
	from tmp_fsod_t001t f
			left join dim_Date dt on dt.DateValue = v_BillingDate AND  dt.CompanyCode = f.CompanyCode
			and dt.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateIdBillingDate <> ifnull(dt.Dim_DateID, 1);

	/* Dim_BillingDocumentTypeid */
	update tmp_fsod_t001t f
	set f.Dim_BillingDocumentTypeid = ifnull(bdt.dim_billingdocumenttypeid, 1)
	from tmp_fsod_t001t f
			left join dim_billingdocumenttype bdt on bdt.Type = v_BillingType AND bdt.RowIsCurrent= 1
	where f.Dim_BillingDocumentTypeid <> ifnull(bdt.dim_billingdocumenttypeid, 1);

	/* Dim_DistributionChannelId */
	update tmp_fsod_t001t f
	set f.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f)
	from tmp_fsod_t001t f
			left join dim_distributionchannel dc on dc.DistributionChannelCode = f.v_DistChannel AND dc.RowIsCurrent = 1
	where f.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f);

	/* Dim_DateidActualGI_Original */
	update tmp_fsod_t001t f
	set f.Dim_DateidActualGI_Original = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join Dim_Date dd on dd.DateValue = likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidActualGI_Original <> ifnull(dd.Dim_Dateid, 1);

	/*dim_actualrouteid */
	update tmp_fsod_t001t f
	set f.dim_actualrouteid = ifnull(r.dim_Routeid, 1)
	from tmp_fsod_t001t f
			left join dim_Route r on r.RouteCode = LIKP_ROUTE AND r.RowIsCurrent = 1
	where f.dim_actualrouteid <> ifnull(r.dim_Routeid, 1);

	/* dim_unitofmeasureidtotalweight*/
		update tmp_fsod_t001t f
	set f.dim_unitofmeasureidtotalweight = ifnull(uom.Dim_UnitOfMeasureId, 1)
	from tmp_fsod_t001t f
			left join Dim_UnitOfMeasure uom on uom.UOM = LIKP_GEWEI AND uom.RowIsCurrent = 1
	where f.dim_unitofmeasureidtotalweight <> ifnull(uom.Dim_UnitOfMeasureId, 1);

	/*dim_unitofmeasureidvolum*/
	update tmp_fsod_t001t f
	set f.dim_unitofmeasureidvolum = ifnull(uom.Dim_UnitOfMeasureId, 1)
	from tmp_fsod_t001t f
			left join Dim_UnitOfMeasure uom on uom.UOM = LIKP_VOLEH AND uom.RowIsCurrent = 1
	where f.dim_unitofmeasureidvolum <> ifnull(uom.Dim_UnitOfMeasureId, 1);

	/*dim_unitofmeasureidvolumline*/
	update tmp_fsod_t001t f
	set f.dim_unitofmeasureidvolumline = ifnull(uom.Dim_UnitOfMeasureId, 1)
	from tmp_fsod_t001t f
			left join Dim_UnitOfMeasure uom on uom.UOM = LIPS_VOLEH AND uom.RowIsCurrent = 1
	where f.dim_unitofmeasureidvolumline <> ifnull(uom.Dim_UnitOfMeasureId, 1);

	/*dim_actualshippingconditionid*/
	update tmp_fsod_t001t f
	set f.dim_actualshippingconditionid = ifnull(s.dim_shippingconditionid, 1)
	from tmp_fsod_t001t f
			left join dim_shippingcondition s on s.shippingconditioncode = LIKP_VSBED and s.rowiscurrent = 1
	where f.dim_actualshippingconditionid <> ifnull(s.dim_shippingconditionid, 1);

	/*dim_defaultShipReceivePointid*/
	update tmp_fsod_t001t f
	set f.dim_defaultShipReceivePointid = ifnull(srp.Dim_ShipReceivePointid, 1)
	from tmp_fsod_t001t f
			left join Dim_ShipReceivePoint srp on srp.ShipReceivePointCode = likp_vstel
	where f.dim_defaultShipReceivePointid <> ifnull(srp.Dim_ShipReceivePointid, 1);

	/*dim_warehousenumberid*/
	update tmp_fsod_t001t f
	set f.dim_warehousenumberid = ifnull(wn.dim_warehousenumberid, 1)
	from tmp_fsod_t001t f
			left join dim_warehousenumber wn on wn.WarehouseCode = ifnull(LIKP_LGNUM, 'Not Set') AND wn.RowIsCurrent = 1
	where f.dim_warehousenumberid <> ifnull(wn.dim_warehousenumberid, 1);

	/*dim_unitofmeasureidtotalweightline*/
	update tmp_fsod_t001t f
	set f.dim_unitofmeasureidtotalweightline = ifnull(uom.Dim_UnitOfMeasureId, 1)
	from tmp_fsod_t001t f
			left join Dim_UnitOfMeasure uom on uom.UOM = LIPS_GEWEI AND uom.RowIsCurrent = 1
	where f.dim_unitofmeasureidtotalweightline <> ifnull(uom.Dim_UnitOfMeasureId, 1);

    /*dim_dateidchangedon*/
	update tmp_fsod_t001t f
	set f.dim_dateidchangedon = ifnull(dt.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_Date dt on  dt.DateValue = LIKP_AEDAT AND dt.CompanyCode = f.CompanyCode
			and dt.plantcode_factory = f.LIPS_WERKS
	where f.dim_dateidchangedon <> ifnull(dt.Dim_Dateid, 1);

	/*dim_dateiditemdlvrcreated*/
	update tmp_fsod_t001t f
	set f.dim_dateiditemdlvrcreated = ifnull(dt.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_Date dt on  dt.DateValue = LIPS_ERDAT AND dt.CompanyCode = f.CompanyCode
			and dt.plantcode_factory = f.LIPS_WERKS
	where f.dim_dateiditemdlvrcreated <> ifnull(dt.Dim_Dateid, 1);

    /*dim_dateidexpirationdate*/
	update tmp_fsod_t001t f
	set f.dim_dateidexpirationdate = ifnull(dt.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_Date dt on  dt.DateValue = LIPS_VFDAT AND dt.CompanyCode = f.CompanyCode
			and dt.plantcode_factory = f.LIPS_WERKS
	where f.dim_dateidexpirationdate <> ifnull(dt.Dim_Dateid, 1);

   INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateidBillingDate,
              Dim_BillingDocumentTypeid,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,
	      Dim_PurchaseOrderTypeId,
	      fact_salesorderdeliveryid,
		/* Curr changes 8 Sep 2013 */
	      dim_Currencyid_TRA,
	      dim_Currencyid_GBL,
	      dim_currencyid_STAT,
	      amt_exchangerate_STAT,
		 /* changes 26 Sep 2013 */
		  dd_SDCreateTime,
		  dd_DeliveryTime,
		  dd_PickingTime,
		  dd_GITime,
		  dd_SDLineCreateTime,
		  dd_BillofLading,
		  dd_batch,
		  dim_actualrouteid,
		  dd_weightgroup,
		  dd_transportationgroup,
		  dd_PackageCount,
		  dim_unitofmeasureidtotalweight,
          dim_unitofmeasureidvolum,
	  dim_unitofmeasureidvolumline,
		  dim_actualshippingconditionid,
		  dim_defaultShipReceivePointid,
		  dim_warehousenumberid,
		  dd_unloadingpoint,
		  ct_totalweightheader,
		  ct_grossweight,
		  ct_volumheader,
          dim_unitofmeasureidtotalweightline,
          ct_volumline,
		  /*Georgiana EA Changes 21 Jan 2016*/
          dd_changeuser,
          dd_creationuser,
          dd_externalidentification,
          dd_shippingpoint,
          dd_dlvritemcategory,
          dd_itemreturns,
          dim_dateidchangedon,
          dim_dateiditemdlvrcreated,
          dim_dateidexpirationdate
		  /*End of changes 21 Jan 2016*/
		  )
      SELECT dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateidBillingDate,
              Dim_BillingDocumentTypeid,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,
	      Dim_PurchaseOrderTypeId,
	      rid,
	      dim_Currencyid_TRA,
	      dim_Currencyid_GBL,
	      dim_currencyid_STAT,
	      amt_exchangerate_STAT,
		 /* changes 26 Sep 2013 */
		  dd_SDCreateTime,
		  dd_DeliveryTime,
		  dd_PickingTime,
		  dd_GITime,
		  dd_SDLineCreateTime,
		  dd_BillofLading,
		  dd_batch,
		  dim_actualrouteid,
		  dd_weightgroup,
		  dd_transportationgroup,
		  dd_PackageCount,
		  dim_unitofmeasureidtotalweight,
          dim_unitofmeasureidvolum,
	  dim_unitofmeasureidvolumline,
		  dim_actualshippingconditionid,
		  dim_defaultShipReceivePointid,
		  dim_warehousenumberid,
		  dd_unloadingpoint,
		  ct_totalweightheader,
		  ct_grossweight,
		  ct_volumheader,
          dim_unitofmeasureidtotalweightline,
          ct_volumline,
		  /*Georgiana EA Changes 21 Jan 2016*/
          dd_changeuser,
          dd_creationuser,
          dd_externalidentification,
          dd_shippingpoint,
          dd_dlvritemcategory,
          dd_itemreturns,
          dim_dateidchangedon,
          dim_dateiditemdlvrcreated,
          dim_dateidexpirationdate
from tmp_fsod_t001t;
drop table if exists tmp_fsod_t001t;

	update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_salesorderdeliveryid),0) from fact_salesorderdelivery)
	where table_name = 'fact_salesorderdelivery';

       DROP TABLE IF EXISTS tmp_fact_sodf_dimpc_1;
	CREATE TABLE tmp_fact_sodf_dimpc_1
	AS
	SELECT pc.ProfitCenterCode,pc.ControllingArea,ifnull(LIKP_WADAT_IST,LIKP_WADAT) LIKP_WADAT_IST,min(pc.ValidTo) as min_ValidTo
	FROM LIKP_LIPS , Dim_ProfitCenter pc
	WHERE pc.ProfitCenterCode = LIPS_PRCTR
 	AND pc.ControllingArea = LIPS_KOKRS
       AND pc.ValidTo >= ifnull(LIKP_WADAT_IST,LIKP_WADAT)
	AND pc.RowIsCurrent = 1
	GROUP BY pc.ProfitCenterCode,pc.ControllingArea,ifnull(LIKP_WADAT_IST,LIKP_WADAT);

	DROP TABLE IF EXISTS tmp_fact_sodf_dimpc_2;
	CREATE TABLE tmp_fact_sodf_dimpc_2
	AS
	SELECT t.*,pc.Dim_ProfitCenterid
	from Dim_ProfitCenter pc, tmp_fact_sodf_dimpc_1 t
	WHERE pc.ProfitCenterCode = t.ProfitCenterCode
	AND pc.ControllingArea = t.ControllingArea
	AND pc.ValidTo = t.min_ValidTo;

	DROP TABLE IF EXISTS tmp_fact_sodf_minsched;
	CREATE TABLE tmp_fact_sodf_minsched
	AS
	select f1.dd_SalesDocNo,f1.dd_SalesItemNo,min(f1.dd_ScheduleNo) min_dd_ScheduleNo
	from fact_salesorder f1
	GROUP BY f1.dd_SalesDocNo,f1.dd_SalesItemNo;


	DROP TABLE IF EXISTS tmp_fact_sodf_LIKP_LIPS;
	CREATE TABLE tmp_fact_sodf_LIKP_LIPS
	AS
	SELECT l.*,f.min_dd_ScheduleNo
	from LIKP_LIPS l left join tmp_fact_sodf_minsched f on f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS
	where not exists (select 1 from fact_salesorder f1
                        where f1.dd_SalesDocNo = LIPS_VGBEL and f1.dd_SalesItemNo = LIPS_VGPOS and f1.dd_ItemRelForDelv = 'X')
		or not exists (select 1 from loop_tbl_722 lt where LIKP_VBELN = lt.v_DlvrDocNo and LIPS_POSNR = lt.v_DlvrItemNo);

drop table if exists tmp_fsod_t002t;
create table tmp_fsod_t002t as SELECT ifnull(ll.LIPS_VGBEL,'Not Set') dd_SalesDocNo,
              ll.LIPS_VGPOS dd_SalesItemNo,
              0 dd_ScheduleNo,
              ll.LIKP_VBELN dd_SalesDlvrDocNo,
              ll.LIPS_POSNR dd_SalesDlvrItemNo,
              ifnull(ll.lips_bwart ,'Not Set') dd_MovementType,
              ifnull(f.dd_ReferenceDocumentNo,'Not Set') as dd_ReferenceDocNo,
              ll.LIPS_LFIMG ct_QtyDelivered,
              ll.LIPS_WAVWR amt_Cost_DocCurr,
              ll.LIPS_WAVWR amt_Cost,
              /*(ll.LIPS_WAVWR * (case when f.amt_ExchangeRate < 0 then (1/(-1 * f.amt_ExchangeRate)) else f.amt_ExchangeRate end)) amt_Cost,*/
              ifnull(f.amt_SubTotal3,0.0000) as amt_SalesSubTotal3,
              ifnull(f.amt_SubTotal4,0.0000) as amt_SalesSubTotal4,
              ll.lips_vbeaf ct_FixedProcessDays,
              ll.lips_vbeav ct_ShipProcessDays,
              ll.LIPS_LFIMG ct_ScheduleQtySalesUnit,
              ll.LIPS_LFIMG ct_ConfirmedQty,
              ifnull(f.ct_PriceUnit,1) ct_PriceUnit,
              ifnull(f.amt_UnitPrice,0) amt_UnitPrice,
              ifnull(f.amt_ExchangeRate,1) amt_ExchangeRate,
              ifnull(f.amt_ExchangeRate_GBL,1) amt_ExchangeRate_GBL
			  ,convert(bigint, 1) as  Dim_DateidPlannedGoodsIssue 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPlannedGoodsIssue,
              ,convert(bigint, 1) as  Dim_DateidActualGoodsIssue 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGoodsIssue,
              ,convert(bigint, 1) as  Dim_DateidDeliveryDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_lfdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDeliveryDate,
              ,convert(bigint, 1) as  Dim_DateidLoadingDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoadingDate,
              ,convert(bigint, 1) as  Dim_DateidPickingDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_kodat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPickingDate,
              ,convert(bigint, 1) as  Dim_DateidDlvrDocCreated 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDlvrDocCreated,
              ,convert(bigint, 1) as  Dim_DateidMatlAvail 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.lips_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMatlAvail,
              ,convert(bigint, 1) as  Dim_CustomeridSoldTo 			-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = ll.likp_kunag),f.Dim_CustomerID) Dim_CustomeridSoldTo,
              ,convert(bigint, 1) as  Dim_CustomeridShipTo 			-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = ll.likp_kunnr),1) Dim_CustomeridShipTo,
              ,convert(bigint, 1) as  Dim_Partid 					-- ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber = ll.lips_matnr AND dp.Plant = ll.lips_werks),1) Dim_Partid,
              ,pl.Dim_Plantid
              ,convert(bigint, 1) as  Dim_StorageLocationid 		-- ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode = ll.lips_lgort and sl.plant = ll.lips_werks),1) Dim_StorageLocationid,
              ,convert(bigint, 1) as  Dim_ProductHierarchyid 		-- ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy = ll.lips_prodh),1) Dim_ProductHierarchyid,
              ,convert(bigint, 1) as  Dim_DeliveryHeaderStatusid 	-- ifnull((select Dim_SalesOrderHeaderStatusid from Dim_SalesOrderHeaderStatus sohs where sohs.SalesDocumentNumber = ll.LIKP_VBELN),1) Dim_DeliveryHeaderStatusid,
              ,convert(bigint, 1) as  Dim_DeliveryItemStatusid 		-- ifnull((select Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber = ll.LIKP_VBELN and sois.SalesItemNumber = ll.LIPS_POSNR),1) Dim_DeliveryItemStatusid,
              ,ifnull(f.Dim_DateidSalesOrderCreated,1) Dim_DateidSalesOrderCreated,
              ifnull(f.Dim_DateidSchedDeliveryReq,1) Dim_DateidSchedDeliveryReq,
              ifnull(f.Dim_DateidSchedDlvrReqPrev,1) Dim_DateidSchedDlvrReqPrev,
              ifnull(f.Dim_DateidMtrlAvail,1) as Dim_DateidMatlAvailOriginal,
              ifnull(f.Dim_Currencyid,1) Dim_Currencyid,
              ifnull(f.Dim_Companyid,1) Dim_Companyid,
              ifnull(f.Dim_SalesDivisionid,1) Dim_SalesDivisionid,
              ifnull(f.Dim_ShipReceivePointid,1) Dim_ShipReceivePointid,
              ifnull(f.Dim_DocumentCategoryid,1) Dim_DocumentCategoryid,
              ifnull(f.Dim_SalesDocumentTypeid,1) Dim_SalesDocumentTypeid,
              ifnull(f.Dim_SalesOrgid,1) Dim_SalesOrgid,
              ifnull(f.Dim_SalesGroupid,1) Dim_SalesGroupid,
              ifnull(f.Dim_CostCenterid,1) Dim_CostCenterid,
              ifnull(f.Dim_BillingBlockid,1) Dim_BillingBlockid,
              ifnull(f.Dim_TransactionGroupid,1) Dim_TransactionGroupid,
              ifnull(f.Dim_CustomerGroup1id,1) Dim_CustomerGroup1id,
              ifnull(f.Dim_CustomerGroup2id,1) Dim_CustomerGroup2id,
              ifnull(f.dim_salesorderitemcategoryid,1) dim_salesorderitemcategoryid,
              ifnull(f.Dim_ScheduleLineCategoryId,1) Dim_ScheduleLineCategoryId
           ,convert(bigint, 1) as  Dim_ProfitCenterid 			-- ifnull((select pc.Dim_ProfitCenterid from tmp_fact_sodf_dimpc_2 pc where  pc.ProfitCenterCode = LIPS_PRCTR AND pc.ControllingArea = LIPS_KOKRS AND pc.LIKP_WADAT_IST = ifnull(ll.LIKP_WADAT_IST,ll.LIKP_WADAT)),1) Dim_ProfitCenterid,
           ,convert(bigint, 1) as  Dim_ControllingAreaId 		-- ifnull((select ca.Dim_ControllingAreaid from Dim_ControllingArea ca where  ca.ControllingAreaCode = ll.LIPS_KOKRS),1) Dim_ControllingAreaId,
           ,convert(bigint,1) as Dim_DateIdBillingDate ---- ifnull((SELECT dt.Dim_DateID FROM dim_Date dt WHERE dt.DateValue = ll.LIKP_FKDAT AND dt.CompanyCode = pl.CompanyCode ), 1),
           ,convert(bigint, 1) as  Dim_BillingDocumentTypeId 	-- ifnull((SELECT dim_billingdocumenttypeid FROM dim_billingdocumenttype bdt WHERE bdt.Type = ll.LIKP_FKARV AND bdt.RowIsCurrent= 1), 1) Dim_BillingDocumentTypeId,
           ,convert(bigint, 1) as  Dim_DistributionChannelId 	-- ifnull((SELECT Dim_DistributionChannelId FROM dim_distributionchannel dc WHERE dc.DistributionChannelCode = ll.LIKP_VTWIV AND dc.RowIsCurrent = 1),f.Dim_DistributionChannelId) Dim_DistributionChannelId,
           ,convert(bigint, 1) as  Dim_DateidActualGI_Original 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGI_Original,
		   ,ifnull(Dim_PurchaseOrderTypeId,1) Dim_PurchaseOrderTypeId,
			(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by '') fact_salesorderdeliveryid,
                ifnull(f.dim_Currencyid_TRA,1) dim_Currencyid_TRA,
                ifnull(f.dim_Currencyid_GBL,1) dim_Currencyid_GBL,
                ifnull(f.dim_currencyid_STAT,1) dim_currencyid_STAT,
                ifnull(f.amt_exchangerate_STAT,1) amt_exchangerate_STAT,
		/* changes 26 Sep 2013 */
			ifnull(LIKP_ERZET, '000000') as dd_SDCreateTime,
			ifnull(LIKP_LFUHR, '000000') as dd_DeliveryTime,
			ifnull(LIKP_KOUHR, '000000') as dd_PickingTime,
			ifnull(LIKP_WAUHR, '000000') as dd_GITime,
			ifnull(LIPS_ERZET, '000000') as dd_SDLineCreateTime,
			ifnull(likp_bolnr,'Not Set') as dd_BillofLading,
			ifnull(lips_charg,'Not Set') as dd_batch,
			convert(bigint,1) as dim_actualrouteid,	-----ifnull((SELECT dim_Routeid FROM dim_Route r WHERE r.RouteCode = LIKP_ROUTE AND r.RowIsCurrent = 1), 1) dim_actualrouteid,
		ifnull(LIKP_GRULG, 'Not Set') dd_weightgroup,
		ifnull(LIKP_TRAGR, 'Not Set') dd_transportationgroup,
		ifnull(LIKP_ANZPK, 0) dd_PackageCount
		,convert(bigint,1) as dim_unitofmeasureidtotalweight -----ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM = LIKP_GEWEI AND uom.RowIsCurrent = 1), 1) dim_unitofmeasureidtotalweight,
		,convert(bigint,1) as dim_unitofmeasureidvolum     -----ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM = LIKP_VOLEH AND uom.RowIsCurrent = 1), 1) dim_unitofmeasureidvolum,
		,convert(bigint,1) as dim_unitofmeasureidvolumline   ----ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM = LIPS_VOLEH AND uom.RowIsCurrent = 1), 1) dim_unitofmeasureidvolumline,
		,convert(bigint,1) as dim_actualshippingconditionid   ----ifnull( (select dim_shippingconditionid from dim_shippingcondition s where s.shippingconditioncode = LIKP_VSBED and s.rowiscurrent = 1), 1) dim_actualshippingconditionid,
		,convert(bigint,1) as dim_defaultShipReceivePointid   ----ifnull((SELECT Dim_ShipReceivePointid FROM Dim_ShipReceivePoint srp WHERE srp.ShipReceivePointCode = likp_vstel),1) dim_defaultShipReceivePointid,
		,convert(bigint,1) as dim_warehousenumberid , ------------ifnull((SELECT dim_warehousenumberid FROM dim_warehousenumber wn WHERE wn.WarehouseCode = ifnull(LIKP_LGNUM, 'Not Set') AND wn.RowIsCurrent = 1),1) dim_warehousenumberid,
		ifnull(LIKP_ABLAD, 'Not Set') dd_unloadingpoint,
		ifnull(LIKP_BTGEW, 0) ct_totalweightheader,
		ifnull(LIPS_BRGEW, 0) ct_grossweight,
		ifnull(LIKP_VOLUM, 0) ct_volumheader
		,convert(bigint, 1) as dim_unitofmeasureidtotalweightline, -----ifnull((select uom.Dim_UnitOfMeasureId from Dim_UnitOfMeasure uom where   uom.UOM = LIPS_GEWEI AND uom.RowIsCurrent = 1), 1)  dim_unitofmeasureidtotalweightline,
		ifnull(LIPS_VOLUM, 0) ct_volumline,
		/*Georgiana EA Changes 21 Jan 2016*/
        ifnull(LIKP_AENAM, 'Not Set') dd_changeuser,
        ifnull(LIKP_ERNAM,'Not Set') dd_creationuser,
        Ifnull(LIKP_LIFEX, 'Not Set') dd_externalidentification,
        ifnull(LIKP_VSTEL, 'Not Set') dd_shippingpoint,
        ifnull(LIPS_PSTYV, 'Not Set') dd_dlvritemcategory,
        ifnull(LIPS_SHKZG, 'Not Set') dd_itemreturns
        ,convert(bigint,1) as dim_dateidchangedon --------ifnull((SELECT dt.Dim_DateID FROM dim_Date dt WHERE dt.DateValue = LIKP_AEDAT AND dt.CompanyCode = pl.CompanyCode ), 1)  dim_dateidchangedon,
        ,convert(bigint,1) as dim_dateiditemdlvrcreated   -----ifnull((SELECT dt.Dim_DateIDFROM dim_Date dt  WHERE dt.DateValue = LIPS_ERDAT AND dt.CompanyCode = pl.CompanyCode ), 1) dim_dateiditemdlvrcreated,
        ,convert(bigint,1) as dim_dateidexpirationdate, -----------ifnull((SELECT dt.Dim_DateID FROM dim_Date dt WHERE dt.DateValue = LIPS_VFDAT AND dt.CompanyCode = pl.CompanyCode ), 1) dim_dateidexpirationdate
				/*End of changes 21 Jan 2016*/
        		likp_wadat, 			-- Dim_DateidPlannedGoodsIssue,
        likp_wadat_ist, 		-- Dim_DateidActualGoodsIssue,
        likp_lfdat, 			-- Dim_DateidDeliveryDate,
        likp_lddat,				-- Dim_DateidLoadingDate,
        likp_kodat,				-- Dim_DateidPickingDate,
        likp_erdat, 			-- Dim_DateidDlvrDocCreated,
        lips_mbdat, 			-- Dim_DateidMatlAvail,
        likp_kunag, 			-- Dim_CustomeridSoldTo,
        likp_kunnr, 			-- Dim_CustomeridShipTo,
        lips_matnr, lips_werks, -- Dim_Partid,
        lips_lgort, 			-- Dim_StorageLocationid,
        lips_prodh, 			-- Dim_ProductHierarchyid,
        LIPS_PRCTR, LIPS_KOKRS, -- Dim_ProfitCenterid, Dim_ControllingAreaId
        LIKP_FKARV,			-- Dim_BillingDocumentTypeid,
        LIKP_VTWIV, f.Dim_DistributionChannelId as Dim_DistributionChannelId_f, -- Dim_DistributionChannelId,
		pl.CompanyCode, f.Dim_CustomerID, LIKP_FKDAT, LIPS_GEWEI,LIKP_AEDAT	, LIPS_ERDAT,LIPS_VFDAT	,LIKP_ROUTE,LIKP_GEWEI,LIKP_VOLEH,LIPS_VOLEH,LIKP_VSBED,likp_vstel,LIKP_LGNUM
            FROM tmp_fact_sodf_LIKP_LIPS ll
          left join fact_salesorder f on f.dd_SalesDocNo = ll.LIPS_VGBEL and f.dd_SalesItemNo = ll.LIPS_VGPOS
                                          and f.dd_ScheduleNo = ll.min_dd_ScheduleNo
          inner join Dim_Plant pl on pl.plantcode = ll.LIPS_WERKS;


	/* Dim_DateidPlannedGoodsIssue */
	update tmp_fsod_t002t f
	set f.Dim_DateidPlannedGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_wadat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidPlannedGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidActualGoodsIssue */
	update tmp_fsod_t002t f
	set f.Dim_DateidActualGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidActualGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDeliveryDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidDeliveryDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_lfdat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidDeliveryDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidLoadingDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidLoadingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_lddat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidLoadingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidPickingDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidPickingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_kodat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidPickingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDlvrDocCreated */
	update tmp_fsod_t002t f
	set f.Dim_DateidDlvrDocCreated = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_erdat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidDlvrDocCreated <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidMatlAvail */
	update tmp_fsod_t002t f
	set f.Dim_DateidMatlAvail = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.lips_mbdat AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidMatlAvail <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_Partid */
	update tmp_fsod_t002t f
	set f.Dim_Partid = ifnull(dp.dim_partid, 1)
	from tmp_fsod_t002t f
			left join dim_part dp on dp.PartNumber = f.lips_matnr AND dp.Plant = f.lips_werks
	where f.Dim_Partid <> ifnull(dp.dim_partid, 1);

	/* Dim_StorageLocationid */
	update tmp_fsod_t002t f
	set f.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
	from tmp_fsod_t002t f
			left join Dim_StorageLocation sl on sl.LocationCode = f.lips_lgort and sl.plant = f.lips_werks
	where f.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

	/* Dim_ProductHierarchyid */
	update tmp_fsod_t002t f
	set f.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
	from tmp_fsod_t002t f
			left join Dim_ProductHierarchy ph on ph.ProductHierarchy = lips_prodh
	where f.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

	/* Dim_DeliveryHeaderStatusid */
	update tmp_fsod_t002t f
	set f.Dim_DeliveryHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
	from tmp_fsod_t002t f
			left join Dim_SalesOrderHeaderStatus sohs on sohs.SalesDocumentNumber = f.dd_SalesDlvrDocNo
	where f.Dim_DeliveryHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

	/* Dim_DeliveryItemStatusid */
	update tmp_fsod_t002t f
	set f.Dim_DeliveryItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
	from tmp_fsod_t002t f
			left join (select a.SalesDocumentNumber,a.SalesItemNumber,max(a.Dim_SalesOrderItemStatusid) Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus a group by a.SalesDocumentNumber,a.SalesItemNumber) sois on sois.SalesDocumentNumber = f.dd_SalesDlvrDocNo and sois.SalesItemNumber = f.dd_SalesDlvrItemNo
	where f.Dim_DeliveryItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

	/* Dim_ProfitCenterid */
	update tmp_fsod_t002t f
	set f.Dim_ProfitCenterid = ifnull(pc.Dim_ProfitCenterid, 1)
	from tmp_fsod_t002t f
			left join tmp_fact_sodf_dimpc_2 pc on   pc.ProfitCenterCode = f.LIPS_PRCTR
												AND pc.ControllingArea  = f.LIPS_KOKRS
												AND pc.LIKP_WADAT_IST   = ifnull(f.LIKP_WADAT_IST,f.LIKP_WADAT)
	where f.Dim_ProfitCenterid <> ifnull(pc.Dim_ProfitCenterid, 1);

	/* Dim_ControllingAreaId */
	update tmp_fsod_t002t f
	set f.Dim_ControllingAreaId = ifnull(ca.Dim_ControllingAreaid, 1)
	from tmp_fsod_t002t f
			left join Dim_ControllingArea ca on ca.ControllingAreaCode = f.LIPS_KOKRS
	where f.Dim_ControllingAreaId <> ifnull(ca.Dim_ControllingAreaid, 1);

	/*Dim_DateIdBillingDate*/

	update tmp_fsod_t002t f
	set f.Dim_DateIdBillingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on  dd.DateValue = f.LIKP_FKDAT AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateIdBillingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_BillingDocumentTypeid */
	update tmp_fsod_t002t f
	set f.Dim_BillingDocumentTypeid = ifnull(bdt.dim_billingdocumenttypeid, 1)
	from tmp_fsod_t002t f
			left join dim_billingdocumenttype bdt on bdt.Type = LIKP_FKARV AND bdt.RowIsCurrent= 1
	where f.Dim_BillingDocumentTypeid <> ifnull(bdt.dim_billingdocumenttypeid, 1);

	/* Dim_DistributionChannelId */
	update tmp_fsod_t002t f
	set f.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f)
	from tmp_fsod_t002t f
			left join dim_distributionchannel dc on dc.DistributionChannelCode = f.LIKP_VTWIV AND dc.RowIsCurrent = 1
	where f.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f);

	/* Dim_DateidActualGI_Original */
	update tmp_fsod_t002t f
	set f.Dim_DateidActualGI_Original = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join Dim_Date dd on dd.DateValue = likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.Dim_DateidActualGI_Original <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_CustomeridSoldTo */
	update tmp_fsod_t002t f
	set f.Dim_CustomeridSoldTo = ifnull(cust.Dim_CustomerID, f.Dim_CustomerID)
	from tmp_fsod_t002t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunag
	where f.Dim_CustomeridSoldTo <> ifnull(cust.Dim_CustomerID, f.Dim_CustomerID);

	/* Dim_CustomeridShipTo */
	update tmp_fsod_t002t f
	set f.Dim_CustomeridShipTo = ifnull(cust.Dim_CustomerID, 1)
	from tmp_fsod_t002t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunnr
	where f.Dim_CustomeridShipTo <> ifnull(cust.Dim_CustomerID, 1);

   /*dim_actualrouteid*/

	update tmp_fsod_t002t f
	set f.dim_actualrouteid = ifnull(r.dim_Routeid, 1)
	from tmp_fsod_t002t f
			left join dim_Route r on r.RouteCode = f.LIKP_ROUTE AND r.RowIsCurrent = 1
	where f.dim_actualrouteid <> ifnull(r.dim_Routeid, 1);

	/*dim_unitofmeasureidtotalweight*/

	update tmp_fsod_t002t f
	set f.dim_unitofmeasureidtotalweight = ifnull(uom.Dim_UnitOfMeasureId, 1)
	from tmp_fsod_t002t f
			left join Dim_UnitOfMeasure uom on uom.UOM = LIKP_GEWEI AND uom.RowIsCurrent = 1
	where f.dim_unitofmeasureidtotalweight <> ifnull(uom.Dim_UnitOfMeasureId, 1);

/*dim_unitofmeasureidvolum  */

	update tmp_fsod_t002t f
	set f.dim_unitofmeasureidvolum = ifnull(uom.Dim_UnitOfMeasureId, 1)
	from tmp_fsod_t002t f
			left join Dim_UnitOfMeasure uom on uom.UOM = LIKP_VOLEH AND uom.RowIsCurrent = 1
	where f.dim_unitofmeasureidvolum <> ifnull(uom.Dim_UnitOfMeasureId, 1);

	/*dim_unitofmeasureidvolumline */

	update tmp_fsod_t002t f
	set f.dim_unitofmeasureidvolumline = ifnull(uom.Dim_UnitOfMeasureId, 1)
	from tmp_fsod_t002t f
			left join Dim_UnitOfMeasure uom on uom.UOM = LIPS_VOLEH AND uom.RowIsCurrent = 1
	where f.dim_unitofmeasureidvolumline <> ifnull(uom.Dim_UnitOfMeasureId, 1);

	/*dim_actualshippingconditionid */

	update tmp_fsod_t002t f
	set f.dim_actualshippingconditionid = ifnull(s.dim_shippingconditionid, 1)
	from tmp_fsod_t002t f
			left join dim_shippingcondition s on s.shippingconditioncode = LIKP_VSBED and s.rowiscurrent = 1
	where f.dim_actualshippingconditionid <> ifnull(s.dim_shippingconditionid, 1);

	/*dim_defaultShipReceivePointid */
	update tmp_fsod_t002t f
	set f.dim_defaultShipReceivePointid = ifnull(srp.Dim_ShipReceivePointid, 1)
	from tmp_fsod_t002t f
			left join Dim_ShipReceivePoint srp on srp.ShipReceivePointCode = likp_vstel
	where f.dim_defaultShipReceivePointid <> ifnull(srp.Dim_ShipReceivePointid, 1);

	/*dim_warehousenumberid */
	update tmp_fsod_t002t f
	set f.dim_warehousenumberid = ifnull(wn.dim_warehousenumberid, 1)
	from tmp_fsod_t002t f
			left join dim_warehousenumber wn on wn.WarehouseCode = ifnull(LIKP_LGNUM, 'Not Set') AND wn.RowIsCurrent = 1
	where f.dim_warehousenumberid <> ifnull(wn.dim_warehousenumberid, 1);

   /*dim_unitofmeasureidtotalweightline*/

	update tmp_fsod_t002t f
	set f.dim_unitofmeasureidtotalweightline = ifnull(uom.Dim_UnitOfMeasureId, 1)
	from tmp_fsod_t002t f
			left join Dim_UnitOfMeasure uom on uom.UOM = LIPS_GEWEI AND uom.RowIsCurrent = 1
	where f.dim_unitofmeasureidtotalweightline <> ifnull(uom.Dim_UnitOfMeasureId, 1);

  /*dim_dateidchangedon */

  	update tmp_fsod_t002t f
	set f.dim_dateidchangedon = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.LIKP_AEDAT AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.dim_dateidchangedon <> ifnull(dd.Dim_Dateid, 1);

/*dim_dateiditemdlvrcreated */

  	update tmp_fsod_t002t f
	set f.dim_dateiditemdlvrcreated = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.LIPS_ERDAT AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.dim_dateiditemdlvrcreated <> ifnull(dd.Dim_Dateid, 1);

/*dim_dateidexpirationdate*/
  	update tmp_fsod_t002t f
	set f.dim_dateidexpirationdate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.LIPS_VFDAT AND dd.CompanyCode = f.CompanyCode
			and dd.plantcode_factory = f.LIPS_WERKS
	where f.dim_dateidexpirationdate <> ifnull(dd.Dim_Dateid, 1);

      INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,Dim_PurchaseOrderTypeId,fact_salesorderdeliveryid,
                /* Curr changes 8 Sep 2013 */
              dim_Currencyid_TRA,
              dim_Currencyid_GBL,
              dim_currencyid_STAT,
              amt_exchangerate_STAT,
			  /* changes 26 Sep 2013 */
			  dd_SDCreateTime,
			  dd_DeliveryTime,
			  dd_PickingTime,
			  dd_GITime,
			  dd_SDLineCreateTime,
			  dd_BillofLading,
			  dd_batch,
			  dim_actualrouteid,
		  dd_weightgroup,
		  dd_transportationgroup,
		  dd_PackageCount,
		  dim_unitofmeasureidtotalweight,
          dim_unitofmeasureidvolum,
	  dim_unitofmeasureidvolumline,
		  dim_actualshippingconditionid,
		  dim_defaultShipReceivePointid,
		  dim_warehousenumberid,
		  dd_unloadingpoint,
		  ct_totalweightheader,
		  ct_grossweight,
		  ct_volumheader,
                  dim_unitofmeasureidtotalweightline,
                  ct_volumline,
				  /*Georgiana EA Changes 21 Jan 2016*/
              dd_changeuser,
              dd_creationuser,
              dd_externalidentification,
              dd_shippingpoint,
              dd_dlvritemcategory,
              dd_itemreturns,
              dim_dateidchangedon,
              dim_dateiditemdlvrcreated,
              dim_dateidexpirationdate
			  /*End of changes 21 Jan 2016*/)
      SELECT              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,Dim_PurchaseOrderTypeId,fact_salesorderdeliveryid,
                /* Curr changes 8 Sep 2013 */
              dim_Currencyid_TRA,
              dim_Currencyid_GBL,
              dim_currencyid_STAT,
              amt_exchangerate_STAT,
			  /* changes 26 Sep 2013 */
			  dd_SDCreateTime,
			  dd_DeliveryTime,
			  dd_PickingTime,
			  dd_GITime,
			  dd_SDLineCreateTime,
			  dd_BillofLading,
			  dd_batch,
			  dim_actualrouteid,
		  dd_weightgroup,
		  dd_transportationgroup,
		  dd_PackageCount,
		  dim_unitofmeasureidtotalweight,
          dim_unitofmeasureidvolum,
	  dim_unitofmeasureidvolumline,
		  dim_actualshippingconditionid,
		  dim_defaultShipReceivePointid,
		  dim_warehousenumberid,
		  dd_unloadingpoint,
		  ct_totalweightheader,
		  ct_grossweight,
		  ct_volumheader,
                  dim_unitofmeasureidtotalweightline,
                  ct_volumline,
				  /*Georgiana EA Changes 21 Jan 2016*/
              dd_changeuser,
              dd_creationuser,
              dd_externalidentification,
              dd_shippingpoint,
              dd_dlvritemcategory,
              dd_itemreturns,
              dim_dateidchangedon,
              dim_dateiditemdlvrcreated,
              dim_dateidexpirationdate
	      from tmp_fsod_t002t;

drop table if exists tmp_fsod_t002t ;

/*  Liviu Ionescu - Remove extra logic for ct_QtyDelivered - BI-3553*/
/* drop table if exists tmp_updt_VBFA
create table tmp_updt_VBFA
as
select fact_salesorderdeliveryid, max(case when (sod.ct_QtyDelivered - v.vbfa_rfmng) < 0 then 0 else (sod.ct_QtyDelivered - v.vbfa_rfmng) end) ct_QtyDelivered
FROM VBFA v, fact_salesorderdelivery sod
  WHERE sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
      and v.vbfa_bwart = '602' and sod.ct_QtyDelivered > 0
      and not exists (select 1 from VBFA v1
                      where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                            and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat)
GROUP BY fact_salesorderdeliveryid

UPDATE fact_salesorderdelivery sod
SET sod.ct_QtyDelivered = ifnull(v.ct_QtyDelivered,0)
FROM tmp_updt_VBFA v, fact_salesorderdelivery sod
WHERE sod.fact_salesorderdeliveryid = v.fact_salesorderdeliveryid
  AND ifnull(sod.ct_QtyDelivered,-1) = ifnull(v.ct_QtyDelivered,0)

drop table if exists tmp_updt_VBFA
*/
/*
UPDATE fact_salesorderdelivery sod
  SET sod.ct_QtyDelivered = case when (sod.ct_QtyDelivered - v.vbfa_rfmng) < 0 then 0 else (sod.ct_QtyDelivered - v.vbfa_rfmng) end
FROM (select distinct vbfa_rfmng,vbfa_vbelv,vbfa_posnv,vbfa_bwart,vbfa_erdat from VBFA) v, fact_salesorderdelivery sod
  WHERE sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
      and v.vbfa_bwart = '602' and sod.ct_QtyDelivered > 0
      and not exists (select 1 from VBFA v1
                      where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                            and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat) */
			    
/* Madalina 3 Oct 2016 - Remove extra logic for ct_QtyDelivered - BI-3553*/
/* MERGE INTO fact_salesorderdelivery fact
USING (
SELECT fact_salesorderdeliveryid, max(case when (sod.ct_QtyDelivered - v.vbfa_rfmng) < 0 then 0 else (sod.ct_QtyDelivered - v.vbfa_rfmng) end) AS ct_QtyDelivered
FROM (select distinct vbfa_rfmng,vbfa_vbelv,vbfa_posnv,vbfa_bwart,vbfa_erdat from VBFA) v, fact_salesorderdelivery sod
  WHERE sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
      and v.vbfa_bwart = '602' and sod.ct_QtyDelivered > 0
      and not exists (select 1 from VBFA v1
                      where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                            and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat)
group by fact_salesorderdeliveryid) src
ON fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid 
WHEN MATCHED THEN UPDATE
SET fact.ct_Qtydelivered = src.ct_QtyDelivered */

UPDATE fact_salesorderdelivery sod
SET sod.ct_QtyDelivered = IFNULL(ll.LIPS_LFIMG,0)
FROM LIKP_LIPS ll, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
	AND sod.dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
	AND sod.dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
	AND sod.dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
	AND sod.ct_QtyDelivered <> IFNULL(ll.LIPS_LFIMG,0);


/* End BI-3553 */
/* start CALL bi_populate_so_shipment() */

drop table if exists update_so_shipment_001;
create table update_so_shipment_001 as
select f.dd_SalesDocNo v_dd_SalesDocNo, f.dd_SalesItemNo v_dd_SalesItemNo, f.dd_ScheduleNo v_dd_ScheduleNo,
	ifnull(sum(f.ct_QtyDelivered), 0) v_ct_DeliveredQty,
	ifnull(max(f.Dim_DateidDeliveryDate), 1) v_Dim_DateidShipmentDelivery,
	ifnull(max(f.Dim_DateidActualGoodsIssue), 1) v_Dim_DateidActualGI,
	min(f.Dim_CustomeridShipto) v_Dim_CustomeridShipTo,
	ifnull(min(f.Dim_DateidDlvrDocCreated), 1) v_Dim_DateidDlvrDocCreated
from fact_salesorderdelivery f
	inner join dim_salesorderitemstatus sois on f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.GoodsMovementStatus <> 'Not yet processed'
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo;

/* LK: 12 Aug 2014 - Dim_CustomeridShipTo should not depend on sois.GoodsMovementStatus */
drop table if exists update_so_shipment_001_Dim_CustomeridShipTo;
CREATE TABLE update_so_shipment_001_Dim_CustomeridShipTo
AS
select f.dd_SalesDocNo v_dd_SalesDocNo, f.dd_SalesItemNo v_dd_SalesItemNo, f.dd_ScheduleNo v_dd_ScheduleNo,
min(f.Dim_CustomeridShipto) v_Dim_CustomeridShipTo
from fact_salesorderdelivery f
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo;


UPDATE fact_salesorder so
SET ct_DeliveredQty = 0
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE  ct_DeliveredQty <> 0;

UPDATE fact_salesorder so
   SET ct_DeliveredQty = v_ct_DeliveredQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

UPDATE fact_salesorder so
   SET Dim_DateidShipmentDelivery = v_Dim_DateidShipmentDelivery
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;


UPDATE fact_salesorder so
   SET Dim_DateidActualGI = v_Dim_DateidActualGI
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

/*UPDATE fact_salesorder so
   SET Dim_CustomeridShipTo = ifnull(v_Dim_CustomeridShipTo, Dim_CustomeridShipTo)
	,dw_update_date = current_timestamp  Added automatically by update_dw_update_date.pl
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo*/

/* LK: 12 Aug 2014 - Dim_CustomeridShipTo should not depend on sois.GoodsMovementStatus */
/*UPDATE fact_salesorder so
   SET Dim_CustomeridShipTo = v_Dim_CustomeridShipTo
   ,dw_update_date = current_timestamp  Added automatically by update_dw_update_date.pl
  FROM update_so_shipment_001_Dim_CustomeridShipTo u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo
 AND v_Dim_CustomeridShipTo is NOT NULL and Dim_CustomeridShipTo = 1 AND v_Dim_CustomeridShipTo <> 1*/

drop table if exists update_so_shipment_001_Dim_CustomeridShipTo;

UPDATE fact_salesorder so
   SET Dim_DateidDlvrDocCreated = v_Dim_DateidDlvrDocCreated
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;


  UPDATE fact_salesorder so
  SET ct_DeliveredQty = so.ct_ConfirmedQty
  fROM dim_salesorderitemstatus s, fact_salesorder so
  WHERE so.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      AND so.dd_ItemRelForDelv = 'X'
      AND s.OverallDeliveryStatus = 'Completely processed'
      AND so.ct_DeliveredQty < so.ct_ConfirmedQty
      AND not exists (select 1 from fact_salesorderdelivery sod inner join VBFA v on sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
                      where v.vbfa_bwart = '602' and sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo
                      and not exists (select 1 from VBFA v1 where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                                      and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat));

/* end update so_shipment */

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_salesorderdeliveryid),0) from fact_salesorderdelivery)
where table_name = 'fact_salesorderdelivery';

  INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,fact_salesorderdeliveryid,
                dim_Currencyid_TRA,
                dim_Currencyid_GBL,
                dim_currencyid_STAT,
                amt_exchangerate_STAT	)

      SELECT f.dd_SalesDocNo,
              f.dd_SalesItemNo,
              f.dd_ScheduleNo,
              'Not Set' dd_SalesDlvrDocNo,
              0 dd_SalesDlvrItemNo,
              'Not Set' dd_MovementType,
              f.dd_ReferenceDocumentNo,
              0 ct_QtyDelivered,
              0 amt_Cost_DocCurr,
              0 amt_Cost,
              ifnull(f.amt_SubTotal3,0.0000) amt_SalesSubTotal3,
              ifnull(f.amt_SubTotal4,0.0000) amt_SalesSubTotal4,
              0 ct_FixedProcessDays,
              0 ct_ShipProcessDays,
              f.ct_ScheduleQtySalesUnit,
              f.ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL,
              f.Dim_DateidGoodsIssue,
              1 Dim_DateidActualGoodsIssue,
              f.Dim_DateidSchedDelivery,
              f.Dim_DateidLoading,
              1 Dim_DateidPickingDate,
              1 Dim_DateidDlvrDocCreated,
              f.Dim_DateidMtrlAvail,
              f.Dim_CustomerID Dim_CustomeridSoldTo,
              f.Dim_CustomerID Dim_CustomeridShipTo,
              f.Dim_Partid,
              f.Dim_Plantid,
              f.Dim_StorageLocationid,
              f.Dim_ProductHierarchyid,
              1 Dim_DeliveryHeaderStatusid,
              1 Dim_DeliveryItemStatusid,
              f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId,
              1 Dim_ProfitCenterid,
              1 Dim_ControllingAreaId,
              1 Dim_DateIdBillingDate,
              1 Dim_BillingDocumentTypeId,
              f.Dim_DistributionChannelId Dim_DistributionChannelId,
	(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by ''),
                f.dim_Currencyid_TRA,
                f.dim_Currencyid_GBL,
                f.dim_currencyid_STAT,
                f.amt_exchangerate_STAT

      FROM fact_salesorder f inner join dim_salesorderitemstatus s
                                    ON f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      where f.dd_ItemRelForDelv = 'X' and s.OverallDeliveryStatus <> 'Completely processed'
            and (ct_ConfirmedQty - ct_DeliveredQty) > 0
            and not exists (select 1 from fact_salesorderdelivery f1
                            where f1.dd_SalesDocNo = f.dd_SalesDocNo and f1.dd_SalesItemNo = f.dd_SalesItemNo and f1.dd_ScheduleNo = f.dd_ScheduleNo);
-----------------
/* Madalina 3 Oct 2016 - Remove extra logic for ct_QtyDelivered - BI-3553*/
UPDATE fact_salesorderdelivery sod
SET sod.ct_QtyDelivered = IFNULL(ll.LIPS_LFIMG,0)
FROM LIKP_LIPS ll, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
	AND sod.dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
	AND sod.dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
	AND sod.dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
	AND sod.ct_QtyDelivered <> IFNULL(ll.LIPS_LFIMG,0);
/* End BI-3553 */	

/*Georgiana 13 ian 2017 Unable to get a stable source of rows*/
/*UPDATE fact_salesorderdelivery sod
SET sod.Dim_deliveryTypeId = dt.Dim_deliveryTypeId
  FROM LIKP_LIPS, Dim_Deliverytype dt, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
  AND LIKP_LFART IS NOT NULL
  AND dt.DeliveryType = LIKP_LFART
  AND dt.RowIsCurrent = 1*/
  
  merge into  fact_salesorderdelivery sod
using (select distinct fact_salesorderdeliveryid,dt.Dim_deliveryTypeId
  FROM LIKP_LIPS, Dim_Deliverytype dt, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
  AND LIKP_LFART IS NOT NULL
  AND dt.DeliveryType = LIKP_LFART
  AND dt.RowIsCurrent = 1
and sod.Dim_deliveryTypeId <> dt.Dim_deliveryTypeId) t
on sod.fact_salesorderdeliveryid=t.fact_salesorderdeliveryid
when matched then update set sod.Dim_deliveryTypeId = t.Dim_deliveryTypeId;

UPDATE facT_salesorderdelivery
SET Dim_deliveryTypeId = 1
WHERE Dim_deliveryTypeId IS NULL;


MERGE INTO fact_salesorderdelivery fact
USING (select fact_salesorderdeliveryid,max(oscc.dim_overallstatusforcreditcheckID) as dim_overallstatusforcreditcheckID
FROM likp_lips_vbuk v, dim_overallstatusforcreditcheck oscc,fact_salesorderdelivery sod
WHERE	sod.dd_SalesDlvrDocNo = v.VBUK_VBELN
AND	oscc.overallstatusforcreditcheck = ifnull(v.VBUK_CMGST, 'Not Set')
AND oscc.Description <>'Not Set'
AND oscc.RowIsCurrent = 1
group by fact_salesorderdeliveryid) src
ON fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
WHEN MATCHED THEN UPDATE SET fact.Dim_OverallStatusCreditCheckId = src.dim_overallstatusforcreditcheckID
WHERE fact.Dim_OverallStatusCreditCheckId <> src.dim_overallstatusforcreditcheckID;


/* Unstable set of rows */
drop table if exists tmp_for_upd_dd_billing_no;
create table tmp_for_upd_dd_billing_no as
select distinct dd_billing_no,dd_SalesDlvrDocNo,dd_SalesDlvrItemNo,
	 row_number() over (partition by dd_SalesDlvrDocNo, dd_SalesDlvrItemNo order by '') as rownumber
FROM fact_billing;

UPDATE fact_salesorderdelivery sod
    SET sod.dd_billing_no = fb.dd_billing_no
FROM tmp_for_upd_dd_billing_no fb, fact_salesorderdelivery sod
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        and rownumber = 1;

drop table if exists tmp_for_upd_dd_billing_no;

UPDATE fact_salesorderdelivery sod
	SET sod.Dim_SalesDistrictId = so.Dim_SalesDistrictId
FROM fact_salesorder so, fact_salesorderdelivery sod
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo 
 	and sod.dd_SalesItemNo = so.dd_SalesItemNo 
 	and sod.dd_ScheduleNo = so.dd_ScheduleNo;

 /*Georgiana Changes 18 Mar 2016 adding dd_DeliveryNumber according to BI-2348*/
 UPDATE fact_salesorderdelivery sod
	SET sod.dd_DeliveryNumber = ifnull(so.dd_DeliveryNumber,'Not Set')

FROM fact_salesorder so,fact_salesorderdelivery sod
 WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo and sod.dd_ScheduleNo = so.dd_ScheduleNo;
/*18 Mar 2016 End of Changes */

/* Start of the final update	*/

UPDATE fact_salesorderdelivery sd
  /*  SET sd.amt_Cost =
            Decimal(((CASE
                  WHEN f.amt_ExchangeRate < 0
                  THEN
                      (1 / (-1 * f.amt_ExchangeRate))
                  ELSE
                      f.amt_ExchangeRate
                END) * sd.amt_Cost_DocCurr),18,4) , */
	SET sd.amt_Cost = sd.amt_Cost_DocCurr,			--Not multiplying by local exchg rate. Stored as it is in doc/tran curr
        sd.ct_PriceUnit = f.ct_PriceUnit,
        sd.amt_UnitPrice = f.amt_UnitPrice
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

merge into fact_salesorderdelivery fact
using (select sd.fact_salesorderdeliveryid, min(f.amt_UnitPriceUoM) amt_UnitPriceUoM
	   From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
	   WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
			AND f.dd_SalesItemNo = v.VBAP_POSNR
			AND f.dd_ScheduleNo = v.VBEP_ETENR
			AND sd.dd_SalesDocNo = f.dd_SalesDocNo
			AND sd.dd_SalesItemNo = f.dd_SalesItemNo
			AND sd.dd_ScheduleNo = f.dd_ScheduleNo
	   group by sd.fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.amt_UnitPriceUoM = src.amt_UnitPriceUoM
where fact.amt_UnitPriceUoM <> src.amt_UnitPriceUoM;


/*START optimization 1 - 28.10 ALIN GHEORGHE*/
merge into fact_salesorderdelivery sod using
(select
		f.dd_SalesDocNo,
		f.dd_SalesItemNo,
		f.dd_ScheduleNo,
		f.amt_ExchangeRate,
		f.amt_ExchangeRate_GBL,
		f.Dim_DateidSalesOrderCreated,
		f.Dim_DateidSchedDeliveryReq,
		f.Dim_DateidSchedDlvrReqPrev,
		f.Dim_DateidMtrlAvail,		
		f.Dim_DateidFirstDate,
		f.Dim_Currencyid,		
		f.Dim_Currencyid_TRA,		
		f.Dim_Currencyid_GBL,		
		f.Dim_Currencyid_STAT,		
		f.amt_ExchangeRate_STAT,		
		f.Dim_Companyid,
		f.Dim_SalesDivisionid,		
		f.Dim_ShipReceivePointid,
		f.Dim_DocumentCategoryid,		
		f.Dim_SalesDocumentTypeid,
		f.Dim_SalesOrgid,		
		f.Dim_SalesGroupid,
		f.Dim_CostCenterid,		
		f.Dim_BillingBlockid,
		f.Dim_TransactionGroupid,	
		f.dim_purchaseordertypeid,
		f.Dim_Customerid,
		f.Dim_CustomerGroup1id,
		f.Dim_CustomerGroup2id,
		f.dim_salesorderitemcategoryid,
		f.Dim_ScheduleLineCategoryId	
	From fact_salesorder f, VBAK_VBAP_VBEP v
	where 	f.dd_SalesDocNo = v.VBAK_VBELN
		AND f.dd_SalesItemNo = v.VBAP_POSNR
		AND f.dd_ScheduleNo = v.VBEP_ETENR) subsel
on sod.dd_SalesDocNo = subsel.dd_SalesDocNo and sod.dd_SalesItemNo = subsel.dd_SalesItemNo and sod.dd_ScheduleNo = subsel.dd_ScheduleNo
when matched then update
set
		sod.amt_ExchangeRate = subsel.amt_ExchangeRate,
        sod.amt_ExchangeRate_GBL = subsel.amt_ExchangeRate_GBL,
		sod.Dim_DateidSalesOrderCreated = subsel.Dim_DateidSalesOrderCreated,
        sod.Dim_DateidSchedDeliveryReq = subsel.Dim_DateidSchedDeliveryReq,
		sod.Dim_DateidSchedDlvrReqPrev = subsel.Dim_DateidSchedDlvrReqPrev,
        sod.Dim_DateidMatlAvailOriginal = subsel.Dim_DateidMtrlAvail,
		sod.Dim_DateidFirstDate = subsel.Dim_DateidFirstDate,
        sod.Dim_Currencyid = subsel.Dim_Currencyid,
		sod.Dim_Currencyid_TRA = subsel.Dim_Currencyid_TRA,
		sod.Dim_Currencyid_GBL = subsel.Dim_Currencyid_GBL,
		sod.Dim_Currencyid_STAT = subsel.Dim_Currencyid_STAT,
		sod.amt_ExchangeRate_STAT = subsel.amt_ExchangeRate_STAT,
		sod.Dim_Companyid = subsel.Dim_Companyid,
        sod.Dim_SalesDivisionid = subsel.Dim_SalesDivisionid,
		sod.Dim_ShipReceivePointid = subsel.Dim_ShipReceivePointid,
        sod.Dim_DocumentCategoryid = subsel.Dim_DocumentCategoryid,
		sod.Dim_SalesDocumentTypeid = subsel.Dim_SalesDocumentTypeid,
        sod.Dim_SalesOrgid = subsel.Dim_SalesOrgid,
		sod.Dim_SalesGroupid = subsel.Dim_SalesGroupid,
        sod.Dim_CostCenterid = subsel.Dim_CostCenterid,
		sod.Dim_BillingBlockid = subsel.Dim_BillingBlockid,
        sod.Dim_TransactionGroupid = subsel.Dim_TransactionGroupid,
		sod.dim_purchaseordertypeid = subsel.dim_purchaseordertypeid,
		sod.Dim_CustomeridSoldTo = CASE sod.Dim_CustomeridSoldTo
              WHEN 1 THEN subsel.Dim_CustomerID
              ELSE sod.Dim_CustomeridSoldTo
            END,
        sod.Dim_CustomerGroup1id = subsel.Dim_CustomerGroup1id,
		sod.Dim_CustomerGroup2id = subsel.Dim_CustomerGroup2id,
        sod.dim_salesorderitemcategoryid = subsel.dim_salesorderitemcategoryid,
        sod.Dim_ScheduleLineCategoryId = subsel.Dim_ScheduleLineCategoryId;

update fact_salesorderdelivery f
set f.ct_leadtime = ifnull(pt.leadtime,0)
from dim_part pt, fact_salesorderdelivery f
where f.dim_partid = pt.dim_partid
and f.ct_leadtime <> ifnull(pt.leadtime,0);

update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup4Id = so.Dim_MaterialPriceGroup4Id
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup4Id,1) <> so.Dim_MaterialPriceGroup4Id;

update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup5Id = so.Dim_MaterialPriceGroup5Id
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup5Id,1) <> so.Dim_MaterialPriceGroup5Id;



/*
UPDATE fact_salesorderdelivery sd
    SET sd.amt_ExchangeRate = f.amt_ExchangeRate,
        sd.amt_ExchangeRate_GBL = f.amt_ExchangeRate_GBL
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET
        sd.Dim_DateidSalesOrderCreated = f.Dim_DateidSalesOrderCreated,
        sd.Dim_DateidSchedDeliveryReq = f.Dim_DateidSchedDeliveryReq
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_DateidSchedDlvrReqPrev = f.Dim_DateidSchedDlvrReqPrev,
        sd.Dim_DateidMatlAvailOriginal = f.Dim_DateidMtrlAvail
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_DateidFirstDate = f.Dim_DateidFirstDate,
        sd.Dim_Currencyid = f.Dim_Currencyid
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_TRA = f.Dim_Currencyid_TRA
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_GBL = f.Dim_Currencyid_GBL
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_STAT = f.Dim_Currencyid_STAT
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.amt_ExchangeRate_STAT = f.amt_ExchangeRate_STAT
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo

UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Companyid = f.Dim_Companyid,
        sd.Dim_SalesDivisionid = f.Dim_SalesDivisionid
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_ShipReceivePointid = f.Dim_ShipReceivePointid,
        sd.Dim_DocumentCategoryid = f.Dim_DocumentCategoryid

From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo

update fact_salesorderdelivery f
set f.ct_leadtime = ifnull(pt.leadtime,0)
from dim_part pt, fact_salesorderdelivery f
where f.dim_partid = pt.dim_partid
and f.ct_leadtime <> ifnull(pt.leadtime,0)

UPDATE fact_salesorderdelivery sd
    SET sd.Dim_SalesDocumentTypeid = f.Dim_SalesDocumentTypeid,
        sd.Dim_SalesOrgid = f.Dim_SalesOrgid
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_SalesGroupid = f.Dim_SalesGroupid,
        sd.Dim_CostCenterid = f.Dim_CostCenterid
From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_BillingBlockid = f.Dim_BillingBlockid,
        sd.Dim_TransactionGroupid = f.Dim_TransactionGroupid

From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo

UPDATE fact_salesorderdelivery sd
    SET sd.dim_purchaseordertypeid = f.dim_purchaseordertypeid

From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_CustomeridSoldTo =
            CASE sd.Dim_CustomeridSoldTo
              WHEN 1 THEN f.Dim_CustomerID
              ELSE sd.Dim_CustomeridSoldTo
            END,
        sd.Dim_CustomerGroup1id = f.Dim_CustomerGroup1id

From fact_salesorder f, VBAK_VBAP_VBEP v,fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo


update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup4Id = so.Dim_MaterialPriceGroup4Id
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup4Id,1) <> so.Dim_MaterialPriceGroup4Id

update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup5Id = so.Dim_MaterialPriceGroup5Id
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup5Id,1) <> so.Dim_MaterialPriceGroup5Id

UPDATE fact_salesorderdelivery sd
    SET sd.Dim_CustomerGroup2id = f.Dim_CustomerGroup2id,
        sd.dim_salesorderitemcategoryid = f.dim_salesorderitemcategoryid,
        sd.Dim_ScheduleLineCategoryId = f.Dim_ScheduleLineCategoryId
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
  WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
		
		
/*END optimization 1 - 28.10 ALIN GHEORGHE*/

/* Start Changes 12 Feb 2014 */
UPDATE fact_salesorderdelivery sod
 set sod.dim_scheduledeliveryblockid = so.dim_scheduledeliveryblockid
FROM  fact_salesorder so, fact_salesorderdelivery sod
 WHERE sod.dd_Salesdocno = so.dd_SalesDocNo
 and sod.dd_SalesItemNo = so.dd_SalesItemNo
 and sod.dd_ScheduleNo = so.dd_ScheduleNo
 and ifnull(sod.dim_scheduledeliveryblockid,-1) <> ifnull(so.dim_scheduledeliveryblockid,-2);

UPDATE fact_salesorderdelivery sod
SET sod.dim_scheduledeliveryblockid = 1
WHERE sod.dim_scheduledeliveryblockid is NULL;

/* End Changes 12 Feb 2014 */

/* Start Changes 14 Feb 2014 */

UPDATE fact_salesorderdelivery sod
SET sod.Dim_CustomerGroup4id = ifnull(so.Dim_CustomerGroup4id,1)
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.Dim_CustomerGroup4id,-1) <> ifnull(so.Dim_CustomerGroup4id,-2);

update fact_salesorderdelivery
set Dim_CustomerGroup4id = 1
where Dim_CustomerGroup4id is NULL;

/* END Changes 14 Feb 2014 */

UPDATE fact_salesorderdelivery sod
set dd_trackingNo = dd_BillofLading
where ifnull(dd_trackingNo,'Not Set') <> dd_BillofLading;

UPDATE fact_salesorderdelivery sod
set dd_trackingNo = 'Not Set'
where dd_trackingNo is null;
/* Start Changes 02 May 2014 */

update fact_salesorderdelivery set dd_BusinessCustomerPONo = 'Not Set' where dd_BusinessCustomerPONo is NULL;

UPDATE fact_salesorderdelivery sod
SET sod.dd_BusinessCustomerPONo = ifnull(so.dd_BusinessCustomerPONo,'Not Set')
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.dd_BusinessCustomerPONo,'Not Set') <> ifnull(so.dd_BusinessCustomerPONo,'Not Set');

/* END Changes 2 May 2014 */

/* Suchithra Start Changes 17 Nov 2014*/
/* Fix for Issue_Sales Order Reason Code not populating_Added the Update statement to set the Sales Doc Order Reason ID   */
update fact_salesorderdelivery set dim_salesdocorderreasonid = 1 where dim_salesdocorderreasonid is NULL;

UPDATE fact_salesorderdelivery sod
SET sod.dim_salesdocorderreasonid = ifnull(so.dim_salesdocorderreasonid,1)
FROM fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.dim_salesdocorderreasonid,-1) <> ifnull(so.dim_salesdocorderreasonid,-2);
/* Suchithra End Changes 17 Nov 2014*/

/* Ensure subsequent items have the correct Dim_CustomeridShipTo*/
/*DROP TABLE IF EXISTS fact_salesorder_tmp_Dim_CustomeridShipToFix
CREATE TABLE fact_salesorder_tmp_Dim_CustomeridShipToFix
AS
SELECT DISTINCT f1.dd_salesdocno,f1.dd_salesitemno,f1.Dim_CustomeridShipTo,f2.Dim_CustomeridShipTo new_Dim_CustomeridShipTo
FROM fact_salesorder f1, fact_salesorder f2
WHERE f1.dd_salesdocno = f2.dd_salesdocno
AND f1.Dim_CustomeridShipTo = 1
AND f2.Dim_CustomeridShipTo <> 1*/

/*UPDATE fact_salesorder f
SET f.Dim_CustomeridShipTo =  b.new_Dim_CustomeridShipTo
FROM fact_salesorder_tmp_Dim_CustomeridShipToFix b,fact_salesorder f
WHERE f.dd_salesdocno = b.dd_salesdocno
AND f.dd_salesitemno = b.dd_salesitemno
AND f.Dim_CustomeridShipTo = 1
AND  b.new_Dim_CustomeridShipTo <> 1
AND f.Dim_CustomeridShipTo <> b.new_Dim_CustomeridShipTo*/


DROP TABLE IF EXISTS var_pShipToPartyPartnerFunction;
CREATE TABLE var_pShipToPartyPartnerFunction
(
pShipToPartyPartnerFunction varchar(5)
);
INSERT INTO var_pShipToPartyPartnerFunction values ('WE');


UPDATE fact_salesorder so
   SET so.Dim_CustomeridShipTo = ifnull(c.Dim_CustomerId,1)
  FROM vbpa sdp, Dim_Customer c,var_pShipToPartyPartnerFunction, fact_salesorder so
 WHERE     sdp.vbpa_parvw = pShipToPartyPartnerFunction
       AND so.dd_SalesDocNo = sdp.vbpa_vbeln
       AND sdp.vbpa_posnr = 0
       AND c.CustomerNumber = sdp.vbpa_kunnr
       AND c.RowIsCurrent = 1
       AND ifnull(so.Dim_CustomeridShipTo, -1) <> ifnull(c.Dim_CustomerId,1);

UPDATE fact_salesorder so
   SET so.Dim_CustomeridShipTo = ifnull(c.Dim_CustomerId,1)
  FROM vbpa sdp, Dim_Customer c, var_pShipToPartyPartnerFunction,fact_salesorder so
 WHERE     sdp.vbpa_parvw = pShipToPartyPartnerFunction
       AND so.dd_SalesDocNo = sdp.vbpa_vbeln
       AND so.dd_Salesitemno = sdp.vbpa_posnr
       AND c.CustomerNumber = sdp.vbpa_kunnr
       AND c.RowIsCurrent = 1
       AND ifnull(so.Dim_CustomeridShipTo, -1) <> ifnull(c.Dim_CustomerId,1);


/* BI-309 */
merge into fact_salesorderdelivery sod
using (select distinct fact_salesorderdeliveryid,sc.dim_shippingconditionid
from  fact_salesorderdelivery sod
inner join likp_lips l on sod.dd_SalesDlvrDocNo = LIKP_VBELN AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
inner join knvv v on l.LIPS_VTWEG  =  v.KNVV_VTWEG AND l.LIPS_SPART  =  v.KNVV_SPART AND l.LIKP_vkorg  =   v.KNVV_VKORG AND l.LIKP_KUNNR =   v.KNVV_KUNNR
left join (select * from dim_shippingcondition s
					where s.rowiscurrent = 1) sc on sc.shippingconditioncode = KNVV_VSBED
where
dim_defaultshippingconditionid <> ifnull(sc.dim_shippingconditionid,1)) t
on t.fact_salesorderdeliveryid=sod.fact_salesorderdeliveryid
when matched then update set dim_defaultshippingconditionid = ifnull(t.dim_shippingconditionid,1);


merge into fact_salesorderdelivery sod
using (select distinct fact_salesorderdeliveryid,tst.dim_tranportbyshippingtypeid
 FROM fact_salesorderdelivery sod
 inner join LIKP_LIPS l on sod.dd_SalesDlvrDocNo = LIKP_VBELN AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
 inner join tvrot t on LIKP_ROUTE = t.tvrot_route
 LEFT JOIN (select * from dim_tranportbyshippingtype s) tst on tst.ShippingType = TVRO_VSART
WHERE
sod.dim_tranportbyshippingtypeid <> ifnull(tst.dim_tranportbyshippingtypeid, 1)) t
on t.fact_salesorderdeliveryid=sod.fact_salesorderdeliveryid
when matched then update set sod.dim_tranportbyshippingtypeid = ifnull(t.dim_tranportbyshippingtypeid, 1)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/;


/*drop table if exists tmp_defaultroute_upd
create table tmp_defaultroute_upd as
select dd_SalesDlvrDocNo,f.dd_SalesDlvrItemNo, trolz_route,
row_number() over (partition by dd_SalesDlvrDocNo,f.dd_SalesDlvrItemNo) as rowseqno
from fact_salesorderdelivery f, dim_shippingcondition s,
dim_shipreceivepoint r, dim_customer c, TROLZ t, dim_part pt, tvst v
where f.dim_defaultshippingconditionid = s.dim_shippingconditionid
and r.dim_shipreceivepointid = f.Dim_defaultShipReceivePointid
and f.Dim_CustomeridShipTo = c.dim_customerid
and f.dim_partid = pt.dim_partid
and s.rowiscurrent = 1 and c.rowiscurrent = 1
and pt.rowiscurrent = 1
and v.tvst_vstel = r.ShipReceivePointCode
and ifnull(v.tvst_aland, 'Not Set') = ifnull(trolz_aland, 'Not Set')
and ifnull(v.tvst_azone, 'Not Set') = ifnull(trolz_azone, 'Not Set')
and s.ShippingConditionCode = TROLZ_VSBED
and ifnull(TROLZ_LLAND,'Not Set') = ifnull(c.country,'Not Set')
and pt.TransportationGroup = TROLZ_TRAGR
--and trolz_grulg = dd_weightgroup
*/

update TROLZ_HISTORY a
set end_date = current_date
where exists (select 1 from TROLZ b
where ifnull(a.trolz_aland,'Not Set') = ifnull(b.trolz_aland,'Not Set')
and ifnull(a.trolz_azone,'Not Set') = ifnull(b.trolz_azone,'Not Set')
and ifnull(a.trolz_vsbed,'Not Set') = ifnull(b.trolz_vsbed,'Not Set')
and ifnull(a.trolz_tragr,'Not Set') = ifnull(b.trolz_tragr,'Not Set')
and ifnull(a.trolz_lland,'Not Set') = ifnull(b.trolz_lland,'Not Set')
and ifnull(a.trolz_lzone,'Not Set') = ifnull(b.trolz_lzone,'Not Set')
and ifnull(a.trolz_grulg,'Not Set') = ifnull(b.trolz_grulg,'Not Set')
and a.trolz_route <> b.trolz_route)
and end_date = '2050-12-31';

insert into TROLZ_HISTORY
select ifnull(a.trolz_aland,'Not Set') trolz_aland,
       ifnull(a.trolz_azone,'Not Set') trolz_azone,
       ifnull(a.trolz_vsbed,'Not Set') trolz_vsbed,
       ifnull(a.trolz_tragr,'Not Set') trolz_tragr,
       ifnull(a.trolz_lland,'Not Set') trolz_lland,
       ifnull(a.trolz_lzone,'Not Set') trolz_lzone,
       ifnull(a.trolz_grulg,'Not Set') trolz_grulg,
       ifnull(a.trolz_route,'Not Set') trolz_route,
       '2050-12-31' as end_date
        from TROLZ a
where not exists  (select 1 from TROLZ_HISTORY b
where ifnull(a.trolz_aland,'Not Set') = ifnull(b.trolz_aland,'Not Set')
and ifnull(a.trolz_azone,'Not Set') = ifnull(b.trolz_azone,'Not Set')
and ifnull(a.trolz_vsbed,'Not Set') = ifnull(b.trolz_vsbed,'Not Set')
and ifnull(a.trolz_tragr,'Not Set') = ifnull(b.trolz_tragr,'Not Set')
and ifnull(a.trolz_lland,'Not Set') = ifnull(b.trolz_lland,'Not Set')
and ifnull(a.trolz_lzone,'Not Set') = ifnull(b.trolz_lzone,'Not Set')
and ifnull(a.trolz_grulg,'Not Set') = ifnull(b.trolz_grulg,'Not Set')
AND ifnull(a.trolz_route,'Not Set') = ifnull(b.trolz_route,'Not Set') );

drop table if exists tmp_defaultroute_upd1;
create table tmp_defaultroute_upd1 as
select distinct dd_SalesDlvrDocNo,f.dd_SalesDlvrItemNo,
ShipReceivePointCode , ShippingConditionCode, c.country , pt.TransportationGroup ,f.Dim_DateidActualGoodsIssue
from fact_salesorderdelivery f, dim_shippingcondition s,
dim_shipreceivepoint r, dim_customer c, dim_part pt
where f.dim_defaultshippingconditionid = s.dim_shippingconditionid
and r.dim_shipreceivepointid = f.Dim_defaultShipReceivePointid
and f.Dim_CustomeridShipTo = c.dim_customerid
and f.dim_partid = pt.dim_partid
and s.rowiscurrent = 1 and c.rowiscurrent = 1
and pt.rowiscurrent = 1;

drop table if exists tmp_defaultroute_upd2;
create table tmp_defaultroute_upd2 as
select dd_SalesDlvrDocNo,dd_SalesDlvrItemNo, trolz_route,
row_number() over (partition by dd_SalesDlvrDocNo,f.dd_SalesDlvrItemNo order by end_date) as rowseqno
from tmp_defaultroute_upd1 f, TROLZ_HISTORY t, tvst v, dim_date dd
where v.tvst_vstel = f.ShipReceivePointCode
and ifnull(v.tvst_aland, 'Not Set') = ifnull(trolz_aland, 'Not Set')
and ifnull(v.tvst_azone, 'Not Set') = ifnull(trolz_azone, 'Not Set')
and ifnull(f.ShippingConditionCode, 'Not Set') = ifnull(TROLZ_VSBED,'Not Set')
and ifnull(TROLZ_LLAND,'Not Set') = ifnull(f.country,'Not Set')
and ifnull(f.TransportationGroup,'Not Set') = ifnull(TROLZ_TRAGR,'Not Set')
and f.Dim_DateidActualGoodsIssue = dd.dim_dateid
and case when dd.datevalue = '0001-01-01' then current_date else datevalue end < end_date ;



UPDATE fact_salesorderdelivery sod
SET 	dim_defaultrouteid =	ifnull(ro.dim_Routeid,1)

FROM fact_salesorderdelivery sod
INNER JOIN tmp_defaultroute_upd2 u on sod.dd_SalesDlvrDocNo = u.dd_SalesDlvrDocNo AND sod.dd_SalesDlvrItemNo = u.dd_SalesDlvrItemNo
left join (select * from dim_Route r
                       WHERE  r.RowIsCurrent = 1) ro on ro.RouteCode = u.TROLZ_ROUTE
WHERE
rowseqno = 1
and dim_defaultrouteid <> ifnull(ro.dim_Routeid,1);

drop table if exists tmp_defaultroute_upd1;
drop table if exists tmp_defaultroute_upd2;

UPDATE fact_salesorderdelivery sod
SET sod.dim_tranportbyshippingtypeid = ifnull(tbst.dim_tranportbyshippingtypeid,1)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM fact_salesorderdelivery sod
  INNER JOIN LIKP_LIPS l on sod.dd_SalesDlvrDocNo = LIKP_VBELN AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
  INNER JOIN tvrot t on LIKP_ROUTE = t.tvrot_route
  LEFT JOIN (select * from dim_tranportbyshippingtype s ) tbst on tbst.ShippingType = TVRO_VSART
WHERE
sod.dim_tranportbyshippingtypeid <> ifnull(tbst.dim_tranportbyshippingtypeid,1);

/*new custom measure ct_durationatdci*/
drop table if exists tmp_durationdci;
create table tmp_durationdci as
select fact_salesorderdeliveryid,
       dim_companyid,
       dd_salesdlvrdocno,
       dd_salesdlvritemno,
       dd_salesdocno,
       customernumber,
       dd_batch,partnumber,
       documentcategory,
       documenttype,
       order_date,
       actual_goods_issue_date,
       convert(date,FROM_POSIX_TIME(a.RETURN_ORDER)) as return_order_date,
       latest_order_date,
       case when convert(date,FROM_POSIX_TIME(a.RETURN_ORDER))-a.latest_Order_Date<0 then 0
                                                 else convert(date,FROM_POSIX_TIME(a.RETURN_ORDER))-a.latest_Order_Date end as ct_durationatdci from
(select fact_salesorderdeliveryid,
        dim_companyid,
        dd_salesdlvrdocno,
        dd_salesdlvritemno,
        dd_salesdocno,
        customernumber,
        dd_batch,
        partnumber,
        documentcategory,
        documenttype,
        g.datevalue order_date,
        h.datevalue actual_goods_issue_date,
        avg(case when documentcategory='H' then posix_time(g.datevalue) else null end) over (partition by customernumber,partnumber,dd_batch) as RETURN_ORDER,
     /*   max(case when documenttype='ZSO' then h.datevalue else null end)  over (partition by customernumber,partnumber,dd_batch) as latest_order_Date */
       max(case when documentcategory='C' then h.datevalue else null end)  over (partition by customernumber,partnumber,dd_batch) as latest_order_Date
 from fact_Salesorderdelivery f,
      dim_part pt ,
      dim_customer c,
      dim_documentcategory d,
      dim_salesdocumenttype e,
      dim_date g,
      dim_date h
where f.dim_partid = pt.dim_partid
and f.Dim_CustomeridSoldTo = c.dim_customerid
and f.dim_documentcategoryid = d.dim_documentcategoryid
and f.dim_salesdocumenttypeid = e.dim_salesdocumenttypeid
and f.dim_dateidsalesordercreated = g.dim_dateid
and f.dim_dateidactualgi_original = h.dim_dateid ) a;


update fact_Salesorderdelivery a
set a.ct_durationatdci = b.ct_durationatdci
from tmp_durationdci b,  fact_Salesorderdelivery a
where a.fact_salesorderdeliveryid = b.fact_salesorderdeliveryid
and a.ct_durationatdci <> b.ct_durationatdci;


update fact_Salesorderdelivery a
set a.dim_dateidreturnorder = c.dim_dateid
from tmp_durationdci b, dim_date c, dim_company d, fact_Salesorderdelivery a
where a.fact_salesorderdeliveryid = b.fact_salesorderdeliveryid
  and ifnull(b.return_order_date,'0001-01-01') = c.datevalue
  and c.companycode = d.companycode
  and c.plantcode_factory = 'Not Set'
  and b.dim_companyid = d.dim_companyid
  and a.dim_dateidreturnorder <> c.dim_dateid;

  update fact_Salesorderdelivery a
set a.dim_dateidlatestorder = c.dim_dateid
from tmp_durationdci b, dim_date c, dim_company d, fact_Salesorderdelivery a
where a.fact_salesorderdeliveryid = b.fact_salesorderdeliveryid
  and ifnull(b.latest_order_date,'0001-01-01') = c.datevalue
  and c.companycode = d.companycode
  and c.plantcode_factory = 'Not Set'
  and b.dim_companyid = d.dim_companyid
  and a.dim_dateidlatestorder <> c.dim_dateid;

drop table if exists tmp_durationdci;
DROP TABLE IF EXISTS tmp_actualreceipt_sod;
CREATE TABLE tmp_actualreceipt_sod
AS
SELECT DISTINCT so.dd_SalesDocNo,so.dd_SalesItemNo,
MAX(e.ekbe_budat) ActReceiptDate_Max
FROM fact_salesorder so, fact_purchase fp, EKBE_DELIV e, dim_date salesdt
WHERE ifnull(so.dd_CustomerPONo,'Not Set') = ifnull(fp.dd_documentno,'Not Set')
AND fp.dd_DocumentNo = ekbe_ebeln
AND fp.dd_DocumentItemNo = ekbe_ebelp
AND so.dim_dateidsalesordercreated = salesdt.dim_dateid AND e.ekbe_budat > salesdt.datevalue
GROUP BY so.dd_SalesDocNo,so.dd_SalesItemNo;

UPDATE fact_salesorderdelivery sod
SET sod.dim_dateidactualreceipt = d.dim_dateid
FROM tmp_actualreceipt_sod t, dim_date d, dim_company dc, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = t.dd_SalesDocNo
AND sod.dd_SalesItemNo = t.dd_SalesItemNo
AND sod.dim_companyid = dc.dim_companyid
AND t.ActReceiptDate_Max = d.datevalue and d.companycode = dc.companycode
and d.plantcode_factory = 'Not Set'
AND sod.dim_dateidactualreceipt <> d.dim_dateid;

/* Madalina 3 Oct 2016 - Also use Material and Batch number when linking Shipping to Sales and Purchasing - BI-4288 */
DROP TABLE IF EXISTS tmp_actualreceipt_sod;
CREATE TABLE tmp_actualreceipt_sod
AS
SELECT DISTINCT so.dd_SalesDocNo,so.dd_SalesItemNo, sdp.partnumber, sb.batchnumber, ifnull(so.dd_CustomerPONo,'Not Set') as dd_CustomerPONo,
first_value(e.ekbe_budat) over (partition by so.dd_SalesDocNo,so.dd_SalesItemNo order by e.ekbe_budat desc) ActReceiptDate_Max
FROM fact_salesorder so, fact_purchase fp, EKBE_DELIV e, dim_date salesdt, dim_part sdp, dim_batch sb, dim_purchasemisc pm, dim_part pdp, dim_batch pb
WHERE ifnull(so.dd_CustomerPONo,'Not Set') = ifnull(fp.dd_documentno,'Not Set')
AND fp.dd_DocumentNo = ekbe_ebeln
AND fp.dd_DocumentItemNo = ekbe_ebelp
AND so.dim_dateidsalesordercreated = salesdt.dim_dateid AND e.ekbe_budat > salesdt.datevalue
AND fp.dim_partid = pdp.dim_partid
AND fp.dim_batchid = pb.dim_batchid
AND so.dim_partid = sdp.dim_partid
AND so.dim_batchid = sb.dim_batchid
AND pdp.partnumber = sdp.partnumber
AND pb.batchnumber = sb.batchnumber
AND fp.dim_purchasemiscid = pm.dim_purchasemiscid
and ifnull(pm.ItemReturn, 'Not Set') <> 'X';

UPDATE fact_salesorderdelivery sod
SET sod.dim_dateidactualreceipt = d.dim_dateid
FROM tmp_actualreceipt_sod t, dim_date d, dim_company dc, fact_salesorderdelivery sod, dim_part dp, dim_batch b
WHERE sod.dd_SalesDocNo = t.dd_SalesDocNo
AND sod.dd_SalesItemNo = t.dd_SalesItemNo
AND sod.dim_companyid = dc.dim_companyid
AND t.ActReceiptDate_Max = d.datevalue and d.companycode = dc.companycode
and d.plantcode_factory = 'Not Set'
AND sod.dim_partid = dp.dim_partid
AND sod.dim_batchid = b.dim_batchid
AND dp.partnumber = t.partnumber
AND b.batchnumber = t.batchnumber
AND sod.dim_dateidactualreceipt <> d.dim_dateid;
/* END BI-4288 */

insert into stg_deletion_purchase_doc
select EKPO_EBELN,EKPO_EBELP,EKPO_AEDAT from EKKO_EKPO_EKET a
where not exists (SELECT 1 from stg_deletion_purchase_doc b
where a.EKPO_EBELN = b.EKPO_EBELN
and a.EKPO_EBELP = b.EKPO_EBELP
and a.EKPO_AEDAT = b.EKPO_AEDAT)
and EKPO_LOEKZ = 'L';



DROP TABLE IF EXISTS tmp_actualreceipt_deletionind;
CREATE TABLE tmp_actualreceipt_deletionind
AS
SELECT DISTINCT so.dd_SalesDocNo,so.dd_SalesItemNo,
max(EKPO_AEDAT) change_date
FROM fact_salesorder so, stg_deletion_purchase_doc e, dim_date salesdt
WHERE ifnull(so.dd_CustomerPONo,'Not Set') = ifnull(e.EKPO_EBELN,'Not Set')
AND so.dim_dateidsalesordercreated = salesdt.dim_dateid AND e.EKPO_AEDAT > salesdt.datevalue
GROUP by so.dd_SalesDocNo,so.dd_SalesItemNo;


UPDATE fact_salesorderdelivery sod
SET sod.dim_dateidactualreceipt = d.dim_dateid
FROM tmp_actualreceipt_deletionind t, dim_date d, dim_company dc, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = t.dd_SalesDocNo
AND sod.dd_SalesItemNo = t.dd_SalesItemNo
AND sod.dim_companyid = dc.dim_companyid
AND t.change_date - 3 = d.datevalue and d.companycode = dc.companycode
and d.plantcode_factory = 'Not Set'
AND sod.dim_dateidactualreceipt <> d.dim_dateid;

DROP TABLE IF EXISTS tmp_actualreceipt_deletionind;

UPDATE dim_plant pl
SET pl.flag_atlaslite_plant = 'ATLAS LITE'
FROM Z1SD_ISD_INSCOPE z, dim_plant pl
WHERE pl.plantcode = z.Z1SD_ISD_INSCOPE_WERKS
AND z.Z1SD_ISD_INSCOPE_INS_SALES = 'L'
AND pl.flag_atlaslite_plant <> 'ATLAS LITE';

/* Get distinct PO Doc No and UDate */
/* Pick up the minimum date after salesordercreation date */
DROP TABLE IF EXISTS tmp_upd_proxyfordd_po;
CREATE TABLE tmp_upd_proxyfordd_po
AS
SELECT so.dd_SalesDocNo,so.dd_SalesItemNo,min(hdr.CDHDR_UDATE) ProxyForDelivDate_Min
FROM fact_purchase fp, CDPOS_PO pos,CDHDR_PO hdr, fact_salesorder so --, dim_date salesdt
WHERE ifnull(so.dd_CustomerPONo,'Not Set') = ifnull(fp.dd_documentno,'Not Set')
AND fp.dd_documentno = CDHDR_OBJECTID
AND CDHDR_OBJECTID = CDPOS_OBJECTID and CDHDR_CHANGENR = CDPOS_CHANGENR
AND CDPOS_FNAME = 'LOEKZ'
and CDPOS_TABNAME = 'EKPO'
--AND so.dim_dateidsalesordercreated = salesdt.dim_dateid AND hdr.CDHDR_UDATE > salesdt.datevalue
GROUP BY so.dd_SalesDocNo,so.dd_SalesItemNo;

UPDATE fact_salesorderdelivery sod
SET sod.dim_dateidproxyfordeliverydate = d.dim_dateid
FROM tmp_upd_proxyfordd_po t, dim_date d, dim_company dc, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = t.dd_SalesDocNo
AND sod.dd_SalesItemNo = t.dd_SalesItemNo
AND sod.dim_companyid = dc.dim_companyid
AND t.ProxyForDelivDate_Min = d.datevalue and d.companycode = dc.companycode
and d.plantcode_factory = 'Not Set'
AND sod.dim_dateidproxyfordeliverydate <> d.dim_dateid;

UPDATE fact_salesorderdelivery sod
SET dim_dateiddeliverydate = sod.dim_dateidproxyfordeliverydate
WHERE sod.dim_dateidproxyfordeliverydate <> 1;
/* end Actual Receipt Date */

/* Octavian: Every Angle Transition Addons */
/*START OPTIMIZATION 2 - ALIN GHEORGHE*/
merge into fact_salesorderdelivery fsod using
(select 
fso.dd_SalesDocNo,
fso.dd_SalesItemNo,
fso.dd_ScheduleNo,
fso.amt_grossweight_marm,
fso.dim_materiallistingid,
fso.dd_numerator,
fso.dim_dateidpricing,
fso.dim_dateidorginalrequestdate,
fso.dim_customerpurchaseordertypeid,
fso.dim_datecustomerpurchaseordertypeid,
fso.dd_purchordernumbershiptoparty,
fso.dd_urreference,
fso.dd_incoterms2,
fso.dim_CustomerGroup_vbkd_ID,
fso.ct_additionaldays,
fso.dim_tranportbyshippingtype_vbkd_id,
fso.Dim_CustomerPaymentTerms_vbkd_Id,
fso.dd_term_product_proposal,
fso.dd_referencedocno_xblnr,
fso.dd_shorttext,
fso.dd_requirementstypes,
fso.dd_originatingitem,
fso.dd_originatingdoc,
fso.dim_original_req_dateid,
fso.ct_original_req,
fso.dim_first_promised_dateid,
fso.dim_baseunitofmeasureid,
fso.ct_first_promised__req,
fso.ct_CumOrderQty,
fso.dim_dateidvalidto_leadtime,
fso.dim_dateidvalidstartdateid_kotg992,
fso.dim_dateidvalidenddateid_kotg992,
fso.dim_customergroupid_kotg992,
fso.dim_distributionchanelcodeid_kotg992,
fso.dim_salesorgid_a055,
fso.dd_matnrbycustomer,
fso.dd_matnrdescripbycustomer,
fso.dim_DateidExpiryDate
from fact_salesorder fso) subs
on subs.dd_SalesDocNo = fsod.dd_SalesDocNo
AND subs.dd_SalesItemNo = fsod.dd_SalesItemNo
AND subs.dd_ScheduleNo = fsod.dd_ScheduleNo
when matched then update
set
fsod.amt_grossweight_marm					  = subs.amt_grossweight_marm,
fsod.dim_materiallistingid                    = subs.dim_materiallistingid,
fsod.dd_numerator                             = subs.dd_numerator,
fsod.dim_dateidpricing                        = subs.dim_dateidpricing,
fsod.dim_dateidorginalrequestdate             = subs.dim_dateidorginalrequestdate,
fsod.dim_customerpurchaseordertypeid          = subs.dim_customerpurchaseordertypeid,
fsod.dim_datecustomerpurchaseordertypeid      = subs.dim_datecustomerpurchaseordertypeid,
fsod.dd_purchordernumbershiptoparty           = subs.dd_purchordernumbershiptoparty,
fsod.dd_urreference                           = subs.dd_urreference,
fsod.dd_incoterms2                            = subs.dd_incoterms2,
fsod.dim_CustomerGroup_vbkd_ID                = subs.dim_CustomerGroup_vbkd_ID,
fsod.ct_additionaldays                        = subs.ct_additionaldays,
fsod.dim_tranportbyshippingtype_vbkd_id       = subs.dim_tranportbyshippingtype_vbkd_id,
fsod.Dim_CustomerPaymentTerms_vbkd_Id         = subs.Dim_CustomerPaymentTerms_vbkd_Id,
fsod.dd_term_product_proposal                 = subs.dd_term_product_proposal,
fsod.dd_referencedocno_xblnr                  = subs.dd_referencedocno_xblnr,
fsod.dd_shorttext                             = subs.dd_shorttext,
fsod.dd_requirementstypes                     = subs.dd_requirementstypes,
fsod.dd_originatingitem                       = subs.dd_originatingitem,
fsod.dd_originatingdoc                        = subs.dd_originatingdoc,
fsod.dim_original_req_dateid                  = subs.dim_original_req_dateid,
fsod.ct_original_req                          = subs.ct_original_req,
fsod.dim_first_promised_dateid                = subs.dim_first_promised_dateid,
fsod.dim_baseunitofmeasureid                  = subs.dim_baseunitofmeasureid,
fsod.ct_first_promised__req                   = subs.ct_first_promised__req,
fsod.ct_CumOrderQty                           = subs.ct_CumOrderQty,
fsod.dim_dateidvalidto_leadtime               = subs.dim_dateidvalidto_leadtime,
fsod.dim_dateidvalidstartdateid_kotg992       = subs.dim_dateidvalidstartdateid_kotg992,
fsod.dim_dateidvalidenddateid_kotg992         = subs.dim_dateidvalidenddateid_kotg992,
fsod.dim_customergroupid_kotg992              = subs.dim_customergroupid_kotg992,
fsod.dim_distributionchanelcodeid_kotg992     = subs.dim_distributionchanelcodeid_kotg992,
fsod.dim_salesorgid_a055                      = subs.dim_salesorgid_a055,
fsod.dd_matnrbycustomer                       = subs.dd_matnrbycustomer,
fsod.dd_matnrdescripbycustomer                = subs.dd_matnrdescripbycustomer,
fsod.dim_DateidExpiryDate	                  = subs.dim_DateidExpiryDate,
fsod.dw_update_date 						  = current_timestamp;


UPDATE fact_salesorderdelivery f
SET dd_batch = ifnull(LIPS_CHARG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_batch <> ifnull(LIPS_CHARG,'Not Set');

/* This is to ensure that if the value is changed to null, it will be updated for dim_batchid as well */
/* Madalina 19 Aug 2016 - Reset of dim_batchid is handled in dim_batch dimension script - BI-3822 */
/* update fact_salesorderdelivery ia
SET dim_batchid = 1
WHERE dim_batchid <> 1 */

update fact_salesorderdelivery ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp, fact_salesorderdelivery ia
where ia.dim_partid = dp.dim_partid and
ia.dd_batch = b.batchnumber and
dp.partnumber = b.partnumber and
dp.plant = b.plantcode
and ia.dim_batchid <> b.dim_batchid;

update fact_salesorderdelivery ia
SET dim_batchid = 1
where dd_batch = 'Not Set';
/* Octavian: Every Angle Transition Addons */
/*
update fact_salesorderdelivery sd
SET sd.amt_grossweight_marm = fso.amt_grossweight_marm
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	/*
FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.amt_grossweight_marm <> fso.amt_grossweight_marm

UPDATE fact_salesorderdelivery f
SET dd_batch = ifnull(LIPS_CHARG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_batch <> ifnull(LIPS_CHARG,'Not Set')

/* This is to ensure that if the value is changed to null, it will be updated for dim_batchid as well */
/* Madalina 19 Aug 2016 - Reset of dim_batchid is handled in dim_batch dimension script - BI-3822 */
/* update fact_salesorderdelivery ia
SET dim_batchid = 1
WHERE dim_batchid <> 1 */
/*
update fact_salesorderdelivery ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp, fact_salesorderdelivery ia
where ia.dim_partid = dp.dim_partid and
ia.dd_batch = b.batchnumber and
dp.partnumber = b.partnumber and
dp.plant = b.plantcode
and ia.dim_batchid <> b.dim_batchid

update fact_salesorderdelivery ia
SET dim_batchid = 1
where dd_batch = 'Not Set'

update fact_salesorderdelivery sd
SET sd.dim_materiallistingid = fso.dim_materiallistingid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	/*
FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_materiallistingid <> fso.dim_materiallistingid

update fact_salesorderdelivery sd
SET sd.dd_numerator = fso.dd_numerator
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_numerator <> fso.dd_numerator

update fact_salesorderdelivery sd
SET sd.dim_dateidpricing = fso.dim_dateidpricing
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_dateidpricing <> fso.dim_dateidpricing

update fact_salesorderdelivery sd
SET sd.dim_dateidorginalrequestdate = fso.dim_dateidorginalrequestdate
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_dateidorginalrequestdate <> fso.dim_dateidorginalrequestdate
/* Octavian: Every Angle Transition Addons */

/* Octavian: Every Angle Transition part 2 */
/*update fact_salesorderdelivery sd
SET sd.dim_customerpurchaseordertypeid = fso.dim_customerpurchaseordertypeid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso, fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_customerpurchaseordertypeid <> fso.dim_customerpurchaseordertypeid


update fact_salesorderdelivery sd
SET sd.dim_datecustomerpurchaseordertypeid = fso.dim_datecustomerpurchaseordertypeid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_datecustomerpurchaseordertypeid <> fso.dim_datecustomerpurchaseordertypeid

update fact_salesorderdelivery sd
SET sd.dd_purchordernumbershiptoparty = fso.dd_purchordernumbershiptoparty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_purchordernumbershiptoparty <> fso.dd_purchordernumbershiptoparty

update fact_salesorderdelivery sd
SET sd.dd_urreference = fso.dd_urreference
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_urreference <> fso.dd_urreference

update fact_salesorderdelivery sd
SET sd.dd_incoterms2 = fso.dd_incoterms2
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_incoterms2 <> fso.dd_incoterms2

update fact_salesorderdelivery sd
SET sd.dim_CustomerGroup_vbkd_ID = fso.dim_CustomerGroup_vbkd_ID
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_CustomerGroup_vbkd_ID <> fso.dim_CustomerGroup_vbkd_ID

update fact_salesorderdelivery sd
SET sd.ct_additionaldays = fso.ct_additionaldays
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.ct_additionaldays <> fso.ct_additionaldays

update fact_salesorderdelivery sd
SET sd.dim_tranportbyshippingtype_vbkd_id = fso.dim_tranportbyshippingtype_vbkd_id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_tranportbyshippingtype_vbkd_id <> fso.dim_tranportbyshippingtype_vbkd_id

update fact_salesorderdelivery sd
SET sd.Dim_CustomerPaymentTerms_vbkd_Id = fso.Dim_CustomerPaymentTerms_vbkd_Id
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.Dim_CustomerPaymentTerms_vbkd_Id <> fso.Dim_CustomerPaymentTerms_vbkd_Id


update fact_salesorderdelivery sd
SET sd.dd_term_product_proposal = fso.dd_term_product_proposal,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso, fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_term_product_proposal <> fso.dd_term_product_proposal

update fact_salesorderdelivery sd
SET sd.dd_referencedocno_xblnr = fso.dd_referencedocno_xblnr,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso, fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_referencedocno_xblnr <> fso.dd_referencedocno_xblnr

update fact_salesorderdelivery sd
SET sd.dd_shorttext = fso.dd_shorttext,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_shorttext <> fso.dd_shorttext

update fact_salesorderdelivery sd
SET sd.dd_requirementstypes = fso.dd_requirementstypes,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_requirementstypes <> fso.dd_requirementstypes

update fact_salesorderdelivery sd
SET sd.dd_originatingitem = fso.dd_originatingitem,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_originatingitem <> fso.dd_originatingitem

update fact_salesorderdelivery sd
SET sd.dd_originatingdoc = fso.dd_originatingdoc,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dd_originatingdoc <> fso.dd_originatingdoc

update fact_salesorderdelivery sd
SET sd.dim_original_req_dateid = fso.dim_original_req_dateid,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_original_req_dateid <> fso.dim_original_req_dateid


update fact_salesorderdelivery sd
SET sd.ct_original_req = fso.ct_original_req,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.ct_original_req <> fso.ct_original_req

update fact_salesorderdelivery sd
SET sd.dim_first_promised_dateid = fso.dim_first_promised_dateid,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_first_promised_dateid <> fso.dim_first_promised_dateid

/* Update 15 FEBRUARY 2016 - OSTOIAN - UPDATE FOR VBEP_MEINS */
/*
 update fact_salesorderdelivery sd
SET sd.dim_baseunitofmeasureid = fso.dim_baseunitofmeasureid
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso, fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND  sd.dim_baseunitofmeasureid <> fso.dim_baseunitofmeasureid

/* END OF CHANGES 15 FEBRUARY 2016 OSTOIAN */
/*
update fact_salesorderdelivery sd
SET sd.ct_first_promised__req = fso.ct_first_promised__req,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.ct_first_promised__req <> fso.ct_first_promised__req

update fact_salesorderdelivery sd
SET sd.ct_CumOrderQty = fso.ct_CumOrderQty,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso, fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.ct_CumOrderQty <> fso.ct_CumOrderQty

update fact_salesorderdelivery sd
SET sd.dim_dateidvalidto_leadtime = fso.dim_dateidvalidto_leadtime,
	dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
/*FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.dim_dateidvalidto_leadtime <> fso.dim_dateidvalidto_leadtime

UPDATE fact_salesorderdelivery sod
SET sod.dim_dateidvalidstartdateid_kotg992 = so.dim_dateidvalidstartdateid_kotg992
	,sod.dw_update_date = current_timestamp
   FROM	fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
   AND sod.dd_SalesItemNo = so.dd_SalesItemNo
   AND sod.dd_ScheduleNo = so.dd_ScheduleNo
   AND sod.dim_dateidvalidstartdateid_kotg992 <> so.dim_dateidvalidstartdateid_kotg992

UPDATE fact_salesorderdelivery sod
SET sod.dim_dateidvalidenddateid_kotg992 = so.dim_dateidvalidenddateid_kotg992
	,sod.dw_update_date = current_timestamp
   FROM	fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
   AND sod.dd_SalesItemNo = so.dd_SalesItemNo
   AND sod.dd_ScheduleNo = so.dd_ScheduleNo
   AND sod.dim_dateidvalidenddateid_kotg992 <> so.dim_dateidvalidenddateid_kotg992

UPDATE fact_salesorderdelivery sod
SET sod.dim_customergroupid_kotg992 = so.dim_customergroupid_kotg992
	,sod.dw_update_date = current_timestamp
   FROM	fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
   AND sod.dd_SalesItemNo = so.dd_SalesItemNo
   AND sod.dd_ScheduleNo = so.dd_ScheduleNo
   AND sod.dim_customergroupid_kotg992 <> so.dim_customergroupid_kotg992

 UPDATE fact_salesorderdelivery sod
SET sod.dim_distributionchanelcodeid_kotg992 = so.dim_distributionchanelcodeid_kotg992
	,sod.dw_update_date = current_timestamp
	   FROM	fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
   AND sod.dd_SalesItemNo = so.dd_SalesItemNo
   AND sod.dd_ScheduleNo = so.dd_ScheduleNo
   AND sod.dim_distributionchanelcodeid_kotg992 <> so.dim_distributionchanelcodeid_kotg992

    UPDATE fact_salesorderdelivery sod
SET sod.dim_salesorgid_a055 = so.dim_salesorgid_a055
	,sod.dw_update_date = current_timestamp
   FROM	fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
   AND sod.dd_SalesItemNo = so.dd_SalesItemNo
   AND sod.dd_ScheduleNo = so.dd_ScheduleNo
   AND sod.dim_salesorgid_a055 <> so.dim_salesorgid_a055


UPDATE fact_salesorderdelivery sod
SET sod.dd_matnrbycustomer = so.dd_matnrbycustomer
	,sod.dw_update_date = current_timestamp
   FROM	fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
   AND sod.dd_SalesItemNo = so.dd_SalesItemNo
   AND sod.dd_ScheduleNo = so.dd_ScheduleNo
   AND sod.dd_matnrbycustomer <> so.dd_matnrbycustomer

UPDATE fact_salesorderdelivery sod
SET sod.dd_matnrdescripbycustomer = so.dd_matnrdescripbycustomer
	,sod.dw_update_date = current_timestamp
   FROM	fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
   AND sod.dd_SalesItemNo = so.dd_SalesItemNo
   AND sod.dd_ScheduleNo = so.dd_ScheduleNo
   AND sod.dd_matnrdescripbycustomer <> so.dd_matnrdescripbycustomer
/* Octavian: Every Angle Transition part 2 */

/*Begin 26-Oct-2015 Andrian Added new Expiry Date according BI-1267*/
/*UPDATE fact_salesorderdelivery sod
SET sod.dim_DateidExpiryDate = so.dim_DateidExpiryDate
	,sod.dw_update_date = current_timestamp
 FROM	fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
   AND sod.dd_SalesItemNo = so.dd_SalesItemNo
   AND sod.dd_ScheduleNo = so.dd_ScheduleNo
   AND sod.dim_DateidExpiryDate <> so.dim_DateidExpiryDate

/*End 26-Oct-2015 Andrian Added new Expiry Date according BI-1267*/


/* END OPTIMIZATION 2 - ALIN GHEORGHE  */

/*Begin 15-Feb-2016 Andrian Added new logic Ordering Plant according BI-1907*/
/*21 Nov 2017 Georgiana Changes, adding additional update for dd_purchordernumbershiptoparty in order to avoid refresh issues and changed the logic for dim_plantidordering to be populated directly from purchase also to avoid refresh issues*/
merge into fact_salesorderdelivery f
using ( select distinct f.fact_salesorderdeliveryid,vbkd_bstkd_e
 from fact_salesorderdelivery f,  vbak_vbap_vbkd vkd
WHERE f.dd_SalesDocNo  =  vkd.VBKD_VBELN
AND f.dd_SalesItemNo  =  vkd.VBKD_POSNR
and ifnull(f.dd_purchordernumbershiptoparty,'Not Set') <> ifnull(vbkd_bstkd_e,'Not Set')) t
on t.fact_salesorderdeliveryid=f.fact_salesorderdeliveryid
when matched then update set f.dd_purchordernumbershiptoparty=ifnull(vbkd_bstkd_e,'Not Set');

drop table if exists tmp_delivdestinationplant_upd;
create table tmp_delivdestinationplant_upd as
select distinct f.fact_salesorderdeliveryid,p.dim_plantidordering,
ROW_NUMBER() OVER (PARTITION BY f.fact_salesorderdeliveryid ORDER BY p.dd_documentno,p.dd_documentitemno,p.dd_scheduleno) AS rowseqno
 from fact_salesorderdelivery f,
fact_purchase p
where ifnull(f.dd_purchordernumbershiptoparty,'Not Set') = ifnull(p.dd_documentno,'Not Set');
/* 21 Nov 2017 Georgiana end of changes*/

update fact_salesorderdelivery f
set f.dim_plantidordering = u.dim_plantidordering
from tmp_delivdestinationplant_upd u,fact_salesorderdelivery f
where f.fact_salesorderdeliveryid = u.fact_salesorderdeliveryid
and rowseqno =1
and f.dim_plantidordering <> u.dim_plantidordering;
drop table if exists tmp_delivdestinationplant_upd;

/* Madalina 1 Nov 2016 - Consider the plant related to the empty material BI-4376*/
drop table if exists tmp_distinct_z1sdsrshipto;
create table tmp_distinct_z1sdsrshipto as
select distinct Z1SD_SR_SHIPTO_KUNWE,Z1SD_SR_SHIPTO_WERKS
from Z1SD_SR_SHIPTO
where Z1SD_SR_SHIPTO_MATNR is null;

drop table if exists tmp1_distinct_kunnr;
create  table tmp1_distinct_kunnr as
select distinct a.fact_salesorderdeliveryid,b.customernumber
from fact_salesorderdelivery a,dim_customer b
where a.dim_customeridshipto = b.dim_customerid;

drop table if exists tmp01_delivdestinationplant_upd;
create table tmp01_delivdestinationplant_upd as
select distinct f.fact_salesorderdeliveryid,
      pl.dim_plantid, row_number() over (partition by f.fact_salesorderdeliveryid order by f.fact_salesorderdeliveryid) as rowseqno
    from fact_salesorderdelivery f,
    tmp1_distinct_kunnr tdk,
    tmp_distinct_z1sdsrshipto tdz,
    dim_plant pl
where (dd_purchordernumbershiptoparty = 'Not Set' or f.dim_plantidordering = 1)
and f.fact_salesorderdeliveryid = tdk.fact_salesorderdeliveryid
and tdk.customernumber = tdz.Z1SD_SR_SHIPTO_KUNWE
and tdz.Z1SD_SR_SHIPTO_WERKS = pl.plantcode;


update fact_salesorderdelivery f
set f.dim_plantidordering = u.dim_plantid
from tmp01_delivdestinationplant_upd u,fact_salesorderdelivery f
where f.fact_salesorderdeliveryid = u.fact_salesorderdeliveryid
and rowseqno=1
and f.dim_plantidordering <> u.dim_plantid;

drop table if exists tmp1_distinct_kunnr;
drop table if exists tmp_distinct_z1sdsrshipto;
drop table if exists tmp01_delivdestinationplant_upd;

/* Madalina 5 Oct 2016 - BC tk Shipments to Lite ComOps not correct in area */
/* revert to old logic BI-4376 */
/* merge into fact_salesorderdelivery sod
using ( select distinct sod.fact_salesorderdeliveryid, 
		first_value(pl.dim_plantid) over ( partition by fact_salesorderdeliveryid order by '') as dim_plantid
		from fact_salesorderdelivery sod, dim_customer cust, dim_deliverytype dt, T001W, dim_plant pl
		where sod.Dim_CustomeridSoldTo = cust.dim_customerid 
			and sod.dim_deliverytypeid = dt.dim_deliverytypeid
			and DeliveryType = 'LF'
			and cust.CustomerNumber = ifnull(KUNNR, 'Not Set')
			and pl.plantcode = ifnull(WERKS, 'Not Set')
		) upd
on sod.fact_salesorderdeliveryid = upd.fact_salesorderdeliveryid
when matched then update
set sod.dim_plantidordering = ifnull(upd.dim_plantid, 1),
	dw_update_date = current_timestamp */
/* END - 5 Oct 2016 */


update fact_salesorderdelivery f
set f.ct_leadtimedest = ifnull(destpart.leadtime,0)
from dim_part pt, dim_plant pld, dim_part destpart,fact_salesorderdelivery f
where f.dim_partid = pt.dim_partid and f.dim_plantidordering = pld.dim_plantid
and destpart.partnumber = pt.partnumber and destpart.plant = pld.plantcode
and f.ct_leadtimedest <> ifnull(destpart.leadtime,0);


/*End 26-Oct-2015 Andrian Added new Expiry Date according BI-1267*/

/* Octavian: Every Angle update LIPS fields */
/*START OPTIMIZATION 3 - ALIN GHEORGHE */

UPDATE fact_salesorderdelivery f
SET 
dd_changeuser = ifnull(LIKP_AENAM,'Not Set'),
dd_creationuser = ifnull(LIKP_ERNAM,'Not Set'),
dd_externalidentification = ifnull(LIKP_LIFEX,'Not Set'),
dd_shippingpoint = ifnull(LIKP_VSTEL,'Not Set'),
dd_dlvritemcategory = ifnull(LIPS_PSTYV,'Not Set'),
dd_itemreturns = ifnull(LIPS_SHKZG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_changeuser <> ifnull(LIKP_AENAM,'Not Set')
AND dd_creationuser <> ifnull(LIKP_ERNAM,'Not Set')
AND dd_externalidentification <> ifnull(LIKP_LIFEX,'Not Set')
AND dd_shippingpoint <> ifnull(LIKP_VSTEL,'Not Set')
AND dd_dlvritemcategory <> ifnull(LIPS_PSTYV,'Not Set')
AND dd_itemreturns <> ifnull(LIPS_SHKZG,'Not Set');


/* Octavian: Every Angle update LIPS fields */
/*
UPDATE fact_salesorderdelivery f
SET dd_changeuser = ifnull(LIKP_AENAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_changeuser <> ifnull(LIKP_AENAM,'Not Set')

UPDATE fact_salesorderdelivery f
SET dd_creationuser = ifnull(LIKP_ERNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_creationuser <> ifnull(LIKP_ERNAM,'Not Set')

UPDATE fact_salesorderdelivery f
SET dd_externalidentification = ifnull(LIKP_LIFEX,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_externalidentification <> ifnull(LIKP_LIFEX,'Not Set')

UPDATE fact_salesorderdelivery f
SET dd_shippingpoint = ifnull(LIKP_VSTEL,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_shippingpoint <> ifnull(LIKP_VSTEL,'Not Set')

UPDATE fact_salesorderdelivery f
SET dd_dlvritemcategory = ifnull(LIPS_PSTYV,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_dlvritemcategory <> ifnull(LIPS_PSTYV,'Not Set')

UPDATE fact_salesorderdelivery f
SET dd_itemreturns = ifnull(LIPS_SHKZG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_itemreturns <> ifnull(LIPS_SHKZG,'Not Set')

*/

/*END OPTIMIZATION 3 -ALIN GHEORGHE*/
UPDATE fact_salesorderdelivery f
SET f.dim_dateidchangedon = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l, dim_date dt, dim_plant pl,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dt.datevalue = ifnull(LIKP_AEDAT,'0001-01-01')
AND dt.CompanyCode = pl.CompanyCode
and dt.plantcode_factory = l.LIPS_WERKS
AND pl.plantcode = ifnull(LIPS_WERKS,'Not Set')
AND f.dim_dateidchangedon <> dt.dim_dateid;

UPDATE fact_salesorderdelivery f
SET f.dim_dateiditemdlvrcreated = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l, dim_date dt, dim_plant pl,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dt.datevalue = ifnull(LIPS_ERDAT,'0001-01-01')
AND dt.CompanyCode = pl.CompanyCode
and dt.plantcode_factory = l.LIPS_WERKS
AND pl.plantcode = ifnull(LIPS_WERKS,'Not Set')
AND f.dim_dateiditemdlvrcreated <> dt.dim_dateid;

UPDATE fact_salesorderdelivery f
SET f.dim_dateidexpirationdate = dt.dim_dateid,
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l, dim_date dt, dim_plant pl,fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dt.datevalue = ifnull(LIPS_VFDAT,'0001-01-01')
AND dt.CompanyCode = pl.CompanyCode
and dt.plantcode_factory = l.LIPS_WERKS
AND pl.plantcode = ifnull(LIPS_WERKS,'Not Set')
AND f.dim_dateidexpirationdate <> dt.dim_dateid;
/* Octavian: Every Angle update LIPS fields */

/*Georgiana 17 Mar 2016 adding dim_partsalesid, dim_partsalesid_vbap, amt_NetValueItem  and ct_actualinvoiceqty: BI-2357*/

update fact_salesorderdelivery
set Dim_PartSalesId = 1
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where Dim_PartSalesId is NULL;

UPDATE fact_salesorderdelivery sod
SET sod.Dim_PartSalesId = ifnull(so.Dim_PartSalesId,1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_salesorder so,fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.Dim_PartSalesId,-1) <> ifnull(so.Dim_PartSalesId,-2);


update fact_salesorderdelivery sd
SET sd.Dim_SalesUoMid = fso.Dim_SalesUoMid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	FROM fact_salesorder fso,fact_salesorderdelivery sd
where sd.dd_SalesDocNo = fso.dd_SalesDocNo
AND sd.dd_SalesItemNo = fso.dd_SalesItemNo
AND sd.dd_ScheduleNo = fso.dd_ScheduleNo
AND sd.Dim_SalesUoMid <> fso.Dim_SalesUoMid;


UPDATE fact_salesorderdelivery fso
SET dim_partsalesid_vbap = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM VBAK_VBAP vp,fact_salesorderdelivery fso
where   fso.dd_SalesDocNo = vp.VBAP_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = 0
AND VBAK_VKORG IS NULL
AND dim_partsalesid_vbap <> 1;

/*Ambigous replace needs to be addressed*/
/*UPDATE fact_salesorderdelivery fso
SET dim_partsalesid_vbap = sprt.Dim_PartSalesId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl
FROM VBAK_VBAP_VBEP vp, Dim_PartSales sprt,fact_salesorderdelivery fso
where   fso.dd_SalesDocNo = vp.VBAK_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = vp.VBEP_ETENR
AND VBAK_VKORG IS NOT NULL
AND sprt.SalesOrgCode = VBAK_VKORG
AND sprt.PartNumber = VBAP_MATNR
AND VBAP_VRKME = sprt.sales_uom
AND sprt.RowIsCurrent = 1
AND dim_partsalesid_vbap <> sprt.Dim_PartSalesId*/

drop table if exists tmp_for_upd_dim_partsalesid;
create table tmp_for_upd_dim_partsalesid as
select distinct sprt.Dim_PartSalesId, fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo, row_number() over (partition by fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo order by fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo) as rowseqno
FROM VBAK_VBAP_VBEP vp, Dim_PartSales sprt,fact_salesorderdelivery fso
where   fso.dd_SalesDocNo = vp.VBAK_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = vp.VBEP_ETENR
AND VBAK_VKORG IS NOT NULL
AND sprt.SalesOrgCode = VBAK_VKORG
AND sprt.PartNumber = VBAP_MATNR
AND VBAP_VRKME = sprt.sales_uom
AND sprt.RowIsCurrent = 1
AND dim_partsalesid_vbap <> sprt.Dim_PartSalesId;

UPDATE fact_salesorderdelivery fso
SET dim_partsalesid_vbap = vp.Dim_PartSalesId
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_for_upd_dim_partsalesid vp,fact_salesorderdelivery fso
where   fso.dd_SalesDocNo = vp.dd_SalesDocNo
AND fso.dd_SalesItemNo = vp.dd_SalesItemNo
AND fso.dd_ScheduleNo = vp.dd_ScheduleNo
and vp.rowseqno=1
AND dim_partsalesid_vbap <> vp.Dim_PartSalesId;

drop table if exists tmp_for_upd_dim_partsalesid;

UPDATE fact_salesorderdelivery fso
SET dim_partsalesid_vbap = 1
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM VBAK_VBAP_VBEP vp,fact_salesorderdelivery fso
where   fso.dd_SalesDocNo = vp.VBAK_VBELN
AND fso.dd_SalesItemNo = vp.VBAP_POSNR
AND fso.dd_ScheduleNo = vp.VBEP_ETENR
AND VBAK_VKORG IS NULL
AND dim_partsalesid_vbap <> 1;

UPDATE fact_salesorderdelivery fso SET dim_partsalesid_vbap = 1 where dim_partsalesid_vbap IS NULL;


merge into fact_salesorderdelivery sod
using (  select distinct fact_salesorderdeliveryid, max(fb.amt_NetValueItem) as amt_NetValueItem
FROM fact_billing fb,fact_salesorderdelivery sod
WHERE fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
group by fact_salesorderdeliveryid) t on t.fact_salesorderdeliveryid=sod.fact_salesorderdeliveryid
when matched then update set sod.amt_NetValueItem = t.amt_NetValueItem
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where sod.amt_NetValueItem <> t.amt_NetValueItem;

merge into fact_salesorderdelivery sod
using (  select distinct fact_salesorderdeliveryid, max(fb.ct_actualinvoiceqty) as ct_actualinvoiceqty 
FROM fact_billing fb,fact_salesorderdelivery sod
WHERE fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
group by  fact_salesorderdeliveryid) t on t.fact_salesorderdeliveryid=sod.fact_salesorderdeliveryid
when matched then update set sod.ct_actualinvoiceqty = t.ct_actualinvoiceqty 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where sod.ct_actualinvoiceqty <> t.ct_actualinvoiceqty ;

/*17 Mar End of changes*/

/*Georgiana 21 Mar Changes adding dd_SalesDlvrDocNo and dd_SalesDlvrItemNo to fact_salesorder*/
/*Ambiguous Replace*/
/*update fact_salesorder so
SET so.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
from fact_salesorderdelivery sod,fact_salesorder so
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND so.dd_SalesDlvrDocNo <> sod.dd_SalesDlvrDocNo*/

drop table if exists tmp_salesDlvrDocNo;
create table tmp_salesDlvrDocNo as
select a.dd_salesdocno,
      a.dd_salesitemno,
	  a.dd_scheduleno,
	  a.dd_SalesDlvrDocNo from
(
 select a.dd_salesdocno,
         a.dd_salesitemno,
		  a.dd_scheduleno,
		     a.dd_SalesDlvrDocNo,
     row_number() over (partition by  a.dd_salesdocno,
	                                   dd_salesitemno,
									   dd_scheduleno
	                        order by b.datevalue desc) RN
   from fact_salesorderdelivery a,
                       dim_date b

where a.dim_dateiddlvrdoccreated = b.dim_dateid
 ) a
where a.RN=1;

update fact_salesorder so
SET so.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
from tmp_salesDlvrDocNo sod,fact_salesorder so
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND so.dd_SalesDlvrDocNo <> sod.dd_SalesDlvrDocNo;

drop table if exists tmp_salesDlvrDocNo;

/*Ambiguous Replace*/
/*update fact_salesorder so
SET so.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
from fact_salesorderdelivery sod,fact_salesorder so
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND so.dd_SalesDlvrItemNo <> sod.dd_SalesDlvrItemNo*/

drop table if exists tmp_salesDlvrItemNo;
create table tmp_salesDlvrItemNo as
select a.dd_salesdocno,
      a.dd_salesitemno,
	  a.dd_scheduleno,
	  a.dd_SalesDlvrItemNo from
(
 select a.dd_salesdocno,
         a.dd_salesitemno,
		  a.dd_scheduleno,
		     a.dd_SalesDlvrItemNo,
     row_number() over (partition by  a.dd_salesdocno,
	                                   dd_salesitemno,
									   dd_scheduleno
	                        order by b.datevalue desc) RN
   from fact_salesorderdelivery a,
                       dim_date b

where a.dim_dateiddlvrdoccreated = b.dim_dateid
 ) a
where a.RN=1;

update fact_salesorder so
SET so.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
from tmp_salesDlvrItemNo sod,fact_salesorder so
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND so.dd_SalesDlvrItemNo <> sod.dd_SalesDlvrItemNo;

drop table if exists tmp_salesDlvrItemNo;

/* 21 Mar 2016 End of Changes*/

/* Madalina 10 Jun 2016 - Shipment Doc No, Shipment Doc Item and Actual Goods Issue Date are Not Set for some items BI-3042 */
/*Andrian 24 Jul 2016 ambiguos replace avoid*/
update fact_salesorder fso
    set fso.dd_SalesDlvrDocNo = f.dd_SalesDlvrDocNo
from (select dd_SalesDocNo,dd_SalesItemNo,max(dd_SalesDlvrDocNo) as dd_SalesDlvrDocNo from fact_salesorder
where dd_SalesDlvrDocNo <> 'Not Set'
group by dd_SalesDocNo,dd_SalesItemNo) f, fact_salesorder fso	
where f.dd_SalesDocNo = fso.dd_SalesDocNo
		and f.dd_SalesItemNo = fso.dd_SalesItemNo
		and fso.dd_SalesDlvrDocNo = 'Not Set';


update fact_salesorder fso
    set fso.dd_salesdlvritemno = f.dd_salesdlvritemno
from (select dd_SalesDocNo,dd_SalesItemNo,max(dd_salesdlvritemno) as dd_salesdlvritemno from fact_salesorder
where dd_salesdlvritemno <> '0'
group by dd_SalesDocNo,dd_SalesItemNo) f, fact_salesorder fso	
where f.dd_SalesDocNo = fso.dd_SalesDocNo
		and f.dd_SalesItemNo = fso.dd_SalesItemNo
		and fso.dd_salesdlvritemno = '0';

update fact_salesorder fso
    set fso.dim_dateidactualgi = f.dim_dateidactualgi
from (select dd_SalesDocNo,dd_SalesItemNo,max(dim_dateidactualgi) as dim_dateidactualgi from fact_salesorder
where dim_dateidactualgi <> '1'
group by dd_SalesDocNo,dd_SalesItemNo) f, fact_salesorder fso	
where f.dd_SalesDocNo = fso.dd_SalesDocNo
		and f.dd_SalesItemNo = fso.dd_SalesItemNo
		and fso.dim_dateidactualgi = '1';

/* Liviu Ionescu - BI-3553 - Post EAR - Shipping: LIPS-UMVKN, Denominator for Conversion of Sales qty */

/* UPDATE fact_salesorderdelivery f
SET dd_SalesqtyConversionDenom = ifnull(LIPS_UMVKN,1),
dw_update_date = CURRENT_TIMESTAMP
FROM LIKP_LIPS l, fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(LIPS_VGPOS,0)
AND dd_SalesqtyConversionDenom <> ifnull(LIPS_UMVKN,1)
*/

MERGE INTO fact_salesorderdelivery f 
USING 
(
SELECT dd_SalesDlvrItemNo,dd_SalesDocNo,dd_SalesItemNo,MAX(IFNULL(L.LIPS_UMVKN,1)) AS dd_SalesqtyConversionDenom
FROM LIKP_LIPS L, fact_salesorderdelivery f
WHERE dd_SalesDlvrDocNo = ifnull(L.LIKP_VBELN,'Not Set')
AND dd_SalesDlvrItemNo = ifnull(L.LIPS_POSNR,0)
AND dd_SalesDocNo = ifnull(L.LIPS_VGBEL,'Not Set')
AND dd_SalesItemNo = ifnull(L.LIPS_VGPOS,0)
GROUP BY dd_SalesDlvrItemNo,dd_SalesDocNo,dd_SalesItemNo) X 
ON (F.dd_SalesDlvrItemNo = X.dd_SalesDlvrItemNo 
AND F.dd_SalesDocNo = X.dd_SalesDocNo 
AND F.dd_SalesItemNo = X.dd_SalesItemNo)
WHEN MATCHED THEN 
UPDATE SET F.dd_SalesqtyConversionDenom = X.dd_SalesqtyConversionDenom, dw_update_date = CURRENT_TIMESTAMP
WHERE F.dd_SalesqtyConversionDenom <> X.dd_SalesqtyConversionDenom;



/* End Liviu Ionescu */

/*End of Changes 10 Jun 2016 */
/* Octavian :EA add LIPS_CHARG to batch beside VBAP */

/*Ambiguous Replace*/
/* update fact_salesorder so
SET so.dd_batchno = sod.dd_batch,
dw_update_date = CURRENT_TIMESTAMP
from fact_salesorderdelivery sod,fact_salesorder so
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND so.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
AND so.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
AND so.dd_batchno = 'Not Set'  /* Madalina 02 Aug 2016 - don't overwrite batch if is already completed from Sales -  BI-3590 
AND so.dd_batchno <> sod.dd_batch
*/

MERGE INTO fact_salesorder so
USING (
SELECT SO.dd_SalesDocNo,SO.dd_SalesItemNo,SO.dd_ScheduleNo,SO.dd_SalesDlvrDocNo,SO.dd_SalesDlvrItemNo,MAX(SOD.dd_batch) AS dd_batchno
FROM fact_salesorderdelivery sod,fact_salesorder SO
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND so.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
AND so.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
AND so.dd_batchno = 'Not Set'  /* Madalina 02 Aug 2016 - don't overwrite batch if is already completed from Sales -  BI-3590 */
GROUP BY SO.dd_SalesDocNo,SO.dd_SalesItemNo,SO.dd_ScheduleNo,SO.dd_SalesDlvrDocNo,SO.dd_SalesDlvrItemNo)X
ON ( SO.dd_SalesDocNo = X.dd_SalesDocNo
AND SO.dd_SalesItemNo = X.dd_SalesItemNo
AND SO.dd_ScheduleNo = X.dd_ScheduleNo
AND SO.dd_SalesDlvrDocNo = X.dd_SalesDlvrDocNo
AND SO.dd_SalesDlvrItemNo = X.dd_SalesDlvrItemNo)
WHEN MATCHED THEN 
UPDATE SET so.dd_batchno = X.dd_batchno,
dw_update_date = CURRENT_TIMESTAMP;

update fact_salesorder ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp,fact_salesorder ia
where ia.dim_partid = dp.dim_partid and
ia.dd_batchno = b.batchnumber and
dp.partnumber = b.partnumber and
dp.plant = b.plantcode
and ia.dim_batchid <> b.dim_batchid;
/* Octavian :EA add LIPS_CHARG to batch beside VBAP */

update fact_salesorder ia
set dim_batchid = 1
where dd_batchno = 'Not Set';

/* Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */

UPDATE fact_salesorderdelivery f
SET f.std_exchangerate_dateid = dt.dim_dateid

	FROM fact_salesorderdelivery f, dim_plant p
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode
		 and dt.plantcode_factory = p.plantcode
WHERE f.dim_plantid = p.dim_plantid
AND   dt.datevalue = current_date
AND   f.std_exchangerate_dateid <> dt.dim_dateid;

 /* END Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */

 /*26 May 2016 Gerorgiana EA Changes adding dd_packagingmasterial according to BI-2627*/

drop table if exists tmp_vbfa_forhandlingunit;
create table tmp_vbfa_forhandlingunit as
select distinct VBFA_VBELV,VBFA_POSNV,VBFA_VBELN,min(VBFA_POSNN)VBFA_POSNN_min from vbfa where vbfa_vbtyp_n='X' and vbfa_bwart is null
group by VBFA_VBELV,VBFA_POSNV,VBFA_VBELN;

drop table if exists tmp_for_packagingmat;
create table tmp_for_packagingmat as
select distinct va.vbfa_vbelv,va.vbfa_posnv,
group_concat(distinct ve.VEKP_VHILM) as VEKP_VHILM
 from fact_salesorderdelivery sod, tmp_vbfa_forhandlingunit va,vekp ve
where va.VBFA_VBELN=ve.VEKP_VENUM
and sod.dd_SalesDlvrDocNo = va.vbfa_vbelv and sod.dd_SalesDlvrItemNo = va.vbfa_posnv
Group by  va.vbfa_vbelv,va.vbfa_posnv;


UPDATE fact_salesorderdelivery sod
SET sod.dd_packagingmasterial = ifnull(t.VEKP_VHILM,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_for_packagingmat t,fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = t.vbfa_vbelv
AND sod.dd_SalesDlvrItemNo = t.vbfa_posnv
AND sod.dd_packagingmasterial <> ifnull(t.VEKP_VHILM,'Not Set');

/*26 May 2016 End of Changes*/
/*01 Jun 2016 Georgiana EA changes according to BI-3079*/
update fact_salesorderdelivery sod
set Dim_DateidActualGoodsIssue =dd.Dim_Dateid
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from Dim_Date dd,LIKP_LIPS, Dim_Plant pl, fact_salesorderdelivery sod
WHERE dd.DateValue = ifnull(likp_wadat_ist, '0001-01-01') AND dd.CompanyCode = pl.CompanyCode
and dd.plantcode_factory = LIPS_WERKS
and ifnull(LIKP_VBELN, 'Not Set') = dd_SalesDlvrDocNo and ifnull(LIPS_POSNR, 0) = dd_SalesDlvrItemNo
and pl.plantcode = ifnull(LIPS_WERKS, 'Not Set')
and dd.Dim_Dateid <> Dim_DateidActualGoodsIssue;
/* 01 Jun 2016 End of Changes*/
/* Madalina 04 Aug 2016 - add Execution Status Shipping Item/Header fields - BI-3501 */
/* Execution Status Shipping Item - dd_executionstatusitem */
drop table if exists tmp_upd_execstatusitem;
create table tmp_upd_execstatusitem as
select distinct sod.dd_SalesDlvrDocNo, sod.dd_SalesDlvrItemNo, sod.dd_SalesDocNo, sod.dd_SalesItemNo, sod.dd_ScheduleNo,
	CASE WHEN dis.GoodsMovementStatus = 'Not yet processed' OR dis.GoodsMovementStatus = 'Partially processed' THEN 'Open'
	ELSE CASE WHEN dis.GoodsMovementStatus = 'Completely processed' THEN 'Closed'
		 ELSE CASE WHEN dis.GoodsMovementStatus = 'Not Set' THEN 
		 			(CASE WHEN dis.OverallProcessingStatus = 'Completely processed' THEN 'Closed' ELSE 'Open' END)
			  END
		 END
	END as dd_executionstatusitem
from dim_salesorderitemstatus dis, fact_salesorderdelivery sod
where sod.dim_deliveryitemstatusid = dis.dim_salesorderitemstatusid;

update fact_salesorderdelivery sod
set sod.dd_executionstatusitem = t.dd_executionstatusitem
from tmp_upd_execstatusitem t, fact_salesorderdelivery sod
where sod.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
	and sod.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo
	and sod.dd_SalesDocNo = t.dd_SalesDocNo
	and sod.dd_SalesItemNo = t.dd_SalesItemNo
	and sod.dd_ScheduleNo = t.dd_ScheduleNo
	and  sod.dd_executionstatusitem <> t.dd_executionstatusitem;

/* Execution Status Shipping Header - dd_executionstatusheader */	
drop table if exists tmp_upd_execstatusheader;
create table tmp_upd_execstatusheader as
select sod.dd_SalesDlvrDocNo, group_concat(distinct dd_executionstatusitem ORDER BY dd_executionstatusitem asc SEPARATOR ',') dd_headerstatus
from fact_salesorderdelivery sod
group by sod.dd_SalesDlvrDocNo;

drop table if exists tmp_upd_execstatusheader1;
create table tmp_upd_execstatusheader1 as
select distinct t.dd_SalesDlvrDocNo,
	case when dd_headerstatus like '%Closed%' then ( case when dd_headerstatus like '%Open%' then 'Partially Open' else 'Closed' end) 
		when dd_headerstatus like '%Open%' then 'Open' 
	end as dd_headerstatus1
from tmp_upd_execstatusheader t;

update fact_salesorderdelivery sod
set sod.dd_executionstatusheader = t.dd_headerstatus1
from tmp_upd_execstatusheader1 t, fact_salesorderdelivery sod
where sod.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
	and sod.dd_executionstatusheader <> t.dd_headerstatus1;
	
/* END 04 Aug 2016 */



/*Begin Andrian based on BI-2349*/
drop table if exists tmp_sales_header;
create table tmp_sales_header as
select a.dd_salesdocno,
      a.dd_salesitemno,
	  a.dd_scheduleno,
	  a.vbuk_pkstk from
(
 select a.dd_salesdocno,
         dd_salesitemno,
		  dd_scheduleno,
		     vbuk_pkstk,
     row_number() over (partition by  a.dd_salesdocno,
	                                   dd_salesitemno,
									   dd_scheduleno
	                        order by b.datevalue desc) RN
   from fact_salesorderdelivery a,
                       dim_date b,
			  LIKP_LIPS_VBUK c
where a.dim_dateiddlvrdoccreated = b.dim_dateid
  and a.dd_salesdlvrdocno = c.vbuk_vbeln
  and vbuk_pkstk is not null
 ) a
where a.RN=1;

drop table if exists tmp_sales_item;
create table tmp_sales_item as
select a.dd_salesdocno,
      a.dd_salesitemno,
	  a.dd_scheduleno,
	  a.vbup_pksta from
(select a.dd_salesdocno,
         dd_salesitemno,
		 dd_scheduleno,
		 vbup_pksta,
         row_number() over (partition by a.dd_salesdocno,
		                                  dd_salesitemno,
										  dd_scheduleno
							order by b.datevalue desc) RN
     from fact_salesorderdelivery a,
	                     dim_date b,
						 LIKP_LIPS_VBUP c
where a.dim_dateiddlvrdoccreated = b.dim_dateid
and a.dd_salesdlvrdocno = c.vbup_vbeln
and a.dd_salesdlvritemno = c.vbup_posnr
and vbup_pksta is not null
) a
where a.RN=1;

update fact_salesorder a
set a.dd_packingstatusheader = ifnull(b.vbuk_pkstk,'Not Set')
from tmp_sales_header b,fact_salesorder a
where a.dd_salesdocno = b.dd_salesdocno
  and a.dd_salesitemno = b.dd_salesitemno
  and a.dd_scheduleno = b.dd_scheduleno
  and a.dd_packingstatusheader <> ifnull(b.vbuk_pkstk,'Not Set');


  update fact_salesorder a
set a.dd_packingstatusitem = ifnull(b.vbup_pksta,'Not Set')
from tmp_sales_item b, fact_salesorder a
where a.dd_salesdocno = b.dd_salesdocno
  and a.dd_salesitemno = b.dd_salesitemno
  and a.dd_scheduleno = b.dd_scheduleno
  and a.dd_packingstatusitem <> ifnull(b.vbup_pksta,'Not Set');
/*End Andrian based on BI-2349*/

Drop table if exists flag_holder_722;
DROP TABLE IF EXISTS fact_salesorder_tmp_Dim_CustomeridShipToFix;
Drop table if exists cursor_table1_722;
Drop table if exists update_tbl_722;
Drop table if exists loop_tbl_722;
Drop table if exists update_so_shipment_001;
DROP TABLE IF EXISTS var_pShipToPartyPartnerFunction;
Drop table if exists tmp_fact_sodf_LIKP_LIPS;
