/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_certificateprofile_fact.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 25 May 2016 */
/*  */
/*  */
/*   Description    : Populate Certificate Profile Fact */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */
 
 drop table if exists tmp_fact_certificateprofile;

 create table tmp_fact_certificateprofile like fact_certificateprofile INCLUDING DEFAULTS INCLUDING IDENTITY;

	
 delete from number_fountain m where m.table_name = 'tmp_fact_certificateprofile';
 insert into number_fountain
	select 'tmp_fact_certificateprofile',
			ifnull(max(f.fact_certificateprofileid ), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from tmp_fact_certificateprofile f;
  
 /* first add the keys of the staging tables QCVK, QCVM, QCVMT, QPMK_QPMT */
insert into tmp_fact_certificateprofile 
(
	fact_certificateprofileid,
	/* qcvk keys */
	dd_certificatetype,                /* CTYP */
	dd_certifprofileno,                /* VORLNR */
	dd_version, 			           /* VERSION */
	/* QCVM keys*/ 
	dd_nocharacteristicblock,    /* BLOCKNR */
	dd_conseccharactno,           /* MERKMALNR */
	/* Madalina 19 Aug 2016 - add new field as part of the grain BI-3530 */
	dd_conditionrecordno        /*KONDI_KNUMH*/
)
	select 
		(select max_id from number_fountain where table_name = 'tmp_fact_certificateprofile') + row_number() over(order by '') AS fact_certificateprofileid,
		QCVK_CTYP,
		QCVK_VORLNR,
		QCVK_VERSION,
		ifnull(QCVM_BLOCKNR, 0),
		ifnull(QCVM_MERKMALNR, 0),
		ifnull(KONDI_KNUMH, 'Not Set')   /* Madalina 19 Aug 2016 - add new field as part of the grain BI-3530 */
	from QCVK k left join QCVM m 
			on  k.QCVK_CTYP = m.QCVM_CTYP
			and k.QCVK_VORLNR = m.QCVM_VORLNR
			and k.QCVK_VERSION = m.QCVM_VERSION
		LEFT JOIN QCVMT mt
			ON m.QCVM_CTYP = mt.QCVMT_CTYP
			AND m.QCVM_VORLNR = mt.QCVMT_VORLNR
			AND m.QCVM_VERSION = mt.QCVMT_VERSION
			AND m.QCVM_BLOCKNR = mt.QCVMT_BLOCKNR
			AND m.QCVM_MERKMALNR = mt.QCVMT_MERKMALNR
		LEFT JOIN QPMK_QPMT q ON
			m.QCVM_MKMNR = q.QPMK_MKMNR AND
			m.QCVM_ZAEHLER = q.QPMK_ZAEHLER AND
			m.QCVM_VERSION = q.QPMK_VERSION
			/* Madalina 19 Aug 2016 - table for Certif profile assignments fields - KOTI002_KONDI BI-3530 */
		LEFT JOIN KOTI002_KONDI ko ON
			ko.KONDI_CTYP = k.QCVK_CTYP
			and ko.KONDI_VORLNR = k.QCVK_VORLNR
			and ko.KONDI_VERSION = k.QCVK_VERSION;
			
	
/* update fields from QCVK */		
update tmp_fact_certificateprofile cp
	set cp.dd_searchfield = ifnull(QCVK_SORTFELD, 'Not Set'),
		dw_update_date = current_timestamp
	from QCVK k, tmp_fact_certificateprofile cp
	where cp.dd_certificatetype = k.QCVK_CTYP
		  and cp.dd_certifprofileno = k.QCVK_VORLNR
		  and cp.dd_version = k.QCVK_VERSION 
		  and cp.dd_searchfield <> ifnull(QCVK_SORTFELD,'Not Set');

update tmp_fact_certificateprofile cp
	set cp.dd_shorttextheader = ifnull(QCVK_KURZTEXT,'Not Set'),
		dw_update_date = current_timestamp
	from QCVK k, tmp_fact_certificateprofile cp
	where cp.dd_certificatetype = k.QCVK_CTYP
		  and cp.dd_certifprofileno = k.QCVK_VORLNR
		  and cp.dd_version = k.QCVK_VERSION 
		  and cp.dd_shorttextheader <> ifnull(QCVK_KURZTEXT,'Not Set');
/**/		 
update tmp_fact_certificateprofile cp	
	set cp.dim_certificatetypestextid = c.dim_certificatetypestextid,
		dw_update_date = current_timestamp
	from dim_certificatetypestext c, tmp_fact_certificateprofile cp
	where cp.dd_certificatetype = c.certificatetype   /* QCVK-CTYP = TQ05T-ZGTYP */
		and cp.dim_certificatetypestextid <> c.dim_certificatetypestextid;

/* Madalina 19 Aug 2016 - add new field from QCVK - Description - BI-3530 */
update tmp_fact_certificateprofile cp
	set cp.dd_description = ifnull(QCVK_ZZADDDES, 'Not Set'),
		dw_update_date = current_timestamp
	from QCVK k, tmp_fact_certificateprofile cp
	where cp.dd_certificatetype = k.QCVK_CTYP
		  and cp.dd_certifprofileno = k.QCVK_VORLNR
		  and cp.dd_version = k.QCVK_VERSION 
		  and cp.dd_description <> ifnull(QCVK_ZZADDDES, 'Not Set');
		  
/* update fields from QVCM */
	/* update dim */

merge into tmp_fact_certificateprofile cp 
using (select distinct cp.fact_certificateprofileid, max(i.dim_inspectioncharacteristicid) as dim_inspectioncharacteristicid

from QCVM m, dim_inspectioncharacteristic i, tmp_fact_certificateprofile cp 
where m.QCVM_MKMNR = i.masterinspcharacteristics 
and m.QCVM_ZAEHLER = i.InspectionPlant
 and m.QCVM_VERSION = i.VersionNumber 
and cp.dd_certificatetype = m.QCVM_CTYP 
and cp.dd_certifprofileno = m.QCVM_VORLNR 
and cp.dd_version = m.QCVM_VERSION 
and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR 
and cp.dd_conseccharactno = m.QCVM_MERKMALNR
group by  cp.fact_certificateprofileid) t
 on t.fact_certificateprofileid=cp.fact_certificateprofileid
when matched then update set cp.dim_masterinspectionid = t.dim_inspectioncharacteristicid;
 		
update tmp_fact_certificateprofile cp 
	set cp.dim_plantid = p.dim_plantid, 
		dw_update_date = current_timestamp
	from QCVM m, dim_plant p, tmp_fact_certificateprofile cp 
	where m.QCVM_ZAEHLER = p.PlantCode	
		  and cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR	
 		  and cp.dim_plantid <> ifnull(p.dim_plantid, 1); 
 
update tmp_fact_certificateprofile cp
 	set cp.dim_unitofmeasureid = u.dim_unitofmeasureid,
 		dw_update_date = current_timestamp
	from QCVM m, dim_unitofmeasure u, tmp_fact_certificateprofile cp
	where m.QCVM_MASSEINHSW = u.uom
		  and cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dim_unitofmeasureid <> ifnull(u.dim_unitofmeasureid,1);
 
 	/* update dd */
update tmp_fact_certificateprofile cp
 	set cp.dd_target = ifnull(m.QCVM_ZZTARGET, 'Not Set'),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_target <> ifnull(m.QCVM_ZZTARGET, 'Not Set');
 	
update tmp_fact_certificateprofile cp
 	set cp.dd_lowerlimit = ifnull(m.QCVM_ZZLOWER, 'Not Set'),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_lowerlimit <> ifnull(m.QCVM_ZZLOWER, 'Not Set');
		  
update tmp_fact_certificateprofile cp
 	set cp.dd_upperlimit = ifnull(m.QCVM_ZZUPPER, 'Not Set'),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_upperlimit <> ifnull(m.QCVM_ZZUPPER, 'Not Set');

update tmp_fact_certificateprofile cp
 	set cp.dd_accuracy = ifnull(m.QCVM_STELLEN, 0),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_accuracy <> ifnull(m.QCVM_STELLEN, 0);	
	
update tmp_fact_certificateprofile cp
 	set cp.dd_onoffdates = ifnull(m.QCVM_ZZONOFF,'Not Set'),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_onoffdates <> ifnull(m.QCVM_ZZONOFF,'Not Set');	

update tmp_fact_certificateprofile cp
 	set cp.dd_characteristics = ifnull(m.QCVM_ZZINDC1,'Not Set'),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_characteristics <> ifnull(m.QCVM_ZZINDC1,'Not Set');	
		  
update tmp_fact_certificateprofile cp
 	set cp.dd_expnotation = ifnull(m.QCVM_KZEXPDS,'Not Set'),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_expnotation <> ifnull(m.QCVM_KZEXPDS,'Not Set');	
		  		  
update tmp_fact_certificateprofile cp
 	set cp.dd_textelement = ifnull(m.QCVM_TEXT_ELEM,'Not Set'),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_textelement <> ifnull(m.QCVM_TEXT_ELEM,'Not Set');	

update tmp_fact_certificateprofile cp
 	set cp.dd_inspectionmethod = ifnull(m.QCVM_KZMETHODE,'Not Set'),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_inspectionmethod <> ifnull(m.QCVM_KZMETHODE,'Not Set');	

/*	- replaced with dim	below  
update tmp_fact_certificateprofile cp
 	from QCVM m
 	set cp.dd_characteristicresult = m.QCVM_KZHERKWERT,
 		dw_update_date = current_timestamp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_characteristicresult <> ifnull(m.QCVM_KZHERKWERT,'Not Set')
*/

 update tmp_fact_certificateprofile cp
	set cp.dim_certifresultvaluesid = c.dim_certifresultvaluesid,
 		dw_update_date = current_timestamp
	from dim_certifresultvalues c, QCVM m, tmp_fact_certificateprofile cp
	where m.QCVM_KZHERKWERT = c.characteristicresult   /*QCVM_KZHERKWERT = TQ61T_KZHERKWER */
		and cp.dd_certificatetype = m.QCVM_CTYP
		and cp.dd_certifprofileno = m.QCVM_VORLNR
		and cp.dd_version = m.QCVM_VERSION 
		and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		and cp.dim_certifresultvaluesid <> c.dim_certifresultvaluesid;

/* -	replaced with dim below	
update tmp_fact_certificateprofile cp
 	from QCVM m
 	set cp.dd_characteristicshorttext = m.QCVM_KZHERKTEXT,
 		dw_update_date = current_timestamp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_characteristicshorttext <> ifnull(m.QCVM_KZHERKTEXT,'Not Set')
*/

 update tmp_fact_certificateprofile cp
	set cp.dim_certifcharacttextid = c.dim_certifcharacttextid,
 		dw_update_date = current_timestamp
	from dim_certifcharacttext c, QCVM m, tmp_fact_certificateprofile cp
	where m.QCVM_KZHERKTEXT = c.characteristicshorttext            /* QCVM_KZHERKTEXT = TQ64T_KZHERKTEXT */
		and cp.dd_certificatetype = m.QCVM_CTYP
		and cp.dd_certifprofileno = m.QCVM_VORLNR
		and cp.dd_version = m.QCVM_VERSION 
		and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		and cp.dim_certifcharacttextid <> c.dim_certifcharacttextid;

/* -	replaced with dim below	
update tmp_fact_certificateprofile cp
 	from QCVM m
 	set cp.dd_inspectionspecification = m.QCVM_KZHERKVG,
 		dw_update_date = current_timestamp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_inspectionspecification <> ifnull(m.QCVM_KZHERKVG,'Not Set')
*/

update tmp_fact_certificateprofile cp
	set cp.dim_inspectionspectextid = i.dim_inspectionspectextid,
 		dw_update_date = current_timestamp
	from QCVM m, dim_inspectionspectext i, tmp_fact_certificateprofile cp
	where m.QCVM_KZHERKVG = i.inspectionspecification    /* TQ63T_KZHERKVORG = QCVM_KZHERKVG */
		and cp.dd_certificatetype = m.QCVM_CTYP
		and cp.dd_certifprofileno = m.QCVM_VORLNR
		and cp.dd_version = m.QCVM_VERSION 
		and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		and cp.dim_inspectionspectextid <> i.dim_inspectionspectextid;
	  
update tmp_fact_certificateprofile cp
 	set cp.dd_sortnumber = ifnull(m.QCVM_VMSORTNR,0),
 		dw_update_date = current_timestamp
	from QCVM m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_sortnumber <> ifnull(m.QCVM_VMSORTNR,0);

/* update fields from QPMK */
update tmp_fact_certificateprofile cp
	set cp.dim_inspectioncharacteristicid = i.dim_inspectioncharacteristicid,
		dw_update_date = current_timestamp
	from QPMK_QPMT q, dim_inspectioncharacteristic i, dim_plant p, tmp_fact_certificateprofile cp
	where cp.dim_plantid = p.dim_plantid
		  and q.QPMK_ZAEHLER = p.plantcode
		  and cp.dim_masterinspectionid = i.dim_inspectioncharacteristicid
		  and q.QPMK_MKMNR = i.masterinspcharacteristics 
		  and q.QPMK_VERSION = cp.dd_version 
		  and cp.dim_inspectioncharacteristicid <> ifnull(i.dim_inspectioncharacteristicid,1);

/* fields from QCVMT */
update tmp_fact_certificateprofile cp
 	set cp.dd_shorttextlevel = ifnull(m.QCVMT_KURZTEXT,'Not Set'),
 		dw_update_date = current_timestamp
	from QCVMT m, tmp_fact_certificateprofile cp
 	where cp.dd_certificatetype = m.QCVMT_CTYP
		  and cp.dd_certifprofileno = m.QCVMT_VORLNR
		  and cp.dd_version = m.QCVMT_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVMT_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVMT_MERKMALNR
		  and cp.dd_shorttextlevel <> ifnull(m.QCVMT_KURZTEXT,'Not Set');
	
	
/* customer measures */ 	   

drop table if exists tmp_dd_certiftype;
create table tmp_dd_certiftype as
	select QCVM_CTYP, QCVM_VORLNR, QCVM_VERSION, QCVM_BLOCKNR, QCVM_MERKMALNR, 
		   case when QCVM_ZZCHTYPE = 1 then '1 (Quantitative)'
								 	  when QCVM_ZZCHTYPE = 2 then '2 (Qualitative)'
								 	  else 'Not Set'
								 	  end characteristicstype
	from QCVM;
	
update tmp_fact_certificateprofile cp
	set cp.dd_characteristicstype = m.characteristicstype,
 		dw_update_date = current_timestamp
	from tmp_dd_certiftype m, tmp_fact_certificateprofile cp
	where cp.dd_certificatetype = m.QCVM_CTYP
		  and cp.dd_certifprofileno = m.QCVM_VORLNR
		  and cp.dd_version = m.QCVM_VERSION 
		  and cp.dd_nocharacteristicblock = m.QCVM_BLOCKNR
		  and cp.dd_conseccharactno = m.QCVM_MERKMALNR
		  and cp.dd_characteristicstype <> ifnull(m.characteristicstype, 'Not Set');
 
/* Madalina 19 Aug 2016 - update fields from KOTI002_KONDI BI-3530 */
/* Application */
update tmp_fact_certificateprofile fc
set fc.dd_application = ifnull(ko.KOTI002_KAPPL, 'Not Set')
from KOTI002_KONDI ko, tmp_fact_certificateprofile fc
where fc.dd_certificatetype = KONDI_CTYP
	and fc.dd_certifprofileno = KONDI_VORLNR
	and fc.dd_version = KONDI_VERSION
	and fc.dd_conditionrecordno =  ifnull(ko.KONDI_KNUMH, 'Not Set')
	and fc.dd_application <> ifnull(ko.KOTI002_KAPPL, 'Not Set');
	
/* Profile Assignment type */	
update tmp_fact_certificateprofile fc
set dd_profileAssignmentType = 	ifnull(KOTI002_KSCHL, 'Not Set')
from KOTI002_KONDI ko, tmp_fact_certificateprofile fc
where fc.dd_certificatetype = KONDI_CTYP
	and fc.dd_certifprofileno = KONDI_VORLNR
	and fc.dd_version = KONDI_VERSION
	and fc.dd_conditionrecordno =  ifnull(ko.KONDI_KNUMH, 'Not Set')
	and dd_profileAssignmentType <> ifnull(KOTI002_KSCHL, 'Not Set');
	
/* Material */

/* Madalina 07 Sep 2016 - add Material Number based on KOTI002-MATNR only - BI-3878 */
update tmp_fact_certificateprofile fc
set fc.dd_partnumber = ifnull(KOTI002_MATNR, 'Not Set')
from KOTI002_KONDI ko, tmp_fact_certificateprofile fc
where fc.dd_certificatetype = KONDI_CTYP
	and fc.dd_certifprofileno = KONDI_VORLNR
	and fc.dd_version = KONDI_VERSION
	and fc.dd_conditionrecordno =  ifnull(ko.KONDI_KNUMH, 'Not Set')
	and fc.dd_partnumber <> ifnull(KOTI002_MATNR, 'Not Set');
/* END BI-3878 */
	
/* Customer */
update tmp_fact_certificateprofile fc
set fc.dim_customerid = dc.dim_customerid
from KOTI002_KONDI ko, dim_customer dc, tmp_fact_certificateprofile fc
where fc.dd_certificatetype = KONDI_CTYP
	and fc.dd_certifprofileno = KONDI_VORLNR
	and fc.dd_version = KONDI_VERSION
	and fc.dd_conditionrecordno =  ifnull(ko.KONDI_KNUMH, 'Not Set')
	and dc.customernumber = ifnull(KOTI002_KNDNR, 'Not Set')
	and fc.dim_customerid <> dc.dim_customerid;

/* Valid to date */
update tmp_fact_certificateprofile fc
set fc.dim_validtodateid = d.dim_dateid
from KOTI002_KONDI ko, dim_date d, tmp_fact_certificateprofile fc, dim_plant p
where fc.dd_certificatetype = KONDI_CTYP
	and fc.dd_certifprofileno = KONDI_VORLNR
	and fc.dd_version = KONDI_VERSION
	and fc.dd_conditionrecordno =  ifnull(ko.KONDI_KNUMH, 'Not Set')
	and d.datevalue = ifnull(KOTI002_DATBI, '0001-01-01')
	and fc.dim_plantid = p.dim_plantid
	AND p.plantcode = d.plantcode_factory
	and p.CompanyCode = d.CompanyCode
	and fc.dim_validtodateid <> d.dim_dateid;
	
/* Valid from date */
update tmp_fact_certificateprofile fc
set fc.dim_validfromdateid = d.dim_dateid
from KOTI002_KONDI ko, dim_date d, tmp_fact_certificateprofile fc, dim_plant p
where fc.dd_certificatetype = KONDI_CTYP
	and fc.dd_certifprofileno = KONDI_VORLNR
	and fc.dd_version = KONDI_VERSION
	and fc.dd_conditionrecordno =  ifnull(ko.KONDI_KNUMH, 'Not Set')
	and d.datevalue = ifnull(KOTI002_DATAB, '0001-01-01')
	and fc.dim_plantid = p.dim_plantid
	AND p.plantcode = d.plantcode_factory
	and p.CompanyCode = d.CompanyCode
	and fc.dim_validfromdateid <> d.dim_dateid;

/* End 19 Aug 2016 */

TRUNCATE TABLE fact_certificateprofile;
INSERT INTO fact_certificateprofile
select * from tmp_fact_certificateprofile;
