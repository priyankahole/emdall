
/* ***********************************************************************************************************************************
   Script         - vw_bi_populate_vendor_fact.sql
   Author         - Banciu Bogdan
   Created ON     - Feb 2016
   Description    - Populate fact_Vendor as requested by Wacom

 ********************************************Change History**************************************************************************
  Date			    By			 Version		Desc
  29 Feb 2016		BogdanB		1.0		Create the first version of the script
  04 Mar 2016     	Liviu I     1.2 	Copy script to Merck
  22 June 2016 		Octavian    2.0     Change columns and using a drop rename table approach
 *************************************************************************************************************************************/

drop table if exists tmp_fact_vendor;
create table tmp_fact_vendor
LIKE fact_vendor INCLUDING DEFAULTS INCLUDING IDENTITY;

/* To generate surrogate keys*/
DELETE FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_vendor';

INSERT INTO NUMBER_FOUNTAIN
select 	'tmp_fact_vendor',
	ifnull(max(f.fact_vendorid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from tmp_fact_vendor f;

/* Insert new rows */

INSERT INTO tmp_fact_vendor(
	fact_vendorid,
	dim_vendorid,
	dim_vendormasterid,
	dim_vendorpurchasingid,
	dim_companyid
	)
SELECT
(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'tmp_fact_vendor') + row_number() over(order by '') as fact_vendorid,
	ifnull(ve.dim_vendorid,1) AS dim_vendorid,
	ifnull(vm.dim_vendormasterid,1) AS dim_vendormasterid,
	ifnull(vp.dim_vendorpurchasingid,1) AS dim_vendorpurchasingid,
	ifnull(co.dim_companyid, 1) AS dim_companyid
FROM dim_vendor ve
FULL OUTER JOIN dim_vendormaster vm ON ve.vendornumber = vm.vendornumber
FULL OUTER JOIN dim_vendorpurchasing vp ON ve.vendornumber = vp.vendornumber
/* LEFT JOIN dim_company co ON ifnull(co.companycode,'Not Set') = ifnull(right(ve.companycode,4),'Not Set') */
LEFT JOIN dim_company co ON co.companycode = vm.companycode;
/* WHERE NOT EXISTS (SELECT 1
		FROM fact_vendor fve
			WHERE ve.dim_Vendorid = fve.dim_Vendorid
	)
*/

TRUNCATE TABLE fact_vendor;
INSERT INTO fact_vendor
SELECT * FROM tmp_fact_vendor;

DROP TABLE IF EXISTS tmp_fact_vendor;

/*Octavian Stepan 07-FEB-2018 APP-8829*/
drop table if exists tmp_search_terms;
create table tmp_search_terms as
select distinct f.fact_vendorid, 
       first_value(ifnull(a.adrc_sort1,'Not Set')) over (partition by ADRC_ADDRNUMBER ORDER BY ADRC_NATION ASC) as sort1,
       first_value(ifnull(a.adrc_sort2,'Not Set')) over (partition by ADRC_ADDRNUMBER ORDER BY ADRC_NATION ASC) as sort2
  from fact_vendor f, lfa1 l, adrc a, dim_vendor d
 where f.dim_vendorid                 =  d.dim_vendorid
   and ifnull(l.lifnr,'Not Set')      =  d.vendornumber
   and ifnull(l.lfa1_adrnr,'Not Set') =  ifnull(a.adrc_addrnumber,'Not Set');

update fact_vendor f
   set dd_search_term1 = ifnull(t.sort1,'Not Set')
  from fact_vendor f, tmp_search_terms t
 where f.fact_vendorid = t.fact_vendorid
   and dd_search_term1 <>ifnull(t.sort1,'Not Set');

update fact_vendor f
   set dd_search_term2 = ifnull(t.sort2,'Not Set')
  from fact_vendor f, tmp_search_terms t
 where f.fact_vendorid = t.fact_vendorid
   and dd_search_term2 <>ifnull(t.sort2,'Not Set');


drop table if exists tmp_search_terms;