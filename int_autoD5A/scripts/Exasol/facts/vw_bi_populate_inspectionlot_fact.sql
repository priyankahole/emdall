
DROP TABLE IF EXISTS tmp_del1_inslot;
CREATE TABLE tmp_del1_inslot
AS
SELECT DISTINCT il.dd_inspectionlotno from fact_inspectionlot il;

DROP TABLE IF EXISTS tmp_ins1_inslot;
CREATE TABLE tmp_ins1_inslot
AS
SELECT il.dd_inspectionlotno from fact_inspectionlot il where 1=2;

insert into tmp_ins1_inslot
select distinct q.QALS_PRUEFLOS from qals q;

/*call vectorwise(combine 'tmp_ins1_inslot-tmp_del1_inslot')*/
delete from tmp_ins1_inslot
WHERE dd_inspectionlotno in  (SELECT dd_inspectionlotno FROM tmp_del1_inslot);



DROP TABLE IF EXISTS tmp_ins2_inslot;
CREATE TABLE tmp_ins2_inslot
AS
SELECT DISTINCT q.*
FROM qals q,tmp_ins1_inslot i
WHERE q.QALS_PRUEFLOS = i.dd_inspectionlotno;

DROP TABLE IF EXISTS tmp_dim_profitcenter;
CREATE TABLE tmp_dim_profitcenter
AS
SELECT  ControllingArea, ProfitCenterCode,QALS_ERSTELDAT, MIN(ValidTo) as min_validto
FROM dim_profitcenter, qals
WHERE  ControllingArea = QALS_KOKRS
AND ProfitCenterCode = QALS_PRCTR
AND ValidTo >= QALS_ERSTELDAT
/*APP-9894 IENE remove RowIsCurrent condition
AND RowIsCurrent = 1*/
GROUP BY  ControllingArea, ProfitCenterCode,QALS_ERSTELDAT ;

DROP TABLE IF EXISTS tmp_dim_profitcenter2;
CREATE TABLE tmp_dim_profitcenter2
AS
SELECT DISTINCT p1.*,p2.dim_profitcenterid
FROM dim_profitcenter p2,tmp_dim_profitcenter p1
WHERE p1.ControllingArea = p2.ControllingArea
AND p1.ProfitCenterCode = p2.ProfitCenterCode
AND p1.min_validto = p2.validto;
/*APP-9894 IENE remove RowIsCurrent condition
AND p2.RowIsCurrent = 1*/


/*INSERT INTO fact_inspectionlot(fact_inspectionlotid,
                               ct_actualinspectedqty,
                               ct_actuallotqty,
                               ct_blockedqty,
                               ct_inspectionlotqty,
                               ct_postedqty,
                               ct_qtydefective,
                               ct_qtyreturned,
                               ct_reserveqty,
                               ct_sampleqty,
                               ct_samplesizeqty,
                               ct_scrapqty,
                               ct_unrestrictedqty,
                               dd_ObjectNumber,
                               dd_batchno,
                               dd_changedby,
                               dd_createdby,
                               dd_documentitemno,
                               dd_documentno,
                               dd_inspectionlotno,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocNo,
                               dd_MaterialDocYear,
                               dd_orderno,
                               dd_ScheduleNo,
                               Dim_AccountCategoryid,
                               dim_costcenterid,
                               dim_dateidinspectionend,
                               dim_dateidinspectionstart,
                               dim_dateidkeydate,
                               dim_dateidlotcreated,
                               dim_dateidposting,
                               dim_documenttypetextid,
                               dim_inspectionlotmiscid,
                               dim_inspectionlotoriginid,
                               dim_inspectionlotstoragelocationid,
                               dim_inspectionsamplestatusid,
                               dim_inspectionseverityid,
                               dim_inspectionstageid,
                               dim_inspectiontypeid,
                               dim_itemcategoryid,
                               dim_lotunitofmeasureid,
                               dim_manufacturerid,
                               dim_ObjectCategoryid,
                               dim_partid,
                               dim_plantid,
                               dim_purchaseorgid,
                               dim_routeid,
                               dim_sampleunitofmeasureid,
                               dim_specialstockid,
                               dim_statusprofileid,
                               dim_storagelocationid,
                               dim_tasklisttypeid,
                               dim_tasklistusageid,
                               dim_vendorid,
                               dim_warehousenumberid,
                               dim_Controllingareaid,
                               dim_profitcenterid)
        SELECT (SELECT ifnull(max(fil.fact_inspectionlotid), 1) id
             FROM fact_inspectionlot fil)
          + row_number() over(),
                  QALS_LMENGEPR ct_actualinspectedqty,
          QALS_LMENGEIST ct_actuallotqty,
          QALS_LMENGE04 ct_blockedqty,
          QALS_LOSMENGE ct_inspectionlotqty,
          QALS_LMENGEZUB ct_postedqty,
          QALS_LMENGESCH ct_qtydefective,
          QALS_LMENGE07 ct_qtyreturned,
          QALS_LMENGE05 ct_reserveqty,
          QALS_LMENGE03 ct_sampleqty,
          QALS_GESSTICHPR ct_samplesizeqty,
          QALS_LMENGE02 ct_scrapqty,
          QALS_LMENGE01 ct_unrestrictedqty,
          ifnull(QALS_OBJNR,'Not Set') dd_ObjectNumber,
          ifnull(QALS_CHARG, 'Not Set') dd_batchno,
          ifnull(QALS_AENDERER, 'Not Set') dd_changedby,
          QALS_ERSTELLER dd_createdby,
          QALS_EBELP dd_documentitemno,
          ifnull(QALS_EBELN, 'Not Set') dd_documentno,
          QALS_PRUEFLOS dd_inspectionlotno,
          QALS_ZEILE dd_MaterialDocItemNo,
          ifnull(QALS_MBLNR, 'Not Set') dd_MaterialDocNo,
          QALS_MJAHR dd_MaterialDocYear,
          ifnull(QALS_AUFNR, 'Not Set') dd_orderno,
          QALS_ETENR dd_ScheduleNo,
          (SELECT dim_accountcategoryid
             FROM dim_accountcategory ac
            WHERE ac.Category = ifnull(QALS_KNTTP, 'Not Set')
                  AND ac.RowIsCurrent = 1)
             Dim_AccountCategoryid,
          ifnull((SELECT dim_costcenterid
             FROM dim_costcenter cc
            WHERE     cc.Code = ifnull(QALS_KOSTL, 'Not Set')
                  AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set')
                  AND cc.RowIsCurrent = 1),1)
             dim_costcenterid,
          ifnull((SELECT dim_dateid
                   FROM dim_date ie
                  WHERE ie.DateValue = QALS_PAENDTERM
                        AND ie.CompanyCode = dp.CompanyCode
                                                AND QALS_PAENDTERM IS NOT NULL), 1)
             dim_dateidinspectionend,
          ifnull((SELECT dim_dateid
                   FROM dim_date dis
                  WHERE dis.DateValue = QALS_PASTRTERM
                        AND dis.CompanyCode = dp.CompanyCode
                                                AND QALS_PASTRTERM IS NOT NULL), 1)
             dim_dateidinspectionstart,
          ifnull((SELECT dim_dateid
                   FROM dim_date kd
                  WHERE kd.DateValue = QALS_GUELTIGAB
                        AND kd.CompanyCode = dp.CompanyCode
                                                AND QALS_GUELTIGAB IS NOT NULL), 1)
             dim_dateidkeydate,
          ifnull((SELECT dim_dateid
                      FROM dim_date lcd
                     WHERE lcd.DateValue = QALS_ENSTEHDAT
                           AND lcd.CompanyCode = dp.CompanyCode
                                                   AND QALS_ENSTEHDAT IS NOT NULL), 1)
             dim_dateidlotcreated,
          ifnull((SELECT dim_dateid
                   FROM dim_date pd
                  WHERE pd.DateValue = QALS_BUDAT
                        AND pd.companycode = dp.CompanyCode
                                                AND QALS_BUDAT IS NOT NULL), 1)
             dim_dateidposting,
          (SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.type = ifnull(QALS_BLART, 'Not Set')
                  AND dtt.RowIsCurrent = 1)
             dim_documenttypetextid,
          (SELECT dim_inspectionlotmiscid
             FROM dim_inspectionlotmisc
            WHERE     AutomaticInspectionLot = ifnull(QALS_STAT01, 'Not Set')
                  AND BatchManagementRequired = ifnull(QALS_XCHPF, 'Not Set')
                  AND CompleteInspection = ifnull(QALS_HPZ, 'Not Set')
                  AND DocumentationRequired = ifnull(QALS_STAT19, 'Not Set')
                  AND InspectionPlanRequired = ifnull(QALS_STAT20, 'Not Set')
                  AND InspectionStockQuantity = ifnull(QALS_INSMK, 'Not Set')
                  AND ShotTermInspectionComplete =
                         ifnull(QALS_STAT14, 'Not Set')
                  AND StockPostingsComplete = ifnull(QALS_STAT34, 'Not Set')
                  AND UsageDecisionMade = ifnull(QALS_STAT35, 'Not Set')
                  AND LotSkipped = ifnull(QALS_KZSKIPLOT, 'Not Set'))
             dim_inspectionlotmiscid,
          (SELECT dim_inspectionlotoriginid
             FROM dim_inspectionlotorigin ilo
            WHERE ilo.InspectionLotOriginCode =
                     ifnull(QALS_HERKUNFT, 'Not Set')
                  AND ilo.RowIsCurrent = 1)
             dim_inspectionlotoriginid,
          ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation isl
                  WHERE     isl.LocationCode = QALS_LAGORTVORG
                        AND isl.RowIsCurrent = 1
                        AND isl.Plant = dp.PlantCode
                                                AND QALS_LAGORTVORG IS NOT NULL), 1)
          dim_inspectionlotstoragelocationid,
          (SELECT dim_inspectionsamplestatusid
             FROM dim_inspectionsamplestatus iss
            WHERE iss.statuscode = ifnull(QALS_LVS_STIKZ, 'Not Set')
                  AND iss.RowIsCurrent = 1)
             dim_inspectionsamplestatusid,
          ifnull((SELECT dim_inspectionseverityid
             FROM dim_inspectionseverity isvr
            WHERE isvr.InspectionSeverityCode = QALS_PRSCHAERFE
                  AND isvr.RowIsCurrent = 1),1)
             dim_inspectionseverityid,
          ifnull((SELECT dim_inspectionstageid
             FROM dim_inspectionstage istg
            WHERE istg.InspectionStageCode = QALS_PRSTUFE
                  AND istg.InspectionStageRuleCode =
                         ifnull(QALS_DYNREGEL, 'Not Set')
                  AND istg.RowIsCurrent = 1),1)
             dim_inspectionstageid,
          (SELECT dim_inspectiontypeid
             FROM dim_inspectiontype ityp
            WHERE ityp.InspectionTypeCode = ifnull(QALS_ART, 'Not Set')
                  AND ityp.RowIsCurrent = 1)
             dim_inspectiontypeid,
          (SELECT dim_itemcategoryid
             FROM dim_itemcategory icc
            WHERE icc.CategoryCode = ifnull(QALS_PSTYP, 'Not Set')
                  AND icc.RowIsCurrent = 1)
             dim_itemcategoryid,
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure luom
            WHERE luom.UOM = ifnull(QALS_MENGENEINH, 'Not Set')
                  AND luom.RowIsCurrent = 1)
             dim_lotunitofmeasureid,
          ifnull((SELECT dim_vendorid
             FROM dim_vendor dv
            WHERE dv.VendorNumber = ifnull(QALS_HERSTELLER, 'Not Set')
                  AND dv.RowIsCurrent = 1),1)
             dim_manufacturerid,
          (SELECT dim_ObjectCategoryid
             FROM dim_ObjectCategory oc
            WHERE oc.ObjectCategoryCode = ifnull(QALS_OBTYP, 'Not Set')
                  AND oc.RowIsCurrent = 1)
             dim_ObjectCategoryid,
          dim_partid,
          dim_plantid,
          (SELECT dim_purchaseorgid
             FROM dim_purchaseorg po
            WHERE po.PurchaseOrgCode = ifnull(QALS_EKORG, 'Not Set')
                  AND po.RowIsCurrent = 1)
             dim_purchaseorgid,
          (SELECT dim_routeid
             FROM dim_route r
            WHERE r.RouteCode = ifnull(QALS_LS_ROUTE, 'Not Set')
                  AND r.RowIsCurrent = 1)
             dim_routeid,
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure uom
            WHERE uom.UOM = ifnull(QALS_EINHPROBE, 'Not Set')
                  AND uom.RowIsCurrent = 1)
             dim_sampleunitofmeasureid,
          (SELECT dim_specialstockid
             FROM dim_specialstock ss
            WHERE ss.specialstockindicator = ifnull(QALS_SOBKZ, 'Not Set')
                  AND ss.RowIsCurrent = 1)
             dim_specialstockid,
          (SELECT dim_statusprofileid
             FROM dim_statusprofile sp
            WHERE sp.StatusProfileCode = ifnull(QALS_STSMA, 'Not Set')
                  AND sp.RowIsCurrent = 1)
             dim_statusprofileid,
          ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation sl
                  WHERE sl.LocationCode = ifnull(QALS_LAGORTCHRG, 'Not Set')
                        AND sl.Plant = dp.PlantCode
                        AND sl.RowIsCurrent = 1
                                                AND QALS_LAGORTCHRG IS NOT NULL), 1)
          dim_storagelocationid,
          (SELECT dim_tasklisttypeid
             FROM dim_tasklisttype tlt
            WHERE tlt.TaskListTypeCode = ifnull(QALS_PLNTY, 'Not Set')
                  AND tlt.RowIsCurrent = 1)
             dim_tasklisttypeid,
          (SELECT dim_tasklistusageid
             FROM dim_tasklistusage tlu
            WHERE tlu.UsageCode = ifnull(QALS_PPLVERW, 'Not Set')
                  AND tlu.RowIsCurrent = 1)
             dim_tasklistusageid,
          ifnull((SELECT dim_vendorid
             FROM dim_vendor dv1
            WHERE dv1.VendorNumber = ifnull(QALS_LIFNR, 'Not Set')
                  AND dv1.RowIsCurrent = 1),1)
             dim_vendorid,
          ifnull((SELECT dim_warehousenumberid
             FROM dim_warehousenumber wn
            WHERE wn.WarehouseCode = ifnull(QALS_LGNUM, 'Not Set')
                  AND wn.RowIsCurrent = 1),1)
             dim_warehousenumberid,
          ifnull((SELECT dim_controllingareaid
             FROM dim_controllingarea ca
            WHERE ca.ControllingAreaCode = QALS_KOKRS),1) dim_controllingareaid,
ifnull((SELECT  pc.dim_profitcenterid FROM tmp_dim_profitcenter2 pc WHERE  pc.ControllingArea = QALS_KOKRS AND pc.ProfitCenterCode = QALS_PRCTR
AND pc.QALS_ERSTELDAT = QALS_ERSTELDAT ),1) dim_profitcenterid
      FROM tmp_ins2_inslot q
          INNER JOIN dim_plant dp
             ON dp.PlantCode = q.QALS_WERK AND dp.RowIsCurrent = 1
          INNER JOIN dim_part dpa
             ON     dpa.PartNumber = QALS_MATNR
                AND dpa.Plant = dp.PlantCode
                AND dpa.RowIsCurrent = 1*/



/* INSERT INTO tmp_pre_fact_inspectionlot(fact_inspectionlotid,
                               ct_actualinspectedqty,
                               ct_actuallotqty,
                               ct_blockedqty,
                               ct_inspectionlotqty,
                               ct_postedqty,
                               ct_qtydefective,
                               ct_qtyreturned,
                               ct_reserveqty,
                               ct_sampleqty,
                               ct_samplesizeqty,
                               ct_scrapqty,
                               ct_unrestrictedqty,
                               dd_ObjectNumber,
                               dd_batchno,
                               dd_changedby,
                               dd_createdby,
                               dd_documentitemno,
                               dd_documentno,
                               dd_inspectionlotno,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocNo,
                               dd_MaterialDocYear,
                               dd_orderno,
                               dd_ScheduleNo,
                               Dim_AccountCategoryid,
                               dim_costcenterid,
                               dim_dateidinspectionend,
                               dim_dateidinspectionstart,
                               dim_dateidkeydate,
                               dim_dateidlotcreated,
                               dim_dateidposting,
                               dim_documenttypetextid,
                               dim_inspectionlotmiscid,
                               dim_inspectionlotoriginid,
                               dim_inspectionlotstoragelocationid,
                               dim_inspectionsamplestatusid,
                               dim_inspectionseverityid,
                               dim_inspectionstageid,
                               dim_inspectiontypeid,
                               dim_itemcategoryid,
                               dim_lotunitofmeasureid,
                               dim_manufacturerid,
                               dim_ObjectCategoryid,
                               dim_partid,
                               dim_plantid,
                               dim_purchaseorgid,
                               dim_routeid,
                               dim_sampleunitofmeasureid,
                               dim_specialstockid,
                               dim_statusprofileid,
                               dim_storagelocationid,
                               dim_tasklisttypeid,
                               dim_tasklistusageid,
                               dim_vendorid,
                               dim_warehousenumberid,
                               dim_Controllingareaid,
                               dim_profitcenterid)*/

DROP TABLE IF EXISTS tmp_pre_fact_inspectionlot;
CREATE TABLE tmp_pre_fact_inspectionlot AS
SELECT (SELECT ifnull(max(fil.fact_inspectionlotid), 1) id FROM fact_inspectionlot fil) + row_number() over(order by '') as fact_inspectionlotid,
        QALS_LMENGEPR ct_actualinspectedqty,
        QALS_LMENGEIST ct_actuallotqty,
        QALS_LMENGE04 ct_blockedqty,
        QALS_LOSMENGE ct_inspectionlotqty,
        QALS_LMENGEZUB ct_postedqty,
        QALS_LMENGESCH ct_qtydefective,
        QALS_LMENGE07 ct_qtyreturned,
        QALS_LMENGE05 ct_reserveqty,
        QALS_LMENGE03 ct_sampleqty,
        QALS_GESSTICHPR ct_samplesizeqty,
        QALS_LMENGE02 ct_scrapqty,
        QALS_LMENGE01 ct_unrestrictedqty,
        ifnull(QALS_OBJNR,'Not Set') dd_ObjectNumber,
        ifnull(QALS_CHARG, 'Not Set') dd_batchno,
        ifnull(QALS_AENDERER, 'Not Set') dd_changedby,
        QALS_ERSTELLER dd_createdby,
        QALS_EBELP dd_documentitemno,
        ifnull(QALS_EBELN, 'Not Set') dd_documentno,
        QALS_PRUEFLOS dd_inspectionlotno,
        QALS_ZEILE dd_MaterialDocItemNo,
        ifnull(QALS_MBLNR, 'Not Set') dd_MaterialDocNo,
        QALS_MJAHR dd_MaterialDocYear,
        ifnull(QALS_AUFNR, 'Not Set') dd_orderno,
        QALS_ETENR dd_ScheduleNo,
       convert(BIGINT, 1) as Dim_AccountCategoryid,
       convert(BIGINT, 1) as dim_costcenterid,
       convert(BIGINT, 1) as dim_dateidinspectionend,
       convert(BIGINT, 1) as dim_dateidinspectionstart,
       convert(BIGINT, 1) as dim_dateidkeydate,
       convert(BIGINT, 1) as dim_dateidlotcreated,
       convert(BIGINT, 1) as dim_dateidposting,
       convert(BIGINT, 1) as dim_documenttypetextid,
       convert(BIGINT, 1) as dim_inspectionlotmiscid,
       convert(BIGINT, 1) as dim_inspectionlotoriginid,
       convert(BIGINT, 1) as dim_inspectionlotstoragelocationid,
       convert(BIGINT, 1) as dim_inspectionsamplestatusid,
       convert(BIGINT, 1) as dim_inspectionseverityid,
       convert(BIGINT, 1) as dim_inspectionstageid,
       convert(BIGINT, 1) as dim_inspectiontypeid,
       convert(BIGINT, 1) as dim_itemcategoryid,
       convert(BIGINT, 1) as dim_lotunitofmeasureid,
       convert(BIGINT, 1) as dim_manufacturerid,
       convert(BIGINT, 1) as dim_ObjectCategoryid,
       dim_partid as dim_partid,
       dim_plantid as dim_plantid,
       convert(BIGINT, 1) as dim_purchaseorgid,
       convert(BIGINT, 1) as dim_routeid,
       convert(BIGINT, 1) as dim_sampleunitofmeasureid,
       convert(BIGINT, 1) as dim_specialstockid,
       convert(BIGINT, 1) as dim_statusprofileid,
       convert(BIGINT, 1) as dim_storagelocationid,
       convert(BIGINT, 1) as dim_tasklisttypeid,
       convert(BIGINT, 1) as dim_tasklistusageid,
       convert(BIGINT, 1) as dim_vendorid,
       convert(BIGINT, 1) as dim_warehousenumberid,
       convert(BIGINT, 1) as dim_Controllingareaid,
       convert(BIGINT, 1) as dim_profitcenterid,
       convert(BIGINT, 1) AS dim_dateidrecordcreated,
       convert(BIGINT, 1) AS dim_dateidrecordchanged,
       convert(BIGINT, 1) AS dim_vendormasterid,
       convert(BIGINT, 1) AS dim_customerid,
     QALS_KNTTP,          -- used in dim_accountcategoryid
     QALS_KOSTL, QALS_KOKRS,    -- used in dim_costcenterid
     QALS_PAENDTERM, QALS_WERK, -- used in dim_dateidinspectionend
     QALS_PASTRTERM,            -- used in dim_dateidinspectionstart
     QALS_GUELTIGAB,            -- dim_dateidkeydate
     QALS_ENSTEHDAT,            -- dim_dateidlotcreated
     QALS_BUDAT,                -- dim_dateidposting
     QALS_BLART,        -- dim_documenttypetextid
     QALS_STAT01,QALS_XCHPF,QALS_HPZ,QALS_STAT19,QALS_STAT20,QALS_INSMK,QALS_STAT14,QALS_STAT34,QALS_STAT35,QALS_KZSKIPLOT,  -- dim_inspectionlotmiscid
     QALS_HERKUNFT,     -- dim_inspectionlotoriginid
     QALS_LAGORTVORG,   -- dim_inspectionlotstoragelocationid
     QALS_LVS_STIKZ,    -- dim_inspectionsamplestatusid
     QALS_PRSCHAERFE,   --  dim_inspectionseverityid
     QALS_PRSTUFE, QALS_DYNREGEL, -- dim_inspectionstageid
     QALS_ART,         -- dim_inspectiontypeid
     QALS_PSTYP,       -- dim_itemcategoryid
     QALS_MENGENEINH,  -- dim_lotunitofmeasureid
     QALS_HERSTELLER,  -- dim_manufacturerid
     QALS_OBTYP,       -- dim_ObjectCategoryid
     QALS_EKORG,       -- dim_purchaseorgid
     QALS_LS_ROUTE,    -- dim_routeid
     QALS_EINHPROBE,   -- dim_sampleunitofmeasureid
     QALS_SOBKZ,       --dim_specialstockid
     QALS_STSMA,       --dim_statusprofileid
     QALS_LAGORTCHRG,  --dim_storagelocationid
     QALS_PLNTY,       --dim_tasklisttypeid
     QALS_PPLVERW,     --dim_tasklistusageid
     QALS_LIFNR,       --dim_vendorid
     QALS_LGNUM, --dim_warehousenumberid
     QALS_PRCTR,QALS_ERSTELDAT,  --dim_profitcenterid, -- dim_dateidrecordcreated
     QALS_AENDERDAT, -- dim_dateidrecordchanged
     QALS_BUKRS, -- dim_vendormasterid
     QALS_KUNNR, -- dim_customerid
     ifnull(QALS_KTEXTLOS,'Not Set') as dd_shorttext -- APP-7073 missing data in short text


FROM tmp_ins2_inslot q
INNER JOIN dim_plant dp
           ON dp.PlantCode = q.QALS_WERK /*"APP-9894 IENE remove RowIsCurrent condition" AND dp.RowIsCurrent = 1*/
INNER JOIN dim_part dpa
           ON     dpa.PartNumber = q.QALS_MATNR
           AND dpa.Plant = dp.PlantCode
           /*APP-9894 IENE remove RowIsCurrent condition
           AND dpa.RowIsCurrent = 1*/;

UPDATE  tmp_pre_fact_inspectionlot f
SET f.Dim_AccountCategoryid = ac.dim_accountcategoryid
FROM tmp_pre_fact_inspectionlot f,
     dim_accountcategory ac
WHERE     ac.Category = ifnull(f.QALS_KNTTP, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND ac.RowIsCurrent = 1*/
    AND f.Dim_AccountCategoryid <> ac.dim_accountcategoryid;

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_costcenterid = cc.dim_costcenterid
FROM dim_costcenter cc, tmp_pre_fact_inspectionlot f
WHERE     cc.Code = ifnull(f.QALS_KOSTL, 'Not Set')
      AND cc.ControllingArea = ifnull(f.QALS_KOKRS, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND cc.RowIsCurrent = 1*/
      AND f.dim_costcenterid <> cc.dim_costcenterid;

 /* Madalina 30 Dec 2016 - plantcode_factory additional condition - BI-5085*/ 
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_dateidinspectionend = ie.dim_dateid
FROM tmp_pre_fact_inspectionlot f, dim_date ie, dim_plant dp
WHERE    ie.DateValue = f.QALS_PAENDTERM
     AND ie.CompanyCode = dp.CompanyCode
	 AND dp.plantcode = ie.plantcode_factory 
     AND f.QALS_PAENDTERM IS NOT NULL
     AND dp.PlantCode = ifnull(f.QALS_WERK,'Not Set')
     AND f.dim_dateidinspectionend <> ie.dim_dateid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_dateidinspectionstart = dis.dim_dateid
FROM tmp_pre_fact_inspectionlot f, dim_date dis, dim_plant dp
WHERE     dis.DateValue = f.QALS_PASTRTERM
      AND dis.CompanyCode = dp.CompanyCode
	  AND dp.plantcode = dis.plantcode_factory 
      AND f.QALS_PASTRTERM IS NOT NULL
      AND dp.PlantCode = f.QALS_WERK
      AND f.dim_dateidinspectionstart <> dis.dim_dateid;

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_dateidkeydate = kd.dim_dateid
FROM tmp_pre_fact_inspectionlot f, dim_date kd, dim_plant dp
WHERE     kd.DateValue = f.QALS_GUELTIGAB
      AND kd.CompanyCode = dp.CompanyCode
	  AND dp.plantcode = kd.plantcode_factory
      AND f.QALS_GUELTIGAB IS NOT NULL
      AND dp.PlantCode = f.QALS_WERK
      AND f.dim_dateidkeydate <> kd.dim_dateid;

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_dateidlotcreated = lcd.dim_dateid
FROM  tmp_pre_fact_inspectionlot f, dim_date lcd,dim_plant dp
WHERE lcd.DateValue = f.QALS_ENSTEHDAT
      AND lcd.CompanyCode = dp.CompanyCode
	  AND dp.plantcode = lcd.plantcode_factory
      AND f.QALS_ENSTEHDAT IS NOT NULL
      AND dp.PlantCode = f.QALS_WERK
      AND f.dim_dateidlotcreated <> lcd.dim_dateid;

 UPDATE tmp_pre_fact_inspectionlot f
 SET f.dim_dateidposting = pd.dim_dateid
 FROM tmp_pre_fact_inspectionlot f, dim_date pd,dim_plant dp
 WHERE     pd.DateValue = f.QALS_BUDAT
       AND pd.companycode = dp.CompanyCode
	   AND dp.plantcode = pd.plantcode_factory
       AND f.QALS_BUDAT IS NOT NULL
       AND dp.PlantCode = f.QALS_WERK
       AND f.dim_dateidposting <> pd.dim_dateid;

 UPDATE tmp_pre_fact_inspectionlot f
 SET f.dim_dateidrecordchanged = pd.dim_dateid
 FROM tmp_pre_fact_inspectionlot f, dim_date pd,dim_plant dp
 WHERE     pd.DateValue = ifnull(f.QALS_AENDERDAT,'0001-01-01')
       AND pd.companycode = dp.CompanyCode
	   AND dp.plantcode = pd.plantcode_factory
       AND f.QALS_BUDAT IS NOT NULL
       AND dp.PlantCode = f.QALS_WERK
       AND f.dim_dateidrecordchanged <> pd.dim_dateid;

 UPDATE tmp_pre_fact_inspectionlot f
 SET f.dim_dateidrecordcreated = pd.dim_dateid
 FROM tmp_pre_fact_inspectionlot f, dim_date pd,dim_plant dp
 WHERE     pd.DateValue = ifnull(f.QALS_ERSTELDAT,'0001-01-01')
       AND pd.companycode = dp.CompanyCode
	   AND dp.plantcode = pd.plantcode_factory
       AND f.QALS_BUDAT IS NOT NULL
       AND dp.PlantCode = f.QALS_WERK
       AND f.dim_dateidrecordcreated <> pd.dim_dateid;

 /* END  BI-5085*/
 
UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_documenttypetextid = dtt.dim_documenttypetextid
FROM tmp_pre_fact_inspectionlot f,dim_documenttypetext dtt
WHERE     dtt.type = ifnull(f.QALS_BLART, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND dtt.RowIsCurrent = 1*/
      AND f.dim_documenttypetextid <> dtt.dim_documenttypetextid;

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionlotmiscid = dins.dim_inspectionlotmiscid
FROM tmp_pre_fact_inspectionlot f, dim_inspectionlotmisc dins
WHERE     dins.AutomaticInspectionLot = ifnull(f.QALS_STAT01, 'Not Set')
      AND dins.BatchManagementRequired = ifnull(f.QALS_XCHPF, 'Not Set')
      AND dins.CompleteInspection = ifnull(f.QALS_HPZ, 'Not Set')
      AND dins.DocumentationRequired = ifnull(f.QALS_STAT19, 'Not Set')
      AND dins.InspectionPlanRequired = ifnull(f.QALS_STAT20, 'Not Set')
      AND dins.InspectionStockQuantity = ifnull(f.QALS_INSMK, 'Not Set')
      AND dins.ShotTermInspectionComplete = ifnull(f.QALS_STAT14, 'Not Set')
      AND dins.StockPostingsComplete = ifnull(f.QALS_STAT34, 'Not Set')
      AND dins.UsageDecisionMade = ifnull(f.QALS_STAT35, 'Not Set')
      AND dins.LotSkipped = ifnull(f.QALS_KZSKIPLOT, 'Not Set')
    AND f.dim_inspectionlotmiscid <> dins.dim_inspectionlotmiscid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionlotoriginid = ilo.dim_inspectionlotoriginid
FROM dim_inspectionlotorigin ilo, tmp_pre_fact_inspectionlot f
WHERE     ilo.InspectionLotOriginCode = ifnull(f.QALS_HERKUNFT, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND ilo.RowIsCurrent = 1*/
    AND f.dim_inspectionlotoriginid <> ilo.dim_inspectionlotoriginid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionlotstoragelocationid = isl.dim_storagelocationid
FROM tmp_pre_fact_inspectionlot f, dim_storagelocation isl,dim_plant dp
WHERE     isl.LocationCode = f.QALS_LAGORTVORG
      /*APP-9894 IENE remove RowIsCurrent condition
      AND isl.RowIsCurrent = 1*/
      AND isl.Plant = dp.PlantCode
      AND f.QALS_LAGORTVORG IS NOT NULL
      AND dp.PlantCode = f.QALS_WERK
      AND f.dim_inspectionlotstoragelocationid <> isl.dim_storagelocationid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionsamplestatusid = iss.dim_inspectionsamplestatusid
FROM dim_inspectionsamplestatus iss,tmp_pre_fact_inspectionlot f
WHERE     iss.statuscode = ifnull(f.QALS_LVS_STIKZ, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND iss.RowIsCurrent = 1*/
      AND f.dim_inspectionsamplestatusid <> iss.dim_inspectionsamplestatusid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionseverityid = isvr.dim_inspectionseverityid
FROM dim_inspectionseverity isvr, tmp_pre_fact_inspectionlot f
WHERE     isvr.InspectionSeverityCode = f.QALS_PRSCHAERFE
      /*APP-9894 IENE remove RowIsCurrent condition
      AND isvr.RowIsCurrent = 1*/
      AND f.dim_inspectionseverityid <> isvr.dim_inspectionseverityid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectionstageid = istg.dim_inspectionstageid
FROM dim_inspectionstage istg, tmp_pre_fact_inspectionlot f
 WHERE     istg.InspectionStageCode = f.QALS_PRSTUFE
       AND istg.InspectionStageRuleCode = ifnull(f.QALS_DYNREGEL, 'Not Set')
       /*APP-9894 IENE remove RowIsCurrent condition
       AND istg.RowIsCurrent = 1*/
       AND f.dim_inspectionstageid <> istg.dim_inspectionstageid;




UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_inspectiontypeid = ityp.dim_inspectiontypeid
FROM dim_inspectiontype ityp, tmp_pre_fact_inspectionlot f
WHERE     ityp.InspectionTypeCode = ifnull(f.QALS_ART, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND ityp.RowIsCurrent = 1*/
      AND f.dim_inspectiontypeid <> ityp.dim_inspectiontypeid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_itemcategoryid = icc.dim_itemcategoryid
FROM dim_itemcategory icc,tmp_pre_fact_inspectionlot f
WHERE     icc.CategoryCode = ifnull(f.QALS_PSTYP, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND icc.RowIsCurrent = 1*/
      AND f.dim_itemcategoryid <> icc.dim_itemcategoryid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_lotunitofmeasureid = luom.dim_unitofmeasureid
FROM dim_unitofmeasure luom, tmp_pre_fact_inspectionlot f
WHERE     luom.UOM = ifnull(f.QALS_MENGENEINH, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND luom.RowIsCurrent = 1*/
      AND f.dim_lotunitofmeasureid <> luom.dim_unitofmeasureid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_manufacturerid = dv.dim_vendorid
FROM dim_vendor dv, tmp_pre_fact_inspectionlot f
WHERE     dv.VendorNumber = ifnull(f.QALS_HERSTELLER, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND dv.RowIsCurrent = 1*/
      AND  f.dim_manufacturerid <> dv.dim_vendorid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_ObjectCategoryid = oc.dim_ObjectCategoryid
FROM dim_ObjectCategory oc, tmp_pre_fact_inspectionlot f
WHERE     oc.ObjectCategoryCode = ifnull(f.QALS_OBTYP, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND oc.RowIsCurrent = 1*/
      AND f.dim_ObjectCategoryid <> oc.dim_ObjectCategoryid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_purchaseorgid = po.dim_purchaseorgid
FROM dim_purchaseorg po, tmp_pre_fact_inspectionlot f
WHERE po.PurchaseOrgCode = ifnull(f.QALS_EKORG, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND po.RowIsCurrent = 1*/
      AND f.dim_purchaseorgid <> po.dim_purchaseorgid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_routeid = r.dim_routeid
FROM tmp_pre_fact_inspectionlot f, dim_route r
WHERE     r.RouteCode = ifnull(f.QALS_LS_ROUTE, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND r.RowIsCurrent = 1*/
      AND f.dim_routeid <> r.dim_routeid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_sampleunitofmeasureid = uom.dim_unitofmeasureid
FROM dim_unitofmeasure uom, tmp_pre_fact_inspectionlot f
WHERE      uom.UOM = ifnull(f.QALS_EINHPROBE, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND uom.RowIsCurrent = 1*/
      AND f.dim_sampleunitofmeasureid <> uom.dim_unitofmeasureid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_specialstockid = ss.dim_specialstockid
FROM  dim_specialstock ss, tmp_pre_fact_inspectionlot f
WHERE     ss.specialstockindicator = ifnull(f.QALS_SOBKZ, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND ss.RowIsCurrent = 1*/
      AND f.dim_specialstockid <> ss.dim_specialstockid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_statusprofileid = sp.dim_statusprofileid
FROM dim_statusprofile sp , tmp_pre_fact_inspectionlot f
 WHERE    sp.StatusProfileCode = ifnull(f.QALS_STSMA, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND sp.RowIsCurrent = 1*/
      AND f.dim_statusprofileid <> sp.dim_statusprofileid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_storagelocationid = sl.dim_storagelocationid
FROM dim_storagelocation sl, tmp_pre_fact_inspectionlot f, dim_plant dp
WHERE    sl.LocationCode = ifnull(f.QALS_LAGORTCHRG, 'Not Set')
     AND sl.Plant = dp.PlantCode
     /*APP-9894 IENE remove RowIsCurrent condition
     AND sl.RowIsCurrent = 1*/
     AND f.QALS_LAGORTCHRG IS NOT NULL
     AND dp.PlantCode = f.QALS_WERK
     AND f.dim_storagelocationid <> sl.dim_storagelocationid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_tasklisttypeid = tlt.dim_tasklisttypeid
FROM dim_tasklisttype tlt,tmp_pre_fact_inspectionlot f
WHERE     tlt.TaskListTypeCode = ifnull(f.QALS_PLNTY, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND tlt.RowIsCurrent = 1*/
      AND f.dim_tasklisttypeid <> tlt.dim_tasklisttypeid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_tasklistusageid = tlu.dim_tasklistusageid
FROM dim_tasklistusage tlu,tmp_pre_fact_inspectionlot f
WHERE     tlu.UsageCode = ifnull(f.QALS_PPLVERW, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND tlu.RowIsCurrent = 1*/
      AND f.dim_tasklistusageid <> tlu.dim_tasklistusageid;



UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_vendorid  = dv1.dim_vendorid
FROM dim_vendor dv1, tmp_pre_fact_inspectionlot f
WHERE    dv1.VendorNumber = ifnull(f.QALS_LIFNR, 'Not Set')
     /*APP-9894 IENE remove RowIsCurrent condition
     AND dv1.RowIsCurrent = 1*/
     AND f.dim_vendorid  <> dv1.dim_vendorid;

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_vendormasterid = dv2.dim_vendormasterid
FROM dim_vendormaster dv2,tmp_pre_fact_inspectionlot f
WHERE dv2.VendorNumber = ifnull(QALS_LIFNR, 'Not Set')
AND dv2.companycode = ifnull(QALS_BUKRS, 'Not Set')
      AND f.dim_vendormasterid <> dv2.dim_vendormasterid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_warehousenumberid = wn.dim_warehousenumberid
FROM dim_warehousenumber wn, tmp_pre_fact_inspectionlot f
WHERE     wn.WarehouseCode = ifnull(f.QALS_LGNUM, 'Not Set')
      /*APP-9894 IENE remove RowIsCurrent condition
      AND wn.RowIsCurrent = 1*/
      AND f.dim_warehousenumberid <> wn.dim_warehousenumberid;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_controllingareaid = ca.dim_controllingareaid
FROM dim_controllingarea ca,tmp_pre_fact_inspectionlot f
WHERE    ca.ControllingAreaCode = f.QALS_KOKRS
     AND f.dim_controllingareaid <> ca.dim_controllingareaid;

UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_profitcenterid = pc.dim_profitcenterid
FROM tmp_pre_fact_inspectionlot f, tmp_dim_profitcenter2 pc
WHERE      pc.ControllingArea = f.QALS_KOKRS
     AND pc.ProfitCenterCode = f.QALS_PRCTR
       AND pc.QALS_ERSTELDAT = f.QALS_ERSTELDAT
       AND f.dim_profitcenterid <> pc.dim_profitcenterid ;


UPDATE tmp_pre_fact_inspectionlot f
SET f.dim_customerid = c.dim_customerid
FROM dim_customer c,tmp_pre_fact_inspectionlot f
WHERE    c.customernumber = ifnull(QALS_KUNNR,'Not Set')
     AND f.dim_customerid <> c.dim_customerid;

DROP TABLE IF EXISTS tmp_del1_inslot;
DROP TABLE IF EXISTS tmp_ins1_inslot;
DROP TABLE IF EXISTS tmp_ins2_inslot;
DROP TABLE IF EXISTS tmp_dim_profitcenter2;
DROP TABLE IF EXISTS tmp_dim_profitcenter;





INSERT INTO fact_inspectionlot(fact_inspectionlotid,
                               ct_actualinspectedqty,
                               ct_actuallotqty,
                               ct_blockedqty,
                               ct_inspectionlotqty,
                               ct_postedqty,
                               ct_qtydefective,
                               ct_qtyreturned,
                               ct_reserveqty,
                               ct_sampleqty,
                               ct_samplesizeqty,
                               ct_scrapqty,
                               ct_unrestrictedqty,
                               dd_ObjectNumber,
                               dd_batchno,
                               dd_changedby,
                               dd_createdby,
                               dd_documentitemno,
                               dd_documentno,
                               dd_inspectionlotno,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocNo,
                               dd_MaterialDocYear,
                               dd_orderno,
                               dd_ScheduleNo,
                               Dim_AccountCategoryid,
                               dim_costcenterid,
                               dim_dateidinspectionend,
                               dim_dateidinspectionstart,
                               dim_dateidkeydate,
                               dim_dateidlotcreated,
                               dim_dateidposting,
                               dim_documenttypetextid,
                               dim_inspectionlotmiscid,
                               dim_inspectionlotoriginid,
                               dim_inspectionlotstoragelocationid,
                               dim_inspectionsamplestatusid,
                               dim_inspectionseverityid,
                               dim_inspectionstageid,
                               dim_inspectiontypeid,
                               dim_itemcategoryid,
                               dim_lotunitofmeasureid,
                               dim_manufacturerid,
                               dim_ObjectCategoryid,
                               dim_partid,
                               dim_plantid,
                               dim_purchaseorgid,
                               dim_routeid,
                               dim_sampleunitofmeasureid,
                               dim_specialstockid,
                               dim_statusprofileid,
                               dim_storagelocationid,
                               dim_tasklisttypeid,
                               dim_tasklistusageid,
                               dim_vendorid,
                               dim_warehousenumberid,
                               dim_Controllingareaid,
                               dim_profitcenterid,
             dim_vendormasterid,
             dim_customerid,
             dim_dateidrecordcreated,
             dim_dateidrecordchanged,
	     dd_shorttext)

SELECT  fact_inspectionlotid,
        ct_actualinspectedqty,
        ct_actuallotqty,
        ct_blockedqty,
        ct_inspectionlotqty,
        ct_postedqty,
        ct_qtydefective,
        ct_qtyreturned,
        ct_reserveqty,
        ct_sampleqty,
        ct_samplesizeqty,
        ct_scrapqty,
        ct_unrestrictedqty,
        dd_ObjectNumber,
        dd_batchno,
        dd_changedby,
        dd_createdby,
        dd_documentitemno,
        dd_documentno,
        dd_inspectionlotno,
        dd_MaterialDocItemNo,
        dd_MaterialDocNo,
        dd_MaterialDocYear,
        dd_orderno,
        dd_ScheduleNo,
        Dim_AccountCategoryid,
        dim_costcenterid,
        dim_dateidinspectionend,
        dim_dateidinspectionstart,
        dim_dateidkeydate,
        dim_dateidlotcreated,
        dim_dateidposting,
        dim_documenttypetextid,
        dim_inspectionlotmiscid,
        dim_inspectionlotoriginid,
        dim_inspectionlotstoragelocationid,
        dim_inspectionsamplestatusid,
        dim_inspectionseverityid,
        dim_inspectionstageid,
        dim_inspectiontypeid,
        dim_itemcategoryid,
        dim_lotunitofmeasureid,
        dim_manufacturerid,
        dim_ObjectCategoryid,
        dim_partid,
        dim_plantid,
        dim_purchaseorgid,
        dim_routeid,
        dim_sampleunitofmeasureid,
        dim_specialstockid,
        dim_statusprofileid,
        dim_storagelocationid,
        dim_tasklisttypeid,
        dim_tasklistusageid,
        dim_vendorid,
        dim_warehousenumberid,
        dim_Controllingareaid,
        dim_profitcenterid,
  dim_vendormasterid,
  dim_customerid,
  dim_dateidrecordcreated,
  dim_dateidrecordchanged,
  dd_shorttext
FROM tmp_pre_fact_inspectionlot;

/* Octavian: Every Angle Transition Addons */
update fact_inspectionlot ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp
, fact_inspectionlot ia
where ia.dim_partid = dp.dim_partid and
ia.dd_batchno = b.batchnumber and
dp.partnumber = b.partnumber
and b.plantcode = dp.plant
and ia.dim_batchid <> b.dim_batchid;
/* Octavian: Every Angle Transition Addons */

/* Octavian : Every Angle */
UPDATE fact_inspectionlot il
SET il.dim_vendorpurchasingid = vp.dim_vendorpurchasingid,
il.dw_update_date = CURRENT_TIMESTAMP
FROM QALS q,dim_vendorpurchasing vp
, fact_inspectionlot il
WHERE vp.purchasingorg = ifnull(q.QALS_EKORG,'Not Set')
AND vp.vendornumber = ifnull(q.QALS_LIFNR,'Not Set')
AND il.dd_inspectionlotno = q.QALS_PRUEFLOS
AND il.dim_vendorpurchasingid <> vp.dim_vendorpurchasingid;

UPDATE fact_inspectionlot il
SET  il.dd_usagedecisionhasbeenmade = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j
, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0218' and j.JEST_INACT is NULL
AND  il.dd_usagedecisionhasbeenmade <> 'X';

UPDATE fact_inspectionlot il
SET  il.dd_allinspcompleted = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j
, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0217' and j.JEST_INACT is NULL
AND  il.dd_allinspcompleted <> 'X';

UPDATE fact_inspectionlot il
SET  il.dd_planspecassigned = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j
, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0205' and j.JEST_INACT is NULL
AND  il.dd_planspecassigned <> 'X';

UPDATE fact_inspectionlot il
SET  il.dd_resultsconfirmed = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j
, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0213' and j.JEST_INACT is NULL
AND  il.dd_resultsconfirmed <> 'X';

UPDATE fact_inspectionlot il
SET  il.dd_lotcanceled = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j
, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0224' and j.JEST_INACT is NULL
AND  il.dd_lotcanceled <> 'X';

/* Liviu Ionescu add dd_QuantityPostingRequired for BI-3410 */
UPDATE fact_inspectionlot il
SET  il.dd_QuantityPostingRequired = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0203' and j.JEST_INACT is NULL
AND  il.dd_QuantityPostingRequired <> 'X';

UPDATE fact_inspectionlot il
SET  il.dd_inspectionactive = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j
, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0212' and j.JEST_INACT is NULL
AND  il.dd_inspectionactive <> 'X';

UPDATE fact_inspectionlot il
SET il.dd_releaseddate = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'I0002'
and j.jcds_chgnr = '1'
AND il.dd_releaseddate <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_usagedecisionhasbeenmadedate = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'I0218'
and j.jcds_chgnr = '1'
AND il.dd_usagedecisionhasbeenmadedate <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_allinspcompleteddate = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'I0217'
and j.jcds_chgnr = '1'
AND il.dd_allinspcompleteddate <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_stockpostingcompldate = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'I0220'
and j.jcds_chgnr = '1'
AND il.dd_stockpostingcompldate <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_inspectionactivedate = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'I0212'
and j.jcds_chgnr = '1'
AND il.dd_inspectionactivedate <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_resultsconfirmeddate = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'I0213'
and j.jcds_chgnr = '1'
AND il.dd_resultsconfirmeddate <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_dateuserstatusqcco = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'E0004'
and j.jcds_chgnr = '1'
AND il.dd_dateuserstatusqcco <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_userstatusqcco = ifnull(j.JCDS_USNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'E0004'
and j.jcds_chgnr = '1'
AND il.dd_userstatusqcco <> ifnull(j.JCDS_USNAM,'Not Set');

UPDATE fact_inspectionlot il
SET il.dd_dateuserstatussamr = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'E0002'
and j.jcds_chgnr = '1'
AND il.dd_dateuserstatussamr <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_userstatussamr = ifnull(j.JCDS_USNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'E0002'
and j.jcds_chgnr = '1'
AND il.dd_userstatussamr <> ifnull(j.JCDS_USNAM,'Not Set');

UPDATE fact_inspectionlot il
SET il.dd_dateuserstatuscrtd = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'E0001'
and j.jcds_chgnr = '1'
AND il.dd_dateuserstatuscrtd <> ifnull(j.JCDS_UDATE,'0001-01-01');

UPDATE fact_inspectionlot il
SET il.dd_userstatuscrtd = ifnull(j.JCDS_USNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'E0001'
and j.jcds_chgnr = '1'
AND il.dd_userstatuscrtd <> ifnull(j.JCDS_USNAM,'Not Set');

UPDATE fact_inspectionlot il
SET il.dd_userstatusinsc = ifnull(j.JCDS_USNAM,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'E0003'
and j.jcds_chgnr = '1'
AND il.dd_userstatusinsc <> ifnull(j.JCDS_USNAM,'Not Set');

UPDATE fact_inspectionlot il
SET il.dd_dateuserstatusinsc = ifnull(j.JCDS_UDATE,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM JCDS_QALS j
, fact_inspectionlot il
WHERE j.JCDS_OBJNR = il.dd_objectnumber
AND j.JCDS_STAT = 'E0003'
and j.jcds_chgnr = '1'
AND il.dd_dateuserstatusinsc <> ifnull(j.JCDS_UDATE,'0001-01-01');
/* Octavian : Every Angle */

/*04 Nov 2016- Georgiana- moving these changes in the custom script due to refresh issues*/
/* 07 Jul 2016 Georgiana Adding  Profile Fields  according to BI-3414*/

/*Profile Columns*/
/*ZQM00001*/
/* Madalina 14 Sep 2016 - correct conditions - BI-3414*/
/*MERGE INTO fact_inspectionlot il 
USING (
SELECT DISTINCT il.fact_inspectionlotid, FIRST_VALUE(ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')) 
						OVER (PARTITION BY TJ30T_STSMA ORDER BY TJ30T_STSMA desc) AS dd_IntervetStdInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00001'
/* AND dd_IntervetSchInsplotP <> ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set') 
/*AND dd_IntervetStdInsplotP <> ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')
) src
ON il.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE
SET il.dd_IntervetStdInsplotP = src.dd_IntervetStdInsplotP
where il.dd_IntervetStdInsplotP <> src.dd_IntervetStdInsplotP*/

/*ZQM00002*/
/*MERGE INTO fact_inspectionlot il 
USING (
SELECT DISTINCT il.fact_inspectionlotid, FIRST_VALUE(ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')) 
						OVER (PARTITION BY TJ30T_STSMA ORDER BY TJ30T_STSMA desc) AS dd_IntervetaUDInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00002'
/* AND dd_IntervetSchInsplotP <> ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set') 
/*AND dd_IntervetaUDInsplotP <> ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')
) src
ON il.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE
SET il.dd_IntervetaUDInsplotP = src.dd_IntervetaUDInsplotP
where il.dd_IntervetaUDInsplotP <> src.dd_IntervetaUDInsplotP*/

/*ZQM00003*/
/*MERGE INTO fact_inspectionlot il 
USING (
SELECT DISTINCT il.fact_inspectionlotid, FIRST_VALUE(ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')) 
						OVER (PARTITION BY TJ30T_STSMA ORDER BY TJ30T_STSMA desc) AS dd_IntervetSchInsplotP
FROM JEST_QALS j, TJ30T t, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber AND j.JEST_INACT is NULL
AND jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND tj30t_stsma='ZQM00003'
/*AND dd_IntervetSchInsplotP <> ifnull(concat(tj30t_txt04,' (',tj30t_txt30, ')'),'Not Set')
) src
ON il.fact_inspectionlotid = src.fact_inspectionlotid
WHEN MATCHED THEN UPDATE
SET il.dd_IntervetSchInsplotP = src.dd_IntervetSchInsplotP
where il.dd_IntervetSchInsplotP <> src.dd_IntervetSchInsplotP*/

/* I0211 Additional column */
/*UPDATE fact_inspectionlot il
SET  il.dd_inspinstructionprinted = 'X',
dw_update_date = CURRENT_TIMESTAMP
FROM JEST_QALS j, fact_inspectionlot il
WHERE j.JEST_OBJNR = il.dd_objectnumber
and j.JEST_STAT = 'I0211' and j.JEST_INACT is NULL
AND  il.dd_inspinstructionprinted <> 'X'*/

/*Date Columns*/

/*drop table if exists tmp_distinct_jcds
Create table tmp_distinct_jcds as 
select t.*,row_number() over (partition by JCDS_OBJNR,QALS_STSMA order by JCDS_UDATE desc) as rn from (
select distinct jq.JCDS_OBJNR, jq.JCDS_stat, jq.JCDS_UDATE,qals_stsma, max (jcds_chgnr) as jcds_chgnr
FROM JCDS_QALS jq, JEST_QALS j,tj30t t
WHERE  jest_stat=tj30t_estat
AND qals_stsma=tj30t_stsma
AND j.JEST_OBJNR = jq.JCDS_OBJNR
and jq.JCDS_stat=j.jest_stat
and qals_stsma in ('ZQM00001','ZQM00002','ZQM00003')
Group BY jq.JCDS_OBJNR, jq.JCDS_stat, jq.JCDS_UDATE,qals_stsma) t*/


/*ZQM00001*/
/*Update fact_inspectionlot il
SET dim_dateidintervetstd = dt.dim_dateid
FROM  tmp_distinct_jcds jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE dt.DateValue = jq.JCDS_UDATE
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode
AND il.dim_plantid = dp.dim_plantid
AND jq.JCDS_OBJNR = il.dd_objectnumber
AND qals_stsma='ZQM00001'
AND rn = 1
AND dim_dateidintervetstd <> dt.dim_dateid*/

/*ZQM00002*/
/*Update fact_inspectionlot il
SET dim_dateidintervetaud = dt.dim_dateid
FROM  tmp_distinct_jcds jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE dt.DateValue = jq.JCDS_UDATE
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode
AND il.dim_plantid = dp.dim_plantid
AND jq.JCDS_OBJNR = il.dd_objectnumber
AND qals_stsma='ZQM00002'
AND rn = 1
AND dim_dateidintervetaud <> dt.dim_dateid*/

/*ZQM00003*/
/*Update fact_inspectionlot il
SET dim_dateidintervetsch = dt.dim_dateid
FROM  tmp_distinct_jcds jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE dt.DateValue = jq.JCDS_UDATE
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode
AND il.dim_plantid = dp.dim_plantid
AND jq.JCDS_OBJNR = il.dd_objectnumber
AND qals_stsma='ZQM00003'
AND rn = 1
AND dim_dateidintervetsch <> dt.dim_dateid*/

/*I0211*/
/*drop table if exists tmp_distinct_jcds
Create table tmp_distinct_jcds as
select t.*, row_number() over (partition by JCDS_OBJNR order by JCDS_UDATE desc) as rn from ( 
select distinct jq.JCDS_OBJNR, jq.JCDS_stat, jq.JCDS_UDATE, max (jcds_chgnr) as jcds_chgnr
FROM JCDS_QALS jq, JEST_QALS j
WHERE  
j.JEST_OBJNR = jq.JCDS_OBJNR
and jq.JCDS_stat=j.jest_stat
and jq.JCDS_stat='I0211'
Group BY jq.JCDS_OBJNR, jq.JCDS_stat, jq.JCDS_UDATE) t*/

/*Update fact_inspectionlot il
SET dim_dateidinspintructionprinted = dt.dim_dateid
FROM  tmp_distinct_jcds jq, Dim_date dt, dim_plant dp, fact_inspectionlot il
WHERE dt.DateValue = jq.JCDS_UDATE
AND dt.CompanyCode = dp.CompanyCode
and dt.plantcode_factory = dp.plantcode
AND il.dim_plantid = dp.dim_plantid
AND jq.JCDS_OBJNR = il.dd_objectnumber
AND jq.JCDS_stat='I0211'
AND rn = 1
AND dim_dateidinspintructionprinted <> dt.dim_dateid*/
/* 07 Jul 2016 End Of changes*/
/*04 Nov 2016 End of changes*/

DROP TABLE IF EXISTS tmp_del1_inslot;
DROP TABLE IF EXISTS tmp_ins1_inslot;
DROP TABLE IF EXISTS tmp_ins2_inslot;
DROP TABLE IF EXISTS tmp_dim_profitcenter2;
DROP TABLE IF EXISTS tmp_dim_profitcenter;
DROP TABLE IF EXISTS tmp_insplot_forinsert;

/* Added std_exchangerate_dateid by FPOPESCU, on 05 January 2016 */

UPDATE fact_inspectionlot am
  SET am.std_exchangerate_dateid = dt.dim_dateid
FROM fact_inspectionlot am INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
     INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
  WHERE dt.datevalue = current_date
AND am.std_exchangerate_dateid <> dt.dim_dateid;

/*Alin 14 Oct 2016 Added dd_userstatusQALS to BI-4262*/
MERGE INTO  fact_inspectionlot il
USING 
(
SELECT DISTINCT il.FACT_INSPECTIONLOTID,first_value(ifnull(tj.TJ30T_TXT04, 'Not Set')) over (partition by JEST_OBJNR, QALS_AUFNR order by JEST_STAT asc) as dd_userstatusQALS
FROM fact_inspectionlot il, JEST_JSTO_QALS j1, tj30t tj
WHERE  il.dd_ObjectNumber = J1.JEST_OBJNR
AND il.dd_orderno = IFNULL(J1.QALS_AUFNR,'Not Set')
AND J1.JSTO_STSMA = TJ.TJ30T_STSMA
AND TJ.TJ30T_ESTAT = j1.JEST_STAT
AND JEST_STAT LIKE 'E%'
AND il.dd_userstatusQALS <> ifnull(tj.TJ30T_TXT04, 'Not Set')
) t
ON t.FACT_INSPECTIONLOTID = il.FACT_INSPECTIONLOTID
WHEN MATCHED THEN UPDATE
SET il.dd_userstatusQALS = t.dd_userstatusQALS;

/*Alin 1 Nov 2016 Added dd_systemstatusAUFK to BI-4262*/
MERGE INTO  fact_productionorder po
USING 
(
SELECT
-- DISTINCT 
po.fact_productionorderid,
group_concat(tj.TJ02T_TXT04 SEPARATOR ' ') as dd_systemstatusAUFK 
FROM fact_productionorder po, JEST_JSTO_AUFK j1, tj02t tj
WHERE  po.dd_ObjectNumber = J1.JEST_OBJNR
AND po.dd_ordernumber = IFNULL(J1.AUFK_AUFNR,'Not Set')
AND TJ.TJ02T_ISTAT = j1.JEST_STAT
AND JEST_STAT LIKE 'I%'
AND po.dd_userstatusAUFK <> ifnull(tj.TJ02T_TXT04, 'Not Set')
GROUP BY  po.fact_productionorderid
) t
ON t.fact_productionorderid = po.fact_productionorderid
WHEN MATCHED THEN UPDATE
SET po.dd_systemstatusAUFK = t.dd_systemstatusAUFK;


/* Yogini 3 Nov 2016 Added ct_grprocessingtime_purchaseord and ct_grprocessingtime_prodord according to BI-4590 */
merge into fact_inspectionlot f_il
using (select distinct f_il.fact_inspectionlotid,e.EKPO_WEBAZ
	from fact_inspectionlot f_il, EKKO_EKPO_EKET e
	where f_il.dd_documentno = ifnull(e.EKPO_EBELN, 'Not Set')
	and f_il.dd_documentitemno = ifnull(e.EKPO_EBELP, 0)
	and f_il.ct_grprocessingtime_purchaseord <> ifnull(e.EKPO_WEBAZ, 0)) as sc
on f_il.fact_inspectionlotid = sc.fact_inspectionlotid
when matched then update
set f_il.ct_grprocessingtime_purchaseord = ifnull(sc.EKPO_WEBAZ, 0);

merge into fact_inspectionlot f_il
using (select distinct fact_inspectionlotid,AFPO_WEBAZ 
	from fact_inspectionlot f_il, AFKO_AFPO_AUFK a
	where f_il.dd_orderno = ifnull(a.AFKO_AUFNR, 'Not Set')
	and f_il.ct_grprocessingtime_prodord <> ifnull(a.AFPO_WEBAZ, 0)) as sc
on f_il.fact_inspectionlotid = sc.fact_inspectionlotid
when matched then update 
set f_il.ct_grprocessingtime_prodord = ifnull(sc.AFPO_WEBAZ, 0);

/* 25 Jan 2017 Georgiana Adding ct_LROT_Hit_Manual and ct_LROT_Miss_Manual according to BI-5307*/

merge into fact_inspectionlot f
using (
select distinct fact_inspectionlotid,LROT_Hit_Manual,LROT_Miss_Manual from 	dim_date d, fact_inspectionlot f,LROT_manualupload stg, dim_plant pl
where
d.dim_dateid=dim_targetreleasedateid 
and pl.plantcode=stg.plant_code
and pl.dim_plantid=f.dim_plantid
and calendarweekid=stg.period) t
on t.fact_inspectionlotid=f.fact_inspectionlotid
when matched then update set 
ct_LROT_Hit_Manual =ifnull(t.LROT_Hit_Manual,0),
ct_LROT_Miss_Manual= ifnull(t.LROT_Miss_Manual,0);

/*START BI-5163 Alin  30.01.2017*/
UPDATE fact_inspectionlot am
SET am.dim_dateiduserstcrtdlastset = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'E0001'
and j.jcds_chgnr = '1'
AND am.dim_dateiduserstcrtdlastset <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateiduserstINSClastset = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'E0003'
and j.jcds_chgnr = '1'
AND am.dim_dateiduserstINSClastset <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateiduserstQCCOlastset = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'E0004'
and j.jcds_chgnr = '1'
AND am.dim_dateiduserstQCCOlastset <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateiduserstSAMRlastset = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'E0002'
and j.jcds_chgnr = '1'
AND am.dim_dateiduserstSAMRlastset <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateidallinspcompleteddt = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'I0217'
and j.jcds_chgnr = '1'
AND am.dim_dateidallinspcompleteddt <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateidinspectionactivedt = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'I0212'
and j.jcds_chgnr = '1'
AND am.dim_dateidinspectionactivedt <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateidreleaseddatedt = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'I0002'
and j.jcds_chgnr = '1'
AND am.dim_dateidreleaseddatedt <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateidresultsconfirmeddt = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'I0213'
and j.jcds_chgnr = '1'
AND am.dim_dateidresultsconfirmeddt <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateidstockpostingcompldt = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'I0220'
and j.jcds_chgnr = '1'
AND am.dim_dateidstockpostingcompldt <> dt.dim_dateid;

UPDATE fact_inspectionlot am
SET am.dim_dateidusagedecisionmadedt = dt.dim_dateid
FROM fact_inspectionlot am 
INNER JOIN dim_plant p ON am.dim_plantid = p.dim_plantid
INNER JOIN dim_date dt ON dt.companycode = p.companycode and dt.plantcode_factory=p.plantcode
INNER JOIN JCDS_QALS j ON j.JCDS_OBJNR = am.dd_objectnumber
WHERE 
dt.datevalue = ifnull(j.JCDS_UDATE,'0001-01-01')
AND j.JCDS_STAT = 'I0218'
and j.jcds_chgnr = '1'
AND am.dim_dateidusagedecisionmadedt <> dt.dim_dateid;
/*END BI-5163 Alin*/

/* Madalina 25 Apr 2017 - APP-6076 */
/*fact inserts */
/*keep unique values of Calendar Week, per each Plant*/
drop table if exists tmp_dimDateCalendarWeek;
create table tmp_dimDateCalendarWeek as
select * from (
		select dim_dateid,CalendarWeekYr,MonthYear, plantcode_factory, datevalue, row_number() over (partition by CalendarWeekYr, plantcode_factory order by dayofmonth asc) rn from dim_date) t
		where rn = 1
		order by 1;

drop table if exists brinit_date_plants;
create table brinit_date_plants as
select distinct dd.dim_dateid as dim_targetreleasedateid,
    dp.dim_plantid as dim_plantid
/* from dim_date dd */
from tmp_dimDateCalendarWeek dd
inner join dim_plant dp on dd.plantcode_factory = dp.plantcode
where plantcode_factory in ( select plantcode from newiteminstockbrinit)
and calendarweekyr between '2004-01' and '2020-01'
and not exists ( select 1 
				from fact_inspectionlot fil
				inner join dim_date trd on fil.dim_targetreleasedateid = trd.dim_dateid
				where dd.datevalue = trd.datevalue
				and dd.plantcode_factory = trd.plantcode_factory);

insert into fact_inspectionlot
(fact_inspectionlotid, dd_inspectionlotno, dim_targetreleasedateid, dim_plantid, dd_lrotcommentmanual_flag)
select 
	(SELECT ifnull(max(fil.fact_inspectionlotid), 1) id FROM fact_inspectionlot fil) + row_number() over(order by '') as fact_inspectionlotid,
	'Not Available' dd_inspectionlotno,
	dim_targetreleasedateid,
	dim_plantid,
	1 as dd_lrotcommentmanual_flag
from brinit_date_plants;

/* delete duplicates of Calendar week */
merge into fact_inspectionlot fil using
(select distinct fact_inspectionlotid
	 from fact_inspectionlot fil
		inner join dim_date trd on fil.dim_targetreleasedateid = trd.dim_dateid
	where not exists
		( select 1 from tmp_dimDateCalendarWeek tmp
			where trd.dim_dateid = tmp.dim_dateid)
        and dd_inspectionlotno = 'Not Available'
		and dd_lrotcommentmanual_flag = 1) del
on fil.fact_inspectionlotid = del.fact_inspectionlotid
when matched then delete;  

/*fact updates - for comments*/
/* if new ILots are added to area, with the brinit plant combinations, then the comments need to be updated for these new records */
drop table if exists tmp_for_update_delete_brinit_comments;
create table tmp_for_update_delete_brinit_comments as
select original_rows.fact_inspectionlotid as original_fact_inspectionlotid, 
	BRINIT_rows.fact_inspectionlotid as BRINIT_fact_inspectionlotid, 
	BRINIT_rows.dim_targetreleasedateid, 
	BRINIT_rows.dim_plantid, 
	BRINIT_rows.dd_lrotcommentmanual,
	BRINIT_rows.dd_lrottop75impactedmanual,
	BRINIT_rows.dd_lrot_miss_manual,
	BRINIT_rows.dd_lrot_hit_manual
	from
		(select fact_inspectionlotid, dim_targetreleasedateid, dim_plantid, dd_lrotcommentmanual, dd_lrottop75impactedmanual, dd_lrot_miss_manual, dd_lrot_hit_manual
		from fact_inspectionlot
		where dd_inspectionlotno = 'Not Available'
		and dd_lrotcommentmanual_flag = 1) BRINIT_rows
	inner join
		(select fact_inspectionlotid, dim_targetreleasedateid, dim_plantid, dd_lrotcommentmanual, dd_lrottop75impactedmanual, dd_lrot_miss_manual, dd_lrot_hit_manual
		from fact_inspectionlot
		where dd_inspectionlotno <> 'Not Available'
		and dd_lrotcommentmanual_flag = 0) original_rows
	on BRINIT_rows.dim_targetreleasedateid = original_rows.dim_targetreleasedateid
	and BRINIT_rows.dim_plantid =original_rows.dim_plantid;

/* comments update */
merge into fact_inspectionlot fi using
	(select original_fact_inspectionlotid, dd_lrotcommentmanual, dd_lrottop75impactedmanual, dd_lrot_miss_manual, dd_lrot_hit_manual
	from
		tmp_for_update_delete_brinit_comments
	) upd
on fi.fact_inspectionlotid = upd.original_fact_inspectionlotid
when matched then update
set fi.dd_lrotcommentmanual = upd.dd_lrotcommentmanual,
	fi.dd_lrottop75impactedmanual = upd.dd_lrottop75impactedmanual,
	fi.dd_lrot_hit_manual = upd.dd_lrot_hit_manual,
	fi.dd_lrot_miss_manual = upd.dd_lrot_miss_manual;

/*create backups for the comments that will be further deleted*/
insert into deleted_brinit_comments_for_IL (dim_targetreleasedateid, dim_plantid, dd_lrotcommentmanual, dd_lrottop75impactedmanual, dd_lrot_miss_manual, dd_lrot_hit_manual)
select  fi.dim_targetreleasedateid, fi.dim_plantid, fi.dd_lrotcommentmanual, fi.dd_lrottop75impactedmanual, fi.dd_lrot_miss_manual, fi.dd_lrot_hit_manual
from tmp_for_update_delete_brinit_comments tmp
inner join fact_inspectionlot fi on fi.dim_plantid = tmp.dim_plantid
where fi.dd_inspectionLotNo = 'Not Available'
	and fi.dd_lrotcommentmanual <> 'No comments'
	and dd_lrotcommentmanual_flag = 1;

/*fact deletions*/	
merge into fact_inspectionlot fi using
	(select fi.fact_inspectionlotid
	from tmp_for_update_delete_brinit_comments tmp 
	inner join fact_inspectionlot fi on fi.dim_plantid = tmp.dim_plantid
	where dd_inspectionLotNo = 'Not Available'
	and dd_lrotcommentmanual_flag = 1
	) del
on fi.fact_inspectionlotid = del.fact_inspectionlotid
when matched then delete; 

/*Alin 17 Oct 2017 - history table for plantcodes that do not come from SAP and editable fields */
DELETE from fact_inspectionlot_manual where snapshotdate = current_date;

insert into fact_inspectionlot_manual
(fact_inspectionlotid, dd_inspectionlotno, dim_targetreleasedateid, dim_plantid, DD_LROTCOMMENTMANUAL,DD_LROT_HIT_MANUAL, DD_LROT_MISS_MANUAL, DD_LROTTOP75IMPACTEDMANUAL, snapshotdate)
SELECT 
FACT_INSPECTIONLOTID, DD_INSPECTIONLOTNO, DIM_TARGETRELEASEDATEID, F.DIM_PLANTID, DD_LROTCOMMENTMANUAL, DD_LROT_HIT_MANUAL, DD_LROT_MISS_MANUAL, DD_LROTTOP75IMPACTEDMANUAL, current_date
from fact_inspectionlot f
inner join dim_plant dp on f.dim_plantid = dp.dim_plantid
where  dp.plantcode in ( select DISTINCT plantcode from newiteminstockbrinit);

DELETE FROM fact_inspectionlot_manual
WHERE snapshotdate < current_date - INTERVAL '1' YEAR;

/*Octavian S 15-FEB-2018 APP-6837 Add new attribute*/
--Long text on inspection lot
 MERGE INTO fact_inspectionlot il
USING (
SELECT DISTINCT il.dd_inspectionlotno,first_value(shorttext) over (partition by dd_inspectionlotno) shorttext
FROM (SELECT DISTINCT stxh_tdname, 
group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
FROM stxh s 
WHERE stxh_tdobject LIKE '%QPRUEFLOS%' 
AND stxh_tdid = 'QALS'
GROUP BY stxh_tdname) s
,fact_inspectiONlot il
WHERE right(TRIM(leading '0' FROM stxh_tdname),8)=il.dd_inspectionlotno
)t
ON t.dd_inspectionlotno = il.dd_inspectionlotno
WHEN MATCHED
THEN UPDATE SET dd_longtext_stxh = IFNULL(t.shorttext,'Not Set');
 
 
 /*19 Apr 2018 Georgiana Changes according to APP-8617 creating a default logic based on Plant.Plant Title and Target Release Date.Date
additional filters for this will be: equals with Year to Date and not equal with Current Month*/

/*05 June 2018 Georgiana - revert default logic changes*/
/*drop table if exists  tmp_for_default_LROT_ATLAS
create table tmp_for_default_LROT_ATLAS as
select distinct calendarmonthid,planttitle_merck,DD_LROTCOMMENTMANUAL,f.dim_plantid,f.dim_targetreleasedateid from fact_inspectionlot f, dim_date d, dim_plant pl
where dim_targetreleasedateid=dim_dateid
and f.dim_plantid=pl.dim_plantid
and year(datevalue)=year(current_date)
and calendarmonthid = concat( year(current_date), case when length(month(current_date)-1)=1 then concat('0', month(current_date)-1) else month(current_Date)-1 end)
and DD_LROTCOMMENTMANUAL <>'No comments'
order by 2,1*/

/*merge into fact_inspectionlot f
using ( select distinct fact_inspectionlotid,t.DD_LROTCOMMENTMANUAL
from fact_inspectionlot f,tmp_for_default_LROT_ATLAS t,dim_date d1, dim_date d2
where f.dim_plantid = t.dim_plantid
and f.dim_targetreleasedateid = d1.dim_dateid
and t.dim_targetreleasedateid = d2.dim_dateid
and d1.calendarmonthid=d2.calendarmonthid
and d1.plantcode_factory=d2.plantcode_factory
and d1.companycode=d2.companycode
and f.DD_LROTCOMMENTMANUAL='No comments') t
on f.fact_inspectionlotid=t.fact_inspectionlotid
when matched then update set f.DD_LROTCOMMENTMANUAL=t.DD_LROTCOMMENTMANUAL*/


/*19 Apr 2018 End of changes*/

drop table if exists  tmp_for_default_LROT_ATLAS;
