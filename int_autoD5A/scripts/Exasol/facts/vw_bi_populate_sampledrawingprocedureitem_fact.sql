
/* ################################################################################################################## */
/* */
/*   Author         : Georgiana */
/*   Created On     : 7 Sept 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   7 Sept 2016      Gergiana	1.0  		  First draft */

/******************************************************************************************************************/


delete from number_fountain m where m.table_name = 'fact_sampledrawingprocedureitem';
insert into number_fountain
select 'fact_sampledrawingprocedureitem',
ifnull(max(f.fact_sampledrawingprocedureitemid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_sampledrawingprocedureitem f;

insert into fact_sampledrawingprocedureitem
(
fact_sampledrawingprocedureitemid,
dd_sampledrawprocedure,
dd_version,
dd_sampleitem,
dim_sampledrawingitemsid,
dim_sampledrawingprocedureid,
dim_validfromdateid,
dim_unitofmeasureid,
ct_sizeofrezervedsample,
dw_insert_date)
SELECT
(select max_id   from number_fountain   where table_name = 'fact_sampledrawingprocedureitem') + row_number() over(order by '') AS fact_sampledrawingprocedureitemid,
ifnull(q.QPRVP_PRZIEHVERF, 'Not Set'),
ifnull(q.QPRVP_VERSION, 'Not Set'),
ifnull(q.QPRVP_POSNRPRZV, 0),
1 as dim_sampledrawingitemsid,
1 as dim_sampledrawingprocedureid,
1 as dim_validfromdateid,
1 as dim_unitofmeasureid,
ifnull(q.QPRVP_UMFRUECKL,0),
current_date
FROM
QPRVP q, QPRVK_QPRVKT
where QPRVK_PRZIEHVERF=QPRVP_PRZIEHVERF 
AND  QPRVK_VERSION=QPRVP_VERSION
and not exists (select 1 from fact_sampledrawingprocedureitem
                      where dd_sampledrawprocedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set')
					       and dd_version = ifnull(q.QPRVP_VERSION, 'Not Set')
						   and dd_sampleitem = ifnull(q.QPRVP_POSNRPRZV, 0));

merge into fact_sampledrawingprocedureitem f
using (select distinct f.fact_sampledrawingprocedureitemid, ds.dim_sampledrawingitemsid
FROM QPRVP q,fact_sampledrawingprocedureitem f, dim_sampledrawingitems ds
where dd_sampledrawprocedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set')
and dd_version = ifnull(q.QPRVP_VERSION, 'Not Set')
and dd_sampleitem = ifnull(q.QPRVP_POSNRPRZV, 0)
and ds.SampleProcedure = dd_sampledrawprocedure
and ds.VersionNo = dd_version
and ItemNo = dd_sampleitem
and f.dim_sampledrawingitemsid<>ds.dim_sampledrawingitemsid) t
on t.fact_sampledrawingprocedureitemid=f.fact_sampledrawingprocedureitemid
when matched then update set 
 f.dim_sampledrawingitemsid = t.dim_sampledrawingitemsid,
 dw_update_date=current_date;
 
 
 merge into fact_sampledrawingprocedureitem f
using (select distinct f.fact_sampledrawingprocedureitemid, ds.dim_sampledrawingprocedureid
FROM QPRVK_QPRVKT q,fact_sampledrawingprocedureitem f, dim_sampledrawingprocedure ds
where dd_sampledrawprocedure = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set')
and dd_version = ifnull(q.QPRVK_VERSION, 'Not Set')
and ds.SampleProcedure = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set') 
and  ds.VersionNo = ifnull(q.QPRVK_VERSION, 'Not Set')
and f.dim_sampledrawingprocedureid <> ds.dim_sampledrawingprocedureid) t
on t.fact_sampledrawingprocedureitemid=f.fact_sampledrawingprocedureitemid
when matched then update set 
 f.dim_sampledrawingprocedureid = t.dim_sampledrawingprocedureid,
 dw_update_date=current_date;


 merge into fact_sampledrawingprocedureitem f
using (select distinct f.fact_sampledrawingprocedureitemid, dt.dim_dateid as dim_dateid
FROM QPRVK_QPRVKT q,fact_sampledrawingprocedureitem f, dim_date dt
where 
dd_sampledrawprocedure = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set')
and dd_version = ifnull(q.QPRVK_VERSION, 'Not Set')
and dt.datevalue=ifnull(q.QPRVK_GUELTIGAB,'0001-01-01')
AND dt.companycode = 'Not Set'
and dt.plantcode_factory = 'Not Set'
and f.dim_validfromdateid <> dt.dim_dateid) t
on t.fact_sampledrawingprocedureitemid=f.fact_sampledrawingprocedureitemid
when matched then update set  
f.dim_validfromdateid = t.dim_dateid,
 dw_update_date=current_date;
 
merge into fact_sampledrawingprocedureitem f
using (select distinct f.fact_sampledrawingprocedureitemid, uom.dim_unitofmeasureid
FROM QPRVP q,fact_sampledrawingprocedureitem f, dim_unitofmeasure uom
where dd_sampledrawprocedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set')
and dd_version = ifnull(q.QPRVP_VERSION, 'Not Set')
and dd_sampleitem = ifnull(q.QPRVP_POSNRPRZV, 0)
and uom.uom=ifnull(q.QPRVP_MERUECKL,'Not Set')
and f.dim_unitofmeasureid<>uom.dim_unitofmeasureid) t
on t.fact_sampledrawingprocedureitemid=f.fact_sampledrawingprocedureitemid
when matched then update set 
 f.dim_unitofmeasureid = t.dim_unitofmeasureid,
 dw_update_date=current_date;