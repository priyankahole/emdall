
/* ################################################################################################################## */
/* */
/*   Author         : Georgiana */
/*   Created On     : 30 Aug 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   30 Aug 2016      Gergiana	1.0  		  First draft */

/******************************************************************************************************************/


delete from number_fountain m where m.table_name = 'fact_inspectioncatalogcodegroup';
insert into number_fountain
select 'fact_inspectioncatalogcodegroup',
ifnull(max(f.fact_inspectioncatalogcodegroupid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inspectioncatalogcodegroup f;

insert into fact_inspectioncatalogcodegroup (
fact_inspectioncatalogcodegroupid,
dd_catalog,
dd_codegroup,
dd_code,
dd_version,
dim_dateidvalidfromdate,
dim_inspectioncataloggroupid,
dim_inspectioncatalogcodeid,
Dim_inspectioncatalogtypeid,
dw_insert_date)
SELECT
(select max_id   from number_fountain   where table_name = 'fact_inspectioncatalogcodegroup') + row_number() over(order by '') AS fact_inspectioncatalogcodegroupid, 
ifnull(QPCD_KATALOGART,'Not Set'),
ifnull(QPCD_CODEGRUPPE,'Not Set'),
ifnull(QPCD_CODE,'Not Set'),
ifnull(QPCD_VERSION,'Not Set'),
1 as dim_dateidvalidfromdate,
1 as dim_inspectioncataloggroupid,
1 as dim_inspectioncatalogcodeid,
1 as Dim_inspectioncatalogtypeid,
current_date
from QPCD_QPCT q
where
 not exists (select 1 from fact_inspectioncatalogcodegroup
                 where dd_catalog =ifnull(QPCD_KATALOGART,'Not Set')
				       and dd_codegroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
					   and dd_code = ifnull(QPCD_CODE,'Not Set')
					   and dd_version = ifnull(QPCD_VERSION,'Not Set'));

merge into fact_inspectioncatalogcodegroup f
using (select distinct fact_inspectioncatalogcodegroupid, dt.dim_dateid as dim_dateid
from QPCD_QPCT q,fact_inspectioncatalogcodegroup f,dim_date dt
where 
dd_catalog =ifnull(QPCD_KATALOGART,'Not Set')
and dd_codegroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
and dd_code = ifnull(QPCD_CODE,'Not Set')
and dd_version = ifnull(QPCD_VERSION,'Not Set')
and dt.datevalue =ifnull(QPCD_GUELTIGAB,'0001-01-01')
AND dt.plantcode_factory = 'Not Set'
AND dt.companycode = 'Not Set'
and f.dim_dateidvalidfromdate <> dt.dim_dateid) t
on t.fact_inspectioncatalogcodegroupid=f.fact_inspectioncatalogcodegroupid
when matched then update set f.dim_dateidvalidfromdate=t.dim_dateid;

merge into fact_inspectioncatalogcodegroup f
using (select distinct fact_inspectioncatalogcodegroupid, d.dim_inspectioncataloggroupid
from QPCD_QPCT q,fact_inspectioncatalogcodegroup f,dim_inspectioncataloggroup d
where 
dd_catalog =ifnull(QPCD_KATALOGART,'Not Set')
and dd_codegroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
and dd_code = ifnull(QPCD_CODE,'Not Set')
and dd_version = ifnull(QPCD_VERSION,'Not Set')
and d."catalog" = dd_catalog
and d.CodeGroup = dd_codegroup
and f.dim_inspectioncataloggroupid <> d.dim_inspectioncataloggroupid) t
on t.fact_inspectioncatalogcodegroupid=f.fact_inspectioncatalogcodegroupid
when matched then update set f.dim_inspectioncataloggroupid=t.dim_inspectioncataloggroupid;

merge into fact_inspectioncatalogcodegroup f
using (select distinct fact_inspectioncatalogcodegroupid, d.Dim_inspectioncatalogtypeid
from QPCD_QPCT q,fact_inspectioncatalogcodegroup f,dim_inspectioncatalogtype d
where 
dd_catalog =ifnull(QPCD_KATALOGART,'Not Set')
and dd_codegroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
and dd_code = ifnull(QPCD_CODE,'Not Set')
and dd_version = ifnull(QPCD_VERSION,'Not Set')
and d.inspectioncatalogtypecode = dd_catalog
and f.Dim_inspectioncatalogtypeid <> d.Dim_inspectioncatalogtypeid) t
on t.fact_inspectioncatalogcodegroupid=f.fact_inspectioncatalogcodegroupid
when matched then update set f.Dim_inspectioncatalogtypeid=t.Dim_inspectioncatalogtypeid;

merge into fact_inspectioncatalogcodegroup f
using (select distinct f.fact_inspectioncatalogcodegroupid, ifnull(case when q.QPGR_STATUS='1' then '1 (Being created)' 
                               when q.QPGR_STATUS='2' then '2 (Released)'
							   when q.QPGR_STATUS='3' then '3 (Can no longer be used)'
							   when q.QPGR_STATUS='4' then '4 (Deletion flag)' end,'Not Set') as dd_status

from fact_inspectioncatalogcodegroup f, QPGR_QPGT q
where 
dd_catalog =ifnull(QPGR_KATALOGART,'Not Set')
and dd_codegroup = ifnull(QPGR_CODEGRUPPE,'Not Set')
and f.dd_status <>  ifnull(case when q.QPGR_STATUS='1' then '1 (Being created)' 
                               when q.QPGR_STATUS='2' then '2 (Released)'
							   when q.QPGR_STATUS='3' then '3 (Can no longer be used)'
							   when q.QPGR_STATUS='4' then '4 (Deletion flag)' end,'Not set')) t
on t.fact_inspectioncatalogcodegroupid=f.fact_inspectioncatalogcodegroupid
when matched then update set f.dd_status = t.dd_status;

merge into fact_inspectioncatalogcodegroup f
using (select distinct fact_inspectioncatalogcodegroupid, d.dim_inspectioncatalogcodesid
from QPCD_QPCT q,fact_inspectioncatalogcodegroup f,dim_inspectioncatalogcodes d
where 
dd_catalog =ifnull(QPCD_KATALOGART,'Not Set')
and dd_codegroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
and dd_code = ifnull(QPCD_CODE,'Not Set')
and dd_version = ifnull(QPCD_VERSION,'Not Set')
and  "catalog" = ifnull(QPCD_KATALOGART, 'Not Set')
AND CodeGroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
AND Code = ifnull(QPCD_CODE,'Not Set')
AND VersionNumber = ifnull(QPCD_VERSION, 'Not Set')
and f.dim_inspectioncatalogcodeid <> d.dim_inspectioncatalogcodesid) t
on t.fact_inspectioncatalogcodegroupid=f.fact_inspectioncatalogcodegroupid
when matched then update set f.dim_inspectioncatalogcodeid=t.dim_inspectioncatalogcodesid;

							   