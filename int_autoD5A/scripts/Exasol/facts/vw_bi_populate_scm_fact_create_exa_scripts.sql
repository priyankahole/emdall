/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_scm_fact_create_exa_scripts.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 23 Mar 2017 */
/*  */
/*  */
/*   Description    : Contains all the scripts that are used to populate SCM Fact */
/* 					  These scripts are manually run, when any modification is necessary. Otherwise, they are static and it is enough to be called ( from vw_bi_populate_scm_fact.sql) */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */




/* ******************************************************************************************************************************************************************************** */
/* ****************************************first generate the temp tables containing the basic material states, along with the basic states transitions *************************** */
/* Source tables for this first part: Material movement, Purchasing, Inspection Lot, Production Order */
/* ******************************************************************************************************************************************************************************** */

drop script if exists create_basic_temp_tables;
create script create_basic_temp_tables () as
	local v_count = query([[select count(*) from tmp_basic_mat_states_combinations;]])
	/* start with the second row, the first one is keeping the material states --> no need for that anymore */
	/* iterate through all the possible combinations */
	for i = 2,  v_count[1][1]+1 do
		/* transpose each line, so the new table can be iterated again */
		query([[drop table if exists tmp_basic_transpose]]);
		query([[create table tmp_basic_transpose as 
				select t.tmptabletoselect, t.materialstate, row_number() over ( order by t.tmptabletoselect) as rn from 
					/* it is mandatory to keep the correct order of the material states: 1- raw, 2- bulk etc */
					(select '1#raw' as tmptabletoselect, raw as materialstate from tmp_basic_mat_states_combinations where tmp_basic_mat_states_combinationsid in ( ]] .. i .. [[) union 
					select '2#bulk' as tmptabletoselect, bulk as materialstate from tmp_basic_mat_states_combinations where tmp_basic_mat_states_combinationsid in ( ]] .. i .. [[) union 
					select '3#fpu' as tmptabletoselect, fpu as materialstate from tmp_basic_mat_states_combinations where tmp_basic_mat_states_combinationsid in ( ]] .. i .. [[) union
					select '4#fpp' as tmptabletoselect, fpp as materialstate from tmp_basic_mat_states_combinations where tmp_basic_mat_states_combinationsid in ( ]] .. i .. [[) ) t
				order by tmptabletoselect
			]]);
		/*delete the indices and # -> they were used only for keeping the correct order of the materials*/
		query([[update tmp_basic_transpose set tmptabletoselect = substring(tmptabletoselect, instr(tmptabletoselect,'#')+1, length(tmptabletoselect)) ]]);

		v_count2 = query([[select count(*) from tmp_basic_transpose;]])	
		activematerials = 0;
		v_x = query ([[select 'X']])
		/*create a new table, containing all the active materials from the current combination*/
		query([[delete from tmp_basic_active_materials_list]]);
		/* iterate through all the material states from the current materials combination */
		v_tmptabletoselect = query ([[select '']])
		v_next_tmptabletoselect = query ([[select '']])
		for j = 1, v_count2[1][1] do
			v_materialstate = query([[ select materialstate from tmp_basic_transpose where rn = ]] .. j .. [[ ]]) 		  /*possible values: X or nulls*/
			v_compare = query ([[select '']])
			v_raw = query([[select 'raw']])
			/* check if the material state is active in this combination */
			if (v_materialstate[1][1] == v_x[1][1]) then
				activematerials = activematerials + 1		
				/*only the combinations of 1 or 2 material states are available*/
				if activematerials == 1 then
					v_tmptabletoselect = query([[ select tmptabletoselect from tmp_basic_transpose where rn = ]] .. j .. [[ ]]) 	/*possible values: raw_1, bulk_1, bulk_2 etc*/
				elseif activematerials == 2 then
					v_next_tmptabletoselect = query([[ select tmptabletoselect from tmp_basic_transpose where rn = ]] .. j .. [[ ]]) 	
				end			
			end /*end of the processing for active material states*/
		end /* end of checking all the material states for the current combination*/
		if activematerials == 1 then
			--query([[insert into alt_test values (']] ..  v_tmptabletoselect[1][1] .. [[' ) ]]  );
			if v_tmptabletoselect[1][1] ~= v_compare then
				/*create the basic temp tables - from Material Movement, Production Order, Inspection Lot and Purchasing*/
				/*raw temp table is created different from the rest*/
				if v_tmptabletoselect[1][1] == v_raw[1][1] then
					query([[drop table if exists temp_]] .. v_tmptabletoselect[1][1] .. [[;]]);
					query([[create table temp_]] .. v_tmptabletoselect[1][1] .. [[ as
							select
							distinct f_mm.dim_plantid,
							         f_mm.dim_partid,
							         f_mm.dim_batchid,
							         ifnull(f_ins.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno,
									 convert(varchar(12), 'Not Set') as dd_ordernumber, 
							         f_mm.dd_DocumentNo,
							         f_mm.dd_DocumentItemNo,
									 f_pur.dd_scheduleno,
							         f_pur.dim_dateidschedorder,
							         f_pur.dim_dateidcreate,
							         f_mm.dim_dateidpostingdate,
							         ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr,
							         ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco,
							         ifnull(f_ins.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart,
							         ifnull(f_ins.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade
							         from fact_materialmovement f_mm
										  inner join fact_purchase f_pur on  f_mm.dim_plantid = f_pur.dim_plantidordering
																		  and f_mm.dim_partid = f_pur.dim_partid
																		  and f_pur.dd_documentno = f_mm.dd_documentno
																		  and f_pur.dd_documentitemno = f_mm.dd_documentitemno
							              left join fact_inspectionlot f_ins on  f_mm.dim_plantid = f_ins.dim_plantid
																			  and f_mm.dim_partid = f_ins.dim_partid
																			  and f_mm.dim_batchid = f_ins.dim_batchid
							              inner join dim_movementtype mt on f_mm.dim_movementtypeid = mt.dim_movementtypeid
							              inner join dim_part dp on f_mm.dim_partid = dp.dim_partid
								where 
							  mt.movementtype in ('101','102')
							  and f_mm.dim_dateidpostingdate <> 1 
							  and f_mm.dim_plantid <> 1
							  and f_mm.dim_partid <> 1
							  and f_mm.dim_batchid <> 1
							  and lower(dp.ItemSubType_Merck) = ']] .. v_tmptabletoselect[1][1] .. [[';]]);
				else
					--query([[insert into alt_test values (']] ..  v_tmptabletoselect[1][1] ..[[' ) ]]  ); 
					query([[drop table if exists temp_]] .. v_tmptabletoselect[1][1] .. [[;]]);
					query([[create table temp_]] .. v_tmptabletoselect[1][1] .. [[ as
					select distinct f_prodorder.dim_plantid,
									f_prodorder.dim_partidheader as dim_partid,
									f_prodorder.dim_batchid,
					                ifnull(f_insp.dd_inspectionlotno, 'Not Set') as dd_inspectionlotno,
					                f_prodorder.dd_ordernumber,
									convert(varchar(50), 'Not Set') dd_DocumentNo,
					         		convert(decimal (18,0), 0) dd_DocumentItemNo,
									convert(decimal (18,0), 0) dd_scheduleno,
							        convert(bigint, 1) dim_dateidschedorder,
							        convert(bigint, 1) dim_dateidcreate,
							        convert(bigint, 1) dim_dateidpostingdate,
					                ifnull(dd_dateuserstatussamr,'0001-01-01') as dd_dateuserstatussamr,
					                ifnull(dd_dateuserstatusqcco,'0001-01-01') as dd_dateuserstatusqcco,
					                ifnull(f_insp.dim_dateidinspectionstart, 1) as dim_dateidinspectionstart,
					                ifnull(f_insp.dim_dateidusagedecisionmade, 1) as dim_dateidusagedecisionmade
					from fact_productionorder f_prodorder 
							left join fact_inspectionlot f_insp on f_insp.dim_batchid = f_prodorder.dim_batchid
															  and f_insp.dim_partid = f_prodorder.dim_partidheader
															  and f_insp.dim_plantid = f_prodorder.dim_plantid
							inner join dim_part dp on f_prodorder.dim_partidheader = dp.dim_partid
					where lower(dp.ItemSubType_Merck) = ']] .. v_tmptabletoselect[1][1] .. [[';]]);
				end
				/*also generate the transitions from one material state to itself - used for bulk-bulk, fpu-fpu etc */
				query([[drop table if exists temp_]] .. v_tmptabletoselect[1][1] .. '_' .. v_tmptabletoselect[1][1] .. [[;]]);
				query([[create table temp_]] .. v_tmptabletoselect[1][1] .. '_' .. v_tmptabletoselect[1][1] .. [[ as
				select distinct f_mm.dim_plantid,
				                f_mm.dim_partid,
				                f_mm.dim_batchid,
				                f_prodord.dd_ordernumber,
				                f_prodord.dim_dateidactualheaderfinish,
				                f_prodord.dim_dateidactualrelease,
				                f_prodord.dim_dateidactualstart,
				                f_prodord.dim_partidheader,
				                f_prodord.dim_batchid as dim_batchidprod
				 from fact_materialmovement f_mm,
				      fact_productionorder f_prodord,
				      dim_movementtype mt,
					  dim_part dp,
					  dim_part dph
				where f_mm.dim_movementtypeid = mt.dim_movementtypeid
				 and  mt.movementtype in ('261','262')
				 and f_mm.dim_plantid = f_prodord.dim_plantid
				 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
				 and dp.dim_partid = f_mm.dim_partid
				 and dph.dim_partid = f_prodord.dim_partidheader
				 and lower(dp.ItemSubType_Merck) = ']] .. v_tmptabletoselect[1][1] .. [[' 
				 and lower(dph.ItemSubType_Merck) = ']] .. v_tmptabletoselect[1][1] .. [[';]]) ;
			end
		elseif activematerials == 2 then
			if v_tmptabletoselect[1][1] ~= v_compare and v_next_tmptabletoselect[1][1] ~= v_compare then 
				/*generate the tables that keep the transitions between two material states*/
				query([[drop table if exists temp_]] .. v_tmptabletoselect[1][1] .. '_' .. v_next_tmptabletoselect[1][1] .. [[;]]);
				query([[create table temp_]] .. v_tmptabletoselect[1][1] .. '_' .. v_next_tmptabletoselect[1][1] .. [[ as
				select distinct f_mm.dim_plantid,
				                f_mm.dim_partid,
				                f_mm.dim_batchid,
				                f_prodord.dd_ordernumber,
				                f_prodord.dim_dateidactualheaderfinish,
				                f_prodord.dim_dateidactualrelease,
				                f_prodord.dim_dateidactualstart,
				                f_prodord.dim_partidheader,
				                f_prodord.dim_batchid as dim_batchidprod
				 from fact_materialmovement f_mm,
				      fact_productionorder f_prodord,
				      dim_movementtype mt,
					  dim_part dp,
					  dim_part dph
				where f_mm.dim_movementtypeid = mt.dim_movementtypeid
				 and  mt.movementtype in ('261','262')
				 and f_mm.dim_plantid = f_prodord.dim_plantid
				 and f_mm.dd_productionordernumber = f_prodord.dd_ordernumber
				 and dp.dim_partid = f_mm.dim_partid
				 and dph.dim_partid = f_prodord.dim_partidheader
				 and lower(dp.ItemSubType_Merck) = ']] .. v_tmptabletoselect[1][1] .. [[' 
				 and lower(dph.ItemSubType_Merck) = ']] .. v_next_tmptabletoselect[1][1] .. [[';]]) ;
			end	
		
		end /*end of columns generation*/			
	end /*end of posible combinations iteration*/
/


/* ******************************************************************************************************************************************************************************** */
/* ****************************************second step: generate the combinations of the material states ************************************************************************** */
/* Source tables for this first part: Material movement, Purchasing, Inspection Lot, Production Order */
/* ******************************************************************************************************************************************************************************** */


drop script if exists fact_inserts;
create script fact_inserts () as
	query ([[delete from fact_scm_test]]);
	local v_count = query([[select count(*) from tmp_mat_states_combinations;]])
	/* start with the second row, pkey = 1 never exists */
	/* iterate through all the possible combinations */
	for i = 2,  v_count[1][1]+1 do
		output('processing ' .. i .. ' combination')
		/* transpose each line, so the new table can be iterated again */
		query([[drop table if exists tmp_transpose]]);
		query([[create table tmp_transpose as 
				select t.tmptabletoselect, t.materialstate, row_number() over ( order by t.tmptabletoselect) as rn from 
					/* it is mandatory to keep the correct order of the material states: 1- raw, 2- bulk etc */
					(select '1#raw_1' as tmptabletoselect, dd_raw_1 as materialstate from tmp_mat_states_combinations where tmp_mat_states_combinationsid in ( ]] .. i .. [[) union 
					select '2#bulk_1' as tmptabletoselect, dd_bulk_1 as materialstate from tmp_mat_states_combinations where tmp_mat_states_combinationsid in ( ]] .. i .. [[) union 
					select '3#bulk_2' as tmptabletoselect, dd_bulk_2 as materialstate from tmp_mat_states_combinations where tmp_mat_states_combinationsid in ( ]] .. i .. [[) union 
					select '4#bulk_3' as tmptabletoselect, dd_bulk_3 as materialstate from tmp_mat_states_combinations where tmp_mat_states_combinationsid in ( ]] .. i .. [[) union 
					select '5#fpu_1' as tmptabletoselect, dd_fpu_1 as materialstate from tmp_mat_states_combinations where tmp_mat_states_combinationsid in ( ]] .. i .. [[) union
					select '6#fpp_1' as tmptabletoselect, dd_fpp_1 as materialstate from tmp_mat_states_combinations where tmp_mat_states_combinationsid in ( ]] .. i .. [[) ) t
				order by tmptabletoselect
			]]);
		/*delete the indexes and # -> they were used only for keeping the correct order of the materials*/
		query([[update tmp_transpose set tmptabletoselect = substring(tmptabletoselect, instr(tmptabletoselect,'#')+1, length(tmptabletoselect)) ]]);
		/*count the number of material states in the current combinations - this will be constant = the number of material states available in the fact*/
		v_count2 = query([[select count(*) from tmp_transpose;]])	
		/*for each iteration, re-initialize the number of active materials*/
		activematerials = 0;
		v_x = query ([[select 'X']])
		/*create a new table, containing all the active materials from the current combination*/
		query([[delete from tmp_active_materials_list]]);
		/*this statement will be used in the insert -> for the where not exists condition - not needed*/
		/*v_inactive_materials_stmt = ''  */
		/* iterate through all the material states from the current materials combination */
		for j = 1, v_count2[1][1] do
			v_materialstate = query([[ select materialstate from tmp_transpose where rn = ]] .. j .. [[ ]]) 		  /*possible values: X or nulls*/
			v_tmptabletoselect = query([[ select tmptabletoselect from tmp_transpose where rn = ]] .. j .. [[ ]]) 	/*possible values: raw_1, bulk_1, bulk_2 etc*/
			/*instead of creating temp_bulk_1, temp_bulk_2 and temp_bulk_3 for example, I only created temp_bulk -> it holds the same information; this is why the below substring is done*/
			/*the same for the transitions: eg: only temp_raw_bulk exists, and is used for all raw - bulk (1,2,3)
			/*however, all the dd-s and dim-s are kept with the index - _2,_3, etc*/
			v_tmptabletoselect_table = query([[ select substring(']] .. v_tmptabletoselect[1][1] .. [[',1,instr(']] .. v_tmptabletoselect[1][1] .. [[','_')-1) ]])
			/*re-initialize -> this flag will determine the Shipping join*/
			v_active_fpp = 0	
			/* check if the material state is active in this combination */
			if (v_materialstate[1][1] == v_x[1][1]) then
				/*increase the number of active material states*/
				activematerials = activematerials + 1
				/*hold into a temp table all the material active states -> to search for FPP in the end*/
				/*tmp_active_materials_list doesn't mantain the order in which the materials were added, but the order is not needed in this case*/
				query([[insert into tmp_active_materials_list values (']] .. v_tmptabletoselect[1][1] .. [[')]])
				/* when we find the first active material of the combination, then no join can be executed - just initialize the temporary table*/
				if activematerials == 1 then
					query ([[ drop table if exists tmp_rez_intermediar]] .. activematerials .. [[]]);
					query ([[ create table tmp_rez_intermediar]] .. activematerials ..[[ as select
									dim_plantid as dim_plantid,
									dim_partid as dim_partid]] .. '_' .. v_tmptabletoselect[1][1] .. [[,
									dim_batchid as dim_batchid]] .. '_' .. v_tmptabletoselect[1][1] .. [[,
									dd_documentno as dd_documentno,
									dd_DocumentItemNo as dd_DocumentItemNo,
									dd_scheduleno as dd_scheduleno,
									dim_dateidschedorder as dim_dateidschedorder,
									dim_dateidcreate as dim_dateidcreate,
									dd_dateuserstatussamr as dd_dateuserstatussamr]] .. '_' .. v_tmptabletoselect[1][1] .. [[,
									dd_dateuserstatusqcco as dd_dateuserstatusqcco]] .. '_' .. v_tmptabletoselect[1][1] .. [[,
									dim_dateidinspectionstart as dim_dateidinspectionstart]] .. '_' .. v_tmptabletoselect[1][1] .. [[,
									dim_dateidusagedecisionmade as dim_dateidusagedecisionmade]] .. '_' .. v_tmptabletoselect[1][1] .. [[
							 from ]] .. 'temp_' .. v_tmptabletoselect_table[1][1] .. [[]]);
				else
					query ([[ drop table if exists tmp_rez_intermediar]] .. activematerials ..[[]]);
					query ([[ create table tmp_rez_intermediar]] .. activematerials ..[[ as select distinct
								   a.*,
								   b.dd_ordernumber as dd_ordernumber_]] .. v_previous_tmptabletoselect[1][1] .. '_' .. v_tmptabletoselect[1][1] .. [[,
							       b.dim_partidheader as dim_partid_]] .. v_tmptabletoselect[1][1] .. [[,
							       b.dim_batchidprod as dim_batchid_]] .. v_tmptabletoselect[1][1] .. [[,
							       b.dim_dateidactualrelease as dim_dateidactualrelease_]] .. v_tmptabletoselect[1][1] .. [[,
							       b.dim_dateidactualstart as dim_dateidactualstart_]] .. v_tmptabletoselect[1][1] .. [[,
							       b.dim_dateidactualheaderfinish as dim_dateidactualheaderfinish_]] .. v_tmptabletoselect[1][1] .. [[,
							       c.dd_inspectionlotno as dd_inspectionlotno_]] .. v_tmptabletoselect[1][1] .. [[,
							       c.dd_dateuserstatusqcco as dd_dateuserstatusqcco_]] .. v_tmptabletoselect[1][1] .. [[,
							       c.dim_dateidinspectionstart as dim_dateidinspectionstart_]] .. v_tmptabletoselect[1][1] .. [[,
							       c.dd_dateuserstatussamr as dd_dateuserstatussamr_]] .. v_tmptabletoselect[1][1] .. [[,
							       c.dim_dateidusagedecisionmade as dim_dateidusagedecisionmade_]] .. v_tmptabletoselect[1][1] .. [[ 
									from tmp_rez_intermediar]] .. activematerials-1 .. [[ a
										inner join temp_]] .. v_previous_tmptabletoselect_table[1][1] .. '_' .. v_tmptabletoselect_table[1][1] .. [[ b
											on a.dim_plantid = b.dim_plantid
	                                          and a.dim_partid_]] .. v_previous_tmptabletoselect[1][1] .. [[  = b.dim_partid
	                                          and a.dim_batchid_]] .. v_previous_tmptabletoselect[1][1] .. [[ = b.dim_batchid
										inner join temp_]] .. v_tmptabletoselect_table[1][1] .. [[ c
											on b.dim_partidheader = c.dim_partid
	                                          and b.dim_plantid = c.dim_plantid
	                                          and b.dim_batchidprod = c.dim_batchid
	                                          and b.dd_ordernumber = c.dd_ordernumber 
							]] );
	
					/* drop old intermediary results table */
					query ([[ drop table if exists tmp_rez_intermediar]] .. activematerials-1 ..[[]]);
				end
				/* hold into a variable the previous active material state */
				v_previous_tmptabletoselect = query([[ select tmptabletoselect from tmp_transpose where rn = ]] .. j .. [[ ]]) 	
				v_previous_tmptabletoselect_table = query([[ select substring(']] .. v_previous_tmptabletoselect[1][1] .. [[',1,instr(']] .. v_previous_tmptabletoselect[1][1] .. [[','_')-1) ]])
			/*end of the processing for active material states*/
			else
				/*need to hold the inactive materials as well, in order to use them in the not exists statement -> for the insert*/ -- no need for this anymore
				-- v_inactive_materials_stmt = ' ' .. v_inactive_materials_stmt .. ' AND m.dim_partid_' .. v_tmptabletoselect[1][1] .. ' = 1 and m.dim_batchid_' .. v_tmptabletoselect[1][1] .. ' = 1   '
			end  
		end /* end of joining all the material states for the current combination*/
		/*inserting main condition -> more than one active material per combination*/
		if activematerials > 1 then
			/*determine all the columns that resulted from joining the active material states of the current material combination */
			v_rezulted_columns = query([[describe tmp_rez_intermediar]] .. activematerials .. [[]])
			v_columns_to_insert = ''
			for k = 1, #v_rezulted_columns do /* # is the array's dimension. Can be applied to variables defined as tables, not to tables directly */
				if v_columns_to_insert == '' then
					v_columns_to_insert = v_rezulted_columns[k][1];
				else
					v_columns_to_insert = v_columns_to_insert .. ', ' .. v_rezulted_columns[k][1];
				end
			end /* end of the loop that concatenates all the columns*/
			/*prepare the fact p-key*/
			query([[delete from number_fountain m where m.table_name = 'fact_scm_test';]]);
			query([[insert into number_fountain
					select 'fact_scm_test',
					ifnull(max(f.fact_mmprodhierarchyid ),
					ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
					from fact_scm_test f;]]);

			/*insert into fact avoiding where not exists condition, by using merge into insead */
			v_where_not_exists_condition = ' m.dim_plantid = a.dim_plantid ';
			v_tmp_active_materials_list = query ([[select * from tmp_active_materials_list]])
			for ii = 1, #v_tmp_active_materials_list do
				v_where_not_exists_condition = v_where_not_exists_condition .. 
						' and m.dim_partid_' .. v_tmp_active_materials_list[ii][1] .. ' = a.dim_partid_' .. v_tmp_active_materials_list[ii][1] ..
						' and m.dim_batchid_' .. v_tmp_active_materials_list[ii][1] .. ' = a.dim_batchid_' .. v_tmp_active_materials_list[ii][1] 		
			end		
			/*need to find out the last active material. If it was a FPP, then join the last intermediary result table with Shipping, before inserting into SCM fact.*/
			v_last_active_mat = query([[select max(active_material) from tmp_active_materials_list where active_material like 'fpp%']])
			/*if the last active material was a FPP*/
			if v_last_active_mat[1][1] ~= null then				
				/*initially, the Shipping join was kept into tmp_rez_intermediar, so the columns to insert were get as well using the describe statement. However, that solution caused performance issues*/
				/*add the Shipping columns into the inserted columns*/
				v_columns_to_insert = v_columns_to_insert .. ', ' .. 	'dim_plantid_comops,' ..
																		'dd_salesdlvrdocno_comops,' ..
																		'dd_salesdlvritemno_comops,' ..
																		'dim_dateiddlvrdoccreated_comops,' ..
																		'dim_dateidactualgoodsissue_comops,' ..
																		'dim_dateidactualreceipt_comops,' ..
																		'dd_inspectionlotno_comops,' ..
																		'dd_dateuserstatussamr_comops,' ..
																		'dim_dateidinspectionstart_comops,' ..
																		'dd_dateuserstatusqcco_comops,' ..
																		'dim_Dateidusagedecisionmade_comops,' ..
																		'dd_salesdlvrdocno_customer,' ..
																		'dd_salesdlvritemno_customer,' ..
																		'dim_dateiddlvrdoccreated_customer,' ..
																		'dim_dateidactualgoodsissue_customer,' ..
																		'ct_qtydelivered,' ..
																		'dd_SalesDocNo_customer,' ..
																		'dd_SalesItemNo_customer'	
				/*insert into fact - joining with Shipping before*/
				query([[ insert into fact_scm_test m 
						(  fact_mmprodhierarchyid, ]] ..
						v_columns_to_insert .. [[)
						 select 
									(select max_id from number_fountain where table_name = 'fact_scm_test') + row_number() over(ORDER BY '') as fact_mmprodhierarchyid, 
									a.*, 
									ifnull(b.dim_plantid_comops,1) as dim_plantid_comops,
									ifnull(b.dd_salesdlvrdocno_comops,0) as dd_salesdlvrdocno_comops,
									ifnull(b.dd_salesdlvritemno_comops,0) as dd_salesdlvritemno_comops,
									ifnull(b.dim_dateiddlvrdoccreated_comops,1) as dim_dateiddlvrdoccreated_comops,
									ifnull(b.dim_dateidactualgoodsissue_comops,1) as dim_dateidactualgoodsissue_comops,
									ifnull(b.dim_dateidactualreceipt_comops,1) as dim_dateidactualreceipt_comops,
									ifnull(b.dd_inspectionlotno_comops,'Not Set') as dd_inspectionlotno_comops,
									ifnull(b.dd_dateuserstatussamr_comops,'0001-01-01') as dd_dateuserstatussamr_comops,
									ifnull(b.dim_dateidinspectionstart_comops,1) as dim_dateidinspectionstart_comops,
									ifnull(b.dd_dateuserstatusqcco_comops,'0001-01-01') as dd_dateuserstatusqcco_comops,
									ifnull(b.dim_Dateidusagedecisionmade_comops,1) as dim_Dateidusagedecisionmade_comops,
									ifnull(b.dd_salesdlvrdocno_customer,0) as dd_salesdlvrdocno_customer,
									ifnull(b.dd_salesdlvritemno_customer,0) as dd_salesdlvritemno_customer,
									ifnull(b.dim_dateiddlvrdoccreated_customer,1) as dim_dateiddlvrdoccreated_customer,
									ifnull(b.dim_dateidactualgoodsissue_customer,1) as dim_dateidactualgoodsissue_customer,
									ifnull(b.ct_qtydelivered,0) as  ct_qtydelivered,
									ifnull(b.dd_SalesDocNo_customer,'Not Set') as dd_SalesDocNo_customer,
									ifnull(b.dd_SalesItemNo_customer,0) as dd_SalesItemNo_customer 
									from tmp_rez_intermediar]] .. activematerials .. ' a ' .. [[
										inner join dim_batch bafpp on a.dim_batchid_]] .. v_last_active_mat[1][1] .. [[ = bafpp.dim_batchid
										inner join dim_part pafpp on a.dim_partid_]] .. v_last_active_mat[1][1] .. [[ = pafpp.dim_partid
										inner join tmp004_insplot_salesorderdelivery002 b on a.dim_plantid = b.dim_plantid  
																							and bafpp.batchnumber = b.basdbatchnumber 
																							and pafpp.partnumber = b.pasdpartnumber ]] ..
									 ' where not exists ( select 1 from fact_scm_test m where ' .. v_where_not_exists_condition .. [[);]]);		
			else
				/*insert into fact */
				query([[ insert into fact_scm_test m 
							(  fact_mmprodhierarchyid, ]] ..
							v_columns_to_insert .. [[)
						    select 
									(select max_id from number_fountain where table_name = 'fact_scm_test') + row_number() over(ORDER BY '') as fact_mmprodhierarchyid, ]]
									.. v_columns_to_insert .. [[ from tmp_rez_intermediar]] .. activematerials .. ' a where not exists ( select 1 from fact_scm_test m where ' .. v_where_not_exists_condition 
									.. [[);]]);	
			end
		end /* end of inserting main condition -> more than one active material per combination*/		
	end /*end of posible combinations iteration*/
/


/* ******************************************************************************************************************************************************************************** */
/* ****************************************third step: fact deletions  ************************************************************************************************************ */
/* ******************************************************************************************************************************************************************************** */


drop script if exists fact_deletions;
create script fact_deletions () as
	v_count = query([[select count(*) from tmp_transpose;]]);
	/*insert _compops as a material state */
	query([[insert into tmp_transpose (tmptabletoselect, rn) values ('comops', ]] .. v_count[1][1] + 1 .. [[);]]);
	/* iterate through all the material states */
	for j = 1, v_count[1][1] + 1 do
		v_material = query([[ select tmptabletoselect from tmp_transpose where rn = ]] .. j .. [[ ]]);
		query([[ merge into fact_scm_test mmp using
				(select mmp.fact_mmprodhierarchyid
				from fact_scm_test mmp,
					 fact_inspectionlot fi, dim_inspectiontype dit
				where mmp.DD_INSPECTIONLOTNO_]] .. v_material[1][1] .. [[ = fi.dd_inspectionlotno and fi.dim_inspectiontypeid = dit.dim_inspectiontypeid
					and dit.InspectionTypeCode not like '01%' and dit.InspectionTypeCode not like '04%' and dit.InspectionTypeCode not like '03%') del
				on mmp.fact_mmprodhierarchyid = del.fact_mmprodhierarchyid
				when matched then delete;]]);
	end
/


/* ******************************************************************************************************************************************************************************** */
/* ****************************************fourth step: fact updates ************************************************************************************************************** */
/* ******************************************************************************************************************************************************************************** */

/* Update the Target Master Recipe Days BI-4755*/
drop script if exists fact_update_target_master_recipe_days;
create script fact_update_target_master_recipe_days () as
	v_count = query([[select count(*) from tmp_transpose;]]);
	/* in this point, tmp_transpose already contains comops as material state*/
	/* iterate through all the material states */
	for j = 1, v_count[1][1] do
		v_material = query([[ select tmptabletoselect from tmp_transpose where rn = ]] .. j .. [[ ]]);
		query([[update fact_scm_test fmm
				set fmm.ct_TargetMasterRecipe_]] .. v_material[1][1] .. [[ = ifnull(tmp.targerMasterRecipe, 0)
				from fact_scm_test fmm, dim_part dp, tmp_PlanOrder_TargetPerPart tmp
				where fmm.dim_partid_]] .. v_material[1][1] .. [[ = dp.dim_partid
					and tmp.partnumber = dp.partnumber
					and fmm.ct_TargetMasterRecipe_]] .. v_material[1][1] .. [[ <> ifnull(tmp.targerMasterRecipe, 0);]]);
	end
/


/* Use dim instead of dd for 'Date User Status SAMR was last set' and 'Date User Status QCCO was last set' - BI-4772  */
drop script if exists fact_update_date_user_status;
create script fact_update_date_user_status () as
	v_count = query([[select count(*) from tmp_transpose;]]);
	/* in this point, tmp_transpose already contains comops as material state, but it is not needed for these dims*/
	/* iterate through all the material states */
	for j = 1, v_count[1][1] do
		v_material = query([[ select tmptabletoselect from tmp_transpose where rn = ]] .. j .. [[ ]]);
		if v_material[1][1] ~= 'comops' then
			/* Date User Status SAMR was last set */
			query([[update fact_scm_test fmm
					set fmm.dim_dateuserstatussamr_]] .. v_material[1][1] .. [[ = dd.dim_dateid
					from fact_scm_test fmm, dim_date dd, dim_plant pl
					where pl.dim_plantid = fmm.dim_plantid
					and pl.companycode = dd.companycode
					and fmm.dd_dateuserstatussamr_]] .. v_material[1][1] .. [[ = dd.datevalue
					and pl.plantcode = dd.plantcode_factory  
					and fmm.dim_dateuserstatussamr_]] .. v_material[1][1] .. [[  <> dd.dim_dateid; ]]);
					
			query([[update fact_scm_test fmm
					set fmm.dim_dateuserstatusqcco_]] .. v_material[1][1] .. [[ = dd.dim_dateid
					from fact_scm_test fmm, dim_date dd, dim_plant pl
					where pl.dim_plantid = fmm.dim_plantid
					and pl.companycode = dd.companycode
					and fmm.dd_dateuserstatusqcco_]] .. v_material[1][1] .. [[  = dd.datevalue
					and pl.plantcode = dd.plantcode_factory  
					and fmm.dim_dateuserstatusqcco_]] .. v_material[1][1] .. [[ <> dd.dim_dateid;]]);
		end
	end
/


/* Add fields from Min Max: Min released stock in days and Lot size in days - BI-4868 */
/* add fields Float before production (in days) - BI-4886	*/
drop script if exists fact_update_min_max_measures ;
create script fact_update_min_max_measures () as
	v_count = query([[select count(*) from tmp_transpose;]]);
	for j = 1, v_count[1][1] do
		v_material = query([[ select tmptabletoselect from tmp_transpose where rn = ]] .. j .. [[ ]]);
		/* Add fields from Min Max - BI-4868 */
		/* Min released stock in days */
		query([[update fact_scm_test fmm
				set fmm.ct_minReleasedStockDays_]] .. v_material[1][1] .. [[ = fi.ct_minreleasedstockindays_merck
				from  fact_scm_test fmm, fact_inventoryatlas fi
				where fmm.dim_partid_]] .. v_material[1][1] .. [[ = fi.dim_partid
					and fmm.dim_plantid = fi.dim_plantid
					and fmm.ct_minReleasedStockDays_]] .. v_material[1][1] .. [[ <> fi.ct_minreleasedstockindays_merck;]]);
			
		/* Lot size in days */
		query([[update fact_scm_test fmm
				set fmm.ct_lotSizeDays_]] .. v_material[1][1] .. [[ = fi.ct_lotsizeindays_merck
				from  fact_scm_test fmm, fact_inventoryatlas fi
				where fmm.dim_partid_]] .. v_material[1][1] .. [[ = fi.dim_partid
					and fmm.dim_plantid = fi.dim_plantid
					and fmm.ct_lotSizeDays_]] .. v_material[1][1] .. [[ <> fi.ct_lotsizeindays_merck;]]);
		/* END BI-4868 */
		
		/* add fields Float before production (in days) - BI-4886	*/
		query([[update fact_scm_test fmm
				set ct_floatBeforeProduction_]] .. v_material[1][1] .. [[ = ifnull(T436A_VORGZ, 0)
				from  fact_scm_test fmm, dim_part dp, T436A
				where fmm.dim_partid_]] .. v_material[1][1] .. [[ =  dp.dim_partid
					and dp.schedmarginkey = ifnull(T436A_FHORI, 0)
					and dp.plant = ifnull(T436A_WERKS, 'Not Set')
					and ct_floatBeforeProduction_]] .. v_material[1][1] .. [[ <> ifnull(T436A_VORGZ, 0);]]);
		/* END BI-4886	*/
	end
/
