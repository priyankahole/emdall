/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_independentrequirements_fact.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 07 Jul 2016 */
/*  */
/*  */
/*   Description    : Populate Independent Requirements Fact */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */

--temporary table, used for updates
 drop table if exists tmp_fact_independentrequirements;
 create table tmp_fact_independentrequirements
	LIKE fact_independentrequirements INCLUDING DEFAULTS INCLUDING IDENTITY;

--generate surrogate key
 delete from number_fountain m where m.table_name = 'fact_independentrequirements';
 insert into number_fountain
	select 'fact_independentrequirements',
			ifnull(max(f.fact_independentrequirementsid ), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
	from fact_independentrequirements f;

/* Madalina 03 Aug 2016 - Refresh all data since 2011 - BI-3400 */
delete from fact_independentrequirements where dim_deliveryFinishDate in ( select dim_dateid from dim_date where datevalue > '2011-01-01'); 
/* END 03 Aug 2016 */

/* create temporary table PBIM_PBED_INDREQ, which also contains ids for related dim_part, dim_plant and dim_date   */
drop table if exists tmp_PBIM_PBED_INDREQ;
create table tmp_PBIM_PBED_INDREQ
	as select
			PBIM_MATNR,
		    PBIM_WERKS,
		    PBIM_BEDAE,
		    PBIM_VERSB,
		    PBIM_PBDNR,
		    PBIM_BDZEI,
		    PBIM_ZUVKZ,
		    PBIM_VERVS,
		    PBIM_VERKZ,
		    PBIM_PLNKZ,
		    PBIM_LOEVR,
		    PBIM_UEBKZ,
		    PBIM_DATLP,
		    PBIM_VRSIO,
		    PBIM_MCINF,
		    PBIM_STFNA,
		    PBED_PDATU,
		    PBED_WDATU,
		    PBED_AENAM,
		    PBED_LAEDA,
		    PBED_MEINS,
		    PBED_PLNMG,
		    PBED_ENTMG,
		    PBED_UPLMG,
		    PBED_ENTLI,
		    PBED_ENTLU,
		    PBED_PERXX,
		    dp.dim_partid as tmp_dim_partid,
			pl.dim_plantid as tmp_dim_plantid,
			d.dim_dateid as tmp_dim_deliveryFinishDate
	from PBIM_PBED_INDREQ p
			inner join dim_part dp on dp.partnumber = ifnull(p.PBIM_MATNR, 'Not Set')
			inner join dim_plant pl on pl.plantcode = ifnull(p.PBIM_WERKS, 'Not Set') 
									and pl.plantcode = dp.plant    /* Madalina 20 Jul 2016 BI-3400*/
			inner join dim_date d on d.datevalue = ifnull(PBED_PDATU, '0001-01-01') and pl.companycode = d.companycode
			AND pl.plantcode = d.plantcode_factory;

/* insert into temp table records new records from PBIM_PBED_INDREQ */
insert into tmp_fact_independentrequirements
	(fact_independentrequirementsid,
	dim_partid,
	dim_plantid,
	dd_RequirementsType,   --PBIM_BEDAE
	dd_versionIndepReqmts,  --PBIM_VERSB
	dd_RequirementsPlan,  --PBIM_PBDNR
	dd_IndReqmtsPointer, 			 --PBIM_BDZEI
	dim_deliveryFinishDate			 --PBED_PDATU
	)
select
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_independentrequirements') + row_number() over(order by '') as fact_independentrequirementsid,
	tmp_dim_partid,
	tmp_dim_plantid,
	ifnull(PBIM_BEDAE, 'Not Set'),
    ifnull(PBIM_VERSB, 'Not Set'),
    ifnull(PBIM_PBDNR, 'Not Set'),
    ifnull(PBIM_BDZEI, 0),
    tmp_dim_deliveryFinishDate
from tmp_PBIM_PBED_INDREQ
where not exists
	( select 1 from fact_independentrequirements fi
		where fi.dim_partid = tmp_dim_partid
		and fi.dim_plantid = tmp_dim_plantid
		and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
		and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
		and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
		and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
		and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	);


/* update the rest of fields from temp table */
update tmp_fact_independentrequirements fi
set dd_ConsumptionIndicator = ifnull(p.PBIM_ZUVKZ, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_ConsumptionIndicator <> ifnull(p.PBIM_ZUVKZ, 'Not Set');

update tmp_fact_independentrequirements fi
set dd_versionActive = ifnull(p.PBIM_VERVS, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_versionActive <> ifnull(p.PBIM_VERVS, 'Not Set');

update tmp_fact_independentrequirements fi
set dd_ConsumptionIndicReqmts = ifnull(p.PBIM_VERKZ, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and  dd_ConsumptionIndicReqmts <> ifnull(p.PBIM_VERKZ, 'Not Set');

update tmp_fact_independentrequirements fi
set dd_PlanningIndicator = ifnull(p.PBIM_PLNKZ, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_PlanningIndicator <> ifnull(p.PBIM_PLNKZ, 'Not Set');

update tmp_fact_independentrequirements fi
set dd_DeletionIndicator = ifnull(p.PBIM_LOEVR, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_DeletionIndicator <> ifnull(p.PBIM_LOEVR, 'Not Set');

update tmp_fact_independentrequirements fi
set dd_DataCopiedIndic = ifnull(p.PBIM_UEBKZ, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_DataCopiedIndic <> ifnull(p.PBIM_UEBKZ, 'Not Set');

update tmp_fact_independentrequirements fi
set fi.dim_dateid = d.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, dim_date d, dim_plant pl, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and d.datevalue = ifnull(PBIM_DATLP, '0001-01-01')
	AND d.plantcode_factory = pl.plantcode
	and d.companycode = pl.companycode
	and pl.plantcode = ifnull(p.PBIM_WERKS, 'Not Set')
	and fi.dim_dateid <> d.dim_dateid;

update tmp_fact_independentrequirements fi
set dd_versionInfoStructure = ifnull(PBIM_VRSIO,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_versionInfoStructure <> ifnull(PBIM_VRSIO,'Not Set');

update tmp_fact_independentrequirements fi
set dd_InfoStructure = ifnull(PBIM_MCINF,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_InfoStructure <> ifnull(PBIM_MCINF,'Not Set');

update tmp_fact_independentrequirements fi
set dd_fieldNameDDIC = ifnull(PBIM_STFNA, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_fieldNameDDIC <> ifnull(PBIM_STFNA, 'Not Set');

update tmp_fact_independentrequirements fi
set dim_ReqstdDate = d.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, dim_date d, dim_plant pl, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and d.datevalue = ifnull(PBED_WDATU, '0001-01-01')
	and d.companycode = pl.companycode
	and d.plantcode_factory = pl.plantcode
	and pl.plantcode = ifnull(p.PBIM_WERKS, 'Not Set')
	and dim_ReqstdDate <> d.dim_dateid;

update tmp_fact_independentrequirements fi
set dd_changedby = ifnull(PBED_AENAM, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_changedby <> ifnull(PBED_AENAM, 'Not Set');

update tmp_fact_independentrequirements fi
set dim_dateLastChange = d.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, dim_date d, dim_plant pl, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and d.datevalue = ifnull(PBED_LAEDA, '0001-01-01')
	and d.companycode = pl.companycode
	AND d.plantcode_factory = pl.plantcode
	and pl.plantcode = ifnull(p.PBIM_WERKS, 'Not Set')
	and dim_dateLastChange <> d.dim_dateid;

update tmp_fact_independentrequirements fi
set fi.dim_unitofmeasureid = u.dim_unitofmeasureid,
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, dim_unitofmeasure u, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and u.uom = ifnull(PBED_MEINS, 'Not Set')
	and fi.dim_unitofmeasureid <> u.dim_unitofmeasureid;

update tmp_fact_independentrequirements fi
set ct_PlannedQuantity = ifnull(PBED_PLNMG, 0),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and ct_PlannedQuantity <> ifnull(PBED_PLNMG, 0);

update tmp_fact_independentrequirements fi
set ct_WithdrawalQuantity = ifnull(PBED_ENTMG, 0),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and ct_WithdrawalQuantity <> ifnull(PBED_ENTMG, 0);

update tmp_fact_independentrequirements fi
set ct_OrigPlannedQuantity = ifnull(PBED_UPLMG, 0),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and ct_OrigPlannedQuantity <> ifnull(PBED_UPLMG, 0);

update tmp_fact_independentrequirements fi
set dd_dataType = ifnull(PBED_ENTLI, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_dataType <> ifnull(PBED_ENTLI, 'Not Set');

update tmp_fact_independentrequirements fi
set dd_periodIndicator = ifnull(PBED_ENTLU, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_periodIndicator <> ifnull(PBED_ENTLU, 'Not Set');

update tmp_fact_independentrequirements fi
set dd_PlanningPeriod = ifnull(PBED_PERXX, 'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
from tmp_PBIM_PBED_INDREQ p, tmp_fact_independentrequirements fi
where fi.dim_partid = tmp_dim_partid
	and fi.dim_plantid = tmp_dim_plantid
	and fi.dd_RequirementsType = ifnull(PBIM_BEDAE, 'Not Set')
	and fi.dd_versionIndepReqmts = ifnull(PBIM_VERSB, 'Not Set')
	and fi.dd_RequirementsPlan = ifnull(PBIM_PBDNR, 'Not Set')
	and fi.dd_IndReqmtsPointer = ifnull(PBIM_BDZEI, 0)
	and fi.dim_deliveryFinishDate = tmp_dim_deliveryFinishDate
	and dd_PlanningPeriod <> ifnull(PBED_PERXX, 'Not Set');

/*12 Feb 2018 Georgiana Changes according to FP-6337*/
merge into  tmp_fact_independentrequirements F_ir
using (select distinct fact_independentrequirementsid, drd.dim_dateid from
tmp_fact_independentrequirements F_ir, dim_date drd,dim_plant pl,dim_date dfd 
where pl.dim_plantid=f_ir.dim_plantid
and  f_ir.dim_deliveryfinishdate = dfd.Dim_Dateid
and drd.datevalue = (CASE WHEN dfd.datevalue < current_date THEN current_date ELSE dfd.datevalue END) 
 and drd.companycode = pl.companycode 
AND drd.plantcode_factory = pl.plantcode) t
on f_ir.fact_independentrequirementsid=t.fact_independentrequirementsid
when matched then update set dim_requirementsdateid=ifnull(t.dim_dateid,1);

insert into fact_independentrequirements
select * from tmp_fact_independentrequirements tmp_fi
where not exists
	(select 1
	from fact_independentrequirements fi
	where
		tmp_fi.dim_partid = fi.dim_partid
		and tmp_fi.dim_plantid = fi.dim_plantid
		and tmp_fi.dd_RequirementsType = fi.dd_RequirementsType
		and tmp_fi.dd_versionIndepReqmts = fi.dd_versionIndepReqmts
		and tmp_fi.dd_RequirementsPlan = fi.dd_RequirementsPlan
		and tmp_fi.dd_IndReqmtsPointer = fi.dd_IndReqmtsPointer
		and tmp_fi.dim_deliveryFinishDate = fi.dim_deliveryFinishDate
	);

drop table if exists tmp_fact_independentrequirements;
	