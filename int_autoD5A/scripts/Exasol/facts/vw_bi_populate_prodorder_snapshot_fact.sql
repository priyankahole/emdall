/**********************************************************************************/
/*  26 Aug 2015   1.3	      LiviuT    BI-1122 Convert dd_OrderUnderdelTolerance and dd_OrderOverdelivTolerance to measures */
/*  26 Aug 2015   1.3	      LiviuT    BI-1122 populate dd_OrderUnderdelTolerance, dd_OrderOverdelivTolerance, dd_OrderUnltdTolerance */
/*  10 Apr 2014   1.3             Lokesh        Instead of next 7 days, use next week for finding out open prod orders : BI-351 */
/*  17 Dec 2014   1.2	      Lokesh    Do not capture data with schedule finish date in last week. Added query to delete data for rerun of current snapshot */
/*  29 Oct 2014   1.0	      Lokesh    New script for new Merck Subj Area - Prod Order Snapshot */
/**********************************************************************************/

/* Store current date in beginning - to handle the scenario of date changing during script run */
DROP TABLE IF EXISTS tmp_current_date_fprsnapshot;
CREATE TABLE tmp_current_date_fprsnapshot
AS
SELECT current_date currdate;

/* If today's snapshot is being rerun, delete existing data */
DELETE FROM fact_productionorder_snapshot WHERE dd_snapshotdate = current_date;


/* Part 1 - Update  existing rows - only non-orig columns */
/* For all existing ord-items in snapshot table, update the values(of non-orig cols) to current values */
/* Note that orig columns won't change */

UPDATE fact_productionorder_snapshot s
SET dim_dateidscheduledfinish = fpr.dim_dateidscheduledfinish
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber 
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dim_dateidscheduledfinish <> fpr.dim_dateidscheduledfinish;


UPDATE fact_productionorder_snapshot s
SET ct_totalorderqty = fpr.ct_totalorderqty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr,fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_totalorderqty <> fpr.ct_totalorderqty;

UPDATE fact_productionorder_snapshot s
SET dd_cancelledorder = fpr.dd_cancelledorder
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr,fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dd_cancelledorder <> fpr.dd_cancelledorder;

UPDATE fact_productionorder_snapshot s
SET ct_underdeliverytol_merck = fpr.ct_underdeliverytol_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr,fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_underdeliverytol_merck <> fpr.ct_underdeliverytol_merck;

UPDATE fact_productionorder_snapshot s
SET ct_overdeliverytol_merck = fpr.ct_overdeliverytol_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr,fact_productionorder_snapshot s 
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_overdeliverytol_merck <> fpr.ct_overdeliverytol_merck;

UPDATE fact_productionorder_snapshot s
SET dd_unimitedoverdelivery_merck = fpr.dd_unimitedoverdelivery_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr,fact_productionorder_snapshot s 
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dd_unimitedoverdelivery_merck <> fpr.dd_unimitedoverdelivery_merck;

UPDATE fact_productionorder_snapshot s
SET ct_GRQty = fpr.ct_GRQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_GRQty <> fpr.ct_GRQty;

UPDATE fact_productionorder_snapshot s
SET ct_OrderItemQty = fpr.ct_OrderItemQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_OrderItemQty <> fpr.ct_OrderItemQty;

UPDATE fact_productionorder_snapshot s
SET s.dim_dateidactualitemfinish = fpr.dim_dateidactualitemfinish
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dim_dateidactualitemfinish <> fpr.dim_dateidactualitemfinish;

UPDATE fact_productionorder_snapshot s
SET s.dim_dateidactualheaderfinishdate_merck = fpr.dim_dateidactualheaderfinishdate_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.dim_dateidactualheaderfinishdate_merck <> fpr.dim_dateidactualheaderfinishdate_merck;

UPDATE fact_productionorder_snapshot s
SET s.ct_GRQty = fpr.ct_GRQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
AND s.dd_orderitemno = fpr.dd_orderitemno
AND s.ct_GRQty <> fpr.ct_GRQty;

/* Part 2 - Insert new rows */
/* For a given ord-item with scheduled finish date in next week, there are 2 scenarios possible :
A. The given ord-item is already present in fact_productionorder_snapshot. So, update the original fields from this table ( latest row for that ord-item )
B. The doc-item is new. So, in this case the original values will be the same as current values */

/* First capture the latest row from snapshot subj area for each order/item */
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_maxdate_1;
CREATE TABLE tmp_fact_productionorder_snapshot_maxdate_1
AS
SELECT dd_ordernumber, dd_orderitemno, max(dd_snapshotdate) max_dd_snapshotdate
FROM fact_productionorder_snapshot
GROUP BY dd_ordernumber, dd_orderitemno;

DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_maxdate;
CREATE TABLE tmp_fact_productionorder_snapshot_maxdate
AS
SELECT f.*
FROM fact_productionorder_snapshot f, tmp_fact_productionorder_snapshot_maxdate_1 m
WHERE f.dd_ordernumber = m.dd_ordernumber AND f.dd_orderitemno = m.dd_orderitemno AND f.dd_snapshotdate = m.max_dd_snapshotdate;


/* Insert new order-item */
/* Only open production orders with due dates in the next week */
/* Getting dates for next week : First find out the calenderweekid of current day + 7 days. Use that to join with scheduled finish date */
/* Cannot directly use week + 1 as it won't work on year-end */
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_ins;
CREATE TABLE tmp_fact_productionorder_snapshot_ins
AS
SELECT DISTINCT dd_ordernumber, dd_orderitemno
FROM fact_productionorder pr, dim_date d, tmp_current_date_fprsnapshot, dim_date dnextweek, dim_company dc
WHERE pr.dim_dateidscheduledfinish = d.dim_dateid
AND dc.dim_companyid = pr.dim_companyid AND dnextweek.datevalue = (currdate + INTERVAL '7' DAY) AND dnextweek.companycode = dc.companycode
and dnextweek.plantcode_factory = 'Not Set'
AND d.calendarweekid = dnextweek.calendarweekid
AND dim_dateidactualitemfinish = 1
AND ifnull(pr.dd_deletionflag,'Not Set')<>'Y';          /* Ensure that its not closed */


/* ALL production orders with due dates in the past 7 days (cancelled, closed or open) */
/*INSERT INTO tmp_fact_productionorder_snapshot_ins
SELECT DISTINCT dd_ordernumber, dd_orderitemno
FROM fact_productionorder pr, dim_date d, tmp_current_date_fprsnapshot
WHERE pr.dim_dateidscheduledfinish = d.dim_dateid
AND d.datevalue > currdate - INTERVAL '7' DAY
AND  d.datevalue <= currdate*/

DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_del;
CREATE TABLE tmp_fact_productionorder_snapshot_del
AS
SELECT DISTINCT dd_ordernumber, dd_orderitemno
FROM fact_productionorder_snapshot;

DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_upd;
CREATE TABLE tmp_fact_productionorder_snapshot_upd
AS
SELECT distinct i.*
FROM tmp_fact_productionorder_snapshot_ins i, tmp_fact_productionorder_snapshot_del d
WHERE i.dd_ordernumber = d.dd_ordernumber 
AND i.dd_orderitemno = d.dd_orderitemno;

/* So, wherever there's existing row and there's an update, mark dd_latestsnapshotflag as N */
UPDATE fact_productionorder_snapshot s
SET dd_latestsnapshotflag = 'N'
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_fact_productionorder_snapshot_upd u, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = u.dd_ordernumber AND s.dd_orderitemno = u.dd_orderitemno
AND s.dd_latestsnapshotflag <> 'N';

rename tmp_fact_productionorder_snapshot_ins to tmp_fact_productionorder_snapshot_ins1
;
create table tmp_fact_productionorder_snapshot_ins as 
(select * from tmp_fact_productionorder_snapshot_ins1) minus (Select * from tmp_fact_productionorder_snapshot_del)
;
drop table tmp_fact_productionorder_snapshot_ins1
;

drop table if exists max_holder_43;
Create table max_holder_43
as Select ifnull(max(fact_productionorder_snapshotid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_productionorder_snapshot;


/* For new inserts, pick up directly from production order */
INSERT INTO fact_productionorder_snapshot(
    fact_productionorder_snapshotid,
	fact_productionorderid,
	Dim_DateidRelease,
	Dim_DateidBOMExplosion,
	Dim_DateidLastScheduling,
	Dim_DateidTechnicalCompletion,
	Dim_DateidBasicStart,
	Dim_DateidScheduledRelease,
	Dim_DateidScheduledFinish,
	Dim_DateidScheduledStart,
	Dim_DateidActualStart,
	Dim_DateidConfirmedOrderFinish,
	Dim_DateidActualHeaderFinish,
	Dim_DateidActualHeaderFinishDate_merck,
	Dim_DateidActualRelease,
	Dim_DateidActualItemFinish,
	Dim_DateidPlannedOrderDelivery,
	Dim_DateidRoutingTransfer,
	Dim_DateidBasicFinish,
	Dim_ControllingAreaid,
	Dim_ProfitCenterId,
	Dim_tasklisttypeid,
	Dim_PartidHeader,
	Dim_bomstatusid,
	Dim_bomusageid,
	dim_productionschedulerid,
	Dim_ProcurementID,
	Dim_SpecialProcurementID,
	Dim_UnitOfMeasureid,
	Dim_PartidItem,
	Dim_AccountCategoryid,
	Dim_StockTypeid,
	Dim_StorageLocationid,
	Dim_Plantid,
	dim_specialstockid,
	Dim_ConsumptionTypeid,
	Dim_SalesOrgid,
	Dim_PurchaseOrgid,
	Dim_Currencyid,
	Dim_Companyid,
	Dim_ordertypeid,
	dim_productionorderstatusid,
	dim_productionordermiscid,
	Dim_BuildTypeid,
	dd_ordernumber,
	dd_orderitemno,
	dd_bomexplosionno,
	dd_plannedorderno,
	dd_SalesOrderNo,
	dd_SalesOrderItemNo,
	dd_SalesOrderDeliveryScheduleNo,
	ct_ConfirmedReworkQty,
	ct_ScrapQty,
	ct_OrderItemQty,
	ct_TotalOrderQty,
	ct_GRQty,
	ct_GRProcessingTime,
	amt_estimatedTotalCost,
	amt_ValueGR,
	dd_BOMLevel,
	dd_sequenceno,
	dd_ObjectNumber,
	ct_ConfirmedScrapQty,
	dd_OrderDescription,
	Dim_MrpControllerId,
	dim_currencyid_TRA,
	dim_currencyid_GBL,
	dd_OperationNumber,
	dd_WorkCenter,
	dim_customerid,
	dd_RoutingOperationNo,
	dd_snapshotdate,
	dim_dateidscheduledfinish_orig_merck,
	ct_totalorderqty_orig_merck,
	dd_cancelledorder_orig_merck,
	ct_underdeliverytol_orig_merck,
	ct_overdeliverytol_orig_merck,
	dd_unimitedoverdelivery_orig_merck,
	ct_GRQty_orig_merck,
	ct_OrderItemQty_orig_merck,
	/* For new inserts orig and non-orig values are same */
	ct_underdeliverytol_merck,
	ct_overdeliverytol_merck,
	dd_unimitedoverdelivery_merck,
	ct_snapshotcount,
	dd_latestsnapshotflag,
	ct_OrderUnderdelTolerance,
	ct_OrderOverdelivTolerance,
	dd_OrderUnltdTolerance,
	std_exchangerate_dateid)

SELECT
    (SELECT max_holder_43.maxid FROM max_holder_43) + row_number() over(order by '') fact_productionorder_snapshotid,
	f.fact_productionorderid,
	f.Dim_DateidRelease,
	f.Dim_DateidBOMExplosion,
	f.Dim_DateidLastScheduling,
	f.Dim_DateidTechnicalCompletion,
	f.Dim_DateidBasicStart,
	f.Dim_DateidScheduledRelease,
	f.Dim_DateidScheduledFinish,
	f.Dim_DateidScheduledStart,
	f.Dim_DateidActualStart,
	f.Dim_DateidConfirmedOrderFinish,
	f.Dim_DateidActualHeaderFinish,
	f.Dim_DateidActualHeaderFinishDate_merck,
	f.Dim_DateidActualRelease,
	f.Dim_DateidActualItemFinish,
	f.Dim_DateidPlannedOrderDelivery,
	f.Dim_DateidRoutingTransfer,
	f.Dim_DateidBasicFinish,
	f.Dim_ControllingAreaid,
	f.Dim_ProfitCenterId,
	f.Dim_tasklisttypeid,
	f.Dim_PartidHeader,
	f.Dim_bomstatusid,
	f.Dim_bomusageid,
	f.dim_productionschedulerid,
	f.Dim_ProcurementID,
	f.Dim_SpecialProcurementID,
	f.Dim_UnitOfMeasureid,
	f.Dim_PartidItem,
	f.Dim_AccountCategoryid,
	f.Dim_StockTypeid,
	f.Dim_StorageLocationid,
	f.Dim_Plantid,
	f.dim_specialstockid,
	f.Dim_ConsumptionTypeid,
	f.Dim_SalesOrgid,
	f.Dim_PurchaseOrgid,
	f.Dim_Currencyid,
	f.Dim_Companyid,
	f.Dim_ordertypeid,
	f.dim_productionorderstatusid,
	f.dim_productionordermiscid,
	f.Dim_BuildTypeid,
	f.dd_ordernumber,
	f.dd_orderitemno,
	f.dd_bomexplosionno,
	f.dd_plannedorderno,
	f.dd_SalesOrderNo,
	f.dd_SalesOrderItemNo,
	f.dd_SalesOrderDeliveryScheduleNo,
	f.ct_ConfirmedReworkQty,
	f.ct_ScrapQty,
	f.ct_OrderItemQty,
	f.ct_TotalOrderQty,
	f.ct_GRQty,
	f.ct_GRProcessingTime,
	f.amt_estimatedTotalCost,
	f.amt_ValueGR,
	f.dd_BOMLevel,
	f.dd_sequenceno,
	f.dd_ObjectNumber,
	f.ct_ConfirmedScrapQty,
	f.dd_OrderDescription,
	f.Dim_MrpControllerId,
	f.dim_currencyid_TRA,
	f.dim_currencyid_GBL,
	f.dd_OperationNumber,
	f.dd_WorkCenter,
	dim_customerid,
	dd_RoutingOperationNo,
	currdate, f.dim_dateidscheduledfinish,
	f.ct_totalorderqty,f.dd_cancelledorder,f.ct_underdeliverytol_merck,f.ct_overdeliverytol_merck,
	f.dd_unimitedoverdelivery_merck,
	f.ct_GRQty,f.ct_OrderItemQty,
	f.ct_underdeliverytol_merck,f.ct_overdeliverytol_merck,
	f.dd_unimitedoverdelivery_merck,
	1,
	'Y',
	f.ct_OrderUnderdelTolerance,
	f.ct_OrderOverdelivTolerance,
	f.dd_unlimitedoverdelflagorder,   /*f.dd_OrderUnltdTolerance */
	f.std_exchangerate_dateid std_exchangerate_dateid
FROM tmp_fact_productionorder_snapshot_ins i,fact_productionorder f, tmp_current_date_fprsnapshot
WHERE i.dd_ordernumber = f.dd_ordernumber AND i.dd_orderitemno = f.dd_orderitemno;

drop table if exists max_holder_43;
Create table max_holder_43
as Select ifnull(max(fact_productionorder_snapshotid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_productionorder_snapshot;

/* For existing ord-item, pick up orig fields from latest prod-order snapshot for that ord-item*/
/* Ensure that only those are picked up that have the latest schedule finish date in next week */
INSERT INTO fact_productionorder_snapshot(
    fact_productionorder_snapshotid,
	fact_productionorderid,
	Dim_DateidRelease,
	Dim_DateidBOMExplosion,
	Dim_DateidLastScheduling,
	Dim_DateidTechnicalCompletion,
	Dim_DateidBasicStart,
	Dim_DateidScheduledRelease,
	Dim_DateidScheduledFinish,
	Dim_DateidScheduledStart,
	Dim_DateidActualStart,
	Dim_DateidConfirmedOrderFinish,
	Dim_DateidActualHeaderFinish,
	Dim_DateidActualHeaderFinishDate_merck,
	Dim_DateidActualRelease,
	Dim_DateidActualItemFinish,
	Dim_DateidPlannedOrderDelivery,
	Dim_DateidRoutingTransfer,
	Dim_DateidBasicFinish,
	Dim_ControllingAreaid,
	Dim_ProfitCenterId,
	Dim_tasklisttypeid,
	Dim_PartidHeader,
	Dim_bomstatusid,
	Dim_bomusageid,
	dim_productionschedulerid,
	Dim_ProcurementID,
	Dim_SpecialProcurementID,
	Dim_UnitOfMeasureid,
	Dim_PartidItem,
	Dim_AccountCategoryid,
	Dim_StockTypeid,
	Dim_StorageLocationid,
	Dim_Plantid,
	dim_specialstockid,
	Dim_ConsumptionTypeid,
	Dim_SalesOrgid,
	Dim_PurchaseOrgid,
	Dim_Currencyid,
	Dim_Companyid,
	Dim_ordertypeid,
	dim_productionorderstatusid,
	dim_productionordermiscid,
	Dim_BuildTypeid,
	dd_ordernumber,
	dd_orderitemno,
	dd_bomexplosionno,
	dd_plannedorderno,
	dd_SalesOrderNo,
	dd_SalesOrderItemNo,
	dd_SalesOrderDeliveryScheduleNo,
	ct_ConfirmedReworkQty,
	ct_ScrapQty,
	ct_OrderItemQty,
	ct_TotalOrderQty,
	ct_GRQty,
	ct_GRProcessingTime,
	amt_estimatedTotalCost,
	amt_ValueGR,
	dd_BOMLevel,
	dd_sequenceno,
	dd_ObjectNumber,
	ct_ConfirmedScrapQty,
	dd_OrderDescription,
	Dim_MrpControllerId,
	dim_currencyid_TRA,
	dim_currencyid_GBL,
	dd_OperationNumber,
	dd_WorkCenter,
	dim_customerid,
	dd_RoutingOperationNo,
	dd_snapshotdate,
	dim_dateidscheduledfinish_orig_merck,
	ct_totalorderqty_orig_merck,
	dd_cancelledorder_orig_merck,
	ct_underdeliverytol_orig_merck,
	ct_overdeliverytol_orig_merck,
	dd_unimitedoverdelivery_orig_merck,
	ct_GRQty_orig_merck,
	ct_OrderItemQty_orig_merck,
	dd_cancelledorder,
	ct_underdeliverytol_merck,
	ct_overdeliverytol_merck,
	dd_unimitedoverdelivery_merck,
	ct_snapshotcount,
	dd_latestsnapshotflag,
	std_exchangerate_dateid)
SELECT 
    (SELECT max_holder_43.maxid FROM max_holder_43) + row_number() over(order by '') fact_productionorder_snapshotid,
	fpr.fact_productionorderid,
	fpr.Dim_DateidRelease,
	fpr.Dim_DateidBOMExplosion,
	fpr.Dim_DateidLastScheduling,
	fpr.Dim_DateidTechnicalCompletion,
	fpr.Dim_DateidBasicStart,
	fpr.Dim_DateidScheduledRelease,
	fpr.Dim_DateidScheduledFinish,
	fpr.Dim_DateidScheduledStart,
	fpr.Dim_DateidActualStart,
	fpr.Dim_DateidConfirmedOrderFinish,
	fpr.Dim_DateidActualHeaderFinish,
	fpr.Dim_DateidActualHeaderFinishDate_merck,
	fpr.Dim_DateidActualRelease,
	fpr.Dim_DateidActualItemFinish,
	fpr.Dim_DateidPlannedOrderDelivery,
	fpr.Dim_DateidRoutingTransfer,
	fpr.Dim_DateidBasicFinish,
	fpr.Dim_ControllingAreaid,
	fpr.Dim_ProfitCenterId,
	fpr.Dim_tasklisttypeid,
	fpr.Dim_PartidHeader,
	fpr.Dim_bomstatusid,
	fpr.Dim_bomusageid,
	fpr.dim_productionschedulerid,
	fpr.Dim_ProcurementID,
	fpr.Dim_SpecialProcurementID,
	fpr.Dim_UnitOfMeasureid,
	fpr.Dim_PartidItem,
	fpr.Dim_AccountCategoryid,
	fpr.Dim_StockTypeid,
	fpr.Dim_StorageLocationid,
	fpr.Dim_Plantid,
	fpr.dim_specialstockid,
	fpr.Dim_ConsumptionTypeid,
	fpr.Dim_SalesOrgid,
	fpr.Dim_PurchaseOrgid,
	fpr.Dim_Currencyid,
	fpr.Dim_Companyid,
	fpr.Dim_ordertypeid,
	fpr.dim_productionorderstatusid,
	fpr.dim_productionordermiscid,
	fpr.Dim_BuildTypeid,
	fpr.dd_ordernumber,
	fpr.dd_orderitemno,
	fpr.dd_bomexplosionno,
	fpr.dd_plannedorderno,
	fpr.dd_SalesOrderNo,
	fpr.dd_SalesOrderItemNo,
	fpr.dd_SalesOrderDeliveryScheduleNo,
	fpr.ct_ConfirmedReworkQty,
	fpr.ct_ScrapQty,
	fpr.ct_OrderItemQty,
	fpr.ct_TotalOrderQty,
	fpr.ct_GRQty,
	fpr.ct_GRProcessingTime,
	fpr.amt_estimatedTotalCost,
	fpr.amt_ValueGR,
	fpr.dd_BOMLevel,
	fpr.dd_sequenceno,
	fpr.dd_ObjectNumber,
	fpr.ct_ConfirmedScrapQty,
	fpr.dd_OrderDescription,
	fpr.Dim_MrpControllerId,
	fpr.dim_currencyid_TRA,
	fpr.dim_currencyid_GBL,
	fpr.dd_OperationNumber,
	fpr.dd_WorkCenter,
	fpr.dim_customerid,
	fpr.dd_RoutingOperationNo,
	currdate,
	s.dim_dateidscheduledfinish,
	s.ct_totalorderqty,
	s.dd_cancelledorder,
	s.ct_underdeliverytol_merck,
	s.ct_overdeliverytol_merck,
	s.dd_unimitedoverdelivery_merck,
	s.ct_GRQty,
	s.ct_OrderItemQty,
	fpr.dd_cancelledorder,
	fpr.ct_underdeliverytol_merck,
	fpr.ct_overdeliverytol_merck,
	fpr.dd_unimitedoverdelivery_merck,
	s.ct_snapshotcount + 1,
	'Y',
	fpr.std_exchangerate_dateid std_exchangerate_dateid	
FROM 	fact_productionorder fpr, tmp_current_date_fprsnapshot, tmp_fact_productionorder_snapshot_maxdate s,
	tmp_fact_productionorder_snapshot_upd u,max_holder_43
WHERE s.dd_ordernumber = fpr.dd_ordernumber AND s.dd_orderitemno = fpr.dd_orderitemno
AND u.dd_ordernumber = s.dd_ordernumber AND u.dd_orderitemno = s.dd_orderitemno;

/* Update dim_dateidsnapshot */
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidsnapshot = dt.dim_dateid
FROM dim_date dt,dim_company dc,fact_productionorder_snapshot s
WHERE s.dd_snapshotdate = dt.datevalue
AND s.dim_companyid = dc.dim_companyid AND dc.companycode = dt.CompanyCode
AND dt.plantcode_factory = 'Not Set'
AND s.dim_dateidsnapshot <> dt.dim_dateid;

/* 26 August 2015 Begin changes LiviuT */
UPDATE fact_productionorder_snapshot s
SET ct_OrderUnderdelTolerance = fpr.ct_OrderUnderdelTolerance
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.ct_OrderUnderdelTolerance <> fpr.ct_OrderUnderdelTolerance;
	  
UPDATE fact_productionorder_snapshot s
SET ct_OrderOverdelivTolerance = fpr.ct_OrderOverdelivTolerance
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr,fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.ct_OrderOverdelivTolerance <> fpr.ct_OrderOverdelivTolerance;

UPDATE fact_productionorder_snapshot s
SET dd_OrderUnltdTolerance = fpr.dd_unlimitedoverdelflagorder
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_OrderUnltdTolerance <> fpr.dd_unlimitedoverdelflagorder;

/* Octavian: Every Angle Transition Addons */
update fact_productionorder_snapshot ia
set ia.dim_batchid = b.dim_batchid,
ia.dw_update_date = current_timestamp
from dim_batch b, dim_part dp, fact_productionorder_snapshot ia
where
ia.dd_batch = b.batchnumber and
ia.Dim_PartidHeader = dp.dim_partid and dp.partnumber = b.partnumber and
 dp.plant = b.plantcode and
ia.dim_batchid <> b.dim_batchid;

UPDATE fact_productionorder_snapshot s
SET s.dim_tasklistheaderid = fpr.dim_tasklistheaderid
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_tasklistheaderid <> fpr.dim_tasklistheaderid;
/* Octavian: Every Angle Transition Addons */

/* Georgiana: Every Angle Transition Addons */

UPDATE fact_productionorder_snapshot s
SET s.ct_expectedsurplusordeficit = fpr.ct_expectedsurplusordeficit
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.ct_expectedsurplusordeficit <> fpr.ct_expectedsurplusordeficit;

	  
UPDATE fact_productionorder_snapshot s
SET s.dd_alternativebom = fpr.dd_alternativebom
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_alternativebom <> fpr.dd_alternativebom;
	  
UPDATE fact_productionorder_snapshot s
SET s.dd_billofmaterial = fpr.dd_billofmaterial
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_billofmaterial <> fpr.dd_billofmaterial;

UPDATE fact_productionorder_snapshot s
SET s.Dim_baseUnitOfMeasureid = fpr.Dim_baseUnitOfMeasureid
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.Dim_baseUnitOfMeasureid <> fpr.Dim_baseUnitOfMeasureid;
	  
UPDATE fact_productionorder_snapshot s
SET s.dd_ordercategory = fpr.dd_ordercategory
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_ordercategory <> fpr.dd_ordercategory;

	  
UPDATE fact_productionorder_snapshot s
SET s.dd_enteredby = fpr.dd_enteredby
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_enteredby <> fpr.dd_enteredby;
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_tehnicalcompletiondateid = fpr.dim_tehnicalcompletiondateid
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_tehnicalcompletiondateid <> fpr.dim_tehnicalcompletiondateid;
	  
UPDATE fact_productionorder_snapshot s
SET s.Dim_dateidorderduedate = fpr.Dim_dateidorderduedate
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.Dim_dateidorderduedate <> fpr.Dim_dateidorderduedate;
	  
UPDATE fact_productionorder_snapshot s
SET s.dd_groupcounter = fpr.dd_groupcounter
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_groupcounter <> fpr.dd_groupcounter;
	  
UPDATE fact_productionorder_snapshot s
SET s.dd_group = fpr.dd_group
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_group <> fpr.dd_group;
	  
		  
UPDATE fact_productionorder_snapshot s
SET s.dd_productionversion = fpr.dd_productionversion
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_productionversion <> fpr.dd_productionversion; 

	  
UPDATE fact_productionorder_snapshot s
SET s.dim_productionordertypeid = fpr.dim_productionordertypeid
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_productionordertypeid <> fpr.dim_productionordertypeid; 
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidcreationdate = fpr.dim_dateidcreationdate
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidcreationdate <> fpr.dim_dateidcreationdate; 
	  
UPDATE fact_productionorder_snapshot s
SET s.dd_overheadkey = fpr.dd_overheadkey
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_overheadkey <> fpr.dd_overheadkey; 
	  
UPDATE fact_productionorder_snapshot s
SET s.dd_productversion = fpr.dd_productversion
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_productversion <> fpr.dd_productversion; 

	  
/* Octavian: Every Angle Transition */

UPDATE fact_productionorder_snapshot s
SET s.dd_released = fpr.dd_released
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_released <> fpr.dd_released;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_materialshortage = fpr.dd_materialshortage
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_materialshortage <> fpr.dd_materialshortage;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_confirmed = fpr.dd_confirmed
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_confirmed <> fpr.dd_confirmed;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_delivered = fpr.dd_delivered
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_delivered <> fpr.dd_delivered;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_technicallycompleted = fpr.dd_technicallycompleted
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_technicallycompleted <> fpr.dd_technicallycompleted;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_closed = fpr.dd_closed
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_closed <> fpr.dd_closed;
	  
UPDATE fact_productionorder_snapshot s
SET s.dd_paproductquality = fpr.dd_paproductquality
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_paproductquality <> fpr.dd_paproductquality;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_capacityreasons = fpr.dd_capacityreasons
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_capacityreasons <> fpr.dd_capacityreasons;

UPDATE fact_productionorder_snapshot s
SET s.dd_waitforsupplypurchased = fpr.dd_waitforsupplypurchased
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_waitforsupplypurchased <> fpr.dd_waitforsupplypurchased;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_expirydateqpend = fpr.dd_expirydateqpend
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_expirydateqpend <> fpr.dd_expirydateqpend;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_changeversion = fpr.dd_changeversion
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_changeversion <> fpr.dd_changeversion;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_requestcust = fpr.dd_requestcust
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_requestcust <> fpr.dd_requestcust;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_delaypackpro = fpr.dd_delaypackpro
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_delaypackpro <> fpr.dd_delaypackpro;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_othercomment = fpr.dd_othercomment
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_othercomment <> fpr.dd_othercomment;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_paforecastdemand = fpr.dd_paforecastdemand
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_paforecastdemand <> fpr.dd_paforecastdemand;
      
UPDATE fact_productionorder_snapshot s
SET s.dd_datefictoutind = fpr.dd_datefictoutind
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_datefictoutind <> fpr.dd_datefictoutind;
/* Octavian: Every Angle Transition END */

/*Georgiana Every Angle Transition Addons*/
UPDATE fact_productionorder_snapshot po
SET po.dd_changeddocreleased = fpr.dd_changeddocreleased,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_changeddocreleased <> fpr.dd_changeddocreleased; 

UPDATE fact_productionorder_snapshot po
SET po.dd_changedmaterialshortage = fpr.dd_changedmaterialshortage,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_changedmaterialshortage <> fpr.dd_changedmaterialshortage; 

UPDATE fact_productionorder_snapshot po
SET po.dd_changeconfirmed = fpr.dd_changeconfirmed,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_changeconfirmed <> fpr.dd_changeconfirmed; 

UPDATE fact_productionorder_snapshot po
SET po.dd_changedelivered = fpr.dd_changedelivered,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_changedelivered <> fpr.dd_changedelivered; 

UPDATE fact_productionorder_snapshot po
SET po.dd_changetechnicallycompleted = fpr.dd_changetechnicallycompleted,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_changetechnicallycompleted <> fpr.dd_changetechnicallycompleted; 

UPDATE fact_productionorder_snapshot po
SET po.dd_changeclosed = fpr.dd_changeclosed,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_changeclosed <> fpr.dd_changeclosed; 


UPDATE fact_productionorder_snapshot po
SET po.dd_changedeletionflag = fpr.dd_changedeletionflag,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_changedeletionflag <> fpr.dd_changedeletionflag; 

UPDATE fact_productionorder_snapshot po
SET po.dd_changematerialcommitted = fpr.dd_changematerialcommitted,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_changematerialcommitted <> fpr.dd_changematerialcommitted; 

/*Georgiana EA Changes End*/

/* Octavian EA missing JCDS value */
UPDATE fact_productionorder_snapshot po
SET po.dd_qmtestsscheduled = fpr.dd_qmtestsscheduled,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_qmtestsscheduled <> fpr.dd_qmtestsscheduled;
/* Octavian EA missing JCDS value */

/* Octavian adding AUFK-LTEXT and AUFK-KTEXT */
UPDATE fact_productionorder_snapshot po
SET po.dd_OrderDescription = fpr.dd_OrderDescription,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_OrderDescription <> fpr.dd_OrderDescription; 

UPDATE fact_productionorder_snapshot po
SET po.dd_longtextexists = fpr.dd_longtextexists,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dd_longtextexists <> fpr.dd_longtextexists; 
/* Octavian adding AUFK-LTEXT */

/*23 Sept Georgiana Adding AFKO_BMENGE*/

UPDATE fact_productionorder_snapshot po
SET  po.ct_basequantity = fpr.ct_basequantity
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
	AND po.ct_basequantity <> fpr.ct_basequantity;
	
	/*Georgiana End of changes*/
/* 18 Oct 2016 Georgiana Adding dim_workcenterid according to BI-4441*/

UPDATE fact_productionorder_snapshot po
SET po.dim_workcenterid= fpr.dim_workcenterid,
dw_update_date = CURRENT_TIMESTAMP
FROM fact_productionorder fpr, fact_productionorder_snapshot po
WHERE po.dd_ordernumber = fpr.dd_ordernumber
      AND po.dd_orderitemno = fpr.dd_orderitemno
AND po.dim_workcenterid <> fpr.dim_workcenterid; 

/*Georgiana End Of changes*/

/*START BI-5163 Alin*/
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidtechcompldt = fpr.dim_dateidtechcompldt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidtechcompldt <> fpr.dim_dateidtechcompldt; 

UPDATE fact_productionorder_snapshot s
SET s.dim_dateidcloseddt = fpr.dim_dateidcloseddt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidcloseddt <> fpr.dim_dateidcloseddt; 
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidconfirmeddt = fpr.dim_dateidconfirmeddt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidconfirmeddt <> fpr.dim_dateidconfirmeddt; 
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidchangedeletionflagdt = fpr.dim_dateidchangedeletionflagdt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidchangedeletionflagdt <> fpr.dim_dateidchangedeletionflagdt; 
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidchangedelivereddt = fpr.dim_dateidchangedelivereddt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidchangedelivereddt <> fpr.dim_dateidchangedelivereddt;
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidchangematerialcommitteddt = fpr.dim_dateidchangematerialcommitteddt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidchangematerialcommitteddt <> fpr.dim_dateidchangematerialcommitteddt;
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidchangedmaterialshortageddt = fpr.dim_dateidchangedmaterialshortageddt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidchangedmaterialshortageddt <> fpr.dim_dateidchangedmaterialshortageddt;
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidqmtestsscheduleddt = fpr.dim_dateidqmtestsscheduleddt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidqmtestsscheduleddt <> fpr.dim_dateidqmtestsscheduleddt;
	  
UPDATE fact_productionorder_snapshot s
SET s.dim_dateidchangeddocreleaseddt = fpr.dim_dateidchangeddocreleaseddt
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dim_dateidchangeddocreleaseddt <> fpr.dim_dateidchangeddocreleaseddt;

/*END BI-5163 Alin*/

/*START 24 feb 2017 - Alin Gh BI-5573*/
UPDATE fact_productionorder_snapshot s
SET s.dd_overheadkeydescription = fpr.dd_overheadkeydescription
	,dw_update_date = current_timestamp
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber
      AND s.dd_orderitemno = fpr.dd_orderitemno
      AND s.dd_overheadkeydescription  <> fpr.dd_overheadkeydescription; 
/*END 24 feb 2017 - Alin Gh BI-5573*/

/* Yogini APP-7256 8 Sep 2017 */
UPDATE fact_productionorder_snapshot s
SET dim_dateidactualheaderfinishdate_prodorder = fpr.Dim_DateidActualHeaderFinishDate_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber 
AND s.dd_orderitemno = fpr.dd_orderitemno
AND dim_dateidactualheaderfinishdate_prodorder <> fpr.Dim_DateidActualHeaderFinishDate_merck;

UPDATE fact_productionorder_snapshot s
SET dim_dateidactualitemfinish_prodorder = fpr.dim_dateidactualitemfinish
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber 
AND s.dd_orderitemno = fpr.dd_orderitemno
AND dim_dateidactualitemfinish_prodorder <> fpr.dim_dateidactualitemfinish;

UPDATE fact_productionorder_snapshot s
SET dim_dateidconfirmedorderfinish_prodorder = fpr.dim_dateidconfirmedorderfinish
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder fpr, fact_productionorder_snapshot s
WHERE s.dd_ordernumber = fpr.dd_ordernumber 
AND s.dd_orderitemno = fpr.dd_orderitemno
AND dim_dateidconfirmedorderfinish_prodorder <> fpr.dim_dateidconfirmedorderfinish;
/* END Yogini APP-7256 8 Sep 2017 */

/* 25 Jan 2017 Georgiana Adding ct_SA_Hit_Manual and ct_SA_Miss_Manual according to BI-5307*/

merge into fact_productionorder_snapshot f
using (
select distinct fact_productionorder_snapshotid,SA_Hit_Manual,SA_Miss_Manual from 	dim_date d, fact_productionorder_snapshot f,SA_manualupload stg, dim_plant pl
where
d.dim_dateid=dim_dateidscheduledfinish_orig_merck 
and pl.plantcode=stg.plant_code
and pl.dim_plantid=f.dim_plantid
and calendarweekid=stg.period) t
on t.fact_productionorder_snapshotid=f.fact_productionorder_snapshotid
when matched then update set
ct_SA_Hit_Manual =ifnull(t.SA_Hit_Manual,0),
ct_SA_Miss_Manual= ifnull(t.SA_Miss_Manual,0);

/*Georgiana End Of Changes*/

/* Madalina 26 Apr 2017 - APP-6076 */
/* facts inserts */
/*keep unique values of Calendar Week, per each Plant*/
drop table if exists tmp_dimDateCalendarWeek_PO;
create table tmp_dimDateCalendarWeek_PO as
select * from (
		select dim_dateid,CalendarWeekYr,MonthYear, plantcode_factory, datevalue, row_number() over (partition by CalendarWeekYr, plantcode_factory order by dayofmonth asc) rn from dim_date) t
		where rn = 1
		order by 1;
		
/* insert into Production Order the plant - date combinations for 2015-01 - today */
drop table if exists brinit_date_plants_prodords;
create table brinit_date_plants_prodords as
select distinct dd.dim_dateid as dim_dateidscheduledfinish,
    dp.dim_plantid as dim_plantid
/*from dim_date dd*/
from tmp_dimDateCalendarWeek_PO dd
inner join dim_plant dp on dd.plantcode_factory = dp.plantcode
where plantcode_factory in ( select plantcode from newiteminstockbrinit)
and calendarweekyr between '2015-01' and (select distinct calendarweekyr from dim_date where datevalue = current_date)
and not exists ( select 1 
				from fact_productionorder pos
				inner join dim_date od on pos.dim_dateidscheduledfinish = od.dim_dateid
				where dd.datevalue = od.datevalue
				and dd.plantcode_factory = od.plantcode_factory);

Drop table if exists max_holder_prodorder;
Create table max_holder_prodorder
as Select ifnull(max(fact_productionorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) as maxid
from fact_productionorder;

insert into fact_productionorder
(fact_productionorderid, dd_ordernumber, dd_orderitemno, dim_dateidscheduledfinish, dim_plantid, dd_sacommentmanual_flag)
select 
	max_holder_prodorder.maxid + row_number() over(order by '') as fact_productionorderid,
	'Not Available' dd_ordernumber,
	0 as dd_orderitemno,
	dim_dateidscheduledfinish,
	dim_plantid, 
	1 as dd_sacommentmanual_flag
from brinit_date_plants_prodords, max_holder_prodorder;  

/* delete duplicates of Calendar week from PO*/
merge into fact_productionorder fil using
(select distinct fact_productionorderid
	 from fact_productionorder fil
		inner join dim_date trd on fil.dim_dateidscheduledfinish = trd.dim_dateid
	where not exists
		( select 1 from tmp_dimDateCalendarWeek_PO tmp
			where trd.dim_dateid = tmp.dim_dateid)
        and dd_ordernumber = 'Not Available'
		and dd_sacommentmanual_flag = 1) del
on fil.fact_productionorderid = del.fact_productionorderid
when matched then delete;  

/* insert into Production Order Snapshot the plant - date combinations for 2015-01 - today */
/*these rows won't be inserted within the initial insert, because of the companycode conditions*/
insert into fact_productionorder_snapshot fpos
(fact_productionorderid, dd_ordernumber, dd_orderitemno, dim_dateidscheduledfinish_orig_merck, dim_plantid, dd_sacommentmanual_flag)
select 
fact_productionorderid, dd_ordernumber, dd_orderitemno, dim_dateidscheduledfinish, dim_plantid, dd_sacommentmanual_flag
from fact_productionorder fpo
where dd_sacommentmanual_flag = 1
and dd_ordernumber = 'Not Available'
and dd_orderitemno = 0
and not exists ( select 1 from fact_productionorder_snapshot fpos
					 where fpos.fact_productionorderid = fpo.fact_productionorderid);  

/* delete duplicates of Calendar week from POS*/
merge into fact_productionorder_snapshot fil using
(select distinct fact_productionorderid
	 from fact_productionorder_snapshot fil
		inner join dim_date trd on fil.dim_dateidscheduledfinish_orig_merck = trd.dim_dateid
	where not exists
		( select 1 from tmp_dimDateCalendarWeek_PO tmp
			where trd.dim_dateid = tmp.dim_dateid)
        and dd_ordernumber = 'Not Available'
		and dd_sacommentmanual_flag = 1) del
on fil.fact_productionorderid = del.fact_productionorderid
when matched then delete;  

/* fact updates - for the manual comment*/
/* if new orders are added to area, with the brinit combinations, then the comments need to be updated for these new records --*/

drop table if exists tmp_for_update_delete_brinit_comments_prodorder;
create table tmp_for_update_delete_brinit_comments_prodorder as
select original_rows.fact_productionorderid as original_fact_productionorderid, 
	BRINIT_rows.fact_productionorderid as BRINIT_fact_productionorderid, 
	BRINIT_rows.dim_dateidscheduledfinish_orig_merck, 
	BRINIT_rows.dim_plantid, 
	BRINIT_rows.dd_sacommentmanual,
	BRINIT_rows.dd_sa_miss_manual,
	BRINIT_rows.dd_satop75impactedmanual,
	BRINIT_rows.dd_sa_hit_manual
	from
		(select fact_productionorderid, dim_dateidscheduledfinish_orig_merck, dim_plantid, dd_sacommentmanual, dd_sa_miss_manual, dd_satop75impactedmanual, dd_sa_hit_manual
		from fact_productionorder_snapshot
		where dd_sacommentmanual_flag = 1
			and dd_ordernumber = 'Not Available'
			and dd_orderitemno = 0) BRINIT_rows
	inner join
		(select fact_productionorderid, dim_dateidscheduledfinish_orig_merck, dim_plantid, dd_sacommentmanual, dd_sa_miss_manual, dd_satop75impactedmanual, dd_sa_hit_manual
		from fact_productionorder_snapshot
		where dd_sacommentmanual_flag = 0
			and dd_ordernumber <> 'Not Available'
			) original_rows
	on BRINIT_rows.dim_dateidscheduledfinish_orig_merck = original_rows.dim_dateidscheduledfinish_orig_merck
	and BRINIT_rows.dim_plantid =original_rows.dim_plantid;

/* comments update */
merge into fact_productionorder_snapshot fi using
	(select original_fact_productionorderid, dd_sacommentmanual, dd_sa_miss_manual, dd_satop75impactedmanual, dd_sa_hit_manual
	from
		tmp_for_update_delete_brinit_comments_prodorder
	) upd
on fi.fact_productionorderid = upd.original_fact_productionorderid
when matched then update
set fi.dd_sacommentmanual = upd.dd_sacommentmanual,
	fi.dd_sa_miss_manual = upd.dd_sa_miss_manual,
	fi.dd_satop75impactedmanual = upd.dd_satop75impactedmanual,
	fi.dd_sa_hit_manual	= upd.dd_sa_hit_manual;

/* backup the rows that will be deleted */
insert into deleted_brinit_comments_for_PRODORD (dim_dateidscheduledfinish_orig_merck, dim_plantid, dd_sacommentmanual, dd_sa_miss_manual, dd_satop75impactedmanual, dd_sa_hit_manual)
select  fi.dim_dateidscheduledfinish_orig_merck, fi.dim_plantid, fi.dd_sacommentmanual, fi.dd_sa_miss_manual, fi.dd_satop75impactedmanual, fi.dd_sa_hit_manual
from tmp_for_update_delete_brinit_comments_prodorder tmp
inner join fact_productionorder_snapshot fi on fi.dim_plantid = tmp.dim_plantid
where fi.dd_ordernumber = 'Not Available'
	and fi.dd_sacommentmanual <> 'No comments'
	and dd_sacommentmanual_flag = 1;

/*delete all brinit rows from Prod Order + Prod Order snapshot, where the plants are now available in SAP, with a valid Order number*/
merge into fact_productionorder_snapshot fi using
	(select fi.fact_productionorderid
	from tmp_for_update_delete_brinit_comments_prodorder tmp 
	inner join fact_productionorder_snapshot fi on fi.dim_plantid = tmp.dim_plantid
	where dd_ordernumber = 'Not Available'
	and dd_sacommentmanual_flag = 1
	) del
on fi.fact_productionorderid = del.fact_productionorderid
when matched then delete; 

merge into fact_productionorder fi using
	(select fi.fact_productionorderid
	from tmp_for_update_delete_brinit_comments_prodorder tmp 
	inner join fact_productionorder fi on fi.dim_plantid = tmp.dim_plantid
	where dd_ordernumber = 'Not Available'
	and dd_sacommentmanual_flag = 1
	) del
on fi.fact_productionorderid = del.fact_productionorderid
when matched then delete;
/*END APP-6076*/

/* Andrei Robescu APP-9225 */
drop table if exists tmp_mlgn_t320;
create table tmp_mlgn_t320 as select distinct T320_WERKS, MLGN_MATNR, T320_LGNUM, MLGN_LGNUM, MLGN_BSSKZ from
MLGN , T320 WHERE T320_LGNUM = MLGN_LGNUM;

update fact_productionorder_snapshot f
set f.dd_WarehouseSpecialMovInd = ifnull(t.MLGN_BSSKZ,'Not Set')
from fact_productionorder_snapshot f, tmp_mlgn_t320 t, dim_part p, dim_plant pl
where f.dim_partiditem = p.dim_partid
/* and p.plant = pl.plantcode */
and f.dim_plantid = pl.dim_plantid
and pl.plantcode = t.T320_WERKS
and p.partnumber = t.MLGN_MATNR
and f.dd_WarehouseSpecialMovInd <> ifnull(t.MLGN_BSSKZ,'Not Set');
/* end Andrei Robescu APP-9225 */

DROP TABLE IF EXISTS tmp_current_date_fprsnapshot;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_maxdate_1;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_maxdate;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_ins;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_del;
DROP TABLE IF EXISTS tmp_fact_productionorder_snapshot_upd;

 /*19 Apr 2018 Georgiana Changes according to APP-8617 creating a default logic based on Plant.Plant Title Original Schedules Finish Date. Date 
  additional filters for this will be: equals with Year to Date,not equal with Current Month and not equal with Current Week*/
  
drop table if exists  tmp_for_default_SACOMMENT;
create table tmp_for_default_SACOMMENT as
select distinct calendarmonthid,planttitle_merck,DD_SACOMMENTMANUAL,f.dim_plantid,f.dim_dateidscheduledfinish_orig_merck from fact_productionorder_snapshot f, dim_date d, dim_plant pl
where dim_dateidscheduledfinish_orig_merck=dim_dateid
and f.dim_plantid=pl.dim_plantid
and year(datevalue)=year(current_date)
and calendarmonthid = concat( year(current_date), case when length(month(current_date)-1)=1 then concat('0', month(current_date)-1) else month(current_Date)-1 end)
and week(datevalue)<> week(current_date)
and DD_SACOMMENTMANUAL <>'No comments'
order by 2,1;


merge into fact_productionorder_snapshot f
using ( select distinct fact_productionorder_snapshotid,t.DD_SACOMMENTMANUAL
from fact_productionorder_snapshot f,tmp_for_default_SACOMMENT t, dim_date d1, dim_date d2
where f.dim_plantid = t.dim_plantid
and f.dim_dateidscheduledfinish_orig_merck = d1.dim_dateid
and t.dim_dateidscheduledfinish_orig_merck = d2.dim_dateid
and d1.calendarmonthid=d2.calendarmonthid
and d1.plantcode_factory=d2.plantcode_factory
and d1.companycode=d2.companycode
and f.DD_SACOMMENTMANUAL='No comments') t
on f.fact_productionorder_snapshotid = t.fact_productionorder_snapshotid
when matched then update set f.DD_SACOMMENTMANUAL=t.DD_SACOMMENTMANUAL;

/*19 Apr 2018 end of changes*/
drop table if exists  tmp_for_default_SACOMMENT;


