
 /* Update the Target Master Recipe Days BI-4755*/ 
 merge into fact_scm_test fmm using 
         ( select distinct fmm.fact_mmprodhierarchyid 
           ,raw_1.targerMasterRecipe raw_1_targerMasterRecipe 
 ,raw_1.dim_partid dim_partid_raw_1 
 ,antigen_1.targerMasterRecipe antigen_1_targerMasterRecipe 
 ,antigen_1.dim_partid dim_partid_antigen_1 
 ,antigen_2.targerMasterRecipe antigen_2_targerMasterRecipe 
 ,antigen_2.dim_partid dim_partid_antigen_2 
 ,antigen_3.targerMasterRecipe antigen_3_targerMasterRecipe 
 ,antigen_3.dim_partid dim_partid_antigen_3 
 ,antigen_4.targerMasterRecipe antigen_4_targerMasterRecipe 
 ,antigen_4.dim_partid dim_partid_antigen_4 
 ,antigen_5.targerMasterRecipe antigen_5_targerMasterRecipe 
 ,antigen_5.dim_partid dim_partid_antigen_5 
 ,bulk_1.targerMasterRecipe bulk_1_targerMasterRecipe 
 ,bulk_1.dim_partid dim_partid_bulk_1 
 ,bulk_2.targerMasterRecipe bulk_2_targerMasterRecipe 
 ,bulk_2.dim_partid dim_partid_bulk_2 
 ,bulk_3.targerMasterRecipe bulk_3_targerMasterRecipe 
 ,bulk_3.dim_partid dim_partid_bulk_3 
 ,fpu_1.targerMasterRecipe fpu_1_targerMasterRecipe 
 ,fpu_1.dim_partid dim_partid_fpu_1 
 ,fpu_2.targerMasterRecipe fpu_2_targerMasterRecipe 
 ,fpu_2.dim_partid dim_partid_fpu_2 
 ,fpu_3.targerMasterRecipe fpu_3_targerMasterRecipe 
 ,fpu_3.dim_partid dim_partid_fpu_3 
 ,fpu_4.targerMasterRecipe fpu_4_targerMasterRecipe 
 ,fpu_4.dim_partid dim_partid_fpu_4 
 ,fpu_5.targerMasterRecipe fpu_5_targerMasterRecipe 
 ,fpu_5.dim_partid dim_partid_fpu_5 
 ,fpp_1.targerMasterRecipe fpp_1_targerMasterRecipe 
 ,fpp_1.dim_partid dim_partid_fpp_1 
 ,fpp_2.targerMasterRecipe fpp_2_targerMasterRecipe 
 ,fpp_2.dim_partid dim_partid_fpp_2 
 ,fpp_3.targerMasterRecipe fpp_3_targerMasterRecipe 
 ,fpp_3.dim_partid dim_partid_fpp_3 
 ,comops.targerMasterRecipe comops_targerMasterRecipe 
 ,comops.dim_partid dim_partid_comops 
  from fact_scm_test fmm 
                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) raw_1 on raw_1.dim_partid = fmm.dim_partid_raw_1 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) antigen_1 on antigen_1.dim_partid = fmm.dim_partid_antigen_1 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) antigen_2 on antigen_2.dim_partid = fmm.dim_partid_antigen_2 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) antigen_3 on antigen_3.dim_partid = fmm.dim_partid_antigen_3 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) antigen_4 on antigen_4.dim_partid = fmm.dim_partid_antigen_4 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) antigen_5 on antigen_5.dim_partid = fmm.dim_partid_antigen_5 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) bulk_1 on bulk_1.dim_partid = fmm.dim_partid_bulk_1 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) bulk_2 on bulk_2.dim_partid = fmm.dim_partid_bulk_2 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) bulk_3 on bulk_3.dim_partid = fmm.dim_partid_bulk_3 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) fpu_1 on fpu_1.dim_partid = fmm.dim_partid_fpu_1 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) fpu_2 on fpu_2.dim_partid = fmm.dim_partid_fpu_2 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) fpu_3 on fpu_3.dim_partid = fmm.dim_partid_fpu_3 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) fpu_4 on fpu_4.dim_partid = fmm.dim_partid_fpu_4 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) fpu_5 on fpu_5.dim_partid = fmm.dim_partid_fpu_5 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) fpp_1 on fpp_1.dim_partid = fmm.dim_partid_fpp_1 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) fpp_2 on fpp_2.dim_partid = fmm.dim_partid_fpp_2 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) fpp_3 on fpp_3.dim_partid = fmm.dim_partid_fpp_3 
                                  left join (select distinct tmp.targerMasterRecipe, dp.dim_partid  
                                          from dim_part dp, tmp_PlanOrder_TargetPerPart tmp 
                                          where tmp.partnumber = dp.partnumber) comops on comops.dim_partid = fmm.dim_partid_comops 
                                 ) upd 
             on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
             when matched then update 
             set 
fmm.ct_TargetMasterRecipe_raw_1 = ifnull(upd.raw_1_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_antigen_1 = ifnull(upd.antigen_1_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_antigen_2 = ifnull(upd.antigen_2_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_antigen_3 = ifnull(upd.antigen_3_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_antigen_4 = ifnull(upd.antigen_4_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_antigen_5 = ifnull(upd.antigen_5_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_bulk_1 = ifnull(upd.bulk_1_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_bulk_2 = ifnull(upd.bulk_2_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_bulk_3 = ifnull(upd.bulk_3_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_fpu_1 = ifnull(upd.fpu_1_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_fpu_2 = ifnull(upd.fpu_2_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_fpu_3 = ifnull(upd.fpu_3_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_fpu_4 = ifnull(upd.fpu_4_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_fpu_5 = ifnull(upd.fpu_5_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_fpp_1 = ifnull(upd.fpp_1_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_fpp_2 = ifnull(upd.fpp_2_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_fpp_3 = ifnull(upd.fpp_3_targerMasterRecipe, 0)  
                             ,fmm.ct_TargetMasterRecipe_comops = ifnull(upd.comops_targerMasterRecipe, 0)  
                              ;
 /* Use dim instead of dd for Date User Status SAMR was last set and Date User Status QCCO was last set - BI-4772 */ 
  
 /*Date User Status SAMR was last set*/ 
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid 
 , dd_amrraw_1.dim_dateid dd_amrraw_1_dim_dateid  , dd_amrantigen_1.dim_dateid dd_amrantigen_1_dim_dateid  , dd_amrantigen_2.dim_dateid dd_amrantigen_2_dim_dateid  , dd_amrantigen_3.dim_dateid dd_amrantigen_3_dim_dateid  , dd_amrantigen_4.dim_dateid dd_amrantigen_4_dim_dateid  , dd_amrantigen_5.dim_dateid dd_amrantigen_5_dim_dateid  , dd_amrbulk_1.dim_dateid dd_amrbulk_1_dim_dateid  , dd_amrbulk_2.dim_dateid dd_amrbulk_2_dim_dateid  , dd_amrbulk_3.dim_dateid dd_amrbulk_3_dim_dateid  , dd_amrfpu_1.dim_dateid dd_amrfpu_1_dim_dateid  , dd_amrfpu_2.dim_dateid dd_amrfpu_2_dim_dateid  , dd_amrfpu_3.dim_dateid dd_amrfpu_3_dim_dateid  , dd_amrfpu_4.dim_dateid dd_amrfpu_4_dim_dateid  , dd_amrfpu_5.dim_dateid dd_amrfpu_5_dim_dateid  , dd_amrfpp_1.dim_dateid dd_amrfpp_1_dim_dateid  , dd_amrfpp_2.dim_dateid dd_amrfpp_2_dim_dateid  , dd_amrfpp_3.dim_dateid dd_amrfpp_3_dim_dateid  from fact_scm_test fmm 
                 inner join dim_plant pl on pl.dim_plantid = fmm.dim_plantid 
                 left join dim_date dd_amrraw_1 on pl.companycode = dd_amrraw_1.companycode 
                             and fmm.dd_dateuserstatussamr_raw_1 = dd_amrraw_1.datevalue 
                             and pl.plantcode = dd_amrraw_1.plantcode_factory 
                             left join dim_date dd_amrantigen_1 on pl.companycode = dd_amrantigen_1.companycode 
                             and fmm.dd_dateuserstatussamr_antigen_1 = dd_amrantigen_1.datevalue 
                             and pl.plantcode = dd_amrantigen_1.plantcode_factory 
                             left join dim_date dd_amrantigen_2 on pl.companycode = dd_amrantigen_2.companycode 
                             and fmm.dd_dateuserstatussamr_antigen_2 = dd_amrantigen_2.datevalue 
                             and pl.plantcode = dd_amrantigen_2.plantcode_factory 
                             left join dim_date dd_amrantigen_3 on pl.companycode = dd_amrantigen_3.companycode 
                             and fmm.dd_dateuserstatussamr_antigen_3 = dd_amrantigen_3.datevalue 
                             and pl.plantcode = dd_amrantigen_3.plantcode_factory 
                             left join dim_date dd_amrantigen_4 on pl.companycode = dd_amrantigen_4.companycode 
                             and fmm.dd_dateuserstatussamr_antigen_4 = dd_amrantigen_4.datevalue 
                             and pl.plantcode = dd_amrantigen_4.plantcode_factory 
                             left join dim_date dd_amrantigen_5 on pl.companycode = dd_amrantigen_5.companycode 
                             and fmm.dd_dateuserstatussamr_antigen_5 = dd_amrantigen_5.datevalue 
                             and pl.plantcode = dd_amrantigen_5.plantcode_factory 
                             left join dim_date dd_amrbulk_1 on pl.companycode = dd_amrbulk_1.companycode 
                             and fmm.dd_dateuserstatussamr_bulk_1 = dd_amrbulk_1.datevalue 
                             and pl.plantcode = dd_amrbulk_1.plantcode_factory 
                             left join dim_date dd_amrbulk_2 on pl.companycode = dd_amrbulk_2.companycode 
                             and fmm.dd_dateuserstatussamr_bulk_2 = dd_amrbulk_2.datevalue 
                             and pl.plantcode = dd_amrbulk_2.plantcode_factory 
                             left join dim_date dd_amrbulk_3 on pl.companycode = dd_amrbulk_3.companycode 
                             and fmm.dd_dateuserstatussamr_bulk_3 = dd_amrbulk_3.datevalue 
                             and pl.plantcode = dd_amrbulk_3.plantcode_factory 
                             left join dim_date dd_amrfpu_1 on pl.companycode = dd_amrfpu_1.companycode 
                             and fmm.dd_dateuserstatussamr_fpu_1 = dd_amrfpu_1.datevalue 
                             and pl.plantcode = dd_amrfpu_1.plantcode_factory 
                             left join dim_date dd_amrfpu_2 on pl.companycode = dd_amrfpu_2.companycode 
                             and fmm.dd_dateuserstatussamr_fpu_2 = dd_amrfpu_2.datevalue 
                             and pl.plantcode = dd_amrfpu_2.plantcode_factory 
                             left join dim_date dd_amrfpu_3 on pl.companycode = dd_amrfpu_3.companycode 
                             and fmm.dd_dateuserstatussamr_fpu_3 = dd_amrfpu_3.datevalue 
                             and pl.plantcode = dd_amrfpu_3.plantcode_factory 
                             left join dim_date dd_amrfpu_4 on pl.companycode = dd_amrfpu_4.companycode 
                             and fmm.dd_dateuserstatussamr_fpu_4 = dd_amrfpu_4.datevalue 
                             and pl.plantcode = dd_amrfpu_4.plantcode_factory 
                             left join dim_date dd_amrfpu_5 on pl.companycode = dd_amrfpu_5.companycode 
                             and fmm.dd_dateuserstatussamr_fpu_5 = dd_amrfpu_5.datevalue 
                             and pl.plantcode = dd_amrfpu_5.plantcode_factory 
                             left join dim_date dd_amrfpp_1 on pl.companycode = dd_amrfpp_1.companycode 
                             and fmm.dd_dateuserstatussamr_fpp_1 = dd_amrfpp_1.datevalue 
                             and pl.plantcode = dd_amrfpp_1.plantcode_factory 
                             left join dim_date dd_amrfpp_2 on pl.companycode = dd_amrfpp_2.companycode 
                             and fmm.dd_dateuserstatussamr_fpp_2 = dd_amrfpp_2.datevalue 
                             and pl.plantcode = dd_amrfpp_2.plantcode_factory 
                             left join dim_date dd_amrfpp_3 on pl.companycode = dd_amrfpp_3.companycode 
                             and fmm.dd_dateuserstatussamr_fpp_3 = dd_amrfpp_3.datevalue 
                             and pl.plantcode = dd_amrfpp_3.plantcode_factory 
                               ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update set  fmm.dim_dateuserstatussamr_raw_1 = ifnull(upd.dd_amrraw_1_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_antigen_1 = ifnull(upd.dd_amrantigen_1_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_antigen_2 = ifnull(upd.dd_amrantigen_2_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_antigen_3 = ifnull(upd.dd_amrantigen_3_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_antigen_4 = ifnull(upd.dd_amrantigen_4_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_antigen_5 = ifnull(upd.dd_amrantigen_5_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_bulk_1 = ifnull(upd.dd_amrbulk_1_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_bulk_2 = ifnull(upd.dd_amrbulk_2_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_bulk_3 = ifnull(upd.dd_amrbulk_3_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_fpu_1 = ifnull(upd.dd_amrfpu_1_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_fpu_2 = ifnull(upd.dd_amrfpu_2_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_fpu_3 = ifnull(upd.dd_amrfpu_3_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_fpu_4 = ifnull(upd.dd_amrfpu_4_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_fpu_5 = ifnull(upd.dd_amrfpu_5_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_fpp_1 = ifnull(upd.dd_amrfpp_1_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_fpp_2 = ifnull(upd.dd_amrfpp_2_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatussamr_fpp_3 = ifnull(upd.dd_amrfpp_3_dim_dateid, 0) 
                                  ; 
 /*Date User Status QCCO was last set*/ 
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid 
  , dd_ccoraw_1.dim_dateid dd_ccoraw_1_dim_dateid  , dd_ccoantigen_1.dim_dateid dd_ccoantigen_1_dim_dateid  , dd_ccoantigen_2.dim_dateid dd_ccoantigen_2_dim_dateid  , dd_ccoantigen_3.dim_dateid dd_ccoantigen_3_dim_dateid  , dd_ccoantigen_4.dim_dateid dd_ccoantigen_4_dim_dateid  , dd_ccoantigen_5.dim_dateid dd_ccoantigen_5_dim_dateid  , dd_ccobulk_1.dim_dateid dd_ccobulk_1_dim_dateid  , dd_ccobulk_2.dim_dateid dd_ccobulk_2_dim_dateid  , dd_ccobulk_3.dim_dateid dd_ccobulk_3_dim_dateid  , dd_ccofpu_1.dim_dateid dd_ccofpu_1_dim_dateid  , dd_ccofpu_2.dim_dateid dd_ccofpu_2_dim_dateid  , dd_ccofpu_3.dim_dateid dd_ccofpu_3_dim_dateid  , dd_ccofpu_4.dim_dateid dd_ccofpu_4_dim_dateid  , dd_ccofpu_5.dim_dateid dd_ccofpu_5_dim_dateid  , dd_ccofpp_1.dim_dateid dd_ccofpp_1_dim_dateid  , dd_ccofpp_2.dim_dateid dd_ccofpp_2_dim_dateid  , dd_ccofpp_3.dim_dateid dd_ccofpp_3_dim_dateid  from fact_scm_test fmm 
     inner join dim_plant pl on pl.dim_plantid = fmm.dim_plantid 
     left join dim_date dd_ccoraw_1 on pl.companycode = dd_ccoraw_1.companycode  
                             and fmm.dd_dateuserstatusqcco_raw_1 = dd_ccoraw_1.datevalue 
                             and pl.plantcode = dd_ccoraw_1.plantcode_factory 
                             left join dim_date dd_ccoantigen_1 on pl.companycode = dd_ccoantigen_1.companycode  
                             and fmm.dd_dateuserstatusqcco_antigen_1 = dd_ccoantigen_1.datevalue 
                             and pl.plantcode = dd_ccoantigen_1.plantcode_factory 
                             left join dim_date dd_ccoantigen_2 on pl.companycode = dd_ccoantigen_2.companycode  
                             and fmm.dd_dateuserstatusqcco_antigen_2 = dd_ccoantigen_2.datevalue 
                             and pl.plantcode = dd_ccoantigen_2.plantcode_factory 
                             left join dim_date dd_ccoantigen_3 on pl.companycode = dd_ccoantigen_3.companycode  
                             and fmm.dd_dateuserstatusqcco_antigen_3 = dd_ccoantigen_3.datevalue 
                             and pl.plantcode = dd_ccoantigen_3.plantcode_factory 
                             left join dim_date dd_ccoantigen_4 on pl.companycode = dd_ccoantigen_4.companycode  
                             and fmm.dd_dateuserstatusqcco_antigen_4 = dd_ccoantigen_4.datevalue 
                             and pl.plantcode = dd_ccoantigen_4.plantcode_factory 
                             left join dim_date dd_ccoantigen_5 on pl.companycode = dd_ccoantigen_5.companycode  
                             and fmm.dd_dateuserstatusqcco_antigen_5 = dd_ccoantigen_5.datevalue 
                             and pl.plantcode = dd_ccoantigen_5.plantcode_factory 
                             left join dim_date dd_ccobulk_1 on pl.companycode = dd_ccobulk_1.companycode  
                             and fmm.dd_dateuserstatusqcco_bulk_1 = dd_ccobulk_1.datevalue 
                             and pl.plantcode = dd_ccobulk_1.plantcode_factory 
                             left join dim_date dd_ccobulk_2 on pl.companycode = dd_ccobulk_2.companycode  
                             and fmm.dd_dateuserstatusqcco_bulk_2 = dd_ccobulk_2.datevalue 
                             and pl.plantcode = dd_ccobulk_2.plantcode_factory 
                             left join dim_date dd_ccobulk_3 on pl.companycode = dd_ccobulk_3.companycode  
                             and fmm.dd_dateuserstatusqcco_bulk_3 = dd_ccobulk_3.datevalue 
                             and pl.plantcode = dd_ccobulk_3.plantcode_factory 
                             left join dim_date dd_ccofpu_1 on pl.companycode = dd_ccofpu_1.companycode  
                             and fmm.dd_dateuserstatusqcco_fpu_1 = dd_ccofpu_1.datevalue 
                             and pl.plantcode = dd_ccofpu_1.plantcode_factory 
                             left join dim_date dd_ccofpu_2 on pl.companycode = dd_ccofpu_2.companycode  
                             and fmm.dd_dateuserstatusqcco_fpu_2 = dd_ccofpu_2.datevalue 
                             and pl.plantcode = dd_ccofpu_2.plantcode_factory 
                             left join dim_date dd_ccofpu_3 on pl.companycode = dd_ccofpu_3.companycode  
                             and fmm.dd_dateuserstatusqcco_fpu_3 = dd_ccofpu_3.datevalue 
                             and pl.plantcode = dd_ccofpu_3.plantcode_factory 
                             left join dim_date dd_ccofpu_4 on pl.companycode = dd_ccofpu_4.companycode  
                             and fmm.dd_dateuserstatusqcco_fpu_4 = dd_ccofpu_4.datevalue 
                             and pl.plantcode = dd_ccofpu_4.plantcode_factory 
                             left join dim_date dd_ccofpu_5 on pl.companycode = dd_ccofpu_5.companycode  
                             and fmm.dd_dateuserstatusqcco_fpu_5 = dd_ccofpu_5.datevalue 
                             and pl.plantcode = dd_ccofpu_5.plantcode_factory 
                             left join dim_date dd_ccofpp_1 on pl.companycode = dd_ccofpp_1.companycode  
                             and fmm.dd_dateuserstatusqcco_fpp_1 = dd_ccofpp_1.datevalue 
                             and pl.plantcode = dd_ccofpp_1.plantcode_factory 
                             left join dim_date dd_ccofpp_2 on pl.companycode = dd_ccofpp_2.companycode  
                             and fmm.dd_dateuserstatusqcco_fpp_2 = dd_ccofpp_2.datevalue 
                             and pl.plantcode = dd_ccofpp_2.plantcode_factory 
                             left join dim_date dd_ccofpp_3 on pl.companycode = dd_ccofpp_3.companycode  
                             and fmm.dd_dateuserstatusqcco_fpp_3 = dd_ccofpp_3.datevalue 
                             and pl.plantcode = dd_ccofpp_3.plantcode_factory 
                               ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update set  fmm.dim_dateuserstatusqcco_raw_1 = ifnull( upd.dd_ccoraw_1_dim_dateid, 0) 
                                  ,fmm.dim_dateuserstatusqcco_antigen_1 = ifnull(upd.dd_ccoantigen_1_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_antigen_2 = ifnull(upd.dd_ccoantigen_2_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_antigen_3 = ifnull(upd.dd_ccoantigen_3_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_antigen_4 = ifnull(upd.dd_ccoantigen_4_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_antigen_5 = ifnull(upd.dd_ccoantigen_5_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_bulk_1 = ifnull(upd.dd_ccobulk_1_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_bulk_2 = ifnull(upd.dd_ccobulk_2_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_bulk_3 = ifnull(upd.dd_ccobulk_3_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_fpu_1 = ifnull(upd.dd_ccofpu_1_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_fpu_2 = ifnull(upd.dd_ccofpu_2_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_fpu_3 = ifnull(upd.dd_ccofpu_3_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_fpu_4 = ifnull(upd.dd_ccofpu_4_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_fpu_5 = ifnull(upd.dd_ccofpu_5_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_fpp_1 = ifnull(upd.dd_ccofpp_1_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_fpp_2 = ifnull(upd.dd_ccofpp_2_dim_dateid, 0) 
                                      ,fmm.dim_dateuserstatusqcco_fpp_3 = ifnull(upd.dd_ccofpp_3_dim_dateid, 0) 
                                      ; 
 /* Add fields from Min Max: Min released stock in days and Lot size in days - BI-4868    */ 
 
  merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid  ,raw_1.ct_minreleasedstockindays_merck raw_1_ct_minreleasedstockindays_merck 
                                  ,raw_1.ct_lotsizeindays_merck raw_1_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
								inner join fact_inventoryatlas raw_1 on fmm.dim_plantid_raw_1 = raw_1.dim_plantid and fmm.dim_partid_raw_1 = raw_1.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set  fmm.ct_minReleasedStockDays_raw_1 = ifnull(upd.raw_1_ct_minreleasedstockindays_merck, 0) ,
				      fmm.ct_lotsizedays_raw_1 = ifnull( upd.raw_1_ct_lotsizedays_merck, 0) ;
					  
					  
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid  ,antigen_1.ct_minreleasedstockindays_merck antigen_1_ct_minreleasedstockindays_merck 
                                  ,antigen_1.ct_lotsizeindays_merck antigen_1_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							    inner join fact_inventoryatlas antigen_1 on fmm.dim_plantid_antigen_1 = antigen_1.dim_plantid and fmm.dim_partid_antigen_1 = antigen_1.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set   fmm.ct_minReleasedStockDays_antigen_1 = ifnull( upd.antigen_1_ct_minreleasedstockindays_merck, 0),
                       fmm.ct_lotsizedays_antigen_1 = ifnull( upd.antigen_1_ct_lotsizedays_merck ,0) ;
					   
					   
					   
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid  ,antigen_2.ct_minreleasedstockindays_merck antigen_2_ct_minreleasedstockindays_merck 
                                  ,antigen_2.ct_lotsizeindays_merck antigen_2_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							      inner join fact_inventoryatlas antigen_2 on fmm.dim_plantid_antigen_2 = antigen_2.dim_plantid and fmm.dim_partid_antigen_2 = antigen_2.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set   fmm.ct_minReleasedStockDays_antigen_2 = ifnull( upd.antigen_2_ct_minreleasedstockindays_merck, 0),                                  
				       fmm.ct_lotsizedays_antigen_2 = ifnull( upd.antigen_2_ct_lotsizedays_merck ,0);
					   
					   
					   
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid  ,antigen_3.ct_minreleasedstockindays_merck antigen_3_ct_minreleasedstockindays_merck 
                                  ,antigen_3.ct_lotsizeindays_merck antigen_3_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							         inner join fact_inventoryatlas antigen_3 on fmm.dim_plantid_antigen_3 = antigen_3.dim_plantid and fmm.dim_partid_antigen_3 = antigen_3.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set  fmm.ct_minReleasedStockDays_antigen_3 = ifnull( upd.antigen_3_ct_minreleasedstockindays_merck, 0) ,
                      fmm.ct_lotsizedays_antigen_3 = ifnull( upd.antigen_3_ct_lotsizedays_merck ,0);
					  

					  
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid   ,antigen_4.ct_minreleasedstockindays_merck antigen_4_ct_minreleasedstockindays_merck 
                                  ,antigen_4.ct_lotsizeindays_merck antigen_4_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							       inner join fact_inventoryatlas antigen_4 on fmm.dim_plantid_antigen_4 = antigen_4.dim_plantid and fmm.dim_partid_antigen_4 = antigen_4.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set  fmm.ct_minReleasedStockDays_antigen_4 = ifnull( upd.antigen_4_ct_minreleasedstockindays_merck, 0),
                      fmm.ct_lotsizedays_antigen_4 = ifnull( upd.antigen_4_ct_lotsizedays_merck ,0) ;


					  
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid   ,antigen_5.ct_minreleasedstockindays_merck antigen_5_ct_minreleasedstockindays_merck 
                                  ,antigen_5.ct_lotsizeindays_merck antigen_5_ct_lotsizedays_merck  
								         from fact_scm_test fmm 
							    inner join fact_inventoryatlas antigen_5 on fmm.dim_plantid_antigen_5 = antigen_5.dim_plantid and fmm.dim_partid_antigen_5 = antigen_5.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set  fmm.ct_minReleasedStockDays_antigen_5 = ifnull( upd.antigen_5_ct_minreleasedstockindays_merck, 0),
                      fmm.ct_lotsizedays_antigen_5 = ifnull( upd.antigen_5_ct_lotsizedays_merck ,0);
					  
					  
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid    ,bulk_1.ct_minreleasedstockindays_merck bulk_1_ct_minreleasedstockindays_merck 
                                  ,bulk_1.ct_lotsizeindays_merck bulk_1_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							        inner join fact_inventoryatlas bulk_1 on fmm.dim_plantid_bulk_1 = bulk_1.dim_plantid and fmm.dim_partid_bulk_1 = bulk_1.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set  fmm.ct_minReleasedStockDays_bulk_1 = ifnull( upd.bulk_1_ct_minreleasedstockindays_merck, 0),
                      fmm.ct_lotsizedays_bulk_1 = ifnull( upd.bulk_1_ct_lotsizedays_merck ,0);
					  
					  
					  
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid    ,bulk_2.ct_minreleasedstockindays_merck bulk_2_ct_minreleasedstockindays_merck 
                                  ,bulk_2.ct_lotsizeindays_merck bulk_2_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							       inner join fact_inventoryatlas bulk_2 on fmm.dim_plantid_bulk_2 = bulk_2.dim_plantid and fmm.dim_partid_bulk_2 = bulk_2.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set   fmm.ct_minReleasedStockDays_bulk_2 = ifnull( upd.bulk_2_ct_minreleasedstockindays_merck, 0), 
                       fmm.ct_lotsizedays_bulk_2 = ifnull( upd.bulk_2_ct_lotsizedays_merck ,0);
					   
					   
					   
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid    ,bulk_3.ct_minreleasedstockindays_merck bulk_3_ct_minreleasedstockindays_merck 
                                  ,bulk_3.ct_lotsizeindays_merck bulk_3_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							       inner join fact_inventoryatlas bulk_3 on fmm.dim_plantid_bulk_3 = bulk_3.dim_plantid and fmm.dim_partid_bulk_3 = bulk_3.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set   fmm.ct_minReleasedStockDays_bulk_3 = ifnull( upd.bulk_3_ct_minreleasedstockindays_merck, 0), 
                       fmm.ct_lotsizedays_bulk_3 = ifnull( upd.bulk_3_ct_lotsizedays_merck ,0);
					   
					   
merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid     ,fpu_1.ct_minreleasedstockindays_merck fpu_1_ct_minreleasedstockindays_merck 
                                  ,fpu_1.ct_lotsizeindays_merck fpu_1_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							           inner join fact_inventoryatlas fpu_1 on fmm.dim_plantid_fpu_1 = fpu_1.dim_plantid and fmm.dim_partid_fpu_1 = fpu_1.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set   fmm.ct_minReleasedStockDays_fpu_1 = ifnull( upd.fpu_1_ct_minreleasedstockindays_merck, 0) ,
                       fmm.ct_lotsizedays_fpu_1 = ifnull( upd.fpu_1_ct_lotsizedays_merck ,0) ;
					   
					   
					 
merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid     ,fpu_2.ct_minreleasedstockindays_merck fpu_2_ct_minreleasedstockindays_merck 
                                  ,fpu_2.ct_lotsizeindays_merck fpu_2_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
							           inner join fact_inventoryatlas fpu_2 on fmm.dim_plantid_fpu_2 = fpu_2.dim_plantid and fmm.dim_partid_fpu_2 = fpu_2.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set    fmm.ct_minReleasedStockDays_fpu_2 = ifnull( upd.fpu_2_ct_minreleasedstockindays_merck, 0) ,
                        fmm.ct_lotsizedays_fpu_2 = ifnull( upd.fpu_2_ct_lotsizedays_merck ,0) ;
						
						
merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid     ,fpu_3.ct_minreleasedstockindays_merck fpu_3_ct_minreleasedstockindays_merck 
                                  ,fpu_3.ct_lotsizeindays_merck fpu_3_ct_lotsizedays_merck  
								         from fact_scm_test fmm 
							     inner join fact_inventoryatlas fpu_3 on fmm.dim_plantid_fpu_3 = fpu_3.dim_plantid and fmm.dim_partid_fpu_3 = fpu_3.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set    fmm.ct_minReleasedStockDays_fpu_3 = ifnull( upd.fpu_3_ct_minreleasedstockindays_merck, 0), 
                        fmm.ct_lotsizedays_fpu_3 = ifnull( upd.fpu_3_ct_lotsizedays_merck ,0) ;
						
						
						
merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid     ,fpu_4.ct_minreleasedstockindays_merck fpu_4_ct_minreleasedstockindays_merck 
                                  ,fpu_4.ct_lotsizeindays_merck fpu_4_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
						  inner join fact_inventoryatlas fpu_4 on fmm.dim_plantid_fpu_4 = fpu_4.dim_plantid and fmm.dim_partid_fpu_4 = fpu_4.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set   fmm.ct_minReleasedStockDays_fpu_4 = ifnull( upd.fpu_4_ct_minreleasedstockindays_merck, 0),
                       fmm.ct_lotsizedays_fpu_4 = ifnull( upd.fpu_4_ct_lotsizedays_merck ,0) ;
					   
					   
merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid    ,fpu_5.ct_minreleasedstockindays_merck fpu_5_ct_minreleasedstockindays_merck 
                                  ,fpu_5.ct_lotsizeindays_merck fpu_5_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
						   inner join fact_inventoryatlas fpu_5 on fmm.dim_plantid_fpu_5 = fpu_5.dim_plantid and fmm.dim_partid_fpu_5 = fpu_5.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set   fmm.ct_minReleasedStockDays_fpu_5 = ifnull( upd.fpu_5_ct_minreleasedstockindays_merck, 0),
                       fmm.ct_lotsizedays_fpu_5 = ifnull( upd.fpu_5_ct_lotsizedays_merck ,0) ;
					   
					   
					   
merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid    ,fpp_1.ct_minreleasedstockindays_merck fpp_1_ct_minreleasedstockindays_merck 
                                  ,fpp_1.ct_lotsizeindays_merck fpp_1_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
					  inner join fact_inventoryatlas fpp_1 on fmm.dim_plantid_fpp_1 = fpp_1.dim_plantid and fmm.dim_partid_fpp_1 = fpp_1.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set  fmm.ct_minReleasedStockDays_fpp_1 = ifnull( upd.fpp_1_ct_minreleasedstockindays_merck, 0) ,
                      fmm.ct_lotsizedays_fpp_1 = ifnull( upd.fpp_1_ct_lotsizedays_merck ,0) ;
					  
					  
merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid    ,fpp_2.ct_minreleasedstockindays_merck fpp_2_ct_minreleasedstockindays_merck 
                                  ,fpp_2.ct_lotsizeindays_merck fpp_2_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
					   inner join fact_inventoryatlas fpp_2 on fmm.dim_plantid_fpp_2 = fpp_2.dim_plantid and fmm.dim_partid_fpp_2 = fpp_2.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set fmm.ct_minReleasedStockDays_fpp_2 = ifnull( upd.fpp_2_ct_minreleasedstockindays_merck, 0), 
                     fmm.ct_lotsizedays_fpp_2 = ifnull( upd.fpp_2_ct_lotsizedays_merck ,0) ;
					 
					 
					 
merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid          ,fpp_3.ct_minreleasedstockindays_merck fpp_3_ct_minreleasedstockindays_merck 
                                  ,fpp_3.ct_lotsizeindays_merck fpp_3_ct_lotsizedays_merck 
								         from fact_scm_test fmm 
					   inner join fact_inventoryatlas fpp_3 on fmm.dim_plantid_fpp_3 = fpp_3.dim_plantid and fmm.dim_partid_fpp_3 = fpp_3.dim_partid 
								 ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set fmm.ct_minReleasedStockDays_fpp_3 = ifnull( upd.fpp_3_ct_minreleasedstockindays_merck, 0) ,
                     fmm.ct_lotsizedays_fpp_3 = ifnull( upd.fpp_3_ct_lotsizedays_merck ,0);
 
 /*
 merge into fact_scm_test fmm using 
              (select distinct fmm.fact_mmprodhierarchyid  ,raw_1.ct_minreleasedstockindays_merck raw_1_ct_minreleasedstockindays_merck 
                                  ,raw_1.ct_lotsizeindays_merck raw_1_ct_lotsizedays_merck 
                              ,antigen_1.ct_minreleasedstockindays_merck antigen_1_ct_minreleasedstockindays_merck 
                                  ,antigen_1.ct_lotsizeindays_merck antigen_1_ct_lotsizedays_merck 
                              ,antigen_2.ct_minreleasedstockindays_merck antigen_2_ct_minreleasedstockindays_merck 
                                  ,antigen_2.ct_lotsizeindays_merck antigen_2_ct_lotsizedays_merck 
                              ,antigen_3.ct_minreleasedstockindays_merck antigen_3_ct_minreleasedstockindays_merck 
                                  ,antigen_3.ct_lotsizeindays_merck antigen_3_ct_lotsizedays_merck 
                              ,antigen_4.ct_minreleasedstockindays_merck antigen_4_ct_minreleasedstockindays_merck 
                                  ,antigen_4.ct_lotsizeindays_merck antigen_4_ct_lotsizedays_merck 
                              ,antigen_5.ct_minreleasedstockindays_merck antigen_5_ct_minreleasedstockindays_merck 
                                  ,antigen_5.ct_lotsizeindays_merck antigen_5_ct_lotsizedays_merck 
                              ,bulk_1.ct_minreleasedstockindays_merck bulk_1_ct_minreleasedstockindays_merck 
                                  ,bulk_1.ct_lotsizeindays_merck bulk_1_ct_lotsizedays_merck 
                              ,bulk_2.ct_minreleasedstockindays_merck bulk_2_ct_minreleasedstockindays_merck 
                                  ,bulk_2.ct_lotsizeindays_merck bulk_2_ct_lotsizedays_merck 
                              ,bulk_3.ct_minreleasedstockindays_merck bulk_3_ct_minreleasedstockindays_merck 
                                  ,bulk_3.ct_lotsizeindays_merck bulk_3_ct_lotsizedays_merck 
                              ,fpu_1.ct_minreleasedstockindays_merck fpu_1_ct_minreleasedstockindays_merck 
                                  ,fpu_1.ct_lotsizeindays_merck fpu_1_ct_lotsizedays_merck 
                              ,fpu_2.ct_minreleasedstockindays_merck fpu_2_ct_minreleasedstockindays_merck 
                                  ,fpu_2.ct_lotsizeindays_merck fpu_2_ct_lotsizedays_merck 
                              ,fpu_3.ct_minreleasedstockindays_merck fpu_3_ct_minreleasedstockindays_merck 
                                  ,fpu_3.ct_lotsizeindays_merck fpu_3_ct_lotsizedays_merck 
                              ,fpu_4.ct_minreleasedstockindays_merck fpu_4_ct_minreleasedstockindays_merck 
                                  ,fpu_4.ct_lotsizeindays_merck fpu_4_ct_lotsizedays_merck 
                              ,fpu_5.ct_minreleasedstockindays_merck fpu_5_ct_minreleasedstockindays_merck 
                                  ,fpu_5.ct_lotsizeindays_merck fpu_5_ct_lotsizedays_merck 
                              ,fpp_1.ct_minreleasedstockindays_merck fpp_1_ct_minreleasedstockindays_merck 
                                  ,fpp_1.ct_lotsizeindays_merck fpp_1_ct_lotsizedays_merck 
                              ,fpp_2.ct_minreleasedstockindays_merck fpp_2_ct_minreleasedstockindays_merck 
                                  ,fpp_2.ct_lotsizeindays_merck fpp_2_ct_lotsizedays_merck 
                              ,fpp_3.ct_minreleasedstockindays_merck fpp_3_ct_minreleasedstockindays_merck 
                                  ,fpp_3.ct_lotsizeindays_merck fpp_3_ct_lotsizedays_merck 
                              from fact_scm_test fmm 
              left join fact_inventoryatlas raw_1 on fmm.dim_plantid_raw_1 = raw_1.dim_plantid and fmm.dim_partid_raw_1 = raw_1.dim_partid 
                                  left join fact_inventoryatlas antigen_1 on fmm.dim_plantid_antigen_1 = antigen_1.dim_plantid and fmm.dim_partid_antigen_1 = antigen_1.dim_partid 
                                  left join fact_inventoryatlas antigen_2 on fmm.dim_plantid_antigen_2 = antigen_2.dim_plantid and fmm.dim_partid_antigen_2 = antigen_2.dim_partid 
                                  left join fact_inventoryatlas antigen_3 on fmm.dim_plantid_antigen_3 = antigen_3.dim_plantid and fmm.dim_partid_antigen_3 = antigen_3.dim_partid 
                                  left join fact_inventoryatlas antigen_4 on fmm.dim_plantid_antigen_4 = antigen_4.dim_plantid and fmm.dim_partid_antigen_4 = antigen_4.dim_partid 
                                  left join fact_inventoryatlas antigen_5 on fmm.dim_plantid_antigen_5 = antigen_5.dim_plantid and fmm.dim_partid_antigen_5 = antigen_5.dim_partid 
                                  left join fact_inventoryatlas bulk_1 on fmm.dim_plantid_bulk_1 = bulk_1.dim_plantid and fmm.dim_partid_bulk_1 = bulk_1.dim_partid 
                                  left join fact_inventoryatlas bulk_2 on fmm.dim_plantid_bulk_2 = bulk_2.dim_plantid and fmm.dim_partid_bulk_2 = bulk_2.dim_partid 
                                  left join fact_inventoryatlas bulk_3 on fmm.dim_plantid_bulk_3 = bulk_3.dim_plantid and fmm.dim_partid_bulk_3 = bulk_3.dim_partid 
                                  left join fact_inventoryatlas fpu_1 on fmm.dim_plantid_fpu_1 = fpu_1.dim_plantid and fmm.dim_partid_fpu_1 = fpu_1.dim_partid 
                                  left join fact_inventoryatlas fpu_2 on fmm.dim_plantid_fpu_2 = fpu_2.dim_plantid and fmm.dim_partid_fpu_2 = fpu_2.dim_partid 
                                  left join fact_inventoryatlas fpu_3 on fmm.dim_plantid_fpu_3 = fpu_3.dim_plantid and fmm.dim_partid_fpu_3 = fpu_3.dim_partid 
                                  left join fact_inventoryatlas fpu_4 on fmm.dim_plantid_fpu_4 = fpu_4.dim_plantid and fmm.dim_partid_fpu_4 = fpu_4.dim_partid 
                                  left join fact_inventoryatlas fpu_5 on fmm.dim_plantid_fpu_5 = fpu_5.dim_plantid and fmm.dim_partid_fpu_5 = fpu_5.dim_partid 
                                  left join fact_inventoryatlas fpp_1 on fmm.dim_plantid_fpp_1 = fpp_1.dim_plantid and fmm.dim_partid_fpp_1 = fpp_1.dim_partid 
                                  left join fact_inventoryatlas fpp_2 on fmm.dim_plantid_fpp_2 = fpp_2.dim_plantid and fmm.dim_partid_fpp_2 = fpp_2.dim_partid 
                                  left join fact_inventoryatlas fpp_3 on fmm.dim_plantid_fpp_3 = fpp_3.dim_plantid and fmm.dim_partid_fpp_3 = fpp_3.dim_partid 
                                   ) upd 
                 on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
                 when matched then update 
                 set  fmm.ct_minReleasedStockDays_raw_1 = ifnull(upd.raw_1_ct_minreleasedstockindays_merck, 0) 
                                  , fmm.ct_lotsizedays_raw_1 = ifnull( upd.raw_1_ct_lotsizedays_merck, 0) 
                                   , fmm.ct_minReleasedStockDays_antigen_1 = ifnull( upd.antigen_1_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_antigen_1 = ifnull( upd.antigen_1_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_antigen_2 = ifnull( upd.antigen_2_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_antigen_2 = ifnull( upd.antigen_2_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_antigen_3 = ifnull( upd.antigen_3_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_antigen_3 = ifnull( upd.antigen_3_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_antigen_4 = ifnull( upd.antigen_4_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_antigen_4 = ifnull( upd.antigen_4_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_antigen_5 = ifnull( upd.antigen_5_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_antigen_5 = ifnull( upd.antigen_5_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_bulk_1 = ifnull( upd.bulk_1_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_bulk_1 = ifnull( upd.bulk_1_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_bulk_2 = ifnull( upd.bulk_2_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_bulk_2 = ifnull( upd.bulk_2_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_bulk_3 = ifnull( upd.bulk_3_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_bulk_3 = ifnull( upd.bulk_3_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_fpu_1 = ifnull( upd.fpu_1_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_fpu_1 = ifnull( upd.fpu_1_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_fpu_2 = ifnull( upd.fpu_2_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_fpu_2 = ifnull( upd.fpu_2_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_fpu_3 = ifnull( upd.fpu_3_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_fpu_3 = ifnull( upd.fpu_3_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_fpu_4 = ifnull( upd.fpu_4_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_fpu_4 = ifnull( upd.fpu_4_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_fpu_5 = ifnull( upd.fpu_5_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_fpu_5 = ifnull( upd.fpu_5_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_fpp_1 = ifnull( upd.fpp_1_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_fpp_1 = ifnull( upd.fpp_1_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_fpp_2 = ifnull( upd.fpp_2_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_fpp_2 = ifnull( upd.fpp_2_ct_lotsizedays_merck ,0) 
                                 , fmm.ct_minReleasedStockDays_fpp_3 = ifnull( upd.fpp_3_ct_minreleasedstockindays_merck, 0) 
                                 , fmm.ct_lotsizedays_fpp_3 = ifnull( upd.fpp_3_ct_lotsizedays_merck ,0)   */
                                 
 /* Add fields Float before production (in days) - BI-4886 */ 
 merge into fact_scm_test fmm using 
            (select fmm.fact_mmprodhierarchyid ,raw_1.T436A_FHORI raw_1_T436A_FHORI 
,raw_1.T436A_WERKS raw_1_T436A_WERKS 
,raw_1.T436A_VORGZ raw_1_T436A_VORGZ 
,antigen_1.T436A_FHORI antigen_1_T436A_FHORI 
,antigen_1.T436A_WERKS antigen_1_T436A_WERKS 
,antigen_1.T436A_VORGZ antigen_1_T436A_VORGZ 
,antigen_2.T436A_FHORI antigen_2_T436A_FHORI 
,antigen_2.T436A_WERKS antigen_2_T436A_WERKS 
,antigen_2.T436A_VORGZ antigen_2_T436A_VORGZ 
,antigen_3.T436A_FHORI antigen_3_T436A_FHORI 
,antigen_3.T436A_WERKS antigen_3_T436A_WERKS 
,antigen_3.T436A_VORGZ antigen_3_T436A_VORGZ 
,antigen_4.T436A_FHORI antigen_4_T436A_FHORI 
,antigen_4.T436A_WERKS antigen_4_T436A_WERKS 
,antigen_4.T436A_VORGZ antigen_4_T436A_VORGZ 
,antigen_5.T436A_FHORI antigen_5_T436A_FHORI 
,antigen_5.T436A_WERKS antigen_5_T436A_WERKS 
,antigen_5.T436A_VORGZ antigen_5_T436A_VORGZ 
,bulk_1.T436A_FHORI bulk_1_T436A_FHORI 
,bulk_1.T436A_WERKS bulk_1_T436A_WERKS 
,bulk_1.T436A_VORGZ bulk_1_T436A_VORGZ 
,bulk_2.T436A_FHORI bulk_2_T436A_FHORI 
,bulk_2.T436A_WERKS bulk_2_T436A_WERKS 
,bulk_2.T436A_VORGZ bulk_2_T436A_VORGZ 
,bulk_3.T436A_FHORI bulk_3_T436A_FHORI 
,bulk_3.T436A_WERKS bulk_3_T436A_WERKS 
,bulk_3.T436A_VORGZ bulk_3_T436A_VORGZ 
,fpu_1.T436A_FHORI fpu_1_T436A_FHORI 
,fpu_1.T436A_WERKS fpu_1_T436A_WERKS 
,fpu_1.T436A_VORGZ fpu_1_T436A_VORGZ 
,fpu_2.T436A_FHORI fpu_2_T436A_FHORI 
,fpu_2.T436A_WERKS fpu_2_T436A_WERKS 
,fpu_2.T436A_VORGZ fpu_2_T436A_VORGZ 
,fpu_3.T436A_FHORI fpu_3_T436A_FHORI 
,fpu_3.T436A_WERKS fpu_3_T436A_WERKS 
,fpu_3.T436A_VORGZ fpu_3_T436A_VORGZ 
,fpu_4.T436A_FHORI fpu_4_T436A_FHORI 
,fpu_4.T436A_WERKS fpu_4_T436A_WERKS 
,fpu_4.T436A_VORGZ fpu_4_T436A_VORGZ 
,fpu_5.T436A_FHORI fpu_5_T436A_FHORI 
,fpu_5.T436A_WERKS fpu_5_T436A_WERKS 
,fpu_5.T436A_VORGZ fpu_5_T436A_VORGZ 
,fpp_1.T436A_FHORI fpp_1_T436A_FHORI 
,fpp_1.T436A_WERKS fpp_1_T436A_WERKS 
,fpp_1.T436A_VORGZ fpp_1_T436A_VORGZ 
,fpp_2.T436A_FHORI fpp_2_T436A_FHORI 
,fpp_2.T436A_WERKS fpp_2_T436A_WERKS 
,fpp_2.T436A_VORGZ fpp_2_T436A_VORGZ 
,fpp_3.T436A_FHORI fpp_3_T436A_FHORI 
,fpp_3.T436A_WERKS fpp_3_T436A_WERKS 
,fpp_3.T436A_VORGZ fpp_3_T436A_VORGZ 
,comops.T436A_FHORI comops_T436A_FHORI 
,comops.T436A_WERKS comops_T436A_WERKS 
,comops.T436A_VORGZ comops_T436A_VORGZ 
 from fact_scm_test fmm 
                  inner join dim_part dp_raw_1 on fmm.dim_partid_raw_1 = dp_raw_1.dim_partid 
                             left join T436A raw_1 on dp_raw_1.schedmarginkey = ifnull(raw_1.T436A_FHORI, 0) 
                                 and dp_raw_1.plant = ifnull(raw_1.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_antigen_1 on fmm.dim_partid_antigen_1 = dp_antigen_1.dim_partid 
                             left join T436A antigen_1 on dp_antigen_1.schedmarginkey = ifnull(antigen_1.T436A_FHORI, 0) 
                                 and dp_antigen_1.plant = ifnull(antigen_1.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_antigen_2 on fmm.dim_partid_antigen_2 = dp_antigen_2.dim_partid 
                             left join T436A antigen_2 on dp_antigen_2.schedmarginkey = ifnull(antigen_2.T436A_FHORI, 0) 
                                 and dp_antigen_2.plant = ifnull(antigen_2.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_antigen_3 on fmm.dim_partid_antigen_3 = dp_antigen_3.dim_partid 
                             left join T436A antigen_3 on dp_antigen_3.schedmarginkey = ifnull(antigen_3.T436A_FHORI, 0) 
                                 and dp_antigen_3.plant = ifnull(antigen_3.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_antigen_4 on fmm.dim_partid_antigen_4 = dp_antigen_4.dim_partid 
                             left join T436A antigen_4 on dp_antigen_4.schedmarginkey = ifnull(antigen_4.T436A_FHORI, 0) 
                                 and dp_antigen_4.plant = ifnull(antigen_4.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_antigen_5 on fmm.dim_partid_antigen_5 = dp_antigen_5.dim_partid 
                             left join T436A antigen_5 on dp_antigen_5.schedmarginkey = ifnull(antigen_5.T436A_FHORI, 0) 
                                 and dp_antigen_5.plant = ifnull(antigen_5.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_bulk_1 on fmm.dim_partid_bulk_1 = dp_bulk_1.dim_partid 
                             left join T436A bulk_1 on dp_bulk_1.schedmarginkey = ifnull(bulk_1.T436A_FHORI, 0) 
                                 and dp_bulk_1.plant = ifnull(bulk_1.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_bulk_2 on fmm.dim_partid_bulk_2 = dp_bulk_2.dim_partid 
                             left join T436A bulk_2 on dp_bulk_2.schedmarginkey = ifnull(bulk_2.T436A_FHORI, 0) 
                                 and dp_bulk_2.plant = ifnull(bulk_2.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_bulk_3 on fmm.dim_partid_bulk_3 = dp_bulk_3.dim_partid 
                             left join T436A bulk_3 on dp_bulk_3.schedmarginkey = ifnull(bulk_3.T436A_FHORI, 0) 
                                 and dp_bulk_3.plant = ifnull(bulk_3.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_fpu_1 on fmm.dim_partid_fpu_1 = dp_fpu_1.dim_partid 
                             left join T436A fpu_1 on dp_fpu_1.schedmarginkey = ifnull(fpu_1.T436A_FHORI, 0) 
                                 and dp_fpu_1.plant = ifnull(fpu_1.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_fpu_2 on fmm.dim_partid_fpu_2 = dp_fpu_2.dim_partid 
                             left join T436A fpu_2 on dp_fpu_2.schedmarginkey = ifnull(fpu_2.T436A_FHORI, 0) 
                                 and dp_fpu_2.plant = ifnull(fpu_2.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_fpu_3 on fmm.dim_partid_fpu_3 = dp_fpu_3.dim_partid 
                             left join T436A fpu_3 on dp_fpu_3.schedmarginkey = ifnull(fpu_3.T436A_FHORI, 0) 
                                 and dp_fpu_3.plant = ifnull(fpu_3.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_fpu_4 on fmm.dim_partid_fpu_4 = dp_fpu_4.dim_partid 
                             left join T436A fpu_4 on dp_fpu_4.schedmarginkey = ifnull(fpu_4.T436A_FHORI, 0) 
                                 and dp_fpu_4.plant = ifnull(fpu_4.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_fpu_5 on fmm.dim_partid_fpu_5 = dp_fpu_5.dim_partid 
                             left join T436A fpu_5 on dp_fpu_5.schedmarginkey = ifnull(fpu_5.T436A_FHORI, 0) 
                                 and dp_fpu_5.plant = ifnull(fpu_5.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_fpp_1 on fmm.dim_partid_fpp_1 = dp_fpp_1.dim_partid 
                             left join T436A fpp_1 on dp_fpp_1.schedmarginkey = ifnull(fpp_1.T436A_FHORI, 0) 
                                 and dp_fpp_1.plant = ifnull(fpp_1.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_fpp_2 on fmm.dim_partid_fpp_2 = dp_fpp_2.dim_partid 
                             left join T436A fpp_2 on dp_fpp_2.schedmarginkey = ifnull(fpp_2.T436A_FHORI, 0) 
                                 and dp_fpp_2.plant = ifnull(fpp_2.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_fpp_3 on fmm.dim_partid_fpp_3 = dp_fpp_3.dim_partid 
                             left join T436A fpp_3 on dp_fpp_3.schedmarginkey = ifnull(fpp_3.T436A_FHORI, 0) 
                                 and dp_fpp_3.plant = ifnull(fpp_3.T436A_WERKS, 'Not Set') 
                              inner join dim_part dp_comops on fmm.dim_partid_comops = dp_comops.dim_partid 
                             left join T436A comops on dp_comops.schedmarginkey = ifnull(comops.T436A_FHORI, 0) 
                                 and dp_comops.plant = ifnull(comops.T436A_WERKS, 'Not Set') 
                              ) upd 
             on fmm.fact_mmprodhierarchyid = upd.fact_mmprodhierarchyid 
             when matched then update 
             set ct_floatBeforeProduction_raw_1 = 
                         case when (upd.raw_1_T436A_FHORI is null and upd.raw_1_T436A_WERKS is null) then ct_floatBeforeProduction_raw_1 
                             else ifnull(upd.raw_1_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_antigen_1 = 
                         case when (upd.antigen_1_T436A_FHORI is null and upd.antigen_1_T436A_WERKS is null) then ct_floatBeforeProduction_antigen_1 
                             else ifnull(upd.antigen_1_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_antigen_2 = 
                         case when (upd.antigen_2_T436A_FHORI is null and upd.antigen_2_T436A_WERKS is null) then ct_floatBeforeProduction_antigen_2 
                             else ifnull(upd.antigen_2_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_antigen_3 = 
                         case when (upd.antigen_3_T436A_FHORI is null and upd.antigen_3_T436A_WERKS is null) then ct_floatBeforeProduction_antigen_3 
                             else ifnull(upd.antigen_3_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_antigen_4 = 
                         case when (upd.antigen_4_T436A_FHORI is null and upd.antigen_4_T436A_WERKS is null) then ct_floatBeforeProduction_antigen_4 
                             else ifnull(upd.antigen_4_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_antigen_5 = 
                         case when (upd.antigen_5_T436A_FHORI is null and upd.antigen_5_T436A_WERKS is null) then ct_floatBeforeProduction_antigen_5 
                             else ifnull(upd.antigen_5_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_bulk_1 = 
                         case when (upd.bulk_1_T436A_FHORI is null and upd.bulk_1_T436A_WERKS is null) then ct_floatBeforeProduction_bulk_1 
                             else ifnull(upd.bulk_1_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_bulk_2 = 
                         case when (upd.bulk_2_T436A_FHORI is null and upd.bulk_2_T436A_WERKS is null) then ct_floatBeforeProduction_bulk_2 
                             else ifnull(upd.bulk_2_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_bulk_3 = 
                         case when (upd.bulk_3_T436A_FHORI is null and upd.bulk_3_T436A_WERKS is null) then ct_floatBeforeProduction_bulk_3 
                             else ifnull(upd.bulk_3_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_fpu_1 = 
                         case when (upd.fpu_1_T436A_FHORI is null and upd.fpu_1_T436A_WERKS is null) then ct_floatBeforeProduction_fpu_1 
                             else ifnull(upd.fpu_1_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_fpu_2 = 
                         case when (upd.fpu_2_T436A_FHORI is null and upd.fpu_2_T436A_WERKS is null) then ct_floatBeforeProduction_fpu_2 
                             else ifnull(upd.fpu_2_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_fpu_3 = 
                         case when (upd.fpu_3_T436A_FHORI is null and upd.fpu_3_T436A_WERKS is null) then ct_floatBeforeProduction_fpu_3 
                             else ifnull(upd.fpu_3_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_fpu_4 = 
                         case when (upd.fpu_4_T436A_FHORI is null and upd.fpu_4_T436A_WERKS is null) then ct_floatBeforeProduction_fpu_4 
                             else ifnull(upd.fpu_4_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_fpu_5 = 
                         case when (upd.fpu_5_T436A_FHORI is null and upd.fpu_5_T436A_WERKS is null) then ct_floatBeforeProduction_fpu_5 
                             else ifnull(upd.fpu_5_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_fpp_1 = 
                         case when (upd.fpp_1_T436A_FHORI is null and upd.fpp_1_T436A_WERKS is null) then ct_floatBeforeProduction_fpp_1 
                             else ifnull(upd.fpp_1_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_fpp_2 = 
                         case when (upd.fpp_2_T436A_FHORI is null and upd.fpp_2_T436A_WERKS is null) then ct_floatBeforeProduction_fpp_2 
                             else ifnull(upd.fpp_2_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_fpp_3 = 
                         case when (upd.fpp_3_T436A_FHORI is null and upd.fpp_3_T436A_WERKS is null) then ct_floatBeforeProduction_fpp_3 
                             else ifnull(upd.fpp_3_T436A_VORGZ, 0) end , fmm.ct_floatBeforeProduction_comops = 
                         case when (upd.comops_T436A_FHORI is null and upd.comops_T436A_WERKS is null) then ct_floatBeforeProduction_comops 
                             else ifnull(upd.comops_T436A_VORGZ, 0) end ;