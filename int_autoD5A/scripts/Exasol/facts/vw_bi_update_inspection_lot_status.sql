DROP TABLE IF EXISTS tmp_dim_inspectionlotstatus;
CREATE TABLE  tmp_dim_inspectionlotstatus AS 
SELECT distinct j1.JEST_OBJNR AS dd_ObjectNumber,
       convert(varchar(7),'Not Set') AS ApprovalGranted,
       convert(varchar(7),'Not Set') AS ApprovalNotGranted,
       convert(varchar(7),'Not Set') AS Created,
       convert(varchar(7),'Not Set') AS LotCancelled,
       convert(varchar(7),'Not Set') as LotDetailDataDeleted,
       convert(varchar(7),'Not Set') as LotSampleDataDeleted,
       convert(varchar(7),'Not Set') as Released,
       convert(varchar(7),'Not Set') as SkippedLot,
       convert(varchar(7),'Not Set') as UsageDecisionMade,
	   convert(varchar(7),'Not Set') as InspectionComplete FROM JEST_QALS j1;

UPDATE   tmp_dim_inspectionlotstatus
SET ApprovalGranted = 'X'
 FROM JEST_QALS j, tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0322' 
AND j.JEST_INACT is NULL; 

UPDATE   tmp_dim_inspectionlotstatus
SET ApprovalNotGranted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0325' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET Created = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0001' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotCancelled = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0224' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotDetailDataDeleted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0227' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET LotSampleDataDeleted = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0228' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET Released = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0002' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET SkippedLot = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0209' 
AND j.JEST_INACT is NULL;

UPDATE   tmp_dim_inspectionlotstatus
SET UsageDecisionMade = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0218' 
AND j.JEST_INACT is NULL;
     
/* Suchithra - 17Mar2015 - Update for the new field InspectionComplete */
UPDATE   tmp_dim_inspectionlotstatus
SET InspectionComplete = 'X'
 FROM JEST_QALS j,tmp_dim_inspectionlotstatus
WHERE j.JEST_OBJNR = dd_ObjectNumber
AND j.JEST_STAT = 'I0217' 
AND j.JEST_INACT is NULL; 
/* End of Update - 17Mar2015 - Suchithra */
   
/* Suchithra - 17Mar2015 - Additional Clause added for the new field InspectionComplete */     
UPDATE fact_inspectionlot fil
   SET fil.dim_inspectionlotstatusid = ils.Dim_InspectionLotStatusId,
       fil.dw_update_date = current_timestamp
	FROM
       dim_inspectionlotstatus ils,
       tmp_dim_inspectionlotstatus j1,
       fact_inspectionlot fil
 WHERE ils.ApprovalGranted = j1.ApprovalGranted
       AND ils.ApprovalNotGranted = j1.ApprovalNotGranted 
       AND ils.Created = j1.Created 
       AND ils.LotCancelled = j1.LotCancelled
       AND ils.LotDetailDataDeleted = j1.LotDetailDataDeleted
       AND ils.LotSampleDataDeleted = j1.LotSampleDataDeleted
       AND ils.Released = j1.Released 
       AND ils.SkippedLot = j1.SkippedLot
       AND ils.UsageDecisionMade = j1.UsageDecisionMade
       AND ils.InspectionComplete = j1.InspectionComplete 
       AND j1.dd_ObjectNumber = fil.dd_ObjectNumber;
DROP TABLE IF EXISTS tmp_dim_inspectionlotstatus;