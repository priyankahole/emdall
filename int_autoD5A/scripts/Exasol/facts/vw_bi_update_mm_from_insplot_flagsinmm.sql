truncate table fact_materialmovement_forLots_tmp;

UPDATE fact_materialmovement
SET LotsAcceptedFlag = 'N'
WHERE LotsAcceptedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsAwaitingInspectionFlag = 'N'
WHERE LotsAwaitingInspectionFlag = 'Y';

UPDATE fact_materialmovement
SET LotsInspectedFlag = 'N'
WHERE LotsInspectedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsSkippedFlag = 'N'
WHERE LotsSkippedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsAcceptedFlag = 'N'
WHERE PercentLotsAcceptedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsInspectedFlag = 'N'
WHERE PercentLotsInspectedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsRejectedFlag = 'N'
WHERE PercentLotsRejectedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsReceivedFlag = 'N'
WHERE LotsReceivedFlag = 'Y';

INSERT INTO fact_materialmovement_forLots_tmp(fact_materialmovementid,
                                              dd_MaterialDocNo,
                                              dd_MaterialDocItemNo,
                                              dd_MaterialDocYear,
                                              dim_movementtypeid,
                                              Rank1,
                                              LotsAcceptedFlag,
                                              LotsAwaitingInspectionFlag,
                                              LotsInspectedFlag,
                                              LotsRejectedFlag,
                                              LotsSkippedFlag,
                                              PercentLotsAcceptedFlag,
                                              PercentLotsInspectedFlag,
                                              PercentLotsRejectedFlag,LotsReceivedFlag)
   SELECT fact_materialmovementid,
          dd_MaterialDocNo,
          dd_MaterialDocItemNo,
          dd_MaterialDocYear,
          mm.dim_movementtypeid,
          RANK() OVER (PARTITION BY dd_MaterialDocNo, dd_MaterialDocItemNo, dd_MaterialDocYear,mm.dim_movementtypeid ORDER BY fact_materialmovementid ASC) as Rank1,
'N','N','N','N','N','N','N','N','N'
from fact_materialmovement mm, dim_movementtype mt
where mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = mm.dim_movementtypeid;


DELETE FROM fact_materialmovement_forLots_tmp
 WHERE Rank1 <> 1;


UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET f_mm.LotsAcceptedFlag = ifnull(subs.LotsAcceptedFlag, 'N')
        FROM
          dim_movementtype mt 
  inner join fact_materialmovement_forLots_tmp f_mm
  on mt.dim_movementtypeid = f_mm.dim_movementtypeid
 left join (select 'Y' as LotsAcceptedFlag,il.dd_MaterialDocNo,il.dd_MaterialDocItemNo,il.dd_MaterialDocYear FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils,
                              dim_CodeText ud
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X' AND ud.dim_codetextid =
                                     il.dim_inspusagedecisionid
                              AND ud.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACC',
                                      'ACCI',
                                      'ACUS',
                                      'ACCP',
                                      'ACCN','A50','AX')) subs
on subs.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo and  subs.dd_MaterialDocNo = f_mm.dd_MaterialDocNo and subs.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
WHERE mt.movementtype IN ('101');

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        f_mm.LotsAwaitingInspectionFlag = ifnull(subs.LotsAwaitingInspectionFlag, 'N')
   FROM
          dim_movementtype mt 
  inner join fact_materialmovement_forLots_tmp f_mm
  on mt.dim_movementtypeid = f_mm.dim_movementtypeid
 left join (select 'Y' as LotsAwaitingInspectionFlag,il.dd_MaterialDocNo,il.dd_MaterialDocItemNo,il.dd_MaterialDocYear FROM fact_inspectionlot il,
                              dim_inspectionlotmisc ilm,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X' AND il.dim_inspectionlotmiscid =
                                     ilm.dim_inspectionlotmiscid
                              AND ilm.LotSkipped <> 'X' AND il.dim_inspusagedecisionid = 1) subs
on subs.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo and  subs.dd_MaterialDocNo = f_mm.dd_MaterialDocNo and subs.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
WHERE mt.movementtype IN ('101');

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        f_mm.LotsInspectedFlag = ifnull(subs.LotsInspectedFlag, 'N')
   FROM
          dim_movementtype mt 
  inner join fact_materialmovement_forLots_tmp f_mm
  on mt.dim_movementtypeid = f_mm.dim_movementtypeid
left join (SELECT 'Y' as LotsInspectedFlag,il.dd_MaterialDocNo,il.dd_MaterialDocItemNo,il.dd_MaterialDocYear FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X'
                              AND il.dim_inspusagedecisionid <> 1) subs
on subs.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo and  subs.dd_MaterialDocNo = f_mm.dd_MaterialDocNo and subs.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
WHERE mt.movementtype IN ('101');

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        f_mm.LotsRejectedFlag = ifnull(subs.LotsRejectedFlag, 'N')
 FROM
          dim_movementtype mt 
  inner join fact_materialmovement_forLots_tmp f_mm
  on mt.dim_movementtypeid = f_mm.dim_movementtypeid
  left join (SELECT 'Y' as LotsRejectedFlag,il.dd_MaterialDocNo,il.dd_MaterialDocItemNo,il.dd_MaterialDocYear
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid <> 1
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND udc.Code NOT IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCP',
                                      'ACCI',
                                      'ACCN',
                                      'ACUS',
                                      'ACC','A50','AX')) subs
 on subs.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo and  subs.dd_MaterialDocNo = f_mm.dd_MaterialDocNo and subs.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
WHERE mt.movementtype IN ('101');

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        LotsSkippedFlag = ifnull(subs.LotsSkippedFlag, 'N')
 FROM
          dim_movementtype mt 
  inner join fact_materialmovement_forLots_tmp f_mm
  on mt.dim_movementtypeid = f_mm.dim_movementtypeid
  left join (SELECT 'Y'  LotsSkippedFlag,il.dd_MaterialDocNo,il.dd_MaterialDocItemNo,il.dd_MaterialDocYear
                         FROM fact_inspectionlot il,
                              dim_inspectionlotmisc ilm,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspectionlotmiscid =
                                     ilm.dim_inspectionlotmiscid
                              AND ilm.LotSkipped = 'X') subs
   on subs.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo and  subs.dd_MaterialDocNo = f_mm.dd_MaterialDocNo and subs.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
WHERE mt.movementtype IN ('101');


UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET PercentLotsAcceptedFlag = ifnull(subs.PercentLotsAcceptedFlag, 'N')
FROM
          dim_movementtype mt 
  inner join fact_materialmovement_forLots_tmp f_mm
  on mt.dim_movementtypeid = f_mm.dim_movementtypeid
  left join (SELECT 'Y' as PercentLotsAcceptedFlag,il.dd_MaterialDocNo,il.dd_MaterialDocItemNo,il.dd_MaterialDocYear
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND udc.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCP',
                                      'ACCI',
                                      'ACCN',
                                      'ACUS',
                                      'ACC','A50','AX')) subs
   on subs.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo and  subs.dd_MaterialDocNo = f_mm.dd_MaterialDocNo and subs.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
WHERE mt.movementtype IN ('101');

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        PercentLotsInspectedFlag = ifnull(subs.PercentLotsInspectedFlag, 'N')
FROM dim_movementtype mt 
  inner join fact_materialmovement_forLots_tmp f_mm
  on mt.dim_movementtypeid = f_mm.dim_movementtypeid
  left join (SELECT 'Y' as PercentLotsInspectedFlag,il.dd_MaterialDocNo,il.dd_MaterialDocItemNo,il.dd_MaterialDocYear
                         FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils,
                              dim_codetext ct
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid =
                                     ct.dim_Codetextid
                              AND ct.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCI',
                                      'ACCP',
                                      'QUAR',
                                      'QURP',
                                      'REJO',
                                      'REJR',
                                      'REJS',
                                      'REJV','A50','AX','R','R1','R2','R3','R4','R40','RQ','X')) subs
 on subs.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo and  subs.dd_MaterialDocNo = f_mm.dd_MaterialDocNo and subs.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
WHERE mt.movementtype IN ('101');


UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        PercentLotsRejectedFlag = ifnull(subs.PercentLotsRejectedFlag, 'N')
 FROM dim_movementtype mt 
  inner join fact_materialmovement_forLots_tmp f_mm
  on mt.dim_movementtypeid = f_mm.dim_movementtypeid
  left join (SELECT 'Y' as PercentLotsRejectedFlag,il.dd_MaterialDocNo,il.dd_MaterialDocItemNo,il.dd_MaterialDocYear
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND il.dim_inspusagedecisionid <> 1
                              AND udc.Code IN
                                     ('QUAR',
                                      'QURP',
                                      'REJO',
                                      'REJR',
                                      'REJS',
                                      'REJV','R','R1','R2','R3','R4','R40','RQ','X')) subs
 on subs.dd_MaterialDocItemNo = f_mm.dd_MaterialDocItemNo and  subs.dd_MaterialDocNo = f_mm.dd_MaterialDocNo and subs.dd_MaterialDocYear = f_mm.dd_MaterialDocYear
 WHERE mt.movementtype IN ('101');

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        LotsReceivedFlag = 'Y' 
        FROM
          dim_movementtype mt, fact_materialmovement_forLots_tmp f_mm
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;



UPDATE fact_materialmovement f_mm
   SET f_mm.LotsAcceptedFlag = t.LotsAcceptedFlag,
       f_mm.LotsAwaitingInspectionFlag = t.LotsAwaitingInspectionFlag,
       f_mm.LotsInspectedFlag = t.LotsInspectedFlag,
       f_mm.LotsRejectedFlag = t.LotsRejectedFlag,
       f_mm.LotsSkippedFlag = t.LotsSkippedFlag,
       f_mm.PercentLotsAcceptedFlag = t.PercentLotsAcceptedFlag,
       f_mm.PercentLotsInspectedFlag = t.PercentLotsInspectedFlag,
       f_mm.PercentLotsRejectedFlag = t.PercentLotsRejectedFlag,
       f_mm.LotsReceivedflag = t.LotsReceivedFlag
  FROM fact_materialmovement_forLots_tmp t,fact_materialmovement f_mm
 WHERE     f_mm.fact_materialmovementid = t.fact_materialmovementid
       AND f_mm.dd_MaterialDocNo = t.dd_MaterialDocNo
       AND f_mm.dd_MaterialDocItemNo = t.dd_MaterialDocItemNo
       AND f_mm.dd_MaterialDocYear = t.dd_MaterialDocYear
       AND f_mm.dim_movementtypeid = t.dim_movementtypeid;

truncate table fact_materialmovement_forLots_tmp;