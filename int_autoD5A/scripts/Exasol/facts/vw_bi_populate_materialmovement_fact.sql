

/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 9 Sep 2013 */
/*   Description    : Stored Proc bi_populate_materialmovement_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc
/*   21 Apr 2016      CristianT 2.0          Adding PO Currency from Purchasing */
/*   13 Apr 2016      Georgiana 1.4              Changing the formula for ct_squantity and adding ct_quantityDC according to BI-2563 */  
/*   04 Jun 2015      Georgiana 1.4              Changing the insert statement for tmp_fact_materialmovement_perf1 */                                                          
/*   28 Nov 2014      Simona	1.3  		 Add dd_ReservationNumber and dd_ItemAutomaticallyCreated used in fact_consumptionhistory*/
/*   21 May 2014      Cornelia	1.0  		 Add dim_uomunitofentryid */
/*   19 May 2014      Cornelia	1.0  		 Add dim_unitofmeasureorderunitid */
/*   21 Sep 2013      Lokesh	1.2 		Performance changes - use hash function and use combine */
/*   09 Sep 2013      Lokesh	1.1           Exchange rate changes	  */
/*											  Note that amts are in local curr e.g MSEG_WAERS is same as dc.currency */
/* 											  We will take MSEG_WAERS as TRA and dim_company.currency as local curr */
/*   06 Sep 2013      Lokesh	1.0  		  Existing version migrated from CVS */
/******************************************************************************************************************/

drop table if exists pGlobalCurrency_tbl;
create table pGlobalCurrency_tbl ( pGlobalCurrency varchar(3));
drop table if exists dim_profitcenter_789;
create table dim_profitcenter_789 as 
select * from dim_profitcenter order by ValidTo;

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'bi_populate_materialmovement_fact START');

Insert into pGlobalCurrency_tbl values('USD');

Update pGlobalCurrency_tbl
SET pGlobalCurrency =
        ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');
                
INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'UPDATE fact_materialmovement ms');

UPDATE MKPF_MSEG 
SET MSEG_BUKRS = ifnull(MSEG_BUKRS,'Not Set')
where MSEG_BUKRS <>  ifnull(MSEG_BUKRS,'Not Set');

UPDATE MKPF_MSEG1 
SET MSEG1_BUKRS = ifnull(MSEG1_BUKRS,'Not Set') 
where  MSEG1_BUKRS <>  ifnull(MSEG1_BUKRS,'Not Set');


drop table if exists tmp_distinct_mkpf_mseg;
create table tmp_distinct_mkpf_mseg as
select b.* from (
select a.*,row_Number() over (partition by MSEG_MBLNR,MSEG_MJAHR,MSEG_ZEILE order by MKPF_BUDAT,MKPF_CPUDT desc) as RN 
from MKPF_MSEG a) b
where b.rn=1;

drop table if exists tmp_distinct_mkpf_mseg1;
create table tmp_distinct_mkpf_mseg1 as
select b.* from (
select a.*,row_Number() over (partition by MSEG1_MBLNR,MSEG1_MJAHR,MSEG1_ZEILE order by MKPF_BUDAT,MKPF_CPUDT desc) as RN 
from MKPF_MSEG1 a) b
where b.rn=1;
drop table if exists MKPF_MSEG;
drop table if exists MKPF_MSEG1;


rename tmp_distinct_mkpf_mseg to MKPF_MSEG;
rename tmp_distinct_mkpf_mseg1 to MKPF_MSEG1;

alter table MKPF_MSEG drop RN cascade;
alter table MKPF_MSEG1 drop RN cascade;
/* delete existing movement from fact */

drop table if exists tmp_fact_materialmovement_del;
create table tmp_fact_materialmovement_del 
as
select DISTINCT ms.fact_materialmovementid
FROM fact_materialmovement ms, MKPF_MSEG m,dim_plant dp,dim_company dc
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

ALTER TABLE tmp_fact_materialmovement_del
add  PRIMARY KEY (fact_materialmovementid);

insert into tmp_fact_materialmovement_del
select DISTINCT ms.fact_materialmovementid
FROM fact_materialmovement ms, MKPF_MSEG1 m,dim_plant dp,dim_company dc
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;


merge into fact_materialmovement fmm
using tmp_fact_materialmovement_del tfmd
on fmm.fact_materialmovementid = tfmd.fact_materialmovementid
when matched then delete;


/* LK:  Used a tmp table to convert similar updates into insert-combine */

DROP TABLE IF EXISTS tmp_fact_materialmovement_perf1;
CREATE TABLE tmp_fact_materialmovement_perf1 like fact_materialmovement INCLUDING DEFAULTS INCLUDING IDENTITY;
ALTER TABLE tmp_fact_materialmovement_perf1 ADD PRIMARY KEY (fact_materialmovementid);



drop table if exists tmp_fmatmov_t001t;
create table tmp_fmatmov_t001t as
SELECT  ms.fact_materialmovementid,
          ms.dd_MaterialDocNo,
          ms.dd_MaterialDocItemNo,
         ms.dd_MaterialDocYear,
				ifnull(m.MSEG_EBELN,'Not Set') dd_DocumentNo,
				m.MSEG_EBELP dd_DocumentItemNo,
				ifnull(m.MSEG_KDAUF,'Not Set') dd_SalesOrderNo ,
				m.MSEG_KDPOS dd_SalesOrderItemNo,
				ifnull(m.MSEG_KDEIN,0) dd_SalesOrderDlvrNo,
				ifnull(m.MSEG_SAKTO,'Not Set') as dd_GLAccountNo,
				ifnull(m.MSEG_LFBNR,'Not Set') as dd_ReferenceDocNo,
				ifnull(m.MSEG_LFPOS,0) as dd_ReferenceDocItem,
				CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END as dd_debitcreditid,
				ifnull(m.MSEG_AUFNR,'Not Set') as dd_productionordernumber,
  				m.MSEG_AUFPS as dd_productionorderitemno ,
  				m.MSEG_GRUND as dd_GoodsMoveReason,
 				ifnull(m.MSEG_CHARG, 'Not Set') as dd_BatchNumber,
  				 m.MSEG_BWTAR as dd_ValuationType ,
                               ms.amt_LocalCurrAmt,
                               ms.amt_StdUnitPrice,
			(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR as amt_DeliveryCost ,
			(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT as amt_AltPriceControl,
			 m.MSEG_SALK3 as amt_OnHand_ValStock,
        convert(decimal (18,4), 1) as amt_ExchangeRate,
       convert(decimal (18,4), 1) as amt_ExchangeRate_GBL,
       m.MSEG_MENGE as ct_Quantity,
	(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE as ct_QuantityDC,
	(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG as ct_QtyEntryUOM,
	 (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG as ct_QtyGROrdUnit ,
  		m.MSEG_LBKUM as ct_QtyOnHand_ValStock,
	(CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG as ct_QtyOrdPriceUnit,
                               ms.Dim_MovementTypeid,
                               ms.Dim_Companyid,
	convert(bigint, 1) as Dim_CurrencyId, 
	convert(bigint, 1) as Dim_Partid, 
                               ms.Dim_Plantid,
	convert(bigint, 1) as Dim_StorageLocationid,
                               ms.Dim_Vendorid,
	convert(bigint, 1) as dim_DateIDMaterialDocDate, 
	convert(bigint, 1) as dim_DateIDPostingDate,
                               ms.Dim_producthierarchyid,
      convert(bigint, 1) as  Dim_MovementIndicatorid ,
                               ms.Dim_Customerid,
                               ms.Dim_CostCenterid,
      convert(bigint, 1) as  Dim_specialstockid,
      convert(bigint, 1) as  Dim_unitofmeasureid,
      convert(bigint, 1) as  Dim_controllingareaid,
     ms.Dim_profitcenterid,
    convert(bigint, 1) as   Dim_consumptiontypeid,
    convert(bigint, 1) as    Dim_stocktypeid ,
	convert(bigint, 1) as Dim_PurchaseGroupid,
	convert(bigint, 1) as Dim_materialgroupid,
     convert(bigint, 1) as   Dim_ReceivingPlantId,
     convert(bigint, 1) as   Dim_ReceivingPartId,
     convert(bigint, 1) as   Dim_RecvIssuStorLocid,
	 convert(bigint, 1) as Dim_CurrencyId_TRA , 
	convert(bigint, 1) as Dim_CurrencyId_GBL ,
ms.amt_plannedprice,
ms.amt_plannedprice1,
ms.amt_pounitprice,
ms.amt_vendorspend,
ms.ct_orderquantity,
ms.dd_consignmentflag,
ms.dd_documentscheduleno,
ms.dd_incoterms2,
ms.dd_vendorspendflag,
ms.dim_accountcategoryid,
ms.dim_afssizeid,
ms.dim_customergroup1id,
ms.dim_dateidcosting,
ms.dim_dateiddelivery,
ms.dim_dateiddoccreation,
ms.dim_dateidordered,
ms.dim_dateidstatdelivery,
ms.dim_documentcategoryid,
ms.dim_documenttypeid,
ms.dim_grstatusid,
ms.dim_incotermid,
ms.dim_inspusagedecisionid,
ms.dim_itemcategoryid,
ms.dim_itemstatusid,
ms.dim_poissustoragelocid,
ms.dim_poplantidordering,
ms.dim_poplantidsupplying,
ms.dim_postoragelocid,
ms.dim_productionorderstatusid,
ms.dim_productionordertypeid,
ms.dim_purchaseorgid,
ms.dim_salesdocumenttypeid,
ms.dim_salesgroupid,
ms.dim_salesorderheaderstatusid,
ms.dim_salesorderitemstatusid,
ms.dim_salesorderrejectreasonid,
ms.dim_salesorgid,
ms.dim_termid,
ms.dirtyrow,
ms.lotsacceptedflag,
ms.lotsawaitinginspectionflag,
ms.lotsinspectedflag,
ms.lotsrejectedflag,
ms.lotsskippedflag,
ms.percentlotsacceptedflag,
ms.percentlotsinspectedflag,
ms.percentlotsrejectedflag,
ms.qtyrejectedexternalamt,
ms.qtyrejectedinternalamt,
ms.dd_intorder,
ms.ct_lotnotthruqm,
ms.lotsreceivedflag,
m.MSEG_WAERS, --amt_ExchangeRate
m.MKPF_BUDAT,  --amt_ExchangeRate
a.pGlobalCurrency, --amt_ExchangeRate_GBL
dc.currency, --Dim_CurrencyId
m.MSEG_MATNR, --Dim_Partid
m.MSEG_WERKS,   --Dim_Partid
m.MSEG_LGORT,  --Dim_StorageLocationid
m.MSEG_BUKRS, --dim_DateIDMaterialDocDate
m.MKPF_BLDAT,  --dim_DateIDMaterialDocDate
m.MSEG_KZBEW, --Dim_MovementIndicatorid
m.MSEG_SOBKZ, --Dim_specialstockid
m.MSEG_MEINS,  --Dim_unitofmeasureid
m.MSEG_KOKRS,  --Dim_controllingareaid
m.MSEG_KZVBR, --Dim_consumptiontypeid
 m.MSEG_INSMK, --Dim_stocktypeid
m.MSEG_UMWRK, --Dim_ReceivingPlantId
m.MSEG_UMMAT, --Dim_ReceivingPartId
m.MSEG_UMLGO  --Dim_RecvIssuStorLocid
FROM fact_materialmovement ms, MKPF_MSEG m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl a
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear;

update tmp_fmatmov_t001t f
set   f.amt_ExchangeRate = ifnull(CASE WHEN f.MSEG_WAERS = f.Currency THEN 1.00 ELSE  z.exchangeRate end,1)
from tmp_fmatmov_t001t f
             left join (select * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_materialmovement_fact') z on
              z.pFromCurrency  = f.MSEG_WAERS and z.pToCurrency = f.Currency and z.pDate =f.MKPF_BUDAT
where f.amt_ExchangeRate <> ifnull(CASE WHEN f.MSEG_WAERS = f.Currency THEN 1.00 ELSE  z.exchangeRate end,1);

update tmp_fmatmov_t001t f
set   f.amt_ExchangeRate_GBL = ifnull(CASE WHEN f.MSEG_WAERS = f.pGlobalCurrency THEN 1.00 ELSE  z.exchangeRate end,1)
from tmp_fmatmov_t001t f
             left join (select * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_materialmovement_fact') z on
              z.pFromCurrency  = f.MSEG_WAERS and z.pToCurrency = f.pGlobalCurrency and z.pDate = current_date
where f.amt_ExchangeRate_GBL <> ifnull(CASE WHEN f.MSEG_WAERS = f.pGlobalCurrency THEN 1.00 ELSE  z.exchangeRate end,1);

update tmp_fmatmov_t001t f
set   f.Dim_CurrencyId = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t001t f
             left join dim_currency dcr on
              dcr.CurrencyCode = f.currency
where  f.Dim_CurrencyId <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t001t f
set   f.Dim_Partid = ifnull(dpr.Dim_Partid,1)
from tmp_fmatmov_t001t f
             left join Dim_Part dpr on
              dpr.PartNumber = f.MSEG_MATNR  AND dpr.Plant = f.MSEG_WERKS
where  f.Dim_Partid <> ifnull(dpr.Dim_Partid,1);

update tmp_fmatmov_t001t f
set   f.Dim_StorageLocationid = ifnull(dsl.Dim_StorageLocationid,1)
from tmp_fmatmov_t001t f
             left join dim_storagelocation dsl on
              dsl.LocationCode = f.MSEG_LGORT AND dsl.Plant = f.MSEG_WERKS
where  f.Dim_StorageLocationid <> ifnull(dsl.Dim_StorageLocationid,1);

update tmp_fmatmov_t001t f
set   f.dim_DateIDMaterialDocDate = ifnull(mdd.Dim_Dateid,1)
from tmp_fmatmov_t001t f
             left join dim_date mdd  on
              mdd.CompanyCode = f.MSEG_BUKRS AND f.MKPF_BLDAT = mdd.DateValue
			  and f.MSEG_WERKS = mdd.plantcode_factory
where  f.dim_DateIDMaterialDocDate <> ifnull(mdd.Dim_Dateid,1);

update tmp_fmatmov_t001t f
set   f.dim_DateIDPostingDate = ifnull(pd.Dim_Dateid,1)
from tmp_fmatmov_t001t f
             left join dim_date pd  on
             pd.CompanyCode = f.MSEG_BUKRS AND f.MKPF_BUDAT = pd.DateValue
			 and f.MSEG_WERKS = pd.plantcode_factory
where  f.dim_DateIDPostingDate <> ifnull(pd.Dim_Dateid,1);

update tmp_fmatmov_t001t f
set   f.Dim_MovementIndicatorid = ifnull(dmi.Dim_MovementIndicatorid,1)
from tmp_fmatmov_t001t f
             left join dim_movementindicator dmi  on
             dmi.TypeCode = f.MSEG_KZBEW
where  f.Dim_MovementIndicatorid <> ifnull(dmi.Dim_MovementIndicatorid,1);

update tmp_fmatmov_t001t f
set   f.Dim_specialstockid = ifnull(spt.dim_specialstockid,1)
from tmp_fmatmov_t001t f
             left join dim_specialstock spt  on
             spt.specialstockindicator = f.MSEG_SOBKZ
where  f.Dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

update tmp_fmatmov_t001t f
set   f.Dim_unitofmeasureid = ifnull(uom.Dim_unitofmeasureid,1)
from tmp_fmatmov_t001t f
             left join dim_unitofmeasure uom  on
             uom.UOM = f.MSEG_MEINS
where  f.Dim_unitofmeasureid <> ifnull(uom.Dim_unitofmeasureid,1);

update tmp_fmatmov_t001t f
set   f.Dim_controllingareaid = ifnull(ca.Dim_controllingareaid,1)
from tmp_fmatmov_t001t f
             left join dim_controllingarea ca  on
             ca.ControllingAreaCode = f.MSEG_KOKRS
where f.Dim_controllingareaid <> ifnull(ca.Dim_controllingareaid,1);

update tmp_fmatmov_t001t f
set   f.Dim_consumptiontypeid = ifnull(ct.Dim_consumptiontypeid,1)
from tmp_fmatmov_t001t f
             left join dim_consumptiontype ct  on
             ct.ConsumptionCode = f.MSEG_KZVBR
where f.Dim_consumptiontypeid <> ifnull(ct.Dim_consumptiontypeid,1);

update tmp_fmatmov_t001t f
set   f.Dim_stocktypeid = ifnull(st.Dim_StockTypeid,1)
from tmp_fmatmov_t001t f
             left join dim_stocktype st  on
             st.TypeCode = f.MSEG_INSMK
where  f.Dim_stocktypeid <> ifnull(st.Dim_StockTypeid,1);

update tmp_fmatmov_t001t f
set   f.Dim_PurchaseGroupid = ifnull(pg1.Dim_PurchaseGroupid,1)
from tmp_fmatmov_t001t f
             left join 
          (select dpr.PartNumber,dpr.Plant,pg.Dim_PurchaseGroupid FROM dim_part dpr 
              inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup) pg1
         on pg1.PartNumber = f.MSEG_MATNR AND pg1.Plant = f.MSEG_WERKS
where  f.Dim_PurchaseGroupid <> ifnull(pg1.Dim_PurchaseGroupid,1);

update tmp_fmatmov_t001t f
set   f.Dim_materialgroupid = ifnull(mg1.Dim_materialgroupid,1)
from tmp_fmatmov_t001t f
             left join 
          (select dpr.PartNumber,dpr.Plant,mg.Dim_materialgroupid FROM dim_part dpr 
              inner join dim_materialgroup mg on mg.MaterialGroupCode = dpr.MaterialGroup) mg1
         on mg1.PartNumber = f.MSEG_MATNR AND mg1.Plant = f.MSEG_WERKS
where  f.Dim_materialgroupid <> ifnull(mg1.Dim_materialgroupid,1);


update tmp_fmatmov_t001t f
set   f.Dim_ReceivingPlantId = ifnull(pl.dim_plantid,1)
from tmp_fmatmov_t001t f
             left join dim_plant pl  on
             pl.PlantCode = f.MSEG_UMWRK
where f.Dim_ReceivingPlantId <> ifnull(pl.dim_plantid,1);

update tmp_fmatmov_t001t f
set   f.Dim_ReceivingPartId = ifnull(prt.Dim_Partid,1)
from tmp_fmatmov_t001t f
             left join dim_part prt
            on prt.Plant = f.MSEG_UMWRK AND prt.PartNumber = f.MSEG_UMMAT
where f.Dim_ReceivingPartId <> ifnull(prt.Dim_Partid,1);

update tmp_fmatmov_t001t f
set   f.Dim_RecvIssuStorLocid = ifnull(dsl.Dim_StorageLocationid,1)
from tmp_fmatmov_t001t f
             left join dim_storagelocation dsl
            on dsl.LocationCode = f.MSEG_UMLGO AND dsl.Plant = f.MSEG_UMWRK
where f.Dim_RecvIssuStorLocid <> ifnull(dsl.Dim_StorageLocationid,1);

update tmp_fmatmov_t001t f
set   f.Dim_CurrencyId_TRA = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t001t f
             left join dim_currency dcr
            on dcr.CurrencyCode = f.MSEG_WAERS
where f.Dim_CurrencyId_TRA <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t001t f
set   f.Dim_CurrencyId_GBL = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t001t f
             left join dim_currency dcr
            on dcr.CurrencyCode = pGlobalCurrency
where f.Dim_CurrencyId_GBL <> ifnull(dcr.Dim_Currencyid,1);


  INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
			       ct_QuantityDC,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
				dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
/* These columns are not actually being updated. But need to keep the values. So they are needed in combine */
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
dd_consignmentflag,
dd_documentscheduleno,
dd_incoterms2,
dd_vendorspendflag,
dim_accountcategoryid,
dim_afssizeid,
dim_customergroup1id,
dim_dateidcosting,
dim_dateiddelivery,
dim_dateiddoccreation,
dim_dateidordered,
dim_dateidstatdelivery,
dim_documentcategoryid,
dim_documenttypeid,
dim_grstatusid,
dim_incotermid,
dim_inspusagedecisionid,
dim_itemcategoryid,
dim_itemstatusid,
dim_poissustoragelocid,
dim_poplantidordering,
dim_poplantidsupplying,
dim_postoragelocid,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_purchaseorgid,
dim_salesdocumenttypeid,
dim_salesgroupid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_salesorderrejectreasonid,
dim_salesorgid,
dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
dd_intorder,
ct_lotnotthruqm,
lotsreceivedflag)
select distinct  fact_materialmovementid,
                        dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                ifnull(dd_ReferenceDocNo, 'Not Set') as dd_ReferenceDocNo,
                                ifnull(dd_ReferenceDocItem, 0) as dd_ReferenceDocItem,
                               ifnull(dd_debitcreditid, 'Not Set') as dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               ifnull(dd_GoodsMoveReason,0) as dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
			       ct_QuantityDC,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               ifnull(Dim_MovementTypeid,1) as Dim_MovementTypeid,
                               ifnull(dim_Companyid,1) as dim_Companyid,
                               ifnull(Dim_Currencyid, 1) as Dim_Currencyid,
                               ifnull(Dim_Partid, 1) as Dim_Partid,
                               ifnull(Dim_Plantid, 1) as Dim_Plantid,
                               ifnull(Dim_StorageLocationid, 1) as Dim_StorageLocationid,
                               ifnull(Dim_Vendorid, 1) as Dim_Vendorid,
                               ifnull(dim_DateIDMaterialDocDate, 1) as dim_DateIDMaterialDocDate,
                               ifnull(dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
                               ifnull(dim_producthierarchyid, 1) as dim_producthierarchyid,
                               ifnull(dim_MovementIndicatorid, 1) as dim_MovementIndicatorid,
                               ifnull(dim_Customerid, 1) as dim_Customerid,
                               ifnull(dim_CostCenterid, 1) as dim_CostCenterid,
                               ifnull(dim_specialstockid, 1) as dim_specialstockid,
                               ifnull(dim_unitofmeasureid, 1) as dim_unitofmeasureid,
                               ifnull(dim_controllingareaid, 1) as dim_controllingareaid,
				                       ifnull(dim_profitcenterid, 1) as dim_profitcenterid,
                               ifnull(dim_consumptiontypeid, 1) as dim_consumptiontypeid,
                               ifnull(dim_stocktypeid, 1) as dim_stocktypeid,
                               ifnull(Dim_PurchaseGroupid, 1) as Dim_PurchaseGroupid,
                               ifnull(dim_materialgroupid, 1) as dim_materialgroupid,
                               ifnull(Dim_ReceivingPlantId, 1) as Dim_ReceivingPlantId,
                               ifnull(Dim_ReceivingPartId, 1) as Dim_ReceivingPartId,
                               ifnull(Dim_RecvIssuStorLocid, 1) as Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
/* These columns are not actually being updated. But need to keep the values. So they are needed in combine */
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
ifnull(dd_consignmentflag, 0) as dd_consignmentflag,
ifnull(dd_documentscheduleno, 0) as dd_documentscheduleno,
dd_incoterms2,
ifnull(dd_vendorspendflag, 0) as dd_vendorspendflag,
ifnull(dim_accountcategoryid, 1) as dim_accountcategoryid,
ifnull(dim_afssizeid, 1) as dim_afssizeid,
ifnull(dim_customergroup1id, 1) as dim_customergroup1id,
ifnull(dim_dateidcosting, 1) as dim_dateidcosting,
ifnull(dim_dateiddelivery, 1) as dim_dateiddelivery,
ifnull(dim_dateiddoccreation, 1) as dim_dateiddoccreation,
ifnull(dim_dateidordered, 1) as dim_dateidordered, 
ifnull(dim_dateidstatdelivery, 1) as dim_dateidstatdelivery,
ifnull(dim_documentcategoryid, 1) as dim_documentcategoryid,
ifnull(dim_documenttypeid, 1) as dim_documenttypeid,
ifnull(dim_grstatusid, 1) as dim_grstatusid,
ifnull(dim_incotermid, 1) as dim_incotermid,
ifnull(dim_inspusagedecisionid, 1) as dim_inspusagedecisionid,
ifnull(dim_itemcategoryid, 1) as dim_itemcategoryid,
ifnull(dim_itemstatusid, 1) as dim_itemstatusid,
ifnull(dim_poissustoragelocid, 1) as dim_poissustoragelocid,
ifnull(dim_poplantidordering, 1) as dim_poplantidordering,
ifnull(dim_poplantidsupplying, 1) as dim_poplantidsupplying,
ifnull(dim_postoragelocid, 1) as dim_postoragelocid,
ifnull(dim_productionorderstatusid, 1) as dim_productionorderstatusid,
ifnull(dim_productionordertypeid, 1) as dim_productionordertypeid,
ifnull(dim_purchaseorgid, 1) as dim_purchaseorgid,
ifnull(dim_salesdocumenttypeid, 1) as dim_salesdocumenttypeid,
ifnull(dim_salesgroupid, 1) as dim_salesgroupid,
ifnull(dim_salesorderheaderstatusid, 1) as dim_salesorderheaderstatusid,
ifnull(dim_salesorderitemstatusid, 1) as dim_salesorderitemstatusid,
ifnull(dim_salesorderrejectreasonid, 1) as dim_salesorderrejectreasonid,
ifnull(dim_salesorgid, 1) as dim_salesorgid,
ifnull(dim_termid, 1) as dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
dd_intorder,
ct_lotnotthruqm,
lotsreceivedflag
from tmp_fmatmov_t001t;

insert into  fact_materialmovement
(LOTSACCEPTEDFLAG,
LOTSREJECTEDFLAG,
PERCENTLOTSINSPECTEDFLAG,
PERCENTLOTSREJECTEDFLAG,
PERCENTLOTSACCEPTEDFLAG,
LOTSSKIPPEDFLAG,
LOTSAWAITINGINSPECTIONFLAG,
LOTSINSPECTEDFLAG,
LOTSRECEIVEDFLAG,
DD_MATERIALDOCNO,
DD_DOCUMENTNO,
DD_SALESORDERNO,
DD_GLACCOUNTNO,
DD_PRODUCTIONORDERNUMBER,
DD_BATCHNUMBER,
DD_VALUATIONTYPE,
DD_DEBITCREDITID,
DD_INCOTERMS2,
DD_INTORDER,
DD_REFERENCEDOCNO,
DD_INSPECTIONSTATUSINGRDOC,
DD_ITEMAUTOMATICALLYCREATED,
DD_MATERIALDOCITEMNO,
DD_MATERIALDOCYEAR,
DD_GOODSMOVEREASON,
AMT_LOCALCURRAMT,
AMT_DELIVERYCOST,
AMT_ALTPRICECONTROL,
AMT_ONHAND_VALSTOCK,
CT_QUANTITY,
CT_QUANTITYDC,
CT_QTYENTRYUOM,
CT_QTYGRORDUNIT,
CT_QTYONHAND_VALSTOCK,
CT_QTYORDPRICEUNIT,
DIRTYROW,
DD_CONSIGNMENTFLAG,
AMT_POUNITPRICE,
AMT_STDUNITPRICE,
AMT_PLANNEDPRICE,
AMT_PLANNEDPRICE1,
DD_DOCUMENTSCHEDULENO,
CT_ORDERQUANTITY,
DD_REFERENCEDOCITEM,
QTYREJECTEDEXTERNALAMT,
QTYREJECTEDINTERNALAMT,
DD_VENDORSPENDFLAG,
DD_RESERVATIONNUMBER,
DIM_MOVEMENTTYPEID,
DIM_COMPANYID,
DIM_CURRENCYID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_VENDORID,
DIM_DATEIDMATERIALDOCDATE,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCCREATION,
DIM_DATEIDORDERED,
DIM_PRODUCTHIERARCHYID,
DIM_MOVEMENTINDICATORID,
DIM_CUSTOMERID,
DIM_COSTCENTERID,
DIM_ACCOUNTCATEGORYID,
DIM_CONSUMPTIONTYPEID,
DIM_CONTROLLINGAREAID,
DIM_CUSTOMERGROUP1ID,
DIM_DOCUMENTCATEGORYID,
DIM_DOCUMENTTYPEID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEORGID,
DIM_SALESDOCUMENTTYPEID,
DIM_SALESGROUPID,
DIM_SALESORDERHEADERSTATUSID,
DIM_SALESORDERITEMSTATUSID,
DIM_SALESORDERREJECTREASONID,
DIM_SALESORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKTYPEID,
DIM_UNITOFMEASUREID,
DIM_MATERIALGROUPID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_TERMID,
DIM_PROFITCENTERID,
DIM_RECEIVINGPLANTID,
DIM_RECEIVINGPARTID,
DIM_DATEIDCOSTING,
DIM_INCOTERMID,
DIM_DATEIDDELIVERY,
DIM_DATEIDSTATDELIVERY,
DIM_GRSTATUSID,
DIM_RECVISSUSTORLOCID,
DIM_POPLANTIDORDERING,
DIM_POPLANTIDSUPPLYING,
DIM_POSTORAGELOCID,
DIM_POISSUSTORAGELOCID,
DIM_INSPUSAGEDECISIONID,
DIM_AFSSIZEID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DIM_UNITOFMEASUREORDERUNITID,
DIM_UOMUNITOFENTRYID,
DIM_PROJECTSOURCEID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_DOCUMENTITEMNO,
DD_SALESORDERITEMNO,
DD_SALESORDERDLVRNO,
DD_PRODUCTIONORDERITEMNO,
CT_LOTNOTTHRUQM,
AMT_VENDORSPEND,
FACT_MATERIALMOVEMENTID) 
select IFNULL(ftmp.LOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSSKIPPEDFLAG,'N'),
IFNULL(ftmp.LOTSAWAITINGINSPECTIONFLAG,'N'),
IFNULL(ftmp.LOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.LOTSRECEIVEDFLAG,'N'),
IFNULL(ftmp.DD_MATERIALDOCNO,'Not Set'),
IFNULL(ftmp.DD_DOCUMENTNO,'Not Set'),
IFNULL(ftmp.DD_SALESORDERNO,'Not Set'),
IFNULL(ftmp.DD_GLACCOUNTNO,'Not Set'),
IFNULL(ftmp.DD_PRODUCTIONORDERNUMBER,'Not Set'),
IFNULL(ftmp.DD_BATCHNUMBER,'Not Set'),
IFNULL(ftmp.DD_VALUATIONTYPE,'Not Set'),
IFNULL(ftmp.DD_DEBITCREDITID,'Not Set'),
IFNULL(ftmp.DD_INCOTERMS2,'Not Set'),
IFNULL(ftmp.DD_INTORDER,'Not Set'),
IFNULL(ftmp.DD_REFERENCEDOCNO,'Not Set'),
IFNULL(ftmp.DD_INSPECTIONSTATUSINGRDOC,'Not Set'),
IFNULL(ftmp.DD_ITEMAUTOMATICALLYCREATED,'Not Set'),
IFNULL(ftmp.DD_MATERIALDOCITEMNO,0),
IFNULL(ftmp.DD_MATERIALDOCYEAR,0),
IFNULL(ftmp.DD_GOODSMOVEREASON,0),
IFNULL(ftmp.AMT_LOCALCURRAMT,0),
IFNULL(ftmp.AMT_DELIVERYCOST,0),
IFNULL(ftmp.AMT_ALTPRICECONTROL,0),
IFNULL(ftmp.AMT_ONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QUANTITY,0),
IFNULL(ftmp.CT_QUANTITYDC,0),
IFNULL(ftmp.CT_QTYENTRYUOM,0),
IFNULL(ftmp.CT_QTYGRORDUNIT,0),
IFNULL(ftmp.CT_QTYONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QTYORDPRICEUNIT,0),
IFNULL(ftmp.DIRTYROW,0),
IFNULL(ftmp.DD_CONSIGNMENTFLAG,0),
IFNULL(ftmp.AMT_POUNITPRICE,0),
IFNULL(ftmp.AMT_STDUNITPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE1,0),
IFNULL(ftmp.DD_DOCUMENTSCHEDULENO,0),
IFNULL(ftmp.CT_ORDERQUANTITY,0),
IFNULL(ftmp.DD_REFERENCEDOCITEM,0),
IFNULL(ftmp.QTYREJECTEDEXTERNALAMT,0),
IFNULL(ftmp.QTYREJECTEDINTERNALAMT,0),
IFNULL(ftmp.DD_VENDORSPENDFLAG,0),
IFNULL(ftmp.DD_RESERVATIONNUMBER,0),
IFNULL(ftmp.DIM_MOVEMENTTYPEID,1),
IFNULL(ftmp.DIM_COMPANYID,1),
IFNULL(ftmp.DIM_CURRENCYID,1),
IFNULL(ftmp.DIM_PARTID,1),
IFNULL(ftmp.DIM_PLANTID,1),
IFNULL(ftmp.DIM_STORAGELOCATIONID,1),
IFNULL(ftmp.DIM_VENDORID,1),
IFNULL(ftmp.DIM_DATEIDMATERIALDOCDATE,1),
IFNULL(ftmp.DIM_DATEIDPOSTINGDATE,1),
IFNULL(ftmp.DIM_DATEIDDOCCREATION,1),
IFNULL(ftmp.DIM_DATEIDORDERED,1),
IFNULL(ftmp.DIM_PRODUCTHIERARCHYID,1),
IFNULL(ftmp.DIM_MOVEMENTINDICATORID,1),
IFNULL(ftmp.DIM_CUSTOMERID,1),
IFNULL(ftmp.DIM_COSTCENTERID,1),
IFNULL(ftmp.DIM_ACCOUNTCATEGORYID,1),
IFNULL(ftmp.DIM_CONSUMPTIONTYPEID,1),
IFNULL(ftmp.DIM_CONTROLLINGAREAID,1),
IFNULL(ftmp.DIM_CUSTOMERGROUP1ID,1),
IFNULL(ftmp.DIM_DOCUMENTCATEGORYID,1),
IFNULL(ftmp.DIM_DOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_ITEMCATEGORYID,1),
IFNULL(ftmp.DIM_ITEMSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERTYPEID,1),
IFNULL(ftmp.DIM_PURCHASEGROUPID,1),
IFNULL(ftmp.DIM_PURCHASEORGID,1),
IFNULL(ftmp.DIM_SALESDOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_SALESGROUPID,1),
IFNULL(ftmp.DIM_SALESORDERHEADERSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERITEMSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERREJECTREASONID,1),
IFNULL(ftmp.DIM_SALESORGID,1),
IFNULL(ftmp.DIM_SPECIALSTOCKID,1),
IFNULL(ftmp.DIM_STOCKTYPEID,1),
IFNULL(ftmp.DIM_UNITOFMEASUREID,1),
IFNULL(ftmp.DIM_MATERIALGROUPID,1),
IFNULL(ftmp.AMT_EXCHANGERATE_GBL,1),
IFNULL(ftmp.AMT_EXCHANGERATE,1),
IFNULL(ftmp.DIM_TERMID,1),
IFNULL(ftmp.DIM_PROFITCENTERID,1),
IFNULL(ftmp.DIM_RECEIVINGPLANTID,1),
IFNULL(ftmp.DIM_RECEIVINGPARTID,1),
IFNULL(ftmp.DIM_DATEIDCOSTING,1),
IFNULL(ftmp.DIM_INCOTERMID,1),
IFNULL(ftmp.DIM_DATEIDDELIVERY,1),
IFNULL(ftmp.DIM_DATEIDSTATDELIVERY,1),
IFNULL(ftmp.DIM_GRSTATUSID,1),
IFNULL(ftmp.DIM_RECVISSUSTORLOCID,1),
IFNULL(ftmp.DIM_POPLANTIDORDERING,1),
IFNULL(ftmp.DIM_POPLANTIDSUPPLYING,1),
IFNULL(ftmp.DIM_POSTORAGELOCID,1),
IFNULL(ftmp.DIM_POISSUSTORAGELOCID,1),
IFNULL(ftmp.DIM_INSPUSAGEDECISIONID,1),
IFNULL(ftmp.DIM_AFSSIZEID,1),
IFNULL(ftmp.DIM_CURRENCYID_TRA,1),
IFNULL(ftmp.DIM_CURRENCYID_GBL,1),
IFNULL(ftmp.DIM_UNITOFMEASUREORDERUNITID,1),
IFNULL(ftmp.DIM_UOMUNITOFENTRYID,1),
IFNULL(ftmp.DIM_PROJECTSOURCEID,1),
IFNULL(ftmp.DW_INSERT_DATE,CURRENT_TIMESTAMP),
IFNULL(ftmp.DW_UPDATE_DATE,CURRENT_TIMESTAMP),
ftmp.DD_DOCUMENTITEMNO,
ftmp.DD_SALESORDERITEMNO,
ftmp.DD_SALESORDERDLVRNO,
ftmp.DD_PRODUCTIONORDERITEMNO,
ftmp.CT_LOTNOTTHRUQM,
ftmp.AMT_VENDORSPEND,
ftmp.FACT_MATERIALMOVEMENTID
 from fact_materialmovement  f
right join tmp_fact_materialmovement_perf1 ftmp                /*only new rec in PERF1*/ 
on f.fact_materialmovementid = ftmp.fact_materialmovementid
where f.fact_materialmovementid is  null;
 

 UPDATE fact_materialmovement ms
  SET     ms.dim_profitcenterid = ifnull(pc.Dim_ProfitCenterid,1)
 from fact_materialmovement ms 
          inner join MKPF_MSEG m
                  on ms.dd_MaterialDocNo = m.MSEG_MBLNR 
                 AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE 
                 AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
          inner join dim_plant dp
                  on m.MSEG_WERKS = dp.PlantCode
          inner join dim_company dc 
                  on m.MSEG_BUKRS = dc.CompanyCode
           left join dim_profitcenter_789 pc
                 on pc.ControllingArea = m.MSEG_KOKRS
                      AND pc.ProfitCenterCode = m.MSEG_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT
where  ms.dim_profitcenterid <> ifnull(pc.Dim_ProfitCenterid,1);



    UPDATE fact_materialmovement ms     
  SET     amt_LocalCurrAmt = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR * ms.amt_ExchangeRate
          from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
		  fact_materialmovement ms 
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND amt_LocalCurrAmt <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR * ms.amt_ExchangeRate;

         UPDATE fact_materialmovement ms     
  SET     amt_StdUnitPrice = m.MSEG_DMBTR / (case when m.MSEG_ERFMG=0 then null else m.MSEG_ERFMG end)	
         from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
		  fact_materialmovement ms  
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND amt_StdUnitPrice <> ( m.MSEG_DMBTR / (case when m.MSEG_ERFMG=0 then null else m.MSEG_ERFMG end));

  UPDATE fact_materialmovement ms
  SET     Dim_Vendorid = 1
  from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
		  fact_materialmovement ms
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	  AND m.MSEG_LIFNR is null;


        UPDATE fact_materialmovement ms
  SET     ms.Dim_Vendorid = dv.Dim_Vendorid
   from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_vendor dv,
    fact_materialmovement ms
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  dv.VendorNumber = m.MSEG_LIFNR
	AND ms.Dim_Vendorid <> dv.Dim_Vendorid;

/*Georgiana 09 Mar 2016 Adding dim_vendormasterid as part of EA Changes*/	
         UPDATE fact_materialmovement ms
  SET     ms.Dim_Vendormasterid = ifnull(dv.Dim_Vendormasterid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
               from MKPF_MSEG m,
          dim_plant dp,
          dim_company dc,
	dim_vendormaster dv,
    fact_materialmovement ms
    WHERE m.MSEG_WERKS = dp.PlantCode
          AND m.MSEG_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
          AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
	AND  dv.VendorNumber = m.MSEG_LIFNR
	AND dv.companycode = ifnull(MSEG_BUKRS, 'Not Set')
	AND ms.Dim_Vendormasterid <> ifnull(dv.Dim_Vendormasterid, 1);
/*Georgiana: End of Changes*/

  UPDATE fact_materialmovement ms
  SET     ms.dim_producthierarchyid = ifnull(dpr1.dim_producthierarchyid, 1)
        from fact_materialmovement ms 
                 inner join MKPF_MSEG m
                         on ms.dd_MaterialDocNo = m.MSEG_MBLNR
                        AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                        AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
                 inner join   dim_plant dp
                        on m.MSEG_WERKS = dp.PlantCode
                 inner join dim_company dc 
                        on m.MSEG_BUKRS = dc.CompanyCode
                 left join (SELECT dpr.PartNumber,dpr.Plant,dph.dim_producthierarchyid
                           FROM dim_producthierarchy dph, dim_part dpr
                           WHERE     dph.ProductHierarchy = dpr.ProductHierarchy) dpr1
                        on dpr1.PartNumber = m.MSEG_MATNR
                         AND dpr1.Plant = m.MSEG_WERKS
 where  ms.dim_producthierarchyid <> ifnull(dpr1.dim_producthierarchyid, 1);

		  
/* Updates from MSEG1 */		  

truncate table tmp_fact_materialmovement_perf1;


/* Create denorm table for insert */
DROP TABLE IF EXISTS tmp_denorm_fact_mm_mseg1;
create table tmp_denorm_fact_mm_mseg1
as
select distinct ms.*,m.*,dc.Currency dc_currency,pGlobalCurrency
 FROM fact_materialmovement ms, MKPF_MSEG1 m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
 AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;



drop table if exists tmp_fmatmov_t002t;
create table tmp_fmatmov_t002t as
SELECT 			ms.fact_materialmovementid,
                      ms.dd_MaterialDocNo,
                               ms.dd_MaterialDocItemNo,
                               ms.dd_MaterialDocYear,
				ifnull(ms.MSEG1_EBELN,'Not Set') dd_DocumentNo,
				ms.MSEG1_EBELP dd_DocumentItemNo,
				ifnull(ms.MSEG1_KDAUF,'Not Set') dd_SalesOrderNo ,
				ms.MSEG1_KDPOS dd_SalesOrderItemNo,
				ifnull(ms.MSEG1_KDEIN,0) as dd_SalesOrderDlvrNo,
				ifnull(ms.MSEG1_SAKTO,'Not Set') as dd_GLAccountNo,
				ifnull(ms.MSEG1_LFBNR,'Not Set') as dd_ReferenceDocNo,
				ifnull(ms.MSEG1_LFPOS,0) as dd_ReferenceDocItem,
				CASE WHEN ms.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END as dd_debitcreditid,
				ifnull(ms.MSEG1_AUFNR,'Not Set') as dd_productionordernumber,
  				ms.MSEG1_AUFPS  as dd_productionorderitemno,
  				ms.MSEG1_GRUND as dd_GoodsMoveReason,
 				ifnull(ms.MSEG1_CHARG, 'Not Set') as dd_BatchNumber,
  				ms.MSEG1_BWTAR as  dd_ValuationType,
               ms.amt_LocalCurrAmt,
	   		   ms.MSEG1_DMBTR / (case when ms.MSEG1_ERFMG=0 then null else ms.MSEG1_ERFMG end) as amt_StdUnitPrice,
     			(CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BNBTR as amt_DeliveryCost,
			    (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BUALT as amt_AltPriceControl ,
			   ms.MSEG1_SALK3 amt_OnHand_ValStock,
        convert(decimal (18,4), 1) as amt_ExchangeRate,
        	convert(decimal (18,4), 1) as amt_ExchangeRate_GBL, ms.MSEG1_MENGE as ct_Quantity,
				(CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_MENGE as ct_QuantityDC ,
				 (CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_ERFMG as ct_QtyEntryUOM ,
				(CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BSTMG  as ct_QtyGROrdUnit,
  				ms.MSEG1_LBKUM  ct_QtyOnHand_ValStock,
				(CASE WHEN ms.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * ms.MSEG1_BPMNG ct_QtyOrdPriceUnit,
                               ms.Dim_MovementTypeid,
                               ms.Dim_Companyid,
	convert(bigint, 1) as Dim_CurrencyId, 
	convert(bigint, 1) as Dim_Partid, 
                               ms.Dim_Plantid,
	convert(bigint, 1) as Dim_StorageLocationid,
	convert(bigint, 1) as Dim_Vendorid,						                               
	convert(bigint, 1) as dim_DateIDMaterialDocDate, 
	convert(bigint, 1) as dim_DateIDPostingDate,
                               ms.Dim_producthierarchyid,
       convert(bigint, 1) as  Dim_MovementIndicatorid ,
                               ms.Dim_Customerid,
                               ms.Dim_CostCenterid,
       convert(bigint, 1) as  Dim_specialstockid,
      convert(bigint, 1) as  Dim_unitofmeasureid,
      convert(bigint, 1) as  Dim_controllingareaid,
                          	ms.Dim_profitcenterid,
        convert(bigint, 1) as   Dim_consumptiontypeid,
    convert(bigint, 1) as    Dim_stocktypeid ,
	convert(bigint, 1) as Dim_PurchaseGroupid,
	convert(bigint, 1) as Dim_materialgroupid,
     convert(bigint, 1) as   Dim_ReceivingPlantId,
     convert(bigint, 1) as   Dim_ReceivingPartId,
     convert(bigint, 1) as   Dim_RecvIssuStorLocid,
	 convert(bigint, 1) as Dim_CurrencyId_TRA , 
	convert(bigint, 1) as Dim_CurrencyId_GBL ,
ms.amt_plannedprice,
ms.amt_plannedprice1,
ms.amt_pounitprice,
ms.amt_vendorspend,
ms.ct_orderquantity,
ms.dd_consignmentflag,
ms.dd_documentscheduleno,
ms.dd_incoterms2,
ms.dd_vendorspendflag,
ms.dim_accountcategoryid,
ms.dim_afssizeid,
ms.dim_customergroup1id,
ms.dim_dateidcosting,
ms.dim_dateiddelivery,
ms.dim_dateiddoccreation,
ms.dim_dateidordered,
ms.dim_dateidstatdelivery,
ms.dim_documentcategoryid,
ms.dim_documenttypeid,
ms.dim_grstatusid,
ms.dim_incotermid,
ms.dim_inspusagedecisionid,
ms.dim_itemcategoryid,
ms.dim_itemstatusid,
ms.dim_poissustoragelocid,
ms.dim_poplantidordering,
ms.dim_poplantidsupplying,
ms.dim_postoragelocid,
ms.dim_productionorderstatusid,
ms.dim_productionordertypeid,
ms.dim_purchaseorgid,
ms.dim_salesdocumenttypeid,
ms.dim_salesgroupid,
ms.dim_salesorderheaderstatusid,
ms.dim_salesorderitemstatusid,
ms.dim_salesorderrejectreasonid,
ms.dim_salesorgid,
ms.dim_termid,
ms.dirtyrow,
ms.lotsacceptedflag,
ms.lotsawaitinginspectionflag,
ms.lotsinspectedflag,
ms.lotsrejectedflag,
ms.lotsskippedflag,
ms.percentlotsacceptedflag,
ms.percentlotsinspectedflag,
ms.percentlotsrejectedflag,
ms.qtyrejectedexternalamt,
ms.qtyrejectedinternalamt,
				ms.dd_intorder,
				ms.ct_lotnotthruqm,
				ms.lotsreceivedflag,
ms.MSEG1_WAERS, --amt_ExchangeRate
ms.MKPF_BUDAT,  --amt_ExchangeRate
ms.pGlobalCurrency, --amt_ExchangeRate_GBL
ms.dc_currency, --Dim_CurrencyId
ms.MSEG1_MATNR, --Dim_Partid
ms.MSEG1_WERKS,   --Dim_Partid
ms.MSEG1_LGORT,  --Dim_StorageLocationid
ms.MSEG1_LIFNR,  --Dim_Vendorid
ms.MSEG1_BUKRS, --dim_DateIDMaterialDocDate
ms.MKPF_BLDAT,  --dim_DateIDMaterialDocDate
ms.MSEG1_KZBEW, --Dim_MovementIndicatorid
ms.MSEG1_SOBKZ, --Dim_specialstockid
ms.MSEG1_MEINS,  --Dim_unitofmeasureid
ms.MSEG1_KOKRS,  --Dim_controllingareaid
ms.MSEG1_KZVBR, --Dim_consumptiontypeid
 ms.MSEG1_INSMK, --Dim_stocktypeid
ms.MSEG1_UMWRK, --Dim_ReceivingPlantId
ms.MSEG1_UMMAT, --Dim_ReceivingPartId
ms.MSEG1_UMLGO  --Dim_RecvIssuStorLocid
FROM tmp_denorm_fact_mm_mseg1 ms;

update tmp_fmatmov_t002t f
set   f.amt_ExchangeRate = ifnull(CASE WHEN f.MSEG1_WAERS = f.dc_currency THEN 1.00 ELSE  z.exchangeRate end,1)
from tmp_fmatmov_t002t f
             left join (select * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_materialmovement_fact') z on
              z.pFromCurrency  = f.MSEG1_WAERS and z.pToCurrency = f.dc_currency and z.pDate =f.MKPF_BUDAT
where f.amt_ExchangeRate <> ifnull(CASE WHEN f.MSEG1_WAERS = f.dc_currency THEN 1.00 ELSE  z.exchangeRate end,1);

update tmp_fmatmov_t002t f
set   f.amt_ExchangeRate_GBL = ifnull(CASE WHEN f.MSEG1_WAERS = f.pGlobalCurrency THEN 1.00 ELSE  z.exchangeRate end,1)
from tmp_fmatmov_t002t f
             left join (select * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_materialmovement_fact') z on
              z.pFromCurrency  = f.MSEG1_WAERS and z.pToCurrency = f.pGlobalCurrency and z.pDate = current_date
where f.amt_ExchangeRate_GBL <> ifnull(CASE WHEN f.MSEG1_WAERS = f.pGlobalCurrency THEN 1.00 ELSE  z.exchangeRate end,1);

update tmp_fmatmov_t002t f
set   f.Dim_CurrencyId = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t002t f
             left join dim_currency dcr on
              dcr.CurrencyCode = f.dc_currency
where  f.Dim_CurrencyId <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t002t f
set   f.Dim_Partid = ifnull(dpr.Dim_Partid,1)
from tmp_fmatmov_t002t f
             left join Dim_Part dpr on
              dpr.PartNumber = f.MSEG1_MATNR  AND dpr.Plant = f.MSEG1_WERKS
where  f.Dim_Partid <> ifnull(dpr.Dim_Partid,1);

update tmp_fmatmov_t002t f
set   f.Dim_StorageLocationid = ifnull(dsl.Dim_StorageLocationid,1)
from tmp_fmatmov_t002t f
             left join dim_storagelocation dsl on
              dsl.LocationCode = f.MSEG1_LGORT AND dsl.Plant = f.MSEG1_WERKS
where  f.Dim_StorageLocationid <> ifnull(dsl.Dim_StorageLocationid,1);

update tmp_fmatmov_t002t f
set   f.Dim_Vendorid = ifnull(dv.Dim_Vendorid,1)
from tmp_fmatmov_t002t f
             left join Dim_Vendor dv
            on dv.VendorNumber = f.MSEG1_LIFNR
where f.Dim_Vendorid <> ifnull(dv.Dim_Vendorid,1);

update tmp_fmatmov_t002t f
set   f.dim_DateIDMaterialDocDate = ifnull(mdd.Dim_Dateid,1)
from tmp_fmatmov_t002t f
             left join dim_date mdd  on
              mdd.CompanyCode = f.MSEG1_BUKRS AND f.MKPF_BLDAT = mdd.DateValue
	      and f.MSEG1_WERKS = mdd.plantcode_factory
where  f.dim_DateIDMaterialDocDate <> ifnull(mdd.Dim_Dateid,1);

update tmp_fmatmov_t002t f
set   f.dim_DateIDPostingDate = ifnull(pd.Dim_Dateid,1)
from tmp_fmatmov_t002t f
             left join dim_date pd  on
             pd.CompanyCode = f.MSEG1_BUKRS AND f.MKPF_BUDAT = pd.DateValue
			 and f.MSEG1_WERKS = pd.plantcode_factory
where  f.dim_DateIDPostingDate <> ifnull(pd.Dim_Dateid,1);

update tmp_fmatmov_t002t f
set   f.Dim_MovementIndicatorid = ifnull(dmi.Dim_MovementIndicatorid,1)
from tmp_fmatmov_t002t f
             left join dim_movementindicator dmi  on
             dmi.TypeCode = f.MSEG1_KZBEW
where  f.Dim_MovementIndicatorid <> ifnull(dmi.Dim_MovementIndicatorid,1);

update tmp_fmatmov_t002t f
set   f.Dim_specialstockid = ifnull(spt.dim_specialstockid,1)
from tmp_fmatmov_t002t f
             left join dim_specialstock spt  on
             spt.specialstockindicator = f.MSEG1_SOBKZ
where  f.Dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

update tmp_fmatmov_t002t f
set   f.Dim_unitofmeasureid = ifnull(uom.Dim_unitofmeasureid,1)
from tmp_fmatmov_t002t f
             left join dim_unitofmeasure uom  on
             uom.UOM = f.MSEG1_MEINS
where  f.Dim_unitofmeasureid <> ifnull(uom.Dim_unitofmeasureid,1);

update tmp_fmatmov_t002t f
set   f.Dim_controllingareaid = ifnull(ca.Dim_controllingareaid,1)
from tmp_fmatmov_t002t f
             left join dim_controllingarea ca  on
             ca.ControllingAreaCode = f.MSEG1_KOKRS
where f.Dim_controllingareaid <> ifnull(ca.Dim_controllingareaid,1);

update tmp_fmatmov_t002t f
set   f.Dim_consumptiontypeid = ifnull(ct.Dim_consumptiontypeid,1)
from tmp_fmatmov_t002t f
             left join dim_consumptiontype ct  on
             ct.ConsumptionCode = f.MSEG1_KZVBR
where f.Dim_consumptiontypeid <> ifnull(ct.Dim_consumptiontypeid,1);

update tmp_fmatmov_t002t f
set   f.Dim_stocktypeid = ifnull(st.Dim_StockTypeid,1)
from tmp_fmatmov_t002t f
             left join dim_stocktype st  on
             st.TypeCode = f.MSEG1_INSMK
where  f.Dim_stocktypeid <> ifnull(st.Dim_StockTypeid,1);

update tmp_fmatmov_t002t f
set   f.Dim_PurchaseGroupid = ifnull(pg1.Dim_PurchaseGroupid,1)
from tmp_fmatmov_t002t f
             left join 
          (select dpr.PartNumber,dpr.Plant,pg.Dim_PurchaseGroupid FROM dim_part dpr 
              inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup) pg1
         on pg1.PartNumber = f.MSEG1_MATNR AND pg1.Plant = f.MSEG1_WERKS
where  f.Dim_PurchaseGroupid <> ifnull(pg1.Dim_PurchaseGroupid,1);

update tmp_fmatmov_t002t f
set   f.Dim_materialgroupid = ifnull(mg1.Dim_materialgroupid,1)
from tmp_fmatmov_t002t f
             left join 
          (select dpr.PartNumber,dpr.Plant,mg.Dim_materialgroupid FROM dim_part dpr 
              inner join dim_materialgroup mg on mg.MaterialGroupCode = dpr.MaterialGroup) mg1
         on mg1.PartNumber = f.MSEG1_MATNR AND mg1.Plant = f.MSEG1_WERKS
where  f.Dim_materialgroupid <> ifnull(mg1.Dim_materialgroupid,1);


update tmp_fmatmov_t002t f
set   f.Dim_ReceivingPlantId = ifnull(pl.dim_plantid,1)
from tmp_fmatmov_t002t f
             left join dim_plant pl  on
             pl.PlantCode = f.MSEG1_UMWRK
where f.Dim_ReceivingPlantId <> ifnull(pl.dim_plantid,1);

update tmp_fmatmov_t002t f
set   f.Dim_ReceivingPartId = ifnull(prt.Dim_Partid,1)
from tmp_fmatmov_t002t f
             left join dim_part prt
            on prt.Plant = f.MSEG1_UMWRK AND prt.PartNumber = f.MSEG1_UMMAT
where f.Dim_ReceivingPartId <> ifnull(prt.Dim_Partid,1);

update tmp_fmatmov_t002t f
set   f.Dim_RecvIssuStorLocid = ifnull(dsl.Dim_StorageLocationid,1)
from tmp_fmatmov_t002t f
             left join dim_storagelocation dsl
            on dsl.LocationCode = f.MSEG1_UMLGO AND dsl.Plant = f.MSEG1_UMWRK
where f.Dim_RecvIssuStorLocid <> ifnull(dsl.Dim_StorageLocationid,1);

update tmp_fmatmov_t002t f
set   f.Dim_CurrencyId_TRA = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t002t f
             left join dim_currency dcr
            on dcr.CurrencyCode = f.MSEG1_WAERS
where f.Dim_CurrencyId_TRA <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t002t f
set   f.Dim_CurrencyId_GBL = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t002t f
             left join dim_currency dcr
            on dcr.CurrencyCode = pGlobalCurrency
where f.Dim_CurrencyId_GBL <> ifnull(dcr.Dim_Currencyid,1);

INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
			       ct_QuantityDC,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
				dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
dd_consignmentflag,
dd_documentscheduleno,
dd_incoterms2,
dd_vendorspendflag,
dim_accountcategoryid,
dim_afssizeid,
dim_customergroup1id,
dim_dateidcosting,
dim_dateiddelivery,
dim_dateiddoccreation,
dim_dateidordered,
dim_dateidstatdelivery,
dim_documentcategoryid,
dim_documenttypeid,
dim_grstatusid,
dim_incotermid,
dim_inspusagedecisionid,
dim_itemcategoryid,
dim_itemstatusid,
dim_poissustoragelocid,
dim_poplantidordering,
dim_poplantidsupplying,
dim_postoragelocid,
dim_productionorderstatusid,
dim_productionordertypeid,
dim_purchaseorgid,
dim_salesdocumenttypeid,
dim_salesgroupid,
dim_salesorderheaderstatusid,
dim_salesorderitemstatusid,
dim_salesorderrejectreasonid,
dim_salesorgid,
dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
				dd_intorder,
				ct_lotnotthruqm,
lotsreceivedflag)
select fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                               ifnull(dd_ReferenceDocNo, 'Not Set') as dd_ReferenceDocNo,
                               ifnull(dd_ReferenceDocItem,0) as dd_ReferenceDocItem,
                               ifnull(dd_debitcreditid, 'Not Set') as dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               ifnull(dd_GoodsMoveReason, 0) as dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
			       ct_QuantityDC,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               ifnull(Dim_MovementTypeid,1) as Dim_MovementTypeid,
                               ifnull(dim_Companyid,1) as dim_Companyid,
                               ifnull(Dim_Currencyid,1) as Dim_Currencyid,
                               ifnull(Dim_Partid, 1) as Dim_Partid,
                               ifnull(Dim_Plantid, 1) as Dim_Plantid,
                               ifnull(Dim_StorageLocationid, 1) as Dim_StorageLocationid,
                               ifnull(Dim_Vendorid, 1) as Dim_Vendorid,
                               ifnull(dim_DateIDMaterialDocDate, 1) as dim_DateIDMaterialDocDate,
                               ifnull(dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
                               ifnull(dim_producthierarchyid, 1) as dim_producthierarchyid,
                               ifnull(dim_MovementIndicatorid, 1) as dim_MovementIndicatorid,
                               ifnull(dim_Customerid, 1) as dim_Customerid,
                               ifnull(dim_CostCenterid, 1) as dim_CostCenterid,
                               ifnull(dim_specialstockid, 1) as dim_specialstockid,
                               ifnull(dim_unitofmeasureid, 1) as dim_unitofmeasureid,
                               ifnull(dim_controllingareaid, 1) as dim_controllingareaid,
				                       ifnull(dim_profitcenterid, 1) as dim_profitcenterid,
                               ifnull(dim_consumptiontypeid, 1) as dim_consumptiontypeid,
                               ifnull(dim_stocktypeid, 1) as dim_stocktypeid,
                               ifnull(Dim_PurchaseGroupid, 1) as Dim_PurchaseGroupid,
                               ifnull(dim_materialgroupid, 1) as dim_materialgroupid,
                               ifnull(Dim_ReceivingPlantId, 1) as Dim_ReceivingPlantId,
                               ifnull(Dim_ReceivingPartId, 1) as Dim_ReceivingPartId,
                               ifnull(Dim_RecvIssuStorLocid, 1) as Dim_RecvIssuStorLocid,
			   dim_Currencyid_TRA,
			   dim_Currencyid_GBL,
amt_plannedprice,
amt_plannedprice1,
amt_pounitprice,
amt_vendorspend,
ct_orderquantity,
ifnull(dd_consignmentflag, 0) as dd_consignmentflag,
ifnull(dd_documentscheduleno, 0) as dd_documentscheduleno,
dd_incoterms2,
ifnull(dd_vendorspendflag, 0) as dd_vendorspendflag,
ifnull(dim_accountcategoryid, 1) as dim_accountcategoryid,
ifnull(dim_afssizeid, 1) as dim_afssizeid,
ifnull(dim_customergroup1id, 1) as dim_customergroup1id,
ifnull(dim_dateidcosting, 1) as dim_dateidcosting,
ifnull(dim_dateiddelivery, 1) as dim_dateiddelivery,
ifnull(dim_dateiddoccreation,1) as dim_dateiddoccreation,
ifnull(dim_dateidordered, 1) as dim_dateidordered,
ifnull(dim_dateidstatdelivery, 1) as dim_dateidstatdelivery,
ifnull(dim_documentcategoryid, 1) as dim_documentcategoryid,
ifnull(dim_documenttypeid, 1) as dim_documenttypeid,
ifnull(dim_grstatusid, 1) as dim_grstatusid,
ifnull(dim_incotermid, 1) as dim_incotermid,
ifnull(dim_inspusagedecisionid, 1) as dim_inspusagedecisionid,
ifnull(dim_itemcategoryid, 1) as dim_itemcategoryid,
ifnull(dim_itemstatusid, 1) as dim_itemstatusid,
ifnull(dim_poissustoragelocid, 1) as dim_poissustoragelocid,
ifnull(dim_poplantidordering, 1) as dim_poplantidordering,
ifnull(dim_poplantidsupplying, 1) as dim_poplantidsupplying,
ifnull(dim_postoragelocid, 1) as dim_postoragelocid,
ifnull(dim_productionorderstatusid, 1) as dim_productionorderstatusid,
ifnull(dim_productionordertypeid, 1) as dim_productionordertypeid,
ifnull(dim_purchaseorgid, 1) as dim_purchaseorgid,
ifnull(dim_salesdocumenttypeid, 1) as dim_salesdocumenttypeid,
ifnull(dim_salesgroupid, 1) as dim_salesgroupid,
ifnull(dim_salesorderheaderstatusid, 1) as dim_salesorderheaderstatusid,
ifnull(dim_salesorderitemstatusid, 1) as dim_salesorderitemstatusid,
ifnull(dim_salesorderrejectreasonid, 1) as dim_salesorderrejectreasonid,
ifnull(dim_salesorgid, 1) as dim_salesorgid,
ifnull(dim_termid, 1) as dim_termid,
dirtyrow,
lotsacceptedflag,
lotsawaitinginspectionflag,
lotsinspectedflag,
lotsrejectedflag,
lotsskippedflag,
percentlotsacceptedflag,
percentlotsinspectedflag,
percentlotsrejectedflag,
qtyrejectedexternalamt,
qtyrejectedinternalamt,
				dd_intorder,
				ct_lotnotthruqm,
lotsreceivedflag from tmp_fmatmov_t002t;

insert into  fact_materialmovement
(LOTSACCEPTEDFLAG,
LOTSREJECTEDFLAG,
PERCENTLOTSINSPECTEDFLAG,
PERCENTLOTSREJECTEDFLAG,
PERCENTLOTSACCEPTEDFLAG,
LOTSSKIPPEDFLAG,
LOTSAWAITINGINSPECTIONFLAG,
LOTSINSPECTEDFLAG,
LOTSRECEIVEDFLAG,
DD_MATERIALDOCNO,
DD_DOCUMENTNO,
DD_SALESORDERNO,
DD_GLACCOUNTNO,
DD_PRODUCTIONORDERNUMBER,
DD_BATCHNUMBER,
DD_VALUATIONTYPE,
DD_DEBITCREDITID,
DD_INCOTERMS2,
DD_INTORDER,
DD_REFERENCEDOCNO,
DD_INSPECTIONSTATUSINGRDOC,
DD_ITEMAUTOMATICALLYCREATED,
DD_MATERIALDOCITEMNO,
DD_MATERIALDOCYEAR,
DD_GOODSMOVEREASON,
AMT_LOCALCURRAMT,
AMT_DELIVERYCOST,
AMT_ALTPRICECONTROL,
AMT_ONHAND_VALSTOCK,
CT_QUANTITY,
CT_QUANTITYDC,
CT_QTYENTRYUOM,
CT_QTYGRORDUNIT,
CT_QTYONHAND_VALSTOCK,
CT_QTYORDPRICEUNIT,
DIRTYROW,
DD_CONSIGNMENTFLAG,
AMT_POUNITPRICE,
AMT_STDUNITPRICE,
AMT_PLANNEDPRICE,
AMT_PLANNEDPRICE1,
DD_DOCUMENTSCHEDULENO,
CT_ORDERQUANTITY,
DD_REFERENCEDOCITEM,
QTYREJECTEDEXTERNALAMT,
QTYREJECTEDINTERNALAMT,
DD_VENDORSPENDFLAG,
DD_RESERVATIONNUMBER,
DIM_MOVEMENTTYPEID,
DIM_COMPANYID,
DIM_CURRENCYID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_VENDORID,
DIM_DATEIDMATERIALDOCDATE,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCCREATION,
DIM_DATEIDORDERED,
DIM_PRODUCTHIERARCHYID,
DIM_MOVEMENTINDICATORID,
DIM_CUSTOMERID,
DIM_COSTCENTERID,
DIM_ACCOUNTCATEGORYID,
DIM_CONSUMPTIONTYPEID,
DIM_CONTROLLINGAREAID,
DIM_CUSTOMERGROUP1ID,
DIM_DOCUMENTCATEGORYID,
DIM_DOCUMENTTYPEID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEORGID,
DIM_SALESDOCUMENTTYPEID,
DIM_SALESGROUPID,
DIM_SALESORDERHEADERSTATUSID,
DIM_SALESORDERITEMSTATUSID,
DIM_SALESORDERREJECTREASONID,
DIM_SALESORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKTYPEID,
DIM_UNITOFMEASUREID,
DIM_MATERIALGROUPID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_TERMID,
DIM_PROFITCENTERID,
DIM_RECEIVINGPLANTID,
DIM_RECEIVINGPARTID,
DIM_DATEIDCOSTING,
DIM_INCOTERMID,
DIM_DATEIDDELIVERY,
DIM_DATEIDSTATDELIVERY,
DIM_GRSTATUSID,
DIM_RECVISSUSTORLOCID,
DIM_POPLANTIDORDERING,
DIM_POPLANTIDSUPPLYING,
DIM_POSTORAGELOCID,
DIM_POISSUSTORAGELOCID,
DIM_INSPUSAGEDECISIONID,
DIM_AFSSIZEID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DIM_UNITOFMEASUREORDERUNITID,
DIM_UOMUNITOFENTRYID,
DIM_PROJECTSOURCEID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_DOCUMENTITEMNO,
DD_SALESORDERITEMNO,
DD_SALESORDERDLVRNO,
DD_PRODUCTIONORDERITEMNO,
CT_LOTNOTTHRUQM,
AMT_VENDORSPEND,
FACT_MATERIALMOVEMENTID) 
select IFNULL(ftmp.LOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSSKIPPEDFLAG,'N'),
IFNULL(ftmp.LOTSAWAITINGINSPECTIONFLAG,'N'),
IFNULL(ftmp.LOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.LOTSRECEIVEDFLAG,'N'),
IFNULL(ftmp.DD_MATERIALDOCNO,'Not Set'),
IFNULL(ftmp.DD_DOCUMENTNO,'Not Set'),
IFNULL(ftmp.DD_SALESORDERNO,'Not Set'),
IFNULL(ftmp.DD_GLACCOUNTNO,'Not Set'),
IFNULL(ftmp.DD_PRODUCTIONORDERNUMBER,'Not Set'),
IFNULL(ftmp.DD_BATCHNUMBER,'Not Set'),
IFNULL(ftmp.DD_VALUATIONTYPE,'Not Set'),
IFNULL(ftmp.DD_DEBITCREDITID,'Not Set'),
IFNULL(ftmp.DD_INCOTERMS2,'Not Set'),
IFNULL(ftmp.DD_INTORDER,'Not Set'),
IFNULL(ftmp.DD_REFERENCEDOCNO,'Not Set'),
IFNULL(ftmp.DD_INSPECTIONSTATUSINGRDOC,'Not Set'),
IFNULL(ftmp.DD_ITEMAUTOMATICALLYCREATED,'Not Set'),
IFNULL(ftmp.DD_MATERIALDOCITEMNO,0),
IFNULL(ftmp.DD_MATERIALDOCYEAR,0),
IFNULL(ftmp.DD_GOODSMOVEREASON,0),
IFNULL(ftmp.AMT_LOCALCURRAMT,0),
IFNULL(ftmp.AMT_DELIVERYCOST,0),
IFNULL(ftmp.AMT_ALTPRICECONTROL,0),
IFNULL(ftmp.AMT_ONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QUANTITY,0),
IFNULL(ftmp.CT_QUANTITYDC,0),
IFNULL(ftmp.CT_QTYENTRYUOM,0),
IFNULL(ftmp.CT_QTYGRORDUNIT,0),
IFNULL(ftmp.CT_QTYONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QTYORDPRICEUNIT,0),
IFNULL(ftmp.DIRTYROW,0),
IFNULL(ftmp.DD_CONSIGNMENTFLAG,0),
IFNULL(ftmp.AMT_POUNITPRICE,0),
IFNULL(ftmp.AMT_STDUNITPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE1,0),
IFNULL(ftmp.DD_DOCUMENTSCHEDULENO,0),
IFNULL(ftmp.CT_ORDERQUANTITY,0),
IFNULL(ftmp.DD_REFERENCEDOCITEM,0),
IFNULL(ftmp.QTYREJECTEDEXTERNALAMT,0),
IFNULL(ftmp.QTYREJECTEDINTERNALAMT,0),
IFNULL(ftmp.DD_VENDORSPENDFLAG,0),
IFNULL(ftmp.DD_RESERVATIONNUMBER,0),
IFNULL(ftmp.DIM_MOVEMENTTYPEID,1),
IFNULL(ftmp.DIM_COMPANYID,1),
IFNULL(ftmp.DIM_CURRENCYID,1),
IFNULL(ftmp.DIM_PARTID,1),
IFNULL(ftmp.DIM_PLANTID,1),
IFNULL(ftmp.DIM_STORAGELOCATIONID,1),
IFNULL(ftmp.DIM_VENDORID,1),
IFNULL(ftmp.DIM_DATEIDMATERIALDOCDATE,1),
IFNULL(ftmp.DIM_DATEIDPOSTINGDATE,1),
IFNULL(ftmp.DIM_DATEIDDOCCREATION,1),
IFNULL(ftmp.DIM_DATEIDORDERED,1),
IFNULL(ftmp.DIM_PRODUCTHIERARCHYID,1),
IFNULL(ftmp.DIM_MOVEMENTINDICATORID,1),
IFNULL(ftmp.DIM_CUSTOMERID,1),
IFNULL(ftmp.DIM_COSTCENTERID,1),
IFNULL(ftmp.DIM_ACCOUNTCATEGORYID,1),
IFNULL(ftmp.DIM_CONSUMPTIONTYPEID,1),
IFNULL(ftmp.DIM_CONTROLLINGAREAID,1),
IFNULL(ftmp.DIM_CUSTOMERGROUP1ID,1),
IFNULL(ftmp.DIM_DOCUMENTCATEGORYID,1),
IFNULL(ftmp.DIM_DOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_ITEMCATEGORYID,1),
IFNULL(ftmp.DIM_ITEMSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERTYPEID,1),
IFNULL(ftmp.DIM_PURCHASEGROUPID,1),
IFNULL(ftmp.DIM_PURCHASEORGID,1),
IFNULL(ftmp.DIM_SALESDOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_SALESGROUPID,1),
IFNULL(ftmp.DIM_SALESORDERHEADERSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERITEMSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERREJECTREASONID,1),
IFNULL(ftmp.DIM_SALESORGID,1),
IFNULL(ftmp.DIM_SPECIALSTOCKID,1),
IFNULL(ftmp.DIM_STOCKTYPEID,1),
IFNULL(ftmp.DIM_UNITOFMEASUREID,1),
IFNULL(ftmp.DIM_MATERIALGROUPID,1),
IFNULL(ftmp.AMT_EXCHANGERATE_GBL,1),
IFNULL(ftmp.AMT_EXCHANGERATE,1),
IFNULL(ftmp.DIM_TERMID,1),
IFNULL(ftmp.DIM_PROFITCENTERID,1),
IFNULL(ftmp.DIM_RECEIVINGPLANTID,1),
IFNULL(ftmp.DIM_RECEIVINGPARTID,1),
IFNULL(ftmp.DIM_DATEIDCOSTING,1),
IFNULL(ftmp.DIM_INCOTERMID,1),
IFNULL(ftmp.DIM_DATEIDDELIVERY,1),
IFNULL(ftmp.DIM_DATEIDSTATDELIVERY,1),
IFNULL(ftmp.DIM_GRSTATUSID,1),
IFNULL(ftmp.DIM_RECVISSUSTORLOCID,1),
IFNULL(ftmp.DIM_POPLANTIDORDERING,1),
IFNULL(ftmp.DIM_POPLANTIDSUPPLYING,1),
IFNULL(ftmp.DIM_POSTORAGELOCID,1),
IFNULL(ftmp.DIM_POISSUSTORAGELOCID,1),
IFNULL(ftmp.DIM_INSPUSAGEDECISIONID,1),
IFNULL(ftmp.DIM_AFSSIZEID,1),
IFNULL(ftmp.DIM_CURRENCYID_TRA,1),
IFNULL(ftmp.DIM_CURRENCYID_GBL,1),
IFNULL(ftmp.DIM_UNITOFMEASUREORDERUNITID,1),
IFNULL(ftmp.DIM_UOMUNITOFENTRYID,1),
IFNULL(ftmp.DIM_PROJECTSOURCEID,1),
IFNULL(ftmp.DW_INSERT_DATE,CURRENT_TIMESTAMP),
IFNULL(ftmp.DW_UPDATE_DATE,CURRENT_TIMESTAMP),
ftmp.DD_DOCUMENTITEMNO,
ftmp.DD_SALESORDERITEMNO,
ftmp.DD_SALESORDERDLVRNO,
ftmp.DD_PRODUCTIONORDERITEMNO,
ftmp.CT_LOTNOTTHRUQM,
ftmp.AMT_VENDORSPEND,
ftmp.FACT_MATERIALMOVEMENTID
 from fact_materialmovement  f
right join tmp_fact_materialmovement_perf1 ftmp
on f.fact_materialmovementid = ftmp.fact_materialmovementid
where f.fact_materialmovementid is  null;


truncate table tmp_fact_materialmovement_perf1;
truncate table tmp_fact_materialmovement_del;

 		  
UPDATE fact_materialmovement ms
  SET     amt_LocalCurrAmt = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR * ms.amt_exchangerate
  FROM  MKPF_MSEG1 m,
          dim_plant dp,
          dim_company dc,
		  fact_materialmovement ms
    WHERE m.MSEG1_WERKS = dp.PlantCode
          AND m.MSEG1_BUKRS = dc.CompanyCode
          AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
          AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
          AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear;


   UPDATE fact_materialmovement ms
  SET     ms.dim_profitcenterid = ifnull(pc.Dim_ProfitCenterid,1)
 from fact_materialmovement ms 
          inner join MKPF_MSEG1 m
                  on ms.dd_MaterialDocNo = m.MSEG1_MBLNR 
                 AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE 
                 AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
          inner join dim_plant dp
                  on m.MSEG1_WERKS = dp.PlantCode
          inner join dim_company dc 
                  on m.MSEG1_BUKRS = dc.CompanyCode
           left join dim_profitcenter_789 pc
                 on pc.ControllingArea = m.MSEG1_KOKRS
                      AND pc.ProfitCenterCode = m.MSEG1_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT
where  ms.dim_profitcenterid <> ifnull(pc.Dim_ProfitCenterid,1);

		  
       UPDATE fact_materialmovement ms
  SET     ms.dim_producthierarchyid = ifnull(dpr1.dim_producthierarchyid, 1)
        from fact_materialmovement ms 
                 inner join  MKPF_MSEG1 m
                         on ms.dd_MaterialDocNo = m.MSEG1_MBLNR
                        AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                        AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
                 inner join   dim_plant dp
                        on m.MSEG1_WERKS = dp.PlantCode
                 inner join dim_company dc 
                        on m.MSEG1_BUKRS = dc.CompanyCode
                 left join (SELECT dpr.PartNumber,dpr.Plant,dph.dim_producthierarchyid
                           FROM dim_producthierarchy dph, dim_part dpr
                           WHERE     dph.ProductHierarchy = dpr.ProductHierarchy) dpr1
                        on dpr1.PartNumber = m.MSEG1_MATNR
                         AND dpr1.Plant = m.MSEG1_WERKS
 where  ms.dim_producthierarchyid <> ifnull(dpr1.dim_producthierarchyid, 1);       

INSERT INTO processinglog (processinglogid, referencename, enddate, description)
 VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),
'bi_populate_materialmovement_fact',current_timestamp, 
'UPDATE fact_materialmovement ms');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',
current_timestamp, 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');


Drop table if exists max_holder_789;
create table max_holder_789
as
Select ifnull(max(fact_materialmovementid),ifnull((select DIM_PROJECTSOURCEID * MULTIPLIER from dim_projectsource), 0)) as maxid
from fact_materialmovement;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG;
CREATE TABLE tmp_mm_MKPF_MSEG as
select MSEG_MBLNR dd_MaterialDocNo, MSEG_ZEILE dd_MaterialDocItemNo, MSEG_MJAHR dd_MaterialDocYear
from MKPF_MSEG;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_del;
CREATE TABLE tmp_mm_MKPF_MSEG_del
as 
select * from tmp_mm_MKPF_MSEG where 1=2;
insert into tmp_mm_MKPF_MSEG_del
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
from fact_materialmovement ;

merge into tmp_mm_MKPF_MSEG dst
using (select distinct t1.rowid rid
	  from tmp_mm_MKPF_MSEG t1
				inner join tmp_mm_MKPF_MSEG_del t2 on ifnull(t1.dd_MaterialDocNo,'Not Set') = ifnull(t2.dd_MaterialDocNo,'Not Set')
                                                  and ifnull(t1.dd_MaterialDocItemNo,-1) = ifnull(t2.dd_MaterialDocItemNo,-1)
                                                  and ifnull(t1.dd_MaterialDocYear,-1) = ifnull(t2.dd_MaterialDocYear,-1)) src
on dst.rowid = src.rid
when matched then delete;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_allcols;
CREATE TABLE tmp_mm_MKPF_MSEG_allcols as
SELECT distinct m.*
FROM MKPF_MSEG m,tmp_mm_MKPF_MSEG m1
where m.MSEG_MBLNR = m1.dd_MaterialDocNo
and m.MSEG_ZEILE = m1.dd_MaterialDocItemNo
and m.MSEG_MJAHR = m1.dd_MaterialDocYear;


drop table if exists tmp_fmatmov_t003t;
create table tmp_fmatmov_t003t as
   select max_holder_789.maxid + row_number() over(order by '') fact_materialmovementid,
		  /*hash(concat(m.MSEG_MBLNR,m.MSEG_ZEILE,m.MSEG_MJAHR)) fact_materialmovementid,*/
		  bb.* 
		  from max_holder_789,(SELECT DISTINCT
          m.MSEG_MBLNR dd_MaterialDocNo,
          m.MSEG_ZEILE dd_MaterialDocItemNo,
          m.MSEG_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG_AUFNR,'Not Set') dd_productionordernumber, 
          ifnull(m.MSEG_AUFPS,1) dd_productionorderitemno,
          m.MSEG_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG_CHARG, 'Not Set') dd_BatchNumber,
          ifnull(m.MSEG_BWTAR,'Not Set') dd_ValuationType,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR amt_LocalCurrAmt,
          ifnull(m.MSEG_DMBTR,0) / (case when m.MSEG_ERFMG =0 then 1 else  m.MSEG_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT amt_AltPriceControl,
          ifnull(m.MSEG_SALK3,0) amt_OnHand_ValStock,
	  convert( decimal (18,4),1) amt_ExchangeRate,
	  convert( decimal (18,4),1)  amt_ExchangeRate_GBL, m.MSEG_MENGE ct_Quantity,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE ct_QuantityDC,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG ct_QtyGROrdUnit,
          ifnull(m.MSEG_LBKUM,0) ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG ct_QtyOrdPriceUnit,
          convert(bigint, 1) Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          convert(bigint, 1) Dim_Currencyid,	/* Local curr */
	 convert(bigint, 1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
	  convert(bigint, 1) Dim_StorageLocationid,
    convert(bigint, 1) Dim_Vendorid,
    convert(bigint, 1) dim_DateidMaterialDocDate,
    convert(bigint, 1) dim_DateIDPostingDate,
	convert(bigint, 1) dim_producthierarchyid,
   convert(bigint, 1) dim_MovementIndicatorid,
    convert(bigint, 1) dim_Customerid,
	convert(bigint, 1) dim_CostCenterid,
    convert(bigint, 1) dim_specialstockid,
    convert(bigint, 1) dim_unitofmeasureid,
    convert(bigint, 1) dim_controllingareaid,
    convert(bigint, 1) Dim_ProfitCenterId,
    convert(bigint, 1) dim_consumptiontypeid,
    convert(bigint, 1) dim_stocktypeid,
	convert(bigint, 1) Dim_PurchaseGroupid,
     convert(bigint, 1) Dim_materialgroupid,
     convert(bigint, 1) Dim_ReceivingPlantId,
     convert(bigint, 1) Dim_ReceivingPartId,
	convert(bigint, 1) Dim_RecvIssuStorLocid,
         convert(bigint, 1) Dim_Currencyid_TRA,								
          convert(bigint, 1) Dim_Currencyid_GBL,
				'N' as lotsreceivedflag,
     m.MSEG_LIFNR,  --Dim_Vendorid
m.MSEG_BUKRS, --dim_DateIDMaterialDocDate
m.MKPF_BLDAT,  --dim_DateIDMaterialDocDate
m.MKPF_BUDAT,  --dim_DateIDPostingDate
m.MSEG_MATNR,   --dim_producthierarchyid
m.MSEG_WERKS,    --dim_producthierarchyid
m.MSEG_KZBEW,    -- dim_MovementIndicatorid
m.MSEG_KUNNR,    --dim_Customerid
m.MSEG_SOBKZ, --dim_specialstockid
m.MSEG_MEINS, --dim_unitofmeasureid
m.MSEG_KOKRS, --dim_controllingareaid
m.MSEG_KZVBR, --dim_consumptiontypeid
m.MSEG_INSMK, --dim_stocktypeid
m.MSEG_UMMAT,  --Dim_ReceivingPartId
m.MSEG_UMLGO,  --Dim_RecvIssuStorLocid
m.MSEG_UMWRK,  --Dim_RecvIssuStorLocid
m.MSEG_WAERS,  --Dim_Currencyid_TRA
pGlobalCurrency --Dim_Currencyid_GBL
      FROM pGlobalCurrency_tbl, tmp_mm_MKPF_MSEG_allcols m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode ) bb;

update tmp_fmatmov_t003t f
set   f.Dim_Vendorid = ifnull(dv.Dim_Vendorid,1)
from tmp_fmatmov_t003t f
             left join Dim_Vendor dv
            on dv.VendorNumber = f.MSEG_LIFNR
where f.Dim_Vendorid <> ifnull(dv.Dim_Vendorid,1);

update tmp_fmatmov_t003t f
set   f.dim_DateIDMaterialDocDate = ifnull(mdd.Dim_Dateid,1)
from tmp_fmatmov_t003t f
             left join dim_date mdd  on
              mdd.CompanyCode = f.MSEG_BUKRS AND f.MKPF_BLDAT = mdd.DateValue
			  and f.MSEG_WERKS = mdd.plantcode_factory
where  f.dim_DateIDMaterialDocDate <> ifnull(mdd.Dim_Dateid,1);

update tmp_fmatmov_t003t f
set   f.dim_DateIDPostingDate = ifnull(pd.Dim_Dateid,1)
from tmp_fmatmov_t003t f
             left join dim_date pd  on
             pd.CompanyCode = f.MSEG_BUKRS AND f.MKPF_BUDAT = pd.DateValue
			 and f.MSEG_WERKS = pd.plantcode_factory
where  f.dim_DateIDPostingDate <> ifnull(pd.Dim_Dateid,1);

UPDATE tmp_fmatmov_t003t ms
  SET     ms.dim_producthierarchyid = ifnull(case WHEN ms.MSEG_MATNR IS NULL then 1 else dpr1.dim_producthierarchyid end, 1)
        from tmp_fmatmov_t003t ms 
                 left join (SELECT dpr.PartNumber,dpr.Plant,dph.dim_producthierarchyid
                           FROM dim_producthierarchy dph, dim_part dpr
                           WHERE     dph.ProductHierarchy = dpr.ProductHierarchy) dpr1
                        on dpr1.PartNumber = ms.MSEG_MATNR
                         AND dpr1.Plant = ms.MSEG_WERKS
 where   ms.dim_producthierarchyid = ifnull(case WHEN ms.MSEG_MATNR IS NULL then 1 else dpr1.dim_producthierarchyid end, 1);    

update tmp_fmatmov_t003t f
set   f.Dim_MovementIndicatorid = ifnull(dmi.Dim_MovementIndicatorid,1)
from tmp_fmatmov_t003t f
             left join dim_movementindicator dmi  on
             dmi.TypeCode = f.MSEG_KZBEW
where  f.Dim_MovementIndicatorid <> ifnull(dmi.Dim_MovementIndicatorid,1);

update tmp_fmatmov_t003t f
set   f.dim_Customerid = ifnull(dcu.dim_Customerid,1)
from tmp_fmatmov_t003t f
             left join dim_customer dcu  on
             dcu.CustomerNumber = f.MSEG_KUNNR
where  f.dim_Customerid <> ifnull(dcu.dim_Customerid,1);


update tmp_fmatmov_t003t f
set   f.Dim_specialstockid = ifnull(spt.dim_specialstockid,1)
from tmp_fmatmov_t003t f
             left join dim_specialstock spt  on
             spt.specialstockindicator = f.MSEG_SOBKZ
where  f.Dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

update tmp_fmatmov_t003t f
set   f.Dim_unitofmeasureid = ifnull(uom.Dim_unitofmeasureid,1)
from tmp_fmatmov_t003t f
             left join dim_unitofmeasure uom  on
             uom.UOM = f.MSEG_MEINS
where  f.Dim_unitofmeasureid <> ifnull(uom.Dim_unitofmeasureid,1);

update tmp_fmatmov_t003t f
set   f.Dim_controllingareaid = ifnull(ca.Dim_controllingareaid,1)
from tmp_fmatmov_t003t f
             left join dim_controllingarea ca  on
             ca.ControllingAreaCode = f.MSEG_KOKRS
where f.Dim_controllingareaid <> ifnull(ca.Dim_controllingareaid,1);

update tmp_fmatmov_t003t f
set   f.Dim_consumptiontypeid = ifnull(ct.Dim_consumptiontypeid,1)
from tmp_fmatmov_t003t f
             left join dim_consumptiontype ct  on
             ct.ConsumptionCode = f.MSEG_KZVBR
where f.Dim_consumptiontypeid <> ifnull(ct.Dim_consumptiontypeid,1);

update tmp_fmatmov_t003t f
set   f.Dim_stocktypeid = ifnull(st.Dim_StockTypeid,1)
from tmp_fmatmov_t003t f
             left join dim_stocktype st  on
             st.TypeCode = f.MSEG_INSMK
where  f.Dim_stocktypeid <> ifnull(st.Dim_StockTypeid,1);

update tmp_fmatmov_t003t f
set   f.Dim_PurchaseGroupid = ifnull(case when  f.MSEG_MATNR IS NULL then 1 else  pg1.Dim_PurchaseGroupid end ,1)
from tmp_fmatmov_t003t f
             left join 
          (select dpr.PartNumber,dpr.Plant,pg.Dim_PurchaseGroupid FROM dim_part dpr 
              inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup) pg1
         on pg1.PartNumber = f.MSEG_MATNR AND pg1.Plant = f.MSEG_WERKS
where  f.Dim_PurchaseGroupid <> ifnull(case when  f.MSEG_MATNR IS NULL then 1 else  pg1.Dim_PurchaseGroupid end ,1);

update tmp_fmatmov_t003t f
set   f.Dim_materialgroupid = ifnull(mg1.Dim_materialgroupid,1)
from tmp_fmatmov_t003t f
             left join 
          (select dpr.PartNumber,dpr.Plant,mg.Dim_materialgroupid FROM dim_part dpr 
              inner join dim_materialgroup mg on mg.MaterialGroupCode = dpr.MaterialGroup) mg1
         on mg1.PartNumber = f.MSEG_MATNR AND mg1.Plant = f.MSEG_WERKS
where  f.Dim_materialgroupid <> ifnull(mg1.Dim_materialgroupid,1);

update tmp_fmatmov_t003t f
set   f.Dim_ReceivingPlantId = ifnull(pl.dim_plantid,1)
from tmp_fmatmov_t003t  f
             left join dim_plant pl  on
             pl.PlantCode = f.MSEG_UMWRK
where f.Dim_ReceivingPlantId <> ifnull(pl.dim_plantid,1);

update tmp_fmatmov_t003t f
set   f.Dim_ReceivingPartId = ifnull(prt.Dim_Partid,1)
from tmp_fmatmov_t003t f
             left join dim_part prt
            on prt.Plant = f.MSEG_UMWRK AND prt.PartNumber = f.MSEG_UMMAT
where f.Dim_ReceivingPartId <> ifnull(prt.Dim_Partid,1);

update tmp_fmatmov_t003t f
set   f.Dim_RecvIssuStorLocid = ifnull((case when  f.MSEG_UMLGO IS NULL then 1 else dsl.Dim_StorageLocationid end),1)
from tmp_fmatmov_t003t f
             left join dim_storagelocation dsl
            on dsl.LocationCode = f.MSEG_UMLGO AND dsl.Plant = f.MSEG_UMWRK
where f.Dim_RecvIssuStorLocid <> ifnull((case when  f.MSEG_UMLGO IS NULL then 1 else dsl.Dim_StorageLocationid end),1);

update tmp_fmatmov_t003t f
set   f.Dim_CurrencyId_TRA = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t003t f
             left join dim_currency dcr
            on dcr.CurrencyCode = f.MSEG_WAERS
where f.Dim_CurrencyId_TRA <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t003t f
set   f.Dim_CurrencyId_GBL = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t003t f
             left join dim_currency dcr
            on dcr.CurrencyCode = pGlobalCurrency
where f.Dim_CurrencyId_GBL <> ifnull(dcr.Dim_Currencyid,1);


INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
			       ct_QuantityDC,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,                   
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
							   dim_Currencyid_TRA,
							   dim_Currencyid_GBL,
lotsreceivedflag)
select fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                               ifnull(dd_ReferenceDocNo,'Not Set') as dd_ReferenceDocNo,
                               ifnull(dd_ReferenceDocItem, 0) as dd_ReferenceDocItem,
                               ifnull(dd_debitcreditid,'Not Set') as dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               ifnull(dd_GoodsMoveReason, 0) as dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
			       ct_QuantityDC,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               ifnull(Dim_MovementTypeid, 1) as Dim_MovementTypeid,
                               ifnull(dim_Companyid, 1) as dim_Companyid,
                               ifnull(Dim_Currencyid, 1) as Dim_Currencyid,
                               ifnull(Dim_Partid,1) as Dim_Partid,
                               ifnull(Dim_Plantid, 1) as Dim_Plantid,
                               ifnull(Dim_StorageLocationid, 1) as Dim_StorageLocationid,
                               ifnull(Dim_Vendorid, 1) as Dim_Vendorid,
                               ifnull(dim_DateIDMaterialDocDate, 1) as dim_DateIDMaterialDocDate,
                               ifnull(dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
                               ifnull(dim_producthierarchyid, 1) as dim_producthierarchyid,
                               ifnull(dim_MovementIndicatorid, 1) as dim_MovementIndicatorid,
                               ifnull(dim_Customerid, 1) as dim_Customerid,
                               ifnull(dim_CostCenterid, 1) as dim_CostCenterid,
                               ifnull(dim_specialstockid, 1) as dim_specialstockid,
                               ifnull(dim_unitofmeasureid, 1) as dim_unitofmeasureid,
                               ifnull(dim_controllingareaid, 1) as dim_controllingareaid,
                               ifnull(dim_profitcenterid,      1) as dim_profitcenterid,                   
                               ifnull(dim_consumptiontypeid, 1) as dim_consumptiontypeid,
                               ifnull(dim_stocktypeid, 1) as dim_stocktypeid,
                               ifnull(Dim_PurchaseGroupid, 1) as Dim_PurchaseGroupid,
                               ifnull(dim_materialgroupid, 1) as dim_materialgroupid,
                               ifnull(Dim_ReceivingPlantId, 1) as Dim_ReceivingPlantId,
                               ifnull(Dim_ReceivingPartId, 1) as Dim_ReceivingPartId,
                               ifnull(Dim_RecvIssuStorLocid, 1) as Dim_RecvIssuStorLocid,
							   dim_Currencyid_TRA,
							   dim_Currencyid_GBL,
lotsreceivedflag
from tmp_fmatmov_t003t;

/*Georgiana added durring validation*/
DROP TABLE IF EXISTS TMP_UPDT_MM_MKPF_MSEG_ALLCOLS;
CREATE TABLE TMP_UPDT_MM_MKPF_MSEG_ALLCOLS
AS
SELECT m.*,row_number() over(partition by MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR order by MKPF_AEDAT desc) rono
FROM tmp_mm_MKPF_MSEG_allcols m;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_StdUnitPrice = 0
FROM TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1) 
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG_ERFMG = 0
AND m.rono = 1
AND ms.amt_StdUnitPrice <> 0;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_currencyid = ifnull(dcr.dim_currencyid,1)
FROM TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc, dim_currency dcr, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dcr.CurrencyCode = dc.currency
AND m.rono = 1
AND ms.dim_currencyid <> ifnull(dcr.dim_currencyid,1);

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_partid = ifnull(dpr.dim_partid,1)
FROM TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc,dim_part dpr, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dpr.PartNumber = m.MSEG_MATNR
AND dpr.Plant = m.MSEG_WERKS
AND m.rono = 1
AND ms.dim_partid <> ifnull(dpr.dim_partid,1);



UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_storagelocationid = dsl.dim_storagelocationid
FROM TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc,dim_storagelocation dsl, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND dsl.LocationCode = m.MSEG_LGORT AND dsl.Plant = m.MSEG_WERKS
AND m.rono = 1
AND ms.dim_storagelocationid <> dsl.dim_storagelocationid;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_exchangerate = z.exchangerate
FROM dim_currency tra, dim_currency lcl,dim_date d, tmp_getExchangeRate1 z, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_currencyid_tra,1) = ifnull(tra.dim_currencyid,1)
AND ifnull(ms.dim_currencyid,1) = ifnull(lcl.dim_currencyid,1)
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = lcl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact' 
AND ms.amt_exchangerate <> z.exchangerate;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_exchangerate_gbl = z.exchangerate
FROM dim_currency tra, dim_currency gbl,dim_date d, tmp_getExchangeRate1 z,
tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_currencyid_tra,1) = ifnull(tra.dim_currencyid,1)
AND ifnull(ms.dim_currencyid_gbl,1) = ifnull(gbl.dim_currencyid,1)
AND ms.dim_DateIDPostingDate = d.dim_dateid
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = gbl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate_gbl <> z.exchangerate;


UPDATE tmp_fact_materialmovement_perf1 ms
SET Dim_MovementTypeid = ifnull(mt.Dim_MovementTypeid,1)
FROM dim_movementtype mt, TMP_UPDT_MM_MKPF_MSEG_ALLCOLS m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG_WERKS = dp.PlantCode
AND  m.MSEG_BUKRS = dc.CompanyCode
AND mt.MovementType = m.MSEG_BWART 
AND mt.ConsumptionIndicator = ifnull(m.MSEG_KZVBR, 'Not Set') 
AND mt.MovementIndicator = ifnull(m.MSEG_KZBEW, 'Not Set') 
AND mt.ReceiptIndicator = ifnull(m.MSEG_KZZUG, 'Not Set') 
AND mt.SpecialStockIndicator = ifnull(m.MSEG_SOBKZ, 'Not Set')
AND m.rono = 1
AND ms.Dim_MovementTypeid <> ifnull(mt.Dim_MovementTypeid,1);

DROP TABLE IF EXISTS TMP_UPDT_MM_MKPF_MSEG_ALLCOLS;
/*GEorgiana End*/			 
/* Replaced NOT EXISTS with combine-insert*/		

		 
 /*   WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)) bb */

drop table if exists max_holder_789;

/*Georgiana Changes 01 Jul 2015*/
/*Update tmp_fact_materialmovement_perf1 fmt
From  MKPF_MSEG m
set Dim_ProfitCenterId =  ifnull((SELECT  pc.Dim_ProfitCenterid
		    FROM dim_profitcenter_789 pc
		   WHERE  pc.ControllingArea = MSEG_KOKRS
		      AND pc.ProfitCenterCode = MSEG_PRCTR
		      AND pc.ValidTo >= m.MKPF_BUDAT),1) 
	,fmt.dw_update_date = current_timestamp  Added automatically by update_dw_update_date.pl
    WHERE fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR*/

drop table if exists tmp2_MKPF_MSEG;
create table tmp2_MKPF_MSEG as select distinct * from MKPF_MSEG;

DROP TABLE IF EXISTS tmp1_MKPF_MSEG;
CREATE TABLE tmp1_MKPF_MSEG
AS
SELECT m.*, ROW_NUMBER() OVER(PARTITION BY MSEG_MBLNR, MSEG_ZEILE, MSEG_MJAHR ORDER BY MKPF_AEDAT DESC) rono
FROM tmp2_MKPF_MSEG m;

drop table if exists tmp2_MKPF_MSEG;

/*replaced update with merge due to unable to get a stable set of rows*/
merge into tmp_fact_materialmovement_perf1 f
using
    (select  distinct fact_materialmovementid, ifnull(pc.Dim_ProfitCenterid,1) as Dim_ProfitCenterid
    From tmp1_MKPF_MSEG m
    INNER JOIN dim_plant dp
    ON m.MSEG_WERKS = dp.PlantCode
    INNER JOIN dim_company dc
    ON m.MSEG_BUKRS = dc.CompanyCode
    inner join tmp_fact_materialmovement_perf1 fmt
    on fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
    AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
    AND fmt.dd_MaterialDocNo = m.MSEG_MBLNR 
    and m.rono = 1
    left join dim_profitcenter_789 pc
    on pc.ControllingArea = m.MSEG_KOKRS
    AND pc.ProfitCenterCode = m.MSEG_PRCTR
    AND pc.ValidTo >= m.MKPF_BUDAT) t
on t.fact_materialmovementid = f.fact_materialmovementid
when matched then update set
f.Dim_ProfitCenterid = t.Dim_ProfitCenterid
where f.Dim_ProfitCenterid <> t.Dim_ProfitCenterid;

/*End Changes 01 Jul 2015*/


/*Update tmp_fact_materialmovement_perf1 fmt
From MKPF_MSEG m
set dim_CostCenterid =   ifnull((select  dim_CostCenterid
                          FROM dim_costcenter dcc
                          WHERE     dcc.Code = m.MSEG_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG_KOKRS = dcc.ControllingArea), 1)
          ,fmt.dw_update_date = current_timestamp  
    WHERE fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR*/

/*drop table if exists tmp_costcenter_upd
create table tmp_costcenter_upd as
select fmt.dd_MaterialDocNo,
	   fmt.dd_MaterialDocItemNo,
	   fmt.dd_MaterialDocYear,
       dcc.Dim_CostCenterid Dim_CostCenterid,
       row_number() over (partition by fmt.dd_MaterialDocNo, fmt.dd_MaterialDocItemNo order by validto desc) as rownum  
from tmp_fact_materialmovement_perf1 fmt,
MKPF_MSEG m,
dim_costcenter dcc
WHERE fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR
and dcc.Code = m.MSEG_KOSTL
AND dcc.validTo >= MKPF_BUDAT
AND m.MSEG_KOKRS = dcc.ControllingArea
and dcc.RowIsCurrent = 1*/

/*update tmp_fact_materialmovement_perf1 fmt
from tmp_costcenter_upd tmp
SET fmt.Dim_CostCenterid = ifnull(tmp.Dim_CostCenterid,1)
WHERE fmt.dd_MaterialDocNo = tmp.dd_MaterialDocNo
	 and  fmt.dd_MaterialDocItemNo = tmp.dd_MaterialDocItemNo
	 and  fmt.dd_MaterialDocYear = tmp.dd_MaterialDocYear
     and tmp.rownum = 1
drop table if exists tmp_costcenter_upd*/

/*Georgiana Changes 01 Jul 2015*/
Update tmp_fact_materialmovement_perf1 fmt
set fmt.dim_CostCenterid = ifnull(case  WHEN m.MSEG_KOSTL IS NULL then 1 else dcc1.dim_CostCenterid end,1)
From tmp1_MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
          inner join tmp_fact_materialmovement_perf1 fmt
    on  fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
        AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
        AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR and m.rono = 1
           left join (select dcc.Code,dcc.ControllingArea,min(dcc.dim_CostCenterid) as dim_CostCenterid from MKPF_MSEG m 
inner join dim_costcenter dcc
    on   dcc.Code = m.MSEG_KOSTL
       AND m.MSEG_KOKRS = dcc.ControllingArea
    where dcc.validTo >= m.MKPF_BUDAT
    group by dcc.Code,dcc.ControllingArea) dcc1
     on   dcc1.Code = m.MSEG_KOSTL
       AND m.MSEG_KOKRS = dcc1.ControllingArea
where fmt.dim_CostCenterid <> ifnull(case  WHEN m.MSEG_KOSTL IS NULL then 1 else dcc1.dim_CostCenterid end,1);
/*End Chamges 01 Jul 2015*/

insert into  fact_materialmovement
(LOTSACCEPTEDFLAG,
LOTSREJECTEDFLAG,
PERCENTLOTSINSPECTEDFLAG,
PERCENTLOTSREJECTEDFLAG,
PERCENTLOTSACCEPTEDFLAG,
LOTSSKIPPEDFLAG,
LOTSAWAITINGINSPECTIONFLAG,
LOTSINSPECTEDFLAG,
LOTSRECEIVEDFLAG,
DD_MATERIALDOCNO,
DD_DOCUMENTNO,
DD_SALESORDERNO,
DD_GLACCOUNTNO,
DD_PRODUCTIONORDERNUMBER,
DD_BATCHNUMBER,
DD_VALUATIONTYPE,
DD_DEBITCREDITID,
DD_INCOTERMS2,
DD_INTORDER,
DD_REFERENCEDOCNO,
DD_INSPECTIONSTATUSINGRDOC,
DD_ITEMAUTOMATICALLYCREATED,
DD_MATERIALDOCITEMNO,
DD_MATERIALDOCYEAR,
DD_GOODSMOVEREASON,
AMT_LOCALCURRAMT,
AMT_DELIVERYCOST,
AMT_ALTPRICECONTROL,
AMT_ONHAND_VALSTOCK,
CT_QUANTITY,
CT_QUANTITYDC,
CT_QTYENTRYUOM,
CT_QTYGRORDUNIT,
CT_QTYONHAND_VALSTOCK,
CT_QTYORDPRICEUNIT,
DIRTYROW,
DD_CONSIGNMENTFLAG,
AMT_POUNITPRICE,
AMT_STDUNITPRICE,
AMT_PLANNEDPRICE,
AMT_PLANNEDPRICE1,
DD_DOCUMENTSCHEDULENO,
CT_ORDERQUANTITY,
DD_REFERENCEDOCITEM,
QTYREJECTEDEXTERNALAMT,
QTYREJECTEDINTERNALAMT,
DD_VENDORSPENDFLAG,
DD_RESERVATIONNUMBER,
DIM_MOVEMENTTYPEID,
DIM_COMPANYID,
DIM_CURRENCYID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_VENDORID,
DIM_DATEIDMATERIALDOCDATE,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCCREATION,
DIM_DATEIDORDERED,
DIM_PRODUCTHIERARCHYID,
DIM_MOVEMENTINDICATORID,
DIM_CUSTOMERID,
DIM_COSTCENTERID,
DIM_ACCOUNTCATEGORYID,
DIM_CONSUMPTIONTYPEID,
DIM_CONTROLLINGAREAID,
DIM_CUSTOMERGROUP1ID,
DIM_DOCUMENTCATEGORYID,
DIM_DOCUMENTTYPEID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEORGID,
DIM_SALESDOCUMENTTYPEID,
DIM_SALESGROUPID,
DIM_SALESORDERHEADERSTATUSID,
DIM_SALESORDERITEMSTATUSID,
DIM_SALESORDERREJECTREASONID,
DIM_SALESORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKTYPEID,
DIM_UNITOFMEASUREID,
DIM_MATERIALGROUPID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_TERMID,
DIM_PROFITCENTERID,
DIM_RECEIVINGPLANTID,
DIM_RECEIVINGPARTID,
DIM_DATEIDCOSTING,
DIM_INCOTERMID,
DIM_DATEIDDELIVERY,
DIM_DATEIDSTATDELIVERY,
DIM_GRSTATUSID,
DIM_RECVISSUSTORLOCID,
DIM_POPLANTIDORDERING,
DIM_POPLANTIDSUPPLYING,
DIM_POSTORAGELOCID,
DIM_POISSUSTORAGELOCID,
DIM_INSPUSAGEDECISIONID,
DIM_AFSSIZEID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DIM_UNITOFMEASUREORDERUNITID,
DIM_UOMUNITOFENTRYID,
DIM_PROJECTSOURCEID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_DOCUMENTITEMNO,
DD_SALESORDERITEMNO,
DD_SALESORDERDLVRNO,
DD_PRODUCTIONORDERITEMNO,
CT_LOTNOTTHRUQM,
AMT_VENDORSPEND,
FACT_MATERIALMOVEMENTID) 
select IFNULL(ftmp.LOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSSKIPPEDFLAG,'N'),
IFNULL(ftmp.LOTSAWAITINGINSPECTIONFLAG,'N'),
IFNULL(ftmp.LOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.LOTSRECEIVEDFLAG,'N'),
IFNULL(ftmp.DD_MATERIALDOCNO,'Not Set'),
IFNULL(ftmp.DD_DOCUMENTNO,'Not Set'),
IFNULL(ftmp.DD_SALESORDERNO,'Not Set'),
IFNULL(ftmp.DD_GLACCOUNTNO,'Not Set'),
IFNULL(ftmp.DD_PRODUCTIONORDERNUMBER,'Not Set'),
IFNULL(ftmp.DD_BATCHNUMBER,'Not Set'),
IFNULL(ftmp.DD_VALUATIONTYPE,'Not Set'),
IFNULL(ftmp.DD_DEBITCREDITID,'Not Set'),
IFNULL(ftmp.DD_INCOTERMS2,'Not Set'),
IFNULL(ftmp.DD_INTORDER,'Not Set'),
IFNULL(ftmp.DD_REFERENCEDOCNO,'Not Set'),
IFNULL(ftmp.DD_INSPECTIONSTATUSINGRDOC,'Not Set'),
IFNULL(ftmp.DD_ITEMAUTOMATICALLYCREATED,'Not Set'),
IFNULL(ftmp.DD_MATERIALDOCITEMNO,0),
IFNULL(ftmp.DD_MATERIALDOCYEAR,0),
IFNULL(ftmp.DD_GOODSMOVEREASON,0),
IFNULL(ftmp.AMT_LOCALCURRAMT,0),
IFNULL(ftmp.AMT_DELIVERYCOST,0),
IFNULL(ftmp.AMT_ALTPRICECONTROL,0),
IFNULL(ftmp.AMT_ONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QUANTITY,0),
IFNULL(ftmp.CT_QUANTITYDC,0),
IFNULL(ftmp.CT_QTYENTRYUOM,0),
IFNULL(ftmp.CT_QTYGRORDUNIT,0),
IFNULL(ftmp.CT_QTYONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QTYORDPRICEUNIT,0),
IFNULL(ftmp.DIRTYROW,0),
IFNULL(ftmp.DD_CONSIGNMENTFLAG,0),
IFNULL(ftmp.AMT_POUNITPRICE,0),
IFNULL(ftmp.AMT_STDUNITPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE1,0),
IFNULL(ftmp.DD_DOCUMENTSCHEDULENO,0),
IFNULL(ftmp.CT_ORDERQUANTITY,0),
IFNULL(ftmp.DD_REFERENCEDOCITEM,0),
IFNULL(ftmp.QTYREJECTEDEXTERNALAMT,0),
IFNULL(ftmp.QTYREJECTEDINTERNALAMT,0),
IFNULL(ftmp.DD_VENDORSPENDFLAG,0),
IFNULL(ftmp.DD_RESERVATIONNUMBER,0),
IFNULL(ftmp.DIM_MOVEMENTTYPEID,1),
IFNULL(ftmp.DIM_COMPANYID,1),
IFNULL(ftmp.DIM_CURRENCYID,1),
IFNULL(ftmp.DIM_PARTID,1),
IFNULL(ftmp.DIM_PLANTID,1),
IFNULL(ftmp.DIM_STORAGELOCATIONID,1),
IFNULL(ftmp.DIM_VENDORID,1),
IFNULL(ftmp.DIM_DATEIDMATERIALDOCDATE,1),
IFNULL(ftmp.DIM_DATEIDPOSTINGDATE,1),
IFNULL(ftmp.DIM_DATEIDDOCCREATION,1),
IFNULL(ftmp.DIM_DATEIDORDERED,1),
IFNULL(ftmp.DIM_PRODUCTHIERARCHYID,1),
IFNULL(ftmp.DIM_MOVEMENTINDICATORID,1),
IFNULL(ftmp.DIM_CUSTOMERID,1),
IFNULL(ftmp.DIM_COSTCENTERID,1),
IFNULL(ftmp.DIM_ACCOUNTCATEGORYID,1),
IFNULL(ftmp.DIM_CONSUMPTIONTYPEID,1),
IFNULL(ftmp.DIM_CONTROLLINGAREAID,1),
IFNULL(ftmp.DIM_CUSTOMERGROUP1ID,1),
IFNULL(ftmp.DIM_DOCUMENTCATEGORYID,1),
IFNULL(ftmp.DIM_DOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_ITEMCATEGORYID,1),
IFNULL(ftmp.DIM_ITEMSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERTYPEID,1),
IFNULL(ftmp.DIM_PURCHASEGROUPID,1),
IFNULL(ftmp.DIM_PURCHASEORGID,1),
IFNULL(ftmp.DIM_SALESDOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_SALESGROUPID,1),
IFNULL(ftmp.DIM_SALESORDERHEADERSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERITEMSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERREJECTREASONID,1),
IFNULL(ftmp.DIM_SALESORGID,1),
IFNULL(ftmp.DIM_SPECIALSTOCKID,1),
IFNULL(ftmp.DIM_STOCKTYPEID,1),
IFNULL(ftmp.DIM_UNITOFMEASUREID,1),
IFNULL(ftmp.DIM_MATERIALGROUPID,1),
IFNULL(ftmp.AMT_EXCHANGERATE_GBL,1),
IFNULL(ftmp.AMT_EXCHANGERATE,1),
IFNULL(ftmp.DIM_TERMID,1),
IFNULL(ftmp.DIM_PROFITCENTERID,1),
IFNULL(ftmp.DIM_RECEIVINGPLANTID,1),
IFNULL(ftmp.DIM_RECEIVINGPARTID,1),
IFNULL(ftmp.DIM_DATEIDCOSTING,1),
IFNULL(ftmp.DIM_INCOTERMID,1),
IFNULL(ftmp.DIM_DATEIDDELIVERY,1),
IFNULL(ftmp.DIM_DATEIDSTATDELIVERY,1),
IFNULL(ftmp.DIM_GRSTATUSID,1),
IFNULL(ftmp.DIM_RECVISSUSTORLOCID,1),
IFNULL(ftmp.DIM_POPLANTIDORDERING,1),
IFNULL(ftmp.DIM_POPLANTIDSUPPLYING,1),
IFNULL(ftmp.DIM_POSTORAGELOCID,1),
IFNULL(ftmp.DIM_POISSUSTORAGELOCID,1),
IFNULL(ftmp.DIM_INSPUSAGEDECISIONID,1),
IFNULL(ftmp.DIM_AFSSIZEID,1),
IFNULL(ftmp.DIM_CURRENCYID_TRA,1),
IFNULL(ftmp.DIM_CURRENCYID_GBL,1),
IFNULL(ftmp.DIM_UNITOFMEASUREORDERUNITID,1),
IFNULL(ftmp.DIM_UOMUNITOFENTRYID,1),
IFNULL(ftmp.DIM_PROJECTSOURCEID,1),
IFNULL(ftmp.DW_INSERT_DATE,CURRENT_TIMESTAMP),
IFNULL(ftmp.DW_UPDATE_DATE,CURRENT_TIMESTAMP),
ftmp.DD_DOCUMENTITEMNO,
ftmp.DD_SALESORDERITEMNO,
ftmp.DD_SALESORDERDLVRNO,
ftmp.DD_PRODUCTIONORDERITEMNO,
ftmp.CT_LOTNOTTHRUQM,
ftmp.AMT_VENDORSPEND,
ftmp.FACT_MATERIALMOVEMENTID
 from fact_materialmovement  f
right join tmp_fact_materialmovement_perf1 ftmp
on f.fact_materialmovementid = ftmp.fact_materialmovementid
where f.fact_materialmovementid is  null;	

/*Truncate perf table to be reused again*/
truncate table tmp_fact_materialmovement_perf1;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');

Drop table if exists max_holder_789;
create table max_holder_789
as
Select ifnull(max(fact_materialmovementid),ifnull((select DIM_PROJECTSOURCEID * MULTIPLIER from dim_projectsource),0)) as maxid
from fact_materialmovement;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1;
CREATE TABLE tmp_mm_MKPF_MSEG1 as
select MSEG1_MBLNR dd_MaterialDocNo, MSEG1_ZEILE dd_MaterialDocItemNo, MSEG1_MJAHR dd_MaterialDocYear
from MKPF_MSEG1;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_del;
CREATE TABLE tmp_mm_MKPF_MSEG1_del
as 
select * from tmp_mm_MKPF_MSEG1 where 1=2;
insert into tmp_mm_MKPF_MSEG1_del
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
From fact_materialmovement;

rename tmp_mm_MKPF_MSEG1 to tmp_mm_MKPF_MSEG1_1
;
create table tmp_mm_MKPF_MSEG1 as 
(select * from tmp_mm_MKPF_MSEG1_1) minus (Select * from tmp_mm_MKPF_MSEG1_del)
;
drop table tmp_mm_MKPF_MSEG1_1
;

DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_allcols;
CREATE TABLE tmp_mm_MKPF_MSEG1_allcols as
SELECT distinct m.*
FROM MKPF_MSEG1 m,tmp_mm_MKPF_MSEG1 m1
where m.MSEG1_MBLNR = m1.dd_MaterialDocNo
and m.MSEG1_ZEILE = m1.dd_MaterialDocItemNo
and m.MSEG1_MJAHR = m1.dd_MaterialDocYear;



drop table if exists tmp_fmatmov_t004t;
create table tmp_fmatmov_t004t as
   select max_holder_789.maxid + row_number() over(order by '') fact_materialmovementid,
		  /*hash(concat(m.MSEG_MBLNR,m.MSEG_ZEILE,m.MSEG_MJAHR)) fact_materialmovementid,*/
		  bb.* 
		  from max_holder_789,(SELECT DISTINCT
          m.MSEG1_MBLNR dd_MaterialDocNo,
          m.MSEG1_ZEILE dd_MaterialDocItemNo,
          m.MSEG1_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG1_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG1_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG1_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG1_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG1_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG1_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG1_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG1_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG1_AUFNR,'Not Set') dd_productionordernumber, 
          ifnull(m.MSEG1_AUFPS,1) dd_productionorderitemno,
          ifnull(m.MSEG1_GRUND,'Not Set') dd_GoodsMoveReason,
          ifnull(m.MSEG1_CHARG, 'Not Set') dd_BatchNumber,
          ifnull(m.MSEG1_BWTAR,'Not Set') dd_ValuationType,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR amt_LocalCurrAmt,
          ifnull(m.MSEG1_DMBTR,0) / (case when m.MSEG1_ERFMG =0 then 1 else  m.MSEG1_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BUALT amt_AltPriceControl,
          ifnull(m.MSEG1_SALK3,0) amt_OnHand_ValStock,
	  convert( decimal (18,4),1) amt_ExchangeRate,
	  convert( decimal (18,4),1) amt_ExchangeRate_GBL, m.MSEG1_MENGE ct_Quantity,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_MENGE ct_QuantityDC,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BSTMG ct_QtyGROrdUnit,
          ifnull(m.MSEG1_LBKUM,0) ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BPMNG ct_QtyOrdPriceUnit,
          convert(bigint, 1) Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          convert(bigint, 1) Dim_Currencyid,	/* Local curr */
	 convert(bigint, 1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
	convert(bigint, 1) Dim_StorageLocationid,
    convert(bigint, 1) Dim_Vendorid,
    convert(bigint, 1) dim_DateidMaterialDocDate,
    convert(bigint, 1) dim_DateIDPostingDate,
	convert(bigint, 1) dim_producthierarchyid,
   convert(bigint, 1) dim_MovementIndicatorid,
    convert(bigint, 1) dim_Customerid,
	convert(bigint, 1) dim_CostCenterid,
    convert(bigint, 1) dim_specialstockid,
    convert(bigint, 1) dim_unitofmeasureid,
    convert(bigint, 1) dim_controllingareaid,
    convert(bigint, 1) Dim_ProfitCenterId,
    convert(bigint, 1) dim_consumptiontypeid,
    convert(bigint, 1) dim_stocktypeid,
	convert(bigint, 1) Dim_PurchaseGroupid,
     convert(bigint, 1) Dim_materialgroupid,
     convert(bigint, 1) Dim_ReceivingPlantId,
     convert(bigint, 1) Dim_ReceivingPartId,
	convert(bigint, 1) Dim_RecvIssuStorLocid,
         convert(bigint, 1) Dim_Currencyid_TRA,								
          convert(bigint, 1) Dim_Currencyid_GBL,
	  convert(bigint, 1) DIM_DATEIDDOCCREATION,
				'N' as lotsreceivedflag,
     m.MSEG1_LIFNR,  --Dim_Vendorid
m.MSEG1_BUKRS, --dim_DateIDMaterialDocDate
m.MKPF_BLDAT,  --dim_DateIDMaterialDocDate
m.MKPF_BUDAT,  --dim_DateIDPostingDate
m.MSEG1_MATNR,   --dim_producthierarchyid
m.MSEG1_WERKS,    --dim_producthierarchyid
m.MSEG1_KZBEW,    -- dim_MovementIndicatorid
m.MSEG1_KUNNR,    --dim_Customerid
m.MSEG1_SOBKZ, --dim_specialstockid
m.MSEG1_MEINS, --dim_unitofmeasureid
m.MSEG1_KOKRS, --dim_controllingareaid
m.MSEG1_KZVBR, --dim_consumptiontypeid
m.MSEG1_INSMK, --dim_stocktypeid
m.MSEG1_UMMAT,  --Dim_ReceivingPartId
m.MSEG1_UMLGO,  --Dim_RecvIssuStorLocid
m.MSEG1_UMWRK,  --Dim_RecvIssuStorLocid
m.MSEG1_WAERS,  --Dim_Currencyid_TRA
pGlobalCurrency --Dim_Currencyid_GBL
      FROM pGlobalCurrency_tbl, tmp_mm_MKPF_MSEG1_allcols m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode ) bb;


update tmp_fmatmov_t004t f
set   f.Dim_Vendorid = ifnull(dv.Dim_Vendorid,1)
from tmp_fmatmov_t004t f
             left join Dim_Vendor dv
            on dv.VendorNumber = f.MSEG1_LIFNR
where f.Dim_Vendorid <> ifnull(dv.Dim_Vendorid,1);

update tmp_fmatmov_t004t f
set   f.dim_DateIDMaterialDocDate = ifnull(mdd.Dim_Dateid,1)
from tmp_fmatmov_t004t f
             left join dim_date mdd  on
              mdd.CompanyCode = f.MSEG1_BUKRS AND f.MKPF_BLDAT = mdd.DateValue
			  and f.MSEG1_WERKS = mdd.plantcode_factory
where  f.dim_DateIDMaterialDocDate <> ifnull(mdd.Dim_Dateid,1);

update tmp_fmatmov_t004t f
set   f.dim_DateIDPostingDate = ifnull(pd.Dim_Dateid,1)
from tmp_fmatmov_t004t f
             left join dim_date pd  on
             pd.CompanyCode = f.MSEG1_BUKRS AND f.MKPF_BUDAT = pd.DateValue
			 and f.MSEG1_WERKS = pd.plantcode_factory
where  f.dim_DateIDPostingDate <> ifnull(pd.Dim_Dateid,1);

UPDATE tmp_fmatmov_t004t ms
  SET     ms.dim_producthierarchyid = ifnull(case WHEN ms.MSEG1_MATNR IS NULL then 1 else dpr1.dim_producthierarchyid end, 1)
        from tmp_fmatmov_t004t ms 
                 left join (SELECT dpr.PartNumber,dpr.Plant,dph.dim_producthierarchyid
                           FROM dim_producthierarchy dph, dim_part dpr
                           WHERE     dph.ProductHierarchy = dpr.ProductHierarchy) dpr1
                        on dpr1.PartNumber = ms.MSEG1_MATNR
                         AND dpr1.Plant = ms.MSEG1_WERKS
 where   ms.dim_producthierarchyid = ifnull(case WHEN ms.MSEG1_MATNR IS NULL then 1 else dpr1.dim_producthierarchyid end, 1);    

update tmp_fmatmov_t004t f
set   f.Dim_MovementIndicatorid = ifnull(dmi.Dim_MovementIndicatorid,1)
from tmp_fmatmov_t004t f
             left join dim_movementindicator dmi  on
             dmi.TypeCode = f.MSEG1_KZBEW
where  f.Dim_MovementIndicatorid <> ifnull(dmi.Dim_MovementIndicatorid,1);

update tmp_fmatmov_t004t f
set   f.dim_Customerid = ifnull(dcu.dim_Customerid,1)
from tmp_fmatmov_t004t f
             left join dim_customer dcu  on
             dcu.CustomerNumber = f.MSEG1_KUNNR
where  f.dim_Customerid <> ifnull(dcu.dim_Customerid,1);


update tmp_fmatmov_t004t f
set   f.Dim_specialstockid = ifnull(spt.dim_specialstockid,1)
from tmp_fmatmov_t004t f
             left join dim_specialstock spt  on
             spt.specialstockindicator = f.MSEG1_SOBKZ
where  f.Dim_specialstockid <> ifnull(spt.dim_specialstockid,1);

update tmp_fmatmov_t004t f
set   f.Dim_unitofmeasureid = ifnull(uom.Dim_unitofmeasureid,1)
from tmp_fmatmov_t004t f
             left join dim_unitofmeasure uom  on
             uom.UOM = f.MSEG1_MEINS
where  f.Dim_unitofmeasureid <> ifnull(uom.Dim_unitofmeasureid,1);

update tmp_fmatmov_t004t f
set   f.Dim_controllingareaid = ifnull(ca.Dim_controllingareaid,1)
from tmp_fmatmov_t004t f
             left join dim_controllingarea ca  on
             ca.ControllingAreaCode = f.MSEG1_KOKRS
where f.Dim_controllingareaid <> ifnull(ca.Dim_controllingareaid,1);

update tmp_fmatmov_t004t f
set   f.Dim_consumptiontypeid = ifnull(ct.Dim_consumptiontypeid,1)
from tmp_fmatmov_t004t f
             left join dim_consumptiontype ct  on
             ct.ConsumptionCode = f.MSEG1_KZVBR
where f.Dim_consumptiontypeid <> ifnull(ct.Dim_consumptiontypeid,1);

update tmp_fmatmov_t004t f
set   f.Dim_stocktypeid = ifnull(st.Dim_StockTypeid,1)
from tmp_fmatmov_t004t f
             left join dim_stocktype st  on
             st.TypeCode = f.MSEG1_INSMK
where  f.Dim_stocktypeid <> ifnull(st.Dim_StockTypeid,1);

update tmp_fmatmov_t004t f
set   f.Dim_PurchaseGroupid = ifnull(case when  f.MSEG1_MATNR IS NULL then 1 else  pg1.Dim_PurchaseGroupid end ,1)
from tmp_fmatmov_t004t f
             left join 
          (select dpr.PartNumber,dpr.Plant,pg.Dim_PurchaseGroupid FROM dim_part dpr 
              inner join dim_purchasegroup pg on dpr.PurchaseGroupCode = pg.PurchaseGroup) pg1
         on pg1.PartNumber = f.MSEG1_MATNR AND pg1.Plant = f.MSEG1_WERKS
where  f.Dim_PurchaseGroupid <> ifnull(case when  f.MSEG1_MATNR IS NULL then 1 else  pg1.Dim_PurchaseGroupid end ,1);

update tmp_fmatmov_t004t f
set   f.Dim_materialgroupid = ifnull(mg1.Dim_materialgroupid,1)
from tmp_fmatmov_t004t f
             left join 
          (select dpr.PartNumber,dpr.Plant,mg.Dim_materialgroupid FROM dim_part dpr 
              inner join dim_materialgroup mg on mg.MaterialGroupCode = dpr.MaterialGroup) mg1
         on mg1.PartNumber = f.MSEG1_MATNR AND mg1.Plant = f.MSEG1_WERKS
where  f.Dim_materialgroupid <> ifnull(mg1.Dim_materialgroupid,1);

update tmp_fmatmov_t004t f
set   f.Dim_ReceivingPlantId = ifnull(pl.dim_plantid,1)
from tmp_fmatmov_t004t  f
             left join dim_plant pl  on
             pl.PlantCode = f.MSEG1_UMWRK
where f.Dim_ReceivingPlantId <> ifnull(pl.dim_plantid,1);

update tmp_fmatmov_t004t f
set   f.Dim_ReceivingPartId = ifnull(prt.Dim_Partid,1)
from tmp_fmatmov_t004t f
             left join dim_part prt
            on prt.Plant = f.MSEG1_UMWRK AND prt.PartNumber = f.MSEG1_UMMAT
where f.Dim_ReceivingPartId <> ifnull(prt.Dim_Partid,1);

update tmp_fmatmov_t004t f
set   f.Dim_RecvIssuStorLocid = ifnull((case when  f.MSEG1_UMLGO IS NULL then 1 else dsl.Dim_StorageLocationid end),1)
from tmp_fmatmov_t004t f
             left join dim_storagelocation dsl
            on dsl.LocationCode = f.MSEG1_UMLGO AND dsl.Plant = f.MSEG1_UMWRK
where f.Dim_RecvIssuStorLocid <> ifnull((case when  f.MSEG1_UMLGO IS NULL then 1 else dsl.Dim_StorageLocationid end),1);

update tmp_fmatmov_t004t f
set   f.Dim_CurrencyId_TRA = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t004t f
             left join dim_currency dcr
            on dcr.CurrencyCode = f.MSEG1_WAERS
where f.Dim_CurrencyId_TRA <> ifnull(dcr.Dim_Currencyid,1);

update tmp_fmatmov_t004t f
set   f.Dim_CurrencyId_GBL = ifnull(dcr.Dim_Currencyid,1)
from tmp_fmatmov_t004t f
             left join dim_currency dcr
            on dcr.CurrencyCode = pGlobalCurrency
where f.Dim_CurrencyId_GBL <> ifnull(dcr.Dim_Currencyid,1);


  INSERT INTO tmp_fact_materialmovement_perf1(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
			       ct_QuantityDC,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,Dim_Currencyid_TRA, Dim_Currencyid_GBL,
lotsreceivedflag)
select fact_materialmovementid,
              dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                               ifnull(dd_ReferenceDocNo, 'Not Set') as dd_ReferenceDocNo,
                               ifnull(dd_ReferenceDocItem, 'Not Set') as dd_ReferenceDocItem,
                               ifnull(dd_debitcreditid, 'Not Set') as dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               ifnull(dd_GoodsMoveReason, 0) as dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
			       ct_QuantityDC,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               ifnull(Dim_MovementTypeid, 1) as Dim_MovementTypeid,
                               ifnull(dim_Companyid, 1) as dim_Companyid,
                               ifnull(Dim_Currencyid, 1) as Dim_Currencyid,
                               ifnull(Dim_Partid,1) as Dim_Partid,
                               ifnull(Dim_Plantid, 1) as Dim_Plantid,
                               ifnull(Dim_StorageLocationid, 1) as Dim_StorageLocationid,
                               ifnull(Dim_Vendorid,1) as Dim_Vendorid,
                               ifnull(dim_DateIDMaterialDocDate,1) as dim_DateIDMaterialDocDate,
                               ifnull(dim_DateIDPostingDate, 1) as dim_DateIDPostingDate,
                               ifnull(dim_producthierarchyid, 1) as dim_producthierarchyid,
                               ifnull(dim_MovementIndicatorid, 1) as dim_MovementIndicatorid,
                               ifnull(dim_Customerid, 1) as dim_Customerid,
                               ifnull(dim_CostCenterid, 1) as dim_CostCenterid,
                               ifnull(dim_specialstockid, 1) as dim_specialstockid,
                               ifnull(dim_unitofmeasureid, 1) as dim_unitofmeasureid,
                               ifnull(dim_controllingareaid, 1) as dim_controllingareaid,
                               ifnull(dim_profitcenterid, 1) as dim_profitcenterid,
                               ifnull(dim_consumptiontypeid, 1) as dim_consumptiontypeid,
                               ifnull(dim_stocktypeid, 1) as dim_stocktypeid,
                               ifnull(Dim_PurchaseGroupid, 1) as Dim_PurchaseGroupid,
                               ifnull(dim_materialgroupid, 1) as dim_materialgroupid,
                               ifnull(Dim_ReceivingPlantId, 1) as Dim_ReceivingPlantId,
                               ifnull(Dim_ReceivingPartId, 1) as Dim_ReceivingPartId,
                               ifnull(Dim_RecvIssuStorLocid, 1) as Dim_RecvIssuStorLocid,
                               Dim_Currencyid_TRA, Dim_Currencyid_GBL,
lotsreceivedflag
from tmp_fmatmov_t004t;	 

/*Georgiana Added durring validation*/
UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_StdUnitPrice = 0
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_ERFMG = 0
AND ms.amt_StdUnitPrice <> 0;

/*UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_currencyid = dcr.dim_currencyid
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc, dim_currency dcr, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dcr.CurrencyCode = dc.currency
AND ms.dim_currencyid <> dcr.dim_currencyid
*/

drop table if exists tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT;
CREATE TABLE tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT
AS 
SELECT MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MAX(MKPF_AEDAT) AS MKPF_AEDAT_MAX
FROM tmp_mm_MKPF_MSEG1_allcols
GROUP BY MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_currencyid = ifnull(dcr.dim_currencyid,1)
FROM tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT m, dim_plant dp, dim_company dc, dim_currency dcr, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dcr.CurrencyCode = dc.currency
AND ms.dim_currencyid <> ifnull(dcr.dim_currencyid,1);

drop table if exists tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT;



/* UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_partid = dpr.dim_partid
FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc,dim_part dpr, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dpr.PartNumber = m.MSEG1_MATNR
AND dpr.Plant = m.MSEG1_WERKS
AND ms.dim_partid <> dpr.dim_partid
*/

drop table if exists tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT_part;
CREATE TABLE tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT_part
AS 
SELECT MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_WERKS,MSEG1_MATNR,MSEG1_MJAHR,MAX(MKPF_AEDAT) AS MKPF_AEDAT_MAX
FROM tmp_mm_MKPF_MSEG1_allcols
GROUP BY MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MSEG1_WERKS,MSEG1_MATNR;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_partid = ifnull(dpr.dim_partid,1)
FROM tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT_part m, dim_plant dp, dim_company dc,dim_part dpr, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dpr.PartNumber = m.MSEG1_MATNR
AND dpr.Plant = m.MSEG1_WERKS
AND ms.dim_partid <> ifnull(dpr.dim_partid,1);


/*UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.dim_storagelocationid = dsl.dim_storagelocationid
FROM tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT_part m, dim_plant dp, dim_company dc,dim_storagelocation dsl, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND dsl.LocationCode = m.MSEG1_LGORT AND dsl.Plant = m.MSEG1_WERKS
AND ms.dim_storagelocationid <> dsl.dim_storagelocationid*/

merge into  tmp_fact_materialmovement_perf1 ms
using (select distinct fact_materialmovementid, dsl.dim_storagelocationid
 FROM tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc,dim_storagelocation dsl, tmp_fact_materialmovement_perf1 ms
 WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
 AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
 AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
 AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
 AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
 AND dsl.LocationCode = m.MSEG1_LGORT AND dsl.Plant = m.MSEG1_WERKS
 AND ms.dim_storagelocationid <> dsl.dim_storagelocationid) t
on t.fact_materialmovementid=ms.fact_materialmovementid
when matched then update set ms.dim_storagelocationid = t.dim_storagelocationid;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_exchangerate = z.exchangerate
FROM dim_currency tra, dim_currency lcl,dim_date d, tmp_getExchangeRate1 z, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_currencyid_tra,1) = ifnull(tra.dim_currencyid,1)
AND ifnull(ms.dim_currencyid,1) = ifnull(lcl.dim_currencyid,1)
AND ifnull(ms.dim_DateIDPostingDate, 1) = ifnull(d.dim_dateid,1)
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = lcl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate <> z.exchangerate;

UPDATE tmp_fact_materialmovement_perf1 ms
SET ms.amt_exchangerate_gbl = z.exchangerate
FROM dim_currency tra, dim_currency gbl,dim_date d, tmp_getExchangeRate1 z, tmp_fact_materialmovement_perf1 ms
WHERE ifnull(ms.dim_currencyid_tra, 1) = ifnull(tra.dim_currencyid,1)
AND ifnull(ms.dim_currencyid_gbl,1) = ifnull(gbl.dim_currencyid,1)
AND ifnull(ms.dim_DateIDPostingDate, 1) = ifnull(d.dim_dateid,1)
AND z.pFromCurrency  = tra.currencycode AND z.pToCurrency = gbl.currencycode AND z.pDate = d.datevalue
AND z.fact_script_name = 'bi_populate_materialmovement_fact'
AND ms.amt_exchangerate_gbl <> z.exchangerate;



/*UPDATE tmp_fact_materialmovement_perf1 ms
SET Dim_MovementTypeid = mt.Dim_MovementTypeid
FROM dim_movementtype mt, tmp_mm_MKPF_MSEG1_allcols_MAX_MKPF_AEDAT_part m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
WHERE ms.dim_plantid = dp.dim_plantid
AND ms.dim_companyid = dc.dim_companyid
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_WERKS = dp.PlantCode
AND  m.MSEG1_BUKRS = dc.CompanyCode
AND mt.MovementType = m.MSEG1_BWART
AND mt.ConsumptionIndicator = ifnull(m.MSEG1_KZVBR, 'Not Set')
AND mt.MovementIndicator = ifnull(m.MSEG1_KZBEW, 'Not Set')
AND mt.ReceiptIndicator = ifnull(m.MSEG1_KZZUG, 'Not Set')
AND mt.SpecialStockIndicator = ifnull(m.MSEG1_SOBKZ, 'Not Set')
AND ms.Dim_MovementTypeid <> mt.Dim_MovementTypeid*/

merge into  tmp_fact_materialmovement_perf1 ms
using (select distinct fact_materialmovementid, ifnull(mt.Dim_MovementTypeid, 1) as Dim_MovementTypeid
 FROM dim_movementtype mt, tmp_mm_MKPF_MSEG1_allcols m, dim_plant dp, dim_company dc, tmp_fact_materialmovement_perf1 ms
 WHERE ifnull(ms.dim_plantid, 1) = ifnull(dp.dim_plantid, 1)
AND ifnull(ms.dim_companyid,1) = ifnull(dc.dim_companyid,1)
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND mt.MovementType = m.MSEG1_BWART
AND mt.ConsumptionIndicator = ifnull(m.MSEG1_KZVBR, 'Not Set')
AND mt.MovementIndicator = ifnull(m.MSEG1_KZBEW, 'Not Set')
AND mt.ReceiptIndicator = ifnull(m.MSEG1_KZZUG, 'Not Set')
AND mt.SpecialStockIndicator = ifnull(m.MSEG1_SOBKZ, 'Not Set')
AND ifnull(ms.Dim_MovementTypeid,1) <> ifnull(mt.Dim_MovementTypeid,1)) t
on t.fact_materialmovementid=ms.fact_materialmovementid
when matched then update set ms.Dim_MovementTypeid = t.Dim_MovementTypeid;

/*Georgiana End*/
drop table if exists max_holder_789;

merge into tmp_fact_materialmovement_perf1 fact
 USING (select a.* from 
(select fmt.fact_materialmovementid,pc.Dim_ProfitCenterid,row_number() over (partition by fact_materialmovementid order by pc.ValidTo) as RN
 From pGlobalCurrency_tbl, MKPF_MSEG1 m
 INNER JOIN dim_plant dp
 ON m.MSEG1_WERKS = dp.PlantCode
 INNER JOIN dim_company dc
 ON m.MSEG1_BUKRS = dc.CompanyCode
 inner join tmp_fact_materialmovement_perf1 fmt
 on fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
 AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear
 AND fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
 left join dim_profitcenter_789 pc
 on pc.ControllingArea = m.MSEG1_KOKRS
 AND pc.ProfitCenterCode = m.MSEG1_PRCTR
 AND pc.ValidTo >= m.MKPF_BUDAT
 where fmt.Dim_ProfitCenterId <> ifnull( pc.Dim_ProfitCenterid,1)) a
where a.RN=1) src
 ON fact.fact_materialmovementid = src.fact_materialmovementid
 WHEN MATCHED THEN UPDATE
 SET fact.Dim_ProfitCenterid = ifnull(src.Dim_ProfitCenterid, 1);

/*replaced update to merge because unable to get stable set of rows error */
merge into tmp_fact_materialmovement_perf1 f
using
(select  distinct fact_materialmovementid, ifnull(case WHEN m.MSEG1_KOSTL IS NULL then 1 else dcc1.dim_CostCenterid end,1) as dim_CostCenterid
From pGlobalCurrency_tbl, (select distinct MSEG1_ZEILE,MSEG1_MJAHR,MSEG1_MBLNR,MSEG1_WERKS,MSEG1_BUKRS,MSEG1_KOSTL,MSEG1_KOKRS,MKPF_BUDAT from MKPF_MSEG1) m
INNER JOIN dim_plant dp
ON m.MSEG1_WERKS = dp.PlantCode
INNER JOIN dim_company dc
ON m.MSEG1_BUKRS = dc.CompanyCode
inner join tmp_fact_materialmovement_perf1 fmt
on fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear
AND fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
left join (select dcc.Code,dcc.ControllingArea,min(dcc.dim_CostCenterid) as dim_CostCenterid from MKPF_MSEG1 m 
inner join dim_costcenter dcc
on dcc.Code = m.MSEG1_KOSTL
AND m.MSEG1_KOKRS = dcc.ControllingArea
where dcc.validTo >= m.MKPF_BUDAT
group by dcc.Code,dcc.ControllingArea) dcc1
on dcc1.Code = m.MSEG1_KOSTL
AND m.MSEG1_KOKRS = dcc1.ControllingArea) t
on t.fact_materialmovementid = f.fact_materialmovementid
when matched then update set
f.dim_CostCenterid = t.dim_CostCenterid
where f.dim_CostCenterid <> t.dim_CostCenterid;


/*Populate mm with data in tmp table. Note that this won't need any not exists as its already handled in combine before */
insert into  fact_materialmovement
(LOTSACCEPTEDFLAG,
LOTSREJECTEDFLAG,
PERCENTLOTSINSPECTEDFLAG,
PERCENTLOTSREJECTEDFLAG,
PERCENTLOTSACCEPTEDFLAG,
LOTSSKIPPEDFLAG,
LOTSAWAITINGINSPECTIONFLAG,
LOTSINSPECTEDFLAG,
LOTSRECEIVEDFLAG,
DD_MATERIALDOCNO,
DD_DOCUMENTNO,
DD_SALESORDERNO,
DD_GLACCOUNTNO,
DD_PRODUCTIONORDERNUMBER,
DD_BATCHNUMBER,
DD_VALUATIONTYPE,
DD_DEBITCREDITID,
DD_INCOTERMS2,
DD_INTORDER,
DD_REFERENCEDOCNO,
DD_INSPECTIONSTATUSINGRDOC,
DD_ITEMAUTOMATICALLYCREATED,
DD_MATERIALDOCITEMNO,
DD_MATERIALDOCYEAR,
DD_GOODSMOVEREASON,
AMT_LOCALCURRAMT,
AMT_DELIVERYCOST,
AMT_ALTPRICECONTROL,
AMT_ONHAND_VALSTOCK,
CT_QUANTITY,
CT_QUANTITYDC,
CT_QTYENTRYUOM,
CT_QTYGRORDUNIT,
CT_QTYONHAND_VALSTOCK,
CT_QTYORDPRICEUNIT,
DIRTYROW,
DD_CONSIGNMENTFLAG,
AMT_POUNITPRICE,
AMT_STDUNITPRICE,
AMT_PLANNEDPRICE,
AMT_PLANNEDPRICE1,
DD_DOCUMENTSCHEDULENO,
CT_ORDERQUANTITY,
DD_REFERENCEDOCITEM,
QTYREJECTEDEXTERNALAMT,
QTYREJECTEDINTERNALAMT,
DD_VENDORSPENDFLAG,
DD_RESERVATIONNUMBER,
DIM_MOVEMENTTYPEID,
DIM_COMPANYID,
DIM_CURRENCYID,
DIM_PARTID,
DIM_PLANTID,
DIM_STORAGELOCATIONID,
DIM_VENDORID,
DIM_DATEIDMATERIALDOCDATE,
DIM_DATEIDPOSTINGDATE,
DIM_DATEIDDOCCREATION,
DIM_DATEIDORDERED,
DIM_PRODUCTHIERARCHYID,
DIM_MOVEMENTINDICATORID,
DIM_CUSTOMERID,
DIM_COSTCENTERID,
DIM_ACCOUNTCATEGORYID,
DIM_CONSUMPTIONTYPEID,
DIM_CONTROLLINGAREAID,
DIM_CUSTOMERGROUP1ID,
DIM_DOCUMENTCATEGORYID,
DIM_DOCUMENTTYPEID,
DIM_ITEMCATEGORYID,
DIM_ITEMSTATUSID,
DIM_PRODUCTIONORDERSTATUSID,
DIM_PRODUCTIONORDERTYPEID,
DIM_PURCHASEGROUPID,
DIM_PURCHASEORGID,
DIM_SALESDOCUMENTTYPEID,
DIM_SALESGROUPID,
DIM_SALESORDERHEADERSTATUSID,
DIM_SALESORDERITEMSTATUSID,
DIM_SALESORDERREJECTREASONID,
DIM_SALESORGID,
DIM_SPECIALSTOCKID,
DIM_STOCKTYPEID,
DIM_UNITOFMEASUREID,
DIM_MATERIALGROUPID,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_TERMID,
DIM_PROFITCENTERID,
DIM_RECEIVINGPLANTID,
DIM_RECEIVINGPARTID,
DIM_DATEIDCOSTING,
DIM_INCOTERMID,
DIM_DATEIDDELIVERY,
DIM_DATEIDSTATDELIVERY,
DIM_GRSTATUSID,
DIM_RECVISSUSTORLOCID,
DIM_POPLANTIDORDERING,
DIM_POPLANTIDSUPPLYING,
DIM_POSTORAGELOCID,
DIM_POISSUSTORAGELOCID,
DIM_INSPUSAGEDECISIONID,
DIM_AFSSIZEID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DIM_UNITOFMEASUREORDERUNITID,
DIM_UOMUNITOFENTRYID,
DIM_PROJECTSOURCEID,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DD_DOCUMENTITEMNO,
DD_SALESORDERITEMNO,
DD_SALESORDERDLVRNO,
DD_PRODUCTIONORDERITEMNO,
CT_LOTNOTTHRUQM,
AMT_VENDORSPEND,
FACT_MATERIALMOVEMENTID) 
select IFNULL(ftmp.LOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSREJECTEDFLAG,'N'),
IFNULL(ftmp.PERCENTLOTSACCEPTEDFLAG,'N'),
IFNULL(ftmp.LOTSSKIPPEDFLAG,'N'),
IFNULL(ftmp.LOTSAWAITINGINSPECTIONFLAG,'N'),
IFNULL(ftmp.LOTSINSPECTEDFLAG,'N'),
IFNULL(ftmp.LOTSRECEIVEDFLAG,'N'),
IFNULL(ftmp.DD_MATERIALDOCNO,'Not Set'),
IFNULL(ftmp.DD_DOCUMENTNO,'Not Set'),
IFNULL(ftmp.DD_SALESORDERNO,'Not Set'),
IFNULL(ftmp.DD_GLACCOUNTNO,'Not Set'),
IFNULL(ftmp.DD_PRODUCTIONORDERNUMBER,'Not Set'),
IFNULL(ftmp.DD_BATCHNUMBER,'Not Set'),
IFNULL(ftmp.DD_VALUATIONTYPE,'Not Set'),
IFNULL(ftmp.DD_DEBITCREDITID,'Not Set'),
IFNULL(ftmp.DD_INCOTERMS2,'Not Set'),
IFNULL(ftmp.DD_INTORDER,'Not Set'),
IFNULL(ftmp.DD_REFERENCEDOCNO,'Not Set'),
IFNULL(ftmp.DD_INSPECTIONSTATUSINGRDOC,'Not Set'),
IFNULL(ftmp.DD_ITEMAUTOMATICALLYCREATED,'Not Set'),
IFNULL(ftmp.DD_MATERIALDOCITEMNO,0),
IFNULL(ftmp.DD_MATERIALDOCYEAR,0),
IFNULL(ftmp.DD_GOODSMOVEREASON,0),
IFNULL(ftmp.AMT_LOCALCURRAMT,0),
IFNULL(ftmp.AMT_DELIVERYCOST,0),
IFNULL(ftmp.AMT_ALTPRICECONTROL,0),
IFNULL(ftmp.AMT_ONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QUANTITY,0),
IFNULL(ftmp.CT_QUANTITYDC,0),
IFNULL(ftmp.CT_QTYENTRYUOM,0),
IFNULL(ftmp.CT_QTYGRORDUNIT,0),
IFNULL(ftmp.CT_QTYONHAND_VALSTOCK,0),
IFNULL(ftmp.CT_QTYORDPRICEUNIT,0),
IFNULL(ftmp.DIRTYROW,0),
IFNULL(ftmp.DD_CONSIGNMENTFLAG,0),
IFNULL(ftmp.AMT_POUNITPRICE,0),
IFNULL(ftmp.AMT_STDUNITPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE,0),
IFNULL(ftmp.AMT_PLANNEDPRICE1,0),
IFNULL(ftmp.DD_DOCUMENTSCHEDULENO,0),
IFNULL(ftmp.CT_ORDERQUANTITY,0),
IFNULL(ftmp.DD_REFERENCEDOCITEM,0),
IFNULL(ftmp.QTYREJECTEDEXTERNALAMT,0),
IFNULL(ftmp.QTYREJECTEDINTERNALAMT,0),
IFNULL(ftmp.DD_VENDORSPENDFLAG,0),
IFNULL(ftmp.DD_RESERVATIONNUMBER,0),
IFNULL(ftmp.DIM_MOVEMENTTYPEID,1),
IFNULL(ftmp.DIM_COMPANYID,1),
IFNULL(ftmp.DIM_CURRENCYID,1),
IFNULL(ftmp.DIM_PARTID,1),
IFNULL(ftmp.DIM_PLANTID,1),
IFNULL(ftmp.DIM_STORAGELOCATIONID,1),
IFNULL(ftmp.DIM_VENDORID,1),
IFNULL(ftmp.DIM_DATEIDMATERIALDOCDATE,1),
IFNULL(ftmp.DIM_DATEIDPOSTINGDATE,1),
IFNULL(ftmp.DIM_DATEIDDOCCREATION,1),
IFNULL(ftmp.DIM_DATEIDORDERED,1),
IFNULL(ftmp.DIM_PRODUCTHIERARCHYID,1),
IFNULL(ftmp.DIM_MOVEMENTINDICATORID,1),
IFNULL(ftmp.DIM_CUSTOMERID,1),
IFNULL(ftmp.DIM_COSTCENTERID,1),
IFNULL(ftmp.DIM_ACCOUNTCATEGORYID,1),
IFNULL(ftmp.DIM_CONSUMPTIONTYPEID,1),
IFNULL(ftmp.DIM_CONTROLLINGAREAID,1),
IFNULL(ftmp.DIM_CUSTOMERGROUP1ID,1),
IFNULL(ftmp.DIM_DOCUMENTCATEGORYID,1),
IFNULL(ftmp.DIM_DOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_ITEMCATEGORYID,1),
IFNULL(ftmp.DIM_ITEMSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERSTATUSID,1),
IFNULL(ftmp.DIM_PRODUCTIONORDERTYPEID,1),
IFNULL(ftmp.DIM_PURCHASEGROUPID,1),
IFNULL(ftmp.DIM_PURCHASEORGID,1),
IFNULL(ftmp.DIM_SALESDOCUMENTTYPEID,1),
IFNULL(ftmp.DIM_SALESGROUPID,1),
IFNULL(ftmp.DIM_SALESORDERHEADERSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERITEMSTATUSID,1),
IFNULL(ftmp.DIM_SALESORDERREJECTREASONID,1),
IFNULL(ftmp.DIM_SALESORGID,1),
IFNULL(ftmp.DIM_SPECIALSTOCKID,1),
IFNULL(ftmp.DIM_STOCKTYPEID,1),
IFNULL(ftmp.DIM_UNITOFMEASUREID,1),
IFNULL(ftmp.DIM_MATERIALGROUPID,1),
IFNULL(ftmp.AMT_EXCHANGERATE_GBL,1),
IFNULL(ftmp.AMT_EXCHANGERATE,1),
IFNULL(ftmp.DIM_TERMID,1),
IFNULL(ftmp.DIM_PROFITCENTERID,1),
IFNULL(ftmp.DIM_RECEIVINGPLANTID,1),
IFNULL(ftmp.DIM_RECEIVINGPARTID,1),
IFNULL(ftmp.DIM_DATEIDCOSTING,1),
IFNULL(ftmp.DIM_INCOTERMID,1),
IFNULL(ftmp.DIM_DATEIDDELIVERY,1),
IFNULL(ftmp.DIM_DATEIDSTATDELIVERY,1),
IFNULL(ftmp.DIM_GRSTATUSID,1),
IFNULL(ftmp.DIM_RECVISSUSTORLOCID,1),
IFNULL(ftmp.DIM_POPLANTIDORDERING,1),
IFNULL(ftmp.DIM_POPLANTIDSUPPLYING,1),
IFNULL(ftmp.DIM_POSTORAGELOCID,1),
IFNULL(ftmp.DIM_POISSUSTORAGELOCID,1),
IFNULL(ftmp.DIM_INSPUSAGEDECISIONID,1),
IFNULL(ftmp.DIM_AFSSIZEID,1),
IFNULL(ftmp.DIM_CURRENCYID_TRA,1),
IFNULL(ftmp.DIM_CURRENCYID_GBL,1),
IFNULL(ftmp.DIM_UNITOFMEASUREORDERUNITID,1),
IFNULL(ftmp.DIM_UOMUNITOFENTRYID,1),
IFNULL(ftmp.DIM_PROJECTSOURCEID,1),
IFNULL(ftmp.DW_INSERT_DATE,CURRENT_TIMESTAMP),
IFNULL(ftmp.DW_UPDATE_DATE,CURRENT_TIMESTAMP),
ftmp.DD_DOCUMENTITEMNO,
ftmp.DD_SALESORDERITEMNO,
ftmp.DD_SALESORDERDLVRNO,
ftmp.DD_PRODUCTIONORDERITEMNO,
ftmp.CT_LOTNOTTHRUQM,
ftmp.AMT_VENDORSPEND,
ftmp.FACT_MATERIALMOVEMENTID
 from fact_materialmovement  f
right join tmp_fact_materialmovement_perf1 ftmp
on f.fact_materialmovementid = ftmp.fact_materialmovementid
where f.fact_materialmovementid is  null;

truncate table tmp_fact_materialmovement_perf1;	

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');                        

/*call vectorwise (combine 'fact_materialmovement')*/

UPDATE fact_materialmovement mm 
SET  mm.dd_VendorSpendFlag = 1
 FROM dim_movementtype mt,fact_materialmovement mm  
WHERE ifnull(mm.dim_movementtypeid,1) = ifnull(mt.dim_movementtypeid, 1) 
AND ( mt.movementtype IN ('101','102','105','106','122','123','161','162','501','502','521','522') OR mm.dd_ConsignmentFlag = 1)
AND mm.dd_VendorSpendFlag <> 1;

UPDATE fact_materialmovement mm
SET mm.amt_VendorSpend = ( CASE WHEN mm.amt_AltPriceControl = 0 THEN mm.amt_LocalCurrAmt ELSE mm.amt_AltPriceControl END) + amt_DeliveryCost
WHERE mm.dd_VendorSpendFlag = 1;


     INSERT INTO processinglog (processinglogid, referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'UPDATE fact_materialmovement ms,fact_purchase po');

DROP TABLE IF EXISTS tmp_fact_purch_001;
CREATE TABLE tmp_fact_purch_001 as
SELECT DISTINCT ms.dd_DocumentNo, po.dd_DocumentItemNo, 
		po.dim_purchasegroupid,
		po.dim_purchaseorgid,
		po.dim_itemcategoryid,
		po.dim_Termid,
		po.dim_documenttypeid,
		po.dim_accountcategoryid,
		po.dim_itemstatusid,
		po.Dim_DateidCreate,
		po.Dim_DateidOrder,
		po.Dim_MaterialGroupid,
		po.Dim_DateidCosting,
		po.Dim_IncoTerm1id,
		po.dd_incoterms2,
		po.dd_IntOrder,
		po.Dim_PlantidOrdering,
		po.Dim_PlantidSupplying,
		po.Dim_StorageLocationid,
		po.Dim_IssuStorageLocid,
		po.amt_ExchangeRate,
		po.dim_vendorid,
		po.dim_vendormasterid /*Georgiana 09 Mar 2016 EA changes*/
FROM fact_purchase po inner join fact_materialmovement ms 
	on ms.dd_DocumentNo = po.dd_DocumentNo AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo;

  UPDATE fact_materialmovement ms
    SET ms.dim_purchasegroupid =  ifnull(po.dim_purchasegroupid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dim_purchasegroupid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
	AND  po.dim_purchasegroupid <> 1
	AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
	AND  ms.dim_purchasegroupid <>  ifnull(po.dim_purchasegroupid, 1);

  UPDATE fact_materialmovement ms
    SET        ms.dim_purchaseorgid = ifnull(po.dim_purchaseorgid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dim_purchaseorgid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.dim_purchaseorgid <> ifnull(po.dim_purchaseorgid, 1);

     UPDATE fact_materialmovement ms
    SET      ms.dim_itemcategoryid = ifnull(po.dim_itemcategoryid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dim_itemcategoryid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.dim_itemcategoryid <> ifnull(po.dim_itemcategoryid, 1);

          UPDATE fact_materialmovement ms
    SET ms.dim_Termid = ifnull(po.dim_Termid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dim_Termid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.dim_Termid <> ifnull(po.dim_Termid, 1);

          UPDATE fact_materialmovement ms
    SET ms.dim_documenttypeid = IFNULL(po.dim_documenttypeid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dim_documenttypeid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.dim_documenttypeid <> IFNULL(po.dim_documenttypeid, 1);


          UPDATE fact_materialmovement ms
    SET ms.dim_accountcategoryid = ifnull(po.dim_accountcategoryid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dim_accountcategoryid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.dim_accountcategoryid <>  ifnull(po.dim_accountcategoryid, 1);

          UPDATE fact_materialmovement ms
    SET ms.dim_itemstatusid = ifnull(po.dim_itemstatusid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dim_itemstatusid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND ms.dim_itemstatusid <> ifnull(po.dim_itemstatusid, 1);


UPDATE fact_materialmovement ms
SET ms.Dim_DateIDDocCreation = ifnull(po.Dim_DateidCreate, 1)
,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,max(Dim_DateidCreate) as dim_DateidCreate
from tmp_fact_purch_001
group by dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
WHERE ms.dd_DocumentNo = po.dd_DocumentNo
AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND ms.Dim_DateIDDocCreation <> ifnull(po.Dim_DateidCreate, 1);

          UPDATE fact_materialmovement ms
    SET ms.Dim_DateIDOrdered = ifnull(po.Dim_DateidOrder, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,max(Dim_DateidOrder) as Dim_DateidOrder
from tmp_fact_purch_001
group by dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.Dim_DateIDOrdered <> ifnull(po.Dim_DateidOrder, 1);


          UPDATE fact_materialmovement ms
    SET ms.Dim_MaterialGroupid = po.Dim_MaterialGroupid
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,Dim_MaterialGroupid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.Dim_MaterialGroupid <>  po.Dim_MaterialGroupid;

          UPDATE fact_materialmovement ms
    SET ms.Dim_DateidCosting = ifnull(po.Dim_DateidCosting, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,max(Dim_DateidCosting) as Dim_DateidCosting
 from tmp_fact_purch_001
 group by dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.Dim_DateidCosting <> ifnull(po.Dim_DateidCosting, 1);

  UPDATE fact_materialmovement ms
    SET ms.Dim_IncoTermid = ifnull(po.Dim_IncoTerm1id, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,Dim_IncoTerm1id from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND ms.Dim_IncoTermid <> ifnull(po.Dim_IncoTerm1id, 1);

          UPDATE fact_materialmovement ms
    SET ms.dd_incoterms2 = po.dd_incoterms2
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dd_incoterms2 from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.dd_incoterms2 <>  po.dd_incoterms2;

          UPDATE fact_materialmovement ms
    SET ms.dd_IntOrder = po.dd_IntOrder
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dd_IntOrder from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
	AND  ms.dd_IntOrder <> po.dd_IntOrder;

          UPDATE fact_materialmovement ms
    SET ms.dd_IntOrder = po.dd_IntOrder
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,dd_IntOrder from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
        AND  ms.dd_IntOrder is null 
	AND po.dd_IntOrder is not null;

          UPDATE fact_materialmovement ms
    SET ms.Dim_POPlantidOrdering = ifnull(po.Dim_PlantidOrdering, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,Dim_PlantidOrdering from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND ms.Dim_POPlantidOrdering <> ifnull(po.Dim_PlantidOrdering, 1);

          UPDATE fact_materialmovement ms
    SET ms.Dim_POPlantidSupplying = ifnull(po.Dim_PlantidSupplying, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,Dim_PlantidSupplying from tmp_fact_purch_001)  po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND ms.Dim_POPlantidSupplying <> ifnull(po.Dim_PlantidSupplying, 1);

          UPDATE fact_materialmovement ms
    SET ms.Dim_POStorageLocid = ifnull(po.Dim_StorageLocationid,1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,Dim_StorageLocationid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND ms.Dim_POStorageLocid <>  ifnull(po.Dim_StorageLocationid,1);

          UPDATE fact_materialmovement ms
    SET ms.Dim_POIssuStorageLocid = ifnull(po.Dim_IssuStorageLocid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,Dim_IssuStorageLocid from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
AND  ms.Dim_POIssuStorageLocid <>  ifnull(po.Dim_IssuStorageLocid, 1);

          UPDATE fact_materialmovement ms
    SET ms.Dim_ReceivingPlantId = (case when ms.Dim_ReceivingPlantId = 1 and ifnull(ms.Dim_Plantid, 1) <> po.Dim_PlantidOrdering 
                                              and (ifnull(ms.Dim_Plantid,1) = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId end)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,Dim_PlantidOrdering,Dim_PlantidSupplying  from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl, fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
	AND  ms.Dim_ReceivingPlantId <> (case when ms.Dim_ReceivingPlantId = 1 and ifnull(ms.Dim_Plantid, 1) <> po.Dim_PlantidOrdering 
                                              and (ifnull(ms.Dim_Plantid,1) = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId end);

          UPDATE fact_materialmovement ms
    SET ms.Dim_RecvIssuStorLocid = (case when ms.Dim_RecvIssuStorLocid = 1 and ifnull(ms.Dim_StorageLocationid,1) <> ifnull(po.Dim_StorageLocationid,1)
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid end)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  From (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_Vendorid,Dim_StorageLocationid,Dim_IssuStorageLocid   from tmp_fact_purch_001) po, dim_vendor v, dim_plant pl,fact_materialmovement ms
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ifnull(po.Dim_Vendorid, 1) = ifnull(v.Dim_Vendorid, 1)
        AND ifnull(ms.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
		AND ifnull(ms.dim_vendorid, 1) = ifnull(po.dim_vendorid, 1)
	AND  ms.Dim_RecvIssuStorLocid <> (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ifnull(ms.Dim_StorageLocationid, 1) = ifnull(po.Dim_IssuStorageLocid, 1) 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid end);
					
DROP TABLE IF EXISTS tmp_fact_purch_001;
DROP TABLE IF EXISTS tmp_fact_purch_002;

CREATE TABLE tmp_fact_purch_002
AS
   SELECT po.dd_DocumentNo,
          po.dd_DocumentItemNo,
          po.amt_exchangerate,
          avg(po.amt_StdUnitPrice) AS po_amt_StdUnitPrice,
          avg(po.amt_UnitPrice) AS po_amt_UnitPrice
     FROM fact_purchase po
          INNER JOIN fact_materialmovement ms
             ON     ms.dd_DocumentNo = po.dd_DocumentNo
                AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
   GROUP BY po.dd_DocumentNo, po.dd_DocumentItemNo, po.amt_exchangerate;

UPDATE fact_materialmovement ms 
 SET ms.amt_StdUnitPrice = po_amt_StdUnitPrice * po.amt_ExchangeRate,
 ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 FROM (select dd_DocumentNo,dd_DocumentItemNo,max(po_amt_StdUnitPrice) as po_amt_StdUnitPrice,max(amt_ExchangeRate) as amt_ExchangeRate
 from tmp_fact_purch_002
group by dd_DocumentNo,dd_DocumentItemNo) po,fact_materialmovement ms 
 WHERE ms.dd_DocumentNo = po.dd_DocumentNo
 AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
 AND ms.amt_StdUnitPrice <> (po_amt_StdUnitPrice * po.amt_ExchangeRate);

UPDATE fact_materialmovement ms
   SET ms.amt_POUnitPrice = po_amt_UnitPrice * po.amt_ExchangeRate,
       ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 FROM (select dd_DocumentNo,dd_DocumentItemNo,max(po_amt_UnitPrice) as po_amt_UnitPrice,max(amt_ExchangeRate) as amt_ExchangeRate
 from tmp_fact_purch_002
group by dd_DocumentNo,dd_DocumentItemNo) po,fact_materialmovement ms
 WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
       AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
       AND ms.amt_POUnitPrice <> (po_amt_UnitPrice * po.amt_ExchangeRate);

DROP TABLE IF EXISTS tmp_fact_purch_002;        
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'UPDATE fact_materialmovement ms,fact_purchase po');


INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement m,fact_salesorder so');

DROP TABLE IF EXISTS tmp_salesorder_fields;
CREATE TABLE tmp_salesorder_fields
AS select distinct so.dd_SalesDocNo,so.dd_SalesItemNo
,so.dim_salesorderheaderstatusid,
so.dim_salesorderitemstatusid,
so.dim_salesdocumenttypeid,
so.dim_salesorderrejectreasonid,
so.dim_salesgroupid,
so.dim_salesorgid,
so.dim_customergroup1id,
so.dim_documentcategoryid,
so.Dim_DateidSalesOrderCreated 
from fact_salesorder so
inner join fact_materialmovement ms on ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo;
	
  /*UPDATE fact_materialmovement ms
    SET ms.dim_salesorderheaderstatusid = so.dim_salesorderheaderstatusid
	,ms.dw_update_date = current_timestamp  Added automatically by update_dw_update_date.pl
	FROM tmp_salesorder_fields so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderheaderstatusid <> so.dim_salesorderheaderstatusid */

DROP TABLE IF EXISTS tmp_salesorder_fields_112207;
CREATE TABLE tmp_salesorder_fields_112207
AS
SELECT dim_salesorderheaderstatusid,dd_SalesDocNo,dd_SalesItemNo,MAX(DIM_DATEIDSALESORDERCREATED) AS DIM_DATEIDSALESORDERCREATED_MAX
FROM tmp_salesorder_fields
GROUP BY dim_salesorderheaderstatusid,dd_SalesDocNo,dd_SalesItemNo;

UPDATE fact_materialmovement ms
 SET ms.dim_salesorderheaderstatusid = ifnull(so.dim_salesorderheaderstatusid, 1)
 ,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 FROM tmp_salesorder_fields_112207 so,fact_materialmovement ms
 WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
 AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
 AND ms.dim_salesorderheaderstatusid <> ifnull(so.dim_salesorderheaderstatusid, 1);

DROP TABLE IF EXISTS tmp_salesorder_fields_1122071;
CREATE TABLE tmp_salesorder_fields_1122071
AS
SELECT dim_salesorderitemstatusid,dim_salesorderheaderstatusid,dd_SalesDocNo,dd_SalesItemNo,MAX(DIM_DATEIDSALESORDERCREATED) AS DIM_DATEIDSALESORDERCREATED_MAX
FROM tmp_salesorder_fields
GROUP BY dim_salesorderitemstatusid,dim_salesorderheaderstatusid,dd_SalesDocNo,dd_SalesItemNo;

 /* UPDATE fact_materialmovement ms
    SET         ms.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
	,ms.dw_update_date = current_timestamp  Added automatically by update_dw_update_date.pl
        FROM tmp_salesorder_fields so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderitemstatusid <>  so.dim_salesorderitemstatusid
*/
DROP TABLE IF EXISTS TMP_salesorder_fields_01122017;
CREATE TABLE TMP_salesorder_fields_01122017
AS
SELECT dim_documentcategoryid,dim_customergroup1id,dim_salesorgid,dim_salesgroupid,dim_salesorderrejectreasonid ,dim_salesdocumenttypeid,dim_salesorderitemstatusid,dd_SalesDocNo,dd_SalesItemNo,MAX(DIM_DATEIDSALESORDERCREATED) MAX_DIM_DATEIDSALESORDERCREATED
FROM tmp_salesorder_fields
GROUP BY dim_documentcategoryid,dim_customergroup1id,dim_salesorgid,dim_salesgroupid,
dim_salesorderrejectreasonid ,dim_salesdocumenttypeid,dim_salesorderitemstatusid,dd_SalesDocNo,
dd_SalesItemNo;

UPDATE fact_materialmovement ms
 SET ms.dim_salesorderitemstatusid = ifnull(so.dim_salesorderitemstatusid, 1)
 ,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 FROM TMP_salesorder_fields_01122017  so,fact_materialmovement ms
 WHERE ms.dd_SalesOrderNo = so.dd_SalesDocNo
 AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
 AND ms.dim_salesorderitemstatusid <> ifnull(so.dim_salesorderitemstatusid, 1);



          UPDATE fact_materialmovement ms
    SET ms.dim_salesdocumenttypeid = ifnull(so.dim_salesdocumenttypeid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
        FROM TMP_salesorder_fields_01122017 so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesdocumenttypeid <> ifnull(so.dim_salesdocumenttypeid, 1);

          UPDATE fact_materialmovement ms
    SET ms.dim_salesorderrejectreasonid = ifnull(so.dim_salesorderrejectreasonid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
        FROM TMP_salesorder_fields_01122017 so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderrejectreasonid <> ifnull(so.dim_salesorderrejectreasonid, 1);

          UPDATE fact_materialmovement ms
    SET ms.dim_salesgroupid = ifnull(so.dim_salesgroupid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
        FROM TMP_salesorder_fields_01122017 so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesgroupid <>  ifnull(so.dim_salesgroupid, 1);

          UPDATE fact_materialmovement ms
    SET ms.dim_salesorgid = ifnull(so.dim_salesorgid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
        FROM TMP_salesorder_fields_01122017 so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorgid <> ifnull(so.dim_salesorgid, 1);

          UPDATE fact_materialmovement ms
    SET ms.dim_customergroup1id = ifnull(so.dim_customergroup1id, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
        FROM TMP_salesorder_fields_01122017 so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_customergroup1id <>  ifnull(so.dim_customergroup1id, 1);

          UPDATE fact_materialmovement ms
    SET ms.dim_documentcategoryid = ifnull(so.dim_documentcategoryid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
        FROM TMP_salesorder_fields_01122017 so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_documentcategoryid <> ifnull(so.dim_documentcategoryid, 1);

                   UPDATE fact_materialmovement ms
    SET ms.Dim_DateIDDocCreation = ifnull(so.MAX_DIM_DATEIDSALESORDERCREATED, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
        FROM TMP_salesorder_fields_01122017 so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDDocCreation <>  ifnull(so.MAX_DIM_DATEIDSALESORDERCREATED, 1);

          UPDATE fact_materialmovement ms
    SET ms.Dim_DateIDOrdered = ifnull(so.MAX_DIM_DATEIDSALESORDERCREATED, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
        FROM TMP_salesorder_fields_01122017  so,fact_materialmovement ms
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDOrdered <> ifnull(so.MAX_DIM_DATEIDSALESORDERCREATED, 1);

DROP TABLE IF EXISTS TMP_salesorder_fields_01122017;

  
DROP TABLE IF EXISTS tmp_salesorder_fields;
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement m,fact_salesorder so');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement ms,fact_productionorder po');

  UPDATE fact_materialmovement ms
    SET ms.dim_productionorderstatusid = po.dim_productionorderstatusid
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,fact_materialmovement ms
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionorderstatusid <>  po.dim_productionorderstatusid;

UPDATE fact_materialmovement ms
    SET  ms.dim_productionordertypeid = ifnull(po.Dim_ordertypeid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_productionorder po,fact_materialmovement ms
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionordertypeid <> ifnull(po.Dim_ordertypeid, 1);

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement ms,fact_productionorder po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

  UPDATE fact_materialmovement ms
    SET ms.dim_inspusagedecisionid = ifnull(fil.dim_inspusagedecisionid, 1)
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_inspectionlot fil,fact_materialmovement ms
  WHERE     ms.dd_MaterialDocItemNo = fil.dd_MaterialDocItemNo
        AND ms.dd_MaterialDocNo = fil.dd_MaterialDocNo
        AND ms.dd_MaterialDocYear = fil.dd_MaterialDocYear
AND  ms.dim_inspusagedecisionid <> ifnull(fil.dim_inspusagedecisionid, 1);

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp,'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',current_timestamp, 'bi_populate_materialmovement_fact END');

drop table if exists bwtarupd_tmp_100;

create table bwtarupd_tmp_100 as
Select  x.MATNR, x.BWKEY, max((x.LFGJA * 100) + x.LFMON) updcol 
from mbew_no_bwtar x
Group by  x.MATNR, x.BWKEY;

/* 25 Apr 2018 Georgiana Changes due to integration issues in STG, bellow updates were causing issues on the server and we optimized them by creating additional temp tables*/
/*UPDATE fact_materialmovement ia
SET amt_PlannedPrice1 = ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0)
	,ia.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl
FROM  mbew_no_bwtar m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt,
       fact_materialmovement ia
 WHERE ifnull(ia.dim_partid, 1) = ifnull(prt.Dim_partid, 1)
  and ifnull(ia.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol 
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and amt_PlannedPrice1 <> ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0)*/
  
  drop table if exists tmp_for_upd_amt;
create table tmp_for_upd_amt as
select distinct m.MATNR,m.BWKEY,ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0) as amt_col from mbew_no_bwtar m,bwtarupd_tmp_100 x
where x.MATNR = m.MATNR   and  x.BWKEY = m.BWKEY  and ((m.LFGJA * 100) + m.LFMON) = updcol ;

UPDATE fact_materialmovement ia SET amt_PlannedPrice1 =amt_col 
,ia.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  tmp_for_upd_amt m,       dim_plant pl,        dim_part prt,        fact_materialmovement ia 
WHERE ifnull(ia.dim_partid, 1) = ifnull(prt.Dim_partid, 1)   and ifnull(ia.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)  
and prt.PartNumber = m.MATNR   and pl.ValuationArea = m.BWKEY  
and amt_PlannedPrice1 <> amt_col;
 drop table if exists tmp_for_upd_amt;

/*UPDATE fact_materialmovement ia
SET amt_PlannedPrice = ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0)
	,ia.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl
FROM  mbew_no_bwtar m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt,
      fact_materialmovement ia
 WHERE ifnull(ia.dim_partid, 1) = ifnull(prt.Dim_partid,1)
  and ifnull(ia.dim_plantid, 1) = ifnull(pl.dim_plantid, 1)
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol 
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and  amt_PlannedPrice <> ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0)*/
  
    drop table if exists tmp_for_upd_amt;
create table tmp_for_upd_amt as
select distinct m.MATNR,m.BWKEY,ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end ),0) as amt_col from mbew_no_bwtar m,bwtarupd_tmp_100 x
where x.MATNR = m.MATNR   and  x.BWKEY = m.BWKEY  and ((m.LFGJA * 100) + m.LFMON) = updcol ;

UPDATE fact_materialmovement ia
SET amt_PlannedPrice = amt_col
	,ia.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_for_upd_amt m,
       dim_plant pl,
       dim_part prt,
      fact_materialmovement ia
 WHERE ia.dim_partid = prt.Dim_partid
  and ia.dim_plantid = pl.dim_plantid
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and  amt_PlannedPrice <> amt_col;
 drop table if exists tmp_for_upd_amt;
 /*25 Apr End of Changes*/
/*call bi_purchase_matmovement_dlvr_link()*/
/*replaced update with merge to prevent unabe to get stable set of rows error*/
merge into fact_materialmovement mm 
using
(select distinct fact_materialmovementid, ifnull(uom.dim_unitofmeasureid, 1) as dim_unitofmeasureorderunitid
FROM dim_unitofmeasure uom,(select distinct MSEG_BSTME,MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR from 
MKPF_MSEG) m,dim_plant dp,dim_company dc,fact_materialmovement ms
WHERE uom.uom = m.MSEG_BSTME
AND m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_unitofmeasureorderunitid,-1) <> ifnull(uom.dim_unitofmeasureid,-2)
) t
on t.fact_materialmovementid = mm.fact_materialmovementid
when matched then update set
mm.dim_unitofmeasureorderunitid = t.dim_unitofmeasureorderunitid,
mm.dw_update_date = current_timestamp
;

/*replaced update with merge to prevent unable to get a stable set of rows*/
merge into fact_materialmovement mm 
using
(select distinct fact_materialmovementid, uom.dim_unitofmeasureid as dim_unitofmeasureorderunitid
FROM dim_unitofmeasure uom,(select distinct MSEG1_BSTME,MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR from 
MKPF_MSEG1) m,dim_plant dp,dim_company dc,fact_materialmovement ms
WHERE uom.uom = m.MSEG1_BSTME
AND m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_unitofmeasureorderunitid,-1) <> ifnull(uom.dim_unitofmeasureid,-2)
) t
on t.fact_materialmovementid = mm.fact_materialmovementid
when matched then update set
mm.dim_unitofmeasureorderunitid = t.dim_unitofmeasureorderunitid,
mm.dw_update_date = current_timestamp
;
  
UPDATE fact_materialmovement ms
set ms.dim_unitofmeasureorderunitid = 1
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE  ms.dim_unitofmeasureorderunitid IS NULL;

/*replaced update with merge to prevent unable to get stable set of rows error*/  
merge into fact_materialmovement mm 
using
(select distinct fact_materialmovementid, ifnull(uom.dim_unitofmeasureid, 1) as dim_unitofmeasureid
FROM dim_unitofmeasure uom,(select distinct MSEG_ERFME,MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR from 
MKPF_MSEG) m,dim_plant dp,dim_company dc,fact_materialmovement ms
WHERE uom.uom = m.MSEG_ERFME
AND m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_uomunitofentryid,-1) <> ifnull(uom.dim_unitofmeasureid,-2)
) t
on t.fact_materialmovementid = mm.fact_materialmovementid
when matched then update set
mm.dim_uomunitofentryid = t.dim_unitofmeasureid,
mm.dw_update_date = current_timestamp;
 
 /*replaced update with merge to prevent unable to get stable set of rows error*/ 
merge into fact_materialmovement mm 
using
(select distinct fact_materialmovementid, uom.dim_unitofmeasureid as dim_unitofmeasureid
FROM dim_unitofmeasure uom,(select distinct MSEG1_ERFME,MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR from 
MKPF_MSEG1) m,dim_plant dp,dim_company dc,fact_materialmovement ms
  WHERE uom.uom = m.MSEG1_ERFME
AND m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_uomunitofentryid,-1) <> ifnull(uom.dim_unitofmeasureid,-2)
) t
on t.fact_materialmovementid = mm.fact_materialmovementid
when matched then update set
mm.dim_uomunitofentryid = t.dim_unitofmeasureid,
mm.dw_update_date = current_timestamp;

UPDATE fact_materialmovement ms
set ms.dim_uomunitofentryid = 1
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE  ms.dim_uomunitofentryid IS NULL;

/* Update global exchange rate */
UPDATE fact_materialmovement ms
SET amt_exchangerate_GBL = z.exchangeRate
	,ms.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM dim_currency tra ,pGlobalCurrency_tbl, tmp_getExchangeRate1 z, dim_date dd,fact_materialmovement ms
WHERE ifnull(ms.dim_currencyid_tra,1) = ifnull(tra.dim_currencyid,1) AND ifnull(ms.dim_DateIDPostingDate, 1) = ifnull(dd.dim_dateid, 1)
AND z.pFromCurrency  = tra.currencycode and z.fact_script_name = 'bi_populate_materialmovement_fact'
AND z.pToCurrency = pGlobalCurrency AND z.pFromExchangeRate = 0 AND z.pDate = dd.datevalue
AND ms.amt_exchangerate_GBL <> z.exchangeRate;

/* Octavian: Every Angle Transition Addons */
/* replaced update with merge to pass unable to get stable set of rows */
merge into fact_materialmovement fmm 
using
(select distinct fact_materialmovementid, ifnull(MSEG_DMBTR,0) as amt_lc
FROM (select distinct MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR,MSEG_DMBTR from MKPF_MSEG) m,
dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND amt_lc <> ifnull(MSEG_DMBTR,0)
) t
on t.fact_materialmovementid = fmm.fact_materialmovementid
when matched then update set
fmm.amt_lc = t.amt_lc,
fmm.dw_update_date = current_timestamp;

/* replaced update with merge to pass unable to get stable set of rows*/
merge into fact_materialmovement fmm 
using
(select distinct fact_materialmovementid, ifnull(MSEG1_DMBTR,0) as amt_lc
FROM (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MSEG1_DMBTR from MKPF_MSEG1) m, dim_plant dp, dim_company dc, fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND amt_lc <> ifnull(MSEG1_DMBTR,0)
) t
on t.fact_materialmovementid = fmm.fact_materialmovementid
when matched then update set
fmm.amt_lc = t.amt_lc,
fmm.dw_update_date = current_timestamp;

/* UPDATE fact_materialmovement fch
SET dd_itemtext = ifnull(MSEG_SGTXT,'Not Set')
	,dw_update_date = current_timestamp 
FROM (select distinct MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR,MSEG_SGTXT from MKPF_MSEG) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_itemtext <> ifnull(MSEG_SGTXT,'Not Set') 
*/


MERGE INTO fact_materialmovement fch
 USING (SELECT dd_MaterialDocNo,dd_MaterialDocItemNo,MAX(ifnull(MSEG1_SGTXT,'Not Set')) AS dd_itemtext
 FROM (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MSEG1_SGTXT from MKPF_MSEG1) m, dim_plant dp,
dim_company dc,fact_materialmovement fch
 WHERE m.MSEG1_WERKS = dp.PlantCode
 AND m.MSEG1_BUKRS = dc.CompanyCode
 AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
 AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
 AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
 GROUP BY fch.dd_MaterialDocNo,fch.dd_MaterialDocItemNo )x
 ON (fch.dd_MaterialDocItemNo = x.dd_MaterialDocItemNo
 and fch.dd_MaterialDocNo = x.dd_MaterialDocNo)
 WHEN MATCHED THEN
UPDATE SET dd_itemtext = x.dd_itemtext
 ,dw_update_date = current_timestamp;

UPDATE fact_materialmovement fch
SET dd_itemtext = ifnull(MSEG1_SGTXT,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MSEG1_SGTXT from MKPF_MSEG1) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_itemtext <> ifnull(MSEG1_SGTXT,'Not Set');

drop table if exists tmp1_dim_batch;
create table tmp1_dim_batch as
select distinct dim_batchid,batchnumber,partnumber,plantcode from dim_batch
where plantcode <>'Not Set';

update fact_materialmovement ia
set ia.dim_batchid = ifnull(b.dim_batchid,1),
ia.dw_update_date = current_timestamp
from tmp1_dim_batch b, dim_part dp,fact_materialmovement ia
where ifnull(ia.dim_partid, 1) = ifnull(dp.dim_partid, 1) and
ia.dd_batchnumber = b.batchnumber and
dp.partnumber = b.partnumber
and dp.plant = b.plantcode
and ia.dim_batchid <> ifnull(b.dim_batchid, 1);
/* Octavian: Every Angle Transition Addons */


/*Georgiana EA Transition Addons*/

UPDATE fact_materialmovement fch
SET dd_documentheadertext = ifnull(MKPF_BKTXT,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR,MKPF_BKTXT from MKPF_MSEG) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_documentheadertext <> ifnull(MKPF_BKTXT,'Not Set');

/* replaced update with merge to pass unable to get stable set of rows*/
merge into fact_materialmovement fmm 
using 
(select distinct fact_materialmovementid, ifnull(MKPF_BKTXT,'Not Set') as dd_documentheadertext
FROM (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MKPF_BKTXT from MKPF_MSEG1) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_documentheadertext <> ifnull(MKPF_BKTXT,'Not Set')
) t
on t.fact_materialmovementid = fmm.fact_materialmovementid
when matched then update set
fmm.dd_documentheadertext = t.dd_documentheadertext,
fmm.dw_update_date = current_timestamp;

UPDATE fact_materialmovement fch
SET dim_dateidaccountiddoccreated = ifnull(dt.dim_dateid, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR,MKPF_CPUDT from MKPF_MSEG) m, dim_plant dp, dim_company dc, dim_date dt,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dt.CompanyCode = m.MSEG_BUKRS 
AND m.MKPF_CPUDT = dt.DateValue
and m.MSEG_WERKS = dt.plantcode_factory
AND dim_dateidaccountiddoccreated <> ifnull(dt.dim_dateid, 1);

UPDATE fact_materialmovement fch
SET dim_dateidaccountiddoccreated = ifnull(dt.dim_dateid, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MKPF_CPUDT from MKPF_MSEG1) m, dim_plant dp, dim_company dc, dim_date dt,fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dt.CompanyCode = m.MSEG1_BUKRS 
AND m.MKPF_CPUDT = dt.DateValue
and m.MSEG1_WERKS = dt.plantcode_factory
AND dim_dateidaccountiddoccreated <> ifnull(dt.dim_dateid, 1);

UPDATE fact_materialmovement fch
SET dd_username = ifnull(MKPF_USNAM,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR,MKPF_USNAM from MKPF_MSEG) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_username <> ifnull(MKPF_USNAM,'Not Set');

UPDATE fact_materialmovement fch
SET dd_username = ifnull(MKPF_USNAM,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MKPF_USNAM from MKPF_MSEG1) m, dim_plant dp, dim_company dc, fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_username <> ifnull(MKPF_USNAM,'Not Set');

UPDATE fact_materialmovement fch
SET dd_unloadingpoint = ifnull(MSEG_ABLAD,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR,MSEG_ABLAD from MKPF_MSEG) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_unloadingpoint <> ifnull(MSEG_ABLAD,'Not Set');

UPDATE fact_materialmovement fch
SET dd_unloadingpoint = ifnull(MSEG1_ABLAD,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM  (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MSEG1_ABLAD from MKPF_MSEG1) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_unloadingpoint <> ifnull(MSEG1_ABLAD,'Not Set');

UPDATE fact_materialmovement fch
SET dd_numberofmaterialdoc = ifnull(MSEG_SMBLN,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR,MSEG_SMBLN from MKPF_MSEG) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_numberofmaterialdoc <> ifnull(MSEG_SMBLN,'Not Set');

UPDATE fact_materialmovement fch
SET dd_numberofmaterialdoc = ifnull(MSEG1_SMBLN,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MSEG1_SMBLN from MKPF_MSEG1) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_numberofmaterialdoc <> ifnull(MSEG1_SMBLN,'Not Set');

UPDATE fact_materialmovement fch
SET dd_goodsrecipent = ifnull(MSEG_WEMPF,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG_WERKS,MSEG_BUKRS,MSEG_MBLNR,MSEG_ZEILE,MSEG_MJAHR,MSEG_WEMPF from MKPF_MSEG) m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_goodsrecipent <> ifnull(MSEG_WEMPF,'Not Set');

UPDATE fact_materialmovement fch
SET dd_goodsrecipent = ifnull(MSEG1_WEMPF,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM (select distinct MSEG1_WERKS,MSEG1_BUKRS,MSEG1_MBLNR,MSEG1_ZEILE,MSEG1_MJAHR,MSEG1_WEMPF from MKPF_MSEG1) m, dim_plant dp, dim_company dc, fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_goodsrecipent <> ifnull(MSEG1_WEMPF,'Not Set');

/*EA Changes End*/

/* Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */

UPDATE fact_materialmovement ms
SET ms.std_exchangerate_dateid = ifnull(ms.dim_DateIDPostingDate, 1)
WHERE   ms.std_exchangerate_dateid <> ifnull(ms.dim_DateIDPostingDate,1);

/* END Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016  */

/* 21 Apr 2016 CristianT Start: Adding PO Currency from Purchasing */
UPDATE fact_materialmovement mm
SET mm.Dim_POCurrencyid = ifnull(pr.Dim_POCurrencyid, 1)
FROM (select distinct dd_DocumentNo,dd_DocumentItemNo,Dim_POCurrencyid from fact_purchase) pr,fact_materialmovement mm
WHERE pr.dd_DocumentNo = mm.dd_DocumentNo
      AND pr.dd_DocumentItemNo = mm.dd_DocumentItemNo
      AND mm.Dim_POCurrencyid <> ifnull(pr.Dim_POCurrencyid, 1);

/* 21 Apr 2016 CristianT End: Adding PO Currency from Purchasing */

/* 26 Apr 2016 Georgiana Changes: Adding Net Order Price and Order Price Unit columns*/

UPDATE fact_materialmovement mm
SET mm.amt_netorderprice = fp.amt_netorderprice,
mm.dw_update_date = current_timestamp
FROM  (select distinct dd_DocumentNo,dd_DocumentItemNo,avg(amt_netorderprice) amt_netorderprice from fact_purchase
group by dd_DocumentNo,dd_DocumentItemNo) fp,fact_materialmovement mm
WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
AND  mm.amt_netorderprice <> fp.amt_netorderprice;


UPDATE fact_materialmovement mm
SET mm.dim_unitofpricemeasureid = ifnull(fp.dim_unitofpricemeasureid, 1),
mm.dw_update_date = current_timestamp
FROM (select distinct dd_DocumentNo,dd_DocumentItemNo,dim_unitofpricemeasureid from fact_purchase) fp,fact_materialmovement mm
WHERE fp.dd_DocumentNo = mm.dd_DocumentNo
AND fp.dd_DocumentItemNo = mm.dd_DocumentItemNo
AND  mm.dim_unitofpricemeasureid <> ifnull(fp.dim_unitofpricemeasureid, 1);

UPDATE fact_materialmovement ms
SET ms.ct_Quantity = m.MSEG_MENGE
FROM fact_materialmovement ms, MKPF_MSEG m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl a
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ms.ct_Quantity <> m.MSEG_MENGE;

UPDATE fact_materialmovement ms
SET ms.ct_Quantity = m.MSEG1_MENGE
FROM fact_materialmovement ms, MKPF_MSEG1 m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
 AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND ms.ct_Quantity <> m.MSEG1_MENGE;


UPDATE fact_materialmovement ms
SET ms.ct_QuantityDC = (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE
FROM fact_materialmovement ms, MKPF_MSEG m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl a
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ms.ct_QuantityDC <> (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE;

UPDATE fact_materialmovement ms
SET ms.ct_QuantityDC = (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_MENGE
FROM fact_materialmovement ms, MKPF_MSEG1 m,dim_plant dp,dim_company dc,pGlobalCurrency_tbl
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG1_MBLNR
 AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear
AND ms.ct_QuantityDC <> (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_MENGE;

/*26 Apr 2016 End Of Changes*/

/*12 Oct 2016- added Reference field (BI-4154) */
UPDATE fact_materialmovement fch
SET dd_reference = ifnull(MKPF_XBLNR,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM MKPF_MSEG m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fch.dd_MaterialDocYear
AND dd_reference <> ifnull(MKPF_XBLNR,'Not Set');

UPDATE fact_materialmovement fch
SET dd_reference = ifnull(MKPF_XBLNR,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM MKPF_MSEG1 m, dim_plant dp, dim_company dc,fact_materialmovement fch
WHERE m.MSEG1_WERKS = dp.PlantCode
AND m.MSEG1_BUKRS  = dc.CompanyCode
AND fch.dd_MaterialDocNo = m.MSEG1_MBLNR
AND fch.dd_MaterialDocItemNo = m.MSEG1_ZEILE
AND m.MSEG1_MJAHR = fch.dd_MaterialDocYear
AND dd_reference <> ifnull(MKPF_XBLNR,'Not Set');
/* END OF (BI-4154)*/

/* Andrei APP-7532 */
/* drop table if exists tmp_minposting

create table tmp_minposting as select b.dim_batchid, b.PartNumber, b.batchnumber, b.PlantCode, MIN(d.datevalue) as mindate
FROM fact_materialmovement f 
inner join dim_batch b on f.dim_batchid = b.dim_batchid 
inner join  dim_date d on d.dim_dateid = f.dim_dateidpostingdate
where f.dim_dateidpostingdate <> 1
GROUP BY b.dim_batchid, b.PartNumber, b.batchnumber, b.PlantCode

UPDATE fact_materialmovement f
SET f.DIM_DATEIDMINPOSTINGDATE = d.dim_dateid
FROM fact_materialmovement f
inner join tmp_minposting t on f.dim_batchid = t.dim_batchid
inner join dim_plant pl ON t.plantcode = pl.plantcode
inner join  dim_date d on t.mindate = d.datevalue and d.plantcode_factory = t.plantcode and d.companycode = pl.companycode
where f.DIM_DATEIDMINPOSTINGDATE <> d.dim_dateid */

merge into fact_materialmovement m using ( 
 select t.dim_batchid,dt.dim_dateid
from  (                                   
select ifnull(t101.dim_batchid,t501_312_911.dim_batchid) as dim_batchid, ifnull(t101.partnumber,t501_312_911.partnumber) as partnumber,
ifnull(t101.batchnumber,t501_312_911.batchnumber) as batchnumber, ifnull(t101.plantcode,t501_312_911.plantcode) as plantcode, t101.mindate,
t501_312_911.mindate , CASE WHEN t501_312_911.mindate IS NOT NULL THEN t501_312_911.mindate ELSE
t101.mindate END AS mindatefinal from 
(select  b.dim_batchid, b.PartNumber, b.batchnumber, b.PlantCode, 
MIN(d.datevalue) as mindate
FROM fact_materialmovement f 
inner join dim_batch b on ifnull(f.dim_batchid, 1) = ifnull(b.dim_batchid, 1)
inner join  dim_date d on ifnull(d.dim_dateid, 1) = ifnull(f.dim_dateidpostingdate, 1)
inner join dim_movementtype m on f.dim_movementtypeid = m.dim_movementtypeid
where  m.MovementType in ('101')  and 
 f.dim_dateidpostingdate <> 1
GROUP BY b.dim_batchid, b.PartNumber, b.batchnumber, b.PlantCode) t101
full outer join
(select  b.dim_batchid, b.PartNumber, b.batchnumber, b.PlantCode,
MIN(d.datevalue) as mindate
FROM fact_materialmovement f 
inner join dim_batch b on ifnull(f.dim_batchid, 1) = ifnull(b.dim_batchid, 1)
inner join  dim_date d on ifnull(d.dim_dateid, 1) = ifnull(f.dim_dateidpostingdate, 1)
inner join dim_movementtype m on f.dim_movementtypeid = ifnull(m.dim_movementtypeid, 1)
where  m.MovementType in ('501', '321', '911')  and 
 f.dim_dateidpostingdate <> 1
GROUP BY b.dim_batchid, b.PartNumber, b.batchnumber, b.PlantCode) t501_312_911 
on t101.dim_batchid = t501_312_911.dim_batchid ) t 
inner join dim_plant pl on t.plantcode = pl.plantcode
inner join dim_date dt on dt.plantcode_factory = t.plantcode and dt.datevalue = t.mindatefinal and pl.companycode = dt.companycode
) t_id
on m.dim_batchid =  t_id.dim_batchid
when matched then update 
set m.DIM_DATEIDMINPOSTINGDATE = ifnull(t_id.dim_dateid, 1)
where m.DIM_DATEIDMINPOSTINGDATE <> ifnull(t_id.dim_dateid, 1);

update  fact_materialmovement f set f.DIM_DATEIDMINPOSTINGDATE = 1
from fact_materialmovement f inner join dim_movementtype m on f.dim_movementtypeid = ifnull(m.dim_movementtypeid,1)
where m.MovementType not in ('101', '501', '321', '911');

/* End Andrei APP-7532 */

drop table if exists dim_profitcenter_789;
drop table if exists bwtarupd_tmp_100;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_allcols;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG1_del;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_allcols;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG;
DROP TABLE IF EXISTS tmp_mm_MKPF_MSEG_del;
drop table if exists tmp_fact_materialmovement_perf1;
drop table if exists tmp_fact_materialmovement_del;
DROP TABLE IF EXISTS tmp_denorm_fact_mm_mseg1;
DROP TABLE IF EXISTS tmp_matmvmt_forinsert;
