/*********************************************Change History*******************************************************/
/*Date             By        Version           Desc                                                            */
/*   10 Feb 2015      Liviu Ionescu            Add Combine for table tmp_pGlobalCurrency_planorder */
/*    13 Jul 2016      Liviu Ionescu            Delete missing orders fro SAP according to BI-3462 */
/******************************************************************************************************************/


/* Removed START OF PROC */

 DROP TABLE IF EXISTS tmp_pGlobalCurrency_planorder;
 CREATE TABLE tmp_pGlobalCurrency_planorder ( pGlobalCurrency VARCHAR(3) NULL);

 INSERT INTO tmp_pGlobalCurrency_planorder VALUES ( 'USD' );

update tmp_pGlobalCurrency_planorder
SET pGlobalCurrency = ifnull(s.property_value, 'USD')
FROM tmp_pGlobalCurrency_planorder tmp
 		CROSS JOIN (select property_value from systemproperty WHERE property = 'customer.global.currency') s;


/*    13 Jul 2016      Liviu Ionescu            Delete missing orders fro SAP according to BI-23462 - to be considered is change is keeped */
/*  Keep the records in auxiliary table in case restore is needed */
insert into fact_planorderdeleted
(FACT_PLANORDERID,
DIM_COMPANYID,
DIM_PARTID,
DIM_MPNID,
DIM_STORAGELOCATIONID,
DIM_PLANTID,
DIM_UNITOFMEASUREID,
DIM_PURCHASEORGID,
DIM_VENDORID,
DIM_FIXEDVENDORID,
DIM_SPECIALPROCUREMENTID,
DIM_CONSUMPTIONTYPEID,
DIM_ACCOUNTCATEGORYID,
DIM_SPECIALSTOCKID,
CT_QTYTOTAL,
CT_COMPLETED,
AMT_EXTENDEDPRICE,
DD_PLANORDERNO,
CT_QTYREDUCED,
DIRTYROW,
DIM_BOMSTATUSID,
DIM_BOMUSAGEID,
DIM_OBJECTTYPEID,
DIM_PRODUCTIONSCHEDULERID,
DIM_SCHEDULINGERRORID,
DIM_TASKLISTTYPEID,
DIM_ORDERTYPEID,
DIM_DATEIDSTART,
DIM_DATEIDFINISH,
DIM_DATEIDOPENING,
DD_BOMEXPLOSIONNO,
DIM_ACTIONSTATEID,
DIM_DATEIDCONVERSIONDATE,
DIM_PLANORDERSTATUSID,
DIM_PLANORDERMISCID,
DIM_DATEIDPRODUCTIONSTART,
DIM_DATEIDPRODUCTIONFINISH,
DIM_DATEIDEXPLOSION,
DIM_DATEIDACTION,
DIM_SCHEDULETYPEID,
DIM_AVAILABILITYCONFIRMATIONID,
DD_PRODUCTIONORDERNO,
DD_SALESORDERITEMNO,
DD_SALESORDERSCHEDULENO,
DD_SALESORDERNO,
CT_GRPROCESSINGTIME,
CT_QTYISSUED,
CT_QTYCOMMITTED,
DIM_CURRENCYID,
DIM_CUSTOMERGROUP1ID,
DIM_CUSTOMERID,
DIM_DOCUMENTCATEGORYID,
DIM_SALESDOCUMENTTYPEID,
AMT_EXTENDEDPRICE_GBL,
DD_SEQUENCENO,
DD_PLANNSCENARIO,
AMT_STDUNITPRICE,
DD_DOCUMENTNO,
DIM_DATEIDMRPSCHED,
DIM_PROCUREMENTID,
DIM_DATEIDWEEKENDING,
DIM_DATEIDMRPSTARTDATE,
DIM_MRPCONTROLLERID,
DD_AGREEMENTNO,
DD_AGREEMENTITEMNO,
DIM_DATEIDMRPDOCK,
DIM_DATEIDSCHEDULEDSTART,
DIM_DATEIDSCHEDULEDFINISH,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_PROFITCENTERID,
DIM_CURRENCYID_TRA,
DIM_CURRENCYID_GBL,
DW_INSERT_DATE,
DW_UPDATE_DATE,
DIM_PROJECTSOURCEID,
DD_FORECASTPROFILE,
STD_EXCHANGERATE_DATEID,
DD_PRODUCTIONVERSION,
AMT_COMPONENT_SCRAP,
AMT_OPERATION_SCRAP,
CT_SCRAP_QUANTITY,
DeleteTime)

select 
p.FACT_PLANORDERID,
p.DIM_COMPANYID,
p.DIM_PARTID,
p.DIM_MPNID,
p.DIM_STORAGELOCATIONID,
p.DIM_PLANTID,
p.DIM_UNITOFMEASUREID,
p.DIM_PURCHASEORGID,
p.DIM_VENDORID,
p.DIM_FIXEDVENDORID,
p.DIM_SPECIALPROCUREMENTID,
p.DIM_CONSUMPTIONTYPEID,
p.DIM_ACCOUNTCATEGORYID,
p.DIM_SPECIALSTOCKID,
p.CT_QTYTOTAL,
p.CT_COMPLETED,
p.AMT_EXTENDEDPRICE,
p.DD_PLANORDERNO,
p.CT_QTYREDUCED,
p.DIRTYROW,
p.DIM_BOMSTATUSID,
p.DIM_BOMUSAGEID,
p.DIM_OBJECTTYPEID,
p.DIM_PRODUCTIONSCHEDULERID,
p.DIM_SCHEDULINGERRORID,
p.DIM_TASKLISTTYPEID,
p.DIM_ORDERTYPEID,
p.DIM_DATEIDSTART,
p.DIM_DATEIDFINISH,
p.DIM_DATEIDOPENING,
p.DD_BOMEXPLOSIONNO,
p.DIM_ACTIONSTATEID,
p.DIM_DATEIDCONVERSIONDATE,
p.DIM_PLANORDERSTATUSID,
p.DIM_PLANORDERMISCID,
p.DIM_DATEIDPRODUCTIONSTART,
p.DIM_DATEIDPRODUCTIONFINISH,
p.DIM_DATEIDEXPLOSION,
p.DIM_DATEIDACTION,
p.DIM_SCHEDULETYPEID,
p.DIM_AVAILABILITYCONFIRMATIONID,
p.DD_PRODUCTIONORDERNO,
p.DD_SALESORDERITEMNO,
p.DD_SALESORDERSCHEDULENO,
p.DD_SALESORDERNO,
p.CT_GRPROCESSINGTIME,
p.CT_QTYISSUED,
p.CT_QTYCOMMITTED,
p.DIM_CURRENCYID,
p.DIM_CUSTOMERGROUP1ID,
p.DIM_CUSTOMERID,
p.DIM_DOCUMENTCATEGORYID,
p.DIM_SALESDOCUMENTTYPEID,
p.AMT_EXTENDEDPRICE_GBL,
p.DD_SEQUENCENO,
p.DD_PLANNSCENARIO,
p.AMT_STDUNITPRICE,
p.DD_DOCUMENTNO,
p.DIM_DATEIDMRPSCHED,
p.DIM_PROCUREMENTID,
p.DIM_DATEIDWEEKENDING,
p.DIM_DATEIDMRPSTARTDATE,
p.DIM_MRPCONTROLLERID,
p.DD_AGREEMENTNO,
p.DD_AGREEMENTITEMNO,
p.DIM_DATEIDMRPDOCK,
p.DIM_DATEIDSCHEDULEDSTART,
p.DIM_DATEIDSCHEDULEDFINISH,
p.AMT_EXCHANGERATE_GBL,
p.AMT_EXCHANGERATE,
p.DIM_PROFITCENTERID,
p.DIM_CURRENCYID_TRA,
p.DIM_CURRENCYID_GBL,
p.DW_INSERT_DATE,
p.DW_UPDATE_DATE,
p.DIM_PROJECTSOURCEID,
p.DD_FORECASTPROFILE,
p.STD_EXCHANGERATE_DATEID,
p.DD_PRODUCTIONVERSION,
p.AMT_COMPONENT_SCRAP,
p.AMT_OPERATION_SCRAP,
p.CT_SCRAP_QUANTITY,
current_timestamp as DeleteTime 

from fact_planorder p where dd_PlanOrderNo not in (select distinct PLAF_PLNUM from PLAF)
and exists (select 1 from PLAF);
delete from fact_planorder where dd_PlanOrderNo not in (select distinct PLAF_PLNUM from PLAF)
and exists (select 1 from PLAF);
/* next 2 updates are absolete now that we delete the records from the fact as there will never be records to be updated*/

/*Inactive old plan orders - Q1a*/
UPDATE fact_planorder po
   SET po.Dim_ActionStateid = 3
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 WHERE NOT EXISTS
          (SELECT 1
             FROM PLAF p
            WHERE p.PLAF_PLNUM = po.dd_PlanOrderNo)
AND po.Dim_ActionStateid <> 3;

/*Inactive old plan orders - Q1b*/
UPDATE fact_planorder po
   SET po.ct_Completed = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
 WHERE NOT EXISTS
          (SELECT 1
             FROM PLAF p
            WHERE p.PLAF_PLNUM = po.dd_PlanOrderNo)
AND po.ct_Completed <> 1;

/*Update plan order*/
DROP TABLE IF EXISTS tmp_fpo_mbew_no_bwtar;
CREATE TABLE tmp_fpo_mbew_no_bwtar AS SELECT ifnull((CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0) calcPrice,
                        sp.BWKEY,
                        sp.MATNR
                FROM mbew_no_bwtar sp
               WHERE  ((sp.LFGJA * 100) + sp.LFMON) =
                            (SELECT max((x.LFGJA * 100) + x.LFMON)
                               FROM mbew_no_bwtar x
                              WHERE x.MATNR = sp.MATNR AND x.BWKEY = sp.BWKEY
                                    AND ((x.VPRSV = 'S' AND x.STPRS > 0)
                                         OR (x.VPRSV = 'V' AND x.VERPR > 0)))
                     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0)
                          OR (sp.VPRSV = 'V' AND sp.VERPR > 0));

/* Q2 - column 1 - Dim_Companyid*/

UPDATE fact_planorder po
SET po.Dim_Companyid = dc.Dim_Companyid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Companyid <> dc.Dim_Companyid;


/* Q2 - column 2 - Dim_Partid */
UPDATE fact_planorder po
SET po.Dim_Partid = dp.Dim_Partid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM plaf p,
	dim_plant pl,
	dim_company dc,
	dim_currency c,
	dim_part dp,
	dim_unitofmeasure uom,
	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Partid <> dp.Dim_Partid;

/* Q2 - column 3 - Dim_mpnid */
UPDATE fact_planorder po
SET po.Dim_mpnid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	fact_planorder po

WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_mpnid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_mpnid = mpn.dim_partid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_part mpn, fact_planorder po

WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND CONVERT( varchar (7), uom.UOM) = CONVERT (varchar (7), p.plaf_meins)
AND dc.Currency = c.CurrencyCode
AND mpn.PartNumber = p.PLAF_EMATN
AND mpn.Plant = p.PLAF_PLWRK
AND mpn.RowIsCurrent = 1;


/* Q2 - column 4 - Dim_StorageLocationid */

UPDATE fact_planorder po
SET po.Dim_StorageLocationid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po

WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_StorageLocationid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_StorageLocationid = sl.Dim_StorageLocationid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_storagelocation sl, fact_planorder po

WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sl.LocationCode = p.plaf_lgort
AND sl.Plant = p.PLAF_PLWRK
AND sl.RowIsCurrent = 1;



/* Q2 - column 5 - Dim_Plantid */

UPDATE fact_planorder po
SET po.Dim_Plantid = pl.Dim_Plantid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po

WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Plantid  <> pl.Dim_Plantid;


/* Q2 - column6 - Dim_UnitOfMeasureid */
UPDATE fact_planorder po
SET po.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_UnitOfMeasureid  <> uom.Dim_UnitOfMeasureid;


/* Q2 - column7 - Dim_PurchaseOrgid */
UPDATE fact_planorder po
SET po.Dim_PurchaseOrgid = pog.Dim_PurchaseOrgid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,dim_purchaseorg pog, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pog.PurchaseOrgCode = pl.PurchOrg
AND po.Dim_PurchaseOrgid  <> pog.Dim_PurchaseOrgid;



/* Q2 - column8 - Dim_Vendorid */
UPDATE fact_planorder po
SET po.Dim_Vendorid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Vendorid <> 1;

UPDATE fact_planorder po
SET po.Dim_Vendorid = dv.Dim_Vendorid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_vendor dv, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dv.VendorNumber = p.PLAF_EMLIF;


/* Q2 - column9 - Dim_FixedVendorid */

UPDATE fact_planorder po
SET po.Dim_FixedVendorid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_FixedVendorid <> 1;


UPDATE fact_planorder po
SET po.Dim_FixedVendorid = fv.Dim_Vendorid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/

   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_vendor fv, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND fv.VendorNumber = p.PLAF_FLIEF;


/* Q2 - column10 - Dim_SpecialProcurementid */
UPDATE fact_planorder po
SET po.Dim_SpecialProcurementid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_SpecialProcurementid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_SpecialProcurementid = sp.Dim_SpecialProcurementid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_specialprocurement sp, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sp.specialprocurement = p.PLAF_SOBES
AND ifnull(po.Dim_SpecialProcurementid,-1) <> ifnull(sp.Dim_SpecialProcurementid,-2);

/* Q2 - column11 - Dim_ConsumptionTypeid */
UPDATE fact_planorder po
SET po.Dim_ConsumptionTypeid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_ConsumptionTypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_ConsumptionTypeid = dcp.Dim_ConsumptionTypeid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_consumptiontype dcp, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dcp.ConsumptionCode = p.PLAF_KZVBR
AND ifnull(po.Dim_ConsumptionTypeid,-1) <> ifnull(dcp.Dim_ConsumptionTypeid,-2);

/* Q2 - column12 - Dim_AccountCategoryid */
UPDATE fact_planorder po
SET po.Dim_AccountCategoryid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_AccountCategoryid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_AccountCategoryid = ac.Dim_AccountCategoryid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_accountcategory ac, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ac.Category = p.plaf_knttp
AND ifnull(po.Dim_AccountCategoryid,-1) <> ifnull(ac.Dim_AccountCategoryid,-2);


/* Q2 - column13 - Dim_SpecialStockid */

UPDATE fact_planorder po
SET po.Dim_SpecialStockid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_SpecialStockid,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_SpecialStockid = st.Dim_SpecialStockid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_specialstock st,fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND st.specialstockindicator = p.PLAF_SOBKZ
AND ifnull(po.Dim_SpecialStockid,-1) <> ifnull(st.Dim_SpecialStockid,-2);

/* Q2 - column14 - dim_bomstatusid */
UPDATE fact_planorder po
SET po.dim_bomstatusid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
 		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_bomstatusid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_bomstatusid = bs.dim_bomstatusid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_bomstatus bs, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND bs.BOMStatusCode = p.plaf_ststa
AND ifnull(po.dim_bomstatusid,-1) <> ifnull(bs.dim_bomstatusid,-2);

/* Q2 - column15 - dim_bomusageid */
UPDATE fact_planorder po
SET po.dim_bomusageid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_bomusageid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_bomusageid = bu.dim_bomusageid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_bomusage bu, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND bu.BOMUsageCode = p.plaf_stlan
AND ifnull(po.dim_bomusageid,-1) <> ifnull(bu.dim_bomusageid,-2);

/* Q2 - column16 - dim_objecttypeid */
UPDATE fact_planorder po
SET po.dim_objecttypeid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_objecttypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_objecttypeid = ot.dim_objecttypeid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_objecttype ot, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ot.ObjectType = p.PLAF_OBART
AND ifnull(po.dim_objecttypeid,-1) <> ifnull(ot.dim_objecttypeid,-2);


/* Q2 - column17 - dim_productionschedulerid */
UPDATE fact_planorder po
SET po.dim_productionschedulerid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_productionschedulerid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_productionschedulerid = ps.dim_productionschedulerid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_productionscheduler ps, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ps.ProductionScheduler = p.PLAF_PLGRP
AND ps.Plant = p.PLAF_PLWRK
AND ifnull(po.dim_productionschedulerid,-1) <> ifnull(ps.dim_productionschedulerid,-2);


/* Q2 - column18 - dim_schedulingerrorid */
UPDATE fact_planorder po
SET po.dim_schedulingerrorid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_schedulingerrorid,-1) <> 1;



UPDATE fact_planorder po
SET po.dim_schedulingerrorid = se.dim_schedulingerrorid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_schedulingerror se, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND se.SchedulingErrorCode = p.PLAF_TRMER
AND ifnull(po.dim_schedulingerrorid,-1) <> ifnull(se.dim_schedulingerrorid,-2);


/* Q2 - column19 - dim_tasklisttypeid */
UPDATE fact_planorder po
SET po.dim_tasklisttypeid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_tasklisttypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_tasklisttypeid = tst.dim_tasklisttypeid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_tasklisttype tst,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND tst.TaskListTypeCode = p.PLAF_PLNTY
AND ifnull(po.dim_tasklisttypeid,-1) <> ifnull(tst.dim_tasklisttypeid,-2);


/* Q2 - column20 - dim_ordertypeid */
UPDATE fact_planorder po
SET po.dim_ordertypeid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_ordertypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_ordertypeid = ordt.dim_ordertypeid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_ordertype ordt, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ordt.OrderTypeCode = p.PLAF_PAART
AND ifnull(po.dim_ordertypeid,-1) <> ifnull(ordt.dim_ordertypeid,-2);



/* Q2 - column21 - Dim_dateidStart */
UPDATE fact_planorder po
SET po.Dim_dateidStart = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidStart,-1) <> 1;



UPDATE fact_planorder po
SET po.Dim_dateidStart = ds.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date ds, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ds.DateValue = p.PLAF_PSTTR
AND pl.CompanyCode = ds.CompanyCode and ds.plantcode_factory=pl.plantcode
AND ifnull(po.Dim_dateidStart,-1) <> ifnull(ds.dim_dateid,-2);


/* Q2 - column22 - Dim_dateidFinish */
UPDATE fact_planorder po
SET po.Dim_dateidFinish = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidFinish,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_dateidFinish = df.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_PEDTR
AND pl.CompanyCode = df.CompanyCode and df.plantcode_factory=pl.plantcode
AND p.PLAF_PEDTR IS NOT NULL
AND ifnull(po.Dim_dateidFinish,-1) <> ifnull(df.dim_dateid,-2);



/* Q2 - column23 - Dim_dateidOpening */
UPDATE fact_planorder po
SET po.Dim_dateidOpening = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidOpening,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_dateidOpening = dop.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date dop, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dop.DateValue = p.PLAF_PERTR
AND pl.CompanyCode = dop.CompanyCode and dop.plantcode_factory=pl.plantcode
AND p.PLAF_PERTR IS NOT NULL
AND ifnull(po.Dim_dateidOpening,-1) <> ifnull(dop.dim_dateid,-2);


/* Q2 - column24 - ct_QtyTotal */
UPDATE fact_planorder po
SET po.ct_QtyTotal = p.PLAF_GSMNG
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.ct_QtyTotal <> p.PLAF_GSMNG;



/* Q2 - column25 - amt_ExtendedPrice : LK - This query can be tuned further if required */
	DROP TABLE IF EXISTS tmp_fact_planorder_t001;
	create table tmp_fact_planorder_t001 as
	select  sp.calcPrice, p.plaf_plnum
	FROM 	plaf p,
			dim_plant pl,
			dim_part dp,
			tmp_fpo_mbew_no_bwtar sp
	WHERE pl.PlantCode = p.PLAF_PLWRK
	AND dp.PartNumber = p.PLAF_MATNR
	AND dp.Plant = p.PLAF_PLWRK
	AND sp.MATNR = dp.PartNumber
	AND sp.BWKEY = pl.ValuationArea;

	UPDATE fact_planorder po
	SET po.amt_ExtendedPrice = ifnull(tmp.calcPrice,0)* p.PLAF_GSMNG
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	FROM fact_planorder po
			INNER JOIN plaf p ON po.dd_PlanOrderNo = p.plaf_plnum
			LEFT JOIN tmp_fact_planorder_t001 tmp ON tmp.plaf_plnum = po.dd_PlanOrderNo
	WHERE po.Dim_ActionStateid = 2
	AND po.amt_ExtendedPrice <> ifnull(tmp.calcPrice,0)* p.PLAF_GSMNG;

	DROP TABLE IF EXISTS tmp_fact_planorder_t001;
	/* Original

				UPDATE fact_planorder po
			   FROM plaf p,
					dim_plant pl,
					dim_company dc,
					dim_currency c,
					dim_part dp,
					dim_unitofmeasure uom
			SET        amt_ExtendedPrice =
					  (ifnull((SELECT calcPrice
							FROM tmp_fpo_mbew_no_bwtar sp
						   WHERE sp.MATNR = dp.PartNumber AND sp.BWKEY = pl.ValuationArea),
						 0)
					   * p.PLAF_GSMNG)
			WHERE     po.dd_PlanOrderNo = p.plaf_plnum
			AND po.Dim_ActionStateid = 2
			AND pl.PlantCode = p.PLAF_PLWRK
			AND dc.CompanyCode = pl.CompanyCode
			AND dp.PartNumber = p.PLAF_MATNR
			AND dp.Plant = p.PLAF_PLWRK
			AND uom.UOM = p.plaf_meins
			AND dc.Currency = c.CurrencyCode*/

/* Q2 - column26 - dd_bomexplosionno */
UPDATE fact_planorder po
SET po.dd_bomexplosionno = p.PLAF_SERNR
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_bomexplosionno,'xx') <> ifnull(p.PLAF_SERNR,'yy');


/* Q2 - column27 - ct_QtyReduced */
UPDATE fact_planorder po
SET po.ct_QtyReduced = p.plaf_ABMNG
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyReduced,-1) <> ifnull(p.plaf_ABMNG,-2);


/* Q2 - column28 - Dim_DateidProductionStart */
UPDATE fact_planorder po
SET po.Dim_DateidProductionStart = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidProductionStart,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_DateidProductionStart = df.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_TERST
AND pl.CompanyCode = df.CompanyCode and df.plantcode_factory=pl.plantcode
AND ifnull(po.Dim_DateidProductionStart,-1) <> ifnull(df.dim_dateid,-2);


/* Q2 - column29 - Dim_DateidProductionFinish */
UPDATE fact_planorder po
SET po.Dim_DateidProductionFinish = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidProductionFinish,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_DateidProductionFinish = df.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_TERED
AND pl.CompanyCode = df.CompanyCode and df.plantcode_factory=pl.plantcode
AND ifnull(po.Dim_DateidProductionFinish,-1) <> ifnull(df.dim_dateid,-2);

/* Q2 - column30 - Dim_DateidExplosion */
UPDATE fact_planorder po
SET po.Dim_DateidExplosion = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidExplosion,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_DateidExplosion = df.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_PALTR
AND pl.CompanyCode = df.CompanyCode and df.plantcode_factory=pl.plantcode
AND ifnull(po.Dim_DateidExplosion,-1) <> ifnull(df.dim_dateid,-2);

/* Q2 - column31 - Dim_DateidAction */
UPDATE fact_planorder po
SET po.Dim_DateidAction = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidAction,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_DateidAction = df.dim_dateid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_MDACD
AND pl.CompanyCode = df.CompanyCode and df.plantcode_factory=pl.plantcode
AND ifnull(po.Dim_DateidAction,-1) <> ifnull(df.dim_dateid,-2);


/* Q2 - column32 - dim_scheduletypeid */
UPDATE fact_planorder po
SET po.dim_scheduletypeid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_scheduletypeid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_scheduletypeid = st.dim_scheduletypeid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_scheduletype st, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND st.ScheduleTypeCode = p.PLAF_LVSCH
AND ifnull(po.dim_scheduletypeid,-1) <> ifnull(st.dim_scheduletypeid,-2);



/* Q2 - column33 - dim_availabilityconfirmationid */
UPDATE fact_planorder po
SET po.dim_availabilityconfirmationid = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_availabilityconfirmationid,-1) <> 1;


UPDATE fact_planorder po
SET po.dim_availabilityconfirmationid = acf.dim_availabilityconfirmationid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_availabilityconfirmation acf,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND acf.AvailabilityConfirmationCode = p.PLAF_MDPBV
AND ifnull(po.dim_availabilityconfirmationid,-1) <> ifnull(acf.dim_availabilityconfirmationid,-2);


/* Q2 - column34 - Dim_ProcurementId */
UPDATE fact_planorder po
SET po.Dim_ProcurementId = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
	    fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_ProcurementId,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_ProcurementId = pc.dim_procurementid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, Dim_Procurement pc, fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pc.procurement = p.PLAF_BESKZ
AND ifnull(po.Dim_ProcurementId,-1) <> ifnull(pc.Dim_ProcurementId,-2);



/* Q2 - column35 - Dim_Currencyid */
UPDATE fact_planorder po
SET po.Dim_Currencyid = c.Dim_Currencyid
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dp.Currency = c.CurrencyCode
AND ifnull(po.Dim_Currencyid,-1) <> ifnull(c.Dim_Currencyid,-2);



/* Q2 - column36 - dd_SalesOrderNo */
UPDATE fact_planorder po
SET po.dd_SalesOrderNo = p.PLAF_KDAUF
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderNo,'xx') <> ifnull(p.PLAF_KDAUF,'yy');



/* Q2 - column37 - dd_SalesOrderItemNo */
UPDATE fact_planorder po
SET po.dd_SalesOrderItemNo = p.PLAF_KDPOS
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderItemNo,-1) <> ifnull(p.PLAF_KDPOS,-2);


/* Q2 - column38 - dd_SalesOrderScheduleNo */
UPDATE fact_planorder po
SET po.dd_SalesOrderScheduleNo = p.PLAF_KDEIN
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderScheduleNo,-1) <> ifnull(p.PLAF_KDEIN,-2);


/* Q2 - column39 - ct_GRProcessingTime */
UPDATE fact_planorder po
SET po.ct_GRProcessingTime = p.PLAF_WEBAZ
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_GRProcessingTime,-1) <> ifnull(p.PLAF_WEBAZ,-2);


/* Q2 - column40 - ct_QtyIssued */
UPDATE fact_planorder po
SET po.ct_QtyIssued = p.PLAF_WAMNG
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyIssued,-1) <> ifnull(p.PLAF_WAMNG,-2);


/* Q2 - column41 - ct_QtyCommitted */
UPDATE fact_planorder po
SET po.ct_QtyCommitted = p.PLAF_VFMNG
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyCommitted,-1) <> ifnull(p.PLAF_VFMNG,-2);


/* Q2 - column42 - dd_SequenceNo */
UPDATE fact_planorder po
SET po.dd_SequenceNo = ifnull(p.PLAF_SEQNR, 'Not Set')
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SequenceNo,'xx') <> ifnull(p.PLAF_SEQNR, 'Not Set');


/* Q2 - column43 - dd_PlannScenario */
UPDATE fact_planorder po
SET po.dd_PlannScenario = ifnull(p.PLAF_PLSCN, 'Not Set')
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_PlannScenario,'xx') <> ifnull(p.PLAF_VFMNG,'Not Set');


/* Q2 - column44 - amt_StdUnitPrice */
UPDATE fact_planorder po
SET po.amt_StdUnitPrice = 0
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.amt_StdUnitPrice,-1) <> 0;


UPDATE fact_planorder po
SET po.amt_StdUnitPrice = sp.calcPrice
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        tmp_fpo_mbew_no_bwtar sp,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sp.MATNR = dp.PartNumber
AND sp.BWKEY = pl.ValuationArea
AND ifnull(po.amt_StdUnitPrice,-1) <> ifnull(sp.calcPrice,-2);


/* Q2 - column45 - Dim_MRPControllerId */
UPDATE fact_planorder po
SET po.Dim_MRPControllerId = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_MRPControllerId,-1) <> 1;


UPDATE fact_planorder po
SET po.Dim_MRPControllerId = mc.Dim_MRPControllerId
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,Dim_MRPController mc,  fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND mc.MRPController = PLAF_DISPO AND mc.Plant = PLAF_PLWRK
AND ifnull(po.Dim_MRPControllerId,-1) <> ifnull(mc.Dim_MRPControllerId,-2);



/* Q2 - column46 - dd_AgreementNo */
UPDATE fact_planorder po
SET po.dd_AgreementNo = ifnull(p.PLAF_KONNR, 'Not Set')
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_AgreementNo,'xx') <> ifnull(p.PLAF_KONNR,'Not Set');

/* Q2 - column47 - dd_AgreementItemNo */
UPDATE fact_planorder po
SET po.dd_AgreementItemNo = ifnull(p.PLAF_KTPNR, 0)
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_AgreementItemNo,-1) <> ifnull(p.PLAF_KTPNR,0);


/* Q2 - column48 - dim_dateidscheduledstart */
UPDATE fact_planorder po
SET po.dim_dateidscheduledstart = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
        fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_dateidscheduledstart,-1) <> 1;


drop table if exists tmp_fact_planorder_temp2;
create  table  tmp_fact_planorder_temp2
as select distinct (dd.dim_dateid) dim_dateidscheduledstart, po.fact_planorderid, row_number()
		OVER(partition by po.fact_planorderid ORDER BY dim_dateidscheduledstart DESC) rownumber
FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
	kbko k,
        dim_unitofmeasure uom, dim_date dd,
        fact_planorder po
WHERE  po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND k.kbko_gstrs = dd.DateValue
AND p.plaf_plnum = k.kbko_plnum
AND pl.CompanyCode = dd.CompanyCode and dd.plantcode_factory=pl.plantcode;

update fact_planorder po
set po.dim_dateidscheduledstart = tmp.dim_dateidscheduledstart
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from fact_planorder po, tmp_fact_planorder_temp2 tmp
where po.fact_planorderid = tmp.fact_planorderid
AND ifnull(po.dim_dateidscheduledstart,-1) <> ifnull(tmp.dim_dateidscheduledstart,-2)
AND rownumber = 1;

drop table if exists tmp_fact_planorder_temp2;


/* Q2 - column49 - dim_dateidscheduledfinish */
UPDATE fact_planorder po
SET po.dim_dateidscheduledfinish = 1
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,
		fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_dateidscheduledfinish,-1) <> 1;


DROP TABLE IF EXISTS tmp_fact_planorder_temp2;
CREATE TABLE tmp_fact_planorder_temp2
AS SELECT DISTINCT(dd.dim_dateid) dim_dateidscheduledfinish, fact_planorderid, row_number()
		OVER(partition by po.fact_planorderid ORDER BY dim_dateidscheduledfinish DESC) rownumber
FROM fact_planorder po,
     plaf p,
     dim_plant pl,
     dim_company dc,
     dim_currency c,
     dim_part dp,
	 kbko k,
     dim_unitofmeasure uom, dim_date dd
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode 
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pl.CompanyCode = dd.CompanyCode and dd.plantcode_factory=pl.plantcode
AND k.kbko_gltrs = dd.DateValue
AND p.plaf_plnum = k.kbko_plnum;

UPDATE fact_planorder po
SET po.dim_dateidscheduledfinish = tmp.dim_dateidscheduledfinish
FROM fact_planorder po, tmp_fact_planorder_temp2 tmp
WHERE po.fact_planorderid = tmp.fact_planorderid
AND ifnull(po.dim_dateidscheduledfinish,-1) <> ifnull(tmp.dim_dateidscheduledfinish,-2)
AND rownumber = 1;

DROP TABLE IF EXISTS tmp_fact_planorder_temp2;








/*Insert into the fact table*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_planorder';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_planorder',ifnull(max(fact_planorderid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_planorder;


drop table if exists fact_planorder_tmp;
create table fact_planorder_tmp as select po.dd_PlanOrderNo FROM fact_planorder po;


/* Insert Statement Optimized */


drop table if exists fact_planorder_di09;
Create table fact_planorder_di09 as
Select  a.*,
	convert( varchar(18), null) PARTNUMBER_upd,
    	convert( varchar(20), null) COMPANYCODE_upd,
 	convert( varchar(20), null) VALUATIONAREA_upd,
	convert( varchar(20), null) PLAF_DISPO_upd,
	convert( varchar(20), null) PLAF_EMATN_upd,
	convert( varchar(20), null) PLAF_EMLIF_upd,
	convert( varchar(20), null) PLAF_FLIEF_upd,
   	convert( VARCHAR(1), null) PLAF_KNTTP_upd,
   	convert( VARCHAR(1), null) PLAF_KZVBR_upd,
	convert( varchar(4), null) PLAF_LGORT_upd,
	convert( VARCHAR(1), null) PLAF_LVSCH_upd,
        convert( date, null) PLAF_MDACD_upd,
 	convert( VARCHAR(1), null) PLAF_MDPBV_upd,
        convert( VARCHAR(1), null) PLAF_OBART_upd,
	convert( varchar(10), null) PLAF_PAART_upd,
        convert( date, null) PLAF_PALTR_upd,
        convert( date, null) PLAF_PEDTR_upd,
 	convert( date, null) PLAF_PERTR_upd,
        convert( varchar(3), null) PLAF_PLGRP_upd,
	convert( VARCHAR(1), null) PLAF_PLNTY_upd,
	convert( varchar(4), null) PLAF_PLWRK_upd,
	convert( varchar(1), null) PLAF_SOBES_upd,
	convert( VARCHAR(1), null) PLAF_SOBKZ_upd,
	convert( VARCHAR(1), null) PLAF_STLAN_upd,
	convert( DECIMAL (18,0), null) PLAF_STSTA_upd,
	convert( date, null) PLAF_TERED_upd,
	convert( date, null) PLAF_TERST_upd,
	convert( varchar(2), null) PLAF_TRMER_upd,
	convert( varchar(20), null) PlantCode_upd,
	convert( varchar(18), null) PLAF_MATNR_upd,
	convert( DECIMAL (9,0),null) CalendarYear_upd,
	convert( DECIMAL (9,0),null) FinancialMonthNumber_upd,
	convert( decimal (18,4),null) PLAF_UMREZ_upd,
	convert( decimal (18,4),null) PLAF_UMREN_upd,
	convert( decimal (18,4),null) PLAF_GSMNG_upd,
	convert( varchar(1), null) PLAF_BESKZ_upd
FROM fact_planorder a
where 1=2;

/* Optimize NOT EXISTS */


DROP TABLE IF EXISTS tmp_ins_fact_planorder;
CREATE TABLE tmp_ins_fact_planorder
AS
SELECT p.plaf_plnum dd_PlanOrderNo
    from plaf p;


/* Need exactly the same table structure for combine to work */
DROP TABLE IF EXISTS tmp_del_fact_planorder;
CREATE TABLE tmp_del_fact_planorder
AS
SELECT * FROM tmp_ins_fact_planorder where 1 = 2;

INSERT INTO tmp_del_fact_planorder
SELECT po.dd_PlanOrderNo
FROM fact_planorder_tmp po;

delete from tmp_ins_fact_planorder
where dd_PlanOrderNo in (Select distinct dd_PlanOrderNo from tmp_del_fact_planorder);


DROP TABLE IF EXISTS tmp_ins_plaf_fact_planorder;
CREATE TABLE tmp_ins_plaf_fact_planorder
AS
SELECT p.*
FROM plaf p, tmp_ins_fact_planorder t
WHERE p.plaf_plnum = t.dd_PlanOrderNo;

INSERT INTO fact_planorder_di09(fact_planorderid,
                           Dim_ActionStateid,
                           Dim_Companyid,
                           Dim_Partid,
                           Dim_mpnid,
                           Dim_StorageLocationid,
                           Dim_Plantid,
                           Dim_UnitOfMeasureid,
                           Dim_PurchaseOrgid,
                           Dim_Vendorid,
                           Dim_FixedVendorid,
                           Dim_SpecialProcurementid,
                           Dim_ConsumptionTypeid,
                           Dim_AccountCategoryid,
                           Dim_SpecialStockid,
                           dim_bomstatusid,
                           dim_bomusageid,
                           dim_objecttypeid,
                           dim_productionschedulerid,
                           dim_schedulingerrorid,
                           dim_tasklisttypeid,
                           dim_ordertypeid,
                           Dim_dateidStart,
                           Dim_dateidFinish,
                           Dim_dateidOpening,
                           ct_QtyTotal,
                           ct_Completed,
                           amt_ExtendedPrice,
                           dd_PlanOrderNo,
                           dd_bomexplosionno,
                           ct_QtyReduced,
                           Dim_PlanOrderStatusid,
                           Dim_DateidConversionDate,
                           Dim_DateidProductionStart,
                           Dim_DateidProductionFinish,
                           Dim_DateidExplosion,
                           Dim_DateidAction,
                           dim_scheduletypeid,
                           dim_availabilityconfirmationid,
                           dim_currencyid,
						   	dim_currencyid_TRA,
							dim_currencyid_GBL,
							amt_ExchangeRate,
							amt_ExchangeRate_GBL,
                           Dim_ProcurementId,
                           dd_SalesOrderNo,
                           dd_SalesOrderItemNo,
                           dd_SalesOrderScheduleNo,
                           ct_GRProcessingTime,
                           ct_QtyIssued,
                           ct_QtyCommitted,
                           dd_SequenceNo,
                           dd_PlannScenario,
                           amt_StdUnitPrice,
                           Dim_MRPControllerId,
                           dd_AgreementNo,
                           dd_AgreementItemNo,
			PARTNUMBER_upd,
			COMPANYCODE_upd,
			VALUATIONAREA_upd,
			PLAF_DISPO_upd,
			PLAF_EMATN_upd,
			PLAF_EMLIF_upd,
			PLAF_FLIEF_upd,
			PLAF_KNTTP_upd,
			PLAF_KZVBR_upd,
			PLAF_LGORT_upd,
			PLAF_LVSCH_upd,
			PLAF_MDACD_upd,
			PLAF_MDPBV_upd,
			PLAF_OBART_upd,
			PLAF_PAART_upd,
			PLAF_PALTR_upd,
			PLAF_PEDTR_upd,
			PLAF_PERTR_upd,
			PLAF_PLGRP_upd,
			PLAF_PLNTY_upd,
			PLAF_PLWRK_upd,
			PLAF_SOBES_upd,
			PLAF_SOBKZ_upd,
			PLAF_STLAN_upd,
			PLAF_STSTA_upd,
			PLAF_TERED_upd,
			PLAF_TERST_upd,
			PLAF_TRMER_upd,
			PlantCode_upd,
			PLAF_MATNR_upd,
			CalendarYear_upd,
			FinancialMonthNumber_upd,
			PLAF_UMREZ_upd,
			PLAF_UMREN_upd,
			PLAF_GSMNG_upd,
			PLAF_BESKZ_upd)
   SELECT IFNULL((SELECT MAX_ID FROM NUMBER_FOUNTAIN
WHERE TABLE_NAME = 'fact_planorder' ), 1) + ROW_NUMBER() OVER(order by '')
fact_planorderid,
	2 Dim_ActionStateid,
	Dim_Companyid,
	dp.Dim_Partid,
	1 Dim_mpnid,
	1 Dim_StorageLocationid,
	Dim_Plantid,
	Dim_UnitOfMeasureid,
	Dim_PurchaseOrgid,
	1 Dim_Vendorid,
	1 Dim_FixedVendorid,
	1 Dim_SpecialProcurementid,
	1 Dim_ConsumptionTypeid,
	1 Dim_AccountCategoryid,
	1 Dim_SpecialStockid,
	1 dim_bomstatusid,
	1 dim_bomusageid,
	1 dim_objecttypeid,
	1 dim_productionschedulerid,
	1 dim_schedulingerrorid,
	1 dim_tasklisttypeid,
	1 dim_ordertypeid,
	ds.dim_dateid Dim_dateidStart,
	1 Dim_dateidFinish,
	1 Dim_dateidOpening,
	p.PLAF_GSMNG ct_QtyTotal,
	0 ct_Completed,
	0 amt_ExtendedPrice,
	p.PLAF_PLNUM dd_PlanOrderNo,
	p.PLAF_SERNR dd_bomexplosionno,
	p.plaf_ABMNG ct_QtyReduced,
	1 Dim_PlanOrderStatusid,
	1 Dim_DateidConversionDate,
	1 Dim_DateidProductionStart,
	1 Dim_DateidProductionFinish,
	1 Dim_DateidExplosion,
	1 Dim_DateidAction,
	1 dim_scheduletypeid,
	1 dim_availabilityconfirmationid,
	c.dim_currencyid,
	c.dim_currencyid dim_currencyid_TRA,  /*Default */
        convert(bigint, 1) dim_currencyid_GBL,
        1 amt_ExchangeRate,           /*Default */
        convert(decimal (18,4), 0) amt_ExchangeRate_GBL,   /* Order finish date used to calculate exchg rate*/
	1 Dim_ProcurementId,
	p.PLAF_KDAUF dd_SalesOrderNo,
	p.PLAF_KDPOS dd_SalesOrderItemNo,
	p.PLAF_KDEIN dd_SalesOrderScheduleNo,
	p.PLAF_WEBAZ ct_GRProcessingTime,
	p.PLAF_WAMNG ct_QtyIssued,
	p.PLAF_VFMNG ct_QtyCommitted,
	ifnull(p.PLAF_SEQNR, 'Not Set'),
	ifnull(p.PLAF_PLSCN, 'Not Set'),
	0 amt_StdUnitPrice,
	1 Dim_MRPControllerId,
        ifnull(PLAF_KONNR,'Not Set') dd_AgreementNo,
        ifnull(PLAF_KTPNR,0) dd_AgreementItemNo,
	dp.PARTNUMBER PARTNUMBER_upd,
	pl.COMPANYCODE COMPANYCODE_upd,
	pl.VALUATIONAREA VALUATIONAREA_upd,
	p.PLAF_DISPO PLAF_DISPO_upd,
	p.PLAF_EMATN PLAF_EMATN_upd,
	p.PLAF_EMLIF PLAF_EMLIF_upd,
	p.PLAF_FLIEF PLAF_FLIEF_upd,
	p.PLAF_KNTTP PLAF_KNTTP_upd,
	p.PLAF_KZVBR PLAF_KZVBR_upd,
	p.PLAF_LGORT PLAF_LGORT_upd,
	p.PLAF_LVSCH PLAF_LVSCH_upd,
	p.PLAF_MDACD PLAF_MDACD_upd,
	p.PLAF_MDPBV PLAF_MDPBV_upd,
	p.PLAF_OBART PLAF_OBART_upd,
	p.PLAF_PAART PLAF_PAART_upd,
	p.PLAF_PALTR PLAF_PALTR_upd,
	p.PLAF_PEDTR PLAF_PEDTR_upd,
	p.PLAF_PERTR PLAF_PERTR_upd,
	p.PLAF_PLGRP PLAF_PLGRP_upd,
	p.PLAF_PLNTY PLAF_PLNTY_upd,
	p.PLAF_PLWRK PLAF_PLWRK_upd,
	p.PLAF_SOBES PLAF_SOBES_upd,
	p.PLAF_SOBKZ PLAF_SOBKZ_upd,
	p.PLAF_STLAN PLAF_STLAN_upd,
	p.PLAF_STSTA PLAF_STSTA_upd,
	p.PLAF_TERED PLAF_TERED_upd,
	p.PLAF_TERST PLAF_TERST_upd,
	p.PLAF_TRMER PLAF_TRMER_upd,
	pl.PlantCode PlantCode_upd,
        p.PLAF_MATNR PLAF_MATNR_upd,
        ds.CalendarYear CalendarYear_upd,
        ds.FinancialMonthNumber FinancialMonthNumber_upd,
        p.PLAF_UMREZ PLAF_UMREZ_upd,
        p.PLAF_UMREN PLAF_UMREN_upd,
        p.PLAF_GSMNG PLAF_GSMNG_upd,
        p.PLAF_BESKZ PLAF_BESKZ_upd
FROM tmp_ins_plaf_fact_planorder p
          INNER JOIN dim_plant pl
             ON pl.PlantCode = p.PLAF_PLWRK
          INNER JOIN dim_company dc
             ON dc.CompanyCode = pl.CompanyCode
          INNER JOIN Dim_Currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date ds
             ON ds.DateValue = p.PLAF_PSTTR
                AND pl.CompanyCode = ds.CompanyCode and ds.plantcode_factory=pl.plantcode
          INNER JOIN dim_part dp
             ON     dp.PartNumber = p.PLAF_MATNR
                AND dp.Plant = p.PLAF_PLWRK
          INNER JOIN dim_unitofmeasure uom
             ON CONVERT(varchar (7), uom.UOM) = CONVERT(varchar (7), p.plaf_meins)
          INNER JOIN dim_purchaseorg pog
             ON pog.PurchaseOrgCode = pl.PurchOrg;


update fact_planorder_di09 f
set Dim_Currencyid_GBL = t.Dim_Currencyid
from fact_planorder_di09 f
		CROSS JOIN (SELECT c.Dim_Currencyid
					FROM dim_currency c, tmp_pGlobalCurrency_planorder tc
                    WHERE c.CurrencyCode = tc.pGlobalCurrency ) t
where Dim_Currencyid_GBL <> t.Dim_Currencyid;

/*CURRENCY LOGIC CHANGED - ALIN - 19 FEB 2018*/
update fact_planorder_di09 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_planorder_di09 f
      INNER JOIN dim_part dc ON f.dim_partid = dc.dim_partid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z
		      INNER JOIN tmp_pGlobalCurrency_planorder tc ON z.pToCurrency = tc.pGlobalCurrency
				  WHERE     z.fact_script_name = 'bi_populate_planorder_fact'
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);

   /* WHERE NOT EXISTS
             (SELECT 1
                FROM fact_planorder_tmp po
               WHERE po.dd_PlanOrderNo = p.plaf_plnum)*/

Update fact_planorder_di09
SET Dim_mpnid= IFNULL( mpn.dim_partid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN  dim_part mpn ON mpn.PartNumber = f.PLAF_EMATN_upd
						AND mpn.Plant = f.PLAF_PLWRK_upd;


Update fact_planorder_di09 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = plaf_lgort_upd
						 AND sl.Plant = f.PLAF_PLWRK_upd;

Update fact_planorder_di09 f
Set f.Dim_Vendorid=ifnull( dv.Dim_Vendorid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_vendor dv ON  dv.VendorNumber = f.PLAF_EMLIF_upd;



Update fact_planorder_di09 f
Set f.Dim_FixedVendorid=ifnull( fv.Dim_Vendorid,1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_vendor fv ON fv.VendorNumber = f.PLAF_FLIEF_upd;



Update fact_planorder_di09 f
Set f.Dim_SpecialProcurementid = ifnull(sp.Dim_SpecialProcurementid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_specialprocurement sp ON sp.specialprocurement = f.PLAF_SOBES_upd;



Update fact_planorder_di09 f
Set f.Dim_ConsumptionTypeid = ifnull(dcp.Dim_ConsumptionTypeid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_consumptiontype dcp ON dcp.ConsumptionCode = f.PLAF_KZVBR_upd;



Update fact_planorder_di09 f
Set f.Dim_AccountCategoryid = ifnull( ac.Dim_AccountCategoryid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_accountcategory ac ON ac.Category = f.plaf_knttp_upd;


Update fact_planorder_di09 f
Set f.Dim_SpecialStockid=ifnull( st.Dim_SpecialStockid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_specialstock st ON st.specialstockindicator = f.PLAF_SOBKZ_upd;


Update fact_planorder_di09 f
Set f.dim_bomstatusid=ifnull( bs.dim_bomstatusid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_bomstatus bs ON bs.BOMStatusCode = f.plaf_ststa_upd;


Update fact_planorder_di09 f
Set f.dim_bomusageid = ifnull( bu.dim_bomusageid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_bomusage bu ON bu.BOMUsageCode = f.plaf_stlan_upd;


Update fact_planorder_di09 f
Set f.dim_objecttypeid = ifnull( ot.dim_objecttypeid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_objecttype ot ON ot.ObjectType = f.PLAF_OBART_upd;


Update fact_planorder_di09 f
Set f.dim_productionschedulerid = ifnull(ps.dim_productionschedulerid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_productionscheduler ps ON ps.ProductionScheduler = f.PLAF_PLGRP_upd
							AND ps.Plant = f.PLAF_PLWRK_upd;

Update fact_planorder_di09 f
Set f.dim_schedulingerrorid = ifnull( se.dim_schedulingerrorid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_schedulingerror se ON se.SchedulingErrorCode = f.PLAF_TRMER_upd;


Update fact_planorder_di09 f
Set f.dim_tasklisttypeid=ifnull( tst.dim_tasklisttypeid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_tasklisttype tst ON tst.TaskListTypeCode = f.PLAF_PLNTY_upd;


Update fact_planorder_di09 f
Set f.dim_ordertypeid = ifnull( ordt.dim_ordertypeid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN dim_ordertype ordt ON ordt.OrderTypeCode = PLAF_PAART_upd;

merge into fact_planorder_di09 f
using (select distinct fact_planorderid, df.dim_dateid
FROM  fact_planorder_di09 f
inner join dim_plant pl on f.dim_plantid=pl.dim_plantid
		LEFT JOIN dim_date df ON df.DateValue = f.PLAF_PEDTR_upd
		                      AND df.CompanyCode = CompanyCode_upd
							  AND df.plantcode_factory=pl.plantcode) t
on t.fact_planorderid=f.fact_planorderid
when matched then update set f.Dim_dateidFinish=ifnull(t.dim_dateid, 1);



merge into fact_planorder_di09 f
using (select distinct fact_planorderid, dop.dim_dateid
FROM fact_planorder_di09 f
inner join dim_plant pl on f.dim_plantid=pl.dim_plantid
		LEFT JOIN dim_date dop ON dop.DateValue = f.PLAF_PERTR_upd
					AND CompanyCode_upd = dop.CompanyCode
                    AND dop.plantcode_factory=pl.plantcode) t
on t.fact_planorderid=f.fact_planorderid
when matched then update set f.Dim_dateidOpening = ifnull(t.dim_dateid, 1);


merge into fact_planorder_di09 f
using (select distinct fact_planorderid, df.dim_dateid
FROM  fact_planorder_di09 f
inner join dim_plant pl on f.dim_plantid=pl.dim_plantid
		LEFT JOIN dim_date df ON df.DateValue = f.PLAF_TERST_upd
					AND CompanyCode_upd = df.CompanyCode
                     AND df.plantcode_factory=pl.plantcode) t
on t.fact_planorderid=f.fact_planorderid
when matched then update set  f.Dim_DateidProductionStart = ifnull( t.dim_dateid, 1);


merge into fact_planorder_di09 f
using (select distinct fact_planorderid, df.dim_dateid
FROM fact_planorder_di09 f
inner join dim_plant pl on f.dim_plantid=pl.dim_plantid
		LEFT JOIN dim_date df ON df.DateValue = PLAF_TERED_upd
					AND CompanyCode_upd = df.CompanyCode
AND df.plantcode_factory=pl.plantcode) t
on t.fact_planorderid=f.fact_planorderid
when matched then update set f.Dim_DateidProductionFinish=ifnull(t.dim_dateid, 1);

merge into fact_planorder_di09 f
using (select distinct fact_planorderid, df.dim_dateid
FROM  fact_planorder_di09 f
inner join dim_plant pl on f.dim_plantid=pl.dim_plantid
		LEFT JOIN dim_date df ON df.DateValue = f.PLAF_PALTR_upd
					AND CompanyCode_upd = df.CompanyCode
AND df.plantcode_factory=pl.plantcode) t
on t.fact_planorderid=f.fact_planorderid
when matched then update set f.Dim_DateidExplosion=ifnull(t.dim_dateid, 1);

merge into fact_planorder_di09 f
using (select distinct fact_planorderid, df.dim_dateid
FROM fact_planorder_di09 f
inner join dim_plant pl on f.dim_plantid=pl.dim_plantid
		LEFT JOIN dim_date df ON df.DateValue = f.PLAF_MDACD_upd
					AND CompanyCode_upd = df.CompanyCode
AND df.plantcode_factory=pl.plantcode) t
on t.fact_planorderid=f.fact_planorderid
when matched then update set f.Dim_DateidAction=ifnull(t.dim_dateid, 1);


Update fact_planorder_di09 f
Set f.dim_scheduletypeid = ifnull(st.dim_scheduletypeid, 1)
FROM   fact_planorder_di09 f
		LEFT JOIN dim_scheduletype st ON st.ScheduleTypeCode = f.PLAF_LVSCH_upd;


Update fact_planorder_di09 f
Set f.dim_availabilityconfirmationid= ifnull(acf.dim_availabilityconfirmationid, 1)
FROM  fact_planorder_di09 f
		LEFT JOIN dim_availabilityconfirmation acf ON acf.AvailabilityConfirmationCode = PLAF_MDPBV_upd;


Update fact_planorder_di09
Set amt_StdUnitPrice = ifnull(
                        (CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0)
FROM  fact_planorder_di09
     LEFT JOIN  mbew_no_bwtar sp
                ON sp.MATNR = PartNumber_upd AND sp.BWKEY = ValuationArea_upd
WHERE  ((sp.LFGJA * 100) + sp.LFMON) = (SELECT max((x.LFGJA * 100) + x.LFMON)
							       		FROM mbew_no_bwtar x
							       		WHERE    x.MATNR = sp.MATNR
											 AND x.BWKEY = sp.BWKEY
							                 AND (   (x.VPRSV = 'S' AND x.STPRS > 0)
									              OR (x.VPRSV = 'V' AND x.VERPR > 0)
												 )
                                        )
	AND (   (sp.VPRSV = 'S' AND sp.STPRS > 0)
		 OR (sp.VPRSV = 'V' AND sp.VERPR > 0)) ;


/*  ORIGINAL

Update fact_planorder_di09
Set amt_StdUnitPrice = ifnull(
             (SELECT ifnull(
                        (CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0)
                FROM mbew_no_bwtar sp
               WHERE sp.MATNR = PartNumber_upd AND sp.BWKEY = ValuationArea_upd
                     AND ((sp.LFGJA * 100) + sp.LFMON) =
                            (SELECT max((x.LFGJA * 100) + x.LFMON)
                               FROM mbew_no_bwtar x
                              WHERE x.MATNR = sp.MATNR AND x.BWKEY = sp.BWKEY
                                    AND ((x.VPRSV = 'S' AND x.STPRS > 0)
                                         OR (x.VPRSV = 'V' AND x.VERPR > 0)))
                     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0)
                          OR (sp.VPRSV = 'V' AND sp.VERPR > 0)) ), 0) */

Update fact_planorder_di09 f
Set f.Dim_MRPControllerId = ifnull(mc.Dim_MRPControllerId, 1)
FROM fact_planorder_di09 f
		LEFT JOIN Dim_MRPController mc ON mc.MRPController = f.PLAF_DISPO_upd AND mc.Plant = PLAF_PLWRK_upd;




UPDATE fact_planorder_di09
SET amt_ExtendedPrice = ifnull(standardprice, 0)* PLAF_GSMNG_upd
FROM fact_planorder_di09 f
                LEFT JOIN (select max(STANDARDPRICE)STANDARDPRICE,pCompanyCode,pPlant,pMaterialNo,pFiYear,pPeriod,vUMREZ,vUMREN from tmp_getStdPrice where fact_script_name = 'bi_populate_planorder_fact'
                 group by pCompanyCode,pPlant,pMaterialNo,pFiYear,pPeriod,vUMREZ,vUMREN) tmp ON tmp.pCompanyCode = f.COMPANYCODE_upd AND tmp.pPlant = f.PlantCode_upd
        AND pMaterialNo = f.PLAF_MATNR_upd
        AND tmp.pFiYear = f.CalendarYear_upd
        AND tmp.pPeriod = f.FinancialMonthNumber_upd
        AND vUMREZ = CASE WHEN PLAF_UMREZ_upd = 0 THEN 1 ELSE PLAF_UMREZ_upd END
        AND vUMREN = CASE WHEN PLAF_UMREN_upd = 0 THEN 1 ELSE PLAF_UMREN_upd END;




Update fact_planorder_di09 f
Set f.Dim_ProcurementId = ifnull( pc.dim_procurementid, 1)
FROM fact_planorder_di09 f
		LEFT JOIN Dim_Procurement pc ON pc.procurement = f.PLAF_BESKZ_upd;





INSERT INTO fact_planorder(fact_planorderid,
                           Dim_ActionStateid,
                           Dim_Companyid,
                           Dim_Partid,
                           Dim_mpnid,
                           Dim_StorageLocationid,
                           Dim_Plantid,
                           Dim_UnitOfMeasureid,
                           Dim_PurchaseOrgid,
                           Dim_Vendorid,
                           Dim_FixedVendorid,
                           Dim_SpecialProcurementid,
                           Dim_ConsumptionTypeid,
                           Dim_AccountCategoryid,
                           Dim_SpecialStockid,
                           dim_bomstatusid,
                           dim_bomusageid,
                           dim_objecttypeid,
                           dim_productionschedulerid,
                           dim_schedulingerrorid,
                           dim_tasklisttypeid,
                           dim_ordertypeid,
                           Dim_dateidStart,
                           Dim_dateidFinish,
                           Dim_dateidOpening,
                           ct_QtyTotal,
                           ct_Completed,
                           amt_ExtendedPrice,
                           dd_PlanOrderNo,
                           dd_bomexplosionno,
                           ct_QtyReduced,
                           Dim_PlanOrderStatusid,
                           Dim_DateidConversionDate,
                           Dim_DateidProductionStart,
                           Dim_DateidProductionFinish,
                           Dim_DateidExplosion,
                           Dim_DateidAction,
                           dim_scheduletypeid,
                           dim_availabilityconfirmationid,
                           dim_currencyid,
						   	dim_currencyid_TRA,
							dim_currencyid_GBL,
							amt_ExchangeRate,
							amt_ExchangeRate_GBL,
                           Dim_ProcurementId,
			   dd_SalesOrderNo,
                           dd_SalesOrderItemNo,
                           dd_SalesOrderScheduleNo,
                           ct_GRProcessingTime,
                           ct_QtyIssued,
                           ct_QtyCommitted,
                           dd_SequenceNo,
                           dd_PlannScenario,
                           amt_StdUnitPrice,
                           Dim_MRPControllerId,
                           dd_AgreementNo,
                           dd_AgreementItemNo)
Select fact_planorderid,
                           Dim_ActionStateid,
                           Dim_Companyid,
                           Dim_Partid,
                           Dim_mpnid,
                           Dim_StorageLocationid,
                           Dim_Plantid,
                           Dim_UnitOfMeasureid,
                           Dim_PurchaseOrgid,
                           Dim_Vendorid,
                           Dim_FixedVendorid,
                           Dim_SpecialProcurementid,
                           Dim_ConsumptionTypeid,
                           Dim_AccountCategoryid,
                           Dim_SpecialStockid,
                           dim_bomstatusid,
                           dim_bomusageid,
                           dim_objecttypeid,
                           dim_productionschedulerid,
                           dim_schedulingerrorid,
                           dim_tasklisttypeid,
                           dim_ordertypeid,
                           Dim_dateidStart,
                           Dim_dateidFinish,
                           Dim_dateidOpening,
                           ct_QtyTotal,
                           ct_Completed,
                           amt_ExtendedPrice,
                           dd_PlanOrderNo,
                           dd_bomexplosionno,
                           ct_QtyReduced,
                           Dim_PlanOrderStatusid,
                           Dim_DateidConversionDate,
                           Dim_DateidProductionStart,
                           Dim_DateidProductionFinish,
                           Dim_DateidExplosion,
                           Dim_DateidAction,
                           dim_scheduletypeid,
                           dim_availabilityconfirmationid,
                           dim_currencyid,
						   	dim_currencyid_TRA,
							dim_currencyid_GBL,
							amt_ExchangeRate,
							amt_ExchangeRate_GBL,
                           Dim_ProcurementId,
			   dd_SalesOrderNo,
                           dd_SalesOrderItemNo,
                           dd_SalesOrderScheduleNo,
                           ct_GRProcessingTime,
                           ct_QtyIssued,
                           ct_QtyCommitted,
                           dd_SequenceNo,
                           dd_PlannScenario,
                           amt_StdUnitPrice,
                           Dim_MRPControllerId,
                           dd_AgreementNo,
                           dd_AgreementItemNo
from fact_planorder_di09;


drop table if exists fact_planorder_di09;


/*Update schedule start and end date*/
drop table if exists tmp_kbko_updt;
create table tmp_kbko_updt
as
select row_number() over(partition by kbko_plnum order by kbko_gstrs desc)rono,kbko_plnum,kbko_gstrs
from kbko;

UPDATE fact_planorder po
SET dim_dateidscheduledstart = dd.dim_dateid
FROM
        tmp_kbko_updt k,
        dim_date dd,
        dim_company dc,
        fact_planorder po,
		dim_plant pl
WHERE dd_PlanOrderNo = k.kbko_plnum AND k.kbko_gstrs = dd.DateValue
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND dd.CompanyCode = dc.CompanyCode
        and po.dim_plantid=pl.dim_plantid
		and dd.plantcode_factory=pl.plantcode
        AND k.rono = 1
        AND ifnull(dim_dateidscheduledstart,-1) <> ifnull(dd.dim_dateid,-2);


drop table if exists tmp_kbko_updt;
create table tmp_kbko_updt
as
select row_number() over(partition by kbko_plnum order by kbko_gltrs desc)rono,kbko_plnum,kbko_gltrs
from kbko;

UPDATE fact_planorder po
SET dim_dateidscheduledfinish = dd.dim_dateid
FROM
	 tmp_kbko_updt k,
	dim_date dd,
	dim_company dc,
	fact_planorder po,
dim_plant pl
WHERE dd_PlanOrderNo = k.kbko_plnum AND k.kbko_gltrs = dd.DateValue
	AND po.Dim_Companyid = dc.Dim_Companyid
	AND dd.CompanyCode = dc.CompanyCode
and po.dim_plantid=pl.dim_plantid
and dd.plantcode_factory=pl.plantcode
	AND k.rono = 1
	AND ifnull(dim_dateidscheduledfinish,-1) <> ifnull(dd.dim_dateid,-2);

drop table if exists tmp_kbko_updt;

UPDATE fact_planorder po
SET po.dim_profitcenterid = pc.dim_profitcenterid
FROM
	dim_part pt,
	dim_profitcenter pc,
	fact_planorder po
WHERE po.dim_PartId = pt.Dim_PartId
 	AND pc.ProfitCenterCode = pt.ProfitCenterCode
 	AND pc.validto >= current_date
 	AND pc.RowIsCurrent = 1
	AND ifnull(po.dim_profitcenterid,1) <> ifnull(pc.dim_profitcenterid,1);


UPDATE fact_planorder po
SET po.dim_profitcenterid = 1
WHERE po.dim_profitcenterid IS NULL;

/*Update plan order miscellaneous dimension*/
UPDATE fact_planorder po
SET po.Dim_PlanOrderMiscid = m.Dim_PlanOrderMiscid
FROM
       dim_planordermisc m,
       plaf p,
       fact_planorder po
 WHERE     po.dd_PlanOrderNo = ifnull(p.PLAF_PLNUM, 'Not Set')
       AND ProductionDateScheduling = ifnull(p.PLAF_PRSCH, 'Not Set')
       AND PlanningWithoutFinalAssembly = ifnull(p.PLAF_VRPLA, 'Not Set')
       AND SubcontractingVendor = ifnull(p.PLAF_LBLKZ, 'Not Set')
       AND ConversionIndicator = ifnull(p.PLAF_UMSKZ, 'Not Set')
       AND FirmingIndicator = ifnull(p.PLAF_AUFFX, 'Not Set')
       AND FixingIndicator = ifnull(p.PLAF_STLFX, 'Not Set')
       AND SchedulingIndicator = ifnull(p.PLAF_TRMKZ, 'Not Set')
       AND AssemblyOrderProcedures = ifnull(p.PLAF_MONKZ, 'Not Set')
       AND LeadingOrder = ifnull(p.PLAF_PRNKZ, 'Not Set');


/* Update currencies and exchange rates from fact_productionorder */

/*
UPDATE    fact_planorder po
SET po.dim_currencyID = pod.dim_currencyID
FROM  fact_productionorder pod,  fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.dim_currencyID <> pod.dim_currencyID
*/

/*28 Apr 2017 Georgiana Unable to get a stable source of rows fix*/
/*UPDATE    fact_planorder po
SET po.dim_currencyID_TRA = pod.dim_currencyID_TRA
FROM fact_productionorder pod, fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.dim_currencyID_TRA <> pod.dim_currencyID_TRA*/

/*
merge into fact_planorder po
using ( select distinct fact_planorderid,pod.dim_currencyID_TRA
FROM fact_productionorder pod, fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.dim_currencyID_TRA <> pod.dim_currencyID_TRA) t
on t.fact_planorderid=po.fact_planorderid
when matched then update set po.dim_currencyID_TRA = t.dim_currencyID_TRA
*/

update fact_planorder f
set dim_currencyID_TRA = dim_currencyid
where dim_currencyID_TRA <> dim_currencyid;

/*
UPDATE    fact_planorder po
SET po.amt_ExchangeRate = pod.amt_ExchangeRate
FROM fact_productionorder pod, fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.amt_ExchangeRate <> pod.amt_ExchangeRate
*/

/*28 Apr 2017 Georgiana Unable to get a stable source of rows fix*/
/*UPDATE    fact_planorder po
SET po.amt_ExtendedPrice_GBL = pod.amt_ExchangeRate_GBL
FROM fact_productionorder pod,  fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.amt_ExtendedPrice_GBL <> pod.amt_ExchangeRate_GBL*/

merge into fact_planorder po
using ( select distinct fact_planorderid,pod.amt_ExchangeRate_GBL
FROM fact_productionorder pod, fact_planorder po
WHERE pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.amt_ExtendedPrice_GBL <> pod.amt_ExchangeRate_GBL) t
on t.fact_planorderid=po.fact_planorderid
when matched then update set po.amt_ExtendedPrice_GBL = t.amt_ExchangeRate_GBL;



UPDATE    fact_planorder po
SET po.amt_ExtendedPrice_GBL = pod.amt_ExchangeRate_GBL
FROM fact_productionorder pod, fact_planorder po
WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
AND po.amt_ExtendedPrice_GBL <> pod.amt_ExchangeRate_GBL;

/* Update amounts to transaction currencies where applicable */
UPDATE  fact_planorder po
SET  po.amt_ExtendedPrice = amt_ExtendedPrice /   amt_ExchangeRate            /* As extended price is in local curr, change that to tran curr */
 WHERE   dim_currencyID <> dim_currencyID_TRA;


UPDATE   fact_planorder po
SET po.amt_StdUnitPrice = amt_StdUnitPrice /   amt_ExchangeRate              /* As amt_StdUnitPrice is in local curr, change that to tran curr */
WHERE   dim_currencyID <> dim_currencyID_TRA;


UPDATE   fact_planorder po
SET po.amt_ExtendedPrice_GBL = amt_ExtendedPrice *   amt_ExchangeRate_GBL
WHERE  dim_currencyID_TRA <> dim_currencyID_GBL;
 /* Octavian : Every Angle related field */
 
/*update fact_planorder f
SET dd_forecastprofile = ifnull(MPOP_V_PROPR,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
from dim_part dp, dim_plant p, mpop_v v,fact_planorder f
where f.dim_partid = dp.dim_partid AND
p.dim_plantid = f.dim_plantid AND
dp.partnumber = MPOP_V_MATNR AND
p.plantcode = MPOP_V_WERKS
AND dd_forecastprofile <> ifnull(MPOP_V_PROPR,'Not Set')
*/
/*Octavian : Every Angle related field */

merge into fact_planorder f
using( select fact_planorderid,MAX(MPOP_V_PROPR) AS MPOP_V_PROPR
from mpop_v v,dim_part dp, dim_plant p,fact_planorder f
where f.dim_partid = dp.dim_partid AND
p.dim_plantid = f.dim_plantid AND
dp.partnumber = MPOP_V_MATNR AND
p.plantcode = MPOP_V_WERKS
group by fact_planorderid) t
on t.fact_planorderid = f.fact_planorderid
when matched then update set dd_forecastprofile = ifnull(MPOP_V_PROPR,'Not Set')
where dd_forecastprofile <> ifnull(MPOP_V_PROPR,'Not Set');


/* start APP-7409 ALIN 11 sept 17*/
UPDATE fact_planorder po
SET po.ct_scrap_quantity = ifnull(p.PLAF_AVMNG, 0) 
FROM plaf p, fact_planorder po
 WHERE   po.dd_PlanOrderNo = ifnull(p.PLAF_PLNUM, 'Not Set')
 AND po.ct_scrap_quantity <> ifnull(p.PLAF_AVMNG, 0);


MERGE INTO FACT_PLANORDER F
USING(
SELECT DISTINCT FACT_PLANORDERID, SUM(IFNULL(r.RESB_AUSCH, 0)) AS RESB_AUSCH
from fact_planorder f, RESB r
where f.dd_planorderno = ifnull(r.RESB_PLNUM, 'Not Set') 
and r.RESB_BDART = 'SB'
GROUP BY FACT_PLANORDERID
) t
on t.fact_planorderid = f.fact_planorderid
when matched then update set f.amt_component_scrap = t.resb_ausch;


MERGE INTO FACT_PLANORDER F
USING(
SELECT DISTINCT FACT_PLANORDERID, SUM(IFNULL(r.RESB_AVOAU, 0)) AS RESB_AVOAU
from fact_planorder f, RESB r
where f.dd_planorderno = ifnull(r.RESB_PLNUM, 'Not Set') 
and r.RESB_BDART = 'SB'
GROUP BY FACT_PLANORDERID
) t
on t.fact_planorderid = f.fact_planorderid
when matched then update set f.amt_operation_scrap = t.resb_AVOAU;
/* end APP-7409 ALIN 11 sept 17*/

/* Andrei APP-7259 */
UPDATE fact_planorder po
SET po.dd_productionversion = ifnull(p.PLAF_VERID,'Not Set')
	,po.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
   FROM plaf p,
        dim_plant pl,
     	fact_planorder po
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND pl.PlantCode = p.PLAF_PLWRK
AND po.dd_productionversion <> ifnull(p.PLAF_VERID,'Not Set');

UPDATE fact_planorder po
SET po.dd_group = ifnull(m.MKAL_PLNNR,'Not Set')
	,po.dw_update_date = current_timestamp 
	FROM fact_planorder po,
	     mkal m,
		 plaf p,
		 dim_plant pl
      	where po.dd_PlanOrderNo = p.plaf_plnum
AND pl.PlantCode = p.PLAF_PLWRK
AND p.PLAF_MATNR = m.MKAL_MATNR
and p.PLAF_VERID = m.MKAL_VERID
and p.PLAF_PLWRK = m.MKAL_WERKS
and po.dd_group <> ifnull(m.MKAL_PLNNR,'Not Set');

UPDATE fact_planorder po
SET po.dd_groupcounter = ifnull(m.MKAL_ALNAL,'Not Set')
	,po.dw_update_date = current_timestamp 
	FROM fact_planorder po,
	     mkal m,
		 plaf p,
		 dim_plant pl
      	where po.dd_PlanOrderNo = p.plaf_plnum
AND pl.PlantCode = p.PLAF_PLWRK
AND p.PLAF_MATNR = m.MKAL_MATNR
and p.PLAF_VERID = m.MKAL_VERID
and p.PLAF_PLWRK = m.MKAL_WERKS
and po.dd_groupcounter <> ifnull(m.MKAL_ALNAL,'Not Set');

UPDATE fact_planorder po
SET po.dd_alternativebom = ifnull(m.MKAL_STLAL,'Not Set')
	,po.dw_update_date = current_timestamp 
	FROM fact_planorder po,
	     mkal m,
		 plaf p,
		 dim_plant pl
      	where po.dd_PlanOrderNo = p.plaf_plnum
AND pl.PlantCode = p.PLAF_PLWRK
AND p.PLAF_MATNR = m.MKAL_MATNR
and p.PLAF_VERID = m.MKAL_VERID
and p.PLAF_PLWRK = m.MKAL_WERKS
and po.dd_alternativebom <> ifnull(m.MKAL_STLAL,'Not Set');

UPDATE fact_planorder po
SET po.dd_bomusage = ifnull(m.MKAL_STLAN,'Not Set')
	,po.dw_update_date = current_timestamp 
	FROM fact_planorder po,
	     mkal m,
		 plaf p,
		 dim_plant pl
      	where po.dd_PlanOrderNo = p.plaf_plnum
AND pl.PlantCode = p.PLAF_PLWRK
AND p.PLAF_MATNR = m.MKAL_MATNR
and p.PLAF_VERID = m.MKAL_VERID
and p.PLAF_PLWRK = m.MKAL_WERKS
and po.dd_bomusage <> ifnull(m.MKAL_STLAN,'Not Set');
/* END  Andrei APP-7259 */

drop table if exists fact_planorder_tmp;
DROP TABLE IF EXISTS tmp_ins_plaf_fact_planorder;
DROP TABLE IF EXISTS tmp_ins_fact_planorder;
DROP TABLE IF EXISTS tmp_del_fact_planorder;

/*Alin APP-6528 12 feb 2018 --NEW TTI FACTOR_PL_ORDER */
DROP TABLE if exists tmp_max_date;
create table tmp_max_date as
select distinct 
ifnull(KEKO_WERKS, 'Not Set') as plant, 
ifnull(KEKO_MATNR, 'Not Set') as part,
max(c.CKIS_KADKY)  as KADKY
FROM CKIS C, KEKO K
where c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and year(c.CKIS_KADKY) = year(current_date)
GROUP BY 1,2;

drop table if exists tmp_for_tti_calc_plorder;
create table tmp_for_tti_calc_plorder as 
select  partnumber, plant, costing_lot_size,
sum(sum_wrtfw_kpf) over (partition by partnumber, plant) as sum_wrtfw_kpf
from
(
select  d.partnumber, d.plant, --IFNULL(CKIS_POSNR, 0) AS ORDERITEMNO,
ifnull(CKIS_WRTFW_KPF, 0) AS sum_wrtfw_kpf,
ifnull(KEKO_LOSGR, 0) as costing_lot_size
FROM CKIS C, KEKO K, dim_part d, tmp_max_date t
WHERE
c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and d.plant = ifnull(KEKO_WERKS, 'Not Set')
and d.partnumber = ifnull(KEKO_MATNR, 'Not Set')

and d.plant = t.plant
and d.partnumber = t.part
and c.CKIS_KADKY = t.KADKY
)t;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_plorder;
CREATE TABLE TMP_2_TTI_CALCULATION_plorder AS
select distinct fact_planorderid, dd_PlanOrderNo, 
d.partnumber, d.plant, 
sum_wrtfw_kpf/costing_lot_size as tt_calc
from fact_planorder f, tmp_for_tti_calc_plorder t, dim_part d
where f.dim_partid = d.dim_partid
and t.plant = d.plant
and t.partnumber = d.partnumber;

update fact_planorder f
set ct_tti_new_factor = ifnull(t.tt_calc, 0)
from fact_planorder f, TMP_2_TTI_CALCULATION_plorder t
where f.fact_planorderid = t.fact_planorderid;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_plorder;
drop table if exists tmp_for_tti_calc_plorder;
DROP TABLE if exists tmp_max_date;

update fact_planorder f
set f.STD_EXCHANGERATE_DATEID = f.dim_dateidfinish
where f.STD_EXCHANGERATE_DATEID <> f.dim_dateidfinish;

--------PLANNED ORDER FIXED AND VARIABLE TTIs
--FIXED TTI FACTOR
drop table if exists tmp_for_tti_calc_plorder_fixed;
create table tmp_for_tti_calc_plorder_fixed as 
select distinct partnumber, plant, /*costing_lot_size*/
sum(sum_wrtfw_kpf) over (partition by partnumber, plant) as sum_wrtfw_kpf
from
(
select  d.partnumber, d.plant, /*IFNULL(CKIS_POSNR, 0) AS ORDERITEMNO*/
ifnull(CKIS_WRTFW_KPF, 0) AS sum_wrtfw_kpf
/*ifnull(KEKO_LOSGR, 0) as costing_lot_size*/
FROM CKIS C, KEKO K, dim_part d
WHERE
c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and c.CKIS_KADKY in  ('2018-01-01')
AND c.ckis_psknz = 'F'
and d.plant = ifnull(KEKO_WERKS, 'Not Set')
and d.partnumber = ifnull(KEKO_MATNR, 'Not Set')
)t;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_plorder_fixed;
CREATE TABLE TMP_2_TTI_CALCULATION_plorder_fixed AS
select distinct fact_planorderid, dd_PlanOrderNo, 
d.partnumber, d.plant, 
sum_wrtfw_kpf as tt_calc
from fact_planorder f, tmp_for_tti_calc_plorder_fixed t, dim_part d
where f.dim_partid = d.dim_partid
and t.plant = d.plant
and t.partnumber = d.partnumber;

update fact_planorder f
set ct_fixed_tti = 0;

update fact_planorder f
set ct_fixed_tti = ifnull(t.tt_calc, 0)
from fact_planorder f, TMP_2_TTI_CALCULATION_plorder_fixed t
where f.fact_planorderid = t.fact_planorderid;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_plorder_fixed;
drop table if exists tmp_for_tti_calc_plorder_fixed;



--VARIABLE TTI FACTOR
drop table if exists tmp_for_tti_calc_plorder_variable;
create table tmp_for_tti_calc_plorder_variable as 
select distinct partnumber, plant, costing_lot_size,
sum(sum_wrtfw_kpf) over (partition by partnumber, plant) as sum_wrtfw_kpf
from
(
select  d.partnumber, d.plant, /*IFNULL(CKIS_POSNR, 0) AS ORDERITEMNO*/
ifnull(CKIS_WRTFW_KPF, 0) AS sum_wrtfw_kpf,
ifnull(KEKO_LOSGR, 0) as costing_lot_size
FROM CKIS C, KEKO K, dim_part d
WHERE
c.CKIS_BZOBJ = k.KEKO_BZOBJ
and c.CKIS_KALNR = k.KEKO_KALNR
and c.CKIS_KALKA = k.KEKO_KALKA
and ifnull(c.CKIS_KADKY,'0001-01-01') = ifnull(k.KEKO_KADKY,'0001-01-01')
and c.CKIS_TVERS = k.KEKO_TVERS
and c.CKIS_BWVAR = k.KEKO_BWVAR
and ifnull(c.CKIS_KKZMA,'Not Set') = ifnull(k.KEKO_KKZMA,'Not Set')
and CKIS_TYPPS = 'E'
and c.CKIS_KADKY is not null
and c.CKIS_KADKY = k.KEKO_KADKY
and c.CKIS_KADKY in  ('2018-01-01')
AND c.ckis_psknz IS NULL
and d.plant = ifnull(KEKO_WERKS, 'Not Set')
and d.partnumber = ifnull(KEKO_MATNR, 'Not Set')
)t;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_plorder_variable;
CREATE TABLE TMP_2_TTI_CALCULATION_plorder_variable AS
select distinct fact_planorderid, dd_PlanOrderNo, 
d.partnumber, d.plant, 
sum_wrtfw_kpf/costing_lot_size as tt_calc
from fact_planorder f, tmp_for_tti_calc_plorder_variable t, dim_part d
where f.dim_partid = d.dim_partid
and t.plant = d.plant
and t.partnumber = d.partnumber;

update fact_planorder f
set ct_variable_tti =0;

update fact_planorder f
set ct_variable_tti = ifnull(t.tt_calc, 0)
from fact_planorder f, TMP_2_TTI_CALCULATION_plorder_variable t
where f.fact_planorderid = t.fact_planorderid;

DROP TABLE IF EXISTS TMP_2_TTI_CALCULATION_plorder_variable;
drop table if exists tmp_for_tti_calc_plorder_variable;
