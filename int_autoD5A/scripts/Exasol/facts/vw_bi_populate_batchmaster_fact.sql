drop table if exists tmp_fact_batchmaster;
create table tmp_fact_batchmaster
LIKE fact_batchmaster INCLUDING DEFAULTS INCLUDING IDENTITY;


/* Octavian: PREPROCESS */
drop table if exists dim_batch_filterfact;
create table dim_batch_filterfact
AS 
SELECT * FROM dim_batch a
where
(a.plantcode = 'Not Set' and
not exists(
select 1 from dim_batch z
where a.partnumber = z.partnumber
and a.batchnumber = z.batchnumber
and z.plantcode <> 'Not Set'))
OR
(a.plantcode <> 'Not Set');
/* Octavian: PREPROCESS */

delete from number_fountain m where m.table_name = 'tmp_fact_batchmaster';

insert into number_fountain
select 	'tmp_fact_batchmaster',
ifnull(max(d.fact_batchmasterid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from tmp_fact_batchmaster d;

INSERT INTO tmp_fact_batchmaster(
	fact_batchmasterID,
	Dim_BatchMasterId,
	Dim_MaterialMasterId,  
	Dim_StorageLocationMasterID,  
	Dim_PlantMasterId
	)
	
SELECT 	(select ifnull(m.max_id, 0) from number_fountain m where m.table_name = 'tmp_fact_batchmaster') + row_number() over(ORDER BY '') as fact_batchmasterID,
	t.* from (select distinct
	ifnull(db.dim_batchid,1) Dim_BatchMasterId,
	ifnull(dp.dim_partid,1) Dim_MaterialMasterId,  
	ifnull(dsl.dim_storagelocationid,1) Dim_StorageLocationMasterID,  
	ifnull(pl.dim_plantid,1) Dim_PlantMasterId
FROM dim_batch_filterfact db
	left join dim_plant pl on db.PlantCode = pl.PlantCode
	left join dim_part dp on dp.partnumber = db.partnumber
				and dp.Plant = pl.PlantCode
	left join dim_company co on pl.companycode = co.companycode
	left join dim_storagelocation dsl on dsl.Plant = pl.PlantCode
						and dsl.locationcode = dp.StockLocation
	left join dim_salesorg so on pl.salesorg = so.salesorgcode) t;

/* Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016 .
-- No Tranzactions, nothing to calculate 

UPDATE fact_batchvendormasterdata am
	FROM dim_plant p
		 INNER JOIN dim_date dt ON dt.companycode = p.companycode
SET am.std_exchangerate_dateid = dt.dim_dateid
WHERE am.Dim_PlantMasterId = p.dim_plantid
AND   dt.datevalue = current_date
AND   am.std_exchangerate_dateid <> dt.dim_dateid

END Update std_exchangerate_dateid, by FPOPESCU on 05 January 2016 . */

TRUNCATE TABLE fact_batchmaster;
INSERT INTO fact_batchmaster
SELECT *
FROM tmp_fact_batchmaster;

/* Andrei - 7424 */

update fact_batchmaster b
set b.dim_dateiddonotsellafter = d.dim_dateid
from fact_batchmaster b, dim_batch db, dim_date d, dim_plant p
where d.datevalue = db.donotsellafter
and b.dim_batchmasterid = db.dim_batchid
and d.plantcode_factory = db.plantcode
and p.dim_plantid = b.dim_plantmasterid
and p.companycode = d.companycode
and p.plantcode = d.plantcode_factory 
and  b.dim_dateiddonotsellafter <> d.dim_dateid;

update fact_batchmaster b
set b.dim_dateiddonotshipafter = d.dim_dateid
from fact_batchmaster b, dim_batch db, dim_date d, dim_plant p
where d.datevalue = db.donotshipafter
and b.dim_batchmasterid = db.dim_batchid
and d.plantcode_factory = db.plantcode
and p.dim_plantid = b.dim_plantmasterid
and p.companycode = d.companycode
and p.plantcode = d.plantcode_factory 
and  b.dim_dateiddonotshipafter <> d.dim_dateid;


