UPDATE fact_productionorder fp
SET fp.Dim_CustomerGroup1id = fs1.Dim_CustomerGroup1id
FROM fact_salesorder fs1,fact_productionorder fp
WHERE     fp.dd_SalesOrderNo = fs1.dd_SalesDocNo
AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo
AND fp.dd_SalesOrderDeliveryScheduleNo = fs1.dd_ScheduleNo
AND ifnull(fp.Dim_CustomerGroup1id,-1) <> fs1.Dim_CustomerGroup1id;

UPDATE fact_productionorder fp
SET fp.Dim_CustomerID = fs1.Dim_CustomerID
FROM fact_salesorder fs1,fact_productionorder fp
WHERE     fp.dd_SalesOrderNo = fs1.dd_SalesDocNo
AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo
AND fp.dd_SalesOrderDeliveryScheduleNo = fs1.dd_ScheduleNo
AND IFNULL(fp.Dim_CustomerID,-1) <> fs1.Dim_CustomerID;

UPDATE fact_productionorder fp
SET fp.Dim_DocumentCategoryid = fs1.Dim_DocumentCategoryid
FROM fact_salesorder fs1,fact_productionorder fp
WHERE     fp.dd_SalesOrderNo = fs1.dd_SalesDocNo
AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo
AND fp.dd_SalesOrderDeliveryScheduleNo = fs1.dd_ScheduleNo
AND IFNULL(fp.Dim_DocumentCategoryid,-1) <> fs1.Dim_DocumentCategoryid;

UPDATE fact_productionorder fp
SET fp.Dim_SalesDocumentTypeid = fs1.Dim_SalesDocumentTypeid
FROM fact_salesorder fs1,fact_productionorder fp
WHERE     fp.dd_SalesOrderNo = fs1.dd_SalesDocNo
AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo
AND fp.dd_SalesOrderDeliveryScheduleNo = fs1.dd_ScheduleNo
AND IFNULL(fp.Dim_SalesDocumentTypeid,-1) <> fs1.Dim_SalesDocumentTypeid ;

UPDATE fact_productionorder fp
SET fp.amt_ExchangeRate = fs1.amt_ExchangeRate
FROM fact_salesorder fs1,fact_productionorder fp
WHERE     fp.dd_SalesOrderNo = fs1.dd_SalesDocNo
AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo
AND fp.dd_SalesOrderDeliveryScheduleNo = fs1.dd_ScheduleNo
AND fp.amt_ExchangeRate <> fs1.amt_ExchangeRate;


UPDATE fact_productionorder fp
SET fp.amt_ExchangeRate_GBL = fs1.amt_ExchangeRate_GBL
FROM fact_salesorder fs1,fact_productionorder fp
WHERE     fp.dd_SalesOrderNo = fs1.dd_SalesDocNo
AND fp.dd_SalesOrderItemNo = fs1.dd_SalesItemNo
AND fp.dd_SalesOrderDeliveryScheduleNo = fs1.dd_ScheduleNo
AND fp.amt_ExchangeRate_GBL <> fs1.amt_ExchangeRate_GBL;
