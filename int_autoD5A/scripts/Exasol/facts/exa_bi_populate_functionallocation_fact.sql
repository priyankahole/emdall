DROP TABLE IF EXISTS tmp_fact_functionallocation;

CREATE TABLE tmp_fact_functionallocation
LIKE fact_functionallocation INCLUDING DEFAULTS INCLUDING IDENTITY;

delete from number_fountain m where m.table_name = 'tmp_fact_functionallocation';
insert into number_fountain
select 'tmp_fact_functionallocation',
ifnull(max(f.fact_functionallocationid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_fact_functionallocation f;

insert into tmp_fact_functionallocation (
fact_functionallocationid,
dd_FunctionalLocation ,
dd_AcctAssignment,
dim_functionallocationid,
dim_plantid ,
dim_controllingareaid ,
dim_companyid,
DW_UPDATE_DATE,
DW_INSERT_DATE,
AMT_EXCHANGERATE_GBL,
AMT_EXCHANGERATE,
DIM_CURRENCYID_GBL,
DIM_CURRENCYID,
DIM_CURRENCYID_TRA,
DIM_PROJECTSOURCEID)
SELECT
(select max_id   from number_fountain   where table_name = 'tmp_fact_functionallocation') + row_number() over(order by '') AS fact_functionallocationid,
ifnull(IFLOT_TPLNR,'Not Set') as dd_FunctionalLocation,
ifnull(ILOA_ILOAN,'Not Set') as dd_AcctAssignment,
1 as dim_functionallocationid,
1 as dim_plantid,
1 as dim_controllingareaid,
1 as dim_companyid,
current_date as DW_UPDATE_DATE,
current_date as DW_INSERT_DATE,
1 as AMT_EXCHANGERATE_GBL,
1 as AMT_EXCHANGERATE,
1 as DIM_CURRENCYID_GBL,
1 as DIM_CURRENCYID,
1 as DIM_CURRENCYID_TRA,
1 as DIM_PROJECTSOURCEID
FROM IFLOT ft, ILOA il
where ft.iflot_tplnr = il.ILOA_TPLNR
and ft.iflot_iloan = il.ILOA_ILOAN
and not exists (select 1 from tmp_fact_functionallocation d1
where d1.dd_FunctionalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and d1.dd_AcctAssignment = ifnull(ILOA_ILOAN,'Not Set'));

update  tmp_fact_functionallocation f
set f.dim_functionallocationid = dd.dim_functionallocationid
from  tmp_fact_functionallocation f, IFLOT ft, ILOA il,dim_functionallocation dd
where ft.iflot_tplnr = il.ILOA_TPLNR
and f.dd_FunctionalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and f.dd_AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and dd.FunctonalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and dd.AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and f.dim_functionallocationid <> dd.dim_functionallocationid;

update  tmp_fact_functionallocation f
set f.dim_plantid = dd.dim_plantid
from  tmp_fact_functionallocation f, IFLOT ft, ILOA il,dim_plant dd
where ft.iflot_tplnr = il.ILOA_TPLNR
and f.dd_FunctionalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and f.dd_AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and dd.plantcode=ifnull(ILOA_SWERK,'Not Set')
and f.dim_plantid <> dd.dim_plantid;

update  tmp_fact_functionallocation f
set f.dim_controllingareaid = dd.dim_controllingareaid
from  tmp_fact_functionallocation f, IFLOT ft, ILOA il,dim_controllingarea dd
where ft.iflot_tplnr = il.ILOA_TPLNR
and f.dd_FunctionalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and f.dd_AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and dd.ControllingAreaCode=ifnull(ILOA_KOKRS,'Not Set')
and f.dim_controllingareaid <> dd.dim_controllingareaid;

update  tmp_fact_functionallocation f
set f.dim_companyid = dd.dim_companyid
from  tmp_fact_functionallocation f, IFLOT ft, ILOA il,dim_company dd
where ft.iflot_tplnr = il.ILOA_TPLNR
and f.dd_FunctionalLocation = ifnull(IFLOT_TPLNR,'Not Set')
and f.dd_AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and dd.companycode=ifnull(ILOA_BUKRS,'Not Set')
and f.dim_companyid <> dd.dim_companyid;

/*Alin 12 Mar 2018 APP-7878 new field request*/
merge into tmp_fact_functionallocation f
using(
select distinct fact_functionallocationid, 
case when ji.jest_stat = 'I0076' then 'Yes' else 'No' end dd_floc_deletion_flag 
from  tmp_fact_functionallocation f, IFLOT ft, ILOA il, JEST_IFLOT JI
where ft.iflot_tplnr = il.ILOA_TPLNR
and f.dd_FunctionalLocation = ifnull(ft.IFLOT_TPLNR,'Not Set')
and f.dd_AcctAssignment = ifnull(ILOA_ILOAN,'Not Set')
and ifnull(ji.IFLOT_TPLNR, 'Not Set') = f.dd_FunctionalLocation) t
on f.fact_functionallocationid = t.fact_functionallocationid
when matched then update 
set dd_floc_deletion_flag = ifnull(t.dd_floc_deletion_flag, 'Not Set');


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'tmp_fact_functionallocation';   

DROP TABLE if EXISTS fact_functionallocation;
RENAME tmp_fact_functionallocation to fact_functionallocation;
