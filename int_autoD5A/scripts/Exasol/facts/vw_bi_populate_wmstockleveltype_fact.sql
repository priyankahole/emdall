
/* ################################################################################################################## */
/* */
/*   Author         : Georgiana */
/*   Created On     : 29 Aug 2016 */
/*   Description    : */

/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */

/*   29 Aug 2016      Gergiana	1.0  		  First draft */

/******************************************************************************************************************/


delete from number_fountain m where m.table_name = 'fact_wmstockleveltype';
insert into number_fountain
select 'fact_wmstockleveltype',
ifnull(max(f.fact_wmstockleveltypeid), 
ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_wmstockleveltype f;

insert into fact_wmstockleveltype (
fact_wmstockleveltypeid ,
dd_materialnumber,
dd_warehousenumber,
dd_specialmovementindicator,
dd_storagetypeindforplacement,
dd_storagetypeindforremoval,
dd_storageind,
dim_unitofmeasureid,
dd_deletionflag,
ct_loadingequipmentqty1,
ct_loadingequipmentqty2,
ct_loadingequipmentqty3,
dd_storageunittype1,
dd_storageunittype2,
dd_storageunittype3,
dim_unitofmeasureidquantity1,
dim_unitofmeasureidquantity2,
dim_unitofmeasureidquantity3,
dd_storagetype,
ct_maxstoragebinqty,
ct_minstoragebinqty,
dd_pickingarea,
dw_insert_date)

SELECT
(select max_id   from number_fountain   where table_name = 'fact_wmstockleveltype') + row_number() over(order by '') AS fact_wmstockleveltypeid, 
ifnull(MLGN_MATNR,'Not Set'),
ifnull(MLGN_LGNUM,'Not Set'),
ifnull(MLGN_BSSKZ,'Not Set'),
ifnull(MLGN_LTKZE,'Not Set'),
ifnull(MLGN_LTKZA,'Not Set'),
ifnull(MLGN_LGBKZ,'Not Set'),
1 as dim_unitofmeasureid,
ifnull(MLGN_LVORM,'Not Set'),
ifnull(MLGN_LHMG1,0),
ifnull(MLGN_LHMG2,0),
ifnull(MLGN_LHMG3,0),
ifnull(MLGN_LETY1,'Not Set'),
ifnull(MLGN_LETY2,'Not Set'),
ifnull(MLGN_LETY3,'Not Set'),
1 as dim_unitofmeasureidquantity1,
1 as dim_unitofmeasureidquantity2,
1 as dim_unitofmeasureidquantity3,
ifnull(MLGT_LGTYP,'Not Set'),
ifnull(MLGT_LPMAX,0),
ifnull(MLGT_LPMIN,0),
ifnull(MLGT_KOBER,'Not Set'),
current_date
from MLGN mn
full outer join MLGT mt
on mn.MLGN_MATNR = mt.MLGT_MATNR and mn.MLGN_LGNUM = mt.MLGT_LGNUM
where not exists (select 1 from fact_wmstockleveltype f
                  where f.dd_materialnumber= ifnull(MLGN_MATNR,'Not Set')
                         and f.dd_warehousenumber= ifnull(MLGN_LGNUM,'Not Set')
						 and f.dd_storagetype = ifnull(MLGT_LGTYP,'Not Set'));
						 

merge into fact_wmstockleveltype f
using (select distinct  fact_wmstockleveltypeid,
MLGN_BSSKZ,
MLGN_LTKZE,
MLGN_LTKZA,
MLGN_LGBKZ,
MLGN_LVORM,
MLGN_LETY1,
MLGN_LETY2,
MLGN_LETY3
from fact_wmstockleveltype f, MLGN
where f.dd_materialnumber= ifnull(MLGN_MATNR,'Not Set')
   and f.dd_warehousenumber= ifnull(MLGN_LGNUM,'Not Set')) t
   on t.fact_wmstockleveltypeid = f.fact_wmstockleveltypeid
when matched then update set 
dd_specialmovementindicator = ifnull(MLGN_BSSKZ,'Not Set'),
dd_storagetypeindforplacement = ifnull(MLGN_LTKZE,'Not Set'),
dd_storagetypeindforremoval = ifnull(MLGN_LTKZA,'Not Set'),
dd_storageind = ifnull(MLGN_LGBKZ, 'Not Set'),
dd_deletionflag = ifnull(MLGN_LVORM,'Not Set'),
dd_storageunittype1 = ifnull(MLGN_LETY1,'Not Set'),
dd_storageunittype2 = ifnull(MLGN_LETY2,'Not Set'),
dd_storageunittype3 = ifnull(MLGN_LETY3,'Not Set')
,dw_update_date = current_timestamp;
 
merge into fact_wmstockleveltype f
using (select distinct  fact_wmstockleveltypeid,
MLGN_LHMG1,
MLGN_LHMG2,
MLGN_LHMG3
from fact_wmstockleveltype f, MLGN
where f.dd_materialnumber= ifnull(MLGN_MATNR,'Not Set')
   and f.dd_warehousenumber= ifnull(MLGN_LGNUM,'Not Set')) t
   on t.fact_wmstockleveltypeid = f.fact_wmstockleveltypeid
when matched then update set 
ct_loadingequipmentqty1 = ifnull(MLGN_LHMG1,0),
ct_loadingequipmentqty2 = ifnull(MLGN_LHMG2,0),
ct_loadingequipmentqty3 = ifnull(MLGN_LHMG3,0)
,dw_update_date = current_timestamp;
   
merge into fact_wmstockleveltype f
using (select distinct  fact_wmstockleveltypeid,uom.dim_unitofmeasureid
from fact_wmstockleveltype f, dim_unitofmeasure uom,MLGN
where f.dd_materialnumber= ifnull(MLGN_MATNR,'Not Set')
   and f.dd_warehousenumber= ifnull(MLGN_LGNUM,'Not Set')
   and uom.uom = ifnull(MLGN_LVSME,'Not Set')
   and f.dim_unitofmeasureid <> uom.dim_unitofmeasureid) t
    on t.fact_wmstockleveltypeid = f.fact_wmstockleveltypeid
when matched then update set   f.dim_unitofmeasureid = t.dim_unitofmeasureid
,dw_update_date = current_timestamp;

merge into fact_wmstockleveltype f
using (select distinct  fact_wmstockleveltypeid,uom.dim_unitofmeasureid
from fact_wmstockleveltype f, dim_unitofmeasure uom,MLGN
where f.dd_materialnumber= ifnull(MLGN_MATNR,'Not Set')
   and f.dd_warehousenumber= ifnull(MLGN_LGNUM,'Not Set')
   and uom.uom = ifnull(MLGN_LHME1,'Not Set')
   and f.dim_unitofmeasureidquantity1 <> uom.dim_unitofmeasureid) t
    on t.fact_wmstockleveltypeid = f.fact_wmstockleveltypeid
when matched then update set   f.dim_unitofmeasureidquantity1 = t.dim_unitofmeasureid
,dw_update_date = current_timestamp;

merge into fact_wmstockleveltype f
using (select distinct  fact_wmstockleveltypeid,uom.dim_unitofmeasureid
from fact_wmstockleveltype f, dim_unitofmeasure uom,MLGN
where f.dd_materialnumber= ifnull(MLGN_MATNR,'Not Set')
   and f.dd_warehousenumber= ifnull(MLGN_LGNUM,'Not Set')
   and uom.uom = ifnull(MLGN_LHME2,'Not Set')
   and f.dim_unitofmeasureidquantity2 <> uom.dim_unitofmeasureid) t
    on t.fact_wmstockleveltypeid = f.fact_wmstockleveltypeid
when matched then update set   f.dim_unitofmeasureidquantity2 = t.dim_unitofmeasureid
,dw_update_date = current_timestamp;

merge into fact_wmstockleveltype f
using (select distinct  fact_wmstockleveltypeid,uom.dim_unitofmeasureid
from fact_wmstockleveltype f, dim_unitofmeasure uom,MLGN
where f.dd_materialnumber= ifnull(MLGN_MATNR,'Not Set')
   and f.dd_warehousenumber= ifnull(MLGN_LGNUM,'Not Set')
   and uom.uom = ifnull(MLGN_LHME3,'Not Set')
   and f.dim_unitofmeasureidquantity3 <> uom.dim_unitofmeasureid) t
    on t.fact_wmstockleveltypeid = f.fact_wmstockleveltypeid
when matched then update set   f.dim_unitofmeasureidquantity3 = t.dim_unitofmeasureid
,dw_update_date = current_timestamp;


merge into fact_wmstockleveltype f
using (select distinct  fact_wmstockleveltypeid,
MLGT_KOBER, MLGT_LPMAX,
MLGT_LPMIN
from fact_wmstockleveltype f,  MLGT 
where f.dd_materialnumber= ifnull(MLGT_MATNR,'Not Set')
   and f.dd_warehousenumber= ifnull(MLGT_LGNUM,'Not Set')
   and dd_storagetype = ifnull(MLGT_LGTYP,'Not Set')   ) t
   on t.fact_wmstockleveltypeid = f.fact_wmstockleveltypeid
when matched then update set 
dd_pickingarea = ifnull(MLGT_KOBER,'Not Set'),
ct_maxstoragebinqty = ifnull(MLGT_LPMAX,0),
ct_minstoragebinqty = ifnull(MLGT_LPMIN,0)
,dw_update_date = current_timestamp;

/*Alin 25 sept APP-7595*/
merge into fact_wmstockleveltype f
using (select distinct  fact_wmstockleveltypeid, MLGT_LGPLA
from fact_wmstockleveltype f,  MLGT 
where f.dd_materialnumber= ifnull(MLGT_MATNR,'Not Set')
   and f.dd_warehousenumber= ifnull(MLGT_LGNUM,'Not Set')
   and dd_storagetype = ifnull(MLGT_LGTYP,'Not Set')   ) t
   on t.fact_wmstockleveltypeid = f.fact_wmstockleveltypeid
when matched then update set 
dd_storagebin = ifnull(MLGT_LGPLA,'Not Set'),
dw_update_date = current_timestamp;

/* Andrei APP-7864 - add dim_part */
merge into fact_wmstockleveltype f using
(select distinct f.fact_wmstockleveltypeid,
                 first_value(p.dim_partid) over ( partition by p.partnumber order by '' ) as dim_partid
from fact_wmstockleveltype f, dim_part p, MLGN
where f.dd_materialnumber= ifnull(MLGN_MATNR,'Not Set')
and f.dd_warehousenumber= ifnull(MLGN_LGNUM,'Not Set')
and p.partnumber = ifnull(MLGN_MATNR,'Not Set') ) t
on f.fact_wmstockleveltypeid = t.fact_wmstockleveltypeid
when matched then update  set f.dim_partid = t.dim_partid
where f.dim_partid <> t.dim_partid;

