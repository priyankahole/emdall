
UPDATE    dim_defectreporttype drt
    SET drt.DefectReportTypeName = t.TQ86T_KURZTEXT,
			drt.dw_update_date = current_timestamp
	FROM
          dim_defectreporttype drt, TQ86T t
 WHERE drt.RowIsCurrent = 1
 AND t.TQ86T_FEART = drt.DefectReportTypeCode;
