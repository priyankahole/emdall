DROP TABLE IF EXISTS tmp_dim_maintenanceitem;

CREATE TABLE tmp_dim_maintenanceitem
LIKE dim_maintenanceitem INCLUDING DEFAULTS INCLUDING IDENTITY;

insert into tmp_dim_maintenanceitem
(dim_maintenanceitemid,
maintenanceitem
)
select 
1, 'Not Set'
from (select 1) a
where not exists ( select 'x' from tmp_dim_maintenanceitem where dim_maintenanceitemid = 1);

delete from number_fountain m where m.table_name = 'tmp_dim_maintenanceitem';
insert into number_fountain
select 'tmp_dim_maintenanceitem',
 ifnull(max(d.dim_maintenanceitemid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_dim_maintenanceitem d
where d.dim_maintenanceitemid <> 1;


insert into tmp_dim_maintenanceitem
(dim_maintenanceitemid,
maintenanceitem,
maintenanceplan,
maintenancestrategy,
itemshorttext,
equipmentnumber,
tasklisttype,
keyfortasklistgroup,
groupcounter,
plannergroup,
workcenterobjectid,
locaccountassignment,
ordertype,
priority)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'tmp_dim_maintenanceitem') + row_number() over(order by '') AS dim_maintenanceitemid,
 ifnull(MPOS_WAPOS, 'Not Set') as maintenanceitem,
 ifnull(MPOS_WARPL,'Not Set') as maintenanceplan,
 ifnull(MPOS_WSTRA,'Not Set') as maintenancestrategy,
 ifnull(MPOS_PSTXT, 'Not Set') as itemshorttext,
 ifnull(MPOS_EQUNR, 'Not Set') as equipmentnumber,
 ifnull(MPOS_PLNTY, 'Not Set') as tasklisttype,
 ifnull(MPOS_PLNNR,'Not Set') as keyfortasklistgroup,
 ifnull(MPOS_PLNAL,'Not Set') as groupcounter,
 ifnull(MPOS_WPGRP, 'Not Set') as plannergroup,
 ifnull(MPOS_GEWRK,'Not Set') as workcenterobjectid,
 ifnull(MPOS_ILOAN,'Not Set') as locaccountassignment,
 ifnull(MPOS_AUART,'Not Set') as ordertype,
 ifnull(MPOS_PRIOK,'Not Set') as priority
 
FROM 
MPOS q
where not exists ( select 'x' from  tmp_dim_maintenanceitem
                    where  ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem);


					
update tmp_dim_maintenanceitem d
set d.maintenanceplan=ifnull(MPOS_WARPL,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.maintenanceplan <> ifnull(MPOS_WARPL,'Not Set');

update tmp_dim_maintenanceitem d
set d.maintenancestrategy=ifnull(MPOS_WSTRA,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.maintenancestrategy <> ifnull(MPOS_WSTRA,'Not Set');

update tmp_dim_maintenanceitem d
set d.itemshorttext=ifnull(MPOS_PSTXT,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.itemshorttext <> ifnull(MPOS_PSTXT,'Not Set');

update tmp_dim_maintenanceitem d
set d.tasklisttype=ifnull(MPOS_PLNTY,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.tasklisttype <> ifnull(MPOS_PLNTY,'Not Set');

update tmp_dim_maintenanceitem d
set d.keyfortasklistgroup=ifnull(MPOS_PLNNR,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.keyfortasklistgroup <> ifnull(MPOS_PLNNR,'Not Set');

update tmp_dim_maintenanceitem d
set d.groupcounter=ifnull(MPOS_PLNAL,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.groupcounter <> ifnull(MPOS_PLNAL,'Not Set');

update tmp_dim_maintenanceitem d
set d.workcenterobjectid=ifnull(MPOS_GEWRK,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.workcenterobjectid <> ifnull(MPOS_GEWRK,'Not Set');

update tmp_dim_maintenanceitem d
set d.locaccountassignment=ifnull(MPOS_ILOAN,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.locaccountassignment <> ifnull(MPOS_ILOAN,'Not Set');

update tmp_dim_maintenanceitem d
set d.ordertype=ifnull(MPOS_AUART,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.ordertype <> ifnull(MPOS_AUART,'Not Set');

update tmp_dim_maintenanceitem d
set d.priority=ifnull(MPOS_PRIOK,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.priority <> ifnull(MPOS_PRIOK,'Not Set');

update tmp_dim_maintenanceitem d
set d.equipmentnumber=ifnull(MPOS_EQUNR,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.equipmentnumber <> ifnull(MPOS_EQUNR,'Not Set');

update tmp_dim_maintenanceitem d
set d.plannergroup=ifnull(MPOS_WPGRP,'Not Set')
from MPOS m ,tmp_dim_maintenanceitem d
where ifnull(MPOS_WAPOS, 'Not Set') = maintenanceitem
and d.plannergroup <> ifnull(MPOS_WPGRP,'Not Set');

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'tmp_dim_maintenanceitem';        

DROP TABLE if EXISTS dim_maintenanceitem;
RENAME tmp_dim_maintenanceitem to dim_maintenanceitem;