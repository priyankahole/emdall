/* ################################################################################################################## */
/* */
/*   Script         : exa_bi_populate_survey */
/*   Author         : Andrei R */
/*   Created On     : 8 Mar 2017 */
/*   Modifications  : Madalina 5 May 2017 - adjusting script */
				/*grain: surveycode*/
/*  */
/* ################################################################################################################## */

/* DELETE FROM SURVEY --  the delete stmt is performed within the python script */

/*IMPORT INTO SURVEY FROM CSV
AT DC1STGC1INT01CONNECTION
FILE '/home/fusionops/ispring_clustered_storage/configurations/MerckDE6/files/CustomerSurvey/SURVEY.csv'
COLUMN SEPARATOR = ','
ENCODING = 'ISO-8859-1'
SKIP = 1 */


INSERT INTO DIM_SURVEY (dim_surveyid)
select 1
from (select 1) a
where not exists (select '1' from dim_survey where dim_surveyid = 1);

DELETE FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_survey';
INSERT INTO NUMBER_FOUNTAIN 
SELECT 'dim_survey', ifnull(max(dim_surveyid),1)
FROM DIM_SURVEY;


INSERT INTO DIM_SURVEY (dim_surveyid,SurveyCode,SurveyDescription)
SELECT(SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'dim_survey') + row_number() over(order by '') as DIM_SURVEYID,
surveycode, surveydescription from survey S WHERE NOT EXISTS(SELECT 1  FROM dim_survey ds where s.surveycode = ds.surveycode) ; 

UPDATE DIM_SURVEY DS 
SET DS.SURVEYDESCRIPTION = S.SURVEYDESCRIPTION, 
DW_UPDATE_DATE = CURRENT_TIMESTAMP
FROM SURVEY S, DIM_SURVEY DS
WHERE DS.SURVEYCODE = S.SURVEYCODE AND 
DS.SURVEYDESCRIPTION <> S.SURVEYDESCRIPTION;
