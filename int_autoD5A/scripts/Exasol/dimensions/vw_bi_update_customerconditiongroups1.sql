
UPDATE dim_CustomerConditionGroups1 a 
   SET a.Description = ifnull(TVKGGT_VTEXT, 'Not Set')
FROM TVKGGT, dim_CustomerConditionGroups1 a 
 WHERE a.CustomerCondGrp1 = TVKGGT_KDKGR AND RowIsCurrent = 1;
