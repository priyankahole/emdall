
/* Cristina I - 10/6/2015 - Create the first version of dim_ordermaster */
/* Liviu I - 12/11/2015 - Add TimeCreated (AUFK_ERFZEIT), ObjectNumber (AUFK_OBJNR) */
/* Liviu I - 01/15/2016 - Add CostCenter (AUFK_KOSTL) */
/* Suchithra V - 04March2016 - Added Work Center (AUFK_VAPLZ) */
/* Suchithra V - 03May2016 - Added Profit Center AUFK_PRCTR
		 Added Planning Indicator from AFIH AFIH_PLKNZ */
/* Suchithra V - 04May2016 - Added system condition from AFIH AFIH_ANLZU */
/* Suchithra V - 06May2016 - Added Workcenter Desc */


insert into dim_ordermaster(
dim_ordermasterid,
"order",
Order_Type,
Order_category,
Reference_order,
Entered_by,
Created_on,
Last_changed_by,
change_date,
Description,
Long_text_exists,
Company_Code,
Plant,
Business_Area,
Controlling_Area,
Cost_collector_key,
Responsible_CCtr,
Location_plant,
Statistical,
Currency,
Order_status,
Status_change,
Reached_status,
"CREATED",
Released,
Completed,
Closed,
Planned_release,
Planned_completion,
Planned_closing_date,
Release_date,
Technical_completion,
"close",
Object_ID,
Disallowed_trans_grp,
TimeCreated,
ObjectNumber,
CostCenter,
ProfitCenter,
RowStartDate,
RowEndDate,
RowIsCurrent,
RowChangeReason)
SELECT
1,
'Not Set',
'Not Set',
0,
'Not Set',
'Not Set',
'1970-01-01',
'Not Set',
'1970-01-01',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
0,
'1970-01-01',
0,
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'1970-01-01',
'1970-01-01',
'1970-01-01',
'1970-01-01',
'1970-01-01',
'1970-01-01',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
'Not Set',
current_timestamp,
null,
1,
null
FROM (SELECT 1) as a
where not exists (select 1 from dim_ordermaster where dim_ordermasterid = 1);


delete from number_fountain m where m.table_name = 'dim_ordermaster';

insert into number_fountain
select 	'dim_ordermaster',
	ifnull(max(d.dim_ordermasterid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ordermaster d
where d.dim_ordermasterid <> 1;

INSERT INTO dim_ordermaster(
dim_ordermasterid,
"order",
Order_Type,
Order_category,
Reference_order,
Entered_by,
Created_on,
Last_changed_by,
change_date,
Description,
Long_text_exists,
Company_Code,
Plant,
Business_Area,
Controlling_Area,
Cost_collector_key,
Responsible_CCtr,
Location_plant,
Statistical,
Currency,
Order_status,
Status_change,
Reached_status,
"CREATED",
Released,
Completed,
Closed,
Planned_release,
Planned_completion,
Planned_closing_date,
Release_date,
Technical_completion,
"close",
Object_ID,
Disallowed_trans_grp,
TimeCreated,
ObjectNumber,
CostCenter,
ProfitCenter,
RowStartDate,
RowIsCurrent
)
     SELECT (
select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_ordermaster') + row_number() over(order by ''),t.* FROM (SELECT DISTINCT
IFNULL(AUFK_AUFNR,'Not Set') as AUFK_AUFNR,
IFNULL(AUFK_AUART,'Not Set'),
IFNULL(AUFK_AUTYP,0),
IFNULL(AUFK_REFNR,'Not Set'),
IFNULL(AUFK_ERNAM,'Not Set'),
IFNULL(AUFK_ERDAT,'1970-01-01'),
IFNULL(AUFK_AENAM,'Not Set'),
IFNULL(AUFK_AEDAT,'1970-01-01'),
IFNULL(AUFK_KTEXT,'Not Set'),
IFNULL(AUFK_LTEXT,'Not Set'),
IFNULL(AUFK_BUKRS,'Not Set'),
IFNULL(AUFK_WERKS,'Not Set'),
IFNULL(AUFK_GSBER,'Not Set'),
IFNULL(AUFK_KOKRS,'Not Set'),
IFNULL(AUFK_CCKEY,'Not Set'),
IFNULL(AUFK_KOSTV,'Not Set'),
IFNULL(AUFK_SOWRK,'Not Set'),
IFNULL(AUFK_ASTKZ,'Not Set'),
IFNULL(AUFK_WAERS,'Not Set'),
IFNULL(AUFK_ASTNR,0),
IFNULL(AUFK_STDAT,'1970-01-01'),
IFNULL(AUFK_ESTNR,0),
IFNULL(AUFK_PHAS0,'Not Set'),
IFNULL(AUFK_PHAS1,'Not Set'),
IFNULL(AUFK_PHAS2,'Not Set'),
IFNULL(AUFK_PHAS3,'Not Set'),
IFNULL(AUFK_PDAT1,'1970-01-01'),
IFNULL(AUFK_PDAT2,'1970-01-01'),
IFNULL(AUFK_PDAT3,'1970-01-01'),
IFNULL(AUFK_IDAT1,'1970-01-01'),
IFNULL(AUFK_IDAT2,'1970-01-01'),
IFNULL(AUFK_IDAT3,'1970-01-01'),
IFNULL(AUFK_OBJID,'Not Set'),
IFNULL(AUFK_VOGRP,'Not Set'),
IFNULL(AUFK_ERFZEIT,'Not Set'),
IFNULL(AUFK_OBJNR,'Not Set'),
IFNULL(AUFK_KOSTL,'Not Set'),
IFNULL(AUFK_PRCTR,'Not Set'),
current_timestamp,
1
FROM AUFK) t
WHERE NOT EXISTS
(SELECT 1
FROM dim_ordermaster
WHERE "order" = AUFK_AUFNR);

/* AUFK-VAPLZ*/
merge into dim_ordermaster d
using (select distinct AUFK_AUFNR,first_value(AUFK_VAPLZ) over (partition by AUFK_AUFNR order by AUFK_AUFNR desc) as AUFK_VAPLZ FROM AUFK) a
on d."order" = AUFK_AUFNR
when matched then update set
d.workcenter = ifnull(AUFK_VAPLZ,'Not Set')
where d.workcenter <> ifnull(AUFK_VAPLZ,'Not Set');


/* 03May2016 - Planning Indicator */
update dim_ordermaster d
set planningindicator = ifnull(AFIH_PLKNZ,'Not Set') ,dw_update_date = current_timestamp
from afih, dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and planningindicator <> ifnull(AFIH_PLKNZ,'Not Set');

/*04May2016 - System Condition */
update dim_ordermaster d
set systemcondition = ifnull(AFIH_ANLZU,'Not Set'),dw_update_date = current_timestamp
from afih, dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and systemcondition <> ifnull(AFIH_ANLZU,'Not Set');

/*06May2016 - Work Center Desc. Work CEnter Dim key is different. *
update dim_ordermaster o
set o.workcenterdesc =  ifnull(w.ktext_description,'Not Set'), dw_update_date = current_timestamp
from dim_workcenter w,dim_ordermaster o
where o.workcenter = w.workcenter
and w.plant = o.plant
and w.costcenteractivitytype <> 'Not Set'
and o.workcenterdesc <>  ifnull(w.ktext_description,'Not Set') */

/*
merge into dim_ordermaster o
 using
 (select distinct o.workcenter, o.plant, o.workcenterdesc, w.ktext_description, o.dim_ordermasterid
 FROM dim_workcenter w, dim_ordermaster o
 WHERE o.workcenter = w.workcenter
 and w.plant = o.plant
 and w.costcenteractivitytype <> 'Not Set'
 and o.workcenterdesc <> ifnull(w.ktext_description,'Not Set')
 group by o.workcenter, o.plant, o.workcenterdesc, w.ktext_description, o.dim_ordermasterid
 )w
 on (
 w.workcenter = o.workcenter and
 w.plant = o.plant and
w.dim_ordermasterid = o.dim_ordermasterid

 )
 when matched then update set o.workcenterdesc = ifnull(w.ktext_description,'Not Set'), 
dw_update_date = current_timestamp
 where o.workcenterdesc <> ifnull(w.ktext_description,'Not Set')*/


/*Alin 27 Jun 2018 APP-7223 logic change requested for workcenterdesc*/
merge into dim_ordermaster d
using(
select distinct dim_ordermasterid, ifnull(cr.CRTX_KTEXT, 'Not Set') as descr
from dim_ordermaster d, AUFK a, CRHD c, CRTX cr
where "order" = AUFK_AUFNR
and AUFK_VAPLZ = CRHD_ARBPL
and CRHD_OBJID = CRTX_OBJID
and aufk_werks = crhd_werks
and CRTX_SPRAS = 'E'
) t
on t.dim_ordermasterid = d.dim_ordermasterid
when matched then update 
set d.workcenterdesc = ifnull(t.descr,'Not Set'), 
dw_update_date = current_timestamp
 where d.workcenterdesc <> ifnull(t.descr,'Not Set');
 


update dim_ordermaster d
set orderpriority = ifnull(AFIH_PRIOK,'Not Set') ,dw_update_date = current_timestamp
from afih, dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and orderpriority <> ifnull(AFIH_PRIOK,'Not Set');

update dim_ordermaster d
set orderrevision = ifnull(AFIH_REVNR,'Not Set') ,dw_update_date = current_timestamp
from afih,dim_ordermaster d
where trim(leading '0' from d."order") = trim(leading '0' from afih_aufnr)
and orderrevision <> ifnull(AFIH_REVNR,'Not Set');

/*Alin 6 febr 2018 APP-7878*/
update dim_ordermaster
set technical_completion = IFNULL(AUFK_IDAT2,'1970-01-01')
from dim_ordermaster, AUFK
where "order" = ifnull(AUFK_AUFNR, 'Not Set')
and technical_completion <> IFNULL(AUFK_IDAT2,'1970-01-01');

update dim_ordermaster
set "close" = IFNULL(AUFK_IDAT3,'1970-01-01')
from dim_ordermaster, AUFK
where "order" = ifnull(AUFK_AUFNR, 'Not Set')
and "close" <> IFNULL(AUFK_IDAT3,'1970-01-01');

/*Alin App-7223 22 Oct*/
update dim_ordermaster d
set CREATED = IFNULL(AUFK_PHAS0,'Not Set'),
dw_update_date = current_timestamp
from AUFK, dim_ordermaster d
where d."order" = IFNULL(AUFK_AUFNR, 'Not Set')
and CREATED <> ifnull(AUFK_PHAS0,'Not Set');

update dim_ordermaster d
set Released = IFNULL(AUFK_PHAS1,'Not Set'),
dw_update_date = current_timestamp
from AUFK, dim_ordermaster d
where d."order" = IFNULL(AUFK_AUFNR, 'Not Set')
and Released <> ifnull(AUFK_PHAS1,'Not Set');

update dim_ordermaster d
set Completed = IFNULL(AUFK_PHAS2,'Not Set'),
dw_update_date = current_timestamp
from AUFK, dim_ordermaster d
where d."order" = IFNULL(AUFK_AUFNR, 'Not Set')
and Completed <> ifnull(AUFK_PHAS2,'Not Set');

update dim_ordermaster d
set Closed = IFNULL(AUFK_PHAS3,'Not Set'),
dw_update_date = current_timestamp
from AUFK, dim_ordermaster d
where d."order" = IFNULL(AUFK_AUFNR, 'Not Set')
and Closed <> ifnull(AUFK_PHAS3,'Not Set');

/*Alin 31 Oct 2018 APP-9909*/
update dim_ordermaster d
set discontinued = 'X'
from JEST_JSTO_AUFK jja, dim_ordermaster d
where  d."order" = IFNULL(jja.AUFK_AUFNR, 'Not Set')
and jest_stat = 'E0023'
and JSTO_STSMA like 'ZPM%'
and JEST_INACT is null;

update dim_ordermaster d
set execution_wo_completed = 'X'
from JEST_JSTO_AUFK jja, dim_ordermaster d
where  d."order" = IFNULL(jja.AUFK_AUFNR, 'Not Set')
and jest_stat = 'E0034'
and JSTO_STSMA like 'ZPM%'
and JEST_INACT is null;

delete from number_fountain m where m.table_name = 'dim_ordermaster';