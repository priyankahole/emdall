UPDATE    dim_Address da

   SET da.Address1 = ifnull(adrc_name1, 'Not Set'),
       da.Address2 = ifnull(adrc_name2, 'Not Set'),
       da.City = ifnull(adrc_city1, 'Not Set'),
       da.State = ifnull(adrc_region, 'Not Set'),
       da.Zip = ifnull(adrc_post_Code1, 'Not Set'),
       da.Country = ifnull(adrc_country, 'Not Set'),
       da.CountryName = ifnull(t005t_landx, 'Not Set'),
	da.street = ifnull(adrc_street,'Not Set'),  /* Marius 16 Dec 2014 + str_suppl1/2/3 + location */
	da.str_suppl1 = ifnull(adrc_str_suppl1,'Not Set'),
	da.str_suppl2 = ifnull(adrc_str_suppl2,'Not Set'),
	da.str_suppl3 = ifnull(adrc_str_suppl3,'Not Set'),
	da.location = ifnull(adrc_location,'Not Set')
          FROM
          adrc a, t005t, dim_Address da
   WHERE   a.adrc_addrnumber = da.AddressNumber
          AND a.adrc_addrnumber IS NOT NULL
          AND a.adrc_country = t005t_land1;
