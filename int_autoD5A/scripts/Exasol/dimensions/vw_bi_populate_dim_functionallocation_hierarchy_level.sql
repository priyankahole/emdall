drop table if exists tmp_IFLOTX;
create table tmp_IFLOTX as
select iflotx_tplnr,iflotx_pltxu from IFLOTX
where iflotx_spras = 'E'
union
select iflotx_tplnr,iflotx_pltxu from IFLOTX a 
where iflotx_spras <> 'E'
and not exists (select 1 from IFLOTX b
where b.iflotx_spras = 'E'
and a.iflotx_tplnr = b.iflotx_tplnr);

delete from hierarchy
where hierarchyname = 'Functional Location';

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,
       0 as "level",
       1 as subhierarchy,
       1 as root,
       'MN' as nodetype,
        0 sequenceno,
        a.IFLOT_TPLNR as memberid,
        a.IFLOT_TPLNR as parentid,
        ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, 
        tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is null;

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       1 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 0 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       2 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 1 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       3 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 2 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       4 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 3 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       5 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 4 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       6 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 5 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       7 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 6 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       8 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 7 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

insert into hierarchy
       (hierarchycode,
        hierarchyname,
        tabname,
        fieldname,
        "level",
        subhierarchy,
        root,
        nodetype,
        sequenceno,
        memberid,
        parentid,
        memberiddesc)
 select 7777 as hierarchycode,
       'Functional Location' as hierarchyname,
       'dim_functionallocation' as tabname,
       'functonallocation' as fieldname,   
       9 as "level",
       1 as subhierarchy,
       1 as root,
       'CN' as nodetype,
       0 as sequenceno,
       a.IFLOT_TPLNR as memberid,
       a.IFLOT_TPLMA as parentid,
       ifnull(b.iflotx_pltxu,'Not Set') as memberiddesc from IFLOT a, tmp_IFLOTX b
where a.IFLOT_TPLNR = b.IFLOTX_TPLNR
and iflot_tplma is not null
and exists (select 1 from hierarchy c where c."level" = 8 and a.IFLOT_TPLMA = c.memberid)
and not exists (select 1 from hierarchy d where a.IFLOT_TPLNR = d.memberid);

update hierarchy a
set nodetype = 'LF'
where not exists (select 1 from hierarchy b
where a.memberid = b.parentid)
and hierarchyname = 'Functional Location';

update hierarchy
set root = substr(parentid,1,2);

update hierarchy
set root = parentid
where memberid = parentid;

insert into hierarchy(hierarchycode,"level",subhierarchy,nodetype,sequenceno,fieldname,memberiddesc,hierarchyname,tabname,memberid,parentid,root)
values(7777,0,1,'MN',0,'functonallocation','Not Set','Functional Location','dim_functionallocation','Not Set','Not Set1','1');