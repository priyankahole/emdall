/* ##################################################################################################################
  
     Script         : vw_bi_populate_dim_characteristic
     Author         : Simona
     Created On     : 1 April 2014

  
     Change History
     Date            By        Version           Desc
#################################################################################################################### */

SELECT 'START OF PROC vw_bi_populate_dim_characteristic',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS tmp_dim_characteristic;

CREATE TABLE tmp_dim_characteristic
LIKE Dim_Characteristic INCLUDING DEFAULTS INCLUDING IDENTITY;



SELECT 'Insert 1 on tmp_dim_characteristic',TIMESTAMP(LOCAL_TIMESTAMP);

DELETE FROM NUMBER_FOUNTAIN M where M.TABLE_NAME = 'tmp_dim_characteristic';

INSERT INTO NUMBER_FOUNTAIN 
select 	'tmp_dim_characteristic',
	ifnull(max(d.Dim_CharacteristicId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_dim_characteristic d
where d.Dim_CharacteristicId <> 1;

INSERT INTO tmp_Dim_Characteristic( Dim_CharacteristicId
  ,InternalCharNumber
  ,CharacteristicName
  ,CharacteristicID
  ,Status
  ,RowStartDate
  ,RowEndDate
  ,RowIsCurrent
  ,RowChangeReason )
SELECT 			1,
				0,
				'Not Set',
				'Not Set',
				'Not Set',
				current_timestamp,
				null,
				1,
				null
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM Dim_Characteristic WHERE Dim_CharacteristicId = 1);

INSERT INTO tmp_Dim_Characteristic( Dim_CharacteristicId
  ,InternalCharNumber
  ,CharacteristicName
  ,CharacteristicID
  ,Status
  ,RowStartDate
  ,RowEndDate
  ,RowIsCurrent
  ,RowChangeReason )
								
SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'tmp_Dim_Characteristic') 
		+ row_number() over (order by '')
	    ) Dim_CharacteristicId,
a.*	
FROM		 
(SELECT DISTINCT    ifnull(CABN.CABN_ATINN,0),
					ifnull(CABN.CABN_ATNAM,'Not Set'),
					ifnull(CABN.CABN_ATIDN,'Not Set'),
					ifnull(CABN.CABN_ATMST,'Not Set'),
					current_timestamp,
					null,
					1,
					null
FROM CABN
)a;
  
DELETE FROM NUMBER_FOUNTAIN M where M.TABLE_NAME = 'tmp_dim_characteristic';

/*CALL vectorwise(combine 'tmp_Dim_Characteristic')
CALL vectorwise(combine 'Dim_Characteristic - Dim_Characteristic')
CALL vectorwise(combine 'Dim_Characteristic')
CALL vectorwise(combine 'Dim_Characteristic + tmp_Dim_Characteristic')*/
DROP TABLE if EXISTS Dim_Characteristic;
RENAME tmp_dim_characteristic to Dim_Characteristic;

SELECT 'END OF PROC  vw_bi_populate_dim_characteristic',TIMESTAMP(LOCAL_TIMESTAMP);