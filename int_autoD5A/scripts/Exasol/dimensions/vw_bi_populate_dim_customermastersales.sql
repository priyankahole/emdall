INSERT INTO dim_customermastersales(dim_customermastersalesid,RowIsCurrent)
   SELECT 1,1
     FROM ( SELECT 1 )  AS t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_customermastersales
               WHERE dim_customermastersalesid = 1);

/*Update Process */
UPDATE dim_customermastersales
   SET ABCClassification = ifnull(KNVV_KLABC, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND ABCClassification <> ifnull(KNVV_KLABC, 'Not Set');

UPDATE dim_customermastersales
   SET AccountAssignmentGroup = ifnull(KNVV_KTGRD, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND AccountAssignmentGroup <> ifnull(KNVV_KTGRD, 'Not Set');

merge into dim_customermastersales  cm
using (select distinct dim_customermastersalesid, TVKTT_VTEXT, row_number() over (partition by dim_customermastersalesid order by dim_customermastersalesid) as rowseqno
 FROM KNVV, TVKTT, dim_customermastersales
 WHERE knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND tvktt.TVKTT_KTGRD = KNVV_KTGRD) t on t.dim_customermastersalesid=cm.dim_customermastersalesid
when matched then update set AccountAssignmentGroupDesciption = ifnull(t.TVKTT_VTEXT, 'Not Set'),
 dw_update_date = current_timestamp
where rowseqno=1
and AccountAssignmentGroupDesciption <> ifnull(t.TVKTT_VTEXT, 'Not Set');


UPDATE dim_customermastersales
   SET SalesOffice = ifnull(knvv_VKBUR, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND SalesOffice <> ifnull(knvv_VKBUR, 'Not Set');

UPDATE dim_customermastersales
   SET SalesOfficeDescription = ifnull(TVKBT_BEZEI, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVKBT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_VKBUR = TVKBT_VKBUR
       AND SalesOfficeDescription <> ifnull(TVKBT_BEZEI, 'Not Set');

UPDATE dim_customermastersales
   SET SalesDistrict = ifnull(KNVV_BZIRK, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND SalesDistrict <> ifnull(KNVV_BZIRK, 'Not Set');

UPDATE dim_customermastersales
   SET SalesDistrictDescription = ifnull(T171T_BZTXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, T171T, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNVV_BZIRK = T171T_BZIRK
       AND SalesDistrictDescription <> ifnull(T171T_BZTXT, 'Not Set');

UPDATE dim_customermastersales
   SET CustomerGroup = ifnull(KNVV_KDGRP, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND CustomerGroup <> ifnull(KNVV_KDGRP, 'Not Set');

UPDATE dim_customermastersales
   SET CustomerGroupDescription = ifnull(T151T_KTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, T151T, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND knvv.knvv_KDGRP = T151T_KDGRP
       AND CustomerGroupDescription <> ifnull(T151T_KTEXT, 'Not Set');

UPDATE dim_customermastersales
   SET PaymentTerms = ifnull(KNVV_ZTERM, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND PaymentTerms <> ifnull(KNVV_ZTERM, 'Not Set');

/* 5 Jul 2016 Madalina BI-3360 */
UPDATE dim_customermastersales 
set paymentTermsDescription = ifnull(TVZBT_VTEXT, 'Not Set'),
	dw_update_date = current_timestamp
FROM TVZBT t, dim_customermastersales 
where paymentTerms = ifnull(TVZBT_ZTERM, 'Not Set')
	and paymentTermsDescription <> ifnull(TVZBT_VTEXT, 'Not Set');
/* END 5 Jul 2016 */
UPDATE dim_customermastersales
   SET CustomerName = ifnull(k.KNA1_NAME1, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, KNA1 k, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND KNA1_KUNNR = knvv.knvv_KUNNR
       AND CustomerName <> ifnull(k.KNA1_NAME1, 'Not Set');

UPDATE dim_customermastersales
   SET DistributionChannelDescription = ifnull(TVTWT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVTWT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVTWT_VTWEG = knvv.KNVV_VTWEG
       AND DistributionChannelDescription <> ifnull(TVTWT_VTEXT, 'Not Set');

UPDATE dim_customermastersales
   SET Division = ifnull(TSPAT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TSPAT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TSPAT_SPART = knvv.KNVV_SPART
       AND Division <> ifnull(TSPAT_VTEXT, 'Not Set');

UPDATE dim_customermastersales
   SET SalesOrgDescription = ifnull(TVKOT_VTEXT, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, TVKOT, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND TVKOT_VKORG = knvv.KNVV_VKORG
       AND SalesOrgDescription <> ifnull(TVKOT_VTEXT, 'Not Set');


UPDATE dim_customermastersales
   SET Customergroup3 = ifnull(KNVV_KVGR3, 'Not Set'),
			dw_update_date = current_timestamp
FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND ifnull(Customergroup3,'X') <> ifnull(KNVV_KVGR3, 'Not Set')
       AND Customergroup3 <> ifnull(KNVV_KVGR3, 'Not Set');

 UPDATE dim_customermastersales
 SET deletionFlag = ifnull(KNVV_LOEVM, 'Not Set'),
 			dw_update_date = current_timestamp
 FROM KNVV, dim_customermastersales
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND deletionFlag <> ifnull(KNVV_LOEVM, 'Not Set');


 /*Georgiana EA Transition Addons*/

  UPDATE dim_customermastersales
 SET customerorderblock = ifnull(KNVV_AUFSD, 'Not Set'),
 			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND customerorderblock <> ifnull(KNVV_AUFSD, 'Not Set');


  UPDATE dim_customermastersales
 SET customerbillingblock = ifnull(KNVV_FAKSD, 'Not Set'),
 			dw_update_date = current_timestamp
FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND customerbillingblock <> ifnull(KNVV_FAKSD, 'Not Set');

  UPDATE dim_customermastersales
 SET pricegroup = ifnull(KNVV_KONDA, 'Not Set'),
 			dw_update_date = current_timestamp
  FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND pricegroup <> ifnull(KNVV_KONDA, 'Not Set');

   UPDATE dim_customermastersales
 SET customerdeliveryblock = ifnull(KNVV_LIFSD, 'Not Set'),
 			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND customerdeliveryblock <> ifnull(KNVV_LIFSD, 'Not Set');

    UPDATE dim_customermastersales
 SET pricelisttype = ifnull(KNVV_PLTYP, 'Not Set'),
 			dw_update_date = current_timestamp
  FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND pricelisttype <> ifnull(KNVV_PLTYP, 'Not Set');

      UPDATE dim_customermastersales
 SET ShippingConditions = ifnull(KNVV_VSBED, 'Not Set'),
 			dw_update_date = current_timestamp
  FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND ShippingConditions <> ifnull(KNVV_VSBED, 'Not Set');

      UPDATE dim_customermastersales
 SET itemproposal = ifnull(KNVV_VSORT, 'Not Set'),
 			dw_update_date = current_timestamp
  FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
 AND KNVV_VTWEG = DistributionChannel
 AND KNVV_VKORG = SalesOrg
 AND KNVV_SPART = DivisionCode
 AND itemproposal <> ifnull(KNVV_VSORT, 'Not Set');

 /*Georgiana End of Changes*/


  /* Start New fields added as a part of Columbia Report 856 */
UPDATE dim_customermastersales
   SET custpricingProcedure = ifnull(KNVV_KALKS, 'Not Set'),
			dw_update_date = current_timestamp
  FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND custpricingProcedure <> ifnull(KNVV_KALKS, 'Not Set');

UPDATE dim_customermastersales
   SET deliveryPriority = ifnull(KNVV_LPRIO,1.0000),
			dw_update_date = current_timestamp
    FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND deliveryPriority <> ifnull(KNVV_LPRIO,1.0000);

UPDATE dim_customermastersales
   SET incoterm1 = ifnull(KNVV_INCO1, 'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND incoterm1 <> ifnull(KNVV_INCO1, 'Not Set');

UPDATE dim_customermastersales
   SET incoTerm2 = ifnull(KNVV_INCO2, 'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND incoTerm2 <> ifnull(KNVV_INCO2, 'Not Set');

UPDATE dim_customermastersales
   SET maxPartialDelivery = ifnull(KNVV_ANTLF,1.0000),
			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND maxPartialDelivery <> ifnull(KNVV_ANTLF,1.0000);

UPDATE dim_customermastersales
   SET storeLocator = ifnull(KNVV_AGREL, 'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND storeLocator <> ifnull(KNVV_AGREL, 'Not Set');



UPDATE dim_customermastersales
   SET customergroup5 = ifnull(KNVV_KVGR5, 'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup5 <> ifnull(KNVV_KVGR5, 'Not Set');

UPDATE dim_customermastersales
   SET customergroup2 = ifnull(KNVV_KVGR2, 'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup2 <> ifnull(KNVV_KVGR2, 'Not Set');

UPDATE dim_customermastersales
   SET customergroup1 = ifnull(KNVV_KVGR1, 'Not Set'),
			dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
 WHERE     knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND customergroup1 <> ifnull(KNVV_KVGR1, 'Not Set');


/* End New fields added as a part of Columbia Report 856 Nov 2014*/

/* NN - 10 Dec 2015 - New field added as part of BI-1242 */
/*
UPDATE dim_customermastersales
FROM KNVV
SET dd_ItemProposal = ifnull(KNVV_VSORT, 'Not Set'),
  dw_update_date = current_timestamp
WHERE  knvv_KUNNR = CustomerNumber
       AND KNVV_VTWEG = DistributionChannel
       AND KNVV_VKORG = SalesOrg
       AND KNVV_SPART = DivisionCode
       AND dd_ItemProposal <> ifnull(KNVV_VSORT, 'Not Set')
*/
/* NN - 10 Dec 2015 - New field added as part of BI-1242 */

/* Insert Process */
/* Modifed on 12222014 to fix issue while processing */
truncate table tmp_dim_customermastersales_knvv_ins;
truncate table tmp_dim_customermastersales_knvv_del;

drop table if exists tmp_dim_customermastersales_knvv_ins;
create table tmp_dim_customermastersales_knvv_ins as
select knvv_KUNNR,KNVV_VTWEG,KNVV_VKORG,KNVV_SPART
FROM    KNVV;

drop table if exists tmp_dim_customermastersales_knvv_del;
create table tmp_dim_customermastersales_knvv_del as
select * from tmp_dim_customermastersales_knvv_ins where 1=2;
/* end of modification on 12222014 */

insert into tmp_dim_customermastersales_knvv_del
select CustomerNumber,DistributionChannel,SalesOrg,DivisionCode
from dim_customermastersales;

merge into tmp_dim_customermastersales_knvv_ins dst
using (select distinct t0.rowid rid
	   from tmp_dim_customermastersales_knvv_ins t0
				inner join tmp_dim_customermastersales_knvv_del t1 on ifnull(t0.knvv_KUNNR,'Not Set') = ifnull(t1.knvv_KUNNR,'Not Set') and
																	  ifnull(t0.KNVV_VTWEG,'Not Set') = ifnull(t1.KNVV_VTWEG,'Not Set') and
																	  ifnull(t0.KNVV_VKORG,'Not Set') = ifnull(t1.KNVV_VKORG,'Not Set') and
																	  ifnull(t0.KNVV_SPART,'Not Set') = ifnull(t1.KNVV_SPART,'Not Set')
	   ) src on dst.rowid = src.rid
when matched then delete;
		/* VW Original
			call vectorwise (combine 'tmp_dim_customermastersales_knvv_ins-tmp_dim_customermastersales_knvv_del') */

drop table if exists tmp_dcms_ins_t1;
create table tmp_dcms_ins_t1 as
select distinct	ifnull(KNVV_KLABC,'Not Set') ABCClassification,
		ifnull(KNVV_KTGRD,'Not Set') AccountAssignmentGroup,
        convert(varchar(200), 'Not Set') as AccountAssignmentGroupDesciption, 		-- ifnull((SELECT TVKTT_VTEXT FROM TVKTT WHERE tvktt.TVKTT_KTGRD = knvv.KNVV_KTGRD), 'Not Set') AccountAssignmentGroupDesciption,
		convert(varchar(200), 'Not Set') as CustomerName, 							-- ifnull((SELECT k1.KNA1_NAME1 FROM KNA1 k1 WHERE k1.KNA1_KUNNR = knvv.KNVV_KUNNR), 'Not Set') CustomerName,
		ifnull(knvv.knvv_KUNNR,'Not Set') CustomerNumber,
		ifnull(knvv.knvv_VTWEG,'Not Set') DistributionChannel,
		convert(varchar(200), 'Not Set') as DistributionChannelDescription, 		-- ifnull((SELECT t.TVTWT_VTEXT FROM TVTWT t WHERE t.TVTWT_VTWEG = knvv.KNVV_VTWEG), 'Not Set') DistributionChannelDescription,
		convert(varchar(200), 'Not Set') as Division, 								-- ifnull((SELECT t.TSPAT_VTEXT FROM TSPAT t WHERE t.TSPAT_SPART = knvv.KNVV_SPART), 'Not Set') Division,
		ifnull(knvv.knvv_SPART,'Not Set') DivisionCode,
		1 RowIsCurrent,
		current_Date RowStartDate,
		ifnull(knvv.knvv_VKORG,'Not Set') SalesOrg,
		convert(varchar(200), 'Not Set') as SalesOrgDescription, 					-- ifnull((SELECT t.TVKOT_VTEXT FROM TVKOT t WHERE t.TVKOT_VKORG = knvv.KNVV_VKORG), 'Not Set') SalesOrgDescription,
		ifnull(knvv_VKBUR,'Not Set') SalesOffice,
		convert(varchar(200), 'Not Set') as SalesOfficeDescription, 				-- ifnull((SELECT t.TVKBT_BEZEI FROM TVKBT t WHERE t.TVKBT_VKBUR = knvv.KNVV_VKBUR), 'Not Set') SalesOfficeDescription,
		ifnull(KNVV_BZIRK,'Not Set') SalesDistrict,
		convert(varchar(200), 'Not Set') as SalesDistrictDescription, 				-- ifnull((SELECT t.T171T_BZTXT FROM T171T t WHERE t.T171T_BZIRK = knvv.KNVV_BZIRK), 'Not Set') SalesDistrictDescription,
		ifnull(knvv_KDGRP,'Not Set') CustomerGroup,
		convert(varchar(200), 'Not Set') as CustomerGroupDescription, 				-- ifnull((SELECT t.T151T_KTEXT FROM T151T t WHERE t.T151T_KDGRP = knvv.KNVV_KDGRP), 'Not Set') CustomerGroupDescription,
		ifnull(knvv_zterm,'Not Set') PaymentTerms,
		'Not Set' PaymentTermsDescription,
		ifnull(KNVV_KVGR3,'Not Set') CustomerGroup3,
		convert(varchar(200), 'Not Set') as CustomerGroup3Description,              --	  ifnull((SELECT t.TVV3T_BEZEI FROM TVV3T t WHERE t.TVV3T_KVGR3 = knvv.KNVV_KVGR3),'Not Set') CustomerGroup3Description,
	    ifnull(KNVV_VKGRP,'Not Set') SalesGroup
		,ifnull(KNVV_KALKS,'Not Set') custpricingProcedure
		,ifnull(KNVV_LPRIO,0.0000) deliveryPriority
		,ifnull(KNVV_INCO1, 'Not Set') incoterm1
		,ifnull(KNVV_INCO2,'Not Set') incoTerm2
		,ifnull(KNVV_ANTLF,0.0000) maxPartialDelivery
		,ifnull(KNVV_AGREL,'Not Set') storeLocator
		,ifnull(KNVV_KVGR5,'Not Set') customergroup5
		,ifnull(KNVV_KVGR2,'Not Set') customergroup2
		,ifnull(KNVV_KVGR1,'Not Set') customergroup1
		,ifnull(KNVV_LOEVM,'Not Set') deletionFlag,
		ifnull(KNVV_AUFSD, 'Not Set') customerorderblock,
		ifnull(KNVV_FAKSD, 'Not Set') customerbillingblock,
		ifnull(KNVV_KONDA, 'Not Set') pricegroup,
		ifnull(KNVV_LIFSD, 'Not Set') customerdeliveryblock,
		ifnull( KNVV_PLTYP, 'Not Set') pricelisttype,
		ifnull(KNVV_VSBED, 'Not Set') ShippingConditions,
		ifnull(KNVV_VSORT, 'Not Set') itemproposal
FROM KNVV knvv,
	 tmp_dim_customermastersales_knvv_ins c
WHERE    knvv.knvv_KUNNR = c.knvv_KUNNR
     AND knvv.KNVV_VTWEG = c.KNVV_VTWEG
     AND knvv.KNVV_VKORG = c.KNVV_VKORG
     AND knvv.KNVV_SPART = c.KNVV_SPART;

/* AccountAssignmentGroupDesciption - Ambiguous replace*/

merge into tmp_dcms_ins_t1 knvv
using (select distinct t.TVKTT_VTEXT, t.TVKTT_KTGRD, row_number() over (partition by t.TVKTT_KTGRD order by t.TVKTT_KTGRD ) as rowseqno
 from TVKTT t) t2 on t2.TVKTT_KTGRD = knvv.AccountAssignmentGroup
when matched then update set knvv.AccountAssignmentGroupDesciption = ifnull(t2.TVKTT_VTEXT, 'Not Set')
 where
rowseqno = 1 
and knvv.AccountAssignmentGroupDesciption <> ifnull(t2.TVKTT_VTEXT, 'Not Set');


/* CustomerName */
update tmp_dcms_ins_t1 knvv
set knvv.CustomerName = ifnull(t.KNA1_NAME1, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join KNA1 t on t.KNA1_KUNNR = knvv.CustomerNumber
where knvv.CustomerName <> ifnull(t.KNA1_NAME1, 'Not Set');

/* DistributionChannelDescription */
update tmp_dcms_ins_t1 knvv
set knvv.DistributionChannelDescription = ifnull(t.TVTWT_VTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TVTWT t on t.TVTWT_VTWEG = knvv.DistributionChannel
where knvv.DistributionChannelDescription <> ifnull(t.TVTWT_VTEXT, 'Not Set');

/* Division */
update tmp_dcms_ins_t1 knvv
set knvv.Division = ifnull(t.TSPAT_VTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TSPAT t on t.TSPAT_SPART = knvv.DivisionCode
where knvv.Division <> ifnull(t.TSPAT_VTEXT, 'Not Set');

/* SalesOrgDescription */
update tmp_dcms_ins_t1 knvv
set knvv.SalesOrgDescription = ifnull(t.TVKOT_VTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TVKOT t on t.TVKOT_VKORG = knvv.SalesOrg
where knvv.SalesOrgDescription <> ifnull(t.TVKOT_VTEXT, 'Not Set');

/* SalesOfficeDescription */
update tmp_dcms_ins_t1 knvv
set knvv.SalesOfficeDescription = ifnull(t.TVKBT_BEZEI, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TVKBT t on t.TVKBT_VKBUR = knvv.SalesOffice
where knvv.SalesOfficeDescription <> ifnull(t.TVKBT_BEZEI, 'Not Set');

/* SalesDistrictDescription */
update tmp_dcms_ins_t1 knvv
set knvv.SalesDistrictDescription = ifnull(t.T171T_BZTXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join T171T t on t.T171T_BZIRK = knvv.SalesDistrict
where knvv.SalesDistrictDescription <> ifnull(t.T171T_BZTXT, 'Not Set');

/* CustomerGroupDescription */
update tmp_dcms_ins_t1 knvv
set knvv.CustomerGroupDescription = ifnull(t.T151T_KTEXT, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join T151T t on t.T151T_KDGRP = knvv.CustomerGroup
where knvv.CustomerGroupDescription <> ifnull(t.T151T_KTEXT, 'Not Set');

/*CustomerGroup3Description*/
update tmp_dcms_ins_t1 knvv
set knvv.CustomerGroup3Description = ifnull(t.TVV3T_BEZEI, 'Not Set')
from tmp_dcms_ins_t1 knvv
		left join TVV3T t on t.TVV3T_KVGR3 = knvv.CustomerGroup3
where knvv.CustomerGroup3Description <> ifnull(t.TVV3T_BEZEI, 'Not Set');


delete from number_fountain m where m.table_name = 'dim_customermastersales';

insert into number_fountain
select 	'dim_customermastersales',
	ifnull(max(d.dim_customermastersalesid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customermastersales d
where d.dim_customermastersalesid <> 1;

INSERT INTO dim_customermastersales(
				Dim_CustomerMasterSalesId,
				ABCClassification,
                                    AccountAssignmentGroup,
                                    AccountAssignmentGroupDesciption,
                                    CustomerName,
                                    CustomerNumber,
                                    DistributionChannel,
                                    DistributionChannelDescription,
                                    Division,
                                    DivisionCode,
                                    RowIsCurrent,
                                    RowStartDate,
                                    SalesOrg,
                                    SalesOrgDescription,
                                    SalesOffice,
                                    SalesOfficeDescription,
                                    SalesDistrict,
                                    SalesDistrictDescription,
                                    CustomerGroup,
                                    CustomerGroupDescription,
                                    PaymentTerms,
                                    PaymentTermsDescription,
				    CustomerGroup3,
				    CustomerGroup3Description,
				    SalesGroup
				    /* Start New fields added as a part of Columbia 856 report Nov 2014*/
						,pricingProcedure
						,deliveryPriority
						,incoterm1
						,incoTerm2
						,maxPartialDelivery
						,storeLocator

				    	,customergroup5
					,customergroup2
					,customergroup1
					, deletionFlag,
                    /*Ea Transition Addons*/
					customerorderblock,
					customerbillingblock,
					pricegroup,
					customerdeliveryblock,
					pricelisttype,
					ShippingConditions,
					itemproposal
					/*End of EA Transition Addons*/
					                   )
 SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customermastersales') + row_number() over(order by '') Dim_CustomerMasterSalesId,
		a.*
	FROM (SELECT distinct 		ABCClassification,
                                    AccountAssignmentGroup,
                                    AccountAssignmentGroupDesciption,
                                    CustomerName,
                                    CustomerNumber,
                                    DistributionChannel,
                                    DistributionChannelDescription,
                                    Division,
                                    DivisionCode,
                                    RowIsCurrent,
                                    RowStartDate,
                                    SalesOrg,
                                    SalesOrgDescription,
                                    SalesOffice,
                                    SalesOfficeDescription,
                                    SalesDistrict,
                                    SalesDistrictDescription,
                                    CustomerGroup,
                                    CustomerGroupDescription,
                                    PaymentTerms,
                                    PaymentTermsDescription,
				    CustomerGroup3,
				    CustomerGroup3Description,
				    SalesGroup
				    /* Start New fields added as a part of Columbia 856 report Nov 2014*/
				,custpricingProcedure
						,deliveryPriority
						,incoterm1
						,incoTerm2
						,maxPartialDelivery
						,storeLocator

				    	,customergroup5
					,customergroup2
					,customergroup1
					, deletionFlag,
                    /*Ea Transition Addons*/
					customerorderblock,
					customerbillingblock,
					pricegroup,
					customerdeliveryblock,
					pricelisttype,
					ShippingConditions,
					itemproposal
	  from tmp_dcms_ins_t1 ) a;

drop table if exists tmp_dcms_ins_t1;

UPDATE dim_customermastersales
SET productattributeid1 = ifnull(KNVV_PRAT1, 'Not Set'),
dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
WHERE
    knvv_KUNNR = CustomerNumber
AND KNVV_VTWEG = DistributionChannel
AND KNVV_VKORG = SalesOrg
AND KNVV_SPART = DivisionCode
AND productattributeid1 <> ifnull(KNVV_PRAT1, 'Not Set');

UPDATE dim_customermastersales
SET productattributeid2 = ifnull(KNVV_PRAT2, 'Not Set'),
dw_update_date = current_timestamp
 FROM dim_customermastersales, KNVV
WHERE
    knvv_KUNNR = CustomerNumber
AND KNVV_VTWEG = DistributionChannel
AND KNVV_VKORG = SalesOrg
AND KNVV_SPART = DivisionCode
AND productattributeid2 <> ifnull(KNVV_PRAT2, 'Not Set');

delete from number_fountain m where m.table_name = 'dim_customermastersales';

		/* VW Original
			call vectorwise (combine 'tmp_dim_customermastersales_knvv_ins-tmp_dim_customermastersales_knvv_ins')
			call vectorwise (combine 'tmp_dim_customermastersales_knvv_del-tmp_dim_customermastersales_knvv_del') */
truncate table tmp_dim_customermastersales_knvv_ins;
truncate table tmp_dim_customermastersales_knvv_del;



