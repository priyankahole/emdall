insert into dim_customer
(Dim_CustomerID,RowChangeReason,RowEndDate,RowIsCurrent,RowStartDate,Industry,CustomerNumber,Country,Name,
City,PostalCode,Region,Plant,OneTime,IndustryCode,Classification,ClassificationDescription,CreationDate,CreditRep,CreditLimit,
RiskClass,CustAccountGroup,RegionDescription,CustAccountGroupDescription,Name2,HighCustomerNumber,HigherCustomerNumber,
TopLevelCustomerNumber,HierarchyType,HighCustomerDesc,HigherCustomerDesc,TopLevelCustomerDesc,CountryName,Channel,SubChannel,
ChannelDescription,SubChannelDescription,TradingPartner,Attribute3,Attribute4,Attribute5,Attribute6,Attribute7,Attribute8,ConditionGroup1,
ConditionGroup2,IndustryCode1,ConditionGroupDescription2,IndustryCode1Description)
SELECT
1,null,null,1,current_timestamp,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set',null,'Not Set',0.00,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set'
FROM (SELECT 1) a
where not exists(select 1 from dim_customer where dim_customerid = 1); 

/* ADDING MISSING VALUES ACCORDING TO BI-3627*/
delete from kna1 where kna1_kunnr like '%10277747%';
delete from kna1 where kna1_kunnr like '%10305341%';
delete from kna1 where kna1_kunnr like '%00300086%';

insert into kna1(KNA1_KUNNR,KNA1_NAME1,KNA1_WERKS,KNA1_LAND1,KNA1_XCPDK,KNA1_ORT01,KNA1_PSTLZ,KNA1_REGIO,KNA1_BRSCH,KNA1_KUKLA,KNA1_DEAR2,
KNA1_ERDAT,KNA1_KTOKD,KNA1_NAME2,KNA1_KATR1,KNA1_KATR2,KNA1_VBUND,KNA1_KATR3,KNA1_KATR4,KNA1_KATR5,KNA1_KATR6,KNA1_KATR7,
KNA1_KATR8,KNA1_KDKG1,KNA1_KDKG2,KNA1_BRAN1,KNA1_FAKSD,KNA1_DATLT,KNA1_LIFSD,KNA1_LOEVM,KNA1_TELFX,KNA1_AUFSD,
KNA1_SORTL,KNA1_TELF1,KNA1_LZONE,KNA1_STRAS,KNA1_ADRNR,KNA1_SPRAS,KNA1_NODEL,KNA1_STCEG,KNA1_ANRED,KNA1_GFORM)
values
(
'0010277747',
'Landmark Braidwood',
NULL,
'AU',
NULL,
'Wetherill Park',
'2164',
'NSW',
NULL,NULL,NULL,
'2012-10-10', '0002', 'ID 0070122810', 
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
'LANDMARK B',NULL,NULL, '"Unit 7 - 136 Newton Rd"', '0000714855', 'E',NULL,NULL,NULL,NULL
);

insert into kna1(KNA1_KUNNR,KNA1_NAME1,KNA1_WERKS,KNA1_LAND1,KNA1_XCPDK,KNA1_ORT01,KNA1_PSTLZ,KNA1_REGIO,KNA1_BRSCH,KNA1_KUKLA,KNA1_DEAR2,
KNA1_ERDAT,KNA1_KTOKD,KNA1_NAME2,KNA1_KATR1,KNA1_KATR2,KNA1_VBUND,KNA1_KATR3,KNA1_KATR4,KNA1_KATR5,KNA1_KATR6,KNA1_KATR7,
KNA1_KATR8,KNA1_KDKG1,KNA1_KDKG2,KNA1_BRAN1,KNA1_FAKSD,KNA1_DATLT,KNA1_LIFSD,KNA1_LOEVM,KNA1_TELFX,KNA1_AUFSD,
KNA1_SORTL,KNA1_TELF1,KNA1_LZONE,KNA1_STRAS,KNA1_ADRNR,KNA1_SPRAS,KNA1_NODEL,KNA1_STCEG,KNA1_ANRED,KNA1_GFORM)
values
(
'0010305341',
'Gerard Bailey-Watts',
NULL,
'GB',
NULL,
'Tiverton',
'EX16 7LH',
'DV',
NULL,'Z1',NULL,
'2013-11-28', '0002', 'TEL: 07789 721445', 
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
'BAILEY-','WA',NULL,NULL, '1 Appledore Cottages', '0000820269', 'E',NULL,NULL,NULL
);

insert into kna1(KNA1_KUNNR,KNA1_NAME1,KNA1_WERKS,KNA1_LAND1,KNA1_XCPDK,KNA1_ORT01,KNA1_PSTLZ,KNA1_REGIO,KNA1_BRSCH,KNA1_KUKLA,KNA1_DEAR2,
KNA1_ERDAT,KNA1_KTOKD,KNA1_NAME2,KNA1_KATR1,KNA1_KATR2,KNA1_VBUND,KNA1_KATR3,KNA1_KATR4,KNA1_KATR5,KNA1_KATR6,KNA1_KATR7,
KNA1_KATR8,KNA1_KDKG1,KNA1_KDKG2,KNA1_BRAN1,KNA1_FAKSD,KNA1_DATLT,KNA1_LIFSD,KNA1_LOEVM,KNA1_TELFX,KNA1_AUFSD,
KNA1_SORTL,KNA1_TELF1,KNA1_LZONE,KNA1_STRAS,KNA1_ADRNR,KNA1_SPRAS,KNA1_NODEL,KNA1_STCEG,KNA1_ANRED,KNA1_GFORM)
values
(
'0000300086',
'Serge Allard',
NULL,
'US',
NULL,
'MILLSBORO',
'19966',
'DE',
NULL,NULL,NULL,
'2004-12-15', 'ZEMP', 'Intervet Inc.', 
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'(302) 933-4012',NULL,
'ALLARD','(800) 992-8051',NULL,'29160 Intervet Lane PO Box 318', '0000297014', 'E',NULL,NULL,NULL,NULL
);
/* ADDING MISSING VALUES ACCORDING TO BI-3627*/

drop table if exists tmp_cust_ins_t1;
create table tmp_cust_ins_t1 as
select  KNA1_KUNNR CustomerNumber, 
		ifnull(KNA1_BRSCH, 'Not Set') IndustryCode,
		convert(VARCHAR(200), 'Not Set') as Industry, 						-- ifnull((SELECT T5UNT_NTEXT FROM T5UNT WHERE T5UNT_NAICS = KNA1_BRSCH ),'Not Set') Industry,
		ifnull(KNA1_LAND1, 'Not Set') Country,
		ifnull(KNA1_NAME1, 'Not Set') Name,
		ifnull(KNA1_NAME2, 'Not Set') Name2,
		ifnull(KNA1_ORT01, 'Not Set') City,
		ifnull(KNA1_PSTLZ, 'Not Set') PostalCode,
		ifnull(KNA1_REGIO, 'Not Set') Region,
		CASE WHEN ifnull(KNA1_XCPDK, '') = 'X' THEN 'Yes' ELSE 'No' END OneTime,
		ifnull(KNA1_KUKLA,'Not Set') Classification,
		convert(VARCHAR(200), 'Not Set') as ClassificationDescription, 		-- ifnull((SELECT t.TKUKT_VTEXT FROM TKUKT t WHERE t.TKUKT_KUKLA = KNA1_KUKLA), 'Not Set') ClassificationDescription,
		1 RowIsCurrent,
		current_timestamp RowStartDate,
		KNA1_ERDAT CreationDate,
		ifnull(KNA1_KTOKD,'Not Set') CustAccountGroup,
		convert(VARCHAR(200), 'Not Set') as CustAccountGroupDescription,	-- ifnull((SELECT t.T077X_TXT30 FROM T077X t WHERE t.T077X_KTOKD = KNA1_KTOKD AND T.T077X_SPRAS = 'E'), 'Not Set') CustAccountGroupDescription,
		convert(VARCHAR(200), 'Not Set') as RegionDescription, 				--	ifnull((SELECT T005U_BEZEI FROM T005U WHERE T005U_LAND1 = KNA1_LAND1 AND T005U_BLAND = KNA1_REGIO),'Not Set') RegionDescription,
		ifnull(t005t_landx, 'Not Set') CountryName,
		ifnull(KNA1_KATR1, 'Not Set') Channel,
		ifnull(KNA1_KATR2, 'Not Set') SubChannel,
		convert(VARCHAR(200), 'Not Set') as ChannelDescription, 			-- ifnull((SELECT t.TVK1T_VTEXT FROM TVK1T t WHERE t.TVK1T_KATR1 = KNA1_KATR1),'Not Set') ChannelDescription,
		convert(VARCHAR(200), 'Not Set') as SubChannelDescription, 			-- ifnull((SELECT t.TVK2T_VTEXT FROM TVK2T t WHERE t.TVK2T_KATR2 = KNA1_KATR2),'Not Set') SubChannelDescription,
		ifnull(KNA1_VBUND,'Not Set') TradingPartner,
		ifnull(KNA1_KATR3,'Not Set') Attribute3,
		ifnull(KNA1_KATR4,'Not Set') Attribute4,
		ifnull(KNA1_KATR5,'Not Set') Attribute5,
		ifnull(KNA1_KATR6,'Not Set') Attribute6,
		ifnull(KNA1_KATR7,'Not Set') Attribute7,
		ifnull(KNA1_KATR8,'Not Set') Attribute8,
		ifnull(KNA1_KDKG1,'Not Set') ConditionGroup1,
		ifnull(KNA1_KDKG2,'Not Set') ConditionGroup2,
		ifnull(KNA1_BRAN1,'Not Set') IndustryCode1,
		convert(VARCHAR(200), 'Not Set') as ConditionGroupDescription2, 	-- ifnull((SELECT TVKGGT_VTEXT FROM TVKGGT where TVKGGT_KDKGR = KNA1_KDKG2),'Not Set') ConditionGroupDescription2,
		convert(VARCHAR(200), 'Not Set') as IndustryCode1Description, 		-- ifnull((SELECT TBRCT_VTEXT FROM TBRCT where TBRCT_BRACO = KNA1_BRAN1),'Not Set') IndustryCode1Description,
		ifnull(KNA1_STRAS,'Not Set') Address, 
		ifnull(KNA1_NODEL, 'Not Set') centraldeletionblock, /*Georgiana EA Addons 03 Feb 2016*/
	    ifnull(KNA1_STCEG, 'Not Set') vatregistrationno, /*End Of Changes*/	
	    	ifnull(KNA1_TELFX, 'Not Set') faxnumber,
	ifnull(KNA1_LOEVM, 'Not Set') centraldeletionflag,
	ifnull(KNA1_AUFSD, 'Not Set') centralorderblockcust,
	ifnull(KNA1_ANRED, 'Not Set')  title,
	ifnull(KNA1_GFORM, 'Not Set')  legalstatuscode
	
	FROM kna1 LEFT JOIN t005t on KNA1_LAND1 = T005T_LAND1
	WHERE NOT EXISTS (SELECT 1
	                FROM dim_customer dc
	               WHERE dc.CustomerNumber = kna1_kunnr);
				   
update tmp_cust_ins_t1 kn
set kn.Industry = ifnull(T5UNT_NTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join T5UNT t1 on t1.T5UNT_NAICS = kn.IndustryCode
where kn.Industry <> ifnull(T5UNT_NTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.ClassificationDescription = ifnull(TKUKT_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TKUKT t1 on t1.TKUKT_KUKLA = kn.Classification
where kn.ClassificationDescription <> ifnull(TKUKT_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.CustAccountGroupDescription = ifnull(T077X_TXT30, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join T077X t1 on t1.T077X_KTOKD = kn.CustAccountGroup AND T1.T077X_SPRAS = 'E'
where kn.CustAccountGroupDescription <> ifnull(T077X_TXT30, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.RegionDescription = ifnull(T005U_BEZEI, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join T005U t1 on t1.T005U_LAND1 = kn.Country AND T1.T005U_BLAND = Region
where kn.RegionDescription <> ifnull(T005U_BEZEI, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.ChannelDescription = ifnull(TVK1T_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TVK1T t1 on t1.TVK1T_KATR1 = kn.Channel
where kn.ChannelDescription <> ifnull(TVK1T_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.SubChannelDescription = ifnull(TVK2T_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TVK2T t1 on t1.TVK2T_KATR2 = kn.SubChannel
where kn.SubChannelDescription <> ifnull(TVK2T_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.ConditionGroupDescription2 = ifnull(TVKGGT_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TVKGGT t1 on t1.TVKGGT_KDKGR = kn.ConditionGroup2
where kn.ConditionGroupDescription2 <> ifnull(TVKGGT_VTEXT, 'Not Set');

update tmp_cust_ins_t1 kn
set kn.IndustryCode1Description = ifnull(TBRCT_VTEXT, 'Not Set')
FROM tmp_cust_ins_t1 kn
		left join TBRCT t1 on t1.TBRCT_BRACO = kn.IndustryCode1
where kn.IndustryCode1Description <> ifnull(TBRCT_VTEXT, 'Not Set');


delete from number_fountain m where m.table_name = 'dim_customer';

insert into number_fountain
select 	'dim_customer',
	ifnull(max(d.Dim_CustomerID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customer d
where d.Dim_CustomerID <> 1;

INSERT INTO dim_customer(Dim_CustomerID,
                         CustomerNumber,
                         IndustryCode,
                         Industry,
                         Country,
                         Name,
						 Name2,
                         City,
                         PostalCode,
                         Region,
                         OneTime,
						 Classification,
						 ClassificationDescription,
                         RowIsCurrent,
                         RowStartDate,
						 CreationDate,
						 CustAccountGroup,
						 CustAccountGroupDescription,
                         RegionDescription,
						 CountryName,
				         Channel,
			             SubChannel,
 			             ChannelDescription,
			             SubChannelDescription,
			             TradingPartner,
			             Attribute3,
						 Attribute4,
						 Attribute5,
						 Attribute6,
						 Attribute7,
						 Attribute8,
						 ConditionGroup1,
						 ConditionGroup2,
						 IndustryCode1,
						 ConditionGroupDescription2,
						 IndustryCode1Description,
						 Address, /*Georgiana EA Addons 03 Feb 2016*/
						 centraldeletionblock,
			             vatregistrationno, /*End of Changes*/
				     faxnumber,
				    centraldeletionflag,
			centralorderblockcust,
			/* Madalina 5 Jul 2016 BI-3360 */
			title,
			legalstatuscode)
			SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_customer') + row_number() over(order by '') Dim_CustomerID,
	   a.* 
from (
		SELECT distinct CustomerNumber,
					IndustryCode,
					Industry,
					Country,
					Name,
					Name2,
					City,
					PostalCode,
					Region,
					OneTime,
					Classification,
					ClassificationDescription,
					RowIsCurrent,
					RowStartDate,
					CreationDate,
					CustAccountGroup,
					CustAccountGroupDescription,
					RegionDescription,
					CountryName,
					Channel,
					SubChannel,
					ChannelDescription,
					SubChannelDescription,
					TradingPartner,
					Attribute3,
					Attribute4,
					Attribute5,
					Attribute6,
					Attribute7,
					Attribute8,
					ConditionGroup1,
					ConditionGroup2,
					IndustryCode1,
					ConditionGroupDescription2,
					IndustryCode1Description,
					Address,
					centraldeletionblock,
			        vatregistrationno,
				faxnumber,
				centraldeletionflag,
                                centralorderblockcust,
				title,
				legalstatuscode
		from tmp_cust_ins_t1) a;

UPDATE dim_customer dc 
   SET 	dc.IndustryCode = ifnull(KNA1_BRSCH, 'Not Set'),
		dc.Country = ifnull(KNA1_LAND1, 'Not Set'),
		dc.Name = ifnull(KNA1_NAME1, 'Not Set'),
		dc.Name2 = ifnull(KNA1_NAME2, 'Not Set'),	   
		dc.City = ifnull(KNA1_ORT01, 'Not Set'),
		dc.PostalCode = ifnull(KNA1_PSTLZ, 'Not Set'),
		dc.Region = ifnull(KNA1_REGIO, 'Not Set'),
		dc.OneTime =(CASE WHEN ifnull(KNA1_XCPDK, '') = 'X' THEN 'Yes' ELSE 'No' END),
		dc.Classification = ifnull(KNA1_KUKLA,'Not Set'),
		dc.CreationDate = KNA1_ERDAT,
		dc.CustAccountGroup = ifnull(KNA1_KTOKD,'Not Set'),
		dc.CountryName = ifnull(t005t_landx, 'Not Set'),
		dc.Channel = ifnull(KNA1_KATR1,'Not Set'),
		dc.SubChannel = ifnull(KNA1_KATR2,'Not Set'),
		dc.TradingPartner = ifnull(KNA1_VBUND,'Not Set'),
		dc.Attribute3 = ifnull(KNA1_KATR3,'Not Set'),
		dc.Attribute4 = ifnull(KNA1_KATR4,'Not Set'),
		dc.Attribute5 = ifnull(KNA1_KATR5,'Not Set'),
		dc.Attribute6 = ifnull(KNA1_KATR6,'Not Set'),
		dc.Attribute7 = ifnull(KNA1_KATR7,'Not Set'),
		dc.Attribute8 = ifnull(KNA1_KATR8,'Not Set'),
		dc.ConditionGroup1 = ifnull(KNA1_KDKG1,'Not Set'),
		dc.ConditionGroup2 = ifnull(KNA1_KDKG2,'Not Set'),
		dc.IndustryCode1 = ifnull(KNA1_BRAN1,'Not Set'),
		dc.Address = ifnull(KNA1_STRAS,'Not Set'),
		dc.telephone1 = ifnull(KNA1_TELF1,'Not Set'),

	/* Madalina 5 Jul 2016 BI-3360 */
	dc.title = ifnull(KNA1_ANRED, 'Not Set'),  
	dc.legalstatuscode = ifnull(KNA1_GFORM, 'Not Set'), 
    dc.centraldeletionflag = ifnull(KNA1_LOEVM, 'Not Set'),	
	dc.deletionflag =ifnull(KNA1_LOEVM, 'Not Set'),
	dc.centraldeletionblock = ifnull(KNA1_NODEL, 'Not Set') , 
	 dc.vatregistrationno = ifnull(KNA1_STCEG, 'Not Set'),
	 dc.centralorderblockcust = ifnull(KNA1_AUFSD, 'Not Set') ,
		dc.dw_update_date = current_timestamp
FROM dim_customer dc, kna1 kn, t005t      
	WHERE    dc.CustomerNumber = kn.kna1_kunnr 
	     AND dc.RowIsCurrent = 1 
	     AND KNA1_LAND1 = T005T_LAND1;

update dim_customer dc
set dc.Industry = ifnull(T5UNT_NTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join T5UNT t1 on t1.T5UNT_NAICS = kn.KNA1_BRSCH
where dc.RowIsCurrent = 1 and 
	  dc.Industry <> ifnull(T5UNT_NTEXT, 'Not Set');

update dim_customer dc
set dc.ClassificationDescription = ifnull(TKUKT_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TKUKT t1 on t1.TKUKT_KUKLA = KNA1_KUKLA
where dc.RowIsCurrent = 1 and 
	  dc.ClassificationDescription <> ifnull(TKUKT_VTEXT, 'Not Set');

update dim_customer dc
set dc.CustAccountGroupDescription = ifnull(T077X_TXT30, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join T077X t1 on t1.T077X_KTOKD = KNA1_KTOKD AND T1.T077X_SPRAS = 'E'
where dc.RowIsCurrent = 1 and 
	  dc.CustAccountGroupDescription <> ifnull(T077X_TXT30, 'Not Set');
	  
update dim_customer dc
set dc.RegionDescription = ifnull(T005U_BEZEI, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join T005U t1 on t1.T005U_LAND1 = KNA1_LAND1 AND T005U_BLAND = KNA1_REGIO
where dc.RowIsCurrent = 1 and 
	  dc.RegionDescription <> ifnull(T005U_BEZEI, 'Not Set');
	  	  
update dim_customer dc
set dc.ChannelDescription = ifnull(TVK1T_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TVK1T t1 on t1.TVK1T_KATR1 = KNA1_KATR1
where dc.RowIsCurrent = 1 and 
	  dc.ChannelDescription <> ifnull(TVK1T_VTEXT, 'Not Set');

update dim_customer dc
set dc.SubChannelDescription = ifnull(TVK2T_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TVK2T t1 on t1.TVK2T_KATR2 = KNA1_KATR2
where dc.RowIsCurrent = 1 and 
	  dc.SubChannelDescription <> ifnull(TVK2T_VTEXT, 'Not Set');

update dim_customer dc
set dc.ConditionGroupDescription2 = ifnull(TVKGGT_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TVKGGT t1 on t1.TVKGGT_KDKGR = kn.KNA1_KDKG2
where dc.RowIsCurrent = 1 and 
	  dc.ConditionGroupDescription2 <> ifnull(TVKGGT_VTEXT, 'Not Set');
	  
update dim_customer dc
set dc.IndustryCode1Description = ifnull(TBRCT_VTEXT, 'Not Set')
FROM dim_customer dc
		inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
		inner join t005t t on kn.KNA1_LAND1 = t.T005T_LAND1
		left join TBRCT t1 on t1.TBRCT_BRACO = kn.KNA1_BRAN1
where dc.RowIsCurrent = 1 and 
	  dc.IndustryCode1Description <> ifnull(TBRCT_VTEXT, 'Not Set');

	  /* For Merck */
UPDATE dim_customer c
SET c.region_merck = m.region_merck
    ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM country_merckregion_mapping m, dim_customer c
WHERE upper(rtrim(m.country)) = upper(rtrim(c.countryname))
AND ifnull(c.region_merck,'xx') <> ifnull(m.region_merck,'yy');

UPDATE dim_customer c
SET c.region_merck = 'Not Set'
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE c.region_merck IS NULL;

 update dim_customer dc
 set addressnumber = ifnull(KNA1_ADRNR,'Not Set') 
 FROM dim_customer dc
 inner join kna1 kn on dc.CustomerNumber = kn.kna1_kunnr
 left join t005t on KNA1_LAND1 = T005T_LAND1
WHERE  addressnumber <> ifnull(KNA1_ADRNR,'Not Set') ;

/*26 May 2016 Georgiana EA-Changes adding update statement for paymethod column according to BI-3020*/
update dim_customer dc
 set dc.paymethod = ifnull(k.knb1_ZWELS, 'Not Set')
 from (select kna1_kunnr,max(knb1_ZWELS) as knb1_ZWELS from kna1_knb1
group by kna1_kunnr) k, dim_customer dc
 where dc.CustomerNumber = k.kna1_kunnr
 and dc.paymethod <> ifnull(k.knb1_ZWELS, 'Not Set');

/*26 May 2016 End Of changes*/

/* 5 Jul 2016 Madalina BI-3360 */
update dim_customer c
set legalstatusdescription = ifnull(TVGFT_VTEXT, 'Not Set'),
	dw_update_date = current_timestamp
from TVGFT t, dim_customer c
where legalstatuscode = ifnull(TVGFT_GFORM, 'Not Set')
	and legalstatusdescription <> ifnull(TVGFT_VTEXT, 'Not Set');
	
/* 31 Mar 2017 Andrei - add KNB1-CESSION_KZ - BI-5895 */
UPDATE dim_Customer dc 
SET dc.AcctsReceivablePledgingIndicator = ifnull(k.knb1_CESSION_KZ, 'Not Set')
from dim_Customer dc INNER JOIN KNA1_KNB1 k
on dc.CustomerNumber = k.kna1_kunnr
WHERE
dc.AcctsReceivablePledgingIndicator <> ifnull(k.knb1_CESSION_KZ, 'Not Set');

