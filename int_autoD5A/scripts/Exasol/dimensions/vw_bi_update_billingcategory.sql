UPDATE    dim_billingcategory bc
	SET bc.Description = ifnull(dt.DD07T_DDTEXT, 'Not Set'),
		bc.dw_update_date = current_timestamp
       FROM
          dim_billingcategory bc, DD07T dt		
 WHERE bc.Category = dt.DD07T_DOMVALUE 
 AND bc.RowIsCurrent = 1
 AND dt.DD07T_DOMNAME = 'FKTYP' 
 AND dt.DD07T_DOMNAME IS NOT NULL;
 
