UPDATE    dim_inspectionsamplestatus s
   SET s.description = t.DD07T_DDTEXT,
			s.dw_update_date = current_timestamp
	FROM
         dim_inspectionsamplestatus s, DD07T t
 WHERE s.RowIsCurrent = 1
       AND    t.DD07T_DOMNAME = 'LVS_STIKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND s.statuscode = t.DD07T_DOMVALUE;
