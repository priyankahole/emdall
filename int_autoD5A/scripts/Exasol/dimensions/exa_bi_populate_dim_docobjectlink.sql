insert into Dim_DocObjectLink (dim_DocObjectLinkid, doctype, docno, docversion, docpart, linksapobj, counterkey, docmngmtobjkey, docdescription, dw_insert_date, dw_update_date)
select 1,'Not Set','Not Set','Not Set','Not Set','Not Set',1,'Not Set','Not Set',current_timestamp,current_timestamp
FROM (SELECT 1) a
where not exists (select 1 from Dim_DocObjectLink where Dim_DocObjectLinkid = 1);
               
UPDATE    dim_DocObjectLink dim
  SET dim.docdescription = ifnull(ddt.drat_dktxt, 'Not Set'),
    dim.dw_update_date = current_timestamp
       FROM
          dim_DocObjectLink dim, drad_drat ddt    
 WHERE dim.doctype = ddt.DRAD_DOKAR
 AND dim.docno = ddt.DRAD_DOKNR
 and dim.docversion = ddt.DRAD_DOKVR
 and dim.docpart = ddt.DRAD_DOKTL
 and dim.linksapobj = ddt.DRAD_DOKOB
 and dim.counterkey = ddt.DRAD_OBZAE
 and dim.docmngmtobjkey = ddt.DRAD_OBJKY
 AND dim.docdescription <> ifnull(ddt.drat_dktxt, 'Not Set');
 
delete from number_fountain m where m.table_name = 'Dim_DocObjectLink';

insert into number_fountain
select  'Dim_DocObjectLink',
  ifnull(max(d.Dim_DocObjectLinkId), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_DocObjectLink d
where d.Dim_DocObjectLinkId <> 1;
               
INSERT INTO dim_DocObjectLink(Dim_DocObjectLinkId
                , doctype
                              , docno
                              , docversion
                              , docpart
                              , linksapobj
                              , counterkey
                              , docmngmtobjkey
                              , docdescription
                              , dw_insert_date
                              , dw_update_date)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_DocObjectLink') 
          + row_number() over(order by '') ,
       ifnull(DRAD_DOKAR, 'Not Set')
      ,ifnull(DRAD_DOKNR, 'Not Set')
      ,ifnull(DRAD_DOKVR, 'Not Set')
      ,ifnull(DRAD_DOKTL, 'Not Set')
      ,ifnull(DRAD_DOKOB, 'Not Set')
      ,ifnull(DRAD_OBZAE, 1)
      ,ifnull(DRAD_OBJKY, 'Not Set')
      ,ifnull(DRAT_DKTXT, 'Not Set')
      ,current_timestamp
      ,current_timestamp
       FROM drad_drat ddt
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM Dim_DocObjectLink dim
                    WHERE dim.doctype = ddt.DRAD_DOKAR
                        AND dim.docno = ddt.DRAD_DOKNR
                        and dim.docversion = ddt.DRAD_DOKVR
                        and dim.docpart = ddt.DRAD_DOKTL
                        and dim.linksapobj = ddt.DRAD_DOKOB
                        and dim.counterkey = ddt.DRAD_OBZAE
                        and dim.docmngmtobjkey = ddt.DRAD_OBJKY)
                ;
   
delete from number_fountain m where m.table_name = 'Dim_DocObjectLink';
