
UPDATE  dim_accountingtransferstatus ats
SET 	ats.AccountingTransferStatusName = ifnull(DD07T_DDTEXT, 'Not Set'),
		ats.dw_update_date = current_timestamp
		FROM 	dim_accountingtransferstatus ats, DD07T d
WHERE 	ats.AccountingTransferStatusCode = d.DD07T_DOMVALUE
		AND d.DD07T_DOMNAME = 'RFBSK'
		AND d.DD07T_DOMVALUE IS NOT NULL
		AND ats.RowIsCurrent = 1;
				
