insert into dim_paymentreason
(dim_paymentreasonid, companycode, paymentreasoncode, shorttext, description, 
rowstartdate, rowenddate, rowiscurrent, rowchangereason) 
SELECT 1,'Not Set','Not Set','Not Set','Not Set',
current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_paymentreason where dim_paymentreasonid = 1);

UPDATE    dim_paymentreason pr
   SET pr.ShortText = t.T053S_TXT20, pr.Description = t.T053S_TXT40,
		pr.dw_update_date = current_timestamp
FROM T053S t,  dim_paymentreason pr
 WHERE pr.RowIsCurrent = 1
 AND t.T053S_BUKRS = pr.CompanyCode
 AND pr.PaymentReasonCode = t.T053S_RSTGR;
 
delete from number_fountain m where m.table_name = 'dim_paymentreason';

insert into number_fountain
select 	'dim_paymentreason',
	ifnull(max(d.dim_paymentreasonid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_paymentreason d
where d.dim_paymentreasonid <> 1; 

INSERT INTO dim_paymentreason(Dim_PaymentReasonId,
								CompanyCode,
                              PaymentReasonCode,
                              ShortText,
                              Description,
                              RowStartDate,
                              RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_paymentreason') 
          + row_number() over(order by '') ,
		  T053S_BUKRS,
          T053S_RSTGR,
          T053S_TXT20,
          T053S_TXT40,
          current_timestamp,
          1
     FROM T053S t
    WHERE t.T053S_SPRAS = 'E'
          AND NOT EXISTS
                     (SELECT 1
                        FROM dim_paymentreason pr
                       WHERE     pr.CompanyCode = t.T053S_BUKRS
                             AND pr.PaymentReasonCode = t.T053S_RSTGR
                             AND pr.RowIsCurrent = 1);
					 
delete from number_fountain m where m.table_name = 'dim_paymentreason';	





