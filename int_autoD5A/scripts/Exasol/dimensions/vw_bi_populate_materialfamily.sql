insert into dim_materialfamily
(       dim_materialfamilyid,
        materialfamily,
		materialfamilydescription,
        productlinerollup,
        productline,
        productlinedesc,
		producttype,
		producttypedesc,
		productclass,
		productclassdesc,
        brand,
        branddescription,
		afstargetgroup,
		afstargetgroupdescription,
		strategiccategory,
		strategiccategorydescription,
		subbrand,
		merchandisecollection,
        rowstartdate,
        rowiscurrent
)
SELECT 	1,
        'Not Set',
		'Not Set',
        'Not Set',
        'Not Set',
        'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
        'Not Set',
        'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
		'Not Set',
        current_date,
        1
FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_materialfamily
               WHERE dim_materialfamilyid = 1);

/* Insert distinct Material Family Code From Dim Part */   
delete from number_fountain m where m.table_name = 'dim_materialfamily';

insert into number_fountain
select 	'dim_materialfamily',
	ifnull(max(d.dim_materialfamilyid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_materialfamily d
where d.dim_materialfamilyid <> 1;
	
insert into dim_materialfamily
(       dim_materialfamilyid,
        materialfamily,
		materialfamilydescription,
        productlinerollup,
        productline,
        productlinedesc,
		producttype,
		producttypedesc,
		productclass,
		productclassdesc,
        brand,
        branddescription,
		afstargetgroup,
		afstargetgroupdescription,
		strategiccategory,
		strategiccategorydescription,
		subbrand,
		merchandisecollection,
        rowstartdate,
        rowiscurrent
)	
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_materialfamily')
          + row_number() over(order by '') as dim_materialfamilyid,
mf.*
FROM 
(select distinct materialfamily_columbia as materialfamily, 
		'Not Set' as materialfamilydescription,
        'Not Set' as productlinerollup,
        'Not Set' as productline,
        'Not Set' as productlinedesc,
		'Not Set' as producttype,
		'Not Set' as producttypedesc,
		'Not Set' as productclass,
		'Not Set' as productclassdesc,
        'Not Set' as brand,
        'Not Set' as branddescription,
		'Not Set' as afstargetgroup,
		'Not Set' as afstargetgroupdescription,
		'Not Set' as strategiccategory,
		'Not Set' as strategiccategorydescription,
		'Not Set' as subbrand,
		'Not Set' as merchandisecollection,
        current_date as rowstartdate,
        1 as rowiscurrent
from dim_part p
where p.dim_partid <> 1
and p.materialfamily_columbia <> 'Not Se' 
and p.materialfamily_columbia <> 'Not Set' 
) mf
WHERE NOT EXISTS (SELECT 1
                        FROM dim_materialfamily
                       WHERE materialfamily = mf.materialfamily);

delete from number_fountain m where m.table_name = 'dim_materialfamily';







UPDATE    dim_materialfamily d

SET 	d.materialfamilydescription = mf.materialfamilydescription,
        d.productlinerollup = mf.productlinerollup,
        d.productline = mf.productline,
        d.productlinedesc = mf.productlinedesc,
		d.producttype = mf.producttype,
		d.producttypedesc = mf.producttypedesc,
		d.productclass = mf.productclass,
		d.productclassdesc = mf.productclassdesc,
        d.brand = mf.brand,
        d.branddescription = mf.branddescription,
		d.afstargetgroup = mf.afstargetgroup,
		d.afstargetgroupdescription = mf.afstargetgroupdescription,
		d.strategiccategory = mf.strategiccategory,
		d.strategiccategorydescription = mf.strategiccategorydescription,
		d.subbrand = mf.subbrand,
		d.merchandisecollection = mf.merchandisecollection,
		d.dw_update_date = current_timestamp
from  dim_materialfamily d,
(
select distinct 
p.materialfamily_columbia as materialfamily, 
p.materialdescription_columbia as materialfamilydescription,
CASE WHEN ph.level1desc = 'Not Set' THEN 'Not Set'
     WHEN ph.level1desc = 'Footwear' THEN 'Footwear' 
     ELSE 'Apparel' 
END as productlinerollup,
ph.level1code as productline, 
ph.level1desc as productlinedesc, 
ph.level2code as producttype, 
ph.level2desc as producttypedesc,
ph.level3code as productclass, 
ph.level3desc as productclassdesc, 
p.division as brand, 
p.divisiondescription as branddescription,
p.afstargetgroup, 
p.afstargetgroupdescription,
p.strategiccategory_columbia as strategiccategory, 
p.strategiccategorydescription_columbia as strategiccategorydescription,
ps.materialgroup4 as subbrand, 
ps.materialgroup5 as merchandisecollection
from dim_part p, dim_producthierarchy ph, dim_partsales ps
where p.producthierarchy = ph.producthierarchy
and p.partnumber = ps.partnumber
and p.dim_partid <> 1
and p.materialfamily_columbia <> 'Not Se' 
and p.materialfamily_columbia <> 'Not Set'
) mf
WHERE d.materialfamily = mf.materialfamily AND d.RowIsCurrent = 1;
					