

  insert into dim_tax (dim_taxid,
                          LanguageKey,
                          Taxcode,
						  TEXT1)
select 1, 'Not Set', 'Not Set','Not Set'
from (select 1) a
where not exists ( select 'x' from dim_tax where dim_taxid = 1); 

update dim_tax dt
set  dt.TEXT1 =  ifnull(t.T007S_TEXT1,'Not Set'),
    dt.dw_update_date = current_timestamp
from T007S t, dim_tax dt
where     dt.LanguageKey = t.T007S_KALSM
      and dt.Taxcode     = t.T007S_MWSKZ
      and ifnull(dt.TEXT1,'X') <> ifnull(t.T007S_TEXT1, 'Y');

delete from number_fountain m where m.table_name = 'dim_tax';
insert into number_fountain
select 'dim_tax',
 ifnull(max(d.dim_taxid ), 
              ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_tax d
where d.dim_taxid <> 1;

insert into dim_tax (dim_taxid,
                          LanguageKey,
                          Taxcode,
						  TEXT1,
                          dw_insert_date)
select (select ifnull(m.max_id, 1) 
        from number_fountain m 
        where m.table_name = 'dim_tax') + row_number() over(order by '') AS dim_taxid,
       ifnull(t.T007S_KALSM, 'Not Set') AS LanguageKey,
       ifnull(t.T007S_MWSKZ, 'Not Set') AS Taxcode,
	   ifnull(t.T007S_TEXT1, 'Not Set') AS TEXT1,
       current_timestamp
from T007S t
where not exists (select 'x'
             from dim_tax dt
             where  dt.LanguageKey = t.T007S_KALSM
			    and dt.Taxcode     = t.T007S_MWSKZ);
			
      