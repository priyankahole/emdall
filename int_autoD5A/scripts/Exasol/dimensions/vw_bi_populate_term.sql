
/* Using max(though its a string). No other way to handle duplicates in T052U_ZTERM/T052U_TEXT1 */
DROP TABLE IF EXISTS tmp_distinct_t052u;
CREATE TABLE tmp_distinct_t052u
AS
SELECT u.T052U_ZTERM,MAX(T052U_TEXT1) T052U_TEXT1
FROM t052u u
GROUP BY u.T052U_ZTERM;


UPDATE    dim_term dt
   SET Name = ifnull(T052U_TEXT1, 'Not Set'),
                        dt.dw_update_date = current_timestamp
FROM tmp_distinct_t052u u, dim_term dt
 WHERE dt.RowIsCurrent = 1
 AND u.T052U_ZTERM = dt.Termcode ;


INSERT INTO dim_term(dim_termId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_term
               WHERE dim_termId = 1);

delete from number_fountain m where m.table_name = 'dim_term';

insert into number_fountain
select 	'dim_term',
	ifnull(max(d.Dim_Termid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_term d
where d.Dim_Termid <> 1;	

INSERT INTO dim_term(Dim_Termid,
		     Name,
                     TermCode,
                     RowStartDate,
                     RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_term') 
          + row_number() over(order by ''),
			 ifnull(T052U_TEXT1, 'Not Set'),
                   T052U_ZTERM,
                   current_timestamp,
                   1
     FROM tmp_distinct_t052u
    WHERE T052U_TEXT1 IS NOT NULL
          AND NOT EXISTS
                 (SELECT 1
                    FROM dim_term
                   WHERE termcode = T052U_ZTERM);
DROP TABLE IF EXISTS tmp_distinct_t052u;
