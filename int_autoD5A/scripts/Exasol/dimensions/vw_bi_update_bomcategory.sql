
UPDATE    dim_bomcategory bc
SET 	bc.Description = t.DD07T_DDTEXT,
		bc.dw_update_date = current_timestamp
	FROM	dim_bomcategory bc, DD07T t
WHERE 	bc.RowIsCurrent = 1
		AND  t.DD07T_DOMNAME = 'STLTY'
        AND t.DD07T_DOMVALUE IS NOT NULL
        AND bc.category = t.DD07T_DOMVALUE;
