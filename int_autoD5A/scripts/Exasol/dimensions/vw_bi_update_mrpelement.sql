UPDATE    dim_MRPElement mrpe

   SET mrpe.Description = ifnull(DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T t, dim_MRPElement mrpe
 WHERE mrpe.RowIsCurrent = 1
     AND    t.DD07T_DOMNAME = 'DELKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND mrpe.MRPElement = t.DD07T_DOMVALUE
;