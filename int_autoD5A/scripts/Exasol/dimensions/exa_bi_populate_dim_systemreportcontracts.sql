/* ***********************************************************************************************************************************
   Script         - exa_bi_populate_dim_systemreportcontracts.sql
   Author         - Octavian Stepan
   Created ON     - Jun 2018
   Description    - Populate dim_systemreportcontracts as requested by Merck Ah - APP-9861
   ************************************************************************************************************************************/

delete from number_fountain m where m.table_name = 'dim_systemreportcontracts';

insert into number_fountain				   
select 	'dim_systemreportcontracts',
	ifnull(max(d.dim_systemreportcontractsid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_systemreportcontracts d
where d.dim_systemreportcontractsid <> 1;

INSERT INTO dim_systemreportcontracts(
dim_systemreportcontractsid,
Login,
Contractmanager,
Label,
Agreementno,
Title,
Counterparty,
Type,
Expirationdate,
Agreementhasdocument,
Status)
   SELECT (
select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_systemreportcontracts')   + row_number() over(order by ''),
t.Login,
t.Contractmanager,
t.Label,
t.Agreementno,
t.Title,
t.Counterparty,
t.Type,
t.Expirationdate,
t.Agreementhasdocument,
t.Status
FROM stg_systemreportcontracts t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_systemreportcontracts s
                  WHERE s.Login = t.Login
                    AND ifnull(s.Contractmanager,'x')= ifnull (t.Contractmanager,'x')) ;
