UPDATE    dim_consumptiontype ct

   SET ct.Description = ifnull(DD07T_DDTEXT, 'Not Set')
          FROM
          DD07T dt,  dim_consumptiontype ct
 WHERE ct.RowIsCurrent = 1
 AND   DD07T_DOMNAME = 'KZVBR'
          AND DD07T_DOMVALUE IS NOT NULL
          AND ct.ConsumptionCode = dt.DD07T_DOMVALUE
 ;