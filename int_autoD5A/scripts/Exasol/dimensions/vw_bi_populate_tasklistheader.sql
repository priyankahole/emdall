/* Octavian: Every Angle Transition Addons */
insert into dim_tasklistheader (dim_tasklistheaderid,
                          tasklisttype,
                          keytasklistgr,
     groupcounter,
     internalcounter)
select 1, 'Not Set', 'Not Set', 'Not Set', 1
from (select 1) a
where not exists ( select 'x' from dim_tasklistheader where dim_tasklistheaderid = 1);

delete from number_fountain m where m.table_name = 'dim_tasklistheader';
insert into number_fountain
select 'dim_tasklistheader',
 ifnull(max(d.dim_tasklistheaderid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_tasklistheader d
where d.dim_tasklistheaderid <> 1;

insert into dim_tasklistheader (dim_tasklistheaderid,
                          tasklisttype,
                          keytasklistgr,
     					  groupcounter,
     					  internalcounter,
     					  dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_tasklistheader') + row_number() over(order by '') AS dim_tasklistheaderid,
        ifnull(ds.PLKO_PLNTY, 'Not Set') AS tasklisttype,
        ifnull(ds.PLKO_PLNNR, 'Not Set') AS keytasklistgr,
 ifnull(ds.PLKO_PLNAL, 'Not Set') AS groupcounter,
 ifnull(ds.PLKO_ZAEHL, 0) AS internalcounter,
       current_timestamp
from PLKO ds
 where not exists (select 'x' from dim_tasklistheader dc where dc.tasklisttype = ifnull(ds.PLKO_PLNTY, 'Not Set') 
 														   AND dc.keytasklistgr = ifnull(ds.PLKO_PLNNR, 'Not Set')
 														   AND dc.groupcounter = ifnull(ds.PLKO_PLNAL, 'Not Set')
 														   AND dc.internalcounter = ifnull(ds.PLKO_ZAEHL, 0)
 														   );
														   

update dim_tasklistheader dc

set delflag = ifnull(PLKO_DELKZ,'Not Set'),
    dc.dw_update_date = current_timestamp
from PLKO ds ,dim_tasklistheader dc
where dc.tasklisttype = ifnull(ds.PLKO_PLNTY, 'Not Set') 
AND dc.keytasklistgr = ifnull(ds.PLKO_PLNNR, 'Not Set')
AND dc.groupcounter = ifnull(ds.PLKO_PLNAL, 'Not Set')
AND dc.internalcounter = ifnull(ds.PLKO_ZAEHL,0)
AND delflag <> ifnull(PLKO_DELKZ,'Not Set');

update dim_tasklistheader dc
set statustasklisthdr = ifnull(PLKO_STATU,'Not Set'),
    dc.dw_update_date = current_timestamp
from PLKO ds ,dim_tasklistheader dc
where dc.tasklisttype = ifnull(ds.PLKO_PLNTY, 'Not Set') 
AND dc.keytasklistgr = ifnull(ds.PLKO_PLNNR, 'Not Set')
AND dc.groupcounter = ifnull(ds.PLKO_PLNAL, 'Not Set')
AND dc.internalcounter = ifnull(ds.PLKO_ZAEHL,0)
AND statustasklisthdr <> ifnull(PLKO_STATU,'Not Set');
