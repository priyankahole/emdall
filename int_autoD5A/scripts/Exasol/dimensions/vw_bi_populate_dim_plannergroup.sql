/*##################################################################################################################
	Script         : vw_bi_populate_dim_plannergroup                                                                                                                                              
	Created By     : Suchithra                                                                                                        
	Created On     : 19 Apr 2016  
	Description    : Script to populated Planner Group dimension for Plant Maintanence 
			 Grain : MaintPlanningPlant(T024I_IWERK),CustomerServiceGroup(T024I_INGRP)

	Change History                                                                                                                                   
	Date            By        Version           Desc                                                                 

####################################################################################################################*/ 


INSERT INTO dim_plannergroup(
	dim_plannergroupid,
	MaintPlanningPlant,
	CustomerServiceGroup,
	MaintPlannerGroup,
	TelephoneNumber,
	OrderType,
	RowStartDate,
	RowIsCurrent
)
  SELECT    
  	1,
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
    	current_timestamp,
    	1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_plannergroup WHERE dim_plannergroupId = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_plannergroup';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_plannergroup',
                ifnull(max(d.dim_plannergroupId),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_plannergroup d
where d.dim_plannergroupId <> 1;


INSERT INTO dim_plannergroup(
	dim_plannergroupid,
	MaintPlanningPlant,
	CustomerServiceGroup,
	MaintPlannerGroup,
	TelephoneNumber,
	OrderType,
	RowStartDate,
	RowIsCurrent
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_plannergroup') + row_number() over(order by '')  as dim_plannergroupid,
	ifnull(T024I_IWERK,'Not Set') as MaintPlanningPlant ,
	ifnull(T024I_INGRP,'Not Set') as CustomerServiceGroup,
	ifnull(T024I_INNAM,'Not Set') as MaintPlannerGroup, 
	ifnull(T024I_INTEL,'Not Set') as TelephoneNumber,
	ifnull(T024I_AUART_WP,'Not Set') as OrderType,
    	current_timestamp,
    	1
FROM T024I
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_plannergroup
        WHERE MaintPlanningPlant = ifnull(T024I_IWERK,'Not Set')
        AND CustomerServiceGroup = ifnull(T024I_INGRP,'Not Set')
     );

update dim_plannergroup
set 	MaintPlannerGroup = ifnull(T024I_INNAM,'Not Set')
FROM dim_plannergroup, T024I
WHERE MaintPlanningPlant = ifnull(T024I_IWERK,'Not Set')
AND CustomerServiceGroup = ifnull(T024I_INGRP,'Not Set')
and MaintPlannerGroup <> ifnull(T024I_INNAM,'Not Set');

update dim_plannergroup
set 	TelephoneNumber = ifnull(T024I_INTEL,'Not Set')
FROM dim_plannergroup, T024I
WHERE MaintPlanningPlant = ifnull(T024I_IWERK,'Not Set')
AND CustomerServiceGroup = ifnull(T024I_INGRP,'Not Set')
and TelephoneNumber <> ifnull(T024I_INTEL,'Not Set');

update dim_plannergroup
set 	OrderType = ifnull(T024I_AUART_WP,'Not Set')
FROM dim_plannergroup, T024I
WHERE MaintPlanningPlant = ifnull(T024I_IWERK,'Not Set')
AND CustomerServiceGroup = ifnull(T024I_INGRP,'Not Set')
and OrderType <> ifnull(T024I_AUART_WP,'Not Set');      
	
DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_plannergroup';        

