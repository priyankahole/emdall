UPDATE    dim_inspectionlotorigin o

   SET o.InspectionLotOriginName = t.TQ31T_HERKTXT
       FROM
          TQ31T t,  dim_inspectionlotorigin o
 WHERE o.RowIsCurrent = 1
 AND o.InspectionLotOriginCode = t.TQ31T_HERKUNFT;

