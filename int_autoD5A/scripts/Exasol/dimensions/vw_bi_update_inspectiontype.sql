
UPDATE    dim_inspectiontype it

   SET it.InspectionTypeName = t.TQ30T_LTXA1,
       it.InspectionTypeText = t.TQ30T_KURZTEXT
          FROM
          TQ30T t, dim_inspectiontype it
 WHERE it.RowIsCurrent = 1
     AND it.InspectionTypeCode = t.TQ30T_ART;
