

UPDATE dim_accountassignmentgroup aag 
SET aag.Description = ifnull(t.TVKTT_VTEXT, 'Not Set'),
	aag.dw_update_date = current_timestamp
	from dim_accountassignmentgroup aag, TVKTT t
WHERE aag.AccountAssignmentGroup = t.TVKTT_KTGRD 
AND aag.RowIsCurrent = 1;
			   
