
UPDATE dim_CustomerConditionGroups a 
   SET a.Description = ifnull(TVKGGT_VTEXT, 'Not Set')
FROM TVKGGT, dim_CustomerConditionGroups a 
 WHERE a.CustomerCondGrp = TVKGGT_KDKGR AND RowIsCurrent = 1;
