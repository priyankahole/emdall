INSERT INTO dim_mainactivitytype(dim_mainactivitytypeid, RowIsCurrent,mainactivitytype,mainactivitytypedesc,
rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_mainactivitytype
               WHERE dim_mainactivitytypeid = 1);

delete from number_fountain m where m.table_name = 'dim_mainactivitytype';
   
insert into number_fountain
select 	'dim_mainactivitytype',
	ifnull(max(d.dim_mainactivitytypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_mainactivitytype d
where d.dim_mainactivitytypeid <> 1; 

INSERT INTO dim_mainactivitytype(dim_mainactivitytypeid,
                                     mainactivitytype,
									 mainactivitytypedesc,
                                     RowStartDate,
                                     RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_mainactivitytype') 
          + row_number() over(order by ''),
			 T353I_T_ILART,
          ifnull(T353I_T_ILATX,'Not Set'),
          current_timestamp,
          1
     FROM T353I_T t1
    WHERE  NOT EXISTS
                (SELECT 1
                   FROM dim_mainactivitytype s
                  WHERE     s.mainactivitytype = t1.T353I_T_ILART
                        AND s.RowIsCurrent = 1)
;


UPDATE dim_mainactivitytype s
   SET s.mainactivitytypedesc = ifnull(T353I_T_ILATX,'Not Set'),
			s.dw_update_date = current_timestamp
FROM T353I_T t1,  dim_mainactivitytype s
 WHERE s.RowIsCurrent = 1
 AND  s.mainactivitytype = t1.T353I_T_ILART
 AND s.mainactivitytypedesc <> ifnull(T353I_T_ILATX,'Not Set');