UPDATE    dim_objecttype ot

   SET ot.Description = ifnull(DD07T_DDTEXT, 'Not Set')
          FROM
          DD07T t, dim_objecttype ot
 WHERE ot.RowIsCurrent = 1
        AND   t.DD07T_DOMNAME = 'OBART'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND ot.ObjectType = t.DD07T_DOMVALUE
;