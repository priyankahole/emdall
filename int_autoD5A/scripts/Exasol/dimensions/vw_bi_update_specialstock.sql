UPDATE    dim_SpecialStock st

   SET st.Description = t.T148T_SOTXT
       FROM
          t148t t, dim_SpecialStock st
 WHERE st.RowIsCurrent = 1
  AND st.specialstockindicator = t.T148T_SOBKZ
;
