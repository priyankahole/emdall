delete from dim_autidtrailforpredictiveforecast;
/*DDL changes*/
/* New dimension created*/
/*Take into account only future records*/
insert into  dim_autidtrailforpredictiveforecast 
select distinct 1 as dim_autidtrailforpredictiveforecastid,f.dd_partnumber,dd_plantcode,dd_reportingdate,audit_message,forecast_rank,dd_forecasttype,forecast_comments,prev_rank,last_entry,to_date(dw_insert_date,'YYYY-MM-DD') as changed_date
from fact_fosalesforecast f,fact_fosalesforecast_audittrail a where
f.dim_partid=a.dim_partid
and f.dim_plantid=a.dim_plantid
and f.dim_dateidreporting=a.dim_dateidreporting
and to_date(to_char(f.dd_forecastdate),'YYYYMMDD') >= to_date(dd_reportingdate,'DD MON YYYY')
and case when forecast_rank='CF' then 'P1' else forecast_rank end= concat('P',dd_forecastrank)
and audit_message not in (concat('Forecast ran on ',case when dd_reportingdate='07 Feb 2018' then '13 Feb 2018' else dd_reportingdate end),'Forecast Type: Manual','This Material is one of your Top 25% Materials' );

/* 12 Sep 2018 Georgiana Changes calculating the months between last change date and current date which should be the 22nd of each month for the current run*/
DROP TABLE IF EXISTS tmp_maxrptdate;
CREATE TABLE tmp_maxrptdate
as
SELECT DISTINCT TO_DATE(dd_reportingdate,'DD MON YYYY') dd_reportingdate
from fact_fosalesforecast
where dd_latestreporting='Yes' and dd_holdout='3' ; 

merge into fact_fosalesforecast fosf
using (
select distinct fosf.fact_fosalesforecastid,
round(min(months_between(current_date,changed_date)),0) as ct_monthssincelastchange
 from fact_fosalesforecast fosf,dim_autidtrailforpredictiveforecast atfp,tmp_maxrptdate r
where   atfp.dd_partnumber=fosf.dd_partnumber
and atfp.dd_plantcode=fosf.dd_plantcode 
/*and fosf.dd_reportingdate = atfp.dd_reportingdate*/
and dd_holdout=3
and TO_DATE(fosf.dd_reportingdate,'DD MON YYYY') = r.dd_reportingdate
/*and atfp.last_entry='Y'*/
group by fosf.fact_fosalesforecastid) t
on t.fact_fosalesforecastid=fosf.fact_fosalesforecastid
when matched then update set fosf.ct_monthssincelastchange=t.ct_monthssincelastchange;