insert into dim_orderconfirmation(dim_orderconfirmationid, operation_activity_no, ordernumber)
  SELECT 1, 'Not Set', 'Not Set'
  FROM (select 1) a
  where not exists (select 1 from dim_orderconfirmation where dim_orderconfirmationid = 1);


delete from number_fountain m where m.table_name = 'dim_orderconfirmation';

insert into number_fountain
select  'dim_orderconfirmation',
  ifnull(max(d.dim_orderconfirmationid), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_orderconfirmation d
where d.dim_orderconfirmationid <> 1;



insert into dim_orderconfirmation
  (
    dim_orderconfirmationid,
    ordernumber,
    operation_activity_no,
    completion_number, 
    confirmation_number
  )
  SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_orderconfirmation') 
          + row_number() over(order by '') ,
  AFRU_AUFNR,
  AFRU_VORNR,
  AFRU_RUECK,
  AFRU_RMZHL

  FROM STG_AFRU A
  WHERE NOT EXISTS (SELECT 1
                        FROM dim_orderconfirmation 
                         WHERE completion_number = A.AFRU_RUECK
                         AND confirmation_number = A.AFRU_RMZHL
                       );

delete from number_fountain m where m.table_name = 'dim_orderconfirmation';

  update dim_orderconfirmation d
  SET d.ordernumber = ifnull(A.AFRU_AUFNR, 'Not Set')
  from dim_orderconfirmation d, STG_AFRU A
  WHERE  completion_number = A.AFRU_RUECK
  AND confirmation_number = A.AFRU_RMZHL
  AND d.ordernumber <> ifnull(A.AFRU_AUFNR, 'Not Set');

  update dim_orderconfirmation d
  SET d.operation_activity_no = ifnull(aa.AFVC_VORNR, 'Not Set')
  from dim_orderconfirmation d, STG_AFRU A, AUFK_AFKO_AFVC aa
  WHERE completion_number = A.AFRU_RUECK
  AND confirmation_number = A.AFRU_RMZHL
  and A.AFRU_AUFNR = aa.AUFK_AUFNR
  AND AFRU_AUFPL = AFVC_AUFPL
  and AFRU_APLZL = AFVC_APLZL
  AND d.operation_activity_no <> ifnull(aa.AFVC_VORNR, 'Not Set');

  update dim_orderconfirmation d
  SET d.operation_short_text = ifnull(aa.AFVC_LTXA1, 'Not Set')
  from dim_orderconfirmation d, STG_AFRU A, AUFK_AFKO_AFVC aa
  WHERE completion_number = A.AFRU_RUECK
  AND confirmation_number = A.AFRU_RMZHL
  and A.AFRU_AUFNR = aa.AUFK_AUFNR
  AND AFRU_AUFPL = AFVC_AUFPL
  and AFRU_APLZL = AFVC_APLZL
  AND d.operation_short_text <> ifnull(aa.AFVC_LTXA1, 'Not Set');

------------planned hours
drop table if exists tmp_for_actual_hours;
create table tmp_for_actual_hours as
select distinct t.dim_ordermasterid, t."order", t.operation_activity_no, t.completion_number, t.confirmation_number,ifnull(sum(t.actualhrs), 0) as actualhrs
from
(
SELECT distinct o.dim_ordermasterid, "order", c.operation_activity_no, c.completion_number, c.confirmation_number,
case when stg.AFRU_ISMNE = 'STD' then sum(stg.AFRU_ISMNW) 
when  stg.AFRU_ISMNE = 'MIN' then sum(stg.AFRU_ISMNW/60) END
as actualhrs
  FROM dim_ordermaster o, stg_afru stg, dim_orderconfirmation c
  where ifnull(stg.AFRU_AUFNR, 'Not Set') = o."order"
    and o."order" = c.ordernumber
  and completion_number = stg.AFRU_RUECK
    AND confirmation_number = stg.AFRU_RMZHL
  group by o.dim_ordermasterid, "order", c.operation_activity_no, c.completion_number, c.confirmation_number, AFRU_ISMNE
)t
group by t.dim_ordermasterid, t."order", t.operation_activity_no, t.completion_number, t.confirmation_number;


update dim_orderconfirmation
set actual_hours = actualhrs
from dim_orderconfirmation d, tmp_for_actual_hours t
where t."order" = d.ordernumber
and t.operation_activity_no = d.operation_activity_no
and t.completion_number = d.completion_number
and t.confirmation_number = d.confirmation_number;

drop table if exists tmp_for_actual_hours;

------------planned hours
drop table if exists tmp_for_planned_hours;
create table tmp_for_planned_hours as
select distinct d.dim_ordermasterid, d."order", c.operation_activity_no, c.completion_number, c.confirmation_number,  a.AFVV_ARBEI as plannedhrs --avg(a.AFVV_ARBEI) as plannedhrs
from stg_afru s, AUFK_AFKO_AFVV a, dim_ordermaster d, dim_orderconfirmation c
where  AFRU_AUFPL = AFVV_AUFPL
and AFRU_APLZL = AFVV_APLZL
and d."order" = ifnull(a.AUFK_AUFNR, 'Not Set')
and d."order" = c.ordernumber
and completion_number = ifnull(s.AFRU_RUECK, 'Not Set')
AND confirmation_number = ifnull(s.AFRU_RMZHL, 'Not Set');
--group by d.dim_ordermasterid, d."order", c.operation_activity_no, c.completion_number, c.confirmation_number

update dim_orderconfirmation
set planned_hours = plannedhrs
from dim_orderconfirmation d, tmp_for_planned_hours t
where t."order" = d.ordernumber
and t.operation_activity_no = d.operation_activity_no
and t.completion_number = d.completion_number
and t.confirmation_number = d.confirmation_number
and planned_hours <> plannedhrs;

drop table if exists tmp_for_planned_hours;
