UPDATE    dim_transactioneventtype et
   SET et.TransactionEventTypeDescription = w.T158W_LTEXT,
          FROM
          t158w w, dim_transactioneventtype et
 WHERE et.RowIsCurrent = 1
 AND et.TransactionEventTypeCode = w.T158W_VGART
;