insert into dim_inspectioncatalogcodes
(dim_inspectioncatalogcodesid,
"catalog",
CodeGroup,
Code,
VersionNumber,
ShortText)
select 
1, 'Not Set','Not Set','Not Set', 'Not Set','Not Set'
from (select 1) a
where not exists ( select 'x' from dim_inspectioncatalogcodes where dim_inspectioncatalogcodesid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectioncatalogcodes';
insert into number_fountain
select 'dim_inspectioncatalogcodes',
 ifnull(max(d.dim_inspectioncatalogcodesid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectioncatalogcodes d
where d.dim_inspectioncatalogcodesid <> 1;

insert into dim_inspectioncatalogcodes
(dim_inspectioncatalogcodesid,
"catalog",
CodeGroup,
Code,
VersionNumber,
ShortText,
Datarecordused)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_inspectioncatalogcodes') + row_number() over(order by '') AS dim_inspectioncatalogcodesid,
 ifnull(QPCD_KATALOGART, 'Not Set') as "catalog",
 ifnull(QPCD_CODEGRUPPE,'Not Set') as CodeGroup,
 ifnull(QPCD_CODE,'Not Set') as Code,
 ifnull(QPCD_VERSION, 'Not Set') as VersionNumber,
 ifnull(QPCT_KURZTEXT, 'Not Set') as ShortText,
 ifnull(QPCD_VERWENDUNG,'Not Set') as Datarecordused
FROM 
QPCD_QPCT q
where not exists ( select 'x' from  dim_inspectioncatalogcodes
                    where "catalog" = ifnull(QPCD_KATALOGART, 'Not Set')
						  AND CodeGroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
                          AND Code = ifnull(QPCD_CODE,'Not Set')
                          AND VersionNumber = ifnull(QPCD_VERSION, 'Not Set') );
						  
/*Octavian S - 6-SEP-2018 - Fixed Inspection codes issue*/

Update dim_inspectioncatalogcodes
SET ShortText = ifnull(QPCT_KURZTEXT, 'Not Set')
From (SELECT DISTINCT QPCT_KURZTEXT, QPCD_KATALOGART,QPCD_CODEGRUPPE,QPCD_CODE ,QPCD_VERSION from QPCD_QPCT) q,dim_inspectioncatalogcodes
where "catalog" = ifnull(QPCD_KATALOGART, 'Not Set')
	AND CodeGroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
    AND Code = ifnull(QPCD_CODE,'Not Set')
    AND VersionNumber = ifnull(QPCD_VERSION, 'Not Set')
	AND ShortText <> ifnull(QPCT_KURZTEXT, 'Not Set');
/*	
Update dim_inspectioncatalogcodes
SET Datarecordused = ifnull(QPCD_VERWENDUNG,'Not Set')
From QPCD_QPCT q,dim_inspectioncatalogcodes
where "catalog" = ifnull(QPCD_KATALOGART, 'Not Set')
	AND CodeGroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
    AND Code = ifnull(QPCD_CODE,'Not Set')
    AND VersionNumber = ifnull(QPCD_VERSION, 'Not Set')
	AND Datarecordused <> ifnull(QPCD_VERWENDUNG,'Not Set')
*/

merge into dim_inspectioncatalogcodes d
using
(
	select distinct dim_inspectioncatalogcodesid, QPCD_VERWENDUNG
	From QPCD_QPCT q,dim_inspectioncatalogcodes
	where "catalog" = ifnull(QPCD_KATALOGART, 'Not Set')
	AND CodeGroup = ifnull(QPCD_CODEGRUPPE,'Not Set')
	AND Code = ifnull(QPCD_CODE,'Not Set')
	AND VersionNumber = ifnull(QPCD_VERSION, 'Not Set')
	AND Datarecordused <> ifnull(QPCD_VERWENDUNG,'Not Set')
)x
on d.dim_inspectioncatalogcodesid = x.dim_inspectioncatalogcodesid
when matched then update set Datarecordused = ifnull(x.QPCD_VERWENDUNG,'Not Set');



