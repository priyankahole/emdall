UPDATE    dim_supplyarea a
   SET a.Description = ifnull(PVKT_PVBTX, 'Not Set')
       FROM
          PVKT p, dim_supplyarea a
 WHERE a.RowIsCurrent = 1
       AND a.SupplyArea = p.PVKT_PRVBE AND a.Plant = p.PVKT_WERKS 
;