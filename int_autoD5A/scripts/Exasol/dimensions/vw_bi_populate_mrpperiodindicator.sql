insert into dim_periodindicator 
(dim_periodindicatorid, description, periodindicator, rowstartdate, rowenddate, rowiscurrent, rowchangereason) 
SELECT 1,'Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_periodindicator where dim_periodindicatorid = 1);


UPDATE    dim_periodindicator p
   SET p.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			p.dw_update_date = current_timestamp 
FROM DD07T t,  dim_periodindicator p
 WHERE p.RowIsCurrent = 1
     AND   t.DD07T_DOMNAME = 'PERKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND p.PeriodIndicator = t.DD07T_DOMVALUE;

delete from number_fountain m where m.table_name = 'dim_periodindicator';

insert into number_fountain
select 	'dim_periodindicator',
	ifnull(max(d.dim_periodindicatorid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_periodindicator d
where d.dim_periodindicatorid <> 1;


INSERT INTO dim_periodindicator(dim_periodindicatorid,
                                Description,
                                PeriodIndicator,
                                RowStartDate,
                                RowIsCurrent)
   SELECT ((select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_periodindicator')) 
          + row_number() over(order by '') ,
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'PERKZ' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_periodindicator
                    WHERE PeriodIndicator = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'PERKZ')
   ORDER BY 2;

delete from number_fountain m where m.table_name = 'dim_periodindicator';