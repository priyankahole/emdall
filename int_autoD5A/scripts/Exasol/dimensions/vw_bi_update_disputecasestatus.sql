
UPDATE dim_DisputeCaseStatus s

SET s.Description = ifnull(t.SCMGSTATPROFST_STAT_ORDNO_DESCR, 'Not Set')FROM
SCMGSTATPROFST t, dim_DisputeCaseStatus s
WHERE s.RowIsCurrent = 1
AND s.DisputeCaseStatus=t.SCMGSTATPROFST_STAT_ORDERNO;