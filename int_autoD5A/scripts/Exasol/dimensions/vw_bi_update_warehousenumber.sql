UPDATE    dim_warehousenumber w
      SET w.WarehouseName = t.T300T_LNUMT,
			w.dw_update_date = current_timestamp
			FROM
          dim_warehousenumber w,T300T t
 WHERE w.RowIsCurrent = 1
     AND w.WarehouseCode = t.T300T_LGNUM 
;
