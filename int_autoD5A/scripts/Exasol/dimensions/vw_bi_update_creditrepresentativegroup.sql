UPDATE dim_CreditRepresentativeGroup crg
   SET CreditRepGroupName = ifnull(t.T024B_STEXT, 'Not Set'),
       RMailUserName = ifnull(t.T024B_SMAIL, 'Not Set'),
			dw_update_date = current_timestamp
	FROM dim_CreditRepresentativeGroup crg, T024B t
 WHERE     crg.CreditRepresentativeGroup = t.T024B_SBGRP
       AND crg.CreditControlArea = t.T024B_KKBER
       AND crg.RowIsCurrent = 1;

