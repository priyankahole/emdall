UPDATE    dim_FunctionalArea fa
   SET fa.Description = t.TFKBT_FKBTX,
			fa.dw_update_date = current_timestamp
       FROM
         dim_FunctionalArea fa, TFKBT t
 WHERE fa.RowIsCurrent = 1
 AND fa.FunctionalArea = t.TFKBT_FKBER;

