/*##################################################################################################################
	Script         : vw_bi_populate_dim_equipment                                                                                                                                              
	Created By     : Suchithra                                                                                                        
	Created On     : 09 Mar 2016  
	Description    : Script to populated Equipment dimension for Plant Maintanence 
			Grain : EquipmentNo(equi_equnr),EquipmentValidtoDate(equz_datbi), EquipUsagePeriods(equz_eqlfn)                                                                                                 
	Change History                                                                                                                                   
	Date            By        	Version		Desc                                                                 
	02May2016	Suchithra 	1.1		Added Equipment Model No	
	06May2016	Suchithra	1.2		Added logic to update Eqp Desc in language 'NL' as well
	23May2016	Suchithra	1.3		Added EQUZ_DATAB Valid from date (equipmentvalidfromdate)
	17Jul2017	IonelEne	1.4		Default value for EquipmentValidtoDate set to '9999-12-31'
####################################################################################################################*/ 

DROP TABLE IF EXISTS tmp_dim_equipment;

CREATE TABLE tmp_dim_equipment
LIKE dim_equipment INCLUDING DEFAULTS INCLUDING IDENTITY;

INSERT INTO tmp_dim_equipment(
	dim_equipmentid,
	EquipmentNo,
	EquipmentValidtoDate,
	EquipUsagePeriods,
	TechObjectAuthGroup,
	EquipmentVendor,
	TechnicalObjType,
	EquipmentCategory,
	EquipmentManufacturer,
	EquipmentStartUpDate,
	ManufacturerSerialNo,
	CatalogProfile,
	TechIdentificationNo,
	EquipmentDesc,
	MaterialNo,
	RowStartDate,
	RowIsCurrent
)
  SELECT    
  	1,
	'Not Set',
	'9999-12-31',
	0, 			 
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'0001-01-01',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
	'Not Set',
    	current_timestamp,
    	1
FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM tmp_dim_equipment WHERE dim_equipmentId = 1);  


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'tmp_dim_equipment';

INSERT INTO NUMBER_FOUNTAIN
select  'tmp_dim_equipment',
                ifnull(max(d.dim_equipmentId),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from tmp_dim_equipment d
where d.dim_equipmentId <> 1;


INSERT INTO tmp_dim_equipment(
	dim_equipmentid,
	EquipmentNo,
	EquipmentValidtoDate,
	EquipUsagePeriods,
	TechObjectAuthGroup,
	EquipmentVendor,
	TechnicalObjType,
	EquipmentCategory,
	EquipmentManufacturer,
	EquipmentStartUpDate,
	ManufacturerSerialNo,
	CatalogProfile,
	TechIdentificationNo,
	-- EquipmentDesc,
	MaterialNo,
	RowStartDate,
	RowIsCurrent
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'tmp_dim_equipment') + row_number() over(order by '')  as dim_equipmentid,
	ifnull(equi_equnr,'Not Set') as EquipmentNo,
	ifnull(equz_datbi,'9999-12-31') as EquipmentValidtoDate,
	ifnull(equz_eqlfn,0)		as EquipUsagePeriods,
	ifnull(equi_begru,'Not Set') as TechObjectAuthGroup,
	ifnull(equi_elief,'Not Set') as EquipmentVendor,
	ifnull(equi_eqart,'Not Set') as TechnicalObjType,
	ifnull(equi_eqtyp,'Not Set') as EquipmentCategory,
	ifnull(equi_herst,'Not Set') as EquipmentManufacturer,
	ifnull(equi_inbdt,'0001-01-01') as EquipmentStartUpDate,
	ifnull(equi_serge,'Not Set') as ManufacturerSerialNo,
	ifnull(equz_rbnr,'Not Set') as CatalogProfile,
	ifnull(equz_tidnr,'Not Set') as TechIdentificationNo,
	-- ifnull(EQKT_EQKTX,'Not Set') as EquipmentDesc,
	ifnull(EQUI_MATNR,'Not Set') as MaterialNo,
    	current_timestamp,
    	1
FROM EQUI
LEFT JOIN EQUZ ON equi_equnr = equz_equnr  /* if there are no changes on an equipment there won't be an entry in EQUZ */
WHERE NOT EXISTS
      (SELECT 1
         FROM tmp_dim_equipment
        WHERE EquipmentNo = ifnull(equi_equnr ,'Not Set')
        AND EquipmentValidtoDate = ifnull(equz_datbi,'9999-12-31')
        AND EquipUsagePeriods = ifnull(equz_eqlfn,0));
		
update tmp_dim_equipment a
set a.equipmentvalidfromdate = ifnull(b.EQUZ_DATAB,'0001-01-01')
from EQUZ b,tmp_dim_equipment a
where a.EquipmentNo = ifnull(b.equz_equnr ,'Not Set')
and a.EquipmentValidtoDate = ifnull(b.equz_datbi,'9999-12-31')
and  a.EquipUsagePeriods = ifnull(b.equz_eqlfn,0)
and a.equipmentvalidfromdate <> ifnull(b.EQUZ_DATAB,'0001-01-01');		
	
update tmp_dim_equipment d
set  EquipmentDesc = ifnull(EQKT_EQKTX,'Not Set')
from eqkt e, tmp_dim_equipment d
where  EquipmentNo = EQKT_EQUNR
and EQKT_SPRAS = 'E'
and EquipmentDesc <> ifnull(EQKT_EQKTX,'Not Set'); 	

/*06May2016 - conside N/NL language text as well */
update tmp_dim_equipment d
set  EquipmentDesc = ifnull(EQKT_EQKTX,'Not Set')
from eqkt e,tmp_dim_equipment d
where  EquipmentNo = EQKT_EQUNR
and EQKT_SPRAS <> 'E'
and EquipmentDesc = 'Not Set' /* Could not find E text */
and EquipmentDesc <> ifnull(EQKT_EQKTX,'Not Set');
/* End of Changes 06May2016 */

/* 
02May2016 - Equipment Model No 
Get CUOBJ from INOB for a given Equipment # OBJEK. 
Get AUSP_ATWRT using AUSP_OBJEK = INOB_CUOBJ AND KLART = '002' AND ATINN = '0000001391'
*/
update tmp_dim_equipment d
set d.modelno = ifnull(ausp_atwrt,'Not Set')
from equi,AUSP_model, inob,tmp_dim_equipment d
where d.EquipmentNo = ifnull(equi_equnr,'Not Set') 
and  equi_equnr = inob_objek
and inob_cuobj = ausp_objek
and inob_klart = '002'
and d.EquipmentNo <> ifnull(ausp_atwrt,'Not Set');
/*End of Changes 02May2016 */

/*Alin 22 Nov 2017 - missing updates for the existing fields*/
merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.equi_begru,'Not Set') as equi_begru
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.TechObjectAuthGroup = t.equi_begru
where d.TechObjectAuthGroup <> t.equi_begru;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.equi_elief,'Not Set') as equi_elief
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.EquipmentVendor = t.equi_elief
where d.EquipmentVendor <> t.equi_elief;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.equi_eqart,'Not Set') as equi_eqart
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.TechnicalObjType = t.equi_eqart
where d.TechnicalObjType <> t.equi_eqart;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.equi_eqtyp,'Not Set') as equi_eqtyp
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.EquipmentCategory = t.equi_eqtyp
where d.EquipmentCategory <> t.equi_eqtyp;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.equi_herst,'Not Set') as equi_herst
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.EquipmentManufacturer = t.equi_herst
where d.EquipmentManufacturer <> t.equi_herst;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(equi_inbdt,'0001-01-01') as equi_inbdt
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.EquipmentStartUpDate = t.equi_inbdt
where d.EquipmentStartUpDate <> t.equi_inbdt;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.equi_serge,'Not Set') as equi_serge
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.ManufacturerSerialNo = t.equi_serge
where d.ManufacturerSerialNo <> t.equi_serge;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(z.equz_rbnr,'Not Set') as equz_rbnr
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.CatalogProfile = t.equz_rbnr
where d.CatalogProfile <> t.equz_rbnr;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(z.equz_tidnr,'Not Set') as equz_tidnr
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.TechIdentificationNo = t.equz_tidnr
where d.TechIdentificationNo <> t.equz_tidnr;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.EQUI_MATNR,'Not Set') as EQUI_MATNR
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.MaterialNo = t.EQUI_MATNR
where d.MaterialNo <> t.EQUI_MATNR;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.EQUI_TYPBZ,'Not Set') as EQUI_TYPBZ
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.manufacturer_model_no = t.EQUI_TYPBZ
where d.manufacturer_model_no <> t.EQUI_TYPBZ;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.EQUI_LVORM,'Not Set') as EQUI_LVORM
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.deletion_flag = t.EQUI_LVORM
where d.deletion_flag <> t.EQUI_LVORM;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.EQUI_ANSWT, 0) as EQUI_ANSWT
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.aquisition_value = t.EQUI_ANSWT
where d.aquisition_value <> t.EQUI_ANSWT;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(e.EQUI_WAERS, 'Not Set') as EQUI_WAERS
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.currency = t.EQUI_WAERS
where d.currency <> t.EQUI_WAERS;

merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(z.EQUZ_HEQUI, 'Not Set') as EQUZ_HEQUI
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.superior_equipment = t.EQUZ_HEQUI
where d.superior_equipment <> t.EQUZ_HEQUI;


merge into tmp_dim_equipment d
using
(select distinct dim_equipmentid, ifnull(Z.EQUZ_HEQNR, 'Not Set') as EQUZ_HEQNR
FROM tmp_dim_equipment d inner join EQUI e on
ifnull(equi_equnr ,'Not Set') = EquipmentNo
LEFT JOIN EQUZ  z ON e.equi_equnr = z.equz_equnr
AND EquipmentValidtoDate = ifnull(z.equz_datbi,'9999-12-31')
AND EquipUsagePeriods = ifnull(z.equz_eqlfn,0)
) t
on t.dim_equipmentid = d.dim_equipmentid
when matched then update
set d.equi_pos_at_install_loc = t.EQUZ_HEQNR
where d.equi_pos_at_install_loc <> t.EQUZ_HEQNR;

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'tmp_dim_equipment';        

DROP TABLE if EXISTS dim_equipment;
RENAME tmp_dim_equipment to dim_equipment;


