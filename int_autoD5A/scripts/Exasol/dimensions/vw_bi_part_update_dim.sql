UPDATE dim_part dp 
   SET PartDescription = ifnull(MAKT_MAKTX, 'Not Set'),
       Revision = ifnull(MARA_KZREV, 'Not Set'),
       UnitOfMeasure = ifnull(MARA_MEINS, 'Not Set'),
       CommodityCode = ifnull(MARC_STAWN, 'Not Set'),
       PartType = ifnull(MARA_MTART, 'Not Set'),
       LeadTime = ifnull(MARC_PLIFZ, 0),
       StockLocation = ifnull(marc_lgpro, 'Not Set'),
       PurchaseGroupCode = ifnull(marc_ekgrp, 'Not Set'),
       PurchaseGroupDescription =
          ifnull((SELECT t.T024_EKNAM
                    FROM t024 t
                   WHERE t.T024_EKGRP = MARC_EKGRP),
                 'Not Set'),
       MRPController = ifnull(marc_dispo, 'Not Set'),
       MaterialGroup = ifnull(mara_matkl, 'Not Set'),
       Plant = ifnull(mck.marc_werks, 'Not Set'),
       ABCIndicator = ifnull(marc_maabc, 'Not Set'),
       ProcurementType = ifnull(marc_beskz, 'Not Set'),
       StorageLocation = ifnull(MARC_LGFSB, 'Not Set'),
       CriticalPart = ifnull(MARC_KZKRI, 'Not Set'),
       MRPType = ifnull(MARC_DISMM, 'Not Set'),
       SupplySource = ifnull(MARC_BWSCL, 'Not Set'),
       StrategyGroup = ifnull(MARC_STRGR, 'Not Set'),
       TransportationGroup = ifnull(MARA_TRAGR, 'Not Set'),
       Division = ifnull(MARA_SPART, 'Not Set'),
       DivisionDescription =
          ifnull((SELECT TSPAT_VTEXT
                    FROM TSPAT
                   WHERE TSPAT_SPART = MARA_SPART AND TSPAT_SPRAS = 'E'),
                 'Not Set'),
       GeneralItemCategory = ifnull(MARA_MTPOS, 'Not Set'),
       DeletionFlag = ifnull(MARC_LVORM, 'Not Set'),
       MaterialStatus = ifnull(ifnull(MARC_MMSTA, MARA_MSTAE), 'Not Set'),
       ProductHierarchy = ifnull(MARA_PRDHA, 'Not Set'),
       MPN = ifnull(MARA_MFRPN, 'Not Set'),
       BulkMaterial = ifnull(MARC_SCHGT, 'Not Set'),
       ProductHierarchyDescription =
          ifnull((SELECT t.VTEXT
                    FROM t179t t
                   WHERE t.PRODH = mck.MARA_PRDHA),
                 'Not Set'),
       MRPProfile = ifnull(mck.MARC_DISPR, 'Not Set'),
       MRPProfileDescription =
          ifnull((SELECT p.T401T_KTEXT
                    FROM t401t p
                   WHERE p.T401T_DISPR = mck.MARC_DISPR),
                 'Not Set'),
       PartTypeDescription =
          ifnull((SELECT pt.T134T_MTBEZ
                    FROM T134T pt
                   WHERE pt.T134T_MTART = mck.MARA_MTART),
                 'Not Set'),
       MaterialGroupDescription =
          ifnull((SELECT mg.T023T_WGBEZ
                    FROM T023T mg
                   WHERE mg.T023T_MATKL = mck.MARA_MATKL),
                 'Not Set'),
       MRPTypeDescription =
          ifnull((SELECT mt.T438T_DIBEZ
                    FROM T438T mt
                   WHERE mt.T438T_DISMM = mck.MARC_DISMM),
                 'Not Set'),
       ProcurementTypeDescription =
          ifnull(
             (SELECT dd.DD07T_DDTEXT
                FROM DD07T dd
               WHERE dd.DD07T_DOMNAME = 'BESKZ'
                     AND dd.DD07T_DOMVALUE = MARC_BESKZ),
             'Not Set'),
       MRPControllerDescription =
          ifnull(
             (SELECT mc.T024D_DSNAM
                FROM T024D mc
               WHERE mc.T024D_DISPO = mck.MARC_DISPO
                     AND mc.T024D_WERKS = mck.MARC_WERKS),
             'Not Set'),
       MRPGroup = ifnull(MARC_DISGR, 'Not Set'),
       MRPGroupDescription =
          ifnull(
             (SELECT mpg.T438X_TEXT40
                FROM T438X mpg
               WHERE mpg.T438X_DISGR = mck.MARC_DISGR
                     AND mpg.T438X_WERKS = mck.MARC_WERKS),
             'Not Set'),
       MRPLotSize = ifnull(mck.MARC_DISLS, 'Not Set'),
       MRPLotSizeDescription =
          ifnull((SELECT ml.T439T_LOSLT
                    FROM T439T ml
                   WHERE ml.T439T_DISLS = mck.MARC_DISLS),
                 'Not Set'),
       materialstatusdescription =
          ifnull(
             (SELECT t141t_MTSTB
                FROM t141t mst
               WHERE mst.T141T_MMSTA = ifnull(mck.MARC_MMSTA, mck.MARA_MSTAE)),
             'Not Set'),
       dp.SpecialProcurement = ifnull(mck.MARC_SOBSL, 'Not Set'),
       dp.SpecialProcurementDescription =
          ifnull(
             (SELECT sp.T460T_LTEXT
                FROM t460t sp
               WHERE sp.T460T_SOBSL = mck.marc_sobsl
                     AND sp.T460T_WERKS = mck.MARC_WERKS),
             'Not Set'),
       dp.InhouseProductionTime = mck.MARC_DZEIT,
       dp.ProfitCenterCode = ifnull(mck.MARC_PRCTR, 'Not Set'),
       dp.ProfitCenterName =
          ifnull(
             (SELECT c.CEPCT_KTEXT
                FROM cepct c
               WHERE c.CEPCT_PRCTR = mck.MARC_PRCTR
                     AND c.CEPCT_DATBI > current_date),
             'Not Set'),
       dp.ExternalMaterialGroupCode = ifnull(MARA_EXTWG, 'Not Set'),
       dp.ExternalMaterialGroupDescription =
          ifnull((SELECT t.TWEWT_EWBEZ
                    FROM twewt t
                   WHERE t.TWEWT_EXTWG = MARA_EXTWG),
                 'Not Set'),
       dp.MaterialDiscontinuationFlag = ifnull(MARC_KZAUS, 'Not Set'),
       dp.MaterialDiscontinuationFlagDescription =
          ifnull(
             (SELECT DD07T_DDTEXT
                FROM DD07T
               WHERE DD07T_DOMNAME = 'KZAUS' AND DD07T_DOMVALUE = MARC_KZAUS),
             'Not Set'),
       dp.ProcessingTime = MARC_BEARZ,
       dp.TotalReplenishmentLeadTime = MARC_WZEIT,
       dp.GRProcessingTime = MARC_WEBAZ,
       dp.OldPartNumber = ifnull(MARA_BISMT, 'Not Set'),
       dp.AFSColor = ifnull(MARA_J_3ACOL, 'Not Set'),
       dp.AFSColorDescription =
          ifnull((SELECT J_3ACOLRT_TEXT
                    FROM J_3ACOLRT
                   WHERE J_3ACOLRT_J_3ACOL = MARA_J_3ACOL),
                 'Not Set'),
       dp.SDMaterialStatusDescription =
          ifnull((SELECT TVMST_VMSTB
                    FROM tvmst
                   WHERE TVMST_VMSTA = MARA_MSTAV),
                 'Not Set'),
       dp.Volume = MARA_VOLUM,
       dp.AFSMasterGrid = ifnull(MARA_J_3APGNR, 'Not Set'),
       dp.AFSPattern = ifnull(MARA_AFS_SCHNITT, 'Not Set'),
       dp.DoNotCost = ifnull(MARC_NCOST, 'Not Set'),
       dp.PlantMaterialStatus = ifnull(mck.MARC_MMSTA, 'Not Set'),
       dp.PlantMaterialStatusDescription =
          ifnull((SELECT t141t_MTSTB
                    FROM t141t mst
                   WHERE mst.T141T_MMSTA = mck.MARC_MMSTA),
                 'Not Set'),
       dp.REMProfile = ifnull(MARC_SFEPR, 'Not Set'),
       dp.PlanningTimeFence = ifnull(MARC_FXHOR, 0),
       dp.SafetyStock = ifnull(MARC_EISBE, 0.0000),
       dp.RoundingValue = ifnull(MARC_BSTRF, 0.0000),
       dp.ReorderPoint = ifnull(MARC_MINBE, 0.0000),
       dp.MinimumLotSize = ifnull(MARC_BSTMI,0.0000),
       dp.ValidFrom = (select ifnull(j1.J_3AMAD_J_4ADTFR,'0001-01-01') FROM J_3AMAD j1 where mck.mara_matnr = j1.j_3amad_matnr and j1.J_3AMAD_WERKS = mck.MARC_WERKS),
       dp.MRPStatus = ifnull((SELECT j2.J_3AMAD_J_4ASTAT from J_3AMAD j2 where mck.mara_matnr = j2.j_3amad_matnr and j2.J_3AMAD_WERKS = mck.MARC_WERKS), 'Not Set'),
       dp.CheckingGroupCode = ifnull(mck.MARC_MTVFP, 'Not Set'),
       dp.CheckingGroup = ifnull((SELECT tm.TMVFT_BEZEI FROM TMVFT tm where mck.marc_mtvfp = tm.tmvft_mtvfp), 'Not Set'),
       dp.UPCNumber=ifnull((select MEAN_EAN11 from MEAN where MEAN_MATNR = mck.mara_matnr), 'Not Set'),
       dp.MaximumStockLevel = ifnull(mck.MARC_MABST, 0.0000),
       dp.ConsumptionMode = ifnull(mck.MARC_VRMOD, 'Not Set'),
       dp.ConsumptionModeDescription = ifnull((SELECT cdm.DD07T_DDTEXT FROM DD07T cdm WHERE cdm.DD07T_DOMNAME = 'VRMOD' AND cdm.DD07T_DOMVALUE = MARC_VRMOD), 'Not Set'),
       dp.ConsumptionPeriodBackward = ifnull(mck.MARC_VINT1, 0),
       dp.ConsumptionPeriodForward = ifnull(mck.MARC_VINT2, 0),
       dp.PartLongDesc = ifnull(mck.MAKT_MAKTG,'Not Set'),
       dp.UPCCode = ifnull(mck.MARA_EAN11,'Not Set'),
       dp.NDCCode = ifnull(mck.MARA_GROES,'Not Set'),
       dp.MRPControllerTelephone = ifnull((SELECT mc.T024D_DSTEL FROM T024D mc WHERE mc.T024D_DISPO = mck.MARC_DISPO AND mc.T024D_WERKS = mck.MARC_WERKS), 'Not Set'),
       dp.dw_update_date = current_timestamp
from mara_marc_makt mck, dim_plant p, dim_part dp 
 WHERE     dp.PartNumber = mck.MARA_MATNR
       AND dp.Plant = mck.MARC_WERKS
       AND p.plantcode = marc_werks;

      update dim_part  set ValidFrom ='0001-01-01' where ValidFrom is null;