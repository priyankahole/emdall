/* 	Server: QA
	Process Name: WM Requirement Type Transfer
	Interface No: 2
*/

INSERT INTO dim_wmrequirementtype
(dim_wmrequirementtypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmrequirementtype where dim_wmrequirementtypeid = 1);

UPDATE    dim_wmrequirementtype wmrt
   SET  wmrt.RequirementTypeDescription = t.T308T_BTYPT,
			wmrt.dw_update_date = current_timestamp
FROM T308T t,dim_wmrequirementtype wmrt
 WHERE wmrt.RowIsCurrent = 1
     AND wmrt.RequirementType = t.T308T_BETYP
	   AND wmrt.WarehouseNumber = t.T308T_LGNUM;
 
delete from number_fountain m where m.table_name = 'dim_wmrequirementtype';

insert into number_fountain				   
select 	'dim_wmrequirementtype',
	ifnull(max(d.dim_wmrequirementtypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmrequirementtype d
where d.dim_wmrequirementtypeid <> 1;

INSERT INTO dim_wmrequirementtype(dim_wmrequirementtypeid,
							WarehouseNumber,
							RequirementType,
                            RequirementTypeDescription,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmrequirementtype') + row_number() over(order by ''),
		  t.T308T_LGNUM,
          t.T308T_BETYP,
		  t.T308T_BTYPT,
          current_timestamp,
          1
     FROM T308T t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_wmrequirementtype s
                  WHERE    s.RequirementType = t.T308T_BETYP
					    AND s.WarehouseNumber = t.T308T_LGNUM
                        AND s.RowIsCurrent = 1) ;
