UPDATE    dim_tasklisttype y
   SET y.Description = ifnull(TCA02_TXT, 'Not Set'),
			y.dw_update_date = current_timestamp
       FROM dim_tasklisttype y,
          TCA02 t
 WHERE y.RowIsCurrent = 1
 AND y.TaskListTypeCode = t.TCA02_PLNTY
;

INSERT INTO dim_tasklisttype(dim_tasklisttypeId, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_tasklisttype
               WHERE dim_tasklisttypeId = 1);


delete from number_fountain m where m.table_name = 'dim_tasklisttype';

insert into number_fountain
select 	'dim_tasklisttype',
	ifnull(max(d.Dim_tasklisttypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_tasklisttype d
where d.Dim_tasklisttypeid <> 1;

INSERT INTO dim_tasklisttype(Dim_tasklisttypeid,
								Description,
                                TaskListTypeCode,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_tasklisttype') 
          + row_number() over(order by ''),
			 ifnull(TCA02_TXT, 'Not Set'),
            TCA02_PLNTY,
            current_timestamp,
            1
       FROM TCA02
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_tasklisttype
                    WHERE TaskListTypeCode = TCA02_PLNTY)
   ORDER BY 2 ;
