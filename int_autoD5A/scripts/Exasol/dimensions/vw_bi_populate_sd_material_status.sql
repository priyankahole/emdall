INSERT INTO dim_sdmaterialstatus(Dim_SDMaterialStatusid,
                                 SDMaterialStatus,
                                 SDMaterialStatusDescription,
                                 RowStartDate,
                                 RowEndDate,
                                 RowIsCurrent,
                                 RowChangeReason)
   SELECT 1,
          'Not Set',
          'Not Set',
          NULL,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_sdmaterialstatus
               WHERE dim_sdmaterialstatusid = 1);

UPDATE dim_sdmaterialstatus a 
   SET a.SDMaterialStatusDescription = ifnull(TVMST_VMSTB, 'Not Set'),
			a.dw_update_date = current_timestamp
   FROM TVMST , dim_sdmaterialstatus a 
 WHERE a.SDMaterialStatus = TVMST_VMSTA AND RowIsCurrent = 1;

delete from number_fountain m where m.table_name = 'dim_sdmaterialstatus';

insert into number_fountain
select 	'dim_sdmaterialstatus',
	ifnull(max(d.Dim_SDMaterialStatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_sdmaterialstatus d
where d.Dim_SDMaterialStatusid <> 1; 
 
 INSERT INTO dim_sdmaterialstatus(Dim_SDMaterialStatusid,
                                    SDMaterialStatus,
                                    SDMaterialStatusDescription,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_sdmaterialstatus') 
          + row_number() over(order by '') ,
			 t.TVMST_VMSTA,
          ifnull(t.TVMST_VMSTB, 'Not Set'),
          current_timestamp,
          1
     FROM TVMST t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_sdmaterialstatus a
               WHERE a.SDMaterialStatus = t.TVMST_VMSTA);