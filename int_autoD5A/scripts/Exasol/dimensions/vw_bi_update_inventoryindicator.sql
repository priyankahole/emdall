
UPDATE    dim_inventoryindicator ii

        SET ii.Description = ifnull(dt.DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T dt, dim_inventoryindicator ii
 WHERE ii.InventoryIndicator = dt.DD07T_DOMVALUE AND ii.RowIsCurrent = 1
 AND dt.DD07T_DOMNAME = 'LVS_KZINV' AND dt.DD07T_DOMNAME IS NOT NULL;
