UPDATE    
dim_salesriskcategory src
SET src.SalesRiskCategoryDescription = ifnull(t.T691T_RTEXT, 'Not Set'),
	src.dw_update_date = current_timestamp
FROM
dim_salesriskcategory src,
T691T t
WHERE src.RowIsCurrent = 1
AND src.SalesRiskCategory = t.T691T_CTLPC
AND src.CreditControlArea = t.T691T_KKBER
;
