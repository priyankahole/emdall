UPDATE    dim_catalogprofile cp
    
   SET cp.CatalogProfileName = ifnull(t.T352B_T_RBNRX, 'Not Set')
          FROM
          T352B_T t , dim_catalogprofile cp
 WHERE cp.RowIsCurrent = 1
 AND t.T352B_T_RBNR = cp.CatalogProfileCode
;