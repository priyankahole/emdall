
INSERT INTO dim_creditcontrolarea(dim_creditcontrolareaId, RowIsCurrent,creditcontrolareacode,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_creditcontrolarea
               WHERE dim_creditcontrolareaId = 1);
			   
UPDATE    dim_creditcontrolarea cca
   SET cca.CreditControlAreaName = t.T014T_KKBTX,
			cca.dw_update_date = current_timestamp
       FROM dim_creditcontrolarea cca,
          T014T t
 WHERE cca.RowIsCurrent = 1
 AND t.T014T_KKBER = cca.CreditControlAreaCode;
 
delete from number_fountain m where m.table_name = 'dim_creditcontrolarea';

insert into number_fountain
select 	'dim_creditcontrolarea',
	ifnull(max(d.dim_creditcontrolareaId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_creditcontrolarea d
where d.dim_creditcontrolareaId <> 1;

INSERT INTO dim_creditcontrolarea(Dim_CreditControlAreaid,
							CreditControlAreaCode,
                            CreditControlAreaName,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_creditcontrolarea') 
          + row_number() over(order by '') ,
			T014T_KKBER,
          T014T_KKBTX,
          current_timestamp,
          1
     FROM T014T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_creditcontrolarea cca
               WHERE cca.CreditControlAreaCode = T014T_KKBER
                     AND cca.CreditControlAreaName = T014T_KKBTX);

delete from number_fountain m where m.table_name = 'dim_creditcontrolarea';
					 