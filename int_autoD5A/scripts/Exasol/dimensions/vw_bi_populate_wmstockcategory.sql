/* 	Server: QA
	Process Name: WM Stock Category Transfer
	Interface No: 2
*/

INSERT INTO dim_wmstockcategory
(dim_wmstockcategoryid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmstockcategory where dim_wmstockcategoryid = 1);
 
delete from number_fountain m where m.table_name = 'dim_wmstockcategory';

insert into number_fountain				   
select 	'dim_wmstockcategory',
	ifnull(max(d.dim_wmstockcategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmstockcategory d
where d.dim_wmstockcategoryid <> 1;

INSERT INTO dim_wmstockcategory(dim_wmstockcategoryid,
                               WMStockCategory,
                               RowStartDate,
                               RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmstockcategory') 
          + row_number() over(order by '') , a.*  from (select distinct
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T) a
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_wmstockcategory
                    WHERE WMStockCategory = a.DD07T_DOMVALUE)
   ORDER BY 2;

UPDATE    dim_wmstockcategory wmsc
SET wmsc.Description = ifnull(dt.DD07T_DDTEXT, 'Not Set'),
			wmsc.dw_update_date = current_timestamp
FROM dim_wmstockcategory wmsc, (select distinct DD07T_DDTEXT,DD07T_DOMVALUE,DD07T_DOMNAME, row_number() over (partition by DD07T_DOMVALUE order by DD07T_DOMVALUE) as RN from DD07T) dt
WHERE 		wmsc.WMStockCategory = dt.DD07T_DOMVALUE AND wmsc.RowIsCurrent = 1
		AND dt.DD07T_DOMNAME IS NOT NULL
and dt.RN=1;