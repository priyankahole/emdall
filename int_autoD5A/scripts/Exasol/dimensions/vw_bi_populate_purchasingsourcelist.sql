/* Octavian: Purchasing Source List dimension based on EORD. This dimension doesn't use the number fountain as we couldn't link up properly to Purchase (not all EORD PK fields can be linked to Purchase actual fields and adding would mean to modify it's grain) */
/* Removed the dependency on number_fountain values as it uses hash per each combination, generating new values each time as it has a not exist combination in the where condition */

insert into dim_purchasingsourcelist (dim_purchasingsourcelistid,
                          materialnumber,
                          plant,
     nosourcelistrecord)
select 1, 'Not Set', 'Not Set', 0
from (select 1) a
where not exists ( select 'x' from dim_purchasingsourcelist where dim_purchasingsourcelistid = 1);



/* 
delete from number_fountain m where m.table_name = 'dim_purchasingsourcelist'
insert into number_fountain
select 'dim_purchasingsourcelist',
 ifnull(max(d.dim_purchasingsourcelistid), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_purchasingsourcelist d
where d.dim_purchasingsourcelistid <> 1
*/

update dim_purchasingsourcelist d
set plant = 'Not S'
where plant = 'Not Set';

insert into dim_purchasingsourcelist (dim_purchasingsourcelistid,
                          materialnumber,
                          plant,
     nosourcelistrecord,
     dw_insert_date)
 select 
 
 /* (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_purchasingsourcelist') + */
 FIRST_VALUE(
 ABS(HASH(concat(ds.EORD_MATNR,ds.EORD_WERKS)))
 ) over(partition by ds.EORD_MATNR,ds.EORD_WERKS ORDER BY ds.EORD_MATNR,ds.EORD_WERKS) AS dim_purchasingsourcelistid,
        ifnull(ds.EORD_MATNR, 'Not Set') AS materialnumber,
        ifnull(ds.EORD_WERKS, 'Not Set') AS plant,
 ifnull(ds.EORD_ZEORD, 0) AS nosourcelistrecord,
       current_timestamp
from EORD ds
 where not exists (select 'x' from dim_purchasingsourcelist dc where dc.materialnumber = ifnull(ds.EORD_MATNR, 'Not Set') 
 														   AND dc.plant = ifnull(ds.EORD_WERKS, 'Not Set')
 														   AND dc.nosourcelistrecord = ifnull(ds.EORD_ZEORD,0)
 														   )
/* ORDER BY materialnumber,plant,nosourcelistrecord */ 														   
;


UPDATE dim_purchasingsourcelist d
SET usageinmatplanning = ifnull(e.EORD_AUTET,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND usageinmatplanning <> ifnull(e.EORD_AUTET,'Not Set');

UPDATE dim_purchasingsourcelist d
SET validto = ifnull(e.EORD_BDATU,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND validto <> ifnull(e.EORD_BDATU,'0001-01-01');

UPDATE dim_purchasingsourcelist d
SET agreementnumber = ifnull(e.EORD_EBELN,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND agreementnumber <> ifnull(e.EORD_EBELN,'Not Set');

UPDATE dim_purchasingsourcelist d
SET agreementitem = ifnull(e.EORD_EBELP,0),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND agreementitem <> ifnull(e.EORD_EBELP,0);

UPDATE dim_purchasingsourcelist d
SET purchasingorg = ifnull(e.EORD_EKORG,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND purchasingorg <> ifnull(e.EORD_EKORG,'Not Set');

UPDATE dim_purchasingsourcelist d
SET categoryof = ifnull(e.EORD_EORTP,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND categoryof <> ifnull(e.EORD_EORTP,'Not Set');

UPDATE dim_purchasingsourcelist d
SET fixedoutlinepurchase = ifnull(e.EORD_FEBEL,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND fixedoutlinepurchase <> ifnull(e.EORD_FEBEL,'Not Set');

UPDATE dim_purchasingsourcelist d
SET fixedvendor = ifnull(e.EORD_FLIFN,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND fixedvendor <> ifnull(e.EORD_FLIFN,'Not Set');

UPDATE dim_purchasingsourcelist d
SET fixedplant_stocktrasnporder = ifnull(e.EORD_FRESW,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND fixedplant_stocktrasnporder <> ifnull(e.EORD_FRESW,'Not Set');

UPDATE dim_purchasingsourcelist d
SET purchaseuom = ifnull(e.EORD_MEINS,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND purchaseuom <> ifnull(e.EORD_MEINS,'Not Set');

UPDATE dim_purchasingsourcelist d
SET blockedsource = ifnull(e.EORD_NOTKZ,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND blockedsource <> ifnull(e.EORD_NOTKZ,'Not Set');

UPDATE dim_purchasingsourcelist d
SET plant_matprocured = ifnull(e.EORD_RESWK,'Not Set'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND plant_matprocured <> ifnull(e.EORD_RESWK,'Not Set');

UPDATE dim_purchasingsourcelist d
SET validfrom = ifnull(e.EORD_VDATU,'0001-01-01'),
dw_update_date = CURRENT_TIMESTAMP
FROM EORD e, dim_purchasingsourcelist d
WHERE
d.materialnumber = ifnull(e.EORD_MATNR,'Not Set')
AND d.plant = ifnull(e.EORD_WERKS,'Not Set')
AND d.nosourcelistrecord = ifnull(e.EORD_ZEORD,0)
AND validfrom <> ifnull(e.EORD_VDATU,'0001-01-01');