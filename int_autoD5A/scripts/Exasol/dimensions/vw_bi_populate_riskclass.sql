UPDATE dim_riskclass
   SET RiskClassText = ifnull(t.RISK_CLASS_TXT, 'Not Set'),
			dw_update_date = current_timestamp
      FROM ukm_risk_cl0t t, dim_riskclass
 WHERE RiskClass = t.RISK_CLASS AND RowIsCurrent = 1
;

INSERT INTO dim_riskclass(dim_riskclassId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_riskclass
               WHERE dim_riskclassId = 1);

delete from number_fountain m where m.table_name = 'dim_riskclass';
   
insert into number_fountain
select 	'dim_riskclass',
	ifnull(max(d.Dim_riskclassId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_riskclass d
where d.dim_riskclassId <> 1; 


INSERT INTO dim_riskclass(Dim_riskclassId,
			   RiskClass,
                           RiskClassText,
                           RowStartDate,
                           RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_riskclass')
         + row_number() over(order by ''),
			  RISK_CLASS,
          ifnull(t.RISK_CLASS_TXT,'Not Set'),
          current_timestamp,
          1
     FROM ukm_risk_cl0t t
    WHERE NOT EXISTS
                     (SELECT 1
                        FROM dim_riskclass rk
                       WHERE    rk.RiskClass = t.RISK_CLASS
                             AND rk.RowIsCurrent = 1)
;

