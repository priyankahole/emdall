
UPDATE    dim_bomusage bu

   SET bu.Description = ifnull(t.T416T_ANTXT, 'Not Set')
          FROM
          T416T t, dim_bomusage bu
 WHERE bu.RowIsCurrent = 1
 AND bu.BOMUsageCode = t.T416T_STLAN;
