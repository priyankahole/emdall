
UPDATE    dim_materialgroup s
   SET s.MaterialGroupName = t.T023T_WGBEZ,
			s.dw_update_date = current_timestamp
			FROM
          dim_materialgroup s, T023T t
 WHERE s.RowIsCurrent = 1
 AND s.MaterialGroupCode = t.T023T_MATKL;
