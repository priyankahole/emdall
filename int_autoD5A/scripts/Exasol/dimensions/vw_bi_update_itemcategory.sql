
UPDATE    dim_itemcategory ic

   SET ic.Description = t.T163Y_PTEXT,
       ic.Category = ifnull(T163Y_EPSTP, 'Not Set')
          FROM
          T163Y t, dim_itemcategory ic
 WHERE ic.RowIsCurrent = 1
 AND t.T163Y_PSTYP = ic.categorycode;
