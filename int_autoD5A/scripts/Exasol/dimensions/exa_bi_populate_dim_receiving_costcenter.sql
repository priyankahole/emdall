/*DEFAULT ROW*/

INSERT INTO dim_receiving_costcenter(
dim_receiving_costcenterid,
objectnumber,
distribution_rule_group,
seq_number,
settlement_type,
settlement_percentage_rate,
receiver_cost_center,
language,
controlling_area,
cost_center,
cost_center_descr,
valid_to_Date,
dw_insert_date,
dw_update_date
)
  SELECT    
1 as dim_receiving_costcenter,
'Not Set' as objectnumber,
0 as distribution_rule_group,
0 as seq_number,
'Not Set' as settlement_type,
0 as settlement_percentage_rate,
'Not Set' as receiver_cost_center,
'Not Set' as language,
'Not Set' as controlling_area,
'Not Set' as cost_center,
'Not Set' as cost_center_descr,
'0001-01-01' as valid_to_Date,
current_timestamp as dw_insert_date,
current_timestamp as dw_update_date

FROM (SELECT 1) a
WHERE NOT EXISTS(SELECT 1 FROM dim_receiving_costcenter WHERE dim_receiving_costcenterid = 1);  


DROP TABLE IF EXISTS cobrb_cskt;
create table cobrb_cskt AS 
SELECT DISTINCT *
FROM COBRB co, CSKT c
where co.COBRB_KOSTL = c.CSKT_KOSTL
and co.COBRB_PERBZ	= 'GES'
and c.CSKT_DATBI	= '9999-12-31';


DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_receiving_costcenter';

INSERT INTO NUMBER_FOUNTAIN
select  'dim_receiving_costcenter',
                ifnull(max(d.dim_receiving_costcenterid),
                ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_receiving_costcenter d
where d.dim_receiving_costcenterid <> 1;

/*insert new rows*/
INSERT INTO dim_receiving_costcenter(
dim_receiving_costcenterid,
receiver_cost_center,
objectnumber,
distribution_rule_group,
seq_number,
settlement_type,
settlement_percentage_rate,
language,
controlling_area,
cost_center,
cost_center_descr,
valid_to_Date,
dw_insert_date
)
SELECT ( SELECT ifnull(m.max_id, 1) FROM NUMBER_FOUNTAIN m WHERE m.table_name = 'dim_receiving_costcenter') + row_number() over(order by '') as dim_receiving_costcenterid,
ifnull(COBRB_KOSTL, 'Not Set') as receiver_cost_center,
ifnull(COBRB_OBJNR, 'Not Set') as objectnumber,
ifnull(COBRB_BUREG, 0) as distribution_rule_group,
ifnull(COBRB_LFDNR, 0) as seq_number,
ifnull(COBRB_PERBZ, 'Not Set') as settlement_type,
ifnull(COBRB_PROZS, '0') as settlement_percentage_rate,
ifnull(CSKT_SPRAS, 'Not Set') as language,
ifnull(CSKT_KOKRS, 'Not Set') as controlling_area,
ifnull(CSKT_KOSTL, 'Not Set') as cost_center,
ifnull(CSKT_KTEXT, 'Not Set') as cost_center_descr,
ifnull(CSKT_DATBI, '0001-01-01') as valid_to_Date,

current_timestamp
FROM cobrb_cskt t
WHERE NOT EXISTS
      (SELECT 1
         FROM dim_receiving_costcenter d
        where ifnull(t.COBRB_KOSTL, 'Not Set') = d.receiver_cost_center
        and ifnull(t.COBRB_OBJNR, 'Not Set') = d.objectnumber
        and ifnull(t.COBRB_BUREG, 0) = d.distribution_rule_group
        and ifnull(t.COBRB_LFDNR, 0) = d.seq_number
        );

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_receiving_costcenter'; 

/*
seq_number,
settlement_type,
settlement_percentage_rate,
language,
controlling_area,
cost_center,
valid_to_Date,
*/
--merges to be made
merge into dim_receiving_costcenter d
using(
select distinct dim_receiving_costcenterid, ifnull(COBRB_PERBZ, 'Not Set') as settlement_type
from dim_receiving_costcenter d, cobrb_cskt t
where ifnull(t.COBRB_KOSTL, 'Not Set') = d.receiver_cost_center
and ifnull(t.COBRB_OBJNR, 'Not Set') = d.objectnumber
and ifnull(t.COBRB_BUREG, 0) = d.distribution_rule_group
and ifnull(t.COBRB_LFDNR, 0) = d.seq_number
)t
on t.dim_receiving_costcenterid = d.dim_receiving_costcenterid
when matched then update
set d.settlement_type = t.settlement_type,
dw_update_date = current_timestamp
where d.settlement_type <> t.settlement_type;

merge into dim_receiving_costcenter d
using(
select distinct dim_receiving_costcenterid, ifnull(COBRB_PROZS, '0') as settlement_percentage_rate
from dim_receiving_costcenter d, cobrb_cskt t
where ifnull(t.COBRB_KOSTL, 'Not Set') = d.receiver_cost_center
and ifnull(t.COBRB_OBJNR, 'Not Set') = d.objectnumber
and ifnull(t.COBRB_BUREG, 0) = d.distribution_rule_group
and ifnull(t.COBRB_LFDNR, 0) = d.seq_number
)t
on t.dim_receiving_costcenterid = d.dim_receiving_costcenterid
when matched then update
set d.settlement_percentage_rate = t.settlement_percentage_rate,
dw_update_date = current_timestamp;

merge into dim_receiving_costcenter d
using(
select distinct dim_receiving_costcenterid, ifnull(CSKT_SPRAS, 'Not Set') as language
from dim_receiving_costcenter d, cobrb_cskt t
where ifnull(t.COBRB_KOSTL, 'Not Set') = d.receiver_cost_center
and ifnull(t.COBRB_OBJNR, 'Not Set') = d.objectnumber
and ifnull(t.COBRB_BUREG, 0) = d.distribution_rule_group
and ifnull(t.COBRB_LFDNR, 0) = d.seq_number
)t
on t.dim_receiving_costcenterid = d.dim_receiving_costcenterid
when matched then update
set d.language = t.language,
dw_update_date = current_timestamp
where d.language <> t.language;

merge into dim_receiving_costcenter d
using(
select distinct dim_receiving_costcenterid, ifnull(CSKT_KOKRS, 'Not Set') as controlling_area
from dim_receiving_costcenter d, cobrb_cskt t
where ifnull(t.COBRB_KOSTL, 'Not Set') = d.receiver_cost_center
and ifnull(t.COBRB_OBJNR, 'Not Set') = d.objectnumber
and ifnull(t.COBRB_BUREG, 0) = d.distribution_rule_group
and ifnull(t.COBRB_LFDNR, 0) = d.seq_number
)t
on t.dim_receiving_costcenterid = d.dim_receiving_costcenterid
when matched then update
set d.controlling_area = t.controlling_area,
dw_update_date = current_timestamp
where d.controlling_area <> t.controlling_area;

merge into dim_receiving_costcenter d
using(
select distinct dim_receiving_costcenterid, ifnull(CSKT_KOSTL, 'Not Set') as cost_center
from dim_receiving_costcenter d, cobrb_cskt t
where ifnull(t.COBRB_KOSTL, 'Not Set') = d.receiver_cost_center
and ifnull(t.COBRB_OBJNR, 'Not Set') = d.objectnumber
and ifnull(t.COBRB_BUREG, 0) = d.distribution_rule_group
and ifnull(t.COBRB_LFDNR, 0) = d.seq_number
)t
on t.dim_receiving_costcenterid = d.dim_receiving_costcenterid
when matched then update
set d.cost_center = t.cost_center,
dw_update_date = current_timestamp
where d.cost_center <> t.cost_center;

merge into dim_receiving_costcenter d
using(
select distinct dim_receiving_costcenterid, ifnull(CSKT_KTEXT, 'Not Set') as cost_center_descr
from dim_receiving_costcenter d, cobrb_cskt t
where ifnull(t.COBRB_KOSTL, 'Not Set') = d.receiver_cost_center
and ifnull(t.COBRB_OBJNR, 'Not Set') = d.objectnumber
and ifnull(t.COBRB_BUREG, 0) = d.distribution_rule_group
and ifnull(t.COBRB_LFDNR, 0) = d.seq_number
)t
on t.dim_receiving_costcenterid = d.dim_receiving_costcenterid
when matched then update
set d.cost_center_descr = t.cost_center_descr,
dw_update_date = current_timestamp
where d.cost_center_descr <> t.cost_center_descr;

merge into dim_receiving_costcenter d
using(
select distinct dim_receiving_costcenterid, ifnull(CSKT_DATBI, '0001-01-01') as valid_to_Date
from dim_receiving_costcenter d, cobrb_cskt t
where ifnull(t.COBRB_KOSTL, 'Not Set') = d.receiver_cost_center
and ifnull(t.COBRB_OBJNR, 'Not Set') = d.objectnumber
and ifnull(t.COBRB_BUREG, 0) = d.distribution_rule_group
and ifnull(t.COBRB_LFDNR, 0) = d.seq_number
)t
on t.dim_receiving_costcenterid = d.dim_receiving_costcenterid
when matched then update
set d.valid_to_Date = t.valid_to_Date,
dw_update_date = current_timestamp
where d.valid_to_Date <> t.valid_to_Date;

DELETE FROM NUMBER_FOUNTAIN where TABLE_NAME = 'dim_receiving_costcenter';