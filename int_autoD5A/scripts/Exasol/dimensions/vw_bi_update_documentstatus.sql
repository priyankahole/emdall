
UPDATE    dim_documentstatus ds

   SET ds.Description = ifnull(DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T dt, dim_documentstatus ds
 WHERE ds.RowIsCurrent = 1
 AND    dt.DD07T_DOMNAME = 'ESTAK'
          AND dt.DD07T_DOMVALUE IS NOT NULL
          AND ds.Status = dt.DD07T_DOMVALUE;

