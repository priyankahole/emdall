
insert into dim_vendormaster
(dim_vendormasterid, 
vendornumber, 
companycode)

select 	1, 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_vendormaster where dim_vendormasterid = 1);

delete from number_fountain m where m.table_name = 'dim_vendormaster';
insert into number_fountain
select 'dim_vendormaster',
 ifnull(max(d.dim_vendormasterid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_vendormaster d
where d.dim_vendormasterid <> 1;

insert into dim_vendormaster (dim_vendormasterid,
     vendornumber, 
     companycode,
	 recordcreationdate,
	 deletionflag,
	 postingblock,
	 pmtkeyterms,
	 dw_insert_date)
     select (select ifnull(m.max_id, 1) 
         from number_fountain m 
         where m.table_name = 'dim_vendormaster') + row_number() over(order by '') AS dim_vendormasterid,
	ifnull(lb.LFB1_LIFNR, 'Not Set') as vendornumber,
	ifnull(lb.LFB1_BUKRS, 'Not Set') as companycode,
	lb.LFB1_ERDAT as recordcreationdate,
	ifnull(lb.LFB1_LOEVM, 'Not Set') deletionflag,
	ifnull(lb.LFB1_SPERR, 'Not Set') postingblock,
	ifnull(lb.LFB1_ZTERM, 'Not Set') pmtkeyterms,
	current_timestamp
FROM
LFB1 lb
where not exists (select 'x' from dim_vendormaster dv
	               where ifnull(lb.LFB1_LIFNR, 'Not Set') = dv.vendornumber
	                  and ifnull(lb.LFB1_BUKRS, 'Not Set') = dv.companycode);

UPDATE dim_vendormaster dv
SET recordcreationdate = lb.LFB1_ERDAT
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM LFB1 lb, dim_vendormaster dv
WHERE
ifnull(lb.LFB1_LIFNR, 'Not Set') = dv.vendornumber
and ifnull(lb.LFB1_BUKRS, 'Not Set') = dv.companycode
AND recordcreationdate <> lb.LFB1_ERDAT;

UPDATE dim_vendormaster dv
SET deletionflag = ifnull(lb.LFB1_LOEVM, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM LFB1 lb, dim_vendormaster dv
WHERE
ifnull(lb.LFB1_LIFNR, 'Not Set') = dv.vendornumber
and ifnull(lb.LFB1_BUKRS, 'Not Set') = dv.companycode
AND deletionflag <> ifnull(lb.LFB1_LOEVM, 'Not Set');

UPDATE dim_vendormaster dv
SET postingblock = ifnull(lb.LFB1_SPERR, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM LFB1 lb, dim_vendormaster dv
WHERE
ifnull(lb.LFB1_LIFNR, 'Not Set') = dv.vendornumber
and ifnull(lb.LFB1_BUKRS, 'Not Set') = dv.companycode
AND postingblock <> ifnull(lb.LFB1_SPERR, 'Not Set');

UPDATE dim_vendormaster dv
SET pmtkeyterms = ifnull(lb.LFB1_ZTERM, 'Not Set') 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM LFB1 lb, dim_vendormaster dv
WHERE
ifnull(lb.LFB1_LIFNR, 'Not Set') = dv.vendornumber
and ifnull(lb.LFB1_BUKRS, 'Not Set') = dv.companycode
AND pmtkeyterms <> ifnull(lb.LFB1_ZTERM, 'Not Set') ;

/*Alin - BI-5058*/
UPDATE dim_vendormaster dv
SET indiv_payment_indic = ifnull(lb.LFB1_XPORE, 'Not Set') 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM LFB1 lb, dim_vendormaster dv
WHERE
ifnull(lb.LFB1_LIFNR, 'Not Set') = dv.vendornumber
and ifnull(lb.LFB1_BUKRS, 'Not Set') = dv.companycode
AND indiv_payment_indic <> ifnull(lb.LFB1_XPORE, 'Not Set') ;

/* Andrei R - add Clerk's internet address - APP-6504 */
UPDATE dim_vendormaster d
SET d.CLRKSINTERNET = ifnull(l.lfb1_intad, 'Not Set')
from dim_vendormaster d 
INNER JOIN LFB1 l on 
d.companycode = ifnull(l.lfb1_bukrs, 'Not Set') and 
d.vendornumber = ifnull(l.lfb1_lifnr, 'Not Set')
WHERE
d.CLRKSINTERNET <> ifnull(l.lfb1_intad, 'Not Set');


