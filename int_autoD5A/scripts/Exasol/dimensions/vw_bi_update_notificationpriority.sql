UPDATE       dim_notificationpriority s
          SET s.NotificationPriorityName = T356_T_PRIOKX,
			s.dw_update_date = current_timestamp
		FROM
             dim_notificationpriority s, T356_T t1, T356A_T t2
       WHERE t1.T356_T_ARTPR = t2.T356A_T_ARTPR
 AND s.RowIsCurrent = 1
 AND  s.NotificationPriorityType = t1.T356_T_ARTPR
 AND s.NotificationPriorityCode = t1.T356_T_PRIOK
;