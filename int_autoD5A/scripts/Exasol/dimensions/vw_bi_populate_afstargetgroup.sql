insert into Dim_AfsTargetGroup (Dim_AfsTargetGroupId,AfsTargetGroup,AfsTargetGroupDescription,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason)
select 1,'Not Set','Not Set',current_timestamp,NULL,1,NULL
FROM (SELECT 1) a
where not exists (select 1 from Dim_AfsTargetGroup where Dim_AfsTargetGroupid = 1);
               
UPDATE dim_AfsTargetGroup a FROM J_3AGENDRT 
   SET a.AFSTargetGroupDescription = ifnull(J_3AGENDRT_TEXT, 'Not Set')
 WHERE a.AFSTargetGroup = J_3AGENDRT_J_3AGEND AND RowIsCurrent = 1;
               
INSERT INTO dim_AfsTargetGroup(dim_AfsTargetGroupId,
                                    AfsTargetGroup,
                                    AFSTargetGroupDescription,
                                    RowStartDate,
                                    RowIsCurrent)
   SELECT (SELECT ifnull(max(atg.dim_AfsTargetGroupId)+1, 1) id
             FROM dim_AfsTargetGroup atg) 
          + row_number() over() ,
			 t.J_3AGENDRT_J_3AGEND,
          ifnull(t.J_3AGENDRT_TEXT, 'Not Set'),
          current_timestamp,
          1
     FROM J_3AGENDRT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_AfsTargetGroup a
               WHERE a.dim_AfsTargetGroupId = t.J_3AGENDRT_J_3AGEND);