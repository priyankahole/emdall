/* 	Server: QA
	Process Name: Storage Bin Transfer
	Interface No: 2
*/

INSERT INTO dim_storagebin
(dim_storagebinid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_storagebin where dim_storagebinid = 1);

/*LumiD, replaced update with merge distinct for unable t get rows error */
/*
UPDATE    dim_storagebin sb
   SET sb."zone" = ifnull(l.LAGP_LZONE,'Not Set'),
	sb.StorageSection = ifnull(l.LAGP_LGBER,'Not Set'),
			sb.dw_update_date = current_timestamp
 FROM dim_storagebin sb, LAGP l
 WHERE sb.RowIsCurrent = 1
     AND sb.StorageType = l.LAGP_LGTYP
	   AND sb.WarehouseNumber = l.LAGP_LGNUM
	   AND sb.StorageBin = l.LAGP_LGPLA*/

merge into dim_storagebin sb
using
( select distinct dim_storagebinid, ifnull(l.LAGP_LZONE,'Not Set') as LZONE, 
ifnull(l.LAGP_LGBER,'Not Set') as StorageSection
from dim_storagebin sb, LAGP l 
WHERE sb.RowIsCurrent = 1 
AND sb.StorageType = l.LAGP_LGTYP 
AND sb.WarehouseNumber = l.LAGP_LGNUM 
AND sb.StorageBin = l.LAGP_LGPLA
) x
on sb.dim_storagebinid = x.dim_storagebinid
when matched then
update set sb."zone" = x.LZONE, 
sb.StorageSection = x.StorageSection, 
sb.dw_update_date = current_timestamp;

/*Lumid, replace uodate with merge distinct to pass unable to get stable rows error%/
/*
UPDATE    dim_storagebin sb
SET	sb.StorageSectionDescription = ifnull(t.T302T_LBERT, 'Not Set')
 FROM dim_storagebin sb
INNER JOIN LAGP l on  sb.StorageType = l.LAGP_LGTYP AND sb.WarehouseNumber = l.LAGP_LGNUM AND sb.StorageBin = l.LAGP_LGPLA
LEFT JOIN T302T t  on t.T302T_LGNUM = l.LAGP_LGNUM	AND t.T302T_LGTYP = l.LAGP_LGTYP AND t.T302T_LGBER = l.LAGP_LGBER
 WHERE sb.RowIsCurrent = 1
AND sb.StorageSectionDescription <> ifnull(t.T302T_LBERT, 'Not Set')*/

merge into dim_storagebin sb 
using 
(select distinct dim_storagebinid, ifnull(t.T302T_LBERT, 'Not Set') as StorageSectionDescription
FROM dim_storagebin sb 
INNER JOIN LAGP l on sb.StorageType = l.LAGP_LGTYP 
AND sb.WarehouseNumber = l.LAGP_LGNUM 
AND sb.StorageBin = l.LAGP_LGPLA 
LEFT JOIN T302T t on t.T302T_LGNUM = l.LAGP_LGNUM 
AND t.T302T_LGTYP = l.LAGP_LGTYP 
AND t.T302T_LGBER = l.LAGP_LGBER 
WHERE sb.RowIsCurrent = 1 
) x
on sb.dim_storagebinid = x.dim_storagebinid
when matched then
update set sb.StorageSectionDescription = x.StorageSectionDescription
where sb.StorageSectionDescription <> x.StorageSectionDescription;

delete from number_fountain m where m.table_name = 'dim_storagebin';

insert into number_fountain				   
select 	'dim_storagebin',
	ifnull(max(d.dim_storagebinid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_storagebin d
where d.dim_storagebinid <> 1;
drop table if exists tmp_ins_storagebin;
create table tmp_ins_storagebin as
select distinct  l.LAGP_LGNUM as WarehouseNumber,
          l.LAGP_LGTYP as StorageType,
		  l.LAGP_LGPLA as StorageBin,
		  ifnull(l.LAGP_LZONE,'Not Set') as "zone",
          ifnull(l.LAGP_LGBER,'Not Set') as StorageSection,
	      convert(varchar(25), 'Not Set') as StorageSectionDescription, -----(ifnull((SELECT t.T302T_LBERT from T302T t where t.T302T_LGNUM = l.LAGP_LGNUM AND t.T302T_LGTYP = l.LAGP_LGTYP AND t.T302T_LGBER = l.LAGP_LGBER) ,'Not Set'),
		  IFNULL(l.LAGP_VERIF,'Not Set') AS VerificationField,
          current_timestamp as RowStartDate,
          1 as RowIsCurrent
     FROM LAGP l
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_storagebin s
                  WHERE s.StorageType = l.LAGP_LGTYP
					    AND s.WarehouseNumber = l.LAGP_LGNUM
						AND s.StorageBin = l.LAGP_LGPLA
                        AND s.RowIsCurrent = 1);


UPDATE    tmp_ins_storagebin sb
SET	sb.StorageSectionDescription = ifnull(t.T302T_LBERT, 'Not Set')
 FROM tmp_ins_storagebin sb
INNER JOIN LAGP l on  sb.StorageType = l.LAGP_LGTYP AND sb.WarehouseNumber = l.LAGP_LGNUM AND sb.StorageBin = l.LAGP_LGPLA
LEFT JOIN T302T t  on t.T302T_LGNUM = l.LAGP_LGNUM	AND t.T302T_LGTYP = l.LAGP_LGTYP AND t.T302T_LGBER = l.LAGP_LGBER
 WHERE sb.RowIsCurrent = 1
AND sb.StorageSectionDescription <> ifnull(t.T302T_LBERT, 'Not Set');

INSERT INTO dim_storagebin(dim_storagebinid,
                            WarehouseNumber,
			StorageType,
                            StorageBin,
				"zone",
			    StorageSection,
			    StorageSectionDescription,
			VerificationField,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_storagebin') 
          + row_number() over(order by '') , 
            WarehouseNumber,
			StorageType,
            StorageBin,
			"zone",
			StorageSection,
			StorageSectionDescription,
			VerificationField,
            RowStartDate,
            RowIsCurrent
     FROM tmp_ins_storagebin;
						
/*24 Aug 2018  Georgiana changes: unable to get a stable source of rows fix*/
drop table if exists tmp_nodup_LAGP;
create table tmp_nodup_LAGP as 
select distinct LAGP_LGNUM,LAGP_LGPLA,LAGP_LGTYP,LAGP_LPTYP,LAGP_KZLER,LAGP_PLAUF,LAGP_SKZUE,LAGP_SKZUA,LAGP_SPGRU,LAGP_VERIF
from LAGP;
						
update dim_storagebin sb
set sb.storagebintype = ifnull(l.LAGP_LPTYP,'Not Set'),
dw_update_date = current_timestamp
FROM dim_storagebin sb, tmp_nodup_LAGP l
where sb.WarehouseNumber = l.LAGP_LGNUM
AND sb.StorageBin = l.LAGP_LGPLA
and sb.storagetype=l.LAGP_LGTYP
AND sb.storagebintype <> ifnull(l.LAGP_LPTYP,'Not Set');

update dim_storagebin sb
set sb.emptybin = ifnull(l.LAGP_KZLER,'Not Set'),
dw_update_date = current_timestamp
from tmp_nodup_LAGP l,dim_storagebin sb
where sb.WarehouseNumber = l.LAGP_LGNUM
AND sb.StorageBin = l.LAGP_LGPLA
and sb.storagetype=l.LAGP_LGTYP
AND sb.emptybin <> ifnull(l.LAGP_KZLER,'Not Set');

update dim_storagebin sb
set sb.binsection = ifnull(l.LAGP_PLAUF,'Not Set'),
dw_update_date = current_timestamp
from tmp_nodup_LAGP l,dim_storagebin sb
where sb.WarehouseNumber = l.LAGP_LGNUM
AND sb.StorageBin = l.LAGP_LGPLA
and sb.storagetype=l.LAGP_LGTYP
AND sb.binsection <> ifnull(l.LAGP_PLAUF,'Not Set');

update dim_storagebin sb
set sb.BlockingIndicatorPut = ifnull(l.LAGP_SKZUE,'Not Set'),
dw_update_date = current_timestamp
from tmp_nodup_LAGP l,dim_storagebin sb
where sb.WarehouseNumber = l.LAGP_LGNUM
AND sb.StorageBin = l.LAGP_LGPLA
and sb.storagetype=l.LAGP_LGTYP
AND sb.BlockingIndicatorPut <> ifnull(l.LAGP_SKZUE,'Not Set');

update dim_storagebin sb
set sb.BlockingIndicatorRem = ifnull(l.LAGP_SKZUA,'Not Set'),
dw_update_date = current_timestamp
from tmp_nodup_LAGP l,dim_storagebin sb
where sb.WarehouseNumber = l.LAGP_LGNUM
AND sb.StorageBin = l.LAGP_LGPLA
and sb.storagetype=l.LAGP_LGTYP
AND sb.BlockingIndicatorRem <> ifnull(l.LAGP_SKZUA,'Not Set');

update dim_storagebin sb
set sb.BlockingReason = ifnull(l.LAGP_SPGRU,'Not Set'),
dw_update_date = current_timestamp
from tmp_nodup_LAGP l,dim_storagebin sb
where sb.WarehouseNumber = l.LAGP_LGNUM
AND sb.StorageBin = l.LAGP_LGPLA
and sb.storagetype=l.LAGP_LGTYP
AND sb.BlockingReason <> ifnull(l.LAGP_SPGRU,'Not Set');

update dim_storagebin sb
set sb.VerificationField = ifnull(l.LAGP_VERIF,'Not Set'),
dw_update_date = current_timestamp
from tmp_nodup_LAGP l,dim_storagebin sb
where sb.WarehouseNumber = l.LAGP_LGNUM
AND sb.StorageBin = l.LAGP_LGPLA
and sb.storagetype=l.LAGP_LGTYP
AND sb.VerificationField <> ifnull(l.LAGP_VERIF,'Not Set');

drop table if exists tmp_nodup_LAGP;