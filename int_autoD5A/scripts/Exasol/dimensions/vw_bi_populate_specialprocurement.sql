/* 	Server: QA
	Process Name: Special Procurement Transfer
	Interface No: 2
*/

INSERT INTO dim_SpecialProcurement (dim_SpecialProcurementId,                                
                                   Description,
                                   RowStartDate,
                                   RowEndDate,
                                   RowIsCurrent,
                                   RowChangeReason)
   SELECT 1,
	  'Not Set',
          current_timestamp,
          NULL,
          1,
          NULL
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_SpecialProcurement
               WHERE dim_SpecialProcurementid = 1);


UPDATE    Dim_SpecialProcurement sp
   SET sp.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			sp.dw_update_date = current_timestamp
       FROM Dim_SpecialProcurement sp,
          DD07T t
 WHERE sp.RowIsCurrent = 1
    AND  t.DD07T_DOMNAME = 'SOBES'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND sp.SpecialProcurement = t.DD07T_DOMVALUE ;
 
delete from number_fountain m where m.table_name = 'Dim_SpecialProcurement';

insert into number_fountain				   
select 	'Dim_SpecialProcurement',
	ifnull(max(d.Dim_SpecialProcurementID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_SpecialProcurement d
where d.Dim_SpecialProcurementID <> 1;

INSERT INTO Dim_SpecialProcurement(Dim_SpecialProcurementID,
                                Description,
                                SpecialProcurement,
                                RowStartDate,
                                RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_SpecialProcurement') 
          + row_number() over(order by ''),
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'SOBES' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM Dim_SpecialProcurement
                    WHERE SpecialProcurement = DD07T_DOMVALUE
                          AND DD07T_DOMNAME = 'SOBES')
   ORDER BY 2  ;
