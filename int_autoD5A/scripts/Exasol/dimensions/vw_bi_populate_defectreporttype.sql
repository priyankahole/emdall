
INSERT INTO dim_defectreporttype(dim_defectreporttypeid, RowIsCurrent,defectreporttypecode,defectreporttypename,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_defectreporttype
               WHERE dim_defectreporttypeid = 1);

UPDATE    dim_defectreporttype drt
    SET drt.DefectReportTypeName = t.TQ86T_KURZTEXT,
			drt.dw_update_date = current_timestamp
	FROM
          dim_defectreporttype drt, TQ86T t
 WHERE drt.RowIsCurrent = 1
 AND t.TQ86T_FEART = drt.DefectReportTypeCode;
			   
delete from number_fountain m where m.table_name = 'dim_defectreporttype';

insert into number_fountain
select 	'dim_defectreporttype',
	ifnull(max(d.dim_defectreporttypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_defectreporttype d
where d.dim_defectreporttypeid <> 1;			   
			   
INSERT INTO dim_defectreporttype(Dim_DefectReportTypeid,
                                                                                DefectReportTypeCode,
                                          DefectReportTypeName,
                                          RowStartDate,
                                          RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_defectreporttype')
          + row_number() over(order by '') ,
                        t.TQ86T_FEART,
          t.TQ86T_KURZTEXT,
          current_timestamp,
          1
     FROM TQ86T t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_defectreporttype s
               WHERE s.DefectReportTypeCode = t.TQ86T_FEART
                     AND s.RowIsCurrent = 1);

delete from number_fountain m where m.table_name = 'dim_defectreporttype';
