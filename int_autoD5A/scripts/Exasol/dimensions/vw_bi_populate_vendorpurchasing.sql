/* Octavian: As part of Every Angle: dimension based on LFM1 */
insert into dim_vendorpurchasing (dim_vendorpurchasingid)
select 1
from (select 1) a
where not exists ( select 'x' from dim_vendorpurchasing where dim_vendorpurchasingid = 1); 

delete from number_fountain m where m.table_name = 'dim_vendorpurchasing';
insert into number_fountain
select 'dim_vendorpurchasing',
 ifnull(max(d.dim_vendorpurchasingid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_vendorpurchasing d
where d.dim_vendorpurchasingid <> 1;



insert into dim_vendorpurchasing (dim_vendorpurchasingid,
                          vendornumber,
                          purchasingorg,
						  dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_vendorpurchasing') + row_number() over(order by '') AS dim_vendorpurchasingid,
       ifnull(l.LIFNR,'Not Set'),
       ifnull(l.EKORG,'Not Set'),
       current_timestamp
from LFM1 l
 where not exists (select 'x' from dim_vendorpurchasing vp where 
	      vp.vendornumber = ifnull(l.LIFNR,'Not Set')
	  and vp.purchasingorg = ifnull(l.EKORG,'Not Set'));
	  
/*according to APP-6053*/	  
/*update dim_vendorpurchasing vp
set vp.acctno_w_vendor = ifnull(l.LFM1_EIKTO, 'Not Set'),
    vp.dw_update_date = current_timestamp
from LFM1 l, dim_vendorpurchasing vp
where vp.vendornumber = ifnull(l.LIFNR,'Not Set')
and vp.purchasingorg = ifnull(l.EKORG,'Not Set')
AND vp.acctno_w_vendor <> ifnull(l.LFM1_EIKTO, 'Not Set')*/

update dim_vendorpurchasing vp
set vp.purchasinggroup = ifnull(l.LFM1_EKGRP, 'Not Set'),
    vp.dw_update_date = current_timestamp
from LFM1 l, dim_vendorpurchasing vp
where vp.vendornumber = ifnull(l.LIFNR,'Not Set')
and vp.purchasingorg = ifnull(l.EKORG,'Not Set')
AND vp.purchasinggroup <> ifnull(l.LFM1_EKGRP, 'Not Set');

update dim_vendorpurchasing vp
set vp.incoterms1 = ifnull(l.LFM1_INCO1, 'Not Set'),
    vp.dw_update_date = current_timestamp
from LFM1 l, dim_vendorpurchasing vp
where vp.vendornumber = ifnull(l.LIFNR,'Not Set')
and vp.purchasingorg = ifnull(l.EKORG,'Not Set')
AND vp.incoterms1 <> ifnull(l.LFM1_INCO1, 'Not Set');

update dim_vendorpurchasing vp
set vp.incoterms2 = ifnull(l.LFM1_INCO2, 'Not Set'),
    vp.dw_update_date = current_timestamp
from LFM1 l, dim_vendorpurchasing vp
where vp.vendornumber = ifnull(l.LIFNR,'Not Set')
and vp.purchasingorg = ifnull(l.EKORG,'Not Set')
AND vp.incoterms2 <> ifnull(l.LFM1_INCO2, 'Not Set');

update dim_vendorpurchasing vp
set vp.salesperson = ifnull(l.LFM1_VERKF, 'Not Set'),
    vp.dw_update_date = current_timestamp
from LFM1 l, dim_vendorpurchasing vp
where vp.vendornumber = ifnull(l.LIFNR,'Not Set')
and vp.purchasingorg = ifnull(l.EKORG,'Not Set')
AND vp.salesperson <> ifnull(l.LFM1_VERKF, 'Not Set');

update dim_vendorpurchasing vp
set vp.currency = ifnull(l.LFM1_WAERS, 'Not Set'),
    vp.dw_update_date = current_timestamp
from LFM1 l, dim_vendorpurchasing vp
where vp.vendornumber = ifnull(l.LIFNR,'Not Set')
and vp.purchasingorg = ifnull(l.EKORG,'Not Set')
AND vp.currency <> ifnull(l.LFM1_WAERS, 'Not Set');

update dim_vendorpurchasing vp
set vp.terms_of_payment = ifnull(l.LFM1_ZTERM, 'Not Set'),
    vp.dw_update_date = current_timestamp
from LFM1 l, dim_vendorpurchasing vp
where vp.vendornumber = ifnull(l.LIFNR,'Not Set')
and vp.purchasingorg = ifnull(l.EKORG,'Not Set')
and vp.terms_of_payment <> ifnull(l.LFM1_ZTERM,'Not Set');
	