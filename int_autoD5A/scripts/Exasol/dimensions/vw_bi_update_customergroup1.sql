
UPDATE    dim_customergroup1 cg1

   SET cg1.Description = ifnull(TVV1T_BEZEI, 'Not Set')
       FROM
          TVV1T t, dim_customergroup1 cg1
   WHERE t.TVV1T_KVGR1 = cg1.CustomerGroup AND t.TVV1T_SPRAS = 'E';
