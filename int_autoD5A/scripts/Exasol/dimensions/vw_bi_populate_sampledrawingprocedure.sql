insert into dim_sampledrawingprocedure
(dim_sampledrawingprocedureid, 
SampleProcedure, 
VersionNo)

select 	1, 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_sampledrawingprocedure where dim_sampledrawingprocedureid = 1);

delete from number_fountain m where m.table_name = 'dim_sampledrawingprocedure';
insert into number_fountain
select 'dim_sampledrawingprocedure',
 ifnull(max(d.dim_sampledrawingprocedureid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_sampledrawingprocedure d
where d.dim_sampledrawingprocedureid <> 1;

insert into dim_sampledrawingprocedure (dim_sampledrawingprocedureid,
                          SampleProcedure,
                          VersionNo,
     					  PhysicalSample,
     					  ValidFromDate,
						  ConfirmationRequired,
						  SampleProcedureusedintasklist,
						  ShortText,
     					  dw_insert_date)
 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_sampledrawingprocedure') + row_number() over(order by '') AS dim_sampledrawingprocedureid,
        ifnull(q.QPRVK_PRZIEHVERF, 'Not Set') AS SampleProcedure,
        ifnull(q.QPRVK_VERSION, 'Not Set') AS VersionNo,
        ifnull(q.QPRVK_GEBANZ, 'Not Set') AS PhysicalSample,
        q.QPRVK_GUELTIGAB as ValidFromDate,
		ifnull(q.QPRVK_KZQUIT, 'Not Set') as ConfirmationRequired,
		ifnull( q.QPRVK_KZVPRZVPL, 'Not Set') as SampleProcedureusedintasklist,
		ifnull(q.QPRVKT_KURZTEXT, 'Not Set') as ShortText,
       current_timestamp
from QPRVK_QPRVKT q
 where not exists (select 'x' from dim_sampledrawingprocedure ds
                        where ds.SampleProcedure = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set') 
                             AND ds.VersionNo = ifnull(q.QPRVK_VERSION, 'Not Set'));
							  
UPDATE dim_sampledrawingprocedure ds	
SET 	PhysicalSample=ifnull(q.QPRVK_GEBANZ, 'Not Set') 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVK_QPRVKT q,dim_sampledrawingprocedure ds
WHERE ds.SampleProcedure = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set') 
AND ds.VersionNo = ifnull(q.QPRVK_VERSION, 'Not Set')
AND PhysicalSample <> ifnull(q.QPRVK_GEBANZ, 'Not Set');

UPDATE dim_sampledrawingprocedure ds	
SET ds.ValidFromDate = q.QPRVK_GUELTIGAB
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVK_QPRVKT q,dim_sampledrawingprocedure ds
WHERE ds.SampleProcedure = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set') 
AND ds.VersionNo = ifnull(q.QPRVK_VERSION, 'Not Set')
AND ds.ValidFromDate <> q.QPRVK_GUELTIGAB ;

UPDATE dim_sampledrawingprocedure ds	
SET ds.ConfirmationRequired = ifnull(q.QPRVK_KZQUIT, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVK_QPRVKT q,dim_sampledrawingprocedure ds
WHERE ds.SampleProcedure = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set') 
AND ds.VersionNo = ifnull(q.QPRVK_VERSION, 'Not Set')
AND ds.ConfirmationRequired <> ifnull(q.QPRVK_KZQUIT, 'Not Set');

UPDATE dim_sampledrawingprocedure ds	
SET ds.SampleProcedureusedintasklist = ifnull( q.QPRVK_KZVPRZVPL, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVK_QPRVKT q,dim_sampledrawingprocedure ds
WHERE ds.SampleProcedureusedintasklist = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set') 
AND ds.VersionNo = ifnull(q.QPRVK_VERSION, 'Not Set')
AND ds.SampleProcedureusedintasklist <> ifnull( q.QPRVK_KZVPRZVPL, 'Not Set');

UPDATE dim_sampledrawingprocedure ds	
SET ds.ShortText = ifnull(q.QPRVKT_KURZTEXT, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVK_QPRVKT q,dim_sampledrawingprocedure ds	
WHERE ds.SampleProcedureusedintasklist = ifnull(q.QPRVK_PRZIEHVERF, 'Not Set') 
AND ds.VersionNo = ifnull(q.QPRVK_VERSION, 'Not Set')
AND ds.ShortText <> ifnull(q.QPRVKT_KURZTEXT, 'Not Set');

/*Octavian S 15-FEB-2018 APP-6837 Add new attribute*/
--Sample-drawing procedure text
 MERGE INTO dim_sampledrawingprocedure sp
 USING (
        SELECT DISTINCT sp.sampleprocedure,s.shorttext
          FROM (SELECT DISTINCT stxh_tdname,
                       group_concat(TRIM(s.tline_tdline) ORDER BY STXH_TDTXTLINES ASC SEPARATOR ' ') AS shorttext
                  FROM stxh s 
                 WHERE stxh_tdobject LIKE '%QPRV%' 
                   AND stxh_tdid     = 'QPVK'
                 GROUP BY stxh_tdname)      s
                 ,dim_sampledrawingprocedure          sp
         WHERE TRIM(substr(stxh_tdname,4,8)) = sp.sampleprocedure
       )t
   ON t.sampleprocedure  = sp.sampleprocedure
 WHEN MATCHED 
 THEN UPDATE SET dd_longtext_stxh_info_sample_proc = IFNULL(t.shorttext,'Not Set');






							  
					

