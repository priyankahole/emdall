UPDATE    dim_notificationtype nt
     SET nt.NotificationTypeName = t.TQ80_T_QMARTX,
			nt.dw_update_date = current_timestamp
	 FROM
          dim_notificationtype nt, TQ80_T t
 WHERE nt.RowIsCurrent = 1
 AND nt.NotificationTypeCode = t.TQ80_T_QMART
;