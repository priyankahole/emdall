Update companydetailed_d00 t1
set t1.pPeriodDates = t2.pReturn 
from companydetailed_d00 t1
		left join fiscalprd_i00 t2 on    t2.pCompanyCode = t1.pCompanyCode
									 and t2.CalDate = t1.DT
Where    t1.pflag1=1
     AND t1.pCompanyCode <> 'Not Set';

Update companydetailed_d00
set pflag2=1
where  pflag1=1 and pCompanyCode <>  'Not Set' and pPeriodDates is not null;

Update companydetailed_d00
Set FINYEAR=substr(pPeriodDates,1,4)
Where  pflag2=1;

