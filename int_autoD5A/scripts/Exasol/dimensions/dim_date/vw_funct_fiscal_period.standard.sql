Update fiscalprd_i00
set pCalYear =  YEAR(CalDate),
		pCalMth = MONTH(CalDate),
		pCalMthDay = cast(to_char(CalDate,'DD') as integer), 			/* VW:  DAYOFMONTH(CalDate) */
		Period = 0,
		FiscalYear = 0;

Update fiscalprd_i00 t1
set pPeriv = PERIV
From T001, fiscalprd_i00 t1
WHERE BUKRS = pCompanyCode;

Update fiscalprd_i00 t1
set pVariant1 = XJABH ,
    pVariant2 = XKALE
From T009, fiscalprd_i00 t1
Where PERIV = pPeriv;

Update fiscalprd_i00 
set pflag=1 
where pVariant1 = 'X';

Update fiscalprd_i00 
set pflag=2 
Where pVariant1 is null and pVariant2 is null;
 
drop table if exists T009B_i00;
Create table T009B_i00 as
Select POPER, RELJR,PERIV, 
		CASE WHEN MOD(case when BDATJ = 0 then '0001' else BDATJ end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
				THEN TO_DATE(('2-28-' || case when BDATJ = 0 then '0001' else BDATJ end),'MM-DD-YYYY')
			 ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when BDATJ = 0 then '0001' else BDATJ end), 'DD-MM-YYYY') END sdt,
	   row_number () over (order by BDATJ, BUMON, BUTAG) rid
from T009B ;
		/* VW ORIGINAL:	 
			Create table T009B_i00 as
			Select POPER, RELJR,PERIV, STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y') sdt,row_number () over (order by BDATJ, BUMON, BUTAG) rid
			from T009B */

merge into fiscalprd_i00 dest
using (select t1.rowid rid, ifnull(ds.v_rid, 0) as v_rid
	   from fiscalprd_i00 t1
				left join (select min(rid) v_rid , t0.rowid rid
	                       from fiscalprd_i00 t0
									inner join T009B_i00 t2 on t2.PERIV = t0.pPeriv
                           where cast(t0.CalDate as date) <= cast(t2.sdt as date)
						   group by t0.rowid
						  ) ds on t1.rowid = ds.rid
	  ) src on dest.rowid = src.rid
when matched then update set dest.minid = src.v_rid;
		/* VW ORIGINAL:
			Update fiscalprd_i00
			set minid = ifnull(( select min(rid) from  T009B_i00  where PERIV = pPeriv and ansidate(CalDate) <= ansidate(sdt)),0)
			where pflag=1 */

Update fiscalprd_i00 t1
Set Period = ifnull(POPER,0)
from fiscalprd_i00 t1
		left join T009B_i00 t2 on rid = t1.minid
where t1.pflag = 1;

Update fiscalprd_i00 t1
Set pYearShift = ifnull(RELJR,'0')
from fiscalprd_i00 t1
		left join T009B_i00 t2 on rid = t1.minid
where t1.pflag = 1;


drop table if exists T009B_i00;

Update fiscalprd_i00
set FiscalYear = case when pYearShift = '+1' then  (pCalYear + 1)
	              when pYearShift = '-1' then  (pCalYear - 1)
		      else pCalYear
		 end
where pflag=1;
   
drop table if exists T009B_sub009;

create table T009B_sub009 as 
SELECT PERIV,BDATJ,POPER,CASE WHEN MOD(case when BDATJ = 0 then '0001' else BDATJ end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
								THEN TO_DATE(('2-28-' || case when BDATJ = 0 then '0001' else BDATJ end),'MM-DD-YYYY')
							  ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when BDATJ = 0 then '0001' else BDATJ end), 'DD-MM-YYYY') END subdt 
FROM T009B 
group by PERIV,BDATJ,POPER,CASE WHEN MOD(case when BDATJ = 0 then '0001' else BDATJ end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
								  THEN TO_DATE(('2-28-' || case when BDATJ = 0 then '0001' else BDATJ end),'MM-DD-YYYY')
							    ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when BDATJ = 0 then '0001' else BDATJ end), 'DD-MM-YYYY') END 
order by 4 desc;

/* Pick up the max subdt which is less than f.CalDate */
DROP TABLE IF EXISTS tmp_upd_pfromdate_fiscalprd;
CREATE TABLE tmp_upd_pfromdate_fiscalprd
AS
SELECT pPeriv,Period,pCalYear,CalDate,max(cast(subdt as date)) + (INTERVAL '1' DAY) tDT
FROM fiscalprd_i00 f, T009B_sub009 t
WHERE f.pPeriv = t.PERIV AND (f.pCalYear = t.BDATJ OR f.pCalYear = t.BDATJ + 1)
AND ((f.Period > 1 and t.POPER < f.Period) or (f.Period = 1 and t.POPER = 12))
AND cast(t.subdt as date) < cast(f.CalDate as date)
GROUP BY pPeriv,Period,pCalYear,CalDate;

Update fiscalprd_i00 f
SET f.pFromDate = t.tDT
FROM tmp_upd_pfromdate_fiscalprd t,fiscalprd_i00 f
WHERE f.pPeriv = t.pPeriv and f.Period = t.Period and f.pCalYear = t.pCalYear and f.CalDate = t.CalDate
AND pflag=1;



drop table if exists T009B_sub009;
create table T009B_sub009 as 
SELECT t.*,
	   CASE WHEN MOD(case when BDATJ = 0 then '0001' else BDATJ end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
			  THEN TO_DATE(('2-28-' || case when BDATJ = 0 then '0001' else BDATJ end),'MM-DD-YYYY')
		    ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when BDATJ = 0 then '0001' else BDATJ end), 'DD-MM-YYYY') END sdt,
	   row_number() over(order by CASE WHEN MOD(case when BDATJ = 0 then '0001' else BDATJ end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
										THEN TO_DATE(('2-28-' || case when BDATJ = 0 then '0001' else BDATJ end),'MM-DD-YYYY')
									   ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when BDATJ = 0 then '0001' else BDATJ end), 'DD-MM-YYYY') END) rid
FROM T009B t;
		/* VW ORIGINAL:
			create table T009B_sub009 as 
			SELECT *,ansidate(STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y'))  sdt, row_number() over(order by ansidate(STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', BDATJ), '%d/%m/%Y'))) rid
			FROM T009B */

Update fiscalprd_i00 set minid=0;

merge into fiscalprd_i00 dst
using (select t0.rowid rid, min(t1.rid) v_rid
	   from fiscalprd_i00 t0
				left join T009B_sub009 t1 on    t1.PERIV = t0.pPeriv
											and cast(t1.sdt as date)  > cast(t0.CalDate as date) 
											and (t1.BDATJ = pCalYear or t1.BDATJ = t0.pCalYear + 1)
											and ((t0.Period < 12 and t1.POPER > t0.Period) or (t0.Period = 12 and t1.POPER = 1))
	   where t0.pflag = 1
	   group by t0.rowid
	  ) src on dst.rowid = src.rid
when matched then update set dst.minid = src.v_rid;
		/* VW ORIGINAL:
			Update fiscalprd_i00 set minid= (SELECT min(rid)
								FROM T009B_sub009
								WHERE ansidate(sdt)  > ansidate(CalDate) and PERIV = pPeriv and (BDATJ = pCalYear or BDATJ = pCalYear + 1)
									and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1)))
			where pflag=1 */

Update fiscalprd_i00 t1
set pToDate = sdt
from fiscalprd_i00 t1
		left join T009B_sub009 t2 on t2.rid = t1.minid
where t1.pflag = 1  ;

drop table if exists T009B_sub009;
create table T009B_sub009 as
SELECT PERIV,POPER,CASE WHEN MOD(case when BDATJ = 0 then '0001' else BDATJ end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
						  THEN TO_DATE(('2-28-' || case when BDATJ = 0 then '0001' else BDATJ end),'MM-DD-YYYY')
						ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when BDATJ = 0 then '0001' else BDATJ end), 'DD-MM-YYYY') END subdt
FROM T009B group by PERIV,POPER,CASE WHEN MOD(case when BDATJ = 0 then '0001' else BDATJ end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
									   THEN TO_DATE(('2-28-' || case when BDATJ = 0 then '0001' else BDATJ end),'MM-DD-YYYY')
									 ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when BDATJ = 0 then '0001' else BDATJ end), 'DD-MM-YYYY') END 
order by subdt desc;

DROP TABLE IF EXISTS tmp_upd_ptodate_fiscalprd;
CREATE TABLE tmp_upd_ptodate_fiscalprd
AS
SELECT pPeriv,Period,pToDate,max(cast(subdt as date)) tDT
FROM fiscalprd_i00 f, T009B_sub009 t
WHERE f.pPeriv = t.PERIV AND f.Period = t.POPER
AND cast(t.subdt as date) < cast(f.pToDate as date)
GROUP BY pPeriv,Period,pToDate;

Update fiscalprd_i00 f
SET f.pToDate = t.tDT
FROM tmp_upd_ptodate_fiscalprd t,fiscalprd_i00 f
WHERE f.pPeriv = t.pPeriv AND f.Period = t.Period AND f.pToDate = t.pToDate
AND pflag=1;
    
drop table if exists T009B_sub009;
drop table if exists T009B_i00;

Create table T009B_i00 as
Select t.*,row_number() over(ORDER BY BUMON, BUTAG) rid
FROM T009B t;

Update fiscalprd_i00 set minid = 0;

merge into fiscalprd_i00 dst
using (select t0.rowid rid, ifnull(min(t1.rid), 0) v_rid
	   from fiscalprd_i00 t0
				left join T009B_i00 t1 on    t1.PERIV = t0.pPeriv
										 and BDATJ = 0
										 and  cast(CalDate as date) <= CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
																			  THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear end),'MM-DD-YYYY')
																			ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear end), 'DD-MM-YYYY') END
	   where t0.pflag = 2
	   group by t0.rowid
	  ) src on dst.rowid = src.rid
when matched then update set dst.minid = src.v_rid
where dst.minid <> src.v_rid;
		/* VW ORIGINAL:
			Update fiscalprd_i00 
			set minid =ifnull((SELECT min(rid) FROM T009B_i00
									WHERE PERIV = pPeriv and BDATJ = 0
										and ansidate(CalDate) <= ansidate((case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
														  then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
														  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
														end))),0)
			where pflag =2 */

Update fiscalprd_i00 t1
set Period = ifnull(POPER, 0)
from fiscalprd_i00 t1
		left join T009B_i00 t2 on t2.rid = t1.minid
where t1.pflag = 2;

Update fiscalprd_i00 t1
set pYearShift = ifnull(RELJR, '0')
from fiscalprd_i00 t1
		left join T009B_i00 t2 on t2.rid = t1.minid
where t1.pflag = 2;
   
update fiscalprd_i00
set FiscalYear = case when pYearShift = '+1' then (pCalYear + 1)
		      when pYearShift = '-1' then (pCalYear - 1)
		      else pCalYear
		 end
where pflag=2;


drop table if exists T009B_i00;
create table T009B_i00 as 
SELECT t0.iid,
		CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
			   THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear end),'MM-DD-YYYY') + ( INTERVAL '1' DAY)
			 ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear end), 'DD-MM-YYYY') + ( INTERVAL '1' DAY) END tdt
FROM fiscalprd_i00 t0
		inner join T009B t1 on    PERIV = pPeriv and BDATJ = 0
							  and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
							  and cast(CalDate as date) > CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
																 THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear end),'MM-DD-YYYY')
															   ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear end), 'DD-MM-YYYY') END
where pflag = 2;
		/* VW ORIGINAL:			
			create table T009B_i00 as SELECT iid,case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
										  then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y') + ( INTERVAL '1' DAY)
										  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y') + ( INTERVAL '1' DAY)
										end tdt
								FROM T009B,fiscalprd_i00
								WHERE PERIV = pPeriv  and BDATJ = 0
									and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
									and ansidate(CalDate) > ansidate(case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
													  then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
													  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
													end)
						and pflag=2 */

drop table if exists tb_max_o23;
create table tb_max_o23 as 
Select iid,max(tdt) maxtdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00 t0
set pFromDate = maxtdt 
from tb_max_o23 t, fiscalprd_i00 t0
Where t0.pflag = 2 and t.iid = t0.iid;
   
drop table if exists T009B_i00;
drop table if exists tb_max_o23;

create table T009B_i00 as 
SELECT t0.iid,
		CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear-1 end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
			   THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear-1 end),'MM-DD-YYYY') + ( INTERVAL '1' DAY)
			 ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear-1 end), 'DD-MM-YYYY') + ( INTERVAL '1' DAY) END tdt
FROM fiscalprd_i00 t0
		inner join T009B t1 on    PERIV = pPeriv and BDATJ = 0
							  and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
							  and cast(CalDate as date) > CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear-1 end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
																 THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear-1 end),'MM-DD-YYYY')
															   ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear-1 end), 'DD-MM-YYYY') END
where pflag = 2 and pFromDate is null;
		/* VW ORIGINAL:	 
			create table T009B_i00 as SELECT iid,case when MOD(pCalYear-1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
										  then STR_TO_DATE(concat('28/2/', pCalYear-1), '%d/%m/%Y') + ( INTERVAL '1' DAY)
										  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear-1),  '%d/%m/%Y') + ( INTERVAL '1' DAY)
										end tDT
								FROM T009B,fiscalprd_i00
								WHERE PERIV = pPeriv  and BDATJ = 0
									and ((Period > 1 and POPER < Period) or (Period = 1 and POPER = 12))
									and ansidate(CalDate) > ansidate(case when MOD(pCalYear-1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
													  then STR_TO_DATE(concat('28/2/', pCalYear-1), '%d/%m/%Y')
													  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear-1), '%d/%m/%Y')
													end)
						and pflag=2 and pFromDate is null */

drop table if exists tb_max_o23;
create table tb_max_o23 as 
Select iid,max(tdt) maxtdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00 t0
set pFromDate = maxtdt 
from tb_max_o23 t, fiscalprd_i00 t0
Where  pflag = 2 and pFromDate is null and t.iid = t0.iid;

drop table if exists tb_max_o23;

drop table if exists T009B_i00;
create table T009B_i00 as 
SELECT t0.iid,
		CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
			   THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear end),'MM-DD-YYYY')
			 ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear end), 'DD-MM-YYYY') END tdt
FROM fiscalprd_i00 t0
		inner join T009B t1 on    PERIV = pPeriv and BDATJ = 0
							  and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
							  and cast(CalDate as date) > CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
																 THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear end),'MM-DD-YYYY')
															   ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear end), 'DD-MM-YYYY') END
where pflag = 2;
		/* VW ORIGINAL:	 
			create table T009B_i00 as SELECT iid,case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
										  then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
										  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
										end tDT
								FROM T009B,fiscalprd_i00
								WHERE PERIV = pPeriv and BDATJ = 0
									and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
									and ansidate(CalDate) > ansidate(case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
													  then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
													  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
													end)
						and  pflag=2  */

drop table if exists tb_max_o23;
create table tb_max_o23 as 
Select iid,min(tdt) mintdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00 t0
set pToDate1 = mintdt
from tb_max_o23 t, fiscalprd_i00 t0
Where pflag = 2 and t.iid = t0.iid;

drop table if exists tb_max_o23;

drop table if exists T009B_i00;
create table T009B_i00 as 
SELECT t0.iid,
		CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear+1 end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
			   THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear+1 end),'MM-DD-YYYY')
			 ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear+1 end), 'DD-MM-YYYY') END tdt
FROM fiscalprd_i00 t0
		inner join T009B t1 on    PERIV = pPeriv and BDATJ = 0
							  and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
							  and cast(CalDate as date) < CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear+1 end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
																 THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear+1 end),'MM-DD-YYYY')
															   ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear+1 end), 'DD-MM-YYYY') END
where pflag = 2 and pToDate1 is null;
		/* VW ORIGINAL:	
			create table T009B_i00 as SELECT iid,case when MOD(pCalYear+1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
										  then STR_TO_DATE(concat('28/2/', pCalYear+1), '%d/%m/%Y')
										  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear+1), '%d/%m/%Y')
										end tDT
								FROM T009B,fiscalprd_i00
								WHERE PERIV = pPeriv and BDATJ = 0
									and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
									and ansidate(CalDate) < ansidate(case when MOD(pCalYear+1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
													  then STR_TO_DATE(concat('28/2/', pCalYear+1), '%d/%m/%Y')
													  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear+1), '%d/%m/%Y')
													end)
						and pflag=2  and pToDate1 is null*/ 

drop table if exists tb_max_o23;

create table tb_max_o23 as 
Select iid,min(tdt) mintdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00 t0
set pToDate1 = mintdt
from tb_max_o23 t, fiscalprd_i00 t0
Where pflag=2 and pToDate1 is null and t.iid=t0.iid;

drop table if exists tb_max_o23;

drop table if exists T009B_i00;
create table T009B_i00 as 
SELECT t0.iid,
		CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear+1 end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
			   THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear+1 end),'MM-DD-YYYY')
			 ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear+1 end), 'DD-MM-YYYY') END tdt
FROM fiscalprd_i00 t0
		inner join T009B t1 on    PERIV = pPeriv and BDATJ = 0 and POPER = Period
							  and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
							  and cast(pToDate1 as date) > CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear+1 end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
																 THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear+1 end),'MM-DD-YYYY')
															   ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear+1 end), 'DD-MM-YYYY') END
where pflag = 2;
		/* VW ORIGINAL:	
			create table T009B_i00 as  SELECT iid,case when MOD(pCalYear+1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
										  then STR_TO_DATE(concat('28/2/', pCalYear+1), '%d/%m/%Y')
										  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear+1), '%d/%m/%Y')
										end tDT
								FROM T009B,fiscalprd_i00
								WHERE PERIV = pPeriv and BDATJ = 0 and POPER = Period
									and ((Period < 12 and POPER > Period) or (Period = 12 and POPER = 1))
									and ansidate(pToDate1) > ansidate(case when MOD(pCalYear+1,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
													  then STR_TO_DATE(concat('28/2/', pCalYear+1), '%d/%m/%Y')
													  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear+1), '%d/%m/%Y')
													end)
					and pflag=2 */

drop table if exists tb_max_o23;
create table tb_max_o23 as  
Select iid,max(tdt) maxtdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00 t0
Set pToDate = maxtdt
from tb_max_o23 t, fiscalprd_i00 t0
where pflag=2 and t.iid=t0.iid;

drop table if exists tb_max_o23;
    
drop table if exists T009B_i00;
create table T009B_i00 as 
SELECT t0.iid,
		CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
			   THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear end),'MM-DD-YYYY')
			 ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear end), 'DD-MM-YYYY') END tdt
FROM fiscalprd_i00 t0
		inner join T009B t1 on    PERIV = pPeriv and BDATJ = 0 and POPER = Period
							  and cast(pToDate1 as date) > CASE WHEN MOD(case when pCalYear = 0 then '0001' else pCalYear end,4) <> 0 AND concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0)) = '29-02'
																 THEN TO_DATE(('2-28-' || case when pCalYear = 0 then '0001' else pCalYear end),'MM-DD-YYYY')
															   ELSE TO_DATE(concat(lpad(BUTAG,2,'0'), '-', lpad(BUMON,2,0), '-', case when pCalYear = 0 then '0001' else pCalYear end), 'DD-MM-YYYY') END
where pflag = 2 and pToDate is null;
		/* VW ORIGINAL:	
			create table T009B_i00 as  SELECT iid,case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
										  then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
										  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
										end tDT
								FROM T009B,fiscalprd_i00
								WHERE PERIV = pPeriv and BDATJ = 0 and POPER = Period
									and ansidate(pToDate1) > ansidate(case when MOD(pCalYear,4) <> 0 AND concat(BUTAG, '/', BUMON) = '29/2'
													  then STR_TO_DATE(concat('28/2/', pCalYear), '%d/%m/%Y')
													  else STR_TO_DATE(concat(BUTAG, '/', BUMON, '/', pCalYear), '%d/%m/%Y')
													end)
					and  pflag=2 and pToDate is null */

drop table if exists tb_max_o23;

create  table tb_max_o23 as 
Select iid,max(tdt) maxtdt 
from T009B_i00 
group by iid;

Update fiscalprd_i00 t0
Set pToDate = maxtdt
from tb_max_o23 t, fiscalprd_i00 t0
where pflag=2 and pToDate is null and t.iid=t0.iid;
   
drop table if exists tb_max_o23; 
  
Update fiscalprd_i00 set Period = pCalMth where pflag=0;  
Update fiscalprd_i00 set FiscalYear = pCalYear where pflag=0;
Update fiscalprd_i00 set pFromDate = ( TO_DATE(concat('01/', pCalMth, '/', pCalYear), 'DD/MM/YYYY')) where pflag=0;    

Update fiscalprd_i00 set pToDate = add_months(trunc(pFromDate,'MONTH'),1)-1 where pflag=0;
		/* VW ORIGINAL:	
			Update fiscalprd_i00 set pToDate = (Last_Day(pFromDate)) where pflag=0  */
    
Update fiscalprd_i00 set pReturn = (concat(FiscalYear,'|',RIGHT(concat('00',Period),2),'|',to_char(pFromDate, 'YYYY-MM-DD HH24:MI:SS'), '|', to_char(pToDate, 'YYYY-MM-DD HH24:MI:SS')));
		/* VW ORIGINAL:	
			Update fiscalprd_i00 set pReturn = (concat(FiscalYear,'|',RIGHT(concat('00',Period),2),'|',cast(pFromDate as varchar(20)), '|', cast(pToDate as varchar(20)))) */

DROP TABLE IF EXISTS tmp_upd_pfromdate_fiscalprd;
DROP TABLE IF EXISTS tmp_upd_ptodate_fiscalprd;
