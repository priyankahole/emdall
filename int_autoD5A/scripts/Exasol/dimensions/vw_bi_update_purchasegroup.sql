UPDATE    dim_purchasegroup pg

   SET pg.Description = ifnull(T024_EKNAM, 'Not Set')
       FROM
          T024 t, dim_purchasegroup pg
   WHERE t.T024_EKGRP = pg.PurchaseGroup AND pg.RowIsCurrent = 1
;
