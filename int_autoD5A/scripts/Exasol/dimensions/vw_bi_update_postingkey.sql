UPDATE    dim_postingkey pk
   SET pk.Name = t.TBSLT_LTEXT,
			pk.dw_update_date = current_timestamp 
   FROM
          dim_postingkey pk, TBSLT t
 WHERE pk.RowIsCurrent = 1
   AND t.TBSLT_SPRAS = 'E'
          AND pk.PostingKey = t.TBSLT_BSCHL
          AND pk.SpecialGLIndicator = ifnull(t.TBSLT_UMSKZ, 'Not Set')   
;
