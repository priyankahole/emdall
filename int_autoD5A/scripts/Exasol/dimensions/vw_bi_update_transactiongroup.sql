UPDATE    dim_transactiongroup tg

   SET tg.Description = ifnull(DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T t,  dim_transactiongroup tg
   WHERE     t.DD07T_DOMNAME = 'TRVOG'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND tg.TransactionGroup = t.DD07T_DOMVALUE
;