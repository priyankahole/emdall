/* 	Server: QA
	Process Name: WM Blocking Reason Transfer
	Interface No: 2
*/


INSERT INTO dim_wmblockingreason
(dim_wmblockingreasonid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmblockingreason where dim_wmblockingreasonid = 1);

UPDATE    dim_wmblockingreason wmbr
SET  wmbr.WMBlockingReasonDescription = t.T330T_SPGTX,
			wmbr.dw_update_date = current_timestamp
FROM T330T t, dim_wmblockingreason wmbr
 WHERE wmbr.RowIsCurrent = 1
     AND wmbr.WMBlockingReason = t.T330T_SPGRU
	   AND wmbr.WarehouseNumber = t.T330T_LGNUM;
 
delete from number_fountain m where m.table_name = 'dim_wmblockingreason';

insert into number_fountain				   
select 	'dim_wmblockingreason',
	ifnull(max(d.dim_wmblockingreasonid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmblockingreason d
where d.dim_wmblockingreasonid <> 1;

INSERT INTO dim_wmblockingreason(dim_wmblockingreasonid,
							WarehouseNumber,
							WMBlockingReason,
                            WMBlockingReasonDescription,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmblockingreason') + row_number() over(order by ''),
		  t.T330T_LGNUM,
          t.T330T_SPGRU,
		  t.T330T_SPGTX,
          current_timestamp,
          1
     FROM T330T t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_wmblockingreason s
                  WHERE    s.WMBlockingReason = t.T330T_SPGRU
					    AND s.WarehouseNumber = t.T330T_LGNUM
                        AND s.RowIsCurrent = 1) ;
