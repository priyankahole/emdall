
/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_countrytaxclassif.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 12 Sep 2016 */
/*  */
/*  */
/*   Description    : Populate Country for tax classification dimension. BI-3416 */
/*  				  Grain: MaterialNumber, Plant, DepartureCountry, SalesOrganization, DistributionChannel*/ 
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */


insert into dim_countrytaxclassif (dim_countrytaxclassifid, MaterialNumber, DepartureCountry, TaxClassificationMaterial)
select 1, 'Not Set', 'Not Set', 'Not Set'
from ( select 1) a
where not exists ( select 'x' from dim_countrytaxclassif where dim_countrytaxclassifid = 1);

delete from number_fountain m where m.table_name = 'dim_countrytaxclassif';

INSERT INTO number_fountain
select 	'dim_countrytaxclassif',
	ifnull(max(d.dim_countrytaxclassifid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM dim_countrytaxclassif d
WHERE d.dim_countrytaxclassifid <> 1; 

insert into dim_countrytaxclassif
(
	dim_countrytaxclassifid,
	MaterialNumber,
	DepartureCountry,
	TaxClassificationMaterial,
	CreatedOn,
	DateofLastChange,
	Plant,
	SalesOrganization,
	DistributionChannel
)
select 
    (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_countrytaxclassif') + row_number() over(order by '') AS dim_countrytaxclassifid,
	ifnull(MLAN_MATNR, 'Not Set') as MaterialNumber, 
    ifnull(MLAN_ALAND, 'Not Set') as DepartureCountry, 
    ifnull(MLAN_TAXM1, 'Not Set') as TaxClassificationMaterial, 
    ifnull(MARA_ERSDA, '1900-01-01') as CreatedOn, 
    ifnull(MARA_LAEDA, '1900-01-01') as DateofLastChange, 
    ifnull(MARC_WERKS, 'Not Set') as Plant, 
    ifnull(TVKWZ_VKORG, 'Not Set') as SalesOrganization,
    ifnull(TVKWZ_VTWEG, 'Not Set') as DistributionChannel
from TVKWZ_T001W_MLAN t
where not exists (select 1 from dim_countrytaxclassif ctc
			WHERE ctc.MaterialNumber = ifnull(t.MLAN_MATNR, 'Not Set')
			AND ctc.DepartureCountry = ifnull(MLAN_ALAND, 'Not Set')
			AND ctc.Plant = ifnull(MARC_WERKS, 'Not Set')
			and ctc.SalesOrganization = ifnull(TVKWZ_VKORG, 'Not Set')
			and ctc.DistributionChannel = ifnull(TVKWZ_VTWEG, 'Not Set')
);

update dim_countrytaxclassif ctc
set ctc.TaxClassificationMaterial = ifnull(MLAN_TAXM1, 'Not Set')
from dim_countrytaxclassif ctc, TVKWZ_T001W_MLAN t
where ifnull(MLAN_MATNR, 'Not Set') = ctc.MaterialNumber
    and ifnull(MLAN_ALAND, 'Not Set') = ctc.DepartureCountry
	and ifnull(MARC_WERKS, 'Not Set') = ctc.Plant
    and ifnull(TVKWZ_VKORG, 'Not Set') = ctc.SalesOrganization
    and ifnull(TVKWZ_VTWEG, 'Not Set') = ctc.DistributionChannel
	and ctc.TaxClassificationMaterial <> ifnull(MLAN_TAXM1, 'Not Set');

/* Andrei R APP-7189 */
update dim_countrytaxclassif ctc
set ctc.Description = ifnull(TSKMT_VTEXT, 'Not Set')
from dim_countrytaxclassif ctc, TSKMT k
where  ifnull(TSKMT_TAXKM, 'Not Set') = ctc.TaxClassificationMaterial
    and TSKMT_TATYP = 'MWST'
	and ctc.Description <> ifnull(TSKMT_VTEXT, 'Not Set');
/* END Andrei R APP-7189 */