UPDATE    dim_tasklistusage u
      SET u.UsageName = t.T411T_TXT,
			u.dw_update_date = current_timestamp
			FROM
          dim_tasklistusage u,T411T t
 WHERE u.RowIsCurrent = 1
 AND  u.UsageCode = t.T411T_VERWE
;