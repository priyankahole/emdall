UPDATE    dim_casetype ds

   SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set')
       FROM
          SCMGCASETYPET dt,  dim_casetype ds
 WHERE ds.RowIsCurrent = 1
          AND ds.CaseType = dt.CASE_TYPE
          AND dt.CASE_TYPE IS NOT NULL
;