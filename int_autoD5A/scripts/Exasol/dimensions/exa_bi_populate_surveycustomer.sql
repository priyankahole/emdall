/* ################################################################################################################## */
/* */
/*   Script         : exa_bi_populate_surveycustomer */
/*   Author         : Andrei R */
/*   Created On     : 10 Mar 2017 */
/*  */
/* ################################################################################################################## */

/* DELETE FROM SURVEYCUSTOMER


IMPORT INTO SURVEYCUSTOMER FROM CSV
AT DC1STGC1INT01CONNECTION
FILE '/home/fusionops/ispring_clustered_storage/configurations/MerckDE6/files/CustomerSurvey/CUSTOMER.csv'
COLUMN SEPARATOR = ','
ENCODING = 'ISO-8859-1'
SKIP = 1 */



INSERT INTO DIM_SURVEYCUSTOMER (dim_surveycustomerid)
select 1
from (select 1) a
where not exists (select '1' from dim_surveycustomer where dim_surveycustomerid = 1);

DELETE FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_surveycustomer';
INSERT INTO NUMBER_FOUNTAIN 
SELECT 'dim_surveycustomer', ifnull(max(dim_surveycustomerid),1)
FROM DIM_SURVEYCUSTOMER;


INSERT INTO DIM_SURVEYCUSTOMER (dim_surveycustomerid,CustomerCode,CustomerDescription)
SELECT(SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'dim_surveycustomer') + row_number() over(order by '') as DIM_SURVEYCUSTOMERID,
customercode, customerdescription from surveycustomer S WHERE NOT EXISTS(SELECT 1  FROM dim_surveycustomer ds where s.customercode = ds.customercode) ; 

UPDATE DIM_SURVEYCUSTOMER DS 
SET DS.CUSTOMERDESCRIPTION = S.CUSTOMERDESCRIPTION, 
DW_UPDATE_DATE = CURRENT_TIMESTAMP
FROM SURVEYCUSTOMER S, DIM_SURVEYCUSTOMER DS
WHERE DS.CUSTOMERCODE = S.CUSTOMERCODE AND 
DS.CUSTOMERDESCRIPTION <> S.CUSTOMERDESCRIPTION;
