UPDATE    dim_inspectionsamplestatus s
   SET s.description = t.DD07T_DDTEXT,
			s.dw_update_date = current_timestamp
	FROM
         dim_inspectionsamplestatus s, DD07T t
 WHERE s.RowIsCurrent = 1
       AND    t.DD07T_DOMNAME = 'LVS_STIKZ'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND s.statuscode = t.DD07T_DOMVALUE;
		  
INSERT INTO dim_inspectionsamplestatus(dim_inspectionsamplestatusid, RowIsCurrent,statuscode,description,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_inspectionsamplestatus
               WHERE dim_inspectionsamplestatusid = 1);

delete from number_fountain m where m.table_name = 'dim_inspectionsamplestatus';
   
insert into number_fountain
select 	'dim_inspectionsamplestatus',
	ifnull(max(d.dim_inspectionsamplestatusid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_inspectionsamplestatus d
where d.dim_inspectionsamplestatusid <> 1; 
			   
INSERT INTO dim_inspectionsamplestatus(dim_inspectionsamplestatusid,
                                       statuscode,
                                       description,
                                       RowStartDate,
                                       RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_inspectionsamplestatus')
          + row_number() over(order by '') ,
                t.DD07T_DOMVALUE,
          t.DD07T_DDTEXT,
          current_timestamp,
          1
     FROM DD07T t
    WHERE t.DD07T_DOMNAME = 'LVS_STIKZ' AND t.DD07T_DOMVALUE IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_inspectionsamplestatus s
                  WHERE     s.statuscode = t.DD07T_DOMVALUE
                        AND t.DD07T_DOMNAME = 'LVS_STIKZ'
                        AND s.RowIsCurrent = 1);
