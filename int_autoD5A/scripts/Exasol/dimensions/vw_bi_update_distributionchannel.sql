UPDATE    dim_distributionchannel dc

   SET dc.DistributionChannelName = t.TVTWT_VTEXT
       FROM
          TVTWT t,  dim_distributionchannel dc
 WHERE dc.RowIsCurrent = 1
 AND t.TVTWT_VTWEG = dc.DistributionChannelCode;
