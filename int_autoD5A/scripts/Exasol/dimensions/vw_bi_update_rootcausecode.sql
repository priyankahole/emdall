UPDATE    dim_rootcausecode ds

   SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set')
       FROM
          UDMATTR_RCCODET dt,  dim_rootcausecode ds
 WHERE ds.RowIsCurrent = 1
          AND ds.RootCCode = dt.ROOT_CCODE
          AND dt.ROOT_CCODE IS NOT NULL
;
