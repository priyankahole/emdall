UPDATE    dim_statusprofile p

   SET p.StatusProfileName = t.TJ20T_TXT
       FROM
          TJ20T t,dim_statusprofile p
 WHERE p.RowIsCurrent = 1
 AND p.StatusProfileCode = t.TJ20T_STSMA
;
