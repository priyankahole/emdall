/*This dimension is based on a file which Merck is placing on SFTP path with the targets and some achieved values*/
/*There are some achieved values that are comming from different reports and are calculated above*/
/*APP-7981 Ocatavian S - 30-MAR-2018 - Multiple SA's - Add Editable Fields and Configure Ranges of Fields*/

/*APP-9263 - Octavian S - Added lastimported to filter the reports after last imported date*/
update dim_misc_targets
set lastimported='No';

insert into DIM_MISC_TARGETS
(calENDarID,
type,
target,
achieved)
select
calENDarID,
Type ,
target,
achieved
from
 MISC_TARGETS
 where not exists (select 1 from DIM_MISC_TARGETS where MISC_TARGETS.calENDarid=DIM_MISC_TARGETS.calENDarID and MISC_TARGETS.Type=DIM_MISC_TARGETS.Type);

/*Start Calculating the Achieved values from Reports*/

DROP TABLE IF EXISTS tmp_for_AverageDaysPerDollar;
CREATE TABLE tmp_for_AverageDaysPerDollar AS
SELECT 'TOTAL' Col_ord_0,resultset.Col_ord_3 AS AverageDaysPerDollar
  FROM ( SELECT rowcnt_0.*
           FROM ( SELECT 1,
                         ROUND(count(DISTINCT f_ap.dd_AccountingDocNo),0) Col_ord_2,
                         ROUND((sum(-1*(f_ap.amt_InLocalCurrency * amt_exchangerate_gbl/(CASE WHEN amt_exchangerate = 0 THEN 1 ELSE amt_exchangerate END))
                               *(CASE WHEN f_ap.Dim_DateIdCreated >1 AND f_ap.Dim_DateIdClearing > 1
                                      THEN (apcd.DateValue - cdd.DateValue)
                                      ELSE 0.0000
                                 END))/
                                 CASE WHEN sum(-1*f_ap.amt_InLocalCurrency)=0
                                      THEN 1.00
                                      ELSE sum(-1*(f_ap.amt_InLocalCurrency * amt_exchangerate_gbl/
                                           (CASE WHEN amt_exchangerate = 0 THEN 1 ELSE amt_exchangerate END) ))
                                 END ),2) Col_ord_3,
                          ROUND(SUM((-1*f_ap.amt_InLocalCurrency / IFNULL((CASE WHEN amt_exchangerate = 0 THEN 1 ELSE amt_exchangerate END),1)) * f_ap.amt_ExchangeRate_GBL),2) Col_ord_4
                   FROM fact_accountspayable AS f_ap
                  INNER JOIN Dim_Date AS apcd
                          ON f_ap.Dim_DateIdClearing = apcd.Dim_Dateid
                         AND apcd.calENDarmonthid BETWEEN  concat(year(current_date),'01') AND
                            (CASE WHEN month(current_date)=1
                                  THEN month(current_date)
                                  ELSE concat(year(current_date),CASE WHEN length(month(current_date)-1)=1
                                                                      THEN concat('0',month(current_date)-1)
                                                                      ELSE month(current_date)-1
                                                                 END)
                             END)
                 INNER JOIN dim_documenttypetext AS dttap ON f_ap.Dim_DocumentTypeId = dttap.dim_documenttypetextid  AND (lower(dttap.Type) != lower('ZP'))
                 INNER JOIN Dim_VENDor AS apv ON f_ap.Dim_VENDorId = apv.Dim_VENDorid  AND ((TRIM(LEADING '0' FROM apv.VENDorNumber)) NOT IN (('3038892'),('3078180')) )
                 INNER JOIN dim_term AS cpt ON f_ap.Dim_CustomerPaymentTermsid = cpt.dim_termid  AND (lower(cpt.TermCode) != lower('Not Set'))
                 INNER JOIN Dim_Date AS cdd ON f_ap.Dim_DateIdCreated = cdd.Dim_Dateid
                 WHERE (lower(f_ap.dd_clearingdocumentno) LIKE lower('2%'))
                   AND (lower(CASE WHEN LEFT(TRIM(LEADING '0' FROM apv.VENDorNumber) ,1)='3' THEN 'Y' ELSE'N' END) = lower('Y'))
         ) rowcnt_0) resultset ;

UPDATE dim_misc_targets d
   SET d.achieved = t.AverageDaysPerDollar
  FROM DIM_MISC_TARGETS d ,tmp_for_AverageDaysPerDollar t
 WHERE concat(year(calENDarid), (CASE WHEN length (month(calENDarid))=1
                                      THEN concat('0',month(calENDarid))
                                      ELSE month(calENDarid)
                                 END ))=
                                (CASE WHEN month(current_date)=1
                                      THEN month(current_date)
                                      ELSE concat(year(current_date),CASE WHEN length(month(current_date)-1)=1
                                                                          THEN concat('0',month(current_date)-1)
                                                                          ELSE month(current_date)-1
                                                                    END)
                                END)
   AND TYPE='Average Days Per Dollar - Target (Edit Here)';


   DROP TABLE IF EXISTS tmp_for_PullThrough;
   CREATE TABLE tmp_for_PullThrough as
   SELECT 'TOTAL' Col_ord_0,resultset.Col_ord_3 as PullThrough
     FROM ( SELECT rowcnt_0.*
              FROM ( SELECT 1,
                            ROUND(count(distinct f_ap.dd_AccountingDocNo),0) Col_ord_1,
                            ROUND((sum(-1*(f_ap.amt_InLocalCurrency * amt_exchangerate_gbl/(CASE WHEN amt_exchangerate = 0 THEN 1 ELSE amt_exchangerate END))
                                *(CASE WHEN f_ap.Dim_DateIdCreated >1 AND f_ap.Dim_DateIdClearing > 1 THEN (apcd.DateValue - cdd.DateValue) ELSE 0.0000 END))/
                                       CASE WHEN sum(-1*f_ap.amt_InLocalCurrency)=0 THEN 1.00 ELSE
                                   sum(-1*(f_ap.amt_InLocalCurrency * amt_exchangerate_gbl/(case when amt_exchangerate = 0 THEN 1 ELSE amt_exchangerate END) )) END ),2) Col_ord_2,
                            ROUND(SUM((-1*f_ap.amt_InLocalCurrency / ifnull((case when amt_exchangerate = 0 THEN 1 ELSE amt_exchangerate END),1)) * f_ap.amt_ExchangeRate_GBL),2) Col_ord_3
   FROM fact_accountspayable AS f_ap
   INNER JOIN Dim_Date AS apcd
           ON f_ap.Dim_DateIdClearing = apcd.Dim_Dateid
          AND apcd.calENDarmonthid BETWEEN  concat(year(current_date),'01') AND
             (CASE WHEN month(current_date)=1
                   THEN month(current_date)
                   ELSE concat(year(current_date),CASE WHEN length(month(current_date)-1)=1
                                                       THEN concat('0',month(current_date)-1)
                                                       ELSE month(current_date)-1
                                                  END)
              END)
   INNER JOIN dim_documenttypetext AS dttap ON f_ap.Dim_DocumentTypeId = dttap.dim_documenttypetextid  AND (lower(dttap.Type) != lower('ZP'))
   INNER JOIN Dim_VENDor AS apv ON f_ap.Dim_VENDorId = apv.Dim_VENDorid  AND ((TRIM(LEADING '0' FROM apv.VENDorNumber)) NOT IN (('3038892'),('3078180')) )
   INNER JOIN dim_term AS cpt ON f_ap.Dim_CustomerPaymentTermsid = cpt.dim_termid  AND (lower(cpt.termbucket_merck) = lower('90'))  AND (lower(cpt.TermCode) != lower('Not Set'))
   INNER JOIN Dim_Date AS cdd ON f_ap.Dim_DateIdCreated = cdd.Dim_Dateid
   WHERE (lower(f_ap.dd_clearingdocumentno) LIKE lower('2%'))
    AND (lower(CASE WHEN LEFT(TRIM(LEADING '0' FROM apv.VENDorNumber) ,1)='3' THEN 'Y' ELSE'N' END) = lower('Y'))
    ) rowcnt_0) resultset ;


UPDATE DIM_MISC_TARGETS d
   SET d.achieved = t.PullThrough
  FROM DIM_MISC_TARGETS d ,tmp_for_PullThrough t
 WHERE concat(year(calENDarid), (CASE WHEN length (month(calENDarid))=1
                                     THEN concat('0',month(calENDarid))
                                     ELSE month(calENDarid)
                                END ))=
                               (CASE WHEN month(current_date)=1
                                     THEN month(current_date)
                                     ELSE concat(year(current_date),CASE WHEN length(month(current_date)-1)=1
                                                                         THEN concat('0',month(current_date)-1)
                                                                         ELSE month(current_date)-1
                                                                   END)
                               END)
   AND TYPE='Pull Through - Target';
