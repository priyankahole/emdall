
UPDATE 	dim_codeText d
SET 	d.Description = s.QPCT_KURZTEXT,
			d.dw_update_date = current_timestamp
	from dim_codeText d, QPCT s
WHERE 	d."catalog" = s.QPCT_KATALOGART
		AND d.CodeGroup = s.QPCT_CODEGRUPPE
		AND d.Code = s.QPCT_CODE
		AND 'Version' = s.QPCT_VERSION
		AND d.RowIsCurrent = 1;

