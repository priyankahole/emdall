UPDATE    dim_reasoncode drc

   SET drc.ReasonCode = ifnull(rc.Reason_mnemonic,'Not Set'),
       drc.Description = ifnull(rc.Description,'Not Set'),
       drc.CodeType = ifnull(rc.Code_type,'Not Set')
       FROM
          ReasonCode rc,dim_reasoncode drc
	WHERE drc.ReasonCodeId = rc.ID
	  AND drc.RowIsCurrent = 1
;
