UPDATE    dim_ReleaseIndicator ri

   SET ri.ReleaseIndicator = ifnull(t.T16FE_FRGET, 'Not Set')
       FROM
          T16FE t, dim_ReleaseIndicator ri
 WHERE ri.RowIsCurrent = 1
 AND t.T16FE_FRGKE = ri.ReleaseIndicatorCode;
