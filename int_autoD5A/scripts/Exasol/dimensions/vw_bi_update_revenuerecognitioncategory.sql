UPDATE    dim_revenuerecognitioncategory r

   SET r.Description = ifnull(t.DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T t, dim_revenuerecognitioncategory r
 WHERE r.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'RR_RELTYP'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND r.Category = ifnull(t.DD07T_DOMVALUE, 'Not Set')
;
