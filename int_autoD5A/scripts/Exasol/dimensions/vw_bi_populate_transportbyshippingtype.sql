/*********************************************Change History*******************************************************/
/*   Date            By        Version           Desc 															  */
/*   7 Jan 2015      Marius    1.0               Dimension Created                                                */

/******************************************************************************************************************/


INSERT INTO dim_tranportbyshippingtype(dim_tranportbyshippingtypeid )
SELECT 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_tranportbyshippingtype
               WHERE dim_tranportbyshippingtypeid = 1);

UPDATE	dim_tranportbyshippingtype tst
SET		tst.ShippingTypeDesc = t173t_bezei,
		tst.dw_update_date = current_timestamp
FROM	t173t, dim_tranportbyshippingtype tst
WHERE   t173t_vsart = tst.ShippingType;

delete from number_fountain m where m.table_name = 'dim_tranportbyshippingtype';

insert into number_fountain
select 	'dim_tranportbyshippingtype',
		ifnull(max(d.dim_tranportbyshippingtypeid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_tranportbyshippingtype d
where d.dim_tranportbyshippingtypeid <> 1;	
			   
INSERT INTO dim_tranportbyshippingtype(dim_tranportbyshippingtypeid,
                        ShippingType,
						ShippingTypeDesc)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_tranportbyshippingtype')
          + row_number() over(order by ''),
          ifnull(t173t_vsart, 'Not Set'),
          ifnull(t173t_bezei, 'Not Set')
     FROM t173t
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_tranportbyshippingtype tst
                       WHERE tst.ShippingType = t173t_vsart)
        and t173t_vsart is not null;

delete from number_fountain m where m.table_name = 'dim_tranportbyshippingtype';
		