UPDATE    dim_controllingarea ca

   SET ca.ControllingAreaName = ifnull(TKA01_BEZEI, 'Not Set'),
       ca.CurrencyKey = ifnull(TKA01_WAERS, 'Not Set'),
       ca.FiscalYrVariant = ifnull(TKA01_LMONA, 'Not Set'),
       ca.AllocationIndicator = ifnull(TKA01_KOKFI, 'Not Set')
          FROM
          TKA01 t , dim_controllingarea ca 
	WHERE t.TKA01_KOKRS IS NOT NULL
          AND ca.ControllingAreaCode = t.TKA01_KOKRS	   
 ;