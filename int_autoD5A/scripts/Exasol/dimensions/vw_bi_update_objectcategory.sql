UPDATE    dim_objectcategory oc
       SET oc.ObjectCategoryName = t.TJ03T_TXT,
			oc.dw_update_date = current_timestamp
	   FROM
         dim_objectcategory oc, TJ03T t
 WHERE oc.RowIsCurrent = 1
      AND oc.ObjectCategoryCode = t.TJ03T_OBTYP
;