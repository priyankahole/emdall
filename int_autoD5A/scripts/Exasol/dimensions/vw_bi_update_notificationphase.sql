UPDATE    dim_notificationphase s
   SET s.NotificationPhaseName = t.DD07T_DDTEXT,
			s.dw_update_date = current_timestamp
  FROM
          dim_notificationphase s, DD07T t
 WHERE s.RowIsCurrent = 1
    AND   t.DD07T_DOMNAME = 'QM_PHASE'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND s.NotificationPhaseCode = t.DD07T_DOMVALUE
;