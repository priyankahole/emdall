insert into dim_sampledrawingitems (
dim_sampledrawingitemsid, 
SampleProcedure, 
VersionNo,
ItemNo)

select 	1, 'Not Set', 'Not Set', 0
from (select 1) a
where not exists ( select 'x' from dim_sampledrawingitems where dim_sampledrawingitemsid = 1);

delete from number_fountain m where m.table_name = 'dim_sampledrawingitems';
insert into number_fountain
select 'dim_sampledrawingitems',
 ifnull(max(d.dim_sampledrawingitemsid ), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_sampledrawingitems d
where d.dim_sampledrawingitemsid <> 1;

insert into dim_sampledrawingitems (
dim_sampledrawingitemsid, 
SampleProcedure, 
VersionNo,
ItemNo,
ReserveSamplesNo,
Formulafield,
PhysicalSampleContainerPrimary,
PhysicalSampleContainer,
unitofmeasure,
PhysicalSampleContainerPrimaryNo,
Highestsamplenovalid,
sizeofreservesample,
primaryfactor,
pooledfactor,
Formulafield2
)

 select (select ifnull(m.max_id, 1) 
         from number_fountain m 
 where m.table_name = 'dim_sampledrawingitems') + row_number() over(order by '') AS dim_sampledrawingitemsid,
        ifnull(QPRVP_PRZIEHVERF, 'Not Set') SampleProcedure,
		ifnull(QPRVP_VERSION, 'Not Set') VersionNo,
		ifnull(QPRVP_POSNRPRZV, 0) ItemNo,
		ifnull(QPRVP_ANZRUECKL, 0) ReserveSamplesNo,
		ifnull(QPRVP_FORMEL1, 'Not Set') Formulafield,
		ifnull(QPRVP_GEBINDE1, 'Not Set') PhysicalSampleContainerPrimary,
		ifnull(QPRVP_GEBINDER, 'Not Set') PhysicalSampleContainer,
		ifnull(QPRVP_MERUECKL, 'Not Set') unitofmeasure,
		ifnull(QPRVP_PRANZ1, 0) PhysicalSampleContainerPrimaryNo,
		ifnull(QPRVP_PROBNRPL, 0) Highestsamplenovalid,
		ifnull(QPRVP_UMFRUECKL, 0) sizeofreservesample,
		ifnull(QPRVP_FAKTORPR1,0) primaryfactor,
		ifnull(QPRVP_FAKTORPR2,0) pooledfactor,
		ifnull (QPRVP_FORMEL2, 'Not Set') Formulafield2
 from QPRVP q
 where not exists (select 'x' from dim_sampledrawingitems ds
                        where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
                             AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
							 AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0));


UPDATE dim_sampledrawingitems ds	
SET 	ReserveSamplesNo =ifnull(q.QPRVP_ANZRUECKL, 0) 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND ReserveSamplesNo <> ifnull(q.QPRVP_ANZRUECKL, 0);
	  
UPDATE dim_sampledrawingitems ds	
SET 	Formulafield = ifnull(QPRVP_FORMEL1, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND Formulafield <> ifnull(QPRVP_FORMEL1, 'Not Set');
	  
UPDATE dim_sampledrawingitems ds	
SET 	PhysicalSampleContainerPrimary = ifnull(QPRVP_GEBINDE1, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND PhysicalSampleContainerPrimary <> ifnull(QPRVP_GEBINDE1, 'Not Set');

UPDATE dim_sampledrawingitems ds	
SET 	PhysicalSampleContainer = ifnull(QPRVP_GEBINDER, 'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND PhysicalSampleContainer <> ifnull(QPRVP_GEBINDER, 'Not Set');

	  
UPDATE dim_sampledrawingitems ds	
SET 	unitofmeasure = ifnull(QPRVP_MERUECKL, 'Not Set') 
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND unitofmeasure <> ifnull(QPRVP_MERUECKL, 'Not Set');
	  
UPDATE dim_sampledrawingitems ds	
SET 	PhysicalSampleContainerPrimaryNo = ifnull(QPRVP_PRANZ1, 0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND PhysicalSampleContainerPrimaryNo <> ifnull(QPRVP_PRANZ1, 0);
	 
UPDATE dim_sampledrawingitems ds	
SET 	Highestsamplenovalid = ifnull(QPRVP_PROBNRPL, 0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND Highestsamplenovalid <> ifnull(QPRVP_PROBNRPL, 0);
	  
UPDATE dim_sampledrawingitems ds	
SET 	sizeofreservesample = ifnull(QPRVP_UMFRUECKL, 0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND sizeofreservesample <> ifnull(QPRVP_UMFRUECKL, 0);
	  

UPDATE dim_sampledrawingitems ds	
SET 	primaryfactor = ifnull(QPRVP_FAKTORPR1,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND primaryfactor <> ifnull(QPRVP_FAKTORPR1,0);
	  
UPDATE dim_sampledrawingitems ds	
SET 	pooledfactor = ifnull(QPRVP_FAKTORPR2,0)
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND pooledfactor <> ifnull(QPRVP_FAKTORPR2,0);
	  
UPDATE dim_sampledrawingitems ds	
SET 	Formulafield2 = ifnull(QPRVP_FORMEL2,'Not Set')
,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QPRVP q,dim_sampledrawingitems ds
where ds.SampleProcedure = ifnull(q.QPRVP_PRZIEHVERF, 'Not Set') 
      AND ds.VersionNo = ifnull(q.QPRVP_VERSION, 'Not Set')
	  AND ItemNo = ifnull(q.QPRVP_POSNRPRZV, 0)
	  AND Formulafield2 <> ifnull(QPRVP_FORMEL2,'Not Set');
	  

