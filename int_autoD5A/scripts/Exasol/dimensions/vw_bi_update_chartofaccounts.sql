UPDATE    dim_chartofaccounts coa
   SET coa.ShortText = s.SKAT_TXT20, 
       coa.Description = s.SKAT_TXT50,
	   coa.dw_update_date = current_timestamp
	  FROM
          dim_chartofaccounts coa, SKAT s 
 WHERE coa.RowIsCurrent = 1
 AND  coa.CharOfAccounts = s.SKAT_KTOPL
 AND coa.GLAccountNumber = s.SKAT_SAKNR
 AND s.SKAT_SPRAS = 'E';
		  
;