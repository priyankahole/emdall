/* ################################################################################################################## */
/* */
/*   Script         : exa_bi_populate_surveyquestions */
/*   Author         : Andrei R */
/*   Created On     : 10 Mar 2017 */
/*   Modifications  : Madalina 5 May 2017 - adjusting script */
/*  				Grain: QuestionCode, SubQuestionCode*/
/* ################################################################################################################## */

/* DELETE FROM SURVEYQUESTIONS  --  the delete stmt is performed within the python script */

/*IMPORT INTO SURVEYQUESTIONS FROM CSV
AT DC1STGC1INT01CONNECTION
FILE '/home/fusionops/ispring_clustered_storage/configurations/MerckDE6/files/CustomerSurvey/QUESTIONS.csv'
COLUMN SEPARATOR = '|'
ENCODING = 'ISO-8859-1'
SKIP = 1
*/

/* the final quesition description is formed using qs desc + subqs desc */
update SURVEYQUESTIONS
set questiondescription = case when subquestiondescription <> 'Not Set' then questiondescription || '   ' || subquestiondescription
								else questiondescription end;

/*delete the questions that will be reinserted*/
delete from  DIM_SURVEYQUESTIONS s where exists (SELECT 1  FROM surveyquestions ds where s.questioncode = ds.questioncode
																					and s.subquestioncode = ds.subquestioncode);

INSERT INTO DIM_SURVEYQUESTIONS (dim_surveyquestionsid)
select 1
from (select 1) a
where not exists (select '1' from dim_surveyquestions where dim_surveyquestionsid = 1);

/*insert into the dimension the questions that do not exist*/
DELETE FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_surveyquestions';
INSERT INTO NUMBER_FOUNTAIN 
SELECT 'dim_surveyquestions', ifnull(max(dim_surveyquestionsid),1)
FROM DIM_SURVEYQUESTIONS;

INSERT INTO DIM_SURVEYQUESTIONS (dim_surveyquestionsid,QuestionCode,SubQuestionCode,SurveyCode,QuestionDescription,NpsIndicator, nrcrtWithSubq)
SELECT(SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'dim_surveyquestions') + row_number() over(order by '') as DIM_SURVEYQUESTIONSID,
questioncode, SubQuestionCode, surveycode, questiondescription, npsindicator, nrcrt
from surveyquestions S WHERE NOT EXISTS(SELECT 1  FROM dim_surveyquestions ds where s.questioncode = ds.questioncode
																					and s.subquestioncode = ds.subquestioncode
										) ; 

UPDATE DIM_SURVEYQUESTIONS DS 
SET DS.SURVEYCODE = S.SURVEYCODE,
DW_UPDATE_DATE = CURRENT_TIMESTAMP
FROM SURVEYQUESTIONS S, DIM_SURVEYQUESTIONS DS
WHERE DS.QUESTIONCODE = S.QUESTIONCODE  
	AND DS.SUBQUESTIONCODE = S.SUBQUESTIONCODE
	AND DS.SURVEYCODE <> S.SURVEYCODE;

UPDATE DIM_SURVEYQUESTIONS DS 
SET DS.QUESTIONDESCRIPTION = S.QUESTIONDESCRIPTION,
DW_UPDATE_DATE = CURRENT_TIMESTAMP
FROM SURVEYQUESTIONS S, DIM_SURVEYQUESTIONS DS
WHERE DS.QUESTIONCODE = S.QUESTIONCODE  
	AND DS.SUBQUESTIONCODE = S.SUBQUESTIONCODE
	AND DS.QUESTIONDESCRIPTION <> S.QUESTIONDESCRIPTION;

UPDATE DIM_SURVEYQUESTIONS DS 
SET DS.NPSINDICATOR = S.NPSINDICATOR,
DW_UPDATE_DATE = CURRENT_TIMESTAMP
FROM SURVEYQUESTIONS S, DIM_SURVEYQUESTIONS DS
WHERE DS.QUESTIONCODE = S.QUESTIONCODE  
	AND DS.SUBQUESTIONCODE = S.SUBQUESTIONCODE
	AND DS.NPSINDICATOR <> S.NPSINDICATOR;
	
update DIM_SURVEYQUESTIONS DS 
set nrcrt = to_number(substring(nrcrtWithSubq, 0, instr( nrcrtWithSubq, '.') -1));

/* Adjust some question numbers for the US survey */
update dim_surveyquestions q
set q.nrcrt = 0
where q.surveycode = '561350' and q.nrcrt = 4;


update dim_surveyquestions q
set q.nrcrt = 0
where q.surveycode = '561350' and q.nrcrt = 5;

update dim_surveyquestions q
set q.nrcrt = q.nrcrt - 2
where q.surveycode = '561350' and q.nrcrt >= 6;


