
UPDATE    dim_itemstatus s

   SET s.Description = ifnull(DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T t,dim_itemstatus s 
 WHERE s.RowIsCurrent = 1
       AND     t.DD07T_DOMNAME = 'EPSTATU'
          AND DD07T_DOMVALUE IS NOT NULL
          AND s.status = t.DD07T_DOMVALUE;
