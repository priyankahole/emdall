/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_certifresultvalues_dim.sql */
/*   Author         : Madalina Herghelegiu */
/*   Created On     : 30 May 2016 */
/*  */
/*  */
/*   Description    : Populate Certificates result values Dimension */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* #################################################################################################################### */

insert into dim_certifresultvalues (dim_certifresultvaluesid, characteristicresult, descCharactResult)
select '1', 'Not Set', 'Not Set'
from (select 1) a
where not exists ( select 'x' from dim_certifresultvalues where dim_certifresultvaluesid = 1);

delete from number_fountain m where m.table_name = 'dim_certifresultvalues';

insert into number_fountain
select 'dim_certifresultvalues',
		ifnull( max(d.dim_certifresultvaluesid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
		from dim_certifresultvalues d
		where dim_certifresultvaluesid <> 1;
		
insert into dim_certifresultvalues
	(
	dim_certifresultvaluesid,
	characteristicresult
	)
	select ( select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_certifresultvalues') + row_number() over(order by '') AS dim_certifresultvaluesid,
			ifnull(TQ61T_KZHERKWER, 'Not Set') as characteristicresult
	from TQ61T t
	where not exists (select 1 from dim_certifresultvalues c
    				  where c.characteristicresult = ifnull(t.TQ61T_KZHERKWER, 'Not Set'));
	
update dim_certifresultvalues c
set c.descCharactResult = ifnull(t.TQ61T_KURZTEXT, 'Not Set'),
	dw_update_date = current_timestamp 
from TQ61T t, dim_certifresultvalues c
where c.characteristicresult = ifnull(t.TQ61T_KZHERKWER, 'Not Set')
	and c.descCharactResult <> ifnull(t.TQ61T_KURZTEXT, 'Not Set');
