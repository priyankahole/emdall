
INSERT INTO dim_afsseason(Dim_AfsSeasonId,SeasonIndicator,Collection,Theme,Description,RowStartDate
,RowEndDate,RowIsCurrent,RowChangeReason)
SELECT 1,'Not Set','Not Set','Not Set','Not Set',current_timestamp,null,1,null
FROM (SELECT 1) a
where not exists(select 1 from dim_afsseason where Dim_AfsSeasonId = 1);

drop table if exists tmp_J_3ASEANT;
create table tmp_J_3ASEANT
as
select J_3ASEANT_J_3ASEAN,J_3ASEANT_AFS_COLLECTION,J_3ASEANT_AFS_THEME,J_3ASEANT_TEXT,row_number() over (partition by J_3ASEANT_J_3ASEAN,J_3ASEANT_AFS_COLLECTION,J_3ASEANT_AFS_THEME) rownum
from J_3ASEANT where J_3ASEANT_TEXT is not null;

UPDATE	dim_afsseason ase
FROM 	tmp_J_3ASEANT t
SET 	ase.Description = ifnull(t.J_3ASEANT_TEXT, 'Not Set'),
		ase.dw_update_date = current_timestamp
WHERE 	ase.RowIsCurrent = 1
		AND ase.SeasonIndicator = t.J_3ASEANT_J_3ASEAN
		AND ase.Collection = t.J_3ASEANT_AFS_COLLECTION
		AND ase.Theme = t.J_3ASEANT_AFS_THEME
		AND rownum = 1
		AND ase.Description <> ifnull(t.J_3ASEANT_TEXT, 'Not Set');
		
drop table if exists tmp_J_3ASEANT;

call vectorwise(combine 'dim_afsseason');
 
delete from number_fountain m where m.table_name = 'dim_afsseason';

insert into number_fountain
select 	'dim_afsseason',
	ifnull(max(d.Dim_AfsSeasonId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_afsseason d
where d.Dim_AfsSeasonId <> 1; 
 
INSERT INTO dim_afsseason(Dim_AfsSeasonId,
			SeasonIndicator,
			  Collection,
			  Theme,
			  Description,
              RowStartDate,
              RowIsCurrent)
     SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_afsseason') 
          + row_number() over(),
			J_3ASEANT_J_3ASEAN,
	    ifnull(J_3ASEANT_AFS_COLLECTION, 'Not Set'),
	    ifnull(J_3ASEANT_AFS_THEME, 'Not Set'),
	    J_3ASEANT_TEXT,
        current_timestamp,
        1
       FROM J_3ASEANT
      WHERE J_3ASEANT_J_3ASEAN IS NOT NULL
	    AND NOT EXISTS
                  (SELECT 1
                     FROM dim_afsseason
                    WHERE SeasonIndicator = J_3ASEANT_J_3ASEAN
                     AND Collection = ifnull(J_3ASEANT_AFS_COLLECTION, 'Not Set')
                     AND Theme = ifnull (J_3ASEANT_AFS_THEME, 'Not Set'));
					 
delete from number_fountain m where m.table_name = 'dim_afsseason';	
