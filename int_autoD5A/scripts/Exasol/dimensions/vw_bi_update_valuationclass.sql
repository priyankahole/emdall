UPDATE    dim_ValuationClass vc

   SET DescriptionOfValuationClass = ifnull(T025T_BKBEZ, 'Not Set')
       FROM
          t025t t, dim_ValuationClass vc
 WHERE vc.RowIsCurrent = 1
 AND t.T025T_BKLAS = vc.ValuationClass;
