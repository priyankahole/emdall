
UPDATE    dim_escalationreason ds

   SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set')
       FROM
          SCMGATTR_SESCALT dt, dim_escalationreason ds
 WHERE ds.RowIsCurrent = 1
          AND ds.EscalReason = dt.ESCAL_REASON
          AND dt.ESCAL_REASON IS NOT NULL;
