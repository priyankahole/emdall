

insert into dim_customerpartnerfunctions 
(dim_customerpartnerfunctionsid, rowchangereason, rowenddate, rowiscurrent, rowstartdate, 
customernumber1, customername1, salesorgcode, salesorgdesc, distributionchannelcode, 
distributionchannelname, divisioncode, divisionname, partnerfunction, partnerfunctiondesc, 
partnercounter, customernumberbusinesspartner, customernamebusinesspartner, vendoraccnumber, 
vendorname, personalnumber, contactpersonnumber, customerdescriptionofpartner, defaultpartner
) 
SELECT 1,null,null,1,current_timestamp,
'Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set','Not Set',
'Not Set','Not Set','Not Set','Not Set',
'Not Set',0,0,'Not Set','Not Set'
FROM (SELECT 1) a
where not exists(select 1 from dim_customerpartnerfunctions where dim_customerpartnerfunctionsid = 1);


UPDATE dim_customerpartnerfunctions cpf
   SET   VendorName = ifnull(NAME1, 'Not Set')
   FROM dim_customerpartnerfunctions cpf, KNVP kp, LFA1 l1
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
                            AND cpf.SalesOrgCode = kp.KNVP_VKORG
                            AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
                            AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
                            AND cpf.PartnerFunction = kp.KNVP_PARVW
	AND cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
                            AND cpf.RowIsCurrent = 1						
        AND l1.LIFNR = kp.KNVP_LIFNR
        AND VendorName <> ifnull(NAME1, 'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf 
   SET   CustomerNameBusinessPartner = ifnull(KNA1_NAME1, 'Not Set')
   FROM dim_customerpartnerfunctions cpf, KNVP kp, KNA1 k1
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND k1.KNA1_KUNNR = kp.KNVP_KUNN2
        AND CustomerNameBusinessPartner <> ifnull(KNA1_NAME1, 'Not Set')
        AND cpf.RowIsCurrent = 1;        
 
UPDATE dim_customerpartnerfunctions cpf
   SET   SalesOrgDesc = ifnull(TVKOT_VTEXT, 'Not Set')
  FROM dim_customerpartnerfunctions cpf, KNVP kp, TVKOT
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND TVKOT_VKORG = kp.KNVP_VKORG
        AND SalesOrgDesc <> ifnull(TVKOT_VTEXT, 'Not Set')
        AND cpf.RowIsCurrent = 1;
 
 UPDATE dim_customerpartnerfunctions cpf
   SET   PartnerFunctionDesc = ifnull( TPART_VTEXT, 'Not Set')
   FROM dim_customerpartnerfunctions cpf, KNVP kp,TPART
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND TPART_PARVW = kp.KNVP_PARVW
        AND PartnerFunctionDesc <> ifnull( TPART_VTEXT, 'Not Set')
        AND cpf.RowIsCurrent = 1;
 
 UPDATE dim_customerpartnerfunctions cpf
   SET   DivisionName = ifnull(TSPAT_VTEXT, 'Not Set')
 FROM dim_customerpartnerfunctions cpf, KNVP kp, TSPAT
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND TSPAT_SPART = kp.KNVP_SPART
        AND DivisionName <> ifnull(TSPAT_VTEXT, 'Not Set')
        AND cpf.RowIsCurrent = 1;
        
        UPDATE dim_customerpartnerfunctions cpf 
   SET    CustomerName1 = ifnull(KNA1_NAME1, 'Not Set')
   FROM dim_customerpartnerfunctions cpf, KNVP kp, KNA1 k1
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND k1.KNA1_KUNNR = kp.KNVP_KUNNR
        AND CustomerName1 <> ifnull( KNA1_NAME1,'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf
   SET   DistributionChannelName = ifnull(TVTWT_VTEXT, 'Not Set')
   FROM dim_customerpartnerfunctions cpf, KNVP kp, TVTWT
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND TVTWT_VTWEG = kp.KNVP_VTWEG
        AND DistributionChannelName <> ifnull(TVTWT_VTEXT, 'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf
   SET    VendorAccNumber= ifnull(kp.KNVP_LIFNR,'Not Set')
  FROM dim_customerpartnerfunctions cpf, KNVP kp
 WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
			    AND VendorAccNumber <> ifnull(kp.KNVP_LIFNR,'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf
   SET    
     CustomerNumberBusinessPartner = ifnull(kp.KNVP_KUNN2,'Not Set')
FROM dim_customerpartnerfunctions cpf, KNVP kp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
			    AND CustomerNumberBusinessPartner <> ifnull(kp.KNVP_KUNN2,'Not Set')
        AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf
   SET PersonalNumber = ifnull(kp.KNVP_PERNR, 0)
 FROM  dim_customerpartnerfunctions cpf, KNVP kp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND cpf.RowIsCurrent = 1
  AND PersonalNumber <> ifnull(kp.KNVP_PERNR, 0);

UPDATE dim_customerpartnerfunctions cpf
   SET ContactPersonNumber = ifnull(kp.KNVP_PARNR, 0)
   FROM dim_customerpartnerfunctions cpf, KNVP kp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND cpf.RowIsCurrent = 1
  AND ContactPersonNumber <> ifnull(kp.KNVP_PARNR, 0);

UPDATE dim_customerpartnerfunctions cpf 
   SET CustomerDescriptionofPartner = ifnull(kp.KNVP_KNREF, 'Not Set')
   FROM dim_customerpartnerfunctions cpf, KNVP kp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
       AND CustomerDescriptionofPartner <> ifnull(kp.KNVP_KNREF, 'Not Set')
 AND cpf.RowIsCurrent = 1;

UPDATE dim_customerpartnerfunctions cpf
   SET DefaultPartner = ifnull(kp.KNVP_DEFPA, 'Not Set')
   FROM dim_customerpartnerfunctions cpf, KNVP kp
    WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
        AND cpf.SalesOrgCode = kp.KNVP_VKORG
        AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
        AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
        AND cpf.PartnerFunction = kp.KNVP_PARVW
        AND cpf.PartnerCounter =  ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set')
        AND cpf.RowIsCurrent = 1
     AND DefaultPartner <> ifnull(kp.KNVP_DEFPA, 'Not Set');
     
     delete from NUMBER_FOUNTAIN where table_name = 'dim_customerpartnerfunctions';
     
INSERT INTO NUMBER_FOUNTAIN
select 	'dim_customerpartnerfunctions',
	ifnull(max(d.dim_customerpartnerfunctionsid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_customerpartnerfunctions d
where d.dim_customerpartnerfunctionsid <> 1;

     INSERT INTO dim_customerpartnerfunctions(dim_customerpartnerfunctionsid,
                                         CustomerNumber1,
                                         CustomerName1,
                                         DistributionChannelCode,
                                         DistributionChannelName,
                                         DivisionCode,
                                         DivisionName,
                                         PartnerFunction,
                                         PartnerFunctionDesc,
                                         SalesOrgCode,
                                         SalesOrgDesc,
                                         PartnerCounter,
                                         CustomerNumberBusinessPartner,
                                         CustomerNameBusinessPartner,
                                         VendorAccNumber,
                                         VendorName,
                                         PersonalNumber,
                                         ContactPersonNumber,
                                         CustomerDescriptionofPartner,
                                         DefaultPartner,
                                         RowStartDate,
                                         RowIsCurrent)
   SELECT (SELECT ifnull(m.max_id, 1) 
		   from NUMBER_FOUNTAIN m 
		   WHERE m.table_name = 'dim_customerpartnerfunctions') + row_number() over (order by '') dim_customerpartnerfunctionsid,
		   src.*
    from (select distinct
          kp.KNVP_KUNNR CustomerNumber1,
          ifnull(k1.KNA1_NAME1,'Not Set')
          CustomerName1,
          ifnull(tv1.TVTWT_VTWEG,'Not Set')
          DistChannelCode,
          ifnull(tv2.TVTWT_VTEXT,'Not Set')
          DistChannelName,
          ifnull(ts.TSPAT_SPART,'Not Set')
          DivisionCode,
          ifnull(ts1.TSPAT_VTEXT,'Not Set')
          DivisionName,
          ifnull(tp.TPART_PARVW,'Not Set') PartnerFunction,
          ifnull(tp.TPART_VTEXT,'Not Set') PartnerFunctionDesc,
          ifnull(tvk.TVKOT_VKORG,'Not Set')
          SalesOrgCode,
          ifnull(tvk1.TVKOT_VTEXT,'Not Set')
          SalesOrgDesc,
          ifnull(cast(kp.KNVP_PARZA as varchar(10)), 'Not Set') PartnerCounter,
          kp.KNVP_KUNN2 CustomerNumberBusinessPartner,
          ifnull(k2.KNA1_NAME1,'Not Set')
          CustomerNameBusinessPartner,
          kp.KNVP_LIFNR VendorAccNumber,
          ifnull(l1.NAME1,'Not Set')
          VendorName,
          ifnull(kp.KNVP_PERNR, 0) PersonalNumber,
          ifnull(kp.KNVP_PARNR, 0) ContactPersonNumber,
          ifnull(kp.KNVP_KNREF, 'Not Set') CustomerDescriptionofPartner,
          ifnull(kp.KNVP_DEFPA, 'Not Set') DefaultPartner,
          current_date,
          1
          FROM KNVP kp
          INNER JOIN tpart tp ON kp.KNVP_PARVW = tp.TPART_PARVW
		  LEFT JOIN KNA1 k1 ON k1.KNA1_KUNNR = kp.KNVP_KUNNR
		  LEFT JOIN TVTWT tv1 ON tv1.TVTWT_VTWEG = kp.KNVP_VTWEG
		  LEFT JOIN TVTWT tv2 ON tv2.TVTWT_VTWEG = kp.KNVP_VTWEG
		  LEFT JOIN TSPAT ts ON ts.TSPAT_SPART = kp.KNVP_SPART
		  LEFT JOIN TSPAT ts1 ON ts1.TSPAT_SPART = kp.KNVP_SPART
		  LEFT JOIN TVKOT tvk ON tvk.TVKOT_VKORG = kp.KNVP_VKORG
		  LEFT JOIN TVKOT tvk1 ON tvk1.TVKOT_VKORG = kp.KNVP_VKORG
		  LEFT JOIN KNA1 k2 ON k2.KNA1_KUNNR = kp.KNVP_KUNN2
		  LEFT JOIN LFA1 l1 ON l1.LIFNR = kp.KNVP_LIFNR
		  WHERE NOT EXISTS
                     (SELECT 1
                        FROM dim_customerpartnerfunctions cpf
                       WHERE cpf.CustomerNumber1 = kp.KNVP_KUNNR
                            AND cpf.SalesOrgCode = kp.KNVP_VKORG
                            AND cpf.DivisionCode = ifnull(kp.KNVP_SPART, 'Not Set')
                            AND cpf.DistributionChannelCode = ifnull(kp.KNVP_VTWEG, 'Not Set')
                            AND cpf.PartnerFunction = kp.KNVP_PARVW
                            AND cpf.PartnerCounter = ifnull(convert(varchar(10),kp.KNVP_PARZA), 'Not Set'))
		) src;

delete from NUMBER_FOUNTAIN where table_name = 'dim_customerpartnerfunctions';
