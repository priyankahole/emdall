
UPDATE    dim_customergroup cg
   SET cg.Description = ifnull(T151T_KTEXT, 'Not Set'),
			cg.dw_update_date = current_timestamp
	 FROM
          dim_customergroup cg, T151T t
 WHERE cg.RowIsCurrent = 1
      AND t.T151T_KDGRP = cg.CustomerGroup 
	  AND t.T151T_SPRAS = 'E';

