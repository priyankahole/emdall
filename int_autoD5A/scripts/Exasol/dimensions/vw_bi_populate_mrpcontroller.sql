update dim_mrpcontroller dm
set dm.Description = ifnull(ds.T024D_DSNAM, 'Not Set'),
    dm.TelephoneNo = ifnull(ds.T024D_DSTEL,'Not Set'),
			dm.dw_update_date = current_timestamp
 from dim_mrpcontroller dm, T024D ds 
where     dm.MRPController = ds.T024D_DISPO
      AND dm.Plant         = ds.T024D_WERKS
      AND RowIsCurrent = 1
	  AND (   ifnull(dm.Description,'NS') <> ifnull(ds.T024D_DSNAM, 'Not Set')
           OR ifnull(dm.TelephoneNo,'NS') <> ifnull(ds.T024D_DSTEL, 'Not Set'));

INSERT INTO dim_MRPController(dim_MRPControllerId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_MRPController
               WHERE dim_MRPControllerId = 1);

delete from number_fountain m where m.table_name = 'dim_MRPController';
   
insert into number_fountain
select 	'dim_MRPController',
	ifnull(max(d.Dim_MRPControllerID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_MRPController d
where d.Dim_MRPControllerID <> 1; 

INSERT INTO dim_MRPController(Dim_MRPControllerID,
                                Description,
                                MRPController,
                                Plant,
								TelephoneNo,
                                RowStartDate,
                                RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_MRPController') 
          + row_number() over(order by '') ,
			 ifnull(T024D_DSNAM, 'Not Set'),
            T024D_DISPO,
            ifnull(T024D_WERKS,'Not Set'),
			ifnull(T024D_DSTEL,'Not Set'),
            current_date,
            1
       FROM T024D t
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_MRPController
                    WHERE MRPController = T024D_DISPO
                          AND Plant = T024D_WERKS
                          AND RowIsCurrent = 1)
;
