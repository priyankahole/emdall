
UPDATE    dim_ProductionOrderType pot
   SET pot.Description = ifnull(T003P_TXT, 'Not Set'),
			pot.dw_update_date = current_timestamp
FROM T003P tp, dim_ProductionOrderType pot
 WHERE pot.RowIsCurrent = 1
 AND tp.T003P_AUART = pot.TypeCode
;

INSERT INTO dim_ProductionOrderType(dim_ProductionOrderTypeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_ProductionOrderType
               WHERE dim_ProductionOrderTypeId = 1);

delete from number_fountain m where m.table_name = 'dim_ProductionOrderType';
   
insert into number_fountain
select 	'dim_ProductionOrderType',
	ifnull(max(d.Dim_productionordertypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ProductionOrderType d
where d.Dim_productionordertypeid <> 1; 

INSERT INTO dim_ProductionOrderType(Dim_productionordertypeid,
                           Description,
                           TypeCode,
                           RowStartDate,
                           RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_ProductionOrderType')
         + row_number() over(order by ''),
			  ifnull(T003P_TXT, 'Not Set'),
            T003P_AUART,
            current_timestamp,
            1
       FROM T003P
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_ProductionOrderType
                    WHERE TypeCode = T003P_AUART and rowiscurrent = 1)
   ORDER BY 2 ;

