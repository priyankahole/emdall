UPDATE    dim_ValuationType vt

   SET AccountCategory = ifnull(T149D_KKREF, 'Not Set')
         FROM
          T149D t, dim_ValuationType vt
 WHERE vt.RowIsCurrent = 1
 AND t.T149D_BWTAR = vt.ValuationType;
