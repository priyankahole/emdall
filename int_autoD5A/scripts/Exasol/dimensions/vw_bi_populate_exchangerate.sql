
DROP TABLE IF EXISTS tmp_tcurr;
CREATE TABLE tmp_tcurr as SELECT t.*, concat(substr((99999999 - tcurr_gdatu),0,4),'-',substr((99999999 - tcurr_gdatu),5,2),'-',substr((99999999 - tcurr_gdatu),7,2)) tcurr_gdatu_date  FROM TCURR t;

UPDATE  dim_exchangerate er
                SET     er.FromCurrency = ifnull(tc.tcurr_fcurr, 'Not Set'),
                        er.FromCurrencyUnitsRatio = ifnull(tc.tcurr_ffact, 0),
                        er.EffectiveFrom = ifnull(tc.tcurr_gdatu_date, '0001-01-01'),
                        er.ExchangeRateType = ifnull(tc.tcurr_kurst, 'Not Set'),
                        er.ToCurrency = ifnull(tc.tcurr_tcurr, 'Not Set'),
                        er.ToCurrencyUnitsRatio = ifnull(tc.tcurr_tfact, 0),
                        er.ExchangeRate = ifnull(tc.tcurr_ukurs, 0),
			er.dw_update_date = current_timestamp
FROM    tmp_tcurr tc,  dim_exchangerate er
WHERE er.RowIsCurrent = 1
AND tc.tcurr_kurst = er.ExchangeRateType
AND tc.tcurr_tcurr = er.ToCurrency
AND tc.tcurr_fcurr = er.FromCurrency
AND tc.tcurr_gdatu_date = er.EffectiveFrom;


--Update EffectiveTo
DROP TABLE IF EXISTS EffectiveTo_tmp;

CREATE TABLE EffectiveTo_tmp
AS
SELECT distinct tc.tcurr_kurst,tc.tcurr_tcurr,tc.tcurr_fcurr,tc.tcurr_gdatu_date,Min(er.EffectiveFrom - 1) as EffectiveTo
FROM tmp_tcurr tc,dim_exchangerate er
WHERE   tc.tcurr_kurst = er.ExchangeRateType
                AND tc.tcurr_tcurr = er.ToCurrency
                AND tc.tcurr_fcurr = er.FromCurrency
                AND tc.tcurr_gdatu_date < er.EffectiveFrom
GROUP BY tcurr_kurst,tcurr_tcurr,tcurr_fcurr,tc.tcurr_gdatu_date;


UPDATE dim_exchangerate er

SET er.EffectiveTo = tmp.EffectiveTo,
			er.dw_update_date = current_timestamp
FROM EffectiveTo_tmp tmp, dim_exchangerate er
WHERE tmp.tcurr_kurst = er.ExchangeRateType
        AND tmp.tcurr_tcurr = er.ToCurrency
        AND tmp.tcurr_fcurr = er.FromCurrency
        AND tmp.tcurr_gdatu_date = er.EffectiveFrom;

UPDATE dim_exchangerate er 
SET er.EffectiveTo = '9999-12-31',
			er.dw_update_date = current_timestamp
WHERE EffectiveTo = '0001-01-01'
AND dim_exchangerateid <> 1;

DROP TABLE IF EXISTS EffectiveTo_tmp;
DROP TABLE IF EXISTS tmp_tcurr;

INSERT INTO dim_exchangerate(dim_exchangerateid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_exchangerate
               WHERE dim_exchangerateid = 1);


DROP TABLE IF EXISTS tmp_tcurr;
CREATE TABLE tmp_tcurr as SELECT t.*, concat(substr((99999999 - tcurr_gdatu),0,4),'-',substr((99999999 - tcurr_gdatu),5,2),'-',substr((99999999 - tcurr_gdatu),7,2)) tcurr_gdatu_date  FROM TCURR t;

delete from number_fountain m where m.table_name = 'dim_exchangerate';
   
insert into number_fountain
select 	'dim_exchangerate',
	ifnull(max(d.dim_exchangerateid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_exchangerate d
where d.dim_exchangerateid <> 1; 

INSERT INTO dim_exchangerate(dim_exchangerateid,
                                                                FromCurrency,
                                                                FromCurrencyUnitsRatio,
                                                                EffectiveFrom,
                                                                EffectiveTo,
                                                                ExchangeRateType,
                                                                ToCurrency,
                                                                ToCurrencyUnitsRatio,
                                                                ExchangeRate,
                                                                RowStartDate,
                                                                RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_exchangerate')
          + row_number() over(order by ''),
                 ifnull(tc.tcurr_fcurr, 'Not Set'),
                 ifnull(tc.tcurr_ffact, 0),
                 ifnull(tc.tcurr_gdatu_date, '0001-01-01'),
                 '0001-01-01',
                 ifnull(tc.tcurr_kurst, 'Not Set'),
                 ifnull(tc.tcurr_tcurr, 'Not Set'),
                 ifnull(tc.tcurr_tfact, 0),
                 ifnull(tc.tcurr_ukurs, 0),
        current_timestamp,
         1
     FROM tmp_tcurr tc
    WHERE NOT EXISTS
                (SELECT 1
                    FROM dim_exchangerate ert
                                        WHERE ert.ExchangeRateType = tc.tcurr_kurst
                                        AND ert.ToCurrency = tc.tcurr_tcurr
                                        AND ert.FromCurrency = tc.tcurr_fcurr
                                        AND ert.EffectiveFrom = tc.tcurr_gdatu_date
                                );

--Update EffectiveTo
DROP TABLE IF EXISTS EffectiveTo_tmp;

CREATE TABLE EffectiveTo_tmp
AS
SELECT distinct tc.tcurr_kurst,tc.tcurr_tcurr,tc.tcurr_fcurr,tc.tcurr_gdatu_date,Min(er.EffectiveFrom - 1) as EffectiveTo
FROM tmp_tcurr tc,dim_exchangerate er
WHERE   tc.tcurr_kurst = er.ExchangeRateType
                AND tc.tcurr_tcurr = er.ToCurrency
                AND tc.tcurr_fcurr = er.FromCurrency
                AND tc.tcurr_gdatu_date < er.EffectiveFrom
GROUP BY tcurr_kurst,tcurr_tcurr,tcurr_fcurr,tc.tcurr_gdatu_date;


UPDATE dim_exchangerate er
SET er.EffectiveTo = tmp.EffectiveTo,
			er.dw_update_date = current_timestamp
FROM EffectiveTo_tmp tmp, dim_exchangerate er
WHERE tmp.tcurr_kurst = er.ExchangeRateType
        AND tmp.tcurr_tcurr = er.ToCurrency
        AND tmp.tcurr_fcurr = er.FromCurrency
        AND tmp.tcurr_gdatu_date = er.EffectiveFrom;

UPDATE dim_exchangerate er 
SET er.EffectiveTo = '9999-12-31',
			er.dw_update_date = current_timestamp
FROM  dim_exchangerate er
WHERE EffectiveTo = '0001-01-01'
AND dim_exchangerateid <> 1;

DROP TABLE IF EXISTS EffectiveTo_tmp;
DROP TABLE IF EXISTS tmp_tcurr;

