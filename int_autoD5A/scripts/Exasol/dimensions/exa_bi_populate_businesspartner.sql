INSERT INTO dim_businesspartner (dim_businesspartnerID)
select 1
from (select 1) a
where not exists (select '1' from dim_businesspartner where dim_businesspartnerID = 1);

DELETE FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'dim_businesspartner';
INSERT INTO NUMBER_FOUNTAIN 
SELECT 'dim_businesspartner', ifnull(max(dim_businesspartnerID),1)
FROM dim_businesspartner;

INSERT INTO dim_businesspartner (dim_businesspartnerID,
                           regulatoryprocess,
						   customer,
						   deahin,
						   DW_UPDATE_DATE,
						   DW_INSERT_DATE,
						   RowStartDate,
						   RowEndDate,
						   rowchangereason,
						   RowIsCurrent)
SELECT( 
SELECT IFNULL(M.MAX_ID,1) FROM NUMBER_FOUNTAIN M WHERE M.TABLE_NAME = 'dim_businesspartner') + row_number() over(order by '') as dim_businesspartnerID,
		ifnull(REGPROC,'Not Set'),
		ifnull(SHIPTO,'Not Set'),
		ifnull(DEA_NO,'Not Set'),
		current_timestamp,
		current_timestamp,
		null,
		null,
		'Not Set',
		1
from ZZDEA_CUSTMAP z
 WHERE NOT EXISTS
	(SELECT 1  FROM dim_businesspartner p where ifnull(z.REGPROC,'Not Set') = ifnull(p.regulatoryprocess,'Not Set')
	                                   and ifnull(z.SHIPTO,'Not Set') = ifnull(p.customer,'Not Set')
									   and ifnull(z.DEA_NO, 'Not Set') = ifnull(p.deahin,'Not Set')
									   ) ;