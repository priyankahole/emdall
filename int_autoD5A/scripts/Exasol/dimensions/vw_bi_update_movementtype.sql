UPDATE    dim_movementtype mt

   SET mt.Description = t156t_btext
       FROM
          t156t t, dim_movementtype mt
 WHERE mt.RowIsCurrent = 1
      AND    t.t156t_bwart = mt.MovementType
          AND ifnull(t.t156t_sobkz, 'Not Set') = mt.SpecialStockIndicator
          AND ifnull(t.t156t_kzbew, 'Not Set') = mt.MovementIndicator
          AND ifnull(t.t156t_kzzug, 'Not Set') = mt.ReceiptIndicator
          AND ifnull(t.t156t_kzvbr, 'Not Set') = mt.ConsumptionIndicator
;