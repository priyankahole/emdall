/* 	Server: QA
	Process Name: WM Storage Type Transfer
	Interface No: 2
*/
INSERT INTO dim_wmstoragetype
(dim_wmstoragetypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmstoragetype where dim_wmstoragetypeid = 1);

UPDATE    dim_wmstoragetype st
   SET st.StorageTypeDescription = ifnull(t.T301T_LTYPT,'Not Set'),
			st.dw_update_date = current_timestamp  
FROM T301T t ,dim_wmstoragetype st
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	  and st.StorageTypeDescription <> ifnull(t.T301T_LTYPT,'Not Set');
	   
UPDATE    dim_wmstoragetype st	   
set  st.MixedStorage = ifnull(dd1.DD07T_DDTEXT, 'Not Set')
FROM dim_wmstoragetype st,T301T t
 Left join DD07T dd1 on dd1.DD07T_DOMVALUE = t.T331_MISCH
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	   AND dd1.DD07T_DOMNAME = 'T331_MISCH'
	   AND st.MixedStorage = ifnull(dd1.DD07T_DDTEXT, 'Not Set');
	   
UPDATE    dim_wmstoragetype st	   
set  st.NegativeStockQty = ifnull(dd2.DD07T_DDTEXT,'No')
FROM dim_wmstoragetype st,T301T t
 Left join DD07T dd2 on dd2.DD07T_DOMVALUE = t.T331_NEGAT
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	   AND dd2.DD07T_DOMNAME = 'XFELD'
	   AND st.NegativeStockQty <> ifnull(dd2.DD07T_DDTEXT,'No');
	   
UPDATE    dim_wmstoragetype st	   
set  st.PutawayStrategy = ifnull(dd3.DD07T_DDTEXT,'Not Set')
FROM dim_wmstoragetype st,T301T t
 Left join DD07T dd3 on dd3.DD07T_DOMVALUE = t.T331_STEIN
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	   AND dd3.DD07T_DOMNAME = 'T331_STEIN'
	   AND st.PutawayStrategy <> ifnull(dd3.DD07T_DDTEXT,'Not Set');
	   
UPDATE    dim_wmstoragetype st	   
set  st.StkRmvlStrategy = ifnull(dd4.DD07T_DDTEXT,'Not Set')
FROM dim_wmstoragetype st,T301T t
 Left join DD07T dd4 on dd4.DD07T_DOMVALUE = t.T331_STAUS
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	   AND dd4.DD07T_DOMNAME = 'T331_STAUS'
	   AND st.StkRmvlStrategy <> ifnull(dd4.DD07T_DDTEXT,'Not Set');

 
drop table if exists tmp_dwmst_ins_t1;
create table tmp_dwmst_ins_t1
as SELECT 
		      t.T301T_LGNUM WarehouseNumber,
           t.T301T_LGTYP StorageType,
          ifnull(t.T301T_LTYPT,'Not Set') StorageTypeDescription,
          convert(varchar(200), 'Not Set') MixedStorage,             /*ifnull((SELECT dd1.DD07T_DDTEXT FROM DD07T dd1 WHERE dd1.DD07T_DOMNAME = 'T331_MISCH' AND dd1.DD07T_DOMVALUE = t.T331_MISCH), 'Not Set') MixedStorage,*/
          convert(varchar(200), 'Not Set') NegativeStockQty, /*ifnull((SELECT dd2.DD07T_DDTEXT FROM DD07T dd2 WHERE dd2.DD07T_DOMNAME = 'XFELD' AND dd2.DD07T_DOMVALUE = t.T331_NEGAT), 'No') NegativeStockQty,*/
          convert(varchar(200), 'Not Set') PutawayStrategy, /*ifnull((SELECT dd3.DD07T_DDTEXT FROM DD07T dd3 WHERE dd3.DD07T_DOMNAME = 'T331_STEIN' AND dd3.DD07T_DOMVALUE = t.T331_STEIN), 'Not Set') PutawayStrategy,*/
          convert(varchar(200), 'Not Set') StkRmvlStrategy, /* ifnull((SELECT dd4.DD07T_DDTEXT FROM DD07T dd4 WHERE dd4.DD07T_DOMNAME = 'T331_STAUS' AND dd4.DD07T_DOMVALUE = t.T331_STAUS), 'Not Set') StkRmvlStrategy,*/
          current_timestamp RowStartDate,
          1 RowIsCurrent
     FROM T301T t
    WHERE NOT EXISTS
                (SELECT 1
                   FROM dim_wmstoragetype s
                  WHERE s.StorageType = t.T301T_LGTYP
					    AND s.WarehouseNumber = t.T301T_LGNUM
                        AND s.RowIsCurrent = 1) ;
/*MixedStorage*/
UPDATE    tmp_dwmst_ins_t1 st	   
set  st.MixedStorage = ifnull(dd1.DD07T_DDTEXT, 'Not Set')
FROM tmp_dwmst_ins_t1 st,T301T t
 Left join DD07T dd1 on dd1.DD07T_DOMVALUE = t.T331_MISCH
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	   AND dd1.DD07T_DOMNAME = 'T331_MISCH'
	   AND st.MixedStorage = ifnull(dd1.DD07T_DDTEXT, 'Not Set');

/*NegativeStockQty*/	   
UPDATE    tmp_dwmst_ins_t1 st	   
set  st.NegativeStockQty = ifnull(dd2.DD07T_DDTEXT,'No')
FROM tmp_dwmst_ins_t1 st,T301T t
 Left join DD07T dd2 on dd2.DD07T_DOMVALUE = t.T331_NEGAT
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	   AND dd2.DD07T_DOMNAME = 'XFELD'
	   AND st.NegativeStockQty <> ifnull(dd2.DD07T_DDTEXT,'No');

/*PutawayStrategy*/	   
UPDATE    tmp_dwmst_ins_t1 st	   
set  st.PutawayStrategy = ifnull(dd3.DD07T_DDTEXT,'Not Set')
FROM tmp_dwmst_ins_t1 st,T301T t
 Left join DD07T dd3 on dd3.DD07T_DOMVALUE = t.T331_STEIN
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	   AND dd3.DD07T_DOMNAME = 'T331_STEIN'
	   AND st.PutawayStrategy <> ifnull(dd3.DD07T_DDTEXT,'Not Set');

/*	StkRmvlStrategy*/   
UPDATE    tmp_dwmst_ins_t1 st	   
set  st.StkRmvlStrategy = ifnull(dd4.DD07T_DDTEXT,'Not Set')
FROM tmp_dwmst_ins_t1 st,T301T t
 Left join DD07T dd4 on dd4.DD07T_DOMVALUE = t.T331_STAUS
 WHERE st.RowIsCurrent = 1
     AND st.StorageType = t.T301T_LGTYP
	   AND st.WarehouseNumber = t.T301T_LGNUM
	   AND dd4.DD07T_DOMNAME = 'T331_STAUS'
	   AND st.StkRmvlStrategy <> ifnull(dd4.DD07T_DDTEXT,'Not Set');
	   
delete from number_fountain m where m.table_name = 'dim_wmstoragetype';

insert into number_fountain				   
select 	'dim_wmstoragetype',
	ifnull(max(d.dim_wmstoragetypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmstoragetype d
where d.dim_wmstoragetypeid <> 1;
	   

INSERT INTO dim_wmstoragetype(dim_wmstoragetypeid,
                            WarehouseNumber,
							              StorageType,
                            StorageTypeDescription,
                            MixedStorage,
                            NegativeStockQty,
                            PutawayStrategy,
                            StkRmvlStrategy,                            
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmstoragetype') 
          + row_number() over(order by '') ,
		  a.* from(select DISTINCT WarehouseNumber,
							              StorageType,
                            StorageTypeDescription,
                            MixedStorage,
                            NegativeStockQty,
                            PutawayStrategy,
                            StkRmvlStrategy,                            
                            RowStartDate,
                            RowIsCurrent
		               from tmp_dwmst_ins_t1) a;
DROP TABLE IF EXISTS tmp_dwmst_ins_t1;
