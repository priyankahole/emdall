UPDATE    dim_blockingpaymentreason bpr
   SET bpr.ExplainReasonForPaymentBlock = ifnull(T008T_TEXTL, 'Not Set'),
		bpr.dw_update_date = current_timestamp
   FROM
          dim_blockingpaymentreason bpr, T008T t	
       WHERE t.T008T_ZAHLS = bpr.BlockingKeyPayment AND bpr.RowIsCurrent = 1;