
UPDATE    dim_inspectionstage s

   SET s.InspectionStageName = q.QDPST_KURZTEXT
       FROM
          QDPST q, dim_inspectionstage s
 WHERE s.RowIsCurrent = 1
      AND s.InspectionStageCode = q.QDPST_PRSTUFE
          AND s.InspectionStageRuleCode = q.QDPST_DYNREGEL;
