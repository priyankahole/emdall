/* 	Server: QA
	Process Name: PO Transaction Type Transfer
	Interface No: 2
*/

INSERT INTO dim_POTransactionType
(dim_POTransactionTypeid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_POTransactionType where dim_POTransactionTypeid = 1);

UPDATE    dim_POTransactionType pot
   SET pot.POTransactionType = ifnull(dd.DD07T_DDTEXT, 'Not Set'),
			pot.dw_update_date = current_timestamp
FROM dd07t dd, dim_POTransactionType pot
 WHERE pot.RowIsCurrent = 1
 AND dd.DD07T_DOMNAME = 'VGABE'
 AND dd.DD07T_DOMVALUE = pot.POTransactionTypeCode;
 
delete from number_fountain m where m.table_name = 'dim_POTransactionType';

insert into number_fountain				   
select 	'dim_POTransactionType',
	ifnull(max(d.dim_POTransactionTypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_POTransactionType d
where d.dim_POTransactionTypeid <> 1;

INSERT INTO dim_POTransactionType(dim_POTransactionTypeid,
								POTransactionType,
								POTransactionTypeCode,
								RowStartDate,
								RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_POTransactionType') 
          + row_number() over(order by ''),
		 ifnull(dd.DD07T_DDTEXT, 'Not Set'),
		 ifnull(dd.DD07T_DOMVALUE, 'Not Set'),
        current_timestamp,
         1
     FROM dd07t dd
    WHERE dd.DD07T_DDTEXT IS NOT NULL
		  AND dd.DD07T_DOMNAME = 'VGABE'
          AND NOT EXISTS
                 (SELECT 1
                    FROM dim_POTransactionType pot
                   WHERE pot.POTransactionTypeCode = dd.DD07T_DOMVALUE);