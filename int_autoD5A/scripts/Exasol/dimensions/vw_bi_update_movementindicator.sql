UPDATE    dim_movementindicator mi

   SET mi.Description = ifnull(t.DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T t, dim_movementindicator mi
 WHERE mi.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'KZBEW'
          AND mi.TypeCode = ifnull(t.DD07T_DOMVALUE, 'Not Set')
;