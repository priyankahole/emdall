
UPDATE    dim_documentcategory dc

   SET dc.Description = ifnull(DD07T_DDTEXT, 'Not Set')
       FROM
          DD07T dt,  dim_documentcategory dc
 WHERE dc.RowIsCurrent = 1
 AND  dt.DD07T_DOMNAME = 'VBTYP'
          AND dt.DD07T_DOMVALUE IS NOT NULL
          AND dc.DocumentCategory = dt.DD07T_DOMVALUE;

