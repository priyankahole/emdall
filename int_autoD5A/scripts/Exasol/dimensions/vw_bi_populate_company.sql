insert into dim_company(Dim_Companyid,CompanyCode,CompanyName,PostalCode,City,"state",Country,Currency,Language,FiscalYearKey,RowStartDate,RowEndDate,RowIsCurrent,RowChangeReason,ChartOfAccounts,Region,CountryName) 
SELECT 1,'Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set','Not Set',current_timestamp,null,1,null,'Not Set','Not Set','Not Set'
FROM (SELECT 1) a
where not exists(select 1 from dim_company where dim_companyid = 1);


/* Liviu Ionescu - unstable set of rows */
drop table if exists tmp_adrc_upd_company;
create table tmp_adrc_upd_company as
select distinct a.adrc_addrnumber,a.adrc_post_code1, a.adrc_city1, a.adrc_region,
	row_number() over (partition by a.adrc_addrnumber order by '') as rownumber
FROM adrc a;

/* Interface Populate Company Code VW */
 
UPDATE dim_company dc
SET dc.CompanyName = butxt,
    dc.PostalCode =  ifnull(adrc_post_code1, 'Not Set'),
    dc.City = ifnull(adrc_city1, 'Not Set'),
    dc."state" = ifnull(adrc_region, 'Not Set'),
    dc.Country = ifnull(land1, 'Not Set'),
    dc.CountryName = ifnull(t005t_landx, 'Not Set'),
    dc.Currency = waers,
    dc.ChartOfAccounts =  ktopl,
    dc.Language = spras,
    dc.FiscalYearKey = ifnull(periv, 'Not Set'),
	dc.dw_update_date = current_timestamp
FROM dim_company dc  
	inner join t001 t on t.bukrs = dc.CompanyCode
/*           LEFT JOIN adrc x ON t.t001_adrnr = x.adrc_addrnumber - unstable set of rows */
	LEFT JOIN tmp_adrc_upd_company x ON t.t001_adrnr = x.adrc_addrnumber
		and x.rownumber = 1
  	   INNER JOIN t005t y on t.land1 = y.t005t_land1
	WHERE   dc.RowIsCurrent = 1;

drop table if exists tmp_adrc_upd_company;

delete from number_fountain m where m.table_name = 'dim_company';

insert into number_fountain
select 	'dim_company',
	ifnull(max(d.Dim_Companyid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_company d
where d.Dim_Companyid <> 1;

	
INSERT INTO dim_company(Dim_Companyid,
			CompanyCode,
                        CompanyName,
                        PostalCode,
                        City,
                        "state",
                        Country,
                        Currency,
			            ChartOfAccounts,
                        Language,
                        FiscalYearKey,
                        RowStartDate,
                        RowIsCurrent,
			CountryName)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_company') 
          + row_number() over(order by '') ,
			bukrs,
          butxt,
          ifnull(adrc_post_code1, 'Not Set'),
          ifnull(adrc_city1, 'Not Set'),
          ifnull(adrc_region, 'Not Set'),
          ifnull(land1, 'Not Set'),
          waers,
          ktopl,
          spras,
          ifnull(periv, 'Not Set'),
          current_timestamp,
          1,
	  ifnull(t005t_landx, 'Not Set')
     FROM t001 t LEFT JOIN adrc
             ON t.t001_adrnr = adrc_addrnumber and t.SPRAS = ADRC_LANGU
	  INNER JOIN t005t on t.land1 = t005t_land1
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_company
                       WHERE CompanyCode = t.bukrs);

delete from number_fountain m where m.table_name = 'dim_company';
					   
UPDATE dim_company d
SET d.region = s.region,
		d.dw_update_date = current_timestamp
from country_region_mapping s,dim_company d
WHERE 
s.countrycode = d.country
       AND 
ifnull(s.region, 'Not Set') <> ifnull(d.region, 'Not Set');

UPDATE dim_company t
   SET t.region = 'Not Set',
		t.dw_update_date = current_timestamp
 WHERE t.region IS NULL;
 
/* Andrei APP-7785 */
UPDATE dim_company apc
set apc.companycodecountry = CONCAT( apc.CompanyCode , ' - ' , apc.CountryName )
from dim_company apc;


UPDATE dim_company apc
set apc.companycoderegion = CONCAT( apc.CompanyCode , ' - ', CASE WHEN apc.CountryName = 'Australia'
THEN 'APSA'
WHEN apc.CountryName = 'China'
THEN 'APSA'
WHEN apc.CountryName = 'Hong Kong'
THEN 'APSA'
WHEN apc.CountryName = 'India'
THEN 'APSA'
WHEN apc.CountryName = 'Indonesia'
THEN 'APSA'
WHEN apc.CountryName = 'Israel'
THEN 'APSA'
WHEN apc.CountryName = 'Japan'
THEN 'APSA'
WHEN apc.CountryName = 'South Korea'
THEN 'APSA'
WHEN apc.CountryName = 'Malaysia'
THEN 'APSA'
WHEN apc.CountryName = 'Philippines'
THEN 'APSA'
WHEN apc.CountryName = 'Singapore'
THEN 'APSA'
WHEN apc.CountryName = 'South Africa'
THEN 'APSA'
WHEN apc.CountryName = 'Taiwan'
THEN 'APSA'
WHEN apc.CountryName = 'Thailand'
THEN 'APSA'
WHEN apc.CountryName = 'Turkey'
THEN 'APSA'
WHEN apc.CountryName = 'Utd.Arab Emir.'
THEN 'APSA'
WHEN apc.CountryName = 'Vietnam'
THEN 'APSA'
WHEN apc.CountryName = 'Austria'
THEN 'EMEA'
WHEN apc.CountryName = 'Belgium'
THEN 'EMEA'
WHEN apc.CountryName = 'Bulgaria'
THEN 'EMEA'
WHEN apc.CountryName = 'Cyprus'
THEN 'EMEA'
WHEN apc.CountryName = 'Czech Republic'
THEN 'EMEA'
WHEN apc.CountryName = 'Denmark'
THEN 'EMEA'
WHEN apc.CountryName = 'Egypt'
THEN 'EMEA'
WHEN apc.CountryName = 'Finland'
THEN 'EMEA'
WHEN apc.CountryName = 'France'
THEN 'EMEA'
WHEN apc.CountryName = 'Germany' 
THEN 'EMEA'
WHEN apc.CountryName = 'Greece'
THEN 'EMEA'
WHEN apc.CountryName = 'Hungary'
THEN 'EMEA'
WHEN apc.CountryName = 'Ireland'
THEN 'EMEA'
WHEN apc.CountryName = 'Italy'
THEN 'EMEA'
WHEN apc.CountryName = 'Morocco'
THEN 'EMEA'
WHEN apc.CountryName = 'Netherlands'
THEN 'EMEA'
WHEN apc.CountryName = 'New Zealand'
THEN 'EMEA'
WHEN apc.CountryName = 'Norway'
THEN 'EMEA'
WHEN apc.CountryName = 'Poland'
THEN 'EMEA'
WHEN apc.CountryName = 'Romania'
THEN 'EMEA'
WHEN apc.CountryName = 'Russian Federation'
THEN 'EMEA'
WHEN apc.CountryName = 'Spain'
THEN 'EMEA'
WHEN apc.CountryName = 'Sweden'
THEN 'EMEA'
WHEN apc.CountryName = 'Switzerland'
THEN 'EMEA'
WHEN apc.CountryName = 'Uruguay'
THEN 'EMEA'
WHEN apc.CountryName = 'United Kingdom'
THEN 'EMEA' 
WHEN apc.CountryName = 'Argentina'
THEN 'Latin America'
WHEN apc.CountryName = 'Brazil'
THEN 'Latin America'
WHEN apc.CountryName = 'Chile'
THEN 'Latin America'
WHEN apc.CountryName = 'Colombia'
THEN 'Latin America'
WHEN apc.CountryName = 'Costa Rica'
THEN 'Latin America'
WHEN apc.CountryName = 'Ecuador'
THEN 'Latin America'
WHEN apc.CountryName = 'Mexico'
THEN 'Latin America'
WHEN apc.CountryName = 'Panama'
THEN 'Latin America'
WHEN apc.CountryName = 'Peru'
THEN 'Latin America'
WHEN apc.CountryName = 'Venezuela'
THEN 'Latin America'
WHEN apc.CountryName = 'Canada'
THEN 'US/PR/Canada'
WHEN apc.CountryName = 'USA'
THEN 'US/PR/Canada'
ELSE ' '
END
)
from dim_company apc;

UPDATE dim_company apc
set apc.companyregion = 
(CASE WHEN apc.CountryName = 'Australia'
THEN 'APSA'
WHEN apc.CountryName = 'China'
THEN 'APSA'
WHEN apc.CountryName = 'Hong Kong'
THEN 'APSA'
WHEN apc.CountryName = 'India'
THEN 'APSA'
WHEN apc.CountryName = 'Indonesia'
THEN 'APSA'
WHEN apc.CountryName = 'Israel'
THEN 'APSA'
WHEN apc.CountryName = 'Japan'
THEN 'APSA'
WHEN apc.CountryName = 'South Korea'
THEN 'APSA'
WHEN apc.CountryName = 'Malaysia'
THEN 'APSA'
WHEN apc.CountryName = 'Philippines'
THEN 'APSA'
WHEN apc.CountryName = 'Singapore'
THEN 'APSA'
WHEN apc.CountryName = 'South Africa'
THEN 'APSA'
WHEN apc.CountryName = 'Taiwan'
THEN 'APSA'
WHEN apc.CountryName = 'Thailand'
THEN 'APSA'
WHEN apc.CountryName = 'Turkey'
THEN 'APSA'
WHEN apc.CountryName = 'Utd.Arab Emir.'
THEN 'APSA'
WHEN apc.CountryName = 'Vietnam'
THEN 'APSA'
WHEN apc.CountryName = 'Austria'
THEN 'EMEA'
WHEN apc.CountryName = 'Belgium'
THEN 'EMEA'
WHEN apc.CountryName = 'Bulgaria'
THEN 'EMEA'
WHEN apc.CountryName = 'Cyprus'
THEN 'EMEA'
WHEN apc.CountryName = 'Czech Republic'
THEN 'EMEA'
WHEN apc.CountryName = 'Denmark'
THEN 'EMEA'
WHEN apc.CountryName = 'Egypt'
THEN 'EMEA'
WHEN apc.CountryName = 'Finland'
THEN 'EMEA'
WHEN apc.CountryName = 'France'
THEN 'EMEA'
WHEN apc.CountryName = 'Germany' 
THEN 'EMEA'
WHEN apc.CountryName = 'Greece'
THEN 'EMEA'
WHEN apc.CountryName = 'Hungary'
THEN 'EMEA'
WHEN apc.CountryName = 'Ireland'
THEN 'EMEA'
WHEN apc.CountryName = 'Italy'
THEN 'EMEA'
WHEN apc.CountryName = 'Morocco'
THEN 'EMEA'
WHEN apc.CountryName = 'Netherlands'
THEN 'EMEA'
WHEN apc.CountryName = 'New Zealand'
THEN 'EMEA'
WHEN apc.CountryName = 'Norway'
THEN 'EMEA'
WHEN apc.CountryName = 'Poland'
THEN 'EMEA'
WHEN apc.CountryName = 'Romania'
THEN 'EMEA'
WHEN apc.CountryName = 'Russian Federation'
THEN 'EMEA'
WHEN apc.CountryName = 'Spain'
THEN 'EMEA'
WHEN apc.CountryName = 'Sweden'
THEN 'EMEA'
WHEN apc.CountryName = 'Switzerland'
THEN 'EMEA'
WHEN apc.CountryName = 'Uruguay'
THEN 'EMEA'
WHEN apc.CountryName = 'United Kingdom'
THEN 'EMEA' 
WHEN apc.CountryName = 'Portugal'
THEN 'EMEA' 
WHEN apc.CountryName = 'Argentina'
THEN 'Latin America'
WHEN apc.CountryName = 'Brazil'
THEN 'Latin America'
WHEN apc.CountryName = 'Chile'
THEN 'Latin America'
WHEN apc.CountryName = 'Colombia'
THEN 'Latin America'
WHEN apc.CountryName = 'Costa Rica'
THEN 'Latin America'
WHEN apc.CountryName = 'Ecuador'
THEN 'Latin America'
WHEN apc.CountryName = 'Mexico'
THEN 'Latin America'
WHEN apc.CountryName = 'Panama'
THEN 'Latin America'
WHEN apc.CountryName = 'Peru'
THEN 'Latin America'
WHEN apc.CountryName = 'Venezuela'
THEN 'Latin America'
WHEN apc.CountryName = 'Canada'
THEN 'US/PR/Canada'
WHEN apc.CountryName = 'USA'
THEN 'US/PR/Canada'
ELSE ' '
END
)
from dim_company apc;

