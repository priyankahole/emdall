/*   Script         :    */
/*   Author         : George */
/*   Created On     : 30 Sep 2014 */
/*   Description    : Custom Term Dimension script. Updating fields from imported file  */

/* Madalina 6 Dec 2016 - Add insert from CSV table - BI-4413*/
delete from number_fountain m where m.table_name = 'dim_term';

insert into number_fountain
select 	'dim_term',
	ifnull(max(d.Dim_Termid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_term d
where d.Dim_Termid <> 1;	

INSERT INTO dim_term(Dim_Termid,
                     TermCode,
                     RowStartDate,
                     RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_term') + row_number() over(order by ''),
                   lpad(m.PaymentTermCode,4,'0'),
                   current_timestamp,
                   1
    FROM merck_import_PaymentTermsCategories m
    WHERE PaymentTermCode IS NOT NULL
          AND NOT EXISTS
                 (SELECT 1
                    FROM dim_term
                   WHERE termcode = lpad(m.PaymentTermCode,4,'0'));
/* END BI-4413 */

Update dim_term dt
set TermCategory_merck = ifnull(m.TermCategory,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from merck_import_PaymentTermsCategories m, dim_term dt
where dt.termcode = lpad(m.PaymentTermCode,4,'0')
and TermCategory_merck <> ifnull(m.TermCategory,'Not Set');

Update dim_term dt
set TermBucket_merck = ifnull(m.TermBucket,'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
from merck_import_PaymentTermsCategories m, dim_term dt
where dt.termcode = lpad(m.PaymentTermCode,4,'0')
and TermBucket_merck <> ifnull(m.TermBucket,'Not Set');

/*Alin 2 June 2018 APP-7276*/
merge into dim_term d
using(
select distinct dim_termid, max(ifnull(m.Z1MM_FO_PAYM_CATEGORY_TERM,'Not Set')) as Z1MM_FO_PAYM_CATEGORY_TERM
from Z1MM_FO_PAYM m, dim_term dt
where dt.termcode = ifnull(Z1MM_FO_PAYM_ZTERM, 'Not Set')
group by dim_termid
)t
on t.dim_termid = d.dim_termid
when matched then update
set d.CATEGORY_TERM = t.Z1MM_FO_PAYM_CATEGORY_TERM
where d.CATEGORY_TERM <> t.Z1MM_FO_PAYM_CATEGORY_TERM;


merge into dim_term d
using(
select distinct dim_termid, max(ifnull(m.Z1MM_FO_PAYM_BUCKET_TERM,'Not Set')) as Z1MM_FO_PAYM_BUCKET_TERM
from Z1MM_FO_PAYM m, dim_term dt
where dt.termcode = ifnull(Z1MM_FO_PAYM_ZTERM, 'Not Set')
group by dim_termid
)t
on t.dim_termid = d.dim_termid
when matched then update
set d.BUCKET_TERM = t.Z1MM_FO_PAYM_BUCKET_TERM
where d.BUCKET_TERM <> t.Z1MM_FO_PAYM_BUCKET_TERM;