/* START OF CODE BLOCKS - tmp_getcostingdate would have the final the data    */

DELETE FROM tmp_getcostingdate
WHERE fact_script_name = 'bi_populate_purchasing_fact';

Drop table if exists getcostingdate_sub99;

Create table getcostingdate_sub99 LIKE tmp_getcostingdate INCLUDING DEFAULTS INCLUDING IDENTITY;

drop table if exists ekko_ekpo_eket_jj890;

create table ekko_ekpo_eket_jj890 as select * from ekko_ekpo_eket;

/* 18Jul2015 - Creating Temp table to avoid using subquery in the select. Done to fix Consistency Check error.*/
drop table if exists tmp_minDate_cstDate;
CREATE table tmp_minDate_cstDate
as
SELECT x.EKPO_EBELN as DocNum, x.EKPO_EBELP as DocItm,MIN(x.EKET_BEDAT) as minDate
FROM ekko_ekpo_eket_jj890 x, fact_purchase fp
WHERE x.EKPO_EBELN = fp.dd_DocumentNo
AND x.EKPO_EBELP = fp.dd_DocumentItemNo
GROUP BY x.EKPO_EBELN,x.EKPO_EBELP;

INSERT INTO getcostingdate_sub99
(pCompanyCode,
pPlant,
pMaterialNo,
pFiYear,
pPeriod,
fact_script_name
)
SELECT EKPO_BUKRS pCompanyCode, EKPO_WERKS pPlant, EKPO_MATNR pMaterialNo, dt.FinancialYear pFiYear, dt.FinancialMonthNumber pPeriod,
         'bi_populate_purchasing_fact' fact_script_name
FROM ekko_ekpo_eket, dim_date dt,fact_purchase fp,tmp_minDate_cstDate t
WHERE     fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND fp.dd_DocumentNo = t.DocNum
AND fp.dd_DocumentItemNo = t.DocItm
AND dt.DateValue = t.minDate
AND dt.CompanyCode = EKPO_BUKRS
AND fp.amt_StdUnitPrice = 0;

drop table if exists ekko_ekpo_eket_jj890;


INSERT INTO tmp_getcostingdate
(	pCompanyCode,
	pPlant,
	pMaterialNo,
	pFiYear,
	pPeriod,
	fact_script_name
)
select distinct 
	pCompanyCode,
	pPlant,
	pMaterialNo,
	pFiYear,
	pPeriod,
	fact_script_name
from getcostingdate_sub99;


Drop table if exists getcostingdate_sub99;
drop table if exists tmp_minDate_cstDate;

