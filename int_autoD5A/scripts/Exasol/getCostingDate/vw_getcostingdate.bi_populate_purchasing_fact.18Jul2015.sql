/* START OF CODE BLOCKS - tmp_getcostingdate would have the final the data    */

DELETE FROM tmp_getcostingdate
WHERE fact_script_name = 'bi_populate_purchasing_fact';

Drop table if exists getcostingdate_sub99;

Create table getcostingdate_sub99 as select * from tmp_getcostingdate where 1=2;

drop table if exists ekko_ekpo_eket_jj890;

create table ekko_ekpo_eket_jj890 as select * from ekko_ekpo_eket;

INSERT INTO getcostingdate_sub99
(pCompanyCode,
pPlant,
pMaterialNo,
pFiYear,
pPeriod,
fact_script_name
)
SELECT EKPO_BUKRS pCompanyCode, EKPO_WERKS pPlant, EKPO_MATNR pMaterialNo, dt.FinancialYear pFiYear, dt.FinancialMonthNumber pPeriod,
         'bi_populate_purchasing_fact' fact_script_name
FROM ekko_ekpo_eket, dim_date dt,fact_purchase fp
WHERE     fp.dd_DocumentNo = EKPO_EBELN
AND fp.dd_DocumentItemNo = EKPO_EBELP
AND fp.dd_ScheduleNo = EKET_ETENR
AND dt.DateValue = (select min(x.EKET_BEDAT) from ekko_ekpo_eket_jj890 x
                                 where x.EKPO_EBELN = fp.dd_DocumentNo and x.EKPO_EBELP = fp.dd_DocumentItemNo)
AND dt.CompanyCode = EKPO_BUKRS
AND fp.amt_StdUnitPrice = 0;

drop table if exists ekko_ekpo_eket_jj890;

INSERT INTO tmp_getcostingdate
(	pCompanyCode,
	pPlant,
	pMaterialNo,
	pFiYear,
	pPeriod,
	fact_script_name
)
select distinct 
	pCompanyCode,
	pPlant,
	pMaterialNo,
	pFiYear,
	pPeriod,
	fact_script_name
from getcostingdate_sub99;


Drop table if exists getcostingdate_sub99;

CALL VECTORWISE( COMBINE 'tmp_getcostingdate');


