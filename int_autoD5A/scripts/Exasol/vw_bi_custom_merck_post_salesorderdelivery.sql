/*   Script         : vw_bi_custom_merck_post_salesorderdelivery.sql                                                  	    */
/*   Author         : Nicoleta C                                                                   			 	    */
/*   Created On     : Mar 2015	                                                                       		        */ 
/*   Description    : vw_bi_custom_merck_post_salesorderdelivery - Script for post populating fact_salesorder for Merck  */




/*********************************************Change History*************************************************************/

/*  Date             By         Version	    Desc                                                  			            */
/*  23 Mar 2015	     Nicoleta C	1.0	    	Script for post populating fact_salesorder 									*/

/************************************************************************************************************************/

UPDATE fact_salesorderdelivery sod
SET sod.ct_ConfirmedQtyBaseUoM = ifnull(ct_ConfirmedQty * convert(decimal(18,2),LIPS_UMVKZ) / convert(decimal(18,2),LIPS_UMVKN), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM LIKP_LIPS, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
and sod.ct_ConfirmedQtyBaseUoM <> ifnull(ct_ConfirmedQty * convert(decimal(18,2),LIPS_UMVKZ) / convert(decimal(18,2),LIPS_UMVKN), 0);

UPDATE fact_salesorderdelivery sod
SET sod.ct_QtyDeliveredBaseUoM = ifnull(ct_QtyDelivered* convert(decimal(18,2),LIPS_UMVKZ) / convert(decimal(18,2),LIPS_UMVKN), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM LIKP_LIPS, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
and sod.ct_QtyDeliveredBaseUoM <> ifnull(ct_QtyDelivered* convert(decimal(18,2),LIPS_UMVKZ) / convert(decimal(18,2),LIPS_UMVKN), 0);

UPDATE fact_salesorderdelivery sod
SET sod.ct_ScheduleQtyBaseUoM = ifnull(ct_ScheduleQtySalesUnit* convert(decimal(18,2),LIPS_UMVKZ) / convert(decimal(18,2),LIPS_UMVKN), 0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM LIKP_LIPS, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
and sod.ct_ScheduleQtyBaseUoM <> ifnull(ct_ScheduleQtySalesUnit* convert(decimal(18,2),LIPS_UMVKZ) / convert(decimal(18,2),LIPS_UMVKN), 0);

UPDATE fact_salesorderdelivery sod
SET sod.ct_ShippedOrderQtyBaseUoM = ifnull(LIPS_LGMNG,0)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM LIKP_LIPS, fact_salesorderdelivery sod 
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
  and sod.ct_ShippedOrderQtyBaseUoM <> ifnull(LIPS_LGMNG,0);
  
  
MERGE INTO fact_salesorder so
USING (
select sum(f.ct_ShippedOrderQtyBaseUoM ) as ct_ShippedOrderQtyBaseUoM, f.dd_SalesDocNo,f.dd_SalesItemNo, f.dd_ScheduleNo from fact_salesorderdelivery f, dim_salesorderitemstatus sois,fact_salesorder so
                           where f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid and 
				f.dd_SalesDocNo = so.dd_SalesDocNo
                                and f.dd_SalesItemNo = so.dd_SalesItemNo
                                and f.dd_ScheduleNo = so.dd_ScheduleNo
                                and sois.GoodsMovementStatus <> 'Not yet processed'
GROUP BY  f.dd_SalesDocNo,f.dd_SalesItemNo, f.dd_ScheduleNo) t on  so.dd_SalesDocNo=t.dd_SalesDocNo AND so.dd_SalesItemNo=t.dd_SalesItemNo AND so.dd_ScheduleNo=t.dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET so.ct_ShippedOrderQtyBaseUoM=t.ct_ShippedOrderQtyBaseUoM
WHERE so.ct_ShippedOrderQtyBaseUoM<>t.ct_ShippedOrderQtyBaseUoM;
 
 update fact_salesorderdelivery f
set  ct_ScheduleQtyBaseUoMIRU = ifnull(f.ct_ScheduleQtyBaseUoM ,0)/marm_umrez 
,ct_QtyDeliveredBaseUoMIRU = ifnull(f.ct_QtyDeliveredBaseUoM ,0)/marm_umrez 
,ct_ConfirmedQtyBaseUoMIRU = ifnull(f.ct_ConfirmedQtyBaseUoM,0)/marm_umrez 
,ct_ShippedOrderQtyBaseUoMIRU  = ifnull(f.ct_ShippedOrderQtyBaseUoM,0)/marm_umrez 

from dim_part dp, MARM m, fact_salesorderdelivery f
WHERE f.dim_partid = dp.dim_partid
	and m.marm_matnr =dp.partnumber
	and marm_meinh = 'IRU';
	
update fact_salesorder f
set  ct_ShippedOrderQtyBaseUoMIRU  = ifnull(f.ct_ShippedOrderQtyBaseUoM,0)/marm_umrez 
from dim_part dp, MARM m,fact_salesorder f
WHERE f.dim_partid = dp.dim_partid
	and m.marm_matnr =dp.partnumber
	and marm_meinh = 'IRU';
	


UPDATE fact_salesorderdelivery sod
SET sod.dd_salesorderuom  = ifnull(LIPS_VRKME, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM LIKP_LIPS, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
and sod.dd_salesorderuom  <> ifnull(LIPS_VRKME, 'Not Set');

UPDATE fact_salesorderdelivery sod
SET sod.dd_baseunitofmeasure = ifnull(LIPS_MEINS, 'Not Set')
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM LIKP_LIPS,fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
and sod.dd_baseunitofmeasure  <> ifnull(LIPS_MEINS, 'Not Set');

update fact_salesorderdelivery f
set f.ct_leadtime = ifnull(pt.leadtime,0)
from dim_part pt, fact_salesorderdelivery f
where f.dim_partid = pt.dim_partid
and f.ct_leadtime <> ifnull(pt.leadtime,0);

update fact_salesorderdelivery f
set f.ct_leadtimedest = ifnull(destpart.leadtime,0)
from dim_part pt, dim_plant pld, dim_part destpart,fact_salesorderdelivery f
where f.dim_partid = pt.dim_partid and f.dim_plantidordering = pld.dim_plantid
and destpart.partnumber = pt.partnumber and destpart.plant = pld.plantcode
and f.ct_leadtimedest <> ifnull(destpart.leadtime,0);

UPDATE fact_salesorderdelivery sd
    SET sd.dim_dateidsocreated = f.dim_dateidsocreated
From fact_salesorder f,  fact_salesorderdelivery sd /*, VBAK_VBAP_VBEP v*/
WHERE   /*  f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR 
        AND */ sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo
        and sd.dim_dateidsocreated <> f.dim_dateidsocreated;

/*13 Jan 2016 Georgiana Unable to get a stable source of rows error- taking the max of dim_dateidsocreated*/
drop table if exists tmp_socreated_upd;     
 create table tmp_socreated_upd as   
 select distinct sd.dd_SalesDocNo,sd.dd_SalesItemNo, max(f.dim_dateidsocreated) as dim_dateidsocreated
 from fact_salesorderdelivery sd
,fact_salesorder f   
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        and sd.dim_dateidsocreated = 1
group by sd.dd_SalesDocNo,sd.dd_SalesItemNo;     
        
UPDATE fact_salesorderdelivery sd
    SET sd.dim_dateidsocreated = f.dim_dateidsocreated
From tmp_socreated_upd f, fact_salesorderdelivery sd 
WHERE   sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        and sd.dim_dateidsocreated = 1; 
drop table if exists tmp_socreated_upd;

update fact_salesorderdelivery f
set f.ct_actualtransportleadtimecaldays = ifnull(r.transittimeincalendardays,0)
from dim_route r,fact_salesorderdelivery f
where f.dim_actualrouteid = r.dim_routeid
and f.ct_actualtransportleadtimecaldays <> ifnull(r.transittimeincalendardays,0);

update fact_salesorderdelivery f
set f.ct_defaulttransportleadtimecaldays = ifnull(r.transittimeincalendardays,0)
from dim_route r,fact_salesorderdelivery f
where f.dim_defaultrouteid = r.dim_routeid
and f.ct_defaulttransportleadtimecaldays <> ifnull(r.transittimeincalendardays,0);        

update
fact_salesorderdelivery f 
set ct_totalweightheaderkg = ct_totalweightheader* T006_ZAEHL/T006_NENNR
from
t006 t, Dim_UnitOfMeasure u,fact_salesorderdelivery f 
where f.dim_unitofmeasureidtotalweight = u.Dim_UnitOfMeasureid
and u.uom = t.T006_MSEHI
and ct_totalweightheaderkg <> ct_totalweightheader* T006_ZAEHL/T006_NENNR;

update
fact_salesorderdelivery f 
set ct_grossweightkg = ct_grossweight* T006_ZAEHL/T006_NENNR
from
t006 t, Dim_UnitOfMeasure u,fact_salesorderdelivery f 
where f.dim_unitofmeasureidtotalweightline = u.Dim_UnitOfMeasureid
and u.uom = t.T006_MSEHI
and ct_grossweightkg <> ct_grossweight* T006_ZAEHL/T006_NENNR;
	 


/* BI-309 */
 update dim_route r
 set r.shippingtypedesc = t.shippingtypedesc
 from dim_tranportbyshippingtype t,dim_route r
where t.ShippingType = r.ShippingType 
 and r.shippingtypedesc <> t.shippingtypedesc;
