
/*   17 Sep 2013      Lokesh	1.1  		  Changed queries to use BSIK and BSAK so that there is no need to split AR script */



/* Populate Tran->Global curr */

/* Use posting date(BUDAT) for global rate. */

DELETE FROM tmp_getExchangeRate1
WHERE fact_script_name = 'bi_populate_accountspayable_fact';

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSIK_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSIK_BUDAT pDate,NULL exchangeRate,'bi_populate_accountspayable_fact'
FROM   BSIK arc;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAK_WAERS pFromCurrency,'USD' pToCurrency,NULL pFromExchangeRate,BSAK_BUDAT pDate,NULL exchangeRate,'bi_populate_accountspayable_fact'
FROM BSAK arc;

UPDATE tmp_getExchangeRate1
SET pToCurrency = ifnull((SELECT property_value
                                FROM systemproperty
                               WHERE property = 'customer.global.currency'),
                              'USD')
WHERE fact_script_name = 'bi_populate_accountspayable_fact';

/* Insert Tran--> Local curr mappings - again with posting date */

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSIK_WAERS pFromCurrency,dc.Currency pToCurrency,NULL pFromExchangeRate,BSIK_BUDAT pDate,NULL exchangeRate,'bi_populate_accountspayable_fact'
FROM   BSIK arc,dim_company dc
WHERE dc.CompanyCode = arc.BSIK_BUKRS;

INSERT INTO tmp_getExchangeRate1 (pFromCurrency,pToCurrency,pFromExchangeRate,pDate,exchangeRate,fact_script_name)
SELECT DISTINCT BSAK_WAERS pFromCurrency,dc.Currency pToCurrency,NULL pFromExchangeRate,BSAK_BUDAT pDate,NULL exchangeRate,'bi_populate_accountspayable_fact'
FROM BSAK arc,dim_company dc
WHERE dc.CompanyCode = arc.BSAK_BUKRS;

/* Remove duplicates */

drop table if exists tmp_getExchangeRate1_fact_accountspayable_nodups;
create table tmp_getExchangeRate1_fact_accountspayable_nodups
as
select distinct * from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_accountspayable_fact';

delete from tmp_getExchangeRate1 where fact_script_name = 'bi_populate_accountspayable_fact';

insert into tmp_getExchangeRate1
select * from tmp_getExchangeRate1_fact_accountspayable_nodups;

drop table tmp_getExchangeRate1_fact_accountspayable_nodups;
