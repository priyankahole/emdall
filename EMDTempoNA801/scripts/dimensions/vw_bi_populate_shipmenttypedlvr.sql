INSERT INTO dim_shipment(dim_shipmentid,
                          Salesdeldocno,
                          ShipmentType,  
                          ShipmentTypeDescription)
   SELECT 1,
          'Not Set',
          'Not Set',
          'Not Set'
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_shipment
               WHERE dim_shipmentid = 1);
			   
			   
UPDATE dim_shipment d
SET ShipmentType = IFNULL(LIKP_VSBED,'Not Set')
FROM  dim_shipment d, LIKP_LIPS 
WHERE Salesdeldocno = LIKP_VBELN 
  AND ShipmentType <> IFNULL(LIKP_VSBED,'Not Set');
 

DELETE FROM number_fountain m where m.table_name = 'dim_shipment';

INSERT INTO number_fountain
SELECT 	'dim_shipment',
	IFNULL(max(d.dim_shipmentid), 
	IFNULL((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_shipment d
where d.dim_shipmentid <> 1; 

  INSERT INTO dim_shipment 
(
	  dim_shipmentid,
      Salesdeldocno,
      ShipmentType,  
      ShipmentTypeDescription
)
   SELECT ((select IFNULL(m.max_id, 1) from number_fountain m where m.table_name = 'dim_shipment') + row_number() over(order by '')),t.*
FROM (SELECT DISTINCT 
          LIKP_VBELN,
          IFNULL(LIKP_VSBED,'Not Set'),
          IFNULL(TVSBT_VTEXT,'Not Set')
  FROM  LIKP_LIPS
		LEFT JOIN TVSBT tv ON  tv.TVSBT_VSBED = LIKP_VSBED
  WHERE not exists (select 1 from dim_shipment s where Salesdeldocno = LIKP_VBELN)) t;