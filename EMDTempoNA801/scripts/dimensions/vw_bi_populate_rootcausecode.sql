UPDATE    
dim_rootcausecode ds
SET ds.Description = ifnull(dt.DESCRIPTION, 'Not Set'),
	ds.dw_update_date = current_timestamp
FROM
dim_rootcausecode ds,
UDMATTR_RCCODET dt
WHERE ds.RowIsCurrent = 1
  AND ds.RootCCode = dt.ROOT_CCODE
  AND dt.ROOT_CCODE IS NOT NULL
;

INSERT INTO dim_rootcausecode(dim_rootcausecodeId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_rootcausecode
               WHERE dim_rootcausecodeId = 1);

delete from number_fountain m where m.table_name = 'dim_rootcausecode';

insert into number_fountain
select 	'dim_rootcausecode',
	ifnull(max(d.dim_rootcausecodeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_rootcausecode d
where d.dim_rootcausecodeid <> 1; 

INSERT INTO dim_rootcausecode(dim_rootcausecodeid,
			       CaseType,
			       RootCCode,
                               Description,
                               RowStartDate,
                               RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_rootcausecode') 
          + row_number() over(order by '') ,
			 ifnull(CASE_TYPE, 'Not Set'),
			 ifnull(ROOT_CCODE, 'Not Set'),
            ifnull(DESCRIPTION,'Not Set'),
            current_timestamp,
            1
       FROM UDMATTR_RCCODET
      WHERE NOT EXISTS
                  (SELECT 1
                     FROM dim_rootcausecode
                    WHERE RootCCode = ROOT_CCODE)
   ORDER BY 2
;
