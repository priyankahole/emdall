UPDATE    dim_documenttypetext dt
   SET dt.Description = t.T003T_LTEXT,
			dt.dw_update_date = current_timestamp
	FROM
          dim_documenttypetext dt, T003T t
   WHERE dt.Type = t.T003T_BLART AND dt.RowIsCurrent = 1;
   
INSERT INTO dim_documenttypetext(dim_documenttypetextid, RowIsCurrent,type,description,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_documenttypetext
               WHERE dim_documenttypetextid = 1);

delete from number_fountain m where m.table_name = 'dim_documenttypetext';
   
insert into number_fountain
select 	'dim_documenttypetext',
	ifnull(max(d.dim_documenttypetextid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_documenttypetext d
where d.dim_documenttypetextid <> 1; 

INSERT INTO dim_documenttypetext(dim_documenttypetextid,
                                                                Type,
                                 Description,
                                 RowStartDate,
                                 RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_documenttypetext')
          + row_number() over(order by '') ,
          ifnull(t.T003T_BLART, 'Not Set'), 
          ifnull(t.T003T_LTEXT, 'Not Set'),
          current_timestamp,
          1
     FROM T003T t
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_documenttypetext dt
                       WHERE dt.Type = t.T003T_BLART AND dt.RowIsCurrent = 1);
   