INSERT INTO DIM_BOMHEADER (DIM_BOMHEADERID)
select 1
from (select 1) a
where not exists (select '1' from DIM_BOMHEADER where DIM_BOMHEADERID = 1);

delete from number_fountain m where m.table_name = 'dim_bomheader';

insert into number_fountain
select 	'dim_bomheader',
	ifnull(max(d.DIM_BOMHEADERID), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_bomheader d
where d.DIM_BOMHEADERID <> 1;


INSERT INTO DIM_BOMHEADER (DIM_BOMHEADERID,
                           BOMCategory,
                           BillOfMaterial,
						   AlternativeBOM,
						   InternalCounter,
						   ChangeNo,
						   DW_UPDATE_DATE,
						   DW_INSERT_DATE,
						   RowStartDate,
						   RowEndDate,
						   rowchangereason,
						   RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_bomheader')
          +  row_number() over(order by '') as DIM_BOMHEADERID,
		STKO_STLTY,
		STKO_STLNR,
		STKO_STLAL,
		STKO_STKOZ,
		ifnull(STKO_AENNR,'Not Set'),
		current_timestamp,
		current_timestamp,
		null,
		null,
		'Not Set',
		1
from BOMV2_STKO S 
 WHERE NOT EXISTS
	(SELECT 1  FROM DIM_BOMHEADER ds where s.STKO_STLTY = ds.BOMCategory
	                                   and s.STKO_STLNR = ds.BillOfMaterial
									   and s.STKO_STLAL = ds.AlternativeBOM
									   and s.STKO_STKOZ = ds.InternalCounter) AND s.STKO_LKENZ is null AND s.STKO_LOEKZ is null;
									   -- AND s.STKO_STLST in ('10','15','16','12','11','17') 
									   
UPDATE    DIM_BOMHEADER ds
   SET ds.ChangeNo = ifnull(STKO_AENNR, 'Not Set'),
        DW_UPDATE_DATE =  current_timestamp
       FROM
          BOMV2_STKO s ,DIM_BOMHEADER ds		
 WHERE ds.RowIsCurrent = 1
 AND s.STKO_STLTY = ds.BOMCategory
 and s.STKO_STLNR = ds.BillOfMaterial
 and s.STKO_STLAL = ds.AlternativeBOM
 and s.STKO_STKOZ = ds.InternalCounter;
 
 
 UPDATE    DIM_BOMHEADER ds
   SET ds.BaseQty = ifnull(STKO_BMENG, 0),
        DW_UPDATE_DATE =  current_timestamp
       FROM
          BOMV2_STKO s ,DIM_BOMHEADER ds		
 WHERE ds.RowIsCurrent = 1
 AND s.STKO_STLTY = ds.BOMCategory
 and s.STKO_STLNR = ds.BillOfMaterial
 and s.STKO_STLAL = ds.AlternativeBOM
 and s.STKO_STKOZ = ds.InternalCounter
 and ds.BaseQty <> ifnull(STKO_BMENG, 0);
 
 
  UPDATE    DIM_BOMHEADER ds
   SET ds.ValidFromStko = STKO_DATUV,
        DW_UPDATE_DATE =  current_timestamp
       FROM
          BOMV2_STKO s ,DIM_BOMHEADER ds		
 WHERE ds.RowIsCurrent = 1
 AND s.STKO_STLTY = ds.BOMCategory
 and s.STKO_STLNR = ds.BillOfMaterial
 and s.STKO_STLAL = ds.AlternativeBOM
 and s.STKO_STKOZ = ds.InternalCounter;
 
 
  
  UPDATE    DIM_BOMHEADER ds
   SET ds.DateRecordCreatedOnStko = STKO_ANDAT,
        DW_UPDATE_DATE =  current_timestamp
       FROM
          BOMV2_STKO s , DIM_BOMHEADER ds		
 WHERE ds.RowIsCurrent = 1
 AND s.STKO_STLTY = ds.BOMCategory
 and s.STKO_STLNR = ds.BillOfMaterial
 and s.STKO_STLAL = ds.AlternativeBOM
 and s.STKO_STKOZ = ds.InternalCounter;

 drop table if exists BOMV2_tmp_validto;
 
 create table BOMV2_tmp_validto as 
 select dim_bomheaderid,billofmaterial,alternativebom,validfromstko,
	ifnull(lead(validfromstko,1) over (partition by billofmaterial,alternativebom order by validfromstko) - 1,'9999-01-01') as validto
 from dim_bomheader ;
 
 
 UPDATE    DIM_BOMHEADER ds
   SET ds.validtodate = t.validto
       ,DW_UPDATE_DATE =  current_timestamp
       FROM
           BOMV2_tmp_validto t , DIM_BOMHEADER ds	   
 WHERE ds.RowIsCurrent = 1
 AND t.dim_bomheaderid = ds.dim_bomheaderid;
 -- and ds.validtodate <> t.validto
 
 
update dim_bomheader
set unique_header = hash_md5(concat(bomcategory,billofmaterial,alternativebom,InternalCounter))
where unique_header <> hash_md5(concat(bomcategory,billofmaterial,alternativebom,InternalCounter));

UPDATE dim_bomheader
SET PROJECTSOURCEID = 3
WHERE PROJECTSOURCEID <> 3;
