
INSERT INTO dim_plant
(dim_plantid
,RowIsCurrent)
SELECT 1, 1
FROM (SELECT 1) a
WHERE NOT EXISTS ( SELECT 'x' FROM dim_plant WHERE dim_plantid = 1);

UPDATE dim_plant
SET CompanyCode = k.BUKRS,
       Name = t.NAME1,
       PostalCode = ifnull(PSTLZ, 'Not Set'),
       City = ifnull(ORT01, 'Not Set'),
       "state" = ifnull(REGIO, 'Not Set'),
       Country = ifnull(LAND1, 'Not Set'),
       SalesOrg = case when SalesOrg = 'Not Set' then SalesOrg
 else ifnull(T001W_VKORG, 'Not Set') end,
       PurchOrg =
          CASE
             WHEN PurchOrg = 'Not Set' THEN PurchOrg
             ELSE ifnull(EKORG, WERKS)
          END,
       PlanningPlant = ifnull(T001W_IWERK, 'Not Set'),
       ValuationArea = t.BWKEY,
       FactoryCalendarKey = ifnull(t.FABKL, 'Not Set'),
       LanguageKey = ifnull(t.SPRAS, 'E'),
       CountryName = ifnull(t005t_landx, 'Not Set'),
			dw_update_date = current_timestamp
FROM T001W t, t001k k, T005T,dim_plant
 WHERE t.bwkey = k.bwkey AND PlantCode = t.werks AND t.LAND1 = T005T_LAND1;


DROP TABLE IF EXISTS tmp_dim_plant_insert;
CREATE TABLE tmp_dim_plant_insert
AS
SELECT convert(varchar(7),WERKS) PlantCode
FROM t001w;

DROP TABLE IF EXISTS tmp_dim_plant_delete;
CREATE TABLE tmp_dim_plant_delete
AS
SELECT *
FROM tmp_dim_plant_insert
WHERE 1=2;

INSERT INTO tmp_dim_plant_delete
SELECT PlantCode
FROM dim_plant;


merge into tmp_dim_plant_insert  i using tmp_dim_plant_delete d
on i.PLANTCODE = d.PLANTCODE
when matched then delete ;

DROP TABLE IF EXISTS tmp_dim_plant_insert2;
CREATE TABLE tmp_dim_plant_insert2
AS
SELECT t.* FROM t001w t,tmp_dim_plant_insert i
WHERE t.WERKS = i.PlantCode;

delete from number_fountain m where m.table_name = 'dim_plant';

insert into number_fountain
select 	'dim_plant',
	ifnull(max(d.dim_plantid),
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_plant d
where d.dim_plantid <> 1;

INSERT INTO dim_plant(dim_plantid,
		      PlantCode,
                      Name,
                      PostalCode,
                      City,
                      "state",
                      Country,
                      SalesOrg,
                      PurchOrg,
                      PlanningPlant,
                      ValuationArea,
                      LanguageKey,
                      RowStartDate,
                      RowIsCurrent,
					  projectsourceid)
   SELECT
   (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_plant')
          + row_number()
             over(order by ''),
   	      t.WERKS,
          t.NAME1,
          ifnull(t.PSTLZ, 'Not Set'),
          ifnull(t.ORT01, 'Not Set'),
          ifnull(t.REGIO, 'Not Set'),
          ifnull(t.LAND1, 'Not Set'),
          ifnull(t.T001W_VKORG, 'Not Set'),
          ifnull(t.EKORG, t.WERKS),
          ifnull(t.T001W_IWERK, 'Not Set'),
          ifnull(t.BWKEY, 'Not Set'),
          ifnull(t.SPRAS, 'E'),
          current_date,
          1,
		  ifnull((select s.dim_projectsourceid from dim_projectsource s),1) projectsourceid
     FROM t001w t,tmp_dim_plant_insert2 p2
    WHERE p2.WERKS = t.WERKS;

update dim_plant set plant_emd = case when plantcode <> 'Not Set' then plantcode || ' - ' || name else name end
where plant_emd <> case when plantcode <> 'Not Set' then plantcode || ' - ' || name else name end;

update dim_plant dp
  set dp.CompanyCode = k.BUKRS
  from dim_plant dp, t001k k
  where dp.ValuationArea = k.bwkey;

update dim_plant
set country  = 'CA'
where plant_emd  = 'CAC - CA Consignment';

update dim_plant
set country  = 'US'
where plant_emd  = 'USC - US Consignment';

update dim_plant p
set p.region=m.region
from dim_plant p,EMDTempoCC4.plant_mapping_country m
where p.country=m.plant_country
and p.region<>m.region;

/* 24 Apr 2017 CristianB Start: update the manufacturingSite field (Merck HC Manufacturing Site) APP-6081 */

 UPDATE dim_plant dpl
SET dpl.manufacturingSite = ifnull(mp.manufacturingSite,'Not Set'),
  dpl.plantrole = ifnull(mp.plantrole,'Commercial')
from dim_plant dpl, EMDTEMPOCC4.manufacturingsite_mapping mp
where dpl.plantcode = mp.plantcode;
update dim_plant
set plantrole ='Commercial' 
where plantrole = 'Not Set';

update dim_plant dp
set dp.Executive_Scope_flag=ifnull(p.executive, 'Not Set')
from dim_plant dp,EMDTempoCC4.plant_new_mapping p
where dp.plantcode=p.plantcode
and p.platform like '%Tempo NA%'
and dp.Executive_Scope_flag<>ifnull(p.executive, 'Not Set');

DROP TABLE IF EXISTS tmp_dim_plant_insert;
DROP TABLE IF EXISTS tmp_dim_plant_delete;
DROP TABLE IF EXISTS tmp_dim_plant_insert2;
