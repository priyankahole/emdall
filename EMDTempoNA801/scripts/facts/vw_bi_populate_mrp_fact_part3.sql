


/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   11 Jan 2014      Lokesh    1.15              Removed the incorrect last 2 queries. In sync with prod/app now */
/*   11 Jan 2014      Lokesh    1.14              Add <> in updates for performance      */
/******************************************************************************************************************/


Drop table if exists mdtb_tmp_q01;
Create table mdtb_tmp_q01 as Select m.*,(ifnull(MDTB_UMDAT,MDTB_DAT01) -  MDTB_DAT02) leadcolupd from  mdtb m Where  ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL ;


MERGE INTO fact_mrp t
USING ( SELECT ic.Dim_ItemCategoryid, v.Dim_Vendorid, fv.Dim_Vendorid fvvendorid, dt.Dim_DocumentTypeid,
               ct.Dim_ConsumptionTypeid, m.fact_mrpid
		From    eban pr,
		        mdkp k,
		        mdtb_tmp_q01 t,
		        dim_vendor v,
		        dim_itemcategory ic,
		        dim_documenttype dt,
		        dim_consumptiontype ct,
		        dim_part dp,
		        dim_vendor fv,
		        dim_plant pl, fact_mrp m, 	tmpvariable_00e tmp
	  WHERE      tmp.tmpTotalCount > 0 and m.Dim_ActionStateid = 2
	        AND k.MDKP_DTNUM = t.MDTB_DTNUM
	        AND m.dd_DocumentNo = pr.eban_BANFN
	        AND m.dd_DocumentItemNo = pr.eban_BNFPO
	        AND m.dd_DocumentNo = t.MDTB_DELNR
	        AND m.dd_DocumentItemNo = t.MDTB_DELPS
	        AND dp.Dim_Partid = m.Dim_Partid
	        AND pl.dim_plantid = m.dim_plantid
	        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
	        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
	        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
	        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
	        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
	        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
	        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
	        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN ) x
ON t.fact_mrpid = x.fact_mrpid
WHEN MATCHED THEN UPDATE
SET t.Dim_ItemCategoryid = x.Dim_ItemCategoryid,
    t.Dim_Vendorid = x.Dim_Vendorid,
	t.Dim_FixedVendorid = x.fvvendorid,
    t.Dim_DocumentTypeid = x.Dim_DocumentTypeid,
	T.Dim_ConsumptionTypeid = X.Dim_ConsumptionTypeid;





drop table if exists eban_p00;
Create table eban_p00 as Select n.*, ifnull(EBAN_FLIEF, EBAN_LIFNR) eban_colupd from eban  n where ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL;

Drop table if exists fact_mrp_del_p00;
Create table fact_mrp_del_p00 as Select fact_mrpid,ct_leadTimeVariance,leadcolupd,k.MDKP_MATNR,k.MDKP_PLWRK,eban_colupd,eban_BANFN,eban_BNFPO,dp.LeadTime
From fact_mrp m, eban_p00 pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
        tmpvariable_00e
  WHERE    tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN;

Update fact_mrp_del_p00 pdo
Set pdo.ct_leadTimeVariance =   ifnull((pdo.leadcolupd) - glt.v_leadTime, -1 * pdo.LeadTime)
FROM fact_mrp_del_p00 pdo
              LEFT JOIN ( SELECT t0.* from tmp_getLeadTime t0
	                      where    t0.fact_script_name = 'bi_populate_mrp_fact'
                              AND t0.DocumentType=  'PR') glt 
	      ON glt.pPart =pdo.MDKP_MATNR 
              AND glt.pPlant =  pdo.MDKP_PLWRK
              AND glt.pVendor =   pdo.eban_colupd
              AND glt.DocumentNumber =  pdo.eban_BANFN
              AND glt.DocumentLineNumber =  pdo.eban_BNFPO

AND pdo.ct_leadTimeVariance <>   ifnull((pdo.leadcolupd) - glt.v_leadTime, -1 * pdo.LeadTime);


Update fact_mrp m
Set m.ct_leadTimeVariance = pdo.ct_leadTimeVariance
from fact_mrp_del_p00 pdo,
     fact_mrp m
Where m.fact_mrpid = pdo.fact_mrpid
AND m.ct_leadTimeVariance <> pdo.ct_leadTimeVariance;



		
/* Update curr/exchg rate from eban */


UPDATE fact_mrp m
SET dim_CurrencyID =  1
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e, fact_mrp m
	WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.dim_CurrencyID <> 1;		
	

MERGE INTO  fact_mrp t
USING( SELECT cur.dim_CurrencyID , fact_mrpid 
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
		dim_currency cur,dim_company dc ,
	    tmpvariable_00e,
	    fact_mrp m
WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
		AND dc.companycode = pl.companycode
		And dc.currency = cur.currencycode
		and dc.RowIsCurrent = 1) x
ON t.fact_mrpid = x.fact_mrpid 
WHEN MATCHED THEN UPDATE
SET t.dim_CurrencyID = x.dim_CurrencyID ;
		
		
MERGE INTO fact_mrp t
USING (SELECT t1.fact_mrpid, t2.dim_CurrencyID
              FROM  (SELECT  m.fact_mrpid, tmp.pGlobalCurrency 
                       FROM    fact_mrp m,
			       eban pr,
			       mdkp k,
			       mdtb_tmp_q01 t,
			       tmpvariable_00e  tmp
			   WHERE   m.Dim_ActionStateid = 2
			       AND k.MDKP_DTNUM = t.MDTB_DTNUM
			       AND m.dd_DocumentNo = pr.eban_banfn
			       AND m.dd_DocumentItemNo = pr.eban_BNFPO
			       AND m.dd_DocumentNo = t.MDTB_DELNR
			       AND m.dd_DocumentItemNo = t.MDTB_DELPS
			       AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN)  t1
              LEFT JOIN  (SELECT cur.dim_CurrencyID, currencycode 
                   				 FROM dim_currency cur) t2 
              ON  t1.pGlobalCurrency = t2.currencycode        
) src
ON t.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN UPDATE
   SET t.dim_CurrencyID_GBL = src.dim_CurrencyID;


MERGE INTO fact_mrp t
USING (SELECT t1.fact_mrpid, t2.dim_CurrencyID
              FROM  (SELECT m.fact_mrpid, pr.EBAN_WAERS
			FROM   eban pr,
			       mdkp k,
			       mdtb_tmp_q01 t,
			       fact_mrp m
			 WHERE     m.Dim_ActionStateid = 2
			       AND k.MDKP_DTNUM = t.MDTB_DTNUM
			       AND m.dd_DocumentNo = pr.eban_banfn
			       AND m.dd_DocumentItemNo = pr.eban_BNFPO
			       AND m.dd_DocumentNo = t.MDTB_DELNR
			       AND m.dd_DocumentItemNo = t.MDTB_DELPS
			       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
			       AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN)  t1
              LEFT JOIN  (SELECT cur.dim_CurrencyID, currencycode 
                   				 FROM dim_currency cur) t2 
              ON  t1.EBAN_WAERS = t2.currencycode        
) src
ON t.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN UPDATE
   SET t.dim_CurrencyID_TRA = src.dim_CurrencyID;
		
	      

MERGE INTO fact_mrp t
USING ( SELECT t1.fact_mrpid, t2.exchangeRate
		FROM  (SELECT m.fact_mrpid, pl.companycode, 
		              pr.EBAN_WAERS, pr.eban_BADAT, tmp.tmpTotalCount
			From  eban pr,
			      mdkp k,
			      mdtb_tmp_q01 t,
			      dim_plant pl,
			      tmpvariable_00e tmp,
			      fact_mrp m
			WHERE     m.Dim_ActionStateid = 2
			        AND k.MDKP_DTNUM = t.MDTB_DTNUM
			        AND m.dd_DocumentNo = pr.eban_banfn
			        AND m.dd_DocumentItemNo = pr.eban_BNFPO
			        AND m.dd_DocumentNo = t.MDTB_DELNR
			        AND m.dd_DocumentItemNo = t.MDTB_DELPS
			        AND pl.dim_plantid = m.dim_plantid
			        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
			        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN)  t1
        LEFT JOIN (SELECT  exr.exchangeRate, dc.companycode, exr.pFromCurrency, exr.pDate
			from tmp_getExchangeRate1 exr,dim_company dc
					where exr.pToCurrency = dc.currency
					and exr.pFromExchangeRate = 0
					and dc.RowIsCurrent = 1	
					and exr.fact_script_name = 'bi_populate_mrp_fact'	) t2
        ON  t2.companycode = t1.companycode
            AND t2.pFromCurrency = t1.EBAN_WAERS  
            AND t2.pDate = t1.eban_BADAT    
            AND t1.tmpTotalCount > 0  
)src
ON t.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN UPDATE
   SET t.amt_ExchangeRate = ifnull(src.exchangeRate, 1);
		
		
MERGE INTO fact_mrp t
USING ( SELECT t1.fact_mrpid, t2.exchangeRate
		FROM  (SELECT m.fact_mrpid, pr.EBAN_WAERS, tmp.pGlobalCurrency , tmp.tmpTotalCount
			FROM  eban pr,
			      mdkp k,
			      mdtb_tmp_q01 t,
			      dim_plant pl,
			      tmpvariable_00e tmp,
			      fact_mrp m
			WHERE     	 m.Dim_ActionStateid = 2
			        AND k.MDKP_DTNUM = t.MDTB_DTNUM
			        AND m.dd_DocumentNo = pr.eban_banfn
			        AND m.dd_DocumentItemNo = pr.eban_BNFPO
			        AND m.dd_DocumentNo = t.MDTB_DELNR
			        AND m.dd_DocumentItemNo = t.MDTB_DELPS
					AND pl.dim_plantid = m.dim_plantid
			        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
			        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN)  t1
        LEFT JOIN (SELECT  ex.exchangeRate, ex.pToCurrency, ex.pFromCurrency
                           FROM tmp_getExchangeRate1  ex
	    	   WHERE    pFromExchangeRate = 0
		  	AND pDate = CURRENT_DATE
			AND fact_script_name = 'bi_populate_mrp_fact') t2
        ON      t1.tmpTotalCount > 0 
            AND t2.pFromCurrency = t1.EBAN_WAERS  
            AND t2.pToCurrency = t1.pGlobalCurrency 
)src
ON t.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN UPDATE
       SET t.amt_ExchangeRate_GBL = ifnull(src.exchangeRate, 1);

	
MERGE INTO fact_mrp t
USING ( SELECT m.fact_mrpid, pr.eban_menge, pr.eban_preis, pr.eban_peinh
				FROM  	eban pr,
				        mdkp k,
				        mdtb_tmp_q01 t,
				        dim_plant pl,
					    tmpvariable_00e,
						fact_mrp m
				WHERE     tmpTotalCount > 0 
				AND m.Dim_ActionStateid = 2
		        AND k.MDKP_DTNUM = t.MDTB_DTNUM
		        AND m.dd_DocumentNo = pr.eban_banfn
		        AND m.dd_DocumentItemNo = pr.eban_BNFPO
		        AND m.dd_DocumentNo = t.MDTB_DELNR
		        AND m.dd_DocumentItemNo = t.MDTB_DELPS
				AND pl.dim_plantid = m.dim_plantid
		        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
		        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
) x
ON x.fact_mrpid = t.fact_mrpid
WHEN MATCHED THEN UPDATE
   SET t.amt_ExtendedPrice = ifnull(x.eban_menge * x.eban_preis / ifnull((case when x.eban_peinh = 0 then null else x.eban_peinh end), 1), 0) ;
		
MERGE INTO fact_mrp t
USING ( SELECT m.fact_mrpid, pr.eban_menge, pr.eban_preis, pr.eban_peinh
				FROM  	eban pr,
				        mdkp k,
				        mdtb_tmp_q01 t,
				        dim_plant pl,
					    tmpvariable_00e,
						fact_mrp m
				WHERE      m.Dim_ActionStateid = 2
				        AND k.MDKP_DTNUM = t.MDTB_DTNUM
				        AND m.dd_DocumentNo = pr.eban_banfn
				        AND m.dd_DocumentItemNo = pr.eban_BNFPO
				        AND m.dd_DocumentNo = t.MDTB_DELNR
				        AND m.dd_DocumentItemNo = t.MDTB_DELPS
						AND pl.dim_plantid = m.dim_plantid
				        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
				        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
) x
ON x.fact_mrpid = t.fact_mrpid
WHEN MATCHED THEN UPDATE
   SET t.amt_ExtendedPrice_GBL = ifnull(x.eban_MENGE * x.eban_PREIS / ifnull((case when x.eban_PEINH = 0 then null else x.eban_PEINH end), 1), 0) * t.amt_ExchangeRate_GBL;

        

MERGE INTO fact_mrp fact
USING ( SELECT t0.fact_mrpid, ifnull(od.dim_dateid, 1) Dim_DateidReschedule
		FROM  ( SELECT m.fact_mrpid, m.dd_PlannScenarioLTP, m.Dim_ActionStateid,
		               t.MDTB_DTNUM, t.MDTB_UMDAT, 
		               pr.EBAN_FLIEF, pr.EBAN_LIFNR,
		               pl.CompanyCode
		        FROM fact_mrp m 
							INNER JOIN eban pr ON  m.dd_DocumentNo = pr.eban_banfn
							                   AND m.dd_DocumentItemNo = pr.eban_BNFPO
							INNER JOIN mdtb_tmp_q01 t ON m.dd_DocumentNo = t.MDTB_DELNR
							                          AND m.dd_DocumentItemNo = t.MDTB_DELPS
		                    INNER JOIN dim_plant pl ON m.dim_plantid = pl.dim_plantid) t0
			    INNER JOIN mdkp k ON t0.dd_PlannScenarioLTP = k.MDKP_PLSCN 
		                        AND t0.MDTB_DTNUM = k.MDKP_DTNUM 
			  
			    CROSS JOIN tmpvariable_00e t1row
			    LEFT JOIN dim_date od ON od.DateValue = t0.MDTB_UMDAT 
									AND od.CompanyCode = t0.CompanyCode
		WHERE  t1row.tmpTotalCount > 0 and t0.Dim_ActionStateid = 2
		AND ifnull(t0.EBAN_FLIEF, t0.EBAN_LIFNR) IS NOT NULL) src
ON fact.fact_mrpid = src.fact_mrpid
WHEN MATCHED THEN
UPDATE SET fact.Dim_DateidReschedule = src.Dim_DateidReschedule
WHERE fact.Dim_DateidReschedule <> src.Dim_DateidReschedule;

drop table if exists mdtb_tmp_q01;
drop table if exists fact_mrp_del_p00;
drop table if exists eban_p00;
