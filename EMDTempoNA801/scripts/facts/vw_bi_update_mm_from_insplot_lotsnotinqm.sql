drop table if exists ttt_00;

create table ttt_00 as select il.dd_MaterialDocYear,il.dd_MaterialDocItemNo, il.dd_MAterialDocNo
FROM facT_inspectionlot il, dim_inspectionlotstatus ils
WHERE il.Dim_InspectionLotStatusId = ils.dim_inspectionlotstatusid AND ils.LotCancelled <> 'X'
GROUP by il.dd_MaterialDocYear,il.dd_MaterialDocItemNo, il.dd_MAterialDocNo;

Update fact_materialmovement mm
SET mm.ct_LotNotThruQM =1
FROM dim_movementtype mt,fact_materialmovement mm
WHERE mm.Dim_MovementTypeid = mt.Dim_MovementTypeid;

Update fact_materialmovement mm
SET mm.ct_LotNotThruQM =0
FROM dim_movementtype mt,ttt_00 tt,fact_materialmovement mm
WHERE mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
AND  mm.dd_MaterialDocYear = tt.dd_MaterialDocYear
AND  mm.dd_MaterialDocItemNo = tt.dd_MaterialDocItemNo
AND mm.dd_MaterialDocNo = tt.dd_MAterialDocNo
AND  mt.MovementType = '101'
AND  mm.ct_LotNotThruQM <> 0;

Update fact_materialmovement mm
SET mm.ct_LotNotThruQM =0
FROM dim_movementtype mt,fact_materialmovement mm
WHERE mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
AND  mt.MovementType <> '101'
AND  mm.ct_LotNotThruQM <> 0;

drop table if exists ttt_00;
