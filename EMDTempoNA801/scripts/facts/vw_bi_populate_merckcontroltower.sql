drop table if exists tmp_processing_day;
create table tmp_processing_day as select case when hour(current_timestamp) between 0 and 20 then current_date - interval '1' day else current_date end tdy from dual;

drop table if exists tmp_get_next_periods;
create table tmp_get_next_periods
  as
  select dd.dim_dateid, dd.datevalue
  from dim_date dd
  where dd.dayofmonth = 1
    and dd.datevalue between (select tdy from tmp_processing_day) - interval '1' month + interval '1' day and (select tdy from tmp_processing_day) + interval '12' month
    and dd.companycode = 'Not Set';

/* Tempo + Phoenix + Spain */
drop table if exists tmp_hc_partid;
create table tmp_hc_partid
  as
  select dp.dim_partid
    ,pl.dim_plantid
    ,dd.dim_dateid periodid
    ,dp.partnumber
    ,pl.plantcode
    ,dd.datevalue
    ,max(ifnull(bw.dim_bwproducthierarchyid,1)) dim_bwproducthierarchyid
    ,max(ifnull(dc.dim_companyid,1)) dim_companyid
    ,max(ifnull(mdg.dim_mdg_partid,1)) dim_mdg_partid
    ,max(ifnull(dim_con.dim_bwhierarchycountryid,1)) dim_bwhierarchycountryid
  from dim_part dp
    inner join dim_plant pl on dp.plant = pl.plantcode
    inner join dim_bwproducthierarchy bw on dp.producthierarchy = bw.lowerhierarchycode and dp.productgroupsbu = bw.upperhierarchycode
      and to_date('2018-12-28') between bw.upperhierstartdate and bw.upperhierenddate
    left join dim_bwhierarchycountry dim_con on pl.country = dim_con.nodehierarchynamelvl7 and dim_con.internalhierarchyname = '0COUNTRY05' and dim_con.validto = '9999-12-31'
    left join dim_company dc on pl.companycode = dc.companycode
    left join dim_mdg_part mdg on right('000000000000000000' || dp.partnumber,18) = right('000000000000000000' || mdg.partnumber,18)
    cross join tmp_get_next_periods dd
  where bw.BUSINESSSECTOR = 'BS-01'
  group by dp.dim_partid,pl.dim_plantid,dd.dim_dateid,dp.partnumber,pl.plantcode,dd.datevalue;

/* Get Inventory */
/* drop table if exists tmp_inventoryvalues
create table tmp_inventoryvalues
  as
  select
  i.dim_partid
  ,i.dim_plantid
  ,sum(i.ct_StockQty) ct_stockavailable
  ,sum(i.ct_StockInQInsp) ct_stockinqa
  ,sum(i.ct_TotalRestrictedStock) ct_stockrestricted
  ,sum(i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) ct_stocktotal
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.amt_OnHand)
      ELSE (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) * i.ct_baseuomratioPCcustom * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    END) *
    (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END) *
    i.amt_exchangerate_GBL
  ) amt_stocktotal_wcogs
  ,sum(i.amt_OnHand * i.amt_exchangerate_GBL) amt_stocktotal_stdprice
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.amt_StockValueAmt)
      ELSE (i.ct_StockQty) * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    END) *
    (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END) *
    i.amt_exchangerate_GBL
  ) amt_stockavailable_wcogs
  ,sum(i.amt_StockValueAmt * i.amt_exchangerate_GBL) amt_stockavailable_stdprice
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN i.amt_StockInQInspAmt
      ELSE i.ct_StockInQInsp * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    END) *
    (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END) *
    i.amt_exchangerate_GBL
  ) amt_stockinqa_wcogs
  ,sum(i.amt_StockInQInspAmt * i.amt_exchangerate_GBL) amt_stockinqa_stdprice
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.ct_TotalRestrictedStock*i.amt_StdUnitPrice)
      ELSE (i.ct_TotalRestrictedStock) * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    END) *
    (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END) *
    i.amt_exchangerate_GBL
  ) amt_stockrestricted_wcogs
  ,sum(i.ct_TotalRestrictedStock*i.amt_StdUnitPrice * i.amt_exchangerate_GBL) amt_stockrestricted_stdprice
  from fact_inventoryaging i
    inner join dim_part prt on i.dim_partid = prt.dim_partid
    inner join dim_company com on i.dim_companyid = com.dim_companyid
    inner join dim_mdg_part mdg_part on i.dim_mdg_partid = mdg_part.dim_mdg_partid
  where
    CASE WHEN prt.PartNumber_NoLeadZero = '5030270000' AND com.company = '001500' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = '1046RPB.0041' AND com.company = '001046' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = '1046RPB.0040' AND com.company = '001046' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'FR21030170085' AND com.company = '001047' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'A1204211' AND com.company = '001954' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'A1204211' AND com.company = '001215' THEN 'X'
    ELSE 'Not Set' END = 'Not Set'
  group by i.dim_partid,i.dim_plantid  */

/* APP-8761 Oana 31Jan2018 */
drop table if exists tmp_inventoryvalues;
create table tmp_inventoryvalues
  as
  select
  i.dim_partid
  ,i.dim_plantid
  ,sum(case when i.dd_calcmhc_stocktype = 'Unrestricted use' then (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) else 0 end) ct_stockavailable
  ,sum(case when i.dd_calcmhc_stocktype = 'Quality inspection' then (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) else 0 end) ct_stockinqa
	,sum(case when i.dd_calcmhc_stocktype = 'Blocked stock' then (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) else 0 end) ct_stockrestricted
  ,sum(i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) ct_stocktotal
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.amt_OnHand)
      ELSE (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) * i.ct_baseuomratioPCcustom * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    * (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END)END) *
    i.amt_exchangerate_GBL
  ) amt_stocktotal_wcogs
  ,sum(i.amt_OnHand * i.amt_exchangerate_GBL) amt_stocktotal_stdprice
  ,sum( case when i.dd_calcmhc_stocktype = 'Unrestricted use' then
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.amt_OnHand)
      ELSE (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) * i.ct_baseuomratioPCcustom * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    * (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END)END) *
    i.amt_exchangerate_GBL
		else 0 end
  ) amt_stockavailable_wcogs
  ,sum(case when i.dd_calcmhc_stocktype = 'Unrestricted use' then (i.amt_OnHand * i.amt_exchangerate_GBL) else 0 end) amt_stockavailable_stdprice
  ,sum(case when i.dd_calcmhc_stocktype = 'Quality inspection' then
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.amt_OnHand)
      ELSE (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) * i.ct_baseuomratioPCcustom * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    * (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END)END) *
    i.amt_exchangerate_GBL
		else 0 end
  ) amt_stockinqa_wcogs
  ,sum(case when i.dd_calcmhc_stocktype = 'Quality inspection' then (i.amt_OnHand * i.amt_exchangerate_GBL) else 0 end) amt_stockinqa_stdprice
  ,sum(case when i.dd_calcmhc_stocktype = 'Blocked stock' then
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.amt_OnHand)
      ELSE (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) * i.ct_baseuomratioPCcustom * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    * (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END)END) *
    i.amt_exchangerate_GBL
		else 0 end
  ) amt_stockrestricted_wcogs
  ,sum(case when i.dd_calcmhc_stocktype = 'Blocked stock' then (i.amt_OnHand * i.amt_exchangerate_GBL) else 0 end) amt_stockrestricted_stdprice
  from fact_inventoryaging i
    inner join dim_part prt on i.dim_partid = prt.dim_partid
    inner join dim_company com on i.dim_companyid = com.dim_companyid
    inner join dim_mdg_part mdg_part on i.dim_mdg_partid = mdg_part.dim_mdg_partid
  where
    CASE WHEN prt.PartNumber_NoLeadZero = '5030270000' AND com.company = '001500' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = '1046RPB.0041' AND com.company = '001046' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = '1046RPB.0040' AND com.company = '001046' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'FR21030170085' AND com.company = '001047' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'A1204211' AND com.company = '001954' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'A1204211' AND com.company = '001215' THEN 'X'
    ELSE 'Not Set' END = 'Not Set'
  group by i.dim_partid,i.dim_plantid;
/* END APP-8761 Oana 31Jan2018 */

/* Get Forecast from SAP */
drop table if exists tmp_sapfcst;
create table tmp_sapfcst
as
select PBIM_MATNR,PBIM_WERKS,date_trunc('month',PBED_PDATU) PBED_PDATU,SUM(PBED_PLNMG+PBED_ENTMG) PBED_PLNMG
from PBED_PBIM
where PBIM_BEDAE = 'VSF' and PBIM_VERSB = '00' and PBED_PDATU <= date_trunc('month',(select tdy from tmp_processing_day) + interval '13' month) - interval '1' day
group by PBIM_MATNR,PBIM_WERKS,date_trunc('month',PBED_PDATU);

/* Get Sales Data Tempo */
/*drop table if exists tmp_GIsales
create table tmp_GIsales
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(CASE WHEN f.Dim_DateidActualGI <> 1 then f.ct_DeliveredQty else 0 end) ct_shippedmtd
    ,sum(f.ct_DeliveredQty*f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE NULL END) * f.amt_exchangerate_GBL) amt_shippedmtd
  from fact_salesorder f
    inner join dim_date agi on f.dim_dateidactualgi = agi.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
    inner join dim_distributionchannel dch on f.dim_distributionchannelid = dch.dim_distributionchannelid
  where cust.TradingPartner = 'Not Set'
    and dch.DistributionChannelCode IN ('40','41','42','43','44','45','47','48','51','59','60','61','69')
    and dc.DocumentCategory = 'C'
    and dt.DocumentType IN ('YOR','YOTH','YFOC','YKB','YKE')
    and agi.datevalue between date_trunc('month',(select tdy from tmp_processing_day)) and date_trunc('month',(select tdy from tmp_processing_day) + interval '1' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid */

/* APP-8175 new measures from Sales and Sales Delivery- Oana 24Nov2017 */
drop table if exists tmp_GIsalesdelivery;
create table tmp_GIsalesdelivery as
  select f.dim_partid
      ,f.dim_plantid
  	,SUM(ct_QtyDelivered) ct_shippedmtd
  	,SUM((f.ct_QtyDelivered*f.amt_UnitPriceUoM/(case when f.ct_PriceUnit = 0 then 1 else f.ct_PriceUnit end))* f.amt_exchangerate_GBL) amt_shippedmtd
  from fact_salesorderdelivery f
  	inner join dim_date dt on f.dim_dateidactualgoodsissue = dt.dim_dateid
  where f.dd_mtddemand_flag = '3rd. Party'
  and date_trunc('month',dt.datevalue) = date_trunc('month',(select tdy from tmp_processing_day))
  and dt.datevalue <= (select tdy from tmp_processing_day)
group by f.dim_partid,f.dim_plantid;
/* END APP-8175 new measures from Sales and Sales Delivery- Oana 24Nov2017 */

/* Get OPEN Sales Data Tempo */
/* drop table if exists tmp_opensales
create table tmp_opensales
  as
  select f.dim_partid
    ,f.dim_plantid
    ,date_trunc('month',pd.datevalue) GIDate
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
      end) ct_opensales
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
        else (
          case
            when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
            else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
          end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
      end * f.amt_exchangerate_GBL) amt_opensales
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
    inner join dim_salesorderitemstatus sois on f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
    inner join dim_salesorderrejectreason rr on f.dim_salesorderrejectreasonid = rr.dim_salesorderrejectreasonid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
    inner join dim_distributionchannel dch on f.dim_distributionchannelid = dch.dim_distributionchannelid
  where cust.TradingPartner = 'Not Set'
    and hs.OverallProcessStatusItem not in ('Completely processed', 'Tratado completamente')
    and sois.OverallProcessingStatus not in ('Completely processed', 'Tratado completamente')
    and rr.RejectReasonCode = 'Not Set'
    and f.dd_clearedblockedsts <> 'Blocked'
    and f.dd_ScheduleNo = 1
    and dch.DistributionChannelCode IN ('40','41','42','43','44','45','47','48','51','59','60','61','69')
    and dc.DocumentCategory = 'C'
    and dt.DocumentType IN ('YOR','YOTH','YFOC','YKB','YKE')
    and pd.datevalue between (select tdy from tmp_processing_day) + interval '1' day and date_trunc('month',(select tdy from tmp_processing_day) + interval '13' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid,date_trunc('month',pd.datevalue) */

/* APP-8766 - Oana 5Feb2018 */
drop table if exists tmp_opensales;
create table tmp_opensales
  as
  select f.dim_partid
    ,f.dim_plantid
    ,date_trunc('month',pd.datevalue) GIDate
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end)) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end))
      end * ct_baseuomratio) ct_opensales
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
        else (
          case
            when (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end)) < 0 then 0.0000
            else (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end))
          end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
      end * f.amt_exchangerate_GBL) amt_opensales
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
  where f.dd_merckhcpastdueflag = 'X'
    and pd.datevalue between (select tdy from tmp_processing_day) + interval '1' day and date_trunc('month',(select tdy from tmp_processing_day) + interval '13' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid,date_trunc('month',pd.datevalue);
/* End APP-8766 - Oana 5Feb2018 */

/* Get Sales Data Tempo INTERCO*/
/*drop table if exists tmp_GIsales_interco
create table tmp_GIsales_interco
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(CASE WHEN f.Dim_DateidActualGI <> 1 then f.ct_DeliveredQty else 0 end) ct_shippedmtd_interco
  from fact_salesorder f
    inner join dim_date agi on f.dim_dateidactualgi = agi.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
  where cust.TradingPartner <> 'Not Set'
    and dc.DocumentCategory = 'C'
    and dt.DocumentType IN ('YOR','YOTH','YFOC','YKB','YKE')
    and agi.datevalue between date_trunc('month',(select tdy from tmp_processing_day)) and date_trunc('month',(select tdy from tmp_processing_day) + interval '1' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid */

/* APP-8175 new measures from Sales and Sales Delivery- Oana 24Nov2017 */
drop table if exists tmp_GIsalesdelivery_interco;
create table tmp_GIsalesdelivery_interco as
select f.dim_partid
    ,f.dim_plantid
	,SUM(ct_QtyDelivered) ct_shippedmtd_interco
	,SUM((f.ct_QtyDelivered*f.amt_UnitPriceUoM/(case when f.ct_PriceUnit = 0 then 1 else f.ct_PriceUnit end))* f.amt_exchangerate_GBL) amt_shippedmtd_interco
from fact_salesorderdelivery f
	inner join dim_date dt on f.dim_dateidactualgoodsissue = dt.dim_dateid
where f.dd_mtddemand_flag = 'Intercompany'
and date_trunc('month',dt.datevalue) = date_trunc('month',(select tdy from tmp_processing_day))
and dt.datevalue <= (select tdy from tmp_processing_day)
group by f.dim_partid,f.dim_plantid;
/* END APP-8175 new measures from Sales and Sales Delivery- Oana 24Nov2017 */

/* Get OPEN Sales Data Tempo INTERCO */
/* drop table if exists tmp_opensales_interco
create table tmp_opensales_interco
  as
  select f.dim_partid
    ,f.dim_plantid
    ,date_trunc('month',pd.datevalue) GIDate
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
      end) ct_opensales_interco
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
    inner join dim_salesorderitemstatus sois on f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
    inner join dim_salesorderrejectreason rr on f.dim_salesorderrejectreasonid = rr.dim_salesorderrejectreasonid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
  where cust.TradingPartner <> 'Not Set'
    and hs.OverallProcessStatusItem not in ('Completely processed', 'Tratado completamente')
    and sois.OverallProcessingStatus not in ('Completely processed', 'Tratado completamente')
    and rr.RejectReasonCode = 'Not Set'
    and f.dd_clearedblockedsts <> 'Blocked'
    and f.dd_ScheduleNo = 1
    and dc.DocumentCategory = 'C'
    and dt.DocumentType IN ('YOR','YOTH','YFOC','YKB','YKE')
    and pd.datevalue between (select tdy from tmp_processing_day) + interval '1' day and date_trunc('month',(select tdy from tmp_processing_day) + interval '13' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid,date_trunc('month',pd.datevalue) */

/* APP-8766 - Oana 5Feb2018 */
drop table if exists tmp_opensales_interco;
create table tmp_opensales_interco
  as
  select f.dim_partid
    ,f.dim_plantid
    ,date_trunc('month',pd.datevalue) GIDate
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end)) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end))
      end * ct_baseuomratio) ct_opensales_interco
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
  where f.dd_merckhcpastdueflag = 'X - Interco'
    and pd.datevalue between (select tdy from tmp_processing_day) + interval '1' day and date_trunc('month',(select tdy from tmp_processing_day) + interval '13' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid,date_trunc('month',pd.datevalue);
/* END APP-8766 - Oana 5Feb2018 */


/* Get PastDue Sales Data Tempo */
/* drop table if exists tmp_pastduesales
create table tmp_pastduesales
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
      end) ct_pastdue
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
        else (
          case
            when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
            else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
          end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
      end * f.amt_exchangerate_GBL) amt_pastdue
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
    inner join dim_salesorderitemstatus sois on f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
    inner join dim_salesorderrejectreason rr on f.dim_salesorderrejectreasonid = rr.dim_salesorderrejectreasonid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
    inner join dim_distributionchannel dch on f.dim_distributionchannelid = dch.dim_distributionchannelid
  where cust.TradingPartner = 'Not Set'
    and hs.OverallProcessStatusItem not in ('Completely processed', 'Tratado completamente')
    and sois.OverallProcessingStatus not in ('Completely processed', 'Tratado completamente')
    and rr.RejectReasonCode = 'Not Set'
    and f.dd_clearedblockedsts <> 'Blocked'
    and f.dd_ScheduleNo = 1
    and dch.DistributionChannelCode IN ('40','41','42','43','44','45','47','48','51','59','60','61','69')
    and dc.DocumentCategory = 'C'
    and dt.DocumentType IN ('YOR','YOTH','YFOC','YKB','YKE')
    and pd.datevalue between '2016-01-01' and (select tdy from tmp_processing_day)
  group by f.dim_partid,f.dim_plantid */

/* APP-8766 - Oana 5Feb2018 */
drop table if exists tmp_pastduesales;
create table tmp_pastduesales
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end)) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end))
      end * ct_baseuomratio) ct_pastdue
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
        else (
          case
            when (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end)) < 0 then 0.0000
            else (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end))
          end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
      end * f.amt_exchangerate_GBL) amt_pastdue
  from fact_salesorder f
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
  where f.dd_merckhcpastdueflag = 'X'
  group by f.dim_partid,f.dim_plantid;
/* End APP-8766 - Oana 5Feb2018 */

/* Get PastDue Sales Data Tempo INTERCO */
/* drop table if exists tmp_pastduesales_interco
create table tmp_pastduesales_interco
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
      end) ct_pastdue_interco
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
    inner join dim_salesorderitemstatus sois on f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
    inner join dim_salesorderrejectreason rr on f.dim_salesorderrejectreasonid = rr.dim_salesorderrejectreasonid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
  where cust.TradingPartner <> 'Not Set'
    and hs.OverallProcessStatusItem not in ('Completely processed', 'Tratado completamente')
    and sois.OverallProcessingStatus not in ('Completely processed', 'Tratado completamente')
    and rr.RejectReasonCode = 'Not Set'
    and f.dd_clearedblockedsts <> 'Blocked'
    and f.dd_ScheduleNo = 1
    and dc.DocumentCategory = 'C'
    and dt.DocumentType IN ('YOR','YOTH','YFOC','YKB','YKE')
    and pd.datevalue between '2016-01-01' and (select tdy from tmp_processing_day)
  group by f.dim_partid,f.dim_plantid */

/* APP-8766 - Oana 5Feb2018 */
drop table if exists tmp_pastduesales_interco;
create table tmp_pastduesales_interco
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end)) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - (case when f.ct_ShippedAgnstOrderQty > f.CT_BILLINGQTYSTOCKUOM then f.ct_ShippedAgnstOrderQty else f.CT_BILLINGQTYSTOCKUOM end))
      end * ct_baseuomratio) ct_pastdue_interco
  from fact_salesorder f
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
  where f.dd_merckhcpastdueflag = 'X - Interco'
  group by f.dim_partid,f.dim_plantid;
/* End APP-8766 - Oana 5Feb2018 */

/* Purchase data Tempo Spain Emerald APP-6839 */
/* 6982 - Control Tower Availability Date Veronica P*/


drop table if exists tmp_purchdata;
create table tmp_purchdata as
select
  f.dim_partid
  ,f.dim_plantidordering dim_plantid
  ,date_trunc('month',case when dtdel.datevalue < current_date
        then current_date
        else dtdel.datevalue end) as deliv_month
  ,SUM(case
    when atrb.itemdeliverycomplete = 'X' then 0.0000
    when atrb.ItemGRIndicator = 'Not Set' then 0.0000
    when (f.ct_DeliveryQty - f.ct_ReceivedQty) < 0 then 0.0000
    else (f.ct_DeliveryQty - f.ct_ReceivedQty)
  end) open_qty
from fact_purchase f
  inner join dim_date dtdel on f.dim_availabilitydateid = dtdel.dim_dateid
  inner join dim_purchasemisc atrb on f.dim_purchasemiscid = atrb.dim_purchasemiscid
where dtdel.datevalue >= '2017-01-01' and f.dd_deletionindicator = 'Not Set'
group by f.dim_partid, f.dim_plantidordering,
	date_trunc('month',case when dtdel.datevalue < current_date
        then current_date
        else dtdel.datevalue end);

/* APP-8175 new measures from Sales and Sales Delivery- Oana 24Nov2017  */
drop table if exists tmp_3rdparty_orders;
create table tmp_3rdparty_orders as
	select f.dim_partid
		,f.dim_plantid
		,sum((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) * f.amt_exchangerate_GBL) amt_ordered
		,sum(ct_ScheduleQtySalesUnit) qty_ordered
	from fact_salesorder f, DIM_DATE_FACTORY_CALENDAR dt, dim_salesmisc dsi
	where f.dim_dateidaccordinggid = dt.dim_dateid
    and f.dim_salesmiscid = dsi.dim_salesmiscid
		and date_trunc('month',dt.datevalue) = date_trunc('month',(select tdy from tmp_processing_day))
    and dt.datevalue <= (select tdy from tmp_processing_day)
		and f.dd_mtddemand_flag = '3rd. Party'
	group by f.dim_partid	,f.dim_plantid;

drop table if exists tmp_intercompany_orders;
create table tmp_intercompany_orders as
	select f.dim_partid
		,f.dim_plantid
		,sum((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) * f.amt_exchangerate_GBL) amt_ordered
		,sum(ct_ScheduleQtySalesUnit) qty_ordered
	from fact_salesorder f, DIM_DATE_FACTORY_CALENDAR dt, dim_salesmisc dsi
	where f.dim_dateidaccordinggid = dt.dim_dateid
    and f.dim_salesmiscid = dsi.dim_salesmiscid
		and date_trunc('month',dt.datevalue) = date_trunc('month',(select tdy from tmp_processing_day))
    and dt.datevalue <= (select tdy from tmp_processing_day)
		and f.dd_mtddemand_flag = 'Intercompany'
	group by f.dim_partid	,f.dim_plantid;

drop table if exists tmp_3rdparty_ytd_orders;
create table tmp_3rdparty_ytd_orders as
	select f.dim_partid
		,f.dim_plantid
		,sum((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) * f.amt_exchangerate_GBL) amt_ordered
		,sum(ct_ScheduleQtySalesUnit) qty_ordered
	from fact_salesorder f, DIM_DATE_FACTORY_CALENDAR dt, dim_salesmisc dsi
	where f.dim_dateidaccordinggid = dt.dim_dateid
    and f.dim_salesmiscid = dsi.dim_salesmiscid
		and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day))and (select tdy from tmp_processing_day)
		and f.dd_mtddemand_flag = '3rd. Party'
	group by f.dim_partid	,f.dim_plantid;

--YTD June 1st
drop table if exists tmp_3rdparty_ytd_orders_jun1;
create table tmp_3rdparty_ytd_orders_jun1 as
	select f.dim_partid
		,f.dim_plantid
		,sum((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) * f.amt_exchangerate_GBL) amt_ordered
		,sum(ct_ScheduleQtySalesUnit) qty_ordered
	from fact_salesorder f, DIM_DATE_FACTORY_CALENDAR dt, dim_salesmisc dsi, dim_date dtsoc
	where f.dim_dateidaccordinggid = dt.dim_dateid
    and f.dim_dateidsalesordercreated = dtsoc.dim_dateid
    and f.dim_salesmiscid = dsi.dim_salesmiscid
    and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day)) and (select tdy from tmp_processing_day)
	and dtsoc.datevalue between TO_DATE(year(CURRENT_DATE)||'-06-01','YYYY-MM-DD') and (select tdy from tmp_processing_day)
	and f.dd_mtddemand_flag = '3rd. Party'
	group by f.dim_partid	,f.dim_plantid;
	
drop table if exists tmp_intercompany_ytd_orders;
create table tmp_intercompany_ytd_orders as
	select f.dim_partid
		,f.dim_plantid
		,sum((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f.amt_ScheduleTotal) ELSE f.amt_ScheduleTotal END) * f.amt_exchangerate_GBL) amt_ordered
		,sum(ct_ScheduleQtySalesUnit) qty_ordered
	from fact_salesorder f, DIM_DATE_FACTORY_CALENDAR dt, dim_salesmisc dsi
	where f.dim_dateidaccordinggid = dt.dim_dateid
    and f.dim_salesmiscid = dsi.dim_salesmiscid
		and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day)) and (select tdy from tmp_processing_day)
		and f.dd_mtddemand_flag = 'Intercompany'
	group by f.dim_partid	,f.dim_plantid;

drop table if exists tmp_GIsalesdelivery_ytd;
create table tmp_GIsalesdelivery_ytd as
select f.dim_partid
    ,f.dim_plantid
	,SUM(ct_QtyDelivered) ct_shippedytd
	,SUM((f.ct_QtyDelivered*f.amt_UnitPriceUoM/(case when f.ct_PriceUnit = 0 then 1 else f.ct_PriceUnit end))* f.amt_exchangerate_GBL) amt_shippedytd
from fact_salesorderdelivery f
	inner join dim_date dt on f.dim_dateidactualgoodsissue = dt.dim_dateid
where f.dd_mtddemand_flag = '3rd. Party'
and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day)) and (select tdy from tmp_processing_day)
group by f.dim_partid,f.dim_plantid;

--YTD June 1st
drop table if exists tmp_GIsalesdelivery_ytd_jun1;
create table tmp_GIsalesdelivery_ytd_jun1 as
select f.dim_partid
    ,f.dim_plantid
	,SUM(ct_QtyDelivered) ct_shippedytd
	,SUM((f.ct_QtyDelivered*f.amt_UnitPriceUoM/(case when f.ct_PriceUnit = 0 then 1 else f.ct_PriceUnit end))* f.amt_exchangerate_GBL) amt_shippedytd
from fact_salesorderdelivery f
	inner join dim_date dt on f.dim_dateidactualgoodsissue = dt.dim_dateid
	inner join dim_date dtsoc on f.dim_dateidsalesordercreated = dtsoc.dim_dateid
where f.dd_mtddemand_flag = '3rd. Party'
and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day)) and (select tdy from tmp_processing_day)
and dtsoc.datevalue between TO_DATE(year(CURRENT_DATE)||'-06-01','YYYY-MM-DD') and (select tdy from tmp_processing_day)
group by f.dim_partid,f.dim_plantid;

drop table if exists tmp_GIsalesdel_interco_ytd;
create table tmp_GIsalesdel_interco_ytd as
select f.dim_partid
    ,f.dim_plantid
	,SUM(ct_QtyDelivered) ct_shippedytd_ic
	,SUM((f.ct_QtyDelivered*f.amt_UnitPriceUoM/(case when f.ct_PriceUnit = 0 then 1 else f.ct_PriceUnit end))* f.amt_exchangerate_GBL) amt_shippedytd_ic
from fact_salesorderdelivery f
	inner join dim_date dt on f.dim_dateidactualgoodsissue = dt.dim_dateid
where f.dd_mtddemand_flag = 'Intercompany'
and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day)) and (select tdy from tmp_processing_day)
group by f.dim_partid,f.dim_plantid;

drop table if exists tmp_cancelleddemand;
create table tmp_cancelleddemand as
select f_so.dim_partid
	,f_so.dim_plantid
	,SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = '3rd. Party' THEN
		((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) ELSE f_so.amt_ScheduleTotal END))* f_so.amt_exchangerate_GBL
			- (f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))* f_so.amt_exchangerate_GBL
	ELSE 0 END) amt_canceldemand_euro,
	SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = '3rd. Party' THEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_DeliveredQty)
			ELSE 0 END) ct_canceldemand
from fact_salesorder f_so, dim_salesmisc dsi, DIM_DATE_FACTORY_CALENDAR dt
where f_so.dim_dateidaccordinggid = dt.dim_dateid
	and f_so.dim_salesmiscid = dsi.dim_salesmiscid
	and date_trunc('month',datevalue) = date_trunc('month', (select tdy from tmp_processing_day))
  and dt.datevalue <= (select tdy from tmp_processing_day)
group by  f_so.dim_partid	,f_so.dim_plantid;

drop table if exists tmp_cancelleddemand_ic;
create table tmp_cancelleddemand_ic as
select f_so.dim_partid
	,f_so.dim_plantid
	,SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = 'Intercompany' THEN
		((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) ELSE f_so.amt_ScheduleTotal END))* f_so.amt_exchangerate_GBL
			- (f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))* f_so.amt_exchangerate_GBL
	ELSE 0 END) amt_canceldemand_ic_euro,
	SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = 'Intercompany' THEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_DeliveredQty)
			ELSE 0 END) ct_canceldemand_ic
from fact_salesorder f_so, dim_salesmisc dsi, DIM_DATE_FACTORY_CALENDAR dt
where f_so.dim_dateidaccordinggid = dt.dim_dateid
	and f_so.dim_salesmiscid = dsi.dim_salesmiscid
	and date_trunc('month',datevalue) = date_trunc('month', (select tdy from tmp_processing_day))
  and dt.datevalue <= (select tdy from tmp_processing_day)
group by  f_so.dim_partid	,f_so.dim_plantid;

drop table if exists tmp_ytd_cancelleddemand_ic;
create table tmp_ytd_cancelleddemand_ic as
select f_so.dim_partid
	,f_so.dim_plantid
	,SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = 'Intercompany' THEN
		((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) ELSE f_so.amt_ScheduleTotal END))* f_so.amt_exchangerate_GBL
			- (f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))* f_so.amt_exchangerate_GBL
	ELSE 0 END) amt_ytd_canceldemand_ic_euro,
	SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = 'Intercompany' THEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_DeliveredQty)
			ELSE 0 END) ct_ytd_canceldemand_ic
from fact_salesorder f_so, dim_salesmisc dsi, DIM_DATE_FACTORY_CALENDAR dt
where f_so.dim_dateidaccordinggid = dt.dim_dateid
	and f_so.dim_salesmiscid = dsi.dim_salesmiscid
	and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day)) and (select tdy from tmp_processing_day)
group by  f_so.dim_partid	,f_so.dim_plantid;

drop table if exists tmp_ytd_cancelleddemand;
create table tmp_ytd_cancelleddemand as
select f_so.dim_partid
	,f_so.dim_plantid
	,SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = '3rd. Party' THEN
		((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) ELSE f_so.amt_ScheduleTotal END))* f_so.amt_exchangerate_GBL
			- (f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))* f_so.amt_exchangerate_GBL
	ELSE 0 END) amt_ytd_canceldemand_euro,
	SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = '3rd. Party' THEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_DeliveredQty)
			ELSE 0 END) ct_ytd_canceldemand
from fact_salesorder f_so, dim_salesmisc dsi, DIM_DATE_FACTORY_CALENDAR dt
where f_so.dim_dateidaccordinggid = dt.dim_dateid
	and f_so.dim_salesmiscid = dsi.dim_salesmiscid
	and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day)) and (select tdy from tmp_processing_day)
group by  f_so.dim_partid	,f_so.dim_plantid;

--YTD June 1st
drop table if exists tmp_ytd_cancelleddemand1;
create table tmp_ytd_cancelleddemand1 as
select f_so.dim_partid
	,f_so.dim_plantid
	,SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = '3rd. Party' THEN
		((CASE WHEN dsi.ReturnsItem = 'X' THEN (-1*f_so.amt_ScheduleTotal) ELSE f_so.amt_ScheduleTotal END))* f_so.amt_exchangerate_GBL
			- (f_so.ct_DeliveredQty*f_so.amt_UnitPriceUoM/(CASE WHEN f_so.ct_PriceUnit <> 0 THEN f_so.ct_PriceUnit ELSE NULL END))* f_so.amt_exchangerate_GBL
	ELSE 0 END) amt_ytd_canceldemand_euro,
	SUM(CASE WHEN f_so.DD_CANCELLEDDEMAND_FLAG = '3rd. Party' THEN (f_so.ct_ScheduleQtySalesUnit - f_so.ct_DeliveredQty)
			ELSE 0 END) ct_ytd_canceldemand
from fact_salesorder f_so, dim_salesmisc dsi, DIM_DATE_FACTORY_CALENDAR dt, dim_date dtsoc
where f_so.dim_dateidaccordinggid = dt.dim_dateid
	and f_so.dim_dateidsalesordercreated = dtsoc.dim_dateid
	and f_so.dim_salesmiscid = dsi.dim_salesmiscid
	and dt.datevalue between date_trunc('year',(select tdy from tmp_processing_day)) and (select tdy from tmp_processing_day)
	and dtsoc.datevalue between TO_DATE(year(CURRENT_DATE)||'-06-01','YYYY-MM-DD') and (select tdy from tmp_processing_day)
group by  f_so.dim_partid	,f_so.dim_plantid;

/* END APP-8175 new measures from Sales and Sales Delivery - Oana 24Nov2017 */

/* APP-8212 Production Order - Oana 12Dec2017 */
drop table if exists tmp_prodorder_openqty;
create table tmp_prodorder_openqty as
select dim_partiditem as dim_partid
	,dim_plantid
	,sum(case when (f.ct_OrderItemQty - f.ct_GRQty) > 0 then (f.ct_OrderItemQty - f.ct_GRQty) else 0 end) ct_openqty
from fact_productionorder f, dim_date dt, dim_productionorderstatus pos, dim_mdg_part mdg
where f.dim_DateidofAvailability = dt.dim_dateid
	and f.dim_productionorderstatusid = pos.dim_productionorderstatusid
	and	f.dim_mdg_partid = mdg.dim_mdg_partid
	and pos.Closed2 = 'No'
	and pos.TechnicallyCompleted2 = 'No'
	and pos.Delivered2 = 'No'
	and date_trunc('MONTH',datevalue) between '2016-01-01' and date_trunc('MONTH',(select tdy from tmp_processing_day))
	and mdg.crossplantmatstatus in ('20','30','40','Not Set')
group by dim_partiditem ,dim_plantid;

drop table if exists tmp_prodorder_openqty_period;
create table tmp_prodorder_openqty_period as
select dim_partiditem as dim_partid
	,dim_plantid
	,date_trunc('MONTH',datevalue) period
	,sum(case when (f.ct_OrderItemQty - f.ct_GRQty) > 0 then (f.ct_OrderItemQty - f.ct_GRQty) else 0 end) ct_openqty
from fact_productionorder f, dim_date dt, dim_productionorderstatus pos, dim_mdg_part mdg
where f.dim_DateidofAvailability = dt.dim_dateid
	and f.dim_productionorderstatusid = pos.dim_productionorderstatusid
	and	f.dim_mdg_partid = mdg.dim_mdg_partid
	and pos.Closed2 = 'No'
	and pos.TechnicallyCompleted2 = 'No'
	and pos.Delivered2 = 'No'
	and date_trunc('MONTH',datevalue) > date_trunc('MONTH',(select tdy from tmp_processing_day))
	and mdg.crossplantmatstatus in ('20','30','40','Not Set')
group by dim_partiditem, dim_plantid, date_trunc('MONTH',datevalue);
/* END APP-8212 Production Order - Oana 12Dec2017 */

/* APP-8176 Planned Order - Oana 13Dec2017 */
drop table if exists tmp_planorder_qtytotal;
create table tmp_planorder_qtytotal as
select f.dim_partid
	,f.dim_plantid
	,sum(f.ct_QtyTotal) ct_TotalQty
from fact_planorder f, dim_actionstate ast, dim_mdg_part mdg, dim_part dp, dim_date dt
where f.dim_actionstateid = ast.dim_actionstateid
	and f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_partid = dp.dim_partid
	and f.dim_DateidofAvailability = dt.dim_dateid
	and date_trunc('MONTH',datevalue) between '2016-01-01' and date_trunc('MONTH',(select tdy from tmp_processing_day))
	and ast.ActionStatusDescription = 'Active'
	and mdg.crossplantmatstatus in ('20','30','40','Not Set')
	and lower(dp.MRPTypeDescription) <> 'no planning'
group by f.dim_partid, f.dim_plantid;

drop table if exists tmp_planorder_qtytotal_period;
create table tmp_planorder_qtytotal_period as
select f.dim_partid
	,f.dim_plantid
	,date_trunc('MONTH',datevalue) period
	,sum(f.ct_QtyTotal) ct_TotalQty
from fact_planorder f, dim_actionstate ast, dim_mdg_part mdg, dim_part dp, dim_date dt
where f.dim_actionstateid = ast.dim_actionstateid
	and f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_partid = dp.dim_partid
	and f.dim_DateidofAvailability = dt.dim_dateid
	and date_trunc('MONTH',datevalue) > date_trunc('MONTH',(select tdy from tmp_processing_day))
	and ast.ActionStatusDescription = 'Active'
	and mdg.crossplantmatstatus in ('20','30','40','Not Set')
	and lower(dp.MRPTypeDescription) <> 'no planning'
group by f.dim_partid, f.dim_plantid, date_trunc('MONTH',datevalue);
/* END APP-8176 Planned Order - Oana 13Dec2017  */



/* Roxana H - 7 Feb 2018 - APP-8467 */

drop table if exists tmp_openqtypurchase;
create table tmp_openqtypurchase as
select
   dp.partnumber
  ,pl.plantcode
  ,to_char(case when dtship.datevalue < current_date then current_date
              else dtship.datevalue end,'Mon YYYY') as ship_monthyear
  ,SUM(case when dpr.itemdeliverycomplete = 'X' then 0.0000
            when dpr.ItemGRIndicator = 'Not Set' then 0.0000
            when (fp.ct_DeliveryQty - fp.ct_ReceivedQty) < 0 then 0.0000
              else (fp.ct_DeliveryQty - fp.ct_ReceivedQty) end) openqtypurchase
from fact_purchase fp, dim_date dtdeliv, dim_releaseindicator dr, dim_purchasemisc dpr, dim_date dtship, dim_plant pl, dim_part dp
where fp.dim_dateiddelivery = dtdeliv.dim_dateid
  and fp.dim_dateidship = dtship.dim_dateid
  and fp.dim_releaseindicatorid = dr.dim_releaseindicatorid
  and fp.dim_purchasemiscid = dpr.dim_purchasemiscid
  and fp.dim_plantidsupplying = pl.dim_plantid
  and fp.dim_partid = dp.dim_partid
  and fp.dd_deletionindicator = 'Not Set'
  and dr.releaseindicator <> 'Blocked'
  and fp.dim_plantidsupplying <> 1
  and dtdeliv.datevalue >= '2017-01-01'
group by dp.partnumber,pl.plantcode,
     to_char(case when dtship.datevalue < current_date then current_date
                    else dtship.datevalue end,'Mon YYYY')
having SUM(case when dpr.itemdeliverycomplete = 'X' then 0.0000
                when dpr.ItemGRIndicator = 'Not Set' then 0.0000
                when (fp.ct_DeliveryQty - fp.ct_ReceivedQty) < 0 then 0.0000
                   else (fp.ct_DeliveryQty - fp.ct_ReceivedQty) end) > 0;

/* End APP-8467 */

/* Roxana H - 7 Feb 2018 - APP-8673 */

drop table if exists tmp_qtytotalplanorder;
create table tmp_qtytotalplanorder as
select
   dp.partnumber
  ,pl.plantcode
  ,to_char(case when dt.datevalue < current_date then current_date
              else dt.datevalue end,'Mon YYYY') as finishdate_monthyear
  ,sum(f.ct_QtyTotal) ct_TotalQty
from fact_planorder f, dim_actionstate ast, dim_part dp, dim_date dt, dim_plant pl
where f.dim_actionstateid = ast.dim_actionstateid
  and f.dim_partid = dp.dim_partid
  and f.dim_dateidfinish = dt.dim_dateid
  and f.dim_prodplantid = pl.dim_plantid
  and dt.datevalue >= '2017-01-01'
  and ast.ActionStatusDescription = 'Active'
  and f.dd_FlagTransferOutPlanned = 'X'
group by dp.partnumber, pl.plantcode,
  to_char(case when dt.datevalue < current_date then current_date
              else dt.datevalue end,'Mon YYYY')
having sum(f.ct_QtyTotal) > 0;

/* End APP-8673 */


delete from fact_merckcontroltower;
insert into fact_merckcontroltower
  (fact_merckcontroltowerid
  ,amt_exchangerate
  ,amt_exchangerate_GBL
  ,dim_projectsourceid
  ,dim_partid
  ,dim_plantid
  ,dim_mdg_partid
  ,dim_bwproducthierarchy
  ,dim_periodid
  ,ct_stockavailable
  ,ct_stockinqa
  ,ct_stockrestricted
  ,ct_stocktotal
  ,amt_stocktotal_wcogs
  ,amt_stocktotal_stdprice
  ,amt_stockavailable_wcogs
  ,amt_stockavailable_stdprice
  ,amt_stockinqa_wcogs
  ,amt_stockinqa_stdprice
  ,amt_stockrestricted_wcogs
  ,amt_stockrestricted_stdprice
  ,ct_salesforecast
  ,ct_shippedmtd
  ,amt_shippedmtd
  ,ct_opensales
  ,amt_opensales
  ,ct_shippedmtd_interco
  ,ct_opensales_interco
  ,ct_pastdue
  ,amt_pastdue
  ,ct_pastdue_interco
  ,dim_companyid
  ,amt_avgsaleprice
  ,dim_bwhierarchycountryid
  ,ct_purchopenqty
  ,amt_salesop_euro
  ,ct_salesop
  ,ct_salescf_qty
  ,amt_SupplyRisk_Euro
  ,ct_avgsalesfcstqty
  ,amt_ytdsalesop_euro
  ,amt_ordered_3rdparty_euro
  ,ct_ordered_3rdparty
  ,amt_ordered_intercomp_euro
  ,ct_ordered_intercomp
  ,amt_ordered_3rdparty_ytd_euro
  ,amt_ordered_3rdparty_ytd_euro_jun1
  ,ct_ordered_3rdparty_ytd
  ,CT_ORDERED_3RDPARTY_YTD_JUN1
  ,amt_ordered_intercomp_ytd_euro
  ,ct_ordered_intercomp_ytd
  ,amt_shippedmtd_interco
  ,ct_shippedytd
  ,CT_SHIPPEDYTD_JUN1
  ,amt_shippedytd_euro
  ,AMT_SHIPPEDYTD_EURO_JUN1
  ,amt_shippedytd_ic_euro
  ,ct_shippedytd_ic
  ,amt_canceldemand_euro
  ,ct_canceldemand
  ,amt_canceldemand_ic_euro
  ,ct_canceldemand_ic
  ,amt_ytd_canceldemand_ic_euro
  ,ct_ytd_canceldemand_ic
  ,amt_ytd_canceldemand_euro
  ,amt_ytd_canceldemand_euro_JUN1
  ,ct_ytd_canceldemand
  ,ct_ytd_canceldemand_JUN1
  ,ct_openqty
  ,ct_TotalQty
  ,ct_eur_packs)
select
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1) + row_number() over(order by '') fact_merckcontroltowerid
  ,1 amt_exchangerate
  ,1 amt_exchangerate_GBL
  ,(select s.dim_projectsourceid from dim_projectsource s) dim_projectsourceid
  ,ifnull(t.dim_partid,1) dim_partid
  ,ifnull(t.dim_plantid,1) dim_plantid
  ,ifnull(t.dim_mdg_partid,1) dim_mdg_partid
  ,ifnull(t.dim_bwproducthierarchyid,1) dim_bwproducthierarchyid
  ,ifnull(t.periodid,1) dim_periodid
  ,ifnull(inv.ct_stockavailable,0) ct_stockavailable
  ,ifnull(inv.ct_stockinqa,0) ct_stockinqa
  ,ifnull(inv.ct_stockrestricted,0) ct_stockrestricted
  ,ifnull(inv.ct_stocktotal,0) ct_stocktotal
  ,ifnull(inv.amt_stocktotal_wcogs,0) amt_stocktotal_wcogs
  ,ifnull(inv.amt_stocktotal_stdprice,0) amt_stocktotal_stdprice
  ,ifnull(inv.amt_stockavailable_wcogs,0) amt_stockavailable_wcogs
  ,ifnull(inv.amt_stockavailable_stdprice,0) amt_stockavailable_stdprice
  ,ifnull(inv.amt_stockinqa_wcogs,0) amt_stockinqa_wcogs
  ,ifnull(inv.amt_stockinqa_stdprice,0) amt_stockinqa_stdprice
  ,ifnull(inv.amt_stockrestricted_wcogs,0) amt_stockrestricted_wcogs
  ,ifnull(inv.amt_stockrestricted_stdprice,0) amt_stockrestricted_stdprice
  ,ifnull(asp.FCST_QTY,0) ct_salesforecast
  ,ifnull(gis.ct_shippedmtd,0) ct_shippedmtd
  ,ifnull(gis.amt_shippedmtd,0) amt_shippedmtd
  ,ifnull(os.ct_opensales,0) ct_opensales
  ,ifnull(os.amt_opensales,0) amt_opensales
  ,ifnull(gis_ic.ct_shippedmtd_interco,0) ct_shippedmtd_interco
  ,ifnull(os_ic.ct_opensales_interco,0) ct_opensales_interco
  ,ifnull(pd.ct_pastdue,0) ct_pastdue
  ,ifnull(pd.amt_pastdue,0) amt_pastdue
  ,ifnull(pd_ic.ct_pastdue_interco,0) ct_pastdue_interco
  ,ifnull(t.dim_companyid,1) dim_companyid
  ,(case when ifnull(asp.PRICE,0)< 0 then 0 else ifnull(asp.PRICE,0) end) amt_avgsaleprice
  ,ifnull(t.dim_bwhierarchycountryid,1) dim_bwhierarchycountryid
  ,ifnull(prch.open_qty,0) ct_purchopenqty
  ,ifnull(asp.op_euro,0) amt_salesop_euro
  ,ifnull(asp.op_qty,0) ct_salesop
  ,ifnull(asp.cf_qty,0) ct_salescf_qty
  ,ifnull(asp.SupplyRisk_Euro,0) amt_SupplyRisk_Euro
  ,ifnull(asp.avg_fcst_qty,0) ct_avgsalesfcstqty
  ,ifnull(asp.ytdsalesop_euro,0) amt_ytdsalesop_euro
  ,ifnull(o_3rd.amt_ordered,0) amt_ordered_3rdparty_euro
  ,ifnull(o_3rd.qty_ordered,0) ct_ordered_3rdparty
  ,ifnull(o_ic.amt_ordered,0) amt_ordered_intercomp_euro
  ,ifnull(o_ic.qty_ordered,0) ct_ordered_intercomp
  ,ifnull(oytd_3rd.amt_ordered,0) amt_ordered_3rdparty_ytd_euro
  ,ifnull(oytd_3rd1.amt_ordered,0) amt_ordered_3rdparty_ytd_euro_jun1
  ,ifnull(oytd_3rd.qty_ordered,0) ct_ordered_3rdparty_ytd
  ,ifnull(oytd_3rd1.qty_ordered,0) CT_ORDERED_3RDPARTY_YTD_JUN1
  ,ifnull(oytd_ic.amt_ordered,0) amt_ordered_intercomp_ytd_euro
  ,ifnull(oytd_ic.qty_ordered,0) ct_ordered_intercomp_ytd
  ,ifnull(gis_ic.amt_shippedmtd_interco,0) amt_shippedmtd_interco
  ,ifnull(gisd.ct_shippedytd,0) ct_shippedytd
  ,ifnull(gisd1.ct_shippedytd,0) CT_SHIPPEDYTD_JUN1
  ,ifnull(gisd.amt_shippedytd,0) amt_shippedytd_euro
  ,ifnull(gisd1.amt_shippedytd,0) AMT_SHIPPEDYTD_EURO_JUN1
  ,ifnull(gisd_ic.amt_shippedytd_ic,0) amt_shippedytd_ic_euro
  ,ifnull(gisd_ic.ct_shippedytd_ic,0) ct_shippedytd_ic
  ,ifnull(cd.amt_canceldemand_euro,0) amt_canceldemand_euro
  ,ifnull(cd.ct_canceldemand,0) ct_canceldemand
  ,ifnull(cd_ic.amt_canceldemand_ic_euro,0) amt_canceldemand_ic_euro
  ,ifnull(cd_ic.ct_canceldemand_ic,0) ct_canceldemand_ic
  ,ifnull(ycd_ic.amt_ytd_canceldemand_ic_euro,0) amt_ytd_canceldemand_ic_euro
  ,ifnull(ycd_ic.ct_ytd_canceldemand_ic,0) ct_ytd_canceldemand_ic
  ,ifnull(ycd.amt_ytd_canceldemand_euro,0) amt_ytd_canceldemand_euro
  ,ifnull(ycd1.amt_ytd_canceldemand_euro,0) amt_ytd_canceldemand_euro_JUN1
  ,ifnull(ycd.ct_ytd_canceldemand,0) ct_ytd_canceldemand
  ,ifnull(ycd1.ct_ytd_canceldemand,0) ct_ytd_canceldemand_JUN1
  ,ifnull(po.ct_openqty,0) ct_openqty
  ,ifnull(pno.ct_TotalQty,0) ct_TotalQty
  ,ifnull(lc.euro_packs,0) ct_eur_packs
from tmp_hc_partid t
  left join tmp_inventoryvalues inv on t.dim_partid = inv.dim_partid and t.dim_plantid = inv.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_sapfcst fcst on t.partnumber = fcst.PBIM_MATNR and t.plantcode = fcst.PBIM_WERKS and t.datevalue = fcst.PBED_PDATU
  left join tmp_GIsalesdelivery gis on t.dim_partid = gis.dim_partid and t.dim_plantid = gis.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_opensales os on t.dim_partid = os.dim_partid and t.dim_plantid = os.dim_plantid and t.datevalue = os.GIDate
  left join tmp_GIsalesdelivery_interco gis_ic on t.dim_partid = gis_ic.dim_partid and t.dim_plantid = gis_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_opensales_interco os_ic on t.dim_partid = os_ic.dim_partid and t.dim_plantid = os_ic.dim_plantid and t.datevalue = os_ic.GIDate
  left join tmp_pastduesales pd on t.dim_partid = pd.dim_partid and t.dim_plantid = pd.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_pastduesales_interco pd_ic on t.dim_partid = pd_ic.dim_partid and t.dim_plantid = pd_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join EMDOtherDatasourcesBE3.csv_averagesaleprice asp on trim(leading '0' from t.partnumber) = trim(leading '0' from asp.MATNR) and t.plantcode = asp.WERKS and t.datevalue = asp.PERIOD
  left join tmp_purchdata prch on t.dim_partid = prch.dim_partid and t.dim_plantid = prch.dim_plantid and t.datevalue = prch.deliv_month
  left join tmp_3rdparty_orders o_3rd on t.dim_partid = o_3rd.dim_partid and t.dim_plantid = o_3rd.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_intercompany_orders o_ic on t.dim_partid = o_ic.dim_partid and t.dim_plantid = o_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_3rdparty_ytd_orders oytd_3rd on t.dim_partid = oytd_3rd.dim_partid and t.dim_plantid = oytd_3rd.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_3rdparty_ytd_orders_jun1 oytd_3rd1 on t.dim_partid = oytd_3rd1.dim_partid and t.dim_plantid = oytd_3rd1.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_intercompany_ytd_orders oytd_ic on t.dim_partid = oytd_ic.dim_partid and t.dim_plantid = oytd_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_GIsalesdelivery_ytd gisd on t.dim_partid = gisd.dim_partid and t.dim_plantid = gisd.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_GIsalesdelivery_ytd_jun1 gisd1 on t.dim_partid = gisd1.dim_partid and t.dim_plantid = gisd1.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_GIsalesdel_interco_ytd gisd_ic on t.dim_partid = gisd_ic.dim_partid and t.dim_plantid = gisd_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_cancelleddemand cd on t.dim_partid = cd.dim_partid and t.dim_plantid = cd.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_cancelleddemand_ic cd_ic on t.dim_partid = cd_ic.dim_partid and t.dim_plantid = cd_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_ytd_cancelleddemand ycd on t.dim_partid = ycd.dim_partid and t.dim_plantid = ycd.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_ytd_cancelleddemand1 ycd1 on t.dim_partid = ycd1.dim_partid and t.dim_plantid = ycd1.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_ytd_cancelleddemand_ic ycd_ic on t.dim_partid = ycd_ic.dim_partid and t.dim_plantid = ycd_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_prodorder_openqty po on t.dim_partid = po.dim_partid and t.dim_plantid = po.dim_plantid and  t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_planorder_qtytotal pno on t.dim_partid = pno.dim_partid and t.dim_plantid = pno.dim_plantid and  t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join EMDOtherDatasourcesBE3.csv_lc_to_ctrltower lc on trim(leading '0' from t.partnumber) = trim(leading '0' from lc.itemglobalcode) and t.plantcode = lc.plant and date_trunc('month',t.datevalue) = date_trunc('month',lc.datevalue);

/* APP-6980 - Oana 30 Aug 2017 */
update fact_merckcontroltower f
	set f.dim_jda_productsubsetid = ifnull(djp.dim_jda_productsubsetid,1)
from fact_merckcontroltower f, dim_part dp, dim_jda_productsubset djp
where f.dim_partid = dp.dim_partid
 and trim(leading '0' from dp.partnumber) = trim(leading '0' from djp.itemglobalcode);
/* END APP-6980 - Oana 30 Aug 2017 */

/* APP-7869 Demand Fulfillment Red Flag - Oana 23Nov2017  */
update fact_merckcontroltower f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(dd.dim_hc_demandfulfillmentriskid ,1)
from fact_merckcontroltower f,dim_part dpt, dim_plant dpl, dim_hc_demandfulfillmentrisk dd
where f.dim_partid = dpt.dim_partid
	and f.dim_plantid = dpl.dim_plantid
	and dpt.partnumber = dd.partnumber
	and dpl.plantcode = dd.plant;
/* END APP-7869 Demand Fulfillment Red Flag - Oana 23Nov2017 */

drop table if exists tmp_hc_comments;
create table tmp_hc_comments as
  select distinct dim_partid
      ,dim_plantid
      ,dim_periodid
      ,dd_comment
      ,dd_comment1
      ,dd_comment2
      ,dd_comment3
  from EMD586.fact_merckcontroltower
  where dim_projectsourceid = 3;

update fact_merckcontroltower f
  set f.dd_comment = ifnull(t.dd_comment,'Not Set'),
    f.dd_comment1 = ifnull(t.dd_comment1,0),
    f.dd_comment2 = ifnull(t.dd_comment2,'Not Set'),
    f.dd_comment3 = ifnull(t.dd_comment3,'0001-01-01')
from fact_merckcontroltower f, tmp_hc_comments t
where f.dim_partid = t.dim_partid
    and f.dim_plantid = t.dim_plantid
    and f.dim_periodid = t.dim_periodid;
/* APP-9667 - Cristian Cleciu 23May2018 */
UPDATE FACT_MERCKCONTROLTOWER
SET dd_comment2 = CASE WHEN (AMT_PASTDUE>0 AND dd_comment2='Not Set') THEN '0'
					   ELSE 'Not Set'
					   END
WHERE dd_comment2='Not Set';				
/* END APP-9667 - Cristian Cleciu 23May2018 */

/* APP-8212 and APP-8176 Planned Order - Oana 05Jan2018 */
update fact_merckcontroltower f
	set f.ct_openqty = t.ct_openqty
from fact_merckcontroltower f, dim_Date dt , tmp_prodorder_openqty_period t
where f.dim_partid = t.dim_partid
	and f.dim_plantid = t.dim_plantid
	and f.dim_periodid = dt.dim_Dateid
	and dt.datevalue = t.period;

update fact_merckcontroltower f
	set f.ct_TotalQty = t.ct_TotalQty
from fact_merckcontroltower f, dim_Date dt , tmp_planorder_qtytotal_period t
where f.dim_partid = t.dim_partid
	and f.dim_plantid = t.dim_plantid
	and f.dim_periodid = dt.dim_Dateid
	and dt.datevalue = t.period;
/* END APP-8212 and APP-8176 Planned Order - Oana 05Jan2018  */


/* Roxana H - 7 Feb 2018 - APP-8467 */

update fact_merckcontroltower f
  set f.ct_openqtypurchase = t.openqtypurchase
from fact_merckcontroltower f, dim_Date dt , tmp_openqtypurchase t, dim_part d, dim_plant p
where  f.dim_partid = d.dim_partid
  and t.partnumber = d.partnumber
  and f.dim_plantid = p.dim_plantid
  and t.plantcode = p.plantcode
  and f.dim_periodid = dt.dim_Dateid
  and dt.monthyear = t.ship_monthyear;


/* Roxana H - 7 Feb 2018 - APP-8673 */

update fact_merckcontroltower f
  set f.ct_TotalQtyplanned = t.ct_TotalQty
from fact_merckcontroltower f, dim_Date dt , tmp_qtytotalplanorder t, dim_part d, dim_plant p
where  f.dim_partid = d.dim_partid
  and t.partnumber = d.partnumber
  and f.dim_plantid = p.dim_plantid
  and t.plantcode = p.plantcode
  and f.dim_periodid = dt.dim_Dateid
  and dt.monthyear = t.finishdate_monthyear;

/* Cristian Cleciu - 1 Mar 2018 - APP-8916 Invetory projection - updated Stock Available*/

DROP TABLE IF EXISTS tmp_fact_merckcontroltower_inv_prj;
CREATE TABLE tmp_fact_merckcontroltower_inv_prj AS
SELECT
f.FACT_MERCKCONTROLTOWERID,
f.DD_DUMMYROW,
dp.partnumber,
dpl.plantcode,
dd.DATEVALUE,
suM(CT_STOCKAVAILABLE) AS CT_STOCKAVAILABLE,
sum(CT_STOCKINQA) AS CT_STOCKINQA,
SUM( (CT_PURCHOPENQTY) + (ct_openqty) + (ct_TotalQty) ) AS projected_gr_qty,
sum( ((greatest((case when (ct_salesforecast) >= (CT_SHIPPEDMTD)
						then (ct_salesforecast) - (CT_SHIPPEDMTD)
						else 0 end)
						,( (CT_PASTDUE) + (CT_OPENSALES) ))) )
	+ (CT_PASTDUE_INTERCO) + (CT_OPENSALES_INTERCO) + (ct_TotalQtyplanned) + (ct_openqtypurchase) )  AS projected_gi_qty,
SUM(
CASE WHEN (case when CT_STOCKAVAILABLE <0
				then 0
				else CT_STOCKAVAILABLE END)
			+ (CT_STOCKINQA) + (ct_openqty) + (CT_PURCHOPENQTY) + (ct_TotalQty)
			- (greatest((case when (ct_salesforecast) >= (CT_SHIPPEDMTD)
								then (ct_salesforecast) - (CT_SHIPPEDMTD)
								else 0 end)
						  ,( (CT_PASTDUE) + (CT_OPENSALES) )))
			- (ct_TotalQtyplanned) - (ct_openqtypurchase) - (CT_PASTDUE_INTERCO) - (CT_OPENSALES_INTERCO) >0
	THEN (case when CT_STOCKAVAILABLE <0
				then 0
				else CT_STOCKAVAILABLE END)
			+ (CT_STOCKINQA) + (ct_openqty) + (CT_PURCHOPENQTY) + (ct_TotalQty)
			- (greatest((case when (ct_salesforecast) >= (CT_SHIPPEDMTD)
								then (ct_salesforecast) - (CT_SHIPPEDMTD)
								else 0 end)
						,( (CT_PASTDUE) + (CT_OPENSALES) )))
			- (ct_TotalQtyplanned) - (ct_openqtypurchase) - (CT_PASTDUE_INTERCO) - (CT_OPENSALES_INTERCO)
ELSE 0
END) as ct_inventory
from fact_merckcontroltower f
join dim_hc_demandfulfillmentrisk dfr on dfr.dim_hc_demandfulfillmentriskid = f.dim_hc_demandfulfillmentriskid
join dim_date dd on dd.dim_dateid = f.dim_periodid
join dim_part dp on dp.dim_partid = f.dim_partid
join dim_plant dpl on dpl.dim_plantid = f.dim_plantid
WHERE f.DD_DUMMYROW = 'Not Set'
GROUP BY
FACT_MERCKCONTROLTOWERID,
f.DD_DUMMYROW,
dp.partnumber,
dpl.plantcode,
dd.datevalue
ORDER BY dd.datevalue ASC;

-- for first day of the month
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = CURRENT_DATE-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,-1)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = CURRENT_DATE-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +1 = inventory current month
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,1)-DAY(CURRENT_DATE)+1
AND t.datevalue = CURRENT_DATE-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,1)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +2 = inventory current month +1
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,2)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,1)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,2)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +3 = inventory current month +2
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,3)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,2)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,3)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +4 = inventory current month +3
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,4)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,3)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,4)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +5 = inventory current month +4
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,5)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,4)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,5)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +6 = inventory current month +5
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,6)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,5)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,6)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +7 = inventory current month +6
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,7)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,6)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,7)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +8 = inventory current month +7
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,8)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,7)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,8)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +9 = inventory current month +8
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,9)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,8)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,9)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +10 = inventory current month +9
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,10)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,9)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,10)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +11 = inventory current month +10
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,11)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,10)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,11)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +12 = inventory current month +11
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,12)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,11)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,12)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

-- stock avalilable current month +13 = inventory current month +12
UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.CT_STOCKAVAILABLE = t.ct_inventory
FROM tmp_fact_merckcontroltower_inv_prj u, tmp_fact_merckcontroltower_inv_prj T
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,13)-DAY(CURRENT_DATE)+1
AND t.datevalue = ADD_MONTHS(CURRENT_DATE,12)-DAY(CURRENT_DATE)+1
AND u.partnumber = t.partnumber
AND u.plantcode = t.plantcode
AND u.CT_STOCKAVAILABLE <> t.ct_inventory;

UPDATE tmp_fact_merckcontroltower_inv_prj u
SET u.ct_inventory = CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END
FROM tmp_fact_merckcontroltower_inv_prj u
WHERE u.datevalue = ADD_MONTHS(CURRENT_DATE,13)-DAY(CURRENT_DATE)+1
AND u.ct_inventory <> CASE WHEN (u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty)>0 THEN u.CT_STOCKAVAILABLE + CT_STOCKINQA + projected_gr_qty -projected_gi_qty ELSE 0 END;

UPDATE FACT_MERCKCONTROLTOWER f
SET f.CT_STOCKAVAILABLE = t.CT_STOCKAVAILABLE
FROM FACT_MERCKCONTROLTOWER f, tmp_fact_merckcontroltower_inv_prj t
WHERE f.FACT_MERCKCONTROLTOWERID = t.FACT_MERCKCONTROLTOWERID
AND f.CT_STOCKAVAILABLE <> t.CT_STOCKAVAILABLE;
/* END Cristian Cleciu - 1 Mar 2018 - APP-8916 */

/* APP-8935 Oana 09March2018 */
update fact_merckcontroltower f
	set amt_cogsfixedplanrate_emd = ifnull(c.Z_GCPLFF,0)
from fact_merckcontroltower f, dim_company dc, dim_MDG_Part dp, csv_cogs c
where f.dim_companyid = dc.dim_companyid
	and f.dim_MDG_Partid = dp.dim_MDG_Partid
	and trim(leading '0' from PRODUCT) = trim(leading '0' from dp.partnumber)
	and trim(leading '0' from c.Z_REPUNIT) = trim(leading '0' from dc.company);
/* END: APP-8935 Oana 09March2018 */

/* APP-9638 Cristian Cleciu 17MAY2018 */
drop table if exists tmp_sales_mspi;
create table tmp_sales_mspi
  as
  select DISTINCT f.dim_partid
    ,f.dim_plantid
    ,dd_mspiFlag
  from fact_salesorder f
WHERE dd_mspiFlag <>'Local SNO';

update fact_merckcontroltower f
	set f.dd_mspiFlag = t.dd_mspiFlag
from fact_merckcontroltower f, tmp_sales_mspi t
where f.dim_partid = t.dim_partid
	and f.dim_plantid = t.dim_plantid;

UPDATE fact_merckcontroltower f 
SET f.dim_mspiflagid = d.dim_mspiflagid 
FROM fact_merckcontroltower f, dim_mspiflag d 
WHERE f.dd_mspiFlag =d.mspi_flag 
AND f.dim_mspiflagid <> d.dim_mspiflagid;
/* END APP-9638 Cristian Cleciu 17MAY2018 */

/* APP-9916 Cristian Cleciu 29.06.2018 */
UPDATE FACT_MERCKCONTROLTOWER f
SET f.DIM_WAVEFLAGID = CASE WHEN dp.COUNTRY IN ('GB','MX','AR','US','KR') OR LEFT(m.MSPI_FLAG,4) IN ('MSPI')
THEN 2
ELSE 3
END
FROM FACT_MERCKCONTROLTOWER f, DIM_PLANT dp, DIM_MSPIFLAG m
WHERE
dp.DIM_PLANTID = f.DIM_PLANTID
AND m.DIM_MSPIFLAGID = f.DIM_MSPIFLAGID;
/* END APP-9916 Cristian Cleciu 29.06.2018 */

drop table if exists tmp_hc_partid;
drop table if exists tmp_inventoryvalues;
drop table if exists tmp_sapfcst;
drop table if exists tmp_GIsales;
drop table if exists tmp_opensales;
drop table if exists tmp_GIsales_interco;
drop table if exists tmp_opensales_interco;
drop table if exists tmp_pastduesales;
drop table if exists tmp_pastduesales_interco;
drop table if exists tmp_processing_day;
drop table if exists tmp_purchdata;
drop table if exists tmp_GIsalesdelivery;
drop table if exists tmp_GIsalesdelivery_interco;
drop table if exists tmp_GIsalesdelivery_ytd;
drop table if exists tmp_GIsalesdel_interco_ytd;
drop table if exists tmp_openqtypurchase;
drop table if exists tmp_qtytotalplanorder;
drop table if exists tmp_sales_mspi;