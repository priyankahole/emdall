DELETE FROM NUMBER_FOUNTAIN WHERE TABLE_NAME = 'fact_Qualitynotificationitem';

INSERT INTO NUMBER_FOUNTAIN(table_name,max_id)
select 	'fact_Qualitynotificationitem',
	ifnull(max(f.fact_Qualitynotificationitemid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_Qualitynotificationitem f;

Drop table if exists tmp_far_variable_holder;

create table tmp_far_variable_holder as
Select  ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD') pGlobalCurrency;
DROP TABLE IF EXISTS fact_accountsreceivable_temp;


UPDATE fact_Qualitynotificationitem fi
SET fi.ct_ExternalDefectiveQty = IFNULL(q1.QMFE_FMGFRD,0)
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.ct_InternalDefectiveQty = IFNULL(q1.QMFE_FMGEIG,0)
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_CreatedBy = IFNULL(q1.QMFE_ERNAM,'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_ChangedBy = IFNULL(q1.QMFE_AENAM,'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_DateIdCreatedOn = ifnull(dt.dim_dateid, 'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_date dt, dim_Company dc, dim_plant pl,fact_Qualitynotificationitem fi
WHERE dt.DateValue = q1.QMFE_ERDAT
AND pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = IFNULL(pl.CompanyCode,'Not Set')
AND dt.CompanyCode =IFNULL(dc.CompanyCode,'Not Set')             
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_DateIdChangedOn = ifnull(dt.dim_dateid, 'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_date dt, dim_Company dc, dim_plant pl,fact_Qualitynotificationitem fi
WHERE dt.DateValue = q1.QMFE_AEDAT
AND pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = IFNULL(pl.CompanyCode,'Not Set')
AND dt.CompanyCode =IFNULL(dc.CompanyCode,'Not Set')            
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

/*
UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_CreatedOn = q1.QMFE_ERDAT
	,fi.dw_update_date = current_timestamp 
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM

UPDATE fact_Qualitynotificationitem fi
FROM QMFE q1
SET fi.dd_ChangedOn = q1.QMFE_AEDAT
	,fi.dw_update_date = current_timestamp 
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
*/

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_ItemShortText = IFNULL(q1.QMFE_FETXT,'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_NotificationItemNo = IFNULL(q1.QMFE_FENUM,0)
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_NotificationNo = IFNULL(q1.QMFE_QMNUM,0)
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dim_companyid = dc.dim_Companyid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_plant pl, dim_Company dc , fact_Qualitynotificationitem fi
WHERE pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = pl.CompanyCode
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;


UPDATE fact_Qualitynotificationitem fi
SET fi.dim_partid = p.dim_partid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_part p ,fact_Qualitynotificationitem fi
WHERE p.PartNumber = IFNULL(Q1.QMFE_MATNR,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dim_plantid =pl.dim_plantid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_plant pl,fact_Qualitynotificationitem fi
WHERE pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_UnitOfMeasureid =u.dim_unitofmeasureid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_UnitOfMeasure u,fact_Qualitynotificationitem fi
WHERE u.uom = IFNULL(Q1.QMFE_FMGEIN,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_InspectionCatalogTypeid = d.Dim_InspectionCatalogTypeid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_inspectioncatalogtype d,fact_Qualitynotificationitem fi
WHERE d.InspectionCatalogTypeCode = q1.QMFE_FEKAT
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dim_notificationitemmiscid = dn.dim_notificationitemmiscid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_notificationitemmisc dn,fact_Qualitynotificationitem fi
WHERE dn.originaldefect = IFNULL(q1.QMFE_KZORG,'Not Set') 
AND dn.repetitivedefect = IFNULL(q1.QMFE_WDFEH,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

/*UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_codetextid = c.Dim_codetextid
	,fi.dw_update_date = current_timestamp 
FROM QMFE q1, dim_codetext c,fact_Qualitynotificationitem fi
WHERE c.codegroup = IFNULL(q1.QMFE_OTGRP,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
*/

merge into fact_Qualitynotificationitem fact
using (
	select fi.fact_Qualitynotificationitemid,max(c.Dim_codetextid) Dim_codetextid
	FROM QMFE q1, dim_codetext c,fact_Qualitynotificationitem fi
	WHERE c.codegroup = IFNULL(q1.QMFE_OTGRP,'Not Set')
	AND fi.dd_NotificationNo = q1.QMFE_QMNUM
	AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
	group by fi.fact_Qualitynotificationitemid
) t on fact.fact_Qualitynotificationitemid = t.fact_Qualitynotificationitemid
when matched then update set fact.Dim_codetextid = t.Dim_codetextid, fact.dw_update_date = current_timestamp
where fact.Dim_codetextid <> t.Dim_codetextid;


UPDATE fact_Qualitynotificationitem fi
SET fi.dim_inspectionpartcatalogtypeid = i.dim_inspectionpartcatalogtypeid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_inspectionpartcatalogtype i,fact_Qualitynotificationitem fi
WHERE i.inspectionpartcatalogtypecode = IFNULL(Q1.QMFE_OTKAT,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_ItemOrderNumber =  IFNULL(q1.QMFE_FCOAUFNR,'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_EquipmentNumber =  IFNULL(q1.QMFE_EQUNR,'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dim_CostCenterId = cc.dim_CostCenterId
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_CostCenter cc,fact_Qualitynotificationitem fi
WHERE cc.code = IFNULL(Q1.QMFE_KOSTL,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.ct_ItemDefectiveQtyInt =  IFNULL(q1.QMFE_MENGE,0)
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_NumberOfDefects =  IFNULL(q1.QMFE_ANZFEHLER,0)
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_objectpartid = ct.Dim_codetextid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_codetext ct,fact_Qualitynotificationitem fi
WHERE ct.codegroup = IFNULL(q1.QMFE_OTGRP,'Not Set')
  AND ct.code = IFNULL(q1.QMFE_OTEIL,'Not Set')
  AND ct.version = IFNULL(q1.QMFE_OTVER,'Not Set')
  AND ct."catalog" = IFNULL(q1.QMFE_OTKAT,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dim_damagecodeid = ctt.dim_codetextid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_codetext ctt,fact_Qualitynotificationitem fi
WHERE ctt.codegroup = IFNULL(q1.QMFE_FEGRP,'Not Set')
	AND ctt."catalog" = IFNULL(q1.QMFE_FEKAT,'Not Set')
    AND ctt.code = IFNULL(q1.QMFE_FECOD,'Not Set')
    AND ctt.version = IFNULL(q1.QMFE_FEVER,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_TimeOfChange =  IFNULL(q1.QMFE_AEZEIT,'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dd_FunctionalLocation =  IFNULL(q1.QMFE_TPLNR,'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

INSERT INTO fact_Qualitynotificationitem(amt_ExchangeRate,
    amt_ExchangeRate_GBL,
    ct_ExternalDefectiveQty,
    ct_InternalDefectiveQty,
    dd_CreatedBy,
	dd_ChangedBy,
	dim_DateIdCreatedOn,
	dim_DateIdChangedOn,
    dd_ItemShortText,
    dd_NotificationItemNo,
    dd_NotificationNo,
	dim_companyid,
    dim_defectreporttypeid,
    dim_partid,
    dim_plantid,
    dim_qualitynotificationmiscid, /*unused*/
    Dim_UnitOfMeasureid,
    dim_inspectioncatalogtypeid,
    dim_notificationitemmiscid,
	dim_codetextid,
	dim_inspectionpartcatalogtypeid,
	dd_ItemOrderNumber,
	dd_EquipmentNumber,
	dim_CostCenterId,
	ct_ItemDefectiveQtyInt,
	dd_NumberOfDefects,
	dd_TimeOfChange,
	dd_FunctionalLocation,
	dim_ObjectPartId,
	dim_DamageCodeId,
	fact_Qualitynotificationitemid,
	dim_projectsourceid,
	dim_mdg_partid,
	dim_bwproducthierarchyid
	)
SELECT 
	1 amt_ExchangeRate,	
	1 amt_exchangerate_GBL, 
	IFNULL(QMFE_FMGFRD,0),
	IFNULL(QMFE_FMGEIG,0),
    IFNULL(QMFE_ERNAM, 'Not Set'),
	IFNULL(QMFE_AENAM, 'Not Set'),
	1,
	1,
	  QMFE_FETXT,
          QMFE_FENUM,
          QMFE_QMNUM,
	  1 dim_companyid,
	  1,
         1 dim_partid,
           1 dim_plantid,
          1, 
        1 as dim_unitofmeasureid,
	1 as dim_inspectioncatalogtypeid,     
	1 as dim_notificationitemmiscid,
    1 as dim_codetext,   
	1 as dim_inspectionpartcatalogtypeid,
	IFNULL(QMFE_FCOAUFNR,'Not Set'),
	IFNULL(QMFE_EQUNR,'Not Set'),
	1 as dim_costcenter,
	IFNULL(QMFE_MENGE,0),
	IFNULL(QMFE_ANZFEHLER,0),
	IFNULL(QMFE_AEZEIT,'Not Set'), 
	IFNULL(QMFE_TPLNR,'Not Set'),
	/*IFNULL(QMFE_FECOD,'Not Set') */
	1 as dim_ObjectPartId,
   1 as dim_DamageCodeId,             
                 
        /* hash(concat(QMFE_QMNUM,QMFE_FENUM))*/
	(  (SELECT max_id
                FROM NUMBER_FOUNTAIN
               WHERE table_name = 'fact_Qualitynotificationitem')
           + row_number() OVER(ORDER BY QMFE_QMNUM,QMFE_FENUM)),
	ps.dim_projectsourceid,
	CAST(1 AS BIGINT) dim_mdg_partid,
	CAST(1 AS BIGINT) dim_bwproducthierarchyid

 FROM tmp_far_variable_holder t, QMFE, DIM_PROJECTSOURCE ps
WHERE  NOT EXISTS /*( SELECT 1 FROM fact_Qualitynotificationitem where hash(concat(qmfe_qmnum, qmfe_fenum)) = fact_Qualitynotificationitemid)*/
( SELECT 1 FROM fact_Qualitynotificationitem WHERE qmfe_qmnum = dd_notificationno
                    AND qmfe_fenum = dd_notificationitemno);

UPDATE fact_Qualitynotificationitem a
set a.dim_Companyid = dc.dim_Companyid
from fact_Qualitynotificationitem a, QMFE b,dim_Company dc, dim_plant pl
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND pl.plantcode = IFNULL(QMFE_WERKS,'Not Set')
AND pl.rowiscurrent = 1
AND pl.companycode = dc.companycode
AND dc.rowiscurrent = 1;

UPDATE fact_Qualitynotificationitem a
set a.dim_partid = p.dim_partid
from fact_Qualitynotificationitem a, QMFE b,dim_part p
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND p.PartNumber = IFNULL(QMFE_MATNR,'Not Set')
and p.rowiscurrent = 1;

UPDATE fact_Qualitynotificationitem a
set a.dim_plantid = pl.dim_plantid
from fact_Qualitynotificationitem a, QMFE b,dim_plant pl
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND pl.plantcode = IFNULL(qmfe_werks,'Not Set')
AND pl.rowiscurrent = 1 ;

UPDATE fact_Qualitynotificationitem a
set a.dim_unitofmeasureid =u.dim_unitofmeasureid
from fact_Qualitynotificationitem a, QMFE b,dim_UnitOfMeasure u
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND u.uom = IFNULL(QMFE_FMGEIN,'Not Set')
and u.rowiscurrent = 1;

UPDATE fact_Qualitynotificationitem a
set a.dim_inspectioncatalogtypeid =d.Dim_InspectionCatalogTypeid
from fact_Qualitynotificationitem a, QMFE b,dim_inspectioncatalogtype d
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND d.InspectionCatalogTypeCode = QMFE_FEKAT
and d.rowiscurrent = 1;

UPDATE fact_Qualitynotificationitem a
set a.Dim_NotificationItemMiscId =dn.Dim_NotificationItemMiscId
from fact_Qualitynotificationitem a, QMFE b,dim_NotificationItemMisc dn
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND dn.originaldefect = IFNULL(QMFE_KZORG,'Not Set') 
AND dn.repetitivedefect = IFNULL(QMFE_WDFEH,'Not Set');

/* original update - modify due to ambigous update
UPDATE fact_Qualitynotificationitem a
set a.dim_codetextid =c.dim_codetextid
from fact_Qualitynotificationitem a, QMFE b,dim_codetext c
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND c.codegroup = IFNULL(QMFE_OTGRP,'Not Set')
and c.rowiscurrent = 1*/

merge into fact_Qualitynotificationitem fact
using (
	select a.fact_Qualitynotificationitemid, max(c.dim_codetextid) dim_codetextid
	from fact_Qualitynotificationitem a, QMFE b,dim_codetext c
	where b.qmfe_qmnum = a.dd_notificationno
	AND b.qmfe_fenum = a.dd_notificationitemno
	AND c.codegroup = IFNULL(QMFE_OTGRP,'Not Set')
	and c.rowiscurrent = 1
	group by a.fact_Qualitynotificationitemid
) t on fact.fact_Qualitynotificationitemid = t.fact_Qualitynotificationitemid
when matched then update set fact.dim_codetextid = t.dim_codetextid
where fact.dim_codetextid <> t.dim_codetextid;


UPDATE fact_Qualitynotificationitem a
set a.dim_inspectionpartcatalogtypeid =i.dim_inspectionpartcatalogtypeid
from fact_Qualitynotificationitem a, QMFE b,dim_inspectionpartcatalogtype i
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND i.inspectionpartcatalogtypecode = IFNULL(QMFE_OTKAT,'Not Set')
and i.rowiscurrent = 1;

UPDATE fact_Qualitynotificationitem a
set a.dim_CostCenterId =cc.dim_CostCenterId
from fact_Qualitynotificationitem a, QMFE b,dim_CostCenter cc
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND cc.code = IFNULL(QMFE_KOSTL,'Not Set')
and cc.rowiscurrent = 1;

/*UPDATE fact_Qualitynotificationitem a
set a.dim_ObjectPartId =ct.dim_codetextId
from fact_Qualitynotificationitem a, QMFE b,dim_codetext ct
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND ct.codegroup = IFNULL(QMFE_OTGRP,'Not Set')
and ct.rowiscurrent = 1*/

merge into fact_Qualitynotificationitem fact
using (
	select a.fact_Qualitynotificationitemid, max(ct.dim_codetextId) dim_codetextId
	from fact_Qualitynotificationitem a, QMFE b,dim_codetext ct
	where b.qmfe_qmnum = a.dd_notificationno
	AND b.qmfe_fenum = a.dd_notificationitemno
	AND ct.codegroup = IFNULL(QMFE_OTGRP,'Not Set')
	and ct.rowiscurrent = 1
	group by a.fact_Qualitynotificationitemid
) t on fact.fact_Qualitynotificationitemid = t.fact_Qualitynotificationitemid
when matched then update set fact.dim_ObjectPartId =t.dim_codetextId
where fact.dim_ObjectPartId <> t.dim_codetextId;

drop table if exists tmp1_upd1;
create table tmp1_upd1 as
select fact_qualitynotificationitemid,max(ctt.dim_codetextId) as dim_codetextId
 from fact_Qualitynotificationitem a, QMFE b,dim_codetext ctt
where b.qmfe_qmnum = a.dd_notificationno
AND b.qmfe_fenum = a.dd_notificationitemno
AND ctt.codegroup = IFNULL(QMFE_FEGRP,'Not Set')
and ctt.rowiscurrent = 1
group by fact_qualitynotificationitemid;

UPDATE fact_Qualitynotificationitem a
set a.dim_ObjectPartId =ctt.dim_codetextId
from fact_Qualitynotificationitem a,  tmp1_upd1 ctt
where a.fact_qualitynotificationitemid = ctt.fact_qualitynotificationitemid;



UPDATE fact_Qualitynotificationitem fi
SET fi.amt_Exchangerate_GBL = z.exchangeRate
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM tmp_getExchangeRate1 z,dim_company dc, tmp_far_variable_holder t,fact_Qualitynotificationitem fi
WHERE dc.dim_companyid = fi.dim_companyid
AND z.pFromCurrency  = dc.currency 
AND z.fact_script_name = 'bi_populate_qualitynotificationitem_fact' 
AND z.pToCurrency = t.pGlobalCurrency 
AND z.pFromExchangeRate = 0 
AND z.pDate = SYSDATE;		    
		    
UPDATE fact_Qualitynotificationitem fi
SET fi.dim_currencyid = cur.dim_currencyid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_plant pl, dim_Company dc , dim_currency cur,fact_Qualitynotificationitem fi
WHERE pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = pl.CompanyCode
AND dc.currency = cur.currencycode
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
AND fi.dim_currencyid <> cur.dim_currencyid;

UPDATE fact_Qualitynotificationitem fi  
SET fi.dim_currencyid_GBL = c.dim_currencyid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_plant pl, dim_Company dc , dim_currency c,tmp_far_variable_holder t,fact_Qualitynotificationitem fi  
WHERE pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = pl.CompanyCode
AND c.CurrencyCode = t.pGlobalCurrency
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
AND fi.dim_currencyid_GBL <> c.dim_currencyid;

UPDATE fact_Qualitynotificationitem fi
SET fi.dim_currencyid_TRA = cur.dim_currencyid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_plant pl, dim_Company dc , dim_currency cur,fact_Qualitynotificationitem fi
WHERE pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = pl.CompanyCode
AND dc.currency = cur.currencycode
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM
AND fi.dim_currencyid_TRA <> cur.dim_currencyid;

/* update dim_objectpartid and dim_damagecodeid*/

UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_objectpartid = ct.Dim_codetextid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_codetext ct, fact_Qualitynotificationitem fi
WHERE ct.codegroup = IFNULL(q1.QMFE_OTGRP,'Not Set')
  AND ct.code = IFNULL(q1.QMFE_OTEIL,'Not Set')
  AND ct.version = IFNULL(q1.QMFE_OTVER,'Not Set')
  AND ct."catalog" = IFNULL(q1.QMFE_OTKAT,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.dim_damagecodeid = ctt.dim_codetextid
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_codetext ctt,fact_Qualitynotificationitem fi
WHERE ctt.codegroup = IFNULL(q1.QMFE_FEGRP,'Not Set')
	AND ctt."catalog" = IFNULL(q1.QMFE_FEKAT,'Not Set')
    AND ctt.code = IFNULL(q1.QMFE_FECOD,'Not Set')
    AND ctt.version = IFNULL(q1.QMFE_FEVER,'Not Set')
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_DateIdCreatedOn = ifnull(dt.dim_dateid, 'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_date dt, dim_Company dc, dim_plant pl,fact_Qualitynotificationitem fi
WHERE dt.DateValue = q1.QMFE_ERDAT
AND pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = IFNULL(pl.CompanyCode,'Not Set')
AND dt.CompanyCode =IFNULL(dc.CompanyCode,'Not Set')             
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_DateIdChangedOn = ifnull(dt.dim_dateid, 'Not Set')
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM QMFE q1, dim_date dt, dim_Company dc, dim_plant pl,fact_Qualitynotificationitem fi
WHERE dt.DateValue = q1.QMFE_AEDAT
AND pl.plantcode = IFNULL(q1.QMFE_WERKS,'Not Set')
AND dc.CompanyCode = IFNULL(pl.CompanyCode,'Not Set')
AND dt.CompanyCode =IFNULL(dc.CompanyCode,'Not Set')             
AND fi.dd_NotificationNo = q1.QMFE_QMNUM
AND fi.dd_NotificationItemNo = q1.QMFE_FENUM;

/*  New Fields 31 Jan 2014 */
UPDATE fact_Qualitynotificationitem fi
SET fi.dd_documentno =  IFNULL(fq.dd_documentno,'Not Set'),
    fi.dd_documentitemno =  IFNULL(fq.dd_documentitemno,0),
	fi.dim_vendorid = IFNULL(fq.dim_vendorid,1),
	fi.Dim_dateidrecordcreated = IFNULL(fq.Dim_dateidrecordcreated,1),
	fi.dim_dateidnotificationcompletion = ifnull(fq.dim_dateidnotificationcompletion,1),
	fi.dd_VendorMaterialNo = ifnull(fq.dd_VendorMaterialNo,'Not Set'),
	fi.dim_notificationtypeid = ifnull(fq.dim_notificationtypeid,1)
	,fi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_Qualitynotification fq,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = fq.dd_notificationo;

/*UPDATE fact_Qualitynotificationitem fi
SET fi.Dim_codetextCausesid = dc.Dim_codetextid
	,fi.dw_update_date = current_timestamp 
FROM QMUR QM, Dim_codetext dc,fact_Qualitynotificationitem fi
WHERE QM.QMUR_QMNUM = fi.dd_NotificationNo
  AND QM.QMUR_FENUM = fi.dd_NotificationItemNo
  AND QM.QMUR_URKAT = dc."catalog"
  AND QM.QMUR_URGRP = dc.codegroup
  AND QM.QMUR_URCOD = dc.code
  */
  
   merge into fact_Qualitynotificationitem fact
  using (
		select fi.fact_Qualitynotificationitemid, max(dc.Dim_codetextid) Dim_codetextid
		FROM QMUR QM, Dim_codetext dc,fact_Qualitynotificationitem fi
		WHERE QM.QMUR_QMNUM = fi.dd_NotificationNo
		  AND QM.QMUR_FENUM = fi.dd_NotificationItemNo
		  AND QM.QMUR_URKAT = dc."catalog"
		  AND QM.QMUR_URGRP = dc.codegroup
		  AND QM.QMUR_URCOD = dc.code
		group by fi.fact_Qualitynotificationitemid
  ) t on fact.fact_Qualitynotificationitemid = t.fact_Qualitynotificationitemid
  when matched then update set fact.Dim_codetextCausesid = t.Dim_codetextid
	,fact.dw_update_date = current_timestamp 
where fact.Dim_codetextCausesid <> t.Dim_codetextid;
  
/*UPDATE fact_Qualitynotificationitem fi
SET fi.dd_DefectNumber = ifnull(QM.QMUR_URNUM,0)
	,fi.dw_update_date = current_timestamp 
FROM QMUR QM,fact_Qualitynotificationitem fi
WHERE QM.QMUR_QMNUM = fi.dd_NotificationNo
  AND QM.QMUR_FENUM = fi.dd_NotificationItemNo
  */
  
  merge into fact_Qualitynotificationitem fact
  using
  (
	select distinct fi.fact_Qualitynotificationitemid, ifnull(max(QM.QMUR_URNUM),0) QMUR_URNUM
	FROM QMUR QM,fact_Qualitynotificationitem fi
	WHERE QM.QMUR_QMNUM = fi.dd_NotificationNo
	  AND QM.QMUR_FENUM = fi.dd_NotificationItemNo 
	  group by fi.fact_Qualitynotificationitemid
  ) t on fact.fact_Qualitynotificationitemid = t.fact_Qualitynotificationitemid
  when matched then update set fact.dd_DefectNumber = t.QMUR_URNUM
	,fact.dw_update_date = current_timestamp 
  where fact.dd_DefectNumber <> t.QMUR_URNUM;
 
 /* original update 
UPDATE fact_Qualitynotificationitem fi
SET fi.dim_dateidqntaskcreated =  ifnull((SELECT dim_dateid FROM dim_date dnc
                      				WHERE dnc.DateValue = QM.QMSM_ERDAT AND dnc.CompanyCode = dc.CompanyCode
							        AND QMSM_ERDAT IS NOT NULL),1)
	,fi.dw_update_date = current_timestamp 
FROM QMSM QM, dim_company dc,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = QM.QMSM_QMNUM
AND fi.dd_NotificationItemNo = QM.QMSM_FENUM
*/

merge into fact_Qualitynotificationitem fact
using
(
	 SELECT distinct ifnull(dim_dateid, 1) dim_dateid, fact_Qualitynotificationitemid 
	 FROM  QMSM QM ,dim_date dnc, dim_company dc, fact_Qualitynotificationitem fi
		WHERE  fi.dim_companyid = dc.dim_companyid
		AND dnc.CompanyCode = dc.CompanyCode
		AND dnc.DateValue = QM.QMSM_ERDAT 
		AND fi.dd_NotificationNo = QM.QMSM_QMNUM
		AND fi.dd_NotificationItemNo = QM.QMSM_FENUM
		AND QMSM_ERDAT IS NOT NULL
) t on fact.fact_Qualitynotificationitemid = t.fact_Qualitynotificationitemid
when matched then update set fact.dim_dateidqntaskcreated  = t.dim_dateid, fact.dw_update_date = current_timestamp
where fact.dim_dateidqntaskcreated  <> t.dim_dateid;


/* original update 
UPDATE fact_Qualitynotificationitem fi
SET fi.dim_dateidqntaskcompleted =  ifnull((SELECT dim_dateid FROM dim_date dnc
                      				WHERE dnc.DateValue = QM.QMSM_ERLDAT AND dnc.CompanyCode = dc.CompanyCode
							        AND QMSM_ERLDAT IS NOT NULL),1)
	,fi.dw_update_date = current_timestamp 
FROM QMSM QM, dim_company dc,fact_Qualitynotificationitem fi
WHERE fi.dd_NotificationNo = QM.QMSM_QMNUM
AND fi.dd_NotificationItemNo = QM.QMSM_FENUM
*/


merge into fact_Qualitynotificationitem fact
using
(
	 SELECT distinct ifnull(dim_dateid, 1) dim_dateid, fact_Qualitynotificationitemid 
	 FROM  QMSM QM ,dim_date dnc, dim_company dc, fact_Qualitynotificationitem fi
		WHERE  fi.dim_companyid = dc.dim_companyid
		AND dnc.CompanyCode = dc.CompanyCode
		AND dnc.DateValue = QM.QMSM_ERDAT 
		AND fi.dd_NotificationNo = QM.QMSM_QMNUM
		AND fi.dd_NotificationItemNo = QM.QMSM_FENUM
		AND QMSM_ERDAT IS NOT NULL
) t on fact.fact_Qualitynotificationitemid = t.fact_Qualitynotificationitemid
when matched then update set fact.dim_dateidqntaskcompleted  = t.dim_dateid, fact.dw_update_date = current_timestamp
where fact.dim_dateidqntaskcompleted  <> t.dim_dateid;



update fact_Qualitynotificationitem 
set dd_documentno = 'Not Set' 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dd_documentno is NULL;

update fact_Qualitynotificationitem 
set dd_documentitemno = 0 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dd_documentitemno is NULL;

update fact_Qualitynotificationitem 
set dim_vendorid = 1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_vendorid is NULL;

update fact_Qualitynotificationitem 
set Dim_dateidrecordcreated = 1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where Dim_dateidrecordcreated is NULL;

update fact_Qualitynotificationitem 
set dim_dateidnotificationcompletion = 1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_dateidnotificationcompletion is NULL;

update fact_Qualitynotificationitem 
set dd_VendorMaterialNo = 'Not Set' 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dd_VendorMaterialNo is NULL;

update fact_Qualitynotificationitem 
set dd_DefectNumber = 0 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dd_DefectNumber is NULL;

update fact_Qualitynotificationitem 
set dim_notificationtypeid = 1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_notificationtypeid is NULL;

update fact_Qualitynotificationitem 
set Dim_codetextCausesid = 1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where Dim_codetextCausesid is NULL;

update fact_Qualitynotificationitem 
set dim_dateidqntaskcreated = 1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_dateidqntaskcreated is NULL;

update fact_Qualitynotificationitem 
set dim_dateidqntaskcompleted = 1 
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where dim_dateidqntaskcompleted is NULL;

/*Update dim_part, dim_plant from fact_qualitynotification*/
UPDATE fact_qualitynotificationitem fqi
SET fqi.dim_partid = IFNULL(fq.dim_partid,1),
	fqi.dim_plantid = IFNULL(fq.dim_plantid,1)
	,fqi.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
FROM fact_qualitynotification fq,fact_qualitynotificationitem fqi
WHERE 	 
fqi.dd_NotificationNo = fq.dd_notificationo;

/* MDG Part */

DROP TABLE IF EXISTS tmp_dim_mdg_partid;
CREATE TABLE tmp_dim_mdg_partid as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;


UPDATE fact_Qualitynotificationitem f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM fact_Qualitynotificationitem f, tmp_dim_mdg_partid tmp
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

drop table if exists tmp_dim_mdg_partid;

/* Update BW Hierarchy */

update fact_salesorder f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_salesorder f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;