delete from fact_merckcontroltower_snapshot
where dim_dateidsnapshot = (select dim_Dateid from dim_date where datevalue = current_Date and companycode = 'Not Set');

drop table if exists tmp_ct_processing_day;
create table tmp_ct_processing_day as select case when hour(current_timestamp) between 0 and 20 then current_date - interval '1' day else current_date end tdy from dual;

update fact_merckcontroltower_snapshot
	set dd_current_snapshot = 'Not Set';

delete from number_fountain where table_name = 'fact_merckcontroltower_snapshot';
insert into number_fountain
	select 'fact_merckcontroltower_snapshot',
		ifnull(max(fact_merckcontroltower_snapshotid), ifnull((select dim_projectsourceid * multiplier from dim_projectsource),0))
	from fact_merckcontroltower_snapshot;

insert into fact_merckcontroltower_snapshot (fact_merckcontroltower_snapshotid, dw_update_date, dw_insert_date, amt_exchangerate, amt_exchangerate_gbl, dim_projectsourceid, dim_partid, dim_plantid, dim_mdg_partid, dim_bwproducthierarchy, dim_periodid, ct_stockavailable, ct_stockinqa, ct_stockrestricted, ct_stocktotal, amt_stocktotal_wcogs, amt_stocktotal_stdprice, amt_stockavailable_wcogs, amt_stockavailable_stdprice, amt_stockinqa_wcogs, amt_stockinqa_stdprice, amt_stockrestricted_wcogs, amt_stockrestricted_stdprice, ct_salesforecast, ct_shippedmtd, amt_shippedmtd, ct_opensales, amt_opensales, ct_shippedmtd_interco, ct_opensales_interco, ct_pastdue, amt_pastdue, ct_pastdue_interco, dim_companyid, amt_avgsaleprice, dim_bwhierarchycountryid, ct_purchopenqty, amt_salesop_euro, ct_salesop, dim_jda_productsubsetid, dd_dummyrow, dd_comment, ct_salescf_qty, amt_supplyrisk_euro, ct_avgsalesfcstqty, amt_ytdsalesop_euro, amt_ordered_3rdparty_euro, ct_ordered_3rdparty, amt_ordered_intercomp_euro, ct_ordered_intercomp, amt_ordered_3rdparty_ytd_euro, ct_ordered_3rdparty_ytd, amt_ordered_intercomp_ytd_euro, ct_ordered_intercomp_ytd, amt_shippedmtd_interco, ct_shippedytd, amt_shippedytd_euro, amt_shippedytd_ic_euro, ct_shippedytd_ic, amt_canceldemand_euro, ct_canceldemand, amt_canceldemand_ic_euro, ct_canceldemand_ic, amt_ytd_canceldemand_ic_euro, ct_ytd_canceldemand_ic, amt_ytd_canceldemand_euro, ct_ytd_canceldemand, ct_openqty, ct_totalqty, ct_eur_packs,ct_openqtypurchase,ct_TotalQtyplanned,ct_LOCAL_RISK_QTY,ct_LOCAL_RISK_EURO,ct_NETWORK_RISK_EURO,ct_NETWORK_RISK_QTY,dd_LOCALRISK_FLAG,dd_NETWORKRISK_FLAG,AMT_COGSFIXEDPLANRATE_EMD,dd_current_snapshot,dd_comment1,dd_comment2,dd_comment3,dd_mspiFlag,dim_mspiflagid,AMT_SHIPPEDYTD_EURO_JUN1,AMT_ORDERED_3RDPARTY_YTD_EURO_JUN1, CT_SHIPPEDYTD_JUN1, CT_ORDERED_3RDPARTY_YTD_JUN1, amt_ytd_canceldemand_euro_JUN1, ct_ytd_canceldemand_JUN1,DIM_WAVEFLAGID)
select (select max_id from number_fountain where table_name = 'fact_merckcontroltower_snapshot') + row_number() over(order by '') fact_merckcontroltower_snapshotid,
	current_timestamp as dw_update_date,
	current_timestamp as dw_insert_date,
	amt_exchangerate,
	amt_exchangerate_gbl,
	dim_projectsourceid,
	dim_partid,
	dim_plantid,
	dim_mdg_partid,
	dim_bwproducthierarchy,
	dim_periodid,
	ct_stockavailable,
	ct_stockinqa,
	ct_stockrestricted,
	ct_stocktotal,
	amt_stocktotal_wcogs,
	amt_stocktotal_stdprice,
	amt_stockavailable_wcogs,
	amt_stockavailable_stdprice,
	amt_stockinqa_wcogs,
	amt_stockinqa_stdprice,
	amt_stockrestricted_wcogs,
	amt_stockrestricted_stdprice,
	ct_salesforecast,
	ct_shippedmtd,
	amt_shippedmtd,
	ct_opensales,
	amt_opensales,
	ct_shippedmtd_interco,
	ct_opensales_interco,
	ct_pastdue,
	amt_pastdue,
	ct_pastdue_interco,
	dim_companyid,
	amt_avgsaleprice,
	dim_bwhierarchycountryid,
	ct_purchopenqty,
	amt_salesop_euro,
	ct_salesop,
	dim_jda_productsubsetid,
	dd_dummyrow,
	dd_comment,
	ct_salescf_qty,
	amt_supplyrisk_euro,
	ct_avgsalesfcstqty,
	amt_ytdsalesop_euro,
	amt_ordered_3rdparty_euro,
	ct_ordered_3rdparty,
	amt_ordered_intercomp_euro,
	ct_ordered_intercomp,
	amt_ordered_3rdparty_ytd_euro,
	ct_ordered_3rdparty_ytd,
	amt_ordered_intercomp_ytd_euro,
	ct_ordered_intercomp_ytd,
	amt_shippedmtd_interco,
	ct_shippedytd,
	amt_shippedytd_euro,
	amt_shippedytd_ic_euro,
	ct_shippedytd_ic,
	amt_canceldemand_euro,
	ct_canceldemand,
	amt_canceldemand_ic_euro,
	ct_canceldemand_ic,
	amt_ytd_canceldemand_ic_euro,
	ct_ytd_canceldemand_ic,
	amt_ytd_canceldemand_euro,
	ct_ytd_canceldemand,
	ct_openqty,
	ct_totalqty,
	ct_eur_packs,
	ct_openqtypurchase,
	ct_TotalQtyplanned,
	dfr.LOCAL_RISK_QTY,
	dfr.LOCAL_RISK_EURO,
	dfr.NETWORK_RISK_EURO,
	dfr.NETWORK_RISK_QTY,
	dfr.LOCALRISK_FLAG,
	dfr.NETWORKRISK_FLAG,
	f.AMT_COGSFIXEDPLANRATE_EMD,
	'X' as dd_current_snapshot,
	f.dd_comment1,
	f.dd_comment2,
	f.dd_comment3,
	f.dd_mspiFlag,
	f.dim_mspiflagid,
	f.AMT_SHIPPEDYTD_EURO_JUN1,
	f.AMT_ORDERED_3RDPARTY_YTD_EURO_JUN1,
	f.CT_SHIPPEDYTD_JUN1,
	f.CT_ORDERED_3RDPARTY_YTD_JUN1,
	f.amt_ytd_canceldemand_euro_JUN1,
	f.ct_ytd_canceldemand_JUN1,
	f.DIM_WAVEFLAGID
from fact_merckcontroltower f, dim_Date dt, dim_hc_demandfulfillmentrisk dfr
where dim_periodid = dim_Dateid
	and dfr.dim_hc_demandfulfillmentriskid = f.dim_hc_demandfulfillmentriskid
	and date_trunc('month',datevalue) = date_trunc('month',(select tdy from tmp_ct_processing_day));

update fact_merckcontroltower_snapshot
	set dim_dateidsnapshot = (select dim_Dateid from dim_date where datevalue = current_Date and companycode = 'Not Set')
where dim_dateidsnapshot = 1;

/* previous month values to calculate delta */
drop table if exists tmp_ct_snap_prevmonth;
create table tmp_ct_snap_prevmonth as
select dim_partid,
	dim_plantid,
	dim_periodid,
	dt.datevalue,
	CT_PASTDUE,
	AMT_PASTDUE,
	ct_opensales,
	amt_opensales,
	ct_salesforecast,
	AMT_STOCKTOTAL_WCOGS,
	AMT_STOCKTOTAL_STDPRICE,
	CT_STOCKTOTAL,
	CT_PASTDUE_INTERCO,
	ct_NETWORK_RISK_EURO,
	ct_NETWORK_RISK_QTY,
	amt_ordered_3rdparty_euro,
	CT_ORDERED_INTERCOMP,
	amt_supplyrisk_euro
from fact_merckcontroltower_snapshot f, dim_Date snp, dim_Date dt
where dim_dateidsnapshot = snp.dim_Dateid
	and f.dim_periodid = dt.dim_Dateid
and snp.datevalue = (case when current_Date = date_trunc('month', current_date) then current_date - INTERVAL '1' MONTH
						else date_trunc('month', current_date)
				end);

update fact_merckcontroltower_snapshot f
	set f.amt_pastdue_prevmonth =  t.amt_pastdue,
		f.ct_pastdue_prevm =  t.ct_pastdue,
		f.ct_opensales_prevm =  t.ct_opensales,
		f.amt_opensales_prevm =  t.amt_opensales,
		f.ct_salesforecast_prevm =  t.ct_salesforecast,
		f.amt_stocktotal_wcogs_prevm =  t.amt_stocktotal_wcogs,
		f.amt_stocktotal_stdprice_prevm =  t.amt_stocktotal_stdprice,
		f.ct_stocktotal_prevm =  t.ct_stocktotal,
		f.ct_pastdue_interco_prevm =  t.ct_pastdue_interco,
		f.ct_network_risk_euro_prevm =  t.ct_network_risk_euro,
		f.ct_network_risk_qty_prevm =  t.ct_network_risk_qty,
		f.amt_ordered_3rdparty_euro_prevm =  t.amt_ordered_3rdparty_euro,
		f.ct_ordered_intercomp_prevm =  t.ct_ordered_intercomp,
		f.amt_supplyrisk_euro_prevm =  t.amt_supplyrisk_euro
from fact_merckcontroltower_snapshot f,  dim_Date snp, tmp_ct_snap_prevmonth t
where dim_dateidsnapshot = snp.dim_Dateid
	and f.dim_partid = t.dim_partid
	and	f.dim_plantid  = t.dim_plantid
	and snp.datevalue = current_Date;

/* previous month values to calculate delta */
drop table if exists tmp_ct_snap_prevday;
create table tmp_ct_snap_prevday as
select dim_partid,
	dim_plantid,
	dim_periodid,
	dt.datevalue,
	CT_PASTDUE,
	AMT_PASTDUE,
	ct_opensales,
	amt_opensales,
	ct_salesforecast,
	AMT_STOCKTOTAL_WCOGS,
	AMT_STOCKTOTAL_STDPRICE,
	CT_STOCKTOTAL,
	CT_PASTDUE_INTERCO,
	ct_NETWORK_RISK_EURO,
	ct_NETWORK_RISK_QTY,
	amt_ordered_3rdparty_euro,
	CT_ORDERED_INTERCOMP,
	amt_supplyrisk_euro
from fact_merckcontroltower_snapshot f, dim_Date snp, dim_Date dt
where dim_dateidsnapshot = snp.dim_Dateid
	and f.dim_periodid = dt.dim_Dateid
and snp.datevalue = current_date - 1;

update fact_merckcontroltower_snapshot f
	set f.amt_pastdue_prevday =  t.amt_pastdue,
		f.ct_pastdue_prevd =  t.ct_pastdue,
		f.ct_opensales_prevd =  t.ct_opensales,
		f.amt_opensales_prevd =  t.amt_opensales,
		f.ct_salesforecast_prevd =  t.ct_salesforecast,
		f.amt_stocktotal_wcogs_prevd =  t.amt_stocktotal_wcogs,
		f.amt_stocktotal_stdprice_prevd =  t.amt_stocktotal_stdprice,
		f.ct_stocktotal_prevd =  t.ct_stocktotal,
		f.ct_pastdue_interco_prevd =  t.ct_pastdue_interco,
		f.ct_network_risk_euro_prevd =  t.ct_network_risk_euro,
		f.ct_network_risk_qty_prevd =  t.ct_network_risk_qty,
		f.amt_ordered_3rdparty_euro_prevd =  t.amt_ordered_3rdparty_euro,
		f.ct_ordered_intercomp_prevd =  t.ct_ordered_intercomp,
		f.amt_supplyrisk_euro_prevd =  t.amt_supplyrisk_euro
from fact_merckcontroltower_snapshot f,  dim_Date snp, tmp_ct_snap_prevday t
where dim_dateidsnapshot = snp.dim_Dateid
	and f.dim_partid = t.dim_partid
	and	f.dim_plantid  = t.dim_plantid
	and snp.datevalue = current_Date;

update EMD586.fact_merckcontroltower_snapshot
		set dd_current_snapshot = 'Not Set'
where dim_projectsourceid = 3;

delete from EMD586.fact_merckcontroltower_snapshot
where dim_dateidsnapshot = (select dim_Dateid from dim_date where datevalue = current_Date and companycode = 'Not Set')
and dim_projectsourceid = 3;
