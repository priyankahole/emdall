/* Cristian Cleciu - App-8452 - update deleted fields with x in STAS according to TEMPO EU BOM logic */

DROP TABLE IF EXISTS tmp_fact_bomv2_stastodate;
CREATE TABLE tmp_fact_bomv2_stastodate AS
SELECT STAS_STLNR, STAS_STLTY,STAS_STLAL, STAS_STLKN, count(stas_datuv) count, max(stas_datuv) stas_datuv FROM bomv2_stas
where stas_lkenz IS NULL
GROUP BY STAS_STLNR, STAS_STLTY,STAS_STLAL, STAS_STLKN
HAVING count(stas_datuv)>1;
      
DROP TABLE IF EXISTS bomv2_stas_fix;
CREATE TABLE bomv2_stas_fix
AS SELECT * FROM bomv2_stas;

UPDATE bomv2_stas_fix
SET stas_lkenz = 'X'
FROM bomv2_stas_fix f, tmp_fact_bomv2_stastodate t
WHERE f.STAS_STLNR= t.STAS_STLNR
AND f.STAS_STLTY= t.STAS_STLTY
AND f.STAS_STLAL= t.STAS_STLAL
AND f.STAS_STLKN= t.STAS_STLKN
AND f.stas_datuv= t.stas_datuv;

/* END Cristian Cleciu - App-8452 - update deleted fields with x in STAS according to TEMPO EU BOM logic */


drop table if exists BOMV2_BOMlevels_basis;
create table BOMV2_BOMlevels_basis as

select M.MAST_MATNR as MaterialNumber,
	m.MAST_WERKS as PlantCode,
	m.MAST_STLAL as AlternativeBOM,
	m.MAST_STLNR as BOMNumber,
	m.MAST_STLAN as BOMUsage,
	SP.STPO_POSNR as Item,
	SP.STPO_IDNRK as Component,
	SP.STPO_POTX1 as ComponentDescription,
	SP.STPO_STLTY as BOMCategory,
	SP.STPO_STLKN as BOMItemNodeNumber,
	SP.STPO_DATUV as ValidFromDate,
	SP.STPO_MENGE as ComponentQuantity,
	sp.STPO_STPOZ as BomItemCounter,
	sp.STPO_REKRS AS isRecursive,
	sk.STKO_STKOZ as bomHeaderCounter
from
	BOMV2_STPO SP
	inner JOIN BOMV2_STKO SK ON SP.STPO_STLTY = SK.STKO_STLTY AND SP.STPO_STLNR = SK.STKO_STLNR
	inner JOIN bomv2_stas_fix S ON S.STAS_STLNR = SK.STKO_STLNR AND S.STAS_STLTY = SP.STPO_STLTY
	AND S.STAS_STLKN = SP.STPO_STLKN /*AND S.STAS_DATUV = SP.STPO_DATUV*/ AND
	S.STAS_STLAL = SK.STKO_STLAL
	inner JOIN BOMV2_MAST M  ON M.MAST_STLNR = SK.STKO_STLNR AND SK.STKO_STLAL = M.MAST_STLAL
WHERE SK.STKO_LKENZ is null AND SK.STKO_LOEKZ is null and S.STAS_LKENZ is null;
/*AND SK.STKO_STLST in ('10','15','16','12','11','17')*/

DROP TABLE IF EXISTS BOMV2_BOMLevels_selectminalternative;
CREATE TABLE BOMV2_BOMLevels_selectminalternative
AS
SELECT DISTINCT BOMV2_mkal.mkal_matnr,BOMV2_mkal.mkal_werks,BOMV2_mkal.mkal_stlal
from BOMV2_MKAL,
	(
	SELECT mkal_matnr,mkal_werks,min(mkal_verid) mkal_verid
	FROM BOMV2_mkal where current_date >= mkal_adatu AND current_date <= mkal_bdatu AND IFNULL(MKAL_MKSP,-1) <> 1
	GROUP BY mkal_matnr,mkal_werks
	) t,BOMV2_Marc_prodorder m
WHERE BOMV2_mkal.mkal_matnr = t.mkal_matnr AND BOMV2_mkal.mkal_werks = t.mkal_werks AND BOMV2_mkal.mkal_verid = t.mkal_verid 
AND current_date >= BOMV2_mkal.mkal_adatu AND current_date <= BOMV2_mkal.mkal_bdatu AND IFNULL(BOMV2_mkal.MKAL_MKSP,-1) <> 1
and m.marc_matnr=t.mkal_matnr and m.marc_werks=t.mkal_werks and marc_altsl=3;

drop table if exists BOMV2_BOMLevels_minalternative_neq3;
create table BOMV2_BOMLevels_minalternative_neq3 as
select distinct materialnumber,plantcode, min(alternativebom) as alternativebom 
from BOMV2_BOMlevels_basis,BOMV2_marc_prodorder
where marc_matnr=materialnumber and marc_werks=plantcode and marc_altsl <> 3
group by materialnumber,plantcode;

insert into BOMV2_BOMLevels_selectminalternative select * from BOMV2_BOMLevels_minalternative_neq3;


/*Create level 1*/
drop table if exists BOMV2_BOMLevels_1;
create table BOMV2_BOMLevels_1 as
select distinct t0.materialnumber as rootpartnumber,t0.alternativebom as rootalternativebom,t0.bomnumber as rootbomnumber,t0.*, cast ('1' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0 /*,BOMLevels_selectminalternative
where t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks
AND ALTERNATIVEBOM = mkal_stlal*/ ;

/* Check the components that have active BOM's for Level 2*/
/* If a record for the material (component/Plant) exists in table MAST, it means it has a BOM*/


drop table if exists BOMV2_BOMLevels_2_mast_filter;
create table BOMV2_BOMLevels_2_mast_filter as
select distinct t0.* from BOMV2_BOMLevels_1 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
and t0.isRecursive IS NULL;

/*Create level 2 (for the above components)*/

drop table if exists BOMV2_BOMLevels_2;
create table BOMV2_BOMLevels_2 as
select distinct t2.rootpartnumber,t2.rootalternativebom,t2.rootbomnumber,t0.*, cast ('2' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_2_mast_filter t2, BOMV2_BOMLevels_selectminalternative 
where
  t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t2.component
and t0.plantcode=T2.plantcode
and t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 3*/
/* If a record for the material (component/Plant) exists in table MAST, it means it has a BOM*/

drop table if exists BOMV2_BOMLevels_mast_filter_3;
create table BOMV2_BOMLevels_mast_filter_3 as
select distinct t0.* from BOMV2_BOMLevels_2 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 3*/

drop table if exists BOMV2_BOMLevels_3;
create table BOMV2_BOMLevels_3 as
select distinct t3.rootpartnumber,t3.rootalternativebom,t3.rootbomnumber,t0.*, cast ('3' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_3 t3, BOMV2_BOMLevels_selectminalternative 
where
  t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t3.component
and t0.plantcode=T3.plantcode
and t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 4*/

drop table if exists BOMV2_BOMLevels_mast_filter_4;
create table BOMV2_BOMLevels_mast_filter_4 as
select distinct t0.* from BOMV2_BOMLevels_3 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 4*/

drop table if exists BOMV2_BOMLevels_4;
create table BOMV2_BOMLevels_4 as
select distinct t4.rootpartnumber,t4.rootalternativebom,t4.rootbomnumber ,t0.*, cast ('4' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_4 t4, BOMV2_BOMLevels_selectminalternative 
where
t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t4.component
and t0.plantcode=T4.plantcode
and t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 5*/
drop table if exists BOMV2_BOMLevels_mast_filter_5;
create table BOMV2_BOMLevels_mast_filter_5 as
select distinct t0.* from BOMV2_BOMLevels_4 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 5*/

drop table if exists BOMV2_BOMLevels_5;
create table BOMV2_BOMLevels_5 as
select distinct t5.rootpartnumber,t5.rootalternativebom,t5.rootbomnumber ,t0.*, cast ('5' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_5 t5, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t5.component
and t0.plantcode=T5.plantcode
and t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 6*/
drop table if exists BOMV2_BOMLevels_mast_filter_6;
create table BOMV2_BOMLevels_mast_filter_6 as
select distinct t0.* from BOMV2_BOMLevels_5 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 6*/

drop table if exists BOMV2_BOMLevels_6;
create table BOMV2_BOMLevels_6 as
select distinct t6.rootpartnumber,t6.rootalternativebom,t6.rootbomnumber ,t0.*, cast ('6' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_6 t6, BOMV2_BOMLevels_selectminalternative 
where
t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t6.component
and t0.plantcode=T6.plantcode
and t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 7*/
drop table if exists BOMV2_BOMLevels_mast_filter_7;
create table BOMV2_BOMLevels_mast_filter_7 as
select distinct t0.* from BOMV2_BOMLevels_6 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 7*/

drop table if exists BOMV2_BOMLevels_7;
create table BOMV2_BOMLevels_7 as
select distinct t7.rootpartnumber,t7.rootalternativebom,t7.rootbomnumber ,t0.*, cast ('7' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_7 t7, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t7.component
and t0.plantcode=T7.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 8*/
drop table if exists BOMV2_BOMLevels_mast_filter_8;
create table BOMV2_BOMLevels_mast_filter_8 as
select distinct t0.* from BOMV2_BOMLevels_7 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 8*/

drop table if exists BOMV2_BOMLevels_8;
create table BOMV2_BOMLevels_8 as
select distinct t8.rootpartnumber,t8.rootalternativebom,t8.rootbomnumber ,t0.*, cast ('8' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_8 t8, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t8.component
and t0.plantcode=T8.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 9*/
drop table if exists BOMV2_BOMLevels_mast_filter_9;
create table BOMV2_BOMLevels_mast_filter_9 as
select distinct t0.* from BOMV2_BOMLevels_8 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 9*/

drop table if exists BOMV2_BOMLevels_9;
create table BOMV2_BOMLevels_9 as
select distinct t9.rootpartnumber,t9.rootalternativebom,t9.rootbomnumber ,t0.*, cast ('9' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_9 t9, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t9.component
and t0.plantcode=T9.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 10*/
drop table if exists BOMV2_BOMLevels_mast_filter_10;
create table BOMV2_BOMLevels_mast_filter_10 as
select distinct t0.* from BOMV2_BOMLevels_9 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 10*/

drop table if exists BOMV2_BOMLevels_10;
create table BOMV2_BOMLevels_10 as
select distinct t10.rootpartnumber,t10.rootalternativebom,t10.rootbomnumber ,t0.*, cast ('10' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_10 t10, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t10.component
and t0.plantcode=T10.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 11*/
drop table if exists BOMV2_BOMLevels_mast_filter_11;
create table BOMV2_BOMLevels_mast_filter_11 as
select distinct t0.* from BOMV2_BOMLevels_10 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 11*/

drop table if exists BOMV2_BOMLevels_11;
create table BOMV2_BOMLevels_11 as
select distinct t11.rootpartnumber,t11.rootalternativebom,t11.rootbomnumber ,t0.*, cast ('11' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_11 t11, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t11.component
and t0.plantcode=T11.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 12*/
drop table if exists BOMV2_BOMLevels_mast_filter_12;
create table BOMV2_BOMLevels_mast_filter_12 as
select distinct t0.* from BOMV2_BOMLevels_11 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 12*/

drop table if exists BOMV2_BOMLevels_12;
create table BOMV2_BOMLevels_12 as
select distinct t12.rootpartnumber,t12.rootalternativebom,t12.rootbomnumber ,t0.*, cast ('12' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_12 t12, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t12.component
and t0.plantcode=T12.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 13*/
drop table if exists BOMV2_BOMLevels_mast_filter_13;
create table BOMV2_BOMLevels_mast_filter_13 as
select distinct t0.* from BOMV2_BOMLevels_12 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 13*/

drop table if exists BOMV2_BOMLevels_13;
create table BOMV2_BOMLevels_13 as
select distinct t13.rootpartnumber,t13.rootalternativebom,t13.rootbomnumber ,t0.*, cast ('13' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_13 t13, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t13.component
and t0.plantcode=T13.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 14*/
drop table if exists BOMV2_BOMLevels_mast_filter_14;
create table BOMV2_BOMLevels_mast_filter_14 as
select distinct t0.* from BOMV2_BOMLevels_13 t0, BOMV2_mast
where t0.component=mast_matnr
	and t0.plantcode=mast_werks
	and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 14*/

drop table if exists BOMV2_BOMLevels_14;
create table BOMV2_BOMLevels_14 as
select distinct t14.rootpartnumber,t14.rootalternativebom,t14.rootbomnumber ,t0.*, cast ('14' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_14 t14, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t14.component
and t0.plantcode=T14.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 15*/
drop table if exists BOMV2_BOMLevels_mast_filter_15;
create table BOMV2_BOMLevels_mast_filter_15 as
select distinct t0.* from BOMV2_BOMLevels_14 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 15*/

drop table if exists BOMV2_BOMLevels_15;
create table BOMV2_BOMLevels_15 as
select distinct t15.rootpartnumber,t15.rootalternativebom,t15.rootbomnumber ,t0.*, cast ('15' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_15 t15, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t15.component
and t0.plantcode=T15.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 16*/
drop table if exists BOMV2_BOMLevels_mast_filter_16;
create table BOMV2_BOMLevels_mast_filter_16 as
select distinct t0.* from BOMV2_BOMLevels_15 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 16*/

drop table if exists BOMV2_BOMLevels_16;
create table BOMV2_BOMLevels_16 as
select distinct t16.rootpartnumber,t16.rootalternativebom,t16.rootbomnumber ,t0.*, cast ('16' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_16 t16, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t16.component
and t0.plantcode=T16.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 17*/
drop table if exists BOMV2_BOMLevels_mast_filter_17;
create table BOMV2_BOMLevels_mast_filter_17 as
select distinct t0.* from BOMV2_BOMLevels_16 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 17*/

drop table if exists BOMV2_BOMLevels_17;
create table BOMV2_BOMLevels_17 as
select distinct t17.rootpartnumber,t17.rootalternativebom,t17.rootbomnumber ,t0.*, cast ('17' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_17 t17, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t17.component
and t0.plantcode=T17.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 18*/
drop table if exists BOMV2_BOMLevels_mast_filter_18;
create table BOMV2_BOMLevels_mast_filter_18 as
select distinct t0.* from BOMV2_BOMLevels_17 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 18*/

drop table if exists BOMV2_BOMLevels_18;
create table BOMV2_BOMLevels_18 as
select distinct t18.rootpartnumber,t18.rootalternativebom,t18.rootbomnumber ,t0.*, cast ('18' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_18 t18, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t18.component
and t0.plantcode=T18.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 19*/
drop table if exists BOMV2_BOMLevels_mast_filter_19;
create table BOMV2_BOMLevels_mast_filter_19 as
select distinct t0.* from BOMV2_BOMLevels_18 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 19*/

drop table if exists BOMV2_BOMLevels_19;
create table BOMV2_BOMLevels_19 as
select distinct t19.rootpartnumber,t19.rootalternativebom,t19.rootbomnumber ,t0.*, cast ('19' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_19 t19, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t19.component
and t0.plantcode=T19.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 20*/
drop table if exists BOMV2_BOMLevels_mast_filter_20;
create table BOMV2_BOMLevels_mast_filter_20 as
select distinct t0.* from BOMV2_BOMLevels_19 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 20*/

drop table if exists BOMV2_BOMLevels_20;
create table BOMV2_BOMLevels_20 as
select distinct t20.rootpartnumber,t20.rootalternativebom,t20.rootbomnumber ,t0.*, cast ('20' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_20 t20, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t20.component
and t0.plantcode=T20.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 21*/
drop table if exists BOMV2_BOMLevels_mast_filter_21;
create table BOMV2_BOMLevels_mast_filter_21 as
select distinct t0.* from BOMV2_BOMLevels_20 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 21*/

drop table if exists BOMV2_BOMLevels_21;
create table BOMV2_BOMLevels_21 as
select distinct t21.rootpartnumber,t21.rootalternativebom,t21.rootbomnumber ,t0.*, cast ('21' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_21 t21, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t21.component
and t0.plantcode=T21.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 22*/
drop table if exists BOMV2_BOMLevels_mast_filter_22;
create table BOMV2_BOMLevels_mast_filter_22 as
select distinct t0.* from BOMV2_BOMLevels_21 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 22*/

drop table if exists BOMV2_BOMLevels_22;
create table BOMV2_BOMLevels_22 as
select distinct t22.rootpartnumber,t22.rootalternativebom,t22.rootbomnumber ,t0.*, cast ('22' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_22 t22, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t22.component
and t0.plantcode=T22.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 23*/
drop table if exists BOMV2_BOMLevels_mast_filter_23;
create table BOMV2_BOMLevels_mast_filter_23 as
select distinct t0.* from BOMV2_BOMLevels_22 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 23*/

drop table if exists BOMV2_BOMLevels_23;
create table BOMV2_BOMLevels_23 as
select distinct t23.rootpartnumber,t23.rootalternativebom,t23.rootbomnumber ,t0.*, cast ('23' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_23 t23, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t23.component
and t0.plantcode=T23.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 24*/
drop table if exists BOMV2_BOMLevels_mast_filter_24;
create table BOMV2_BOMLevels_mast_filter_24 as
select distinct t0.* from BOMV2_BOMLevels_23 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 24*/

drop table if exists BOMV2_BOMLevels_24;
create table BOMV2_BOMLevels_24 as
select distinct t24.rootpartnumber,t24.rootalternativebom,t24.rootbomnumber ,t0.*, cast ('24' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_24 t24, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t24.component
and t0.plantcode=T24.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 25*/
drop table if exists BOMV2_BOMLevels_mast_filter_25;
create table BOMV2_BOMLevels_mast_filter_25 as
select distinct t0.* from BOMV2_BOMLevels_24 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 25*/

drop table if exists BOMV2_BOMLevels_25;
create table BOMV2_BOMLevels_25 as
select distinct t25.rootpartnumber,t25.rootalternativebom,t25.rootbomnumber ,t0.*, cast ('25' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_25 t25, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t25.component
and t0.plantcode=T25.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 26*/
drop table if exists BOMV2_BOMLevels_mast_filter_26;
create table BOMV2_BOMLevels_mast_filter_26 as
select distinct t0.* from BOMV2_BOMLevels_25 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 26*/

drop table if exists BOMV2_BOMLevels_26;
create table BOMV2_BOMLevels_26 as
select distinct t26.rootpartnumber,t26.rootalternativebom,t26.rootbomnumber ,t0.*, cast ('26' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_26 t26, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t26.component
and t0.plantcode=T26.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 27*/
drop table if exists BOMV2_BOMLevels_mast_filter_27;
create table BOMV2_BOMLevels_mast_filter_27 as
select distinct t0.* from BOMV2_BOMLevels_26 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 27*/

drop table if exists BOMV2_BOMLevels_27;
create table BOMV2_BOMLevels_27 as
select distinct t27.rootpartnumber,t27.rootalternativebom,t27.rootbomnumber ,t0.*, cast ('27' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_27 t27, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t27.component
and t0.plantcode=T27.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 28*/
drop table if exists BOMV2_BOMLevels_mast_filter_28;
create table BOMV2_BOMLevels_mast_filter_28 as
select distinct t0.* from BOMV2_BOMLevels_27 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 28*/

drop table if exists BOMV2_BOMLevels_28;
create table BOMV2_BOMLevels_28 as
select distinct t28.rootpartnumber,t28.rootalternativebom,t28.rootbomnumber ,t0.*, cast ('28' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_28 t28, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t28.component
and t0.plantcode=T28.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 29*/
drop table if exists BOMV2_BOMLevels_mast_filter_29;
create table BOMV2_BOMLevels_mast_filter_29 as
select distinct t0.* from BOMV2_BOMLevels_28 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 29*/

drop table if exists BOMV2_BOMLevels_29;
create table BOMV2_BOMLevels_29 as
select distinct t29.rootpartnumber,t29.rootalternativebom,t29.rootbomnumber ,t0.*, cast ('29' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_29 t29, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t29.component
and t0.plantcode=T29.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 30*/
drop table if exists BOMV2_BOMLevels_mast_filter_30;
create table BOMV2_BOMLevels_mast_filter_30 as
select distinct t0.* from BOMV2_BOMLevels_29 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 30*/

drop table if exists BOMV2_BOMLevels_30;
create table BOMV2_BOMLevels_30 as
select distinct t30.rootpartnumber,t30.rootalternativebom,t30.rootbomnumber ,t0.*, cast ('30' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_30 t30, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t30.component
and t0.plantcode=T30.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 31*/
drop table if exists BOMV2_BOMLevels_mast_filter_31;
create table BOMV2_BOMLevels_mast_filter_31 as
select distinct t0.* from BOMV2_BOMLevels_30 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 31*/

drop table if exists BOMV2_BOMLevels_31;
create table BOMV2_BOMLevels_31 as
select distinct t31.rootpartnumber,t31.rootalternativebom,t31.rootbomnumber ,t0.*, cast ('31' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_31 t31, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t31.component
and t0.plantcode=T31.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 32*/
drop table if exists BOMV2_BOMLevels_mast_filter_32;
create table BOMV2_BOMLevels_mast_filter_32 as
select distinct t0.* from BOMV2_BOMLevels_31 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 32*/

drop table if exists BOMV2_BOMLevels_32;
create table BOMV2_BOMLevels_32 as
select distinct t32.rootpartnumber,t32.rootalternativebom,t32.rootbomnumber ,t0.*, cast ('32' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_32 t32, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t32.component
and t0.plantcode=T32.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 33*/
drop table if exists BOMV2_BOMLevels_mast_filter_33;
create table BOMV2_BOMLevels_mast_filter_33 as
select distinct t0.* from BOMV2_BOMLevels_32 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 33*/

drop table if exists BOMV2_BOMLevels_33;
create table BOMV2_BOMLevels_33 as
select distinct t33.rootpartnumber,t33.rootalternativebom,t33.rootbomnumber ,t0.*, cast ('33' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_33 t33, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t33.component
and t0.plantcode=T33.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 34*/
drop table if exists BOMV2_BOMLevels_mast_filter_34;
create table BOMV2_BOMLevels_mast_filter_34 as
select distinct t0.* from BOMV2_BOMLevels_33 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 34*/

drop table if exists BOMV2_BOMLevels_34;
create table BOMV2_BOMLevels_34 as
select distinct t34.rootpartnumber,t34.rootalternativebom,t34.rootbomnumber ,t0.*, cast ('34' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_34 t34, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t34.component
and t0.plantcode=T34.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 35*/
drop table if exists BOMV2_BOMLevels_mast_filter_35;
create table BOMV2_BOMLevels_mast_filter_35 as
select distinct t0.* from BOMV2_BOMLevels_34 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 35*/

drop table if exists BOMV2_BOMLevels_35;
create table BOMV2_BOMLevels_35 as
select distinct t35.rootpartnumber,t35.rootalternativebom,t35.rootbomnumber ,t0.*, cast ('35' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_35 t35, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t35.component
and t0.plantcode=T35.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 36*/
drop table if exists BOMV2_BOMLevels_mast_filter_36;
create table BOMV2_BOMLevels_mast_filter_36 as
select distinct t0.* from BOMV2_BOMLevels_35 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 36*/

drop table if exists BOMV2_BOMLevels_36;
create table BOMV2_BOMLevels_36 as
select distinct t36.rootpartnumber,t36.rootalternativebom,t36.rootbomnumber ,t0.*, cast ('36' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_36 t36, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t36.component
and t0.plantcode=T36.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 37*/
drop table if exists BOMV2_BOMLevels_mast_filter_37;
create table BOMV2_BOMLevels_mast_filter_37 as
select distinct t0.* from BOMV2_BOMLevels_36 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 37*/

drop table if exists BOMV2_BOMLevels_37;
create table BOMV2_BOMLevels_37 as
select distinct t37.rootpartnumber,t37.rootalternativebom,t37.rootbomnumber ,t0.*, cast ('37' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_37 t37, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t37.component
and t0.plantcode=T37.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 38*/
drop table if exists BOMV2_BOMLevels_mast_filter_38;
create table BOMV2_BOMLevels_mast_filter_38 as
select distinct t0.* from BOMV2_BOMLevels_37 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 38*/

drop table if exists BOMV2_BOMLevels_38;
create table BOMV2_BOMLevels_38 as
select distinct t38.rootpartnumber,t38.rootalternativebom,t38.rootbomnumber ,t0.*, cast ('38' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_38 t38, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t38.component
and t0.plantcode=T38.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 39*/
drop table if exists BOMV2_BOMLevels_mast_filter_39;
create table BOMV2_BOMLevels_mast_filter_39 as
select distinct t0.* from BOMV2_BOMLevels_38 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 39*/

drop table if exists BOMV2_BOMLevels_39;
create table BOMV2_BOMLevels_39 as
select distinct t39.rootpartnumber,t39.rootalternativebom,t39.rootbomnumber ,t0.*, cast ('39' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_39 t39, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t39.component
and t0.plantcode=T39.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 40*/
drop table if exists BOMV2_BOMLevels_mast_filter_40;
create table BOMV2_BOMLevels_mast_filter_40 as
select distinct t0.* from BOMV2_BOMLevels_39 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 40*/

drop table if exists BOMV2_BOMLevels_40;
create table BOMV2_BOMLevels_40 as
select distinct t40.rootpartnumber,t40.rootalternativebom,t40.rootbomnumber ,t0.*, cast ('40' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_40 t40, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t40.component
and t0.plantcode=T40.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 41*/
drop table if exists BOMV2_BOMLevels_mast_filter_41;
create table BOMV2_BOMLevels_mast_filter_41 as
select distinct t0.* from BOMV2_BOMLevels_40 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 41*/

drop table if exists BOMV2_BOMLevels_41;
create table BOMV2_BOMLevels_41 as
select distinct t41.rootpartnumber,t41.rootalternativebom,t41.rootbomnumber ,t0.*, cast ('41' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_41 t41, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t41.component
and t0.plantcode=T41.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 42*/
drop table if exists BOMV2_BOMLevels_mast_filter_42;
create table BOMV2_BOMLevels_mast_filter_42 as
select distinct t0.* from BOMV2_BOMLevels_41 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 42*/

drop table if exists BOMV2_BOMLevels_42;
create table BOMV2_BOMLevels_42 as
select distinct t42.rootpartnumber,t42.rootalternativebom,t42.rootbomnumber ,t0.*, cast ('42' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_42 t42, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t42.component
and t0.plantcode=T42.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 43*/
drop table if exists BOMV2_BOMLevels_mast_filter_43;
create table BOMV2_BOMLevels_mast_filter_43 as
select distinct t0.* from BOMV2_BOMLevels_42 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 43*/

drop table if exists BOMV2_BOMLevels_43;
create table BOMV2_BOMLevels_43 as
select distinct t43.rootpartnumber,t43.rootalternativebom,t43.rootbomnumber ,t0.*, cast ('43' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_43 t43, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t43.component
and t0.plantcode=T43.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 44*/
drop table if exists BOMV2_BOMLevels_mast_filter_44;
create table BOMV2_BOMLevels_mast_filter_44 as
select distinct t0.* from BOMV2_BOMLevels_43 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 44*/

drop table if exists BOMV2_BOMLevels_44;
create table BOMV2_BOMLevels_44 as
select distinct t44.rootpartnumber,t44.rootalternativebom,t44.rootbomnumber ,t0.*, cast ('44' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_44 t44, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t44.component
and t0.plantcode=T44.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 45*/
drop table if exists BOMV2_BOMLevels_mast_filter_45;
create table BOMV2_BOMLevels_mast_filter_45 as
select distinct t0.* from BOMV2_BOMLevels_44 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 45*/

drop table if exists BOMV2_BOMLevels_45;
create table BOMV2_BOMLevels_45 as
select distinct t45.rootpartnumber,t45.rootalternativebom,t45.rootbomnumber ,t0.*, cast ('45' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_45 t45, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t45.component
and t0.plantcode=T45.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 46*/
drop table if exists BOMV2_BOMLevels_mast_filter_46;
create table BOMV2_BOMLevels_mast_filter_46 as
select distinct t0.* from BOMV2_BOMLevels_45 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 46*/

drop table if exists BOMV2_BOMLevels_46;
create table BOMV2_BOMLevels_46 as
select distinct t46.rootpartnumber,t46.rootalternativebom,t46.rootbomnumber ,t0.*, cast ('46' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_46 t46, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t46.component
and t0.plantcode=T46.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 47*/
drop table if exists BOMV2_BOMLevels_mast_filter_47;
create table BOMV2_BOMLevels_mast_filter_47 as
select distinct t0.* from BOMV2_BOMLevels_46 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 47*/

drop table if exists BOMV2_BOMLevels_47;
create table BOMV2_BOMLevels_47 as
select distinct t47.rootpartnumber,t47.rootalternativebom,t47.rootbomnumber ,t0.*, cast ('47' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_47 t47, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t47.component
and t0.plantcode=T47.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 48*/
drop table if exists BOMV2_BOMLevels_mast_filter_48;
create table BOMV2_BOMLevels_mast_filter_48 as
select distinct t0.* from BOMV2_BOMLevels_47 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 48*/

drop table if exists BOMV2_BOMLevels_48;
create table BOMV2_BOMLevels_48 as
select distinct t48.rootpartnumber,t48.rootalternativebom,t48.rootbomnumber ,t0.*, cast ('48' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_48 t48, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t48.component
and t0.plantcode=T48.plantcode
and  t0.alternativebom=mkal_stlal;

/* Check the components that have active BOM's for Level 49*/
drop table if exists BOMV2_BOMLevels_mast_filter_49;
create table BOMV2_BOMLevels_mast_filter_49 as
select distinct t0.* from BOMV2_BOMLevels_48 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 49*/

drop table if exists BOMV2_BOMLevels_49;
create table BOMV2_BOMLevels_49 as
select distinct t49.rootpartnumber,t49.rootalternativebom,t49.rootbomnumber ,t0.*, cast ('49' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_49 t49, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t49.component
and t0.plantcode=T49.plantcode
and  t0.alternativebom=mkal_stlal;


/* Check the components that have active BOM's for Level 50*/
drop table if exists BOMV2_BOMLevels_mast_filter_50;
create table BOMV2_BOMLevels_mast_filter_50 as
select distinct t0.* from BOMV2_BOMLevels_49 t0, BOMV2_mast
where t0.component=mast_matnr
and t0.plantcode=mast_werks
and t0.component is not null
	and t0.isRecursive IS NULL;

/*Create level 50*/

drop table if exists BOMV2_BOMLevels_50;
create table BOMV2_BOMLevels_50 as
select distinct t50.rootpartnumber,t50.rootalternativebom,t50.rootbomnumber ,t0.*, cast ('50' as varchar(2)) as BomLevel 
from BOMV2_BOMlevels_basis t0, BOMV2_BOMLevels_mast_filter_50 t50, BOMV2_BOMLevels_selectminalternative 
where
 t0.materialnumber=mkal_matnr and t0.plantcode=mkal_werks and t0.materialnumber=t50.component
and t0.plantcode=T50.plantcode
and  t0.alternativebom=mkal_stlal;



drop table if exists BOMV2_BOMLevels;
create table BOMV2_BOMLevels as
select * from BOMV2_BOMLevels_1
union all
select * from BOMV2_BOMLevels_2
union all
select * from BOMV2_BOMLevels_3
union all
select * from BOMV2_BOMLevels_4
union all
select * from BOMV2_BOMLevels_5
union all
select * from BOMV2_BOMLevels_6
union all
select * from BOMV2_BOMLevels_7
union all
select * from BOMV2_BOMLevels_8
union all
select * from BOMV2_BOMLevels_9
union all
select * from BOMV2_BOMLevels_10
union all
select * from BOMV2_BOMLevels_11
union all
select * from BOMV2_BOMLevels_12
union all
select * from BOMV2_BOMLevels_13
union all
select * from BOMV2_BOMLevels_14
union all
select * from BOMV2_BOMLevels_15
union all
select * from BOMV2_BOMLevels_16
union all
select * from BOMV2_BOMLevels_17
union all
select * from BOMV2_BOMLevels_18
union all
select * from BOMV2_BOMLevels_19
union all
select * from BOMV2_BOMLevels_20
union all
select * from BOMV2_BOMLevels_21
union all
select * from BOMV2_BOMLevels_22
union all
select * from BOMV2_BOMLevels_23
union all
select * from BOMV2_BOMLevels_24
union all
select * from BOMV2_BOMLevels_25
union all
select * from BOMV2_BOMLevels_26
union all
select * from BOMV2_BOMLevels_27
union all
select * from BOMV2_BOMLevels_28
union all
select * from BOMV2_BOMLevels_29
union all
select * from BOMV2_BOMLevels_30
union all
select * from BOMV2_BOMLevels_31
union all
select * from BOMV2_BOMLevels_32
union all
select * from BOMV2_BOMLevels_33
union all
select * from BOMV2_BOMLevels_34
union all
select * from BOMV2_BOMLevels_35
union all
select * from BOMV2_BOMLevels_36
union all
select * from BOMV2_BOMLevels_37
union all
select * from BOMV2_BOMLevels_38
union all
select * from BOMV2_BOMLevels_39
union all
select * from BOMV2_BOMLevels_40
union all
select * from BOMV2_BOMLevels_41
union all
select * from BOMV2_BOMLevels_42
union all
select * from BOMV2_BOMLevels_43
union all
select * from BOMV2_BOMLevels_44
union all
select * from BOMV2_BOMLevels_45
union all
select * from BOMV2_BOMLevels_46
union all
select * from BOMV2_BOMLevels_47
union all
select * from BOMV2_BOMLevels_48
union all
select * from BOMV2_BOMLevels_49
union all
select * from BOMV2_BOMLevels_50;


DROP TABLE IF EXISTS BOMV2_tmp_pGlobalCurrency_bom;
CREATE TABLE BOMV2_tmp_pGlobalCurrency_bom ( pGlobalCurrency VARCHAR(3) NULL);

INSERT INTO BOMV2_tmp_pGlobalCurrency_bom VALUES ( 'USD' );

update BOMV2_tmp_pGlobalCurrency_bom
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');


/* MODIFY fact_bom_tmp_populate TO TRUNCATED */

DROP TABLE IF EXISTS BOMV2_tmp_fact_bom_populate;
CREATE TABLE BOMV2_tmp_fact_bom_populate
like fact_bomv2  including identity including defaults;

alter table BOMV2_tmp_fact_bom_populate add column dd_plant varchar(7) default 'Not Set';

insert into BOMV2_tmp_fact_bom_populate(
	dd_rootpart,
	dd_partnumber,
	dd_immediateparentpart,
	dd_Alternative,
	dd_alternative_orig,
	dd_level,
	dd_BomItemNo,
	dd_BOMItemNodeNo,
	dd_BomNumber,
	dd_ComponentNumber,
	Dim_BomCategoryId,
	dd_plant,
	dd_BomItemCounter,
	dd_bomusage,
	dim_projectsourceid,
	dd_bomHeaderCounter,
	fact_bomv2id)
SELECT 
	ifnull(rootpartnumber, 'Not Set') as dd_rootpart,
	ifnull(rootpartnumber, 'Not Set') as dd_partnumber,
	ifnull(materialnumber, 'Not Set') as dd_immediateparentpart,
	ifnull(rootalternativebom, 'Not Set') as dd_Alternative ,
	ifnull(alternativebom, 'Not Set') as dd_alternative_orig,
	ifnull(BomLevel, 0) as dd_level,
	ifnull(item, 'Not Set') as dd_BomItemNo,
	ifnull(BomItemNodeNumber, 0) as dd_BOMItemNodeNo,
	ifnull(rootbomnumber, 'Not Set') as dd_BomNumber,
	ifnull(Component, 'Not Set') as dd_ComponentNumber,
	ifnull(bc.Dim_BomCategoryId, 1) as Dim_BomCategoryId,
	ifnull(plantcode,'Not Set') as dd_plant,
	ifnull(BomItemCounter, 0) as dd_BomItemCounter,
	ifnull(BOMUsage, 'Not Set') as dd_bomusage,
	(select s.dim_projectsourceid from dim_projectsource s) dim_projectsourceid,
	bomHeaderCounter as dd_bomHeaderCounter,
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1) + row_number() over(order by '') fact_bomv2id
FROM
BOMV2_BOMLevels
  INNER JOIN dim_bomcategory bc
             ON bc.Category = 'M' AND bc.RowIsCurrent = 1;


merge into BOMV2_tmp_fact_bom_populate f
using (select distinct t.fact_bomv2id, s.STPO_FMENG,s.STPO_LKENZ,s.STPO_POTX1,s.STPO_SORTF, s.STPO_CSSTR,s.STPO_MENGE,
s.STPO_AUSCH, s.STPO_NLFZT, s.STPO_PREIS, s.STPO_PEINH,s.STPO_VGKNT,s.STPO_AENAM, s.STPO_ANNAM,s.STPO_STPOZ,s.STPO_NLFZV
		from BOMV2_tmp_fact_bom_populate t,dim_bomcategory bc, BOMV2_STPO s
		where
		t.dim_bomcategoryid=bc.dim_bomcategoryid and
		s.STPO_STLTY=bc.Category and
		s.STPO_STLNR =t.dd_BomNumber and
		s.STPO_STLKN = t.dd_BOMItemNodeNo and
		s.STPO_STPOZ = dd_BomItemCounter) t1
	on t1.fact_bomv2id=f.fact_bomv2id
when matched then update set
		dd_fixedqty=   ifnull(t1.STPO_FMENG, 'Not Set') ,
		dd_deletionindicator_stpo=  ifnull(t1.STPO_LKENZ, 'Not Set') ,
		dd_bomitemtext1=  ifnull(t1.STPO_POTX1, 'Not Set') ,
		dd_sortstring =  ifnull(t1.STPO_SORTF, 'Not Set'),
		ct_AvgMatPurityinPercent= ifnull(t1.STPO_CSSTR,0),
		ct_ComponentQty = ifnull(t1.STPO_MENGE,0),
		ct_ComponentScrapinPercent = ifnull(t1.STPO_AUSCH,0) ,
		ct_LeadTimeOffset = ifnull(t1.STPO_NLFZT,0) ,
		amt_Price = ifnull(t1.STPO_PREIS,0) ,
		amt_PriceUnit= ifnull(t1.STPO_PEINH,1),
		dd_BOMPredecessorNodeNo = ifnull(t1.STPO_VGKNT, 0) ,
		dd_ChangedBy = ifnull(t1.STPO_AENAM, 'Not Set') ,
         dd_CreatedBy= ifnull(t1.STPO_ANNAM, 'Not Set'),
		 ct_OperationLeadTimeOffset = ifnull(t1.STPO_NLFZV,0);


merge into BOMV2_tmp_fact_bom_populate fa
USING ( SELECT
                t.fact_bomv2id, ifnull(uom.Dim_UnitOfMeasureid,1) AS Dim_ComponentUOMId
                FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                 LEFT JOIN dim_unitofmeasure uom ON uom.UOM = s.STPO_MEINS AND uom.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_ComponentUOMId = ifnull(SRC.Dim_ComponentUOMId,1);







MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T1.fact_bomv2id, ifnull(pitem.Dim_Partid,1) AS Dim_BOMComponentId
                FROM BOMV2_tmp_fact_bom_populate T1       
                Inner JOIN dim_part pitem ON pitem.PartNumber = T1.dd_ComponentNumber AND pitem.Plant = t1.dd_plant
                     AND pitem.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_BOMComponentId = ifnull(SRC.Dim_BOMComponentId,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T1.fact_bomv2id, ifnull(pitem.Dim_Partid,1) AS dim_rootpartid
                FROM BOMV2_tmp_fact_bom_populate T1
                                INNER JOIN dim_part pitem ON pitem.PartNumber = T1.dd_rootpart AND pitem.Plant = t1.dd_plant AND pitem.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.dim_rootpartid = ifnull(SRC.dim_rootpartid,1);


MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T1.fact_bomv2id, ifnull(pitem.Dim_Partid,1) AS dim_partidimmediateparent
                FROM BOMV2_tmp_fact_bom_populate T1
                                INNER JOIN dim_part pitem ON pitem.PartNumber = T1.dd_immediateparentpart AND pitem.Plant = t1.dd_plant AND pitem.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.dim_partidimmediateparent = ifnull(SRC.dim_partidimmediateparent,1);


MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(be.dim_dateid,1) AS Dim_ChangedOnDateid
               FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                    Inner join dim_date_factory_calendar be ON be.DateValue = ifnull(s.STPO_AEDAT,'0001-01-01') AND 'Not Set' = be.CompanyCode and be.plantcode_factory = ifnull(S.STPO_PSWRK, 'Not Set') 
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_ChangedOnDateid = ifnull(SRC.Dim_ChangedOnDateid,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(dr.dim_dateid,1) AS Dim_CreatedOnDateid
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_date_factory_calendar dr ON dr.DateValue = ifnull(STPO_ANDAT,'0001-01-01') AND 'Not Set' = dr.CompanyCode and dr.plantcode_factory = ifnull(s.STPO_PSWRK, 'Not Set')
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_CreatedOnDateid = ifnull(SRC.Dim_CreatedOnDateid,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(cur.Dim_Currencyid,1) AS Dim_Currencyid_TRA
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                  inner join dim_currency cur ON cur.CurrencyCode = ifnull(s.STPO_WAERS,'Not Set')
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid_TRA = ifnull(SRC.Dim_Currencyid_TRA,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(dp.Dim_PlantId,1) AS Dim_IssuingPlantId
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_plant dp ON dp.PlantCode = ifnull(s.STPO_PSWRK,'Not Set') AND dp.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_IssuingPlantId = ifnull(SRC.Dim_IssuingPlantId,1);


MERGE INTO BOMV2_tmp_fact_bom_populate  FA
USING ( SELECT
                T.fact_bomv2id, ifnull(dp.Dim_PlantId,1) AS Dim_MaterialPlantId
                FROM BOMV2_tmp_fact_bom_populate  T
                                inner join dim_plant dp ON dp.PlantCode = t.dd_plant AND dp.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_MaterialPlantId = ifnull(SRC.Dim_MaterialPlantId,1);


MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(porg.Dim_PurchaseOrgId,1) AS Dim_PurchasingOrgId
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join Dim_PurchaseOrg porg ON porg.PurchaseOrgCode = ifnull(s.STPO_EKORG,'Not Set') AND porg.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_PurchasingOrgId = ifnull(SRC.Dim_PurchasingOrgId,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(sl.Dim_StorageLocationid,1) AS Dim_StorageLocationid
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_storagelocation sl ON sl.LocationCode = ifnull(s.STPO_LGORT,'Not Set') AND sl.Plant = t.dd_plant AND sl.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_StorageLocationid = ifnull(SRC.Dim_StorageLocationid,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(pg.Dim_PurchaseGroupId,1) AS Dim_PurchasingGroupId
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join Dim_PurchaseGroup pg ON pg.PurchaseGroup = ifnull(s.STPO_EKGRP,'Not Set') AND pg.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_PurchasingGroupId = ifnull(SRC.Dim_PurchasingGroupId,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(mg.Dim_MaterialGroupid,1) AS Dim_MaterialGroupid
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join Dim_MaterialGroup mg ON mg.MaterialGroupCode = s.STPO_MATKL AND mg.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_MaterialGroupid = ifnull(SRC.Dim_MaterialGroupid,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(ls.dim_dateid,1) AS Dim_ValidFromDateid
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_date_factory_calendar ls ON ls.DateValue = ifnull(s.STPO_DATUV,'0001-01-01') AND 'Not Set' = ls.CompanyCode and ls.plantcode_factory = ifnull(S.STPO_PSWRK, 'Not Set')
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_ValidFromDateid = ifnull(SRC.Dim_ValidFromDateid,1);


MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(bic.Dim_BomItemCategoryId,1) AS Dim_BomItemCategoryId
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_bomitemcategory bic ON bic.ItemCategory = s.STPO_POSTP AND bic.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_BomItemCategoryId = ifnull(SRC.Dim_BomItemCategoryId,1);


MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(dv.Dim_VendorId,1) AS Dim_VendorId
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                inner join dim_Vendor dv ON dv.VendorNumber = s.STPO_LIFNR AND dv.RowIsCurrent = 1
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_VendorId = ifnull(SRC.Dim_VendorId,1);


MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(cur.Dim_Currencyid,1) AS Dim_Currencyid_GBL
                FROM BOMV2_tmp_fact_bom_populate T
                                ,dim_currency cur, BOMV2_tmp_pGlobalCurrency_bom
                                WHERE  cur.CurrencyCode = pGlobalCurrency
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid_GBL = ifnull(SRC.Dim_Currencyid_GBL,1);


MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, z.exchangeRate AS amt_ExchangeRate_GBL
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                                INNER JOIN tmp_getExchangeRate1 z ON ifnull(z.pFromCurrency,'x') = ifnull(s.STPO_WAERS,'x') AND z.fact_script_name = 'bi_populate_billofmaterials_fact' AND z.pDate = CURRENT_DATE
                                INNER JOIN BOMV2_tmp_pGlobalCurrency_bom ON z.pToCurrency = pGlobalCurrency
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.amt_ExchangeRate_GBL = ifnull(SRC.amt_ExchangeRate_GBL,1);


MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, ifnull(cur.Dim_Currencyid, 1) AS Dim_Currencyid
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                 inner join dim_plant dp on dp.PlantCode = s.STPO_PSWRK AND dp.RowIsCurrent = 1
                 inner join  dim_company c on dp.companycode = c.companycode
                inner join dim_currency cur on c.currency = cur.CurrencyCode

) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.Dim_Currencyid = ifnull(SRC.Dim_Currencyid,1);

MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING ( SELECT
                T.fact_bomv2id, z.exchangeRate AS amt_ExchangeRate
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
				  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                  inner join tmp_getExchangeRate1 z on z.pFromCurrency  = STPO_WAERS and z.fact_script_name = 'bi_populate_billofmaterials_fact'
				  inner join dim_plant dp on dp.PlantCode = STPO_PSWRK AND dp.RowIsCurrent = 1
                  inner join  dim_company c on dp.companycode = c.companycode and z.pToCurrency = c.currency AND z.pDate = s.STPO_AEDAT
) SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.amt_ExchangeRate = ifnull(SRC.amt_ExchangeRate,1);

UPDATE BOMV2_tmp_fact_bom_populate b
   SET dim_parentpartprodhierarchyid = ifnull(dim_producthierarchyid,1)
from dim_producthierarchy dph, dim_part dpr, BOMV2_tmp_fact_bom_populate b
WHERE dph.ProductHierarchy = dpr.ProductHierarchy
       AND dpr.Dim_Partid = b.Dim_partId
       AND dim_parentpartprodhierarchyid <> ifnull(dim_producthierarchyid,1);

UPDATE BOMV2_tmp_fact_bom_populate
   SET dim_parentpartprodhierarchyid = 1
 WHERE dim_parentpartprodhierarchyid IS NULL;
 
 
DROP TABLE IF EXISTS BOMV2_tmp_distinct_stko_stlst;
CREATE TABLE BOMV2_tmp_distinct_stko_stlst
AS
SELECT DISTINCT STKO_STLNR,STKO_STLST, STKO_STLAL,STKO_ANDAT,STKO_ANNAM,STKO_DATUV,STKO_LKENZ,STKO_STKTX, STKO_WRKAN, STKO_BMEIN, STKO_STLTY, STKO_STKOZ
FROM BOMV2_STKO
WHERE (RTRIM(STKO_LKENZ) = '' OR STKO_LKENZ IS NULL)
AND STKO_STLTY = 'M';
/* and STKO_STLST in ('1','3','11','12') */

merge into BOMV2_tmp_fact_bom_populate b using 
( select FACT_BOMV2ID, bs.Dim_bomstatusid -- max(bs.Dim_bomstatusid)
	from BOMV2_tmp_distinct_stko_stlst stko, 
		dim_bomstatus bs , 
		BOMV2_tmp_fact_bom_populate b
	WHERE b.dd_BomNumber = stko.stko_stlnr
	AND b.dd_alternative_orig = stko.stko_stlal
	and b.dd_bomHeaderCounter = stko.stko_stkoz
	AND bs.BOMStatusCode = ifnull(stko.STKO_STLST, 0)
	AND b.Dim_bomstatusid <> bs.Dim_bomstatusid
	-- GROUP by FACT_BOMV2ID
		)bs on bs.FACT_BOMV2ID = b.FACT_BOMV2ID
when matched then update 
SET b.Dim_bomstatusid = ifnull(bs.Dim_bomstatusid,1)
;

merge into BOMV2_tmp_fact_bom_populate b using 
( select FACT_BOMV2ID, dt.dim_dateid -- max(dt.dim_dateid)
		from BOMV2_tmp_distinct_stko_stlst stko, dim_date_factory_calendar dt, BOMV2_tmp_fact_bom_populate b, dim_plant p
		WHERE b.dd_BomNumber = stko.stko_stlnr
		AND b.dd_alternative_orig = stko.stko_stlal
		and b.dd_bomHeaderCounter = stko.stko_stkoz
		AND dt.datevalue= stko.STKO_ANDAT
		AND 'Not Set' = dt.CompanyCode
		and p.dim_plantid = b.dim_plantid
		and dt.plantcode_factory = ifnull(p.plantcode, 'Not Set')
		AND b.dim_creaedondateid_stko <> dt.dim_dateid
		--GROUP by FACT_BOMV2ID
		)bs on bs.FACT_BOMV2ID = b.FACT_BOMV2ID
when matched then update 
SET b.dim_creaedondateid_stko = ifnull(bs.dim_dateid,1);

merge into BOMV2_tmp_fact_bom_populate b using 
( select FACT_BOMV2ID, STKO_ANNAM --max(STKO_ANNAM)
	from BOMV2_tmp_distinct_stko_stlst stko, BOMV2_tmp_fact_bom_populate b
	WHERE b.dd_BomNumber = stko.stko_stlnr
	AND b.dd_alternative_orig = stko.stko_stlal
	and b.dd_bomHeaderCounter = stko.stko_stkoz
	AND b.dd_creationuser <> ifnull(stko.STKO_ANNAM, 'Not Set')
	--GROUP by FACT_BOMV2ID
	)bs on bs.FACT_BOMV2ID = b.FACT_BOMV2ID
when matched then update
SET b.dd_creationuser = ifnull(bs.STKO_ANNAM, 'Not Set');

merge into BOMV2_tmp_fact_bom_populate b using 
( select FACT_BOMV2ID, dt.dim_dateid --max(dt.dim_dateid)
	from BOMV2_tmp_distinct_stko_stlst stko, dim_date_factory_calendar dt,  BOMV2_tmp_fact_bom_populate b, dim_bomheader ds 
	WHERE b.dd_BomNumber = stko.stko_stlnr
	AND b.dd_alternative_orig = stko.stko_stlal
	and b.dd_bomHeaderCounter = stko.stko_stkoz
        AND dt.datevalue= stko.STKO_DATUV
        AND 'Not Set' = dt.CompanyCode
        and dt.plantcode_factory = 'Not Set' /*ifnull(stko.STKO_WRKAN, 'Not Set')*/
        and b.dim_bomheaderid = ds.dim_bomheaderid
        and stko.STKO_STLTY = ds.BOMCategory
        and stko.STKO_STLNR = ds.BillOfMaterial
        and stko.STKO_STLAL = ds.AlternativeBOM
        and stko.STKO_STKOZ = ds.InternalCounter
        AND b.dim_dateidvalidfrom <> dt.dim_dateid
	--GROUP by FACT_BOMV2ID
	)bs on bs.FACT_BOMV2ID = b.FACT_BOMV2ID
when matched then update
SET b.dim_dateidvalidfrom = ifnull(bs.dim_dateid,1);

/* Andrei 5.05.2017 BI - 5741*/
MERGE INTO BOMV2_tmp_fact_bom_populate FA
USING (
SELECT T.fact_bomv2id, ifnull(ls.dim_dateid,1) AS Dim_ValidFromDateid --max((ifnull(ls.dim_dateid,1))
                  FROM BOMV2_tmp_fact_bom_populate t
                  inner join  dim_bomcategory bc   on t.dim_bomcategoryid=bc.dim_bomcategoryid 
                  inner join BOMV2_STPO s on s.STPO_STLTY=bc.Category and s.STPO_STLNR =t.dd_BomNumber and s.STPO_STLKN = t.dd_BOMItemNodeNo and s.STPO_STPOZ = dd_BomItemCounter
                  inner join (select ls.CompanyCode, plantcode_factory, DateValue,dim_dateid from dim_date_factory_calendar ls  
							WHERE 'Not Set' = ls.CompanyCode
							GROUP BY ls.CompanyCode, plantcode_factory, DateValue,dim_dateid)ls
						ON 'Not Set' = ls.CompanyCode and ls.plantcode_factory = ifnull(S.STPO_PSWRK, 'Not Set') AND ls.DateValue = ifnull(s.STPO_DATUV,'0001-01-01')
                  inner join BOMV2_STKO st on st.STKO_STLNR=s.STPO_STLNR and st.STKO_STLTY=s.STPO_STLTY
		  and t.dd_bomHeaderCounter = st.stko_stkoz
	--	GROUP BY T.fact_bomid
	)SRC
ON FA.fact_bomv2id = SRC.fact_bomv2id
WHEN MATCHED THEN UPDATE SET FA.dim_dateidvalidfrom2 = ifnull(SRC.Dim_ValidFromDateid,1);


DROP TABLE IF EXISTS tmp_bomv2updateBillOfMaterialMisc;

CREATE TABLE tmp_bomv2updateBillOfMaterialMisc AS
SELECT distinct
bom.fact_bomv2id
,ifnull(bomi.Dim_BillOfMaterialMiscid,1) AS Dim_BillOfMaterialMiscid
 FROM dim_bomcategory bc,BOMV2_STPO p, Dim_BillOfMaterialMisc bomi , BOMV2_tmp_fact_bom_populate bom
 WHERE bc.dim_bomcategoryid = bom.dim_bomcategoryid
 AND bom.dd_BomNumber = p.STPO_STLNR
 AND bom.dd_BomItemNodeNo = p.STPO_STLKN
 AND bc.Category = p.STPO_STLTY
 AND bomi.FixedQuantity = ifnull(p.STPO_FMENG, 'Not Set')
 AND bomi.Net = ifnull(p.STPO_NETAU, 'Not Set')
 AND bomi.BulkMaterial = ifnull(p.STPO_SCHGT, 'Not Set')
 AND bomi.MaterialProvision = ifnull(p.STPO_BEIKZ, 'Not Set')
 AND bomi.ItemRelevantToSales = ifnull(p.STPO_RVREL, 'Not Set')
 AND bomi.ItemRelevantToProduction = ifnull(p.STPO_SANFE, 'Not Set')
 AND bomi.ItemRelevantForPlantMaint = ifnull(p.STPO_SANIN, 'Not Set')
 AND bomi.ItemRelevantToEngg = ifnull(p.STPO_SANKA, 'Not Set')
 AND bomi.BOMIsRecursive = ifnull(p.STPO_REKRI, 'Not Set')
 AND bomi.RecursivenessAllowed = ifnull(p.STPO_REKRS, 'Not Set')
 AND bomi.CAD = ifnull(p.STPO_CADPO, 'Not Set')
 AND bom.Dim_BillOfMaterialMiscid <> ifnull(bomi.Dim_BillOfMaterialMiscid,1);


UPDATE BOMV2_tmp_fact_bom_populate bom
 SET bom.Dim_BillOfMaterialMiscid = ifnull(tmp.Dim_BillOfMaterialMiscid,1)
 FROM  tmp_bomv2updateBillOfMaterialMisc tmp , BOMV2_tmp_fact_bom_populate bom
 WHERE bom.fact_bomv2id = tmp.fact_bomv2id
 AND bom.Dim_BillOfMaterialMiscid <> ifnull(tmp.Dim_BillOfMaterialMiscid,1);


UPDATE BOMV2_tmp_fact_bom_populate bom
SET Dim_BaseUOMId = 1 
FROM BOMV2_tmp_distinct_stko_stlst hdr, dim_bomstatus bs,dim_bomcategory bc , BOMV2_tmp_fact_bom_populate bom
 WHERE     bom.dd_BomNumber = STKO_STLNR
       AND bom.dd_Alternative = STKO_STLAL
	and bom.dd_bomHeaderCounter = stko_stkoz
       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
       AND bc.category = STKO_STLTY
       AND bc.RowIsCurrent = 1
       AND bs.BOMStatusCode = STKO_STLST
       AND bs.RowIsCurrent = 1
AND IFNULL(bom.dim_BaseUOMId,-1) <> 1;



MERGE INTO BOMV2_tmp_fact_bom_populate bom USING 
(	SELECT  FACT_BOMV2ID, ifnull(uom.Dim_UnitOfMeasureid,1)  Dim_UnitOfMeasureid --max(ifnull(uom.Dim_UnitOfMeasureid,1))
		FROM BOMV2_tmp_distinct_stko_stlst hdr, dim_bomstatus bs,dim_bomcategory bc,  BOMV2_tmp_fact_bom_populate bom
		 ,dim_unitofmeasure uom
		 WHERE     bom.dd_BomNumber = STKO_STLNR
		       AND bom.dd_Alternative = STKO_STLAL
		       and bom.dd_bomHeaderCounter = stko_stkoz
		       AND bom.Dim_BomcategoryId = bc.Dim_BomcategoryId
		       AND bc.category = STKO_STLTY
		       AND bc.RowIsCurrent = 1
		       AND bs.BOMStatusCode = STKO_STLST
		       AND bs.RowIsCurrent = 1
				 AND uom.UOM = STKO_BMEIN AND uom.RowIsCurrent = 1
				AND IFNULL(bom.Dim_BaseUOMId,-1) <> IFNULL(uom.Dim_UnitOfMeasureid,-2)
		       --GROUP by FACT_BOMV2ID
		       )bs on bs.FACT_BOMV2ID = bom.FACT_BOMV2ID
when matched then update
SET Dim_BaseUOMId = ifnull(bs.Dim_UnitOfMeasureid,1);


DROP TABLE IF EXISTS BOMV2_tmp_FactBOMValidToDate;

CREATE TABLE BOMV2_tmp_FactBOMValidToDate
AS
SELECT   distinct dim_bomcategoryid, dd_BomNumber,dd_BomItemNo,dd_bomHeaderCounter,dt.dateValue as ValidFromDate,'1 Jan 9999' as ValidToDate
FROM BOMV2_tmp_fact_bom_populate inner join dim_date dt on dt.dim_dateid = dim_validfromdateid 
where dim_validfromdateid <> 1;

DROP TABLE IF EXISTS BOMV2_tmp_FactBOMValidToDate_1;

CREATE TABLE BOMV2_tmp_FactBOMValidToDate_1 AS
SELECT distinct b1.dim_bomcategoryid, b1.dd_BomNumber,b1.dd_BomItemNo, b1.dd_bomHeaderCounter ,b2.validfromdate, (dt.datevalue - INTERVAL '1' DAY) ValidToDate FROM 
BOMV2_tmp_FactBOMValidToDate b2, BOMV2_tmp_fact_bom_populate b1,dim_date dt
WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid 
and b1.dd_BomNumber = b2.dd_BomNumber 
and b1.dd_BomItemNo = b2.dd_BomItemNo
and b1.dd_bomHeaderCounter = b2.dd_bomHeaderCounter
AND dt.dim_dateid = b1.dim_validfromdateid
AND b2.validfromdate < dt.datevalue;

MERGE INTO BOMV2_tmp_fact_bom_populate b1 USING (
	SELECT b1.fact_bomv2id, max(ifnull(dt.dim_dateid,1)) dim_dateid
	FROM BOMV2_tmp_FactBOMValidToDate_1 b2, dim_date_factory_calendar dt,dim_Date dfrd , BOMV2_tmp_fact_bom_populate b1
	WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid
		AND b1.dd_BomNumber = b2.dd_BomNumber
		AND b1.dd_BomItemNo = b2.dd_BomItemNo
		and b1.dd_bomHeaderCounter = b2.dd_bomHeaderCounter
		AND dfrd.dim_dateid = b1.dim_validfromdateid
		AND dfrd.datevalue = b2.validfromdate
		AND dfrd.companycode = 'Not Set'
		AND dt.datevalue = b2.ValidToDate
		AND dt.companycode = 'Not Set'
		AND dt.plantcode_factory = 'Not Set'
		AND b1.dim_dateidvalidto <> dt.dim_dateid
	GROUP BY b1.fact_bomv2id)dt on dt.fact_bomv2id = b1.fact_bomv2id
WHEN MATCHED THEN UPDATE 
SET b1.dim_dateidvalidto = ifnull(dt.dim_dateid,1);


UPDATE BOMV2_tmp_fact_bom_populate b1
SET b1.dim_dateidvalidto = ifnull(dt.Dim_DateId,1)
  FROM (select max(ifnull(Dim_DateId,1)) Dim_DateId from dim_date_factory_calendar dt
	where dt.DateValue = '9999-12-31'
  		AND dt.plantcode_factory = 'Not Set'
  		AND dt.CompanyCode = 'Not Set')dt , BOMV2_tmp_fact_bom_populate b1
WHERE (b1.dim_dateidvalidto = 1 OR b1.dim_dateidvalidto IS NULL);

   
DROP TABLE IF EXISTS BOMV2_tmp_FactBOMValidToDate2;

CREATE TABLE BOMV2_tmp_FactBOMValidToDate2
AS
SELECT   distinct dim_bomcategoryid, dd_BomNumber,dd_BomItemNo,dt.dateValue as ValidFromDate,'1 Jan 9999' as ValidToDate
FROM BOMV2_tmp_fact_bom_populate inner join dim_date dt on dt.dim_dateid = dim_dateidvalidfrom2 
where dim_dateidvalidfrom2 <> 1;

             
/* 
DROP TABLE IF EXISTS tmp_FactBOMValidToDate_2

CREATE TABLE tmp_FactBOMValidToDate_2 AS
SELECT distinct b1.dim_bomcategoryid, b1.dd_BomNumber,b1.dd_BomItemNo ,b2.validfromdate, (dt.datevalue - INTERVAL '1' DAY) ValidToDate FROM 
tmp_FactBOMValidToDate2 b2, tmp_fact_bom_populate b1,dim_date dt
WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid 
and b1.dd_BomNumber = b2.dd_BomNumber 
and b1.dd_BomItemNo = b2.dd_BomItemNo
AND dt.dim_dateid = b1.dim_dateidvalidfrom2
AND b2.validfromdate < dt.datevalue

merge into tmp_fact_bom_populate b1 using 
(select b1.fact_bomid, max(dt.dim_dateid)dim_dateid
	FROM	tmp_FactBOMValidToDate_2 b2, dim_date dt,dim_Date dfrd , tmp_fact_bom_populate b1
	WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid
	AND b1.dd_BomNumber = b2.dd_BomNumber
	AND b1.dd_BomItemNo = b2.dd_BomItemNo
	AND dfrd.dim_dateid = b1.dim_dateidvalidfrom2
	AND dfrd.datevalue = b2.validfromdate
	AND dfrd.companycode = 'Not Set'
	AND dt.datevalue = b2.ValidToDate
	AND dt.companycode = 'Not Set'
	AND dt.plantcode_factory = 'Not Set'
	AND b1.dim_dateidvalidto2 <> dt.dim_dateid
	GROUP BY  b1.fact_bomid)dt on  b1.fact_bomid =  dt.fact_bomid
when matched then update 
SET b1.dim_dateidvalidto2 = dt.dim_dateid

	
UPDATE tmp_fact_bom_populate b1
SET b1.dim_dateidvalidto2 = dt.Dim_DateId
  FROM dim_Date dt , tmp_fact_bom_populate b1
WHERE (b1.dim_dateidvalidto2 = 1 OR b1.dim_dateidvalidto2 IS NULL)
  AND dt.DateValue = '1 Jan 9999'
  AND dt.plantcode_factory = 'Not Set'
  AND dt.CompanyCode = 'Not Set'

  */
/* End Andrei 5.05.2017 BI - 5741*/


UPDATE BOMV2_tmp_fact_bom_populate b
SET b.dd_deletionindicator_stko = ifnull(stko.STKO_LKENZ, 'Not Set')
from BOMV2_tmp_distinct_stko_stlst stko, BOMV2_tmp_fact_bom_populate b
WHERE b.dd_BomNumber = stko.stko_stlnr
AND b.dd_alternative_orig = stko.stko_stlal
 and b.dd_bomHeaderCounter = stko.stko_stkoz
AND b.dd_deletionindicator_stko <> ifnull(stko.STKO_LKENZ, 'Not Set');

UPDATE BOMV2_tmp_fact_bom_populate b
SET b.dd_alternativebomtext = ifnull(stko.STKO_STKTX, 'Not Set')
from BOMV2_tmp_distinct_stko_stlst stko,
		BOMV2_tmp_fact_bom_populate b
WHERE b.dd_BomNumber = stko.stko_stlnr
	AND b.dd_alternative_orig = stko.stko_stlal
	and b.dd_bomHeaderCounter = stko.stko_stkoz
	AND b.dd_alternativebomtext <> ifnull(stko.STKO_STKTX, 'Not Set');

UPDATE BOMV2_tmp_fact_bom_populate b
SET b.dd_indicator_alt = ifnull(s.STZU_ALTST,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
FROM BOMV2_STZU s , BOMV2_tmp_fact_bom_populate b	
WHERE b.dd_BomNumber = s.STZU_STLNR
	AND s.STZU_STLTY = 'M'
	AND b.dd_indicator_alt <> ifnull(s.STZU_ALTST,'Not Set');

UPDATE BOMV2_tmp_fact_bom_populate b
SET b.dd_deletionindicator = ifnull(STKO_LOEKZ,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
FROM BOMV2_STKO , BOMV2_tmp_fact_bom_populate b	
WHERE (RTRIM(STKO_LKENZ) = '' OR STKO_LKENZ IS NULL)
	and b.dd_BomNumber = BOMV2_STKO.stko_stlnr
	AND b.dd_Alternative = BOMV2_STKO.stko_stlal
	and b.dd_bomHeaderCounter = BOMV2_STKO.stko_stkoz
	AND b.dd_deletionindicator <> ifnull(STKO_LOEKZ,'Not Set');

update BOMV2_tmp_fact_bom_populate b
set b.dd_ProductionSupplyArea = ifnull(STPO_PRVBE, 'Not Set')
from BOMV2_STPO p, dim_bomcategory c , BOMV2_tmp_fact_bom_populate b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STPO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STPO_STLNR, 'Not Set')
        and b.dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
		and b.dd_BomItemCounter = ifnull(STPO_STPOZ, 0)
        and b.dd_ProductionSupplyArea <> ifnull(STPO_PRVBE, 'Not Set');


/* Liviu Ionescu Fix missing IDs: BI-3558 */
UPDATE BOMV2_tmp_fact_bom_populate bom
SET Dim_PartId = 1
FROM  BOMV2_MAST m, dim_bomusage bu, dim_bomcategory bc, BOMV2_tmp_fact_bom_populate bom, dim_plant AS dp
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
AND bom.Dim_MaterialPlantId = dp.dim_plantid
AND dp.plantcode = m.MAST_WERKS
AND bom.Dim_PartId <> 1;


UPDATE BOMV2_tmp_fact_bom_populate bom
SET Dim_PartId = ifnull(pt.Dim_PartId,1)
FROM  BOMV2_MAST m, dim_bomusage bu, dim_bomcategory bc
	,dim_part pt , BOMV2_tmp_fact_bom_populate bom, dim_plant AS dp
WHERE m.MAST_STLNR = bom.dd_BomNumber
AND m.MAST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'M'
AND bom.Dim_MaterialPlantId = dp.dim_plantid
AND dp.plantcode = m.MAST_WERKS
AND pt.PartNumber = m.MAST_MATNR AND pt.Plant = m.MAST_WERKS AND pt.RowIsCurrent = 1
AND bom.Dim_PartId <> ifnull(pt.Dim_PartId,1);

/* Update Dim_PartId */
UPDATE    BOMV2_tmp_fact_bom_populate bom 
SET bom.Dim_PartId  =  1
FROM BOMV2_PRST p,dim_bomusage bu,dim_bomcategory bc , BOMV2_tmp_fact_bom_populate bom 
WHERE p.PRST_STLNR = bom.dd_BomNumber
AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P';	

UPDATE    BOMV2_tmp_fact_bom_populate bom 
SET bom.Dim_PartId  = ifnull(pt.Dim_PartId,1)			
FROM BOMV2_PRST p,dim_bomusage bu,dim_bomcategory bc
,dim_part pt , BOMV2_tmp_fact_bom_populate bom 
WHERE p.PRST_STLNR = bom.dd_BomNumber
AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P'
AND pt.PartNumber = p.PRST_MATNR
AND pt.Plant = p.PRST_WERKS
AND pt.RowIsCurrent = 1
AND bom.Dim_PartId  <> ifnull(pt.Dim_PartId,1);

MERGE INTO BOMV2_tmp_fact_bom_populate bom USING 
	(SELECT FACT_BOMV2ID, MAX(ifnull(bu.Dim_bomusageid,1)) Dim_bomusageid
	FROM  BOMV2_MAST m, dim_bomusage bu, dim_bomcategory bc ,  BOMV2_tmp_fact_bom_populate bom, dim_plant AS dp
	WHERE m.MAST_STLNR = bom.dd_BomNumber
	AND m.MAST_STLAL = bom.dd_Alternative
	AND bu.BOMUsageCode = m.MAST_STLAN AND bu.RowIsCurrent = 1
	AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
	AND bc.Category = 'M'
	AND bom.Dim_MaterialPlantId = dp.dim_plantid
    AND dp.plantcode = m.MAST_WERKS
	AND bom.Dim_BomUsageId <> bu.Dim_bomusageid
GROUP BY FACT_BOMV2ID)bu on bu.FACT_BOMV2ID = bom.FACT_BOMV2ID
WHEN MATCHED THEN UPDATE
	SET bom.Dim_BomUsageId = ifnull(bu.Dim_bomusageid,1) ; 

UPDATE    BOMV2_tmp_fact_bom_populate bom
SET bom.Dim_BomUsageId = ifnull(bu.Dim_bomusageid,1)
FROM BOMV2_PRST p,dim_bomusage bu,dim_bomcategory bc , BOMV2_tmp_fact_bom_populate bom
WHERE p.PRST_STLNR = bom.dd_BomNumber
AND p.PRST_STLAL = bom.dd_Alternative
AND bu.BOMUsageCode = p.PRST_STLAN AND bu.RowIsCurrent = 1
AND bc.dim_bomcategoryid = bom.dim_bomcategoryid
AND bc.Category = 'P'
AND bom.Dim_BomUsageId <> ifnull(bu.Dim_bomusageid,1);
/*End LI:BI-3558 */

  UPDATE BOMV2_tmp_fact_bom_populate
    SET std_exchangerate_dateid = ifnull(Dim_ChangedOnDateid,1)
        ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE std_exchangerate_dateid <> ifnull(Dim_ChangedOnDateid,1);
 
 alter table  BOMV2_tmp_fact_bom_populate drop column dd_plant cascade;

delete from fact_bomv2;
insert into fact_bomv2 
select * from BOMV2_tmp_fact_bom_populate;

/* Andrei 29 may 2017 */

update fact_bomv2 f
set unique_header = hash_md5(concat(b.category,f.dd_bomnumber,f.dd_alternative,dd_bomHeaderCounter))
from dim_bomcategory b , fact_bomv2 f
where 
f.dim_bomcategoryid = b.dim_bomcategoryid and
convert(varchar(256),unique_header) <> convert(varchar(256),hash_md5(concat(b.category,f.dd_bomnumber,f.dd_alternative,dd_bomHeaderCounter)));


/* Cristian Cleciu - App-8452 - modify Valid To Date logic */

DROP TABLE IF EXISTS bomv2_tmp_validto;
CREATE TABLE bomv2_tmp_validto AS 
  SELECT fact_bomv2id, 
         dpip.partnumber AS immediateparentpart, 
         dd_bomnumber, 
         m.mast_stlnr, 
         dd_bomitemno, 
         dd_level, 
         dd_alternative, 
         CASE 
           WHEN S.stas_lkenz = 'X' THEN S.stas_datuv 
           ELSE To_date('9999-12-31', 'YYYY-MM-DD') 
         END             AS validto, 
         'Not Set'       AS companycode, 
         'Not Set'       AS plantcode_factory 
  FROM   fact_bomv2 f 
         INNER JOIN dim_part dpip 
                 ON f.dim_partidimmediateparent = dpip.dim_partid 
         INNER JOIN dim_bomusage bu 
                 ON bu.dim_bomusageid = f.dim_bomusageid 
         INNER JOIN bomv2_mast m 
                 ON m.mast_matnr = dpip.partnumber 
                    AND dpip.plant = m.mast_werks 
                    AND bu.bomusagecode = m.mast_stlan 
         INNER JOIN dim_bomcategory bc 
                 ON f.dim_bomcategoryid = bc.dim_bomcategoryid 
         LEFT JOIN bomv2_stpo sp 
                ON sp.stpo_stlty = bc.category 
                   AND sp.stpo_stlnr = m.mast_stlnr 
                   AND sp.stpo_stlkn = f.dd_bomitemnodeno 
                   AND sp.stpo_stpoz = f.dd_bomitemcounter 
         LEFT JOIN (SELECT DISTINCT stas_stlnr, 
                                    stas_stlty, 
                                    stas_stlal, 
                                    stas_stlkn, 
                                    stas_lkenz, 
                                    stas_datuv 
                    FROM   bomv2_stas_fix 
                    WHERE  stas_lkenz = 'X') S 
                ON s.stas_stlnr = sp.stpo_stlnr 
                   AND s.stas_stlty = sp.stpo_stlty 
                   AND s.stas_stlkn = sp.stpo_stlkn 
                   AND s.stas_stlal = f.dd_alternative 
  ORDER  BY f.dd_bomnumber, 
            f.dd_level, 
            f.dd_alternative, 
            f.dd_bomitemno, 
            f.dd_bomitemnodeno; 

DROP TABLE IF EXISTS bomv2_tmp_maxvalidto;
CREATE TABLE bomv2_tmp_maxvalidto AS
SELECT   fact_bomv2id, 
         dd_bomnumber, 
         dd_bomitemno, 
         dd_level, 
         dd_alternative, 
         Max(validto) validto, 
         companycode, 
         plantcode_factory 
FROM     bomv2_tmp_validto f 
GROUP BY fact_bomv2id, 
         dd_bomnumber, 
         dd_bomitemno, 
         dd_level, 
         dd_alternative, 
         companycode, 
         plantcode_factory 
ORDER BY f.dd_bomnumber , 
         f.dd_level , 
         f.dd_alternative , 
         f.dd_bomitemno;
         
UPDATE fact_bomv2 f 
SET    f.dim_dateidvalidto2 = Ifnull(dt.dim_dateid,1), 
       dw_update_date = CURRENT_TIMESTAMP 
FROM   fact_bomv2 f, 
       bomv2_tmp_maxvalidto vt, 
       dim_date_factory_calendar dt 
WHERE  f.fact_bomv2id = vt.fact_bomv2id 
AND    dt.datevalue = vt.validto 
AND    dt.companycode = vt.companycode 
AND    dt.plantcode_factory = vt.plantcode_factory 
AND    f.dim_dateidvalidto2 != Ifnull(dt.dim_dateid,1);

/* END Cristian Cleciu - App-8452 - modify Valid To Date logic */



update fact_bomv2 b
set b.dd_activeItem =
        ifnull((case when d.datevalue <= current_date and p.deletionflag <> 'X' then 'Active'
                else 'Not Active'
        end),'Not Set')
from dim_part p, dim_date d, fact_bomv2 b
where  p.dim_partid = b.dim_bomcomponentid
           and b.dim_validfromdateid = d.dim_dateid
           and  b.dd_activeItem <>
		ifnull((case when d.datevalue <= current_date and p.deletionflag <> 'X' then 'Active'
				else 'Not Active'
		end),'Not Set');

merge into fact_bomv2 b using ( 
	select fact_bomv2id, ifnull(ds.dim_bomheaderid,1) dim_bomheaderid -- max(ifnull(ds.dim_bomheaderid,1)) dim_bomheaderid
	from dim_bomcategory c, dim_bomheader ds , fact_bomv2 b
	where c.category = ds.BOMCategory
		and c.dim_bomcategoryid = b.dim_bomcategoryid
		and b.dd_bomnumber = ds.BillOfMaterial
		and b.dd_alternative = ds.AlternativeBOM
		and b.dd_bomHeaderCounter = ds.Internalcounter
		and b.dim_bomheaderid <> ds.dim_bomheaderid
	--group by fact_bomv2id
	)ds on ds.fact_bomv2id = b.fact_bomv2id
when matched then update 
set b.dim_bomheaderid = ifnull(ds.dim_bomheaderid,1);											


UPDATE fact_bomv2 b
SET b.dim_dateidvalidfrom = ifnull(dt.dim_dateid,1)
from BOMV2_tmp_distinct_stko_stlst stko, dim_date_factory_calendar dt, dim_bomheader ds , fact_bomv2 b
WHERE b.dd_BomNumber = stko.stko_stlnr
AND b.dd_alternative_orig = stko.stko_stlal
and b.dd_bomHeaderCounter = stko.stko_stkoz
AND dt.datevalue= stko.STKO_DATUV
AND 'Not Set' = dt.CompanyCode
and dt.plantcode_factory = 'Not Set' /*ifnull(stko.STKO_WRKAN, 'Not Set')*/
and b.dim_bomheaderid = ds.dim_bomheaderid
and stko.STKO_STLTY = ds.BOMCategory
and stko.STKO_STLNR = ds.BillOfMaterial
and stko.STKO_STLAL = ds.AlternativeBOM
and stko.STKO_STKOZ = ds.InternalCounter
AND b.dim_dateidvalidfrom <> ifnull(dt.dim_dateid,1);											





DROP TABLE IF EXISTS BOMV2_tmp_FactBOMValidToDate;

CREATE TABLE BOMV2_tmp_FactBOMValidToDate
AS
SELECT   distinct dim_bomcategoryid, dd_BomNumber,dd_BomItemNo,dd_bomHeaderCounter, dt.dateValue as ValidFromDate,'1 Jan 9999' as ValidToDate
FROM fact_bomv2 inner join dim_date dt on dt.dim_dateid = dim_validfromdateid 
where dim_validfromdateid <> 1;

DROP TABLE IF EXISTS BOMV2_tmp_FactBOMValidToDate_1;

CREATE TABLE BOMV2_tmp_FactBOMValidToDate_1 AS
SELECT distinct b1.dim_bomcategoryid, b1.dd_BomNumber,b1.dd_BomItemNo,b1.dd_bomHeaderCounter ,b2.validfromdate, (dt.datevalue - INTERVAL '1' DAY) ValidToDate FROM 
BOMV2_tmp_FactBOMValidToDate b2, fact_bomv2 b1,dim_date dt
WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid 
and b1.dd_BomNumber = b2.dd_BomNumber 
and b1.dd_BomItemNo = b2.dd_BomItemNo
and b1.dd_bomHeaderCounter = b2.dd_bomHeaderCounter
AND dt.dim_dateid = b1.dim_validfromdateid
AND b2.validfromdate < dt.datevalue;

MERGE INTO fact_bomv2 b1 using ( 
	SELECT fact_bomv2id, max(ifnull(dt.dim_dateid,1)) dim_dateid
	FROM	 BOMV2_tmp_FactBOMValidToDate_1 b2, dim_date_factory_calendar dt,dim_Date dfrd,  fact_bomv2 b1
	WHERE b1.dim_bomcategoryid = b2.dim_bomcategoryid
		AND b1.dd_BomNumber = b2.dd_BomNumber
		AND b1.dd_BomItemNo = b2.dd_BomItemNo
		and b1.dd_bomHeaderCounter = b2.dd_bomHeaderCounter
		AND dfrd.dim_dateid = b1.dim_validfromdateid
		AND dfrd.datevalue = b2.validfromdate
		AND dfrd.companycode = 'Not Set'
		AND dt.datevalue = b2.ValidToDate
		AND dt.companycode = 'Not Set'
		AND dt.plantcode_factory = 'Not Set'
		AND b1.dim_dateidvalidto <> ifnull(dt.dim_dateid,1)
	GROUP BY fact_bomv2id)dt on dt.fact_bomv2id = b1.fact_bomv2id
when matched then update
SET b1.dim_dateidvalidto = ifnull(dt.dim_dateid,1);

UPDATE fact_bomv2 b1
SET b1.dim_dateidvalidto = ifnull(dt.Dim_DateId,1)
  FROM (select max(Dim_DateId) Dim_DateId from dim_date_factory_calendar dt
	where dt.DateValue = '9999-12-31'
  		AND dt.plantcode_factory = 'Not Set'
  		AND dt.CompanyCode = 'Not Set') dt, fact_bomv2 b1
WHERE (b1.dim_dateidvalidto = 1 OR b1.dim_dateidvalidto IS NULL);

  
  
DROP TABLE IF EXISTS BOMV2_tmp_FactBOMValidToDate2;
update fact_bomv2 b
set dd_activeHeader =
                ifnull((case when dt.datevalue <= current_date and p.deletionflag <> 'X' and dp.deletionflag <> 'X' then 'Active'
                        else 'Not Active'
                end), 'Not Set')
from dim_date dt, dim_part dp, dim_part p, fact_bomv2 b
where b.dim_dateidvalidfrom = dt.dim_dateid
        and dp.dim_partid = b.dim_rootpartid
        and  p.dim_partid = b.dim_bomcomponentid
        and dd_activeHeader <>
                ifnull((case when dt.datevalue <= current_date and p.deletionflag <> 'X' and dp.deletionflag <> 'X' then 'Active'
                        else 'Not Active'
                end), 'Not Set');
/* End 15 Jun 2016 */
/* Madalina 23 Jun 2016 - add BOM Header Counter and update Base Quantity - BI-3178 */
/* Liviu Ionescu - add this to the grain according to APP-8715 - update no longer needed */

/*merge into fact_bom b using 
( select fact_bomid, max(STKO_STKOZ) STKO_STKOZ
	from STKO s, dim_bomcategory c, fact_bom b,dim_bomstatus bs
	where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STKO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STKO_STLNR, 'Not Set')
        and b.dd_alternative = ifnull(STKO_STLAL, 'Not Set')
	and b.dd_bomHeaderCounter = ifnull(stko.stko_stkoz, 0)
                and b.Dim_bomstatusid = bs.Dim_bomstatusid
        AND bs.BOMStatusCode = ifnull(STKO_STLST, 0)
        and b.dd_bomHeaderCounter <> ifnull(STKO_STKOZ, 0)
	group by fact_bomid)s on s.fact_bomid = b.fact_bomid
when matched then update
set b.dd_bomHeaderCounter = ifnull(STKO_STKOZ, 0) */

update fact_bomv2 b
set b.dd_BaseQty = ifnull(s.STKO_BMENG, 0)
from BOMV2_STKO s, dim_bomcategory c, fact_bomv2 b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STKO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STKO_STLNR, 'Not Set')
        and b.dd_alternative = ifnull(STKO_STLAL, 'Not Set')
        and b.dd_bomHeaderCounter = ifnull(STKO_STKOZ, 0)
        and b.dd_BaseQty <> ifnull(s.STKO_BMENG, 0);

/* End 23 Jun 2016 */

/* Andrei 16 mar 2017 - Merck HH PRD - BOM subject area changes - BI-5741 */
/*
update fact_bom b
from STKO s, dim_bomcategory c
set b.dd_ChangeNumber = ifnull(STKO_AENNR, 'Not Set')
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STKO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STKO_STLNR, 'Not Set')
        and b.dd_alternative = ifnull(STKO_STLAL, 'Not Set')
		and b.dd_bomHeaderCounter = ifnull(STKO_STKOZ, 0)
        and b.dd_ChangeNumber <> ifnull(STKO_AENNR, 'Not Set')
	*/

  update fact_bomv2 b 
set b.dd_ChangeNumber = ifnull(STKO_AENNR, 'Not Set')
from BOMV2_STKO s, dim_bomcategory c, dim_bomstatus bs ,  fact_bomv2 b 
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STKO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STKO_STLNR, 'Not Set')
        and b.dd_alternative = ifnull(STKO_STLAL, 'Not Set')
		and b.dd_bomHeaderCounter = ifnull(STKO_STKOZ, 0)
		    AND bs.BOMStatusCode = ifnull(STKO_STLST, 0)
		    and b.Dim_bomstatusid = bs.Dim_bomstatusid
        and b.dd_ChangeNumber <> ifnull(STKO_AENNR, 'Not Set');
        
/*Cristian Cleciu - App-8452 - modify BOM Item Change number logic*/
DROP TABLE IF EXISTS bomv2_tmp_maxstasaennr;
CREATE TABLE bomv2_tmp_maxstasaennr AS 
SELECT   stas_stlnr, 
         stas_stlty, 
         stas_stlal, 
         stas_stlkn, 
         Max(stas_aennr) AS STAS_AENNR 
FROM     bomv2_stas_fix 
WHERE    stas_lkenz IS NULL
GROUP BY stas_stlnr, 
         stas_stlty, 
         stas_stlal, 
         stas_stlkn;
         
DROP TABLE IF EXISTS bomv2_tmp_upditemchangenumber;

CREATE table bomv2_tmp_upditemchangenumber AS 
SELECT DISTINCT fact_bomv2id,  
                dd_alternative,
                stpo_stlty,
                stpo_stlnr,
                stpo_stlkn
FROM            fact_bomv2 f 
INNER JOIN      dim_part dpip 
ON              f.dim_partidimmediateparent = dpip.dim_partid 
INNER JOIN      dim_bomusage bu 
ON              bu.dim_bomusageid = f.dim_bomusageid 
INNER JOIN      bomv2_mast m 
ON              m.mast_matnr = dpip.partnumber 
AND             dpip.plant = m.mast_werks 
AND             bu.bomusagecode = m.mast_stlan 
INNER JOIN      dim_bomcategory bc 
ON              f.dim_bomcategoryid = bc.dim_bomcategoryid 
INNER JOIN      bomv2_stpo sp 
ON              sp.stpo_stlty = bc.category 
AND             sp.stpo_stlnr = m.mast_stlnr 
AND             sp.stpo_stlkn = f.dd_bomitemnodeno 
AND             sp.stpo_stpoz = f.dd_bomitemcounter;

DROP TABLE IF EXISTS bomv2_tmp_upditemchangenumber2;

CREATE table bomv2_tmp_upditemchangenumber2 AS 
SELECT DISTINCT fact_bomv2id,     
			    Ifnull( s.stas_aennr, 'Not Set' ) AS dd_bomitemchangenumber 
FROM            bomv2_tmp_upditemchangenumber sp 
INNER JOIN      bomv2_tmp_maxstasaennr s 
ON              s.stas_stlnr = sp.stpo_stlnr 
AND             s.stas_stlty = sp.stpo_stlty 
AND             s.stas_stlkn = sp.stpo_stlkn 
AND             s.stas_stlal = sp.dd_alternative;

UPDATE fact_bomv2 b 
SET    b.dd_bomitemchangenumber = tmp.dd_bomitemchangenumber 
FROM   fact_bomv2 b, 
       bomv2_tmp_upditemchangenumber2 tmp 
WHERE  tmp.fact_bomv2id = b.fact_bomv2id 
AND    b.dd_bomitemchangenumber <> tmp.dd_bomitemchangenumber;

/*END Cristian Cleciu - App-8452 - modify BOM Item Change number logic*/


/*Cristian Cleciu - App-8452 - modify Valid From Date (Item) logic*/

DROP TABLE IF EXISTS bomv2_tmp_validfrom; 

CREATE TABLE bomv2_tmp_validfrom AS 
  SELECT DISTINCT fact_bomv2id, 
                  dpip.partnumber AS immediateparentpart, 
                  f.dd_bomnumber, 
                  m.mast_stlnr, 
                  f.dd_bomitemno, 
                  stas_stlkn,
                  f.dd_level, 
                  f.dd_alternative, 
                  CASE 
                    WHEN s.stas_lkenz IS NULL THEN 
                    Ifnull(s.stas_datuv, To_date('0001-01-01', 
                                         'YYYY-MM-DD')) 
                    ELSE To_date('1900-01-01', 'YYYY-MM-DD') 
                  END             AS validfrom, 
                  'Not Set'       AS companycode, 
                  'Not Set'       AS plantcode_factory 
  FROM   fact_bomv2 f 
         INNER JOIN dim_part dpip 
                 ON f.dim_partidimmediateparent = dpip.dim_partid 
         INNER JOIN dim_bomusage bu 
                 ON bu.dim_bomusageid = f.dim_bomusageid 
         INNER JOIN bomv2_mast m 
                 ON m.mast_matnr = dpip.partnumber 
                    AND dpip.plant = m.mast_werks 
                    AND bu.bomusagecode = m.mast_stlan 
         INNER JOIN dim_bomcategory bc 
                 ON f.dim_bomcategoryid = bc.dim_bomcategoryid 
         LEFT JOIN bomv2_stpo sp 
                ON sp.stpo_stlty = bc.category 
                   AND sp.stpo_stlnr = m.mast_stlnr 
                   AND sp.stpo_stlkn = f.dd_bomitemnodeno 
                   AND sp.stpo_stpoz = f.dd_bomitemcounter 
         LEFT JOIN(SELECT DISTINCT stas_stlnr, 
                                   stas_stlty, 
                                   stas_stlal, 
                                   stas_stlkn, 
                                   stas_lkenz, 
                                   stas_datuv 
                   FROM   bomv2_stas_fix 
                   WHERE  stas_lkenz IS NULL) s 
                ON s.stas_stlnr = sp.stpo_stlnr 
                   AND s.stas_stlty = sp.stpo_stlty 
                   AND s.stas_stlkn = sp.stpo_stlkn 
                   AND s.stas_stlal = f.dd_alternative 
  ORDER  BY f.dd_bomnumber, 
            f.dd_level, 
            f.dd_alternative, 
            f.dd_bomitemno; 

UPDATE fact_bomv2 f 
SET    f.dim_validfromdateid = Ifnull(dd.dim_dateid, 1) 
FROM   fact_bomv2 f, 
       dim_date_factory_calendar dd, 
       bomv2_tmp_validfrom tmp 
WHERE  f.fact_bomv2id = tmp.fact_bomv2id 
       AND tmp.validfrom = dd.datevalue 
       AND tmp.companycode = dd.companycode 
       AND tmp.plantcode_factory = dd.plantcode_factory 
       AND f.dim_validfromdateid <> Ifnull(dd.dim_dateid, 1); 

/*END Cristian Cleciu - App-8452 - modify Valid From Date (Item) logic*/

update fact_bomv2 b
set b.dd_BOMItemaAlternativeItemGroup = ifnull(STPO_ALPGR, 'Not Set')
from BOMV2_STPO p, dim_bomcategory c, fact_bomv2 b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STPO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STPO_STLNR, 'Not Set')
        and b.dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
		and b.dd_BomItemCounter = ifnull(STPO_STPOZ, 0)
        and b.dd_BOMItemaAlternativeItemGroup <> ifnull(STPO_ALPGR, 'Not Set');
		
update fact_bomv2 b
set b.dd_BOMItemaAlternativeItemRankingOrder = ifnull(STPO_ALPRF, 0)
from BOMV2_STPO p, dim_bomcategory c, fact_bomv2 b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STPO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STPO_STLNR, 'Not Set')
        and b.dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
		and b.dd_BomItemCounter = ifnull(STPO_STPOZ, 0)
        and b.dd_BOMItemaAlternativeItemRankingOrder <> ifnull(STPO_ALPRF, 0);
			
update fact_bomv2 b
set b.dd_BOMItemaAlternativeItemStrategy = ifnull(STPO_ALPST, 'Not Set')
from BOMV2_STPO p, dim_bomcategory c, fact_bomv2 b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STPO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STPO_STLNR, 'Not Set')
        and b.dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
		and b.dd_BomItemCounter = ifnull(STPO_STPOZ, 0)
        and b.dd_BOMItemaAlternativeItemStrategy <> ifnull(STPO_ALPST, 'Not Set');
				
update fact_bomv2 b
set b.dd_BOMCoProduct = ifnull(STPO_KZKUP, 'Not Set')
from BOMV2_STPO p, dim_bomcategory c, fact_bomv2 b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STPO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STPO_STLNR, 'Not Set')
        and b.dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
		and b.dd_BomItemCounter = ifnull(STPO_STPOZ, 0)
        and b.dd_BOMCoProduct <> ifnull(STPO_KZKUP, 'Not Set');
	
update fact_bomv2 b
set b.dd_BOMItemTextLine2 = ifnull(STPO_POTX2, 'Not Set')
from BOMV2_STPO p, dim_bomcategory c, fact_bomv2 b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STPO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STPO_STLNR, 'Not Set')
        and b.dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
		and b.dd_BomItemCounter = ifnull(STPO_STPOZ, 0)
        and b.dd_BOMItemTextLine2 <> ifnull(STPO_POTX2, 'Not Set');	
				
update fact_bomv2 b
set   b.dim_UomIdOpLeadTimeOffsetUnit = ifnull(d.dim_unitofmeasureid ,1)
from BOMV2_STPO p, dim_bomcategory c, dim_unitofmeasure d, fact_bomv2 b
where b.dim_bomcategoryid = c.dim_bomcategoryid
        and c.category = ifnull(STPO_STLTY, 'Not Set')
        and b.dd_bomnumber = ifnull(STPO_STLNR, 'Not Set')
        and b.dd_BOMItemNodeNo = ifnull(STPO_STLKN, 0)
		and b.dd_BomItemCounter = ifnull(STPO_STPOZ, 0)
		and   d.uom = ifnull(p.stpo_nlfmv,'Not Set') 
         and b.dim_UomIdOpLeadTimeOffsetUnit <> ifnull(d.dim_unitofmeasureid,1) ;
		
update fact_bomv2 b
set b.dd_FromLotSize = ifnull(MAST_LOSBS, 0)
from BOMV2_MAST m, dim_bomusage bu,dim_part pt, fact_bomv2 b
where 
   m.MAST_STLNR = b.dd_BomNumber	
AND m.MAST_STLAL = b.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN 
AND bu.dim_bomusageid = b.dim_bomusageid
AND bu.RowIsCurrent = 1
and pt.PartNumber = b.dd_RootPart
AND pt.Plant = m.MAST_WERKS 
and b.Dim_RootPartId = pt.Dim_PartId
        and b.dd_FromLotSize <> ifnull(MAST_LOSBS, 0);
		
update fact_bomv2 b
set b.dd_ToLotSize = ifnull(MAST_LOSVN, 0)
from BOMV2_MAST m, dim_bomusage bu,dim_part pt, fact_bomv2 b
where 
   m.MAST_STLNR = b.dd_BomNumber	
AND m.MAST_STLAL = b.dd_Alternative
AND bu.BOMUsageCode = m.MAST_STLAN 
AND bu.dim_bomusageid = b.dim_bomusageid
AND bu.RowIsCurrent = 1
and pt.PartNumber = b.dd_RootPart
AND pt.Plant = m.MAST_WERKS 
and b.Dim_RootPartId = pt.Dim_PartId
        and b.dd_ToLotSize <> ifnull(MAST_LOSVN, 0);
				
update fact_bomv2 b
set b.dim_dateidchangedon = ifnull(d.dim_dateid,1),
	b.dw_update_date = current_timestamp
from dim_plant pl , dim_bomcategory c, BOMV2_STKO SK, dim_date_factory_calendar d, fact_bomv2 b
where 
	 b.Dim_MaterialPlantId = pl.dim_plantid
	and  c.dim_bomcategoryid = b.dim_bomcategoryid
	and b.dd_BomNumber = SK.STKO_STLNR AND SK.STKO_STLAL = b.dd_alternative_orig AND b.dd_bomHeaderCounter = ifnull(STKO_STKOZ, 0)
	and c.category = ifnull(STKO_STLTY, 'Not Set')
	and d.datevalue = ifnull(STKO_AEDAT,'0001-01-01') and d.companycode = pl.companycode and d.plantcode_factory = pl.plantcode
	and b.dim_dateidchangedon <> ifnull(d.dim_dateid,1);

/* End 16 mar 2017 */

/* Update BW Hierarchy */

update fact_bomv2 f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_bomv2 f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

update fact_bomv2 f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_bomv2 f, dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1
and f.dim_bwproducthierarchyid <> bw.dim_bwproducthierarchyid;

/* MDG Part */
DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_BOMV2;
CREATE TABLE TMP_DIM_MDG_PARTID_BOMV2 as
SELECT pr.dim_partid, max(md.dim_mdg_partid)dim_mdg_partid
FROM dim_mdg_part md,
     dim_part pr
WHERE right('000000000000000000' || md.partnumber,18) = right('000000000000000000' || pr.partnumber,18)
GROUP BY dim_partid;


UPDATE fact_bomv2 f
SET f.dim_mdg_partid = ifnull(tmp.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM TMP_DIM_MDG_PARTID_BOMV2 tmp, fact_bomv2 f
WHERE f.dim_partid = tmp.dim_partid
      AND f.dim_mdg_partid <> ifnull(tmp.dim_mdg_partid, 1);

DROP TABLE IF EXISTS TMP_DIM_MDG_PARTID_BOMV2;

DROP TABLE IF EXISTS BOMV2_tmp_FactBOMValidToDate;

/*UPDATE fact_bom f
FROM fact_bom fp
SET f.dd_Alternative = fp.dd_Alternative
WHERE f.dd_rootpart = fp.dd_ComponentNumber
AND f.dd_level > 0
AND fp.dd_level = 0
AND f.dd_Alternative <> fp.dd_Alternative*/

/* These 2 procs are called after this */
/* CALL bi_populate_bom_level() */
/* CALL bi_process_bom_fact() */

DROP TABLE IF EXISTS BOMV2_tmp_pGlobalCurrency_bom;
DROP TABLE IF EXISTS BOMV2_TMP1_STKO;
DROP TABLE IF EXISTS BOMV2_TMP2_STKO;
DROP TABLE IF EXISTS BOMV2_TMP_DEL_fact_bom_tmp_populate_1;
DROP TABLE IF EXISTS BOMV2_TMP_DEL_fact_bom_tmp_populate_2;

