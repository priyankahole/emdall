/* Custom proc starts here */

/* Run this just before bi_process_bom_fact  */
/* ( for another proc, create a similar custom proc and run that before the actual proc ) */


      
/* Populate tmp_getStdPrice  This will change for each proc where this function is required*/

DROP TABLE IF EXISTS tmp_resb_for_shortagefact;
create table tmp_resb_for_shortagefact
as
select RESB_RSNUM,RESB_RSPOS,min(rb.RESB_BDTER) as min_RESB_BDTER
FROM RESB rb
		INNER JOIN fact_shortage_temp s ON rb.RESB_RSNUM = s.dd_ReservationNo 
						AND rb.RESB_RSPOS = s.dd_ReservationItemNo
WHERE s.dd_ReservationNo <> 0 
	AND s.dd_ReservationItemNo <> 0 
GROUP BY RESB_RSNUM,RESB_RSPOS;


INSERT INTO tmp_getStdPrice
(
 pCompanyCode,
 pPlant ,
 pMaterialNo  ,
 pFiYear,
 pPeriod,
 vUMREZ,
 vUMREN,
 PONumber,
 pUnitPrice,
 fact_script_name
)
SELECT DISTINCT
pl.CompanyCode,
pl.PlantCode,
r.RESB_MATNR,
dt.FinancialYear,
dt.FinancialMonthNumber, RESB_UMREZ, RESB_UMREN, NULL,
ifnull((CASE WHEN ifnull(RESB_PEINH, 0) = 0 THEN 1 ELSE RESB_PEINH END),0),
'bi_populate_shortage_fact'
FROM 
fact_shortage_temp s 
		INNER JOIN resb r ON s.dd_ReservationNo = r.RESB_RSNUM
       		                  AND s.dd_ReservationItemNo = r.RESB_RSPOS
		INNER JOIN dim_plant pl ON pl.PlantCode = r.RESB_WERKS
		LEFT JOIN tmp_resb_for_shortagefact r1 ON r1.RESB_RSNUM = r.RESB_RSNUM 
							AND  r1.RESB_RSPOS = r.RESB_RSPOS
		INNER JOIN dim_date dt ON dt.DateValue = r1.min_RESB_BDTER
					AND dt.CompanyCode = pl.CompanyCode
WHERE      s.dd_ReservationNo <> 0 
       AND s.dd_ReservationItemNo <> 0        
       AND pl.RowIsCurrent = 1;	   
       
UPDATE tmp_getStdPrice
SET flag_upd = 'N'
WHERE fact_script_name = 'bi_populate_shortage_fact' ;

DROP TABLE IF EXISTS tmp_resb_for_shortagefact;
