/*test*/

 
 /**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Ashu  */
/*   Created On     : Jul 2013 */
/*   Description    : Stored Proc bi_populate_mrp_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   01Jun2015	     Suchithra  2.0		 Fixes for Ambigous replace error */
/*   6 Sep 2013      Lokesh    1.2               Curr, Exchange rate changes 	*/
/*   6 Aug 2013      Lokesh    1.1               Moved leadtime logic to standard format				  */
/*                                               Order of scripts: mrp_fact_part1, vw_getLeadTime.bi_populate_mrp_fact.part1.sql */
/*                                              leadtime std, mrp_fact_part2,leadtime std, vw_getLeadTime.bi_populate_mrp_fact.part2,  mrp_fact_part3    */
/*   Jul   2013      Ashu      1.0               Existing code migrated to Vectorwise.				  */
/******************************************************************************************************************/

/* Note that this is no more present in mysql. So, for validation, refresh this table from vw prod */

Drop table if exists tmpvariable_00e ;

Create table tmpvariable_00e(
pGlobalCurrency varchar(3) null,
tmpTotalCount integer null);

Insert into tmpvariable_00e values(null,null);

Update tmpvariable_00e
SET tmpTotalCount = ifnull((select count(*) from mdkp k inner join mdtb t on k.MDKP_DTNUM = t.MDTB_DTNUM),0);

Update tmpvariable_00e
SET pGlobalCurrency = ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');

DROP TABLE IF EXISTS TMP_MRP_Q1_UPD;
CREATE TABLE TMP_MRP_Q1_UPD
as
SELECT DISTINCT mrp.fact_mrpid
FROM fact_mrp mrp,tmpvariable_00e, dim_mrpexception mex, dim_mrpelement me, dim_date dn
  WHERE     tmpTotalCount > 0
        AND Dim_DateidActionClosed = 1
        AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
        AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
        AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
        AND mrp.dd_documentno <> 'Not Set'
        AND (ct_Completed <> 1 OR Dim_ActionStateid <> 3)
        AND NOT EXISTS
                    (SELECT 1
                      FROM mdtb t, mdkp m
                      WHERE     mrp.dd_documentno = t.MDTB_DELNR
                            AND mrp.dd_documentitemno = t.MDTB_DELPS
                            AND mrp.dd_scheduleno = t.MDTB_DELET
                            AND t.MDTB_DELNR IS NOT NULL
                            AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                            AND m.MDKP_DTNUM = t.MDTB_DTNUM
                            AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                            AND mrp.ct_QtyMRP = t.MDTB_MNG01
                            AND t.MDTB_MNG03 = 0
                            AND t.MDTB_RDMNG = 0
                            AND to_date(dn.DateValue, 'MM-DD-YYYY') =
                                  to_date(ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00),
                                          ('0001-01-01')), 'MM-DD-YYYY')
                            AND mrp.Dim_ActionStateid = 2
                            AND mrp.dd_PlannScenarioLTP = m.MDKP_PLSCN);




/* Q1 - Upd ct_Completed */
UPDATE fact_mrp mrp
SET     ct_Completed = 1
From    TMP_MRP_Q1_UPD t, fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND mrp.ct_Completed <> 1;


/* Q1 - For the same rows where ct_Completed was updated above, update Dim_ActionStateid to 3 */
UPDATE fact_mrp mrp
SET     Dim_ActionStateid = 3
From    TMP_MRP_Q1_UPD t, fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND Dim_ActionStateid <> 3;



UPDATE fact_mrp mrp
SET Dim_DateidActionClosed = dt.Dim_Dateid
FROM TMP_MRP_Q1_UPD t,dim_date dt, dim_company cc, fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND dt.DateValue = current_date
AND cc.Dim_Companyid = mrp.Dim_Companyid
AND cc.CompanyCode = dt.CompanyCode
AND Dim_DateidActionClosed <> dt.Dim_Dateid;





DROP TABLE IF EXISTS TMP_MRP_Q2_UPD;
CREATE TABLE TMP_MRP_Q2_UPD
as
SELECT DISTINCT mrp.fact_mrpid
FROM fact_mrp mrp,tmpvariable_00e, dim_mrpexception mex, dim_mrpelement me, dim_date dn,dim_part dp
WHERE  tmpTotalCount > 0
AND Dim_DateidActionClosed = 1
AND mrp.Dim_Partid = dp.Dim_Partid
AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
AND mrp.dd_documentno = 'Not Set'
AND  ct_Completed <> 1
AND NOT EXISTS
                    (SELECT 1
                      FROM mdtb t, mdkp m
                      WHERE dp.PartNumber = m.MDKP_MATNR
                            AND dp.Plant = m.MDKP_PLWRK
                            and t.MDTB_DELNR is null
                            AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                            AND m.MDKP_DTNUM = t.MDTB_DTNUM
                            AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                            AND mrp.ct_QtyMRP = t.MDTB_MNG01
                            AND t.MDTB_MNG03 = 0
                            AND t.MDTB_RDMNG = 0
                            AND to_date(dn.DateValue, 'MM-DD-YYYY')=
                                  to_date(ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00),
                                          ('0001-01-01')),'MM-DD-YYYY' )
                            AND mrp.Dim_ActionStateid = 2
                            AND mrp.dd_PlannScenarioLTP = m.MDKP_PLSCN);


/* Q2 - Upd ct_Completed */
UPDATE fact_mrp mrp
SET     ct_Completed = 1
From    TMP_MRP_Q2_UPD t, fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND mrp.ct_Completed <> 1;


/* Q1 - For the same rows where ct_Completed was updated above, update Dim_ActionStateid to 3 */
UPDATE fact_mrp mrp
SET     Dim_ActionStateid = 3
From    TMP_MRP_Q2_UPD t, fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND Dim_ActionStateid <> 3;



UPDATE fact_mrp mrp
SET Dim_DateidActionClosed = dt.Dim_Dateid
FROM TMP_MRP_Q2_UPD t,dim_date dt, dim_company cc, fact_mrp mrp
WHERE mrp.fact_mrpid = t.fact_mrpid
AND dt.DateValue = current_date
AND cc.Dim_Companyid = mrp.Dim_Companyid
AND cc.CompanyCode = dt.CompanyCode
AND Dim_DateidActionClosed <> dt.Dim_Dateid;




DELETE FROM fact_mrp
WHERE EXISTS
            (SELECT 1
              FROM dim_date d
              WHERE d.Dim_Dateid = Dim_DateidMRP
                    AND d.DateValue < (current_date - ( INTERVAL '6' MONTH)))
        AND ct_Completed = 1
	AND ( ifnull((select count(*) from mdkp k inner join mdtb t on k.MDKP_DTNUM = t.MDTB_DTNUM),0)) > 0;



Drop table if exists max_holder_00e;
Create table max_holder_00e
as
Select ifnull(max(fact_mrpid),0) maxid
from fact_mrp;


drop table if exists fact_mrp_00e;
Create table fact_mrp_00e as 
Select Dim_Partid,
	dim_mrpexceptionID1,
	Dim_MRPElementid,
	ct_QtyMRP,
	dd_documentno,
	dd_documentitemno,
	dd_scheduleno,
	Dim_DateidDateNeeded,
	dd_PlannScenarioLTP
From fact_mrp
Where dd_documentno <> 'Not Set'
AND Dim_ActionStateid = 2;

Drop table if exists fact_mrp_ne_01;
Drop table if exists fact_mrp_ne_02;



Drop table if exists mdtb_r00;
Create table mdtb_r00 
as 
Select m.*, ifnull(MDTB_UMDAT, MDTB_DAT00) calvalue01 
from mdtb m
where ifnull(MDTB_UMDAT, MDTB_DAT00) IS NOT NULL 
AND MDTB_MNG03 = 0 
AND MDTB_RDMNG = 0 
and MDTB_DELNR is not null;



drop table if exists fact_mrp_ne_01;
Create table fact_mrp_ne_01 
as
SELECT  ifnull(maxh.maxid,0) + row_number() over(order by '') fact_mrpid,
            Dim_MRPElementid,
            dim_mrpexceptionID dim_mrpexceptionID1,
            CONVERT (BIGINT, 1) dim_mrpexceptionID2,
            Dim_Companyid,
            c.Dim_Currencyid,
	    c.Dim_Currencyid Dim_Currencyid_TRA, /*No separate tran currency */
	    CONVERT(BIGINT, 1) Dim_Currencyid_GBL,
	    CONVERT(decimal (18,5), 1) amt_ExchangeRate ,			
		   CONVERT(decimal (18,5), 1 )amt_ExchangeRate_GBL,
            ar.dim_dateid Dim_DateidActionRequired,
            CONVERT (BIGINT, 1) Dim_DateidActionClosed,
            dn.dim_dateid Dim_DateidDateNeeded,
	    CONVERT (BIGINT, 1) Dim_DateidOriginalDock,
            mrpd.Dim_Dateid Dim_DateidMRP,
            Dim_Partid,
            CONVERT (BIGINT, 1) Dim_StorageLocationid,
            Dim_Plantid,
            Dim_UnitOfMeasureid,
            CONVERT (BIGINT, 1) Dim_PurchaseGroupid,
            Dim_PurchaseOrgid,
            CONVERT (BIGINT, 1) Dim_Vendorid,
            CONVERT (BIGINT, 1) Dim_ActionStateid,
            CONVERT (BIGINT, 1) Dim_ItemCategoryid,
            CONVERT (BIGINT, 1) Dim_DocumentTypeid,
            Dim_MRPProcedureid,
            CONVERT (BIGINT, 1) Dim_MRPDiscontinuationIndicatorid,
            CONVERT (BIGINT, 1) Dim_periodindicatorid,
            CONVERT (BIGINT, 1) Dim_specialprocurementid,
            CONVERT (BIGINT, 1) Dim_FixedVendorid,
            CONVERT (BIGINT, 1) Dim_ConsumptionTypeid,
            t.MDTB_MNG01 ct_QtyMRP,
            t.MDTB_MNG02 ct_QtyScrap,
            ifnull(t.MDTB_WEBAZ, 0) ct_DaysGRPProcessing,
            m.MDKP_BERW2 ct_DaysReceiptCoverage,
            m.MDKP_BERW1 ct_DaysStockCoverage,
            ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
            t.mdtb_delps dd_DocumentItemNo,
            t.mdtb_delet dd_ScheduleNo,
            t.mdtb_dtnum dd_mrptablenumber,
            t.mdtb_baugr dd_peggedrequirement,
            t.mdtb_sernr dd_bomexplosionno,
            ifnull(t.mdtb_aufvr, 'Not Set') dd_SourceOrderNo,
            ifnull(t.mdtb_del12, 'Not Set') dd_MRPElementNo,
            CONVERT (BIGINT, 1) dim_mrplisttypeid,
            ifnull(MDKP_PLSCN,'Not Set') dd_PlannScenarioLTP,
            ifnull(MDTB_BAART,'Not Set') dd_POOrderType,
            ifnull(MDTB_BESKZ,'Not Set') dd_ProcurementType,
            ifnull(MDTB_PLART,'Not Set') dd_PlanningType,
	CONVERT (BIGINT, 1) dim_DateidMRPDock,
	CONVERT (BIGINT, 1) Dim_DateidReschedule,
    t.MDTB_OLDSL MDTB_OLDSL_sub,
	t.MDTB_DAT01 MDTB_DAT01_sub,
	dc.CompanyCode CompanyCode_sub,
	t.MDTB_LGORT MDTB_LGORT_sub,
	pl.plantcode plantcode_sub,
	m.MDKP_EKGRP MDKP_EKGRP_sub,
	m.mdkp_kzaus mdkp_kzaus_sub,
	t.MDTB_PERKZ MDTB_PERKZ_sub,
	t.MDTB_SOBES MDTB_SOBES_sub,
	m.MDKP_DTART MDKP_DTART_sub,
	MDTB_DAT00 MDTB_DAT00_sub,
	MDTB_WEBAZ MDTB_WEBAZ_sub,
	t.MDTB_UMDAT MDTB_UMDAT_sub
      FROM max_holder_00e maxh,tmpvariable_00e tmpv,
	        mdtb_r00 t,
		mdkp m,
		dim_mrpelement me,
		dim_mrpexception mex,
		dim_part dp, 
		dim_plant pl, 
		dim_unitofmeasure uom,
		Dim_PurchaseOrg po,
		dim_company dc,
		dim_currency c,
		dim_date mrpd,
		dim_date dn,
		dim_date ar,
		dim_mrpprocedure pr
      WHERE   tmpv.tmpTotalCount > 0 
              AND t.MDTB_DTNUM = m.MDKP_DTNUM
              AND     t.MDTB_DELKZ = me.MRPElement
                  AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
              AND ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
              AND     m.MDKP_PLWRK = dp.Plant
                  AND m.MDKP_MATNR = dp.PartNumber
              AND m.MDKP_PLWRK = pl.PlantCode
              AND uom.UOM = m.MDKP_MEINS
              AND pl.PurchOrg = po.PurchaseOrgCode
              AND pl.CompanyCode = dc.CompanyCode
              AND c.currencycode = dc.currency
              AND mrpd.DateValue = m.MDKP_dsdat
                  AND mrpd.CompanyCode = dc.CompanyCode
              AND     dn.DateValue = t.calvalue01
                  AND dn.CompanyCode = dc.CompanyCode
              AND ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode
              AND m.MDKP_DISVF = pr.MRPProcedure;

	      
update fact_mrp_ne_01 f
set Dim_Currencyid_GBL = t.Dim_Currencyid
from fact_mrp_ne_01 f 
		CROSS JOIN (SELECT c.Dim_Currencyid 
					FROM dim_currency c, tmpvariable_00e tc
                    WHERE c.CurrencyCode = tc.pGlobalCurrency ) t
where Dim_Currencyid_GBL <> t.Dim_Currencyid;

update fact_mrp_ne_01 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_mrp_ne_01 f
      INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmpvariable_00e tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_mrp_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);     

Drop table if exists mdtb_r00;


Update fact_mrp_ne_01 f
Set dim_mrpexceptionID2 = ifnull(dim_mrpexceptionid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_mrpexception ex ON f.MDTB_OLDSL_sub = ex.exceptionkey
WHERE dim_mrpexceptionID2 <> ifnull(dim_mrpexceptionid,1);



Update fact_mrp_ne_01 f
Set f.Dim_DateidOriginalDock = ifnull(od.dim_dateid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_DAT01_sub 
		                      AND od.CompanyCode = f.CompanyCode_sub
WHERE Dim_DateidOriginalDock <> ifnull(od.dim_dateid,1);

Update fact_mrp_ne_01 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = f.MDTB_LGORT_sub 
						 AND sl.Plant=f.plantcode_sub
WHERE f.Dim_StorageLocationid <> ifnull( sl.Dim_StorageLocationid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_PurchaseGroupid = ifnull( pg.Dim_PurchaseGroupid, 1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_purchasegroup pg ON pg.PurchaseGroup = f.MDKP_EKGRP_sub
WHERE f.Dim_PurchaseGroupid <> ifnull( pg.Dim_PurchaseGroupid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_MRPDiscontinuationIndicatorid = ifnull( di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_mrpdiscontinuationindicator di ON f.mdkp_kzaus_sub = di."indicator"
WHERE f.Dim_MRPDiscontinuationIndicatorid <> ifnull( di.Dim_MRPDiscontinuationIndicatorid,1);

Update fact_mrp_ne_01 f
Set f.Dim_periodindicatorid = ifnull( pid.Dim_periodindicatorid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_periodindicator pid ON f.MDTB_PERKZ_sub = pid.periodindicator
WHERE f.Dim_periodindicatorid <> ifnull( pid.Dim_periodindicatorid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_specialprocurementid =  ifnull( sp.Dim_specialprocurementid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_specialprocurement sp ON f.MDTB_SOBES_sub = sp.specialprocurement
WHERE f.Dim_specialprocurementid <> ifnull( sp.Dim_specialprocurementid,1);

Update fact_mrp_ne_01 f
Set f.dim_mrplisttypeid = ifnull( mlt.Dim_MRPListTypeid, 1)
from  fact_mrp_ne_01 f
		LEFT JOIN Dim_MRPListType mlt ON mlt.Type = f.MDKP_DTART_sub 
WHERE  f.dim_mrplisttypeid <> ifnull( mlt.Dim_MRPListTypeid, 1);


Update fact_mrp_ne_01 f
Set dim_DateidMRPDock =  ifnull( mdt.Dim_Dateid,1)
from  fact_mrp_ne_01 f
		LEFT JOIN  dim_date mdt ON mdt.CompanyCode = f.CompanyCode_sub
WHERE mdt.DateValue = (CASE when f.MDTB_UMDAT_sub is null then f.MDTB_DAT00_sub 
		   	                ELSE (f.MDTB_UMDAT_sub - (interval '1' day) * f.MDTB_WEBAZ_sub ) 
					    END) 
AND dim_DateidMRPDock <> ifnull( mdt.Dim_Dateid,1);



Update fact_mrp_ne_01 f
Set f.Dim_DateidReschedule = ifnull(od.dim_dateid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_UMDAT_sub
		                      AND od.CompanyCode = f.CompanyCode_sub
WHERE f.Dim_DateidReschedule <> ifnull(od.dim_dateid,1);



Create table fact_mrp_ne_02 as Select * from fact_mrp_ne_01 where 1=2;

INSERT INTO fact_mrp_ne_02(fact_mrpid,
                      Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      dd_MRPElementNo,
                      dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
			dd_POOrderType,
                      dd_ProcurementType,
                      dd_PlanningType,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule,
			 MDTB_OLDSL_sub,
                        MDTB_DAT01_sub,
                        CompanyCode_sub,
                        MDTB_LGORT_sub,
                        plantcode_sub,
                        MDKP_EKGRP_sub,
                        mdkp_kzaus_sub,
                        MDTB_PERKZ_sub,
                        MDTB_SOBES_sub,
                        MDKP_DTART_sub,
                        MDTB_DAT00_sub,
                        MDTB_WEBAZ_sub,
                        MDTB_UMDAT_sub)
Select                ne01.fact_mrpid,
                      ne01.Dim_MRPElementid,
                      ne01.dim_mrpexceptionID1,
                      ne01.dim_mrpexceptionID2,
                      ne01.Dim_Companyid,
                      ne01.Dim_Currencyid,
					  ne01.Dim_Currencyid_TRA,
					  ne01.Dim_Currencyid_GBL,
					  ne01.amt_ExchangeRate,
					  ne01.amt_ExchangeRate_GBL,					  
                      ne01.Dim_DateidActionRequired,
                      ne01.Dim_DateidActionClosed,
                      ne01.Dim_DateidDateNeeded,
                      ne01.Dim_DateidOriginalDock,
                      ne01.Dim_DateidMRP,
                      ne01.Dim_Partid,
                      ne01.Dim_StorageLocationid,
                      ne01.Dim_Plantid,
                      ne01.Dim_UnitOfMeasureid,
                      ne01.Dim_PurchaseGroupid,
                      ne01.Dim_PurchaseOrgid,
                      ne01.Dim_Vendorid,
                      ne01.Dim_ActionStateid,
                      ne01.Dim_ItemCategoryid,
                      ne01.Dim_DocumentTypeid,
                      ne01.Dim_MRPProcedureid,
                      ne01.Dim_MRPDiscontinuationIndicatorid,
                      ne01.Dim_periodindicatorid,
                      ne01.Dim_specialprocurementid,
                      ne01.Dim_FixedVendorid,
                      ne01.Dim_ConsumptionTypeid,
                      ne01.ct_QtyMRP,
                      ne01.ct_QtyScrap,
                      ne01.ct_DaysGRPProcessing,
                      ne01.ct_DaysReceiptCoverage,
                      ne01.ct_DaysStockCoverage,
                      ne01.dd_DocumentNo,
                      ne01.dd_DocumentItemNo,
                      ne01.dd_ScheduleNo,
                      ne01.dd_mrptablenumber,
                      ne01.dd_peggedrequirement,
                      ne01.dd_bomexplosionno,
                      ne01.dd_SourceOrderNo,
                      ne01.dd_MRPElementNo,
                      ne01.dim_mrplisttypeid,
                      ne01.dd_PlannScenarioLTP,
                      ne01.dd_POOrderType,
		      ne01.dd_ProcurementType,
                      ne01.dd_PlanningType,
                      ne01.dim_DateidMRPDock,
                      ne01.Dim_DateidReschedule,
			 ne01.MDTB_OLDSL_sub,
                        ne01.MDTB_DAT01_sub,
                        ne01.CompanyCode_sub,
                        ne01.MDTB_LGORT_sub,
                        ne01.plantcode_sub,
                        ne01.MDKP_EKGRP_sub,
                        ne01.mdkp_kzaus_sub,
                        ne01.MDTB_PERKZ_sub,
                        ne01.MDTB_SOBES_sub,
                        ne01.MDKP_DTART_sub,
                        ne01.MDTB_DAT00_sub,
                        ne01.MDTB_WEBAZ_sub,
                        ne01.MDTB_UMDAT_sub
From fact_mrp_ne_01 ne01,
     fact_mrp_00e mrp
WHERE mrp.Dim_Partid = ne01.Dim_Partid
AND   mrp.dim_mrpexceptionID1 = ne01.dim_mrpexceptionID1
AND  mrp.Dim_MRPElementid = ne01.Dim_MRPElementid
AND mrp.ct_QtyMRP = ne01.ct_QtyMRP
AND mrp.dd_documentno = ne01.dd_documentno 
AND mrp.dd_documentitemno = ne01.dd_documentitemno
AND mrp.dd_scheduleno = ne01.dd_scheduleno
AND mrp.Dim_DateidDateNeeded = ne01.Dim_DateidDateNeeded
AND mrp.dd_PlannScenarioLTP = ne01.dd_PlannScenarioLTP;

merge into fact_mrp_ne_01 t1
using fact_mrp_ne_02 t2
on t1.fact_mrpid = t2.fact_mrpid
when matched then delete;

Insert into fact_mrp(fact_mrpid,
                      Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,							  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      dd_MRPElementNo,
                      dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dd_POOrderType,
		      dd_ProcurementType,
                      dd_PlanningType,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule)
 SELECT  ifnull(max_holder_00e.maxid,1) + row_number() over(order by '') fact_mrpid,
		      Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,							  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
		      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      dd_MRPElementNo,
                      dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dd_POOrderType,
                      dd_ProcurementType,
                      dd_PlanningType,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule
From fact_mrp_ne_01,max_holder_00e;


drop table if exists fact_mrp_00e;
drop table if exists fact_mrp_ne_01;
drop table if exists fact_mrp_ne_02;



Drop table if exists max_holder_00e;
Create table max_holder_00e
as
Select ifnull(max(fact_mrpid),0) maxid
from fact_mrp;

drop table if exists fact_mrp_00e;
Create table fact_mrp_00e as
Select Dim_Partid,
	dim_mrpexceptionID1,
	Dim_MRPElementid,
	ct_QtyMRP,
	Dim_DateidDateNeeded,
	dd_PlannScenarioLTP
From fact_mrp
Where dd_documentno ='Not Set'
AND Dim_ActionStateid = 2;


drop table if exists mdtb_r01;

Create table mdtb_r01 as Select * from mdtb where ifnull(MDTB_UMDAT, MDTB_DAT00) IS NOT NULL AND MDTB_MNG03 = 0 AND MDTB_RDMNG = 0 and MDTB_DELNR is null;


drop table if exists fact_mrp_ne_01;
Create table fact_mrp_ne_01 
AS    SELECT ifnull(max_holder_00e.maxid,0) + row_number() over(order by '') fact_mrpid,
            Dim_MRPElementid,
            dim_mrpexceptionID dim_mrpexceptionID1,
            CONVERT (BIGINT, 1) dim_mrpexceptionID2,
            Dim_Companyid,
            c.Dim_Currencyid,
	    c.Dim_Currencyid Dim_Currencyid_TRA, /*No separate tran currency */
	    CONVERT (BIGINT, 1) Dim_Currencyid_GBL,
	    CONVERT(DECIMAL (18,4) , 1) amt_ExchangeRate,			
	    CONVERT(decimal (18,4), 1) amt_ExchangeRate_GBL,			
            ar.dim_dateid Dim_DateidActionRequired,
            CONVERT (BIGINT, 1) Dim_DateidActionClosed,
            dn.dim_dateid Dim_DateidDateNeeded,
            CONVERT (BIGINT, 1) Dim_DateidOriginalDock,
            mrpd.Dim_Dateid Dim_DateidMRP,
            Dim_Partid,
            CONVERT (BIGINT, 1) Dim_StorageLocationid,
            Dim_Plantid,
            Dim_UnitOfMeasureid,
            CONVERT (BIGINT, 1) Dim_PurchaseGroupid,
            Dim_PurchaseOrgid,
            CONVERT (BIGINT, 1) Dim_Vendorid,
            CONVERT (BIGINT, 1) Dim_ActionStateid,
            CONVERT (BIGINT, 1) Dim_ItemCategoryid,
            CONVERT (BIGINT, 1) Dim_DocumentTypeid,
            Dim_MRPProcedureid,
            CONVERT (BIGINT, 1) Dim_MRPDiscontinuationIndicatorid,
            CONVERT (BIGINT, 1) Dim_periodindicatorid,
            CONVERT (BIGINT, 1) Dim_specialprocurementid,
            CONVERT (BIGINT, 1) Dim_FixedVendorid,
            CONVERT (BIGINT, 1) Dim_ConsumptionTypeid,
            t.MDTB_MNG01 ct_QtyMRP,
            t.MDTB_MNG02 ct_QtyScrap,
            ifnull(t.MDTB_WEBAZ, 0) ct_DaysGRPProcessing,
            m.MDKP_BERW2 ct_DaysReceiptCoverage,
            m.MDKP_BERW1 ct_DaysStockCoverage,
            ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
            t.mdtb_delps dd_DocumentItemNo,
            t.mdtb_delet dd_ScheduleNo,
            t.mdtb_dtnum dd_mrptablenumber,
            t.mdtb_baugr dd_peggedrequirement,
            t.mdtb_sernr dd_bomexplosionno,
            ifnull(t.mdtb_aufvr, 'Not Set') dd_SourceOrderNo,
            ifnull(t.mdtb_del12, 'Not Set') dd_MRPElementNo,
            CONVERT (BIGINT, 1) dim_mrplisttypeid,
            MDKP_PLSCN dd_PlannScenarioLTP,
            CONVERT (BIGINT, 1) dim_DateidMRPDock,
            CONVERT (BIGINT, 1) Dim_DateidReschedule,
	    t.MDTB_OLDSL MDTB_OLDSL_sub,
        t.MDTB_DAT01 MDTB_DAT01_sub,
        dc.CompanyCode CompanyCode_sub,
        t.MDTB_LGORT MDTB_LGORT_sub,
        pl.plantcode plantcode_sub,
        m.MDKP_EKGRP MDKP_EKGRP_sub,
        m.mdkp_kzaus mdkp_kzaus_sub,
        t.MDTB_PERKZ MDTB_PERKZ_sub,
        t.MDTB_SOBES MDTB_SOBES_sub,
        m.MDKP_DTART MDKP_DTART_sub,
        MDTB_DAT00 MDTB_DAT00_sub,
        MDTB_WEBAZ MDTB_WEBAZ_sub,
        t.MDTB_UMDAT MDTB_UMDAT_sub
      FROM tmpvariable_00e,max_holder_00e,mdtb_r01 t
            INNER JOIN mdkp m
              ON t.MDTB_DTNUM = m.MDKP_DTNUM
            INNER JOIN dim_mrpelement me
              ON     t.MDTB_DELKZ = me.MRPElement
                  AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
            INNER JOIN dim_mrpexception mex
              ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
            INNER JOIN dim_part dp
              ON     m.MDKP_PLWRK = dp.Plant
                  AND m.MDKP_MATNR = dp.PartNumber
            INNER JOIN dim_plant pl
              ON m.MDKP_PLWRK = pl.PlantCode
            INNER JOIN dim_unitofmeasure uom
              ON uom.UOM = m.MDKP_MEINS
            INNER JOIN Dim_PurchaseOrg po
              ON pl.PurchOrg = po.PurchaseOrgCode
            INNER JOIN dim_company dc
              ON pl.CompanyCode = dc.CompanyCode
            INNER JOIN dim_currency c
              ON c.currencycode = dc.currency
            INNER JOIN dim_date mrpd
              ON mrpd.DateValue = m.MDKP_dsdat
                  AND mrpd.CompanyCode = dc.CompanyCode
            INNER JOIN dim_date dn
              ON     dn.DateValue = ifnull(t.MDTB_UMDAT, t.MDTB_DAT00)
                  AND dn.CompanyCode = dc.CompanyCode
            INNER JOIN dim_date ar
              ON ar.DateValue = current_date 
              AND ar.CompanyCode = dc.CompanyCode
            INNER JOIN dim_mrpprocedure pr
              ON m.MDKP_DISVF = pr.MRPProcedure
      WHERE tmpTotalCount > 0 ;


update fact_mrp_ne_01 f
set Dim_Currencyid_GBL = t.Dim_Currencyid
from fact_mrp_ne_01 f 
		CROSS JOIN (SELECT c.Dim_Currencyid 
					FROM dim_currency c, tmpvariable_00e tc
                    WHERE c.CurrencyCode = tc.pGlobalCurrency ) t
where Dim_Currencyid_GBL <> t.Dim_Currencyid;

update fact_mrp_ne_01 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_mrp_ne_01 f
      INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmpvariable_00e tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_mrp_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);   


drop table if exists mdtb_r01;


Update fact_mrp_ne_01 f
Set dim_mrpexceptionID2 = ifnull(dim_mrpexceptionid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_mrpexception ex ON f.MDTB_OLDSL_sub = ex.exceptionkey
WHERE dim_mrpexceptionID2 <> ifnull(dim_mrpexceptionid,1);



Update fact_mrp_ne_01 f
Set f.Dim_DateidOriginalDock = ifnull(od.dim_dateid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_DAT01_sub 
		                      AND od.CompanyCode = f.CompanyCode_sub
WHERE Dim_DateidOriginalDock <> ifnull(od.dim_dateid,1);

Update fact_mrp_ne_01 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = f.MDTB_LGORT_sub 
						 AND sl.Plant=f.plantcode_sub
WHERE f.Dim_StorageLocationid <> ifnull( sl.Dim_StorageLocationid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_PurchaseGroupid = ifnull( pg.Dim_PurchaseGroupid, 1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_purchasegroup pg ON pg.PurchaseGroup = f.MDKP_EKGRP_sub
WHERE f.Dim_PurchaseGroupid <> ifnull( pg.Dim_PurchaseGroupid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_MRPDiscontinuationIndicatorid = ifnull( di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_mrpdiscontinuationindicator di ON f.mdkp_kzaus_sub = di."indicator"
WHERE f.Dim_MRPDiscontinuationIndicatorid <> ifnull( di.Dim_MRPDiscontinuationIndicatorid,1);

Update fact_mrp_ne_01 f
Set f.Dim_periodindicatorid = ifnull( pid.Dim_periodindicatorid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_periodindicator pid ON f.MDTB_PERKZ_sub = pid.periodindicator
WHERE f.Dim_periodindicatorid <> ifnull( pid.Dim_periodindicatorid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_specialprocurementid =  ifnull( sp.Dim_specialprocurementid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_specialprocurement sp ON f.MDTB_SOBES_sub = sp.specialprocurement
WHERE f.Dim_specialprocurementid <> ifnull( sp.Dim_specialprocurementid,1);

Update fact_mrp_ne_01 f
Set f.dim_mrplisttypeid = ifnull( mlt.Dim_MRPListTypeid, 1)
from  fact_mrp_ne_01 f
		LEFT JOIN Dim_MRPListType mlt ON mlt.Type = f.MDKP_DTART_sub 
WHERE  f.dim_mrplisttypeid <> ifnull( mlt.Dim_MRPListTypeid, 1);


Update fact_mrp_ne_01 f
Set dim_DateidMRPDock =  ifnull( mdt.Dim_Dateid,1)
from  fact_mrp_ne_01 f
		LEFT JOIN  dim_date mdt ON mdt.CompanyCode = f.CompanyCode_sub
WHERE mdt.DateValue = (CASE when f.MDTB_UMDAT_sub is null then f.MDTB_DAT00_sub 
		   	                ELSE (f.MDTB_UMDAT_sub - (interval '1' day) * f.MDTB_WEBAZ_sub ) 
					    END) 
AND dim_DateidMRPDock <> ifnull( mdt.Dim_Dateid,1);



Update fact_mrp_ne_01 f
Set f.Dim_DateidReschedule = ifnull(od.dim_dateid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_UMDAT_sub
		                      AND od.CompanyCode = f.CompanyCode_sub
WHERE f.Dim_DateidReschedule <> ifnull(od.dim_dateid,1);



drop table if exists fact_mrp_ne_02;

Create table fact_mrp_ne_02 as Select * from fact_mrp_ne_01 where 1=2;
INSERT INTO fact_mrp_ne_02(fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
                      Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
                      dim_mrplisttypeid,
			dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule,
			 MDTB_OLDSL_sub,
                        MDTB_DAT01_sub,
                        CompanyCode_sub,
                        MDTB_LGORT_sub,
                        plantcode_sub,
                        MDKP_EKGRP_sub,
                        mdkp_kzaus_sub,
                        MDTB_PERKZ_sub,
                        MDTB_SOBES_sub,
                        MDKP_DTART_sub,
                        MDTB_DAT00_sub,
                        MDTB_WEBAZ_sub,
                        MDTB_UMDAT_sub)
Select ne01.fact_mrpid,
                        ne01.Dim_MRPElementid,
                      ne01.dim_mrpexceptionID1,
                      ne01.dim_mrpexceptionID2,
                      ne01.Dim_Companyid,
                      ne01.Dim_Currencyid,
					  ne01.Dim_Currencyid_TRA,
					  ne01.Dim_Currencyid_GBL,
					  ne01.amt_ExchangeRate,
					  ne01.amt_ExchangeRate_GBL,					  
                     ne01.Dim_DateidActionRequired,
                      ne01.Dim_DateidActionClosed,
                      ne01.Dim_DateidDateNeeded,
                      ne01.Dim_DateidOriginalDock,
                      ne01.Dim_DateidMRP,
                      ne01.Dim_Partid,
                      ne01.Dim_StorageLocationid,
                      ne01.Dim_Plantid,
                      ne01.Dim_UnitOfMeasureid,
                      ne01.Dim_PurchaseGroupid,
                      ne01.Dim_PurchaseOrgid,
                      ne01.Dim_Vendorid,
                      ne01.Dim_ActionStateid,
                      ne01.Dim_ItemCategoryid,
                      ne01.Dim_DocumentTypeid,
                      ne01.Dim_MRPProcedureid,
                      ne01.Dim_MRPDiscontinuationIndicatorid,
                      ne01.Dim_periodindicatorid,
                      ne01.Dim_specialprocurementid,
                      ne01.Dim_FixedVendorid,
                      ne01.Dim_ConsumptionTypeid,
                      ne01.ct_QtyMRP,
                      ne01.ct_QtyScrap,
                      ne01.ct_DaysGRPProcessing,
                      ne01.ct_DaysReceiptCoverage,
                      ne01.ct_DaysStockCoverage,
                      ne01.dd_DocumentNo,
                      ne01.dd_DocumentItemNo,
                      ne01.dd_ScheduleNo,
                      ne01.dd_mrptablenumber,
                      ne01.dd_peggedrequirement,
                      ne01.dd_bomexplosionno,
                        ne01.dd_SourceOrderNo,
			ne01.dd_MRPElementNo,
                      ne01.dim_mrplisttypeid,
		 ne01.dd_PlannScenarioLTP,
                      ne01.dim_DateidMRPDock,
                      ne01.Dim_DateidReschedule,
		 ne01.MDTB_OLDSL_sub,
                       ne01.MDTB_DAT01_sub,
                        ne01.CompanyCode_sub,
                        ne01.MDTB_LGORT_sub,
                        ne01.plantcode_sub,
                        ne01.MDKP_EKGRP_sub,
                        ne01.mdkp_kzaus_sub,
                        ne01.MDTB_PERKZ_sub,
                        ne01.MDTB_SOBES_sub,
                        ne01.MDKP_DTART_sub,
                        ne01.MDTB_DAT00_sub,
                        ne01.MDTB_WEBAZ_sub,
                        ne01.MDTB_UMDAT_sub
From fact_mrp_ne_01 ne01,
     fact_mrp_00e mrp
Where  mrp.Dim_Partid = ne01.Dim_Partid
      AND mrp.dim_mrpexceptionID1 = ne01.dim_mrpexceptionID1
      AND mrp.Dim_MRPElementid = ne01.Dim_MRPElementid
      AND mrp.ct_QtyMRP = ne01.ct_QtyMRP
      AND mrp.Dim_DateidDateNeeded  = ne01.Dim_DateidDateNeeded
      AND mrp.dd_PlannScenarioLTP  = ne01.dd_PlannScenarioLTP;


merge into fact_mrp_ne_01 t1
using (select distinct t.fact_mrpid
         from fact_mrp_ne_01 t
	 INNER JOIN fact_mrp_ne_02 t2 ON  t.fact_mrpid = t2.fact_mrpid ) src
on t1.fact_mrpid = src.fact_mrpid
when matched then delete;



INSERT INTO fact_mrp(fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
			dim_mrplisttypeid,
                        dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule)
Select  ifnull(max_holder_00e.maxid,0) + row_number() over(order by ''),
	                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,						  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
			dim_mrplisttypeid,
                        dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule
From fact_mrp_ne_01,max_holder_00e;




drop table if exists fact_mrp_00e;
drop table if exists fact_mrp_ne_01;
drop table if exists fact_mrp_ne_02;

Drop table if exists max_holder_00e;
Create table max_holder_00e
as
Select ifnull(max(fact_mrpid),0) maxid
from fact_mrp;


drop table if exists fact_mrp_00e;
Create table fact_mrp_00e as
Select Dim_Partid,
dim_mrpexceptionID1,
Dim_MRPElementid,
ct_QtyMRP,
dd_documentno,
dd_documentitemno,
dd_scheduleno,dd_PlannScenarioLTP
From fact_mrp
Where dd_documentno <> 'Not Set'
AND Dim_DateidDateNeeded = 1
AND Dim_ActionStateid = 2;



Drop table if exists mdtb_r02 ;

Create table mdtb_r02 as Select * from mdtb where MDTB_UMDAT IS NULL AND MDTB_DAT00 IS NULL AND MDTB_MNG03 = 0 AND MDTB_RDMNG = 0 AND MDTB_DELNR is not null;

drop table if exists fact_mrp_ne_01;
Create table fact_mrp_ne_01
AS    SELECT ifnull(max_holder_00e.maxid,1) + row_number() over(order by '') fact_mrpid,
		Dim_MRPElementid,
            dim_mrpexceptionID dim_mrpexceptionID1,
            CONVERT (BIGINT, 1) dim_mrpexceptionID2,
            Dim_Companyid,
            c.Dim_Currencyid,
	    c.Dim_Currencyid Dim_Currencyid_TRA, /*No separate tran currency */
	    CONVERT (BIGINT, 1) Dim_Currencyid_GBL,
	    CONVERT (decimal (18,5), 1) amt_ExchangeRate,
	    CONVERT (decimal (18,5), 1) amt_ExchangeRate_GBL,			
            ar.dim_dateid Dim_DateidActionRequired,
            CONVERT (BIGINT, 1) Dim_DateidActionClosed,
            CONVERT (BIGINT, 1) Dim_DateidDateNeeded,
            CONVERT (BIGINT, 1) Dim_DateidOriginalDock,
            mrpd.Dim_Dateid Dim_DateidMRP,
            Dim_Partid,
            CONVERT (BIGINT, 1) Dim_StorageLocationid,
            Dim_Plantid,
            Dim_UnitOfMeasureid,
            CONVERT (BIGINT, 1) Dim_PurchaseGroupid,
            Dim_PurchaseOrgid,
            CONVERT (BIGINT, 1) Dim_Vendorid,
            CONVERT (BIGINT, 1) Dim_ActionStateid,
            CONVERT (BIGINT, 1) Dim_ItemCategoryid,
            CONVERT (BIGINT, 1) Dim_DocumentTypeid,
            Dim_MRPProcedureid,
            CONVERT (BIGINT, 1) Dim_MRPDiscontinuationIndicatorid,
            CONVERT (BIGINT, 1) Dim_periodindicatorid,
            CONVERT (BIGINT, 1) Dim_specialprocurementid,
            CONVERT (BIGINT, 1) Dim_FixedVendorid,
            CONVERT (BIGINT, 1) Dim_ConsumptionTypeid,
            t.MDTB_MNG01 ct_QtyMRP,
            t.MDTB_MNG02 ct_QtyScrap,
            ifnull(t.MDTB_WEBAZ, 0) ct_DaysGRPProcessing,
            m.MDKP_BERW2 ct_DaysReceiptCoverage,
            m.MDKP_BERW1 ct_DaysStockCoverage,
            ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
            t.mdtb_delps dd_DocumentItemNo,
            t.mdtb_delet dd_ScheduleNo,
            t.mdtb_dtnum dd_mrptablenumber,
            t.mdtb_baugr dd_peggedrequirement,
            t.mdtb_sernr dd_bomexplosionno,
            ifnull(t.mdtb_aufvr, 'Not Set') dd_SourceOrderNo,
            ifnull(t.mdtb_del12, 'Not Set') dd_MRPElementNo,
            CONVERT (BIGINT, 1) dim_mrplisttypeid,
            MDKP_PLSCN dd_PlannScenarioLTP,
            CONVERT (BIGINT, 1)dim_DateidMRPDock,
            CONVERT (BIGINT, 1) Dim_DateidReschedule,
	    t.MDTB_OLDSL MDTB_OLDSL_sub,
        t.MDTB_DAT01 MDTB_DAT01_sub,
        dc.CompanyCode CompanyCode_sub,
        t.MDTB_LGORT MDTB_LGORT_sub,
        pl.plantcode plantcode_sub,
        m.MDKP_EKGRP MDKP_EKGRP_sub,
        m.mdkp_kzaus mdkp_kzaus_sub,
        t.MDTB_PERKZ MDTB_PERKZ_sub,
        t.MDTB_SOBES MDTB_SOBES_sub,
        m.MDKP_DTART MDKP_DTART_sub,
        MDTB_DAT00 MDTB_DAT00_sub,
        MDTB_WEBAZ MDTB_WEBAZ_sub,
        t.MDTB_UMDAT MDTB_UMDAT_sub
      FROM tmpvariable_00e,max_holder_00e,mdtb_r02 t
            INNER JOIN mdkp m
              ON t.MDTB_DTNUM = m.MDKP_DTNUM
            INNER JOIN dim_mrpelement me
              ON     t.MDTB_DELKZ = me.MRPElement
                  AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
            INNER JOIN dim_mrpexception mex
              ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
            INNER JOIN dim_part dp
              ON     m.MDKP_PLWRK = dp.Plant
                  AND m.MDKP_MATNR = dp.PartNumber
            INNER JOIN dim_unitofmeasure uom
              ON uom.UOM = m.MDKP_MEINS
            INNER JOIN dim_plant pl
              ON m.MDKP_PLWRK = pl.PlantCode
            INNER JOIN Dim_PurchaseOrg po
              ON pl.PurchOrg = po.PurchaseOrgCode
            INNER JOIN dim_company dc
              ON pl.CompanyCode = dc.CompanyCode
            INNER JOIN Dim_Currency c
              ON c.currencycode = dc.currency
            INNER JOIN dim_date mrpd
              ON mrpd.DateValue = m.MDKP_dsdat
                  AND mrpd.CompanyCode = dc.CompanyCode
            INNER JOIN dim_date ar
              ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode
            INNER JOIN dim_mrpprocedure pr
              ON m.MDKP_DISVF = pr.MRPProcedure
      WHERE   tmpTotalCount > 0 ;
      

update fact_mrp_ne_01 f
set Dim_Currencyid_GBL = t.Dim_Currencyid
from fact_mrp_ne_01 f 
		CROSS JOIN (SELECT c.Dim_Currencyid 
					FROM dim_currency c, tmpvariable_00e tc
                    WHERE c.CurrencyCode = tc.pGlobalCurrency ) t
where Dim_Currencyid_GBL <> t.Dim_Currencyid;

update fact_mrp_ne_01 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_mrp_ne_01 f
      INNER JOIN dim_company dc ON f.dim_companyid = dc.dim_companyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmpvariable_00e tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_mrp_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.Currency
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);     

Drop table if exists mdtb_r02 ;

Update fact_mrp_ne_01 f
Set dim_mrpexceptionID2 = ifnull(dim_mrpexceptionid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_mrpexception ex ON f.MDTB_OLDSL_sub = ex.exceptionkey
WHERE dim_mrpexceptionID2 <> ifnull(dim_mrpexceptionid,1);



Update fact_mrp_ne_01 f
Set f.Dim_DateidOriginalDock = ifnull(od.dim_dateid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_DAT01_sub 
		                      AND od.CompanyCode = f.CompanyCode_sub
WHERE Dim_DateidOriginalDock <> ifnull(od.dim_dateid,1);

Update fact_mrp_ne_01 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = f.MDTB_LGORT_sub 
						 AND sl.Plant=f.plantcode_sub
WHERE f.Dim_StorageLocationid <> ifnull( sl.Dim_StorageLocationid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_PurchaseGroupid = ifnull( pg.Dim_PurchaseGroupid, 1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_purchasegroup pg ON pg.PurchaseGroup = f.MDKP_EKGRP_sub
WHERE f.Dim_PurchaseGroupid <> ifnull( pg.Dim_PurchaseGroupid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_MRPDiscontinuationIndicatorid = ifnull( di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_mrpdiscontinuationindicator di ON f.mdkp_kzaus_sub = di."indicator"
WHERE f.Dim_MRPDiscontinuationIndicatorid <> ifnull( di.Dim_MRPDiscontinuationIndicatorid,1);

Update fact_mrp_ne_01 f
Set f.Dim_periodindicatorid = ifnull( pid.Dim_periodindicatorid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_periodindicator pid ON f.MDTB_PERKZ_sub = pid.periodindicator
WHERE f.Dim_periodindicatorid <> ifnull( pid.Dim_periodindicatorid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_specialprocurementid =  ifnull( sp.Dim_specialprocurementid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_specialprocurement sp ON f.MDTB_SOBES_sub = sp.specialprocurement
WHERE f.Dim_specialprocurementid <> ifnull( sp.Dim_specialprocurementid,1);

Update fact_mrp_ne_01 f
Set f.dim_mrplisttypeid = ifnull( mlt.Dim_MRPListTypeid, 1)
from  fact_mrp_ne_01 f
		LEFT JOIN Dim_MRPListType mlt ON mlt.Type = f.MDKP_DTART_sub 
WHERE  f.dim_mrplisttypeid <> ifnull( mlt.Dim_MRPListTypeid, 1);


Update fact_mrp_ne_01 f
Set dim_DateidMRPDock =  ifnull( mdt.Dim_Dateid,1)
from  fact_mrp_ne_01 f
		LEFT JOIN  dim_date mdt ON mdt.CompanyCode = f.CompanyCode_sub
WHERE mdt.DateValue = (CASE when f.MDTB_UMDAT_sub is null then f.MDTB_DAT00_sub 
		   	                ELSE (f.MDTB_UMDAT_sub - (interval '1' day) * f.MDTB_WEBAZ_sub ) 
					    END) 
AND dim_DateidMRPDock <> ifnull( mdt.Dim_Dateid,1);



Update fact_mrp_ne_01 f
Set f.Dim_DateidReschedule = ifnull(od.dim_dateid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_UMDAT_sub
		                      AND od.CompanyCode = f.CompanyCode_sub
WHERE f.Dim_DateidReschedule <> ifnull(od.dim_dateid,1);


Create table fact_mrp_ne_02 as Select * from fact_mrp_ne_01 where 1=2;

INSERT INTO fact_mrp_ne_02(
			fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      dd_MRPElementNo,
			 dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule,
			 MDTB_OLDSL_sub,
                        MDTB_DAT01_sub,
                        CompanyCode_sub,
                        MDTB_LGORT_sub,
                        plantcode_sub,
                        MDKP_EKGRP_sub,
                        mdkp_kzaus_sub,
                        MDTB_PERKZ_sub,
                        MDTB_SOBES_sub,
                        MDKP_DTART_sub,
                        MDTB_DAT00_sub,
                        MDTB_WEBAZ_sub,
                        MDTB_UMDAT_sub)
Select ne01.fact_mrpid,
                        ne01.Dim_MRPElementid,
                      ne01.dim_mrpexceptionID1,
                      ne01.dim_mrpexceptionID2,
                      ne01.Dim_Companyid,
                      ne01.Dim_Currencyid,
					  ne01.Dim_Currencyid_TRA,
					  ne01.Dim_Currencyid_GBL,
					  ne01.amt_ExchangeRate,
					  ne01.amt_ExchangeRate_GBL,					  
                      ne01.Dim_DateidActionRequired,
                      ne01.Dim_DateidActionClosed,
                      ne01.Dim_DateidDateNeeded,
                      ne01.Dim_DateidOriginalDock,
                      ne01.Dim_DateidMRP,
                      ne01.Dim_Partid,
                      ne01.Dim_StorageLocationid,
                      ne01.Dim_Plantid,
                      ne01.Dim_UnitOfMeasureid,
                      ne01.Dim_PurchaseGroupid,
                      ne01.Dim_PurchaseOrgid,
                      ne01.Dim_Vendorid,
                      ne01.Dim_ActionStateid,
                      ne01.Dim_ItemCategoryid,
                      ne01.Dim_DocumentTypeid,
                      ne01.Dim_MRPProcedureid,
                      ne01.Dim_MRPDiscontinuationIndicatorid,
                      ne01.Dim_periodindicatorid,
                      ne01.Dim_specialprocurementid,
                      ne01.Dim_FixedVendorid,
                      ne01.Dim_ConsumptionTypeid,
                      ne01.ct_QtyMRP,
                      ne01.ct_QtyScrap,
                      ne01.ct_DaysGRPProcessing,
                      ne01.ct_DaysReceiptCoverage,
                      ne01.ct_DaysStockCoverage,
                      ne01.dd_DocumentNo,
                      ne01.dd_DocumentItemNo,
                      ne01.dd_ScheduleNo,
                      ne01.dd_mrptablenumber,
                      ne01.dd_peggedrequirement,
                      ne01.dd_bomexplosionno,
                      ne01.dd_SourceOrderNo,
                      ne01.dd_MRPElementNo,
			 ne01.dim_mrplisttypeid,
                      ne01.dd_PlannScenarioLTP,
                      ne01.dim_DateidMRPDock,
                      ne01.Dim_DateidReschedule,
			 ne01.MDTB_OLDSL_sub,
                        ne01.MDTB_DAT01_sub,
                       ne01.CompanyCode_sub,
                        ne01.MDTB_LGORT_sub,
                        ne01.plantcode_sub,
                        ne01.MDKP_EKGRP_sub,
                        ne01.mdkp_kzaus_sub,
                        ne01.MDTB_PERKZ_sub,
                        ne01.MDTB_SOBES_sub,
                        ne01.MDKP_DTART_sub,
                        ne01.MDTB_DAT00_sub,
                        ne01.MDTB_WEBAZ_sub,
                        ne01.MDTB_UMDAT_sub
From fact_mrp_ne_01 ne01,
     fact_mrp_00e mrp
Where  mrp.Dim_Partid = ne01.Dim_Partid
      AND mrp.dim_mrpexceptionID1 =ne01.dim_mrpexceptionID1
      AND mrp.Dim_MRPElementid = ne01.Dim_MRPElementid 
      AND mrp.ct_QtyMRP = ne01.ct_QtyMRP
      AND mrp.dd_documentno = ne01.dd_documentno
      AND mrp.dd_documentitemno = ne01.dd_documentitemno
      AND mrp.dd_scheduleno = ne01.dd_scheduleno
      AND mrp.dd_PlannScenarioLTP = ne01.dd_PlannScenarioLTP; 




/* Unstable set of rows*/

merge into fact_mrp_ne_01 t1
using (select distinct t.fact_mrpid
         from fact_mrp_ne_01 t
	 INNER JOIN fact_mrp_ne_02 t2 ON  t.fact_mrpid = t2.fact_mrpid ) src
on t1.fact_mrpid = src.fact_mrpid
when matched then delete;


Insert into fact_mrp(fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      dd_MRPElementNo,
			 dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule)
Select ifnull(max_holder_00e.maxid,0) + row_number() over(order by ''),
	            Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                      dd_SourceOrderNo,
                      dd_MRPElementNo,
			 dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule
From fact_mrp_ne_01,max_holder_00e;

drop table if exists fact_mrp_00e;
drop table if exists fact_mrp_ne_01;
drop table if exists fact_mrp_ne_02;
Drop table if exists max_holder_00e;
Create table max_holder_00e
as
Select ifnull(max(fact_mrpid),0) maxid
from fact_mrp;


drop table if exists fact_mrp_00e;
Create table fact_mrp_00e as
Select Dim_Partid,
dim_mrpexceptionID1,
Dim_MRPElementid,
ct_QtyMRP,
dd_PlannScenarioLTP
From fact_mrp
Where dd_documentno = 'Not Set'
      AND Dim_DateidDateNeeded = 1
      AND Dim_ActionStateid = 2;


Drop table if exists mdtb_r03;

Create table mdtb_r03 as Select * from mdtb where MDTB_UMDAT IS NULL AND MDTB_DAT00 IS NULL AND MDTB_MNG03 = 0 AND MDTB_RDMNG = 0 AND MDTB_DELNR is null;

Create table fact_mrp_ne_01
AS SELECT ifnull(max_holder_00e.maxid,0) + row_number() over(order by '') fact_mrpid,
		Dim_MRPElementid,
            dim_mrpexceptionID dim_mrpexceptionID1,
            CONVERT (BIGINT, 1) dim_mrpexceptionID2,
            Dim_Companyid,
            c.Dim_Currencyid,
	    c.Dim_Currencyid Dim_Currencyid_TRA, /*No separate tran currency */
	    CONVERT(BIGINT, 1) Dim_Currencyid_GBL,
	    CONVERT(decimal (18,5), 1) amt_ExchangeRate,
	    CONVERT(decimal (18,5), 1 ) amt_ExchangeRate_GBL,			
            ar.dim_dateid Dim_DateidActionRequired,
            CONVERT (BIGINT, 1)  Dim_DateidActionClosed,
            CONVERT (BIGINT, 1)  Dim_DateidDateNeeded,
            CONVERT (BIGINT, 1)  Dim_DateidOriginalDock,
            mrpd.Dim_Dateid Dim_DateidMRP,
            Dim_Partid,
            CONVERT (BIGINT, 1)  Dim_StorageLocationid,
            Dim_Plantid,
            Dim_UnitOfMeasureid,
            CONVERT (BIGINT, 1)  Dim_PurchaseGroupid,
            Dim_PurchaseOrgid,
            CONVERT (BIGINT, 1)  Dim_Vendorid,
            CONVERT (BIGINT, 1)  Dim_ActionStateid,
            CONVERT (BIGINT, 1)  Dim_ItemCategoryid,
            CONVERT (BIGINT, 1)  Dim_DocumentTypeid,
            Dim_MRPProcedureid,
             CONVERT (BIGINT, 1)  Dim_MRPDiscontinuationIndicatorid,
             CONVERT (BIGINT, 1)  Dim_periodindicatorid,
             CONVERT (BIGINT, 1)  Dim_specialprocurementid,
            CONVERT (BIGINT, 1)  Dim_FixedVendorid,
            CONVERT (BIGINT, 1)  Dim_ConsumptionTypeid,
            t.MDTB_MNG01 ct_QtyMRP,
            t.MDTB_MNG02 ct_QtyScrap,
            ifnull(t.MDTB_WEBAZ, 0) ct_DaysGRPProcessing,
            m.MDKP_BERW2 ct_DaysReceiptCoverage,
            m.MDKP_BERW1 ct_DaysStockCoverage,
            ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
            t.mdtb_delps dd_DocumentItemNo,
            t.mdtb_delet dd_ScheduleNo,
            t.mdtb_dtnum dd_mrptablenumber,
            t.mdtb_baugr dd_peggedrequirement,
            t.mdtb_sernr dd_bomexplosionno,
            ifnull(t.mdtb_aufvr, 'Not Set') dd_SourceOrderNo,
            ifnull(t.mdtb_del12, 'Not Set') dd_MRPElementNo,
            CONVERT (BIGINT, 1)  dim_mrplisttypeid,
            MDKP_PLSCN dd_PlannScenarioLTP,
            CONVERT (BIGINT, 1) dim_DateidMRPDock,
            CONVERT (BIGINT, 1) Dim_DateidReschedule,
	 t.MDTB_OLDSL MDTB_OLDSL_sub,
        t.MDTB_DAT01 MDTB_DAT01_sub,
        dc.CompanyCode CompanyCode_sub,
        t.MDTB_LGORT MDTB_LGORT_sub,
        pl.plantcode plantcode_sub,
        m.MDKP_EKGRP MDKP_EKGRP_sub,
        m.mdkp_kzaus mdkp_kzaus_sub,
        t.MDTB_PERKZ MDTB_PERKZ_sub,
        t.MDTB_SOBES MDTB_SOBES_sub,
        m.MDKP_DTART MDKP_DTART_sub,
        MDTB_DAT00 MDTB_DAT00_sub,
        MDTB_WEBAZ MDTB_WEBAZ_sub,
        t.MDTB_UMDAT MDTB_UMDAT_sub
      FROM tmpvariable_00e,max_holder_00e,mdtb_r03 t
            INNER JOIN mdkp m
              ON t.MDTB_DTNUM = m.MDKP_DTNUM
            INNER JOIN dim_mrpelement me
              ON     t.MDTB_DELKZ = me.MRPElement
                  AND ((m.MDKP_DTART = 'MD' AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI')) OR m.MDKP_DTART <> 'MD')
            INNER JOIN dim_mrpexception mex
              ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
            INNER JOIN dim_part dp
              ON     m.MDKP_PLWRK = dp.Plant
                  AND m.MDKP_MATNR = dp.PartNumber
            INNER JOIN dim_unitofmeasure uom
              ON uom.UOM = m.MDKP_MEINS
            INNER JOIN dim_plant pl
              ON m.MDKP_PLWRK = pl.PlantCode
            INNER JOIN Dim_PurchaseOrg po
              ON pl.PurchOrg = po.PurchaseOrgCode
            INNER JOIN dim_company dc
              ON pl.CompanyCode = dc.CompanyCode
            INNER JOIN Dim_Currency c
              ON c.currencycode = dc.currency
            INNER JOIN dim_date mrpd
              ON mrpd.DateValue = m.MDKP_dsdat
                  AND mrpd.CompanyCode = dc.CompanyCode
            INNER JOIN dim_date ar
              ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode
            INNER JOIN dim_mrpprocedure pr
              ON m.MDKP_DISVF = pr.MRPProcedure
      WHERE     tmpTotalCount > 0 ;

update fact_mrp_ne_01 f
set Dim_Currencyid_GBL = t.Dim_Currencyid
from fact_mrp_ne_01 f 
		CROSS JOIN (SELECT c.Dim_Currencyid 
					FROM dim_currency c, tmpvariable_00e tc
                    WHERE c.CurrencyCode = tc.pGlobalCurrency ) t
where Dim_Currencyid_GBL <> t.Dim_Currencyid;

update fact_mrp_ne_01 f
set amt_ExchangeRate_GBL = ifnull(x.exchangeRate , 1)
from fact_mrp_ne_01 f
      INNER JOIN dim_currency dc ON f.dim_currencyid = dc.dim_currencyid
	  LEFT JOIN ( SELECT distinct z.pFromCurrency, z.exchangeRate
	              FROM  tmp_getExchangeRate1 z 
		      INNER JOIN tmpvariable_00e tc ON z.pToCurrency = tc.pGlobalCurrency 	
				  WHERE     z.fact_script_name = 'bi_populate_mrp_fact' 
				  		and z.pDate = current_date
						and z.pFromExchangeRate = 0 ) x
				  ON x.pFromCurrency  = dc.CurrencyCode
WHERE amt_ExchangeRate_GBL <> ifnull(x.exchangeRate , 1);   

Drop table if exists mdtb_r03;

Update fact_mrp_ne_01 f
Set dim_mrpexceptionID2 = ifnull(dim_mrpexceptionid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_mrpexception ex ON f.MDTB_OLDSL_sub = ex.exceptionkey
WHERE dim_mrpexceptionID2 <> ifnull(dim_mrpexceptionid,1);

Update fact_mrp_ne_01 f
Set f.Dim_DateidOriginalDock = ifnull(od.dim_dateid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_DAT01_sub 
		                      AND od.CompanyCode = f.CompanyCode_sub
WHERE Dim_DateidOriginalDock <> ifnull(od.dim_dateid,1);


Update fact_mrp_ne_01 f
Set f.Dim_StorageLocationid= ifnull( sl.Dim_StorageLocationid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_storagelocation sl ON sl.LocationCode = f.MDTB_LGORT_sub 
						 AND sl.Plant=f.plantcode_sub
WHERE f.Dim_StorageLocationid <> ifnull( sl.Dim_StorageLocationid, 1);
Update fact_mrp_ne_01 f
Set f.Dim_PurchaseGroupid = ifnull( pg.Dim_PurchaseGroupid, 1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_purchasegroup pg ON pg.PurchaseGroup = f.MDKP_EKGRP_sub
WHERE f.Dim_PurchaseGroupid <> ifnull( pg.Dim_PurchaseGroupid, 1);



Update fact_mrp_ne_01 f
Set f.Dim_MRPDiscontinuationIndicatorid = ifnull( di.Dim_MRPDiscontinuationIndicatorid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_mrpdiscontinuationindicator di ON f.mdkp_kzaus_sub = di."indicator"
WHERE f.Dim_MRPDiscontinuationIndicatorid <> ifnull( di.Dim_MRPDiscontinuationIndicatorid,1);

Update fact_mrp_ne_01 f
Set f.Dim_periodindicatorid = ifnull( pid.Dim_periodindicatorid, 1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_periodindicator pid ON f.MDTB_PERKZ_sub = pid.periodindicator
WHERE f.Dim_periodindicatorid <> ifnull( pid.Dim_periodindicatorid, 1);

Update fact_mrp_ne_01 f
Set f.Dim_specialprocurementid =  ifnull( sp.Dim_specialprocurementid,1)
FROM  fact_mrp_ne_01 f
		LEFT JOIN dim_specialprocurement sp ON f.MDTB_SOBES_sub = sp.specialprocurement
WHERE f.Dim_specialprocurementid <> ifnull( sp.Dim_specialprocurementid,1);

Update fact_mrp_ne_01 f
Set f.dim_mrplisttypeid = ifnull( mlt.Dim_MRPListTypeid, 1)
from  fact_mrp_ne_01 f
		LEFT JOIN Dim_MRPListType mlt ON mlt.Type = f.MDKP_DTART_sub 
WHERE  f.dim_mrplisttypeid <> ifnull( mlt.Dim_MRPListTypeid, 1);

Update fact_mrp_ne_01 f
Set dim_DateidMRPDock =  ifnull( mdt.Dim_Dateid,1)
from  fact_mrp_ne_01 f
		LEFT JOIN  dim_date mdt ON mdt.CompanyCode = f.CompanyCode_sub
WHERE mdt.DateValue = (CASE when f.MDTB_UMDAT_sub is null then f.MDTB_DAT00_sub 
		   	                ELSE (f.MDTB_UMDAT_sub - (interval '1' day) * f.MDTB_WEBAZ_sub ) 
					    END) 
AND dim_DateidMRPDock <> ifnull( mdt.Dim_Dateid,1);


Update fact_mrp_ne_01 f
Set f.Dim_DateidReschedule = ifnull(od.dim_dateid,1)
FROM fact_mrp_ne_01 f
		LEFT JOIN dim_date od ON od.DateValue = f.MDTB_UMDAT_sub
		                      AND od.CompanyCode = f.CompanyCode_sub
WHERE f.Dim_DateidReschedule <> ifnull(od.dim_dateid,1);

Create table fact_mrp_ne_02 as Select * from fact_mrp_ne_01 where 1=2;


INSERT INTO fact_mrp_ne_02(			fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
			dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
                      Dim_DateidReschedule,
                           MDTB_OLDSL_sub,
                        MDTB_DAT01_sub,
                        CompanyCode_sub,
                        MDTB_LGORT_sub,
                        plantcode_sub,
                        MDKP_EKGRP_sub,
                        mdkp_kzaus_sub,
                        MDTB_PERKZ_sub,
                        MDTB_SOBES_sub,
                        MDKP_DTART_sub,
                        MDTB_DAT00_sub,
                        MDTB_WEBAZ_sub,
                        MDTB_UMDAT_sub)
Select 			ne01.fact_mrpid,
                        ne01.Dim_MRPElementid,
                      ne01.dim_mrpexceptionID1,
                      ne01.dim_mrpexceptionID2,
                      ne01.Dim_Companyid,
                      ne01.Dim_Currencyid,
					  ne01.Dim_Currencyid_TRA,
					  ne01.Dim_Currencyid_GBL,
					  ne01.amt_ExchangeRate,
					  ne01.amt_ExchangeRate_GBL,					  
                      ne01.Dim_DateidActionRequired,
                      ne01.Dim_DateidActionClosed,
                      ne01.Dim_DateidDateNeeded,
                      ne01.Dim_DateidOriginalDock,
                      ne01.Dim_DateidMRP,
                      ne01.Dim_Partid,
                      ne01.Dim_StorageLocationid,
                      ne01.Dim_Plantid,
                      ne01.Dim_UnitOfMeasureid,
                      ne01.Dim_PurchaseGroupid,
                      ne01.Dim_PurchaseOrgid,
                      ne01.Dim_Vendorid,
                      ne01.Dim_ActionStateid,
                      ne01.Dim_ItemCategoryid,
                      ne01.Dim_DocumentTypeid,
                      ne01.Dim_MRPProcedureid,
                      ne01.Dim_MRPDiscontinuationIndicatorid,
                      ne01.Dim_periodindicatorid,
                      ne01.Dim_specialprocurementid,
                      ne01.Dim_FixedVendorid,
                      ne01.Dim_ConsumptionTypeid,
                      ne01.ct_QtyMRP,
                      ne01.ct_QtyScrap,
                      ne01.ct_DaysGRPProcessing,
                      ne01.ct_DaysReceiptCoverage,
                      ne01.ct_DaysStockCoverage,
                      ne01.dd_DocumentNo,
                      ne01.dd_DocumentItemNo,
                      ne01.dd_ScheduleNo,
                      ne01.dd_mrptablenumber,
                      ne01.dd_peggedrequirement,
                      ne01.dd_bomexplosionno,
                        ne01.dd_SourceOrderNo,
                        ne01.dd_MRPElementNo,
			ne01.dim_mrplisttypeid,
                      ne01.dd_PlannScenarioLTP,
                      ne01.dim_DateidMRPDock,
                      ne01.Dim_DateidReschedule,
                           ne01.MDTB_OLDSL_sub,
                        ne01.MDTB_DAT01_sub,
                        ne01.CompanyCode_sub,
                        ne01.MDTB_LGORT_sub,
                        ne01.plantcode_sub,
                        ne01.MDKP_EKGRP_sub,
                        ne01.mdkp_kzaus_sub,
                        ne01.MDTB_PERKZ_sub,
                        ne01.MDTB_SOBES_sub,
                        ne01.MDKP_DTART_sub,
                        ne01.MDTB_DAT00_sub,
                        ne01.MDTB_WEBAZ_sub,
                        ne01.MDTB_UMDAT_sub
From fact_mrp_ne_01 ne01,
     fact_mrp_00e mrp
Where mrp.Dim_Partid =  ne01.Dim_Partid
      AND mrp.dim_mrpexceptionID1 = ne01.dim_mrpexceptionID1
	      AND mrp.Dim_MRPElementid = ne01.Dim_MRPElementid
	      AND mrp.ct_QtyMRP = ne01.ct_QtyMRP
	      AND mrp.dd_PlannScenarioLTP = ne01.dd_PlannScenarioLTP;


merge into fact_mrp_ne_01 t1
using (select distinct t.fact_mrpid
         from fact_mrp_ne_01 t
	 INNER JOIN fact_mrp_ne_02 t2 ON  t.fact_mrpid = t2.fact_mrpid ) src
on t1.fact_mrpid = src.fact_mrpid
when matched then delete;






Insert into fact_mrp(fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,					  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
                        dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
			Dim_DateidReschedule)
 SELECT  ifnull(max_holder_00e.maxid,0) + row_number() over(order by '') fact_mrpid,
                        Dim_MRPElementid,
                      dim_mrpexceptionID1,
                      dim_mrpexceptionID2,
                      Dim_Companyid,
                      Dim_Currencyid,
					  Dim_Currencyid_TRA,
					  Dim_Currencyid_GBL,
					  amt_ExchangeRate,
					  amt_ExchangeRate_GBL,		  
                      Dim_DateidActionRequired,
                      Dim_DateidActionClosed,
                      Dim_DateidDateNeeded,
                      Dim_DateidOriginalDock,
                      Dim_DateidMRP,
                      Dim_Partid,
                      Dim_StorageLocationid,
                      Dim_Plantid,
                      Dim_UnitOfMeasureid,
                      Dim_PurchaseGroupid,
                      Dim_PurchaseOrgid,
                      Dim_Vendorid,
                      Dim_ActionStateid,
                      Dim_ItemCategoryid,
                      Dim_DocumentTypeid,
                      Dim_MRPProcedureid,
                      Dim_MRPDiscontinuationIndicatorid,
                      Dim_periodindicatorid,
                      Dim_specialprocurementid,
                      Dim_FixedVendorid,
                      Dim_ConsumptionTypeid,
                      ct_QtyMRP,
                      ct_QtyScrap,
                      ct_DaysGRPProcessing,
                      ct_DaysReceiptCoverage,
                      ct_DaysStockCoverage,
                      dd_DocumentNo,
                      dd_DocumentItemNo,
                      dd_ScheduleNo,
                      dd_mrptablenumber,
                      dd_peggedrequirement,
                      dd_bomexplosionno,
                        dd_SourceOrderNo,
                        dd_MRPElementNo,
                        dim_mrplisttypeid,
                      dd_PlannScenarioLTP,
                      dim_DateidMRPDock,
			Dim_DateidReschedule
From fact_mrp_ne_01,max_holder_00e;

drop table if exists fact_mrp_00e;
drop table if exists fact_mrp_ne_01;
drop table if exists fact_mrp_ne_02;


/* LK: 20 Sep 2013 - amt_UnitStdPrice is in local currency ( as STPRS in mbew_no_bwtar is in plant/company currency ) */
/* At this point, before updates from other facts, local and tran curr are the same. */
/* When the columns are updated from other fact tables, ensure that this is updated to tran curr */


DROP TABLE IF EXISTS tmp_unitstdprice_update;
CREATE TABLE tmp_unitstdprice_update
AS
SELECT p.PartNumber, pl.ValuationArea, fact_mrpid 
FROM  fact_mrp m
      CROSS JOIN  tmpvariable_00e tmp
      INNER JOIN dim_plant pl ON pl.dim_plantid = m.dim_plantid
	  INNER JOIN dim_part p ON m.Dim_Partid = p.Dim_Partid
WHERE   tmp.tmpTotalCount > 0
	AND m.Dim_ActionStateid = 2
	AND (m.amt_UnitStdPrice is null or m.amt_UnitStdPrice = 0);

UPDATE fact_mrp m
SET m.amt_UnitStdPrice =  ifnull((CASE sp.VPRSV WHEN 'S' 
										        THEN (sp.STPRS / (case when sp.PEINH = 0 then null else  sp.PEINH end))
												WHEN 'V' 
										        THEN (sp.VERPR / (case when sp.PEINH = 0 then null else  sp.PEINH end))
                                  END),0)
FROM fact_mrp m
      INNER JOIN tmp_unitstdprice_update tmp ON tmp.fact_mrpid = m.fact_mrpid
      LEFT JOIN mbew_no_bwtar sp ON sp.MATNR = tmp.PartNumber and sp.BWKEY = tmp.ValuationArea
WHERE ((sp.LFGJA * 100) + sp.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from mbew_no_bwtar x 
                                       where     x.MATNR = sp.MATNR 
									         AND x.BWKEY = sp.BWKEY
                                             AND ((x.VPRSV = 'S' AND x.STPRS > 0) OR 
											      (x.VPRSV = 'V' AND x.VERPR > 0)   )
									   )
     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0) OR (sp.VPRSV = 'V' AND sp.VERPR > 0));



UPDATE fact_mrp m
SET m.Dim_Vendorid = po.Dim_Vendorid
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, 
    fact_mrp m
WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;
UPDATE fact_mrp m
SET  m.Dim_ConsumptionTypeid = po.Dim_ConsumptionTypeid
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;



UPDATE fact_mrp m
SET m.Dim_FixedVendorid = po.Dim_FixedVendorid
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;

	
UPDATE fact_mrp m
SET m.dd_firmed = ifnull(pm.FirmingIndicator,'Not Set')
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	   tmpvariable_00e, dim_planordermisc pm, fact_mrp m   
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
        AND pm.Dim_PlanOrderMiscid = po.Dim_PlanOrderMiscid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;
	
	
drop table if exists tmp_po_fact_mrp;
create table tmp_po_fact_mrp as
select distinct fact_mrpid,
     FIRST_VALUE(od.dim_dateid) OVER(PARTITION BY fact_mrpid ORDER BY od.dim_dateid DESC) dim_dateid
From fact_planorder po,
     Dim_Company dc,
     mdkp k,
     mdtb t,
     dim_part p,
     tmpvariable_00e,
     fact_mrp m, dim_date od
where 1 = 1
        AND m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	AND tmpTotalCount > 0
	AND od.DateValue = t.MDTB_UMDAT AND od.CompanyCode = dc.CompanyCode
	AND m.Dim_DateidReschedule <> od.dim_dateid;
	
update fact_mrp m
SET m.Dim_DateidReschedule = tmp.dim_dateid
from  tmp_po_fact_mrp tmp, fact_mrp m
where m.fact_mrpid=tmp.fact_mrpid;
	
	
/* LK: 21 Sep 2013: Before updating amt, convert stdprice to tran currency in po */	
UPDATE fact_mrp m
SET m.amt_UnitStdPrice = m.amt_UnitStdPrice / po.amt_ExchangeRate	
From    fact_planorder po,
	Dim_Company dc,
	mdkp k,
	mdtb t,
	dim_part p,
	tmpvariable_00e,
	fact_mrp m
WHERE     m.Dim_ActionStateid = 2
AND k.MDKP_DTNUM = t.MDTB_DTNUM
AND m.dd_DocumentNo = po.dd_PlanOrderNo
AND m.dd_DocumentNo = t.MDTB_DELNR
AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
AND po.Dim_Companyid = dc.Dim_Companyid
AND m.Dim_Partid = p.Dim_Partid
and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
AND tmpTotalCount > 0;	

UPDATE fact_mrp m
SET m.amt_ExtendedPrice = m.amt_UnitStdPrice * m.ct_QtyMRP 
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e,
        fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid		
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;
		
		
		
DROP TABLE IF EXISTS tmp_upd_leadtimevar_mrp;
CREATE TABLE tmp_upd_leadtimevar_mrp
AS
SELECT DISTINCT m.dd_DocumentNo,m.dd_PlannScenarioLTP,m.Dim_Partid,MDTB_UMDAT,MDTB_DAT01,MDTB_DAT02,p.LeadTime, k.MDKP_DTNUM, t.MDTB_DELKZ, t.mdtb_dtpos
FROM fact_mrp m,mdkp k,mdtb t,dim_part p
WHERE m.Dim_ActionStateid = 2
AND m.dd_DocumentNo = t.MDTB_DELNR
AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
AND k.MDKP_DTNUM = t.MDTB_DTNUM
AND m.Dim_Partid = p.Dim_Partid
AND p.partnumber = k.mdkp_matnr
AND p.plant = k.mdkp_plwrk;

DELETE FROM tmp_upd_leadtimevar_mrp a
WHERE EXISTS ( SELECT 1 FROM tmp_upd_leadtimevar_mrp b
        WHERE a.dd_DocumentNo = b.dd_DocumentNo AND a.dd_PlannScenarioLTP = b.dd_PlannScenarioLTP AND a.MDKP_DTNUM = b.MDKP_DTNUM AND a.MDTB_DELKZ = b.MDTB_DELKZ
        AND a.Dim_Partid = b.Dim_Partid
        AND b.mdtb_dtpos > a.mdtb_dtpos);

UPDATE fact_mrp m
SET m.ct_leadTimeVariance = ifnull(((ifnull(MDTB_UMDAT, MDTB_DAT01) -  MDTB_DAT02) - p.LeadTime), -1 * p.LeadTime)
FROM tmp_upd_leadtimevar_mrp p,
     dim_mrpelement me, 
     tmpvariable_00e, 
     fact_mrp m
WHERE me.Dim_MRPElementID = m.Dim_MRPElementid 
   AND me.mrpelement = p.MDTB_DELKZ
   AND m.Dim_ActionStateid = 2 
   AND m.dd_DocumentNo = p.dd_DocumentNo
   AND m.dd_PlannScenarioLTP = p.dd_PlannScenarioLTP 
   AND m.Dim_Partid = p.Dim_Partid
   AND tmpTotalCount > 0
   AND m.ct_leadTimeVariance <> ifnull(((ifnull(MDTB_UMDAT, MDTB_DAT01) -  MDTB_DAT02) - p.LeadTime), -1 * p.LeadTime);

DROP TABLE IF EXISTS tmp_upd_leadtimevar_mrp;	


	


	
	
/* Get the currency and exchange rate columns from planorder */	
UPDATE fact_mrp m
    SET m.Dim_CurrencyID = po.Dim_CurrencyID
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e,
    fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;
	
	
UPDATE fact_mrp m
SET m.Dim_CurrencyID_TRA = po.Dim_CurrencyID_TRA
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;



UPDATE fact_mrp m
    SET m.Dim_CurrencyID_GBL = po.Dim_CurrencyID_GBL
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e,
    fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;


	UPDATE fact_mrp m
SET m.amt_ExchangeRate = po.amt_ExchangeRate
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;

	
UPDATE fact_mrp m
SET m.amt_ExchangeRate_GBL = po.amt_ExchangeRate_GBL
From  fact_planorder po,
        Dim_Company dc,
        mdkp k,
        mdtb t,
        dim_part p,
	tmpvariable_00e, fact_mrp m
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = po.dd_PlanOrderNo
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND po.Dim_Companyid = dc.Dim_Companyid
        AND m.Dim_Partid = p.Dim_Partid
	and p.partnumber = k.mdkp_matnr /* 01Jun2015 to fix ambigous replace error */
	AND tmpTotalCount > 0;	

	


        


