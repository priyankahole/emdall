drop table if exists fact_purchase_temp;
create table fact_purchase_temp as select distinct dd_DocumentNo from fact_purchase;

delete from NUMBER_FOUNTAIN where table_name = 'fact_poaudittrail';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_poaudittrail',ifnull(max(fact_poaudittrailid),0)
FROM fact_poaudittrail;

/* Get ItemNo and ScheduleNo before inserting in the fact table because VW does not support functions in join condition*/
update CDPOS_PO set CDPOS_ItemNo = substr(CDPOS_TABKEY,14,5);

update CDPOS_PO set CDPOS_ScheduleNo = substr(CDPOS_TABKEY,19,4);


DROP TABLE IF EXISTS tmp_fapo_t1001l;
CREATE TABLE tmp_fapo_t1001l AS
select
	((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_poaudittrail') + row_number() over (order by'')) fact_poaudittrailid,
	  CDHDR_CHANGENR	dd_ChangeDocNo,
	  CDHDR_USERNAME	dd_UserName,
	  dim_Dateid		dim_DateidChange,
	  CDHDR_UTIME		dd_TimeChange,
	  ifnull(CDHDR_PLANCHNGNR, 'Not Set')  dd_PLAN_ChangeNo,
	  ifnull(CDHDR_ACT_CHNGNO, 'Not Set')  dd_ACT_ChangeNo,
	  ifnull(CDHDR_WAS_PLANND, 'Not Set')  dd_Planned,
	  ifnull(CDHDR_OBJECTID, 'Not Set')	   dd_DocumentNo,
	  CDPOS_ItemNo		dd_ItemNo,
	  CDPOS_ScheduleNo  dd_ScheduleNo,
	  ifnull(CDPOS_TABNAME, 'Not Set')		dd_TableName,
	  ifnull(CDPOS_FNAME, 'Not Set')		dd_FieldName,
	  ifnull(CDPOS_CHNGIND, 'Not Set')		dd_ChangeType,
	  ifnull(CDPOS_TEXT_CASE, 'Not Set')	dd_TextCase,
	  CONVERT (BIGINT, 1) AS dim_UnitOfMeasureidOld, --ifnull((select Dim_UnitOfMeasureid from dim_unitofmeasure where UOM = CDPOS_UNIT_OLD), 1)	dim_UnitOfMeasureidOld,
	  CONVERT (BIGINT, 1) AS dim_UnitOfMeasureidNew, --ifnull((select Dim_UnitOfMeasureid from dim_unitofmeasure where UOM = CDPOS_UNIT_New), 1)	dim_UnitOfMeasureidNew,
	  CONVERT (BIGINT, 1) AS dim_CurrencyidOld, --ifnull((select Dim_Currencyid from dim_currency where CurrencyCode = CDPOS_CUKY_OLD), 1) 	dim_CurrencyidOld,
	  CONVERT (BIGINT, 1) AS dim_CurrencyidNew, --ifnull((select Dim_Currencyid from dim_currency where CurrencyCode = CDPOS_CUKY_NEW), 1) 	dim_CurrencyidNew,
	  CONVERT (BIGINT, 1) dim_DateidItemDeliveryOld,
	  CONVERT (BIGINT, 1) dim_DateidItemDeliveryNew,
	  CONVERT (BIGINT, 1) dim_DateidStatRelDeliveryOld,
	  CONVERT (BIGINT, 1) dim_DateidStatRelDeliveryNew,
	  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'FRGKE' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
	  	   else 'Not Set' end dd_ReleaseIndicatorOld,
	  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'FRGKE' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
	  	   else 'Not Set' end dd_ReleaseIndicatorNew,
	  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'FRGZU' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
	  	   else 'Not Set' end dd_ReleasedOld,
	  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'FRGZU' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
	  	   else 'Not Set' end dd_ReleasedNew,
	  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'PROCSTAT' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
	  	   else 'Not Set' end dd_ProcessStatusOld,
	  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'PROCSTAT' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
	  	   else 'Not Set' end dd_ProcessStatusNew,
	  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'RLWRT' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
	  	   else 0.00 end amt_ReleaseTotalValOld,
	  case when CDPOS_TABNAME = 'EKKO' and CDPOS_FNAME = 'RLWRT' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
	  	   else 0.00 end amt_ReleaseTotalValNew,  
	  CONVERT (BIGINT, 1) dim_DateidItemCreationOld,
	  CONVERT (BIGINT, 1) dim_DateidItemCreationNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'BRTWR' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
	  	   else 0.00 end amt_GrossValueOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'BRTWR' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
	  	   else 0.00 end amt_GrossValueNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'EFFWR' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
	  	   else 0.00 end amt_EffectiveValueOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'EFFWR' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
	  	   else 0.00 end amt_EffectiveValueNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'EREKZ' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
	  	   else 'Not Set' end dd_FinalInvoiceIndOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'EREKZ' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
	  	   else 'Not Set' end dd_FinalInvoiceIndNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NAVNW' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
	  	   else 0.00 end amt_TaxAmountOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NAVNW' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
	  	   else 0.00 end amt_TaxAmountNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NETPR' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
	  	   else 0.00 end amt_NetOrderPriceOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NETPR' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
	  	   else 0.00 end amt_NetOrderPriceNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NETWR' then ifnull(decimal(CDPOS_VALUE_OLD,18,2), 0)
	  	   else 0.00 end amt_ItemNetPriceOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'NETWR' then ifnull(decimal(CDPOS_VALUE_NEW,18,2), 0)
	  	   else 0.00 end amt_ItemNetPriceNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'SCHPR' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
	  	   else 'Not Set' end dd_ItemEstimatedPriceOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'SCHPR' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
	  	   else 'Not Set' end dd_ItemEstimatedPriceNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'LOEKZ' then ifnull(CDPOS_VALUE_OLD, 'Not Set')
	  	   else 'Not Set' end dd_ItemDeletionIndicatorOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'LOEKZ' then ifnull(CDPOS_VALUE_NEW, 'Not Set')
	  	   else 'Not Set' end dd_ItemDeletionIndicatorNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'KTMNG' then ifnull(decimal(CDPOS_VALUE_OLD,18,3), 0)
	  	   else 0.000 end ct_TargetQuantityOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'KTMNG' then ifnull(decimal(CDPOS_VALUE_NEW,18,3), 0)
	  	   else 0.000 end ct_TargetQuantityNew,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'MENGE' then ifnull(decimal(CDPOS_VALUE_OLD,18,3), 0)
	  	   else 0.000 end ct_OrderQuantityOld,
	  case when CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'MENGE' then ifnull(decimal(CDPOS_VALUE_NEW,18,3), 0)
	  	   else 0.000 end ct_OrderQuantityNew,
	  case when CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'MENGE' then ifnull(decimal(CDPOS_VALUE_OLD,18,3), 0)
	  	   else 0.000 end ct_ScheduledQuantityOld,
	  case when CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'MENGE' then ifnull(decimal(CDPOS_VALUE_NEW,18,3), 0)
	  	   else 0.000 end ct_ScheduledQuantityNew,
	  CONVERT (BIGINT, 1) AS Dim_CompanyId, --ifnull((select distinct Dim_CompanyId from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_CompanyId, -- header
	  CONVERT (BIGINT, 1) AS Dim_Currencyid, --ifnull((select distinct Dim_Currencyid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_Currencyid, -- header
	  CONVERT (BIGINT, 1) AS Dim_DateidCreate, --ifnull((select distinct Dim_DateidCreate from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_DateidCreate, -- header
	  CONVERT (BIGINT, 1) AS Dim_DateidItemCreate, --ifnull((select distinct Dim_DateidItemCreate from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID and dd_DocumentItemNo = CDPOS_ItemNo),1) Dim_DateidItemCreate, -- item
	  CONVERT (BIGINT, 1) AS Dim_DocumentTypeid, --ifnull((select distinct Dim_DocumentTypeid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_DocumentTypeid, -- header
	  CONVERT (BIGINT, 1) AS Dim_PlantidOrdering,--ifnull((select distinct Dim_PlantidOrdering from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID and dd_DocumentItemNo = CDPOS_ItemNo),1) Dim_PlantidOrdering, -- item
	  CONVERT (BIGINT, 1) AS Dim_POCurrencyid, --ifnull((select distinct Dim_POCurrencyid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_POCurrencyid, -- header
	  CONVERT (BIGINT, 1) AS Dim_Producthierarchyid, --ifnull((select distinct Dim_Producthierarchyid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID and dd_DocumentItemNo = CDPOS_ItemNo),1) Dim_Producthierarchyid, -- item
	  CONVERT (BIGINT, 1) AS Dim_PurchaseGroupid, --ifnull((select distinct Dim_PurchaseGroupid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_PurchaseGroupid, -- header
	  CONVERT (BIGINT, 1) AS Dim_PurchaseOrgid, --ifnull((select distinct Dim_PurchaseOrgid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_PurchaseOrgid, -- header
	  CONVERT (BIGINT, 1) AS Dim_StorageLocationid,--ifnull((select distinct Dim_StorageLocationid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID and dd_DocumentItemNo = CDPOS_ItemNo),1) Dim_StorageLocationid, -- item
	  CONVERT (BIGINT, 1) AS Dim_Vendorid,--ifnull((select distinct Dim_Vendorid from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_Vendorid, -- header
	  CONVERT (BIGINT, 1) AS Dim_Currencyid_TRA, --ifnull((select distinct Dim_Currencyid_TRA from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_Currencyid_TRA,
	  CONVERT (BIGINT, 1) AS Dim_Currencyid_GBL, --ifnull((select distinct Dim_Currencyid_GBL from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) Dim_Currencyid_GBL,
	  CONVERT (BIGINT, 1) AS amt_ExchangeRate, --ifnull((select distinct amt_ExchangeRate from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) amt_ExchangeRate,
	  CONVERT (BIGINT, 1) AS amt_ExchangeRate_GBL, --ifnull((select distinct amt_ExchangeRate_GBL from fact_purchase where dd_DocumentNo = CDPOS_OBJECTID),1) amt_ExchangeRate_GBL
	  CDPOS_UNIT_OLD,  --dim_UnitOfMeasureidOld
	  CDPOS_UNIT_New,  --dim_UnitOfMeasureidNew
	  CDPOS_CUKY_OLD,  --dim_CurrencyidOld
	  CDPOS_CUKY_NEW,  --dim_CurrencyidNew
	  CDPOS_OBJECTID,
	  CDPOS_ItemNo
from CDHDR_PO inner join CDPOS_PO on CDHDR_OBJECTID = CDPOS_OBJECTID and CDHDR_CHANGENR = CDPOS_CHANGENR
			  inner join fact_purchase_temp fp on CDPOS_OBJECTID = fp.dd_DocumentNo
  			  inner join dim_date dd on CDHDR_UDATE = dd.DateValue and dd.CompanyCode = 'Not Set'
where not exists (select 1 from fact_poaudittrail where dd_ChangeDocNo = CDPOS_CHANGENR and dd_DocumentNo = CDPOS_OBJECTID and dd_TableName = CDPOS_TABNAME  and dd_FieldName = CDPOS_FNAME and dd_ChangeType = CDPOS_CHNGIND 
									and dd_ItemNo = CDPOS_ItemNo and dd_ScheduleNo = CDPOS_ScheduleNo);

	update tmp_fapo_t1001l f
	set f.dim_UnitOfMeasureidOld = ifnull(du.Dim_UnitOfMeasureid, 1)
	from tmp_fapo_t1001l f
			left join dim_unitofmeasure du ON du.UOM = f.CDPOS_UNIT_OLD
	where f.dim_UnitOfMeasureidOld <> ifnull(du.Dim_UnitOfMeasureid, 1);	

	UPDATE tmp_fapo_t1001l f
	SET f.dim_UnitOfMeasureidNew = IFNULL(du.Dim_UnitOfMeasureid, 1)
	FROM tmp_fapo_t1001l f
		LEFT JOIN dim_unitofmeasure du ON du.UOM = f.CDPOS_UNIT_New
	WHERE f.dim_UnitOfMeasureidNew <> IFNULL(du.Dim_UnitOfMeasureid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.dim_CurrencyidOld = IFNULL(dc.Dim_Currencyid, 1)
	FROM tmp_fapo_t1001l f
		LEFT JOIN dim_currency dc ON dc.CurrencyCode = f.CDPOS_CUKY_OLD
	WHERE f.dim_CurrencyidOld <> IFNULL(dc.Dim_Currencyid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.dim_CurrencyidNew = IFNULL(dc.Dim_Currencyid, 1)
	FROM tmp_fapo_t1001l f
		LEFT JOIN dim_currency dc ON dc.CurrencyCode = f.CDPOS_CUKY_NEW
	WHERE f.dim_CurrencyidNew <> IFNULL(dc.Dim_Currencyid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_CompanyId = IFNULL(fp.Dim_CompanyId, 1)
	FROM tmp_fapo_t1001l f
		LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_CompanyId <> IFNULL(fp.Dim_CompanyId, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_Currencyid = IFNULL(f.Dim_Currencyid, 1)
	FROM tmp_fapo_t1001l f
		LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_Currencyid <> IFNULL(fp.Dim_Currencyid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_DateidCreate = IFNULL(fp.Dim_DateidCreate, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_DateidCreate <> IFNULL(fp.Dim_DateidCreate, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_DateidItemCreate = IFNULL(fp.Dim_DateidItemCreate, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID and fp.dd_DocumentItemNo = f.CDPOS_ItemNo
	WHERE f.Dim_DateidItemCreate <> IFNULL(fp.Dim_DateidItemCreate, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_DocumentTypeid = IFNULL(fp.Dim_DocumentTypeid, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_DocumentTypeid <> IFNULL(fp.Dim_DocumentTypeid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_PlantidOrdering = IFNULL(fp.Dim_PlantidOrdering, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID and fp.dd_DocumentItemNo = f.CDPOS_ItemNo
	WHERE f.Dim_PlantidOrdering <> IFNULL(fp.Dim_PlantidOrdering, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_POCurrencyid = IFNULL(fp.Dim_POCurrencyid, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_POCurrencyid <> IFNULL(fp.Dim_POCurrencyid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_Producthierarchyid = IFNULL(fp.Dim_Producthierarchyid, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID and fp.dd_DocumentItemNo = f.CDPOS_ItemNo
	WHERE f.Dim_Producthierarchyid <> IFNULL(fp.Dim_Producthierarchyid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_PurchaseGroupid = IFNULL(fp.Dim_PurchaseGroupid, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_PurchaseGroupid <> IFNULL(fp.Dim_PurchaseGroupid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_PurchaseOrgid = IFNULL(fp.Dim_PurchaseOrgid, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_PurchaseOrgid <> IFNULL(fp.Dim_PurchaseOrgid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_StorageLocationid = IFNULL(fp.Dim_StorageLocationid, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID and fp.dd_DocumentItemNo = f.CDPOS_ItemNo
	WHERE f.Dim_StorageLocationid <> IFNULL(fp.Dim_StorageLocationid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_Vendorid = IFNULL(fp.Dim_Vendorid, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_Vendorid <> IFNULL(fp.Dim_Vendorid, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_Currencyid_TRA = IFNULL(fp.Dim_Currencyid_TRA, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_Currencyid_TRA <> IFNULL(fp.Dim_Currencyid_TRA, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.Dim_Currencyid_GBL = IFNULL(fp.Dim_Currencyid_GBL, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.Dim_Currencyid_GBL <> IFNULL(fp.Dim_Currencyid_GBL, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.amt_ExchangeRate = IFNULL(fp.amt_ExchangeRate, 1)
	FROM tmp_fapo_t1001l f
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.amt_ExchangeRate <> IFNULL(fp.amt_ExchangeRate, 1);
	
	UPDATE tmp_fapo_t1001l f
	SET f.amt_ExchangeRate_GBL = IFNULL(fp.amt_ExchangeRate_GBL, 1)
	FROM tmp_fapo_t1001l f 
	LEFT JOIN fact_purchase fp ON fp.dd_DocumentNo = f.CDPOS_OBJECTID
	WHERE f.amt_ExchangeRate_GBL <> IFNULL(fp.amt_ExchangeRate_GBL, 1);
	
insert into fact_poaudittrail
(
  fact_POAuditTrailid,
  dd_ChangeDocNo,
  dd_UserName,
  dim_DateidChange,
  dd_TimeChange,
  dd_PLAN_ChangeNo,
  dd_ACT_ChangeNo,
  dd_Planned,
  dd_DocumentNo,
  dd_ItemNo,
  dd_ScheduleNo,
  dd_TableName,
  dd_FieldName,
  dd_ChangeType,
  dd_TextCase,
  dim_UnitOfMeasureidOld,
  dim_UnitofmeasureidNew,
  dim_CurrencyidOld,
  dim_CurrencyidNew,
  dim_DateidItemDeliveryOld,
  dim_DateidItemDeliveryNew,
  dim_DateidStatRelDeliveryOld,
  dim_DateidStatRelDeliveryNew,
  dd_ReleaseIndicatorOld,
  dd_ReleaseIndicatorNew,
  dd_ReleasedOld,
  dd_ReleasedNew,
  dd_ProcessStatusOld,
  dd_ProcessStatusNew,
  amt_ReleaseTotalValOld,
  amt_ReleaseTotalValNew,
  dim_DateidItemCreationOld,
  dim_DateidItemCreationNew,
  amt_GrossValueOld,
  amt_GrossValueNew,
  amt_EffectiveValueOld,
  amt_EffectiveValueNew,
  dd_FinalInvoiceIndOld,
  dd_FinalInvoiceIndNew,
  amt_TaxAmountOld,
  amt_TaxAmountNew,
  amt_NetOrderPriceOld,
  amt_NetOrderPriceNew,
  amt_ItemNetPriceOld,
  amt_ItemNetPriceNew,
  dd_ItemEstimatedPriceOld,
  dd_ItemEstimatedPriceNew,
  dd_ItemDeletionIndicatorOld,
  dd_ItemDeletionIndicatorNew,
  ct_TargetQuantityOld,
  ct_TargetQuantityNew,
  ct_OrderQuantityOld,
  ct_OrderQuantityNew,
  ct_ScheduledQuantityOld,
  ct_ScheduledQuantityNew,
  Dim_CompanyId,
  Dim_Currencyid,	
  Dim_DateidCreate,
  Dim_DateidItemCreate,
  Dim_DocumentTypeid,
  Dim_PlantidOrdering,
  Dim_POCurrencyid,
  Dim_Producthierarchyid,
  Dim_PurchaseGroupid,
  Dim_PurchaseOrgid,
  Dim_StorageLocationid,
  Dim_Vendorid,
  dim_Currencyid_TRA,
  dim_Currencyid_GBL,
  amt_ExchangeRate,
  amt_ExchangeRate_GBL
)
SELECT
  fact_POAuditTrailid,
  dd_ChangeDocNo,
  dd_UserName,
  dim_DateidChange,
  dd_TimeChange,
  dd_PLAN_ChangeNo,
  dd_ACT_ChangeNo,
  dd_Planned,
  dd_DocumentNo,
  dd_ItemNo,
  dd_ScheduleNo,
  dd_TableName,
  dd_FieldName,
  dd_ChangeType,
  dd_TextCase,
  dim_UnitOfMeasureidOld,
  dim_UnitofmeasureidNew,
  dim_CurrencyidOld,
  dim_CurrencyidNew,
  dim_DateidItemDeliveryOld,
  dim_DateidItemDeliveryNew,
  dim_DateidStatRelDeliveryOld,
  dim_DateidStatRelDeliveryNew,
  dd_ReleaseIndicatorOld,
  dd_ReleaseIndicatorNew,
  dd_ReleasedOld,
  dd_ReleasedNew,
  dd_ProcessStatusOld,
  dd_ProcessStatusNew,
  amt_ReleaseTotalValOld,
  amt_ReleaseTotalValNew,
  dim_DateidItemCreationOld,
  dim_DateidItemCreationNew,
  amt_GrossValueOld,
  amt_GrossValueNew,
  amt_EffectiveValueOld,
  amt_EffectiveValueNew,
  dd_FinalInvoiceIndOld,
  dd_FinalInvoiceIndNew,
  amt_TaxAmountOld,
  amt_TaxAmountNew,
  amt_NetOrderPriceOld,
  amt_NetOrderPriceNew,
  amt_ItemNetPriceOld,
  amt_ItemNetPriceNew,
  dd_ItemEstimatedPriceOld,
  dd_ItemEstimatedPriceNew,
  dd_ItemDeletionIndicatorOld,
  dd_ItemDeletionIndicatorNew,
  ct_TargetQuantityOld,
  ct_TargetQuantityNew,
  ct_OrderQuantityOld,
  ct_OrderQuantityNew,
  ct_ScheduledQuantityOld,
  ct_ScheduledQuantityNew,
  Dim_CompanyId,
  Dim_Currencyid,	
  Dim_DateidCreate,
  Dim_DateidItemCreate,
  Dim_DocumentTypeid,
  Dim_PlantidOrdering,
  Dim_POCurrencyid,
  Dim_Producthierarchyid,
  Dim_PurchaseGroupid,
  Dim_PurchaseOrgid,
  Dim_StorageLocationid,
  Dim_Vendorid,
  dim_Currencyid_TRA,
  dim_Currencyid_GBL,
  amt_ExchangeRate,
  amt_ExchangeRate_GBL
from tmp_fapo_t1001l;
									
/* Update date dim ids because VW does not support select in a case conditional expression*/			
update fact_poaudittrail
set dim_DateidItemDeliveryOld = dd.Dim_Dateid
from CDPOS_PO, dim_date dd, fact_poaudittrail
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'EINDT' and
	  TO_DATE(CDPOS_VALUE_OLD, 'YYYY-MM-DD') = dd.DateValue and --STR_TO_DATE(CDPOS_VALUE_OLD,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
set dim_DateidItemDeliveryNew = dd.Dim_Dateid
from CDPOS_PO, dim_date dd, fact_poaudittrail
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'EINDT' and
	  TO_DATE(CDPOS_VALUE_OLD, 'YYYY-MM-DD') = dd.DateValue and --STR_TO_DATE(CDPOS_VALUE_NEW,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
set dim_DateidStatRelDeliveryOld = dd.Dim_Dateid
from CDPOS_PO, dim_date dd, fact_poaudittrail
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'SLFDT' and
	  TO_DATE(CDPOS_VALUE_OLD, 'YYYY-MM-DD') = dd.DateValue and --STR_TO_DATE(CDPOS_VALUE_OLD,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
set dim_DateidStatRelDeliveryNew = dd.Dim_Dateid
from CDPOS_PO, dim_date dd, fact_poaudittrail
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKET' and CDPOS_FNAME = 'SLFDT' and
	  TO_DATE(CDPOS_VALUE_OLD, 'YYYY-MM-DD') = dd.DateValue and --STR_TO_DATE(CDPOS_VALUE_NEW,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
set dim_DateidItemCreationOld = dd.Dim_Dateid
from CDPOS_PO, dim_date dd, fact_poaudittrail
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'AEDAT' and
	  TO_DATE(CDPOS_VALUE_OLD, 'YYYY-MM-DD') = dd.DateValue and --STR_TO_DATE(CDPOS_VALUE_OLD,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
update fact_poaudittrail
set dim_DateidItemCreationNew = dd.Dim_Dateid
from CDPOS_PO, dim_date dd, fact_poaudittrail
where dd_ChangeDocNo = CDPOS_CHANGENR and
	  dd_DocumentNo = CDPOS_OBJECTID and
	  dd_ItemNo = CDPOS_ItemNo and
	  dd_ScheduleNo = CDPOS_ScheduleNo and
	  CDPOS_TABNAME = 'EKPO' and CDPOS_FNAME = 'AEDAT' and
	  TO_DATE(CDPOS_VALUE_OLD, 'YYYY-MM-DD') = dd.DateValue and --STR_TO_DATE(CDPOS_VALUE_NEW,'%Y%m%d') = dd.DateValue and
	  CompanyCode = 'Not Set';
	  
DROP TABLE IF EXISTS tmp_fapo_t1001l;
drop table if exists fact_purchase_temp;
