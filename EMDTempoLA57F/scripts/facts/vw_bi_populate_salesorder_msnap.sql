drop table if exists tmp_dayofprocessing_var_msnap;
create table tmp_dayofprocessing_var_msnap
as
select to_Date(case when extract(hour from current_timestamp) between 0 and 16 then sysdate - 1 else sysdate end) as processing_date from (select 1) a;

delete from number_fountain m where m.table_name = 'fact_salesorder_msnap';

insert into number_fountain
select 	'fact_salesorder_msnap',
	ifnull(max(fact_salesorder_msnapid),ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_salesorder_msnap d ;

drop table if exists tmp_loaded_datevalues_msnap;

create table tmp_loaded_datevalues_msnap
as
select distinct(fc.dim_dateid) as dim_dateid
from fact_salesorder_msnap ws inner join dim_date_factory_calendar fc on ws.dim_dateidsnapshot = fc.dim_Dateid
where fc.datevalue =(select processing_date from tmp_dayofprocessing_var_msnap) ;

DELETE FROM fact_Salesorder_msnap t
where t.dim_dateidsnapshot in (select dim_dateid from tmp_loaded_datevalues_msnap );

insert into fact_Salesorder_msnap (
fact_Salesorder_msnapID
,DD_SALESDOCNO
,DD_SALESITEMNO
,DD_SCHEDULENO
,CT_SCHEDULEQTYSALESUNIT
,CT_CONFIRMEDQTY
,CT_CORRECTEDQTY
,CT_DELIVEREDQTY
,CT_ONHANDCOVEREDQTY
,AMT_UNITPRICE
,CT_PRICEUNIT
,AMT_SCHEDULETOTAL
,AMT_STDCOST
,AMT_TARGETVALUE
,CT_TARGETQTY
,AMT_EXCHANGERATE
,CT_OVERDLVRTOLERANCE
,CT_UNDERDLVRTOLERANCE
,DIM_DATEIDSALESORDERCREATED
,DIM_DATEIDSCHEDDELIVERYREQ
,DIM_DATEIDSCHEDDLVRREQPREV
,DIM_DATEIDSCHEDDELIVERY
,DIM_DATEIDGOODSISSUE
,DIM_DATEIDMTRLAVAIL
,DIM_DATEIDLOADING
,DIM_DATEIDTRANSPORT
,DIM_CURRENCYID
,DIM_PRODUCTHIERARCHYID
,DIM_PLANTID
,DIM_COMPANYID
,DIM_STORAGELOCATIONID
,DIM_SALESDIVISIONID
,DIM_SHIPRECEIVEPOINTID
,DIM_DOCUMENTCATEGORYID
,DIM_SALESDOCUMENTTYPEID
,DIM_SALESORGID
,DIM_CUSTOMERID
,DIM_DATEIDVALIDFROM
,DIM_DATEIDVALIDTO
,DIM_SALESGROUPID
,DIM_COSTCENTERID
,DIM_CONTROLLINGAREAID
,DIM_BILLINGBLOCKID
,DIM_TRANSACTIONGROUPID
,DIM_SALESORDERREJECTREASONID
,DIM_PARTID
,DIM_PARTSALESID
,DIM_SALESORDERHEADERSTATUSID
,DIM_SALESORDERITEMSTATUSID
,DIM_CUSTOMERGROUP1ID
,DIM_CUSTOMERGROUP2ID
,DIM_SALESORDERITEMCATEGORYID
,AMT_EXCHANGERATE_GBL
,CT_FILLQTY
,CT_ONHANDQTY
,DIM_DATEIDFIRSTDATE
,DIM_SCHEDULELINECATEGORYID
,DIM_SALESMISCID
,DD_ITEMRELFORDELV
,DIM_DATEIDSHIPMENTDELIVERY
,DIM_DATEIDACTUALGI
,DIM_DATEIDSHIPDLVRFILL
,DIM_DATEIDACTUALGIFILL
,DIM_PROFITCENTERID
,DIM_CUSTOMERIDSHIPTO
,DIM_DATEIDGUARANTEEDATE
,DIM_UNITOFMEASUREID
,DIM_DISTRIBUTIONCHANNELID
,DD_BATCHNO
,DD_CREATEDBY
,DIM_CUSTOMPARTNERFUNCTIONID
,DIM_DATEIDLOADINGFILL
,DIM_PAYERPARTNERFUNCTIONID
,DIM_BILLTOPARTYPARTNERFUNCTIONID
,CT_SHIPPEDAGNSTORDERQTY
,CT_CMLQTYRECEIVED
,DIM_CUSTOMPARTNERFUNCTIONID1
,DIM_CUSTOMPARTNERFUNCTIONID2
,DIM_CUSTOMERGROUPID
,DIM_SALESOFFICEID
,DIM_HIGHERCUSTOMERID
,DIM_HIGHERSALESORGID
,DIM_HIGHERDISTCHANNELID
,DIM_HIGHERSALESDIVID
,DD_SOLDTOHIERARCHYLEVEL
,DD_HIERARCHYTYPE
,DIM_TOPLEVELCUSTOMERID
,DIM_TOPLEVELDISTCHANNELID
,DIM_TOPLEVELSALESDIVID
,DIM_TOPLEVELSALESORGID
,CT_AFSUNALLOCATEDQTY
,AMT_AFSUNALLOCATEDVALUE
,CT_AFSALLOCATIONRQTY
,CT_CUMCONFIRMEDQTY
,CT_AFSALLOCATIONFQTY
,CT_CUMORDERQTY
,AMT_AFSONDELIVERYVALUE
,CT_SHIPPEDORBILLEDQTY
,AMT_SHIPPEDORBILLED
,DIM_CREDITREPRESENTATIVEID
,DD_CUSTOMERPONO
,CT_INCOMPLETEQTY
,AMT_INCOMPLETE
,DIM_DELIVERYBLOCKID
,DD_AFSSTOCKCATEGORY
,DD_AFSSTOCKTYPE
,DIM_DATEIDAFSCANCELDATE
,AMT_AFSNETPRICE
,AMT_AFSNETVALUE
,AMT_BASEPRICE
,DIM_MATERIALPRICEGROUP1ID
,AMT_CREDITHOLDVALUE
,AMT_DELIVERYBLOCKVALUE
,DIM_AFSSIZEID
,DIM_AFSREJECTIONREASONID
,DIM_DATEIDDLVRDOCCREATED
,AMT_CUSTOMEREXPECTEDPRICE
,CT_AFSALLOCATEDQTY
,CT_AFSONDELIVERYQTY
,DIM_OVERALLSTATUSCREDITCHECKID
,DIM_SALESDISTRICTID
,DIM_ACCOUNTASSIGNMENTGROUPID
,DIM_MATERIALGROUPID
,DIM_SALESDOCORDERREASONID
,AMT_SUBTOTAL3
,AMT_SUBTOTAL4
,DIM_DATEIDAFSREQDELIVERY
,AMT_DICOUNTACCRUALNETPRICE
,CT_AFSOPENQTY
,DIM_PURCHASEORDERTYPEID
,DIM_DATEIDQUOTATIONVALIDFROM
,DIM_DATEIDPURCHASEORDER
,DIM_DATEIDQUOTATIONVALIDTO
,DIM_AFSSEASONID
,DD_AFSDEPARTMENT
,DIM_DATEIDSOCREATED
,DIM_DATEIDSODOCUMENT
,DD_SUBSEQUENTDOCNO
,DD_SUBSDOCITEMNO
,DD_SUBSSCHEDULENO
,DIM_SUBSDOCCATEGORYID
,CT_AFSTOTALDRAWN
,DIM_DATEIDASLASTDATE
,DD_REFERENCEDOCUMENTNO
,DD_REQUIREMENTTYPE
,AMT_STDPRICE
,DIM_DATEIDNEXTDATE
,AMT_TAX
,DIM_DATEIDEARLIESTSOCANCELDATE
,DIM_DATEIDLATESTCUSTPOAGI
,DD_BUSINESSCUSTOMERPONO
,DIM_ROUTEID
,DIM_BILLINGDATEID
,DIM_CUSTOMERPAYMENTTERMSID
,DIM_SALESRISKCATEGORYID
,DD_CREDITREP
,DD_CREDITLIMIT
,DIM_CUSTOMERRISKCATEGORYID
,CT_FILLQTY_CRD
,CT_FILLQTY_PDD
,DIM_H1CUSTOMERID
,DIM_DATEIDSOITEMCHANGEDON
,DIM_SHIPPINGCONDITIONID
,DD_AFSALLOCATIONGROUPNO
,DIM_DATEIDREJECTION
,DD_CUSTOMERMATERIALNO
,DIM_BASEUOMID
,DIM_SALESUOMID
,AMT_UNITPRICEUOM
,DIM_DATEIDFIXEDVALUE
,DIM_PAYMENTTERMSID
,DIM_DATEIDNETDUEDATE
,DIM_MATERIALPRICEGROUP4ID
,DIM_MATERIALPRICEGROUP5ID
,AMT_SUBTOTAL3_ORDERQTY
,AMT_SUBTOTAL3INCUSTCONFIG_BILLING
,DIM_CUSTOMERMASTERSALESID
,DD_SALESORDERBLOCKED
,DD_SOCREATETIME
,DD_REQDELIVERYTIME
,DD_SOLINECREATETIME
,DD_DELIVERYTIME
,DD_PLANNEDGITIME
,DIM_DATEIDARPOSTING
,DIM_CURRENCYID_TRA
,DIM_CURRENCYID_GBL
,DIM_CURRENCYID_STAT
,AMT_EXCHANGERATE_STAT
,DD_ARUNNUMBER
,DIM_AVAILABILITYCHECKID
,DD_IDOCNUMBER
,DIM_INCOTERMID
,DD_INTERNATIONALARTICLENO
,DD_RELEASERULE
,DIM_DELIVERYPRIORITYID
,DD_REFERENCEDOCITEMNO
,DIM_SALESORDERPARTNERID
,DD_PURCHASEREQUISITION
,DD_ITEMOFREQUISITION
,CT_AFSCALCULATEDOPENQTY
,AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE
,DD_DOCUMENTCONDITIONNO
,AMT_SUBTOTAL1
,AMT_SUBTOTAL2
,AMT_SUBTOTAL5
,AMT_SUBTOTAL6
,DD_HIGHLEVELITEM
,DD_PODOCUMENTNO
,DD_PODOCUMENTITEMNO
,DD_POSCHEDULENO
,DD_PRODORDERNO
,DD_PRODORDERITEMNO
,DIM_CUSTOMERCONDITIONGROUPS1ID
,DIM_CUSTOMERCONDITIONGROUPS2ID
,DIM_CUSTOMERCONDITIONGROUPS3ID
,CT_AFSOPENPOQTY
,CT_AFSINTRANSITQTY
,DIM_SCHEDULEDELIVERYBLOCKID
,DIM_CUSTOMERGROUP4ID
,DD_OPENCONFIRMATIONSEXISTS
,DD_CONDITIONNO
,DD_CREDITMGR
,DD_CLEAREDBLOCKEDSTS
,CT_AFSINRDDUNITS
,CT_AFSAFTERRDDUNITS
,DIM_AGREEMENTSID
,DIM_CHARGEBACKCONTRACTID
,DD_RO_MATERIALDESC
,DIM_CUSTOMERIDCBOWNER
,CT_COMMITTEDQTY
,DIM_DATEIDREQDELIVLINE
,DIM_MATERIALPRICINGGROUPID
,DIM_BILLINGBLOCKSALESDOCLVLID
,DD_DELIVERYINDICATOR
,DD_SHIPTOPARTYSTREET
,DIM_CUSTOMERCONDITIONGROUPS4ID
,DIM_CUSTOMERCONDITIONGROUPS5ID
,DD_PURCHASEORDERITEM
,DIM_MATERIALPRICEGROUP2ID
,DIM_PROJECTSOURCEID
,DD_BILLING_NO
,DIM_MDG_PARTID
,DIM_BWPRODUCTHIERARCHYID
,CT_FIRSTNONZEROCONFIRMQTY
,CT_VOLUME
,CT_NOTCONFIRMEDQTY
,DD_CUSTOMCUSTEXPPRICEINC
,DD_DOCUMENTFLOWGROUP
,DD_ORDERBY
,DD_CUSTOMPRICEMISSINGINC
,DIM_CUSTOMERENDUSERFORFTRADE
,DIM_DATEIDSNAPSHOT
,dim_accordinggidatemto_emd
,dim_dateidexpectedship_emd
,dim_dateidrequested_emd
,ct_baseuomratio
,ct_baseuomratiokg
,ct_countsalesdocitem
,ct_deliveryisfull
,ct_deliveryontime
,ct_deliveryontimecasual
,dd_ace_openorders
,dd_backorderflag
,dd_backorderreason
,dd_backorder_responsibility
,dd_deliveryisfull
,dd_deliveryontime
,dd_deliveryontimecasual
,dd_intercompflag
,dd_lsintercompflag
,dd_mercklsbackorderfilter
,dd_ora_active_hold_info
,dd_ora_attribute11
,dd_ora_line_on_hold
,dd_ora_tradsales_flag
,dim_backorder_dateid
,dim_commercialviewid
,dim_dateidactualgimerckls
,dim_dateidconfirmeddelivery_emd
,AMT_EXCHANGERATE_CUSTOM
,DD_OPENQTYCVRDBYMAT
,DD_OPENQTYCVRDBYPLANT
,DIM_CLUSTERID
,DW_INSERT_DATE
,DW_UPDATE_DATE
,dim_countryhierpsid
,dim_countryhierarid
,dim_gsamapimportid
,dim_date_enddateloadingid
,dim_customer_shipto
,dim_productionschedulerid
,dd_inco1
,dd_inco2
,dd_ship_to_party
,dd_mtomts
,DIM_ADDRESSID
,dd_CompleteInSingleDelivery
,dd_batch_split
,dd_delivery_group
)
select
(select ifnull(max(m.max_id), 1) from number_fountain m where m.table_name = 'fact_salesorder_msnap')
          + row_number()
             over(order by '') fact_Salesorder_msnapID
,ifnull(f.DD_SALESDOCNO,'Not Set')
,ifnull(f.DD_SALESITEMNO,0)
,ifnull(f.DD_SCHEDULENO,0)
,ifnull(f.CT_SCHEDULEQTYSALESUNIT,0)
,ifnull(f.CT_CONFIRMEDQTY,0)
,ifnull(f.CT_CORRECTEDQTY,0)
,ifnull(f.CT_DELIVEREDQTY,0)
,ifnull(f.CT_ONHANDCOVEREDQTY,0)
,ifnull(f.AMT_UNITPRICE,0)
,ifnull(f.CT_PRICEUNIT,1)
,ifnull(f.AMT_SCHEDULETOTAL,0)
,ifnull(f.AMT_STDCOST,0)
,ifnull(f.AMT_TARGETVALUE,0)
,ifnull(f.CT_TARGETQTY,0)
,ifnull(f.AMT_EXCHANGERATE,1)
,ifnull(f.CT_OVERDLVRTOLERANCE,0)
,ifnull(f.CT_UNDERDLVRTOLERANCE,0)
,ifnull(f.DIM_DATEIDSALESORDERCREATED,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERYREQ,1)
,ifnull(f.DIM_DATEIDSCHEDDLVRREQPREV,1)
,ifnull(f.DIM_DATEIDSCHEDDELIVERY,1)
,ifnull(f.DIM_DATEIDGOODSISSUE,1)
,ifnull(f.DIM_DATEIDMTRLAVAIL,1)
,ifnull(f.DIM_DATEIDLOADING,1)
,ifnull(f.DIM_DATEIDTRANSPORT,1)
,ifnull(f.DIM_CURRENCYID,1)
,ifnull(f.DIM_PRODUCTHIERARCHYID,1)
,ifnull(f.DIM_PLANTID,1)
,ifnull(f.DIM_COMPANYID,1)
,ifnull(f.DIM_STORAGELOCATIONID,1)
,ifnull(f.DIM_SALESDIVISIONID,1)
,ifnull(f.DIM_SHIPRECEIVEPOINTID,1)
,ifnull(f.DIM_DOCUMENTCATEGORYID,1)
,ifnull(f.DIM_SALESDOCUMENTTYPEID,1)
,ifnull(f.DIM_SALESORGID,1)
,ifnull(f.DIM_CUSTOMERID,1)
,ifnull(f.DIM_DATEIDVALIDFROM,1)
,ifnull(f.DIM_DATEIDVALIDTO,1)
,ifnull(f.DIM_SALESGROUPID,0)
,ifnull(f.DIM_COSTCENTERID,1)
,ifnull(f.DIM_CONTROLLINGAREAID,0)
,ifnull(f.DIM_BILLINGBLOCKID,0)
,ifnull(f.DIM_TRANSACTIONGROUPID,0)
,ifnull(f.DIM_SALESORDERREJECTREASONID,0)
,ifnull(f.DIM_PARTID,1)
,ifnull(f.DIM_PARTSALESID,1)
,ifnull(f.DIM_SALESORDERHEADERSTATUSID,0)
,ifnull(f.DIM_SALESORDERITEMSTATUSID,0)
,ifnull(f.DIM_CUSTOMERGROUP1ID,0)
,ifnull(f.DIM_CUSTOMERGROUP2ID,0)
,ifnull(f.DIM_SALESORDERITEMCATEGORYID,1)
,ifnull(f.AMT_EXCHANGERATE_GBL,1)
,ifnull(f.CT_FILLQTY,0)
,ifnull(f.CT_ONHANDQTY,0)
,ifnull(f.DIM_DATEIDFIRSTDATE,1)
,ifnull(f.DIM_SCHEDULELINECATEGORYID,1)
,ifnull(f.DIM_SALESMISCID,1)
,ifnull(f.DD_ITEMRELFORDELV,'Not Set')
,ifnull(f.DIM_DATEIDSHIPMENTDELIVERY,1)
,ifnull(f.DIM_DATEIDACTUALGI,1)
,ifnull(f.DIM_DATEIDSHIPDLVRFILL,1)
,ifnull(f.DIM_DATEIDACTUALGIFILL,1)
,ifnull(f.DIM_PROFITCENTERID,1)
,ifnull(f.DIM_CUSTOMERIDSHIPTO,1)
,ifnull(f.DIM_DATEIDGUARANTEEDATE,1)
,ifnull(f.DIM_UNITOFMEASUREID,1)
,ifnull(f.DIM_DISTRIBUTIONCHANNELID,1)
,ifnull(f.DD_BATCHNO,'Not Set')
,ifnull(f.DD_CREATEDBY,'Not Set')
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID,1)
,ifnull(f.DIM_DATEIDLOADINGFILL,1)
,ifnull(f.DIM_PAYERPARTNERFUNCTIONID,1)
,ifnull(f.DIM_BILLTOPARTYPARTNERFUNCTIONID,1)
,ifnull(f.CT_SHIPPEDAGNSTORDERQTY,0)
,ifnull(f.CT_CMLQTYRECEIVED,0)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID1,1)
,ifnull(f.DIM_CUSTOMPARTNERFUNCTIONID2,1)
,ifnull(f.DIM_CUSTOMERGROUPID,1)
,ifnull(f.DIM_SALESOFFICEID,1)
,ifnull(f.DIM_HIGHERCUSTOMERID,1)
,ifnull(f.DIM_HIGHERSALESORGID,1)
,ifnull(f.DIM_HIGHERDISTCHANNELID,1)
,ifnull(f.DIM_HIGHERSALESDIVID,1)
,ifnull(f.DD_SOLDTOHIERARCHYLEVEL,0)
,ifnull(f.DD_HIERARCHYTYPE,'Not Set')
,ifnull(f.DIM_TOPLEVELCUSTOMERID,1)
,ifnull(f.DIM_TOPLEVELDISTCHANNELID,1)
,ifnull(f.DIM_TOPLEVELSALESDIVID,1)
,ifnull(f.DIM_TOPLEVELSALESORGID,1)
,ifnull(f.CT_AFSUNALLOCATEDQTY,0)
,ifnull(f.AMT_AFSUNALLOCATEDVALUE,0)
,ifnull(f.CT_AFSALLOCATIONRQTY,0)
,ifnull(f.CT_CUMCONFIRMEDQTY,0)
,ifnull(f.CT_AFSALLOCATIONFQTY,0)
,ifnull(f.CT_CUMORDERQTY,0)
,ifnull(f.AMT_AFSONDELIVERYVALUE,0)
,ifnull(f.CT_SHIPPEDORBILLEDQTY,0)
,ifnull(f.AMT_SHIPPEDORBILLED,0)
,ifnull(f.DIM_CREDITREPRESENTATIVEID,1)
,ifnull(f.DD_CUSTOMERPONO,'Not Set')
,ifnull(f.CT_INCOMPLETEQTY,0)
,ifnull(f.AMT_INCOMPLETE,0)
,ifnull(f.DIM_DELIVERYBLOCKID,1)
,ifnull(f.DD_AFSSTOCKCATEGORY,'Not Set')
,ifnull(f.DD_AFSSTOCKTYPE,'Not Set')
,ifnull(f.DIM_DATEIDAFSCANCELDATE,1)
,ifnull(f.AMT_AFSNETPRICE,0)
,ifnull(f.AMT_AFSNETVALUE,0)
,ifnull(f.AMT_BASEPRICE,0)
,ifnull(f.DIM_MATERIALPRICEGROUP1ID,1)
,ifnull(f.AMT_CREDITHOLDVALUE,0)
,ifnull(f.AMT_DELIVERYBLOCKVALUE,0)
,ifnull(f.DIM_AFSSIZEID,1)
,ifnull(f.DIM_AFSREJECTIONREASONID,1)
,ifnull(f.DIM_DATEIDDLVRDOCCREATED,1)
,ifnull(f.AMT_CUSTOMEREXPECTEDPRICE,0)
,ifnull(f.CT_AFSALLOCATEDQTY,0)
,ifnull(f.CT_AFSONDELIVERYQTY,0)
,ifnull(f.DIM_OVERALLSTATUSCREDITCHECKID,1)
,ifnull(f.DIM_SALESDISTRICTID,1)
,ifnull(f.DIM_ACCOUNTASSIGNMENTGROUPID,1)
,ifnull(f.DIM_MATERIALGROUPID,1)
,ifnull(f.DIM_SALESDOCORDERREASONID,1)
,ifnull(f.AMT_SUBTOTAL3,0)
,ifnull(f.AMT_SUBTOTAL4,0)
,ifnull(f.DIM_DATEIDAFSREQDELIVERY,1)
,ifnull(f.AMT_DICOUNTACCRUALNETPRICE,0)
,ifnull(f.CT_AFSOPENQTY,0)
,ifnull(f.DIM_PURCHASEORDERTYPEID,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDFROM,1)
,ifnull(f.DIM_DATEIDPURCHASEORDER,1)
,ifnull(f.DIM_DATEIDQUOTATIONVALIDTO,1)
,ifnull(f.DIM_AFSSEASONID,1)
,ifnull(f.DD_AFSDEPARTMENT,'Not Set')
,ifnull(f.DIM_DATEIDSOCREATED,1)
,ifnull(f.DIM_DATEIDSODOCUMENT,1)
,ifnull(f.DD_SUBSEQUENTDOCNO,'Not Set')
,ifnull(f.DD_SUBSDOCITEMNO,0)
,ifnull(f.DD_SUBSSCHEDULENO,0)
,ifnull(f.DIM_SUBSDOCCATEGORYID,1)
,ifnull(f.CT_AFSTOTALDRAWN,0)
,ifnull(f.DIM_DATEIDASLASTDATE,1)
,ifnull(f.DD_REFERENCEDOCUMENTNO,'Not Set')
,ifnull(f.DD_REQUIREMENTTYPE,'Not Set')
,ifnull(f.AMT_STDPRICE,0)
,ifnull(f.DIM_DATEIDNEXTDATE,1)
,ifnull(f.AMT_TAX,0)
,ifnull(f.DIM_DATEIDEARLIESTSOCANCELDATE,1)
,ifnull(f.DIM_DATEIDLATESTCUSTPOAGI,1)
,ifnull(f.DD_BUSINESSCUSTOMERPONO,'Not Set')
,ifnull(f.DIM_ROUTEID,1)
,ifnull(f.DIM_BILLINGDATEID,1)
,ifnull(f.DIM_CUSTOMERPAYMENTTERMSID,1)
,ifnull(f.DIM_SALESRISKCATEGORYID,1)
,ifnull(f.DD_CREDITREP,'Not Set')
,ifnull(f.DD_CREDITLIMIT,0)
,ifnull(f.DIM_CUSTOMERRISKCATEGORYID,1)
,ifnull(f.CT_FILLQTY_CRD,0)
,ifnull(f.CT_FILLQTY_PDD,0)
,ifnull(f.DIM_H1CUSTOMERID,1)
,ifnull(f.DIM_DATEIDSOITEMCHANGEDON,1)
,ifnull(f.DIM_SHIPPINGCONDITIONID,1)
,ifnull(f.DD_AFSALLOCATIONGROUPNO,'Not Set')
,ifnull(f.DIM_DATEIDREJECTION,1)
,ifnull(f.DD_CUSTOMERMATERIALNO,'Not Set')
,ifnull(f.DIM_BASEUOMID,1)
,ifnull(f.DIM_SALESUOMID,1)
,ifnull(f.AMT_UNITPRICEUOM,0)
,ifnull(f.DIM_DATEIDFIXEDVALUE,1)
,ifnull(f.DIM_PAYMENTTERMSID,1)
,ifnull(f.DIM_DATEIDNETDUEDATE,1)
,ifnull(f.DIM_MATERIALPRICEGROUP4ID,1)
,ifnull(f.DIM_MATERIALPRICEGROUP5ID,1)
,ifnull(f.AMT_SUBTOTAL3_ORDERQTY,0)
,ifnull(f.AMT_SUBTOTAL3INCUSTCONFIG_BILLING,0)
,ifnull(f.DIM_CUSTOMERMASTERSALESID,1)
,ifnull(f.DD_SALESORDERBLOCKED,'Not Set')
,ifnull(f.DD_SOCREATETIME,'Not Set')
,ifnull(f.DD_REQDELIVERYTIME,'Not Set')
,ifnull(f.DD_SOLINECREATETIME,'Not Set')
,ifnull(f.DD_DELIVERYTIME,'Not Set')
,ifnull(f.DD_PLANNEDGITIME,'Not Set')
,ifnull(f.DIM_DATEIDARPOSTING,1)
,ifnull(f.DIM_CURRENCYID_TRA,1)
,ifnull(f.DIM_CURRENCYID_GBL,1)
,ifnull(f.DIM_CURRENCYID_STAT,1)
,ifnull(f.AMT_EXCHANGERATE_STAT,1)
,ifnull(f.DD_ARUNNUMBER,'Not Set')
,ifnull(f.DIM_AVAILABILITYCHECKID,1)
,ifnull(f.DD_IDOCNUMBER,'Not Set')
,ifnull(f.DIM_INCOTERMID,1)
,ifnull(f.DD_INTERNATIONALARTICLENO,'Not Set')
,ifnull(f.DD_RELEASERULE,'Not Set')
,ifnull(f.DIM_DELIVERYPRIORITYID,1)
,ifnull(f.DD_REFERENCEDOCITEMNO,0)
,ifnull(f.DIM_SALESORDERPARTNERID,1)
,ifnull(f.DD_PURCHASEREQUISITION,'Not Set')
,ifnull(f.DD_ITEMOFREQUISITION,0)
,ifnull(f.CT_AFSCALCULATEDOPENQTY,0)
,ifnull(f.AMT_BILLINGCUSTCONFIGSUBTOTAL3UNITPRICE,0)
,ifnull(f.DD_DOCUMENTCONDITIONNO,'Not Set')
,ifnull(f.AMT_SUBTOTAL1,0)
,ifnull(f.AMT_SUBTOTAL2,0)
,ifnull(f.AMT_SUBTOTAL5,0)
,ifnull(f.AMT_SUBTOTAL6,0)
,ifnull(f.DD_HIGHLEVELITEM,0)
,ifnull(f.DD_PODOCUMENTNO,'Not Set')
,ifnull(f.DD_PODOCUMENTITEMNO,0)
,ifnull(f.DD_POSCHEDULENO,0)
,ifnull(f.DD_PRODORDERNO,'Not Set')
,ifnull(f.DD_PRODORDERITEMNO,0)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS1ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS2ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS3ID,1)
,ifnull(f.CT_AFSOPENPOQTY,0)
,ifnull(f.CT_AFSINTRANSITQTY,0)
,ifnull(f.DIM_SCHEDULEDELIVERYBLOCKID,1)
,ifnull(f.DIM_CUSTOMERGROUP4ID,1)
,ifnull(f.DD_OPENCONFIRMATIONSEXISTS,'Not Set')
,ifnull(f.DD_CONDITIONNO,'Not Set')
,ifnull(f.DD_CREDITMGR,'Not Set')
,ifnull(f.DD_CLEAREDBLOCKEDSTS,'Not Set')
,ifnull(f.CT_AFSINRDDUNITS,0)
,ifnull(f.CT_AFSAFTERRDDUNITS,0)
,ifnull(f.DIM_AGREEMENTSID,1)
,ifnull(f.DIM_CHARGEBACKCONTRACTID,1)
,ifnull(f.DD_RO_MATERIALDESC,'Not Set')
,ifnull(f.DIM_CUSTOMERIDCBOWNER,1)
,ifnull(f.CT_COMMITTEDQTY,0)
,ifnull(f.DIM_DATEIDREQDELIVLINE,1)
,ifnull(f.DIM_MATERIALPRICINGGROUPID,1)
,ifnull(f.DIM_BILLINGBLOCKSALESDOCLVLID,1)
,ifnull(f.DD_DELIVERYINDICATOR,'Not Set')
,ifnull(f.DD_SHIPTOPARTYSTREET,'Not Set')
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS4ID,1)
,ifnull(f.DIM_CUSTOMERCONDITIONGROUPS5ID,1)
,ifnull(f.DD_PURCHASEORDERITEM,'0')
,ifnull(f.DIM_MATERIALPRICEGROUP2ID,1)
,ifnull(f.DIM_PROJECTSOURCEID,1)
,ifnull(f.DD_BILLING_NO,'Not Set')
,ifnull(f.DIM_MDG_PARTID,1)
,ifnull(f.DIM_BWPRODUCTHIERARCHYID,1)
,ifnull(f.CT_FIRSTNONZEROCONFIRMQTY,0)
,ifnull(f.CT_VOLUME,0)
,ifnull(f.CT_NOTCONFIRMEDQTY,0)
,ifnull(f.DD_CUSTOMCUSTEXPPRICEINC,'Not Set')
,ifnull(f.DD_DOCUMENTFLOWGROUP,'Not Set')
,ifnull(f.DD_ORDERBY,'Not Set')
,ifnull(f.DD_CUSTOMPRICEMISSINGINC,'Not Set')
,ifnull(f.DIM_CUSTOMERENDUSERFORFTRADE,1)
,ifnull((select dim_dateid from dim_date_factory_calendar where companycode = 'Not Set' and plantcode_Factory = 'Not Set' and datevalue = (select processing_date from tmp_dayofprocessing_var_msnap)),1) DIM_DATEIDSNAPSHOT
,ifnull(dim_accordinggidatemto_emd,1)
,ifnull(dim_dateidexpectedship_emd,1)
,ifnull(dim_dateidrequested_emd,1)
,ifnull(ct_baseuomratio,0)
,ifnull(ct_baseuomratiokg,0)
,ifnull(ct_countsalesdocitem,0)
,ct_deliveryisfull
,ct_deliveryontime
,ct_deliveryontimecasual
,dd_ace_openorders
,ifnull(dd_backorderflag,'Not Set')
,dd_backorderreason
,dd_backorder_responsibility
,dd_deliveryisfull
,dd_deliveryontime
,dd_deliveryontimecasual
,dd_intercompflag
,dd_lsintercompflag
,dd_mercklsbackorderfilter
,ifnull(dd_ora_active_hold_info,0)
,ifnull(dd_ora_attribute11,0)
,ifnull(dd_ora_line_on_hold,0)
,ifnull(dd_ora_tradsales_flag,0)
,ifnull(dim_backorder_dateid,1)
,dim_commercialviewid
,dim_dateidactualgimerckls
,dim_dateidconfirmeddelivery_emd
,AMT_EXCHANGERATE_CUSTOM
,DD_OPENQTYCVRDBYMAT
,DD_OPENQTYCVRDBYPLANT
,ifnull(dim_clusterid,1)
,F.DW_INSERT_DATE
,F.DW_UPDATE_DATE
,ifnull(f.dim_countryhierpsid,1)
,ifnull(f.dim_countryhierarid,1)
,ifnull(f.dim_gsamapimportid,1)
,ifnull(f.dim_date_enddateloadingid,1)
,ifnull(f.dim_customer_shipto,1)
,ifnull(f.dim_productionschedulerid,1)
,ifnull(f.dd_inco1,'Not Set')
,ifnull(f.dd_inco2,'Not Set')
,ifnull(f.dd_ship_to_party,'Not Set')
,ifnull(prt.mtomts,'Not Set')
,IFNULL(F.DIM_ADDRESSID,1)
,ifnull(f.dd_CompleteInSingleDelivery,'Not Set')
,ifnull(f.dd_batch_split,'Not Set')
,ifnull(f.dd_delivery_group,'Not Set')
from fact_salesorder f
inner join dim_plant pl on f.dim_plantid = pl.dim_plantid and pl.plantcode <>'CH73'/* @Catalin Exclude CH73 plant as requested by client BI-5280*/
inner join dim_part prt on f.dim_partid = prt.dim_partid
inner join dim_bwproducthierarchy uph on f.dim_bwproducthierarchyid = uph.dim_bwproducthierarchyid
inner join dim_date_factory_calendar mlsrd on (
CASE
  WHEN UPPER(uph.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN decode(f.dim_dateidexpectedship_emd,1,f.dim_dateidrequested_emd,f.dim_dateidexpectedship_emd)
  ELSE CASE WHEN TRIM(prt.mtomts) = 'MTO' THEN decode(f.dim_accordinggidatemto_emd,1,f.dim_dateidconfirmeddelivery_emd,f.dim_accordinggidatemto_emd)
            ELSE decode(f.dim_dateidexpectedship_emd,1,f.dim_dateidrequested_emd,f.dim_dateidexpectedship_emd)
            END
END) = mlsrd.dim_dateid
inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
inner join dim_salesorderitemstatus its on f.dim_salesorderitemstatusid = its.dim_salesorderitemstatusid
where
to_Date(mlsrd.datevalue) >= case when current_date = TRUNC (SYSDATE, 'month') then TRUNC (SYSDATE, 'month') - interval '1' month
																								else TRUNC (SYSDATE, 'month')
																								end
and TO_DATE(mlsrd.datevalue) <= case when current_date = TRUNC (SYSDATE, 'month') then TRUNC (SYSDATE, 'month') - interval '1' day
															 else (TRUNC (SYSDATE, 'month') + interval '1' month) - interval '1' day
															 end
 AND mlsrd.datevalue  <> '0001-01-01'
     AND f.DD_ACE_OPENORDERS = 'X'
     AND uph.businesssector = 'BS-02'
     AND concat(f.dd_salesdocno,f.dd_salesitemno) not in (select distinct concat(dd_salesdocno,dd_salesitemno) from fact_salesorder_msnap f,dim_date dd
where f.dim_dateidsnapshot=dd.dim_dateid
and dd.datevalue=dd.monthenddate)
     AND f.dd_scheduleno=dd_min_scheduleno;

/*Disabled as requested Roxana D 2017-10-27
update fact_salesorder_msnap   f
set f.dim_clusterid = dc.dim_clusterid
from fact_salesorder_msnap   f, dim_mdg_part mdg, dim_bwproducthierarchy ph, dim_cluster dc
where f.dim_mdg_partid = mdg.dim_mdg_partid
	and f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
	and businesssector = 'BS-02'
	and mdg.primary_production_location = dc.primary_manufacturing_site
	and f.dim_clusterid <> dc.dim_clusterid

update emd586.fact_salesorder_msnap f
set f.dim_clusterid = ff.dim_clusterid
from emd586.fact_salesorder_msnap f,EMDTempoLA57F.fact_salesorder_msnap ff
where f.fact_salesorder_msnapid = ff.fact_salesorder_msnapid
	and f.dim_projectsourceid=2
	and f.dim_clusterid <> ff.dim_clusterid
	*/


/*Add dd_primaryproductionlocation Roxana D 2017-11-01*/


update FACT_SALESORDER_MSNAP f
set f.DD_PRIMARYPRODUCTIONLOCATION = mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
FROM  FACT_SALESORDER_MSNAP f
inner join  dim_cluster dc on dc.dim_clusterid = f.dim_clusterid
inner join dim_bwproducthierarchy ph on f.dim_bwproducthierarchyid = ph.dim_bwproducthierarchyid
   and businesssector = 'BS-02'
inner join (select distinct primary_production_location,primary_production_location_name  
from dim_mdg_part ) mdg on mdg.primary_production_location = dc.primary_manufacturing_site
where f.DD_PRIMARYPRODUCTIONLOCATION <> mdg.primary_production_location || ' - ' || mdg.primary_production_location_name
and f.dim_dateidsnapshot in (select dim_dateid from tmp_loaded_datevalues_msnap );

/*End*/

	drop table if exists tmp_valueallschedules_msnap;
	create table tmp_valueallschedules_msnap as
	select ifnull(DD_SALESDOCNO, 'Not Set')DD_SALESDOCNO,
		   ifnull(DD_SALESITEMNO, 'Not Set')DD_SALESITEMNO,

	sum(ct_ScheduleQtySalesUnit)  ct_ScheduleQtySalesUnit
	from
			fact_salesorder f
	group by DD_SALESDOCNO,DD_SALESITEMNO;

	update fact_salesorder_msnap f
	set f.ct_ScheduleQtySalesUnit = t.ct_ScheduleQtySalesUnit

	from fact_salesorder_msnap f, tmp_valueallschedules_msnap t
	where f.DD_SALESDOCNO = t.DD_SALESDOCNO
	and f.DD_SALESITEMNO = t.DD_SALESITEMNO
	and f.ct_ScheduleQtySalesUnit <> t.ct_ScheduleQtySalesUnit;

	drop table if exists update_MerckLSActualGoodsIssueDate_msnap_1;
	create table update_MerckLSActualGoodsIssueDate_msnap_1
	as select distinct f.dd_salesdocno,f.dd_salesitemno,max(d.datevalue) datevalue
	from fact_salesorder f,fact_salesorder_msnap ff,dim_date d
	where f.dim_dateidactualgimerckls=d.dim_dateid
	and f.dd_salesdocno=ff.dd_salesdocno
	and f.dd_salesitemno=ff.dd_salesitemno
	and f.dim_dateidactualgimerckls<>1
	group by f.dd_salesdocno,f.dd_salesitemno;

	drop table if exists update_MerckLSActualGoodsIssueDate_msnap;
	create table update_MerckLSActualGoodsIssueDate_msnap
	as select distinct f.dd_salesdocno,f.dd_salesitemno,f.dim_dateidactualgimerckls
	from fact_salesorder f,fact_salesorder_msnap ff,dim_date d,update_MerckLSActualGoodsIssueDate_msnap_1 dd
	where f.dim_dateidactualgimerckls=d.dim_dateid
	and f.dd_salesdocno=ff.dd_salesdocno
	and f.dd_salesitemno=ff.dd_salesitemno
	and f.dd_salesdocno=dd.dd_salesdocno
	and f.dd_salesitemno=dd.dd_salesitemno
	and f.dim_dateidactualgimerckls<>1
	and d.datevalue=dd.datevalue
	group by f.dd_salesdocno,f.dd_salesitemno,f.dim_dateidactualgimerckls;

	update fact_salesorder_msnap f
	set f.dim_dateidactualgimerckls=t.dim_dateidactualgimerckls
	from fact_salesorder_msnap f,update_MerckLSActualGoodsIssueDate_msnap t,dim_date d
	where f.dd_salesdocno=t.dd_salesdocno
	and f.dd_salesitemno=t.dd_salesitemno
	and f.dim_dateidsnapshot=d.dim_dateid
	and f.dim_dateidactualgimerckls=1;

	drop table if exists update_MerckLSActualGoodsIssueDate_msnap_1;
	create table update_MerckLSActualGoodsIssueDate_msnap_1
	as select distinct f.dd_salesdocno,f.dd_salesitemno,max(d.datevalue) datevalue
	from fact_salesorder f,fact_salesorder_msnap ff,dim_date d
	where f.dim_dateidactualgi=d.dim_dateid
	and f.dd_salesdocno=ff.dd_salesdocno
	and f.dd_salesitemno=ff.dd_salesitemno
	and f.dim_dateidactualgi<>1
	group by f.dd_salesdocno,f.dd_salesitemno;

	drop table if exists update_MerckLSActualGoodsIssueDate_msnap;
	create table update_MerckLSActualGoodsIssueDate_msnap
	as select distinct f.dd_salesdocno,f.dd_salesitemno,f.dim_dateidactualgi
	from fact_salesorder f,fact_salesorder_msnap ff,dim_date d,update_MerckLSActualGoodsIssueDate_msnap_1 dd
	where f.dim_dateidactualgi=d.dim_dateid
	and f.dd_salesdocno=ff.dd_salesdocno
	and f.dd_salesitemno=ff.dd_salesitemno
	and f.dd_salesdocno=dd.dd_salesdocno
	and f.dd_salesitemno=dd.dd_salesitemno
	and f.dim_dateidactualgi<>1
	and d.datevalue=dd.datevalue
	group by f.dd_salesdocno,f.dd_salesitemno,f.dim_dateidactualgi;


	update fact_salesorder_msnap f
	set f.dim_dateidactualgi=t.dim_dateidactualgi
	from fact_salesorder_msnap f,update_MerckLSActualGoodsIssueDate_msnap t
	where f.dd_salesdocno=t.dd_salesdocno
	and f.dd_salesitemno=t.dd_salesitemno
	and f.dim_dateidactualgi=1;

	drop table if exists tmp_valueallschedules_msnap_DeliveryQty;
	create table tmp_valueallschedules_msnap_DeliveryQty as
	select ifnull(DD_SALESDOCNO, 'Not Set')DD_SALESDOCNO,
		   ifnull(DD_SALESITEMNO, 'Not Set')DD_SALESITEMNO,

	sum(ct_DeliveredQty) ct_DeliveredQty
	from
			fact_salesorder f
	group by DD_SALESDOCNO,DD_SALESITEMNO;


	update fact_salesorder_msnap f
	set f.ct_DeliveredQty = t.ct_DeliveredQty
	from fact_salesorder_msnap f, tmp_valueallschedules_msnap_DeliveryQty t
	where f.DD_SALESDOCNO = t.DD_SALESDOCNO
	and f.DD_SALESITEMNO = t.DD_SALESITEMNO
	and f.ct_DeliveredQty <> t.ct_DeliveredQty;

	update fact_salesorder_msnap f
	set f.ct_deliveryisfull = (case when f.dd_deliveryisfull = 'X' then 1 else 0 end ),
	f.ct_deliveryontime = (case when f.dd_deliveryontime = 'X' then 1 else 0 end ),
	f.ct_deliveryontimecasual = (case when f.dd_deliveryontimecasual = 'X' then 1 else 0 end )
	from fact_salesorder_msnap f,dim_date d
	where f.dim_dateidsnapshot=d.dim_dateid
	and datevalue >= '2017-04-14';


	update fact_salesorder_msnap f_so
set dim_mercklsconforreqdateid = mlsrd.dim_dateid
from fact_salesorder_msnap f_so,dim_date_factory_calendar mlsrd, dim_part prt, dim_bwproducthierarchy bw
where f_so.Dim_Partid = prt.Dim_Partid
and f_so.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
and
CASE WHEN UPPER(bw.businessdesc) LIKE '%RESEARCH SOLUTION%' THEN f_so.dim_dateidexpectedship_emd ELSE
  CASE WHEN trim(prt.mtomts) = 'MTO' THEN f_so.dim_accordinggidatemto_emd
      ELSE f_so.dim_dateidexpectedship_emd END END=mlsrd.dim_dateid
and dim_mercklsconforreqdateid <> mlsrd.dim_dateid;

update fact_salesorder_msnap f
set f.dd_CustomerPONo=ff.dd_CustomerPONo
from fact_salesorder_msnap f,fact_salesorder ff
where f.dd_salesdocno=ff.dd_salesdocno and f.dd_salesitemno=ff.dd_salesitemno
and f.dd_scheduleno=ff.dd_scheduleno
and f.dd_CustomerPONo='Not Set'
and f.dd_CustomerPONo<>ff.dd_CustomerPONo;

update emd586.fact_salesorder_msnap f
set  f.dd_CustomerPONo=ff.dd_CustomerPONo
from emd586.fact_salesorder_msnap f,fact_salesorder_msnap ff
where f.fact_salesorder_msnapid=ff.fact_salesorder_msnapid
and f.dd_salesdocno=ff.dd_salesdocno and to_char(f.dd_salesitemno)=to_char(ff.dd_salesitemno)
and f.dd_scheduleno=ff.dd_scheduleno and f.dim_dateidsnapshot=ff.dim_dateidsnapshot
and f.dd_CustomerPONo='Not Set'
and f.dd_CustomerPONo<>ff.dd_CustomerPONo;


drop table if exists tmp_dayofprocessing_var_msnap;
drop table if exists tmp_loaded_datevalues_msnap;

/* 2017-10-10 Fix ShipToHeader  Roxana D*/

drop table if exists tmp_msnap;
create table tmp_msnap as 
select max(dim_customer_shipto) as dim_customer_shipto ,dd_salesdocno, dim_projectsourceid
from fact_salesorder
group by dd_salesdocno, dim_projectsourceid;

update fact_salesorder_msnap f
set f.dim_customer_shipto = s.dim_customer_shipto
from fact_salesorder_msnap f, tmp_msnap s
where f.dd_salesdocno = s.dd_salesdocno
	and f.dim_projectsourceid = s.dim_projectsourceid
 	and f.dim_customer_shipto = 1;

update emd586.fact_salesorder_msnap f
set f.dim_customer_shipto = s.dim_customer_shipto
from emd586.fact_salesorder_msnap f, tmp_msnap s
where f.dd_salesdocno = s.dd_salesdocno
	and f.dim_projectsourceid = s.dim_projectsourceid
	and f.dim_customer_shipto = 1;


drop table if exists tmp_msnap;

/*End Of ShipTo Header */


/*2017-12-13 Add dimension for BIFILTERS_PRE_PRD Roxana D*/


update fact_salesorder_MSNAP f
set f.dim_BIFILTERS_PRE_PRDId = ifnull(d.DIM_BIFILTERS_PRE_PRDid, 1)
from fact_salesorder_MSNAP f, DIM_BIFILTERS_PRE_PRD d
where f.dim_projectsourceid = d.dim_projectsourceid
and case when f.DD_BIFILTERS_PRE_PRD_NEW = 'X' then 'Yes' else 'No' end = TRIM(d.BIFILTERS_PRE_PRD_CODE)
AND f.dim_BIFILTERS_PRE_PRDId <> ifnull(d.DIM_BIFILTERS_PRE_PRDid, 1);


/**/