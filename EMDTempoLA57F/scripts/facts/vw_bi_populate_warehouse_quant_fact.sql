

drop table if exists fact_wmquant_tmp ;

CREATE TABLE fact_wmquant_tmp 
AS 
select * from fact_wmquant where 1 = 2;

/*initialize NUMBER_FOUNTAIN*/

delete from NUMBER_FOUNTAIN f where f.table_name = 'fact_wmquant';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_wmquant',ifnull(max(f.fact_wmquantid) , ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1)) 
FROM fact_wmquant f;


DROP TABLE IF EXISTS fact_wmquant_t1;
CREATE TABLE fact_wmquant_t1
AS
SELECT row_number() over (order by '') rid,
                 wn.dim_warehousenumberid as dim_warehousenumberid,
                 ifnull(st.LQUA_LQNUM, 'Not Set') as dd_quantno,
			     CONVERT(BIGINT, 1)  dim_partid,
			     pl.Dim_Plantid dim_plantid,
			     CONVERT(BIGINT, 1) dim_storagetypeid,  				   
			     CONVERT(BIGINT, 1) dim_storagebinid,                 
			     CONVERT(BIGINT, 1) dim_storagelocationid,
			     CONVERT(BIGINT, 1) dim_unitofmeasureid,
			     ifnull(st.LQUA_VERME, 0) as ct_availablestock,
			     ifnull(st.LQUA_CHARG,'Not Set') as dd_batchnumber,
			     CONVERT(BIGINT, 1) dim_wmstockcategoryid,
			     CONVERT(BIGINT, 1) dim_specialstockid,
			     ifnull(st.LQUA_SONUM,'Not Set') as dd_specialstockno,
			     CONVERT(BIGINT, 1) dim_wmblockingreasonid,
			     CONVERT(BIGINT, 1) dim_dateidlastmovement,
			     ifnull(st.LQUA_BTANR,'Not Set') as dd_lastchangetodocno,
			     ifnull(st.LQUA_BTAPS,0) as dd_lastchangetodocitemno,
			     CONVERT(BIGINT, 1) dim_dateidlaststockplacement,
			     CONVERT(BIGINT, 1) dim_dateidlaststockremoval,
			     CONVERT(BIGINT, 1) dim_dateidlaststockaddition,
			     CONVERT(BIGINT, 1) dim_dateidgoodsreceipt,
			ifnull(st.LQUA_WENUM,'Not Set') as dd_goodsreceiptno,
			ifnull(st.LQUA_WEPOS,0) as dd_goodsreceiptitemno,
			CONVERT(BIGINT, 1) dim_wmstorageunittypeid,
			ifnull(st.LQUA_GESME,0) as ct_totalqty,
			ifnull(st.LQUA_EINME,0) as ct_putawayqty,
			ifnull(st.LQUA_AUSME,0) as ct_removeqty,
			ifnull(st.LQUA_MGEWI,0) as ct_materialweight,
			CONVERT(BIGINT, 1) dim_weightunitid,
			ifnull(st.LQUA_TBNUM,'Not Set') as dd_transferreqno,
			ifnull(st.LQUA_IVPOS,0) as dd_inventorydocitemno,
			CONVERT(BIGINT, 1) dim_wmrequirementtypeid,
			ifnull(st.LQUA_BENUM,'Not Set') as dd_requirementno,
			ifnull(st.LQUA_LENUM,'Not Set') as dd_storageunitno,
			ifnull(st.LQUA_QPLOS,'Not Set') as dd_inspectionlotno,
			CONVERT(BIGINT, 1) dim_dateidbestbefore,
			ifnull(st.LQUA_QKAPV,0) as ct_capacityusage,
			CONVERT(BIGINT, 1) dim_wmpickingareaid,
			ifnull(st.LQUA_TRAME, 0) as ct_opentransferqty,
			ifnull(st.LQUA_VBELN,'Not Set') as dd_deliveryno,
			ifnull(st.LQUA_POSNR,0) as dd_deliveryitemno,
			CONVERT(BIGINT, 1) dim_dateidlastinventory,
			CONVERT(BIGINT, 1) Dim_WMQuantMiscId,
              st.LQUA_MATNR LQUA_MATNR,
              st.LQUA_WERKS LQUA_WERKS,
              ifnull(st.LQUA_LGNUM, 'Not Set')  LQUA_LGNUM,
              st.LQUA_LGPLA LQUA_LGPLA,
              st.LQUA_LGORT LQUA_LGORT,
              st.LQUA_MEINS LQUA_MEINS,
              st.LQUA_BESTQ LQUA_BESTQ,
              st.LQUA_SOBKZ LQUA_SOBKZ,
              st.LQUA_SPGRU LQUA_SPGRU,
              st.LQUA_BDATU LQUA_BDATU,
              st.LQUA_EDATU LQUA_EDATU,
              st.LQUA_ADATU LQUA_ADATU,
              st.LQUA_ZDATU LQUA_ZDATU,
              st.LQUA_WDATU LQUA_WDATU,
              st.LQUA_LETYP LQUA_LETYP,
              st.LQUA_GEWEI LQUA_GEWEI,
              st.LQUA_BETYP LQUA_BETYP, 
              st.LQUA_VFDAT LQUA_VFDAT,
              st.LQUA_LGTYP LQUA_LGTYP,
              st.LQUA_KOBER LQUA_KOBER,
              st.LQUA_IDATU LQUA_IDATU,
              ifnull(st.LQUA_SKZUE,'Not Set') LQUA_SKZUE,
              ifnull(st.LQUA_SKZUA,'Not Set') LQUA_SKZUA,
              ifnull(st.LQUA_SKZSE,'Not Set') LQUA_SKZSE,
              ifnull(st.LQUA_SKZSA,'Not Set') LQUA_SKZSA,
              ifnull(st.LQUA_SKZSI,'Not Set') LQUA_SKZSI,
              ifnull(st.LQUA_VIRGO,'Not Set') LQUA_VIRGO,
              ifnull(st.LQUA_KZHUQ,'Not Set') LQUA_KZHUQ,
              pl.CompanyCode CompanyCode
     FROM LQUA st,
          dim_plant pl,
          dim_warehousenumber wn
    WHERE st.LQUA_WERKS = pl.PlantCode
		 AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')			
          AND NOT EXISTS
                (SELECT 1
                   FROM fact_wmquant fq
                  WHERE     wn.dim_warehousenumberid = fq.dim_warehousenumberid
                        AND  st.LQUA_LQNUM = fq.dd_quantno)	;
			
UPDATE fact_wmquant_t1 f
SET dim_partid = ifnull(dp.dim_partid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_part dp ON  dp.PartNumber = f.LQUA_MATNR AND dp.plant = f.LQUA_WERKS;


UPDATE fact_wmquant_t1 f
SET dim_storagetypeid = ifnull(sty.dim_wmstoragetypeid,1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_wmstoragetype sty ON sty.storagetype = f.LQUA_LGTYP
					 AND sty.WarehouseNumber = f.LQUA_LGNUM;
					 
UPDATE fact_wmquant_t1 f
SET dim_storagebinid =  ifnull(sb.dim_storagebinid,1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_storagebin sb ON sb.storagebin = f.LQUA_LGPLA
                                    AND sb.storagetype = f.LQUA_LGTYP;
					 					 
UPDATE fact_wmquant_t1 f
SET dim_storagebinid =  ifnull(sb.dim_storagebinid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_storagebin sb ON sb.storagebin = f.LQUA_LGPLA
                                    AND sb.storagetype = f.LQUA_LGTYP;
					 			
UPDATE fact_wmquant_t1 f
SET dim_storagelocationid =  ifnull(sl.dim_storagelocationid,1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_storagelocation sl ON sl.plant = f.LQUA_WERKS
                                  AND sl.locationcode = f.LQUA_LGORT;
				    
UPDATE fact_wmquant_t1 f
SET dim_unitofmeasureid =  ifnull( um.dim_unitofmeasureid,1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_unitofmeasure um ON um.uom = f.LQUA_MEINS;
				  
UPDATE fact_wmquant_t1 f
SET dim_wmstockcategoryid =  wmsc.dim_wmstockcategoryid
FROM fact_wmquant_t1 f
     LEFT JOIN dim_wmstockcategory wmsc ON wmsc.WMStockCategory = f.LQUA_BESTQ;
				  
				  
UPDATE fact_wmquant_t1 f
SET dim_specialstockid =  ifnull(ss.dim_specialstockid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_specialstock ss ON ss.specialstockindicator = f.LQUA_SOBKZ;
  
 UPDATE fact_wmquant_t1 f
SET dim_wmblockingreasonid =  ifnull(wmbr.dim_wmblockingreasonid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_wmblockingreason wmbr ON wmbr.WMBlockingReason = f.LQUA_SPGRU
			               AND wmbr.WarehouseNumber = f.LQUA_LGNUM	;

UPDATE fact_wmquant_t1 f
SET dim_dateidlastmovement =  ifnull(lmd.dim_dateid,1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_date lmd ON lmd.DateValue = f.LQUA_BDATU
			   AND lmd.CompanyCode = f.CompanyCode;
		   	   
			       			  	
UPDATE fact_wmquant_t1 f
SET dim_dateidlaststockplacement =  ifnull(lspd.dim_dateid,1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_date lspd ON lspd.DateValue = f.LQUA_EDATU
			  AND lspd.CompanyCode = f.CompanyCode;	
			
UPDATE fact_wmquant_t1 f
SET dim_dateidlaststockremoval = ifnull( lsrd.dim_dateid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_date lsrd ON lsrd.DateValue = f.LQUA_ADATU
                             AND lsrd.CompanyCode = f.CompanyCode;
			     
UPDATE fact_wmquant_t1 f
SET dim_dateidlaststockaddition =  ifnull(lsad.dim_dateid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_date lsad ON lsad.DateValue = f.LQUA_ZDATU
			   AND lsad.CompanyCode = f.CompanyCode;
			   
UPDATE fact_wmquant_t1 f
SET dim_dateidgoodsreceipt =  ifnull(grd.dim_dateid,1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_date grd ON grd.DateValue = f.LQUA_WDATU
			    AND grd.CompanyCode = f.CompanyCode;
			    
UPDATE fact_wmquant_t1 f
SET dim_wmstorageunittypeid =  ifnull(wmsut.dim_wmstorageunittypeid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_wmstorageunittype wmsut
          	     ON  wmsut.StorageUnitType = f.LQUA_LETYP
			     AND  wmsut.WarehouseNumber = ifnull(f.LQUA_LGNUM, 'Not Set');			    			   
				 
UPDATE fact_wmquant_t1 f
SET dim_weightunitid =  ifnull ( wum.dim_unitofmeasureid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_unitofmeasure wum
          	     ON wum.uom = f.LQUA_GEWEI;
				 
UPDATE fact_wmquant_t1 f
SET dim_wmrequirementtypeid =  ifnull(wmrt.dim_wmrequirementtypeid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN dim_wmrequirementtype wmrt
          	     ON wmrt.RequirementType = f.LQUA_BETYP
			AND wmrt.WarehouseNumber = ifnull(f.LQUA_LGNUM, 'Not Set');	

UPDATE fact_wmquant_t1 f
SET dim_dateidbestbefore =  ifnull(bbd.dim_dateid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN  dim_date bbd
          	     ON bbd.DateValue = f.LQUA_VFDAT
                 AND bbd.CompanyCode = f.CompanyCode;
				 
UPDATE fact_wmquant_t1 f
SET dim_wmpickingareaid = ifnull( wmpa.dim_wmpickingareaid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN  dim_wmpickingarea wmpa
          	    ON wmpa.StorageType = f.LQUA_LGTYP
			    AND wmpa.PickingArea = f.LQUA_KOBER
			    AND wmpa.WarehouseNumber = ifnull(f.LQUA_LGNUM, 'Not Set');
				
UPDATE fact_wmquant_t1 f
SET dim_dateidlastinventory =  ifnull(lid.dim_dateid, 1)
FROM fact_wmquant_t1 f
     LEFT JOIN  dim_date lid
          	    ON lid.DateValue = f.LQUA_IDATU
				AND lid.CompanyCode = f.CompanyCode;	

UPDATE fact_wmquant_t1 f
SET Dim_WMQuantMiscId =  ifnull(wmqmisc.Dim_WMQuantMiscId ,1)
FROM fact_wmquant_t1 f
     LEFT JOIN  dim_wmquantmiscellaneous wmqmisc
          	    ON  wmqmisc.userputawayblocked  = ifnull(f.LQUA_SKZUE,'Not Set')
							  AND wmqmisc.userremovalblocked  = ifnull(f.LQUA_SKZUA,'Not Set')
							  AND wmqmisc.systcurrentplacementblocked = ifnull(f.LQUA_SKZSE,'Not Set')
							  AND wmqmisc.systremovalblocked  = ifnull(f.LQUA_SKZSA,'Not Set')
							  AND wmqmisc.systInventoryblocked  = ifnull(f.LQUA_SKZSI,'Not Set')
							  AND wmqmisc.nogrdata = ifnull(f.LQUA_VIRGO,'Not Set')
							  AND wmqmisc.OnHandlingUnit = ifnull(f.LQUA_KZHUQ,'Not Set');
							  
INSERT INTO fact_wmquant_tmp (fact_wmquantid,
								dim_warehousenumberid,
								dd_quantno,
								dim_partid,
								dim_plantid,
								dim_storagetypeid,
								dim_storagebinid,
								dim_storagelocationid,
								dim_unitofmeasureid,
								ct_availablestock,
								dd_batchnumber,
								dim_wmstockcategoryid,
								dim_specialstockid,
								dd_specialstockno,
								dim_wmblockingreasonid,
								dim_dateidlastmovement,
								dd_lastchangetodocno,
								dd_lastchangetodocitemno,
								dim_dateidlaststockplacement,
								dim_dateidlaststockremoval,
								dim_dateidlaststockaddition,
								dim_dateidgoodsreceipt,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dim_wmstorageunittypeid,
								ct_totalqty,
								ct_putawayqty,
								ct_removeqty,
								ct_materialweight,
								dim_weightunitid,
								dd_transferreqno,
								dd_inventorydocitemno,
								dim_wmrequirementtypeid,
								dd_requirementno,
								dd_storageunitno,
								dd_inspectionlotno,
								dim_dateidbestbefore,
								ct_capacityusage,
								dim_wmpickingareaid,
								ct_opentransferqty,
								dd_deliveryno,
								dd_deliveryitemno,
								dim_dateidlastinventory,
								Dim_WMQuantMiscId)
   SELECT ((SELECT ifnull(max_id, 0)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_wmquant') +
        			 row_number() over (order by '')) fact_wmquantid,
			a.*
         FROM (SELECT DISTINCT			
               dim_warehousenumberid,
								dd_quantno,
								dim_partid,
								dim_plantid,
								dim_storagetypeid,
								dim_storagebinid,
								dim_storagelocationid,
								dim_unitofmeasureid,
								ct_availablestock,
								dd_batchnumber,
								dim_wmstockcategoryid,
								dim_specialstockid,
								dd_specialstockno,
								dim_wmblockingreasonid,
								dim_dateidlastmovement,
								dd_lastchangetodocno,
								dd_lastchangetodocitemno,
								dim_dateidlaststockplacement,
								dim_dateidlaststockremoval,
								dim_dateidlaststockaddition,
								dim_dateidgoodsreceipt,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dim_wmstorageunittypeid,
								ct_totalqty,
								ct_putawayqty,
								ct_removeqty,
								ct_materialweight,
								dim_weightunitid,
								dd_transferreqno,
								dd_inventorydocitemno,
								dim_wmrequirementtypeid,
								dd_requirementno,
								dd_storageunitno,
								dd_inspectionlotno,
								dim_dateidbestbefore,
								ct_capacityusage,
								dim_wmpickingareaid,
								ct_opentransferqty,
								dd_deliveryno,
								dd_deliveryitemno,
								dim_dateidlastinventory,
								Dim_WMQuantMiscId
                   FROM fact_wmquant_t1) a	;
						
						
UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_partid = ifnull(dp.dim_partid ,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
       dim_part dp,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq  
where dp.PartNumber = st.LQUA_MATNR 
AND dp.plant = st.LQUA_WERKS  
AND  st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND  st.LQUA_LQNUM = fwmq.dd_quantno
AND  fwmq.dim_partid <> ifnull(dp.dim_partid ,1);


UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_plantid = ifnull(pl.Dim_Plantid ,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
       dim_part dp,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where  st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND  st.LQUA_LQNUM = fwmq.dd_quantno
AND  fwmq.dim_plantid <> ifnull(pl.Dim_Plantid ,1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_storagetypeid = ifnull(sty.dim_wmstoragetypeid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn, 
	   dim_wmstoragetype sty,
       fact_wmquant_tmp fwmq 	   
where  sty.storagetype = st.LQUA_LGTYP
AND sty.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_storagetypeid <> ifnull(sty.dim_wmstoragetypeid,1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_storagebinid = ifnull(sb.dim_storagebinid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn, 
	   dim_storagebin sb,
       fact_wmquant_tmp fwmq 	   
where  sb.storagebin = st.LQUA_LGPLA
AND sb.storagetype = st.LQUA_LGTYP
AND sb.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_storagebinid <> ifnull(sb.dim_storagebinid,1);


UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_storagelocationid = ifnull(sl.dim_storagelocationid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn, 
	   dim_storagelocation sl,
       fact_wmquant_tmp fwmq	   
where  sl.plant = st.LQUA_WERKS
AND sl.locationcode = st.LQUA_LGORT
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_storagelocationid <> ifnull(sl.dim_storagelocationid,1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_unitofmeasureid = ifnull( um.dim_unitofmeasureid,1),
    dw_update_date = current_timestamp
	FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn, 
	  dim_unitofmeasure um,
	  fact_wmquant_tmp fwmq
where  um.uom = st.LQUA_MEINS
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_unitofmeasureid <> ifnull( um.dim_unitofmeasureid,1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.ct_availablestock = ifnull(st.LQUA_VERME, 0),
    dw_update_date = current_timestamp
FROM
   LQUA st,
   dim_plant pl,
   dim_warehousenumber wn,
   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND  fwmq.ct_availablestock <> ifnull(st.LQUA_VERME, 0);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_batchnumber  = ifnull(st.LQUA_CHARG,'Not Set'),
    dw_update_date = current_timestamp
FROM
   LQUA st,
   dim_plant pl,
   dim_warehousenumber wn,
   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_batchnumber  <> ifnull(st.LQUA_CHARG,'Not Set');


UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_wmstockcategoryid  = ifnull(wmsc.dim_wmstockcategoryid,1),
    dw_update_date = current_timestamp
FROM
   LQUA st,
   dim_plant pl,
   dim_warehousenumber wn,
   dim_wmstockcategory wmsc,
   fact_wmquant_tmp fwmq
where wmsc.WMStockCategory = st.LQUA_BESTQ
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmstockcategoryid  <> ifnull(wmsc.dim_wmstockcategoryid,1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_specialstockid  = ifnull(ss.dim_specialstockid,1),
    dw_update_date = current_timestamp
FROM
   LQUA st,
   dim_plant pl,
   dim_warehousenumber wn,
   dim_specialstock ss,
   fact_wmquant_tmp fwmq
where ss.specialstockindicator = st.LQUA_SOBKZ
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_specialstockid  <> ifnull(ss.dim_specialstockid,1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_specialstockno  = ifnull(st.LQUA_SONUM,'Not Set'),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_specialstockno  <>  ifnull(st.LQUA_SONUM,'Not Set');

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_wmblockingreasonid  = ifnull(wmbr.dim_wmblockingreasonid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmblockingreason wmbr,
	   fact_wmquant_tmp fwmq
where wmbr.WMBlockingReason = st.LQUA_SPGRU
AND wmbr.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmblockingreasonid  <> ifnull(wmbr.dim_wmblockingreasonid,1);		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_dateidlastmovement = ifnull(lmd.dim_dateid, 1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lmd,
	   fact_wmquant_tmp fwmq
where lmd.DateValue = st.LQUA_BDATU
AND lmd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlastmovement <> ifnull(lmd.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_lastchangetodocno  = ifnull(st.LQUA_BTANR,'Not Set'),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	 
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_lastchangetodocno <> ifnull(st.LQUA_BTANR,'Not Set');	


UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_lastchangetodocitemno  = ifnull(st.LQUA_BTAPS,0),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_lastchangetodocitemno  <> ifnull(st.LQUA_BTAPS,0);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_dateidlaststockplacement = ifnull( lspd.dim_dateid, 1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lspd,
	   fact_wmquant_tmp fwmq
where lspd.DateValue = st.LQUA_EDATU
AND lspd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlaststockplacement <> ifnull( lspd.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_dateidlaststockremoval = ifnull( lsrd.dim_dateid, 1),
   dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lsrd,
	   fact_wmquant_tmp fwmq
where lsrd.DateValue = st.LQUA_ADATU
AND lsrd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlaststockremoval <> ifnull( lsrd.dim_dateid, 1);	

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_dateidlaststockaddition = ifnull( lsad.dim_dateid, 1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lsad,
	   fact_wmquant_tmp fwmq
where lsad.DateValue = st.LQUA_ZDATU
AND lsad.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlaststockaddition <> ifnull( lsad.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_dateidgoodsreceipt = ifnull( grd.dim_dateid, 1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date grd,
	   fact_wmquant_tmp fwmq
where grd.DateValue = st.LQUA_WDATU
AND grd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidgoodsreceipt <> ifnull( grd.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_dateidgoodsreceipt = ifnull( grd.dim_dateid, 1),
    dw_update_date = current_timestamp
FROM   LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date grd,
	   fact_wmquant_tmp fwmq
where grd.DateValue = st.LQUA_WDATU
AND grd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidgoodsreceipt <> ifnull( grd.dim_dateid, 1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_goodsreceiptno  = ifnull(st.LQUA_WENUM,'Not Set'),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_goodsreceiptno <> ifnull(st.LQUA_WENUM,'Not Set');		


UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_goodsreceiptitemno  = ifnull(st.LQUA_WEPOS,0),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_goodsreceiptitemno  <> ifnull(st.LQUA_WEPOS,0);		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_wmstorageunittypeid  =  ifnull(wmsut.dim_wmstorageunittypeid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmstorageunittype wmsut,
	   fact_wmquant_tmp fwmq
where wmsut.StorageUnitType = st.LQUA_LETYP
AND wmsut.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmstorageunittypeid <> ifnull(wmsut.dim_wmstorageunittypeid,1);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.ct_totalqty  = ifnull(st.LQUA_GESME,0),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_totalqty <> ifnull(st.LQUA_GESME,0);	

UPDATE fact_wmquant_tmp fwmq
SET fwmq.ct_putawayqty  = ifnull(st.LQUA_EINME,0), 
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_putawayqty <> ifnull(st.LQUA_EINME,0);	

UPDATE fact_wmquant_tmp fwmq
SET fwmq.ct_removeqty  = ifnull(st.LQUA_AUSME,0),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_removeqty <> ifnull(st.LQUA_AUSME,0);	

UPDATE fact_wmquant_tmp fwmq
SET fwmq.ct_materialweight  = ifnull( st.LQUA_MGEWI,0),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_materialweight <> ifnull( st.LQUA_MGEWI,0);

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_weightunitid  = ifnull(wum.dim_unitofmeasureid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_unitofmeasure wum,
	   fact_wmquant_tmp fwmq
where wum.uom = st.LQUA_GEWEI
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_weightunitid  <> ifnull(wum.dim_unitofmeasureid,1);

	
UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_transferreqno  = ifnull(st.LQUA_TBNUM,'Not Set'),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_transferreqno <> ifnull(st.LQUA_TBNUM,'Not Set');	

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_inventorydocitemno  = ifnull(st.LQUA_IVPOS,0),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_inventorydocitemno <> ifnull(st.LQUA_IVPOS,0);	

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_wmrequirementtypeid  =  ifnull(wmrt.dim_wmrequirementtypeid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmrequirementtype wmrt,
	   fact_wmquant_tmp fwmq
where wmrt.RequirementType = st.LQUA_BETYP
AND wmrt.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set')
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmrequirementtypeid <> ifnull(wmrt.dim_wmrequirementtypeid,1);		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_requirementno  = ifnull(st.LQUA_BENUM,'Not Set'),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_requirementno <> ifnull(st.LQUA_BENUM,'Not Set');		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_storageunitno  = ifnull(st.LQUA_LENUM,'Not Set'), 
    dw_update_date = current_timestamp 
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_storageunitno <> ifnull(st.LQUA_LENUM,'Not Set');	


UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_inspectionlotno  = ifnull(st.LQUA_QPLOS,'Not Set'),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_inspectionlotno <> ifnull(st.LQUA_QPLOS,'Not Set');		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_dateidbestbefore = ifnull( bbd.dim_dateid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date bbd,
	   fact_wmquant_tmp fwmq
where bbd.DateValue = st.LQUA_VFDAT
AND bbd.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidbestbefore <> ifnull( bbd.dim_dateid,1);		

		
UPDATE fact_wmquant_tmp fwmq
SET fwmq.ct_capacityusage  = ifnull(st.LQUA_QKAPV,0),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_capacityusage <> ifnull(st.LQUA_QKAPV,0);		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_wmpickingareaid  = ifnull(wmpa.dim_wmpickingareaid,1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmpickingarea wmpa,
	   fact_wmquant_tmp fwmq
where wmpa.StorageType = st.LQUA_LGTYP
AND wmpa.PickingArea = st.LQUA_KOBER
AND wmpa.WarehouseNumber = ifnull(st.LQUA_LGNUM, 'Not Set') 
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_wmpickingareaid <> ifnull(wmpa.dim_wmpickingareaid,1);		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.ct_opentransferqty  = ifnull(st.LQUA_TRAME, 0) ,
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.ct_opentransferqty  <> ifnull(st.LQUA_TRAME, 0) ;	

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_deliveryno  = ifnull(st.LQUA_VBELN,'Not Set'), 
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_deliveryno <> ifnull(st.LQUA_VBELN,'Not Set');	

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dd_deliveryitemno  = ifnull(st.LQUA_POSNR,0), 
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dd_deliveryitemno  <> ifnull(st.LQUA_POSNR,0);		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.dim_dateidlastinventory = ifnull( lid.dim_dateid, 1),
    dw_update_date = current_timestamp
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_date lid,
	   fact_wmquant_tmp fwmq
where lid.DateValue = st.LQUA_IDATU
AND lid.CompanyCode = pl.CompanyCode
AND st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND fwmq.dim_dateidlastinventory <> ifnull( lid.dim_dateid, 1);		

UPDATE fact_wmquant_tmp fwmq
SET fwmq.Dim_WMQuantMiscId = ifnull ( wmqmisc.Dim_WMQuantMiscId,1),
    dw_update_date = current_timestamp  
FROM
       LQUA st,
       dim_plant pl,
	   dim_warehousenumber wn,
	   dim_wmquantmiscellaneous wmqmisc,
	   fact_wmquant_tmp fwmq
where st.LQUA_WERKS = pl.PlantCode
AND wn.warehousecode = ifnull(st.LQUA_LGNUM, 'Not Set')
AND wn.dim_warehousenumberid = fwmq.dim_warehousenumberid
AND st.LQUA_LQNUM = fwmq.dd_quantno
AND wmqmisc.userputawayblocked  = ifnull(st.LQUA_SKZUE,'Not Set')
AND wmqmisc.userremovalblocked  = ifnull(st.LQUA_SKZUA,'Not Set')
AND wmqmisc.systcurrentplacementblocked = ifnull(st.LQUA_SKZSE,'Not Set')
AND wmqmisc.systremovalblocked  = ifnull(st.LQUA_SKZSA,'Not Set')
AND wmqmisc.systInventoryblocked  = ifnull(st.LQUA_SKZSI,'Not Set')
AND wmqmisc.OnHandlingUnit = ifnull(st.LQUA_KZHUQ,'Not Set')
AND fwmq.Dim_WMQuantMiscId <> ifnull ( wmqmisc.Dim_WMQuantMiscId,1);

Insert into fact_wmquant
(fact_wmquantid,
								dim_warehousenumberid,
								dd_quantno,
								dim_partid,
								dim_plantid,
								dim_storagetypeid,
								dim_storagebinid,
								dim_storagelocationid,
								dim_unitofmeasureid,
								ct_availablestock,
								dd_batchnumber,
								dim_wmstockcategoryid,
								dim_specialstockid,
								dd_specialstockno,
								dim_wmblockingreasonid,
								dim_dateidlastmovement,
								dd_lastchangetodocno,
								dd_lastchangetodocitemno,
								dim_dateidlaststockplacement,
								dim_dateidlaststockremoval,
								dim_dateidlaststockaddition,
								dim_dateidgoodsreceipt,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dim_wmstorageunittypeid,
								ct_totalqty,
								ct_putawayqty,
								ct_removeqty,
								ct_materialweight,
								dim_weightunitid,
								dd_transferreqno,
								dd_inventorydocitemno,
								dim_wmrequirementtypeid,
								dd_requirementno,
								dd_storageunitno,
								dd_inspectionlotno,
								dim_dateidbestbefore,
								ct_capacityusage,
								dim_wmpickingareaid,
								ct_opentransferqty,
								dd_deliveryno,
								dd_deliveryitemno,
								dim_dateidlastinventory,
								Dim_WMQuantMiscId
)
select fact_wmquantid,
								dim_warehousenumberid,
								dd_quantno,
								dim_partid,
								dim_plantid,
								dim_storagetypeid,
								dim_storagebinid,
								dim_storagelocationid,
								dim_unitofmeasureid,
								ct_availablestock,
								dd_batchnumber,
								dim_wmstockcategoryid,
								dim_specialstockid,
								dd_specialstockno,
								dim_wmblockingreasonid,
								dim_dateidlastmovement,
								dd_lastchangetodocno,
								dd_lastchangetodocitemno,
								dim_dateidlaststockplacement,
								dim_dateidlaststockremoval,
								dim_dateidlaststockaddition,
								dim_dateidgoodsreceipt,
								dd_goodsreceiptno,
								dd_goodsreceiptitemno,
								dim_wmstorageunittypeid,
								ct_totalqty,
								ct_putawayqty,
								ct_removeqty,
								ct_materialweight,
								dim_weightunitid,
								dd_transferreqno,
								dd_inventorydocitemno,
								dim_wmrequirementtypeid,
								dd_requirementno,
								dd_storageunitno,
								dd_inspectionlotno,
								dim_dateidbestbefore,
								ct_capacityusage,
								dim_wmpickingareaid,
								ct_opentransferqty,
								dd_deliveryno,
								dd_deliveryitemno,
								dim_dateidlastinventory,
								Dim_WMQuantMiscId
	from fact_wmquant_tmp;


drop table if exists fact_wmquant_tmp;