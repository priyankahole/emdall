UPDATE dim_MaterialPriceGroup1 a
   SET a.Description = ifnull(TVM1T_BEZEI, 'Not Set'),
       dw_update_date = current_timestamp
	   FROM dim_MaterialPriceGroup1 a,TVM1T
 WHERE a.MaterialPriceGroup1 = TVM1T_MVGR1 AND RowIsCurrent = 1
	   and a.Description <> ifnull(TVM1T_BEZEI, 'Not Set');

