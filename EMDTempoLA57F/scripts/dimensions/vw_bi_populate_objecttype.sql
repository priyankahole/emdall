UPDATE    dim_objecttype ot
   SET ot.Description = ifnull(DD07T_DDTEXT, 'Not Set'),
			ot.dw_update_date = current_timestamp
       FROM dim_objecttype ot,
          DD07T t
 WHERE ot.RowIsCurrent = 1
        AND   t.DD07T_DOMNAME = 'OBART'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND ot.ObjectType = t.DD07T_DOMVALUE
;

INSERT INTO dim_ObjectType(dim_ObjectTypeId, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_ObjectType
               WHERE dim_ObjectTypeId = 1);

delete from number_fountain m where m.table_name = 'dim_ObjectType';
   
insert into number_fountain
select 	'dim_ObjectType',
	ifnull(max(d.Dim_objecttypeid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_ObjectType d
where d.Dim_objecttypeid <> 1; 

INSERT INTO dim_ObjectType(Dim_objecttypeid,
                           Description,
                           ObjectType,
                           RowStartDate,
                           RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_ObjectType')
         + row_number() over(order by ''),
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'OBART' AND DD07T_DOMVALUE IS NOT NULL
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_ObjectType
                    WHERE ObjectType = DD07T_DOMVALUE AND DD07T_DOMNAME = 'OBART')
   ORDER BY 2 ;
