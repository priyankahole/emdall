UPDATE dim_deliveryblock a
   SET Description = ifnull(TVLST_VTEXT, 'Not Set')
    FROM dim_deliveryblock a, TVLST
 WHERE a.DeliveryBlock = TVLST_LIFSP AND RowIsCurrent = 1;

