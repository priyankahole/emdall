INSERT INTO dim_salesorderrejectreason(dim_salesorderrejectreasonid,
                             RejectReasonCode,
                             Description)
   SELECT 1,
          'Not Set',
          'Not Set'
     FROM (SELECT 1) a
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_salesorderrejectreason
               WHERE dim_salesorderrejectreasonid = 1);

UPDATE    dim_salesorderrejectreason a
	SET a.Description = ifnull(TVAGT_BEZEI, 'Not Set'),
			a.dw_update_date = current_timestamp
       FROM dim_salesorderrejectreason a,
          TVAGT t
     WHERE t.TVAGT_ABGRU IS NOT NULL AND a.RejectReasonCode = t.TVAGT_ABGRU;
 
delete from number_fountain m where m.table_name = 'dim_salesorderrejectreason';

insert into number_fountain
select 	'dim_salesorderrejectreason',
	ifnull(max(d.Dim_SalesOrderRejectReasonid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_salesorderrejectreason d
where d.Dim_SalesOrderRejectReasonid <> 1; 
 
INSERT
  INTO dim_salesorderrejectreason(Dim_SalesOrderRejectReasonid,
          RejectReasonCode,
          Description)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_salesorderrejectreason') 
          + row_number() over(order by ''),
			 TVAGT_ABGRU,
	ifnull(TVAGT_BEZEI,'Not Set')
FROM TVAGT
WHERE TVAGT_ABGRU is not null
	and not exists (select 1 from dim_salesorderrejectreason a where a.RejectReasonCode = TVAGT_ABGRU);