drop table if exists tmp_demandfulfillment_redflag;
create table tmp_demandfulfillment_redflag as
select 	partnumber,
	plantcode,
SUM((((GREATEST((CASE WHEN  AMT_SHIPPEDMTD > ( ct_salesforecast * AMT_AVGSALEPRICE ) THEN 0
												ELSE ( ct_salesforecast * AMT_AVGSALEPRICE )- AMT_SHIPPEDMTD END), ( AMT_PASTDUE + AMT_OPENSALES )) )
	 - (( Least(Greatest( (CASE WHEN  AMT_SHIPPEDMTD > ( ct_salesforecast * (CASE WHEN AMT_AVGSALEPRICE =0
																																								THEN case when ((CT_PASTDUE + CT_OPENSALES) ) = 0 then 0
																																													else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																											end
																																								ELSE AMT_AVGSALEPRICE
																																								END) )
															THEN 0
															ELSE ( ct_salesforecast * (CASE WHEN AMT_AVGSALEPRICE =0
																															THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																																				else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																	end
																															ELSE AMT_AVGSALEPRICE
																													END) ) -  AMT_SHIPPEDMTD
													END) , ( AMT_PASTDUE + AMT_OPENSALES ) 	)
				,  CT_STOCKAVAILABLE * (CASE WHEN AMT_AVGSALEPRICE =0
																						THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																											else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																								end
																						ELSE AMT_AVGSALEPRICE
																				END) ) ) ) ) -- end of GREATEST()- Least(Greatest())
 	- LEAST((Greatest((CASE WHEN  AMT_SHIPPEDMTD > ( ct_salesforecast * AMT_AVGSALEPRICE ) THEN 0
														ELSE ( ct_salesforecast * AMT_AVGSALEPRICE )- AMT_SHIPPEDMTD
											END) , ( AMT_PASTDUE + AMT_OPENSALES )))
	 				- (Least(Greatest( (CASE WHEN  AMT_SHIPPEDMTD > ( ct_salesforecast * (CASE WHEN AMT_AVGSALEPRICE =0
																																													THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																																																		else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																															end
																																													ELSE AMT_AVGSALEPRICE
																																											END) ) THEN 0
																	 		  ELSE ( ct_salesforecast * (CASE WHEN AMT_AVGSALEPRICE =0
																																				THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																																									else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																							end
																																				ELSE AMT_AVGSALEPRICE
																																		END) ) -  AMT_SHIPPEDMTD
																		END) , ( AMT_PASTDUE + AMT_OPENSALES ) 	)
										 ,  CT_STOCKAVAILABLE * (CASE WHEN AMT_AVGSALEPRICE =0
																									THEN case when (CT_PASTDUE + CT_OPENSALES) = 0 then 0
																														else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																											 end
																									ELSE AMT_AVGSALEPRICE
																							END)))
		,( CT_STOCKINQA * (CASE WHEN AMT_AVGSALEPRICE =0
														THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																			else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																	end
														ELSE AMT_AVGSALEPRICE
												END) ) ) ) -- end of  (GREATEST()- Least(Greatest())) - LEAST(Greatest()-Least(Greatest())))
- least( ((((Greatest( (CASE WHEN  AMT_SHIPPEDMTD > ( ct_salesforecast * AMT_AVGSALEPRICE ) THEN 0 ELSE ( ct_salesforecast * AMT_AVGSALEPRICE )-  AMT_SHIPPEDMTD END)
												,( AMT_PASTDUE + AMT_OPENSALES ) ) )
					 - ((Least(Greatest( (CASE WHEN  AMT_SHIPPEDMTD > ( ct_salesforecast * (CASE WHEN AMT_AVGSALEPRICE =0
																																													THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																																																		else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																																end
																																													ELSE AMT_AVGSALEPRICE
																																													END) )
																				THEN 0
																				ELSE ( ct_salesforecast * (CASE WHEN AMT_AVGSALEPRICE =0
																																				THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																																									else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																						end
																																				ELSE AMT_AVGSALEPRICE
																																	 END) ) -  AMT_SHIPPEDMTD
																				END) , ( AMT_PASTDUE + AMT_OPENSALES ) 	)
								 , CT_STOCKAVAILABLE * (CASE 	WHEN AMT_AVGSALEPRICE =0
									 													 	THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																												else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																												end
																							ELSE AMT_AVGSALEPRICE
																				END)  ) )))
						-	LEAST( (Greatest( (CASE WHEN  AMT_SHIPPEDMTD > ( ct_salesforecast * AMT_AVGSALEPRICE ) THEN 0  ELSE ( ct_salesforecast * AMT_AVGSALEPRICE )-  AMT_SHIPPEDMTD END)
											,( AMT_PASTDUE + AMT_OPENSALES )))
											- (( Least( Greatest( (CASE WHEN  AMT_SHIPPEDMTD > ( ct_salesforecast * (CASE WHEN AMT_AVGSALEPRICE =0
																																																		THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																																																							else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																																					end
																																																		ELSE AMT_AVGSALEPRICE
																																																		END) )
																									THEN 0
																									ELSE ( ct_salesforecast * (CASE WHEN AMT_AVGSALEPRICE =0
																																									THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																																														else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																												end
																																									ELSE AMT_AVGSALEPRICE
																																							END) ) -  AMT_SHIPPEDMTD
																									END)
																	, ( AMT_PASTDUE + AMT_OPENSALES ))
														,  CT_STOCKAVAILABLE * (CASE WHEN AMT_AVGSALEPRICE =0
																													THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																																		else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																																end
																													ELSE AMT_AVGSALEPRICE
																										END) ) ))
								, ( CT_STOCKINQA * (CASE WHEN AMT_AVGSALEPRICE =0
																				THEN case when ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																									else ( AMT_PASTDUE + AMT_OPENSALES ) / ( CT_PASTDUE + CT_OPENSALES )
																							end
																				ELSE AMT_AVGSALEPRICE
																		END) ) 		) ) )
			, CT_PURCHOPENQTY * (CASE WHEN  AMT_AVGSALEPRICE =0
																	THEN  case when  ( CT_PASTDUE + CT_OPENSALES ) = 0 then 0
																							else  ( AMT_PASTDUE + AMT_OPENSALES )  /  ( CT_PASTDUE + CT_OPENSALES )
																				end
																ELSE  AMT_AVGSALEPRICE
														END)
)) as local_risk_euro,
SUM((((CASE
       WHEN greatest((CASE
                          WHEN ct_salesforecast >= CT_SHIPPEDMTD THEN ct_salesforecast - CT_SHIPPEDMTD
                          ELSE 0
                      END),(CT_PASTDUE + CT_OPENSALES)) >= CT_STOCKAVAILABLE THEN greatest((CASE
                                                                                                WHEN ct_salesforecast >= CT_SHIPPEDMTD THEN ct_salesforecast - CT_SHIPPEDMTD
                                                                                                ELSE 0
                                                                                            END),(CT_PASTDUE + CT_OPENSALES)) - CT_STOCKAVAILABLE
       ELSE 0
   END) - (Least((CASE
                      WHEN GREATEST((CASE
                                         WHEN ct_salesforecast >= CT_SHIPPEDMTD THEN ct_salesforecast - CT_SHIPPEDMTD
                                         ELSE 0
                                     END),(CT_PASTDUE + CT_OPENSALES)) >= CT_STOCKAVAILABLE THEN GREATEST((CASE
                                                                                                               WHEN ct_salesforecast >= CT_SHIPPEDMTD THEN ct_salesforecast - CT_SHIPPEDMTD
                                                                                                               ELSE 0
                                                                                                           END),(CT_PASTDUE + CT_OPENSALES)) - CT_STOCKAVAILABLE
                      ELSE 0
                  END), CT_STOCKINQA)))) - least((((CASE
                                                        WHEN greatest((CASE
                                                                           WHEN ct_salesforecast >= CT_SHIPPEDMTD THEN ct_salesforecast - CT_SHIPPEDMTD
                                                                           ELSE 0
                                                                       END),(CT_PASTDUE + CT_OPENSALES)) >= CT_STOCKAVAILABLE THEN greatest((CASE
                                                                                                                                                 WHEN ct_salesforecast >= CT_SHIPPEDMTD THEN ct_salesforecast - CT_SHIPPEDMTD
                                                                                                                                                 ELSE 0
                                                                                                                                             END),(CT_PASTDUE + CT_OPENSALES)) - CT_STOCKAVAILABLE
                                                        ELSE 0
                                                    END) - (Least((CASE
                                                                       WHEN GREATEST((CASE
                                                                                          WHEN ct_salesforecast >= CT_SHIPPEDMTD THEN ct_salesforecast - CT_SHIPPEDMTD
                                                                                          ELSE 0
                                                                                      END),(CT_PASTDUE + CT_OPENSALES)) >= CT_STOCKAVAILABLE THEN GREATEST((CASE
                                                                                                                                                                WHEN ct_salesforecast >= CT_SHIPPEDMTD THEN ct_salesforecast - CT_SHIPPEDMTD
                                                                                                                                                                ELSE 0
                                                                                                                                                            END),(CT_PASTDUE + CT_OPENSALES)) - CT_STOCKAVAILABLE
                                                                       ELSE 0
                                                                   END), CT_STOCKINQA)))) , CT_PURCHOPENQTY)) as local_risk_qty

from EMD586.fact_merckcontroltower f, EMD586.dim_date dt, EMD586.dim_part dpt, EMD586.dim_plant dpl
where f.dim_periodid = dim_Dateid
	and f.dim_partid = dpt.dim_partid
	and f.dim_plantid = dpl.dim_plantid
	and date_trunc('month',datevalue) = date_trunc('month',current_date)
group by partnumber,plantcode;

delete from dim_hc_demandfulfillmentrisk;
insert into dim_hc_demandfulfillmentrisk (dim_hc_demandfulfillmentriskid,partnumber,plant ,source_site,local_risk_euro,local_risk_qty,network_risk_euro,network_risk_qty,dw_insert_date,dw_update_date,projectsourceid )
	select 1,'Not Set','Not Set','Not Set',0,0,0,0, current_timestamp, current_timestamp,1
	from (select 1)
	where not exists (select 1 from dim_hc_demandfulfillmentrisk where dim_hc_demandfulfillmentriskid = 1) ;

insert into dim_hc_demandfulfillmentrisk (dim_hc_demandfulfillmentriskid,partnumber,plant,source_site,local_risk_euro,local_risk_qty,network_risk_euro,network_risk_qty,dw_insert_date,dw_update_date,projectsourceid)
select (select dim_projectsourceid * multiplier from dim_projectsource) + row_number() over(order by'') as dim_hc_demandfulfillmentriskid
	,ifnull(t.partnumber,'Not Set') partnumber
	,ifnull(t.plantcode,'Not Set') as plant
	,ifnull(csvs.source_site,'Not Set') as source_site
	,cast(ifnull(t.local_risk_euro,0) as decimal(18,4)) as local_risk_euro
	,cast(ifnull(t.local_risk_qty,0) as decimal(18,4)) as local_risk_qty
	,0 as netwrok_risk_euro
	,0 as netwrok_risk_qty
	,current_timestamp as dw_insert_date
	,current_timestamp as dw_update_date
	,1 as projectsourceid
from tmp_demandfulfillment_redflag t
left outer join EMDTEMPOCC4.csv_supplied_sourcing csvs
	on right('000000000000000000' || t.partnumber,18) = right('000000000000000000' || csvs.partnumber,18)
	and t.plantcode = csvs.plant;

drop table if exists tmp_network_risk ;
create table tmp_network_risk as
select t.partnumber
	,csvs.source_site
	,sum(t.local_risk_euro) as network_risk_euro
	,sum(t.local_risk_qty) as network_risk_qty
from tmp_demandfulfillment_redflag t, EMDTEMPOCC4.csv_supplied_sourcing csvs
where right('000000000000000000' || t.partnumber,18) = right('000000000000000000' || csvs.partnumber,18)
	and t.plantcode = csvs.plant
	and csvs.source_site <> csvs.plant
group by t.partnumber ,csvs.source_site;

update dim_hc_demandfulfillmentrisk d
	set d.network_risk_euro = greatest(ifnull(t.network_risk_euro,0),0),
		d.network_risk_qty = greatest(ifnull(t.network_risk_qty,0),0)
from dim_hc_demandfulfillmentrisk d, tmp_network_risk t
where d.partnumber = t.partnumber
	and d.plant = t.source_site;

update dim_hc_demandfulfillmentrisk d
	set d.localrisk_flag = case when local_risk_qty > 0 then 'Yes' else 'Not Set' end,
		d.networkrisk_flag = case when network_risk_qty > 0 then 'Yes' else 'Not Set' end;

/* Merck HC Control Tower */
update fact_merckcontroltower f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(dd.dim_hc_demandfulfillmentriskid ,1)
from fact_merckcontroltower f,dim_part dpt, dim_plant dpl, dim_hc_demandfulfillmentrisk dd
where f.dim_partid = dpt.dim_partid
	and f.dim_plantid = dpl.dim_plantid
	and dpt.partnumber = dd.partnumber
	and dpl.plantcode = dd.plant;

update EMD586.fact_merckcontroltower f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(f1.dim_hc_demandfulfillmentriskid ,1)
from EMD586.fact_merckcontroltower f, EMDTEMPOLA57F.fact_merckcontroltower f1
where f.fact_merckcontroltowerid = f1.fact_merckcontroltowerid
	and f.dim_hc_demandfulfillmentriskid  <> ifnull(f1.dim_hc_demandfulfillmentriskid ,1);

/* Inspection Lot */
merge into fact_inspectionlot f
using (select f.fact_inspectionlotid
		,ifnull(dd.dim_hc_demandfulfillmentriskid ,1) as dim_hc_demandfulfillmentriskid
		from fact_inspectionlot f
			inner join dim_part dpt on f.dim_partid = dpt.dim_partid
			inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
			left outer join dim_hc_demandfulfillmentrisk dd on dpt.partnumber = dd.partnumber
					and dpl.plantcode = dd.plant) drsk
on f.fact_inspectionlotid = drsk.fact_inspectionlotid
when matched then update
	set f.dim_hc_demandfulfillmentriskid = drsk.dim_hc_demandfulfillmentriskid;

update EMD586.fact_inspectionlot f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(f1.dim_hc_demandfulfillmentriskid ,1)
from EMD586.fact_inspectionlot f, EMDTEMPOLA57F.fact_inspectionlot f1
where f.fact_inspectionlotid = f1.fact_inspectionlotid
	and f.dim_hc_demandfulfillmentriskid  <> ifnull(f1.dim_hc_demandfulfillmentriskid ,1);

/* Planned Order */
merge into fact_planorder f
using (select f.fact_planorderid
		,ifnull(dd.dim_hc_demandfulfillmentriskid ,1) as dim_hc_demandfulfillmentriskid
		from fact_planorder f
			inner join dim_part dpt on f.dim_partid = dpt.dim_partid
			inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
			left outer join dim_hc_demandfulfillmentrisk dd on dpt.partnumber = dd.partnumber
					and dpl.plantcode = dd.plant) drsk
on f.fact_planorderid = drsk.fact_planorderid
when matched then update
	set f.dim_hc_demandfulfillmentriskid = drsk.dim_hc_demandfulfillmentriskid;

update EMD586.fact_planorder f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(f1.dim_hc_demandfulfillmentriskid ,1)
from EMD586.fact_planorder f, EMDTEMPOLA57F.fact_planorder f1
where f.fact_planorderid = f1.fact_planorderid
	and f.dim_hc_demandfulfillmentriskid  <> ifnull(f1.dim_hc_demandfulfillmentriskid ,1);

/* Production Order */
merge into fact_productionorder f
using (select f.fact_productionorderid
		,ifnull(dd.dim_hc_demandfulfillmentriskid ,1) as dim_hc_demandfulfillmentriskid
		from fact_productionorder f
			inner join dim_part dpt on f.dim_partiditem = dpt.dim_partid
			inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
			left outer join dim_hc_demandfulfillmentrisk dd on dpt.partnumber = dd.partnumber
					and dpl.plantcode = dd.plant) drsk
on f.fact_productionorderid = drsk.fact_productionorderid
when matched then update
	set f.dim_hc_demandfulfillmentriskid = drsk.dim_hc_demandfulfillmentriskid;

update EMD586.fact_productionorder f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(f1.dim_hc_demandfulfillmentriskid ,1)
from EMD586.fact_productionorder f, EMDTEMPOLA57F.fact_productionorder f1
where f.fact_productionorderid = f1.fact_productionorderid
	and f.dim_hc_demandfulfillmentriskid  <> ifnull(f1.dim_hc_demandfulfillmentriskid ,1);

/* Purchase Order */
merge into fact_purchase f
using (select f.fact_purchaseid
		,ifnull(dd.dim_hc_demandfulfillmentriskid ,1) as dim_hc_demandfulfillmentriskid
		from fact_purchase f
			inner join dim_part dpt on f.dim_partid = dpt.dim_partid
			inner join dim_plant dpl on f.dim_plantidordering = dpl.dim_plantid
			left outer join dim_hc_demandfulfillmentrisk dd on dpt.partnumber = dd.partnumber
					and dpl.plantcode = dd.plant) drsk
on f.fact_purchaseid = drsk.fact_purchaseid
when matched then update
	set f.dim_hc_demandfulfillmentriskid = drsk.dim_hc_demandfulfillmentriskid;

update EMD586.fact_purchase f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(f1.dim_hc_demandfulfillmentriskid ,1)
from EMD586.fact_purchase f, fact_purchase f1
where f.fact_purchaseid = f1.fact_purchaseid
	and f.dim_hc_demandfulfillmentriskid  <> ifnull(f1.dim_hc_demandfulfillmentriskid ,1);

/* Purchase Requisition */
merge into fact_purchase_requisition f
using (select f.fact_purchase_requisitionid
		,ifnull(dd.dim_hc_demandfulfillmentriskid ,1) as dim_hc_demandfulfillmentriskid
		from fact_purchase_requisition f
			inner join dim_part dpt on f.dim_materialid = dpt.dim_partid
			inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
			left outer join dim_hc_demandfulfillmentrisk dd on dpt.partnumber = dd.partnumber
					and dpl.plantcode = dd.plant) drsk
on f.fact_purchase_requisitionid = drsk.fact_purchase_requisitionid
when matched then update
	set f.dim_hc_demandfulfillmentriskid = drsk.dim_hc_demandfulfillmentriskid;

update EMD586.fact_purchase_requisition f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(f1.dim_hc_demandfulfillmentriskid ,1)
from EMD586.fact_purchase_requisition f, fact_purchase_requisition f1
where f.fact_purchase_requisitionid = f1.fact_purchase_requisitionid
	and f.dim_hc_demandfulfillmentriskid  <> ifnull(f1.dim_hc_demandfulfillmentriskid ,1);

/* Stock Analytics */
merge into fact_inventoryaging f
using (select f.fact_inventoryagingid
		,ifnull(dd.dim_hc_demandfulfillmentriskid ,1) as dim_hc_demandfulfillmentriskid
		from fact_inventoryaging f
			inner join dim_part dpt on f.dim_partid = dpt.dim_partid
			inner join dim_plant dpl on f.dim_plantid = dpl.dim_plantid
			left outer join dim_hc_demandfulfillmentrisk dd on dpt.partnumber = dd.partnumber
					and dpl.plantcode = dd.plant) drsk
on f.fact_inventoryagingid = drsk.fact_inventoryagingid
when matched then update
	set f.dim_hc_demandfulfillmentriskid = drsk.dim_hc_demandfulfillmentriskid;

update EMD586.fact_inventoryaging f
	set f.dim_hc_demandfulfillmentriskid  = ifnull(f1.dim_hc_demandfulfillmentriskid ,1)
from EMD586.fact_inventoryaging f, fact_inventoryaging f1
where f.fact_inventoryagingid = f1.fact_inventoryagingid
	and f.dim_hc_demandfulfillmentriskid  <> ifnull(f1.dim_hc_demandfulfillmentriskid ,1);
