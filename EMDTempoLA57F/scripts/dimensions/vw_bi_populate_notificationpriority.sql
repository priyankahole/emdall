UPDATE       dim_notificationpriority s
          SET s.NotificationPriorityName = T356_T_PRIOKX,
			s.dw_update_date = current_timestamp
		FROM
             dim_notificationpriority s, T356_T t1, T356A_T t2
       WHERE t1.T356_T_ARTPR = t2.T356A_T_ARTPR
 AND s.RowIsCurrent = 1
 AND  s.NotificationPriorityType = t1.T356_T_ARTPR
 AND s.NotificationPriorityCode = t1.T356_T_PRIOK
;

INSERT INTO dim_notificationpriority(dim_notificationpriorityId, RowIsCurrent,notificationprioritytype,notificationprioritycode,notificationpriorityname,
rowstartdate)
SELECT 1, 1,'Not Set','Not Set','Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_notificationpriority
               WHERE dim_notificationpriorityId = 1);

delete from number_fountain m where m.table_name = 'dim_notificationpriority';
   
insert into number_fountain
select 	'dim_notificationpriority',
	ifnull(max(d.Dim_NotificationPriorityid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_notificationpriority d
where d.Dim_NotificationPriorityid <> 1; 

INSERT INTO dim_notificationpriority(Dim_NotificationPriorityid,
                                     NotificationPriorityTypeCode,
                                     NotificationPriorityType,
                                     NotificationPriorityCode,
                                     NotificationPriorityName,
                                     RowStartDate,
                                     RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_notificationpriority') 
          + row_number() over(order by ''),
			 T356_T_ARTPR,
          T356A_T_ARTPRX,
          T356_T_PRIOK,
          T356_T_PRIOKX,
          current_timestamp,
          1
     FROM T356_T t1, T356A_T t2
    WHERE t1.T356_T_ARTPR = t2.T356A_T_ARTPR
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_notificationpriority s
                  WHERE     s.NotificationPriorityTypeCode = T356_T_ARTPR
                        AND s.NotificationPriorityCode = T356_T_PRIOK
                        AND s.RowIsCurrent = 1)
;
