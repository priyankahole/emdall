UPDATE    dim_revenuerecognitioncategory r
   SET r.Description = ifnull(t.DD07T_DDTEXT, 'Not Set'),
			r.dw_update_date = current_timestamp
       FROM
         dim_revenuerecognitioncategory r, DD07T t			
 WHERE r.RowIsCurrent = 1
 AND t.DD07T_DOMNAME = 'RR_RELTYP'
          AND t.DD07T_DOMVALUE IS NOT NULL
          AND r.Category = ifnull(t.DD07T_DOMVALUE, 'Not Set')
;

INSERT INTO dim_revenuerecognitioncategory(dim_revenuerecognitioncategoryId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_revenuerecognitioncategory
               WHERE dim_revenuerecognitioncategoryId = 1);

delete from number_fountain m where m.table_name = 'dim_revenuerecognitioncategory';

insert into number_fountain
select 	'dim_revenuerecognitioncategory',
	ifnull(max(d.Dim_RevenueRecognitionCategoryId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_revenuerecognitioncategory d
where d.Dim_RevenueRecognitionCategoryId <> 1; 

INSERT INTO dim_revenuerecognitioncategory(Dim_RevenueRecognitionCategoryId,
                               Description,
                               Category,
                               RowStartDate,
                               RowIsCurrent)
        SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_revenuerecognitioncategory') 
          + row_number() over(order by ''),
			 ifnull(DD07T_DDTEXT, 'Not Set'),
            ifnull(DD07T_DOMVALUE,'Not Set'),
            current_timestamp,
            1
       FROM DD07T t
      WHERE DD07T_DOMNAME = 'RR_RELTYP' and DD07T_DOMVALUE is not null
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_revenuerecognitioncategory
                    WHERE Category = ifnull(t.DD07T_DOMVALUE,'Not Set')  AND t.DD07T_DOMNAME = 'RR_RELTYP')
;

