UPDATE    dim_productionscheduler ps
   SET ps.Description = ifnull(T024F_TXT, 'Not Set'),
			ps.dw_update_date = current_timestamp
       FROM dim_productionscheduler ps,
          T024F t
 WHERE ps.RowIsCurrent = 1
      AND ps.ProductionScheduler = t.T024F_FEVOR AND ps.plant = t.T024F_WERKS 
;

INSERT INTO dim_productionscheduler(dim_productionschedulerId, RowIsCurrent,description,rowstartdate)
SELECT 1, 1,'Not Set',current_timestamp
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_productionscheduler
               WHERE dim_productionschedulerId = 1);

delete from number_fountain m where m.table_name = 'dim_productionscheduler';
   
insert into number_fountain
select 	'dim_productionscheduler',
	ifnull(max(d.dim_productionschedulerid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_productionscheduler d
where d.dim_productionschedulerid <> 1; 

INSERT INTO dim_productionscheduler(dim_productionschedulerid,
                                    Description,
                                    ProductionScheduler,
                                    Plant,
                                    RowStartDate,
                                    RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_productionscheduler')
         + row_number() over(order by ''),
			   ifnull(T024F_TXT, 'Not Set'),
                   T024F_FEVOR,
                   T024F_WERKS,
                   current_timestamp,
                   1
     FROM T024F
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM dim_productionscheduler
                   WHERE ProductionScheduler = T024F_FEVOR
                         AND plant = T024F_WERKS and rowiscurrent = 1)
   ORDER BY 2 ;

