merge into dim_producthierarchy dim
using (select t1.DIM_PRODUCTHIERARCHYID, 
			  ifnull(t2.VTEXT, 'Not Set') as Level1Desc,
			  ifnull(t3.VTEXT, 'Not Set') as Level2Desc,
			  ifnull(t4.VTEXT, 'Not Set') as Level3Desc,
			  ifnull(t5.VTEXT, 'Not Set') as Level4Desc,
			  ifnull(t6.VTEXT, 'Not Set') as Level5Desc,
			  ifnull(t7.VTEXT, 'Not Set') as Level6Desc,
			  (CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 3 
					THEN substring(t1.ProductHierarchy, 1, 3)
				ELSE 'Not Set' END) as Level1Code,
			  (CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 6 
					THEN substring(t1.ProductHierarchy, 1, 6)
				ELSE 'Not Set' END) as Level2Code,
			  (CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 9 
					THEN substring(t1.ProductHierarchy, 1, 9)
				ELSE 'Not Set' END) as Level3Code,
			  (CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 12 
					THEN substring(t1.ProductHierarchy, 1, 12)
				ELSE 'Not Set' END) as Level4Code,
			  (CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 15 
					THEN substring(t1.ProductHierarchy, 1, 15)
				ELSE 'Not Set' END) as Level5Code,
			  (CASE WHEN t1.ProductHierarchy <> 'Not Set' AND length(t1.ProductHierarchy) >= 18 
					THEN substring(t1.ProductHierarchy, 1, 18)
				ELSE 'Not Set' END) as Level6Code
	   from DIM_PRODUCTHIERARCHY t1
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 3)
						   where length(ph.ProductHierarchy) >= 3) t2
						on t1.DIM_PRODUCTHIERARCHYID = t2.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 6)
						   where length(ph.ProductHierarchy) >= 6) t3
						on t1.DIM_PRODUCTHIERARCHYID = t3.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 9)
						   where length(ph.ProductHierarchy) >= 9) t4
						on t1.DIM_PRODUCTHIERARCHYID = t4.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 12)
						   where length(ph.ProductHierarchy) >= 12) t5
						on t1.DIM_PRODUCTHIERARCHYID = t5.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 15)
						   where length(ph.ProductHierarchy) >= 15) t6
						on t1.DIM_PRODUCTHIERARCHYID = t6.DIM_PRODUCTHIERARCHYID
				left join (select ph.DIM_PRODUCTHIERARCHYID, ph1.VTEXT
					       from dim_producthierarchy ph
									inner join t179t ph1 on ph1.PRODH = substring(ph.ProductHierarchy, 1, 18)
						   where length(ph.ProductHierarchy) >= 18) t7
						on t1.DIM_PRODUCTHIERARCHYID = t7.DIM_PRODUCTHIERARCHYID
	  ) src on dim.DIM_PRODUCTHIERARCHYID = src.DIM_PRODUCTHIERARCHYID
when matched then update set dim.Level1Desc = src.Level1Desc,
							 dim.Level1Code = src.Level1Code,
							 dim.Level2Desc = src.Level2Desc,
							 dim.Level2Code = src.Level2Code,							 
							 dim.Level3Desc = src.Level3Desc,
							 dim.Level3Code = src.Level3Code,
							 dim.Level4Desc = src.Level4Desc,
							 dim.Level4Code = src.Level4Code,
							 dim.Level5Desc = src.Level5Desc,
							 dim.Level5Code = src.Level5Code,
							 dim.Level6Desc = src.Level6Desc,
							 dim.Level6Code = src.Level6Code,
							 dim.dw_update_date = current_timestamp;
							 