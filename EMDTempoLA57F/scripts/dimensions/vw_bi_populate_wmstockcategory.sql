/* 	Server: QA
	Process Name: WM Stock Category Transfer
	Interface No: 2
*/

INSERT INTO dim_wmstockcategory
(dim_wmstockcategoryid
,RowIsCurrent)
select 1, 1
from (select 1) a
where not exists ( select 'x' from dim_wmstockcategory where dim_wmstockcategoryid = 1);

UPDATE    dim_wmstockcategory wmsc
SET wmsc.Description = ifnull(dt.DD07T_DDTEXT, 'Not Set'),
			wmsc.dw_update_date = current_timestamp
	FROM dim_wmstockcategory wmsc, DD07T dt
WHERE 		wmsc.WMStockCategory = dt.DD07T_DOMVALUE 
AND wmsc.RowIsCurrent = 1
AND dt.DD07T_DOMNAME = 'BESTQ' 
AND dt.DD07T_DOMNAME IS NOT NULL;
 
delete from number_fountain m where m.table_name = 'dim_wmstockcategory';

insert into number_fountain				   
select 	'dim_wmstockcategory',
	ifnull(max(d.dim_wmstockcategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_wmstockcategory d
where d.dim_wmstockcategoryid <> 1;

INSERT INTO dim_wmstockcategory(dim_wmstockcategoryid,
								Description,
                               WMStockCategory,
                               RowStartDate,
                               RowIsCurrent)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_wmstockcategory') 
          + row_number() over(order by '') ,
			ifnull(DD07T_DDTEXT, 'Not Set'),
            DD07T_DOMVALUE,
            current_timestamp,
            1
       FROM DD07T
      WHERE DD07T_DOMNAME = 'BESTQ' 
            AND NOT EXISTS
                  (SELECT 1
                     FROM dim_wmstockcategory
                    WHERE WMStockCategory = DD07T_DOMVALUE AND DD07T_DOMNAME = 'BESTQ')
   ORDER BY 2;
