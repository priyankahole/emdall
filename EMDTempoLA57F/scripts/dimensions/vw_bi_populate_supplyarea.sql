UPDATE    dim_supplyarea a
   SET a.Description = ifnull(PVKT_PVBTX, 'Not Set'),
			a.dw_update_date = current_timestamp
       FROM
          dim_supplyarea a, PVKT p
 WHERE a.RowIsCurrent = 1
       AND a.SupplyArea = p.PVKT_PRVBE 
	   AND a.Plant = p.PVKT_WERKS 
;

INSERT INTO dim_supplyarea(dim_supplyareaId, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_supplyarea
               WHERE dim_supplyareaId = 1);

delete from number_fountain m where m.table_name = 'dim_supplyarea';

insert into number_fountain
select 	'dim_supplyarea',
	ifnull(max(d.Dim_supplyareaid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_supplyarea d
where d.Dim_supplyareaid <> 1;

INSERT INTO dim_supplyarea(Dim_supplyareaid,
                           SupplyArea,
                           Plant,
                           Description,
                           RowStartDate,
                           RowIsCurrent)
        SELECT DISTINCT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_supplyarea') 
          + row_number() over(order by ''),
			   PVKT_PRVBE,
                   PVKT_WERKS,
                   ifnull(PVKT_PVBTX, 'Not Set'),
                   current_timestamp,
                   1
     FROM PVKT p
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_SupplyArea a
               WHERE a.SupplyArea = p.PVKT_PRVBE AND a.Plant = p.PVKT_WERKS)
;
