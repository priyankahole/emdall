
delete from number_fountain m where m.table_name = 'dim_plant';

insert into number_fountain
select 	'dim_plant',
	ifnull(max(d.dim_plantid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from  dim_plant d
where d.dim_plantid <> 1;

/*
insert into dim_plant
(
dim_plantid,
PlantCode,
Name,
plant_emd,
projectsourceid
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_plant') 
          + row_number() over(order by '') ,
SC23001 as PlantCode,
SC23002 as Name,
CONCAT(SC23001,' - ',SC23002)  as plant_emd,
 ifnull((select s.dim_projectsourceid from dim_projectsource s),1) projectsourceid
from ScalaAU_1923_SC23X100 t 
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_plant
                       WHERE PlantCode = t.SC23001);*/
		       
insert into dim_plant
(
dim_plantid,
PlantCode,
Name,
plant_emd,
projectsourceid
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_plant') 
          + row_number() over(order by '') ,
COMPANYCODE as PlantCode,
COMPANYCODE as Name,
CONCAT(COMPANYCODE,' - ',COMPANYCODE)  as plant_emd,
 ifnull((select s.dim_projectsourceid from dim_projectsource s),1) projectsourceid
from dim_company t 
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_plant
                       WHERE PlantCode = t.COMPANYCODE);
		       
		       
		       


delete from EMD586.dim_plant where projectsourceid = 16;
insert into EMD586.dim_plant
(
dim_plantid,
PlantCode,
Name,
plant_emd,
projectsourceid
)
select 
(
dim_plantid,
PlantCode,
Name,
plant_emd,
projectsourceid
)
from  dim_plant
where dim_plantid <>1;
		       




		       


