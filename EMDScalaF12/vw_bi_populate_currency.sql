/* populate dim_currency */


INSERT INTO Dim_Currency(Dim_CurrencyId )
SELECT 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM Dim_Currency
               WHERE Dim_CurrencyId = 1);
			   
delete from number_fountain m where m.table_name = 'Dim_Currency';

drop table if exists tmp_currency;
create table tmp_currency as 
select distinct   CurrencyCode,Currency, row_number() over(partition by CurrencyCode order by CurrencyCode) ro    from emd586.Dim_Currency
order by CurrencyCode;

insert into number_fountain
select 	'Dim_Currency',
	ifnull(max(d.Dim_CurrencyId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from Dim_Currency d
where d.Dim_CurrencyId <> 1;

INSERT INTO Dim_Currency(Dim_Currencyid,CurrencyCode, Currency,PROJECTSOURCEID)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'Dim_Currency') 
          + row_number() over(order by ''),
		 CurrencyCode, Currency, 16 as PROJECTSOURCEID
     FROM tmp_currency c 
    WHERE ro = 1 and NOT EXISTS (SELECT 1
                        FROM dim_currency dc
                       WHERE dc.CurrencyCode = c.CurrencyCode);

delete from number_fountain m where m.table_name = 'Dim_Currency';


delete from EMD586.Dim_Currency where PROJECTSOURCEID = 16;
	insert into EMD586.Dim_Currency
	(
	Dim_Currencyid,CurrencyCode, Currency, PROJECTSOURCEID)
	select 
	Dim_Currencyid,CurrencyCode, Currency,PROJECTSOURCEID
	from Dim_Currency where Dim_Currencyid <> 1;