delete from inventoryaging_RO_1759;
insert into inventoryaging_RO_1759
(
 company,  
    CompanyEMD, 
     LocalMaterialNumber,
     MaterialDescription,
     ExtendedPRODGroup,
     UpperHierarchy,
    BatchNumber,
    PlantEMD,      
    StorageLocationCode,
    DefaultStorageLocation,
    ProcurementType,
    ManufactureDate,
    ExpiryDate,
      OnHandAmount,
      TotalStock,
      SUMQtyInTransit,
      SUMStockInTransfer,
      SUMBlockedStock,
	  inTransit,
	  QCCategory
      )
Select  
    company,  
    CompanyEMD, 
     LocalMaterialNumber,
     MaterialDescription,
     ExtendedPRODGroup,
     UpperHierarchy,
    BatchNumber,
    PlantEMD,      
    StorageLocationCode,
    DefaultStorageLocation,
    ProcurementType,
    ManufactureDate,
    ExpiryDate,
    sum(OnHandAmount) OnHandAmount,
    SUM(TotalStock) TotalStock,
    sum(SUMQtyInTransit) SUMQtyInTransit,
    sum(SUMStockInTransfer) SUMStockInTransfer,
    SUM(SUMBlockedStock) SUMBlockedStock,
	inTransit,
	QCCategory
from (
Select Comp.SubID company, CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
       SC01.SC01001  LocalMaterialNumber,
       CONCAT(SC01002,SC01003) MaterialDescription,
       SC01023 ExtendedPRODGroup,
       left(SC01039,3) UpperHierarchy,
       IFNULL(concat(SC33.BatchNumber,'-',SC33.BatchID),'Not Set') BatchNumber,
       IFNULL(SC33.WhID,'Not Set') PlantEMD,    
  	   IFNULL(SC33.WhID,'Not Set') StorageLocationCode,   
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.BinNumber)), ''), 'NotSet') DefaultStorageLocation,
       IFNULL(SuppCustType.ProcureInfo,'Not Set') ProcurementType,
       CASE WHEN IFNULL(SC33.DateManufact,'') = ''  THEN '1900-01-01' ELSE CAST(SC33.DateManufact AS NVARCHAR(50)) END ManufactureDate,
       IFNULL(Case When SC33.[ExpireDate]in  ('1900-01-01','9999-12-31','9999-12-31') then '9999-01-01' ELSE convert(VARCHAR(50),SC33.[ExpireDate]) END, '9999-01-01') ExpiryDate,
       sum(IFNULL(SC33.BalanceQty,0)) OnHandAmount,
       sum(IFNULL(SC33.BalanceQty,0)) TotalStock,
       0 SUMQtyInTransit,
       0 SUMStockInTransfer,
       0 SUMBlockedStock,
	   0 as inTransit,
	   SC33.QCCategory as QCCategory
from ScalaRO_1759_SC01R100 SC01
Left Join (Select CompanyCode,CompanyName,CMGID SubID  from ScalaInfo_ScaCompanies) Comp
ON 'R1'=Comp.CompanyCode
Left Join (Select SC33001 StockCode, SC33002 WhID, SC33003 BatchID,SC33004 BinNumber,SC33005 BalanceQty, SC33006 AllocQty,SC33007 OrderedQty,SC33008 ReceivedQty, SC33009 BatchNumber,
                        SC33010 LockStatus,SC33013 DateReceived,SC33014 DateManufact,SC33015    DateBestBefo,SC33017 SourForBatch,SC33023 BatCostUnit1,SC33034  BatCostUnit2,SC33038 QCCategory, 
                        SC33039 QCQuantity, SC33045 [ExpireDate], SC33018 SuppCustRet, SC33012 FLAGEmptBatc,SC33022 SupplBatchNo, SC33036 ParentBatcID from ScalaRO_1759_SC33R100 Where SC33012=0) SC33
ON SC33.StockCode =SC01.SC01001
Left Join (Select SC23001 WhID, SC23002 WhDescript from ScalaRO_1759_SC23R100) SC23
ON SC33.WhID = SC23.WhID
Left join (Select A.* From (
                                    Select PL01001 CustSuppCode, PL01002 CustSuppName, PL01057 CustSuppAccCode,SuppInfo.SY24003 ProcureInfo  From ScalaRO_1759_PL01R100 P1
                                    Left Join (Select SY24002,SY24003 From ScalaRO_1759_SY24R100 Where SY24001='CI') SuppInfo
                                    ON P1.PL01057 = SuppInfo.SY24002
                                    Union All
                                    Select SL01001 CustSuppCode, SL01002 CustSuppName, SL01032 CustSuppAccCode,CustInfo.SY24003 ProcureInfo From ScalaRO_1759_SL01R100 S1
                                    Left Join (Select SY24002,SY24003 From ScalaRO_1759_SY24R100 Where SY24001='BQ') CustInfo
                                    ON S1.SL01032 = CustInfo.SY24002
                                    ) A
                                        ) SuppCustType
ON SC33.SuppCustRet = SuppCustType.CustSuppCode 
Group By Comp.SubID,CONCAT(Comp.SubID,' - ',Comp.CompanyName), 
       SC01.SC01001,
       CONCAT(SC01002,SC01003),
       SC01023,
       left(SC01039,3),
       IFNULL(concat(SC33.BatchNumber,'-',SC33.BatchID),'Not Set'),
       IFNULL(SC33.WhID,'Not Set'),    
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.BinNumber)), ''), 'NotSet'),
       IFNULL(NULLIF(LTRIM(RTRIM(SC01.SC01114)), ''), 'NotSet'),
       IFNULL(SuppCustType.ProcureInfo,'Not Set'),
   	   CASE WHEN IFNULL(SC33.DateManufact,'') = ''  THEN '1900-01-01' ELSE CAST(SC33.DateManufact AS NVARCHAR(50)) END   ,  
--IFNULL((Case When SC33.DateManufact=convert(datetime,'1-Jan-1900') then convert(datetime,'1-Jan-1900') ELSE convert(VARCHAR,SC33.DateManufact) END),convert(datetime,'1-Jan-1900')),
       IFNULL(Case When SC33.[ExpireDate]in  ('1900-01-01','9999-12-31','9999-12-31') then '9999-01-01' ELSE convert(VARCHAR(50),SC33.[ExpireDate]) END, '9999-01-01') 
		,SC33.QCCategory
union all
Select Comp.SubID as company,
       CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
       SC01.SC01001  LocalMaterialNumber,
       CONCAT(SC01002,SC01003) MaterialDescription,
       SC01023 ExtendedPRODGroup,
       left(SC01039,3) UpperHierarchy,
       'Not Set' BatchNumber,
       IFNULL(PC03.PC03035,'Not Set') PlantEMD, 
 	   IFNULL(SC23.WhID,'Not Set') StorageLocationCode,    
       'NotSet' DefaultStorageLocation,
       IFNULL(SuppCustType.ProcureInfo,'Not Set') ProcurementType,
       '9999-01-01' ManufactureDate,
       '9999-01-01'  ExpiryDate,      
   	   sum(0)  OnHandAmount,
       SUM(CONVERT(DECIMAL(13,2),PC03010)-CONVERT(DECIMAL(13,2),PC03011)) TotalStock,
       SUM(CONVERT(DECIMAL(13,2),PC03010) - CONVERT(DECIMAL(13,2),PC03011)) SUMQtyInTransit,
       sum(0) SUMStockInTransfer,
       sum(0) SUMBlockedStock,
	   1 as inTransit,
	   'Not Set' as QCCategory
from ScalaRO_1759_PC03R100 PC03
Left Join (Select  
						 CompanyCode
						,CompanyName
						,CMGID SubID  
						from ScalaInfo_ScaCompanies) Comp ON 'R1'=Comp.CompanyCode 
Left Join ScalaRO_1759_SC01R100 SC01 ON SC01.SC01001=PC03005
Left Join (Select 
						 SC23001 WhID
						,SC23002 WhDescript 
				from ScalaRO_1759_SC23R100) SC23 ON PC03.PC03035 = SC23.WhID
Left Join (Select 
						 PC01001 PONo
						,PC01003 SuppCode 
				From ScalaRO_1759_PC01R100) PC01 ON PC03.PC03001 = PC01.PONo
Left join (Select A.* From (Select PL01001 CustSuppCode
										,PL01002 CustSuppName
										,PL01057 CustSuppAccCode
										,SuppInfo.SY24003 ProcureInfo  
									From ScalaRO_1759_PL01R100 P1
                                    Left Join (Select  SY24002
															,SY24003 
													From ScalaRO_1759_SY24R100 Where SY24001='CI') SuppInfo ON P1.PL01057 = SuppInfo.SY24002
                  ) A
                      ) SuppCustType ON PC01.SuppCode = SuppCustType.CustSuppCode    
where  PC03.PC03035 = '02'   
Group By 
	   Comp.SubID,
	   CONCAT(Comp.SubID,' - ',Comp.CompanyName),        
       SC01.SC01001,
       CONCAT(SC01002,SC01003),
       SC01023,
       left(SC01039,3),
       IFNULL(PC03.PC03035,'Not Set'),          
       IFNULL(SuppCustType.ProcureInfo,'Not Set'),
	   SC23.WhID
) A 
Group By company,A.CompanyEMD, 
       A.LocalMaterialNumber,
       A.MaterialDescription,
       A.ExtendedPRODGroup,
       A.UpperHierarchy,
       A.BatchNumber,
       A.PlantEMD,     
       A.StorageLocationCode,
       A.DefaultStorageLocation,
       A.ProcurementType,
       A.ManufactureDate,
       A.ExpiryDate,
       A.OnHandAmount,
       A.TotalStock,
	   A.inTransit,
	   A.QCCategory;
	

/*update inventoryaging_AU_1923
set PlantEMDOriginal = PlantEMD; 

update inventoryaging_AU_1923
set PlantEMD = company*/


/* Insert into invnetoryaging */

delete from number_fountain m where m.table_name = 'fact_inventoryaging';
insert into number_fountain
select  'fact_inventoryaging',
  ifnull(max(d.fact_inventoryagingid), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inventoryaging d
where d.fact_inventoryagingid <> 1;

delete from fact_inventoryaging ;
insert into fact_inventoryaging 
(
fact_inventoryagingid, 
dim_partid,
dim_plantid,
dim_companyid,
dim_storagelocationid,
dim_bwproducthierarchyid,
dim_projectsourceid,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
dim_currencyid,
dim_dateidexpirydate,
amt_OnHand,
ct_StockQty,
ct_TotalRestrictedStock,
ct_StockInQInsp,
ct_StockInTransit,
ct_StockInTransfer,
ct_BlockedStock,
amt_StdUnitPrice,
dd_BatchNo,
DD_BATCHSTATUS,
DD_BATCH_STATUS_QKZ
)
SELECT 
	 (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_inventoryaging')  + row_number() over(order by '')  as fact_inventoryagingid
	,ifnull(dp.dim_partid,1) as dim_partid
	,ifnull(dpl.dim_plantid,1) as dim_plantid
	,ifnull(dc.dim_companyid,1) as dim_companyid
	,ifnull(str.dim_storagelocationid,1) as dim_storagelocationid
	,1 as dim_bwproducthierarchyid
	,16 as dim_projectsourceid
	,1 as AMT_EXCHANGERATE
	,1 as AMT_EXCHANGERATE_GBL
	,ifnull(dim_currencyid,1) as dim_currencyid
	,ifnull(d1.dim_dateid,1) as dim_dateidexpirydate
	,ifnull(t.ONHANDAMOUNT,0 ) as amt_OnHand 
	,case when dw.BATCH_STATUS = 1 then 0
	  	  when dw.BATCH_STATUS = 0 and (ExpiryDate<=current_date or QCCategory in ('99','11')) then 0 
	  	  when t.inTransit = 1 then 0
	 	  else ifnull(t.OnHandAmount,0) end as ct_StockQty
	,case when dw.BATCH_STATUS = 1 then ifnull(t.OnHandAmount,0)
		  when dw.BATCH_STATUS = 0 and (ExpiryDate <= current_date or QCCategory in ('99','11')) then 0
		  when t.inTransit = 1 then 0
		  else 0 end as ct_TotalRestrictedStock
	,case when dw.BATCH_STATUS = 0 and ExpiryDate > current_date and QCCategory in ('99','11') then ifnull(t.OnHandAmount,0)
		  else 0 end as ct_StockInQInsp
	,case when t.inTransit = 1 then ifnull(t.SUMQTYINTRANSIT,0)
		  else 0 end as ct_StockInTransit
	,ifnull(t.SUMSTOCKINTRANSFER,0) as ct_StockInTransfer
	,ifnull(t.SUMBLOCKEDSTOCK,0) as ct_BlockedStock
	,ifnull(al.AVGSTDUNITPRICE,0) as amt_StdUnitPrice
	,ifnull(BatchNumber,'Not Set') as  dd_BatchNo
	,case when dw.BATCH_STATUS = 0 and ExpiryDate <= current_date then 'Restricted'
		  when dw.BATCH_STATUS = 0 and ExpiryDate > current_date and QCCategory in ('99','11') then 'Restricted'
	      when dw.BATCH_STATUS = 1 then 'Restricted' 
	      else 'Unrestricted' end as DD_BATCHSTATUS
	,case when dw.BATCH_STATUS = 0 and ExpiryDate <= current_date then '6' 
		  when dw.BATCH_STATUS = 0 and ExpiryDate > current_date and QCCategory in ('99','11') then '4'
		  when t.inTransit = 1 then '0'
		  else DW.QKZ end as BatchStatusQKZ

from inventoryaging_RO_1759 t
	INNER join dim_warehouse dw on dw.warehousecode = t.PlantEMD
    left join dim_plant dpl on dpl.dim_warehouseid = dw.dim_warehouseid
 	INNER join dim_part dp on dp.partnumber = t.LocalMaterialNumber and dp.storagelocation = PlantEMD
    left join dim_company dc on dc.CompanyCode = t.COMPANY
    left join tmp_mapall al on  t.PlantEMD = al.STORAGELOCATION and t.LocalMaterialNumber = al.LocalMaterialNumber
    left join dim_storagelocation str on t.StorageLocationCode = str.LocationCode --and t.PlantEMD = str.Plant
    left join dim_date d1 on  d1.CompanyCode ='Not Set' and t.ExpiryDate  = d1.datevalue;

update fact_inventoryaging f
set amt_OnHand = amt_StdUnitPrice * amt_OnHand;

update fact_inventoryaging f
set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid,1)
from fact_inventoryaging f,dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode;

update fact_inventoryaging f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_inventoryaging f,dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1;


UPDATE fact_inventoryaging f
SET f.amt_ExchangeRate_GBL = ifnull(r.exrate,1)
,f.dw_update_date = current_timestamp
FROM fact_inventoryaging f, dim_currency dc, EMDSIALUSA490.csv_oprate_sigma r
WHERE f.dim_currencyid = dc.dim_currencyid
AND dc.CurrencyCode = r.fromc;


drop table if exists tmp_updatedate;
create table tmp_updatedate as
select max(dw_update_date) dd_updatedate from  fact_inventoryaging;

update fact_inventoryaging f
set f.dd_updatedate = t.dd_updatedate
from fact_inventoryaging f, tmp_updatedate t
where f.dd_updatedate <> t.dd_updatedate;


UPDATE fact_inventoryaging f SET 
	f.dim_mdg_partid = ifnull(mdg.dim_mdg_partid, 1),
    f.dw_update_date = current_timestamp
FROM fact_inventoryaging f
inner join dim_part dp on dp.dim_partid  =  f.dim_partid
inner join dim_mdg_part mdg on dp.PartNumber  =  mdg.PartNumber
WHERE  
f.dim_mdg_partid <> ifnull(mdg.dim_mdg_partid, 1);



DROP TABLE IF EXISTS csv_cogs;
CREATE TABLE csv_cogs as
SELECT * FROM EMDTempoCC4.csv_cogs;

update fact_inventoryaging f set 
	 f.amt_cogsactualrate_emd = ifnull(c.Z_GCACTF,0)
	,f.amt_cogsfixedrate_emd = ifnull(c.Z_GCFIXF,0)
	,f.amt_cogsfixedplanrate_emd = ifnull(c.Z_GCPLFF,0)
	,f.amt_cogsplanrate_emd = ifnull(c.Z_GCPLRF,0)
	,f.amt_cogsprevyearfixedrate_emd = ifnull(c.Z_GCPYFF,0)
	,f.amt_cogsprevyearrate_emd = ifnull(c.Z_GCPYRF,0)
	,f.amt_cogsprevyearto1_emd = ifnull(c.Z_GCPYTF1,0)
	,f.amt_cogsprevyearto2_emd = ifnull(c.Z_GCPYTF2,0)
	,f.amt_cogsprevyearto3_emd = ifnull(c.Z_GCPYTF3,0)
	,f.amt_cogsturnoverrate1_emd = ifnull(c.Z_GCTOF1,0)
	,f.amt_cogsturnoverrate2_emd = ifnull(c.Z_GCTOF2,0)
	,f.amt_cogsturnoverrate3_emd = ifnull(c.Z_GCTOF3,0)
from fact_inventoryaging f
inner join dim_MDG_Part d on f.dim_MDG_Partid= d.dim_MDG_Partid
inner join dim_company dc on f.dim_companyid = dc.dim_companyid
inner join csv_cogs c on trim(leading '0' from c.Z_REPUNIT) = trim(leading '0' from dc.company) 
and trim(leading '0' from c.PRODUCT) = trim(leading '0' from d.partnumber);

/* Country Hierarchy */
update fact_inventoryaging f
set f.dim_bwhierarchycountryid = dim_con.dim_bwhierarchycountryid
from fact_inventoryaging f
inner join dim_bwproducthierarchy dim_prd on dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
inner join dim_plant dp on dp.dim_plantid = f.dim_plantid
inner join dim_bwhierarchycountry dim_con on dp.country = dim_con.nodehierarchynamelvl7
where 
dim_prd.business in ('DIV-32') and dim_con.internalhierarchyname = '0COUNTRY04' and dim_con.validto = '9999-12-31';
  
update fact_inventoryaging f
set f.dim_bwhierarchycountryid = dim_con.dim_bwhierarchycountryid
from fact_inventoryaging f 
inner join dim_bwproducthierarchy dim_prd on dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
inner join dim_plant dp on dp.dim_plantid = f.dim_plantid
inner join dim_bwhierarchycountry dim_con on dp.country = dim_con.nodehierarchynamelvl7
where 
dim_prd.business  in ('DIV-31','DIV-35','DIV-34') and dim_con.internalhierarchyname = '0COUNTRY05' and dim_con.validto = '9999-12-31'; 

update fact_inventoryaging f
set f.dim_bwhierarchycountryid = dim_con.dim_bwhierarchycountryid
from fact_inventoryaging f
inner join dim_bwproducthierarchy dim_prd on dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
inner join dim_plant dp on dp.dim_plantid = f.dim_plantid
inner join dim_bwhierarchycountry dim_con on dp.country = dim_con.nodehierarchynamelvl7
where 
dim_prd.business  in ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88') 
and dim_con.internalhierarchyname = '0COUNTRY07' and dim_con.validto = '9999-12-31';

update fact_inventoryaging f
set f.dim_bwhierarchycountryid = dim_con.dim_bwhierarchycountryid
from fact_inventoryaging f
inner join dim_bwproducthierarchy dim_prd on dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
inner join dim_plant dp on dp.dim_plantid = f.dim_plantid
inner join dim_bwhierarchycountry dim_con on  dp.country = dim_con.nodehierarchynamelvl7
where 
dim_prd.business  in ('DIV-87') and dim_con.internalhierarchyname = '0COUNTRY08' and dim_con.validto = '9999-12-31';


/* update BatchStatusId */
UPDATE fact_inventoryaging i
SET i.dim_batchstatusid = db.dim_batchstatusid
FROM fact_inventoryaging i
inner join dim_part prt on i.dim_partid = prt.dim_partid 
inner join dim_batchstatus db on i.dd_batchno = db.batchnumber and prt.partnumber = db.partnumber and prt.plant = db.plantcode
and i.dim_batchstatusid <> db.dim_batchstatusid;

/* populate EMD586 */

delete from EMD586.fact_inventoryaging where dim_projectsourceid = 16;
insert into EMD586.fact_inventoryaging 
  (
fact_inventoryagingid, 
dim_partid,
dim_plantid,
dim_companyid,
dim_storagelocationid,
dim_bwproducthierarchyid,
dim_projectsourceid,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
dim_currencyid,
dim_dateidexpirydate,
amt_OnHand,
ct_StockQty,
ct_TotalRestrictedStock,
ct_StockInQInsp,
ct_StockInTransit,
ct_StockInTransfer,
ct_BlockedStock,
amt_StdUnitPrice,
dd_BatchNo,
DD_BATCHSTATUS,
DD_BATCH_STATUS_QKZ,
dd_updatedate,
dim_mdg_partid,
amt_cogsactualrate_emd,
amt_cogsfixedrate_emd,
amt_cogsfixedplanrate_emd,
amt_cogsplanrate_emd,
amt_cogsprevyearfixedrate_emd,
amt_cogsprevyearrate_emd,
amt_cogsprevyearto1_emd,
amt_cogsprevyearto2_emd,
amt_cogsprevyearto3_emd,
amt_cogsturnoverrate1_emd,
amt_cogsturnoverrate2_emd,
amt_cogsturnoverrate3_emd,
ct_baseuomratioPCcustom,
dim_bwhierarchycountryid,
dim_batchstatusid)
  select fact_inventoryagingid, 
dim_partid,
dim_plantid,
dim_companyid,
dim_storagelocationid,
dim_bwproducthierarchyid,
dim_projectsourceid,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
dim_currencyid,
dim_dateidexpirydate,
amt_OnHand,
ct_StockQty,
ct_TotalRestrictedStock,
ct_StockInQInsp,
ct_StockInTransit,
ct_StockInTransfer,
ct_BlockedStock,
amt_StdUnitPrice,
dd_BatchNo,
DD_BATCHSTATUS,
DD_BATCH_STATUS_QKZ,
dd_updatedate,
dim_mdg_partid,
amt_cogsactualrate_emd,
amt_cogsfixedrate_emd,
amt_cogsfixedplanrate_emd,
amt_cogsplanrate_emd,
amt_cogsprevyearfixedrate_emd,
amt_cogsprevyearrate_emd,
amt_cogsprevyearto1_emd,
amt_cogsprevyearto2_emd,
amt_cogsprevyearto3_emd,
amt_cogsturnoverrate1_emd,
amt_cogsturnoverrate2_emd,
amt_cogsturnoverrate3_emd,
ct_baseuomratioPCcustom,
dim_bwhierarchycountryid,
dim_batchstatusid
from fact_inventoryaging 
where dim_projectsourceid = 16;
