delete from number_fountain m where m.table_name = 'dim_batchstatus';
insert into number_fountain
select  'dim_batchstatus',
  ifnull(max(d.DIM_BATCHSTATUSID), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_batchstatus d
where d.DIM_BATCHSTATUSID <> 1;


/* Insert into dim_batchstatus "Not Set" */
insert into dim_batchstatus (
	 dim_batchstatusid
	,batchstatus
	,batch_status_qkz
	,batchnumber
	,partnumber
	,plantcode
	,dw_insert_date
	,dw_update_date
	,rowstartdate
	,rowenddate
	,rowiscurrent
	,rowchangereason
	,projectsourceid
)
SELECT 1
       ,'Not Set'
       ,'Not Set'
       ,'Not Set'
       ,'Not Set'
       ,'Not Set'
	   ,CURRENT_TIMESTAMP
	   ,CURRENT_TIMESTAMP
	   ,null
	   ,null
       ,null
       ,null
	   ,(SELECT dim_projectsourceid from dim_projectsource)
FROM   (SELECT 1) a
WHERE  NOT EXISTS (SELECT 'x'
                   FROM   dim_batchstatus
                   WHERE  dim_batchstatusid = 1);


/* create temporary with the unique batch statuses in order to avoid duplication at update */
DROP TABLE IF EXISTS tmp_batchstatuses;
CREATE TABLE tmp_batchstatuses AS
SELECT distinct
		 ifnull(i.DD_BATCHSTATUS,'Not Set') as batchstatus
		,ifnull(i.DD_BATCH_STATUS_QKZ,'Not Set') as batch_status_qkz
		,ifnull(i.dd_batchno,'Not Set') as batchnumber
		,ifnull(prt.partnumber,'Not Set') as partnumber
		,ifnull(prt.plant,'Not Set') as plantcode
	 FROM fact_inventoryaging i
     inner join dim_part prt on i.dim_Partid = prt.dim_Partid;



/* populate the dimension */
INSERT INTO dim_batchstatus(
	 dim_batchstatusid
	,batchstatus
	,batch_status_qkz
	,batchnumber
	,partnumber
	,plantcode
        ,projectsourceid
)
SELECT
	((SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_batchstatus') + row_number() over (order by ''))
	,tb.*
	,(SELECT dim_projectsourceid from dim_projectsource) as projectsourceid
FROM
	tmp_batchstatuses as tb
WHERE NOT EXISTS (SELECT 1 FROM dim_batchstatus db WHERE db.partnumber=tb.partnumber AND db.plantcode=tb.plantcode AND db.batchnumber=tb.batchnumber);


/* clean temporary tables */
DROP TABLE IF EXISTS tmp_batchstatuses;

delete from EMD586.DIM_BATCHSTATUS where projectsourceid = 16;
insert into EMD586.DIM_BATCHSTATUS
(BATCHNUMBER, 
	BATCHSTATUS, 
	BATCH_STATUS_QKZ, 
	DIM_BATCHSTATUSID, 
	PROJECTSOURCEID, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	PARTNUMBER, 
	PLANTCODE, 
	ROWCHANGEREASON, 
	ROWENDDATE, 
	ROWISCURRENT, 
	ROWSTARTDATE)
select 
BATCHNUMBER, 
	BATCHSTATUS, 
	BATCH_STATUS_QKZ, 
	DIM_BATCHSTATUSID, 
	PROJECTSOURCEID, 
	DW_INSERT_DATE, 
	DW_UPDATE_DATE, 
	PARTNUMBER, 
	PLANTCODE, 
	ROWCHANGEREASON, 
	ROWENDDATE, 
	ROWISCURRENT, 
	ROWSTARTDATE
 from dim_batchstatus
where projectsourceid = 16;

