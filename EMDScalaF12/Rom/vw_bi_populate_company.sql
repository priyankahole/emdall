
delete from number_fountain m where m.table_name = 'dim_company';

insert into number_fountain
select 	'dim_company',
	ifnull(max(d.Dim_Companyid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_company d
where d.Dim_Companyid <> 1;

delete from dim_company;
insert into dim_company
(
Dim_Companyid,
COMPANYCODE,
company,
companyname,
Name2,
projectsourceid
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_company') 
          + row_number() over(order by '') ,
CMGID as COMPANYCODE,
CMGID as company,
companyname as companyname,
'Not Set' as Name2,
16 as projectsourceid
from ScalaInfo_ScaCompanies t 
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_company
                       WHERE CompanyCode = t.COMPANYCODE);


update dim_company
set CountryName = 'Romania'
where companycode = 1759;

update Dim_Company
set Name2 = case when company != 'Not Set' then company || ' - ' || companyname else companyname end
where Name2 <> case when company != 'Not Set' then company || ' - ' || companyname else companyname end; 



/*populate 586 */
delete from EMD586.dim_company where projectsourceid = 16;
insert into EMD586.dim_company
(
Dim_Companyid,
COMPANYCODE,
company,
companyname,
CountryName,
Name2,
projectsourceid
)
select 
(
Dim_Companyid,
COMPANYCODE,
company,
companyname,
CountryName,
Name2,
projectsourceid
)
from  dim_company
where Dim_Companyid <>1;
