drop table tmp_mapall;
create table tmp_mapall as 
Select
   	 CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
	 CONCAT(C.WH,' - ',D.WHDescr) PlantEMD ,
     CONCAT(C.WH,' - ',D.WHDescr) as plantdescritpion,
     case when D.BUSINESS is null then 'Not Set' else D.BUSINESS end as plant,
	 ifnull(D.WHCode,'Not Set') as storagelocation,
     IFNULL(ListCountry.Countryname,'Not Set') PlantCountryName, 
     SC01001 LocalMaterialNumber,
     CONCAT(SC01002,SC01003) MaterialDescription,
     SC01023 ExtendedPRODGroup,
     left(SC01039,3) UpperHierarchy,
     SC01035 PartTypeCode,
      B.AccCodeDesc PartType,
      C.WHSafetyStock SafetyStock,
      C.ReservedStock SumTotalRestrictedStock, 
      C.StockBalance-C.ReservedStock SumUnrestrictedStockQty,
       C.StockBalance SumTotalStock,
       C.StdUnitPrice AVGStdUnitPrice,
       C.StockBalance * C.StdUnitPrice SUMOnHandAmt,
       CompProperty.Val Currency,
       cast (1 as bigint) as dim_currencyid
from ScalaRO_1759_SC01R100 A
Left Outer Join (Select 
						 SY24002 AccCode
						,SY24003 AccCodeDesc 
				from ScalaRO_1759_SY24R100 Where SY24001='IE' ) B ON A.SC01035 = B.AccCode
Left Outer Join (Select  
						 SC03001 MatrlCode
						,SC03002 WH
						,SC03003 StockBalance
						,SC03004 ReservedStock
						,SC03092 WHSafetyStock
						,SC03058 StdUnitPrice 
				from ScalaRO_1759_SC03R100) C ON A.SC01001=C.MatrlCode
Inner Join (Select 
						 SC23001 WHCode
						,SC23002 WHDescr
						,SC23081 WHCountryCode
						,DW.BUSINESS  
				from ScalaRO_1759_SC23R100 w
				inner join DIM_WAREHOUSE DW ON DW.WAREHOUSECODE = W.SC23001) D ON C.WH = D.WHCode
Left outer Join (Select 
						 SY24002 CountryCode
						,SY24003 Countryname 
				from ScalaRO_1759_SY24R100 Where SY24001='BM') ListCountry ON D.WHCountryCode = ListCountry.CountryCode
Left Outer Join (Select 
						 CompanyCode
						,CompanyName
						,CMGID SubID  
				from ScalaInfo_ScaCompanies) Comp ON 'R1'=Comp.CompanyCode
Left Outer Join (Select 
						 distinct Val 
				from ScalaRO_1759_ScaCompanyProperty where PropertyID = 111) CompProperty on 'R1'=Comp.CompanyCode;

update tmp_mapall
  set dim_currencyid = ifnull(dc.dim_currencyid,1)
  from tmp_mapall al 
    left join dim_currency dc on al.Currency = dc.CURRENCYCODE;

/* populate part */

delete from number_fountain m where m.table_name = 'dim_part';

insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from  dim_part d
where d.dim_partid <> 1;


delete from pre_dim_part;
insert into  pre_dim_part
(
partnumber,
plant,
ORG_DESCRIPTION,
PARTTYPE,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
projectsourceid,
STANDARDUNITPRICE,
storagelocation
)
SELECT distinct 
LocalMaterialNumber as partnumber,
ifnull(Plant, 'Not Set') as plant,
CompanyEMD as ORG_DESCRIPTION,
ifnull(PartTypeCode, 'Not Set') as PARTTYPE,
ifnull(PartType, 'Not Set')  as PartTypeDescription,
MaterialDescription as PartDescription,
ifnull(UpperHierarchy,'Not Set') as productgroupsbu,
ifnull(ExtendedPRODGroup,'Not Set') as producthierarchy,
ifnull((select s.dim_projectsourceid from dim_projectsource s),1) as  projectsourceid,
ifnull(AVGSTDUNITPRICE,0)  as STANDARDUNITPRICE,
storagelocation
from  tmp_mapall t ;

delete from dim_part;
insert into   dim_part
(
dim_partid,
partnumber,
PartNumber_NoLeadZero,
plant,
ORG_DESCRIPTION,
PARTTYPE,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
projectsourceid,
STANDARDUNITPRICE,
storagelocation
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') 
          + row_number() over(order by '') ,
partnumber,
partnumber as PartNumber_NoLeadZero,
plant,
ORG_DESCRIPTION,
PARTTYPE,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
projectsourceid,
STANDARDUNITPRICE,
storagelocation
from  pre_dim_part t 
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_part dp
                       WHERE dp.Plant = t.Plant and dp.partnumber = t.partnumber);

update Dim_Part
set productgroupsbu2 = REPLACE(productgroupsbu,'SBU-','')
where productgroupsbu2 <> REPLACE(productgroupsbu,'SBU-','');


update dim_part dp
set dp.gpsname = CASE 
					WHEN (lower(bw.MRKHC_BRAND) like '%crinone%' or lower(bw.MRKHC_BRAND) like '%cetrotide%') and lower(dp.productgroupsbu)='304' THEN 'External Fertility'  
                                        WHEN lower(bw.MRKHC_BRAND) like '%campral%' and lower(dp.productgroupsbu)='973' THEN 'Diabetes / Campral'
					WHEN upper(gd.gpsname)='NOT SET' THEN 'Not Set'
					ELSE gd.GPSName 
				 END
from dim_part dp, EMDTEMPOCC4.csv_gpsdata gd, dim_bwproducthierarchy bw
where RIGHT(gd.SBU,3) = dp.productgroupsbu
	and dp.producthierarchy = bw.lowerhierarchycode
	and dp.productgroupsbu = bw.upperhierarchycode
	and to_date('2018-12-28') between bw.upperhierstartdate and bw.upperhierenddate;


update dim_part dp set
		dp.MDGM_MATERIAL_NO = ifnull(mdg.partnumber,'Not Set')
from dim_part dp
left join dim_mdg_part mdg on mdg.partnumber = dp.partnumber
where ifnull(dp.MDGM_MATERIAL_NO,'') <> ifnull(mdg.partnumber,'Not Set');


UPDATE dim_part dp SET
dp.ownership = CASE
				WHEN upper(bw.businessdesc) = upper('CONSUMER HEALTH') THEN 'CH'
				WHEN dmp.partnumber IN ('5021028813', '3023318813', '3023318814', 'BN1A5H52', '5021008813') THEN 'SNO'
			    WHEN dpl.plantcode = 'UY01' THEN 'SNO'
			    WHEN upper(dmp.IMCCODE) in ('1B','2B','3B','4','5') and dp.productgroupsbu2 = '975' and bw.BUSINESSFIELD ='BD-A03' then 'GBMD'
				WHEN upper(dmp.IMCCODE) in ('1A', '1B','1C','1D','2A','2B','2C') 
				AND manufacturingSite in ( 'China (Nantong)','Semoy (FRANCE)', 'Mollet (SPAIN)','Mexico City (MEXICO)'
										,'Rio (BRAZIL)','Darmstadt (GERMANY)','Meyzieu (FRANCE)','Calais (FRANCE)','Merck & CIE (SWITZ)') THEN 'GPO'
				WHEN DMP.IMCCODE IN ('1A','1B','1D','2A','2C','3A','3C','3D','3E','6') THEN 'SNO'
				WHEN DMP.IMCCODE IN ('1C','2B','3B','4','5') THEN 'GBMD'
				ELSE 'Not Set'
				END

FROM dim_part dp
INNER join dim_bwproducthierarchy bw on dp.producthierarchy = bw.lowerhierarchycode and dp.productgroupsbu = bw.upperhierarchycode
INNER join dim_mdg_part dmp on right('000000000000000000' || dp.partnumber,18) = right('000000000000000000' || dmp.partnumber,18)
INNER join (select distinct 
					 plantcode
					,MANUFACTURINGSITE 
			from dim_plant) dpl on dpl.plantcode = dp.plant
WHERE 
upper(bw.businesssector) = upper('BS-01')
AND to_date('2018-12-28') BETWEEN bw.upperhierstartdate AND bw.upperhierenddate;



delete from EMD586.dim_part where projectsourceid = 16;
insert into EMD586.dim_part
(
dim_partid,
partnumber,
PartNumber_NoLeadZero,
plant,
ORG_DESCRIPTION,
PARTTYPE,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
productgroupsbu2,
STANDARDUNITPRICE,
projectsourceid,
gpsname,
storagelocation,
MDGM_MATERIAL_NO,
ownership
)
select 
(
dim_partid,
partnumber,
PartNumber_NoLeadZero,
plant,
ORG_DESCRIPTION,
PARTTYPE,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
productgroupsbu2,
STANDARDUNITPRICE,
projectsourceid,
gpsname,
storagelocation,
MDGM_MATERIAL_NO,
ownership
)
from  dim_part
where dim_partid <>1;
