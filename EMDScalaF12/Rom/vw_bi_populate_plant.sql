
delete from number_fountain m where m.table_name = 'dim_plant';

insert into number_fountain
select  'dim_plant',
  ifnull(max(d.dim_plantid), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from  dim_plant d
where d.dim_plantid <> 1;

insert into dim_plant
(
dim_plantid,
PlantCode,
Name,
plant_emd,
Country,
CountryName,
projectsourceid,
DIM_WAREHOUSEID
)
SELECT 
(select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_plant') + row_number() over(order by '') as dim_plantid
,dw.Business as PlantCode
,SC23002 as Name
,CONCAT(SC23001,' - ',SC23002)  as plant_emd
,SY24002 as Country
,InitCap(SY24003) as CountryName
,ifnull((select s.dim_projectsourceid from dim_projectsource s),1) projectsourceid
,dw.DIM_WAREHOUSEID
from ScalaRO_1759_SC23R100 t
inner join DIM_WAREHOUSE dw on dw.warehousecode =  t.SC23001
cross join ScalaRO_1759_SY24R100 Where SY24001='BM' and SY24002 ='RO'
and NOT EXISTS (SELECT 1
                FROM dim_plant
                WHERE PlantCode = CONCAT('1759',' ', dw.business));
   

 /* Update the manufacturingSite field (Merck HC Manufacturing Site) APP-6081 */
UPDATE dim_plant dpl
SET dpl.manufacturingSite = ifnull(mp.manufacturingSite,'Not Set'),
  dpl.plantrole = ifnull(mp.plantrole,'Commercial')
from dim_plant dpl
	left outer join EMDTEMPOCC4.manufacturingsite_mapping mp on dpl.plantcode = mp.plantcode;  

     
delete from EMD586.dim_plant where projectsourceid = 16;
insert into EMD586.dim_plant
(
dim_plantid,
PlantCode,
Name,
plant_emd,
Country,
CountryName,
projectsourceid,
plantrole
)
select 
(
dim_plantid,
PlantCode,
Name,
plant_emd,
Country,
CountryName,
projectsourceid,
plantrole
)
from  dim_plant
where dim_plantid <>1;
