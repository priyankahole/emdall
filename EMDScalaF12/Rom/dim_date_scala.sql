CREATE TABLE dim_date 
LIKE emdtempocc4.dim_date INCLUDING DEFAULTS INCLUDING IDENTITY; 
  
INSERT INTO dim_date
SELECT * FROM emdtempocc4.dim_date WHERE companycode = 'Not Set';

UPDATE dim_date AS d 
SET projectsourceid = 16;

UPDATE dim_date AS D 
SET dim_dateid = TO_NUMBER(CONCAT('16',to_char(RIGHT(dim_dateid,LEN(dim_dateid)-2))))
WHERE dim_dateid <> 1;




DELETE FROM emd586.dim_date 
WHERE projectsourceid = 16;

INSERT INTO emd586.dim_date
SELECT * FROM dim_date;

INSERT INTO emd586.dim_date(businessdaysseqno, calendarmonthid, calendarmonthnumber, calendarquarter, calendarquarterid, calendarquartername, calendarweek, calendarweekid, calendarweekyr, calendarweekyr2, calendaryear, companycode, currentperiodname, datename, datevalue, dayflag, dayofcalendaryear, dayoffinancialyear, dayofmonth, daysincalendaryear, daysincalendaryearsofar, daysinfinancialyear, daysinfinancialyearsofar, daysinmonth, daysinmonthsofar, dim_dateid, dw_insert_date, dw_update_date, financialhalf, financialhalfenddate, financialhalfid, financialhalfname, financialhalfstartdate, financialmonthenddate, financialmonthid, financialmonthnumber, financialmonthstartdate, financialmonthyear, financialquarter, financialquarterid, financialquartername, financialweek, financialweekid, financialyear, financialyearenddate, financialyearstartdate, fiscalyearendflag, fiscalyearstartflag, isaleapyear, isaleapyear2, isapublicholiday, isapublicholiday2, isaspecialday, isaweekendday, isaweekendday2, juliandate, monthabbreviation, monthenddate, monthflag, monthname, monthstartdate, monthyear, projectsourceid, quarterflag, season, weekdayabbreviation, weekdayname, weekdaynumber, weekdaysincalendaryear, weekdaysincalendaryearsofar, weekdaysinfinancialyear, weekdaysinfinancialyearsofar, weekdaysinmonth, weekdaysinmonthsofar, weekenddate, weekenddatethursday, weekendflag, weekflag, weekstartdate, weekstartflag, workdaysincalendaryear, workdaysincalendaryearsofar, workdaysinfinancialyear, workdaysinfinancialyearsofar, workdaysinmonth, workdaysinmonthsofar, yearflag, calendarmonthid2)
SELECT businessdaysseqno, calendarmonthid, calendarmonthnumber, calendarquarter, calendarquarterid, calendarquartername, calendarweek, calendarweekid, calendarweekyr, calendarweekyr2, calendaryear, companycode, currentperiodname, datename, datevalue, dayflag, dayofcalendaryear, dayoffinancialyear, dayofmonth, daysincalendaryear, daysincalendaryearsofar, daysinfinancialyear, daysinfinancialyearsofar, daysinmonth, daysinmonthsofar, dim_dateid, dw_insert_date, dw_update_date, financialhalf, financialhalfenddate, financialhalfid, financialhalfname, financialhalfstartdate, financialmonthenddate, financialmonthid, financialmonthnumber, financialmonthstartdate, financialmonthyear, financialquarter, financialquarterid, financialquartername, financialweek, financialweekid, financialyear, financialyearenddate, financialyearstartdate, fiscalyearendflag, fiscalyearstartflag, isaleapyear, isaleapyear2, isapublicholiday, isapublicholiday2, isaspecialday, isaweekendday, isaweekendday2, juliandate, monthabbreviation, monthenddate, monthflag, monthname, monthstartdate, monthyear, projectsourceid, quarterflag, season, weekdayabbreviation, weekdayname, weekdaynumber, weekdaysincalendaryear, weekdaysincalendaryearsofar, weekdaysinfinancialyear, weekdaysinfinancialyearsofar, weekdaysinmonth, weekdaysinmonthsofar, weekenddate, weekenddatethursday, weekendflag, weekflag, weekstartdate, weekstartflag, workdaysincalendaryear, workdaysincalendaryearsofar, workdaysinfinancialyear, workdaysinfinancialyearsofar, workdaysinmonth, workdaysinmonthsofar, yearflag, calendarmonthid2
FROM dim_date 
;
