drop table if exists tmp_sales_ScalaAU_1923X100;
Create table tmp_sales_ScalaAU_1923X100 as 
Select 
	'Global Scala' [Platform],
	OR03.WhID PlantCode,
	SC23.WHDesc Plantname,
	OR03.AccCode AccCodeSBU,
	CASE WHEN OR01021 IS NULL OR LTRIM(RTRIM(OR01021))='' THEN 'Not Set' ELSE OR01021 END BillingDoc,
	OR01022 BillingDate,
	OR03.OrderNo SalesDocNo,
    OR03.OrderLine SalesDocItem,
	OR03.DelDateReq SalesDocSchedule,
	OR01072 BusinessCustomerPO,
	ScalaInfo.CMGID Company,
	OR03.StockCode PartNumber,
	OR03.StockDesc Product,
	StockInfo.AccCodeDesc [MaterialTypeforGlobalRepDesc],
/*	StockStatus.StatusDesc ItemCategory,
	StockStatus.StatusCode ItemCategoryCode, */
	OR01002 DocumentTypeCode,
	CASE OR01002
		WHEN '0' Then 'Quotation' 
	     WHEN '1' Then 'Normal Order' 
		 WHEN '2' Then 'Invoice Order' 
		 WHEN '3' Then 'Direct Order' 
		 WHEN '4' Then 'Back Order' 
		 WHEN '5' Then 'Repeat Order' 
		 WHEN '6' Then 'Work Order' 
		 WHEN '7' Then 'Direct Credit Order' 
		 WHEN '8' Then 'Credit Note Order' 
		 WHEN '9' Then 'Re-invoice Order' ELSE 
		 'Not Set' END DocType,
	IFNULL(DistChannel.DistributionChannel,'Not Set') ChannelCode,
	CASE DistChannel.DistributionChannel
		WHEN '40' Then 'Domestic Sales (dir.)'
		WHEN '41' Then 'Wholesalers (distr.)'
		WHEN '60' Then 'Export Direct Cust.'
		WHEN '70' Then 'Interco sales'
		ELSE 'Not Set'
	END ChannelName,
	OR01015 SOCreateDate,
	OR03.DelDateReq OriginalRequestedDate,
	OR03.OrgConfDelDate PromiseDeliveryDate,
	OR19.DelDatActual  GoodIssueDate,
	OR19.DelDatActual ActualGoodIssuedate,
	OR03.ConfDelDate ConfirmedDeliveryDateEMD,
	--'Not Set' MaterialAvailDate,	
	--'MASTER DATA' MRKHC_BRAND,
	--'Not Set' TradingPartner,
	IFNULL(OR01003,'Not Set') SoldToPartycustNo,
	IFNULL(SL01.CustName,'Not Set') SoldToPartycustName,
	IFNULL(SL01DelCust.CustCode,'Not Set') ShipToPartyustomerNumber,
	IFNULL(SL01DelCust.CustName,'Not Set')  ShipToPartyustomerName,
	--'Not Set' BusinessSectorName,
	--'MasterData' BusinessFieldName,
	--'MasterData' BusinessLineName,
	--'MasterData' ProductGroupName,
	OR03.OrderQty OrderQty,
	OR03.UnitPrice UnitPrice,
	OR03.UOMCode UOM,
	OR03.QtyShipped ShippedQty,
	OR03.OrderQty-OR03.QtyShipped ShipAgainstOrderQty,
	OR03.OrderQty-OR03.QtyShipped OpenOrderQty,
	OR03.OrderQty * OR03.UnitPrice OrderAmount,
	OR03.UnitPrice * OR03.QtyShipped DeliveryAmount,
	CASE WHEN LTRIM(RTRIM(OR19.BatchNo))='' OR OR19.BatchNo IS NULL THEN 'Not Set' ELSE  OR19.BatchNo END DeliveryBatchNo
from ScalaAU_1923_OR01X100 OR01
Left Outer Join (Select * from ScalaInfo_ScaCompanies) ScalaInfo
ON 'Q1' = ScalaInfo.CompanyCode
Left Outer Join (Select OR03001 OrderNo, OR03002 OrderLine, OR03005 StockCode, OR03006 StockDesc, OR03046 WhID, OR03037 DelDateReq,
                        OR03116 OrgConfDelDate, OR03114 ConfDelDate, OR03011 OrderQty, OR03008 UnitPrice, OR03010 UOMCode,
						OR03012 QtyShipped,OR03028 AccCode   from ScalaAU_1923_OR03X100) OR03
ON OR01001 = OR03.OrderNo
Left Outer Join (Select OR19001 OrderNo,OR19002 OrderLine,OR19003 StructLineNo,OR19004 AutoStructLineNo, OR19005 BatchNo, OR19006 QtyRequested,
                        OR19007 QtyDeliver, OR19008 QtyProposDel, OR19009 DelDatReques, OR19010 DelDatCommit, OR19011 DelDatActual, OR19030 QtyDelivered
					 From ScalaAU_1923_OR19X100) OR19
ON OR03.OrderNo = OR19.OrderNo AND OR03.OrderLine = OR19.OrderLine 
Left Outer Join (Select SC23001 WHID,SC23002 WHDesc from ScalaAU_1923_SC23X100) SC23
ON OR03.WhID = SC23.WHID
Left outer Join (Select SC01001 StockCode, SC01002 StockDesc, SC01035 StockAccCode, SC01066 StockStatus From ScalaAU_1923_SC01X100) SC01
ON SC01.StockCode = OR03.StockCode
Left Outer Join (Select SY24002 AccCode,SY24003 AccCodeDesc from ScalaAU_1923_SY24X100 Where SY24001='IE' ) StockInfo
ON SC01.StockAccCode = StockInfo.AccCode
/* Left Outer Join (Select SY24002 StatusCode,SY24003 StatusDesc from ScalaAU_1923_SY24X100 Where SY24001='IC' ) StockStatus
ON SC01.StockStatus = StockStatus.StatusCode */
Left Outer Join (Select SL01001 CustCode, SL01002 CustName from ScalaAU_1923_SL01X100) SL01
ON OR01003 = SL01.CustCode
Left Outer Join (Select SL01001 CustCode, DistributionChannel From GS_EN_UDD_ATR_0027_L_X100) DistChannel
On SL01.CustCode = DistChannel.CustCode
Left Outer Join (Select SL01001 CustCode, SL01002 CustName from ScalaAU_1923_SL01X100) SL01DelCust
ON OR01004 = SL01DelCust.CustCode

Union All

Select 
	'Global Scala' [Platform],
	OR21.WhID PlantCode,
	SC23.WHDesc Plantname,
	OR21.AccCode AccCodeSBU,
	CASE WHEN OR20021 IS NULL OR LTRIM(RTRIM(OR20021))='' THEN 'Not Set' ELSE OR20021 END BillingDoc,
	OR20022 BillingDate,
	OR21.OrderNo SalesDocNo,
    OR21.OrderLine SalesDocItem,
	OR21.DelDateReq SalesDocSchedule,
	OR20072 BusinessCustomerPO,
	ScalaInfo.CMGID Company,
	OR21.StockCode PartNumber,
	OR21.StockDesc Product,
	StockInfo.AccCodeDesc [MaterialTypeforGlobalRepDesc],
/*	StockStatus.StatusDesc ItemCategory,
	StockStatus.StatusCode ItemCategoryCode,*/
	OR20002 DocumentTypeCode,
	CASE OR20002
		WHEN '0' Then 'Quotation' 
	     WHEN '1' Then 'Normal Order' 
		 WHEN '2' Then 'Invoice Order' 
		 WHEN '3' Then 'Direct Order' 
		 WHEN '4' Then 'Back Order' 
		 WHEN '5' Then 'Repeat Order' 
		 WHEN '6' Then 'Work Order' 
		 WHEN '7' Then 'Direct Credit Order' 
		 WHEN '8' Then 'Credit Note Order' 
		 WHEN '9' Then 'Re-invoice Order' ELSE 
		 'Not Set' END DocType,
	IFNULL(DistChannel.DistributionChannel,'Not Set') ChannelCode,
	CASE DistChannel.DistributionChannel
		WHEN '40' Then 'Domestic Sales (dir.)'
		WHEN '41' Then 'Wholesalers (distr.)'
		WHEN '60' Then 'Export Direct Cust.'
		WHEN '70' Then 'Interco sales'
		ELSE 'Not Set'
	END ChannelName,
	OR20015 SOCreateDate,
	OR21.DelDateReq OriginalRequestedDate,
	OR21.OrgConfDelDate PromiseDeliveryDate,
	OR23.DelDatActual  GoodIssueDate,
	OR23.DelDatActual ActualGoodIssuedate,
	OR21.ConfDelDate ConfirmedDeliveryDateEMD,
	--'Not Set' MaterialAvailDate,	
	--'MASTER DATA' MRKHC_BRAND,
	--'Not Set' TradingPartner,
	IFNULL(OR20003,'Not Set') SoldToPartycustNo,
	IFNULL(SL01.CustName,'Not Set') SoldToPartycustName,
	IFNULL(SL01DelCust.CustCode,'Not Set') ShipToPartyustomerNumber,
	IFNULL(SL01DelCust.CustName,'Not Set')  ShipToPartyustomerName,
	--'Not Set' BusinessSectorName,
	--'MasterData' BusinessFieldName,
	--'MasterData' BusinessLineName,
	--'MasterData' ProductGroupName,
	OR21.OrderQty OrderQty,
	OR21.UnitPrice UnitPrice,
	OR21.UOMCode UOM,
	OR21.QtyShipped ShippedQty,
	OR21.OrderQty-OR21.QtyShipped ShipAgainstOrderQty,
	OR21.OrderQty-OR21.QtyShipped OpenOrderQty,
	OR21.OrderQty * OR21.UnitPrice OrderAmount,
	OR21.UnitPrice * OR21.QtyShipped DeliveryAmount,
	CASE WHEN LTRIM(RTRIM(OR23.BatchNo))='' OR OR23.BatchNo IS NULL THEN 'Not Set' ELSE  OR23.BatchNo END DeliveryBatchNo
from ScalaAU_1923_OR20X100 OR20
Left Outer Join (Select * from ScalaInfo_ScaCompanies) ScalaInfo
ON 'Q1' = ScalaInfo.CompanyCode
Left Outer Join (Select OR21001 OrderNo, OR21002 OrderLine, OR21005 StockCode, OR21006 StockDesc, OR21046 WhID, OR21037 DelDateReq,
                        OR21116 OrgConfDelDate, OR21114 ConfDelDate, OR21011 OrderQty, OR21008 UnitPrice, OR21010 UOMCode,
						OR21012 QtyShipped, OR21028 AccCode from ScalaAU_1923_OR21X100) OR21
ON OR20001 = OR21.OrderNo
Left Outer Join (Select OR23001 OrderNo,OR23002 OrderLine,OR23003 StructLineNo,OR23004 AutoStructLineNo, OR23005 BatchNo, OR23006 QtyRequested,
                        OR23007 QtyDeliver, OR23008 QtyProposDel, OR23009 DelDatReques, OR23010 DelDatCommit, OR23011 DelDatActual, OR23030 QtyDelivered
					 From ScalaAU_1923_OR23X100) OR23
ON OR21.OrderNo = OR23.OrderNo AND OR21.OrderLine = OR23.OrderLine 
Left Outer Join (Select SC23001 WHID,SC23002 WHDesc from ScalaAU_1923_SC23X100) SC23
ON OR21.WhID = SC23.WHID
Left outer Join (Select SC01001 StockCode, SC01002 StockDesc, SC01035 StockAccCode, SC01066 StockStatus From ScalaAU_1923_SC01X100) SC01
ON SC01.StockCode = OR21.StockCode
Left Outer Join (Select SY24002 AccCode,SY24003 AccCodeDesc from ScalaAU_1923_SY24X100 Where SY24001='IE' ) StockInfo
ON SC01.StockAccCode = StockInfo.AccCode
/* Left Outer Join (Select SY24002 StatusCode,SY24003 StatusDesc from ScalaAU_1923_SY24X100 Where SY24001='IC' ) StockStatus
ON SC01.StockStatus = StockStatus.StatusCode */
Left Outer Join (Select SL01001 CustCode, SL01002 CustName from ScalaAU_1923_SL01X100) SL01
ON OR20003 = SL01.CustCode
Left Outer Join (Select SL01001 CustCode, DistributionChannel From GS_EN_UDD_ATR_0027_L_X100) DistChannel
On SL01.CustCode = DistChannel.CustCode
Left Outer Join (Select SL01001 CustCode, SL01002 CustName from ScalaAU_1923_SL01X100) SL01DelCust
ON OR20004 = SL01DelCust.CustCode;




/* Populate sales */

DELETE FROM number_fountain AS m WHERE m.table_name = 'fact_salesorder';
INSERT INTO number_fountain
SELECT 'fact_salesorder',
  IFNULL(MAX(d.fact_salesorderid), 
  IFNULL((SELECT s.dim_projectsourceid * s.multiplier FROM dim_projectsource s),1))
FROM fact_salesorder d
WHERE d.fact_salesorderid <> 1;

DROP TABLE IF EXISTS tmp_fact_salesorder;

CREATE TABLE tmp_fact_salesorder
AS 
SELECT 
  (SELECT IFNULL(m.max_id, 1) 
    FROM number_fountain m 
    WHERE m.table_name = 'fact_salesorder') + ROW_NUMBER() OVER(ORDER BY '') AS fact_salesorderid,
  IFNULL(salesdocno,'Not Set') AS dd_salesdocno,
  IFNULL(salesdocitem,1) AS dd_salesitemno,
  '1' AS dd_scheduleno ,
  16 AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  IFNULL(dp.dim_partid,1) AS dim_partid,
  IFNULL(dpl.dim_plantid,1) AS dim_plantid,
  IFNULL(dc.dim_companyid,1) AS dim_companyid,
  IFNULL(orderqty,0) AS ct_ScheduleQtySalesUnit,
  IFNULL(shippedqty,0) AS ct_DeliveredQty,
  IFNULL(shipagainstorderqty, 0) AS ct_ShippedAgnstOrderQty,
  IFNULL(unitprice, 0) AS amt_UnitPrice,
  IFNULL(trim(leading '0' FROM deliverybatchno),'Not Set') AS dd_BatchNo,
  IFNULL(socreatedate,'0001-01-01') AS dd_socreatedate,
  CAST(1 AS BIGINT) AS dim_dateidsocreated,
  IFNULL(originalrequesteddate,'0001-01-01') AS dd_originalrequesteddate,
  CAST(1 AS BIGINT) AS dim_dateidscheddlvrreqprev,
  IFNULL(promisedeliverydate,'0001-01-01') AS dd_promisedeliverydate,
  CAST(1 AS BIGINT) AS dim_dateidscheddelivery,
  IFNULL(goodissuedate,'0001-01-01') AS dd_goodissuedate,
  CAST(1 AS BIGINT) AS dim_dateidgoodsissue,
  IFNULL(actualgoodissuedate,'0001-01-01') AS dd_actualgoodissuedate,
  CAST(1 AS BIGINT) AS dim_dateidactualgi,
  IFNULL(confirmeddeliverydateemd,'0001-01-01') AS dd_confirmeddeliverydateemd,
  CAST(1 AS BIGINT) AS dim_dateidconfirmeddelivery_emd
FROM tmp_sales_ScalaAU_1923X100 AS t
  LEFT JOIN dim_part AS dp ON dp.Plant = t.Plantcode AND dp.partnumber = t.PARTNUMBER
  LEFT JOIN dim_plant AS dpl ON dpl.PlantCode = t.Plantcode
  LEFT JOIN dim_company AS dc ON dc.CompanyCode = t.COMPANY
;

UPDATE tmp_fact_salesorder AS f
SET f.dim_dateidsocreated = dt.dim_dateid
FROM dim_date AS dt, tmp_fact_salesorder AS f	
WHERE dt.datevalue = f.dd_socreatedate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidsocreated <> dt.dim_dateid;
  
UPDATE tmp_fact_salesorder AS f
SET f.dim_dateidscheddlvrreqprev = dt.dim_dateid
FROM dim_date AS dt, tmp_fact_salesorder AS f	
WHERE dt.datevalue = f.dd_originalrequesteddate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidscheddlvrreqprev <> dt.dim_dateid;
  
UPDATE tmp_fact_salesorder AS f
SET f.dim_dateidscheddelivery = dt.dim_dateid
FROM dim_date AS dt, tmp_fact_salesorder AS f	
WHERE dt.datevalue = f.dd_promisedeliverydate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidscheddelivery <> dt.dim_dateid;
  
UPDATE tmp_fact_salesorder AS f
SET f.dim_dateidgoodsissue = dt.dim_dateid
FROM dim_date AS dt, tmp_fact_salesorder AS f	
WHERE dt.datevalue = f.dd_goodissuedate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidgoodsissue <> dt.dim_dateid;
  
UPDATE tmp_fact_salesorder AS f
SET f.dim_dateidactualgi = dt.dim_dateid
FROM dim_date AS dt, tmp_fact_salesorder AS f	
WHERE dt.datevalue = f.dd_actualgoodissuedate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidactualgi <> dt.dim_dateid;
  
UPDATE tmp_fact_salesorder AS f
SET f.dim_dateidconfirmeddelivery_emd = dt.dim_dateid
FROM dim_date AS dt, tmp_fact_salesorder AS f	
WHERE dt.datevalue = f.dd_confirmeddeliverydateemd
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidconfirmeddelivery_emd <> dt.dim_dateid;
    
DELETE FROM fact_salesorder;

INSERT INTO fact_salesorder
(
  fact_salesorderid,
  dd_salesdocno,
  dd_salesitemno,
  dd_scheduleno, 
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_partid,
  dim_plantid,
  dim_companyid,
  ct_scheduleqtysalesunit,
  ct_deliveredqty,
  ct_shippedagnstorderqty,
  amt_unitprice,
  dd_batchno,
  dim_dateidsocreated,
  dim_dateidscheddlvrreqprev,
  dim_dateidscheddelivery,
  dim_dateidgoodsissue,
  dim_dateidactualgi,
  dim_dateidconfirmeddelivery_emd
) 
SELECT 
  fact_salesorderid,
  dd_salesdocno,
  dd_salesitemno,
  dd_scheduleno, 
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_partid,
  dim_plantid,
  dim_companyid,
  ct_scheduleqtysalesunit,
  ct_deliveredqty,
  ct_shippedagnstorderqty,
  amt_unitprice,
  dd_batchno,
  dim_dateidsocreated,
  dim_dateidscheddlvrreqprev,
  dim_dateidscheddelivery,
  dim_dateidgoodsissue,
  dim_dateidactualgi,
  dim_dateidconfirmeddelivery_emd
FROM tmp_fact_salesorder;

DROP TABLE IF EXISTS tmp_fact_salesorder;

DELETE FROM emd586.fact_salesorder 
WHERE dim_projectsourceid = 16;

INSERT INTO emd586.fact_salesorder
(
  fact_salesorderid,
  dd_salesdocno,
  dd_salesitemno,
  dd_scheduleno, 
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_partid,
  dim_plantid,
  dim_companyid,
  ct_scheduleqtysalesunit,
  ct_deliveredqty,
  ct_shippedagnstorderqty,
  amt_unitprice,
  dd_batchno,
  dim_dateidsocreated,
  dim_dateidscheddlvrreqprev,
  dim_dateidscheddelivery,
  dim_dateidgoodsissue,
  dim_dateidactualgi,
  dim_dateidconfirmeddelivery_emd
) 
SELECT 
  fact_salesorderid,
  dd_salesdocno,
  dd_salesitemno,
  dd_scheduleno, 
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_partid,
  dim_plantid,
  dim_companyid,
  ct_scheduleqtysalesunit,
  ct_deliveredqty,
  ct_shippedagnstorderqty,
  amt_unitprice,
  dd_batchno,
  dim_dateidsocreated,
  dim_dateidscheddlvrreqprev,
  dim_dateidscheddelivery,
  dim_dateidgoodsissue,
  dim_dateidactualgi,
  dim_dateidconfirmeddelivery_emd
FROM fact_salesorder;