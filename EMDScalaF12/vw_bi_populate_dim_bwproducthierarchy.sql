/*drop table if exists tmp_hierarchy;
create table tmp_hierarchy as 
select distinct left(SC01039,3) as UPPERHIERARCHY from ScalaAU_1923_SC01X100 where left(SC01039,3) is not null;


delete from number_fountain m where m.table_name = 'dim_bwproducthierarchy';

insert into number_fountain
select  'dim_bwproducthierarchy',
  ifnull(max(d.dim_bwproducthierarchyid), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from  dim_bwproducthierarchy d
where d.dim_bwproducthierarchyid <> 1; 

insert into dim_bwproducthierarchy
(
dim_bwproducthierarchyid,
upperhierarchycode,
productgroup,
PROJECTSOURCEID
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_bwproducthierarchy') 
          + row_number() over(order by '') DIM_BWPRODUCTHIERARCHYID,
ifnull(UPPERHIERARCHY, 'Not Set') as upperhierarchycode,
concat ('SBU -',ifnull(UPPERHIERARCHY, 'Not Set')) as productgroup,
16 as PROJECTSOURCEID
from tmp_hierarchy t 
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_bwproducthierarchy
                       WHERE ifnull(t.UPPERHIERARCHY, 'Not Set') = upperhierarchycode);


delete from EMD586.dim_bwproducthierarchy where PROJECTSOURCEID =16;
insert into EMD586.dim_bwproducthierarchy
(
dim_bwproducthierarchyid,
upperhierarchycode,
productgroup,
PROJECTSOURCEID
)
select 
dim_bwproducthierarchyid,
upperhierarchycode,
productgroup,
PROJECTSOURCEID
from dim_bwproducthierarchy where dim_bwproducthierarchyid <>1*/

drop table if exists pre_bwhierarchy;
create table pre_bwhierarchy as
select distinct
	BUSINESSSECTOR,  
	PRODUCTGROUP, 
	UPPERHIERARCHYCODE, 
	LOWERHIERARCHYCODE, 
	max(UPPERHIERSTARTDATE) as UPPERHIERSTARTDATE, 
	max(UPPERHIERENDDATE) as UPPERHIERENDDATE
 from EMD586.dim_bwproducthierarchy bw
where dim_bwproducthierarchyid <>1
and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and BUSINESSSECTOR <> 'Not Set'
group by 
	BUSINESSSECTOR,  
	PRODUCTGROUP,
	UPPERHIERARCHYCODE, 
	LOWERHIERARCHYCODE;

delete from dim_bwproducthierarchy where dim_bwproducthierarchyid <> 1;
insert into dim_bwproducthierarchy
(
dim_bwproducthierarchyid ,
projectsourceid,
	BUSINESSSECTOR,  
	PRODUCTGROUP,  
	UPPERHIERARCHYCODE, 
	LOWERHIERARCHYCODE, 
	 UPPERHIERSTARTDATE, 
	 UPPERHIERENDDATE
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_bwproducthierarchy')  + row_number() over(order by '')  as dim_bwproducthierarchyid ,
16 as projectsourceid,
		BUSINESSSECTOR,  
	PRODUCTGROUP, 
	UPPERHIERARCHYCODE, 
	LOWERHIERARCHYCODE, 
	 UPPERHIERSTARTDATE, 
	 UPPERHIERENDDATE
from pre_bwhierarchy;

delete from EMD586.dim_bwproducthierarchy where projectsourceid = 16;
insert into  EMD586.dim_bwproducthierarchy
(
dim_bwproducthierarchyid ,
projectsourceid,
	BUSINESSSECTOR,  
	PRODUCTGROUP,  
	UPPERHIERARCHYCODE, 
	LOWERHIERARCHYCODE, 
	 UPPERHIERSTARTDATE, 
	 UPPERHIERENDDATE
)
select 
dim_bwproducthierarchyid ,
projectsourceid,
	BUSINESSSECTOR,  
	PRODUCTGROUP,  
	UPPERHIERARCHYCODE, 
	LOWERHIERARCHYCODE, 
	 UPPERHIERSTARTDATE, 
	 UPPERHIERENDDATE
	 from 
	 dim_bwproducthierarchy;