Select 
	   CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
       CONCAT(C.WH,' - ',D.WHDescr) PlantEMD , 
       IFNULL(ListCountry.Countryname,'Not Set') PlantCountryName, 
       SC01001 [Local Material Number],
       CONCAT(SC01002,SC01003) [Material Description],
       SC01023 [Extended PROD Group],
       substring(SC01039,10,4) [Upper Hierarchy],
       SC01035 PartTypeCode,
       B.AccCodeDesc PartType,
       C.WHSafetyStock SafetyStock,
       C.ReservedStock SumTotalRestrictedStock, 
       C.StockBalance-C.ReservedStock SumUnrestrictedStockQty,
       C.StockBalance SumTotalStock,
       C.StdUnitPrice AVGStdUnitPrice,
       C.StockBalance * C.StdUnitPrice SUMOnHandAmt,
       CompProperty.[Value] Currency
       --'MASTER DATA' [Base Unit of Measure],
 from ScalaAU_1923_SC01X100 A
Left Outer Join (Select SY24002 AccCode,SY24003 AccCodeDesc from ScalaAU_1923_SY24X100 Where SY24001='IE' ) B
ON A.SC01035 = B.AccCode
Left Outer Join (Select SC03001 MatrlCode,SC03002 WH, SC03003 StockBalance, SC03004 ReservedStock,SC03092 WHSafetyStock,SC03058 StdUnitPrice from ScalaAU_1923_SC03X100) C
ON A.SC01001=C.MatrlCode
Left Outer Join (Select SC23001 WHCode,SC23002 WHDescr,SC23081 WHCountryCode  from ScalaAU_1923_SC23X100) D
ON C.WH = D.WHCode
Left outer Join (Select SY24002 CountryCode,SY24003 Countryname from ScalaAU_1923_SY24X100 Where SY24001='BM') ListCountry
ON D.WHCountryCode = ListCountry.CountryCode
Left Outer Join (Select CompanyCode,CompanyName,CMGID SubID  from ScalaInfo_ScaCompanies) Comp
ON 'X1'=Comp.CompanyCode
Left Outer Join (Select [Value] from ScalaAU_1923_ScaCompanyProperty where PropertyID = 111) CompProperty
on 'X1'=Comp.CompanyCode
Where  B.AccCode in ('00','01','05','06','07','08','09','10','11','35','36','40') ;



Select    
    CompanyEMD, 
     [Local Material Number],
     [Material Description],
     [Extended PROD Group],
     [Upper Hierarchy],
    BatchNumber,
    PlantEMD,      
    StorageLocationCode,
    DefaultStorageLocation,
    ProcurementType,
    BatchStatusQKZ,
    BatchStatus,
    ManufactureDate,
    ExpiryDate,
    sum(OnHandAmount) OnHandAmount,
    SUM(TotalStock) TotalStock,
    SUM(ExpiredStockQuantity) ExpiredStockQuantity,
    sUM(SUMUnrestrictedStockQty) SUMUnrestrictedStockQty,
    SUM(SUMTotalRestrictedStock) SUMTotalRestrictedStock,
    SUM(SUMStockInQualityInspection) SUMStockInQualityInspection,
    sum(SUMQtyInTransit) SUMQtyInTransit,
    sum(SUMStockInTransfer) SUMStockInTransfer,
    SUM(SUMBlockedStock) SUMBlockedStock
from (
Select CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
       SC01.SC01001 [Local Material Number],
       CONCAT(SC01002,SC01003) [Material Description],
       SC01023 [Extended PROD Group],
       substring(SC01039,10,4) [Upper Hierarchy],
       IFNULL(SC33.BatchNumber,'Not Set') BatchNumber,
       IFNULL(CONCAT(SC33.WhID,' - ',SC23.WhDescript),'Not Set') PlantEMD,       
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.BinNumber)), ''), 'NotSet') StorageLocationCode,
       IFNULL(NULLIF(LTRIM(RTRIM(SC01.SC01114)), ''), 'NotSet')  DefaultStorageLocation,
       IFNULL(SuppCustType.ProcureInfo,'Not Set') ProcurementType,
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.LockStatus)), ''), 'NotSet') BatchStatusQKZ,
       CASE WHEN SC33.LockStatus=0 or SC33.LockStatus IS NULL  THEN 'UNRESTRICTED' ELSE 'RESTRICTED' END BatchStatus,
		CASE WHEN IFNULL(SC33.DateManufact,'') = ''  THEN '1900-01-01' ELSE CAST(SC33.DateManufact AS NVARCHAR(50)) END ManufactureDate,
      -- IFNULL((Case When SC33.DateManufact=convert(datetime,'1-Jan-1900') then convert(datetime,'1-Jan-1900')ELSE convert(VARCHAR,SC33.DateManufact) END),convert(datetime,'1-Jan-1900')) ManufactureDate,
        IFNULL(Case When SC33.[ExpireDate]= '1900-01-01' then '1900-01-01' ELSE convert(VARCHAR(50),SC33.[ExpireDate]) END, '1900-01-01') ExpiryDate,
       sum(IFNULL(SC33.BalanceQty * SC33.BatCostUnit1,0))  OnHandAmount,
       sum(IFNULL(SC33.BalanceQty,0)) TotalStock,
       sum(IFNULL(Case When SC33.[ExpireDate]<CURRENT_DATE() then SC33.BalanceQty ELSE 0 END,0)) ExpiredStockQuantity,
       sum(IFNULL(CASE WHEN SC33.LockStatus=0 or SC33.LockStatus IS NULL  THEN SC33.BalanceQty ELSE 0 END,0)) SUMUnrestrictedStockQty,
       sum(IFNULL(CASE WHEN SC33.LockStatus=0 or SC33.LockStatus IS NULL  THEN 0 ELSE SC33.BalanceQty+SC33.AllocQty END,0)) SUMTotalRestrictedStock,
       sum(IFNULL(CASE WHEN SC33.QCQuantity=0 or SC33.QCQuantity IS NULL  THEN 0 ELSE SC33.QCQuantity END,0)) SUMStockInQualityInspection,
       sum(0) SUMQtyInTransit,
       sum(0) SUMStockInTransfer,
       sum(IFNULL(CASE WHEN SC33.LockStatus=9 THEN SC33.BalanceQty ELSE 0 END,0)) SUMBlockedStock
from ScalaAU_1923_SC01X100 SC01
Left Outer Join (Select CompanyCode,CompanyName,CMGID SubID  from ScalaInfo_ScaCompanies) Comp
ON 'X1'=Comp.CompanyCode
Left Outer Join (Select SC33001 StockCode, SC33002 WhID, SC33003 BatchID,SC33004 BinNumber,SC33005 BalanceQty, SC33006 AllocQty,SC33007 OrderedQty,SC33008 ReceivedQty, SC33009 BatchNumber,
                        SC33010 LockStatus,SC33013 DateReceived,SC33014 DateManufact,SC33015    DateBestBefo,SC33017 SourForBatch,SC33023 BatCostUnit1,SC33034  BatCostUnit2,SC33038 QCCategory, 
                        SC33039 QCQuantity, SC33045 [ExpireDate], SC33018 SuppCustRet, SC33012 FLAGEmptBatc,SC33022 SupplBatchNo, SC33036 ParentBatcID from ScalaAU_1923_SC33X100 Where SC33012=0) SC33
ON SC33.StockCode =SC01.SC01001
Left Outer Join (Select SC23001 WhID, SC23002 WhDescript from ScalaAU_1923_SC23X100) SC23
ON SC33.WhID = SC23.WhID
Left Outer join (Select A.* From (
                                    Select PL01001 CustSuppCode, PL01002 CustSuppName, PL01057 CustSuppAccCode,SuppInfo.SY24003 ProcureInfo  From ScalaAU_1923_PL01X100 P1
                                    Left Outer Join (Select SY24002,SY24003 From ScalaAU_1923_SY24X100 Where SY24001='CI') SuppInfo
                                    ON P1.PL01057 = SuppInfo.SY24002
                                    Union All
                                    Select SL01001 CustSuppCode, SL01002 CustSuppName, SL01032 CustSuppAccCode,CustInfo.SY24003 ProcureInfo From ScalaAU_1923_SL01X100 S1
                                    Left Outer Join (Select SY24002,SY24003 From ScalaAU_1923_SY24X100 Where SY24001='BQ') CustInfo
                                    ON S1.SL01032 = CustInfo.SY24002
                                    ) A
                                        ) SuppCustType
ON SC33.SuppCustRet = SuppCustType.CustSuppCode 
Group By CONCAT(Comp.SubID,' - ',Comp.CompanyName), 
       SC01.SC01001,
       CONCAT(SC01002,SC01003),
       SC01023,
       substring(SC01039,10,4),
       IFNULL(SC33.BatchNumber,'Not Set'),
       IFNULL(CONCAT(SC33.WhID,' - ',SC23.WhDescript),'Not Set'),    
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.BinNumber)), ''), 'NotSet'),
       IFNULL(NULLIF(LTRIM(RTRIM(SC01.SC01114)), ''), 'NotSet'),
       IFNULL(SuppCustType.ProcureInfo,'Not Set'),
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.LockStatus)), ''), 'NotSet'),
       CASE WHEN SC33.LockStatus=0 or SC33.LockStatus IS NULL  THEN 'UNRESTRICTED' ELSE 'RESTRICTED' END,
	 CASE WHEN IFNULL(SC33.DateManufact,'') = ''  THEN '1900-01-01' ELSE CAST(SC33.DateManufact AS NVARCHAR(50)) END   ,  
--IFNULL((Case When SC33.DateManufact=convert(datetime,'1-Jan-1900') then convert(datetime,'1-Jan-1900') ELSE convert(VARCHAR,SC33.DateManufact) END),convert(datetime,'1-Jan-1900')),
       IFNULL(Case When SC33.[ExpireDate]= '1900-01-01' then '1900-01-01' ELSE convert(VARCHAR(50),SC33.[ExpireDate]) END, '1900-01-01')
union all
Select CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
       SC01.SC01001 [Local Material Number],
       CONCAT(SC01002,SC01003) [Material Description],
       SC01023 [Extended PROD Group],
       substring(SC01039,10,4) [Upper Hierarchy],
       'Not Set' BatchNumber,
       CONCAT(PC03.PC03035,'-',SC23.WhDescript) PlantEMD,     
       'NotSet' StorageLocationCode,
       'NotSet'  DefaultStorageLocation,
       IFNULL(SuppCustType.ProcureInfo,'Not Set') ProcurementType,
       'NotSet' BatchStatusQKZ,
       'UNRESTRICTED' BatchStatus,
     '1900-01-01' ManufactureDate,
       '1900-01-01'  ExpiryDate,      
   sum(0)  OnHandAmount,
       SUM(CONVERT(DECIMAL(13,2),PC03010)-CONVERT(DECIMAL(13,2),PC03011)) TotalStock,
   sum(0) ExpiredStockQuantity,
       SUM(CONVERT(DECIMAL(13,2),PC03010) - CONVERT(DECIMAL(13,2),PC03011)) SUMUnrestrictedStockQty,
       sum(0) SUMTotalRestrictedStock,
       sum(0) SUMStockInQualityInspection,
       SUM(CONVERT(DECIMAL(13,2),PC03010) - CONVERT(DECIMAL(13,2),PC03011)) SUMQtyInTransit,
       sum(0) SUMStockInTransfer,
       sum(0) SUMBlockedStock
from ScalaAU_1923_PC03X100 PC03
Left Outer Join (Select CompanyCode,CompanyName,CMGID SubID  from ScalaInfo_ScaCompanies) Comp
ON 'X1'=Comp.CompanyCode 
Left Outer Join (Select * From ScalaAU_1923_SC01X100) SC01
ON SC01.SC01001=PC03005
Left Outer Join (Select SC23001 WhID, SC23002 WhDescript from ScalaAU_1923_SC23X100) SC23
ON PC03.PC03035 = SC23.WhID
Left Outer Join (Select PC01001 PONo, PC01003 SuppCode From ScalaAU_1923_PC01X100) PC01
ON PC03.PC03001 = PC01.PONo
Left Outer join (Select A.* From (
                                    Select PL01001 CustSuppCode, PL01002 CustSuppName, PL01057 CustSuppAccCode,SuppInfo.SY24003 ProcureInfo  From ScalaAU_1923_PL01X100 P1
                                    Left Outer Join (Select SY24002,SY24003 From ScalaAU_1923_SY24X100 Where SY24001='CI') SuppInfo
                                    ON P1.PL01057 = SuppInfo.SY24002
                                    ) A
                                        ) SuppCustType
ON PC01.SuppCode = SuppCustType.CustSuppCode    
Group By CONCAT(Comp.SubID,' - ',Comp.CompanyName),        
       SC01.SC01001,
       CONCAT(SC01002,SC01003),
       SC01023,
       substring(SC01039,10,4),
       CONCAT(PC03.PC03035,'-',SC23.WhDescript),          
       IFNULL(SuppCustType.ProcureInfo,'Not Set')
) A 
Group By A.CompanyEMD, 
       A.[Local Material Number],
       A.[Material Description],
       A.[Extended PROD Group],
       A.[Upper Hierarchy],
       A.BatchNumber,
       A.PlantEMD,     
       A.StorageLocationCode,
       A.DefaultStorageLocation,
       A.ProcurementType,
       A.BatchStatusQKZ,
       A.BatchStatus,
       A.ManufactureDate,
       A.ExpiryDate,
       A.OnHandAmount,
       A.TotalStock;