/* Changes just in union all */
delete from number_fountain m where m.table_name = 'dim_storagelocation';

insert into number_fountain
select 	'dim_storagelocation',
	ifnull(max(d.dim_storagelocationid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_storagelocation d
where d.dim_storagelocationid <> 1; 


drop table if exists prelocations;
create table prelocations as 
select distinct trim (SC33004) as LocationCode, trim (SC33002) as Plant ,SC23002 as WhDescript
     from ScalaAU_1923_SC33X100 SC33
          left join  ScalaAU_1923_SC23X100 SC23 ON SC33.SC33002 = SC23.SC23001 where SC33004 is not null
 union all
select distinct trim (SC33004) as LocationCode, trim (SC33002) as Plant ,SC23002 as WhDescript
     from ScalaMY_1042_SC33Z100 SC33
          left join  ScalaMY_1042_SC23Z100 SC23 ON SC33.SC33002 = SC23.SC23001 where SC33004 is not null
;        
delete from dim_storagelocation where dim_storagelocationid <>1; 
insert into dim_storagelocation
(
dim_storagelocationid,
LocationCode,
Description,
Plant,
projectsourceid
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_company') 
          + row_number() over(order by '')  as  dim_storagelocationid ,
LocationCode,
concat (Plant,'-',WhDescript) as Description,
Plant as Plant,
16 as projectsourceid 
 from prelocations t
 WHERE NOT EXISTS (SELECT 1
                        FROM dim_storagelocation
                       WHERE LocationCode = t.LocationCode);

delete from EMD586.dim_storagelocation
where projectsourceid = 16;

insert into EMD586.dim_storagelocation
(
dim_storagelocationid,
LocationCode,
Description,
Plant,
projectsourceid
)
select 
dim_storagelocationid,
LocationCode,
Description,
Plant,
projectsourceid
from dim_storagelocation where dim_storagelocationid <>1;