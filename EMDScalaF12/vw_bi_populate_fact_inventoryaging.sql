delete from inventoryaging_AU_1923;
insert into inventoryaging_AU_1923
(
 company,  
    CompanyEMD, 
     LocalMaterialNumber,
     MaterialDescription,
     ExtendedPRODGroup,
     UpperHierarchy,
    BatchNumber,
    PlantEMD,      
    StorageLocationCode,
    DefaultStorageLocation,
    ProcurementType,
    BatchStatusQKZ,
    BatchStatus,
    ManufactureDate,
    ExpiryDate,
      OnHandAmount,
      TotalStock,
      ExpiredStockQuantity,
      SUMUnrestrictedStockQty,
      SUMTotalRestrictedStock,
      SUMStockInQualityInspection,
      SUMQtyInTransit,
      SUMStockInTransfer,
      SUMBlockedStock
      )
Select  
    company,  
    CompanyEMD, 
     LocalMaterialNumber,
     MaterialDescription,
     ExtendedPRODGroup,
     UpperHierarchy,
    BatchNumber,
    PlantEMD,      
    StorageLocationCode,
    DefaultStorageLocation,
    ProcurementType,
    BatchStatusQKZ,
    BatchStatus,
    ManufactureDate,
    ExpiryDate,
    sum(OnHandAmount) OnHandAmount,
    SUM(TotalStock) TotalStock,
    SUM(ExpiredStockQuantity) ExpiredStockQuantity,
    sUM(SUMUnrestrictedStockQty) SUMUnrestrictedStockQty,
    SUM(SUMTotalRestrictedStock) SUMTotalRestrictedStock,
    SUM(SUMStockInQualityInspection) SUMStockInQualityInspection,
    sum(SUMQtyInTransit) SUMQtyInTransit,
    sum(SUMStockInTransfer) SUMStockInTransfer,
    SUM(SUMBlockedStock) SUMBlockedStock
from (
Select Comp.SubID company, CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
       SC01.SC01001  LocalMaterialNumber,
       CONCAT(SC01002,SC01003) MaterialDescription,
       SC01023 ExtendedPRODGroup,
       left(SC01039,3) UpperHierarchy,
       IFNULL(SC33.BatchNumber,'Not Set') BatchNumber,
       IFNULL(SC33.WhID,'Not Set') PlantEMD,       
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.BinNumber)), ''), 'NotSet') StorageLocationCode,
       IFNULL(NULLIF(LTRIM(RTRIM(SC01.SC01114)), ''), 'NotSet')  DefaultStorageLocation,
       IFNULL(SuppCustType.ProcureInfo,'Not Set') ProcurementType,
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.LockStatus)), ''), 'NotSet') BatchStatusQKZ,
       CASE WHEN SC33.LockStatus=0 or SC33.LockStatus IS NULL  THEN 'UNRESTRICTED' ELSE 'RESTRICTED' END BatchStatus,
    CASE WHEN IFNULL(SC33.DateManufact,'') = ''  THEN '1900-01-01' ELSE CAST(SC33.DateManufact AS NVARCHAR(50)) END ManufactureDate,
      -- IFNULL((Case When SC33.DateManufact=convert(datetime,'1-Jan-1900') then convert(datetime,'1-Jan-1900')ELSE convert(VARCHAR,SC33.DateManufact) END),convert(datetime,'1-Jan-1900')) ManufactureDate,
        IFNULL(Case When SC33.[ExpireDate]= '1900-01-01' then '1900-01-01' ELSE convert(VARCHAR(50),SC33.[ExpireDate]) END, '1900-01-01') ExpiryDate,
       sum(IFNULL(SC33.BalanceQty * SC33.BatCostUnit1,0))  OnHandAmount,
       sum(IFNULL(SC33.BalanceQty,0)) TotalStock,
       sum(IFNULL(Case When SC33.[ExpireDate]<CURRENT_DATE() then SC33.BalanceQty ELSE 0 END,0)) ExpiredStockQuantity,
       sum(IFNULL(CASE WHEN SC33.LockStatus=0 or SC33.LockStatus IS NULL  THEN SC33.BalanceQty ELSE 0 END,0)) SUMUnrestrictedStockQty,
       sum(IFNULL(CASE WHEN SC33.LockStatus=0 or SC33.LockStatus IS NULL  THEN 0 ELSE SC33.BalanceQty+SC33.AllocQty END,0)) SUMTotalRestrictedStock,
       sum(IFNULL(CASE WHEN SC33.QCQuantity=0 or SC33.QCQuantity IS NULL  THEN 0 ELSE SC33.QCQuantity END,0)) SUMStockInQualityInspection,
       sum(0) SUMQtyInTransit,
       sum(0) SUMStockInTransfer,
       sum(IFNULL(CASE WHEN SC33.LockStatus=9 THEN SC33.BalanceQty ELSE 0 END,0)) SUMBlockedStock
from ScalaAU_1923_SC01X100 SC01
Left Outer Join (Select CompanyCode,CompanyName,CMGID SubID  from ScalaInfo_ScaCompanies) Comp
ON 'X1'=Comp.CompanyCode
Left Outer Join (Select SC33001 StockCode, SC33002 WhID, SC33003 BatchID,SC33004 BinNumber,SC33005 BalanceQty, SC33006 AllocQty,SC33007 OrderedQty,SC33008 ReceivedQty, SC33009 BatchNumber,
                        SC33010 LockStatus,SC33013 DateReceived,SC33014 DateManufact,SC33015    DateBestBefo,SC33017 SourForBatch,SC33023 BatCostUnit1,SC33034  BatCostUnit2,SC33038 QCCategory, 
                        SC33039 QCQuantity, SC33045 [ExpireDate], SC33018 SuppCustRet, SC33012 FLAGEmptBatc,SC33022 SupplBatchNo, SC33036 ParentBatcID from ScalaAU_1923_SC33X100 Where SC33012=0) SC33
ON SC33.StockCode =SC01.SC01001
Left Outer Join (Select SC23001 WhID, SC23002 WhDescript from ScalaAU_1923_SC23X100) SC23
ON SC33.WhID = SC23.WhID
Left Outer join (Select A.* From (
                                    Select PL01001 CustSuppCode, PL01002 CustSuppName, PL01057 CustSuppAccCode,SuppInfo.SY24003 ProcureInfo  From ScalaAU_1923_PL01X100 P1
                                    Left Outer Join (Select SY24002,SY24003 From ScalaAU_1923_SY24X100 Where SY24001='CI') SuppInfo
                                    ON P1.PL01057 = SuppInfo.SY24002
                                    Union All
                                    Select SL01001 CustSuppCode, SL01002 CustSuppName, SL01032 CustSuppAccCode,CustInfo.SY24003 ProcureInfo From ScalaAU_1923_SL01X100 S1
                                    Left Outer Join (Select SY24002,SY24003 From ScalaAU_1923_SY24X100 Where SY24001='BQ') CustInfo
                                    ON S1.SL01032 = CustInfo.SY24002
                                    ) A
                                        ) SuppCustType
ON SC33.SuppCustRet = SuppCustType.CustSuppCode 
Group By Comp.SubID,CONCAT(Comp.SubID,' - ',Comp.CompanyName), 
       SC01.SC01001,
       CONCAT(SC01002,SC01003),
       SC01023,
       left(SC01039,3),
       IFNULL(SC33.BatchNumber,'Not Set'),
       IFNULL(SC33.WhID,'Not Set'),    
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.BinNumber)), ''), 'NotSet'),
       IFNULL(NULLIF(LTRIM(RTRIM(SC01.SC01114)), ''), 'NotSet'),
       IFNULL(SuppCustType.ProcureInfo,'Not Set'),
       IFNULL(NULLIF(LTRIM(RTRIM(SC33.LockStatus)), ''), 'NotSet'),
       CASE WHEN SC33.LockStatus=0 or SC33.LockStatus IS NULL  THEN 'UNRESTRICTED' ELSE 'RESTRICTED' END,
   CASE WHEN IFNULL(SC33.DateManufact,'') = ''  THEN '1900-01-01' ELSE CAST(SC33.DateManufact AS NVARCHAR(50)) END   ,  
--IFNULL((Case When SC33.DateManufact=convert(datetime,'1-Jan-1900') then convert(datetime,'1-Jan-1900') ELSE convert(VARCHAR,SC33.DateManufact) END),convert(datetime,'1-Jan-1900')),
       IFNULL(Case When SC33.[ExpireDate]= '1900-01-01' then '1900-01-01' ELSE convert(VARCHAR(50),SC33.[ExpireDate]) END, '1900-01-01')
union all
Select Comp.SubID as company,
       CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
       SC01.SC01001  LocalMaterialNumber,
       CONCAT(SC01002,SC01003) MaterialDescription,
       SC01023 ExtendedPRODGroup,
       left(SC01039,3) UpperHierarchy,
       'Not Set' BatchNumber,
       IFNULL(PC03.PC03035,'Not Set') PlantEMD,     
       'NotSet' StorageLocationCode,
       'NotSet'  DefaultStorageLocation,
       IFNULL(SuppCustType.ProcureInfo,'Not Set') ProcurementType,
       'NotSet' BatchStatusQKZ,
       'UNRESTRICTED' BatchStatus,
     '1900-01-01' ManufactureDate,
       '1900-01-01'  ExpiryDate,      
   sum(0)  OnHandAmount,
       SUM(CONVERT(DECIMAL(13,2),PC03010)-CONVERT(DECIMAL(13,2),PC03011)) TotalStock,
   sum(0) ExpiredStockQuantity,
       SUM(CONVERT(DECIMAL(13,2),PC03010) - CONVERT(DECIMAL(13,2),PC03011)) SUMUnrestrictedStockQty,
       sum(0) SUMTotalRestrictedStock,
       sum(0) SUMStockInQualityInspection,
       SUM(CONVERT(DECIMAL(13,2),PC03010) - CONVERT(DECIMAL(13,2),PC03011)) SUMQtyInTransit,
       sum(0) SUMStockInTransfer,
       sum(0) SUMBlockedStock
from ScalaAU_1923_PC03X100 PC03
Left Outer Join (Select CompanyCode,CompanyName,CMGID SubID  from ScalaInfo_ScaCompanies) Comp
ON 'X1'=Comp.CompanyCode 
Left Outer Join (Select * From ScalaAU_1923_SC01X100) SC01
ON SC01.SC01001=PC03005
Left Outer Join (Select SC23001 WhID, SC23002 WhDescript from ScalaAU_1923_SC23X100) SC23
ON PC03.PC03035 = SC23.WhID
Left Outer Join (Select PC01001 PONo, PC01003 SuppCode From ScalaAU_1923_PC01X100) PC01
ON PC03.PC03001 = PC01.PONo
Left Outer join (Select A.* From (
                                    Select PL01001 CustSuppCode, PL01002 CustSuppName, PL01057 CustSuppAccCode,SuppInfo.SY24003 ProcureInfo  From ScalaAU_1923_PL01X100 P1
                                    Left Outer Join (Select SY24002,SY24003 From ScalaAU_1923_SY24X100 Where SY24001='CI') SuppInfo
                                    ON P1.PL01057 = SuppInfo.SY24002
                                    ) A
                                        ) SuppCustType
ON PC01.SuppCode = SuppCustType.CustSuppCode    
Group By Comp.SubID,CONCAT(Comp.SubID,' - ',Comp.CompanyName),        
       SC01.SC01001,
       CONCAT(SC01002,SC01003),
       SC01023,
       left(SC01039,3),
       IFNULL(PC03.PC03035,'Not Set'),          
       IFNULL(SuppCustType.ProcureInfo,'Not Set')
) A 
Group By company,A.CompanyEMD, 
       A.LocalMaterialNumber,
       A.MaterialDescription,
       A.ExtendedPRODGroup,
       A.UpperHierarchy,
       A.BatchNumber,
       A.PlantEMD,     
       A.StorageLocationCode,
       A.DefaultStorageLocation,
       A.ProcurementType,
       A.BatchStatusQKZ,
       A.BatchStatus,
       A.ManufactureDate,
       A.ExpiryDate,
       A.OnHandAmount,
       A.TotalStock;

update inventoryaging_AU_1923
set PlantEMDOriginal = PlantEMD; 

update inventoryaging_AU_1923
set PlantEMD = company;


/* Insert into invnetoryaging */

delete from number_fountain m where m.table_name = 'fact_inventoryaging';
insert into number_fountain
select  'fact_inventoryaging',
  ifnull(max(d.fact_inventoryagingid), 
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from fact_inventoryaging d
where d.fact_inventoryagingid <> 1;

delete from fact_inventoryaging ;
insert into fact_inventoryaging 
(
fact_inventoryagingid, 
dim_partid,
dim_plantid,
dim_companyid,
dim_storagelocationid,
dim_bwproducthierarchyid,
dim_projectsourceid,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
dim_currencyid,
dim_dateidexpirydate,
amt_OnHand,
ct_StockQty,
ct_TotalRestrictedStock,
ct_StockInQInsp,
ct_StockInTransit,
ct_StockInTransfer,
ct_BlockedStock,
amt_StdUnitPrice,
dd_BatchNo,
DD_BATCHSTATUS,
DD_BATCH_STATUS_QKZ
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'fact_inventoryaging')  + row_number() over(order by '')  as fact_inventoryagingid ,
ifnull(dp.dim_partid,1) as dim_partid,
ifnull(dpl.dim_plantid,1) as dim_plantid,
ifnull(dc.dim_companyid,1) as dim_companyid,
ifnull(str.dim_storagelocationid,1) as dim_storagelocationid,
ifnull(dim_bwproducthierarchyid,1) as dim_bwproducthierarchyid,
16 as dim_projectsourceid,
1 as AMT_EXCHANGERATE,
1 as AMT_EXCHANGERATE_GBL,
ifnull(dim_currencyid,1) as dim_currencyid,
ifnull(d1.dim_dateid,1) as dim_dateidexpirydate, 
       ifnull(t.ONHANDAMOUNT,0 ) as amt_OnHand ,
       ifnull(t.SUMUNRESTRICTEDSTOCKQTY,0) as ct_StockQty,
       ifnull(t.SUMTOTALRESTRICTEDSTOCK,0) as ct_TotalRestrictedStock,
       ifnull(t.SUMSTOCKINQUALITYINSPECTION,0) as ct_StockInQInsp,
       ifnull(t.SUMQTYINTRANSIT,0) as ct_StockInTransit,
       ifnull(t.SUMSTOCKINTRANSFER,0) as ct_StockInTransfer,
       ifnull(t.SUMBLOCKEDSTOCK,0) as ct_BlockedStock,
       ifnull(al.AVGSTDUNITPRICE,0) as amt_StdUnitPrice,
       ifnull(BatchNumber,'Not Set') as  dd_BatchNo,
              case when BatchStatus = 'UNRESTRICTED' then 'Unrestricted'
             when BatchStatus = 'RESTRICTED' then 'Restricted' 
              else 'Not Set'  end as DD_BATCHSTATUS,
       ifnull(BatchStatusQKZ,'Not Set') as DD_BATCH_STATUS_QKZ



from inventoryaging_AU_1923 t
    left join dim_part dp on dp.Plant = t.PlantEMD and dp.partnumber = t.LocalMaterialNumber
    left join dim_plant dpl on dpl.PlantCode = t.PlantEMD
    left join dim_company dc on dc.CompanyCode = t.COMPANY
    left join tmp_mapall al on  t.PlantEMD = al.Plant and t.LocalMaterialNumber = al.LocalMaterialNumber
    --left join dim_bwproducthierarchy bw on bw.upperhierarchycode = t.UpperHierarchy
    left join dim_storagelocation str on t.StorageLocationCode  =str.LocationCode and t.PlantEMDOriginal = str.Plant
    left join dim_date d1 on  d1.CompanyCode ='Not Set' and t.ExpiryDate  = d1.datevalue;


update fact_inventoryaging f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_inventoryaging f,dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode;

update fact_inventoryaging f
set f.dim_bwproducthierarchyid = bw.dim_bwproducthierarchyid
from fact_inventoryaging f,dim_part dp, dim_bwproducthierarchy bw
where f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and bw.upperhierarchycode = 'Not Set'
and f.dim_bwproducthierarchyid = 1;



UPDATE fact_inventoryaging f
SET f.amt_ExchangeRate_GBL = ifnull(r.exrate,1)
FROM fact_inventoryaging f, dim_currency dc, EMDSIALUSA490.csv_oprate_sigma r
WHERE f.dim_currencyid = dc.dim_currencyid
  AND dc.CurrencyCode = r.fromc;

/* populate EMD586 */



delete from EMD586.fact_inventoryaging where dim_projectsourceid = 16;
insert into EMD586.fact_inventoryaging 
  (
    fact_inventoryagingid, 
dim_partid,
dim_plantid,
dim_companyid,
dim_storagelocationid,
dim_bwproducthierarchyid,
dim_projectsourceid,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
dim_currencyid,
dim_dateidexpirydate,
amt_OnHand,
ct_StockQty,
ct_TotalRestrictedStock,
ct_StockInQInsp,
ct_StockInTransit,
ct_StockInTransfer,
ct_BlockedStock,
amt_StdUnitPrice,
dd_BatchNo,
DD_BATCHSTATUS,
DD_BATCH_STATUS_QKZ)
  select fact_inventoryagingid, 
dim_partid,
dim_plantid,
dim_companyid,
dim_storagelocationid,
dim_bwproducthierarchyid,
dim_projectsourceid,
AMT_EXCHANGERATE,
AMT_EXCHANGERATE_GBL,
dim_currencyid,
dim_dateidexpirydate,
amt_OnHand,
ct_StockQty,
ct_TotalRestrictedStock,
ct_StockInQInsp,
ct_StockInTransit,
ct_StockInTransfer,
ct_BlockedStock,
amt_StdUnitPrice,
dd_BatchNo,
DD_BATCHSTATUS,
DD_BATCH_STATUS_QKZ
from fact_inventoryaging 
where dim_projectsourceid = 16;
