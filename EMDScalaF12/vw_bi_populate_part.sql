drop table tmp_mapall;
create table tmp_mapall as 
Select 
   CONCAT(Comp.SubID,' - ',Comp.CompanyName) CompanyEMD, 
     CONCAT(C.WH,' - ',D.WHDescr) PlantEMD , 
     ifnull(C.WH,'Not Set') as plantdescritpion,
     '1923' as plant,
     IFNULL(ListCountry.Countryname,'Not Set') PlantCountryName, 
     SC01001 LocalMaterialNumber,
     CONCAT(SC01002,SC01003) MaterialDescription,
     SC01023 ExtendedPRODGroup,
     left(SC01039,3) UpperHierarchy,
     SC01035 PartTypeCode,
      B.AccCodeDesc PartType,
      C.WHSafetyStock SafetyStock,
      C.ReservedStock SumTotalRestrictedStock, 
      C.StockBalance-C.ReservedStock SumUnrestrictedStockQty,
       C.StockBalance SumTotalStock,
       C.StdUnitPrice AVGStdUnitPrice,
       C.StockBalance * C.StdUnitPrice SUMOnHandAmt,
       CompProperty.Val Currency,
       cast (1 as bigint) as dim_currencyid
 from ScalaAU_1923_SC01X100 A
Left Outer Join (Select SY24002 AccCode,SY24003 AccCodeDesc from ScalaAU_1923_SY24X100 Where SY24001='IE' ) B
ON A.SC01035 = B.AccCode
Left Outer Join (Select SC03001 MatrlCode,SC03002 WH, SC03003 StockBalance, SC03004 ReservedStock,SC03092 WHSafetyStock,SC03058 StdUnitPrice from ScalaAU_1923_SC03X100) C
ON A.SC01001=C.MatrlCode
Left Outer Join (Select SC23001 WHCode,SC23002 WHDescr,SC23081 WHCountryCode  from ScalaAU_1923_SC23X100) D
ON C.WH = D.WHCode
Left outer Join (Select SY24002 CountryCode,SY24003 Countryname from ScalaAU_1923_SY24X100 Where SY24001='BM') ListCountry
ON D.WHCountryCode = ListCountry.CountryCode
Left Outer Join (Select CompanyCode,CompanyName,CMGID SubID  from ScalaInfo_ScaCompanies) Comp
ON 'X1'=Comp.CompanyCode
Left Outer Join (Select Val from ScalaAU_1923_ScaCompanyProperty where PropertyID = 111) CompProperty
on 'X1'=Comp.CompanyCode
Where  B.AccCode in ('00','01','05','06','07','08','09','10','11','35','36','40') ;

update tmp_mapall
  set dim_currencyid = ifnull(dc.dim_currencyid,1)
  from tmp_mapall al 
    left join dim_currency dc on al.Currency = dc.CURRENCYCODE;

/* populate part */

delete from number_fountain m where m.table_name = 'dim_part';

insert into number_fountain
select 	'dim_part',
	ifnull(max(d.dim_partid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from  dim_part d
where d.dim_partid <> 1;


delete from pre_dim_part;
insert into  pre_dim_part
(
partnumber,
plant,
ORG_DESCRIPTION,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
projectsourceid,
STANDARDUNITPRICE
)
SELECT distinct 
LocalMaterialNumber as partnumber,
ifnull(Plant, 'Not Set') as plant,
CompanyEMD as ORG_DESCRIPTION,
ifnull(MATERIALDESCRIPTION, 'Not Set')  as PartTypeDescription,
MaterialDescription as PartDescription,
ifnull(UpperHierarchy,'Not Set') as productgroupsbu,
ifnull(ExtendedPRODGroup,'Not Set') as producthierarchy,
ifnull((select s.dim_projectsourceid from dim_projectsource s),1) as  projectsourceid,
ifnull(AVGSTDUNITPRICE,0)  as STANDARDUNITPRICE
from  tmp_mapall t ;


insert into   dim_part
(
dim_partid,
partnumber,
plant,
ORG_DESCRIPTION,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
projectsourceid,
STANDARDUNITPRICE
)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_part') 
          + row_number() over(order by '') ,
partnumber,
plant,
ORG_DESCRIPTION,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
projectsourceid,
STANDARDUNITPRICE
from  pre_dim_part t 
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_part dp
                       WHERE dp.Plant = t.Plant and dp.partnumber = t.partnumber);

update Dim_Part
set productgroupsbu2 = REPLACE(productgroupsbu,'SBU-','')
where productgroupsbu2 <> REPLACE(productgroupsbu,'SBU-','');

delete from EMD586.dim_part where projectsourceid = 16;
insert into EMD586.dim_part
(
dim_partid,
partnumber,
plant,
ORG_DESCRIPTION,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
productgroupsbu2,
STANDARDUNITPRICE,
projectsourceid
)
select 
(
dim_partid,
partnumber,
plant,
ORG_DESCRIPTION,
PartTypeDescription,
PartDescription,
productgroupsbu,
producthierarchy,
productgroupsbu2,
STANDARDUNITPRICE,
projectsourceid
)
from  dim_part
where dim_partid <>1;