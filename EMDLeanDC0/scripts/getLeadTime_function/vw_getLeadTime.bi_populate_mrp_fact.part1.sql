/* START OF CODE BLOCKS - tmp_getLeadTime would have the final the data    */

DELETE FROM tmp_getLeadTime
WHERE fact_script_name = 'bi_populate_mrp_fact';

INSERT INTO tmp_getLeadTime
(pPart,
pPlant,             
pVendor,            
DocumentNumber,     
DocumentLineNumber, 
DocumentType,
fact_script_name
)       
Select distinct MDKP_MATNR,
       MDKP_PLWRK,
       v.vendorNumber,
       MDTB_DELNR,
       MDTB_DELPS,
       'PO',
       'bi_populate_mrp_fact'
From  fact_mrp m,
        fact_purchase p,
        dim_plant pl,
        mdkp k,
        mdtb t,
        dim_vendor v,
        dim_date dt
WHERE     m.Dim_ActionStateid = 2
        AND dt.Dim_Dateid <> 1
        AND m.dd_DocumentNo = p.dd_DocumentNo
        AND m.dd_DocumentItemNo = p.dd_DocumentItemNo
        AND m.dd_ScheduleNo = p.dd_ScheduleNo
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND t.MDTB_DELNR = m.dd_DocumentNo
        AND t.MDTB_DELPS = m.dd_DocumentItemNo
        AND t.MDTB_DELET = m.dd_ScheduleNo
        AND v.Dim_Vendorid = p.Dim_Vendorid
        AND p.Dim_DateidDelivery = dt.Dim_Dateid
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
        AND pl.dim_plantid = p.Dim_PlantidOrdering;
	 
