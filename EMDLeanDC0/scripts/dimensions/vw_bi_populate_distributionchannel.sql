UPDATE    dim_distributionchannel dc
   SET dc.DistributionChannelName = t.TVTWT_VTEXT,
			dc.dw_update_date = current_timestamp
       FROM dim_distributionchannel dc,
          TVTWT t
 WHERE dc.RowIsCurrent = 1
 AND t.TVTWT_VTWEG = dc.DistributionChannelCode;

INSERT INTO dim_distributionchannel(dim_distributionchannelid, RowIsCurrent,distributionchannelcode,distributionchannelname,rowstartdate)
SELECT 1, 1,'Not Set','Not Set',current_timestamp
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_distributionchannel
               WHERE dim_distributionchannelid = 1);
delete from number_fountain m where m.table_name = 'dim_distributionchannel';
   
insert into number_fountain
select 	'dim_distributionchannel',
	ifnull(max(d.Dim_DistributionChannelid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_distributionchannel d
where d.Dim_DistributionChannelid <> 1; 

INSERT INTO dim_distributionchannel(Dim_DistributionChannelid,
                                                        DistributionChannelCode,
                            DistributionChannelName,
                            RowStartDate,
                            RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_distributionchannel')
          + row_number() over(order by '') ,
                        TVTWT_VTWEG,
          TVTWT_VTEXT,
          current_timestamp,
          1
     FROM TVTWT t
    WHERE NOT EXISTS
             (SELECT 1
                FROM dim_distributionchannel dc
               WHERE dc.DistributionChannelCode = TVTWT_VTWEG
                     AND dc.DistributionChannelName = TVTWT_VTEXT);

