
INSERT INTO dim_accountcategory(dim_accountcategoryid, RowIsCurrent)
SELECT 1, 1
     FROM ( SELECT 1 )  AS t
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_accountcategory
               WHERE dim_accountcategoryid = 1);

UPDATE    dim_accountcategory ac
SET ac.Description = ifnull(t163i_knttx, 'Not Set'),
	ac.dw_update_date = current_timestamp
from t163i t, dim_accountcategory ac
WHERE ac.Category = t.t163i_knttp AND ac.RowIsCurrent = 1;
   
delete from number_fountain m where m.table_name = 'dim_accountcategory';

insert into number_fountain
select 	'dim_accountcategory',
	ifnull(max(d.dim_accountcategoryid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_accountcategory d
where d.dim_accountcategoryid <> 1;
			   
INSERT INTO dim_accountcategory(Dim_AccountCategoryid,
                                Category,
                                Description,
                                RowStartDate,
                                RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_accountcategory')
          + row_number() over(order by ''),
                  t163i_knttp,
          ifnull(t163i_knttx, 'Not Set'),
          current_timestamp,
          1
     FROM t163i
    WHERE NOT EXISTS (SELECT 1
                        FROM dim_accountcategory
                       WHERE Category = t163i_knttp);

delete from number_fountain m where m.table_name = 'dim_accountcategory';					