Drop table if exists fiscalprd_i00;
Create table fiscalprd_i00(
iid bigint null,
pCompanyCode varchar(18) null,
CalDate timestamp null,
pFromDate timestamp null,
pToDate timestamp null,
pToDate1 date null,
pMon Integer null,
pVariant1 varchar(4) null,
pVariant2 varchar(4) null,
pReturn varchar(53) null,
pYearShift varchar(4) null,
pYearShiftCnt Integer null,
pPrevFIYEAR Integer null,
pPrevPeriod Integer null,
pCalYear  Integer null,
pCalMth   Integer null,
pCalMthDay  Integer null,
Period    Integer null,
FiscalYear  Integer null,
pPeriv varchar(2) null,
pflag integer default 0,
minid integer default 0);

Insert into fiscalprd_i00 (iid,pCompanyCode,CalDate)
Select row_number() over(order by ''),pCompanyCode,dt from
companydetailed_d00;

