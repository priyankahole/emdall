UPDATE    dim_FunctionalArea fa
   SET fa.Description = t.TFKBT_FKBTX,
			fa.dw_update_date = current_timestamp
       FROM
         dim_FunctionalArea fa, TFKBT t
 WHERE fa.RowIsCurrent = 1
 AND fa.FunctionalArea = t.TFKBT_FKBER;
 
INSERT INTO dim_functionalarea(dim_functionalareaid, RowIsCurrent)
SELECT 1, 1
     FROM (SELECT 1) D
WHERE NOT EXISTS
             (SELECT 1
                FROM dim_functionalarea
               WHERE dim_functionalareaid = 1);

delete from number_fountain m where m.table_name = 'dim_functionalarea';
   
insert into number_fountain
select 	'dim_functionalarea',
	ifnull(max(d.Dim_FunctionalAreaId), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
from dim_functionalarea d
where d.Dim_FunctionalAreaId <> 1; 
			   
INSERT INTO dim_functionalarea(Dim_FunctionalAreaId,
                                                        FunctionalArea,
                             Description,
                             RowStartDate,
                             RowIsCurrent)
   SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_functionalarea')
          + row_number() over(order by '') ,
                        TFKBT_FKBER,
          TFKBT_FKBTX,
          current_timestamp,
          1
     FROM TFKBT t
    WHERE NOT EXISTS
                     (SELECT 1
                        FROM dim_functionalarea fa
                       WHERE fa.FunctionalArea = t.TFKBT_FKBER
                             AND fa.RowIsCurrent = 1);


