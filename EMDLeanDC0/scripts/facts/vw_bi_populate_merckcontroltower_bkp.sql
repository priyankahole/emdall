drop table if exists tmp_processing_day;
create table tmp_processing_day as select case when hour(current_timestamp) between 0 and 12 then current_date - interval '1' day else current_date end tdy from dual;

drop table if exists tmp_get_next_periods;
create table tmp_get_next_periods
  as
  select dd.dim_dateid, dd.datevalue
  from dim_date dd
  where dd.dayofmonth = 1
    and dd.datevalue between (select tdy from tmp_processing_day) - interval '1' month + interval '1' day and (select tdy from tmp_processing_day) + interval '12' month
    and dd.companycode = 'Not Set';

/* Tempo + Phoenix + Spain */
drop table if exists tmp_hc_partid;
create table tmp_hc_partid
  as
  select dp.dim_partid
    ,pl.dim_plantid
    ,dd.dim_dateid periodid
    ,dp.partnumber
    ,pl.plantcode
    ,dd.datevalue
    ,max(ifnull(bw.dim_bwproducthierarchyid,1)) dim_bwproducthierarchyid
    ,max(ifnull(dc.dim_companyid,1)) dim_companyid
    ,max(ifnull(mdg.dim_mdg_partid,1)) dim_mdg_partid
    ,max(ifnull(dim_con.dim_bwhierarchycountryid,1)) dim_bwhierarchycountryid
  from dim_part dp
    inner join dim_plant pl on dp.plant = pl.plantcode
    inner join dim_bwproducthierarchy bw on dp.producthierarchy = bw.lowerhierarchycode and dp.productgroupsbu = bw.upperhierarchycode
      and to_date('2017-12-28') between bw.upperhierstartdate and bw.upperhierenddate
    left join dim_bwhierarchycountry dim_con on pl.country = dim_con.nodehierarchynamelvl7 and dim_con.internalhierarchyid = '548YXNWXHSJ2Y19OSUCHY2CL7'
    left join dim_company dc on pl.companycode = dc.companycode
    left join dim_mdg_part mdg on right('000000000000000000' || dp.partnumber,18) = right('000000000000000000' || mdg.partnumber,18)
    cross join tmp_get_next_periods dd
  where bw.BUSINESSSECTOR = 'BS-01'
  group by dp.dim_partid,pl.dim_plantid,dd.dim_dateid,dp.partnumber,pl.plantcode,dd.datevalue;

/* Get Inventory */
drop table if exists tmp_inventoryvalues;
create table tmp_inventoryvalues
  as
  select
  i.dim_partid
  ,i.dim_plantid
  ,sum(i.ct_StockQty) ct_stockavailable
  ,sum(i.ct_StockInQInsp) ct_stockinqa
  ,sum(i.ct_TotalRestrictedStock) ct_stockrestricted
  ,sum(i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) ct_stocktotal
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.amt_OnHand)
      ELSE (i.ct_StockQty + i.ct_StockInQInsp + i.ct_BlockedStock + i.ct_StockInTransit + i.ct_StockInTransfer + i.ct_TotalRestrictedStock) * i.ct_baseuomratioPCcustom * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    END) *
    (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END) *
    i.amt_exchangerate_GBL
  ) amt_stocktotal_wcogs
  ,sum(i.amt_OnHand * i.amt_exchangerate_GBL) amt_stocktotal_stdprice
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.amt_StockValueAmt)
      ELSE (i.ct_StockQty) * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    END) *
    (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END) *
    i.amt_exchangerate_GBL
  ) amt_stockavailable_wcogs
  ,sum(i.amt_StockValueAmt * i.amt_exchangerate_GBL) amt_stockavailable_stdprice
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN i.amt_StockInQInspAmt
      ELSE i.ct_StockInQInsp * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    END) *
    (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END) *
    i.amt_exchangerate_GBL
  ) amt_stockinqa_wcogs
  ,sum(i.amt_StockInQInspAmt * i.amt_exchangerate_GBL) amt_stockinqa_stdprice
  ,sum(
    (CASE
      WHEN (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl) = 0 THEN (i.ct_TotalRestrictedStock*i.amt_StdUnitPrice)
      ELSE (i.ct_TotalRestrictedStock) * (i.amt_cogsfixedplanrate_emd/i.amt_exchangerate_gbl)
    END) *
    (CASE
      WHEN mdg_part.PPU = 0 THEN 1
      ELSE mdg_part.PPU
    END) *
    (CASE
      WHEN mdg_part.ACT_CONV = 0 THEN 1
      ELSE mdg_part.ACT_CONV
    END) *
    i.amt_exchangerate_GBL
  ) amt_stockrestricted_wcogs
  ,sum(i.ct_TotalRestrictedStock*i.amt_StdUnitPrice * i.amt_exchangerate_GBL) amt_stockrestricted_stdprice
  from fact_inventoryaging i
    inner join dim_part prt on i.dim_partid = prt.dim_partid
    inner join dim_company com on i.dim_companyid = com.dim_companyid
    inner join dim_mdg_part mdg_part on i.dim_mdg_partid = mdg_part.dim_mdg_partid
  where
    CASE WHEN prt.PartNumber_NoLeadZero = '5030270000' AND com.company = '001500' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = '1046RPB.0041' AND com.company = '001046' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = '1046RPB.0040' AND com.company = '001046' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'FR21030170085' AND com.company = '001047' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'A1204211' AND com.company = '001954' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'A1204211' AND com.company = '001215' THEN 'X'
    ELSE 'Not Set' END = 'Not Set'
  group by i.dim_partid,i.dim_plantid;

/* Get Forecast from SAP */
drop table if exists tmp_sapfcst;
create table tmp_sapfcst
as
select PBIM_MATNR,PBIM_WERKS,date_trunc('month',PBED_PDATU) PBED_PDATU,SUM(PBED_PLNMG+PBED_ENTMG) PBED_PLNMG
from PBED_PBIM
where PBIM_BEDAE = 'VSF' and PBIM_VERSB = '00' and PBED_PDATU <= date_trunc('month',(select tdy from tmp_processing_day) + interval '13' month) - interval '1' day
group by PBIM_MATNR,PBIM_WERKS,date_trunc('month',PBED_PDATU);

/* Get Sales Data Phoenix */
drop table if exists tmp_GIsales;
create table tmp_GIsales
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(CASE WHEN f.Dim_DateidActualGI <> 1 then f.ct_DeliveredQty else 0 end) ct_shippedmtd
    ,sum(f.ct_DeliveredQty*f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE NULL END) * f.amt_exchangerate_GBL) amt_shippedmtd
  from fact_salesorder f
    inner join dim_date agi on f.dim_dateidactualgi = agi.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
    inner join dim_distributionchannel dch on f.dim_distributionchannelid = dch.dim_distributionchannelid
  where cust.TradingPartner = 'Not Set'
    and dch.DistributionChannelCode IN ('10','11','12','13','14','15','16','17','18','19','30','35','39')
    and dc.DocumentCategory in ('C','I')
    and dt.DocumentType IN ('YKB','YKL','YTA')
    and agi.datevalue between date_trunc('month',(select tdy from tmp_processing_day)) and date_trunc('month',(select tdy from tmp_processing_day) + interval '1' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid;

/* Get OPEN Sales Data Phoenix */
drop table if exists tmp_opensales;
create table tmp_opensales
  as
  select f.dim_partid
    ,f.dim_plantid
    ,date_trunc('month',pd.datevalue) GIDate
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
      end) ct_opensales
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
        else (
          case
            when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
            else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
          end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
      end * f.amt_exchangerate_GBL) amt_opensales
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
    inner join dim_salesorderitemstatus sois on f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
    inner join dim_salesorderrejectreason rr on f.dim_salesorderrejectreasonid = rr.dim_salesorderrejectreasonid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
    inner join dim_distributionchannel dch on f.dim_distributionchannelid = dch.dim_distributionchannelid
  where cust.TradingPartner = 'Not Set'
    and hs.OverallProcessStatusItem not in ('Completely processed', 'Tratado completamente')
    and sois.OverallProcessingStatus not in ('Completely processed', 'Tratado completamente')
    and rr.RejectReasonCode = 'Not Set'
    and f.dd_clearedblockedsts <> 'Blocked'
    and f.dd_ScheduleNo = 1
    and dch.DistributionChannelCode IN ('10','11','12','13','14','15','16','17','18','19','30','35','39')
    and dc.DocumentCategory in ('C','I')
    and dt.DocumentType IN ('YKB','YKL','YTA')
    and pd.datevalue between (select tdy from tmp_processing_day) + interval '1' day and date_trunc('month',(select tdy from tmp_processing_day) + interval '13' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid,date_trunc('month',pd.datevalue);

/* Get Sales Data Phoenix INTERCO*/
drop table if exists tmp_GIsales_interco;
create table tmp_GIsales_interco
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(CASE WHEN f.Dim_DateidActualGI <> 1 then f.ct_DeliveredQty else 0 end) ct_shippedmtd_interco
  from fact_salesorder f
    inner join dim_date agi on f.dim_dateidactualgi = agi.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
  where cust.TradingPartner <> 'Not Set'
    and dc.DocumentCategory in ('C','I')
    and dt.DocumentType IN ('YKB','YKL','YTA')
    and agi.datevalue between date_trunc('month',(select tdy from tmp_processing_day)) and date_trunc('month',(select tdy from tmp_processing_day) + interval '1' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid;

/* Get OPEN Sales Data Phoenix INTERCO */
drop table if exists tmp_opensales_interco;
create table tmp_opensales_interco
  as
  select f.dim_partid
    ,f.dim_plantid
    ,date_trunc('month',pd.datevalue) GIDate
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
      end) ct_opensales_interco
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
    inner join dim_salesorderitemstatus sois on f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
    inner join dim_salesorderrejectreason rr on f.dim_salesorderrejectreasonid = rr.dim_salesorderrejectreasonid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
  where cust.TradingPartner <> 'Not Set'
    and hs.OverallProcessStatusItem not in ('Completely processed', 'Tratado completamente')
    and sois.OverallProcessingStatus not in ('Completely processed', 'Tratado completamente')
    and rr.RejectReasonCode = 'Not Set'
    and f.dd_clearedblockedsts <> 'Blocked'
    and f.dd_ScheduleNo = 1
    and dc.DocumentCategory in ('C','I')
    and dt.DocumentType IN ('YKB','YKL','YTA')
    and pd.datevalue between (select tdy from tmp_processing_day) + interval '1' day and date_trunc('month',(select tdy from tmp_processing_day) + interval '13' month) - interval '1' day
  group by f.dim_partid,f.dim_plantid,date_trunc('month',pd.datevalue);

/* Get PastDue Sales Data Phoenix */
drop table if exists tmp_pastduesales;
create table tmp_pastduesales
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
      end) ct_pastdue
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
        else (
          case
            when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
            else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
          end * f.amt_UnitPriceUoM/(CASE WHEN f.ct_PriceUnit <> 0 THEN f.ct_PriceUnit ELSE 1 END))
      end * f.amt_exchangerate_GBL) amt_pastdue
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
    inner join dim_salesorderitemstatus sois on f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
    inner join dim_salesorderrejectreason rr on f.dim_salesorderrejectreasonid = rr.dim_salesorderrejectreasonid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
    inner join dim_distributionchannel dch on f.dim_distributionchannelid = dch.dim_distributionchannelid
  where cust.TradingPartner = 'Not Set'
    and hs.OverallProcessStatusItem not in ('Completely processed', 'Tratado completamente')
    and sois.OverallProcessingStatus not in ('Completely processed', 'Tratado completamente')
    and rr.RejectReasonCode = 'Not Set'
    and f.dd_clearedblockedsts <> 'Blocked'
    and f.dd_ScheduleNo = 1
    and dch.DistributionChannelCode IN ('10','11','12','13','14','15','16','17','18','19','30','35','39')
    and dc.DocumentCategory in ('C','I')
    and dt.DocumentType IN ('YKB','YKL','YTA')
    and pd.datevalue between '2016-01-01' and (select tdy from tmp_processing_day)
  group by f.dim_partid,f.dim_plantid;

/* Get PastDue Sales Data Phoenix INTERCO */
drop table if exists tmp_pastduesales_interco;
create table tmp_pastduesales_interco
  as
  select f.dim_partid
    ,f.dim_plantid
    ,sum(
      case
        when f.Dim_SalesOrderRejectReasonid <> 1 then 0.0000
        when dt.DocumentType IN ('LP','LK','ZLK','LZM','LZ','ZLZ','ZPL','ZMZC','ZLZC') AND f.dd_ItemRelForDelv = 'X'
          then (
            case
              when (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived)) < 0 then 0.0000
              else (f.ct_ScheduleQtySalesUnit - (f.ct_ShippedAgnstOrderQty - f.ct_CmlQtyReceived))
            end)
        when (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty) < 0 then 0.0000
        else (f.ct_ScheduleQtySalesUnit - f.ct_ShippedAgnstOrderQty)
      end) ct_pastdue_interco
  from fact_salesorder f
    inner join dim_date pd on f.dim_dateidscheddelivery = pd.dim_dateid
    inner join dim_customer cust on f.dim_customerid = cust.dim_customerid
    inner join dim_salesorderheaderstatus hs on f.dim_salesorderheaderstatusid = hs.dim_salesorderheaderstatusid
    inner join dim_salesorderitemstatus sois on f.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid
    inner join dim_salesorderrejectreason rr on f.dim_salesorderrejectreasonid = rr.dim_salesorderrejectreasonid
    inner join dim_salesdocumenttype dt on f.dim_salesdocumenttypeid = dt.dim_salesdocumenttypeid
    inner join dim_documentcategory dc on f.dim_documentcategoryid = dc.dim_documentcategoryid
  where cust.TradingPartner <> 'Not Set'
    and hs.OverallProcessStatusItem not in ('Completely processed', 'Tratado completamente')
    and sois.OverallProcessingStatus not in ('Completely processed', 'Tratado completamente')
    and rr.RejectReasonCode = 'Not Set'
    and f.dd_clearedblockedsts <> 'Blocked'
    and f.dd_ScheduleNo = 1
    and dc.DocumentCategory in ('C','I')
    and dt.DocumentType IN ('YKB','YKL','YTA')
    and pd.datevalue between '2016-01-01' and (select tdy from tmp_processing_day)
  group by f.dim_partid,f.dim_plantid;

/* Purchase data Tempo Spain Emerald APP-6839 */
/* 6982 - Control Tower Availability Date Veronica P*/


drop table if exists tmp_purchdata;
create table tmp_purchdata as
select
  f.dim_partid
  ,f.dim_plantidordering dim_plantid
  ,date_trunc('month',case when dtdel.datevalue < current_date
        then current_date
        else dtdel.datevalue end) as deliv_month
  ,SUM(case
    when atrb.itemdeliverycomplete = 'X' then 0.0000
    when atrb.ItemGRIndicator = 'Not Set' then 0.0000
    when (f.ct_DeliveryQty - f.ct_ReceivedQty) < 0 then 0.0000
    else (f.ct_DeliveryQty - f.ct_ReceivedQty)
  end) open_qty
from fact_purchase f
  inner join dim_date dtdel on f.dim_availabilitydateid = dtdel.dim_dateid
  inner join dim_purchasemisc atrb on f.dim_purchasemiscid = atrb.dim_purchasemiscid
where dtdel.datevalue >= '2017-01-01' and f.dd_deletionindicator = 'Not Set'
group by f.dim_partid, f.dim_plantidordering, 
	date_trunc('month',case when dtdel.datevalue < current_date
        then current_date
        else dtdel.datevalue end);

/* Inventory Projection - APP-7485 - Oana Sept2017 

drop table if exists tmp_inventoryprojection
create table tmp_inventoryprojection as
select i.dim_partid
	,i.dim_plantid
	,i.DD_DUMMYROW
	,datevalue
	,sum(CT_GLIDEPATH) ct_stocktotal
	,sum(CT_AVAILABLESAFETYSTOCK) ct_stockavailable
from fact_inventoryprojection i
    inner join dim_part prt on i.dim_partid = prt.dim_partid
	  inner join dim_plant pl on i.dim_plantid = pl.dim_plantid
    inner join dim_company com on pl.companycode = com.companycode
    inner join dim_mdg_part mdg_part on i.dim_mdg_partid = mdg_part.dim_mdg_partid
    inner join dim_Date dt on dt.dim_dateid = i.DIM_DATEIDMONTH
where
    CASE WHEN prt.PartNumber_NoLeadZero = '5030270000' AND com.company = '001500' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = '1046RPB.0041' AND com.company = '001046' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = '1046RPB.0040' AND com.company = '001046' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'FR21030170085' AND com.company = '001047' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'A1204211' AND com.company = '001954' THEN 'X'
      WHEN prt.PartNumber_NoLeadZero = 'A1204211' AND com.company = '001215' THEN 'X'
    ELSE 'Not Set' END = 'Not Set'
	and date_trunc('month',datevalue) >= date_trunc('month',current_Date)
group by i.dim_partid,i.dim_plantid,i.DD_DUMMYROW,datevalue */



delete from fact_merckcontroltower;
insert into fact_merckcontroltower
  (fact_merckcontroltowerid
  ,amt_exchangerate
  ,amt_exchangerate_GBL
  ,dim_projectsourceid
  ,dim_partid
  ,dim_plantid
  ,dim_mdg_partid
  ,dim_bwproducthierarchy
  ,dim_periodid
  ,ct_stockavailable
  ,ct_stockinqa
  ,ct_stockrestricted
  ,ct_stocktotal
  ,amt_stocktotal_wcogs
  ,amt_stocktotal_stdprice
  ,amt_stockavailable_wcogs
  ,amt_stockavailable_stdprice
  ,amt_stockinqa_wcogs
  ,amt_stockinqa_stdprice
  ,amt_stockrestricted_wcogs
  ,amt_stockrestricted_stdprice
  ,ct_salesforecast
  ,ct_shippedmtd
  ,amt_shippedmtd
  ,ct_opensales
  ,amt_opensales
  ,ct_shippedmtd_interco
  ,ct_opensales_interco
  ,ct_pastdue
  ,amt_pastdue
  ,ct_pastdue_interco
  ,dim_companyid
  ,amt_avgsaleprice
  ,dim_bwhierarchycountryid
  ,ct_purchopenqty
  ,amt_salesop_euro
  ,ct_salesop
  ,ct_salescf_qty
  ,amt_SupplyRisk_Euro
  ,amt_ytdsalesop_euro)
select
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1) + row_number() over(order by '') fact_merckcontroltowerid
  ,1 amt_exchangerate
  ,1 amt_exchangerate_GBL
  ,(select s.dim_projectsourceid from dim_projectsource s) dim_projectsourceid
  ,ifnull(t.dim_partid,1) dim_partid
  ,ifnull(t.dim_plantid,1) dim_plantid
  ,ifnull(t.dim_mdg_partid,1) dim_mdg_partid
  ,ifnull(t.dim_bwproducthierarchyid,1) dim_bwproducthierarchyid
  ,ifnull(t.periodid,1) dim_periodid
  ,ifnull(inv.ct_stockavailable,0) ct_stockavailable
  ,ifnull(inv.ct_stockinqa,0) ct_stockinqa
  ,ifnull(inv.ct_stockrestricted,0) ct_stockrestricted
  ,ifnull(inv.ct_stocktotal,0) ct_stocktotal
  ,ifnull(inv.amt_stocktotal_wcogs,0) amt_stocktotal_wcogs
  ,ifnull(inv.amt_stocktotal_stdprice,0) amt_stocktotal_stdprice
  ,ifnull(inv.amt_stockavailable_wcogs,0) amt_stockavailable_wcogs
  ,ifnull(inv.amt_stockavailable_stdprice,0) amt_stockavailable_stdprice
  ,ifnull(inv.amt_stockinqa_wcogs,0) amt_stockinqa_wcogs
  ,ifnull(inv.amt_stockinqa_stdprice,0) amt_stockinqa_stdprice
  ,ifnull(inv.amt_stockrestricted_wcogs,0) amt_stockrestricted_wcogs
  ,ifnull(inv.amt_stockrestricted_stdprice,0) amt_stockrestricted_stdprice
  ,ifnull(asp.FCST_QTY,0) ct_salesforecast
  ,ifnull(gis.ct_shippedmtd,0) ct_shippedmtd
  ,ifnull(gis.amt_shippedmtd,0) amt_shippedmtd
  ,ifnull(os.ct_opensales,0) ct_opensales
  ,ifnull(os.amt_opensales,0) amt_opensales
  ,ifnull(gis_ic.ct_shippedmtd_interco,0) ct_shippedmtd_interco
  ,ifnull(os_ic.ct_opensales_interco,0) ct_opensales_interco
  ,ifnull(pd.ct_pastdue,0) ct_pastdue
  ,ifnull(pd.amt_pastdue,0) amt_pastdue
  ,ifnull(pd_ic.ct_pastdue_interco,0) ct_pastdue_interco
  ,ifnull(t.dim_companyid,1) dim_companyid
  ,(case when ifnull(asp.PRICE,0)< 0 then 0 else ifnull(asp.PRICE,0) end) amt_avgsaleprice
  ,ifnull(t.dim_bwhierarchycountryid,1) dim_bwhierarchycountryid
  ,ifnull(prch.open_qty,0) ct_purchopenqty
  ,ifnull(asp.op_euro,0) amt_salesop_euro
  ,ifnull(asp.op_qty,0) ct_salesop
  ,ifnull(asp.cf_qty,0) ct_salescf_qty
  ,ifnull(asp.SupplyRisk_Euro,0) amt_SupplyRisk_Euro
  ,ifnull(asp.ytdsalesop_euro,0) amt_ytdsalesop_euro
from tmp_hc_partid t
  left join tmp_inventoryvalues inv on t.dim_partid = inv.dim_partid and t.dim_plantid = inv.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_sapfcst fcst on t.partnumber = fcst.PBIM_MATNR and t.plantcode = fcst.PBIM_WERKS and t.datevalue = fcst.PBED_PDATU
  left join tmp_GIsales gis on t.dim_partid = gis.dim_partid and t.dim_plantid = gis.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_opensales os on t.dim_partid = os.dim_partid and t.dim_plantid = os.dim_plantid and t.datevalue = os.GIDate
  left join tmp_GIsales_interco gis_ic on t.dim_partid = gis_ic.dim_partid and t.dim_plantid = gis_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_opensales_interco os_ic on t.dim_partid = os_ic.dim_partid and t.dim_plantid = os_ic.dim_plantid and t.datevalue = os_ic.GIDate
  left join tmp_pastduesales pd on t.dim_partid = pd.dim_partid and t.dim_plantid = pd.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join tmp_pastduesales_interco pd_ic on t.dim_partid = pd_ic.dim_partid and t.dim_plantid = pd_ic.dim_plantid and t.datevalue = date_trunc('month',(select tdy from tmp_processing_day))
  left join EMDOtherDatasourcesBE3.csv_averagesaleprice asp on trim(leading '0' from t.partnumber) = trim(leading '0' from asp.MATNR) and t.plantcode = asp.WERKS and t.datevalue = asp.PERIOD
  left join tmp_purchdata prch on t.dim_partid = prch.dim_partid and t.dim_plantid = prch.dim_plantid and t.datevalue = prch.deliv_month;

/* APP-6980 - Oana 30 Aug 2017 
update fact_merckcontroltower f
	set f.dim_jda_productsubsetid = djp.dim_jda_productsubsetid
from fact_merckcontroltower f, dim_part dp, dim_jda_productsubset djp
where f.dim_partid = dp.dim_partid
 and trim(leading '0' from dp.partnumber) = trim(leading '0' from djp.itemglobalcode)
 END */


/* APP-6980 - Oana 30 Aug 2017 

update fact_merckcontroltower f
  set f.ct_stocktotal = t.ct_stocktotal,
		f.ct_stockavailable =  t.ct_stockavailable,
		f.dd_dummyrow = t.dd_dummyrow
from fact_merckcontroltower f , tmp_inventoryprojection t, dim_date dt
where f.dim_periodid = dt.dim_dateid
	and f.dim_partid = t.dim_partid
	and f.dim_plantid = t.dim_plantid
	and dt.datevalue = t.datevalue + interval '1' month */

drop table if exists tmp_hc_partid;
drop table if exists tmp_inventoryvalues;
drop table if exists tmp_sapfcst;
drop table if exists tmp_GIsales;
drop table if exists tmp_opensales;
drop table if exists tmp_GIsales_interco;
drop table if exists tmp_opensales_interco;
drop table if exists tmp_pastduesales;
drop table if exists tmp_pastduesales_interco;
drop table if exists tmp_processing_day;
drop table if exists tmp_purchdata;
drop table if exists tmp_inventoryprojection;
