TRUNCATE TABLE fact_materialmovement_forLots_tmp;
UPDATE fact_materialmovement
SET LotsAcceptedFlag = 'N'
    ,dw_update_date = current_timestamp
WHERE LotsAcceptedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsAwaitingInspectionFlag = 'N'
    ,dw_update_date = current_timestamp
WHERE LotsAwaitingInspectionFlag = 'Y';

UPDATE fact_materialmovement
SET LotsInspectedFlag = 'N'
    ,dw_update_date = current_timestamp
WHERE LotsInspectedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsSkippedFlag = 'N'
    ,dw_update_date = current_timestamp
WHERE LotsSkippedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsAcceptedFlag = 'N'
    ,dw_update_date = current_timestamp
WHERE PercentLotsAcceptedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsInspectedFlag = 'N'
    ,dw_update_date = current_timestamp
WHERE PercentLotsInspectedFlag = 'Y';

UPDATE fact_materialmovement
SET PercentLotsRejectedFlag = 'N'
    ,dw_update_date = current_timestamp
WHERE PercentLotsRejectedFlag = 'Y';

UPDATE fact_materialmovement
SET LotsReceivedFlag = 'N'
    ,dw_update_date = current_timestamp
WHERE LotsReceivedFlag = 'Y';



INSERT INTO fact_materialmovement_forLots_tmp(fact_materialmovementid,
                                              dd_MaterialDocNo,
                                              dd_MaterialDocItemNo,
                                              dd_MaterialDocYear,
                                              dim_movementtypeid,
                                              Rank1,
                                              LotsAcceptedFlag,
                                              LotsAwaitingInspectionFlag,
                                              LotsInspectedFlag,
                                              LotsRejectedFlag,
                                              LotsSkippedFlag,
                                              PercentLotsAcceptedFlag,
                                              PercentLotsInspectedFlag,
                                              PercentLotsRejectedFlag,LotsReceivedFlag)
   SELECT fact_materialmovementid,
          dd_MaterialDocNo,
          dd_MaterialDocItemNo,
          dd_MaterialDocYear,
          mm.dim_movementtypeid,
          RANK() OVER (PARTITION BY dd_MaterialDocNo, dd_MaterialDocItemNo, dd_MaterialDocYear,mm.dim_movementtypeid ORDER BY fact_materialmovementid ASC) as Rank1,
'N','N','N','N','N','N','N','N','N'
from fact_materialmovement mm, dim_movementtype mt
where mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = mm.dim_movementtypeid;


DELETE FROM fact_materialmovement_forLots_tmp
 WHERE Rank1 <> 1;



UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET f_mm.LotsAcceptedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils,
                              dim_CodeText ud
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND ud.dim_codetextid =
                                     il.dim_inspusagedecisionid
                              AND ud.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACC',
                                      'ACCI',
                                      'ACUS',
                                      'ACCP',
                                      'ACCN','A50','AX')), 'N')
FROM
        fact_materialmovement_forLots_tmp f_mm,  dim_movementtype mt
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

UPDATE    fact_materialmovement_forLots_tmp f_mm
       
   SET        f_mm.LotsAwaitingInspectionFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_inspectionlotmisc ilm,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dim_inspectionlotmiscid =
                                     ilm.dim_inspectionlotmiscid
                              AND ilm.LotSkipped <> 'X' AND il.dim_inspusagedecisionid = 1), 'N')
 FROM
          fact_materialmovement_forLots_tmp f_mm,dim_movementtype mt
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;



UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        f_mm.LotsInspectedFlag = ifnull((SELECT 'Y' FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.lotcancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dim_inspusagedecisionid <> 1), 'N')
        FROM
          fact_materialmovement_forLots_tmp f_mm,dim_movementtype mt
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        f_mm.LotsRejectedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid <> 1
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND udc.Code NOT IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCP',
                                      'ACCI',
                                      'ACCN',
                                      'ACUS',
                                      'ACC','A50','AX')), 'N')
        FROM
          fact_materialmovement_forLots_tmp f_mm,dim_movementtype mt
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        LotsSkippedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_inspectionlotmisc ilm,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dim_inspectionlotmiscid =
                                     ilm.dim_inspectionlotmiscid
                              AND ilm.LotSkipped = 'X'), 'N')

        FROM
         fact_materialmovement_forLots_tmp f_mm, dim_movementtype mt
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;



UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET PercentLotsAcceptedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND udc.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCP',
                                      'ACCI',
                                      'ACCN',
                                      'ACUS',
                                      'ACC','A50','AX')), 'N')
     FROM
       fact_materialmovement_forLots_tmp f_mm,dim_movementtype mt
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;



UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        PercentLotsInspectedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_inspectionlotstatus ils,
                              dim_codetext ct
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dim_inspusagedecisionid =
                                     ct.dim_Codetextid
                              AND ct.Code IN
                                     ('A',
                                      'ACCC',
                                      'CLSD',
                                      'ACCI',
                                      'ACCP',
                                      'QUAR',
                                      'QURP',
                                      'REJO',
                                      'REJR',
                                      'REJS',
                                      'REJV','A50','AX','R','R1','R2','R3','R4','R40','RQ','X')), 'N')
        FROM
          fact_materialmovement_forLots_tmp f_mm,dim_movementtype mt
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;



UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        PercentLotsRejectedFlag = ifnull((SELECT 'Y'
                         FROM fact_inspectionlot il,
                              dim_Codetext udc,
                              dim_inspectionlotstatus ils
                        WHERE ils.dim_inspectionlotstatusid =
                                 il.dim_inspectionlotstatusid
                              AND ils.LotCancelled <> 'X'
                              AND il.dim_inspusagedecisionid =
                                     udc.dim_codetextid
                              AND il.dim_inspusagedecisionid <> 1
                              AND il.dd_MaterialDocNo = f_mm.dd_MaterialDocNo
                              AND il.dd_MaterialDocItemNo =
                                     f_mm.dd_MaterialDocItemNo
                              AND il.dd_MaterialDocYear =
                                     f_mm.dd_MaterialDocYear
                              AND udc.Code IN
                                     ('QUAR',
                                      'QURP',
                                      'REJO',
                                      'REJR',
                                      'REJS',
                                      'REJV','R','R1','R2','R3','R4','R40','RQ','X')), 'N')
        FROM
          fact_materialmovement_forLots_tmp f_mm,dim_movementtype mt
 WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;

UPDATE    fact_materialmovement_forLots_tmp f_mm
   SET        LotsReceivedFlag = 'Y' 
        FROM
          fact_materialmovement_forLots_tmp f_mm,dim_movementtype mt
WHERE mt.movementtype IN ('101')
 AND mt.dim_movementtypeid = f_mm.dim_movementtypeid;




UPDATE fact_materialmovement f_mm
   SET f_mm.LotsAcceptedFlag = t.LotsAcceptedFlag,
       f_mm.LotsAwaitingInspectionFlag = t.LotsAwaitingInspectionFlag,
       f_mm.LotsInspectedFlag = t.LotsInspectedFlag,
       f_mm.LotsRejectedFlag = t.LotsRejectedFlag,
       f_mm.LotsSkippedFlag = t.LotsSkippedFlag,
       f_mm.PercentLotsAcceptedFlag = t.PercentLotsAcceptedFlag,
       f_mm.PercentLotsInspectedFlag = t.PercentLotsInspectedFlag,
       f_mm.PercentLotsRejectedFlag = t.PercentLotsRejectedFlag,
       f_mm.LotsReceivedflag = t.LotsReceivedFlag
	   ,f_mm.dw_update_date = current_timestamp
  FROM fact_materialmovement f_mm,fact_materialmovement_forLots_tmp t
 WHERE     f_mm.fact_materialmovementid = t.fact_materialmovementid
       AND f_mm.dd_MaterialDocNo = t.dd_MaterialDocNo
       AND f_mm.dd_MaterialDocItemNo = t.dd_MaterialDocItemNo
       AND f_mm.dd_MaterialDocYear = t.dd_MaterialDocYear
       AND f_mm.dim_movementtypeid = t.dim_movementtypeid;

TRUNCATE TABLE fact_materialmovement_forLots_tmp;
