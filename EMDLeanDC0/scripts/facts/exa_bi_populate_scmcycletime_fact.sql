/* Create a Temp Fact Table */
drop table if exists tmp_fact_scmcycletime;   
create table tmp_fact_scmcycletime like fact_scmcycletime
including defaults including identity;

/* 101-F - Get All The 101/102-F Movements For The Production Order Output - Receive */ 
drop table if exists tmp_factmm_scmp_101F;   
create table tmp_factmm_scmp_101F as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_unitofmeasureid, max(pd.datevalue) as dd_postingdate, sum(f.ct_quantity) ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid 
and mt.movementtype in ('101','102') and mt.movementindicator = 'F' 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchnumber, f.dd_productionordernumber, f.dd_documentno, f.dd_documentitemno, f.dim_unitofmeasureid;

/* 261 - Get All The 261/262 Movements For The Production Order Consumption - Issue */ 
drop table if exists tmp_factmm_scmp_261;   
create table tmp_factmm_scmp_261 as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_unitofmeasureid, min(pd.datevalue) dd_postingdate, sum(f.ct_quantity) ct_quantity
from fact_materialmovement f, dim_movementtype mt, dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid  
and mt.movementtype in ('261','262')
/* and f.dd_batchnumber <> 'Not Set' */ and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchnumber, f.dd_productionordernumber, f.dd_documentno, f.dd_documentitemno, f.dim_unitofmeasureid;

/* Production Order Fact - Get Production Order Actual and Scheduled Start and End Dates */ 
drop table if exists tmp_factmm_scmp_prodstartend_fact_prodorder;   
create table tmp_factmm_scmp_prodstartend_fact_prodorder as
select 	dd_ordernumber, min(asd.datevalue) as actualstartdate, min(ssd.datevalue) as scheduledstartdate, 
		min(sfd.datevalue) as scheduledfinishdate, min(afdm.datevalue) as actualheaderfinishdate,  min(ahfd.datevalue) as actualheaderfinish,
		min(fpo.dim_dateidactualstart) as dim_dateidactualstart, min(fpo.dim_dateidscheduledstart) as dim_dateidscheduledstart, 
		min(fpo.dim_dateidscheduledfinish) as dim_dateidscheduledfinish, min(fpo.dim_dateidactualheaderfinishdate) as dim_dateidactualheaderfinishdate, 	
		min(fpo.dim_dateidactualheaderfinish) as dim_dateidactualheaderfinish
from fact_productionorder fpo, dim_date asd, dim_date ssd, dim_date sfd, dim_date afdm, dim_date ahfd
where fpo.dim_dateidactualstart = asd.dim_dateid and fpo.dim_dateidscheduledstart = ssd.dim_dateid and 
dim_dateidscheduledfinish = sfd.dim_dateid and dim_dateidactualheaderfinishdate = afdm.dim_dateid and 
dim_dateidactualheaderfinish = ahfd.dim_dateid
group by fpo.dd_ordernumber;

/* 101-B - Get All The 101/102 B Movements For The Purchase Order Receipt  - Receive */ 
drop table if exists tmp_factmm_scmp_101B;   
create table tmp_factmm_scmp_101B as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber,
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_unitofmeasureid, min(pd.datevalue) as dd_postingdate, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid  
and mt.movementtype in ('101','102') and mt.movementindicator = 'B' 
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_documentno <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchnumber, f.dd_productionordernumber, f.dd_documentno, f.dd_documentitemno, f.dim_unitofmeasureid;

/* 531 - Get All The 531 Movements For The Goods Receipt of BY products  - Receive */ 
drop table if exists tmp_factmm_scmp_531;   
create table tmp_factmm_scmp_531 as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber,
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_unitofmeasureid, min(pd.datevalue) as dd_postingdate, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid  
and mt.movementtype in ('531')
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_productionordernumber <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchnumber, f.dd_productionordernumber, f.dd_documentno, f.dd_documentitemno, f.dim_unitofmeasureid;

/* 321 - Get All The 321 Movements For The Quality End  - Receive */ 
drop table if exists tmp_factmm_scmp_321;   
create table tmp_factmm_scmp_321 as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber,
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_unitofmeasureid, min(pd.datevalue) as dd_postingdate, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid  
and mt.movementtype in ('321','322')
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' 
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchnumber, f.dd_productionordernumber, f.dd_documentno, f.dd_documentitemno, f.dim_unitofmeasureid;

/* Inspection Lot Fact - Get All The Usage Descision Lots For The Quality End  - Receive */ 
drop table if exists tmp_factmm_scmp_ud;   
create table tmp_factmm_scmp_ud as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchno dd_batchnumber, f.dd_orderno dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_lotunitofmeasureid dim_unitofmeasureid, min(ud.datevalue) as dd_postingdate, sum(f.ct_inspectionlotqty) ct_quantity
from fact_inspectionlot f, dim_part dp, dim_plant pl, dim_date ud, dim_CodeText ct
where f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidusagedecisionmade = ud.dim_dateid and f.Dim_InspUsageDecisionId = ct.Dim_CodeTextId
and f.dd_batchno <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and ct.code <> 'R'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidusagedecisionmade <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchno, f.dd_orderno, f.dd_documentno, f.dd_documentitemno, f.dim_lotunitofmeasureid;


/* 643/645 - Get All The 643/645 Movements For The Cross Company Stock Transfer - Issue */ 
drop table if exists tmp_factmm_scmp_643645;   
create table tmp_factmm_scmp_643645 as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_unitofmeasureid, min(pd.datevalue) as dd_postingdate, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid  
and mt.movementtype in ('643','641') and mt.movementindicator = 'L'
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_documentno <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchnumber, f.dd_productionordernumber, f.dd_documentno, f.dd_documentitemno, f.dim_unitofmeasureid;

/* 543/541 - Get All The 543/541 Movements For The Sub Contract Transfer - Issue */ 
drop table if exists tmp_factmm_scmp_543;   
create table tmp_factmm_scmp_543 as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber,
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_unitofmeasureid, min(pd.datevalue) as dd_postingdate, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid  
and mt.movementtype in ('543','541')
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_documentno <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchnumber, f.dd_productionordernumber, f.dd_documentno, f.dd_documentitemno, f.dim_unitofmeasureid;

/* 641/647 - Get All The 641/647 Movements For The Intra Company Stock Transfer - Issue */ 
drop table if exists tmp_factmm_scmp_641647;   
create table tmp_factmm_scmp_641647 as
select f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_unitofmeasureid, min(pd.datevalue) as dd_postingdate, sum(f.ct_quantity) as ct_quantity
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid  
and mt.movementtype in ('641','647') and mt.movementindicator = 'L'
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and f.dd_documentno <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1
group by f.dim_plantid, f.dim_partid, dp.partnumber, pl.plantcode, f.dd_batchnumber, f.dd_productionordernumber, f.dd_documentno, f.dd_documentitemno, f.dim_unitofmeasureid;

--Intra-company STO 641
--Intra-company STO	647
--Cross-company STO	643
--Cross-company STO	645




/* Production time: for each of the batch product plant select 101 movement for production. 
the latest date of the 101 will be the "end of production date"
Find the process order realted to that movement in the material movement table. 
Find the earlieast movement of 261 type against this processorder. This will be the "start of production date." 
Calculate the time between both start and end date and create the "production time"
Latest Confirmed Production Order Date when where is no 261 */

/* Prod Start - Find The First Consumption Posting Date For The Given Part Plant Batch Production Order */ 
drop table if exists tmp_factmm_scmp_261_min;   
create table tmp_factmm_scmp_261_min as
select f.dd_productionordernumber, min(f.dd_postingdate) dd_postingdate_prodstart
from tmp_factmm_scmp_261 f
group by f.dd_productionordernumber;

/* Part Plant Batch Production Order Unique Combination With Start and End Dates */ 
drop table if exists tmp_factmm_scmp_prodstartend;   
create table tmp_factmm_scmp_prodstartend as
select 	pe.dim_plantid, pe.dim_partid, pe.dd_partnumber, pe.dd_plantcode, pe.dd_batchnumber, pe.dd_productionordernumber, 
		pe.dd_purchaseorder, pe.dd_purchaseorderitem, pe.dim_unitofmeasureid, pe.ct_quantity,
		case when ps.dd_postingdate_prodstart is null then cast('0001-01-01' as date) else ps.dd_postingdate_prodstart end dd_postingdate_prodstart, 
		pe.dd_postingdate as dd_postingdate_prodend,
		cast(case when ps.dd_productionordernumber is not null then '261-101 Match' else 'No 261' end as varchar(1024) ) as dd_prodnote,
		'Y' as dd_prodflag
from tmp_factmm_scmp_101f pe left outer join 
		tmp_factmm_scmp_261_min ps on ps.dd_productionordernumber = pe.dd_productionordernumber;

-- Note: There are production orders with no start when there is an end, similarly there are production orders with no end when there is a start - to be reviewed in future

/* Update Missing Start Dates */		
/* Step 1: Update Production Start Date For Records Where 261 Movement Was Not Found */  
update tmp_factmm_scmp_prodstartend f
set f.dd_postingdate_prodstart = case 	when p.dim_dateidactualstart <> 1 then p.actualstartdate
										when p.dim_dateidscheduledstart <> 1 then p.scheduledstartdate
										else f.dd_postingdate_prodstart end,
    f.dd_prodnote = concat(f.dd_prodnote,', ProdOrder-StartDate') 
from tmp_factmm_scmp_prodstartend_fact_prodorder p, tmp_factmm_scmp_prodstartend f
where f.dd_productionordernumber = p.dd_ordernumber and f.dd_postingdate_prodstart = '0001-01-01' and 
f.dd_postingdate_prodstart <> case 	when p.dim_dateidactualstart <> 1 then p.actualstartdate
									when p.dim_dateidscheduledstart <> 1 then p.scheduledstartdate
									else f.dd_postingdate_prodstart end;

/* Step 2: Update Production Start Date For Records Where Earliest 261 Movement Is After Latest 101 Movement Using Production Order Fact Data */  
update tmp_factmm_scmp_prodstartend f
set f.dd_postingdate_prodstart = case 	when p.dim_dateidactualstart <> 1 then p.actualstartdate
										when p.dim_dateidscheduledstart <> 1 then p.scheduledstartdate
										else f.dd_postingdate_prodstart end,
    f.dd_prodnote = concat(f.dd_prodnote,', ProdStartDateAfterEndDateFix-ProdOrder-StartDate') 
from tmp_factmm_scmp_prodstartend_fact_prodorder p, tmp_factmm_scmp_prodstartend f
where f.dd_productionordernumber = p.dd_ordernumber and f.dd_postingdate_prodstart > dd_postingdate_prodend and 
f.dd_postingdate_prodstart <> case 	when p.dim_dateidactualstart <> 1 then p.actualstartdate
									when p.dim_dateidscheduledstart <> 1 then p.scheduledstartdate
									else f.dd_postingdate_prodstart end;

/* Step 3: Update Production Start Date For Records Where Earliest 261 Movement Is After Latest 101 Movement */  
update tmp_factmm_scmp_prodstartend f
set f.dd_postingdate_prodstart = f.dd_postingdate_prodend,
    f.dd_prodnote = concat(f.dd_prodnote,', ProdStartDateAfterEndDateFix') 
from tmp_factmm_scmp_prodstartend f
where f.dd_postingdate_prodstart > f.dd_postingdate_prodend and f.dd_postingdate_prodstart <> f.dd_postingdate_prodend;

/* Step 4: Update Production Start Date with '0001-01-01' */  
update tmp_factmm_scmp_prodstartend f
set f.dd_postingdate_prodstart = f.dd_postingdate_prodend,
    f.dd_prodnote = concat(f.dd_prodnote,', ProdStartDateBlank') 
from tmp_factmm_scmp_prodstartend f
where f.dd_postingdate_prodstart = '0001-01-01' and f.dd_postingdate_prodstart <> f.dd_postingdate_prodend;


/* Insert Data Into Temp Fact Table - We Insert All Data Since This is First Step*/
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_prodstart, dd_postingdate_prodend, dd_prodnote, dd_prodflag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_prodstart, dd_postingdate_prodend, dd_prodnote, dd_prodflag
from tmp_factmm_scmp_prodstartend;

/* Exception for Production Time - Testing and Questions for Customer */
/* 	Case 1 - No Output for Production Order
	Case 2 - No Issue for Production Order  */		
/* END - Production Time - How long did it take to make this product, plant, batch */



/*
Quality Time:
a) For each batch location product find earliest 321 movement. That would be the end of quality date. Find the earliest 101 for that material and plant and batch. 
This would be the start of quality time. calculate the difference under quality time. 
b) if no 321 check inspection lot for that batch and use usage decision date.
*/

/* Quality Start - Combine both 321 and UD Data - 321 Data */
drop table if exists tmp_factmm_scmp_qualitystart;   
create table tmp_factmm_scmp_qualitystart as
select f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber, 
f.dd_purchaseorder, f.dd_purchaseorderitem, f.dim_unitofmeasureid, f.ct_quantity, f.dd_postingdate
from (
select * from tmp_factmm_scmp_101F union all 
select * from tmp_factmm_scmp_101B ) f;

/* Quality Start - Combine both 321 and UD Data - UD Data */
insert into tmp_factmm_scmp_qualitystart
(dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber, 
 dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity, dd_postingdate)
select 	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity, dd_postingdate 
from tmp_factmm_scmp_531 t
where not exists
(select 1
 from tmp_factmm_scmp_qualitystart f
  where f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber and f.dd_purchaseorder = t.dd_purchaseorder and
		f.dd_purchaseorderitem = t.dd_purchaseorderitem
);	
	

/* Quality End - Combine both MM and IL Data - MM Data */
drop table if exists tmp_factmm_scmp_qualityend;   
create table tmp_factmm_scmp_qualityend as
select 	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber, 
		dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, dd_postingdate, ct_quantity, 
		cast('MM_321' as varchar(1024) ) as dd_qualitynote, 'Y' as dd_qualityflag
from tmp_factmm_scmp_321;

/* Quality End - Combine both MM and IL Data - IL Data */ 
insert into tmp_factmm_scmp_qualityend
(dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber,
 dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, dd_postingdate, ct_quantity, dd_qualitynote, dd_qualityflag)
select 	i.dim_plantid, i.dim_partid, i.dd_partnumber, i.dd_plantcode, i.dd_batchnumber, i.dd_productionordernumber, 
		i.dd_purchaseorder, i.dd_purchaseorderitem, i.dim_unitofmeasureid, i.dd_postingdate, i.ct_quantity, 
		cast('IL_UDD' as varchar(1024) ) as dd_qualitynote, 'Y' as dd_qualityflag
from tmp_factmm_scmp_ud i
where not exists
(select * 
from tmp_factmm_scmp_qualityend m
where 	m.dim_plantid = i.dim_plantid and m.dim_partid = i.dim_partid and m.dd_partnumber = i.dd_partnumber and m.dd_plantcode = i.dd_plantcode and 
		m.dd_batchnumber = i.dd_batchnumber and m.dd_productionordernumber = i.dd_productionordernumber and 
		m.dd_purchaseorder = i.dd_purchaseorder and m.dd_purchaseorderitem = i.dd_purchaseorderitem and m.dim_unitofmeasureid = i.dim_unitofmeasureid);


/* Quality Part Plant Batch Unique Combination With Start and End Dates */ 
drop table if exists tmp_factmm_scmp_qualitystartend;   
create table tmp_factmm_scmp_qualitystartend as
select 	qs.dim_plantid, qs.dim_partid, qs.dd_partnumber, qs.dd_plantcode, qs.dd_batchnumber, qs.dd_productionordernumber, 
		qs.dd_purchaseorder, qs.dd_purchaseorderitem, qs.dim_unitofmeasureid, qs.ct_quantity,		
		qs.dd_postingdate as dd_postingdate_qualitystart, qe.dd_postingdate as dd_postingdate_qualityend,		
		case when qe.dd_postingdate is null then 'No QA End Date' else qe.dd_qualitynote end as dd_qualitynote,
		case when qe.dd_postingdate is null then 'Y' else qe.dd_qualityflag end as dd_qualityflag
from tmp_factmm_scmp_qualitystart qs left outer join tmp_factmm_scmp_qualityend qe
on  qs.dim_plantid = qe.dim_plantid and qs.dim_partid = qe.dim_partid and 
	qs.dd_partnumber = qe.dd_partnumber and qs.dd_plantcode = qe.dd_plantcode and qs.dd_batchnumber = qe.dd_batchnumber and 
	qs.dd_productionordernumber = qe.dd_productionordernumber and qs.dd_purchaseorder = qe.dd_purchaseorder and qs.dd_purchaseorderitem = qe.dd_purchaseorderitem and 
	qs.dim_unitofmeasureid = qe.dim_unitofmeasureid;


/* Quality Time - Update Production Order Data In Temp Fact Table - That Matchs Grain */
update tmp_fact_scmcycletime t
set t.dd_postingdate_qualitystart = f.dd_postingdate_qualitystart, 
	t.dd_postingdate_qualityend = f.dd_postingdate_qualityend, 
	t.dd_qualitynote = f.dd_qualitynote, 
	t.dd_qualityflag = f.dd_qualityflag
from tmp_factmm_scmp_qualitystartend f, tmp_fact_scmcycletime t
where 	f.dd_purchaseorder = 'Not Set' and f.dd_productionordernumber <> 'Not Set' and 
		f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber and f.dim_unitofmeasureid = t.dim_unitofmeasureid;

		
/* Quality Time - Insert Production Order Data Into Temp Fact Table - That Does Not Exist */
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_qualitystart, dd_postingdate_qualityend, dd_qualitynote, dd_qualityflag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_qualitystart, dd_postingdate_qualityend, dd_qualitynote, dd_qualityflag
from tmp_factmm_scmp_qualitystartend t
where t.dd_purchaseorder = 'Not Set' and t.dd_productionordernumber <> 'Not Set' and 
not exists 
( select 1
  from tmp_fact_scmcycletime f
  where f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber and f.dim_unitofmeasureid = t.dim_unitofmeasureid
);

		
/* Quality Time - Insert Purchase Order Data Into Temp Fact Table - We Insert Only That Does Not Match Grain */
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_qualitystart, dd_postingdate_qualityend, dd_qualitynote, dd_qualityflag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_qualitystart, dd_postingdate_qualityend, dd_qualitynote, dd_qualityflag
from tmp_factmm_scmp_qualitystartend t
where t.dd_purchaseorder <> 'Not Set' and t.dd_productionordernumber = 'Not Set' ;

/* No Update for Purchase Order data as we cant have a exact match */

/* Exception for Quality Time - Testing and Questions for Customer */
/* 	Case 1 - No Quality End Time
	Case 2 - No Production Order and Purchase Order */		
/* END - Quality Time - How long does it take to test any product, plant, batch */


		
/* Begin - Cycle 1, Cycle 2, Cycle 3 Times */			

/* Start of Cycle 1 */
/* Use Quality End and Production Start To Get Cycle 1 Time */ 
drop table if exists tmp_factmm_scmp_cycle1timestartend;   
create table tmp_factmm_scmp_cycle1timestartend as
select 	pe.dim_plantid, pe.dim_partid, pe.dd_partnumber, pe.dd_plantcode, pe.dd_batchnumber, pe.dd_productionordernumber, 
		pe.dd_purchaseorder, pe.dd_purchaseorderitem, pe.dim_unitofmeasureid, pe.ct_quantity,
		case when ps.dd_postingdate_prodstart is null then cast('0001-01-01'as date) else ps.dd_postingdate_prodstart end dd_postingdate_cycletimestart, 
		pe.dd_postingdate as dd_postingdate_cycletimeend, pe.dd_qualitynote as dd_cycle1note, pe.dd_qualityflag as dd_cycle1flag
from tmp_factmm_scmp_qualityend pe left outer join 
		tmp_factmm_scmp_261_min ps on ps.dd_productionordernumber = pe.dd_productionordernumber
where pe.dd_productionordernumber <> 'Not Set' and pe.dd_purchaseorder = 'Not Set';	

/* Update Missing Start Dates */
/* Step 1 - Update Cycle 1 Time Start Date For Records Where 261 Movement Was Not Found */ 
update tmp_factmm_scmp_cycle1timestartend f
set f.dd_postingdate_cycletimestart = case 	when p.dim_dateidactualstart <> 1 then p.actualstartdate
											when p.dim_dateidscheduledstart <> 1 then p.scheduledstartdate
											else f.dd_postingdate_cycletimestart end,
    f.dd_cycle1note = concat(f.dd_cycle1note,', ProdStartDate-261Missing') 
from tmp_factmm_scmp_prodstartend_fact_prodorder p, tmp_factmm_scmp_cycle1timestartend f
where f.dd_productionordernumber = p.dd_ordernumber and f.dd_postingdate_cycletimestart = '0001-01-01' and 
f.dd_postingdate_cycletimestart <> case when p.dim_dateidactualstart <> 1 then p.actualstartdate
										when p.dim_dateidscheduledstart <> 1 then p.scheduledstartdate
										else f.dd_postingdate_cycletimestart end;

/* Step 2: Update Cycle 1 Time Start Date For Records Where Earliest 261 Movement Is After Latest 101 Movement Using Production Order Fact Data */
update tmp_factmm_scmp_cycle1timestartend f
set f.dd_postingdate_cycletimestart = case 	when p.dim_dateidactualstart <> 1 then p.actualstartdate
											when p.dim_dateidscheduledstart <> 1 then p.scheduledstartdate
											else f.dd_postingdate_cycletimestart end,
    f.dd_cycle1note = concat(f.dd_cycle1note,', ProdStartDateAfterEndDateFix-ProdOrder-StartDate') 
from tmp_factmm_scmp_prodstartend_fact_prodorder p, tmp_factmm_scmp_cycle1timestartend f
where f.dd_productionordernumber = p.dd_ordernumber and f.dd_postingdate_cycletimestart > dd_postingdate_cycletimeend and 
f.dd_postingdate_cycletimestart <> case when p.dim_dateidactualstart <> 1 then p.actualstartdate
										when p.dim_dateidscheduledstart <> 1 then p.scheduledstartdate
										else f.dd_postingdate_cycletimestart end;

/* Step 3: Update Cycle 1 Time Start Date For Records Where Earliest 261 Movement Is After Latest 101 Movement */  
update tmp_factmm_scmp_cycle1timestartend f
set f.dd_postingdate_cycletimestart = f.dd_postingdate_cycletimeend,
    f.dd_cycle1note = concat(f.dd_cycle1note,', ProdStartDateAfterEndDateFix') 
from tmp_factmm_scmp_cycle1timestartend f
where f.dd_postingdate_cycletimestart > f.dd_postingdate_cycletimeend and f.dd_postingdate_cycletimestart <> f.dd_postingdate_cycletimeend;

/* Step 4: Update Production Start Date with '0001-01-01' */  
update tmp_factmm_scmp_cycle1timestartend f
set f.dd_postingdate_cycletimestart = f.dd_postingdate_cycletimeend,
    f.dd_cycle1note = concat(f.dd_cycle1note,', ProdStartDateBlank') 
from tmp_factmm_scmp_cycle1timestartend f
where f.dd_postingdate_cycletimestart = '0001-01-01' and f.dd_postingdate_cycletimestart <> f.dd_postingdate_cycletimeend;


/* Cycle 1 Time - Update Production Order Data In Temp Fact Table - That Matchs Grain */
update tmp_fact_scmcycletime t
set t.dd_postingdate_cycle1start = f.dd_postingdate_cycletimestart, 
	t.dd_postingdate_cycle1end = f.dd_postingdate_cycletimeend, 
	t.dd_cycle1note = f.dd_cycle1note,
	t.dd_cycle1flag = f.dd_cycle1flag
from tmp_factmm_scmp_cycle1timestartend f, tmp_fact_scmcycletime t
where 	f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber and f.dim_unitofmeasureid = t.dim_unitofmeasureid;


/* Cycle 1 Time - Insert Data Into Temp Fact Table - We Insert Only That Does Not Match Grain */
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_cycle1start, dd_postingdate_cycle1end, dd_cycle1note, dd_cycle1flag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber, 
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_cycletimestart, dd_postingdate_cycletimeend, dd_cycle1note, dd_cycle1flag
from tmp_factmm_scmp_cycle1timestartend t
where not exists 
( select 1
  from tmp_fact_scmcycletime f
  where f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		t.dd_productionordernumber = f.dd_productionordernumber and f.dim_unitofmeasureid = t.dim_unitofmeasureid
);
		
/* End of Cycle 1 */


/* Start of Cycle 2 */
/* Use the Production Start (Consumption Table) and Cycle 1 Time Start End Table To Get Cycle 2 Start Plant Part Batch*/ 
drop table if exists tmp_factmm_scmp_cycle2startppb;   
create table tmp_factmm_scmp_cycle2startppb as
select distinct c1.dd_productionordernumber as dd_productionordernumber_cycle1, ps.* 
from tmp_factmm_scmp_261 ps, tmp_factmm_scmp_cycle1timestartend c1
where ps.dd_productionordernumber = c1.dd_productionordernumber;

/* Use Cycle 2 Start Plant Part Batch and Production End Table To Get Previous Production Order */ 
drop table if exists tmp_factmm_scmp_cycle2startppbprod;   
create table tmp_factmm_scmp_cycle2startppbprod as
select distinct c2s.dd_productionordernumber_cycle1, pd.*
from tmp_factmm_scmp_101f pd, tmp_factmm_scmp_cycle2startppb c2s
where pd.dd_partnumber = c2s.dd_partnumber and pd.dd_batchnumber = c2s.dd_batchnumber;

/* Using Previous Production Order Get All Consumtion Records */
drop table if exists tmp_factmm_scmp_cycle2startppb261all;  
create table tmp_factmm_scmp_cycle2startppb261all as
select distinct c2s.dd_productionordernumber_cycle1, ps.*  
from tmp_factmm_scmp_261 ps, tmp_factmm_scmp_cycle2startppbprod c2s
where ps.dd_productionordernumber = c2s.dd_productionordernumber;

/* Get Previous Production Order for the Cycle 1 Production Order */
drop table if exists tmp_factmm_scmp_cycle2startppb261all_prodorder; 
create table tmp_factmm_scmp_cycle2startppb261all_prodorder as
select distinct c2s.dd_productionordernumber_cycle1, ps.dd_productionordernumber as dd_productionordernumber_cycle2  
from tmp_factmm_scmp_261 ps, tmp_factmm_scmp_cycle2startppbprod c2s
where ps.dd_productionordernumber = c2s.dd_productionordernumber;

/* Get Earliest Consumtion (261) For Previous Production Order */
drop table if exists tmp_factmm_scmp_cycle2startppb261;   
create table tmp_factmm_scmp_cycle2startppb261 as
select c2s.dd_productionordernumber_cycle1, min(ps.dd_postingdate) dd_postingdate_prodstart_cycle2  
from tmp_factmm_scmp_261 ps, tmp_factmm_scmp_cycle2startppbprod c2s
where ps.dd_productionordernumber = c2s.dd_productionordernumber
group by c2s.dd_productionordernumber_cycle1; 

/* Use the Production Start Temp Table To Get Cycle 2 Start */ 
drop table if exists tmp_factmm_scmp_cycle2timestartend;   
create table tmp_factmm_scmp_cycle2timestartend as
select 	pe.dim_plantid, pe.dim_partid, pe.dd_partnumber, pe.dd_plantcode, pe.dd_batchnumber, pe.dd_productionordernumber, 
		pe.dd_purchaseorder, pe.dd_purchaseorderitem, pe.dim_unitofmeasureid, pe.ct_quantity,
		case when ps.dd_postingdate_prodstart_cycle2 is null then cast('0001-01-01'as date) 
		else ps.dd_postingdate_prodstart_cycle2 end dd_postingdate_cycletimestart, 
		pe.dd_postingdate as dd_postingdate_cycletimeend, pe.dd_qualitynote as dd_cycle2note, pe.dd_qualityflag as dd_cycle2flag
from tmp_factmm_scmp_qualityend pe left outer join 
		tmp_factmm_scmp_cycle2startppb261 ps on ps.dd_productionordernumber_cycle1 = pe.dd_productionordernumber
where pe.dd_productionordernumber <> 'Not Set' and pe.dd_purchaseorder = 'Not Set';


/* Cycle 2 Time - Update Production Order Data In Temp Fact Table - That Matchs Grain */
update tmp_fact_scmcycletime t
set t.dd_postingdate_cycle2start = f.dd_postingdate_cycletimestart, 
	t.dd_postingdate_cycle2end = f.dd_postingdate_cycletimeend, 
	t.dd_cycle2note = f.dd_cycle2note,
	t.dd_cycle2flag = f.dd_cycle2flag
from tmp_factmm_scmp_cycle2timestartend f, tmp_fact_scmcycletime t
where 	f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber and f.dim_unitofmeasureid = t.dim_unitofmeasureid and 
		t.dd_cycle1flag = 'Y';

/* No Insert Needed As All Prod Data Matches */		
/* End of Cycle 2 */

/* Start of Cycle 3 */
/* Cycle 3 - Use the Production Start (Consumption Table) and Cycle 2 Time Start End Plant Part Batch Production Order*/ 
drop table if exists tmp_factmm_scmp_cycle3startppb;   
create table tmp_factmm_scmp_cycle3startppb as
select distinct c2.dd_productionordernumber_cycle1, c2.dd_productionordernumber_cycle2,  ps.* 
from tmp_factmm_scmp_261 ps, tmp_factmm_scmp_cycle2startppb261all_prodorder c2
where ps.dd_productionordernumber = c2.dd_productionordernumber_cycle2;
-- and ps.dd_productionordernumber = 'J37935' -- 'H76845';

/* Use Cycle 3 Start Plant Part Batch and Production End Table To Get Previous Production Order */ 
drop table if exists tmp_factmm_scmp_cycle3startppbprod;   
create table tmp_factmm_scmp_cycle3startppbprod as
select distinct c3s.dd_productionordernumber_cycle1, c3s.dd_productionordernumber_cycle2, pd.*
from tmp_factmm_scmp_101f pd, tmp_factmm_scmp_cycle3startppb c3s
where pd.dd_partnumber = c3s.dd_partnumber and pd.dd_batchnumber = c3s.dd_batchnumber;
--pd.dim_plantid = c3s.dim_plantid and pd.dim_partid = c3s.dim_partid and pd.dd_plantcode = c3s.dd_plantcode
-- and dd_productionordernumber_cycle1 = 'J37935'

/* Using Previous Production Order Get All Consumtion Records */
drop table if exists tmp_factmm_scmp_cycle3startppb261all;  
create table tmp_factmm_scmp_cycle3startppb261all as
select distinct c3s.dd_productionordernumber_cycle1, c3s.dd_productionordernumber_cycle2, ps.*  
from tmp_factmm_scmp_261 ps, tmp_factmm_scmp_cycle3startppbprod c3s
where ps.dd_productionordernumber = c3s.dd_productionordernumber;
--and ps.dd_productionordernumber = 'J37934'
-- 'J37935' -- 'H76845'

/* Get Previous Production Order for the Cycle 2 Production Order */
drop table if exists tmp_factmm_scmp_cycle3startppb261all_prodorder;  
create table tmp_factmm_scmp_cycle3startppb261all_prodorder as
select distinct c3s.dd_productionordernumber_cycle1, c3s.dd_productionordernumber_cycle2, 
		ps.dd_productionordernumber as dd_productionordernumber_cycle3  
from tmp_factmm_scmp_261 ps, tmp_factmm_scmp_cycle3startppbprod c3s
where ps.dd_productionordernumber = c3s.dd_productionordernumber;

/* Get Earliest Consumtion (261) For Previous Production Order */
drop table if exists tmp_factmm_scmp_cycle3startppb261;   
create table tmp_factmm_scmp_cycle3startppb261 as
select c3s.dd_productionordernumber_cycle1, -- c3s.dd_productionordernumber_cycle2, 
		min(ps.dd_postingdate) dd_postingdate_prodstart_cycle3  
from tmp_factmm_scmp_261 ps, tmp_factmm_scmp_cycle3startppbprod c3s
where ps.dd_productionordernumber = c3s.dd_productionordernumber
--and ps.dd_productionordernumber = 'J37934'
group by c3s.dd_productionordernumber_cycle1; --, c3s.dd_productionordernumber_cycle2; 
-- 'J37935' -- 'H76845';

/* Use the Production Start Temp Table To Get Cycle 3 Start */
drop table if exists tmp_factmm_scmp_cycle3timestartend;   
create table tmp_factmm_scmp_cycle3timestartend as
select 	pe.dim_plantid, pe.dim_partid, pe.dd_partnumber, pe.dd_plantcode, pe.dd_batchnumber, pe.dd_productionordernumber, 
		pe.dd_purchaseorder, pe.dd_purchaseorderitem, pe.dim_unitofmeasureid, pe.ct_quantity,
		case when ps.dd_postingdate_prodstart_cycle3 is null then cast('0001-01-01'as date) 
		else ps.dd_postingdate_prodstart_cycle3 end dd_postingdate_cycletimestart, 
		pe.dd_postingdate as dd_postingdate_cycletimeend, pe.dd_qualitynote as dd_cycle3note, pe.dd_qualityflag as dd_cycle3flag 
from tmp_factmm_scmp_qualityend pe left outer join 
		tmp_factmm_scmp_cycle3startppb261 ps on ps.dd_productionordernumber_cycle1 = pe.dd_productionordernumber
where pe.dd_productionordernumber <> 'Not Set' and pe.dd_purchaseorder = 'Not Set';
-- where pe.dd_batchnumber = 'H76845';


/* Cycle 3 Time - Update Production Order Data In Temp Fact Table - That Matchs Grain */
update tmp_fact_scmcycletime t
set t.dd_postingdate_cycle3start = f.dd_postingdate_cycletimestart, 
	t.dd_postingdate_cycle3end = f.dd_postingdate_cycletimeend, 
	t.dd_cycle3note = f.dd_cycle3note,
	t.dd_cycle3flag = f.dd_cycle3flag
from tmp_factmm_scmp_cycle3timestartend f, tmp_fact_scmcycletime t
where 	f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber and f.dim_unitofmeasureid = t.dim_unitofmeasureid and
		t.dd_cycle1flag = 'Y';

/* No Insert Needed As All Prod Data Matches */	

/* End of Cycle 3 */
/* End - Cycle 1, Cycle 2, Cycle 3 Times */	


/* Total Lead Time Using Reverse Hierarchy / Geneaology Data */
/* Total Lead Time - Get All The 101 Movements For The Production Order Output */ 

/* Get the Level 0 Part Batch For Each Production Part Plant Batch Record */	
drop table if exists tmp_factmm_scmp_tltstartendlevel0;  
create table tmp_factmm_scmp_tltstartendlevel0 as
select  distinct 
		e.dim_plantid, e.dim_partid, e.dd_partnumber, e.dd_plantcode, e.dd_batchnumber, e.dd_productionordernumber, 
		e.dd_purchaseorder, e.dd_purchaseorderitem, e.dim_unitofmeasureid, e.ct_quantity,
		e.dd_postingdate as dd_postingdate_tltend, s.dd_level0partno, s.dd_level0batchno
from tmp_factmm_scmp_101F e
left outer join fact_reverse_batchgeneology_mmgrain_treenonodes s on
		s.dim_plantid = e.dim_plantid and s.dim_partid = e.dim_partid and 
		s.dd_partnumber = e.dd_partnumber and s.dd_plantcode = e.dd_plantcode and
		s.dd_batchnumber = e.dd_batchnumber and s.dd_productionordernumber = e.dd_productionordernumber;
-- where e.dd_partnumber = 'F000017269' and e.dd_batchnumber = 'M55524' and e.dd_plantcode = 'US16';

/* Get the earliest movement for level 0 part batch */	
drop table if exists tmp_factmm_scmp_tltstart_min;   
create table tmp_factmm_scmp_tltstart_min as
select dd_level0partno, dd_level0batchno, min(dd_postingdate) as dd_postingdate_tltstart
from fact_reverse_batchgeneology_mmgrain_treenonodes
where dd_movementtype in ('261','262')
group by dd_level0partno, dd_level0batchno;

/* Get the earliest movement against the production part plant batch */	 /* 252355 */
drop table if exists tmp_factmm_scmp_tltstartendfinal;   
create table tmp_factmm_scmp_tltstartendfinal as
select  distinct
		e.dim_plantid, e.dim_partid, e.dd_partnumber, e.dd_plantcode, e.dd_batchnumber, e.dd_productionordernumber, 
		e.dd_purchaseorder, e.dd_purchaseorderitem, e.dim_unitofmeasureid, e.ct_quantity,
		e.dd_postingdate_tltend, 
		case when min(s.dd_postingdate_tltstart) is null then e.dd_postingdate_tltend 
			 else min(s.dd_postingdate_tltstart) end as dd_postingdate_tltstart,
		cast('Level0-261To101F' as varchar(500) ) as dd_totalleadnote, 'Y' as dd_totalleadflag
from tmp_factmm_scmp_tltstartendlevel0 e
left outer join  tmp_factmm_scmp_tltstart_min s on
		e.dd_level0partno = s.dd_level0partno and e.dd_level0batchno = s.dd_level0batchno
-- where e.dd_partnumber = 'F000017269' and e.dd_batchnumber = 'M55524' and e.dd_plantcode = 'US16'
group by e.dim_plantid, e.dim_partid, e.dd_partnumber, e.dd_plantcode, e.dd_batchnumber, e.dd_productionordernumber, 
		 e.dd_purchaseorder, e.dd_purchaseorderitem, e.dim_unitofmeasureid, e.ct_quantity, e.dd_postingdate_tltend;

/* Test Level 0 Earliest 261 Consumption Date */
/*
drop table if exists tmp_factmm_scmp_tltstart_min_check   
create table tmp_factmm_scmp_tltstart_min_check as
select 	l.dim_plantid, l.dim_partid, l.dd_partnumber, l.dd_plantcode, l.dd_batchnumber, l.dd_productionordernumber, 
		l.dd_purchaseorder, l.dd_purchaseorderitem, l.dim_unitofmeasureid, l.ct_quantity,
		l.dd_postingdate_tltend, l.dd_level0partno, l.dd_level0batchno,
		min(g.dd_postingdate) as dd_postingdate_tltstart
from tmp_factmm_scmp_tltstartendlevel0 l 
inner join fact_reverse_batchgeneology_mmgrain_treenonodes g on l.dd_level0partno = g.dd_level0partno and l.dd_level0batchno = g.dd_level0batchno
where g.dd_movementtype in ('261','262')
and l.dd_partnumber = 'F000017269' and l.dd_batchnumber = 'M55524' and l.dd_plantcode = 'US16'
group by l.dim_plantid, l.dim_partid, l.dd_partnumber, l.dd_plantcode, l.dd_batchnumber, l.dd_productionordernumber, 
		l.dd_purchaseorder, l.dd_purchaseorderitem, l.dim_unitofmeasureid, l.ct_quantity,
		l.dd_postingdate_tltend, l.dd_level0partno, l.dd_level0batchno
*/

/* Total Lead Time - Update Production Order Data In Temp Fact Table - That Matchs Grain */
update tmp_fact_scmcycletime t
set t.dd_postingdate_tltstart = f.dd_postingdate_tltstart, 
	t.dd_postingdate_tltend = f.dd_postingdate_tltend, 
	t.dd_totalleadnote = f.dd_totalleadnote,
	t.dd_totalleadflag = f.dd_totalleadflag
from tmp_factmm_scmp_tltstartendfinal f, tmp_fact_scmcycletime t
where 	f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber and f.dim_unitofmeasureid = t.dim_unitofmeasureid and 
		t.dd_prodflag = 'Y';

/* Test Query */
/* select 	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, 
		dd_productionordernumber, dim_unitofmeasureid, ct_quantity,
		dd_postingdate_tltend, dd_postingdate_tltstart, dd_totalleadnote, dd_totalleadflag
from tmp_factmm_scmp_tltstartendfinal t
where not exists 
( select 1
  from tmp_fact_scmcycletime f
  where f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		t.dd_productionordernumber = f.dd_productionordernumber
) */
/* select *
from tmp_factmm_scmp_tltstartendfinal t
where  exists 
( select 1
  from tmp_fact_scmcycletime f
  where f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber ) */

		



/*
Transfer Time - Cross Company Stock Transfer:
for each of the batch product plant select 101 movement for PO. the latest date of the 101 will be the "end start of transfer date"
Find the delivery/Purchase order related to that movement in the material movement table. 
Find the earlieast movement of 643/645 type against this PO. This will be the "start end of transfer date." 
Calculate the time between both start and end date and create the "Transfer time". 
Note that the plant will be different here. we should create a column for the originating plant 643/645 and the destnation plant(101)
*/
	
/* Transfer Time Dates - Part Plant Batch Purchase Order Unique Combination With Start and End Dates */ 
drop table if exists tmp_factmm_scmp_transccstartend;   
create table tmp_factmm_scmp_transccstartend as
select 	te.dim_plantid, te.dim_partid, te.dd_partnumber, te.dd_plantcode, te.dd_batchnumber, te.dd_productionordernumber,
		te.dd_purchaseorder, te.dd_purchaseorderitem, te.dim_unitofmeasureid, te.ct_quantity,
		ts.dd_postingdate as dd_postingdate_transccstart, te.dd_postingdate as dd_postingdate_transccend,
		cast('643/645-101B Match' as varchar(1024) ) as dd_transfercrscomnote, 'Y' as dd_transfercrscomflag
from tmp_factmm_scmp_643645 ts, tmp_factmm_scmp_101B te
where ts.dd_purchaseorder = te.dd_purchaseorder and ts.dd_purchaseorderitem = te.dd_purchaseorderitem and ts.dd_batchnumber = te.dd_batchnumber;

/* Cross Company Transfer Time - Insert Data Into Temp Fact Table */
/* Since we dont have the grain with Purchase Item Number We will Only insert */
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber,
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_transccstart, dd_postingdate_transccend, dd_transfercrscomnote, dd_transfercrscomflag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber,
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_transccstart, dd_postingdate_transccend, dd_transfercrscomnote, dd_transfercrscomflag
from tmp_factmm_scmp_transccstartend t;

/* Exception for Cross Company Time - */
/* 	Should the 101-B be Max Date?  */		


/*
Transfer Time - Intra Company Stock Transfer
for each of the batch product plant select 101 movement for PO. the latest date of the 101 will be the "end start of transfer date"
Find the delivery/Purchase order related to that movement in the material movement table. 
Find the earlieast movement of 641/647 type against this PO. This will be the "start end of transfer date." 
Calculate the time between both start and end date and create the "Transfer time". 
Note that the plant will be different here. we should create a column for the originating plant 641/647 and the destnation plant(101)
*/
	
/* Intra Company Transfer Time Dates - Part Plant Batch Purchase Order Unique Combination With Start and End Dates */ 
drop table if exists tmp_factmm_scmp_transintracompstartend;   
create table tmp_factmm_scmp_transintracompstartend as
select 	te.dim_plantid, te.dim_partid, te.dd_partnumber, te.dd_plantcode, te.dd_batchnumber, te.dd_productionordernumber,
		te.dd_purchaseorder, te.dd_purchaseorderitem, te.dim_unitofmeasureid, te.ct_quantity,
		ts.dd_postingdate as dd_postingdate_transintracompstart, te.dd_postingdate as dd_postingdate_transintracompend,
		cast('641/647-101B Match' as varchar(1024) ) as dd_transintracompnote, 'Y' as dd_transintracompflag
from tmp_factmm_scmp_641647 ts, tmp_factmm_scmp_101B te
where ts.dd_purchaseorder = te.dd_purchaseorder and ts.dd_purchaseorderitem = te.dd_purchaseorderitem and ts.dd_batchnumber = te.dd_batchnumber;

/* Intra Company Transfer Time - Insert Data Into Temp Fact Table */
/* Since we dont have the grain with Purchase Item Number We will Only insert */
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber,
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_transintracompstart, dd_postingdate_transintracompend, dd_transintracompnote, dd_transintracompflag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_productionordernumber,
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_transintracompstart, dd_postingdate_transintracompend, dd_transintracompnote, dd_transintracompflag
from tmp_factmm_scmp_transintracompstartend t;

/* Exception for Intra Company Time - */
/* 	Should the 101-B be Max Date?  */		



/*
Variant - Subcontracting Transfer time:
for each of the batch product plant select 101 movement for PO. the earliest date of the 101 will be the "end of transfer date"
Find the delivery/Purchase order related to that movement in the material movement table. 
Find the earlieast movement of 541/543 type against this PO. This will be the "start of subcontracting date." 
Calculate the time between both start and end date and create the "subcontracting time". Note that the plant will be different here. we should create a column for the originating plant 541/543 and the destnation plant(101)
*/

drop table if exists tmp_factmm_scmp_subcontractstartend;   
create table tmp_factmm_scmp_subcontractstartend as
select 	te.dim_plantid, te.dim_partid, te.dd_partnumber, te.dd_plantcode, te.dd_batchnumber, te.dd_productionordernumber,
		te.dd_purchaseorder, te.dd_purchaseorderitem, te.dim_unitofmeasureid, te.ct_quantity,		
		ts.dd_postingdate as dd_postingdate_subcontractstart, te.dd_postingdate as dd_postingdate_subcontractend,
		cast('543-101B Match' as varchar(1024) ) as dd_transfersubconnote, 'Y' as dd_transfersubconflag
from tmp_factmm_scmp_543 ts, tmp_factmm_scmp_101B te
where ts.dd_purchaseorder = te.dd_purchaseorder and ts.dd_purchaseorderitem = te.dd_purchaseorderitem;

/* Sub Contract Time - Insert Data Into Temp Fact Table - We Insert Only That Does Not Match Grain */
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_subcontractstart, dd_postingdate_subcontractend,  dd_transfersubconnote, dd_transfersubconflag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_subcontractstart, dd_postingdate_subcontractend,  dd_transfersubconnote, dd_transfersubconflag
from tmp_factmm_scmp_subcontractstartend t;

/* Exception - Logic needs to be reviewed. Very Important */
/* Test Case and f.dd_documentno = '4502828179' and f.dd_documentitemno = 10 and f.dd_materialdocno = 5010848105 */



/*	Product At Risk Wait Time
For each of the batch product plant select 101 movement for production. the earliest date of the 101 will be the "start of product at risk wait time"
Find the earliest movement of 261 type for the batch product plant selected in step 1. This will be the "end of product at risk wait time " 
Calculate the time between both start and end date and create the "product at risk wait time"	*/

/* Get All The 261 Movements For The Production Order Consumption */ 
drop table if exists tmp_factmm_scmp_parwtend;   
create table tmp_factmm_scmp_parwtend as
select 	f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, min(dd_postingdate) as dd_postingdate_parwtend
from tmp_factmm_scmp_261 f
group by f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber;

/* Part Plant Batch Production Order Unique Combination With Start and End Dates */ 
drop table if exists tmp_factmm_scmp_parwtstartend;   
create table tmp_factmm_scmp_parwtstartend as
select 	ps.dim_plantid, ps.dim_partid, ps.dd_partnumber, ps.dd_plantcode, ps.dd_batchnumber,
		ps.dd_productionordernumber, ps.dim_unitofmeasureid, ps.ct_quantity,
		ps.dd_postingdate as dd_postingdate_prodriskwaitstart, 
		case when pe.dd_postingdate_parwtend < ps.dd_postingdate then ps.dd_postingdate else pe.dd_postingdate_parwtend end dd_postingdate_prodriskwaitend,
		'261-101 Match' as dd_prodriskwaitnote, 'Y' as dd_prodriskwaitflag
from tmp_factmm_scmp_101F ps inner join 
		tmp_factmm_scmp_parwtend pe on ps.dim_plantid = pe.dim_plantid and ps.dim_partid = pe.dim_partid and ps.dd_batchnumber = pe.dd_batchnumber;


/* Product At Risk Wait Time - Update Production Order Data In Temp Fact Table - That Matchs Grain */
update tmp_fact_scmcycletime t
set t.dd_postingdate_prodriskwaitstart = f.dd_postingdate_prodriskwaitstart, 
	t.dd_postingdate_prodriskwaitend = f.dd_postingdate_prodriskwaitend, 
	t.dd_prodriskwaitnote = f.dd_prodriskwaitnote,
	t.dd_prodriskwaitflag = f.dd_prodriskwaitflag
from tmp_factmm_scmp_parwtstartend f, tmp_fact_scmcycletime t
where 	f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and 
		f.dd_productionordernumber = t.dd_productionordernumber and
		t.dd_prodflag = 'Y';





/*
Wait time:
For each batch location product find the earliest 321 or UD date. Quality End Time will be Wait Start Time.
Find the next earliest 261/262/541/543/643/645/641/647/309/201/601
calculate the diference. This would be the wait time.
*/

/* Case - Wait Time From Quality Release To First Movement */ /*1755748*//*3374033*/
drop table if exists tmp_factmm_scmp_waitstartall_321;   
create table tmp_factmm_scmp_waitstartall_321 as
select distinct
f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber,
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, 
f.dd_materialdocno, f.dd_materialdocitemno, f.dd_materialdocyear, f.dim_dateidpostingdate, pd.datevalue as dd_postingdate, f.ct_quantity, f.ct_qtyentryuom, 
mt.movementtype dd_movementtype, mt.movementindicator dd_movementindicator, f.dim_uomunitofentryid, f.dim_unitofmeasureid, f.dim_movementtypeid
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid  
and mt.movementtype in ('321','322')
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1;

/*877382*//*863174*/ /*1686094*/
drop table if exists tmp_factmm_scmp_waitstart_321;   
create table tmp_factmm_scmp_waitstart_321 as
select f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, f.dd_materialdocno,
f.dd_productionordernumber, f.dd_purchaseorder, f.dd_purchaseorderitem, f.dim_unitofmeasureid, sum(f.ct_quantity) as ct_quantity, 
min(f.dd_postingdate) dd_postingdate_waitstart
from tmp_factmm_scmp_waitstartall_321 f
group by f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, f.dd_materialdocno,
f.dd_productionordernumber, f.dd_purchaseorder, f.dd_purchaseorderitem, f.dim_unitofmeasureid;

/*1908133*//*1915502*/
drop table if exists tmp_factmm_scmp_waitstartall_ud;   
create table tmp_factmm_scmp_waitstartall_ud as
select distinct
f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchno dd_batchnumber, f.dd_orderno dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, f.dim_lotunitofmeasureid as dim_unitofmeasureid, 
f.dd_materialdocno, f.dd_materialdocitemno, f.dd_materialdocyear, f.dim_dateidusagedecisionmade, ud.datevalue as dd_usagedecisionmadedate, f.ct_inspectionlotqty ct_quantity
from fact_inspectionlot f, dim_part dp, dim_plant pl, dim_date ud, dim_CodeText ct
where f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidusagedecisionmade = ud.dim_dateid and f.Dim_InspUsageDecisionId = ct.Dim_CodeTextId
and f.dd_batchno <> 'Not Set' and f.dd_materialdocno <> 'Not Set' and ct.code <> 'R'
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidusagedecisionmade <> 1;

/*1849170*/ /*1856349*//*1912604*/
drop table if exists tmp_factmm_scmp_waitstart_ud;   
create table tmp_factmm_scmp_waitstart_ud as
select f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, f.dd_materialdocno, 
f.dd_productionordernumber, f.dd_purchaseorder, f.dd_purchaseorderitem, f.dim_unitofmeasureid, sum(f.ct_quantity) as ct_quantity,
min(f.dd_usagedecisionmadedate) dd_postingdate_waitstart
from tmp_factmm_scmp_waitstartall_ud f
group by f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, f.dd_materialdocno, f.dd_productionordernumber, f.dd_purchaseorder, f.dd_purchaseorderitem, f.dim_unitofmeasureid;

/*863174*//*1686094*/
drop table if exists tmp_factmm_scmp_waitstart_all;   
create table tmp_factmm_scmp_waitstart_all as
select f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, f.dd_materialdocno, f.dd_productionordernumber, 
f.dd_purchaseorder, f.dd_purchaseorderitem, f.dim_unitofmeasureid, f.ct_quantity, f.dd_postingdate_waitstart, cast('MM_321' as varchar(1024) ) as dd_waitnote, 'Y' as dd_waitflag
from tmp_factmm_scmp_waitstart_321 f;

/*1856282*/  -- 1856349 -- 1330434 -- 1896564
insert into tmp_factmm_scmp_waitstart_all
(dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, dd_materialdocno, dd_productionordernumber, 
dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity, dd_postingdate_waitstart, dd_waitnote, dd_waitflag)
select f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, f.dd_materialdocno, f.dd_productionordernumber, 
f.dd_purchaseorder, f.dd_purchaseorderitem, f.dim_unitofmeasureid, f.ct_quantity, f.dd_postingdate_waitstart, cast('IL_UDD' as varchar(1024) ) dd_waitnote, 'Y' as dd_waitflag
from tmp_factmm_scmp_waitstart_ud f
where not exists
(select 1
 from tmp_factmm_scmp_waitstart_321 q
 where 	f.dim_plantid = q.dim_plantid and f.dim_partid = q.dim_partid and f.dd_partnumber = q.dd_partnumber and 
		f.dd_plantcode = q.dd_plantcode and f.dd_batchnumber = q.dd_batchnumber and f.dd_materialdocno = q.dd_materialdocno and 
		f.dd_productionordernumber = q.dd_productionordernumber and f.dd_purchaseorder = q.dd_purchaseorder and
		f.dd_purchaseorderitem = q.dd_purchaseorderitem -- and f.dim_unitofmeasureid = q.dim_unitofmeasureid
);

/* If a Batch has multiple quality movments then we rank them by posting date asc */ /*877382*/
drop table if exists tmp_factmm_scmp_waitstart_all_rank;   
create table tmp_factmm_scmp_waitstart_all_rank as
select 	f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber, f.dd_materialdocno, 
		f.dd_productionordernumber, f.dd_purchaseorder, f.dd_purchaseorderitem, 
		f.dim_unitofmeasureid, f.ct_quantity, f.dd_postingdate_waitstart, f.dd_waitnote, f.dd_waitflag,
		dense_rank() over(partition by f.dim_plantid, f.dim_partid, f.dd_partnumber, f.dd_plantcode, f.dd_batchnumber order by dd_postingdate_waitstart asc) dd_rank
from tmp_factmm_scmp_waitstart_all f;

/*877382*/
drop table if exists tmp_factmm_scmp_waitstart_all_rank_between;   
create table tmp_factmm_scmp_waitstart_all_rank_between as
select distinct
f.*, case when l.dd_postingdate_waitstart is null then current_date 
				 else l.dd_postingdate_waitstart-1 end  as dd_postingdate_waitstartto
from tmp_factmm_scmp_waitstart_all_rank f
left outer join tmp_factmm_scmp_waitstart_all_rank l on
		f.dim_plantid = l.dim_plantid and f.dim_partid = l.dim_partid and 
		f.dd_partnumber = l.dd_partnumber and f.dd_plantcode = l.dd_plantcode and
		f.dd_batchnumber = l.dd_batchnumber  and f.dd_rank+1 = l.dd_rank;

/*49346785*/ /*99343952*/
drop table if exists tmp_factmm_scmp_waitendall;   
create table tmp_factmm_scmp_waitendall as
select distinct
f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, 
f.dd_materialdocno, f.dd_materialdocitemno, f.dd_materialdocyear, f.dim_dateidpostingdate, pd.datevalue as dd_postingdate, f.ct_quantity, f.ct_qtyentryuom, 
mt.movementtype dd_movementtype, mt.movementindicator dd_movementindicator, f.dim_uomunitofentryid, f.dim_unitofmeasureid, f.dim_movementtypeid
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid 
and mt.movementtype in ('261','262','541','543','643','645','641','647','309','201','601')
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' 
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1;

/*877382*/
drop table if exists tmp_factmm_scmp_waitstartendfinal;   
create table tmp_factmm_scmp_waitstartendfinal as
select  s.dim_plantid, s.dim_partid, s.dd_partnumber, s.dd_plantcode, s.dd_batchnumber, 
		s.dd_materialdocno, s.dd_productionordernumber, s.dd_purchaseorder, s.dd_purchaseorderitem, 
		s.dim_unitofmeasureid, s.ct_quantity, s.dd_rank, s.dd_postingdate_waitstart, 
		min(e.dd_postingdate) as dd_postingdate_waitend,
--		case when min(e.dd_postingdate) is null then current_date else min(e.dd_postingdate) end 
		concat(s.dd_waitnote, ', QA End To Next Step') as dd_waitnote, s.dd_waitflag
from tmp_factmm_scmp_waitstart_all_rank_between s
left outer join  tmp_factmm_scmp_waitendall e on
		s.dim_plantid = e.dim_plantid and s.dim_partid = e.dim_partid and 
		s.dd_partnumber = e.dd_partnumber and s.dd_plantcode = e.dd_plantcode and
		s.dd_batchnumber = e.dd_batchnumber  and 
		e.dd_postingdate between s.dd_postingdate_waitstart and s.dd_postingdate_waitstartto
-- where s.dd_batchnumber = 'L30911'
group by 	s.dim_plantid, s.dim_partid, s.dd_partnumber, s.dd_plantcode, s.dd_batchnumber, 
			s.dd_materialdocno, s.dd_productionordernumber, s.dd_purchaseorder, s.dd_purchaseorderitem,
			s.dim_unitofmeasureid, s.ct_quantity, s.dd_rank, s.dd_postingdate_waitstart,
			concat(s.dd_waitnote, ', QA End To Next Step'), s.dd_waitflag;


/* Wait Time - Insert Data Into Temp Fact Table - We Insert Only That Does Not Match Grain */
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_materialdocno, dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_waitstart, dd_postingdate_waitend, dd_waitnote, dd_waitflag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_materialdocno, dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_waitstart, dd_postingdate_waitend, dd_waitnote, dd_waitflag
from tmp_factmm_scmp_waitstartendfinal t;

/*where not exists 
( select 1
  from tmp_fact_scmcycletime f
  where f.dim_plantid = t.dim_plantid and f.dim_partid = t.dim_partid and f.dd_partnumber = t.dd_partnumber and 
		f.dd_plantcode = t.dd_plantcode and f.dd_batchnumber = t.dd_batchnumber and t.dd_materialdocno = f.dd_materialdocno and
		f.dd_productionordernumber = t.dd_productionordernumber and f.dd_purchaseorder = t.dd_purchaseorder and
		f.dd_purchaseorderitem = t.dd_purchaseorderitem -- and f.dim_unitofmeasureid = q.dim_unitofmeasureid
		
)*/

/* No Update To Temp Fact Table - Because Wait Time uses Material Movement Document */
			
			

/*	Ship Wait Time
For each of the batch product plant select 321 movement for quality. the earliest date of the 321 will be the "start of ship wait time"
Find the earliest movement of either 643/645/541/543/641/647/601 types for the batch product plant selected in step 1. This will be the "end of ship wait time" 
Calculate the time between both start and end date and create the "ship wait time"	*/
/* Ship Wait Time */
/* Get Movemement Records For 321 */
		
/* get all the movements for following movement codes 643/645/541/543/641/647/601 */
drop table if exists tmp_factmm_scmp_shipwaitendall;   
create table tmp_factmm_scmp_shipwaitendall as
select distinct
f.dim_plantid, f.dim_partid, dp.partnumber dd_partnumber, pl.plantcode dd_plantcode, f.dd_batchnumber, f.dd_productionordernumber, 
f.dd_documentno dd_purchaseorder, f.dd_documentitemno dd_purchaseorderitem, 
f.dd_materialdocno, f.dd_materialdocitemno, f.dd_materialdocyear, f.dim_dateidpostingdate, pd.datevalue as dd_postingdate, f.ct_quantity, f.ct_qtyentryuom, 
mt.movementtype dd_movementtype, mt.movementindicator dd_movementindicator, f.dim_uomunitofentryid, f.dim_unitofmeasureid, f.dim_movementtypeid
from fact_materialmovement f,dim_movementtype mt,dim_part dp, dim_plant pl, dim_date pd
where f.dim_movementtypeid = mt.dim_movementtypeid  and f.dim_partid = dp.dim_partid and f.dim_plantid = pl.dim_plantid and f.dim_dateidpostingdate = pd.dim_dateid 
and mt.movementtype in ('541','543','643','645','641','647','601')
and f.dd_batchnumber <> 'Not Set' and f.dd_materialdocno <> 'Not Set' 
and f.dim_partid <> 1 and f.dim_plantid <> 1 and f.dim_dateidpostingdate <> 1;

/*Join start and end temp tables to get the end date */
drop table if exists tmp_factmm_scmp_shipwaitstartendfinal;   
create table tmp_factmm_scmp_shipwaitstartendfinal as
select  s.dim_plantid, s.dim_partid, s.dd_partnumber, s.dd_plantcode, s.dd_batchnumber, 
		s.dd_materialdocno, s.dd_productionordernumber, s.dd_purchaseorder, s.dd_purchaseorderitem, 
		s.dim_unitofmeasureid, s.ct_quantity, s.dd_rank, s.dd_postingdate_waitstart as dd_postingdate_shipwaitstart, 
		min(e.dd_postingdate) as dd_postingdate_shipwaitend,
--		case when min(e.dd_postingdate) is null then current_date else min(e.dd_postingdate) end
		concat(s.dd_waitnote, ', QA End To Next Step') as dd_shipwaitnote, s.dd_waitflag as dd_shipwaitflag
from tmp_factmm_scmp_waitstart_all_rank_between s
left outer join  tmp_factmm_scmp_shipwaitendall e on
		s.dim_plantid = e.dim_plantid and s.dim_partid = e.dim_partid and 
		s.dd_partnumber = e.dd_partnumber and s.dd_plantcode = e.dd_plantcode and
		s.dd_batchnumber = e.dd_batchnumber  and 
		e.dd_postingdate between s.dd_postingdate_waitstart and s.dd_postingdate_waitstartto
-- where s.dd_batchnumber = 'L30911'
group by 	s.dim_plantid, s.dim_partid, s.dd_partnumber, s.dd_plantcode, s.dd_batchnumber, 
			s.dd_materialdocno, s.dd_productionordernumber, s.dd_purchaseorder, s.dd_purchaseorderitem,
			s.dim_unitofmeasureid, s.ct_quantity, s.dd_rank, s.dd_postingdate_waitstart,
			concat(s.dd_waitnote, ', QA End To Next Step'), s.dd_waitflag;


/* Wait Time - Insert Data Into Temp Fact Table - We Insert Only That Does Not Match Grain */
delete from number_fountain m where m.table_name = 'tmp_fact_scmcycletime';

insert into number_fountain
select  'tmp_fact_scmcycletime', 
		ifnull(max(f.fact_scmcycletimeid), 
		ifnull((select (s.dim_projectsourceid * s.multiplier)-1 from dim_projectsource s),0))
from tmp_fact_scmcycletime f;

insert into tmp_fact_scmcycletime
(	fact_scmcycletimeid, dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_materialdocno, dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_shipwaitstart, dd_postingdate_shipwaitend, dd_shipwaitnote, dd_shipwaitflag )
select 
	(select ifnull(m.max_id, 0) from number_fountain m 
	where m.table_name = 'tmp_fact_scmcycletime') + row_number() over(order by '') fact_scmcycletimeid,
	dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
	dd_materialdocno, dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dim_unitofmeasureid, ct_quantity,
	dd_postingdate_shipwaitstart, dd_postingdate_shipwaitend, dd_shipwaitnote, dd_shipwaitflag
from tmp_factmm_scmp_shipwaitstartendfinal t;



			
update tmp_fact_scmcycletime
set ct_prodtime = case when dd_postingdate_prodstart is not null and dd_postingdate_prodend is not null then 
					case when dd_postingdate_prodstart <> '0001-01-01' and dd_postingdate_prodend <> '0001-01-01' then 
						dd_postingdate_prodend - dd_postingdate_prodstart end end,
	ct_qualitytime = case when dd_postingdate_qualitystart is not null and dd_postingdate_qualityend is not null then 
					case when dd_postingdate_qualitystart <> '0001-01-01' and dd_postingdate_qualityend <> '0001-01-01' then 
						dd_postingdate_qualityend - dd_postingdate_qualitystart end end,
	ct_cycle1time = case when dd_postingdate_cycle1start is not null and dd_postingdate_cycle1end is not null then 
					case when dd_postingdate_cycle1start <> '0001-01-01' and dd_postingdate_cycle1end <> '0001-01-01' then 
						dd_postingdate_cycle1end - dd_postingdate_cycle1start end end,
	ct_cycle2time = case when dd_postingdate_cycle2start is not null and dd_postingdate_cycle2end is not null then 
					case when dd_postingdate_cycle2start <> '0001-01-01' and dd_postingdate_cycle2end <> '0001-01-01' then 
						dd_postingdate_cycle2end - dd_postingdate_cycle2start end end,
	ct_cycle3time = case when dd_postingdate_cycle3start is not null and dd_postingdate_cycle3end is not null then 
					case when dd_postingdate_cycle3start <> '0001-01-01' and dd_postingdate_cycle3end <> '0001-01-01' then 
						dd_postingdate_cycle3end - dd_postingdate_cycle3start end end,
	ct_totalleadtime = case when dd_postingdate_tltstart is not null and dd_postingdate_tltend is not null then 
					case when dd_postingdate_tltstart <> '0001-01-01' and dd_postingdate_tltend <> '0001-01-01' then 
						dd_postingdate_tltend - dd_postingdate_tltstart end end,
	ct_transfercrscomtime = case when dd_postingdate_transccstart is not null and dd_postingdate_transccend is not null then 
					case when dd_postingdate_transccstart <> '0001-01-01' and dd_postingdate_transccend <> '0001-01-01' then 
						dd_postingdate_transccend - dd_postingdate_transccstart end end,
	ct_transintracomptime = case when dd_postingdate_transintracompstart is not null and dd_postingdate_transintracompend is not null then 
					case when dd_postingdate_transintracompstart <> '0001-01-01' and dd_postingdate_transintracompend <> '0001-01-01' then 
						dd_postingdate_transintracompend - dd_postingdate_transintracompstart end end,						
	ct_transfersubcontime = case when dd_postingdate_subcontractstart is not null and dd_postingdate_subcontractend is not null then 
					case when dd_postingdate_subcontractstart <> '0001-01-01' and dd_postingdate_subcontractend <> '0001-01-01' then 
						dd_postingdate_subcontractend - dd_postingdate_subcontractstart end end,
	ct_prodriskwaittime = case when dd_postingdate_prodriskwaitstart is not null and dd_postingdate_prodriskwaitend is not null then 
					case when dd_postingdate_prodriskwaitstart <> '0001-01-01' and dd_postingdate_prodriskwaitend <> '0001-01-01' then 
						dd_postingdate_prodriskwaitend - dd_postingdate_prodriskwaitstart end end,
	ct_waittime = case when dd_postingdate_waitstart is not null and dd_postingdate_waitend is not null then 
					case when dd_postingdate_waitstart <> '0001-01-01' and dd_postingdate_waitend <> '0001-01-01' then 
						dd_postingdate_waitend - dd_postingdate_waitstart end end,
	ct_shipwaittime = case when dd_postingdate_shipwaitstart is not null and dd_postingdate_shipwaitend is not null then 
					case when dd_postingdate_shipwaitstart <> '0001-01-01' and dd_postingdate_shipwaitend <> '0001-01-01' then 
						dd_postingdate_shipwaitend - dd_postingdate_shipwaitstart end end;




/* Populate Fact Table With the combined calculated data */
truncate table fact_scmcycletime;

insert into fact_scmcycletime
(		fact_scmcycletimeid,
		dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber, 
		dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dd_materialdocno,
		dim_unitofmeasureid, ct_quantity,
		dd_postingdate_prodstart, dd_postingdate_prodend,
		dd_postingdate_transccstart, dd_postingdate_transccend,
		dd_postingdate_transintracompstart, dd_postingdate_transintracompend,		
		dd_postingdate_subcontractstart, dd_postingdate_subcontractend,
		dd_postingdate_qualitystart, dd_postingdate_qualityend,
		dd_postingdate_waitstart, dd_postingdate_waitend,	
		dd_postingdate_tltstart, dd_postingdate_tltend,
		dd_postingdate_cycle1start, dd_postingdate_cycle1end,		
		dd_postingdate_cycle2start, dd_postingdate_cycle2end,
		dd_postingdate_cycle3start, dd_postingdate_cycle3end,	
		dd_postingdate_prodriskwaitstart, dd_postingdate_prodriskwaitend,	
		dd_postingdate_shipwaitstart, dd_postingdate_shipwaitend,					
		dd_qualityflag, dd_prodflag, dd_transfercrscomflag, dd_transfersubconflag, dd_waitflag, dd_totalleadflag, dd_cycle1flag, dd_cycle2flag, dd_cycle3flag, 
		dd_prodriskwaitflag, dd_shipwaitflag, dd_transintracompflag,
		dd_qualitynote, dd_prodnote, dd_transfercrscomnote, dd_transfersubconnote, dd_waitnote, dd_totalleadnote, dd_cycle1note, dd_cycle2note, dd_cycle3note, 
		dd_prodriskwaitnote, dd_shipwaitnote, dd_transintracompnote
)
select	fact_scmcycletimeid,
		dim_plantid, dim_partid, dd_partnumber, dd_plantcode, dd_batchnumber,
		dd_productionordernumber, dd_purchaseorder, dd_purchaseorderitem, dd_materialdocno,
		dim_unitofmeasureid, ct_quantity,
		dd_postingdate_prodstart, dd_postingdate_prodend,
		dd_postingdate_transccstart, dd_postingdate_transccend,
		dd_postingdate_transintracompstart, dd_postingdate_transintracompend,
		dd_postingdate_subcontractstart, dd_postingdate_subcontractend,
		dd_postingdate_qualitystart, dd_postingdate_qualityend,
		dd_postingdate_waitstart, dd_postingdate_waitend,	
		dd_postingdate_tltstart, dd_postingdate_tltend,
		dd_postingdate_cycle1start, dd_postingdate_cycle1end,		
		dd_postingdate_cycle2start, dd_postingdate_cycle2end,
		dd_postingdate_cycle3start, dd_postingdate_cycle3end,
		dd_postingdate_prodriskwaitstart, dd_postingdate_prodriskwaitend,	
		dd_postingdate_shipwaitstart, dd_postingdate_shipwaitend,			
		dd_qualityflag, dd_prodflag, dd_transfercrscomflag, dd_transfersubconflag, dd_waitflag, dd_totalleadflag, dd_cycle1flag, dd_cycle2flag, dd_cycle3flag, 
		dd_prodriskwaitflag, dd_shipwaitflag, dd_transintracompflag,
		dd_qualitynote, dd_prodnote, dd_transfercrscomnote, dd_transfersubconnote, dd_waitnote, dd_totalleadnote, dd_cycle1note, dd_cycle2note, dd_cycle3note, 
		dd_prodriskwaitnote, dd_shipwaitnote, dd_transintracompnote		
from tmp_fact_scmcycletime;





-- Duplicating the Update since for some reason it is not working on tmp table.
update fact_scmcycletime
set ct_prodtime = case when dd_postingdate_prodstart is not null and dd_postingdate_prodend is not null then 
					case when dd_postingdate_prodstart <> '0001-01-01' and dd_postingdate_prodend <> '0001-01-01' then 
						dd_postingdate_prodend - dd_postingdate_prodstart end end,
	ct_qualitytime = case when dd_postingdate_qualitystart is not null and dd_postingdate_qualityend is not null then 
					case when dd_postingdate_qualitystart <> '0001-01-01' and dd_postingdate_qualityend <> '0001-01-01' then 
						dd_postingdate_qualityend - dd_postingdate_qualitystart end end,
	ct_cycle1time = case when dd_postingdate_cycle1start is not null and dd_postingdate_cycle1end is not null then 
					case when dd_postingdate_cycle1start <> '0001-01-01' and dd_postingdate_cycle1end <> '0001-01-01' then 
						dd_postingdate_cycle1end - dd_postingdate_cycle1start end end,
	ct_cycle2time = case when dd_postingdate_cycle2start is not null and dd_postingdate_cycle2end is not null then 
					case when dd_postingdate_cycle2start <> '0001-01-01' and dd_postingdate_cycle2end <> '0001-01-01' then 
						dd_postingdate_cycle2end - dd_postingdate_cycle2start end end,
	ct_cycle3time = case when dd_postingdate_cycle3start is not null and dd_postingdate_cycle3end is not null then 
					case when dd_postingdate_cycle3start <> '0001-01-01' and dd_postingdate_cycle3end <> '0001-01-01' then 
						dd_postingdate_cycle3end - dd_postingdate_cycle3start end end,
	ct_totalleadtime = case when dd_postingdate_tltstart is not null and dd_postingdate_tltend is not null then 
					case when dd_postingdate_tltstart <> '0001-01-01' and dd_postingdate_tltend <> '0001-01-01' then 
						dd_postingdate_tltend - dd_postingdate_tltstart end end,
	ct_transfercrscomtime = case when dd_postingdate_transccstart is not null and dd_postingdate_transccend is not null then 
					case when dd_postingdate_transccstart <> '0001-01-01' and dd_postingdate_transccend <> '0001-01-01' then 
						dd_postingdate_transccend - dd_postingdate_transccstart end end,
	ct_transintracomptime = case when dd_postingdate_transintracompstart is not null and dd_postingdate_transintracompend is not null then 
					case when dd_postingdate_transintracompstart <> '0001-01-01' and dd_postingdate_transintracompend <> '0001-01-01' then 
						dd_postingdate_transintracompend - dd_postingdate_transintracompstart end end,						
	ct_transfersubcontime = case when dd_postingdate_subcontractstart is not null and dd_postingdate_subcontractend is not null then 
					case when dd_postingdate_subcontractstart <> '0001-01-01' and dd_postingdate_subcontractend <> '0001-01-01' then 
						dd_postingdate_subcontractend - dd_postingdate_subcontractstart end end,
	ct_prodriskwaittime = case when dd_postingdate_prodriskwaitstart is not null and dd_postingdate_prodriskwaitend is not null then 
					case when dd_postingdate_prodriskwaitstart <> '0001-01-01' and dd_postingdate_prodriskwaitend <> '0001-01-01' then 
						dd_postingdate_prodriskwaitend - dd_postingdate_prodriskwaitstart end end,
	ct_waittime = case when dd_postingdate_waitstart is not null and dd_postingdate_waitend is not null then 
					case when dd_postingdate_waitstart <> '0001-01-01' and dd_postingdate_waitend <> '0001-01-01' then 
						dd_postingdate_waitend - dd_postingdate_waitstart end end,
	ct_shipwaittime = case when dd_postingdate_shipwaitstart is not null and dd_postingdate_shipwaitend is not null then 
					case when dd_postingdate_shipwaitstart <> '0001-01-01' and dd_postingdate_shipwaitend <> '0001-01-01' then 
						dd_postingdate_shipwaitend - dd_postingdate_shipwaitstart end end;




/* Update Dim Date Id for Date Values */
update fact_scmcycletime f
set f.dim_dateidprodstart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_prodstart and 
f.dim_dateidprodstart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidprodend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_prodend and 
f.dim_dateidprodend <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidcctstart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_transccstart and 
f.dim_dateidcctstart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidcctend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_transccend and 
f.dim_dateidcctend <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidictstart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_transintracompstart and 
f.dim_dateidictstart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidictend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_transintracompend and 
f.dim_dateidictend <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidsubcontstart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_subcontractstart and 
f.dim_dateidsubcontstart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidsubcontend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_subcontractend and 
f.dim_dateidsubcontend <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidqualitystart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_qualitystart and 
f.dim_dateidqualitystart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidqualityend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_qualityend and 
f.dim_dateidqualityend <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidwaitstart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_waitstart and 
f.dim_dateidwaitstart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidwaitend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_waitend and 
f.dim_dateidwaitend <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidtltstart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_tltstart and 
f.dim_dateidtltstart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidtltend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_tltend and 
f.dim_dateidtltend <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidcycle1start = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_cycle1start and 
f.dim_dateidcycle1start <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidcycle1end = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_cycle1end and 
f.dim_dateidcycle1end <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidcycle2start = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_cycle2start and 
f.dim_dateidcycle2start <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidcycle2end = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_cycle2end and 
f.dim_dateidcycle2end <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidcycle3start = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_cycle3start and 
f.dim_dateidcycle3start <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidcycle3end = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_cycle3end and 
f.dim_dateidcycle3end <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidprodriskwaitstart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_prodriskwaitstart and 
f.dim_dateidprodriskwaitstart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidprodriskwaitend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_prodriskwaitend and 
f.dim_dateidprodriskwaitend <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidshipwaitstart = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_shipwaitstart and 
f.dim_dateidshipwaitstart <> d.dim_dateid;

update fact_scmcycletime f
set f.dim_dateidshipwaitend = d.dim_dateid 
from dim_date d, fact_scmcycletime f
where d.companycode = 'Not Set' and d.datevalue = f.dd_postingdate_shipwaitend and 
f.dim_dateidshipwaitend <> d.dim_dateid;


/* Update Dim Project Source ID */
-- 8	EMDTrio41B		Trio Instance		065C8F3C_A723_4E50_87B7_89F870542CC1
update fact_scmcycletime f
set dim_projectsourceid = 8;


/* drop temp tables */
drop table if exists TMP_FACTMM_SCMP_261;
drop table if exists TMP_FACTMM_SCMP_321;
drop table if exists TMP_FACTMM_SCMP_643645;
drop table if exists TMP_FACTMM_SCMP_543;
drop table if exists TMP_FACTMM_SCMP_641647;
drop table if exists TMP_FACTMM_SCMP_261_MIN;
drop table if exists TMP_FACTMM_SCMP_PRODSTARTEND;
drop table if exists TMP_FACTMM_SCMP_QUALITYSTART;
drop table if exists TMP_FACTMM_SCMP_QUALITYSTARTEND;
drop table if exists TMP_FACTMM_SCMP_CYCLE2STARTPPB261ALL_PRODORDER;
drop table if exists TMP_FACTMM_SCMP_CYCLE3STARTPPB261ALL_PRODORDER;
drop table if exists TMP_FACTMM_SCMP_TLTSTARTENDLEVEL0;
drop table if exists TMP_FACTMM_SCMP_TRANSCCSTARTEND;
drop table if exists TMP_FACTMM_SCMP_TRANSINTRACOMPSTARTEND;
drop table if exists TMP_FACTMM_SCMP_PARWTEND;
drop table if exists TMP_FACTMM_SCMP_PARWTSTARTEND;
drop table if exists TMP_FACTMM_SCMP_WAITSTART_321;
drop table if exists TMP_FACTMM_SCMP_WAITSTART_ALL_RANK_BETWEEN;
drop table if exists TMP_FACTMM_SCMP_WAITSTARTENDFINAL;
drop table if exists TMP_FACTMM_SCMP_101F;
drop table if exists TMP_FACTMM_SCMP_PRODSTARTEND_FACT_PRODORDER;
drop table if exists TMP_FACTMM_SCMP_101B;
drop table if exists TMP_FACTMM_SCMP_531;
drop table if exists TMP_FACTMM_SCMP_UD;
drop table if exists TMP_FACTMM_SCMP_QUALITYEND;
drop table if exists TMP_FACTMM_SCMP_CYCLE1TIMESTARTEND;
drop table if exists TMP_FACTMM_SCMP_CYCLE2STARTPPB;
drop table if exists TMP_FACTMM_SCMP_CYCLE2STARTPPBPROD;
drop table if exists TMP_FACTMM_SCMP_CYCLE2STARTPPB261ALL;
drop table if exists TMP_FACTMM_SCMP_CYCLE2STARTPPB261;
drop table if exists TMP_FACTMM_SCMP_CYCLE2TIMESTARTEND;
drop table if exists TMP_FACTMM_SCMP_CYCLE3STARTPPB;
drop table if exists TMP_FACTMM_SCMP_CYCLE3STARTPPBPROD;
drop table if exists TMP_FACTMM_SCMP_CYCLE3STARTPPB261ALL;
drop table if exists TMP_FACTMM_SCMP_CYCLE3STARTPPB261;
drop table if exists TMP_FACTMM_SCMP_CYCLE3TIMESTARTEND;
drop table if exists TMP_FACTMM_SCMP_TLTSTART_MIN;
drop table if exists TMP_FACTMM_SCMP_TLTSTARTENDFINAL;
drop table if exists TMP_FACTMM_SCMP_SUBCONTRACTSTARTEND;
drop table if exists TMP_FACTMM_SCMP_WAITSTARTALL_321;
drop table if exists TMP_FACTMM_SCMP_WAITSTARTALL_UD;
drop table if exists TMP_FACTMM_SCMP_WAITSTART_UD;
drop table if exists TMP_FACTMM_SCMP_WAITSTART_ALL;
drop table if exists TMP_FACTMM_SCMP_WAITSTART_ALL_RANK;
drop table if exists TMP_FACTMM_SCMP_WAITENDALL;
drop table if exists TMP_FACTMM_SCMP_SHIPWAITENDALL;
drop table if exists TMP_FACTMM_SCMP_SHIPWAITSTARTENDFINAL;
drop table if exists tmp_fact_scmcycletime;