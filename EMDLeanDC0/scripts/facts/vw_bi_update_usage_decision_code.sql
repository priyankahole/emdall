DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot;	 
CREATE TABLE tmp_qave_fact_inspectionlot AS 
SELECT distinct QAVE_PRUEFLOS,QAVE_VKATART,QAVE_VCODEGRP,QAVE_VERSIONCD,QAVE_VCODE,QAVE_VAEDATUM,QAVE_VNAME, MAX(ifnull(QAVE_ZAEHLER,1)) MAX_QAVE_ZAEHLER
FROM qave
where QAVE_VAEDATUM is not null
GROUP BY QAVE_PRUEFLOS,QAVE_VKATART,QAVE_VCODEGRP,QAVE_VERSIONCD,QAVE_VCODE,QAVE_VAEDATUM,QAVE_VNAME;
							

UPDATE fact_inspectionlot il
SET Dim_InspUsageDecisionId = Dim_CodeTextId,
    dd_UsageDecisionMadeBy = ifnull(QAVE_VNAME,'Not Set')
FROM
       dim_inspectionlotstatus ils,
       tmp_qave_fact_inspectionlot q,
       dim_CodeText t,
       dim_plant pl,
       fact_inspectionlot il
WHERE q.QAVE_PRUEFLOS = il.dd_inspectionlotno
       AND ils.dim_inspectionlotstatusid = il.dim_inspectionlotstatusid
       AND pl.dim_plantid = il.dim_plantid
       AND q.QAVE_VKATART = t."catalog"
       AND q.QAVE_VCODEGRP = t.CodeGroup
       AND q.QAVE_VERSIONCD = t.Version
       AND q.QAVE_VCODE = t.Code
       AND ils.UsageDecisionMade = 'X';

UPDATE fact_inspectionlot il
SET Dim_DateIdActualInspectionEnd = ifnull(dt.dim_dateid,1)
FROM dim_inspectionlotstatus ils,
     tmp_qave_fact_inspectionlot q,
     dim_CodeText t,
     dim_plant pl,
      dim_date dt,
     fact_inspectionlot il
 WHERE q.QAVE_PRUEFLOS = il.dd_inspectionlotno
       AND ils.dim_inspectionlotstatusid = il.dim_inspectionlotstatusid
       AND pl.dim_plantid = il.dim_plantid
       AND q.QAVE_VKATART = t."catalog"
       AND q.QAVE_VCODEGRP = t.CodeGroup
       AND q.QAVE_VERSIONCD = t.Version
       AND q.QAVE_VCODE = t.Code
       AND ils.UsageDecisionMade = 'X'
       AND dt.CompanyCode = pl.CompanyCode AND dt.DateValue = q.QAVE_VAEDATUM;



DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot;

DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot2;	 
CREATE TABLE tmp_qave_fact_inspectionlot2 AS 
SELECT QAVE_PRUEFLOS,QAVE_VDATUM,QAVE_VAENAME,ROW_NUMBER() over (PARTITION BY QAVE_PRUEFLOS ORDER BY QAVE_ZAEHLER DESC) AS RNK
FROM QAVE;

merge into fact_inspectionlot il
using (select fact_inspectionlotid,ifnull(dd.dim_dateid,1) dim_dateid,ifnull(q2.QAVE_VAENAME,'Not Set') as QAVE_VAENAME
   from fact_inspectionlot il
inner join (select * from tmp_qave_fact_inspectionlot2 where RNK=1) q2
on il.dd_inspectionlotno=q2.QAVE_PRUEFLOS
inner join dim_plant pl
on  il.dim_plantid=pl.dim_plantid
left join dim_date dd
on dd.datevalue=q2.QAVE_VDATUM AND dd.CompanyCode=pl.CompanyCode
	  ) src on il.fact_inspectionlotid = src.fact_inspectionlotid
when matched then update set il.dim_dateidUsageDecisionMade = src.dim_dateid, 
                             il.dd_UsageDecisionChangeby=src.QAVE_VAENAME;


DROP TABLE IF EXISTS tmp_qave_fact_inspectionlot2;	 