DROP TABLE IF EXISTS number_fountain_fact_pmorderip24;
CREATE TABLE number_fountain_fact_pmorderip24 LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_pmorderip24;
CREATE TABLE tmp_fact_pmorderip24 LIKE fact_pmorderip24 INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_pmorderip24 WHERE table_name = 'fact_pmorderip24';	

INSERT INTO number_fountain_fact_pmorderip24
SELECT 'fact_pmorderip24', ifnull(max(f.fact_pmorderip24id),
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_pmorderip24 AS f;

INSERT INTO tmp_fact_pmorderip24(
  fact_pmorderip24id,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_client, -- MHIS_MANDT
  dd_maintenanceplan,  -- MHIS_WARPL
  dd_maintenanceplancallnumber, -- MHIS_ABNUM
  dd_maintenancepackagenumber,  -- MHIS_ZAEHL
  dd_nextplanneddate, -- MHIS_NPLDA
  dim_nextplanneddateid, -- MHIS_NPLDA
  dd_functionallocation,  -- IFLOS_STRNO
  dd_maitenanceitem, -- MPOS_WAPOS
  dd_equipmentnumber, -- MPOS_EQUNR
  dd_itemshorttext, -- MPOS_PSTXT
  dd_iteminmaintenanceplan, -- MPOS_WPPOS
  dd_maintenancestrategy, -- MPOS_WSTRA
  dd_maintenanceactivitytype, -- MPOS_ILART
  dd_objectidoftheworkcenter, -- MPOS_GEWRK
  dd_maintenanceplanningplant, -- MPOS_IWERK
  dim_maintenanceplanningplantid, -- MPOS_IWERK
  dd_workcenterdescription,  -- CRHD_ARBPL
  dd_workcenterdescriptiongroupping,  -- csv_mainworkcentergrouping, groupe
  ct_linecount
)
SELECT
  (SELECT max_id 
    FROM number_fountain_fact_pmorderip24 
    WHERE table_name = 'fact_pmorderip24') + ROW_NUMBER() OVER(ORDER BY '') AS fact_pmorderip24id,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS dw_insert_date, 
  current_timestamp AS dw_update_date,
  IFNULL(mh.mhis_mandt,'Not Set') AS dd_client, -- MHIS_MANDT
  IFNULL(mh.mhis_warpl,'Not Set') AS dd_maintenanceplan,  -- MHIS_WARPL
  IFNULL(mh.mhis_abnum,'Not Set') AS dd_maintenanceplancallnumber, -- MHIS_ABNUM
  IFNULL(mh.mhis_zaehl,'Not Set') AS dd_maintenancepackagenumber,  -- MHIS_ZAEHL
  IFNULL(mh.mhis_nplda,'0001-01-01') AS dd_nextplanneddate, -- MHIS_NPLDA
  CAST(1 AS BIGINT) AS dim_nextplanneddateid, -- MHIS_NPLDA
  'Not Set' AS dd_functionallocation,  -- IFLOS_STRNO
  IFNULL(mp.mpos_wapos,'Not Set') AS dd_maitenanceitem, -- MPOS_WAPOS
  IFNULL(mp.mpos_equnr,'Not Set') AS dd_equipmentnumber, -- MPOS_EQUNR
  IFNULL(mp.mpos_pstxt,'Not Set') AS dd_itemshorttext, -- MPOS_PSTXT
  IFNULL(mp.mpos_wppos,'Not Set') AS dd_iteminmaintenanceplan, -- MPOS_WPPOS
  IFNULL(mp.mpos_wstra,'Not Set') AS dd_maintenancestrategy, -- MPOS_WSTRA
  IFNULL(mp.mpos_ilart,'Not Set') AS dd_maintenanceactivitytype, -- MPOS_ILART
  IFNULL(mp.mpos_gewrk,'Not Set') AS dd_objectidoftheworkcenter, -- MPOS_GEWRK
  IFNULL(mp.mpos_iwerk,'Not Set') AS dd_maintenanceplanningplant, -- MPOS_IWERK
  CAST(1 AS BIGINT) AS dim_maintenanceplanningplantid, -- MPOS_IWERK
  'Not Set' AS dd_workcenterdescription,  -- CRHD_ARBPL
  'Not Set' AS dd_workcenterdescriptiongroupping,  -- csv_mainworkcentergrouping, groupe
  1 AS ct_linecount
FROM
  PMA_MPOS AS mp,
  PMA_MHIS AS mh
WHERE
  mp.mpos_warpl = mh.mhis_warpl
;



/* BEGIN - Next Planned Date Update */
UPDATE tmp_fact_pmorderip24 AS f
SET f.dim_nextplanneddateid = IFNULL(dt.dim_dateid, 1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorderip24 AS f	
WHERE dt.datevalue = f.dd_nextplanneddate
  AND dt.companycode = 'Not Set'
  AND f.dim_nextplanneddateid <> dt.dim_dateid;
/* END - Next Planned Date Update */

/* BEGIN - Maintenance Planning Plant Update */
UPDATE tmp_fact_pmorderip24 AS f
SET f.dim_maintenanceplanningplantid = IFNULL(dp.dim_plantid, 1),
  f.dw_update_date = current_timestamp
FROM dim_plant AS dp, tmp_fact_pmorderip24 AS f	
WHERE dp.plantcode = f.dd_maintenanceplanningplant
  AND f.dim_maintenanceplanningplantid <> dp.dim_plantid;
/* END - Maintenance Planning Plant Update */

/* BEGIN - Functional Location Update */
UPDATE tmp_fact_pmorderip24 AS f
SET f.dd_functionallocation = IFNULL(pm.iflos_strno,'Not Set'),
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorderip24 AS f, pma_mpos_iloa_iflos AS pm
WHERE pm.iflos_actvs = 'X'
  AND pm.mpos_wapos = f.dd_maitenanceitem;
/* END - Functional Location Update */

/* BEGIN - Work Center Description */
UPDATE tmp_fact_pmorderip24 AS f
SET f.dd_workcenterdescription = IFNULL(pc.crhd_arbpl,'Not Set'),
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorderip24 AS f, pma_crhd AS pc
WHERE f.dd_objectidoftheworkcenter = pc.crhd_objid
  AND UPPER(TRIM(pc.crhd_objty)) = 'A';
/* END - Work Center Description */

/* BEGIN - dd_workcenterdescriptiongroupping Update */
UPDATE tmp_fact_pmorderip24 AS f
SET f.dd_workcenterdescriptiongroupping = IFNULL(cm.groupe,'Not Set'),
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorderip24 AS f, csv_mainworkcentergrouping AS cm
WHERE f.dd_workcenterdescription = cm.workcenter;
/* END - dd_workcenterdescriptiongroupping Update */

DELETE FROM fact_pmorderip24;

INSERT INTO fact_pmorderip24(
  fact_pmorderip24id,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_client, -- MHIS_MANDT
  dd_maintenanceplan,  -- MHIS_WARPL
  dd_maintenanceplancallnumber, -- MHIS_ABNUM
  dd_maintenancepackagenumber,  -- MHIS_ZAEHL
  dd_nextplanneddate, -- MHIS_NPLDA
  dim_nextplanneddateid, -- MHIS_NPLDA
  dd_functionallocation,  -- IFLOS_STRNO
  dd_maitenanceitem, -- MPOS_WAPOS
  dd_equipmentnumber, -- MPOS_EQUNR
  dd_itemshorttext, -- MPOS_PSTXT
  dd_iteminmaintenanceplan, -- MPOS_WPPOS
  dd_maintenancestrategy, -- MPOS_WSTRA
  dd_maintenanceactivitytype, -- MPOS_ILART
  dd_objectidoftheworkcenter, -- MPOS_GEWRK
  dd_maintenanceplanningplant, -- MPOS_IWERK
  dim_maintenanceplanningplantid, -- MPOS_IWERK
  dd_workcenterdescription,  -- CRHD_ARBPL
  dd_workcenterdescriptiongroupping,  -- csv_mainworkcentergrouping, groupe
  ct_linecount
)
SELECT
  fact_pmorderip24id,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_client, -- MHIS_MANDT
  dd_maintenanceplan,  -- MHIS_WARPL
  dd_maintenanceplancallnumber, -- MHIS_ABNUM
  dd_maintenancepackagenumber,  -- MHIS_ZAEHL
  dd_nextplanneddate, -- MHIS_NPLDA
  dim_nextplanneddateid, -- MHIS_NPLDA
  dd_functionallocation,  -- IFLOS_STRNO
  dd_maitenanceitem, -- MPOS_WAPOS
  dd_equipmentnumber, -- MPOS_EQUNR
  dd_itemshorttext, -- MPOS_PSTXT
  dd_iteminmaintenanceplan, -- MPOS_WPPOS
  dd_maintenancestrategy, -- MPOS_WSTRA
  dd_maintenanceactivitytype, -- MPOS_ILART
  dd_objectidoftheworkcenter, -- MPOS_GEWRK
  dd_maintenanceplanningplant, -- MPOS_IWERK
  dim_maintenanceplanningplantid, -- MPOS_IWERK
  dd_workcenterdescription,  -- CRHD_ARBPL
  dd_workcenterdescriptiongroupping,  -- csv_mainworkcentergrouping, groupe
  ct_linecount
FROM tmp_fact_pmorderip24;

DROP TABLE IF EXISTS tmp_fact_pmorderip24;