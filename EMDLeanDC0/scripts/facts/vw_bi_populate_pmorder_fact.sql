DROP TABLE IF EXISTS number_fountain_fact_pmorder;
CREATE TABLE number_fountain_fact_pmorder LIKE number_fountain INCLUDING DEFAULTS INCLUDING IDENTITY;

DROP TABLE IF EXISTS tmp_fact_pmorder;
CREATE TABLE tmp_fact_pmorder LIKE fact_pmorder INCLUDING DEFAULTS INCLUDING IDENTITY;

DELETE FROM number_fountain_fact_pmorder WHERE table_name = 'fact_pmorder';	

INSERT INTO number_fountain_fact_pmorder
SELECT 'fact_pmorder', ifnull(max(f.fact_pmorderid),
  ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM tmp_fact_pmorder AS f;

INSERT INTO tmp_fact_pmorder(
  fact_pmorderid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_ordernumber, -- AUFK_AUFNR
  dd_ordertype, -- AUFK_AUART
  dd_plant,  -- AUFK_WERKS
  dd_location, -- AUFK_STORT
  dd_locationplant,  -- AUFK_SOWRK
  dd_orderstatus, -- AUFK_ASTNR
  dd_objectid,  -- AUFK_OBJID
  dd_startdate, -- AUFK_SDATE
  dim_dateidstartdate, 
  dd_objectnumber, -- AUFK_OBJNR
  dd_mainworkcenter, -- AUFK_VAPLZ
  dd_plantmainworkcenter, -- AUFK_WAWRK
  dd_basicfinishdate, -- AFKO_GLTRP
  dim_dateidbasicfinishdate,
  dd_basicstartdate, -- AFKO_GSTRP
  dim_dateidbasicstartdate,
  dd_actualstartdate, -- AFKO_GSTRI
  dim_dateidactualstartdate,
  dd_actualfinishdate, -- AFKO_GLTRI
  dim_dateidactualfinishdate,
  dd_generalcounter, -- AFKO_APLZT
  dd_routingnumberoperations, -- AFKO_AUFPT
  dd_responsibleplannergroup,  -- AFKO_PLGRP
  ct_actualwork, -- AFVV_ISMNW
  dd_actualstartexecdate, -- AFVV_ISDD
  dim_dateidactualstartexecdate,
  dd_actualfinishexecdate, -- AFVV_IEDD
  dim_dateidactualfinishexecdate,
  dd_actualstartexectime, -- AFVV_ISDZ
  dd_actualstartexecdateandtime,
  dd_actualfinishexectime, -- AFVV_IEDZ
  dd_actualfinishexecdateandtime,
  dd_operationnumber, -- AFVC_VORNR
  dd_operationshorttext, -- AFVC_LTXA1
  dd_activitytype, -- AFVC_LARNT
  dd_completionconfirmationnumber, -- AFVC_RUECK
  dd_confirmationcounter, -- AFVC_RMZHL
  dd_objectnumberoperation, -- AFVC_OBJNR
  dim_statusobjectinformationid, -- jsto
  dim_maintenanceorderheaderid, -- afih
  dim_plantid, -- plant
  dd_userstatusgmp, -- TJ30T_TXT04
  dd_userstatusorel, -- TJ30T_TXT04
  dd_userstatuscest, -- TJ30T_TXT04
  dd_userstatusocrt, -- TJ30T_TXT04
  dd_systemstatuscnf, -- TJ02T_TXT04
  dd_systemstatuspcnf, -- TJ02T_TXT04
  dd_statuswithstatusnumber, -- TJ30_STONR
  dd_breakdownindicator, -- QMIH_MSAUS
  ct_breakdownduration, -- QMIH_AUSZT
  dd_unitsforbreakdownduration, -- QMIH-MAUEH
  dd_systemstatusreleased,  -- TJ02T_TXT04
  dim_dateidtolerance1month,
  dim_dateidtolerance3months,
  dim_dateidtolerance,
  dd_systemstatuscreated, -- TJ02T_TXT04
  dd_systemstatustechnicallycompleted, -- TJ02T_TXT04
  dd_systemstatusclosed, -- TJ02T_TXT04
  dd_unitofwork, -- AFVV_ARBEH
  dd_wolate1monthflag,
  dd_wolate3monthsflag,
  dd_wolate6monthsflag,
  dd_wolateallflag,
  dd_wolatecriticaleqflag,
  dd_wolatecalibrationflag,
  dd_notificationtypecode,   -- QMEL_QMART
  dd_malfunctionenddate, -- QMIH_AUSBS
  dim_malfunctionenddateid
)
SELECT
  (SELECT max_id 
    FROM number_fountain_fact_pmorder 
    WHERE table_name = 'fact_pmorder') + ROW_NUMBER() OVER(ORDER BY '') AS fact_pmorderid,
  IFNULL((SELECT s.dim_projectsourceid FROM dim_projectsource s),1) AS dim_projectsourceid,
  1 AS amt_exchangerate,
  1 AS amt_exchangerate_gbl,
  1 AS dim_currencyid,
  1 AS dim_currencyid_tra,
  1 AS dim_currencyid_gbl,
  current_timestamp AS dw_insert_date, 
  current_timestamp AS dw_update_date,
  IFNULL(av.aufk_aufnr,'Not Set') AS dd_ordernumber, -- AUFK_AUFNR
  IFNULL(av.aufk_auart,'Not Set') AS dd_ordertype, -- AUFK_AUART
  IFNULL(av.aufk_werks,'Not Set') AS dd_plant,  -- AUFK_WERKS
  IFNULL(av.aufk_stort,'Not Set') AS dd_location, -- AUFK_STORT
  IFNULL(av.aufk_sowrk,'Not Set') AS dd_locationplant,  -- AUFK_SOWRK
  IFNULL(av.aufk_astnr,0) AS dd_orderstatus, -- AUFK_ASTNR
  IFNULL(av.aufk_objid,'Not Set') AS dd_objectid,  -- AUFK_OBJID
  IFNULL(av.aufk_sdate,'0001-01-01') AS dd_startdate, -- AUFK_SDATE
  1 AS dim_dateidstartdate, 
  IFNULL(av.aufk_objnr,'Not Set') AS dd_objectnumber, -- AUFK_OBJNR
  IFNULL(av.aufk_vaplz,'Not Set') AS dd_mainworkcenter, -- AUFK_VAPLZ
  IFNULL(av.aufk_wawrk,'Not Set') AS dd_plantmainworkcenter, -- AUFK_WAWRK
  IFNULL(av.afko_gltrp,'0001-01-01') AS dd_basicfinishdate, -- AFKO_GLTRP
  1 AS dim_dateidbasicfinishdate,
  IFNULL(av.afko_gstrp,'0001-01-01') AS dd_basicstartdate, -- AFKO_GSTRP
  1 AS dim_dateidbasicstartdate,
  IFNULL(av.afko_gstri,'0001-01-01') AS dd_actualstartdate, -- AFKO_GSTRI
  1 AS dim_dateidactualstartdate,
  IFNULL(av.afko_gltri,'0001-01-01') AS dd_actualfinishdate, -- AFKO_GLTRI
  1 AS dim_dateidactualfinishdate,
  IFNULL(av.afko_aplzt,0) AS dd_generalcounter, -- AFKO_APLZT
  IFNULL(av.afko_aufpt,0) AS dd_routingnumberoperations, -- AFKO_AUFPT
  IFNULL(av.afko_plgrp,'Not Set') AS dd_responsibleplannergroup,  -- AFKO_PLGRP
  IFNULL(av.afvv_ismnw,0) AS ct_actualwork, -- AFVV_ISMNW
  IFNULL(av.afvv_isdd,'0001-01-01') AS dd_actualstartexecdate, -- AFVV_ISDD
  1 AS dim_dateidactualstartexecdate,
  IFNULL(av.afvv_iedd,'0001-01-01') AS dd_actualfinishexecdate, -- AFVV_IEDD
  1 AS dim_dateidactualfinishexecdate,
  IFNULL(av.afvv_isdz,'Not Set') AS dd_actualstartexectime, -- AFVV_ISDZ
  '0001-01-01 00:00:00' AS dd_actualstartexecdateandtime,
  IFNULL(av.afvv_iedz,'Not Set') AS dd_actualfinishexectime, -- AFVV_IEDZ
  '0001-01-01 00:00:00' AS dd_actualfinishexecdateandtime,
  IFNULL(ac.afvc_vornr,'Not Set') AS dd_operationnumber, -- AFVC_VORNR
  IFNULL(ac.afvc_ltxa1,'Not Set') AS dd_operationshorttext, -- AFVC_LTXA1
  IFNULL(ac.afvc_larnt,'Not Set') AS dd_activitytype, -- AFVC_LARNT
  IFNULL(ac.afvc_rueck,0) AS dd_completionconfirmationnumber, -- AFVC_RUECK
  IFNULL(ac.afvc_rmzhl,0) AS dd_confirmationcounter, -- AFVC_RMZHL
  IFNULL(ac.afvc_objnr,'Not Set') AS dd_objectnumberoperation, -- AFVC_OBJNR
  1 AS dim_statusobjectinformationid, -- jsto
  1 AS dim_maintenanceorderheaderid, -- afih
  1 AS dim_plantid, -- plant
  'Not Set' AS dd_userstatusgmp, -- TJ30T_TXT04
  'Not Set' AS dd_userstatusorel, -- TJ30T_TXT04
  'Not Set' AS dd_userstatuscest, -- TJ30T_TXT04
  'Not Set' AS dd_userstatusocrt, -- TJ30T_TXT04
  'Not Set' AS dd_systemstatuscnf, -- TJ02T_TXT04
  'Not Set' AS dd_systemstatuspcnf, -- TJ02T_TXT04
  0 AS dd_statuswithstatusnumber, -- TJ30_STONR
  'Not Set' AS dd_breakdownindicator, -- QMIH_MSAUS
  0 AS ct_breakdownduration, -- QMIH_AUSZT
  'Not Set' AS dd_unitsforbreakdownduration, -- QMIH-MAUEH
  'Not Set' AS dd_systemstatusreleased,
  1 AS dim_dateidtolerance1month,
  1 AS dim_dateidtolerance3months,
  1 AS dim_dateidtolerance,
  'Not Set' AS dd_systemstatuscreated, -- TJ02T_TXT04
  'Not Set' AS dd_systemstatustechnicallycompleted, -- TJ02T_TXT04
  'Not Set' AS dd_systemstatusclosed, -- TJ02T_TXT04
  IFNULL(av.afvv_arbeh,'Not Set') AS dd_unitofwork, -- AFVV_ARBEH
  'Not Set' AS dd_wolate1monthflag,
  'Not Set' AS dd_wolate3monthsflag,
  'Not Set' AS dd_wolate6monthsflag,
  'Not Set' AS dd_wolateallflag,
  'Not Set' AS dd_wolatecriticaleqflag,
  'Not Set' AS dd_wolatecalibrationflag,
  'Not Set' AS dd_notificationtypecode,   -- QMEL_QMART
  '0001-01-01' AS dd_malfunctionenddate, -- QMIH_AUSBS
  1 AS dim_malfunctionenddateid
FROM 
  pma_aufk_afko_afvv AS av, 
  pma_aufk_afko_afvc AS ac
WHERE av.afvv_aufpl = ac.afvc_aufpl     -- join between pma_aufk_afko_afvv and pma_aufk_afko_afvc
    AND av.afvv_aplzl = ac.afvc_aplzl   -- join between pma_aufk_afko_afvv and pma_aufk_afko_afvc
;

/* BEGIN - Maintenance Order Header dimension link*/
UPDATE tmp_fact_pmorder AS f
SET f.dim_maintenanceorderheaderid = IFNULL(dm.dim_maintenanceorderheaderid,1),
  f.dw_update_date = current_timestamp
FROM dim_maintenanceorderheader AS dm, tmp_fact_pmorder AS f	
WHERE f.dd_ordernumber = dm.ordernumber
  AND f.dim_maintenanceorderheaderid <> dm.dim_maintenanceorderheaderid;
/* END - Maintenance Order Header dimension link*/
  
/* BEGIN - Status Object Information dimension link*/
UPDATE tmp_fact_pmorder AS f
SET f.dim_statusobjectinformationid = IFNULL(ds.dim_statusobjectinformationid,1),
  f.dw_update_date = current_timestamp
FROM dim_statusobjectinformation AS ds, tmp_fact_pmorder AS f	
WHERE f.dd_objectnumber = ds.objectnumber
  AND f.dim_statusobjectinformationid <> ds.dim_statusobjectinformationid;
/* END - Status Object Information dimension link*/
  
/* BEGIN - Start Date Update */
UPDATE tmp_fact_pmorder AS f
SET f.dim_dateidstartdate = IFNULL(dt.dim_dateid,1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorder AS f	
WHERE dt.datevalue = f.dd_startdate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidstartdate <> dt.dim_dateid;
/* END - Start Date Update */

/* BEGIN - Basic Finish Date Update*/  
UPDATE tmp_fact_pmorder AS f
SET f.dim_dateidbasicfinishdate = IFNULL(dt.dim_dateid,1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorder AS f	
WHERE dt.datevalue = f.dd_basicfinishdate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidbasicfinishdate <> dt.dim_dateid;
/* END - Basic Finish Date Update*/  
  
/* BEGIN - Basic Start Date Update*/  
UPDATE tmp_fact_pmorder AS f
SET f.dim_dateidbasicstartdate = IFNULL(dt.dim_dateid,1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorder AS f	
WHERE dt.datevalue = f.dd_basicstartdate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidbasicstartdate <> dt.dim_dateid;
/* END - Basic Start Date Update*/  
  
/* BEGIN - Actual Start Date Update*/  
UPDATE tmp_fact_pmorder AS f
SET f.dim_dateidactualstartdate = IFNULL(dt.dim_dateid,1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorder AS f	
WHERE dt.datevalue = f.dd_actualstartdate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidactualstartdate <> dt.dim_dateid;
/* END - Actual Start Date Update*/  

/* BEGIN - Actual Finish Date Update*/  
UPDATE tmp_fact_pmorder AS f
SET f.dim_dateidactualfinishdate = IFNULL(dt.dim_dateid,1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorder AS f	
WHERE dt.datevalue = f.dd_actualfinishdate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidactualfinishdate <> dt.dim_dateid;
/* END - Actual Finish Date Update*/
  
/* BEGIN - Actual Start Exec Date Update*/  
UPDATE tmp_fact_pmorder AS f
SET f.dim_dateidactualstartexecdate = IFNULL(dt.dim_dateid,1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorder AS f	
WHERE dt.datevalue = f.dd_actualstartexecdate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidactualstartexecdate <> dt.dim_dateid;
/* END - Actual Start Exec Date Update*/  
  
/* BEGIN - Actual Finish Exec Date Update*/  
UPDATE tmp_fact_pmorder AS f
SET f.dim_dateidactualfinishexecdate = IFNULL(dt.dim_dateid,1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorder AS f	
WHERE dt.datevalue = f.dd_actualfinishexecdate
  AND dt.companycode = 'Not Set'
  AND f.dim_dateidactualfinishexecdate <> dt.dim_dateid;
/* END - Actual Finish Exec Date Update*/  
  
/* BEGIN - Plant Update */
UPDATE tmp_fact_pmorder AS f
SET f.dim_plantid = IFNULL(dp.dim_plantid,1),
  f.dw_update_date = current_timestamp
FROM dim_plant AS dp, tmp_fact_pmorder AS f	
WHERE dp.plantcode = f.dd_plant
  AND f.dim_plantid <> dp.dim_plantid;
/* END - Plant Update */


/* BEGIN - User Status Updates */

UPDATE tmp_fact_pmorder AS f
SET f.dd_userstatusgmp = 'GMP',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_tj30t AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj30t_estat
  AND UPPER(t.tj30t_estat) = 'E0001'
  AND UPPER(t.tj30t_stsma) = 'YWORDER'
  AND f.dd_userstatusgmp <> 'GMP';
  
UPDATE tmp_fact_pmorder AS f
SET f.dd_userstatusorel = 'OREL',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_tj30t AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj30t_estat
  AND UPPER(t.tj30t_estat) = 'E0004'
  AND UPPER(t.tj30t_stsma) = 'YWORDER'
  AND f.dd_userstatusorel <> 'OREL';
  
UPDATE tmp_fact_pmorder AS f
SET f.dd_userstatuscest = 'CEST',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_tj30t AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj30t_estat
  AND UPPER(t.tj30t_estat) = 'E0003'
  AND UPPER(t.tj30t_stsma) = 'YWORDER'
  AND f.dd_userstatuscest <> 'CEST';
  
UPDATE tmp_fact_pmorder AS f
SET f.dd_userstatusocrt = 'OCRT',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_tj30t AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj30t_estat
  AND UPPER(t.tj30t_estat) = 'E0002'
  AND UPPER(t.tj30t_stsma) = 'YWORDER'
  AND f.dd_userstatusocrt <> 'OCRT';

/* END - User Status Updates */
  
/* BEGIN - System Status Updates */
UPDATE tmp_fact_pmorder AS f
SET f.dd_systemstatuscnf = 'CNF',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_TJ02T AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj02t_istat
  AND UPPER(t.tj02t_istat) = 'I0009'
  AND f.dd_systemstatuscnf <> 'CNF';
  
UPDATE tmp_fact_pmorder AS f
SET f.dd_systemstatuspcnf = 'PCNF',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_TJ02T AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj02t_istat
  AND UPPER(t.tj02t_istat) = 'I0010'
  AND f.dd_systemstatuspcnf <> 'PCNF';

UPDATE tmp_fact_pmorder AS f
SET f.dd_systemstatusreleased = 'REL',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_TJ02T AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj02t_istat
  AND UPPER(t.tj02t_istat) = 'I0002'
  AND f.dd_systemstatusreleased <> 'REL';

UPDATE tmp_fact_pmorder AS f
SET f.dd_systemstatuscreated = 'CRTD',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_TJ02T AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj02t_istat
  AND UPPER(t.tj02t_istat) = 'I0001'
  AND f.dd_systemstatuscreated <> 'CRTD';

UPDATE tmp_fact_pmorder AS f
SET f.dd_systemstatustechnicallycompleted = 'TECO',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_TJ02T AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj02t_istat
  AND UPPER(t.tj02t_istat) = 'I0045'
  AND f.dd_systemstatustechnicallycompleted <> 'TECO';

UPDATE tmp_fact_pmorder AS f
SET f.dd_systemstatusclosed = 'CLSD',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_TJ02T AS t
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = t.tj02t_istat
  AND UPPER(t.tj02t_istat) = 'I0046'
  AND f.dd_systemstatusclosed <> 'CLSD';

/* END - System Status Updates */


/* BEGIN -  Tolerance Update */

-- in case of ambiguous years like 9999, 
-- if I add one month it will give an error if the month is December
UPDATE tmp_fact_pmorder AS f 
SET f.dim_dateidtolerance1month = dd2.dim_dateid
FROM tmp_fact_pmorder AS f, dim_date AS dd1, dim_date AS dd2
WHERE f.dim_dateidbasicfinishdate = dd1.dim_dateid
  AND TO_DATE(dd1.monthenddate,'YYYY-MM-DD') + INTERVAL '1' MONTH = dd2.datevalue
  AND dd1.companycode = dd2.companycode
  AND EXTRACT(YEAR FROM dd1.datevalue) < 9998  
  AND EXTRACT(YEAR FROM dd2.datevalue) < 9998
  AND UPPER(f.dd_ordertype) IN ('YWMO','YWMU');  

-- in case of ambiguous years like 9999, 
-- if I add three months it will give an error if the month is October,
-- November or December
UPDATE tmp_fact_pmorder AS f 
SET f.dim_dateidtolerance3months = dd2.dim_dateid
FROM tmp_fact_pmorder AS f, dim_date AS dd1, dim_date AS dd2
WHERE f.dim_dateidbasicfinishdate = dd1.dim_dateid
  AND TO_DATE(dd1.monthenddate,'YYYY-MM-DD') + INTERVAL '3' MONTH = dd2.datevalue
  AND dd1.companycode = dd2.companycode
  AND EXTRACT(YEAR FROM dd1.datevalue) < 9998  
  AND EXTRACT(YEAR FROM dd2.datevalue) < 9998
  AND UPPER(f.dd_ordertype) IN ('YWMP');

UPDATE tmp_fact_pmorder AS f 
SET f.dim_dateidtolerance = 
  CASE 
    WHEN UPPER(f.dd_ordertype) IN ('YWMO','YWMU') THEN dim_dateidtolerance1month
    WHEN UPPER(f.dd_ordertype) IN ('YWMP') THEN dim_dateidtolerance3months
    ELSE 1
  END
;

/* END -  Tolerance Update */


/* BEGIN - Work Order Late Update */

-- we need this information at order level, hence we take the order with 
-- the minim row number and update it with the information needed. In this
-- way the information appears only once at order level.
DROP TABLE IF EXISTS tmp_min_operation_per_order;
CREATE TABLE tmp_min_operation_per_order
AS
SELECT
  fact_pmorderid,dd_ordernumber,
  ROW_NUMBER() OVER (PARTITION BY dd_ordernumber ORDER BY fact_pmorderid) AS operation_number
FROM TMP_FACT_PMORDER;

-- this is a workaround as an error appears in prod if I take all dim_date table
-- and apply the filters in the bigger selects. So I take only the like that is
-- of interest to me
DROP TABLE IF EXISTS tmp_onlytodayfromdimdate;
CREATE TABLE tmp_onlytodayfromdimdate 
AS
SELECT * 
FROM DIM_DATE
WHERE companycode = 'Not Set' 
  AND datevalue = CURRENT_DATE;

UPDATE tmp_fact_pmorder AS f 
SET f.dd_wolate1monthflag = 'Yes'
FROM 
  tmp_fact_pmorder AS f, 
  tmp_onlytodayfromdimdate AS dd1, 
  dim_date AS dd2, 
  tmp_min_operation_per_order AS t
WHERE f.dd_systemstatuscnf = 'Not Set' 
  AND f.dd_systemstatuspcnf = 'Not Set'
  AND UPPER(f.dd_ordertype) in ('YWMO','YWMP')
  AND (f.dd_systemstatuscreated <> 'Not Set' OR f.dd_systemstatusreleased <> 'Not Set')
  AND dd1.datevalue = current_date
  AND dd1.companycode = 'Not Set'
  AND f.dd_basicfinishdate <= TO_DATE(dd1.monthenddate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.dim_dateidtolerance <> 1
  AND f.dim_dateidtolerance = dd2.dim_dateid
  AND dd2.datevalue < TO_DATE(dd1.monthstartdate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.fact_pmorderid = t.fact_pmorderid
  AND f.dd_ordernumber = t.dd_ordernumber
  AND t.operation_number = 1
;

UPDATE tmp_fact_pmorder AS f 
SET f.dd_wolate3monthsflag = 'Yes'
FROM 
  tmp_fact_pmorder AS f, 
  tmp_onlytodayfromdimdate AS dd1, 
  dim_date AS dd2, 
  tmp_min_operation_per_order AS t
WHERE f.dd_systemstatuscnf = 'Not Set' 
  AND f.dd_systemstatuspcnf = 'Not Set'
  AND UPPER(f.dd_ordertype) in ('YWMO','YWMP')
  AND (f.dd_systemstatuscreated <> 'Not Set' OR f.dd_systemstatusreleased <> 'Not Set')
  AND dd1.datevalue = current_date
  AND dd1.companycode = 'Not Set'
  AND f.dd_basicfinishdate <= TO_DATE(dd1.monthenddate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.dim_dateidtolerance <> 1
  AND f.dim_dateidtolerance = dd2.dim_dateid
  AND dd2.datevalue < TO_DATE(dd1.monthstartdate,'YYYY-MM-DD') - INTERVAL '3' MONTH
  AND f.fact_pmorderid = t.fact_pmorderid
  AND f.dd_ordernumber = t.dd_ordernumber
  AND t.operation_number = 1
;

UPDATE tmp_fact_pmorder AS f 
SET f.dd_wolate6monthsflag = 'Yes'
FROM 
  tmp_fact_pmorder AS f, 
  tmp_onlytodayfromdimdate AS dd1, 
  dim_date AS dd2, 
  tmp_min_operation_per_order AS t
WHERE f.dd_systemstatuscnf = 'Not Set' 
  AND f.dd_systemstatuspcnf = 'Not Set'
  AND UPPER(f.dd_ordertype) in ('YWMO','YWMP')
  AND (f.dd_systemstatuscreated <> 'Not Set' OR f.dd_systemstatusreleased <> 'Not Set')
  AND dd1.datevalue = current_date
  AND dd1.companycode = 'Not Set'
  AND f.dd_basicfinishdate <= TO_DATE(dd1.monthenddate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.dim_dateidtolerance <> 1
  AND f.dim_dateidtolerance = dd2.dim_dateid
  AND dd2.datevalue < TO_DATE(dd1.monthstartdate,'YYYY-MM-DD') - INTERVAL '6' MONTH
  AND f.fact_pmorderid = t.fact_pmorderid
  AND f.dd_ordernumber = t.dd_ordernumber
  AND t.operation_number = 1
;

UPDATE tmp_fact_pmorder AS f 
SET f.dd_wolateallflag = 'Yes'
FROM 
  tmp_fact_pmorder AS f, 
  tmp_onlytodayfromdimdate AS dd1, 
  dim_date AS dd2, 
  tmp_min_operation_per_order AS t
WHERE f.dd_systemstatuscnf = 'Not Set' 
  AND f.dd_systemstatuspcnf = 'Not Set'
  AND UPPER(f.dd_ordertype) in ('YWMP')
  AND (f.dd_systemstatuscreated <> 'Not Set' OR f.dd_systemstatusreleased <> 'Not Set')
  AND dd1.datevalue = current_date
  AND dd1.companycode = 'Not Set'
  AND f.dd_basicfinishdate <= TO_DATE(dd1.monthenddate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.dim_dateidtolerance <> 1
  AND f.dim_dateidtolerance = dd2.dim_dateid
  AND dd2.datevalue < TO_DATE(dd1.monthstartdate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.fact_pmorderid = t.fact_pmorderid
  AND f.dd_ordernumber = t.dd_ordernumber
  AND t.operation_number = 1
;

UPDATE tmp_fact_pmorder AS f 
SET f.dd_wolatecriticaleqflag = 'Yes'
FROM 
  tmp_fact_pmorder AS f, 
  tmp_onlytodayfromdimdate AS dd1, 
  dim_date AS dd2, 
  tmp_min_operation_per_order AS t
WHERE f.dd_systemstatuscnf = 'Not Set' 
  AND f.dd_systemstatuspcnf = 'Not Set'
  AND UPPER(f.dd_ordertype) in ('YWMP')
  AND UPPER(f.dd_userstatusgmp) = 'GMP'
  AND (f.dd_systemstatuscreated <> 'Not Set' OR f.dd_systemstatusreleased <> 'Not Set')
  AND dd1.datevalue = current_date
  AND dd1.companycode = 'Not Set'
  AND f.dd_basicfinishdate <= TO_DATE(dd1.monthenddate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.dim_dateidtolerance <> 1
  AND f.dim_dateidtolerance = dd2.dim_dateid
  AND dd2.datevalue < TO_DATE(dd1.monthstartdate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.fact_pmorderid = t.fact_pmorderid
  AND f.dd_ordernumber = t.dd_ordernumber
  AND t.operation_number = 1
;

UPDATE tmp_fact_pmorder AS f 
SET f.dd_wolatecalibrationflag = 'Yes'
FROM tmp_fact_pmorder AS f, 
  tmp_onlytodayfromdimdate AS dd1, 
  dim_date AS dd2, 
  tmp_min_operation_per_order AS t,
  dim_maintenanceorderheader AS dm
WHERE f.dd_systemstatuscnf = 'Not Set' 
  AND f.dd_systemstatuspcnf = 'Not Set'
  AND UPPER(f.dd_ordertype) in ('YWMP')
  AND f.dim_maintenanceorderheaderid = dm.dim_maintenanceorderheaderid
  AND UPPER(dm.maintenanceactivitytype) IN ('Y80','Y81')
  AND (f.dd_systemstatuscreated <> 'Not Set' OR f.dd_systemstatusreleased <> 'Not Set')
  AND dd1.datevalue = current_date
  AND dd1.companycode = 'Not Set'
  AND f.dd_basicfinishdate <= TO_DATE(dd1.monthenddate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.dim_dateidtolerance <> 1
  AND f.dim_dateidtolerance = dd2.dim_dateid
  AND dd2.datevalue < TO_DATE(dd1.monthstartdate,'YYYY-MM-DD') - INTERVAL '1' MONTH
  AND f.fact_pmorderid = t.fact_pmorderid
  AND f.dd_ordernumber = t.dd_ordernumber
  AND t.operation_number = 1
;

/* END - Work Order Late UPDATE */


/* BEGIN - Status with Status Number 30 Update */
UPDATE tmp_fact_pmorder AS f
SET f.dd_statuswithstatusnumber = '30',
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, pma_jest AS j, pma_tj30t AS tjt, pma_tj30 AS tj
WHERE f.dd_objectnumber = j.jest_objnr
  AND j.jest_stat = tjt.tj30t_estat
  AND tjt.tj30t_estat = tj.tj30_estat
  AND tjt.tj30t_stsma = tj.tj30_stsma
  AND UPPER(tjt.tj30t_stsma) = 'YWORDER'
  AND tj.TJ30_STONR = '30';
/* END - Status with Status Number 30 Update */


/* BEGIN - Breakdown Measures Update */

-- first create a table with just AUFNR and QMART only once,
-- as QMART is at AUFNR level, but here we have order line level
DROP TABLE IF EXISTS tmp_get_one_qmart_per_aufnr;
CREATE TABLE tmp_get_one_qmart_per_aufnr
AS
SELECT 
  DISTINCT qmel_aufnr, qmel_qmart
FROM pma_qmih;

UPDATE tmp_fact_pmorder AS f
SET f.dd_notificationtypecode = IFNULL(t.qmel_qmart,'Not Set'),
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, tmp_get_one_qmart_per_aufnr AS t 
WHERE f.dd_ordernumber = t.qmel_aufnr;

-- then create a table which takes the last AUSBS date at 
-- AUFNR level and also take the other values which are necessary
-- to create the breakdown measures
DROP TABLE IF EXISTS tmp_get_latest_ausbs_per_aufnr;
CREATE TABLE tmp_get_latest_ausbs_per_aufnr
AS
SELECT 
  qmel_aufnr, 
  qmih_msaus, 
  qmih_maueh, 
  SUM(IFNULL(CAST(REPLACE(qmih_auszt,',','.') AS DECIMAL(18, 4)),0)) AS qmih_auszt, 
  MAX(qmih_ausbs) AS qmih_ausbs
FROM pma_qmih
WHERE qmih_ausbs IS NOT NULL
  AND qmel_aufnr IS NOT null
GROUP BY qmel_aufnr, qmih_msaus, qmih_maueh;

UPDATE tmp_fact_pmorder AS f
SET f.dd_malfunctionenddate = IFNULL(t.qmih_ausbs,'Not Set'),
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, tmp_get_latest_ausbs_per_aufnr AS t 
WHERE f.dd_ordernumber = t.qmel_aufnr;

UPDATE tmp_fact_pmorder AS f
SET f.dd_breakdownindicator = IFNULL(t.qmih_msaus,'Not Set'),
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, tmp_get_latest_ausbs_per_aufnr AS t 
WHERE f.dd_ordernumber = t.qmel_aufnr;
  
UPDATE tmp_fact_pmorder AS f
SET f.ct_breakdownduration = IFNULL(t.qmih_auszt,0),
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, tmp_get_latest_ausbs_per_aufnr AS t 
WHERE f.dd_ordernumber = t.qmel_aufnr;

UPDATE tmp_fact_pmorder AS f
SET f.dd_unitsforbreakdownduration = IFNULL(t.qmih_maueh,'Not Set'),
  f.dw_update_date = current_timestamp
FROM tmp_fact_pmorder AS f, tmp_get_latest_ausbs_per_aufnr AS t 
WHERE f.dd_ordernumber = t.qmel_aufnr;

/* END - Breakdown Measures update */

/* BEGIN - Malfunction End Date Update*/  
UPDATE tmp_fact_pmorder AS f
SET f.dim_malfunctionenddateid = IFNULL(dt.dim_dateid,1),
  f.dw_update_date = current_timestamp
FROM dim_date AS dt, tmp_fact_pmorder AS f	
WHERE dt.datevalue = f.dd_malfunctionenddate
  AND dt.companycode = 'Not Set'
  AND f.dim_malfunctionenddateid <> dt.dim_dateid;
/* END - Malfunction End Date Update*/  


DELETE FROM fact_pmorder;

INSERT INTO fact_pmorder(
  fact_pmorderid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_ordernumber, -- AUFK_AUFNR
  dd_ordertype, -- AUFK_AUART
  dd_plant,  -- AUFK_WERKS
  dd_location, -- AUFK_STORT
  dd_locationplant,  -- AUFK_SOWRK
  dd_orderstatus, -- AUFK_ASTNR
  dd_objectid,  -- AUFK_OBJID
  dd_startdate, -- AUFK_SDATE
  dim_dateidstartdate, 
  dd_objectnumber, -- AUFK_OBJNR
  dd_mainworkcenter, -- AUFK_VAPLZ
  dd_plantmainworkcenter, -- AUFK_WAWRK
  dd_basicfinishdate, -- AFKO_GLTRP
  dim_dateidbasicfinishdate,
  dd_basicstartdate, -- AFKO_GSTRP
  dim_dateidbasicstartdate,
  dd_actualstartdate, -- AFKO_GSTRI
  dim_dateidactualstartdate,
  dd_actualfinishdate, -- AFKO_GLTRI
  dim_dateidactualfinishdate,
  dd_generalcounter, -- AFKO_APLZT
  dd_routingnumberoperations, -- AFKO_AUFPT
  dd_responsibleplannergroup,  -- AFKO_PLGRP
  ct_actualwork, -- AFVV_ISMNW
  dd_actualstartexecdate, -- AFVV_ISDD
  dim_dateidactualstartexecdate,
  dd_actualfinishexecdate, -- AFVV_IEDD
  dim_dateidactualfinishexecdate,
  dd_actualstartexectime, -- AFVV_ISDZ
  dd_actualstartexecdateandtime,
  dd_actualfinishexectime, -- AFVV_IEDZ
  dd_actualfinishexecdateandtime,
  dd_operationnumber, -- AFVC_VORNR
  dd_operationshorttext, -- AFVC_LTXA1
  dd_activitytype, -- AFVC_LARNT
  dd_completionconfirmationnumber, -- AFVC_RUECK
  dd_confirmationcounter, -- AFVC_RMZHL
  dd_objectnumberoperation, -- AFVC_OBJNR
  dim_statusobjectinformationid, -- jsto
  dim_maintenanceorderheaderid, -- afih
  dim_plantid, -- plant
  dd_userstatusgmp,
  dd_userstatusorel,
  dd_userstatuscest,
  dd_userstatusocrt,
  dd_systemstatuscnf,
  dd_systemstatuspcnf,
  dd_statuswithstatusnumber, -- TJ30_STONR
  dd_breakdownindicator, -- QMIH_MSAUS
  ct_breakdownduration, -- QMIH_AUSZT
  dd_unitsforbreakdownduration, -- QMIH-MAUEH
  dd_systemstatusreleased,
  dim_dateidtolerance1month,
  dim_dateidtolerance3months,
  dim_dateidtolerance,
  dd_systemstatuscreated, -- TJ02T_TXT04
  dd_systemstatustechnicallycompleted, -- TJ02T_TXT04
  dd_systemstatusclosed, -- TJ02T_TXT04
  dd_unitofwork, -- AFVV_ARBEH
  dd_wolate1monthflag,
  dd_wolate3monthsflag,
  dd_wolate6monthsflag,
  dd_wolateallflag,
  dd_wolatecriticaleqflag,
  dd_wolatecalibrationflag,
  dd_notificationtypecode,  -- QMEL_QMART
  dd_malfunctionenddate, -- QMIH_AUSBS
  dim_malfunctionenddateid
)
SELECT 
  fact_pmorderid,
  dim_projectsourceid,
  amt_exchangerate,
  amt_exchangerate_gbl,
  dim_currencyid,
  dim_currencyid_tra,
  dim_currencyid_gbl,
  dw_insert_date, 
  dw_update_date,
  dd_ordernumber, -- AUFK_AUFNR
  dd_ordertype, -- AUFK_AUART
  dd_plant,  -- AUFK_WERKS
  dd_location, -- AUFK_STORT
  dd_locationplant,  -- AUFK_SOWRK
  dd_orderstatus, -- AUFK_ASTNR
  dd_objectid,  -- AUFK_OBJID
  dd_startdate, -- AUFK_SDATE
  dim_dateidstartdate, 
  dd_objectnumber, -- AUFK_OBJNR
  dd_mainworkcenter, -- AUFK_VAPLZ
  dd_plantmainworkcenter, -- AUFK_WAWRK
  dd_basicfinishdate, -- AFKO_GLTRP
  dim_dateidbasicfinishdate,
  dd_basicstartdate, -- AFKO_GSTRP
  dim_dateidbasicstartdate,
  dd_actualstartdate, -- AFKO_GSTRI
  dim_dateidactualstartdate,
  dd_actualfinishdate, -- AFKO_GLTRI
  dim_dateidactualfinishdate,
  dd_generalcounter, -- AFKO_APLZT
  dd_routingnumberoperations, -- AFKO_AUFPT
  dd_responsibleplannergroup,  -- AFKO_PLGRP
  ct_actualwork, -- AFVV_ISMNW
  dd_actualstartexecdate, -- AFVV_ISDD
  dim_dateidactualstartexecdate,
  dd_actualfinishexecdate, -- AFVV_IEDD
  dim_dateidactualfinishexecdate,
  dd_actualstartexectime, -- AFVV_ISDZ
  dd_actualstartexecdateandtime,
  dd_actualfinishexectime, -- AFVV_IEDZ
  dd_actualfinishexecdateandtime,
  dd_operationnumber, -- AFVC_VORNR
  dd_operationshorttext, -- AFVC_LTXA1
  dd_activitytype, -- AFVC_LARNT
  dd_completionconfirmationnumber, -- AFVC_RUECK
  dd_confirmationcounter, -- AFVC_RMZHL
  dd_objectnumberoperation, -- AFVC_OBJNR
  dim_statusobjectinformationid, -- jsto
  dim_maintenanceorderheaderid, -- afih
  dim_plantid, -- plant
  dd_userstatusgmp,
  dd_userstatusorel,
  dd_userstatuscest,
  dd_userstatusocrt,
  dd_systemstatuscnf,
  dd_systemstatuspcnf,
  dd_statuswithstatusnumber, -- TJ30_STONR
  dd_breakdownindicator, -- QMIH_MSAUS
  ct_breakdownduration, -- QMIH_AUSZT
  dd_unitsforbreakdownduration, -- QMIH-MAUEH
  dd_systemstatusreleased,
  dim_dateidtolerance1month,
  dim_dateidtolerance3months,
  dim_dateidtolerance,
  dd_systemstatuscreated, -- TJ02T_TXT04
  dd_systemstatustechnicallycompleted, -- TJ02T_TXT04
  dd_systemstatusclosed, -- TJ02T_TXT04
  dd_unitofwork, -- AFVV_ARBEH
  dd_wolate1monthflag,
  dd_wolate3monthsflag,
  dd_wolate6monthsflag,
  dd_wolateallflag,
  dd_wolatecriticaleqflag,
  dd_wolatecalibrationflag,
  dd_notificationtypecode,  -- QMEL_QMART
  dd_malfunctionenddate, -- QMIH_AUSBS
  dim_malfunctionenddateid
FROM tmp_fact_pmorder;

DROP TABLE IF EXISTS tmp_fact_pmorder;