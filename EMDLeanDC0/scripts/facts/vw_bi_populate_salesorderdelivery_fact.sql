/* ################################################################################################################## */
/* */
/*   Script         : bi_populate_salesorderdelivery_fact */
/*   Author         : Ashu */
/*   Created On     : 18 Feb 2013 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_salesorderdelivery_fact from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*   29 Jan 2015     CristianT 1.23				 Add amt_scheduletotal */
/*   15 Dec 2014     Alex D    1.22              Add dim_routeid */
/*   12 Dec 2014     Alex M    1.21              Add dd_Purchaseorderitem */
/*   2  May 2014     George    1.17              Added dd_BusinessCustomerPONo */
/*   14 Feb 2014     George    1.16              Added Dim_CustomerGroup4id      */
/*   12 Feb 2014     George    1.15              Added Dim_ScheduleDeliveryBlockid                                   */
/*	 26 Sep 2013     Issam     1.14              Added fields dd_SDCreateTime dd_DeliveryTime, dd_PickingTime,
												 dd_GITime, dd_SDLineCreateTime											*/
/*   08 Sep 2013     Lokesh    1.10              Currency and exchange rate changes */
/*   13 Aug 2013     Issam     1.9               Added Sales District */
/*   29 Apr 2013     Hiten     1.2               Revised population logic */
/*   24 Feb 2013     Lokesh    1.1		 Add part 2 + while loop logic  */
/*   18 Feb 2013     Ashu      1.0               Existing code migrated to Vectorwise */
/* #################################################################################################################### */

/*Refresh the required tables from corresponding mysql db first ( for testing ) */
/*cd /home/fusionops/ispring/db/schema_migration/bin */
/*./refresh_vw_from_mysql_sameserver.sh_lk albea albea dim_billingdocumenttype dim_controllingarea dim_customer dim_date dim_distributionchannel dim_part dim_plant dim_producthierarchy dim_salesorderheaderstatus dim_salesorderitemstatus dim_storagelocation fact_salesorder fact_salesorderdelivery likp_lips systemproperty */

Drop table if exists flag_holder_722;
Drop table if exists cursor_table1_722;


DROP TABLE IF EXISTS NUMBER_FOUNTAIN;
CREATE TABLE NUMBER_FOUNTAIN
(
table_name      varchar(40) NOT NULL,
max_id          int     NOT NULL
);


DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'fact_salesorderdelivery';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_salesorderdelivery',ifnull(max(fact_salesorderdeliveryid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM fact_salesorderdelivery;

Create table cursor_table1_722(
v_iid		  Integer,
v_DlvrDocNo       VARCHAR(50) null,
v_plantcode       VARCHAR(50) null,
v_DlvrItemNo      INTEGER null,
v_SalesDocNo      VARCHAR(50) null,
v_SalesItemNo     INTEGER null,
v_DeliveryQty     DECIMAL(18,4) null,
v_AGI_Date        DATE null,
v_PGI_Date        DATE null,
v_GIDate          DATE null,
v_DlvrCost        DECIMAL(18,4) null,
v_SchedQty        DECIMAL(18,4) null,
v_SchedNo         INTEGER null,
v_ControllingArea VARCHAR(4) null,
v_ProfitCenter    VARCHAR(10) null,
v_BillingType     VARCHAR(7) null,
v_DistChannel     VARCHAR(2) null,
v_DlvrRowNo	   INTEGER null,
v_DlvrRowNoMax	   INTEGER null,
v_DeliveryQtyCUMM  DECIMAL(18,4) null);

Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select row_number() over(order by ''),
	LIKP_VBELN v_DlvrDocNo,
	LIPS_POSNR v_DlvrItemNo,
	LIPS_VGBEL v_SalesDocNo,
	LIPS_VGPOS v_SalesItemNo,
	LIPS_LFIMG v_DeliveryQty,
	ifnull(LIKP_WADAT_IST,LIKP_WADAT) v_AGI_Date,
	LIKP_WADAT v_PGI_Date,
	LIPS_WAVWR v_DlvrCost,
	LIPS_WERKS v_plantcode,
	LIPS_KOKRS v_ControllingArea,
	LIPS_PRCTR v_ProfitCenter,
	LIKP_FKARV v_BillingType,
	LIKP_VTWIV v_DistChannel,
	 ROW_NUMBER() OVER (PARTITION BY LIPS_VGBEL,LIPS_VGPOS ORDER BY LIKP_WADAT_IST,LIKP_WADAT,LIKP_VBELN,LIPS_POSNR) AS v_DlvrRowNo,
	 COUNT(1) OVER (PARTITION BY LIPS_VGBEL,LIPS_VGPOS) v_DlvrRowNoMax
from LIKP_LIPS
where exists (select 1 from fact_salesorder f1
              where f1.dd_SalesDocNo = LIPS_VGBEL and f1.dd_SalesItemNo = LIPS_VGPOS and f1.dd_ItemRelForDelv = 'X')
      and LIPS_LFIMG > 0
order by LIKP_VBELN, v_AGI_Date, LIPS_POSNR;

Create table flag_holder_722 as
         SELECT ifnull(property_value,'true') pDeltaChangesFlag
                   FROM systemproperty
                  WHERE property = 'process.delta.salesorderdelivery';

/*** Remove deleted lines ***/

/* CDPOS Deletes - BI-1041 - 7 Aug 2015 */
DELETE FROM fact_salesorderdelivery
WHERE exists (select 1 from CDPOS_LIKP a where a.CDPOS_OBJECTID = dd_SalesDlvrDocNo AND a.CDPOS_CHNGIND = 'D' AND a.CDPOS_TABNAME = 'LIKP');

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE exists (select 1 from LIKP_LIPS a where dd_SalesDlvrDocNo = LIKP_VBELN)
AND not exists ( SELECT 1 from LIKP_LIPS where dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrItemNo = LIPS_POSNR)
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.* FROM fact_salesorderdelivery,flag_holder_722
WHERE exists (select 1 from LIKP_LIPS a where dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrItemNo = LIPS_POSNR)
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE not exists (select 1 from fact_salesorder f
                      where fact_salesorderdelivery.dd_SalesDocNo = f.dd_SalesDocNo
                            and fact_salesorderdelivery.dd_SalesItemNo = f.dd_SalesItemNo
                            and fact_salesorderdelivery.dd_ScheduleNo = f.dd_ScheduleNo)
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
WHERE dd_SalesDlvrDocNo = 'Not Set'
AND pDeltaChangesFlag = 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

Create table delete_tbl_722 As
Select fact_salesorderdelivery.*  FROM fact_salesorderdelivery,flag_holder_722
where  pDeltaChangesFlag <> 'true';

merge into fact_salesorderdelivery fact
using delete_tbl_722 src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then delete;
			 /* ORIGINAL SCRIPT
				call vectorwise( combine 'fact_salesorderdelivery-delete_tbl_722') */

Drop table if exists delete_tbl_722;

/*** All shipment lines not in LIKP_LIPS - 10/16/2013 ***/

DELETE FROM NUMBER_FOUNTAIN
WHERE table_name = 'cursor_table1_722';

INSERT INTO NUMBER_FOUNTAIN
select 'cursor_table1_722',ifnull(max(v_iid), ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),1))
FROM cursor_table1_722;

Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select (select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'cursor_table1_722' ) + row_number() over (order by ''),
	dd_SalesDlvrDocNo v_DlvrDocNo,
	dd_SalesDlvrItemNo v_DlvrItemNo,
	dd_SalesDocNo v_SalesDocNo,
	dd_SalesItemNo v_SalesItemNo,
	ct_QtyDelivered v_DeliveryQty,
	case when f.Dim_DateidActualGoodsIssue = 1 then agi.datevalue else pgi.datevalue end v_AGI_Date,
	pgi.datevalue v_PGI_Date,
	amt_Cost v_DlvrCost,
	pl.plantcode v_plantcode,
	ctr.ControllingAreaCode v_ControllingArea,
	pc.ProfitCenterCode v_ProfitCenter,
	btp.Type v_BillingType,
	dcn.DistributionChannelCode v_DistChannel,
	 1 AS v_DlvrRowNo,
	 1 v_DlvrRowNoMax
from fact_salesorderdelivery f
	inner join dim_date agi on f.Dim_DateidActualGoodsIssue = agi.dim_dateid
	inner join dim_date pgi on f.Dim_DateidPlannedGoodsIssue = pgi.dim_dateid
	inner join dim_plant pl on pl.dim_plantid = f.dim_plantid
	inner join Dim_ControllingArea ctr on ctr.Dim_ControllingAreaId = f.Dim_ControllingAreaId
	inner join Dim_ProfitCenter pc on pc.Dim_ProfitCenterid = f.Dim_ProfitCenterid
	inner join dim_date bdt on f.Dim_DateidBillingDate = bdt.dim_dateid
	inner join dim_billingdocumenttype btp on f.dim_billingdocumenttypeid = btp.dim_billingdocumenttypeid
	inner join dim_distributionchannel dcn on dcn.dim_distributionchannelid = f.dim_distributionchannelid
where exists (select 1 from LIKP_LIPS where f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS)
	and not exists (select 1 from LIKP_LIPS where f.dd_SalesDlvrDocNo = LIKP_VBELN and f.dd_SalesDlvrItemNo = LIPS_POSNR);

Drop table if exists cursor_table1_722_tmp;
Create table cursor_table1_722_tmp As
select v_iid,
	v_DlvrDocNo,
	v_DlvrItemNo,
	v_SalesDocNo,
	v_SalesItemNo,
	v_DeliveryQty,
	v_AGI_Date,
	v_PGI_Date,
	v_DlvrCost,
	v_plantcode,
	v_ControllingArea,
	v_ProfitCenter,
	v_BillingType,
	v_DistChannel,
	 ROW_NUMBER() OVER (PARTITION BY v_SalesDocNo,v_SalesItemNo ORDER BY v_AGI_Date,v_PGI_Date,v_DlvrDocNo,v_DlvrItemNo) AS v_DlvrRowNo,
	 COUNT(1) OVER (PARTITION BY v_SalesDocNo,v_SalesItemNo) v_DlvrRowNoMax
from cursor_table1_722;

delete from cursor_table1_722;
Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrCost,
v_plantcode,
v_ControllingArea,
v_ProfitCenter,
v_BillingType,
v_DistChannel,
v_DlvrRowNo,
v_DlvrRowNoMax)
select v_iid,
	v_DlvrDocNo,
	v_DlvrItemNo,
	v_SalesDocNo,
	v_SalesItemNo,
	v_DeliveryQty,
	v_AGI_Date,
	v_PGI_Date,
	v_DlvrCost,
	v_plantcode,
	v_ControllingArea,
	v_ProfitCenter,
	v_BillingType,
	v_DistChannel,
	v_DlvrRowNo,
	v_DlvrRowNoMax
from cursor_table1_722_tmp;

Drop table if exists cursor_table1_722_tmp;

/******/

/*** Update cummulative delivery qty ***/

Drop table if exists cursor_table1_723;
Create table cursor_table1_723 as
select  a.v_iid,
	a.v_DlvrDocNo,
	a.v_plantcode,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_DlvrCost,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_DeliveryQty) v_DeliveryQtyCUMM
from cursor_table1_722 a inner join cursor_table1_722 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
group by a.v_iid,
	a.v_DlvrDocNo,
	a.v_plantcode,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_DlvrCost,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;

Drop table if exists salesorder_table_101;
Create table salesorder_table_101 as
select f.dd_SalesDocNo v_SalesDocNo,
	f.dd_SalesItemNo v_SalesItemNo,
	f.dd_ScheduleNo v_SchedNo,
	gi.DateValue v_GIDate,
	f.ct_ConfirmedQty v_SchedQty,
	ROW_NUMBER() OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo ORDER BY gi.DateValue,f.dd_ScheduleNo) AS v_DlvrRowNo,
	COUNT(1) OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo) v_DlvrRowNoMax
From fact_salesorder f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
where f.dd_ItemRelForDelv = 'X' and f.ct_ConfirmedQty > 0
	and exists (select 1 from cursor_table1_722 where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo);

Drop table if exists cursor_table1_724;
Create table cursor_table1_724 as
select a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_SchedQty) v_SchedQtyCUMM
from salesorder_table_101 a inner join salesorder_table_101 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
group by a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;
Drop table if exists salesorder_table_101;

/*** Get final values with schedule link ***/

Drop table if exists cursor_table1_722;
Create table cursor_table1_722 as
select a.v_iid,
	a.v_DlvrDocNo,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_plantcode,
	a.v_AGI_Date,
	a.v_PGI_Date,
	b.v_GIDate,
	a.v_DlvrCost,
	b.v_SchedNo,
	a.v_ControllingArea,
	a.v_ProfitCenter,
	a.v_BillingType,
	a.v_DistChannel,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	a.v_DeliveryQtyCUMM,
	case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM
	    then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax and b.v_SchedQtyCUMM <= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then 0
	    		else
	    		case when b.v_SchedQty > a.v_DeliveryQty
			      	then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty then a.v_DeliveryQty
					  when b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty - (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM)
					  else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	else case when b.v_DlvrRowNo = b.v_DlvrRowNoMax
			      	  		then b.v_SchedQty
			      	  		else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  				when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
			      	  					then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 		else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  				when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  			  	else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  		end
			 end
		 end
	    else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty
		      	then case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then a.v_DeliveryQty + (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM) else a.v_DeliveryQty end
		      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then b.v_SchedQty
		      	  	else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM))
		      	   end
		 end
	end v_SchedQty,
      case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM
            then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax
            			then case when (b.v_SchedQtyCUMM - b.v_SchedQty) > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
            						then a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)
            				 	else a.v_DeliveryQty end
		      else
		    	case when b.v_SchedQty > a.v_DeliveryQty
	                      then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty or b.v_DlvrRowNo = b.v_DlvrRowNoMax
	                                then a.v_DeliveryQty
	                                else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty
	                                          else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
	                           end
	                      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty
	                      			else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  						when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)
			      	  							then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 			else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
	                      					when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  		  				else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	   end
	                 end
	              end
            else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty then a.v_DeliveryQty
                      else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM))
                 end
      end v_DeliveryQty,
      ROW_NUMBER() OVER (PARTITION BY a.v_DlvrDocNo,a.v_DlvrItemNo ORDER BY b.v_DlvrRowNo) AS v_DlvrRowSeq
from cursor_table1_723 a
	inner join cursor_table1_724 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
where a.v_DeliveryQtyCUMM > (b.v_SchedQtyCUMM - b.v_SchedQty)
	and ((b.v_DlvrRowNo < b.v_DlvrRowNoMax and b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)) or b.v_DlvrRowNo = b.v_DlvrRowNoMax);

Drop table if exists cursor_table1_723;
Drop table if exists cursor_table1_724;


/*****/

/* loop table */
drop table if exists loop_tbl_722;
create table loop_tbl_722 as
select * from cursor_table1_722 where v_DlvrRowNoMax > 0 and v_DeliveryQty > 0;

/* This intermediate table is used to handle the order by asc that was used in the insert query ( in mysql proc ) */

DROP TABLE IF EXISTS tmp4a_fs_dimpc ;
CREATE TABLE  tmp4a_fs_dimpc
AS
select pc.ProfitCenterCode,pc.ControllingArea,v_AGI_Date,min(pc.ValidTo) as ValidTo
FROM loop_tbl_722, Dim_ProfitCenter pc
WHERE pc.ProfitCenterCode = v_ProfitCenter
AND pc.ControllingArea = v_ControllingArea
AND pc.ValidTo >= v_AGI_Date
AND pc.RowIsCurrent = 1
GROUP BY pc.ProfitCenterCode,pc.ControllingArea,v_AGI_Date;


DROP TABLE IF EXISTS tmp4_fs_dimpc ;
CREATE TABLE  tmp4_fs_dimpc
AS
select a.ProfitCenterCode,a.ControllingArea,v_AGI_Date,a.ValidTo,pc.dim_profitcenterid
FROM tmp4a_fs_dimpc a , Dim_ProfitCenter pc
WHERE pc.ProfitCenterCode = a.ProfitCenterCode
AND pc.ControllingArea = a.ControllingArea
AND pc.RowIsCurrent = 1
AND pc.ValidTo = a.ValidTo;


drop table if exists tmp_fsod_t001t;
create table tmp_fsod_t001t as
	SELECT LIPS_VGBEL dd_SalesDocNo,
              LIPS_VGPOS dd_SalesItemNo,
              f.dd_ScheduleNo,
              LIKP_VBELN dd_SalesDlvrDocNo, 	-- used for Dim_DeliveryHeaderStatusid
              LIPS_POSNR dd_SalesDlvrItemNo,	-- used for Dim_DeliveryItemStatusid
              ifnull(lips_bwart ,'Not Set') dd_MovementType,
              IFNULL(lt.v_DeliveryQty,0) ct_QtyDelivered,
              case when v_DlvrRowSeq = 1 then lt.v_DlvrCost else 0 end amt_Cost_DocCurr,
              case when v_DlvrRowSeq = 1 then lt.v_DlvrCost else 0 end amt_Cost,	/* LK: 8 Sep 2013 */
              /*case when v_DlvrRowSeq = 1 then (lt.v_DlvrCost * (case when f.amt_ExchangeRate < 0 then (1/(-1 * f.amt_ExchangeRate)) else f.amt_ExchangeRate end)) else 0 end amt_Cost,*/
              lips_vbeaf ct_FixedProcessDays,
              lips_vbeav ct_ShipProcessDays,
              lt.v_SchedQty ct_ScheduleQtySalesUnit,
              lt.v_SchedQty ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL,
              convert(bigint, 1) as Dim_DateidPlannedGoodsIssue, 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPlannedGoodsIssue,
              convert(bigint, 1) as Dim_DateidActualGoodsIssue, 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGoodsIssue,
              convert(bigint, 1) as Dim_DateidProofOfSalesDelivery,
              convert(bigint, 1) as Dim_DateidDeliveryDate, 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_lfdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDeliveryDate,
              convert(bigint, 1) as Dim_DateidLoadingDate, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoadingDate,
              convert(bigint, 1) as Dim_DateidPickingDate, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_kodat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPickingDate,
              convert(bigint, 1) as Dim_DateidDlvrDocCreated, 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDlvrDocCreated,
              convert(bigint, 1) as Dim_DateidMatlAvail, 				-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = lips_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMatlAvail,
              convert(bigint, 1) as Dim_CustomeridSoldTo, 				-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = likp_kunag),f.Dim_CustomerID) Dim_CustomeridSoldTo,
              convert(bigint, 1) as Dim_CustomeridShipTo, 				-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = likp_kunnr),1) Dim_CustomeridShipTo,
              convert(bigint, 1) as Dim_Partid, 						-- ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber = lips_matnr AND dp.Plant = lips_werks),1) Dim_Partid,
              pl.Dim_Plantid,
              convert(bigint, 1) as Dim_StorageLocationid, 				-- ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode = lips_lgort and sl.plant = lips_werks),1) Dim_StorageLocationid,
              convert(bigint, 1) as Dim_ProductHierarchyid,				-- ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy = lips_prodh),1) Dim_ProductHierarchyid,
              convert(bigint, 1) as Dim_DeliveryHeaderStatusid, 		-- ifnull((select Dim_SalesOrderHeaderStatusid from Dim_SalesOrderHeaderStatus sohs where sohs.SalesDocumentNumber = LIKP_VBELN),1) Dim_DeliveryHeaderStatusid,
              convert(bigint, 1) as Dim_DeliveryItemStatusid, 			-- ifnull((select Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber = LIKP_VBELN and sois.SalesItemNumber = LIPS_POSNR),1) Dim_DeliveryItemStatusid,
              f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail as Dim_DateidMatlAvailOriginal,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId,
              convert(bigint, 1) as Dim_ProfitCenterid, 	-- ifnull((select pc.Dim_ProfitCenterid from tmp4_fs_dimpc pc where  pc.ProfitCenterCode = v_ProfitCenter AND pc.ControllingArea = v_ControllingArea AND pc.ValidTo >= v_AGI_Date ),1) Dim_ProfitCenterid,
              convert(bigint, 1) as Dim_ControllingAreaId, 	-- ifnull((select ca.Dim_ControllingAreaid from Dim_ControllingArea ca where  ca.ControllingAreaCode = v_ControllingArea),1) Dim_ControllingAreaId,
              f.Dim_BillingDateId Dim_DateIdBillingDate,
              convert(bigint, 1) as Dim_BillingDocumentTypeid, -- ifnull((SELECT dim_billingdocumenttypeid FROM dim_billingdocumenttype bdt WHERE bdt.Type = v_BillingType AND bdt.RowIsCurrent= 1), 1),
              convert(bigint, 1) as Dim_DistributionChannelId, -- ifnull((SELECT Dim_DistributionChannelId FROM dim_distributionchannel dc WHERE dc.DistributionChannelCode = v_DistChannel AND dc.RowIsCurrent = 1),f.Dim_DistributionChannelId),
              convert(bigint, 1) as Dim_DateidActualGI_Original, -- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGI_Original,
              ifnull(f.Dim_PurchaseOrderTypeId,1) Dim_PurchaseOrderTypeId,
		(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by '') rid,
		f.dim_Currencyid_TRA,
		f.dim_Currencyid_GBL,
		f.dim_currencyid_STAT,
		f.amt_exchangerate_STAT,
	/* changes 26 Sep 2013 */
		ifnull(LIKP_ERZET, '000000') as dd_SDCreateTime,
		ifnull(LIKP_LFUHR, '000000') as dd_DeliveryTime,
		ifnull(LIKP_KOUHR, '000000') as dd_PickingTime,
		ifnull(LIKP_WAUHR, '000000') as dd_GITime,
		ifnull(LIPS_ERZET, '000000') as dd_SDLineCreateTime,
		ifnull(likp_bolnr,'Not Set') as dd_BillofLading,
        likp_wadat, 			-- Dim_DateidPlannedGoodsIssue,
        likp_wadat_ist, 		-- Dim_DateidActualGoodsIssue,
        LIKP_PODAT,				-- Dim_DateidProofOfSalesDelivery
        likp_lfdat, 			-- Dim_DateidDeliveryDate,
        likp_lddat,				-- Dim_DateidLoadingDate,
        likp_kodat,				-- Dim_DateidPickingDate,
        likp_erdat, 			-- Dim_DateidDlvrDocCreated,
        lips_mbdat, 			-- Dim_DateidMatlAvail,
        likp_kunag, 			-- Dim_CustomeridSoldTo,
        likp_kunnr, 			-- Dim_CustomeridShipTo,
        lips_matnr, lips_werks, -- Dim_Partid,
        lips_lgort, 			-- Dim_StorageLocationid,
        lips_prodh, 			-- Dim_ProductHierarchyid,
        v_ProfitCenter, v_ControllingArea, v_AGI_Date, -- Dim_ProfitCenterid, Dim_ControllingAreaId
        v_BillingType,			-- Dim_BillingDocumentTypeid,
        v_DistChannel, ifnull(f.Dim_DistributionChannelId,1) as Dim_DistributionChannelId_f, -- Dim_DistributionChannelId,
		pl.CompanyCode, f.Dim_CustomerID
	FROM	loop_tbl_722 lt
		inner join LIKP_LIPS on LIKP_VBELN = lt.v_DlvrDocNo and LIPS_POSNR = lt.v_DlvrItemNo
		inner join fact_salesorder f on f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS and f.dd_ScheduleNo = lt.v_SchedNo
		inner join Dim_Plant pl on pl.plantcode = LIPS_WERKS;

	/* Dim_DateidPlannedGoodsIssue */
	update tmp_fsod_t001t f
	set f.Dim_DateidPlannedGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_wadat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidPlannedGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidActualGoodsIssue */
	update tmp_fsod_t001t f
	set f.Dim_DateidActualGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidActualGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidProofOfSalesDelivery */
	update tmp_fsod_t001t f
	set f.Dim_DateidProofOfSalesDelivery = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.LIKP_PODAT AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidProofOfSalesDelivery <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDeliveryDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidDeliveryDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_lfdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidDeliveryDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidLoadingDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidLoadingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_lddat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidLoadingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidPickingDate */
	update tmp_fsod_t001t f
	set f.Dim_DateidPickingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_kodat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidPickingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDlvrDocCreated */
	update tmp_fsod_t001t f
	set f.Dim_DateidDlvrDocCreated = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.likp_erdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidDlvrDocCreated <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidMatlAvail */
	update tmp_fsod_t001t f
	set f.Dim_DateidMatlAvail = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join dim_date dd on dd.DateValue = f.lips_mbdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidMatlAvail <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_CustomeridSoldTo */
	update tmp_fsod_t001t f
	set f.Dim_CustomeridSoldTo = ifnull(cust.Dim_CustomerID, f.Dim_CustomerID)
	from tmp_fsod_t001t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunag
	where f.Dim_CustomeridSoldTo <> ifnull(cust.Dim_CustomerID, f.Dim_CustomerID);

	/* Dim_CustomeridShipTo */
	update tmp_fsod_t001t f
	set f.Dim_CustomeridShipTo = ifnull(cust.Dim_CustomerID, 1)
	from tmp_fsod_t001t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunnr
	where f.Dim_CustomeridShipTo <> ifnull(cust.Dim_CustomerID, 1);

	/* Dim_Partid */
	update tmp_fsod_t001t f
	set f.Dim_Partid = ifnull(dp.dim_partid, 1)
	from tmp_fsod_t001t f
			left join dim_part dp on dp.PartNumber = f.lips_matnr AND dp.Plant = f.lips_werks
	where f.Dim_Partid <> ifnull(dp.dim_partid, 1);

	/* Dim_StorageLocationid */
	update tmp_fsod_t001t f
	set f.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
	from tmp_fsod_t001t f
			left join Dim_StorageLocation sl on sl.LocationCode = f.lips_lgort and sl.plant = f.lips_werks
	where f.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

	/* Dim_ProductHierarchyid */
	update tmp_fsod_t001t f
	set f.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
	from tmp_fsod_t001t f
			left join Dim_ProductHierarchy ph on ph.ProductHierarchy = lips_prodh
	where f.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

	/* Dim_DeliveryHeaderStatusid */
	update tmp_fsod_t001t f
	set f.Dim_DeliveryHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
	from tmp_fsod_t001t f
			left join Dim_SalesOrderHeaderStatus sohs on sohs.SalesDocumentNumber = f.dd_SalesDlvrDocNo
	where f.Dim_DeliveryHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

	/* Dim_DeliveryItemStatusid */
	update tmp_fsod_t001t f
	set f.Dim_DeliveryItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
	from tmp_fsod_t001t f
			left join (select a.SalesDocumentNumber,a.SalesItemNumber,max(a.Dim_SalesOrderItemStatusid) Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus a group by a.SalesDocumentNumber,a.SalesItemNumber) sois
				on sois.SalesDocumentNumber = f.dd_SalesDlvrDocNo and sois.SalesItemNumber = f.dd_SalesDlvrItemNo
	where f.Dim_DeliveryItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

	/* Dim_ProfitCenterid */
	merge into tmp_fsod_t001t fa
	using (select t0.rid, ifnull(t1.Dim_ProfitCenterid, 1) Dim_ProfitCenterid
		   from tmp_fsod_t001t t0
					left join (select distinct f.rid, pc.Dim_ProfitCenterid
							   from tmp_fsod_t001t f
										inner join tmp4_fs_dimpc pc on    pc.ProfitCenterCode = f.v_ProfitCenter
										   						      AND pc.ControllingArea = f.v_ControllingArea
							   where pc.ValidTo >= f.v_AGI_Date) t1 on t0.rid = t1.rid
		  ) src
	on fa.rid = src.rid
	when matched then update set fa.Dim_ProfitCenterid = ifnull(src.Dim_ProfitCenterid, 1)
	where fa.Dim_ProfitCenterid <> ifnull(src.Dim_ProfitCenterid, 1);

	/* Dim_ControllingAreaId */
	update tmp_fsod_t001t f
	set f.Dim_ControllingAreaId = ifnull(ca.Dim_ControllingAreaid, 1)
	from tmp_fsod_t001t f
			left join Dim_ControllingArea ca on ca.ControllingAreaCode = f.v_ControllingArea
	where f.Dim_ControllingAreaId <> ifnull(ca.Dim_ControllingAreaid, 1);

	/* Dim_BillingDocumentTypeid */
	update tmp_fsod_t001t f
	set f.Dim_BillingDocumentTypeid = ifnull(bdt.dim_billingdocumenttypeid, 1)
	from tmp_fsod_t001t f
			left join dim_billingdocumenttype bdt on bdt.Type = v_BillingType AND bdt.RowIsCurrent= 1
	where f.Dim_BillingDocumentTypeid <> ifnull(bdt.dim_billingdocumenttypeid, 1);

	/* Dim_DistributionChannelId */
	update tmp_fsod_t001t f
	set f.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f)
	from tmp_fsod_t001t f
			left join dim_distributionchannel dc on dc.DistributionChannelCode = f.v_DistChannel AND dc.RowIsCurrent = 1
	where f.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f);

	/* Dim_DateidActualGI_Original */
	update tmp_fsod_t001t f
	set f.Dim_DateidActualGI_Original = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t001t f
			left join Dim_Date dd on dd.DateValue = likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidActualGI_Original <> ifnull(dd.Dim_Dateid, 1);

   INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidProofOfSalesDelivery,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateidBillingDate,
              Dim_BillingDocumentTypeid,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,
	      Dim_PurchaseOrderTypeId,
	      fact_salesorderdeliveryid,
	      dim_Currencyid_TRA,
	      dim_Currencyid_GBL,
	      dim_currencyid_STAT,
	      amt_exchangerate_STAT,
		  dd_SDCreateTime,
		  dd_DeliveryTime,
		  dd_PickingTime,
		  dd_GITime,
		  dd_SDLineCreateTime,
		  dd_BillofLading
		)
select dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidProofOfSalesDelivery,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateidBillingDate,
              Dim_BillingDocumentTypeid,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,
	      Dim_PurchaseOrderTypeId,
	      rid,
	      dim_Currencyid_TRA,
	      dim_Currencyid_GBL,
	      dim_currencyid_STAT,
	      amt_exchangerate_STAT,
		  dd_SDCreateTime,
		  dd_DeliveryTime,
		  dd_PickingTime,
		  dd_GITime,
		  dd_SDLineCreateTime,
		  dd_BillofLading
from tmp_fsod_t001t;

drop table if exists tmp_fsod_t001t;


	update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_salesorderdeliveryid),0) from fact_salesorderdelivery)
	where table_name = 'fact_salesorderdelivery';

    DROP TABLE IF EXISTS tmp_fact_sodf_dimpc_1;
	CREATE TABLE tmp_fact_sodf_dimpc_1
	AS
	SELECT pc.ProfitCenterCode,pc.ControllingArea,ifnull(LIKP_WADAT_IST,LIKP_WADAT) LIKP_WADAT_IST,min(pc.ValidTo) as min_ValidTo
	FROM LIKP_LIPS , Dim_ProfitCenter pc
	WHERE pc.ProfitCenterCode = LIPS_PRCTR
 	AND pc.ControllingArea = LIPS_KOKRS
       AND pc.ValidTo >= ifnull(LIKP_WADAT_IST,LIKP_WADAT)
	AND pc.RowIsCurrent = 1
	GROUP BY pc.ProfitCenterCode,pc.ControllingArea,ifnull(LIKP_WADAT_IST,LIKP_WADAT);

	DROP TABLE IF EXISTS tmp_fact_sodf_dimpc_2;
	CREATE TABLE tmp_fact_sodf_dimpc_2
	AS
	SELECT t.*,pc.Dim_ProfitCenterid
	from Dim_ProfitCenter pc, tmp_fact_sodf_dimpc_1 t
	WHERE pc.ProfitCenterCode = t.ProfitCenterCode
	AND pc.ControllingArea = t.ControllingArea
	AND pc.ValidTo = t.min_ValidTo;

	DROP TABLE IF EXISTS tmp_fact_sodf_minsched;
	CREATE TABLE tmp_fact_sodf_minsched
	AS
	select f1.dd_SalesDocNo,f1.dd_SalesItemNo,min(f1.dd_ScheduleNo) min_dd_ScheduleNo
	from fact_salesorder f1
	GROUP BY f1.dd_SalesDocNo,f1.dd_SalesItemNo;


	DROP TABLE IF EXISTS tmp_fact_sodf_LIKP_LIPS;
	CREATE TABLE tmp_fact_sodf_LIKP_LIPS
	AS
	SELECT l.*,f.min_dd_ScheduleNo
	from LIKP_LIPS l inner join tmp_fact_sodf_minsched f on f.dd_SalesDocNo = LIPS_VGBEL and f.dd_SalesItemNo = LIPS_VGPOS
	where not exists (select 1 from fact_salesorder f1
                        where f1.dd_SalesDocNo = LIPS_VGBEL and f1.dd_SalesItemNo = LIPS_VGPOS and f1.dd_ItemRelForDelv = 'X')
		or not exists (select 1 from loop_tbl_722 lt where LIKP_VBELN = lt.v_DlvrDocNo and LIPS_POSNR = lt.v_DlvrItemNo);


drop table if exists tmp_fsod_t002t;
create table tmp_fsod_t002t as
      SELECT ll.LIPS_VGBEL dd_SalesDocNo,
              ll.LIPS_VGPOS dd_SalesItemNo,
              0 dd_ScheduleNo,
              ll.LIKP_VBELN dd_SalesDlvrDocNo,
              ll.LIPS_POSNR dd_SalesDlvrItemNo,
              ifnull(ll.lips_bwart ,'Not Set') dd_MovementType,
              f.dd_ReferenceDocumentNo as dd_ReferenceDocNo,
              ll.LIPS_LFIMG ct_QtyDelivered,
              ll.LIPS_WAVWR amt_Cost_DocCurr,
              ll.LIPS_WAVWR amt_Cost,
              /*(ll.LIPS_WAVWR * (case when f.amt_ExchangeRate < 0 then (1/(-1 * f.amt_ExchangeRate)) else f.amt_ExchangeRate end)) amt_Cost,*/
              ifnull(f.amt_SubTotal3,0.0000) as amt_SalesSubTotal3,
              ifnull(f.amt_SubTotal4,0.0000) as amt_SalesSubTotal4,
              ll.lips_vbeaf ct_FixedProcessDays,
              ll.lips_vbeav ct_ShipProcessDays,
              ll.LIPS_LFIMG ct_ScheduleQtySalesUnit,
              ll.LIPS_LFIMG ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL
              ,convert(bigint, 1) as  Dim_DateidPlannedGoodsIssue 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPlannedGoodsIssue,
              ,convert(bigint, 1) as  Dim_DateidActualGoodsIssue 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGoodsIssue,
              ,convert(bigint, 1) as  Dim_DateidProofOfSalesDelivery
              ,convert(bigint, 1) as  Dim_DateidDeliveryDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_lfdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDeliveryDate,
              ,convert(bigint, 1) as  Dim_DateidLoadingDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoadingDate,
              ,convert(bigint, 1) as  Dim_DateidPickingDate 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_kodat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidPickingDate,
              ,convert(bigint, 1) as  Dim_DateidDlvrDocCreated 		-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.likp_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidDlvrDocCreated,
              ,convert(bigint, 1) as  Dim_DateidMatlAvail 			-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = ll.lips_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMatlAvail,
              ,convert(bigint, 1) as  Dim_CustomeridSoldTo 			-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = ll.likp_kunag),f.Dim_CustomerID) Dim_CustomeridSoldTo,
              ,convert(bigint, 1) as  Dim_CustomeridShipTo 			-- ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = ll.likp_kunnr),1) Dim_CustomeridShipTo,
              ,convert(bigint, 1) as  Dim_Partid 					-- ifnull((SELECT dim_partid FROM dim_part dp WHERE dp.PartNumber = ll.lips_matnr AND dp.Plant = ll.lips_werks),1) Dim_Partid,
              ,pl.Dim_Plantid
              ,convert(bigint, 1) as  Dim_StorageLocationid 		-- ifnull((SELECT Dim_StorageLocationid FROM Dim_StorageLocation sl WHERE sl.LocationCode = ll.lips_lgort and sl.plant = ll.lips_werks),1) Dim_StorageLocationid,
              ,convert(bigint, 1) as  Dim_ProductHierarchyid 		-- ifnull((SELECT Dim_ProductHierarchyid FROM Dim_ProductHierarchy ph WHERE ph.ProductHierarchy = ll.lips_prodh),1) Dim_ProductHierarchyid,
              ,convert(bigint, 1) as  Dim_DeliveryHeaderStatusid 	-- ifnull((select Dim_SalesOrderHeaderStatusid from Dim_SalesOrderHeaderStatus sohs where sohs.SalesDocumentNumber = ll.LIKP_VBELN),1) Dim_DeliveryHeaderStatusid,
              ,convert(bigint, 1) as  Dim_DeliveryItemStatusid 		-- ifnull((select Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus sois where sois.SalesDocumentNumber = ll.LIKP_VBELN and sois.SalesItemNumber = ll.LIPS_POSNR),1) Dim_DeliveryItemStatusid,
              ,f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail as Dim_DateidMatlAvailOriginal,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId
			  ,convert(bigint, 1) as  Dim_ProfitCenterid 			-- ifnull((select pc.Dim_ProfitCenterid from tmp_fact_sodf_dimpc_2 pc where  pc.ProfitCenterCode = LIPS_PRCTR AND pc.ControllingArea = LIPS_KOKRS AND pc.LIKP_WADAT_IST = ifnull(ll.LIKP_WADAT_IST,ll.LIKP_WADAT)),1) Dim_ProfitCenterid,
              ,convert(bigint, 1) as  Dim_ControllingAreaId 		-- ifnull((select ca.Dim_ControllingAreaid from Dim_ControllingArea ca where  ca.ControllingAreaCode = ll.LIPS_KOKRS),1) Dim_ControllingAreaId,
              ,f.Dim_BillingDateId Dim_DateIdBillingDate
              ,convert(bigint, 1) as  Dim_BillingDocumentTypeId 	-- ifnull((SELECT dim_billingdocumenttypeid FROM dim_billingdocumenttype bdt WHERE bdt.Type = ll.LIKP_FKARV AND bdt.RowIsCurrent= 1), 1) Dim_BillingDocumentTypeId,
              ,convert(bigint, 1) as  Dim_DistributionChannelId 	-- ifnull((SELECT Dim_DistributionChannelId FROM dim_distributionchannel dc WHERE dc.DistributionChannelCode = ll.LIKP_VTWIV AND dc.RowIsCurrent = 1),f.Dim_DistributionChannelId) Dim_DistributionChannelId,
              ,convert(bigint, 1) as  Dim_DateidActualGI_Original 	-- ifnull((SELECT Dim_Dateid FROM Dim_Date dd WHERE dd.DateValue = likp_wadat_ist AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidActualGI_Original,
			  ,ifnull(Dim_PurchaseOrderTypeId,1) Dim_PurchaseOrderTypeId,
			  (select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by '') fact_salesorderdeliveryid,
			  f.dim_Currencyid_TRA,
			  f.dim_Currencyid_GBL,
			  f.dim_currencyid_STAT,
			  f.amt_exchangerate_STAT,
			  ifnull(LIKP_ERZET, '000000') as dd_SDCreateTime,
			  ifnull(LIKP_LFUHR, '000000') as dd_DeliveryTime,
			  ifnull(LIKP_KOUHR, '000000') as dd_PickingTime,
			  ifnull(LIKP_WAUHR, '000000') as dd_GITime,
			  ifnull(LIPS_ERZET, '000000') as dd_SDLineCreateTime,
			  ifnull(likp_bolnr,'Not Set') as dd_BillofLading,
		likp_wadat, 			-- Dim_DateidPlannedGoodsIssue,
        likp_wadat_ist, 		-- Dim_DateidActualGoodsIssue,
        LIKP_PODAT,				-- Dim_DateidProofOfSalesDelivery
        likp_lfdat, 			-- Dim_DateidDeliveryDate,
        likp_lddat,				-- Dim_DateidLoadingDate,
        likp_kodat,				-- Dim_DateidPickingDate,
        likp_erdat, 			-- Dim_DateidDlvrDocCreated,
        lips_mbdat, 			-- Dim_DateidMatlAvail,
        likp_kunag, 			-- Dim_CustomeridSoldTo,
        likp_kunnr, 			-- Dim_CustomeridShipTo,
        lips_matnr, lips_werks, -- Dim_Partid,
        lips_lgort, 			-- Dim_StorageLocationid,
        lips_prodh, 			-- Dim_ProductHierarchyid,
        LIPS_PRCTR, LIPS_KOKRS, -- Dim_ProfitCenterid, Dim_ControllingAreaId
        LIKP_FKARV,			-- Dim_BillingDocumentTypeid,
        LIKP_VTWIV, f.Dim_DistributionChannelId as Dim_DistributionChannelId_f, -- Dim_DistributionChannelId,
		pl.CompanyCode, f.Dim_CustomerID
      FROM tmp_fact_sodf_LIKP_LIPS ll
          inner join fact_salesorder f on f.dd_SalesDocNo = ll.LIPS_VGBEL and f.dd_SalesItemNo = ll.LIPS_VGPOS
                                          and f.dd_ScheduleNo = ll.min_dd_ScheduleNo
          inner join Dim_Plant pl on pl.plantcode = ll.LIPS_WERKS;

	/* Dim_DateidPlannedGoodsIssue */
	update tmp_fsod_t002t f
	set f.Dim_DateidPlannedGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_wadat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidPlannedGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidActualGoodsIssue */
	update tmp_fsod_t002t f
	set f.Dim_DateidActualGoodsIssue = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidActualGoodsIssue <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidProofOfSalesDelivery */
	update tmp_fsod_t002t f
	set f.Dim_DateidProofOfSalesDelivery = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.LIKP_PODAT AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidProofOfSalesDelivery <> ifnull(dd.Dim_Dateid, 1);


	/* Dim_DateidDeliveryDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidDeliveryDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_lfdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidDeliveryDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidLoadingDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidLoadingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_lddat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidLoadingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidPickingDate */
	update tmp_fsod_t002t f
	set f.Dim_DateidPickingDate = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_kodat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidPickingDate <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidDlvrDocCreated */
	update tmp_fsod_t002t f
	set f.Dim_DateidDlvrDocCreated = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.likp_erdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidDlvrDocCreated <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_DateidMatlAvail */
	update tmp_fsod_t002t f
	set f.Dim_DateidMatlAvail = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join dim_date dd on dd.DateValue = f.lips_mbdat AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidMatlAvail <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_Partid */
	update tmp_fsod_t002t f
	set f.Dim_Partid = ifnull(dp.dim_partid, 1)
	from tmp_fsod_t002t f
			left join dim_part dp on dp.PartNumber = f.lips_matnr AND dp.Plant = f.lips_werks
	where f.Dim_Partid <> ifnull(dp.dim_partid, 1);

	/* Dim_StorageLocationid */
	update tmp_fsod_t002t f
	set f.Dim_StorageLocationid = ifnull(sl.Dim_StorageLocationid, 1)
	from tmp_fsod_t002t f
			left join Dim_StorageLocation sl on sl.LocationCode = f.lips_lgort and sl.plant = f.lips_werks
	where f.Dim_StorageLocationid <> ifnull(sl.Dim_StorageLocationid, 1);

	/* Dim_ProductHierarchyid */
	update tmp_fsod_t002t f
	set f.Dim_ProductHierarchyid = ifnull(ph.Dim_ProductHierarchyid, 1)
	from tmp_fsod_t002t f
			left join Dim_ProductHierarchy ph on ph.ProductHierarchy = lips_prodh
	where f.Dim_ProductHierarchyid <> ifnull(ph.Dim_ProductHierarchyid, 1);

	/* Dim_DeliveryHeaderStatusid */
	update tmp_fsod_t002t f
	set f.Dim_DeliveryHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1)
	from tmp_fsod_t002t f
			left join Dim_SalesOrderHeaderStatus sohs on sohs.SalesDocumentNumber = f.dd_SalesDlvrDocNo
	where f.Dim_DeliveryHeaderStatusid <> ifnull(sohs.Dim_SalesOrderHeaderStatusid, 1);

	/* Dim_DeliveryItemStatusid */
	update tmp_fsod_t002t f
	set f.Dim_DeliveryItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
	from tmp_fsod_t002t f
			left join (select a.SalesDocumentNumber,a.SalesItemNumber,max(a.Dim_SalesOrderItemStatusid) Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus a group by a.SalesDocumentNumber,a.SalesItemNumber) sois on sois.SalesDocumentNumber = f.dd_SalesDlvrDocNo and sois.SalesItemNumber = f.dd_SalesDlvrItemNo
	where f.Dim_DeliveryItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);

	/* Dim_ProfitCenterid */
	update tmp_fsod_t002t f
	set f.Dim_ProfitCenterid = ifnull(pc.Dim_ProfitCenterid, 1)
	from tmp_fsod_t002t f
			left join tmp_fact_sodf_dimpc_2 pc on   pc.ProfitCenterCode = f.LIPS_PRCTR
												AND pc.ControllingArea  = f.LIPS_KOKRS
												AND pc.LIKP_WADAT_IST   = ifnull(f.LIKP_WADAT_IST,f.LIKP_WADAT)
	where f.Dim_ProfitCenterid <> ifnull(pc.Dim_ProfitCenterid, 1);

	/* Dim_ControllingAreaId */
	update tmp_fsod_t002t f
	set f.Dim_ControllingAreaId = ifnull(ca.Dim_ControllingAreaid, 1)
	from tmp_fsod_t002t f
			left join Dim_ControllingArea ca on ca.ControllingAreaCode = f.LIPS_KOKRS
	where f.Dim_ControllingAreaId <> ifnull(ca.Dim_ControllingAreaid, 1);

	/* Dim_BillingDocumentTypeid */
	update tmp_fsod_t002t f
	set f.Dim_BillingDocumentTypeid = ifnull(bdt.dim_billingdocumenttypeid, 1)
	from tmp_fsod_t002t f
			left join dim_billingdocumenttype bdt on bdt.Type = LIKP_FKARV AND bdt.RowIsCurrent= 1
	where f.Dim_BillingDocumentTypeid <> ifnull(bdt.dim_billingdocumenttypeid, 1);

	/* Dim_DistributionChannelId */
	update tmp_fsod_t002t f
	set f.Dim_DistributionChannelId = ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f)
	from tmp_fsod_t002t f
			left join dim_distributionchannel dc on dc.DistributionChannelCode = f.LIKP_VTWIV AND dc.RowIsCurrent = 1
	where f.Dim_DistributionChannelId <> ifnull(dc.Dim_DistributionChannelId, Dim_DistributionChannelId_f);

	/* Dim_DateidActualGI_Original */
	update tmp_fsod_t002t f
	set f.Dim_DateidActualGI_Original = ifnull(dd.Dim_Dateid, 1)
	from tmp_fsod_t002t f
			left join Dim_Date dd on dd.DateValue = likp_wadat_ist AND dd.CompanyCode = f.CompanyCode
	where f.Dim_DateidActualGI_Original <> ifnull(dd.Dim_Dateid, 1);

	/* Dim_CustomeridSoldTo */
	update tmp_fsod_t002t f
	set f.Dim_CustomeridSoldTo = ifnull(cust.Dim_CustomerID, f.Dim_CustomerID)
	from tmp_fsod_t002t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunag
	where f.Dim_CustomeridSoldTo <> ifnull(cust.Dim_CustomerID, f.Dim_CustomerID);

	/* Dim_CustomeridShipTo */
	update tmp_fsod_t002t f
	set f.Dim_CustomeridShipTo = ifnull(cust.Dim_CustomerID, 1)
	from tmp_fsod_t002t f
			left join Dim_Customer cust on cust.CustomerNumber = f.likp_kunnr
	where f.Dim_CustomeridShipTo <> ifnull(cust.Dim_CustomerID, 1);

      INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidProofOfSalesDelivery,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,Dim_PurchaseOrderTypeId,fact_salesorderdeliveryid,
              dim_Currencyid_TRA,
              dim_Currencyid_GBL,
              dim_currencyid_STAT,
              amt_exchangerate_STAT,
			  dd_SDCreateTime,
			  dd_DeliveryTime,
			  dd_PickingTime,
			  dd_GITime,
			  dd_SDLineCreateTime,
			  dd_BillofLading)
select dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidProofOfSalesDelivery,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,
              Dim_DateidActualGI_Original,Dim_PurchaseOrderTypeId,fact_salesorderdeliveryid,
              dim_Currencyid_TRA,
              dim_Currencyid_GBL,
              dim_currencyid_STAT,
              amt_exchangerate_STAT,
			  dd_SDCreateTime,
			  dd_DeliveryTime,
			  dd_PickingTime,
			  dd_GITime,
			  dd_SDLineCreateTime,
			  dd_BillofLading
from tmp_fsod_t002t;

drop table if exists tmp_fsod_t002t;

drop table if exists tmp_updt_VBFA;
create table tmp_updt_VBFA
as
select fact_salesorderdeliveryid, max(case when (sod.ct_QtyDelivered - v.vbfa_rfmng) < 0 then 0 else (sod.ct_QtyDelivered - v.vbfa_rfmng) end) ct_QtyDelivered
FROM VBFA v, fact_salesorderdelivery sod
  WHERE sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
      and v.vbfa_bwart = '602' and sod.ct_QtyDelivered > 0
      and not exists (select 1 from VBFA v1
                      where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                            and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat)
GROUP BY fact_salesorderdeliveryid;

UPDATE fact_salesorderdelivery sod
SET sod.ct_QtyDelivered = ifnull(v.ct_QtyDelivered,0)
FROM tmp_updt_VBFA v, fact_salesorderdelivery sod
WHERE sod.fact_salesorderdeliveryid = v.fact_salesorderdeliveryid
  AND ifnull(sod.ct_QtyDelivered,-1) = ifnull(v.ct_QtyDelivered,0);

drop table if exists tmp_updt_VBFA;

/* start CALL bi_populate_so_shipment() */

drop table if exists update_so_shipment_001;
create table update_so_shipment_001 as
select f.dd_SalesDocNo v_dd_SalesDocNo, f.dd_SalesItemNo v_dd_SalesItemNo, f.dd_ScheduleNo v_dd_ScheduleNo,
	ifnull(sum(f.ct_QtyDelivered), 0) v_ct_DeliveredQty,
	ifnull(max(f.Dim_DateidDeliveryDate), 1) v_Dim_DateidShipmentDelivery,
	ifnull(max(f.Dim_DateidActualGoodsIssue), 1) v_Dim_DateidActualGI,
	ifnull(max(f.Dim_DateidProofOfSalesDelivery), 1) v_Dim_DateidProofOfSalesDelivery,
	min(f.Dim_CustomeridShipto) v_Dim_CustomeridShipTo,
	ifnull(min(f.Dim_DateidDlvrDocCreated), 1) v_Dim_DateidDlvrDocCreated
from fact_salesorderdelivery f
	inner join dim_salesorderitemstatus sois on f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
where sois.GoodsMovementStatus <> 'Not yet processed'
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo;

/* LK: 12 Aug 2014 - Dim_CustomeridShipTo should not depend on sois.GoodsMovementStatus */
drop table if exists update_so_shipment_001_Dim_CustomeridShipTo;
CREATE TABLE update_so_shipment_001_Dim_CustomeridShipTo
AS
select f.dd_SalesDocNo v_dd_SalesDocNo, f.dd_SalesItemNo v_dd_SalesItemNo, f.dd_ScheduleNo v_dd_ScheduleNo,
min(f.Dim_CustomeridShipto) v_Dim_CustomeridShipTo
from fact_salesorderdelivery f
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo;

UPDATE fact_salesorder so
SET ct_DeliveredQty = 0
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE  ct_DeliveredQty <> 0;

/*UPDATE fact_salesorder so
   SET ct_DeliveredQty = v_ct_DeliveredQty
	,dw_update_date = current_timestamp
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo and ct_DeliveredQty <> v_ct_DeliveredQty

UPDATE fact_salesorder so
   SET Dim_DateidShipmentDelivery = v_Dim_DateidShipmentDelivery
	,dw_update_date = current_timestamp
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo

UPDATE fact_salesorder so
   SET Dim_DateidActualGI = v_Dim_DateidActualGI
	,dw_update_date = current_timestamp
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo
 */


 /* Updates for SalesOrder */

MERGE INTO fact_salesorder so
USING update_so_shipment_001 u ON so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo
WHEN MATCHED THEN UPDATE SET ct_DeliveredQty = v_ct_DeliveredQty,Dim_DateidShipmentDelivery = v_Dim_DateidShipmentDelivery,Dim_DateidActualGI = v_Dim_DateidActualGI,Dim_DateidProofOfSalesDelivery=v_Dim_DateidProofOfSalesDelivery,dw_update_date = current_timestamp;

/* APP-9348 CristianC 18Apr2018*/
DROP TABLE IF EXISTS TMP_FACT_SALESORDER_max_gi_del;
CREATE TABLE TMP_FACT_SALESORDER_max_gi_del AS
SELECT
dd_SalesDocNo
,dd_SalesItemNo
,max(agd.datevalue) actualgi
,max(adl.datevalue) actuladel
,max(pod.datevalue) posdd
FROM fact_salesorder f
JOIN dim_date agd ON agd.DIM_DATEID = f.dim_dateidactualgi
JOIN dim_date adl ON adl.DIM_DATEID = f.dim_dateidshipmentdelivery
join dim_date pod ON pod.DIM_DATEID = f.dim_dateidproofofsalesdelivery
GROUP BY dd_SalesDocNo,dd_SalesItemNo;

UPDATE fact_salesorder f
SET f.dim_dateidproofofsalesdeliveryline = dd.dim_dateid
FROM fact_salesorder f, dim_date dd, TMP_FACT_SALESORDER_max_gi_del t, dim_company dc
WHERE f.dd_SalesDocNo = t.dd_SalesDocNo
AND f.dd_SalesItemNo = t.dd_SalesItemNo
AND dd.DATEVALUE = t.posdd
and f.dim_companyid = dc.dim_companyid
and dc.companycode = dd.companycode
AND f.dim_dateidproofofsalesdeliveryline <> dd.dim_dateid;

UPDATE fact_salesorder f
SET f.dim_dateidactualgiline = dd.dim_dateid
FROM fact_salesorder f, dim_date dd, TMP_FACT_SALESORDER_max_gi_del t, dim_company dc
WHERE f.dd_SalesDocNo = t.dd_SalesDocNo
AND f.dd_SalesItemNo = t.dd_SalesItemNo
AND dd.DATEVALUE = t.actualgi
and f.dim_companyid = dc.dim_companyid
and dc.companycode = dd.companycode
AND f.dim_dateidactualgiline <> dd.dim_dateid;

UPDATE fact_salesorder f
SET f.dim_dateidshipmentdeliveryline = dd.dim_dateid
FROM fact_salesorder f, dim_date dd, TMP_FACT_SALESORDER_max_gi_del t, dim_company dc
WHERE f.dd_SalesDocNo = t.dd_SalesDocNo
AND f.dd_SalesItemNo = t.dd_SalesItemNo
AND dd.DATEVALUE = t.actuladel
and f.dim_companyid = dc.dim_companyid
and dc.companycode = dd.companycode
AND f.dim_dateidshipmentdeliveryline <> dd.dim_dateid;
/* END APP-9348 CristianC 18Apr2018*/

/* Deviation GI Date vs Conf Del Date */

UPDATE fact_salesorder fso
SET ct_DevGIdtvsConfDeldt = ifnull(Case when cddemd.DateValue = '0001-01-01' then 0
                            else
							    case when agidt.DateValue = '0001-01-01' then (current_date - cddemd.DateValue)
							         else(agidt.DateValue - cddemd.DateValue)
								 end
						     end, 0)
FROM fact_salesorder fso, dim_date agidt, dim_date cddemd
WHERE fso.dim_dateidactualgi = agidt.dim_dateid
AND   fso.dim_dateidconfirmeddelivery_emd = cddemd.dim_dateid;



/*Deviation GI Date vs Req Date*/

UPDATE fact_salesorder fso
SET ct_DevGIdtvsReqdt = ifnull(Case when  rdemd.DateValue = '0001-01-01' then 0
                             else
								  case when agidt.DateValue = '0001-01-01' then (current_date - rdemd.DateValue)
                                       else(agidt.DateValue - rdemd.DateValue)
									   end
					    end, 0)
FROM fact_salesorder fso, dim_date agidt, dim_date rdemd
WHERE fso.dim_dateidactualgi = agidt.dim_dateid
AND   fso.dim_dateidrequested_emd = rdemd.dim_dateid;



/* Deviation GI Date vs Acc GI Conf Date	 */

UPDATE fact_salesorder fso
SET ct_DevGIdtvsAccGIConfdt = ifnull(Case when  acgidmto.DateValue = '0001-01-01' then 999999
								   else
								      case when agidt.DateValue = '0001-01-01' then (current_date - acgidmto.DateValue)
                                           else(agidt.DateValue - acgidmto.DateValue)
							          end
							  end, 0)
FROM fact_salesorder fso, dim_date agidt, dim_date_factory_calendar acgidmto
WHERE fso.dim_dateidactualgi = agidt.dim_dateid
AND   fso.dim_accordinggidatemto_emd = acgidmto.dim_dateid;



 /*Delivery Service according to Requested Date */

UPDATE fact_salesorder fso
SET dd_devGIdtvsAccGidt = ifnull(case when dd_DevGIdtvsConfDeldt in (' ','Not Set') then ' '
                               else
							       case when cast (dd_DevGIdtvsConfDeldt as int) <= 1 then '1'
								        else '0'
										end
								end, ' ');



 /*Delivery Service against confirmed del date */

UPDATE fact_salesorder fso
SET ct_DelSeragconfdeldt =  ifnull(case when rdemd.DateValue >  current_date then 999999
                               else case when dim_dateidactualgi = 1 then 0
                                    else case when ct_DEVGIDTVSREQDT  > 1 then 0
		                                 else 1
                                             end
                                    end
								end, 0)

FROM fact_salesorder fso,dim_date rdemd
WHERE  fso.dim_dateidrequested_emd = rdemd.dim_dateid;



 /* Delivery Service against requested del date */


UPDATE fact_salesorder fso
SET ct_DelSeragreqdeldt =  ifnull(case when cddemd.DateValue >  current_date then 999999
                               else case when dim_dateidactualgi = 1 then 0
                                     else case when ct_DEVGIDTVSCONFDELDT  > 1 then 0
		                            else 1
                                    end
                           end
						   end, 0)
FROM fact_salesorder fso,  dim_date cddemd
WHERE  fso.dim_dateidconfirmeddelivery_emd = cddemd.dim_dateid;



 /* Delivery Service against according GI-date (confirmed)*/

UPDATE fact_salesorder fso
SET    ct_delseragaccgidt = ifnull(CASE
                              WHEN acgidmto.datevalue = '0001-01-01' THEN 999999
                              ELSE
                                CASE
                                  WHEN acgidmto.datevalue > CURRENT_DATE
                                       AND dim_dateidactualgi = 1 THEN 999999
                                  ELSE
                                    CASE
                                      WHEN ( ct_devgidtvsaccgiconfdt >= 1
                                             AND ct_devgidtvsaccgiconfdt <
                                                 999999 )
                                    THEN 0
                                      ELSE 1
                                    END
                                END
                            END, 0)
FROM   fact_salesorder fso,
       dim_date_factory_calendar acgidmto
WHERE  fso.dim_accordinggidatemto_emd = acgidmto.dim_dateid;

UPDATE fact_salesorder
SET dd_ontimeconfirmed = CASE WHEN CT_DELSERAGACCGIDT = 0 THEN 'No' WHEN CT_DELSERAGACCGIDT=1 THEN 'Yes' ELSE 'Not Set' END
FROM fact_salesorder
WHERE dd_ontimeconfirmed <> CASE WHEN CT_DELSERAGACCGIDT = 0 THEN 'No' WHEN CT_DELSERAGACCGIDT=1 THEN 'Yes' ELSE 'Not Set' END ;


/*Merck LS OTIF -- get Actual GI Date on the first scheduleline */
drop table if exists tmp_actualdategi_merckls;
create table tmp_actualdategi_merckls
as select distinct f.dd_salesdocno,
f.DD_SALESITEMNO,
first_value(f.DIM_DATEIDACTUALGI) over (partition by f.dd_salesdocno, f.DD_SALESITEMNO order by dd.datevalue desc) as DD_DATEIDACTUALGI
from fact_salesorder f
	inner join dim_date dd on f.DIM_DATEIDACTUALGI = dd.dim_dateid;

update fact_salesorder f
set f.dim_dateidactualgimerckls = ifnull(t.DD_DATEIDACTUALGI, 1)
from fact_salesorder f, tmp_actualdategi_merckls t
where f.dd_salesdocno = t.dd_salesdocno
and f.DD_SALESITEMNO = t.DD_SALESITEMNO
and f.dim_dateidactualgimerckls <> ifnull(t.DD_DATEIDACTUALGI, 1);
/* and f.DIM_DATEIDACTUALGI = 1 */

/* update fact_salesorder f
set f.dim_dateidactualgimerckls = f.DIM_DATEIDACTUALGI
where f.dim_dateidactualgimerckls = 1 */
/*END Merck LS OTIF -- get Actual GI Date on the first scheduleline */

UPDATE fact_salesorder so
   SET Dim_CustomeridShipTo = ifnull(v_Dim_CustomeridShipTo, Dim_CustomeridShipTo)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;

/* LK: 12 Aug 2014 - Dim_CustomeridShipTo should not depend on sois.GoodsMovementStatus */
UPDATE fact_salesorder so
   SET Dim_CustomeridShipTo = ifnull(v_Dim_CustomeridShipTo, 1)
   ,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001_Dim_CustomeridShipTo u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo
 AND v_Dim_CustomeridShipTo is NOT NULL and Dim_CustomeridShipTo = 1 AND v_Dim_CustomeridShipTo <> 1;

drop table if exists update_so_shipment_001_Dim_CustomeridShipTo;

UPDATE fact_salesorder so
   SET Dim_DateidDlvrDocCreated = ifnull(v_Dim_DateidDlvrDocCreated, 1)
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  FROM update_so_shipment_001 u, fact_salesorder so
 WHERE so.dd_SalesDocNo = v_dd_SalesDocNo and so.dd_SalesItemNo = v_dd_SalesItemNo and so.dd_ScheduleNo = v_dd_ScheduleNo;


  UPDATE fact_salesorder so
  SET ct_DeliveredQty = ifnull(so.ct_ConfirmedQty, 0)
  fROM dim_salesorderitemstatus s, fact_salesorder so
  WHERE so.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      AND so.dd_ItemRelForDelv = 'X'
      AND s.OverallDeliveryStatus = 'Completely processed'
      AND so.ct_DeliveredQty < so.ct_ConfirmedQty
      AND not exists (select 1 from fact_salesorderdelivery sod inner join VBFA v on sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
                      where v.vbfa_bwart = '602' and sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo
                      and not exists (select 1 from VBFA v1 where v1.vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                                      and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat));

/* end update so_shipment */

update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_salesorderdeliveryid),0) from fact_salesorderdelivery)
where table_name = 'fact_salesorderdelivery';

  INSERT INTO fact_salesorderdelivery(
              dd_SalesDocNo,
              dd_SalesItemNo,
              dd_ScheduleNo,
              dd_SalesDlvrDocNo,
              dd_SalesDlvrItemNo,
              dd_MovementType,
              dd_ReferenceDocNo,
              ct_QtyDelivered,
              amt_Cost_DocCurr,
              amt_Cost,
              amt_SalesSubTotal3,
              amt_SalesSubTotal4,
              ct_FixedProcessDays,
              ct_ShipProcessDays,
              ct_ScheduleQtySalesUnit,
              ct_ConfirmedQty,
              ct_PriceUnit,
              amt_UnitPrice,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              Dim_DateidPlannedGoodsIssue,
              Dim_DateidActualGoodsIssue,
              Dim_DateidProofOfSalesDelivery,
              Dim_DateidDeliveryDate,
              Dim_DateidLoadingDate,
              Dim_DateidPickingDate,
              Dim_DateidDlvrDocCreated,
              Dim_DateidMatlAvail,
              Dim_CustomeridSoldTo,
              Dim_CustomeridShipTo,
              Dim_Partid,
              Dim_Plantid,
              Dim_StorageLocationid,
              Dim_ProductHierarchyid,
              Dim_DeliveryHeaderStatusid,
              Dim_DeliveryItemStatusid,
              Dim_DateidSalesOrderCreated,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidMatlAvailOriginal,
              Dim_Currencyid,
              Dim_Companyid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_salesorderitemcategoryid,
              Dim_ScheduleLineCategoryId,
              Dim_ProfitCenterId,
              Dim_ControllingAreaId,
              Dim_DateIdBillingDate,
              Dim_BillingDocumentTypeId,
              Dim_DistributionChannelId,fact_salesorderdeliveryid,
                dim_Currencyid_TRA,
                dim_Currencyid_GBL,
                dim_currencyid_STAT,
                amt_exchangerate_STAT	)

      SELECT f.dd_SalesDocNo,
              f.dd_SalesItemNo,
              f.dd_ScheduleNo,
              'Not Set' dd_SalesDlvrDocNo,
              0 dd_SalesDlvrItemNo,
              'Not Set' dd_MovementType,
              f.dd_ReferenceDocumentNo,
              0 ct_QtyDelivered,
              0 amt_Cost_DocCurr,
              0 amt_Cost,
              ifnull(f.amt_SubTotal3,0.0000) amt_SalesSubTotal3,
              ifnull(f.amt_SubTotal4,0.0000) amt_SalesSubTotal4,
              0 ct_FixedProcessDays,
              0 ct_ShipProcessDays,
              f.ct_ScheduleQtySalesUnit,
              f.ct_ConfirmedQty,
              f.ct_PriceUnit,
              f.amt_UnitPrice,
              f.amt_ExchangeRate,
              f.amt_ExchangeRate_GBL,
              f.Dim_DateidGoodsIssue,
              1 Dim_DateidActualGoodsIssue,
              1 Dim_DateidProofOfSalesDelivery,
              f.Dim_DateidSchedDelivery,
              f.Dim_DateidLoading,
              1 Dim_DateidPickingDate,
              1 Dim_DateidDlvrDocCreated,
              f.Dim_DateidMtrlAvail,
              f.Dim_CustomerID Dim_CustomeridSoldTo,
              f.Dim_CustomerID Dim_CustomeridShipTo,
              f.Dim_Partid,
              f.Dim_Plantid,
              f.Dim_StorageLocationid,
              f.Dim_ProductHierarchyid,
              1 Dim_DeliveryHeaderStatusid,
              1 Dim_DeliveryItemStatusid,
              f.Dim_DateidSalesOrderCreated,
              f.Dim_DateidSchedDeliveryReq,
              f.Dim_DateidSchedDlvrReqPrev,
              f.Dim_DateidMtrlAvail,
              f.Dim_Currencyid,
              f.Dim_Companyid,
              f.Dim_SalesDivisionid,
              f.Dim_ShipReceivePointid,
              f.Dim_DocumentCategoryid,
              f.Dim_SalesDocumentTypeid,
              f.Dim_SalesOrgid,
              f.Dim_SalesGroupid,
              f.Dim_CostCenterid,
              f.Dim_BillingBlockid,
              f.Dim_TransactionGroupid,
              f.Dim_CustomerGroup1id,
              f.Dim_CustomerGroup2id,
              f.dim_salesorderitemcategoryid,
              f.Dim_ScheduleLineCategoryId,
              1 Dim_ProfitCenterid,
              1 Dim_ControllingAreaId,
              f.Dim_BillingDateId Dim_DateIdBillingDate,
              1 Dim_BillingDocumentTypeId,
              f.Dim_DistributionChannelId Dim_DistributionChannelId,
	(select max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesorderdelivery' ) + row_number() over (order by ''),
                f.dim_Currencyid_TRA,
                f.dim_Currencyid_GBL,
                f.dim_currencyid_STAT,
                f.amt_exchangerate_STAT

      FROM fact_salesorder f inner join dim_salesorderitemstatus s
                                    ON f.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      where f.dd_ItemRelForDelv = 'X' and s.OverallDeliveryStatus <> 'Completely processed'
            and (ct_ConfirmedQty - ct_DeliveredQty) > 0
            and not exists (select 1 from fact_salesorderdelivery f1
                            where f1.dd_SalesDocNo = f.dd_SalesDocNo and f1.dd_SalesItemNo = f.dd_SalesItemNo and f1.dd_ScheduleNo = f.dd_ScheduleNo);

UPDATE fact_salesorderdelivery sod
SET sod.Dim_deliveryTypeId = ifnull(dt.Dim_deliveryTypeId, 1)
  FROM LIKP_LIPS, Dim_Deliverytype dt, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = LIPS_POSNR
  AND LIKP_LFART IS NOT NULL
  AND dt.DeliveryType = LIKP_LFART
  AND dt.RowIsCurrent = 1;

UPDATE facT_salesorderdelivery
SET Dim_deliveryTypeId = 1
WHERE Dim_deliveryTypeId IS NULL;

/* 10 Aug 2015 CristianT Start: Fix for ambiguous error */
/* Old update is here
UPDATE       fact_salesorderdelivery sod
FROM 		likp_lips_vbuk v, dim_overallstatusforcreditcheck oscc
SET sod.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckID
WHERE		 sod.dd_SalesDlvrDocNo = v.VBUK_VBELN
AND		 oscc.overallstatusforcreditcheck = ifnull(v.VBUK_CMGST, 'Not Set')
AND 		oscc.RowIsCurrent = 1 */
DROP TABLE IF EXISTS tmp_overallstatus;
CREATE TABLE tmp_overallstatus AS
select distinct sod.fact_salesorderdeliveryid, ifnull(v.VBUK_CMGST, 'Not Set') as VBUK_CMGST
FROM fact_salesorderdelivery sod
		inner join likp_lips_vbuk v on sod.dd_SalesDlvrDocNo = v.VBUK_VBELN;

merge into fact_salesorderdelivery sod
using ( select max(oscc.dim_overallstatusforcreditcheckID) dim_overallstatusforcreditcheckID, tt.fact_salesorderdeliveryid
		from tmp_overallstatus tt
				inner join dim_overallstatusforcreditcheck oscc on tt.VBUK_CMGST = oscc.overallstatusforcreditcheck
		where oscc.RowIsCurrent = 1
	    group by tt.fact_salesorderdeliveryid) src
on sod.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update SET sod.Dim_OverallStatusCreditCheckId = ifnull(src.dim_overallstatusforcreditcheckID, 1)
where sod.Dim_OverallStatusCreditCheckId <> ifnull(src.dim_overallstatusforcreditcheckID, 1);

DROP TABLE IF EXISTS tmp_overallstatus;
/* 10 Aug 2015 CristianT End */

merge into fact_salesorderdelivery fact
using (select fact_salesorderdeliveryid, min(fb.dd_billing_no) dd_billing_no
	   from fact_salesorderdelivery sod
		   		inner join fact_billing fb on    fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        								     AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
	   group by  fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.dd_billing_no = ifnull(src.dd_billing_no,'Not Set')
where fact.dd_billing_no <> ifnull(src.dd_billing_no,'Not Set');

merge into fact_salesorderdelivery fact
using (select fact_salesorderdeliveryid, max(so.Dim_SalesDistrictId) Dim_SalesDistrictId
	   from fact_salesorderdelivery sod
		   		inner join fact_salesorder so on    sod.dd_SalesDocNo = so.dd_SalesDocNo
												and sod.dd_SalesItemNo = so.dd_SalesItemNo
												and sod.dd_ScheduleNo = so.dd_ScheduleNo
	   group by  fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.Dim_SalesDistrictId = ifnull(src.Dim_SalesDistrictId,1)
where fact.Dim_SalesDistrictId <> ifnull(src.Dim_SalesDistrictId,1);

/* Start of the final update	*/

UPDATE fact_salesorderdelivery sd
  /*  SET sd.amt_Cost =
            Decimal(((CASE
                  WHEN f.amt_ExchangeRate < 0
                  THEN
                      (1 / (-1 * f.amt_ExchangeRate))
                  ELSE
                      f.amt_ExchangeRate
                END) * sd.amt_Cost_DocCurr),18,4) , */
	SET sd.amt_Cost = ifnull(sd.amt_Cost_DocCurr,0),			--Not multiplying by local exchg rate. Stored as it is in doc/tran curr
        sd.ct_PriceUnit = ifnull(f.ct_PriceUnit,1),
        sd.amt_UnitPrice = ifnull(f.amt_UnitPrice,0)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;
/*
merge into fact_salesorderdelivery fact
using (select sd.fact_salesorderdeliveryid, min(f.amt_UnitPriceUoM) amt_UnitPriceUoM
	   From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
	   WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
			AND f.dd_SalesItemNo = v.VBAP_POSNR
			AND f.dd_ScheduleNo = v.VBEP_ETENR
			AND sd.dd_SalesDocNo = f.dd_SalesDocNo
			AND sd.dd_SalesItemNo = f.dd_SalesItemNo
			AND sd.dd_ScheduleNo = f.dd_ScheduleNo
	   group by sd.fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.amt_UnitPriceUoM = src.amt_UnitPriceUoM
where fact.amt_UnitPriceUoM <> src.amt_UnitPriceUoM */

/* APP-7744 - Shipped Amt and Std Unit Price incorrect - Oana 23 Oct2017 */
merge into fact_salesorderdelivery fact
using (select sd.fact_salesorderdeliveryid, min(f.amt_UnitPriceUoM) amt_UnitPriceUoM
	   From fact_salesorder f,  fact_salesorderdelivery sd
	   WHERE sd.dd_SalesDocNo = f.dd_SalesDocNo
			AND sd.dd_SalesItemNo = f.dd_SalesItemNo
			AND sd.dd_ScheduleNo = f.dd_ScheduleNo
	   group by sd.fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.amt_UnitPriceUoM = IFNULL(src.amt_UnitPriceUoM,0)
where fact.amt_UnitPriceUoM <> IFNULL(src.amt_UnitPriceUoM,0);
/* END APP-7744 - Shipped Amt and Std Unit Price incorrect - Oana 23 Oct2017 */

UPDATE fact_salesorderdelivery sd
    SET sd.amt_ExchangeRate = IFNULL(f.amt_ExchangeRate,1),
        sd.amt_ExchangeRate_GBL = IFNULL(f.amt_ExchangeRate_GBL,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET
        sd.Dim_DateidSalesOrderCreated = IFNULL(f.Dim_DateidSalesOrderCreated,1),
        sd.Dim_DateidSchedDeliveryReq = IFNULL(f.Dim_DateidSchedDeliveryReq,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_DateidSchedDlvrReqPrev = IFNULL(f.Dim_DateidSchedDlvrReqPrev,1),
        sd.Dim_DateidMatlAvailOriginal = IFNULL(f.Dim_DateidMtrlAvail,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_DateidFirstDate = IFNULL(f.Dim_DateidFirstDate,1),
        sd.Dim_Currencyid = IFNULL(f.Dim_Currencyid,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_TRA = IFNULL(f.Dim_Currencyid_TRA,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_GBL = IFNULL(f.Dim_Currencyid_GBL,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Currencyid_STAT = IFNULL(f.Dim_Currencyid_STAT,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.amt_ExchangeRate_STAT = IFNULL(f.amt_ExchangeRate_STAT,1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sd
    SET sd.Dim_Companyid = IFNULL(f.Dim_Companyid,1),
        sd.Dim_SalesDivisionid = f.Dim_SalesDivisionid
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_ShipReceivePointid = ifnull(f.Dim_ShipReceivePointid, 1),
        sd.Dim_DocumentCategoryid = ifnull(f.Dim_DocumentCategoryid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_SalesDocumentTypeid = ifnull(f.Dim_SalesDocumentTypeid, 1),
        sd.Dim_SalesOrgid = ifnull(f.Dim_SalesOrgid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_SalesGroupid = ifnull(f.Dim_SalesGroupid, 1),
        sd.Dim_CostCenterid = ifnull(f.Dim_CostCenterid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_BillingBlockid = ifnull(f.Dim_BillingBlockid, 1),
        sd.Dim_TransactionGroupid = ifnull(f.Dim_TransactionGroupid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

UPDATE fact_salesorderdelivery sd
    SET sd.dim_purchaseordertypeid = ifnull(f.dim_purchaseordertypeid, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


UPDATE fact_salesorderdelivery sd
    SET sd.Dim_CustomeridSoldTo =
            ifnull(CASE sd.Dim_CustomeridSoldTo
              WHEN 1 THEN f.Dim_CustomerID
              ELSE sd.Dim_CustomeridSoldTo
            END, 1),
        sd.Dim_CustomerGroup1id = ifnull(f.Dim_CustomerGroup1id, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;


update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup4Id = ifnull(so.Dim_MaterialPriceGroup4Id, 1)
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup4Id,-1) <> ifnull(so.Dim_MaterialPriceGroup4Id, 1);

update fact_salesorderdelivery sod
SET sod.Dim_MaterialPriceGroup5Id = ifnull(so.Dim_MaterialPriceGroup5Id, 1)
 from fact_salesorder so, fact_salesorderdelivery sod
WHERE so.dd_SalesDocNo = sod.dd_SalesDocNo
and so.dd_salesItemNo = sod.dd_SalesItemNo
and so.dd_ScheduleNo = sod.dd_ScheduleNo
AND ifnull(sod.Dim_MaterialPriceGroup5Id,-1) <> ifnull(so.Dim_MaterialPriceGroup5Id, 1);

UPDATE fact_salesorderdelivery sd
    SET sd.Dim_CustomerGroup2id = ifnull(f.Dim_CustomerGroup2id, 1),
        sd.dim_salesorderitemcategoryid = ifnull(f.dim_salesorderitemcategoryid, 1),
        sd.Dim_ScheduleLineCategoryId = ifnull(f.Dim_ScheduleLineCategoryId, 1)
From fact_salesorder f, VBAK_VBAP_VBEP v, fact_salesorderdelivery sd
  WHERE     f.dd_SalesDocNo = v.VBAK_VBELN
        AND f.dd_SalesItemNo = v.VBAP_POSNR
        AND f.dd_ScheduleNo = v.VBEP_ETENR
        AND sd.dd_SalesDocNo = f.dd_SalesDocNo
        AND sd.dd_SalesItemNo = f.dd_SalesItemNo
        AND sd.dd_ScheduleNo = f.dd_ScheduleNo;

/* Start Changes 12 Feb 2014 */
UPDATE fact_salesorderdelivery sod
 set sod.dim_scheduledeliveryblockid = ifnull(so.dim_scheduledeliveryblockid, 1)
FROM  fact_salesorder so, fact_salesorderdelivery sod
 WHERE sod.dd_Salesdocno = so.dd_SalesDocNo
 and sod.dd_SalesItemNo = so.dd_SalesItemNo
 and sod.dd_ScheduleNo = so.dd_ScheduleNo
 and ifnull(sod.dim_scheduledeliveryblockid,-1) <> ifnull(so.dim_scheduledeliveryblockid,-2);

UPDATE fact_salesorderdelivery sod
SET sod.dim_scheduledeliveryblockid = 1
WHERE sod.dim_scheduledeliveryblockid is NULL;

UPDATE fact_salesorderdelivery sod
SET sod.Dim_CustomerGroup4id = ifnull(so.Dim_CustomerGroup4id,1)
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.Dim_CustomerGroup4id,-1) <> ifnull(so.Dim_CustomerGroup4id,-2);

update fact_salesorderdelivery
set Dim_CustomerGroup4id = 1
where Dim_CustomerGroup4id is NULL;

/* END Changes 14 Feb 2014 */
UPDATE fact_salesorderdelivery sod
set dd_trackingNo = dd_BillofLading
where ifnull(dd_trackingNo,'Not Set') <> dd_BillofLading;

UPDATE fact_salesorderdelivery sod
set dd_trackingNo = 'Not Set'
where dd_trackingNo is null;
/* Start Changes 02 May 2014 */

update fact_salesorderdelivery set dd_BusinessCustomerPONo = 'Not Set' where dd_BusinessCustomerPONo is NULL;

UPDATE fact_salesorderdelivery sod
SET sod.dd_BusinessCustomerPONo = ifnull(so.dd_BusinessCustomerPONo,'Not Set')
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND ifnull(sod.dd_BusinessCustomerPONo,'Not Set') <> ifnull(so.dd_BusinessCustomerPONo,'Not Set');

/* CristiT Update 13 June 2014 */
update fact_salesorderdelivery sod
set sod.dd_ShipmentNumber = IFNULL(v.VTTK_TKNUM,'Not Set')
FROM fact_salesorderdelivery sod
		inner join LIKP_LIPS l on    sod.dd_SalesDlvrDocNo = l.LIKP_VBELN
  								 AND sod.dd_SalesDlvrItemNo = l.LIPS_POSNR
	    inner join (select max(VTTK_TKNUM) VTTK_TKNUM, VTTP_VBELN from VTTK_VTTP group by VTTP_VBELN) v on l.LIKP_VBELN = v.VTTP_VBELN
where sod.dd_ShipmentNumber <> v.VTTK_TKNUM;

UPDATE fact_salesorderdelivery
SET dd_ShipmentNumber = 'Not Set'
WHERE dd_ShipmentNumber IS NULL;

UPDATE fact_salesorderdelivery sod
SET sod.Dim_ShippingConditionShipmentId = ifnull(sc.Dim_ShippingConditionId, 1)
FROM LIKP_LIPS l, VTTK_VTTP v, Dim_ShippingCondition sc, fact_salesorderdelivery sod
WHERE sod.dd_SalesDlvrDocNo = l.LIKP_VBELN
  AND sod.dd_SalesDlvrItemNo = l.LIPS_POSNR
  AND l.LIKP_VBELN = v.VTTP_VBELN
  AND sc.ShippingConditionCode = ifnull(v.VTTK_VSBED, 'Not Set')
  AND sc.RowIsCurrent = 1;

UPDATE fact_salesorderdelivery
SET Dim_ShippingConditionShipmentId = 1
WHERE Dim_ShippingConditionShipmentId IS NULL;
/* CristiT End of Update 13 June 2014 */

/* CristianT 07 Aug 2015 Start: Added logic to populate the following base columns */
DROP TABLE IF EXISTS tmp_vttk_fields;
CREATE TABLE tmp_vttk_fields AS
SELECT DISTINCT ds.VTTK_TKNUM dd_ShipmentNumber,
       ds.VTTP_VBELN dd_SalesDlvrDocNo,
       ifnull(ddcsc.dim_dateid, 1) dim_currshipmentcompldateid,
       ifnull(ddpsc.dim_dateid, 1) dim_plannedshipmentcompldateid,
       ifnull(ddpse.dim_dateid, 1) dim_plannedshipmentenddateid,
       ifnull(ddsd.dim_dateid, 1) dim_shipmentdateid,
       ifnull(ds.VTTK_EXTI1, 'Not Set') dd_voyfltnum,
       ifnull(ds.VTTK_TDLNR, 'Not Set') dd_serviceagent
FROM (select v.VTTK_TKNUM, v.VTTP_VBELN, v.VTTK_DTABF, v.VTTK_DPABF, v.Vttk_DPTEN, v.VTTK_DATEN, v.VTTK_EXTI1, v.VTTK_TDLNR,
	         dc.companycode
	  from fact_salesorderdelivery sod
				inner join dim_company dc on sod.Dim_Companyid = dc.Dim_Companyid
				inner join LIKP_LIPS l on    sod.dd_SalesDlvrDocNo = l.LIKP_VBELN
										 AND sod.dd_SalesDlvrItemNo = l.LIPS_POSNR
				inner join vttk_vttp v on    v.VTTK_TKNUM = sod.dd_ShipmentNumber
										 and v.VTTP_VBELN = l.LIKP_VBELN) ds
			left join dim_date ddcsc on ddcsc.datevalue = ds.VTTK_DTABF and ddcsc.companycode = ds.companycode
			left join dim_date ddpsc on ddpsc.datevalue = ds.VTTK_DPABF and ddpsc.companycode = ds.companycode
			left join dim_date ddpse on ddpse.datevalue = ds.Vttk_DPTEN and ddpse.companycode = ds.companycode
			left join dim_date ddsd  on ddsd.datevalue = ds.VTTK_DATEN  and ddsd.companycode = ds.companycode;

UPDATE fact_salesorderdelivery sod
SET sod.dim_currshipmentcompldateid = ifnull(tmp.dim_currshipmentcompldateid, 1)
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dim_currshipmentcompldateid <> ifnull(tmp.dim_currshipmentcompldateid, 1);

UPDATE fact_salesorderdelivery sod
SET sod.dim_plannedshipmentcompldateid = ifnull(tmp.dim_plannedshipmentcompldateid, 1)
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dim_plannedshipmentcompldateid <> ifnull(tmp.dim_plannedshipmentcompldateid, 1);

UPDATE fact_salesorderdelivery sod
SET sod.dim_plannedshipmentenddateid = ifnull(tmp.dim_plannedshipmentenddateid, 1)
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dim_plannedshipmentenddateid <> ifnull(tmp.dim_plannedshipmentenddateid, 1);

UPDATE fact_salesorderdelivery sod
SET sod.dim_shipmentdateid = ifnull(tmp.dim_shipmentdateid, 1)
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dim_shipmentdateid <> ifnull(tmp.dim_shipmentdateid, 1);

UPDATE fact_salesorderdelivery sod
SET sod.dd_serviceagent = ifnull(tmp.dd_serviceagent, 'Not Set')
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dd_serviceagent <> ifnull(tmp.dd_serviceagent, 'Not Set');

UPDATE fact_salesorderdelivery sod
SET sod.dd_voyfltnum = ifnull(tmp.dd_voyfltnum, 'Not Set')
FROM tmp_vttk_fields tmp, fact_salesorderdelivery sod
WHERE sod.dd_ShipmentNumber = tmp.dd_ShipmentNumber
      AND sod.dd_SalesDlvrDocNo = tmp.dd_SalesDlvrDocNo
      AND sod.dd_voyfltnum <> ifnull(tmp.dd_voyfltnum, 'Not Set');

DROP TABLE IF EXISTS tmp_vttk_fields;
/* CristianT 07 Aug 2015 End */
 /* Update sales order item from sales order done by Alex Manolache 12 12 2014*/

UPDATE fact_salesorderdelivery sod
SET sod.dd_Purchaseorderitem = ifnull(so.dd_Purchaseorderitem ,'Not Set')
FROM fact_salesorder so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
AND sod.dd_Purchaseorderitem <> ifnull(so.dd_Purchaseorderitem ,'Not Set');

/* Update dim_routeid  from sales order done by Alex D 15 12 2014*/
merge into fact_salesorderdelivery fact
using ( select sod.fact_salesorderdeliveryid,
			   ifnull(min(so.dim_routeid), 1) dim_routeid,
			   ifnull(min(so.amt_scheduletotal), 0) amt_scheduletotal
		FROM fact_salesorder so, fact_salesorderdelivery sod
		WHERE   sod.dd_SalesDocNo = so.dd_SalesDocNo
			AND sod.dd_SalesItemNo = so.dd_SalesItemNo
			AND sod.dd_ScheduleNo = so.dd_ScheduleNo
		group by fact_salesorderdeliveryid
	   ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.dim_routeid = ifnull(src.dim_routeid, 1),
	                         fact.amt_scheduletotal = ifnull(src.amt_scheduletotal, 0);


/*   PM Delivery document */

DROP TABLE IF EXISTS tmp_SalesDlvrDocNo;
CREATE TABLE tmp_SalesDlvrDocNo as
SELECT left (GROUP_CONCAT( distinct dd_SalesDlvrDocNo),54)  dd_SalesDlvrDocNo,f.dd_SalesDocNo,f.dd_SalesItemNo
FROM fact_salesorderdelivery f
GROUP BY f.dd_SalesDocNo,f.dd_SalesItemNo;


UPDATE fact_salesorder so
SET so.dd_SalesDlvrDocNo = f.dd_SalesDlvrDocNo
FROM tmp_SalesDlvrDocNo f , fact_salesorder so
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
AND  f.dd_SalesItemNo = so.dd_SalesItemNo;


DROP TABLE IF EXISTS tmp_SalesDlvrDocNo;
Drop table if exists flag_holder_722;
Drop table if exists cursor_table1_722;
Drop table if exists update_tbl_722;
Drop table if exists loop_tbl_722;
Drop table if exists update_so_shipment_001;
Drop table if exists tmp_fact_sodf_LIKP_LIPS;


/* Marius Shipped against Order Qty - Moved here from vw_bi_populate_so_shippedAgainstOrderQty.sql */


drop table if exists tmp_so_002;
create table tmp_so_002 as
select distinct
	f.dd_SalesDocNo,
	f.dd_SalesItemNo,
	f.dd_ScheduleNo,
	PGI.DateValue GI_Date,
	f.ct_ScheduleQtySalesUnit SchedQty,
	p.PlantCode
from fact_Salesorder f
inner join fact_SalesOrderDelivery sd on f.dd_SalesDocNo = sd.dd_SalesDocNo and f.dd_SalesItemNo = sd.dd_SalesItemNo
/* inner join likp_lips l on l.likp_vbeln = sd.dd_SalesDlvrDocNo and l.lips_posnr = sd.dd_Salesdlvritemno - TEMPORARY COMMENTED TO FIX VALUES */
inner join dim_salesorderitemstatus sois on sd.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
inner join dim_Date PGI on PGI.Dim_DateId = f.Dim_DateidGoodsIssue
inner join Dim_Plant p on p.Dim_Plantid = f.Dim_Plantid
where f.ct_ScheduleQtySalesUnit > 0
	and f.dd_ItemRelForDelv = 'X'
	and sois.GoodsMovementStatus <> 'Not yet processed';

drop table if exists tmp_so_001;
create table tmp_so_001 as
select f.dd_SalesDocNo,
	f.dd_SalesItemNo,
	f.dd_ScheduleNo,
	f.GI_Date,
	f.SchedQty,
	f.PlantCode,
	ROW_NUMBER() OVER(PARTITION BY f.dd_SalesDocNo, f.dd_SalesItemNo
			  ORDER BY f.GI_Date, f.dd_ScheduleNo) RowSeqNo
from tmp_so_002 f;


drop table if exists tmp_sodlvr_001;
create table tmp_sodlvr_001 as
select fd.dd_SalesDocNo,
	fd.dd_SalesItemNo,
	sum(fd.ct_QtyDelivered) TotQtyDelivered
from fact_salesorderdelivery fd
where exists (select 1 from tmp_so_001 a
		where a.dd_SalesDocNo = fd.dd_SalesDocNo
			and a.dd_SalesItemNo = fd.dd_SalesItemNo)
group by fd.dd_SalesDocNo, fd.dd_SalesItemNo;


drop table if exists tmp_socummqty_001;
create table tmp_socummqty_001 as
select VBLB_VBELN c_SalesDocNo,
	VBLB_POSNR c_SalesItemNo,
	max(VBLB_ABEFZ) c_TotalCmlRcvdQty
from VBLB
group by VBLB_VBELN, VBLB_POSNR;


drop table if exists tmp_so_maxseqno_001;
create table tmp_so_maxseqno_001 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	max(a.RowSeqNo) MaxRowSeqNo
from tmp_so_001 a
group by a.dd_SalesDocNo,
	a.dd_SalesItemNo;


drop table if exists tmp_so_002;
create table tmp_so_002 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo,
	sum(b.SchedQty) RunnSchedQty
from tmp_so_001 a
	inner join tmp_so_001 b
	on (a.dd_SalesDocNo = b.dd_SalesDocNo
		and a.dd_SalesItemNo = b.dd_SalesItemNo)
where a.RowSeqNo >= b.RowSeqNo
group by a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo;


drop table if exists tmp_so_001;
create table tmp_so_001 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	a.GI_Date,
	a.SchedQty,
	a.PlantCode,
	a.RowSeqNo,
	a.RunnSchedQty,
	b.TotQtyDelivered,
	ifnull(c.c_TotalCmlRcvdQty,0) TotalCmlRcvdQty,
	m.MaxRowSeqNo
from tmp_so_002 a
	inner join tmp_so_maxseqno_001 m on a.dd_SalesDocNo = m.dd_SalesDocNo and a.dd_SalesItemNo = m.dd_SalesItemNo
	inner join tmp_sodlvr_001 b on a.dd_SalesDocNo = b.dd_SalesDocNo and a.dd_SalesItemNo = b.dd_SalesItemNo
	left join tmp_socummqty_001 c on a.dd_SalesDocNo = c.c_SalesDocNo and a.dd_SalesItemNo = c.c_SalesItemNo;


drop table if exists tmp_so_002;
create table tmp_so_002 as
select a.dd_SalesDocNo,
	a.dd_SalesItemNo,
	a.dd_ScheduleNo,
	case when a.TotQtyDelivered > (a.RunnSchedQty - a.SchedQty)
		then case when a.TotQtyDelivered >= a.RunnSchedQty and a.RowSeqNo < a.MaxRowSeqNo
			then a.SchedQty
			else a.TotQtyDelivered - (a.RunnSchedQty - a.SchedQty)
		     end
	     else 0
	end ShippedAgnstOrderQty,
	case when a.TotalCmlRcvdQty > (a.RunnSchedQty - a.SchedQty)
		then case when a.TotalCmlRcvdQty >= a.RunnSchedQty and a.RowSeqNo < a.MaxRowSeqNo
			then a.SchedQty
			else a.TotalCmlRcvdQty - (a.RunnSchedQty - a.SchedQty)
		     end
	     else 0
	end CmlQtyReceived
from tmp_so_001 a;


UPDATE fact_Salesorder f
      SET f.ct_ShippedAgnstOrderQty = ifnull(a.ShippedAgnstOrderQty, 0),
          f.ct_CmlQtyReceived = ifnull(a.CmlQtyReceived, 0)
FROM tmp_so_002 a, fact_Salesorder f
    WHERE     f.dd_SalesDocNo = a.dd_SalesDocNo
          AND f.dd_SalesItemNo = a.dd_SalesItemNo
          AND f.dd_ScheduleNo = a.dd_ScheduleNo;


drop table if exists tmp_so_001;
drop table if exists tmp_sodlvr_001;
drop table if exists tmp_socummqty_001;
drop table if exists tmp_so_maxseqno_001;
drop table if exists tmp_so_002;

/* END Marius Shipped against Order Qty - Moved here from vw_bi_populate_so_shippedAgainstOrderQty.sql */


/* 6 Feb 2016 Roxana - 	BI-4683 - add new id for Sales Order in order to populate shipment type */

drop table if exists  tmp_itemstatus;
create table tmp_itemstatus as
select dd_SalesDocNo, dd_SalesItemNo, max(dim_deliveryitemstatusid) dim_deliveryitemstatusid from fact_salesorderdelivery
group by dd_SalesDocNo,dd_SalesItemNo;

MERGE INTO fact_salesorder so
USING tmp_itemstatus sod ON so.dd_SalesDocNo = sod.dd_SalesDocNo and so.dd_SalesItemNo = sod.dd_SalesItemNo
WHEN MATCHED THEN UPDATE SET so.dim_deliveryitemstatusid = ifnull(sod.dim_deliveryitemstatusid, 1);


/* 8 Feb 2016 Roxana - Add Total Weight field */

UPDATE fact_salesorderdelivery sod
SET ct_totalweight = ifnull(LIKP_BTGEW,0)
FROM fact_salesorderdelivery sod, LIKP_LIPS a
WHERE dd_SalesDlvrDocNo = LIKP_VBELN
	and dd_SalesDlvrItemNo = LIPS_POSNR;


/* 10 Feb 2016 Roxana - Add Sales UOM */

drop table if exists tmp_salesuom;
create table tmp_salesuom as select distinct dd_SalesDocNo,dd_SalesItemNo,max(Dim_SalesUoMid) Dim_SalesUoMid
from fact_salesorder
group by dd_SalesDocNo,dd_SalesItemNo;

UPDATE fact_salesorderdelivery sod
SET sod.Dim_SalesUoMid  = ifnull(so.Dim_SalesUoMid, 1)
FROM tmp_salesuom so, fact_salesorderdelivery sod
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
	AND sod.dd_SalesItemNo = so.dd_SalesItemNo;

/* Marius LEAN OTIF */

update fact_salesorderdelivery f
set f.dim_deliverysalesorg = ifnull(so.Dim_SalesOrgid, 1)
from fact_salesorderdelivery f, Dim_SalesOrg so,LIKP_LIPS l
where f.dd_SalesDlvrDocNo = l.LIKP_VBELN and f.dd_SalesDlvrItemNo = l.LIPS_POSNR and LIKP_VKORG = so.SalesOrgCode
	and f.dim_deliverysalesorg <> ifnull(so.Dim_SalesOrgid, 1);

update fact_salesorderdelivery f
set f.dim_deliverydivision = ifnull(sd.Dim_SalesDivisionid, 1)
from fact_salesorderdelivery f, Dim_SalesDivision sd ,LIKP_LIPS l
where f.dd_SalesDlvrDocNo = l.LIKP_VBELN and f.dd_SalesDlvrItemNo = l.LIPS_POSNR and LIPS_SPART = sd.DivisionCode
	and f.dim_deliverydivision <> ifnull(sd.Dim_SalesDivisionid, 1);

update fact_salesorderdelivery sod
set sod.dim_salesdistrchannel = ifnull(so.Dim_DistributionChannelId, 1)
from fact_salesorderdelivery sod, fact_salesorder so
WHERE sod.dd_SalesDocNo = so.dd_SalesDocNo
AND sod.dd_SalesItemNo = so.dd_SalesItemNo
AND sod.dd_ScheduleNo = so.dd_ScheduleNo
and sod.dim_salesdistrchannel <> ifnull(so.Dim_DistributionChannelId, 1);

update fact_salesorderdelivery f
set f.dim_delivdatepod = ifnull(d.dim_dateid, 1)
from fact_salesorderdelivery f, LIKP_LIPS l, dim_company c, dim_date d
where f.dd_SalesDlvrDocNo = l.LIKP_VBELN
	and f.dd_SalesDlvrItemNo = l.LIPS_POSNR
	and f.dim_companyid = c.dim_companyid
	and d.companycode = c.companycode
	and d.datevalue = l.LIKP_PODAT
	and f.dim_delivdatepod <> ifnull(d.dim_dateid, 1);

update fact_salesorderdelivery f
set f.dim_bwproducthierarchyid = ifnull(bw.dim_bwproducthierarchyid, 1)
from fact_salesorderdelivery f, dim_part dp, dim_bwproducthierarchy bw
where
f.dim_partid = dp.dim_partid
and dp.producthierarchy = bw.lowerhierarchycode
and dp.productgroupsbu = bw.upperhierarchycode
and to_date('2018-12-28') between bw.upperhierstartdate and bw.upperhierenddate
and f.dim_bwproducthierarchyid <> ifnull(bw.dim_bwproducthierarchyid, 1);

update fact_salesorderdelivery f
set f.dim_mdg_partid = ifnull(m.dim_mdg_partid, 1)
from fact_salesorderdelivery f, dim_part p, dim_mdg_part m
where p.dim_partid = f.dim_partid and m.partnumber = p.partnumber and f.dim_mdg_partid <> ifnull(m.dim_mdg_partid, 1);

drop table if exists tmp_leanotifcalc;
create table tmp_leanotifcalc
as
select dd_SalesDlvrDocNo
,min(a.datevalue)dateidscheddeliveryreq
,min(b.datevalue)dateiddeliverydate
,min(c.datevalue)delivdatepod
,sum(ct_QtyDelivered) QtyDelivered
,sum(ct_ConfirmedQty) ConfirmedQty
,sum(ct_ScheduleQtySalesUnit) ScheduleQtySalesUnit
,min(fact_salesorderdeliveryid)min_fact_salesorderdeliveryid
from fact_salesorderdelivery f
	inner join dim_date a on f.dim_dateidscheddeliveryreq = a.dim_dateid
	inner join dim_date b on f.dim_dateiddeliverydate = b.dim_dateid
	inner join dim_date c on f.dim_delivdatepod = c.dim_dateid
  inner join dim_salesdocumenttype d on f.dim_salesdocumenttypeid = d.Dim_SalesDocumentTypeid
  inner join dim_documentcategory e on f.dim_documentcategoryid = e.dim_documentcategoryid
  inner join dim_salesorderitemcategory g on f.dim_salesorderitemcategoryid = g.dim_salesorderitemcategoryid
where dd_SalesDlvrDocNo <> 'Not Set' and c.datevalue <> '0001-01-01'
  and d.documenttype not in ('YKB','YCRV','YDRV','YRK','YKA')
  and e.documentcategory not in ('B','G','H')
  and g.salesorderitemcategory not in ('YLWT','YGWT')
group by dd_SalesDlvrDocNo;

/* APP-8320 Add BWCountry Hierarchy BogdanS - 20180216 - Start */
update fact_salesorderdelivery f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_salesorderdelivery f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-32')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY04' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_salesorderdelivery f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_salesorderdelivery f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-31','DIV-35','DIV-34')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY05' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_salesorderdelivery f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_salesorderdelivery f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-61','DIV-62','DIV-63','DIV-85','DIV-86','DIV-88')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY07' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;

update fact_salesorderdelivery f
	set f.dim_bwhierarchycountryid = ifnull(dim_con.dim_bwhierarchycountryid, 1)
from fact_salesorderdelivery f, dim_bwhierarchycountry dim_con,dim_bwproducthierarchy dim_prd, dim_plant dp
where dim_prd.dim_bwproducthierarchyid = f.dim_bwproducthierarchyid
	and    dim_prd.business in ('DIV-87')
	and dim_con.INTERNALHIERARCHYNAME = '0COUNTRY08' and dim_con.validto = '9999-12-31'
	and    dp.dim_plantid = f.dim_plantid
	and    dp.country = dim_con.nodehierarchynamelvl7;
/* APP-8320 Add BWCountry Hierarchy BogdanS - 20180216 - End */

/*Ana R - APP - 9658 - Shimpment Volumes - 20180525 Start */

drop table if exists tmp_shipmentmeasures ;

create table tmp_shipmentmeasures 
as 
select  sum(ifnull(src.vfkn_netwr,0)) as amt_freightcostdelivnote 
, ifnull(src.vfkp_waers, 'Not Set') as dd_ccyfreightcost 
, ifnull(lkp.likp_inco2, 'Not Set') as dd_incodelivery2 
, ifnull(lkp.likp_ntgew,0) as ct_netweightdelivnote 
, ifnull(lkp.lips_ntgew,0) as ct_netweightitem 
, ifnull(lkp.lips_brgew,0) as ct_grossweightitem 
, ifnull(lkp.lips_gewei, 'Not Set') as dd_weightunit 
, ifnull(lkp.likp_gewei, 'Not Set') as dd_weightdelivnoteunit 
, ifnull(lkp.lips_voleh, 'Not Set') as dd_volumeunit 
, ifnull(lkp.likp_voleh, 'Not Set') as dd_volumedelivnoteunit 
, lkp.likp_vbeln
, lkp.lips_posnr
, v.vttk_tknum 
, v.vttp_tpnum 
, v.vttp_vbeln 
, ifnull(lkp.lips_volum,0) as ct_volumeitem 
, ifnull(lkp.likp_volum,0) as ct_volumedelivnote  
, f.fact_salesorderdeliveryid

from likp_lips lkp 
inner join fact_salesorderdelivery f
on  f.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
 AND f.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
inner join (select max(VTTK_TKNUM) VTTK_TKNUM , VTTK_VSART,vttp_tpnum, VTTP_VBELN 
            from VTTK_VTTP
            group by VTTK_VSART,vttp_tpnum, VTTP_VBELN ) v 
on lkp.LIKP_VBELN = v.VTTP_VBELN 
and v.VTTK_TKNUM = f.dd_ShipmentNumber
left outer join ( select vfkn.*, vfkp.* 
                  from vfkn 
                  inner join vfkp 
                   on vfkn.vfkn_fknum = vfkp.vfkp_fknum 
                   and vfkn.vfkn_fkpos = vfkp.vfkp_fkpos ) src 
on lkp.likp_vbeln = src.vfkn_rebel 
and lkp.lips_posnr = src.vfkn_repos  
group by src.vfkp_waers , lkp.likp_inco2, lkp.likp_ntgew 
, lkp.lips_ntgew, lkp.lips_brgew , lkp.lips_gewei, lkp.lips_voleh 
, lkp.likp_voleh , v.vttk_tknum, v.vttp_tpnum , lkp.lips_volum 
, lkp.likp_volum, lkp.likp_gewei, v.vttp_vbeln 
, lkp.likp_vbeln, lkp.lips_posnr, f.fact_salesorderdeliveryid
;


update fact_salesorderdelivery f
set  dd_incodelivery2 = tmp.dd_incodelivery2
  , dd_weightunit = tmp.dd_weightdelivnoteunit
  , ct_netweightdelivnote = tmp.ct_netweightdelivnote
  , ct_netweightitem = tmp.ct_netweightitem
  , ct_grossweightitem = tmp.ct_grossweightitem
  , dd_volumeunit = tmp.dd_volumedelivnoteunit 
  , ct_volumeitem = tmp.ct_volumeitem
  , ct_volumedelivnote = tmp.ct_volumedelivnote
  , amt_freightcostdelivnote = tmp.amt_freightcostdelivnote
  , dd_ccyfreightcost = tmp.dd_ccyfreightcost

from fact_salesorderdelivery f, tmp_shipmentmeasures tmp
where f.fact_salesorderdeliveryid = tmp.fact_salesorderdeliveryid 
;

update fact_salesorderdelivery sod
set sod.dd_ShippingTypeDesc = IFNULL(v.T173T_BEZEI, 'Not Set')
FROM fact_salesorderdelivery sod
		inner join LIKP_LIPS l on    sod.dd_SalesDlvrDocNo = l.LIKP_VBELN
  								 AND sod.dd_SalesDlvrItemNo = l.LIPS_POSNR
	    inner join (select VTTK_TKNUM, VTTK_VSART, VTTP_VBELN, T173T_BEZEI  
                      from VTTK_VTTP vt
                      inner join T173T t
                      on vt.VTTK_VSART = t.T173T_VSART) v on l.LIKP_VBELN = v.VTTP_VBELN and v.VTTK_TKNUM = sod.dd_ShipmentNumber
where sod.dd_ShippingTypeDesc <> v.T173T_BEZEI;


/*Ana R - APP - 9658 - Shimpment Volumes - 20180525 End */

/*Populate Bill to Party - APP 8094 - Bogdan S - 21Nov2017 -  Start*/
DROP TABLE IF EXISTS TMP_UPD_DELIVERY_SO;
CREATE TABLE TMP_UPD_DELIVERY_SO
AS
SELECT fso.dd_SalesDocNo,fso.dd_SalesItemNo,max(fso.Dim_BillToPartyPartnerFunctionId) Dim_BillToPartyPartnerFunctionId
FROM fact_salesorder fso
GROUP BY fso.dd_SalesDocNo, fso.dd_SalesItemNo;

UPDATE fact_salesorderdelivery fsd
SET fsd.Dim_BillToPartyPartnerFunctionId = ifnull(fso.Dim_BillToPartyPartnerFunctionId, 1)
FROM TMP_UPD_DELIVERY_SO fso, fact_salesorderdelivery fsd
WHERE fsd.dd_salesdocno = fso.dd_SalesDocNo
   AND fsd.dd_salesitemno = fso.dd_SalesItemNo
   AND fsd.Dim_BillToPartyPartnerFunctionId <> ifnull(fso.Dim_BillToPartyPartnerFunctionId, 1);
/*Populate Bill to Party - APP 8094 - Bogdan S - 21Nov2017 - End*/

/* APP-7957 New Demand Flags - Oana 21Nov2017 */
drop table if exists tmp_demandflags;
create table tmp_demandflags as
select distinct fso.dd_salesdocno,
	fso.dd_salesitemno,
	fso.dd_scheduleno,
	fso.dd_cancelleddemand_flag,
	fso.dd_mtddemand_flag
from fact_salesorder fso, fact_salesorderdelivery fsod
where fso.dd_salesdocno = fsod.dd_salesdocno
	and fso.dd_salesitemno = fsod.dd_salesitemno
	and fso.dd_scheduleno = fsod.dd_scheduleno;

update fact_salesorderdelivery fsod
	set fsod.dd_cancelleddemand_flag = ifnull(t.dd_cancelleddemand_flag,'Not Set'),
		fsod.dd_mtddemand_flag = ifnull(t.dd_mtddemand_flag,'Not Set')
from fact_salesorderdelivery fsod, tmp_demandflags t
where fsod.dd_salesdocno = t.dd_salesdocno
	and fsod.dd_salesitemno = t.dd_salesitemno
	and fsod.dd_scheduleno = t.dd_scheduleno;

drop table if exists tmp_demandflags;
/* END APP-7957 New Demand Flags - Oana 21Nov2017 */
/* APP-9597 Cristian Cleciu 14MAY2018 */
UPDATE fact_salesorderdelivery f SET f.dim_mtddemandid = d.dim_mtddemandid FROM fact_salesorderdelivery f, dim_mtddemand d WHERE f.DD_MTDDEMAND_FLAG =d.mtddemand_flag AND f.dim_mtddemandid <> d.dim_mtddemandid;
/* END APP-9597 Cristian Cleciu 14MAY2018 */
/* Roxana H - 11 Dec 2017 - Add a timestamp of the data load */
drop table if exists tmp_updatedate_deliv;
create table tmp_updatedate_deliv as
select max(dw_update_date) dd_updatedate from  fact_salesorderdelivery;

update fact_salesorderdelivery f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_salesorderdelivery f, tmp_updatedate_deliv t
where f.dd_updatedate <> t.dd_updatedate;

/* Bogdan S - 16 Jan 2018 - Add a timestamp of the data load for SalesOrder- Start*/
update fact_salesorder f
set f.dd_updatedate = ifnull(t.dd_updatedate, '0001-01-01 00:00:00.000000')
from fact_salesorder f, tmp_updatedate_deliv t
where f.dd_updatedate <> t.dd_updatedate;
/* Bogdan S - 16 Jan 2018 - Add a timestamp of the data load for SalesOrder- End*/



/*Make dd_batchNo dimension - from Delivery, InvetoryAging and Inventory history Roxana D 2018-01-17*/

delete from number_fountain m where m.table_name = 'dim_batchno';

insert into number_fountain
select 	'dim_batchno',
	ifnull(max(d.dim_batchnoid),1)
from EMD586.dim_batchno d;

INSERT INTO EMD586.DIM_BATCHNO (DIM_BATCHNOID, BATCHNO, PROJECTSOURCEID, DW_INSERT_DATE, DW_UPDATE_DATE)
SELECT (select ifnull(m.max_id, 1) from number_fountain m where m.table_name = 'dim_batchno')
          +  row_number() over(order by ''),F.dd_batch, 1, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP
FROM
(SELECT DISTINCT DD_BATCH FROM FACT_SALESORDERDELIVERY ) F
WHERE NOT EXISTS (SELECT 1 FROM EMD586.DIM_BATCHNO  D WHERE D.BATCHNO = F.DD_BATCH);

UPDATE FACT_SALESORDERDELIVERY F
SET F.DIM_BATCHNOID = IFNULL(D.DIM_BATCHNOID,1)
FROM FACT_SALESORDERDELIVERY F, EMD586.DIM_BATCHNO D
WHERE F.DD_BATCH = D.BATCHNO
AND F.DIM_BATCHNOID <> IFNULL(D.DIM_BATCHNOID,1);

/**/

/*Add batch expiration date - Roxana D 2018-01-23*/

DROP TABLE IF EXISTS tmp_for_DateidBatchExpiryDate_SOD;
CREATE TABLE tmp_for_DateidBatchExpiryDate_SOD as
SELECT DISTINCT dt1.dim_dateid,i.fact_salesorderdeliveryid, row_number() over (partition by fact_salesorderdeliveryid order by fact_salesorderdeliveryid) as row_num
FROM
fact_salesorderdelivery i
INNER JOIN mch1 m ON i.dd_Batch = m.mch1_charg
INNER JOIN dim_company dc ON i.dim_companyid = dc.dim_companyid
INNER JOIN dim_Part dp1 ON i.dim_Partid = dp1.dim_Partid AND dp1.partnumber = m.mch1_matnr
INNER JOIN emd586.dim_date dt1 ON dt1.CompanyCode = dc.CompanyCode AND dt1.datevalue = CASE WHEN m.mch1_vfdat > '2068-12-31' THEN '9999-12-31' ELSE m.mch1_vfdat END;



UPDATE fact_salesorderdelivery i
SET dim_DateidBatchExpiryDate = ifnull(dt1.dim_dateid, 1)
FROM fact_salesorderdelivery i,tmp_for_DateidBatchExpiryDate_SOD dt1
WHERE i.fact_salesorderdeliveryid=dt1.fact_salesorderdeliveryid
AND row_num=1
AND  dim_DateidBatchExpiryDate <> ifnull(dt1.dim_dateid, 1);

DROP TABLE IF EXISTS tmp_for_DateidBatchExpiryDate_SOD;
/**/

/* APP-7665 - Oana 25Jan2018 */
update fact_salesorderdelivery f
	set f.dim_salesorderheaderstatusid = ifnull(d.dim_salesorderheaderstatusid,1)
from fact_salesorderdelivery f, dim_salesorderheaderstatus d
where dd_SalesDlvrDocNo = SalesDocumentNumber;

update fact_salesorderdelivery f
	set f.Dim_DeliveryItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid, 1)
from fact_salesorderdelivery f
left join (select a.SalesDocumentNumber,a.SalesItemNumber,max(a.Dim_SalesOrderItemStatusid) Dim_SalesOrderItemStatusid from Dim_SalesOrderItemStatus a group by a.SalesDocumentNumber,a.SalesItemNumber) sois
on sois.SalesDocumentNumber = f.dd_SalesDlvrDocNo and sois.SalesItemNumber = f.dd_SalesDlvrItemNo
where f.Dim_DeliveryItemStatusid <> ifnull(sois.Dim_SalesOrderItemStatusid, 1);
/* END APP-7665 - Oana 25Jan2018 */

/*Roxana D 2018-02-28 - Add Billing Date Actual*/

merge into fact_salesorderdelivery fact
using (select fact_salesorderdeliveryid, max(so.Dim_DateidBillingActual) Dim_DateidBillingActual
	   from fact_salesorderdelivery sod
		   		inner join fact_salesorder so on    sod.dd_SalesDocNo = so.dd_SalesDocNo
												and sod.dd_SalesItemNo = so.dd_SalesItemNo
												and sod.dd_ScheduleNo = so.dd_ScheduleNo
	   group by  fact_salesorderdeliveryid
	  ) src on fact.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set fact.Dim_DateidBillingActual = ifnull(src.Dim_DateidBillingActual,1)
where fact.Dim_DateidBillingActual <> ifnull(src.Dim_DateidBillingActual,1);

/**/


/* Liviu Ionescu - APP - APP-9188 */
update fact_salesorderdelivery f
set f.dim_ClusterPlantid = 1;

update fact_salesorderdelivery f
set f.dim_ClusterPlantid = ifnull(dcp.dim_ClusterPlantid,1) -- select count(1)
from fact_salesorderdelivery f
	inner join dim_cluster dc on dc.dim_clusterid = f.dim_clusterid
	inner join (select max(DIM_CLUSTERID) DIM_CLUSTERID, CLUST from dim_cluster  group by CLUST) dcmax on dcmax.CLUST = dc.CLUST
	inner join dim_plant dp on dp.dim_plantid = f.dim_plantid
	inner join (select max(dim_ClusterPlantid) dim_ClusterPlantid, MerckLSCluster,
								ifnull(case when position(' ' in ltrim(rtrim(PlantEMD))) = 0 then ltrim(rtrim(PlantEMD)) else substring(ltrim(rtrim(PlantEMD)),1,position(' ' in ltrim(rtrim(PlantEMD))) -1) end,'Not Set') PlantEMD from dim_ClusterPlant 
			group by MerckLSCluster, ifnull(case when position(' ' in ltrim(rtrim(PlantEMD))) = 0 then ltrim(rtrim(PlantEMD)) else substring(ltrim(rtrim(PlantEMD)),1,position(' ' in ltrim(rtrim(PlantEMD))) -1) end,'Not Set')) dcp on lower(dp.PlantCode) = lower(dcp.PlantEMD)
								and lower(dcmax.Clust) = lower(dcp.MerckLSCluster)
								and f.dim_ClusterPlantid <> ifnull(dcp.dim_ClusterPlantid,1);

/* APP-9314 Oana 16April2018 */
merge into fact_salesorderdelivery f
using (select fsd.fact_salesorderdeliveryid, max(fso.dim_creditreleasedateid) dim_creditreleasedateid
	from fact_salesorderdelivery fsd, fact_salesorder fso
		where fso.dd_SalesDocNo = fsd.dd_SalesDocNo
		and fso.dd_SalesItemNo = fsd.dd_SalesItemNo
		and fso.dd_ScheduleNo = fsd.dd_ScheduleNo
	group by fsd.fact_salesorderdeliveryid) src on f.fact_salesorderdeliveryid = src.fact_salesorderdeliveryid
when matched then update set  f.dim_creditreleasedateid = src.dim_creditreleasedateid
where f.dim_creditreleasedateid <> src.dim_creditreleasedateid;

/* Ana Rusu - APP-9391 Add new calculated field dd_actualconfirmeddate  Start */
merge into fact_salesorder f
using (
select max(datevalue) as actualconfirmeddatetest, f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo 
from fact_salesorder f, dim_date d
where  f.dim_dateidscheddelivery = d.dim_dateid
and f.ct_confirmedqty > 0
group by f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo
) t
 on f.dd_SalesDocNo = t.dd_SalesDocNo
 and f.dd_SalesItemNo = t.dd_SalesItemNo
 and f.dd_ScheduleNo = t.dd_ScheduleNo
when matched then update
 set f.dd_actualconfirmeddate = t.actualconfirmeddatetest;
/* Ana Rusu - APP-9391 Add new calculated field dd_actualconfirmeddate  Start */

/* APP-9638 - Cristian Cleciu 12Jun2018 */
UPDATE FACT_SALESORDERDELIVERY f
SET f.DIM_MSPIFLAGID =s.DIM_MSPIFLAGID
FROM FACT_SALESORDERDELIVERY f, FACT_SALESORDER S
WHERE f.DD_SALESDOCNO = s.DD_SALESDOCNO
AND f.DD_SALESITEMNO = s.DD_SALESITEMNO
AND f.DD_SCHEDULENO = s.DD_SCHEDULENO;
/* END APP-9638 - Cristian Cleciu 12Jun2018 */

/* Alex APP-9690 14 June 2018 */
update fact_salesorderdelivery f
set f.dim_actualrouteid = ifnull(r.dim_routeid,1)
 from  fact_salesorderdelivery f,dim_route r, LIKP_LIPS l
where r.RouteCode  =  l.LIKP_ROUTE and r.RowIsCurrent  =  1
and f.DD_SALESDLVRDOCNO = l.LIKP_VBELN
and l.LIPS_POSNR = DD_SALESDLVRITEMNO;
/* APP-9916 Cristian Cleciu 29.06.2018 */
UPDATE FACT_SALESORDER f
SET f.DIM_WAVEFLAGID = CASE WHEN dp.COUNTRY IN ('GB','MX','AR','US','KR') OR LEFT(m.MSPI_FLAG,4) IN ('MSPI')
THEN 2
ELSE 3
END
FROM FACT_SALESORDER f, DIM_PLANT dp, DIM_MSPIFLAG m
WHERE
dp.DIM_PLANTID = f.DIM_PLANTID
AND m.DIM_MSPIFLAGID = f.DIM_MSPIFLAGID
and f.DIM_WAVEFLAGID <> CASE WHEN dp.COUNTRY IN ('GB','MX','AR','US','KR') OR LEFT(m.MSPI_FLAG,4) IN ('MSPI')
THEN 2
ELSE 3
END;
/* END APP-9916 Cristian Cleciu 29.06.2018 */