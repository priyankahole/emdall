<xsl:variable name="AllUIBlocks" select="InteractionNode/UIBlock"/>
<xsl:variable name="Properties" select="InteractionNode/Properties"/>

<xsl:template name="generate_pagetitle">
<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
<TR>
	<TD HEIGHT="26px" VALIGN="center" ALIGN="left">
		<xsl:attribute name="BGCOLOR"><xsl:value-of select="$Properties/captionbgcolor"/></xsl:attribute>	
		<SPAN STYLE="font: bold 18px Arial,Verdana,sans-serif;">
		<FONT><xsl:attribute name="COLOR"><xsl:value-of select="$Properties/captioncolor"/></xsl:attribute>
		<xsl:value-of select="$Properties/caption"/></FONT></SPAN>
	</TD>
</TR>
</TABLE>
</xsl:template>

<xsl:template name="generate_components">
	<xsl:param name="group"/>
	<xsl:if test="string-length($AllUIBlocks/Component[@type=1])>0">
		<DIV id='popCal' onclick='event.cancelBubble=true' style='POSITION:absolute;visibility:hidden;border:2px ridge;z-index:100;width:10'>
			<iframe name="popFrame" src="/template/calendar.htm" frameborder="0" scrolling="no" width="183" height="188"></iframe>
		</DIV>
	</xsl:if>
	<TABLE>
		<xsl:for-each select="$AllUIBlocks">
		<xsl:for-each select="./Component">
			<xsl:apply-templates select=".">
				<xsl:with-param name="group" select="$group"/>
			</xsl:apply-templates>
		</xsl:for-each>
		</xsl:for-each>
	</TABLE>
</xsl:template>

<xsl:template name="generate_menu">
<xsl:param name="group"/>
<TABLE cellpadding="1" cellspacing="5"><TR>
	<xsl:if test="not($Properties/backbuttonvisible='false')">
		<TD id="back" align="center" valign="middle"> 
		<INPUT style="cursor:hand;height:25;width:80;font-weight:bold" value="Back" ONCLICK="javascript:DisableMenu(); goBack();" type="button"/></TD>
	</xsl:if>
	<xsl:for-each select="$AllUIBlocks">
	<xsl:for-each select="./Component">
		<xsl:apply-templates select=".">
			<xsl:with-param name="menu_exit" select="'true'"/>
			<xsl:with-param name="group" select="$group"/>
		</xsl:apply-templates>
	</xsl:for-each>
	</xsl:for-each>
	<xsl:if test="not($Properties/nextbuttonvisible='false')">
		<TD id="next" align="center" valign="middle"> 
		<INPUT style="cursor:hand;height:25;width:80;font-weight:bold" value="Next" ONCLICK="javascript:DisableMenu(); RequiredFldChk();" type="button"/></TD>
	</xsl:if>
</TR></TABLE>
</xsl:template>

<xsl:template name="generate_helptext">
<xsl:param name="group"/>
	<DIV><xsl:attribute name="id">GroupHelp_<xsl:value-of select="$group"/></xsl:attribute></DIV>
	<xsl:for-each select="$AllUIBlocks">
	<xsl:for-each select="./Component">
		<xsl:choose>
			<xsl:when test="string-length($group)>0">
				<xsl:if test="./CompProperties/layer=$group">
					<DIV style="display:none;"><xsl:attribute name="id">HELP_<xsl:value-of select="./@id"/></xsl:attribute>
						<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
						<xsl:apply-templates select="./CompProperties/helptext"/></SPAN>
					</DIV>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<DIV style="display:none;"><xsl:attribute name="id">HELP_<xsl:value-of select="./@id"/></xsl:attribute>
					<SPAN><xsl:attribute name="STYLE">display:none;<xsl:call-template name="StylingValue"/></xsl:attribute>
					<xsl:apply-templates select="./CompProperties/helptext"/></SPAN>
				</DIV>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<xsl:template match="Component"> 
<xsl:param name="menu_exit"/>
<xsl:param name="group"/>
<xsl:variable name="CompType" select="./CompProperties/component"/>
<xsl:choose>
	<xsl:when test="$menu_exit='true'">
	<xsl:if test="@type='0' and (($CompType='Exit') or ($CompType='MenuItem'))">
		<xsl:choose>
			<xsl:when test="string-length($group)>0">
				<xsl:if test="./CompProperties/layer=$group">
					<xsl:call-template name="MenuItem_0"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="MenuItem_0"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
	</xsl:when>
	<xsl:otherwise>
		<xsl:if test="not($CompType='Exit' or $CompType='MenuItem')">
			<xsl:choose>
			<xsl:when test="string-length($group)>0">
				<xsl:if test="./CompProperties/layer=$group">
					<xsl:call-template name="DisplayComponents"/>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="DisplayComponents"/>
			</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="DisplayComponents"> 
<TR><xsl:attribute name="ID">COMPTR<xsl:value-of select="./@id"/></xsl:attribute>
<xsl:attribute name="onmouseover">javascript: showHelpText('<xsl:value-of select="./@id"/>','<xsl:value-of select="./layer"/>');</xsl:attribute>
<xsl:if test="CompProperties[visible='false']">
	<xsl:attribute name="style">display:none</xsl:attribute>
</xsl:if>
<xsl:variable name="CompType" select="CompProperties/component"/>
<xsl:variable name="Type" select="@type"/>
<xsl:choose>
<xsl:when test="$Type='0'">
	<xsl:choose>
		<xsl:when test="$CompType='Image'">
		<xsl:call-template name="Image_0"/>
		</xsl:when>
		<xsl:when test="$CompType='CheckBox'">
		<xsl:call-template name="CheckBox_0"/>
		</xsl:when>
		<xsl:when test="$CompType='Anchor'">
		<xsl:call-template name="Anchor_0"/>
		</xsl:when>
		<xsl:when test="$CompType='Button'">
		<xsl:call-template name="Button_0"/>
		</xsl:when>
		<xsl:when test="$CompType='ComboBox'">
		<xsl:call-template name="ComboBox_0"/>
		</xsl:when>
		<xsl:when test="$CompType='RadioGroup'">
		<xsl:call-template name="RadioGroup_0"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>

<xsl:when test="$Type='1'">
	<xsl:call-template name="DateTime_1"/>
</xsl:when>

<xsl:when test="$Type='2'">
	<xsl:call-template name="DateTime_1"/>
</xsl:when>

<xsl:when test="$Type='3'">
	<xsl:choose>
		<xsl:when test="$CompType='Label'">
		<xsl:call-template name="Label_3"/>
		</xsl:when>
		<xsl:when test="$CompType='TextBox'">
		<xsl:call-template name="TextBox_3"/>
		</xsl:when>
		<xsl:when test="$CompType='TextArea'">
		<xsl:call-template name="TextArea_3"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>

<xsl:when test="$Type='4'">
	<xsl:call-template name="TextBox_3"/>
</xsl:when>

<xsl:when test="$Type='5'">
	<xsl:choose>
		<xsl:when test="$CompType='RadioGroup'">
		<xsl:call-template name="RadioGroup_5"/>
		</xsl:when>
		<xsl:when test="$CompType='Button'">
		<xsl:call-template name="Button_5"/>
		</xsl:when>
		<xsl:when test="$CompType='Anchor'">
		<xsl:call-template name="AnchorDisplayGrid_5"/>
		</xsl:when>
		<xsl:when test="$CompType='DisplayGrid'">
		<xsl:call-template name="AnchorDisplayGrid_5"/>
		</xsl:when>
		<xsl:when test="$CompType='ComboBox'">
		<xsl:call-template name="ComboBox_5"/>
		</xsl:when>
		<xsl:when test="$CompType='ListBox'">
		<xsl:call-template name="ListBox_5"/>
		</xsl:when>
		<xsl:when test="$CompType='Tree'">
		<xsl:call-template name="Tree_5"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>
</xsl:choose>
</TR>
</xsl:template>

<xsl:template name="Image_0">
<xsl:choose>
	<xsl:when test="./CompProperties[enabled='false']">
		<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute>
		<IMG BORDER="0"><xsl:attribute name="src">/template/images/<xsl:value-of select="./CompProperties/caption"/>
		</xsl:attribute><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute></IMG>
		</TD>
	</xsl:when>
	<xsl:otherwise>
		<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute>
		<SPAN><xsl:attribute name="ONCLICK">
		javascript:if(!disableAnchor){DisableMenu();    { setVariableValue(<xsl:value-of select="./vid"/>,1); RequiredFldChk();}}
		</xsl:attribute><IMG BORDER="0">
		<xsl:attribute name="src">/template/images/<xsl:value-of select="./CompProperties/caption"/></xsl:attribute>
		<xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute></IMG>
		</SPAN></TD>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="CheckBox_0">
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<INPUT TYPE="checkbox">
		<xsl:if test="./vValue='true'"><xsl:attribute name="CHECKED"/></xsl:if>
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="ONCLICK">javascript:   setVariableValue(<xsl:value-of select="./vid"/>,(this.checked==true) ? 1:0); </xsl:attribute>
		</INPUT><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
		<xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption"/></SPAN>
	</TD>
</xsl:template>

<xsl:template name="Anchor_0">
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="ONCLICK">
		javascript:if(!disableAnchor){DisableMenu();  { setVariableValue(<xsl:value-of select="./vid"/>,1); RequiredFldChk();}}
		</xsl:attribute><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
		<SPAN STYLE="color:blue;text-decoration:underline;cursor:hand;"><xsl:value-of select="./CompProperties/caption"/></SPAN></SPAN>
	</TD>
</xsl:template>

<xsl:template name="Button_0">
	<TD colspan="2" valign="middle">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<INPUT type="button"><xsl:attribute name="id">MI<xsl:value-of select="./@id"/></xsl:attribute> 
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="ONCLICK">javascript:DisableMenu();  { setVariableValue(<xsl:value-of select="./vid"/>,1); RequiredFldChk();} 
		</xsl:attribute><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="./CompProperties/caption"/></xsl:attribute></INPUT>
	</TD>
</xsl:template>

<xsl:template name="ComboBox_0">
	<TD NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption"/>&nbsp;&nbsp;&nbsp;</SPAN>
	</TD>
	<TD valign="top"><SELECT SIZE="1"><xsl:attribute name="NAME"><xsl:value-of select="./vid"/></xsl:attribute>
		<xsl:attribute name="VALUE"><xsl:choose><xsl:when test="./vValue='true'">1</xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose></xsl:attribute>
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="ONCHANGE">javascript:    setVariableValue(<xsl:value-of select="./vid"/>,this.value);</xsl:attribute>
		<OPTION VALUE="1">Yes</OPTION>
		<OPTION VALUE="0">No</OPTION></SELECT>
	</TD>
</xsl:template>

<xsl:template name="RadioGroup_0">
<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<TD valign="top" colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption"/>&nbsp;&nbsp;&nbsp;</SPAN>
		<BR/><TABLE WIDTH="100%">
		<TR><TD valign="top" WIDTH="2%"><INPUT TYPE="radio"><xsl:attribute name="NAME">rdBtn<xsl:value-of select="./@id"/></xsl:attribute> 
			<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
			<xsl:attribute name="ONCLICK">javascript:    setVariableValue(<xsl:value-of select="./vid"/>,1);</xsl:attribute>
			<xsl:if test="./vValue='true'"><xsl:attribute name="CHECKED"/></xsl:if></INPUT>
		</TD><TD valign="top">
			<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute> Yes</SPAN></TD></TR>
		<TR><TD valign="top" WIDTH="2%"><INPUT TYPE="radio"><xsl:attribute name="NAME">rdBtn<xsl:value-of select="./@id"/></xsl:attribute> 
			<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
			<xsl:attribute name="ONCLICK">javascript:    setVariableValue(<xsl:value-of select="./vid"/>,0);</xsl:attribute>
			<xsl:if test="./vValue='false'"><xsl:attribute name="CHECKED"/></xsl:if></INPUT>
		</TD><TD valign="top">
			<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute> No</SPAN></TD></TR>
		</TABLE>
	</TD>
</xsl:template>

<xsl:template name="DateTime_1">
	<xsl:variable name="tmpDate">
		<xsl:value-of select="./vValue/day"/> <xsl:call-template name="month"/>, <xsl:value-of select="./vValue/year"/><xsl:text> </xsl:text>
		<xsl:if test="string-length(./vValue/hours)>0"> 
			<xsl:choose>
				<xsl:when test="./vValue/hours > 12">
					 <xsl:value-of select="./vValue/hours - 12"/>:<xsl:if test="string-length(./vValue/minutes)=1">0</xsl:if><xsl:value-of select="./vValue/minutes"/> PM
				</xsl:when>
				<xsl:otherwise>
					 <xsl:value-of select="./vValue/hours"/>:<xsl:if test="string-length(./vValue/minutes)=1">0</xsl:if><xsl:value-of select="./vValue/minutes"/> AM
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:variable> 
	
	<TD NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption"/>&nbsp;&nbsp;&nbsp;</SPAN></TD>
		<xsl:choose>
			<xsl:when test="not(./CompProperties[enabled='false'])">
				<TD><INPUT TYPE="text">
				<xsl:attribute name="id">D<xsl:value-of select="./@id"/></xsl:attribute> 
				<xsl:choose>
					<xsl:when test="string-length(./CompProperties/width)>0">
						<xsl:attribute name="SIZE"><xsl:value-of select="./CompProperties/width"/></xsl:attribute> 	
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="SIZE">18</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:attribute name="VALUE"><xsl:value-of select="$tmpDate"/></xsl:attribute> 
				<xsl:attribute name="MAXLENGTH"><xsl:value-of select="./CompProperties/maxlength"/></xsl:attribute> 
				<xsl:attribute name="NAME"><xsl:value-of select="./@id"/></xsl:attribute>
				<xsl:attribute name="ONCHANGE">javascript:   setVariableValue(<xsl:value-of select="./vid"/>,
					((new Date(this.value)).getMonth()+1)+'.'+(new Date(this.value)).getDate()+
					'.'+(new Date(this.value)).getFullYear()+' '+((new Date(this.value)).getHours().toString().length &gt; 1?'':'0')+
					(new Date(this.value)).getHours()+':'+((new Date(this.value)).getMinutes().toString().length &gt; 1?'':'0')+(new 
					Date(this.value)).getMinutes()+':'+((new Date(this.value)).getSeconds().toString().length &gt; 1?'':'0')+
					(new Date(this.value)).getSeconds());</xsl:attribute>
				</INPUT>
				<xsl:if test="./@type=1">
				<INPUT type="button" value="V">
				<xsl:attribute name="ONCLICK">javascript:popFrame.fPopCalendar(D<xsl:value-of select="./@id"/>,D<xsl:value-of select="./@id"/>,popCal,<xsl:value-of select="./vid"/>);return false;</xsl:attribute>
				</INPUT> </xsl:if></TD>
			</xsl:when>
			<xsl:otherwise>
				<TD valign="top"><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><B><xsl:value-of select="$tmpDate"/></B></SPAN></TD>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template name="Label_3">
	<TD valign="top" colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute>  
		<SPAN> <xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
		<xsl:if test="string-length(./CompProperties/caption)>0">
			<xsl:value-of select="./CompProperties/caption"/><BR/>
		</xsl:if><xsl:apply-templates select="./vValue"/></SPAN>
	</TD>
</xsl:template>

<xsl:template name="TextBox_3">
<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<TD NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption"/>&nbsp;&nbsp;&nbsp;</SPAN></TD>
		<xsl:choose>
			<xsl:when test="not(./CompProperties/enabled='false')">
				<TD><INPUT>
				<xsl:attribute name="TYPE">
				<xsl:choose>
					<xsl:when test="./CompProperties/password='true'">password</xsl:when>
					<xsl:otherwise>text</xsl:otherwise>
				</xsl:choose></xsl:attribute> 
				<xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute> 
				<xsl:choose>
					<xsl:when test="string-length(./CompProperties/width)>0">
						<xsl:attribute name="SIZE"><xsl:value-of select="./CompProperties/width"/></xsl:attribute> 	
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="SIZE">15</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:attribute name="VALUE"><xsl:if test="not(./CompProperties/password='true')"><xsl:value-of select="./vValue"/></xsl:if></xsl:attribute> 
				<xsl:attribute name="MAXLENGTH"><xsl:value-of select="./CompProperties/maxlength"/></xsl:attribute> 
				<xsl:attribute name="NAME"><xsl:value-of select="./@id"/></xsl:attribute> 
				<xsl:attribute name="ONCHANGE">
				<xsl:choose>
					<xsl:when test="./@type='4'">
						javascript:    setEditValue(<xsl:value-of select="./vid"/>,this.value,0<xsl:value-of select="./CompProperties/minvalue"/>,0<xsl:value-of select="./CompProperties/maxvalue"/>);
					</xsl:when>
					<xsl:otherwise>
						javascript:    setVariableValue(<xsl:value-of select="./vid"/>,this.value);
					</xsl:otherwise>
				</xsl:choose></xsl:attribute> 
				</INPUT>
				</TD>
			</xsl:when>
			<xsl:otherwise>
				<TD><SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><B><xsl:apply-templates select="./vValue"/></B></SPAN></TD>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template name="TextArea_3">
<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<TD valign="top" NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption"/>&nbsp;&nbsp;&nbsp;</SPAN></TD>
		<xsl:choose>
			<xsl:when test="not(./CompProperties/enabled='false')">
				<TD valign="top"><TEXTAREA>
					<xsl:choose>
						<xsl:when test="string-length(./CompProperties/width)>0">
							<xsl:attribute name="cols"><xsl:value-of select="./CompProperties/width"/></xsl:attribute> 
						</xsl:when>
						<xsl:otherwise>15</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="string-length(./CompProperties/multiline)>0">
							<xsl:attribute name="rows"><xsl:value-of select="./CompProperties/multiline"/></xsl:attribute> 
						</xsl:when>
						<xsl:otherwise>2</xsl:otherwise>
					</xsl:choose>
					<xsl:attribute name="VALUE"><xsl:apply-templates select="./vValue"/></xsl:attribute> 
					<xsl:attribute name="NAME"><xsl:value-of select="./@id"/></xsl:attribute> 
					<xsl:attribute name="ONCHANGE">javascript:    setVariableValue(<xsl:value-of select="./vid"/>,this.innerHTML);</xsl:attribute> 
					<xsl:apply-templates select="./vValue"/>
				</TEXTAREA></TD>
			</xsl:when>
			<xsl:otherwise>
				<TD valign="top"><SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><B><xsl:apply-templates select="./vValue"/></B></SPAN></TD>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template name="RadioGroup_5">
	<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<xsl:variable name="CompID"><xsl:value-of select="./@id"/></xsl:variable>
	<xsl:variable name="CompValue"><xsl:value-of select="./vValue"/></xsl:variable>
	<xsl:variable name="CompVID"><xsl:value-of select="./vid"/></xsl:variable>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if> <STRONG><xsl:value-of select="./CompProperties/caption"/></STRONG></SPAN><BR/>
		<TABLE WIDTH="100%">
		<xsl:variable name="Enable"><xsl:value-of select="./CompProperties/enabled"/></xsl:variable>
		<xsl:choose>
			<xsl:when test="./CompProperties/subitemposition='horizontal'">
				<TR><TD>
					<xsl:for-each select="./item"> 
						<INPUT TYPE="radio">
						<xsl:attribute name="NAME">rdBtn<xsl:value-of select="$CompID"/></xsl:attribute> 
						<xsl:if test="$Enable='false'"><xsl:attribute name="DISABLED"/></xsl:if>
						<xsl:attribute name="ONCLICK">javascript:    setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>);</xsl:attribute>
						<xsl:if test="$CompValue=./@itemno"><xsl:attribute name="CHECKED"/></xsl:if></INPUT>
						<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute>
						<xsl:value-of select="./name"/></SPAN><xsl:text></xsl:text>
					</xsl:for-each>
				</TD></TR>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="./item"> 
					<TR><TD WIDTH="2%">
					<INPUT TYPE="radio">
					<xsl:attribute name="NAME">rdBtn<xsl:value-of select="$CompID"/></xsl:attribute> 
					<xsl:if test="$Enable='false'"><xsl:attribute name="DISABLED"/></xsl:if>
					<xsl:attribute name="ONCLICK">javascript:    setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>);</xsl:attribute>
					<xsl:if test="$CompValue=./@itemno"><xsl:attribute name="CHECKED"/></xsl:if></INPUT></TD><TD><xsl:text> </xsl:text>
					<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:value-of select="./name"/></SPAN>
					</TD></TR>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		</TABLE>
	</TD>
</xsl:template>

<xsl:template name="Button_5">
	<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<xsl:variable name="CompVID"><xsl:value-of select="./vid"/></xsl:variable>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:value-of select="./CompProperties/caption"/></SPAN><BR/>
		<TABLE WIDTH="100%">
		<xsl:choose>
			<xsl:when test="./CompProperties/subitemposition='horizontal'">
				<TR><TD>
					<xsl:for-each select="./item"> 
						<INPUT TYPE="button">
						<xsl:if test="./CompProperties[enabled='false']"><xsl:attribute name="DISABLED"/></xsl:if>
						<xsl:attribute name="VALUE"><xsl:value-of select="./name"/></xsl:attribute> 
						<xsl:attribute name="ONCLICK">javascript:DisableMenu();   { setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>); RequiredFldChk();}</xsl:attribute> 
						<xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute> </INPUT>&nbsp;&nbsp;
					</xsl:for-each>
				</TD></TR>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="./item"> 
					<TR><TD>
					<INPUT TYPE="button">
					<xsl:if test="./CompProperties[enabled='false']"><xsl:attribute name="DISABLED"/></xsl:if>
					<xsl:attribute name="VALUE"><xsl:value-of select="./name"/></xsl:attribute> 
					<xsl:attribute name="ONCLICK">javascript:DisableMenu();   { setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>); RequiredFldChk();}</xsl:attribute> 
					<xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute></INPUT><xsl:text>   </xsl:text>
					</TD></TR>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		</TABLE>
	</TD>
</xsl:template>

<xsl:template name="AnchorDisplayGrid_5">
	<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<xsl:variable name="CompVID"><xsl:value-of select="./vid"/></xsl:variable>
	<xsl:variable name="CompNo"><xsl:value-of select="./@id"/></xsl:variable>
	<xsl:variable name="Folder"><xsl:value-of select="./CompProperties/folder"/></xsl:variable>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:if test="$Folder='true'">
		<xsl:attribute name="ONCLICK">javascript:folderComponent('<xsl:value-of select="$CompNo"/>','<xsl:value-of select="$StyleString"/>');</xsl:attribute></xsl:if>
		<xsl:attribute name="STYLE"><xsl:if test="$Folder='true'">cursor:hand;</xsl:if><xsl:value-of select="$StyleString"/></xsl:attribute>
		<xsl:if test="$Folder='true'">
			<IMG SRC="/template/images/fold.gif">
			<xsl:attribute name="id">img<xsl:value-of select="./@id"/></xsl:attribute></IMG>&nbsp;
		</xsl:if>
		<STRONG><xsl:value-of select="./CompProperties/caption"/></STRONG></SPAN>
		<xsl:if test="string-length(./row)>0">
		<SCRIPT>
			tableSortCol['<xsl:value-of select="$CompNo"/>'] = -1;
			tableSortColStatus['<xsl:value-of select="$CompNo"/>'] = 0;
			element['<xsl:value-of select="$CompNo"/>'] = new Array();
			element['<xsl:value-of select="$CompNo"/>'].id = <xsl:value-of select="./@id"/>;
			element['<xsl:value-of select="$CompNo"/>'].vid = <xsl:value-of select="./vid"/>;
			element['<xsl:value-of select="$CompNo"/>'].component="<xsl:value-of select="./CompProperties/component"/>";
			element['<xsl:value-of select="$CompNo"/>'].width = "<xsl:value-of select="./CompProperties/width"/>";
			<xsl:if test="string-length(./CompProperties/recordhyperlinkcolumnindex)>0">
				element['<xsl:value-of select="$CompNo"/>'].recordhyperlinkcolumnindex = <xsl:value-of select="./CompProperties/recordhyperlinkcolumnindex"/>;
			</xsl:if>
			element['<xsl:value-of select="$CompNo"/>'].columnHeaders = new Array();
			element['<xsl:value-of select="$CompNo"/>'].displayColumns = new Array();
			<xsl:for-each select="./ColumnHeaders[@Display='Yes']">
				element['<xsl:value-of select="$CompNo"/>'].columnHeaders[<xsl:value-of select="./@colno"/>] ="<xsl:value-of select="."/>";
				element['<xsl:value-of select="$CompNo"/>'].displayColumns[<xsl:value-of select="position()-1"/>] = <xsl:value-of select="./@colno"/>;
			</xsl:for-each>
			element['<xsl:value-of select="$CompNo"/>'].columnTypes = new Array();
			<xsl:for-each select="./ColumnTypes[@Display='Yes']">
				element['<xsl:value-of select="$CompNo"/>'].columnTypes[<xsl:value-of select="./@colno"/>] = "<xsl:value-of select="."/>";
			</xsl:for-each>
			element['<xsl:value-of select="$CompNo"/>'].rows = new Array();	
			<xsl:for-each select="./row">
				<xsl:variable name="RowNo"><xsl:value-of select="position()-1"/></xsl:variable>
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>] = new Array(2);
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].rowNo = <xsl:value-of select="$RowNo"/>;
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns = new Array();				
				<xsl:for-each select="./column[@Display='Yes']">
					<xsl:variable name="columnValue"><xsl:apply-templates select="."/></xsl:variable>
					<xsl:choose>
					<xsl:when test="./@Type='Date'">
						<xsl:choose>
							<xsl:when test="string-length(.)>0">
								element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="./day"/> <xsl:call-template name="month"/>,<xsl:value-of select="./year"/>";
							</xsl:when>
							<xsl:otherwise>
								element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="$columnValue"/>";
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="./@Type='TimeStamp'">
						<xsl:choose>
							<xsl:when test="string-length(.)>0">
							<xsl:variable name="columnDateValue">
								<xsl:value-of select="./day"/> <xsl:call-template name="month"/>,<xsl:value-of select="./year"/><xsl:text> </xsl:text>
								<xsl:if test="string-length(./hours)>0"> 
								<xsl:choose>
									<xsl:when test="./hours > 12"> <xsl:value-of select="./hours - 12"/>:<xsl:if test="string-length(./minutes)=1">0</xsl:if><xsl:value-of select="./minutes"/> PM</xsl:when>
									<xsl:otherwise> <xsl:value-of select="./hours"/>:<xsl:if test="string-length(./minutes)=1">0</xsl:if><xsl:value-of select="./minutes"/> AM</xsl:otherwise>
								</xsl:choose></xsl:if>
							</xsl:variable> 
							element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="$columnDateValue"/>";
							</xsl:when>
							<xsl:otherwise>
								element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="$columnValue"/>";
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="$columnValue"/>";
					</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:for-each>
		</SCRIPT></xsl:if>
		<DIV align="left"><xsl:attribute name="id">F<xsl:value-of select="./@id"/></xsl:attribute> 
			<xsl:if test="$Folder='true'"><xsl:attribute name="STYLE">display:none;</xsl:attribute></xsl:if>
			<xsl:if test="./CompProperties/component='Anchor'">
				<TABLE WIDTH="100%">
				<xsl:for-each select="./item"> 
					<TR><TD><SPAN STYLE="color:blue;text-decoration:underline;cursor:hand;">
					<xsl:attribute name="ONCLICK">javascript:if(!disableAnchor){DisableMenu(); setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>); RequiredFldChk();}</xsl:attribute> 
					<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute>
					<xsl:value-of select="./name"/></SPAN>
					</SPAN></TD></TR>
				</xsl:for-each>
				</TABLE>
			</xsl:if>
			<xsl:if test="not($Folder='true')">
			<SCRIPT>
			document.all['F<xsl:value-of select="$CompNo"/>'].innerHTML = DisplaySortedTable('<xsl:value-of select="$CompNo"/>','<xsl:value-of select="$StyleString"/>');
			</SCRIPT></xsl:if>
		</DIV>
	</TD>
</xsl:template>

<xsl:template name="ComboBox_5">
	<xsl:variable name="SelectedRI">
		<xsl:choose>
			<xsl:when test="string-length(./SelectedRowItem) > 0"><xsl:value-of select="./SelectedRowItem"/></xsl:when>
			<xsl:otherwise>-1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<TD NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption"/>&nbsp;&nbsp;&nbsp;</SPAN>
	</TD><TD><SELECT SIZE="1">
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute> 
		<xsl:attribute name="ONCHANGE">javascript:  setVariableValue(<xsl:value-of select="./vid"/>,this.value);</xsl:attribute> 
		<OPTION VALUE="-1"><xsl:if test="$SelectedRI=-1">
			<xsl:attribute name="SELECTED"/></xsl:if>
			<xsl:text>  none  </xsl:text>
		</OPTION>
		<xsl:if test="string-length(./item)>0">
			<xsl:for-each select="./item"> 
				<OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./@itemno"/></xsl:attribute> 
					<xsl:value-of select="./name"/>
				</OPTION>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="string-length(./row)>0">
			<xsl:for-each select="./row"> 
				<OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./@rowno"/></xsl:attribute> 
					<xsl:if test="./@rowno=$SelectedRI">
						<xsl:attribute name="SELECTED"/></xsl:if>
					<xsl:value-of select="./column"/>
				</OPTION>
			</xsl:for-each>
		</xsl:if>
	</SELECT></TD>
</xsl:template>

<xsl:template name="ListBox_5">
	<xsl:variable name="SelectedRI">
		<xsl:choose>
			<xsl:when test="string-length(./SelectedRowItem) > 0"><xsl:value-of select="./SelectedRowItem"/></xsl:when>
			<xsl:otherwise>-1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<TD valign="top" NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption"/>&nbsp;&nbsp;&nbsp;</SPAN>
	</TD><TD><SELECT MULTIPLE=""><xsl:attribute name="SIZE">
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/multiline)>0">
				<xsl:value-of select="./CompProperties/multiline"/>
			</xsl:when>
			<xsl:otherwise>5</xsl:otherwise>
		</xsl:choose></xsl:attribute>
		<xsl:if test="./CompProperties[enabled='false']"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute> 
		<xsl:attribute name="ONCHANGE">javascript:  setVariableValue(<xsl:value-of select="./vid"/>,this.value);</xsl:attribute> 
		<OPTION VALUE="-1"><xsl:if test="$SelectedRI=-1">
			<xsl:attribute name="SELECTED"/></xsl:if>
			<xsl:text>  none  </xsl:text>
		</OPTION>
		<xsl:if test="string-length(./item)>0">
			<xsl:for-each select="./item"> 
				<OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./@itemno"/></xsl:attribute> 
					<xsl:value-of select="./name"/>
				</OPTION>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="string-length(./row)>0">
			<xsl:for-each select="./row"> 
				<OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./@rowno"/></xsl:attribute> 
					<xsl:if test="./@rowno=$SelectedRI">
						<xsl:attribute name="SELECTED"/></xsl:if>
					<xsl:value-of select="./column"/>
				</OPTION>
			</xsl:for-each>
		</xsl:if>
	</SELECT></TD>
</xsl:template>

<xsl:template name="Tree_5">
	<xsl:variable name="CompVID"><xsl:value-of select="./vid"/></xsl:variable>
	<xsl:variable name="CompNo"><xsl:value-of select="./@id"/></xsl:variable>
	<xsl:variable name="TreeChildCol"><xsl:value-of select="./CompProperties/treechild - 1"/></xsl:variable>
	<xsl:variable name="TreeParentCol"><xsl:value-of select="./CompProperties/treeparent - 1"/></xsl:variable>
	<xsl:variable name="DisplayCol"><xsl:value-of select="./CompProperties/displaycolumnindex - 1"/></xsl:variable>
	<xsl:variable name="BoundCol"><xsl:value-of select="./CompProperties/boundcolumnindex - 1"/></xsl:variable>
	<xsl:variable name="RootRow"><xsl:value-of select="./row[column[@colno=$TreeParentCol]='']/@rowno"/></xsl:variable>
	<TD colspan="2" align="left">
		<xsl:if test="string-length(./row)>0">
		<SCRIPT>
			element['<xsl:value-of select="$CompNo"/>'] = new Array();
			element['<xsl:value-of select="$CompNo"/>'].id = <xsl:value-of select="./@id"/>;
			element['<xsl:value-of select="$CompNo"/>'].vid = <xsl:value-of select="./vid"/>;
			element['<xsl:value-of select="$CompNo"/>'].displaycolumnindex = <xsl:value-of select="$DisplayCol"/>;
			element['<xsl:value-of select="$CompNo"/>'].boundcolumnindex = <xsl:value-of select="$BoundCol"/>;
			element['<xsl:value-of select="$CompNo"/>'].treechild = <xsl:value-of select="$TreeChildCol"/>;
			element['<xsl:value-of select="$CompNo"/>'].treeparent = <xsl:value-of select="$TreeParentCol"/>;
			element['<xsl:value-of select="$CompNo"/>'].enabled = '<xsl:value-of select="./CompProperties/enabled"/>';
			element['<xsl:value-of select="$CompNo"/>'].multiselect = '<xsl:value-of select="./CompProperties/multiselect"/>';
			element['<xsl:value-of select="$CompNo"/>'].selectedValue = <xsl:value-of select="./SelectedRowItem"/>;
			element['<xsl:value-of select="$CompNo"/>'].rows = new Array();	
			<xsl:for-each select="./row">
				<xsl:variable name="RowNo"><xsl:value-of select="./@rowno"/></xsl:variable>
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>] = new Array(2);
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns = new Array();				
				<xsl:for-each select="./column">				
					element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:apply-templates select="."/>";
				</xsl:for-each>
			</xsl:for-each>
		</SCRIPT></xsl:if>
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute>
		<SPAN><xsl:attribute name="STYLE">cursor:hand;<xsl:call-template name="StylingValue"/></xsl:attribute>
		<xsl:if test="string-length(./row)>0">
			<xsl:attribute name="onclick">ViewTreeComponent("<xsl:call-template name="StylingValue"/>","<xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$TreeChildCol]"/>","<xsl:value-of select="$CompNo"/>",1,"<xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$BoundCol]"/>",-1)</xsl:attribute>
			<IMG SRC="/template/images/plus.gif"><xsl:attribute name="id">TIMG<xsl:value-of select="./@id"/>0<xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$BoundCol]"/><xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$TreeChildCol]"/>-1</xsl:attribute></IMG>
		</xsl:if>
		<xsl:if test="string-length(./row)=0"><IMG SRC="/template/images/minus.gif"/></xsl:if>
		&nbsp;&nbsp;<xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if>
		<SPAN STYLE="color:blue;text-decoration:underline;">
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/caption)>0"><xsl:value-of select="./CompProperties/caption"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$DisplayCol]"/></xsl:otherwise>
		</xsl:choose></SPAN></SPAN>
		<DIV style="display:none;"><xsl:attribute name="id">CT<xsl:value-of select="./@id"/>0<xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$BoundCol]"/><xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$TreeChildCol]"/>-1</xsl:attribute></DIV>
	</TD>
</xsl:template>

<xsl:template name="MenuItem_0">
	<TD align="center" valign="middle">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<xsl:if test="./CompProperties/visible='false'"><xsl:attribute name="STYLE">display:none;</xsl:attribute> </xsl:if>
		<INPUT TYPE="button"><xsl:attribute name="STYLE">cursor:hand;<xsl:call-template name="StylingValue"/>
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/width)>0">
				width:<xsl:value-of select="./CompProperties/width"/>;
			</xsl:when>
			<xsl:otherwise>
				width:80;
			</xsl:otherwise>
		</xsl:choose>
		height:25;</xsl:attribute>
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="VALUE"><xsl:value-of select="./CompProperties/caption"/></xsl:attribute>
		<xsl:choose>
			<xsl:when test="./CompProperties/component='Exit'">
				<xsl:attribute name="ONCLICK">javascript: top.close();</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="ONCLICK">javascript:DisableMenu(); setVariableValue(<xsl:value-of select="./vid"/>,1); RequiredFldChk();</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		</INPUT>
	</TD>
</xsl:template>

<xsl:template match="InteractionNode/Properties">
	<xsl:if test="string-length(./title)>0">
	document.title = "<xsl:value-of select="./title"/>";
	</xsl:if>
	<xsl:if test="string-length(./bgcolor)>0">
	document.body.style.backgroundColor = "<xsl:value-of select="./bgcolor"/>";
	</xsl:if>
</xsl:template>

<xsl:template match="vValue">
	<xsl:copy-of select="node()"/>
</xsl:template>

<xsl:template match="column">
	<xsl:copy-of select="node()"/>
</xsl:template>

<xsl:template match="helptext">
	<xsl:copy-of select="node()"/>
</xsl:template>

<xsl:template match="SelectedValue">
	<xsl:copy-of select="node()"/>
</xsl:template>

<xsl:template name="StylingValue">
	<xsl:if test="string-length(./CompProperties/fontcolor)>0">color:<xsl:value-of select="./CompProperties/fontcolor"/>;</xsl:if>
	<xsl:choose><xsl:when test="string-length(./CompProperties/fontface)>0">font-family:<xsl:value-of select="./CompProperties/fontface"/>;</xsl:when>
		<xsl:when test="string-length($Properties/fontface)>0">font-family:<xsl:value-of select="$Properties/fontface"/>;</xsl:when>
		<xsl:otherwise>font-family:arial,verdana;</xsl:otherwise>
	</xsl:choose>
	<xsl:choose><xsl:when test="string-length(./CompProperties/fontsize)>0">font-size:<xsl:value-of select="./CompProperties/fontsize"/>px;</xsl:when>
		<xsl:when test="string-length($Properties/fontsize)>0">font-size:<xsl:value-of select="$Properties/fontsize"/>px;</xsl:when>
		<xsl:otherwise>font-size:12px;</xsl:otherwise>
	</xsl:choose>
	<xsl:if test="string-length(./CompProperties/fontstyle)>0">font-style:<xsl:value-of select="./CompProperties/fontstyle"/>;</xsl:if>
	<xsl:if test="string-length(./CompProperties/fontweight)>0">font-weight:<xsl:value-of select="./CompProperties/fontweight"/>;</xsl:if>
	<xsl:if test="string-length(./CompProperties/bgcolor)>0">background-color:<xsl:value-of select="./CompProperties/bgcolor"/>;</xsl:if>
</xsl:template>

<xsl:template name="month">
<xsl:variable name="CurrMonth"><xsl:value-of select="./vValue/month"/></xsl:variable>
	<xsl:choose>
		<xsl:when test="$CurrMonth=1"> Jan</xsl:when>
		<xsl:when test="$CurrMonth=2"> Feb</xsl:when>
		<xsl:when test="$CurrMonth=3"> March</xsl:when>
		<xsl:when test="$CurrMonth=4"> April</xsl:when>
		<xsl:when test="$CurrMonth=5"> May</xsl:when>
		<xsl:when test="$CurrMonth=6"> June</xsl:when>
		<xsl:when test="$CurrMonth=7"> July</xsl:when>
		<xsl:when test="$CurrMonth=8"> Aug</xsl:when>
		<xsl:when test="$CurrMonth=9"> Sep</xsl:when>
		<xsl:when test="$CurrMonth=10"> Oct</xsl:when>
		<xsl:when test="$CurrMonth=11"> Nov</xsl:when>
		<xsl:when test="$CurrMonth=12"> Dec</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="generate_script">
<SCRIPT>
var id = '<xsl:value-of select="InteractionNode/@navigatorID"/>';
var nodeID = <xsl:value-of select="InteractionNode/@id"/>;
<![CDATA[
function vpair(v,val)
{
  this.vid = v;
  this.value = val;
}
function compProps(Id,comp,minval,maxval,visibl,req,cap,t,ms,tmsg)
{
  this.component = comp;
  this.id = Id;
  if(minval=='')
  	this.minvalue = 0;
  else
	this.minvalue = minval.valueOf();
  if(maxval=='')
 	this.maxvalue = 0;
  else
	this.maxvalue = maxval.valueOf();
  this.visible = visibl;
  this.required = req;
  this.caption = cap;
  this.type = t;
  this.multiselect = ms;
  this.transientmessage = tmsg;
}
var CurrComp;
var hints = new Array();
]]>
var selectedValues = [
	<xsl:for-each select="InteractionNode/selectedValues/SelectedValue">
		new vpair(<xsl:value-of select="./@vid"/>,"<xsl:if test="not(./CompProperties/password='true')"><xsl:apply-templates select="."/></xsl:if>"),
	</xsl:for-each>
	new vpair(0,'')
];
var components = [
	<xsl:for-each select="$AllUIBlocks">
	<xsl:for-each select="./Component">
		new compProps(<xsl:value-of select="./@id"/>,"<xsl:value-of select="./CompProperties/component"/>","<xsl:value-of select="./CompProperties/minvalue"/>",
		"<xsl:value-of select="./CompProperties/maxvalue"/>","<xsl:value-of select="./CompProperties/visible"/>","<xsl:value-of select="./CompProperties/required"/>",
		"<xsl:value-of select="./CompProperties/caption"/>",<xsl:value-of select="./@type"/>,"<xsl:value-of select="./CompProperties/multiselect"/>","<xsl:value-of select="./CompProperties/transientmessage"/>"),
	</xsl:for-each>
	</xsl:for-each>
	new vpair(0,'','','','','','',-1,'','')
];
var element = new Array();
var tableSortCol = new Array();
var tableSortColStatus = new Array();
var disableAnchor = false;
var Months = new Array();
Months[0]='Jan';
Months[1]='Feb';
Months[2]='March';
Months[3]='April';
Months[4]='May';
Months[5]='June';
Months[6]='July';
Months[7]='Aug';
Months[8]='Sept';
Months[9]='Oct';
Months[10]='Nov';
Months[11]='Dec';

<xsl:if test="string-length(InteractionNode/Component/CompProperties[folder='true'])>0">
function folderComponent(currID,CompStyle)
{
	var objIMG = document.all['img'+currID];
	var objDIV = document.all['F'+currID];
	if(objDIV.style.display=='none')
	{
		if(objDIV.innerHTML.length&lt;=0)
			objDIV.innerHTML = DisplaySortedTable(currID, CompStyle);
		objDIV.style.display="";
		objIMG.src="/template/images/open.gif";
	}
	else
	{
		objDIV.style.display="none";
		objIMG.src="/template/images/fold.gif";
	}
}
</xsl:if>
<xsl:if test="string-length(InteractionNode/Component/row)>0">
<![CDATA[
function changeto(highlightcolor)
{
	source=event.srcElement;
	if(source.parentElement.tagName=="TD") source=source.parentElement;
	if(source.tagName=="A")source=source.parentElement.parentElement;
	if(source.tagName=="TD")
	{
		source=source.parentElement;
		if (source.style.backgroundColor!=highlightcolor)
			source.style.backgroundColor=highlightcolor;
	}
}

function changeback(originalcolor)
{
	source=event.srcElement;
	if(source.parentElement.tagName=="TD") source=source.parentElement;
	if(source.tagName=="A")source=source.parentElement.parentElement;
	if (source.tagName=="TD")
	{
		source=source.parentElement;
		source.style.backgroundColor=originalcolor;
	}
}
function sortTable(x, i, ComponentStyle)
{
	var ascSort = true; // 0 - asc sort; 1 - dec sort

	if(tableSortCol[i]==x && tableSortColStatus[i]==1)	
	{
		ascSort = false;
	}

	if(ascSort)
	{
		if(element[i].columnTypes[x]=='Number')
		{
			for(var a=1;a < element[i].rows.length;a++) 
			{
	  		for(var b=0;b < element[i].rows.length-a;b++)
	  		{
   			if((element[i].rows[b].columns[x]-1) > (element[i].rows[b+1].columns[x]-1))
   			{
     				var temp = element[i].rows[b];
     				element[i].rows[b] = element[i].rows[b+1];
     				element[i].rows[b+1] = temp;
    			}
			}
			}
		}
		else if(element[i].columnTypes[x]=='Date' || element[i].columnTypes[x]=='TimeStamp')
		{
			for(var a=1;a < element[i].rows.length;a++) 
			{
	  		for(var b=0;b < element[i].rows.length-a;b++)
	  		{
			if(((new Date(element[i].rows[b].columns[x])) > (new Date(element[i].rows[b+1].columns[x]))) || element[i].rows[b+1].columns[x]=='')
   			{
     				var temp = element[i].rows[b];
     				element[i].rows[b] = element[i].rows[b+1];
     				element[i].rows[b+1] = temp;
    			}
			}
			}
		}
		else
		{
			for(var a=1;a < element[i].rows.length;a++) 
			{
	  		for(var b=0;b < element[i].rows.length-a;b++)
	  		{
			if(element[i].rows[b].columns[x].toLowerCase() > element[i].rows[b+1].columns[x].toLowerCase())
   			{
     				var temp = element[i].rows[b];
     				element[i].rows[b] = element[i].rows[b+1];
     				element[i].rows[b+1] = temp;
    			}
			}
			}
		}
	}
	else
	{
		if(element[i].columnTypes[x]=='Number')
		{
			for(var a=1;a < element[i].rows.length;a++) 
			{
	  		for(var b=0;b < element[i].rows.length-a;b++)
	  		{
   			if((element[i].rows[b].columns[x]-1) < (element[i].rows[b+1].columns[x]-1))
   			{
     				var temp = element[i].rows[b];
     				element[i].rows[b] = element[i].rows[b+1];
     				element[i].rows[b+1] = temp;
    			}
			}
			}
		}
		else if(element[i].columnTypes[x]=='Date' || element[i].columnTypes[x]=='TimeStamp')
		{
			for(var a=1;a < element[i].rows.length;a++) 
			{
	  		for(var b=0;b < element[i].rows.length-a;b++)
	  		{
			if(((new Date(element[i].rows[b].columns[x])) < (new Date(element[i].rows[b+1].columns[x]))) || element[i].rows[b+1].columns[x]=='')
   			{
     				var temp = element[i].rows[b];
     				element[i].rows[b] = element[i].rows[b+1];
     				element[i].rows[b+1] = temp;
    			}
			}
			}
		}
		else
		{
			for(var a=1;a < element[i].rows.length;a++) 
			{
	  		for(var b=0;b < element[i].rows.length-a;b++)
	  		{
			if(element[i].rows[b].columns[x].toLowerCase() < element[i].rows[b+1].columns[x].toLowerCase())
   			{
     				var temp = element[i].rows[b];
     				element[i].rows[b] = element[i].rows[b+1];
     				element[i].rows[b+1] = temp;
    			}
			}
			}
		}
	}

	document.all['F' + i].innerHTML = DisplaySortedTable(i, ComponentStyle);

	for(a=0;a<element[i].displayColumns.length;a++)
	{
		document.all['THDR' +  i + element[i].displayColumns[a]].style.backgroundColor="#3399CC";
		document.all['IMGHDR' +  i + element[i].displayColumns[a]].src="/template/images/harrow.gif";
		document.all['IMGHDR' +  i + element[i].displayColumns[a]].style.display="none";
	}

	var hdrCol = document.all['THDR' +  i + x];
	var hdrImg = document.all['IMGHDR' + i + x];
	hdrImg.style.display = "";
	hdrCol.style.backgroundColor="#387272";
	if(ascSort)	
	{
		tableSortCol[i] = x;
		tableSortColStatus[i] = 1;
		hdrImg.src = "/template/images/harrow.gif";
	}
	else
	{
		tableSortColStatus[i]=0;
		hdrImg.src = "/template/images/varrow.gif";
	}	
}

function DisplaySortedTable(i, ComponentStyle)
{
	var tableComponent = '';

	if(element[i].displayColumns.length>0)
	{
		var tableLength=0;
		var colWidth = new Array();
		if(element[i].width.length>0)
			colWidth=element[i].width.split(",");
		for(x=0;x < colWidth.length;x++) tableLength += (colWidth[x]-1)+1;
		tableComponent = ' <TABLE ID="TBL' + element[i].id + 
				'" align=left BORDER="0" cellspacing="1" cellpadding="2" onMouseover=changeto("#C9C9C9") onMouseout=changeback("#DFDFDF") ';
		if(tableLength > 0)
			tableComponent += ' width="' + tableLength + '" ';
		tableComponent += '><TR><TH bgcolor="#ffffff" style="width:8px">&nbsp;&nbsp;&nbsp;&nbsp;</TH>';

		var tmpColIndex = element[i].displayColumns.length;
	    	for (x = 0; x < tmpColIndex; x++)
		{
			tableComponent  += '<TH ID="THDR'+ element[i].id +''+(element[i].displayColumns[x])+
					'" align=center valign=top STYLE="cursor:hand;background-color:#3399CC;' + ComponentStyle + ';'; 
			if(colWidth[x])
				tableComponent  += 'width:'+ (colWidth[x].length > 0?(colWidth[x]+'px'):'') + ';';
			tableComponent  += '" onclick="sortTable(' + (element[i].displayColumns[x]) + ',\'' + i + '\',\'' + 
				ComponentStyle + '\')"><FONT color="#FFFFFF"><IMG ID="IMGHDR'+ element[i].id +''+(element[i].displayColumns[x])+
					'" SRC="/template/images/harrow.gif" style="display:none">&nbsp;<STRONG>' + 
				element[i].columnHeaders[element[i].displayColumns[x]] + '</STRONG></FONT></TH>';
		}
    		tableComponent  += '</TR>';
	
		for (x = 0; x < element[i].rows.length; x++)
    		{
		tableComponent += '<TR bgcolor="#DFDFDF"><TD style="width:8px" bgcolor="#ffffff">&nbsp;</TD>';				
		for (var y = 0; y  < tmpColIndex; y++)
		{	
			if(y==0 && element[i].component=='Anchor' 
				&& (!element[i].recordhyperlinkindicatorcolumnindex 
					|| (element[i].recordhyperlinkindicatorcolumnindex && element[i].rows[x].columns[element[i].recordhyperlinkindicatorcolumnindex]==1)))
			{
                			tableComponent += '<TD valign="top"><SPAN STYLE="' + ComponentStyle + 
					';color:blue;text-decoration:underline;cursor:hand;" ' +
					' onclick="javascript:if(!disableAnchor){DisableMenu(); setVariableValue(' + element[i].vid + ',' + 
					element[i].rows[x].rowNo + '); RequiredFldChk();}">' +
					element[i].rows[x].columns[element[i].displayColumns[y]] + '</SPAN></TD>';
			}
			else
			{
				tableComponent += '<TD ';
				if(element[i].columnTypes[element[i].displayColumns[y]]=='Date' || element[i].columnTypes[element[i].displayColumns[y]]=='TimeStamp')
					tableComponent += ' NOWRAP ';
				tableComponent += ' valign="top"><SPAN STYLE=' + ComponentStyle + '>' + 
					element[i].rows[x].columns[element[i].displayColumns[y]] + '</SPAN></TD>';
			}
      		 }
		tableComponent += '</TR>';
    		}
		tableComponent += '</TABLE>';
	}
	return tableComponent;
}]]></xsl:if>
<xsl:if test="string-length(InteractionNode/Component/CompProperties[component='Tree'])>0">
<![CDATA[
function ViewTreeComponent(pCompStype,tmpparent,i,generation,pID,uRow)
{
	if(document.all['CT'+element[i].id+''+(generation-1)+pID+tmpparent+''+uRow].innerHTML=='')
	{
	var parent = tmpparent;
	pstrTree = '';
	var tmpRows = element[i].rows.length;
	pstrTree += '<TABLE BORDER="0" CELLSPACING="0"><TR><TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD><TD NOWRAP>';
	for(var x=0; x<tmpRows; x++)
	{
		var elementRow = element[i].rows[x];
		var cID = elementRow.columns[element[i].boundcolumnindex];
		var child = elementRow.columns[element[i].treechild];
		var tmpTitle = elementRow.columns[element[i].displaycolumnindex];
		var tmpParent = elementRow.columns[element[i].treeparent];
		if(tmpParent==parent)
		{
			var totalChild = 0;
			for(var y=0; y<tmpRows; y++)
				if(element[i].rows[y].columns[element[i].treeparent]==child)
						totalChild++;
			
			if(totalChild>0)		
			{
				pstrTree += '<span style="cursor:hand;' + pCompStype + '" onclick=ViewTreeComponent("' 
					+ pCompStype + '","' + child + '",' + i + ',' + (generation+1) + ',"' + cID + '",'+x+') ' +
					' id="T' + element[i].id + '' + generation + '' + cID + '' + child + ''+x+'">' +
					'&nbsp;&nbsp;<img id="TIMG' + element[i].id + '' + generation + '' + cID + '' + child + 
					''+x+'" src="/template/images/plus.gif">&nbsp;&nbsp;<span style="color:blue;text-decoration:underline">'
					+ tmpTitle + '</span></span><BR><DIV ID="CT' + element[i].id + '' + generation + '' + cID + '' + child + 
					''+x+'" style="display:none;"></DIV>';
			}
			else
			{
				pstrTree += '<span style="' + pCompStype + '">';
				if(element[i].enabled!='false')
				{
					if(element[i].multiselect=='true')
					{
						pstrTree +='<INPUT TYPE="CHECKBOX" ID="CB' + element[i].id + '' + cID + '' + child + ''+x + '" ';
						if(element[i].selectedValue == x)
								pstrTree += ' CHECKED ' ;
						pstrTree += '>';
						
					}
					else
					{
						pstrTree +='<INPUT TYPE="RADIO" NAME="RT' +element[i].id+''+ i +	'" ONCLICK=" setVariableValue(' +element[i].vid +',\''+ x + '\');" ';
						if(element[i].selectedValue == x)
								pstrTree += ' CHECKED ' ;
						pstrTree += '>';
					}
				}
				pstrTree += tmpTitle + '<BR></span>';
			}
		}
	}
	pstrTree += '</TD></TR></TABLE>';
	document.all['CT'+element[i].id+''+(generation-1)+pID+tmpparent+''+uRow].innerHTML = pstrTree;
	}

	TreeDropDown(element[i].id+''+(generation-1)+pID+tmpparent+''+uRow);
}
function TreeDropDown(List)
{
	var nested = document.all['CT' + List].style;
	var pnested = document.all['TIMG'+ List];
	if(nested.display=="none")
	{
		pnested.src="/template/images/minus.gif";
		nested.display="";
	}
	else
	{
		pnested.src="/template/images/plus.gif";
		nested.display="none";
	}
}]]></xsl:if>
function window_onload()
{
	<xsl:apply-templates select="$Properties"/>
		
	 dataLoaded();
}
<![CDATA[
function window_onclick()
{
	for (var i=0;i < components.length-1; i++)
	{	
		if(components[i].transientmessage=='true')
			document.all['COMPTR' + components[i].id].style.display='none';
	}
	if(document.all['popCal'])
		popCal.style.visibility = "hidden";
}
var SELECT_TRUE_FALSE=0;
var CNST_ENTER_DATE=1;
var CNST_ENTER_TIME=2;
var CNST_ENTER_STRING=3;
var CNST_ENTER_NUMBER=4;
var SELECT_FROM=5;
var TOID;
var ctrlSelectedValues;

function right(e) 
  {   if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
         {  
			return false;
         }          
      else
          if ((event.button == 2 || event.button == 3)) 
         {   
			alert('Welcome to OCTET system !');
			return false;
         }
      return true;
  }
//document.onmousedown=right;
//if (document.layers) window.captureEvents(Event.MOUSEDOWN);
//window.onmousedown=right;

function DisableMenu()
{
	var menuButtons = document.all.tags('input');
	if(menuButtons)
	{
		for(x=0; x<menuButtons.length; x++)
		{
			if(menuButtons[x].type=='button')
			{
				menuButtons[x].disabled=true;
			}
		}
	}
	disableAnchor = true;
	document.body.style.cursor='wait';
}
function EnableMenu()
{
	var menuButtons = document.all.tags('input');
	if(menuButtons)
	for(x=0; x<menuButtons.length; x++)
	{
		if(menuButtons[x].type=='button')
		{
			menuButtons[x].disabled=false;
		}
	}
	disableAnchor = false;
	document.body.style.cursor='auto';
}

function showHelpText(currID, group)
{
	var helpArea = document.all('GroupHelp_' + group);
	if(helpArea)
		helpArea.innerHTML = document.all['HELP_' + currID].innerHTML;
}

function goBack()
{
	var host = "";
	var pathname = "";
	if(self.ComFrm == null)
	{
		host=document.location.host;
		pathname = document.location.pathname;
	}
	else 
	{
		host = L__OrigHostName;
		var n = L__OrigPath.indexOf("?");
		if(n < 0)
			pathname = L__OrigPath;
		else
			pathname = L__OrigPath.substring(0, n);
	}
    var actionString = 'http://' + host + pathname +'?handler=Back&nodeID='+nodeID+'&navigatorID=' + id;
    document.location.href = actionString;
}

function goNext()
{
	var host = "";
	var pathname = "";
	if(self.ComFrm == null)
	{
		host=document.location.host;
		pathname = document.location.pathname;
	}
	else 
	{
		host = L__OrigHostName;
		var n = L__OrigPath.indexOf("?");
		if(n < 0)
			pathname = L__OrigPath;
		else
			pathname = L__OrigPath.substring(0, n);
	}
	var actionString = 'http://' + host + pathname +'?handler=Next&nodeID='+nodeID+'&navigatorID=' + id + "&";
	var elements = components;
	if(selectedValues != null)
	{
	    for (var i=0;i < selectedValues.length-1; i++)
	    { 
		var ChkVal = ctrlSelectedValues[i].value;
		 if((ChkVal !=  selectedValues[i].value)  || ChkVal!=0  || ChkVal!='null'  || ChkVal!=''
			||  (ChkVal=='-1' && (elements[i].component== 'ListBox' || elements[i].component== 'ComboBox' || elements[i].component== 'Tree')))
		{
			actionString += selectedValues[i].vid + '=' + escape(selectedValues[i].value) + '&';
		}
	    }
	}

	actionString = actionString.substring(0,actionString.length-1);
 	//alert(actionString);
	document.location.href = actionString;
}

function setVariableValue(v,val) 
{
	if (v+"" == "undefined" || v == null) 
	{
		return false;
	}
	if(selectedValues)
	{
		for (var i=0;i < selectedValues.length-1; i++) 
		{
			if (selectedValues[i].vid == v) 
			{
				selectedValues[i].value = val;
			}
		}
	}
	return true;
}

function isPosInteger(inputVal) {
	var inputStr = inputVal.toString();
	for (var i = 0; i < inputStr.length; i++) {
		var oneChar = inputStr.charAt(i);
		if ((oneChar < "0" || oneChar > "9") && oneChar!=".") {
			return false;
		}
	}

	return true;
}

function isNumber(inputVal) {
	oneDecimal = false
	inputStr = inputVal.toString()

	for (var i = 0; i < inputStr.length; i++) {
		var oneChar = inputStr.charAt(i)
		if (i == 0 && oneChar == "-") {
			continue
		}
		if (oneChar == "." && !oneDecimal) {
			oneDecimal = true
			continue
		}
		if (oneChar < "0" || oneChar > "9") {
			return false
		}
	}
	return true;
}

function inRange(inputStr,min,max) {
	var num = parseInt(inputStr);
	if (num < min || num > max) {
		return false;
	}
	return true;
}

function isValid(inputStr,min,max) {
	if (isEmpty(inputStr)) {
		alert("Please enter a number into the field before clicking the button.");
		return false;
	}
	else {
		if (!isNumber(inputStr)) {
			alert("Please make sure entries are numbers only.");
			return false;
		}
		else if(!isPosInteger(inputStr)) {
			alert("Please enter a positive number");
			return false;
		}
		else { 
			if(!(min==0 && max==0)){
				if (!inRange(inputStr,min,max)) {
					alert("Please enter a number between " + min + " and " + max);
					return false;
				}
			}
		}
	}
	return true;
}

function isEmpty(inputStr) {
	if (inputStr == null || inputStr == "") {return true;}
	return false;
}

function setEditValue(v,val,min,max) {
	if(!isValid(val,min,max)) {
		return false;
	}
	else {
		for (var i=0;i < selectedValues.length-1; i++) {
			if (selectedValues[i].vid == v)
			{
				selectedValues[i].value = val;
			}
		}
		return true;
	}
}

function RequiredFldChk()
{
	var CompObj;
	var ReqSuccess=true;
	if(components)
		var elements = components;
	else
		return false;
	for (var i=0;i < selectedValues.length-1; i++)
	{
		if(components[i].component!='Image' && components[i].component!='Label' && components[i].visible!='false')
		{
		if(components[i].type == CNST_ENTER_NUMBER)
		{
			CurrFld=document.all(''+components[i].id);
			if(CurrFld)
				ReqSuccess = setEditValue(selectedValues[i].vid,CurrFld.value,components[i].minvalue,components[i].maxvalue);
		}
		else if(components[i].component== 'ListBox')
		{
			var oListBox = document.all(components[i].id + '');
			var TableRows = '';
			for (x = 0; x < oListBox.options.length; x++)
         			{
				if(oListBox.options(x).selected)
					TableRows += oListBox.options(x).value + ';';
			}
			if(TableRows.length>0)
				TableRows=TableRows.substring(0,TableRows.length-1);
			else
				TableRows='-1';
			selectedValues[i].value=TableRows;
		}
		else if(components[i].component== 'Tree' && components[i].multiselect=='true')
		{
			var treeRows = '';
			var tmpElement = element[components[i].id+''];
			for(x=0;x<tmpElement.rows.length;x++)
			{
				var elementRow = tmpElement.rows[x];
				var cID = elementRow.columns[tmpElement.boundcolumnindex];
				var child = elementRow.columns[tmpElement.treechild];
				var tmpParent = elementRow.columns[tmpElement.treeparent];
				if(document.all('CB' +components[i].id+''+ cID +''+ child +''+x))
				{
					if(document.all('CB' +components[i].id+''+ cID +''+ child +''+x).checked)
						treeRows += x + ';';
				}
			}
			if(treeRows.length>0)
				treeRows = treeRows.substring(0,treeRows.length-1);
			else
				treeRows ='-1';
			selectedValues[i].value=treeRows;
		}
		if(components[i].required=='true' && ReqSuccess)
		{
			ChkVal=selectedValues[i].value;
			if((ChkVal=='' && ChkVal!=0) || ChkVal=='null' || (ChkVal=='-1' && (components[i].component== 'ListBox' || components[i].component== 'ComboBox' || components[i].component== 'Tree')))
			{
				alert(components[i].caption + ' is Required.');
				ReqSuccess=false;
			}
		}
		}
	}
	if(ReqSuccess)
		goNext();
	else
		EnableMenu();
}
function  dataLoaded()
{
     if(selectedValues)
     {
     	ctrlSelectedValues = new Array(selectedValues.length);
     	for(i=0;i<selectedValues.length;i++)
     	{
		ctrlSelectedValues[i] = new vpair();
		ctrlSelectedValues[i].vid = selectedValues[i].vid;
		ctrlSelectedValues[i].value = selectedValues[i].value;
    	}
     }     
}
document.onload=window_onload();
window.onclick=window_onclick();
]]>
</SCRIPT>
</xsl:template>

</xsl:stylesheet>
