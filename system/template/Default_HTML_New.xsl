<xsl:variable name="AllUIBlocks" select="InteractionNode/UIBlock"/>
<xsl:variable name="Properties" select="InteractionNode/Properties"/>
<xsl:variable name="Host" select="InteractionNode/Host"/>

<xsl:template name="generate_pagetitle">
<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
<TR>
	<TD HEIGHT="26px" VALIGN="center" ALIGN="left">
		<xsl:if test="string-length($Properties/captionbgcolor)>0">
			<xsl:attribute name="BGCOLOR"><xsl:value-of select="$Properties/captionbgcolor"/></xsl:attribute>
		</xsl:if>
		<SPAN STYLE="font: bold 18px Arial,Verdana,sans-serif;">
		<FONT><xsl:if test="string-length($Properties/captionbgcolor)>0">
			<xsl:attribute name="COLOR"><xsl:value-of select="$Properties/captioncolor"/></xsl:attribute></xsl:if>
		<xsl:value-of select="$Properties/caption" disable-output-escaping="yes"/></FONT></SPAN>
	</TD>
</TR>
</TABLE>
</xsl:template>

<xsl:template name="generate_description">
	<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
	<xsl:value-of select="$Properties/description" disable-output-escaping="yes"/></SPAN>
</xsl:template>

<xsl:template name="generate_components">
	<xsl:param name="group"/>
	<xsl:if test="string-length($AllUIBlocks/Component[@type='2'])>0">
		<DIV style='POSITION:absolute;visibility:hidden;border:2px ridge;z-index:100;width:10' onclick='event.cancelBubble=true'>
			<xsl:attribute name="id">popCal_<xsl:value-of select="$group"/></xsl:attribute>
			<iframe frameborder="0" scrolling="no" width="183" height="188">
			<xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/calendar.htm</xsl:attribute>
			<xsl:attribute name="name">popFrame_<xsl:value-of select="$group"/></xsl:attribute></iframe>
		</DIV>
		<SCRIPT>calenders[calCount]="popCal_<xsl:value-of select="$group"/>"; calCount++;</SCRIPT>
	</xsl:if>
	<TABLE border="0" cellspacing="0" cellpadding="3" width="100%">
		<xsl:for-each select="$AllUIBlocks">
		<xsl:for-each select="./Component">
			<xsl:variable name="CompGroup"><xsl:value-of select="./CompProperties/group"/></xsl:variable>
			<xsl:if test="not(@uitype='MenuAnchor' or @uitype='MenuButton' or @uitype='MenuImage') and $CompGroup=$group">
				<TR><xsl:attribute name="ID">COMPTR<xsl:value-of select="./@id"/></xsl:attribute>
				<xsl:if test="string-length(./CompProperties/bgcolor)>0"><xsl:attribute name="bgcolor"><xsl:value-of select="./CompProperties/bgcolor"/></xsl:attribute></xsl:if>
				<xsl:attribute name="onmouseover">javascript: showHelpText('<xsl:value-of select="./@id"/>','<xsl:value-of select="./CompProperties/group"/>');<xsl:if test="string-length(./CompEvent[@ename='OnMouseOver'])>0">e<xsl:value-of select="./@id"/>_OnMouseOver(0,0,0);</xsl:if></xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnMouseOut'])>0"><xsl:attribute name="onmouseout">javascript: e<xsl:value-of select="./@id"/>_OnMouseOut(0,0,0);</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnMouseMove'])>0"><xsl:attribute name="OnMouseMove">javascript: e<xsl:value-of select="./@id"/>_OnMouseMove(0,0,0);</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnMouseDown'])>0"><xsl:attribute name="OnMouseDown">javascript: e<xsl:value-of select="./@id"/>_OnMouseDown(0,0,0);</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnMouseUp'])>0"><xsl:attribute name="OnMouseUp">javascript: e<xsl:value-of select="./@id"/>_OnMouseUp(0,0,0);</xsl:attribute></xsl:if>
				<xsl:if test="CompProperties[visible='false']"><xsl:attribute name="style">display:none</xsl:attribute></xsl:if>
				<xsl:apply-templates select="."/>
				</TR>
			</xsl:if>
		</xsl:for-each>
		</xsl:for-each>
	</TABLE>
</xsl:template>

<xsl:template name="generate_menu">
<xsl:param name="group"/>
<xsl:variable name="StyleValue"><xsl:call-template name="StylingValue"/></xsl:variable>
<TABLE cellpadding="3" cellspacing="0"><TR>
	<xsl:if test="not($Properties/backbuttonvisible='false')">
		<TD id="back" align="center" valign="middle"> 
		<xsl:choose>
			<xsl:when test="string-length($Properties/backbuttonimage)>0">
				<IMG BORDER="0" style="cursor:hand;"><xsl:attribute name="ONCLICK">javascript: if(!disableAnchor){DisableMenu();    goBack();}</xsl:attribute>
				<xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/<xsl:value-of select="$Properties/backbuttonimage"/></xsl:attribute></IMG>
			</xsl:when>
			<xsl:otherwise>
				<INPUT ONCLICK="javascript:DisableMenu(); goBack();" type="button">
				<xsl:attribute name="value">
				<xsl:choose>
					<xsl:when test="string-value($Properties/backbuttoncaption)>0">
						<xsl:value-of select="$Properties/backbuttoncaption"/>
					</xsl:when>
					<xsl:otherwise>
						Back
					</xsl:otherwise>
				</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="style">cursor:hand;width:80;height:25;font-weight:bold<xsl:value-of select="$StyleValue"/></xsl:attribute>
				</INPUT>
			</xsl:otherwise>
		</xsl:choose>
		</TD>
	</xsl:if>
	<xsl:for-each select="$AllUIBlocks">
	<xsl:for-each select="./Component">
		<xsl:variable name="CompGroup"><xsl:value-of select="./CompProperties/group"/></xsl:variable>
		<xsl:if test="(@uitype='MenuAnchor' or @uitype='MenuButton' or @uitype='MenuImage') and $CompGroup=$group">
			<xsl:choose>
			<xsl:when test="@uitype='MenuImage'">
				<xsl:call-template name="Image_0"/>
			</xsl:when>
			<xsl:when test="@uitype='MenuAnchor'">
				<xsl:call-template name="Anchor_0"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="Button_0"/>
			</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:for-each>
	</xsl:for-each>
	<xsl:if test="$Properties/resetbuttonvisible='true'">
		<TD id="reset" align="center" valign="middle"> 
		<INPUT value="Reset" type="reset">
			<xsl:attribute name="style">cursor:hand;width:80;height:25;font-weight:bold<xsl:value-of select="$StyleValue"/></xsl:attribute>
		</INPUT>
		</TD>
	</xsl:if>
	<xsl:if test="not($Properties/nextbuttonvisible='false')">
		<TD id="next" align="center" valign="middle"> 
		<xsl:choose>
			<xsl:when test="string-length($Properties/nextbuttonimage)>0">
				<IMG BORDER="0" style="cursor:hand;height:25;width:80;"><xsl:attribute name="ONCLICK">javascript: if(!disableAnchor){DisableMenu();    RequiredFldChk();}</xsl:attribute>
				<xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/<xsl:value-of select="$Properties/nextbuttonimage"/></xsl:attribute></IMG>
			</xsl:when>
			<xsl:otherwise>
				<INPUT ONCLICK="javascript:DisableMenu(); RequiredFldChk();" type="button">
				<xsl:attribute name="value">
				<xsl:choose>
					<xsl:when test="string-value($Properties/nextbuttoncaption)>0">
						<xsl:value-of select="$Properties/nextbuttoncaption"/>
					</xsl:when>
					<xsl:otherwise>
						Next
					</xsl:otherwise>
				</xsl:choose>
				</xsl:attribute>
					<xsl:attribute name="style">cursor:hand;width:80;height:25;font-weight:bold<xsl:value-of select="$StyleValue"/></xsl:attribute>
				</INPUT>
			</xsl:otherwise>
		</xsl:choose>
		</TD>
	</xsl:if>
</TR></TABLE>
</xsl:template>

<xsl:template name="generate_helptext">
<xsl:param name="group"/>
	<DIV><xsl:attribute name="id">GroupHelp_<xsl:value-of select="$group"/></xsl:attribute></DIV>
	<xsl:for-each select="$AllUIBlocks">
	<xsl:for-each select="./Component">
		<xsl:variable name="CompGroup"><xsl:value-of select="./CompProperties/group"/></xsl:variable>
		<xsl:if test="$CompGroup=$group">
			<DIV style="display:none;"><xsl:attribute name="id">HELP_<xsl:value-of select="./@id"/></xsl:attribute>
				<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
				<B><xsl:value-of select="./CompProperties/helptext" disable-output-escaping="yes"/></B></SPAN>
			</DIV>
		</xsl:if>
	</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<xsl:template match="Component"> 
<xsl:variable name="CompType" select="@uitype"/>
<xsl:variable name="Type" select="@type"/>
<xsl:choose>
<xsl:when test="$Type='1'">
	<xsl:choose>
		<xsl:when test="$CompType='Anchor'">
			<xsl:call-template name="Anchor_0"/>
		</xsl:when>
		<xsl:when test="$CompType='Button'">
			<xsl:call-template name="Button_0"/>
		</xsl:when>
		<xsl:when test="$CompType='CheckBox'">
			<xsl:call-template name="CheckBox_0"/>
		</xsl:when>
		<xsl:when test="$CompType='ComboBox'">
			<xsl:call-template name="ComboBox_0"/>
		</xsl:when>
		<xsl:when test="$CompType='Image'">
			<xsl:call-template name="Image_0"/>
		</xsl:when>
		<xsl:when test="$CompType='RadioGroup'">
			<xsl:call-template name="RadioGroup_0"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>

<xsl:when test="$Type='2'">
	<xsl:if test="$CompType='TextBox'">
		<xsl:call-template name="DateTime_1"/>
	</xsl:if>
</xsl:when>

<xsl:when test="$Type='3'">
	<xsl:if test="$CompType='TextBox'">
		<xsl:call-template name="DateTime_1"/>
	</xsl:if>
</xsl:when>

<xsl:when test="$Type='4'">
	<xsl:choose>
		<xsl:when test="$CompType='AnchorTable'">
			<xsl:call-template name="AnchorDisplayGrid_3"/>
		</xsl:when>
		<xsl:when test="$CompType='ComboBox'">
			<xsl:call-template name="ComboBox_3"/>
		</xsl:when>
		<xsl:when test="$CompType='DisplayGrid'">
			<xsl:call-template name="AnchorDisplayGrid_3"/>
		</xsl:when>
		<xsl:when test="$CompType='Graph'">
			<xsl:call-template name="Graph_3"/>
		</xsl:when>
		<xsl:when test="$CompType='ListBox'">
			<xsl:call-template name="ListBox_3"/>
		</xsl:when>
		<xsl:when test="$CompType='RadioGroup'">
			<xsl:call-template name="AnchorDisplayGrid_3"/>
		</xsl:when>
		<xsl:when test="$CompType='Tree'">
			<xsl:call-template name="Tree_3"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>

<xsl:when test="$Type='5'">
	<xsl:choose>
		<xsl:when test="$CompType='Anchor'">
			<xsl:call-template name="Anchor_4"/>
		</xsl:when>
		<xsl:when test="$CompType='Button'">
			<xsl:call-template name="Button_4"/>
		</xsl:when>
		<xsl:when test="$CompType='CheckBox'">
			<xsl:call-template name="CheckBox_4"/>
		</xsl:when>
		<xsl:when test="$CompType='ComboBox'">
			<xsl:call-template name="ComboBox_3"/>
		</xsl:when>
		<xsl:when test="$CompType='ListBox'">
			<xsl:call-template name="ListBox_3"/>
		</xsl:when>
		<xsl:when test="$CompType='RadioGroup'">
			<xsl:call-template name="RadioGroup_4"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>

<xsl:when test="$Type='6'">
	<xsl:if test="$CompType='TextBox'">
		<xsl:call-template name="TextBox_6"/>
	</xsl:if>
</xsl:when>

<xsl:when test="$Type='7'">
	<xsl:choose>
		<xsl:when test="$CompType='Label'">
			<xsl:call-template name="Label_6"/>
		</xsl:when>
		<xsl:when test="$CompType='TextBox'">
			<xsl:call-template name="TextBox_6"/>
		</xsl:when>
		<xsl:when test="$CompType='TextArea'">
			<xsl:call-template name="TextArea_6"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="Anchor_0">
<xsl:choose>
<xsl:when test="string-length(./CompProperties/subcomponent)>0 and ./CompProperties/subcomponent='true'">
<SCRIPT>
SubComponent0['<xsl:value-of select="@name"/>']='Anchor';
<xsl:choose>
	<xsl:when test="./CompProperties[enabled='false']">
		SubComponent1['<xsl:value-of select="@name"/>']=' <SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;<SPAN STYLE="text-decoration:underline;"><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></SPAN><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>';
		SubComponent2['<xsl:value-of select="@name"/>']='';
		SubComponent3['<xsl:value-of select="@name"/>']='';
	</xsl:when>
	<xsl:otherwise>
		SubComponent1['<xsl:value-of select="@name"/>']='<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;<SPAN STYLE="color:blue;text-decoration:underline;cursor:hand;"><xsl:attribute name="ONCLICK">javascript:if(!disableAnchor){setVariableValue(<xsl:value-of select="./vid"/>,1);';
		SubComponent2['<xsl:value-of select="@name"/>']='<xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(</xsl:if>';
		SubComponent3['<xsl:value-of select="@name"/>']='RequiredFldChk();}</xsl:attribute><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></SPAN><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>';
	</xsl:otherwise>
</xsl:choose>
</SCRIPT>
</xsl:when>
<xsl:otherwise>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute> 
		<xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;<SPAN><xsl:if test="string-length(./CompEvent[@ename='OnMouseOver'])>0"><xsl:attribute name="onmouseover">javascript: e<xsl:value-of select="./@id"/>_OnMouseOver(0,0,0);</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnMouseOut'])>0"><xsl:attribute name="onmouseout">javascript: e<xsl:value-of select="./@id"/>_OnMouseOut(0,0,0);</xsl:attribute></xsl:if><xsl:if test="not(./CompProperties[enabled='false'])">
			<xsl:attribute name="ONCLICK">javascript:if(!disableAnchor){DisableMenu();  setVariableValue(<xsl:value-of select="./vid"/>,1); RequiredFldChk();<xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:if>}</xsl:attribute>
		</xsl:if><xsl:attribute name="STYLE">text-decoration:underline;<xsl:if test="not(./CompProperties[enabled='false'])">color:blue;cursor:hand;</xsl:if></xsl:attribute><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></SPAN><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>
	</TD>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="Button_0">
<xsl:choose>
<xsl:when test="string-length(./CompProperties/subcomponent)>0 and ./CompProperties/subcomponent='true'">
<SCRIPT>
SubComponent0['<xsl:value-of select="@name"/>']='Button';
<xsl:choose>
	<xsl:when test="./CompProperties[enabled='false']">
		SubComponent1['<xsl:value-of select="@name"/>']='<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;</SPAN> <INPUT type="button"><xsl:attribute name="id">MI<xsl:value-of select="./@id"/></xsl:attribute> <xsl:attribute name="DISABLED"/><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/><xsl:choose><xsl:when test="string-length(./CompProperties/width)>0">width:<xsl:value-of select="./CompProperties/width"/>;</xsl:when><xsl:otherwise>width:80;</xsl:otherwise></xsl:choose><xsl:if test="string-length(./CompProperties/height)>0">height:<xsl:value-of select="./CompProperties/height"/>;</xsl:if>background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/>;</xsl:attribute><xsl:attribute name="value"><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></xsl:attribute></INPUT><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>';
		SubComponent2['<xsl:value-of select="@name"/>']='';
		SubComponent3['<xsl:value-of select="@name"/>']='';
	</xsl:when>
	<xsl:otherwise>
		SubComponent1['<xsl:value-of select="@name"/>']='<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;</SPAN> <INPUT type="button"><xsl:attribute name="id">MI<xsl:value-of select="./@id"/></xsl:attribute> <xsl:attribute name="ONCLICK">javascript: { setVariableValue(<xsl:value-of select="./vid"/>,1);';
		SubComponent2['<xsl:value-of select="@name"/>']='<xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(</xsl:if>';
		SubComponent3['<xsl:value-of select="@name"/>']='<xsl:if test="not(string-length(./CompProperties/navigation)>0) or ./CompProperties/navigation!='false'">RequiredFldChk();</xsl:if>}</xsl:attribute><xsl:attribute name="STYLE">cursor:hand;<xsl:call-template name="StylingValue"/><xsl:choose><xsl:when test="string-length(./CompProperties/width)>0">width:<xsl:value-of select="./CompProperties/width"/>;</xsl:when><xsl:otherwise>width:80;</xsl:otherwise></xsl:choose><xsl:if test="string-length(./CompProperties/height)>0">height:<xsl:value-of select="./CompProperties/height"/>;</xsl:if>background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute><xsl:attribute name="value"><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></xsl:attribute></INPUT><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>';
	</xsl:otherwise>
</xsl:choose>
</SCRIPT>
</xsl:when>
<xsl:otherwise>
	<TD valign="middle"  colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;</SPAN>
		<INPUT type="button"><xsl:attribute name="ID"><xsl:value-of select="./@id"/></xsl:attribute>
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="ONCLICK">javascript:DisableMenu();  { setVariableValue(<xsl:value-of select="./vid"/>,1); <xsl:if test="not(string-length(./CompProperties/navigation)>0) or ./CompProperties/navigation!='false'">RequiredFldChk();</xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:if>}</xsl:attribute>
		<xsl:if test="string-length(./CompEvent[@ename='OnMouseOver'])>0"><xsl:attribute name="onmouseover">javascript: e<xsl:value-of select="./@id"/>_OnMouseOver(0,0,0);</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnMouseOut'])>0"><xsl:attribute name="onmouseout">javascript: e<xsl:value-of select="./@id"/>_OnMouseOut(0,0,0);</xsl:attribute></xsl:if>
		<xsl:attribute name="STYLE">cursor:hand;font-weight:bold;<xsl:call-template name="StylingValue"/>
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/width)>0">
				width:<xsl:value-of select="./CompProperties/width"/>;
			</xsl:when>
			<xsl:otherwise>
				width:80;
			</xsl:otherwise>
		</xsl:choose>height:25;
		<xsl:if test="string-length(./CompProperties/height)>0">height:<xsl:value-of select="./CompProperties/height"/>;</xsl:if>
		background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
		<xsl:attribute name="value">
			<xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>
		</xsl:attribute></INPUT>
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>
	</TD>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="CheckBox_0">
<xsl:choose>
<xsl:when test="string-length(./CompProperties/subcomponent)>0 and ./CompProperties/subcomponent='true'">
<SCRIPT>
	SubComponent0['<xsl:value-of select="@name"/>']='CheckBox';
	SubComponent1['<xsl:value-of select="@name"/>']='<INPUT TYPE="checkbox"> <xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute><xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if><xsl:attribute name="ONCLICK">javascript:  ';
	SubComponent2['<xsl:value-of select="@name"/>']='<xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(</xsl:if>';
	SubComponent3['<xsl:value-of select="@name"/>']='</xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:attribute name="onchange">javascript: e<xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:attribute></xsl:if></INPUT> <SPAN> <xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></SPAN>';
</SCRIPT>
</xsl:when>
<xsl:otherwise>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<INPUT TYPE="checkbox"> <xsl:attribute name="ID"><xsl:value-of select="./@id"/></xsl:attribute>
		<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
		<xsl:if test="./vValue='true'"><xsl:attribute name="CHECKED"/></xsl:if>
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="ONCLICK">javascript:   setVariableValue(<xsl:value-of select="./vid"/>,this.checked?1:0); <xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:if></xsl:attribute>
		<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:attribute name="onchange">javascript: <xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:attribute></xsl:if>
		</INPUT>&nbsp;<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
			<xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>
		</SPAN>
	</TD>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="ComboBox_0">
	<TD NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>&nbsp;&nbsp;&nbsp;</SPAN>
	</TD>
	<TD valign="top"><SELECT SIZE="1"><xsl:attribute name="ID"><xsl:value-of select="./@id"/></xsl:attribute><xsl:attribute name="NAME"><xsl:value-of select="./vid"/></xsl:attribute><xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0"><xsl:attribute name="ONCLICK">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:attribute></xsl:if>
		<xsl:attribute name="VALUE"><xsl:choose><xsl:when test="./vValue='true'">1</xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose></xsl:attribute>
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="ONCHANGE">javascript: setVariableValue(<xsl:value-of select="./vid"/>,this.value);<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0">e<xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:if></xsl:attribute>
		<OPTION VALUE="1">Yes</OPTION>
		<OPTION VALUE="0">No</OPTION></SELECT><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>
	</TD>
</xsl:template>

<xsl:template name="Image_0">
<xsl:choose>
<xsl:when test="string-length(./CompProperties/subcomponent)>0 and ./CompProperties/subcomponent='true'">
<SCRIPT>
SubComponent0['<xsl:value-of select="@name"/>']='Image';
<xsl:choose>
	<xsl:when test="./CompProperties[enabled='false']">
		SubComponent1['<xsl:value-of select="@name"/>']='<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;</SPAN><IMG BORDER="0"><xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/<xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></xsl:attribute><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/><xsl:if test="string-length(./CompProperties/height)>0">height:<xsl:value-of select="./CompProperties/height"/>;</xsl:if><xsl:if test="string-length(./CompProperties/width)>0">width:<xsl:value-of select="./CompProperties/width"/>;</xsl:if></xsl:attribute></IMG><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>&nbsp;<xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>';
		SubComponent2['<xsl:value-of select="@name"/>']='';
		SubComponent3['<xsl:value-of select="@name"/>']='';
	</xsl:when>
	<xsl:otherwise>
		SubComponent1['<xsl:value-of select="@name"/>']='<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>&nbsp;<xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;</SPAN><SPAN><xsl:attribute name="ONCLICK">javascript:if(!disableAnchor){setVariableValue(<xsl:value-of select="./vid"/>,1);';
		SubComponent2['<xsl:value-of select="@name"/>']='<xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(</xsl:if>';
		SubComponent3['<xsl:value-of select="@name"/>']='<xsl:if test="string-length(./CompProperties/navigation)>0 and ./CompProperties/navigation!='false'">RequiredFldChk();</xsl:if>}</xsl:attribute><IMG BORDER="0"><xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/<xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></xsl:attribute><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/><xsl:if test="string-length(./CompProperties/height)>0">height:<xsl:value-of select="./CompProperties/height"/>;</xsl:if><xsl:if test="string-length(./CompProperties/width)>0">width:<xsl:value-of select="./CompProperties/width"/>;</xsl:if></xsl:attribute></IMG></SPAN><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>';
	</xsl:otherwise>
</xsl:choose>
</SCRIPT>
</xsl:when>
<xsl:otherwise>
<xsl:choose>
	<xsl:when test="./CompProperties[enabled='false']">
		<TD  colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;</SPAN>
		<IMG BORDER="0"><xsl:attribute name="ID"><xsl:value-of select="./@id"/></xsl:attribute><xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/<xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>
		</xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0"><xsl:attribute name="ONCLICK">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnDblClick'])>0"><xsl:attribute name="ONDBLCLICK">e<xsl:value-of select="./@id"/>_OnDblClick();</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnMouseOver'])>0"><xsl:attribute name="onmouseover">javascript: <xsl:value-of select="./@id"/>_OnMouseOver(0,0,0);</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnMouseOut'])>0"><xsl:attribute name="onmouseout">javascript: e<xsl:value-of select="./@id"/>_OnMouseOut(0,0,0);</xsl:attribute></xsl:if>
		<xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute></IMG>
		</TD>
	</xsl:when>
	<xsl:otherwise>
		<TD  colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute>
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:value-of select="./CompProperties/captionprefix" disable-output-escaping="yes"/>&nbsp;</SPAN>
		<IMG BORDER="0"><xsl:attribute name="ID"><xsl:value-of select="./@id"/></xsl:attribute>
			<xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/<xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnDblClick'])>0"><xsl:attribute name="ONDBLCLICK">e<xsl:value-of select="./@id"/>_OnDblClick();</xsl:attribute></xsl:if><xsl:attribute name="ONCLICK">
		javascript:if(!disableAnchor){DisableMenu(); setVariableValue(<xsl:value-of select="./vid"/>,1); <xsl:if test="string-length(./CompProperties/navigation)>0 and ./CompProperties/navigation!='false'">RequiredFldChk();</xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:if>}</xsl:attribute>
			<xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/><xsl:if test="string-length(./CompProperties/height)>0">height:<xsl:value-of select="./CompProperties/height"/>;</xsl:if><xsl:if test="string-length(./CompProperties/width)>0">width:<xsl:value-of select="./CompProperties/width"/>;</xsl:if></xsl:attribute>
		<xsl:if test="string-length(./CompEvent[@ename='OnMouseOver'])>0"><xsl:attribute name="ONMOUSEOVER">javascript: <xsl:value-of select="./@id"/>_OnMouseOver(0,0,0);</xsl:attribute></xsl:if><xsl:if test="string-length(./CompEvent[@ename='OnMouseOut'])>0"><xsl:attribute name="onmouseout">javascript: e<xsl:value-of select="./@id"/>_OnMouseOut(0,0,0);</xsl:attribute></xsl:if>
		</IMG><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>&nbsp;<xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN></TD>
	</xsl:otherwise>
</xsl:choose>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="RadioGroup_0">
<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<TD valign="top" colspan="2"><xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>&nbsp;&nbsp;&nbsp;</SPAN>
		<BR/><TABLE WIDTH="100%" border="0" cellspacing="0" cellpadding="3">
		<TR><TD valign="top" WIDTH="2%"><INPUT TYPE="radio"><xsl:attribute name="NAME">rdBtn<xsl:value-of select="./@id"/></xsl:attribute> 
			<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
			<xsl:attribute name="ONCLICK">javascript:    setVariableValue(<xsl:value-of select="./vid"/>,1);</xsl:attribute>
			<xsl:if test="./vValue='true'"><xsl:attribute name="CHECKED"/></xsl:if><xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:attribute name="onchange">javascript: <xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:attribute></xsl:if></INPUT>
		</TD><TD valign="top">
			<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute> Yes</SPAN></TD></TR>
		<TR><TD valign="top" WIDTH="2%"><INPUT TYPE="radio"><xsl:attribute name="NAME">rdBtn<xsl:value-of select="./@id"/></xsl:attribute> 
			<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
			<xsl:attribute name="ONCLICK">javascript:    setVariableValue(<xsl:value-of select="./vid"/>,0);</xsl:attribute>
			<xsl:if test="./vValue='false'"><xsl:attribute name="CHECKED"/></xsl:if><xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:attribute name="onchange">javascript: <xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:attribute></xsl:if></INPUT>
		</TD><TD valign="top"><SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute> No</SPAN></TD></TR>
		</TABLE>
	</TD>
</xsl:template>

<xsl:template name="DateTime_1">
	<xsl:variable name="tmpDate">
		<xsl:value-of select="./vValue/day"/> <xsl:call-template name="month"/>, <xsl:value-of select="./vValue/year"/><xsl:text> </xsl:text>
		<xsl:if test="string-length(./vValue/hours)>0"> 
			<xsl:choose>
			<xsl:when test="./vValue/hours > 12">
				<xsl:value-of select="./vValue/hours - 12"/>:<xsl:if test="string-length(./vValue/minutes)=1">0</xsl:if><xsl:value-of select="./vValue/minutes"/> PM
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="./vValue/hours"/>:<xsl:if test="string-length(./vValue/minutes)=1">0</xsl:if><xsl:value-of select="./vValue/minutes"/> AM
			</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:variable> 
	
	<TD NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>&nbsp;&nbsp;&nbsp;</SPAN></TD>
		<xsl:choose>
			<xsl:when test="not(./CompProperties[enabled='false'])">
				<TD valign="top"><INPUT TYPE="text">
				<xsl:attribute name="ID"><xsl:value-of select="./@id"/></xsl:attribute> 
				<xsl:choose>
					<xsl:when test="string-length(./CompProperties/width)>0">
						<xsl:attribute name="SIZE"><xsl:value-of select="./CompProperties/width"/></xsl:attribute> 	
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="SIZE">12</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
				<xsl:attribute name="VALUE"><xsl:value-of select="$tmpDate"/></xsl:attribute> 
				<xsl:attribute name="MAXLENGTH"><xsl:value-of select="./CompProperties/maxlength"/></xsl:attribute> 
				<xsl:attribute name="NAME"><xsl:value-of select="./@id"/></xsl:attribute>
				<xsl:attribute name="ONCHANGE">javascript:   setVariableValue(<xsl:value-of select="./vid"/>,
					((new Date(this.value)).getMonth()+1)+'.'+(new Date(this.value)).getDate()+
					'.'+(new Date(this.value)).getFullYear());<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:if></xsl:attribute>
				</INPUT>
				<IMG><xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/calendar.gif</xsl:attribute>
				<xsl:attribute name="ONCLICK">javascript: popFrame_<xsl:value-of select="./CompProperties/group"/>.fPopCalendar(document.all('<xsl:value-of select="./@id"/>'),document.all('<xsl:value-of select="./@id"/>'),popCal_<xsl:value-of select="./CompProperties/group"/>,<xsl:value-of select="./vid"/>,self); return false;</xsl:attribute>
				</IMG> <SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>&nbsp;<xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN></TD>
			</xsl:when>
			<xsl:otherwise>
				<TD valign="top">&nbsp;<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><B><xsl:value-of select="$tmpDate"/></B><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN></TD>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template name="AnchorDisplayGrid_3">
	<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<xsl:variable name="CompVID" select="./vid"/>
	<xsl:variable name="CompNo" select="./@id"/>
	<xsl:variable name="Folder" select="./CompProperties/folder"/>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="$CompNo"/></xsl:attribute> 
		<SPAN><xsl:if test="$Folder='true'">
		<xsl:if test="string-length(./row)>0">
		<xsl:attribute name="ONCLICK">javascript:folderComponent(<xsl:value-of select="$CompNo"/>,"<xsl:value-of select="$StyleString"/>");</xsl:attribute></xsl:if>
		</xsl:if>
		<xsl:attribute name="STYLE"><xsl:if test="$Folder='true'">cursor:hand;</xsl:if><xsl:value-of select="$StyleString"/></xsl:attribute>
		<xsl:if test="$Folder='true'">
			<IMG><xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/fold.gif</xsl:attribute>
			<xsl:attribute name="id">img<xsl:value-of select="./@id"/></xsl:attribute></IMG>&nbsp;
		</xsl:if>
		<STRONG><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></STRONG></SPAN>
		<xsl:if test="string-length(./row)>0">
		<SCRIPT>
			tableSortCol['<xsl:value-of select="$CompNo"/>'] = -1;
			tableSortColStatus['<xsl:value-of select="$CompNo"/>'] = 0;
			element['<xsl:value-of select="$CompNo"/>'] = new Array();
			element['<xsl:value-of select="$CompNo"/>'].id = <xsl:value-of select="./@id"/>;
			element['<xsl:value-of select="$CompNo"/>'].vid = <xsl:value-of select="./vid"/>;
			element['<xsl:value-of select="$CompNo"/>'].component="<xsl:value-of select="@uitype"/>";
			element['<xsl:value-of select="$CompNo"/>'].width = "<xsl:value-of select="./CompProperties/width"/>";
			element['<xsl:value-of select="$CompNo"/>'].multiselect = "<xsl:value-of select="./CompProperties/multiselect"/>";
			element['<xsl:value-of select="$CompNo"/>'].helptextcolumn = "<xsl:value-of select="./CompProperties/helptextcolumn"/>";
			element['<xsl:value-of select="$CompNo"/>'].innerbgcolor = "#DFDFDF";
			element['<xsl:value-of select="$CompNo"/>'].headerbgcolor = "#3399CC";
			element['<xsl:value-of select="$CompNo"/>'].headerforecolor = "#FFFFFF";
			element['<xsl:value-of select="$CompNo"/>'].group = "<xsl:value-of select="./CompProperties/group"/>";
			element['<xsl:value-of select="$CompNo"/>'].groupcolumn = -1;
			<xsl:if test="string-length(./CompProperties/groupbycolumnindex)>0">
				element['<xsl:value-of select="$CompNo"/>'].groupcolumn = <xsl:value-of select="./CompProperties/groupbycolumnindex"/>-1;
			</xsl:if>
			<xsl:if test="string-length(./CompProperties/innerbgcolor)>0">
				element['<xsl:value-of select="$CompNo"/>'].innerbgcolor = "<xsl:value-of select="./CompProperties/innerbgcolor"/>";
			</xsl:if>
			<xsl:if test="string-length(./CompProperties/headerbgcolor)>0">
				element['<xsl:value-of select="$CompNo"/>'].headerbgcolor = "<xsl:value-of select="./CompProperties/headerbgcolor"/>";
			</xsl:if>
			<xsl:if test="string-length(./CompProperties/headerforecolor)>0">
				element['<xsl:value-of select="$CompNo"/>'].headerforecolor = "<xsl:value-of select="./CompProperties/headerforecolor"/>";
			</xsl:if>
			<xsl:choose>
			<xsl:when test="string-length(./CompProperties/pagesize)>0">
				element['<xsl:value-of select="$CompNo"/>'].pagesize = <xsl:value-of select="./CompProperties/pagesize"/>;
			</xsl:when>
			<xsl:otherwise>
				element['<xsl:value-of select="$CompNo"/>'].pagesize = 20;
			</xsl:otherwise>
			</xsl:choose>
			element['<xsl:value-of select="$CompNo"/>'].currstartrow = 0;
			element['<xsl:value-of select="$CompNo"/>'].currendrow = 0 + element['<xsl:value-of select="$CompNo"/>'].pagesize;
			element['<xsl:value-of select="$CompNo"/>'].menucolumns = new Array();
			element['<xsl:value-of select="$CompNo"/>'].menulistcolumns = new Array();
			element['<xsl:value-of select="$CompNo"/>'].menulistcolumns = [<xsl:for-each select="./CompProperties/menulinkcolumns/LinkColumn">"<xsl:value-of select="."/>",</xsl:for-each>""];
			element['<xsl:value-of select="$CompNo"/>'].menucolumns = [<xsl:for-each select="./CompProperties/menucomponents/LinkComponent">"<xsl:value-of select="."/>",</xsl:for-each>""];
			element['<xsl:value-of select="$CompNo"/>'].columnHeaders = new Array();
			element['<xsl:value-of select="$CompNo"/>'].displayColumns = new Array();
			element['<xsl:value-of select="$CompNo"/>'].columnComponents = new Array();
			element['<xsl:value-of select="$CompNo"/>'].columnTypes = new Array();
			element['<xsl:value-of select="$CompNo"/>'].onHeaderClick = false;
			element['<xsl:value-of select="$CompNo"/>'].colSelected = new Array();
			<xsl:if test="string-length(./CompEvent[@ename='OnHeaderClick'])>0">
				element['<xsl:value-of select="$CompNo"/>'].onHeaderClick = true;
			</xsl:if>
			<xsl:for-each select="./ColumnHeaders[@Display='Yes']">
				element['<xsl:value-of select="$CompNo"/>'].colSelected[<xsl:value-of select="@colno"/>] = false;
				element['<xsl:value-of select="$CompNo"/>'].columnHeaders[<xsl:value-of select="@colno"/>] ="<xsl:value-of select="."/>";
				element['<xsl:value-of select="$CompNo"/>'].displayColumns[<xsl:value-of select="position()-1"/>] = <xsl:value-of select="@colno"/>;
				element['<xsl:value-of select="$CompNo"/>'].columnComponents[<xsl:value-of select="@colno"/>] ="<xsl:value-of select="@ColumnComponent"/>";
				element['<xsl:value-of select="$CompNo"/>'].columnTypes[<xsl:value-of select="@colno"/>] = "<xsl:value-of select="@ColumnType"/>";
			</xsl:for-each>
			element['<xsl:value-of select="$CompNo"/>'].rows = new Array();	
			<xsl:for-each select="./row">
				<xsl:variable name="RowNo" select="position()-1"/>
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>] = new Array();
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].rowNo = <xsl:value-of select="@rowno"/>;
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].changed = false;
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].rowSelected = false;
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns = new Array();
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].colComps = new Array();
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].colHelp = new Array();
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].colEnabled = new Array();
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].colBgcolor = new Array();
				<xsl:for-each select="./column[@Display='Yes']">
					<xsl:variable name="columnValue"><xsl:value-of select="." disable-output-escaping="yes"/></xsl:variable>
					<xsl:choose>
					<xsl:when test="./@Type='Date'">
						<xsl:choose>
							<xsl:when test="string-length(.)>0">
								element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="./day"/> <xsl:call-template name="month"/>,<xsl:value-of select="./year"/>";
							</xsl:when>
							<xsl:otherwise>
								element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="$columnValue"/>";
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="./@Type='TimeStamp'">
						<xsl:choose>
							<xsl:when test="string-length(.)>0">
							<xsl:variable name="columnDateValue">
								<xsl:value-of select="./day"/> <xsl:call-template name="month"/>,<xsl:value-of select="./year"/><xsl:text> </xsl:text>
								<xsl:if test="string-length(./hours)>0"> 
								<xsl:choose>
									<xsl:when test="./hours > 12"> <xsl:value-of select="./hours - 12"/>:<xsl:if test="string-length(./minutes)=1">0</xsl:if><xsl:value-of select="./minutes"/> PM</xsl:when>
									<xsl:otherwise> <xsl:value-of select="./hours"/>:<xsl:if test="string-length(./minutes)=1">0</xsl:if><xsl:value-of select="./minutes"/> AM</xsl:otherwise>
								</xsl:choose></xsl:if>
							</xsl:variable>
							element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="$columnDateValue"/>";
							</xsl:when>
							<xsl:otherwise>
								element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="$columnValue"/>";
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="$columnValue"/>";
					</xsl:otherwise>
					</xsl:choose>
						element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].colComps[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="@component"/>";
						element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].colHelp[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="@help"/>";
						element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].colEnabled[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="@enabled"/>";
						element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].colBgcolor[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="@bgcolor"/>".replace(" ","");
				</xsl:for-each>
			</xsl:for-each>
			<xsl:for-each select="./SelectedRowItem/SelectedRow">
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="."/>].changed = true;
			</xsl:for-each>
		</SCRIPT>
		<DIV align="left"><xsl:attribute name="id">F<xsl:value-of select="./@id"/></xsl:attribute> 
			<xsl:if test="$Folder='true'"><xsl:attribute name="STYLE">display:none;</xsl:attribute></xsl:if>
			<xsl:if test="not($Folder='true')">
			<SCRIPT>
			document.all['F<xsl:value-of select="$CompNo"/>'].innerHTML = DisplaySortedTable('<xsl:value-of select="$CompNo"/>','<xsl:value-of select="$StyleString"/>');
			</SCRIPT></xsl:if>			
		</DIV>
		</xsl:if>
	</TD>
</xsl:template>

<xsl:template name="ComboBox_3">
	<xsl:variable name="SelectedRI">
		<xsl:choose>
			<xsl:when test="string-length(./SelectedRowItem/SelectedRow) > 0"><xsl:value-of select="./SelectedRowItem/SelectedRow"/></xsl:when>
			<xsl:otherwise>-1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
<xsl:choose>
<xsl:when test="string-length(./CompProperties/subcomponent)>0 and ./CompProperties/subcomponent='true'">
<SCRIPT>
	SubComponent0['<xsl:value-of select="@name"/>']='ComboBox';
	SubComponent1['<xsl:value-of select="@name"/>']='<SELECT SIZE="1">
		<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute> <xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0"><xsl:attribute name="ONCLICK">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:attribute></xsl:if>
		<xsl:attribute name="ONCHANGE">javascript:  setVariableValue(<xsl:value-of select="./vid"/>,this.value);<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0">e<xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:if>';
	SubComponent2['<xsl:value-of select="@name"/>']='</xsl:attribute>';
	SubComponent3['<xsl:value-of select="@name"/>']='<xsl:if test="string-length(./item)>0"><xsl:for-each select="./item"> <OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./name" disable-output-escaping="yes"/></xsl:attribute> <xsl:value-of select="./name" disable-output-escaping="yes"/></OPTION></xsl:for-each>	</xsl:if><xsl:if test="string-length(./row)>0"><xsl:for-each select="./row"> <OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./column" disable-output-escaping="yes"/></xsl:attribute> <xsl:value-of select="./column" disable-output-escaping="yes"/></OPTION></xsl:for-each></xsl:if><OPTION VALUE="">  </OPTION></SELECT>';
</SCRIPT>
</xsl:when>
<xsl:otherwise>
	<TD NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>&nbsp;&nbsp;&nbsp;</SPAN>
	</TD><TD><SELECT SIZE="1">
		<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
		<xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute> <xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0"><xsl:attribute name="ONCLICK">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:attribute></xsl:if>
		<xsl:attribute name="ONCHANGE">javascript:  setVariableValue(<xsl:value-of select="./vid"/>,this.value);<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0">e<xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:if></xsl:attribute> 
		<OPTION VALUE="-1"><xsl:if test="$SelectedRI=-1">
			<xsl:attribute name="SELECTED"/></xsl:if>
			<xsl:text>  none  </xsl:text>
		</OPTION>
		<xsl:if test="string-length(./item)>0">
			<xsl:for-each select="./item"> 
				<OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./@itemno"/></xsl:attribute> 
					<xsl:if test="./@itemno=$SelectedRI">
						<xsl:attribute name="SELECTED"/></xsl:if>
					<xsl:value-of select="./name" disable-output-escaping="yes"/>
				</OPTION>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="string-length(./row)>0">
			<xsl:for-each select="./row"> 
				<OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./@rowno"/></xsl:attribute> 
					<xsl:if test="./@rowno=$SelectedRI">
						<xsl:attribute name="SELECTED"/></xsl:if>
					<xsl:value-of select="./column" disable-output-escaping="yes"/>
				</OPTION>
			</xsl:for-each>
		</xsl:if>
	</SELECT><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>&nbsp;<xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN></TD>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="Graph_3">
	<TD colspan="2" align="left">
	<applet VIEWASTEXT="">
		<xsl:attribute name="id">CHART<xsl:value-of select="./@id"/></xsl:attribute> 
		<xsl:choose>
			<xsl:when test="./CompProperties/charttype='line'">
				<xsl:attribute name="code">com.objectplanet.gui.LineChartApplet</xsl:attribute>
				<xsl:attribute name="archive">http://<xsl:value-of select="$Host"/>/template/com.objectplanet.gui.LineChartApplet.jar</xsl:attribute>
			</xsl:when>
			<xsl:when test="./CompProperties/charttype='pie'">
				<xsl:attribute name="code">com.objectplanet.gui.PieChartApplet</xsl:attribute>
				<xsl:attribute name="archive">http://<xsl:value-of select="$Host"/>/template/com.objectplanet.gui.PieChartApplet.jar</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="code">com.objectplanet.gui.BarChartApplet</xsl:attribute>
				<xsl:attribute name="archive">http://<xsl:value-of select="$Host"/>/template/com.objectplanet.gui.BarChartApplet.jar</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>	
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/width)>0">
				<xsl:attribute name="width"><xsl:value-of select="./CompProperties/width"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="width">350</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/height)>0">
				<xsl:attribute name="height"><xsl:value-of select="./CompProperties/height"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="height">250</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<param name="seriesCount"><xsl:attribute name="value"><xsl:value-of select="count(./row)"/></xsl:attribute></param>
		<param name="sampleCount"><xsl:attribute name="value"><xsl:value-of select="count(./ColumnHeaders)-1"/></xsl:attribute></param>
		<xsl:for-each select="./row">
			<xsl:variable name="lastColPos" select="count(./column)"/>
			<param><xsl:attribute name="name">sampleValues_<xsl:value-of select="./@rowno"/></xsl:attribute>
				<xsl:attribute name="value">
					<xsl:for-each select="./column"><xsl:if test="position()!=$lastColPos and position()!=1"><xsl:value-of select="."/>,</xsl:if></xsl:for-each>
					<xsl:value-of select="./column[position()=$lastColPos]"/>
				</xsl:attribute>
			</param>
		</xsl:for-each>
		
		<param name="barLabelsOn" value="true"/>
		<param name="legendOn" value="true"/>
		<param name="valueLinesOn" value="true"/>
		<param name="multiColorOn" value="true"/>
		<param name="barOutlineOff" value="true"/>
		<param name="chartTitle"><xsl:attribute name="value"><xsl:value-of select="./CompProperties/caption"/></xsl:attribute></param>
		<param name="sampleColors"><xsl:attribute name="value"><xsl:value-of select="./CompProperties/seriescolors"/></xsl:attribute></param>
		<param name="seriesLabels">
			<xsl:variable name="lastRowPos" select="count(./row)"/>
			<xsl:attribute name="value">
				<xsl:for-each select="./row"><xsl:if test="@rowno!=$lastRowPos"><xsl:value-of select="./column[position()=1]"/>,</xsl:if></xsl:for-each>
				<xsl:value-of select="./row[@rowno=$lastRowPos]/column[position()=1]"/>
			</xsl:attribute>
		</param>
		<param name="sampleLabels">
			<xsl:variable name="lastHeadPos" select="count(./ColumnHeaders)"/>
			<xsl:attribute name="value">
				<xsl:for-each select="./ColumnHeaders"><xsl:if test="position()!=1 and position()!=$lastHeadPos"><xsl:value-of select="."/>,</xsl:if></xsl:for-each>
				<xsl:value-of select="./ColumnHeaders[position()=$lastHeadPos]"/>
			</xsl:attribute>
		</param>
		<param name="barWidth" value="1"/>
		<param name="titleFont" value="Arial, bold, 16"/>
		<param name="automaticRefreshTime" value="10000"/>
		<param name="multiSeriesOn" value="true"/>
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/bgcolor)>0">
				<param name="background"><xsl:attribute name="value"><xsl:value-of select="./CompProperties/bgcolor"/></xsl:attribute></param>
			</xsl:when>
			<xsl:otherwise>
				<param name="background" value="#FFFFFF"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/innerbgcolor)>0">
				<param name="chartBackground"><xsl:attribute name="value"><xsl:value-of select="./CompProperties/innerbgcolor"/></xsl:attribute></param>
			</xsl:when>
			<xsl:otherwise>
				<param name="chartBackground" value="#FFFFFF"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="string-length(./CompProperties/fontcolor)>0">
			<param name="chartForeground"><xsl:attribute name="value"><xsl:value-of select="./CompProperties/fontcolor"/></xsl:attribute></param>
			<param name="foreground"><xsl:attribute name="value"><xsl:value-of select="./CompProperties/fontcolor"/></xsl:attribute></param>
		</xsl:if>
	</applet></TD>
</xsl:template>

<xsl:template name="ListBox_3">
	<xsl:variable name="SelectedRI">
		<xsl:choose>
			<xsl:when test="string-length(./SelectedRowItem/SelectedRow) > 0"><xsl:value-of select="./SelectedRowItem/SelectedRow"/></xsl:when>
			<xsl:otherwise>-1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<TD valign="top" NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>&nbsp;&nbsp;&nbsp;</SPAN>
	</TD><TD><SELECT MULTIPLE=""><xsl:attribute name="SIZE">
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/height)>0">
				<xsl:value-of select="./CompProperties/height"/>
			</xsl:when>
			<xsl:otherwise>5</xsl:otherwise>
		</xsl:choose></xsl:attribute>
		<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
		<xsl:if test="./CompProperties[enabled='false']"><xsl:attribute name="DISABLED"/></xsl:if>
		<xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute> <xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0"><xsl:attribute name="ONCLICK">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:attribute></xsl:if>
		<xsl:attribute name="ONCHANGE">javascript:  setVariableValue(<xsl:value-of select="./vid"/>,this.value+\':;\');<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:if></xsl:attribute> 
		<OPTION VALUE="-1"><xsl:if test="$SelectedRI=-1">
			<xsl:attribute name="SELECTED"/></xsl:if>
			<xsl:text>  none  </xsl:text>
		</OPTION>
		<xsl:if test="string-length(./item)>0">
			<xsl:for-each select="./item"> 
				<OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./@itemno"/></xsl:attribute> 
					<xsl:if test="./@itemno=$SelectedRI">
						<xsl:attribute name="SELECTED"/></xsl:if>
					<xsl:value-of select="./name" disable-output-escaping="yes"/>
				</OPTION>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="string-length(./row)>0">
			<xsl:for-each select="./row"> 
				<OPTION><xsl:attribute name="VALUE"><xsl:value-of select="./@rowno"/></xsl:attribute> 
					<xsl:if test="./@rowno=$SelectedRI">
						<xsl:attribute name="SELECTED"/></xsl:if>
					<xsl:value-of select="./column" disable-output-escaping="yes"/>
				</OPTION>
			</xsl:for-each>
		</xsl:if>
	</SELECT><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>&nbsp;<xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN></TD>
</xsl:template>

<xsl:template name="Tree_3">
	<xsl:variable name="CompVID" select="./vid"/>
	<xsl:variable name="CompNo" select="./@id"/>
	<xsl:variable name="TreeChildCol_1" select="(./CompProperties/treechild)-1"/>
	<xsl:variable name="TreeParentCol_1" select="(./CompProperties/treeparent)-1"/>
	<xsl:variable name="DisplayCol_1" select="(./CompProperties/displaycolumnindex)-1"/>
	<xsl:variable name="BoundCol_1" select="(./CompProperties/boundcolumnindex)-1"/>
	<xsl:variable name="RootRow" select="'0'"/>

		<xsl:if test="string-length(./row)>0">
		<SCRIPT>
			element['<xsl:value-of select="$CompNo"/>'] = new Array();
			element['<xsl:value-of select="$CompNo"/>'].RootRow=<xsl:value-of select="$RootRow"/>;
			element['<xsl:value-of select="$CompNo"/>'].id = <xsl:value-of select="./@id"/>;
			element['<xsl:value-of select="$CompNo"/>'].vid = <xsl:value-of select="./vid"/>;
			element['<xsl:value-of select="$CompNo"/>'].displaycolumnindex = <xsl:value-of select="$DisplayCol_1"/>;
			element['<xsl:value-of select="$CompNo"/>'].boundcolumnindex = <xsl:value-of select="$BoundCol_1"/>;
			element['<xsl:value-of select="$CompNo"/>'].treechild = <xsl:value-of select="$TreeChildCol_1"/>;
			element['<xsl:value-of select="$CompNo"/>'].treeparent = <xsl:value-of select="$TreeParentCol_1"/>;
			element['<xsl:value-of select="$CompNo"/>'].enabled = '<xsl:value-of select="./CompProperties/enabled"/>';
			element['<xsl:value-of select="$CompNo"/>'].multiselect = '<xsl:value-of select="./CompProperties/multiselect"/>';
			element['<xsl:value-of select="$CompNo"/>'].selectedValue = '<xsl:value-of select="./SelectedRowItem/SelectedRow"/>';
			element['<xsl:value-of select="$CompNo"/>'].rows = new Array();	
			<xsl:for-each select="./row">
				<xsl:variable name="RowNo" select="./@rowno"/>
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>] = new Array();
				element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns = new Array();				
				<xsl:for-each select="./column">				
					element['<xsl:value-of select="$CompNo"/>'].rows[<xsl:value-of select="$RowNo"/>].columns[<xsl:value-of select="./@colno"/>]="<xsl:value-of select="." disable-output-escaping="yes"/>";
				</xsl:for-each>
			</xsl:for-each>
		</SCRIPT></xsl:if>
	<TD colspan="2" align="left">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute>
		<SPAN><xsl:attribute name="STYLE">cursor:hand;<xsl:call-template name="StylingValue"/></xsl:attribute>
		<xsl:if test="string-length(./row)>0">
			<xsl:attribute name="onclick">ViewTreeComponent('<xsl:call-template name="StylingValue"/>','<xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$TreeChildCol_1]"/>','<xsl:value-of select="$CompNo"/>',1,'<xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$BoundCol_1]"/>',-1)</xsl:attribute>
			<IMG><xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/plus.gif</xsl:attribute><xsl:attribute name="id">TIMG<xsl:value-of select="./@id"/>0<xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$BoundCol_1]"/><xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$TreeChildCol_1]"/>-1</xsl:attribute></IMG>
		</xsl:if>
		<xsl:if test="string-length(./row)=0"><IMG><xsl:attribute name="src">http://<xsl:value-of select="$Host"/>/template/images/minus.gif</xsl:attribute></IMG></xsl:if>
		&nbsp;&nbsp;<xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if>
		<SPAN STYLE="color:blue;text-decoration:underline;">
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/caption)>0"><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$DisplayCol_1]"/></xsl:otherwise>
		</xsl:choose></SPAN></SPAN>
		<DIV style="display:none;"><xsl:attribute name="id">CT<xsl:value-of select="./@id"/>0<xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$BoundCol_1]"/><xsl:value-of select="./row[@rowno=$RootRow]/column[@colno=$TreeChildCol_1]"/>-1</xsl:attribute></DIV>
	</TD>
</xsl:template>

<xsl:template name="Anchor_4">
	<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<xsl:variable name="CompVID" select="./vid"/>
	<xsl:variable name="CompNo" select="./@id"/>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 
		<SPAN>
		<xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute>
		<STRONG><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></STRONG></SPAN>
		<DIV align="left"><xsl:attribute name="id">F<xsl:value-of select="./@id"/></xsl:attribute> 
			<xsl:if test="@uitype='Anchor' and count(./item)>0">
				<TABLE WIDTH="100%" border="0" cellspacing="0" cellpadding="3">
				<xsl:choose>
					<xsl:when test="./CompProperties/subitemposition='horizontal'">
						<TR><TD>
							<xsl:for-each select="./item"> <xsl:if test="string-length(./name)>0">
								<SPAN STYLE="color:blue;text-decoration:underline;cursor:hand;">
								<xsl:attribute name="ONCLICK">javascript:if(!disableAnchor){DisableMenu(); setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>); <xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:if>RequiredFldChk();}</xsl:attribute> 
								<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute>
								<xsl:value-of select="./name" disable-output-escaping="yes"/></SPAN></SPAN>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</xsl:if>
							</xsl:for-each>
						</TD></TR>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="./item"> <xsl:if test="string-length(./name)>0">
							<TR><TD>
							<SPAN STYLE="color:blue;text-decoration:underline;cursor:hand;">
							<xsl:attribute name="ONCLICK">javascript:if(!disableAnchor){DisableMenu(); setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>); <xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:if>RequiredFldChk();}</xsl:attribute> 
							<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute>
							<xsl:value-of select="./name" disable-output-escaping="yes"/></SPAN></SPAN>
							</TD></TR></xsl:if>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
				</TABLE>
			</xsl:if>
		</DIV>
	</TD>
</xsl:template>


<xsl:template name="Button_4">
	<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<xsl:variable name="CompVID" select="./vid"/>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></SPAN><BR/>
		<TABLE WIDTH="100%" border="0" cellspacing="0" cellpadding="3">
		<xsl:choose>
			<xsl:when test="./CompProperties/subitemposition='horizontal'">
				<TR><TD>
				<xsl:for-each select="./item"> <xsl:if test="string-length(./name)>0">
					<INPUT TYPE="button">
					<xsl:if test="./CompProperties[enabled='false']"><xsl:attribute name="DISABLED"/></xsl:if>
					<xsl:attribute name="VALUE"><xsl:value-of select="./name" disable-output-escaping="yes"/></xsl:attribute> 
					<xsl:attribute name="ONCLICK">javascript:DisableMenu();   { setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>); RequiredFldChk();<xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:if>}</xsl:attribute> 
					<xsl:attribute name="STYLE">width:<xsl:value-of select="./CompProperties/width"/>;<xsl:value-of select="$StyleString"/>;height:<xsl:value-of select="./CompProperties/height"/>;background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/>
					</xsl:attribute> </INPUT>&nbsp;&nbsp;
					</xsl:if>
				</xsl:for-each>
				</TD></TR>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="./item"> <xsl:if test="string-length(./name)>0">
					<TR><TD>
					<INPUT TYPE="button">
					<xsl:if test="./CompProperties[enabled='false']"><xsl:attribute name="DISABLED"/></xsl:if>
					<xsl:attribute name="VALUE"><xsl:value-of select="./name" disable-output-escaping="yes"/></xsl:attribute> 
					<xsl:attribute name="ONCLICK">javascript:DisableMenu();   { setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>); RequiredFldChk();<xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0">e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:if>}</xsl:attribute> 
					<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/>;height:<xsl:value-of select="./CompProperties/height"/>;width:<xsl:value-of select="./CompProperties/width"/>;<xsl:value-of select="$StyleString"/>;
					</xsl:attribute></INPUT>
					</TD></TR></xsl:if>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		</TABLE>
	</TD>
</xsl:template>

<xsl:template name="CheckBox_4">
	<xsl:choose>
	<xsl:when  test="string-length(./row)>0">
		<xsl:call-template name="AnchorDisplayGrid_3"/>
	</xsl:when>
	<xsl:otherwise>
	<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<xsl:variable name="CompID" select="./@id"/>
	<xsl:variable name="CompValue" select="./vValue"/>
	<xsl:variable name="CompVID" select="./vid"/>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if> <STRONG><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></STRONG></SPAN><BR/>
		<TABLE WIDTH="100%" border="0" cellspacing="0" cellpadding="3">
		<xsl:variable name="Enable" select="./CompProperties/enabled"/>
		<xsl:choose>
			<xsl:when test="./CompProperties/subitemposition='horizontal'">
				<TR><TD>
					<xsl:for-each select="./item"> 
						<INPUT TYPE="checkbox">
						<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0"><xsl:attribute name="ONCLICK">javascript: e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:attribute></xsl:if>
						<xsl:attribute name="ID">LCB<xsl:value-of select="$CompID"/><xsl:value-of select="./@itemno"/></xsl:attribute> 
						<xsl:if test="$Enable='false'"><xsl:attribute name="DISABLED"/></xsl:if>
						<xsl:if test="$CompValue=./@itemno"><xsl:attribute name="CHECKED"/></xsl:if>
						<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:attribute name="ONCHANGE">javascript: e<xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:attribute></xsl:if></INPUT>
						<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute>
						<xsl:value-of select="./name" disable-output-escaping="yes"/></SPAN><xsl:text></xsl:text>
					</xsl:for-each>
				</TD></TR>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="./item"> 
					<TR><TD WIDTH="2%">
					<INPUT TYPE="checkbox">
					<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute><xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0"><xsl:attribute name="ONCLICK">javascript: e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:attribute></xsl:if>
					<xsl:attribute name="ID">LCB<xsl:value-of select="$CompID"/><xsl:value-of select="./@itemno"/></xsl:attribute> 
					<xsl:if test="$Enable='false'"><xsl:attribute name="DISABLED"/></xsl:if>
					<xsl:if test="$CompValue=./@itemno"><xsl:attribute name="CHECKED"/></xsl:if>
					<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:attribute name="ONCHANGE">javascript: e<xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:attribute></xsl:if></INPUT></TD><TD><xsl:text> </xsl:text>
					<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:value-of select="./name" disable-output-escaping="yes"/></SPAN>
					</TD></TR>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		</TABLE>
	</TD>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="RadioGroup_4">
	<xsl:choose>
	<xsl:when  test="string-length(./row)>0">
		<xsl:call-template name="AnchorDisplayGrid_3"/>
	</xsl:when>
	<xsl:otherwise>
	<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<xsl:variable name="CompID" select="./@id"/>
	<xsl:variable name="CompValue" select="./vValue"/>
	<xsl:variable name="CompVID" select="./vid"/>
	<TD colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if> <STRONG><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/></STRONG></SPAN><BR/>
		<TABLE WIDTH="100%" border="0" cellspacing="0" cellpadding="3">
		<xsl:variable name="Enable" select="./CompProperties/enabled"/>
		<xsl:variable name="eOnClick" select="./CompEvent[@ename='OnClick']"/>
		<xsl:variable name="eOnChange" select="./CompEvent[@ename='OnChange']"/>
		<xsl:choose>
			<xsl:when test="./CompProperties/subitemposition='horizontal'">
				<TR><TD>
					<xsl:for-each select="./item"> 
						<INPUT TYPE="radio">
						<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
						<xsl:attribute name="NAME">rdBtn<xsl:value-of select="$CompID"/></xsl:attribute> 
						<xsl:if test="$Enable='false'"><xsl:attribute name="DISABLED"/></xsl:if>
						<xsl:attribute name="ONCLICK">javascript:    setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>);<xsl:if test="string-length($eOnClick)>0">e<xsl:value-of select="$CompID"/>_OnClick(0,0,0);</xsl:if></xsl:attribute><xsl:attribute name="VALUE"><xsl:value-of select="./@itemno"/></xsl:attribute> 
						<xsl:if test="$CompValue=./@itemno"><xsl:attribute name="CHECKED"/></xsl:if>
						<xsl:if test="string-length($eOnChange)>0"><xsl:attribute name="ONCHANGE">javascript: <xsl:value-of select="$CompID"/>_OnChange(0,0,0);</xsl:attribute></xsl:if></INPUT>
						<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute>
						<xsl:value-of select="./name" disable-output-escaping="yes"/></SPAN><xsl:text></xsl:text>
					</xsl:for-each>
				</TD></TR>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="./item"> 
					<TR><TD WIDTH="2%">
					<INPUT TYPE="radio">
					<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
					<xsl:attribute name="NAME">rdBtn<xsl:value-of select="$CompID"/></xsl:attribute> 
					<xsl:if test="$Enable='false'"><xsl:attribute name="DISABLED"/></xsl:if>
					<xsl:attribute name="ONCLICK">javascript:    setVariableValue(<xsl:value-of select="$CompVID"/>,<xsl:value-of select="./@itemno"/>);<xsl:if test="string-length($eOnClick)>0">e<xsl:value-of select="$CompID"/>_OnClick(0,0,0);</xsl:if></xsl:attribute><xsl:attribute name="VALUE"><xsl:value-of select="./@itemno"/></xsl:attribute>
					<xsl:if test="$CompValue=./@itemno"><xsl:attribute name="CHECKED"/></xsl:if>
					<xsl:if test="string-length($eOnChange)>0"><xsl:attribute name="ONCHANGE">javascript: <xsl:value-of select="$CompID"/>_OnChange(0,0,0);</xsl:attribute></xsl:if></INPUT></TD><TD><xsl:text> </xsl:text>
					<SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:value-of select="./name" disable-output-escaping="yes"/></SPAN>
					</TD></TR>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		</TABLE>
	</TD>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="Label_6">
	<TD valign="top" colspan="2">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute>  
		<SPAN> <xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>
		<xsl:if test="string-length(./CompProperties/caption)>0">
			<xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>
		</xsl:if><xsl:if test="string-length(./vValue)>0"><BR/>
			<xsl:value-of select="./vValue" disable-output-escaping="yes"/>
		</xsl:if></SPAN>
	</TD>
</xsl:template>

<xsl:template name="TextBox_6">
<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
<xsl:choose>
<xsl:when test="string-length(./CompProperties/subcomponent)>0 and ./CompProperties/subcomponent='true'">
<SCRIPT>
	SubComponent0['<xsl:value-of select="@name"/>']='TextBox';
	SubComponent1['<xsl:value-of select="@name"/>']='<INPUT TYPE="text"><xsl:choose><xsl:when test="string-length(./CompProperties/width)>0"><xsl:attribute name="SIZE"><xsl:value-of select="./CompProperties/width"/></xsl:attribute></xsl:when><xsl:otherwise><xsl:attribute name="SIZE">15</xsl:attribute></xsl:otherwise></xsl:choose><xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute><xsl:if test="./CompProperties/enabled='false'"><xsl:attribute name="DISABLED"/></xsl:if><xsl:attribute name="ONCHANGE">javascript:  ';
	SubComponent2['<xsl:value-of select="@name"/>']='</xsl:attribute> ';
	SubComponent3['<xsl:value-of select="@name"/>']='<xsl:if test="string-length(./CompEvent[@ename='OnClick'])>0"><xsl:attribute name="OnClick">javascript: e<xsl:value-of select="./@id"/>_OnClick(0,0,0);</xsl:attribute></xsl:if><xsl:attribute name="MAXLENGTH"><xsl:value-of select="./CompProperties/maxlength"/></xsl:attribute></INPUT> ';
</SCRIPT>
</xsl:when>
<xsl:otherwise>
	<TD NOWRAP="" width="5%" valign="top">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>&nbsp;&nbsp;&nbsp;</SPAN></TD>
		<xsl:choose>
			<xsl:when test="not(./CompProperties/enabled='false')">
				<TD><INPUT>
				<xsl:attribute name="TYPE">
				<xsl:choose>
					<xsl:when test="./CompProperties/password='true'">password</xsl:when>
					<xsl:otherwise>text</xsl:otherwise>
				</xsl:choose></xsl:attribute> 
				<xsl:attribute name="id"><xsl:value-of select="./@id"/></xsl:attribute> 
				<xsl:choose>
					<xsl:when test="string-length(./CompProperties/width)>0">
						<xsl:attribute name="SIZE"><xsl:value-of select="./CompProperties/width"/></xsl:attribute> 	
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="SIZE">15</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
				<xsl:attribute name="VALUE"><xsl:if test="not(./CompProperties/password='true')"><xsl:value-of select="./vValue"/></xsl:if></xsl:attribute> 
				<xsl:attribute name="MAXLENGTH"><xsl:value-of select="./CompProperties/maxlength"/></xsl:attribute> 
				<xsl:attribute name="NAME"><xsl:value-of select="./@id"/></xsl:attribute> 
				<xsl:attribute name="ONCHANGE">
				<xsl:choose>
					<xsl:when test="./@type='6'">
						javascript:    setEditValue(<xsl:value-of select="./vid"/>,this.value,0<xsl:value-of select="./CompProperties/minvalue"/>,0<xsl:value-of select="./CompProperties/maxvalue"/>);
					</xsl:when>
					<xsl:otherwise>
						javascript:    setVariableValue(<xsl:value-of select="./vid"/>,this.value);
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:if></xsl:attribute> 
				</INPUT><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>&nbsp;<xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN>
				</TD>
			</xsl:when>
			<xsl:otherwise>
				<TD><SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><B><xsl:value-of select="./vValue" disable-output-escaping="yes"/></B><xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN></TD>
			</xsl:otherwise>
		</xsl:choose>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="TextArea_6">
<xsl:variable name="StyleString"><xsl:call-template name="StylingValue"/></xsl:variable>
	<TD valign="top" NOWRAP="" width="5%">
		<xsl:attribute name="id">TD<xsl:value-of select="./@id"/></xsl:attribute> 		
		<SPAN> <xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><xsl:if test="./CompProperties/required='true'"><SUP>* </SUP></xsl:if><xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>&nbsp;&nbsp;&nbsp;</SPAN></TD>
		<xsl:choose>
			<xsl:when test="not(./CompProperties/enabled='false')">
				<TD valign="top"><TEXTAREA><xsl:attribute name="ID"><xsl:value-of select="./@id"/></xsl:attribute>
					<xsl:choose>
						<xsl:when test="string-length(./CompProperties/width)>0">
							<xsl:attribute name="cols"><xsl:value-of select="./CompProperties/width"/></xsl:attribute> 
						</xsl:when>
						<xsl:otherwise><xsl:attribute name="cols">15</xsl:attribute> </xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="string-length(./CompProperties/height)>0">
							<xsl:attribute name="rows"><xsl:value-of select="./CompProperties/height"/></xsl:attribute> 
						</xsl:when>
						<xsl:otherwise><xsl:attribute name="rows">2</xsl:attribute></xsl:otherwise>
					</xsl:choose>
					<xsl:attribute name="STYLE">background-color:<xsl:value-of select="./CompProperties/innerbgcolor" disable-output-escaping="yes"/></xsl:attribute>
					<xsl:attribute name="VALUE"><xsl:value-of select="./vValue" disable-output-escaping="yes"/></xsl:attribute> 
					<xsl:attribute name="NAME"><xsl:value-of select="./@id"/></xsl:attribute> 
					<xsl:attribute name="ONCHANGE">javascript: setVariableValue(<xsl:value-of select="./vid"/>,this.innerHTML);
					<xsl:if test="string-length(./CompEvent[@ename='OnChange'])>0"><xsl:value-of select="./@id"/>_OnChange(0,0,0);</xsl:if></xsl:attribute> 
					<xsl:value-of select="./vValue" disable-output-escaping="yes"/>
				</TEXTAREA><SPAN><xsl:attribute name="STYLE"><xsl:call-template name="StylingValue"/></xsl:attribute>&nbsp;<xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN></TD>
			</xsl:when>
			<xsl:otherwise>
				<TD valign="top"><SPAN><xsl:attribute name="STYLE"><xsl:value-of select="$StyleString"/></xsl:attribute><B><xsl:value-of select="./vValue" disable-output-escaping="yes"/></B>&nbsp;<xsl:value-of select="./CompProperties/captionsuffix" disable-output-escaping="yes"/></SPAN></TD>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>

<xsl:template match="InteractionNode/Properties">
	<xsl:if test="string-length(./title)>0">
	document.title = "<xsl:value-of select="./title"/>";
	</xsl:if>
	<xsl:if test="string-length(./bgcolor)>0">
	document.body.style.backgroundColor = "<xsl:value-of select="./bgcolor"/>";
	</xsl:if>
</xsl:template>

<xsl:template name="StylingValue">
	<xsl:if test="string-length(./CompProperties/fontcolor)>0">color:<xsl:value-of select="./CompProperties/fontcolor"/>;</xsl:if><xsl:choose><xsl:when test="string-length(./CompProperties/fontface)>0">font-family:<xsl:value-of select="./CompProperties/fontface"/>;</xsl:when><xsl:when test="string-length($Properties/fontface)>0">font-family:<xsl:value-of select="$Properties/fontface"/>;</xsl:when><xsl:otherwise>font-family:arial,verdana;</xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="string-length(./CompProperties/fontsize)>0">font-size:<xsl:value-of select="./CompProperties/fontsize"/>px;</xsl:when><xsl:when test="string-length($Properties/fontsize)>0">font-size:<xsl:value-of select="$Properties/fontsize"/>px;</xsl:when><xsl:otherwise>font-size:12px;</xsl:otherwise></xsl:choose><xsl:if test="string-length(./CompProperties/fontstyle)>0">font-style:<xsl:value-of select="./CompProperties/fontstyle"/>;</xsl:if><xsl:if test="string-length(./CompProperties/fontweight)>0">font-weight:<xsl:value-of select="./CompProperties/fontweight"/>;</xsl:if>text-decoration:<xsl:value-of select="./CompProperties/textdecoration"/>;</xsl:template>

<xsl:template name="month">
<xsl:variable name="CurrMonth" select="./vValue/month"/>
	<xsl:choose>
		<xsl:when test="$CurrMonth=1"> Jan</xsl:when>
		<xsl:when test="$CurrMonth=2"> Feb</xsl:when>
		<xsl:when test="$CurrMonth=3"> March</xsl:when>
		<xsl:when test="$CurrMonth=4"> April</xsl:when>
		<xsl:when test="$CurrMonth=5"> May</xsl:when>
		<xsl:when test="$CurrMonth=6"> June</xsl:when>
		<xsl:when test="$CurrMonth=7"> July</xsl:when>
		<xsl:when test="$CurrMonth=8"> Aug</xsl:when>
		<xsl:when test="$CurrMonth=9"> Sep</xsl:when>
		<xsl:when test="$CurrMonth=10"> Oct</xsl:when>
		<xsl:when test="$CurrMonth=11"> Nov</xsl:when>
		<xsl:when test="$CurrMonth=12"> Dec</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="generate_script">
<SCRIPT>
var id = '<xsl:value-of select="InteractionNode/@navigatorID"/>';
var nodeID = <xsl:value-of select="InteractionNode/@id"/>;
var subProcessID = '<xsl:value-of select="InteractionNode/SubProcess/@id"/>';
var hostIP = '<xsl:value-of select="$Host"/>';
<![CDATA[
function vpair(v,val)
{
  this.vid = v;
  this.value = val;
}
function compProps(Id,comp,minval,maxval,visibl,req,cap,t,ms,tmsg,enbled)
{
  this.component = comp;
  this.id = Id;
  if(minval=='')  	this.minvalue = 0;
  else	this.minvalue = minval.valueOf();
  if(maxval=='') 	this.maxvalue = 0;
  else	this.maxvalue = maxval.valueOf();
  this.visible = visibl;
  this.required = req;
  this.caption = cap;
  this.enabled=enbled;
  this.type = t;
  this.multiselect = ms;
  this.transientmessage = tmsg;
}
var CurrComp;
var hints = new Array();
]]>

<xsl:if test="string-length(InteractionNode/SubProcess/@id)>0">
var paramMap = new Array();
var parentIDs = new Array();
var pIDsCount = 0;
<xsl:for-each select="InteractionNode/SubProcess/param">
	paramMap['<xsl:value-of select="@parentid"/>']=new vpair(<xsl:value-of select="@subid"/>,"<xsl:value-of select="@value"/>");
	parentIDs[pIDsCount]='<xsl:value-of select="@parentid"/>';
	pIDsCount++;
</xsl:for-each>
</xsl:if>

var selectedValues = [
	<xsl:for-each select="InteractionNode/selectedValues/SelectedValue">
		new vpair(<xsl:value-of select="./@vid"/>,"<xsl:if test="not(./CompProperties/password='true')"><xsl:value-of select="." disable-output-escaping="yes"/></xsl:if>"),
	</xsl:for-each>
	new vpair(0,'')
];
var components = [
	<xsl:for-each select="$AllUIBlocks">
	<xsl:for-each select="./Component">
		new compProps(<xsl:value-of select="./@id"/>,"<xsl:value-of select="@uitype"/>","<xsl:value-of select="./CompProperties/minvalue"/>",
		"<xsl:value-of select="./CompProperties/maxvalue"/>","<xsl:value-of select="./CompProperties/visible"/>","<xsl:value-of select="./CompProperties/required"/>",
		"<xsl:value-of select="./CompProperties/caption" disable-output-escaping="yes"/>",<xsl:value-of select="./@type"/>,"<xsl:value-of select="./CompProperties/multiselect"/>",
		"<xsl:value-of select="./CompProperties/transientmessage"/>","<xsl:value-of select="./CompProperties/enabled"/>"),
	</xsl:for-each>
	</xsl:for-each>
	new compProps(0,'','','','','','',-1,'','')
];
var element = new Array();
var tableSortCol = new Array();
var tableSortColStatus = new Array();
var disableAnchor = false;
var calenders = new Array();
var menuListComponent1 = new Array();
var menuListComponent2 = new Array();
var SubComponent0 = new Array();
var SubComponent1 = new Array();
var SubComponent2 = new Array();
var SubComponent3 = new Array();
var calCount = 0;

<xsl:for-each select="$AllUIBlocks">
<xsl:for-each select="./Component">
<xsl:variable name="CompID" select="@id"/>
<xsl:for-each select="./CompEvent">
	function e<xsl:value-of select="$CompID"/>_<xsl:value-of select="@ename"/>(param1,param2,param3)
	{
		<xsl:value-of select="."/>
	}
</xsl:for-each>
</xsl:for-each>
</xsl:for-each>

<xsl:if test="string-length($AllUIBlocks/Component/CompProperties[folder='true'])>0">
function folderComponent(currID,CompStyle)
{
	var objIMG = document.all['img'+currID];
	var objDIV = document.all['F'+currID];
	if(objDIV.style.display=='none')
	{
		if(objDIV.innerHTML.length&lt;=0)
			objDIV.innerHTML = DisplaySortedTable(currID, CompStyle);
		objDIV.style.display="";
		objIMG.src="http://"+hostIP+"/template/images/open.gif";
	}
	else
	{
		objDIV.style.display="none";
		objIMG.src="http://"+hostIP+"/template/images/fold.gif";
	}
}
</xsl:if>
<xsl:if test="string-length($AllUIBlocks/Component/row)>0">
<![CDATA[
function changeto(highlightcolor)
{
	source=event.srcElement;
	if(source.tagName=="A" || source.tagName=="INPUT")source=source.parentElement;
	if(source.parentElement.tagName=="TD") source=source.parentElement;
	if(source.tagName=="TD")
	{
		source=source.parentElement;
		if (source.style.backgroundColor!=highlightcolor)  source.style.backgroundColor=highlightcolor;
	}
}

function changeback(originalcolor)
{
	source=event.srcElement;
	if(source.tagName=="A" || source.tagName=="INPUT")source=source.parentElement;
	if(source.parentElement.tagName=="TD") source=source.parentElement;
	if (source.tagName=="TD")
	{
		source=source.parentElement;
		source.style.backgroundColor=originalcolor;
	}
}
function sortTable(x, i, ComponentStyle)
{
	var ascSort = true; // 0 - asc sort; 1 - dec sort
	var sorted = false;
	var listLen = element[i].rows.length;

	if(tableSortCol[i]==x)	
	{	
		sorted = true;
		if(tableSortColStatus[i]==1)	ascSort = false;
	}
	
	if(!sorted)
	{
		if(element[i].columnTypes[x]=='Number' || element[i].columnTypes[x]=='Integer')	numberMergeSort(i, x, 0, element[i].rows.length); // Merge Sort....
		else if(element[i].columnTypes[x]=='Date' || element[i].columnTypes[x]=='TimeStamp') dateMergeSort(i, x, 0, element[i].rows.length); // Merge Sort....
		else	stringMergeSort(i, x, 0, element[i].rows.length); // Merge Sort....
	}
	else
	{
		var count = Math.floor(element[i].rows.length / 2);
		var end = element[i].rows.length-1;
		for(var start=0; start<count; start++)
		{
			var temp = element[i].rows[start];
			element[i].rows[start] = element[i].rows[end];
			element[i].rows[end] = temp;
			end--;
		}
	}
	document.all['F' + i].innerHTML = DisplaySortedTable(i, ComponentStyle);

	for(a=0;a<element[i].displayColumns.length;a++)
	{
		document.all['THDR' +  i + element[i].displayColumns[a]].style.backgroundColor=element[i].headerbgcolor;
		document.all['IMGHDR' +  i + element[i].displayColumns[a]].src="http://"+hostIP+"/template/images/harrow.gif";
		document.all['IMGHDR' +  i + element[i].displayColumns[a]].style.display="none";
	}
	var hdrCol = document.all['THDR' +  i + x];
	var hdrImg = document.all['IMGHDR' + i + x];
	hdrImg.style.display = "";
	hdrCol.style.backgroundColor="#387272";
	if(ascSort)	
	{
		tableSortCol[i] = x;
		tableSortColStatus[i] = 1;
		hdrImg.src = "http://"+hostIP+"/template/images/harrow.gif";
	}
	else
	{
		tableSortColStatus[i]=0;
		hdrImg.src = "http://"+hostIP+"/template/images/varrow.gif";
	}	
}

function stringMergeSort(x, c, start, end)
{
  if (end-start > 1)  // Recursively merge sort
  {
    // Merge sort each half of a
    var mid=parseInt((start+end)/2);
    stringMergeSort(x, c, start, mid);
    stringMergeSort(x, c, mid, end);
   
    // Merge the two halves
    var b = new Array();  // Temporary array
    var j1=start;
    var j2=mid;
    for (var i=0; i<end-start; ++i)
    {
      if (j1>=mid)        b[i]=element[x].rows[j2++];
      else if (j2>=end)        b[i]=element[x].rows[j1++];
      else if (element[x].rows[j1].columns[c].toLowerCase() < element[x].rows[j2].columns[c].toLowerCase())        b[i]=element[x].rows[j1++];
      else        b[i]=element[x].rows[j2++];
    }
    for (i=0; i<end-start; ++i)      element[x].rows[i+start]=b[i]; 
  }
}
function numberMergeSort(x, c, start, end)
{
  if (end-start > 1)  // Recursively merge sort
  {
    // Merge sort each half of a
    var mid=parseInt((start+end)/2);
    stringMergeSort(x, c, start, mid);
    stringMergeSort(x, c, mid, end);
   
    // Merge the two halves
    var b = new Array();  // Temporary array
    var j1=start;
    var j2=mid;
    for (var i=0; i<end-start; ++i)
    {
      if (j1>=mid)        b[i]=element[x].rows[j2++];
      else if (j2>=end)        b[i]=element[x].rows[j1++];
      else if ((element[x].rows[j1].columns[c]-1) < (element[x].rows[j2].columns[c]-1))        b[i]=element[x].rows[j1++];
      else        b[i]=element[x].rows[j2++];
    }
    for (i=0; i<end-start; ++i)      element[x].rows[i+start]=b[i]; 
  }
}
function dateMergeSort(x, c, start, end)
{
  if (end-start > 1)  // Recursively merge sort
  {
    // Merge sort each half of a
    var mid=parseInt((start+end)/2);
    stringMergeSort(x, c, start, mid);
    stringMergeSort(x, c, mid, end);
   
    // Merge the two halves
    var b = new Array();  // Temporary array
    var j1=start;
    var j2=mid;
    for (var i=0; i<end-start; ++i)
    {
      if (j1>=mid)
        b[i]=element[x].rows[j2++];
      else if (j2>=end)
        b[i]=element[x].rows[j1++];
      else if(((new Date(element[i].rows[j1].columns[x])) < (new Date(element[i].rows[j2].columns[x]))) || element[i].rows[b+1].columns[x]=='')        b[i]=element[x].rows[j1++];
      else        b[i]=element[x].rows[j2++];
    }
    for (i=0; i<end-start; ++i)   element[x].rows[i+start]=b[i]; 
  }
}
function selectTableColumn(col,id_1)
{
	var currComp;
	id = id_1 + '';
	var val = !element[id].colSelected[col];
	element[id].colSelected[col] = val;
	var totalRows = element[id].rows.length;
	for(var i=0; i<totalRows; i++)
	{
		currComp = document.all(id+'_'+i+'_'+col);
		if(currComp!=null && !currComp.disabled)
		{
			currComp.checked = val;
			if(val)	setColumnValue(id,i,col,"true");
			else	setColumnValue(id,i,col,"false");
		}
	}
}
function selectTableRow(row,id)
{
	var currComp;
	selectRow(id,row,1);
	var val = !element[id+''].rows[row].rowSelected;
	element[id+''].rows[row].rowSelected = val;
	var totalCols = element[id+''].rows[row].columns.length;
	for(i=0;i<totalCols;i++)
	{
		currComp = document.all(id+'_'+row+'_'+i);
		if(currComp!=null && !currComp.disabled)
		{
			currComp.checked = val;
			if(val)	setColumnValue(id,row,i,"true");
			else	setColumnValue(id,row,i,"false");
		}
	}
}
function DisplaySortedTable(i, ComponentStyle)
{
	var tableComponent = '';
	if(element[i].displayColumns.length>0)
	{
		var tableLength=0;
		var colWidth = new Array();
		var totalCols = 0;
		if(element[i].width.length>0)	colWidth=element[i].width.split(",");
		for(x=0;x < colWidth.length;x++) tableLength += (colWidth[x]-1)+1;
		tableComponent = ' <TABLE ID="TBL' + element[i].id + '" align=left BORDER="0" cellspacing="1" cellpadding="2" onMouseover=changeto("#C9C9C9")  onMouseout=changeback("'+element[i].innerbgcolor+'") ';
		if(tableLength > 0)	tableComponent += ' width="' + tableLength + '" ';
		tableComponent += '><TR>';
		if(element[i].multiselect=="true" || element[i].component=='RadioGroup')
		{
			tableComponent += '<TH bgcolor="'+element[i].headerbgcolor+'"></TH>';
			totalCols++;
		}
		for(z = 0; z < element[i].menucolumns.length-1; z++)
		{
			tableComponent += '<TH bgcolor="'+element[i].headerbgcolor+'"></TH>';
			totalCols++;
		}
		var tmpColIndex = element[i].displayColumns.length;
		var groupBy = false;
		var currGroup = "";
		if(element[i].groupcolumn > -1)
		{
			groupBy = true;
		}
		for (x = 0; x < tmpColIndex; x++)
		{
		        if(element[i].groupcolumn!=element[i].displayColumns[x])
		        {
			tableComponent  += '<TH ID="THDR'+ element[i].id +''+(element[i].displayColumns[x])+
					'" align=center valign=top STYLE="cursor:hand;background-color:'+element[i].headerbgcolor+';' + ComponentStyle + ';'; 
			if(colWidth[x])  tableComponent  += 'width:'+ (colWidth[x].length > 0?(colWidth[x]+'px'):'') + ';';
			if(element[i].onHeaderClick)	tableComponent  += '" onclick="e'+i+'_OnHeaderClick(' + (element[i].displayColumns[x]) + ',\'' + i + '\',\'' + ComponentStyle + '\')';
			tableComponent  += '"><FONT color="'+element[i].headerforecolor+'"><IMG ID="IMGHDR'+ element[i].id +''+(element[i].displayColumns[x])+
				'" SRC="http://'+hostIP+'/template/images/harrow.gif" style="display:none">&nbsp;<STRONG>' + element[i].columnHeaders[element[i].displayColumns[x]] + '</STRONG></FONT></TH>';
			totalCols++;
		        }
		}
    		tableComponent  += '</TR>';
		var totalRows = element[i].rows.length;
		if(element[i].pagesize == 0 || element[i].pagesize>totalRows)	element[i].currendrow = totalRows;
		
		for (x = element[i].currstartrow; x < element[i].currendrow;x++)
    		{
		        	if(groupBy && currGroup!=element[i].rows[x].columns[element[i].groupcolumn])
		        	{
				currGroup = element[i].rows[x].columns[element[i].groupcolumn];
				tableComponent += '<TR bgcolor="'+element[i].innerbgcolor+'" STYLE="' + ComponentStyle + '"><TD colspan='+(tmpColIndex+element[i].menucolumns.length)+'><B>' + currGroup +'</B></TD></TR>';
		        	 }
			tableComponent += '<TR bgcolor="'+element[i].innerbgcolor+'"';
			if(element[i].helptextcolumn!="")  tableComponent += 'onMouseover=\'javascript: showRowHelpText('+x+',"'+i+'", "#C9C9C9")\'';
			tableComponent += '>';	
			for(z = 0; z < (element[i].menucolumns.length-1); z++)  
			{	
			        if(element[i].rows[x].columns[element[i].menulistcolumns[z]-1]!="")
			        {
				tableComponent += '<TD align="center">' +SubComponent1[element[i].menucolumns[z]];
				if(SubComponent0[element[i].menucolumns[z]]=="CheckBox")
				{
					if(SubComponent2[element[i].menucolumns[z]]!='')	tableComponent += SubComponent2[element[i].menucolumns[z]] + x + ',0,\''  + i + '\');selectRow(\'' + element[i].id + '\',' + x + ',1);';
					tableComponent += SubComponent3[element[i].menucolumns[z]] + "</TD>";
				}
				else
				{
					if(SubComponent2[element[i].menucolumns[z]]!='')	tableComponent +=  SubComponent2[element[i].menucolumns[z]] + x + ',0,\''  + i + '\');';
					if(SubComponent3[element[i].menucolumns[z]]!='')	tableComponent +=  'selectRow(\'' + i + '\',' + x + ',1);';
					tableComponent += SubComponent3[element[i].menucolumns[z]]  + "</TD>";
				}
			         }
			}
			var firstCol = 0;
			if(element[i].groupcolumn==element[i].displayColumns[0])
					firstCol = 1;			
			for (var y = 0; y < tmpColIndex; y++)
			{	
			        if(element[i].groupcolumn!=element[i].displayColumns[y])
			        {
				if(y==firstCol  && element[i].multiselect=="true")	 tableComponent += '<TD><input type="checkbox"  onclick="selectRow(\'' + i + '\',' + x + ',this.checked);"></TD>';
				else if(y==firstCol  && element[i].component=='RadioGroup') tableComponent += '<TD><input type="radio"  id="CB' + i + '' + x + '" name="CB' + i + '" onclick="javascript: selectRow(\'' + i + '\',' + x + ',1);"></TD>';
				if(y==firstCol  && element[i].component=='AnchorTable') tableComponent += '<TD valign="top"><SPAN STYLE="' + ComponentStyle + ';color:blue;text-decoration:underline;cursor:hand;" onclick="javascript:if(!disableAnchor){DisableMenu(); selectRow(\'' + i + '\',' + x + ',1); RequiredFldChk();}">' +element[i].rows[x].columns[element[i].displayColumns[y]] + '</SPAN></TD>';
				else
				{
					tableComponent += '<TD bgcolor="'+element[i].rows[x].colBgcolor[element[i].displayColumns[y]]+'" ';
					if(element[i].rows[x].colHelp[element[i].displayColumns[y]]!="")
						tableComponent += 'onMouseover=\'javascript: showColumnHelpText('+element[i].displayColumns[y]+','+x+',"'+i+'", "#C9C9C9")\'';
					if(element[i].columnTypes[element[i].displayColumns[y]]=='Date' || element[i].columnTypes[element[i].displayColumns[y]]=='TimeStamp')	tableComponent += ' NOWRAP ';
					tableComponent += ' valign="top">';
					if(element[i].columnComponents[element[i].displayColumns[y]]!="")
					{
					        if(SubComponent0[element[i].columnComponents[element[i].displayColumns[y]]]=='CheckBox')
					        {
						tableComponent +='<center>'+SubComponent1[element[i].columnComponents[element[i].displayColumns[y]]];
						if(SubComponent2[element[i].columnComponents[element[i].displayColumns[y]]]!='')
							tableComponent +=SubComponent2[element[i].columnComponents[element[i].displayColumns[y]]] +x+',' + element[i].displayColumns[y] +',\''+ i +'\');';
						tableComponent +='setColumnValue(\''+i+'\','+x+','+element[i].displayColumns[y]+',this.checked?\'true\':\'false\');';
						if(element[i].rows[x].columns[element[i].displayColumns[y]]=="true")
							tableComponent +='" CHECKED="';
						if(element[i].rows[x].colEnabled[element[i].displayColumns[y]]=="false")
							tableComponent += '" DISABLED="';
						tableComponent +='" ID="'+i+'_'+x +'_'+element[i].displayColumns[y]+SubComponent3[element[i].columnComponents[element[i].displayColumns[y]]] + '</center>';
					        }
					        else if(SubComponent0[element[i].columnComponents[element[i].displayColumns[y]]]=='ComboBox')
					        {
						tableComponent +=SubComponent1[element[i].columnComponents[element[i].displayColumns[y]]] + 'setColumnValue(\''+i+'\','+x+','+element[i].displayColumns[y]+',this.value);';
						tableComponent +=SubComponent2[element[i].columnComponents[element[i].displayColumns[y]]] + '<OPTION value="'+element[i].rows[x].columns[element[i].displayColumns[y]]+'">'+element[i].rows[x].columns[element[i].displayColumns[y]]+'</OPTION>';
						tableComponent +=SubComponent3[element[i].columnComponents[element[i].displayColumns[y]]];
					        }
					        else if(SubComponent0[element[i].columnComponents[element[i].displayColumns[y]]]=='TextBox')
					        {
						tableComponent +=SubComponent1[element[i].columnComponents[element[i].displayColumns[y]]] + 'setColumnValue(\''+i+'\','+x+','+element[i].displayColumns[y]+',this.value);';
						tableComponent +=SubComponent2[element[i].columnComponents[element[i].displayColumns[y]]] + ' VALUE="'+element[i].rows[x].columns[element[i].displayColumns[y]]+'" ';
						tableComponent +=SubComponent3[element[i].columnComponents[element[i].displayColumns[y]]];
					        }
					}
					else if(element[i].rows[x].colComps[element[i].displayColumns[y]]=="" || element[i].rows[x].colComps[element[i].displayColumns[y]]==" ")
					{
						tableComponent += '<SPAN STYLE=' + ComponentStyle + '>' + element[i].rows[x].columns[element[i].displayColumns[y]] + '</SPAN>';
					}
					else if(element[i].rows[x].colComps[element[i].displayColumns[y]]=="CheckBox")
					{
						tableComponent += '<div align="center"><input id="'+i+'_'+x +'_'+element[i].displayColumns[y]+'" type="checkbox" width="5" onclick="javascript: setColumnValue(\''+i+'\','+x+','+element[i].displayColumns[y]+',this.checked?\'true\':\'false\'); if(this.checked) this.style.backgroundColor=\'yellow\'; else this.style.backgroundColor=\'\';" ';
						if(element[i].rows[x].columns[element[i].displayColumns[y]]=="true")
							tableComponent += ' CHECKED style="background-color:yellow" ';
						if(element[i].rows[x].colEnabled[element[i].displayColumns[y]]=="false")
							tableComponent += ' DISABLED';
						tableComponent += '></div>';
					}
					else if(element[i].rows[x].colComps[element[i].displayColumns[y]]=="TextBox")
					{
						tableComponent += '<div align="center"><input type="text" size="3" onchange="javascript: setColumnValue(\''+i+'\','+x+','+element[i].displayColumns[y]+',this.value)" value="'+element[i].rows[x].columns[element[i].displayColumns[y]]+'" ';
						if(element[i].rows[x].colEnabled[element[i].displayColumns[y]]=="false")
							tableComponent += ' DISABLED';
						tableComponent += ' ></div>';
					}
					tableComponent += '</TD>';
				}
			         }
	      		 }
			tableComponent += '</TR>';
    		}
		if(element[i].pagesize>0 && element[i].pagesize<=element[i].rows.length)
		{
			tableComponent += '<TR  bgcolor="'+element[i].innerbgcolor+'"><TD colspan="'+totalCols+'" align="left" height="20px" valign="center">';
			if(element[i].currstartrow >= element[i].pagesize && element[i].currstartrow>0)
				tableComponent += '<IMG SRC="http://'+hostIP+'/template/images/aro_left.gif" style="cursor:hand;" onclick="javascript: displayPreviousPaging(' + i + ',\'' + ComponentStyle + '\');">&nbsp;';
			tableComponent += '<SPAN STYLE="' + ComponentStyle + '">' +(element[i].currstartrow+1) + ' to ' + element[i].currendrow + ' of ' + element[i].rows.length + '</SPAN>';
			if(element[i].currendrow < element[i].rows.length)
				tableComponent += '&nbsp;<IMG SRC="http://'+hostIP+'/template/images/aro_right.gif" style="cursor:hand;" onclick="javascript: displayNextPaging(' + i + ',\'' + ComponentStyle + '\');">';
			tableComponent += '</TD></TR>';
		}
		tableComponent += '</TABLE>';
	}
//alert(tableComponent);
	return tableComponent;
}
function displayNextPaging(i, ComponentStyle)
{
	element[i].currendrow = element[i].currendrow + element[i].pagesize;
	element[i].currstartrow = element[i].currstartrow + element[i].pagesize;
	if(element[i].currendrow > element[i].rows.length) element[i].currendrow = element[i].rows.length;
	document.all['F' + i].innerHTML = DisplaySortedTable(i, ComponentStyle);
}
function displayPreviousPaging(i, ComponentStyle)
{
	element[i].currendrow = element[i].currstartrow;
	element[i].currstartrow = element[i].currstartrow - element[i].pagesize;
	document.all['F' + i].innerHTML = DisplaySortedTable(i, ComponentStyle);
}
function setColumnValue(id,row,col,val)
{
	element[id].rows[row].columns[col]=val;
	element[id].rows[row].changed=true;
}
function selectRow(id,row,val)
{
	if(val==1)	element[id].rows[row].changed=true;
	else	element[id].rows[row].changed=false;
}
function showRowHelpText(row, i, highlightcolor)
{
	var helpArea = document.all['GroupHelp_' + element[i].group];
	if(helpArea) helpArea.innerHTML = element[i].rows[row].columns[element[i].helptextcolumn-1];
	changeto(highlightcolor);
	event.cancelBubble=true;
}
function showColumnHelpText(col, row, i, highlightcolor)
{
	var helpArea = document.all['GroupHelp_' + element[i].group];
	if(helpArea) helpArea.innerHTML = element[i].rows[row].colHelp[col];
	changeto(highlightcolor);
	event.cancelBubble=true;
}
]]></xsl:if>
<xsl:if test="string-length($AllUIBlocks/Component/CompProperties[component='Tree'])>0">
<![CDATA[
function ViewTreeComponent(pCompStype,tmpparent,i,generation,pID,uRow)
{
	if(document.all['CT'+element[i].id+''+(generation-1)+''+pID+''+tmpparent+''+uRow].innerHTML=='')
	{
	var parent = tmpparent;
	pstrTree = '';
	var tmpRows = element[i].rows.length;
	pstrTree += '<TABLE BORDER="0" CELLSPACING="0"><TR><TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD><TD NOWRAP>';
	for(var x=0; x<tmpRows; x++)
	{
		var elementRow = element[i].rows[x];
		var cID = elementRow.columns[element[i].boundcolumnindex];
		var child = elementRow.columns[element[i].treechild];
		var tmpTitle = elementRow.columns[element[i].displaycolumnindex];
		var tmpParent = elementRow.columns[element[i].treeparent];
		if(tmpParent==parent)
		{
			var totalChild = 0;
			for(var y=0; y<tmpRows; y++)
				if(element[i].rows[y].columns[element[i].treeparent]==child)
						totalChild++;
			
			if(totalChild>0)		
			{
				pstrTree += '<span style="cursor:hand;' + pCompStype + '" onclick=ViewTreeComponent("' 
					+ pCompStype + '","' + child + '",' + i + ',' + (generation+1) + ',"' + cID + '",'+x+') ' +
					' id="T' + element[i].id + '' + generation + '' + cID + '' + child + ''+x+'">' +
					'&nbsp;&nbsp;<img id="TIMG' + element[i].id + '' + generation + '' + cID + '' + child + 
					''+x+'" src="http://'+hostIP+'/template/images/plus.gif">&nbsp;&nbsp;<span style="color:blue;text-decoration:underline">'
					+ tmpTitle + '</span></span><BR><DIV ID="CT' + element[i].id + '' + generation + '' + cID + '' + child + 
					''+x+'" style="display:none;"></DIV>';
			}
			else
			{
				pstrTree += '<span style="' + pCompStype + '">';
				if(element[i].enabled!='false')
				{
					if(element[i].multiselect=='true')
					{
						pstrTree +='<INPUT TYPE="CHECKBOX" ID="CB' + element[i].id + '' + cID + '' + child + ''+x + '" ';
						if(element[i].selectedValue == x)	pstrTree += ' CHECKED ' ;
						pstrTree += '>';
						
					}
					else
					{
						pstrTree +='<INPUT TYPE="RADIO" NAME="RT' +element[i].id+''+ i +	'" ID="CB' + element[i].id + '' + cID + '' + child + ''+x + '" ONCLICK=" setVariableValue(' +element[i].vid +',\''+ x + ':;\');" ';
						if(element[i].selectedValue == x)	pstrTree += ' CHECKED ' ;
						pstrTree += '>';
					}
				}
				pstrTree += tmpTitle + '<BR></span>';
			}
		}
	}
	pstrTree += '</TD></TR></TABLE>';
	document.all['CT'+element[i].id+''+(generation-1)+pID+tmpparent+''+uRow].innerHTML = pstrTree;
	}

	TreeDropDown(element[i].id+''+(generation-1)+pID+tmpparent+''+uRow);
}
function TreeDropDown(List)
{
	var nested = document.all['CT' + List].style;
	var pnested = document.all['TIMG'+ List];
	if(nested.display=="none")
	{
		pnested.src="http://"+hostIP+"/template/images/minus.gif";
		nested.display="";
	}
	else
	{
		pnested.src="http://"+hostIP+"/template/images/plus.gif";
		nested.display="none";
	}
}]]></xsl:if>
function window_onload()
{
	<xsl:apply-templates select="$Properties"/>
	 dataLoaded();
}
<![CDATA[
function window_onclick()
{
	var i = 0;
	for(i=0;i < components.length; i++)
		if(components[i].transientmessage=='true')
			document.all['COMPTR' + components[i].id].style.display='none';
	i=0;
	var temp = calenders.length;
	for(i=0; i<temp; i++)	document.all[calenders[i]].style.visibility = "hidden";
}
var CNST_BOOLEAN=1;
var CNST_DATE=2;
var CNST_DATETIME=3;
var CNST_DBLIST=4;
var CNST_LIST=5;
var CNST_NUMBER=6;
var CNST_STRING=7;
var TOID;
var ctrlSelectedValues;

function right(e) 
  {   if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2)) return false;
      else
          if ((event.button == 2 || event.button == 3)) 
         {   
			alert('Welcome to OCTET system !');
			return false;
         }
      return true;
  }
//document.onmousedown=right;
//if (document.layers) window.captureEvents(Event.MOUSEDOWN);
//window.onmousedown=right;

function DisableMenu()
{
	var menuButtons = document.all.tags('input');
	if(menuButtons) for(x=0; x<menuButtons.length; x++) if(menuButtons[x].type=='button') menuButtons[x].disabled=true;
	disableAnchor = true;
	document.body.style.cursor='wait';
}
function EnableMenu()
{
	var menuButtons = document.all.tags('input');
	if(menuButtons)
	for(x=0; x<menuButtons.length; x++) if(menuButtons[x].type=='button') menuButtons[x].disabled=false;
	disableAnchor = false;
	document.body.style.cursor='auto';
}
function showHelpText(currID, group)
{
	var helpArea = document.all['GroupHelp_' + group];
	if(helpArea) helpArea.innerHTML = document.all['HELP_' + currID].innerHTML;
}
function goBack()
{
    var hostname;
    if(emailHostName.length>0) hostname = emailHostName;
    else hostname = document.location.host + document.location.pathname;
    var actionString = 'http://' + hostname +'?handler=Back&nodeID='+nodeID+'&navigatorID=' + id;
    document.location.href = actionString;
}

function goNext()
{
	var hostname;
	var actionString;
    	if(emailHostName && emailHostName.length>0) hostname = emailHostName;
    	else hostname = document.location.host + document.location.pathname;
	if(subProcessID=='')
	{
		actionString = 'http://' + hostname +'?handler=Next&nodeID='+nodeID+'&navigatorID=' + id + "&";
		var elements = components;
		if(selectedValues != null)
		{
		    for (var i=0;i < selectedValues.length-1; i++)
		    { 
			var ChkVal = ctrlSelectedValues[i].value;
			if((elements[i].component!='Label' &&  elements[i].enabled!='false') 
			||  (ChkVal=='-1' && (elements[i].component== 'ListBox' || elements[i].component== 'ComboBox' || elements[i].component== 'Tree')))
			{
				actionString += selectedValues[i].vid + '=' + escape(selectedValues[i].value) + '&';
			}
		    }
		}
	}
	else
	{
		actionString = 'http://' + hostname +'?handler=Start&processID='+subProcessID+'&';
		var elements = components;
		if(selectedValues != null)
			for (var i=0;i < selectedValues.length-1; i++)
				if(paramMap[selectedValues[i].vid+'']!=null)	paramMap[selectedValues[i].vid+''].value = selectedValues[i].value;
		for (var i=0;i < parentIDs.length; i++)  actionString += paramMap[parentIDs[i]].vid + '=' + escape(paramMap[parentIDs[i]].value) + '&';
	}
	//actionString = actionString.substring(0,actionString.length-1);
 	//alert(actionString);
	document.location.href = actionString;
	EnableMenu();
}

function setVariableValue(v,val) 
{
	if (v+"" == "undefined" || v == null)  return false;
	if(selectedValues)
	{
		for (var i=0;i < selectedValues.length-1; i++) 
		{
			if (selectedValues[i].vid == v) 
			{	
				var xx = val.toString();
				xx = xx.replace("&lt;","<");
				xx = xx.replace("&gt;",">");
				val = xx;
				selectedValues[i].value = xx;
			}
		}
	}
	return true;
}

function isPosInteger(inputVal) {
	var inputStr = inputVal.toString();
	for (var i = 0; i < inputStr.length; i++) {
		var oneChar = inputStr.charAt(i);
		if ((oneChar < "0" || oneChar > "9") && oneChar!=".") return false;
	}
	return true;
}

function isNumber(inputVal) {
	oneDecimal = false
	inputStr = inputVal.toString()
	for (var i = 0; i < inputStr.length; i++) {
		var oneChar = inputStr.charAt(i);
		if (i == 0 && oneChar == "-") 	continue;
		if (oneChar == "." && !oneDecimal) {
			oneDecimal = true;
			continue;
		}
		if (oneChar < "0" || oneChar > "9")  return false;
	}
	return true;
}

function inRange(inputStr,min,max) {
	var num = parseInt(inputStr);
	if (num < min || num > max) return false;
	return true;
}

function isValid(inputStr,min,max) {
	if (isEmpty(inputStr)) {
		//alert("Please enter a number into the field before clicking the button.");
		return false;
	}
	else {
		if (!isNumber(inputStr)) {
			alert("Please make sure entries are numbers only.");
			return false;
		}
		else { 
			if(!(min==0 && max==0) && !inRange(inputStr,min,max)) {
				if(min==0)
					alert("Please enter a number less than "  + max);
				else if(max==0)
					alert("Please enter a number greater than " + min );
				else
					alert("Please enter a number between " + min + " and " + max);
					return false;
			}
		}
	}
	return true;
}

function isEmpty(inputStr) {
	if (inputStr == null || inputStr == "") return true;
	return false;
}

function setEditValue(v,val,min,max) {
	if(!isValid(val,min,max)) return false;
	else {
		for (var i=0;i < selectedValues.length-1; i++)
			if (selectedValues[i].vid == v) selectedValues[i].value = val;
		return true;
	}
}

function RequiredFldChk()
{
	var CompObj;
	var ReqSuccess=true;
	if(components)	var elements = components;
	else		return false;
	for (var i=0;i < selectedValues.length-1; i++)
	{
		if(components[i].component!='Image' && components[i].component!='Label' && components[i].visible!='false')
		{
		if(components[i].type == CNST_NUMBER && components[i].required=="true")
		{
			CurrFld=document.all(''+components[i].id);
			if(CurrFld) 	ReqSuccess = setEditValue(selectedValues[i].vid,CurrFld.value,components[i].minvalue,components[i].maxvalue);
		}
		else if(components[i].component== 'ListBox' && components[i].type!=CNST_DBLIST)
		{
			var oListBox = document.all(components[i].id + '');
			var TableRows = '';
			for (x = 0; x < oListBox.options.length; x++)	if(oListBox.options(x).selected)	TableRows += oListBox.options(x).value + ';';
			if(TableRows.length>0) TableRows=TableRows.substring(0,TableRows.length-1);
			else	TableRows='-1';
			selectedValues[i].value=TableRows;
		}
		else if((components[i].component== 'ListBox' || components[i].component== 'ComboBox' ) && components[i].type==CNST_DBLIST)
		{
			var oListBox = document.all(components[i].id + '');
			var TableRows = '';
			for (x = 0; x < oListBox.options.length; x++)	if(oListBox.options(x).selected)	TableRows += oListBox.options(x).value + ':;';
			if(TableRows.length<=0)	TableRows ='-1:;';
			selectedValues[i].value=TableRows;
		}
		else if(components[i].type==CNST_DBLIST)
		{
			var treeRows = '';
			var tmpElement = element[components[i].id+''];
			if(tmpElement !=null)
			{
			for(x=0;x<tmpElement.rows.length;x++)
			{
				var elementRow = tmpElement.rows[x];
				if(elementRow !=null)
				{
				if(components[i].component== 'Tree')
				{
					var cID = elementRow.columns[tmpElement.boundcolumnindex];
					var child = elementRow.columns[tmpElement.treechild];
					var tmpParent = elementRow.columns[tmpElement.treeparent];
					if(document.all('CB' + components[i].id +''+ cID +''+ child +''+ x) && document.all('CB' +components[i].id+''+ cID +''+ child +''+x).checked)	treeRows += elementRow.rowNo + ':;';
				}
				else if(elementRow.changed)
				{
					treeRows += elementRow.rowNo + ':';
					var tmpColIndex = tmpElement.displayColumns.length;
					for (var y = 0; y  < (tmpColIndex-1); y++)
						treeRows +=  tmpElement.displayColumns[y] + ',"' +elementRow.columns[tmpElement.displayColumns[y]]+'",';
					treeRows +=  tmpElement.displayColumns[y] + ',"' +elementRow.columns[tmpElement.displayColumns[y]]+'";';
				}
				}
			}
			if(treeRows.length<=0)	treeRows ='-1:;';
			selectedValues[i].value=treeRows;
			}
		}
		if(components[i].required=="true" && ReqSuccess)
		{
			ChkVal=selectedValues[i].value;
			if((elements[i].component!='Label' &&  elements[i].enabled!='false' && elements[i].visible!='false'  && ChkVal=='') 
				||  (ChkVal=='-1' && (elements[i].component== 'ListBox' || elements[i].component== 'ComboBox' || elements[i].component== 'Tree')))
			{
				alert(components[i].caption + ' is Required.');
				ReqSuccess=false;
			}
		}
		}
	}
	if(ReqSuccess) goNext();
	else	EnableMenu();
}
function  dataLoaded()
{
     if(selectedValues!=null)
     {
     	ctrlSelectedValues = new Array(selectedValues.length);
     	for(i=0;i<selectedValues.length;i++)
     	{
		ctrlSelectedValues[i] = new vpair();
		ctrlSelectedValues[i].vid = selectedValues[i].vid;
		ctrlSelectedValues[i].value = selectedValues[i].value;
    	}
     }     
}
window.onload=window_onload;
document.onload=window_onload;
document.onclick=window_onclick;
]]>
</SCRIPT>
</xsl:template>

</xsl:stylesheet>
