<xsl:variable name="NodeID" select="InteractionNode/@id"/>
<xsl:variable name="NavigatorID" select="InteractionNode/@navigatorID"/>
<xsl:variable name="SelectedValues" select="InteractionNode/selectedValues/SelectedValue"/>

<xsl:template name="generate_title">
	<xsl:attribute name="title"><xsl:value-of select="InteractionNode/Properties/caption"/></xsl:attribute>
</xsl:template>

<xsl:template name="generate_components">
<xsl:param name="group"/>
	<xsl:for-each select="InteractionNode/UIBlock">
	<xsl:for-each select="./Component">
		<xsl:variable name="CompGroup"><xsl:value-of select="./CompProperties/group"/></xsl:variable>
		<xsl:if test="not(@uitype='MenuAnchor' or @uitype='MenuButton' or @uitype='MenuImage') and $CompGroup=$group">
			<xsl:apply-templates select="."/>
		</xsl:if>
	</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<xsl:template name="generate_menu">
<xsl:param name="group"/>
	<xsl:if test="not(InteractionNode/Properties/backbuttonvisible='false')">
		<do type="accept" label="Back">
		<go method="get" href="http://<CTAG Host><CTAG WizardServlet>">
		<postfield name="handler" value="Back"/>
		<postfield name="nodeID">
	 		<xsl:attribute name="value"><xsl:value-of select="$NodeID"/></xsl:attribute> 
		</postfield>
	 	<postfield name="navigatorID">
	 		<xsl:attribute name="value"><xsl:value-of select="$NavigatorID"/></xsl:attribute> 	
		</postfield>
        		</go></do>
	</xsl:if>
	<xsl:for-each select="InteractionNode/UIBlock">
	<xsl:for-each select="./Component">
		<xsl:variable name="CompGroup"><xsl:value-of select="./CompProperties/group"/></xsl:variable>
		<xsl:if test="(@uitype='MenuAnchor' or @uitype='MenuButton' or @uitype='MenuImage') and $CompGroup=$group">
			<do type="accept">
				<xsl:if test="string-length(./CompProperties/caption)>0">
					<xsl:attribute name="label"><xsl:value-of select="./CompProperties/caption"/></xsl:attribute>
				</xsl:if>
				<xsl:call-template name="MenuItem"/>
			</do>
		</xsl:if>
	</xsl:for-each>
	</xsl:for-each>
	<xsl:if test="not(InteractionNode/Properties/nextbuttonvisible='false')">
		<do type="accept" label="Next">
			<xsl:call-template name="MenuItem"/>
		</do>
	</xsl:if>
</xsl:template>

<xsl:template name="MenuItem">
<xsl:variable name="CurrID" select="./@id"/>
	<go method="post" href="http://<CTAG Host><CTAG WizardServlet>">
		<postfield name="handler" value="Next"/>
		<postfield name="nodeID">
		 	<xsl:attribute name="value"><xsl:value-of select="$NodeID"/></xsl:attribute> 
	 	</postfield>
	 	<postfield name="navigatorID">
	 		<xsl:attribute name="value"><xsl:value-of select="$NavigatorID"/></xsl:attribute> 	
		</postfield>
		<xsl:for-each select="$SelectedValues"> 
	 	<postfield><xsl:attribute name="name"><xsl:value-of select="@vid"/></xsl:attribute>
		<xsl:choose>
			<xsl:when test="$CurrID=@id">
				<xsl:attribute name="value">1</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
	 			<xsl:attribute name="value">$comp<xsl:value-of select="@id"/></xsl:attribute>
			</xsl:otherwise>
	 	</xsl:choose>
		</postfield>
        		</xsl:for-each> 
        	</go>
</xsl:template>

<xsl:template match="Component"> 
<xsl:variable name="CompType" select="@uitype"/>
<xsl:variable name="Type" select="@type"/>
<xsl:choose>
<xsl:when test="@type='1'">
	<xsl:choose>
		<xsl:when test="$CompType='Anchor'">
			<xsl:call-template name="Anchor_1"/>
		</xsl:when>
		<xsl:when test="$CompType='Button'">
			<xsl:call-template name="Anchor_1"/>
		</xsl:when>
		<xsl:when test="$CompType='CheckBox'">
			<xsl:call-template name="TFSelection_1"/>
		</xsl:when>
		<xsl:when test="$CompType='ComboBox'">
			<xsl:call-template name="TFSelection_1"/>
		</xsl:when>
		<xsl:when test="$CompType='Image'">
			<xsl:call-template name="Image_1"/>
		</xsl:when>
		<xsl:when test="$CompType='RadioGroup'">
			<xsl:call-template name="TFSelection_1"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>

<xsl:when test="@type='2'">
	<xsl:if test="$CompType='TextBox'">
		<xsl:call-template name="TextBox_7"/>
	</xsl:if>
</xsl:when>

<xsl:when test="@type='3'">
	<xsl:if test="$CompType='TextBox'">
		<xsl:call-template name="TextBox_7"/>
	</xsl:if>
</xsl:when>

<xsl:when test="@type='4'">
	<xsl:choose>
		<xsl:when test="$CompType='AnchorTable'">
			<xsl:call-template name="Label_7"/>
		</xsl:when>
		<xsl:when test="$CompType='ComboBox'">
			<xsl:call-template name="Label_7"/>
		</xsl:when>
		<xsl:when test="$CompType='DisplayGrid'">
			<xsl:call-template name="Label_7"/>
		</xsl:when>
		<xsl:when test="$CompType='Graph'">
			<xsl:call-template name="Label_7"/>
		</xsl:when>
		<xsl:when test="$CompType='ListBox'">
			<xsl:call-template name="Label_7"/>
		</xsl:when>
		<xsl:when test="$CompType='RadioGroup'">
			<xsl:call-template name="Label_7"/>
		</xsl:when>
		<xsl:when test="$CompType='Tree'">
			<xsl:call-template name="Label_7"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>

<xsl:when test="@type='5'">
	<xsl:choose>
		<xsl:when test="$CompType='Anchor'">
			<xsl:call-template name="Selection_5"/>
		</xsl:when>
		<xsl:when test="$CompType='Button'">
			<xsl:call-template name="Selection_5"/>
		</xsl:when>
		<xsl:when test="$CompType='CheckBox'">
			<xsl:call-template name="Selection_5"/>
		</xsl:when>
		<xsl:when test="$CompType='ComboBox'">
			<xsl:call-template name="Selection_5"/>
		</xsl:when>
		<xsl:when test="$CompType='ListBox'">
			<xsl:call-template name="Selection_5"/>
		</xsl:when>
		<xsl:when test="$CompType='RadioGroup'">
			<xsl:call-template name="Selection_5"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>

<xsl:when test="@type='6'">
	<xsl:if test="$CompType='TextBox'">
		<xsl:call-template name="TextBox_7"/>
	</xsl:if>
</xsl:when>
<xsl:when test="$Type='7'">
	<xsl:choose>
		<xsl:when test="$CompType='Label'">
			<xsl:call-template name="Label_7"/>
		</xsl:when>
		<xsl:when test="$CompType='TextBox'">
			<xsl:call-template name="TextBox_7"/>
		</xsl:when>
		<xsl:when test="$CompType='TextArea'">
			<xsl:call-template name="TextBox_7"/>
		</xsl:when>
	</xsl:choose>
</xsl:when>
</xsl:choose>
</xsl:template>

<xsl:template name="Image_1">
	<p>
	<anchor title="Next"><img>
		<xsl:if test="string-length(./CompProperties/caption)>0">
			<xsl:attribute name="alt"><xsl:value-of select="CompProperties/caption"/></xsl:attribute>
		</xsl:if>
		<xsl:attribute name="src">/template/images/<xsl:value-of select="CompProperties/caption"/></xsl:attribute>
		</img>
		<xsl:call-template name="MenuItem"/>
	</anchor>
	</p>
</xsl:template>

<xsl:template name="Anchor_1">
	<p><xsl:value-of select="CompProperties/caption"/>:
	<anchor title="Next"><xsl:value-of select="CompProperties/caption"/>
		<xsl:call-template name="MenuItem"/>
	</anchor>
	</p>
</xsl:template>

<xsl:template name="TFSelection_1">
	<p>
	<xsl:value-of select="CompProperties/caption"/>:
	<select><xsl:attribute name="name">comp<xsl:value-of select="@id"/></xsl:attribute>
		<xsl:choose>
		<xsl:when test="./vValue='true'">
			<xsl:attribute name="value">1</xsl:attribute>
		</xsl:when>
		<xsl:otherwise>
			<xsl:attribute name="value">0</xsl:attribute>
		</xsl:otherwise>
		</xsl:choose>
		<xsl:attribute name="iname">icomp<xsl:value-of select="@id"/></xsl:attribute>
		<xsl:choose>
		<xsl:when test="./vValue='true'">
			<xsl:attribute name="ivalue">0</xsl:attribute>
		</xsl:when>
		<xsl:otherwise>
			<xsl:attribute name="ivalue">1</xsl:attribute>
		</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="string-length(./CompProperties/caption)>0">
			<xsl:attribute name="title"><xsl:value-of select="CompProperties/caption"/></xsl:attribute>
		</xsl:if>
		<option value="0">No</option>
  		<option value="1">Yes</option>
	</select>
	</p>
</xsl:template>

<xsl:template name="Selection_5">
	<p>
	<xsl:value-of select="CompProperties/caption"/>:
	<select><xsl:attribute name="name">comp<xsl:value-of select="@id"/></xsl:attribute>
		<xsl:if test="string-length(./vValue)>0">	
			<xsl:attribute name="value"><xsl:value-of select="./vValue"/></xsl:attribute>
			<xsl:attribute name="ivalue"><xsl:value-of select="./vValue + 1"/></xsl:attribute>
		</xsl:if>
		<xsl:attribute name="iname">icomp<xsl:value-of select="@id"/></xsl:attribute>
		<xsl:if test="string-length(./CompProperties/caption)>0">	
			<xsl:attribute name="title"><xsl:value-of select="./CompProperties/caption"/></xsl:attribute>
		</xsl:if>
		<option value="-1">none</option>
	    <xsl:for-each select="./item">
		<option>
		<xsl:attribute name="value"><xsl:value-of select="./@itemno"/></xsl:attribute>
		<xsl:value-of select="./name"/></option>
	   </xsl:for-each>
	</select>
	</p>
</xsl:template>

<xsl:template name="Label_7">
	<p>
		<xsl:if test="string-length(./CompProperties/caption)>0">
			<xsl:value-of select="./CompProperties/caption"/><br/>
		</xsl:if>
		<xsl:value-of select="./vValue"/>
	</p>
</xsl:template>

<xsl:template name="TextBox_7">
	<p>
	<xsl:value-of select="./CompProperties/caption"/>:
	<xsl:choose>
	<xsl:when test="CompProperties/disable='true'">
		<xsl:if test="string-length(./vValue)>0 and not(./CompProperties/password='true')">
			<b><xsl:value-of select="./vValue"/></b>
		</xsl:if>
	</xsl:when>
	<xsl:otherwise>
		<input>
		<xsl:if test="string-length(./CompProperties/caption)>0">
			<xsl:attribute name="title"><xsl:value-of select="CompProperties/caption"/></xsl:attribute>
		</xsl:if>
		<xsl:attribute name="name">comp<xsl:value-of select="@id"/></xsl:attribute>
		<xsl:if test="string-length(./vValue)>0 and not(./CompProperties/password='true')">
			<xsl:attribute name="value"><xsl:value-of select="./vValue"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="./CompProperties/password='true'"><xsl:attribute name="type">password</xsl:attribute></xsl:if>
		<xsl:if test="string-length(./CompProperties/maxlength)>0">
			<xsl:attribute name="maxlength"><xsl:value-of select="./CompProperties/maxlength"/></xsl:attribute>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="string-length(./CompProperties/width)>0">
				<xsl:attribute name="size"><xsl:value-of select="./CompProperties/width"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="size">15</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>  
		<xsl:if test="@type='6'">
			<xsl:attribute name="format">*N</xsl:attribute>
		</xsl:if>
		</input>
	</xsl:otherwise>
	</xsl:choose>
	</p>
</xsl:template>

</xsl:stylesheet>
