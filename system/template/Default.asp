<%@ Language=VBScript %>
<%	
	if not(isobject(session("ProcessNavigation"))) then
		Set session("ProcessNavigation") = Server.CreateObject("ProcessNavigation.CPN")
		session("ProcessNavigation").startProcess
	end if
	
	QueryString = Request.ServerVariables("QUERY_STRING")
	UserAgent = Request.ServerVariables("HTTP_USER_AGENT")
	
	CurrHtml=session("ProcessNavigation").getHTML(session.SessionID,QueryString,UserAgent)
	ContentType=session("ProcessNavigation").getContentType(UserAgent)
%>
<%=CurrHtml%>