/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 6 Sep 2013 */
/*   Description    : Stored Proc bi_populate_afs_materialmovement_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   21 May 2014      Cornelia	1.0  		 Add dim_uomunitofentryid */
/*   19 May 2014      Cornelia	1.0  		 Add dim_unitofmeasureorderunitid */
/*   06 Sep 2013      Lokesh	1.0  		 Existing version migrated from CVS */
/*   13 Sep 2013      Lokesh	1.1		 Exchange rate changes	  */
/*						 Note that amts are in local curr e.g MSEG_WAERS is same as dc.currency */
/* 						 We will take MSEG_WAERS as TRA and dim_company.currency as local curr */
/*   22 Oct 2013      Hiten	1.25		 remove and reinsert stage1 data to fact table */
/******************************************************************************************************************/

drop table if exists pGlobalCurrency_tbl;
create table pGlobalCurrency_tbl ( pGlobalCurrency varchar(3));
drop table if exists dim_profitcenter_789;
create table dim_profitcenter_789 as select first 0 * from dim_profitcenter order by ValidTo;

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'bi_populate_materialmovement_fact START');

Insert into pGlobalCurrency_tbl values('USD');

Update pGlobalCurrency_tbl
SET pGlobalCurrency =
        ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD');

UPDATE MKPF_MSEG 
SET MSEG_BUKRS = ifnull(MSEG_BUKRS,'Not Set') 
where MSEG_BUKRS <>  ifnull(MSEG_BUKRS,'Not Set');

UPDATE MKPF_MSEG1 
SET MSEG1_BUKRS = ifnull(MSEG1_BUKRS,'Not Set') 
where  MSEG1_BUKRS <>  ifnull(MSEG1_BUKRS,'Not Set');

DELETE FROM fact_materialmovement
WHERE EXISTS (SELECT 1
		FROM MKPF_MSEG m
		WHERE dd_MaterialDocNo = m.MSEG_MBLNR
			 AND dd_MaterialDocItemNo = m.MSEG_ZEILE
			 AND m.MSEG_MJAHR = dd_MaterialDocYear);

DELETE FROM fact_materialmovement
WHERE EXISTS (SELECT 1
		FROM MKPF_MSEG1 m
		WHERE dd_MaterialDocNo = m.MSEG1_MBLNR
			 AND dd_MaterialDocItemNo = m.MSEG1_ZEILE
			 AND m.MSEG1_MJAHR = dd_MaterialDocYear);


INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');

drop table if exists fact_materialmovement_789;
create table fact_materialmovement_789 as select * from fact_materialmovement where 1=2;
Drop table if exists max_holder_789;
create table max_holder_789(maxid)
as
Select ifnull(max(fact_materialmovementid),0)
from fact_materialmovement;

drop table if exists fact_materialmovement_ppi_789;

create table fact_materialmovement_ppi_789 as
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
from fact_materialmovement;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_ppi_789;

  INSERT INTO fact_materialmovement_789(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,                   
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
			       Dim_AfsSizeId,
				   dim_Currencyid_TRA,
					dim_Currencyid_GBL )
   select max_holder_789.maxid + row_number() over(), bb.* from max_holder_789,
   (SELECT DISTINCT
          m.MSEG_MBLNR dd_MaterialDocNo,
          m.MSEG_ZEILE dd_MaterialDocItemNo,
          m.MSEG_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG_AUFNR,'Not Set') dd_productionordernumber, 
          m.MSEG_AUFPS dd_productionorderitemno,
          m.MSEG_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG_CHARG, 'Not Set') dd_BatchNumber,
          m.MSEG_BWTAR dd_ValuationType,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_DMBTR amt_LocalCurrAmt,
          m.MSEG_DMBTR / (case when m.MSEG_ERFMG =0 then null else  m.MSEG_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BUALT amt_AltPriceControl,
          m.MSEG_SALK3 amt_OnHand_ValStock,
	  (select CASE WHEN MSEG_WAERS = dc.Currency THEN 1.00 ELSE  z.exchangeRate end from tmp_getExchangeRate1 z where z.pFromCurrency  = MSEG_WAERS and z.pToCurrency = dc.Currency and z.pDate = MKPF_BUDAT) amt_ExchangeRate,
	  ( Select CASE WHEN MSEG_WAERS = pGlobalCurrency THEN 1.00 ELSE z.exchangeRate end from tmp_getExchangeRate1 z where z.pFromCurrency  = MSEG_WAERS and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)) amt_ExchangeRate_GBL,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_MENGE ct_Quantity,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BSTMG ct_QtyGROrdUnit,
          m.MSEG_LBKUM ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG_BPMNG ct_QtyOrdPriceUnit,
          ifnull((SELECT mt.Dim_MovementTypeid
                    FROM dim_movementtype mt
                   WHERE mt.MovementType = m.MSEG_BWART
                        AND mt.ConsumptionIndicator = ifnull(m.MSEG_KZVBR, 'Not Set')
                        AND mt.MovementIndicator = ifnull(m.MSEG_KZBEW, 'Not Set')
                        AND mt.ReceiptIndicator = ifnull(m.MSEG_KZZUG, 'Not Set')
                        AND mt.SpecialStockIndicator = ifnull(m.MSEG_SOBKZ, 'Not Set')),1) Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = dc.currency),1) Dim_Currencyid,
	ifnull(( Select (case when  m.MSEG_MATNR IS NULL then 1 else Dim_Partid end) FROM dim_part dpr  WHERE     dpr.PartNumber = m.MSEG_MATNR  AND dpr.Plant = m.MSEG_WERKS),1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
	  ifnull((select (case  WHEN m.MSEG_LGORT IS NULL then 1 else Dim_StorageLocationid end)
                          FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG_LGORT
                                AND dsl.Plant = m.MSEG_WERKS),1) Dim_StorageLocationid,
          ifnull((SELECT dv.Dim_Vendorid
                    FROM dim_vendor dv
                   WHERE dv.VendorNumber = m.MSEG_LIFNR),1) Dim_Vendorid,
          ifnull((SELECT mdd.Dim_Dateid
                    FROM dim_date mdd
                   WHERE mdd.CompanyCode = m.MSEG_BUKRS AND m.MKPF_BLDAT = mdd.DateValue),1) dim_MaterialDocDate,
          ifnull((SELECT pd.Dim_Dateid 
                    FROM dim_date pd
                   WHERE pd.DateValue = m.MKPF_BUDAT AND pd.CompanyCode = m.MSEG_BUKRS),1) dim_PostingDate,
	  ifnull((select (case WHEN m.MSEG_MATNR IS NULL then 1 else dim_producthierarchyid end)
                          FROM dim_producthierarchy dph, dim_part dpr
                          WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                AND dpr.PartNumber = m.MSEG_MATNR
                                AND dpr.Plant = m.MSEG_WERKS),1) dim_producthierarchyid,
          ifnull((SELECT dmi.dim_MovementIndicatorid
                    FROM dim_movementindicator dmi
                   WHERE dmi.TypeCode = m.MSEG_KZBEW),1) dim_MovementIndicatorid,
          ifnull(( SELECT dcu.dim_Customerid
                     FROM dim_customer dcu
                    WHERE dcu.CustomerNumber = m.MSEG_KUNNR),1) dim_Customerid,
		1 dim_CostCenterid,
          ifnull((SELECT spt.dim_specialstockid
                    FROM dim_specialstock spt
                  WHERE spt.specialstockindicator = MSEG_SOBKZ), 1) dim_specialstockid,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = MSEG_MEINS), 1) dim_unitofmeasureid,
          ifnull((SELECT ca.dim_controllingareaid
                    FROM dim_controllingarea ca
                   WHERE ca.ControllingAreaCode = MSEG_KOKRS),1) dim_controllingareaid,
                   1 Dim_ProfitCenterId,
          ifnull((SELECT ct.Dim_ConsumptionTypeid
                    FROM dim_consumptiontype ct
                   WHERE ct.ConsumptionCode = MSEG_KZVBR), 1) dim_consumptiontypeid,
          ifnull((SELECT st.Dim_StockTypeid
                    FROM dim_stocktype st
                   WHERE st.TypeCode = MSEG_INSMK), 1) dim_stocktypeid,
	ifnull(( Select (case when  m.MSEG_MATNR IS NULL then 1 else  pg.Dim_PurchaseGroupid end )
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG_MATNR
                                      AND dpr.Plant = m.MSEG_WERKS), 1) Dim_PurchaseGroupid,
          ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup
                        AND dpr.PartNumber = m.MSEG_MATNR
                        AND dpr.Plant = m.MSEG_WERKS), 1) Dim_materialgroupid,
          ifnull((SELECT pl.dim_plantid
                    FROM dim_plant pl
                   WHERE pl.PlantCode = MSEG_UMWRK), 1) Dim_ReceivingPlantId,
          ifnull((SELECT prt.Dim_Partid
                    FROM dim_part prt
                   WHERE prt.Plant = MSEG_UMWRK AND prt.PartNumber = MSEG_UMMAT), 1) Dim_ReceivingPartId,
	ifnull((select (case when  m.MSEG_UMLGO IS NULL then 1 else Dim_StorageLocationid end)
		FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG_UMLGO
                                AND dsl.Plant = m.MSEG_UMWRK), 1) Dim_RecvIssuStorLocid,
	ifnull((SELECT dim_AfsSizeId
                        FROM dim_afssize
                        WHERE Size = MSEG_J_3ASIZ AND RowIsCurrent = 1),1) Dim_AfsSizeId,
  ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = m.MSEG_WAERS),1) Dim_Currencyid_TRA,
  ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = pGlobalCurrency),1) Dim_Currencyid_GBL		   
						
     FROM pGlobalCurrency_tbl, MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)) bb;

drop table if exists max_holder_789;

Update fact_materialmovement_789 fmt
From pGlobalCurrency_tbl, MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
set Dim_ProfitCenterId =  ifnull((SELECT pc.Dim_ProfitCenterid
		    FROM dim_profitcenter_789 pc
		   WHERE  pc.ControllingArea = MSEG_KOKRS
		      AND pc.ProfitCenterCode = MSEG_PRCTR
		      AND pc.ValidTo >= m.MKPF_BUDAT
		   ),1) 
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)
AND fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR;


Update fact_materialmovement_789 fmt
From pGlobalCurrency_tbl, MKPF_MSEG m
          INNER JOIN dim_plant dp
             ON m.MSEG_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG_BUKRS  = dc.CompanyCode
set dim_CostCenterid =   ifnull((select (case  WHEN m.MSEG_KOSTL IS NULL then 1 else dim_CostCenterid end)
                          FROM dim_costcenter dcc
                          WHERE     dcc.Code = m.MSEG_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG_KOKRS = dcc.ControllingArea), 1)
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
                         AND m.MSEG_MJAHR = ms.dd_MaterialDocYear)
AND fmt.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = fmt.dd_MaterialDocYear
AND  fmt.dd_MaterialDocNo = m.MSEG_MBLNR;


drop table if exists fact_materialmovement_ppi_789;

  INSERT INTO fact_materialmovement(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
							   Dim_AfsSizeId,
							   Dim_Currencyid_TRA,
							   Dim_Currencyid_GBL)
Select fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
							   Dim_AfsSizeId,
							   Dim_Currencyid_TRA,
							   Dim_Currencyid_GBL				   
from fact_materialmovement_789;

drop table if exists fact_materialmovement_789;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 1');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');

drop table if exists fact_materialmovement_789;
create table fact_materialmovement_789 as select * from fact_materialmovement where 1=2;
Drop table if exists max_holder_789;
create table max_holder_789(maxid)
as
Select ifnull(max(fact_materialmovementid),0)
from fact_materialmovement;

create table fact_materialmovement_ppi_789 as
Select dd_MaterialDocNo,dd_MaterialDocItemNo,dd_MaterialDocYear
From fact_materialmovement;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_ppi_789;
  INSERT INTO fact_materialmovement_789(fact_materialmovementid,dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
                               dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
								Dim_AfsSizeId,
								Dim_Currencyid_TRA,
								Dim_Currencyid_GBL)
   Select max_holder_789.maxid + row_number() over(), bb.* from max_holder_789,(SELECT DISTINCT
          m.MSEG1_MBLNR dd_MaterialDocNo,
          m.MSEG1_ZEILE dd_MaterialDocItemNo,
          m.MSEG1_MJAHR dd_MaterialDocYear,
          ifnull(m.MSEG1_EBELN,'Not Set') dd_DocumentNo,
          m.MSEG1_EBELP dd_DocumentItemNo,
          ifnull(m.MSEG1_KDAUF,'Not Set') dd_SalesOrderNo,
          m.MSEG1_KDPOS dd_SalesOrderItemNo,
          ifnull(m.MSEG1_KDEIN,0) dd_SalesOrderDlvrNo,
          ifnull(m.MSEG1_SAKTO,'Not Set') dd_GLAccountNo,
          ifnull(m.MSEG1_LFBNR,'Not Set') dd_ReferenceDocNo,
          ifnull(m.MSEG1_LFPOS,0) dd_ReferenceDocItem,
          CASE WHEN m.MSEG1_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END  dd_debitcreditid,
          ifnull(m.MSEG1_AUFNR,'Not Set') dd_productionordernumber, 
          m.MSEG1_AUFPS dd_productionorderitemno,
          m.MSEG1_GRUND dd_GoodsMoveReason,
          ifnull(m.MSEG1_CHARG, 'Not Set') dd_BatchNumber,
          m.MSEG1_BWTAR dd_ValuationType,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_DMBTR amt_LocalCurrAmt,
          m.MSEG1_DMBTR / (case when m.MSEG1_ERFMG =0 then null else  m.MSEG1_ERFMG end) amt_StdUnitPrice,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BNBTR amt_DeliveryCost,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BUALT amt_AltPriceControl,
          m.MSEG1_SALK3 amt_OnHand_ValStock,
	(Select  CASE WHEN MSEG1_WAERS = dc.Currency THEN 1.00 ELSE z.exchangeRate end from tmp_getExchangeRate1 z where z.pFromCurrency  = MSEG1_WAERS and z.pToCurrency = dc.Currency and z.pDate = MKPF_BUDAT ) amt_ExchangeRate,
	(Select  CASE WHEN MSEG1_WAERS = pGlobalCurrency THEN 1.00 ELSE z.exchangeRate end from tmp_getExchangeRate1 z where z.pFromCurrency  = MSEG1_WAERS and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)) amt_ExchangeRate_GBL,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_MENGE ct_Quantity,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_ERFMG ct_QtyEntryUOM,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BSTMG ct_QtyGROrdUnit,
          m.MSEG1_LBKUM ct_QtyOnHand_ValStock,
          (CASE WHEN m.MSEG1_SHKZG = 'H' THEN -1 ELSE 1 END) * m.MSEG1_BPMNG ct_QtyOrdPriceUnit,
          ifnull((SELECT mt.Dim_MovementTypeid
                    FROM dim_movementtype mt
                   WHERE mt.MovementType = m.MSEG1_BWART
                        AND mt.ConsumptionIndicator = ifnull(m.MSEG1_KZVBR, 'Not Set')
                        AND mt.MovementIndicator = ifnull(m.MSEG1_KZBEW, 'Not Set')
                        AND mt.ReceiptIndicator = ifnull(m.MSEG1_KZZUG, 'Not Set')
                        AND mt.SpecialStockIndicator = ifnull(m.MSEG1_SOBKZ, 'Not Set')),1) Dim_MovementTypeid,
          dc.dim_CompanyId dim_Companyid,
          ifnull((SELECT dcr.Dim_Currencyid
                    FROM dim_currency dcr
                   WHERE dcr.CurrencyCode = dc.currency),1) Dim_Currencyid,
          ifnull((SELECT Dim_Partid
                          FROM dim_part dpr
                          WHERE     dpr.PartNumber = m.MSEG1_MATNR
                                AND dpr.Plant = m.MSEG1_WERKS), 1) Dim_Partid,
          dp.Dim_Plantid Dim_Plantid,
          ifnull((SELECT Dim_StorageLocationid
                          FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG1_LGORT
                                AND dsl.Plant = m.MSEG1_WERKS), 1) Dim_StorageLocationid,
          ifnull((SELECT dv.Dim_Vendorid
                    FROM dim_vendor dv
                   WHERE dv.VendorNumber = m.MSEG1_LIFNR),1) Dim_Vendorid,
          ifnull((SELECT mdd.Dim_Dateid
                    FROM dim_date mdd
                   WHERE mdd.CompanyCode = m.MSEG1_BUKRS AND MKPF_BLDAT = mdd.DateValue),1) dim_MaterialDocDate,
          ifnull((SELECT pd.Dim_Dateid 
                    FROM dim_date pd
                   WHERE pd.DateValue = MKPF_BUDAT AND pd.CompanyCode = m.MSEG1_BUKRS),1) dim_PostingDate,
          ifnull((SELECT dim_producthierarchyid
                          FROM dim_producthierarchy dph, dim_part dpr
                          WHERE     dph.ProductHierarchy = dpr.ProductHierarchy
                                AND dpr.PartNumber = m.MSEG1_MATNR
                                AND dpr.Plant = m.MSEG1_WERKS), 1) dim_producthierarchyid,
          ifnull((SELECT dmi.dim_MovementIndicatorid
                    FROM dim_movementindicator dmi
                   WHERE dmi.TypeCode = m.MSEG1_KZBEW),1) dim_MovementIndicatorid,
          ifnull(( SELECT dcu.dim_Customerid
                     FROM dim_customer dcu
                    WHERE dcu.CustomerNumber = m.MSEG1_KUNNR ),1) dim_Customerid,
                   1 dim_CostCenterid,
          ifnull((SELECT spt.dim_specialstockid
                    FROM dim_specialstock spt
                  WHERE spt.specialstockindicator = MSEG1_SOBKZ), 1) dim_specialstockid,
          ifnull((SELECT uom.Dim_UnitOfMeasureid
                    FROM dim_unitofmeasure uom
                   WHERE uom.UOM = MSEG1_MEINS), 1) dim_unitofmeasureid,
          ifnull((SELECT ca.dim_controllingareaid
                    FROM dim_controllingarea ca
                   WHERE ca.ControllingAreaCode = MSEG1_KOKRS),1) dim_controllingareaid,
                   1 dim_profitcenterid,
          ifnull((SELECT ct.Dim_ConsumptionTypeid
                    FROM dim_consumptiontype ct
                   WHERE ct.ConsumptionCode = MSEG1_KZVBR), 1) dim_consumptiontypeid,
          ifnull((SELECT st.Dim_StockTypeid
                    FROM dim_stocktype st
                   WHERE st.TypeCode = MSEG1_INSMK), 1) dim_stocktypeid,
	 ifnull(( Select (case when  m.MSEG1_MATNR IS NULL then 1 else  pg.Dim_PurchaseGroupid end )
                                FROM dim_part dpr inner join dim_purchasegroup pg
                                      on dpr.PurchaseGroupCode = pg.PurchaseGroup
                                WHERE     dpr.PartNumber = m.MSEG1_MATNR
                                      AND dpr.Plant = m.MSEG1_WERKS), 1) Dim_PurchaseGroupid,
          ifnull((SELECT mg.Dim_materialgroupid
                    FROM dim_materialgroup mg, dim_part dpr
                   WHERE mg.MaterialGroupCode = dpr.MaterialGroup
                        AND dpr.PartNumber = m.MSEG1_MATNR
                        AND dpr.Plant = m.MSEG1_WERKS), 1) Dim_materialgroupid,
          ifnull((SELECT pl.dim_plantid
                    FROM dim_plant pl
                   WHERE pl.PlantCode = MSEG1_UMWRK), 1) Dim_ReceivingPlantId,
          ifnull((SELECT prt.Dim_Partid
                    FROM dim_part prt
                   WHERE prt.Plant = MSEG1_UMWRK AND prt.PartNumber = MSEG1_UMMAT), 1) Dim_ReceivingPartId,
	 ifnull((select (case when  m.MSEG1_UMLGO IS NULL then 1 else Dim_StorageLocationid end)
                FROM dim_storagelocation dsl
                          WHERE     dsl.LocationCode = m.MSEG1_UMLGO
                                AND dsl.Plant = m.MSEG1_UMWRK), 1) Dim_RecvIssuStorLocid,
		ifnull((SELECT dim_AfsSizeId
                        FROM dim_afssize
                        WHERE Size = MSEG1_J_3ASIZ AND RowIsCurrent = 1),1) Dim_AfsSizeId   ,
 ifnull((SELECT dcr.Dim_Currencyid
                  FROM dim_currency dcr
                 WHERE dcr.CurrencyCode = m.MSEG1_WAERS),1) Dim_Currencyid_TRA,
 ifnull((SELECT dcr.Dim_Currencyid
                  FROM dim_currency dcr
                 WHERE dcr.CurrencyCode = pGlobalCurrency ),1) Dim_Currencyid_GBL						
     FROM pGlobalCurrency_tbl, MKPF_MSEG1 m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
    WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear)) bb;

drop table if exists max_holder_789;

Update fact_materialmovement_789 fmt
  FROM pGlobalCurrency_tbl, MKPF_MSEG1 m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
set Dim_ProfitCenterId =  ifnull((SELECT pc.Dim_ProfitCenterid
                    FROM dim_profitcenter_789 pc
                   WHERE  pc.ControllingArea = MSEG1_KOKRS
                      AND pc.ProfitCenterCode = MSEG1_PRCTR
                      AND pc.ValidTo >= m.MKPF_BUDAT
                   ),1)
 WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear)
AND  fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear;

Update fact_materialmovement_789 fmt
  FROM pGlobalCurrency_tbl, MKPF_MSEG1 m
          INNER JOIN dim_plant dp
             ON m.MSEG1_WERKS = dp.PlantCode
          INNER JOIN dim_company dc
             ON m.MSEG1_BUKRS  = dc.CompanyCode
set dim_CostCenterid =  ifnull((select (case  WHEN m.MSEG1_KOSTL IS NULL then 1 else dim_CostCenterid end)
                          FROM dim_costcenter dcc
                          WHERE     dcc.Code = m.MSEG1_KOSTL
                                AND dcc.validTo >= MKPF_BUDAT
                                AND m.MSEG1_KOKRS = dcc.ControllingArea), 1)
 WHERE NOT EXISTS
                 (SELECT 1
                    FROM fact_materialmovement_ppi_789 ms
                   WHERE     ms.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND ms.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = ms.dd_MaterialDocYear)
AND  fmt.dd_MaterialDocNo = m.MSEG1_MBLNR
                         AND fmt.dd_MaterialDocItemNo = m.MSEG1_ZEILE
                         AND m.MSEG1_MJAHR = fmt.dd_MaterialDocYear;

drop table if exists fact_materialmovement_ppi_789;

  INSERT INTO fact_materialmovement(fact_materialmovementid,
  				dd_MaterialDocNo,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocYear,
                               dd_DocumentNo,
                               dd_DocumentItemNo,
                               dd_SalesOrderNo,
                               dd_SalesOrderItemNo,
                               dd_SalesOrderDlvrNo,
                               dd_GLAccountNo,
                                dd_ReferenceDocNo,
                                dd_ReferenceDocItem,
                               dd_debitcreditid,
                               dd_productionordernumber,
                               dd_productionorderitemno,
                               dd_GoodsMoveReason,
                               dd_BatchNumber,
                               dd_ValuationType,
                               amt_LocalCurrAmt,
                               amt_StdUnitPrice,
                               amt_DeliveryCost,
                               amt_AltPriceControl,
                               amt_OnHand_ValStock,
                               amt_ExchangeRate,
                               amt_ExchangeRate_GBL,
                               ct_Quantity,
                               ct_QtyEntryUOM,
                               ct_QtyGROrdUnit,
                               ct_QtyOnHand_ValStock,
                               ct_QtyOrdPriceUnit,
                               Dim_MovementTypeid,
                               dim_Companyid,
                               Dim_Currencyid,
                               Dim_Partid,
                               Dim_Plantid,
                               Dim_StorageLocationid,
                               Dim_Vendorid,
                               dim_DateIDMaterialDocDate,
                               dim_DateIDPostingDate,
                               dim_producthierarchyid,
                               dim_MovementIndicatorid,
                               dim_Customerid,
				 dim_CostCenterid,
                               dim_specialstockid,
                               dim_unitofmeasureid,
                               dim_controllingareaid,
                               dim_profitcenterid,
                               dim_consumptiontypeid,
                               dim_stocktypeid,
                               Dim_PurchaseGroupid,
                               dim_materialgroupid,
                               Dim_ReceivingPlantId,
                               Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
				Dim_AfsSizeId,
				Dim_Currencyid_TRA,
				Dim_Currencyid_GBL)
  Select fact_materialmovementid,
   				dd_MaterialDocNo,
                                dd_MaterialDocItemNo,
                                dd_MaterialDocYear,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dd_SalesOrderNo,
                                dd_SalesOrderItemNo,
                                dd_SalesOrderDlvrNo,
                                dd_GLAccountNo,
                                 dd_ReferenceDocNo,
                                 dd_ReferenceDocItem,
                                dd_debitcreditid,
                                dd_productionordernumber,
                                dd_productionorderitemno,
                                dd_GoodsMoveReason,
                                dd_BatchNumber,
                                dd_ValuationType,
                                amt_LocalCurrAmt,
                                amt_StdUnitPrice,
                                amt_DeliveryCost,
                                amt_AltPriceControl,
                                amt_OnHand_ValStock,
                                amt_ExchangeRate,
                                amt_ExchangeRate_GBL,
                                ct_Quantity,
                                ct_QtyEntryUOM,
                                ct_QtyGROrdUnit,
                                ct_QtyOnHand_ValStock,
                                ct_QtyOrdPriceUnit,
                                Dim_MovementTypeid,
                                dim_Companyid,
                                Dim_Currencyid,
                                Dim_Partid,
                                Dim_Plantid,
                                Dim_StorageLocationid,
                                Dim_Vendorid,
                                dim_DateIDMaterialDocDate,
                                dim_DateIDPostingDate,
                                dim_producthierarchyid,
                                dim_MovementIndicatorid,
                                dim_Customerid,
 				 dim_CostCenterid,
                                dim_specialstockid,
                                dim_unitofmeasureid,
                                dim_controllingareaid,
                                dim_profitcenterid,
                                dim_consumptiontypeid,
                                dim_stocktypeid,
                                Dim_PurchaseGroupid,
                                dim_materialgroupid,
                                Dim_ReceivingPlantId,
                                Dim_ReceivingPartId,
                               Dim_RecvIssuStorLocid,
				Dim_AfsSizeId,
				Dim_Currencyid_TRA,
				Dim_Currencyid_GBL
from fact_materialmovement_789;

drop table if exists fact_materialmovement_789;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'INSERT INTO fact_materialmovement(dd_MaterialDocNo 2');                         

call vectorwise (combine 'fact_materialmovement');

UPDATE fact_materialmovement mm 
 FROM dim_movementtype mt
SET  mm.dd_VendorSpendFlag = 1
WHERE mm.dim_movementtypeid = mt.dim_movementtypeid
AND ( mt.movementtype IN (101,102,105,106,122,123,161,162,501,502,521,522) OR mm.dd_ConsignmentFlag = 1)
AND mm.dd_VendorSpendFlag <> 1;

UPDATE fact_materialmovement mm
SET mm.amt_VendorSpend = ( CASE WHEN mm.amt_AltPriceControl = 0 THEN mm.amt_LocalCurrAmt ELSE mm.amt_AltPriceControl END) + amt_DeliveryCost
WHERE mm.dd_VendorSpendFlag = 1;

call vectorwise (combine 'fact_materialmovement');

        
     INSERT INTO processinglog (processinglogid, referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms,fact_purchase po');
     
  UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_purchasegroupid = po.dim_purchasegroupid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND  po.dim_purchasegroupid <> 1
	AND ms.dim_purchasegroupid <> po.dim_purchasegroupid;

 

  UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET        ms.dim_purchaseorgid = po.dim_purchaseorgid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_purchaseorgid <> po.dim_purchaseorgid;

     UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET      ms.dim_itemcategoryid = po.dim_itemcategoryid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_itemcategoryid <> po.dim_itemcategoryid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_Termid = po.dim_Termid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_Termid <> po.dim_Termid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_documenttypeid = po.dim_documenttypeid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_documenttypeid <> po.dim_documenttypeid;


          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_accountcategoryid = po.dim_accountcategoryid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dim_accountcategoryid <>  po.dim_accountcategoryid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dim_itemstatusid = po.dim_itemstatusid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.dim_itemstatusid <> po.dim_itemstatusid;


          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateIDDocCreation = po.Dim_DateidCreate
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDDocCreation <> po.Dim_DateidCreate;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateIDOrdered = po.Dim_DateidOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateIDOrdered <> po.Dim_DateidOrder;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_MaterialGroupid = po.Dim_MaterialGroupid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_MaterialGroupid <>  po.Dim_MaterialGroupid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.amt_POUnitPrice = po.amt_UnitPrice	* po.amt_ExchangeRate	/* amt_POUnitPrice should be in local curr */
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.amt_POUnitPrice <>  po.amt_UnitPrice * po.amt_ExchangeRate;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.amt_StdUnitPrice = po.amt_StdUnitPrice	* po.amt_ExchangeRate	/* Store in local curr */
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.amt_StdUnitPrice <> po.amt_StdUnitPrice * po.amt_ExchangeRate;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_DateidCosting = po.Dim_DateidCosting
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_DateidCosting <> po.Dim_DateidCosting;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_IncoTermid = po.Dim_IncoTerm1id
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_IncoTermid <> po.Dim_IncoTerm1id;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dd_incoterms2 = po.dd_incoterms2
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.dd_incoterms2 <>  po.dd_incoterms2;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dd_IntOrder = po.dd_IntOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND ms.dd_IntOrder <>  po.dd_IntOrder;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.dd_IntOrder = po.dd_IntOrder
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
        AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
        AND ms.dd_IntOrder is null
	AND po.dd_IntOrder is not null;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_POPlantidOrdering = po.Dim_PlantidOrdering
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POPlantidOrdering <> po.Dim_PlantidOrdering;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_POPlantidSupplying = po.Dim_PlantidSupplying
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POPlantidSupplying <> po.Dim_PlantidSupplying;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_POStorageLocid = po.Dim_StorageLocationid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND ms.Dim_POStorageLocid <>  po.Dim_StorageLocationid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_POIssuStorageLocid = po.Dim_IssuStorageLocid
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
AND  ms.Dim_POIssuStorageLocid <>  po.Dim_IssuStorageLocid;

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_ReceivingPlantId = (case when ms.Dim_ReceivingPlantId = 1 and ms.Dim_Plantid <> po.Dim_PlantidOrdering 
                                              and (ms.Dim_Plantid = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId
                                   end)
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND ms.Dim_ReceivingPlantId <> (case when ms.Dim_ReceivingPlantId = 1 and ms.Dim_Plantid <> po.Dim_PlantidOrdering 
                                              and (ms.Dim_Plantid = po.Dim_PlantidSupplying or pl.PlantCode = v.VendorNumber)
                                          then po.Dim_PlantidOrdering
                                        else ms.Dim_ReceivingPlantId
                                   end);

          UPDATE fact_materialmovement ms
From fact_purchase po, dim_vendor v, dim_plant pl
    SET ms.Dim_RecvIssuStorLocid = (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid
                                   end)
  WHERE     ms.dd_DocumentNo = po.dd_DocumentNo
        AND ms.dd_DocumentItemNo = po.dd_DocumentItemNo
	AND ms.Dim_AfsSizeId = po.Dim_AfsSizeId
        AND po.Dim_Vendorid = v.Dim_Vendorid
        AND ms.Dim_Plantid = pl.Dim_Plantid
	AND ms.Dim_RecvIssuStorLocid <> (case when ms.Dim_RecvIssuStorLocid = 1 and ms.Dim_StorageLocationid <> po.Dim_StorageLocationid 
                                              and ms.Dim_StorageLocationid = po.Dim_IssuStorageLocid 
                                          then po.Dim_StorageLocationid
                                        else ms.Dim_RecvIssuStorLocid
                                   end);
        
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'UPDATE fact_materialmovement ms,fact_purchase po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement m,fact_salesorder so');
  
  UPDATE fact_materialmovement ms
	FROM fact_salesorder so
    SET ms.dim_salesorderheaderstatusid = so.dim_salesorderheaderstatusid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderheaderstatusid <> so.dim_salesorderheaderstatusid;

  UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET         ms.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderitemstatusid <>  so.dim_salesorderitemstatusid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_salesdocumenttypeid = so.dim_salesdocumenttypeid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesdocumenttypeid <> so.dim_salesdocumenttypeid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_salesorderrejectreasonid = so.dim_salesorderrejectreasonid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorderrejectreasonid <> so.dim_salesorderrejectreasonid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_salesgroupid = so.dim_salesgroupid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesgroupid <>  so.dim_salesgroupid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_salesorgid = so.dim_salesorgid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_salesorgid <> so.dim_salesorgid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_customergroup1id = so.dim_customergroup1id
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_customergroup1id <>  so.dim_customergroup1id;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.dim_documentcategoryid = so.dim_documentcategoryid
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.dim_documentcategoryid <> so.dim_documentcategoryid;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.Dim_DateIDDocCreation = so.Dim_DateidSalesOrderCreated
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDDocCreation <>  so.Dim_DateidSalesOrderCreated;

          UPDATE fact_materialmovement ms
        FROM fact_salesorder so
    SET ms.Dim_DateIDOrdered = so.Dim_DateidSalesOrderCreated
  WHERE     ms.dd_SalesOrderNo = so.dd_SalesDocNo
        AND ms.dd_SalesOrderItemNo = so.dd_SalesItemNo
AND  ms.Dim_DateIDOrdered <> so.Dim_DateidSalesOrderCreated;
  
INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement m,fact_salesorder so');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_productionorder po');

  UPDATE fact_materialmovement ms
FROM fact_productionorder po
    SET ms.dim_productionorderstatusid = po.dim_productionorderstatusid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionorderstatusid <>  po.dim_productionorderstatusid;

UPDATE fact_materialmovement ms
FROM fact_productionorder po
    SET  ms.dim_productionordertypeid = po.Dim_ordertypeid
  WHERE     ms.dd_productionordernumber = po.dd_ordernumber
        AND ms.dd_productionorderitemno = po.dd_orderitemno
AND  ms.dim_productionordertypeid <> po.Dim_ordertypeid;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_productionorder po');

INSERT INTO processinglog (processinglogid,referencename, startdate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

  UPDATE fact_materialmovement ms
FROM fact_inspectionlot fil
    SET ms.dim_inspusagedecisionid = fil.dim_inspusagedecisionid
  WHERE     ms.dd_MaterialDocItemNo = fil.dd_MaterialDocItemNo
        AND ms.dd_MaterialDocNo = fil.dd_MaterialDocNo
        AND ms.dd_MaterialDocYear = fil.dd_MaterialDocYear
AND  ms.dim_inspusagedecisionid <> fil.dim_inspusagedecisionid;

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp),'UPDATE fact_materialmovement ms,fact_inspectionlot fil');

INSERT INTO processinglog (processinglogid,referencename, enddate, description) VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),'bi_populate_materialmovement_fact',TIMESTAMP_WO_TZ(current_timestamp), 'bi_populate_materialmovement_fact END');

drop table if exists bwtarupd_tmp_100;

create table bwtarupd_tmp_100 as
Select  x.MATNR, x.BWKEY, max((x.LFGJA * 100) + x.LFMON) updcol
from mbew_no_bwtar x
Group by  x.MATNR, x.BWKEY;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement;

UPDATE fact_materialmovement ia
FROM  mbew_no_bwtar m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt
SET amt_PlannedPrice1 = ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0)
 WHERE ia.dim_partid = prt.Dim_partid
  and ia.dim_plantid = pl.dim_plantid
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and amt_PlannedPrice1 <> ifnull(MBEW_ZPLP1/(case when PEINH=0 then null else PEINH end ),0);

UPDATE fact_materialmovement ia
FROM  mbew_no_bwtar m,
	bwtarupd_tmp_100 x,
       dim_plant pl,
       dim_part prt
SET amt_PlannedPrice = ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0)
 WHERE ia.dim_partid = prt.Dim_partid
  and ia.dim_plantid = pl.dim_plantid
  and prt.PartNumber = m.MATNR
  and pl.ValuationArea = m.BWKEY
  and ((m.LFGJA * 100) + m.LFMON) = updcol
  and  x.MATNR = m.MATNR
  and  x.BWKEY = m.BWKEY
  and  amt_PlannedPrice <> ifnull(MBEW_ZPLPR/(case when PEINH=0 then null else PEINH end),0);

  UPDATE fact_materialmovement ms
  FROM dim_unitofmeasure uom,MKPF_MSEG m,dim_plant dp,dim_company dc
  SET ms.dim_unitofmeasureorderunitid = uom.dim_unitofmeasureid
  WHERE uom.uom = m.MSEG_BSTME
AND m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_unitofmeasureorderunitid,-1) <> ifnull(uom.dim_unitofmeasureid,-2);

 UPDATE fact_materialmovement ms
  FROM dim_unitofmeasure uom,MKPF_MSEG m,dim_plant dp,dim_company dc
  SET ms.dim_uomunitofentryid = uom.dim_unitofmeasureid
  WHERE uom.uom = m.MSEG_ERFME
AND m.MSEG_WERKS = dp.PlantCode
AND m.MSEG_BUKRS = dc.CompanyCode
AND ms.dd_MaterialDocNo = m.MSEG_MBLNR
AND ms.dd_MaterialDocItemNo = m.MSEG_ZEILE
AND m.MSEG_MJAHR = ms.dd_MaterialDocYear
AND ifnull(ms.dim_uomunitofentryid,-1) <> ifnull(uom.dim_unitofmeasureid,-2); 

/*call bi_purchase_matmovement_dlvr_link()*/
drop table if exists dim_profitcenter_789;
drop table if exists bwtarupd_tmp_100;
