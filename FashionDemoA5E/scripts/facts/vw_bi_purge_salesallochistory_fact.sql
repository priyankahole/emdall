/* ##################################################################################################################
  
     Script         : vw_bi_purge_salesallochistory_fact
     Author         : Shanthi
     Created On     : 25 May 2014
  
  
     Description    : New subject area to store the Sales Allocation History
  
#################################################################################################################### */

select 'START OF PROC vw_bi_purge_salesallochistory_fact',TIMESTAMP(LOCAL_TIMESTAMP);

DROP table IF EXISTS fact_salesallochistory_Tmp;

CREATE TABLE fact_salesallochistory_Tmp AS
SELECT * from fact_salesallochistory WHERE 1 = 2;

call vectorwise(combine 'fact_salesallochistory');

INSERT INTO fact_salesallochistory_Tmp
Select ah.* from fact_salesallochistory ah,dim_Date dt
where ah.dim_DateIdSnapshot = dt.Dim_DateId
and ah.dd_SnapshotWeekID = 0
AND ah.dd_SnapshotMonthID = 0
AND dt.datevalue < (current_Date - interval '31' day);

INSERT INTO fact_salesallochistory_Tmp
Select ah.* from fact_salesallochistory ah,dim_Date dt
where ah.dim_DateIdSnapshot = dt.Dim_DateId
and ah.dd_SnapshotWeekID <> 0
AND dt.datevalue < (current_Date - interval '395' day)
and not exists ( SELECT 1 from fact_salesallochistory_Tmp t 
        where t.fact_salesallochistoryid = ah.fact_salesallochistoryid);

call vectorwise(combine 'fact_salesallochistory_tmp');

call vectorwise(combine 'fact_salesallochistory-fact_salesallochistory_tmp');

call vectorwise(combine 'fact_salesallochistory');

select 'END OF PROC vw_bi_purge_salesallochistory_fact',TIMESTAMP(LOCAL_TIMESTAMP);
