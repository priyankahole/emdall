select 'START OF PROC bi_populate_shipment_inbounddeliveries_fact',TIMESTAMP(LOCAL_TIMESTAMP);

Drop table if exists fact_shipmentindelivery_temp;

create table fact_shipmentindelivery_temp
AS
Select * from fact_shipmentindelivery where 1 = 2;

call vectorwise(combine 'fact_shipmentindelivery_temp');

select 'Insert 1 on fact_shipmentindelivery_temp',TIMESTAMP(LOCAL_TIMESTAMP);

delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery_temp',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery_temp;

INSERT INTO fact_shipmentindelivery_temp(fact_shipmentindeliveryid,
                                         dd_ShipmentNo,
                                         dd_ShipmentItemNo,
                                         dd_InboundDeliveryNo,
                                         dd_DocumentNo,
                                         dd_DocumentItemNo,
                                         dd_ScheduleNo,
                                         Dim_DateIdShipmentCreated,
                                         dd_Vessel,
                                         dd_MovementType,
                                         dd_CarrierActual,
                                         Dim_CarrierBillingId,
                                         dd_DomesticContainerID,
                                         Dim_ShipmentTypeId,
                                         Dim_RouteId,
                                         dD_VoyageNo,
                                         Dim_DateIdShipmentEnd,
                                         Dim_DateIdDepartureFromOrigin,
                                         dd_ContainerSize,
                                         dd_3PLFlag,
                                         ct_InboundDlvryCMtrVolume,
										 Dim_DateidDlvrDocCreated,
										 Dim_PlantId,
										 dd_billofladingNo,
										 Dim_DateIdPODelivery,
										 ct_DelivReceivedQty,
										 ct_ReceivedQty,
										 dim_inbrouteid,
										 ct_ItemQty,
										 Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
										 dim_dateidactualpo, /* Roxana 02 dec 2014 */
										dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
										dim_dateidMatAvailability,  /* Marius 15 Dec 2014 */
										dd_DeliveryNumber,          /* Marius 17 Dec 2014 */
										 dim_dateidTransfer, /* Marius 17 Dec 2014 */
										 ct_DeliveryGrossWght, /* Marius 17 Dec 2014 */
										 ct_deliveryNetWght, /* Marius 17 Dec 2014 */
										 Dim_DeliveryRouteId, /* Marius 17 Dec 2014 */
										 dim_shippingconditionid, /* Marius 17 Dec 2014 */
										dim_tranportbyshippingtypeid, /* Marius 6 Jan 2014 */
										dd_ShortText,
										dd_sealnumber,   /* Marius 5 feb 2015 */
										dd_noofforeigntrade , /* Marius 15 feb 2015 */
										dd_PackageCount, /* Marius 4 mar 2015 */ 
										dim_transportconditionid ) /*Roxana 08 may 2015*/										
   SELECT ((SELECT ifnull(max_id, 0)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_shipmentindelivery_temp') + row_number() over ()) fact_shipmentindeliveryid,
          a.*
     FROM (SELECT DISTINCT
                  VTTK_TKNUM,
                  VTTP_TPNUM,
                  ifnull(VTTP_VBELN, 'Not Set'),
                  EKES_EBELN,
                  EKES_EBELP,
                  EKES_J_3AETENR,
                  ifnull(
                     (SELECT Dim_Dateid
                        FROM Dim_Date
                       WHERE     datevalue = VTTK_ERDAT
                             AND CompanyCode = 'Not Set'),
                     1),
                  ifnull(VTTK_TPBEZ, 'Not Set'),
                  ifnull(VTTK_TEXT4, 'Not Set'),
                  ifnull(VTTK_TEXT3, 'Not Set'),
                  ifnull((SELECT Dim_VendorId 
			    FROM Dim_Vendor 
			   WHERE VendorNumber = VTTK_TDLNR
			     AND RowIsCurrent = 1), 1),
                  ifnull(VTTK_SIGNI, 'Not Set'),
                  ifnull(
                     (SELECT dim_Shipmenttypeid
                        FROM dim_ShipmentType st
                       WHERE     st.ShipmentType = VTTK_SHTYP
                             AND st.RowIsCurrent = 1),
                     1),
                  ifnull(
                     (SELECT dim_Routeid
                        FROM dim_Route r
                       WHERE r.RouteCode = VTTK_ROUTE AND r.RowIsCurrent = 1),
                     1),
                  ifnull(VTTK_EXTI2, 'Not Set'),
                  ifnull(
                     (SELECT Dim_Dateid
                        FROM Dim_Date
                       WHERE     datevalue = VTTK_DPTEN
                             AND CompanyCode = 'Not Set'),
                     1)
                     Dim_DateIdShipmentEnd,
                  ifnull(
                     (SELECT Dim_Dateid
                        FROM Dim_Date
                       WHERE     datevalue = VTTK_DATBG
                             AND CompanyCode = 'Not Set'),
                     1)
                     Dim_DateIdDepartureFromOrigin,
                  ifnull(VTTK_ADD01, 'Not Set'),
                  'Not Set', 
                  ifnull(LIKP_VOLUM, 0.0000) ct_InboundDlvryCMtrVolume,
				  ifnull((SELECT Dim_Dateid
                        FROM Dim_Date
                       WHERE     datevalue = LIKP_ERDAT
                             AND CompanyCode = 'Not Set'),
                     1) Dim_DateidDlvrDocCreated,
				   p.dim_plantidordering,
	         ifnull((CASE WHEN VTTK_EXTI1 IS NULL THEN LIKP_BOLNR ELSE VTTK_EXTI1 END),'Not Set') ,
		 p.dim_DateIdDelivery,
		 ifnull(EKES_DABMG, 0.0000),
		 ifnull(EKES_MENGE, 0.0000) ct_ReceivedQty,
		   ifnull(
                     (SELECT dim_Routeid
                        FROM dim_Route r
                       WHERE r.RouteCode = v.LIKP_ROUTE AND r.RowIsCurrent = 1),
                     1) dim_inbrouteid,
					 p.ct_ItemQty,
					 p.Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
					 p.Dim_DateidOrder,
					 p.dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
					 p.dim_dateidMatAvailability,
					 p.dd_DeliveryNumber,
		ifnull((SELECT Dim_Dateid
                        FROM Dim_Date
                       WHERE     datevalue = LIKP_BLDAT
                             AND CompanyCode = 'Not Set'),
                     1) dim_dateidTransfer,
		ifnull(LIKP_BTGEW,0.0000) ct_DeliveryGrossWght,
		ifnull(LIKP_NTGEW,0.0000) ct_deliveryNetWght,
		ifnull(
                     (SELECT dim_Routeid
                        FROM dim_Route r
                       WHERE r.RouteCode = LIKP_ROUTE AND r.RowIsCurrent = 1),
                     1) Dim_DeliveryRouteId,
		ifnull( (select dim_shippingconditionid from dim_shippingcondition s 
					where s.shippingconditioncode = LIKP_VSBED and s.rowiscurrent = 1)
			, 1) dim_shippingconditionid,
		ifnull( (select dim_tranportbyshippingtypeid from dim_tranportbyshippingtype tbst
					where tbst.ShippingType = LIKP_VSART )
			, 1) dim_tranportbyshippingtypeid,
		p.dd_ShortText,
		ifnull(VTTK_TEXT1,'Not Set') as dd_sealnumber,
        p.dd_noofforeigntrade,
		ifnull(LIKP_ANZPK, 0) dd_PackageCount,
		ifnull( (select dim_tranportbyshippingtypeid from dim_tranportbyshippingtype tbst
					where tbst.ShippingType = VTTK_VSART )
			, 1) dim_transportconditionid
             FROM VTTK_VTTP v1
                  INNER JOIN EKES_LIKP_LIPS v ON v1.VTTP_VBELN = v.LIKP_VBELN 
				  /* AND v.LIKP_WADAT_IST IS NOT NULL */
                  INNER JOIN facT_purchase p
                     ON     dd_DocumentNo = EKES_EBELN
                        AND dd_DocumentItemNo = EKES_EBELP
                        AND dd_ScheduleNo = EKES_J_3AETENR
            WHERE NOT EXISTS
                         (SELECT 1
                            FROM fact_shipmentindelivery_temp f1
                           WHERE     f1.dd_ShipmentNo = VTTK_TKNUM
                                 AND f1.dd_ShipmentItemNo = VTTP_TPNUM
                                 AND f1.dd_InboundDeliveryNo = LIKP_VBELN
                                 AND f1.dd_DocumentNo = EKES_EBELN
                                 AND f1.dd_DocumentItemNo = EKES_EBELP
                                 AND f1.dd_ScheduleNo = EKES_J_3AETENR)) a;
				 
call vectorwise (combine 'fact_shipmentindelivery_temp');

delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery_temp',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery_temp;

INSERT INTO fact_shipmentindelivery_temp(fact_shipmentindeliveryid,
                                         dd_DocumentNo,
                                         dd_DocumentItemNo,
                                         dd_ScheduleNo,
                                         dd_ShipmentNo,
                                         dd_ShipmentItemNo,
                                         dd_InboundDeliveryNo,
                                         Dim_DateIdShipmentCreated,
                                         dd_Vessel,
                                         dd_MovementType,
                                         dd_CarrierActual,
                                         Dim_CarrierBillingId,
                                         dd_DomesticContainerID,
                                         Dim_ShipmentTypeId,
                                         Dim_RouteId,
                                         dD_VoyageNo,
                                         Dim_DateIdShipmentEnd,
                                         Dim_DateIdDepartureFromOrigin,
                                         dd_ContainerSize,
                                         dd_3PLFlag,
                                         Dim_DateIdPOBuy,
                                         Dim_PODocumentTypeId,
                                         Dim_DateIdPOETA,
                                         dim_purchasegroupid,
                                         Dim_partid,
                                         Dim_Vendorid,
                                         Dim_DateIdGR,
                                         Dim_PORouteId,
                                         Dim_CustomVendorPartnerFunctionId,
                                         Dim_CustomVendorPartnerFunctionId1,
                                         Dim_POAFSSeasonId,
                                         ct_POScheduleQty,
                                         ct_POItemReceivedQty,
					 ct_POInboundDeliveryQty,
                                         dd_CustomerPOStatus,
                                         dd_PackageCount,
                                         dd_BillofLadingNo,
                                         Dim_CompanyId,
                                         Dim_PlantId,
                                         dd_OriginContainerId,
                                         ct_InboundDlvryCMtrVolume,
										 Dim_DateidDlvrDocCreated,
										 Dim_DateIdPODelivery,
										 ct_DelivReceivedQty,
										 ct_ReceivedQty,
										 dd_ShipmentStatus,
										 dim_inbrouteid,
										 ct_ItemQty,
										 Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
										 dim_dateidactualpo,  /* Roxana 02 dec 2014 */
										 dim_dateidAFSExFactEst,  /* Marius 03 Dec 2014 */
										 dim_dateidMatAvailability,
										 dd_DeliveryNumber,
										 dim_dateidTransfer,
										 ct_DeliveryGrossWght, 
										 ct_deliveryNetWght, 
										 Dim_DeliveryRouteId,
										 dim_shippingconditionid,
										 dim_tranportbyshippingtypeid,
										 dd_ShortText,
										 dd_noofforeigntrade)
   SELECT ((SELECT ifnull(max_id, 1)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_shipmentindelivery_temp') + row_number() over ()) fact_shipmentindeliveryid,
          a.*
     FROM (SELECT DISTINCT
                  p.dd_DocumentNo,
                  p.dd_DocumentItemNo,
                  p.dd_ScheduleNo,
                  'Not Set',
                  0,
                  LIKP_VBELN,
                  1,
                  'Not Set',
                  'Not Set',
                  'Not Set',
                  1,
                  'Not Set',
                  1 Dim_ShipmentTypeId,
                  1 Dim_RouteId,
                  'Not Set',
                  1 Dim_DateIdShipmentEnd,
                  1 Dim_DateIdDepartureFromOrigin,
                  'Not Set',
                  'Not Set',
                  p.Dim_DateIdSchedOrder,
                  p.Dim_DocumentTypeid,
                  p.Dim_DateidAFSDelivery,
                  dim_purchasegroupid,
                  Dim_partid,
                  Dim_Vendorid,
                  Dim_DateIdLastGR,
                  p.dim_AFSRouteid,
                  p.Dim_CustomPartnerFunctionId4,
                  p.Dim_CustomPartnerFunctionId3,
                  p.Dim_AfsSeasonId,
                  ct_DeliveryQty,
                  p.ct_ReceivedQty ct_POItemReceivedQty,
		  ct_QtyReduced,
                  'Not Set',
                  ifnull(LIKP_ANZPK, 0),
                  ifnull(LIKP_BOLNR, 'Not Set'),
                  p.Dim_CompanyId,
                  Dim_plantidordering,
                  ifnull(LIKP_TRAID, 'Not Set'),
                  ifnull(LIKP_VOLUM, 0.0000) ct_InboundDlvryCMtrVolume,
				  ifnull((SELECT Dim_Dateid
                        FROM Dim_Date
                       WHERE     datevalue = LIKP_ERDAT
                             AND CompanyCode = 'Not Set'),
                     1) Dim_DateidDlvrDocCreated,
		     p.Dim_DateIdDelivery,
			 ifnull(EKES_DABMG, 0.0000),
			 ifnull(EKES_MENGE, 0.0000) ct_ReceivedQty,
			 'Not Set',
			 ifnull(
                     (SELECT dim_Routeid
                        FROM dim_Route r
                       WHERE r.RouteCode = LIKP_ROUTE AND r.RowIsCurrent = 1),
                     1) dim_inbrouteid,
					 p.ct_ItemQty,
					 p.Dim_AFSTranspConditionid, /* MArius 28 nov 2014 */
					 p.Dim_DateidOrder,
					 p.dim_dateidAFSExFactEst,   /* MArius 03 Dec 2014 */
					 p.dim_dateidMatAvailability,
					 p.dd_DeliveryNumber,
			ifnull((SELECT Dim_Dateid
							FROM Dim_Date
						   WHERE     datevalue = LIKP_BLDAT
								 AND CompanyCode = 'Not Set'),
						 1) dim_dateidTransfer,
			ifnull(LIKP_BTGEW,0.0000) ct_DeliveryGrossWght,
			ifnull(LIKP_NTGEW,0.0000) ct_deliveryNetWght,
			ifnull(
						 (SELECT dim_Routeid
							FROM dim_Route r
						   WHERE r.RouteCode = LIKP_ROUTE AND r.RowIsCurrent = 1),
						 1) Dim_DeliveryRouteId,
			ifnull( (select dim_shippingconditionid from dim_shippingcondition s 
						where s.shippingconditioncode = LIKP_VSBED and s.rowiscurrent = 1)
				, 1) dim_shippingconditionid,
			ifnull( (select dim_tranportbyshippingtypeid from dim_tranportbyshippingtype tbst
						where tbst.ShippingType = LIKP_VSART )
				, 1) dim_tranportbyshippingtypeid,
			p.dd_ShortText,
			p.dd_noofforeigntrade
		
             FROM facT_purchase p
                  INNER JOIN EKES_LIKP_LIPS
                     ON     dd_DocumentNo = EKES_EBELN
                        AND dd_DocumentItemNo = EKES_EBELP
                        AND dd_ScheduleNo = EKES_J_3AETENR
            WHERE NOT EXISTS
                         (SELECT 1
                            FROM fact_shipmentindelivery_temp f1
                           WHERE     f1.dd_InboundDeliveryNo = LIKP_VBELN
                                 AND f1.dd_DocumentNo = EKES_EBELN
                                 AND f1.dd_DocumentItemNo = EKES_EBELP
                                 AND f1.dd_ScheduleNo = EKES_J_3AETENR)) a;

call vectorwise (combine 'fact_shipmentindelivery_temp');

delete from NUMBER_FOUNTAIN where table_name = 'fact_shipmentindelivery_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_shipmentindelivery_temp',ifnull(max(fact_shipmentindeliveryid),1)
FROM fact_shipmentindelivery_temp;

INSERT INTO fact_shipmentindelivery_temp(fact_shipmentindeliveryid,
                                         dd_DocumentNo,
                                         dd_DocumentItemNo,
                                         dd_ScheduleNo,
                                         dd_ShipmentNo,
                                         dd_ShipmentItemNo,
                                         dd_InboundDeliveryNo,
                                         Dim_DateIdShipmentCreated,
                                         dd_Vessel,
                                         dd_MovementType,
                                         dd_CarrierActual,
                                         Dim_CarrierBillingId,
                                         dd_DomesticContainerID,
                                         Dim_ShipmentTypeId,
                                         Dim_RouteId,
                                         dD_VoyageNo,
                                         Dim_DateIdShipmentEnd,
                                         Dim_DateIdDepartureFromOrigin,
                                         dd_ContainerSize,
                                         dd_3PLFlag,
                                         Dim_DateIdPOBuy,
                                         Dim_PODocumentTypeId,
                                         Dim_DateIdPOETA,
                                         dim_purchasegroupid,
                                         Dim_partid,
                                         Dim_Vendorid,
                                         Dim_DateIdGR,
                                         Dim_PORouteId,
                                         Dim_CustomVendorPartnerFunctionId,
                                         Dim_CustomVendorPartnerFunctionId1,
                                         Dim_POAFSSeasonId,
                                         ct_POScheduleQty,
                                         ct_POItemReceivedQty,
					 ct_POInboundDeliveryQty,
                                         dd_CustomerPOStatus,
                                         dd_PackageCount,
                                         dd_BillofLadingNo,
                                         Dim_CompanyId,
                                         Dim_PlantId,
                                         dd_OriginContainerId,
                                         ct_InboundDlvryCMtrVolume,
                                         ct_InboundDeliveryQty,
										 Dim_DateidDlvrDocCreated,
										 Dim_DateIdPODelivery,
										 ct_DelivReceivedQty,
										 ct_ReceivedQty,
										 dd_ShipmentStatus,
										 ct_ItemQty,
										 Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
										 dim_dateidactualpo,
										 dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
										 dim_dateidMatAvailability,
										 dd_DeliveryNumber,
										 dim_dateidTransfer,
										 ct_DeliveryGrossWght, 
										 ct_deliveryNetWght, 
										 Dim_DeliveryRouteId, 
										 dim_shippingconditionid,
										 dim_tranportbyshippingtypeid,
										 dd_ShortText,
										 dd_noofforeigntrade,
										 dim_transportconditionid
										 )
   SELECT ((SELECT ifnull(max_id, 1)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_shipmentindelivery_temp') + row_number() over ()) fact_shipmentindeliveryid,
                  p.dd_DocumentNo,
                  p.dd_DocumentItemNo,
                  p.dd_ScheduleNo,
                  'Not Set',
                  0,
                  'Not Set',
                  1,
                  'Not Set',
                  'Not Set',
                  'Not Set',
                  1,
                  'Not Set',
                  1 Dim_ShipmentTypeId,
                  1 Dim_RouteId,
                  'Not Set',
                  1 Dim_DateIdShipmentEnd,
                  1 Dim_DateIdDepartureFromOrigin,
                  'Not Set',
                  'Not Set',
                  p.Dim_DateIdSchedOrder,
                  p.Dim_DocumentTypeid,
                  p.Dim_DateidAFSDelivery,
                  dim_purchasegroupid,
                  Dim_partid,
                  Dim_Vendorid,
                  Dim_DateIdLastGR,
                  p.dim_AFSRouteid,
                  p.Dim_CustomPartnerFunctionId4,
                  p.Dim_CustomPartnerFunctionId3,
                  p.Dim_AfsSeasonId,
                  ct_DeliveryQty,
                  p.ct_ReceivedQty ct_POItemReceivedQty,
		  ct_QtyReduced,
                  'Not Set',
                  0,
                  'Not Set' dd_BillofLadingNo,
                  p.Dim_CompanyId,
                  Dim_plantidordering,
                  'Not Set',
                  0.0000,
                  0,
				  1 Dim_DateidDlvrDocCreated,
				  p.Dim_DateIdDelivery,
				  0.0000,
				  0.0000 ct_ReceivedQty,
				  'Not Set',
				  p.ct_ItemQty,
				  p.Dim_AFSTranspConditionid, /* Marius 28 nov 2014 */
				  p.Dim_DateidOrder,
				  p.dim_dateidAFSExFactEst, /* Marius 03 Dec 2014 */
				  p.dim_dateidMatAvailability,
				  p.dd_DeliveryNumber,
				  1 dim_dateidTransfer,
				  0.0000 ct_DeliveryGrossWght,
				  0.0000 ct_deliveryNetWght,
				  1 Dim_DeliveryRouteId,
				  1 dim_shippingconditionid,
				  1 dim_tranportbyshippingtypeid,
				  'Not Set',
				  p.dd_noofforeigntrade,
				  1 dim_transportconditionid
				  
             FROM facT_purchase p
            WHERE NOT EXISTS
                         (SELECT 1
                            FROM fact_shipmentindelivery_temp f1
                           WHERE    f1.dd_DocumentNo = p.dd_DocumentNo
                                 AND f1.dd_DocumentItemNo = p.dd_DocumentItemNo
                                 AND f1.dd_ScheduleNo = p.dd_ScheduleNo);

call vectorwise (combine 'fact_shipmentindelivery_temp');

DROP TABLE IF EXISTS tmp_EKES_LIKP_LIPS;

CREATE TABLE tmp_EKES_LIKP_LIPS AS 
SELECT EKES_EBELN,EKES_EBELP,EKES_J_3AETENR,LIKP_ANZPK,LIKP_BOLNR,LIKP_LFDAT,LIKP_TRAID,LIKP_TRATY,LIKP_VBELN,LIKP_VOLUM,LIKP_WADAT_IST,LIKP_ERDAT,sum(LIPS_LFIMG) AS LFIMG
FROM EKES_LIKP_LIPS
WHERE ekes_j_3aetenr <> 0 
group by EKES_EBELN,EKES_EBELP,EKES_J_3AETENR,LIKP_ANZPK,LIKP_BOLNR,LIKP_LFDAT,LIKP_TRAID,LIKP_TRATY,LIKP_VBELN,LIKP_VOLUM,LIKP_WADAT_IST,LIKP_ERDAT;

 UPDATE fact_shipmentindelivery_temp ft
 FROM tmp_EKES_LIKP_LIPS elp, Dim_Date dt,
 Dim_Company c
SET ft.Dim_DateIdETAFinalDest = dt.Dim_DateId
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_LFDAT
  AND dt.CompanyCode = c.CompanyCode;
  
   UPDATE fact_shipmentindelivery_temp ft
 FROM tmp_EKES_LIKP_LIPS elp, Dim_Date dt,
 Dim_Company c
SET ft.Dim_DateIdInboundDeliveryDate = dt.Dim_DateId
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_LFDAT
  AND dt.CompanyCode = c.CompanyCode;

UPDATE fact_shipmentindelivery_temp ft
 FROM tmp_EKES_LIKP_LIPS elp, Dim_PackagingMaterialType pmt
SET Dim_MeansofTransTypeId = pmt.Dim_PackagingMaterialTypeId
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND pmt.PackagingMaterialType = LIKP_TRATY
  AND pmt.RowIsCurrent = 1;

UPDATE fact_shipmentindelivery_temp ft
 FROM tmp_EKES_LIKP_LIPS elp, Dim_Date dt,
 Dim_Company c
SET ft.Dim_DateidActualGI = dt.Dim_DateId
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_WADAT_IST
  AND dt.CompanyCode = c.companycode;

  
UPDATE fact_shipmentindelivery_temp ft
 FROM tmp_EKES_LIKP_LIPS elp, Dim_Date dt,
 Dim_Company c
SET ft.Dim_DateidDlvrDocCreated = dt.Dim_DateId
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
  AND ft.dim_companyid = c.dim_companyid
  AND dt.DateValue = elp.LIKP_ERDAT
  AND dt.CompanyCode = c.companycode;  
  
UPDATE fact_shipmentindelivery_temp ft
 FROM tmp_EKES_LIKP_LIPS elp
SET ft.dd_OriginContainerId = ifnull(LIKP_TRAID,'Not Set')
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR;

UPDATE fact_shipmentindelivery_temp ft
 FROM tmp_EKES_LIKP_LIPS elp
SET ft.ct_InboundDeliveryQty = ifnull(LFIMG,0)
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
  AND ft.dd_DocumentNo = elp.EKES_EBELN
  AND ft.dd_DocumentItemNo = elp.EKES_EBELP
  AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR;

DROP TABLE IF EXISTS tmp_EKES_LIKP_LIPS;

UPDATE fact_shipmentindelivery_temp ft
 FROM EKES_LIKP_LIPS elp, EIKP e
SET dd_3PLFlag = ifnull(EIKP_LADEL, 'Not Set')
WHERE ft.dd_InboundDeliveryNo = elp.LIKP_VBELN
AND ft.dd_DocumentNo = elp.EKES_EBELN
AND ft.dd_DocumentItemNo = elp.EKES_EBELP
AND ft.dd_ScheduleNo = elp.EKES_J_3AETENR
AND elp.LIKP_EXNUM = e.EIKP_EXNUM
AND (dd_3PLFlag IS NULL OR dd_3PLFlag = 'Not Set');

UPDATE facT_shipmentindelivery_temp ft
 FROM NAST n, Dim_Date dt, Dim_company c
SET ft.Dim_DateidASNOutput =  dt.dim_dateid
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND ft.dim_companyid = c.dim_companyid
AND n.NAST_DATVR = dt.DateValue
AND dt.CompanyCode = c.CompanyCode;

UPDATE facT_shipmentindelivery_temp ft
 FROM NAST n, Dim_ProcessingMessageStatus s
SET ft.Dim_ProcessingStatusId =  s.Dim_ProcessingMessageStatusId
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_VSTAT = s.ProcessingMessage
AND n.NAST_KSCHL = 'ZINB'
AND s.RowIsCurrent = 1;

UPDATE facT_shipmentindelivery_temp ft
 FROM NAST n
SET ft.dd_processingtime =  n.NAST_UHRVR
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_UHRVR IS NOT NULL;

UPDATE facT_shipmentindelivery_temp ft
 FROM NAST n
SET ft.dd_processingtime =  '000000'
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY
AND n.NAST_UHRVR IS NULL;

UPDATE facT_shipmentindelivery_temp ft
 FROM NAST n
SET ft.dd_MessageType =  n.NAST_KSCHL
WHERE ft.dd_ShipmentNo = n.NAST_OBJKY;

DROP TABLE IF EXISTS tmp_vttk_vttp_maxcreateddatetime; 
CREATE table tmp_vttk_vttp_maxcreateddatetime as
SELECT vttp_vbeln, vttk_tknum,vttk_erdat,vttk_erzet,RANK() OVER (PARTITION BY vttp_vbeln ORDER BY vttk_erdat DESC,vttk_erzet DESC) as Rank from vttk_vttp; 
 
update fact_shipmentindelivery_temp  t1
  from tmp_vttk_vttp_maxcreateddatetime t
set dd_CurrentShipmentFlag = 'X' 
WHERE t.vttp_vbeln = t1.dd_InbounddeliveryNo
AND t.vttk_tknum = t1.dd_ShipmentNo
AND t.Rank = 1;

update fact_shipmentindelivery_temp  t1
  from tmp_vttk_vttp_maxcreateddatetime t
set dd_CurrentShipmentFlag = 'Not Set' 
WHERE t.vttp_vbeln = t1.dd_InbounddeliveryNo
AND t.vttk_tknum = t1.dd_ShipmentNo
AND t.Rank <> 1;

DROP TABLE IF EXISTS tmp_vttk_vttp_maxcreateddatetime; 

UPDATE fact_shipmentindelivery_temp t1
SET t1.dd_CartonLevelDtlFlag = 'X'
WHERE t1.dd_InboundDeliveryNo <> 'Not Set'
AND EXISTS ( SELECT 1 FROM vekp v
               WHERE v.VEKP_VPOBJKEY = t1.dd_InboundDeliveryNo);

UPDATE fact_shipmentindelivery_temp t1
SET t1.dd_CartonLevelDtlFlag = 'Not Set'
WHERE t1.dd_InboundDeliveryNo <> 'Not Set'
AND NOT EXISTS ( SELECT 1 FROM vekp v
                WHERE v.VEKP_VPOBJKEY = t1.dd_InboundDeliveryNo);

UPDATE	facT_shipmentindelivery_temp  st
 FROM fact_purchase p
SET st.Dim_DateIdPOBuy = p.Dim_DateIdSchedOrder,
    st.Dim_PODocumentTypeId = p.Dim_DocumentTypeid,
    st.Dim_DateIdPOETA = p.Dim_DateidAFSDelivery
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

/* Update Material Description = dd_ShortText*/ 

UPDATE	facT_shipmentindelivery_temp  st
 FROM fact_purchase p
SET st.dd_ShortText = p.dd_ShortText    
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

/* Commented Marius

update facT_shipmentindelivery_temp st
FROM VBFA_MKPF vm, dim_date dt
SET st.Dim_DateIdGR = dt.dim_dateid
Where vm.MKPF_BUDAT = dt.DateValue
AND st.dd_InboundDeliveryNo=vm.VBFA_VBELV
AND st.Dim_DateIdGR <> dt.dim_dateid */


UPDATE	facT_shipmentindelivery_temp  st
 FROM fact_purchase p
SET st.dim_purchasegroupid = p.Dim_PurchaseGroupId,
    st.Dim_PartId = p.Dim_Partid,
    st.Dim_VendorId = p.Dim_vendorid
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

UPDATE fact_shipmentindelivery_temp t
 FROM dim_part pt, dim_producthierarchy ph
SET t.Dim_ProductHierarchyId = ph.Dim_ProductHierarchyId
WHERE t.dim_partid = pt.dim_partid
  AND pt.ProductHierarchy = ph.ProductHierarchy
  AND ph.RowIsCurrent = 1;

UPDATE fact_shipmentindelivery_temp st
 FROM fact_purchase p
SET st.ct_POInboundDeliveryQty = p.ct_QtyReduced
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo
AND st.dd_InboundDeliveryNo <> 'Not Set'
AND st.ct_POInboundDeliveryQty = 0;
  
UPDATE facT_shipmentindelivery_temp  st
 FROM fact_purchase p
SET st.Dim_PORouteId = p.dim_AFSRouteid,
    st.Dim_CustomVendorPartnerFunctionId = p.Dim_CustomPartnerFunctionId4,
    st.Dim_CustomVendorPartnerFunctionId1 = p.Dim_CustomPartnerFunctionId3,
    st.Dim_POAFSSeasonId = p.Dim_AfsSeasonId
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_ScheduleQty_POItem;
CREATE TABLE tmp_ScheduleQty_POItem AS
SELECT dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo, max(ct_DeliveryQty) as ScheduleQty,max(ct_ReceivedQty) as ReceivedQty from fact_purchase
group by dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo;

UPDATE facT_shipmentindelivery_temp  st
FROM tmp_ScheduleQty_POItem p
SET st.ct_POScheduleQty = p.ScheduleQty,
    st.ct_POItemReceivedQty = p.ReceivedQty
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp_ScheduleQty_POItem;

UPDATE facT_shipmentindelivery_temp st
 FROM fact_purchase a
  SET st.dd_SalesDocNo = ifnull(a.dd_salesdocno,'Not Set'),
      st.dd_SalesItemNo = ifnull(a.dd_salesitemno,0),
      st.dd_SalesScheduleNo = ifnull(a.dd_Salesscheduleno,0)
WHERE st.dd_DocumentNo = a.dd_DocumentNo
AND st.dd_DocumentItemNo = a.dd_DocumentItemNo
AND st.dd_ScheduleNo = a.dd_ScheduleNo;

UPDATE facT_shipmentindelivery_temp st
 FROM fact_SalesOrder so
  SET st.Dim_DateIdSOCancelDate = so.Dim_DateIdAfsCancelDate,
      st.Dim_SalesDocumentTypeid = so.Dim_SalesDocumentTypeid,
      st.Dim_DateIdReqDeliveryDate = so.Dim_DateIdAfsReqDelivery,
	  st.Dim_CustomPartnerFunctionId1 = so.Dim_CustomPartnerFunctionId1
WHERE st.dd_SalesDocNo = so.dd_SalesDocNo
  AND st.dd_SalesItemNo = so.dd_SalesItemNo
  AND st.dd_SalesScheduleNo = so.dd_ScheduleNo;
  
/*Added on Dec 10th 2014, by Roxana*/

UPDATE facT_shipmentindelivery_temp st
FROM fact_SalesOrder so
SET st.Dim_DocumentCategoryid=so.Dim_DocumentCategoryid
WHERE st.dd_SalesDocNo = so.dd_SalesDocNo
  AND st.dd_SalesItemNo = so.dd_SalesItemNo
  AND st.dd_SalesScheduleNo = so.dd_ScheduleNo
  AND st.Dim_DocumentCategoryid<>so.Dim_DocumentCategoryid;
  
DROP TABLE IF EXISTS tmp_AirSeaEstDateCalculation;

CREATE TABLE  tmp_AirSeaEstDateCalculation as 
select distinct vttk_shtyp ShipType, vttk_Dpten shipenddate, vttk_route as Route, substr(vttk_route,1,5) as altrouteprefix, 'Not Set' as altroute, cast(0 as decimal(11,0)) as totalduration from vttk_vttp
where vttk_Dpten IS NOT NULL and vttk_Route is not null;

UPDATE tmp_AirSeaEstDateCalculation t2
   FROM tvrab t1
SET t2.altroute = t1.tvrab_route,
    t2.totalduration = t1.tvrab_gesztd
where t1.tvrab_route <> t2.route
AND substr(t1.tvrab_route,1,5) = t2.altrouteprefix
AND t2.altroute = 'Not Set';

UPDATE tmp_AirSeaEstDateCalculation t2
SET totalduration = round(totalduration/(3600*24),0)
where totalduration <> 0;

UPDATE facT_shipmentindelivery_temp t
   FROM tmp_AirSeaEstDateCalculation t1,
       dim_Route r1
SET t.Dim_DateIdAirorSeaPlanningEstETA =  t.Dim_DateIdShipmentEnd
where r1.dim_routeid = t.dim_routeid
AND r1.routecode = t1.route
AND t1.totalduration = 0;

DROP TABLE IF EXISTS tmp_DateAirSeaCalculation;
CREATE TABLE tmp_DateAirSeaCalculation AS
SELECT  distinct r1.dim_routeid, t.Dim_DateIdShipmentEnd, dt2.dim_dateid  FROM facT_shipmentindelivery_temp t,
       dim_company c1, dim_shipmenttype st, dim_Route r1,
       dim_date dt,
	   tmp_AirSeaEstDateCalculation t1,
       dim_Date dt2
where t.dim_Companyid = c1.dim_companyid
AND  t.dim_shipmenttypeid = st.dim_shipmenttypeid 
AND t.Dim_DateIdShipmentEnd = dt.dim_Dateid
AND r1.dim_routeid = t.dim_routeid
AND dt.datevalue = t1.shipenddate
AND r1.routecode = t1.route
and st.shipmenttype = t1.ShipType
AND t1.totalduration > 0
AND dt2.datevalue = (dt.datevalue + (interval '1' day)*t1.totalduration )
AND dt2.companycode = c1.companycode;

UPDATE facT_shipmentindelivery_temp t
   FROM tmp_DateAirSeaCalculation t1
SET t.Dim_DateIdAirorSeaPlanningEstETA =  t1.dim_dateid
where t.dim_routeid = t1.dim_routeid
AND t.Dim_DateIdShipmentEnd = t1.Dim_DateIdShipmentEnd
AND (t.Dim_DateIdAirorSeaPlanningEstETA IS NULL OR t.Dim_DateIdAirorSeaPlanningEstETA = 1);

DROP TABLE IF EXISTS tmp_DateAirSeaCalculation;
DROP TABLE IF EXISTS tmp_AirSeaEstDateCalculation;   

/* Andra : 19th of May 2014 : Add Dim_PurchaseMiscid, dd_DeletionIndicator*/ 

UPDATE fact_shipmentindelivery_temp  st
 FROM fact_purchase p
SET st.Dim_PurchaseMiscid = p.Dim_PurchaseMiscid
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

UPDATE fact_shipmentindelivery_temp  st
 FROM fact_purchase p
SET st.dd_DeletionIndicator = p.dd_DeletionIndicator
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

UPDATE fact_shipmentindelivery_temp  st
 FROM fact_purchase p
SET st.ct_scheduleLineQty = ifnull(p.ct_scheduleLineQty,0)
WHERE st.dd_DocumentNo = p.dd_DocumentNo
AND st.dd_DocumentItemNo = p.dd_DocumentItemNo
AND st.dd_ScheduleNo = p.dd_ScheduleNo;

DROP TABLE IF EXISTS tmp1_facT_shipmentindelivery_temp;
CREATE TABLE tmp1_facT_shipmentindelivery_temp AS 
sELECT dd_DocumentNo,dd_DocumentItemNo,sum(ct_POInboundDeliveryQty) as InboundQty,
sum(ct_POItemReceivedQty) as Receivedqty from facT_shipmentindelivery_temp
group by dd_DocumentNo,dd_DocumentItemNo;

UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp1_facT_shipmentindelivery_temp t1
SET dd_CustomerPOStatus = 'Open'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.InboundQty = 0;

UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp1_facT_shipmentindelivery_temp t1
SET dd_CustomerPOStatus = 'In-Transit'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.ReceivedQty = 0
  AND t1.InboundQty > 0;

UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp1_facT_shipmentindelivery_temp t1
SET dd_CustomerPOStatus = 'Received'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete <> 'X'
  AND t1.ReceivedQty > 0;

UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm
SET dd_CustomerPOStatus = 'Closed'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND pm.ItemDeliveryComplete = 'X'
  AND pm.ItemGRIndicator = 'X'
  AND t.ct_POItemReceivedQty > 0;

DROP TABLE IF EXISTS tmp1_facT_shipmentindelivery_temp;  

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POScheduleQty
WHERE   dd_CustomerPOStatus IN ('Open');

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POItemReceivedQty
WHERE   dd_CustomerPOStatus IN ('Closed','Received');

UPDATE fact_shipmentindelivery_temp t
SET ct_ShipmentQty = ct_POInboundDeliveryQty
WHERE   dd_CustomerPOStatus IN ('In-Transit');


/* Andra 16 May: Update of the dd_ShipmentStatus*/
DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_shipmentstatus;
CREATE TABLE tmp_facT_shipmentindelivery_shipmentstatus AS 
SELECT dd_DocumentNo,
       dd_DocumentItemNo, 
	   dd_ScheduleNo,
	   dd_ShipmentNo,
       dd_InboundDeliveryNo
	   ,sum(ct_POInboundDeliveryQty) as ct_POInboundDeliveryQty, sum(ct_DelivReceivedQty) AS ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,
       dd_DocumentItemNo, 
	   dd_ScheduleNo,
	   dd_ShipmentNo,
       dd_InboundDeliveryNo;

UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1
SET dd_ShipmentStatus = 'In Transit'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo 
  AND pm.afsdeliverycomplete <> 'X'
  AND p.dd_DeletionIndicator = 'Not Set'
  AND t1.ct_DelivReceivedQty = 0;

UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1
SET dd_ShipmentStatus = 'Received'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo 
  AND pm.afsdeliverycomplete <> 'X'
  AND p.dd_DeletionIndicator = 'Not Set'
  AND t1.ct_DelivReceivedQty > 0;

UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_shipmentindelivery_shipmentstatus t1
SET dd_ShipmentStatus = 'Closed'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND t.dd_ShipmentNo = t1.dd_ShipmentNo
  AND t.dd_InboundDeliveryNo = t1.dd_InboundDeliveryNo 
  AND pm.afsdeliverycomplete = 'X'
  AND t1.ct_POInboundDeliveryQty > 0;
  
DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_shipmentstatus;

DROP TABLE IF EXISTS tmp_facT_sid_shipmentstatus_scheduleLevel;
CREATE TABLE tmp_facT_sid_shipmentstatus_scheduleLevel AS 
SELECT dd_DocumentNo,
       dd_DocumentItemNo, 
	   dd_ScheduleNo
	   ,sum(ct_POInboundDeliveryQty) as ct_POInboundDeliveryQty, sum(ct_DelivReceivedQty) AS ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,
       dd_DocumentItemNo, 
	   dd_ScheduleNo;
  
UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_sid_shipmentstatus_scheduleLevel t1
SET dd_ShipmentStatus = 'Cancelled'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND pm.afsdeliverycomplete = 'X'
  AND t1.ct_POInboundDeliveryQty = 0;
  
UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm,
    tmp_facT_sid_shipmentstatus_scheduleLevel t1
SET dd_ShipmentStatus = 'Deleted'
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND t.dd_DocumentNo = t1.dd_DocumentNo
  AND t.dd_documentItemNo = t1.dd_DocumentItemNo
  AND t.dd_ScheduleNo = t1.dd_ScheduleNo
  AND p.dd_DeletionIndicator = 'L';

DROP TABLE IF EXISTS tmp_facT_sid_shipmentstatus_scheduleLevel; 

/* Andra 16 May: Update of the dd_ShipmentStatus*/


/* Andra 16 May: Update of the Qtys for DirectShip*/

UPDATE facT_shipmentindelivery_temp t
SET t.ct_InTransitQty = (t.ct_ReceivedQty - t.ct_DelivReceivedQty); 
  
UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm
SET t.ct_ClosedQty = t.ct_DelivReceivedQty
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND pm.afsdeliverycomplete = 'X'; 
  
UPDATE facT_shipmentindelivery_temp t
    FROM fact_purchase p,
    dim_purchasemisc pm
SET t.ct_CancelledQty = p.ct_scheduleLineQty
WHERE p.dim_purchasemiscid = pm.dim_purchasemiscid
  AND t.dd_DocumentNo = p.dd_DocumentNo
  AND t.dd_documentItemNo = p.dd_DocumentItemNo
  AND t.dd_ScheduleNo = p.dd_ScheduleNo
  AND p.ct_QtyReduced = 0
  AND p.ct_ReceivedQty = 0
  AND pm.afsdeliverycomplete = 'X'; 
  
DROP TABLE IF EXISTS tmp_fact_purchase_OpenQty_Item; 
CREATE TABLE tmp_fact_purchase_OpenQty_Item AS 
SELECT dd_DocumentNo,dd_DocumentItemNo,sum(ct_scheduleLineQty) as ct_scheduleLineQty
from fact_purchase
group by dd_DocumentNo,dd_DocumentItemNo;

DROP TABLE IF EXISTS tmp_fact_sid_OpenQty_Item; 
CREATE TABLE tmp_fact_sid_OpenQty_Item AS 
SELECT dd_DocumentNo,dd_DocumentItemNo,sum(ct_InTransitQty) as ct_InTransitQty,sum(ct_DelivReceivedQty) as ct_DelivReceivedQty
from facT_shipmentindelivery_temp
group by dd_DocumentNo,dd_DocumentItemNo;

UPDATE facT_shipmentindelivery_temp t
    FROM tmp_fact_purchase_OpenQty_Item tp,
         tmp_fact_sid_OpenQty_Item tsid
SET t.ct_OpenQty = ifnull((tp.ct_scheduleLineQty - (tsid.ct_InTransitQty - tsid.ct_DelivReceivedQty)),0)
WHERE t.dd_DocumentNo = tp.dd_DocumentNo
  AND t.dd_documentItemNo = tp.dd_DocumentItemNo
  AND t.dd_DocumentNo = tsid.dd_DocumentNo
  AND t.dd_documentItemNo = tsid.dd_DocumentItemNo; 
  
DROP TABLE IF EXISTS tmp_fact_purchase_OpenQty_Item;
DROP TABLE IF EXISTS tmp_fact_sid_OpenQty_Item;
  
DROP TABLE IF EXISTS tmp_tsegeUpdateEventDates;

CREATE TABLE tmp_tsegeUpdateEventDates AS 
SELECT DISTINCT TSEGE_HEAD_HDL, TSEGE_EVEN, TSEGE_EVEN_VERTY, 
(case when TSEGE_EVEN_TSTTO = 0 THEN '19000101' ELSE substr(tsege_even_tstto,1,8) END) AS TSEGE_EVEN_TSTTO
FROM TSEGE;

 UPDATE fact_shipmentindelivery_temp st
  FROM vttk_vttp v, 
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c
   SET st.Dim_DateId3plYard = dt.dim_Dateid
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'Z3PL'
   AND ansidate(t.tsege_even_tstto) = dt.datevalue
   AND t.TSEGE_EVEN_VERTY = 1
   AND dt.companycode = c.companycode;
   
/* Andra 18 Jun - add Max functionon 3PL In Yard Date for each inbound delivery */

DROP TABLE IF EXISTS tmp_facT_shipmentindelivery_3pl;
CREATE TABLE tmp_facT_shipmentindelivery_3pl AS 
SELECT dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo,dd_InboundDeliveryNo, max(Dim_DateId3plYard) Dim_DateId3plYard
FROM fact_shipmentindelivery_temp
GROUP BY dd_DocumentNo,dd_DocumentItemNo,dd_ScheduleNo,dd_InboundDeliveryNo;

UPDATE fact_shipmentindelivery_temp sd
FROM tmp_facT_shipmentindelivery_3pl t
SET sd.Dim_DateId3plYard = t.Dim_DateId3plYard
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.Dim_DateId3plYard <> t.Dim_DateId3plYard;

/* End of changes Andra 18 Jun - add Max functionon 3PL In Yard Date for each inbound delivery */

 UPDATE fact_shipmentindelivery_temp st
  FROM vttk_vttp v, 
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c
   SET st.Dim_DateIdContainerAtDCDoor = dt.dim_Dateid
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'ZDOOR'
   AND t.TSEGE_EVEN_VERTY = 1
   AND ansidate(t.tsege_even_tstto) = dt.datevalue
   AND dt.companycode = c.companycode;

 UPDATE fact_shipmentindelivery_temp st
  FROM vttk_vttp v, 
       tmp_tsegeUpdateEventDates t,
       dim_date dt, dim_Company c
   SET st.Dim_DateIdContainerInYard = dt.dim_Dateid
 WHERE st.dd_ShipmentNo  = v.VTTK_TKNUM
   AND st.dd_ShipmentItemNo = v.VTTP_TPNUM
   AND st.dim_companyid = c.dim_Companyid
   AND v.VTTK_HANDLE = t.TSEGE_HEAD_HDL
   AND t.TSEGE_EVEN = 'ZYARD'
   AND t.TSEGE_EVEN_VERTY = 1
   AND ansidate(t.tsege_even_tstto) = dt.datevalue
   AND dt.companycode = c.companycode;

call vectorwise(combine 'fact_shipmentindelivery_temp');

DROP TABLE IF EXISTS tmp_SplitSalesNetValue;
CREATE TABLE tmp_SplitSalesNetValue AS
select dd_Documentno,dd_documentitemno,sd.dd_Inbounddeliveryno,sd.dd_Shipmentno,sd.dd_ShipmentItemNo,sd.dd_salesdocno,sd.dd_salesitemno,dd_Salesscheduleno,sum(sd.ct_InboundDeliveryQty ) as totalschedqty, SUM(so.amt_DicountAccrualNetPrice*so.ct_ConfirmedQty) as avgNetValue,SUM(so.amt_DicountAccrualNetPrice*so.ct_ConfirmedQty)/(case when sum(sd.ct_InboundDeliveryQty) = 0 then null else sum(sd.ct_InboundDeliveryQty) end) from fact_shipmentindelivery_temp sd,
fact_Salesorder so
where sd.dd_Salesdocno = so.dd_Salesdocno
and sd.dd_salesitemno = so.dd_salesitemno
and sd.dd_Salesscheduleno = so.dd_scheduleno
and sd.dd_Salesdocno <> 'Not Set'
group by dd_Documentno,dd_documentitemno,sd.dd_Inbounddeliveryno,sd.dd_Shipmentno,sd.dd_ShipmentItemNo,sd.dd_salesdocno,sd.dd_salesitemno,dd_Salesscheduleno;

DROP TABLE IF EXISTS tmp_ResultFromSplitSalesandShipmentInbound;
CREATE TABLE tmp_ResultFromSplitSalesandShipmentInbound AS
SELECT t.*,sd.dd_scheduleno,sd.ct_InboundDeliveryQty  *  avgNetValue / (CASE WHEN totalschedqty <> 0  THEN totalschedqty ELSE 1 END) AS avg_Value  FROM fact_shipmentindelivery_temp sd
,tmp_SplitSalesNetValue t
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
AND sd.dd_Shipmentno = t.dd_Shipmentno
AND sd.dd_ShipmentItemNo = t.dd_ShipmentItemNo
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.dd_salesdocno = t.dd_salesdocno
AND sd.dd_salesitemno = t.dd_salesitemno
AND sd.dd_Salesscheduleno = t.dd_Salesscheduleno
AND  sd.dd_salesdocno <> 'NotSet'; 

UPDATE fact_shipmentindelivery_temp sd from  tmp_ResultFromSplitSalesandShipmentInbound t
SET sd.amt_so_afsnetvalue = t.avg_Value
WHERE sd.dd_Documentno = t.dd_Documentno
AND sd.dd_documentitemno = t.dd_documentitemno
and sd.dd_scheduleno = t.dd_Scheduleno
AND sd.dd_Shipmentno = t.dd_Shipmentno
AND sd.dd_ShipmentItemNo = t.dd_ShipmentItemNo
AND sd.dd_Inbounddeliveryno = t.dd_Inbounddeliveryno
AND sd.dd_salesdocno = t.dd_salesdocno
AND sd.dd_salesitemno = t.dd_salesitemno
AND sd.dd_Salesscheduleno = t.dd_Salesscheduleno
AND  sd.dd_salesdocno <> 'Not Set';

DROP TABLE IF EXISTS tmp_ResultFromSplitSalesandShipmentInbound;
DROP TABLE IF EXISTS tmp_SplitSalesNetValue;

call vectorwise(combine 'fact_shipmentindelivery_temp');
   
/* Setting the nulls to default values */		

UPDATE fact_shipmentindelivery_temp SET Dim_BillingBlockid = 1 WHERE Dim_BillingBlockid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Companyid = 1 WHERE Dim_Companyid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ControllingAreaid = 1 WHERE Dim_ControllingAreaid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CostCenterid = 1 WHERE Dim_CostCenterid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerGroup1id = 1 WHERE Dim_CustomerGroup1id IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerGroup2id = 1 WHERE Dim_CustomerGroup2id IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomerID = 1 WHERE Dim_CustomerID IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomeridShipTo = 1 WHERE Dim_CustomeridShipTo IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId = 1 WHERE Dim_CustomPartnerFunctionId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId1 = 1 WHERE Dim_CustomPartnerFunctionId1 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomPartnerFunctionId2 = 1 WHERE Dim_CustomPartnerFunctionId2 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomVendorPartnerFunctionId = 1 WHERE Dim_CustomVendorPartnerFunctionId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CustomVendorPartnerFunctionId1 = 1 WHERE Dim_CustomVendorPartnerFunctionId1 IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateId3plYard = 1 WHERE Dim_DateId3plYard IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidActualGI = 1 WHERE Dim_DateidActualGI IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidDlvrDocCreated = 1 WHERE Dim_DateidDlvrDocCreated IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdAirorSeaPlanningEstETA = 1 WHERE Dim_DateIdAirorSeaPlanningEstETA IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidASNExpectedOutput = 1 WHERE Dim_DateidASNExpectedOutput IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidASNOutput = 1 WHERE Dim_DateidASNOutput IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdContainerAtDCDoor = 1 WHERE Dim_DateIdContainerAtDCDoor IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdContainerInYard = 1 WHERE Dim_DateIdContainerInYard IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdDepartureFromOrigin = 1 WHERE Dim_DateIdDepartureFromOrigin IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdETAFinalDest = 1 WHERE Dim_DateIdETAFinalDest IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidFirstDate = 1 WHERE Dim_DateidFirstDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdGR = 1 WHERE Dim_DateIdGR IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdPOBuy = 1 WHERE Dim_DateIdPOBuy IS NULL;

UPDATE fact_shipmentindelivery_temp SET Dim_DateIdPOETA = 1 WHERE Dim_DateIdPOETA IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdReqDeliveryDate = 1 WHERE Dim_DateIdReqDeliveryDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidShipDlvrFill = 1 WHERE Dim_DateidShipDlvrFill IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdShipmentCreated = 1 WHERE Dim_DateIdShipmentCreated IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidShipmentDelivery = 1 WHERE Dim_DateidShipmentDelivery IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdShipmentEnd = 1 WHERE Dim_DateIdShipmentEnd IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateIdSOCancelDate = 1 WHERE Dim_DateIdSOCancelDate IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidValidFrom = 1 WHERE Dim_DateidValidFrom IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DateidValidTo = 1 WHERE Dim_DateidValidTo IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_DocumentCategoryid = 1 WHERE Dim_DocumentCategoryid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_MeansofTransTypeId = 1 WHERE Dim_MeansofTransTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Partid = 1 WHERE Dim_Partid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_Plantid = 1 WHERE Dim_Plantid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_POAFSSeasonId = 1 WHERE Dim_POAFSSeasonId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PODocumentTypeId = 1 WHERE Dim_PODocumentTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PORouteId = 1 WHERE Dim_PORouteId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ProcessingStatusId = 1 WHERE Dim_ProcessingStatusId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ProfitCenterId = 1 WHERE Dim_ProfitCenterId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_PurchasegroupId = 1 WHERE Dim_PurchasegroupId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_routeId = 1 WHERE Dim_routeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesDivisionid = 1 WHERE Dim_SalesDivisionid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesDocumentTypeid = 1 WHERE Dim_SalesDocumentTypeid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesGroupid = 1 WHERE Dim_SalesGroupid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesMiscId = 1 WHERE Dim_SalesMiscId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderHeaderStatusid = 1 WHERE Dim_SalesOrderHeaderStatusid IS NULL;
UPDATE fact_shipmentindelivery_temp SET dim_salesorderitemcategoryid = 1 WHERE dim_salesorderitemcategoryid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderItemStatusid = 1 WHERE Dim_SalesOrderItemStatusid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrderRejectReasonid = 1 WHERE Dim_SalesOrderRejectReasonid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_SalesOrgid = 1 WHERE Dim_SalesOrgid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ScheduleLineCategoryId = 1 WHERE Dim_ScheduleLineCategoryId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_ShipmentTypeId = 1 WHERE Dim_ShipmentTypeId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_TransactionGroupid = 1 WHERE Dim_TransactionGroupid IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_TranspPlanningPointId = 1 WHERE Dim_TranspPlanningPointId IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_VendorId = 1 WHERE Dim_VendorId IS NULL;
update fact_shipmentindelivery_temp set Dim_AFSTranspConditionid = 1 where Dim_AFSTranspConditionid is null; /* Marius 28 nov 2014 */
update fact_shipmentindelivery_temp SET dim_dateidactualpo = 1 where dim_dateidactualpo = 0;
update fact_shipmentindelivery_temp set dim_dateidAFSExFactEst = 1 where dim_dateidAFSExFactEst is null;  /* Marius 03 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_dateidMatAvailability  = 1 WHERE dim_dateidMatAvailability IS NULL; /*Marius 15 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_dateidTransfer  = 1 WHERE dim_dateidTransfer IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET Dim_DeliveryRouteId  = 1 WHERE Dim_DeliveryRouteId IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_shippingconditionid  = 1 WHERE dim_shippingconditionid IS NULL; /*Marius 17 dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_tranportbyshippingtypeid = 1 WHERE dim_tranportbyshippingtypeid IS NULL;

UPDATE fact_shipmentindelivery_temp SET dd_3PLFlag = 'Not Set' WHERE dd_3PLFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_BillofladingNo = 'Not Set' WHERE dd_BillofladingNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_BusinessCustomerPONo = 'Not Set' WHERE dd_BusinessCustomerPONo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CarrierActual  = 'Not Set' WHERE dd_CarrierActual IS NULL;
UPDATE fact_shipmentindelivery_temp SET Dim_CarrierBillingId  = 1 WHERE Dim_CarrierBillingId IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CartonLevelDtlFlag = 'Not Set' WHERE dd_CartonLevelDtlFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ContainerSize = 'Not Set' WHERE dd_ContainerSize IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CurrentShipmentFlag = 'Not Set' WHERE dd_CurrentShipmentFlag IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CustomerPONo = 'Not Set' WHERE dd_CustomerPONo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_CustomerPOStatus = 'Not Set' WHERE dd_CustomerPOStatus IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DocumentItemNo = 0 WHERE dd_DocumentItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DocumentNo  = 'Not Set' WHERE dd_DocumentNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DomesticContainerID  = 'Not Set' WHERE dd_DomesticContainerID IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_InboundDeliveryItemNo  = 0 WHERE dd_InboundDeliveryItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_InboundDeliveryNo  = 'Not Set' WHERE dd_InboundDeliveryNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ItemRelForDelv = 'Not Set' WHERE dd_ItemRelForDelv IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_MessageType = 'Not Set' WHERE dd_MessageType IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_MovementType = 'Not Set' WHERE dd_MovementType IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_OriginContainerId = 'Not Set' WHERE dd_OriginContainerId IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_PackageCount = 0 WHERE dd_PackageCount IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ProcessingTime = '000000' WHERE dd_ProcessingTime IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesDocNo = 'Not Set' WHERE dd_SalesDocNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesItemNo = 0 WHERE dd_SalesItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_SalesScheduleNo = 0 WHERE dd_SalesScheduleNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ScheduleNo = 0 WHERE dd_ScheduleNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentItemNo = 0 WHERE dd_ShipmentItemNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentNo = 'Not Set' WHERE dd_ShipmentNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_Vessel = 'Not Set' WHERE dd_Vessel IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_VoyageNo = 'Not Set' WHERE dd_VoyageNo IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_ShipmentStatus = 'Not Set' WHERE dd_ShipmentStatus IS NULL;
UPDATE fact_shipmentindelivery_temp SET dd_DeliveryNumber = 'Not Set' WHERE dd_DeliveryNumber IS NULL;   /* Marius 17 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET dim_transportconditionid = 1 WHERE dim_transportconditionid IS NULL;

UPDATE fact_shipmentindelivery_temp SET ct_ConfirmedQty = 0 WHERE ct_ConfirmedQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_DelayDays = 0 WHERE ct_DelayDays IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_InboundDeliveryQty = 0 WHERE ct_InboundDeliveryQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_InboundDlvryCMtrVolume = 0 WHERE ct_InboundDlvryCMtrVolume IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_POScheduleQty = 0 WHERE ct_POScheduleQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_PriceUnit = 0 WHERE ct_PriceUnit IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_POItemReceivedQty = 0 WHERE ct_POItemReceivedQty IS NULL; 
UPDATE fact_shipmentindelivery_temp SET ct_ReceivedQty = 0 WHERE ct_ReceivedQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_ShipmentQty = 0 WHERE ct_ShipmentQty IS NULL;
UPDATE fact_shipmentindelivery_temp SET ct_DeliveryGrossWght = 0 WHERE ct_DeliveryGrossWght IS NULL;  /* Marius 17 Dec 2014 */
UPDATE fact_shipmentindelivery_temp SET ct_deliveryNetWght = 0 WHERE ct_deliveryNetWght IS NULL;  /* Marius 17 Dec 2014 */

DROP TABLE IF EXISTS tmp_tsegeUpdateEventDates;

call vectorwise (combine 'fact_shipmentindelivery_temp');

modify fact_shipmentindelivery to truncated;

call vectorwise(combine 'fact_shipmentindelivery+fact_shipmentindelivery_temp');

call vectorwise(combine 'fact_shipmentindelivery');

SELECT 'END OF PROC bi_populate_shipment_inbounddeliveries_fact',TIMESTAMP(LOCAL_TIMESTAMP);
