DROP TABLE IF EXISTS var_pToleranceDays;
CREATE TABLE var_pToleranceDays
(
pType  VARCHAR(10),
pToleranceDays INTEGER
);


INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('EARLY',0);
INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('LATE',0);

UPDATE var_pToleranceDays
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
WHERE property = 'purchasing.deliverytolerance.early'),
              0)
WHERE pType ='EARLY';
			  
UPDATE var_pToleranceDays			  
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
WHERE property = 'purchasing.deliverytolerance.late'),
              0)
WHERE pType ='LATE';
                    
                    
drop table if exists fact_mm_tmp_dlvrlink;
create table fact_mm_tmp_dlvrlink as 
SELECT f.Fact_materialmovementid,
		f.dd_MaterialDocNo,
		f.dd_MaterialDocItemNo,
		f.dd_MaterialDocYear,
		f.dd_DocumentNo,
		f.dd_DocumentItemNo,
		-1*f.ct_Quantity ct_Quantity,
		-1*f.ct_QtyEntryUOM ct_QtyEntryUOM,
		f.Dim_MovementTypeid,
		f.dim_DateIDPostingDate,
		f.dd_debitcreditid,
		dt.datevalue PostingDate,
		ROW_NUMBER() OVER (PARTITION BY f.dd_DocumentNo, f.dd_DocumentItemNo ORDER BY dt.datevalue, f.dd_MaterialDocNo, f.dd_MaterialDocItemNo) RowNum
FROM fact_materialmovement f 
		inner join dim_movementtype mt on f.Dim_MovementTypeid = mt.Dim_MovementTypeid
		inner join dim_date dt on f.dim_DateIDPostingDate = dt.dim_dateid
WHERE mt.MovementType in (641,643) 
        and exists (select 1 from fact_materialmovement f1 
                    where f.dd_DocumentNo = f1.dd_DocumentNo and f.dd_DocumentItemNo = f1.dd_DocumentItemNo
                        and f1.Dim_DateIdDelivery = 1)
        and exists (select 1 from fact_purchase fp 
                    where f.dd_DocumentNo = fp.dd_DocumentNo and f.dd_DocumentItemNo = fp.dd_DocumentItemNo);

    
drop table if exists fact_mm_tmp_dlvrlink_01;
create table fact_mm_tmp_dlvrlink_01 as 
SELECT a.Fact_materialmovementid,
		a.dd_MaterialDocNo,
		a.dd_MaterialDocItemNo,
		a.dd_MaterialDocYear,
		a.dd_DocumentNo,
		a.dd_DocumentItemNo,
		a.ct_Quantity,
		a.ct_QtyEntryUOM,
		a.Dim_MovementTypeid,
		a.dim_DateIDPostingDate,
		a.dd_debitcreditid,
		a.PostingDate,
		a.RowNum,
		SUM(b.ct_QtyEntryUOM) DlvrQtyCUMM
FROM fact_mm_tmp_dlvrlink a 
	 inner join fact_mm_tmp_dlvrlink b on a.dd_DocumentNo = b.dd_DocumentNo and a.dd_DocumentItemNo = b.dd_DocumentItemNo
WHERE a.RowNum >= b.RowNum
GROUP BY a.Fact_materialmovementid,
		a.dd_MaterialDocNo,
		a.dd_MaterialDocItemNo,
		a.dd_MaterialDocYear,
		a.dd_DocumentNo,
		a.dd_DocumentItemNo,
		a.ct_Quantity,
		a.ct_QtyEntryUOM,
		a.Dim_MovementTypeid,
		a.dim_DateIDPostingDate,
		a.dd_debitcreditid,
		a.PostingDate,
		a.RowNum;
		
		
drop table if exists fact_po_mm_tmp;
create table fact_po_mm_tmp as
select dd_DocumentNo,
		dd_DocumentItemNo,
		dd_ScheduleNo,
		Dim_DateidDelivery,
		Dim_DateidStatDelivery,
		ct_QtyIssued,
		dp1.DateValue DeliveryDate,
		ROW_NUMBER() OVER (PARTITION BY dd_DocumentNo, dd_DocumentItemNo ORDER BY dp1.DateValue, dd_ScheduleNo) RowNum
from fact_purchase fp inner join dim_date dp1 on dp1.Dim_Dateid = fp.Dim_DateidDelivery		
where exists (select 1 from fact_mm_tmp_dlvrlink t 
			  where t.dd_DocumentNo = fp.dd_DocumentNo and t.dd_DocumentItemNo = fp.dd_DocumentItemNo)
		and fp.ct_QtyIssued > 0;
			  		

drop table if exists fact_po_mm_tmp_01;
create table fact_po_mm_tmp_01 as
select a.dd_DocumentNo,
		a.dd_DocumentItemNo,
		a.dd_ScheduleNo,
		a.Dim_DateidDelivery,
		a.Dim_DateidStatDelivery,
		a.ct_QtyIssued,
		a.DeliveryDate,
		a.RowNum,
		SUM(b.ct_QtyIssued) QtyIssuedCUMM
from fact_po_mm_tmp a 
	 inner join fact_po_mm_tmp b on a.dd_DocumentNo = b.dd_DocumentNo and a.dd_DocumentItemNo = b.dd_DocumentItemNo
where a.RowNum >= b.RowNum
group by a.dd_DocumentNo,
		a.dd_DocumentItemNo,
		a.dd_ScheduleNo,
		a.Dim_DateidDelivery,
		a.Dim_DateidStatDelivery,
		a.ct_QtyIssued,
		a.DeliveryDate,
		a.RowNum;
		

drop table if exists fact_mm_tmp_dlvrlink;
create table fact_mm_tmp_dlvrlink as 
SELECT a.Fact_materialmovementid,
		b.dd_ScheduleNo,
		b.Dim_DateidDelivery,
		b.Dim_DateidStatDelivery,
		b.ct_QtyIssued
FROM fact_mm_tmp_dlvrlink_01 a 
	 inner join fact_po_mm_tmp_01 b on a.dd_DocumentNo = b.dd_DocumentNo and a.dd_DocumentItemNo = b.dd_DocumentItemNo
WHERE a.DlvrQtyCUMM <= b.QtyIssuedCUMM
	 and a.DlvrQtyCUMM > (b.QtyIssuedCUMM - b.ct_QtyIssued);
		
		
update fact_materialmovement f
from fact_mm_tmp_dlvrlink b
set f.Dim_DateIdDelivery = b.Dim_DateidDelivery
where f.Fact_materialmovementid = b.Fact_materialmovementid
AND f.Dim_DateIdDelivery <> b.Dim_DateidDelivery;

update fact_materialmovement f
from fact_mm_tmp_dlvrlink b
set f.Dim_DateIdStatDelivery = b.Dim_DateidStatDelivery
where f.Fact_materialmovementid = b.Fact_materialmovementid
      AND f.Dim_DateIdStatDelivery <> b.Dim_DateidStatDelivery;
      
update fact_materialmovement f
from fact_mm_tmp_dlvrlink b
set f.dd_DocumentScheduleNo = b.dd_ScheduleNo
where f.Fact_materialmovementid = b.Fact_materialmovementid
AND f.dd_DocumentScheduleNo <> b.dd_ScheduleNo;

update fact_materialmovement f
from fact_mm_tmp_dlvrlink b
set f.ct_OrderQuantity = b.ct_QtyIssued
where f.Fact_materialmovementid = b.Fact_materialmovementid
AND f.ct_OrderQuantity <> b.ct_QtyIssued;
		
		
drop table if exists fact_mm_tmp_dlvrlink;
drop table if exists fact_mm_tmp_dlvrlink_01;
drop table if exists fact_po_mm_tmp;
drop table if exists fact_po_mm_tmp_01;

       
update fact_materialmovement f 
	  FROM dim_date dp, dim_date ds, var_pToleranceDays E,var_pToleranceDays L
     set f.Dim_GRStatusid = case when dp.DateValue > (ANSIDATE(ds.DateValue) + (INTERVAL '1' DAY)*L.pToleranceDays ) then 3 
                                 when dp.DateValue < (ANSIDATE(ds.DateValue) - (INTERVAL '1' DAY)*E.pToleranceDays ) then 4 
                                 else 5 end
   where f.Dim_DateIdStatDelivery <> 1
     and dp.Dim_Dateid = f.dim_DateIDPostingDate
     and ds.Dim_Dateid = f.Dim_DateIdStatDelivery
     and E.pType = 'EARLY'
     and L.pType = 'LATE';
