/*   Script         : vw_bi_populate_salesorderlifeattributesforbilling */
/*   Author         : Andra */
/*   Created On     : 06 Aug 2015 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_salesorder_fillrate migration from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*   07 Aug 2015    Cristina      1.0            Added the statements from Shipments process script */


UPDATE 
fact_billing fb
SET fb.Dim_CustomerId = 1;

SELECT distinct fb.Dim_CustomerId 
FROM fact_billing fb;

DROP TABLE IF EXISTS tmp_sof_customer;
CREATE TABLE tmp_sof_customer
AS
SELECT fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo, 
       fso.Dim_CustomerID,fso.dim_salesorderitemstatusid, fso.Dim_DocumentCategoryid,
       fso.Dim_CostCenterid, fso.Dim_SalesDocumentTypeid, fso.Dim_DateidSchedDelivery,count(*)
FROM fact_salesorderlife fso
GROUP BY fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo, 
       fso.Dim_CustomerID,fso.dim_salesorderitemstatusid, fso.Dim_DocumentCategoryid,
       fso.Dim_CostCenterid, fso.Dim_SalesDocumentTypeid, fso.Dim_DateidSchedDelivery;

UPDATE fact_billing fb
FROM
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET fb.Dim_CustomerId = ifnull(fso.Dim_CustomerID,1)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
       AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
 AND fb.Dim_CustomerId <> fso.Dim_CustomerID; 

UPDATE fact_billing fb
FROM
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET  fb.Dim_SalesDocumentItemStatusId = fso.dim_salesorderitemstatusid
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
       AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
 AND  fb.Dim_SalesDocumentItemStatusId <>fso.dim_salesorderitemstatusid;

UPDATE fact_billing fb
FROM
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.Dim_so_CostCenterid = fso.Dim_CostCenterid
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
       AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
 AND fb.Dim_so_CostCenterid <> fso.Dim_CostCenterid;

UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.Dim_so_SalesDocumentTypeid = fso.Dim_SalesDocumentTypeid
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
       AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
AND  fb.Dim_so_SalesDocumentTypeid <> fso.Dim_SalesDocumentTypeid;


UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.Dim_so_DateidSchedDelivery = fso.Dim_DateidSchedDelivery
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
       AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
 AND fb.Dim_so_DateidSchedDelivery <> fso.Dim_DateidSchedDelivery;
 
DROP TABLE IF EXISTS tmp_sof_customer;

CREATE TABLE tmp_sof_customer
AS
SELECT fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo, 
       fso.Dim_DateidSalesOrderCreated,fso.Dim_SalesOrderRejectReasonid,fso.Dim_DocumentCategoryid, SUM(fso.ct_ConfirmedQty) ct_ConfirmedQty, AVG(fso.amt_UnitPrice) amt_UnitPrice, AVG(fso.ct_PriceUnit) ct_PriceUnit,
	   SUM(fso.ct_DeliveredQty) ct_DeliveredQty, SUM(fso.amt_ScheduleTotal) amt_ScheduleTotal,
	   SUM(fso.ct_ScheduleQtySalesUnit) ct_ScheduleQtySalesUnit
FROM fact_salesorderlife fso
GROUP BY fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo, 
       fso.Dim_DateidSalesOrderCreated,fso.Dim_SalesOrderRejectReasonid, fso.Dim_DocumentCategoryid;

UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET    fb.Dim_so_SalesOrderCreatedId = fso.Dim_DateidSalesOrderCreated
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
 AND fb.Dim_so_SalesOrderCreatedId <> fso.Dim_DateidSalesOrderCreated;

UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.ct_so_ConfirmedQty = fso.ct_ConfirmedQty
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
	   AND fb.ct_so_ConfirmedQty <> fso.ct_ConfirmedQty;

UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET  fb.amt_so_UnitPrice = fso.amt_UnitPrice
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
       AND fb.amt_so_UnitPrice <> fso.amt_UnitPrice;
	   
	   
UPDATE fact_billing fb
FROM
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET fb.amt_so_confirmed =
          ((fso.ct_ConfirmedQty* fso.amt_UnitPrice)
          / (case when fso.ct_PriceUnit=0 then null else fso.ct_PriceUnit end))            
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR;


UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET  fb.ct_so_DeliveredQty = fso.ct_DeliveredQty
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
       AND fb.ct_so_DeliveredQty <> fso.ct_DeliveredQty;


UPDATE fact_billing fb
from
       fact_salesorderlife fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.amt_so_Returned =0
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
	   AND fb.amt_so_Returned <> 0;
	   
UPDATE fact_billing fb
from
       fact_salesorderlife fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.ct_so_ReturnedQty =0
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
	   AND fb.ct_so_ReturnedQty <> 0;
	   

UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.amt_so_Returned = fso.amt_ScheduleTotal
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
       AND dc.DocumentCategory IN ('H', 'K') 
	   AND fso.Dim_SalesOrderRejectReasonid <> 1
	   AND fb.amt_so_Returned <> fso.amt_ScheduleTotal;
	   
UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.ct_so_ReturnedQty = fso.ct_ScheduleQtySalesUnit
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
	   AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
       AND dc.DocumentCategory IN ('H', 'K') 
	   AND fso.Dim_SalesOrderRejectReasonid <> 1
	   AND fb.ct_so_ReturnedQty <> fso.ct_ScheduleQtySalesUnit;

	   
DROP TABLE IF EXISTS tmp_sof_customer;

DROP TABLE IF EXISTS tmp_SalesBillingQty;
CREATE TABLE tmp_SalesBillingQty AS
SELECT fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo,
SUM(fso.ct_ScheduleQtySalesUnit) as TotalOrderQty,
       SUM(fso.ct_DeliveredQty) as TotalDlvrQty 
FROM
       fact_salesorderlife fso
group by fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo;


UPDATE fact_billing fb
from
       tmp_SalesBillingQty t,
       vbak_vbap_vbep vkp
SET    fb.ct_so_OpenOrderQty =
                    (t.TotalOrderQty - t.TotalDlvrQty)
 WHERE     fb.dd_salesdocno = t.dd_SalesDocNo
       AND fb.dd_salesitemno = t.dd_SalesItemNo
	   AND fb.dd_AfsScheduleNo = t.dd_ScheduleNo
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR ;

DROP TABLE IF EXISTS tmp_SalesBillingQty;

DROP TABLE IF EXISTS tmp_sof_customer;
CREATE TABLE tmp_sof_customer
AS
SELECT fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo, 
       fso.Dim_DocumentCategoryid, SUM(fso.amt_ScheduleTotal) amt_ScheduleTotal,
	   SUM(fso.ct_ScheduleQtySalesUnit) ct_ScheduleQtySalesUnit
FROM fact_salesorderlife fso
GROUP BY fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo, 
       fso.Dim_DocumentCategoryid;

UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.amt_so_ScheduleTotal = fso.amt_ScheduleTotal 
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
	   AND fb.amt_so_ScheduleTotal <> fso.amt_ScheduleTotal;


UPDATE fact_billing fb
from
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET   fb.ct_so_ScheduleQtySalesUnit = fso.ct_ScheduleQtySalesUnit 
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR 
	   AND fb.ct_so_ScheduleQtySalesUnit <> fso.ct_ScheduleQtySalesUnit;


UPDATE fact_billing fb
FROM
       tmp_sof_customer fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET fb.amt_revenue =
          (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
              WHEN '5' THEN 0
              WHEN '6' THEN 0
              WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
              WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
              ELSE fb.amt_NetValueItem
           END)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR;


DROP TABLE IF EXISTS fact_salesorder_updexp;
CREATE TABLE fact_salesorder_updexp
AS
SELECT fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo, fso.dim_documentcategoryid,
          (SUM(fso.ct_ScheduleQtySalesUnit) - SUM(fso.ct_DeliveredQty)) exp1,
		  AVG(fso.amt_UnitPrice) amt_UnitPrice, AVG(fso.ct_PriceUnit) ct_PriceUnit
FROM fact_salesorderlife fso
GROUP BY fso.dd_SalesDocNo, fso.dd_SalesItemNo, fso.dd_ScheduleNo,fso.dim_documentcategoryid;

UPDATE fact_billing fb
from
       fact_salesorder_updexp fso,
       Dim_documentcategory dc,
       vbak_vbap_vbep vkp
SET  fb.amt_so_openorder =
      ((fso.exp1 * fso.amt_UnitPrice)
      /(case when fso.ct_PriceUnit=0 then null else fso.ct_PriceUnit end))
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_AfsScheduleNo = fso.dd_ScheduleNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_salesdocno = vkp.VBAK_VBELN
       AND fb.dd_salesitemno = vkp.VBAP_POSNR
	   AND fb.dd_AfsScheduleNo = vkp.VBEP_ETENR;

DROP TABLE IF EXISTS fact_salesorder_updexp;

DROP TABLE IF EXISTS tmp_sod_billing_intermediate;
CREATE TABLE tmp_sod_billing_intermediate
AS
SELECT f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo,sum(f_sod.amt_Cost) amt_sd_Cost,
SUM(f_sod.amt_UnitPrice * f_sod.ct_QtyDelivered) amt_sd_Shipped,
SUM(f_sod.ct_QtyDelivered) ct_sd_QtyDelivered
FROM fact_salesorderlife f_sod
GROUP BY f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo;



UPDATE fact_billing fb 
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp
    SET fb.amt_sd_Cost = sod.amt_sd_Cost
	,fb.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
	AND ifnull(fb.amt_sd_Cost,-1) <> ifnull(sod.amt_sd_Cost,0);


UPDATE fact_billing fb
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp
    SET fb.amt_sd_Shipped = sod.amt_sd_Shipped
	,fb.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
        AND ifnull(fb.amt_sd_Shipped,-1) <> ifnull(sod.amt_sd_Shipped,0);

UPDATE fact_billing fb
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp
    SET fb.ct_sd_QtyDelivered = sod.ct_sd_QtyDelivered
	,fb.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
        AND ifnull(fb.ct_sd_QtyDelivered,-1) <> ifnull(sod.ct_sd_QtyDelivered,0);



UPDATE fact_billing fb FROM fact_salesorderlife sod, LIKP_LIPS lkp
        SET fb.Dim_sd_DateidActualGoodsIssue = sod.Dim_DateidActualGoodsIssue
	,fb.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
	AND ifnull(fb.Dim_sd_DateidActualGoodsIssue,-1) <> ifnull(sod.Dim_DateidActualGoodsIssue,1);


DROP TABLE IF EXISTS tmp_sod_billing_intermediate;
call vectorwise(combine 'fact_billing');
