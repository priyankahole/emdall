
/* Run in this order : vw_bi_populate_shortage_fact.part1.sql, vw_getstdprice_custom.fact_shortage.sql,
	vw_getstdprice_std.part1.sql, vw_funct_fiscal_year.custom.getStdPrice.sql,vw_funct_fiscal_year.standard.sql, vw_getstdprice_std.part2.sql,
		vw_bi_populate_shortage_fact.part2.sql */

UPDATE fact_shortage_temp fs from
       dim_plant pl,
       resb r,
       dim_date dt
   SET fs.amt_StdPrice = ifnull((SELECT StandardPrice from tmp_getStdPrice z 
						where z.pCompanyCode = pl.CompanyCode
						AND z.pPlant = pl.PlantCode 
						AND z.pMaterialNo = r.RESB_MATNR
						AND z.pFiYear = dt.FinancialYear
						AND z.pPeriod = dt.FinancialMonthNumber
						AND z.fact_script_name = 'bi_populate_shortage_fact'
						AND z.vUMREZ = RESB_UMREZ
						AND z.vUMREN = RESB_UMREN
						AND z.PONumber IS NULL
						AND z.pUnitPrice =   ifnull((CASE WHEN ifnull(RESB_PEINH, 0) = 0 THEN 1 ELSE RESB_PEINH END),0)),0)
 WHERE     fs.dd_ReservationNo <> 0 
       AND fs.dd_ReservationItemNo <> 0 
       AND fs.dd_ReservationNo = r.RESB_RSNUM
       AND fs.dd_ReservationItemNo = r.RESB_RSPOS
       AND dt.DateValue = (SELECT min(rb.RESB_BDTER) FROM RESB rb
                           WHERE fs.dd_ReservationNo <> 0 
                             AND fs.dd_ReservationItemNo <> 0 
                             AND rb.RESB_RSNUM = fs.dd_ReservationNo AND rb.RESB_RSPOS = fs.dd_ReservationItemNo)
       AND dt.CompanyCode = pl.CompanyCode
       AND pl.PlantCode = r.RESB_WERKS
       AND pl.RowIsCurrent = 1;		

UPDATE fact_shortage_temp fs from
       fact_purchase fp
   SET fs.amt_StdPrice = fp.amt_StdUnitPrice
 WHERE     fs.dd_DocumentNo = fp.dd_DocumentNo
       AND fs.dd_DocumentItemNo = fp.dd_DocumentItemNo
       AND fs.dd_ScheduleNo = fp.dd_ScheduleNo
       AND fs.dim_ComponentId = fp.Dim_Partid
AND ifnull(fs.amt_StdPrice,-1) <> ifnull(fp.amt_StdUnitPrice,-2) ;

	   
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1_pre;
CREATE TABLE TMP_FS_fact_inventoryaging1_pre
AS	
SELECT 	 inv.dim_PartId,inv.dim_Plantid,inv.dim_stockcategoryid,
inv.ct_StockQty+ inv.ct_StockInQInsp+ inv.ct_BlockedStock+ inv.ct_StockInTransfer as upd_dd_OnHandQty,
inv.ct_StockQty as upd_dd_UnrestrictedStockQty
FROM fact_inventoryaging inv
WHERE EXISTS ( SELECT 1 from fact_shortage_temp s
WHERE s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2);
	
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
CREATE TABLE TMP_FS_fact_inventoryaging1
AS
SELECT inv.dim_PartId,inv.dim_Plantid,inv.dim_stockcategoryid,
SUM(upd_dd_OnHandQty) as sum_upd_dd_OnHandQty,
SUM(inv.upd_dd_UnrestrictedStockQty) sum_upd_dd_UnrestrictedStockQty
FROM TMP_FS_fact_inventoryaging1_pre inv
GROUP BY  inv.dim_PartId,inv.dim_Plantid,inv.dim_stockcategoryid;

UPDATE fact_shortage_temp s
   SET s.dd_OnHandQty = 0
WHERE IFNULL(dd_OnHandQty,-1) <> IFNULL(dd_OnHandQty,-2)   ;

UPDATE fact_shortage_temp s
   SET s.dd_UnrestrictedStockQty = 0
WHERE IFNULL(dd_UnrestrictedStockQty,-1) <> IFNULL(dd_UnrestrictedStockQty,-2)   ;
   
UPDATE fact_shortage_temp s
FROM TMP_FS_fact_inventoryaging1 inv
SET s.dd_OnHandQty = sum_upd_dd_OnHandQty
WHERE     s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2
AND s.dd_OnHandQty  <> sum_upd_dd_OnHandQty;


UPDATE fact_shortage_temp s
FROM TMP_FS_fact_inventoryaging1 inv
SET s.dd_UnrestrictedStockQty = sum_upd_dd_UnrestrictedStockQty
WHERE     s.Dim_ComponentId = inv.dim_Partid
AND s.Dim_ComponentPlantId = inv.dim_Plantid
AND inv.dim_stockcategoryid = 2
AND s.dd_UnrestrictedStockQty  <> sum_upd_dd_UnrestrictedStockQty;
					 
	
DROP TABLE IF EXISTS TMP_FS_MARD;
CREATE TABLE TMP_FS_MARD
AS	
SELECT m.MARD_MATNR,m.MARD_WERKS,sum(ifnull(m.MARD_LABST, 0)) as upd_dd_StockQty
FROM  MARD m
WHERE EXISTS ( select 1 from 	fact_Shortage s, dim_part pt, dim_plant pl
WHERE 	m.MARD_MATNR = pt.PartNumber
AND m.MARD_WERKS = pt.Plant				 
AND s.Dim_ComponentId = pt.Dim_Partid
AND pt.RowIsCurrent = 1
AND s.Dim_ComponentPlantid = pl.Dim_Plantid
AND pl.RowIsCurrent = 1
AND pt.Plant = pl.PlantCode )
GROUP BY m.MARD_MATNR,m.MARD_WERKS;
						 
UPDATE fact_shortage_temp s
   SET s.dd_StockQty = 0
WHERE IFNULL(dd_StockQty,-1) <> IFNULL(dd_StockQty,-2)   ;
             
UPDATE fact_shortage_temp s 
from dim_part pt, dim_plant pl,TMP_FS_MARD m
SET s.dd_StockQty =           upd_dd_StockQty
WHERE     s.Dim_ComponentId = pt.Dim_Partid
AND pt.RowIsCurrent = 1
AND s.Dim_ComponentPlantid = pl.Dim_Plantid
AND pl.RowIsCurrent = 1
AND pt.Plant = pl.PlantCode
AND m.MARD_MATNR = pt.PartNumber AND m.MARD_WERKS = pt.Plant;


DELETE FROM fact_shortage_temp
 WHERE dd_ReservationNo <> 0 
 AND dd_ReservationItemNo <> 0 
 AND EXISTS
          (SELECT 1
             FROM RESB r
            WHERE     r.RESB_RSNUM = dd_ReservationNo
                  AND r.RESB_RSPOS = dd_ReservationItemNo
                  AND r.RESB_XLOEK = 'X'); 
				  
drop table if exists fact_shortage;
rename table fact_shortage_temp to fact_shortage;				  

DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1_pre;
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
DROP TABLE IF EXISTS TMP_FS_MARD;
drop table if exists fact_shortage_temp;
DROP TABLE IF EXISTS fact_shortage_temp_1;
DROP TABLE IF EXISTS TMP_FS_fact_inventoryaging1;
DROP TABLE IF EXISTS TMP_FS_MARD;

