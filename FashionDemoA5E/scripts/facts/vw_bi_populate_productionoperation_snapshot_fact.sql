/**********************************************************************************/
/*  5 Mar 2015   1.0	      Lokesh    New script for new Merck HH Subj Area - Prod Operation Snapshot */
/**********************************************************************************/

/* Store current date in beginning - to handle the scenario of date changing during script run */
DROP TABLE IF EXISTS tmp_current_date_fpopsnapshot;
CREATE TABLE tmp_current_date_fpopsnapshot
AS
SELECT current_date currdate;

/* If today's snapshot is being rerun, delete existing data */
DELETE FROM fact_productionoperation_snapshot WHERE dd_snapshotdate = current_date;


/* Part 1 - Update  existing rows - only non-orig columns */
/* For all existing ord-items in snapshot table, update the values(of non-orig cols) to current values */
/* Note that orig columns won't change */

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET dim_dateidscheduledfinish = fpoforpop.dim_dateidscheduledfinish
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.dim_dateidscheduledfinish <> fpoforpop.dim_dateidscheduledfinish;


UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET ct_totalorderqty = fpoforpop.ct_totalorderqty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.ct_totalorderqty <> fpoforpop.ct_totalorderqty;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET dd_cancelledorder = fpoforpop.dd_cancelledorder
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.dd_cancelledorder <> fpoforpop.dd_cancelledorder;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET ct_underdeliverytol_merck = fpoforpop.ct_underdeliverytol_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.ct_underdeliverytol_merck <> fpoforpop.ct_underdeliverytol_merck;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET ct_overdeliverytol_merck = fpoforpop.ct_overdeliverytol_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.ct_overdeliverytol_merck <> fpoforpop.ct_overdeliverytol_merck;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET dd_unimitedoverdelivery_merck = fpoforpop.dd_unimitedoverdelivery_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.dd_unimitedoverdelivery_merck <> fpoforpop.dd_unimitedoverdelivery_merck;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET ct_GRQty = fpoforpop.ct_GRQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.ct_GRQty <> fpoforpop.ct_GRQty;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET ct_OrderItemQty = fpoforpop.ct_OrderItemQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.ct_OrderItemQty <> fpoforpop.ct_OrderItemQty;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET s.dim_dateidactualitemfinish = fpoforpop.dim_dateidactualitemfinish
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.dim_dateidactualitemfinish <> fpoforpop.dim_dateidactualitemfinish;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET s.dim_dateidactualheaderfinishdate_merck = fpoforpop.dim_dateidactualheaderfinishdate_merck
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.dim_dateidactualheaderfinishdate_merck <> fpoforpop.dim_dateidactualheaderfinishdate_merck;

UPDATE fact_productionoperation_snapshot s
FROM fact_productionorder fpoforpop
SET s.ct_GRQty = fpoforpop.ct_GRQty
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = fpoforpop.dd_OrderNumber
AND s.dd_orderitemno = fpoforpop.dd_orderitemno
AND s.ct_GRQty <> fpoforpop.ct_GRQty;

/* Part 2 - Insert new rows */
/* For a given ord-item with scheduled finish date in next week, there are 2 scenarios possible :
A. The given ord-item is already present in fact_productionoperation_snapshot. So, update the original fields from this table ( latest row for that ord-item )
B. The doc-item is new. So, in this case the original values will be the same as current values */

/* First capture the latest row from snapshot subj area for each order/item */
DROP TABLE IF EXISTS tmp_fact_productionoperation_snapshot_maxdate_1;
CREATE TABLE tmp_fact_productionoperation_snapshot_maxdate_1
AS
SELECT dd_OrderNumber,dd_orderitemno, max(dd_snapshotdate) max_dd_snapshotdate
FROM fact_productionoperation_snapshot
GROUP BY dd_OrderNumber,dd_orderitemno;

DROP TABLE IF EXISTS tmp_fact_productionoperation_snapshot_maxdate;
CREATE TABLE tmp_fact_productionoperation_snapshot_maxdate
AS
SELECT f.*
FROM fact_productionoperation_snapshot f, tmp_fact_productionoperation_snapshot_maxdate_1 m
WHERE f.dd_OrderNumber = m.dd_OrderNumber AND f.dd_orderitemno = m.dd_orderitemno AND f.dd_snapshotdate = m.max_dd_snapshotdate;


/* Insert new order-item */
/* Only open production orders with due dates in the next 7 days */
DROP TABLE IF EXISTS tmp_fact_productionoperation_snapshot_ins;
CREATE TABLE tmp_fact_productionoperation_snapshot_ins
AS
SELECT DISTINCT fpoforpop.dd_OrderNumber,fpoforpop.dd_orderitemno
FROM dim_date d, tmp_current_date_fpopsnapshot, fact_productionorder fpoforpop
/*WHERE fpoforpop.dim_dateidscheduledfinish = d.dim_dateid*/
WHERE fpoforpop.dim_dateidscheduledfinishheader = d.dim_dateid
AND d.datevalue > currdate
AND d.datevalue <= currdate + INTERVAL '7' DAY;
/*AND dim_dateidconfirmedorderfinish = 1      	/* Ensure that its not closed */

/* ALL production orders with due dates in the past 7 days (cancelled, closed or open) */
/*INSERT INTO tmp_fact_productionoperation_snapshot_ins
SELECT DISTINCT dd_RoutingOperationNo, dd_GeneralOrderCounter
FROM fact_productionoperation pr, dim_date d, tmp_current_date_fpopsnapshot
WHERE pr.dim_dateidscheduledfinish = d.dim_dateid
AND d.datevalue > currdate - INTERVAL '7' DAY
AND  d.datevalue <= currdate*/

DROP TABLE IF EXISTS tmp_fact_productionoperation_snapshot_del;
CREATE TABLE tmp_fact_productionoperation_snapshot_del
AS
SELECT * FROM tmp_fact_productionoperation_snapshot_ins WHERE 1=2;
INSERT INTO tmp_fact_productionoperation_snapshot_del
SELECT DISTINCT dd_OrderNumber,dd_orderitemno
FROM fact_productionoperation_snapshot;

DROP TABLE IF EXISTS tmp_fact_productionoperation_snapshot_upd;
CREATE TABLE tmp_fact_productionoperation_snapshot_upd
AS
SELECT distinct i.*
FROM tmp_fact_productionoperation_snapshot_ins i, tmp_fact_productionoperation_snapshot_del d
WHERE i.dd_OrderNumber = d.dd_OrderNumber 
AND i.dd_orderitemno = d.dd_orderitemno;

/* So, wherever there's existing row and there's an update, mark dd_latestsnapshotflag as N */
UPDATE fact_productionoperation_snapshot s
FROM tmp_fact_productionoperation_snapshot_upd u
SET dd_latestsnapshotflag = 'N'
	,dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
WHERE s.dd_OrderNumber = u.dd_OrderNumber AND s.dd_orderitemno = u.dd_orderitemno
AND s.dd_latestsnapshotflag <> 'N';

CALL VECTORWISE(COMBINE 'tmp_fact_productionoperation_snapshot_ins-tmp_fact_productionoperation_snapshot_del');

/* For new inserts, pick up directly from production order */
INSERT INTO fact_productionoperation_snapshot
(fact_productionoperationid,
amt_meteriel_primarycost,
amt_activitycostttl,
amt_price,
amt_priceunit,
ct_activityduration,
ct_actualwork,
ct_forecastedwork,
ct_activityworkinvolved,
dim_businessareaid,
dim_companyid,
dim_controllingareaid,
dim_materialgroupid,
dim_currencyid,
dim_plantid,
dim_profitcenterid,
dim_purchasegroupid,
dim_purchaseorgid,
dim_tasklisttypeid,
dim_vendorid,
dim_costcenterid,
dd_podocumentno,
dim_functionalareaid,
dim_objecttypeid,
dim_unitofmeasureid,
dd_routingoperationno,
dd_generalordercounter,
dd_poitemno,
dim_dateidactualstartexec,
dim_dateidactualfinishexec,
dd_houractualstartexec,
dd_houractualfinishexe,
dim_dateidschedstartexec,
dim_dateidschedfinishexec,
dd_hourschedstartexec,
dd_hourschedfinishexec,
dd_routingrefsequence,
amt_exchangerate,
amt_exchangerate_gbl,
dim_currencyid_tra,
dim_currencyid_gbl,
dd_operationnumber,
dd_workcenter,
dd_bomexplosionno,
dim_productionorderstatusid,
dd_operationshorttext,
dd_descriptionline2,
dd_passorfail,
dd_ordernumber,
dd_orderno,
dim_dateidwchdrstart,
dim_dateidwchdr_end,
dim_plantidwc,
dd_wccategory,
dd_wcstdtxtkey,
dd_wccontrolkey,
dim_dateidwctocc_start,
dim_costcenteridwctocc,
dim_controllingareaiddwctocc,
dd_wctoccactivitytype,
dd_wctoccrefind,
dd_wctoccformulakeycosting,
dd_wctoccbusinessprocess,
dd_wctoccactivitydescrtype,
dd_activitytypeset,
dim_dateidwctocc_end,
dd_objectid,
dd_operationactivity,
dd_snapshotdate,
dim_dateidscheduledfinish_orig_merck,
ct_totalorderqty_orig_merck,
dd_cancelledorder_orig_merck,
ct_underdeliverytol_orig_merck,
ct_overdeliverytol_orig_merck,
dd_unimitedoverdelivery_orig_merck,
ct_GRQty_orig_merck,
ct_OrderItemQty_orig_merck,
ct_underdeliverytol_merck,
ct_overdeliverytol_merck,
dd_unimitedoverdelivery_merck,
ct_snapshotcount,
dd_latestsnapshotflag,
dim_dateidconfirmedorderfinish_prodorder,
dim_dateidscheduledfinishheader_prodorder,
dim_dateidscheduledfinish_prodorder,
dim_workcenterid,
dim_ordertypeid_prodorder,
dim_partidheader_prodorder,
dim_dateidactualheaderfinish_prodorder,
dim_dateidactualheaderfinishdate_merck_prodorder)
SELECT 
f.fact_productionoperationid,
sum(f.amt_meteriel_primarycost),
sum(f.amt_activitycostttl),
avg(f.amt_price),
avg(f.amt_priceunit),
sum(f.ct_activityduration),
sum(f.ct_actualwork),
sum(f.ct_forecastedwork),
sum(f.ct_activityworkinvolved),
f.dim_businessareaid,
f.dim_companyid,
f.dim_controllingareaid,
f.dim_materialgroupid,
f.dim_currencyid,
f.dim_plantid,
f.dim_profitcenterid,
f.dim_purchasegroupid,
f.dim_purchaseorgid,
f.dim_tasklisttypeid,
f.dim_vendorid,
f.dim_costcenterid,
f.dd_podocumentno,
f.dim_functionalareaid,
f.dim_objecttypeid,
f.dim_unitofmeasureid,
f.dd_routingoperationno,
f.dd_generalordercounter,
f.dd_poitemno,
f.dim_dateidactualstartexec,
f.dim_dateidactualfinishexec,
f.dd_houractualstartexec,
f.dd_houractualfinishexe,
f.dim_dateidschedstartexec,
f.dim_dateidschedfinishexec,
f.dd_hourschedstartexec,
f.dd_hourschedfinishexec,
f.dd_routingrefsequence,
avg(f.amt_exchangerate),
avg(f.amt_exchangerate_gbl),
f.dim_currencyid_tra,
f.dim_currencyid_gbl,
f.dd_operationnumber,
f.dd_workcenter,
f.dd_bomexplosionno,
f.dim_productionorderstatusid,
f.dd_operationshorttext,
f.dd_descriptionline2,
f.dd_passorfail,
f.dd_ordernumber,
f.dd_orderno,
f.dim_dateidwchdrstart,
f.dim_dateidwchdr_end,
f.dim_plantidwc,
f.dd_wccategory,
f.dd_wcstdtxtkey,
f.dd_wccontrolkey,
f.dim_dateidwctocc_start,
f.dim_costcenteridwctocc,
f.dim_controllingareaiddwctocc,
f.dd_wctoccactivitytype,
f.dd_wctoccrefind,
f.dd_wctoccformulakeycosting,
f.dd_wctoccbusinessprocess,
f.dd_wctoccactivitydescrtype,
f.dd_activitytypeset,
f.dim_dateidwctocc_end,
f.dd_objectid,
f.dd_operationactivity,
currdate,
max(fpoforpop.dim_dateidscheduledfinish) dim_dateidscheduledfinish,
sum(fpoforpop.ct_totalorderqty),
fpoforpop.dd_cancelledorder,
avg(fpoforpop.ct_underdeliverytol_merck),
avg(fpoforpop.ct_overdeliverytol_merck),
fpoforpop.dd_unimitedoverdelivery_merck,
sum(fpoforpop.ct_GRQty),
sum(fpoforpop.ct_OrderItemQty),
avg(fpoforpop.ct_underdeliverytol_merck),
avg(fpoforpop.ct_overdeliverytol_merck),
fpoforpop.dd_unimitedoverdelivery_merck,
1,
'Y',
max(fpoforpop.dim_dateidconfirmedorderfinish),
max(fpoforpop.dim_dateidscheduledfinishheader),
max(fpoforpop.dim_dateidscheduledfinish),
max(f.dim_workcenterid),
max(fpoforpop.dim_ordertypeid),
max(fpoforpop.dim_partidheader),
max(fpoforpop.dim_dateidactualheaderfinish),
max(fpoforpop.dim_dateidactualheaderfinishdate_merck)
FROM tmp_fact_productionoperation_snapshot_ins i,fact_productionoperation f, tmp_current_date_fpopsnapshot, fact_productionorder fpoforpop
WHERE i.dd_OrderNumber = f.dd_OrderNumber AND fpoforpop.dd_OrderNumber = f.dd_OrderNumber
GROUP BY 
f.fact_productionoperationid,
f.dim_businessareaid,
f.dim_companyid,
f.dim_controllingareaid,
f.dim_materialgroupid,
f.dim_currencyid,
f.dim_plantid,
f.dim_profitcenterid,
f.dim_purchasegroupid,
f.dim_purchaseorgid,
f.dim_tasklisttypeid,
f.dim_vendorid,
f.dim_costcenterid,
f.dd_podocumentno,
f.dim_functionalareaid,
f.dim_objecttypeid,
f.dim_unitofmeasureid,
f.dd_routingoperationno,
f.dd_generalordercounter,
f.dd_poitemno,
f.dim_dateidactualstartexec,
f.dim_dateidactualfinishexec,
f.dd_houractualstartexec,
f.dd_houractualfinishexe,
f.dim_dateidschedstartexec,
f.dim_dateidschedfinishexec,
f.dd_hourschedstartexec,
f.dd_hourschedfinishexec,
f.dd_routingrefsequence,
f.dim_currencyid_tra,
f.dim_currencyid_gbl,
f.dd_operationnumber,
f.dd_workcenter,
f.dd_bomexplosionno,
f.dim_productionorderstatusid,
f.dd_operationshorttext,
f.dd_descriptionline2,
f.dd_passorfail,
f.dd_ordernumber,
f.dd_orderno,
f.dim_dateidwchdrstart,
f.dim_dateidwchdr_end,
f.dim_plantidwc,
f.dd_wccategory,
f.dd_wcstdtxtkey,
f.dd_wccontrolkey,
f.dim_dateidwctocc_start,
f.dim_costcenteridwctocc,
f.dim_controllingareaiddwctocc,
f.dd_wctoccactivitytype,
f.dd_wctoccrefind,
f.dd_wctoccformulakeycosting,
f.dd_wctoccbusinessprocess,
f.dd_wctoccactivitydescrtype,
f.dd_activitytypeset,
f.dim_dateidwctocc_end,
f.dd_objectid,
f.dd_operationactivity,
currdate,
fpoforpop.dd_cancelledorder,
fpoforpop.dd_unimitedoverdelivery_merck;

/* For existing ord-item, pick up orig fields from latest prod-order snapshot for that ord-item*/
/* Ensure that only those are picked up that have the latest schedule finish date in next week */
/* In Sep testing, there's no re-scheduleing. Comment this out. In actual code, fix this by using ord no and update */
/*
INSERT INTO fact_productionoperation_snapshot(
	fact_productionoperationid,
amt_meteriel_primarycost,
amt_activitycostttl,
amt_price,
amt_priceunit,
ct_activityduration,
ct_actualwork,
ct_forecastedwork,
ct_activityworkinvolved,
dim_businessareaid,
dim_companyid,
dim_controllingareaid,
dim_materialgroupid,
dim_currencyid,
dim_plantid,
dim_profitcenterid,
dim_purchasegroupid,
dim_purchaseorgid,
dim_tasklisttypeid,
dim_vendorid,
dim_costcenterid,
dd_podocumentno,
dim_functionalareaid,
dim_objecttypeid,
dim_unitofmeasureid,
dd_routingoperationno,
dd_generalordercounter,
dd_poitemno,
dim_dateidactualstartexec,
dim_dateidactualfinishexec,
dd_houractualstartexec,
dd_houractualfinishexe,
dim_dateidschedstartexec,
dim_dateidschedfinishexec,
dd_hourschedstartexec,
dd_hourschedfinishexec,
dd_routingrefsequence,
amt_exchangerate,
amt_exchangerate_gbl,
dim_currencyid_tra,
dim_currencyid_gbl,
dd_operationnumber,
dd_workcenter,
dd_bomexplosionno,
dim_productionorderstatusid,
dd_operationshorttext,
dd_descriptionline2,
dd_passorfail,
dd_ordernumber,
dd_orderno,
dim_dateidwchdrstart,
dim_dateidwchdr_end,
dim_plantidwc,
dd_wccategory,
dd_wcstdtxtkey,
dd_wccontrolkey,
dim_dateidwctocc_start,
dim_costcenteridwctocc,
dim_controllingareaiddwctocc,
dd_wctoccactivitytype,
dd_wctoccrefind,
dd_wctoccformulakeycosting,
dd_wctoccbusinessprocess,
dd_wctoccactivitydescrtype,
dd_activitytypeset,
dim_dateidwctocc_end,
dd_objectid,
dd_operationactivity,
	dd_snapshotdate,
	dim_dateidscheduledfinish_orig_merck,
	ct_totalorderqty_orig_merck,
	dd_cancelledorder_orig_merck,
	ct_underdeliverytol_orig_merck,
	ct_overdeliverytol_orig_merck,
	dd_unimitedoverdelivery_orig_merck,
	ct_GRQty_orig_merck,
	ct_OrderItemQty_orig_merck,
	dd_cancelledorder,
	ct_underdeliverytol_merck,
	ct_overdeliverytol_merck,
	dd_unimitedoverdelivery_merck,
	ct_snapshotcount,
	dd_latestsnapshotflag)
SELECT DISTINCT
f_propr.fact_productionoperationid,
f_propr.amt_meteriel_primarycost,
f_propr.amt_activitycostttl,
f_propr.amt_price,
f_propr.amt_priceunit,
f_propr.ct_activityduration,
f_propr.ct_actualwork,
f_propr.ct_forecastedwork,
f_propr.ct_activityworkinvolved,
f_propr.dim_businessareaid,
f_propr.dim_companyid,
f_propr.dim_controllingareaid,
f_propr.dim_materialgroupid,
f_propr.dim_currencyid,
f_propr.dim_plantid,
f_propr.dim_profitcenterid,
f_propr.dim_purchasegroupid,
f_propr.dim_purchaseorgid,
f_propr.dim_tasklisttypeid,
f_propr.dim_vendorid,
f_propr.dim_costcenterid,
f_propr.dd_podocumentno,
f_propr.dim_functionalareaid,
f_propr.dim_objecttypeid,
f_propr.dim_unitofmeasureid,
f_propr.dd_routingoperationno,
f_propr.dd_generalordercounter,
f_propr.dd_poitemno,
f_propr.dim_dateidactualstartexec,
f_propr.dim_dateidactualfinishexec,
f_propr.dd_houractualstartexec,
f_propr.dd_houractualfinishexe,
f_propr.dim_dateidschedstartexec,
f_propr.dim_dateidschedfinishexec,
f_propr.dd_hourschedstartexec,
f_propr.dd_hourschedfinishexec,
f_propr.dd_routingrefsequence,
f_propr.amt_exchangerate,
f_propr.amt_exchangerate_gbl,
f_propr.dim_currencyid_tra,
f_propr.dim_currencyid_gbl,
f_propr.dd_operationnumber,
f_propr.dd_workcenter,
f_propr.dd_bomexplosionno,
f_propr.dim_productionorderstatusid,
f_propr.dd_operationshorttext,
f_propr.dd_descriptionline2,
f_propr.dd_passorfail,
f_propr.dd_ordernumber,
f_propr.dd_orderno,
f_propr.dim_dateidwchdrstart,
f_propr.dim_dateidwchdr_end,
f_propr.dim_plantidwc,
f_propr.dd_wccategory,
f_propr.dd_wcstdtxtkey,
f_propr.dd_wccontrolkey,
f_propr.dim_dateidwctocc_start,
f_propr.dim_costcenteridwctocc,
f_propr.dim_controllingareaiddwctocc,
f_propr.dd_wctoccactivitytype,
f_propr.dd_wctoccrefind,
f_propr.dd_wctoccformulakeycosting,
f_propr.dd_wctoccbusinessprocess,
f_propr.dd_wctoccactivitydescrtype,
f_propr.dd_activitytypeset,
f_propr.dim_dateidwctocc_end,
f_propr.dd_objectid,
f_propr.dd_operationactivity,
	currdate,
	s.dim_dateidscheduledfinish,
	s.ct_totalorderqty,
	s.dd_cancelledorder,
	s.ct_underdeliverytol_merck,
	s.ct_overdeliverytol_merck,
	s.dd_unimitedoverdelivery_merck,
	s.ct_GRQty,
	s.ct_OrderItemQty,
	fpoforpop.dd_cancelledorder,
	fpoforpop.ct_underdeliverytol_merck,
	fpoforpop.ct_overdeliverytol_merck,
	fpoforpop.dd_unimitedoverdelivery_merck,
	s.ct_snapshotcount + 1,
	'Y'	
FROM 	fact_productionoperation f_propr, tmp_current_date_fpopsnapshot, tmp_fact_productionoperation_snapshot_maxdate s,
	tmp_fact_productionoperation_snapshot_upd u,fact_productionorder fpoforpop
WHERE s.dd_RoutingOperationNo = f_propr.dd_RoutingOperationNo AND s.dd_GeneralOrderCounter = f_propr.dd_GeneralOrderCounter
AND u.dd_RoutingOperationNo = s.dd_RoutingOperationNo AND u.dd_GeneralOrderCounter = s.dd_GeneralOrderCounter
AND fpoforpop.dd_OrderNumber = f_propr.dd_OrderNumber*/

/* Update dim_dateidsnapshot */
UPDATE fact_productionoperation_snapshot s
FROM dim_date dt,dim_company dc
SET s.dim_dateidsnapshot = dt.dim_dateid
WHERE s.dd_snapshotdate = dt.datevalue
AND s.dim_companyid = dc.dim_companyid AND dc.companycode = dt.CompanyCode
AND s.dim_dateidsnapshot <> dt.dim_dateid;


UPDATE fact_productionoperation_snapshot f
FROM afvc_afvv a
SET dd_objectid = a.afvc_arbid
WHERE f.dd_RoutingOperationNo = a.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = a.AFVC_APLZL;


UPDATE fact_productionoperation_snapshot f
FROM dim_workcenter wc
SET f.dim_workcenterid = wc.dim_workcenterid
WHERE wc.objectid = f.dd_objectid;

DROP TABLE IF EXISTS tmp_distinct_prodorder_data;
CREATE TABLE tmp_distinct_prodorder_data
AS
SELECT dd_ordernumber,dd_unimitedoverdelivery_merck,
sum(ct_totalorderqty) ct_totalorderqty,sum(ct_grqty) ct_grqty,sum(ct_orderitemqty) ct_orderitemqty,avg(ct_overdeliverytol_merck) ct_overdeliverytol_merck,avg(ct_underdeliverytol_merck) ct_underdeliverytol_merck
FROM fact_productionorder
GROUP BY dd_ordernumber,dd_unimitedoverdelivery_merck;


UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dd_unimitedoverdelivery_merck_prodorder = prod.dd_unimitedoverdelivery_merck
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dd_unimitedoverdelivery_merck_prodorder <> prod.dd_unimitedoverdelivery_merck;


UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.ct_totalorderqty_prodorder = prod.ct_totalorderqty
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.ct_totalorderqty_prodorder <> prod.ct_totalorderqty;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET ct_grqty_prodorder = prod.ct_grqty
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.ct_grqty_prodorder <> prod.ct_grqty;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET ct_orderitemqty_prodorder = prod.ct_orderitemqty
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.ct_orderitemqty_prodorder <> prod.ct_orderitemqty;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.ct_overdeliverytol_merck_prodorder = prod.ct_overdeliverytol_merck
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.ct_overdeliverytol_merck_prodorder <> prod.ct_overdeliverytol_merck;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.ct_underdeliverytol_merck_prodorder = prod.ct_underdeliverytol_merck
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.ct_underdeliverytol_merck_prodorder <> prod.ct_underdeliverytol_merck;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dd_unimitedoverdelivery_merck_prodorder = prod.dd_unimitedoverdelivery_merck
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dd_unimitedoverdelivery_merck_prodorder <> prod.dd_unimitedoverdelivery_merck;

/* Note that as per Kiran these columns must have the same value for different items. So, we should get 1 distinct row per prod order regardless of no of items */

DROP TABLE IF EXISTS tmp_distinct_prodorder_data;
CREATE TABLE tmp_distinct_prodorder_data
AS
SELECT DISTINCT dd_ordernumber,
dim_dateidconfirmedorderfinish,dim_dateidactualheaderfinishdate_merck,dim_dateidactualitemfinish,dim_dateidactualrelease,dim_dateidactualstart,dim_dateidbasicfinish,dim_dateidbasicstart,dim_dateidlastscheduling,dim_partidheader,dim_partiditem,dim_mrpcontrollerid,dim_productionorderstatusid,dim_ordertypeid,dim_dateidplannedorderdelivery,dim_productionschedulerid,dim_dateidrelease,dim_dateidroutingtransfer,dim_dateidscheduledfinish,dim_dateidscheduledrelease,dim_dateidscheduledstart,dim_dateidtechnicalcompletion
FROM fact_productionorder;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidconfirmedorderfinish_prodorder = prod.dim_dateidconfirmedorderfinish
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidconfirmedorderfinish_prodorder <> prod.dim_dateidconfirmedorderfinish;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidactualheaderfinishdate_merck_prodorder = prod.dim_dateidactualheaderfinishdate_merck
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidactualheaderfinishdate_merck_prodorder <> prod.dim_dateidactualheaderfinishdate_merck;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidactualitemfinish_prodorder = prod.dim_dateidactualitemfinish
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidactualitemfinish_prodorder <> prod.dim_dateidactualitemfinish;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidactualrelease_prodorder = prod.dim_dateidactualrelease
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidactualrelease_prodorder <> prod.dim_dateidactualrelease;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidactualstart_prodorder = prod.dim_dateidactualstart
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidactualstart_prodorder <> prod.dim_dateidactualstart;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidbasicfinish_prodorder = prod.dim_dateidbasicfinish
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidbasicfinish_prodorder <> prod.dim_dateidbasicfinish;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidbasicstart_prodorder = prod.dim_dateidbasicstart
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidbasicstart_prodorder <> prod.dim_dateidbasicstart;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidlastscheduling_prodorder = prod.dim_dateidlastscheduling
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidlastscheduling_prodorder <> prod.dim_dateidlastscheduling;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_partidheader_prodorder = prod.dim_partidheader
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_partidheader_prodorder <> prod.dim_partidheader;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_partiditem_prodorder = prod.dim_partiditem
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_partiditem_prodorder <> prod.dim_partiditem;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_mrpcontrollerid_prodorder = prod.dim_mrpcontrollerid
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_mrpcontrollerid_prodorder <> prod.dim_mrpcontrollerid;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_productionorderstatusid_prodorder = prod.dim_productionorderstatusid
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_productionorderstatusid_prodorder <> prod.dim_productionorderstatusid;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_ordertypeid_prodorder = prod.dim_ordertypeid
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_ordertypeid_prodorder <> prod.dim_ordertypeid;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidplannedorderdelivery_prodorder = prod.dim_dateidplannedorderdelivery
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidplannedorderdelivery_prodorder <> prod.dim_dateidplannedorderdelivery;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_productionschedulerid_prodorder = prod.dim_productionschedulerid
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_productionschedulerid_prodorder <> prod.dim_productionschedulerid;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidrelease_prodorder = prod.dim_dateidrelease
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidrelease_prodorder <> prod.dim_dateidrelease;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidroutingtransfer_prodorder = prod.dim_dateidroutingtransfer
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidroutingtransfer_prodorder <> prod.dim_dateidroutingtransfer;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidscheduledfinish_prodorder = prod.dim_dateidscheduledfinish
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidscheduledfinish_prodorder <> prod.dim_dateidscheduledfinish;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidscheduledrelease_prodorder = prod.dim_dateidscheduledrelease
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidscheduledrelease_prodorder <> prod.dim_dateidscheduledrelease;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidscheduledstart_prodorder = prod.dim_dateidscheduledstart
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidscheduledstart_prodorder <> prod.dim_dateidscheduledstart;

UPDATE fact_productionoperation_snapshot pops
FROM tmp_distinct_prodorder_data prod
SET pops.dim_dateidtechnicalcompletion_prodorder = prod.dim_dateidtechnicalcompletion
WHERE pops.dd_ordernumber = prod.dd_ordernumber
AND pops.dim_dateidtechnicalcompletion_prodorder <> prod.dim_dateidtechnicalcompletion;

/* Update tolerances from dim_part */
UPDATE fact_productionoperation_snapshot f_popsnap
FROM dim_part dp
SET f_popsnap.ct_underdeliverytol_merck_prodorder = dp.underdelivery_tolerance
WHERE f_popsnap.dim_partidheader_prodorder = dp.dim_partid
AND f_popsnap.ct_underdeliverytol_merck_prodorder <> dp.underdelivery_tolerance;


UPDATE fact_productionoperation_snapshot f_popsnap
FROM dim_part dp
SET f_popsnap.ct_overdeliverytol_merck_prodorder = dp.overdelivery_tolerance
WHERE f_popsnap.dim_partidheader_prodorder = dp.dim_partid
AND f_popsnap.ct_overdeliverytol_merck_prodorder <> dp.overdelivery_tolerance;


UPDATE fact_productionoperation_snapshot f_popsnap
FROM dim_part dp
SET dd_unimitedoverdelivery_merck_prodorder = dp.unlimited_overdelivery_flag
WHERE f_popsnap.dim_partidheader_prodorder = dp.dim_partid
AND f_popsnap.dd_unimitedoverdelivery_merck_prodorder <> dp.unlimited_overdelivery_flag; 


/* Now set the infull,ontime,miss,ontimeandinfull values */
/* For each prod order it can be either 0 or 1 */
UPDATE fact_productionoperation_snapshot
SET ct_infull = 0;
UPDATE fact_productionoperation_snapshot
SET ct_ontime = 0;

/* Exclude all non-PI orders and PI05, PI06, PI07, PI08 from quantity validation */
UPDATE fact_productionoperation_snapshot f_popsnap
FROM dim_productionordertype ord
SET ct_infull = 1
WHERE f_popsnap.Dim_ordertypeid_prodorder = ord.dim_productionordertypeid
AND ord.typecode in ( 'PI05','PI06','PI07','PI08');

UPDATE fact_productionoperation_snapshot f_popsnap
FROM dim_productionordertype ord
SET ct_infull = 1
WHERE f_popsnap.Dim_ordertypeid_prodorder = ord.dim_productionordertypeid
AND ord.typecode NOT LIKE 'PI%';

UPDATE fact_productionoperation_snapshot f_popsnap
SET ct_infull = 1
WHERE f_popsnap.ct_grqty_prodorder<>0 AND f_popsnap.dd_unimitedoverdelivery_merck_prodorder = 'Not Set'
AND f_popsnap.ct_totalorderqty_prodorder * ( 1 - f_popsnap.ct_underdeliverytol_merck_prodorder/100 ) <= f_popsnap.ct_grqty_prodorder
AND f_popsnap.ct_grqty_prodorder <= f_popsnap.ct_totalorderqty_prodorder * ( 1 + f_popsnap.ct_overdeliverytol_merck_prodorder/100);

UPDATE fact_productionoperation_snapshot f_popsnap
SET ct_infull = 1
WHERE f_popsnap.ct_grqty_prodorder<>0 AND f_popsnap.dd_unimitedoverdelivery_merck_prodorder = 'X'
and (f_popsnap.ct_totalorderqty_prodorder * ( 1 - f_popsnap.ct_underdeliverytol_merck_prodorder/100 ) <= f_popsnap.ct_grqty_prodorder );


UPDATE fact_productionoperation_snapshot f_popsnap
FROM dim_date s_origsfd,dim_date p_cnfd
SET ct_ontime = 1
WHERE s_origsfd.dim_dateid = dim_dateidscheduledfinish_orig_merck
AND p_cnfd.dim_dateid = dim_dateidconfirmedorderfinish_prodorder
AND  f_popsnap.dim_dateidconfirmedorderfinish_prodorder > 1 and week(p_cnfd.datevalue,4) <= week(s_origsfd.datevalue,4)
AND year(p_cnfd.datevalue) <= year(s_origsfd.datevalue) ;

UPDATE fact_productionoperation_snapshot  f_popsnap
SET ct_ontimeandinfull = 1
WHERE ct_infull = 1 AND ct_ontime = 1;

UPDATE fact_productionoperation_snapshot  f_popsnap
SET ct_miss = 1 - ct_ontimeandinfull;

