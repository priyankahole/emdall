
select 'START OF PROC vw_bi_populate_billing_sales_shipment_attributes.sql',TIMESTAMP(LOCAL_TIMESTAMP);


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
   SET fb.Dim_SalesDocumentItemStatusId = ifnull(fso.dim_salesorderitemstatusid,1)
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
  SET  fb.Dim_so_CostCenterid = fso.Dim_CostCenterid
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
      
UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
  set  fb.Dim_so_SalesDocumentTypeid = fso.Dim_SalesDocumentTypeid
   WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_so_DateidSchedDelivery = fso.Dim_DateidSchedDelivery
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
 SET   fb.Dim_so_SalesOrderCreatedId = fso.Dim_DateidSalesOrderCreated
  WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId = fso.Dim_CustomPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_PayerPartnerFunctionId = fso.Dim_PayerPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_BillToPartyPartnerFunctionId = fso.Dim_BillToPartyPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomerGroupId = fso.Dim_CustomerGroupId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId1 = fso.Dim_CustomPartnerFunctionId1
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId2 = fso.Dim_CustomPartnerFunctionId2
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomeridShipTo = fso.Dim_CustomeridShipTo
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET    fb.ct_so_ConfirmedQty = ifnull((SELECT SUM(f_so.ct_ConfirmedQty) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno),0)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
	vbrk_vbrp vkp
SET    fb.amt_so_UnitPrice = fso.amt_UnitPrice
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET       fb.ct_so_DeliveredQty = ifnull(( SELECT SUM(f_so.ct_DeliveredQty) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno) ,0)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

UPDATE fact_billing fb FROM
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET       fb.amt_so_Returned = 
                ifnull((SELECT SUM(f_so.amt_ScheduleTotal) FROM fact_salesorder f_so,
						 Dim_documentcategory dc
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno
				     AND dc.Dim_DocumentCategoryid = f_so.Dim_DocumentCategoryid
				     AND dc.DocumentCategory IN ('H', 'K') 
				     AND f_so.Dim_SalesOrderRejectReasonid <> 1),0)
        
 WHERE     fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       dim_billingmisc bm,
       Dim_documentcategory dc,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
 SET      fb.ct_so_OpenOrderQty =
                    ifnull((SELECT SUM(f_so.ct_ScheduleQtySalesUnit) - SUM(f_so.ct_DeliveredQty ) FROM fact_salesorder f_so
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno),0)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


UPDATE fact_billing fb FROM
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
SET    fb.ct_so_ReturnedQty =
                ifnull((SELECT SUM(f_so.ct_ScheduleQtySalesUnit) FROM fact_salesorder f_so,
                                    Dim_documentcategory dc
                                   WHERE f_so.dd_SalesDocNo = fb.dd_salesdocno
                                     AND f_so.dd_SalesItemNo = fb.dd_salesitemno
                                     AND dc.Dim_DocumentCategoryid = f_so.Dim_DocumentCategoryid
                                     AND dc.DocumentCategory IN ('H', 'K')
                  					 AND f_so.Dim_SalesOrderRejectReasonid <> 1
                                     ),0)
 WHERE     fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

       
UPDATE fact_billing fb FROM
       fact_salesorder fso,
       Dim_documentcategory dc
SET    fb.amt_revenue =
          (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
      WHEN '5' THEN 0
      WHEN '6' THEN 0
      WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
      WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
              ELSE fb.amt_NetValueItem
           END)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       Dim_documentcategory dc
SET    fb.amt_Revenue_InDocCurrency = (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
      WHEN '5' THEN 0
      WHEN '6' THEN 0
      WHEN 'N' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
      WHEN 'O' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
              ELSE fb.amt_NetValueItem_InDocCurrency
           END)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       Dim_documentcategory dc,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt,
       vbrk_vbrp vkp
 SET   fb.amt_so_confirmed =
          ifnull((SELECT SUM(
                       f_so.ct_ConfirmedQty
                     * (f_so.amt_UnitPrice
                     / (CASE WHEN f_so.ct_PriceUnit = 0 THEN NULL ELSE f_so.ct_PriceUnit END)))
             FROM fact_salesorder f_so
            WHERE f_so.dd_SalesDocNo = fb.dd_SalesDocNo
                  AND f_so.dd_salesitemno = fb.dd_salesitemno),0)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');


DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;

CREATE TABLE tmp_openorder_from_Sales_to_billing AS
SELECT f_so.dd_SalesDocNo, f_so.dd_salesitemno,SUM(f_so.ct_ScheduleQtySalesUnit) as orderqty,SUM(f_so.ct_DeliveredQty) as DlvrQty, AVG(f_so.amt_UnitPrice) as TotalUnitPrice, ifnull(avg(f_so.ct_PriceUnit),0) as PriceUnit from fact_salesorder f_so
group by f_so.dd_SalesDocNo,f_so.dd_salesitemno;

UPDATE fact_billing fb FROM
	   tmp_openorder_from_Sales_to_billing t,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm,
       vbrk_vbrp vkp
SET       fb.amt_so_openorder = 
          ((t.orderqty - t.DlvrQty)* t.TotalUnitPrice /t.PriceUnit)
 WHERE t.dd_salesDocNo = fb.dd_Salesdocno
       AND t.dd_SalesItemNo = fb.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR       
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;
  
UPDATE fact_billing fb 
 from VBRK_VBRP vkp,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt
SET fb.amt_sd_Cost = ifnull(( SELECT SUM(f_sod.amt_Cost) FROM fact_salesorderdelivery f_sod
                                   WHERE f_sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
                                     AND f_sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo),0) 
  WHERE     fb.dd_billing_no = vkp.VBRK_VBELN
        AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	AND fb.dim_billingmiscid = bm.dim_billingmiscid
	and bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

 UPDATE fact_billing fb 
 from VBRK_VBRP vkp,
 	   fact_salesorderdelivery sod,
       dim_billingmisc bm,
       dim_billingdocumenttype bdt
 SET fb.Dim_sd_DateidActualGoodsIssue = sod.Dim_DateidActualGoodsIssue 
  WHERE     fb.dd_billing_no = vkp.VBRK_VBELN
        AND fb.dd_billing_item_no = vkp.VBRP_POSNR
        AND sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
        AND sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo
		AND fb.dim_billingmiscid = bm.dim_billingmiscid
		and bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;

CREATE TABLE tmp_openorder_from_Shipment_to_billing AS
SELECT f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo,SUM(f_sod.ct_QtyDelivered) as dlvrqty,AVG(f_sod.amt_UnitPrice) as AmtUnitPrice,ifnull(avg(f_sod.ct_PriceUnit),0) as PriceUnit from fact_salesorderdelivery f_sod
group by f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo;
       
       
  UPDATE fact_billing fb 
  from VBRK_VBRP vkp,
  	   tmp_openorder_from_Shipment_to_billing t,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm
    SET fb.amt_sd_Shipped = (t.dlvrqty * t.AmtUnitPrice/(case when t.PriceUnit = 0 then null else t.PriceUnit end )),
        fb.ct_sd_QtyDelivered = t.dlvrqty
  WHERE     fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo
        AND fb.dd_billing_no = vkp.VBRK_VBELN
        AND fb.dd_billing_item_no = vkp.VBRP_POSNR
       AND fb.dim_billingmiscid = bm.dim_billingmiscid
       AND bm.CancelFlag = 'Not Set'
   AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
   AND bdt.type IN ('F1','F2','ZF2','ZF2C');

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;

DROP table if exists tmp_SalesOrderAmtForBilling;   

  CREATE TABLE tmp_SalesOrderAmtForBilling 
  AS
  SELECT dd_billing_no,dd_billing_item_no,dt.datevalue as billingdate,so.dd_SalesDocNo,so.dd_salesItemno,AVG(fb.ct_sd_QtyDelivered) as sd_QtyDelivered,AVG(fb.amt_sd_Shipped) as sd_shipped,sum(so.ct_ScheduleQtySalesUnit) as totalorderqty,sum(so.amt_ScheduleTotal) as totalorderamt,000000000000000000.0000 as acttotalqty, 000000000000000000.0000 as acttotalamt
  FROM fact_billing fb, fact_salesorder so,dim_date dt, dim_billingdocumenttype bdt,  
         dim_billingmisc bm
   WHERE fb.dd_SalesDocNo = so.dd_salesdocno
  and fb.dd_salesitemno = so.dd_SalesItemNo
  and fb.Dim_DateidBilling = dt.dim_dateid
  and fb.dd_Salesdocno <> 'Not Set'
  AND fb.dim_billingmiscid = bm.dim_billingmiscid
  AND bm.CancelFlag = 'Not Set'
  AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
  AND bdt.type IN ('F1','F2','ZF2','ZF2C')
  group by dd_billing_no,dd_billing_item_no,dt.datevalue,so.dd_SalesDocNo,so.dd_salesItemno;

  UPDATE tmp_SalesOrderAmtForBilling
  set acttotalamt =  sd_Shipped,
    acttotalqty =  sd_QtyDelivered
  where totalorderamt > sd_Shipped;

  DROP table if exists tmp_SalesOrderAmtForBilling_100;   

  CREATE TABLE tmp_SalesOrderAmtForBilling_100
  AS SELECT dd_salesDocNo,dd_salesitemno, max(billingdate) as billdate,sum(acttotalamt) as ActualAmt,sum(acttotalqty) as ActualQty from
  tmp_SalesOrderAmtForBilling group by dd_salesDocNo,dd_salesitemno;

  UPDATE tmp_SalesOrderAmtForBilling t1 FROM
    tmp_SalesOrderAmtForBilling_100 t2
  set t1.acttotalamt = t1.acttotalamt + (t1.TotalOrderamt - t2.ActualAmt),
      t1.acttotalqty = t1.acttotalqty + (t1.Totalorderqty - t2.actualqty)
  where t1.dd_Salesdocno = t2.dd_salesdocno
    AND t1.dd_salesitemno = t2.dd_Salesitemno
    and t1.billingdate = t2.billdate
    and t1.totalorderamt > t2.ActualAmt;

  DROP table if exists tmp_SalesOrderAmtForBilling_100; 

  UPDATE fact_billing fb
  from tmp_SalesOrderAmtForBilling tmp
  set fb.amt_so_ScheduleTotal = tmp.acttotalamt,
      fb.ct_so_ScheduleQtySalesUnit = tmp.acttotalqty
  WHERE fb.dd_billing_no = tmp.dd_billing_no
  and fb.dd_billing_item_no = tmp.dd_billing_item_no;

  DROP table if exists tmp_SalesOrderAmtForBilling;

  UPDATE fact_salesorderdelivery sod 
  FROM fact_billing fb,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm
    SET sod.dd_billing_no = fb.dd_billing_no,
        sod.dd_billingitem_no = fb.dd_billing_item_no
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
	AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
	AND bdt.type IN ('F1','F2','ZF2','ZF2C')
AND bm.dim_billingmiscid = fb.dim_billingmiscid
AND bm.CancelFlag = 'Not Set';

select 'END OF PROC vw_bi_populate_billing_sales_shipment_attributes.sql',TIMESTAMP(LOCAL_TIMESTAMP);