/* **************************************************************************************************************** */
/*   Script         : vw_bi_populate_chargebackaging_fact                                                           */
/*   Author         : George                                                                                        */
/*   Created On     : 23 Jul 2014                                                                                   */
/* ********************************************Change History****************************************************** */
/*   Date             By        	Version           Desc                                                          */
/*   25.07.2014       Alexandru U.  0.1               Add days bucket based on PostingDate and CurrentDate          */
/*   04.08.2014       Alexandru U.  0.2               Add DD_PARKING_DOC_NUMBER base on VBELN from irm_ipcbdfl      */
/*   23.08.2014       Alexandru U.  0.2               Add dim_vendor (client wanted only Vendor Name)               */
/*   22.12.2014       Alex D.       0.3               Add Vendor Dimension (update from  fact_chargebacks           */
/* **************************************************************************************************************** */

delete from NUMBER_FOUNTAIN where table_name = 'fact_chargebackaging';

INSERT INTO NUMBER_FOUNTAIN
select 	'fact_chargebackaging',
	ifnull(max(f.fact_chargebackagingid), 
	ifnull((select s.dim_projectsourceid * s.multiplier from dim_projectsource s),0))
from fact_chargebackaging f;

drop table if exists tmp_chargebackaging_new_no_001;
create table tmp_chargebackaging_new_no_001 as
SELECT DISTINCT fc.dd_ipnum
FROM fact_chargebacks fc
WHERE NOT EXISTS (SELECT fca.dd_ipnum FROM fact_chargebackaging fca where fca.dd_ipnum = fc.dd_ipnum);

INSERT INTO fact_chargebackaging(fact_chargebackagingid,
                                  dd_application,
                                  dd_debitcreditind, 
                                  dd_recipient, 
                                  dd_splitcriteria,
                                  dd_package, 
                                  dd_salesbilldocno,
                                  dd_groupkey,  
                                  dd_docconditionno, 
                                  dd_exratetype, 
                                  dd_billdoccancelledflag,
                                  dd_cancelledbilldocno, 
                                  dd_accrualstatus,
                                  dd_parkstatus,
                                  dd_settlementstatus, 
                                  dd_deleteindheader, 
                                  dd_completionstatus, 
                                  dd_hdrcreatedby, 
                                  amt_exchangeratefipost,
                                  amt_exchangeratepricedetr, 
                                  amt_netvalueheader, 
                                  dim_chargebacktypesid, 
                                  dim_salseorgid, 
                                  dim_distributionchannelid, 
                                  dim_purchaseorgid, 
                                  dim_customergroupid,  
                                  dim_salesdistrictid,  
                                  dim_salesdoctypeid, 
                                  dim_billingdoctypeid, 
                                  dim_customeridsoldto, 
                                  dim_dateiddocdate, 
                                  dim_dateidpostingdate, 
                                  dim_companyid, 
                                  dim_companyidclearing, 
                                  dim_dateidhdrcreated, 
                                  dim_dateidhdrlastchg, 
                                  dd_ipnum,
                                  amt_exchangerate,
                                  amt_exchangerate_gbl,
								  amt_ExchangeRate_Stat,
								  dim_currencyid, 
                                  dim_currencyid_tra,
                                  dim_currencyid_stat,
								  amt_AccrualTotalAC,
								  amt_AccrualTotalAR,
								  amt_SettlementTotalSI,
								  amt_SettlementTotalRI,
								  dim_DateidAgingPostingDate,
								  dd_DaysBucket,                /* CurrentDate - PostingDate in days */
								  amt_less30NVH,				/* Get amt_netvalueheader (NVH) based on CurrentDate - PostingDate in days */
								  amt_3160NVH,					/* Get amt_netvalueheader (NVH) based on CurrentDate - PostingDate in days */
								  amt_6190NVH,					/* Get amt_netvalueheader (NVH) based on CurrentDate - PostingDate in days */
								  amt_91120NVH,					/* Get amt_netvalueheader (NVH) based on CurrentDate - PostingDate in days */
								  amt_great120NVH,			    /* Get amt_netvalueheader (NVH) based on CurrentDate - PostingDate in days */
								  DD_PARKING_DOC_NUMBER,        /* VBELN from irm_ipcbdfl */  
								  dim_vendorid
								  )                                                    

SELECT (SELECT ifnull(max_id,0) FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_chargebackaging') + row_number() over() fact_chargebackagingid,
                                'Not Set' dd_application,
                                'Not Set' dd_debitcreditind, 
                                'Not Set' dd_recipient, 
                                'Not Set' dd_splitcriteria,
                                'Not Set' dd_package, 
                                'Not Set' dd_salesbilldocno,
                                'Not Set' dd_groupkey,  
                                'Not Set' dd_docconditionno, 
                                'Not Set' dd_exratetype, 
                                'Not Set' dd_billdoccancelledflag,
                                'Not Set' dd_cancelledbilldocno, 
                                'Not Set'  dd_accrualstatus,
                                'Not Set'  dd_parkstatus,
                                'Not Set'  dd_settlementstatus, 
                                'Not Set'  dd_deleteindheader, 
                                'Not Set'  dd_completionstatus, 
                                'Not Set'  dd_hdrcreatedby, 
                                 0  amt_exchangeratefipost,
                                 0  amt_exchangeratepricedetr, 
                                 0  amt_netvalueheader, 
                                 1 dim_chargebacktypesid, 
                                 1 dim_salseorgid, 
                                 1 dim_distributionchannelid, 
                                 1 dim_purchaseorgid, 
                                 1 dim_customergroupid,  
                                 1 dim_salesdistrictid,  
                                 1 dim_salesdoctypeid, 
                                 1 dim_billingdoctypeid, 
                                 1 dim_customeridsoldto, 
                                 1 dim_dateiddocdate, 
                                 1 dim_dateidpostingdate, 
                                 1 dim_companyid, 
                                 1 dim_companyidclearing, 
                                 1 dim_dateidhdrcreated, 
                                 1 dim_dateidhdrlastchg, 
                                 dd_ipnum,
                       			 1  amt_exchangerate,
                                 1  amt_exchaxngerate_gbl,
								 1 amt_ExchangeRate_Stat,
								 1 dim_currencyid, 
                                 1 dim_currencyid_tra,
                                 1 dim_currencyid_stat,
								 0 amt_AccrualTotalAC,
								 0 amt_AccrualTotalAR,
								 0 amt_SettlementTotalSI,
								 0 amt_SettlementTotalRI,
								 1 dim_DateidAgingPostingDate,
								 'Not Set'  dd_DaysBucket,
								 0 amt_less30NVH,
								 0 amt_3160NVH,	
								 0 amt_6190NVH,	
								 0 amt_91120NVH,	
								 0 amt_great120NVH,
								 'Not Set' DD_PARKING_DOC_NUMBER,
								 1 dim_vendorid
FROM tmp_chargebackaging_new_no_001;
      
call vectorwise(combine 'fact_chargebackaging');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_chargebackaging;
\i /db/schema_migration/bin/wrapper_optimizedb.sh IRM_IPCBHDR;


UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_application = ifnull(fc.dd_application,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_application <> ifnull(fc.dd_application,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_debitcreditind = ifnull(fc.dd_debitcreditind,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND ifnull(fca.dd_debitcreditind,'Not Set') <> ifnull(fc.dd_debitcreditind,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_recipient = ifnull(fc.dd_recipient,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_recipient <> ifnull(fc.dd_recipient,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_splitcriteria = ifnull(fc.dd_splitcriteria,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_splitcriteria <> ifnull(fc.dd_splitcriteria,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_package = ifnull(fc.dd_package,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_package <> ifnull(fc.dd_package,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_salesbilldocno = ifnull(fc.dd_salesbilldocno,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_salesbilldocno <> ifnull(fc.dd_salesbilldocno,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_groupkey = ifnull(fc.dd_groupkey,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_groupkey <> ifnull(fc.dd_groupkey,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_docconditionno = ifnull(fc.dd_docconditionno,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_docconditionno <> ifnull(fc.dd_docconditionno,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_exratetype = ifnull(fc.dd_exratetype,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum =  fc.dd_ipnum
  AND fca.dd_exratetype <> ifnull(fc.dd_exratetype,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_billdoccancelledflag = ifnull(fc.dd_billdoccancelledflag,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_billdoccancelledflag <> ifnull(fc.dd_billdoccancelledflag,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_cancelledbilldocno = ifnull(fc.dd_cancelledbilldocno,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_cancelledbilldocno <> ifnull(fc.dd_cancelledbilldocno,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_accrualstatus = ifnull(fc.dd_accrualstatus,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_accrualstatus <> ifnull(fc.dd_accrualstatus,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_parkstatus = ifnull(fc.dd_parkstatus,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_parkstatus <> ifnull(fc.dd_parkstatus,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_settlementstatus = ifnull(fc.dd_settlementstatus,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_settlementstatus <> ifnull(fc.dd_settlementstatus,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_deleteindheader = ifnull(fc.dd_deleteindheader,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum 
  AND fca.dd_deleteindheader <> ifnull(fc.dd_deleteindheader,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_completionstatus =  ifnull(fc.dd_completionstatus,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_completionstatus <> ifnull(fc.dd_completionstatus,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dd_hdrcreatedby = ifnull(fc.dd_hdrcreatedby,'Not Set')
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dd_hdrcreatedby <> ifnull(fc.dd_hdrcreatedby,'Not Set');

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.amt_exchangeratefipost = ifnull(fc.amt_exchangeratefipost,0)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_IPNum = fc.dd_IPNum
  AND fca.amt_exchangeratefipost <> ifnull(fc.amt_exchangeratefipost,0);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.amt_exchangeratepricedetr = ifnull(fc.amt_exchangeratepricedetr,0)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_IPNum = fc.dd_IPNum
  AND fca.amt_exchangeratepricedetr <> ifnull(fc.amt_exchangeratepricedetr,0);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.amt_netvalueheader = ifnull(fc.amt_netvalueheader,0)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_IPNum = fc.dd_IPNum
  AND fca.amt_netvalueheader <> ifnull(fc.amt_netvalueheader,0);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.amt_ExchangeRate = fc.amt_ExchangeRate
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_IPNum =  fc.dd_IPNum
  AND fca.amt_ExchangeRate <> fc.amt_ExchangeRate;

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc 
SET fca.amt_exchangerate_gbl = fc.amt_exchangerate_gbl
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_IPNum = fc.dd_IPNum
  AND fca.amt_exchangerate_gbl <> fc.amt_exchangerate_gbl;

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_chargebacktypesid = ifnull(fc.dim_chargebacktypesid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_chargebacktypesid <> ifnull(fc.dim_chargebacktypesid,1);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_salseorgid = ifnull(fc.dim_salseorgid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_salseorgid <> ifnull(fc.dim_salseorgid,1);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc 
SET fca.dim_distributionchannelid = ifnull(fc.dim_distributionchannelid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_distributionchannelid <> ifnull(fc.dim_distributionchannelid,1);
  
UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_purchaseorgid = ifnull(fc.dim_purchaseorgid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_purchaseorgid <> ifnull(fc.dim_purchaseorgid,1);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_customergroupid = ifnull(fc.dim_customergroupid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_customergroupid <> ifnull(fc.dim_customergroupid,1);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_salesdistrictid = ifnull(fc.dim_salesdistrictid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_salesdistrictid <> ifnull(fc.dim_salesdistrictid,1);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_salesdoctypeid = ifnull(fc.dim_salesdoctypeid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_salesdoctypeid <> ifnull(fc.dim_salesdoctypeid,1);  
  
UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_billingdoctypeid = ifnull(fc.dim_billingdoctypeid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_billingdoctypeid <> ifnull(fc.dim_billingdoctypeid,1);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_customeridSoldTo = ifnull(fc.dim_customeridSoldTo,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_customeridSoldTo <> ifnull(fc.dim_customeridSoldTo,1);


UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_Currencyid = fc.dim_Currencyid 
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_Currencyid <> fc.dim_Currencyid;

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_Currencyid_tra = fc.dim_Currencyid_tra
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_Currencyid_tra <> fc.dim_Currencyid_tra;
  
UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_Currencyid_stat = fc.dim_Currencyid_stat
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_Currencyid_stat <> fc.dim_Currencyid_stat;
    
UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_dateidDocDate = ifnull(fc.dim_dateidDocDate,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_dateidDocDate <> ifnull(fc.dim_dateidDocDate,1);
  
UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_dateidPostingDate = ifnull(fc.dim_dateidPostingDate,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_dateidPostingDate <> ifnull(fc.dim_dateidPostingDate,1);
  
  /* 
	 25.07.2014
	 Add days bucket based on Postingdate and CurrentDate
	 Script depends on PostingDate population
	 Added and 5 measures for each bucket.
  */
	UPDATE fact_chargebackaging fca
	FROM dim_date d1
	set dd_DaysBucket = case
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue between  0 and 30  and dim_dateidPostingDate <> 1 then  '0 - 30 Days'
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue between 31 and 60  and dim_dateidPostingDate <> 1 then '31 - 60 Days'
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue between 61 and 90  and dim_dateidPostingDate <> 1 then '61 - 90 Days'
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue between 91 and 120 and dim_dateidPostingDate <> 1 then '91 - 120 Days'
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue > 120              and dim_dateidPostingDate <> 1	then '120+ Days'
							else 'Not Set' 
						end,
		amt_less30NVH = case
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue between  0 and 30  and dim_dateidPostingDate <> 1 then (fca.amt_AccrualTotalAC - fca.amt_AccrualTotalAR) - (fca.amt_SettlementTotalSI - fca.amt_SettlementTotalRI)
							else 0
						end,		
		amt_3160NVH   = case
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue between 31 and 60  and dim_dateidPostingDate <> 1 then (fca.amt_AccrualTotalAC - fca.amt_AccrualTotalAR) - (fca.amt_SettlementTotalSI - fca.amt_SettlementTotalRI)
							else 0
						end,
		amt_6190NVH   = case
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue between 61 and 90  and dim_dateidPostingDate <> 1 then (fca.amt_AccrualTotalAC - fca.amt_AccrualTotalAR) - (fca.amt_SettlementTotalSI - fca.amt_SettlementTotalRI)
							else 0
						end,
		amt_91120NVH  = case
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue between 91 and 120 and dim_dateidPostingDate <> 1 then (fca.amt_AccrualTotalAC - fca.amt_AccrualTotalAR) - (fca.amt_SettlementTotalSI - fca.amt_SettlementTotalRI)
							else 0
						end,
		amt_great120NVH = case
							when ansidate(LOCAL_TIMESTAMP) - d1.datevalue > 120 and dim_dateidPostingDate <> 1 then (fca.amt_AccrualTotalAC - fca.amt_AccrualTotalAR) - (fca.amt_SettlementTotalSI - fca.amt_SettlementTotalRI)
							else 0
						  end		
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
	where dim_dateidPostingDate = dim_dateid;
	
UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_Companyid = ifnull(fc.dim_Companyid,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_Companyid <> ifnull(fc.dim_Companyid,1);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_CompanyidClearing = ifnull(fc.dim_CompanyidClearing,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_CompanyidClearing <> ifnull(fc.dim_CompanyidClearing,1);
  
UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_DateidHdrCreated = ifnull(fc.dim_DateidHdrCreated,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_DateidHdrCreated <> ifnull(fc.dim_DateidHdrCreated,1);

UPDATE fact_chargebackaging fca
FROM fact_chargebacks fc
SET fca.dim_DateidHdrLastChg = ifnull(fc.dim_DateidHdrLastChg,1)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = fc.dd_ipnum
  AND fca.dim_DateidHdrLastChg <> ifnull(fc.dim_DateidHdrLastChg,1);
	
/* For Accrual Total */

DROP TABLE if exists tmp_AC_measure_001; 
CREATE TABLE tmp_AC_measure_001 AS
SELECT irm_ipcbdfl_ipnum IpNum_AC, sum(irm_ipcbdfl_wrbtr) Wrbtr_AC 
FROM irm_ipcbdfl 
WHERE irm_ipcbdfl_vbtyp_n ='AC' 
GROUP BY irm_ipcbdfl_ipnum;

UPDATE fact_chargebackaging fca
FROM tmp_AC_measure_001 t
SET fca.amt_AccrualTotalAC = ifnull(t.Wrbtr_AC,0)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = t.IpNum_AC;	  
	  
DROP TABLE if exists tmp_AR_measure_001; 
CREATE TABLE tmp_AR_measure_001 AS
SELECT irm_ipcbdfl_ipnum IpNum_AR, sum(irm_ipcbdfl_wrbtr) Wrbtr_AR
FROM  irm_ipcbdfl 
WHERE irm_ipcbdfl_vbtyp_n ='AR' 
GROUP BY irm_ipcbdfl_ipnum;

UPDATE fact_chargebackaging fca
FROM tmp_AR_measure_001 t
SET fca.amt_AccrualTotalAR = ifnull(t.Wrbtr_AR,0)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = t.IpNum_AR;

  
/* For Settlement Total */

DROP TABLE if exists tmp_SI_measure_001; 
CREATE TABLE tmp_SI_measure_001 AS
SELECT irm_ipcbdfl_ipnum IpNum_SI, sum(irm_ipcbdfl_wrbtr) Wrbtr_SI 
FROM irm_ipcbdfl 
WHERE irm_ipcbdfl_vbtyp_n ='SI' 
GROUP BY irm_ipcbdfl_ipnum;

UPDATE fact_chargebackaging fca
FROM tmp_SI_measure_001 t
SET fca.amt_SettlementTotalSI = ifnull(Wrbtr_SI,0)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = t.IpNum_SI;

DROP TABLE if exists tmp_RI_measure_001; 
CREATE TABLE tmp_RI_measure_001 AS
SELECT irm_ipcbdfl_ipnum IpNum_RI, sum(irm_ipcbdfl_wrbtr) Wrbtr_RI
FROM  irm_ipcbdfl 
WHERE irm_ipcbdfl_vbtyp_n ='RI' 
GROUP BY irm_ipcbdfl_ipnum;

UPDATE fact_chargebackaging fca
FROM tmp_RI_measure_001 t
SET fca.amt_SettlementTotalRI = ifnull(t.Wrbtr_RI,0)
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_ipnum = t.IpNum_RI;

/* Aging Posting Date */
DROP TABLE if exists tmp_AgingPostingDate_001; 
CREATE TABLE tmp_AgingPostingDate_001 AS
SELECT distinct irm_ipcbdfl_erdat Erdat, irm_ipcbdfl_ipnum IpNum_PD
FROM irm_ipcbdfl 
WHERE irm_ipcbdfl_vbtyp_n ='SP';

UPDATE fact_chargebackaging fca
FROM tmp_AgingPostingDate_001 t, dim_date dt, dim_company dc
SET fca.dim_DateidAgingPostingDate = dt.dim_dateid
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_IpNum = t.IpNum_PD
  AND fca.dim_companyid = dc.dim_companyid
  AND dt.DateValue = t.Erdat AND dt.CompanyCode = dc.CompanyCode
  AND fca.dim_DateidAgingPostingDate <> dt.dim_dateid;

/* 	04.08.2014 Parking Document Number 
	used count instead of distinct as it works faster.
*/
DROP TABLE IF EXISTS tmp_ParkingDocNumber_001;
CREATE TABLE tmp_ParkingDocNumber_001 AS  /* 2107, 2082 s*/
SELECT count(*) DUMMY, irm_ipcbdfl_ipnum, irm_ipcbdfl_VBELN
FROM irm_ipcbdfl 
WHERE irm_ipcbdfl_vbtyp_n ='SP'
group by irm_ipcbdfl_ipnum, irm_ipcbdfl_VBELN;

UPDATE fact_chargebackaging fca
FROM tmp_ParkingDocNumber_001 t
SET fca.DD_PARKING_DOC_NUMBER = t.irm_ipcbdfl_VBELN
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
  WHERE fca.dd_IpNum = t.irm_ipcbdfl_ipnum; 

/* 23.08.2014
   Add Vendor Dimension 
*/  
update fact_chargebackaging fca
from IRM_IPCBHDR i, LFA1 l, DIM_VENDOR v
set fca.dim_vendorid = v.dim_vendorid
	,fca.dw_update_date = current_timestamp /* Added automatically by update_dw_update_date.pl*/
where     fca.dd_ipnum = i.IRM_IPCBHDR_IPNUM
      and i.IRM_IPCBHDR_PAYER = l.LIFNR
	  and l.LIFNR = v.vendornumber
	  and fca.dim_vendorid <> v.dim_vendorid;  
  
call vectorwise(combine 'fact_chargebackaging');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_chargebackaging;

DROP TABLE if exists tmp_chargebackaging_new_no_001;
DROP TABLE if exists tmp_AC_measure_001; 
DROP TABLE if exists tmp_AR_measure_001; 
DROP TABLE if exists tmp_SI_measure_001; 
DROP TABLE if exists tmp_RI_measure_001; 
DROP TABLE if exists tmp_AgingPostingDate_001;