/* 
#################################################################################################################
Script         : vw_bi_populate_salesrebates_fact.sql
Author         : SVinayan 
Created On     : 22 Apr 2015 
Description    : 
*********************************************Change History**************************************************
Date		By		Version		Desc                      
22Apr2015   	Suchithra   	1.0     	New Script Created 
05May2015	Suchithra	1.1		Removed fileter on depcode in irm_ippraps table
06May2015	Suchithra	1.2		Update stmt for dim_company id updated to consider
						company code from irm_ippraps table				
14May2015	Suchithra	2.0		Main Insert updated to have outer joins
15May2015	Suchithra	3.0		Main Insert updated to have full outer join b/w postings and DFL table
18May2015	Octavian	3.1		Adding Pricing Date and Service Rendered Date
19May2015   	Octavian    	3.2		Adding Line Amount,Line Agreement No,Quantity 
27May2015   	Octavian    	4.0     	Transforming update statements for multiple columns into individual updates per column. 
						Adding 2 more combine statements for fact to free up the PDT memory.
16Jun2015	Suchithra	4.1		Additional Update for Agreement ID						
#################################################################################################################
*/

/* Update Existing Data */

/*IRM_IPCBHDR */

UPDATE  fact_salesrebates
FROM    irm_ipcbhdr
SET     dd_chargebackiptype  = ifnull(irm_ipcbhdr_iptyp,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE   dd_chargebackipdocno = irm_ipcbhdr_ipnum
	AND dd_chargebackiptype  <> ifnull(irm_ipcbhdr_iptyp,'Not Set');

UPDATE  fact_salesrebates
FROM    irm_ipcbhdr
SET     dd_chargebackvendor  = ifnull(irm_ipcbhdr_payer,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE   dd_chargebackipdocno = irm_ipcbhdr_ipnum
	AND dd_chargebackvendor  <> ifnull(irm_ipcbhdr_payer,'Not Set');

UPDATE  fact_salesrebates
FROM    irm_ipcbhdr
SET     dd_chargebackcreatetime = ifnull(irm_ipcbhdr_erzet,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE   dd_chargebackipdocno = irm_ipcbhdr_ipnum
	AND dd_chargebackcreatetime <> ifnull(irm_ipcbhdr_erzet,'Not Set');


/*IPCBITM*/
UPDATE 	fact_salesrebates
FROM 	irm_ipcbitm
SET 	dd_part = ifnull(irm_ipcbitm_matnr,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_chargebackipdocno  = irm_ipcbitm_ipnum
	AND dd_chargebackipdocitmno =  irm_ipcbitm_ipitm
	AND dd_part <> ifnull(irm_ipcbitm_matnr,'Not Set');

UPDATE 	fact_salesrebates
FROM 	irm_ipcbitm
SET 	amt_chargebackamount  = ifnull(irm_ipcbitm_netwr,'0.0000'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_chargebackipdocno  = irm_ipcbitm_ipnum
	AND dd_chargebackipdocitmno =  irm_ipcbitm_ipitm
	AND amt_chargebackamount  <> ifnull(irm_ipcbitm_netwr,'0.0000');
	
UPDATE 	fact_salesrebates
FROM 	irm_ipcbitm
SET 	dd_chargebackagreementno  = ifnull(irm_ipcbitm_knuma_ag,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_chargebackipdocno  = irm_ipcbitm_ipnum
	AND dd_chargebackipdocitmno =  irm_ipcbitm_ipitm
	AND dd_chargebackagreementno  <> ifnull(irm_ipcbitm_knuma_ag,'Not Set');

UPDATE 	fact_salesrebates
FROM 	irm_ipcbitm
SET 	dd_invoiceno  = ifnull(irm_ipcbitm_zzinv,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_chargebackipdocno  = irm_ipcbitm_ipnum
	AND dd_chargebackipdocitmno =  irm_ipcbitm_ipitm
	AND dd_invoiceno  <> ifnull(irm_ipcbitm_zzinv,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_ipcbitm
SET 	dd_invoivelineno  = ifnull(irm_ipcbitm_zzinv_itm,0),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_chargebackipdocno  = irm_ipcbitm_ipnum
	AND dd_chargebackipdocitmno =  irm_ipcbitm_ipitm
	AND dd_invoivelineno  <> ifnull(irm_ipcbitm_zzinv_itm,0);
	

/* IRM_IPPRDFL */
UPDATE	 fact_salesrebates
FROM 	irm_ipcbdfl
SET		dd_precedingcategory  = ifnull(irm_ipcbdfl_vbtyp_v,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE  	dd_parkdocumentno = irm_ipcbdfl_vbeln
	AND	dd_chargebackdocumentnumber =  irm_ipcbdfl_ipnum
	AND dd_subsequentdoccategory  =  irm_ipcbdfl_vbtyp_n
	AND dd_precedingcategory  <> ifnull(irm_ipcbdfl_vbtyp_v,'Not Set');

UPDATE	 fact_salesrebates
FROM 	irm_ipcbdfl
SET		dd_parkdocumentfiscalyear  = ifnull(irm_ipcbdfl_gjahr,0),
		dw_update_date = CURRENT_TIMESTAMP
WHERE  	dd_parkdocumentno = irm_ipcbdfl_vbeln
	AND	dd_chargebackdocumentnumber =  irm_ipcbdfl_ipnum
	AND dd_subsequentdoccategory  =  irm_ipcbdfl_vbtyp_n
	AND dd_parkdocumentfiscalyear  <> ifnull(irm_ipcbdfl_gjahr,0);

UPDATE	 fact_salesrebates
FROM 	irm_ipcbdfl
SET		dd_referancetransaction  = ifnull(irm_ipcbdfl_awtyp,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE  	dd_parkdocumentno = irm_ipcbdfl_vbeln
	AND	dd_chargebackdocumentnumber =  irm_ipcbdfl_ipnum
	AND dd_subsequentdoccategory  =  irm_ipcbdfl_vbtyp_n
	AND dd_referancetransaction <> ifnull(irm_ipcbdfl_awtyp,'Not Set');
	
UPDATE	 fact_salesrebates
FROM 	irm_ipcbdfl
SET		dd_parkdoctime = ifnull(irm_ipcbdfl_erzet,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE  	dd_parkdocumentno = irm_ipcbdfl_vbeln
	AND	dd_chargebackdocumentnumber =  irm_ipcbdfl_ipnum
	AND dd_subsequentdoccategory  =  irm_ipcbdfl_vbtyp_n
	AND dd_parkdoctime <> ifnull(irm_ipcbdfl_erzet,'Not Set');
	

/* IRM_GCRHDR */

UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimchangename  = ifnull(irm_gcrhdr_aenam,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimchangename  <> ifnull(irm_gcrhdr_aenam,'Not Set');

UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_salesdoctype  = ifnull(irm_gcrhdr_auart,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_salesdoctype  <> ifnull(irm_gcrhdr_auart,'Not Set');

UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimpostingstatus  = ifnull(irm_gcrhdr_bstat,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimpostingstatus  <> ifnull(irm_gcrhdr_bstat,'Not Set');

UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_cancellation  = ifnull(irm_gcrhdr_cancd,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_cancellation  <> ifnull(irm_gcrhdr_cancd,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_vendor  = ifnull(irm_gcrhdr_clmvn,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_vendor  <> ifnull(irm_gcrhdr_clmvn,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_postingblock  = ifnull(irm_gcrhdr_crpbk,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_postingblock  <> ifnull(irm_gcrhdr_crpbk,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimheaderstage  = ifnull(irm_gcrhdr_crstage,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimheaderstage  <> ifnull(irm_gcrhdr_crstage,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimtype  = ifnull(irm_gcrhdr_crtyp,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimtype  <> ifnull(irm_gcrhdr_crtyp,'Not Set');

UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimcreatedby  = ifnull(irm_gcrhdr_ernam,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimcreatedby  <> ifnull(irm_gcrhdr_ernam,'Not Set');

UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_userstatus  = ifnull(irm_gcrhdr_estat,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_userstatus  <> ifnull(irm_gcrhdr_estat,'Not Set');

UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_paymentreference  = ifnull(irm_gcrhdr_kidno,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_paymentreference  <> ifnull(irm_gcrhdr_kidno,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		amt_submittedamount  = ifnull(irm_gcrhdr_subamt,'0.0000'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND amt_submittedamount  <> ifnull(irm_gcrhdr_subamt,'0.0000');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_submittedcurency  = ifnull(irm_gcrhdr_subcur,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_submittedcurency  <> ifnull(irm_gcrhdr_subcur,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_endvalidtimespan  = ifnull(irm_gcrhdr_toprd,0),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_endvalidtimespan  <> ifnull(irm_gcrhdr_toprd,0);
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimtrackingno  = ifnull(irm_gcrhdr_tracknum,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimtrackingno  <> ifnull(irm_gcrhdr_tracknum,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_currentuserstatus  = ifnull(irm_gcrhdr_ustat,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_currentuserstatus  <> ifnull(irm_gcrhdr_ustat,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimdocnoforrefdoc  = ifnull(irm_gcrhdr_vgbel,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimdocnoforrefdoc  <> ifnull(irm_gcrhdr_vgbel,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimrefdocno  = ifnull(irm_gcrhdr_xblnr,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimrefdocno  <> ifnull(irm_gcrhdr_xblnr,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimtimestamp = ifnull(irm_gcrhdr_aezet,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimtimestamp <> ifnull(irm_gcrhdr_aezet,'Not Set');
	
UPDATE 	fact_salesrebates
FROM 	irm_gcrhdr
SET		dd_claimcreatetimestamp = ifnull(irm_gcrhdr_erzet,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_claimnumber  = irm_gcrhdr_vbeln
	AND dd_claimcreatetimestamp <> ifnull(irm_gcrhdr_erzet,'Not Set');

call vectorwise (combine 'fact_salesrebates');
/*IRM_IPPRAPS */
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_reforgunits  = ifnull(irm_ippraps_aworg,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_reforgunits <> ifnull(irm_ippraps_aworg,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_refdocno  = ifnull(irm_ippraps_awref,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_refdocno  <> ifnull(irm_ippraps_awref,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_reftransactioncd  = ifnull(irm_ippraps_awtyp,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_reftransactioncd  <> ifnull(irm_ippraps_awtyp,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_companycode  = ifnull(irm_ippraps_bukrs,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_companycode  <> ifnull(irm_ippraps_bukrs,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_purchrebateevaluationfromperiod  = ifnull(irm_ippraps_efprd,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_purchrebateevaluationfromperiod  <> ifnull(irm_ippraps_efprd,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_purchrebatecreatedby  = ifnull(irm_ippraps_ernam,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_purchrebatecreatedby  <> ifnull(irm_ippraps_ernam,'Not Set');

UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_fiscalyear  = ifnull(irm_ippraps_gjahr,0),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_fiscalyear  <> ifnull(irm_ippraps_gjahr,0);
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_lowlvlparticipantpost  = ifnull(irm_ippraps_lppost,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_lowlvlparticipantpost  <> ifnull(irm_ippraps_lppost,'Not Set');

UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		amt_netvalue  = ifnull(irm_ippraps_netwr,'0.0000'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND amt_netvalue  <> ifnull(irm_ippraps_netwr,'0.0000');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_currencykeyN = ifnull(irm_ippraps_nwaers,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_currencykeyN <> ifnull(irm_ippraps_nwaers,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_calculationrun  = ifnull(irm_ippraps_pcrnum,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_calculationrun  <> ifnull(irm_ippraps_pcrnum,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_evaluationtoperiod  = ifnull(irm_ippraps_perid,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_evaluationtoperiod  <> ifnull(irm_ippraps_perid,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_reasoncodeforpayments  = ifnull(irm_ippraps_rstgr,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_reasoncodeforpayments  <> ifnull(irm_ippraps_rstgr,'Not Set');

UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_settlementadjustmentcode  = ifnull(irm_ippraps_sacod,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_settlementadjustmentcode  <> ifnull(irm_ippraps_sacod,'Not Set');

UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_settlementpartner  = ifnull(irm_ippraps_sktonr,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_settlementpartner  <> ifnull(irm_ippraps_sktonr,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_settlementpartnertype  = ifnull(irm_ippraps_snrart,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_settlementpartnertype  <> ifnull(irm_ippraps_snrart,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_currencykey  = ifnull(irm_ippraps_waers,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_currencykey  <> ifnull(irm_ippraps_waers,'Not Set');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		amt_salesrebateamt  = ifnull(irm_ippraps_wrbtr,'0.0000'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND amt_salesrebateamt  <> ifnull(irm_ippraps_wrbtr,'0.0000');
	
UPDATE	fact_Salesrebates 
FROM	irm_ippraps
SET		dd_purchrebaterecordtimestamp = ifnull(irm_ippraps_erzet,'Not Set'),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	dd_participationnumber = irm_ippraps_panum
	AND dd_accrualdocno = irm_ippraps_vbeln
	AND dd_deploymentcode = irm_ippraps_depcode
	AND dd_participationtype = irm_ippraps_nrart
	AND dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
	AND dd_agreementno = irm_ippraps_knuma_ag
	AND dd_purchrebaterecordtimestamp <> ifnull(irm_ippraps_erzet,'Not Set');

call vectorwise (combine 'fact_salesrebates');

/* Insert New Unique Rows into the Fact */
delete from NUMBER_FOUNTAIN where table_name = 'fact_salesrebates';

INSERT INTO NUMBER_FOUNTAIN
SELECT 'fact_salesrebates',ifnull(max(fact_salesrebatesid),0)
FROM fact_salesrebates;
insert into fact_salesrebates
(
	fact_salesrebatesid,

/*IRM_IPPRAPS*/
	dd_reforgunits ,
	dd_refdocno ,
	dd_reftransactioncd ,
	dd_companycode ,
	dd_deploymentcode,
	dd_purchrebateevaluationfromperiod ,
	dd_purchrebatecreatedby ,
	dd_accrualdocno,
	dd_subsequentdocumentcategory,
	
	dd_fiscalyear ,
	dd_agreementno ,
	dd_lowlvlparticipantpost ,
	amt_netvalue ,
	dd_currencykeyN,
	dd_calculationrun ,
	dd_participationtype,
	dd_participationnumber,
	
	dd_evaluationtoperiod ,
	dd_reasoncodeforpayments ,
	dd_settlementadjustmentcode ,
	dd_settlementpartner ,
	dd_settlementpartnertype ,
	dd_currencykey ,
	amt_salesrebateamt ,
	dd_purchrebaterecordtimestamp,
	
/*IPPRDFL*/
	dd_parkdocumentno,
	dd_chargebackdocumentnumber,
	dd_precedingcategory ,
	dd_subsequentdoccategory ,
	dd_parkdocumentfiscalyear ,
	dd_referancetransaction ,
	dd_parkdoctime,
	
	
/*IPCBHDR*/
	dd_chargebackipdocno ,
	dd_chargebackiptype ,
	dd_chargebackvendor ,
	dd_chargebackcreatetime,
	
/*IPCBITM*/
	dd_chargebackipdocitmno,
	dd_part ,
	amt_chargebackamount ,
	dd_chargebackagreementno ,
	dd_invoiceno ,
	dd_invoivelineno ,
	
/*IPPRASP*/
	dim_rebateagreementsid,
			
/*GCRHDR*/
	dd_claimnumber ,
	dd_claimchangename ,
	dd_salesdoctype ,
	dd_claimpostingstatus ,
	dd_cancellation ,
	dd_vendor ,
	dd_postingblock ,
	dd_claimheaderstage ,
	dd_claimtype ,
	dd_claimcreatedby ,
	dd_userstatus ,
	-- dd_claimfromperiod,
	dd_paymentreference ,
	amt_submittedamount ,
	dd_submittedcurency ,
	dd_endvalidtimespan ,
	dd_claimtrackingno ,
	dd_currentuserstatus ,
	dd_claimdocnoforrefdoc ,
	dd_claimrefdocno ,
	dd_claimtimestamp,
	dd_claimcreatetimestamp
)
select
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_salesrebates') + row_number() over() as fact_salesrebatesid,
	/* IRM_IPPRAPS*/
	ifnull(irm_ippraps_aworg,'Not Set'),
	ifnull(irm_ippraps_awref,'Not Set'),
	ifnull(irm_ippraps_awtyp,'Not Set'),
	ifnull(irm_ippraps_bukrs,'Not Set'),
	ifnull(irm_ippraps_depcode,'Not Set'),
	ifnull(irm_ippraps_efprd,'Not Set'),
	ifnull(irm_ippraps_ernam,'Not Set'),
	ifnull(irm_ippraps_vbeln,'Not Set'),
	ifnull(irm_ippraps_vbtyp_n,'Not Set'),

	ifnull(irm_ippraps_gjahr,0),
	ifnull(irm_ippraps_knuma_ag,'Not Set'),
	ifnull(irm_ippraps_lppost,'Not Set'),
	ifnull(irm_ippraps_netwr,'0.0000'),
	ifnull(irm_ippraps_nwaers,'Not Set'),
	ifnull(irm_ippraps_pcrnum,'Not Set'),
	ifnull(irm_ippraps_nrart,'Not Set'),
	ifnull(irm_ippraps_panum,'Not Set'),

	ifnull(irm_ippraps_perid,'Not Set'),
	ifnull(irm_ippraps_rstgr,'Not Set'),
	ifnull(irm_ippraps_sacod,'Not Set'),
	ifnull(irm_ippraps_sktonr,'Not Set'),
	ifnull(irm_ippraps_snrart,'Not Set'),
	ifnull(irm_ippraps_waers,'Not Set'),
	ifnull(irm_ippraps_wrbtr,'0.0000'),
	ifnull(irm_ippraps_erzet,'Not Set'),

	/* IRM_IPCBDFL*/
	ifnull(irm_ipcbdfl_vbeln,'Not Set'),
	ifnull(irm_ipcbdfl_ipnum,'Not Set'),
	ifnull(irm_ipcbdfl_vbtyp_v,'Not Set'),
	ifnull(irm_ipcbdfl_vbtyp_n,'Not Set'),
	ifnull(irm_ipcbdfl_gjahr,0),
	ifnull(irm_ipcbdfl_awtyp,'Not Set'),
	ifnull(irm_ipcbdfl_erzet,'Not Set'),


	/* IRM_IPCBHDR*/
	ifnull(irm_ipcbhdr_ipnum,'Not Set'),
	ifnull(irm_ipcbhdr_iptyp,'Not Set'),
	ifnull(irm_ipcbhdr_payer,'Not Set'),
	ifnull(irm_ipcbhdr_erzet,'Not Set'),

	/*IRM_IPCBITM*/
	ifnull(irm_ipcbitm_ipitm,0),
	ifnull(irm_ipcbitm_matnr,'Not Set'),
	ifnull(irm_ipcbitm_netwr,'0.0000'),
	ifnull(irm_ipcbitm_knuma_ag,'Not Set'),
	ifnull(irm_ipcbitm_zzinv,'Not Set'),
	ifnull(irm_ipcbitm_zzinv_itm,0),

	/*IRM_IPPRASP*/
	ifnull((select dim_rebateagreementsid from dim_rebateagreements d
			where i.irm_ipcbitm_knuma_ag = d.agreementnum 
			and d.rebatebasis = 'SALE'),1),
			
	/* IRM_GCRHDR*/
	ifnull(irm_gcrhdr_vbeln,'Not Set'),
	ifnull(irm_gcrhdr_aenam,'Not Set'),
	ifnull(irm_gcrhdr_auart,'Not Set'),
	ifnull(irm_gcrhdr_bstat,'Not Set'),
	ifnull(irm_gcrhdr_cancd,'Not Set'),
	ifnull(irm_gcrhdr_clmvn,'Not Set'),
	ifnull(irm_gcrhdr_crpbk,'Not Set'),
	ifnull(irm_gcrhdr_crstage,'Not Set'),
	ifnull(irm_gcrhdr_crtyp,'Not Set'),
	ifnull(irm_gcrhdr_ernam,'Not Set'),
	ifnull(irm_gcrhdr_estat,'Not Set'),
	-- ifnull(irm_gcrhdr_frprd,'Not Set'),
	ifnull(irm_gcrhdr_kidno,'Not Set'),
	ifnull(irm_gcrhdr_subamt,'0.0000'),
	ifnull(irm_gcrhdr_subcur,'Not Set'),
	ifnull(irm_gcrhdr_toprd,0),
	ifnull(irm_gcrhdr_tracknum,'Not Set'),
	ifnull(irm_gcrhdr_ustat,'Not Set'),
	ifnull(irm_gcrhdr_vgbel,'Not Set'),
	ifnull(irm_gcrhdr_xblnr,'Not Set'),
	ifnull(irm_gcrhdr_aezet,'Not Set'),
	ifnull(irm_gcrhdr_erzet,'Not Set')
FROM	irm_ippraps p
left join irm_ipcbdfl d
	ON irm_ipcbdfl_vbeln = irm_ippraps_vbeln 
	and irm_ipcbdfl_gjahr = irm_ippraps_gjahr
	and irm_ipcbdfl_bukrs = irm_ippraps_bukrs /* ippraps, ipprdfl */
	and irm_ipcbdfl_vbtyp_v = 'IP'
left join irm_ipcbitm i
	ON irm_ipcbdfl_ipnum = irm_ipcbitm_ipnum 
	and irm_ipcbdfl_ipitm = irm_ipcbitm_ipitm
left join irm_ipcbhdr h
	on irm_ipcbhdr_ipnum = irm_ipcbitm_ipnum
left join irm_ipprasp a
	on irm_ippraps_knuma_ag = irm_ipprasp_knuma_ag
	and irm_ipprasp_objec = 'SALE'
left join irm_gcrhdr g
	on irm_ippraps_panum = substr(irm_gcrhdr_tracknum,12)
where ifnull(irm_ipcbdfl_vbtyp_n,'Not Set') in ('C1','C2','C3','C4','C5','C6','C7','C8','Not Set') 
and irm_ippraps_Depcode in ('Z1PRD714','Z1PRD715','Z1PRD716','Z1PRD717','Z1PRD720','Z1PRD723')
and not exists (Select 1 from fact_salesrebates
					where  dd_accrualdocno = ifnull(irm_ippraps_vbeln,'Not Set')
					and dd_participationnumber = ifnull(irm_ippraps_panum,'Not Set')
					and dd_agreementno = ifnull(irm_ippraps_knuma_ag,'Not Set')
					and dd_deploymentcode = ifnull(irm_ippraps_depcode,'Not Set')
					and dd_participationtype = ifnull(irm_ippraps_nrart,'Not Set')
					and dd_subsequentdocumentcategory = ifnull(irm_ippraps_vbtyp_n,'Not Set')
					and dd_chargebackipdocno =  ifnull(irm_ipcbdfl_ipnum,'Not Set')
					and dd_subsequentdoccategory = ifnull(irm_ipcbdfl_vbtyp_n,'Not Set')
					and dd_parkdocumentno = ifnull(irm_ipcbdfl_vbeln,'Not Set')
					and dd_chargebackipdocitmno	= ifnull(irm_ipcbdfl_ipitm,0)
					and dd_chargebackdocumentnumber = ifnull(irm_ipcbdfl_ipnum,'Not Set')
					and dd_claimnumber = ifnull(irm_gcrhdr_vbeln,'Not Set')
					);	
					
/* Plant and Company Code*/
UPDATE fact_Salesrebates f
FROM irm_ipcbitm i, dim_plant pl
SET f.dim_plantid = pl.dim_plantid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_chargebackipdocno = i.irm_ipcbitm_ipnum 
AND f.dd_chargebackipdocitmno = i.irm_ipcbitm_ipitm
AND i.irm_ipcbitm_werks = pl.plantcode
AND f.dim_plantid <> pl.dim_plantid;

/* Company code from ipprasp table to be used for populating dimension id*/ 
UPDATE fact_salesrebates f
FROM dim_company c
SET f.dim_companyid = c.dim_companyid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_companycode = c.companycode
and f.dim_companyid <> c.dim_companyid;
					
/* Sales information from billing*/
UPDATE fact_salesrebates sf
FROM fact_billing fb,irm_ipcbhdr h, irm_ipcbitm i
SET sf.dd_sodocno = fb.dd_salesdocno,
	dw_update_date = CURRENT_TIMESTAMP
where sf.dd_chargebackipdocno = h.irm_ipcbhdr_ipnum
and h.irm_ipcbhdr_ipnum = i.irm_ipcbitm_ipnum
and sf.dd_chargebackipdocitmno = i.irm_ipcbitm_ipitm
and fb.dd_billing_no = h.irm_ipcbhdr_vbeln
and fb.dd_billing_item_no = i.irm_ipcbitm_ipitm 
and sf.dd_sodocno <> fb.dd_salesdocno;

UPDATE fact_salesrebates sf
FROM fact_billing fb,irm_ipcbhdr h, irm_ipcbitm i
SET sf.dd_sodocitmno = fb.dd_salesitemno,
	dw_update_date = CURRENT_TIMESTAMP
where sf.dd_chargebackipdocno = h.irm_ipcbhdr_ipnum
and h.irm_ipcbhdr_ipnum = i.irm_ipcbitm_ipnum
and sf.dd_chargebackipdocitmno = i.irm_ipcbitm_ipitm
and fb.dd_billing_no = h.irm_ipcbhdr_vbeln
and fb.dd_billing_item_no = i.irm_ipcbitm_ipitm 
and sf.dd_sodocitmno <> fb.dd_salesitemno;

UPDATE fact_salesrebates sf
FROM fact_billing fb,irm_ipcbhdr h, irm_ipcbitm i
SET sf.dim_partid = fb.dim_partid,
	dw_update_date = CURRENT_TIMESTAMP
where sf.dd_chargebackipdocno = h.irm_ipcbhdr_ipnum
and h.irm_ipcbhdr_ipnum = i.irm_ipcbitm_ipnum
and sf.dd_chargebackipdocitmno = i.irm_ipcbitm_ipitm
and fb.dd_billing_no = h.irm_ipcbhdr_vbeln
and fb.dd_billing_item_no = i.irm_ipcbitm_ipitm 
and sf.dim_partid <> fb.dim_partid;


/* Vendor information from Vendor Dimension*/
UPDATE fact_salesrebates f
FROM dim_vendor v
set f.dim_vendorid = v.dim_vendorid,
	dw_update_date = CURRENT_TIMESTAMP
where f.dd_vendor = v.vendornumber
and  f.dim_vendorid <> v.dim_vendorid;

/* 16Jun2015 - to Update ReabtesAgreement deatils incase Agreement Number is not set */
UPDATE fact_Salesrebates f
from dim_rebateagreements d
set f.dim_rebateagreementsid = ifnull(d.dim_rebateagreementsid,1),
	dw_update_date = CURRENT_TIMESTAMP
where  d.agreementnum = case when f.dd_agreementno = 'Not Set' and f.dd_chargebackagreementno <> 'Not Set' then f.dd_chargebackagreementno
							when f.dd_agreementno <> 'Not Set' and f.dd_chargebackagreementno = 'Not Set' then f.dd_agreementno
						end
and f.dim_rebateagreementsid = 1
and d.rebatebasis = 'SALE'	
and f.dim_rebateagreementsid <> ifnull(d.dim_rebateagreementsid,1);


/* Date Fields */


/*dim_dateidpurchrebateposting = irm_ippraps_budat*/
UPDATE fact_salesrebates f
FROM irm_ippraps p, dim_date dt
SET     dim_dateidpurchrebateposting = dt.dim_dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERE dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
and p.irm_ippraps_budat =dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidpurchrebateposting <> dt.dim_dateid;

/*dim_dateidpurchrebateevalfrom = irm_ippraps_efdat*/
UPDATE fact_salesrebates f
FROM irm_ippraps p, dim_date dt
SET     dim_dateidpurchrebateevalfrom = dt.dim_dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERE dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
and p.irm_ippraps_efdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidpurchrebateevalfrom <> dt.dim_dateid;


/*dim_dateidpurchrebatecreate = irm_ippraps_erdat*/
UPDATE fact_salesrebates f
FROM irm_ippraps p, dim_date dt
SET dim_dateidpurchrebatecreate = dt.dim_dateid,
	dw_update_date = CURRENT_TIMESTAMP
WHERE dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
and p.irm_ippraps_erdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidpurchrebatecreate <> dt.dim_dateid;

/*dim_dateidpurchrebateevalto = irm_ippraps_etdat*/
UPDATE fact_salesrebates f
FROM irm_ippraps p, dim_date dt
SET     dim_dateidpurchrebateevalto = dt.dim_dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERE dd_deploymentcode = irm_ippraps_depcode
and dd_participationtype = irm_ippraps_nrart
and dd_participationnumber = irm_ippraps_panum
and dd_accrualdocno = irm_ippraps_vbeln
and dd_subsequentdocumentcategory = irm_ippraps_vbtyp_n
and p.irm_ippraps_etdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidpurchrebateevalto <> dt.dim_dateid;

call vectorwise (combine 'fact_salesrebates');



UPDATE fact_salesrebates f
FROM irm_ipcbdfl p, dim_date dt
SET     dim_dateidparkdoccreate = dt.dim_dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_chargebackipdocno = p.irm_ipcbdfl_ipnum
and f.dd_chargebackdocumentnumber = p.irm_ipcbdfl_ipnum
and f.dd_chargebackipdocitmno = p.irm_ipcbdfl_ipitm
and dd_parkdocumentno = p.irm_ipcbdfl_vbeln
and dd_companycode = p.irm_ipcbdfl_bukrs
and irm_ipcbdfl_erdat = dt.datevalue
and dt.companycode = p.irm_ipcbdfl_bukrs
and dim_dateidparkdoccreate <> dt.dim_dateid;

-- dim_dateidchargebackposting = irm_ipcbhdr_budat      
UPDATE fact_Salesrebates f
FROM    irm_ipcbhdr h, dim_Date dt
SET
        dim_dateidchargebackposting      = dt.dim_Dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERe
f.dd_chargebackipdocno = h.irm_ipcbhdr_ipnum
and dt.datevalue = irm_ipcbhdr_budat
and f.dd_companycode = dt.companycode
and dim_dateidchargebackposting <> dt.dim_Dateid; 

-- dim_dateidchargebackcreate = irm_ipcbhdr_erdat
UPDATE fact_Salesrebates f
FROM    irm_ipcbhdr h, dim_Date dt
SET
    dim_dateidchargebackcreate       = dt.dim_Dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERe
f.dd_chargebackipdocno = h.irm_ipcbhdr_ipnum
and dt.datevalue = irm_ipcbhdr_erdat
and f.dd_companycode = dt.companycode
and dim_dateidchargebackcreate <> dt.dim_Dateid;

UPDATE fact_Salesrebates f
FROM irm_gcrhdr g, dim_Date dt
SET
        dim_dateidclaimchange = dt.dim_Dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERE
f.dd_claimnumber = g.irm_gcrhdr_vbeln
and g.irm_gcrhdr_aedat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidclaimchange <> dt.dim_Dateid;

UPDATE fact_Salesrebates f
FROM irm_gcrhdr g, dim_Date dt
SET
        dim_dateidclaimcreate = dt.dim_Dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERE
f.dd_claimnumber = g.irm_gcrhdr_vbeln
and g.irm_gcrhdr_erdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidclaimcreate <> dt.dim_Dateid;

UPDATE fact_Salesrebates f
FROM irm_gcrhdr g, dim_Date dt
SET
        dim_dateidclaimfrom = dt.dim_Dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERE
f.dd_claimnumber = g.irm_gcrhdr_vbeln
and g.irm_gcrhdr_frdat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidclaimfrom <> dt.dim_Dateid;

UPDATE fact_Salesrebates f
FROM irm_gcrhdr g, dim_Date dt
SET
        dim_dateidclaimto = dt.dim_Dateid,
		dw_update_date = CURRENT_TIMESTAMP
WHERE
f.dd_claimnumber = g.irm_gcrhdr_vbeln
and g.irm_gcrhdr_todat = dt.datevalue
and f.dd_companycode = dt.companycode
and dim_dateidclaimfrom <> dt.dim_Dateid;

/* Derived Fields */
UPDATE fact_salesrebates
SET amt_rebateamtpaid = ifnull((CASE 
							WHEN dd_subsequentdocumentcategory in ('AR','SI','SE') THEN amt_salesrebateamt*-1
							ELSE amt_salesrebateamt
						END),'0.0000'),
	dw_update_date = CURRENT_TIMESTAMP
WHERE  ifnull(amt_rebateamtpaid,'0.0000') <> ifnull((CASE 
							WHEN dd_subsequentdocumentcategory in ('AR','SI','SE') THEN amt_salesrebateamt*-1
							ELSE amt_salesrebateamt
						END),'0.0000');
						
						
UPDATE fact_Salesrebates f
FROM dim_date fdt, dim_date tdt
SET dd_participationperiod = ifnull(tdt.datevalue - fdt.datevalue,0),
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dim_dateidpurchrebateevalto = tdt.dim_dateid
and f.dim_dateidpurchrebateevalfrom = fdt.dim_Dateid
and dd_participationperiod <> ifnull(tdt.datevalue - fdt.datevalue,0);

/* 18 May : New fields to be added */	
UPDATE  fact_Salesrebates f
FROM    IRM_IPCBITM a, IRM_IPCBHDR b, dim_Date dt, dim_plant pl
SET
        dim_dateidprice = ifnull(dt.dim_Dateid,1),
		dw_update_date = CURRENT_TIMESTAMP
WHERE
		f.dd_chargebackipdocno = a.IRM_IPCBITM_IPNUM AND f.dd_chargebackipdocitmno = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND dt.DateValue = a.IRM_IPCBITM_PRSDT AND dt.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_plantid = pl.dim_plantid
	AND dt.CompanyCode = pl.CompanyCode
	AND f.dim_dateidprice <> ifnull(dt.dim_dateid,1); 
	
UPDATE  fact_Salesrebates f
FROM    IRM_IPCBITM a, IRM_IPCBHDR b, dim_Date dt, dim_plant pl
SET
        dim_dateidservicerend = ifnull(dt.dim_Dateid,1),
		dw_update_date = CURRENT_TIMESTAMP
WHERE
		f.dd_chargebackipdocno = a.IRM_IPCBITM_IPNUM AND f.dd_chargebackipdocitmno = a.IRM_IPCBITM_IPITM
	AND a.IRM_IPCBITM_IPNUM = b.IRM_IPCBHDR_IPNUM
	AND dt.DateValue = a.IRM_IPCBITM_FBUDA AND dt.CompanyCode = b.IRM_IPCBHDR_BUKRS
	AND f.dim_plantid = pl.dim_plantid
	AND dt.CompanyCode = pl.CompanyCode
	AND f.dim_dateidservicerend <> ifnull(dt.dim_dateid,1); 
	
/* 19 May : New fields to be added */
UPDATE 	fact_Salesrebates f
FROM 	IRM_IPCBITM a
SET  	f.amt_line = ifnull(a.IRM_IPCBITM_KZWI1,0),
		dw_update_date = CURRENT_TIMESTAMP
WHERE 	f.dd_chargebackipdocno = a.IRM_IPCBITM_IPNUM AND f.dd_chargebackipdocitmno = a.IRM_IPCBITM_IPITM
	AND f.amt_line <> ifnull(a.IRM_IPCBITM_KZWI1,0);
	
/*UPDATE 	fact_Salesrebates f
FROM 	IRM_IPCBITM itm, dim_agreements d
SET     f.dim_agreementsid = ifnull(d.dim_agreementsid,1)
WHERE   f.dd_chargebackipdocno = itm.IRM_IPCBITM_IPNUM AND f.dd_chargebackipdocitmno = itm.IRM_IPCBITM_IPITM
	AND d.Agreement = ifnull(itm.IRM_IPCBITM_KNUMA_AG,'Not Set')
	AND f.dim_agreementsid <> ifnull(d.dim_agreementsid,1)*/ /* Agreement Num from IPCBITM already populated dd_chargebackagreementno */
	
UPDATE   fact_Salesrebates f
FROM     IRM_IPCBITM itm
SET      f.ct_quantity = ifnull(itm.IRM_IPCBITM_KWMENG,0),
		 dw_update_date = CURRENT_TIMESTAMP
WHERE    f.dd_chargebackipdocno = itm.IRM_IPCBITM_IPNUM AND f.dd_chargebackipdocitmno = itm.IRM_IPCBITM_IPITM
	AND  f.ct_quantity <> ifnull(itm.IRM_IPCBITM_KWMENG,0);
	
/* 20May2015 : DCIND field */
UPDATE fact_salesrebates f
FROM irm_ipcbhdr h
SET f.dd_dcindhdr = ifnull(h.irm_ipcbhdr_dcind,'Not Set'),
	dw_update_date = CURRENT_TIMESTAMP
WHERE f.dd_chargebackipdocno = h.IRM_IPCBHDR_IPNUM
AND f.dd_dcindhdr <> ifnull(h.irm_ipcbhdr_dcind,'Not Set');		

call vectorwise (combine 'fact_salesrebates');
