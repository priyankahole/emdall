
DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

drop table if exists tmp_mtltra_sales_001;
create table tmp_mtltra_sales_001 as
select p.Dim_PartSalesid, 
	p.PartNumber, 
	p.PartDescription, 
	p.SalesOrgCode, 
	p.DistributionChannelCode,
	co.Dim_Companyid, 
	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = co.Currency),1) Dim_CurrencyId,

	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = co.Currency),1) Dim_CurrencyId_TRA,
	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = pGlobalCurrency),1) Dim_CurrencyId_GBL,
        1 amt_Exchangerate,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = co.Currency and z.fact_script_name = 'bi_populate_materialtransaction_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)amt_Exchangerate_GBL,
	so.CompanyCode,
	so.dim_salesorgid,
	ph.Level1Desc ProductLine,
	ph.Level2Desc ProductType,
	ph.Level3Desc ProductClass
from dim_partsales p 
	inner join dim_salesorg so on p.SalesOrgCode = so.SalesOrgCode
	inner join dim_company co on so.CompanyCode = co.CompanyCode
	inner join dim_ProductHierarchy ph on ph.ProductHierarchy = p.ProductHierarchy,
	tmp_GlobalCurr_001;

drop table if exists tmp_mtltra_001;
create table tmp_mtltra_001 as
select p.Dim_Partid, 
	p.PartNumber, 
	p.PartDescription, 
	p.Plant, 
	pl.ValuationArea, 
	pl.CompanyCode, 
	pl.Dim_Plantid, 
	co.Dim_Companyid,
	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = co.Currency),1) Dim_CurrencyId,

	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = co.Currency),1) Dim_CurrencyId_TRA,
	ifnull((SELECT dim_CurrencyId FROM
		 Dim_Currency cur WHERE cur.currencycode = pGlobalCurrency),1) Dim_CurrencyId_GBL,
        1 amt_Exchangerate,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = co.Currency and z.fact_script_name = 'bi_populate_materialtransaction_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)amt_Exchangerate_GBL,
	ph.Level1Desc ProductLine,
	ph.Level2Desc ProductType,
	ph.Level3Desc ProductClass
from dim_part p 
	inner join dim_plant pl on p.plant = pl.PlantCode
	inner join dim_company co on pl.CompanyCode = co.CompanyCode
	inner join dim_ProductHierarchy ph on ph.ProductHierarchy = p.ProductHierarchy,
	tmp_GlobalCurr_001;

drop table if exists tmp_eban_001;
create table tmp_eban_001 as
select EBAN_MATNR,
	EBAN_WERKS,
	EBAN_BADAT,
	EBAN_BANFN,
	EBAN_BNFPO,
	EBAN_INFNR,
	EBAN_LIFNR,
	EBAN_LOEKZ,
	row_number() over(partition by EBAN_MATNR, EBAN_WERKS, EBAN_LIFNR order by EBAN_BADAT, EBAN_LOEKZ DESC, EBAN_BANFN, EBAN_BNFPO) PRSeqNo
from EBAN
where EBAN_BADAT > CURRENT_DATE;

drop table if exists tmp_KONH_KONP_001;
create table tmp_KONH_KONP_001 as 
select KONH_KNUMH,
	KONH_ERDAT,
	A901_DATAB KONH_DATAB,
	A901_DATBI KONH_DATBI,
	KONH_KOSRT,
	KONP_KOPOS,
	KONP_KBETR,
	A901_MATNR,
	A901_LIFNR,
	A901_BUKRS,
	A901_KNUMH,
	A901_KSCHL,
	row_number() over(PARTITION BY KONH_KNUMH ORDER BY KONH_DATAB desc, KONP_KOPOS desc) KONH_KONP_ROWNUM
from KONH_KONP inner join A901 on A901_KNUMH = KONH_KNUMH;

drop table if exists tmp_matl_vendor_001;
create table tmp_matl_vendor_001 as
select distinct eina_matnr mtlvend_matnr, eina_lifnr mtlvend_lifnr, e.werks mtlvend_plant, pl.CompanyCode mtlvend_copmanycode
from EINE e inner join dim_plant pl on e.werks = pl.PlantCode;

insert into tmp_matl_vendor_001
select distinct A901_MATNR, A901_LIFNR, pl.PlantCode mtlvend_plant, pl.CompanyCode mtlvend_copmanycode
from A901 inner join dim_plant pl on pl.CompanyCode = A901_BUKRS
where not exists (select 1 from EINE e where e.eina_matnr = A901_MATNR and e.eina_lifnr = A901_LIFNR
				and e.werks = pl.PlantCode);

drop table if exists tmp_mbew_001;
create table tmp_mbew_001 as
select MATNR,
	BWKEY,
	BWTAR,
	STPRS,
	row_number() over(PARTITION BY MATNR,BWKEY,BWTAR ORDER BY LFGJA desc, LFMON desc) MBEW_ROWNUM
from MBEW
where BWTAR is not null;

\i /db/schema_migration/bin/wrapper_optimizedb.sh MBEW;

\i /db/schema_migration/bin/wrapper_optimizedb.sh EINE;

\i /db/schema_migration/bin/wrapper_optimizedb.sh TVKWZ;

\i /db/schema_migration/bin/wrapper_optimizedb.sh MEAN;


\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_mtltra_sales_001;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_mtltra_001;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_matl_vendor_001;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_eban_001;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_KONH_KONP_001;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_mbew_001;


/*** Part in Plant and Sales both ***/
drop table if exists aggsubqry_001;
Create table aggsubqry_001 as Select MATNR,BWKEY,BWTAR, max(MBEW_LAEPR) maxMBEW_LAEPR from MBEW group by  MATNR,BWKEY,BWTAR;
drop table if exists tmp_fact_mtltra_001;
create table tmp_fact_mtltra_001 as
select a.PartNumber, 
	b.PartDescription, 
	a.Dim_Companyid SalesCompanyid, 
	a.CompanyCode SalesCompanyCode,
	a.Dim_PartSalesid, 
	a.SalesOrgCode, 
	a.dim_salesorgid,
	a.DistributionChannelCode,
	b.Dim_Partid, 
	b.Plant, 
	mn.MEAN_EAN11 UPC, 
	b.ValuationArea, 
	c.BWTAR ValuationType, 
	c.STPRS Cost,
	(select maxMBEW_LAEPR from aggsubqry_001 k1 
	where k1.MATNR = b.PartNumber 
		and k1.BWKEY = b.ValuationArea  
		and k1.BWTAR = c.BWTAR) LastCostingDt,
	b.Dim_Plantid, 
	b.Dim_Companyid PartCompanyid, 
	b.CompanyCode PartCompanyCode,
	e.EBAN_BADAT NextPurchDt,
	e.EBAN_BANFN PRNumber,
	mv.mtlvend_lifnr VendorNumber,
	ir.INFNR InfoRecNumber,
	ir.EINE_ERDAT PIRCreated,
	k.KONP_KBETR ConditionAmt,
	k.KONH_DATAB CondValidFrom,
	k.KONH_DATBI CondValidTo,
	k.KONH_KOSRT QuoteType,
	b.ProductLine,
	b.ProductType,
	b.ProductClass,
	k.A901_KSCHL ConditionType,
       e.EBAN_LOEKZ PRDelFlag,
       a.Dim_CurrencyId Dim_CurrencyId,
       a.Dim_CurrencyId_TRA Dim_CurrencyId_TRA,
       a.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,
       a.amt_ExchangeRate amt_ExchangeRate,
       a.amt_ExchangeRate_GBL amt_ExchangeRate_GBL
from tmp_mtltra_sales_001 a
	inner join tmp_mtltra_001 b on a.PartNumber = b.PartNumber
	inner join TVKWZ sp on (a.SalesOrgCode = sp.TVKWZ_VKORG 
				and a.DistributionChannelCode = sp.TVKWZ_VTWEG 
				and b.Plant = sp.TVKWZ_WERKS)
	left join tmp_matl_vendor_001 mv on mv.mtlvend_matnr = b.PartNumber
						and mv.mtlvend_plant = sp.TVKWZ_WERKS
						and mv.mtlvend_copmanycode = b.CompanyCode
	left join tmp_eban_001 e on (e.EBAN_MATNR = b.PartNumber 
					and e.EBAN_WERKS = b.Plant 
					and e.EBAN_LIFNR = mv.mtlvend_lifnr
					and e.PRSeqNo = 1)
	left join EINE ir on ir.EINA_MATNR = b.PartNumber 
				and ir.EINA_LIFNR = mv.mtlvend_lifnr
				and ir.WERKS = b.Plant
	left join tmp_KONH_KONP_001 k on (k.A901_MATNR = b.PartNumber
					and k.A901_LIFNR = mv.mtlvend_lifnr
					and k.A901_BUKRS = b.CompanyCode)
	left join tmp_mbew_001 c on (c.bwkey = b.ValuationArea 
				and c.MATNR = b.PartNumber 
				and c.MBEW_ROWNUM = 1)
	left join MEAN mn on b.partnumber = mn.MEAN_MATNR;

drop table if exists aggsubqry_001;



/*** No Sales Part ***/
insert into tmp_fact_mtltra_001
select b.PartNumber, 
	b.PartDescription, 
	b.Dim_Companyid SalesCompanyid, 
	b.CompanyCode SalesCompanyCode,
	1 Dim_PartSalesid, 
	sp.TVKWZ_VKORG SalesOrgCode, 
	so.dim_salesorgid,
	sp.TVKWZ_VTWEG DistributionChannelCode,
	b.Dim_Partid, 
	b.Plant, 
	mn.MEAN_EAN11 UPC, 
	b.ValuationArea, 
	c.BWTAR ValuationType, 
	c.STPRS Cost,
	(select max(k1.MBEW_LAEPR) 
	from MBEW k1 
	where k1.MATNR = b.PartNumber 
		and k1.BWKEY = b.ValuationArea  
		and k1.BWTAR = c.BWTAR) LastCostingDt,
	b.Dim_Plantid, 
	b.Dim_Companyid PartCompanyid, 
	b.CompanyCode PartCompanyCode,
	e.EBAN_BADAT NextPurchDt,
	e.EBAN_BANFN PRNumber,
	mv.mtlvend_lifnr VendorNumber,
	ir.INFNR InfoRecNumber,
	ir.EINE_ERDAT PIRCreated,
	k.KONP_KBETR ConditionAmt,
	k.KONH_DATAB CondValidFrom,
	k.KONH_DATBI CondValidTo,
	k.KONH_KOSRT QuoteType,
	b.ProductLine,
	b.ProductType,
	b.ProductClass,
	k.A901_KSCHL ConditionType,
       e.EBAN_LOEKZ PRDelFlag,
       b.Dim_CurrencyId Dim_CurrencyId,
       b.Dim_CurrencyId_TRA Dim_CurrencyId_TRA,
       b.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,
       b.amt_ExchangeRate amt_ExchangeRate,
       b.amt_ExchangeRate_GBL amt_ExchangeRate_GBL
from tmp_mtltra_001 b 
	inner join TVKWZ sp on b.Plant = sp.TVKWZ_WERKS
	inner join dim_salesorg so on sp.TVKWZ_VKORG = so.SalesOrgCode
	left join tmp_matl_vendor_001 mv on mv.mtlvend_matnr = b.PartNumber
						and mv.mtlvend_plant = sp.TVKWZ_WERKS
						and mv.mtlvend_copmanycode = b.CompanyCode
	left join tmp_eban_001 e on (e.EBAN_MATNR = b.PartNumber 
					and e.EBAN_WERKS = b.Plant 
					and e.EBAN_LIFNR = mv.mtlvend_lifnr
					and e.PRSeqNo = 1)
	left join EINE ir on ir.EINA_MATNR = b.PartNumber 
				and ir.EINA_LIFNR = mv.mtlvend_lifnr
				and ir.WERKS = b.Plant
	left join tmp_KONH_KONP_001 k on (k.A901_MATNR = b.PartNumber
					and k.A901_LIFNR = mv.mtlvend_lifnr
					and k.A901_BUKRS = b.CompanyCode)
	left join tmp_mbew_001 c on (c.bwkey = b.ValuationArea 
				and c.MATNR = b.PartNumber 
				and c.MBEW_ROWNUM = 1)
	left join MEAN mn on b.partnumber = mn.MEAN_MATNR
where not exists (select 1 from tmp_mtltra_sales_001 a 
		  where a.PartNumber = b.PartNumber);

/*** No Plant Part ***/
insert into tmp_fact_mtltra_001
select a.PartNumber, 
	a.PartDescription, 
	a.Dim_Companyid SalesCompanyid, 
	a.CompanyCode SalesCompanyCode,
	a.Dim_PartSalesid, 
	a.SalesOrgCode, 
	a.dim_salesorgid,
	a.DistributionChannelCode,
	1 Dim_Partid, 
	sp.TVKWZ_WERKS Plant, 
	mn.MEAN_EAN11 UPC, 
	pl.ValuationArea, 
	c.BWTAR ValuationType, 
	c.STPRS Cost,
	(select max(k1.MBEW_LAEPR) 
	from MBEW k1 
	where k1.MATNR = a.PartNumber 
		and k1.BWKEY = pl.ValuationArea  
		and k1.BWTAR = c.BWTAR) LastCostingDt,
	pl.Dim_Plantid, 
	a.Dim_Companyid PartCompanyid, 
	a.CompanyCode PartCompanyCode,
	e.EBAN_BADAT NextPurchDt,
	e.EBAN_BANFN PRNumber,
	mv.mtlvend_lifnr VendorNumber,
	ir.INFNR InfoRecNumber,
	ir.EINE_ERDAT PIRCreated,
	k.KONP_KBETR ConditionAmt,
	k.KONH_DATAB CondValidFrom,
	k.KONH_DATBI CondValidTo,
	k.KONH_KOSRT QuoteType,
	a.ProductLine,
	a.ProductType,
	a.ProductClass,
	k.A901_KSCHL ConditionType,
       e.EBAN_LOEKZ PRDelFlag,
       a.Dim_CurrencyId Dim_CurrencyId,
       a.Dim_CurrencyId_TRA Dim_CurrencyId_TRA,
       a.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,
       a.amt_ExchangeRate amt_ExchangeRate,
       a.amt_ExchangeRate_GBL amt_ExchangeRate_GBL
from tmp_mtltra_sales_001 a
	inner join TVKWZ sp on (a.SalesOrgCode = sp.TVKWZ_VKORG 
				and a.DistributionChannelCode = sp.TVKWZ_VTWEG)
	inner join dim_plant pl on pl.PlantCode = sp.TVKWZ_WERKS
	left join tmp_matl_vendor_001 mv on mv.mtlvend_matnr = a.PartNumber
						and mv.mtlvend_plant = sp.TVKWZ_WERKS
						and mv.mtlvend_copmanycode = a.CompanyCode
	left join tmp_eban_001 e on (e.EBAN_MATNR = a.PartNumber 
					and e.EBAN_WERKS = sp.TVKWZ_WERKS 
					and e.EBAN_LIFNR = mv.mtlvend_lifnr
					and e.PRSeqNo = 1)
	left join EINE ir on ir.EINA_MATNR = a.PartNumber 
				and ir.EINA_LIFNR = mv.mtlvend_lifnr
				and ir.WERKS = sp.TVKWZ_WERKS
	left join tmp_KONH_KONP_001 k on (k.A901_MATNR = a.PartNumber
					and k.A901_LIFNR = mv.mtlvend_lifnr
					and k.A901_BUKRS = a.CompanyCode)
	left join tmp_mbew_001 c on (c.bwkey = pl.ValuationArea 
				and c.MATNR = a.PartNumber 
				and c.MBEW_ROWNUM = 1)
	left join MEAN mn on a.partnumber = mn.MEAN_MATNR
where not exists (select 1 from tmp_mtltra_001 b 
		  where a.PartNumber = b.PartNumber);

insert into tmp_fact_mtltra_001
select a.PartNumber, 
	b.PartDescription, 
	a.Dim_Companyid SalesCompanyid, 
	a.CompanyCode SalesCompanyCode,
	a.Dim_PartSalesid, 
	a.SalesOrgCode, 
	a.dim_salesorgid,
	a.DistributionChannelCode,
	b.Dim_Partid, 
	b.Plant, 
	mn.MEAN_EAN11 UPC, 
	b.ValuationArea, 
	c.BWTAR ValuationType, 
	c.STPRS Cost,
	(select max(k1.MBEW_LAEPR) 
	from MBEW k1 
	where k1.MATNR = b.PartNumber 
		and k1.BWKEY = b.ValuationArea  
		and k1.BWTAR = c.BWTAR) LastCostingDt,
	b.Dim_Plantid, 
	b.Dim_Companyid PartCompanyid, 
	b.CompanyCode PartCompanyCode,
	null NextPurchDt,
	null PRNumber,
	null VendorNumber,
	null InfoRecNumber,
	null PIRCreated,
	0 ConditionAmt,
	null CondValidFrom,
	null CondValidTo,
	null QuoteType,
	b.ProductLine,
	b.ProductType,
	b.ProductClass,
	null ConditionType,
       null PRDelFlag,
       a.Dim_CurrencyId Dim_CurrencyId,
       a.Dim_CurrencyId_TRA Dim_CurrencyId_TRA,
       a.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,
       a.amt_ExchangeRate amt_ExchangeRate,
       a.amt_ExchangeRate_GBL amt_ExchangeRate_GBL
from tmp_mtltra_sales_001 a
	inner join tmp_mtltra_001 b on a.PartNumber = b.PartNumber
	inner join TVKWZ sp on (a.SalesOrgCode = sp.TVKWZ_VKORG 
				and a.DistributionChannelCode = sp.TVKWZ_VTWEG 
				and b.Plant = sp.TVKWZ_WERKS)
	left join tmp_mbew_001 c on (c.bwkey = b.ValuationArea 
				and c.MATNR = b.PartNumber 
				and c.MBEW_ROWNUM = 1)
	left join MEAN mn on b.partnumber = mn.MEAN_MATNR
where not exists (select 1 from tmp_matl_vendor_001 mv
		  where mv.mtlvend_matnr = b.PartNumber
			and mv.mtlvend_plant = sp.TVKWZ_WERKS
			and mv.mtlvend_copmanycode = b.CompanyCode);

insert into tmp_fact_mtltra_001
select b.PartNumber, 
	b.PartDescription, 
	b.Dim_Companyid SalesCompanyid, 
	b.CompanyCode SalesCompanyCode,
	1 Dim_PartSalesid, 
	sp.TVKWZ_VKORG SalesOrgCode, 
	so.dim_salesorgid,
	sp.TVKWZ_VTWEG DistributionChannelCode,
	b.Dim_Partid, 
	b.Plant, 
	mn.MEAN_EAN11 UPC, 
	b.ValuationArea, 
	c.BWTAR ValuationType, 
	c.STPRS Cost,
	(select max(k1.MBEW_LAEPR) 
	from MBEW k1 
	where k1.MATNR = b.PartNumber 
		and k1.BWKEY = b.ValuationArea  
		and k1.BWTAR = c.BWTAR) LastCostingDt,
	b.Dim_Plantid, 
	b.Dim_Companyid PartCompanyid, 
	b.CompanyCode PartCompanyCode,
	null NextPurchDt,
	null PRNumber,
	null VendorNumber,
	null InfoRecNumber,
	null PIRCreated,
	null ConditionAmt,
	null CondValidFrom,
	null CondValidTo,
	null QuoteType,
	b.ProductLine,
	b.ProductType,
	b.ProductClass,
	null ConditionType,
       null PRDelFlag,
       b.Dim_CurrencyId Dim_CurrencyId,
       b.Dim_CurrencyId_TRA Dim_CurrencyId_TRA,
       b.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,
       b.amt_ExchangeRate amt_ExchangeRate,
       b.amt_ExchangeRate_GBL amt_ExchangeRate_GBL
from tmp_mtltra_001 b 
	inner join TVKWZ sp on b.Plant = sp.TVKWZ_WERKS
	inner join dim_salesorg so on sp.TVKWZ_VKORG = so.SalesOrgCode
	left join tmp_mbew_001 c on (c.bwkey = b.ValuationArea 
				and c.MATNR = b.PartNumber 
				and c.MBEW_ROWNUM = 1)
	left join MEAN mn on b.partnumber = mn.MEAN_MATNR
where not exists (select 1 from tmp_mtltra_sales_001 a 
		  where a.PartNumber = b.PartNumber)
	and not exists (select 1 from tmp_matl_vendor_001 mv
		  where mv.mtlvend_matnr = b.PartNumber
			and mv.mtlvend_plant = sp.TVKWZ_WERKS
			and mv.mtlvend_copmanycode = b.CompanyCode);

insert into tmp_fact_mtltra_001
select a.PartNumber, 
	a.PartDescription, 
	a.Dim_Companyid SalesCompanyid, 
	a.CompanyCode SalesCompanyCode,
	a.Dim_PartSalesid, 
	a.SalesOrgCode, 
	a.dim_salesorgid,
	a.DistributionChannelCode,
	1 Dim_Partid, 
	sp.TVKWZ_WERKS Plant, 
	mn.MEAN_EAN11 UPC, 
	pl.ValuationArea, 
	c.BWTAR ValuationType, 
	c.STPRS Cost,
	(select max(k1.MBEW_LAEPR) 
	from MBEW k1 
	where k1.MATNR = a.PartNumber 
		and k1.BWKEY = pl.ValuationArea  
		and k1.BWTAR = c.BWTAR) LastCostingDt,
	pl.Dim_Plantid, 
	a.Dim_Companyid PartCompanyid, 
	a.CompanyCode PartCompanyCode,
	null NextPurchDt,
	null PRNumber,
	null VendorNumber,
	null InfoRecNumber,
	null PIRCreated,
	null ConditionAmt,
	null CondValidFrom,
	null CondValidTo,
	null QuoteType,
	a.ProductLine,
	a.ProductType,
	a.ProductClass,
	null ConditionType,
       null PRDelFlag,
       a.Dim_CurrencyId Dim_CurrencyId,
       a.Dim_CurrencyId_TRA Dim_CurrencyId_TRA,
       a.Dim_CurrencyId_GBL Dim_CurrencyId_GBL,
       a.amt_ExchangeRate amt_ExchangeRate,
       a.amt_ExchangeRate_GBL amt_ExchangeRate_GBL
from tmp_mtltra_sales_001 a
	inner join TVKWZ sp on (a.SalesOrgCode = sp.TVKWZ_VKORG 
				and a.DistributionChannelCode = sp.TVKWZ_VTWEG)
	inner join dim_plant pl on pl.PlantCode = sp.TVKWZ_WERKS
	left join tmp_mbew_001 c on (c.bwkey = pl.ValuationArea 
				and c.MATNR = a.PartNumber 
				and c.MBEW_ROWNUM = 1)
	left join MEAN mn on a.partnumber = mn.MEAN_MATNR
where not exists (select 1 from tmp_mtltra_001 b 
		  where a.PartNumber = b.PartNumber)
	and not exists (select 1 from tmp_matl_vendor_001 mv
		  where mv.mtlvend_matnr = a.PartNumber
			and mv.mtlvend_plant = sp.TVKWZ_WERKS
			and mv.mtlvend_copmanycode = a.CompanyCode);

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_fact_mtltra_001;

/*** Fact table population ***/

DROP TABLE IF EXISTS fact_materialtransaction_1;
CREATE TABLE fact_materialtransaction_1 AS
SELECT *
FROM fact_materialtransaction a
WHERE 1=2;

INSERT INTO fact_materialtransaction_1(fact_materialtransactionid,
                                     Dim_Partid,
                                     Dim_PartSalesid,
                                     Dim_Plantid,
                                     Dim_SalesOrgid,
                                     Dim_Vendorid,
                                     Dim_CompanyId,
                                     dd_InfoRecNum,
                                     Dim_Dateid_PIRCreated,
                                     amt_ConditionAmt,
                                     Dim_Dateid_CondValidFrom,
                                     Dim_Dateid_CondValidTo,
                                     dd_QuoteType,
                                     amt_Cost,
                                     dd_ValuationType,
                                     dd_UPC,
                                     dd_MaterialDescription,
                                     dd_ColorDescription,
                                     dd_ProductLine,
                                     dd_ProductType,
                                     dd_ProductClass,
                                     Dim_Dateid_PRDate,
                                     Dim_Dateid_NextPurchDt,
                                     Dim_Dateid_LastCosting,
				     dd_PartNumber,
				     dd_ConditionType,
				     dd_MaterialCost,
				     dd_ConditionAmt,
				     dd_PRDelFlag,
					 dim_currencyid,
					 dim_currencyid_TRA,
					 dim_currencyid_GBL,
					 amt_exchangerate,
					 amt_exchangerate_GBL)
SELECT row_number() over() fact_materialtransactionid,
		tp.*
FROM (SELECT DISTINCT ifnull(a.Dim_Partid,1) Dim_Partid,
       ifnull(a.Dim_PartSalesid,1) Dim_PartSalesid,
       ifnull(a.Dim_Plantid,1) Dim_Plantid,
       ifnull(a.Dim_SalesOrgid,1) Dim_SalesOrgid,
       ifnull((select v.Dim_Vendorid from Dim_Vendor v where v.VendorNumber = a.VendorNumber),1) Dim_Vendorid,
       ifnull(a.PartCompanyid,1) Dim_CompanyId,
       ifnull(a.InfoRecNumber,'Not Set') dd_InfoRecNum,
       ifnull((select dt.Dim_Dateid from Dim_Date dt where dt.CompanyCode = a.PartCompanyCode and dt.DateValue = a.PIRCreated),1) Dim_Dateid_PIRCreated,
       0 amt_ConditionAmt,
       ifnull((select dt.Dim_Dateid from Dim_Date dt where dt.CompanyCode = a.PartCompanyCode and dt.DateValue = a.CondValidFrom),1) Dim_Dateid_CondValidFrom,
       ifnull((select dt.Dim_Dateid from Dim_Date dt where dt.CompanyCode = a.PartCompanyCode and dt.DateValue = a.CondValidTo),1) Dim_Dateid_CondValidTo,
       ifnull(a.QuoteType,'Not Set') dd_QuoteType,
       0 amt_Cost,
       ifnull(a.ValuationType,'Not Set') dd_ValuationType,
       ifnull(a.UPC,'Not Set') dd_UPC,
       ifnull(a.PartDescription,'Not Set') dd_MaterialDescription,
       'Not Set' dd_ColorDescription,
       ifnull(a.ProductLine,'Not Set') dd_ProductLine,
       ifnull(a.ProductType,'Not Set') dd_ProductType,
       ifnull(a.ProductClass,'Not Set') dd_ProductClass,
       ifnull((select dt.Dim_Dateid from Dim_Date dt where dt.CompanyCode = a.PartCompanyCode and dt.DateValue = a.NextPurchDt),1) Dim_Dateid_PRDate,
       ifnull((select dt.Dim_Dateid from Dim_Date dt where dt.CompanyCode = a.PartCompanyCode and dt.DateValue = a.NextPurchDt),1) Dim_Dateid_NextPurchDt,
       ifnull((select dt.Dim_Dateid from Dim_Date dt where dt.CompanyCode = a.PartCompanyCode and dt.DateValue = a.LastCostingDt),1) Dim_Dateid_LastCosting,
	ifnull(a.PartNumber,'Not Set') dd_PartNumber,
	ifnull(a.ConditionType,'Not Set') dd_ConditionType,
	ifnull(a.Cost,0) dd_MaterialCost,
	ifnull(a.ConditionAmt, 0.00) dd_ConditionAmt,
	ifnull(PRDelFlag,'Not Set') dd_PRDelFlag,
	 a.dim_currencyid,
	 a.dim_currencyid_TRA,
	 a.dim_currencyid_GBL,
	 a.amt_exchangerate,
	 a.amt_exchangerate_GBL
FROM tmp_fact_mtltra_001 a) tp;

call vectorwise(combine 'fact_materialtransaction_1');

UPDATE fact_materialtransaction_1 mt
  FROM dim_Company co, dim_currency cur
SET mt.dim_CurrencyId = cur.dim_CurrencyId
WHERE mt.Dim_CompanyId = co.Dim_CompanyId
  AND co.Currency = cur.CurrencyCode
  AND (mt.dim_CurrencyId IS NULL OR mt.dim_CurrencyId  = 1);

UPDATE fact_materialtransaction_1 mt
SET mt.dim_CurrencyId_TRA = mt.dim_CurrencyId
WHERE (mt.dim_CurrencyId_TRA IS NULL OR mt.dim_CurrencyId_TRA = 1);

UPDATE fact_materialtransaction_1 mt
  FROM dim_currency cur,tmp_GlobalCurr_001
SET mt.dim_CurrencyId_GBL = cur.dim_CurrencyId
WHERE pGlobalCurrency = cur.CurrencyCode
  AND (mt.dim_CurrencyId_GBL IS NULL OR mt.dim_CurrencyId_GBL  = 1);

UPDATE fact_materialtransaction_1 mt
   SET mt.amt_ExchangeRate = 1
WHERE mt.amt_ExchangeRate IS NULL;

UPDATE fact_materialtransaction_1 mt
  FROM dim_company co,tmp_GlobalCurr_001,tmp_getExchangeRate1 z
SET mt.amt_ExchangeRate_GBL = z.exchangeRate
WHERE z.pFromCurrency = co.Currency 
  and z.fact_script_name = 'bi_populate_materialtransaction_fact' 
  and z.pToCurrency = pGlobalCurrency 
  and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)
  AND (mt.amt_ExchangeRate_GBL IS NULL);


UPDATE fact_materialtransaction_1 mt
SET mt.amt_ExchangeRate_GBL = 1
WHERE mt.amt_ExchangeRate_GBL IS NULL;
  
rename table fact_materialtransaction to fact_materialtransaction_2;
rename table fact_materialtransaction_1 to fact_materialtransaction;
drop table if exists fact_materialtransaction_2;

/*Radu,added dd_PartNumber_noleadzero computation */

UPDATE fact_materialtransaction mt
SET  
mt.dd_PartNumber_noleadzero= 
ifnull(case when length(mt.dd_PartNumber)=18 and mt.dd_PartNumber is integer then trim(leading '0' from mt.dd_PartNumber) else mt.dd_PartNumber end,'Not Set');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialtransaction;

drop table if exists tmp_mtltra_sales_001;
drop table if exists tmp_mtltra_001;
drop table if exists tmp_eban_001;
drop table if exists tmp_KONH_KONP_001;
drop table if exists tmp_fact_mtltra_001;
drop table if exists tmp_mbew_001;
