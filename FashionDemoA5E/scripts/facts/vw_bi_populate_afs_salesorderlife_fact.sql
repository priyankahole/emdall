
DELETE FROM fact_salesorderlife;

DROP TABLE IF EXISTS variable_holder_fact_salesorderlife;

CREATE TABLE variable_holder_fact_salesorderlife(
        pGlobalCurrency,
        pCustomPartnerFunctionKey,
        pCustomPartnerFunctionKey1,
        pCustomPartnerFunctionKey2,
        pBillToPartyPartnerFunction,
        pPayerPartnerFunction)
AS
Select CAST(ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD'),varchar(3)),
       CAST( ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'custom.partnerfunction.key'),
                'Not Set'),varchar(10)),
       CAST( ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'custom.partnerfunction.key1'),
                'Not Set'),varchar(10)),
       CAST( ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'custom.partnerfunction.key2'),
                'Not Set'),varchar(10)),
       CAST('RE',varchar(5)),
       CAST('RG',varchar(5)) ;

 /* update from item data if it exists (posnr <> 0), otherwise update from header data (posnr = 0) */
   UPDATE vbak_vbap_vbep p
   FROM vbak_vbap_vbkd k
   SET p.VBAP_STCUR = k.VBKD_KURSK
   where p.vbak_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0
   and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);


   UPDATE vbak_vbap_vbep p
   FROM vbak_vbap_vbkd k
   SET p.PRSDT = k.VBKD_PRSDT
   where p.vbak_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0
   and ifnull(p.PRSDT,'01011900') <> ifnull(k.VBKD_PRSDT,'01011900');

   UPDATE vbak_vbap p
   FROM vbak_vbap_vbkd k
   SET p.VBAP_STCUR = k.VBKD_KURSK
   where p.vbap_vbeln = k.VBKD_VBELN and VBKD_POSNR = 0
   and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);

   UPDATE vbak_vbap p
   FROM vbak_vbap_vbkd k
   SET p.PRSDT = k.VBKD_PRSDT
   where p.vbap_vbeln = k.VBKD_VBELN and k.VBKD_POSNR = 0
   AND ifnull(p.PRSDT,'01011900') <> ifnull(k.VBKD_PRSDT,'01011900');

 /* Update item data if it exists. So this will overwrite updates from header, where a match is found */

   UPDATE vbak_vbap_vbep p
   FROM vbak_vbap_vbkd k
   SET p.VBAP_STCUR = k.VBKD_KURSK
   where p.vbak_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
   and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);

   UPDATE vbak_vbap_vbep p
   FROM vbak_vbap_vbkd k
   SET p.PRSDT = k.VBKD_PRSDT
   where p.vbak_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
   AND ifnull(p.PRSDT,'01011900') <> ifnull(k.VBKD_PRSDT,'01011900');

   UPDATE vbak_vbap p
   FROM vbak_vbap_vbkd k
   SET p.VBAP_STCUR = k.VBKD_KURSK
   where p.vbap_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
   and ifnull(p.VBAP_STCUR,-1) <> ifnull(k.VBKD_KURSK,-1);

   UPDATE vbak_vbap p
   FROM vbak_vbap_vbkd k
   SET p.PRSDT = k.VBKD_PRSDT
   where p.vbap_vbeln = k.VBKD_VBELN and p.vbap_posnr = k.VBKD_POSNR
   AND ifnull(p.PRSDT,'01011900') <> ifnull(k.VBKD_PRSDT,'01011900');
   
   
DROP TABLE IF EXISTS Dim_CostCenter_702;
CREATE TABLE Dim_CostCenter_702 AS SELECT FIRST 0 * from Dim_CostCenter ORDER BY ValidTo DESC;
 
DROP TABLE IF EXISTS staging_update_702;
DROP TABLE IF EXISTS VBAK_VBAP_VBEP_702;

CREATE TABLE VBAK_VBAP_VBEP_702 AS 
SELECT min(x.VBEP_ETENR) _SalesSchedNo, x.VBAK_VBELN _SaleseDocNo, x.VBAP_POSNR _SalesItemNo
          FROM VBAK_VBAP_VBEP x GROUP BY x.VBAK_VBELN, x.VBAP_POSNR;

CALL VECTORWISE(COMBINE 'VBAK_VBAP_VBEP_702');

/* Backup rows for tracking before they are deleted */
INSERT INTO tmp_fact_salesorderlife_deleted
SELECT dd_SalesDocNo, dd_SalesItemNo,dd_ScheduleNo, timestamp(local_timestamp),fact_salesorderlifeid, 'D'
FROM fact_salesorderlife
WHERE exists (SELECT 1 FROM CDPOS_VBAK a where a.CDPOS_OBJECTID = dd_SalesDocNo AND a.CDPOS_TABNAME = 'VBAK' AND a.CDPOS_CHNGIND = 'D');

INSERT INTO tmp_fact_salesorderlife_deleted
SELECT dd_SalesDocNo, dd_SalesItemNo,dd_ScheduleNo, timestamp(local_timestamp),fact_salesorderlifeid, 'I'
FROM fact_salesorderlife
WHERE exists (SELECT 1 FROM CDPOS_VBAK a where SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_SalesDocNo 
AND SUBSTRING(a.CDPOS_TABKEY,14,6) = dd_SalesItemNo AND a.CDPOS_TABNAME = 'VBAP' AND  a.CDPOS_CHNGIND = 'D' )
AND NOT EXISTS (SELECT 1 FROM VBAK_VBAP_VBEP WHERE VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo);

INSERT INTO tmp_fact_salesorderlife_deleted
SELECT dd_SalesDocNo, dd_SalesItemNo,dd_ScheduleNo, timestamp(local_timestamp),fact_salesorderlifeid, 'S'
FROM fact_salesorderlife
WHERE exists (SELECT 1 FROM CDPOS_VBAK a where SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_SalesDocNo AND SUBSTRING(a.CDPOS_TABKEY,14,6) = dd_SalesItemNo AND SUBSTRING(a.CDPOS_TABKEY,20,4) = dd_scheduleno  AND a.CDPOS_TABNAME = 'VBEP' AND  a.CDPOS_CHNGIND = 'D' )
AND NOT EXISTS (SELECT 1 FROM VBAK_VBAP_VBEP WHERE VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo);

/* Delete based on docno */
DELETE FROM fact_salesorderlife
WHERE exists (SELECT 1 FROM CDPOS_VBAK a where a.CDPOS_OBJECTID = dd_SalesDocNo AND a.CDPOS_TABNAME = 'VBAK' AND a.CDPOS_CHNGIND = 'D');

/* Delete based on itemno */
DELETE FROM fact_salesorderlife
WHERE exists (SELECT 1 FROM CDPOS_VBAK a where SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_SalesDocNo AND SUBSTRING(a.CDPOS_TABKEY,14,6) = dd_SalesItemNo AND a.CDPOS_TABNAME = 'VBAP' AND  a.CDPOS_CHNGIND = 'D' )
AND NOT EXISTS (SELECT 1 FROM VBAK_VBAP_VBEP WHERE VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo);

/* Delete based on schedule */
DELETE FROM fact_salesorderlife
WHERE exists (SELECT 1 FROM CDPOS_VBAK a where SUBSTRING(a.CDPOS_TABKEY,4,10) = dd_SalesDocNo AND SUBSTRING(a.CDPOS_TABKEY,14,6) = dd_SalesItemNo AND SUBSTRING(a.CDPOS_TABKEY,20,4) = dd_scheduleno  AND a.CDPOS_TABNAME = 'VBEP' AND  a.CDPOS_CHNGIND = 'D' )
AND NOT EXISTS (SELECT 1 FROM VBAK_VBAP_VBEP WHERE VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo);

CREATE TABLE staging_update_702
AS
SELECT 
    VBEP_WMENG ct_ScheduleQtySalesUnit,
	vbep_bmeng ct_ConfirmedQty,
	vbep_cmeng ct_CorrectedQty,
	ifnull(Decimal((vbap_netpr ),18,4), 0) amt_UnitPrice,
	vbap_kpein ct_PriceUnit,
	Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') AND VBAP_NETPR > 0 AND VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
		THEN ifnull(Round((VBEP_WMENG * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0) 
		ELSE ifnull((VBEP_WMENG * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0) 
	   END ,18,4) amt_ScheduleTotal,
	Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
			THEN vbap_wavwr
			ELSE 0
		  END ),18,4) amt_StdCost,
	Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
		    THEN vbap_zwert
		    ELSE 0
		END ),18,4) amt_TargetValue,
        decimal(0.000,18,4) amt_Tax,
	CASE WHEN VBEP_ETENR = y._SalesSchedNo
			  THEN vbap_zmeng
			  ELSE 0
		    END ct_TargetQty,
	ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
		WHERE z.pFROMCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency =  co.Currency AND z.pFROMExchangeRate = 0),1)  amt_ExchangeRate,
	ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
		WHERE z.pFROMCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency = pGlobalCurrency AND z.pFROMExchangeRate = 0),1)  amt_ExchangeRate_GBL,
	vbap_uebto ct_OverDlvrTolerance,
	vbap_untto ct_UnderDlvrTolerance,
	ifnull((SELECT Dim_Dateid
	    FROM Dim_Date dd
	    WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
	ifnull((SELECT Dim_Dateid
	    FROM Dim_Date dd
	    WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo,
	ifnull((SELECT uom.Dim_UnitOfMeasureId
                    FROM Dim_UnitOfMeasure uom
                    WHERE   uom.UOM = vbap_kmein
                        AND uom.RowIsCurrent = 1), 1) Dim_UnitOfMeasureId,
	ifnull((SELECT uom.Dim_UnitOfMeasureId
                    FROM Dim_UnitOfMeasure uom
                    WHERE   uom.UOM = vbap_meins
                        AND uom.RowIsCurrent = 1), 1) Dim_BaseUoMid,
	ifnull((SELECT uom.Dim_UnitOfMeasureId
                    FROM Dim_UnitOfMeasure uom
                    WHERE   uom.UOM = vbap_vrkme
                        AND uom.RowIsCurrent = 1), 1) Dim_SalesUoMid,
	Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') AND VBAP_NETPR > 0 AND VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END ,18,4) amt_UnitPriceUoM,
	1 Dim_MaterialPriceGroup4Id,
	1 Dim_MaterialPriceGroup5Id,
	ifnull((SELECT sois.Dim_SalesOrderItemStatusid
                      FROM Dim_SalesOrderItemStatus sois
                      WHERE sois.SalesDocumentNumber = VBAK_VBELN AND sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
    ifnull((SELECT cg1.Dim_CustomerGroup1id
                      FROM Dim_CustomerGroup1 cg1
                      WHERE cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
     ifnull((SELECT cg2.Dim_CustomerGroup2id
                      FROM Dim_CustomerGroup2 cg2
                      WHERE cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
     soic.Dim_SalesOrderItemCategoryid  Dim_salesorderitemcategoryid,
     ifnull(vbep_lfrel,'Not Set') dd_ItemRelForDelv,
	 1 Dim_ProfitCenterId ,
     ifnull((SELECT dc.Dim_DistributionChannelid
                      FROM dim_DistributionChannel dc
                      WHERE   dc.DistributionChannelCode = VBAK_VTWEG
                          AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId ,
     ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
     ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
     ifnull((SELECT r.dim_routeid FROM dim_route r WHERE
                       r.RouteCode = VBAP_ROUTE AND r.RowIsCurrent = 1),1) Dim_RouteId,
     ifnull((SELECT src.Dim_SalesRiskCategoryId 
	         FROM Dim_SalesRiskCategory src WHERE
                       src.SalesRiskCategory = VBAK_CTLPC AND src.CreditControlArea = VBAK_KKBER AND src.RowIsCurrent = 1),1) Dim_SalesRiskCategoryId,
     1 Dim_CustomerRiskCategoryId,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,			  
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.currency),1) Dim_Currencyid,			  				  
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pGlobalCurrency),1) dim_Currencyid_GBL,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,
    ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
            WHERE z.pFROMCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_salesorder_fact'
            AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pFROMExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1) amt_exchangerate_STAT
			
FROM fact_salesorderlife, 
	 VBAK_VBAP_VBEP,
	 Dim_Plant pl,
	 Dim_Company co,
	 Dim_SalesOrderItemCategory soic,
	 VBAK_VBAP_VBEP_702 y, variable_holder_fact_salesorderlife
WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
  AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
  AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
  AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;
  
  
UPDATE staging_update_702
  FROM VBAK_VBAP_VBEP,
	   Dim_Plant pl,
	   Dim_Company co,
	   Dim_SalesOrderItemCategory soic
   SET amt_Tax = ifnull(Decimal(((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0) = 0 THEN 1
					WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') AND VBAP_NETPR > 0 AND VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
					THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							WHEN 0 THEN 1
							ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
						END
					ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
				 END) 
			* (vbep_bmeng )),18,4), 0)
 WHERE pl.PlantCode = VBAP_WERKS 
   AND co.CompanyCode = pl.CompanyCode
   AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
   AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

CALL VECTORWISE(COMBINE 'staging_update_702');

UPDATE staging_update_702 sut
  FROM VBAK_VBAP_VBEP,
       Dim_Plant pl,
       Dim_Company co,
	   Dim_SalesOrderItemCategory soic,
	   VBAK_VBAP_VBEP_702 y
   SET Dim_ProfitCenterId = ifnull((SELECT  pc.dim_profitcenterid
                           FROM dim_profitcenter pc
                           WHERE pc.ProfitCenterCode = VBAP_PRCTR
                           AND pc.ControllingArea = VBAK_KOKRS
                           AND pc.ValidTo >= VBAK_ERDAT
                           AND pc.RowIsCurrent = 1 ),1) 
 WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
   AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
   AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
   AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;

DROP TABLE IF EXISTS tmp_upd_702;

CREATE TABLE tmp_upd_702 AS SELECT FIRST 0 src.dim_salesriskcategoryid updcol1, c.CUSTOMER  updVBAK_KUNNR,src.CreditControlArea  updVBAK_KKBER
FROM ukmbp_cms a
      INNER JOIN BUT000 b on a.PARTNER = b.PARTNER
      INNER JOIN cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
      INNER JOIN dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1
ORDER BY c.customer ;

UPDATE staging_update_702 sut
  FROM VBAK_VBAP_VBEP,
       Dim_Plant pl,
       Dim_Company co,
       Dim_SalesOrderItemCategory soic,
       VBAK_VBAP_VBEP_702 y
   SET Dim_CustomerRiskCategoryId= ifnull((SELECT  updcol1 FROM tmp_upd_702 WHERE updVBAK_KUNNR = VBAK_KUNNR AND updVBAK_KKBER=VBAK_KKBER),1)
 WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
   AND VBAK_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND VBEP_ETENR = dd_ScheduleNo
   AND VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
   AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1;
DROP TABLE IF EXISTS tmp_upd_702;

CALL VECTORWISE(COMBINE 'staging_update_702');

CALL VECTORWISE(COMBINE 'fact_salesorderlife');

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.ct_ScheduleQtySalesUnit=sut.ct_ScheduleQtySalesUnit
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.ct_ScheduleQtySalesUnit <> sut.ct_ScheduleQtySalesUnit;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.ct_ConfirmedQty=sut.ct_ConfirmedQty
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.ct_ConfirmedQty <> sut.ct_ConfirmedQty;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.ct_CorrectedQty=sut.ct_CorrectedQty
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.ct_CorrectedQty <> sut.ct_CorrectedQty;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_UnitPrice=sut.amt_UnitPrice
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.amt_UnitPrice <> sut.amt_UnitPrice;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.ct_PriceUnit=sut.ct_PriceUnit
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND  so.ct_PriceUnit <> sut.ct_PriceUnit;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_ScheduleTotal=sut.amt_ScheduleTotal
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.amt_ScheduleTotal <> sut.amt_ScheduleTotal;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_StdCost=sut.amt_StdCost
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND  so.amt_StdCost <> sut.amt_StdCost;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_TargetValue=sut.amt_TargetValue
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.amt_TargetValue <> sut.amt_TargetValue;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_Tax=sut.amt_Tax
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.amt_Tax <> sut.amt_Tax;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.ct_TargetQty=sut.ct_TargetQty
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.ct_TargetQty <> sut.ct_TargetQty;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_ExchangeRate=sut.amt_ExchangeRate
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.amt_ExchangeRate <> sut.amt_ExchangeRate;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_ExchangeRate_GBL =ifnull(sut.amt_ExchangeRate_GBL,1) 
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.amt_ExchangeRate_GBL <> ifnull(sut.amt_ExchangeRate_GBL,1) ;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.ct_OverDlvrTolerance=sut.ct_OverDlvrTolerance
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.ct_OverDlvrTolerance <> sut.ct_OverDlvrTolerance;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.ct_UnderDlvrTolerance=sut.ct_UnderDlvrTolerance
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.ct_UnderDlvrTolerance <> sut.ct_UnderDlvrTolerance;

CALL VECTORWISE(COMBINE 'fact_salesorderlife');

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_DateidSalesOrderCreated=sut.Dim_DateidSalesOrderCreated
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_DateidSalesOrderCreated <> sut.Dim_DateidSalesOrderCreated;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_DateidFirstDate=sut.Dim_DateidFirstDate
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_DateidFirstDate <> sut.Dim_DateidFirstDate;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_UnitPriceUoM=sut.amt_UnitPriceUoM
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.amt_UnitPriceUoM <> sut.amt_UnitPriceUoM;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_UnitOfMeasureId = sut.Dim_UnitOfMeasureId
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_UnitOfMeasureId <> sut.Dim_UnitOfMeasureId;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_BaseUoMid = sut.Dim_BaseUoMid
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_BaseUoMid <> sut.Dim_BaseUoMid;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_SalesUoMid = sut.Dim_SalesUoMid
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
  AND so.Dim_SalesUoMid <> sut.Dim_SalesUoMid;
  
UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_SalesOrderItemStatusid=sut.Dim_SalesOrderItemStatusid
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_SalesOrderItemStatusid <> sut.Dim_SalesOrderItemStatusid;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET  so.Dim_CustomerGroup1id=sut.Dim_CustomerGroup1id
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_CustomerGroup1id <> sut.Dim_CustomerGroup1id;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_CustomerGroup2id=sut.Dim_CustomerGroup2id
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_CustomerGroup2id <> sut.Dim_CustomerGroup2id;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_salesorderitemcategoryid=sut.Dim_salesorderitemcategoryid
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_salesorderitemcategoryid <> sut.Dim_salesorderitemcategoryid;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET  so.dd_ItemRelForDelv=sut.dd_ItemRelForDelv
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.dd_ItemRelForDelv <> sut.dd_ItemRelForDelv;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET  so.Dim_ProfitCenterId=sut.Dim_ProfitCenterId
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_ProfitCenterId <> sut.Dim_ProfitCenterId;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_DistributionChannelId=sut.Dim_DistributionChannelId
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_DistributionChannelId <> sut.Dim_DistributionChannelId;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.dd_BatchNo=sut.dd_BatchNo
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.dd_BatchNo <> sut.dd_BatchNo;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.dd_CreatedBy=sut.dd_CreatedBy
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.dd_CreatedBy <> sut.dd_CreatedBy;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_RouteId=sut.Dim_RouteId
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_RouteId <> sut.Dim_RouteId;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_SalesRiskCategoryId=sut.Dim_SalesRiskCategoryId
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND  so.Dim_SalesRiskCategoryId <> sut.Dim_SalesRiskCategoryId;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.Dim_CustomerRiskCategoryId=sut.Dim_CustomerRiskCategoryId
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.Dim_CustomerRiskCategoryId <> sut.Dim_CustomerRiskCategoryId;

/*LK: 9 Sep: Currency AND exchg rate chgs */
UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.dim_Currencyid_TRA = sut.dim_Currencyid_TRA
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.dim_Currencyid_TRA <> sut.dim_Currencyid_TRA;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.dim_Currencyid = sut.dim_Currencyid
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.dim_Currencyid <> sut.dim_Currencyid;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.dim_Currencyid_GBL = sut.dim_Currencyid_GBL
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.dim_Currencyid_GBL <> sut.dim_Currencyid_GBL;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.dim_currencyid_STAT = sut.dim_currencyid_STAT
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.dim_currencyid_STAT <> sut.dim_currencyid_STAT;

UPDATE fact_salesorderlife so
  FROM staging_update_702 sut
   SET so.amt_exchangerate_STAT = sut.amt_exchangerate_STAT
 WHERE so.dd_SalesDocNo=sut.dd_SalesDocNo AND so.dd_SalesItemNo=sut.dd_SalesItemNo AND so.dd_ScheduleNo=sut.dd_ScheduleNo
   AND so.amt_exchangerate_STAT <> sut.amt_exchangerate_STAT;

CALL VECTORWISE(COMBINE 'fact_salesorderlife');

DROP TABLE IF EXISTS staging_UPDATE_702;

DROP TABLE IF EXISTS Dim_CostCenter_first1_702;
CREATE TABLE Dim_CostCenter_first1_702 
AS 
SELECT code,ControllingArea,RowIsCurrent,max(Validto) validto, cast(null,integer) dim_costcenterid 
FROM Dim_CostCenter_702 
GROUP BY code,ControllingArea,RowIsCurrent;

UPDATE Dim_CostCenter_first1_702 b
   SET b.dim_costcenterid =(SELECT a.dim_costcenterid 
                            FROM Dim_CostCenter_702 a 
							WHERE b.code=a.code 
							AND b.ControllingArea=a.ControllingArea 
							AND b.RowIsCurrent=a.RowIsCurrent 
							AND b.validto=a.validto);

DROP TABLE IF EXISTS max_holder_702;
CREATE TABLE max_holder_702(maxid)
AS
SELECT ifnull(max(fact_salesorderlifeid),0)
FROM fact_salesorderlife;

DELETE FROM fact_salesorderlife_so_tmp;

/*Take into fact_salesorder_useinsub all the SOs, SO Items and SO Schedules that already exist in the fact*/
DROP TABLE IF EXISTS fact_salesorder_useinsub;
CREATE TABLE fact_salesorder_useinsub AS SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo FROM fact_salesorderlife;

CALL VECTORWISE(COMBINE 'fact_salesorder_useinsub');

/*Create table TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP with SO,Item and Schedule*/
DROP TABLE IF EXISTS TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP;
CREATE TABLE TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP
AS
SELECT * 
  FROM fact_salesorder_useinsub 
 WHERE 1=2;

 /*Insert into TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP the SOs,Items and Schedules which have a valis SO Item Create Date*/
INSERT INTO TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP
SELECT DISTINCT VBAK_VBELN,VBAP_POSNR,VBEP_ETENR
FROM VBAK_VBAP_VBEP
WHERE EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1);

/*Insert into TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP the SOs,Items and Schedules which are already in the fact*/
CALL VECTORWISE(COMBINE 'TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP-fact_salesorder_useinsub');

/*Insert into TMP2_INS_AFS_SOF_VBAK_VBAP_VBEP the new SOs and all their related info from VBAK_VBAP_VBEP */
DROP TABLE IF EXISTS TMP2_INS_AFS_SOF_VBAK_VBAP_VBEP;
CREATE TABLE TMP2_INS_AFS_SOF_VBAK_VBAP_VBEP
AS
SELECT distinct v.*
FROM VBAK_VBAP_VBEP v,TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP f
WHERE f.dd_SalesDocNo = VBAK_VBELN AND f.dd_SalesItemNo = VBAP_POSNR AND f.dd_ScheduleNo = VBEP_ETENR;
    
INSERT INTO fact_salesorderlife_so_tmp(
		    fact_salesorderlifeid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
              ct_PriceUnit,
              amt_ScheduleTotal,
              amt_StdCost,
              amt_TargetValue,
	          amt_Tax,
              ct_TargetQty,
              amt_ExchangeRate,
              amt_ExchangeRate_GBL,
              ct_OverDlvrTolerance,
              ct_UnderDlvrTolerance,
              Dim_DateidSalesOrderCreated,
              Dim_DateidFirstDate,
              Dim_DateidSchedDeliveryReq,
              Dim_DateidSchedDlvrReqPrev,
              Dim_DateidSchedDelivery,
              Dim_DateidGoodsIssue,
              Dim_DateidMtrlAvail,
              Dim_DateidLoading,
              Dim_DateidTransport,
              Dim_DateidGuaranteedate,
              Dim_Currencyid,
              Dim_ProductHierarchyid,
              Dim_Plantid,
              Dim_Companyid,
              Dim_StorageLocationid,
              Dim_SalesDivisionid,
              Dim_ShipReceivePointid,
              Dim_DocumentCategoryid,
              Dim_SalesDocumentTypeid,
              Dim_SalesOrgid,
              Dim_CustomerID,
              Dim_DateidValidFrom,
              Dim_DateidValidTo,
              Dim_SalesGroupid,
              Dim_CostCenterid,
              Dim_ControllingAreaid,
              Dim_BillingBlockid,
              Dim_TransactionGroupid,
              Dim_SalesOrderRejectReasonid,
              Dim_Partid,
              Dim_SalesOrderHeaderStatusid,
              Dim_SalesOrderItemStatusid,
              Dim_CustomerGroup1id,
              Dim_CustomerGroup2id,
              Dim_SalesOrderItemCategoryid,
              Dim_ScheduleLineCategoryId,
              dd_ItemRelForDelv,
              Dim_ProfitCenterId,
              Dim_DistributionChannelId,
              dd_BatchNo,
              dd_CreatedBy,
	          Dim_DateidNextDate,
              Dim_RouteId,
              Dim_SalesRiskCategoryId,
              Dim_CustomerRiskCategoryId,
	      Dim_DateIdFixedValue,
	      Dim_PaymentTermsId,
	      Dim_DateIdNetDueDate,
	      dim_Currencyid_TRA,
	      dim_Currencyid_GBL,
	      dim_currencyid_STAT,
	      amt_exchangerate_STAT,
	      Dim_AvailabilityCheckId
	    )
    SELECT
            max_holder_702.maxid + row_number() over(order by vbak_vbeln,vbap_posnr,vbep_etenr),
            vbak_vbeln dd_SalesDocNo,
            vbap_posnr dd_SalesItemNo,
            vbep_etenr dd_ScheduleNo,
            vbep_wmeng ct_ScheduleQtySalesUnit,
            vbep_bmeng ct_ConfirmedQty,
            vbep_cmeng ct_CorrectedQty,
            ifnull(Decimal((vbap_netpr ),18,4), 0) amt_UnitPrice,
            vbap_kpein ct_PriceUnit,
            Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') AND VBAP_NETPR > 0 AND VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(Round((vbep_wmeng * (VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )),2), 0) 
			ELSE ifnull((vbep_wmeng * vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end )), 0) 
		   END ,18,4) amt_ScheduleTotal,
            Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
			THEN vbap_wavwr
			ELSE 0
		    END ),18,4) amt_StdCost,
            Decimal((CASE WHEN VBEP_ETENR = y._SalesSchedNo
			    THEN vbap_zwert
			    ELSE 0
			END ),18,4) amt_TargetValue,
            decimal(0.000,18,4) amt_Tax,
            ifnull(CASE WHEN VBEP_ETENR = y._SalesSchedNo
                  THEN vbap_zmeng
                  ELSE 0
            END,0) ct_TargetQty,
            ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
		WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency = co.currency AND z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate,
	    ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
		WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency = pGlobalCurrency AND z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate_GBL,
            vbap_uebto ct_OverDlvrTolerance,
            vbap_untto ct_UnderDlvrTolerance,
            1  Dim_DateidSalesOrderCreated,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_edatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDelivery,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_wadat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGoodsIssue,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_mbdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidMtrlAvail,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_lddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidLoading,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbep_tddat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidTransport,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
            ifnull((SELECT Dim_Currencyid
                    FROM Dim_Currency cur
                    WHERE cur.CurrencyCode = co.Currency),1) Dim_Currencyid,
            ifnull((SELECT Dim_ProductHierarchyid
                    FROM Dim_ProductHierarchy ph
                    WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
            pl.Dim_Plantid,
            co.Dim_Companyid,
            ifnull((SELECT Dim_StorageLocationid
                    FROM Dim_StorageLocation sl
                    WHERE sl.LocationCode = vbap_lgort AND sl.plant = vbap_werks),1) Dim_StorageLocationid,
            ifnull((SELECT Dim_SalesDivisionid
                    FROM Dim_SalesDivision sd
                    WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
            ifnull((SELECT Dim_ShipReceivePointid
                    FROM Dim_ShipReceivePoint srp
                    WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
            ifnull((SELECT Dim_DocumentCategoryid
                    FROM Dim_DocumentCategory dc
                    WHERE dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
            ifnull((SELECT Dim_SalesDocumentTypeid
                    FROM Dim_SalesDocumentType sdt
                    WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
            ifnull((SELECT Dim_SalesOrgid
                    FROM Dim_SalesOrg so
                    WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
            ifnull((SELECT Dim_CustomerID
                    FROM Dim_Customer cust
                    WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date vf
                    WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
            ifnull((SELECT Dim_Dateid
                    FROM Dim_Date vt
                    WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
            ifnull((SELECT Dim_SalesGroupid
                    FROM Dim_SalesGroup sg
                    WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
            ifnull((SELECT Dim_CostCenterid
                    FROM Dim_CostCenter_first1_702 cc 
                    WHERE cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs AND cc.RowIsCurrent = 1
                    ) ,1) Dim_CostCenterid,
            ifnull((SELECT Dim_ControllingAreaid
                    FROM Dim_ControllingArea ca
                    WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
            ifnull((SELECT Dim_BillingBlockid
                    FROM Dim_BillingBlock bb
                    WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
            ifnull((SELECT Dim_TransactionGroupid
                    FROM Dim_TransactionGroup tg
                    WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
            ifnull((SELECT Dim_SalesOrderRejectReasonid
                    FROM Dim_SalesOrderRejectReason sorr
                    WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
            ifnull((SELECT dim_partid
                      FROM dim_part dp
                      WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
            ifnull((SELECT Dim_SalesOrderHeaderStatusid
                      FROM Dim_SalesOrderHeaderStatus sohs
                      WHERE sohs.SalesDocumentNumber = VBAK_VBELN),1) Dim_SalesOrderHeaderStatusid,
            ifnull((SELECT Dim_SalesOrderItemStatusid
                      FROM Dim_SalesOrderItemStatus sois
                      WHERE sois.SalesDocumentNumber = VBAK_VBELN AND sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
            ifnull((SELECT Dim_CustomerGroup1id
                      FROM Dim_CustomerGroup1 cg1
                      WHERE cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
            ifnull((SELECT Dim_CustomerGroup2id
                      FROM Dim_CustomerGroup2 cg2
                      WHERE cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
            ifnull((SELECT soic.Dim_SalesOrderItemCategoryid
                      FROM dim_salesorderitemcategory soic
                      WHERE soic.SalesOrderItemCategory = VBAP_PSTYV),1) Dim_SalesOrderItemCategoryid,
            ifnull((SELECT slc.Dim_ScheduleLineCategoryId
                    FROM Dim_ScheduleLineCategory slc
                    WHERE slc.ScheduleLineCategory = VBEP_ETTYP),1) Dim_ScheduleLineCategoryId,
            ifnull(vbep_lfrel,'Not Set') dd_ItemRelForDelv,
            1 Dim_ProfitCenterId,
            ifnull((SELECT dc.Dim_DistributionChannelid
                      FROM dim_DistributionChannel dc
                      WHERE   dc.DistributionChannelCode = VBAK_VTWEG
                          AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
            ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
            ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
	ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
	 ifnull((SELECT r.dim_routeid FROM dim_route r WHERE
                       r.RouteCode = VBAP_ROUTE AND r.RowIsCurrent = 1),1),
	  ifnull((SELECT src.Dim_SalesRiskCategoryId FROM Dim_SalesRiskCategory src WHERE
                       src.SalesRiskCategory = VBAK_CTLPC AND src.CreditControlArea = VBAK_KKBER AND src.RowIsCurrent = 1),1),
	1 Dim_CustomerRiskCategoryId,
	1 Dim_DateIdFixedValue,
	1 Dim_PaymentTermsId,
	1 Dim_DateIdNetDueDate,

    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = var.pGlobalCurrency),1) dim_Currencyid_GBL,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,
	 ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1) amt_exchangerate_STAT	,
	 1
FROM max_holder_702, TMP2_INS_AFS_SOF_VBAK_VBAP_VBEP
    INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
    INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
	INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
    INNER JOIN VBAK_VBAP_VBEP_702 y
          ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo,
	variable_holder_fact_salesorderlife var;

CALL VECTORWISE(COMBINE 'fact_salesorderlife_so_tmp');

/* Moved stat curr UPDATE here for performance issues*/
DROP TABLE IF EXISTS vvv3_i89;
CREATE TABLE vvv3_i89 AS SELECT distinct VBAK_VBELN,VBAP_POSNR,ifnull(PRSDT,VBAP_ERDAT) upd1 FROM VBAK_VBAP_VBEP;

\i /db/schema_migration/bin/wrapper_optimizedb.sh vvv3_i89;
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorderlife_so_tmp;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_getExchangeRate1;

UPDATE fact_salesorderlife_so_tmp f
  FROM tmp_getExchangeRate1 z,Dim_Currency curstat,Dim_Currency curtran,vvv3_i89
   SET amt_exchangerate_STAT = z.exchangeRate
 WHERE f.Dim_Currencyid_TRA = curtran.Dim_Currencyid
   AND varchar(z.pFromCurrency,3)  = varchar(curtran.CurrencyCode,3)
   AND f.dim_currencyid_STAT = curstat.Dim_Currencyid
   AND varchar(z.pToCurrency,3) = varchar(curstat.CurrencyCode,3)
   AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' 
   AND ansidate(z.pDate)  = ansidate(upd1)
   AND z.pFromExchangeRate = 0 
   AND  f.dd_SalesDocNo = VBAK_VBELN
   AND f.dd_SalesItemNo = VBAP_POSNR;

DROP TABLE IF EXISTS vvv3_i89;

UPDATE fact_salesorderlife_so_tmp fo
  FROM VBAK_VBAP_VBEP
 INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
 INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
 INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
   SET amt_Tax  = ifnull(Decimal(((vbap_mwsbp / CASE WHEN ifnull(VBAP_NETWR,0) = 0 THEN 1
					WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') AND VBAP_NETPR > 0 AND VBAP_NETPR <> ROUND((VBAP_KPEIN * (VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)),2)
					THEN CASE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
							WHEN 0 THEN 1
							ELSE ifnull(Round((VBAP_NETWR / (CASE WHEN ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) = 0 THEN 1 ELSE ((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end) / (case when vbap_kpein=0 then 1 else vbap_kpein end )) END)),2),1)
						END
					ELSE ifnull((VBAP_NETWR / (CASE WHEN (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) = 0 THEN 1 ELSE (vbap_netpr / (case when vbap_kpein=0 then 1 else vbap_kpein end)) END)),1)
				 END) 
			* (vbep_bmeng )),18,4), 0) 
 WHERE fo.dd_SalesDocNo = VBAK_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = VBEP_ETENR;

UPDATE fact_salesorderlife_so_tmp fo
  FROM VBAK_VBAP_VBEP
   SET amt_unitpriceuom = Decimal((CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') AND VBAP_NETPR > 0 AND VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END) ,18,4)
 WHERE fo.dd_SalesDocNo = VBAK_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = VBEP_ETENR
   AND (vbap_netpr > 0 OR VBAP_NETWR > 0);

UPDATE fact_salesorderlife_so_tmp fo
  FROM VBAK_VBAP_VBEP, Dim_UnitOfMeasure uom
   SET fo.Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAK_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = VBEP_ETENR
   AND VBAP_KMEIN IS NOT NULL
   AND uom.UOM = vbap_kmein
   AND uom.RowIsCurrent = 1
   AND fo.Dim_UnitOfMeasureId <> uom.Dim_UnitOfMeasureId;

UPDATE fact_salesorderlife_so_tmp fo
  FROM VBAK_VBAP_VBEP
   SET fo.Dim_UnitOfMeasureId = 1
 WHERE fo.dd_SalesDocNo = VBAK_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = VBEP_ETENR
   AND VBAP_KMEIN IS NULL
   AND fo.Dim_UnitOfMeasureId <> 1;

UPDATE fact_salesorderlife_so_tmp fo
  FROM VBAK_VBAP_VBEP, Dim_UnitOfMeasure uom
   SET fo.Dim_BaseUoMid = uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAK_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = VBEP_ETENR
   AND vbap_meins IS NOT NULL
   AND uom.UOM = vbap_meins
   AND uom.RowIsCurrent = 1
   AND fo.Dim_BaseUoMid <> uom.Dim_UnitOfMeasureId;

UPDATE fact_salesorderlife_so_tmp fo
  FROM VBAK_VBAP_VBEP
   SET fo.Dim_BaseUoMid = 1
 WHERE fo.dd_SalesDocNo = VBAK_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = VBEP_ETENR
   AND vbap_meins IS NULL
   AND fo.Dim_BaseUoMid <> 1;

UPDATE fact_salesorderlife_so_tmp fo
  FROM VBAK_VBAP_VBEP, Dim_UnitOfMeasure uom
   SET fo.Dim_SalesUoMid = uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAK_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = VBEP_ETENR
   AND vbap_vrkme IS NOT NULL
   AND uom.UOM = vbap_vrkme
   AND uom.RowIsCurrent = 1
   AND fo.Dim_SalesUoMid <> uom.Dim_UnitOfMeasureId;

UPDATE fact_salesorderlife_so_tmp fo
  FROM VBAK_VBAP_VBEP
   SET fo.Dim_SalesUoMid = 1
 WHERE fo.dd_SalesDocNo = VBAK_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = VBEP_ETENR
   AND vbap_vrkme IS NULL
   AND fo.Dim_SalesUoMid <> 1;

CALL VECTORWISE (COMBINE 'fact_salesorderlife_so_tmp');

DROP TABLE IF EXISTS tmp_upd_702;

CREATE TABLE tmp_upd_702 AS 
SELECT FIRST 0 src.dim_salesriskcategoryid updcol1, c.CUSTOMER  updVBAK_KUNNR,src.CreditControlArea  updVBAK_KKBER
FROM ukmbp_cms a
      INNER JOIN BUT000 b on a.PARTNER = b.PARTNER
      INNER JOIN cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
      INNER JOIN dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1
ORDER BY c.customer;

DROP TABLE IF EXISTS fact_salesorder_tmp_Dim_DateidSalesOrderCreated_del;
CREATE TABLE fact_salesorder_tmp_Dim_DateidSalesOrderCreated_del
AS
SELECT distinct dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
FROM fact_salesorder_useinsub;

DROP TABLE IF EXISTS fact_salesorder_tmp_Dim_DateidSalesOrderCreated_INS;
CREATE TABLE fact_salesorder_tmp_Dim_DateidSalesOrderCreated_INS
AS
SELECT * FROM fact_salesorder_tmp_Dim_DateidSalesOrderCreated_del 
WHERE 1=2;

INSERT INTO fact_salesorder_tmp_Dim_DateidSalesOrderCreated_INS
SELECT DISTINCT VBAK_VBELN,VBAP_POSNR,VBEP_ETENR FROM VBAK_VBAP_VBEP;

CALL VECTORWISE(COMBINE 'fact_salesorder_tmp_Dim_DateidSalesOrderCreated_INS-fact_salesorder_tmp_Dim_DateidSalesOrderCreated_del');

DROP TABLE IF EXISTS VBAK_VBAP_VBEP_tmp_notexists;
CREATE TABLE VBAK_VBAP_VBEP_tmp_notexists
AS
SELECT DISTINCT v.*
FROM VBAK_VBAP_VBEP v,fact_salesorder_tmp_Dim_DateidSalesOrderCreated_INS f
WHERE v.VBAK_VBELN = dd_SalesDocNo AND v.VBAP_POSNR =  dd_SalesItemNo AND v.VBEP_ETENR = f.dd_ScheduleNo ;

UPDATE fact_salesorderlife_so_tmp fst
  FROM VBAK_VBAP_VBEP_tmp_notexists
 INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
 INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
 INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
 INNER JOIN VBAK_VBAP_VBEP_702 y ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
   SET Dim_CustomerRiskCategoryId= ifnull((SELECT  updcol1 FROM tmp_upd_702 WHERE updVBAK_KUNNR = VBAK_KUNNR AND updVBAK_KKBER=VBAK_KKBER),1)
 WHERE EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
   AND fst.dd_SalesDocNo = VBAK_VBELN AND fst.dd_SalesItemNo = VBAP_POSNR AND fst.dd_ScheduleNo = VBEP_ETENR;

DROP TABLE IF EXISTS tmp_upd_702;

UPDATE fact_salesorderlife_so_tmp fst
FROM VBAK_VBAP_VBEP_tmp_notexists
        INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
        INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
        INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
        INNER JOIN VBAK_VBAP_VBEP_702 y ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
SET Dim_DateidSalesOrderCreated = ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1)
          WHERE EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
AND cast(fst.dd_SalesDocNo AS varchar(10)) = cast(VBAK_VBELN AS varchar(10)) AND fst.dd_SalesItemNo = VBAP_POSNR AND fst.dd_ScheduleNo = VBEP_ETENR
AND fst.Dim_DateidSalesOrderCreated <> ifnull((SELECT Dim_Dateid
                    FROM Dim_Date dd
                    WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1);

DROP TABLE IF EXISTS fact_salesorder_tmp_Dim_DateidSalesOrderCreated_del;
DROP TABLE IF EXISTS fact_salesorder_tmp_Dim_DateidSalesOrderCreated_INS;

UPDATE fact_salesorderlife_so_tmp fst
From VBAK_VBAP_VBEP_tmp_notexists
        INNER JOIN Dim_Plant pl ON pl.PlantCode = VBAP_WERKS
        INNER JOIN Dim_Company co ON co.CompanyCode = pl.CompanyCode
        INNER JOIN VBAK_VBAP_VBEP_702  y ON VBAK_VBELN = y._SaleseDocNo AND VBAP_POSNR = y._SalesItemNo
SET Dim_ProfitCenterId = ifnull((SELECT  pc.dim_profitcenterid
                      FROM dim_profitcenter pc
                      WHERE   pc.ProfitCenterCode = VBAP_PRCTR
                          AND pc.ControllingArea = VBAK_KOKRS
                          AND pc.ValidTo >= VBAK_ERDAT
                          AND pc.RowIsCurrent = 1 ),1)
WHERE EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
AND cast(fst.dd_SalesDocNo AS varchar(10)) = cast(VBAK_VBELN AS varchar(10))  AND fst.dd_SalesItemNo = VBAP_POSNR AND fst.dd_ScheduleNo = VBEP_ETENR;

DROP TABLE IF EXISTS fact_salesorder_useinsub;

CALL VECTORWISE (COMBINE 'fact_salesorderlife_so_tmp');

DROP TABLE IF EXISTS staging_upd_702;
CREATE TABLE staging_upd_702 AS 
SELECT    vbap_kwmeng ct_ScheduleQtySalesUnit,
          vbap_kbmeng ct_ConfirmedQty ,
          0.0000 ct_CorrectedQty ,
          ifnull(Decimal((vbap_netpr ),18,4), 0) amt_UnitPrice,
          vbap_kpein ct_PriceUnit,
          decimal((vbap_netwr ),18,4) amt_ScheduleTotal,
          ifnull(decimal((vbap_wavwr ),18,4),0) amt_StdCost,
          ifnull(decimal((vbap_zwert ),18,4),0) amt_TargetValue,
          ifnull(decimal(vbap_mwsbp ,18,4), 0) amt_Tax,
          vbap_zmeng ct_TargetQty,
          ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
		WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency = co.Currency AND z.pFromExchangeRate = 0),1)  amt_ExchangeRate,
	  ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
		WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ANSIDATE(LOCAL_TIMESTAMP) AND z.pToCurrency = pGlobalCurrency AND z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate_GBL,
          vbap_uebto ct_OverDlvrTolerance,
          vbap_untto ct_UnderDlvrTolerance,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          1 Dim_DateidSchedDelivery,
          1 Dim_DateidGoodsIssue,
          1 Dim_DateidMtrlAvail,
          1 Dim_DateidLoading,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
          1 Dim_DateidTransport,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.Currency),1) Dim_Currencyid,
          ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid Dim_Plantid,
         co.Dim_Companyid Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort AND sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          1 Dim_ScheduleLineCategoryId,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
	  1 Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((SELECT Dim_SalesOrderHeaderStatusid
                    FROM Dim_SalesOrderHeaderStatus sohs
                    WHERE sohs.SalesDocumentNumber = VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
          ifnull((SELECT sois.Dim_SalesOrderItemStatusid
                    FROM Dim_SalesOrderItemStatus sois
                    WHERE sois.SalesDocumentNumber = VBAP_VBELN AND sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
          ifnull((SELECT cg1.Dim_CustomerGroup1id
                    FROM Dim_CustomerGroup1 cg1
                    WHERE cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
          ifnull((SELECT cg2.Dim_CustomerGroup2id
                    FROM Dim_CustomerGroup2 cg2
                    WHERE cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
          soic.Dim_SalesOrderItemCategoryid Dim_salesorderitemcategoryid,
          'Not Set' dd_ItemRelForDelv,
	  1 Dim_ProfitCenterId,
          ifnull((SELECT dc.Dim_DistributionChannelid
                    FROM dim_DistributionChannel dc
                    WHERE   dc.DistributionChannelCode = VBAK_VTWEG
                        AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
          ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
          ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
          ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
          ifnull((SELECT r.dim_routeid FROM dim_route r WHERE
                       r.RouteCode = VBAP_ROUTE AND r.RowIsCurrent = 1),1) Dim_RouteId,
          ifnull((SELECT src.Dim_SalesRiskCategoryId FROM Dim_SalesRiskCategory src WHERE
                       src.SalesRiskCategory = VBAK_CTLPC AND src.CreditControlArea = VBAK_KKBER AND src.RowIsCurrent = 1),1) Dim_SalesRiskCategoryId,
	   'Not Set' dd_CreditRep,
	   'Not Set' dd_CreditMgr,
	     decimal(0,18,4) dd_CreditLimit,
	    1 Dim_CustomerRiskCategoryId,
	dd_SalesDocNo,
	dd_SalesItemNo,
	dd_ScheduleNo,
	Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') AND VBAP_NETPR > 0 AND VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END ,18,4) amt_UnitPriceUoM,
	1 Dim_DateIdFixedValue,
	1 Dim_PaymentTermsId,
	1 Dim_DateIdNetDueDate,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = var.pGlobalCurrency),1) dim_Currencyid_GBL,
    ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,
	ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1) amt_exchangerate_STAT

 FROM fact_salesorderlife so,
      VBAK_VBAP,
      dim_SalesOrderItemCategory soic,
      Dim_Plant pl,
      Dim_Company co,
	  variable_holder_fact_salesorderlife var
 WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
      AND VBAP_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo AND
      soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 AND dd_ScheduleNo = 0;

DROP TABLE IF EXISTS tmp_upd_702;
CREATE TABLE tmp_upd_702 AS
SELECT FIRST 0 Dim_CostCenterid,Code,ControllingArea,RowIsCurrent
  FROM Dim_CostCenter cc
ORDER BY cc.ValidTo;

UPDATE staging_upd_702 tu
  FROM VBAK_VBAP,
       dim_SalesOrderItemCategory soic,
       Dim_Plant pl,
       Dim_Company co
   SET Dim_CostCenterid=ifnull((SELECT  Dim_CostCenterid FROM tmp_upd_702 cc
				WHERE cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs AND cc.RowIsCurrent = 1),1)
 WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
   AND VBAP_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo 
   AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 
   AND dd_ScheduleNo = 0;

UPDATE staging_upd_702 tu
  FROM VBAK_VBAP,
       dim_SalesOrderItemCategory soic,
       Dim_Plant pl,
       Dim_Company co
   SET Dim_ProfitCenterId=ifnull((SELECT  pc.dim_profitcenterid
                       FROM dim_profitcenter pc
                      WHERE pc.ProfitCenterCode = VBAP_PRCTR
                        AND pc.ControllingArea = VBAK_KOKRS
                        AND pc.ValidTo >= VBAK_ERDAT
                        AND pc.RowIsCurrent = 1 ),1)
 WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
   AND VBAP_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo 
   AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 
   AND dd_ScheduleNo = 0;

DROP TABLE IF EXISTS tmp_upd_702;
			   
CREATE TABLE tmp_upd_702 AS 
SELECT FIRST 0 src.dim_salesriskcategoryid updcol1, c.CUSTOMER updVBAK_KUNNR,src.CreditControlArea updVBAK_KKBER
  FROM ukmbp_cms a
      INNER JOIN BUT000 b on a.PARTNER = b.PARTNER
      INNER JOIN cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
      INNER JOIN dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1
ORDER BY c.customer  ;

UPDATE staging_upd_702 tu
  FROM VBAK_VBAP,
       dim_SalesOrderItemCategory soic,
       Dim_Plant pl,
       Dim_Company co
   SET Dim_CustomerRiskCategoryId= ifnull((SELECT  updcol1 FROM tmp_upd_702 WHERE updVBAK_KUNNR = VBAK_KUNNR AND updVBAK_KKBER=VBAK_KKBER),1)
 WHERE pl.PlantCode = VBAP_WERKS AND co.CompanyCode = pl.CompanyCode
   AND VBAP_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo 
   AND soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1 
   AND dd_ScheduleNo = 0;

DROP TABLE IF EXISTS tmp_upd_702;

UPDATE fact_salesorderlife fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET fo.Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND VBAP_KMEIN IS NOT NULL
   AND uom.UOM = vbap_kmein
   AND uom.RowIsCurrent = 1
   AND ifnull(fo.Dim_UnitOfMeasureId,-1) <> ifnull(uom.Dim_UnitOfMeasureId,-2);

UPDATE fact_salesorderlife fo
  FROM VBAK_VBAP
   SET fo.Dim_UnitOfMeasureId = 1
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND VBAP_KMEIN IS NULL
   AND fo.Dim_UnitOfMeasureId <> 1;

UPDATE fact_salesorderlife fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET fo.Dim_BaseUoMid =  uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND vbap_meins IS NOT NULL
   AND uom.UOM = vbap_meins
   AND uom.RowIsCurrent = 1
   AND ifnull(fo.Dim_BaseUoMid,-1) <> ifnull(uom.Dim_UnitOfMeasureId,-2);

UPDATE fact_salesorderlife fo
  FROM VBAK_VBAP
   SET fo.Dim_BaseUoMid = 1
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND vbap_meins IS NULL
   AND ifnull(fo.Dim_BaseUoMid,-1) <> 1;

UPDATE fact_salesorderlife fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET fo.Dim_SalesUoMid = uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND vbap_vrkme IS NOT NULL
   AND uom.UOM = vbap_vrkme
   AND uom.RowIsCurrent = 1
   AND ifnull(fo.Dim_SalesUoMid,-1) <> ifnull(uom.Dim_UnitOfMeasureId,-2);

UPDATE fact_salesorderlife fo
  FROM VBAK_VBAP
   SET fo.Dim_SalesUoMid = 1
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND vbap_vrkme IS NULL
   AND ifnull(fo.Dim_SalesUoMid,-1) <> 1;

call vectorwise (combine 'fact_salesorderlife');

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.ct_ScheduleQtySalesUnit=su.ct_ScheduleQtySalesUnit 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND ifnull(so.ct_ScheduleQtySalesUnit,-1) <> ifnull(su.ct_ScheduleQtySalesUnit,-2);

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.ct_ConfirmedQty = su.ct_ConfirmedQty  
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND ifnull(so.ct_ConfirmedQty,-1) <> ifnull(su.ct_ConfirmedQty,-2);

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.ct_CorrectedQty =su.ct_CorrectedQty  
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.ct_CorrectedQty <> su.ct_CorrectedQty ;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.amt_UnitPrice=su.amt_UnitPrice 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_UnitPrice <> su.amt_UnitPrice ;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.ct_PriceUnit=su.ct_PriceUnit
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.ct_PriceUnit <> su.ct_PriceUnit;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.amt_ScheduleTotal=su.amt_ScheduleTotal 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_ScheduleTotal <> su.amt_ScheduleTotal;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.amt_StdCost=su.amt_StdCost 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_StdCost <> su.amt_StdCost;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.amt_TargetValue=su.amt_TargetValue 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_TargetValue <> su.amt_TargetValue;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.amt_Tax=su.amt_Tax 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_Tax <> su.amt_Tax;

UPDATE fact_salesorderlife so 
FROM staging_upd_702 su 
SET so.ct_TargetQty=su.ct_TargetQty 
WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
AND so.ct_TargetQty <> su.ct_TargetQty;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.amt_ExchangeRate=su.amt_ExchangeRate 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_ExchangeRate <> su.amt_ExchangeRate;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.amt_ExchangeRate_GBL=ifnull(su.amt_ExchangeRate_GBL ,1)
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_ExchangeRate_GBL <> ifnull(su.amt_ExchangeRate_GBL ,1);

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.ct_OverDlvrTolerance=su.ct_OverDlvrTolerance 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.ct_OverDlvrTolerance <> su.ct_OverDlvrTolerance;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.ct_UnderDlvrTolerance=su.ct_UnderDlvrTolerance 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.ct_UnderDlvrTolerance <> su.ct_UnderDlvrTolerance;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidSalesOrderCreated=su.Dim_DateidSalesOrderCreated 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidSalesOrderCreated <> su.Dim_DateidSalesOrderCreated;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidFirstDate=su.Dim_DateidFirstDate 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidFirstDate <> su.Dim_DateidFirstDate;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidSchedDeliveryReq=su.Dim_DateidSchedDeliveryReq 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidSchedDeliveryReq <> su.Dim_DateidSchedDeliveryReq ;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidSchedDelivery=su.Dim_DateidSchedDelivery 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidSchedDelivery <> su.Dim_DateidSchedDelivery;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidGoodsIssue=su.Dim_DateidGoodsIssue 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidGoodsIssue <> su.Dim_DateidGoodsIssue;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidMtrlAvail=su.Dim_DateidMtrlAvail 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidMtrlAvail <> su.Dim_DateidMtrlAvail;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidLoading=su.Dim_DateidLoading 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidLoading <> su.Dim_DateidLoading;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidGuaranteedate=su.Dim_DateidGuaranteedate 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidGuaranteedate <> su.Dim_DateidGuaranteedate;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidTransport=su.Dim_DateidTransport 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidTransport <> su.Dim_DateidTransport;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_Currencyid=su.Dim_Currencyid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_Currencyid <> su.Dim_Currencyid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_ProductHierarchyid=su.Dim_ProductHierarchyid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_ProductHierarchyid <> su.Dim_ProductHierarchyid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_Plantid=su.Dim_Plantid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_Plantid <> su.Dim_Plantid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_Companyid=su.Dim_Companyid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_Companyid <> su.Dim_Companyid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_StorageLocationid=su.Dim_StorageLocationid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_StorageLocationid <> su.Dim_StorageLocationid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesDivisionid=su.Dim_SalesDivisionid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesDivisionid <> su.Dim_SalesDivisionid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_ShipReceivePointid=su.Dim_ShipReceivePointid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesDivisionid <> su.Dim_SalesDivisionid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DocumentCategoryid=su.Dim_DocumentCategoryid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DocumentCategoryid <> su.Dim_DocumentCategoryid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesDocumentTypeid=su.Dim_SalesDocumentTypeid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesDocumentTypeid <> su.Dim_SalesDocumentTypeid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesOrgid=su.Dim_SalesOrgid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesOrgid <> su.Dim_SalesOrgid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_CustomerID=su.Dim_CustomerID 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_CustomerID <> su.Dim_CustomerID;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_ScheduleLineCategoryId=su.Dim_ScheduleLineCategoryId 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_ScheduleLineCategoryId <> su.Dim_ScheduleLineCategoryId;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidValidFrom=su.Dim_DateidValidFrom 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidValidFrom <> su.Dim_DateidValidFrom;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidValidTo=su.Dim_DateidValidTo 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidValidTo <> su.Dim_DateidValidTo;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesGroupid=su.Dim_SalesGroupid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesGroupid <> su.Dim_SalesGroupid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_CostCenterid=su.Dim_CostCenterid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_CostCenterid <> su.Dim_CostCenterid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_ControllingAreaid=su.Dim_ControllingAreaid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_ControllingAreaid <> su.Dim_ControllingAreaid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_BillingBlockid=su.Dim_BillingBlockid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_BillingBlockid <> su.Dim_BillingBlockid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_TransactionGroupid=su.Dim_TransactionGroupid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_TransactionGroupid <> su.Dim_TransactionGroupid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesOrderRejectReasonid=su.Dim_SalesOrderRejectReasonid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesOrderRejectReasonid <> su.Dim_SalesOrderRejectReasonid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_Partid=su.Dim_Partid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_Partid <> su.Dim_Partid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesOrderHeaderStatusid=su.Dim_SalesOrderHeaderStatusid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesOrderHeaderStatusid <> su.Dim_SalesOrderHeaderStatusid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesOrderItemStatusid=su.Dim_SalesOrderItemStatusid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesOrderItemStatusid <> su.Dim_SalesOrderItemStatusid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_CustomerGroup1id=su.Dim_CustomerGroup1id 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_CustomerGroup1id <> su.Dim_CustomerGroup1id;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_CustomerGroup2id=su.Dim_CustomerGroup2id 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_CustomerGroup2id <> su.Dim_CustomerGroup2id;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_salesorderitemcategoryid=su.Dim_salesorderitemcategoryid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_salesorderitemcategoryid <> su.Dim_salesorderitemcategoryid;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.dd_ItemRelForDelv=su.dd_ItemRelForDelv 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.dd_ItemRelForDelv <> su.dd_ItemRelForDelv;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_ProfitCenterId=su.Dim_ProfitCenterId 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_ProfitCenterId <> su.Dim_ProfitCenterId;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DistributionChannelId=su.Dim_DistributionChannelId 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DistributionChannelId <> su.Dim_DistributionChannelId;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.dd_BatchNo=su.dd_BatchNo 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.dd_BatchNo <> su.dd_BatchNo;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.dd_CreatedBy=su.dd_CreatedBy 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.dd_CreatedBy <> su.dd_CreatedBy;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_DateidNextDate=su.Dim_DateidNextDate 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_DateidNextDate <> su.Dim_DateidNextDate;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_RouteId=su.Dim_RouteId 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_RouteId <> su.Dim_RouteId;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesRiskCategoryId=su.Dim_SalesRiskCategoryId 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesRiskCategoryId <> su.Dim_SalesRiskCategoryId;

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_CustomerRiskCategoryId=su.Dim_CustomerRiskCategoryId 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_CustomerRiskCategoryId <> su.Dim_CustomerRiskCategoryId;

/* UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_UnitOfMeasureId=su.Dim_UnitOfMeasureId 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_UnitOfMeasureId <> su.Dim_UnitOfMeasureId

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_BaseUoMid=su.Dim_BaseUoMid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_BaseUoMid <> su.Dim_BaseUoMid

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.Dim_SalesUoMid=su.Dim_SalesUoMid 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.Dim_SalesUoMid <> su.Dim_SalesUoMid */

UPDATE fact_salesorderlife so 
  FROM staging_upd_702 su 
   SET so.amt_UnitPriceUoM=su.amt_UnitPriceUoM 
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_UnitPriceUoM <> su.amt_UnitPriceUoM;

/*LK: 9 Sep 2013: Currency AND exchg rate changes */
UPDATE fact_salesorderlife so
  FROM staging_upd_702 su
   SET so.dim_Currencyid_TRA=su.dim_Currencyid_TRA
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.dim_Currencyid_TRA <> su.dim_Currencyid_TRA;

UPDATE fact_salesorderlife so
  FROM staging_upd_702 su
   SET so.dim_Currencyid_GBL=su.dim_Currencyid_GBL
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.dim_Currencyid_GBL <> su.dim_Currencyid_GBL;

UPDATE fact_salesorderlife so
  FROM staging_upd_702 su
   SET so.dim_currencyid_STAT=su.dim_currencyid_STAT
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.dim_currencyid_STAT <> su.dim_currencyid_STAT;

UPDATE fact_salesorderlife so
  FROM staging_upd_702 su
   SET so.amt_exchangerate_STAT=su.amt_exchangerate_STAT
 WHERE so.dd_SalesDocNo=su.dd_SalesDocNo AND so.dd_SalesItemNo=su.dd_SalesItemNo AND so.dd_ScheduleNo = su.dd_ScheduleNo
   AND so.amt_exchangerate_STAT <> su.amt_exchangerate_STAT;
/*LK: 9 Sep 2013:  End of  Currency AND exchg rate changes */

CALL VECTORWISE(COMBINE 'fact_salesorderlife');

DROP TABLE IF EXISTS max_holder_702;
CREATE TABLE max_holder_702(maxid)
AS
SELECT ifnull(max(fact_salesorderlifeid),0)
FROM fact_salesorderlife_so_tmp;

DROP TABLE IF EXISTS fact_salesorderlife_so_tmp2;
CREATE TABLE fact_salesorderlife_so_tmp2 AS SELECT * FROM fact_salesorderlife_so_tmp WHERE 1=2;

/* Take all the SOs,Items and Schedules from fact_salesorderlife*/
DROP TABLE IF EXISTS fact_salesorder_useinsub;
CREATE TABLE fact_salesorder_useinsub 
AS 
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo 
FROM fact_salesorderlife;

CALL VECTORWISE(COMBINE 'fact_salesorder_useinsub');

/* Take all the SOs,Items from vbak_vbap_vbep*/
DROP TABLE IF EXISTS vbak_vbap_vbep_useinsub;
CREATE TABLE vbak_vbap_vbep_useinsub AS SELECT VBAK_VBELN,VBAP_POSNR FROM vbak_vbap_vbep ;

CALL VECTORWISE(COMBINE 'vbak_vbap_vbep_useinsub');

INSERT INTO fact_salesorderlife_so_tmp2(
	        fact_salesorderlifeid,
            dd_SalesDocNo,
            dd_SalesItemNo,
            dd_ScheduleNo,
            ct_ScheduleQtySalesUnit,
            ct_ConfirmedQty,
            ct_CorrectedQty,
            amt_UnitPrice,
            ct_PriceUnit,
            amt_ScheduleTotal,
            amt_StdCost,
            amt_TargetValue,
            amt_Tax,
            ct_TargetQty,
            amt_ExchangeRate,
            amt_ExchangeRate_GBL,
            ct_OverDlvrTolerance,
            ct_UnderDlvrTolerance,
            Dim_DateidSalesOrderCreated,
            Dim_DateidFirstDate,
            Dim_DateidSchedDeliveryReq,
            Dim_DateidSchedDlvrReqPrev,
            Dim_DateidSchedDelivery,
            Dim_DateidGoodsIssue,
            Dim_DateidMtrlAvail,
            Dim_DateidLoading,
            Dim_DateidTransport,
            Dim_DateidGuaranteedate,
            Dim_Currencyid,
            Dim_ProductHierarchyid,
            Dim_Plantid,
            Dim_Companyid,
            Dim_StorageLocationid,
            Dim_SalesDivisionid,
            Dim_ShipReceivePointid,
            Dim_DocumentCategoryid,
            Dim_SalesDocumentTypeid,
            Dim_SalesOrgid,
            Dim_CustomerID,
            Dim_DateidValidFrom,
            Dim_DateidValidTo,
            Dim_SalesGroupid,
            Dim_CostCenterid,
            Dim_ControllingAreaid,
            Dim_BillingBlockid,
            Dim_TransactionGroupid,
            Dim_SalesOrderRejectReasonid,
            Dim_Partid,
            Dim_SalesOrderHeaderStatusid,
            Dim_SalesOrderItemStatusid,
            Dim_CustomerGroup1id,
            Dim_CustomerGroup2id,
            Dim_SalesOrderItemCategoryid,
            Dim_ScheduleLineCategoryId,
            dd_ItemRelForDelv,
            Dim_ProfitCenterId,
            Dim_DistributionChannelId,
            dd_BatchNo,
            dd_CreatedBy,
            Dim_DateidNextDate,
            Dim_routeid,
            Dim_SalesRiskCategoryId,
            Dim_CustomerRiskCategoryId,
		    dim_Currencyid_TRA,
		    dim_Currencyid_GBL,
		    dim_currencyid_STAT,
		    amt_exchangerate_STAT,
		    Dim_AvailabilityCheckId)
SELECT
	      max_holder_702.maxid + row_number() over(),
          vbap_vbeln dd_SalesDocNo,
          vbap_posnr dd_SalesItemNo,
          0 dd_ScheduleNo,
          vbap_kwmeng ct_ScheduleQtySalesUnit,
          vbap_kbmeng ct_ConfirmedQty,
          0.0000 ct_CorrectedQty,
          ifnull(Decimal((vbap_netpr ),18,4), 0) amt_UnitPrice,
          vbap_kpein ct_PriceUnit,
          decimal((vbap_netwr ),18,4) amt_ScheduleTotal,
          ifnull(decimal((vbap_wavwr ),18,4),0) amt_StdCost,
          ifnull(decimal((vbap_zwert ),18,4),0) amt_TargetValue,
          ifnull(decimal(vbap_mwsbp ,18,4), 0) amt_Tax,
          vbap_zmeng ct_TargetQty,
          ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
		  WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pToCurrency = co.currency AND z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate,
	      ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
	   	  WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact' AND z.pDate = ANSIDATE(LOCAL_TIMESTAMP) AND z.pFromExchangeRate = 0 ),1)  amt_ExchangeRate_GBL,
          vbap_uebto ct_OverDlvrTolerance,
          vbap_untto ct_UnderDlvrTolerance,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbap_erdat AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSalesOrderCreated,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = VBAP_STADAT AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidFirstDate,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDeliveryReq,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_vdatu AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidSchedDlvrReqPrev,
          1 Dim_DateidSchedDelivery,
          1 Dim_DateidGoodsIssue,
          1 Dim_DateidMtrlAvail,
          1 Dim_DateidLoading,
          1 Dim_DateidTransport,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date dd
                  WHERE dd.DateValue = vbak_gwldt AND dd.CompanyCode = pl.CompanyCode),1) Dim_DateidGuaranteedate,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = co.Currency),1) Dim_Currencyid,
          ifnull((SELECT Dim_ProductHierarchyid
                  FROM Dim_ProductHierarchy ph
                  WHERE ph.ProductHierarchy = vbap_prodh),1) Dim_ProductHierarchyid,
          pl.Dim_Plantid,
          co.Dim_Companyid,
          ifnull((SELECT Dim_StorageLocationid
                  FROM Dim_StorageLocation sl
                  WHERE sl.LocationCode = vbap_lgort AND sl.plant = vbap_werks),1) Dim_StorageLocationid,
          ifnull((SELECT Dim_SalesDivisionid
                  FROM Dim_SalesDivision sd
                  WHERE sd.DivisionCode = vbap_spart),1) Dim_SalesDivisionid,
          ifnull((SELECT Dim_ShipReceivePointid
                  FROM Dim_ShipReceivePoint srp
                  WHERE srp.ShipReceivePointCode = vbap_vstel),1) Dim_ShipReceivePointid,
          ifnull((SELECT Dim_DocumentCategoryid
                  FROM Dim_DocumentCategory dc
                  WHERE  dc.DocumentCategory = vbak_vbtyp),1) Dim_DocumentCategoryid,
          ifnull((SELECT Dim_SalesDocumentTypeid
                  FROM Dim_SalesDocumentType sdt
                  WHERE sdt.DocumentType = vbak_auart),1) Dim_SalesDocumentTypeid,
          ifnull((SELECT Dim_SalesOrgid
                  FROM Dim_SalesOrg so
                  WHERE so.SalesOrgCode = vbak_vkorg),1) Dim_SalesOrgid,
          ifnull((SELECT Dim_CustomerID
                  FROM Dim_Customer cust
                  WHERE cust.CustomerNumber = vbak_kunnr),1) Dim_CustomerID,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vf
                  WHERE vf.DateValue = vbak_guebg AND vf.CompanyCode = pl.CompanyCode),1) Dim_DateidValidFrom,
          ifnull((SELECT Dim_Dateid
                  FROM Dim_Date vt
                  WHERE vt.DateValue = vbak_gueen AND vt.CompanyCode = pl.CompanyCode),1) Dim_DateidValidTo,
          ifnull((SELECT Dim_SalesGroupid
                  FROM Dim_SalesGroup sg
                  WHERE sg.SalesGroupCode = vbak_vkgrp),1) Dim_SalesGroupid,
                  1 Dim_CostCenterid,
          ifnull((SELECT Dim_ControllingAreaid
                  FROM Dim_ControllingArea ca
                  WHERE ca.ControllingAreaCode = vbak_kokrs),1) Dim_ControllingAreaid,
          ifnull((SELECT Dim_BillingBlockid
                  FROM Dim_BillingBlock bb
                  WHERE bb.BillingBlockCode = vbap_faksp),1) Dim_BillingBlockid,
          ifnull((SELECT Dim_TransactionGroupid
                  FROM Dim_TransactionGroup tg
                  WHERE tg.TransactionGroup = vbak_trvog),1) Dim_TransactionGroupid,
          ifnull((SELECT Dim_SalesOrderRejectReasonid
                  FROM Dim_SalesOrderRejectReason sorr
                  WHERE sorr.RejectReasonCode = vbap_abgru),1) Dim_SalesOrderRejectReasonid,
          ifnull((SELECT dim_partid
                    FROM dim_part dp
                    WHERE dp.PartNumber = VBAP_MATNR AND dp.Plant = VBAP_WERKS),1) Dim_Partid,
          ifnull((SELECT Dim_SalesOrderHeaderStatusid
                    FROM Dim_SalesOrderHeaderStatus sohs
                    WHERE sohs.SalesDocumentNumber = VBAP_VBELN),1) Dim_SalesOrderHeaderStatusid,
          ifnull((SELECT Dim_SalesOrderItemStatusid
                    FROM Dim_SalesOrderItemStatus sois
                    WHERE sois.SalesDocumentNumber = VBAP_VBELN AND sois.SalesItemNumber = VBAP_POSNR),1) Dim_SalesOrderItemStatusid,
          ifnull((SELECT Dim_CustomerGroup1id
                    FROM Dim_CustomerGroup1 cg1
                    WHERE cg1.CustomerGroup = VBAK_KVGR1),1) Dim_CustomerGroup1id,
          ifnull((SELECT Dim_CustomerGroup2id
                    FROM Dim_CustomerGroup2 cg2
                    WHERE cg2.CustomerGroup = VBAK_KVGR2),1) Dim_CustomerGroup2id,
          soic.Dim_SalesOrderItemCategoryid Dim_SalesOrderItemCategoryid,
          1 Dim_ScheduleLineCategoryId,
          'Not Set' dd_ItemRelForDelv,
	      1 Dim_ProfitCenterId,
          ifnull((SELECT dc.Dim_DistributionChannelid
                    FROM dim_DistributionChannel dc
                    WHERE   dc.DistributionChannelCode = VBAK_VTWEG
                        AND dc.RowIsCurrent = 1), 1) Dim_DistributionChannelId,
          ifnull(VBAP_CHARG,'Not Set') dd_BatchNo,
          ifnull(VBAK_ERNAM,'Not Set') dd_CreatedBy,
          ifnull((SELECT nd.Dim_Dateid
                  FROM Dim_Date nd
                  WHERE nd.DateValue = VBAK_CMNGV AND nd.CompanyCode = pl.CompanyCode),1) Dim_DateidNextDate,
           ifnull((SELECT r.dim_routeid FROM dim_route r WHERE
                       r.RouteCode = VBAP_ROUTE AND r.RowIsCurrent = 1),1),
          ifnull((SELECT src.Dim_SalesRiskCategoryId FROM Dim_SalesRiskCategory src WHERE
                       src.SalesRiskCategory = VBAK_CTLPC AND src.CreditControlArea = VBAK_KKBER AND src.RowIsCurrent = 1),1),
           1 Dim_CustomerRiskCategoryId,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = VBAP_WAERK),1) Dim_Currencyid_TRA,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = var.pGlobalCurrency),1) dim_Currencyid_GBL,
          ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = vbak_stwae),1) dim_currencyid_STAT,
   		  ifnull(( SELECT z.exchangeRate FROM tmp_getExchangeRate1 z
			WHERE z.pFromCurrency  = VBAP_WAERK AND z.fact_script_name = 'bi_populate_afs_salesorder_fact'
			AND z.pDate = ifnull(PRSDT,vbak_audat) AND z.pFromExchangeRate = 0 AND z.pToCurrency = vbak_stwae),1)
		  amt_exchangerate_STAT,1

FROM max_holder_702,VBAK_VBAP
    INNER JOIN Dim_Plant pl on pl.PlantCode = VBAP_WERKS
    INNER JOIN Dim_Company co on co.CompanyCode = pl.CompanyCode
    INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1,
	variable_holder_fact_salesorderlife var	
WHERE NOT EXISTS (SELECT 1 FROM fact_salesorder_useinsub f
                   WHERE f.dd_SalesDocNo = VBAP_VBELN AND f.dd_SalesItemNo = VBAP_POSNR AND f.dd_ScheduleNo = 0)
      AND EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
      AND NOT EXISTS ( SELECT 1 FROM vbak_vbap_vbep_useinsub v WHERE vbak_vbap.VBAP_VBELN = v.VBAK_VBELN
                                                         AND vbak_vbap.VBAP_POSNR = v.VBAP_POSNR);

/*to check - ambiguous replace!!! */
/* UPDATE fact_salesorderlife_so_tmp2 fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET fo.amt_UnitPriceUoM = 
   Decimal(CASE WHEN VBAP_VRKME <> ifnull(VBAP_KMEIN,'Not Set') AND VBAP_NETPR > 0 AND VBAP_NETPR <> ROUND((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end),2)
			THEN ifnull(((VBAP_KPEIN * VBAP_NETWR/case VBAP_KWMENG when 0 then 1 else VBAP_KWMENG end)), 0) 
			ELSE ifnull(vbap_netpr, 0) 
		   END ,18,4) 
WHERE fo.dd_SalesDocNo = VBAP_VBELN
  AND fo.dd_SalesItemNo = VBAP_POSNR
  AND fo.dd_ScheduleNo = 0
  AND (vbap_netpr > 0 or vbap_netwr > 0) */

UPDATE fact_salesorderlife_so_tmp2 fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET fo.Dim_UnitOfMeasureId = uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND VBAP_KMEIN IS NOT NULL
   AND uom.UOM = vbap_kmein
   AND uom.RowIsCurrent = 1;

UPDATE fact_salesorderlife_so_tmp2 fo
  FROM VBAK_VBAP
   SET fo.Dim_UnitOfMeasureId = 1
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND VBAP_KMEIN IS NULL;

UPDATE fact_salesorderlife_so_tmp2 fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET fo.Dim_BaseUoMid = uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND vbap_meins IS NOT NULL
   AND uom.UOM = vbap_meins
   AND uom.RowIsCurrent = 1;

UPDATE fact_salesorderlife_so_tmp2 fo
  FROM VBAK_VBAP
   SET fo.Dim_BaseUoMid = 1
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND vbap_meins IS NULL
   AND ifnull(fo.Dim_BaseUoMid,-1) <> 1;

UPDATE fact_salesorderlife_so_tmp2 fo
  FROM VBAK_VBAP, Dim_UnitOfMeasure uom
   SET fo.Dim_SalesUoMid = uom.Dim_UnitOfMeasureId
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND vbap_vrkme IS NOT NULL
   AND uom.UOM = vbap_vrkme
   AND uom.RowIsCurrent = 1
   AND fo.Dim_SalesUoMid <> uom.Dim_UnitOfMeasureId;

UPDATE fact_salesorderlife_so_tmp2 fo
  FROM VBAK_VBAP
   SET fo.Dim_SalesUoMid = 1
 WHERE fo.dd_SalesDocNo = VBAP_VBELN
   AND fo.dd_SalesItemNo = VBAP_POSNR
   AND fo.dd_ScheduleNo = 0
   AND vbap_vrkme IS NULL
   AND ifnull(fo.Dim_SalesUoMid,-1) <> 1;

CALL VECTORWISE (COMBINE 'fact_salesorderlife_so_tmp2');

DROP TABLE IF EXISTS tmp_upd_702;

CREATE TABLE tmp_upd_702 
AS 
SELECT FIRST 0 src.dim_salesriskcategoryid updcol1, c.CUSTOMER updVBAK_KUNNR,src.CreditControlArea updVBAK_KKBER
FROM ukmbp_cms a
     INNER JOIN BUT000 b on a.PARTNER = b.PARTNER
     INNER JOIN cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID
     INNER JOIN dim_salesriskcategory src on src.SalesRiskCategory = a.RISK_CLASS AND src.RowIsCurrent = 1
ORDER BY c.customer  ;

/* Take all the SOs and Items without Schedules from fact_salesorderlife and vbak_vbap_vbep */
DROP TABLE IF EXISTS fact_salesorder_tmp_useinsub_del;
CREATE TABLE fact_salesorder_tmp_useinsub_del
AS
SELECT DISTINCT dd_SalesDocNo,dd_SalesItemNo
FROM fact_salesorder_useinsub WHERE dd_ScheduleNo = 0
UNION
SELECT distinct v.VBAK_VBELN,v.VBAP_POSNR
FROM vbak_vbap_vbep_useinsub v ;

DROP TABLE IF EXISTS fact_salesorder_tmp_useinsub_ins;
CREATE TABLE fact_salesorder_tmp_useinsub_ins
AS
SELECT * FROM fact_salesorder_tmp_useinsub_del WHERE 1=2;

/* Take all the SOs and Items without Schedules from vbak_vbap */
INSERT INTO fact_salesorder_tmp_useinsub_ins
SELECT DISTINCT VBAP_VBELN,VBAP_POSNR FROM VBAK_VBAP;

/* Take all the SOs and Items without Schedules from vbak_vbap which aren't yet in fact_salesorderlife or vbak_vbap_vbep*/
CALL VECTORWISE(combine 'fact_salesorder_tmp_useinsub_ins-fact_salesorder_tmp_useinsub_del');

DROP TABLE IF EXISTS VBAK_VBAP_tmp_notexists;
CREATE TABLE VBAK_VBAP_tmp_notexists
AS
SELECT DISTINCT v.*
FROM VBAK_VBAP v,fact_salesorder_tmp_useinsub_ins f
WHERE v.VBAP_VBELN = f.dd_SalesDocNo AND v.VBAP_POSNR = f.dd_SalesItemNo ;

UPDATE fact_salesorderlife_so_tmp2 fst
  FROM VBAK_VBAP_tmp_notexists
 INNER JOIN Dim_Plant pl on pl.PlantCode = VBAP_WERKS
 INNER JOIN Dim_Company co on co.CompanyCode = pl.CompanyCode
 INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
   SET Dim_CustomerRiskCategoryId= ifnull((SELECT  updcol1 FROM tmp_upd_702 WHERE updVBAK_KUNNR = VBAK_KUNNR AND updVBAK_KKBER=VBAK_KKBER),1)
 WHERE EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
   AND fst.dd_SalesDocNo = VBAP_VBELN AND fst.dd_SalesItemNo = VBAP_POSNR AND  fst.dd_ScheduleNo =0;

DROP TABLE IF EXISTS tmp_upd_702;

CREATE TABLE tmp_upd_702 AS
SELECT FIRST 0 Dim_CostCenterid,Code,ControllingArea,RowIsCurrent
  FROM Dim_CostCenter cc
 ORDER BY cc.ValidTo;

UPDATE fact_salesorderlife_so_tmp2 fst
FROM VBAK_VBAP_tmp_notexists
      inner join Dim_Plant pl on pl.PlantCode = VBAP_WERKS
      inner join Dim_Company co on co.CompanyCode = pl.CompanyCode
      INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
SET  Dim_CostCenterid=ifnull((SELECT  Dim_CostCenterid FROM tmp_upd_702 cc
                                WHERE cc.Code = vbak_kostl AND cc.ControllingArea = vbak_kokrs AND cc.RowIsCurrent = 1),1)
WHERE exists (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
And fst.dd_SalesDocNo = VBAP_VBELN AND fst.dd_SalesItemNo = VBAP_POSNR AND  fst.dd_ScheduleNo =0;

DROP TABLE IF EXISTS tmp_upd_702;

UPDATE fact_salesorderlife_so_tmp2 fst
  FROM VBAK_VBAP_tmp_notexists
       INNER JOIN Dim_Plant pl on pl.PlantCode = VBAP_WERKS
       INNER JOIN Dim_Company co on co.CompanyCode = pl.CompanyCode
       INNER JOIN dim_salesorderitemcategory soic ON soic.SalesOrderItemCategory = VBAP_PSTYV AND soic.RowIsCurrent = 1
   SET Dim_ProfitCenterId=ifnull((SELECT  pc.dim_profitcenterid
                      FROM dim_profitcenter pc
                      WHERE   pc.ProfitCenterCode = VBAP_PRCTR
                          AND pc.ControllingArea = VBAK_KOKRS
                          AND pc.ValidTo >= VBAK_ERDAT
                          AND pc.RowIsCurrent = 1 ),1)
 WHERE EXISTS (SELECT 1 FROM dim_date mdt WHERE mdt.DateValue = vbap_erdat AND mdt.Dim_Dateid > 1)
   AND fst.dd_SalesDocNo = VBAP_VBELN AND fst.dd_SalesItemNo = VBAP_POSNR AND  fst.dd_ScheduleNo =0;

DROP TABLE IF EXISTS fact_salesorder_useinsub;
DROP TABLE IF EXISTS vbak_vbap_vbep_useinsub;

CALL VECTORWISE(COMBINE 'fact_salesorderlife_so_tmp2');
CALL VECTORWISE(COMBINE 'fact_salesorderlife_so_tmp+fact_salesorderlife_so_tmp2');
CALL VECTORWISE(COMBINE 'fact_salesorderlife_so_tmp');
DROP TABLE IF EXISTS fact_salesorderlife_so_tmp2;

/*Start of temporary code split*/

UPDATE fact_salesorderlife_so_tmp fso
  FROM Dim_SalesMisc smisc, VBAK_VBAP vkp
   SET fso.Dim_SalesMiscId = ifnull(smisc.Dim_SalesMiscId,1)
 WHERE fso.dd_SalesDocNo = vkp.VBAP_VBELN
   AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
   AND fso.dd_ScheduleNo = 0
   AND smisc.DeliveryDateQuantityFixed = ifnull(VBAP_FIXMG,'Not Set')
   AND smisc.FixedQuantity = ifnull(VBAP_FMENG, 'Not Set')
   AND smisc.OverDeliveryAllowed = ifnull(VBAP_UEBTK, 'Not Set')
   AND smisc.CashDiscountIndicator = ifnull(VBAP_SKTOF, 'Not Set')
   AND smisc.ReturnsItem = ifnull(VBAP_SHKZG, 'Not Set')
   AND smisc.PricingOk = ifnull(VBAP_PRSOK, 'Not Set')
   AND smisc.CustomerNotPostedGoodsReceipt = ifnull(VBAP_NACHL, 'Not Set')
   AND smisc.ItemRelevantForDelivery = ifnull(VBAP_LFREL, 'Not Set')
   AND smisc.ScheduleConfirmStatus = 'Not Set'
   AND smisc.InvoiceReceiptIndicator = 'Not Set'
   AND ifnull(fso.Dim_SalesMiscId, -1) <> ifnull(smisc.Dim_SalesMiscId,-2);

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP vp, Dim_PartSales sprt
   SET Dim_PartSalesId = ifnull(sprt.Dim_PartSalesId,1)
 WHERE fso.dd_SalesDocNo = vp.VBAP_VBELN
   AND fso.dd_SalesItemNo = vp.VBAP_POSNR
   AND fso.dd_ScheduleNo = 0
   AND VBAK_VKORG IS NOT NULL
   AND sprt.SalesOrgCode = VBAK_VKORG
   AND sprt.PartNumber = VBAP_MATNR
   AND sprt.RowIsCurrent = 1
   AND ifnull(fso.Dim_PartSalesId,-1) <> ifnull(sprt.Dim_PartSalesId,-2);

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP vp
   SET Dim_PartSalesId = 1
 WHERE fso.dd_SalesDocNo = vp.VBAP_VBELN
   AND fso.dd_SalesItemNo = vp.VBAP_POSNR
   AND fso.dd_ScheduleNo = 0
   AND VBAK_VKORG IS NULL;
/*
UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP_VBEP vp, Dim_PartSales sprt
   SET Dim_PartSalesId = ifnull(sprt.Dim_PartSalesId,1)
 WHERE fso.dd_SalesDocNo = vp.VBAK_VBELN
   AND fso.dd_SalesItemNo = vp.VBAP_POSNR
   AND fso.dd_ScheduleNo = vp.VBEP_ETENR
   AND VBAK_VKORG IS NOT NULL
   AND sprt.SalesOrgCode = VBAK_VKORG
   AND sprt.PartNumber = VBAP_MATNR
   AND sprt.RowIsCurrent = 1
   AND ifnull(fso.Dim_PartSalesId,-1) <> ifnull(sprt.Dim_PartSalesId,-2)
*/

DROP TABLE IF EXISTS tmp_life_Dim_PartSalesId;

create table tmp_life_Dim_PartSalesId as
select distinct -1 as Dim_PartSalesId, vp.VBAK_VBELN,vp.VBEP_ETENR,vp.VBAP_POSNR, vp.VBAK_VKORG,vp.VBAP_MATNR
  from  VBAK_VBAP_VBEP vp, Dim_PartSales sprt
  where VBAK_VKORG IS NOT NULL
   AND sprt.SalesOrgCode = vp.VBAK_VKORG
   AND sprt.PartNumber = vp.VBAP_MATNR
   AND sprt.RowIsCurrent = 1 
   AND vp.VBAK_VKORG IS NOT NULL;
   
   update tmp_life_Dim_PartSalesId tmp
   set tmp.Dim_PartSalesId = ifnull((select sprt.Dim_PartSalesId from Dim_PartSales sprt 
										where sprt.SalesOrgCode = tmp.VBAK_VKORG
										and sprt.PartNumber = tmp.VBAP_MATNR),-1);
   UPDATE fact_salesorderlife_so_tmp fso
  FROM tmp_life_Dim_PartSalesId tmp
   SET Dim_PartSalesId = ifnull(tmp.Dim_PartSalesId,1)
 WHERE fso.dd_SalesDocNo = tmp.VBAK_VBELN
   AND fso.dd_SalesItemNo = tmp.VBAP_POSNR
   AND fso.dd_ScheduleNo = tmp.VBEP_ETENR
   AND ifnull(fso.Dim_PartSalesId,-1) <> ifnull(tmp.Dim_PartSalesId,-2);
   
   DROP TABLE IF EXISTS tmp_life_Dim_PartSalesId;

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP_VBEP vp
   SET Dim_PartSalesId = 1
 WHERE fso.dd_SalesDocNo = vp.VBAK_VBELN
   AND fso.dd_SalesItemNo = vp.VBAP_POSNR
   AND fso.dd_ScheduleNo = vp.VBEP_ETENR
   AND VBAK_VKORG IS NULL;

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP_VBEP vp
   SET dd_InternationalArticleNo = ifnull(VBEP_J_3AEAN11,'Not Set')
 WHERE fso.dd_SalesDocNo = vp.VBAK_VBELN
   AND fso.dd_SalesItemNo = vp.VBAP_POSNR
   AND fso.dd_ScheduleNo = vp.VBEP_ETENR
   AND vp.VBEP_J_3AEAN11 IS NOT NULL
   AND ifnull(dd_InternationalArticleNo,'Not Set') <> ifnull(VBEP_J_3AEAN11,'xxx');

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP_VBEP vp
   SET fso.dd_ReleaseRule = ifnull(vp.VBAK_J_3AREREG,'Not Set')
 WHERE fso.dd_SalesDocNo = vp.VBAK_VBELN
   AND fso.dd_SalesItemNo = vp.VBAP_POSNR
   AND fso.dd_ScheduleNo = vp.VBEP_ETENR
   AND VBAK_J_3AREREG IS NOT NULL
   AND ifnull(fso.dd_ReleaseRule,'yyy') <> ifnull(vp.VBAK_J_3AREREG,'xxx');

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP vp
   SET fso.dd_ReleaseRule = ifnull(vp.VBAK_J_3AREREG,'Not Set')
 WHERE fso.dd_SalesDocNo = vp.VBAP_VBELN
   AND fso.dd_SalesItemNo = vp.VBAP_POSNR
   AND fso.dd_ScheduleNo = 0
   AND vp.VBAK_J_3AREREG IS NOT NULL
   AND ifnull(fso.dd_ReleaseRule,'yyy') <> ifnull(vp.VBAK_J_3AREREG,'xxx');

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP_VBKD vp,
       dim_date dt,
       dim_company c
   SET fso.Dim_DateIdFixedValue = dt.Dim_DateId
 WHERE fso.dd_SalesDocNo = vp.VBKD_VBELN
   AND vp.VBKD_POSNR = 0
   AND fso.dim_companyid = c.dim_companyid
   AND vp.VBKD_VALDT IS NOT NULL
   AND dt.DateValue = VBKD_VALDT
   AND dt.CompanyCode = c.CompanyCode
   AND ifnull(fso.Dim_DateIdFixedValue,-1) <> ifnull(dt.Dim_DateId,-2);

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP_VBKD vp,
       dim_date dt,
       dim_company c
   SET fso.Dim_DateIdFixedValue = dt.Dim_DateId
 WHERE fso.dd_SalesDocNo = vp.VBKD_VBELN
   AND fso.dd_SalesItemNo = vp.VBKD_POSNR
   AND fso.dim_companyid = c.dim_companyid
   AND VBKD_VALDT IS NOT NULL
   AND dt.DateValue = VBKD_VALDT
   AND dt.CompanyCode = c.CompanyCode
   AND ifnull(fso.Dim_DateIdFixedValue,-1) <> dt.Dim_DateId;

/* Update  Dim_PaymentTermsId */

DROP TABLE IF EXISTS update_CustomerPaymentTerms_tmp; 
create table update_CustomerPaymentTerms_tmp
AS 
SELECT vp.VBKD_VBELN, vp.VBKD_POSNR, vp.VBKD_ZTERM, pt.Dim_CustomerPaymentTermsid
FROM VBAK_VBAP_VBKD vp,
Dim_CustomerPaymentTerms pt
where pt.PaymentTermCode = vp.VBKD_ZTERM
and VBKD_ZTERM IS NOT NULL
GROUP BY vp.VBKD_VBELN, vp.VBKD_POSNR, vp.VBKD_ZTERM, pt.Dim_CustomerPaymentTermsid;

UPDATE fact_salesorderlife_so_tmp fso
  FROM update_CustomerPaymentTerms_tmp tmp
   SET Dim_PaymentTermsId = tmp.Dim_CustomerPaymentTermsid
 WHERE fso.dd_SalesDocNo = tmp.VBKD_VBELN
   AND tmp.VBKD_POSNR = 0
   AND ifnull(fso.Dim_PaymentTermsId,-1) <> tmp.Dim_CustomerPaymentTermsid;

UPDATE fact_salesorderlife_so_tmp fso
  FROM update_CustomerPaymentTerms_tmp tmp
   SET Dim_PaymentTermsId = tmp.Dim_CustomerPaymentTermsid
 WHERE fso.dd_SalesDocNo = tmp.VBKD_VBELN
   AND fso.dd_SalesItemNo = tmp.VBKD_POSNR
   AND ifnull(fso.Dim_PaymentTermsId,-1) <> tmp.Dim_CustomerPaymentTermsid;
   
drop table  update_CustomerPaymentTerms_tmp;

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP_VBKD vp,
       dim_incoterm it
   SET fso.Dim_IncoTermId = it.dim_incotermid
 WHERE fso.dd_SalesDocNo = vp.VBKD_VBELN
   AND vp.VBKD_POSNR = 0
   AND VBKD_INCO1 IS NOT NULL
   AND it.IncoTermCode = VBKD_INCO1
   AND IFNULL(fso.Dim_IncoTermId,-1) <> it.dim_incotermid;

UPDATE fact_salesorderlife_so_tmp fso
  FROM VBAK_VBAP_VBKD vp,
       dim_incoterm it
   SET fso.Dim_IncoTermId = it.dim_incotermid
 WHERE fso.dd_SalesDocNo = vp.VBKD_VBELN
   AND fso.dd_SalesItemNo = vp.VBKD_POSNR
   AND VBKD_INCO1 IS NOT NULL
   AND it.IncoTermCode = VBKD_INCO1
   AND IFNULL(fso.Dim_IncoTermId,-1 ) <> it.dim_incotermid;

/* 8/25/2015 - Cristina I - changed the update script for dim_afsseasonid */
   
UPDATE fact_salesorderlife_so_tmp fso 
 FROM VBAK_VBAP_VBEP vbk, dim_plant pl, dim_afsseason sea
  SET fso.Dim_AfsSeasonId = ifnull(sea.dim_afsseasonid,1)
 WHERE fso.dd_SalesDocNo = vbk.VBAK_VBELN
   AND fso.dd_SalesItemNo = vbk.VBAP_POSNR
   AND fso.dd_ScheduleNo = vbk.VBEP_ETENR
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1   
   AND sea.SeasonIndicator = VBAP_J_3ASEAN
   AND sea.Theme = VBAP_AFS_THEME
   AND sea.Collection = VBAP_AFS_COLLECTION AND sea.RowIsCurrent = 1
   AND fso.Dim_AfsSeasonId <> ifnull(sea.dim_afsseasonid,1);   

  

UPDATE fact_salesorderlife_so_tmp fso 
   SET Dim_PartSalesId = 1
 WHERE Dim_PartSalesId  IS NULL;

UPDATE fact_salesorderlife_so_tmp fso 
  FROM Dim_SalesMisc smisc, VBAK_VBAP_VBEP vkp
   SET fso.Dim_SalesMiscId = smisc.Dim_SalesMiscId
 WHERE fso.dd_SalesDocNo = vkp.VBAK_VBELN
   AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
   AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
   AND smisc.DeliveryDateQuantityFixed = ifnull(VBAP_FIXMG,'Not Set')
   AND smisc.FixedQuantity = ifnull(VBAP_FMENG, 'Not Set')
   AND smisc.OverDeliveryAllowed = ifnull(VBAP_UEBTK, 'Not Set')
   AND smisc.CashDiscountIndicator = ifnull(VBAP_SKTOF, 'Not Set')
   AND smisc.ReturnsItem = ifnull(VBAP_SHKZG, 'Not Set')
   AND smisc.PricingOk = ifnull(VBAP_PRSOK, 'Not Set')
   AND smisc.CustomerNotPostedGoodsReceipt = ifnull(VBAP_NACHL, 'Not Set')
   AND smisc.ItemRelevantForDelivery = ifnull(VBAP_LFREL, 'Not Set')
   AND smisc.ScheduleConfirmStatus = ifnull(VBEP_WEPOS, 'Not Set')
   AND smisc.InvoiceReceiptIndicator = ifnull(VBEP_REPOS, 'Not Set')
   AND fso.Dim_SalesMiscId <> smisc.Dim_SalesMiscId;

UPDATE fact_salesorderlife_so_tmp fso 
  FROM vbak_vbap_vbep vkp, KNVV k
   SET fso.Dim_CustomerGroupId =
            ifnull(
              (SELECT cg.Dim_CustomerGroupId
                  FROM dim_CustomerGroup cg
                WHERE cg.CustomerGroup = k.knvv_kdgrp AND cg.RowIsCurrent = 1),
              1)
WHERE fso.dd_SalesDocNo = vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
  AND  k.knvv_VTWEG = vkp.VBAK_VTWEG
  AND k.knvv_SPART = vkp.VBAK_SPART
  AND k.knvv_vkorg = vkp.VBAK_VKORG
  AND k.knvv_KUNNR = vkp.VBAK_KUNNR 
  AND fso.dd_ScheduleNo <> 0 ;

UPDATE fact_salesorderlife_so_tmp fso 
  FROM vbak_vbap_vbep vkp, dim_SalesOffice so
   SET fso.Dim_SalesOfficeId = so.Dim_SalesOfficeId
 WHERE fso.dd_SalesDocNo = vkp.VBAK_VBELN
   AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
   AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
   AND vkp.vbak_vkbur IS NOT NULL
   AND fso.dd_ScheduleNo <> 0
   AND so.SalesOfficeCode = vbak_vkbur
   AND so.RowIsCurrent = 1
   AND ifnull(fso.Dim_SalesOfficeId,-1) <> so.Dim_SalesOfficeId;

UPDATE fact_salesorderlife_so_tmp fso 
  FROM vbak_vbap vkp, dim_SalesOffice so
   SET fso.Dim_SalesOfficeId = so.Dim_SalesOfficeId
 WHERE fso.dd_SalesDocNo = vkp.VBAP_VBELN
   AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
   AND vkp.vbak_vkbur IS NOT NULL
   AND fso.dd_ScheduleNo = 0
   AND so.SalesOfficeCode = vbak_vkbur
   AND so.RowIsCurrent = 1
   AND IFNULL(fso.Dim_SalesOfficeId,-1) <> so.Dim_SalesOfficeId;

UPDATE fact_salesorderlife_so_tmp fso 
  FROM vbak_vbap_vbep vkp, KNVV k
   SET  fso.Dim_SalesOfficeId =
            ifnull(
              (SELECT so.Dim_SalesOfficeId
                 FROM dim_SalesOffice so
                WHERE so.SalesOfficeCode = k.knvv_vkbur
                  AND so.RowIsCurrent = 1),
              1)
WHERE fso.dd_SalesDocNo = vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
  AND k.knvv_VTWEG = vkp.VBAK_VTWEG
  AND k.knvv_SPART = vkp.VBAK_SPART
  AND k.knvv_vkorg = vkp.VBAK_VKORG
  AND k.knvv_KUNNR = vkp.VBAK_KUNNR
  AND fso.dd_ScheduleNo <> 0 
  AND fso.Dim_SalesOfficeId = 1;

UPDATE fact_salesorderlife_so_tmp fso 
  FROM vbak_vbap_vbep vkp, KNVV k
   SET fso.Dim_CustomerPaymentTermsId = 
             ifnull((SELECT cpt.dim_Customerpaymenttermsid
                       FROM dim_customerpaymentterms cpt
                      WHERE cpt.PaymentTermCode = k.knvv_zterm
                        AND cpt.RowIsCurrent = 1), 1)
WHERE fso.dd_SalesDocNo = vkp.VBAK_VBELN
  AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo = vkp.VBEP_ETENR
  AND  k.knvv_VTWEG = vkp.VBAK_VTWEG
  AND k.knvv_SPART = vkp.VBAK_SPART
  AND k.knvv_vkorg = vkp.VBAK_VKORG
  AND k.knvv_KUNNR = vkp.VBAK_KUNNR
  AND fso.dd_ScheduleNo <> 0 
  AND IFNULL(fso.Dim_CustomerPaymentTermsId,-1) <> 
             ifnull((SELECT cpt.dim_Customerpaymenttermsid 
			           FROM dim_customerpaymentterms cpt 
					  WHERE cpt.PaymentTermCode = k.knvv_zterm 
					    AND cpt.RowIsCurrent = 1), 1);

UPDATE fact_salesorderlife_so_tmp fso 
  FROM vbak_vbap vkp,KNVV k
   SET fso.Dim_CustomerGroupId =
            ifnull(
             (SELECT cg.Dim_CustomerGroupId
                FROM dim_CustomerGroup cg
               WHERE cg.CustomerGroup = k.knvv_kdgrp AND cg.RowIsCurrent = 1),
             1)
WHERE fso.dd_SalesDocNo = vkp.VBAP_VBELN
  AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
  AND fso.dd_ScheduleNo = 0
  AND k.knvv_VTWEG = vkp.VBAK_VTWEG
  AND k.knvv_SPART = vkp.VBAK_SPART
  AND k.knvv_vkorg = vkp.VBAK_VKORG
  AND k.knvv_KUNNR = vkp.VBAK_KUNNR
  AND ifnull(fso.Dim_CustomerGroupId,-1) <> 
              ifnull(
             (SELECT cg.Dim_CustomerGroupId
                FROM dim_CustomerGroup cg
               WHERE cg.CustomerGroup = k.knvv_kdgrp AND cg.RowIsCurrent = 1),
             1);


UPDATE fact_salesorderlife_so_tmp fso
  FROM vbak_vbap vkp,KNVV k
   SET fso.Dim_SalesOfficeId =
          ifnull(
             (SELECT so.Dim_SalesOfficeId
                FROM dim_SalesOffice so
               WHERE so.SalesOfficeCode = k.knvv_vkbur
                 AND so.RowIsCurrent = 1),
             1)
 WHERE fso.dd_SalesDocNo = vkp.VBAP_VBELN
   AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
   AND fso.dd_ScheduleNo = 0
   AND k.knvv_VTWEG = vkp.VBAK_VTWEG
   AND k.knvv_SPART = vkp.VBAK_SPART
   AND k.knvv_vkorg = vkp.VBAK_VKORG
   AND k.knvv_KUNNR = vkp.VBAK_KUNNR
   AND fso.Dim_SalesOfficeId = 1
   AND IFNULL(fso.Dim_SalesOfficeId,-1) <> 
        ifnull(
             (SELECT so.Dim_SalesOfficeId
                FROM dim_SalesOffice so
               WHERE so.SalesOfficeCode = k.knvv_vkbur
                 AND so.RowIsCurrent = 1),
             1);


UPDATE fact_salesorderlife_so_tmp fso
  FROM vbak_vbap vkp,KNVV k
   SET fso.Dim_CustomerPaymentTermsId = 
			ifnull(
				(SELECT cpt.dim_Customerpaymenttermsid
					FROM dim_customerpaymentterms cpt
				WHERE cpt.PaymentTermCode = k.knvv_zterm
					AND cpt.RowIsCurrent = 1), 1)
WHERE  fso.dd_SalesDocNo = vkp.VBAP_VBELN
	AND fso.dd_SalesItemNo = vkp.VBAP_POSNR
    AND fso.dd_ScheduleNo = 0
    AND k.knvv_VTWEG = vkp.VBAK_VTWEG
    AND k.knvv_SPART = vkp.VBAK_SPART
    AND k.knvv_vkorg = vkp.VBAK_VKORG
    AND k.knvv_KUNNR = vkp.VBAK_KUNNR;


UPDATE fact_salesorderlife_so_tmp fso
   SET fso.Dim_SalesOfficeId = 1
 WHERE fso.Dim_SalesOfficeId IS NULL;

DROP TABLE IF EXISTS Dim_CustomerPartnerFunctions_upd;
CREATE TABLE Dim_CustomerPartnerFunctions_upd AS 
	SELECT first 0 * 
		FROM Dim_CustomerPartnerFunctions 
			order by PartnerCounter desc;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbuk v,dim_overallstatusforcreditcheck oscc
		SET sof.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckID
WHERE sof.dd_SalesDocNo = v.VBUK_VBELN
	AND  oscc.overallstatusforcreditcheck = v.VBUK_CMGST
	AND oscc.RowIsCurrent = 1
	AND v.VBUK_CMGST IS NOT NULL
	AND sof.Dim_OverallStatusCreditCheckId <> oscc.dim_overallstatusforcreditcheckID;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbuk v
		SET sof.Dim_OverallStatusCreditCheckId = 1
WHERE sof.dd_SalesDocNo = v.VBUK_VBELN
	AND v.VBUK_CMGST IS NULL;

UPDATE fact_salesorderlife_so_tmp sof
  FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_fact_salesorderlife vb
   SET sof.Dim_BillToPartyPartnerFunctionId =
            ifnull(
              (SELECT  Dim_CustomerPartnerFunctionsID
                  FROM Dim_CustomerPartnerFunctions_upd cpf
               WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                      AND cpf.SalesOrgCode = shi.vbak_vkorg
                      AND cpf.DivisionCode = shi.vbap_spart
                      AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                      AND cpf.PartnerFunction = sdp.vbpa_parvw
                      AND cpf.RowIsCurrent = 1
                ),
              1)
WHERE sdp.vbpa_parvw = vb.pBillToPartyPartnerFunction
		AND sof.dd_SalesDocNo = shi.vbak_vbeln
        AND sof.dd_SalesItemNo = shi.vbap_posnr
        AND sof.dd_scheduleno = shi.vbep_etenr
		AND sdp.vbpa_vbeln = shi.vbak_vbeln
		AND sof.dd_scheduleno <> 0;


UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_fact_salesorderlife vb
		SET sof.Dim_BillToPartyPartnerFunctionId =
            ifnull(
              (SELECT  Dim_CustomerPartnerFunctionsID
                  FROM Dim_CustomerPartnerFunctions_upd cpf
               WHERE  cpf.CustomerNumber1 = shi.vbak_kunnr
                      AND cpf.SalesOrgCode = shi.vbak_vkorg
                      AND cpf.DivisionCode = shi.vbap_spart
                      AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                      AND cpf.PartnerFunction = sdp.vbpa_parvw
                      AND cpf.RowIsCurrent = 1
                ),
              1)
WHERE sdp.vbpa_parvw = vb.pBillToPartyPartnerFunction
  AND sof.dd_SalesDocNo = shi.vbak_vbeln
  AND sof.dd_SalesItemNo = shi.vbap_posnr
  AND sof.dd_scheduleno = shi.vbep_etenr
  AND sdp.vbpa_vbeln = shi.vbak_vbeln 
  AND sdp.vbpa_posnr = shi.vbap_posnr
  AND sof.dd_scheduleno <> 0;


UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_fact_salesorderlife vb
		SET sof.Dim_PayerPartnerFunctionId =
            ifnull(
              (SELECT  Dim_CustomerPartnerFunctionsID
					FROM Dim_CustomerPartnerFunctions_upd cpf
               WHERE  cpf.CustomerNumber1 = shi.vbak_kunnr
                      AND cpf.SalesOrgCode = shi.vbak_vkorg
                      AND cpf.DivisionCode = shi.vbap_spart
                      AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                      AND cpf.PartnerFunction = sdp.vbpa_parvw
                      AND cpf.RowIsCurrent = 1
                ),
              1)
WHERE sdp.vbpa_parvw = vb.pPayerPartnerFunction
	AND sof.dd_SalesDocNo = shi.vbak_vbeln
    AND sof.dd_SalesItemNo = shi.vbap_posnr
    AND sof.dd_scheduleno = shi.vbep_etenr
	AND sdp.vbpa_vbeln = shi.vbak_vbeln
	AND sof.dd_scheduleno <> 0 ;


UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_fact_salesorderlife vb
		SET sof.Dim_PayerPartnerFunctionId =
            ifnull(
              (SELECT  Dim_CustomerPartnerFunctionsID
                  FROM Dim_CustomerPartnerFunctions_upd cpf
               WHERE  cpf.CustomerNumber1 = shi.vbak_kunnr
                      AND cpf.SalesOrgCode = shi.vbak_vkorg
                      AND cpf.DivisionCode = shi.vbap_spart
                      AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                      AND cpf.PartnerFunction = sdp.vbpa_parvw
                      AND cpf.RowIsCurrent = 1
                ),
              1)
WHERE sdp.vbpa_parvw = vb.pPayerPartnerFunction
	AND  sof.dd_SalesDocNo = shi.vbak_vbeln
        AND sof.dd_SalesItemNo = shi.vbap_posnr
        AND sof.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln 
	AND sdp.vbpa_posnr = shi.vbap_posnr
	AND sof.dd_scheduleno <> 0 ;
  

DROP TABLE IF EXISTS Dim_CustomerPartnerFunctions_upd;
CREATE TABLE Dim_CustomerPartnerFunctions_upd AS 
SELECT first 0 * 
FROM Dim_CustomerPartnerFunctions 
order by PartnerCounter desc;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep shi,
		vbpa sdp,
		variable_holder_fact_salesorderlife vb,
		Dim_CustomerPartnerFunctions_upd cpf
		SET sof.Dim_CustomPartnerFunctionId = ifnull(cpf.Dim_CustomerPartnerFunctionsID,1)
WHERE cpf.CustomerNumber1 = shi.vbak_kunnr
	AND cpf.SalesOrgCode = shi.vbak_vkorg
	AND cpf.DivisionCode = shi.vbap_spart
	AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
	AND cpf.PartnerFunction = sdp.vbpa_parvw
	AND cpf.RowIsCurrent = 1
	AND sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey
	AND sof.dd_SalesDocNo = shi.vbak_vbeln
	AND sof.dd_SalesItemNo = shi.vbap_posnr
	AND sof.dd_scheduleno = shi.vbep_etenr
	AND sdp.vbpa_vbeln = shi.vbak_vbeln
	AND vb.pCustomPartnerFunctionKey <> 'Not Set'
	AND sof.dd_scheduleno <> 0
	AND sof.Dim_CustomPartnerFunctionId <> ifnull(cpf.Dim_CustomerPartnerFunctionsID,-1);


UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_fact_salesorderlife vb
          SET sof.Dim_CustomPartnerFunctionId =
                  ifnull(
                    (SELECT  Dim_CustomerPartnerFunctionsID
                        FROM Dim_CustomerPartnerFunctions_upd cpf
                     WHERE  cpf.CustomerNumber1 = shi.vbak_kunnr
                            AND cpf.SalesOrgCode = shi.vbak_vkorg
                            AND cpf.DivisionCode = shi.vbap_spart
                            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                            AND cpf.PartnerFunction = sdp.vbpa_parvw
                            AND cpf.RowIsCurrent = 1
                      ),
                    1)
WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey
	AND  sof.dd_SalesDocNo = shi.vbak_vbeln
        AND sof.dd_SalesItemNo = shi.vbap_posnr
        AND sof.dd_scheduleno = shi.vbep_etenr
	AND sdp.vbpa_vbeln = shi.vbak_vbeln 
	AND sdp.vbpa_posnr = shi.vbap_posnr
	AND vb.pCustomPartnerFunctionKey <> 'Not Set'
	AND sof.dd_scheduleno <> 0;


UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_fact_salesorderlife vb
        SET sof.Dim_CustomPartnerFunctionId1 =
              ifnull(
                (SELECT  Dim_CustomerPartnerFunctionsID
                    FROM Dim_CustomerPartnerFunctions_upd cpf
                 WHERE  cpf.CustomerNumber1 = shi.vbak_kunnr
                        AND cpf.SalesOrgCode = shi.vbak_vkorg
                        AND cpf.DivisionCode = shi.vbap_spart
                        AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                        AND cpf.PartnerFunction = sdp.vbpa_parvw
                        AND cpf.RowIsCurrent = 1
                  ),
                1)
WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey1
	AND  sof.dd_SalesDocNo = shi.vbak_vbeln
        AND sof.dd_SalesItemNo = shi.vbap_posnr
        AND sof.dd_scheduleno = shi.vbep_etenr
	AND sdp.vbpa_vbeln = shi.vbak_vbeln
	AND vb.pCustomPartnerFunctionKey1 <> 'Not Set'
	AND sof.dd_scheduleno <> 0;


UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep shi,vbpa sdp,variable_holder_fact_salesorderlife vb
          SET sof.Dim_CustomPartnerFunctionId1 =
                  ifnull(
                    (SELECT  Dim_CustomerPartnerFunctionsID
                        FROM Dim_CustomerPartnerFunctions_upd cpf
                     WHERE     cpf.CustomerNumber1 = shi.vbak_kunnr
                            AND cpf.SalesOrgCode = shi.vbak_vkorg
                            AND cpf.DivisionCode = shi.vbap_spart
                            AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                            AND cpf.PartnerFunction = sdp.vbpa_parvw
                            AND cpf.RowIsCurrent = 1
                      ),
                      1)
WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey1
	AND sof.dd_SalesDocNo = shi.vbak_vbeln
        AND sof.dd_SalesItemNo = shi.vbap_posnr
        AND sof.dd_scheduleno = shi.vbep_etenr
	AND sdp.vbpa_vbeln = shi.vbak_vbeln 
	AND sdp.vbpa_posnr = shi.vbap_posnr
	AND vb.pCustomPartnerFunctionKey1 <> 'Not Set'
	AND sof.dd_scheduleno <> 0;


UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep shi,
		vbpa sdp,
		variable_holder_fact_salesorderlife vb,
		Dim_CustomerPartnerFunctions_upd cpf
		SET sof.Dim_CustomPartnerFunctionId2 = ifnull(cpf.Dim_CustomerPartnerFunctionsID,1)
WHERE cpf.CustomerNumber1 = shi.vbak_kunnr
	AND cpf.SalesOrgCode = shi.vbak_vkorg
	AND cpf.DivisionCode = shi.vbap_spart
	AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
	AND cpf.PartnerFunction = sdp.vbpa_parvw
	AND cpf.RowIsCurrent = 1
	AND sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey2
	AND sof.dd_SalesDocNo = shi.vbak_vbeln
	AND sof.dd_SalesItemNo = shi.vbap_posnr
	AND sof.dd_scheduleno = shi.vbep_etenr
	AND sdp.vbpa_vbeln = shi.vbak_vbeln
	AND vb.pCustomPartnerFunctionKey2 <> 'Not Set'
	AND sof.dd_scheduleno <> 0
	AND sof.Dim_CustomPartnerFunctionId2 <> ifnull(cpf.Dim_CustomerPartnerFunctionsID,-1);

UPDATE fact_salesorderlife_so_tmp sof
	From vbak_vbap_vbep shi,vbpa sdp,variable_holder_fact_salesorderlife vb
        SET sof.Dim_CustomPartnerFunctionId2 =
                ifnull(
                  (SELECT  Dim_CustomerPartnerFunctionsID
                      FROM Dim_CustomerPartnerFunctions_upd cpf
                   WHERE  cpf.CustomerNumber1 = shi.vbak_kunnr
                          AND cpf.SalesOrgCode = shi.vbak_vkorg
                          AND cpf.DivisionCode = shi.vbap_spart
                          AND cpf.DistributionChannelCode = shi.VBAK_VTWEG
                          AND cpf.PartnerFunction = sdp.vbpa_parvw
                          AND cpf.RowIsCurrent = 1
                    ),
                  1)
WHERE sdp.vbpa_parvw = vb.pCustomPartnerFunctionKey2
	AND  sof.dd_SalesDocNo = shi.vbak_vbeln
	AND sof.dd_SalesItemNo = shi.vbap_posnr
	AND sof.dd_scheduleno = shi.vbep_etenr
	AND  sdp.vbpa_vbeln = shi.vbak_vbeln 
	AND sdp.vbpa_posnr = shi.vbap_posnr
	AND  vb.pCustomPartnerFunctionKey2 <> 'Not Set'
	AND sof.dd_scheduleno <> 0;


/* LK : 23-May-2013 : Fix the columbia prod issue mentioned by Shanthi */
/* This was because sub-queries in update were converted into joins, resulting in default values not being applied when one of the column has nulls or join columsn values do not match */

/* So, update the default values here first */


DROP TABLE IF EXISTS TMP_fact_salesorderlife_upd_default_del;
CREATE TABLE TMP_fact_salesorderlife_upd_default_del
AS
SELECT sof.*
  FROM fact_salesorderlife sof,vbak_vbap_vbep vbk,dim_plant pl
 WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
   AND sof.dd_ScheduleNo <> 0;


DROP TABLE IF EXISTS TMP_fact_salesorderlife_upd_default_ins;
CREATE TABLE TMP_fact_salesorderlife_upd_default_ins
AS
SELECT * 
  FROM TMP_fact_salesorderlife_upd_default_del;


UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.dd_CustomerPONo = 'Not Set'
WHERE ifnull(sof.dd_CustomerPONo,'xx') <> 'Not Set';

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_CreditRepresentativeId = 1
WHERE ifnull(sof.Dim_CreditRepresentativeId,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_MaterialPriceGroup1Id = 1
WHERE ifnull(sof.Dim_MaterialPriceGroup1Id,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_MaterialPriceGroup2Id = 1
WHERE ifnull(sof.Dim_MaterialPriceGroup2Id,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_DeliveryBlockId = 1
WHERE ifnull(sof.Dim_DeliveryBlockId,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_SalesDocOrderReasonId = 1
WHERE ifnull(sof.Dim_SalesDocOrderReasonId,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_MaterialGroupId = 1
WHERE ifnull(sof.Dim_MaterialGroupId,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.amt_SubTotal3 = 0
WHERE ifnull(sof.amt_SubTotal3,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.amt_Subtotal3_OrderQty = 0
WHERE ifnull(sof.amt_Subtotal3_OrderQty,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.amt_SubTotal4 = 0
WHERE ifnull(sof.amt_SubTotal4,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.amt_SubTotal1 = 0
WHERE ifnull(sof.amt_SubTotal1,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.amt_SubTotal2 = 0
WHERE ifnull(sof.amt_SubTotal2,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.amt_SubTotal5 = 0
WHERE ifnull(sof.amt_SubTotal5,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.amt_SubTotal6 = 0
WHERE ifnull(sof.amt_SubTotal6,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.dd_DocumentConditionNo = 'Not Set'
WHERE sof.dd_DocumentConditionNo is null;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.ct_CumConfirmedQty = 0
WHERE ifnull(sof.ct_CumConfirmedQty,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.ct_CumOrderQty = 0
WHERE ifnull(sof.ct_CumOrderQty,-1) <> 0;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_PurchaseOrderTypeId = 1
WHERE ifnull(sof.Dim_PurchaseOrderTypeId,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_DateIdPurchaseOrder = 1
WHERE ifnull(sof.Dim_DateIdPurchaseOrder,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_DateIdQuotationValidFrom = 1
WHERE ifnull(sof.Dim_DateIdQuotationValidFrom,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_DateIdQuotationValidTo = 1
WHERE ifnull(sof.Dim_DateIdQuotationValidTo,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_DateIdSOCreated = 1
WHERE ifnull(sof.Dim_DateIdSOCreated,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.Dim_DateIdSODocument = 1
WHERE ifnull(sof.Dim_DateIdSODocument,-1) <> 1;

UPDATE TMP_fact_salesorderlife_upd_default_ins sof
SET sof.dd_ReferenceDocumentNo = 'Not Set'
WHERE ifnull(sof.dd_ReferenceDocumentNo,'xx') <> 'Not Set';

call vectorwise(combine 'fact_salesorderlife - TMP_fact_salesorderlife_upd_default_del + TMP_fact_salesorderlife_upd_default_ins');

DROP TABLE IF EXISTS TMP_fact_salesorderlife_upd_default_del;
DROP TABLE IF EXISTS TMP_fact_salesorderlife_upd_default_ins;


UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl
		SET sof.dd_CustomerPONo = ifnull(vbk.VBAK_BSTNK,'Not Set')
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
		AND sof.dd_scheduleno <> 0 
		AND sof.dd_CustomerPONo <> ifnull(vbk.VBAK_BSTNK,'Not Set');

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk, dim_creditrepresentativegroup cg
		SET  sof.Dim_CreditRepresentativeId =
				 ifnull(Dim_CreditRepresentativegroupId, 1)
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND vbk.VBAK_SBGRP IS NOT NULL 
		AND vbk.VBAK_KKBER IS NOT NULL
		AND cg.CreditRepresentativeGroup = vbk.VBAK_SBGRP
		AND cg.CreditControlArea = vbk.VBAK_KKBER
		AND cg.RowIsCurrent = 1 
		AND sof.Dim_CreditRepresentativeId <> ifnull( Dim_CreditRepresentativegroupId, 1);

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk
		SET  sof.Dim_CreditRepresentativeId = 1
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND ( vbk.VBAK_SBGRP IS NULL OR vbk.VBAK_KKBER IS NULL );

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,Dim_MaterialPriceGroup1 mpg
		SET sof.Dim_MaterialPriceGroup1Id = mpg.Dim_MaterialPriceGroup1Id
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND vbk.VBAP_MVGR1 IS NOT NULL
		AND  mpg.MaterialPriceGroup1 = vbk.VBAP_MVGR1
		AND mpg.RowIsCurrent = 1
		AND sof.Dim_MaterialPriceGroup1Id <> ifnull( mpg.Dim_MaterialPriceGroup1Id,1);

UPDATE fact_salesorderlife_so_tmp sof
FROM vbak_vbap_vbep vbk
SET sof.Dim_MaterialPriceGroup1Id = 1
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
AND vbk.VBAP_MVGR1 IS NULL;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,Dim_MaterialPriceGroup2 mpg
		SET sof.Dim_MaterialPriceGroup2Id = mpg.Dim_MaterialPriceGroup2Id
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND vbk.VBAP_MVGR1 IS NOT NULL
		AND  mpg.MaterialPriceGroup2 = vbk.VBAP_MVGR2
		AND mpg.RowIsCurrent = 1
		AND sof.Dim_MaterialPriceGroup2Id <> ifnull( mpg.Dim_MaterialPriceGroup2Id,1);

UPDATE fact_salesorderlife_so_tmp sof
FROM vbak_vbap_vbep vbk
SET sof.Dim_MaterialPriceGroup2Id = 1
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
AND vbk.VBAP_MVGR2 IS NULL;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk, Dim_DeliveryBlock db
		SET sof.Dim_DeliveryBlockId =  db.Dim_DeliveryBlockId
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND vbk.VBAK_LIFSK IS NOT NULL
		AND db.DeliveryBlock = vbk.VBAK_LIFSK
		AND db.RowIsCurrent = 1 
		and ifnull(sof.Dim_DeliveryBlockId,-1) <> ifnull(db.Dim_DeliveryBlockId,-2);

UPDATE fact_salesorderlife_so_tmp sof
FROM vbak_vbap_vbep vbk
SET sof.Dim_DeliveryBlockId =  1
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
AND vbk.VBAK_LIFSK IS NULL;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl,dim_salesdocorderreason sor
		SET sof.Dim_SalesDocOrderReasonId =
				ifnull( sor.dim_salesdocorderreasonid, 1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1
			AND sor.ReasonCode = vbk.VBAK_AUGRU 
			AND sor.RowIsCurrent = 1  
			AND sof.dd_scheduleno <> 0 
			AND sof.Dim_SalesDocOrderReasonId <> ifnull( sor.dim_salesdocorderreasonid, 1);


UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl,dim_materialgroup mg
		SET sof.Dim_MaterialGroupId =
				ifnull( mg.dim_materialgroupid,1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1
			AND  mg.MaterialGroupCode = vbk.VBAP_MATKL
            AND mg.RowIsCurrent = 1  
			AND sof.dd_scheduleno <> 0 
		    AND sof.Dim_MaterialGroupId <> ifnull( mg.dim_materialgroupid,1)
			AND sof.Dim_MaterialGroupId <>  ifnull( mg.dim_materialgroupid,1);

UPDATE fact_salesorderlife_so_tmp sof
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET sof.amt_SubTotal3 =
          ifnull(
               (  vbk.VBAP_KZWI3
                / CASE
                     WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                          AND VBAP_NETPR > 0
                          AND VBAP_NETPR <>
                                 ROUND(
                                    (  VBAP_KPEIN
                                     * VBAP_NETWR
                                     / CASE VBAP_KBMENG
                                          WHEN 0 THEN 1
                                          ELSE VBAP_KBMENG
                                       END),
                                    4)
                     THEN
                        (CASE ifnull(
                                 Round(
                                    (  VBAP_NETWR
                                     / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) end)
                                        / (CASE
                                              WHEN vbap_kpein = 0 THEN 1
                                              ELSE vbap_kpein
                                           END))),
                                    4),
                                 1)
                            WHEN 0
                            THEN
                               1
                            ELSE
                               ifnull(
                                  Round(
                                     (  VBAP_NETWR
                                      / (  (CASE WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) = 0
                                               THEN
                                                  1
                                               ELSE
                                                  (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END)
                                            END)
                                         / (CASE
                                               WHEN vbap_kpein = 0 THEN 1
                                               ELSE vbap_kpein
                                            END))),
                                     4),
                                  1)
                         END)
                     ELSE
                        Case when ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1) = 0 
			then 1
			Else ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1)
			ENd
                  END)
             * vbep_bmeng ,
             0)
 WHERE     vbk.VBAK_VBELN = sof.dd_SalesDocNo
       AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
       AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
       AND pl.plantcode = vbk.VBAP_WERKS
       AND pl.rowiscurrent = 1
       AND sof.dd_scheduleno <> 0
       AND sof.amt_SubTotal3 <>
              ifnull(
                   (  vbk.VBAP_KZWI3
                    / CASE
                         WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                              AND VBAP_NETPR > 0
                              AND VBAP_NETPR <>
                                     ROUND(
                                        (  VBAP_KPEIN
                                         * VBAP_NETWR
                                         / CASE VBAP_KBMENG
                                              WHEN 0 THEN 1
                                              ELSE VBAP_KBMENG
                                           END),
                                        4)
                         THEN
                            (CASE ifnull(
                                     Round( (  VBAP_NETWR
                                         / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) end)
                                            / (CASE
                                                  WHEN vbap_kpein = 0 THEN 1
                                                  ELSE vbap_kpein
                                               END))),
                                        4),
                                     1)
                                WHEN 0
                                THEN
                                   1
                                ELSE
                                   ifnull(
                                      Round(
                                         (  VBAP_NETWR
                                          / (  (CASE
                                                   WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) = 0
                                                   THEN
                                                      1
                                                   ELSE (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END)
                                                END)
                                             / (CASE
                                                   WHEN vbap_kpein = 0 THEN 1
                                                   ELSE vbap_kpein
                                                END))),
                                         4),
                                      1)
                             END)
                         ELSE
                            case when ifnull(
                                 VBAP_NETWR
                               / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                     THEN
                                        1
                                     ELSE (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                                  END),
                               1) = 0 
				then 1
				else  ifnull(
                                 VBAP_NETWR
                               / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                     THEN
                                        1
                                     ELSE (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                                  END),
                               1)
			   end
                      END)
                 * vbep_bmeng , 0);				 

UPDATE fact_salesorderlife_so_tmp sof
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET sof. amt_Subtotal3_OrderQty =
          ifnull(
               (  vbk.VBAP_KZWI3
                / CASE
                     WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                          AND VBAP_NETPR > 0
                          AND VBAP_NETPR <>
                                 ROUND(
                                    (  VBAP_KPEIN
                                     * VBAP_NETWR
                                     / CASE VBAP_KWMENG
                                          WHEN 0 THEN 1
                                          ELSE VBAP_KWMENG
                                       END),
                                    4)
                     THEN
                        (CASE ifnull(
                                 Round(
                                    (  VBAP_NETWR
                                     / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) end)
                                        / (CASE
                                              WHEN vbap_kpein = 0 THEN 1
                                              ELSE vbap_kpein
                                           END))),
                                    4),
                                 1)
                            WHEN 0
                            THEN
                               1
                            ELSE
                               ifnull(
                                  Round(
                                     (  VBAP_NETWR
                                      / (  (CASE WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END) = 0
                                               THEN
                                                  1
                                               ELSE
                                                  (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KWMENG WHEN 0 THEN 1 ELSE VBAP_KWMENG END)
                                            END)
                                         / (CASE
                                               WHEN vbap_kpein = 0 THEN 1
                                               ELSE vbap_kpein
                                            END))),
                                     4),
                                  1)
                         END)
                     ELSE
                        Case when ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1) = 0 
			then 1
			Else ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1)
			ENd
                  END)
             * vbep_wmeng ,
             0)
 WHERE     vbk.VBAK_VBELN = sof.dd_SalesDocNo
       AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
       AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
       AND pl.plantcode = vbk.VBAP_WERKS
       AND pl.rowiscurrent = 1
       AND sof.dd_scheduleno <> 0 ; 		
			 
UPDATE fact_salesorderlife_so_tmp sof
FROM vbak_vbap_vbep vbk, dim_plant pl
   SET sof.amt_SubTotal4 =
          ifnull(
               (  vbk.VBAP_KZWI4
                / CASE
                     WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                          AND VBAP_NETPR > 0
                          AND VBAP_NETPR <>
                                 ROUND(
                                    (  VBAP_KPEIN
                                     * VBAP_NETWR
                                     / CASE VBAP_KBMENG
                                          WHEN 0 THEN 1
                                          ELSE VBAP_KBMENG
                                       END),
                                    4)
                     THEN
                        (CASE ifnull(
                                 Round(
                                    (  VBAP_NETWR
                                     / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) end)
                                        / (CASE
                                              WHEN vbap_kpein = 0 THEN 1
                                              ELSE vbap_kpein
                                           END))),
                                    4),
                                 1)
                            WHEN 0
                            THEN
                               1
                            ELSE
                               ifnull(
                                  Round(
                                     (  VBAP_NETWR
                                      / (  (CASE WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) = 0
                                               THEN
                                                  1
                                               ELSE
                                                  (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END)
                                            END)
                                         / (CASE
                                               WHEN vbap_kpein = 0 THEN 1
                                               ELSE vbap_kpein
                                            END))),
                                     4),
                                  1)
                         END)
                     ELSE
                        Case when ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1) = 0 
			then 1
			Else ifnull(
                             VBAP_NETWR
                           / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                 THEN
                                    1
                                 ELSE
                                    (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                              END),
                           1)
			ENd
                  END)
             * vbep_bmeng ,
             0)
 WHERE     vbk.VBAK_VBELN = sof.dd_SalesDocNo
       AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
       AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
       AND pl.plantcode = vbk.VBAP_WERKS
       AND pl.rowiscurrent = 1
       AND sof.dd_scheduleno <> 0
       AND sof.amt_SubTotal4 <>
              ifnull(
                   (  vbk.VBAP_KZWI4
                    / CASE
                         WHEN     VBAP_VRKME <> ifnull(VBAP_KMEIN, 'Not Set')
                              AND VBAP_NETPR > 0
                              AND VBAP_NETPR <>
                                     ROUND(
                                        (  VBAP_KPEIN
                                         * VBAP_NETWR
                                         / CASE VBAP_KBMENG
                                              WHEN 0 THEN 1
                                              ELSE VBAP_KBMENG
                                           END),
                                        4)
                         THEN
                            (CASE ifnull(
                                     Round( (  VBAP_NETWR
                                         / (  (Case when (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) = 0 then 1 else (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) end)
                                            / (CASE
                                                  WHEN vbap_kpein = 0 THEN 1
                                                  ELSE vbap_kpein
                                               END))),
                                        4),
                                     1)
                                WHEN 0
                                THEN
                                   1
                                ELSE
                                   ifnull(
                                      Round(
                                         (  VBAP_NETWR
                                          / (  (CASE
                                                   WHEN (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END) = 0
                                                   THEN
                                                      1
                                                   ELSE (  VBAP_KPEIN * VBAP_NETWR / CASE VBAP_KBMENG WHEN 0 THEN 1 ELSE VBAP_KBMENG END)
                                                END)
                                             / (CASE
                                                   WHEN vbap_kpein = 0 THEN 1
                                                   ELSE vbap_kpein
                                                END))),
                                         4),
                                      1)
                             END)
                         ELSE
                            case when ifnull(
                                 VBAP_NETWR
                               / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                     THEN
                                        1
                                     ELSE (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                                  END),
                               1) = 0 
				then 1
				else  ifnull(
                                 VBAP_NETWR
                               / (CASE WHEN (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END)) = 0
                                     THEN
                                        1
                                     ELSE (  vbap_netpr / (CASE WHEN vbap_kpein = 0 THEN 1 ELSE vbap_kpein END))
                                  END),
                               1)
			   end
                      END)
                 * vbep_bmeng ,
                 0);

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl
		SET sof.ct_CumConfirmedQty = ifnull(vbk.VBAP_KBMENG, 0)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1  
			AND sof.dd_scheduleno <> 0 
			AND  sof.ct_CumConfirmedQty <> ifnull(vbk.VBAP_KBMENG, 0);


UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl
		SET  sof.ct_CumOrderQty = ifnull(vbk.VBAP_KWMENG, 0)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND sof.ct_CumOrderQty <> ifnull(vbk.VBAP_KWMENG, 0)
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1  
			AND sof.dd_scheduleno <> 0 
			AND sof.ct_CumOrderQty <>  ifnull(vbk.VBAP_KWMENG, 0);

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl,dim_customerpurchaseordertype cpt
		SET sof.Dim_PurchaseOrderTypeId =
				ifnull( cpt.dim_customerpurchaseordertypeid, 1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1
			AND cpt.CustomerPOType = vbk.VBAK_BSARK
            AND cpt.RowIsCurrent = 1  
			AND sof.dd_scheduleno <> 0 
			AND sof.Dim_PurchaseOrderTypeId <>  ifnull( cpt.dim_customerpurchaseordertypeid, 1);


UPDATE  fact_salesorderlife_so_tmp sof
FROM vbak_vbap_vbep vbk,dim_plant pl, dim_Date dt
SET sof.Dim_DateIdPurchaseOrder =
          ifnull( dt.Dim_Dateid,1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
            AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1
	    AND  dt.DateValue = vbk.VBAK_BSTDK
            AND dt.CompanyCode = pl.CompanyCode  
	    AND sof.dd_scheduleno <> 0 
	    AND  sof.Dim_DateIdPurchaseOrder  <>  ifnull( dt.Dim_Dateid,1);

			
UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
		SET sof.Dim_DateIdQuotationValidFrom =
				ifnull( dt.Dim_Dateid,1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1
			AND  dt.DateValue = vbk.VBAK_ANGDT
            AND dt.CompanyCode = pl.CompanyCode   
			AND sof.dd_scheduleno <> 0 
			AND  sof.Dim_DateIdQuotationValidFrom <>  ifnull( dt.Dim_Dateid,1);

			
UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl, dim_Date dt 
		SET sof.Dim_DateIdQuotationValidTo =
				ifnull( dt.Dim_Dateid,1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1
			AND dt.DateValue = vbk.VBAK_BNDDT
            AND dt.CompanyCode = pl.CompanyCode  
			AND sof.dd_scheduleno <> 0 
			AND  sof.Dim_DateIdQuotationValidTo <>  ifnull( dt.Dim_Dateid,1);


UPDATE  fact_salesorderlife_so_tmp sof
FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
SET sof.Dim_DateIdSOCreated =
         ifnull( dt.Dim_Dateid,1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1
			AND  dt.DateValue = vbk.VBAK_ERDAT 
			AND dt.CompanyCode = pl.CompanyCode  
			AND sof.dd_scheduleno <> 0 
			AND sof.Dim_DateIdSOCreated <>  ifnull( dt.Dim_Dateid,1);

UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
		SET sof.Dim_DateIdSOItemChangedOn = dt.Dim_Dateid
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
		AND  dt.DateValue = vbk.VBAP_AEDAT 
		AND dt.CompanyCode = pl.CompanyCode  
		AND sof.dd_scheduleno <> 0 
		AND vbk.VBAP_AEDAT IS NOT NULL;


UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl,dim_Date dt
		SET sof.Dim_DateIdSODocument =
				ifnull(  dt.Dim_Dateid,1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
	    AND pl.plantcode = vbk.VBAP_WERKS 
	    AND pl.rowiscurrent = 1
	    AND  dt.DateValue = vbk.VBAK_AUDAT
            AND dt.CompanyCode = pl.CompanyCode  
	    AND sof.dd_scheduleno <> 0 
	    AND sof.Dim_DateIdSODocument <>  ifnull(  dt.Dim_Dateid,1) ;

UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl
		SET sof.dd_ReferenceDocumentNo = ifnull(vbk.VBAP_VGBEL,'Not Set')
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
	    AND pl.plantcode = vbk.VBAP_WERKS 
	    AND pl.rowiscurrent = 1  
	    AND sof.dd_scheduleno <> 0 	
	    AND  ifnull(sof.dd_ReferenceDocumentNo,'X') <> ifnull(vbk.VBAP_VGBEL,'Not Set');

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap_vbep vbk,dim_plant pl
		SET  sof.dd_ReferenceDocItemNo = ifnull(vbk.VBAP_VGPOS,0)
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND sof.dd_ScheduleNo = vbk.VBEP_ETENR
		AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
		AND  sof.dd_ReferenceDocItemNo <> ifnull(vbk.VBAP_VGPOS,0);


UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl
		SET sof.dd_AfsAllocationGroupNo = ifnull(vbk.VBEP_AFS_AGNUM,'Not Set')
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
	    AND pl.plantcode = vbk.VBAP_WERKS 
	    AND pl.rowiscurrent = 1  
	    AND sof.dd_scheduleno <> 0 
	    AND vbk.VBEP_AFS_AGNUM IS NOT NULL;

UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk
		SET sof.dd_AfsAllocationGroupNo = 'Not Set'
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND (vbk.VBEP_AFS_AGNUM IS NULL OR sof.dd_AfsAllocationGroupNo IS NULL);

UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk 
		SET sof.dd_CustomerMaterialNo = ifnull(ifnull(vbk.VBEP_J_3AKDMAT,vbk.VBAP_KDMAT),'Not Set')
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
	    AND    sof.dd_scheduleno <> 0 
	    AND  (vbk.VBAP_KDMAT IS NOT NULL OR vbk.VBEP_J_3AKDMAT IS NOT NULL);

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap_vbep vbk,Dim_AvailabilityCheck ac
		SET  sof.Dim_AvailabilityCheckId = ac.Dim_AvailabilityCheckId
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND  ac.AvailabilityCheck = vbk.VBAP_MTVFP AND ac.rowiscurrent = 1
		AND  sof.Dim_AvailabilityCheckId <> ac.Dim_AvailabilityCheckId
		AND vbk.VBAP_MTVFP IS NOT NULL;

UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_ShippingCondition sc
		SET sof.Dim_ShippingConditionId = sc.Dim_ShippingConditionId
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND vbk.VBAK_VSBED IS NOT NULL
		AND sc.ShippingConditionCode = vbk.VBAK_VSBED
		AND sc.RowIsCurrent = 1;

UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk 
		SET sof.Dim_ShippingConditionId = 1
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND vbk.VBAK_VSBED IS NULL;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep, dim_materialpricegroup4 mg4
		SET sof.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
WHERE sof.dd_SalesDocNo = VBAK_VBELN
		AND sof.dd_SalesItemNo = VBAP_POSNR
		AND sof.dd_ScheduleNo = VBEP_ETENR
		AND VBAP_MVGR4 is not null
		AND VBAP_MVGR4 = mg4.MaterialPriceGroup4
		AND mg4.RowIsCurrent = 1;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap, dim_materialpricegroup4 mg4
		SET sof.dim_materialpricegroup4id = mg4.dim_materialpricegroup4id
WHERE sof.dd_SalesDocNo = VBAP_VBELN
		AND sof.dd_SalesItemNo = VBAP_POSNR
		AND sof.dd_ScheduleNo = 0
		AND VBAP_MVGR4 is not null
		AND VBAP_MVGR4 = mg4.MaterialPriceGroup4
		AND mg4.RowIsCurrent = 1;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep, dim_materialpricegroup5 mg5
		SET sof.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
WHERE sof.dd_SalesDocNo = VBAK_VBELN
		AND sof.dd_SalesItemNo = VBAP_POSNR
		AND sof.dd_ScheduleNo = VBEP_ETENR
		AND VBAP_MVGR5 is not null
		AND VBAP_MVGR5 = mg5.MaterialPriceGroup5
		AND mg5.RowIsCurrent = 1;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap, dim_materialpricegroup5 mg5
		SET sof.dim_materialpricegroup5id = mg5.dim_materialpricegroup5id
WHERE sof.dd_SalesDocNo = VBAP_VBELN
		AND sof.dd_SalesItemNo = VBAP_POSNR
		AND sof.dd_ScheduleNo = 0
		AND VBAP_MVGR5 is not null
		AND VBAP_MVGR5 = mg5.MaterialPriceGroup5
		AND mg5.RowIsCurrent = 1;

UPDATE fact_salesorderlife_so_tmp sof
 FROM vbak_vbap_vbkd vkd,dim_company dc,dim_salesdistrict sd
  SET sof.Dim_SalesDistrictId = ifnull(sd.Dim_SalesDistrictid,1)
WHERE sd.SalesDistrict = vkd.VBKD_BZIRK
  AND sd.RowIsCurrent = 1	
  AND sof.dd_SalesDocNo = vkd.VBKD_VBELN
  AND sof.dd_salesitemno = vkd.VBKD_POSNR
  AND dc.dim_companyid=sof.dim_companyid
  AND sof.Dim_SalesDistrictId <> ifnull(sd.Dim_SalesDistrictid,1);


UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbkd vkd,dim_company dc,dim_accountassignmentgroup aag
		SET sof.Dim_AccountAssignmentGroupId = 
				ifnull(aag.Dim_AccountAssignmentGroupId,1)
WHERE  sof.dd_SalesDocNo = vkd.VBKD_VBELN
		AND sof.dd_SalesItemNo = vkd.VBKD_POSNR
		AND aag.AccountAssignmentGroup = vkd.VBKD_KTGRD
		AND aag.RowIsCurrent = 1
		and dc.dim_companyid=sof.dim_companyid
		AND sof.Dim_AccountAssignmentGroupId <> ifnull(aag.Dim_AccountAssignmentGroupId,1);


UPDATE    fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbkd vkd,dim_company dc
		SET sof.dd_BusinessCustomerPONo = ifnull(vkd.VBKD_BSTKD, 'Not Set')
	WHERE sof.dd_SalesDocNo = vkd.VBKD_VBELN
		AND sof.dd_SalesItemNo = vkd.VBKD_POSNR
		and dc.dim_companyid=sof.dim_companyid
		AND  vkd.VBKD_BSTKD IS NOT NULL
		AND ifnull(dd_BusinessCustomerPONo,'x') <> ifnull(vkd.VBKD_BSTKD, 'Not Set');

UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbkd vkd,dim_company dc,dim_date dd
		SET sof.Dim_BillingDateId =
				ifnull(dd.dim_dateid,1)				
WHERE sof.dd_SalesDocNo = vkd.VBKD_VBELN 
		AND sof.dd_SalesItemNo = vkd.VBKD_POSNR
		and dc.dim_companyid=sof.dim_companyid
		and dd.datevalue = VBKD_FKDAT 
		AND dd.companycode = dc.CompanyCode
		AND sof.Dim_BillingDateId <> ifnull(dd.dim_dateid,1);


/*    AFS related field population  */
UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbkd vkd,dim_company dc
		SET sof.Dim_SalesDistrictId =
				ifnull(
					(SELECT sd.Dim_SalesDistrictid
						FROM dim_salesdistrict sd
					WHERE sd.SalesDistrict = vkd.VBKD_BZIRK
							AND sd.RowIsCurrent = 1),
				1)
WHERE sof.dd_SalesDocNo = vkd.VBKD_VBELN
            AND sof.dd_SalesItemNo = vkd.VBKD_POSNR
			AND  dc.dim_companyid = sof.dim_companyid
			AND sof.Dim_SalesDistrictId <> 
					ifnull(
						(SELECT sd.Dim_SalesDistrictid
							FROM dim_salesdistrict sd
						WHERE sd.SalesDistrict = vkd.VBKD_BZIRK
						AND sd.RowIsCurrent = 1),
					1) ;


UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbkd vkd,dim_company dc
		set sof.Dim_AccountAssignmentGroupId =
				ifnull(
					(SELECT aag.Dim_AccountAssignmentGroupId
						FROM dim_accountassignmentgroup aag
					WHERE aag.AccountAssignmentGroup = vkd.VBKD_KTGRD
						AND aag.RowIsCurrent = 1),
					1)
WHERE sof.dd_SalesDocNo = vkd.VBKD_VBELN
            AND sof.dd_SalesItemNo = vkd.VBKD_POSNR
			AND  dc.dim_companyid = sof.dim_companyid
			AND sof.Dim_AccountAssignmentGroupId <> 
					ifnull(
						(SELECT aag.Dim_AccountAssignmentGroupId
							FROM dim_accountassignmentgroup aag
						WHERE aag.AccountAssignmentGroup = vkd.VBKD_KTGRD
							AND aag.RowIsCurrent = 1),
						1) ;

UPDATE   fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbkd vkd,dim_company dc
		set sof.dd_BusinessCustomerPONo = ifnull(vkd.VBKD_BSTKD, 'Not Set')
WHERE sof.dd_SalesDocNo = vkd.VBKD_VBELN
            AND sof.dd_SalesItemNo = vkd.VBKD_POSNR
			AND  dc.dim_companyid = sof.dim_companyid
			AND  sof.dd_BusinessCustomerPONo <> ifnull(vkd.VBKD_BSTKD, 'Not Set') ;



UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbkd vkd,dim_company dc
		set sof.Dim_BillingDateId =
				ifnull(
					(SELECT dim_dateid
						FROM dim_date
					WHERE datevalue = VBKD_FKDAT 
						AND companycode = dc.CompanyCode),
					1)
WHERE sof.dd_SalesDocNo = vkd.VBKD_VBELN
            AND sof.dd_SalesItemNo = vkd.VBKD_POSNR
			AND  dc.dim_companyid = sof.dim_companyid;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET sof.dd_CustomerPONo = ifnull(vbk.VBAK_BSTNK,'Not Set')
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
		AND sof.dd_ScheduleNo = 0
		AND  sof.dd_CustomerPONo <> ifnull(vbk.VBAK_BSTNK,'Not Set');

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET sof.Dim_CreditRepresentativeId =
				ifnull(
					(SELECT Dim_CreditRepresentativegroupId
						FROM dim_creditrepresentativegroup cg
					WHERE     cg.CreditRepresentativeGroup = vbk.VBAK_SBGRP
						AND cg.CreditControlArea = vbk.VBAK_KKBER
						AND cg.RowIsCurrent = 1),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET sof.Dim_MaterialPriceGroup1Id =
				ifnull(
					(SELECT Dim_MaterialPriceGroup1Id
						FROM Dim_MaterialPriceGroup1 mpg
					WHERE mpg.MaterialPriceGroup1 = vbk.VBAP_MVGR1
						AND mpg.RowIsCurrent = 1),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;


UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl,Dim_MaterialPriceGroup2 mpg
		SET sof.Dim_MaterialPriceGroup2id = mpg.Dim_MaterialPriceGroup2Id
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
	AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0
	AND mpg.MaterialPriceGroup2 = vbk.VBAP_MVGR2
        AND mpg.RowIsCurrent = 1;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET  sof.Dim_DeliveryBlockId =
				ifnull(
					(SELECT Dim_DeliveryBlockId
						FROM Dim_DeliveryBlock db
					WHERE db.DeliveryBlock = vbk.VBAK_LIFSK
						AND db.RowIsCurrent = 1),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET sof.Dim_SalesDocOrderReasonId =
			ifnull(
				(SELECT dim_salesdocorderreasonid
					FROM dim_salesdocorderreason sor
				WHERE sor.ReasonCode = vbk.VBAK_AUGRU AND sor.RowIsCurrent = 1),
				1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
	AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET  sof.Dim_MaterialGroupId =
				ifnull(
					(SELECT dim_materialgroupid
						FROM dim_materialgroup mg
					WHERE mg.MaterialGroupCode = vbk.VBAP_MATKL
						AND mg.RowIsCurrent = 1),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
	AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
  FROM vbak_vbap vbk
   SET sof.amt_SubTotal1 = ifnull(vbk.VBAP_KZWI1, 0)
 WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND  sof.amt_SubTotal1 <> ifnull(vbk.VBAP_KZWI1, 0);

UPDATE fact_salesorderlife_so_tmp sof
  FROM vbak_vbap vbk
   SET sof.amt_SubTotal2 = ifnull(vbk.VBAP_KZWI2, 0)
 WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND sof.amt_SubTotal2 <> ifnull(vbk.VBAP_KZWI2, 0);

UPDATE fact_salesorderlife_so_tmp sof
  FROM vbak_vbap vbk,dim_plant pl
   SET sof.amt_SubTotal3 = ifnull(vbk.VBAP_KZWI3, 0)
 WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND pl.plantcode = vbk.VBAP_WERKS 
   AND pl.rowiscurrent = 1
   AND sof.dd_ScheduleNo = 0
   AND sof.amt_SubTotal3 <> ifnull(vbk.VBAP_KZWI3, 0);

UPDATE fact_salesorderlife_so_tmp sof
   SET sof.amt_Subtotal3_OrderQty = amt_SubTotal3
 WHERE sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
  FROM vbak_vbap vbk,dim_plant pl
   SET sof.amt_SubTotal4 = ifnull(vbk.VBAP_KZWI4, 0)
 WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND pl.plantcode = vbk.VBAP_WERKS 
   AND pl.rowiscurrent = 1
   AND sof.dd_ScheduleNo = 0
   AND sof.amt_SubTotal4 <> ifnull(vbk.VBAP_KZWI4, 0);   
   
UPDATE fact_salesorderlife_so_tmp sof
  FROM vbak_vbap vbk
   SET sof.amt_SubTotal5 = ifnull(vbk.VBAP_KZWI5, 0)
 WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND sof.amt_SubTotal5 <> ifnull(vbk.VBAP_KZWI5, 0);

UPDATE fact_salesorderlife_so_tmp sof
  FROM vbak_vbap vbk
   SET sof.amt_SubTotal6 = ifnull(vbk.VBAP_KZWI6, 0)
 WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND sof.amt_SubTotal6 <> ifnull(vbk.VBAP_KZWI6, 0);

UPDATE fact_salesorderlife_so_tmp sof
  FROM vbak_vbap vbk
   SET sof.dd_DocumentConditionNo = ifnull(vbk.VBAK_KNUMV, 'Not Set')
 WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND sof.dd_DocumentConditionNo <> ifnull(vbk.VBAK_KNUMV, 'Not Set');

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET    sof.Dim_PurchaseOrderTypeId =
				ifnull(
					(SELECT cpt.dim_customerpurchaseordertypeid
						FROM dim_customerpurchaseordertype cpt
					WHERE cpt.CustomerPOType = vbk.VBAK_BSARK
						AND cpt.RowIsCurrent = 1),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET    sof.Dim_DateIdPurchaseOrder =
				ifnull(
					(SELECT dt.Dim_Dateid
						FROM dim_Date dt
					WHERE dt.DateValue = vbk.VBAK_BSTDK
						AND dt.CompanyCode = pl.CompanyCode),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET     sof.Dim_DateIdQuotationValidFrom =
				ifnull(
					(SELECT dt.Dim_Dateid
						FROM dim_Date dt
					WHERE dt.DateValue = vbk.VBAK_ANGDT
						AND dt.CompanyCode = pl.CompanyCode),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET   sof.Dim_DateIdQuotationValidTo =
				ifnull(
					(SELECT dt.Dim_Dateid
						FROM dim_Date dt
					WHERE dt.DateValue = vbk.VBAK_BNDDT
						AND dt.CompanyCode = pl.CompanyCode),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET    sof.Dim_DateIdSOCreated =
				ifnull(
					(SELECT dt.Dim_Dateid
						FROM dim_Date dt
					WHERE dt.DateValue = vbk.VBAK_ERDAT
						AND dt.CompanyCode = pl.CompanyCode),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET    sof.Dim_DateIdSOItemChangedOn =
				ifnull(
					(SELECT dt.Dim_Dateid
						FROM dim_Date dt
					WHERE dt.DateValue = vbk.VBAP_AEDAT
						AND dt.CompanyCode = pl.CompanyCode),
					1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap vbk,dim_ShippingCondition sc
		SET sof.Dim_ShippingConditionId =
			sc.Dim_ShippingConditionId
WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  sof.dd_ScheduleNo = 0
		AND sc.ShippingConditionCode = ifnull(vbk.VBAK_VSBED ,'Not Set')
		AND sc.rowIsCurrent = 1
		AND ifnull(sof.Dim_ShippingConditionId,-1) <> sc.Dim_ShippingConditionId;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET  sof.Dim_DateIdSODocument =
			ifnull(
				(SELECT dt.Dim_Dateid
					FROM dim_Date dt
				WHERE dt.DateValue = vbk.VBAK_AUDAT
                     AND dt.CompanyCode = pl.CompanyCode),
				1)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0;

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET  sof.dd_ReferenceDocumentNo = ifnull(vbk.VBAP_VGBEL,'Not Set')
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0
		AND  ifnull(sof.dd_ReferenceDocumentNo,'X') <> ifnull(vbk.VBAP_VGBEL,'Not Set');

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,dim_plant pl
		SET  sof.dd_ReferenceDocItemNo = ifnull(vbk.VBAP_VGPOS,0)
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  pl.plantcode = vbk.VBAP_WERKS 
		AND pl.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0
		AND  sof.dd_ReferenceDocItemNo <> ifnull(vbk.VBAP_VGPOS,0);

UPDATE fact_salesorderlife_so_tmp sof
	FROM  vbak_vbap vbk,Dim_AvailabilityCheck ac
		SET  sof.Dim_AvailabilityCheckId = ac.Dim_AvailabilityCheckId
WHERE  vbk.VBAP_VBELN = sof.dd_SalesDocNo
        AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
        AND  ac.AvailabilityCheck = vbk.VBAP_MTVFP 
		AND ac.rowiscurrent = 1
        AND sof.dd_ScheduleNo = 0
		AND  sof.Dim_AvailabilityCheckId <> ac.Dim_AvailabilityCheckId
		AND vbk.VBAP_MTVFP IS NOT NULL
		AND ifnull(sof.Dim_AvailabilityCheckId,-1) <> ac.Dim_AvailabilityCheckId;

call vectorwise(combine 'fact_salesorderlife_so_tmp');

DROP TABLE IF EXISTS tmp_CustomerCreditLimit;

CREATE TABLE tmp_CustomerCreditLimit AS
	SELECT distinct c.CUSTOMER,ifnull(a.CREDIT_LIMIT,0) AS Credit_Limit,a.LIMIT_VALID_DATE,RANK() OVER (PARTITION BY c.CUSTOMER ORDER BY a.LIMIT_VALID_DATE ASC) 
		AS Rank 
			FROM UKMBP_CMS_SGM a 
				inner join BUT000 b on a.PARTNER = b.PARTNER and TIMESTAMP(LOCAL_TIMESTAMP) <= a.LIMIT_VALID_DATE
				inner join cvi_cust_link c on c.PARTNER_GUID = b.PARTNER_GUID;

DELETE FROM tmp_CustomerCreditLimit WHERE Rank <> 1;

UPDATE fact_salesorderlife_so_tmp fso
	FROM    dim_customer dc,
        tmp_CustomerCreditLimit  t
		SET     dd_CreditLimit = t.Credit_Limit
WHERE	 dc.Dim_CustomerId = fso.Dim_CustomerId
		AND dc.CustomerNumber = t.customer;

                                  
DROP TABLE IF EXISTS tmp_CustomerCreditLimit;

DROP TABLE IF EXISTS tmp_upd_702;

CREATE TABLE tmp_upd_702 
	AS 
		Select first 0 
			(case when b.NAME_FIRST is null then b.NAME_LAST else b.NAME_FIRST+' '+ifnull(b.NAME_LAST,'') end) updcol1,d.BUT050_PARTNER2 updVBAK_KUNNR
				FROM cvi_cust_link c inner join but000 b1 on c.PARTNER_GUID = b1.PARTNER_GUID
					inner join BUT050 d on b1.PARTNER = d.BUT050_PARTNER2 AND d.BUT050_RELTYP = 'UKMSB0'
					inner join but000 b on b.PARTNER = d.BUT050_PARTNER1
		where  ANSIDATE(CURRENT_DATE) between ANSIDATE(d.BUT050_DATE_FROM) 
				and ANSIDATE(d.BUT050_DATE_TO)
				and (b.NAME_FIRST is not null or b.NAME_LAST is not null and b.BU_GROUP = 'CRED')
		order by c.customer ;

update fact_salesorderlife sof
	FROM   tmp_upd_702 t,
		   Dim_Customer c
		SET dd_CreditRep = t.updcol1
WHERE sof.dim_Customerid = c.dim_customerid
		and t.updVBAK_KUNNR = c.CustomerNumber
		AND ifnull(sof.dd_CreditRep,'xxx') <> ifnull(t.updcol1,'yyy');

DROP TABLE IF EXISTS tmp_upd_702;

DROP TABLE IF EXISTS tmp_upd_702;

CREATE TABLE tmp_upd_702 
AS 
	Select first 0 
		(case when b.MC_NAME1 is null then b.MC_NAME2 else b.MC_NAME1+' '+ifnull(b.MC_NAME2,'') end) updcol1,d.BUT050_PARTNER2 updVBAK_KUNNR
			FROM cvi_cust_link c inner join but000 b1 on c.PARTNER_GUID = b1.PARTNER_GUID
				inner join BUT050 d on b1.PARTNER = d.BUT050_PARTNER2
				inner join BUT050 d1 on d1.BUT050_PARTNER2 = d.BUT050_PARTNER1
				inner join but000 b on b.PARTNER = d1.BUT050_PARTNER1
	where  ANSIDATE(CURRENT_DATE) between ANSIDATE(d.BUT050_DATE_FROM) 
			and ANSIDATE(d.BUT050_DATE_TO)
			and (b.MC_NAME1 is not null or b.MC_NAME2 is not null)
	order by c.customer ;

update fact_salesorderlife_so_tmp sof
	FROM   tmp_upd_702 t,
           Dim_Customer c
		SET dd_CreditMgr = t.updcol1
WHERE sof.dim_Customerid = c.dim_customerid
	and t.updVBAK_KUNNR = c.CustomerNumber
	AND ifnull(sof.dd_CreditMgr,'xxx') <> ifnull(t.updcol1,'yyy');

DROP TABLE IF EXISTS tmp_upd_702;

call vectorwise(combine 'fact_salesorderlife_so_tmp');

UPDATE  fact_salesorderlife_so_tmp sof
	FROM j_3abdbs ast
		SET sof.dd_AfsStockType = ast.J_3ABDBS_J_3ABSKZ
WHERE  sof.dd_SalesDocNo = ast.J_3ABDBS_AUFNR
		AND sof.dd_SalesItemNo = ast.J_3ABDBS_POSNR
		AND sof.dd_ScheduleNo = ast.J_3ABDBS_ETENR 
		AND ifnull(sof.dd_AfsStockType,'X') <>  ifnull(ast.J_3ABDBS_J_3ABSKZ,'Not Set');

UPDATE fact_salesorderlife_so_tmp sof
  FROM j_3abdbs ast
   SET sof.dd_ArunNumber = ast.J_3ABDBS_ARNUM
 WHERE sof.dd_SalesDocNo = ast.J_3ABDBS_AUFNR
   AND sof.dd_SalesItemNo = ast.J_3ABDBS_POSNR
   AND sof.dd_ScheduleNo = ast.J_3ABDBS_ETENR 
   AND ast.J_3ABDBS_ARNUM IS NOT NULL
   AND IFNULL(sof.dd_ArunNumber,'xxx') <> ifnull(ast.J_3ABDBS_ARNUM,'yyy');


DROP TABLE IF EXISTS ct_group_update;
CREATE TABLE ct_group_update AS 
SELECT J_3ABDBS_AUFNR,J_3ABDBS_POSNR,J_3ABDBS_ETENR,
       J_3ABDBS_J_3ASTAT,sum(J_3ABDBS_MENGE) J_3ABDBS_MENGE
  FROM j_3abdbs
GROUP BY  J_3ABDBS_AUFNR,J_3ABDBS_POSNR,J_3ABDBS_ETENR,J_3ABDBS_J_3ASTAT;

    UPDATE fact_salesorderlife_so_tmp sof
      SET sof.ct_AfsAllocationFQty =
              IFNULL(
                (SELECT ast.J_3ABDBS_MENGE
                    FROM ct_group_update ast
                  WHERE     ast.J_3ABDBS_AUFNR = sof.dd_SalesDocNo
                        AND ast.J_3ABDBS_POSNR = sof.dd_SalesItemNo
                        AND ast.J_3ABDBS_ETENR = sof.dd_ScheduleNo
			AND ast.J_3ABDBS_J_3ASTAT = 'F'	
                        ),
                0)
WHERE sof.ct_AfsAllocationFQty <> 
              IFNULL(
                (SELECT ast.J_3ABDBS_MENGE
                    FROM ct_group_update ast
                  WHERE     ast.J_3ABDBS_AUFNR = sof.dd_SalesDocNo
                        AND ast.J_3ABDBS_POSNR = sof.dd_SalesItemNo
                        AND ast.J_3ABDBS_ETENR = sof.dd_ScheduleNo
                        AND ast.J_3ABDBS_J_3ASTAT = 'F'
                        ),
                0)
;

    UPDATE fact_salesorderlife_so_tmp sof
      SET sof.ct_AfsAllocationRQty =
              IFNULL(
                (SELECT ast.J_3ABDBS_MENGE
                    FROM  ct_group_update ast
                  WHERE     ast.J_3ABDBS_AUFNR = sof.dd_SalesDocNo
                        AND ast.J_3ABDBS_POSNR = sof.dd_SalesItemNo
                        AND ast.J_3ABDBS_ETENR = sof.dd_ScheduleNo
                        AND ast.J_3ABDBS_J_3ASTAT = 'R'),
                0)
WHERE ifnull(ct_AfsAllocationRQty,-1) <> 
              IFNULL(
                (SELECT ast.J_3ABDBS_MENGE
                    FROM  ct_group_update ast
                  WHERE     ast.J_3ABDBS_AUFNR = sof.dd_SalesDocNo
                        AND ast.J_3ABDBS_POSNR = sof.dd_SalesItemNo
                        AND ast.J_3ABDBS_ETENR = sof.dd_ScheduleNo
                        AND ast.J_3ABDBS_J_3ASTAT = 'R'),
                0) ;


  UPDATE fact_salesorderlife_so_tmp sof
   SET sof.ct_AfsAllocatedQty =
          IFNULL(
             (SELECT SUM(ast.J_3ABDBS_MENGE)
                FROM ct_group_update ast
               WHERE     ast.J_3ABDBS_AUFNR = sof.dd_SalesDocNo
                    AND ast.J_3ABDBS_POSNR = sof.dd_SalesItemNo
                    AND ast.J_3ABDBS_ETENR = sof.dd_ScheduleNo
					AND J_3ABDBS_J_3ASTAT IN ('R','F')),
             0);

DROP TABLE IF EXISTS ct_group_update;

UPDATE fact_salesorderlife_so_tmp sof
  FROM j_3abdsi afs
   SET sof.ct_AfsOpenQty = ifnull(J_3ABDSI_MENGE, 0)
 WHERE sof.dd_SalesDocNo = afs.J_3ABDSI_AUFNR
   AND sof.dd_SalesItemNo = afs.J_3ABDSI_POSNR
   AND sof.dd_ScheduleNo = afs.J_3ABDSI_ETENR 
   AND ifnull(sof.ct_AfsOpenQty,0) <> ifnull(J_3ABDSI_MENGE, 0);


UPDATE  fact_salesorderlife_so_tmp sof
    FROM j_3abdsi afs
      SET  sof.dd_RequirementType = ifnull(afs.J_3ABDSI_BDART, 'Not Set')
     WHERE  sof.dd_SalesDocNo = afs.J_3ABDSI_AUFNR
            AND sof.dd_SalesItemNo = afs.J_3ABDSI_POSNR
            AND sof.dd_ScheduleNo = afs.J_3ABDSI_ETENR 
			AND sof.dd_RequirementType <> ifnull(afs.J_3ABDSI_BDART, 'Not Set');

UPDATE  fact_salesorderlife_so_tmp sof
    FROM j_3abdsi afs,
	 Dim_Plant pl
      SET  sof.Dim_DateidSchedDeliveryReq=
		ifnull((SELECT Dim_Dateid
					FROM Dim_Date dd
				WHERE dd.DateValue = afs.J_3ABDSI_vdatu 
					AND dd.CompanyCode = pl.CompanyCode),
				1) 
WHERE  sof.Dim_Plantid = pl.Dim_Plantid
			AND  sof.dd_SalesDocNo = afs.J_3ABDSI_AUFNR
			AND sof.dd_SalesItemNo = afs.J_3ABDSI_POSNR
			AND sof.dd_ScheduleNo = afs.J_3ABDSI_ETENR;

UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk,dim_plant pl
      SET sof.dd_AfsStockCategory = ifnull(vbk.VBEP_J_4KRCAT,'Not Set')
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
			AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1 
			AND sof.dd_AfsStockCategory <>  ifnull(vbk.VBEP_J_4KRCAT,'Not Set');


UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk,dim_plant pl
		SET sof.dd_AfsDepartment = ifnull(vbk.VBAP_J_3ADEPM,'Not Set')
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1 
			AND sof.dd_AfsDepartment <>  ifnull(vbk.VBAP_J_3ADEPM,'Not Set');


UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk,dim_plant pl,dim_date dd
		SET sof.Dim_DateIdAfsCancelDate = dim_dateid
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND vbk.VBAP_POSNR = sof.dd_SalesItemNo 
			AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND vbk.VBAP_J_3ACADA IS NOT NULL
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1 
			AND dd.DateValue = vbk.VBAP_J_3ACADA AND dd.CompanyCode = pl.CompanyCode;


UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk
		SET sof.Dim_DateIdAfsCancelDate = 1
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND vbk.VBAP_POSNR = sof.dd_SalesItemNo 
			AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND vbk.VBAP_J_3ACADA IS NULL;

UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk,dim_plant pl,dim_date dd
		set sof.Dim_DateIdAfsReqDelivery = dim_dateid
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND  vbk.VBAP_POSNR = sof.dd_SalesItemNo
			AND  vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND vbk.VBAP_J_3ARQDA IS NOT NULL
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1 
			AND dd.DateValue = vbk.VBAP_J_3ARQDA
			AND dd.CompanyCode = pl.CompanyCode;

UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk
		set sof.Dim_DateIdAfsReqDelivery = 1
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND  vbk.VBAP_POSNR = sof.dd_SalesItemNo
			AND  vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND vbk.VBAP_J_3ARQDA IS NULL;

UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap vbk,dim_plant pl,dim_date dd
		set sof.Dim_DateIdAfsReqDelivery = dim_dateid
WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
			AND  vbk.VBAP_POSNR = sof.dd_SalesItemNo
			AND dd_ScheduleNo = 0
			AND vbk.VBAP_J_3ARQDA IS NOT NULL
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1 
			AND dd.DateValue = vbk.VBAP_J_3ARQDA
			AND dd.CompanyCode = pl.CompanyCode;

UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap vbk
		set sof.Dim_DateIdAfsReqDelivery = 1
WHERE vbk.VBAP_VBELN = sof.dd_SalesDocNo
			AND  vbk.VBAP_POSNR = sof.dd_SalesItemNo
			AND  sof.dd_ScheduleNo = 0
			AND vbk.VBAP_J_3ARQDA IS NULL;
 
UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl
		set sof.amt_AfsNetValue = ifnull(VBEP_J_3ANETW,0)
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1 
			AND  sof.amt_AfsNetValue <> ifnull(VBEP_J_3ANETW,0);


UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk, Dim_SalesOrderRejectReason sorr
		SET sof.Dim_AfsRejectionReasonId =  sorr.Dim_SalesOrderRejectReasonid
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
			AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND vbk.VBEP_J_3AABGRU IS NOT NULL
			AND sorr.RejectReasonCode = vbk.VBEP_J_3AABGRU
			AND sof.Dim_AfsRejectionReasonId  <> sorr.Dim_SalesOrderRejectReasonid;

UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk
		SET sof.Dim_AfsRejectionReasonId =  1
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
			AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
			AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND vbk.VBEP_J_3AABGRU IS NULL;

/* 8/25/2015 - Cristina I - changed the update script for dim_afssizeid */	

UPDATE  fact_salesorderlife_so_tmp sof
    FROM vbak_vbap_vbep vbk,dim_plant pl,dim_afssize dz, dim_part dp
		SET sof.Dim_AfsSizeId = ifnull( dz.dim_AfsSizeId,1)
	WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
	    AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
	    AND dz.Size = vbk.VBEP_J_3ASIZE AND dz.RowIsCurrent = 1
		AND dp.PartNumber = dz.Material
	  	AND sof.dim_partid = dp.dim_partid
	    AND sof.Dim_AfsSizeId <>  ifnull( dz.dim_AfsSizeId,1);


UPDATE  fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk,dim_plant pl
		SET sof.amt_AfsNetPrice =
              ifnull(
                Decimal((vbk.VBEP_J_3ANETP
                  ),18,4),
                0)
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
            AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
            AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
			AND pl.plantcode = vbk.VBAP_WERKS 
			AND pl.rowiscurrent = 1 ;

DROP TABLE IF EXISTS tmp_NetDueDateCalc;
DROP TABLE IF EXISTS tmp_SalesOrder_ForNetDueDateCalc;

CREATE TABLE tmp_SalesOrder_ForNetDueDateCalc 
AS
SELECT distinct dd_SalesDocNo,dd_SalesItemNo,dd_scheduleNo,
(CASE WHEN fso.Dim_DateIdFixedValue <> 1  THEN fso.Dim_DateIdFixedValue WHEN fso.Dim_DateIdFixedValue = 1 THEN fso.Dim_DateIdAfsReqDelivery ELSE 1 END) AS Dim_DateIdDeliv,
(CASE WHEN fso.Dim_PaymentTermsId <> 1  THEN fso.Dim_PaymentTermsId WHEN fso.Dim_PaymentTermsId = 1 THEN fso.Dim_CustomerPaymentTermsId ELSE 1 END) AS Dim_PayTermsId
FROM fact_salesorderlife_so_tmp fso;


CREATE TABLE tmp_NetDueDateCalc AS
SELECT distinct dd_SalesDocNo,dd_SalesItemNo,dd_scheduleNo,dt.CompanyCode, (dt.datevalue + (interval '1' DAY) *(CASE WHEN T052_ZTAG3 IS NULL THEN (CASE WHEN T052_ZTAG2 IS NULL THEN T052_ZTAG1 ELSE T052_ZTAG2 END) ELSE T052_ZTAG3 END)) AS DueDateValue
FROM tmp_SalesOrder_ForNetDueDateCalc fso,
     T052 t,
     dim_date dt,
     Dim_CustomerPaymentTerms pt
WHERE fso.Dim_DateIdDeliv = dt.Dim_DateId
	AND fso.Dim_PayTermsId = pt.Dim_CustomerPaymentTermsId
	AND t.T052_ZTERM = pt.PaymentTermCode
	AND fso.Dim_DateIdDeliv > 1;

UPDATE fact_salesorderlife_so_tmp fso
    FROM
       tmp_NetDueDateCalc t,
       dim_Date dt
  SET fso.Dim_DateIdNetDueDate = dt.Dim_DateId
WHERE fso.dd_SalesDocNo = t.dd_SalesDocNo
  AND fso.dd_SalesItemNo = t.dd_SalesItemNo
  AND fso.dd_ScheduleNo = t.dd_ScheduleNo
  AND dt.DateValue = t.DueDateValue
  AND dt.companyCode = t.CompanyCode;

DROP TABLE IF EXISTS tmp_SalesOrder_ForNetDueDateCalc;
DROP TABLE IF EXISTS tmp_NetDueDateCalc;

UPDATE fact_salesorderlife_so_tmp
SET Dim_DateIdNetDueDate = 1
WHERE Dim_DateIdNetDueDate IS NULL;

UPDATE fact_salesorderlife_so_tmp sof FROM
       dim_plant pl, 
       cdpos_rejdate cpr,
       cdhdr_vbap_vbep_rejdate chr,
	Dim_Date dd
   SET sof.Dim_DateIdRejection =  dd.Dim_Dateid
where   cpr.CDPOS_CHANGENR = chr.CDHDR_CHANGENR
			AND chr.CDHDR_OBJECTID = sof.dd_SalesDocNo
			AND cpr.CDPOS_FNAME = 'J_3AABGRU'
			AND cpr.CDPOS_TABNAME = 'VBEP'
			AND substr(cpr.CDPOS_TABKEY, 4, 10) = sof.dd_SalesDocNo
			AND substr(cpr.CDPOS_TABKEY, 14, 6) = sof.dd_SalesItemNo
			AND substr(cpr.CDPOS_TABKEY, 20,4 ) = sof.dd_ScheduleNo
			AND sof.Dim_Plantid = pl.dim_plantid
			AND dd.DateValue = chr.CDHDR_UDATE
			AND dd.CompanyCode = pl.CompanyCode
			AND sof.Dim_DateIdRejection <> dd.Dim_Dateid;
			

UPDATE    fact_salesorderlife_so_tmp sof
	FROM vbfa_vbak_vbap f
		SET sof.ct_AfsTotalDrawn = 0
WHERE  f.VBFA_VBELV = sof.dd_SalesDocNo
		AND f.VBFA_POSNV = sof.dd_SalesItemNo
        AND f.VBFA_J_3AETENR = sof.dd_ScheduleNo
        AND f.VBFA_VBTYP_N = 'J'
		AND  sof.ct_AfsTotalDrawn <> 0;


UPDATE    fact_salesorderlife_so_tmp sof
	FROM vbfa_vbak_vbap f
		SET sof.dd_SubsequentDocNo = ifnull(f.VBFA_VBELN, 'Not Set')
WHERE  f.VBFA_VBELV = sof.dd_SalesDocNo
        AND f.VBFA_POSNV = sof.dd_SalesItemNo
        AND f.VBFA_J_3AETENR = sof.dd_ScheduleNo
        AND f.VBFA_VBTYP_N = 'J'
		AND sof.dd_SubsequentDocNo <> ifnull(f.VBFA_VBELN, 'Not Set');


UPDATE    fact_salesorderlife_so_tmp sof
	FROM vbfa_vbak_vbap f
		SET  sof.dd_SubsDocItemNo = f.VBFA_POSNN
WHERE  f.VBFA_VBELV = sof.dd_SalesDocNo
        AND f.VBFA_POSNV = sof.dd_SalesItemNo
        AND f.VBFA_J_3AETENR = sof.dd_ScheduleNo
        AND f.VBFA_VBTYP_N = 'J'
		AND  sof.dd_SubsDocItemNo <> f.VBFA_POSNN;	

UPDATE    fact_salesorderlife_so_tmp sof
	FROM vbfa_vbak_vbap f
		SET sof.Dim_SubsDocCategoryId =
              ifnull((SELECT Dim_DocumentCategoryid
                        FROM Dim_DocumentCategory dc
                      WHERE  dc.DocumentCategory = f.VBFA_VBTYP_N),
                    1)
WHERE  f.VBFA_VBELV = sof.dd_SalesDocNo
				AND f.VBFA_POSNV = sof.dd_SalesItemNo
				AND f.VBFA_J_3AETENR = sof.dd_ScheduleNo 
				AND f.VBFA_VBTYP_N = 'J';


UPDATE       fact_salesorderlife_so_tmp sof
	FROM j_3avbfae afsv,Dim_plant pl
SET sof.dd_SubsScheduleNo = afsv.J_3AVBFAE_ETENN
WHERE  afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
				AND  pl.dim_plantid = sof.dim_plantid
				AND afsv.J_3AVBFAE_VBTYP_N = 'J'
				AND  sof.dd_SubsScheduleNo <>  afsv.J_3AVBFAE_ETENN;


UPDATE       fact_salesorderlife_so_tmp sof
	FROM j_3avbfae afsv,Dim_plant pl
		SET sof.ct_AfsTotalDrawn = ifnull(afsv.J_3AVBFAE_RFMNG, 0)
WHERE  afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
				AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
				AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
				AND  pl.dim_plantid = sof.dim_plantid
				AND afsv.J_3AVBFAE_VBTYP_N = 'J'
				AND sof.ct_AfsTotalDrawn <>  ifnull(afsv.J_3AVBFAE_RFMNG, 0);
				
/* Take max(J_3AVBFAE_ERDAT) in order to avoid ambigous replace error*/
DROP TABLE IF EXISTS tmp_j_3avbfae_LastDate;
CREATE TABLE tmp_j_3avbfae_LastDate
AS
SELECT afsv.J_3AVBFAE_VBELV,afsv.J_3AVBFAE_POSNV,afsv.J_3AVBFAE_ETENV,
       max(J_3AVBFAE_ERDAT) J_3AVBFAE_ERDAT
  FROM j_3avbfae afsv
 WHERE afsv.J_3AVBFAE_VBTYP_N IN ('C','G')
 GROUP BY afsv.J_3AVBFAE_VBELV,afsv.J_3AVBFAE_POSNV,afsv.J_3AVBFAE_ETENV;
 
DROP TABLE IF EXISTS tmp_j_3avbfae_LastDate2;
CREATE TABLE tmp_j_3avbfae_LastDate2
 AS
SELECT sof.fact_salesorderlifeid,sof.dd_SalesDocNo,sof.dd_SalesItemNo, 
       sof.dd_ScheduleNo, pl.CompanyCode,afsv.J_3AVBFAE_ERDAT 
  FROM fact_salesorderlife_so_tmp sof, tmp_j_3avbfae_LastDate afsv, dim_plant pl, dim_Date dt          
 WHERE afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
   AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
   AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
   AND dt.CompanyCode = pl.CompanyCode
   AND dt.DateValue = afsv.J_3AVBFAE_ERDAT 
   AND pl.dim_plantid = sof.dim_plantid;

UPDATE fact_salesorderlife_so_tmp sof
  FROM tmp_j_3avbfae_LastDate2 afsv, dim_date dt
   SET sof.Dim_DateIdAsLastDate =  ifnull( dt.Dim_Dateid,1)             
 WHERE afsv.dd_SalesDocNo = sof.dd_SalesDocNo
   AND afsv.dd_SalesItemNo = sof.dd_SalesItemNo
   AND afsv.dd_ScheduleNo = sof.dd_ScheduleNo
   AND dt.CompanyCode = afsv.CompanyCode
   AND dt.DateValue = afsv.J_3AVBFAE_ERDAT;
   
DROP TABLE IF EXISTS tmp_j_3avbfae_LastDate;
DROP TABLE IF EXISTS tmp_j_3avbfae_LastDate2;


UPDATE       fact_salesorderlife_so_tmp sof
	FROM j_3avbfae afsv,Dim_plant pl
		SET  sof.dd_SubsequentDocNo = ifnull(afsv.J_3AVBFAE_VBELN, 'Not Set')
WHERE  afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
				AND  pl.dim_plantid = sof.dim_plantid
				AND afsv.J_3AVBFAE_VBTYP_N = 'J'
				AND  sof.dd_SubsequentDocNo  <>  ifnull(afsv.J_3AVBFAE_VBELN, 'Not Set');

UPDATE       fact_salesorderlife_so_tmp sof
	FROM j_3avbfae afsv,Dim_plant pl
      SET sof.dd_SubsDocItemNo = afsv.J_3AVBFAE_POSNN
WHERE  afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
				AND  pl.dim_plantid = sof.dim_plantid
				AND afsv.J_3AVBFAE_VBTYP_N = 'J'
				AND  sof.dd_SubsDocItemNo <> afsv.J_3AVBFAE_POSNN;

UPDATE       fact_salesorderlife_so_tmp sof
	FROM j_3avbfae afsv,Dim_plant pl
		SET  sof.Dim_SubsDocCategoryId =
              ifnull(
                (SELECT Dim_DocumentCategoryid
                    FROM Dim_DocumentCategory dc
                  WHERE dc.DocumentCategory = afsv.J_3AVBFAE_VBTYP_N),
                1)
WHERE  afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
                AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
                AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
				AND  pl.dim_plantid = sof.dim_plantid 
				AND afsv.J_3AVBFAE_VBTYP_N = 'J';

				
DROP TABLE IF EXISTS tmp_QuotationsDrawnQty;    
CREATE TABLE tmp_QuotationsDrawnQty
AS
SELECT J_3AVBFAE_VBELV,
      J_3AVBFAE_POSNV,
      J_3AVBFAE_ETENV,
      J_3AVBFAE_VBTYP_V,
      ifnull(sum(afsv.J_3AVBFAE_RFMNG), 0) AS TotalDrawnQty
FROM j_3avbfae afsv
WHERE afsv.J_3AVBFAE_VBTYP_V IN ('B','b','G','g')
GROUP BY J_3AVBFAE_VBELV, J_3AVBFAE_POSNV, J_3AVBFAE_ETENV,J_3AVBFAE_VBTYP_V;
      
UPDATE fact_salesorderlife_so_tmp sof
	 FROM
       dim_documentcategory dc,
       tmp_QuotationsDrawnQty afsv
   SET sof.ct_AfsTotalDrawn = TotalDrawnQty
 WHERE     dc.dim_documentcategoryid = sof.Dim_DocumentCategoryid
       AND afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
       AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
       AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
       AND afsv.J_3AVBFAE_VBTYP_V = dc.DocumentCategory
       AND dc.DocumentCategory IN ('B','b','G','g');

DROP TABLE IF EXISTS tmp_QuotationsDrawnQty;

/* Logic for the processing of columns DrawnUnits Before/In/After RDD Month */

DROP TABLE IF EXISTS tmp_RDDMonthDrawnQty;
DROP TABLE IF EXISTS tmp_RDDMonthDrawnQty_2;
    
CREATE TABLE tmp_RDDMonthDrawnQty AS
SELECT  afsv.J_3AVBFAE_VBELV,
      afsv.J_3AVBFAE_POSNV,
      afsv.J_3AVBFAE_ETENV,
      afsv.J_3AVBFAE_VBTYP_V,
      afsv.J_3AVBFAE_ERDAT,
      sum(ifnull(afsv.J_3AVBFAE_RFMNG, 0)) AS DrawnUnits     
FROM j_3avbfae afsv, dim_date ardd, fact_salesorderlife_so_tmp f_so
WHERE afsv.J_3AVBFAE_VBTYP_V IN ('B','b','G','g')
and f_so.Dim_DateIdAfsReqDelivery = ardd.dim_dateid
AND afsv.J_3AVBFAE_VBELV = f_so.dd_SalesDocNo
AND afsv.J_3AVBFAE_POSNV = f_so.dd_SalesItemNo
AND afsv.J_3AVBFAE_ETENV = f_so.dd_ScheduleNo
GROUP BY afsv.J_3AVBFAE_VBELV, 
         afsv.J_3AVBFAE_POSNV, 
         afsv.J_3AVBFAE_ETENV, 
         afsv.J_3AVBFAE_VBTYP_V,  
         afsv.J_3AVBFAE_ERDAT;
         
alter table tmp_RDDMonthDrawnQty add column rdd_date date not null default '0001-01-01';

alter table tmp_RDDMonthDrawnQty add column ct_AfsAfterRDDUnits decimal(18,4) 
not null default 0;

alter table tmp_RDDMonthDrawnQty add column ct_AfsInRDDUnits decimal(18,4) 
not null default 0 ; 

alter table tmp_RDDMonthDrawnQty add column ct_AfsBeforeRDDUnits decimal(18,4) 
not null default 0 ; 

update tmp_RDDMonthDrawnQty afsv
	FROM dim_date ardd, fact_salesorderlife_so_tmp f_so 
		set afsv.rdd_date = ardd.DateValue
where f_so.Dim_DateIdAfsReqDelivery = ardd.dim_dateid
		AND afsv.J_3AVBFAE_VBELV = f_so.dd_SalesDocNo
		AND afsv.J_3AVBFAE_POSNV = f_so.dd_SalesItemNo
		AND afsv.J_3AVBFAE_ETENV = f_so.dd_ScheduleNo;    

UPDATE tmp_RDDMonthDrawnQty
	SET ct_AfsAfterRDDUnits = ifnull(drawnunits, 0) 
WHERE rdd_date < j_3avbfae_erdat
		AND (integer(month(rdd_date)) <> integer(month(j_3avbfae_erdat))
		OR integer(year(rdd_date)) <> integer(year(j_3avbfae_erdat)));

UPDATE tmp_RDDMonthDrawnQty
	SET ct_AfsInRDDUnits = ifnull(drawnunits, 0) 
WHERE integer(month(rdd_date)) = integer(month(j_3avbfae_erdat))
		AND integer(year(rdd_date)) = integer(year(j_3avbfae_erdat));   

UPDATE tmp_RDDMonthDrawnQty
	SET ct_AfsBeforeRDDUnits = ifnull(drawnunits, 0) 
WHERE rdd_date > j_3avbfae_erdat
		AND (integer(month(rdd_date)) <> integer(month(j_3avbfae_erdat))
		OR integer(year(rdd_date)) <> integer(year(j_3avbfae_erdat)));   

CREATE TABLE tmp_RDDMonthDrawnQty_2 AS
SELECT J_3AVBFAE_VBELV, J_3AVBFAE_POSNV, J_3AVBFAE_ETENV, J_3AVBFAE_VBTYP_V, 
sum(ct_AfsInRDDUnits) ct_TotalAfsInRDD, sum(ct_AfsAfterRDDUnits) ct_TotalAfsAfterRDD, sum(ct_AfsBeforeRDDUnits) ct_TotalAfsBeforeRDD 
FROM tmp_RDDMonthDrawnQty
GROUP BY J_3AVBFAE_VBELV, J_3AVBFAE_POSNV, J_3AVBFAE_ETENV, J_3AVBFAE_VBTYP_V;
        

/* populate the column ct_AfsAfterRDDUnits - Drawn Units After RDD Month */ 
UPDATE fact_salesorderlife_so_tmp sof
	 FROM
      dim_documentcategory dc,
      tmp_RDDMonthDrawnQty_2 afsv
   SET sof.ct_AfsAfterRDDUnits = ifnull(afsv.ct_TotalAfsAfterRDD, 0) 
WHERE sof.Dim_DateIdAfsReqDelivery > 1  
       AND dc.dim_documentcategoryid = sof.Dim_DocumentCategoryid
       AND afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
       AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
       AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
       AND afsv.J_3AVBFAE_VBTYP_V = dc.DocumentCategory
       AND dc.DocumentCategory IN ('B','b','G','g');
	     
/* populate the column ct_AfsInRDDUnits - Drawn Units In RDD Month */   
UPDATE fact_salesorderlife_so_tmp sof
	 FROM
      dim_documentcategory dc,
      tmp_RDDMonthDrawnQty_2 afsv
   SET sof.ct_AfsInRDDUnits = ifnull(afsv.ct_TotalAfsInRDD, 0) 
WHERE sof.Dim_DateIdAfsReqDelivery > 1 
        AND dc.dim_documentcategoryid = sof.Dim_DocumentCategoryid
        AND afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
        AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
        AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
        AND afsv.J_3AVBFAE_VBTYP_V = dc.DocumentCategory
        AND dc.DocumentCategory IN ('B','b','G','g')
		and IFNULL(sof.ct_AfsInRDDUnits,-1) <> ifnull(afsv.ct_TotalAfsInRDD, 0);
	   
/* populate the column ct_AfsInRDDUnits - Drawn Units Before RDD Month */   
UPDATE fact_salesorderlife_so_tmp sof
	 FROM
      dim_documentcategory dc,
      tmp_RDDMonthDrawnQty_2 afsv
   SET sof.ct_AfsBeforeRDDUnits = ifnull(afsv.ct_TotalAfsBeforeRDD, 0) 
WHERE sof.Dim_DateIdAfsReqDelivery > 1 
        AND dc.dim_documentcategoryid = sof.Dim_DocumentCategoryid
        AND afsv.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
        AND afsv.J_3AVBFAE_POSNV = sof.dd_SalesItemNo
        AND afsv.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
        AND afsv.J_3AVBFAE_VBTYP_V = dc.DocumentCategory
        AND dc.DocumentCategory IN ('B','b','G','g')
		AND ifnull(sof.ct_AfsBeforeRDDUnits,-1) <> ifnull(afsv.ct_TotalAfsBeforeRDD, 0);
	   
DROP TABLE IF EXISTS tmp_RDDMonthDrawnQty;
DROP TABLE IF EXISTS tmp_RDDMonthDrawnQty_2; 

/* End of Logic for the processing of columns DrawnUnits Before/In/After RDD Month */ 
   
UPDATE fact_salesorderlife_so_tmp sof
SET sof.ct_AfsUnallocatedQty =
      (ifnull(sof.ct_AfsCalculatedOpenQty, 0) - ifnull(sof.ct_AfsAllocatedQty, 0))
WHERE ifnull(sof.ct_AfsUnallocatedQty,-1) <> 
	(ifnull(sof.ct_AfsCalculatedOpenQty, 0) - ifnull(sof.ct_AfsAllocatedQty, 0));

UPDATE fact_salesorderlife_so_tmp sof
      SET  sof.amt_AfsUnallocatedValue =
              ((ifnull(sof.ct_AfsOpenQty, 0) - ifnull(sof.ct_AfsAllocatedQty, 0))
              * amt_AfsNetPrice)
WHERE ifnull(sof.amt_AfsUnallocatedValue,-1) <> 
              ((ifnull(sof.ct_AfsOpenQty, 0) - ifnull(sof.ct_AfsAllocatedQty, 0))
              * amt_AfsNetPrice) ;            

UPDATE    fact_salesorderlife_so_tmp sof
	FROM dim_overallstatusforcreditcheck ocs
      SET sof.amt_CreditHoldValue =
              (CASE
                  WHEN ocs.OverallStatusforCreditCheck = 'B'
                  THEN
                    sof.ct_ConfirmedQty * sof.amt_AfsNetPrice
                  ELSE
                    0
              END)
WHERE  ocs.dim_overallstatusforcreditcheckID =
                sof.Dim_OverallStatusCreditCheckId
		AND ifnull(sof.amt_CreditHoldValue,-1) <> 
              (CASE
                  WHEN ocs.OverallStatusforCreditCheck = 'B'
                  THEN
                    sof.ct_ConfirmedQty * sof.amt_AfsNetPrice
                  ELSE
                    0
              END) ;


UPDATE  fact_salesorderlife_so_tmp sof
    FROM dim_DeliveryBlock db
      SET sof.amt_DeliveryBlockValue =
              (CASE
                  WHEN db.DeliveryBlock <> 'Not Set'
                  THEN
                    sof.ct_ConfirmedQty * sof.amt_AfsNetPrice
                  ELSE
                    0
              END)
WHERE  db.Dim_DeliveryBlockId = sof.Dim_DeliveryblockId 
		AND ifnull(sof.amt_DeliveryBlockValue,-1) <> 
              (CASE
                  WHEN db.DeliveryBlock <> 'Not Set'
                  THEN
                    sof.ct_ConfirmedQty * sof.amt_AfsNetPrice
                  ELSE
                    0
              END) ;

DROP TABLE IF EXISTs tmp_AfsOpenConfirmations_POQty;

CREATE TABLE tmp_AfsOpenConfirmations_POQty AS
Select J_3AVBFAE_VBELV AS SalesDocNo,J_3avbfae_posnv AS SalesItemNo,j_3avbfae_etenv AS ScheduleNo, J_3ABSSI_J_3ABSKZ AS QtyType, ifnull(SUM(J_3ABSSI_MENGE),0) AS L_MENGE,ifnull(SUM(J_3ABSSI_DABMG),0) AS L_DABMG, cast(0 AS decimal(18,4)) AS B_MENGE,cast(0 AS decimal(18,4)) AS B_DABMG 
FROM J_3ABSSI, J_3AVBFAE,fact_salesorderlife_so_tmp sof
where J_3ABSSI_J_3ABSKZ in ( 'L' )
and J_3ABSSI_J_3ABSNR = J_3AVBFAE_VBELN
and J_3abssi_J_3ahbsp = j_3avbfae_posnn
and j_3abssi_j_3aebsp = j_3avbfae_etenn
and J_3AVBFAE_VBTYP_N = 'V'
and J_3AVBFAE_VBELV= sof.dd_SalesDocNo 
and J_3avbfae_posnv = sof.dd_Salesitemno
and j_3avbfae_etenv = sof.dd_Scheduleno
GROUP BY J_3AVBFAE_VBELV,J_3avbfae_posnv,j_3avbfae_etenv,J_3ABSSI_J_3ABSKZ
UNION ALL
Select J_3AVBFAE_VBELV AS SalesDocNo,J_3avbfae_posnv AS SalesItemNo,j_3avbfae_etenv AS ScheduleNo, J_3ABSSI_J_3ABSKZ AS QtyType, cast(0 AS decimal(18,4)) AS L_MENGE,cast(0 AS decimal(18,4)) AS L_DABMG, ifnull(SUM(J_3ABSSI_MENGE),0) AS B_MENGE,ifnull(SUM(J_3ABSSI_DABMG),0) AS B_DABMG 
FROM J_3ABSSI, J_3AVBFAE,fact_salesorderlife_so_tmp sof
where J_3ABSSI_J_3ABSKZ in ( 'B' )
and J_3ABSSI_J_3ABSNR = J_3AVBFAE_VBELN
and J_3abssi_J_3ahbsp = j_3avbfae_posnn
and j_3abssi_j_3aebsp = j_3avbfae_etenn
and J_3AVBFAE_VBTYP_N = 'V'
and J_3AVBFAE_VBELV= sof.dd_SalesDocNo 
and J_3avbfae_posnv = sof.dd_Salesitemno
and j_3avbfae_etenv = sof.dd_Scheduleno
GROUP BY J_3AVBFAE_VBELV,J_3avbfae_posnv,j_3avbfae_etenv,J_3ABSSI_J_3ABSKZ;

UPDATE fact_salesorderlife_so_tmp  sof
	FROM tmp_AfsOpenConfirmations_POQty t
		SET ct_AfsOpenPOQty = ( CASE WHEN  t.L_MENGE > 0 THEN (B_MENGE+B_DABMG) ELSE  B_MENGE END)  
where sof.dd_Salesdocno = t.SalesDocNo
		and sof.dd_SalesItemNo = t.SalesItemNo
		and sof.dd_ScheduleNo = t.ScheduleNo
		and t.qtytype = 'B';

UPDATE fact_salesorderlife_so_tmp  sof
	FROM tmp_AfsOpenConfirmations_POQty t
		SET ct_AfsInTransitQty =  t.L_MENGE  
where sof.dd_Salesdocno = t.SalesDocNo
		and sof.dd_SalesItemNo = t.SalesItemNo
		and sof.dd_ScheduleNo = t.ScheduleNo
		and t.qtytype = 'L';

DROP TABLE IF EXISTs tmp_AfsOpenConfirmations_POQty;
                
call vectorwise(combine 'fact_salesorderlife_so_tmp');
    DROP TABLE IF EXISTS tmp_CustomerPOsCancelDate;

    CREATE TABLE tmp_CustomerPOsCancelDate AS 
    SELECT sof.dd_CustomerPONo, min(dt.DateValue) AS EarliestCancelDate FROM fact_salesorderlife_so_tmp sof
    INNER JOIN dim_Date dt ON dt.dim_dateid = sof.Dim_DateIdAfsCancelDate
    WHERE sof.dd_CustomerPONo <> 'Not Set' AND sof.Dim_DateIdAfsCancelDate <> 1 GROUP BY sof.dd_CustomerPONo;


UPDATE fact_salesorderlife_so_tmp sof
	FROM dim_Date edt,
          dim_plant pl,
          tmp_CustomerPOsCancelDate po
		SET sof.Dim_DateIdEarliestSOCancelDate = edt.Dim_Dateid
    WHERE     pl.Dim_Plantid = sof.Dim_Plantid
			AND edt.companycode = pl.companycode
			AND edt.DateValue = po.EarliestCancelDate
			AND sof.dd_CustomerPONo = po.dd_CustomerPONo
			AND sof.dd_CustomerPONo <> 'Not Set'
			AND sof.Dim_DateIdEarliestSOCancelDate <> edt.Dim_Dateid;

/* LK : 29 Aug changes - amt_Subtotal3inCustConfig_Billing */


DROP TABLE IF EXISTS tmp_CustomerPOsCancelDate;

UPDATE fact_salesorderlife_so_tmp sof
   FROM dim_overallstatusforcreditcheck os
		SET sof.dd_SalesOrderBlocked = 'X'
WHERE sof.Dim_OverallStatusCreditCheckId = os.dim_overallstatusforcreditcheckid
		AND ( sof.Dim_DeliveryBlockId <> 1 OR sof.Dim_BillingBlockid <> 1 
		OR os.overallstatusforcreditcheck = 'B');

UPDATE fact_salesorderlife_so_tmp sof
SET sof.dd_SalesOrderBlocked = 'Not Set'
WHERE sof.dd_SalesOrderBlocked IS NULL;


Drop table if exists deletepart_702;

DROP TABLE IF EXISTS VBAK_VBAP_VBEP_useinsub;
DROP TABLE IF EXISTS VBAK_VBAP_useinsub;

CREATE TABLE VBAK_VBAP_VBEP_useinsub AS Select VBAK_VBELN,VBAP_POSNR,VBEP_ETENR FROM VBAK_VBAP_VBEP;
CREATE TABLE VBAK_VBAP_useinsub AS SELECT VBAP_VBELN,VBAP_POSNR FROM VBAK_VBAP;

call vectorwise(combine 'VBAK_VBAP_VBEP_useinsub');
call vectorwise(combine 'VBAK_VBAP_useinsub');


DROP TABLE IF EXISTS deletepart_702;
CREATE TABLE deletepart_702
AS
Select *  FROM fact_salesorderlife_so_tmp
  WHERE dd_ScheduleNo = 0
        AND EXISTS
                (SELECT 1 FROM VBAK_VBAP_useinsub WHERE VBAP_VBELN = dd_SalesDocNo)
        AND NOT EXISTS
                    (SELECT 1 FROM VBAK_VBAP_useinsub WHERE VBAP_VBELN = dd_SalesDocNo AND VBAP_POSNR = dd_SalesItemNo);

call vectorwise(combine 'deletepart_702');
call vectorwise ( combine 'fact_salesorderlife_so_tmp-deletepart_702');
call vectorwise(combine 'fact_salesorderlife_so_tmp');

/* Need to check below condition with Shanthi */
DROP Table if exists tmp_MinSalesSchedules_0;
CREATE TABLE tmp_MinSalesSchedules_0
AS 
SELECT dd_SalesDocNo,dd_SalesItemNo,min(dd_ScheduleNo) AS MinScheduleNo
	FROM fact_salesorderlife_so_tmp
where dd_ScheduleNo <> 0
GROUP BY dd_SalesDocNo,dd_SalesItemNo;

DROP TABLE IF EXISTS deletepart_702;
CREATE TABLE deletepart_702
AS
Select *  FROM fact_salesorderlife_so_tmp f
  WHERE dd_ScheduleNo = 0
        AND EXISTS
                (SELECT 1 FROM tmp_MinSalesSchedules_0 t WHERE t.dd_SalesDocNo = f.dd_SalesDocNo
                  AND t.dd_SalesItemNo = f.dd_SalesItemNo);

call vectorwise(combine 'deletepart_702');
call vectorwise ( combine 'fact_salesorderlife_so_tmp-deletepart_702');
call vectorwise(combine 'fact_salesorderlife_so_tmp');

UPDATE fact_salesorderlife_so_tmp sof
  FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm
   SET sof.dim_Customermastersalesid = cms.dim_Customermastersalesid
WHERE     sof.dim_salesorgid = sorg.dim_salesorgid
       AND sof.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = sof.dim_salesdivisionid
       AND cm.dim_customerid = sof.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber;

call vectorwise(combine 'fact_salesorderlife_so_tmp');

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk
		SET dd_SOCreateTime = ifnull(VBAK_ERZET, '000000'),
dd_ReqDeliveryTime = ifnull(VBAK_VZEIT, '000000'),
dd_SOLineCreateTime = ifnull(VBAP_ERZET, '000000'),
dd_DeliveryTime = ifnull(VBEP_EZEIT, '000000'),
dd_PlannedGITime = ifnull(VBEP_WAUHR, '000000')
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
             AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
             AND vbk.VBEP_ETENR = sof.dd_ScheduleNo;

UPDATE fact_salesorderlife_so_tmp sof
	SET dd_SOCreateTime = '000000',
		dd_ReqDeliveryTime = '000000',
		dd_SOLineCreateTime = '000000',
		dd_DeliveryTime = '000000',
		dd_PlannedGITime = '000000'
WHERE dd_SOCreateTime IS NULL OR dd_ReqDeliveryTime IS NULL
		OR dd_SOLineCreateTime IS NULL OR dd_DeliveryTime IS NULL
		OR dd_PlannedGITime IS NULL;

call vectorwise(combine 'fact_salesorderlife_so_tmp');

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk
		SET dd_HighLevelItem = ifnull(VBAP_UEPOS, 0)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
	AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
	AND vbk.VBEP_ETENR = sof.dd_ScheduleNo;

update fact_salesorderlife_so_tmp sof
	FROM J_3AVBFAE a,fact_purchase p
		set dd_PoDocumentNo = ifnull(J_3AVBFAE_VBELN,'Not Set'),
		dd_PoDocumentItemNo = ifnull(J_3AVBFAE_POSNN,0),
		dd_POScheduleNo = ifnull(J_3AVBFAE_ETENN,0)
where a.J_3AVBFAE_VBELV = sof.dd_SalesDocNo
   AND a.J_3AVBFAE_POSNV = sof.dd_SalesItemNo 
   AND a.J_3AVBFAE_ETENV = sof.dd_ScheduleNo
   AND a.J_3AVBFAE_VBTYP_N = 'V'
   and a.J_3AVBFAE_VBELN = p.dd_DocumentNo
   and a.J_3avbfae_posnn = p.dd_DocumentItemno
   and a.J_3avbfae_etenn = p.dd_ScheduleNo;

call vectorwise(combine 'fact_salesorderlife_so_tmp');

UPDATE fact_salesorderlife_so_tmp sof
FROM  fact_productionorder po
	SET sof.dd_ProdOrderNo = ifnull(po.dd_OrderNumber,'Not Set'),
	sof.dd_ProdOrderItemNo = ifnull(po.dd_OrderItemNo,0)
WHERE  sof.dd_SalesDocNo = PO.dd_SalesOrderNo 
		AND sof.dd_SalesItemNo = PO.dd_SalesOrderItemNo 
		AND sof.dd_ScheduleNo = PO.dd_SalesOrderDeliveryScheduleNo;

call vectorwise(combine 'fact_salesorderlife_so_tmp');

UPDATE fact_salesorderlife_so_tmp fso
	FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg
		SET dim_CustomerConditionGroups1id = cg.dim_CustomerConditionGroupsid
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
		AND fso.dd_SalesItemNo = vp.VBKD_POSNR 
		AND VBKD_KDKG1 IS NOT NULL
		AND cg.CustomerCondGrp = VBKD_KDKG1
		AND fso.dim_CustomerConditionGroups1id <> ifnull(cg.dim_CustomerConditionGroupsid,1);

UPDATE fact_salesorderlife_so_tmp fso
	FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg
		SET dim_CustomerConditionGroups2id = cg.dim_CustomerConditionGroupsid
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
		AND fso.dd_SalesItemNo = vp.VBKD_POSNR 
		AND VBKD_KDKG2 IS NOT NULL
		AND cg.CustomerCondGrp = VBKD_KDKG2
		AND fso.dim_CustomerConditionGroups2id <> ifnull(cg.dim_CustomerConditionGroupsid,1);


UPDATE fact_salesorderlife_so_tmp fso
	FROM VBAK_VBAP_VBKD vp,
     dim_CustomerConditionGroups cg
		SET dim_CustomerConditionGroups3id = cg.dim_CustomerConditionGroupsid
where   fso.dd_SalesDocNo = vp.VBKD_VBELN
		AND fso.dd_SalesItemNo = vp.VBKD_POSNR 
		AND VBKD_KDKG3 IS NOT NULL
		AND cg.CustomerCondGrp = VBKD_KDKG3
		AND fso.dim_CustomerConditionGroups3id <> ifnull(cg.dim_CustomerConditionGroupsid,1);


call vectorwise(combine 'fact_salesorderlife_so_tmp');

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk, Dim_DeliveryBlock db
		SET sof.dim_scheduledeliveryblockid =  db.Dim_DeliveryBlockId
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND vbk.VBEP_LIFSP IS NOT NULL
		AND db.DeliveryBlock = vbk.VBEP_LIFSP
		AND db.RowIsCurrent = 1 
		and ifnull(sof.dim_scheduledeliveryblockid,-1) <> ifnull(db.Dim_DeliveryBlockId,-2);

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk
		SET sof.dim_scheduledeliveryblockid =  1
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND vbk.VBEP_LIFSP IS NULL
		and ifnull(sof.dim_scheduledeliveryblockid,-1) <> 1;

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk, Dim_CustomerGroup4 cg4
		set sof.Dim_CustomerGroup4id = ifnull(cg4.Dim_CustomerGroup4id,1)
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
		AND cg4.CustomerGroup = VBAK_KVGR4
		and ifnull(sof.Dim_CustomerGroup4id,-1) <> ifnull(cg4.Dim_CustomerGroup4id,-2);

update fact_salesorderlife_so_tmp 
set dd_clearedblockedsts = 'Cleared';

UPDATE fact_salesorderlife_so_tmp sof
	FROM dim_salesorderheaderstatus sohs,dim_salesorderitemstatus sois
		SET sof.dd_clearedblockedsts = 'Blocked'
WHERE 
	(sohs.GeneralIncompleteStatusItem in ('Not yet processed','Partially processed') 
	OR sohs.OverallBlkdStatus in ('Partially processed','Completely processed') 
	OR sois.GeneralIncompletionStatus in ('Not yet processed','Partially processed'))
	AND sof.dim_salesorderheaderstatusid = sohs.dim_salesorderheaderstatusid
	AND sof.dim_salesorderitemstatusid = sois.dim_salesorderitemstatusid; 

update fact_salesorderlife_so_tmp 
set dd_clearedblockedsts = 'Cleared' 
where dd_clearedblockedsts is null;

call vectorwise(combine 'fact_salesorderlife_so_tmp');

update fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk
		set sof.dd_ConditionNo=ifnull(vbk.VBAK_KNUMV,'Not Set')
where vbk.VBAK_VBELN = sof.dd_SalesDocNo
		and vbk.VBAP_POSNR = sof.dd_SalesItemNo
		and vbk.VBEP_ETENR = sof.dd_ScheduleNo
		and ifnull(sof.dd_ConditionNo,'X') <> ifnull(vbk.VBAK_KNUMV,'Not Set');

UPDATE fact_salesorderlife_so_tmp sof
	FROM vbak_vbap_vbep vbk
		SET sof.amt_DicountAccrualNetPrice =
				(CASE
					WHEN (vbk.VBAP_KBMENG IS NOT NULL OR vbk.VBAP_KBMENG <> 0)
						THEN
							(ifnull(vbk.VBAP_KZWI3, 0) + ifnull(vbk.VBAP_KZWI4, 0))
							/ (case when vbk.VBAP_KBMENG = 0 then (case when vbk.VBAP_KWMENG = 0 then
						null else vbk.VBAP_KWMENG end) else vbk.VBAP_KBMENG end)
      ELSE  0   END)
WHERE     vbk.VBAK_VBELN = sof.dd_SalesDocNo
		AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
		AND vbk.VBEP_ETENR = sof.dd_ScheduleNo;

update  fact_salesorderlife_so_tmp sof
	FROM  VBAK_VBAP_VBEP,
          Dim_Plant pl,
          Dim_Company co
		SET dd_ro_materialdesc = ifnull(VBAP_ARKTX,'Not Set')   
WHERE pl.PlantCode = VBAP_WERKS and co.CompanyCode = pl.CompanyCode
		and VBAK_VBELN = sof.dd_SalesDocNo 
		and VBAP_POSNR = sof.dd_SalesItemNo 
		and VBEP_ETENR = sof.dd_ScheduleNo
		AND dd_ro_materialdesc <> ifnull(VBAP_ARKTX,'Not Set');

call vectorwise(combine 'fact_salesorderlife_so_tmp');

DROP TABLE IF EXISTS deletepart_702;
DROP TABLE IF EXISTS VBAK_VBAP_VBEP_useinsub;
DROP TABLE IF EXISTS VBAK_VBAP_useinsub;

/* call bi_populate_customer_hierarchy */

/* will drop this later */
/* Drop table if exists variable_holder_fact_salesorderlife */
Drop table if exists Dim_CostCenter_702;
Drop table if exists  deletepart_702;
DROP TABLE IF EXISTS VBAK_VBAP_VBEP_702;
DROP TABLE IF EXISTS staging_upd_702;
DROP TABLE IF EXISTS TMP1_INS_AFS_SOF_VBAK_VBAP_VBEP;
DROP TABLE IF EXISTS TMP2_INS_AFS_SOF_VBAK_VBAP_VBEP;

/*End of sales part*/

/*Start of shipments part*/

 /*If pDeltaChangesFlag <> 'true', truncate all rows in fact_salesorderdelivery */
DROP TABLE IF EXISTS tmp_delete_fact_salesorderlife;
CREATE TABLE tmp_delete_fact_salesorderlife
AS
SELECT f.*
  FROM fact_salesorderlife f, systemproperty s
 WHERE s.property = 'process.delta.salesorderlife'
   AND s.property_value <> 'true';

DROP TABLE IF EXISTS fact_salesorderlife_tmp;
CREATE TABLE fact_salesorderlife_tmp
AS
SELECT * FROM fact_salesorderlife;

ALTER TABLE fact_salesorderlife_tmp ADD PRIMARY KEY (fact_salesorderlifeid);
ALTER TABLE fact_salesorderlife_tmp ADD UNIQUE (dd_salesdocno, dd_salesitemno, dd_scheduleno, dd_salesdlvrdocno, dd_salesdlvritemno);

CALL VECTORWISE(COMBINE 'fact_salesorderlife_tmp-tmp_delete_fact_salesorderlife');

DROP TABLE tmp_delete_fact_salesorderlife;

/* LK 30 Mar - This delete would run in any case. If process.delta.salesorderlife was false, f_sof would anyway have 0 rows, otherwise this SQL 
would actually delete rows */
DELETE FROM fact_salesorderlife_tmp
WHERE EXISTS (SELECT 1 FROM CDPOS_LIKP a WHERE a.CDPOS_OBJECTID = dd_SalesDlvrDocNo AND a.CDPOS_CHNGIND = 'D' AND a.CDPOS_TABNAME = 'LIKP');

INSERT INTO variable_tbl_721( pDeltaChangesFlag )
SELECT ifnull(property_value,'true') FROM systemproperty
 WHERE  property = 'process.delta.salesorderlife';

CALL VECTORWISE(COMBINE 'variable_tbl_721');

DROP TABLE IF EXISTS delete_staging_721;

/* LK 26 Aug: Slow query. Tuned now. */

DROP TABLE IF EXISTS tmp_delstg_fact_salesorderlife_ins;
CREATE TABLE tmp_delstg_fact_salesorderlife_ins
AS
SELECT DISTINCT dd_SalesDlvrDocNo,dd_SalesDlvrItemNo
  FROM fact_salesorderlife_tmp,LIKP_LIPS 
 WHERE dd_SalesDlvrDocNo = LIKP_VBELN;

DROP TABLE IF EXISTS tmp_delstg_fact_salesorderlife_del;
CREATE TABLE tmp_delstg_fact_salesorderlife_del
AS
SELECT DISTINCT LIKP_VBELN dd_SalesDlvrDocNo,LIPS_POSNR dd_SalesDlvrItemNo
 FROM LIKP_LIPS;

/* Obtain only the SOs and SO items which are in both sof and LIKP_LIPS*/
CALL VECTORWISE(COMBINE 'tmp_delstg_fact_salesorderlife_ins-tmp_delstg_fact_salesorderlife_del');

CREATE TABLE delete_staging_721 AS
SELECT DISTINCT sod.* FROM fact_salesorderlife_tmp sod,variable_tbl_721,tmp_delstg_fact_salesorderlife_ins ins
 WHERE sod.dd_SalesDlvrDocNo = ins.dd_SalesDlvrDocNo 
   AND sod.dd_SalesDlvrItemNo = ins.dd_SalesDlvrItemNo
   AND pDeltaChangesFlag='true';
   
CALL VECTORWISE(COMBINE 'delete_staging_721');

/* Remove the SOs which are in LIKP_LIPS*/
CALL VECTORWISE (COMBINE 'fact_salesorderlife_tmp-delete_staging_721');
DROP TABLE IF EXISTS delete_staging_721;

CREATE TABLE delete_staging_721 AS
SELECT DISTINCT fact_salesorderlife_tmp.* 
  FROM fact_salesorderlife_tmp,variable_tbl_721,LIKP_LIPS a
 WHERE dd_SalesDlvrDocNo = LIKP_VBELN AND dd_SalesDlvrItemNo = LIPS_POSNR
   AND pDeltaChangesFlag='true';
CALL VECTORWISE(COMBINE 'delete_staging_721');

call vectorwise (combine 'fact_salesorderlife_tmp-delete_staging_721');

DROP TABLE IF EXISTS delete_staging_721;

DROP TABLE IF EXISTS tmp_delstg721_fso_ins;
CREATE TABLE tmp_delstg721_fso_ins
AS
SELECT DISTINCT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
FROM fact_salesorderlife_tmp;

DROP TABLE IF EXISTS tmp_delstg721_fso_del;
CREATE TABLE tmp_delstg721_fso_del
AS
SELECT DISTINCT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
FROM fact_salesorderlife_so_tmp;

CALL VECTORWISE(COMBINE 'tmp_delstg721_fso_ins-tmp_delstg721_fso_del');

CREATE TABLE delete_staging_721 AS
SELECT DISTINCT sof.* 
  FROM fact_salesorderlife_tmp sof,variable_tbl_721,tmp_delstg721_fso_ins f
 WHERE sof.dd_SalesDocNo = f.dd_SalesDocNo 
   AND sof.dd_SalesItemNo = f.dd_SalesItemNo
   AND sof.dd_ScheduleNo = f.dd_ScheduleNo
   AND pDeltaChangesFlag='true';
							
call vectorwise (combine 'fact_salesorderlife_tmp-delete_staging_721');
DROP TABLE IF EXISTS delete_staging_721;

CREATE TABLE delete_staging_721 AS
SELECT sof.* 
  FROM fact_salesorderlife_tmp sof,variable_tbl_721
 WHERE dd_SalesDlvrDocNo = 'Not Set'
   AND pDeltaChangesFlag='true';
   
CALL VECTORWISE(COMBINE 'delete_staging_721');

CALL VECTORWISE(COMBINE 'fact_salesorderlife_tmp-delete_staging_721');
DROP TABLE IF EXISTS delete_staging_721;
 
CREATE TABLE delete_staging_721 AS
SELECT sof.* 
  FROM fact_salesorderlife_tmp sof,variable_tbl_721
 WHERE pDeltaChangesFlag<>'true';

CALL VECTORWISE(COMBINE 'fact_salesorderlife_tmp-delete_staging_721');

DROP TABLE IF EXISTS delete_staging_721;
  
DROP TABLE IF EXISTS max_holder_fact_salesorderlife;
CREATE TABLE max_holder_fact_salesorderlife(maxid)
AS
SELECT ifnull(max(fact_salesorderlifeid),0)
FROM fact_salesorderlife_tmp;

CALL VECTORWISE(COMBINE 'max_holder_fact_salesorderlife');
CALL VECTORWISE(COMBINE 'likp_lips');

SELECT 'Insert 1 on tmp_fact_salesorderdelivery_intermediate_populate',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS LIKP_LIPS_sub00;
CREATE TABLE LIKP_LIPS_sub00 AS SELECT * FROM  LIKP_LIPS WHERE LIPS_LFIMG  > 0 ;

INSERT INTO LIKP_LIPS_sub00
SELECT l.* 
  FROM likp_lips l 
 INNER JOIN fact_salesorderlife_tmp f
    ON l.likp_vbeln = f.dd_Salesdlvrdocno 
   AND l.lips_posnr = f.dd_Salesdlvritemno
 WHERE l.lips_lfimg = 0 AND f.ct_Qtydelivered <> 0;

INSERT INTO fact_salesorderlife_tmp
(fact_salesorderlifeid
,amt_afsnetprice
,amt_afsnetvalue
,amt_baseprice
,amt_customerexpectedprice
,amt_dicountaccrualnetprice
,amt_exchangerate
,amt_exchangerate_gbl
,amt_exchangerate_stat
,amt_scheduletotal
,amt_stdprice
,amt_unitprice
,amt_unitpriceuom
,ct_afsallocatedqty
,ct_afscalculatedopenqty
,ct_afsintransitqty
,ct_afsopenpoqty
,ct_afsopenqty
,ct_afstotaldrawn
,ct_afsunallocatedqty
,ct_confirmedqty
,ct_priceunit
,ct_scheduleqtysalesunit
,dd_afsallocationgroupno
,dd_afsdepartment
,dd_afsstockcategory
,dd_afsstocktype
,dd_arunnumber
,dd_billing_no
,dd_businesscustomerpono
,dd_customerpono
,dd_deliverytime
,dd_idocnumber
,dd_openconfirmationsexists
,dd_referencedocumentno
,dd_referencedocitemno
,dd_releaserule
,dd_salesdocno
,dd_salesitemno
,dd_scheduleno
,dd_subsdocitemno
,dd_subsequentdocno
,dd_subsscheduleno
,dd_SeasonCode 
,dim_accountassignmentgroupid
,dim_afsrejectionreasonid
,dim_afsseasonid 
,dim_afssizeid 
,dim_availabilitycheckid
,dim_billingblockid
,dim_billtopartypartnerfunctionid
,dim_companyid
,dim_controllingareaid
,dim_costcenterid
,dim_creditrepresentativeid
,dim_currencyid
,dim_currencyid_gbl
,dim_currencyid_stat
,dim_currencyid_tra
,dim_customergroup1id
,dim_customergroup2id
,dim_customergroup4id
,dim_customerid
,dim_customeridshipto
,dim_customermastersalesid
,dim_custompartnerfunctionid
,dim_custompartnerfunctionid1
,dim_custompartnerfunctionid2
,dim_dateidactualgi_original
,dim_dateidafscanceldate
,dim_dateidafsreqdelivery
,dim_dateidarposting
,dim_dateidaslastdate
,dim_dateiddlvrdoccreated
,dim_dateidearliestsocanceldate
,dim_dateidfirstdate
,dim_dateidguaranteedate
,dim_dateidpurchaseorder
,dim_dateidquotationvalidfrom
,dim_dateidquotationvalidto
,dim_dateidrejection
,dim_dateidsalesordercreated
,dim_dateidscheddelivery
,dim_dateidscheddeliveryreq
,dim_dateidscheddlvrreqprev
,dim_dateidsocreated
,dim_dateidsodocument
,dim_dateidsoitemchangedon
,dim_deliveryblockid
,dim_deliverypriorityid
,dim_distributionchannelid
,dim_documentcategoryid
,dim_incotermid
,dim_materialgroupid
,dim_materialpricegroup1id
,dim_materialpricegroup4id
,dim_materialpricegroup5id
,dim_overallstatuscreditcheckid
,dim_partid
,dim_globalpartid
,dim_partsalesid
,dim_payerpartnerfunctionid
,dim_plantid
,dim_producthierarchyid
,dim_profitcenterid
,dim_projectsourceid
,dim_purchaseordertypeid
,dim_routeid
,dim_salesdistrictid
,dim_salesdivisionid
,dim_salesdocorderreasonid
,dim_salesdocumenttypeid
,dim_salesgroupid
,dim_salesofficeid
,dim_salesorderheaderstatusid
,dim_salesorderitemcategoryid
,dim_salesorderitemstatusid
,dim_salesorderpartnerid
,dim_salesorderrejectreasonid
,dim_salesorgid
,dim_scheduledeliveryblockid
,dim_schedulelinecategoryid
,dim_shippingconditionid
,dim_shipreceivepointid
,dim_storagelocationid
,dim_subsdoccategoryid
,dim_transactiongroupid

,amt_afsondeliveryvalue
,amt_afsunallocatedvalue
,amt_creditholdvalue
,amt_deliveryblockvalue
,amt_incomplete
,amt_netvalue
,amt_stdcost
,amt_subtotal1
,amt_subtotal2
,amt_subtotal3
,amt_subtotal3_orderqty
,amt_subtotal3incustconfig_billing
,amt_subtotal4
,amt_subtotal5
,amt_subtotal6
,amt_targetvalue
,amt_tax
,checksum1
,ct_afsafterrddunits
,ct_FixedQty
,ct_ReservedQty
,ct_afsbeforerddunits
,ct_afsinrddunits
,ct_afsondeliveryqty
,ct_cmlqtyreceived
,ct_correctedqty
,ct_cumconfirmedqty
,ct_cumorderqty
,ct_fillqty
,ct_fillqty_crd
,ct_fillqty_pdd
,ct_incompleteqty
,ct_onhandcoveredqty
,ct_onhandqty
,ct_overdlvrtolerance
,ct_shippedagnstorderqty
,ct_targetqty
,ct_underdlvrtolerance
,dd_afsdeptdesc
,dd_afsmrpstatus
,dd_asrname
,dd_batchno
,dd_clearedblockedsts
,dd_conditionno
,dd_createdby
,dd_creditlimit
,dd_creditmgr
,dd_creditrep
,dd_customermaterialno
,dd_deliveryindicator
,dd_documentconditionno
,dd_hierarchytype
,dd_highlevelitem
,dd_internationalarticleno
,dd_itemrelfordelv
,dd_packholdflag
,dd_plannedgitime
,dd_podocumentitemno
,dd_podocumentno
,dd_poscheduleno
,dd_prodorderitemno
,dd_prodorderno
,dd_purchaseorderitem
,dd_purchasereq
,dd_purchaserequisition
,dd_purchaserequisitionitemno
,dd_reqdeliverytime
,dd_requirementtype
,dd_salesorderblocked
,dd_socreatetime
,dd_soldtohierarchylevel
,dd_solinecreatetime
,dim_additionalcustomermaterialid
,dim_additionalmaterialid
,dim_agreementsid
,dim_baseuomid
,dim_billingdateid
,dim_chargebackcontractid
,dim_custmasteradditionalattrid
,dim_customerconditiongroups1id
,dim_customerconditiongroups2id
,dim_customerconditiongroups3id
,dim_customerconditiongroups4id
,dim_customerconditiongroups5id
,dim_customergroupid
,dim_customeridcbowner
,dim_customerpaymenttermsid
,dim_customerriskcategoryid
,dim_dateidactualgifill
,dim_dateidbilling
,dim_dateidbillingcreated
,dim_dateiddeliverytime
,dim_dateidfixedvalue
,dim_dateidgoodsissue
,dim_dateidlatestcustpoagi
,dim_dateidloading
,dim_dateidloadingfill
,Dim_DateidMatlAvailOriginal
,dim_dateidnetduedate
,dim_dateidnextdate
,dim_dateidshipdlvrfill
,dim_dateidshipmentdelivery
,dim_dateidtransport
,dim_dateidvalidfrom
,dim_dateidvalidto
,dim_h1customerid
,dim_highercustomerid
,dim_higherdistchannelid
,dim_highersalesdivid
,dim_highersalesorgid
,dim_materialpricegroup2id
,dim_orgid
,dim_paymenttermsid
,dim_salesmiscid
,dim_salesriskcategoryid
,dim_salesuomid
,dim_toplevelcustomerid
,dim_topleveldistchannelid
,dim_toplevelsalesdivid
,dim_toplevelsalesorgid
,dim_unitofmeasureid
,amt_salessubtotal3unitprice
,amt_cost
,amt_cost_doccurr
,ct_afsbilledqty
,ct_afsdeliveredqty
,ct_fixedprocessdays
,ct_qtydelivered
,ct_shipprocessdays
,dd_billingitem_no
,dd_gitime
,dd_movementtype
,dd_pickingtime
,dd_salesdlvrdocno
,dd_salesdlvritemno
,dd_sdcreatetime
,dd_sdlinecreatetime
,dd_universalproductcode
,dim_afsmrpstatusid
,dim_billingdocumenttypeid
,dim_dateidactualgoodsissue
,dim_dateidbillingdate
,dim_dateiddeliverydate
,dim_dateidloadingdate
,dim_dateidmatlavail
,dim_dateidpickingdate
,dim_dateidplannedgoodsissue
,dim_deliveryheaderstatusid
,dim_deliveryitemstatusid
,dim_deliverytypeid
,amt_ATPStock
)
SELECT 
m.maxid + row_number() over(order by f.dd_SalesDocNo,f.dd_SalesItemNo,f.dd_ScheduleNo) fact_salesorderdeliveryid
,ifnull(f.amt_afsnetprice,0) amt_afsnetprice
,ifnull(f.amt_afsnetvalue,0) amt_afsnetvalue
,ifnull(f.amt_baseprice,0) amt_baseprice
,ifnull(f.amt_customerexpectedprice,0) amt_customerexpectedprice
,ifnull(f.amt_dicountaccrualnetprice,0) amt_dicountaccrualnetprice
,ifnull(f.amt_exchangerate,0) amt_exchangerate
,ifnull(f.amt_exchangerate_gbl,0) amt_exchangerate_gbl
,ifnull(f.amt_exchangerate_stat,0) amt_exchangerate_stat
,ifnull(f.amt_scheduletotal,0) amt_scheduletotal
,ifnull(f.amt_stdprice,0) amt_stdprice
,ifnull(f.amt_unitprice,0) amt_unitprice
,ifnull(f.amt_unitpriceuom,0) amt_unitpriceuom
,ifnull(f.ct_afsallocatedqty,0) ct_afsallocatedqty
,ifnull(f.ct_afscalculatedopenqty,0) ct_afscalculatedopenqty
,ifnull(f.ct_afsintransitqty,0) ct_afsintransitqty
,ifnull(f.ct_afsopenpoqty,0) ct_afsopenpoqty
,0 ct_afsopenqty
,ifnull(f.ct_afstotaldrawn,0) ct_afstotaldrawn
,ifnull(f.ct_afsunallocatedqty,0) ct_afsunallocatedqty
,(CASE WHEN (f.ct_ConfirmedQty - LIPS_LFIMG >= 0 ) THEN  LIPS_LFIMG ELSE f.ct_ConfirmedQty END ) ct_ConfirmedQty
,ifnull(f.ct_priceunit,0) ct_priceunit
,(CASE WHEN (f.ct_ScheduleQtySalesUnit - LIPS_LFIMG >= 0 ) THEN  LIPS_LFIMG ELSE f.ct_ScheduleQtySalesUnit END )ct_ScheduleQtySalesUnit         
,ifnull(f.dd_afsallocationgroupno, 'Not Set') dd_afsallocationgroupno
,ifnull(f.dd_afsdepartment, 'Not Set') dd_afsdepartment
,ifnull(f.dd_afsstockcategory, 'Not Set') dd_afsstockcategory
,ifnull(f.dd_afsstocktype, 'Not Set') dd_afsstocktype
,ifnull(f.dd_arunnumber, 'Not Set') dd_arunnumber
,ifnull(f.dd_billing_no, 'Not Set') dd_billing_no
,ifnull(f.dd_businesscustomerpono, 'Not Set') dd_businesscustomerpono
,ifnull(f.dd_customerpono, 'Not Set') dd_customerpono
,ifnull(f.dd_deliverytime, 'Not Set') dd_deliverytime
,ifnull(f.dd_idocnumber, 'Not Set') dd_idocnumber
,ifnull(f.dd_openconfirmationsexists,'Not Set') dd_openconfirmationsexists
,ifnull(f.dd_referencedocumentno, 'Not Set') dd_referencedocumentno
,ifnull(f.dd_referencedocitemno,0) dd_referencedocitemno
,ifnull(f.dd_releaserule, 'Not Set') dd_releaserule
,ifnull(f.dd_salesdocno, 'Not Set') dd_salesdocno
,ifnull(f.dd_salesitemno,0) dd_salesitemno
,ifnull(f.dd_scheduleno,0) dd_scheduleno
,ifnull(f.dd_subsdocitemno,0) dd_subsdocitemno
,ifnull(f.dd_subsequentdocno,'Not Set') dd_subsequentdocno
,ifnull(f.dd_subsscheduleno,0) dd_subsscheduleno
,ifnull(f.dd_SeasonCode, 'Not Set') dd_SeasonCode 
,ifnull(f.dim_accountassignmentgroupid,1) dim_accountassignmentgroupid
,ifnull(f.dim_afsrejectionreasonid,1) dim_afsrejectionreasonid
,ifnull(f.dim_afsseasonid,1) dim_afsseasonid 
,ifnull(f.dim_afssizeid,1) dim_afssizeid 
,ifnull(f.dim_availabilitycheckid,1) dim_availabilitycheckid
,ifnull(f.dim_billingblockid,1) dim_billingblockid
,ifnull(f.dim_billtopartypartnerfunctionid,1) dim_billtopartypartnerfunctionid
,ifnull(f.dim_companyid,1) dim_companyid
,ifnull(f.dim_controllingareaid,1) dim_controllingareaid
,ifnull(f.dim_costcenterid,1) dim_costcenterid
,ifnull(f.dim_creditrepresentativeid,1) dim_creditrepresentativeid
,ifnull(f.dim_currencyid,1) dim_currencyid
,ifnull(f.dim_currencyid_gbl,1) dim_currencyid_gbl
,ifnull(f.dim_currencyid_stat,1) dim_currencyid_stat
,ifnull(f.dim_currencyid_tra,1) dim_currencyid_tra
,ifnull(f.dim_customergroup1id,1) dim_customergroup1id
,ifnull(f.dim_customergroup2id,1) dim_customergroup2id
,ifnull(f.dim_customergroup4id,1) dim_customergroup4id
,ifnull(f.Dim_CustomerID,1) Dim_CustomeridSoldTo
,ifnull(f.dim_customeridshipto,1) dim_customeridshipto
,ifnull(f.dim_customermastersalesid,1) dim_customermastersalesid
,ifnull(f.dim_custompartnerfunctionid,1) dim_custompartnerfunctionid
,ifnull(f.dim_custompartnerfunctionid1,1) dim_custompartnerfunctionid1
,ifnull(f.dim_custompartnerfunctionid2,1) dim_custompartnerfunctionid2
,1 dim_dateidactualgi_original
,ifnull(f.dim_dateidafscanceldate,1) dim_dateidafscanceldate
,ifnull(f.dim_dateidafsreqdelivery,1) dim_dateidafsreqdelivery
,ifnull(f.dim_dateidarposting, 1) dim_dateidarposting
,ifnull(f.dim_dateidaslastdate,1) dim_dateidaslastdate
,1 dim_dateiddlvrdoccreated
,ifnull(f.dim_dateidearliestsocanceldate,1) dim_dateidearliestsocanceldate
,ifnull(f.dim_dateidfirstdate,1) dim_dateidfirstdate
,ifnull(f.dim_dateidguaranteedate,1) dim_dateidguaranteedate
,ifnull(f.dim_dateidpurchaseorder,1) dim_dateidpurchaseorder
,ifnull(f.dim_dateidquotationvalidfrom,1) dim_dateidquotationvalidfrom
,ifnull(f.dim_dateidquotationvalidto,1) dim_dateidquotationvalidto
,ifnull(f.dim_dateidrejection,1) dim_dateidrejection
,ifnull(f.dim_dateidsalesordercreated,1) dim_dateidsalesordercreated
,ifnull(f.dim_dateidscheddelivery,1) dim_dateidscheddelivery
,ifnull(f.dim_dateidscheddeliveryreq,1) dim_dateidscheddeliveryreq
,ifnull(f.dim_dateidscheddlvrreqprev,1) dim_dateidscheddlvrreqprev
,ifnull(f.dim_dateidsocreated,1) dim_dateidsocreated
,ifnull(f.dim_dateidsodocument,1) dim_dateidsodocument
,ifnull(f.dim_dateidsoitemchangedon,1) dim_dateidsoitemchangedon
,ifnull(f.dim_deliveryblockid,1) dim_deliveryblockid
,ifnull(f.dim_deliverypriorityid,1) dim_deliverypriorityid
,ifnull(f.dim_distributionchannelid,1) dim_distributionchannelid
,ifnull(f.dim_documentcategoryid,1) dim_documentcategoryid
,ifnull(f.dim_incotermid,1) dim_incotermid
,ifnull(f.dim_materialgroupid,1) dim_materialgroupid
,ifnull(f.dim_materialpricegroup1id,1) dim_materialpricegroup1id
,ifnull(f.dim_materialpricegroup4id,1) dim_materialpricegroup4id
,ifnull(f.dim_materialpricegroup5id,1) dim_materialpricegroup5id
,ifnull(f.dim_overallstatuscreditcheckid,1) dim_overallstatuscreditcheckid
,ifnull(f.dim_partid,1) dim_partid
,ifnull(f.dim_globalpartid,1) dim_globalpartid
,ifnull(f.dim_partsalesid,1) dim_partsalesid
,ifnull(f.dim_payerpartnerfunctionid,1) dim_payerpartnerfunctionid
,ifnull(f.dim_plantid,1) dim_plantid
,ifnull(f.dim_producthierarchyid,1) dim_producthierarchyid
,ifnull(f.dim_profitcenterid,1) dim_profitcenterid
,ifnull(f.dim_projectsourceid,1) dim_projectsourceid
,ifnull(f.dim_purchaseordertypeid,1) dim_purchaseordertypeid
,ifnull(f.dim_routeid,1) dim_routeid
,ifnull(f.dim_salesdistrictid,1) dim_salesdistrictid
,ifnull(f.dim_salesdivisionid,1) dim_salesdivisionid
,ifnull(f.dim_salesdocorderreasonid,1) dim_salesdocorderreasonid
,ifnull(f.dim_salesdocumenttypeid,1) dim_salesdocumenttypeid
,ifnull(f.dim_salesgroupid,1) dim_salesgroupid
,ifnull(f.dim_salesofficeid,1) dim_salesofficeid
,ifnull(f.dim_salesorderheaderstatusid,1) dim_salesorderheaderstatusid
,ifnull(f.dim_salesorderitemcategoryid,1) dim_salesorderitemcategoryid
,ifnull(f.dim_salesorderitemstatusid,1) dim_salesorderitemstatusid
,ifnull(f.dim_salesorderpartnerid,1) dim_salesorderpartnerid
,ifnull(f.dim_salesorderrejectreasonid,1) dim_salesorderrejectreasonid
,ifnull(f.dim_salesorgid,1) dim_salesorgid
,ifnull(f.dim_scheduledeliveryblockid,1) dim_scheduledeliveryblockid
,ifnull(f.dim_schedulelinecategoryid,1) dim_schedulelinecategoryid
,ifnull(f.dim_shippingconditionid,1) dim_shippingconditionid
,ifnull(f.dim_shipreceivepointid,1) dim_shipreceivepointid
,ifnull(f.dim_storagelocationid,1) dim_storagelocationid
,ifnull(f.dim_subsdoccategoryid,1) dim_subsdoccategoryid
,ifnull(f.dim_transactiongroupid,1) dim_transactiongroupid

,ifnull(f.amt_afsondeliveryvalue,0) amt_afsondeliveryvalue
,ifnull(f.amt_afsunallocatedvalue,0) amt_afsunallocatedvalue
,ifnull(f.amt_creditholdvalue,0) amt_creditholdvalue
,ifnull(f.amt_deliveryblockvalue,0) amt_deliveryblockvalue
,ifnull(f.amt_incomplete,0) amt_incomplete
,ifnull(f.amt_netvalue,0) amt_netvalue
,ifnull(f.amt_stdcost,0) amt_stdcost
,ifnull(f.amt_subtotal1,0) amt_subtotal1
,ifnull(f.amt_subtotal2,0) amt_subtotal2
,ifnull(f.amt_subtotal3,0) amt_subtotal3
,ifnull(f.amt_subtotal3_orderqty,0) amt_subtotal3_orderqty
,ifnull(f.amt_subtotal3incustconfig_billing,0) amt_subtotal3incustconfig_billing
,ifnull(f.amt_subtotal4,0) amt_subtotal4
,ifnull(f.amt_subtotal5,0) amt_subtotal5
,ifnull(f.amt_subtotal6,0) amt_subtotal6
,ifnull(f.amt_targetvalue,0) amt_targetvalue
,ifnull(f.amt_tax,0) amt_tax
,ifnull(f.checksum1,0) checksum1
,ifnull(f.ct_afsafterrddunits,0) ct_afsafterrddunits
,ifnull(f.ct_AfsAllocationFQty,0) ct_FixedQty
,ifnull(f.ct_AfsAllocationRQty,0) ct_ReservedQty
,ifnull(f.ct_afsbeforerddunits,0) ct_afsbeforerddunits
,ifnull(f.ct_afsinrddunits,0) ct_afsinrddunits
,ifnull(f.ct_afsondeliveryqty,0) ct_afsondeliveryqty
,ifnull(f.ct_cmlqtyreceived,0) ct_cmlqtyreceived
,ifnull(f.ct_correctedqty,0) ct_correctedqty
,ifnull(f.ct_cumconfirmedqty,0) ct_cumconfirmedqty
,ifnull(f.ct_cumorderqty,0) ct_cumorderqty
,ifnull(f.ct_fillqty,0) ct_fillqty
,ifnull(f.ct_fillqty_crd,0) ct_fillqty_crd
,ifnull(f.ct_fillqty_pdd,0) ct_fillqty_pdd
,ifnull(f.ct_incompleteqty,0) ct_incompleteqty
,ifnull(f.ct_onhandcoveredqty,0) ct_onhandcoveredqty
,ifnull(f.ct_onhandqty,0) ct_onhandqty
,ifnull(f.ct_overdlvrtolerance,0) ct_overdlvrtolerance
,ifnull(f.ct_shippedagnstorderqty,0) ct_shippedagnstorderqty
,ifnull(f.ct_targetqty,0) ct_targetqty
,ifnull(f.ct_underdlvrtolerance,0) ct_underdlvrtolerance
,ifnull(f.dd_afsdeptdesc, 'Not Set') dd_afsdeptdesc
,ifnull(f.dd_afsmrpstatus, 'Not Set') dd_afsmrpstatus
,ifnull(f.dd_asrname, 'Not Set') dd_asrname
,ifnull(f.dd_batchno, 'Not Set') dd_batchno
,ifnull(f.dd_clearedblockedsts, 'Not Set') dd_clearedblockedsts
,ifnull(f.dd_conditionno, 'Not Set') dd_conditionno
,ifnull(f.dd_createdby, 'Not Set') dd_createdby
,ifnull(f.dd_creditlimit,0) dd_creditlimit
,ifnull(f.dd_creditmgr, 'Not Set') dd_creditmgr
,ifnull(f.dd_creditrep, 'Not Set') dd_creditrep
,ifnull(f.dd_customermaterialno, 'Not Set') dd_customermaterialno
,ifnull(f.dd_deliveryindicator, 'Not Set') dd_deliveryindicator
,ifnull(f.dd_documentconditionno, 'Not Set') dd_documentconditionno
,ifnull(f.dd_hierarchytype, 'Not Set') dd_hierarchytype
,ifnull(f.dd_highlevelitem,0) dd_highlevelitem
,ifnull(f.dd_internationalarticleno, 'Not Set') dd_internationalarticleno
,ifnull(f.dd_itemrelfordelv, 'Not Set') dd_itemrelfordelv
,ifnull(f.dd_packholdflag , 'Not Set')dd_packholdflag
,ifnull(f.dd_plannedgitime, 'Not Set') dd_plannedgitime
,ifnull(f.dd_podocumentitemno,0) dd_podocumentitemno
,ifnull(f.dd_podocumentno, 'Not Set') dd_podocumentno
,ifnull(f.dd_poscheduleno,0) dd_poscheduleno
,ifnull(f.dd_prodorderitemno,0) dd_prodorderitemno
,ifnull(f.dd_prodorderno,'Not Set') dd_prodorderno
,ifnull(f.dd_purchaseorderitem,0) dd_purchaseorderitem
,ifnull(f.dd_purchasereq,'Not Set') dd_purchasereq
,ifnull(f.dd_purchaserequisition,'Not Set') dd_purchaserequisition
,ifnull(f.dd_purchaserequisitionitemno,0) dd_purchaserequisitionitemno
,ifnull(f.dd_reqdeliverytime,'Not Set') dd_reqdeliverytime
,ifnull(f.dd_requirementtype,'Not Set') dd_requirementtype
,ifnull(f.dd_salesorderblocked,'Not Set') dd_salesorderblocked
,ifnull(f.dd_socreatetime,'Not Set') dd_socreatetime
,ifnull(f.dd_soldtohierarchylevel,0) dd_soldtohierarchylevel
,ifnull(f.dd_solinecreatetime,'Not Set') dd_solinecreatetime
,ifnull(f.dim_additionalcustomermaterialid,1) dim_additionalcustomermaterialid
,ifnull(f.dim_additionalmaterialid,1) dim_additionalmaterialid
,ifnull(f.dim_agreementsid,1) dim_agreementsid
,ifnull(f.dim_baseuomid,1) dim_baseuomid
,ifnull(f.dim_billingdateid,1) dim_billingdateid
,ifnull(f.dim_chargebackcontractid,1) dim_chargebackcontractid
,ifnull(f.dim_custmasteradditionalattrid,1) dim_custmasteradditionalattrid
,ifnull(f.dim_customerconditiongroups1id,1) dim_customerconditiongroups1id
,ifnull(f.dim_customerconditiongroups2id,1) dim_customerconditiongroups2id
,ifnull(f.dim_customerconditiongroups3id,1) dim_customerconditiongroups3id
,ifnull(f.dim_customerconditiongroups4id,1) dim_customerconditiongroups4id
,ifnull(f.dim_customerconditiongroups5id,1) dim_customerconditiongroups5id
,ifnull(f.dim_customergroupid,1) dim_customergroupid
,ifnull(f.dim_customeridcbowner,1) dim_customeridcbowner
,ifnull(f.dim_customerpaymenttermsid,1) dim_customerpaymenttermsid
,ifnull(f.dim_customerriskcategoryid,1) dim_customerriskcategoryid
,ifnull(f.dim_dateidactualgifill,1) dim_dateidactualgifill
,ifnull(f.dim_dateidbilling,1) dim_dateidbilling
,ifnull(f.dim_dateidbillingcreated,1) dim_dateidbillingcreated
,ifnull(f.dim_dateiddeliverytime,1) dim_dateiddeliverytime
,ifnull(f.dim_dateidfixedvalue,1) dim_dateidfixedvalue
,ifnull(f.dim_dateidgoodsissue,1) dim_dateidgoodsissue
,ifnull(f.dim_dateidlatestcustpoagi,1) dim_dateidlatestcustpoagi
,ifnull(f.dim_dateidloading,1) dim_dateidloading
,ifnull(f.dim_dateidloadingfill,1) dim_dateidloadingfill
,ifnull(f.dim_dateidmtrlavail,1) Dim_DateidMatlAvailOriginal
,ifnull(f.dim_dateidnetduedate,1) dim_dateidnetduedate
,ifnull(f.dim_dateidnextdate,1) dim_dateidnextdate
,ifnull(f.dim_dateidshipdlvrfill,1) dim_dateidshipdlvrfill
,ifnull(f.dim_dateidshipmentdelivery,1) dim_dateidshipmentdelivery
,ifnull(f.dim_dateidtransport,1) dim_dateidtransport
,ifnull(f.dim_dateidvalidfrom,1) dim_dateidvalidfrom
,ifnull(f.dim_dateidvalidto,1) dim_dateidvalidto
,ifnull(f.dim_h1customerid,1) dim_h1customerid
,ifnull(f.dim_highercustomerid,1) dim_highercustomerid
,ifnull(f.dim_higherdistchannelid,1) dim_higherdistchannelid
,ifnull(f.dim_highersalesdivid,1) dim_highersalesdivid
,ifnull(f.dim_highersalesorgid,1) dim_highersalesorgid
,ifnull(f.dim_materialpricegroup2id,1) dim_materialpricegroup2id
,ifnull(f.dim_orgid,1) dim_orgid
,ifnull(f.dim_paymenttermsid,1) dim_paymenttermsid
,ifnull(f.dim_salesmiscid,1) dim_salesmiscid
,ifnull(f.dim_salesriskcategoryid,1) dim_salesriskcategoryid
,ifnull(f.dim_salesuomid,1) dim_salesuomid
,ifnull(f.dim_toplevelcustomerid,1) dim_toplevelcustomerid
,ifnull(f.dim_topleveldistchannelid,1) dim_topleveldistchannelid
,ifnull(f.dim_toplevelsalesdivid,1) dim_toplevelsalesdivid
,ifnull(f.dim_toplevelsalesorgid,1) dim_toplevelsalesorgid
,ifnull(f.dim_unitofmeasureid,1) dim_unitofmeasureid
,(CASE
       WHEN f.ct_ConfirmedQty > 0
       THEN
         Decimal((f.amt_SubTotal3 / (case when f.ct_ConfirmedQty = 0 then null else f.ct_ConfirmedQty end)),18,4)
        ELSE
        0
        END)amt_salessubtotal3unitprice


,ifnull(lkls.LIPS_WAVWR,0) amt_Cost
,ifnull(lkls.LIPS_WAVWR,0) amt_Cost_DocCurr
,0 ct_afsbilledqty
,0 ct_afsdeliveredqty
,ifnull(lkls.LIPS_VBEAF,0) ct_fixedprocessdays
,ifnull(lkls.LIPS_LFIMG,0) ct_qtydelivered
,ifnull(lkls.LIPS_VBEAV,0) ct_ShipProcessDays
,0 dd_billingitem_no
,'Not Set' dd_gitime
,ifnull(lkls.LIPS_BWART, 'Not Set') dd_movementtype
,'Not Set' dd_pickingtime
,ifnull(lkls.LIKP_VBELN, 'Not Set') dd_salesdlvrdocno
,ifnull(lkls.LIPS_POSNR, 0) dd_salesdlvritemno
,'Not Set' dd_sdcreatetime
,'Not Set' dd_sdlinecreatetime
,'Not Set' dd_universalproductcode
,1 dim_afsmrpstatusid
,ifnull((SELECT dim_billingdocumenttypeid
                    FROM dim_billingdocumenttype bdt
                   WHERE bdt.Type = lkls.LIKP_FKARV AND bdt.RowIsCurrent = 1), 1) dim_billingdocumenttypeid
,1 dim_dateidactualgoodsissue
,1 dim_dateidbillingdate
,1 dim_dateiddeliverydate
,1 dim_dateidloadingdate
,1 dim_dateidmatlavail
,1 dim_dateidpickingdate
,1 dim_dateidplannedgoodsissue
,1 dim_deliveryheaderstatusid
,1 dim_deliveryitemstatusid
,1 dim_deliverytypeid
,0 amt_ATPStock
FROM max_holder_fact_salesorderlife m, fact_salesorderlife_so_tmp f
INNER JOIN LIKP_LIPS_sub00 lkls
    ON  f.dd_salesdocno = lkls.LIPS_VGBEL
    AND f.dd_salesitemno = lkls.LIPS_VGPOS
    AND f.dd_scheduleno = lkls.LIPS_J_3AETENR
WHERE f.dd_ItemRelForDelv = 'X';
	
call vectorwise(combine 'fact_salesorderlife_tmp');
	
UPDATE fact_salesorderlife_tmp sof
    FROM LIKP_LIPS lp
SET sof.dd_SDCreateTime = ifnull(lp.LIKP_ERZET, '000000'),
	sof.dd_DeliveryTime = ifnull(lp.LIKP_LFUHR, '000000'),
	sof.dd_PickingTime = ifnull(lp.LIKP_KOUHR, '000000'),
	sof.dd_GITime = ifnull(lp.LIKP_WAUHR, '000000'),
	sof.dd_SDLineCreateTime	= ifnull(lp.LIPS_ERZET, '000000')	
WHERE sof.dd_SalesDlvrDocNo = lp.LIKP_VBELN
  AND sof.dd_SalesDlvrItemNo = lp.LIPS_POSNR;	

UPDATE fact_salesorderlife_tmp sof
SET sof.dd_SDCreateTime = '000000',
	sof.dd_DeliveryTime = '000000',
	sof.dd_PickingTime = '000000',
	sof.dd_GITime = '000000',
	sof.dd_SDLineCreateTime	= '000000'
WHERE sof.dd_SDCreateTime IS NULL OR sof.dd_DeliveryTime IS NULL
OR sof.dd_PickingTime IS NULL OR sof.dd_GITime IS NULL OR sof.dd_SDLineCreateTime IS NULL;

call vectorwise(combine 'fact_salesorderlife_tmp');	
   
UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidActualGoodsIssue = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR 
   AND dd.DateValue = lkls.likp_wadat_ist
   AND lkls.likp_wadat_ist is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidActualGoodsIssue,-2) <> ifnull(dd.Dim_Dateid,-1);
   
UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidDeliveryDate = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR
   AND dd.DateValue = lkls.LIKP_LFDAT
   AND lkls.LIKP_LFDAT is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidDeliveryDate,-2) <> ifnull(dd.Dim_Dateid,-1);
	
UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidLoadingDate = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR
   AND dd.DateValue = lkls.LIKP_LDDAT
   AND lkls.LIKP_LDDAT is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidLoadingDate,-2) <> ifnull(dd.Dim_Dateid,-1);
   
UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidMatlAvail = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR
   AND dd.DateValue = lkls.lips_mbdat
   AND lkls.lips_mbdat is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidMatlAvail,-2) <> ifnull(dd.Dim_Dateid,-1);
   
UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidPickingDate = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR
   AND dd.DateValue = lkls.likp_kodat
   AND lkls.likp_kodat is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidPickingDate,-2) <> ifnull(dd.Dim_Dateid,-1);
   
UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidPlannedGoodsIssue = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR
   AND dd.DateValue = lkls.likp_wadat
   AND lkls.likp_wadat is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidPlannedGoodsIssue,-2) <> ifnull(dd.Dim_Dateid,-1);
   
UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidActualGI_Original = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR 
   AND dd.DateValue = lkls.likp_wadat_ist
   AND lkls.likp_wadat_ist is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidActualGI_Original,-2) <> ifnull(dd.Dim_Dateid,-1);
   
UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidDlvrDocCreated = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR 
   AND dd.DateValue = lkls.likp_erdat
   AND lkls.likp_erdat is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidDlvrDocCreated,-2) <> ifnull(dd.Dim_Dateid,-1);
   
UPDATE fact_salesorderlife_tmp sof 
  FROM LIKP_LIPS lkls, Dim_DeliveryType dt
   SET sof.Dim_DeliveryTypeId = dt.Dim_DeliveryTypeId
 WHERE sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR
   AND dt.DeliveryType = lkls.LIKP_LFART
   AND dt.RowIsCurrent = 1
   AND lkls.LIKP_LFART IS NOT NULL;

UPDATE fact_salesorderlife_tmp sof 
   SET sof.Dim_DeliveryTypeId = 1
 WHERE sof.Dim_DeliveryTypeId IS NULL;
 
DROP TABLE IF EXISTS cursor_table1_722;
CREATE TABLE cursor_table1_722(
v_iid		  Integer,
v_DlvrDocNo       VARCHAR(50) null, 
v_DlvrItemNo      INTEGER null,
v_SalesDocNo      VARCHAR(50) null,
v_SalesItemNo     INTEGER null,
v_DeliveryQty     DECIMAL(18,4) null,
v_AGI_Date        DATE null,
v_PGI_Date        DATE null,
v_GIDate          DATE null, 
v_SchedQty        DECIMAL(18,4) null,
v_SchedNo         INTEGER null, 
v_DlvrRowNo	   INTEGER null,
v_DlvrRowNoMax	   INTEGER null,
v_DeliveryQtyCUMM  DECIMAL(18,4) null);

INSERT INTO NUMBER_FOUNTAIN
SELECT 'cursor_table1_722',ifnull(max(v_iid),0)
FROM cursor_table1_722;


Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_SchedNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date,
v_DlvrRowNo,
v_DlvrRowNoMax)
SELECT fact_salesorderlifeid,
	dd_SalesDlvrDocNo v_DlvrDocNo, 
	dd_SalesDlvrItemNo v_DlvrItemNo,
	dd_SalesDocNo v_SalesDocNo,
	dd_SalesItemNo v_SalesItemNo,
	dd_ScheduleNo v_SchedNo,
	ct_QtyDelivered v_DeliveryQty,
	case when sof.Dim_DateidActualGoodsIssue = 1 then agi.datevalue else pgi.datevalue end v_AGI_Date,
	pgi.datevalue v_PGI_Date,
 	 1 AS v_DlvrRowNo,
	 1 v_DlvrRowNoMax
FROM fact_salesorderlife_tmp sof 
	inner join dim_date agi on sof.Dim_DateidActualGoodsIssue = agi.dim_dateid
	inner join dim_date pgi on sof.Dim_DateidPlannedGoodsIssue = pgi.dim_dateid
where sof.dd_SalesDlvrDocNo <> 'Not Set' and exists  (SELECT 1 FROM LIKP_LIPS where sof.dd_SalesDocNo = LIPS_VGBEL and sof.dd_SalesItemNo = LIPS_VGPOS AND sof.dd_ScheduleNo = LIPS_J_3AETENR);	

Drop table if exists cursor_table1_722_tmp;
CREATE TABLE cursor_table1_722_tmp As
SELECT v_iid,
	v_DlvrDocNo,
	v_DlvrItemNo,
	v_SalesDocNo,
	v_SalesItemNo,
	v_SchedNo,
	v_DeliveryQty,
	v_AGI_Date,
	v_PGI_Date,
	 ROW_NUMBER() OVER (PARTITION BY v_SalesDocNo,v_SalesItemNo,v_SchedNo ORDER BY v_AGI_Date,v_PGI_Date,v_DlvrDocNo,v_DlvrItemNo) AS v_DlvrRowNo,
	 COUNT(1) OVER (PARTITION BY v_SalesDocNo,v_SalesItemNo,v_SchedNo) v_DlvrRowNoMax
FROM cursor_table1_722;

delete FROM cursor_table1_722;
Insert into cursor_table1_722(
v_iid,
v_DlvrDocNo,
v_DlvrItemNo,
v_SalesDocNo,
v_SalesItemNo,
v_SchedNo,
v_DeliveryQty,
v_AGI_Date,
v_PGI_Date, 
v_DlvrRowNo,
v_DlvrRowNoMax)
SELECT v_iid,
	v_DlvrDocNo,
	v_DlvrItemNo,
	v_SalesDocNo,
	v_SalesItemNo,
	v_SchedNo,
	v_DeliveryQty,
	v_AGI_Date,
	v_PGI_Date,
	v_DlvrRowNo,
	v_DlvrRowNoMax
FROM cursor_table1_722_tmp;

Drop table if exists cursor_table1_722_tmp;	

Drop table if exists cursor_table1_723;
CREATE TABLE cursor_table1_723 AS
SELECT  a.v_iid,
	a.v_DlvrDocNo,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_DeliveryQty) v_DeliveryQtyCUMM
FROM cursor_table1_722 a inner join cursor_table1_722 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo
and a.v_SchedNo = b.v_SchedNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
GROUP BY a.v_iid,
	a.v_DlvrDocNo,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_DeliveryQty,
	a.v_AGI_Date,
	a.v_PGI_Date,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_SchedNo,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;

	Drop table if exists salesorder_table_101;
CREATE TABLE salesorder_table_101 AS
SELECT f.dd_SalesDocNo v_SalesDocNo,
	f.dd_SalesItemNo v_SalesItemNo,
	f.dd_ScheduleNo v_SchedNo,
	gi.DateValue v_GIDate,
	f.ct_ConfirmedQty v_SchedQty,
	ROW_NUMBER() OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo,f.dd_ScheduleNo ORDER BY f.dd_SalesDocNo,f.dd_SalesItemNo,f.dd_ScheduleNo) AS v_DlvrRowNo,
	COUNT(1) OVER (PARTITION BY f.dd_SalesDocNo,f.dd_SalesItemNo,f.dd_ScheduleNo) v_DlvrRowNoMax
From fact_salesorderlife_so_tmp f inner join dim_date gi on f.Dim_DateidGoodsIssue = gi.Dim_Dateid
where f.dd_ItemRelForDelv = 'X' and f.ct_ConfirmedQty > 0
	and exists (SELECT 1 FROM cursor_table1_722 where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo);

Drop table if exists cursor_table1_724;
CREATE TABLE cursor_table1_724 AS
SELECT a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	sum(b.v_SchedQty) v_SchedQtyCUMM
FROM salesorder_table_101 a inner join salesorder_table_101 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo and a.v_SchedNo  = b.v_SchedNo
where a.v_DlvrRowNo >= b.v_DlvrRowNo
GROUP BY a.v_SalesDocNo,
	a.v_SalesItemNo,
	a.v_SchedNo,
	a.v_GIDate,
	a.v_SchedQty,
	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax;
Drop table if exists salesorder_table_101;

/*** Get final values with schedule link ***/

Drop table if exists cursor_table1_722;
CREATE TABLE cursor_table1_722 AS
SELECT a.v_iid,
	a.v_DlvrDocNo,
	a.v_DlvrItemNo,
	a.v_SalesDocNo,
	a.v_SalesItemNo,
 	a.v_AGI_Date,
	a.v_PGI_Date,
	b.v_GIDate,
 	b.v_SchedNo,
 	a.v_DlvrRowNo,
	a.v_DlvrRowNoMax,
	a.v_DeliveryQtyCUMM,
	case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM 
	    then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax and b.v_SchedQtyCUMM <= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then 0
	    		else
	    		case when b.v_SchedQty > a.v_DeliveryQty 
			      	then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty then a.v_DeliveryQty 
					  when b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty - (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM)
					  else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	else case when b.v_DlvrRowNo = b.v_DlvrRowNoMax 
			      	  		then b.v_SchedQty
			      	  		else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  				when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)  
			      	  					then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 		else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  				when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  			  	else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	  		end
			 end
		 end
	    else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty 
		      	then case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then a.v_DeliveryQty + (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM) else a.v_DeliveryQty end
		      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax then b.v_SchedQty 
		      	  	else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) 
		      	   end
		 end
	end v_SchedQty,
      case when b.v_SchedQtyCUMM < a.v_DeliveryQtyCUMM 
            then case when b.v_DlvrRowNo = b.v_DlvrRowNoMax
            			then case when (b.v_SchedQtyCUMM - b.v_SchedQty) > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) 
            						then a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)
            				 	else a.v_DeliveryQty end
		      else
		    	case when b.v_SchedQty > a.v_DeliveryQty 
	                      then case when (a.v_DeliveryQtyCUMM - b.v_SchedQtyCUMM) > a.v_DeliveryQty or b.v_DlvrRowNo = b.v_DlvrRowNoMax
	                                then a.v_DeliveryQty 
	                                else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty 
	                                          else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end 
	                           end
	                      else case when a.v_DlvrRowNo = a.v_DlvrRowNoMax and b.v_DlvrRowNo = b.v_DlvrRowNoMax then a.v_DeliveryQty 
	                      			else case when a.v_DeliveryQtyCUMM - a.v_DeliveryQty = 0 then b.v_SchedQty
			      	  						when b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)  
			      	  							then case when (b.v_SchedQtyCUMM - b.v_SchedQty) >= (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) then b.v_SchedQty
			      	  						 			else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
	                      					when b.v_SchedQty <= (a.v_DeliveryQtyCUMM - (b.v_SchedQtyCUMM - b.v_SchedQty)) then b.v_SchedQty
			      	  		  				else b.v_SchedQtyCUMM - (a.v_DeliveryQtyCUMM - a.v_DeliveryQty) end
			      	   end
	                 end
	              end
            else case when (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) > a.v_DeliveryQty then a.v_DeliveryQty
                      else (b.v_SchedQty - (b.v_SchedQtyCUMM - a.v_DeliveryQtyCUMM)) 
                 end
      end v_DeliveryQty,
      ROW_NUMBER() OVER (PARTITION BY a.v_DlvrDocNo,a.v_DlvrItemNo ORDER BY b.v_DlvrRowNo) AS v_DlvrRowSeq
FROM cursor_table1_723 a
	inner join cursor_table1_724 b on a.v_SalesDocNo = b.v_SalesDocNo and a.v_SalesItemNo = b.v_SalesItemNo and a.v_SchedNo = b.v_SchedNo and a.v_SchedNo = b.v_SchedNo
where a.v_DeliveryQtyCUMM > (b.v_SchedQtyCUMM - b.v_SchedQty)	
	and ((b.v_DlvrRowNo < b.v_DlvrRowNoMax and b.v_SchedQtyCUMM > (a.v_DeliveryQtyCUMM - a.v_DeliveryQty)) or b.v_DlvrRowNo = b.v_DlvrRowNoMax);
	
Drop table if exists cursor_table1_723;
Drop table if exists cursor_table1_724;

UPDATE fact_salesorderlife_tmp sof
  FROM cursor_table1_722 c1
   SET sof.ct_ConfirmedQty = c1.v_SchedQty, 
       sof.ct_ScheduleQtySalesUnit = c1.v_SchedQty
 WHERE sof.dd_SalesDlvrDocNo <> 'Not Set'
   AND sof.dd_SalesDocNo = c1.v_SalesDocNo
   AND sof.dd_SalesItemNo = c1.v_SalesItemNo
   AND sof.dd_ScheduleNo = c1.v_SchedNo
   AND sof.dd_salesdlvrdocno = c1.v_DlvrDocNo
   AND sof.dd_SalesDlvrItemNo = c1.v_DlvrItemNo;


UPDATE fact_salesorderlife_tmp sof
  FROM  VBFA v
   SET sof.ct_QtyDelivered = 
       CASE WHEN (sof.ct_QtyDelivered - v.vbfa_rfmng) < 0 
            THEN 0 
            ELSE (sof.ct_QtyDelivered - v.vbfa_rfmng) END
  WHERE sof.dd_SalesDlvrDocNo = v.vbfa_vbelv 
    AND sof.dd_SalesDlvrItemNo = v.vbfa_posnv
    AND v.vbfa_bwart = '602' 
    AND sof.ct_QtyDelivered > 0;
    
call vectorwise(combine 'fact_salesorderlife_tmp');

/* Take the maxid from the temporary table create above so we can continue with the inserts*/
DROP TABLE IF EXISTS max_holder_fact_salesorderlife;
CREATE TABLE max_holder_fact_salesorderlife(maxid)
AS
SELECT ifnull(max(fact_salesorderlifeid),0)
FROM fact_salesorderlife_tmp;
call vectorwise(combine 'max_holder_fact_salesorderlife');

INSERT INTO fact_salesorderlife_tmp
(fact_salesorderlifeid
,amt_afsnetprice
,amt_afsnetvalue
,amt_baseprice
,amt_customerexpectedprice
,amt_dicountaccrualnetprice
,amt_exchangerate
,amt_exchangerate_gbl
,amt_exchangerate_stat
,amt_scheduletotal
,amt_stdprice
,amt_unitprice
,amt_unitpriceuom
,ct_afsallocatedqty
,ct_afscalculatedopenqty
,ct_afsintransitqty
,ct_afsopenpoqty
,ct_afsopenqty
,ct_afstotaldrawn
,ct_afsunallocatedqty
,ct_confirmedqty
,ct_priceunit
,ct_scheduleqtysalesunit
,dd_afsallocationgroupno
,dd_afsdepartment
,dd_afsstockcategory
,dd_afsstocktype
,dd_arunnumber
,dd_billing_no
,dd_businesscustomerpono
,dd_customerpono
,dd_deliverytime
,dd_idocnumber
,dd_openconfirmationsexists
,dd_referencedocumentno
,dd_referencedocitemno
,dd_releaserule
,dd_salesdocno
,dd_salesitemno
,dd_scheduleno
,dd_subsdocitemno
,dd_subsequentdocno
,dd_subsscheduleno
,dd_SeasonCode 
,dim_accountassignmentgroupid
,dim_afsrejectionreasonid
,dim_afsseasonid
,dim_afssizeid
,dim_availabilitycheckid
,dim_billingblockid
,dim_billtopartypartnerfunctionid
,dim_companyid
,dim_controllingareaid
,dim_costcenterid
,dim_creditrepresentativeid
,dim_currencyid
,dim_currencyid_gbl
,dim_currencyid_stat
,dim_currencyid_tra
,dim_customergroup1id
,dim_customergroup2id
,dim_customergroup4id
,dim_customerid
,dim_customeridshipto
,dim_customermastersalesid
,dim_custompartnerfunctionid
,dim_custompartnerfunctionid1
,dim_custompartnerfunctionid2
,dim_dateidactualgi_original
,dim_dateidafscanceldate
,dim_dateidafsreqdelivery
,dim_dateidarposting
,dim_dateidaslastdate
,dim_dateiddlvrdoccreated
,dim_dateidearliestsocanceldate
,dim_dateidfirstdate
,dim_dateidguaranteedate
,dim_dateidpurchaseorder
,dim_dateidquotationvalidfrom
,dim_dateidquotationvalidto
,dim_dateidrejection
,dim_dateidsalesordercreated
,dim_dateidscheddelivery
,dim_dateidscheddeliveryreq
,dim_dateidscheddlvrreqprev
,dim_dateidsocreated
,dim_dateidsodocument
,dim_dateidsoitemchangedon
,dim_deliveryblockid
,dim_deliverypriorityid
,dim_distributionchannelid
,dim_documentcategoryid
,dim_incotermid
,dim_materialgroupid
,dim_materialpricegroup1id
,dim_materialpricegroup4id
,dim_materialpricegroup5id
,dim_overallstatuscreditcheckid
,dim_partid
,dim_globalpartid
,dim_partsalesid
,dim_payerpartnerfunctionid
,dim_plantid
,dim_producthierarchyid
,dim_profitcenterid
,dim_projectsourceid
,dim_purchaseordertypeid
,dim_routeid
,dim_salesdistrictid
,dim_salesdivisionid
,dim_salesdocorderreasonid
,dim_salesdocumenttypeid
,dim_salesgroupid
,dim_salesofficeid
,dim_salesorderheaderstatusid
,dim_salesorderitemcategoryid
,dim_salesorderitemstatusid
,dim_salesorderpartnerid
,dim_salesorderrejectreasonid
,dim_salesorgid
,dim_scheduledeliveryblockid
,dim_schedulelinecategoryid
,dim_shippingconditionid
,dim_shipreceivepointid
,dim_storagelocationid
,dim_subsdoccategoryid
,dim_transactiongroupid

,amt_afsondeliveryvalue
,amt_afsunallocatedvalue
,amt_creditholdvalue
,amt_deliveryblockvalue
,amt_incomplete
,amt_netvalue
,amt_stdcost
,amt_subtotal1
,amt_subtotal2
,amt_subtotal3
,amt_subtotal3_orderqty
,amt_subtotal3incustconfig_billing
,amt_subtotal4
,amt_subtotal5
,amt_subtotal6
,amt_targetvalue
,amt_tax
,checksum1
,ct_afsafterrddunits
,ct_FixedQty
,ct_ReservedQty
,ct_afsbeforerddunits
,ct_afsinrddunits
,ct_afsondeliveryqty
,ct_cmlqtyreceived
,ct_correctedqty
,ct_cumconfirmedqty
,ct_cumorderqty
,ct_fillqty
,ct_fillqty_crd
,ct_fillqty_pdd
,ct_incompleteqty
,ct_onhandcoveredqty
,ct_onhandqty
,ct_overdlvrtolerance
,ct_shippedagnstorderqty
,ct_targetqty
,ct_underdlvrtolerance
,dd_afsdeptdesc
,dd_afsmrpstatus
,dd_asrname
,dd_batchno
,dd_clearedblockedsts
,dd_conditionno
,dd_createdby
,dd_creditlimit
,dd_creditmgr
,dd_creditrep
,dd_customermaterialno
,dd_deliveryindicator
,dd_documentconditionno
,dd_hierarchytype
,dd_highlevelitem
,dd_internationalarticleno
,dd_itemrelfordelv
,dd_packholdflag
,dd_plannedgitime
,dd_podocumentitemno
,dd_podocumentno
,dd_poscheduleno
,dd_prodorderitemno
,dd_prodorderno
,dd_purchaseorderitem
,dd_purchasereq
,dd_purchaserequisition
,dd_purchaserequisitionitemno
,dd_reqdeliverytime
,dd_requirementtype
,dd_salesorderblocked
,dd_socreatetime
,dd_soldtohierarchylevel
,dd_solinecreatetime
,dim_additionalcustomermaterialid
,dim_additionalmaterialid
,dim_agreementsid
,dim_baseuomid
,dim_billingdateid
,dim_chargebackcontractid
,dim_custmasteradditionalattrid
,dim_customerconditiongroups1id
,dim_customerconditiongroups2id
,dim_customerconditiongroups3id
,dim_customerconditiongroups4id
,dim_customerconditiongroups5id
,dim_customergroupid
,dim_customeridcbowner
,dim_customerpaymenttermsid
,dim_customerriskcategoryid
,dim_dateidactualgifill
,dim_dateidbilling
,dim_dateidbillingcreated
,dim_dateiddeliverytime
,dim_dateidfixedvalue
,dim_dateidgoodsissue
,dim_dateidlatestcustpoagi
,dim_dateidloading
,dim_dateidloadingfill
,Dim_DateidMatlAvailOriginal
,dim_dateidnetduedate
,dim_dateidnextdate
,dim_dateidshipdlvrfill
,dim_dateidshipmentdelivery
,dim_dateidtransport
,dim_dateidvalidfrom
,dim_dateidvalidto
,dim_h1customerid
,dim_highercustomerid
,dim_higherdistchannelid
,dim_highersalesdivid
,dim_highersalesorgid
,dim_materialpricegroup2id
,dim_orgid
,dim_paymenttermsid
,dim_salesmiscid
,dim_salesriskcategoryid
,dim_salesuomid
,dim_toplevelcustomerid
,dim_topleveldistchannelid
,dim_toplevelsalesdivid
,dim_toplevelsalesorgid
,dim_unitofmeasureid
,amt_salessubtotal3unitprice
,amt_cost
,amt_cost_doccurr
,ct_afsbilledqty
,ct_afsdeliveredqty
,ct_fixedprocessdays
,ct_qtydelivered
,ct_shipprocessdays
,dd_billingitem_no
,dd_gitime
,dd_movementtype
,dd_pickingtime
,dd_salesdlvrdocno
,dd_salesdlvritemno
,dd_sdcreatetime
,dd_sdlinecreatetime
,dd_universalproductcode
,dim_afsmrpstatusid
,dim_billingdocumenttypeid
,dim_dateidactualgoodsissue
,dim_dateidbillingdate
,dim_dateiddeliverydate
,dim_dateidloadingdate
,dim_dateidmatlavail
,dim_dateidpickingdate
,dim_dateidplannedgoodsissue
,dim_deliveryheaderstatusid
,dim_deliveryitemstatusid
,dim_deliverytypeid
,amt_ATPStock
)
SELECT 
m.maxid + row_number() over(order by f.dd_SalesDocNo,f.dd_SalesItemNo,f.dd_ScheduleNo) fact_salesorderdeliveryid
,ifnull(f.amt_afsnetprice,0) amt_afsnetprice
,ifnull(f.amt_afsnetvalue,0) amt_afsnetvalue
,ifnull(f.amt_baseprice,0) amt_baseprice
,ifnull(f.amt_customerexpectedprice,0) amt_customerexpectedprice
,ifnull(f.amt_dicountaccrualnetprice,0) amt_dicountaccrualnetprice
,ifnull(f.amt_exchangerate,0) amt_exchangerate
,ifnull(f.amt_exchangerate_gbl,0) amt_exchangerate_gbl
,ifnull(f.amt_exchangerate_stat,0) amt_exchangerate_stat
,ifnull(f.amt_scheduletotal,0) amt_scheduletotal
,ifnull(f.amt_stdprice,0) amt_stdprice
,ifnull(f.amt_unitprice,0) amt_unitprice
,ifnull(f.amt_unitpriceuom,0) amt_unitpriceuom
,ifnull(f.ct_afsallocatedqty,0) ct_afsallocatedqty
,ifnull(f.ct_afscalculatedopenqty,0) ct_afscalculatedopenqty
,ifnull(f.ct_afsintransitqty,0) ct_afsintransitqty
,ifnull(f.ct_afsopenpoqty,0) ct_afsopenpoqty
,0 ct_afsopenqty
,ifnull(f.ct_afstotaldrawn,0) ct_afstotaldrawn
,ifnull(f.ct_afsunallocatedqty,0) ct_afsunallocatedqty
,f.ct_confirmedqty ct_confirmedqty
,ifnull(f.ct_priceunit,0) ct_priceunit
,f.ct_scheduleqtysalesunit ct_scheduleqtysalesunit
,ifnull(f.dd_afsallocationgroupno, 'Not Set') dd_afsallocationgroupno
,ifnull(f.dd_afsdepartment, 'Not Set') dd_afsdepartment
,ifnull(f.dd_afsstockcategory, 'Not Set') dd_afsstockcategory
,ifnull(f.dd_afsstocktype, 'Not Set') dd_afsstocktype
,ifnull(f.dd_arunnumber, 'Not Set') dd_arunnumber
,ifnull(f.dd_billing_no, 'Not Set') dd_billing_no
,ifnull(f.dd_businesscustomerpono, 'Not Set') dd_businesscustomerpono
,ifnull(f.dd_customerpono, 'Not Set') dd_customerpono
,ifnull(f.dd_deliverytime, 'Not Set') dd_deliverytime
,ifnull(f.dd_idocnumber, 'Not Set') dd_idocnumber
,ifnull(f.dd_openconfirmationsexists,'Not Set') dd_openconfirmationsexists
,ifnull(f.dd_referencedocumentno, 'Not Set') dd_referencedocumentno
,ifnull(f.dd_referencedocitemno,0) dd_referencedocitemno
,ifnull(f.dd_releaserule, 'Not Set') dd_releaserule
,ifnull(f.dd_salesdocno, 'Not Set') dd_salesdocno
,ifnull(f.dd_salesitemno,0) dd_salesitemno
,ifnull(f.dd_scheduleno,0) dd_scheduleno
,ifnull(f.dd_subsdocitemno,0) dd_subsdocitemno
,ifnull(f.dd_subsequentdocno,'Not Set') dd_subsequentdocno
,ifnull(f.dd_subsscheduleno,0) dd_subsscheduleno
,ifnull(f.dd_SeasonCode, 'Not Set') dd_SeasonCode 
,ifnull(f.dim_accountassignmentgroupid,1) dim_accountassignmentgroupid
,ifnull(f.dim_afsrejectionreasonid,1) dim_afsrejectionreasonid
,ifnull(f.dim_afsseasonid,1) dim_afsseasonid 
,ifnull(f.dim_afssizeid,1) dim_afssizeid 
,ifnull(f.dim_availabilitycheckid,1) dim_availabilitycheckid
,ifnull(f.dim_billingblockid,1) dim_billingblockid
,ifnull(f.dim_billtopartypartnerfunctionid,1) dim_billtopartypartnerfunctionid
,ifnull(f.dim_companyid,1) dim_companyid
,ifnull(f.dim_controllingareaid,1) dim_controllingareaid
,ifnull(f.dim_costcenterid,1) dim_costcenterid
,ifnull(f.dim_creditrepresentativeid,1) dim_creditrepresentativeid
,ifnull(f.dim_currencyid,1) dim_currencyid
,ifnull(f.dim_currencyid_gbl,1) dim_currencyid_gbl
,ifnull(f.dim_currencyid_stat,1) dim_currencyid_stat
,ifnull(f.dim_currencyid_tra,1) dim_currencyid_tra
,ifnull(f.dim_customergroup1id,1) dim_customergroup1id
,ifnull(f.dim_customergroup2id,1) dim_customergroup2id
,ifnull(f.dim_customergroup4id,1) dim_customergroup4id
,ifnull(f.Dim_CustomerID,1) Dim_CustomeridSoldTo
,ifnull(f.dim_customeridshipto,1) dim_customeridshipto
,ifnull(f.dim_customermastersalesid,1) dim_customermastersalesid
,ifnull(f.dim_custompartnerfunctionid,1) dim_custompartnerfunctionid
,ifnull(f.dim_custompartnerfunctionid1,1) dim_custompartnerfunctionid1
,ifnull(f.dim_custompartnerfunctionid2,1) dim_custompartnerfunctionid2
,1 dim_dateidactualgi_original
,ifnull(f.dim_dateidafscanceldate,1) dim_dateidafscanceldate
,ifnull(f.dim_dateidafsreqdelivery,1) dim_dateidafsreqdelivery
,ifnull(f.dim_dateidarposting, 1) dim_dateidarposting
,ifnull(f.dim_dateidaslastdate,1) dim_dateidaslastdate
,1 dim_dateiddlvrdoccreated
,ifnull(f.dim_dateidearliestsocanceldate,1) dim_dateidearliestsocanceldate
,ifnull(f.dim_dateidfirstdate,1) dim_dateidfirstdate
,ifnull(f.dim_dateidguaranteedate,1) dim_dateidguaranteedate
,ifnull(f.dim_dateidpurchaseorder,1) dim_dateidpurchaseorder
,ifnull(f.dim_dateidquotationvalidfrom,1) dim_dateidquotationvalidfrom
,ifnull(f.dim_dateidquotationvalidto,1) dim_dateidquotationvalidto
,ifnull(f.dim_dateidrejection,1) dim_dateidrejection
,ifnull(f.dim_dateidsalesordercreated,1) dim_dateidsalesordercreated
,ifnull(f.dim_dateidscheddelivery,1) dim_dateidscheddelivery
,ifnull(f.dim_dateidscheddeliveryreq,1) dim_dateidscheddeliveryreq
,ifnull(f.dim_dateidscheddlvrreqprev,1) dim_dateidscheddlvrreqprev
,ifnull(f.dim_dateidsocreated,1) dim_dateidsocreated
,ifnull(f.dim_dateidsodocument,1) dim_dateidsodocument
,ifnull(f.dim_dateidsoitemchangedon,1) dim_dateidsoitemchangedon
,ifnull(f.dim_deliveryblockid,1) dim_deliveryblockid
,ifnull(f.dim_deliverypriorityid,1) dim_deliverypriorityid
,ifnull(f.dim_distributionchannelid,1) dim_distributionchannelid
,ifnull(f.dim_documentcategoryid,1) dim_documentcategoryid
,ifnull(f.dim_incotermid,1) dim_incotermid
,ifnull(f.dim_materialgroupid,1) dim_materialgroupid
,ifnull(f.dim_materialpricegroup1id,1) dim_materialpricegroup1id
,ifnull(f.dim_materialpricegroup4id,1) dim_materialpricegroup4id
,ifnull(f.dim_materialpricegroup5id,1) dim_materialpricegroup5id
,ifnull(f.dim_overallstatuscreditcheckid,1) dim_overallstatuscreditcheckid
,ifnull(f.dim_partid,1) dim_partid
,ifnull(f.dim_globalpartid,1) dim_globalpartid
,ifnull(f.dim_partsalesid,1) dim_partsalesid
,ifnull(f.dim_payerpartnerfunctionid,1) dim_payerpartnerfunctionid
,ifnull(f.dim_plantid,1) dim_plantid
,ifnull(f.dim_producthierarchyid,1) dim_producthierarchyid
,ifnull(f.dim_profitcenterid,1) dim_profitcenterid
,ifnull(f.dim_projectsourceid,1) dim_projectsourceid
,ifnull(f.dim_purchaseordertypeid,1) dim_purchaseordertypeid
,ifnull(f.dim_routeid,1) dim_routeid
,ifnull(f.dim_salesdistrictid,1) dim_salesdistrictid
,ifnull(f.dim_salesdivisionid,1) dim_salesdivisionid
,ifnull(f.dim_salesdocorderreasonid,1) dim_salesdocorderreasonid
,ifnull(f.dim_salesdocumenttypeid,1) dim_salesdocumenttypeid
,ifnull(f.dim_salesgroupid,1) dim_salesgroupid
,ifnull(f.dim_salesofficeid,1) dim_salesofficeid
,ifnull(f.dim_salesorderheaderstatusid,1) dim_salesorderheaderstatusid
,ifnull(f.dim_salesorderitemcategoryid,1) dim_salesorderitemcategoryid
,ifnull(f.dim_salesorderitemstatusid,1) dim_salesorderitemstatusid
,ifnull(f.dim_salesorderpartnerid,1) dim_salesorderpartnerid
,ifnull(f.dim_salesorderrejectreasonid,1) dim_salesorderrejectreasonid
,ifnull(f.dim_salesorgid,1) dim_salesorgid
,ifnull(f.dim_scheduledeliveryblockid,1) dim_scheduledeliveryblockid
,ifnull(f.dim_schedulelinecategoryid,1) dim_schedulelinecategoryid
,ifnull(f.dim_shippingconditionid,1) dim_shippingconditionid
,ifnull(f.dim_shipreceivepointid,1) dim_shipreceivepointid
,ifnull(f.dim_storagelocationid,1) dim_storagelocationid
,ifnull(f.dim_subsdoccategoryid,1) dim_subsdoccategoryid
,ifnull(f.dim_transactiongroupid,1) dim_transactiongroupid

,ifnull(f.amt_afsondeliveryvalue,0) amt_afsondeliveryvalue
,ifnull(f.amt_afsunallocatedvalue,0) amt_afsunallocatedvalue
,ifnull(f.amt_creditholdvalue,0) amt_creditholdvalue
,ifnull(f.amt_deliveryblockvalue,0) amt_deliveryblockvalue
,ifnull(f.amt_incomplete,0) amt_incomplete
,ifnull(f.amt_netvalue,0) amt_netvalue
,ifnull(f.amt_stdcost,0) amt_stdcost
,ifnull(f.amt_subtotal1,0) amt_subtotal1
,ifnull(f.amt_subtotal2,0) amt_subtotal2
,ifnull(f.amt_subtotal3,0) amt_subtotal3
,ifnull(f.amt_subtotal3_orderqty,0) amt_subtotal3_orderqty
,ifnull(f.amt_subtotal3incustconfig_billing,0) amt_subtotal3incustconfig_billing
,ifnull(f.amt_subtotal4,0) amt_subtotal4
,ifnull(f.amt_subtotal5,0) amt_subtotal5
,ifnull(f.amt_subtotal6,0) amt_subtotal6
,ifnull(f.amt_targetvalue,0) amt_targetvalue
,ifnull(f.amt_tax,0) amt_tax
,ifnull(f.checksum1,0) checksum1
,ifnull(f.ct_afsafterrddunits,0) ct_afsafterrddunits
,ifnull(f.ct_AfsAllocationFQty,0) ct_FixedQty
,ifnull(f.ct_AfsAllocationRQty,0) ct_ReservedQty
,ifnull(f.ct_afsbeforerddunits,0) ct_afsbeforerddunits
,ifnull(f.ct_afsinrddunits,0) ct_afsinrddunits
,ifnull(f.ct_afsondeliveryqty,0) ct_afsondeliveryqty
,ifnull(f.ct_cmlqtyreceived,0) ct_cmlqtyreceived
,ifnull(f.ct_correctedqty,0) ct_correctedqty
,ifnull(f.ct_cumconfirmedqty,0) ct_cumconfirmedqty
,ifnull(f.ct_cumorderqty,0) ct_cumorderqty
,ifnull(f.ct_fillqty,0) ct_fillqty
,ifnull(f.ct_fillqty_crd,0) ct_fillqty_crd
,ifnull(f.ct_fillqty_pdd,0) ct_fillqty_pdd
,ifnull(f.ct_incompleteqty,0) ct_incompleteqty
,ifnull(f.ct_onhandcoveredqty,0) ct_onhandcoveredqty
,ifnull(f.ct_onhandqty,0) ct_onhandqty
,ifnull(f.ct_overdlvrtolerance,0) ct_overdlvrtolerance
,ifnull(f.ct_shippedagnstorderqty,0) ct_shippedagnstorderqty
,ifnull(f.ct_targetqty,0) ct_targetqty
,ifnull(f.ct_underdlvrtolerance,0) ct_underdlvrtolerance
,ifnull(f.dd_afsdeptdesc, 'Not Set') dd_afsdeptdesc
,ifnull(f.dd_afsmrpstatus, 'Not Set') dd_afsmrpstatus
,ifnull(f.dd_asrname, 'Not Set') dd_asrname
,ifnull(f.dd_batchno, 'Not Set') dd_batchno
,ifnull(f.dd_clearedblockedsts, 'Not Set') dd_clearedblockedsts
,ifnull(f.dd_conditionno, 'Not Set') dd_conditionno
,ifnull(f.dd_createdby, 'Not Set') dd_createdby
,ifnull(f.dd_creditlimit,0) dd_creditlimit
,ifnull(f.dd_creditmgr, 'Not Set') dd_creditmgr
,ifnull(f.dd_creditrep, 'Not Set') dd_creditrep
,ifnull(f.dd_customermaterialno, 'Not Set') dd_customermaterialno
,ifnull(f.dd_deliveryindicator, 'Not Set') dd_deliveryindicator
,ifnull(f.dd_documentconditionno, 'Not Set') dd_documentconditionno
,ifnull(f.dd_hierarchytype, 'Not Set') dd_hierarchytype
,ifnull(f.dd_highlevelitem,0) dd_highlevelitem
,ifnull(f.dd_internationalarticleno, 'Not Set') dd_internationalarticleno
,ifnull(f.dd_itemrelfordelv, 'Not Set') dd_itemrelfordelv
,ifnull(f.dd_packholdflag , 'Not Set')dd_packholdflag
,ifnull(f.dd_plannedgitime, 'Not Set') dd_plannedgitime
,ifnull(f.dd_podocumentitemno,0) dd_podocumentitemno
,ifnull(f.dd_podocumentno, 'Not Set') dd_podocumentno
,ifnull(f.dd_poscheduleno,0) dd_poscheduleno
,ifnull(f.dd_prodorderitemno,0) dd_prodorderitemno
,ifnull(f.dd_prodorderno,'Not Set') dd_prodorderno
,ifnull(f.dd_purchaseorderitem,0) dd_purchaseorderitem
,ifnull(f.dd_purchasereq,'Not Set') dd_purchasereq
,ifnull(f.dd_purchaserequisition,'Not Set') dd_purchaserequisition
,ifnull(f.dd_purchaserequisitionitemno,0) dd_purchaserequisitionitemno
,ifnull(f.dd_reqdeliverytime,'Not Set') dd_reqdeliverytime
,ifnull(f.dd_requirementtype,'Not Set') dd_requirementtype
,ifnull(f.dd_salesorderblocked,'Not Set') dd_salesorderblocked
,ifnull(f.dd_socreatetime,'Not Set') dd_socreatetime
,ifnull(f.dd_soldtohierarchylevel,0) dd_soldtohierarchylevel
,ifnull(f.dd_solinecreatetime,'Not Set') dd_solinecreatetime
,ifnull(f.dim_additionalcustomermaterialid,1) dim_additionalcustomermaterialid
,ifnull(f.dim_additionalmaterialid,1) dim_additionalmaterialid
,ifnull(f.dim_agreementsid,1) dim_agreementsid
,ifnull(f.dim_baseuomid,1) dim_baseuomid
,ifnull(f.dim_billingdateid,1) dim_billingdateid
,ifnull(f.dim_chargebackcontractid,1) dim_chargebackcontractid
,ifnull(f.dim_custmasteradditionalattrid,1) dim_custmasteradditionalattrid
,ifnull(f.dim_customerconditiongroups1id,1) dim_customerconditiongroups1id
,ifnull(f.dim_customerconditiongroups2id,1) dim_customerconditiongroups2id
,ifnull(f.dim_customerconditiongroups3id,1) dim_customerconditiongroups3id
,ifnull(f.dim_customerconditiongroups4id,1) dim_customerconditiongroups4id
,ifnull(f.dim_customerconditiongroups5id,1) dim_customerconditiongroups5id
,ifnull(f.dim_customergroupid,1) dim_customergroupid
,ifnull(f.dim_customeridcbowner,1) dim_customeridcbowner
,ifnull(f.dim_customerpaymenttermsid,1) dim_customerpaymenttermsid
,ifnull(f.dim_customerriskcategoryid,1) dim_customerriskcategoryid
,ifnull(f.dim_dateidactualgifill,1) dim_dateidactualgifill
,ifnull(f.dim_dateidbilling,1) dim_dateidbilling
,ifnull(f.dim_dateidbillingcreated,1) dim_dateidbillingcreated
,ifnull(f.dim_dateiddeliverytime,1) dim_dateiddeliverytime
,ifnull(f.dim_dateidfixedvalue,1) dim_dateidfixedvalue
,ifnull(f.dim_dateidgoodsissue,1) dim_dateidgoodsissue
,ifnull(f.dim_dateidlatestcustpoagi,1) dim_dateidlatestcustpoagi
,ifnull(f.dim_dateidloading,1) dim_dateidloading
,ifnull(f.dim_dateidloadingfill,1) dim_dateidloadingfill
,ifnull(f.dim_dateidmtrlavail,1) Dim_DateidMatlAvailOriginal
,ifnull(f.dim_dateidnetduedate,1) dim_dateidnetduedate
,ifnull(f.dim_dateidnextdate,1) dim_dateidnextdate
,ifnull(f.dim_dateidshipdlvrfill,1) dim_dateidshipdlvrfill
,ifnull(f.dim_dateidshipmentdelivery,1) dim_dateidshipmentdelivery
,ifnull(f.dim_dateidtransport,1) dim_dateidtransport
,ifnull(f.dim_dateidvalidfrom,1) dim_dateidvalidfrom
,ifnull(f.dim_dateidvalidto,1) dim_dateidvalidto
,ifnull(f.dim_h1customerid,1) dim_h1customerid
,ifnull(f.dim_highercustomerid,1) dim_highercustomerid
,ifnull(f.dim_higherdistchannelid,1) dim_higherdistchannelid
,ifnull(f.dim_highersalesdivid,1) dim_highersalesdivid
,ifnull(f.dim_highersalesorgid,1) dim_highersalesorgid
,ifnull(f.dim_materialpricegroup2id,1) dim_materialpricegroup2id
,ifnull(f.dim_orgid,1) dim_orgid
,ifnull(f.dim_paymenttermsid,1) dim_paymenttermsid
,ifnull(f.dim_salesmiscid,1) dim_salesmiscid
,ifnull(f.dim_salesriskcategoryid,1) dim_salesriskcategoryid
,ifnull(f.dim_salesuomid,1) dim_salesuomid
,ifnull(f.dim_toplevelcustomerid,1) dim_toplevelcustomerid
,ifnull(f.dim_topleveldistchannelid,1) dim_topleveldistchannelid
,ifnull(f.dim_toplevelsalesdivid,1) dim_toplevelsalesdivid
,ifnull(f.dim_toplevelsalesorgid,1) dim_toplevelsalesorgid
,ifnull(f.dim_unitofmeasureid,1) dim_unitofmeasureid
,(CASE
       WHEN f.ct_ConfirmedQty > 0
       THEN
         Decimal((f.amt_SubTotal3 / (case when f.ct_ConfirmedQty = 0 then null else f.ct_ConfirmedQty end)),18,4)
        ELSE
        0
        END)amt_salessubtotal3unitprice


,0 amt_Cost
,0 amt_Cost_DocCurr
,0 ct_afsbilledqty
,0 ct_afsdeliveredqty
,0 ct_fixedprocessdays
,0 ct_qtydelivered
,0 ct_ShipProcessDays
,0 dd_billingitem_no
,'Not Set' dd_gitime
,'Not Set' dd_movementtype
,'Not Set' dd_pickingtime
,'Not Set' dd_salesdlvrdocno
,0 dd_salesdlvritemno
,'Not Set' dd_sdcreatetime
,'Not Set' dd_sdlinecreatetime
,'Not Set' dd_universalproductcode
,1 dim_afsmrpstatusid
,1 dim_billingdocumenttypeid
,1 dim_dateidactualgoodsissue
,1 dim_dateidbillingdate
,1 dim_dateiddeliverydate
,1 dim_dateidloadingdate
,1 dim_dateidmatlavail
,1 dim_dateidpickingdate
,1 dim_dateidplannedgoodsissue
,1 dim_deliveryheaderstatusid
,1 dim_deliveryitemstatusid
,1 dim_deliverytypeid
,0 amt_ATPStock
FROM max_holder_fact_salesorderlife m, fact_salesorderlife_so_tmp f
WHERE NOT EXISTS
                (SELECT 1 FROM fact_salesorderlife_tmp sof 
                 WHERE sof.dd_SalesDocNo = f.dd_SalesDocNo
                  AND sof.dd_SalesItemNo = f.dd_SalesItemNo 
                  AND sof.dd_ScheduleNo = f.dd_ScheduleNo);

	
call vectorwise(combine 'fact_salesorderlife_tmp');

/* Update ct_AfsOpenQty in fact_salesorderlife from fact_salesorder*/

call vectorwise(combine 'fact_salesorderlife_tmp');

UPDATE fact_salesorderlife_tmp sof
   SET sof.ct_AfsOpenQty = 0
 WHERE sof.ct_AfsOpenQty <> 0;
 
UPDATE fact_salesorderlife_tmp sof
  FROM fact_salesorderlife_so_tmp so
   SET sof.ct_AfsOpenQty = (CASE WHEN so.ct_AfsOpenQty < sof.ct_QtyDelivered 
                                 THEN sof.ct_QtyDelivered 
								 ELSE so.ct_AfsOpenQty END)
 WHERE sof.dd_SalesDocNo = so.dd_SalesDocNo
   AND sof.dd_SalesItemNo = so.dd_SalesItemNo
   AND sof.dd_ScheduleNo = so.dd_ScheduleNo
   AND sof.ct_ConfirmedQty = sof.ct_QtyDelivered
   AND sof.dim_DateIdActualGI_Original = 1
   AND sof.dd_SalesDlvrDocNo <> 'Not Set' 
   AND ifnull(sof.ct_AfsOpenQty,-1) <> ifnull((CASE WHEN so.ct_AfsOpenQty < sof.ct_QtyDelivered 
                                                    THEN sof.ct_QtyDelivered 
								                    ELSE so.ct_AfsOpenQty END),-2); 

UPDATE fact_salesorderlife_tmp sof
  FROM fact_salesorderlife_so_tmp so
   SET sof.ct_AfsOpenQty = (CASE WHEN so.ct_AfsOpenQty < sof.ct_Confirmedqty 
                                 THEN sof.ct_Confirmedqty 
								 ELSE so.ct_AfsOpenQty END)
 WHERE sof.dd_SalesDocNo = so.dd_SalesDocNo
   AND sof.dd_SalesItemNo = so.dd_SalesItemNo
   AND sof.dd_ScheduleNo = so.dd_ScheduleNo
   AND sof.ct_ConfirmedQty > sof.ct_QtyDelivered
   AND sof.dim_DateIdActualGI_Original = 1
   AND sof.dd_SalesDlvrDocNo <> 'Not Set' 
   AND ifnull(sof.ct_AfsOpenQty,-1) <> ifnull((CASE WHEN so.ct_AfsOpenQty < sof.ct_Confirmedqty 
                                                    THEN sof.ct_Confirmedqty 
								                    ELSE so.ct_AfsOpenQty END),-2); 

UPDATE fact_salesorderlife_tmp sof
  FROM fact_salesorderlife_so_tmp so
   SET sof.ct_AfsOpenQty = so.ct_AfsOpenQty
 WHERE sof.dd_SalesDocNo = so.dd_SalesDocNo
   AND sof.dd_SalesItemNo = so.dd_SalesItemNo
   AND sof.dd_ScheduleNo = so.dd_ScheduleNo
   AND sof.ct_ConfirmedQty < sof.ct_QtyDelivered
   AND sof.dim_DateIdActualGI_Original = 1
   AND sof.dd_SalesDlvrDocNo <> 'Not Set' 
   AND ifnull(sof.ct_AfsOpenQty,-1) <> ifnull(so.ct_AfsOpenQty,-2); 

UPDATE fact_salesorderlife_tmp sof
  FROM fact_salesorderlife_so_tmp so, dim_salesorderitemstatus sois
   SET sof.ct_AfsOpenQty = (sof.ct_ConfirmedQty - sof.ct_QtyDelivered)
 WHERE sof.dd_SalesDocNo = so.dd_SalesDocNo
   AND sof.dd_SalesItemNo = so.dd_SalesItemNo
   AND sof.dd_ScheduleNo = so.dd_ScheduleNo
   and sof.dim_Deliveryitemstatusid = sois.dim_salesorderitemstatusid
   AND sof.dim_DateIdActualGI_Original <> 1
   AND (sof.ct_ConfirmedQty - sof.ct_QtyDelivered) <= so.ct_AfsOpenQty
   AND sof.dd_SalesDlvrDocNo <> 'Not Set' 
   AND ifnull(sof.ct_AfsOpenQty,-1) <> ifnull(so.ct_AfsOpenQty,-2); 
 
UPDATE fact_salesorderlife_tmp sof
  FROM fact_salesorderlife_so_tmp so
   SET sof.ct_AfsOpenQty = so.ct_AfsOpenQty
 WHERE sof.dd_SalesDocNo = so.dd_SalesDocNo
   AND sof.dd_SalesItemNo = so.dd_SalesItemNo
   AND sof.dd_ScheduleNo = so.dd_ScheduleNo
   AND sof.dd_SalesDlvrDocNo = 'Not Set'
   AND ifnull(sof.ct_AfsOpenQty,0.0000) <> ifnull(so.ct_AfsOpenQty,-2); 

/* Update ct_AfsCalculatedOpenQty in fact_salesorder*/   
/* CALL vectorwise(COMBINE 'fact_salesorder')

UPDATE fact_salesorder t
   SET t.ct_AfsCalculatedOpenQty  = 0
 WHERE t.ct_AfsCalculatedOpenQty <> 0

UPDATE fact_salesorder t
  FROM dim_documentcategory dc
   SET t.ct_AfsCalculatedOpenQty  = (t.ct_ConfirmedQty - t.ct_AfsTotalDrawn)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND dc.DocumentCategory in ('G','g','B','b') 
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0)

UPDATE fact_salesorder t
  FROM dim_documentcategory dc
   SET t.ct_AfsCalculatedOpenQty  = (t.ct_AfsOpenQty - t.ct_AfsOnDeliveryQty)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND t.Dim_AfsRejectionReasonId = 1 
   AND t.ct_AfsOnDeliveryQty >= 0
   AND ((t.ct_AfsOpenQty - t.ct_AfsOnDeliveryQty) >= 0)

UPDATE fact_salesorder t
  FROM dim_documentcategory dc
   SET t.ct_AfsCalculatedOpenQty  = t.ct_AfsOpenQty
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND t.Dim_AfsRejectionReasonId = 1 
   AND t.Dim_DateIdActualGI = 1
   AND t.ct_AfsOnDeliveryQty = 0

UPDATE fact_salesorder t
   SET t.ct_AfsCalculatedOpenQty  = 0
 WHERE t.ct_AfsCalculatedOpenQty IS NULL

call vectorwise(combine 'fact_salesorder')   */

/* Update ct_AfsCalculatedOpenQty in fact_salesorderlife*/  
call vectorwise(combine 'fact_salesorderlife_tmp');

UPDATE fact_salesorderlife_tmp t
   SET t.ct_AfsCalculatedOpenQty  = 0
 WHERE t.ct_AfsCalculatedOpenQty  <> 0;

UPDATE fact_salesorderlife_tmp t
  FROM dim_documentcategory dc
   SET t.ct_AfsCalculatedOpenQty  = (t.ct_ConfirmedQty - t.ct_AfsTotalDrawn)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND dc.DocumentCategory in ('G','g','B','b') 
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0);

UPDATE fact_salesorderlife_tmp t
  FROM dim_documentcategory dc
   SET t.ct_AfsCalculatedOpenQty  = (t.ct_AfsOpenQty - t.ct_QtyDelivered)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND t.Dim_DateidActualGI_Original = 1 
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_AfsOpenQty - t.ct_QtyDelivered) >= 0);

UPDATE fact_salesorderlife_tmp t
  FROM dim_documentcategory dc
   SET t.ct_AfsCalculatedOpenQty  = t.ct_AfsOpenQty
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND t.Dim_DateidActualGI_Original <> 1 
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_AfsOpenQty - (t.ct_ConfirmedQty - t.ct_QtyDelivered)) >= 0);

UPDATE fact_salesorderlife_tmp t
   SET t.ct_AfsCalculatedOpenQty  = 0
 WHERE t.ct_AfsCalculatedOpenQty IS NULL;

call vectorwise(combine 'fact_salesorderlife_tmp');

/* End of Open Qty Logic */
   
UPDATE fact_salesorderlife_tmp sof
  FROM Dim_SalesOrderHeaderStatus sohs
   SET sof.Dim_DeliveryHeaderStatusid = ifnull(sohs.Dim_SalesOrderHeaderStatusid,1)
 WHERE sof.dd_salesdlvrdocno = sohs.SalesDocumentNumber
   AND ifnull(sof.Dim_DeliveryHeaderStatusid,-2) <> ifnull(sohs.Dim_SalesOrderHeaderStatusid,-1);
   
UPDATE fact_salesorderlife_tmp sof
  FROM Dim_SalesOrderItemStatus sois
   SET sof.Dim_DeliveryItemStatusid = ifnull(sois.Dim_SalesOrderItemStatusid,1)
 WHERE sof.dd_salesdlvrdocno = sois.SalesDocumentNumber
   AND sof.dd_salesdlvritemno = sois.SalesItemNumber
   AND ifnull(sof.Dim_DeliveryItemStatusid,-2) <> ifnull(sois.Dim_SalesOrderItemStatusid,-1); 
	
	
call vectorwise(combine 'fact_salesorderlife_tmp');

DROP TABLE IF EXISTS ct_group_update;
CREATE TABLE ct_group_update AS 
SELECT J_3ABDBS_AUFNR,J_3ABDBS_POSNR,J_3ABDBS_ETENR,J_3ABDBS_J_3ASTAT,sum(J_3ABDBS_MENGE) J_3ABDBS_MENGE
FROM j_3abdbs
WHERE J_3ABDBS_J_3ASTAT = 'D'
GROUP BY  J_3ABDBS_AUFNR,J_3ABDBS_POSNR,J_3ABDBS_ETENR,J_3ABDBS_J_3ASTAT;

UPDATE fact_salesorderlife_tmp sof
  FROM ct_group_update ast
   SET sof.ct_AfsDeliveredQty = ast.J_3ABDBS_MENGE  
 WHERE ast.J_3ABDBS_AUFNR = sof.dd_SalesDocNo
   AND ast.J_3ABDBS_POSNR = sof.dd_SalesItemNo
   AND ast.J_3ABDBS_ETENR = sof.dd_ScheduleNo;

DROP TABLE IF EXISTS ct_group_update;

call vectorwise(combine 'fact_salesorderlife_tmp');

UPDATE fact_salesorderlife_tmp sof
  FROM fact_salesorderlife_so_tmp so
   SET sof.ct_ReservedQty = (CASE WHEN sof.ct_AfsOpenQty >= so.ct_AfsAllocationRQty THEN so.ct_AfsAllocationRQty ELSE sof.ct_AfsOpenQty END)
 WHERE sof.dd_SalesDocNo = so.dd_SalesDocNo
   AND sof.dd_SalesItemNo = so.dd_SalesItemNo
   AND sof.dd_ScheduleNo = so.dd_ScheduleNo
   AND sof.ct_AfsOpenQty > 0
   AND ifnull(sof.ct_ReservedQty,0.0000) <> (CASE WHEN sof.ct_AfsOpenQty >= so.ct_AfsAllocationRQty THEN so.ct_AfsAllocationRQty ELSE sof.ct_AfsOpenQty END); 

UPDATE fact_salesorderlife_tmp sof
  FROM fact_salesorderlife_so_tmp so
   SET sof.ct_FixedQty = (CASE WHEN sof.ct_AfsOpenQty >= so.ct_AfsAllocationFQty THEN so.ct_AfsAllocationFQty ELSE sof.ct_AfsOpenQty END)
 WHERE sof.dd_SalesDocNo = so.dd_SalesDocNo
   AND sof.dd_SalesItemNo = so.dd_SalesItemNo
   AND sof.dd_ScheduleNo = so.dd_ScheduleNo
   AND sof.ct_AfsOpenQty >= 0
   AND ifnull(sof.ct_FixedQty,0.0000) <> (CASE WHEN sof.ct_AfsOpenQty >= so.ct_AfsAllocationFQty THEN so.ct_AfsAllocationFQty ELSE sof.ct_AfsOpenQty END); 

call vectorwise(combine 'fact_salesorderlife_tmp');

UPDATE fact_salesorderlife_tmp sof
FROM fact_billing fb, dim_documentcategory bdt, dim_billingmisc bm
 SET sof.dd_billing_no = fb.dd_billing_no,
     sof.dd_Billingitem_no = fb.dd_billing_item_no 
WHERE fb.dd_SalesDlvrDocNo = sof.dd_SalesDlvrDocNo
    AND fb.dd_SalesDlvrItemNo = sof.dd_SalesDlvrItemNo
	AND fb.dim_billingmiscid = bm.dim_billingmiscid
	AND bm.CancelFlag <> 'X'
	AND fb.dim_documentcategoryid = bdt.dim_documentcategoryid
	AND bdt.DocumentCategory IN ('M');
		

UPDATE fact_salesorderlife_tmp sof
  FROM  fact_billing fb, dim_documentcategory bdt, dim_billingmisc bm
   SET sof.Dim_DateidBillingDate = fb.Dim_DateidBilling 
 WHERE fb.dd_SalesDlvrDocNo = sof.dd_SalesDlvrDocNo
   AND fb.dd_SalesDlvrItemNo = sof.dd_SalesDlvrItemNo
   AND fb.dim_billingmiscid = bm.dim_billingmiscid
   AND bm.CancelFlag <> 'X'
   AND fb.dim_documentcategoryid = bdt.dim_documentcategoryid
   AND bdt.DocumentCategory IN ('M')
   AND ifnull(sof.Dim_DateidBillingDate,1) <> fb.Dim_DateidBilling;

/*    
UPDATE fact_salesorderlife_tmp sof
  FROM dim_part dp,
  	   dim_afssize asiz,  	
       MEAN mn
SET sof.dd_UniversalProductCode = ifnull(mn.MEAN_EAN11,'Not Set')
WHERE sof.dim_partid = dp.dim_partid
AND sof.dim_Afssizeid = asiz.dim_AfsSizeId
AND dp.PartNumber = mn.MEAN_MATNR
AND mn.MEAN_EAN11 IS NOT NULL
AND mn.MEAN_J_3AKORDX = asiz.Size
AND dp.RowIsCurrent = 1
AND sof.dim_partid > 1;


UPDATE fact_salesorderlife_tmp sof
SET sof.dd_UniversalProductCode = 'Not Set'
WHERE sof.dd_UniversalProductCode IS NULL;

*/


UPDATE fact_salesorderlife_tmp sof
  FROM VBAK_VBAP_VBEP lp, Dim_DeliveryPriority dp
   SET sof.Dim_DeliveryPriorityId = dp.Dim_DeliveryPriorityId
 WHERE sof.dd_SalesDocNo = lp.VBAK_VBELN
   AND sof.dd_SalesItemNo = lp.VBAP_POSNR
   AND sof.dd_ScheduleNo = lp.VBEP_ETENR
   AND dp.DeliveryPriority = lp.VBAP_LPRIO
   AND dp.RowIsCurrent = 1
   AND lp.vbap_LPRIO IS NOT NULL;
   
UPDATE fact_salesorderlife_tmp sof
  FROM VBAK_VBAP  lp, Dim_DeliveryPriority dp
   SET sof.Dim_DeliveryPriorityId = dp.Dim_DeliveryPriorityId
 WHERE sof.dd_SalesDocNo = lp.VBAP_VBELN
   AND sof.dd_SalesItemNo = lp.VBAP_POSNR
   AND sof.dd_ScheduleNo = 0
   AND dp.DeliveryPriority = lp.VBAP_LPRIO
   AND dp.RowIsCurrent = 1
   AND lp.vbap_LPRIO IS NOT NULL;   

UPDATE fact_salesorderlife_tmp sof
SET sof.Dim_DeliveryPriorityId = 1
WHERE sof.Dim_DeliveryPriorityId IS NULL; 

DROP TABLE IF EXISTS tmp_OrderBookSeasonCode;
CREATE TABLE tmp_OrderBookSeasonCode AS
SELECT   dd_SalesDlvrDocNo,dd_SalesDlvrItemNo, dt.datevalue AS AGIDate FROM fact_salesorderlife_tmp sof, dim_Date dt
where sof.dim_DateidActualGI_Original = dt.dim_Dateid
and sof.dim_DateidActualGI_Original > 1;
 
/*
UPDATE fact_salesorderlife_tmp sof
SET dd_OrderBookSeasonCode = ifnull(dd_SeasonCode,'Not Set')
WHERE sof.dim_DateidActualGI_Original = 1
AND ifnull(dd_OrderBookSeasonCode,'X') <> ifnull(dd_SeasonCode,'Not Set');
 
UPDATE fact_salesorderlife_tmp sof
FROM tmp_OrderBookSeasonCode t, AFS_DPRG_KOND b
SET dd_OrderBookSeasonCode = ifnull(b.AFS_DPRG_KOND_J_3ASEAN,'Not Set')
WHERE sof.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
AND sof.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo
AND t.AGIDate between b.AFS_DPRG_KOND_J_3ADLDV AND b.AFS_DPRG_KOND_J_3ADLDB
AND sof.dim_DateidActualGI_Original <> 1
AND ifnull(dd_OrderBookSeasonCode,'X') <> ifnull(AFS_DPRG_KOND_J_3ASEAN,'Not Set');
 
 
DROP TABLE IF EXISTS tmp_OrderBookSeasonCode;

UPDATE fact_salesorderlife_tmp sof
SET dd_OrderBookSeasonCode = ifnull(dd_SeasonCode,'Not Set')
WHERE sof.dim_DateidActualGI_Original = 1
AND ifnull(dd_OrderBookSeasonCode,'X') <> ifnull(dd_SeasonCode,'Not Set');
*/


/*Custom part ==> standardized*/
/*non-assortment condition, assortment parttype has been defined in afsuserdefinedtable*/

DROP TABLE IF EXISTS ct_group_update;
CREATE TABLE ct_group_update 
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo, SUM(ct_QtyDelivered) ct_QtyDelivered
  FROM fact_salesorderlife_tmp sof,dim_part pt
 WHERE sof.dim_partid = pt.dim_partid 
   AND dd_ScheduleNo <> 0  
   AND pt.assortment <> 'X'
   AND Dim_DateidActualGI_Original = 1
GROUP BY dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo;

CALL VECTORWISE(COMBINE 'afssalesorder_openconfirmations_temp-afssalesorder_openconfirmations_temp');

INSERT INTO afssalesorder_openconfirmations_temp
(SalesDocNo,SalesItemNo,ScheduleNo,L_MENGE,L_DABMG,B_MENGE,B_DABMG)
SELECT J_3AVBFAE_VBELV AS SalesDocNo,J_3avbfae_posnv AS SalesItemNo,j_3avbfae_etenv AS ScheduleNo, 
       ifnull(SUM(J_3ABSSI_MENGE),0) AS L_MENGE,ifnull(SUM(J_3ABSSI_DABMG),0) AS L_DABMG, 
       cast(0 AS decimal(18,4)) AS B_MENGE,cast(0 AS decimal(18,4)) AS B_DABMG 
 FROM J_3ABSSI, J_3AVBFAE, fact_salesorderlife_so_tmp so, dim_salesorderitemcategory sois, dim_part p
WHERE 
/*below condition is filtering only In Transit Docs from J_3ABSSI*/
      J_3ABSSI_J_3ABSKZ in ( 'L' )
  AND sois.dim_salesorderitemcategoryid = so.dim_salesorderitemcategoryid
  AND J_3ABSSI_J_3ABSNR = J_3AVBFAE_VBELN
  AND J_3abssi_J_3aupos = j_3avbfae_posnv
  AND j_3abssi_J_3aeups = j_3avbfae_etenv
  AND J_3AVBFAE_VBELV= so.dd_SalesDocNo 
  AND J_3avbfae_posnv = so.dd_Salesitemno
  AND j_3avbfae_etenv = so.dd_Scheduleno
/*below condition is filtering only by Purchase Orders from J_3AVBFAE*/
  AND J_3AVBFAE_VBTYP_N = 'V'
  AND p.dim_partid = so.dim_Partid
  AND p.assortment <> 'X'
/*Columbia mentioned that custom table ztpo_item_cat is containing all the item categories sor the below condition can be removed*/  
/*AND sois.salesorderitemcategory in  (SELECT ztpo_item_cat_pstyv FROM ztpo_item_cat)*/
GROUP BY J_3AVBFAE_VBELV,J_3avbfae_posnv,j_3avbfae_etenv,J_3ABSSI_J_3ABSKZ;

/* CALL VECTORWISE(COMBINE 'ct_group_update') */

/*
UPDATE fact_salesorder so
   SET so.ct_AfsOnDeliveryQty = 0
 WHERE so.ct_AfsOnDeliveryQty <> 0
 */
/*
UPDATE ct_group_update l 
  FROM afssalesorder_openconfirmations_temp t
   SET l.ct_QtyDelivered = l.ct_QtyDelivered + t.L_MENGE - t.L_DABMG */
/*adding In transit Qty minus Inbound Delivery quantity for Intransit Docs*/
/* WHERE l.dd_SalesDocNo = t.SalesDocNo
   AND l.dd_SalesItemNo = t.salesitemno
   AND l.dd_ScheduleNo = t.Scheduleno
   AND IFNULL(l.ct_QtyDelivered,-1) <> ifnull(l.ct_QtyDelivered + t.L_MENGE - t.L_DABMG,-2)*/ 

/* UPDATE fact_salesorder so
  FROM ct_group_update l
   SET so.ct_AfsOnDeliveryQty = ifnull(l.ct_QtyDelivered,0)
 WHERE so.dd_SalesDocNo = l.dd_SalesDocNo
   AND so.dd_SalesItemNo = l.dd_SalesItemNo
   AND ifnull(ct_AfsOnDeliveryQty,-1) <> ifnull(l.ct_QtyDelivered,0)
             
UPDATE fact_salesorder so 
   SET so.ct_AfsOnDeliveryQty = 0
 WHERE so.ct_AfsOnDeliveryQty IS NULL
*/
/* DROP TABLE IF EXISTS ct_group_update */

/* Starting Assortment updates */
/* CREATE TABLE ct_group_update
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo, SUM(ct_QtyDelivered) ct_QtyDelivered
  FROM fact_salesorderlife_tmp sof,dim_part pt
 WHERE sof.dim_partid = pt.dim_partid 
   AND sof.dd_ScheduleNo <> 0 
   AND pt.assortment = 'X'
   AND sof.Dim_DateidActualGI_Original = 1
GROUP BY sof.dd_SalesDocNo,sof.dd_SalesItemNo,sof.dd_ScheduleNo

CALL VECTORWISE(COMBINE 'ct_group_update')

/*
UPDATE fact_salesorder so
   SET so.ct_AssortOnDeliveryQty = 0
 WHERE so.ct_AssortOnDeliveryQty <> 0

UPDATE fact_salesorder so
  FROM ct_group_update l 
   SET so.ct_AssortOnDeliveryQty = ifnull(l.ct_QtyDelivered,0)
 WHERE so.dd_SalesDocNo = l.dd_SalesDocNo
   AND so.dd_SalesItemNo = l.dd_SalesItemNo
   AND so.dd_Scheduleno = l.dd_ScheduleNo
   AND so.ct_AssortOnDeliveryQty <> ifnull(l.ct_QtyDelivered,0)     
*/   

/* DROP TABLE IF EXISTS ct_group_update */

/*Updates SO ct_AfsCalculatedOpenQty for non-assortment*/
/*
UPDATE fact_salesorder t
  SET t.ct_AfsCalculatedOpenQty  = 0
WHERE t.ct_AfsCalculatedOpenQty <> 0

UPDATE fact_salesorder t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AfsCalculatedOpenQty  = (t.ct_ConfirmedQty - t.ct_AfsTotalDrawn)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory in ('G','g','B','b') 
   AND pt.assortment <>'X'
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0)
   AND ct_AfsCalculatedOpenQty <> ifnull((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn),-1)

UPDATE fact_salesorder t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AfsCalculatedOpenQty  = t.ct_AfsOpenQty - ct_AfsOnDeliveryQty
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND pt.assortment <> 'X'
   AND t.Dim_AfsRejectionReasonId = 1 
   AND t.ct_AfsOpenQty >= ct_AfsOnDeliveryQty
   AND ifnull(ct_AfsCalculatedOpenQty,-1) <> ifnull((t.ct_AfsOpenQty - ct_AfsOnDeliveryQty),-2)
   */
/*End of Updates SO ct_AfsCalculatedOpenQty for non-assortment*/

/*??? Need to understand why is this update performed*/
/* UPDATE fact_salesorder so
SET so.ct_AfsUnallocatedQty =
      (ifnull(so.ct_AfsCalculatedOpenQty, 0) - ifnull(so.ct_AfsAllocatedQty, 0))
WHERE ifnull(ct_AfsUnallocatedQty,-1) <> (ifnull(so.ct_AfsCalculatedOpenQty, 0) - ifnull(so.ct_AfsAllocatedQty, 0))
*/

/*Updates SO ct_AssortOnDeliveryQty = ct_AfsCalculatedOpenQty for assortment*/
/* UPDATE fact_salesorder t
   SET t.ct_AssortOnDeliveryQty  = 0
 WHERE t.ct_AssortOnDeliveryQty <> 0

UPDATE fact_salesorder t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AssortOnDeliveryQty  = ifnull((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn),0)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory in ('G','g','B','b') 
   AND pt.assortment = 'X'
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0)
   AND t.ct_AssortOnDeliveryQty  <> ifnull((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn),0)

UPDATE fact_salesorder t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AssortCalculatedOpenQty  = ifnull((CASE WHEN (t.ct_AfsOpenQty - ct_AssortOnDeliveryQty) > 0 THEN (t.ct_AfsOpenQty - ct_AssortOnDeliveryQty) ELSE 0 END) 
                                              /*+ (t.ct_ConfirmedQty - ct_AssortOnDeliveryQty)*/ /*,0)*/
/* WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND pt.assortment = 'X'
   AND t.Dim_AfsRejectionReasonId = 1 
   /*AND t.Dim_DateidActualGI = 1 */
/*  AND ifnull(t.ct_AssortCalculatedOpenQty,-1)  = ifnull((CASE WHEN (t.ct_AfsOpenQty - ct_AssortOnDeliveryQty) > 0 THEN (t.ct_AfsOpenQty - ct_AssortOnDeliveryQty) ELSE 0 END) 
                                              /*+ (t.ct_ConfirmedQty - ct_AssortOnDeliveryQty),-2)
*/											  
/* Update not needed anymore as we've commented the last part ==> Confirmed -OnDelivery Qty */
/* 
UPDATE fact_salesorder t
 FROM dim_documentcategory dc,dim_part pt
  SET t.ct_AssortCalculatedOpenQty  = ifnull(( CASE WHEN t.ct_AfsOpenQty  > ct_AssortOnDeliveryQty THEN (t.ct_AfsOpenQty - ct_AssortOnDeliveryQty) ELSE 0 END) + (t.ct_ConfirmedQty - ct_DeliveredQty),0)
WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
  AND t.dim_partid = pt.dim_partid
  AND dc.DocumentCategory NOT IN ('G','g','B','b') 
  AND pt.assortment = 'X'
  AND t.Dim_AfsRejectionReasonId = 1 
  AND t.Dim_DateidActualGI <> 1
  AND ifnull(t.ct_AssortCalculatedOpenQty,-2) <> ifnull( ( CASE WHEN t.ct_AfsOpenQty  > ct_AssortOnDeliveryQty THEN (t.ct_AfsOpenQty - ct_AssortOnDeliveryQty) ELSE 0 END) + (t.ct_ConfirmedQty - ct_DeliveredQty),-1)
  */
/*End of Updates SO ct_AssortOnDeliveryQty = ct_AfsCalculatedOpenQty for assortment*/

/*
CALL VECTORWISE(COMBINE 'fact_salesorder')
*/

/*Updates SOF ct_AfsCalculatedOpenQty for non-assortment*/
UPDATE fact_salesorderlife_tmp t
   SET t.ct_AfsCalculatedOpenQty = 0
 WHERE t.ct_AfsCalculatedOpenQty  <> 0;

UPDATE fact_salesorderlife_tmp t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AfsCalculatedOpenQty = ifnull((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn),0)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory in ('G','g','B','b') 
   AND pt.assortment <> 'X'
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0)
   AND t.ct_AfsCalculatedOpenQty  <> ifnull((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn),0);

UPDATE fact_salesorderlife_tmp t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AfsCalculatedOpenQty = IFNULL((t.ct_AfsOpenQty - t.ct_QtyDelivered),0)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND pt.assortment <> 'X'
   AND t.Dim_AfsRejectionReasonId = 1 
   AND t.ct_AfsOpenQty >= t.ct_QtyDelivered
   AND t.Dim_DateidActualGI_Original = 1
   AND t.ct_AfsCalculatedOpenQty  <> IFNULL((t.ct_AfsOpenQty - t.ct_QtyDelivered),0);

UPDATE fact_salesorderlife_tmp t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AfsCalculatedOpenQty = ifnull(t.ct_AfsOpenQty,0)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND t.Dim_DateidActualGI_Original <> 1 
   AND pt.assortment <> 'X'
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_AfsOpenQty - (t.ct_ConfirmedQty - t.ct_QtyDelivered)) >= 0)
   AND t.ct_AfsCalculatedOpenQty  <> ifnull(t.ct_AfsOpenQty,0);
/*End of Updates SOF ct_AfsCalculatedOpenQty for non-assortment*/

CALL VECTORWISE(COMBINE 'fact_salesorderlife_tmp');

/*Updates SOF ct_AssortOnDeliveryQty = ct_AfsCalculatedOpenQty for assortment*/
UPDATE fact_salesorderlife_tmp t
   SET t.ct_AssortCalculatedOpenQty = 0
 WHERE t.ct_AssortCalculatedOpenQty <> 0;

UPDATE fact_salesorderlife_tmp t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AssortCalculatedOpenQty = ifnull((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn),0)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory in ('G','g','B','b') 
   AND pt.assortment = 'X'
   AND t.Dim_AfsRejectionReasonId = 1 
   AND ((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn) >= 0)
   AND t.ct_AssortCalculatedOpenQty  <> ifnull((t.ct_ConfirmedQty - t.ct_AfsTotalDrawn),0);

UPDATE fact_salesorderlife_tmp t
  FROM dim_documentcategory dc,dim_part pt
   SET t.ct_AssortCalculatedOpenQty = ifnull((CASE WHEN t.ct_AfsOpenQty > t.ct_QtyDelivered  THEN (t.ct_AfsOpenQty - t.ct_QtyDelivered) ELSE 0 END) 
                                                   /* + (t.ct_ConfirmedQty - t.ct_QtyDelivered) */ ,0)
 WHERE t.dim_DocumentCategoryid = dc.dim_DocumentCategoryid
   AND t.dim_partid = pt.dim_partid
   AND dc.DocumentCategory NOT IN ('G','g','B','b') 
   AND pt.assortment = 'X'
   AND t.Dim_AfsRejectionReasonId = 1
   AND ifnull(t.ct_AssortCalculatedOpenQty,-1) <> ifnull((CASE WHEN t.ct_AfsOpenQty > t.ct_QtyDelivered  THEN (t.ct_AfsOpenQty - t.ct_QtyDelivered) ELSE 0 END),0);
/*End of Updates SOF ct_AssortOnDeliveryQty = ct_AfsCalculatedOpenQty for assortment*/

call vectorwise(combine 'afssalesorder_openconfirmations_temp-afssalesorder_openconfirmations_temp');

INSERT INTO afssalesorder_openconfirmations_temp
(SalesDocNo,SalesItemNo,ScheduleNo,L_MENGE,L_DABMG,B_MENGE,B_DABMG)
SELECT J_3AVBFAE_VBELV AS SalesDocNo,J_3avbfae_posnv AS SalesItemNo,j_3avbfae_etenv AS ScheduleNo, 
       ifnull(SUM(J_3ABSSI_MENGE),0) AS L_MENGE,ifnull(SUM(J_3ABSSI_DABMG),0) AS L_DABMG, 
	   cast(0 AS decimal(18,4)) AS B_MENGE,cast(0 AS decimal(18,4)) AS B_DABMG 
  FROM J_3ABSSI, J_3AVBFAE,fact_salesorderlife_so_tmp so, dim_salesorderitemcategory sois , dim_part p
/*below condition is filtering only In Transit Docs from J_3ABSSI*/
 WHERE J_3ABSSI_J_3ABSKZ in ( 'L' )
   AND sois.dim_salesorderitemcategoryid = so.dim_salesorderitemcategoryid
   AND J_3ABSSI_J_3ABSNR = J_3AVBFAE_VBELN
   AND J_3abssi_J_3aupos = j_3avbfae_posnv
   AND j_3abssi_J_3aeups = j_3avbfae_etenv
   AND J_3AVBFAE_VBELV= so.dd_SalesDocNo 
   AND J_3avbfae_posnv = so.dd_Salesitemno
   AND j_3avbfae_etenv = so.dd_Scheduleno
   AND J_3AVBFAE_VBTYP_N = 'V'
   AND p.dim_partid = so.dim_Partid
   AND p.assortment = 'X'
/* Below condition not needed for AFS clients */
/* AND sois.salesorderitemcategory in  (SELECT ztpo_item_cat_pstyv FROM ztpo_item_cat) */
GROUP BY J_3AVBFAE_VBELV,J_3avbfae_posnv,j_3avbfae_etenv,J_3ABSSI_J_3ABSKZ;

/*Below Update is Updating the PO Qty*/
UPDATE afssalesorder_openconfirmations_temp t
  FROM J_3ABSSI, J_3AVBFAE
   SET B_MENGE = ifnull(J_3ABSSI_MENGE,0),
       B_DABMG = ifnull(J_3ABSSI_DABMG,0)
 WHERE J_3ABSSI_J_3ABSKZ in ( 'B' )
   AND J_3ABSSI_J_3ABSNR = J_3AVBFAE_VBELN
   AND J_3abssi_J_3aupos = j_3avbfae_posnv
   AND j_3abssi_J_3aeups = j_3avbfae_etenv
   AND J_3AVBFAE_VBELV= t.SalesDocNo 
   AND J_3avbfae_posnv = t.Salesitemno
   AND j_3avbfae_etenv = t.Scheduleno
   AND J_3AVBFAE_VBTYP_N = 'V'
   AND ifnull(B_MENGE,-1) <> ifnull(J_3ABSSI_MENGE,0);

INSERT INTO afssalesorder_openconfirmations_temp
(SalesDocNo,SalesItemNo,ScheduleNo,L_MENGE,L_DABMG,B_MENGE,B_DABMG)
SELECT J_3AVBFAE_VBELV AS SalesDocNo,J_3avbfae_posnv AS SalesItemNo,j_3avbfae_etenv AS ScheduleNo,
       cast(0 AS decimal(18,4))  AS L_MENGE,cast(0 AS decimal(18,4)) AS L_DABMG, 
	   ifnull(SUM(J_3ABSSI_MENGE),0) AS B_MENGE,ifnull(SUM(J_3ABSSI_DABMG),0) AS B_DABMG
  FROM J_3ABSSI, J_3AVBFAE,fact_salesorderlife_so_tmp so, dim_salesorderitemcategory sois,dim_part p
 WHERE J_3ABSSI_J_3ABSKZ in ( 'B' )
   AND sois.dim_salesorderitemcategoryid = so.dim_salesorderitemcategoryid
   AND J_3ABSSI_J_3ABSNR = J_3AVBFAE_VBELN
   AND J_3abssi_J_3aupos = j_3avbfae_posnv
   AND j_3abssi_J_3aeups = j_3avbfae_etenv
   AND J_3AVBFAE_VBELV = so.dd_SalesDocNo 
   AND J_3avbfae_posnv = so.dd_Salesitemno
   AND j_3avbfae_etenv = so.dd_Scheduleno
   AND J_3AVBFAE_VBTYP_N = 'V'
   AND p.dim_partid = so.dim_partid
   AND p.assortment <> 'X'
/* AND sois.salesorderitemcategory in  (SELECT ztpo_item_cat_pstyv FROM ztpo_item_cat) */
   AND NOT EXISTS (SELECT 1 FROM afssalesorder_openconfirmations_temp t
 WHERE t.SalesDocNo = so.dd_SalesDocNo and t.salesItemNo = so.dd_SalesItemno and t.scheduleno = so.dd_ScheduleNo)
GROUP BY J_3AVBFAE_VBELV,J_3avbfae_posnv, j_3avbfae_etenv,J_3ABSSI_J_3ABSKZ;

/*
UPDATE fact_salesorder so
   SET ct_AfsOpenPOQty = 0
 WHERE ct_AfsOpenPOQty <> 0

UPDATE fact_salesorder so
   SET ct_AfsInTransitQty = 0
 WHERE ct_AfsInTransitQty <> 0

UPDATE fact_salesorder so
  FROM afssalesorder_openconfirmations_temp t
   SET so.ct_AfsOpenPOQty = ifnull(( CASE WHEN  t.L_MENGE > 0 THEN (B_MENGE+B_DABMG) ELSE  B_MENGE END),0) 
 WHERE so.dd_Salesdocno = t.SalesDocNo
   AND so.dd_SalesItemNo = t.SalesItemNo
   AND so.dd_ScheduleNo = t.ScheduleNo 
   AND so.ct_AfsOpenPOQty <> ifnull(( CASE WHEN  t.L_MENGE > 0 THEN (B_MENGE+B_DABMG) ELSE  B_MENGE END),0)  

UPDATE fact_salesorder so
  FROM afssalesorder_openconfirmations_temp t
   SET ct_AfsInTransitQty = ifnull(t.L_MENGE,0)
 WHERE so.dd_Salesdocno = t.SalesDocNo
   AND so.dd_SalesItemNo = t.SalesItemNo
   AND so.dd_ScheduleNo = t.ScheduleNo 
   AND so.ct_AfsInTransitQty <> ifnull(t.L_MENGE,0)  
 */  

UPDATE fact_salesorderlife_tmp sof
   SET ct_AfsOpenPOQty = 0
 WHERE ct_AfsOpenPOQty <> 0;

UPDATE fact_salesorderlife_tmp sof
   SET ct_AfsInTransitQty = 0
 WHERE ct_AfsInTransitQty <> 0;

UPDATE fact_salesorderlife_tmp sof
  FROM afssalesorder_openconfirmations_temp t
   SET sof.ct_AfsOpenPOQty = ifnull(( CASE WHEN  t.L_MENGE > 0 THEN (B_MENGE+B_DABMG) ELSE  B_MENGE END),0) 
 WHERE sof.dd_Salesdocno = t.SalesDocNo
   AND sof.dd_SalesItemNo = t.SalesItemNo
   AND sof.dd_ScheduleNo = t.ScheduleNo 
   AND sof.ct_AfsOpenPOQty <> ifnull(( CASE WHEN  t.L_MENGE > 0 THEN (B_MENGE+B_DABMG) ELSE  B_MENGE END),0)  ;

UPDATE fact_salesorderlife_tmp sof
  FROM afssalesorder_openconfirmations_temp t
   SET ct_AfsInTransitQty = ifnull(t.L_MENGE,0)
 WHERE sof.dd_Salesdocno = t.SalesDocNo
   AND sof.dd_SalesItemNo = t.SalesItemNo
   AND sof.dd_ScheduleNo = t.ScheduleNo 
   AND sof.ct_AfsInTransitQty <> ifnull(t.L_MENGE,0)  ;

CALL VECTORWISE(COMBINE 'afssalesorder_openconfirmations_temp-afssalesorder_openconfirmations_temp');

UPDATE fact_salesorderlife_tmp sof
   SET sof.ct_AfsUnallocatedQty =
      (ifnull(sof.ct_AfsCalculatedOpenQty, 0) - ifnull(sof.ct_AfsAllocatedQty, 0))
 WHERE sof.ct_AfsUnallocatedQty <> (ifnull(sof.ct_AfsCalculatedOpenQty, 0) - ifnull(sof.ct_AfsAllocatedQty, 0));
 
/*dim_Customermastersalesid is custom dimension, To check if it can be sandardized*/
UPDATE fact_salesorderlife_tmp so
  FROM dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_customer cm
  SET so.dim_Customermastersalesid = cms.dim_Customermastersalesid
WHERE so.dim_salesorgid = sorg.dim_salesorgid
  AND cm.dim_customerid = so.dim_customeridsoldto
  AND sorg.SalesOrgCode = cms.SalesOrg
  AND cm.customernumber = cms.CustomerNumber
  AND ifnull(so.dim_Customermastersalesid,-1) <> ifnull(cms.dim_Customermastersalesid,-2);

/*Insert Data from Temp Table to Fact Table*/
CALL VECTORWISE(COMBINE 'fact_salesorderlife_tmp');
CALL VECTORWISE(COMBINE 'fact_salesorderlife - fact_salesorderlife');
CALL VECTORWISE(COMBINE 'fact_salesorderlife');
CALL VECTORWISE(COMBINE 'fact_salesorderlife + fact_salesorderlife_tmp');


UPDATE fact_salesorderlife f_sof
  FROM Dim_Date dt,dim_plant pl, vbak_vbap_vbep vbk
   SET f_sof.dim_CustomerCommitmentDId = dt.dim_dateid
 WHERE vbk.VBAK_VBELN = f_sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = f_sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = f_sof.dd_ScheduleNo
   AND dt.DateValue = vbk.VBEP_EDATU
   AND vbk.VBEP_EDATU is not null
   AND dt.companycode = pl.companycode
   AND vbk.VBAP_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND f_sof.dim_CustomerCommitmentDId <> dt.dim_dateid;
   
  UPDATE fact_salesorderlife_tmp sof
  FROM LIKP_LIPS lkls, dim_plant pl, Dim_Date dd
   SET sof.Dim_DateidDlvrDocCreated = ifnull(dd.Dim_Dateid,1)
 WHERE sof.dd_salesdocno = lkls.LIPS_VGBEL
   AND sof.dd_salesitemno = lkls.LIPS_VGPOS
   AND sof.dd_scheduleno = lkls.LIPS_J_3AETENR
   AND sof.dd_SalesDlvrDocNo = lkls.LIKP_VBELN 
   AND sof.dd_SalesDlvrItemNo = lkls.LIPS_POSNR 
   AND dd.DateValue = lkls.likp_erdat
   AND lkls.likp_erdat is not null
   AND dd.companycode = pl.companycode
   AND lkls.LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
   AND ifnull(sof.Dim_DateidDlvrDocCreated,-2) <> ifnull(dd.Dim_Dateid,-1);

/* OTIF Days Late AGI to CCD */
UPDATE fact_salesorderlife f_sof
  FROM  dim_date agid, dim_date ccd
   SET f_sof.dd_otifdayslateccd = current_Date - ccd.dateValue
 WHERE ccd.dim_dateid = f_sof.dim_CustomerCommitmentDId
   AND agid.dim_dateid = f_sof.dim_dateidactualgoodsissue
   AND f_sof.dim_dateidactualgoodsissue = 1
   AND ifnull(f_sof.dd_otifdayslateccd,-1) <> ifnull((current_Date - ccd.dateValue),-2);

/* OTIF Days Late AGI to CCD */
UPDATE fact_salesorderlife f_sof
  FROM dim_date agid, dim_date ccd
   SET f_sof.dd_otifdayslateccd = agid.DateValue - ccd.dateValue
 WHERE ccd.dim_dateid = f_sof.dim_CustomerCommitmentDId
   AND agid.dim_dateid = f_sof.dim_dateidactualgoodsissue
   AND f_sof.dim_dateidactualgoodsissue > 1
   AND f_sof.dd_otifdayslateccd <> (agid.DateValue - ccd.dateValue);

/* OTIF Days Late AGI to RDD */
UPDATE fact_salesorderlife f_sof
  FROM dim_date agid, dim_date rdd
   SET f_sof.dd_OTIFDaysLateRDD = current_Date - rdd.dateValue
 WHERE rdd.dim_dateid = f_sof.dim_dateidafsreqdelivery
   AND agid.dim_dateid = f_sof.dim_dateidactualgoodsissue
   AND f_sof.dim_dateidactualgoodsissue = 1
   AND ifnull(f_sof.dd_OTIFDaysLateRDD,-2) <> ifnull((current_Date - rdd.dateValue), -1);

/* OTIF Days Late AGI to RDD */
UPDATE fact_salesorderlife f_sof
  FROM dim_date agid, dim_date rdd
   SET f_sof.dd_OTIFDaysLateRDD = agid.DateValue - rdd.dateValue
 WHERE rdd.dim_dateid = f_sof.dim_dateidafsreqdelivery
   AND agid.dim_dateid = f_sof.dim_dateidactualgoodsissue
   AND f_sof.dim_dateidactualgoodsissue > 1
   AND ifnull(f_sof.dd_OTIFDaysLateRDD,-1) <> ifnull((agid.DateValue - rdd.dateValue),-2);
   
/* Update On Delivery Qty */

UPDATE fact_salesorderlife  sof 
SET sof.ct_AfsOnDeliveryQty = 0 
WHERE sof.ct_AfsOnDeliveryQty <> 0;
			
DROP TABLE IF EXISTS ct_group_update;
CREATE TABLE ct_group_update 
AS
Select dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo ,dd_salesdlvritemno ,dd_salesdlvrdocno ,SUM(ct_QtyDelivered) ct_QtyDelivered
	From fact_salesorderlife
 where dd_ScheduleNo <> 0 
	AND  Dim_DateidActualGI_Original = 1
 Group by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,dd_salesdlvritemno ,dd_salesdlvrdocno  ;

call vectorwise(combine 'ct_group_update');

UPDATE fact_salesorderlife sof
  FROM ct_group_update l
	SET sof.ct_AfsOnDeliveryQty = l.ct_QtyDelivered
 WHERE     sof.dd_SalesDocNo = l.dd_SalesDocNo
	AND sof.dd_SalesItemNo = l.dd_SalesItemNo
	AND sof.dd_Scheduleno = l.dd_ScheduleNo
	AND sof.dd_salesdlvrdocno=l.dd_salesdlvrdocno  
	AND sof.dd_salesdlvritemno=l.dd_salesdlvritemno;

UPDATE  fact_salesorderlife sof
SET  sof.ct_AfsOnDeliveryQty = 0
WHERE  sof.ct_AfsOnDeliveryQty IS NULL;

DROP TABLE IF EXISTS ct_group_update;

UPDATE fact_salesorderlife sof
      SET  sof.amt_AfsOnDeliveryValue =
              (CASE
                  WHEN sof.ct_AfsOnDeliveryQty > 0
                  THEN
                    (sof.ct_AfsOnDeliveryQty
                      * (CASE
                            WHEN (sof.amt_AfsNetPrice <> 0)
                            THEN
                              sof.amt_AfsNetPrice
                            ELSE
                              sof.amt_UnitPrice
                        END))
                  ELSE
                    0
              END)
WHERE sof.Dim_DateidActualGI_Original = 1
	AND IFNULL(sof.amt_AfsOnDeliveryValue,-1) <> 
              (CASE
                  WHEN sof.ct_AfsOnDeliveryQty > 0
                  THEN
                    (sof.ct_AfsOnDeliveryQty
                      * (CASE
                            WHEN (sof.amt_AfsNetPrice <> 0)
                            THEN
                              sof.amt_AfsNetPrice
                            ELSE
                              sof.amt_UnitPrice
                        END))
                  ELSE
                    0
              END) ;

/* Update dd_SeasonCode */
/*			  
UPDATE fact_salesorderlife so
	FROM dim_date dt, AFS_DPRG_KOND b
		SET dd_SeasonCode  = ifnull(AFS_DPRG_KOND_J_3ASEAN,'Not Set')
WHERE so.Dim_DateIdAFSReqDelivery <> 1
	AND so.Dim_DateIdAFSReqDelivery = dt.Dim_DateId
	AND dt.DateValue between AFS_DPRG_KOND_J_3ADLDV AND AFS_DPRG_KOND_J_3ADLDB
	AND IFNULL(so.dd_SeasonCode ,'xxx') <> ifnull(AFS_DPRG_KOND_J_3ASEAN,'Not Set');			  
*/  

 
 /* Update dim_globalpartid */
 
UPDATE fact_salesorderlife sof
	FROM  VBAK_VBAP_VBEP
		SET   Dim_globalPartid = 
			  IFNULL((SELECT dim_globalpartid
						FROM dim_globalpart dp
					  WHERE dp.PartNumber = VBAP_MATNR 
					        AND dp.Plant = VBAP_WERKS),1)
WHERE VBAK_VBELN = sof.dd_SalesDocNo 
	  AND VBAP_POSNR = sof.dd_SalesItemNo 
	  AND VBEP_ETENR = sof.dd_ScheduleNo;

/* Update dd_CustomCustExpPriceInc, dd_CustomPriceMissingInc,dd_CustomCustomerSKUInc,dd_CustomLongTextInc,dd_CustomOtherInc */
	  
UPDATE fact_salesorderlife sof
SET sof.dd_CustomCustExpPriceInc = 'Not Set'
WHERE (sof.dd_CustomCustExpPriceInc <> 'Not Set' OR sof.dd_CustomCustExpPriceInc IS NULL);

UPDATE fact_salesorderlife sof
SET sof.dd_CustomPriceMissingInc = 'Not Set'
WHERE ( sof.dd_CustomPriceMissingInc <> 'Not Set' OR sof.dd_CustomPriceMissingInc IS NULL);

UPDATE fact_salesorderlife sof
SET sof.dd_CustomCustomerSKUInc = 'Not Set'
WHERE (sof.dd_CustomCustomerSKUInc <> 'Not Set' OR sof.dd_CustomCustomerSKUInc IS NULL);

UPDATE fact_salesorderlife sof
SET sof.dd_CustomLongTextInc = 'Not Set'
WHERE (sof.dd_CustomLongTextInc <> 'Not Set' OR sof.dd_CustomLongTextInc IS NULL);

UPDATE fact_salesorderlife sof
SET sof.dd_CustomOtherInc = 'Not Set'
WHERE ( sof.dd_CustomOtherInc <> 'Not Set' OR sof.dd_CustomOtherInc IS NULL);
	  
UPDATE fact_salesorderlife sof FROM vbak_vbuv v
	SET sof.dd_CustomCustExpPriceInc = (CASE WHEN v.VBUV_FDNAM ='CEPOK' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo  
  AND v.VBUV_POSNR = 0 AND v.VBUV_ETENR = 0
  AND ifnull(sof.dd_CustomCustExpPriceInc,'xxx') <> (CASE WHEN v.VBUV_FDNAM ='CEPOK' THEN 'X' ELSE 'Not Set' END);

UPDATE       fact_salesorderlife sof FROM vbak_vbuv v
	SET sof.dd_CustomPriceMissingInc = (CASE WHEN v.VBUV_FDNAM ='PRSOK' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo  
  AND v.VBUV_POSNR = 0 AND v.VBUV_ETENR = 0
  AND ifnull(sof.dd_CustomPriceMissingInc,'xxx') <> (CASE WHEN v.VBUV_FDNAM ='PRSOK' THEN 'X' ELSE 'Not Set' END);

UPDATE       fact_salesorderlife sof FROM vbak_vbuv v
	SET sof.dd_CustomCustomerSKUInc = (CASE WHEN v.VBUV_FDNAM ='J_3AKDMAT' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo  
  AND v.VBUV_POSNR = 0 AND v.VBUV_ETENR = 0
  AND ifnull(sof.dd_CustomCustomerSKUInc,'xxx') <> (CASE WHEN v.VBUV_FDNAM ='J_3AKDMAT' THEN 'X' ELSE 'Not Set' END);

UPDATE       fact_salesorderlife sof FROM vbak_vbuv v
	SET sof.dd_CustomLongTextInc = (CASE WHEN v.VBUV_TDID IN ('ZE20','ZE24') THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo  
  AND v.VBUV_POSNR = 0 AND v.VBUV_ETENR = 0
  AND ifnull(sof.dd_CustomLongTextInc,'xxx') <> (CASE WHEN v.VBUV_TDID IN ('ZE20','ZE24') THEN 'X' ELSE 'Not Set' END);

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
	SET sof.dd_CustomOtherInc = (CASE WHEN v.VBUV_FDNAM IN ('CEPOK','PRSOK','J_3AKDMAT') OR v.VBUV_TDID IN ('ZE20','ZE24') THEN 'Not Set' ELSE 'X' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo  
  AND v.VBUV_POSNR = 0 AND v.VBUV_ETENR = 0
  AND ifnull(sof.dd_CustomOtherInc,'xxx') <>
	(CASE WHEN v.VBUV_FDNAM IN ('CEPOK','PRSOK','J_3AKDMAT') OR v.VBUV_TDID IN ('ZE20','ZE24') THEN 'Not Set' ELSE 'X' END);
	
UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomCustExpPriceInc = (CASE WHEN v.VBUV_FDNAM ='CEPOK' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = 0
AND IFNULL(sof.dd_CustomCustExpPriceInc ,'xxx') <>  (CASE WHEN v.VBUV_FDNAM ='CEPOK' THEN 'X' ELSE 'Not Set' END) ;

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomPriceMissingInc = (CASE WHEN v.VBUV_FDNAM ='PRSOK' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = 0
AND IFNULL(sof.dd_CustomPriceMissingInc ,'xxx') <>  (CASE WHEN v.VBUV_FDNAM ='PRSOK' THEN 'X' ELSE 'Not Set' END) ;

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomCustomerSKUInc = (CASE WHEN v.VBUV_FDNAM ='J_3AKDMAT' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = 0
AND IFNULL(sof.dd_CustomCustomerSKUInc ,'xxx') <>  (CASE WHEN v.VBUV_FDNAM ='J_3AKDMAT' THEN 'X' ELSE 'Not Set' END) ;

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomLongTextInc = (CASE WHEN v.VBUV_TDID IN ('ZE20','ZE24') THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = 0
AND IFNULL(sof.dd_CustomLongTextInc ,'xxx') <>  (CASE WHEN v.VBUV_TDID IN ('ZE20','ZE24') THEN 'X' ELSE 'Not Set' END) ;

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomOtherInc = (CASE WHEN v.VBUV_FDNAM IN ('CEPOK','PRSOK','J_3AKDMAT') OR v.VBUV_TDID IN ('ZE20','ZE24') THEN 'Not Set' ELSE 'X' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = 0
AND IFNULL(sof.dd_CustomOtherInc ,'xxx') <>  (CASE WHEN v.VBUV_FDNAM IN ('CEPOK','PRSOK','J_3AKDMAT') OR v.VBUV_TDID IN ('ZE20','ZE24') THEN 'Not Set' ELSE 'X' END) ;


UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomCustExpPriceInc = (CASE WHEN v.VBUV_FDNAM ='CEPOK' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = sof.dd_ScheduleNo
AND IFNULL(sof.dd_CustomCustExpPriceInc ,'xxx') <>  (CASE WHEN v.VBUV_FDNAM ='CEPOK' THEN 'X' ELSE 'Not Set' END) ;

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomPriceMissingInc = (CASE WHEN v.VBUV_FDNAM ='PRSOK' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = sof.dd_ScheduleNo
AND IFNULL(sof.dd_CustomPriceMissingInc ,'xxx') <>  (CASE WHEN v.VBUV_FDNAM ='PRSOK' THEN 'X' ELSE 'Not Set' END) ;

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomCustomerSKUInc = (CASE WHEN v.VBUV_FDNAM ='J_3AKDMAT' THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = sof.dd_ScheduleNo
AND IFNULL(sof.dd_CustomCustomerSKUInc ,'xxx') <>  (CASE WHEN v.VBUV_FDNAM ='J_3AKDMAT' THEN 'X' ELSE 'Not Set' END) ;

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomLongTextInc = (CASE WHEN v.VBUV_TDID IN ('ZE20','ZE24') THEN 'X' ELSE 'Not Set' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = sof.dd_ScheduleNo
AND IFNULL(sof.dd_CustomLongTextInc ,'xxx') <>  (CASE WHEN v.VBUV_TDID IN ('ZE20','ZE24') THEN 'X' ELSE 'Not Set' END) ;

UPDATE fact_salesorderlife sof FROM vbak_vbuv v
SET sof.dd_CustomOtherInc = (CASE WHEN v.VBUV_FDNAM IN ('CEPOK','PRSOK','J_3AKDMAT') OR v.VBUV_TDID IN ('ZE20','ZE24') THEN 'Not Set' ELSE 'X' END)
WHERE v.VBAK_VBELN = sof.dd_SalesDocNo AND v.VBUV_POSNR = sof.dd_SalesItemNo AND v.VBUV_ETENR = sof.dd_ScheduleNo
AND IFNULL(sof.dd_CustomOtherInc ,'xxx') <>  (CASE WHEN v.VBUV_FDNAM IN ('CEPOK','PRSOK','J_3AKDMAT') OR v.VBUV_TDID IN ('ZE20','ZE24') THEN 'Not Set' ELSE 'X' END) ;


/* Add sales rep dimension */
UPDATE fact_salesorderlife so
FROM vbpa sdp,variable_holder_fact_salesorderlife 
SET so.Dim_CustomSalesRepId =
                ifnull(
                  (SELECT Dim_CustomerId
                      FROM Dim_Customer c
WHERE     c.CustomerNumber = sdp.vbpa_kunnr
                          AND c.RowIsCurrent = 1),
                  1)
WHERE sdp.vbpa_parvw = pCustomPartnerFunctionKey
AND (so.dd_SalesDocNo - 1111111111) = sdp.vbpa_vbeln
AND sdp.vbpa_posnr = 0
AND pCustomPartnerFunctionKey <> 'Not Set';


UPDATE fact_salesorderlife so
FROM  vbpa sdp,variable_holder_fact_salesorderlife
SET so.Dim_CustomSalesRepId =
                ifnull(
                  (SELECT Dim_CustomerId
                      FROM Dim_Customer c
WHERE     c.CustomerNumber = sdp.vbpa_kunnr
                          AND c.RowIsCurrent = 1),
                  1)
WHERE sdp.vbpa_parvw = pCustomPartnerFunctionKey
AND (so.dd_SalesDocNo - 1111111111) = sdp.vbpa_vbeln
AND so.dd_Salesitemno = sdp.vbpa_posnr
AND pCustomPartnerFunctionKey <> 'Not Set';

Drop table if exists variable_holder_fact_salesorderlife;

call vectorwise(combine 'fact_salesorderlife');