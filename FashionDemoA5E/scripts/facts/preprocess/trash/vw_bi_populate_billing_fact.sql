/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 20 May 2013 */
/*   Description    : Stored Proc bi_populate_billing_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   29 Apr 2014	Lokesh	1.6     Optimized queries */
/*   24 Feb 2014      Cornelia          Added: Dim_partsalesid */
/*   14 Feb 2014      George    1.5     Added: Dim_CustomerGroup4id */ 
/*   03 Feb 2014      George    1.4     Added: dim_materialpricegroup4id, dim_materialpricegroup5id,dim_CustomerConditionGroups1id,dim_CustomerConditionGroups2id,dim_CustomerConditionGroups3id */
/*   21 Nov 2013      Cornelia  1.4               Enable Material Pricing Group ->Dim_MaterialPricingGroupId				  */
/*   09 Sep 2013      Lokesh	1.3		  Currency and Exchange rate changes. Store tran amts and store Tran->loc and tran->gbl rates */
/*   31 May 2013      Lokesh    1.2       Converted multi-column updates to single-column 						  */
/*   23 May 2013      Lokesh	1.1		  Merged Shanthi's changes from vw_bi_populate_billing_sales_shipment_attributes.sql  */
/*   20 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/



/* ALTER TABLE fact_billing DISABLE KEYS */
/* SET FOREIGN_KEY_CHECKS = 0 */

select 'Checkpoint A',TIMESTAMP(LOCAL_TIMESTAMP);

select 'START OF PROC bi_populate_billing_fact',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS tmp_pGlobalCurrency_fact_billing;
CREATE TABLE tmp_pGlobalCurrency_fact_billing ( pGlobalCurrency CHAR(3) NULL);

INSERT INTO tmp_pGlobalCurrency_fact_billing VALUES ( 'USD' );

update tmp_pGlobalCurrency_fact_billing 
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

/* Insert 1 */

DROP TABLE IF EXISTS tmp_db_fb1 ;
CREATE TABLE tmp_db_fb1
AS
SELECT pc.ProfitCenterCode,pc.ControllingArea,VBRK_FKDAT,min(pc.ValidTo) as min_ValidTo
FROM dim_profitcenter pc,VBRK_VBRP v
WHERE   pc.ProfitCenterCode = VBRP_PRCTR
AND pc.ControllingArea = VBRP_KOKRS
AND pc.ValidTo >= VBRK_FKDAT
AND pc.RowIsCurrent = 1
GROUP BY pc.ProfitCenterCode,pc.ControllingArea,VBRK_FKDAT;


DROP TABLE IF EXISTS tmp_db_fb2;
CREATE TABLE tmp_db_fb2
AS
SELECT pc.*,t.VBRK_FKDAT,t.min_ValidTo
FROM dim_profitcenter_fact_billing pc,tmp_db_fb1 t
WHERE pc.ProfitCenterCode = t.ProfitCenterCode
AND  pc.ControllingArea = t.ControllingArea
AND pc.ValidTo = t.min_ValidTo;

delete from NUMBER_FOUNTAIN where table_name = 'fact_billing';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_billing',ifnull(max(fact_billingid),0)
FROM fact_billing;


DROP TABLE IF EXISTS tmp_ins_fact_billing;
CREATE TABLE tmp_ins_fact_billing
AS
SELECT DISTINCT VBRK_VBELN as dd_billing_no, VBRP_POSNR as dd_billing_item_no,cast(1 as int) dim_companyid, cast(1 as int) dim_plantid , 'Not Set' currency
     FROM    VBRK_VBRP;


/* Need exactly the same table structure for combine to work */
DROP TABLE IF EXISTS tmp_del_fact_billing;
CREATE TABLE tmp_del_fact_billing
AS
SELECT * FROM tmp_ins_fact_billing where 1 = 2;

INSERT INTO tmp_del_fact_billing
SELECT DISTINCT b.dd_billing_no, b.dd_billing_item_no,1,1,'Not Set'
FROM fact_billing b;

call vectorwise(combine 'tmp_ins_fact_billing - tmp_del_fact_billing');

UPDATE tmp_ins_fact_billing b
FROM dim_company c, VBRK_VBRP
SET b.Dim_Companyid = c.dim_companyid
WHERE b.dd_billing_no = VBRK_VBELN AND b.dd_billing_item_no = VBRP_POSNR
AND c.companycode = VBRK_BUKRS AND c.RowIsCurrent = 1
AND b.Dim_Companyid <> c.dim_companyid;

UPDATE tmp_ins_fact_billing b
FROM dim_company c,VBRK_VBRP
SET b.currency = c.currency
WHERE b.dd_billing_no = VBRK_VBELN AND b.dd_billing_item_no = VBRP_POSNR
AND c.companycode = VBRK_BUKRS AND c.RowIsCurrent = 1
AND b.Dim_Companyid <> c.dim_companyid;


UPDATE tmp_ins_fact_billing b
FROM dim_plant dp, VBRK_VBRP
SET b.dim_plantid = dp.dim_plantid
WHERE b.dd_billing_no = VBRK_VBELN AND b.dd_billing_item_no = VBRP_POSNR
AND dp.PlantCode = VBRP_WERKS AND dp.RowIsCurrent = 1;


INSERT INTO fact_billing(ct_BillingQtySalesUOM,
                         amt_ExchangeRate,
                         amt_ExchangeRate_GBL,
                         amt_CashDiscount,
                         ct_BillingQtyStockUOM,
                         dd_billing_no,
                         dd_billing_item_no,
                         dd_CancelledDocumentNo,
                         dd_SalesDlvrItemNo,
                         dd_SalesDlvrDocNo,
                         dd_fiscalyear,
                         dd_postingperiod,
                         Dim_AccountingTransferStatusid,
                         Dim_Companyid,
                         Dim_ConditionProcedureid,
                         Dim_ControllingAreaid,
                         Dim_CreditControlAreaid,
                         Dim_Currencyid,
                         Dim_CustomerGroup1id,
                         Dim_CustomerID,
                         Dim_CustomerPaymentTermsid,
                         Dim_DateidBilling,
                         Dim_DateidCreated,
                         Dim_DistributionChannelId,
                         Dim_DocumentCategoryid,
                         Dim_DocumentTypeid,
                         Dim_IncoTermid,
                         Dim_MaterialGroupid,
                         Dim_Partid,
                         Dim_Plantid,
                         Dim_ProductHierarchyid,
                         Dim_ProfitCenterid,
                         Dim_SalesDivisionid,
                         Dim_SalesGroupid,
                         Dim_SalesOfficeid,
                         Dim_SalesOrgid,
                         Dim_ShipReceivePointid,
                         Dim_StorageLocationid,
                         Dim_UnitOfMeasureId,
                         Dim_SalesOrderItemCategoryid,
                         dd_salesdocno,
                         dd_salesitemno,
                         Dim_BillingCategoryId,
                         Dim_RevenueRecognitionCategoryId,
                         amt_cost_InDocCurrency,
                         amt_NetValueHeader_InDocCurrency,
                         amt_NetValueItem_InDocCurrency,
                         amt_CustomerConfigSubtotal1_InDocCurrency,
                         amt_CustomerConfigSubtotal2_InDocCurrency,
                         amt_CustomerConfigSubtotal3_InDocCurrency,
                         amt_CustomerConfigSubtotal4_InDocCurrency,
                         amt_CustomerConfigSubtotal5_InDocCurrency,
                         amt_CustomerConfigSubtotal6_InDocCurrency,
                         dd_CustomerPONumber,
                         dd_CreatedBy,fact_billingid,
                         Dim_Currencyid_TRA,
                         Dim_Currencyid_GBL,
                         Dim_MaterialPricingGroupId,
                         dim_partsalesid)
   SELECT VBRP_FKLMG * VBRP_UMVKN / VBRP_UMVKZ,
         ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
              where z.pFromCurrency  = VBRK_WAERK and z.fact_script_name = 'bi_populate_billing_fact' and z.pToCurrency =b.Currency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT ),1)
                          amt_ExchangeRate ,


                  ifnull(( select z.exchangeRate from tmp_getExchangeRate1 z
                  where z.pFromCurrency  = VBRK_WAERK and z.fact_script_name = 'bi_populate_billing_fact' and z.pToCurrency = pGlobalCurrency AND z.pDate = VBRK_FKDAT AND z.pFromExchangeRate = 0 ),1)
                  amt_ExchangeRate_GBL ,
          VBRP_SKFBP amt_CashDiscount,
          VBRP_FKLMG ct_BillingQtyStockUOM,
          VBRK_VBELN dd_billing_no,
          VBRP_POSNR dd_billing_item_no,
          ifnull(VBRK_SFAKN,'Not Set') dd_CancelledDocumentNo,
          VBRP_VGPOS dd_SalesDlvrItemNo,
          ifnull(VBRP_VGBEL, 'Not Set') dd_SalesDlvrDocNo,
          VBRK_GJAHR dd_fiscalyear,
          VBRK_POPER dd_postingperiod,
             1 Dim_AccountingTransferStatusid,
             1 Dim_Companyid,
             1 Dim_ConditionProcedureid,
             1 Dim_ControllingAreaid,
             1 Dim_CreditControlAreaid,
             1 Dim_Currencyid,
             1 Dim_CustomerGroup1id,
             1 Dim_CustomerID,
             1 Dim_CustomerPaymentTermsid,
             1 Dim_DateidBilling,
          1 Dim_DateIdCreated,
             1 Dim_DistributionChannelId,
             1 Dim_DocumentCategoryid,
             1 Dim_DocumentTypeid,
             1 Dim_IncoTermid,
             1 Dim_MaterialGroupid,
             1 Dim_Partid,
          ifnull(dim_Plantid,1) dim_Plantid,
          ifnull((SELECT dim_producthierarchyid
             FROM dim_producthierarchy ph
            WHERE ph.ProductHierarchy = VBRP_PRODH
                  AND ph.RowIsCurrent = 1),1)
             dim_producthierarchyid,
                          1 Dim_ProfitCenterid,
             1 Dim_SalesDivisionid,
             1 Dim_SalesGroupid,
             1 Dim_SalesOfficeid,
             1 Dim_SalesOrgid,
          ifnull((SELECT dim_shipreceivepointid
             FROM dim_shipreceivepoint srp
            WHERE srp.ShipReceivePointCode = VBRP_VSTEL),1)
             Dim_ShipReceivePointid,
            1 Dim_StorageLocationid,
             1 Dim_UnitOfMeasureId,
                  1 ,
          ifnull(VBRP_AUBEL, 'Not Set'),
          VBRP_AUPOS,
                 1 Dim_BillingCategoryId ,
                 1 Dim_RevenueRecognitionCategoryId,
       VBRP_WAVWR,
       VBRK_NETWR,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_NETWR,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5,
       (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6,
       ifnull(VBRK_BSTNK_VF,'Not Set'),
       ifnull(VBRK_ERNAM,'Not Set') dd_CreatedBy,
            (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_billing') + row_number() over (),
                       1 dim_currencyid_TRA,
                       1 dim_currencyid_GBL,
            1 Dim_MaterialPricingGroupId,
            1 Dim_PartSalesId


     FROM    VBRK_VBRP v, tmp_pGlobalCurrency_fact_billing, tmp_ins_fact_billing b
        WHERE b.dd_billing_no = VBRK_VBELN
        AND b.dd_billing_item_no = VBRP_POSNR;


update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_billingid),0) from fact_billing)
where table_name = 'fact_billing';




/* Create a combined lookup table so that same joins are not required to be repeated in similar queries */

DROP TABLE IF EXISTS TMP_BILLING_COMBINED_UPD;

CREATE TABLE TMP_BILLING_COMBINED_UPD
AS
SELECT * from VBRK_VBRP vkp
WHERE EXISTS ( select 1 from fact_billing fb where fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR );


/* Update 1 - Part 1 - Need to handle getExchangeRate */


/* Update 1 column 1 - ct_BillingQtySalesUOM */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET ct_BillingQtySalesUOM = ifnull(VBRP_FKLMG * (VBRP_UMVKN / VBRP_UMVKZ),0)
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(ct_BillingQtySalesUOM,-1) <> ifnull(VBRP_FKLMG * (VBRP_UMVKN / VBRP_UMVKZ),0);


/* Update 1 column 2 - ct_BillingQtyStockUOM - NULLABLE column ( hence the ifnull with <> condition ) */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET ct_BillingQtyStockUOM = VBRP_FKLMG
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(ct_BillingQtyStockUOM,-1) <> ifnull(VBRP_FKLMG,-1);


/* Update currency IDs */


UPDATE fact_billing fb
FROM dim_company dc,dim_currency cur,VBRK_VBRP vkp
SET fb.dim_CurrencyId = cur.dim_CurrencyID			  
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_CurrencyId <> cur.dim_CurrencyID;


UPDATE fact_billing fb
FROM VBRK_VBRP vkp,dim_currency tra
SET fb.dim_CurrencyId_TRA = tra.dim_currencyid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND tra.currencycode = VBRK_WAERK 
AND fb.dim_CurrencyId_TRA <> tra.dim_currencyid;



UPDATE fact_billing fb
FROM tmp_pGlobalCurrency_fact_billing t_exch,dim_currency gbl
SET fb.dim_CurrencyId_GBL = gbl.dim_currencyid
WHERE gbl.currencycode = pGlobalCurrency
AND fb.dim_CurrencyId_GBL <> gbl.dim_currencyid;

/* Update 1 column 3 - amt_ExchangeRate */


/* Now update the value where a match is found */
/* LK: 8 Sep: amt_ExchangeRate should have tran->local rate. Replaced z.pToCurrency = dc.currency ( from globalcurr ) */
UPDATE fact_billing fb
FROM dim_company dc,VBRK_VBRP vkp,
tmp_getExchangeRate1 z,dim_currency cur
SET amt_ExchangeRate = z.exchangeRate
		   /* getExchangeRate(VBRK_WAERK, pGlobalCurrency,VBRK_KURRF,   VBRK_FKDAT), */			  
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
AND cur.CurrencyCode = dc.currency
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND z.pFromCurrency  = VBRK_WAERK and z.pToCurrency = dc.currency AND z.pFromExchangeRate = VBRK_KURRF AND z.pDate = VBRK_FKDAT
and z.fact_script_name = 'bi_populate_billing_fact'
AND fb.amt_ExchangeRate <> z.exchangeRate;



/* Update 1 column 4 - amt_ExchangeRate_GBL */


UPDATE fact_billing fb
FROM VBRK_VBRP vkp,tmp_pGlobalCurrency_fact_billing t_exch,
	 tmp_getExchangeRate1 z
SET
 amt_ExchangeRate_GBL = z.exchangeRate   
        /*  getExchangeRate(dc.currency,pGlobalCurrency,VBRK_KURRF,VBRK_FKDAT),	*/
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND z.pFromCurrency  = VBRK_WAERK and z.pToCurrency = t_exch.pGlobalCurrency AND z.pDate = ANSIDATE(LOCAL_TIMESTAMP) AND pFromExchangeRate = 0
and z.fact_script_name = 'bi_populate_billing_fact'
AND fb.amt_ExchangeRate_GBL <> z.exchangeRate;


select 'Checkpoint B',TIMESTAMP(LOCAL_TIMESTAMP);

/* Update 1 column 5 - amt_CashDiscount */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
set
   amt_CashDiscount = VBRP_SKFBP
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND amt_CashDiscount <> VBRP_SKFBP;



/* Update 1 column 6 - dd_CancelledDocumentNo */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET
  dd_CancelledDocumentNo = ifnull(VBRK_SFAKN,'Not Set')
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND IFNULL(dd_CancelledDocumentNo,'xx') <> ifnull(VBRK_SFAKN,'Not Set');


/* Update 1 column 7 - dd_SalesDlvrItemNo */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET
 dd_SalesDlvrItemNo = VBRP_VGPOS
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND IFNULL(dd_SalesDlvrItemNo,-1) = IFNULL(VBRP_VGPOS,-2 );

/* Update 1 column 8 - dd_SalesDlvrDocNo */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET
 dd_SalesDlvrDocNo = ifnull(VBRP_VGBEL, 'Not Set')
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND IFNULL(dd_SalesDlvrDocNo,'xx') <> ifnull(VBRP_VGBEL, 'Not Set');

/* Update 1 column 9 - Dim_DateidBilling */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp,
dim_date dt
SET
Dim_DateidBilling = dt.dim_dateid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dt.DateValue = VBRK_FKDAT AND dt.CompanyCode = VBRK_BUKRS
AND Dim_DateidBilling <> dt.dim_dateid;

/* Update 1 column 10 - Dim_DateidCreated */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp,
dim_date dt
SET
       Dim_DateidCreated = dt.dim_dateid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dt.DateValue = VBRK_ERDAT AND dt.CompanyCode = VBRK_BUKRS
AND Dim_DateidCreated <> dt.dim_dateid;

select 'Checkpoint C',TIMESTAMP(LOCAL_TIMESTAMP);

/* Update 1 column 11 - Dim_DistributionChannelId */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp,
dim_distributionchannel ddc
SET
	   Dim_DistributionChannelId = ddc.Dim_DistributionChannelid

WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ddc.DistributionChannelCode = VBRK_VTWEG AND ddc.RowIsCurrent = 1
AND fb.Dim_DistributionChannelId <> ddc.Dim_DistributionChannelid;



/* Update 1 - Part 2 column 12 onwards - Need to handle WHERE binary dc1.DocumentCategory = VBRK_VBTYP
/* Also need to handle - ORDER BY pc.ValidTo ASC LIMIT 1 */


/* 2a */	

drop table if exists dim_profitcenter_fact_billing;
create table dim_profitcenter_fact_billing as Select first 0 * from dim_profitcenter  ORDER BY ValidTo ASC;



/* Update 1 - column 12 */


UPDATE fact_billing fb
FROM VBRK_VBRP vkp,dim_documentcategory dc1
SET        
       Dim_DocumentCategoryid = dc1.Dim_documentcategoryid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ltrim(rtrim(dc1.DocumentCategory )) = ltrim(rtrim(VBRK_VBTYP)) AND dc1.RowIsCurrent = 1
and fb.Dim_DocumentCategoryid <> dc1.Dim_documentcategoryid ;

	
/* 2b */	

/* Update 1 - column 13 */	

UPDATE fact_billing fb
FROM VBRK_VBRP vkp,
dim_billingdocumenttype bdt
SET  	
       Dim_DocumentTypeid = bdt.dim_billingdocumenttypeid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND bdt.Type = VBRK_FKART AND bdt.RowIsCurrent = 1
AND Dim_DocumentTypeid <> bdt.dim_billingdocumenttypeid;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp,
	dim_incoterm it
SET  	
       Dim_IncoTermid = it.dim_IncoTermId
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND it.IncoTermCode = VBRK_INCO1 AND it.RowIsCurrent = 1
AND IFNULL(fb.Dim_IncoTermid,-1) <> IFNULL( it.dim_IncoTermId,-2) ;

/* Update 1 - column 15 */	

UPDATE fact_billing fb
FROM VBRK_VBRP vkp,
	dim_materialgroup mg
SET  	
       Dim_MaterialGroupid = mg.dim_materialgroupid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND mg.MaterialGroupCode = VBRP_MATKL AND mg.RowIsCurrent = 1
AND IFNULL(fb.Dim_MaterialGroupid,-1) <> IFNULL(mg.dim_materialgroupid,-2) ;

/* Update 1 - column 16 */	
select 'Checkpoint D',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE fact_billing fb
FROM VBRK_VBRP vkp,
	dim_part p
SET  	
       fb.Dim_Partid = p.dim_partid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND p.PartNumber = VBRP_MATNR AND p.Plant = VBRP_WERKS AND p.RowIsCurrent = 1
AND IFNULL(fb.Dim_Partid,-1) <> IFNULL(p.dim_partid,-2);


call vectorwise(combine 'fact_billing');
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_billing ;

/* Update 1 - column 17 */	
UPDATE fact_billing fb
FROM dim_plant dp,VBRK_VBRP vkp
SET  	
 fb.Dim_Plantid = dp.dim_plantid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dp.PlantCode = vkp.VBRP_WERKS AND dp.RowIsCurrent = 1
AND IFNULL( fb.Dim_Plantid,-1 ) <> IFNULL(dp.dim_plantid,-2);

/* Update 1 - column 18 */	
UPDATE fact_billing fb
FROM VBRK_VBRP vkp,
	dim_producthierarchy ph
SET  	
	   Dim_ProductHierarchyid = ph.dim_producthierarchyid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ph.ProductHierarchy = VBRP_PRODH AND ph.RowIsCurrent = 1
AND IFNULL(fb.Dim_ProductHierarchyid,-1) <> IFNULL(ph.dim_producthierarchyid,-2);
	

/* 2c */	
	
/* Update 1 - column 19 */		
	
UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET  	
       Dim_ProfitCenterid =
          ifnull((SELECT pc.dim_profitcenterid
                      FROM dim_profitcenter_fact_billing pc
                      WHERE   pc.ProfitCenterCode = VBRP_PRCTR
                          AND pc.ControllingArea = VBRP_KOKRS
                          AND pc.ValidTo >= VBRK_FKDAT
                          AND pc.RowIsCurrent = 1 ),1)
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

/* End of Update 1 - Part 2 */

/* Update 1 - Part 3 : Tested */

/* Update 1 column 20 */


UPDATE fact_billing fb
 FROM VBRK_VBRP vkp
 ,dim_salesdivision sd
SET Dim_SalesDivisionid = sd.dim_salesdivisionid
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND sd.DivisionCode = VBRK_SPART   
AND IFNULL(fb.Dim_SalesDivisionid,-1) <> IFNULL(sd.dim_salesdivisionid,-2);

/* Update 1 column 21 - Dim_SalesGroupid */


UPDATE fact_billing fb
 FROM VBRK_VBRP vkp
 ,dim_salesgroup sg
SET Dim_SalesGroupid = sg.dim_salesgroupid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND sg.SalesGroupCode = VBRP_VKGRP 
AND IFNULL(fb.Dim_SalesGroupid,-1) <> IFNULL(sg.dim_salesgroupid,-2);

select 'Checkpoint E',TIMESTAMP(LOCAL_TIMESTAMP);

/* Update 1 column 22 - Dim_SalesOfficeid */


UPDATE fact_billing fb
 FROM VBRK_VBRP vkp
 ,dim_salesoffice so1
SET Dim_SalesOfficeid = so1.dim_salesofficeid
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND so1.SalesOfficeCode = VBRP_VKBUR 
AND IFNULL(fb.Dim_SalesOfficeid,-1) <> IFNULL(so1.dim_salesofficeid,-2);


/* Update 1 column 23 - Dim_SalesOrgid */
UPDATE fact_billing fb
FROM dim_salesorg so,VBRK_VBRP vkp
SET fb.Dim_SalesOrgid = ifnull(so.dim_salesorgid,1)
 WHERE so.SalesOrgCode = vkp.VBRK_VKORG
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 and IFNULL(fb.Dim_SalesOrgid,-1) <> ifnull(so.dim_salesorgid,1);
 
 
/* Update 1 column 24 - Dim_ControllingAreaid - NOT NULL column so IFNULL not used with the <> condition */ 
 
UPDATE fact_billing fb
FROM dim_controllingarea ca,VBRK_VBRP vkp
SET fb.Dim_ControllingAreaid = ca.dim_controllingareaid
WHERE  ca.ControllingAreaCode = vkp.VBRP_KOKRS
AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_ControllingAreaid <>  ca.dim_controllingareaid;


/* Update 1 column 25 - Dim_StorageLocationid */ 

/* Update column Dim_StorageLocationid */

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
 ,dim_storagelocation sl
SET Dim_StorageLocationid = sl.dim_storagelocationid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND  sl.LocationCode = VBRP_LGORT
                     AND sl.plant = VBRP_WERKS
                     AND sl.RowIsCurrent = 1
AND IFNULL(fb.Dim_StorageLocationid,-1) <> IFNULL(sl.dim_storagelocationid,-2);


/* Update column Dim_AccountingTransferStatusid */


UPDATE fact_billing fb
FROM VBRK_VBRP vkp
 ,dim_accountingtransferstatus ats
SET Dim_AccountingTransferStatusid = ats.dim_accountingtransferstatusid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ats.AccountingTransferStatusCode = VBRK_RFBSK
                     AND ats.RowIsCurrent = 1
AND IFNULL(fb.Dim_AccountingTransferStatusid,-1) <> IFNULL(ats.dim_accountingtransferstatusid,-2);


/* Update column fb.Dim_ConditionProcedureid */


UPDATE fact_billing fb
FROM VBRK_VBRP vkp
 ,dim_conditionprocedure dc1
SET fb.Dim_ConditionProcedureid = dc1.dim_conditionprocedureid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND dc1.Dim_ConditionProcedureid = VBRK_KALSM
                     AND dc1.RowIsCurrent = 1
AND IFNULL(fb.Dim_ConditionProcedureid,-1) <> IFNULL(dc1.dim_conditionprocedureid,-2);


/* Update column Dim_CreditControlAreaid */



UPDATE fact_billing fb
FROM VBRK_VBRP vkp ,dim_creditcontrolarea cca
SET Dim_CreditControlAreaid = cca.dim_creditcontrolareaid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND cca.CreditControlAreaName = VBRP_KOKRS
                     AND cca.RowIsCurrent = 1
AND IFNULL(fb.Dim_CreditControlAreaid,-1) <> IFNULL(cca.dim_creditcontrolareaid,-2);


/* Update column Dim_CustomerID */



UPDATE fact_billing fb
FROM VBRK_VBRP vkp ,dim_customer c
SET Dim_CustomerID = c.dim_customerid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND c.CustomerNumber = VBRK_KUNAG AND c.RowIsCurrent = 1
AND IFNULL(fb.Dim_CustomerID,-1) <> IFNULL(c.dim_customerid,-2);


/* Update column Dim_CustomerGroup1id */



UPDATE fact_billing fb
FROM VBRK_VBRP vkp ,dim_customergroup1 cg1
SET Dim_CustomerGroup1id = cg1.dim_customergroup1id
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND cg1.CustomerGroup = VBRK_KDGRP
AND IFNULL(fb.Dim_CustomerGroup1id,-1) <> IFNULL(cg1.dim_customergroup1id,-2);

/* Update column Dim_CustomerPaymentTermsid */


call vectorwise(combine 'fact_billing');
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_billing ;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp ,dim_customerpaymentterms cpt
SET Dim_CustomerPaymentTermsid = cpt.dim_customerpaymenttermsid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND cpt.PaymentTermCode = VBRK_ZTERM
AND cpt.RowIsCurrent = 1
AND IFNULL(fb.Dim_CustomerPaymentTermsid,-1) <> IFNULL(cpt.dim_customerpaymenttermsid,-2);


/* Update column fb.Dim_ShipReceivePointid */


select 'Checkpoint F',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE fact_billing fb
FROM VBRK_VBRP vkp ,dim_shipreceivepoint srp
SET fb.Dim_ShipReceivePointid = srp.dim_shipreceivepointid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND srp.ShipReceivePointCode = VBRP_VSTEL
AND IFNULL(fb.Dim_ShipReceivePointid,-1) <> IFNULL(srp.dim_shipreceivepointid,-2);


/* Update column fb.Dim_UnitOfMeasureId */



UPDATE fact_billing fb
FROM VBRK_VBRP vkp ,dim_unitofmeasure um
SET fb.Dim_UnitOfMeasureId = um.dim_unitofmeasureid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND um.UOM = VBRP_MEINS
AND IFNULL(fb.Dim_UnitOfMeasureId,-1) <> IFNULL(um.dim_unitofmeasureid,-2);


/* Update 1 - Part 4 : tested */
UPDATE fact_billing fb
FROM VBRK_VBRP vkp,dim_company dc
SET
  fb.Dim_Companyid = dc.Dim_Companyid
WHERE dc.CompanyCode = vkp.VBRK_BUKRS AND dc.RowIsCurrent = 1
 AND fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND   fb.Dim_Companyid <> dc.Dim_Companyid;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET fb.dd_fiscalyear = VBRK_GJAHR
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(fb.dd_fiscalyear,-1) <> ifnull(VBRK_GJAHR,-2);

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET fb.dd_postingperiod = VBRK_POPER
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(fb.dd_postingperiod,-1) <> ifnull(VBRK_POPER,-2);

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET fb.dd_salesitemno = VBRP_AUPOS
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(fb.dd_salesitemno,-1) <> ifnull(VBRP_AUPOS,-2);

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET amt_cost_InDocCurrency = VBRP_WAVWR
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(fb.amt_cost_InDocCurrency,-1) <> ifnull(VBRP_WAVWR,-2);

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET amt_NetValueHeader_InDocCurrency = VBRK_NETWR
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(fb.amt_NetValueHeader_InDocCurrency,-1) <> ifnull(VBRK_NETWR,-2);
 
UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET fb.dd_salesdocno = ifnull(VBRP_AUBEL, 'Not Set')
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dd_salesdocno <> ifnull(VBRP_AUBEL, 'Not Set');
 
 
/* Update column fb.dim_salesorderitemcategoryid */



UPDATE fact_billing fb
FROM VBRK_VBRP vkp ,dim_salesorderitemcategory soic
SET fb.dim_salesorderitemcategoryid = soic.dim_salesorderitemcategoryid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND soic.SalesOrderItemCategory = vkp.VBRP_PSTYV AND soic.RowIsCurrent = 1
AND IFNULL(fb.dim_salesorderitemcategoryid,-1) <> IFNULL(soic.dim_salesorderitemcategoryid,-2);


/* Update column fb.Dim_BillingCategoryId */



UPDATE fact_billing fb
FROM VBRK_VBRP vkp
 ,dim_billingcategory bc
SET fb.Dim_BillingCategoryId = bc.dim_billingcategoryid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND bc.Category = ifnull(VBRK_FKTYP,'Not Set')
                   AND bc.RowIsCurrent = 1
AND IFNULL(fb.Dim_BillingCategoryId,-1) <> IFNULL(bc.dim_billingcategoryid,-2);


/* Update column fb.Dim_RevenueRecognitionCategoryId */



UPDATE fact_billing fb
FROM VBRK_VBRP vkp
 ,dim_RevenueRecognitioncategory rrc
SET fb.Dim_RevenueRecognitionCategoryId = rrc.dim_RevenueRecognitioncategoryid
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND rrc.Category = ifnull(VBRP_RRREL,'Not Set') AND rrc.RowIsCurrent = 1
AND IFNULL(fb.Dim_RevenueRecognitionCategoryId,-1) <> IFNULL(rrc.dim_RevenueRecognitioncategoryid,-2);
 

select 'Checkpoint G',TIMESTAMP(LOCAL_TIMESTAMP);

/* Update 1 - Part 5 : tested*/

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET
        amt_NetValueItem_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_NETWR
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal1_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal2_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2
 WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal3_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal4_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal5_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET amt_CustomerConfigSubtotal6_InDocCurrency = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET
dd_CustomerPONumber = ifnull(VBRK_BSTNK_VF,'Not Set')
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
 AND ifnull(fb.dd_CustomerPONumber,'xx') <> ifnull(VBRK_BSTNK_VF,'Not Set');

UPDATE fact_billing fb
FROM VBRK_VBRP vkp
SET
dd_CreatedBy = ifnull(VBRK_ERNAM,'Not Set')
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
  AND ifnull(fb.dd_CreatedBy,'xx') <> ifnull(VBRK_ERNAM,'Not Set');

/* End of Update 1 */




select 'Checkpoint H',TIMESTAMP(LOCAL_TIMESTAMP);
/* Update 2  - RECURSIVE not required to be removed */

/* Update 3 */ 

UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET
 amt_cost = VBRP_WAVWR * amt_ExchangeRate
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(amt_cost,-1) <> ifnull(VBRP_WAVWR * amt_ExchangeRate,-2);
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_NetValueHeader = VBRK_NETWR /* * amt_ExchangeRate */
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(amt_NetValueHeader,-1) <> ifnull(VBRK_NETWR,-2);

 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_NetValueItem = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_NETWR /** amt_ExchangeRate*/
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(amt_NetValueItem,-1) <> (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) * VBRP_NETWR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal1 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI1  /** amt_ExchangeRate*/
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal2 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI2 /** amt_ExchangeRate*/
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal3 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3 /** amt_ExchangeRate*/
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND IFNULL(amt_CustomerConfigSubtotal3,-1) <> (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI3;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal4 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI4 /** amt_ExchangeRate*/
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal5 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5 /** amt_ExchangeRate*/
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(amt_CustomerConfigSubtotal5,-1) <> (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI5;
 
UPDATE    fact_billing fb
FROM vbrk_vbrp vkp
SET amt_CustomerConfigSubtotal6 = (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6 /** amt_ExchangeRate*/
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
 AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(amt_CustomerConfigSubtotal6,-1) <>  (CASE WHEN VBRP_SHKZG IS NOT NULL THEN -1 ELSE 1 END) *VBRP_KZWI6;

call vectorwise(combine 'fact_billing');
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_billing ;
 
 /* Update column Dim_MaterialPricingGroupId */

UPDATE fact_billing fb
SET fb.Dim_MaterialPricingGroupId = 1
WHERE IFNULL(fb.Dim_MaterialPricingGroupId,-1) <> 1;

UPDATE fact_billing fb
FROM VBRK_VBRP vkp,dim_MaterialPricingGroup mpg
SET fb.Dim_MaterialPricingGroupId = mpg.dim_MaterialPricingGroupId
WHERE fb.dd_billing_no = vkp.VBRK_VBELN AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND mpg.materialpricinggroupcode = ifnull(VBRP_UKONM,'Not Set')
AND mpg.RowIsCurrent = 1
AND IFNULL(fb.Dim_MaterialPricingGroupId,-1) <> IFNULL(mpg.dim_MaterialPricingGroupId,-2);


/*UPDATE fact_Billing fb
  FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm
   SET fb.dim_Customermastersalesid = cms.dim_Customermastersalesid
 WHERE     fb.dim_salesorgid = sorg.dim_salesorgid
       AND fb.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = fb.dim_salesdivisionid
       AND cm.dim_customerid = fb.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber*/

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid;
CREATE TABLE tmp_perf_fb_dim_Customermastersalesid
AS
SELECT DISTINCT fb.dim_salesorgid,fb.dim_distributionchannelid,fb.dim_salesdivisionid,fb.dim_customerid, cast(1 as integer) dim_Customermastersalesid
FROM fact_billing fb;

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid_2;
CREATE TABLE tmp_perf_fb_dim_Customermastersalesid_2
AS	   
SELECT  DISTINCT fb.dim_salesorgid,fb.dim_distributionchannelid,fb.dim_salesdivisionid,fb.dim_customerid,  cms.dim_Customermastersalesid dim_Customermastersalesid
FROM
       dim_Customermastersales cms,
       dim_salesorg sorg,
       dim_distributionchannel dc,
       dim_salesdivision sd,
       dim_customer cm,
	   tmp_perf_fb_dim_Customermastersalesid fb
 WHERE     fb.dim_salesorgid = sorg.dim_salesorgid
       AND fb.dim_distributionchannelid = dc.dim_distributionchannelid
       AND sd.dim_salesdivisionid = fb.dim_salesdivisionid
       AND cm.dim_customerid = fb.dim_customerid
       AND sorg.SalesOrgCode = cms.SalesOrg
       AND dc.distributionchannelcode = cms.distributionchannel
       AND sd.DivisionCode = cms.Divisioncode
       AND cm.customernumber = cms.CustomerNumber;	   
	   
UPDATE fact_Billing fb
  FROM tmp_perf_fb_dim_Customermastersalesid_2 upd
 SET fb.dim_Customermastersalesid = upd.dim_Customermastersalesid
 WHERE     fb.dim_salesorgid =  upd.dim_salesorgid
       AND fb.dim_distributionchannelid = upd.dim_distributionchannelid
       AND fb.dim_salesdivisionid = upd.dim_salesdivisionid
       AND fb.dim_customerid = upd.dim_customerid
AND fb.dim_Customermastersalesid <> upd.dim_Customermastersalesid; 

 
/* Update 4 */

/* Update 4 - Part 1 */   
/* Shanthi's queries from here */


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
   SET fb.Dim_SalesDocumentItemStatusId = ifnull(fso.dim_salesorderitemstatusid,1)
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND IFNULL(fb.Dim_SalesDocumentItemStatusId,-1) <> ifnull(fso.dim_salesorderitemstatusid,1);

UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
  SET  fb.Dim_so_CostCenterid = fso.Dim_CostCenterid
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND IFNULL(fb.Dim_so_CostCenterid,-1) <> IFNULL(fso.Dim_CostCenterid,-2);

UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
  set  fb.Dim_so_SalesDocumentTypeid = fso.Dim_SalesDocumentTypeid
   WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND IFNULL(fb.Dim_so_SalesDocumentTypeid,-1) <> IFNULL(fso.Dim_SalesDocumentTypeid,-2);


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_so_DateidSchedDelivery = fso.Dim_DateidSchedDelivery
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND IFNULL(fb.Dim_so_DateidSchedDelivery,-1) = IFNULL(fso.Dim_DateidSchedDelivery,-2);


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
 SET   fb.Dim_so_SalesOrderCreatedId = fso.Dim_DateidSalesOrderCreated
  WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND fb.Dim_so_SalesOrderCreatedId <> fso.Dim_DateidSalesOrderCreated;

	   
UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId = fso.Dim_CustomPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND fb.Dim_CustomPartnerFunctionId <> fso.Dim_CustomPartnerFunctionId;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_PayerPartnerFunctionId = fso.Dim_PayerPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND fb.Dim_PayerPartnerFunctionId <> fso.Dim_PayerPartnerFunctionId;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_BillToPartyPartnerFunctionId = fso.Dim_BillToPartyPartnerFunctionId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
	   AND fb.Dim_BillToPartyPartnerFunctionId <> fso.Dim_BillToPartyPartnerFunctionId;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomerGroupId = fso.Dim_CustomerGroupId
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(fb.Dim_CustomerGroupId,-1) <> fso.Dim_CustomerGroupId;


UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId1 = fso.Dim_CustomPartnerFunctionId1
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(fb.Dim_CustomPartnerFunctionId1,-1) <> fso.Dim_CustomPartnerFunctionId1;

UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomPartnerFunctionId2 = fso.Dim_CustomPartnerFunctionId2
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND ifnull(fb.Dim_CustomPartnerFunctionId2,-1) <> fso.Dim_CustomPartnerFunctionId2;

select 'Checkpoint I',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE fact_billing fb FROM
       fact_salesorder fso,
       vbrk_vbrp vkp
SET    fb.Dim_CustomeridShipTo = fso.Dim_CustomeridShipTo
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dd_billing_no = vkp.VBRK_VBELN
       AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.Dim_CustomeridShipTo <> fso.Dim_CustomeridShipTo;


DROP TABLE IF EXISTS tmp_fb_fact_salesorder;
create table tmp_fb_fact_salesorder
as
select f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,SUM(f_so.ct_ConfirmedQty) sum_ct_ConfirmedQty,SUM(f_so.ct_DeliveredQty) sum_ct_DeliveredQty,
SUM(f_so.ct_ScheduleQtySalesUnit) - SUM(f_so.ct_DeliveredQty ) as diff_sched_dlvrqty,
SUM( f_so.ct_ConfirmedQty * (f_so.amt_UnitPrice / (CASE WHEN f_so.ct_PriceUnit = 0 THEN NULL ELSE f_so.ct_PriceUnit END))) upd_amt_so_confirmed
FROM fact_salesorder f_so
group by f_so.dd_SalesDocNo,f_so.dd_SalesItemNo;

/* tmp_fb_fact_salesorder_a stores aggregate value diff_sched_dlvrqty grouped by doc,item. But it also has an additinoal column Dim_DocumentCategoryid
If there are no doc,item combinations in fso having different documentcategoryid, then rowcount in tmp_fb_fact_salesorder_a will be same as tmp_fb_fact_salesorder */
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_a;
create table tmp_fb_fact_salesorder_a
as
select DISTINCT fso.dd_SalesDocNo,fso.dd_SalesItemNo,fso.Dim_DocumentCategoryid,f_so.diff_sched_dlvrqty,f_so.upd_amt_so_confirmed
FROM tmp_fb_fact_salesorder f_so, fact_salesorder fso
WHERE fso.dd_SalesDocNo = f_so.dd_SalesDocNo
AND fso.dd_SalesItemNo = f_so.dd_SalesItemNo;

call vectorwise(combine 'tmp_fb_fact_salesorder');
call vectorwise(combine 'tmp_fb_fact_salesorder_a');

DROP TABLE IF EXISTS tmp_fb_amt_updates;
CREATE TABLE tmp_fb_amt_updates
AS
SELECT distinct fb.*
FROM fact_billing fb,dim_billingmisc bm,dim_billingdocumenttype bdt,vbrk_vbrp vkp
WHERE fb.dd_billing_no = vkp.VBRK_VBELN
AND fb.dd_billing_item_no = vkp.VBRP_POSNR
AND fb.dim_billingmiscid = bm.dim_billingmiscid
AND bm.CancelFlag = 'Not Set'
AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
AND bdt.type IN ('F1','F2','ZF2','ZF2C');

/* This is required for using combine*/
ALTER TABLE tmp_fb_amt_updates
ADD PRIMARY KEY (fact_billingid);


UPDATE tmp_fb_amt_updates fb
SET fb.ct_so_ConfirmedQty = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_so_UnitPrice = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.ct_so_DeliveredQty = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_so_Returned = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.ct_so_OpenOrderQty = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.ct_so_ReturnedQty = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_so_confirmed = 0;

UPDATE tmp_fb_amt_updates fb
SET fb.amt_sd_Cost = 0;

UPDATE tmp_fb_amt_updates fb FROM
       tmp_fb_fact_salesorder fso
SET    fb.ct_so_ConfirmedQty = fso.sum_ct_ConfirmedQty 
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND fb.ct_so_ConfirmedQty <> fso.sum_ct_ConfirmedQty;


UPDATE tmp_fb_amt_updates fb FROM
       fact_salesorder fso
SET    fb.amt_so_UnitPrice = fso.amt_UnitPrice
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND fb.amt_so_UnitPrice <> fso.amt_UnitPrice;

   
UPDATE tmp_fb_amt_updates fb FROM
       tmp_fb_fact_salesorder fso
SET       fb.ct_so_DeliveredQty = fso.sum_ct_DeliveredQty
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND fb.ct_so_DeliveredQty <> fso.sum_ct_DeliveredQty;



DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2;
create table tmp_fb_fact_salesorder_2
as
/* select f_so.dd_SalesDocNo,f_so.dd_SalesItemNo, SUM(f_so.amt_ScheduleTotal) sum_amt_ScheduleTotal, SUM(f_so.ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit, SUM(f_so.ct_ScheduleQtySalesUnit) - SUM(f_so.ct_DeliveredQty ) as diff_sched_dlvrqt */
select f_so.dd_SalesDocNo,f_so.dd_SalesItemNo,SUM(f_so.ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit, SUM(f_so.amt_ScheduleTotal) sum_amt_ScheduleTotal
FROM fact_salesorder f_so, Dim_documentcategory dc 
WHERE dc.Dim_DocumentCategoryid = f_so.Dim_DocumentCategoryid
AND dc.RowIsCurrent = 1
AND dc.DocumentCategory IN ('H', 'K')
AND f_so.Dim_SalesOrderRejectReasonid <> 1
group by f_so.dd_SalesDocNo,f_so.dd_SalesItemNo;


DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2a;
create table tmp_fb_fact_salesorder_2a
as
SELECT DISTINCT fso.dd_SalesDocNo,fso.dd_SalesItemNo,fso.Dim_DocumentCategoryid,f_so.sum_ct_ScheduleQtySalesUnit
FROM tmp_fb_fact_salesorder_2 f_so, fact_salesorder fso
WHERE fso.dd_SalesDocNo = f_so.dd_SalesDocNo
AND fso.dd_SalesItemNo = f_so.dd_SalesItemNo;

call vectorwise(combine 'tmp_fb_fact_salesorder_2');
call vectorwise(combine 'tmp_fb_fact_salesorder_2a');



UPDATE tmp_fb_amt_updates fb FROM
       tmp_fb_fact_salesorder_2 fso
SET       fb.amt_so_Returned = fso.sum_amt_ScheduleTotal
WHERE fso.dd_SalesDocNo = fb.dd_salesdocno
AND fso.dd_SalesItemNo = fb.dd_salesitemno
AND fb.amt_so_Returned <> fso.sum_amt_ScheduleTotal;     


   
UPDATE tmp_fb_amt_updates fb FROM
       tmp_fb_fact_salesorder_a fso,Dim_documentcategory dc
 SET      fb.ct_so_OpenOrderQty = fso.diff_sched_dlvrqty
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
AND dc.RowIsCurrent = 1
AND fb.ct_so_OpenOrderQty <> fso.diff_sched_dlvrqty;


UPDATE tmp_fb_amt_updates fb FROM
	tmp_fb_fact_salesorder_2 fso
SET    fb.ct_so_ReturnedQty = fso.sum_ct_ScheduleQtySalesUnit
WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
AND fb.dd_salesitemno = fso.dd_SalesItemNo
AND fb.ct_so_ReturnedQty <> fso.sum_ct_ScheduleQtySalesUnit;


UPDATE tmp_fb_amt_updates fb FROM
	fact_salesorder fso,Dim_documentcategory dc
SET    fb.amt_revenue =
          (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
      WHEN '5' THEN 0
      WHEN '6' THEN 0
      WHEN 'N' THEN (-1 * fb.amt_NetValueItem)
      WHEN 'O' THEN (-1 * fb.amt_NetValueItem)
              ELSE fb.amt_NetValueItem
           END)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1;


UPDATE tmp_fb_amt_updates fb FROM
        fact_salesorder fso,Dim_documentcategory dc
SET    fb.amt_Revenue_InDocCurrency = (CASE dc.DocumentCategory
              WHEN 'U' THEN 0
      WHEN '5' THEN 0
      WHEN '6' THEN 0
      WHEN 'N' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
      WHEN 'O' THEN (-1 * fb.amt_NetValueItem_InDocCurrency)
              ELSE fb.amt_NetValueItem_InDocCurrency
           END)
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1;

call vectorwise(combine 'tmp_fb_amt_updates');
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_fb_amt_updates ;


UPDATE tmp_fb_amt_updates fb FROM
       tmp_fb_fact_salesorder_a fso,
       Dim_documentcategory dc
 SET   fb.amt_so_confirmed = fso.upd_amt_so_confirmed
 WHERE     fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND dc.Dim_DocumentCategoryid = fso.Dim_DocumentCategoryid
       AND dc.RowIsCurrent = 1
AND fb.amt_so_confirmed <> fso.upd_amt_so_confirmed;


DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;

CREATE TABLE tmp_openorder_from_Sales_to_billing AS
SELECT f_so.dd_SalesDocNo, f_so.dd_salesitemno,SUM(f_so.ct_ScheduleQtySalesUnit) as orderqty,SUM(f_so.ct_DeliveredQty) as DlvrQty, AVG(f_so.amt_UnitPrice) as TotalUnitPrice, ifnull(avg(f_so.ct_PriceUnit),0) as PriceUnit from fact_salesorder f_so
group by f_so.dd_SalesDocNo,f_so.dd_salesitemno;

UPDATE tmp_fb_amt_updates fb FROM
           tmp_openorder_from_Sales_to_billing t
SET       fb.amt_so_openorder =
          ((t.orderqty - t.DlvrQty)* t.TotalUnitPrice /(CASE WHEN t.PriceUnit = 0 THEN 1 ELSE t.PriceUnit END))
 WHERE t.dd_salesDocNo = fb.dd_Salesdocno
       AND t.dd_SalesItemNo = fb.dd_SalesItemNo;

DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;

select 'Checkpoint J',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS tmp_fb_fact_salesorderdelivery;
CREATE TABLE tmp_fb_fact_salesorderdelivery
AS
SELECT f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo,sum(f_sod.amt_Cost) as sum_amt_Cost
FROM fact_salesorderdelivery f_sod
GROUP BY f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo;

UPDATE tmp_fb_amt_updates fb
 from tmp_fb_fact_salesorderdelivery f_sod
SET fb.amt_sd_Cost = f_sod.sum_amt_Cost
WHERE f_sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
  AND f_sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo
AND fb.amt_sd_Cost <> f_sod.sum_amt_Cost;

UPDATE tmp_fb_amt_updates fb
FROM fact_salesorderdelivery sod
 SET fb.Dim_sd_DateidActualGoodsIssue = sod.Dim_DateidActualGoodsIssue
WHERE sod.dd_SalesDlvrDocNo = fb.dd_SalesDlvrDocNo
AND sod.dd_SalesDlvrItemNo = fb.dd_SalesDlvrItemNo
AND ifnull(fb.Dim_sd_DateidActualGoodsIssue,-1) <> sod.Dim_DateidActualGoodsIssue;

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;

CREATE TABLE tmp_openorder_from_Shipment_to_billing AS
SELECT f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo,SUM(f_sod.ct_QtyDelivered) as dlvrqty,AVG(f_sod.amt_UnitPrice) as AmtUnitPrice,ifnull(avg(f_sod.ct_PriceUnit),0) as PriceUnit from fact_salesorderdelivery f_sod
group by f_sod.dd_SalesDlvrDocNo, f_sod.dd_SalesDlvrItemNo;


  UPDATE tmp_fb_amt_updates fb
  from tmp_openorder_from_Shipment_to_billing t
    SET fb.amt_sd_Shipped = (t.dlvrqty * t.AmtUnitPrice/(case when t.PriceUnit = 0 then null else t.PriceUnit end ))
  WHERE     fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo;
   
   
  UPDATE tmp_fb_amt_updates fb
  from tmp_openorder_from_Shipment_to_billing t
    SET fb.ct_sd_QtyDelivered = t.dlvrqty
  WHERE     fb.dd_SalesDlvrDocNo = t.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = t.dd_SalesDlvrItemNo;

DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;

DROP table if exists tmp_SalesOrderAmtForBilling;

call vectorwise(combine 'tmp_fb_amt_updates');

/*PK on any 1 column is mandatory*/
call vectorwise(combine 'fact_billing-tmp_fb_amt_updates+tmp_fb_amt_updates');


  CREATE TABLE tmp_SalesOrderAmtForBilling
  AS
  SELECT dd_billing_no,dd_billing_item_no,dt.datevalue as billingdate,so.dd_SalesDocNo,so.dd_salesItemno,AVG(fb.ct_sd_QtyDelivered) as sd_QtyDelivered,AVG(fb.amt_sd_Shipped) as sd_shipped,sum(so.ct_ScheduleQtySalesUnit) as totalorderqty,sum(so.amt_ScheduleTotal) as totalorderamt,000000000000000000.0000 as acttotalqty, 000000000000000000.0000 as acttotalamt
  FROM fact_billing fb, fact_salesorder so,dim_date dt, dim_billingdocumenttype bdt,
         dim_billingmisc bm
   WHERE fb.dd_SalesDocNo = so.dd_salesdocno
  and fb.dd_salesitemno = so.dd_SalesItemNo
  and fb.Dim_DateidBilling = dt.dim_dateid
  and fb.dd_Salesdocno <> 'Not Set'
  AND fb.dim_billingmiscid = bm.dim_billingmiscid
  AND bm.CancelFlag = 'Not Set'
  AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
  AND bdt.type IN ('F1','F2','ZF2','ZF2C')
  group by dd_billing_no,dd_billing_item_no,dt.datevalue,so.dd_SalesDocNo,so.dd_salesItemno;

  UPDATE tmp_SalesOrderAmtForBilling
  set acttotalamt =  sd_Shipped,
    acttotalqty =  sd_QtyDelivered
  where totalorderamt > sd_Shipped;

  DROP table if exists tmp_SalesOrderAmtForBilling_100;

  CREATE TABLE tmp_SalesOrderAmtForBilling_100
  AS SELECT dd_salesDocNo,dd_salesitemno, max(billingdate) as billdate,sum(acttotalamt) as ActualAmt,sum(acttotalqty) as ActualQty from
  tmp_SalesOrderAmtForBilling group by dd_salesDocNo,dd_salesitemno;

  UPDATE tmp_SalesOrderAmtForBilling t1 FROM
    tmp_SalesOrderAmtForBilling_100 t2
  set t1.acttotalamt = t1.acttotalamt + (t1.TotalOrderamt - t2.ActualAmt)
  where t1.dd_Salesdocno = t2.dd_salesdocno
    AND t1.dd_salesitemno = t2.dd_Salesitemno
    and t1.billingdate = t2.billdate
    and t1.totalorderamt > t2.ActualAmt
AND ifnull(t1.acttotalamt,-1) <> t1.acttotalamt + (t1.TotalOrderamt - t2.ActualAmt);

  UPDATE tmp_SalesOrderAmtForBilling t1 FROM
    tmp_SalesOrderAmtForBilling_100 t2
      SET t1.acttotalqty = t1.acttotalqty + (t1.Totalorderqty - t2.actualqty)
  where t1.dd_Salesdocno = t2.dd_salesdocno
    AND t1.dd_salesitemno = t2.dd_Salesitemno
    and t1.billingdate = t2.billdate
    and t1.totalorderamt > t2.ActualAmt
AND ifnull(t1.acttotalqty,-1) <>  t1.acttotalqty + (t1.Totalorderqty - t2.actualqty);


  DROP table if exists tmp_SalesOrderAmtForBilling_100;

  UPDATE fact_billing fb
  from tmp_SalesOrderAmtForBilling tmp
  set fb.amt_so_ScheduleTotal = tmp.acttotalamt
  WHERE fb.dd_billing_no = tmp.dd_billing_no
  and fb.dd_billing_item_no = tmp.dd_billing_item_no
  AND fb.amt_so_ScheduleTotal <> tmp.acttotalamt
  AND IFNULL(fb.amt_so_ScheduleTotal,-1) = IFNULL(tmp.acttotalamt,-2);
  
  UPDATE fact_billing fb
  from tmp_SalesOrderAmtForBilling tmp
  set 
      fb.ct_so_ScheduleQtySalesUnit = tmp.acttotalqty
  WHERE fb.dd_billing_no = tmp.dd_billing_no
  and fb.dd_billing_item_no = tmp.dd_billing_item_no
  AND IFNULL(fb.ct_so_ScheduleQtySalesUnit,-1) <> IFNULL(tmp.acttotalqty,-2);  

select 'Checkpoint K',TIMESTAMP(LOCAL_TIMESTAMP);

  DROP table if exists tmp_SalesOrderAmtForBilling;
  
  UPDATE fact_salesorderdelivery sod
  FROM fact_billing fb,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm
    SET sod.dd_billing_no = fb.dd_billing_no
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
        AND bdt.type IN ('F1','F2','ZF2','ZF2C')
AND bm.dim_billingmiscid = fb.dim_billingmiscid
AND bm.CancelFlag = 'Not Set'
AND sod.dd_billing_no <> fb.dd_billing_no;  
  
  UPDATE fact_salesorderdelivery sod
  FROM fact_billing fb,
       dim_billingdocumenttype bdt,
       dim_billingmisc bm
    SET sod.dd_billingitem_no = fb.dd_billing_item_no
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND bdt.dim_billingdocumenttypeid = fb.dim_documenttypeid
        AND bdt.type IN ('F1','F2','ZF2','ZF2C')
AND bm.dim_billingmiscid = fb.dim_billingmiscid
AND bm.CancelFlag = 'Not Set'
AND sod.dd_billingitem_no <> fb.dd_billing_item_no;

DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid;
DROP TABLE IF EXISTS tmp_perf_fb_dim_Customermastersalesid_2;
DROP TABLE IF EXISTS tmp_pGlobalCurrency_fact_billing;
drop table if exists dim_profitcenter_fact_billing;
DROP TABLE IF EXISTS tmp_db_fb1 ;
DROP TABLE IF EXISTS tmp_db_fb2;
DROP TABLE IF EXISTS tmp_ins_fact_billing;
DROP TABLE IF EXISTS tmp_del_fact_billing;
DROP TABLE IF EXISTS tmp_fact_billing_VBRK_VBRP;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_a;
DROP TABLE IF EXISTS tmp_fb_amt_updates;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2;
DROP TABLE IF EXISTS tmp_fb_fact_salesorder_2a;
DROP TABLE IF EXISTS tmp_openorder_from_Sales_to_billing;
DROP TABLE IF EXISTS tmp_fb_fact_salesorderdelivery;
DROP TABLE IF EXISTS tmp_openorder_from_Shipment_to_billing;
DROP table if exists tmp_SalesOrderAmtForBilling;
  DROP table if exists tmp_SalesOrderAmtForBilling_100;

/* Start Changes 03 Feb 2014 */

UPDATE fact_billing fb FROM
       fact_salesorder fso
SET fb.dim_materialpricegroup4id= ifnull(fso.dim_materialpricegroup4id,1)
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_materialpricegroup4id <> ifnull(fso.dim_materialpricegroup4id,1);

UPDATE fact_billing fb FROM
       fact_salesorder fso
SET fb.dim_materialpricegroup5id= ifnull(fso.dim_materialpricegroup5id,1)
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_materialpricegroup5id <> ifnull(fso.dim_materialpricegroup5id,1);

UPDATE fact_billing fb FROM
       fact_salesorder fso
SET fb.dim_CustomerConditionGroups1id = ifnull(fso.dim_CustomerConditionGroups1id,1)
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_CustomerConditionGroups1id <> ifnull(fso.dim_CustomerConditionGroups1id,1);

UPDATE fact_billing fb FROM
       fact_salesorder fso
SET  fb.dim_CustomerConditionGroups2id= ifnull(fso.dim_CustomerConditionGroups2id,1)
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_CustomerConditionGroups2id <> ifnull(fso.dim_CustomerConditionGroups2id,1);

UPDATE fact_billing fb FROM
       fact_salesorder fso
SET  fb.dim_CustomerConditionGroups3id= ifnull(fso.dim_CustomerConditionGroups3id,1)
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND fb.dim_CustomerConditionGroups3id <> ifnull(fso.dim_CustomerConditionGroups3id,1)
AND fb.dim_CustomerConditionGroups3id <> ifnull(fso.dim_CustomerConditionGroups3id,1);


/* End Changes 03 Feb 2014 */

/* Start Changes 14 Feb 2014 */

UPDATE fact_billing fb FROM
       fact_salesorder fso
SET fb.Dim_CustomerGroup4id= ifnull(fso.Dim_CustomerGroup4id,1)
WHERE  fb.dd_salesdocno = fso.dd_SalesDocNo
       AND fb.dd_salesitemno = fso.dd_SalesItemNo
       AND ifnull(fb.Dim_CustomerGroup4id,-1) <> ifnull(fso.Dim_CustomerGroup4id,-2);


/* END Changes 14 Feb 2014 */

/* Update dim_partsalesid */

UPDATE fact_billing fb
FROM dim_partsales ps, dim_part dp, dim_salesorg so, dim_distributionchannel dc
SET fb.dim_partsalesid = ps.dim_partsalesid
WHERE fb.dim_partid = dp.dim_partid and dp.partnumber = ps.partnumber
	AND fb.dim_salesorgid = so.dim_salesorgid and so.salesorgcode = ps.salesorgcode
	AND fb.dim_distributionchannelid = dc.dim_distributionchannelid and dc.distributionchannelcode = ps.distributionchannelcode
AND IFNULL(fb.dim_partsalesid,1) <> ps.dim_partsalesid;

update fact_billing set dim_partsalesid = 1 where dim_partsalesid is NULL;
/* End Update dim_partsalesid */

call vectorwise(combine 'fact_billing');
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_billing ;

select 'END OF PROC bi_populate_billing_fact',TIMESTAMP(LOCAL_TIMESTAMP);
  
	   
