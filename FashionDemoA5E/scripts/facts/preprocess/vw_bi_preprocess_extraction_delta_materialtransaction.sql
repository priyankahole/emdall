Insert into KONH_KONP ( KONH_KNUMH,
				KONH_ERDAT,
				KONH_DATAB,
				KONH_DATBI,
				KONH_KOSRT,
				KONH_KSCHL,
				KONP_KOPOS,
				KONP_KBETR)
SELECT DISTINCT KONH_KNUMH,
				KONH_ERDAT,
				KONH_DATAB,
				KONH_DATBI,
				KONH_KOSRT,
				KONH_KSCHL,
				KONP_KOPOS,
				KONP_KBETR
	FROM DELTA_KONH_KONP de
				WHERE NOT EXISTS
                 (SELECT 1
                    FROM KONH_KONP e
                   WHERE de.KONH_KNUMH = e.KONH_KNUMH
                         AND de.KONP_KOPOS = e.KONP_KOPOS);
						 
CALL vectorwise(combine 'KONH_KONP');