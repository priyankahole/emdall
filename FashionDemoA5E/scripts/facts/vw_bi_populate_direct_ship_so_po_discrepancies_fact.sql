/* *********************************************************************************************************************** */
/*   Script         : vw_bi_populate_direct_ship_so_po_discrepancies_fact */
/*   Author         : George I. */
/*   Created On     : 17 Nov 2013 */
/*  */
/*  */
/*   Description    : vw_bi_populate_direct_ship_so_po_discrepancies_fact VW script*/
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/* ************************************************************************************************************************ */

DELETE FROM fact_directshipsopodiscrepancies;

delete from NUMBER_FOUNTAIN where table_name = 'fact_directshipsopodiscrepancies';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_directshipsopodiscrepancies',ifnull(max(fact_directshipsopodiscrepanciesid),0)
FROM 	fact_directshipsopodiscrepancies;

delete from NUMBER_FOUNTAIN where table_name = 'processinglog';

INSERT INTO NUMBER_FOUNTAIN
select 'processinglog',ifnull(max(processinglogid),0)
FROM processinglog;

INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'vw_bi_populate_direct_ship_so_po_discrepancies_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'vw_bi_populate_direct_ship_so_po_discrepancies_fact START',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

drop table if exists fact_directshipsopodiscrepancies_t10;
create table fact_directshipsopodiscrepancies_t10 as select a.*
from fact_directshipsopodiscrepancies a;

  INSERT INTO fact_directshipsopodiscrepancies_t10 (Dim_CompanyId,
											Dim_CustomerID,
											Dim_SalesDocumentTypeid,
											dd_SalesDocNo,
											dd_SalesItemNo,
											dd_ScheduleNo,
											dim_plantid,
											Dim_Partid,
											Dim_UnitOfMeasureId,
											dd_DocumentNo,
											dd_DocumentItemNo,
											dd_POScheduleLineNo,
											Dim_DateidSchedDeliveryReq,
											ct_SalesOrderQuantity,
											ct_PurchaseOrderQuantity,
											ct_QuantityDifference,
											Dim_Currencyid,
											Dim_CurrencyId_TRA,
									        Dim_CurrencyId_GBL,
											amt_ExchangeRate,
											amt_ExchangeRate_gbl,
											fact_directshipsopodiscrepanciesid)

select 1 Dim_CompanyId,
	   1 Dim_CustomerID,
	   1 Dim_SalesDocumentTypeid,
	   ifnull(VBAK_VBELN,'Not Set') dd_SalesDocNo,
	   ifnull(VBAP_POSNR,'Not Set') dd_SalesItemNo,
	   ifnull(VBEP_ETENR,'Not Set') dd_ScheduleNo,
	   1 dim_plantid,
	   1 Dim_Partid,
	   /*ifnull(VBEP_J_3ASIZE,'Not Set') dd_Grid,
	   ifnull(VBEP_J_4KRCAT,'Not Set') dd_StockCategory,*/
	   1 Dim_UnitOfMeasureId,
	   ifnull(EKPO_EBELN,'Not Set') dd_DocumentNo,
	   ifnull(EKPO_EBELP,1) dd_DocumentItemNo,
	   ifnull(EKET_ETENR,1) dd_POScheduleLineNo,
	   1 Dim_DateidSchedDeliveryReq,
	   ifnull(a.VBEP_WMENG,0) ct_SalesOrderQuantity,
	   ifnull(b.EKET_MENGE,0) ct_PurchaseOrderQuantity,
	   0 ct_QuantityDifference,
	   1 Dim_Currencyid,
	   1 Dim_CurrencyId_TRA,
	   1 Dim_CurrencyId_GBL,
	   0 amt_ExchangeRate,
	   0 amt_ExchangeRate_gbl,
	   (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_directshipsopodiscrepancies') + row_number() over()

from vbak_vbap_vbep a, ekko_ekpo_eket b, EBAN c, VBUK d
WHERE a.VBEP_BANFN /*to be added */ = c.BANFN
  AND a.VBEP_BNFPO /*to be added */ = c.BNFPO
  AND c.EBAN_EBELN /*to be added*/ = b.EKPO_EBELN
  AND b.EKPO_EBELP = c.EBAN_EBELP
  AND a.VBEP_ETENR = b.EKET_J_3AETENR /*to be added*/
  AND a.VBAK_VBELN = d.VBUK_VBELN
  AND a.VBAP_PSTYV = 'ZTAS'
  AND d.VBUK_GBSTK <> 'C'
  AND c.LOEKZ <> 'X'
  AND b.EKKO_LOEKZ <> 'L'
  AND b.EKET_J_3AELIKZ = 'X' AND EKET_WEMNG = 0;

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'vw_bi_populate_direct_ship_so_po_discrepancies_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'INSERT INTO fact_directshipsopodiscrepancies_t10',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	  
  
  UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, ekko_ekpo_eket e
  set Dim_Companyid = ifnull((SELECT Dim_Companyid FROM Dim_Company dc WHERE dc.CompanyCode = e.EKKO_BUKRS),1)
  WHERE f.dd_DocumentNo = e.EKPO_EBELN
  AND f.dd_DocumentItemNo = e.EKPO_EBELP;
  
  UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, vbak_vbap_vbep v
  set Dim_CustomerID =  ifnull((SELECT Dim_CustomerID FROM Dim_Customer cust WHERE cust.CustomerNumber = v.vbak_kunnr),1) 
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN
  AND f.dd_SalesItemNo = v.VBAP_POSNR
  AND f.dd_ScheduleNo = v.VBEP_ETENR;
    
  UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, vbak_vbap_vbep v
  set Dim_SalesDocumentTypeid = ifnull((SELECT Dim_SalesDocumentTypeid FROM Dim_SalesDocumentType sdt WHERE sdt.DocumentType = v.vbak_auart),1)
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN
  AND f.dd_SalesItemNo = v.VBAP_POSNR
  AND f.dd_ScheduleNo = v.VBEP_ETENR;
  
  UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, vbak_vbap_vbep v
  set dim_plantid = ifnull((SELECT dim_plantid FROM Dim_Plant dp WHERE dp.PlantCode = v.VBAP_WERKS),1)
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN
  AND f.dd_SalesItemNo = VBAP_POSNR
  AND f.dd_ScheduleNo = VBEP_ETENR;
	
  UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, vbak_vbap_vbep v 	
  set Dim_Partid = ifnull((SELECT dim_partid FROM Dim_Part dp WHERE dp.PartNumber = v.VBAP_MATNR),1)
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN
  AND f.dd_SalesItemNo = v.VBAP_POSNR
  AND f.dd_ScheduleNo = v.VBEP_ETENR;
  
  UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, vbak_vbap_vbep v 	
  set Dim_UnitOfMeasureId = ifnull((SELECT Dim_UnitOfMeasureId FROM Dim_UnitOfMeasure du WHERE du.UOM = v.VBAP_MEINS),1)
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN
  AND f.dd_SalesItemNo = v.VBAP_POSNR
  AND f.dd_ScheduleNo = v.VBEP_ETENR;  
  
  UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, vbak_vbap_vbep v, Dim_Company dcm
  set Dim_DateidSchedDeliveryReq = ifnull((select dt.Dim_Dateid from dim_date dt where dt.DateValue = v.VBAK_VDATU and dt.CompanyCode = dcm.CompanyCode ),1)
  WHERE f.dd_SalesDocNo = v.VBAK_VBELN
  AND f.dd_SalesItemNo = v.VBAP_POSNR
  AND f.dd_ScheduleNo = v.VBEP_ETENR
  AND dcm.Dim_CompanyId = f.Dim_CompanyId;
  
  /*UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, ekko_ekpo_eket e, Dim_Company dcm
  set Dim_PurchaseOrExFactoryDateid = ifnull((select dt.Dim_Dateid from dim_date dt where dt.DateValue = e.EKPO_J_3AEXFCM and dt.CompanyCode = dcm.CompanyCode),1)
  WHERE f.dd_DocumentNo = e.EKPO_EBELN
  AND f.dd_DocumentItemNo = e.EKPO_EBELP
  AND dcm.Dim_CompanyId = f.Dim_CompanyId;*/
  
  UPDATE fact_directshipsopodiscrepancies_t10
  from fact_directshipsopodiscrepancies_t10 f, fact_directshipsopodiscrepancies_t10 e
  set ct_QuantityDifference = e.ct_PurchaseOrderQuantity - e.ct_SalesOrderQuantity
  WHERE f.fact_directshipsopodiscrepanciesid = e.fact_directshipsopodiscrepanciesid;
  
  
INSERT INTO processinglog (referencename, startdate, description,processinglogid) 
SELECT 'vw_bi_populate_direct_ship_so_po_discrepancies_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'UPDATE fact_directshipsopodiscrepancies',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1;

update NUMBER_FOUNTAIN set max_id = ( select max(processinglogid) from processinglog)
where table_name = 'processinglog';

INSERT INTO fact_directshipsopodiscrepancies (Dim_CompanyId,
											Dim_CustomerID,
											Dim_SalesDocumentTypeid,
											dd_SalesDocNo,
											dd_SalesItemNo,
											dd_ScheduleNo,
											dim_plantid,
											Dim_Partid,
											Dim_UnitOfMeasureId,
											dd_DocumentNo,
											dd_DocumentItemNo,
											Dim_DateidSchedDeliveryReq,
											ct_SalesOrderQuantity,
											ct_PurchaseOrderQuantity,
											ct_QuantityDifference,
											fact_directshipsopodiscrepanciesid)

SELECT 										Dim_CompanyId,
											Dim_CustomerID,
											Dim_SalesDocumentTypeid,
											dd_SalesDocNo,
											dd_SalesItemNo,
											dd_ScheduleNo,
											dim_plantid,
											Dim_Partid,
											Dim_UnitOfMeasureId,
											dd_DocumentNo,
											dd_DocumentItemNo,
											Dim_DateidSchedDeliveryReq,
											ct_SalesOrderQuantity,
											ct_PurchaseOrderQuantity,
											ct_QuantityDifference,
											fact_directshipsopodiscrepanciesid
from fact_directshipsopodiscrepancies_t10;

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'vw_bi_populate_direct_ship_so_po_discrepancies_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'INSERT INTO fact_directshipsopodiscrepancies',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';	  
  
drop table if exists fact_directshipsopodiscrepancies_t10;

/*  ------------------- */
/* VAS */
/*
drop table if exists fact_directshipsopodiscrepancies_VAS_t1;
create table fact_directshipsopodiscrepancies_VAS_t1 as
Select b.J_3AVATL_BELNR, b.J_3AVATL_POSNR, b.J_3AVATL_ETENR, b.J_3AVATL_J_3ATLTYP, b.J_3AVATL_J_3AFIELD1, b.J_3AVATL_J_3AFIELD2, b.J_3AVATL_J_3AFIELD3
from vbak_vbap_vbep a, J_3AVATL b
where a.VBEP_ETENR = b.J_3AVATL_ETENR
AND a.VBAP_POSNR = b.J_3AVATL.POSNR;

drop table if exists fact_directshipsopodiscrepancies_VAS_t2;
create table fact_directshipsopodiscrepancies_VAS_t2 as
select b.J_3AVAP_BELNR, b.J_3AVAP_POSNR, b.J_3AVAP_ETENR, b.J_3AVAP_J_3ATLTYP, b.J_3AVAP_J_3AFIELD1, b.J_3AVAP_J_3AFIELD2, b.J_3AVAP_J_3AFIELD3
from ekko_ekpo_eket a, J_3AVAP b
where a.EKET_ETENR = b.J_3AVAP_ETENR
AND a.EKKO_ADDNR = b.J_3AVAP.BELNR;


drop table if exists fact_directshipsopodiscrepancies_VAS_t3;
create table fact_directshipsopodiscrepancies_VAS_t3 as
select count(a.VBAP_POSNR) 

from vbak_vbap_vbep a, ekko_ekpo_eket b, EBAN c
WHERE a.VBEP_BANFN = c.BANFN
  AND a.VBEP_BNFPO = c.BNFPO
  AND c.EBAN_EBELN  = b.EKPO_EBELN
  AND b.EKET_EBELP  = c.EBAN_EBELP
  AND a.VBEP_ETENR = b.EKET_J_3AETENR

  AND a.VBEP_ETENE is NULL
*/

INSERT INTO processinglog (referencename, enddate, description,processinglogid) 
SELECT 'vw_bi_populate_direct_ship_so_po_discrepancies_fact',TIMESTAMP(LOCAL_TIMESTAMP), 'vw_bi_populate_direct_ship_so_po_discrepancies_fact END',
(SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'processinglog') + 1 ;

update NUMBER_FOUNTAIN set max_id = max_id + 1
where table_name = 'processinglog';