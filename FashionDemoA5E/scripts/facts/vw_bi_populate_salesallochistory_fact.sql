/* ##################################################################################################################
  
     Script         : vw_bi_populate_salesallochistory_fact
     Author         : Shanthi
     Created On     : 25 May 2014
  
  
     Description    : New subject area to store the Sales Allocation History
  
#################################################################################################################### */

select 'START OF PROC vw_bi_populate_salesallochistory_fact',TIMESTAMP(LOCAL_TIMESTAMP);

Drop table if exists max_holder_702;
Create table max_holder_702(maxid)
as
Select ifnull(max(fact_salesallochistoryid),0)
from fact_salesallochistory;

DROP TABLE IF EXISTS fact_salesallochistory_tmp1

CREATE TABLE fact_salesallochistory_tmp1 AS 
SELECt * from fact_salesallochistory where 1 = 2;

insert into fact_salesallochistory_tmp1 
SELECT * FROM fact_salesallochistory , dim_Date
WHERE Dim_DateId = dim_DateIdSnapshot
and datevalue = current_Date;

call vectorwise(combine 'fact_salesallochistory-fact_salesallochistory_tmp1');

INSERT INTO fact_salesallochistory(fact_salesallochistoryid,
                                   Dim_DateIdSnapshot,
				   dd_SnapShotYear,
                                   dd_SnapshotWeekID,
                                   dd_SnapshotMonthID,
                                   dd_SalesDocNo,
                                   dd_SalesItemNo,
                                   dd_ScheduleNo,
                                   ct_ScheduleQtySalesUnit,
                                   ct_ConfirmedQty,
                                   ct_CorrectedQty,
                                   ct_DeliveredQty,
                                   amt_UnitPrice,
                                   ct_PriceUnit,
                                   amt_ScheduleTotal,
                                   amt_StdCost,
                                   amt_ExchangeRate,
                                   Dim_DateidSalesOrderCreated,
                                   Dim_DateidSchedDeliveryReq,
                                   Dim_DateidSchedDlvrReqPrev,
                                   Dim_DateidSchedDelivery,
                                   Dim_DateidGoodsIssue,
                                   Dim_DateidMtrlAvail,
                                   Dim_DateidLoading,
                                   Dim_DateidTransport,
                                   Dim_Currencyid,
                                   Dim_ProductHierarchyid,
                                   Dim_Plantid,
                                   Dim_Companyid,
                                   Dim_StorageLocationid,
                                   Dim_SalesDivisionid,
                                   Dim_ShipReceivePointid,
                                   Dim_DocumentCategoryid,
                                   Dim_SalesDocumentTypeid,
                                   Dim_SalesOrgid,
                                   Dim_CustomerID,
                                   Dim_SalesGroupid,
                                   Dim_CostCenterid,
                                   Dim_ControllingAreaid,
                                   Dim_TransactionGroupid,
                                   Dim_SalesOrderRejectReasonid,
                                   Dim_Partid,
                                   Dim_PartSalesId,
                                   Dim_SalesOrderHeaderStatusid,
                                   Dim_SalesOrderItemStatusid,
                                   Dim_CustomerGroup1id,
                                   Dim_CustomerGroup2id,
                                   dim_salesorderitemcategoryid,
                                   amt_ExchangeRate_gbl,
                                   Dim_DateidFirstDate,
                                   Dim_ScheduleLineCategoryId,
                                   Dim_SalesMiscId,
                                   dd_ItemRelForDelv,
                                   Dim_DateidShipmentDelivery,
                                   Dim_DateidActualGI,
                                   Dim_ProfitCenterId,
                                   Dim_CustomeridShipTo,
                                   Dim_UnitOfMeasureId,
                                   Dim_DistributionChannelId,
                                   dd_BatchNo,
                                   dd_CreatedBy,
                                   Dim_CustomPartnerFunctionId,
                                   Dim_PayerPartnerFunctionId,
                                   Dim_BillToPartyPartnerFunctionId,
                                   Dim_CustomerGroupId,
                                   Dim_SalesOfficeId,
                                   ct_AfsUnallocatedQty,
                                   amt_AfsUnallocatedValue,
                                   ct_AfsAllocationRQty,
                                   ct_CumConfirmedQty,
                                   ct_AfsAllocationFQty,
                                   ct_CumOrderQty,
                                   amt_AfsOnDeliveryValue,
                                   ct_ShippedOrBilledQty,
                                   amt_ShippedOrBilled,
                                   Dim_CreditRepresentativeId,
                                   dd_CustomerPONo,
                                   ct_IncompleteQty,
                                   amt_Incomplete,
                                   Dim_DeliveryBlockId,
                                   dd_AfsStockCategory,
                                   dd_AfsStockType,
                                   Dim_DateIdAfsCancelDate,
                                   amt_AfsNetPrice,
                                   amt_AfsNetValue,
                                   amt_BasePrice,
                                   Dim_MaterialPriceGroup1Id,
                                   amt_CreditHoldValue,
                                   amt_DeliveryBlockValue,
                                   Dim_AfsSizeId,
                                   Dim_AfsRejectionReasonId,
                                   Dim_DateidDlvrDocCreated,
                                   amt_CustomerExpectedPrice,
                                   ct_AfsAllocatedQty,
                                   ct_AfsOnDeliveryQty,
                                   Dim_OverallStatusCreditCheckId,
                                   Dim_SalesDistrictId,
                                   Dim_AccountAssignmentGroupId,
                                   Dim_MaterialGroupId,
                                   Dim_SalesDocOrderReasonId,
                                   amt_SubTotal3,
                                   amt_SubTotal4,
                                   Dim_DateIdAfsReqDelivery,
                                   ct_AfsOpenQty,
                                   Dim_AFSSeasonId,
                                   dd_AfsDepartment,
                                   Dim_DateIdSOCreated,
                                   Dim_DateIdSODocument,
                                   ct_AfsTotalDrawn,
                                   dd_ReferenceDocumentNo,
                                   dd_RequirementType,
                                   amt_StdPrice,
                                   amt_Tax,
                                   dd_BusinessCustomerPONo,
                                   Dim_BillingDateId,
                                   Dim_CustomerPaymentTermsId,
                                   Dim_SalesRiskCategoryId,
                                   dd_CreditRep,
                                   dd_CreditLimit,
                                   Dim_CustomerRiskCategoryId,
                                   Dim_DateIdSOItemChangedOn,
                                   Dim_ShippingConditionId,
                                   dd_AfsAllocationGroupNo,
                                   dd_CustomerMaterialNo,
                                   Dim_BaseUoMid,
                                   Dim_SalesUoMid,
                                   amt_UnitPriceUoM,
                                   Dim_DateIdFixedValue,
                                   Dim_PaymentTermsId,
                                   amt_Subtotal3inCustConfig_Billing,
                                   dim_customermastersalesid,
                                   dd_SalesOrderBlocked,
                                   dim_Currencyid_TRA,
                                   dim_Currencyid_GBL,
                                   dim_currencyid_STAT,
                                   amt_ExchangeRate_STAT,
                                   dd_ArunNumber,
                                   dd_AllocReqCategory,
                                   Dim_AvailabilityCheckId,
                                   Dim_IncoTermId,
                                   dd_ReleaseRule,
                                   dim_Deliverypriorityid,
                                   dd_ReferenceDocItemNo,
                                   Dim_SalesOrderPartnerId,
                                   dd_PurchaseRequisition,
                                   dd_ItemofRequisition,
                                   ct_AfsCalculatedOpenQty,
                                   amt_BillingCustConfigSubtotal3UnitPrice,
                                   dd_DocumentConditionNo,
                                   amt_SubTotal1,
                                   amt_SubTotal2,
                                   amt_SubTotal5,
                                   amt_SubTotal6,
                                   dim_CustomerConditionGroups1Id,
                                   dim_CustomerConditionGroups2Id,
                                   dim_CustomerConditionGroups3Id,
                                   ct_AfsOpenPOQty,
                                   ct_AfsInTransitQty,
                                   dim_scheduledeliveryblockid,
                                   dim_customergroup4id,
                                   dd_OpenConfirmationsExists,
                                   dd_ConditionNo,
                                   dd_CreditMgr,
                                   dd_clearedblockedsts,
                                   amt_sovalueprice,
                                   amt_discountaccrualnetprice,
                                   ct_CreditHoldQty,
                                   ct_DelBlockQty,
                                   ct_GIQty,
                                   ct_NetConfirmedQty,
                                   ct_BlockedQty,
                                   ct_UnconfirmedQty,
                                   ct_RejectedQty)
SELECT max_holder_702.maxid + row_number() over() fact_salesallochistoryid,
       ifnull(
          (SELECT dim_DateID
             FROM dim_date
            WHERE datevalue = CURRENT_DATE AND companycode = dc.companycode),
          1)
          Dim_DateIdSnapshot,
       year(current_date) dd_SnapShotYear,
       0,
       0,
       dd_SalesDocNo,
       dd_SalesItemNo,
       dd_ScheduleNo,
       ct_ScheduleQtySalesUnit,
       ct_ConfirmedQty,
       ct_CorrectedQty,
       ct_DeliveredQty,
       amt_UnitPrice,
       ct_PriceUnit,
       amt_ScheduleTotal,
       amt_StdCost,
       amt_ExchangeRate,
       Dim_DateidSalesOrderCreated,
       Dim_DateidSchedDeliveryReq,
       Dim_DateidSchedDlvrReqPrev,
       Dim_DateidSchedDelivery,
       Dim_DateidGoodsIssue,
       1 Dim_DateidMtrlAvail,
       Dim_DateidLoading,
       Dim_DateidTransport,
       Dim_Currencyid,
       Dim_ProductHierarchyid,
       Dim_Plantid,
       f.Dim_Companyid,
       Dim_StorageLocationid,
       Dim_SalesDivisionid,
       Dim_ShipReceivePointid,
       f.Dim_DocumentCategoryid,
       Dim_SalesDocumentTypeid,
       Dim_SalesOrgid,
       Dim_CustomerID,
       Dim_SalesGroupid,
       Dim_CostCenterid,
       Dim_ControllingAreaid,
       Dim_TransactionGroupid,
       Dim_SalesOrderRejectReasonid,
       Dim_Partid,
       Dim_PartSalesId,
       f.Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       Dim_CustomerGroup1id,
       Dim_CustomerGroup2id,
       dim_salesorderitemcategoryid,
       amt_ExchangeRate_gbl,
       Dim_DateidFirstDate,
       Dim_ScheduleLineCategoryId,
       Dim_SalesMiscId,
       dd_ItemRelForDelv,
       Dim_DateidShipmentDelivery,
       Dim_DateidActualGI,
       Dim_ProfitCenterId,
       Dim_CustomeridShipTo,
       Dim_UnitOfMeasureId,
       Dim_DistributionChannelId,
       dd_BatchNo,
       dd_CreatedBy,
       Dim_CustomPartnerFunctionId,
       Dim_PayerPartnerFunctionId,
       Dim_BillToPartyPartnerFunctionId,
       Dim_CustomerGroupId,
       Dim_SalesOfficeId,
       ct_AfsUnallocatedQty,
       amt_AfsUnallocatedValue,
       ct_AfsAllocationRQty,
       ct_CumConfirmedQty,
       ct_AfsAllocationFQty,
       ct_CumOrderQty,
       amt_AfsOnDeliveryValue,
       ct_ShippedOrBilledQty,
       amt_ShippedOrBilled,
       Dim_CreditRepresentativeId,
       dd_CustomerPONo,
       ct_IncompleteQty,
       amt_Incomplete,
       f.Dim_DeliveryBlockId,
       dd_AfsStockCategory,
       dd_AfsStockType,
       Dim_DateIdAfsCancelDate,
       amt_AfsNetPrice,
       amt_AfsNetValue,
       amt_BasePrice,
       Dim_MaterialPriceGroup1Id,
       amt_CreditHoldValue,
       amt_DeliveryBlockValue,
       Dim_AfsSizeId,
       Dim_AfsRejectionReasonId,
       Dim_DateidDlvrDocCreated,
       amt_CustomerExpectedPrice,
       ct_AfsAllocatedQty,
       ct_AfsOnDeliveryQty,
       Dim_OverallStatusCreditCheckId,
       Dim_SalesDistrictId,
       Dim_AccountAssignmentGroupId,
       Dim_MaterialGroupId,
       Dim_SalesDocOrderReasonId,
       amt_SubTotal3,
       amt_SubTotal4,
       Dim_DateIdAfsReqDelivery,
       ct_AfsOpenQty,
       Dim_AFSSeasonId,
       dd_AfsDepartment,
       Dim_DateIdSOCreated,
       Dim_DateIdSODocument,
       ct_AfsTotalDrawn,
       dd_ReferenceDocumentNo,
       dd_RequirementType,
       amt_StdPrice,
       amt_Tax,
       dd_BusinessCustomerPONo,
       Dim_BillingDateId,
       Dim_CustomerPaymentTermsId,
       Dim_SalesRiskCategoryId,
       dd_CreditRep,
       dd_CreditLimit,
       Dim_CustomerRiskCategoryId,
       Dim_DateIdSOItemChangedOn,
       Dim_ShippingConditionId,
       dd_AfsAllocationGroupNo,
       dd_CustomerMaterialNo,
       Dim_BaseUoMid,
       Dim_SalesUoMid,
       amt_UnitPriceUoM,
       Dim_DateIdFixedValue,
       Dim_PaymentTermsId,
       amt_Subtotal3inCustConfig_Billing,
       dim_customermastersalesid,
       dd_SalesOrderBlocked,
       dim_Currencyid_TRA,
       dim_Currencyid_GBL,
       dim_currencyid_STAT,
       amt_ExchangeRate_STAT,
       dd_ArunNumber,
       dd_AllocReqCategory,
       Dim_AvailabilityCheckId,
       Dim_IncoTermId,
       dd_ReleaseRule,
       dim_Deliverypriorityid,
       dd_ReferenceDocItemNo,
       Dim_SalesOrderPartnerId,
       dd_PurchaseRequisition,
       dd_ItemofRequisition,
       (CASE WHEN f.dd_OpenConfirmationsExists = 'X' THEN f.ct_ConfirmedQty ELSE (CASE WHEN ((f.ct_AfsCalculatedOpenQty + f.ct_AfsOpenPOQty) > f.ct_AfsInTransitQty) THEN (f.ct_AfsCalculatedOpenQty + f.ct_AfsOpenPOQty - f.ct_AfsInTransitQty) ELSE 0 END) END) ct_AfsCalculatedOpenQty,
       amt_BillingCustConfigSubtotal3UnitPrice,
       dd_DocumentConditionNo,
       amt_SubTotal1,
       amt_SubTotal2,
       amt_SubTotal5,
       amt_SubTotal6,
       dim_CustomerConditionGroups1Id,
       dim_CustomerConditionGroups2Id,
       dim_CustomerConditionGroups3Id,
       ct_AfsOpenPOQty,
       ct_AfsInTransitQty,
       dim_scheduledeliveryblockid,
       dim_customergroup4id,
       dd_OpenConfirmationsExists,
       dd_ConditionNo,
       dd_CreditMgr,
       dd_clearedblockedsts,
       (CASE
           WHEN f.ct_ConfirmedQty > 0
           THEN
              cast(
                 (  f.amt_SubTotal3
                  / (CASE
                        WHEN f.ct_ConfirmedQty = 0 THEN NULL
                        ELSE f.ct_ConfirmedQty
                     END)) AS decimal(18, 4))
           ELSE
              0
        END)
          amt_sovalueprice,
       amt_dicountaccrualnetprice,
       (CASE
           WHEN ocs.OverallStatusforCreditCheck = 'B' THEN f.ct_ConfirmedQty
           ELSE 0
        END)
          ct_CreditHoldQty,
       amt_CreditHoldValue,
       (CASE
           WHEN db.DeliveryBlock <> 'Not Set' THEN f.ct_ConfirmedQty
           ELSE 0
        END)
          ct_DelBlockQty,
       f.ct_DeliveredQty ct_GIQty,
       (CASE
           WHEN docg.documentcategory IN ('G', 'B')
           THEN
              f.ct_AfsCalculatedOpenQty
           ELSE
              f.ct_ConfirmedQty
        END)
          ct_NetConfirmedQty,
       (CASE
           WHEN (   db.DeliveryBlock <> 'Not Set'
                 OR ocs.OverallStatusforCreditCheck = 'B')
           THEN
              f.ct_ConfirmedQty
           ELSE
              0
        END)
          ct_BlockedQty,
       0 ct_UnconfirmedQty,
       0 ct_RejectedQty,
       (CASE WHEN (f.Dim_DateidActualGI <> 1) then f.ct_DeliveredQty else 0 end) ct_GIQty
  FROM fact_salesorder f,
       dim_company dc,
       dim_overallstatusforcreditcheck ocs,
       dim_DeliveryBlock db,
       dim_documentcategory docg,
       dim_Salesorderheaderstatus sohs,
       dim_Salesorderrejectreason sors
       max_holder_702
 WHERE     f.dim_CompanyId = dc.dim_Companyid
       AND f.Dim_OverallStatusCreditCheckId =
              ocs.dim_overallstatusforcreditcheckID
       AND db.Dim_DeliveryBlockId = f.Dim_DeliveryblockId
       AND f.dim_documentcategoryid = docg.Dim_DocumentCategoryid
       AND sohs.dim_Salesorderheaderstatusid = f.dim_Salesorderheaderstatusid
       AND sors.dim_Salesorderrejectreasonid = f.dim_afsrejectionreasonid;

UPDATE fact_salesallochistory ah
FROM  dim_company c, dim_Date dt
SET ah.dd_SnapshotWeekID = dt.financialweek
WHERE ah.dim_companyid = c.dim_companyid
and dt.datevalue = current_Date
and dt.companycode = c.companycode
and dt.weekdayname = 'Sunday';

DROP TABLE IF EXISTS tmp_MinSunday_Date;

CREATE TABLE tmp_MinSunday_Date as 
select financialyear,financialmonthnumber,datevalue,companycode,weekdayname, Rank() over(partition by financialyear,financialmonthnumber order by datevalue asc) as rank1
from dim_date where weekdayname = 'Sunday'; 

DELETE from tmp_MinSunday_Date where rank1 <> 1;

UPDATE fact_salesallochistory ah
FROM  dim_company c, dim_Date dt, tmp_MinSunday_Date t
SET ah.dd_SnapshotMonthID = t.financialmonthnumber
WHERE ah.dim_companyid = c.dim_companyid
and dt.datevalue = current_Date
and dt.companycode = c.companycode
and dt.weekdayname = t.weekdayname
and dt.datevalue = t.datevalue
and dt.companycode = t.companycode;

DROP TABLE IF EXISTS tmp_MinSunday_Date;

UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_ConfSOValue = ah.ct_ConfirmedQty*ah.amt_sovalueprice,
    amt_ConfNetSales = ah.ct_ConfirmedQty*ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date;

UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_IncompleteSOValue = ah.ct_IncompleteQty*ah.amt_sovalueprice,
    amt_IncompleteNetSales = ah.ct_IncompleteQty*ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date; 
    
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_CreditHoldSOValue = ah.ct_CreditHoldQty *ah.amt_sovalueprice,
    amt_CreditHoldNetSales = ah.ct_CreditHoldQty *ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date; 
  
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_DelBlockSOValue = ah.ct_DelBlockQty * ah.amt_sovalueprice,
    amt_DelBlockNetSales = ah.ct_DelBlockQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date; 
  
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_ReservedSOValue = ah.ct_ReservedQty * ah.amt_sovalueprice,
    amt_ReservedNetSales = ah.ct_ReservedQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date; 
   
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_FixedSOValue = ah.ct_FixedQty * ah.amt_sovalueprice,
    amt_FixedNetSales = ah.ct_FixedQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date;   
  
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_AllocSOValue = ah.ct_AfsAllocatedQty * ah.amt_sovalueprice,
    amt_AllocNetSales = ah.ct_AfsAllocatedQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date;   
  
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_UnAllocSOValue = ah.ct_AfsUnallocatedQty * ah.amt_sovalueprice,
    amt_UnAllocNetSales = ah.ct_AfsUnallocatedQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date;

UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_OpenSOValue = ah.ct_AfsCalculatedOpenQty * ah.amt_sovalueprice,
    amt_OpenNetSales = ah.ct_AfsCalculatedOpenQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date; 
  
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_OnDelivSOValue = ah.ct_AfsOnDeliveryQty * ah.amt_sovalueprice,
    amt_OnDelivNetSales = ah.ct_AfsOnDeliveryQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date; 
  
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_GISOValue = ah.ct_GIQty * ah.amt_sovalueprice,
    amt_GINetSales = ah.ct_GIQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date; 

UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_ShippedSOValue = ah.ct_ShippedOrBilledQty * ah.amt_sovalueprice,
    amt_ShippedNetSales = ah.ct_ShippedOrBilledQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date;
  
UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_BlockedSOValue = ah.ct_BlockedQty * ah.amt_sovalueprice,
    amt_BlockedNetSales = ah.ct_BlockedQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date;

UPDATE fact_salesallochistory ah
 FROM Dim_Date dt
SET amt_UnConfSOValue = ah.ct_UnconfirmedQty * ah.amt_sovalueprice,
    amt_UnConfNetSales = ah.ct_UnconfirmedQty * ah.amt_dicountaccrualnetprice
WHERE ah.Dim_DateIdSnapshot = dt.Dim_DateId
and dt.datevalue = current_date;

select 'END OF PROC vw_bi_populate_salesallochistory_fact',TIMESTAMP(LOCAL_TIMESTAMP);
