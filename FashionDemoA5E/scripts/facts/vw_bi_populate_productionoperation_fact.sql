﻿
/* ################################################################################################################## */
/* */
/*   Script         : vw_bi_populate_productionoperation_fact.sql */
/*   Author         : Abdelhadi */
/*   Created On     : 18 Oct 2013 */
/* */
/* */
/*   Description    : Script for Delivery Execution fact*/
/* */
/*   Change History */
/* ####################################################################################################################   */

Drop table if exists pGlobalCurrency_po_77;

Create table pGlobalCurrency_po_77(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_77(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_77
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

Drop table if exists fact_productionoperation_tmp;
Drop table if exists max_holder_prop;

Create table fact_productionoperation_tmp as
SELECT dd_RoutingOperationNo,dd_GeneralOrderCounter
FROM fact_productionoperation ;

Create table max_holder_prop
as
Select ifnull(max(fact_productionoperationid),0) maxid
from fact_productionoperation;

INSERT INTO fact_productionoperation(fact_productionoperationid,
									amt_Meteriel_PrimaryCost,
									amt_ActivityCostTtl,
									amt_Price,
									amt_PriceUnit,
									ct_ActivityDuration,
									ct_ActualWork,
									ct_ActivityWorkInvolved,
									dim_businessareaid,
									dim_companyid,
									dim_controllingareaid,
									dim_materialgroupid,
									dim_currencyid,
									dim_plantid,
									dim_profitcenterid,
									dim_purchasegroupid,
									dim_purchaseorgid,
									dim_tasklisttypeid,
									dim_vendorid,
									dim_costcenterid,
									dd_PODocumentNo,
									dim_functionalareaid,
									dim_objecttypeid,
									dim_unitofmeasureid,
									dd_RoutingOperationNo,
									dd_GeneralOrderCounter,
									dd_POItemNo,
									Dim_DateIdActualStartExec,
									Dim_DateIdActualFinishExec,
									dd_hourActualStartExec,
									dd_hourActualFinishExe,
									Dim_DateIdSchedStartExec,
									Dim_DateIdSchedFinishExec,
									dd_hourSchedStartExec,
									dd_hourSchedFinishExec,
									dd_RoutingRefSequence,
									dd_Operationshorttext,
									dd_descriptionline2,
									dd_OperationNumber,
									dd_WorkCenter,
									dd_bomexplosionno,
									dim_productionorderstatusid,
									dim_currencyid_TRA,
									dim_currencyid_GBL)
		SELECT maxid + row_number() over(),tp.*
		From (SELECT DISTINCT
				ifnull(AA.AFVC_MAT_PRKST, 0) amt_Meteriel_PrimaryCost,
				ifnull(AA.AFVC_PRKST, 0) amt_ActivityCostTtl,
				ifnull(AA.AFVC_PREIS, 0) amt_Price,
				ifnull(AA.AFVC_PEINH, 0) amt_PriceUnit,
				ifnull(AA.AFVV_DAUNO, 0) ct_ActivityDuration,
				ifnull(AA.AFVV_ISMNW, 0) ct_ActualWork,
				ifnull(AA.AFVV_ARBEI, 0) ct_ActivityWorkInvolved,
				1	dim_businessareaid,
				1	dim_companyid,
				1	dim_controllingareaid,	
				1	dim_materialgroupid,
				1	dim_currencyid,	
				1	dim_plantid,	
				1	dim_profitcenterid,	
				1	dim_purchasegroupid,	
				1	dim_purchaseorgid,	
				1	dim_tasklisttypeid,	
				1	dim_vendorid,	
				1	dim_costcenterid,	
				ifnull(AA.AFVC_EBELN, 'Not Set') dd_PODocumentNo,
				1	dim_functionalareaid,	
				1	dim_objecttypeid,	
				1	dim_unitofmeasureid,	
				ifnull(AA.AFVC_AUFPL, 0) dd_RoutingOperationNo,
				ifnull(AA.AFVC_APLZL, 0) dd_GeneralOrderCounter,
				ifnull(AA.AFVC_EBELP, 0) dd_POItemNo,
				1	Dim_DateIdActualStartExec,	
				1	Dim_DateIdActualFinishExec,	
				ifnull(AA.AFVV_ISDZ, 'Not Set') dd_hourActualStartExec,
				ifnull(AA.AFVV_IEDZ, 'Not Set') dd_hourActualFinishExe,
				1	Dim_DateIdSchedStartExec,	
				1	Dim_DateIdSchedFinishExec,	
				ifnull(AA.AFVV_FSAVZ, 'Not Set') dd_hourSchedStartExec,
				ifnull(AA.AFVV_FSEDZ, 'Not Set') dd_hourSchedFinishExec,
				ifnull(AA.AFVC_VPLFL, 'Not Set') dd_RoutingRefSequence,
				ifnull(AA.AFVC_LTXA1, 'Not Set') dd_Operationshorttext,
				ifnull(AA.AFVC_LTXA2, 'Not Set') dd_descriptionline2,
				'Not Set'	dd_OperationNumber,
				'Not Set'	dd_WorkCenter,
				'Not Set'	dd_bomexplosionno,
				1	dim_productionorderstatusid,
				ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = AFVC_WAERS),1) Dim_Currencyid_TRA,
				ifnull((SELECT Dim_Currencyid
                  FROM Dim_Currency cur
                  WHERE cur.CurrencyCode = pGlobalCurrency),1) dim_Currencyid_GBL
		FROM  AFVC_AFVV AA,pGlobalCurrency_po_77
			WHERE NOT EXISTS
						 (SELECT 1
							FROM fact_productionoperation_tmp pop
						   WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
								 AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL)) tp, max_holder_prop m;
		
Drop table if exists fact_productionoperation_tmp;
Drop table if exists max_holder_prop;

DELETE FROM fact_productionoperation
WHERE EXISTS
          (SELECT 1
             FROM AFVC_AFVV
            WHERE     AFVC_AUFPL = dd_RoutingOperationNo
                  AND AFVC_APLZL = dd_GeneralOrderCounter
				  AND AUFK_LOEKZ = 'X');                
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET amt_Meteriel_PrimaryCost = ifnull(AFVC_MAT_PRKST ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_Meteriel_PrimaryCost <> ifnull(AA.AFVC_MAT_PRKST ,0);
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET amt_ActivityCostTtl = ifnull (AFVC_PRKST ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_ActivityCostTtl <> ifnull (AFVC_PRKST ,0);
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET amt_Price = ifnull(AFVC_PREIS ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_Price <> ifnull(AA.AFVC_PREIS ,0);
			
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET amt_PriceUnit = ifnull(AFVC_PEINH ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.amt_PriceUnit <> ifnull(AA.AFVC_PEINH ,0);
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET ct_ActivityDuration = ifnull(AFVV_DAUNO ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActivityDuration <> ifnull(AA.AFVV_DAUNO ,0);
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET ct_ActualWork = ifnull(AFVV_ISMNW ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActualWork <> ifnull(AA.AFVV_ISMNW ,0);	
				
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET ct_ActivityWorkInvolved = ifnull(AFVV_ARBEI ,0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND pop.ct_ActivityWorkInvolved <> ifnull(AA.AFVV_ARBEI ,0);
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_businessarea bsar	
SET dim_businessareaid = bsar.dim_businessareaid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_GSBER = bsar.Businessarea
		AND pop.dim_businessareaid <> bsar.dim_businessareaid;
									
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_company comp	
SET dim_companyid = comp.dim_companyid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_BUKRS = comp.companycode
		AND pop.dim_companyid <> comp.dim_companyid;					
												
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_controllingarea ctrar	
SET dim_controllingareaid = ctrar.dim_controllingareaid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_ANFKOKRS = ctrar.controllingareacode
		AND pop.dim_controllingareaid <> ctrar.dim_controllingareaid;	
						
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_materialgroup matgp	
SET dim_materialgroupid = matgp.dim_materialgroupid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_MATKL = matgp.materialgroupcode
		AND pop.dim_materialgroupid <> matgp.dim_materialgroupid;

UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_currency cur	
SET dim_currencyid = cur.dim_currencyid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL
		AND AA.AFVC_WAERS = cur.currencycode
		AND pop.dim_currencyid <> cur.dim_currencyid;

UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_plant pla	
SET dim_plantid = pla.dim_plantid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_WERKS = pla.plantcode
		AND pop.dim_plantid <> pla.dim_plantid;
						
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_profitcenter proctr	
SET dim_profitcenterid = proctr.dim_profitcenterid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_PRCTR = proctr.profitcentercode
		AND pop.dim_profitcenterid <> proctr.dim_profitcenterid;
						
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_purchasegroup porgp	
SET dim_purchasegroupid = porgp.dim_purchasegroupid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_EKGRP = porgp.purchasegroup
		AND pop.dim_purchasegroupid <> porgp.dim_purchasegroupid;
						
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_purchaseorg pororg	
SET dim_purchaseorgid = pororg.dim_purchaseorgid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_EKORG = pororg.purchaseorgcode
		AND pop.dim_purchaseorgid <> pororg.dim_purchaseorgid;
						
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_tasklisttype tsklt	
SET dim_tasklisttypeid = tsklt.dim_tasklisttypeid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_PLNTY = tsklt.tasklisttypecode
		AND pop.dim_tasklisttypeid <> tsklt.dim_tasklisttypeid;

UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_vendor ven	
SET dim_vendorid = ven.dim_vendorid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_LIFNR = ven.vendornumber
		AND pop.dim_vendorid <> ven.dim_vendorid;						
						
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_costcenter coctr	
SET dim_costcenterid = coctr.dim_costcenterid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_ANFKO = coctr.code
		AND pop.dim_costcenterid <> coctr.dim_costcenterid;	
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_PODocumentNo = ifnull(AFVC_EBELN, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_PODocumentNo <> ifnull(AA.AFVC_EBELN, 'Not Set');

UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_functionalarea fctar	
SET dim_functionalareaid = fctar.dim_functionalareaid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_FUNC_AREA = fctar.functionalarea
		AND pop.dim_functionalareaid <> fctar.dim_functionalareaid;

UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_objecttype objt	
SET dim_objecttypeid = objt.dim_objecttypeid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVC_OTYPE = objt.objecttype
		AND pop.dim_objecttypeid <> objt.dim_objecttypeid;

UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_unitofmeasure uniom	
SET dim_unitofmeasureid = uniom.dim_unitofmeasureid
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_MEINH = uniom.uom
		AND pop.dim_unitofmeasureid <> uniom.dim_unitofmeasureid;

UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_POItemNo = ifnull(AFVC_EBELP, 0)
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_POItemNo <> ifnull(AA.AFVC_EBELP, 0);
		
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_date asedt	
SET Dim_DateIdActualStartExec = asedt.Dim_DateId
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_ISDD = asedt.datevalue 
		AND AA.AFVC_BUKRS = asedt.companycode
		AND pop.Dim_DateIdActualStartExec <> asedt.Dim_DateId;
						
UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_date afedt	
SET Dim_DateIdActualFinishExec = afedt.Dim_DateId
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_IEDD = afedt.datevalue 
		AND AA.AFVC_BUKRS = afedt.companycode
		AND pop.Dim_DateIdActualFinishExec <> afedt.Dim_DateId;						
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_hourActualStartExec = ifnull(AFVV_ISDZ, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourActualStartExec <> ifnull(AA.AFVV_ISDZ, 'Not Set');						
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_hourActualFinishExe = ifnull(AFVV_IEDZ, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourActualFinishExe <> ifnull(AA.AFVV_IEDZ, 'Not Set');				

UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_date ssedt	
SET Dim_DateIdSchedStartExec = ssedt.Dim_DateId
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_FSAVD = ssedt.datevalue 
		AND AA.AFVC_BUKRS = ssedt.companycode
		AND pop.Dim_DateIdSchedStartExec <> ssedt.Dim_DateId;	

UPDATE fact_productionoperation pop
from AFVC_AFVV AA,dim_date sfedt	
SET Dim_DateIdSchedFinishExec = sfedt.Dim_DateId
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND AA.AFVV_FSEDD = sfedt.datevalue 
		AND AA.AFVC_BUKRS = sfedt.companycode
		AND pop.Dim_DateIdSchedFinishExec <> sfedt.Dim_DateId;	
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_hourSchedStartExec = ifnull(AFVV_FSAVZ, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourSchedStartExec <> ifnull(AA.AFVV_FSAVZ, 'Not Set');					
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_hourSchedFinishExec = ifnull(AFVV_FSEDZ, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_hourSchedFinishExec <> ifnull(AA.AFVV_FSEDZ, 'Not Set');				
				
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_RoutingRefSequence = ifnull(AFVC_VPLFL, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_RoutingRefSequence <> ifnull(AA.AFVC_VPLFL, 'Not Set');

UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_Operationshorttext = ifnull(AFVC_LTXA1, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_Operationshorttext <> ifnull(AA.AFVC_LTXA1, 'Not Set');
		
UPDATE fact_productionoperation pop
from AFVC_AFVV AA
SET dd_descriptionline2 = ifnull(AFVC_LTXA2, 'Not Set')
	WHERE	pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
		AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
		AND pop.dd_descriptionline2 <> ifnull(AA.AFVC_LTXA2, 'Not Set');		

UPDATE fact_productionoperation pop
from fact_productionorder po 
SET pop.dd_OrderNumber = po.dd_OrderNumber 
	WHERE	pop.dd_RoutingOperationNo = po.dd_RoutingOperationNo;	
			
UPDATE fact_productionoperation pop
from fact_productionorder po 
SET dd_OperationNumber = po.dd_OperationNumber,
dd_WorkCenter = po.dd_WorkCenter,
dd_bomexplosionno = po.dd_bomexplosionno,
dim_productionorderstatusid = po.dim_productionorderstatusid
	WHERE	pop.dd_RoutingOperationNo = po.dd_ordernumber;											

UPDATE fact_productionoperation pop
SET dd_OperationNumber = 'Not Set'
	WHERE dd_OperationNumber is NULL;

UPDATE fact_productionoperation pop
SET dd_WorkCenter = 'Not Set'
	WHERE dd_WorkCenter is NULL;

UPDATE fact_productionoperation pop
SET dd_bomexplosionno = 'Not Set'
	WHERE dd_bomexplosionno is NULL;

UPDATE fact_productionoperation pop
SET dim_productionorderstatusid = 1
	WHERE dim_productionorderstatusid is NULL;
	
UPDATE fact_productionoperation pop
FROM AFVC_AFVV AA,
     dim_company dc
SET amt_ExchangeRate = 1
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND ifnull(amt_ExchangeRate,-1) <> 1; 

UPDATE fact_productionoperation pop
FROM AFVC_AFVV AA,
     dim_company dc,
	 tmp_getExchangeRate1 ex
SET amt_ExchangeRate = ex.exchangeRate 
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND  pFromCurrency = AA.AFVC_WAERS
and pToCurrency = dc.Currency
and pFromExchangeRate = 0
and pDate = ANSIDATE(LOCAL_TIMESTAMP)
and fact_script_name = 'bi_populate_productionorder_fact' 
AND amt_ExchangeRate <> ex.exchangeRate ;


UPDATE fact_productionoperation pop
FROM AFVC_AFVV AA,
     dim_company dc
SET amt_ExchangeRate_GBL = 1
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND ifnull(amt_ExchangeRate_GBL,-1) <> 1;  

UPDATE fact_productionoperation pop
FROM AFVC_AFVV AA,
     dim_company dc,
	 tmp_getExchangeRate1 ex,
	 pGlobalCurrency_po_77
SET amt_ExchangeRate_GBL = ex.exchangeRate 
WHERE pop.dd_RoutingOperationNo = AA.AFVC_AUFPL
AND pop.dd_GeneralOrderCounter = AA.AFVC_APLZL	
AND dc.CompanyCode = AA.AFVC_BUKRS
AND  pFromCurrency = AA.AFVC_WAERS
and pToCurrency = pGlobalCurrency
and pFromExchangeRate = 0
and pDate =  ANSIDATE(LOCAL_TIMESTAMP)   /*current date to be used for global rate*/
and fact_script_name = 'bi_populate_productionorder_fact'
AND amt_ExchangeRate <> ex.exchangeRate ;	


UPDATE fact_productionoperation f
FROM afvc_afvv a
SET dd_objectid = a.afvc_arbid
WHERE f.dd_RoutingOperationNo = a.AFVC_AUFPL
AND f.dd_GeneralOrderCounter = a.AFVC_APLZL;


UPDATE fact_productionoperation f
FROM dim_workcenter wc
SET f.dim_workcenterid = wc.dim_workcenterid
WHERE wc.objectid = f.dd_objectid;

UPDATE fact_productionoperation f
FROM dim_costcenter dc, CRCO c
SET f.dim_costcenterid = dc.dim_costcenterid
WHERE f.dd_objectid = c.crco_objid
AND c.crco_KOSTL = dc.code
AND f.dim_costcenterid <> dc.dim_costcenterid;
