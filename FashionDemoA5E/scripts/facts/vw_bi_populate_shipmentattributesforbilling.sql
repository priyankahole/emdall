DROP TABLE IF EXISTS tmp_sod_billing_intermediate;
CREATE TABLE tmp_sod_billing_intermediate
AS
SELECT f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo,sum(f_sod.amt_Cost) amt_sd_Cost,
SUM(f_sod.amt_UnitPrice * f_sod.ct_QtyDelivered) amt_sd_Shipped,
SUM(f_sod.ct_QtyDelivered) ct_sd_QtyDelivered
FROM fact_salesorderdelivery f_sod
GROUP BY f_sod.dd_SalesDlvrDocNo,f_sod.dd_SalesDlvrItemNo;



UPDATE fact_billing fb 
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp
    SET fb.amt_sd_Cost = sod.amt_sd_Cost
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
	AND ifnull(fb.amt_sd_Cost,-1) <> ifnull(sod.amt_sd_Cost,0);


UPDATE fact_billing fb
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp
    SET fb.amt_sd_Shipped = sod.amt_sd_Shipped
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
        AND ifnull(fb.amt_sd_Shipped,-1) <> ifnull(sod.amt_sd_Shipped,0);

UPDATE fact_billing fb
FROM tmp_sod_billing_intermediate sod, LIKP_LIPS lkp
    SET fb.ct_sd_QtyDelivered = sod.ct_sd_QtyDelivered
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
        AND ifnull(fb.ct_sd_QtyDelivered,-1) <> ifnull(sod.ct_sd_QtyDelivered,0);



UPDATE fact_billing fb FROM fact_salesorderdelivery sod, LIKP_LIPS lkp
        SET fb.Dim_sd_DateidActualGoodsIssue = sod.Dim_DateidActualGoodsIssue
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo
        AND fb.dd_SalesDlvrDocNo = lkp.LIKP_VBELN
        AND fb.dd_SalesDlvrItemNo = lkp.LIPS_POSNR
	AND ifnull(fb.Dim_sd_DateidActualGoodsIssue,-1) <> ifnull(sod.Dim_DateidActualGoodsIssue,1);


DROP TABLE IF EXISTS tmp_sod_billing_intermediate;
call vectorwise(combine 'fact_billing');
