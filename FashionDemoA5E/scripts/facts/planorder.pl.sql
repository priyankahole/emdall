/* 28 Aug 2013 Shanthi Included profit center dimension */
/* 21 Sep 2013 Lokesh  Currency/exchange rate changes */

select current_time;

select 'START OF PROC vw_bi_populate_planorder_fact_1',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS tmp_pGlobalCurrency_planorder;
CREATE TABLE tmp_pGlobalCurrency_planorder ( pGlobalCurrency CHAR(3) NULL);

INSERT INTO tmp_pGlobalCurrency_planorder VALUES ( 'USD' );

update tmp_pGlobalCurrency_planorder 
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

/*Inactive old plan orders - Q1a*/
UPDATE fact_planorder po
   SET po.Dim_ActionStateid = 3
 WHERE NOT EXISTS
          (SELECT 1
             FROM PLAF p
            WHERE p.PLAF_PLNUM = po.dd_PlanOrderNo)
AND po.Dim_ActionStateid <> 3			;

/*Inactive old plan orders - Q1b*/
UPDATE fact_planorder po
   SET po.ct_Completed = 1
 WHERE NOT EXISTS
          (SELECT 1
             FROM PLAF p
            WHERE p.PLAF_PLNUM = po.dd_PlanOrderNo)
AND po.ct_Completed <> 1;

/*Update plan order*/
DROP TABLE IF EXISTS tmp_fpo_mbew_no_bwtar;
CREATE TABLE tmp_fpo_mbew_no_bwtar AS SELECT ifnull((CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0) calcPrice,
                        sp.BWKEY,
                        sp.MATNR
                FROM mbew_no_bwtar sp
               WHERE  ((sp.LFGJA * 100) + sp.LFMON) =
                            (SELECT max((x.LFGJA * 100) + x.LFMON)
                               FROM mbew_no_bwtar x
                              WHERE x.MATNR = sp.MATNR AND x.BWKEY = sp.BWKEY
                                    AND ((x.VPRSV = 'S' AND x.STPRS > 0)
                                         OR (x.VPRSV = 'V' AND x.VERPR > 0)))
                     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0)
                          OR (sp.VPRSV = 'V' AND sp.VERPR > 0));

/* Q2 - column 1 - Dim_Companyid*/

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_Companyid = dc.Dim_Companyid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Companyid <> dc.Dim_Companyid;		


/* Q2 - column 2 - Dim_Partid */
UPDATE fact_planorder po
FROM plaf p,
	dim_plant pl,
	dim_company dc,
	dim_currency c,
	dim_part dp,
	dim_unitofmeasure uom
SET po.Dim_Partid = dp.Dim_Partid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Partid <> dp.Dim_Partid;

/* Q2 - column 3 - Dim_mpnid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_mpnid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_mpnid,-1) <> 1;


UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_part mpn
SET po.Dim_mpnid = mpn.dim_partid	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND varchar(uom.UOM,3) = varchar(p.plaf_meins,3)
AND dc.Currency = c.CurrencyCode
AND mpn.PartNumber = p.PLAF_EMATN
AND mpn.Plant = p.PLAF_PLWRK
AND mpn.RowIsCurrent = 1;

/* Q2 - column 4 - Dim_StorageLocationid */

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_StorageLocationid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_StorageLocationid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_storagelocation sl
SET po.Dim_StorageLocationid = sl.Dim_StorageLocationid	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sl.LocationCode = p.plaf_lgort
AND sl.Plant = p.PLAF_PLWRK
AND sl.RowIsCurrent = 1;


/* Q2 - column 5 - Dim_Plantid */

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_Plantid = pl.Dim_Plantid		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Plantid  <> pl.Dim_Plantid	;

/* Q2 - column6 - Dim_UnitOfMeasureid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_UnitOfMeasureid = uom.Dim_UnitOfMeasureid	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_UnitOfMeasureid  <> uom.Dim_UnitOfMeasureid;

/* Q2 - column7 - Dim_PurchaseOrgid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,dim_purchaseorg pog  
SET po.Dim_PurchaseOrgid = pog.Dim_PurchaseOrgid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pog.PurchaseOrgCode = pl.PurchOrg
AND po.Dim_PurchaseOrgid  <> pog.Dim_PurchaseOrgid;


/* Q2 - column8 - Dim_Vendorid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_Vendorid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_Vendorid <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_vendor dv
SET po.Dim_Vendorid = dv.Dim_Vendorid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dv.VendorNumber = p.PLAF_EMLIF;

/* Q2 - column9 - Dim_FixedVendorid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_FixedVendorid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.Dim_FixedVendorid <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_vendor fv
SET po.Dim_FixedVendorid = fv.Dim_Vendorid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND fv.VendorNumber = p.PLAF_FLIEF;

/* Q2 - column10 - Dim_SpecialProcurementid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_SpecialProcurementid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_SpecialProcurementid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_specialprocurement sp
SET po.Dim_SpecialProcurementid = sp.Dim_SpecialProcurementid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sp.specialprocurement = p.PLAF_SOBES
AND ifnull(po.Dim_SpecialProcurementid,-1) = ifnull(sp.Dim_SpecialProcurementid,-2);

/* Q2 - column11 - Dim_ConsumptionTypeid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_ConsumptionTypeid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_ConsumptionTypeid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_consumptiontype dcp
SET po.Dim_ConsumptionTypeid = dcp.Dim_ConsumptionTypeid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dcp.ConsumptionCode = p.PLAF_KZVBR
AND ifnull(po.Dim_ConsumptionTypeid,-1) <> ifnull(dcp.Dim_ConsumptionTypeid,-2);

/* Q2 - column12 - Dim_AccountCategoryid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_AccountCategoryid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_AccountCategoryid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_accountcategory ac
SET po.Dim_AccountCategoryid = ac.Dim_AccountCategoryid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ac.Category = p.plaf_knttp
AND ifnull(po.Dim_AccountCategoryid,-1) <> ifnull(ac.Dim_AccountCategoryid,-2);


/* Q2 - column13 - Dim_SpecialStockid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_SpecialStockid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_SpecialStockid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_specialstock st
SET po.Dim_SpecialStockid = st.Dim_SpecialStockid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND st.specialstockindicator = p.PLAF_SOBKZ
AND ifnull(po.Dim_SpecialStockid,-1) <> ifnull(st.Dim_SpecialStockid,-2);

/* Q2 - column14 - dim_bomstatusid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_bomstatusid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_bomstatusid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_bomstatus bs
SET po.dim_bomstatusid = bs.dim_bomstatusid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND bs.BOMStatusCode = p.plaf_ststa
AND ifnull(po.dim_bomstatusid,-1) <> ifnull(bs.dim_bomstatusid,-2);

/* Q2 - column15 - dim_bomusageid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_bomusageid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_bomusageid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_bomusage bu
SET po.dim_bomusageid = bu.dim_bomusageid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND bu.BOMUsageCode = p.plaf_stlan
AND ifnull(po.dim_bomusageid,-1) <> ifnull(bu.dim_bomusageid,-2);

/* Q2 - column16 - dim_objecttypeid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_objecttypeid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_objecttypeid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_objecttype ot
SET po.dim_objecttypeid = ot.dim_objecttypeid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ot.ObjectType = p.PLAF_OBART
AND ifnull(po.dim_objecttypeid,-1) <> ifnull(ot.dim_objecttypeid,-2);

/* Q2 - column17 - dim_productionschedulerid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_productionschedulerid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_productionschedulerid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_productionscheduler ps
SET po.dim_productionschedulerid = ps.dim_productionschedulerid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ps.ProductionScheduler = p.PLAF_PLGRP
AND ps.Plant = p.PLAF_PLWRK
AND ifnull(po.dim_productionschedulerid,-1) <> ifnull(ps.dim_productionschedulerid,-2);


/* Q2 - column18 - dim_schedulingerrorid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_schedulingerrorid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_schedulingerrorid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_schedulingerror se
SET po.dim_schedulingerrorid = se.dim_schedulingerrorid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND se.SchedulingErrorCode = p.PLAF_TRMER
AND ifnull(po.dim_schedulingerrorid,-1) <> ifnull(se.dim_schedulingerrorid,-2);

/* Q2 - column19 - dim_tasklisttypeid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_tasklisttypeid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_tasklisttypeid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_tasklisttype tst
SET po.dim_tasklisttypeid = tst.dim_tasklisttypeid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND tst.TaskListTypeCode = p.PLAF_PLNTY
AND ifnull(po.dim_tasklisttypeid,-1) <> ifnull(tst.dim_tasklisttypeid,-2);

/* Q2 - column20 - dim_ordertypeid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_ordertypeid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_ordertypeid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_ordertype ordt
SET po.dim_ordertypeid = ordt.dim_ordertypeid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ordt.OrderTypeCode = p.PLAF_PAART
AND ifnull(po.dim_ordertypeid,-1) <> ifnull(ordt.dim_ordertypeid,-2);


/* Q2 - column21 - Dim_dateidStart */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_dateidStart = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidStart,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date ds
SET po.Dim_dateidStart = ds.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ds.DateValue = p.PLAF_PSTTR
AND pl.CompanyCode = ds.CompanyCode
AND ifnull(po.Dim_dateidStart,-1) <> ifnull(ds.dim_dateid,-2);

/* Q2 - column22 - Dim_dateidFinish */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_dateidFinish = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidFinish,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df
SET po.Dim_dateidFinish = df.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_PEDTR
AND pl.CompanyCode = df.CompanyCode
AND p.PLAF_PEDTR IS NOT NULL
AND ifnull(po.Dim_dateidFinish,-1) <> ifnull(df.dim_dateid,-2);


/* Q2 - column23 - Dim_dateidOpening */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_dateidOpening = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_dateidOpening,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date dop
SET po.Dim_dateidOpening = dop.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND dop.DateValue = p.PLAF_PERTR
AND pl.CompanyCode = dop.CompanyCode
AND p.PLAF_PERTR IS NOT NULL
AND ifnull(po.Dim_dateidOpening,-1) <> ifnull(dop.dim_dateid,-2);

/* Q2 - column24 - ct_QtyTotal */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.ct_QtyTotal = p.PLAF_GSMNG	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND po.ct_QtyTotal <> p.PLAF_GSMNG;


/* Q2 - column25 - amt_ExtendedPrice : LK - This query can be tuned further if required */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET        amt_ExtendedPrice =
          (ifnull((SELECT calcPrice 
				FROM tmp_fpo_mbew_no_bwtar sp
               WHERE sp.MATNR = dp.PartNumber AND sp.BWKEY = pl.ValuationArea),
             0)
           * p.PLAF_GSMNG)
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode;

/* Q2 - column26 - dd_bomexplosionno */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dd_bomexplosionno = p.PLAF_SERNR	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_bomexplosionno,'xx') <> ifnull(p.PLAF_SERNR,'yy');

/* Q2 - column27 - ct_QtyReduced */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.ct_QtyReduced = p.plaf_ABMNG	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyReduced,-1) <> ifnull(p.plaf_ABMNG,-2);

/* Q2 - column28 - Dim_DateidProductionStart */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_DateidProductionStart = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidProductionStart,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df
SET po.Dim_DateidProductionStart = df.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_TERST
AND pl.CompanyCode = df.CompanyCode
AND ifnull(po.Dim_DateidProductionStart,-1) <> ifnull(df.dim_dateid,-2);


/* Q2 - column29 - Dim_DateidProductionFinish */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_DateidProductionFinish = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidProductionFinish,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df
SET po.Dim_DateidProductionFinish = df.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_TERED
AND pl.CompanyCode = df.CompanyCode
AND ifnull(po.Dim_DateidProductionFinish,-1) <> ifnull(df.dim_dateid,-2);

/* Q2 - column30 - Dim_DateidExplosion */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_DateidExplosion = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidExplosion,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df
SET po.Dim_DateidExplosion = df.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_PALTR
AND pl.CompanyCode = df.CompanyCode
AND ifnull(po.Dim_DateidExplosion,-1) <> ifnull(df.dim_dateid,-2);


/* Q2 - column31 - Dim_DateidAction */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_DateidAction = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_DateidAction,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_date df
SET po.Dim_DateidAction = df.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND df.DateValue = p.PLAF_MDACD
AND pl.CompanyCode = df.CompanyCode
AND ifnull(po.Dim_DateidAction,-1) <> ifnull(df.dim_dateid,-2);

/* Q2 - column32 - dim_scheduletypeid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_scheduletypeid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_scheduletypeid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_scheduletype st
SET po.dim_scheduletypeid = st.dim_scheduletypeid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND st.ScheduleTypeCode = p.PLAF_LVSCH
AND ifnull(po.dim_scheduletypeid,-1) <> ifnull(st.dim_scheduletypeid,-2);


/* Q2 - column33 - dim_availabilityconfirmationid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_availabilityconfirmationid = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_availabilityconfirmationid,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, dim_availabilityconfirmation acf
SET po.dim_availabilityconfirmationid = acf.dim_availabilityconfirmationid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND acf.AvailabilityConfirmationCode = p.PLAF_MDPBV
AND ifnull(po.dim_availabilityconfirmationid,-1) <> ifnull(acf.dim_availabilityconfirmationid,-2);

/* Q2 - column34 - Dim_ProcurementId */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_ProcurementId = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_ProcurementId,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom, Dim_Procurement pc
SET po.Dim_ProcurementId = pc.dim_procurementid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pc.procurement = p.PLAF_BESKZ
AND ifnull(po.Dim_ProcurementId,-1) <> ifnull(pc.Dim_ProcurementId,-2);


/* Q2 - column35 - Dim_Currencyid */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_Currencyid = c.Dim_Currencyid		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_Currencyid,-1) <> ifnull(c.Dim_Currencyid,-2);


/* Q2 - column36 - dd_SalesOrderNo */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dd_SalesOrderNo = p.PLAF_KDAUF	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderNo,'xx') <> ifnull(p.PLAF_KDAUF,'yy');


/* Q2 - column37 - dd_SalesOrderItemNo */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dd_SalesOrderItemNo = p.PLAF_KDPOS	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderItemNo,-1) <> ifnull(p.PLAF_KDPOS,-2);

/* Q2 - column38 - dd_SalesOrderScheduleNo */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dd_SalesOrderScheduleNo = p.PLAF_KDEIN	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SalesOrderScheduleNo,-1) <> ifnull(p.PLAF_KDEIN,-2);

/* Q2 - column39 - ct_GRProcessingTime */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.ct_GRProcessingTime = p.PLAF_WEBAZ	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_GRProcessingTime,-1) <> ifnull(p.PLAF_WEBAZ,-2);

/* Q2 - column40 - ct_QtyIssued */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.ct_QtyIssued = p.PLAF_WAMNG	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyIssued,-1) <> ifnull(p.PLAF_WAMNG,-2);

/* Q2 - column41 - ct_QtyCommitted */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.ct_QtyCommitted = p.PLAF_VFMNG	
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.ct_QtyCommitted,-1) <> ifnull(p.PLAF_VFMNG,-2);

/* Q2 - column42 - dd_SequenceNo */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dd_SequenceNo = ifnull(p.PLAF_SEQNR, 'Not Set')
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_SequenceNo,'xx') <> ifnull(p.PLAF_SEQNR, 'Not Set');

/* Q2 - column43 - dd_PlannScenario */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dd_PlannScenario = ifnull(p.PLAF_PLSCN, 'Not Set')
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_PlannScenario,'xx') <> ifnull(p.PLAF_VFMNG,'Not Set');

/* Q2 - column44 - amt_StdUnitPrice */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.amt_StdUnitPrice = 0		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.amt_StdUnitPrice,-1) <> 0;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,tmp_fpo_mbew_no_bwtar sp
SET po.amt_StdUnitPrice = sp.calcPrice
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND sp.MATNR = dp.PartNumber AND sp.BWKEY = pl.ValuationArea
AND ifnull(po.amt_StdUnitPrice,-1) <> ifnull(sp.calcPrice,-2);

/* Q2 - column45 - Dim_MRPControllerId */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.Dim_MRPControllerId = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.Dim_MRPControllerId,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom,Dim_MRPController mc
SET po.Dim_MRPControllerId = mc.Dim_MRPControllerId
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND mc.MRPController = PLAF_DISPO AND mc.Plant = PLAF_PLWRK
AND ifnull(po.Dim_MRPControllerId,-1) <> ifnull(mc.Dim_MRPControllerId,-2);


/* Q2 - column46 - dd_AgreementNo */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dd_AgreementNo = ifnull(p.PLAF_KONNR, 'Not Set')
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_AgreementNo,'xx') <> ifnull(p.PLAF_KONNR,'Not Set');

/* Q2 - column47 - dd_AgreementItemNo */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dd_AgreementItemNo = ifnull(p.PLAF_KTPNR, 0)
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dd_AgreementItemNo,-1) <> ifnull(p.PLAF_KTPNR,0);

/* Q2 - column48 - dim_dateidscheduledstart */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_dateidscheduledstart = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_dateidscheduledstart,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
	 kbko k,
        dim_unitofmeasure uom, dim_date dd
SET po.dim_dateidscheduledstart = dd.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND k.kbko_gstrs = dd.DateValue
AND p.plaf_plnum = k.kbko_plnum
AND pl.CompanyCode = dd.CompanyCode
AND ifnull(po.dim_dateidscheduledstart,-1) <> ifnull(dd.dim_dateid,-2);

/* Q2 - column49 - dim_dateidscheduledfinish */
UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
        dim_unitofmeasure uom
SET po.dim_dateidscheduledfinish = 1		
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND ifnull(po.dim_dateidscheduledfinish,-1) <> 1;

UPDATE fact_planorder po
   FROM plaf p,
        dim_plant pl,
        dim_company dc,
        dim_currency c,
        dim_part dp,
	 kbko k,
        dim_unitofmeasure uom, dim_date dd
SET po.dim_dateidscheduledfinish = dd.dim_dateid
WHERE     po.dd_PlanOrderNo = p.plaf_plnum
AND po.Dim_ActionStateid = 2
AND pl.PlantCode = p.PLAF_PLWRK
AND dc.CompanyCode = pl.CompanyCode
AND dp.PartNumber = p.PLAF_MATNR
AND dp.Plant = p.PLAF_PLWRK
AND uom.UOM = p.plaf_meins
AND dc.Currency = c.CurrencyCode
AND pl.CompanyCode = dd.CompanyCode
AND k.kbko_gltrs = dd.DateValue
AND p.plaf_plnum = k.kbko_plnum
AND ifnull(po.dim_dateidscheduledfinish,-1) <> ifnull(dd.dim_dateid,-2);


call vectorwise (combine 'fact_planorder');

select current_time;

select 'START OF PROC vw_bi_populate_planorder_fact_2',TIMESTAMP(LOCAL_TIMESTAMP);

/*Insert into the fact table*/
delete from NUMBER_FOUNTAIN where table_name = 'fact_planorder';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_planorder',ifnull(max(fact_planorderid),0)
FROM fact_planorder;

\i /db/schema_migration/bin/wrapper_optimizedb.sh plaf;
\i /db/schema_migration/bin/wrapper_optimizedb.sh kbko;

drop table if exists fact_planorder_tmp;
create table fact_planorder_tmp as select po.dd_PlanOrderNo FROM fact_planorder po;

INSERT INTO fact_planorder(fact_planorderid,
                           Dim_ActionStateid,
                           Dim_Companyid,
                           Dim_Partid,
                           Dim_mpnid,
                           Dim_StorageLocationid,
                           Dim_Plantid,
                           Dim_UnitOfMeasureid,
                           Dim_PurchaseOrgid,
                           Dim_Vendorid,
                           Dim_FixedVendorid,
                           Dim_SpecialProcurementid,
                           Dim_ConsumptionTypeid,
                           Dim_AccountCategoryid,
                           Dim_SpecialStockid,
                           dim_bomstatusid,
                           dim_bomusageid,
                           dim_objecttypeid,
                           dim_productionschedulerid,
                           dim_schedulingerrorid,
                           dim_tasklisttypeid,
                           dim_ordertypeid,
                           Dim_dateidStart,
                           Dim_dateidFinish,
                           Dim_dateidOpening,
                           ct_QtyTotal,
                           ct_Completed,
                           amt_ExtendedPrice,
                           dd_PlanOrderNo,
                           dd_bomexplosionno,
                           ct_QtyReduced,
                           Dim_PlanOrderStatusid,
                           Dim_DateidConversionDate,
                           Dim_DateidProductionStart,
                           Dim_DateidProductionFinish,
                           Dim_DateidExplosion,
                           Dim_DateidAction,
                           dim_scheduletypeid,
                           dim_availabilityconfirmationid,
                           dim_currencyid,
						   dim_currencyid_TRA,
						   dim_currencyid_GBL,
						   amt_ExchangeRate,
						   amt_ExchangeRate_GBL,
                           Dim_ProcurementId,
                           dd_SalesOrderNo,
                           dd_SalesOrderItemNo,
                           dd_SalesOrderScheduleNo,
                           ct_GRProcessingTime,
                           ct_QtyIssued,
                           ct_QtyCommitted,
                           dd_SequenceNo,
                           dd_PlannScenario,
                           amt_StdUnitPrice,
                           Dim_MRPControllerId,
                           dd_AgreementNo,
                           dd_AgreementItemNo/*,
                                                   dim_dateidscheduledstart,
                                                   dim_dateidscheduledfinish*/)
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_planorder') + row_number() over ()) fact_planorderid,
                  2 Dim_ActionStateid,
          Dim_Companyid,
          dp.Dim_Partid,
          ifnull(
             (SELECT dim_partid
                FROM dim_part mpn
               WHERE     mpn.PartNumber = p.PLAF_EMATN
                     AND mpn.Plant = p.PLAF_PLWRK),
             1)
             Dim_mpnid,
          ifnull(
             (SELECT Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE     sl.LocationCode = p.plaf_lgort
                     AND sl.Plant = p.PLAF_PLWRK),
             1)
             Dim_StorageLocationid,
          Dim_Plantid,
          Dim_UnitOfMeasureid,
          Dim_PurchaseOrgid,
          ifnull(
             (SELECT Dim_Vendorid
                FROM dim_vendor dv
               WHERE dv.VendorNumber = p.PLAF_EMLIF),
             1)
             Dim_Vendorid,
          ifnull(
             (SELECT Dim_Vendorid
                FROM dim_vendor fv
               WHERE fv.VendorNumber = p.PLAF_FLIEF),
             1)
             Dim_FixedVendorid,
          ifnull(
             (SELECT Dim_SpecialProcurementid
                FROM dim_specialprocurement sp
               WHERE sp.specialprocurement = p.PLAF_SOBES),
             1)
             Dim_SpecialProcurementid,
          ifnull(
             (SELECT Dim_ConsumptionTypeid
                FROM dim_consumptiontype dcp
               WHERE dcp.ConsumptionCode = p.PLAF_KZVBR),
             1)
             Dim_ConsumptionTypeid,
          ifnull((SELECT Dim_AccountCategoryid
                    FROM dim_accountcategory ac
                   WHERE ac.Category = p.plaf_knttp),
                 1)
             Dim_AccountCategoryid,
          ifnull(
             (SELECT Dim_SpecialStockid
                FROM dim_specialstock st
               WHERE st.specialstockindicator = p.PLAF_SOBKZ),
             1)
             Dim_SpecialStockid,
          ifnull(
             (SELECT dim_bomstatusid
                FROM dim_bomstatus bs
               WHERE bs.BOMStatusCode = p.plaf_ststa),
             1)
             dim_bomstatusid,
          ifnull(
             (SELECT dim_bomusageid
                FROM dim_bomusage bu
               WHERE bu.BOMUsageCode = p.plaf_stlan),
             1)
             dim_bomusageid,
          ifnull(
             (SELECT dim_objecttypeid
                FROM dim_objecttype ot
               WHERE ot.ObjectType = p.PLAF_OBART),
             1)
             dim_objecttypeid,
          ifnull(
             (SELECT dim_productionschedulerid
                FROM dim_productionscheduler ps
               WHERE     ps.ProductionScheduler = p.PLAF_PLGRP
                     AND ps.Plant = p.PLAF_PLWRK),
             1)
             dim_productionschedulerid,
          ifnull(
             (SELECT dim_schedulingerrorid
                FROM dim_schedulingerror se
               WHERE se.SchedulingErrorCode = p.PLAF_TRMER),
             1)
             dim_schedulingerrorid,
          ifnull(
             (SELECT dim_tasklisttypeid
                FROM dim_tasklisttype tst
               WHERE tst.TaskListTypeCode = p.PLAF_PLNTY),
             1)
             dim_tasklisttypeid,
          ifnull(
             (SELECT dim_ordertypeid
                FROM dim_ordertype ordt
               WHERE ordt.OrderTypeCode = p.PLAF_PAART),
             1)
             dim_ordertypeid,
          ds.dim_dateid Dim_dateidStart,
                  ifnull(
                     (SELECT df.dim_dateid
                            FROM dim_date df
                           WHERE df.DateValue = p.PLAF_PEDTR
                                     AND pl.CompanyCode = df.CompanyCode),
                     1)
             Dim_dateidFinish,
                  ifnull(
                     (SELECT dop.dim_dateid
                            FROM dim_date dop
                           WHERE dop.DateValue = p.PLAF_PERTR
                                     AND pl.CompanyCode = dop.CompanyCode),
                     1)
             Dim_dateidOpening,
          p.PLAF_GSMNG ct_QtyTotal,
          0 ct_Completed,
          /*ifnull(
             getStdPrice(pl.CompanyCode,
                         pl.PlantCode,
                         PLAF_MATNR,
                         ds.CalendarYear,
                         ds.FinancialMonthNumber,
                         CASE WHEN PLAF_UMREZ = 0 THEN 1 ELSE PLAF_UMREZ END,
                         CASE WHEN PLAF_UMREN = 0 THEN 1 ELSE PLAF_UMREN END,
                         NULL,
                         0),
             0)*/
                  ifnull((select standardprice from tmp_getStdPrice
                                  where pCompanyCode = pl.CompanyCode and
                                            pPlant = pl.PlantCode and
                                            pMaterialNo = PLAF_MATNR and
                                            pFiYear = ds.CalendarYear and
                                            pPeriod = ds.FinancialMonthNumber and
						fact_script_name = 'bi_populate_planorder_fact' and 
                                            vUMREZ = CASE WHEN PLAF_UMREZ = 0 THEN 1 ELSE PLAF_UMREZ END and
                                            vUMREN = CASE WHEN PLAF_UMREN = 0 THEN 1 ELSE PLAF_UMREN END), 0)
          * p.PLAF_GSMNG
             amt_ExtendedPrice,
          p.PLAF_PLNUM dd_PlanOrderNo,
          p.PLAF_SERNR dd_bomexplosionno,
          p.plaf_ABMNG ct_QtyReduced,
          1 Dim_PlanOrderStatusid,
          1 Dim_DateidConversionDate,
                  ifnull(
                         (SELECT df.dim_dateid
                            FROM dim_date df
                           WHERE df.DateValue = p.PLAF_TERST
                                         AND pl.CompanyCode = df.CompanyCode),
                         1)
             Dim_DateidProductionStart,
                  ifnull(
                         (SELECT df.dim_dateid
                            FROM dim_date df
                           WHERE df.DateValue = p.PLAF_TERED
                                         AND pl.CompanyCode = df.CompanyCode),
                         1)
             Dim_DateidProductionFinish,
                  ifnull(
                         (SELECT df.dim_dateid
                            FROM dim_date df
                           WHERE df.DateValue = p.PLAF_PALTR
                                         AND pl.CompanyCode = df.CompanyCode),
                         1)
             Dim_DateidExplosion,
                  ifnull(
                         (SELECT df.dim_dateid
                            FROM dim_date df
                           WHERE df.DateValue = p.PLAF_MDACD
                                         AND pl.CompanyCode = df.CompanyCode),
                         1)
             Dim_DateidAction,
          ifnull(
             (SELECT st.dim_scheduletypeid
                FROM dim_scheduletype st
               WHERE st.ScheduleTypeCode = p.PLAF_LVSCH),
             1)
             dim_scheduletypeid,
          ifnull(
             (SELECT acf.dim_availabilityconfirmationid
                FROM dim_availabilityconfirmation acf
               WHERE acf.AvailabilityConfirmationCode = p.PLAF_MDPBV),
             1)
             dim_availabilityconfirmationid,
          c.dim_currencyid dim_currencyid,
		  c.dim_currencyid dim_currencyid_TRA,	/*Default */
		  ( SELECT cur.dim_currencyid FROM dim_currency cur where cur.currencycode = pGlobalCurrency ) dim_currencyid_GBL,
		  1 amt_ExchangeRate,		/*Default */
		  
		  ifnull((select z.exchangeRate 
		  from tmp_getExchangeRate1 z 
		  where z.pFromCurrency  = dc.Currency 
		  and z.fact_script_name = 'bi_populate_planorder_fact' 
		  and z.pToCurrency = pGlobalCurrency 
		  and z.pDate = PLAF_PEDTR ),1) amt_ExchangeRate_GBL,	/* Order finish date used to calculate exchg rate*/
          ifnull(
             (SELECT pc.dim_procurementid
                FROM Dim_Procurement pc
               WHERE pc.procurement = p.PLAF_BESKZ),
             1),
          p.PLAF_KDAUF dd_SalesOrderNo,
          p.PLAF_KDPOS dd_SalesOrderItemNo,
          p.PLAF_KDEIN dd_SalesOrderScheduleNo,
          p.PLAF_WEBAZ ct_GRProcessingTime,
          p.PLAF_WAMNG ct_QtyIssued,
          p.PLAF_VFMNG ct_QtyCommitted,
          ifnull(p.PLAF_SEQNR, 'Not Set'),
          ifnull(p.PLAF_PLSCN, 'Not Set'),
          ifnull(
             (SELECT ifnull(
                        (CASE sp.VPRSV
                            WHEN 'S' THEN (sp.STPRS / sp.PEINH)
                            WHEN 'V' THEN (sp.VERPR / sp.PEINH)
                         END),
                        0)
                FROM mbew_no_bwtar sp
               WHERE sp.MATNR = dp.PartNumber AND sp.BWKEY = pl.ValuationArea
                     AND ((sp.LFGJA * 100) + sp.LFMON) =
                            (SELECT max((x.LFGJA * 100) + x.LFMON)
                               FROM mbew_no_bwtar x
                              WHERE x.MATNR = sp.MATNR AND x.BWKEY = sp.BWKEY
                                    AND ((x.VPRSV = 'S' AND x.STPRS > 0)
                                         OR (x.VPRSV = 'V' AND x.VERPR > 0)))
                     AND ((sp.VPRSV = 'S' AND sp.STPRS > 0)
                          OR (sp.VPRSV = 'V' AND sp.VERPR > 0))
               /*LIMIT 1*/),
             0)
             amt_StdUnitPrice,
          ifnull(
             (SELECT mc.Dim_MRPControllerId
                FROM Dim_MRPController mc
               WHERE     mc.MRPController = PLAF_DISPO
                     AND mc.Plant = PLAF_PLWRK),
             1)
             Dim_MRPControllerId,
          ifnull(PLAF_KONNR,'Not Set') dd_AgreementNo,
          ifnull(PLAF_KTPNR,0) dd_AgreementItemNo/*,
                  ifnull((SELECT dd.dim_dateid
                                        FROM dim_date dd
                                   WHERE dd.DateValue = k.kbko_gstrs
                                                 AND pl.CompanyCode = dd.CompanyCode),1) dim_dateidscheduledstart,
                  ifnull((SELECT dd.dim_dateid
                                    FROM dim_date dd
                                   WHERE dd.DateValue = k.kbko_gltrs
                                                 AND pl.CompanyCode = dd.CompanyCode),1) dim_dateidscheduledfinish*/
     FROM tmp_pGlobalCurrency_planorder,plaf p
          INNER JOIN dim_plant pl
             ON pl.PlantCode = p.PLAF_PLWRK
          INNER JOIN dim_company dc
             ON dc.CompanyCode = pl.CompanyCode
          INNER JOIN Dim_Currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date ds
             ON ds.DateValue = p.PLAF_PSTTR
                AND pl.CompanyCode = ds.CompanyCode
          INNER JOIN dim_part dp
             ON     dp.PartNumber = p.PLAF_MATNR
                AND dp.Plant = p.PLAF_PLWRK
          INNER JOIN dim_unitofmeasure uom
             ON uom.UOM = p.plaf_meins
          INNER JOIN dim_purchaseorg pog
             ON pog.PurchaseOrgCode = pl.PurchOrg
			 
                  /*INNER JOIN kbko k
                         ON p.plaf_plnum = k.kbko_plnum*/
    WHERE NOT EXISTS
             (SELECT 1
                FROM fact_planorder_tmp po
               WHERE po.dd_PlanOrderNo = p.plaf_plnum);

/*Update schedule start and end date*/
/*UPDATE fact_planorder po
FROM kbko k,dim_date dd,dim_company dc
SET dim_dateidscheduledstart = dd.dim_dateid
WHERE dd_PlanOrderNo = k.kbko_plnum AND k.kbko_gstrs = dd.DateValue
AND po.Dim_Companyid = dc.Dim_Companyid
AND dd.CompanyCode = dc.CompanyCode
AND ifnull(dim_dateidscheduledstart,-1) <> ifnull(dd.dim_dateid,-2);

UPDATE fact_planorder po
FROM KBKO k,dim_date dd,dim_company dc
SET dim_dateidscheduledfinish = dd.dim_dateid
WHERE dd_PlanOrderNo = k.kbko_plnum AND k.kbko_gltrs = dd.DateValue
AND po.Dim_Companyid = dc.Dim_Companyid
AND dd.CompanyCode = dc.CompanyCode
AND ifnull(dim_dateidscheduledfinish,-1) <> ifnull(dd.dim_dateid,-2);

UPDATE fact_planorder po
  FROM dim_part pt, dim_profitcenter pc
SET po.dim_profitcenterid = pc.dim_profitcenterid
WHERE po.dim_PartId = pt.Dim_PartId
 AND pc.ProfitCenterCode = pt.ProfitCenterCode
 AND pc.validto >= current_date
 AND pc.RowIsCurrent = 1
AND ifnull(po.dim_profitcenterid,1) <> ifnull(pc.dim_profitcenterid,1);

UPDATE fact_planorder po
SET po.dim_profitcenterid = 1
WHERE po.dim_profitcenterid IS NULL;

/*Update plan order miscellaneous dimension*/
UPDATE fact_planorder po
        FROM
       dim_planordermisc m,
       plaf p
   SET po.Dim_PlanOrderMiscid = m.Dim_PlanOrderMiscid
 WHERE     po.dd_PlanOrderNo = ifnull(p.PLAF_PLNUM, 'Not Set')
       AND ProductionDateScheduling = ifnull(p.PLAF_PRSCH, 'Not Set')
       AND PlanningWithoutFinalAssembly = ifnull(p.PLAF_VRPLA, 'Not Set')
       AND SubcontractingVendor = ifnull(p.PLAF_LBLKZ, 'Not Set')
       AND ConversionIndicator = ifnull(p.PLAF_UMSKZ, 'Not Set')
       AND FirmingIndicator = ifnull(p.PLAF_AUFFX, 'Not Set')
       AND FixingIndicator = ifnull(p.PLAF_STLFX, 'Not Set')
       AND SchedulingIndicator = ifnull(p.PLAF_TRMKZ, 'Not Set')
       AND AssemblyOrderProcedures = ifnull(p.PLAF_MONKZ, 'Not Set')
       AND LeadingOrder = ifnull(p.PLAF_PRNKZ, 'Not Set');
	   
	   
/* Update currencies and exchange rates from fact_productionorder */	
UPDATE    fact_planorder po
       FROM
          fact_productionorder pod
   SET po.dim_currencyID = pod.dim_currencyID
   WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
   AND po.dim_currencyID <> pod.dim_currencyID;
   
UPDATE    fact_planorder po
       FROM
          fact_productionorder pod
   SET po.dim_currencyID_TRA = pod.dim_currencyID_TRA
   WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
   AND po.dim_currencyID_TRA <> pod.dim_currencyID_TRA;

   UPDATE    fact_planorder po
       FROM
          fact_productionorder pod
   SET po.dim_currencyID_GBL = pod.dim_currencyID_GBL
   WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
   AND po.dim_currencyID_GBL <> pod.dim_currencyID_GBL;
   
   
   UPDATE    fact_planorder po
       FROM
          fact_productionorder pod
   SET po.amt_ExchangeRate = pod.amt_ExchangeRate
   WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
   AND po.amt_ExchangeRate <> pod.amt_ExchangeRate;    
   
   UPDATE    fact_planorder po
       FROM
          fact_productionorder pod
   SET po.amt_ExchangeRate_GBL = pod.amt_ExchangeRate_GBL
   WHERE   pod.dd_ordernumber = po.dd_ProductionOrderNo
   AND po.amt_ExchangeRate_GBL <> pod.amt_ExchangeRate_GBL; 

*/


/* Update amounts to transaction currencies where applicable */

UPDATE    fact_planorder po
SET		  amt_ExtendedPrice = amt_ExtendedPrice /   amt_ExchangeRate		/* As extended price is in local curr, change that to tran curr */
WHERE	  dim_currencyID <> dim_currencyID_TRA;


UPDATE    fact_planorder po
SET		  amt_StdUnitPrice = amt_StdUnitPrice /   amt_ExchangeRate		/* As amt_StdUnitPrice is in local curr, change that to tran curr */
WHERE	  dim_currencyID <> dim_currencyID_TRA;


UPDATE    fact_planorder po
SET		  amt_ExtendedPrice_GBL = amt_ExtendedPrice *   amt_ExchangeRate_GBL
WHERE	  dim_currencyID_TRA <> dim_currencyID_GBL;
   

drop table if exists fact_planorder_tmp;
call vectorwise (combine 'fact_planorder');
