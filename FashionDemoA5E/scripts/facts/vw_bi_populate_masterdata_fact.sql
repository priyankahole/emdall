/**************************************************************************************************************/
/*   Script         : fact_masterdataaccuracy                                                */
/*   Author         : Andrian Russu                                                                                */
/*   Created On     : 18 May 2015                                                                            */ 
/*   Description    : Populate script for  fact_masterdataaccuracy from 2 sources fact_inspectionlot and fact_productionorder in order to create a monthly snapshot */
/**************************************************************************************************************/
/*  Date             By         Version	    Desc                                                               */
/**************************************************************************************************************/

/*Populating from fact_inspectionlot*/
 delete from number_fountain m where m.table_name = 'fact_masterdataaccuracy';
insert into number_fountain
select 'fact_masterdataaccuracy',
 ifnull(max(f.fact_masterdataaccuracyid ), 
              1)
from fact_masterdataaccuracy f;


  insert into fact_masterdataaccuracy
(
fact_masterdataaccuracyid,
dw_insert_date,
dw_update_date,
dd_orderno,
dd_objecttype,
dim_partid,
dim_plantid,
dim_InpectionEndDateid,
dim_GraceEndDateid,
dim_ActualInspEndDateid,
dd_batchno,
dd_changedby,
dd_createdby,
dd_documentitemno,
dd_documentno,
dd_inspectionlotno,
dd_materialdocitemno,
dd_materialdocno,
dd_materialdocyear,
dd_scheduleno,
dd_usagedecisionchangeby,
dd_usagedecisionmadeby,
dd_GraceDays,
dim_inspectionlotoriginid,
 dim_inspusagedecisionid,
 dim_inspectionlotstatusid
)
select 
(select max_id 
 from number_fountain 
 where table_name = 'fact_masterdataaccuracy') + row_number() over() AS fact_masterdataaccuracyid,
 current_timestamp as dw_insert_date,
 current_timestamp as dw_update_date,
 tmp.dd_orderno,
 'Inspections' as dd_objecttype,
 dim_partid,
 dim_plantid,
 dim_dateidinspectionend as dim_InpectionEndDateid,
 dim_dateidinspectionend as dim_GraceEndDateid,
 dim_dateidactualinspectionend as dim_ActualInspEndDateid,
 dd_batchno,
dd_changedby,
dd_createdby,
dd_documentitemno,
dd_documentno,
dd_inspectionlotno,
dd_materialdocitemno,
dd_materialdocno,
dd_materialdocyear,
dd_scheduleno,
dd_usagedecisionchangeby,
dd_usagedecisionmadeby,
 0 as dd_GraceDays,
 dim_inspectionlotoriginid,
 dim_inspusagedecisionid,
 dim_inspectionlotstatusid
 from 
 fact_inspectionlot tmp 
 where not exists 
 (
 select dd_orderno
 from fact_masterdataaccuracy f_snap
 where 
 tmp.dd_orderno = f_snap.dd_orderno 
 and tmp.dd_inspectionlotno = f_snap.dd_inspectionlotno
 and f_snap.dd_objecttype = 'Inspections'
 ) ;

 
 
 
 
/*update dd_OpenClosed */
 update fact_masterdataaccuracy
 set dd_OpenClosed = case 
                          when dim_ActualInspEndDateid = 1 then 'Open'
						  else 'Closed'
                     end,
      dw_update_date =current_timestamp
                     
where   dd_objecttype = 'Inspections' 
and dd_OpenClosed <> case 
                          when dim_ActualInspEndDateid = 1 then 'Open'
						  else 'Closed'
                     end;



/*Populating from fact_productionorder */
delete from number_fountain m where m.table_name = 'fact_masterdataaccuracy';
insert into number_fountain
select 'fact_masterdataaccuracy',
 ifnull(max(f.fact_masterdataaccuracyid ), 
              1)
from fact_masterdataaccuracy f;




insert into fact_masterdataaccuracy
(
fact_masterdataaccuracyid,
dw_insert_date,
dw_update_date,
dd_orderno,
dd_objecttype,
dim_partid,
dim_plantid,
dim_ScheduledFinishDateid,
dim_GraceEndDateid,
dim_TechnicalCompletionDateid,
dd_bomexplosionno,
dd_bomlevel,
dd_batchno,
dd_operationnumber,
dd_orderdescription,
dd_orderitemno,
dd_plannedorderno,
dd_salesorderdeliveryscheduleno,
dd_salesorderitemno,
dd_salesorderno,
dd_sequenceno,
dd_workcenter,
dd_GraceDays,
 dd_cancelledorder,
 dim_dateidconfirmedorderfinish,
 dim_dateidscheduledfinishheader,
 dd_unimitedoverdelivery_merck
)
select 
(select max_id 
 from number_fountain 
 where table_name = 'fact_masterdataaccuracy') + row_number() over() AS fact_masterdataaccuracyid,
 current_timestamp as dw_insert_date,
 current_timestamp as dw_update_date,
 fp.dd_ordernumber,
 'Process Order' as dd_objecttype,
 dim_partidheader as dim_partid,
 dim_plantid,
 dim_dateidscheduledfinish as dim_ScheduledFinishDateid,
 dim_dateidscheduledfinish as dim_GraceEndDateid,
 dim_dateidtechnicalcompletion as dim_TechnicalCompletionDateid,
 dd_bomexplosionno,
dd_bomlevel,
dd_batch as dd_batchno,
dd_operationnumber,
dd_orderdescription,
dd_orderitemno,
dd_plannedorderno,
dd_salesorderdeliveryscheduleno,
dd_salesorderitemno,
dd_salesorderno,
dd_sequenceno,
dd_workcenter,
 0 as dd_GraceDays,
 dd_cancelledorder,
 dim_dateidconfirmedorderfinish,
 dim_dateidscheduledfinishheader,
 dd_unimitedoverdelivery_merck
 from 
 fact_productionorder fp
 where not exists (select dd_ordernumber from fact_masterdataaccuracy f_snap where fp.dd_ordernumber = f_snap.dd_orderno and dd_objecttype = 'Process Order' )
 and dd_orderitemno = 1;
 
 

					 
					 
/*update dd_OpenClosed */
 update fact_masterdataaccuracy
 set dd_OpenClosed = case 
                          when dim_TechnicalCompletionDateid = 1 then 'Open'
						  else 'Closed'
                     end,
      dw_update_date =current_timestamp
                     
where   dd_objecttype = 'Process Order' 
and dd_OpenClosed <> case 
                          when dim_TechnicalCompletionDateid = 1 then 'Open'
						  else 'Closed'
                     end;




/*update dd_PastDueStatus */
update fact_masterdataaccuracy a1
from  dim_date dd2,  dim_date dd3, dim_date dd4
set dd_PastDueStatus = case when dd_objecttype = 'Inspections' 
and dim_ActualInspEndDateid = 1 and DATE (a1.dw_insert_date)> dd2.datevalue then 'Past Due' 
                     when dd_objecttype = 'Inspections' and dd3.datevalue > dd2.datevalue then 'Past Due' 
	when dd_objecttype = 'Process Order' and dim_TechnicalCompletionDateid = 1 and DATE (a1.dw_insert_date) > dd2.datevalue then 'Past Due'
                      when dd_objecttype = 'Process Order' and dd4.datevalue > dd2.datevalue	then 'Past Due'
                        else 'Not Past Due'
                        end,
     dw_update_date =current_timestamp						
where   dim_GraceEndDateid = dd2.dim_dateid
and dim_ActualInspEndDateid =dd3.dim_Dateid
and dim_TechnicalCompletionDateid =dd4.dim_Dateid
and  dd_PastDueStatus <> case when dd_objecttype = 'Inspections' and dim_ActualInspEndDateid = 1 and DATE (a1.dw_insert_date) > dd2.datevalue then 'Past Due' 
                            when dd_objecttype = 'Inspections' and dd3.datevalue > dd2.datevalue then 'Past Due' 
							when dd_objecttype = 'Process Order' and dim_TechnicalCompletionDateid = 1 and DATE (a1.dw_insert_date)> dd2.datevalue then 'Past Due'
                            when dd_objecttype = 'Process Order' and dd4.datevalue > dd2.datevalue	then 'Past Due'
                        else 'Not Past Due'
                        end;



 
 
 /*ct_OpenOrders */
 update fact_masterdataaccuracy
 set ct_OpenOrders = 1,
 dw_update_date =current_timestamp 
 where dd_OpenClosed = 'Open'
 and ct_OpenOrders <> 1;
 
 
 update fact_masterdataaccuracy
 set ct_OpenOrders = 0,
 dw_update_date =current_timestamp 
 where dd_OpenClosed = 'Closed'
 and ct_OpenOrders <> 0;
 
  /*ct_AllOrders */
 update fact_masterdataaccuracy
 set ct_AllOrders = 1,
 dw_update_date =current_timestamp
 where  ct_AllOrders <> 1;
 
 

 /* ct_ClosedPastDue*/ 
update fact_masterdataaccuracy
from dim_date dd1, dim_date dd2
set ct_ClosedPastDue = 1,
dw_update_date =current_timestamp
where dd_objecttype = 'Process Order' 
and dim_TechnicalCompletionDateid <> 1
and dd1.datevalue > dd2.datevalue     
and dim_TechnicalCompletionDateid = dd1.dim_dateid 
and dim_GraceEndDateid = dd2.dim_dateid
and  ct_ClosedPastDue <> 1;


update fact_masterdataaccuracy
from dim_date dd1, dim_date dd2
set ct_ClosedPastDue = 1,
dw_update_date =current_timestamp
where dd_objecttype = 'Inspections'
and dim_ActualInspEndDateid <> 1
and dd1.datevalue > dd2.datevalue     
and dim_ActualInspEndDateid = dd1.dim_dateid 
and dim_GraceEndDateid = dd2.dim_dateid
and ct_ClosedPastDue <> 1;

update fact_masterdataaccuracy
set ct_ClosedPastDue = 0 ,dw_update_date =current_timestamp
where ct_ClosedPastDue <> 1
and ct_ClosedPastDue <> 0;


 /* ct_OpenPastDue*/
update fact_masterdataaccuracy a1
from  dim_date dd2
 set ct_OpenPastDue = 1,
 dw_update_date =current_timestamp
where dd_objecttype = 'Process Order' 
and dim_TechnicalCompletionDateid = 1
and DATE (a1.dw_insert_date) > dd2.datevalue     
and dim_GraceEndDateid = dd2.dim_dateid
and  ct_OpenPastDue <> 1;  

   
update fact_masterdataaccuracy a1
from  dim_date dd2
 set ct_OpenPastDue = 1,
 dw_update_date =current_timestamp
where dd_objecttype = 'Inspections'
and dim_ActualInspEndDateid = 1
and DATE (a1.dw_insert_date) > dd2.datevalue     
and dim_GraceEndDateid = dd2.dim_dateid
and ct_OpenPastDue <> 1; 

update fact_masterdataaccuracy
set ct_OpenPastDue = 0, dw_update_date =current_timestamp
where ct_OpenPastDue <> 1
and ct_OpenPastDue <> 0;
 
 
   update fact_masterdataaccuracy a from dim_date b, dim_date d
 set a.dd_pastduecategory = case when ansidate(date('today')) - d.datevalue between 1 and 7 then '001-007 Days'
                                 when ansidate(date('today')) - d.datevalue between 8 and 14 then '008-014 Days'
                                 when ansidate(date('today')) - d.datevalue between 15 and 21 then '015-021 Days'
                                 when ansidate(date('today')) - d.datevalue between 22 and 89 then '022-089 Days'
                                 when ansidate(date('today')) - d.datevalue between 90 and 179 then '090-179 Days'
                                 when ansidate(date('today'))- d.datevalue between 180 and 269 then '180-269 Days'
                                 when ansidate(date('today')) - d.datevalue between 270 and 369 then '270-369 Days'
                                 when ansidate(date('today')) - d.datevalue > 369 then '> 369 Days' end,
     a.dw_update_date = current_timestamp
 where a.dim_actualinspenddateid = b.dim_dateid and
       a.dim_graceenddateid = d.dim_dateid and 
       a.dd_objecttype = 'Inspections' and 
       b.datevalue = '0001-01-01' and 
       ansidate(date('today'))> d.datevalue;
       

 
  update fact_masterdataaccuracy a from dim_date b,  dim_date d
 set a.dd_pastduecategory = case when b.datevalue - d.datevalue between 1 and 7 then '001-007 Days'
                                 when b.datevalue - d.datevalue between 8 and 14 then '008-014 Days'
                                 when b.datevalue - d.datevalue between 15 and 21 then '015-021 Days'
                                 when b.datevalue - d.datevalue between 22 and 89 then '022-089 Days'
                                 when b.datevalue - d.datevalue between 90 and 179 then '090-179 Days'
                                 when b.datevalue - d.datevalue between 180 and 269 then '180-269 Days'
                                 when b.datevalue - d.datevalue between 270 and 369 then '270-369 Days'
                                 when b.datevalue - d.datevalue > 369 then '> 369 Days' end,
     a.dw_update_date = current_timestamp
 where a.dim_actualinspenddateid = b.dim_dateid and
       a.dim_graceenddateid = d.dim_dateid and 
       a.dd_objecttype = 'Inspections' and 
       b.datevalue  > d.datevalue;
       
   update fact_masterdataaccuracy a from dim_date b, dim_date d
 set a.dd_pastduecategory = case when ansidate(date('today'))- d.datevalue between 1 and 7 then '001-007 Days'
                                 when ansidate(date('today')) - d.datevalue between 8 and 14 then '008-014 Days'
                                 when ansidate(date('today')) - d.datevalue between 15 and 21 then '015-021 Days'
                                 when ansidate(date('today')) - d.datevalue between 22 and 89 then '022-089 Days'
                                 when ansidate(date('today')) - d.datevalue between 90 and 179 then '090-179 Days'
                                 when ansidate(date('today')) - d.datevalue between 180 and 269 then '180-269 Days'
                                 when ansidate(date('today')) - d.datevalue between 270 and 369 then '270-369 Days'
                                 when ansidate(date('today')) - d.datevalue > 369 then '> 369 Days' end,
     a.dw_update_date = current_timestamp
 where a.dim_technicalcompletiondateid = b.dim_dateid and
       a.dim_graceenddateid = d.dim_dateid and 
       a.dd_objecttype = 'Process Order' and 
       b.datevalue = '0001-01-01' and 
       ansidate(date('today')) > d.datevalue;
       
      update fact_masterdataaccuracy a from dim_date b, dim_date d
 set a.dd_pastduecategory = case when b.datevalue - d.datevalue between 1 and 7 then '001-007 Days'
                                 when b.datevalue - d.datevalue between 8 and 14 then '008-014 Days'
                                 when b.datevalue - d.datevalue between 15 and 21 then '015-021 Days'
                                 when b.datevalue - d.datevalue between 22 and 89 then '022-089 Days'
                                 when b.datevalue - d.datevalue between 90 and 179 then '090-179 Days'
                                 when b.datevalue - d.datevalue between 180 and 269 then '180-269 Days'
                                 when b.datevalue - d.datevalue between 270 and 369 then '270-369 Days'
                                 when b.datevalue - d.datevalue > 369 then '> 369 Days' end,
     a.dw_update_date = current_timestamp
 where a.dim_technicalcompletiondateid = b.dim_dateid and
       a.dim_graceenddateid = d.dim_dateid and 
       a.dd_objecttype = 'Process Order' and 
       b.datevalue  > d.datevalue;
	   
	 call vectorwise(combine 'fact_masterdataaccuracy');

 



