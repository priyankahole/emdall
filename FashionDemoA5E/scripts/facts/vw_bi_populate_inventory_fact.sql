 /**************************************************************************************************************/
/*   Script         :    */
/*   Author         : Lokesh */
/*   Created On     : 19 Aug 2013 */
/*   Description    : Stored Proc bi_populate_inventory_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   19 Aug 2013      Lokesh    1.0               Existing code migrated to Vectorwise.                                       */
/*                                                Run after 1.vw_funct_fiscal_year.inventory_fact_and_process_S031.sql */
/*                                                                                                2.vw_funct_fiscal_year.standard.sql and 3.vw_process_S031.sql    */
/******************************************************************************************************************/

/* Run after process_S031          */

DROP TABLE IF EXISTS tmp_inv_fact_cursor;
CREATE TABLE    tmp_inv_fact_cursor
(
done INT ,
pFiYear INT,
pCompanyCode CHAR(4),
pPlantCode CHAR(4),
pPeriod INT,
pDates Char(45),
pDates1 Char(45),
pFromDate ansidate,
pToDate ansidate,
pToYear INT,
pToMonth INT
);


INSERT INTO tmp_inv_fact_cursor
(pCompanyCode,pPlantCode,pFiYear,pPeriod)
SELECT DISTINCT BUKRS, BWKEY, LFGJA, LFMON
FROM (SELECT b.BUKRS, a.BWKEY, a.LFGJA, a.LFMON FROM MBEW a INNER JOIN T001K b ON a.BWKEY = b.BWKEY
                UNION
          SELECT b.BUKRS, a.BWKEY, a.LFGJA, a.LFMON FROM MBEWH a INNER JOIN T001K b ON a.BWKEY = b.BWKEY) x;


UPDATE tmp_inv_fact_cursor t
FROM tmp_funct_fiscal_year z
SET pDates = z.pReturn,pDates1 = z.pReturn
WHERE z.pCompanyCode = t.pCompanyCode
and z.FiscalYear = t.pFiYear
and z.Period = t.pPeriod
and z.fact_script_name = 'bi_populate_inventory_fact';

  UPDATE tmp_inv_fact_cursor t
  SET pFromDate = cast(substr(pDates, 1, 10) AS date);

  UPDATE tmp_inv_fact_cursor t
  SET pToDate = cast(substr(pDates, 12, 10) AS date);

  UPDATE tmp_inv_fact_cursor t
  set pToYear = Year(pToDate);

  UPDATE tmp_inv_fact_cursor t
  set pToMonth = Month(pToDate);


DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_inventory';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_inventory', ifnull(max(fact_inventoryid), 0) FROM fact_inventory;


  INSERT INTO fact_inventory(Dim_Plantid,
                            Dim_Partid,
                            Dim_producthierarchyid,
                            Dim_Companyid,
                            Dim_DateidRecorded,
                            Dim_DateidRecordedEnd,fact_inventoryid)
   (SELECT DISTINCT
           Dim_Plantid,
           Dim_Partid,
           ifnull(
              (SELECT dim_producthierarchyid
                 FROM dim_producthierarchy dph
                WHERE dph.ProductHierarchy = p.ProductHierarchy
                      AND dph.RowIsCurrent = 1),
              1)
              dim_producthierarchyid,
           cc.Dim_Companyid,
           ddr.Dim_Dateid,
           ddre.Dim_Dateid,
                   (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventory') + row_number() over ()
      FROM tmp_inv_fact_cursor t,dim_plant pl,
           dim_date ddr,
           dim_date ddre,
                    S031 a
                 INNER JOIN
                    T001K b
                 ON a.WERKS = b.BWKEY
              INNER JOIN
                 dim_part p
              ON     a.MATNR = p.PartNumber
                 AND a.WERKS = p.Plant
                 AND a.MATNR IS NOT NULL
           INNER JOIN
              dim_company cc
           ON b.BUKRS = cc.CompanyCode
     WHERE     b.BWKEY = pl.PlantCode
           AND ddr.DateValue = pFromDate
           AND b.BUKRS = ddr.companycode
           AND ddre.DateValue = pToDate
           AND b.BUKRS = ddre.companycode
           AND b.BUKRS = pCompanyCode
           AND b.BWKEY = pPlantCode
           AND a.SPMON_YEAR = pToYear
           AND a.SPMON_MONTH = pToMonth
           AND 1 <> ifnull(
                 (SELECT 1
                    FROM fact_inventory fi
                   WHERE     fi.Dim_Companyid = cc.Dim_Companyid
                         AND fi.Dim_DateidRecorded = ddr.Dim_Dateid
                         AND ddr.CompanyCode = b.BUKRS
                         AND fi.Dim_DateidRecordedEnd = ddre.Dim_Dateid
                         AND ddre.CompanyCode = b.BUKRS
                         AND fi.Dim_Partid = p.Dim_Partid
                         AND p.Plant = b.BWKEY
                         AND fi.Dim_Plantid = pl.Dim_Plantid),0));

DROP TABLE IF EXISTS tmp_inv_fact_cursor;


