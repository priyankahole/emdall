
--A] Local currency = Tran currency


SELECT first 5 distinct dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_TRA =  dim_currencyid;


--B] Local Currency <> Tran currency


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_TRA <>  dim_currencyid;


--C] GBL currency = TRAN curr


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_TRA =  dim_currencyid_GBL;


--D] GBL <> TRAN


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_TRA <>  dim_currencyid_GBL;


--E] STAT = TRAN


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_TRA =  dim_currencyid_STAT;


--F] STAT <> TRAN


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_TRA <>  dim_currencyid_STAT;


--G] STAT = LOCAL


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_STAT =  dim_currencyid;


--H] STAT <> LOCAL


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_STAT <>  dim_currencyid;


--I] GBL = STAT


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_STAT =  dim_currencyid_GBL;


--J] GBL <> STAT


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_STAT <>  dim_currencyid_GBL;



--K] GBL = local


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_GBL =  dim_currencyid;


--L] GBL <> local


SELECT first 5 dim_currencyid_TRA,dim_currencyid,amt_exchangerate,dim_currencyid_GBL,amt_exchangerate_GBL
, Dim_Currencyid_STAT, amt_exchangerate_STAT
FROM fact_salesorderdelivery
WHERE dim_currencyid_GBL <>  dim_currencyid;


\g
