Drop table if exists pGlobalCurrency_po_41;

Create table pGlobalCurrency_po_41(
pGlobalCurrency varchar(3) null);

Insert into pGlobalCurrency_po_41(pGlobalCurrency) values(null);

Update pGlobalCurrency_po_41
SET pGlobalCurrency =
       ifnull((SELECT property_value
                 FROM systemproperty
                WHERE property = 'customer.global.currency'),
              'USD');

UPDATE fact_excessandshortage mrp
FROM  dim_mrpexception mex,
       dim_mrpelement me,
       dim_date dn
SET ct_Completed = 1
WHERE     Dim_DateidActionClosed = 1
       AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
       AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
       AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
       AND mrp.dd_documentno <> 'Not Set'
	AND ct_Completed <> 1
       AND 1<> ifnull( (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE     mrp.dd_documentno = t.MDTB_DELNR
                          AND mrp.dd_documentitemno = t.MDTB_DELPS
                          AND mrp.dd_scheduleno = t.MDTB_DELET
                          AND t.MDTB_DELNR IS NOT NULL
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), ANSIDATE('0001-01-01'))
                          AND mrp.Dim_ActionStateid = 2),0);

UPDATE fact_excessandshortage mrp
FROM  dim_mrpexception mex,
       dim_mrpelement me,
       dim_date dn
   SET Dim_ActionStateid = 3
 WHERE     Dim_DateidActionClosed = 1
       AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
       AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
       AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
       AND mrp.dd_documentno <> 'Not Set'
	AND Dim_ActionStateid <> 3
       AND 1<>  ifnull( (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE     mrp.dd_documentno = t.MDTB_DELNR
                          AND mrp.dd_documentitemno = t.MDTB_DELPS
                          AND mrp.dd_scheduleno = t.MDTB_DELET
                          AND t.MDTB_DELNR IS NOT NULL
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), ANSIDATE('0001-01-01'))
                          AND mrp.Dim_ActionStateid = 2),0);

UPDATE fact_excessandshortage mrp
FROM  dim_mrpexception mex,
       dim_mrpelement me,
       dim_date dn
   SET Dim_DateidActionClosed =
          (SELECT dt.Dim_Dateid
             FROM dim_date dt, dim_company cc
            WHERE     dt.DateValue = current_date
                  AND cc.Dim_Companyid = mrp.Dim_Companyid
                  AND cc.CompanyCode = dt.CompanyCode)
 WHERE     Dim_DateidActionClosed = 1
       AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
       AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
       AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
       AND mrp.dd_documentno <> 'Not Set'
       AND 1<>  ifnull( (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE     mrp.dd_documentno = t.MDTB_DELNR
                          AND mrp.dd_documentitemno = t.MDTB_DELPS
                          AND mrp.dd_scheduleno = t.MDTB_DELET
                          AND t.MDTB_DELNR IS NOT NULL
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03 
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG 
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), ANSIDATE('0001-01-01'))
                          AND mrp.Dim_ActionStateid = 2),0)
AND Dim_DateidActionClosed <> (SELECT dt.Dim_Dateid
             FROM dim_date dt, dim_company cc
            WHERE     dt.DateValue = current_date
                  AND cc.Dim_Companyid = mrp.Dim_Companyid
                  AND cc.CompanyCode = dt.CompanyCode);
                         

UPDATE fact_excessandshortage mrp
FROM dim_part dp,
       dim_mrpexception mex,
       dim_mrpelement me,
       dim_date dn
   SET ct_Completed = 1
 WHERE     Dim_DateidActionClosed = 1
       AND mrp.Dim_Partid = dp.Dim_Partid
       AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
       AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
       AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
       AND mrp.dd_documentno ='Not Set'
	AND ct_Completed <> 1
       AND 1<>  ifnull( (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE dp.PartNumber = m.MDKP_MATNR
                          AND dp.Plant = m.MDKP_PLWRK
                          and t.MDTB_DELNR is null
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), ANSIDATE('0001-01-01'))
                          AND mrp.Dim_ActionStateid = 2),0);



UPDATE fact_excessandshortage mrp
FROM dim_part dp,
       dim_mrpexception mex,
       dim_mrpelement me,
       dim_date dn
   SET Dim_ActionStateid = 3
 WHERE     Dim_DateidActionClosed = 1
       AND mrp.Dim_Partid = dp.Dim_Partid
       AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
       AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
       AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
       AND mrp.dd_documentno ='Not Set'
	AND Dim_ActionStateid <> 3
       AND 1 <> ifnull( (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE dp.PartNumber = m.MDKP_MATNR
                          AND dp.Plant = m.MDKP_PLWRK
                          and t.MDTB_DELNR is null
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), ANSIDATE('0001-01-01'))
                          AND mrp.Dim_ActionStateid = 2),0);



UPDATE fact_excessandshortage mrp
FROM dim_part dp,
       dim_mrpexception mex,
       dim_mrpelement me,
       dim_date dn
   SET Dim_DateidActionClosed =
          (SELECT dt.Dim_Dateid
             FROM dim_date dt, dim_company cc
            WHERE     dt.DateValue = current_date
                  AND cc.Dim_Companyid = mrp.Dim_Companyid
                  AND cc.CompanyCode = dt.CompanyCode)
 WHERE     Dim_DateidActionClosed = 1
       AND mrp.Dim_Partid = dp.Dim_Partid
       AND me.Dim_MRPElementID = mrp.Dim_MRPElementid
       AND mex.dim_mrpexceptionID = mrp.dim_mrpexceptionID1
       AND mrp.Dim_DateidDateNeeded = dn.Dim_Dateid
       AND mrp.dd_documentno = 'Not Set'
       AND 1 <>  ifnull( (SELECT 1
                     FROM mdtb t, mdkp m
                    WHERE dp.PartNumber = m.MDKP_MATNR
                          AND dp.Plant = m.MDKP_PLWRK
                          and t.MDTB_DELNR is null
                          AND mex.exceptionkey = ifnull(t.MDTB_AUSSL, 'Not Set')
                          AND m.MDKP_DTNUM = t.MDTB_DTNUM
                          AND me.MRPElement = ifnull(t.MDTB_DELKZ, 'Not Set')
                          AND mrp.ct_QtyMRP = t.MDTB_MNG01
                          AND mrp.ct_QtyShortage = t.MDTB_MNG03 
                          AND mrp.ct_QtyExcess = t.MDTB_RDMNG 
                          AND (t.MDTB_MNG03 > 0 OR t.MDTB_RDMNG > 0)
                          AND dn.DateValue =
                                 ifnull(ifnull(t.MDTB_UMDAT, t.MDTB_DAT00), ANSIDATE('0001-01-01'))
                          AND mrp.Dim_ActionStateid = 2),0)
AND Dim_DateidActionClosed <> (SELECT dt.Dim_Dateid
             FROM dim_date dt, dim_company cc
            WHERE     dt.DateValue = current_date
                  AND cc.Dim_Companyid = mrp.Dim_Companyid
                  AND cc.CompanyCode = dt.CompanyCode) ;


DELETE FROM fact_excessandshortage
 WHERE EXISTS
          (SELECT 1
             FROM dim_date d
            WHERE d.Dim_Dateid = Dim_DateidMRP
                  AND d.DateValue < (current_date -  ( INTERVAL '3' MONTH)))
       AND ct_Completed = 1;

Drop table if exists max_holder_41;

Create table max_holder_41(maxid)
as
Select ifnull(max(fact_excessandshortageid),0)
from fact_excessandshortage;

drop table if exists fact_excessandshortage_sub41;

Create table fact_excessandshortage_sub41 as
Select first 0 mrp.Dim_Partid,
     mrp.dim_mrpexceptionID1,
     mrp.Dim_MRPElementid,
     mrp.ct_QtyMRP,
     mrp.ct_QtyShortage,
     mrp.ct_QtyExcess ,
     mrp.dd_documentno,
     mrp.dd_documentitemno, 
     mrp.dd_scheduleno,
     mrp.Dim_DateidDateNeeded 
From fact_excessandshortage mrp
Where  mrp.dd_documentno <> 'Not Set'
AND mrp.Dim_ActionStateid = 2;

Drop table if exists fact_excessandshortage_tmp_41;

Create table fact_excessandshortage_tmp_41 as 
Select first 0 *,
varchar(null,5) MDTB_OLDSL_upd,
ANSIDATE(null) MDTB_DAT01_upd,
varchar(null,20) CompanyCode_upd,
varchar(null,5) MDTB_LGORT_upd,
varchar(null,20) plantcode_upd,
varchar(null,3) MDKP_EKGRP_upd,
char(null,1) mdkp_kzaus_upd,
char(null,1) MDTB_SOBES_upd
 from fact_excessandshortage where 1=2;

Drop table if exists mdtb_pi00;

Create table mdtb_pi00 as Select *,ifnull(MDTB_UMDAT, MDTB_DAT00) datevalue_upd
from mdtb Where (MDTB_MNG03 > 0 OR MDTB_RDMNG > 0) and MDTB_DELNR is not null
AND ifnull(MDTB_UMDAT, MDTB_DAT00) is not null
AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI');

INSERT INTO fact_excessandshortage_tmp_41( fact_excessandshortageid,
		     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
			MDTB_OLDSL_upd,
			MDTB_DAT01_upd,
			CompanyCode_upd,
			MDTB_LGORT_upd,
			plantcode_upd,
			MDKP_EKGRP_upd,
			mdkp_kzaus_upd,
			MDTB_SOBES_upd )
SELECT max_holder_41.maxid + row_number() over(),
		  ifnull((select Dim_MRPElementid 
				  from dim_mrpelement me
                  where     t.MDTB_DELKZ = me.MRPElement
                        AND me.RowIsCurrent = 1),1) Dim_MRPElementid,
          ifnull((select dim_mrpexceptionID 
				  from dim_mrpexception mex
				  where     ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
						AND mex.RowIsCurrent = 1),1) dim_mrpexceptionID1,
	      1 dim_mrpexceptionID2,
          Dim_Companyid,
          ifnull((select Dim_Currencyid
				  from dim_currency c
				  where c.currencycode = dc.currency),1) Dim_Currencyid,
          ifnull((select ar.dim_dateid
				  from dim_date ar
				  where     ar.DateValue = current_date 
						AND ar.CompanyCode = dc.CompanyCode),1) Dim_DateidActionRequired,
          1 Dim_DateidActionClosed,
          ifnull((select dim_dateid
				  from dim_date dn
				  where     dn.DateValue = t.datevalue_upd
						AND dn.CompanyCode = dc.CompanyCode),1) Dim_DateidDateNeeded,
          1 Dim_DateidOriginalDock,
          ifnull((select Dim_Dateid
				  from dim_date mrpd
				  where     mrpd.DateValue = m.MDKP_dsdat
						AND mrpd.CompanyCode = dc.CompanyCode),1) Dim_DateidMRP,
          ifnull((select Dim_Partid
				  from dim_part dp
				  where     m.MDKP_PLWRK = dp.Plant
				        AND m.MDKP_MATNR = dp.PartNumber
                        AND dp.RowIsCurrent = 1),1) Dim_Partid,
          1 Dim_StorageLocationid,
          pl.Dim_Plantid,
          ifnull((select Dim_UnitOfMeasureid 
			      from dim_unitofmeasure uom
				  where    uom.UOM = m.MDKP_MEINS 
					   AND uom.RowIsCurrent = 1),1) Dim_UnitOfMeasureid,
          1 Dim_PurchaseGroupid,
          ifnull((select Dim_PurchaseOrgid
			      from Dim_PurchaseOrg po
				  where     pl.PurchOrg = po.PurchaseOrgCode 
					    AND po.RowIsCurrent = 1),1) Dim_PurchaseOrgid,
          1 Dim_Vendorid,
          2 Dim_ActionStateid,
          1 Dim_ItemCategoryid,
          1 Dim_DocumentTypeid,
          ifnull((select Dim_MRPProcedureid
				  from dim_mrpprocedure pr
				  where     m.MDKP_DISVF = pr.MRPProcedure 
					    AND pr.RowIsCurrent = 1),1) Dim_MRPProcedureid,
          1 Dim_MRPDiscontinuationIndicatorid,
          1 Dim_specialprocurementid,
          1 Dim_FixedVendorid,
          1 Dim_ConsumptionTypeid,
          t.MDTB_MNG01 ct_QtyMRP,
          t.MDTB_MNG03 ct_QtyShortage,
          t.MDTB_RDMNG ct_QtyExcess,
          m.MDKP_BERW2 ct_DaysReceiptCoverage,
          m.MDKP_BERW1 ct_DaysStockCoverage,
          ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
          t.mdtb_delps dd_DocumentItemNo,
          t.mdtb_delet dd_ScheduleNo,
          t.mdtb_dtnum dd_mrptablenumber,
          t.mdtb_baugr dd_peggedrequirement,
          t.mdtb_sernr dd_bomexplosionno,
	 t.MDTB_OLDSL,
	 t.MDTB_DAT01,
	 dc.CompanyCode,
	 t.MDTB_LGORT,
	 pl.plantcode,
	 m.MDKP_EKGRP,
	 m.mdkp_kzaus,
	 t.MDTB_SOBES
     FROM  max_holder_41, mdtb_pi00 t        
          INNER JOIN mdkp m
             ON t.MDTB_DTNUM = m.MDKP_DTNUM
          INNER JOIN dim_plant pl
             ON m.MDKP_PLWRK = pl.PlantCode AND pl.RowIsCurrent = 1
          INNER JOIN dim_company dc
             ON pl.CompanyCode = dc.CompanyCode AND dc.RowIsCurrent = 1;

drop table if exists mdtb_pi00;


Update fact_excessandshortage_tmp_41 sb
Set dim_mrpexceptionID2 = ifnull(
             (SELECT dim_mrpexceptionid
                FROM dim_mrpexception ex
               WHERE sb.MDTB_OLDSL_upd = ex.exceptionkey AND ex.RowIsCurrent = 1),1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_DateidOriginalDock = ifnull(
             (SELECT od.dim_dateid
                FROM dim_date od
               WHERE od.DateValue = sb.MDTB_DAT01_upd
                     AND od.CompanyCode = sb.CompanyCode_upd),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_StorageLocationid = ifnull(
             (SELECT sl.Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE sl.LocationCode = sb.MDTB_LGORT_upd
                     AND sl.Plant = sb.plantcode_upd),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_PurchaseGroupid =  ifnull(
             (SELECT Dim_PurchaseGroupid
                FROM dim_purchasegroup pg
               WHERE pg.PurchaseGroup = sb.MDKP_EKGRP_upd AND pg.RowIsCurrent = 1),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_MRPDiscontinuationIndicatorid =  ifnull(
             (SELECT Dim_MRPDiscontinuationIndicatorid
                FROM dim_mrpdiscontinuationindicator di
               WHERE sb.mdkp_kzaus_upd = di.Indicator AND di.RowIsCurrent = 1),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_specialprocurementid = ifnull(
             (SELECT Dim_specialprocurementid
                FROM dim_specialprocurement sp
               WHERE sb.MDTB_SOBES_upd = sp.specialprocurement
                     AND sp.RowIsCurrent = 1),
             1);


Drop table if exists eands_pe01;
Create table eands_pe01 As Select * from fact_excessandshortage_tmp_41 where 1=2;

INSERT into eands_pe01(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
                        MDTB_OLDSL_upd,
                        MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
			MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd)
Select ne.fact_excessandshortageid,
                     ne.Dim_MRPElementid,
                     ne.dim_mrpexceptionID1,
                     ne.dim_mrpexceptionID2,
                     ne.Dim_Companyid,
                     ne.Dim_Currencyid,
                     ne.Dim_DateidActionRequired,
                     ne.Dim_DateidActionClosed,
                     ne.Dim_DateidDateNeeded,
                     ne.Dim_DateidOriginalDock,
                     ne.Dim_DateidMRP,
                     ne.Dim_Partid,
                     ne.Dim_StorageLocationid,
                     ne.Dim_Plantid,
                     ne.Dim_UnitOfMeasureid,
                     ne.Dim_PurchaseGroupid,
                     ne.Dim_PurchaseOrgid,
                     ne.Dim_Vendorid,
                     ne.Dim_ActionStateid,
                     ne.Dim_ItemCategoryid,
                     ne.Dim_DocumentTypeid,
                     ne.Dim_MRPProcedureid,
                     ne.Dim_MRPDiscontinuationIndicatorid,
                     ne.Dim_specialprocurementid,
                     ne.Dim_FixedVendorid,
                     ne.Dim_ConsumptionTypeid,
                     ne.ct_QtyMRP,
                     ne.ct_QtyShortage,
                     ne.ct_QtyExcess,
                     ne.ct_DaysReceiptCoverage,
                     ne.ct_DaysStockCoverage,
                     ne.dd_DocumentNo,
                     ne.dd_DocumentItemNo,
                     ne.dd_ScheduleNo,
                     ne.dd_mrptablenumber,
                     ne.dd_peggedrequirement,
                     ne.dd_bomexplosionno,
                        ne.MDTB_OLDSL_upd,
                        ne.MDTB_DAT01_upd,
                        ne.CompanyCode_upd,
                        ne.MDTB_LGORT_upd,
                        ne.plantcode_upd,
			ne.MDKP_EKGRP_upd,
                        ne.mdkp_kzaus_upd,
                        ne.MDTB_SOBES_upd
From fact_excessandshortage_tmp_41 ne,fact_excessandshortage_sub41 mrp
WHERE mrp.Dim_Partid = ne.Dim_Partid
AND mrp.dim_mrpexceptionID1 = ne.dim_mrpexceptionID1
AND mrp.Dim_MRPElementid = ne.Dim_MRPElementid
AND mrp.ct_QtyMRP = ne.ct_QtyMRP
AND mrp.ct_QtyShortage = ne.ct_QtyShortage
AND mrp.ct_QtyExcess = ne.ct_QtyExcess
AND mrp.dd_documentno = ne.dd_documentno
AND mrp.dd_documentitemno = ne.dd_documentitemno
AND mrp.dd_scheduleno = ne.dd_scheduleno 
AND mrp.Dim_DateidDateNeeded = ne.Dim_DateidDateNeeded;

call vectorwise(combine 'fact_excessandshortage_tmp_41');
call vectorwise(combine 'eands_pe01');

call vectorwise(combine 'fact_excessandshortage_tmp_41 - eands_pe01');


INSERT INTO fact_excessandshortage( fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno)
Select max_holder_41.maxid + row_number() over(),
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
from fact_excessandshortage_tmp_41,max_holder_41;
                     



Drop table if exists eands_pe01;
Drop table if exists fact_excessandshortage_sub41;
Drop table if exists fact_excessandshortage_tmp_41;
Drop table if exists max_holder_41;

Create table max_holder_41(maxid)
as
Select ifnull(max(fact_excessandshortageid),0)
from fact_excessandshortage;

Create table fact_excessandshortage_sub41 as
Select first 0 mrp.Dim_Partid ,
	     mrp.dim_mrpexceptionID1,
	     mrp.Dim_MRPElementid,
	     mrp.ct_QtyMRP ,
	     mrp.ct_QtyShortage,
	     mrp.ct_QtyExcess,
	     mrp.Dim_DateidDateNeeded 
FROM fact_excessandshortage mrp
Where mrp.dd_documentno = 'Not Set'
AND  mrp.Dim_ActionStateid = 2;

Create table fact_excessandshortage_tmp_41 as
Select first 0 *,
varchar(null,5) MDTB_OLDSL_upd,
ANSIDATE(null) MDTB_DAT01_upd,
varchar(null,20) CompanyCode_upd,
varchar(null,5) MDTB_LGORT_upd,
varchar(null,20) plantcode_upd,
varchar(null,3) MDKP_EKGRP_upd,
char(null,1) mdkp_kzaus_upd,
char(null,1) MDTB_SOBES_upd
 from fact_excessandshortage where 1=2;

drop table if exists mdtb_pi00;
Create table mdtb_pi00 as Select *,ifnull(MDTB_UMDAT, MDTB_DAT00) datevalue_upd from mdtb where (MDTB_MNG03 > 0 OR MDTB_RDMNG > 0) and MDTB_DELNR is null
AND ifnull(MDTB_UMDAT, MDTB_DAT00) is not null
AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI');

INSERT INTO fact_excessandshortage_tmp_41(fact_excessandshortageid,
		     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
			  MDTB_OLDSL_upd,
                        MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
                        MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd )
   SELECT max_holder_41.maxid + row_number() over(),
	  Dim_MRPElementid,
          dim_mrpexceptionID dim_mrpexceptionID1,
             1 dim_mrpexceptionID2,
          Dim_Companyid,
          c.Dim_Currencyid,
          ar.dim_dateid Dim_DateidActionRequired,
          1 Dim_DateidActionClosed,
          dn.dim_dateid Dim_DateidDateNeeded,
             1 Dim_DateidOriginalDock,
          mrpd.Dim_Dateid Dim_DateidMRP,
          Dim_Partid,
             1 Dim_StorageLocationid,
          Dim_Plantid,
          Dim_UnitOfMeasureid,
             1 Dim_PurchaseGroupid,
          Dim_PurchaseOrgid,
          1 Dim_Vendorid,
          2 Dim_ActionStateid,
          1 Dim_ItemCategoryid,
          1 Dim_DocumentTypeid,
          Dim_MRPProcedureid,
             1 Dim_MRPDiscontinuationIndicatorid,
             1 Dim_specialprocurementid,
          1 Dim_FixedVendorid,
          1 Dim_ConsumptionTypeid,
          t.MDTB_MNG01 ct_QtyMRP,
          t.MDTB_MNG03 ct_QtyShortage,
          t.MDTB_RDMNG ct_QtyExcess,
          m.MDKP_BERW2 ct_DaysReceiptCoverage,
          m.MDKP_BERW1 ct_DaysStockCoverage,
          ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
          t.mdtb_delps dd_DocumentItemNo,
          t.mdtb_delet dd_ScheduleNo,
          t.mdtb_dtnum dd_mrptablenumber,
          t.mdtb_baugr dd_peggedrequirement,
          t.mdtb_sernr dd_bomexplosionno,
	      t.MDTB_OLDSL,
         t.MDTB_DAT01,
         dc.CompanyCode,
         t.MDTB_LGORT,
         pl.plantcode,
         m.MDKP_EKGRP,
         m.mdkp_kzaus,
         t.MDTB_SOBES
     FROM max_holder_41,mdtb_pi00 t
          INNER JOIN dim_mrpelement me
             ON     t.MDTB_DELKZ = me.MRPElement
                AND me.RowIsCurrent = 1
          INNER JOIN dim_mrpexception mex
             ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
                AND mex.RowIsCurrent = 1
          INNER JOIN mdkp m
             ON t.MDTB_DTNUM = m.MDKP_DTNUM
          INNER JOIN dim_part dp
             ON     m.MDKP_PLWRK = dp.Plant
                AND m.MDKP_MATNR = dp.PartNumber
                AND dp.RowIsCurrent = 1
          INNER JOIN dim_plant pl
             ON m.MDKP_PLWRK = pl.PlantCode AND pl.RowIsCurrent = 1
          INNER JOIN dim_unitofmeasure uom
             ON uom.UOM = m.MDKP_MEINS AND uom.RowIsCurrent = 1
          INNER JOIN Dim_PurchaseOrg po
             ON pl.PurchOrg = po.PurchaseOrgCode AND po.RowIsCurrent = 1
          INNER JOIN dim_company dc
             ON pl.CompanyCode = dc.CompanyCode AND dc.RowIsCurrent = 1
          INNER JOIN dim_currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date mrpd
             ON mrpd.DateValue = m.MDKP_dsdat
                AND mrpd.CompanyCode = dc.CompanyCode
          INNER JOIN dim_date dn
             ON     dn.DateValue = datevalue_upd
                AND dn.CompanyCode = dc.CompanyCode
          INNER JOIN dim_date ar
             ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode
          INNER JOIN dim_mrpprocedure pr
             ON m.MDKP_DISVF = pr.MRPProcedure AND pr.RowIsCurrent = 1;


drop table if exists mdtb_pi00;

Update fact_excessandshortage_tmp_41 sb
Set dim_mrpexceptionID2 = ifnull(
             (SELECT dim_mrpexceptionid
                FROM dim_mrpexception ex
               WHERE sb.MDTB_OLDSL_upd = ex.exceptionkey AND ex.RowIsCurrent = 1),1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_DateidOriginalDock = ifnull(
             (SELECT od.dim_dateid
                FROM dim_date od
               WHERE od.DateValue = sb.MDTB_DAT01_upd
                     AND od.CompanyCode = sb.CompanyCode_upd),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_StorageLocationid = ifnull(
             (SELECT sl.Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE sl.LocationCode = sb.MDTB_LGORT_upd
                     AND sl.Plant = sb.plantcode_upd),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_PurchaseGroupid =  ifnull(
             (SELECT Dim_PurchaseGroupid
                FROM dim_purchasegroup pg
               WHERE pg.PurchaseGroup = sb.MDKP_EKGRP_upd AND pg.RowIsCurrent = 1),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_MRPDiscontinuationIndicatorid =  ifnull(
             (SELECT Dim_MRPDiscontinuationIndicatorid
                FROM dim_mrpdiscontinuationindicator di
               WHERE sb.mdkp_kzaus_upd = di.Indicator AND di.RowIsCurrent = 1),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_specialprocurementid = ifnull(
             (SELECT Dim_specialprocurementid
                FROM dim_specialprocurement sp
               WHERE sb.MDTB_SOBES_upd = sp.specialprocurement
                     AND sp.RowIsCurrent = 1),
             1);

Create table eands_pe01 As Select * from fact_excessandshortage_tmp_41 where 1=2;

Insert into eands_pe01(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
                          MDTB_OLDSL_upd,
			 MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
                        MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd)
Select ne.fact_excessandshortageid,
                     ne.Dim_MRPElementid,
                     ne.dim_mrpexceptionID1,
                     ne.dim_mrpexceptionID2,
                     ne.Dim_Companyid,
                     ne.Dim_Currencyid,
                     ne.Dim_DateidActionRequired,
                     ne.Dim_DateidActionClosed,
                     ne.Dim_DateidDateNeeded,
                     ne.Dim_DateidOriginalDock,
                     ne.Dim_DateidMRP,
                     ne.Dim_Partid,
                     ne.Dim_StorageLocationid,
                     ne.Dim_Plantid,
                     ne.Dim_UnitOfMeasureid,
                     ne.Dim_PurchaseGroupid,
                     ne.Dim_PurchaseOrgid,
                     ne.Dim_Vendorid,
                     ne.Dim_ActionStateid,
                     ne.Dim_ItemCategoryid,
                     ne.Dim_DocumentTypeid,
                     ne.Dim_MRPProcedureid,
                     ne.Dim_MRPDiscontinuationIndicatorid,
                     ne.Dim_specialprocurementid,
                     ne.Dim_FixedVendorid,
                     ne.Dim_ConsumptionTypeid,
                     ne.ct_QtyMRP,
                     ne.ct_QtyShortage,
                     ne.ct_QtyExcess,
                     ne.ct_DaysReceiptCoverage,
                     ne.ct_DaysStockCoverage,
                     ne.dd_DocumentNo,
                     ne.dd_DocumentItemNo,
                     ne.dd_ScheduleNo,
                     ne.dd_mrptablenumber,
                     ne.dd_peggedrequirement,
                     ne.dd_bomexplosionno,
                          ne.MDTB_OLDSL_upd,
			 ne.MDTB_DAT01_upd,
                        ne.CompanyCode_upd,
                        ne.MDTB_LGORT_upd,
                        ne.plantcode_upd,
                        ne.MDKP_EKGRP_upd,
                        ne.mdkp_kzaus_upd,
                        ne.MDTB_SOBES_upd
From fact_excessandshortage_tmp_41 ne,fact_excessandshortage_sub41 mrp
Where mrp.Dim_Partid = ne.Dim_Partid
     AND mrp.dim_mrpexceptionID1 = ne.dim_mrpexceptionID1
     AND mrp.Dim_MRPElementid = ne.Dim_MRPElementid
     AND mrp.ct_QtyMRP = ne.ct_QtyMRP
     AND mrp.ct_QtyShortage = ne.ct_QtyShortage
     AND mrp.ct_QtyExcess = ne.ct_QtyExcess
     AND mrp.Dim_DateidDateNeeded = ne.Dim_DateidDateNeeded ;


call vectorwise(combine 'fact_excessandshortage_tmp_41');
call vectorwise(combine 'eands_pe01');

call vectorwise(combine 'fact_excessandshortage_tmp_41 - eands_pe01');





INSERT INTO fact_excessandshortage(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
)
Select max_holder_41.maxid + row_number() over(),
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
From fact_excessandshortage_tmp_41,max_holder_41;

Drop table if exists eands_pe01;
Drop table if exists fact_excessandshortage_tmp_41;
Drop table if exists fact_excessandshortage_sub41;
Drop table if exists max_holder_41;

Create table max_holder_41(maxid)
as
Select ifnull(max(fact_excessandshortageid),0)
from fact_excessandshortage;

Create table fact_excessandshortage_sub41 as
Select first 0 mrp.Dim_Partid ,
	     mrp.dim_mrpexceptionID1,
	     mrp.Dim_MRPElementid,
	     mrp.ct_QtyMRP,
	     mrp.ct_QtyShortage ,
	     mrp.ct_QtyExcess,
	     mrp.dd_documentno, 
	     mrp.dd_documentitemno, 
	     mrp.dd_scheduleno
From fact_excessandshortage mrp
Where  mrp.dd_documentno <> 'Not Set'
AND  mrp.Dim_DateidDateNeeded = 1
AND  mrp.Dim_ActionStateid = 2;


Create table fact_excessandshortage_tmp_41 as
Select first 0 *,
varchar(null,5) MDTB_OLDSL_upd,
ANSIDATE(null) MDTB_DAT01_upd,
varchar(null,20) CompanyCode_upd,
varchar(null,5) MDTB_LGORT_upd,
varchar(null,20) plantcode_upd,
varchar(null,3) MDKP_EKGRP_upd,
char(null,1) mdkp_kzaus_upd,
char(null,1) MDTB_SOBES_upd
 from fact_excessandshortage where 1=2;

Drop table if exists mdtb_pi00;
Create table mdtb_pi00 as Select * from mdtb   
WHERE     MDTB_UMDAT IS NULL 
AND MDTB_DAT00 IS NULL 
AND (MDTB_MNG03 > 0 OR MDTB_RDMNG > 0) 
AND MDTB_DELNR is not null AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI');

INSERT INTO fact_excessandshortage_tmp_41(fact_excessandshortageid,
		     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
			 MDTB_OLDSL_upd,
                        MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
                        MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd )
   SELECT  max_holder_41.maxid + row_number() over(),
	  Dim_MRPElementid,
          dim_mrpexceptionID dim_mrpexceptionID1,
             1 dim_mrpexceptionID2,
          Dim_Companyid,
          c.Dim_Currencyid,
          ar.dim_dateid Dim_DateidActionRequired,
          1 Dim_DateidActionClosed,
          1 Dim_DateidDateNeeded,
             1 Dim_DateidOriginalDock,
          mrpd.Dim_Dateid Dim_DateidMRP,
          Dim_Partid,
             1 Dim_StorageLocationid,
          Dim_Plantid,
          Dim_UnitOfMeasureid,
             1 Dim_PurchaseGroupid,
          Dim_PurchaseOrgid,
          1 Dim_Vendorid,
          2 Dim_ActionStateid,
          1 Dim_ItemCategoryid,
          1 Dim_DocumentTypeid,
          Dim_MRPProcedureid,
             1 Dim_MRPDiscontinuationIndicatorid,
             1 Dim_specialprocurementid,
          1 Dim_FixedVendorid,
          1 Dim_ConsumptionTypeid,
          t.MDTB_MNG01 ct_QtyMRP,
          t.MDTB_MNG03 ct_QtyShortage,
          t.MDTB_RDMNG ct_QtyExcess,
          m.MDKP_BERW2 ct_DaysReceiptCoverage,
          m.MDKP_BERW1 ct_DaysStockCoverage,
          ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
          t.mdtb_delps dd_DocumentItemNo,
          t.mdtb_delet dd_ScheduleNo,
          t.mdtb_dtnum dd_mrptablenumber,
          t.mdtb_baugr dd_peggedrequirement,
          t.mdtb_sernr dd_bomexplosionno,
	  t.MDTB_OLDSL,
         t.MDTB_DAT01,
         dc.CompanyCode,
         t.MDTB_LGORT,
         pl.plantcode,
         m.MDKP_EKGRP,
         m.mdkp_kzaus,
         t.MDTB_SOBES
     FROM max_holder_41,mdtb_pi00 t
          INNER JOIN dim_mrpelement me
             ON     t.MDTB_DELKZ = me.MRPElement
                AND me.RowIsCurrent = 1
          INNER JOIN dim_mrpexception mex
             ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
                AND mex.RowIsCurrent = 1
          INNER JOIN mdkp m
             ON t.MDTB_DTNUM = m.MDKP_DTNUM
          INNER JOIN dim_part dp
             ON     m.MDKP_PLWRK = dp.Plant
                AND m.MDKP_MATNR = dp.PartNumber
                AND dp.RowIsCurrent = 1
          INNER JOIN dim_unitofmeasure uom
             ON uom.UOM = m.MDKP_MEINS AND uom.RowIsCurrent = 1
          INNER JOIN dim_plant pl
             ON m.MDKP_PLWRK = pl.PlantCode AND pl.RowIsCurrent = 1
          INNER JOIN Dim_PurchaseOrg po
             ON pl.PurchOrg = po.PurchaseOrgCode AND po.RowIsCurrent = 1
          INNER JOIN dim_company dc
             ON pl.CompanyCode = dc.CompanyCode AND dc.RowIsCurrent = 1
          INNER JOIN Dim_Currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date mrpd
             ON mrpd.DateValue = m.MDKP_dsdat
                AND mrpd.CompanyCode = dc.CompanyCode
          INNER JOIN dim_date ar
             ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode
          INNER JOIN dim_mrpprocedure pr
             ON m.MDKP_DISVF = pr.MRPProcedure AND pr.RowIsCurrent = 1;

drop table if exists mdtb_pi00;


Update fact_excessandshortage_tmp_41 sb
Set dim_mrpexceptionID2 =   (SELECT dim_mrpexceptionid
             FROM dim_mrpexception ex
            WHERE ifnull(sb.MDTB_OLDSL_upd, 'Not Set') = ex.exceptionkey
                  AND ex.RowIsCurrent = 1);


Update fact_excessandshortage_tmp_41 sb
Set Dim_DateidOriginalDock = ifnull(
             (SELECT od.dim_dateid
                FROM dim_date od
               WHERE od.DateValue = sb.MDTB_DAT01_upd
                     AND od.CompanyCode = sb.CompanyCode_upd),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_StorageLocationid = ifnull(
             (SELECT sl.Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE sl.LocationCode = sb.MDTB_LGORT_upd
                     AND sl.Plant = sb.plantcode_upd),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_PurchaseGroupid =  ifnull(
             (SELECT Dim_PurchaseGroupid
                FROM dim_purchasegroup pg
               WHERE pg.PurchaseGroup = sb.MDKP_EKGRP_upd AND pg.RowIsCurrent = 1),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_MRPDiscontinuationIndicatorid =  ifnull(
             (SELECT Dim_MRPDiscontinuationIndicatorid
                FROM dim_mrpdiscontinuationindicator di
               WHERE sb.mdkp_kzaus_upd = di.Indicator AND di.RowIsCurrent = 1),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_specialprocurementid = ifnull(
             (SELECT Dim_specialprocurementid
                FROM dim_specialprocurement sp
               WHERE sb.MDTB_SOBES_upd = sp.specialprocurement
                     AND sp.RowIsCurrent = 1),
             1);

Create table eands_pe01 As Select * from fact_excessandshortage_tmp_41 where 1=2;

Insert into eands_pe01(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
		     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
                         MDTB_OLDSL_upd,
                        MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
                        MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd )
Select ne.fact_excessandshortageid,
                     ne.Dim_MRPElementid,
                     ne.dim_mrpexceptionID1,
                     ne.dim_mrpexceptionID2,
                     ne.Dim_Companyid,
                     ne.Dim_Currencyid,
                     ne.Dim_DateidActionRequired,
                     ne.Dim_DateidActionClosed,
                     ne.Dim_DateidDateNeeded,
                     ne.Dim_DateidOriginalDock,
                     ne.Dim_DateidMRP,
                     ne.Dim_Partid,
                     ne.Dim_StorageLocationid,
                     ne.Dim_Plantid,
                     ne.Dim_UnitOfMeasureid,
                     ne.Dim_PurchaseGroupid,
                     ne.Dim_PurchaseOrgid,
                     ne.Dim_Vendorid,
                     ne.Dim_ActionStateid,
                     ne.Dim_ItemCategoryid,
                     ne.Dim_DocumentTypeid,
                     ne.Dim_MRPProcedureid,
                     ne.Dim_MRPDiscontinuationIndicatorid,
                     ne.Dim_specialprocurementid,
                     ne.Dim_FixedVendorid,
                     ne.Dim_ConsumptionTypeid,
                     ne.ct_QtyMRP,
                     ne.ct_QtyShortage,
		     ne.ct_QtyExcess,
                     ne.ct_DaysReceiptCoverage,
                     ne.ct_DaysStockCoverage,
                     ne.dd_DocumentNo,
                     ne.dd_DocumentItemNo,
                     ne.dd_ScheduleNo,
                     ne.dd_mrptablenumber,
                     ne.dd_peggedrequirement,
                     ne.dd_bomexplosionno,
                         ne.MDTB_OLDSL_upd,
                        ne.MDTB_DAT01_upd,
                        ne.CompanyCode_upd,
                        ne.MDTB_LGORT_upd,
                        ne.plantcode_upd,
                        ne.MDKP_EKGRP_upd,
                        ne.mdkp_kzaus_upd,
                        ne.MDTB_SOBES_upd 
From fact_excessandshortage_tmp_41 ne,fact_excessandshortage_sub41 mrp
Where mrp.Dim_Partid = ne.Dim_Partid 
     AND mrp.dim_mrpexceptionID1 = ne.dim_mrpexceptionID1
     AND mrp.Dim_MRPElementid = ne.Dim_MRPElementid
     AND mrp.ct_QtyMRP = ne.ct_QtyMRP
     AND mrp.ct_QtyShortage = ne.ct_QtyShortage 
     AND mrp.ct_QtyExcess = ne.ct_QtyExcess
     AND mrp.dd_documentno = ne.dd_documentno
     AND mrp.dd_documentitemno = ne.dd_documentitemno 
     AND mrp.dd_scheduleno = ne.dd_scheduleno;

call vectorwise(combine 'fact_excessandshortage_tmp_41');
call vectorwise(combine 'eands_pe01');

call vectorwise(combine 'fact_excessandshortage_tmp_41 - eands_pe01');


INSERT INTO fact_excessandshortage(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno)
Select max_holder_41.maxid + row_number() over(),
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
From fact_excessandshortage_tmp_41,max_holder_41; 


drop table if exists eands_pe01;
Drop table if exists max_holder_41;
Drop table if exists fact_excessandshortage_sub41;
Drop table if exists fact_excessandshortage_tmp_41;

Create table max_holder_41(maxid)
as
Select ifnull(max(fact_excessandshortageid),0)
from fact_excessandshortage;

Create table fact_excessandshortage_sub41 as
Select first 0 mrp.Dim_Partid,
	     mrp.dim_mrpexceptionID1,
	     mrp.Dim_MRPElementid,
	     mrp.ct_QtyMRP ,
	     mrp.ct_QtyShortage, 
	     mrp.ct_QtyExcess
From fact_excessandshortage mrp
Where mrp.dd_documentno = 'Not Set'
AND mrp.Dim_DateidDateNeeded = 1
 AND mrp.Dim_ActionStateid = 2;

Create table fact_excessandshortage_tmp_41 as
Select first 0 *,
varchar(null,5) MDTB_OLDSL_upd,
ANSIDATE(null) MDTB_DAT01_upd,
varchar(null,20) CompanyCode_upd,
varchar(null,5) MDTB_LGORT_upd,
varchar(null,20) plantcode_upd,
varchar(null,3) MDKP_EKGRP_upd,
char(null,1) mdkp_kzaus_upd,
char(null,1) MDTB_SOBES_upd
 from fact_excessandshortage where 1=2;

drop table if exists mdtb_pi00;
Create table mdtb_pi00 as Select * from mdtb 
WHERE     MDTB_UMDAT IS NULL
AND MDTB_DAT00 IS NULL
AND (MDTB_MNG03 > 0 OR MDTB_RDMNG > 0)
AND MDTB_DELNR is null AND MDTB_DELKZ NOT IN ('SM', 'SB', 'SA', 'SI');

INSERT INTO fact_excessandshortage_tmp_41(fact_excessandshortageid,
		     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno,
			  MDTB_OLDSL_upd,
                        MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
                        MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd )
   SELECT  max_holder_41.maxid + row_number() over(),
	  Dim_MRPElementid,
          dim_mrpexceptionID dim_mrpexceptionID1,
             1 dim_mrpexceptionID2,
          Dim_Companyid,
          c.Dim_Currencyid,
          ar.dim_dateid Dim_DateidActionRequired,
          1 Dim_DateidActionClosed,
          1 Dim_DateidDateNeeded,
             1 Dim_DateidOriginalDock,
          mrpd.Dim_Dateid Dim_DateidMRP,
          Dim_Partid,
             1 Dim_StorageLocationid,
          Dim_Plantid,
          Dim_UnitOfMeasureid,
             1 Dim_PurchaseGroupid,
          Dim_PurchaseOrgid,
          1 Dim_Vendorid,
          2 Dim_ActionStateid,
          1 Dim_ItemCategoryid,
          1 Dim_DocumentTypeid,
          Dim_MRPProcedureid,
             1 Dim_MRPDiscontinuationIndicatorid,
             1 Dim_specialprocurementid,
          1 Dim_FixedVendorid,
          1 Dim_ConsumptionTypeid,
          t.MDTB_MNG01 ct_QtyMRP,
          t.MDTB_MNG03 ct_QtyShortage,
          t.MDTB_RDMNG ct_QtyExcess,
          m.MDKP_BERW2 ct_DaysReceiptCoverage,
          m.MDKP_BERW1 ct_DaysStockCoverage,
          ifnull(t.mdtb_delnr,'Not Set') dd_DocumentNo,
          t.mdtb_delps dd_DocumentItemNo,
          t.mdtb_delet dd_ScheduleNo,
          t.mdtb_dtnum dd_mrptablenumber,
          t.mdtb_baugr dd_peggedrequirement,
          t.mdtb_sernr dd_bomexplosionno,
		 t.MDTB_OLDSL,
         t.MDTB_DAT01,
         dc.CompanyCode,
         t.MDTB_LGORT,
         pl.plantcode,
         m.MDKP_EKGRP,
         m.mdkp_kzaus,
         t.MDTB_SOBES
     FROM max_holder_41,mdtb_pi00 t
          INNER JOIN dim_mrpelement me
             ON     t.MDTB_DELKZ = me.MRPElement
                AND me.RowIsCurrent = 1
          INNER JOIN dim_mrpexception mex
             ON ifnull(t.MDTB_AUSSL, 'Not Set') = mex.exceptionkey
                AND mex.RowIsCurrent = 1
          INNER JOIN mdkp m
             ON t.MDTB_DTNUM = m.MDKP_DTNUM
          INNER JOIN dim_part dp
             ON     m.MDKP_PLWRK = dp.Plant
                AND m.MDKP_MATNR = dp.PartNumber
                AND dp.RowIsCurrent = 1
          INNER JOIN dim_unitofmeasure uom
             ON uom.UOM = m.MDKP_MEINS AND uom.RowIsCurrent = 1
          INNER JOIN dim_plant pl
             ON m.MDKP_PLWRK = pl.PlantCode AND pl.RowIsCurrent = 1
          INNER JOIN Dim_PurchaseOrg po
             ON pl.PurchOrg = po.PurchaseOrgCode AND po.RowIsCurrent = 1
          INNER JOIN dim_company dc
             ON pl.CompanyCode = dc.CompanyCode AND dc.RowIsCurrent = 1
          INNER JOIN Dim_Currency c
             ON c.currencycode = dc.currency
          INNER JOIN dim_date mrpd
             ON mrpd.DateValue = m.MDKP_dsdat
                AND mrpd.CompanyCode = dc.CompanyCode
          INNER JOIN dim_date ar
             ON ar.DateValue = current_date AND ar.CompanyCode = dc.CompanyCode
          INNER JOIN dim_mrpprocedure pr
             ON m.MDKP_DISVF = pr.MRPProcedure AND pr.RowIsCurrent = 1;

drop table if exists mdtb_pi00;

Update fact_excessandshortage_tmp_41 sb
Set dim_mrpexceptionID2 =   (SELECT dim_mrpexceptionid
             FROM dim_mrpexception ex
            WHERE ifnull(sb.MDTB_OLDSL_upd, 'Not Set') = ex.exceptionkey
                  AND ex.RowIsCurrent = 1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_DateidOriginalDock = ifnull(
             (SELECT od.dim_dateid
                FROM dim_date od
               WHERE od.DateValue = sb.MDTB_DAT01_upd
                     AND od.CompanyCode = sb.CompanyCode_upd),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_StorageLocationid = ifnull(
             (SELECT sl.Dim_StorageLocationid
                FROM dim_storagelocation sl
               WHERE sl.LocationCode = sb.MDTB_LGORT_upd
                     AND sl.Plant = sb.plantcode_upd),
             1);
Update fact_excessandshortage_tmp_41 sb
Set Dim_PurchaseGroupid =  ifnull(
             (SELECT Dim_PurchaseGroupid
                FROM dim_purchasegroup pg
               WHERE pg.PurchaseGroup = sb.MDKP_EKGRP_upd AND pg.RowIsCurrent = 1),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_MRPDiscontinuationIndicatorid =  ifnull(
             (SELECT Dim_MRPDiscontinuationIndicatorid
                FROM dim_mrpdiscontinuationindicator di
               WHERE sb.mdkp_kzaus_upd = di.Indicator AND di.RowIsCurrent = 1),
             1);

Update fact_excessandshortage_tmp_41 sb
Set Dim_specialprocurementid = ifnull(
             (SELECT Dim_specialprocurementid
                FROM dim_specialprocurement sp
               WHERE sb.MDTB_SOBES_upd = sp.specialprocurement
                     AND sp.RowIsCurrent = 1),
             1);

Create table eands_pe01 As Select * from fact_excessandshortage_tmp_41 where 1=2;

Insert into eands_pe01(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
		    dd_bomexplosionno,
                          MDTB_OLDSL_upd,
                        MDTB_DAT01_upd,
                        CompanyCode_upd,
                        MDTB_LGORT_upd,
                        plantcode_upd,
                        MDKP_EKGRP_upd,
                        mdkp_kzaus_upd,
                        MDTB_SOBES_upd )
Select ne.fact_excessandshortageid,
                     ne.Dim_MRPElementid,
                     ne.dim_mrpexceptionID1,
                     ne.dim_mrpexceptionID2,
                     ne.Dim_Companyid,
                     ne.Dim_Currencyid,
                     ne.Dim_DateidActionRequired,
                     ne.Dim_DateidActionClosed,
                     ne.Dim_DateidDateNeeded,
                     ne.Dim_DateidOriginalDock,
                     ne.Dim_DateidMRP,
                     ne.Dim_Partid,
                     ne.Dim_StorageLocationid,
                     ne.Dim_Plantid,
                     ne.Dim_UnitOfMeasureid,
                     ne.Dim_PurchaseGroupid,
                     ne.Dim_PurchaseOrgid,
                     ne.Dim_Vendorid,
                     ne.Dim_ActionStateid,
                     ne.Dim_ItemCategoryid,
                     ne.Dim_DocumentTypeid,
                     ne.Dim_MRPProcedureid,
                     ne.Dim_MRPDiscontinuationIndicatorid,
                     ne.Dim_specialprocurementid,
                     ne.Dim_FixedVendorid,
                     ne.Dim_ConsumptionTypeid,
                     ne.ct_QtyMRP,
                     ne.ct_QtyShortage,
                     ne.ct_QtyExcess,
                     ne.ct_DaysReceiptCoverage,
                     ne.ct_DaysStockCoverage,
                     ne.dd_DocumentNo,
                     ne.dd_DocumentItemNo,
                     ne.dd_ScheduleNo,
                     ne.dd_mrptablenumber,
                     ne.dd_peggedrequirement,
		    ne.dd_bomexplosionno,
                          ne.MDTB_OLDSL_upd,
                        ne.MDTB_DAT01_upd,
                        ne.CompanyCode_upd,
                        ne.MDTB_LGORT_upd,
                        ne.plantcode_upd,
                        ne.MDKP_EKGRP_upd,
                        ne.mdkp_kzaus_upd,
                        ne.MDTB_SOBES_upd 
From fact_excessandshortage_tmp_41 ne,fact_excessandshortage_sub41 mrp
WHERE mrp.Dim_Partid = ne.Dim_Partid
AND mrp.dim_mrpexceptionID1 =  ne.dim_mrpexceptionID1 
AND mrp.Dim_MRPElementid = ne.Dim_MRPElementid
AND mrp.ct_QtyMRP = ne.ct_QtyMRP
AND mrp.ct_QtyShortage = ne.ct_QtyShortage 
AND mrp.ct_QtyExcess = ne.ct_QtyExcess ;

call vectorwise(combine 'fact_excessandshortage_tmp_41');
call vectorwise(combine 'eands_pe01');

call vectorwise(combine 'fact_excessandshortage_tmp_41 - eands_pe01');



INSERT INTO fact_excessandshortage(fact_excessandshortageid,
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno)
Select max_holder_41.maxid + row_number() over(),
                     Dim_MRPElementid,
                     dim_mrpexceptionID1,
                     dim_mrpexceptionID2,
                     Dim_Companyid,
                     Dim_Currencyid,
                     Dim_DateidActionRequired,
                     Dim_DateidActionClosed,
                     Dim_DateidDateNeeded,
                     Dim_DateidOriginalDock,
                     Dim_DateidMRP,
                     Dim_Partid,
                     Dim_StorageLocationid,
                     Dim_Plantid,
                     Dim_UnitOfMeasureid,
                     Dim_PurchaseGroupid,
                     Dim_PurchaseOrgid,
                     Dim_Vendorid,
                     Dim_ActionStateid,
                     Dim_ItemCategoryid,
                     Dim_DocumentTypeid,
                     Dim_MRPProcedureid,
                     Dim_MRPDiscontinuationIndicatorid,
                     Dim_specialprocurementid,
                     Dim_FixedVendorid,
                     Dim_ConsumptionTypeid,
                     ct_QtyMRP,
                     ct_QtyShortage,
                     ct_QtyExcess,
                     ct_DaysReceiptCoverage,
                     ct_DaysStockCoverage,
                     dd_DocumentNo,
                     dd_DocumentItemNo,
                     dd_ScheduleNo,
                     dd_mrptablenumber,
                     dd_peggedrequirement,
                     dd_bomexplosionno
From fact_excessandshortage_tmp_41,max_holder_41;




drop table if exists eands_pe01;
drop table if exists fact_excessandshortage_tmp_41;
drop table if exists fact_excessandshortage_sub41;
Drop table if exists max_holder_41;

UPDATE fact_excessandshortage m
FROM  plaf po,
       mdkp k,
       mdtb t,
       dim_vendor v,
       dim_vendor fv,
       dim_consumptiontype ct,
       dim_part p,
       dim_plant pl,
       Dim_Date dd
   SET m.Dim_Vendorid = v.Dim_Vendorid
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND dd.Dim_Dateid <> 1
       AND m.dd_DocumentNo = po.PLAF_PLNUM
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND p.Dim_Partid = m.Dim_Partid
       AND pl.PlantCode = p.Plant
       AND v.VendorNumber = ifnull(PLAF_EMLIF, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(PLAF_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(po.PLAF_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND dd.DateValue = ifnull(MDTB_UMDAT, MDTB_DAT01)
       AND ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL
       AND MDTB_DAT02 IS NOT NULL
       AND ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL
       AND dd.CompanyCode = pl.CompanyCode
	AND  m.Dim_Vendorid <> v.Dim_Vendorid;

     
UPDATE fact_excessandshortage m
FROM  plaf po,
       mdkp k,
       mdtb t,
       dim_vendor v,
       dim_vendor fv,
       dim_consumptiontype ct,
       dim_part p,
       dim_plant pl,
       Dim_Date dd
   SET
  m.Dim_FixedVendorid = fv.Dim_Vendorid
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND dd.Dim_Dateid <> 1
       AND m.dd_DocumentNo = po.PLAF_PLNUM
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND p.Dim_Partid = m.Dim_Partid
       AND pl.PlantCode = p.Plant
       AND v.VendorNumber = ifnull(PLAF_EMLIF, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(PLAF_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(po.PLAF_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND dd.DateValue = ifnull(MDTB_UMDAT, MDTB_DAT01)
       AND ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL
       AND MDTB_DAT02 IS NOT NULL
       AND ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL
       AND dd.CompanyCode = pl.CompanyCode
	AND m.Dim_FixedVendorid <> fv.Dim_Vendorid;



      UPDATE fact_excessandshortage m
FROM  plaf po,
       mdkp k,
       mdtb t,
       dim_vendor v,
       dim_vendor fv,
       dim_consumptiontype ct,
       dim_part p,
       dim_plant pl,
       Dim_Date dd
   SET m.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND dd.Dim_Dateid <> 1
       AND m.dd_DocumentNo = po.PLAF_PLNUM
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND p.Dim_Partid = m.Dim_Partid
       AND pl.PlantCode = p.Plant
       AND v.VendorNumber = ifnull(PLAF_EMLIF, 'Not Set')
       AND v.RowIsCurrent = 1
       AND fv.VendorNumber = ifnull(PLAF_FLIEF, 'Not Set')
       AND fv.RowIsCurrent = 1
       AND ct.ConsumptionCode = ifnull(po.PLAF_KZVBR, 'Not Set')
       AND ct.RowIsCurrent = 1
       AND dd.DateValue = ifnull(MDTB_UMDAT, MDTB_DAT01)
       AND ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL
       AND MDTB_DAT02 IS NOT NULL
       AND ifnull(PLAF_FLIEF, PLAF_EMLIF) IS NOT NULL
       AND dd.CompanyCode = pl.CompanyCode
	AND m.Dim_ConsumptionTypeid  <> ct.Dim_ConsumptionTypeid;
