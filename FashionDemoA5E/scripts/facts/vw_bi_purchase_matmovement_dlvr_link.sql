
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   18 May 2014      Hiten	1.17              Fix for duplicate records 	  */
/*   29 Sep 2013      Lokesh	1.1               Curr and Exchange rate changes	  */
/*   27 Sep 2013      Lokesh	1.0  		  Existing version taken from CVS */
/******************************************************************************************************************/

select 'START OF PROC VW_bi_purchase_matmovement_dlvr_link',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS var_pToleranceDays;
CREATE TABLE var_pToleranceDays
(
pType  VARCHAR(10),
pToleranceDays INTEGER
);

INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('EARLY',0);
INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('LATE',0);

UPDATE var_pToleranceDays
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.early'), 0)
WHERE pType ='EARLY';
			  
UPDATE var_pToleranceDays			  
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.late'), 0)
WHERE pType ='LATE';

DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink;
CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
    SELECT f.Fact_materialmovementid,
		f.dd_MaterialDocNo,
		f.dd_MaterialDocItemNo,
		f.dd_MaterialDocYear,
		f.dd_DocumentNo,
		f.dd_DocumentItemNo,
		f.dd_DocumentScheduleNo,
		decimal(f.ct_QtyEntryUOM,18,5) * (ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1)) / ifnull(uc.marm_umren,ifnull(ucf.DENOM,1))) ct_Quantity,
		decimal(f.ct_QtyEntryUOM,18,5) * (ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1)) / ifnull(uc.marm_umren,ifnull(ucf.DENOM,1))) ct_QtyEntryUOM, 
		f.Dim_MovementTypeid,
		f.dim_DateIDPostingDate,
		f.dd_debitcreditid,
		f.Dim_AfsSizeId,
		f.Fact_materialmovementid Fact_mmid_ref, 
		dp.DateValue PostingDate,
		f.dim_uomunitofentryid,
		f.dim_Partid,
		case when f.amt_LocalCurrAmt <> 0.00000000 and f.ct_QtyEntryUOM <> 0.00000000 
			then (f.amt_LocalCurrAmt / f.ct_QtyEntryUOM) 
			else 0.00000000 
		end mm_UnitPrice,
		ROW_NUMBER() OVER(PARTITION BY f.dd_DocumentNo, f.dd_DocumentItemNo, f.Dim_AfsSizeId
				  ORDER BY dp.DateValue, f.dd_MaterialDocNo, f.dd_MaterialDocItemNo, dd.DateValue, f.dd_DocumentScheduleNo) RowSeqNo
    FROM fact_materialmovement f 
	 INNER JOIN dim_unitofmeasure uom ON f.dim_uomunitofentryid = uom.dim_unitofmeasureid
         INNER JOIN dim_Part dp1 ON f.dim_Partid = dp1.dim_Partid
        inner join dim_movementtype mt on f.Dim_MovementTypeid = mt.Dim_MovementTypeid
        inner join dim_date dp on dp.Dim_Dateid = f.dim_DateIDPostingDate
        inner join dim_date dd on dd.Dim_Dateid = f.Dim_DateIdDelivery
	LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
    WHERE mt.MovementType in (101,102,122,123,161,162) 
        and exists (select 1 from fact_materialmovement f1 
				inner join dim_movementtype mt1 on f1.Dim_MovementTypeid = mt1.Dim_MovementTypeid
                    where f.dd_DocumentNo = f1.dd_DocumentNo and f.dd_DocumentItemNo = f1.dd_DocumentItemNo
			and f.Dim_AfsSizeId = f1.Dim_AfsSizeId
                        and f1.Dim_DateIdDelivery = 1 and mt1.MovementType in (101,102,122,123,161,162))
        and exists (select 1 from fact_purchase fp 
                    where f.dd_DocumentNo = fp.dd_DocumentNo and f.dd_DocumentItemNo = fp.dd_DocumentItemNo
				and f.Dim_AfsSizeId = fp.Dim_AfsSizeId);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink;

DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink1;
CREATE TABLE fact_materialmovement_tmp_dlvrlink1 AS
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_DocumentScheduleNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
	f.dim_uomunitofentryid,
	f.dim_Partid,
	f.mm_UnitPrice,
       sum(ifnull(f1.ct_QtyEntryUOM,0)) - f.ct_QtyEntryUOM  CumuMMGRQty,
       f.PostingDate,
       f.RowSeqNo
  FROM fact_materialmovement_tmp_dlvrlink f
	inner join fact_materialmovement_tmp_dlvrlink f1
  		on  (f1.dd_DocumentNo = f.dd_DocumentNo 
			and f1.dd_DocumentItemNo = f.dd_DocumentItemNo 
			and f1.Dim_AfsSizeId = f.Dim_AfsSizeId)
  WHERE f.RowSeqNo >= f1.RowSeqNo
  GROUP BY f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_DocumentScheduleNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
	f.dim_uomunitofentryid,
	f.dim_Partid,
	f.mm_UnitPrice,
       f.PostingDate,
       f.RowSeqNo;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink1;

DROP TABLE IF EXISTS tmp_fact_purch_001;
CREATE TABLE tmp_fact_purch_001 AS
select f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.ct_DeliveryQty * (f.PriceConversion_EqualTo / case when f.PriceConversion_Denom = 0.0000 then 1.0000 else f.PriceConversion_Denom end) OrderQty,
	f.ct_ReceivedQty * (f.PriceConversion_EqualTo / case when f.PriceConversion_Denom = 0.0000 then 1.0000 else f.PriceConversion_Denom end) DlvrReceivedQty,
	(f.ct_DeliveryQty - f.ct_ReceivedQty) * (f.PriceConversion_EqualTo / case when f.PriceConversion_Denom = 0.0000 then 1.0000 else f.PriceConversion_Denom end) OpenQty,
	f.Dim_DateidDelivery dim_DeliveryDate,
	dd.DateValue DeliveryDate,
	f.Dim_DateidStatDelivery dim_StatDeliveryDate,
	f.Dim_UnitOfMeasureid,
	row_number() over (partition by f.dd_DocumentNo,f.dd_DocumentItemNo,f.Dim_AfsSizeId  
				order by dd.DateValue, f.dd_ScheduleNo) as RowSeqNum
from fact_purchase f inner join dim_date dd on dd.Dim_Dateid = f.Dim_DateidDelivery
where exists (select 1 from fact_materialmovement f1 
			inner join dim_movementtype mt1 on f1.Dim_MovementTypeid = mt1.Dim_MovementTypeid
	    where f.dd_DocumentNo = f1.dd_DocumentNo and f.dd_DocumentItemNo = f1.dd_DocumentItemNo
		and f.Dim_AfsSizeId = f1.Dim_AfsSizeId
		and f1.Dim_DateIdDelivery = 1 and mt1.MovementType in (101,102,122,123,161,162));

DROP TABLE IF EXISTS tmp_fact_purch_002;
CREATE TABLE tmp_fact_purch_002 AS
select f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.OrderQty,
	f.DlvrReceivedQty,
	f.OpenQty,
	f.dim_DeliveryDate,
	f.DeliveryDate,
	f.dim_StatDeliveryDate,
	f.Dim_UnitOfMeasureid,
	sum(ifnull(b.DlvrReceivedQty,0)) CumuDlvrReceivedQty,
	f.RowSeqNum
from tmp_fact_purch_001 f 
	inner join tmp_fact_purch_001 b 
	on f.dd_DocumentNo = b.dd_DocumentNo and f.dd_DocumentItemNo = b.dd_DocumentItemNo and f.Dim_AfsSizeId = b.Dim_AfsSizeId
where f.RowSeqNum >= b.RowSeqNum
group by f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.OrderQty,
	f.DlvrReceivedQty,
	f.OpenQty,
	f.dim_DeliveryDate,
	f.DeliveryDate,
	f.dim_StatDeliveryDate,
	f.Dim_UnitOfMeasureid,
	f.RowSeqNum;


DROP IF EXISTS fact_materialmovement_tmp_dlvrlink;
CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref, 
	f.dim_Partid,
	f.dim_uomunitofentryid,
	po.Dim_UnitOfMeasureid,
	po.dd_ScheduleNo ScheduleLineNo, 
	po.OrderQty,
	po.DlvrReceivedQty,
	po.OpenQty,
	po.dim_DeliveryDate,
	po.DeliveryDate,
	po.dim_StatDeliveryDate,
	po.CumuDlvrReceivedQty,
	po.CumuDlvrReceivedQty - po.DlvrReceivedQty CumuDlvrRcvdQtyPrev,
	row_number() over (partition by f.Fact_materialmovementid order by po.DeliveryDate, po.dd_ScheduleNo) as rownum,
	f.CumuMMGRQty,
       f.PostingDate,
	f.mm_UnitPrice,
       f.dd_DocumentScheduleNo
FROM tmp_fact_purch_002 po 
	inner join fact_materialmovement_tmp_dlvrlink1 f 
		on po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and po.Dim_AfsSizeId = f.Dim_AfsSizeId
WHERE ((po.CumuDlvrReceivedQty > f.CumuMMGRQty) or (po.CumuDlvrReceivedQty >= f.CumuMMGRQty and f.ct_QtyEntryUOM < 0)) 
	and (f.CumuMMGRQty + f.ct_QtyEntryUOM) > (po.CumuDlvrReceivedQty - po.DlvrReceivedQty)
      or (select count(*) from fact_purchase po1 
	    where po1.dd_DocumentNo = po.dd_DocumentNo and po1.dd_DocumentItemNo = po.dd_DocumentItemNo and po1.Dim_AfsSizeId = po.Dim_AfsSizeId) = 1;
  
  
  DROP IF EXISTS fact_materialmovement_tmp_dlvrlink1;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink1 as
  SELECT distinct cast(0 as decimal(15,0)) Fact_materialmovementid,
        m.dd_MaterialDocNo,
        m.dd_MaterialDocItemNo,
        m.dd_MaterialDocYear,
        m.dd_DocumentNo,
        m.dd_DocumentItemNo,
        m.ct_Quantity * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) ct_Quantity,
        m.ct_QtyEntryUOM * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) ct_QtyEntryUOM,
        m.Dim_MovementTypeid,
        m.dim_DateIDPostingDate,
        m.dd_debitcreditid,
        m.Dim_AfsSizeId,
        m.Fact_mmid_ref,
        m.ScheduleLineNo,
        m.OrderQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) OrderQty,
        m.DlvrReceivedQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) DlvrReceivedQty,
        m.OpenQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) OpenQty,
        m.dim_DeliveryDate,
        m.dim_StatDeliveryDate,
	m.DeliveryDate,
        m.CumuDlvrReceivedQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) CumuDlvrReceivedQty,
	m.CumuDlvrRcvdQtyPrev * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) CumuDlvrRcvdQtyPrev,
        m.CumuMMGRQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) CumuMMGRQty,
        m.PostingDate,
	m.dim_Partid,
	m.mm_UnitPrice,
	m.dim_uomunitofentryid,
	rownum
  FROM fact_materialmovement_tmp_dlvrlink m 
        INNER JOIN dim_Part dp1 ON m.dim_Partid = dp1.dim_Partid
	INNER JOIN dim_unitofmeasure uom ON m.dim_uomunitofentryid = uom.dim_unitofmeasureid
	LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
  where m.rownum > 1;

INSERT INTO fact_materialmovement_tmp_dlvrlink1
SELECT m.Fact_materialmovementid,
        m.dd_MaterialDocNo,
        m.dd_MaterialDocItemNo,
        m.dd_MaterialDocYear,
        m.dd_DocumentNo,
        m.dd_DocumentItemNo,
        m.ct_Quantity * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) ct_Quantity,
        m.ct_QtyEntryUOM * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) ct_QtyEntryUOM,
        m.Dim_MovementTypeid,
        m.dim_DateIDPostingDate,
        m.dd_debitcreditid,
        m.Dim_AfsSizeId,
        m.Fact_mmid_ref,
        m.ScheduleLineNo,
        m.OrderQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) OrderQty,
        m.DlvrReceivedQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) DlvrReceivedQty,
        m.OpenQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) OpenQty,
        m.dim_DeliveryDate,
        m.dim_StatDeliveryDate,
	m.DeliveryDate,
        m.CumuDlvrReceivedQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) CumuDlvrReceivedQty,
	m.CumuDlvrRcvdQtyPrev * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) CumuDlvrRcvdQtyPrev,
        m.CumuMMGRQty * (ifnull(uc.marm_umren,ifnull(ucf.DENOM,1)) / ifnull(uc.marm_umrez,ifnull(ucf.COUNTER,1))) CumuMMGRQty,
        m.PostingDate,
	m.dim_Partid,
	m.mm_UnitPrice,
	m.dim_uomunitofentryid,
	rownum
FROM fact_materialmovement_tmp_dlvrlink m
            INNER JOIN dim_Part dp1 ON m.dim_Partid = dp1.dim_Partid
	    INNER JOIN dim_unitofmeasure uom ON m.dim_uomunitofentryid = uom.dim_unitofmeasureid
	    LEFT JOIN MARM uc ON uc.marm_matnr = dp1.partnumber and uc.marm_meinh = uom.uom
	LEFT JOIN FO_UOM_CONV ucf ON uom.uom = ucf.F_UOM and dp1.unitofmeasure = ucf.T_UOM
WHERE m.rownum = 1;

drop table if exists fact_materialmovement_tmp_dlvrlink1_del;
create table fact_materialmovement_tmp_dlvrlink1_del as
select m.dd_MaterialDocNo dd_MaterialDocNo_del,
        m.dd_MaterialDocItemNo dd_MaterialDocItemNo_del,
        m.dd_MaterialDocYear dd_MaterialDocYear_del,
        m.dd_DocumentNo dd_DocumentNo_del,
        m.dd_DocumentItemNo dd_DocumentItemNo_del,
        m.ScheduleLineNo ScheduleLineNo_del
from fact_materialmovement_tmp_dlvrlink1 m
group by m.dd_MaterialDocNo,
        m.dd_MaterialDocItemNo,
        m.dd_MaterialDocYear,
        m.dd_DocumentNo,
        m.dd_DocumentItemNo,
        m.ScheduleLineNo
having count(*) > 1;

delete from fact_materialmovement_tmp_dlvrlink1
where exists (select 1 from fact_materialmovement_tmp_dlvrlink1_del m
		where m.dd_MaterialDocNo_del = dd_MaterialDocNo and
			m.dd_MaterialDocItemNo_del = dd_MaterialDocItemNo and
			m.dd_MaterialDocYear_del = dd_MaterialDocYear);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink1;


DROP IF EXISTS fact_materialmovement_tmp_dlvrlink;
CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
SELECT * FROM fact_materialmovement_tmp_dlvrlink1;

 DROP IF EXISTS fact_mm_tmp_dlvrlink_grqty;
 CREATE TABLE fact_mm_tmp_dlvrlink_grqty (Fact_materialmovementid, Fact_mmid_ref, ScheduleLineNo, QtyEntryUOM, rownum) AS
 SELECT f.Fact_materialmovementid, 
 		f.Fact_mmid_ref, 
 		f.ScheduleLineNo, 
 		case when (CumuMMGRQty + ct_QtyEntryUOM) < CumuDlvrReceivedQty then (CumuMMGRQty + ct_QtyEntryUOM - CumuDlvrRcvdQtyPrev) else f.DlvrReceivedQty end,
		rownum
  FROM fact_materialmovement_tmp_dlvrlink1 f
  WHERE Fact_materialmovementid = 0 
        and exists (select 1 from fact_purchase po inner join dim_date dd1 on dd1.Dim_Dateid = po.Dim_DateidDelivery
                      where po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and po.Dim_AfsSizeId = f.Dim_AfsSizeId
                          and po.ct_ReceivedQty > 0
                          and (dd1.DateValue > f.DeliveryDate or (dd1.DateValue = f.DeliveryDate and po.dd_ScheduleNo > f.ScheduleLineNo)));

delete from fact_materialmovement_tmp_dlvrlink
where exists (select 1 from fact_mm_tmp_dlvrlink_grqty x 
		where x.Fact_mmid_ref = fact_materialmovement_tmp_dlvrlink.Fact_mmid_ref 
			and x.ScheduleLineNo = fact_materialmovement_tmp_dlvrlink.ScheduleLineNo 
			and x.rownum = fact_materialmovement_tmp_dlvrlink.rownum);

  INSERT INTO fact_materialmovement_tmp_dlvrlink
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       gr.QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
	f.DeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate,
	f.dim_Partid,
	f.mm_UnitPrice,
	f.dim_uomunitofentryid,
       f.rownum
  FROM fact_materialmovement_tmp_dlvrlink1 f
  		inner join fact_mm_tmp_dlvrlink_grqty gr 
  		on f.Fact_mmid_ref = gr.Fact_mmid_ref and f.ScheduleLineNo = gr.ScheduleLineNo and f.rownum = gr.rownum;

    
 DROP IF EXISTS fact_mm_tmp_dlvrlink_grqty;
 CREATE TABLE fact_mm_tmp_dlvrlink_grqty (Fact_materialmovementid, Fact_mmid_ref, ScheduleLineNo, QtyEntryUOM, rownum) AS
 SELECT f.Fact_materialmovementid, 
	f.Fact_mmid_ref, 
	f.ScheduleLineNo, 
	(CumuMMGRQty + ct_QtyEntryUOM) - ifnull(f.CumuDlvrRcvdQtyPrev,0),
	rownum
  FROM fact_materialmovement_tmp_dlvrlink1 f
  WHERE Fact_materialmovementid = 0 
        and not exists (select 1 from fact_purchase po inner join dim_date dd1 on dd1.Dim_Dateid = po.Dim_DateidDelivery
                      where po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and po.Dim_AfsSizeId = f.Dim_AfsSizeId
                          and po.ct_ReceivedQty > 0
                          and (dd1.DateValue > f.DeliveryDate or (dd1.DateValue = f.DeliveryDate and po.dd_ScheduleNo > f.ScheduleLineNo)));

delete from fact_materialmovement_tmp_dlvrlink
where exists (select 1 from fact_mm_tmp_dlvrlink_grqty x 
		where x.Fact_mmid_ref = fact_materialmovement_tmp_dlvrlink.Fact_mmid_ref 
			and x.ScheduleLineNo = fact_materialmovement_tmp_dlvrlink.ScheduleLineNo 
			and x.rownum = fact_materialmovement_tmp_dlvrlink.rownum);

  INSERT INTO fact_materialmovement_tmp_dlvrlink
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       gr.QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
	f.DeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate,
	f.dim_Partid,
	f.mm_UnitPrice,
	f.dim_uomunitofentryid,
       f.rownum
  FROM fact_materialmovement_tmp_dlvrlink1 f
  	inner join fact_mm_tmp_dlvrlink_grqty gr 
  		on f.Fact_mmid_ref = gr.Fact_mmid_ref and f.ScheduleLineNo = gr.ScheduleLineNo and f.rownum = gr.rownum;
  
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink ;

  UPDATE fact_materialmovement_tmp_dlvrlink f 
  SET ct_QtyEntryUOM = CumuDlvrReceivedQty - CumuMMGRQty        
  WHERE exists (select 1 from fact_materialmovement_tmp_dlvrlink1 m
                where m.Fact_materialmovementid = 0 and m.Fact_mmid_ref = f.Fact_materialmovementid)
        and f.Fact_materialmovementid <> 0;
  

UPDATE fact_materialmovement f 
FROM fact_materialmovement_tmp_dlvrlink m 
    SET f.Dim_DateIdDelivery = m.dim_DeliveryDate,
        f.Dim_DateIdStatDelivery = m.dim_StatDeliveryDate,
        f.dd_DocumentScheduleNo = m.ScheduleLineNo,
        f.ct_OrderQuantity = m.OrderQty,
        f.ct_QtyEntryUOM = m.ct_QtyEntryUOM,
        f.ct_Quantity = m.ct_QtyEntryUOM,
        f.amt_LocalCurrAmt = ifnull(m.mm_UnitPrice * m.ct_QtyEntryUOM, 0)	
WHERE f.Fact_materialmovementid = m.Fact_materialmovementid
	AND not exists (select 1 from fact_materialmovement a
			where a.Fact_materialmovementid <> f.Fact_materialmovementid
				and f.dd_MaterialDocNo = a.dd_MaterialDocNo and f.dd_MaterialDocItemNo = a.dd_MaterialDocItemNo and f.dd_MaterialDocYear = a.dd_MaterialDocYear
				and f.dd_DocumentNo = a.dd_DocumentNo and f.dd_DocumentItemNo = a.dd_DocumentItemNo and a.dd_DocumentScheduleNo = m.ScheduleLineNo);    

 call vectorwise(combine 'fact_materialmovement');
  
  DROP IF EXISTS fact_materialmovement_tmp_dlvrlink1;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink1 AS
  SELECT (select max(mm.Fact_materialmovementid) from fact_materialmovement mm)
  		+ row_number() over() Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.mm_UnitPrice,
       f.PostingDate 
  FROM  fact_materialmovement_tmp_dlvrlink f
  WHERE f.Fact_materialmovementid = 0;
  

INSERT INTO fact_materialmovement  
       (Fact_materialmovementid,
       dd_MaterialDocNo,
       dd_MaterialDocItemNo,
       dd_MaterialDocYear,
       dd_DocumentNo,
       dd_DocumentItemNo,
       dd_SalesOrderNo,
       dd_SalesOrderItemNo,
       dd_SalesOrderDlvrNo,
       dd_GLAccountNo,
       dd_ProductionOrderNumber,
       dd_ProductionOrderItemNo,
       dd_BatchNumber,
       dd_ValuationType,
       dd_debitcreditid,
       dd_GoodsMoveReason,
       amt_LocalCurrAmt,
       amt_DeliveryCost,
       amt_AltPriceControl,
       amt_OnHand_ValStock,
       ct_Quantity,
       ct_QtyEntryUOM,
       ct_QtyGROrdUnit,
       ct_QtyOnHand_ValStock,
       ct_QtyOrdPriceUnit,
       Dim_MovementTypeid,
       dim_Companyid,
       Dim_Currencyid,
	   Dim_Currencyid_TRA,
	   Dim_Currencyid_GBL,	   
       Dim_Partid,
       Dim_Plantid,
       Dim_StorageLocationid,
       Dim_Vendorid,
       dim_DateIDMaterialDocDate,
       dim_DateIDPostingDate,
       Dim_DateIDDocCreation,
       Dim_DateIDOrdered,
       dim_producthierarchyid,
       dim_MovementIndicatorid,
       dim_Customerid,
       dim_CostCenterid,
       Dim_AccountCategoryid,
       Dim_ConsumptionTypeid,
       Dim_ControllingAreaid,
       Dim_CustomerGroup1id,
       Dim_DocumentCategoryid,
       Dim_DocumentTypeid,
       Dim_ItemCategoryid,
       Dim_ItemStatusid,
       dim_productionorderstatusid,
       Dim_productionordertypeid,
       Dim_PurchaseGroupid,
       Dim_PurchaseOrgid,
       Dim_SalesDocumentTypeid,
       Dim_SalesGroupid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       dim_salesorderrejectreasonid,
       Dim_SalesOrgid,
       dim_specialstockid,
       Dim_StockTypeid,
       Dim_UnitOfMeasureid,
       dirtyrow,
       Dim_MaterialGroupid,
       amt_ExchangeRate_GBL,
       amt_ExchangeRate,
       Dim_Termid,
       dd_ConsignmentFlag,
       Dim_ProfitCenterId,
       Dim_ReceivingPlantId,
       Dim_ReceivingPartId,
       amt_POUnitPrice,
       amt_StdUnitPrice,
       Dim_DateidCosting,
       Dim_IncoTermid,
       Dim_DateIdDelivery,
       Dim_DateIdStatDelivery,
       Dim_GRStatusid,
       dd_incoterms2,
       dd_IntOrder,
       amt_PlannedPrice,
       amt_PlannedPrice1,
       dd_DocumentScheduleNo,
       Dim_RecvIssuStorLocid,
       ct_OrderQuantity,
       Dim_POPlantidOrdering,
       Dim_POPlantidSupplying,
       Dim_POStorageLocid,
       Dim_POIssuStorageLocid,
       Dim_InspUsageDecisionId,
       ct_LotNotThruQM,
       dd_ReferenceDocNo,
       dd_ReferenceDocItem,
       LotsAcceptedFlag,
       LotsRejectedFlag,
       PercentLotsInspectedFlag,
       PercentLotsRejectedFlag,
       PercentLotsAcceptedFlag,
       LotsSkippedFlag,
       LotsAwaitingInspectionFlag,
       LotsInspectedFlag,
       QtyRejectedExternalAmt,
       QtyRejectedInternalAmt,
       Dim_AfsSizeId,
       amt_vendorspend,
       dd_vendorspendflag,
       dim_uomunitofentryid,
       dim_unitofmeasureorderunitid,
       lotsreceivedflag,
       dd_inspectionstatusingrdoc)
  SELECT m.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_SalesOrderNo,
       f.dd_SalesOrderItemNo,
       f.dd_SalesOrderDlvrNo,
       f.dd_GLAccountNo,
       f.dd_ProductionOrderNumber,
       f.dd_ProductionOrderItemNo,
       f.dd_BatchNumber,
       f.dd_ValuationType,
       f.dd_debitcreditid,
       f.dd_GoodsMoveReason,
       ifnull(m.mm_UnitPrice * m.ct_QtyEntryUOM, 0) amt_LocalCurrAmt,
       0 amt_DeliveryCost,
       0 amt_AltPriceControl,
       0 amt_OnHand_ValStock,
       m.ct_QtyEntryUOM ct_Quantity,
       m.ct_QtyEntryUOM ct_QtyEntryUOM,
       0 ct_QtyGROrdUnit,
       0 ct_QtyOnHand_ValStock,
       0 ct_QtyOrdPriceUnit,
       f.Dim_MovementTypeid,
       f.dim_Companyid,
       f.Dim_Currencyid,
	   f.Dim_Currencyid_TRA,
	   f.Dim_Currencyid_GBL,
       f.Dim_Partid,
       f.Dim_Plantid,
       f.Dim_StorageLocationid,
       f.Dim_Vendorid,
       f.dim_DateIDMaterialDocDate,
       f.dim_DateIDPostingDate,
       f.Dim_DateIDDocCreation,
       f.Dim_DateIDOrdered,
       f.dim_producthierarchyid,
       f.dim_MovementIndicatorid,
       f.dim_Customerid,
       f.dim_CostCenterid,
       f.Dim_AccountCategoryid,
       f.Dim_ConsumptionTypeid,
       f.Dim_ControllingAreaid,
       f.Dim_CustomerGroup1id,
       f.Dim_DocumentCategoryid,
       f.Dim_DocumentTypeid,
       f.Dim_ItemCategoryid,
       f.Dim_ItemStatusid,
       f.dim_productionorderstatusid,
       f.Dim_productionordertypeid,
       f.Dim_PurchaseGroupid,
       f.Dim_PurchaseOrgid,
       f.Dim_SalesDocumentTypeid,
       f.Dim_SalesGroupid,
       f.Dim_SalesOrderHeaderStatusid,
       f.Dim_SalesOrderItemStatusid,
       f.dim_salesorderrejectreasonid,
       f.Dim_SalesOrgid,
       f.dim_specialstockid,
       f.Dim_StockTypeid,
       f.Dim_UnitOfMeasureid,
       f.dirtyrow,
       f.Dim_MaterialGroupid,
       f.amt_ExchangeRate_GBL,
       f.amt_ExchangeRate,
       f.Dim_Termid,
       f.dd_ConsignmentFlag,
       f.Dim_ProfitCenterId,
       f.Dim_ReceivingPlantId,
       f.Dim_ReceivingPartId,
       f.amt_POUnitPrice,
       f.amt_StdUnitPrice,
       f.Dim_DateidCosting,
       f.Dim_IncoTermid,
       m.dim_DeliveryDate Dim_DateIdDelivery,
       m.dim_StatDeliveryDate Dim_DateIdStatDelivery,
       f.Dim_GRStatusid,
       f.dd_incoterms2,
       f.dd_IntOrder,
       f.amt_PlannedPrice,
       f.amt_PlannedPrice1,
       m.ScheduleLineNo dd_DocumentScheduleNo,
       f.Dim_RecvIssuStorLocid,
       m.OrderQty ct_OrderQuantity,
       f.Dim_POPlantidOrdering,
       f.Dim_POPlantidSupplying,
       f.Dim_POStorageLocid,
       f.Dim_POIssuStorageLocid,
       f.Dim_InspUsageDecisionId,
       0 ct_LotNotThruQM,
       f.dd_ReferenceDocNo,
       f.dd_ReferenceDocItem,
       f.LotsAcceptedFlag,
       f.LotsRejectedFlag,
       f.PercentLotsInspectedFlag,
       f.PercentLotsRejectedFlag,
       f.PercentLotsAcceptedFlag,
       f.LotsSkippedFlag,
       f.LotsAwaitingInspectionFlag,
       f.LotsInspectedFlag,
       f.QtyRejectedExternalAmt,
       f.QtyRejectedInternalAmt,
       f.Dim_AfsSizeId,
       0,
       f.dd_vendorspendflag,
       f.dim_uomunitofentryid,
       f.dim_unitofmeasureorderunitid,
       f.lotsreceivedflag,
       f.dd_inspectionstatusingrdoc
  FROM fact_materialmovement f 
       inner join fact_materialmovement_tmp_dlvrlink1 m on f.Fact_materialmovementid = m.Fact_mmid_ref;

   call vectorwise(combine 'fact_materialmovement');
       
UPDATE fact_materialmovement f 
FROM dim_date dp, dim_date ds, var_pToleranceDays E,var_pToleranceDays L
     set f.Dim_GRStatusid = case when dp.DateValue > (ANSIDATE(ds.DateValue) + (INTERVAL '1' DAY)*L.pToleranceDays ) then 3 
                                 when dp.DateValue < (ANSIDATE(ds.DateValue) - (INTERVAL '1' DAY)*E.pToleranceDays ) then 4 
                                 else 5 end
WHERE f.Dim_DateIdStatDelivery <> 1
     and dp.Dim_Dateid = f.dim_DateIDPostingDate
     and ds.Dim_Dateid = f.Dim_DateIdStatDelivery
     and E.pType = 'EARLY'
     and L.pType = 'LATE';

   call vectorwise(combine 'fact_materialmovement');
     
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement ;

DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink;
DROP IF EXISTS fact_materialmovement_tmp_dlvrlink1;
DROP IF EXISTS fact_mm_tmp_dlvrlink_grqty;
DROP IF EXISTS fact_mm_dlvr_link_scheduleno_temp;
DROP TABLE IF EXISTS tmp_fact_purch_001;
DROP TABLE IF EXISTS tmp_fact_purch_002;

select 'END OF PROC VW_bi_purchase_matmovement_dlvr_link',TIMESTAMP(LOCAL_TIMESTAMP);
