/**************************************************************************************************************/
/*   Script         : 	 */
/*   Author         : Lokesh */
/*   Created On     : 17 May 2013 */
/*   Description    : Stored Proc bi_populate_disputemgmt_fact migration from MySQL to Vectorwise syntax   */
/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   18 Sep 2013      Lokesh    1.1               Currency and exchange rate changes							  */
/*   17 May 2013      Lokesh    1.0               Existing code migrated to Vectorwise                            */
/******************************************************************************************************************/

DELETE FROM fact_disputemanagement;

delete from NUMBER_FOUNTAIN where table_name = 'fact_disputemanagement';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_disputemanagement',ifnull(max(fact_disputemanagementid),0)
FROM fact_disputemanagement;


DROP TABLE IF EXISTS tmp_FDM_DCPROC_fact_disputemanagement;
CREATE TABLE tmp_FDM_DCPROC_fact_disputemanagement
AS
SELECT *,left(FDM_DCPROC_TIMESTAMP,4) + '-' + left(shift(FDM_DCPROC_TIMESTAMP,-4),2) + '-' + left(shift(FDM_DCPROC_TIMESTAMP,-6),2) as fdm_datevalue
FROM FDM_DCPROC;

update tmp_FDM_DCPROC_fact_disputemanagement
set fdm_datevalue = '1900-01-01' where fdm_datevalue like '%-00-%';

DROP TABLE IF EXISTS tmp_SCMG_T_CASE_ATTR_fact_disputemanagement;
CREATE TABLE tmp_SCMG_T_CASE_ATTR_fact_disputemanagement
AS
SELECT *,left(CREATE_TIME,4) + '-' + left(shift(CREATE_TIME,-4),2) + '-' + left(shift(CREATE_TIME,-6),2) as CREATE_TIME_datevalue,
	left(CHANGE_TIME,4) + '-' + left(shift(CHANGE_TIME,-4),2) + '-' + left(shift(CHANGE_TIME,-6),2) as CHANGE_TIME_datevalue,
	left(CLOSING_TIME,4) + '-' + left(shift(CLOSING_TIME,-4),2) + '-' + left(shift(CLOSING_TIME,-6),2) as CLOSING_TIME_datevalue
FROM SCMG_T_CASE_ATTR;


update tmp_SCMG_T_CASE_ATTR_fact_disputemanagement
set closing_time_datevalue = '1900-01-01' where closing_time_datevalue like '%-00-%';
update tmp_SCMG_T_CASE_ATTR_fact_disputemanagement
set CHANGE_TIME_datevalue = '1900-01-01' where CHANGE_TIME_datevalue like '%00%';
update tmp_SCMG_T_CASE_ATTR_fact_disputemanagement
set CREATE_TIME_datevalue = '1900-01-01' where CREATE_TIME_datevalue like '%00%';
 

  INSERT INTO fact_disputemanagement(Dim_CompanyId,
                                    Dim_CustomerId,
                                    Dim_DateIdClearing,
                                    dd_ClearingDocumentNo,
                                    dd_AssignmentNumber,
                                    dd_FiscalYear,
                                    dd_AccountingDocNo,
                                    dd_AccountingDocItemNo,
                                    Dim_DateIdPosting,
                                    Dim_DateIdCreated,
                                    Dim_BusinessAreaId,
                                    dd_debitcreditid,
                                    Dim_CreditControlAreaId,
                                    Dim_DocumentTypeId,
                                    dd_FiscalPeriod,
                                    Dim_ChartOfAccountsId,
                                    Dim_CurrencyId,
									Dim_CurrencyId_TRA,
									Dim_CurrencyId_GBL,									
                                    Dim_PaymentReasonId,
                                    Dim_PostingKeyId,
                                    dd_InvoiceNumberTransBelongTo,
                                    Dim_DateIdBaseDateForDueDateCalc,
                                    Dim_DateIdAccDocDateEntered,
                                    Dim_ClearedFlagId,
                                    amt_ExchangeRate_GBL,
                                    amt_ExchangeRate,
                                    dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_SalesScheduleNo,
                                    Dim_NetDueDateId,
                                    dd_BillingNo,
                                    dd_ProductionOrderNo,
                                    Dim_SpecialGlTransactionTypeId,
                                    Dim_AccountsReceivableDocStatusId,
                                    dd_RelevantInvoiceFiscalYear,
                                    Dim_PartId,
                                    Dim_PlantId,
                                    Dim_PaymentTermsId,
                                    ct_CashDiscountDays1,
                                    dd_CustomerPONumber,
                                    dd_CreditRep,
                                    dd_CreditLimit,
                                    Dim_RiskClassId,
                                    dd_DisputeCaseID,
                                    amt_OriginalAmtDisputed,
                                    amt_DisputedAmt,
                                    amt_DisputeAmtPaid,
                                    amt_DisputeAmtCredited,
                                    amt_DisputeAmtCleared,
                                    amt_DisputeAmtWrittenOff,
                                    Dim_SalesOrgid,
                                    Dim_DateidAmountPaid,
                                    Dim_DateidAmountCredited,
                                    Dim_DateidAmountCleared,
                                    Dim_DateidAmtWrittenOff,
                                    dd_DisputeStatus,
                                    Dim_DateidDisputeCaseCreated,
                                    Dim_DateidDisputeCaseLastChanged,
                                    Dim_DateidDisputeClosed,
                                    Dim_DateidDisputePlannedClosed,
                                    dd_Processor,
                                    dd_PersonResponsible,
                                    dd_DisputeCaseTitle,
                                    dd_DisputeEscalationReason,
                                    dd_DisputeCategory,
                                    dd_DisputePriority,fact_disputemanagementid)
									
  SELECT f.Dim_CompanyId,
          f.Dim_CustomerId,
          f.Dim_DateIdClearing,
          f.dd_ClearingDocumentNo,
          f.dd_AssignmentNumber,
          f.dd_FiscalYear,
          f.dd_AccountingDocNo,
          f.dd_AccountingDocItemNo,
          f.Dim_DateIdPosting,
          f.Dim_DateIdCreated,
          f.Dim_BusinessAreaId,
          f.dd_debitcreditid,
          f.Dim_CreditControlAreaId,
          f.Dim_DocumentTypeId,
          f.dd_FiscalPeriod,
          f.Dim_ChartOfAccountsId,
          f.Dim_CurrencyId,
		  f.Dim_CurrencyId_TRA,
		  f.Dim_CurrencyId_GBL,
          f.Dim_PaymentReasonId,
          f.Dim_PostingKeyId,
          f.dd_InvoiceNumberTransBelongTo,
          f.Dim_DateIdBaseDateForDueDateCalc,
          f.Dim_DateIdAccDocDateEntered,
          f.Dim_ClearedFlagId,
          f.amt_ExchangeRate_GBL,
          f.amt_ExchangeRate,
          f.dd_SalesDocNo,
          f.dd_SalesItemNo,
          f.dd_SalesScheduleNo,
          f.Dim_NetDueDateId,
          f.dd_BillingNo,
          f.dd_ProductionOrderNo,
          f.Dim_SpecialGlTransactionTypeId,
          f.Dim_AccountsReceivableDocStatusId,
          f.dd_RelevantInvoiceFiscalYear,
          f.Dim_PartId,
          f.Dim_PlantId,
          f.Dim_PaymentTermsId,
          f.ct_CashDiscountDays1,
          f.dd_CustomerPONumber,
          f.dd_CreditRep,
          f.dd_CreditLimit,
          f.Dim_RiskClassId,
          f.dd_DisputeCaseID,

          FDM_DCPROC_DELTA_ORIGINAL amt_OriginalAmtDisputed,
          FDM_DCPROC_DELTA_DISPUTED amt_DisputedAmt,
          FDM_DCPROC_DELTA_PAID amt_DisputeAmtPaid,
          FDM_DCPROC_DELTA_CREDITED amt_DisputeAmtCredited,
          FDM_DCPROC_DELTA_WRITE_OFF amt_DisputeAmtCleared,
          FDM_DCPROC_DELTA_NOT_SOLVED amt_DisputeAmtWrittenOff,
          Dim_SalesOrgid,
		  
			ifnull((select dt.Dim_Dateid from dim_date dt			  
                 where dt.DateValue = fdm_datevalue
                 and dt.CompanyCode = dcm.CompanyCode and amt_DisputeAmtPaid <> 0 and FDM_DCPROC_TIMESTAMP > 0),1) Dim_DateidAmountPaid,
			ifnull((select dt.Dim_Dateid from dim_date dt			  
                 where dt.DateValue = fdm_datevalue
                 and dt.CompanyCode = dcm.CompanyCode and amt_DisputeAmtCredited <> 0 and FDM_DCPROC_TIMESTAMP > 0),1) Dim_DateidAmountCredited,
			ifnull((select dt.Dim_Dateid from dim_date dt			  
                 where dt.DateValue = fdm_datevalue
                 and dt.CompanyCode = dcm.CompanyCode and amt_DisputeAmtCleared <> 0 and FDM_DCPROC_TIMESTAMP > 0),1) Dim_DateidAmountCleared,
			ifnull((select dt.Dim_Dateid from dim_date dt			  
                 where dt.DateValue = fdm_datevalue
                 and dt.CompanyCode = dcm.CompanyCode and amt_DisputeAmtWrittenOff <> 0 and FDM_DCPROC_TIMESTAMP > 0),1) Dim_DateidAmtWrittenOff,

          a.STAT_ORDERNO dd_DisputeStatus,

			ifnull((select dt.Dim_Dateid from dim_date dt			  
                 where dt.DateValue = a.CREATE_TIME_datevalue
                 and dt.CompanyCode = dcm.CompanyCode and amt_DisputeAmtPaid <> 0 and a.CREATE_TIME > 0),1) Dim_DateidDisputeCaseCreated,
			ifnull((select dt.Dim_Dateid from dim_date dt			  
                 where dt.DateValue = a.CHANGE_TIME_datevalue
                 and dt.CompanyCode = dcm.CompanyCode and amt_DisputeAmtPaid <> 0 and a.CHANGE_TIME > 0),1) Dim_DateidDisputeCaseLastChanged,
			ifnull((select dt.Dim_Dateid from dim_date dt			  
                 where dt.DateValue = a.CLOSING_TIME_datevalue
                 and dt.CompanyCode = dcm.CompanyCode and amt_DisputeAmtPaid <> 0 and a.CLOSING_TIME > 0),1) Dim_DateidDisputeClosed,
				 
			ifnull((select dt.Dim_Dateid from dim_date dt			  
                 where dt.DateValue = a.PLAN_END_DATE
                 and dt.CompanyCode = dcm.CompanyCode ),1) Dim_DateidDisputePlannedClosed,
				 
          ifnull(a.PROCESSOR,'Not Set') dd_Processor,
          ifnull(a.RESPONSIBLE,'Not Set') dd_PersonResponsible,
          ifnull(a.CASE_TITLE,'Not Set') dd_DisputeCaseTitle,
          ifnull(a.ESCAL_REASON,'Not Set') dd_DisputeEscalationReason,
          ifnull(a.CATEGORY,'Not Set') dd_DisputeCategory,
         ifnull(a.PRIORITY,0) dd_DisputePriority,
		 (SELECT max_id  from NUMBER_FOUNTAIN WHERE table_name = 'fact_disputemanagement') + row_number() over()
		  
  FROM fact_accountsreceivable f
      INNER JOIN dim_company dcm ON dcm.Dim_CompanyId = f.Dim_CompanyId
      inner join (tmp_SCMG_T_CASE_ATTR_fact_disputemanagement a inner join tmp_FDM_DCPROC_fact_disputemanagement b on a.CASE_GUID = b.FDM_DCPROC_CASE_GUID_LOC)
          on FDM_DCPROC_OBJ_KEY = concat(dcm.CompanyCode,f.dd_AccountingDocNo,f.dd_fiscalyear,LPAD(cast(f.dd_AccountingDocItemNo as CHAR(3)),3,'0'))
            AND EXT_KEY = f.dd_DisputeCaseId
  WHERE b.FDM_DCPROC_OBJ_TYPE = 'BSEG' and b.FDM_DCPROC_NEW_CLASS in ('DISP_INV','DISP_PAY','DISP_RES');
  
  
  
  update NUMBER_FOUNTAIN set max_id = ( select ifnull(max(fact_disputemanagementid),0) from fact_disputemanagement)
where table_name = 'fact_disputemanagement';	  

DROP TABLE IF EXISTS tmp_FDM_DCPROC_fact_disputemanagement;
DROP TABLE IF EXISTS tmp_SCMG_T_CASE_ATTR_fact_disputemanagement;

