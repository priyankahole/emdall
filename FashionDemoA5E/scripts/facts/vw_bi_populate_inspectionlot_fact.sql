
DROP TABLE IF EXISTS tmp_del1_inslot;
CREATE TABLE tmp_del1_inslot
AS
SELECT DISTINCT il.dd_inspectionlotno from fact_inspectionlot il;

DROP TABLE IF EXISTS tmp_ins1_inslot;
CREATE TABLE tmp_ins1_inslot
AS
SELECT il.dd_inspectionlotno from fact_inspectionlot il where 1=2;

insert into tmp_ins1_inslot
select distinct q.QALS_PRUEFLOS from qals q;

call vectorwise(combine 'tmp_ins1_inslot-tmp_del1_inslot');

DROP TABLE IF EXISTS tmp_ins2_inslot;
CREATE TABLE tmp_ins2_inslot
AS
SELECT DISTINCT q.*
FROM qals q,tmp_ins1_inslot i
WHERE q.QALS_PRUEFLOS = i.dd_inspectionlotno;

DROP TABLE IF EXISTS tmp_dim_profitcenter;
CREATE TABLE tmp_dim_profitcenter
AS
SELECT  ControllingArea, ProfitCenterCode,QALS_ERSTELDAT, MIN(ValidTo) as min_validto
FROM dim_profitcenter, qals
WHERE  ControllingArea = QALS_KOKRS
AND ProfitCenterCode = QALS_PRCTR
AND ValidTo >= QALS_ERSTELDAT
AND RowIsCurrent = 1
GROUP BY  ControllingArea, ProfitCenterCode,QALS_ERSTELDAT ;

DROP TABLE IF EXISTS tmp_dim_profitcenter2;
CREATE TABLE tmp_dim_profitcenter2
AS
SELECT DISTINCT p1.*,p2.dim_profitcenterid
FROM dim_profitcenter p2,tmp_dim_profitcenter p1
WHERE p1.ControllingArea = p2.ControllingArea
AND p1.ProfitCenterCode = p2.ProfitCenterCode
AND p1.min_validto = p2.validto
AND p2.RowIsCurrent = 1;


INSERT INTO fact_inspectionlot(fact_inspectionlotid,
                               ct_actualinspectedqty,
                               ct_actuallotqty,
                               ct_blockedqty,
                               ct_inspectionlotqty,
                               ct_postedqty,
                               ct_qtydefective,
                               ct_qtyreturned,
                               ct_reserveqty,
                               ct_sampleqty,
                               ct_samplesizeqty,
                               ct_scrapqty,
                               ct_unrestrictedqty,
                               dd_ObjectNumber,
                               dd_batchno,
                               dd_changedby,
                               dd_createdby,
                               dd_documentitemno,
                               dd_documentno,
                               dd_inspectionlotno,
                               dd_MaterialDocItemNo,
                               dd_MaterialDocNo,
                               dd_MaterialDocYear,
                               dd_orderno,
                               dd_ScheduleNo,
                               Dim_AccountCategoryid,
                               dim_costcenterid,
                               dim_dateidinspectionend,
                               dim_dateidinspectionstart,
                               dim_dateidkeydate,
                               dim_dateidlotcreated,
                               dim_dateidposting,
                               -- QALS_AENDERDAT dim_dateidrecordchanged,
                               -- QALS_ERSTELDAT dim_dateidrecordcreated,
                               dim_documenttypetextid,
                               dim_inspectionlotmiscid,
                               dim_inspectionlotoriginid,
                               dim_inspectionlotstoragelocationid,
                               dim_inspectionsamplestatusid,
                               dim_inspectionseverityid,
                               dim_inspectionstageid,
                               dim_inspectiontypeid,
                               dim_itemcategoryid,
                               dim_lotunitofmeasureid,
                               dim_manufacturerid,
                               dim_ObjectCategoryid,
                               dim_partid,
                               dim_plantid,
                               dim_purchaseorgid,
                               dim_routeid,
                               dim_sampleunitofmeasureid,
                               dim_specialstockid,
                               dim_statusprofileid,
                               dim_storagelocationid,
                               dim_tasklisttypeid,
                               dim_tasklistusageid,
                               dim_vendorid,
                               dim_warehousenumberid,
                               dim_Controllingareaid,
                               dim_profitcenterid)
        SELECT (SELECT ifnull(max(fil.fact_inspectionlotid), 1) id
             FROM fact_inspectionlot fil)
          + row_number() over(),
                  QALS_LMENGEPR ct_actualinspectedqty,
          QALS_LMENGEIST ct_actuallotqty,
          QALS_LMENGE04 ct_blockedqty,
          QALS_LOSMENGE ct_inspectionlotqty,
          QALS_LMENGEZUB ct_postedqty,
          QALS_LMENGESCH ct_qtydefective,
          QALS_LMENGE07 ct_qtyreturned,
          QALS_LMENGE05 ct_reserveqty,
          QALS_LMENGE03 ct_sampleqty,
          QALS_GESSTICHPR ct_samplesizeqty,
          QALS_LMENGE02 ct_scrapqty,
          QALS_LMENGE01 ct_unrestrictedqty,
          ifnull(QALS_OBJNR,'Not Set') dd_ObjectNumber,
          ifnull(QALS_CHARG, 'Not Set') dd_batchno,
          ifnull(QALS_AENDERER, 'Not Set') dd_changedby,
          QALS_ERSTELLER dd_createdby,
          QALS_EBELP dd_documentitemno,
          ifnull(QALS_EBELN, 'Not Set') dd_documentno,
          QALS_PRUEFLOS dd_inspectionlotno,
          QALS_ZEILE dd_MaterialDocItemNo,
          ifnull(QALS_MBLNR, 'Not Set') dd_MaterialDocNo,
          QALS_MJAHR dd_MaterialDocYear,
          ifnull(QALS_AUFNR, 'Not Set') dd_orderno,
          QALS_ETENR dd_ScheduleNo,
          (SELECT dim_accountcategoryid
             FROM dim_accountcategory ac
            WHERE ac.Category = ifnull(QALS_KNTTP, 'Not Set')
                  AND ac.RowIsCurrent = 1)
             Dim_AccountCategoryid,
          ifnull((SELECT dim_costcenterid
             FROM dim_costcenter cc
            WHERE     cc.Code = ifnull(QALS_KOSTL, 'Not Set')
                  AND cc.ControllingArea = ifnull(QALS_KOKRS, 'Not Set')
                  AND cc.RowIsCurrent = 1),1)
             dim_costcenterid,
          ifnull((SELECT dim_dateid
                   FROM dim_date ie
                  WHERE ie.DateValue = QALS_PAENDTERM
                        AND ie.CompanyCode = dp.CompanyCode
                                                AND QALS_PAENDTERM IS NOT NULL), 1)
             dim_dateidinspectionend,
          ifnull((SELECT dim_dateid
                   FROM dim_date dis
                  WHERE dis.DateValue = QALS_PASTRTERM
                        AND dis.CompanyCode = dp.CompanyCode
                                                AND QALS_PASTRTERM IS NOT NULL), 1)
             dim_dateidinspectionstart,
          ifnull((SELECT dim_dateid
                   FROM dim_date kd
                  WHERE kd.DateValue = QALS_GUELTIGAB
                        AND kd.CompanyCode = dp.CompanyCode
                                                AND QALS_GUELTIGAB IS NOT NULL), 1)
             dim_dateidkeydate,
          ifnull((SELECT dim_dateid
                      FROM dim_date lcd
                     WHERE lcd.DateValue = QALS_ENSTEHDAT
                           AND lcd.CompanyCode = dp.CompanyCode
                                                   AND QALS_ENSTEHDAT IS NOT NULL), 1)
             dim_dateidlotcreated,
          ifnull((SELECT dim_dateid
                   FROM dim_date pd
                  WHERE pd.DateValue = QALS_BUDAT
                        AND pd.companycode = dp.CompanyCode
                                                AND QALS_BUDAT IS NOT NULL), 1)
             dim_dateidposting,
          -- QALS_AENDERDAT dim_dateidrecordchanged,
          -- QALS_ERSTELDAT dim_dateidrecordcreated,
          (SELECT dim_documenttypetextid
             FROM dim_documenttypetext dtt
            WHERE dtt.type = ifnull(QALS_BLART, 'Not Set')
                  AND dtt.RowIsCurrent = 1)
             dim_documenttypetextid,
          (SELECT dim_inspectionlotmiscid
             FROM dim_inspectionlotmisc
            WHERE     AutomaticInspectionLot = ifnull(QALS_STAT01, 'Not Set')
                  AND BatchManagementRequired = ifnull(QALS_XCHPF, 'Not Set')
                  AND CompleteInspection = ifnull(QALS_HPZ, 'Not Set')
                  AND DocumentationRequired = ifnull(QALS_STAT19, 'Not Set')
                  AND InspectionPlanRequired = ifnull(QALS_STAT20, 'Not Set')
                  AND InspectionStockQuantity = ifnull(QALS_INSMK, 'Not Set')
                  AND ShotTermInspectionComplete =
                         ifnull(QALS_STAT14, 'Not Set')
                  AND StockPostingsComplete = ifnull(QALS_STAT34, 'Not Set')
                  AND UsageDecisionMade = ifnull(QALS_STAT35, 'Not Set')
                  AND LotSkipped = ifnull(QALS_KZSKIPLOT, 'Not Set'))
             dim_inspectionlotmiscid,
          (SELECT dim_inspectionlotoriginid
             FROM dim_inspectionlotorigin ilo
            WHERE ilo.InspectionLotOriginCode =
                     ifnull(QALS_HERKUNFT, 'Not Set')
                  AND ilo.RowIsCurrent = 1)
             dim_inspectionlotoriginid,
          ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation isl
                  WHERE     isl.LocationCode = QALS_LAGORTVORG
                        AND isl.RowIsCurrent = 1
                        AND isl.Plant = dp.PlantCode
                                                AND QALS_LAGORTVORG IS NOT NULL), 1)
          dim_inspectionlotstoragelocationid,
          (SELECT dim_inspectionsamplestatusid
             FROM dim_inspectionsamplestatus iss
            WHERE iss.statuscode = ifnull(QALS_LVS_STIKZ, 'Not Set')
                  AND iss.RowIsCurrent = 1)
             dim_inspectionsamplestatusid,
          ifnull((SELECT dim_inspectionseverityid
             FROM dim_inspectionseverity isvr
            WHERE isvr.InspectionSeverityCode = QALS_PRSCHAERFE
                  AND isvr.RowIsCurrent = 1),1)
             dim_inspectionseverityid,
          (SELECT dim_inspectionstageid
             FROM dim_inspectionstage istg
            WHERE istg.InspectionStageCode = QALS_PRSTUFE
                  AND istg.InspectionStageRuleCode =
                         ifnull(QALS_DYNREGEL, 'Not Set')
                  AND istg.RowIsCurrent = 1)
             dim_inspectionstageid,
          (SELECT dim_inspectiontypeid
             FROM dim_inspectiontype ityp
            WHERE ityp.InspectionTypeCode = ifnull(QALS_ART, 'Not Set')
                  AND ityp.RowIsCurrent = 1)
             dim_inspectiontypeid,
          (SELECT dim_itemcategoryid
             FROM dim_itemcategory icc
            WHERE icc.CategoryCode = ifnull(QALS_PSTYP, 'Not Set')
                  AND icc.RowIsCurrent = 1)
             dim_itemcategoryid,
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure luom
            WHERE luom.UOM = ifnull(QALS_MENGENEINH, 'Not Set')
                  AND luom.RowIsCurrent = 1)
             dim_lotunitofmeasureid,
          ifnull((SELECT dim_vendorid
             FROM dim_vendor dv
            WHERE dv.VendorNumber = ifnull(QALS_HERSTELLER, 'Not Set')
                  AND dv.RowIsCurrent = 1),1)
             dim_manufacturerid,
          (SELECT dim_ObjectCategoryid
             FROM dim_ObjectCategory oc
            WHERE oc.ObjectCategoryCode = ifnull(QALS_OBTYP, 'Not Set')
                  AND oc.RowIsCurrent = 1)
             dim_ObjectCategoryid,
          dim_partid,
          dim_plantid,
          (SELECT dim_purchaseorgid
             FROM dim_purchaseorg po
            WHERE po.PurchaseOrgCode = ifnull(QALS_EKORG, 'Not Set')
                  AND po.RowIsCurrent = 1)
             dim_purchaseorgid,
          (SELECT dim_routeid
             FROM dim_route r
            WHERE r.RouteCode = ifnull(QALS_LS_ROUTE, 'Not Set')
                  AND r.RowIsCurrent = 1)
             dim_routeid,
          (SELECT dim_unitofmeasureid
             FROM dim_unitofmeasure uom
            WHERE uom.UOM = ifnull(QALS_EINHPROBE, 'Not Set')
                  AND uom.RowIsCurrent = 1)
             dim_sampleunitofmeasureid,
          (SELECT dim_specialstockid
             FROM dim_specialstock ss
            WHERE ss.specialstockindicator = ifnull(QALS_SOBKZ, 'Not Set')
                  AND ss.RowIsCurrent = 1)
             dim_specialstockid,
          (SELECT dim_statusprofileid
             FROM dim_statusprofile sp
            WHERE sp.StatusProfileCode = ifnull(QALS_STSMA, 'Not Set')
                  AND sp.RowIsCurrent = 1)
             dim_statusprofileid,
          ifnull((SELECT dim_storagelocationid
                   FROM dim_storagelocation sl
                  WHERE sl.LocationCode = ifnull(QALS_LAGORTCHRG, 'Not Set')
                        AND sl.Plant = dp.PlantCode
                        AND sl.RowIsCurrent = 1
                                                AND QALS_LAGORTCHRG IS NOT NULL), 1)
          dim_storagelocationid,
          (SELECT dim_tasklisttypeid
             FROM dim_tasklisttype tlt
            WHERE tlt.TaskListTypeCode = ifnull(QALS_PLNTY, 'Not Set')
                  AND tlt.RowIsCurrent = 1)
             dim_tasklisttypeid,
          (SELECT dim_tasklistusageid
             FROM dim_tasklistusage tlu
            WHERE tlu.UsageCode = ifnull(QALS_PPLVERW, 'Not Set')
                  AND tlu.RowIsCurrent = 1)
             dim_tasklistusageid,
          ifnull((SELECT dim_vendorid
             FROM dim_vendor dv1
            WHERE dv1.VendorNumber = ifnull(QALS_LIFNR, 'Not Set')
                  AND dv1.RowIsCurrent = 1),1)
             dim_vendorid,
          (SELECT dim_warehouseid
             FROM dim_warehousenumber wn
            WHERE wn.WarehouseCode = ifnull(QALS_LGNUM, 'Not Set')
                  AND wn.RowIsCurrent = 1)
             dim_warehousenumberid,
          ifnull((SELECT dim_controllingareaid
             FROM dim_controllingarea ca
            WHERE ca.ControllingAreaCode = QALS_KOKRS),1) dim_controllingareaid,
ifnull((SELECT  pc.dim_profitcenterid FROM tmp_dim_profitcenter2 pc WHERE  pc.ControllingArea = QALS_KOKRS AND pc.ProfitCenterCode = QALS_PRCTR 
AND pc.QALS_ERSTELDAT = QALS_ERSTELDAT ),1) dim_profitcenterid
          /*ifnull((SELECT  pc.dim_profitcenterid
             FROM dim_profitcenter pc,
                                tmp_dim_profitcenter tmp_pc
            WHERE  pc.ControllingArea = QALS_KOKRS
               AND pc.ProfitCenterCode = QALS_PRCTR
              AND pc.ValidTo >= QALS_ERSTELDAT
              AND pc.RowIsCurrent = 1
                          AND tmp_pc.ControllingArea = QALS_KOKRS
              AND tmp_pc.ProfitCenterCode = QALS_PRCTR
                          AND pc.ValidTo = tmp_pc.min_validto), 1)  1 dim_profitcenterid */
      FROM tmp_ins2_inslot q
          INNER JOIN dim_plant dp
             ON dp.PlantCode = q.QALS_WERK AND dp.RowIsCurrent = 1
          INNER JOIN dim_part dpa
             ON     dpa.PartNumber = QALS_MATNR
                AND dpa.Plant = dp.PlantCode
                AND dpa.RowIsCurrent = 1;
    /*WHERE NOT EXISTS
             (SELECT 1
                FROM fact_inspectionlot il
               WHERE il.dd_inspectionlotno = q.QALS_PRUEFLOS)*/



DROP TABLE IF EXISTS tmp_del1_inslot;
DROP TABLE IF EXISTS tmp_ins1_inslot;
DROP TABLE IF EXISTS tmp_ins2_inslot;
DROP TABLE IF EXISTS tmp_dim_profitcenter2;
DROP TABLE IF EXISTS tmp_dim_profitcenter;




