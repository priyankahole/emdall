


/*********************************************Change History*******************************************************/
/*   Date             By        Version           Desc                                                            */
/*   11 Jan 2014      Lokesh    1.15              Removed the incorrect last 2 queries. In sync with prod/app now */
/*   11 Jan 2014      Lokesh    1.14              Add <> in updates for performance      */
/******************************************************************************************************************/


Drop table if exists mdtb_tmp_q01;
Create table mdtb_tmp_q01 as Select *,(ifnull(MDTB_UMDAT,MDTB_DAT01) -  MDTB_DAT02) leadcolupd from  mdtb Where  ifnull(MDTB_UMDAT, MDTB_DAT01) IS NOT NULL AND MDTB_DAT02 IS NOT NULL ;

UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET m.Dim_ItemCategoryid = ic.Dim_ItemCategoryid
  WHERE      tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_BANFN
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_ItemCategoryid <> ic.Dim_ItemCategoryid;



UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET         m.Dim_Vendorid = v.Dim_Vendorid
  WHERE       tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_BANFN
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_Vendorid <> v.Dim_Vendorid;



        UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET m.Dim_FixedVendorid = fv.Dim_Vendorid
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_BANFN
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_FixedVendorid <> fv.Dim_Vendorid;



        UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET m.Dim_DocumentTypeid = dt.Dim_DocumentTypeid
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_BANFN
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_DocumentTypeid <> dt.Dim_DocumentTypeid;



        UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET m.Dim_ConsumptionTypeid = ct.Dim_ConsumptionTypeid
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.Dim_ConsumptionTypeid <> ct.Dim_ConsumptionTypeid;

drop table if exists eban_p00;
Create table eban_p00 as Select *, ifnull(EBAN_FLIEF, EBAN_LIFNR) eban_colupd from eban where ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL;
Drop table if exists fact_mrp_del_p00;
Create table fact_mrp_del_p00 as Select fact_mrpid,ct_leadTimeVariance,leadcolupd,k.MDKP_MATNR,k.MDKP_PLWRK,eban_colupd,eban_BANFN,eban_BNFPO,dp.LeadTime
From fact_mrp m, eban_p00 pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
        tmpvariable_00e
  WHERE    tmpTotalCount > 0 and  m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN;

Update fact_mrp_del_p00 pdo
Set pdo.ct_leadTimeVariance =   ifnull(((pdo.leadcolupd)
                  - (Select v_leadTime
                        from tmp_getLeadTime glt
                        where glt.pPart =pdo.MDKP_MATNR
                              AND glt.pPlant =  pdo.MDKP_PLWRK
                              AND glt.pVendor =   pdo.eban_colupd
                              AND glt.DocumentNumber=  pdo.eban_BANFN
                              AND glt.DocumentLineNumber=  pdo.eban_BNFPO
			      AND glt.fact_script_name = 'bi_populate_mrp_fact'
                              AND glt.DocumentType=  'PR')), -1 * pdo.LeadTime)
WHERE pdo.ct_leadTimeVariance <>  ifnull(((pdo.leadcolupd)
                  - (Select v_leadTime
                        from tmp_getLeadTime glt
                        where glt.pPart =pdo.MDKP_MATNR
                              AND glt.pPlant =  pdo.MDKP_PLWRK
                              AND glt.pVendor =   pdo.eban_colupd
                              AND glt.DocumentNumber=  pdo.eban_BANFN
                              AND glt.DocumentLineNumber=  pdo.eban_BNFPO
                              AND glt.fact_script_name = 'bi_populate_mrp_fact'
                              AND glt.DocumentType=  'PR')), -1 * pdo.LeadTime);


Update fact_mrp m
from fact_mrp_del_p00 pdo
Set m.ct_leadTimeVariance = pdo.ct_leadTimeVariance
Where m.fact_mrpid = pdo.fact_mrpid
AND m.ct_leadTimeVariance <> pdo.ct_leadTimeVariance;



		
/* Update curr/exchg rate from eban */


UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
	tmpvariable_00e
    SET dim_CurrencyID =  1
	WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND m.dim_CurrencyID <> 1;		

UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_vendor v,
        dim_itemcategory ic,
        dim_documenttype dt,
        dim_consumptiontype ct,
        dim_part dp,
        dim_vendor fv,
        dim_plant pl,
		dim_currency cur,dim_company dc ,
	tmpvariable_00e
    SET dim_CurrencyID = cur.dim_CurrencyID 
	WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND dp.Dim_Partid = m.Dim_Partid
        AND pl.dim_plantid = m.dim_plantid
        AND v.VendorNumber = ifnull(EBAN_LIFNR, 'Not Set')
        AND fv.VendorNumber = ifnull(EBAN_FLIEF, 'Not Set')
        AND ic.CategoryCode = ifnull(pr.eban_PSTYP, 'Not Set')
        AND dt.Type = ifnull(pr.eban_BSART, 'Not Set')
        AND dt.Category = ifnull(pr.eban_BSTYP, 'Not Set')
        AND ct.ConsumptionCode = ifnull(pr.EBAN_KZVBR, 'Not Set')
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
		AND dc.companycode = pl.companycode
		And dc.currency = cur.currencycode
		and dc.RowIsCurrent = 1
	AND m.dim_CurrencyID <> cur.dim_CurrencyID;		
		

UPDATE fact_mrp m
FROM
       eban pr,
       mdkp k,
       mdtb_tmp_q01 t,
       tmpvariable_00e
   SET dim_CurrencyID_GBL =
          ifnull((SELECT cur.dim_CurrencyID
                    FROM dim_currency cur
                   WHERE pGlobalCurrency = cur.currencycode), 1)
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.eban_banfn
       AND m.dd_DocumentItemNo = pr.eban_BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
       AND ifnull(m.dim_CurrencyID_GBL, -1) <>
              ifnull((SELECT cur.dim_CurrencyID
                        FROM dim_currency cur
                       WHERE pGlobalCurrency = cur.currencycode), 1);

UPDATE fact_mrp m
FROM
       eban pr,
       mdkp k,
       mdtb_tmp_q01 t,
       tmpvariable_00e
   SET dim_CurrencyID_TRA =
          ifnull((SELECT cur.dim_CurrencyID
                    FROM dim_currency cur
                   WHERE pr.EBAN_WAERS = cur.currencycode), 1)
 WHERE     m.Dim_ActionStateid = 2
       AND k.MDKP_DTNUM = t.MDTB_DTNUM
       AND m.dd_DocumentNo = pr.eban_banfn
       AND m.dd_DocumentItemNo = pr.eban_BNFPO
       AND m.dd_DocumentNo = t.MDTB_DELNR
       AND m.dd_DocumentItemNo = t.MDTB_DELPS
       AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
       AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
       AND ifnull(m.dim_CurrencyID_TRA, -1) <>
              ifnull((SELECT cur.dim_CurrencyID
                        FROM dim_currency cur
                       WHERE pr.EBAN_WAERS = cur.currencycode), 1);		

UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_plant pl,
	tmpvariable_00e
    SET amt_ExchangeRate = 	
			ifnull((SELECT exchangeRate from tmp_getExchangeRate1 ,dim_company dc
					where  tmpTotalCount > 0 
					and dc.companycode = pl.companycode
					and dc.RowIsCurrent = 1
					and pFromCurrency = pr.EBAN_WAERS 
					and pToCurrency = dc.currency
					and pFromExchangeRate = 0
					and pDate = pr.eban_BADAT
					and fact_script_name = 'bi_populate_mrp_fact'),1) 				
	WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND pl.dim_plantid = m.dim_plantid
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND ifnull(m.amt_ExchangeRate,-1)  <>  ifnull((SELECT exchangeRate from tmp_getExchangeRate1 ,dim_company dc
                                        where  tmpTotalCount > 0
                                        and dc.companycode = pl.companycode
                                        and dc.RowIsCurrent = 1
                                        and pFromCurrency = pr.EBAN_WAERS
                                        and pToCurrency = dc.currency
                                        and pFromExchangeRate = 0
                                        and pDate = pr.eban_BADAT
                                        and fact_script_name = 'bi_populate_mrp_fact'),1);		
		
UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_plant pl,
	tmpvariable_00e
    SET amt_ExchangeRate_GBL = 	
			ifnull((SELECT exchangeRate from tmp_getExchangeRate1 
					where  tmpTotalCount > 0 and pFromCurrency = pr.EBAN_WAERS 
					and pToCurrency = pGlobalCurrency
					and pFromExchangeRate = 0
					and pDate = ANSIDATE(LOCAL_TIMESTAMP)
					and fact_script_name = 'bi_populate_mrp_fact'),1) 	
	WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND ifnull(m.amt_ExchangeRate_GBL,-1) <> ifnull((SELECT exchangeRate from tmp_getExchangeRate1
                                        where  tmpTotalCount > 0 and pFromCurrency = pr.EBAN_WAERS
                                        and pToCurrency = pGlobalCurrency
                                        and pFromExchangeRate = 0
                                        and pDate = ANSIDATE(LOCAL_TIMESTAMP)
                                        and fact_script_name = 'bi_populate_mrp_fact'),1);
		

UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_plant pl,
	tmpvariable_00e
    SET amt_ExtendedPrice = ifnull(pr.eban_menge * eban_preis / ifnull((case when eban_peinh = 0 then null else eban_peinh end), 1), 0)  
	/* LK: eban_preis is in tran currency ( EBAN_WAERS ), no need to use exchg rates here */	
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
		AND ifnull(m.amt_ExtendedPrice,-1) <> ifnull(pr.eban_menge * eban_preis / ifnull((case when eban_peinh = 0 then null else eban_peinh end), 1), 0);		
		
		
UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_plant pl,
	tmpvariable_00e
    SET amt_ExtendedPrice_GBL = ifnull(pr.eban_MENGE * eban_PREIS / ifnull((case when eban_PEINH = 0 then null else eban_PEINH end), 1), 0) * amt_ExchangeRate_GBL
  WHERE     m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND ifnull(m.amt_ExtendedPrice_GBL,-1) <> ifnull(pr.eban_MENGE * eban_PREIS / ifnull((case when eban_PEINH = 0 then null else eban_PEINH end), 1), 0) * amt_ExchangeRate_GBL;



UPDATE fact_mrp m
From  eban pr,
        mdkp k,
        mdtb_tmp_q01 t,
        dim_plant pl,
	tmpvariable_00e
    SET Dim_DateidReschedule = ifnull((SELECT od.dim_dateid
                                      FROM dim_date od
                                      WHERE od.DateValue = t.MDTB_UMDAT
                                          AND od.CompanyCode = pl.CompanyCode), 1)
  WHERE     tmpTotalCount > 0 and m.Dim_ActionStateid = 2
        AND k.MDKP_DTNUM = t.MDTB_DTNUM
        AND m.dd_DocumentNo = pr.eban_banfn
        AND m.dd_DocumentItemNo = pr.eban_BNFPO
        AND m.dd_DocumentNo = t.MDTB_DELNR
        AND m.dd_DocumentItemNo = t.MDTB_DELPS
        AND ifnull(EBAN_FLIEF, EBAN_LIFNR) IS NOT NULL
        AND m.dd_PlannScenarioLTP = k.MDKP_PLSCN
	AND Dim_DateidReschedule <> ifnull((SELECT od.dim_dateid
                                      FROM dim_date od
                                      WHERE od.DateValue = t.MDTB_UMDAT
                                          AND od.CompanyCode = pl.CompanyCode), 1);		

drop table if exists mdtb_tmp_q01;
drop table if exists fact_mrp_del_p00;
drop table if exists eban_p00;

