/*********************************************************************
Last update date : July 26, 2012
Last updated by : Hiten
**********************************************************************/

select 'A - START OF PROC bi_populate_invaging_fact_salesorderstock',TIMESTAMP(LOCAL_TIMESTAMP);


DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

  DELETE FROM MSKA WHERE MSKA_KAEIN = 0 and MSKA_KAINS = 0 and MSKA_KALAB = 0 and MSKA_KASPE = 0;

   /* Intermediate tables -- replace MBEW_NO_BWTAR  */
drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = decimal((b.STPRS / b.PEINH), 18, 5)
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.VERPR / b.PEINH), 18, 5)
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;


  INSERT INTO dim_storagelocation(Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent,dim_storagelocationid)
   SELECT DISTINCT MSKA_LGORT,
                   MSKA_LGORT,
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   'Not Set',
                   MSKA_WERKS,
                   'Not Set',
                   date(LOCAL_TIMESTAMP),
                   1,row_number() over ()	--This should be changed later ( to max + 1 ). This should be standard - common for all such scenarios
     FROM MSKA m
    WHERE MSKA_LGORT IS NOT NULL
          AND NOT EXISTS (SELECT 1
                            FROM dim_storagelocation
                           WHERE LocationCode = MSKA_LGORT AND Plant = MSKA_WERKS);

select 'A1 - Before final insert',TIMESTAMP(LOCAL_TIMESTAMP);

delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryaging',ifnull(max(fact_inventoryagingid),0)
FROM fact_inventoryaging;

  INSERT INTO fact_inventoryaging (ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                ct_BlockedStock,
                                ct_StockInTransfer,
                                ct_BlockedConsgnStock,
                                ct_ConsgnStockInQInsp,
                                ct_RestrictedConsgnStock,
                                ct_UnrestrictedConsgnStock,
                                ct_BlockedStockReturns,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_BlockedStockAmt,
                                amt_BlockedStockAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StockInTransferAmt,
                                amt_StockInTransferAmt_GBL,
                                amt_UnrestrictedConsgnStockAmt,
                                amt_UnrestrictedConsgnStockAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_ValuationType,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dd_MovementType,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
								dim_Currencyid_TRA,
								dim_Currencyid_GBL,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_StockCategoryid,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL,amt_ExchangeRate)
  SELECT a.MSKA_KALAB StockQty,
          a.MSKA_KAEIN ct_TotalRestrictedStock,
          a.MSKA_KAINS ct_StockInQInsp,
          a.MSKA_KASPE ct_BlockedStock,
          0 ct_StockInTransfer,
          0 ct_BlockedConsgnStock,
          0 ct_ConsgnStockInQInsp,
          0 ct_RestrictedConsgnStock,
          0 ct_UnrestrictedConsgnStock,
          0 ct_BlockedStockReturns,
          (b.multiplier * a.MSKA_KALAB) StockValueAmt,
          ifnull(((b.multiplier * a.MSKA_KALAB) * ifnull((select z.exchangeRate from tmp_getExchangeRate1 z 
							  where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_invaging_fact_salesorderstock' and z.pToCurrency = pGlobalCurrency and z.pDate = a.MSKA_ERSDA),0)),0) StockValueAmt_GBL,
          ifnull((b.multiplier * a.MSKA_KASPE), 0) amt_BlockedStockAmt,
          ifnull(((b.multiplier * a.MSKA_KASPE) * ifnull((select z.exchangeRate from tmp_getExchangeRate1 z 
							  where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_invaging_fact_salesorderstock' and z.pToCurrency = pGlobalCurrency and z.pDate = a.MSKA_ERSDA),0)), 0) amt_BlockedStockAmt_GBL,
          ifnull((b.multiplier * a.MSKA_KAINS), 0) amt_StockInQInspAmt,
          ifnull(((b.multiplier * a.MSKA_KAINS) * ifnull((select z.exchangeRate from tmp_getExchangeRate1 z 
							  where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_invaging_fact_salesorderstock' and z.pToCurrency = pGlobalCurrency and z.pDate = a.MSKA_ERSDA),0)), 0) amt_StockInQInspAmt_GBL,
          0 amt_StockInTransferAmt,
          0 amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt,
          0 amt_UnrestrictedConsgnStockAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          'Not Set' ValuationType,
          a.MSKA_VBELN DocumentNo,
          a.MSKA_POSNR DocumentItemNo,
          'Not Set' MovementType,
          dt.dim_dateid StorageLocEntryDate,
          dt.dim_dateid LastReceivedDate,
          dp.Dim_PartID,
          p.dim_plantid,
          sloc.Dim_StorageLocationid,
          cmp.dim_Companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
		  dc.dim_currencyid dim_currencyid_TRA,	
		  ifnull((SELECT Dim_Currencyid
                 FROM Dim_Currency dcr
                WHERE dcr.CurrencyCode = pGlobalCurrency ), 1) Dim_Currencyid_GBL  
          1 dim_stocktypeid,
          spstk.dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          pg.Dim_PurchaseGroupid,
          dph.dim_producthierarchyid,
          uom.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          ifnull((select stkc.dim_stockcategoryid from dim_stockcategory stkc
                  where stkc.categorycode = 'MSKA'),1) dim_StockCategoryid , (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging')+ row_number() over (),
	  ifnull((select z.exchangeRate from tmp_getExchangeRate1 z 
		   where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_invaging_fact_salesorderstock' 
			and z.pToCurrency = pGlobalCurrency and z.pDate = a.MSKA_ERSDA),1) amt_ExchangeRate_GBL,
			1 amt_ExchangeRate
     FROM MSKA a
          INNER JOIN dim_plant p
             ON a.MSKA_WERKS = p.PlantCode
          INNER JOIN dim_Company cmp
             ON cmp.CompanyCode = p.CompanyCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b   ON     b.MATNR = a.MSKA_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKA_MATNR AND dp.Plant = a.MSKA_WERKS
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_date dt
             ON a.MSKA_ERSDA = dt.DateValue and dt.CompanyCode = p.CompanyCode
          INNER JOIN dim_storagelocation sloc
             ON sloc.LocationCode = MSKA_LGORT AND sloc.Plant = MSKA_WERKS
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = cmp.Currency
          INNER JOIN dim_specialstock spstk
             ON spstk.specialstockindicator = a.MSKA_SOBKZ, tmp_GlobalCurr_001 gbl;

call vectorwise(combine 'fact_inventoryaging');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;

select 'B - END OF PROC bi_populate_invaging_fact_salesorderstock',TIMESTAMP(LOCAL_TIMESTAMP);