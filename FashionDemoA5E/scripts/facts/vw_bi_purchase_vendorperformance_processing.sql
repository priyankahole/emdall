/***************************************************************************************************** 
Last Update Date : Aug 20, 12013
Last Updated By : Hiten Suthar
Last Change Description : Late-Early days calculations changed to consider business days only
*****************************************************************************************************/

INSERT INTO processinglog 
(processinglogid,
referencename, 
startdate, 
description) 
VALUES ((select ifnull(max(processinglogid),0) +1 from processinglog),
'bi_purchase_vendorperformance_processing',
TIMESTAMP_WO_TZ(current_timestamp), 
'bi_purchase_vendorperformance_processing START');
  
Drop table if exists fact_purchase_tmp_709;
Drop table if exists doc_tmp_709;
Drop table if exists mainholder_709;
Drop table if exists holder_709;
Drop table if exists fact_purch_tmp_709_fl_01;

drop table if exists fact_purch_invqty_001;
create table fact_purch_invqty_001 as
select sum(e.EKBE_MENGE) InvQty, e.EKBE_EBELN, e.EKBE_EBELP
from EKBE_PURCH e 
where e.EKBE_VGABE = '2'
group by e.EKBE_EBELN, e.EKBE_EBELP;

drop table if exists fact_purch_lineqty_001;
create table fact_purch_lineqty_001 as
select sum(f1.ct_DeliveryQty) LineQty, f1.dd_DocumentNo f1_DocumentNo, f1.dd_DocumentItemNo f1_DocumentItemNo
from fact_purchase f1 inner join dim_purchasemisc m on m.Dim_PurchaseMiscid = f1.Dim_PurchaseMiscid
where ((m.ItemGRIndicator = 'X'
            AND exists (select 1 from fact_purchase b 
                        where f1.dd_DocumentNo = b.dd_DocumentNo and f1.dd_DocumentItemNo = b.dd_DocumentItemNo
                              and ((b.ct_DeliveryQty - b.ct_ReceivedQty) > 0 OR b.Dim_DateidLastGR = 1)))
          OR f1.dd_InvStatus <> 'Full')		
group by f1.dd_DocumentNo, f1.dd_DocumentItemNo;

/*** Getting only work/business days - start ***/

drop table if exists tmp_dim_date_001;
create table tmp_dim_date_001 as
select Dim_Dateid, DateValue, CompanyCode, 
	row_number() over(partition by companyCode order by DateValue) DateSeqNo,
	0 MaxDateSeqNo, 0 MinDateSeqNo
from dim_date
where isapublicholiday = 0 and isaweekendday = 0;

drop table if exists tmp_dimdt_holiday_001;
create table tmp_dimdt_holiday_001 as
select Dim_Dateid, DateValue, CompanyCode, 0 DateSeqNo
from dim_date
where isapublicholiday = 1 or isaweekendday = 1;

drop table if exists tmp_dimdt_holiday_002;
create table tmp_dimdt_holiday_002 as
select a.Dim_Dateid, a.DateValue, a.CompanyCode, a.DateSeqNo, max(b.DateSeqNo) MaxDateSeqNo
from tmp_dimdt_holiday_001 a 
	inner join tmp_dim_date_001 b on a.CompanyCode = b.CompanyCode
where a.DateValue >= b.DateValue
group by a.Dim_Dateid, a.DateValue, a.CompanyCode, a.DateSeqNo;

drop table if exists tmp_dimdt_holiday_001;
create table tmp_dimdt_holiday_001 as
select a.Dim_Dateid, a.DateValue, a.CompanyCode, a.DateSeqNo, a.MaxDateSeqNo, min(b.DateSeqNo) MinDateSeqNo
from tmp_dimdt_holiday_002 a 
	inner join tmp_dim_date_001 b on a.CompanyCode = b.CompanyCode
where a.DateValue <= b.DateValue
group by a.Dim_Dateid, a.DateValue, a.CompanyCode, a.DateSeqNo, a.MaxDateSeqNo;

insert into tmp_dim_date_001 (Dim_Dateid, DateValue, CompanyCode, DateSeqNo, MaxDateSeqNo, MinDateSeqNo)
select Dim_Dateid, DateValue, CompanyCode, DateSeqNo, MaxDateSeqNo, MinDateSeqNo
from tmp_dimdt_holiday_001;

/*** Getting only work/business days - end ***/

Create table fact_purch_tmp_709_fl_01  as
SELECT   row_number() over(order by dd_DocumentNo, dd_DocumentItemNo, sd.DateValue, dd_ScheduleNo) iid,
	 fact_purchaseid v_fact_vendperfid,
         dd_DocumentNo v_dd_DocumentNo,
         dd_DocumentItemNo v_dd_DocumentItemNo,
         dd_ScheduleNo v_dd_ScheduleNo,
         case when m.ItemGRIndicator = 'X' and (m.ItemDeliveryComplete = 'X' or m.DeliveryComplete = 'X') then ct_ReceivedQty else ct_DeliveryQty end v_ct_DeliveryQty,
         ct_ReceivedQty v_ct_ReceivedQty,
         dd.DateValue v_DeliveryDate,
	 a.Dim_DateidDelivery,
	 (select case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end from tmp_dim_date_001 dd0 where dd0.Dim_Dateid = dd.Dim_Dateid) DlvrDateSeqNo,
         sd.DateValue v_StatDeliveryDate,
	 a.Dim_DateidStatDelivery,
	 (select case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end from tmp_dim_date_001 dd0 where dd0.Dim_Dateid = sd.Dim_Dateid) StatDateSeqNo,
          case when m.ItemDeliveryComplete = 'X' or m.DeliveryComplete = 'X' then 'X' else '' end v_DlvrComplete,
          m.ItemGRIndicator v_GRIndicator,
          pl.CompanyCode v_CompanyCode, 
	cast (0 as DECIMAL(18,4)) vQtyEarly ,
	cast (0 as DECIMAL(18,4)) vQtyOnTime ,
	cast (0 as DECIMAL(18,4)) vQtyLate ,
	cast (0 as bigint) vLateDlvrDays, 
	cast (0 as bigint) vEarlyDlvrDays ,
	cast (0 as DECIMAL(18,4)) vQtyEarly_ddt ,
	cast (0 as DECIMAL(18,4)) vQtyOnTime_ddt ,
	cast (0 as DECIMAL(18,4)) vQtyLate_ddt ,
	0 vLateDlvrDays_ddt, 
	0 vEarlyDlvrDays_ddt,
	cast (0 as DECIMAL(18,4)) vQtyReturn,
	ANSIDATE(current_date)  v_LastGRDate,
	ANSIDATE(current_date) v_FirstGRDate,
	cast (0 as bigint) v_tmp_RcvdDlvCount,
	ifnull(e.InvQty,0) vInvoiceQty ,
	ifnull(f1.LineQty,0) vPOLineQty,
	ifnull((SELECT property_value
		 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.late'), 1) pLateToleranceDays,
	ifnull((SELECT property_value
		 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.early'), 1) pEarlyToleranceDays,
	180 vPastDueCutOffDays,
	cast (0 as bigint) pflag,
	row_number() over(partition by dd_DocumentNo, dd_DocumentItemNo order by sd.DateValue, dd_ScheduleNo) LineDlvrSeq,
	pl.CompanyCode
  FROM fact_purchase a 
	inner join dim_purchasemisc m on m.Dim_PurchaseMiscid = a.Dim_PurchaseMiscid
	inner join Dim_Date sd on sd.Dim_Dateid = a.Dim_DateidStatDelivery
	inner join Dim_Date dd on dd.Dim_Dateid = a.Dim_DateidDelivery
	inner join Dim_Plant pl on pl.Dim_Plantid = a.Dim_PlantidOrdering
	inner join fact_purch_lineqty_001 f1 on f1.f1_DocumentNo = a.dd_DocumentNo and f1.f1_DocumentItemNo = a.dd_DocumentItemNo
	left join fact_purch_invqty_001 e on e.EKBE_EBELN = dd_DocumentNo and e.EKBE_EBELP = dd_DocumentItemNo;


drop table if exists fact_materialmovement_vendperform;
create table fact_materialmovement_vendperform as
  select a.dd_DocumentNo Document_No,
         a.dd_DocumentItemNo DocumentLine_No,
         a.dd_DocumentScheduleNo dd_ScheduleNo ,
         mt.MovementType MovementType,
         dt.DateValue PostingDate,
	 (select case when dd0.DateSeqNo = 0 then dd0.MinDateSeqNo else dd0.DateSeqNo end from tmp_dim_date_001 dd0 where dd0.Dim_Dateid = dt.Dim_Dateid) PostDateSeqNo,
         a.ct_QtyEntryUOM Quantity,
	 a.dim_DateIDPostingDate,
	 dt.CompanyCode
  from fact_materialmovement a 
        inner join dim_movementtype mt on a.Dim_MovementTypeid = mt.Dim_MovementTypeid
        inner join dim_date dt on a.dim_DateIDPostingDate = dt.Dim_Dateid
  where mt.MovementType in (122,161,123,162,101,102);  


drop table if exists fact_purchase_tmp_709_QtyEarly;
create table fact_purchase_tmp_709_QtyEarly as
Select Sum(ifnull(a.Quantity,0)) vQtyEarly, a.Document_No,a.DocumentLine_No,a.dd_scheduleno
From fact_materialmovement_vendperform a 
	inner join fact_purch_tmp_709_fl_01 b
	on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo	
where a.PostDateSeqNo < (b.StatDateSeqNo - b.pEarlyToleranceDays)
	and b.v_GRIndicator = 'X'
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno;

drop table if exists fact_purchase_tmp_709_QtyOnTime;
create table fact_purchase_tmp_709_QtyOnTime as
Select Sum(ifnull(a.Quantity,0)) vQtyOnTime, a.Document_No,a.DocumentLine_No,a.dd_scheduleno
From fact_materialmovement_vendperform a 
	inner join fact_purch_tmp_709_fl_01 b
	on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo	
where a.PostDateSeqNo <= b.StatDateSeqNo + b.pLateToleranceDays
	and b.v_GRIndicator = 'X'
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno;

drop table if exists fact_purchase_tmp_709_QtyEarly_ddt;
create table fact_purchase_tmp_709_QtyEarly_ddt as
Select Sum(ifnull(a.Quantity,0)) vQtyEarly_ddt, a.Document_No,a.DocumentLine_No,a.dd_scheduleno
From fact_materialmovement_vendperform a 
	inner join fact_purch_tmp_709_fl_01 b
	on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo	
where a.PostDateSeqNo < b.DlvrDateSeqNo - b.pEarlyToleranceDays
	and b.v_GRIndicator = 'X'
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno;

drop table if exists fact_purchase_tmp_709_QtyOnTime_ddt;
create table fact_purchase_tmp_709_QtyOnTime_ddt as
Select Sum(ifnull(a.Quantity,0)) vQtyOnTime_ddt, a.Document_No,a.DocumentLine_No,a.dd_scheduleno
From fact_materialmovement_vendperform a 
	inner join fact_purch_tmp_709_fl_01 b
	on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo	
where a.PostDateSeqNo <= b.DlvrDateSeqNo + b.pLateToleranceDays
	and b.v_GRIndicator = 'X'
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno;

drop table if exists fact_purchase_tmp_709_01;
create table fact_purchase_tmp_709_01 as
select iid,
	v_fact_vendperfid,
        v_dd_DocumentNo,
        v_dd_DocumentItemNo,
        v_dd_ScheduleNo,
        v_ct_DeliveryQty,
        v_ct_ReceivedQty,
        v_DeliveryDate,
        v_StatDeliveryDate,
        v_DlvrComplete,
        v_GRIndicator,
        v_CompanyCode, 	
	IFNULL((select m.vQtyEarly from fact_purchase_tmp_709_QtyEarly m 
	 		where m.Document_No = b.v_dd_DocumentNo and m.DocumentLine_No = b.v_dd_DocumentItemNo 
	 			and m.dd_scheduleno = b.v_dd_ScheduleNo),0) vQtyEarly,
	IFNULL((select m.vQtyOnTime from fact_purchase_tmp_709_QtyOnTime m 
	 		where m.Document_No = b.v_dd_DocumentNo and m.DocumentLine_No = b.v_dd_DocumentItemNo 
	 			and m.dd_scheduleno = b.v_dd_ScheduleNo),0) vQtyOnTime,
	vQtyLate,
	vLateDlvrDays, 
	vEarlyDlvrDays,
	IFNULL((select m.vQtyEarly_ddt from fact_purchase_tmp_709_QtyEarly_ddt m 
	 		where m.Document_No = b.v_dd_DocumentNo and m.DocumentLine_No = b.v_dd_DocumentItemNo 
	 			and m.dd_scheduleno = b.v_dd_ScheduleNo),0) vQtyEarly_ddt,
	IFNULL((select m.vQtyOnTime_ddt from fact_purchase_tmp_709_QtyOnTime_ddt m 
	 		where m.Document_No = b.v_dd_DocumentNo and m.DocumentLine_No = b.v_dd_DocumentItemNo 
	 			and m.dd_scheduleno = b.v_dd_ScheduleNo),0) vQtyOnTime_ddt,
	vQtyLate_ddt,
	vLateDlvrDays_ddt, 
	vEarlyDlvrDays_ddt,
	IFNULL((Select Sum(ifnull(a.Quantity,0))
	      From fact_materialmovement_vendperform a 
	      Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
	      and a.MovementType in (122,123,161,162)),0) vQtyReturn,
	(Select max(a.PostingDate)
	From fact_materialmovement_vendperform a
	Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo
		AND a.MovementType = 101) v_LastGRDate,
	(Select min(a.PostingDate)
	From fact_materialmovement_vendperform a
	Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo
		AND a.MovementType = 101) v_FirstGRDate,
	IFNULL((select count(*) from fact_purchase a
	      where a.dd_DocumentNo = b.v_dd_DocumentNo and a.dd_DocumentItemNo = b.v_dd_DocumentItemNo
		   and ifnull(a.ct_ReceivedQty,0) > 0),1) v_tmp_RcvdDlvCount,
	vInvoiceQty,
	vPOLineQty,
	pLateToleranceDays,
	pEarlyToleranceDays,
	vPastDueCutOffDays,
	pflag,
	DlvrDateSeqNo,
	StatDateSeqNo
from fact_purch_tmp_709_fl_01 b
where b.v_GRIndicator = 'X';

drop table if exists fact_purchase_tmp_709_QtyEarly;
drop table if exists fact_purchase_tmp_709_QtyOnTime;
drop table if exists fact_purchase_tmp_709_QtyEarly_ddt;
drop table if exists fact_purchase_tmp_709_QtyOnTime_ddt;


delete from fact_purch_tmp_709_fl_01
where v_GRIndicator = 'X';


drop table if exists fact_purchase_tmp_709_LDays;
create table fact_purchase_tmp_709_LDays as
select min(a.PostingDate) GRDate,
	min(b.v_StatDeliveryDate) DlvrDt,
	a.Document_No,
	a.DocumentLine_No,
	a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo,
	min(a.PostDateSeqNo) PostDateSeqNo
from fact_materialmovement_vendperform a
	inner join fact_purchase_tmp_709_01 b
		on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
where a.MovementType = 101
	AND a.PostDateSeqNo > (b.StatDateSeqNo + b.pLateToleranceDays)
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo;

drop table if exists fact_purchase_tmp_709_EDays;
create table fact_purchase_tmp_709_EDays as
select max(a.PostingDate) GRDate,
	min(b.v_StatDeliveryDate) DlvrDt,
	a.Document_No,
	a.DocumentLine_No,
	a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo,
	max(a.PostDateSeqNo) PostDateSeqNo
from fact_materialmovement_vendperform a
	inner join fact_purchase_tmp_709_01 b
		on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
where a.MovementType = 101
	AND a.PostDateSeqNo < (b.StatDateSeqNo - b.pEarlyToleranceDays)
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo;
		
drop table if exists fact_purchase_tmp_709_LDays_ddt;
create table fact_purchase_tmp_709_LDays_ddt as
select min(a.PostingDate) GRDate,
	min(b.v_DeliveryDate) DlvrDt,
	a.Document_No,
	a.DocumentLine_No,
	a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo,
	min(a.PostDateSeqNo) PostDateSeqNo
from fact_materialmovement_vendperform a
	inner join fact_purchase_tmp_709_01 b
		on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
where a.MovementType = 101
	AND a.PostDateSeqNo > (b.DlvrDateSeqNo + b.pLateToleranceDays)
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo;

drop table if exists fact_purchase_tmp_709_EDays_ddt;
create table fact_purchase_tmp_709_EDays_ddt as
select max(a.PostingDate) GRDate,
	min(b.v_DeliveryDate) DlvrDt,
	a.Document_No,
	a.DocumentLine_No,
	a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo,
	max(a.PostDateSeqNo) PostDateSeqNo
from fact_materialmovement_vendperform a
	inner join fact_purchase_tmp_709_01 b
		on a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
where a.MovementType = 101
	AND a.PostDateSeqNo < (b.DlvrDateSeqNo - b.pEarlyToleranceDays)
group by a.Document_No,a.DocumentLine_No,a.dd_scheduleno,
	b.DlvrDateSeqNo,
	b.StatDateSeqNo;

drop table if exists fact_purchase_tmp_709_calc_X;
create table fact_purchase_tmp_709_calc_X as
select iid,
	v_fact_vendperfid,
        v_dd_DocumentNo,
        v_dd_DocumentItemNo,
        v_dd_ScheduleNo,
        v_ct_DeliveryQty,
        v_ct_ReceivedQty,
        v_DeliveryDate,
        v_StatDeliveryDate,
	case when vQtyEarly < 0 then 0 when vQtyEarly > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyEarly end vQtyEarly,
	(case when vQtyOnTime < 0 then 0 when vQtyOnTime > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyOnTime end)
		- (case when vQtyEarly < 0 then 0 when vQtyEarly > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyEarly end) vQtyOnTime,
	vQtyLate,
	vLateDlvrDays, 
	IFNULL((Select (a.StatDateSeqNo - a.PostDateSeqNo)
		From fact_purchase_tmp_709_EDays a
		Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
			and vQtyEarly > 0),0) vEarlyDlvrDays,
	case when vQtyEarly_ddt < 0 then 0 when vQtyEarly_ddt > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyEarly_ddt end vQtyEarly_ddt,
	(case when vQtyOnTime_ddt < 0 then 0 when vQtyOnTime_ddt > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyOnTime_ddt end)
		- (case when vQtyEarly_ddt < 0 then 0 when vQtyEarly_ddt > v_ct_ReceivedQty then v_ct_ReceivedQty else vQtyEarly_ddt end) vQtyOnTime_ddt,
	vQtyLate_ddt,
	vLateDlvrDays_ddt, 
	IFNULL((Select (a.DlvrDateSeqNo - a.PostDateSeqNo)
		From fact_purchase_tmp_709_EDays_ddt a
		Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
			and vQtyEarly_ddt > 0),0) vEarlyDlvrDays_ddt,
	pLateToleranceDays,
	pEarlyToleranceDays,
	vPastDueCutOffDays,
        v_CompanyCode,
	DlvrDateSeqNo,
	StatDateSeqNo,
	ifnull((select case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end 
	 from tmp_dim_date_001 dd0 where dd0.DateValue = CURRENT_DATE and dd0.CompanyCode = v_CompanyCode), 0) CurrDateSeqNo
from fact_purchase_tmp_709_01 b;


drop table if exists fact_purchase_tmp_709_calc_Y;
create table fact_purchase_tmp_709_calc_Y as
select iid,
	v_fact_vendperfid,
        v_dd_DocumentNo,
        v_dd_DocumentItemNo,
        v_dd_ScheduleNo,
        v_ct_DeliveryQty,
        v_ct_ReceivedQty,
        v_DeliveryDate,
        v_StatDeliveryDate,
	vQtyEarly,
	vQtyOnTime,
	case when StatDateSeqNo < (CurrDateSeqNo - pLateToleranceDays) then v_ct_DeliveryQty - (vQtyEarly+vQtyOnTime) 
	     else 0
	end vQtyLate,
	IFNULL((Select (a.PostDateSeqNo - a.StatDateSeqNo)
		From fact_purchase_tmp_709_LDays a
		Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
		and StatDateSeqNo < (CurrDateSeqNo - pLateToleranceDays) and (v_ct_DeliveryQty - (vQtyEarly+vQtyOnTime)) > 0),0) vLateDlvrDays, 
	vEarlyDlvrDays,
	vQtyEarly_ddt,
	vQtyOnTime_ddt,
	case when DlvrDateSeqNo < (CurrDateSeqNo - pLateToleranceDays) then v_ct_DeliveryQty - (vQtyEarly+vQtyOnTime) 
	     else 0
	end vQtyLate_ddt,
	IFNULL((Select (a.PostDateSeqNo - a.DlvrDateSeqNo)
		From fact_purchase_tmp_709_LDays_ddt a
		Where a.Document_No = b.v_dd_DocumentNo and a.DocumentLine_No = b.v_dd_DocumentItemNo and a.dd_scheduleno = b.v_dd_ScheduleNo
		and DlvrDateSeqNo < (CurrDateSeqNo - pLateToleranceDays) and (v_ct_DeliveryQty - (vQtyEarly+vQtyOnTime)) > 0),0) vLateDlvrDays_ddt, 
	vEarlyDlvrDays_ddt,
	pLateToleranceDays,
	pEarlyToleranceDays,
	vPastDueCutOffDays,
	DlvrDateSeqNo,
	StatDateSeqNo,
	ifnull(CurrDateSeqNo, 0)
from fact_purchase_tmp_709_calc_X b;

drop table if exists fact_purchase_tmp_709;
create table fact_purchase_tmp_709 as
select a.iid,
	a.v_fact_vendperfid,
        a.v_dd_DocumentNo,
        a.v_dd_DocumentItemNo,
        a.v_dd_ScheduleNo,
        a.v_ct_DeliveryQty,
        a.v_ct_ReceivedQty,
        a.v_DeliveryDate,
        a.v_StatDeliveryDate,
        a.v_DlvrComplete,
        a.v_GRIndicator,
        a.v_CompanyCode, 	
	b.vQtyEarly,
	b.vQtyOnTime,
	b.vQtyLate,
	b.vLateDlvrDays, 
	b.vEarlyDlvrDays,
	b.vQtyEarly_ddt,
	b.vQtyOnTime_ddt,
	b.vQtyLate_ddt,
	b.vLateDlvrDays_ddt, 
	b.vEarlyDlvrDays_ddt,
	a.vQtyReturn,
	a.v_LastGRDate,
	a.v_FirstGRDate,
	a.v_tmp_RcvdDlvCount,
	a.vInvoiceQty,
	a.vPOLineQty,
	a.pLateToleranceDays,
	a.pEarlyToleranceDays,
	a.vPastDueCutOffDays,
	a.pflag,
	a.DlvrDateSeqNo,
	a.StatDateSeqNo,
	ifnull((select case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end 
	 from tmp_dim_date_001 dd0 where dd0.DateValue = CURRENT_DATE and dd0.CompanyCode = v_CompanyCode), 0) CurrDateSeqNo
from fact_purchase_tmp_709_01 a
	inner join fact_purchase_tmp_709_calc_Y b on a.iid = b.iid;

insert into fact_purchase_tmp_709 
select a.iid,
	a.v_fact_vendperfid,
        a.v_dd_DocumentNo,
        a.v_dd_DocumentItemNo,
        a.v_dd_ScheduleNo,
        a.v_ct_DeliveryQty,
        a.v_ct_ReceivedQty,
        a.v_DeliveryDate,
        a.v_StatDeliveryDate,
        a.v_DlvrComplete,
        a.v_GRIndicator,
        a.v_CompanyCode, 	
	a.vQtyEarly,
	a.vQtyOnTime,
	a.vQtyLate,
	a.vLateDlvrDays, 
	a.vEarlyDlvrDays,
	a.vQtyEarly_ddt,
	a.vQtyOnTime_ddt,
	a.vQtyLate_ddt,
	a.vLateDlvrDays_ddt, 
	a.vEarlyDlvrDays_ddt,
	a.vQtyReturn,
	a.v_LastGRDate,
	a.v_FirstGRDate,
	a.v_tmp_RcvdDlvCount,
	a.vInvoiceQty,
	a.vPOLineQty,
	a.pLateToleranceDays,
	a.pEarlyToleranceDays,
	a.vPastDueCutOffDays,
	a.pflag,
	a.DlvrDateSeqNo,
	a.StatDateSeqNo,
	ifnull((select case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end 
	 from tmp_dim_date_001 dd0 where dd0.DateValue = CURRENT_DATE and dd0.CompanyCode = v_CompanyCode), 0) CurrDateSeqNo
from fact_purch_tmp_709_fl_01 a;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_purchase_tmp_709 ;


/*** final fact table update ***/

UPDATE fact_purchase
From fact_purchase_tmp_709
SET fact_purchase.ct_EarlyQty = ifnull(fact_purchase_tmp_709.vQtyEarly, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET fact_purchase.ct_LateQty = ifnull(fact_purchase_tmp_709.vQtyLate, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;


UPDATE fact_purchase
From fact_purchase_tmp_709
SET fact_purchase.ct_AcceptQty = ifnull( (fact_purchase_tmp_709.vQtyOnTime + fact_purchase_tmp_709.vQtyEarly), 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_OnTimeQty = ifnull(fact_purchase_tmp_709.vQtyOnTime, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_ReturnQty = ifnull(fact_purchase_tmp_709.vQtyReturn, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;


UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_OTD = ifnull( ((fact_purchase_tmp_709.v_ct_DeliveryQty
               - (fact_purchase_tmp_709.vQtyEarly
                  + fact_purchase_tmp_709.vQtyLate))
              / (case when fact_purchase_tmp_709.v_ct_DeliveryQty = 0 then 1 else fact_purchase_tmp_709.v_ct_DeliveryQty end)), 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_LeadTimeQty = ifnull( ((fact_purchase_tmp_709.vQtyOnTime
               + fact_purchase_tmp_709.vQtyEarly)
              / (case when fact_purchase_tmp_709.v_ct_ReceivedQty=0 then 1 else fact_purchase_tmp_709.v_ct_ReceivedQty end )), 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_latedeliverydays = ifnull(fact_purchase_tmp_709.vLateDlvrDays, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_earlydeliverydays = ifnull(fact_purchase_tmp_709.vEarlyDlvrDays, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_EarlyQty_ddt = ifnull(fact_purchase_tmp_709.vQtyEarly_ddt, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_LateQty_ddt = ifnull(fact_purchase_tmp_709.vQtyLate_ddt, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_AcceptQty_ddt = ifnull( (fact_purchase_tmp_709.vQtyOnTime_ddt + fact_purchase_tmp_709.vQtyEarly_ddt), 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_OnTimeQty_ddt = ifnull(fact_purchase_tmp_709.vQtyOnTime_ddt, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_OTD_ddt = ifnull(
             ((fact_purchase_tmp_709.v_ct_DeliveryQty
               - (fact_purchase_tmp_709.vQtyEarly_ddt
                  + fact_purchase_tmp_709.vQtyLate_ddt))
              / ( case when fact_purchase_tmp_709.v_ct_DeliveryQty = 0 then 1 else fact_purchase_tmp_709.v_ct_DeliveryQty  end)), 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_LeadTimeQty_ddt = ifnull(
             ((fact_purchase_tmp_709.vQtyOnTime_ddt
               + fact_purchase_tmp_709.vQtyEarly_ddt)
              / (case when fact_purchase_tmp_709.v_ct_ReceivedQty = 0 then 1 else  fact_purchase_tmp_709.v_ct_ReceivedQty end) ), 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_latedeliverydays_ddt = ifnull(fact_purchase_tmp_709.vLateDlvrDays_ddt, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.ct_earlydeliverydays_ddt = ifnull(fact_purchase_tmp_709.vEarlyDlvrDays_ddt, 0)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;


UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.Dim_DateidLastGR = 1
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.Dim_DateidLastGR = IFNULL((SELECT dt.Dim_Dateid
                          FROM dim_date dt
                          WHERE dt.DateValue = fact_purchase_tmp_709.v_LastGRDate
                                AND dt.CompanyCode = fact_purchase_tmp_709.v_CompanyCode),1)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid
AND  fact_purchase_tmp_709.v_ct_ReceivedQty > 0 AND fact_purchase_tmp_709.v_LastGRDate is not null ;


UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.Dim_DateIdFirstGR = 1
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.Dim_DateIdFirstGR = IFNULL((SELECT dt.Dim_Dateid
                          FROM dim_date dt
                          WHERE dt.DateValue = fact_purchase_tmp_709.v_FirstGRDate
                                AND dt.CompanyCode = fact_purchase_tmp_709.v_CompanyCode),1)
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid
AND fact_purchase_tmp_709.v_ct_ReceivedQty > 0 AND fact_purchase_tmp_709.v_FirstGRDate is not null ;


UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.Dim_GRStatusid =
          CASE
             WHEN fact_purchase_tmp_709.v_ct_ReceivedQty > 0 AND fact_purchase_tmp_709.v_LastGRDate is not null
             THEN
                CASE
                   WHEN vLateDlvrDays > 0
                   THEN 3
                   WHEN vEarlyDlvrDays > 0
		   THEN 4
                   ELSE 5
                END
             ELSE
                CASE
                   WHEN StatDateSeqNo < (CurrDateSeqNo - fact_purchase_tmp_709.pLateToleranceDays )
                   THEN 3
                   ELSE 2
                END
          END
WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

UPDATE fact_purchase
From fact_purchase_tmp_709
SET    fact_purchase.dd_InvStatus =
          CASE
             WHEN (fact_purchase_tmp_709.vInvoiceQty > 0
                   AND fact_purchase_tmp_709.vInvoiceQty <>
                          fact_purchase_tmp_709.vPOLineQty)
             THEN 'Partial'
             WHEN (fact_purchase_tmp_709.vInvoiceQty > 0
                   AND fact_purchase_tmp_709.vInvoiceQty =
                          fact_purchase_tmp_709.vPOLineQty)
             THEN 'Full'
             ELSE fact_purchase.dd_InvStatus
          END
 WHERE fact_purchase.fact_purchaseid = fact_purchase_tmp_709.v_fact_vendperfid;

 /** OTIF Calc **/

UPDATE fact_purchase f
SET    f.dd_QtyInFull = 'Not Set'
WHERE (f.dd_QtyInFull <> 'Not Set' OR f.dd_QtyInFull IS NULL);

UPDATE fact_purchase f
SET    f.dd_OnTime_Delv = 'Not Set'
WHERE (f.dd_OnTime_Delv <> 'Not Set'  OR f.dd_OnTime_Delv IS NULL);

UPDATE fact_purchase f
SET    f.dd_OnTime_Stat = 'Not Set'
WHERE (f.dd_OnTime_Stat <> 'Not Set' OR f.dd_OnTime_Stat IS NULL);

drop table if exists fact_purch_lineqty_001;
create table fact_purch_lineqty_001 as
select f1.fact_purchaseid,
	f1.ct_DeliveryQty, 
	f1.ct_ReceivedQty,
	f1.dd_DocumentNo, 
	f1.dd_DocumentItemNo, 
	f1.dd_ScheduleNo,
	m.UnlimitedOverDelivery,
	m.ItemDeliveryComplete,
	f1.ct_OverDeliveryTolerance,
	f1.ct_UnderDeliveryTolerance,
	f1.ct_OnTimeQty_ddt,
	f1.ct_OnTimeQty ct_OnTimeQty_stat,
	 (select case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end from tmp_dim_date_001 dd0 where dd0.Dim_Dateid = dd.Dim_Dateid) DlvrDateSeqNo,
	 (select case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end from tmp_dim_date_001 dd0 where dd0.Dim_Dateid = sd.Dim_Dateid) StatDateSeqNo,
	ifnull((select case when dd0.DateSeqNo = 0 then dd0.MaxDateSeqNo else dd0.DateSeqNo end 
		from tmp_dim_date_001 dd0 where dd0.DateValue = CURRENT_DATE and dd0.CompanyCode = dd.CompanyCode), 0) CurrDateSeqNo,
	ifnull((SELECT property_value
		 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.late'), 1) pLateToleranceDays,
	ifnull((SELECT property_value
		 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.early'), 1) pEarlyToleranceDays,
	row_number() over(partition by f1.dd_DocumentNo, f1.dd_DocumentItemNo order by dd.DateValue, f1.dd_ScheduleNo) LineDlvrSeq
from fact_purchase f1 
	inner join dim_purchasemisc m on m.Dim_PurchaseMiscid = f1.Dim_PurchaseMiscid
	inner join Dim_Date sd on sd.Dim_Dateid = f1.Dim_DateidStatDelivery
	inner join Dim_Date dd on dd.Dim_Dateid = f1.Dim_DateidDelivery
where m.ItemGRIndicator = 'X'
            AND (f1.dd_QtyInFull = 'Not Set' 
		 OR f1.dd_OnTime_Delv = 'Not Set' 
		 OR f1.dd_OnTime_Stat = 'Not Set');

drop table if exists fact_purch_lineqty_002;
create table fact_purch_lineqty_002 as
select sum(f1.ct_DeliveryQty) LineDelvQty, sum(f1.ct_ReceivedQty) LineRcvdQty,
	max(f.LineDlvrSeq) MaxLineDlvrSeq,
	f1.dd_DocumentNo f1_DocumentNo, f1.dd_DocumentItemNo f1_DocumentItemNo
from fact_purchase f1 
	inner join fact_purch_lineqty_001 f on f1.dd_DocumentNo = f.dd_DocumentNo
						and f1.dd_DocumentItemNo = f.dd_DocumentItemNo
						and f1.dd_ScheduleNo = f.dd_ScheduleNo
group by f1.dd_DocumentNo, f1.dd_DocumentItemNo;

drop table if exists fact_purch_otif_001;
create table fact_purch_otif_001 as
select f1.fact_purchaseid,
	case when (lq.LineRcvdQty between (lq.LineDelvQty - (lq.LineDelvQty * f1.ct_UnderDeliveryTolerance / 100)) and (lq.LineDelvQty + (lq.LineDelvQty * f1.ct_UnderDeliveryTolerance / 100))
				or f1.UnlimitedOverDelivery = 'X' and lq.LineRcvdQty >= (lq.LineDelvQty - (lq.LineDelvQty * f1.ct_UnderDeliveryTolerance / 100)))
			and f1.ct_ReceivedQty > 0
		then 'Y'
		when f1.ct_ReceivedQty >= f1.ct_DeliveryQty 
		then 'Y'
		else 'N'
	end dd_QtyInFull,
	case when f1.DlvrDateSeqNo <= (f1.CurrDateSeqNo + f1.pEarlyToleranceDays)
		then case when f1.ct_ReceivedQty > 0 and f1.ct_OnTimeQty_ddt = f1.ct_ReceivedQty
			then 'Y'
			else 'N'
		     end
		when f1.ct_ReceivedQty > 0
		then 'N'
		else 'Not Set'
	end dd_OnTime_Delv,
	case when f1.StatDateSeqNo <= (f1.CurrDateSeqNo + f1.pEarlyToleranceDays)
		then case when f1.ct_ReceivedQty > 0 and f1.ct_OnTimeQty_stat = f1.ct_ReceivedQty
			then 'Y'
			else 'N'
		     end
		when f1.ct_ReceivedQty > 0
		then 'N'
		else 'Not Set'
	end dd_OnTime_Stat
from fact_purch_lineqty_001 f1 
	inner join fact_purch_lineqty_002 lq on f1.dd_DocumentNo = lq.f1_DocumentNo 
						and f1.dd_DocumentItemNo = lq.f1_DocumentItemNo;

UPDATE fact_purchase f
From fact_purch_otif_001 otf
SET    f.dd_QtyInFull = otf.dd_QtyInFull
WHERE f.fact_purchaseid = otf.fact_purchaseid
	and f.dd_QtyInFull <> otf.dd_QtyInFull;

UPDATE fact_purchase f
From fact_purch_otif_001 otf
SET    f.dd_OnTime_Delv = otf.dd_OnTime_Delv
WHERE f.fact_purchaseid = otf.fact_purchaseid
	and f.dd_OnTime_Delv <> otf.dd_OnTime_Delv;

UPDATE fact_purchase f
From fact_purch_otif_001 otf
SET    f.dd_OnTime_Stat = otf.dd_OnTime_Stat
WHERE f.fact_purchaseid = otf.fact_purchaseid
	and f.dd_OnTime_Stat <> otf.dd_OnTime_Stat;

call vectorwise(combine 'fact_purchase');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_purchase;

INSERT INTO processinglog (processinglogid,referencename, startdate, description) 
VALUES ((select ifnull(max(processinglogid),0) + 1 from processinglog),
	'bi_purchase_vendorperformance_processing',
	TIMESTAMP_WO_TZ(current_timestamp),
	'bi_purchase_vendorperformance_processing END');


Drop table if exists fact_purchase_tmp_709;
Drop table if exists doc_tmp_709;
Drop table if exists mainholder_709;
Drop table if exists holder_709;
Drop table if exists fact_purch_tmp_709_fl_01;
drop table if exists fact_purch_invqty_001;
drop table if exists fact_purch_lineqty_001;
drop table if exists fact_purch_lineqty_002;
drop table if exists tmp_dim_date_001;
drop table if exists tmp_dimdt_holiday_001;
drop table if exists tmp_dimdt_holiday_002;
drop table if exists fact_purchase_tmp_709_LDays;
drop table if exists fact_purchase_tmp_709_EDays;
drop table if exists fact_purchase_tmp_709_LDays_ddt;
drop table if exists fact_purchase_tmp_709_EDays_ddt;
drop table if exists fact_purch_otif_001;
