/*   Date             By        Version             	 Desc                                                             */
/*  19 Aug 2015    Cristina		 1.1               		Add Grid Value and Season Info                                    */

UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmsohierarchytype = ifnull(vbk.VBAK_J_3AHITYP,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmsohierarchytype <> ifnull(vbk.VBAK_J_3AHITYP,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsordersubsflag = ifnull(vbk.VBAK_J_3ASUBS,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsordersubsflag <> ifnull(vbk.VBAK_J_3ASUBS,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsorderduplflag = ifnull(vbk.VBAK_J_3ADUPK,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsorderduplflag <> ifnull(vbk.VBAK_J_3ADUPK,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsidentcust = ifnull(vbk.VBAK_J_3ASTCU,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsidentcust <> ifnull(vbk.VBAK_J_3ASTCU,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsrddqualifier = ifnull(vbk.VBAK_J_3ARQLF,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsrddqualifier <> ifnull(vbk.VBAK_J_3ARQLF,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl, dim_date dd 
SET sof.dim_afsdateidadditional = dd.dim_dateid
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
  AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
  AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
  AND vbk.VBAK_J_3ADDAT IS NOT NULL
  AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
  AND dd.DateValue = vbk.VBAK_J_3ADDAT 
  AND dd.CompanyCode = pl.CompanyCode;

UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsdqualifier = ifnull(vbk.VBAK_J_3ADQLF,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsdqualifier <> ifnull(vbk.VBAK_J_3ADQLF,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afscanceldqualifier = ifnull(vbk.VBAK_J_3ACQLF,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afscanceldqualifier <> ifnull(vbk.VBAK_J_3ACQLF,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsorderstrategy = ifnull(vbk.VBAK_J_3AOSLM,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsorderstrategy <> ifnull(vbk.VBAK_J_3AOSLM,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmsostore = ifnull(vbk.VBAK_J_3AVGSTO,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmsostore <> ifnull(vbk.VBAK_J_3AVGSTO,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmsodocno = ifnull(vbk.VBAK_J_3AVMSOB,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmsodocno <> ifnull(vbk.VBAK_J_3AVMSOB,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmsodistrrule = ifnull(vbk.VBAK_AFS_MSOSR,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmsodistrrule <> ifnull(vbk.VBAK_AFS_MSOSR,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsassortmentformat = ifnull(vbk.VBAK_J_3AKVGR6,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsassortmentformat <> ifnull(vbk.VBAK_J_3AKVGR6,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsfreight = ifnull(vbk.VBAK_J_3AKVGR7,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsfreight <> ifnull(vbk.VBAK_J_3AKVGR7,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afspackslip = ifnull(vbk.VBAK_J_3AKVGR8,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afspackslip <> ifnull(vbk.VBAK_J_3AKVGR8,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afscustgroup9 = ifnull(vbk.VBAK_J_3AKVGR9,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afscustgroup9 <> ifnull(vbk.VBAK_J_3AKVGR9,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afscustgroup10 = ifnull(vbk.VBAK_J_3AKVGR10,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afscustgroup10 <> ifnull(vbk.VBAK_J_3AKVGR10,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsitemrefind = ifnull(vbk.VBAP_J_3ABZUS,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsitemrefind <> ifnull(vbk.VBAP_J_3ABZUS,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.ct_afstargetqty = ifnull(vbk.VBAP_J_3ATQTY, 0.0000) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.ct_afstargetqty <> ifnull(vbk.VBAP_J_3ATQTY, 0.0000) ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsitemcdqualifier = ifnull(vbk.VBAP_J_3ACQLF,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsitemcdqualifier <> ifnull(vbk.VBAP_J_3ACQLF,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsitemrddqualifier = ifnull(vbk.VBAP_J_3ARQLF,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsitemrddqualifier <> ifnull(vbk.VBAP_J_3ARQLF,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsitemcounter = ifnull(vbk.VBAP_J_3AMSOFA, 0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsitemcounter <> ifnull(vbk.VBAP_J_3AMSOFA, 0) ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmsomix = ifnull(vbk.VBAP_J_3AMSOM,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmsomix <> ifnull(vbk.VBAP_J_3AMSOM,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afspriceindicator = ifnull(vbk.VBAP_J_3AMSPI,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afspriceindicator <> ifnull(vbk.VBAP_J_3AMSPI,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsallocstratg = ifnull(vbk.VBAP_J_3AMSAR,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsallocstratg <> ifnull(vbk.VBAP_J_3AMSAR,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmatpriceindicator = ifnull(vbk.VBAP_J_3APIND,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmatpriceindicator <> ifnull(vbk.VBAP_J_3APIND,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afssplitindicator = ifnull(vbk.VBAP_J_3ASPLIT,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afssplitindicator <> ifnull(vbk.VBAP_J_3ASPLIT,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmatgridno = ifnull(vbk.VBAP_J_3APGNR,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmatgridno <> ifnull(vbk.VBAP_J_3APGNR,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl, dim_date dd
SET sof.dd_afsdateidmatgridvalidfrom = dd.dim_dateid
WHERE vbk.VBAK_VBELN = sof.dd_SalesDocNo
  AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
  AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
  AND vbk.VBAP_J_3AVDAT IS NOT NULL
  AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
  AND dd.DateValue = vbk.VBAP_J_3AVDAT 
  AND dd.CompanyCode = pl.CompanyCode; 
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afscondrecno = ifnull(vbk.VBAP_J_3AKNUMH,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afscondrecno <> ifnull(vbk.VBAP_J_3AKNUMH,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsvasmatgroup = ifnull(vbk.VBAP_J_3AVASG,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsvasmatgroup <> ifnull(vbk.VBAP_J_3AVASG,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsgroupindicator = ifnull(vbk.VBAP_J_3AVASGP,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsgroupindicator <> ifnull(vbk.VBAP_J_3AVASGP,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsitemvastype = ifnull(vbk.VBAP_J_3AVASIT,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsitemvastype <> ifnull(vbk.VBAP_J_3AVASIT,'Not Set');
   
/* AFS Season Info */   

/*
UPDATE fact_salesorderlife sof FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsitemseasonind = ifnull(vbk.VBAP_J_3ASEAN,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsitemseasonind <> ifnull(vbk.VBAP_J_3ASEAN,'Not Set');
   
UPDATE fact_salesorderlife sof FROM VBAK_VBAP_VBEP vbk, dim_plant pl
SET sof.dd_afsitemcollection = ifnull(vbk.VBAP_AFS_COLLECTION,'Not Set')
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsitemcollection <> ifnull(vbk.VBAP_AFS_COLLECTION,'Not Set');
   
UPDATE fact_salesorderlife sof FROM VBAK_VBAP_VBEP vbk, dim_plant pl
SET sof.dd_afsitemtheme = ifnull(vbk.VBAP_AFS_THEME,'Not Set')
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsitemtheme <> ifnull(vbk.VBAP_AFS_THEME,'Not Set');   
   
UPDATE fact_salesorderlife sof FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsscheduleseasonind = ifnull(vbk.VBEP_J_3SEANS,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsscheduleseasonind <> ifnull(vbk.VBEP_J_3SEANS,'Not Set');   
   
*/   
   
/* AFS Grid Value Info */

/*
UPDATE fact_salesorderlife sof FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsschedulegridvalue = ifnull(vbk.VBEP_J_3ASIZE,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsschedulegridvalue <> ifnull(vbk.VBEP_J_3ASIZE,'Not Set');    
 
*/
 
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afscustcoordcode = ifnull(vbk.VBAP_J_3ACUCO,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afscustcoordcode <> ifnull(vbk.VBAP_J_3ACUCO,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afscumulationind = ifnull(vbk.VBAP_J_3AKOMM,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afscumulationind <> ifnull(vbk.VBAP_J_3AKOMM,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmso2itemno = ifnull(vbk.VBAP_J_3AVMSOP,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmso2itemno <> ifnull(vbk.VBAP_J_3AVMSOP,0);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsbomcorrelation = ifnull(vbk.VBAP_AFS_BOMCORR,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsbomcorrelation <> ifnull(vbk.VBAP_AFS_BOMCORR,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsqtydistrprofile = ifnull(vbk.VBAP_AFS_PRPKPROF,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsqtydistrprofile <> ifnull(vbk.VBAP_AFS_PRPKPROF,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsprofilefactor = ifnull(vbk.VBAP_AFS_PRPKFACT,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsprofilefactor <> ifnull(vbk.VBAP_AFS_PRPKFACT,0);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afssubsrule = ifnull(vbk.VBAP_AFS_SUBRU,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afssubsrule <> ifnull(vbk.VBAP_AFS_SUBRU,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_AfsitemreqmtCategory = ifnull(vbk.VBAP_J_4KRCAT,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_AfsitemreqmtCategory <> ifnull(vbk.VBAP_J_4KRCAT,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsalescheduleline = ifnull(vbk.VBEP_AFS_ALE_ID,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsalescheduleline <> ifnull(vbk.VBEP_AFS_ALE_ID,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsallocationrun = ifnull(vbk.VBEP_AFS_AGTAB,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsallocationrun <> ifnull(vbk.VBEP_AFS_AGTAB,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsallocprio = ifnull(vbk.VBEP_AFS_ARPRI,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsallocprio <> ifnull(vbk.VBEP_AFS_ARPRI,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsinputgridvalue = ifnull(vbk.VBEP_AFS_SZIN,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsinputgridvalue <> ifnull(vbk.VBEP_AFS_SZIN,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsentryconvtype = ifnull(vbk.VBEP_AFS_CTIN,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsentryconvtype <> ifnull(vbk.VBEP_AFS_CTIN,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsphysinvblk = ifnull(vbk.VBEP_J_3ABLIC,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsphysinvblk <> ifnull(vbk.VBEP_J_3ABLIC,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsschedulerefind = ifnull(vbk.VBEP_J_3ABZUS,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsschedulerefind <> ifnull(vbk.VBEP_J_3ABZUS,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsschedulebatch = ifnull(vbk.VBEP_J_3ACHARG,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsschedulebatch <> ifnull(vbk.VBEP_J_3ACHARG,'Not Set');
   
UPDATE fact_salesorderlife sof
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsoslgroup = ifnull(vbk.VBEP_J_3ALNGR,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsoslgroup <> ifnull(vbk.VBEP_J_3ALNGR,0);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmsomixfactor = ifnull(vbk.VBEP_J_3AFACT,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmsomixfactor <> ifnull(vbk.VBEP_J_3AFACT,0) ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsscheduleref = ifnull(vbk.VBEP_J_3ARETEN,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsscheduleref <> ifnull(vbk.VBEP_J_3ARETEN,0);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afspoitem = ifnull(vbk.VBEP_J_3APOSEX,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afspoitem <> ifnull(vbk.VBEP_J_3APOSEX,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afspreceedscheduleno = ifnull(vbk.VBEP_J_3AETENV,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afspreceedscheduleno <> ifnull(vbk.VBEP_J_3AETENV,0);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsschedrefsched = ifnull(vbk.VBEP_J_3AVGETE,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsschedrefsched <> ifnull(vbk.VBEP_J_3AVGETE,0);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsheartsize = ifnull(vbk.VBEP_J_3AHSIZ,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsheartsize <> ifnull(vbk.VBEP_J_3AHSIZ,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsmso2scheduleno = ifnull(vbk.VBEP_J_3AVMSOE,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsmso2scheduleno <> ifnull(vbk.VBEP_J_3AVMSOE,0);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afscomplrefind = ifnull(vbk.VBEP_J_3AVOREF,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afscomplrefind <> ifnull(vbk.VBEP_J_3AVOREF,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsprecedref = ifnull(vbk.VBEP_J_3AVGREF,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsprecedref <> ifnull(vbk.VBEP_J_3AVGREF,'Not Set') );
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afscompletionrule = ifnull(vbk.VBEP_J_3AERLRE,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afscompletionrule <> ifnull(vbk.VBEP_J_3AERLRE,'Not Set') ;
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.amt_afsgrossweight = ifnull(vbk.VBEP_AFS_BRGEW,0.0000) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.amt_afsgrossweight <> ifnull(vbk.VBEP_AFS_BRGEW,0.0000);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.amt_afsnetweight = ifnull(vbk.VBEP_AFS_NTGEW,0.0000) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.amt_afsnetweight <> ifnull(vbk.VBEP_AFS_NTGEW,0.0000);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.amt_afsvolume = ifnull(vbk.VBEP_AFS_VOLUM,0.0000) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.amt_afsvolume <> ifnull(vbk.VBEP_AFS_VOLUM,0.0000);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsorigscheduleno = ifnull(vbk.VBEP_AFS_UETENR,0) 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsorigscheduleno <> ifnull(vbk.VBEP_AFS_UETENR,0);
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afssubsscheduleno = ifnull(vbk.VBEP_AFS_SUBFL,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afssubsscheduleno <> ifnull(vbk.VBEP_AFS_SUBFL,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsstatistvalues = ifnull(vbk.VBEP_AFS_KOWRR,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsstatistvalues <> ifnull(vbk.VBEP_AFS_KOWRR,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsgridvaluegrp = ifnull(vbk.VBEP_J_3ASZGR,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsgridvaluegrp <> ifnull(vbk.VBEP_J_3ASZGR,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afstaxclassification = ifnull(vbk.VBEP_J_3ATAXKM,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afstaxclassification <> ifnull(vbk.VBEP_J_3ATAXKM,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afspricing = ifnull(vbk.VBEP_J_3APROK,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afspricing <> ifnull(vbk.VBEP_J_3APROK,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsschedulevastype = ifnull(vbk.VBEP_J_3AVASIT,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsschedulevastype = ifnull(vbk.VBEP_J_3AVASIT,'Not Set');
   
UPDATE fact_salesorderlife sof 
FROM VBAK_VBAP_VBEP vbk, dim_plant pl 
SET sof.dd_afsschedulereqcateg = ifnull(vbk.VBEP_AFS_RCATWA,'Not Set') 
WHERE  vbk.VBAK_VBELN = sof.dd_SalesDocNo
   AND vbk.VBAP_POSNR = sof.dd_SalesItemNo
   AND vbk.VBEP_ETENR = sof.dd_ScheduleNo
   AND pl.plantcode = vbk.VBAP_WERKS AND pl.rowiscurrent = 1 
   AND sof.dd_afsschedulereqcateg <> ifnull(vbk.VBEP_AFS_RCATWA,'Not Set');