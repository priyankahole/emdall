
select 'START OF PROC VW_bi_purchase_matmovement_dlvr_link',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS var_pToleranceDays;
CREATE TABLE var_pToleranceDays
(
pType  VARCHAR(10),
pToleranceDays INTEGER
);



INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('EARLY',0);
INSERT INTO var_pToleranceDays(pType,pToleranceDays) values ('LATE',0);

UPDATE var_pToleranceDays
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.early'), 0)
WHERE pType ='EARLY';
			  
UPDATE var_pToleranceDays			  
SET pToleranceDays =
       ifnull((SELECT property_value
                 FROM systemproperty
		WHERE property = 'purchasing.deliverytolerance.late'), 0)
WHERE pType ='LATE';

DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink;
CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
    SELECT f.Fact_materialmovementid,
		f.dd_MaterialDocNo,
		f.dd_MaterialDocItemNo,
		f.dd_MaterialDocYear,
		f.dd_DocumentNo,
		f.dd_DocumentItemNo,
		f.dd_DocumentScheduleNo,
		decimal(f.ct_Quantity,18,5) ct_Quantity,
		decimal(ct_QtyEntryUOM,18,5) ct_QtyEntryUOM, 
		f.Dim_MovementTypeid,
		f.dim_DateIDPostingDate,
		f.dd_debitcreditid,
		f.Dim_AfsSizeId,
		f.Fact_materialmovementid Fact_mmid_ref, 
		dp.DateValue PostingDate,
		ROW_NUMBER() OVER(PARTITION BY f.dd_DocumentNo, f.dd_DocumentItemNo, f.Dim_AfsSizeId
				  ORDER BY dp.DateValue, f.dd_MaterialDocNo, f.dd_MaterialDocItemNo, dd.DateValue, f.dd_DocumentScheduleNo) RowSeqNo
    FROM fact_materialmovement f 
        inner join dim_movementtype mt on f.Dim_MovementTypeid = mt.Dim_MovementTypeid
        inner join dim_date dp on dp.Dim_Dateid = f.dim_DateIDPostingDate
        inner join dim_date dd on dd.Dim_Dateid = f.Dim_DateIdDelivery
    WHERE mt.MovementType in (101,102,122,123,161,162) 
        and exists (select 1 from fact_materialmovement f1 
				inner join dim_movementtype mt1 on f1.Dim_MovementTypeid = mt1.Dim_MovementTypeid
                    where f.dd_DocumentNo = f1.dd_DocumentNo and f.dd_DocumentItemNo = f1.dd_DocumentItemNo
			and f.Dim_AfsSizeId = f1.Dim_AfsSizeId
                        and f1.Dim_DateIdDelivery = 1 and mt1.MovementType in (101,102,122,123,161,162))
        and exists (select 1 from fact_purchase fp 
                    where f.dd_DocumentNo = fp.dd_DocumentNo and f.dd_DocumentItemNo = fp.dd_DocumentItemNo
				and f.Dim_AfsSizeId = fp.Dim_AfsSizeId);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink;

DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink1;
CREATE TABLE fact_materialmovement_tmp_dlvrlink1 AS
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_DocumentScheduleNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       sum(ifnull(f1.ct_QtyEntryUOM,0)) - f.ct_QtyEntryUOM  CumuMMGRQty,
       f.PostingDate 
  FROM fact_materialmovement_tmp_dlvrlink f
		inner join fact_materialmovement_tmp_dlvrlink f1
  		on  (f1.dd_DocumentNo = f.dd_DocumentNo 
			and f1.dd_DocumentItemNo = f.dd_DocumentItemNo 
			and f1.Dim_AfsSizeId = f.Dim_AfsSizeId)
  WHERE f.RowSeqNo >= f1.RowSeqNo
  GROUP BY f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_DocumentScheduleNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.PostingDate ;
    
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink1;

DROP TABLE IF EXISTS tmp_fact_purch_001;
CREATE TABLE tmp_fact_purch_001 AS
select f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.ct_DeliveryQty OrderQty,
	f.ct_ReceivedQty DlvrReceivedQty,
	(f.ct_DeliveryQty - f.ct_ReceivedQty) OpenQty,
	f.Dim_DateidDelivery dim_DeliveryDate,
	dd.DateValue DeliveryDate,
	f.Dim_DateidStatDelivery dim_StatDeliveryDate,
	row_number() over (partition by f.dd_DocumentNo,f.dd_DocumentItemNo,f.Dim_AfsSizeId  
				order by dd.DateValue, f.dd_ScheduleNo) as RowSeqNum
from fact_purchase f inner join dim_date dd on dd.Dim_Dateid = f.Dim_DateidDelivery
where exists (select 1 from fact_materialmovement f1 
			inner join dim_movementtype mt1 on f1.Dim_MovementTypeid = mt1.Dim_MovementTypeid
	    where f.dd_DocumentNo = f1.dd_DocumentNo and f.dd_DocumentItemNo = f1.dd_DocumentItemNo
		and f.Dim_AfsSizeId = f1.Dim_AfsSizeId
		and f1.Dim_DateIdDelivery = 1 and mt1.MovementType in (101,102,122,123,161,162));

DROP TABLE IF EXISTS tmp_fact_purch_002;
CREATE TABLE tmp_fact_purch_002 AS
select f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.OrderQty,
	f.DlvrReceivedQty,
	f.OpenQty,
	f.dim_DeliveryDate,
	f.DeliveryDate,
	f.dim_StatDeliveryDate,
	sum(ifnull(b.DlvrReceivedQty,0)) CumuDlvrReceivedQty
from tmp_fact_purch_001 f 
	inner join tmp_fact_purch_001 b 
	on f.dd_DocumentNo = b.dd_DocumentNo and f.dd_DocumentItemNo = b.dd_DocumentItemNo and f.Dim_AfsSizeId = b.Dim_AfsSizeId
where f.RowSeqNum >= b.RowSeqNum
group by f.dd_DocumentNo,
	f.dd_DocumentItemNo,
	f.Dim_AfsSizeId,
	f.dd_ScheduleNo, 
	f.OrderQty,
	f.DlvrReceivedQty,
	f.OpenQty,
	f.dim_DeliveryDate,
	f.DeliveryDate,
	f.dim_StatDeliveryDate;


  DROP IF EXISTS fact_mm_dlvr_link_scheduleno_temp;
  CREATE TABLE fact_mm_dlvr_link_scheduleno_temp AS
	SELECT f.Fact_materialmovementid, 
		po.dd_ScheduleNo, 
		po.OrderQty,
		po.DlvrReceivedQty,
		po.OpenQty,
		po.dim_DeliveryDate,
		po.DeliveryDate,
		po.dim_StatDeliveryDate,
		po.CumuDlvrReceivedQty,
		po.CumuDlvrReceivedQty - po.DlvrReceivedQty CumuDlvrRcvdQtyPrev,
		row_number() over (partition by f.Fact_materialmovementid order by po.DeliveryDate, po.dd_ScheduleNo) as rownum
	FROM tmp_fact_purch_002 po 
		inner join fact_materialmovement_tmp_dlvrlink1 f 
			on po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and po.Dim_AfsSizeId = f.Dim_AfsSizeId
	WHERE (po.CumuDlvrReceivedQty > CumuMMGRQty)
	      or (select count(*) from fact_purchase po1 
		    where po1.dd_DocumentNo = po.dd_DocumentNo and po1.dd_DocumentItemNo = po.dd_DocumentItemNo and po1.Dim_AfsSizeId = po.Dim_AfsSizeId) = 1;
 
  
  DROP IF EXISTS fact_materialmovement_tmp_dlvrlink;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       ifnull(sn.dd_ScheduleNo, 0) ScheduleLineNo,
       ifnull(sn.OrderQty, 0) OrderQty,
       ifnull(sn.DlvrReceivedQty, 0) DlvrReceivedQty,
       ifnull(sn.OpenQty, 0) OpenQty,
       ifnull(sn.dim_DeliveryDate, 1) dim_DeliveryDate,
       ifnull(sn.dim_StatDeliveryDate, 1) dim_StatDeliveryDate,
       ifnull(sn.DeliveryDate,f.PostingDate) DeliveryDate,
       ifnull(sn.CumuDlvrReceivedQty,0) CumuDlvrReceivedQty,
       ifnull(sn.CumuDlvrRcvdQtyPrev,0) CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate 
  FROM fact_materialmovement_tmp_dlvrlink1 f 
	left join fact_mm_dlvr_link_scheduleno_temp sn on f.Fact_materialmovementid = sn.Fact_materialmovementid and sn.rownum = 1;
      
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink;
 
  
  DROP IF EXISTS fact_materialmovement_tmp_dlvrlink1;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink1 as
  SELECT distinct cast(0 as decimal(15,0)) Fact_materialmovementid,
        m.dd_MaterialDocNo,
        m.dd_MaterialDocItemNo,
        m.dd_MaterialDocYear,
        m.dd_DocumentNo,
        m.dd_DocumentItemNo,
        m.ct_Quantity,
        m.ct_QtyEntryUOM,
        m.Dim_MovementTypeid,
        m.dim_DateIDPostingDate,
        m.dd_debitcreditid,
        m.Dim_AfsSizeId,
        m.Fact_mmid_ref,
        fp.dd_ScheduleNo ScheduleLineNo,
        fp.OrderQty,
        fp.DlvrReceivedQty,
        fp.OpenQty,
        fp.dim_DeliveryDate,
        fp.dim_StatDeliveryDate,
	fp.DeliveryDate,
        fp.CumuDlvrReceivedQty,
	fp.CumuDlvrReceivedQty - fp.DlvrReceivedQty CumuDlvrRcvdQtyPrev,
        m.CumuMMGRQty,
        m.PostingDate
  FROM fact_materialmovement_tmp_dlvrlink m 
      inner join tmp_fact_purch_002 fp on m.dd_DocumentNo = fp.dd_DocumentNo and m.dd_DocumentItemNo = fp.dd_DocumentItemNo and m.Dim_AfsSizeId = fp.Dim_AfsSizeId
  where (m.CumuMMGRQty + m.ct_QtyEntryUOM) > (fp.CumuDlvrReceivedQty - fp.DlvrReceivedQty) and fp.CumuDlvrReceivedQty > m.CumuMMGRQty
        and (select count(*) from fact_purchase po1 
            where po1.dd_DocumentNo = m.dd_DocumentNo and po1.dd_DocumentItemNo = m.dd_DocumentItemNo and po1.Dim_AfsSizeId = m.Dim_AfsSizeId
                  and po1.ct_ReceivedQty > 0) > 1
        and fp.DlvrReceivedQty > 0
        and m.ScheduleLineNo <> fp.dd_ScheduleNo;

INSERT INTO fact_materialmovement_tmp_dlvrlink1
SELECT * FROM fact_materialmovement_tmp_dlvrlink;
    
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink1;

 DROP IF EXISTS fact_mm_tmp_dlvrlink_grqty;
 CREATE TABLE fact_mm_tmp_dlvrlink_grqty (Fact_materialmovementid, Fact_mmid_ref, ScheduleLineNo, QtyEntryUOM) AS
 SELECT f.Fact_materialmovementid, 
 		f.Fact_mmid_ref, 
 		f.ScheduleLineNo, 
 		case when (CumuMMGRQty + ct_QtyEntryUOM)< CumuDlvrReceivedQty then (CumuMMGRQty + ct_QtyEntryUOM - CumuDlvrRcvdQtyPrev) else f.DlvrReceivedQty end
  FROM fact_materialmovement_tmp_dlvrlink1 f
  WHERE Fact_materialmovementid = 0 
        and exists (select 1 from fact_purchase po inner join dim_date dd1 on dd1.Dim_Dateid = po.Dim_DateidDelivery
                      where po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and po.Dim_AfsSizeId = f.Dim_AfsSizeId
                          and po.ct_ReceivedQty > 0
                          and (dd1.DateValue > f.DeliveryDate or (dd1.DateValue = f.DeliveryDate and po.dd_ScheduleNo > f.ScheduleLineNo)));

  INSERT INTO fact_materialmovement_tmp_dlvrlink
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       gr.QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
	f.DeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate
  FROM fact_materialmovement_tmp_dlvrlink1 f
  		inner join fact_mm_tmp_dlvrlink_grqty gr 
  		on f.Fact_materialmovementid = gr.Fact_materialmovementid and f.Fact_mmid_ref = gr.Fact_mmid_ref and f.ScheduleLineNo = gr.ScheduleLineNo;

    
 DROP IF EXISTS fact_mm_tmp_dlvrlink_grqty;
 CREATE TABLE fact_mm_tmp_dlvrlink_grqty (Fact_materialmovementid, Fact_mmid_ref, ScheduleLineNo, QtyEntryUOM) AS
 SELECT f.Fact_materialmovementid, 
	f.Fact_mmid_ref, 
	f.ScheduleLineNo, 
	(CumuMMGRQty + ct_QtyEntryUOM) - ifnull(f.CumuDlvrRcvdQtyPrev,0)
  FROM fact_materialmovement_tmp_dlvrlink1 f
  WHERE Fact_materialmovementid = 0 
        and not exists (select 1 from fact_purchase po inner join dim_date dd1 on dd1.Dim_Dateid = po.Dim_DateidDelivery
                      where po.dd_DocumentNo = f.dd_DocumentNo and po.dd_DocumentItemNo = f.dd_DocumentItemNo and po.Dim_AfsSizeId = f.Dim_AfsSizeId
                          and po.ct_ReceivedQty > 0
                          and (dd1.DateValue > f.DeliveryDate or (dd1.DateValue = f.DeliveryDate and po.dd_ScheduleNo > f.ScheduleLineNo)));
                          

  INSERT INTO fact_materialmovement_tmp_dlvrlink
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       gr.QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
	f.DeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate
  FROM fact_materialmovement_tmp_dlvrlink1 f
  	inner join fact_mm_tmp_dlvrlink_grqty gr 
  		on f.Fact_materialmovementid = gr.Fact_materialmovementid and f.Fact_mmid_ref = gr.Fact_mmid_ref and f.ScheduleLineNo = gr.ScheduleLineNo;
  
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_dlvrlink ;

  UPDATE fact_materialmovement_tmp_dlvrlink f 
  SET ct_QtyEntryUOM = CumuDlvrReceivedQty - CumuMMGRQty        
  WHERE exists (select 1 from fact_materialmovement_tmp_dlvrlink1 m
                where m.Fact_materialmovementid = 0 and m.Fact_mmid_ref = f.Fact_materialmovementid)
        and f.Fact_materialmovementid <> 0;


  DROP IF EXISTS fact_materialmovement_tmp_dlvrlink1;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink1 AS
  SELECT f.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate 
  FROM fact_materialmovement_tmp_dlvrlink f;
 
  DROP IF EXISTS fact_materialmovement_tmp_dlvrlink;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink AS
  SELECT * FROM fact_materialmovement_tmp_dlvrlink1;
  

UPDATE fact_materialmovement f 
FROM fact_materialmovement_tmp_dlvrlink m 
    SET f.Dim_DateIdDelivery = m.dim_DeliveryDate,
        f.Dim_DateIdStatDelivery = m.dim_StatDeliveryDate,
        f.dd_DocumentScheduleNo = m.ScheduleLineNo,
        f.ct_OrderQuantity = m.OrderQty,
        f.ct_QtyEntryUOM = m.ct_QtyEntryUOM,
        f.ct_Quantity = m.ct_QtyEntryUOM,
        f.amt_LocalCurrAmt = ifnull(f.amt_StdUnitPrice * m.ct_QtyEntryUOM, 0)
WHERE f.Fact_materialmovementid = m.Fact_materialmovementid;    

 call vectorwise(combine 'fact_materialmovement');

  
  DROP IF EXISTS fact_materialmovement_tmp_dlvrlink1;
  CREATE TABLE fact_materialmovement_tmp_dlvrlink1 AS
  SELECT (select max(mm.Fact_materialmovementid) from fact_materialmovement mm)
  		+ row_number() over() Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.ct_Quantity,
       f.ct_QtyEntryUOM,
       f.Dim_MovementTypeid,
       f.dim_DateIDPostingDate,
       f.dd_debitcreditid,
       f.Dim_AfsSizeId,
       f.Fact_mmid_ref,
       f.ScheduleLineNo,
       f.OrderQty,
       f.DlvrReceivedQty,
       f.OpenQty,
       f.dim_DeliveryDate,
       f.dim_StatDeliveryDate,
       f.CumuDlvrReceivedQty,
	f.CumuDlvrRcvdQtyPrev,
       f.CumuMMGRQty,
       f.PostingDate 
  FROM  fact_materialmovement_tmp_dlvrlink f
  WHERE f.Fact_materialmovementid = 0;
  

INSERT INTO fact_materialmovement  
       (Fact_materialmovementid,
       dd_MaterialDocNo,
       dd_MaterialDocItemNo,
       dd_MaterialDocYear,
       dd_DocumentNo,
       dd_DocumentItemNo,
       dd_SalesOrderNo,
       dd_SalesOrderItemNo,
       dd_SalesOrderDlvrNo,
       dd_GLAccountNo,
       dd_ProductionOrderNumber,
       dd_ProductionOrderItemNo,
       dd_BatchNumber,
       dd_ValuationType,
       dd_debitcreditid,
       dd_GoodsMoveReason,
       amt_LocalCurrAmt,
       amt_DeliveryCost,
       amt_AltPriceControl,
       amt_OnHand_ValStock,
       ct_Quantity,
       ct_QtyEntryUOM,
       ct_QtyGROrdUnit,
       ct_QtyOnHand_ValStock,
       ct_QtyOrdPriceUnit,
       Dim_MovementTypeid,
       dim_Companyid,
       Dim_Currencyid,
       Dim_Partid,
       Dim_Plantid,
       Dim_StorageLocationid,
       Dim_Vendorid,
       dim_DateIDMaterialDocDate,
       dim_DateIDPostingDate,
       Dim_DateIDDocCreation,
       Dim_DateIDOrdered,
       dim_producthierarchyid,
       dim_MovementIndicatorid,
       dim_Customerid,
       dim_CostCenterid,
       Dim_AccountCategoryid,
       Dim_ConsumptionTypeid,
       Dim_ControllingAreaid,
       Dim_CustomerGroup1id,
       Dim_DocumentCategoryid,
       Dim_DocumentTypeid,
       Dim_ItemCategoryid,
       Dim_ItemStatusid,
       dim_productionorderstatusid,
       Dim_productionordertypeid,
       Dim_PurchaseGroupid,
       Dim_PurchaseOrgid,
       Dim_SalesDocumentTypeid,
       Dim_SalesGroupid,
       Dim_SalesOrderHeaderStatusid,
       Dim_SalesOrderItemStatusid,
       dim_salesorderrejectreasonid,
       Dim_SalesOrgid,
       dim_specialstockid,
       Dim_StockTypeid,
       Dim_UnitOfMeasureid,
       dirtyrow,
       Dim_MaterialGroupid,
       amt_ExchangeRate_GBL,
       amt_ExchangeRate,
       Dim_Termid,
       dd_ConsignmentFlag,
       Dim_ProfitCenterId,
       Dim_ReceivingPlantId,
       Dim_ReceivingPartId,
       amt_POUnitPrice,
       amt_StdUnitPrice,
       Dim_DateidCosting,
       Dim_IncoTermid,
       Dim_DateIdDelivery,
       Dim_DateIdStatDelivery,
       Dim_GRStatusid,
       dd_incoterms2,
       dd_IntOrder,
       amt_PlannedPrice,
       amt_PlannedPrice1,
       dd_DocumentScheduleNo,
       Dim_RecvIssuStorLocid,
       ct_OrderQuantity,
       Dim_POPlantidOrdering,
       Dim_POPlantidSupplying,
       Dim_POStorageLocid,
       Dim_POIssuStorageLocid,
       Dim_InspUsageDecisionId,
       ct_LotNotThruQM,
       dd_ReferenceDocNo,
       dd_ReferenceDocItem,
       LotsAcceptedFlag,
       LotsRejectedFlag,
       PercentLotsInspectedFlag,
       PercentLotsRejectedFlag,
       PercentLotsAcceptedFlag,
       LotsSkippedFlag,
       LotsAwaitingInspectionFlag,
       LotsInspectedFlag,
       QtyRejectedExternalAmt,
       QtyRejectedInternalAmt,
       Dim_AfsSizeId)
  SELECT m.Fact_materialmovementid,
       f.dd_MaterialDocNo,
       f.dd_MaterialDocItemNo,
       f.dd_MaterialDocYear,
       f.dd_DocumentNo,
       f.dd_DocumentItemNo,
       f.dd_SalesOrderNo,
       f.dd_SalesOrderItemNo,
       f.dd_SalesOrderDlvrNo,
       f.dd_GLAccountNo,
       f.dd_ProductionOrderNumber,
       f.dd_ProductionOrderItemNo,
       f.dd_BatchNumber,
       f.dd_ValuationType,
       f.dd_debitcreditid,
       f.dd_GoodsMoveReason,
       ifnull(f.amt_StdUnitPrice * m.ct_QtyEntryUOM, 0) amt_LocalCurrAmt,
       0 amt_DeliveryCost,
       0 amt_AltPriceControl,
       0 amt_OnHand_ValStock,
       m.ct_QtyEntryUOM ct_Quantity,
       m.ct_QtyEntryUOM ct_QtyEntryUOM,
       0 ct_QtyGROrdUnit,
       0 ct_QtyOnHand_ValStock,
       0 ct_QtyOrdPriceUnit,
       f.Dim_MovementTypeid,
       f.dim_Companyid,
       f.Dim_Currencyid,
       f.Dim_Partid,
       f.Dim_Plantid,
       f.Dim_StorageLocationid,
       f.Dim_Vendorid,
       f.dim_DateIDMaterialDocDate,
       f.dim_DateIDPostingDate,
       f.Dim_DateIDDocCreation,
       f.Dim_DateIDOrdered,
       f.dim_producthierarchyid,
       f.dim_MovementIndicatorid,
       f.dim_Customerid,
       f.dim_CostCenterid,
       f.Dim_AccountCategoryid,
       f.Dim_ConsumptionTypeid,
       f.Dim_ControllingAreaid,
       f.Dim_CustomerGroup1id,
       f.Dim_DocumentCategoryid,
       f.Dim_DocumentTypeid,
       f.Dim_ItemCategoryid,
       f.Dim_ItemStatusid,
       f.dim_productionorderstatusid,
       f.Dim_productionordertypeid,
       f.Dim_PurchaseGroupid,
       f.Dim_PurchaseOrgid,
       f.Dim_SalesDocumentTypeid,
       f.Dim_SalesGroupid,
       f.Dim_SalesOrderHeaderStatusid,
       f.Dim_SalesOrderItemStatusid,
       f.dim_salesorderrejectreasonid,
       f.Dim_SalesOrgid,
       f.dim_specialstockid,
       f.Dim_StockTypeid,
       f.Dim_UnitOfMeasureid,
       f.dirtyrow,
       f.Dim_MaterialGroupid,
       f.amt_ExchangeRate_GBL,
       f.amt_ExchangeRate,
       f.Dim_Termid,
       f.dd_ConsignmentFlag,
       f.Dim_ProfitCenterId,
       f.Dim_ReceivingPlantId,
       f.Dim_ReceivingPartId,
       f.amt_POUnitPrice,
       f.amt_StdUnitPrice,
       f.Dim_DateidCosting,
       f.Dim_IncoTermid,
       m.dim_DeliveryDate Dim_DateIdDelivery,
       m.dim_StatDeliveryDate Dim_DateIdStatDelivery,
       f.Dim_GRStatusid,
       f.dd_incoterms2,
       f.dd_IntOrder,
       f.amt_PlannedPrice,
       f.amt_PlannedPrice1,
       m.ScheduleLineNo dd_DocumentScheduleNo,
       f.Dim_RecvIssuStorLocid,
       m.OrderQty ct_OrderQuantity,
       f.Dim_POPlantidOrdering,
       f.Dim_POPlantidSupplying,
       f.Dim_POStorageLocid,
       f.Dim_POIssuStorageLocid,
       f.Dim_InspUsageDecisionId,
       0 ct_LotNotThruQM,
       f.dd_ReferenceDocNo,
       f.dd_ReferenceDocItem,
       f.LotsAcceptedFlag,
       f.LotsRejectedFlag,
       f.PercentLotsInspectedFlag,
       f.PercentLotsRejectedFlag,
       f.PercentLotsAcceptedFlag,
       f.LotsSkippedFlag,
       f.LotsAwaitingInspectionFlag,
       f.LotsInspectedFlag,
       f.QtyRejectedExternalAmt,
       f.QtyRejectedInternalAmt,
       f.Dim_AfsSizeId
  FROM fact_materialmovement f 
       inner join fact_materialmovement_tmp_dlvrlink1 m on f.Fact_materialmovementid = m.Fact_mmid_ref;

   call vectorwise(combine 'fact_materialmovement');
       
UPDATE fact_materialmovement f 
FROM dim_date dp, dim_date ds, var_pToleranceDays E,var_pToleranceDays L
     set f.Dim_GRStatusid = case when dp.DateValue > (ANSIDATE(ds.DateValue) + (INTERVAL '1' DAY)*L.pToleranceDays ) then 3 
                                 when dp.DateValue < (ANSIDATE(ds.DateValue) - (INTERVAL '1' DAY)*E.pToleranceDays ) then 4 
                                 else 5 end
WHERE f.Dim_DateIdStatDelivery <> 1
     and dp.Dim_Dateid = f.dim_DateIDPostingDate
     and ds.Dim_Dateid = f.Dim_DateIdStatDelivery
     and E.pType = 'EARLY'
     and L.pType = 'LATE';

   call vectorwise(combine 'fact_materialmovement');

     
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement ;

DROP TABLE IF EXISTS fact_materialmovement_tmp_dlvrlink;
DROP IF EXISTS fact_materialmovement_tmp_dlvrlink1;
DROP IF EXISTS fact_mm_tmp_dlvrlink_grqty;
DROP IF EXISTS fact_mm_dlvr_link_scheduleno_temp;
DROP TABLE IF EXISTS tmp_fact_purch_001;
DROP TABLE IF EXISTS tmp_fact_purch_002;


 select 'END OF PROC VW_bi_purchase_matmovement_dlvr_link',TIMESTAMP(LOCAL_TIMESTAMP);
