
UPDATE fact_planorder po
From    dim_plant pl ,
        dim_part prt ,
        fact_mrp fm,
        PLSC p ,
        dim_mrpelement me ,
        dim_date dt ,
        dim_date dt1 
SET po.Dim_DateidMRPSched = case when fm.dim_dateidreschedule = 1 then fm.dim_dateidoriginaldock 
                                     else fm.dim_dateidreschedule end
WHERE pl.dim_plantid = po.dim_plantid
AND prt.dim_partid = po.dim_partid
AND  fm.dd_Documentno = po.dd_PlanOrderNo
AND fm.dim_partid = prt.dim_partid
AND fm.dd_PlannScenarioLTP = po.dd_PlannScenario
AND p.PLSC_PLSCN = po.dd_PlannScenario
AND me.Dim_MRPElementID = fm.Dim_MRPElementID
AND dt.dim_dateid = fm.dim_dateidreschedule
AND dt1.dim_dateid = fm.dim_dateidoriginaldock
AND me.MRPElement <> 'U3'
AND (case when fm.dim_dateidreschedule = 1 then dt1.DateValue else dt.DateValue end) BETWEEN p.PLSC_PDAT1 AND p.PLSC_PDAT2
AND  po.Dim_DateidMRPSched <> case when fm.dim_dateidreschedule = 1 then fm.dim_dateidoriginaldock else fm.dim_dateidreschedule end;


UPDATE fact_planorder po
From    dim_plant pl ,
        dim_part prt ,
        fact_mrp fm,
        PLSC p ,
        dim_mrpelement me ,
        dim_date dt ,
        dim_date dt1
SET po.Dim_DateidMRPDock = case when fm.dim_dateidreschedule = 1 then fm.dim_dateidoriginaldock 
                                    else 1 end
WHERE pl.dim_plantid = po.dim_plantid
AND prt.dim_partid = po.dim_partid
AND  fm.dd_Documentno = po.dd_PlanOrderNo
AND fm.dim_partid = prt.dim_partid
AND fm.dd_PlannScenarioLTP = po.dd_PlannScenario
AND p.PLSC_PLSCN = po.dd_PlannScenario
AND me.Dim_MRPElementID = fm.Dim_MRPElementID
AND dt.dim_dateid = fm.dim_dateidreschedule
AND dt1.dim_dateid = fm.dim_dateidoriginaldock
AND me.MRPElement <> 'U3'
AND (case when fm.dim_dateidreschedule = 1 then dt1.DateValue else dt.DateValue end) BETWEEN p.PLSC_PDAT1 AND p.PLSC_PDAT2
AND po.Dim_DateidMRPDock <> case when fm.dim_dateidreschedule = 1 then fm.dim_dateidoriginaldock else 1 end;
        
modify fact_plord_tmp to truncated;

/* fn busines date function code */
Drop table if exists ti_p09;
Create table ti_p09 as
Select datevalue,companycode,row_number() over(partition by companycode order by datevalue asc) seq
From dim_date
where IsaPublicHoliday =0;

Drop table if exists businessdate_p09;
Create table businessdate_p09 as
Select dt.DateValue,dt.CompanyCode,-1*fm.ct_DaysGRPProcessing rp,ANSIDATE('0001-01-01') mrpdockdate,Integer(null) seqno
From   fact_planorder po
        INNER JOIN dim_plant pl ON pl.dim_plantid = po.dim_plantid
        INNER JOIN dim_part prt ON prt.dim_partid = po.dim_partid
        INNER JOIN fact_mrp fm
              ON     fm.dd_Documentno = po.dd_PlanOrderNo
                  AND fm.dim_partid = prt.dim_partid
                  AND fm.dd_PlannScenarioLTP = po.dd_PlannScenario
        INNER JOIN PLSC p ON p.PLSC_PLSCN = po.dd_PlannScenario
        INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = fm.Dim_MRPElementID
        INNER JOIN dim_date dt ON dt.dim_dateid = fm.dim_dateidreschedule
  WHERE me.MRPElement <> 'U3' AND po.Dim_DateidMRPDock = 1
        AND dt.DateValue BETWEEN p.PLSC_PDAT1 AND p.PLSC_PDAT2;

Update businessdate_p09
Set seqno=ifnull((Select seq from ti_p09 where datevalue = businessdate_p09.datevalue and companycode=businessdate_p09.companycode),0);

Update businessdate_p09
Set mrpdockdate=ifnull((Select datevalue from ti_p09 where seq=(businessdate_p09.seqno + (businessdate_p09.rp)) and companycode=businessdate_p09.companycode),ANSIDATE('0001-01-01'));
/* END busines date function code */

INSERT INTO fact_plord_tmp
select po.Fact_planorderid,dt.CompanyCode,
ifnull((Select mrpdockdate from businessdate_p09 where CompanyCode=dt.CompanyCode and rp=(-1*fm.ct_DaysGRPProcessing) and datevalue=dt.DateValue),ANSIDATE('0001-01-01'))
  from fact_planorder po
        INNER JOIN dim_plant pl ON pl.dim_plantid = po.dim_plantid
        INNER JOIN dim_part prt ON prt.dim_partid = po.dim_partid
        INNER JOIN fact_mrp fm
              ON     fm.dd_Documentno = po.dd_PlanOrderNo
                  AND fm.dim_partid = prt.dim_partid
                  AND fm.dd_PlannScenarioLTP = po.dd_PlannScenario
        INNER JOIN PLSC p ON p.PLSC_PLSCN = po.dd_PlannScenario
        INNER JOIN dim_mrpelement me ON me.Dim_MRPElementID = fm.Dim_MRPElementID
        INNER JOIN dim_date dt ON dt.dim_dateid = fm.dim_dateidreschedule
  WHERE me.MRPElement <> 'U3' AND po.Dim_DateidMRPDock = 1
        AND dt.DateValue BETWEEN p.PLSC_PDAT1 AND p.PLSC_PDAT2;

UPDATE fact_planorder po
From fact_plord_tmp  t ,
   dim_date dt 
set po.Dim_DateidMRPDock = dt.Dim_DateId
Where po.fact_planorderid = t.fact_planorderid
AND dt.datevalue = t.MRPDockDate and dt.companycode = t.companycode ;
  
UPDATE fact_planorder po 
FROM dim_Date dt ,
       dim_plant pl 
   SET po.Dim_DateIdWeekEnding =
          (SELECT dt1.dim_dateid FROM dim_date dt1
           WHERE datevalue = (CASE DAYOFWEEK(dt.datevalue)
              WHEN 1 THEN (dt.datevalue + ( INTERVAL '6' DAY))
              WHEN 2 THEN (dt.datevalue + ( INTERVAL '5' DAY))
              WHEN 3 THEN (dt.datevalue + ( INTERVAL '4' DAY))
              WHEN 4 THEN (dt.datevalue + ( INTERVAL '3' DAY))
              WHEN 5 THEN (dt.datevalue + ( INTERVAL '2' DAY))
              WHEN 6 THEN (dt.datevalue + ( INTERVAL '1' DAY))
              ELSE dt.datevalue
           END) AND dt1.CompanyCode = pl.Companycode)
  WHERE po.Dim_DateidMRPDock <> 1
AND  dt.Dim_DateId = po.Dim_DateidMRPDock
AND pl.dim_plantid = po.dim_plantid AND pl.rowiscurrent = 1;

Drop table if exists interval_tmp_p01;
Create table interval_tmp_p01 As
Select   po.fact_planorderid,
	 (dt.DateValue - (( INTERVAL '1' day) * pt.LeadTime )) calcolumn,
	 pl.CompanyCode
From     fact_planorder po,
	 dim_part pt ,
	 dim_plant pl ,
	 dim_Date dt
Where    po.Dim_DateidMRPDock <> 1
	 AND  po.Dim_PartId = pt.Dim_PartId AND pt.RowIsCurrent = 1
	 AND  pl.dim_plantid = po.dim_plantid AND pl.rowiscurrent = 1
	 AND dt.Dim_DateId = po.Dim_DateidMRPDock;

UPDATE fact_planorder po
FROM interval_tmp_p01 itp
SET po.Dim_DateIdMRPStartDate = ifnull((SELECT dt1.dim_dateid FROM dim_date dt1
                		        WHERE     dt1.datevalue = itp.calcolumn
                    		              AND dt1.companycode = itp.CompanyCode),1)
Where po.fact_planorderid = itp.fact_planorderid;

Drop table if exists interval_tmp_p01;


