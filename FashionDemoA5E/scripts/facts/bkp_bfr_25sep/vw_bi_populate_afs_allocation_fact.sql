/* ##################################################################################################################
  
     Script         : bi_populate_afs_allocation_fact
     Author         : Shanthi
     Created On     : 22 May 2013
  
  
     Description    : Stored Proc bi_populate_afs_allocation_fact from MySQL to Vectorwise syntax
  
     Change History
     Date            By        Version           Desc
     22 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise
#################################################################################################################### */

select 'START OF PROC bi_populate_afs_allocation_fact',TIMESTAMP(LOCAL_TIMESTAMP);

Drop table if exists fact_afsallocation_temp;

create table fact_afsallocation_temp
AS
Select * from fact_afsallocation where 1 = 2;

call vectorwise(combine 'fact_salesorderdelivery');

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE J_3ABDBS SET J_3ABDBS_J_3ASTAT = 'Not Set' WHERE J_3ABDBS_J_3ASTAT IS NULL;

UPDATE J_3ARESH SET J_3ARESH_J_3ASTAT = 'Not Set' WHERE J_3ARESH_J_3ASTAT IS NULL;

select 'Insert 1 on fact_afsallocation_temp',TIMESTAMP(LOCAL_TIMESTAMP);

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid,
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  'Not Set',
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          f.ct_AfsOpenQty,
	  0 ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ct_ScheduleQtySalesUnit,
          ct_ConfirmedQty,
	  0,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f. Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          f.ct_ReservedQty,
          f.ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1)
     FROM fact_salesorderdelivery f
     WHERE NOT EXISTS( SELECT 1 FROM j_3abdsi j
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo);

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  'Not Set',
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull(j.J_3abdsi_menge,0) ct_AfsOpenQty,
          0 ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          f.ct_ScheduleQtySalesUnit,
          f.ct_ConfirmedQty,
	  0.0000 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1)
     FROM fact_salesorderdelivery f, j_3abdsi j
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND NOT EXISTS ( SELECT 1 FROM J_3ABDBS j1
			    WHERE j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
	    		      AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			      AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM J_3ARESH j2
			    WHERE f.dd_SalesdocNo = j2.J_3ARESH_AUFNR
			     AND  f.dd_SalesItemNo = j2.J_3ARESH_POSNR
			     AND f.dd_ScheduleNo = j2.J_3ARESH_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND f1.dd_AfsMrpStatus = 'Not Set'
			    AND f1.dd_AfsReshMrpStatus = 'Not Set' );

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  j1.J_3ABDBS_J_3ASTAT,
	  'Not Set',
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull((CASE WHEN j.J_3abdsi_menge > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0) ct_AfsOpenQty,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'D' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ifnull((CASE WHEN ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN ct_ConfirmedQty > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  J_3ABDBS_MENGE ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'R' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ReservedQty,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'F' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1)
     FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND NOT EXISTS ( SELECT 1 FROM J_3ARESH j2
			    WHERE f.dd_SalesdocNo = j2.J_3ARESH_AUFNR
			     AND  f.dd_SalesItemNo = j2.J_3ARESH_POSNR
			     AND f.dd_ScheduleNo = j2.J_3ARESH_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND f1.dd_AfsReshMrpStatus = 'Not Set' );

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;
		    
INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  j1.J_3ARESH_J_3ASTAT,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull((CASE WHEN j.J_3ABDSI_menge > j1.J_3ARESH_MENGE THEN j1.J_3ARESH_MENGE ELSE j.J_3abdsi_menge END),0),
	  ifnull((CASE WHEN j1.J_3ARESH_J_3ASTAT = 'D' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ifnull((CASE WHEN ct_ScheduleQtySalesUnit > j1.J_3ARESH_MENGE THEN j1.J_3ARESH_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN ct_ConfirmedQty > j1.J_3ARESH_MENGE THEN j1.J_3ARESH_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  0 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j1.J_3ARESH_J_3ASTAT = 'R' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExpectedReservedQty,
	  ifnull((CASE WHEN j1.J_3ARESH_J_3ASTAT = 'F' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExecptedFixedQty
 FROM fact_salesorderdelivery f, j_3abdsi j, J_3ARESH j1
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j1.J_3ARESH_AUFNR = f.dd_SalesDocNo
			  AND j1.J_3ARESH_POSNR = f.dd_SalesItemNo
			  AND j1.J_3ARESH_ETENR = f.dd_ScheduleNo
			  AND NOT EXISTS ( SELECT 1 FROM J_3abdbs j2
			  WHERE j2.J_3ABDBS_AUFNR = f.dd_SalesDocNo
			  AND j2.J_3ABDBS_POSNR = f.dd_SalesItemNo
			  AND j2.J_3ABDBS_ETENR = f.dd_ScheduleNo)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp t1
			   WHERE t1.dd_salesdocno = f.dd_SalesDocNo
			     AND t1.dd_SalesItemNo = f.dd_SalesItemNo
			     AND t1.dd_ScheduleNo = f.dd_scheduleno
			     AND t1.dd_AfsMrpstatus = 'Not Set'
			     AND t1.dd_AfsReshMrpStatus = j1.J_3ARESH_J_3ASTAT);

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;


INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  j1.J_3ABDBS_J_3ASTAT,
	  j2.J_3ARESH_J_3ASTAT,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull((CASE WHEN j.J_3abdsi_menge > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0),
	  ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'D' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
	  ifnull((CASE WHEN (ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ScheduleQtySalesUnit > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (ct_ConfirmedQty > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ConfirmedQty > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  ifnull(j1.J_3ABDBS_MENGE,0) ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
	  f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'R' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ReservedQty,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'F' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'R' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ExpectedReservedQty,
          ifnull((CASE WHEN j1.J_3ABDBS_J_3ASTAT = 'F' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ExecptedFixedQty
     FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j1.J_3ABDBS_J_3ASTAT = j2.J_3ARESH_J_3ASTAT
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus);



call vectorwise ( combine 'fact_afsallocation_temp');

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;
			    
INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  'Not Set',
	  j2.J_3ARESH_J_3ASTAT,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull((CASE WHEN j.J_3abdsi_menge > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0),
	  ifnull((CASE WHEN J_3ABDBS_J_3ASTAT = 'D' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
	  ifnull((CASE WHEN (ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ScheduleQtySalesUnit > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (ct_ConfirmedQty > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ConfirmedQty > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  0 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'R' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExpectedReservedQty,
          ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'F' THEN J_3ARESH_MENGE ELSE 0 END),0) ct_ExecptedFixedQty
	  FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j2.J_3ARESH_J_3ASTAT NOT IN ( SELECT DISTINCT j3.J_3ABDBS_J_3ASTAT FROM J_3ABDBS j3
								  where j2.J_3ARESH_AUFNR = j3.J_3ABDBS_AUFNR
								  AND j2.J_3ARESH_POSNR = j3.J_3ABDBS_POSNR
								  AND j2.J_3ARESH_ETENR = j3.J_3ABDBS_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus);

		  
call vectorwise(combine 'fact_afsallocation_temp');

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;
			    
INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty
				    )
   SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo)) fact_afsallocationid, 
	  dd_SalesDocNo,
          dd_SalesItemNo,
          dd_ScheduleNo,
	  J_3ABDBS_J_3ASTAT,
	  'Not Set',
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull((CASE WHEN j.J_3abdsi_menge > j1.J_3ABDBS_MENGE THEN j1.J_3ABDBS_MENGE ELSE j.J_3abdsi_menge END),0),
	  ifnull((CASE WHEN J_3ABDBS_J_3ASTAT = 'D' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
	  ifnull((CASE WHEN (ct_ScheduleQtySalesUnit > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ScheduleQtySalesUnit > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ScheduleQtySalesUnit END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (ct_ConfirmedQty > j1.J_3ABDBS_MENGE AND j1.J_3ABDBS_MENGE >= j2.J_3ARESH_MENGE) THEN j2.J_3ARESH_MENGE WHEN (ct_ConfirmedQty > j2.J_3ARESH_MENGE and j2.J_3ARESH_MENGE >= j1.J_3ABDBS_MENGE) THEN j1.J_3ABDBS_MENGE ELSE ct_ConfirmedQty END),0) ct_ConfirmedQty,
	  ifnull(j1.J_3ABDBS_MENGE,0) ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          ifnull((CASE WHEN J_3ABDBS_J_3ASTAT = 'R' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_ReservedQty,
          ifnull((CASE WHEN J_3ABDBS_J_3ASTAT = 'F' THEN J_3ABDBS_MENGE ELSE 0 END),0) ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  0 ct_ExpectedReservedQty,
          0 ct_ExecptedFixedQty
	  FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j1.J_3ABDBS_J_3ASTAT NOT IN ( SELECT DISTINCT j3.J_3ARESH_J_3ASTAT FROM J_3ARESH j3
								  where j2.J_3ARESH_AUFNR = j3.J_3ARESH_AUFNR
								  AND j1.J_3ABDBS_POSNR = j3.J_3ARESH_POSNR
								  AND j1.J_3ABDBS_ETENR = j3.J_3ARESH_ETENR)
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus);

		  
call vectorwise(combine 'fact_afsallocation_temp');

DROP TABLE IF EXISTS tmp_AllocatedAndForecastRESH;

CREATE TABLE tmp_AllocatedAndForecastRESH AS 
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,dd_AfsReshMrpStatus,SUM(ct_ExpectedReservedQty) as ExpReservedQty,SUM(ct_ExpectedFixedQty) as ExpFixedQty
FROM fact_afsallocation_temp
group by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,dd_AfsReshMrpStatus;


DROP TABLE IF EXISTS tmp_TotalOrdConfQtyAtSchedule;

CREATE TABLE tmp_TotalOrdConfQtyAtSchedule AS 
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,SUM(ct_AfsOpenQty) as OpenQty, sum(ct_ConfirmedQty) as ConfQty, SUM(ct_ScheduleQtySalesUnit) as OrderQty
FROM fact_afsallocation_temp
group by dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo;

delete from NUMBER_FOUNTAIN where table_name = 'fact_afsallocation_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_afsallocation_temp',ifnull(max(fact_afsallocationid),1)
FROM fact_afsallocation_temp;

INSERT INTO fact_afsallocation_temp(
fact_afsallocationid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
				    dd_AfsMrpStatus,
				    dd_AfsReshMrpStatus,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
				    ct_AfsOnDeliveryQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
				    				ct_AfsAllocatedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
				    Dim_CurrencyId_TRA,
				    Dim_CurrencyId_GBL,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdsodocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    dd_billing_no,
				    dd_Billingitem_no,
				    amt_BillingCustomerConfigSubtotal4,
				    ct_AfsBilledQty,
				    Dim_DateIdRejection,
				    ct_ExpectedReservedQty,
				    ct_ExpectedFixedQty
				    )
SELECT ((SELECT ifnull(max_id, 1) from NUMBER_FOUNTAIN WHERE table_name = 'fact_afsallocation_temp') + row_number() over (order by t.dd_SalesDocNo,t.dd_SalesItemNo,t.dd_ScheduleNo)) fact_afsallocationid, 
	  t.dd_SalesDocNo,
          t.dd_SalesItemNo,
          t.dd_ScheduleNo,
	  'Not Set',
	  j2.J_3ARESH_J_3ASTAT,
          dd_SalesDlvrDocNo,
          dd_SalesDlvrItemNo,
          ifnull(dd_MovementType,'Not Set'),
          ifnull(f.dd_AfsStockCategory,'Not Set'),
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          ct_QtyDelivered,
          amt_Cost_DocCurr,
          f.amt_Cost,
          f.amt_SalesSubTotal3,
          f.amt_SalesSubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          ifnull((CASE WHEN (J_3abdsi_menge - t1.OpenQty ) >= (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) THEN (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) ELSE 0 END),0),
	      0 ct_AfsOnDeliveryQty,
          ct_FixedProcessDays,
          ct_ShipProcessDays,
          ifnull((CASE WHEN (f.ct_ScheduleQtySalesUnit - t1.OrderQty) >= (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) THEN (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) ELSE 0 END),0) ct_ScheduleQtySalesUnit,
          ifnull((CASE WHEN (f.ct_ConfirmedQty - t1.ConfQty) >= (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) THEN (J_3ARESH_MENGE - t.ExpReservedQty - t.ExpFixedQty) Else 0 END),0) ct_ConfirmedQty,
	      0 ct_AfsAllocatedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidPlannedGoodsIssue,
          f.Dim_DateidActualGoodsIssue,
          f.Dim_DateidDeliveryDate,
          f.Dim_DateidLoadingDate,
          f.Dim_DateidPickingDate,
          f.Dim_DateidDlvrDocCreated,
          f.Dim_DateidMatlAvail,
          f.Dim_CustomeridSoldTo,
          f.Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          f.Dim_DeliveryHeaderStatusid,
          f.Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
	  f.Dim_DateidMatlAvail,
          f.Dim_Currencyid,
	  f.Dim_CurrencyId_TRA,
	  f.Dim_CurrencyId_GBL,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
          f.Dim_ControllingAreaId,
          f.Dim_DateidBillingDate,
          ifnull(f.dim_billingdocumenttypeid,1),
          f.Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
          f.Dim_DateidActualGI_Original,
          f.dd_AfsDepartment,
          0 ct_ReservedQty,
          0 ct_FixedQty,
          f.amt_SalesSubTotal3UnitPrice,
	  f.Dim_DateIdSODocument,
	  f.Dim_DateIdSOCreated,
	  ifnull(f.dd_SDCreatedBy,'Not Set'),
	  ifnull(f.Dim_DeliveryblockId,1),
	  0,
	  ifnull(f.Dim_DateIdSOItemChangedOn,1),
          ifnull(f.Dim_ShippingConditionId,1),
	  ifnull(f.dd_AfsAllocationGroupNo,'Not Set'),
	  ifnull(f.Dim_SalesOrderHeaderStatusId,1),
	  ifnull(f.Dim_SalesOrderItemStatusId,1),
	  ifnull(f.ct_AfsTotalDrawn,0.0000),
	  ifnull(f.Dim_DateidSchedDelivery,1),
	  ifnull(dd_billing_no,'Not Set'),
	  ifnull(dd_Billingitem_no,'Not Set'),
	  ifnull(amt_BillingCustomerConfigSubtotal4,0.0000),
	  ifnull(ct_AfsBilledQty,0.0000),
	  ifnull(f.Dim_DateIdRejection,1),
	  ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'R' THEN (j2.J_3ARESH_MENGE - t.ExpReservedQty) ELSE 0 END),0) ct_ExpectedReservedQty,
          ifnull((CASE WHEN j2.J_3ARESH_J_3ASTAT = 'F' THEN (j2.J_3ARESH_MENGE - t.ExpFixedQty) ELSE 0 END),0) ct_ExecptedFixedQty
	  FROM fact_salesorderdelivery f, j_3abdsi j, J_3ABDBS j1, J_3ARESH j2, tmp_AllocatedAndForecastRESH t, tmp_TotalOrdConfQtyAtSchedule t1
			WHERE j.J_3ABDSI_AUFNR = f.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = f.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = f.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = t1.dd_SalesDocNo
			  AND j.J_3ABDSI_POSNR = t1.dd_SalesItemNo
			  AND j.J_3ABDSI_ETENR = t1.dd_ScheduleNo
			  AND j.J_3ABDSI_AUFNR = j1.J_3ABDBS_AUFNR
			  AND j.J_3ABDSI_POSNR = j1.J_3ABDBS_POSNR
			  AND j.J_3ABDSI_ETENR = j1.J_3ABDBS_ETENR
			  AND j1.J_3ABDBS_AUFNR = j2.J_3ARESH_AUFNR
			  AND j1.J_3ABDBS_POSNR = j2.J_3ARESH_POSNR
			  AND j1.J_3ABDBS_ETENR = j2.J_3ARESH_ETENR
			  AND j1.J_3ABDBS_AUFNR  = t.dd_SalesDocNo
			  AND j1.J_3ABDBS_POSNR = t.dd_SalesItemNo
			  AND j1.J_3ABDBS_ETENR = t.dd_ScheduleNo
			  AND j2.J_3ARESH_J_3ASTAT = t.dd_AfsReshMrpStatus
			  AND ((t.dd_AfsReshMrpStatus = 'R' AND t.ExpReservedQty < j2.J_3ARESH_MENGE) OR
			       (t.dd_AfsReshMrpStatus = 'F' AND t.ExpFixedQty < j2.J_3ARESH_MENGE))
			  AND NOT EXISTS ( SELECT 1 FROM fact_afsallocation_temp f1
			    WHERE f.dd_SalesDocNo = f1.dd_SalesDocNo
			    AND f.dd_SalesItemNo = f1.dd_SalesItemNo
			    AND f.dd_ScheduleNo = f1.dd_scheduleno
			    AND j1.J_3ABDBS_J_3ASTAT  = f1.dd_AfsMrpStatus
			    AND j2.J_3ARESH_J_3ASTAT = f1.dd_AfsReshMrpStatus);


/* UPDATE fact_afsallocation_temp f_afsa
 FROM J_3ARESH j, tmp_AllocatedAndForecastRESH t
SET f_afsa.dd_AfsReshMrpStatus = 'R',
    f_afsa.ct_ExpectedReservedQty = j.J_3ARESH_MENGE - t.ExpReservedQty 
WHERE j.J_3ARESH_AUFNR = f_afsa.dd_SalesDocNo
AND j.J_3ARESH_POSNR = f_afsa.dd_SalesItemNo
AND j.J_3ARESH_ETENR = f_afsa.dd_ScheduleNo
AND t.dd_SalesDocNo = f_afsa.dd_SalesDocNo
AND t.dd_SalesItemNo = f_afsa.dd_SalesItemNo
AND t.dd_ScheduleNo = f_afsa.dd_ScheduleNo
AND j.J_3ARESH_J_3ASTAT = 'R'
AND f_afsa.dd_AfsMrpStatus = 'Not Set' */

DROP TABLE IF EXISTS tmp_AllocatedAndForecastRESH;

DROP TABLE IF EXISTS tmp_TotalOrdConfQtyAtSchedule;

call vectorwise ( combine 'fact_afsallocation_temp');

UPDATE fact_afsallocation_temp afsa
SET Dim_DateIdRejection = ifnull(afsa.Dim_DateIdSOItemChangedOn, afsa.Dim_DateIdSOCreated)
WHERE afsa.Dim_AfsRejectionReasonId <> 1
and Dim_DateIdRejection = 1;

UPDATE fact_Afsallocation_temp afsa
SET ct_AfsUnallocatedQty = afsa.ct_AfsOpenQty - afsa.ct_AfsAllocatedQty
WHERE afsa.ct_AfsOpenQty >= afsa.ct_AfsAllocatedQty;

UPDATE fact_afsallocation_temp afsa
 FROM J_3ARESH j
 SET dd_ArunStockType = ifnull(j.J_3ARESH_J_3ABSKZ,'Not Set')
 WHERE j.J_3ARESH_AUFNR = afsa.dd_SalesDocNo
 and j.J_3ARESH_POSNR  = afsa.dd_SalesItemNo
 and j.J_3ARESH_ETENR = afsa.dd_ScheduleNo
 and j.J_3ARESH_J_3ASTAT = afsa.dd_AfsReshMrpStatus
 AND j.J_3ARESH_J_3ABSKZ IS NOT NULL;

UPDATE fact_afsallocation_temp afsa
 SET dd_ArunStockType = 'Not Set'
 WHERE dd_ArunStockType IS  NULL;

UPDATE fact_afsallocation_temp afsa
 FROM J_3ARESH j, dim_plant pl, dim_date dt
 SET afsa.Dim_DateIdPOSchedLineAvailability = dt.dim_Dateid
 WHERE j.J_3ARESH_AUFNR = afsa.dd_SalesDocNo
 and j.J_3ARESH_POSNR  = afsa.dd_SalesItemNo
 and j.J_3ARESH_ETENR = afsa.dd_ScheduleNo
 and j.J_3ARESH_J_3ASTAT = afsa.dd_AfsReshMrpStatus
 and afsa.dim_plantid = pl.dim_plantid
 AND dt.companycode = pl.companycode
 AND dt.datevalue = j.J_3ARESH_TERMN
 AND j.J_3ARESH_TERMN IS NOT NULL;

UPDATE fact_afsallocation_temp afsa
 SET afsa.Dim_DateIdPOSchedLineAvailability = 1
 WHERE afsa.Dim_DateIdPOSchedLineAvailability IS  NULL;

call vectorwise(combine 'fact_afsallocation_temp');
 
 DROP TABLE IF EXISTS tmp_AfsRescheduleAllocation;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE fact_afsallocation_temp afsa
SET dd_ProjectedCategory = 'Not Set',
Dim_DateIdProjectedDate = 1,
ct_ProjectedQty = 0; 

UPDATE fact_afsallocation_temp afsa
SET Dim_DateIdProjectedDate = Dim_DateidActualGI_Original
WHERE Dim_DateidActualGI_Original > 1 
AND ct_AFSBilledQty > 0;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE  fact_afsallocation_temp
SET Dim_DateIdProjectedDate = Dim_DateidPlannedGoodsIssue
WHERE Dim_DateidActualGI_Original = 1
AND dd_SalesDlvrDocNo <> 'Not Set'
AND Dim_DateidPlannedGoodsIssue > 1;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdAfsReqDelivery
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND (afsa.ct_AfsOpenQty - afsa.ct_QtyDelivered) > 0
AND cg.DocumentCategory NOT in ('G','g','B','b')
AND dt.DateValue > current_Date;

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdAfsReqDelivery
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND (afsa.ct_ConfirmedQty - afsa.ct_AfsTotalDrawn) > 0
AND cg.DocumentCategory in ('G','g','B','b')
AND dt.DateValue > current_Date;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdPOSchedLineAvailability
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND (afsa.ct_ConfirmedQty - afsa.ct_AfsTotalDrawn) > 0
AND cg.DocumentCategory in ('G','g','B','b')
AND dt.DateValue <= current_Date;

UPDATE  fact_afsallocation_temp afsa
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdPOSchedLineAvailability
WHERE  afsa.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = afsa.Dim_DateIdAfsReqDelivery
AND (afsa.ct_AfsOpenQty- afsa.ct_QtyDelivered) > 0
AND cg.DocumentCategory NOT in ('G','g','B','b')
AND dt.DateValue <= current_Date;

call vectorwise(combine 'fact_afsallocation_temp');


UPDATE  fact_afsallocation_temp afsa
SET afsa.Dim_DateIdProjectedDate = afsa.Dim_DateIdRejection
where afsa.Dim_AfsRejectionReasonId <> 1;

call vectorwise(combine 'fact_afsallocation_temp');

UPDATE fact_afsallocation_temp afsa
SET dd_ProjectedCategory = 'REJECTED',
ct_ProjectedQty = afsa.ct_ScheduleQtySalesUnit 
WHERE afsa.Dim_AfsRejectionReasonId <> 1;

UPDATE fact_afsallocation_temp afsa
SET dd_ProjectedCategory = 'SHIPPED',
ct_ProjectedQty = afsa.ct_AFSBilledQty
WHERE Dim_DateidActualGI_Original > 1 
AND afsa.Dim_AfsRejectionReasonId = 1
AND ct_AFSBilledQty > 0
AND afsa.dd_AfsMrpStatus = 'Not Set'
AND afsa.dd_AfsReshMrpStatus = 'Not Set';

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc
SET dd_ProjectedCategory = 'QUOTATION' ,
ct_ProjectedQty = (afsa.ct_ConfirmedQty - afsa.ct_AfsTotalDrawn)
WHERE dc.dim_Documentcategoryid = afsa.dim_Documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND dc.DocumentCategory IN ('B','b');

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc
SET dd_ProjectedCategory = 'ALLOCATED CONTRACT' ,
ct_ProjectedQty = (CASE WHEN afsa.ct_ExpectedReservedQty = 0 THEN afsa.ct_ReservedQty ELSE afsa.ct_ExpectedReservedQty END)
WHERE dc.dim_Documentcategoryid = afsa.dim_Documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND dc.DocumentCategory IN ('G','g')
AND afsa.ct_AfsOpenQty > 0
AND (afsa.dd_AfsReshMrpStatus = 'R' OR afsa.dd_AfsMrpStatus = 'R');

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc
SET dd_ProjectedCategory = 'UNALLOCATED CONTRACT',
ct_ProjectedQty = (afsa.ct_AfsOpenQty - afsa.ct_AfsAllocatedQty)
WHERE dc.dim_Documentcategoryid = afsa.dim_Documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND dc.DocumentCategory IN ('G','g')
AND afsa.dd_AfsReshMrpStatus = 'Not Set'
AND afsa.dd_AfsMrpStatus = 'Not Set'
AND afsa.ct_AfsOpenQty > 0;

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc
SET dd_ProjectedCategory = 'UNALLOCATED ORDER' ,
ct_ProjectedQty = (afsa.ct_AfsOpenQty - afsa.ct_AfsAllocatedQty)
WHERE dc.dim_Documentcategoryid = afsa.dim_Documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND afsa.dd_AfsReshMrpStatus = 'Not Set'
AND afsa.dd_AfsMrpStatus = 'Not Set'
AND afsa.Dim_DateidActualGI_Original = 1
AND (afsa.ct_AfsOpenQty-afsa.ct_QtyDelivered) > 0
AND dc.DocumentCategory NOT IN ('B','b','G','g');

UPDATE fact_afsallocation_temp afsa
FROM  dim_Documentcategory dc
SET dd_ProjectedCategory = 'DELIVERY BLOCK',
ct_ProjectedQty = (CASE WHEN afsa.ct_ExpectedFixedQty = 0 THEN afsa.ct_FixedQty ELSE afsa.ct_ExpectedFixedQty END)
WHERE dc.dim_documentcategoryid = afsa.dim_documentcategoryid
AND afsa.Dim_AfsRejectionReasonId = 1
AND dc.DocumentCategory NOT IN ('B','b','G','g')
AND (afsa.dd_AfsReshMrpStatus = 'F' OR afsa.dd_AfsMrpStatus = 'F')
AND afsa.Dim_DeliveryBlockid > 1;

UPDATE fact_afsallocation_temp afsa
FROM  dim_Documentcategory dc,
      dim_overallstatusforcreditcheck ocs
SET dd_ProjectedCategory = 'CREDIT BLOCK',
ct_ProjectedQty = (CASE WHEN afsa.ct_ExpectedFixedQty = 0 THEN afsa.ct_FixedQty ELSE afsa.ct_ExpectedFixedQty END)
WHERE dc.dim_documentcategoryid = afsa.dim_documentcategoryid
AND dc.DocumentCategory NOT IN ('B','b','G','g')
AND ocs.dim_overallstatusforcreditcheckID = 
                afsa.Dim_OverallStatusCreditCheckId 
AND afsa.Dim_AfsRejectionReasonId = 1
AND  ( afsa.dd_AfsMrpStatus = 'F' OR afsa.dd_AfsReshMrpStatus = 'F')
AND afsa.Dim_DeliveryBlockid = 1
AND ocs.OverallStatusforCreditCheck IN ('B');

UPDATE fact_afsallocation_temp afsa
FROM dim_Documentcategory dc,
      dim_overallstatusforcreditcheck ocs
SET dd_ProjectedCategory = 'PROJECTED TO SHIP' ,
ct_ProjectedQty = (CASE WHEN afsa.ct_ExpectedFixedQty = 0 THEN afsa.ct_FixedQty ELSE afsa.ct_ExpectedFixedQty END)
WHERE dc.dim_documentcategoryid = afsa.dim_documentcategoryid
AND dc.DocumentCategory NOT IN ('B','b','G','g')
AND ocs.dim_overallstatusforcreditcheckID = 
                afsa.Dim_OverallStatusCreditCheckId 
AND afsa.Dim_AfsRejectionReasonId = 1
AND (afsa.dd_AfsMrpStatus = 'F' OR afsa.dd_AfsReshMrpStatus = 'F')
AND afsa.Dim_DeliveryBlockid = 1
AND ocs.OverallStatusforCreditCheck NOT IN ('B');

UPDATE fact_afsallocation_temp afsa
SET dd_ProjectedCategory = 'ON DELIVERY',
ct_ProjectedQty = afsa.ct_AfsOnDeliveryQty
WHERE afsa.dd_AfsMrpStatus = 'D'
AND afsa.dd_AfsReshMrpStatus = 'Not Set'
AND afsa.ct_AfsOnDeliveryQty > 0;
  
call vectorwise(combine 'fact_afsallocation_temp');
call vectorwise(combine 'fact_afsallocation - fact_afsallocation');
call vectorwise(combine 'fact_afsallocation');
call vectorwise(combine 'fact_afsallocation + fact_afsallocation_temp'); 

select 'Process Complete';
select current_time;

select 'END OF PROC bi_populate_afs_allocation_fact',TIMESTAMP(LOCAL_TIMESTAMP);