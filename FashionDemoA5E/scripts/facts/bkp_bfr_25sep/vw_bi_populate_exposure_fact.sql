
/**************************************************************************************************************/
/*   Script         : vw_bi_custom_columbia_accountsreceivable */
/*   Author         : Shanthi */
/*   Created On     : 14 Aug 2013 */
/*   Description    : Stored Proc vw_bi_populate_exposure_fact.sql migration from MySQL to Vectorwise syntax */
/*********************************************Change History*******************************************************/
/*   Date            By        Version           Desc 															  */
/*   14 Aug 2013      Shanthi    1.0               Existing code migrated to Vectorwise 							  */
/******************************************************************************************************************/

/*********************************************START****************************************************************/

drop table if exists fact_exposure_temp;
CREATE TABLE fact_exposure_temp as SELECT * from fact_exposure where 1 = 2;

delete from NUMBER_FOUNTAIN where table_name = 'fact_exposure_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_exposure_temp',ifnull(max(fact_exposureid),1)
FROM fact_exposure_temp;

INSERT INTO fact_exposure_temp(fact_exposureid,
			  amt_InLocalCurrency,
                          dd_FiscalYear,
                          dd_FiscalPeriod,
                          dd_AccountingDocItemNo,
                          dd_AccountingDocNo,
                          dd_AssignmentNumber,
                          dd_CreditLimit,
                          dd_CreditRep,
                          dim_companyid,
                          Dim_CurrencyId,
			  Dim_CurrencyId_TRA,
			  Dim_CurrencyId_GBL,
			  amt_ExchangeRate,
			  amt_ExchangeRate_GBL,
                          dim_customerid,
                          Dim_DateIdAccountingDocument,
                          dim_DateIdAFSReqDelivery,
                          dim_dateidfixedvalue,
                          Dim_DateIdNetDueDate,
                          Dim_DateIdPosting,
                          Dim_PaymentTermsId,
			  dim_dateidactualgidate,
			  dim_documenttypeid,
			  dd_DueType,
			  dd_SalesDocNo,
			  dd_SalesItemNo,
			  dd_DisputeCaseID,
			  Dim_PaymentReasonId
			  )
 SELECT ((SELECT ifnull(max_id, 1)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_exposure_temp') + row_number() over ()) fact_exposureid,
          a.*
  FROM ( SELECT DISTINCT ar.amt_InLocalCurrency,
                   ar.dd_FiscalYear,
                   ar.dd_FiscalPeriod,
                   ar.dd_AccountingDocItemNo,
                   ar.dd_AccountingDocNo,
                   ar.dd_AssignmentNumber,
                   ifnull(ar.dd_CreditLimit,0),
                   ifnull(ar.dd_CreditRep,'Not Set'),
                   ar.dim_companyid,
                   ar.Dim_CurrencyId,
		   ar.Dim_CurrencyId_TRA,
		   ar.Dim_CurrencyId_GBL,
		   ar.amt_ExchangeRate,
		   ar.amt_ExchangeRate_GBL,
                   ar.dim_customerid,
                   ar.Dim_DateIdAccDocDateEntered,
                   1,
                   1,
                   ar.Dim_NetDueDateId,
                   ar.Dim_DateIdPosting,
                   1,
		   ifnull(ar.dim_dateidactualgidate,1),
                   ar.dim_documenttypeid,
		   'AR',
		   'Not Set',
		   0,
		   ar.dd_DisputeCaseID,
		   ar.Dim_PaymentReasonId
     FROM fact_accountsreceivable ar
     WHERE ar.dim_clearedflagid IN (2,4) AND
      NOT EXISTS ( SELECT 1 FROM fact_exposure_temp e
                         WHERE e.dd_AccountingDocNo = ar.dd_AccountingDocNo
			   AND e.dd_AccountingDocItemNo = ar.dd_AccountingDocItemNo
			   AND e.dd_AssignmentNumber = ar.dd_AssignmentNumber
			   AND e.Dim_CustomerId = ar.Dim_CustomerId
			   AND e.Dim_CompanyId = ar.Dim_CompanyId
			   AND e.dd_DueType = 'AR')) a;


DROP TABLE IF EXISTS tmp_ARSOLink;
CREATE TABLE tmp_ARSOLink AS
SELECT ar.dd_AccountingDocNo,ar.dd_AccountingDocItemNo,ar.dd_AssignmentNumber, ar.dim_companyid,ar.dim_customerid,min(dt.datevalue) as AFSReqDate
FROM fact_accountsreceivable ar
inner join fact_billing b on ar.dd_accountingdocno = b.dd_billing_no
 inner join fact_salesorder so on b.dd_salesdocno = so.dd_salesdocno and b.dd_salesitemno = so.dd_salesitemno and b.dd_AfsScheduleNo = so.dd_scheduleNo
 inner join dim_date dt on dt.dim_Dateid = so.dim_DateidAFSReqDelivery
 WHERE ar.dd_InvoiceNumberTransBelongTo = 'Not Set' and so.dim_DateidAFSReqDelivery > 1
 group by ar.dd_AccountingDocNo,ar.dd_AccountingDocItemNo,ar.dd_AssignmentNumber, ar.dim_companyid,ar.dim_customerid;

UPDATE fact_exposure_temp ar
 FROM tmp_ARSOLink t, dim_date dt, dim_Company c
 SET ar.dim_DateIdAFSReqDelivery = dt.dim_Dateid
WHERE ar.dd_AccountingDocNo = t.dd_AccountingDocNo
AND ar.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
aND ar.dd_AssignmentNumber = t.dd_AssignmentNumber
AND ar.dim_companyid = t.dim_companyid
AND ar.dim_customerid = t.dim_customerid
AND c.Dim_Companyid = ar.dim_Companyid
AND c.CompanyCode = dt.CompanyCode
AND dt.datevalue = t.AFSReqDate
AND ar.dd_DueType = 'AR';

DROP TABLE IF EXISTS tmp_ARSOLink;

DROP TABLE IF EXISTS tmp_ARSOLink;
CREATE TABLE tmp_ARSOLink AS
SELECT ar.dd_AccountingDocNo,ar.dd_AccountingDocItemNo,ar.dd_AssignmentNumber, ar.dim_companyid,ar.dim_customerid,min(dt.datevalue) as FixedDate
FROM fact_accountsreceivable ar
inner join fact_billing b on ar.dd_accountingdocno = b.dd_billing_no
 inner join fact_salesorder so on b.dd_salesdocno = so.dd_salesdocno and b.dd_salesitemno = so.dd_salesitemno and b.dd_AfsScheduleNo = so.dd_scheduleNo
 inner join dim_date dt on dt.dim_Dateid = so.Dim_DateIdFixedValue
 WHERE ar.dd_InvoiceNumberTransBelongTo = 'Not Set' and so.Dim_DateIdFixedValue > 1
 group by ar.dd_AccountingDocNo,ar.dd_AccountingDocItemNo,ar.dd_AssignmentNumber, ar.dim_companyid,ar.dim_customerid;

UPDATE fact_exposure_temp ar
 FROM tmp_ARSOLink t, dim_date dt, dim_Company c
 SET ar.dim_DateIdAFSReqDelivery = dt.dim_Dateid
WHERE ar.dd_AccountingDocNo = t.dd_AccountingDocNo
AND ar.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
aND ar.dd_AssignmentNumber = t.dd_AssignmentNumber
AND ar.dim_companyid = t.dim_companyid
AND ar.dim_customerid = t.dim_customerid
AND c.Dim_Companyid = ar.dim_Companyid
AND c.CompanyCode = dt.CompanyCode
AND dt.datevalue = t.FixedDate
AND ar.dd_DueType = 'AR';

DROP TABLE IF EXISTS tmp_ARSOLink;
CREATE TABLE tmp_ARSOLink AS
SELECT ar.dd_AccountingDocNo,ar.dd_AccountingDocItemNo,ar.dd_AssignmentNumber, ar.dim_companyid,ar.dim_customerid,t052_zterm,MAX((CASE WHEN IFNULL(T052_ZTAG3,0) IS NULL THEN (CASE WHEN IFNULL(T052_ZTAG2,0) IS NULL THEN IFNULL(T052_ZTAG1,0) ELSE IFNULL(T052_ZTAG2,0) END) ELSE IFNULL(T052_ZTAG3,0) END)) as PaymentTermsDays
FROM fact_accountsreceivable ar
inner join fact_billing b on ar.dd_accountingdocno = b.dd_billing_no
 inner join fact_salesorder so on b.dd_salesdocno = so.dd_salesdocno and b.dd_salesitemno = so.dd_salesitemno and b.dd_AfsScheduleNo = so.dd_scheduleNo
 inner join dim_CustomerPaymentTerms ct on ct.dim_CustomerPaymentTermsId = so.Dim_PaymentTermsId
 inner join t052 on t052_Zterm = ct.PaymentTermCode
 WHERE ar.dd_InvoiceNumberTransBelongTo = 'Not Set' and so.Dim_PaymentTermsId > 1
 group by ar.dd_AccountingDocNo,ar.dd_AccountingDocItemNo,ar.dd_AssignmentNumber, ar.dim_companyid,ar.dim_customerid,t052_zterm ;

UPDATE fact_exposure_temp ar
 FROM tmp_ARSOLink t, dim_CustomerPaymentTerms dt
 SET ar.Dim_PaymentTermsId = ifnull(dt.dim_CustomerPaymentTermsId,1)
WHERE ar.dd_AccountingDocNo = t.dd_AccountingDocNo
AND ar.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
aND ar.dd_AssignmentNumber = t.dd_AssignmentNumber
AND ar.dim_companyid = t.dim_companyid
AND ar.dim_customerid = t.dim_customerid
AND dt.PaymentTermCode = t.t052_zterm
AND dt.RowIsCurrent = 1
AND ar.dd_DueType = 'AR';

DROP TABLE IF EXISTS tmp_ARSOLink;

CREATE TABLE tmp_ARSOLink AS
SELECT ar.dd_AccountingDocNo,ar.dd_AccountingDocItemNo,ar.dd_AssignmentNumber, ar.dim_companyid,ar.dim_customerid,t052_zterm,MAX((CASE WHEN IFNULL(T052_ZTAG3,0) IS NULL THEN (CASE WHEN IFNULL(T052_ZTAG2,0) IS NULL THEN IFNULL(T052_ZTAG1,0) ELSE IFNULL(T052_ZTAG2,0) END) ELSE IFNULL(T052_ZTAG3,0) END)) as PaymentTermsDays
FROM fact_accountsreceivable ar
inner join fact_billing b on ar.dd_accountingdocno = b.dd_billing_no
 inner join fact_salesorder so on b.dd_salesdocno = so.dd_salesdocno and b.dd_salesitemno = so.dd_salesitemno and b.dd_AfsScheduleNo = so.dd_scheduleNo
 inner join dim_CustomerPaymentTerms ct on ct.dim_CustomerPaymentTermsId = so.dim_CustomerPaymentTermsId
 inner join t052 on t052_Zterm = ct.PaymentTermCode
 WHERE ar.dd_InvoiceNumberTransBelongTo = 'Not Set' and so.dim_CustomerPaymentTermsId > 1
 group by ar.dd_AccountingDocNo,ar.dd_AccountingDocItemNo,ar.dd_AssignmentNumber, ar.dim_companyid,ar.dim_customerid,t052_zterm ;

UPDATE fact_exposure_temp ar
 FROM tmp_ARSOLink t, dim_CustomerPaymentTerms dt
 SET ar.Dim_PaymentTermsId = ifnull(dt.dim_CustomerPaymentTermsId,1)
WHERE ar.dd_AccountingDocNo = t.dd_AccountingDocNo
AND ar.dd_AccountingDocItemNo = t.dd_AccountingDocItemNo
aND ar.dd_AssignmentNumber = t.dd_AssignmentNumber
AND ar.dim_companyid = t.dim_companyid
AND ar.dim_customerid = t.dim_customerid
AND dt.PaymentTermCode = t.t052_zterm
AND dt.RowIsCurrent = 1
AND (ar.Dim_PaymentTermsId = 1 OR  ar.Dim_PaymentTermsId IS NULL)
AND ar.dd_DueType = 'AR';

DROP TABLE IF EXISTS tmp_ARSOLink;

delete from NUMBER_FOUNTAIN where table_name = 'fact_exposure_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_exposure_temp',ifnull(max(fact_exposureid),1)
FROM fact_exposure_temp;

INSERT INTO fact_exposure_temp(fact_exposureid,
			  amt_ScheduleTotal,
			  amt_Tax,
                          dd_AccountingDocItemNo,
                          dd_AccountingDocNo,
                          dd_AssignmentNumber,
                          Dim_companyid,
                          dim_customerid,
			  dd_CreditRep,
			  dd_CreditLimit,
			  dim_CurrencyId,
			  Dim_CurrencyId_TRA,
			  Dim_CurrencyId_GBL,
			  amt_ExchangeRate,
			  amt_ExchangeRate_GBL,
			  dim_dateidactualgidate,
			  dim_dateidafsreqdelivery,
			  dim_DateidFixedvalue,
			  Dim_PaymentTermsId,
                          dd_DueType,
			  dd_SalesDocNo,
			  dd_SalesItemNo)

  SELECT ((SELECT ifnull(max_id, 1)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_exposure_temp') + row_number() over ()) fact_exposureid,
          a.*
  FROM (  SELECT  sum(so.amt_AfsNetValue),
	    	  sum(so.amt_Tax),
                   0,
                   'Not Set',
                   'Not Set',
                   so.dim_companyid,
                   so.dim_customerid,
		   ifnull(so.dd_CreditRep,'Not Set'),
		   ifnull(so.dd_CreditLimit,0),
		   so.dim_CurrencyId,
		   so.Dim_CurrencyId_TRA,
		   so.Dim_CurrencyId_GBL,
		   so.amt_ExchangeRate,
		   so.amt_ExchangeRate_GBL,
		   ifnull((CASE WHEN so.dim_dateidactualgi <> 1 THEN so.dim_dateidactualgi ELSE so.dim_DateIdAFSReqDelivery END),1),
		   so.dim_dateidafsreqdelivery,
		   ifnull(so.dim_DateidFixedvalue,1),
		   ifnull((CASE WHEN so.Dim_PaymentTermsId <> 1 THEN so.Dim_PaymentTermsId ELSE so.Dim_CustomerPaymentTermsId END),1),
		   'Sales',
		   so.dd_SalesDocNo,
		   so.dd_SalesItemNo
     FROM fact_salesorder so inner join dim_date dt on so.dim_DateIdAFSReqDelivery = dt.Dim_DateId 
     inner join dim_salesorderitemstatus sois on sois.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
     WHERE so.dim_AfsRejectionReasonId = 1 AND dt.DateValue > current_date 
     AND NOT EXISTS ( SELECT 1 FROM fact_exposure_temp e
                         WHERE e.dd_AccountingDocNo = 'Not Set'
			   AND e.dd_AccountingDocItemNo = 0
			   AND e.dd_AssignmentNumber = 'Not Set'
			   AND e.Dim_CustomerId = so.Dim_CustomerId
			   AND e.Dim_CompanyId = so.Dim_CompanyId
			   AND e.dim_dateidafsreqdelivery = so.dim_dateidafsreqdelivery
			   AND e.dim_DateidFixedvalue = so.dim_DateidFixedvalue
			   AND e.dim_dateidactualgidate = so.dim_dateidactualgi
			   and e.dd_SalesDocNo = so.dd_SalesDocNo
			   AND e.dd_SalesItemNo  = so.dd_SalesItemNo
			   AND e.dd_DueType = 'Sales')
	AND so.dd_SalesDocNo NOT IN ( SELECT distinct fs.dd_SalesDocNo from fact_salesorderdelivery fs where fs.dd_SalesDlvrDocNo <> 'Not Set')
	GROUP BY so.dim_customerid,so.dim_companyid,so.dd_CreditRep,so.dd_CreditLimit,so.dim_CurrencyId,so.Dim_CurrencyId_TRA,so.Dim_CurrencyId_GBL,so.amt_ExchangeRate,so.amt_ExchangeRate_GBL,(CASE WHEN so.dim_dateidactualgi <> 1 THEN so.dim_dateidactualgi ELSE so.dim_DateIdAFSReqDelivery END),so.dim_dateidafsreqdelivery,so.dim_DateidFixedvalue ,ifnull((CASE WHEN so.Dim_PaymentTermsId <> 1 THEN so.Dim_PaymentTermsId ELSE so.Dim_CustomerPaymentTermsId END),1),so.dd_SalesDocNo,so.dd_SalesItemNo) a;

delete from NUMBER_FOUNTAIN where table_name = 'fact_exposure_temp';
 
INSERT INTO NUMBER_FOUNTAIN
select 'fact_exposure_temp',ifnull(max(fact_exposureid),1)
FROM fact_exposure_temp;

INSERT INTO fact_exposure_temp(fact_exposureid,
			  amt_ScheduleTotal,
			  amt_Tax,
                          dd_AccountingDocItemNo,
                          dd_AccountingDocNo,
                          dd_AssignmentNumber,
                          Dim_companyid,
                          dim_customerid,
			  dd_CreditRep,
			  dd_CreditLimit,
			  dim_CurrencyId,
			  Dim_CurrencyId_TRA,
			  Dim_CurrencyId_GBL,
			  amt_ExchangeRate,
			  amt_ExchangeRate_GBL,
			  dim_dateidactualgidate,
			  dim_dateidafsreqdelivery,
			  dim_DateidFixedvalue,
			  Dim_PaymentTermsId,
                          dd_DueType,
			  dd_SalesDocNo,
			  dd_SalesItemNo)

  SELECT ((SELECT ifnull(max_id, 1)
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_exposure_temp') + row_number() over ()) fact_exposureid,
          a.*
  FROM (  SELECT  sum(so.amt_AfsNetValue),
	    	  sum(so.amt_Tax),
                   0,
                   'Not Set',
                   'Not Set',
                   so.dim_companyid,
                   so.dim_customerid,
		   ifnull(so.dd_CreditRep,'Not Set'),
		   ifnull(so.dd_CreditLimit,0),
		   so.dim_CurrencyId,
		   so.Dim_CurrencyId_TRA,
		   so.Dim_CurrencyId_GBL,
		   so.amt_ExchangeRate,
		   so.amt_ExchangeRate_GBL,
		   (CASE WHEN so.dim_dateidactualgi <> 1 THEN so.dim_dateidactualgi ELSE dt2.dim_dateId END),
		   dt2.dim_Dateid,
		   ifnull(so.dim_DateidFixedvalue,1),
		   ifnull((CASE WHEN so.Dim_PaymentTermsId <> 1 THEN so.Dim_PaymentTermsId ELSE so.Dim_CustomerPaymentTermsId END),1),
		   'Sales',
		   so.dd_SalesDocNo,
		   so.dd_SalesItemNo
     FROM fact_salesorder so inner join dim_date dt on so.dim_DateIdAFSReqDelivery = dt.Dim_DateId 
     inner join Dim_Company com on com.dim_Companyid = so.dim_companyid
     inner join dim_Date dt2 on dt2.datevalue = current_Date and dt2.companycode = com.companycode
     inner join dim_salesorderitemstatus sois on sois.dim_salesorderitemstatusid = so.dim_salesorderitemstatusid
     WHERE so.dim_AfsRejectionReasonId = 1 AND dt.DateValue <= current_date AND sois.OverallDeliveryStatus IN ('Not yet processed','Partially processed')
     AND NOT EXISTS ( SELECT 1 FROM fact_exposure_temp e
                         WHERE e.dd_AccountingDocNo = 'Not Set'
			   AND e.dd_AccountingDocItemNo = 0
			   AND e.dd_AssignmentNumber = 'Not Set'
			   AND e.Dim_CustomerId = so.Dim_CustomerId
			   AND e.Dim_CompanyId = so.Dim_CompanyId
			   AND e.dim_dateidafsreqdelivery = so.dim_dateidafsreqdelivery
			   AND e.dim_DateidFixedvalue = so.dim_DateidFixedvalue
			   AND e.dim_dateidactualgidate = so.dim_dateidactualgi
			   and e.dd_SalesDocNo = so.dd_SalesDocNo
			   AND e.dd_SalesItemNo  = so.dd_SalesItemNo
			   AND e.dd_DueType = 'Sales')
	AND so.dd_SalesDocNo NOT IN ( SELECT distinct fs.dd_SalesDocNo from fact_salesorderdelivery fs where fs.dd_SalesDlvrDocNo <> 'Not Set')
	GROUP BY so.dim_customerid,so.dim_companyid,so.dd_CreditRep,so.dd_CreditLimit,so.dim_CurrencyId,so.Dim_CurrencyId_TRA,so.Dim_CurrencyId_GBL,so.amt_ExchangeRate,so.amt_ExchangeRate_GBL,(CASE WHEN so.dim_dateidactualgi <> 1 THEN so.dim_dateidactualgi ELSE dt2.dim_dateId END),dt2.dim_dateId ,so.dim_DateidFixedvalue ,ifnull((CASE WHEN so.Dim_PaymentTermsId <> 1 THEN so.Dim_PaymentTermsId ELSE so.Dim_CustomerPaymentTermsId END),1),so.dd_SalesDocNo,so.dd_SalesItemNo) a;

DROP TABLE IF EXISTS tmp_NetDueDateCalc;
DROP TABLE IF EXISTS tmp_SalesOrder_ForNetDueDateCalc;

CREATE TABLE tmp_SalesOrder_ForNetDueDateCalc AS
SELECT distinct fso.dd_Salesdocno,fso.dd_Salesitemno, fso.Dim_CompanyId,fso.Dim_CustomerId,(CASE WHEN fso.Dim_DateIdFixedValue <> 1 tHEN Dim_DateIdFixedValue ELSE fso.Dim_DateIdAfsReqDelivery END) as Dim_DateIdDeliv,
fso.Dim_PaymentTermsId
FROM fact_exposure_temp fso WHERE fso.dd_DueType = 'Sales';

CREATE TABLE tmp_NetDueDateCalc AS
SELECT dd_Salesdocno,dd_salesitemno,dim_companyid,dim_customerid, dt.CompanyCode, fso.Dim_DateIdDeliv,t.t052_zterm, ( dt.datevalue + (interval '1' DAY) *(CASE WHEN ifnull(T052_ZTAG3,0) = 0 THEN (CASE WHEN ifnull(T052_ZTAG2,0) = 0 THEN ifnull(T052_ZTAG1,0) ELSE ifnull(T052_ZTAG2,0) END) ELSE ifnull(T052_ZTAG3,0) END)) AS DueDateValue
FROM tmp_SalesOrder_ForNetDueDateCalc fso,
     T052 t,
     dim_date dt,
     Dim_CustomerPaymentTerms pt
WHERE fso.Dim_DateIdDeliv = dt.Dim_DateId
AND fso.Dim_PaymentTermsId = pt.Dim_CustomerPaymentTermsId
AND t.T052_ZTERM = pt.PaymentTermCode
AND fso.Dim_DateIdDeliv > 1 group by dd_Salesdocno,dd_salesitemno,dim_companyid,dim_customerid,dt.companycode,fso.Dim_DateIdDeliv,dt.datevalue,t.t052_zterm,
(CASE WHEN ifnull(T052_ZTAG3,0) = 0 THEN (CASE WHEN ifnull(T052_ZTAG2,0) = 0 THEN ifnull(T052_ZTAG1,0) ELSE ifnull(T052_ZTAG2,0) END) ELSE ifnull(T052_ZTAG3,0) END);

UPDATE fact_exposure_temp e
    FROM
       tmp_NetDueDateCalc t,
       dim_Date dt
  SET e.Dim_DateIdNetDueDate = dt.Dim_DateId
WHERE e.Dim_CompanyId = t.Dim_CompanyId
  AND e.Dim_CustomerId = t.Dim_CustomerId
  AND e.dd_AccountingDocNo = 'Not Set'
  AND e.dd_AccountingDocItemNo = 0
  AND e.dd_AssignmentNumber = 'Not Set'
  AND e.dd_DueType = 'Sales'
  AND dt.DateValue = t.DueDateValue
  AND dt.companyCode = t.CompanyCode
  AND e.dd_SalesDocNo = t.dd_SalesDocNo
  AND e.dd_SalesItemNo = t.dd_SalesItemNo
  AND (t.Dim_DateIdDeliv = e.Dim_DateIdAFSReqDelivery OR t.Dim_DateIdDeliv = e.Dim_DateIdFixedValue);

DROP TABLE IF EXISTS tmp_SalesOrder_ForNetDueDateCalc;
DROP TABLE IF EXISTS tmp_NetDueDateCalc;

UPDATE fact_exposure_temp e
SET dd_FiscalYear = 0, dd_FiscalPeriod = 0, Dim_DateIdAccountingDocument = 1, Dim_DateIdPosting = 1
WHERE e.dd_AccountingDocNo = 'Not Set'
AND e.dd_AccountingDocItemNo = 0
AND e.dd_AssignmentNumber = 'Not Set'
AND e.dd_DueType = 'Sales';

UPDATE fact_exposure_temp e
SET amt_InLocalCurrency = amt_ScheduleTotal + amt_Tax
WHERE dd_DueType = 'Sales' and amt_InLocalCurrency = 0;

UPDATE fact_exposure_temp e
SET e.dim_DocumentTypeId = 1
WHERE  e.dim_DocumentTypeId  IS NULL;

UPDATE fact_exposure_temp e
SET e.dd_DisputeCaseID = 'Not Set'
WHERE e.dd_DisputeCaseID IS NULL;

UPDATE fact_exposure_temp e
SET e.Dim_PaymentReasonId = 1
WHERE e.Dim_PaymentReasonId IS NULL;

modify fact_exposure to truncated;

call vectorwise(combine 'fact_exposure+fact_exposure_temp');