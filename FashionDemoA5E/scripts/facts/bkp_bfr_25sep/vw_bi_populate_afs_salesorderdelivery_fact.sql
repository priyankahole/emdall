
/* ##################################################################################################################*/
/*   Change History */
/*   Date            By        Version           Desc */
/*   25 Aug 2013     Lokesh      1.33            Performance changes */
/*   29 Aug 2013     Shanthi     1.36            Fixed the amt_Salessubtotal3unitprice                                 */
/* #################################################################################################################### */




select 'START OF PROC VW_bi_populate_afs_salesorderdelivery_fact',TIMESTAMP(LOCAL_TIMESTAMP);

/*##########PART 1 Starts###############################	 */

Drop table if exists cursor_tbl1_721;
Drop table if exists cursor_tbl2_721;
Drop table if exists variable_tbl_721;

Create table variable_tbl_721( pDeltaChangesFlag varchar(10) null);

Create table cursor_tbl1_721 (
v_SalesDocNo      varchar(50) null,
v_SalesItemNo     integer null,
v_TotConfQty     decimal(18,4) null,
v_TotSchedQty     decimal(18,4) null,
v_GIDateMax       date null,
v_SchedQty        decimal(18,4) null,
v_ConfQty        decimal(18,4) null,
v_SchedNo         integer null,
v_Count           integer null,
iflag integer default 0);

Create table cursor_tbl2_721 (
v_SalesDocNo      varchar(50) null,
v_SalesItemNo     integer null,
v_TotConfQty     decimal(18,4) null,
v_TotSchedQty     decimal(18,4) null,
v_GIDateMax       date null,
v_SchedQty        decimal(18,4) null,
v_ConfQty        decimal(18,4) null,
v_SchedNo         integer null,
v_Count           integer null,
iflag integer default 0);


 /*If pDeltaChangesFlag <> 'true', truncate all rows in fact_salesorderdelivery */
 drop table if exists tmp_delete_fact_salesorderdelivery;
 create table tmp_delete_fact_salesorderdelivery
 as
 select f.*
 from fact_salesorderdelivery f, systemproperty s
 where s.property = 'process.delta.salesorderdelivery'
 and s.property_value <> 'true';

 call vectorwise(combine 'fact_salesorderdelivery-tmp_delete_fact_salesorderdelivery');

 drop table tmp_delete_fact_salesorderdelivery;

/* LK 30 Mar - This delete would run in any case. If delta.salesorderdelivery was false, f_sod would anyway have 0 rows, otherwise this SQL 
would actually delete rows */
DELETE FROM fact_salesorderdelivery
WHERE exists (select 1 from CDPOS_LIKP a where a.CDPOS_OBJECTID = dd_SalesDlvrDocNo AND a.CDPOS_CHNGIND = 'D' AND a.CDPOS_TABNAME = 'LIKP');

call vectorwise(combine 'fact_salesorderdelivery');


INSERT INTO variable_tbl_721( pDeltaChangesFlag )
SELECT ifnull(property_value,'true') from systemproperty
Where  property = 'process.delta.salesorderdelivery';

call vectorwise(combine 'variable_tbl_721');
 select 'A1',TIMESTAMP(LOCAL_TIMESTAMP);
/*call bi_populate_sod_headerstatus_dim()	--This is not called in the latest version of proc in mysql ( LK : 31 Mar ) */
/*call bi_populate_sod_itemstatus_dim() 	--This is not called in the latest version of proc in mysql ( LK : 31 Mar ) */

drop table if exists delete_staging_721;

/* LK 26 Aug: Slow query. Tuned now. */

DROP TABLE IF EXISTS tmp_delstg721_fact_salesorderdelivery_ins;
CREATE TABLE tmp_delstg721_fact_salesorderdelivery_ins
AS
SELECT DISTINCT dd_SalesDlvrDocNo,dd_SalesDlvrItemNo
FROM fact_salesorderdelivery sod,LIKP_LIPS 
WHERE dd_SalesDlvrDocNo = LIKP_VBELN;

DROP TABLE IF EXISTS tmp_delstg721_fact_salesorderdelivery_del;
CREATE TABLE tmp_delstg721_fact_salesorderdelivery_del
AS
select DISTINCT LIKP_VBELN dd_SalesDlvrDocNo,LIPS_POSNR dd_SalesDlvrItemNo
FROM LIKP_LIPS;


CALL VECTORWISE(COMBINE 'tmp_delstg721_fact_salesorderdelivery_ins-tmp_delstg721_fact_salesorderdelivery_del');


Create table delete_staging_721 as
Select distinct sod.* from fact_salesorderdelivery sod,variable_tbl_721,tmp_delstg721_fact_salesorderdelivery_ins ins
WHERE sod.dd_SalesDlvrDocNo = ins.dd_SalesDlvrDocNo 
AND sod.dd_SalesDlvrItemNo = ins.dd_SalesDlvrItemNo
AND pDeltaChangesFlag='true';
call vectorwise(combine 'delete_staging_721');

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');
drop table if exists delete_staging_721;

 select 'A',TIMESTAMP(LOCAL_TIMESTAMP);


Create table delete_staging_721 as
Select distinct fact_salesorderdelivery.* from fact_salesorderdelivery,variable_tbl_721,LIKP_LIPS a
WHERE dd_SalesDlvrDocNo = LIKP_VBELN and dd_SalesDlvrItemNo = LIPS_POSNR
AND  pDeltaChangesFlag='true';
call vectorwise(combine 'delete_staging_721');

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');
drop table if exists delete_staging_721;



DROP TABLE IF EXISTS tmp_delstg721_fso_ins;
CREATE TABLE tmp_delstg721_fso_ins
AS
SELECT DISTINCT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
FROM fact_salesorderdelivery;

DROP TABLE IF EXISTS tmp_delstg721_fso_del;
CREATE TABLE tmp_delstg721_fso_del
AS
SELECT DISTINCT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo
FROM fact_salesorder;

CALL VECTORWISE(COMBINE 'tmp_delstg721_fso_ins-tmp_delstg721_fso_del');

Create table delete_staging_721 as
Select distinct fact_salesorderdelivery.* from fact_salesorderdelivery,variable_tbl_721,tmp_delstg721_fso_ins f
WHERE fact_salesorderdelivery.dd_SalesDocNo = f.dd_SalesDocNo 
and  fact_salesorderdelivery.dd_SalesItemNo = f.dd_SalesItemNo
and  fact_salesorderdelivery.dd_ScheduleNo = f.dd_ScheduleNo
AND   pDeltaChangesFlag='true';
							

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');
drop table if exists delete_staging_721;

Create table delete_staging_721 as
Select fact_salesorderdelivery.* from fact_salesorderdelivery,variable_tbl_721
WHERE dd_SalesDlvrDocNo = 'Not Set'
AND  pDeltaChangesFlag='true';
call vectorwise(combine 'delete_staging_721');

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');
drop table if exists delete_staging_721;
 
Create table delete_staging_721 as
Select fact_salesorderdelivery.* from fact_salesorderdelivery,variable_tbl_721
where pDeltaChangesFlag<>'true';

call vectorwise (combine 'fact_salesorderdelivery-delete_staging_721');

drop table if exists delete_staging_721;

Drop table if exists max_holder_721;
Create table max_holder_721(maxid)
as
Select ifnull(max(fact_salesorderdeliveryid),0)
from fact_salesorderdelivery;
call vectorwise(combine 'max_holder_721');
call vectorwise(combine 'likp_lips');
select 'Insert 1 on fact_salesorderdelivery',TIMESTAMP(LOCAL_TIMESTAMP);

drop table if exists LIKP_LIPS_sub00;
Create table LIKP_LIPS_sub00 as Select * from  LIKP_LIPS where LIPS_LFIMG  > 0 ;

\i /db/schema_migration/bin/wrapper_optimizedb.sh LIKP_LIPS_sub00;
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorderdelivery;

Drop table if exists fact_salesorderdelivery_sub001;
Create table fact_salesorderdelivery_sub001 as 
Select *,
varchar(null,20) companycode_upd,
ansidate(null) likp_erdat_upd,
varchar(null,4) likp_fkarv_upd,
ansidate(null) likp_fkdat_upd,
ansidate(null) likp_kodat_upd,
varchar(null,10) likp_kunag_upd,
varchar(null,10) likp_kunnr_upd,
ansidate(null) likp_lddat_upd,
ansidate(null) likp_lfdat_upd,
varchar('',10) likp_vbeln_upd,
varchar(null,2) likp_vtwiv_upd,
ansidate(null) likp_wadat_upd,
ansidate(null) likp_wadat_ist_upd,
varchar(null,4) lips_kokrs_upd,
varchar(null,4) lips_lgort_upd,
varchar(null,18) lips_matnr_upd,
ansidate(null) lips_mbdat_upd,
integer(null) lips_posnr_upd,
varchar(null,18) lips_prodh_upd,
varchar(null,4) lips_werks_upd
from fact_salesorderdelivery where 1=2;

INSERT INTO fact_salesorderdelivery_sub001(
fact_salesorderdeliveryid,dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdSODocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    Dim_DateIdRejection,
				    dd_CustomerMaterial,
					companycode_upd,
					likp_erdat_upd,
					likp_fkarv_upd,
					likp_fkdat_upd,
					likp_kodat_upd,
					likp_kunag_upd,
					likp_kunnr_upd,
					likp_lddat_upd,
					likp_lfdat_upd,
					likp_vbeln_upd,
					likp_vtwiv_upd,
					likp_wadat_upd,
					likp_wadat_ist_upd,
					lips_kokrs_upd,
					lips_lgort_upd,
					lips_matnr_upd,
					lips_mbdat_upd,
					lips_posnr_upd,
					lips_prodh_upd,
					lips_werks_upd
					)
	   SELECT maxid + row_number() over(order by LIPS_VGBEL,LIPS_VGPOS,LIPS_J_3AETENR) fact_salesorderdeliveryid,
	  LIPS_VGBEL dd_SalesDocNo,
          LIPS_VGPOS dd_SalesItemNo,
          LIPS_J_3AETENR dd_ScheduleNo,
          LIKP_VBELN dd_SalesDlvrDocNo,
          LIPS_POSNR dd_SalesDlvrItemNo,
          ifnull(lips_bwart, 'Not Set') dd_MovementType,
          f.dd_AfsStockCategory,
          ifnull(f.dd_CustomerPONo,'Not Set'),
          f.dd_AfsStockType,
          LIPS_LFIMG ct_QtyDelivered,
          LIPS_WAVWR amt_Cost_DocCurr,
          Decimal((LIPS_WAVWR
           * Decimal((CASE
                 WHEN f.amt_ExchangeRate < 0
                 THEN
                    (1 / (case when (-1 * f.amt_ExchangeRate)= 0 then null else (-1 * f.amt_ExchangeRate) end))
                 ELSE
                    f.amt_ExchangeRate
              END),18,4)),18,4)
             amt_Cost,
          f.amt_SubTotal3,
          f.amt_SubTotal4,
          f.amt_AfsNetPrice,
          f.amt_AfsNetValue,
          f.amt_BasePrice,
          f.amt_CustomerExpectedPrice,
          ifnull(f.amt_DicountAccrualNetPrice,0),
          f.ct_AfsOpenQty,
          lips_vbeaf ct_FixedProcessDays,
          lips_vbeav ct_ShipProcessDays,
          (CASE WHEN (f.ct_ScheduleQtySalesUnit - LIPS_LFIMG >= 0 ) THEN  LIPS_LFIMG ELSE f.ct_ScheduleQtySalesUnit END )ct_ScheduleQtySalesUnit,
          (CASE WHEN (f.ct_ConfirmedQty - LIPS_LFIMG >= 0 ) THEN  LIPS_LFIMG ELSE f.ct_ConfirmedQty END ) ct_ConfirmedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
             1 Dim_DateidPlannedGoodsIssue,
             1 Dim_DateidActualGoodsIssue,
             1 Dim_DateidDeliveryDate,
             1 Dim_DateidLoadingDate,
             1 Dim_DateidPickingDate,
             1 Dim_DateidDlvrDocCreated,
             1 Dim_DateidMatlAvail,
	 f.Dim_CustomerID Dim_CustomeridSoldTo,
                 1 Dim_CustomeridShipTo,
             1 Dim_Partid,
          pl.Dim_Plantid,
             1 Dim_StorageLocationid,
                 1 Dim_ProductHierarchyid,
                 1 Dim_DeliveryHeaderStatusid,
             1 Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
          f.Dim_DateidMtrlAvail,
          f.Dim_Currencyid,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1  Dim_ProfitCenterid,
                 1 Dim_ControllingAreaId,
             1 Dim_DateidBillingDate,
                 1 Dim_BillingDocumentTypeid,
             f.Dim_DistributionChannelId Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId,
          Dim_AfsRejectionReasonId,
          Dim_SalesOrderRejectReasonid,
             1 Dim_DateidActualGI_Original,
          LIPS_J_3ADEPM dd_AfsDepartment,
          f.ct_AfsAllocationRQty ct_ReservedQty,
          f.ct_AfsAllocationFQty ct_FixedQty,
          (CASE
              WHEN f.ct_ConfirmedQty > 0
              THEN
                 Decimal((f.amt_SubTotal3 / (case when f.ct_ConfirmedQty = 0 then null else f.ct_ConfirmedQty end)),18,4)
              ELSE
                 0
           END)
             amt_SalesSubTotal3UnitPrice,
	    f.Dim_DateIdSODocument Dim_DateIdSODocument,
	    f.Dim_DateIdSOCreated Dim_DateIdSOCreated,
	    f.dd_CreatedBy dd_SDCreatedBy,
	    f.Dim_DeliveryblockId,
	    f.ct_AfsUnallocatedQty,
	    ifnull(f.Dim_DateIdSOItemChangedOn,1),
            f.Dim_ShippingConditionId,
	    f.dd_AfsAllocationGroupNo,
	    f.Dim_SalesOrderHeaderStatusId,
	    f.Dim_SalesOrderItemStatusId,
	    f.ct_AfsTotalDrawn,
	    ifnull(f.Dim_DateidSchedDelivery,1),
	    ifnull(f.Dim_DateIdRejection,1),
	    ifnull(f.dd_CustomerMaterialNo,'Not Set'),
	   companycode,
	likp_erdat,
	likp_fkarv,
	likp_fkdat,
	likp_kodat,
	likp_kunag,
	likp_kunnr,
	likp_lddat,
	likp_lfdat,
	likp_vbeln,
	likp_vtwiv,
	likp_wadat,
	likp_wadat_ist,
	lips_kokrs,
	lips_lgort,
	lips_matnr,
	lips_mbdat,
	lips_posnr,
	lips_prodh,
	lips_werks
     FROM max_holder_721,LIKP_LIPS_sub00
          INNER JOIN fact_salesorder f
             ON     f.dd_SalesDocNo = LIPS_VGBEL
                AND f.dd_SalesItemNo = LIPS_VGPOS
                AND f.dd_ScheduleNo = LIPS_J_3AETENR
          INNER JOIN Dim_Plant pl
             ON LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
    WHERE f.dd_ItemRelForDelv = 'X' ;
 select 'B',TIMESTAMP(LOCAL_TIMESTAMP);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorderdelivery_sub001;

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidPlannedGoodsIssue = ifnull( (SELECT Dim_Dateid
					FROM Dim_Date dd
				       WHERE dd.DateValue = fss.likp_wadat_upd
					     AND dd.CompanyCode = fss.CompanyCode_upd),
				     1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidActualGoodsIssue = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_wadat_ist_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidDeliveryDate = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_lfdat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidLoadingDate = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_lddat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidPickingDate = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_kodat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set  Dim_DateidDlvrDocCreated = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_erdat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidMatlAvail = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.lips_mbdat_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_CustomeridSoldTo = ifnull((SELECT Dim_CustomerID
                    FROM Dim_Customer cust
                   WHERE cust.CustomerNumber = fss.likp_kunag_upd),
                 fss.Dim_CustomeridSoldTo);


Update fact_salesorderdelivery_sub001 fss
Set Dim_CustomeridShipTo = ifnull((SELECT Dim_CustomerID
                    FROM Dim_Customer cust
                   WHERE cust.CustomerNumber = fss.likp_kunnr_upd),
                 1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_Partid = ifnull(
             (SELECT dim_partid
                FROM dim_part dp
               WHERE dp.PartNumber = fss.lips_matnr_upd AND dp.Plant = fss.lips_werks_upd),
             1);


Update fact_salesorderdelivery_sub001 fss
Set Dim_StorageLocationid = ifnull(
             (SELECT Dim_StorageLocationid
                FROM Dim_StorageLocation sl
               WHERE sl.LocationCode = fss.lips_lgort_upd AND sl.plant = fss.lips_werks_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_ProductHierarchyid = ifnull((SELECT Dim_ProductHierarchyid
                    FROM Dim_ProductHierarchy ph
                   WHERE ph.ProductHierarchy = fss.lips_prodh_upd),
                 1);



Update fact_salesorderdelivery_sub001 fss
Set Dim_DeliveryHeaderStatusid = ifnull((SELECT Dim_SalesOrderHeaderStatusid
                    FROM Dim_SalesOrderHeaderStatus sohs
                   WHERE sohs.SalesDocumentNumber = fss.LIKP_VBELN_upd),
                 1);


Update fact_salesorderdelivery_sub001 fss
Set Dim_DeliveryItemStatusid = ifnull(
             (SELECT Dim_SalesOrderItemStatusid
                FROM Dim_SalesOrderItemStatus sois
               WHERE sois.SalesDocumentNumber = fss.LIKP_VBELN_upd
                     AND sois.SalesItemNumber = fss.LIPS_POSNR_upd), 1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_ControllingAreaId = ifnull((SELECT ca.Dim_ControllingAreaid
                    FROM Dim_ControllingArea ca
                   WHERE ca.ControllingAreaCode = fss.LIPS_KOKRS_upd),
                 1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidBillingDate = ifnull(
             (SELECT dt.Dim_DateID
                FROM dim_Date dt
               WHERE dt.DateValue = fss.LIKP_FKDAT_upd
                     AND dt.CompanyCode = fss.CompanyCode_upd),
             1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_BillingDocumentTypeid = ifnull((SELECT dim_billingdocumenttypeid
                    FROM dim_billingdocumenttype bdt
                   WHERE bdt.Type = fss.LIKP_FKARV_upd AND bdt.RowIsCurrent = 1),
                 1);

Update fact_salesorderdelivery_sub001 fss
Set Dim_DistributionChannelId = ifnull(
             (SELECT Dim_DistributionChannelId
                FROM dim_distributionchannel
               WHERE DistributionChannelCode = fss.LIKP_VTWIV_upd
                     AND RowIsCurrent = 1),
             fss.Dim_DistributionChannelId) ;

Update fact_salesorderdelivery_sub001 fss
Set Dim_DateidActualGI_Original = ifnull(
             (SELECT Dim_Dateid
                FROM Dim_Date dd
               WHERE dd.DateValue = fss.likp_wadat_ist_upd
                     AND dd.CompanyCode = fss.CompanyCode_upd),
             1);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_salesorderdelivery_sub001;
 select 'C',TIMESTAMP(LOCAL_TIMESTAMP);

INSERT INTO fact_salesorderdelivery(
					fact_salesorderdeliveryid,
					dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
				 Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
					 Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
                                    Dim_DateIdSODocument,
                                    Dim_DateIdSOCreated,
                                    dd_SDCreatedBy,
                                    Dim_DeliveryblockId,
                                    ct_AfsUnallocatedQty,
                                    Dim_DateIdSOItemChangedOn,
                                    Dim_ShippingConditionId,
                                    dd_AfsAllocationGroupNo,
                                    Dim_SalesOrderHeaderStatusId,
                                    Dim_SalesOrderItemStatusId,
                                    ct_AfsTotalDrawn,
                                    Dim_DateidSchedDelivery,
                                    Dim_DateIdRejection,
                                    dd_CustomerMaterial)
Select fact_salesorderdeliveryid,
					dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
				 Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateidBillingDate,
                                    Dim_BillingDocumentTypeid,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
					 Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
                                    Dim_DateIdSODocument,
                                    Dim_DateIdSOCreated,
                                    dd_SDCreatedBy,
                                    Dim_DeliveryblockId,
                                    ct_AfsUnallocatedQty,
                                    Dim_DateIdSOItemChangedOn,
                                    Dim_ShippingConditionId,
                                    dd_AfsAllocationGroupNo,
                                    Dim_SalesOrderHeaderStatusId,
                                    Dim_SalesOrderItemStatusId,
                                    ct_AfsTotalDrawn,
                                    Dim_DateidSchedDelivery,
                                    Dim_DateIdRejection,
                                    dd_CustomerMaterial
From fact_salesorderdelivery_sub001 ;
 select 'D',TIMESTAMP(LOCAL_TIMESTAMP);

call vectorwise(combine 'fact_salesorderdelivery');
drop table if exists fact_salesorderdelivery_sub001;

drop table if exists LIKP_LIPS_sub00;
drop table if exists delete_staging_721;
drop table if exists Dim_ProfitCenter_721;
drop table if exists Dim_ProfitCenter_upd_721;

Create table Dim_ProfitCenter_721 as select first 0 * from Dim_ProfitCenter ORDER BY ValidTo ASC;
call vectorwise(combine 'Dim_ProfitCenter_721');

Create table Dim_ProfitCenter_upd_721 as Select 
maxid+row_number() over(order by LIPS_VGBEL,LIPS_VGPOS,LIPS_J_3AETENR) iid,LIPS_VGBEL,LIPS_VGPOS,LIPS_J_3AETENR,LIPS_PRCTR,LIPS_KOKRS,LIKP_WADAT_IST,LIKP_WADAT,1 Dim_ProfitCenterid
 FROM max_holder_721,LIKP_LIPS
          INNER JOIN fact_salesorder f
             ON     f.dd_SalesDocNo = LIPS_VGBEL
                AND f.dd_SalesItemNo = LIPS_VGPOS
                AND f.dd_ScheduleNo = LIPS_J_3AETENR
          INNER JOIN Dim_Plant pl
             ON LIPS_WERKS = pl.plantcode AND pl.RowIsCurrent = 1
    WHERE f.dd_ItemRelForDelv = 'X' AND LIPS_LFIMG > 0;
call vectorwise(combine 'Dim_ProfitCenter_upd_721');
 select 'C2',TIMESTAMP(LOCAL_TIMESTAMP);


Update Dim_ProfitCenter_upd_721
set Dim_ProfitCenterid= ifnull(
             (SELECT pc.Dim_ProfitCenterid
                FROM Dim_ProfitCenter_721 pc
               WHERE     pc.ProfitCenterCode = LIPS_PRCTR
                     AND pc.ControllingArea = LIPS_KOKRS
                     AND pc.ValidTo >= ifnull(LIKP_WADAT_IST, LIKP_WADAT)
                     AND pc.RowIsCurrent = 1),
             1);
call vectorwise(combine 'Dim_ProfitCenter_upd_721');

Update fact_salesorderdelivery f
from Dim_ProfitCenter_upd_721 a
Set f.Dim_ProfitCenterid=a.Dim_ProfitCenterid
where f.fact_salesorderdeliveryid=iid
AND f.Dim_ProfitCenterid <> a.Dim_ProfitCenterid;

call vectorwise(combine 'fact_salesorderdelivery');

/*##########PART 1 Ends###############################	 */

/*# LK : Working perfectly till here in VW	 */

/*##########PART 2 Starts###############################	 */

/*Moved the cursor tables here as this should be happening after the true/false logic deletes data from f_sod */

DROP TABLE IF EXISTS tmp_perf_cursor_tbl1_721;
CREATE TABLE tmp_perf_cursor_tbl1_721
AS
SELECT sod.dd_SalesDocNo ,sod.dd_SalesItemNo, sod.dd_ScheduleNo,
	   SUM(sod.ct_confirmedqty) sum_ct_confirmedqty
FROM fact_salesorderdelivery sod
GROUP BY  sod.dd_SalesDocNo ,sod.dd_SalesItemNo, sod.dd_ScheduleNo;
	   

Insert into cursor_tbl1_721(v_SalesDocNo,v_SalesItemNo,v_SchedNo)
select  first 0 so.dd_Salesdocno, 
	so.dd_Salesitemno, 
	so.dd_scheduleno 
from fact_salesorder so,tmp_perf_cursor_tbl1_721 sod
where sod.dd_SalesDocNo = so.dd_salesdocno and sod.dd_SalesItemNo = so.dd_salesitemno and sod.dd_ScheduleNo = so.dd_Scheduleno
AND so.ct_ConfirmedQty > sod.sum_ct_confirmedqty
ORDER by so.dd_SalesDocNo, so.dd_SalesItemNo, so.dd_ScheduleNo;

call vectorwise(combine 'cursor_tbl1_721');
 select 'D2',TIMESTAMP(LOCAL_TIMESTAMP);
 
 DROP TABLE IF EXISTS tmp_perf_cursor_tbl2_721_confqty;
CREATE TABLE tmp_perf_cursor_tbl2_721_confqty
AS
SELECT sod.dd_SalesDocNo ,sod.dd_SalesItemNo, sod.dd_ScheduleNo,
	   SUM(sod.ct_ScheduleQtySalesUnit) sum_ct_ScheduleQtySalesUnit
FROM fact_salesorderdelivery sod
GROUP BY  sod.dd_SalesDocNo ,sod.dd_SalesItemNo, sod.dd_ScheduleNo;
	   

Insert into cursor_tbl2_721(v_SalesDocNo,v_SalesItemNo,v_SchedNo)
select first 0 so.dd_Salesdocno, 
	so.dd_Salesitemno,
	so.dd_scheduleno 
from fact_salesorder so, tmp_perf_cursor_tbl2_721_confqty sod
WHERE sod.dd_SalesDocNo = so.dd_salesdocno and sod.dd_SalesItemNo = so.dd_salesitemno and sod.dd_ScheduleNo = so.dd_Scheduleno
AND so.ct_ScheduleQtySalesUnit > sod.sum_ct_ScheduleQtySalesUnit
ORDER by so.dd_SalesDocNo, so.dd_SalesItemNo, so.dd_ScheduleNo;
call vectorwise(combine 'cursor_tbl2_721');

update cursor_tbl1_721                    
set v_ConfQty = ifnull((select f.ct_ConfirmedQty
                              from fact_salesorder f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo 
                                    and f.dd_ItemRelForDelv = 'X'),0);
call vectorwise(combine 'cursor_tbl1_721');
                                   
update cursor_tbl1_721 
set v_GIDateMax = ifnull((select max(dt.DateValue) from fact_salesorderdelivery fd 
                                          INNER JOIN dim_Date dt ON fd.Dim_DateidPlannedGoodsIssue = dt.dim_dateid
                            where fd.dd_SalesDocNo = v_SalesDocNo and fd.dd_SalesItemNo = v_SalesItemNo and fd.dd_ScheduleNo = v_SchedNo), ANSIDATE(current_date));

update cursor_tbl1_721
set v_TotConfQty =   ifnull((select SUM(f.ct_ConfirmedQty)
                              from fact_salesorderdelivery f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo),0);

call vectorwise(combine 'cursor_tbl1_721');
Update cursor_tbl1_721
Set v_ConfQty = v_ConfQty - v_TotConfQty
Where (v_ConfQty - v_TotConfQty) > 0
AND v_ConfQty <> (v_ConfQty - v_TotConfQty);

call vectorwise(combine 'cursor_tbl1_721');

  UPDATE fact_salesorderdelivery sod
From cursor_tbl1_721, dim_date dt 
    SET sod.ct_ConfirmedQty =  sod.ct_ConfirmedQty + v_ConfQty
  WHERE sod.dd_SalesDocNo = v_SalesDocNo and sod.dd_SalesItemNo = v_SalesItemNo and sod.dd_ScheduleNo = v_SchedNo
  AND dt.Datevalue = v_GIDateMax AND dt.dim_Dateid = sod.dim_DateIdPlannedGoodsIssue
AND  sod.ct_ConfirmedQty <> ( sod.ct_ConfirmedQty + v_ConfQty);
call vectorwise(combine 'fact_salesorderdelivery');

 select 'E',TIMESTAMP(LOCAL_TIMESTAMP);
/*##########PART 2 Ends###############################	 */

/*##########PART 3 Starts###############################	 */
  
Update cursor_tbl2_721
    set v_SchedQty = ifnull((select f.ct_ScheduleQtySalesUnit
                              from fact_salesorder f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo 
                                    and f.dd_ItemRelForDelv = 'X'),0);

call vectorwise(combine 'cursor_tbl2_721');

Update cursor_tbl2_721
    set v_GIDateMax = ifnull((select max(dt.DateValue) from fact_salesorderdelivery fd 
                                          INNER JOIN dim_Date dt ON fd.Dim_DateidPlannedGoodsIssue = dt.dim_dateid
                            where fd.dd_SalesDocNo = v_SalesDocNo and fd.dd_SalesItemNo = v_SalesItemNo and fd.dd_ScheduleNo = v_SchedNo), ANSIDATE(current_date));

call vectorwise(combine 'cursor_tbl2_721');
Update cursor_tbl2_721
set v_TotSchedQty =   ifnull((select SUM(f.ct_ScheduleQtySalesUnit)
                              from fact_salesorderdelivery f
                              where f.dd_SalesDocNo = v_SalesDocNo and f.dd_SalesItemNo = v_SalesItemNo
                                    and f.dd_ScheduleNo = v_SchedNo),0);

call vectorwise(combine 'cursor_tbl2_721');

Update cursor_tbl2_721
Set v_SchedQty = v_SchedQty - v_TotSchedQty
where (v_SchedQty - v_TotSchedQty) > 0
AND  v_SchedQty <> (v_SchedQty - v_TotSchedQty);

call vectorwise(combine 'cursor_tbl2_721');

  UPDATE fact_salesorderdelivery sod
From cursor_tbl2_721, dim_date dt 
    SET sod.ct_ScheduleQtySalesUnit =  sod.ct_ScheduleQtySalesUnit + v_SchedQty
  WHERE sod.dd_SalesDocNo = v_SalesDocNo and sod.dd_SalesItemNo = v_SalesItemNo and sod.dd_ScheduleNo = v_SchedNo
  AND dt.Datevalue = v_GIDateMax AND
  dt.dim_Dateid = sod.dim_DateIdPlannedGoodsIssue
AND  sod.ct_ScheduleQtySalesUnit <> (sod.ct_ScheduleQtySalesUnit + v_SchedQty);
  
call vectorwise(combine 'fact_salesorderdelivery');

/*##########PART 3 Ends###############################	 */

/*##########PART 4 Starts###############################	 */

UPDATE fact_salesorderdelivery sod
From  VBFA v
  SET sod.ct_QtyDelivered = CASE WHEN (sod.ct_QtyDelivered - v.vbfa_rfmng) < 0 THEN 0 ELSE (sod.ct_QtyDelivered - v.vbfa_rfmng) END
  WHERE sod.dd_SalesDlvrDocNo = v.vbfa_vbelv AND sod.dd_SalesDlvrItemNo = v.vbfa_posnv
      AND v.vbfa_bwart = '602' AND sod.ct_QtyDelivered > 0;
call vectorwise(combine 'fact_salesorderdelivery');

/* ##################################################################################################################*/
/* */
/*   Script         : bi_populate_so_shipment */
/*   Author         : Ashu */
/*   Created On     : 22 Jan 2013 */
/* */
/* */
/*   Description    : Stored Proc bi_populate_so_shipment from MySQL to Vectorwise syntax */
/* */
/*   Change History */
/*   Date            By        Version           Desc */
/*   22 Jan 2013     Ashu      1.0               Existing code migrated to Vectorwise */
/*   20 Mar 2013     Lokesh	   1.1               Minor changes in logging, comments etc. Removed join with likp_lips  */
/*						 	to sync up with the current version on prod ( changes done by shanthi )  */
/* #################################################################################################################### */

select current_time;

DROP TABLE IF EXISTS tmp_ct_QtyDeliveredsummax;
CREATE TABLE tmp_ct_QtyDeliveredsummax
AS
SELECT dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo,sum(f.ct_QtyDelivered) sum_ct_QtyDelivered,max(f.Dim_DateidDeliveryDate)  max_Dim_DateidDeliveryDate,
 max(f.Dim_DateidActualGoodsIssue) max_Dim_DateidActualGoodsIssue,
  min(f.Dim_DateidDlvrDocCreated) min_Dim_DateidDlvrDocCreated
FROM fact_salesorderdelivery f, dim_salesorderitemstatus sois 
where f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid
and sois.GoodsMovementStatus <> 'Not yet processed'
GROUP BY dd_SalesDocNo,dd_SalesItemNo,dd_ScheduleNo;

UPDATE fact_salesorder
SET ct_DeliveredQty = 0
WHERE ct_DeliveredQty <> 0;

UPDATE fact_salesorder so
FROM tmp_ct_QtyDeliveredsummax f
 SET ct_DeliveredQty = sum_ct_QtyDelivered
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
and f.dd_SalesItemNo = so.dd_SalesItemNo
and f.dd_ScheduleNo = so.dd_ScheduleNo;

call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder
SET Dim_DateidShipmentDelivery = 1
WHERE Dim_DateidShipmentDelivery <> 1;

UPDATE fact_salesorder so
FROM tmp_ct_QtyDeliveredsummax f
 SET Dim_DateidShipmentDelivery = max_Dim_DateidDeliveryDate
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
and f.dd_SalesItemNo = so.dd_SalesItemNo
and f.dd_ScheduleNo = so.dd_ScheduleNo;


call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder
SET Dim_DateidActualGI = 1
WHERE Dim_DateidActualGI <> 1;

UPDATE fact_salesorder so
FROM tmp_ct_QtyDeliveredsummax f
 SET Dim_DateidActualGI = max_Dim_DateidActualGoodsIssue
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
and f.dd_SalesItemNo = so.dd_SalesItemNo
and f.dd_ScheduleNo = so.dd_ScheduleNo;


call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder so
SET Dim_CustomeridShipTo = ifnull(( select Dim_CustomeridShipto from fact_Salesorderdelivery f,
                                   dim_salesorderitemstatus sois 
                           where  f.Dim_DeliveryItemStatusid = sois.Dim_SalesOrderItemStatusid AND f.dd_SalesDocNo = so.dd_SalesDocNo
                                and f.dd_SalesItemNo = so.dd_SalesItemNo
                                and f.dd_ScheduleNo = so.dd_ScheduleNo
                                and sois.GoodsMovementStatus <> 'Not yet processed' ),so.Dim_CustomerID)
WHERE so.Dim_CustomeridShipTo = 1;

call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorder
SET Dim_DateidDlvrDocCreated = 1
WHERE Dim_DateidDlvrDocCreated <> 1;

UPDATE fact_salesorder so
FROM tmp_ct_QtyDeliveredsummax f
 SET Dim_DateidDlvrDocCreated = min_Dim_DateidDlvrDocCreated
WHERE f.dd_SalesDocNo = so.dd_SalesDocNo
and f.dd_SalesItemNo = so.dd_SalesItemNo
and f.dd_ScheduleNo = so.dd_ScheduleNo;

call vectorwise(combine 'fact_salesorder');

DROP TABLE IF EXISTS tmp1_soshipmt;
CREATE TABLE tmp1_soshipmt
AS
SELECT dd_SalesDocNo,dd_SalesItemNo
FROM fact_salesorderdelivery sod inner join VBFA v on sod.dd_SalesDlvrDocNo = v.vbfa_vbelv and sod.dd_SalesDlvrItemNo = v.vbfa_posnv
WHERE v.vbfa_bwart = '602'
and not exists (select 1 from VBFA v1 where v1 .vbfa_vbelv = v.vbfa_vbelv and v1.vbfa_posnv = v.vbfa_posnv
                                      and v1.vbfa_bwart = '601' and v1.vbfa_erdat > v.vbfa_erdat);

call vectorwise(combine 'tmp1_soshipmt');

 select 'F',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE fact_salesorder so
FROM dim_salesorderitemstatus s
SET ct_DeliveredQty = so.ct_ConfirmedQty
WHERE so.Dim_SalesOrderItemStatusid = s.Dim_SalesOrderItemStatusid
      AND so.dd_ItemRelForDelv = 'X'
      AND s.OverallDeliveryStatus = 'Completely processed'
      AND so.ct_DeliveredQty < so.ct_ConfirmedQty
      AND not exists (select 1 from tmp1_soshipmt sod 
                      where sod.dd_SalesDocNo = so.dd_SalesDocNo and sod.dd_SalesItemNo = so.dd_SalesItemNo)
AND  ct_DeliveredQty <> so.ct_ConfirmedQty;
call vectorwise(combine 'fact_salesorder');

 select 'G',TIMESTAMP(LOCAL_TIMESTAMP);
                                     
DROP TABLE IF EXISTS tmp1_soshipmt;

DROP TABLE IF EXISTS tmp_CustomerPOAGIDate;

CREATE TABLE tmp_CustomerPOAGIDate as 
SELECT so.dd_CustomerPONo, max(dt.DateValue) AS LatestCustomPOAGI FROM fact_Salesorder so
INNER JOIN dim_Date dt ON dt.dim_dateid = so.Dim_DateidActualGI
WHERE so.dd_CustomerPONo <> 'Not Set' AND so.Dim_DateidActualGI <> 1 GROUP BY so.dd_CustomerPONo;


UPDATE fact_Salesorder so
	FROM dim_Date edt,
          dim_plant pl,
          tmp_CustomerPOAGIDate po
      SET so.Dim_DateIdLatestCustPOAGI = edt.Dim_Dateid
    WHERE     pl.Dim_Plantid = so.Dim_Plantid
          AND edt.companycode = pl.companycode
          AND edt.DateValue = po.LatestCustomPOAGI
          AND so.dd_CustomerPONo = po.dd_CustomerPONo
	  AND so.dd_CustomerPONo <> 'Not Set'
AND  so.Dim_DateIdLatestCustPOAGI<> edt.Dim_Dateid;


DROP TABLE IF EXISTS tmp_CustomerPOAGIDate;

Select 'process complete';
Select current_time;

/*   End of Script         : bi_populate_so_shipment */
/* ##################################################################################################################*/

Drop table if exists max_holder_721;
Create table max_holder_721(maxid)
as
Select ifnull(max(fact_salesorderdeliveryid),0)
from fact_salesorderdelivery;
call vectorwise(combine 'max_holder_721');
 select 'E2',TIMESTAMP(LOCAL_TIMESTAMP);


select 'Insert 2 on fact_salesorderdelivery',TIMESTAMP(LOCAL_TIMESTAMP);

DROP TABLE IF EXISTS tmp_ins2_fs_ins;
CREATE TABLE tmp_ins2_fs_ins
AS
SELECT f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo
FROM fact_salesorder f;

DROP TABLE IF EXISTS tmp_ins2_fs_del;
CREATE TABLE tmp_ins2_fs_del
AS
SELECT f.dd_SalesDocNo, f.dd_SalesItemNo, f.dd_ScheduleNo
FROM fact_salesorderdelivery f;


call vectorwise(combine 'tmp_ins2_fs_ins-tmp_ins2_fs_del');



INSERT INTO fact_salesorderdelivery(fact_salesorderdeliveryid,
					dd_SalesDocNo,
                                    dd_SalesItemNo,
                                    dd_ScheduleNo,
                                    dd_SalesDlvrDocNo,
                                    dd_SalesDlvrItemNo,
                                    dd_MovementType,
                                    dd_AfsStockCategory,
                                    dd_CustomerPONo,
                                    dd_AfsStockType,
                                    ct_QtyDelivered,
                                    amt_Cost_DocCurr,
                                    amt_Cost,
                                    amt_SalesSubTotal3,
                                    amt_SalesSubTotal4,
                                    amt_AfsNetPrice,
                                    amt_AfsNetValue,
                                    amt_BasePrice,
                                    amt_CustomerExpectedPrice,
                                    amt_DicountAccrualNetPrice,
                                    ct_AfsOpenQty,
                                    ct_FixedProcessDays,
                                    ct_ShipProcessDays,
                                    ct_ScheduleQtySalesUnit,
                                    ct_ConfirmedQty,
                                    ct_PriceUnit,
                                    amt_UnitPrice,
                                    amt_ExchangeRate,
                                    amt_ExchangeRate_GBL,
                                    Dim_DateidPlannedGoodsIssue,
                                    Dim_DateidActualGoodsIssue,
                                    Dim_DateidDeliveryDate,
                                    Dim_DateidLoadingDate,
                                    Dim_DateidPickingDate,
                                    Dim_DateidDlvrDocCreated,
                                    Dim_DateidMatlAvail,
                                    Dim_CustomeridSoldTo,
                                    Dim_CustomeridShipTo,
                                    Dim_Partid,
                                    Dim_Plantid,
                                    Dim_StorageLocationid,
                                    Dim_ProductHierarchyid,
                                    Dim_DeliveryHeaderStatusid,
                                    Dim_DeliveryItemStatusid,
                                    Dim_DateidSalesOrderCreated,
                                    Dim_DateidSchedDeliveryReq,
                                    Dim_DateidSchedDlvrReqPrev,
                                    Dim_DateidMatlAvailOriginal,
                                    Dim_Currencyid,
                                    Dim_Companyid,
                                    Dim_SalesDivisionid,
                                    Dim_ShipReceivePointid,
                                    Dim_DocumentCategoryid,
                                    Dim_SalesDocumentTypeid,
                                    Dim_SalesOrgid,
                                    Dim_SalesGroupid,
                                    Dim_CostCenterid,
                                    Dim_BillingBlockid,
                                    Dim_TransactionGroupid,
                                    Dim_CustomerGroup1id,
                                    Dim_CustomerGroup2id,
                                    Dim_salesorderitemcategoryid,
                                    Dim_ScheduleLineCategoryId,
                                    Dim_ProfitCenterId,
                                    Dim_ControllingAreaId,
                                    Dim_DateIdBillingDate,
                                    Dim_BillingDocumentTypeId,
                                    Dim_DistributionChannelId,
                                    Dim_OverallStatusCreditCheckId,
                                    Dim_AfsSizeId,
                                    Dim_SalesDocOrderReasonId,
                                    Dim_MaterialPriceGroup1Id,
                                    Dim_MaterialGroupId,
                                    Dim_AccountAssignmentGroupId,
                                    Dim_DateIdAfsCancelDate,
                                    Dim_DateIdEarliestSOCancelDate,
                                    Dim_DateIdAfsReqDelivery,
                                    Dim_CustomPartnerFunctionId,
                                    Dim_PayerPartnerFunctionId,
                                    Dim_BillToPartyPartnerFunctionId,
                                    Dim_CustomPartnerFunctionId1,
                                    Dim_CustomPartnerFunctionId2,
                                    Dim_CreditRepresentativeId,
                                    Dim_PurchaseOrderTypeId,
                                    Dim_DateIdQuotationValidFrom,
                                    Dim_DateIdPurchaseOrder,
                                    Dim_DateIdQuotationValidTo,
                                    Dim_AFSSeasonId,
                                    Dim_AfsRejectionReasonId,
                                    Dim_SalesOrderRejectReasonid,
                                    Dim_DateidActualGI_Original,
                                    dd_AfsDepartment,
                                    ct_ReservedQty,
                                    ct_FixedQty,
                                    amt_SalesSubTotal3UnitPrice,
				    Dim_DateIdSODocument,
				    Dim_DateIdSOCreated,
				    dd_SDCreatedBy,
				    Dim_DeliveryblockId,
				    ct_AfsUnallocatedQty,
				    Dim_DateIdSOItemChangedOn,
				    Dim_ShippingConditionId,
				    dd_AfsAllocationGroupNo,
				    Dim_SalesOrderHeaderStatusId,
				    Dim_SalesOrderItemStatusId,
				    ct_AfsTotalDrawn,
				    Dim_DateidSchedDelivery,
				    Dim_DateIdRejection,
				    dd_CustomerMaterial)
   SELECT maxid + row_number() over(),f.dd_SalesDocNo,
          f.dd_SalesItemNo,
          f.dd_ScheduleNo,
          'Not Set' dd_SalesDlvrDocNo,
          0 dd_SalesDlvrItemNo,
          'Not Set' dd_MovementType,
          f.dd_AfsStockCategory,
          f.dd_CustomerPONo,
          'Not Set',
          0 ct_QtyDelivered,
          0 amt_Cost_DocCurr,
          0 amt_Cost,
          f.amt_SubTotal3 amt_SalesSubTotal3,
          f.amt_SubTotal4 amt_SalesSubTotal4,
          f.amt_AfsNetPrice amt_AfsNetPrice,
          f.amt_AfsNetValue amt_AfsNetValue,
          f.amt_BasePrice amt_BasePrice,
          IFNULL(f.amt_CustomerExpectedPrice,0) amt_CustomerExpectedPrice,
          IFNULL(f.amt_DicountAccrualNetPrice,0) amt_DicountAccrualNetPrice,
          f.ct_AfsOpenQty ct_AfsOpenQty,
          0 ct_FixedProcessDays,
          0 ct_ShipProcessDays,
          f.ct_ScheduleQtySalesUnit,
          f.ct_ConfirmedQty,
          f.ct_PriceUnit,
          f.amt_UnitPrice,
          f.amt_ExchangeRate,
          f.amt_ExchangeRate_GBL,
          f.Dim_DateidGoodsIssue,
          1,
          f.Dim_DateidSchedDelivery,
          f.Dim_DateidLoading,
          1 Dim_DateidPickingDate,
          1 Dim_DateidDlvrDocCreated,
          f.Dim_DateidMtrlAvail,
          f.Dim_CustomerID Dim_CustomeridSoldTo,
          f.Dim_CustomerID Dim_CustomeridShipTo,
          f.Dim_Partid,
          f.Dim_Plantid,
          f.Dim_StorageLocationid,
          f.Dim_ProductHierarchyid,
          1 Dim_DeliveryHeaderStatusid,
          1 Dim_DeliveryItemStatusid,
          f.Dim_DateidSalesOrderCreated,
          f.Dim_DateidSchedDeliveryReq,
          f.Dim_DateidSchedDlvrReqPrev,
          f.Dim_DateidMtrlAvail,
          f.Dim_Currencyid,
          f.Dim_Companyid,
          f.Dim_SalesDivisionid,
          f.Dim_ShipReceivePointid,
          f.Dim_DocumentCategoryid,
          f.Dim_SalesDocumentTypeid,
          f.Dim_SalesOrgid,
          f.Dim_SalesGroupid,
          f.Dim_CostCenterid,
          f.Dim_BillingBlockid,
          f.Dim_TransactionGroupid,
          f.Dim_CustomerGroup1id,
          f.Dim_CustomerGroup2id,
          f.dim_salesorderitemcategoryid,
          f.Dim_ScheduleLineCategoryId,
          1 Dim_ProfitCenterid,
          1 Dim_ControllingAreaId,
          1 Dim_DateIdBillingDate,
          1 Dim_BillingDocumentTypeId,
          f.Dim_DistributionChannelId Dim_DistributionChannelId,
          f.Dim_OverallStatusCreditCheckId Dim_OverallStatusCreditCheckId,
          f.Dim_AfsSizeId Dim_AfsSizeId,
          f.Dim_SalesDocOrderReasonId Dim_SalesDocOrderReasonId,
          f.Dim_MaterialPriceGroup1Id Dim_MaterialPriceGroup1Id,
          f.Dim_MaterialGroupId Dim_MaterialGroupId,
          f.Dim_AccountAssignmentGroupId Dim_AccountAssignmentGroupId,
          f.Dim_DateIdAfsCancelDate Dim_DateIdAfsCancelDate,
          f.Dim_DateIdEarliestSOCancelDate Dim_DateIdEarliestSOCancelDate,
          f.Dim_DateIdAfsReqDelivery Dim_DateIdAfsReqDelivery,
          f.Dim_CustomPartnerFunctionId Dim_CustomPartnerFunctionId,
          f.Dim_PayerPartnerFunctionId Dim_PayerPartnerFunctionId,
          f.Dim_BillToPartyPartnerFunctionId Dim_BillToPartyPartnerFunctionId,
          f.Dim_CustomPartnerFunctionId1 Dim_CustomPartnerFunctionId1,
          f.Dim_CustomPartnerFunctionId2 Dim_CustomPartnerFunctionId2,
          f.Dim_CreditRepresentativeId Dim_CreditRepresentativeId,
          f.Dim_PurchaseOrderTypeId Dim_PurchaseOrderTypeId,
          f.Dim_DateIdQuotationValidFrom Dim_DateIdQuotationValidFrom,
          f.Dim_DateIdPurchaseOrder Dim_DateIdPurchaseOrder,
          f.Dim_DateIdQuotationValidTo Dim_DateIdQuotationValidTo,
          f.Dim_AFSSeasonId Dim_AFSSeasonId,
          f.Dim_AfsRejectionReasonId Dim_AfsRejectionReasonId,
          f.Dim_SalesOrderRejectReasonid Dim_SalesOrderRejectReasonid,
          1 Dim_DateidActualGI_Original,
          f.dd_AfsDepartment dd_AfsDepartment,
          f.ct_AfsAllocationRQty ct_ReservedQty,
          f.ct_AfsAllocationFQty ct_FixedQty,
          (CASE
              WHEN f.ct_ConfirmedQty > 0
              THEN
                 Decimal((f.amt_SubTotal3 / (case when f.ct_ConfirmedQty=0 then null else f.ct_ConfirmedQty end)),18,4)
              ELSE
                 0
           END)
             amt_SalesSubTotal3UnitPrice,
	   f.Dim_DateIdSODocument Dim_DateIdSODocument,
	   f.Dim_DateIdSOCreated Dim_DateIdSOCreated,
	   f.dd_CreatedBy dd_SDCreatedBy,
           f.Dim_DeliveryblockId,
	   f.ct_AfsUnallocatedQty,
	   ifnull(f.Dim_DateIdSOItemChangedOn,1),
	   f.Dim_ShippingConditionId,
	   f.dd_AfsAllocationGroupNo,
	   f.Dim_SalesOrderHeaderStatusId,
	   f.Dim_SalesOrderItemStatusId,
	   ct_AfsTotalDrawn,
	    ifnull(f.Dim_DateidSchedDelivery,1),
	    ifnull(f.Dim_DateIdRejection,1),
	    ifnull(dd_CustomerMaterialNo,'Not Set')
     FROM fact_salesorder f,max_holder_721,tmp_ins2_fs_ins f1
    WHERE f1.dd_SalesDocNo = f.dd_SalesDocNo
	AND  f1.dd_SalesItemNo = f.dd_SalesItemNo
	AND  f1.dd_ScheduleNo = f.dd_ScheduleNo;
call vectorwise(combine 'fact_salesorderdelivery');

 select 'H',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE       fact_salesorderdelivery sod
From likp_lips_vbuk v,dim_overallstatusforcreditcheck oscc
SET sod.Dim_OverallStatusCreditCheckId = oscc.dim_overallstatusforcreditcheckID
Where sod.dd_SalesDlvrDocNo = v.VBUK_VBELN
AND  oscc.overallstatusforcreditcheck = v.VBUK_CMGST
AND  oscc.RowIsCurrent = 1
AND sod.Dim_OverallStatusCreditCheckId <> oscc.dim_overallstatusforcreditcheckID;
call vectorwise(combine 'fact_salesorderdelivery');
 select 'I',TIMESTAMP(LOCAL_TIMESTAMP);
 
 
 DROP TABLE IF EXISTS tmp_ct_BilledQtyAtSchedule_sum;
 CREATE TABLE tmp_ct_BilledQtyAtSchedule_sum
 AS
 SELECT dd_SalesDlvrDocNo,dd_SalesDlvrItemNo,dd_salesdocno,dd_salesitemno,dd_AfsScheduleNo,SUM(bil.ct_BilledQtyAtSchedule) sum_ct_BilledQtyAtSchedule
 FROM fact_billing bil
INNER JOIN dim_billingdocumenttype bdt ON bdt.Dim_BillingDocumentTypeid = bil.Dim_DocumentTypeid
INNER JOIN dim_billingmisc bm ON bm.Dim_BillingMiscId = bil.Dim_BillingMiscId
WHERE bdt.Type = 'ZF2'
AND bm.CancelFlag <> 'X'
GROUP BY dd_SalesDlvrDocNo,dd_SalesDlvrItemNo,dd_salesdocno,dd_salesitemno,dd_AfsScheduleNo;


 /* Slow query - Tuned LK 25 Aug */
 UPDATE fact_salesorderdelivery sod
 SET ct_AfsBilledQty = 0
 WHERE ct_AfsBilledQty <> 0;
 
  
  UPDATE fact_salesorderdelivery sod
  FROM tmp_ct_BilledQtyAtSchedule_sum bil
    SET sod.ct_AfsBilledQty = sum_ct_BilledQtyAtSchedule
WHERE 	sod.dd_SalesDlvrDocNo = bil.dd_SalesDlvrDocNo
AND sod.dd_SalesDlvrItemNo = bil.dd_SalesDlvrItemNo
AND sod.dd_salesdocno = bil.dd_salesdocno
AND sod.dd_SalesItemNo = bil.dd_salesitemno
AND sod.dd_ScheduleNo = bil.dd_AfsScheduleNo
AND ct_AfsBilledQty <> sum_ct_BilledQtyAtSchedule;
	
call vectorwise(combine 'fact_salesorderdelivery');
 select 'J',TIMESTAMP(LOCAL_TIMESTAMP);

/* AFS Rescheduling fields */

call vectorwise(combine 'fact_salesorder');

UPDATE fact_salesorderdelivery sod
  FROM
       fact_salesorder so
 set sod.ct_AfsAllocatedQty = so.ct_AfsAllocatedQty
 WHERE sod.dd_Salesdocno = so.dd_SalesDocNo
 and sod.dd_SalesItemNo = so.dd_SalesItemNo
 and sod.dd_ScheduleNo = so.dd_ScheduleNo
 and sod.ct_AfsAllocatedQty <> so.ct_AfsAllocatedQty;

call vectorwise(combine 'fact_salesorderdelivery');
UPDATE fact_salesorderdelivery sod
  FROM dim_part dp,
  	   dim_afssize asiz,  	
       MEAN mn
SET sod.dd_UniversalProductCode = ifnull(mn.MEAN_EAN11,'Not Set')
WHERE sod.dim_partid = dp.dim_partid
AND sod.dim_Afssizeid = asiz.dim_AfsSizeId
AND dp.PartNumber = mn.MEAN_MATNR
AND mn.MEAN_EAN11 IS NOT NULL
AND mn.MEAN_J_3AKORDX = asiz.Size
AND dp.RowIsCurrent = 1
AND sod.dim_partid > 1;
UPDATE fact_salesorderdelivery sod
SET sod.dd_UniversalProductCode = 'Not Set'
WHERE sod.dd_UniversalProductCode IS NULL;
call vectorwise (combine 'fact_salesorderdelivery');
 select 'K',TIMESTAMP(LOCAL_TIMESTAMP);
 
DROP TABLE IF EXISTS tmp_AfsRescheduleAllocation;

CREATE TABLE tmp_AfsRescheduleAllocation
AS
   SELECT j.J_3ARESH_AUFNR AS SalesDocNo,
          j.J_3ARESH_POSNR AS SalesItemNo,
          j.J_3ARESH_ETENR AS ScheduleNo,
          j.J_3ARESH_J_3ASTAT AS Type,
          SUM(j.J_3ARESH_MENGE) AS RescheduleQty
     FROM J_3ARESH j
   GROUP BY j.J_3ARESH_AUFNR,
            j.J_3ARESH_POSNR,
            j.J_3ARESH_ETENR,
            j.J_3ARESH_J_3ASTAT;

call vectorwise(combine 'tmp_AfsRescheduleAllocation');	     

UPDATE fact_salesorderdelivery sod
FROM
 tmp_AfsRescheduleAllocation t
 set sod.ct_ExpectedReservedQty = t.RescheduleQty
 WHERE t.SalesDocNo = sod.dd_SalesDocNo
 and t.salesitemno  = sod.dd_SalesItemNo
 and t.ScheduleNo = sod.dd_ScheduleNo
 and t.Type = 'R'
 and sod.ct_ExpectedReservedQty <> t.RescheduleQty;

call vectorwise(combine 'fact_salesorderdelivery');

 
UPDATE fact_salesorderdelivery sod
 FROM
 tmp_AfsRescheduleAllocation t
 set sod.ct_ExpectedFixedQty = t.RescheduleQty
 WHERE t.SalesDocNo = sod.dd_SalesDocNo
 and t.salesitemno  = sod.dd_SalesItemNo
 and t.ScheduleNo = sod.dd_ScheduleNo
 and t.Type = 'F'
 and sod.ct_ExpectedFixedQty <> t.RescheduleQty;

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_Salesorderdelivery sod
 FROM J_3ARESH j
 SET dd_ArunStockType = ifnull(j.J_3ARESH_J_3ABSKZ,'Not Set')
 WHERE j.J_3ARESH_AUFNR = sod.dd_SalesDocNo
 and j.J_3ARESH_POSNR  = sod.dd_SalesItemNo
 and j.J_3ARESH_ETENR = sod.dd_ScheduleNo
 and dd_ArunStockType <> ifnull(j.J_3ARESH_J_3ABSKZ,'Not Set');
 
 DROP TABLE IF EXISTS tmp_AfsRescheduleAllocation;

 DROP TABLE IF EXISTS tmp_J_3ARESH;
 
 CREATE TABLE tmp_J_3ARESH AS
 SELECT distinct j.J_3ARESH_AUFNR, j.J_3ARESH_POSNR, j.J_3ARESH_ETENR, 'Not Set' as AllocReserved, 'Not Set' as AllocFixed,
 'Not Set' as AllocDelivery from J_3ARESH j;
 
 UPDATE tmp_J_3ARESH t
   FROM J_3ARESH j
  SET  AllocReserved = 'X'
 WHERE   j.J_3ARESH_AUFNR = t.J_3ARESH_AUFNR
     and j.J_3ARESH_POSNR  = t.J_3ARESH_POSNR
 	 and j.J_3ARESH_ETENR = t.J_3ARESH_ETENR 
     and j.J_3ARESH_J_3ASTAT  = 'R';
 
 UPDATE tmp_J_3ARESH t
   FROM J_3ARESH j
  SET  AllocFixed = 'X'
 WHERE   j.J_3ARESH_AUFNR = t.J_3ARESH_AUFNR
     and j.J_3ARESH_POSNR  = t.J_3ARESH_POSNR
 	 and j.J_3ARESH_ETENR = t.J_3ARESH_ETENR 
     and j.J_3ARESH_J_3ASTAT  = 'F';
 
 UPDATE tmp_J_3ARESH t
   FROM J_3ARESH j
  SET  AllocDelivery = 'X'
 WHERE   j.J_3ARESH_AUFNR = t.J_3ARESH_AUFNR
     and j.J_3ARESH_POSNR  = t.J_3ARESH_POSNR
 	 and j.J_3ARESH_ETENR = t.J_3ARESH_ETENR 
     and j.J_3ARESH_J_3ASTAT  = 'D';
     
    
UPDATE fact_salesorderdelivery sod
from dim_afsmrpstatus a,tmp_J_3ARESH t
set sod.dim_afsmrpstatusid = a.dim_afsmrpstatusid 
where a.AllocReserved = t.AllocReserved
AND a.AllocFixed = t.AllocFixed
AND a.AllocDelivery = t.AllocDelivery
AND t.J_3ARESH_AUFNR = sod.dd_SalesDocNo
and t.J_3ARESH_POSNR  = sod.dd_SalesItemNo
and t.J_3ARESH_ETENR = sod.dd_ScheduleNo;
 
DROP TABLE IF EXISTS tmp_J_3ARESH; 

 call vectorwise(combine 'fact_salesorderdelivery');

UPDATE fact_Salesorderdelivery sod
SET Dim_DateIdProjectedDate = Dim_DateidDeliveryDate
WHERE Dim_DateidActualGI_Original > 1 
AND ct_AFSBilledQty > 0;

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE  fact_Salesorderdelivery
SET Dim_DateIdProjectedDate = Dim_DateidPlannedGoodsIssue
WHERE Dim_DateidActualGI_Original = 1
AND Dim_DateidPlannedGoodsIssue > 1;

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE  fact_Salesorderdelivery sod
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdAfsReqDelivery
WHERE  sod.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = sod.Dim_DateIdAfsReqDelivery
AND (sod.ct_AfsOpenQty- sod.ct_QtyDelivered) > 0
AND cg.DocumentCategory NOT in ('G','g','B','b')
AND dt.DateValue > current_Date;

UPDATE  fact_Salesorderdelivery sod
    FROM dim_date dt, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = Dim_DateIdAfsReqDelivery
WHERE  sod.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = sod.Dim_DateIdAfsReqDelivery
AND (sod.ct_ConfirmedQty - sod.ct_AfsTotalDrawn) > 0
AND cg.DocumentCategory in ('G','g','B','b')
AND dt.DateValue > current_Date;

call vectorwise(combine 'fact_salesorderdelivery');
 select 'L',TIMESTAMP(LOCAL_TIMESTAMP);

UPDATE  fact_Salesorderdelivery sod
    FROM dim_date dt, Dim_Plant pl, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = ifnull(( SELECT dt2.Dim_DateId from Dim_Date dt2
									 WHERE dt2.DateValue = current_Date AND dt2.companycode = pl.CompanyCode),1)
WHERE  sod.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = sod.Dim_DateIdAfsReqDelivery
AND (sod.ct_ConfirmedQty - sod.ct_AfsTotalDrawn) > 0
AND cg.DocumentCategory in ('G','g','B','b')
AND dt.DateValue <= current_Date;

UPDATE  fact_Salesorderdelivery sod
    FROM dim_date dt, Dim_Plant pl, dim_Documentcategory cg
SET Dim_DateIdProjectedDate = ifnull(( SELECT dt2.Dim_DateId from Dim_Date dt2
									 WHERE dt2.DateValue = current_Date AND dt2.companycode = pl.CompanyCode),1)
WHERE  sod.dim_DocumentCategoryid = cg.dim_DocumentCategoryid
AND dt.Dim_DateId = sod.Dim_DateIdAfsReqDelivery
AND (sod.ct_AfsOpenQty- sod.ct_QtyDelivered) > 0
AND cg.DocumentCategory NOT in ('G','g','B','b')
AND dt.DateValue <= current_Date;

call vectorwise(combine 'fact_salesorderdelivery');

UPDATE  fact_Salesorderdelivery sod
    FROM dim_date dt, Dim_Plant pl, Dim_Date dt2
SET Dim_DateIdProjectedDate = dt.Dim_DateId
WHERE sod.Dim_PlantId = pl.Dim_PlantId
AND dt.Dim_DateId = sod.Dim_DateIdAfsReqDelivery
AND dt2.Dim_dateId = sod.Dim_DateIdSOItemChangedOn
AND Dim_AfsRejectionReasonId <> 1
AND dt2.DateValue < dt.DateValue;

UPDATE  fact_Salesorderdelivery sod
    FROM dim_date dt, Dim_Plant pl, Dim_Date dt2
SET Dim_DateIdProjectedDate = dt2.Dim_dateId
WHERE sod.Dim_PlantId = pl.Dim_PlantId
AND dt.Dim_DateId = sod.Dim_DateIdAfsReqDelivery
AND dt2.Dim_dateId = sod.Dim_DateIdSOItemChangedOn
AND Dim_AfsRejectionReasonId <> 1
AND dt2.DateValue >= dt.DateValue;

call vectorwise(combine 'fact_salesorderdelivery');

 UPDATE fact_salesorderdelivery sod
 SET dd_ProjectedCategory = 'Not Set'
 WHERE ifnull(dd_ProjectedCategory,'xx') <> 'Not Set';
	 
	 
 UPDATE fact_salesorderdelivery sod
 SET ct_ProjectedQty = 0
 WHERE IFNULL(ct_ProjectedQty,-1) <> 0;


 UPDATE fact_salesorderdelivery sod
 SET dd_ProjectedCategory = 'REJECTED'
 WHERE sod.Dim_AfsRejectionReasonId <> 1;


 UPDATE fact_salesorderdelivery sod
 SET ct_ProjectedQty = sod.ct_ScheduleQtySalesUnit 
 WHERE dd_ProjectedCategory = 'REJECTED';
 
call vectorwise(combine 'fact_salesorderdelivery'); 


 UPDATE fact_Salesorderdelivery sod
 SET dd_ProjectedCategory = 'SHIPPED'
 WHERE Dim_DateidActualGI_Original > 1 
 AND sod.Dim_AfsRejectionReasonId = 1
 AND ct_AFSBilledQty > 0;
 
 
 UPDATE fact_Salesorderdelivery sod
 SET ct_ProjectedQty = sod.ct_AFSBilledQty
 WHERE dd_ProjectedCategory = 'SHIPPED';
 
 
  UPDATE fact_salesorderdelivery sod
     FROM dim_Documentcategory dc
 SET dd_ProjectedCategory = 'QUOTE' 
 WHERE dc.dim_Documentcategoryid = sod.dim_Documentcategoryid
 AND sod.Dim_AfsRejectionReasonId = 1
  AND dc.DocumentCategory IN ('B','b') ;
 
 UPDATE fact_salesorderdelivery sod
 SET ct_ProjectedQty = 0
 WHERE dd_ProjectedCategory = 'QUOTE' 
 AND ct_ProjectedQty <> 0;
  
  
 UPDATE fact_salesorderdelivery sod
     FROM dim_Documentcategory dc,
          dim_AfsMrpStatus a
 SET dd_ProjectedCategory = 'ALLOCATED CONTRACT' 
 WHERE dc.dim_Documentcategoryid = sod.dim_Documentcategoryid
  AND a.dim_afsmrpstatusid = sod.dim_afsmrpstatusid
   AND sod.Dim_AfsRejectionReasonId = 1
  AND dc.DocumentCategory IN ('G','g')
  AND a.AllocReserved = 'X'
  AND (sod.ct_ConfirmedQty - sod.ct_AfsTotalDrawn) > 0;  
  
  
 UPDATE fact_salesorderdelivery sod
  SET ct_ProjectedQty = sod.ct_ReservedQty
 WHERE dd_ProjectedCategory = 'ALLOCATED CONTRACT' ;
  
  
   UPDATE fact_salesorderdelivery sod
     FROM dim_Documentcategory dc,
          dim_AfsMrpStatus a
 SET dd_ProjectedCategory = 'UNALLOCATED CONTRACT'
 WHERE dc.dim_Documentcategoryid = sod.dim_Documentcategoryid
  AND a.dim_afsmrpstatusid = sod.dim_afsmrpstatusid
   AND sod.Dim_AfsRejectionReasonId = 1
  AND dc.DocumentCategory IN ('G','g')
  AND a.AllocReserved = 'Not Set'
  AND (sod.ct_ConfirmedQty - sod.ct_AfsTotalDrawn) > 0;
  
  UPDATE fact_salesorderdelivery sod
 SET ct_ProjectedQty = sod.ct_AfsUnallocatedQty
 WHERE dd_ProjectedCategory = 'UNALLOCATED CONTRACT';
  
  
   UPDATE fact_salesorderdelivery sod
     FROM dim_Documentcategory dc
 SET dd_ProjectedCategory = 'UNALLOCATE ORDER' 
 WHERE dc.dim_Documentcategoryid = sod.dim_Documentcategoryid
  AND sod.Dim_AfsRejectionReasonId = 1
  AND sod.dim_afsmrpstatusid = 1
  AND sod.Dim_DateidActualGI_Original = 1
  AND dc.DocumentCategory NOT IN ('B','b','G','g');  

   UPDATE fact_salesorderdelivery sod
 SET   ct_ProjectedQty = sod.ct_AfsUnallocatedQty
 WHERE dd_ProjectedCategory = 'UNALLOCATE ORDER' ;
  
  

 UPDATE fact_salesorderdelivery sod
     FROM facT_salesorder so, dim_AfsMrpStatus a ,dim_Documentcategory dc
 SET dd_ProjectedCategory = 'INCOMPLETE' 
 WHERE sod.dd_salesdocno = so.dd_SalesDocNo
  and sod.dd_SalesItemNo = so.dd_SalesItemno
  and sod.dd_ScheduleNo = so.dd_ScheduleNo 
  and dc.dim_documentcategoryid = sod.dim_documentcategoryid
  AND sod.Dim_AfsRejectionReasonId = 1
  AND a.dim_afsmrpstatusid = sod.dim_afsmrpstatusid
  AND a.AllocReserved = 'X'
  AND dc.DocumentCategory NOT IN ('B','b','G','g')
  AND so.ct_IncompleteQty > 0  ;
  
 UPDATE fact_salesorderdelivery sod
 SET    ct_ProjectedQty = sod.ct_ReservedQty
 WHERE dd_ProjectedCategory = 'INCOMPLETE' ;
  
 UPDATE fact_salesorderdelivery sod
     FROM facT_salesorder so, dim_AfsMrpStatus a,dim_Documentcategory dc
 SET dd_ProjectedCategory = 'UNMET RELEASE RULE' 
 WHERE sod.dd_salesdocno = so.dd_SalesDocNo
  and sod.dd_SalesItemNo = so.dd_SalesItemno
  and sod.dd_ScheduleNo = so.dd_ScheduleNo 
   AND sod.Dim_AfsRejectionReasonId = 1
  AND a.dim_afsmrpstatusid = sod.dim_afsmrpstatusid
  and dc.dim_documentcategoryid = sod.dim_documentcategoryid
  AND a.AllocReserved = 'X'
  AND dc.DocumentCategory NOT IN ('B','b','G','g')
  AND so.ct_IncompleteQty = 0;  
 
 UPDATE fact_salesorderdelivery sod
 SET ct_ProjectedQty = sod.ct_ReservedQty
 WHERE dd_ProjectedCategory = 'UNMET RELEASE RULE' ;

 UPDATE fact_salesorderdelivery sod
     FROM  dim_AfsMrpStatus a,dim_Documentcategory dc
 SET dd_ProjectedCategory = 'DELIVERY BLOCK'
 WHERE a.dim_afsmrpstatusid = sod.dim_afsmrpstatusid
  and dc.dim_documentcategoryid = sod.dim_documentcategoryid
  AND dc.DocumentCategory NOT IN ('B','b','G','g')
  AND a.AllocFixed = 'X'
  AND sod.Dim_DeliveryBlockid > 1;
  
 UPDATE fact_salesorderdelivery sod
 SET ct_ProjectedQty = sod.ct_FixedQty
 WHERE dd_ProjectedCategory = 'DELIVERY BLOCK';
  

   UPDATE fact_salesorderdelivery sod
     FROM  dim_AfsMrpStatus a,dim_Documentcategory dc
 SET dd_ProjectedCategory = 'CREDIT BLOCK'
 WHERE a.dim_afsmrpstatusid = sod.dim_afsmrpstatusid
  and dc.dim_documentcategoryid = sod.dim_documentcategoryid
   AND dc.DocumentCategory NOT IN ('B','b','G','g')
   AND sod.Dim_AfsRejectionReasonId = 1
  AND a.AllocFixed = 'X'
  AND sod.Dim_DeliveryBlockid = 1
  AND sod.Dim_OverallStatusCreditCheckId > 1 ;
  
  

   UPDATE fact_salesorderdelivery sod
 SET ct_ProjectedQty = sod.ct_FixedQty
 WHERE dd_ProjectedCategory = 'CREDIT BLOCK';
  
  
    UPDATE fact_salesorderdelivery sod
     FROM dim_AfsMrpStatus a,dim_Documentcategory dc
 SET dd_ProjectedCategory = 'PROJECTED TO SHIP' 
 WHERE a.dim_afsmrpstatusid = sod.dim_afsmrpstatusid
  and dc.dim_documentcategoryid = sod.dim_documentcategoryid
   AND dc.DocumentCategory NOT IN ('B','b','G','g')
   AND sod.Dim_AfsRejectionReasonId = 1
  AND a.AllocFixed = 'X'
  AND sod.Dim_DeliveryBlockid = 1
  AND sod.Dim_OverallStatusCreditCheckId = 1 ;  

 UPDATE fact_salesorderdelivery sod
 SET   ct_ProjectedQty = sod.ct_FixedQty
 WHERE dd_ProjectedCategory = 'PROJECTED TO SHIP' ;

    UPDATE fact_salesorderdelivery sod
     FROM dim_AfsMrpStatus a
 SET dd_ProjectedCategory = 'ON DELIVERY'
 WHERE a.dim_afsmrpstatusid = sod.dim_afsmrpstatusid
  AND a.AllocDelivery = 'X'
AND dd_ProjectedCategory <> 'ON DELIVERY';
  
 UPDATE fact_salesorderdelivery sod
 SET  ct_ProjectedQty = sod.ct_QtyDelivered
 WHERE dd_ProjectedCategory = 'ON DELIVERY';
  
  call vectorwise(combine 'fact_salesorderdelivery');

/*##########PART 4 Ends###############################	 */

/*##########PART 5 Starts###############################	 */
/*
select 'Calling VW_bi_populate_so_shippedAgainstOrderQty.sql',TIMESTAMP(LOCAL_TIMESTAMP)

\i /home/fusionops/ispring/db/scripts/VW_bi_populate_so_shippedAgainstOrderQty.sql

select 'Calling VW_bi_populate_salesorder_coveredqty.sql',TIMESTAMP(LOCAL_TIMESTAMP)

\i /home/fusionops/ispring/db/scripts/VW_bi_populate_salesorder_coveredqty.sql

select 'Calling VW_bi_populate_salesorder_fillrate.sql',TIMESTAMP(LOCAL_TIMESTAMP)

\i /home/fusionops/ispring/db/scripts/VW_bi_populate_salesorder_fillrate.sql
*/
/*##########PART 5 Ends###############################	 */

/*##########PART 6 Starts###############################	 */

UPDATE fact_salesorderdelivery sod
From  fact_billing fb
    SET sod.dd_billing_no = fb.dd_billing_no,
        sod.dd_Billingitem_no = fb.dd_billing_item_no,
        sod.amt_BillingCustomerConfigSubtotal4 = fb.amt_CustomerConfigSubtotal4
  WHERE     fb.dd_SalesDlvrDocNo = sod.dd_SalesDlvrDocNo
        AND fb.dd_SalesDlvrItemNo = sod.dd_SalesDlvrItemNo;

call vectorwise(combine 'fact_salesorderdelivery');


 select 'M',TIMESTAMP(LOCAL_TIMESTAMP);

select 'Process Complete';
select current_time;

/* ##########PART 6 Ends. Proc Ends ###############################	 */
DROP TABLE IF EXISTS tmp_perf_cursor_tbl1_721;
DROP TABLE IF EXISTS tmp_perf_cursor_tbl2_721_confqty;


DROP TABLE IF EXISTS tmp_delstg721_fact_salesorderdelivery_ins;
DROP TABLE IF EXISTS tmp_delstg721_fact_salesorderdelivery_del;
DROP TABLE IF EXISTS tmp_delstg721_fso_ins;
DROP TABLE IF EXISTS tmp_delstg721_fso_del;
DROP TABLE IF EXISTS tmp_perf_cursor_tbl1_721;
 DROP TABLE IF EXISTS tmp_perf_cursor_tbl2_721_confqty;
DROP TABLE IF EXISTS tmp_ct_QtyDeliveredsummax;
DROP TABLE IF EXISTS tmp_ins2_fs_ins;
DROP TABLE IF EXISTS tmp_ins2_fs_del;
 DROP TABLE IF EXISTS tmp_ct_BilledQtyAtSchedule_sum;
DROP TABLE IF EXISTS tmp_perf_cursor_tbl2_721_confqty;

 select 'END OF PROC VW_bi_populate_afs_salesorderdelivery_fact',TIMESTAMP(LOCAL_TIMESTAMP);
