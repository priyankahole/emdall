/**********************************************************************************/
/*  Script : vw_bi_populate_inventoryaging_fact.sql                               */
/*  Description : This script is recreated to allocate total stock quantity into  */
/*                each material movement quantity to reflect actual stock aging.  */
/*  Created On : July 2, 2013                                                     */
/*  Create By : Hiten Suthar                                                      */
/*                                                                                */
/*  Change History                                                                */
/*  Date         CVSversion  By          Description                              */
/*  02 July 2013  1.0         Hiten      New Script                               */
/*  31 July 2013  1.42        Ashu       Optimized an Insert statement            */
/*  14 Aug 2013   1.47        Hiten      Only latest ex rate                      */
/*                                       and added MARC, STO and WIP stock        */
/*  27 Aug 2013   1.54        Hiten      Default for null column values           */
/*  27 Aug 2013   1.55        Shanthi    Checking in Profit Center dimension      */
/*  28 Aug 2013   1.56	      Shanthi    New Field WIP Qty from PRod Order	  */
/**********************************************************************************/

DROP TABLE IF EXISTS tmp_GlobalCurr_001;
CREATE TABLE tmp_GlobalCurr_001 AS
SELECT ifnull((SELECT property_value
                  FROM systemproperty
                  WHERE property = 'customer.global.currency'),
                'USD') pGlobalCurrency;

DROP TABLE IF EXISTS fact_inventoryaging_tmp_populate;
CREATE TABLE fact_inventoryaging_tmp_populate
AS
SELECT * FROM fact_inventoryaging
where 1=2;

ALTER TABLE fact_inventoryaging_tmp_populate ADD PRIMARY KEY (fact_inventoryagingid);


delete from NUMBER_FOUNTAIN where table_name = 'fact_inventoryaging_tmp_populate';

INSERT INTO NUMBER_FOUNTAIN
select 'fact_inventoryaging_tmp_populate',ifnull(max(fact_inventoryagingid),0)
FROM fact_inventoryaging_tmp_populate;

DELETE FROM fact_inventoryaging_tmp_populate;

DELETE FROM MCHB WHERE MCHB_CLABS = 0 AND MCHB_CUMLM = 0 AND MCHB_CINSM = 0 AND MCHB_CEINM = 0 AND MCHB_CSPEM = 0;
DELETE FROM MARD WHERE MARD_LABST = 0 AND MARD_UMLME = 0 AND MARD_INSME = 0 AND MARD_EINME = 0 AND MARD_SPEME = 0
			AND MARD_KSPEM = 0 AND MARD_KINSM = 0 AND MARD_KEINM = 0 AND MARD_KLABS = 0;
DELETE FROM MSLB WHERE MSLB_LBLAB = 0 AND MSLB_LBINS = 0 AND MSLB_LBEIN = 0;
DELETE FROM MSKU WHERE MSKU_KULAB = 0 AND MSKU_KUINS = 0 AND MSKU_KUEIN = 0;
DELETE FROM MARC WHERE MARC_UMLMC = 0 AND MARC_TRAME = 0;
DELETE FROM MARC WHERE MARC_LVORM = 'X';

UPDATE MCHB SET MCHB_CHARG = 'Not Set' WHERE MCHB_CHARG IS NULL;
UPDATE MSLB SET MSLB_CHARG = 'Not Set' WHERE MSLB_CHARG IS NULL;
UPDATE MSKU SET MSKU_CHARG = 'Not Set' WHERE MSKU_CHARG IS NULL;

UPDATE fact_materialmovement SET dd_BatchNumber = 'Not Set' WHERE dd_BatchNumber IS NULL;

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'dim_storagelocation';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'dim_storagelocation', IFNULL(MAX(dim_storagelocationid), 0)
     FROM dim_storagelocation;

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
				Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent) 
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_storagelocation') + row_number() over (),tty.*
FROM
   (SELECT DISTINCT MCHB_LGORT,
                   MCHB_LGORT,
                   'Not Set' Division,
                   'Not Set' FreezingBookInventory,
                   'Not Set' MRPExclude,
                   'Not Set' StorageResource,
                   'Not Set' PartnerStorageLocation,
                   'Not Set' StorageSalesOrg,
                   'Not Set' DistributionChannel,
                   'Not Set' ShipReceivePoint ,
                   MCHB_WERKS Plant ,
                   'Not Set' InTransit,
                   TIMESTAMP(LOCAL_TIMESTAMP) RowStartDate,
                   1 RowIsCurrent
     FROM MCHB m
    WHERE MCHB_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE LocationCode = MCHB_LGORT AND Plant = MCHB_WERKS)) tty;

 UPDATE NUMBER_FOUNTAIN 
 SET max_id = ifnull(( select max(dim_storagelocationid) from dim_storagelocation), 0)
 WHERE table_name = 'dim_storagelocation';

INSERT INTO dim_storagelocation(Dim_StorageLocationid,
				Description,
                                LocationCode,
                                Division,
                                FreezingBookInventory,
                                MRPExclude,
                                StorageResource,
                                PartnerStorageLocation,
                                StorageSalesOrg,
                                DistributionChannel,
                                ShipReceivePoint,
                                Plant,
                                InTransit,
                                RowStartDate,
                                RowIsCurrent) 
SELECT (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'dim_storagelocation') + row_number() over (),tty.*
FROM
   (SELECT DISTINCT MARD_LGORT,
                   MARD_LGORT,
                   'Not Set' Division,
                   'Not Set' FreezingBookInventory ,
                   'Not Set' MRPExclude,
                   'Not Set' StorageResource,
                   'Not Set' PartnerStorageLocation,
                   'Not Set' StorageSalesOrg,
                   'Not Set' DistributionChannel,
                   'Not Set' ShipReceivePoint,
                   MARD_WERKS Plant ,
                   'Not Set' InTransit,
                   TIMESTAMP(LOCAL_TIMESTAMP) RowStartDate,
                   1 RowIsCurrent          
     FROM MARD m
    WHERE MARD_LGORT IS NOT NULL
          AND NOT EXISTS
                (SELECT 1
                   FROM dim_storagelocation
                  WHERE LocationCode = MARD_LGORT AND Plant = MARD_WERKS)) tty;

\i /db/schema_migration/bin/wrapper_optimizedb.sh dim_storagelocation;

DELETE FROM fact_materialmovement_tmp_invagin;

INSERT INTO fact_materialmovement_tmp_invagin
  (Dim_Partid, Dim_Plantid, Dim_MovementTypeid, dd_BatchNumber, dd_MaterialDocYearItem)
  SELECT x.Dim_Partid, x.Dim_Plantid, x.Dim_MovementTypeid, ifnull(x.dd_BatchNumber,'Not Set') dd_BatchNumber,
      max(concat(x.dd_MaterialDocYear,LPAD(x.dd_MaterialDocNo,10,'0'),LPAD(x.dd_MaterialDocItemNo,5,'0')))
  FROM fact_materialmovement x
        INNER JOIN dim_movementtype dmt ON x.Dim_MovementTypeid = dmt.Dim_MovementTypeid
  WHERE dmt.MovementType in (101,131,105,501,511,521)
  GROUP BY x.Dim_Partid, x.Dim_Plantid, x.Dim_MovementTypeid, ifnull(x.dd_BatchNumber,'Not Set');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_materialmovement_tmp_invagin;

DROP TABLE IF EXISTS tmp_MARD_TotStk_001;
CREATE TABLE tmp_MARD_TotStk_001 AS
SELECT a.MARD_MATNR PartNumber,
	a.MARD_WERKS PlantCode, 
	a.MARD_LGORT StorLocCode, 
	a.MARD_UMLME MARD_UMLME_1,
	a.MARD_LABST MARD_LABST_2, 
	a.MARD_SPEME MARD_SPEME_3, 
	a.MARD_INSME MARD_INSME_4, 
	a.MARD_EINME MARD_EINME_5,
       (a.MARD_LABST + a.MARD_EINME + a.MARD_INSME + a.MARD_SPEME + a.MARD_UMLME) TotalStock,
	a.MARD_KSPEM, 
	a.MARD_KINSM, 
	a.MARD_KEINM,
	a.MARD_KLABS, 
	a.MARD_RETME 
FROM MARD a;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_MARD_TotStk_001;

DROP TABLE IF EXISTS materialmovement_tmp_001;
CREATE TABLE materialmovement_tmp_001 AS
      SELECT cx.fact_materialmovementid,
          cx.ct_Quantity ct_Quantity,
	      dp1.PartNumber PartNumber, 
		  dpl.PlantCode PlantCode, 
		  sl1.LocationCode StorLocCode, 
		  dt1.DateValue PostingDate,
		  ROW_NUMBER() OVER(PARTITION BY mrd.PartNumber, mrd.PlantCode, mrd.StorLocCode
					ORDER BY dt1.DateValue DESC, cx.dd_MaterialDocNo DESC, cx.dd_MaterialDocItemNo DESC, cx.dd_DocumentScheduleNo DESC) RowSeqNo
      FROM fact_materialmovement cx 
	    INNER JOIN dim_movementtype dmt
		      ON cx.Dim_MovementTypeid = dmt.Dim_MovementTypeid AND cx.dd_debitcreditid = 'Debit'
			 AND dmt.MovementType in (101,105,501,511,521,301,305,309,311,315,321,343,561,641,653,262,202,966,312)
            INNER JOIN dim_Plant dpl ON cx.dim_Plantid = dpl.dim_Plantid
            INNER JOIN dim_Part dp1 ON cx.dim_Partid = dp1.dim_Partid
            INNER JOIN dim_storagelocation sl1 ON cx.dim_storagelocationid = sl1.dim_storagelocationid
            INNER JOIN dim_date dt1 ON dt1.Dim_Dateid = cx.dim_DateIDPostingDate
	    INNER JOIN tmp_MARD_TotStk_001 mrd ON dp1.PartNumber = mrd.PartNumber
						  AND dpl.PlantCode = mrd.PlantCode
						  AND sl1.LocationCode = mrd.StorLocCode
     WHERE cx.ct_Quantity > 0
	   AND (dmt.MovementType <> 641 OR (dmt.MovementType = 641 
						AND not exists (SELECT 1 
								FROM fact_materialmovement cx1 INNER JOIN dim_movementtype dmt1
									ON cx1.Dim_MovementTypeid = dmt1.Dim_MovementTypeid 
								WHERE cx1.dd_debitcreditid = 'Debit' AND dmt1.MovementType = 101
								     AND cx1.dim_Partid = cx.dim_Partid AND cx1.dim_storagelocationid = cx.dim_storagelocationid
								     AND cx1.dd_DocumentNo = cx.dd_DocumentNo AND cx1.dd_DocumentItemNo = cx.dd_DocumentItemNo
								     AND cx1.ct_Quantity = cx.ct_Quantity)));

\i /db/schema_migration/bin/wrapper_optimizedb.sh materialmovement_tmp_001;

DROP TABLE IF EXISTS tmp_mm_lastrcptinfo_001;
CREATE TABLE tmp_mm_lastrcptinfo_001 AS
SELECT cx.PartNumber,
	cx.PlantCode,
	cx.StorLocCode,
	cx.ct_Quantity,
	cx.PostingDate
FROM  materialmovement_tmp_001 cx
WHERE cx.RowSeqNo = 1;

DROP TABLE IF EXISTS mm_tmp_maxseqno_001;
CREATE TABLE mm_tmp_maxseqno_001 AS
SELECT a.PartNumber, 
	  a.PlantCode, 
	  a.StorLocCode, 
	  MAX(a.RowSeqNo) MaxRowSeqNo
FROM materialmovement_tmp_001 a
GROUP BY a.PartNumber, 
	  a.PlantCode, 
	  a.StorLocCode;

DROP TABLE IF EXISTS materialmovement_tmp_002;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_mm_lastrcptinfo_001;
\i /db/schema_migration/bin/wrapper_optimizedb.sh mm_tmp_maxseqno_001;

CREATE TABLE materialmovement_tmp_002 AS
SELECT a.fact_materialmovementid,
	  a.ct_Quantity,
	  a.PartNumber, 
	  a.PlantCode, 
	  a.StorLocCode, 
	  a.PostingDate,
	  a.RowSeqNo,
	  (SUM(b.ct_Quantity) - a.ct_Quantity) StockTotalQty,
	  c.MaxRowSeqNo
FROM materialmovement_tmp_001 a
	inner join materialmovement_tmp_001 b on a.PartNumber = b.PartNumber
						and a.PlantCode = b.PlantCode
						and a.StorLocCode = b.StorLocCode
	inner join mm_tmp_maxseqno_001 c on a.PartNumber = c.PartNumber
						and a.PlantCode = c.PlantCode
						and a.StorLocCode = c.StorLocCode
WHERE a.RowSeqNo >= b.RowSeqNo
GROUP BY a.fact_materialmovementid,
	  a.ct_Quantity,
	  a.PartNumber, 
	  a.PlantCode, 
	  a.StorLocCode, 
	  a.PostingDate,
	  a.RowSeqNo,
	  c.MaxRowSeqNo;

\i /db/schema_migration/bin/wrapper_optimizedb.sh materialmovement_tmp_002;

DROP TABLE IF EXISTS materialmovement_tmp_new;
CREATE TABLE materialmovement_tmp_new AS
SELECT cx.fact_materialmovementid,
	cx.dd_MaterialDocNo dd_MaterialDocNo,
	cx.dd_MaterialDocItemNo dd_MaterialDocItemNo,
	cx.dd_MaterialDocYear dd_MaterialDocYear,
	ifnull(cx.dd_DocumentNo,'Not Set') dd_DocumentNo,
	cx.dd_DocumentItemNo dd_DocumentItemNo,
	ifnull(cx.dd_SalesOrderNo,'Not Set') dd_SalesOrderNo,
	cx.dd_SalesOrderItemNo dd_SalesOrderItemNo,
	ifnull(cx.dd_SalesOrderDlvrNo,0) dd_SalesOrderDlvrNo,
	cx.dd_BatchNumber dd_BatchNumber,
	cx.dd_ValuationType dd_ValuationType,
	cx.dd_debitcreditid dd_debitcreditid,
	cx.amt_LocalCurrAmt amt_LocalCurrAmt,
	cx.amt_DeliveryCost amt_DeliveryCost,
	cx.amt_ExchangeRate_GBL amt_ExchangeRate_GBL,
	cx.amt_AltPriceControl amt_AltPriceControl,
	cx.amt_OnHand_ValStock amt_OnHand_ValStock,
	cx.ct_Quantity ct_Quantity,
	cx.ct_Quantity ct_QtyEntryUOM,
	cx.Dim_MovementTypeid Dim_MovementTypeid,
	cx.dim_Companyid dim_Companyid,
	cx.Dim_Partid Dim_Partid,
	cx.Dim_Plantid Dim_Plantid,
	cx.Dim_StorageLocationid Dim_StorageLocationid,
	cx.Dim_Vendorid Dim_Vendorid,
	cx.dim_DateIDPostingDate dim_DateIDPostingDate,
	cx.dim_MovementIndicatorid dim_MovementIndicatorid,
	cx.dim_CostCenterid dim_CostCenterid,
	cx.dim_specialstockid dim_specialstockid,
	cx.Dim_StockTypeid Dim_StockTypeid,
	cx.Dim_Currencyid Dim_Currencyid,
	cx.amt_ExchangeRate amt_ExchangeRate,	
	a.PartNumber,
	a.PlantCode,
	a.StorLocCode,	
	a.PostingDate,	
	a.StockTotalQty,
	b.ct_Quantity LatestReceiptQty,
	b.PostingDate LatestPostingDate,
	a.RowSeqNo,
	a.MaxRowSeqNo
FROM fact_materialmovement cx
	inner join materialmovement_tmp_002 a on a.fact_materialmovementid = cx.fact_materialmovementid
	inner join tmp_mm_lastrcptinfo_001 b on a.PartNumber = b.PartNumber
						and a.PlantCode = b.PlantCode
						and a.StorLocCode = b.StorLocCode
	inner join tmp_MARD_TotStk_001 c on a.PartNumber = c.PartNumber
						and a.PlantCode = c.PlantCode
						and a.StorLocCode = c.StorLocCode
						and a.StockTotalQty < c.TotalStock;

\i /db/schema_migration/bin/wrapper_optimizedb.sh materialmovement_tmp_new;

DROP TABLE IF EXISTS tmp_c2;
CREATE TABLE tmp_c2 AS
SELECT PartNumber,PlantCode,StorLocCode,max(StockTotalQty + ct_QtyEntryUOM) as max_stk
FROM materialmovement_tmp_new c1
GROUP BY PartNumber,PlantCode,StorLocCode;

DROP TABLE IF EXISTS tmp_c3;
CREATE TABLE tmp_c3 AS
SELECT c1.*
FROM materialmovement_tmp_new c1, tmp_c2 c2
WHERE c2.PartNumber = c1.PartNumber AND c2.PlantCode = c1.PlantCode AND c2.StorLocCode = c1.StorLocCode
	AND   c1.StockTotalQty + c1.ct_QtyEntryUOM = ifnull(c2.max_stk,0) ;


/*** MBEW NO BWTAR values ***/

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
    and ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x 
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
                                             AND ((x.VPRSV = 'S' AND x.STPRS > 0) OR (x.VPRSV = 'V' AND x.VERPR > 0)))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR AS
SELECT DISTINCT m.MATNR,
       m.BWKEY,
       m.LFGJA,
       m.LFMON,
       m.VPRSV,
       m.VERPR,
       m.STPRS,
       m.PEINH,
       m.SALK3,
       m.BWTAR,
       m.MBEW_LBKUM,
       ((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, 
       m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;	

drop table if exists tmp_MBEWH_NO_BWTAR;
create table tmp_MBEWH_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEWH_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
	and not exists (select 1 from mbew_no_bwtar a where a.MATNR = m.MATNR and a.BWKEY = m.BWKEY)
    and ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEWH_NO_BWTAR x 
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
                                             AND ((x.VPRSV = 'S' AND x.STPRS > 0) OR (x.VPRSV = 'V' AND x.VERPR > 0)))
group by MATNR,BWKEY;

INSERT INTO tmp2_MBEW_NO_BWTAR
SELECT DISTINCT m.MATNR,
       m.BWKEY,
       m.LFGJA,
       m.LFMON,
       m.VPRSV,
       m.VERPR,
       m.STPRS,
       m.PEINH,
       m.SALK3,
       m.BWTAR,
       m.MBEWH_LBKUM MBEW_LBKUM,
	((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, 
	m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEWH_NO_BWTAR m2,MBEWH_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;


ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.STPRS / b.PEINH), 18,5)
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.VERPR / b.PEINH), 18,5)
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

/* divide inventory stock into material movement qty */

DROP TABLE IF EXISTS tmp_fact_invaging_001;
CREATE TABLE tmp_fact_invaging_001 AS
SELECT b.fact_materialmovementid,
	a.PartNumber,
	a.PlantCode,
	a.StorLocCode,
	case when a.MARD_UMLME_1 > b.StockTotalQty 
		then case when a.MARD_UMLME_1 <= (b.ct_Quantity + b.StockTotalQty) then a.MARD_UMLME_1 else b.ct_Quantity end
		else 0
	end MARD_UMLME_1, 
	case when (a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty
		then case when (a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.ct_Quantity + b.StockTotalQty) 
			  then case when a.MARD_UMLME_1 > b.StockTotalQty then a.MARD_LABST_2 else (a.MARD_LABST_2+a.MARD_UMLME_1) - b.StockTotalQty end
			  else case when a.MARD_UMLME_1 > (b.ct_Quantity + b.StockTotalQty) then 0
					else case when a.MARD_UMLME_1 > b.StockTotalQty then b.ct_Quantity - (a.MARD_UMLME_1-b.StockTotalQty) else b.ct_Quantity end
				end + case when b.RowSeqNo = b.MaxRowSeqNo and a.MARD_LABST_2 > 0 and (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.StockTotalQty+b.ct_Quantity)
					   then (a.MARD_LABST_2+a.MARD_UMLME_1) - (b.StockTotalQty+b.ct_Quantity) 
					   else 0 end
			end
		else 0
	end MARD_LABST_2, 
	case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty
		then case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.ct_Quantity + b.StockTotalQty) 
			  then case when (a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty then a.MARD_SPEME_3 else (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) -  b.StockTotalQty end
			  else case when (a.MARD_LABST_2+a.MARD_UMLME_1) > (b.ct_Quantity + b.StockTotalQty) then 0
				    else case when (a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty then b.ct_Quantity - (a.MARD_LABST_2+a.MARD_UMLME_1-b.StockTotalQty) else b.ct_Quantity end
				end + case when b.RowSeqNo = b.MaxRowSeqNo and a.MARD_SPEME_3 > 0 and (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.StockTotalQty+b.ct_Quantity)
					   then (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.StockTotalQty+b.ct_Quantity) 
					   else 0 end
			 end
		else 0
	end MARD_SPEME_3, 
	case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty
		then case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.ct_Quantity + b.StockTotalQty) 
			  then case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty then a.MARD_INSME_4 
					else (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) -  b.StockTotalQty end
			   else case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.ct_Quantity + b.StockTotalQty) then 0
					else case when (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty 
						  then b.ct_Quantity - (a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1-b.StockTotalQty) 
						  else b.ct_Quantity end
				end + case when b.RowSeqNo = b.MaxRowSeqNo and a.MARD_INSME_4 > 0 and (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.StockTotalQty+b.ct_Quantity)
					   then (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.StockTotalQty+b.ct_Quantity) 
					   else 0 end
			 end
		else 0
	end MARD_INSME_4, 
	case when (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty
		then case when (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) <= (b.ct_Quantity + b.StockTotalQty) 
			  then case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty then a.MARD_EINME_5 
					else (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) -  b.StockTotalQty end
			  else case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.ct_Quantity + b.StockTotalQty) then 0
					else case when (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > b.StockTotalQty 
						  then b.ct_Quantity - (a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1-b.StockTotalQty)
						  else b.ct_Quantity
						end
				end + case when b.RowSeqNo = b.MaxRowSeqNo and a.MARD_EINME_5 > 0 and (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) > (b.StockTotalQty+b.ct_Quantity)
						then (a.MARD_EINME_5+a.MARD_INSME_4+a.MARD_SPEME_3+a.MARD_LABST_2+a.MARD_UMLME_1) - (b.StockTotalQty+b.ct_Quantity) 
						else 0 end
			 end
		else 0
	end MARD_EINME_5,	
	b.PostingDate,	
	b.dim_DateIDPostingDate,
	b.StockTotalQty,
	b.LatestReceiptQty,
	b.LatestPostingDate,
	b.amt_ExchangeRate_GBL,
	b.dd_BatchNumber,
	b.dd_ValuationType,
	b.Dim_StorageLocationid, 
	b.dim_Companyid,
	b.dim_currencyid, 
	b.dim_stocktypeid, 
	b.dim_specialstockid,
	b.Dim_MovementIndicatorid, 
	b.dim_CostCenterid,
	b.Dim_MovementTypeid,
	b.RowSeqNo,
	b.MaxRowSeqNo,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KSPEM ELSE 0 END MARD_KSPEM, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_KINSM ELSE 0 END MARD_KINSM, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_KEINM ELSE 0 END MARD_KEINM,
	CASE b.RowSeqNo WHEN 1 THEN MARD_KLABS ELSE 0 END MARD_KLABS, 
	CASE b.RowSeqNo WHEN 1 THEN MARD_RETME ELSE 0 END MARD_RETME 
FROM tmp_MARD_TotStk_001 a
	inner join materialmovement_tmp_new b on a.PartNumber = b.PartNumber
						and a.PlantCode = b.PlantCode
						and a.StorLocCode = b.StorLocCode;

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp_fact_invaging_001;

DROP TABLE IF EXISTS tmp_MARD_TotStk_001;
DROP TABLE IF EXISTS materialmovement_tmp_001;
DROP TABLE IF EXISTS materialmovement_tmp_002;
DROP TABLE IF EXISTS tmp_mm_lastrcptinfo_001;
DROP TABLE IF EXISTS mm_tmp_maxseqno_001;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;
\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp2_MBEW_NO_BWTAR;

/*Insert 1 */

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MARD';

INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, 
	ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, 
	amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, 
	amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, amt_OnHand, amt_OnHand_GBL, ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, 
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, 
	dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, 
	Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_StockCategoryid,fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,
	Dim_DocumentTypeid, Dim_IncoTermid,Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,
	amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost, amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,
	amt_PreviousPrice,amt_CommericalPrice1, amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL) 
SELECT MARD_LABST_2 ct_StockQty, 
	MARD_EINME_5 ct_TotalRestrictedStock, 
	MARD_INSME_4 ct_StockInQInsp, 
	MARD_SPEME_3 ct_BlockedStock, 
	MARD_UMLME_1 ct_StockInTransfer, 
	MARD_KSPEM ct_BlockedConsgnStock, 
	MARD_KINSM ct_ConsgnStockInQInsp, 
	MARD_KEINM ct_RestrictedConsgnStock,
	MARD_KLABS ct_UnrestrictedConsgnStock, 
	MARD_RETME ct_BlockedStockReturns, 
	CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_LABST_2),15,2) END amt_StockValueAmt,
	ifnull(((CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_LABST_2) ,15,2) END ) * c1.amt_ExchangeRate_GBL), 0) amt_StockValueAmt_GBL,
	CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE Decimal((b.multiplier * MARD_SPEME_3),15,2) END amt_BlockedStockAmt,
	ifnull(((CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_SPEME_3) ,15,2) END) * c1.amt_ExchangeRate_GBL), 0) amt_BlockedStockAmt_GBL,
	CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_INSME_4),15,2) END amt_StockInQInspAmt,
	ifnull(((CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_INSME_4),15,2) END) * c1.amt_ExchangeRate_GBL), 0) amt_StockInQInspAmt_GBL,
	CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_UMLME_1),15,2) END amt_StockInTransferAmt,
	ifnull(((CASE WHEN MARD_UMLME_1 = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_UMLME_1),15,2) END) * c1.amt_ExchangeRate_GBL), 0) amt_StockInTransferAmt_GBL,
	CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_KLABS),15,2) END amt_UnrestrictedConsgnStockAmt,
	ifnull(((CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE decimal((b.multiplier * MARD_KLABS) ,15,2) END ) * c1.amt_ExchangeRate_GBL), 0) amt_UnrestrictedConsgnStockAmt_GBL,
	b.multiplier amt_StdUnitPrice,
	ifnull((b.multiplier * c1.amt_ExchangeRate_GBL), 0) amt_StdUnitPrice_GBL,
	CASE WHEN MARD_LABST_2 + MARD_INSME_4 + MARD_SPEME_3 = MBEW_LBKUM THEN SALK3
		ELSE decimal((CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END
			  + CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END
			  + CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END),15,2)
	END amt_OnHand,
	ifnull(((CASE WHEN MARD_LABST_2 + MARD_INSME_4 + MARD_SPEME_3 = MBEW_LBKUM THEN SALK3
		       ELSE decimal((CASE WHEN MARD_LABST_2 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_LABST_2) END
				  + CASE WHEN MARD_INSME_4 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_INSME_4) END
				  + CASE WHEN MARD_SPEME_3 = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * MARD_SPEME_3) END),15,2) 
		  END ) * c1.amt_ExchangeRate_GBL), 0) amt_OnHand_GBL,
	ifnull(c1.LatestReceiptQty,0) LastReceivedQty, 
	c1.dd_BatchNumber BatchNo, 
	ifnull(c1.dd_ValuationType,'Not Set') ValuationType, 
	'Not Set' DocumentNo,
	0 DocumentItemNo, 
	dmt.MovementType MovementType, 
	c1.dim_DateIDPostingDate StorageLocEntryDate, 
	ifnull((select lrd.dim_dateid from dim_date lrd where c1.LatestPostingDate = lrd.DateValue and lrd.CompanyCode = p.CompanyCode),1) LastReceivedDate,
	dp.Dim_PartID, 
	p.dim_plantid, 
	c1.Dim_StorageLocationid, 
	c1.dim_Companyid,
	1 dim_vendorid, 
	c1.dim_currencyid, 
	c1.dim_stocktypeid, 
	c1.dim_specialstockid,
	ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
	pg.Dim_PurchaseGroupid, 
	dph.dim_producthierarchyid, 
	uom.Dim_UnitOfMeasureid, 
	c1.Dim_MovementIndicatorid, 
	c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id FROM NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	c1.amt_ExchangeRate_GBL
FROM tmp_fact_invaging_001 c1 
	INNER JOIN dim_plant p ON c1.PlantCode = p.PlantCode
	INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = c1.PartNumber AND b.BWKEY = p.ValuationArea
	INNER JOIN dim_movementtype dmt ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid
	INNER JOIN dim_part dp ON dp.PartNumber = c1.PartNumber AND dp.Plant = c1.PlantCode
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode,
	tmp_stkcategory_001 stkc;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select ifnull(max(fact_inventoryagingid),0) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';
	
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

/* Changes in above query - 
	a) Found out all columns where were not insert column list. Populate the default values ( according to earlier defn in MySQL )
	b) ifnull for c1.LatestReceiptQty,lrd.dim_dateid and c1.dd_ValuationType ( which were found after debugging errors )
	c) Fact row_number logic */
/* Above query works now */


/* INSERT 2 */

/* For now replaced getExchangeRate return value with 1 ( checked that tcurr does not have anything in fossil2 or the qa db on mysql 
   For dbs which have data in this table, the function would need to be converted to SQLs. 
   Already started that - refer getExchangeRate_func.sql */

drop table if exists tmp_tbl_StorageLocEntryDate;
CREATE table  tmp_tbl_StorageLocEntryDate AS 
SELECT  c2.PartNumber, c2.PlantCode, c2.StorLocCode,MAX(c2.LatestPostingDate) as LatestPostingDate
FROM	 materialmovement_tmp_new c2
GROUP BY c2.PartNumber, c2.PlantCode, c2.StorLocCode;


INSERT INTO fact_inventoryaging_tmp_populate(
	ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer,
	ct_BlockedConsgnStock, ct_ConsgnStockInQInsp, ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns,
	amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, amt_BlockedStockAmt_GBL, amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, amt_UnrestrictedConsgnStockAmt_GBL,
	amt_StdUnitPrice, amt_StdUnitPrice_GBL, 
	amt_OnHand,	/*Lokesh 26apr*/
	amt_OnHand_GBL,/*Lokesh 26apr*/
	ct_LastReceivedQty, dd_BatchNo, dd_ValuationType,
	dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid,
	dim_Partid, dim_Plantid, dim_StorageLocationid, dim_Companyid, dim_Vendorid,
	dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid,
	dim_producthierarchyid, Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_stockcategoryid,
	fact_inventoryagingid, Dim_ConsumptionTypeid, Dim_DocumentStatusid, Dim_DocumentTypeid, Dim_IncoTermid,
	Dim_ItemCategoryid, Dim_ItemStatusid, Dim_Termid, Dim_PurchaseMiscid, ct_StockInTransit,
	amt_StockInTransitAmt, amt_StockInTransitAmt_GBL, Dim_SupplyingPlantId, amt_MtlDlvrCost, amt_OverheadCost,
	amt_OtherCost, amt_WIPBalance, ct_WIPAging, amt_MovingAvgPrice, amt_PreviousPrice,
	amt_CommericalPrice1, amt_PlannedPrice1, amt_PrevPlannedPrice, amt_CurPlannedPrice, Dim_DateIdPlannedPrice2,
	Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL)
SELECT DISTINCT a.MARD_LABST ct_StockQty, a.MARD_EINME ct_TotalRestrictedStock, a.MARD_INSME ct_StockInQInsp, a.MARD_SPEME ct_BlockedStock, a.MARD_UMLME ct_StockInTransfer,
	a.MARD_KSPEM ct_BlockedConsgnStock, a.MARD_KINSM ct_ConsgnStockInQInsp, a.MARD_KEINM ct_RestrictedConsgnStock, a.MARD_KLABS ct_UnrestrictedConsgnStock, 
	a.MARD_RETME ct_BlockedStockReturns,
	CASE WHEN MARD_LABST = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST) END StockValueAmt, 
	ifnull((CASE WHEN MARD_LABST = MBEW_LBKUM THEN SALK3
		ELSE (b.multiplier * a.MARD_LABST)  END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) StockValueAmt_GBL,
	CASE WHEN MARD_SPEME = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME) END amt_BlockedStockAmt, 
	ifnull(CASE WHEN MARD_SPEME = MBEW_LBKUM THEN SALK3
		ELSE (b.multiplier * a.MARD_SPEME) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)), 0) amt_BlockedStockAmt_GBL, 
	CASE WHEN MARD_INSME = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME) END amt_StockInQInspAmt,
	ifnull((CASE WHEN MARD_INSME = MBEW_LBKUM THEN SALK3
		ELSE (b.multiplier * a.MARD_INSME) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StockInQInspAmt_GBL, 
	CASE WHEN MARD_UMLME = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_UMLME) END amt_StockInTransferAmt, 
	ifnull((CASE WHEN MARD_UMLME = MBEW_LBKUM THEN SALK3
		ELSE (b.multiplier * a.MARD_UMLME) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StockInTransferAmt_GBL, 
	CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_KLABS) END amt_UnrestrictedConsgnStockAmt, 
	ifnull((CASE WHEN MARD_KLABS = MBEW_LBKUM THEN SALK3
		ELSE (b.multiplier * a.MARD_KLABS) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_UnrestrictedConsgnStockAmt_GBL,
	b.multiplier amt_StdUnitPrice, 
	ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)  amt_StdUnitPrice_GBL, 
	/* LK : 26 Apr: Add 2 columns here - Change starts */
	CASE WHEN MARD_LABST+MARD_INSME+MARD_SPEME = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MARD_LABST = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST) END 
			+ CASE WHEN MARD_INSME = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME) END
			+ CASE WHEN MARD_SPEME = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME) END 
		   ) END amt_OnHand,
	ifnull((CASE WHEN MARD_LABST+MARD_INSME+MARD_SPEME = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MARD_LABST = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_LABST) END 
			+ CASE WHEN MARD_INSME = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_INSME) END
			+ CASE WHEN MARD_SPEME = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MARD_SPEME) END 
		   ) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)
	amt_OnHand_GBL,
	/* LK : 26 Apr: Added 2 columns here - Change ends */
	ifnull((SELECT c2.LatestReceiptQty FROM tmp_c3 c2
		WHERE c2.PartNumber = a.MARD_MATNR AND c2.PlantCode = a.MARD_WERKS AND c2.StorLocCode = a.MARD_LGORT),0) LastReceivedQty, 
	'Not Set' BatchNo, 'Not Set' ValuationType,
	'Not Set' dd_DocumentNo, 0 dd_DocumentItemNo, 'Not Set' MovementType,
	ifnull((SELECT lrd.Dim_Dateid FROM tmp_tbl_StorageLocEntryDate c2 inner join dim_date lrd on lrd.DateValue = c2.LatestPostingDate 
		WHERE c2.PartNumber = a.MARD_MATNR AND c2.PlantCode = a.MARD_WERKS AND c2.StorLocCode = a.MARD_LGORT and lrd.CompanyCode = p.CompanyCode), sled.Dim_Dateid) StorageLocEntryDate,
	ifnull((SELECT lrd.Dim_Dateid FROM tmp_tbl_StorageLocEntryDate c2 inner join dim_date lrd on lrd.DateValue = c2.LatestPostingDate 
		WHERE c2.PartNumber = a.MARD_MATNR AND c2.PlantCode = a.MARD_WERKS AND c2.StorLocCode = a.MARD_LGORT and lrd.CompanyCode = p.CompanyCode), sled.Dim_Dateid) LastReceivedDate,
	Dim_Partid, p.dim_plantid, Dim_StorageLocationid, dim_companyid, 1 dim_vendorid,
	dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po where po.PurchaseOrgCode = p.PurchOrg), 1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,  dim_producthierarchyid, Dim_UnitOfMeasureid, 1 Dim_MovementIndicatorid, 1 dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over () , 
	1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL
     FROM MARD a 
	  INNER JOIN dim_plant p ON a.MARD_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARD_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARD_MATNR AND dp.Plant = a.MARD_WERKS
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.MARD_LGORT AND sl.Plant = a.MARD_WERKS AND sl.RowIsCurrent = 1
          INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode	/* LK: 26 Apr change */
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
    WHERE NOT EXISTS (SELECT 1 FROM fact_inventoryaging_tmp_populate c2 WHERE c2.Dim_Partid = dp.Dim_Partid AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_StorageLocationid = sl.Dim_StorageLocationid);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

 UPDATE NUMBER_FOUNTAIN 
 SET max_id = ifnull((select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 WHERE table_name = 'fact_inventoryaging_tmp_populate';

/*Insert 3 */

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedConsgnStock,
	ct_ConsgnStockInQInsp, ct_RestrictedConsgnStock, ct_UnrestrictedConsgnStock, ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL,
	amt_BlockedStockAmt, amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL,
	amt_UnrestrictedConsgnStockAmt, amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL, ct_LastReceivedQty, dd_BatchNo,
	dd_ValuationType, dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid,
	dim_Plantid, dim_StorageLocationid, dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid,
	dim_producthierarchyid, Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_stockcategoryid,
	fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL)
SELECT a.MARD_LABST ct_StockQty, a.MARD_EINME ct_TotalRestrictedStock, a.MARD_INSME ct_StockInQInsp, a.MARD_SPEME ct_BlockedStock,
	a.MARD_UMLME ct_StockInTransfer, a.MARD_KSPEM ct_BlockedConsgnStock, a.MARD_KINSM ct_ConsgnStockInQInsp, a.MARD_KEINM ct_RestrictedConsgnStock,
	a.MARD_KLABS ct_UnrestrictedConsgnStock, a.MARD_RETME ct_BlockedStockReturns, 0 StockValueAmt, 0 StockValueAmt_GBL, 0 amt_BlockedStockAmt,
	0 amt_BlockedStockAmt_GBL, 0 amt_StockInQInspAmt, 0 amt_StockInQInspAmt_GBL, 0 amt_StockInTransferAmt, 0 amt_StockInTransferAmt_GBL,
	0 amt_UnrestrictedConsgnStockAmt, 0 amt_UnrestrictedConsgnStockAmt_GBL, 0 amt_StdUnitPrice, 0 amt_StdUnitPrice_GBL,
	ifnull((SELECT c2.LatestReceiptQty FROM tmp_c3 c2
		WHERE c2.PartNumber = a.MARD_MATNR AND c2.PlantCode = a.MARD_WERKS AND c2.StorLocCode = a.MARD_LGORT),0) LastReceivedQty,
	'Not Set' BatchNo, 'Not Set' ValuationType, 'Not Set' dd_DocumentNo, 0 dd_DocumentItemNo, 'Not Set' MovementType,
	ifnull((SELECT lrd.Dim_Dateid FROM tmp_tbl_StorageLocEntryDate c2 inner join dim_date lrd on lrd.DateValue = c2.LatestPostingDate
		WHERE c2.PartNumber = a.MARD_MATNR AND c2.PlantCode = a.MARD_WERKS AND c2.StorLocCode = a.MARD_LGORT and lrd.CompanyCode = p.CompanyCode),sled.Dim_Dateid) StorageLocEntryDate,
	ifnull((SELECT lrd.Dim_Dateid FROM tmp_tbl_StorageLocEntryDate c2 inner join dim_date lrd on lrd.DateValue = c2.LatestPostingDate
		WHERE c2.PartNumber = a.MARD_MATNR AND c2.PlantCode = a.MARD_WERKS AND c2.StorLocCode = a.MARD_LGORT and lrd.CompanyCode = p.CompanyCode),sled.Dim_Dateid) LastReceivedDate,
	Dim_Partid, p.dim_plantid, Dim_StorageLocationid, dim_companyid, 1 dim_vendorid, dim_currencyid, 1 dim_stocktypeid, 1 dim_specialstockid,
	ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
	       where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
	Dim_PurchaseGroupid, dim_producthierarchyid, Dim_UnitOfMeasureid, 1 Dim_MovementIndicatorid, 1 dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over () , 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL
FROM MARD a
	INNER JOIN dim_plant p ON a.MARD_WERKS = p.PlantCode
	INNER JOIN dim_part dp ON dp.PartNumber = a.MARD_MATNR AND dp.Plant = a.MARD_WERKS
	INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
	INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
	INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
	INNER JOIN dim_storagelocation sl ON     sl.LocationCode = a.MARD_LGORT AND sl.Plant = a.MARD_WERKS AND sl.RowIsCurrent = 1
	/***  INNER JOIN dim_date sled ON sled.DateValue = date_sub(curdate(),interval 4 year) AND sled.CompanyCode = p.CompanyCode ***/
	INNER JOIN dim_date sled ON sled.DateValue = ifnull(a.MARD_DLINL,a.MARD_ERSDA) AND sled.CompanyCode = p.CompanyCode	/* LK: 26 Apr change */
	INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
	INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency, 
	tmp_GlobalCurr_001 gbl,
	tmp_stkcategory_001 stkc
WHERE NOT EXISTS
	(SELECT 1 FROM fact_inventoryaging_tmp_populate c2 
	 WHERE c2.Dim_Partid = dp.Dim_Partid 
		AND c2.dim_plantid = p.dim_plantid
		AND c2.Dim_StorageLocationid = sl.Dim_StorageLocationid);


\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';


/*Delete after Insert 3 */
delete from MCHB
where exists (SELECT 1 FROM MARD
              WHERE     MCHB_MATNR = MARD_MATNR
                    AND MCHB_WERKS = MARD_WERKS
                    AND MCHB_LGORT = MARD_LGORT);

\i /db/schema_migration/bin/wrapper_optimizedb.sh tmp2_MBEW_NO_BWTAR;

/*Insert 4 */

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MCHB';

INSERT INTO fact_inventoryaging_tmp_populate (
	ct_StockQty,ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedStockReturns, amt_StockValueAmt,
	amt_StockValueAmt_GBL, amt_BlockedStockAmt, amt_BlockedStockAmt_GBL, amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, 
	amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt, amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, 
	amt_StdUnitPrice_GBL, 
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid,
	dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid, dim_specialstockid,
	Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, Dim_UnitOfMeasureid, Dim_MovementIndicatorid, dim_CostCenterid, dim_stockcategoryid,
	fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL)
SELECT a.MCHB_CLABS ct_StockQty, a.MCHB_CEINM ct_TotalRestrictedStock, a.MCHB_CINSM ct_StockInQInsp, a.MCHB_CSPEM ct_BlockedStock,
	a.MCHB_CUMLM ct_StockInTransfer, a.MCHB_CRETM ct_BlockedStockReturns,
	CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3 
	 ELSE (b.multiplier * a.MCHB_CLABS) END StockValueAmt,
	ifnull(CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CLABS) END * c1.amt_ExchangeRate_GBL, 0) StockValueAmt_GBL,
	(b.multiplier * a.MCHB_CSPEM) amt_BlockedStockAmt,
	ifnull(((b.multiplier * a.MCHB_CSPEM) * c1.amt_ExchangeRate_GBL), 0) amt_BlockedStockAmt_GBL,
	(b.multiplier * a.MCHB_CINSM) amt_StockInQInspAmt,
	ifnull(((b.multiplier * a.MCHB_CINSM) * c1.amt_ExchangeRate_GBL), 0) amt_StockInQInspAmt_GBL,
	(b.multiplier * a.MCHB_CUMLM) amt_StockInTransferAmt,
	ifnull(((b.multiplier * a.MCHB_CUMLM) * c1.amt_ExchangeRate_GBL), 0) amt_StockInTransferAmt_GBL,
	0 amt_UnrestrictedConsgnStockAmt,0 amt_UnrestrictedConsgnStockAmt_GBL,b.multiplier amt_StdUnitPrice,
	ifnull((b.multiplier  * c1.amt_ExchangeRate_GBL), 0) amt_StdUnitPrice_GBL,
	/* LK: 26 Apr change Start */
	CASE WHEN MCHB_CLABS+MCHB_CSPEM+MCHB_CINSM = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CLABS) END 
			+ CASE WHEN MCHB_CSPEM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CSPEM) END
			+ CASE WHEN MCHB_CINSM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CINSM) END 
		   ) END amt_OnHand,
	 ifnull((CASE WHEN MCHB_CLABS+MCHB_CSPEM+MCHB_CINSM = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CLABS) END 
			+ CASE WHEN MCHB_CSPEM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CSPEM) END
			+ CASE WHEN MCHB_CINSM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CINSM) END 
		   ) END * c1.amt_ExchangeRate_GBL), 0)
	amt_OnHand_GBL,
	/* LK: 26 Apr change End */
	c1.ct_Quantity LastReceivedQty, a.MCHB_CHARG BatchNo, ifnull(c1.dd_ValuationType,'Not Set') ValuationType, 'Not Set' DocumentNo, 0 DocumentItemNo,
	dmt.MovementType MovementType, sled.Dim_Dateid StorageLocEntryDate, c1.dim_DateIDPostingDate LastReceivedDate, dp.Dim_PartID, p.dim_plantid,
	sl.Dim_StorageLocationid, c1.dim_Companyid, 1 dim_vendorid, c1.dim_currencyid, c1.dim_stocktypeid, c1.dim_specialstockid, c1.Dim_PurchaseOrgid,
	pg.Dim_PurchaseGroupid, dph.dim_producthierarchyid, uom.Dim_UnitOfMeasureid, c1.Dim_MovementIndicatorid, c1.dim_CostCenterid,
	stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over () , 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	c1.amt_ExchangeRate_GBL
FROM MCHB a INNER JOIN dim_part dp ON dp.PartNumber = a.MCHB_MATNR AND dp.Plant = a.MCHB_WERKS
     INNER JOIN dim_plant p ON a.MCHB_WERKS = p.PlantCode
     INNER JOIN tmp2_MBEW_NO_BWTAR b ON     b.MATNR = a.MCHB_MATNR AND b.BWKEY = p.ValuationArea
     INNER JOIN (fact_materialmovement c1 INNER JOIN dim_movementtype dmt
               ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid AND dmt.MovementType = 101) ON c1.dim_Partid = dp.dim_partid
           AND c1.dd_BatchNumber = a.MCHB_CHARG
           AND concat(c1.dd_MaterialDocYear,LPAD(c1.dd_MaterialDocNo,10,'0'),LPAD(c1.dd_MaterialDocItemNo,5,'0'))
               = (select x.dd_MaterialDocYearItem
                  from fact_materialmovement_tmp_invagin x
                   where x.Dim_Partid = c1.Dim_Partid
                       AND x.Dim_MovementTypeid = c1.Dim_MovementTypeid
                       AND x.dd_BatchNumber = c1.dd_BatchNumber)
     INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
     INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy AND dph.RowIsCurrent = 1
     INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
     INNER JOIN dim_company co ON co.dim_Companyid = c1.dim_Companyid
     INNER JOIN dim_storagelocation sl ON sl.LocationCode = a.MCHB_LGORT
					  AND sl.Plant = a.MCHB_WERKS AND sl.RowIsCurrent = 1 
     INNER JOIN dim_date sled ON sled.DateValue = a.MCHB_ERSDA AND sled.CompanyCode = co.CompanyCode,
     tmp_stkcategory_001 stkc;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

/*Insert 5 */
/*call vectorwise(combine 'fact_inventoryaging_tmp_populate')*/

DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins;
CREATE TABLE tmp_ivaging_ins5_notexists_ins
AS
SELECT c1.dim_partid,c1.dd_BatchNumber,dmt.MovementType
FROM fact_materialmovement c1 INNER JOIN dim_movementtype dmt
                    ON c1.Dim_MovementTypeid = dmt.Dim_MovementTypeid AND dmt.MovementType in (105,501,511,521);        /* LK:26 Apr change*/

DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_del;
CREATE TABLE tmp_ivaging_ins5_notexists_del
AS
SELECT c2.dim_partid,c2.dd_BatchNumber,dmt1.MovementType
FROM fact_materialmovement c2 INNER JOIN dim_movementtype dmt1
                    ON c2.Dim_MovementTypeid = dmt1.Dim_MovementTypeid AND dmt1.MovementType = 101;

call vectorwise(combine 'tmp_ivaging_ins5_notexists_ins-tmp_ivaging_ins5_notexists_del');

DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins_c1;
CREATE TABLE tmp_ivaging_ins5_notexists_ins_c1
AS
SELECT distinct c1.*,t.MovementType
from fact_materialmovement c1,tmp_ivaging_ins5_notexists_ins t
WHERE c1.dim_partid = t.dim_partid
AND c1.dd_BatchNumber = t.dd_BatchNumber;



INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer, ct_BlockedStockReturns,
                                amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, amt_BlockedStockAmt_GBL, amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL, amt_UnrestrictedConsgnStockAmt,
                                amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL,
				/* LK : 26 Apr change Starts */
				amt_OnHand,
				amt_OnHand_GBL,
				/* LK : 26 Apr change Ends */
				ct_LastReceivedQty, dd_BatchNo,
                                dd_ValuationType, dd_DocumentNo, dd_DocumentItemNo, dd_MovementType, dim_StorageLocEntryDateid, dim_LastReceivedDateid,
                                dim_Partid, dim_Plantid, dim_StorageLocationid, dim_Companyid, dim_Vendorid, dim_Currencyid, dim_StockTypeid,
                                dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, Dim_UnitOfMeasureid,
				Dim_MovementIndicatorid, dim_CostCenterid, dim_stockcategoryid,
				fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
   SELECT a.MCHB_CLABS ct_StockQty, a.MCHB_CEINM ct_TotalRestrictedStock, a.MCHB_CINSM ct_StockInQInsp, a.MCHB_CSPEM ct_BlockedStock,
          a.MCHB_CUMLM ct_StockInTransfer, a.MCHB_CRETM ct_BlockedStockReturns,
	CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3
	 ELSE (b.multiplier * a.MCHB_CLABS) END StockValueAmt,
	ifnull((CASE  WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3
	 ELSE (b.multiplier * a.MCHB_CLABS) END * c1.amt_ExchangeRate_GBL), 0) StockValueAmt_GBL,
          (b.multiplier * a.MCHB_CSPEM) amt_BlockedStockAmt,
          ifnull(((b.multiplier * a.MCHB_CSPEM) * c1.amt_ExchangeRate_GBL), 0) amt_BlockedStockAmt_GBL,
          (b.multiplier * a.MCHB_CINSM) amt_StockInQInspAmt,
          ifnull(((b.multiplier * a.MCHB_CINSM) * c1.amt_ExchangeRate_GBL), 0) amt_StockInQInspAmt_GBL,
          (b.multiplier * a.MCHB_CUMLM) amt_StockInTransferAmt,
          ifnull(((b.multiplier * a.MCHB_CUMLM) * c1.amt_ExchangeRate_GBL), 0) amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt, 0 amt_UnrestrictedConsgnStockAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * c1.amt_ExchangeRate_GBL), 0) amt_StdUnitPrice_GBL,
	/* LK : 26 Apr change Starts */
	CASE WHEN MCHB_CLABS+MCHB_CSPEM+MCHB_CINSM = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CLABS) END 
			+ CASE WHEN MCHB_CSPEM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CSPEM) END
			+ CASE WHEN MCHB_CINSM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CINSM) END 
		   ) END amt_OnHand,
	ifnull((CASE WHEN MCHB_CLABS+MCHB_CSPEM+MCHB_CINSM = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CLABS) END 
			+ CASE WHEN MCHB_CSPEM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CSPEM) END
			+ CASE WHEN MCHB_CINSM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CINSM) END 
		   ) END * c1.amt_ExchangeRate_GBL) ,0)
	amt_OnHand_GBL,
	/* LK : 26 Apr change Ends */		  
          c1.ct_Quantity LastReceivedQty, a.MCHB_CHARG BatchNo, c1.dd_ValuationType ValuationType, 'Not Set' DocumentNo,
          0 DocumentItemNo, c1.MovementType MovementType, 
	/* LK : 26 Apr change Starts */		  
	sled.Dim_Dateid StorageLocEntryDate,
	/* LK : 26 Apr change Ends */	
	c1.dim_DateIDPostingDate LastReceivedDate,
          dp.Dim_PartID, p.dim_plantid, sl.Dim_StorageLocationid, c1.dim_Companyid, 1 dim_vendorid, c1.dim_currencyid, c1.dim_stocktypeid,
          c1.dim_specialstockid, c1.Dim_PurchaseOrgid, pg.Dim_PurchaseGroupid, dph.dim_producthierarchyid, uom.Dim_UnitOfMeasureid,
	c1.Dim_MovementIndicatorid, c1.dim_CostCenterid,
        stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over () , 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	c1.amt_ExchangeRate_GBL
     FROM MCHB a INNER JOIN dim_part dp ON dp.PartNumber = a.MCHB_MATNR AND dp.Plant = a.MCHB_WERKS
          INNER JOIN dim_plant p ON a.MCHB_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON     b.MATNR = a.MCHB_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN tmp_ivaging_ins5_notexists_ins_c1 c1
             ON c1.dim_Partid = dp.dim_partid AND c1.dd_BatchNumber = a.MCHB_CHARG
			 AND concat(c1.dd_MaterialDocYear,LPAD(c1.dd_MaterialDocNo,10,'0'),LPAD(c1.dd_MaterialDocItemNo,5,'0'))
                    = (select x.dd_MaterialDocYearItem
                       from fact_materialmovement_tmp_invagin x
                        where x.Dim_Partid = c1.Dim_Partid AND x.Dim_MovementTypeid = c1.Dim_MovementTypeid AND x.dd_BatchNumber = c1.dd_BatchNumber)
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy AND dph.RowIsCurrent = 1
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company co ON co.dim_Companyid = c1.dim_Companyid
          INNER JOIN dim_storagelocation sl ON     sl.LocationCode = a.MCHB_LGORT AND sl.Plant = a.MCHB_WERKS AND sl.RowIsCurrent = 1
          INNER JOIN dim_date sled ON sled.DateValue = a.MCHB_ERSDA AND sled.CompanyCode = co.CompanyCode,
	  tmp_stkcategory_001 stkc;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

/*Insert 6 */

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty, ct_TotalRestrictedStock, ct_StockInQInsp, ct_BlockedStock, ct_StockInTransfer,
                                ct_BlockedStockReturns, amt_StockValueAmt, amt_StockValueAmt_GBL, amt_BlockedStockAmt, amt_BlockedStockAmt_GBL,
                                amt_StockInQInspAmt, amt_StockInQInspAmt_GBL, amt_StockInTransferAmt, amt_StockInTransferAmt_GBL,
                                amt_UnrestrictedConsgnStockAmt, amt_UnrestrictedConsgnStockAmt_GBL, amt_StdUnitPrice, amt_StdUnitPrice_GBL,
				/* LK : 26 Apr change Starts */
				amt_OnHand,
				amt_OnHand_GBL,
				/* LK : 26 Apr change Ends */								
                                ct_LastReceivedQty, dd_BatchNo, dd_ValuationType, dd_DocumentNo, dd_DocumentItemNo, dd_MovementType,
				 dim_StorageLocEntryDateid, dim_LastReceivedDateid, dim_Partid, dim_Plantid, dim_StorageLocationid, dim_Companyid, dim_Vendorid, dim_Currencyid,
				dim_StockTypeid, dim_specialstockid, Dim_PurchaseOrgid, Dim_PurchaseGroupid, dim_producthierarchyid, Dim_UnitOfMeasureid, Dim_MovementIndicatorid,
				dim_CostCenterid, dim_stockcategoryid,
				fact_inventoryagingid, Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
SELECT a.MCHB_CLABS ct_StockQty, a.MCHB_CEINM ct_TotalRestrictedStock, a.MCHB_CINSM ct_StockInQInsp, a.MCHB_CSPEM ct_BlockedStock,
	a.MCHB_CUMLM ct_StockInTransfer, a.MCHB_CRETM ct_BlockedStockReturns,
	CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3
	 ELSE (b.multiplier * a.MCHB_CLABS) END StockValueAmt,
	ifnull(((CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3
	 ELSE (b.multiplier * a.MCHB_CLABS) END ) * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP) )), 0) StockValueAmt_GBL,
          (b.multiplier * a.MCHB_CSPEM) amt_BlockedStockAmt,
          ifnull((b.multiplier * a.MCHB_CSPEM) * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)), 0) amt_BlockedStockAmt_GBL,
          (b.multiplier * a.MCHB_CINSM) amt_StockInQInspAmt,
          ifnull(((b.multiplier * a.MCHB_CINSM) * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StockInQInspAmt_GBL,
          (b.multiplier * a.MCHB_CUMLM) amt_StockInTransferAmt,
          ifnull(((b.multiplier * a.MCHB_CUMLM) * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StockInTransferAmt_GBL,
          0 amt_UnrestrictedConsgnStockAmt,
          0 amt_UnrestrictedConsgnStockAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
	/* LK : 26 Apr change Starts */
	CASE WHEN MCHB_CLABS+MCHB_CSPEM+MCHB_CINSM = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CLABS) END 
			+ CASE WHEN MCHB_CSPEM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CSPEM) END
			+ CASE WHEN MCHB_CINSM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CINSM) END 
		   ) END amt_OnHand,
	 ifnull((CASE WHEN MCHB_CLABS+MCHB_CSPEM+MCHB_CINSM = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MCHB_CLABS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CLABS) END 
			+ CASE WHEN MCHB_CSPEM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CSPEM) END
			+ CASE WHEN MCHB_CINSM = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MCHB_CINSM) END 
		   ) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)
	amt_OnHand_GBL,
	/* LK : 26 Apr change Ends */	 
          0 LastReceivedQty, a.MCHB_CHARG BatchNo, 'Not Set' ValuationType, 'Not Set' dd_DocumentNo, 0 dd_DocumentItemNo,
          'Not Set' MovementType, sled.Dim_Dateid StorageLocEntryDate, 1 LastReceivedDate, Dim_Partid,
          p.dim_plantid, Dim_StorageLocationid, dim_companyid, 1 dim_vendorid, dim_currencyid,
          1 dim_stocktypeid, 1 dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid, dim_producthierarchyid, Dim_UnitOfMeasureid, 1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over () , 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1) amt_ExchangeRate_GBL
     FROM dim_plant p INNER JOIN MCHB a ON     a.MCHB_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON     b.MATNR = a.MCHB_MATNR AND b.BWKEY = p.ValuationArea 
          INNER JOIN dim_part dp ON dp.PartNumber = a.MCHB_MATNR AND dp.Plant = a.MCHB_WERKS
          INNER JOIN Dim_UnitOfMeasure uom ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_storagelocation sl ON     sl.LocationCode = a.MCHB_LGORT AND sl.Plant = MCHB_WERKS AND sl.RowIsCurrent = 1
          INNER JOIN dim_date sled ON sled.DateValue = a.MCHB_ERSDA AND sled.CompanyCode = p.CompanyCode	/* LK: 26 Apr Change */
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
          tmp_stkcategory_001  stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_materialmovement_tmp_invagin c2 INNER JOIN dim_movementtype dmt1
                    ON c2.Dim_MovementTypeid = dmt1.Dim_MovementTypeid
                  INNER JOIN dim_Plant dpl1 ON c2.dim_Plantid = dpl1.dim_Plantid
                  INNER JOIN dim_Part dp2 ON c2.dim_Partid = dp2.dim_Partid
           WHERE dp2.PartNumber = a.MCHB_MATNR
                AND dpl1.PlantCode = a.MCHB_WERKS
                AND c2.dd_BatchNumber = a.MCHB_CHARG);

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';
 
/***********************LK: 26 Apr change : Insert queries 7/8/9/10/11 Starts ***********************/

/*Insert 7 */

/* Temporary tables to be used in inserts 7 to 11 */

/* LastReceivedQty */

/* Store mm and datevalue in a tmp table */

DROP TABLE IF EXISTS TMP_fact_mm_0;
CREATE TABLE TMP_fact_mm_0 
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType in (541,543,544) and mm.dd_debitcreditid = 'Debit';

/* Only retrive the min dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_1;
CREATE TABLE TMP_fact_mm_1 
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,min(mm.DateValue) as min_DateValue
from TMP_fact_mm_0 mm 
GROUP by mm.Dim_Partid,mm.Dim_Vendorid;

/* Now get the ct_QtyEntryUOM corresponding to the min datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_2;
CREATE TABLE TMP_fact_mm_2
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,mm.ct_Quantity ct_QtyEntryUOM 
FROM TMP_fact_mm_0 mm, TMP_fact_mm_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.min_DateValue;


/* StorageLocEntryDate/LastReceivedDate */

/* Simply use the posting date id for the min/max date */

/* Store mm and datevalue in a tmp table */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_0;
CREATE TABLE TMP_fact_mm_postingdate_0 
AS
SELECT mm.*,mmdt.DateValue
FROM fact_materialmovement mm 
	 inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	 inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType in (541,543,544) and mm.dd_debitcreditid = 'Debit';

/* Only retrive the max dates for each partid/vendorid combination */
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_1;
CREATE TABLE TMP_fact_mm_postingdate_1
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,max(mm.DateValue) as max_DateValue, min(mm.DateValue) as min_DateValue
from TMP_fact_mm_postingdate_0 mm 
GROUP by mm.Dim_Partid,mm.Dim_Vendorid;

/* Now get the dim_DateIDPostingDate corresponding to the max datevalue*/
DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2;
CREATE TABLE TMP_fact_mm_postingdate_2
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate 
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.max_DateValue;

DROP TABLE IF EXISTS TMP_fact_mm_postingdate_2_min;
CREATE TABLE TMP_fact_mm_postingdate_2_min
AS
SELECT mm.Dim_Partid,mm.Dim_Vendorid,mm.dim_DateIDPostingDate 
FROM TMP_fact_mm_postingdate_0 mm, TMP_fact_mm_postingdate_1 t
WHERE mm.Dim_Partid = t.Dim_Partid
AND mm.Dim_Vendorid = t.Dim_Vendorid
AND mm.DateValue = t.min_DateValue;

\i /db/schema_migration/bin/wrapper_optimizedb.sh TMP_fact_mm_postingdate_0;
\i /db/schema_migration/bin/wrapper_optimizedb.sh TMP_fact_mm_postingdate_1;
\i /db/schema_migration/bin/wrapper_optimizedb.sh TMP_fact_mm_postingdate_2;
\i /db/schema_migration/bin/wrapper_optimizedb.sh TMP_fact_mm_postingdate_2_min;

drop table if exists fitp_tmp_p01;
create table fitp_tmp_p01 as 
select  *,
	Integer(0) Dim_Partid_upd,
	Integer(0) Dim_Vendorid_upd,
	varchar(null,20) PurchOrg_upd  
from fact_inventoryaging_tmp_populate where 1=2;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MSLB';

INSERT INTO fitp_tmp_p01(ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL,
				Dim_Partid_upd,
				Dim_Vendorid_upd,
				PurchOrg_upd)
								
 SELECT  a.MSLB_LBLAB ct_StockQty,
          a.MSLB_LBEIN ct_TotalRestrictedStock,
          a.MSLB_LBINS ct_StockInQInsp,
          CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END  StockValueAmt,
          ifnull(((CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END) 
		* ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)  StockValueAmt_GBL,
          (b.multiplier * a.MSLB_LBINS)	 amt_StockInQInspAmt,
          ifnull(((b.multiplier * a.MSLB_LBINS)	 
		* ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)  amt_StockInQInspAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
	CASE WHEN MSLB_LBLAB+MSLB_LBINS = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END 
			+ CASE WHEN MSLB_LBINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBINS) END
		  ) END amt_OnHand,
	ifnull((CASE WHEN MSLB_LBLAB+MSLB_LBINS = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSLB_LBLAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBLAB) END 
			+ CASE WHEN MSLB_LBINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSLB_LBINS) END
		  ) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)
	amt_OnHand_GBL,
	  1 LastReceivedQty,
          a.MSLB_CHARG BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
	  1 StorageLocEntryDate,
	  1 LastReceivedDate,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          v.dim_vendorid,
          dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
	 1 Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
	  stkc.dim_StockCategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), /* table name is correct here do not change it to fitp_tmp_p01 */ 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1),
	dp.Dim_Partid Dim_Partid_upd,
	v.Dim_Vendorid Dim_Vendorid_upd,
	p.PurchOrg PurchOrg_upd
     FROM MSLB a
          INNER JOIN dim_plant p
             ON a.MSLB_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MSLB_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSLB_MATNR AND dp.Plant = a.MSLB_WERKS
          INNER JOIN dim_vendor v
             ON v.VendorNumber = MSLB_LIFNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency, tmp_GlobalCurr_001 gbl,
          tmp_stkcategory_001  stkc;		

Update fitp_tmp_p01 fitp
Set dim_StorageLocEntryDateid = ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2_min mm /* This has min date */
                                  WHERE mm.Dim_Partid = fitp.Dim_Partid_upd and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd ),1);

Update fitp_tmp_p01 fitp
Set dim_LastReceivedDateid = ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2 mm     /* This has max date */
                                  WHERE mm.Dim_Partid = fitp.Dim_Partid_upd and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd ),1);

Update fitp_tmp_p01 fitp
Set ct_LastReceivedQty = ifnull((select mm.ct_QtyEntryUOM
                  from TMP_fact_mm_2 mm
                                  where mm.Dim_Partid = fitp.Dim_Partid_upd and mm.Dim_Vendorid = fitp.Dim_Vendorid_upd
                  ),1);

Update fitp_tmp_p01 fitp
Set Dim_PurchaseOrgid = ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = fitp.PurchOrg_upd),1);

Insert into fact_inventoryaging_tmp_populate(ct_StockQty,
		ct_TotalRestrictedStock,
		ct_StockInQInsp,
		amt_StockValueAmt,
		amt_StockValueAmt_GBL,
		amt_StockInQInspAmt,
		amt_StockInQInspAmt_GBL,
		amt_StdUnitPrice,
		amt_StdUnitPrice_GBL,
		amt_OnHand,
		amt_OnHand_GBL,
		ct_LastReceivedQty,
		dd_BatchNo,
		dd_DocumentNo,
		dd_DocumentItemNo,
		dim_StorageLocEntryDateid,
		dim_LastReceivedDateid,
		dim_Partid,
		dim_Plantid,
		dim_StorageLocationid,
		dim_Companyid,
		dim_Vendorid,
		dim_Currencyid,
		dim_StockTypeid,
		dim_specialstockid,
		Dim_PurchaseOrgid,
		Dim_PurchaseGroupid,
		dim_producthierarchyid,
		Dim_UnitOfMeasureid,
		Dim_MovementIndicatorid,
		dim_CostCenterid,
		dim_stockcategoryid,
		fact_inventoryagingid,
		Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
		Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
		amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
		amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
		amt_ExchangeRate_GBL)
Select ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	amt_OnHand,
	amt_OnHand_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid,
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL
from fitp_tmp_p01;

drop table if exists fitp_tmp_p01;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;
 
/*Insert 8 */
/* -- Vendor Stocks with no valuation	*/
INSERT INTO fact_inventoryaging_tmp_populate 
	(ct_StockQty,
	ct_TotalRestrictedStock,
	ct_StockInQInsp,
	amt_StockValueAmt,
	amt_StockValueAmt_GBL,
	amt_StockInQInspAmt,
	amt_StockInQInspAmt_GBL,
	amt_StdUnitPrice,
	amt_StdUnitPrice_GBL,
	ct_LastReceivedQty,
	dd_BatchNo,
	dd_DocumentNo,
	dd_DocumentItemNo,
	dim_StorageLocEntryDateid,
	dim_LastReceivedDateid,
	dim_Partid,
	dim_Plantid,
	dim_StorageLocationid,
	dim_Companyid,
	dim_Vendorid,
	dim_Currencyid,
	dim_StockTypeid,
	dim_specialstockid,
	Dim_PurchaseOrgid,
	Dim_PurchaseGroupid,
	dim_producthierarchyid,
	Dim_UnitOfMeasureid,
	Dim_MovementIndicatorid,
	dim_CostCenterid,
	dim_stockcategoryid,
	fact_inventoryagingid, 
	Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
	Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
	amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
	amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
	amt_ExchangeRate_GBL)
  SELECT  a.MSLB_LBLAB ct_StockQty,
          a.MSLB_LBEIN ct_TotalRestrictedStock,
          a.MSLB_LBINS ct_StockInQInsp,
          0 StockValueAmt,
          0 StockValueAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
         ifnull((select mm.ct_QtyEntryUOM
                  from TMP_fact_mm_2 mm 
		  where mm.Dim_Partid = dp.Dim_Partid and mm.Dim_Vendorid = v.Dim_Vendorid
                  ),1) LastReceivedQty,
          a.MSLB_CHARG BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
           ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2_min mm	/* This has min date */
		  WHERE mm.Dim_Partid = dp.Dim_Partid and mm.Dim_Vendorid = v.Dim_Vendorid ),1) StorageLocEntryDate,
          ifnull(( select mm.dim_DateIDPostingDate
                  from TMP_fact_mm_postingdate_2 mm	/* This has max date */
		  WHERE mm.Dim_Partid = dp.Dim_Partid and mm.Dim_Vendorid = v.Dim_Vendorid ),1) LastReceivedDate,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          v.dim_vendorid,
          dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_StockCategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)
     FROM MSLB a
          INNER JOIN dim_plant p
             ON a.MSLB_WERKS = p.PlantCode
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSLB_MATNR AND dp.Plant = a.MSLB_WERKS
          INNER JOIN dim_vendor v
             ON v.VendorNumber = MSLB_LIFNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency, tmp_GlobalCurr_001 gbl,
          tmp_stkcategory_001  stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_inventoryaging_tmp_populate c2
           WHERE c2.Dim_Partid = dp.Dim_Partid
                AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_Vendorid = v.Dim_Vendorid);				
				
 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';
			
\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;
	
/*Insert 9 */

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MSKU';

INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                amt_OnHand,
                                amt_OnHand_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Customerid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
  SELECT  a.MSKU_KULAB ct_StockQty,
          a.MSKU_KUEIN ct_TotalRestrictedStock,
          a.MSKU_KUINS ct_StockInQInsp,
          CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3
               ELSE (b.multiplier * a.MSKU_KULAB)
          END StockValueAmt,
          ifnull(((CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3
               ELSE (b.multiplier * a.MSKU_KULAB)
          END) * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)
		  StockValueAmt_GBL,
          (b.multiplier * a.MSKU_KUINS) amt_StockInQInspAmt,
          ifnull(((b.multiplier * a.MSKU_KUINS) * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StockInQInspAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
	CASE WHEN MSKU_KULAB+MSKU_KUINS = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KULAB) END 
			+ CASE WHEN MSKU_KUINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUINS) END
		  ) END amt_OnHand,
	ifnull((CASE WHEN MSKU_KULAB+MSKU_KUINS = MBEW_LBKUM THEN SALK3
	 ELSE (   CASE WHEN MSKU_KULAB = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KULAB) END 
			+ CASE WHEN MSKU_KUINS = MBEW_LBKUM THEN SALK3 ELSE (b.multiplier * a.MSKU_KUINS) END
		  ) END * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0)
	amt_OnHand_GBL,
          0.00 LastReceivedQty,
          a.MSKU_CHARG BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          1 StorageLocEntryDate,
          1 LastReceivedDate,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          cm.dim_customerid,
          dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)
     FROM MSKU a
          INNER JOIN dim_plant p
             ON a.MSKU_WERKS = p.PlantCode
         INNER JOIN tmp2_MBEW_NO_BWTAR b ON     b.MATNR = a.MSKU_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKU_MATNR AND dp.Plant = a.MSKU_WERKS
          INNER JOIN dim_customer cm
             ON cm.CustomerNumber = MSKU_KUNNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc;
				
 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

/*Insert 10 */
/* -- Customer stocks with no valuation	*/
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockQty,
                                ct_TotalRestrictedStock,
                                ct_StockInQInsp,
                                amt_StockValueAmt,
                                amt_StockValueAmt_GBL,
                                amt_StockInQInspAmt,
                                amt_StockInQInspAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Customerid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,ct_StockInTransit,amt_StockInTransitAmt,amt_StockInTransitAmt_GBL,Dim_SupplyingPlantId,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
  SELECT  a.MSKU_KULAB ct_StockQty,
          a.MSKU_KUEIN ct_TotalRestrictedStock,
          a.MSKU_KUINS ct_StockInQInsp,
          0 StockValueAmt,
          0 StockValueAmt_GBL,
          0 amt_StockInQInspAmt,
          0 amt_StockInQInspAmt_GBL,
          0 amt_StdUnitPrice,
          0 amt_StdUnitPrice_GBL,
          0.00 LastReceivedQty,
          a.MSKU_CHARG BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          1 StorageLocEntryDate,
          1 LastReceivedDate,
          Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          dim_companyid,
          cm.dim_customerid,
          dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          ifnull((select po.Dim_PurchaseOrgid from dim_purchaseorg po
                 where po.PurchaseOrgCode = p.PurchOrg),1) Dim_PurchaseOrgid,
          Dim_PurchaseGroupid,
          dim_producthierarchyid,
          Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	1,1,1,1,1,1, 1,1,0,0,0,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)
     FROM MSKU a
          INNER JOIN dim_plant p
             ON a.MSKU_WERKS = p.PlantCode
          INNER JOIN dim_part dp
             ON dp.PartNumber = a.MSKU_MATNR AND dp.Plant = a.MSKU_WERKS
          INNER JOIN dim_customer cm
             ON cm.CustomerNumber = MSKU_KUNNR
          INNER JOIN Dim_UnitOfMeasure uom
             ON uom.UOM = dp.UnitOfMeasure AND uom.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph
             ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_purchasegroup pg
             ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_Currency dc
             ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
    WHERE NOT EXISTS
          (SELECT 1
           FROM fact_inventoryaging_tmp_populate c2
           WHERE c2.Dim_Partid = dp.Dim_Partid
                AND c2.dim_plantid = p.dim_plantid
                AND c2.Dim_CustomerId = cm.Dim_CustomerId);

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
						where table_name = 'fact_inventoryaging_tmp_populate';

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.Dim_Partid, mm.Dim_StorageLocationid, mmdt.Dim_Dateid,
	row_number() over(partition by mm.Dim_Partid, mm.Dim_StorageLocationid order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate;

UPDATE fact_inventoryaging_tmp_populate f
SET f.dim_dateidtransaction = IFNULL((select mmdt.Dim_Dateid
                                          from tmp_custom_postproc_dim_date mmdt
					  where mmdt.Dim_Partid = f.dim_Partid and mmdt.Dim_StorageLocationid = f.dim_StorageLocationid
						and mmdt.RowSeqNo = 1),1)
WHERE f.dim_stockcategoryid in (2,3);

/*Insert 11 */

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm 
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = 641;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'MARC';

/* # INTRA stock analytics */

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (fp.ct_QtyIssued - fp.ct_ReceivedQty) ct_StockInTransit,
          fp.dd_DocumentNo,
          fp.dd_DocumentItemNo,
          fp.Dim_Partid,
          fp.dim_currencyid,
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          fp.Dim_PlantidSupplying, 
	  	  fp.amt_ExchangeRate_GBL
     FROM fact_purchase fp
          INNER JOIN dim_purchasemisc pm ON fp.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
          INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = fp.Dim_Currencyid
     WHERE fp.ct_ReceivedQty < fp.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and fp.Dim_PlantidSupplying <> 1 and fp.Dim_Vendorid = 1;

  INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
   SELECT fp.ct_StockInTransit,
          (b.multiplier * fp.ct_StockInTransit) amt_StockInTransitAmt,
          (b.multiplier * fp.ct_StockInTransit) * fp.amt_ExchangeRate_GBL amt_StockInTransitAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          b.multiplier * fp.amt_ExchangeRate_GBL amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          fp.dd_DocumentNo dd_DocumentNo,
          fp.dd_DocumentItemNo dd_DocumentItemNo,
          ifnull((select mmdt.Dim_Dateid
                  from tmp_custom_postproc_dim_date mmdt
                  where mmdt.dd_DocumentNo = fp.dd_DocumentNo and mmdt.dd_DocumentItemNo = fp.dd_DocumentItemNo
			and mmdt.RowSeqNo = 1),1)  StorageLocEntryDate,
          1 LastReceivedDate,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          fp.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          fp.Dim_PurchaseOrgid,
          fp.Dim_PurchaseGroupid,
          fp.dim_producthierarchyid,
          fp.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          fp.Dim_PlantidSupplying, 
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	  1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,
	  fp.amt_ExchangeRate_GBL
     FROM dim_plant p
          INNER JOIN MARC a ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN tmp_fact_fp_001 fp ON fp.Dim_Partid = dp.Dim_Partid
          INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = fp.Dim_Currencyid, 
	  tmp_stkcategory_001 stkc;

/********* Replaced with above ********
INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                ct_StockInTransfer,
                                amt_StockInTransferAmt,
                                amt_StockInTransferAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid, 
				Dim_ConsumptionTypeid,Dim_DocumentStatusid,Dim_DocumentTypeid,Dim_IncoTermid,Dim_ItemCategoryid,Dim_ItemStatusid,
				Dim_Termid,Dim_PurchaseMiscid,amt_MtlDlvrCost,
				amt_OverheadCost,amt_OtherCost,amt_WIPBalance,ct_WIPAging,amt_MovingAvgPrice,amt_PreviousPrice,amt_CommericalPrice1,
				amt_PlannedPrice1,amt_PrevPlannedPrice,amt_CurPlannedPrice,Dim_DateIdPlannedPrice2,Dim_DateIdLastChangedPrice,
				amt_ExchangeRate_GBL)
   SELECT ifnull(MARC_TRAME,0) ct_StockInTransit,
          ifnull((b.multiplier * MARC_TRAME),0) amt_StockInTransitAmt,
          ifnull(b.multiplier * MARC_TRAME
	   * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0)
            amt_StockInTransitAmt_GBL,
          ifnull(MARC_UMLMC, 0) ct_StockInTransfer,
          ifnull((b.multiplier * MARC_UMLMC),0) amt_StockInTransferAmt,
          ifnull(b.multiplier * MARC_UMLMC
           * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),0) 
	    amt_StockInTransferAmt_GBL,
          b.multiplier amt_StdUnitPrice,
          ifnull((b.multiplier * ( select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP))), 0) amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          'Not Set' dd_DocumentNo,
          0 dd_DocumentItemNo,
          1  StorageLocEntryDate,
          1 LastReceivedDate,
          dp.Dim_Partid,
          p.dim_plantid,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          dc.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          1,
          ifnull(pg.Dim_PurchaseGroupid,1),
          ifnull(dph.dim_producthierarchyid,1),
          1,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          1,
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate')+ row_number() over (), 
	  1,1,1,1,1,1, 1,1,0, 0,0,0,0,0,0,0, 0,0,0,1,1,
	  ifnull((select ifnull(z.exchangeRate,1) from tmp_getExchangeRate1 z 
		where z.pFromCurrency  = dc.CurrencyCode and z.fact_script_name = 'bi_populate_inventoryaging_fact' and z.pToCurrency = pGlobalCurrency and z.pDate = ANSIDATE(LOCAL_TIMESTAMP)),1)
     FROM dim_plant p
          INNER JOIN MARC a ON a.MARC_WERKS = p.PlantCode
          INNER JOIN tmp2_MBEW_NO_BWTAR b ON b.MATNR = a.MARC_MATNR AND b.BWKEY = p.ValuationArea
          INNER JOIN dim_part dp ON dp.PartNumber = a.MARC_MATNR AND dp.Plant = a.MARC_WERKS
          INNER JOIN dim_purchasegroup pg ON pg.PurchaseGroup = dp.PurchaseGroupCode
          INNER JOIN dim_company c ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1
          INNER JOIN dim_producthierarchy dph ON dph.ProductHierarchy = dp.ProductHierarchy
          INNER JOIN dim_Currency dc ON dc.CurrencyCode = c.Currency, 
	  tmp_GlobalCurr_001 gbl,
	  tmp_stkcategory_001 stkc
********************************************/

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';

 
  /*# INTER stock analytics */

DROP TABLE IF EXISTS tmp_custom_postproc_dim_date;
CREATE TABLE tmp_custom_postproc_dim_date AS
select mm.dd_DocumentNo, mm.dd_DocumentItemNo, mmdt.Dim_Dateid,
	row_number() over(partition by mm.dd_DocumentNo, mm.dd_DocumentItemNo order by mmdt.DateValue desc) RowSeqNo
from fact_materialmovement mm 
	inner join dim_movementtype mt on mm.Dim_MovementTypeid = mt.Dim_MovementTypeid
	inner join dim_date mmdt on mmdt.Dim_Dateid = mm.dim_DateIDPostingDate
where mt.MovementType = 643;

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'STO';

DROP TABLE IF EXISTS tmp_fact_fp_001;
CREATE TABLE tmp_fact_fp_001 AS
SELECT (a.ct_QtyIssued - a.ct_ReceivedQty) ct_StockInTransit,
          a.amt_StdUnitPrice,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
          a.Dim_Partid,
          a.dim_plantidordering,
          a.dim_currencyid,
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          a.Dim_PlantIdSupplying, 
		  a.amt_ExchangeRate_GBL
     FROM fact_purchase a
          INNER JOIN dim_purchasemisc pm ON a.Dim_PurchaseMiscid = pm.Dim_PurchaseMiscid
     WHERE a.ct_ReceivedQty < a.ct_QtyIssued and pm.ItemDeliveryComplete = 'Not Set'
           and a.Dim_PlantidSupplying <> 1
           and not exists (select 1 from fact_inventoryaging_tmp_populate fi inner join dim_stockcategory sc on fi.dim_stockcategoryid = sc.Dim_StockCategoryid
                          where fi.dim_Partid = a.dim_partid and fi.dd_DocumentNo = a.dd_DocumentNo and sc.CategoryCode = 'MARC');

  INSERT INTO fact_inventoryaging_tmp_populate (ct_StockInTransit,
                                amt_StockInTransitAmt,
                                amt_StockInTransitAmt_GBL,
                                amt_StdUnitPrice,
                                amt_StdUnitPrice_GBL,
                                ct_LastReceivedQty,
                                dd_BatchNo,
                                dd_DocumentNo,
                                dd_DocumentItemNo,
                                dim_StorageLocEntryDateid,
                                dim_LastReceivedDateid,
                                dim_Partid,
                                dim_Plantid,
                                dim_StorageLocationid,
                                dim_Companyid,
                                dim_Vendorid,
                                dim_Currencyid,
                                dim_StockTypeid,
                                dim_specialstockid,
                                Dim_PurchaseOrgid,
                                Dim_PurchaseGroupid,
                                dim_producthierarchyid,
                                Dim_UnitOfMeasureid,
                                Dim_MovementIndicatorid,
                                dim_CostCenterid,
                                dim_stockcategoryid,
                                Dim_SupplyingPlantId,
				fact_inventoryagingid,
				amt_ExchangeRate_GBL)
   SELECT a.ct_StockInTransit,
          (a.amt_StdUnitPrice * a.ct_StockInTransit) amt_StockInTransitAmt,
          (a.amt_StdUnitPrice * a.amt_ExchangeRate_GBL) amt_StockInTransitAmt_GBL,
          a.amt_StdUnitPrice amt_StdUnitPrice,
          (a.amt_StdUnitPrice * a.amt_ExchangeRate_GBL)  amt_StdUnitPrice_GBL,
          0 LastReceivedQty,
          'Not Set' BatchNo,
          a.dd_DocumentNo,
          a.dd_DocumentItemNo,
          ifnull((select mmdt.Dim_Dateid
                  from tmp_custom_postproc_dim_date mmdt
                  where mmdt.dd_DocumentNo = a.dd_DocumentNo and mmdt.dd_DocumentItemNo = a.dd_DocumentItemNo
			and mmdt.RowSeqNo = 1),1) StorageLocEntryDate,
          1 LastReceivedDate,
          a.Dim_Partid,
          a.dim_plantidordering,
          1 Dim_StorageLocationid,
          c.dim_companyid,
          1 dim_vendorid,
          a.dim_currencyid,
          1 dim_stocktypeid,
          1 dim_specialstockid,
          a.Dim_PurchaseOrgid,
          a.Dim_PurchaseGroupid,
          a.dim_producthierarchyid,
          a.Dim_UnitOfMeasureid,
          1 Dim_MovementIndicatorid,
          1 dim_CostCenterid,
          stkc.dim_stockcategoryid dim_StockCategoryid,
          a.Dim_PlantIdSupplying, 
	  (SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	  a.amt_ExchangeRate_GBL
     FROM tmp_fact_fp_001 a
          INNER JOIN dim_plant p
             ON a.dim_plantidordering = p.dim_plantid
          INNER JOIN dim_part dp
             ON dp.dim_partid = a.dim_partid
          INNER JOIN dim_Currency dc
             ON dc.Dim_Currencyid = a.Dim_Currencyid
          INNER JOIN dim_company c
             ON c.CompanyCode = p.CompanyCode AND c.RowIsCurrent = 1,
	  tmp_stkcategory_001 stkc;

 update NUMBER_FOUNTAIN set max_id = ifnull(( select max(fact_inventoryagingid) from fact_inventoryaging_tmp_populate), 0)
 where table_name = 'fact_inventoryaging_tmp_populate';


/*********************** LK: 26 Apr change : Insert queries 7/8/9/10/11 Ends ***********************/

/* Update 1 */

drop table if exists tmp_MBEW_NO_BWTAR;
create table tmp_MBEW_NO_BWTAR as
SELECT MATNR,BWKEY, max(((m.LFGJA * 100) + m.LFMON)) as col1
from MBEW_NO_BWTAR m
where ((m.VPRSV = 'S' AND m.STPRS > 0) OR (m.VPRSV = 'V' AND m.VERPR > 0))
    and ((m.LFGJA * 100) + m.LFMON) = (select max((x.LFGJA * 100) + x.LFMON) from MBEW_NO_BWTAR x 
                                       where x.MATNR = m.MATNR and x.BWKEY = m.BWKEY
                                             AND ((x.VPRSV = 'S' AND x.STPRS > 0) OR (x.VPRSV = 'V' AND x.VERPR > 0)))
group by MATNR,BWKEY;

drop table if exists tmp2_MBEW_NO_BWTAR;
CREATE TABLE tmp2_MBEW_NO_BWTAR
AS
SELECT DISTINCT m.*,
       ((m.LFGJA * 100) + m.LFMON) as LFGJA_LFMON, 
       m2.col1 as MAX_LFGJA_LFMON
FROM tmp_MBEW_NO_BWTAR m2,MBEW_NO_BWTAR m
WHERE m.MATNR = m2.MATNR
AND m.BWKEY = m2.BWKEY
AND ((m.LFGJA * 100) + m.LFMON) = m2.col1;

ALTER TABLE tmp2_MBEW_NO_BWTAR
ADD column multiplier decimal(18,5);

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.STPRS / b.PEINH), 18,5)
WHERE b.VPRSV = 'S';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier =  decimal((b.VERPR / b.PEINH), 18,5)
WHERE b.VPRSV = 'V';

UPDATE tmp2_MBEW_NO_BWTAR b
SET multiplier = 0
WHERE multiplier IS NULL;

drop table if exists pl_prt_MBEW_NO_BWTAR;
create table pl_prt_MBEW_NO_BWTAR
as
select distinct ia.dim_partid,ia.dim_plantid,prt.PartNumber,pl.ValuationArea,pl.CompanyCode
from fact_inventoryaging_tmp_populate ia,dim_plant pl,dim_part prt
 WHERE ia.dim_partid = prt.Dim_partid
 and ia.dim_plantid = pl.dim_plantid;

UPDATE fact_inventoryaging_tmp_populate ia
FROM  tmp2_MBEW_NO_BWTAR m,
        pl_prt_MBEW_NO_BWTAR pl
  set amt_CommericalPrice1 = ifnull(MBEW_BWPRH,0),
      amt_CurPlannedPrice = ifnull(MBEW_LPLPR,0),
      amt_MovingAvgPrice = ifnull(MBEW_VMVER,0),
      amt_PlannedPrice1 = ifnull(MBEW_ZPLP1,0),
      amt_PreviousPrice = ifnull(MBEW_STPRV,0),
      amt_PrevPlannedPrice = ifnull(MBEW_VPLPR,0),
      Dim_DateIdLastChangedPrice = ifnull((SELECT dt.Dim_Dateid from dim_Date dt
                                            WHERE dt.DateValue = MBEW_LAEPR AND dt.CompanyCode = pl.CompanyCode ),1),
      Dim_DateIdPlannedPrice2 = ifnull((SELECT dt.Dim_Dateid from dim_Date dt
                                            WHERE dt.DateValue = MBEW_ZPLD2 AND dt.CompanyCode = pl.CompanyCode ),1),
      amt_MtlDlvrCost = ifnull((CASE m.VPRSV WHEN 'S' THEN m.STPRS / m.PEINH
                                WHEN 'V' THEN m.VERPR / m.PEINH END), 0)
  WHERE ia.dim_partid = pl.Dim_partid
    and ia.dim_plantid = pl.dim_plantid
    and pl.PartNumber = m.MATNR
    AND pl.ValuationArea = m.BWKEY;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* 2nd and 3rd updates	*/

UPDATE fact_inventoryaging_tmp_populate ia
FROM fact_purchase fp
   SET ia.Dim_ConsumptionTypeid = fp.Dim_ConsumptionTypeid,
       ia.Dim_DocumentStatusid = fp.Dim_DocumentStatusid,
       ia.Dim_DocumentTypeid = fp.Dim_DocumentTypeid,
       ia.Dim_IncoTermid = fp.Dim_IncoTerm1id,
       ia.Dim_ItemCategoryid = fp.Dim_ItemCategoryid,
       ia.Dim_ItemStatusid = fp.Dim_ItemStatusid,
       ia.Dim_Termid = fp.Dim_Termid,
       ia.Dim_PurchaseMiscid = fp.Dim_PurchaseMiscid,
       ia.Dim_SupplyingPlantId = fp.Dim_PlantidSupplying
 WHERE ia.dd_DocumentNo = fp.dd_DocumentNo
       AND ia.dd_DocumentItemNo = fp.dd_DocumentItemNo;

/* COMMENT - THIS UPDATE NEEDS TO BE CHECKED AGAIN.  */

UPDATE fact_purchase fp
FROM fact_inventoryaging_tmp_populate ia
   SET fp.ct_UnrestrictedOnHandQty = ia.ct_StockQty
 WHERE ia.dim_Partid = fp.Dim_Partid
       AND ia.dim_Plantid = fp.Dim_PlantidSupplying
	AND fp.ct_UnrestrictedOnHandQty <> ia.ct_StockQty;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* Update 4	*/
/* This update is using up a lot of memory on VW and does not complete */

drop table if exists tmp_keph;
CREATE TABLE tmp_keph 
AS 
SELECT KEPH_KALNR,KEPH_BWVAR,KEPH_KADKY,KEPH_KST001,k.KEPH_KST002,k.KEPH_KST003 + k.KEPH_KST004 + k.KEPH_KST005 + k.KEPH_KST006 + k.KEPH_KST007 + k.KEPH_KST008
                        + k.KEPH_KST009 + k.KEPH_KST010 + k.KEPH_KST011 + k.KEPH_KST012 + k.KEPH_KST013 + k.KEPH_KST014
                        + k.KEPH_KST015 + k.KEPH_KST016 + k.KEPH_KST017 + k.KEPH_KST018 + k.KEPH_KST019 + k.KEPH_KST020
                        + k.KEPH_KST021 + k.KEPH_KST022 + k.KEPH_KST023 + k.KEPH_KST024 + k.KEPH_KST025 + k.KEPH_KST026
                        + k.KEPH_KST027 as keph_othercosts, k.KEPH_KST001 + k.KEPH_KST002 as keph_allcosts
FROM keph k;

update tmp_keph
set keph_allcosts = keph_allcosts + keph_othercosts;

drop table if exists tmp_dim_date;
create table tmp_dim_date as
select DateValue,FinancialYear,FinancialMonthNumber,(FinancialYear * 100) + FinancialMonthNumber as yymm,CompanyCode from dim_date;

/*Use intermediate table ia1 as this was failing in fossil2 */

drop table if exists ia1;
create table ia1 as
select distinct ia.dim_Partid,ia.dim_plantid,pl.CompanyCode,pl.ValuationArea,p.PartNumber
from fact_inventoryaging_tmp_populate ia,dim_part p, dim_plant pl
where  ia.dim_Partid = p.Dim_Partid and ia.dim_plantid = pl.dim_plantid;

drop table if exists tmp3_MBEW_NO_BWTAR;
create table tmp3_MBEW_NO_BWTAR as
select distinct ia.dim_Partid,ia.dim_plantid,ia.CompanyCode, m.MATNR,m.BWKEY,m.MBEW_KALN1,m.MBEW_BWVA2,((m.MBEW_PDATL * 100) + m.MBEW_PPRDL) as m_yymm,m.multiplier
from ia1 ia, tmp2_MBEW_NO_BWTAR m
where ia.PartNumber = m.MATNR and ia.ValuationArea = m.BWKEY;

/*Create an intermediate table, which would decrease the number of rows read from keph */

drop table if exists tmp4_MBEW_NO_BWTAR;
create table tmp4_MBEW_NO_BWTAR as
select distinct m.*
from tmp3_MBEW_NO_BWTAR m,tmp_dim_date dt
where dt.yymm = m.m_yymm;

drop table if exists tmp4_dim_date;
create table tmp4_dim_date as
select distinct dt.*
from tmp4_MBEW_NO_BWTAR m,tmp_dim_date dt
where dt.yymm = m.m_yymm;

drop table if exists tmp2_keph;
create table tmp2_keph as
select DISTINCT m.dim_Partid,m.dim_plantid,m.CompanyCode,m.m_yymm,k.KEPH_KST001,k.KEPH_KST002,k.keph_othercosts,k.keph_allcosts,k.KEPH_KADKY,k.KEPH_KALNR,k.KEPH_BWVAR
FROM   tmp4_MBEW_NO_BWTAR m, tmp_keph k
where k.KEPH_KALNR = m.MBEW_KALN1 and k.KEPH_BWVAR = m.MBEW_BWVA2 and k.keph_allcosts = m.multiplier;

call vectorwise(combine 'tmp2_keph');

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging_tmp_populate;

UPDATE fact_inventoryaging_tmp_populate ia
	FROM tmp2_keph k,tmp4_dim_date dt
   SET ia.amt_MtlDlvrCost = k.KEPH_KST001,
       ia.amt_OverheadCost = k.KEPH_KST002,
       ia.amt_OtherCost = k.keph_othercosts
 WHERE ia.dim_Partid = k.Dim_Partid and ia.dim_plantid = k.dim_plantid
    and k.KEPH_KADKY = dt.DateValue and k.CompanyCode = dt.CompanyCode
    and dt.yymm = k.m_yymm;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* Issam change 24th Jun */
/* Update Purchase Order and Sales Order Open Qtys */

UPDATE fact_inventoryaging_tmp_populate ia
SET ct_POOpenQty = ifnull((SELECT SUM(case when atrb.itemdeliverycomplete = 'X' then 0.0000 when atrb.ItemGRIndicator = 'Not Set' then 0.0000 when (fpr.ct_DeliveryQty - fpr.ct_ReceivedQty) < 0 then 0.0000 else (fpr.ct_DeliveryQty - fpr.ct_ReceivedQty) end) 
					FROM fact_purchase fpr,
					Dim_PurchaseMisc atrb
					WHERE ia.dim_partid = fpr.Dim_partid
					AND fpr.Dim_PurchaseMiscid = atrb.Dim_PurchaseMiscid
					GROUP BY fpr.Dim_partid), 0),
	ct_SOOpenQty = ifnull((SELECT SUM(fso.ct_AfsOpenQty)
					FROM fact_salesorder fso
					WHERE ia.dim_partid = fso.Dim_partid), 0);

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* Update 5	*/

UPDATE dim_part
SET TotalPlantStock = ifnull((select sum(f_inv.ct_StockQty+f_inv.ct_StockInQInsp+f_inv.ct_BlockedStock+f_inv.ct_StockInTransfer)
                              from fact_inventoryaging_tmp_populate f_inv
                              where f_inv.dim_Partid = dim_part.Dim_Partid),0);
							  
UPDATE dim_part
SET TotalPlantStockAmt = ifnull((SELECT sum(f_inv.amt_StockValueAmt+f_inv.amt_StockInQInspAmt+f_inv.amt_BlockedStockAmt+f_inv.amt_StockInTransferAmt)
                              from fact_inventoryaging_tmp_populate f_inv
                              where f_inv.dim_Partid = dim_part.Dim_Partid),0);							  

call vectorwise(combine 'dim_part');

/* # WIP stock analytics */

DROP TABLE IF EXISTS tmp_stkcategory_001;
CREATE TABLE tmp_stkcategory_001 AS
	SELECT stkc.dim_stockcategoryid 
	FROM dim_stockcategory stkc 
	WHERE stkc.categorycode = 'WIP';

  INSERT INTO fact_inventoryaging_tmp_populate(ct_StockQty,
                               amt_StockValueAmt,
                               ct_LastReceivedQty,
                               dim_StorageLocEntryDateid,
                               dim_LastReceivedDateid,
                               dim_Partid,
                               dim_Plantid,
                               dim_StorageLocationid,
                               dim_Companyid,
                               dim_Vendorid,
                               dim_Currencyid,
                               dim_StockTypeid,
                               dim_specialstockid,
                               Dim_PurchaseOrgid,
                               Dim_PurchaseGroupid,
                               dim_producthierarchyid,
                               Dim_UnitOfMeasureid,
                               Dim_MovementIndicatorid,
                               Dim_ConsumptionTypeid,
                               dim_costcenterid,
                               Dim_DocumentStatusid,
                               Dim_DocumentTypeid,
                               Dim_IncoTermid,
                               Dim_ItemCategoryid,
                               Dim_ItemStatusid,
                               Dim_Termid,
                               Dim_PurchaseMiscid,
                               amt_StockValueAmt_GBL,
                               ct_TotalRestrictedStock,
                               ct_StockInQInsp,
                               ct_BlockedStock,
                               ct_StockInTransfer,
                               ct_UnrestrictedConsgnStock,
                               ct_RestrictedConsgnStock,
                               ct_BlockedConsgnStock,
                               ct_ConsgnStockInQInsp,
                               ct_BlockedStockReturns,
                               amt_BlockedStockAmt,
                               amt_BlockedStockAmt_GBL,
                               amt_StockInQInspAmt,
                               amt_StockInQInspAmt_GBL,
                               amt_StockInTransferAmt,
                               amt_StockInTransferAmt_GBL,
                               amt_UnrestrictedConsgnStockAmt,
                               amt_UnrestrictedConsgnStockAmt_GBL,
                               amt_StdUnitPrice,
                               amt_StdUnitPrice_GBL,
                               dim_stockcategoryid,
                               ct_StockInTransit,
                               amt_StockInTransitAmt,
                               amt_StockInTransitAmt_GBL,
                               Dim_SupplyingPlantId,
                               amt_MtlDlvrCost,
                               amt_OverheadCost,
                               amt_OtherCost,
                               amt_WIPBalance,
                               ct_WIPAging,
			       ct_WIPQty,
                               amt_MovingAvgPrice,
                               amt_PreviousPrice,
                               amt_CommericalPrice1,
                               amt_PlannedPrice1,
                               amt_PrevPlannedPrice,
                               amt_CurPlannedPrice,
                               Dim_DateIdPlannedPrice2,
                               Dim_DateIdLastChangedPrice,
                               Dim_DateidActualRelease,
                               Dim_DateidActualStart,
                               dd_ProdOrdernumber,
                               dd_ProdOrderitemno,
                               dd_Plannedorderno,
                               dim_productionorderstatusid,
                               Dim_productionordertypeid,
			       fact_inventoryagingid,
			       amt_ExchangeRate_GBL)
  SELECT 0 ct_StockQty,
        0 amt_StockValueAmt,
        0 ct_LastReceivedQty,
        1 dim_StorageLocEntryDateid,
        1 dim_LastReceivedDateid,
        po.Dim_PartidItem dim_Partid,
        po.Dim_Plantid dim_Plantid,
        po.Dim_StorageLocationid dim_StorageLocationid,
        po.Dim_Companyid dim_Companyid,
        1 dim_Vendorid,
        po.Dim_Currencyid dim_Currencyid,
        po.Dim_StockTypeid dim_StockTypeid,
        po.dim_specialstockid dim_specialstockid,
        po.Dim_PurchaseOrgid Dim_PurchaseOrgid,
        1 Dim_PurchaseGroupid,
        1 dim_producthierarchyid,
        po.Dim_UnitOfMeasureid Dim_UnitOfMeasureid,
        1 Dim_MovementIndicatorid,
        po.Dim_ConsumptionTypeid Dim_ConsumptionTypeid,
        1 dim_costcenterid,
        1 Dim_DocumentStatusid,
        1 Dim_DocumentTypeid,
        1 Dim_IncoTermid,
        1 Dim_ItemCategoryid,
        1 Dim_ItemStatusid,
        1 Dim_Termid,
        1 Dim_PurchaseMiscid,
        0 amt_StockValueAmt_GBL,
        0 ct_TotalRestrictedStock,
        0 ct_StockInQInsp,
        0 ct_BlockedStock,
        0 ct_StockInTransfer,
        0 ct_UnrestrictedConsgnStock,
        0 ct_RestrictedConsgnStock,
        0 ct_BlockedConsgnStock,
        0 ct_ConsgnStockInQInsp,
        0 ct_BlockedStockReturns,
        0 amt_BlockedStockAmt,
        0 amt_BlockedStockAmt_GBL,
        0 amt_StockInQInspAmt,
        0 amt_StockInQInspAmt_GBL,
        0 amt_StockInTransferAmt,
        0 amt_StockInTransferAmt_GBL,
        0 amt_UnrestrictedConsgnStockAmt,
        0 amt_UnrestrictedConsgnStockAmt_GBL,
        0 amt_StdUnitPrice,
        0 amt_StdUnitPrice_GBL,
        stkc.dim_stockcategoryid,
        0 ct_StockInTransit,
        0 amt_StockInTransitAmt,
        0 amt_StockInTransitAmt_GBL,
        1 Dim_SupplyingPlantId,
        0 amt_MtlDlvrCost,
        0 amt_OverheadCost,
        0 amt_OtherCost,
        po.amt_WIPBalance amt_WIPBalance,
	ifnull((ANSIDATE(LOCAL_TIMESTAMP) - (select pard.DateValue from dim_date pard
						where pard.Dim_Dateid = po.Dim_DateidActualRelease and po.Dim_DateidActualRelease > 1)),0)  ct_WIPAging,
	ifnull(po.ct_WIPQty,0),
        0 amt_MovingAvgPrice,
        0 amt_PreviousPrice,
        0 amt_CommericalPrice1,
        0 amt_PlannedPrice1,
        0 amt_PrevPlannedPrice,
        0 amt_CurPlannedPrice,
        1 Dim_DateIdPlannedPrice2,
        1 Dim_DateIdLastChangedPrice,
        po.Dim_DateidActualRelease Dim_DateidActualRelease,
        po.Dim_DateidActualStart Dim_DateidActualStart,
        po.dd_ordernumber dd_ProdOrdernumber,
        po.dd_orderitemno dd_ProdOrderitemno,
        po.dd_plannedorderno dd_Plannedorderno,
        po.dim_productionorderstatusid dim_productionorderstatusid,
        po.Dim_ordertypeid Dim_productionordertypeid,
	(SELECT max_id from NUMBER_FOUNTAIN WHERE table_name = 'fact_inventoryaging_tmp_populate') + row_number() over (),
	po.amt_ExchangeRate_GBL
  FROM fact_productionorder po 
	INNER JOIN dim_productionorderstatus ost ON po.dim_productionorderstatusid = ost.dim_productionorderstatusid
        INNER JOIN dim_part p ON po.Dim_PartidItem = p.dim_partid
        INNER JOIN dim_Currency dc ON dc.Dim_Currencyid = po.Dim_Currencyid, 
	tmp_stkcategory_001 stkc
  WHERE ost.Closed = 'Not Set' 
      and ost.Delivered = 'Not Set'
      and ost.Created = 'Not Set' 
      and ost.TechnicallyCompleted = 'Not Set'
      and po.ct_OrderItemQty - po.ct_GRQty > 0;

DELETE FROM fact_materialmovement_tmp_invagin;

UPDATE fact_inventoryaging_tmp_populate ig
  FROM dim_part pt, Dim_ProfitCenter pc
SET ig.dim_profitcenterid = pc.dim_profitcenterid
WHERE ig.dim_PartId = pt.Dim_PartId
 AND pc.ProfitCenterCode = pt.ProfitCenterCode
 AND pc.validto >= current_date
 AND pc.RowIsCurrent = 1;
 
UPDATE fact_inventoryaging_tmp_populate ig
SET ig.dim_profitcenterid = 1
WHERE ig.dim_profitcenterid IS NULL;
 

UPDATE fact_inventoryaging_tmp_populate
SET Dim_DateidActualRelease = 1
WHERE Dim_DateidActualRelease is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_DateidActualStart = 1
WHERE Dim_DateidActualStart is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_ProdOrdernumber = 'Not Set'
WHERE dd_ProdOrdernumber is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_ProdOrderitemno = 0
WHERE dd_ProdOrderitemno is null;

UPDATE fact_inventoryaging_tmp_populate
SET dd_Plannedorderno = 'Not Set'
WHERE dd_Plannedorderno is null;

UPDATE fact_inventoryaging_tmp_populate
SET dim_productionorderstatusid = 1
WHERE dim_productionorderstatusid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_productionordertypeid = 1
WHERE Dim_productionordertypeid is null;

UPDATE fact_inventoryaging_tmp_populate
SET Dim_profitcenterid = 1
WHERE Dim_profitcenterid is null;

call vectorwise(combine 'fact_inventoryaging_tmp_populate');

/* Drop original table and rename staging table to orig table name */
drop table if exists fact_inventoryaging;
rename table fact_inventoryaging_tmp_populate to fact_inventoryaging;

\i /db/schema_migration/bin/wrapper_optimizedb.sh fact_inventoryaging;

/* Drop all the intermediate tables created */

drop table if exists materialmovement_tmp_new; 
drop table if exists a1;
drop table if exists a99;
drop table if exists b2;
drop table if exists b2;	
drop table if exists b3;
drop table if exists b4;
drop table if exists b;
drop table if exists ia1;
drop table if exists pl_prt_mbew_no_bwtar;
drop table if exists tmp2_keph;
drop table if exists tmp_MBEW_NO_BWTAR;
drop table if exists tmp_MBEWH_NO_BWTAR;
drop table if exists tmp2_mbew_no_bwtar;
drop table if exists tmp2_tcurf;
drop table if exists tmp2_tcurr;
drop table if exists tmp3_mbew_no_bwtar;
drop table if exists tmp3_tcurr;
drop table if exists tmp4_dim_date;
drop table if exists tmp4_mbew_no_bwtar;
drop table if exists tmp_all_x;
drop table if exists tmp_all_x_2;
drop table if exists tmp_c1;
drop table if exists tmp_c2;
drop table if exists tmp_c3;
drop table if exists tmp_dim_date;
drop table if exists tmp_fact_mm_0;
drop table if exists tmp_fact_mm_1;
drop table if exists tmp_fact_mm_2;
drop table if exists tmp_fact_mm_postingdate_0;
drop table if exists tmp_fact_mm_postingdate_1;
drop table if exists tmp_fact_mm_postingdate_2;
drop table if exists tmp_fact_mm_postingdate_2_min;
drop table if exists tmp_keph;
drop table if exists tmp_lrd;
drop table if exists tmp_mard;
drop table if exists tmp_mbew_no_bwtar;
drop table if exists tmp_tbl_storagelocentrydate;
drop table if exists tmp_tcurf;
drop table if exists tmp_tcurr;
DROP TABLE IF EXISTS tmp_GlobalCurr_001;
DROP TABLE IF EXISTS tmp_stkcategory_001;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins_c1;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_ins;
DROP TABLE IF EXISTS tmp_ivaging_ins5_notexists_del;

