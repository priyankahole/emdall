


/* Update 1 */

/* Update column Dim_ClearedFlagId */

/* This will first update all Dim_ClearedFlagId ( for rows that match the main join condition ) to 1 */
UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_ClearedFlagId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_ClearedFlagId <> 1;

/* This will update just the rows that match the inner subquery */
/* Dim_ClearedFlagId is not null */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_AccountPayableStatus ars
SET Dim_ClearedFlagId = ars.Dim_AccountPayableStatusId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND ars.Status = CASE WHEN BSIK_REBZG IS NOT NULL AND BSIK_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END AND ars.RowIsCurrent = 1
AND fap.Dim_ClearedFlagId <> ars.Dim_AccountPayableStatusId;


/* Update column Dim_DateIdAccDocDateEntered */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_DateIdAccDocDateEntered = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND  fap.Dim_DateIdAccDocDateEntered <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt
SET Dim_DateIdAccDocDateEntered = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_CPUDT AND dt.CompanyCode = arc.BSIK_BUKRS
AND fap.Dim_DateIdAccDocDateEntered <> dt.dim_dateid;


/* Update column Dim_DateIdBaseDateForDueDateCalc */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_DateIdBaseDateForDueDateCalc = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdBaseDateForDueDateCalc <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt
SET Dim_DateIdBaseDateForDueDateCalc = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_ZFBDT AND dt.CompanyCode = arc.BSIK_BUKRS
AND Dim_DateIdBaseDateForDueDateCalc <> dt.dim_dateid;


/* Update column Dim_DateIdCreated */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_DateIdCreated = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND Dim_DateIdCreated <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt
SET Dim_DateIdCreated = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_BLDAT AND dt.CompanyCode = arc.BSIK_BUKRS
AND Dim_DateIdCreated <> dt.dim_dateid;


/* Update column Dim_DateIdPosting */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_DateIdPosting = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND Dim_DateIdPosting <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt
SET Dim_DateIdPosting = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_BUDAT AND dt.CompanyCode = arc.BSIK_BUKRS
AND Dim_DateIdPosting <> dt.dim_dateid;


/* Update column Dim_BlockingPaymentReasonId */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_BlockingPaymentReasonId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_BlockingPaymentReasonId <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_BlockingPaymentReason bpr
SET Dim_BlockingPaymentReasonId = bpr.Dim_BlockingPaymentReasonId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND bpr.BlockingKeyPayment = BSIK_ZLSPR AND bpr.RowIsCurrent = 1
AND fap.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId;


/* Update column Dim_BusinessAreaId */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_BusinessAreaId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_BusinessAreaId <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_BusinessArea ba
SET Dim_BusinessAreaId = ba.Dim_BusinessAreaId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND ba.BusinessArea = BSIK_GSBER AND ba.RowIsCurrent = 1
AND fap.Dim_BusinessAreaId <> ba.Dim_BusinessAreaId;


/* Update column Dim_Currencyid */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_Currencyid = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_Currencyid <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_currency c
SET Dim_Currencyid = c.dim_currencyid
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
 AND c.CurrencyCode = dcm.Currency
AND fap.Dim_Currencyid <> c.dim_currencyid;


/* Update column Dim_DateIdClearing */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_DateIdClearing = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdClearing <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_date dt
SET Dim_DateIdClearing = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSIK_AUGDT AND dt.CompanyCode = arc.BSIK_BUKRS
AND  fap.Dim_DateIdClearing <> dt.dim_dateid;


/* Update column Dim_DocumentTypeId */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_DocumentTypeId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DocumentTypeId <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,dim_documenttypetext dtt
SET Dim_DocumentTypeId = dtt.dim_documenttypetextid
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
 AND dtt.type = BSIK_BLART AND dtt.RowIsCurrent = 1
AND fap.Dim_DocumentTypeId <> dtt.dim_documenttypetextid;


/* Update column Dim_PaymentReasonId */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_PaymentReasonId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_PaymentReasonId <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_PaymentReason pr
SET Dim_PaymentReasonId = pr.Dim_PaymentReasonId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND pr.PaymentReasonCode = BSIK_RSTGR AND pr.CompanyCode = BSIK_BUKRS AND pr.RowIsCurrent = 1
AND fap.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId;


/* Update column Dim_PostingKeyId */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_PostingKeyId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_PostingKeyId <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_PostingKey pk
SET Dim_PostingKeyId = pk.Dim_PostingKeyId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND pk.PostingKey = BSIK_BSCHL AND pk.SpecialGLIndicator = ifnull(BSIK_UMSKZ, 'Not Set') AND pk.RowIsCurrent = 1
AND fap.Dim_PostingKeyId <> pk.Dim_PostingKeyId;


/* Update column Dim_SpecialGLIndicatorId */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_SpecialGLIndicatorId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_SpecialGLIndicatorId <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_SpecialGLIndicator sgl
SET Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND sgl.SpecialGLIndicator = BSIK_UMSKZ AND sgl.AccountType = 'D' AND sgl.RowIsCurrent = 1
AND fap.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId;


/* Update column Dim_TargetSpecialGLIndicatorId */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_TargetSpecialGLIndicatorId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_TargetSpecialGLIndicatorId <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_SpecialGLIndicator sgl
SET Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND sgl.SpecialGLIndicator = BSIK_ZUMSK AND sgl.AccountType = 'D' AND sgl.RowIsCurrent = 1
AND fap.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId;


/* Update column Dim_SpecialGlTransactionTypeId */

UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
SET Dim_SpecialGlTransactionTypeId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_SpecialGlTransactionTypeId <> 1;


UPDATE fact_accountspayable fap
from bsik arc, dim_company dcm, dim_vendor dv
 ,Dim_SpecialGlTransactionType sgt
SET Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND sgt.SpecialGlTransactionTypeId = BSIK_UMSKS AND sgt.RowIsCurrent = 1
AND fap.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId;

/* Update column Dim_CustomerPaymentTermsid */

UPDATE fact_accountspayable fap
from BSIK arc, dim_company dcm, dim_Vendor dv
 ,Dim_Term cpt
SET Dim_CustomerPaymentTermsid = cpt.Dim_Termid
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
  AND cpt.TermCode = BSIK_ZTERM 
AND  arc.BSIK_ZTERM IS NOT NULL;

UPDATE fact_accountspayable fap
from BSIK arc, dim_company dcm, dim_Vendor dv
SET Dim_CustomerPaymentTermsid = 1
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND arc.BSIK_ZTERM IS NULL;
/* Now the colums which did not have inner subqueries */

UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET  amt_CashDiscountDocCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_WSKTO
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET amt_CashDiscountLocalCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_SKNTO
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET amt_InLocalCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_DMBTR
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET amt_TaxInDocCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_WMWST
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET amt_TaxInLocalCurrency = (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSIK_MWSTS
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET dd_AccountingDocItemNo = BSIK_BUZEI
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_AccountingDocItemNo <> BSIK_BUZEI;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET dd_AccountingDocNo = BSIK_BELNR
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_AccountingDocNo <> BSIK_BELNR;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET
dd_AssignmentNumber = ifnull(BSIK_ZUONR, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_AssignmentNumber <> ifnull(BSIK_ZUONR, 'Not Set');


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET dd_debitcreditid = (CASE WHEN BSIK_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1;

UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET
dd_ClearingDocumentNo = ifnull(BSIK_AUGBL, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_ClearingDocumentNo <> ifnull(BSIK_AUGBL, 'Not Set');


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET
dd_FixedPaymentTerms = ifnull(BSIK_ZBFIX, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_FixedPaymentTerms <> ifnull(BSIK_ZBFIX, 'Not Set');


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET
dd_InvoiceNumberTransBelongTo = ifnull(BSIK_REBZG, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_InvoiceNumberTransBelongTo <> ifnull(BSIK_REBZG, 'Not Set');


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET fap.dim_companyid = dcm.Dim_CompanyId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.dim_companyid <> dcm.Dim_CompanyId;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET fap.Dim_VendorID = dv.dim_VendorId
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_VendorID <> dv.dim_VendorId;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET
dd_DocumentNo = ifnull(BSIK_EBELN, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_DocumentNo <> ifnull(BSIK_EBELN, 'Not Set');


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET dd_DocumentItemNo = BSIK_EBELP
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_DocumentItemNo <> BSIK_EBELP;

UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET
dd_ReferenceDocumentNo = ifnull(BSIK_XBLNR, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_ReferenceDocumentNo <> ifnull(BSIK_XBLNR, 'Not Set');


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET dd_CashDiscountPercentage1 = BSIK_ZBD1P
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_CashDiscountPercentage1 <> BSIK_ZBD1P;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET dd_CashDiscountPercentage2 = BSIK_ZBD2P
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_CashDiscountPercentage2 <> BSIK_ZBD2P;


UPDATE fact_accountspayable fap from bsik arc, dim_company dcm, dim_vendor dv
SET
dd_ProductionOrderNo = ifnull(BSIK_AUFNR, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSIK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dd_ProductionOrderNo <> ifnull(BSIK_AUFNR, 'Not Set');



/* End of Update 1 */

DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_accountspayable';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_accountspayable', ifnull(max(fact_accountspayableid), 0)
     FROM fact_accountspayable;


INSERT INTO fact_accountspayable(fact_accountspayableid,
                                 amt_CashDiscountDocCurrency,
                                 amt_CashDiscountLocalCurrency,
                                 amt_InDocCurrency,
                                 amt_InLocalCurrency,
                                 amt_TaxInDocCurrency,
                                 amt_TaxInLocalCurrency,
                                 dd_AccountingDocItemNo,
                                 dd_AccountingDocNo,
                                 dd_AssignmentNumber,
                                 Dim_ClearedFlagId,
                                 Dim_DateIdAccDocDateEntered,
                                 Dim_DateIdBaseDateForDueDateCalc,
                                 Dim_DateIdCreated,
                                 Dim_DateIdPosting,
                                 dd_debitcreditid,
                                 dd_ClearingDocumentNo,
                                 dd_FiscalPeriod,
                                 dd_FiscalYear,
                                 dd_FixedPaymentTerms,
                                 dd_InvoiceNumberTransBelongTo,
                                 Dim_BlockingPaymentReasonId,
                                 Dim_BusinessAreaId,
                                 Dim_CompanyId,
                                 Dim_CurrencyId,
                                 Dim_VendorId,
                                 Dim_DateIdClearing,
                                 Dim_DocumentTypeId,
                                 Dim_PaymentReasonId,
                                 Dim_PostingKeyId,
                                 Dim_SpecialGLIndicatorId,
                                 Dim_TargetSpecialGLIndicatorId,
                                 dd_DocumentNo,
                                 dd_DocumentItemNo,
                                 dd_ReferenceDocumentNo,
                                 dd_CashDiscountPercentage1,
                                 dd_CashDiscountPercentage2,
                                 dd_ProductionOrderNo,
                                 Dim_SpecialGlTransactionTypeId,
								 Dim_CustomerPaymentTermsid)
   SELECT ((SELECT max_id
              FROM NUMBER_FOUNTAIN
             WHERE table_name = 'fact_accountspayable')
           + row_number() over ()) as fact_accountspayableid,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_WSKTO amt_CashDiscountDocCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_SKNTO amt_CashDiscountLocalCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_WRBTR amt_InDocCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_DMBTR amt_InLocalCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_WMWST amt_TaxInDocCurrency,
 (CASE WHEN BSIK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSIK_MWSTS amt_TaxInLocalCurrency,
 BSIK_BUZEI dd_AccountingDocItemNo,
 BSIK_BELNR dd_AccountingDocNo,
 ifnull(BSIK_ZUONR,'Not Set') dd_AssignmentNumber,
 ifnull((SELECT Dim_AccountPayableStatusId
 FROM Dim_AccountPayableStatus ars
 WHERE ars.Status = CASE WHEN BSIK_REBZG IS NOT NULL AND BSIK_REBZJ <> 0 THEN 'P - Partial' ELSE 'O - Open' END
 AND ars.RowIsCurrent = 1), 1) Dim_ClearedFlagId,
 ifnull((SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSIK_CPUDT
 AND dt.CompanyCode = arc.BSIK_BUKRS
 ), 1) Dim_DateIdAccDocDateEntered,
 ifnull((SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSIK_ZFBDT
 AND dt.CompanyCode = arc.BSIK_BUKRS
 ), 1) Dim_DateIdBaseDateForDueDateCalc,
 ifnull( (SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSIK_BLDAT
 AND dt.CompanyCode = arc.BSIK_BUKRS
 ), 1) Dim_DateIdCreated,
 ifnull( (SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSIK_BUDAT
 AND dt.CompanyCode = arc.BSIK_BUKRS)
 , 1) Dim_DateIdPosting,
 (CASE WHEN BSIK_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END) dd_debitcreditid,
 ifnull(BSIK_AUGBL,'Not Set') dd_ClearingDocumentNo,
 BSIK_MONAT dd_FiscalPeriod,
 BSIK_GJAHR dd_FiscalYear,
 ifnull(BSIK_ZBFIX, 'Not Set') dd_FixedPaymentTerms,
 ifnull(BSIK_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
 ifnull( ( SELECT Dim_BlockingPaymentReasonId
 FROM Dim_BlockingPaymentReason bpr
 WHERE bpr.BlockingKeyPayment = BSIK_ZLSPR
 AND bpr.RowIsCurrent = 1 ), 1) Dim_BlockingPaymentReasonId,
 ifnull( ( SELECT Dim_BusinessAreaId
 FROM Dim_BusinessArea ba
 WHERE ba.BusinessArea = BSIK_GSBER
 AND ba.RowIsCurrent = 1), 1) Dim_BusinessAreaId,
 dc.Dim_CompanyId,
 ifnull((SELECT dim_currencyid
 FROM dim_currency c
 WHERE c.CurrencyCode = dc.Currency),1) Dim_Currencyid,
 dv.Dim_VendorID,
 ifnull( (SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSIK_AUGDT
 AND dt.CompanyCode = arc.BSIK_BUKRS), 1) Dim_DateIdClearing,
 ifnull(( SELECT dim_documenttypetextid
 FROM dim_documenttypetext dtt
 WHERE dtt.type = BSIK_BLART
 AND dtt.RowIsCurrent = 1 ), 1) Dim_DocumentTypeId,
 ifnull((SELECT Dim_PaymentReasonId
 FROM Dim_PaymentReason pr
 WHERE pr.PaymentReasonCode = BSIK_RSTGR
 AND pr.CompanyCode = BSIK_BUKRS
 AND pr.RowIsCurrent = 1), 1) Dim_PaymentReasonId,
 ifnull( ( SELECT Dim_PostingKeyId
 FROM Dim_PostingKey pk
 WHERE pk.PostingKey= BSIK_BSCHL
 AND pk.SpecialGLIndicator = ifnull(BSIK_UMSKZ,'Not Set')
 AND pk.RowIsCurrent = 1), 1) Dim_PostingKeyId,
 ifnull( ( SELECT Dim_SpecialGLIndicatorId
 FROM Dim_SpecialGLIndicator sgl
 WHERE sgl.SpecialGLIndicator = BSIK_UMSKZ
 AND sgl.AccountType = 'D'
 AND sgl.RowIsCurrent = 1), 1) Dim_SpecialGLIndicatorId,
 ifnull( ( SELECT Dim_SpecialGLIndicatorId
 FROM Dim_SpecialGLIndicator sgl
 WHERE sgl.SpecialGLIndicator = BSIK_ZUMSK
 AND sgl.AccountType = 'D'
 AND sgl.RowIsCurrent = 1), 1) Dim_TargetSpecialGLIndicatorId,
 ifnull(BSIK_EBELN, 'Not Set') dd_DocumentNo,
 BSIK_EBELP dd_DocumentItemNo,
 ifnull(BSIK_XBLNR,'Not Set') dd_ReferenceDocumentNo,
 BSIK_ZBD1P dd_CashDiscountPercentage1,
 BSIK_ZBD2P dd_CashDiscountPercentage2,
 ifnull(BSIK_AUFNR,'Not Set') dd_ProductionOrderNo,
 ifnull ( ( SELECT Dim_SpecialGlTransactionTypeId
 FROM Dim_SpecialGlTransactionType sgt
 WHERE sgt.SpecialGlTransactionTypeId = BSIK_UMSKS
 AND sgt.RowIsCurrent = 1 ) , 1) Dim_SpecialGlTransactionTypeId,
 ifnull ( ( SELECT Dim_Termid
 FROM Dim_Term cpt
 WHERE cpt.TermCode = BSIK_ZTERM) , 1) Dim_CustomerPaymentTermsid
 FROM BSIK arc
 INNER JOIN dim_company dc ON dc.CompanyCode = ifnull(arc.BSIK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 INNER JOIN dim_vendor dv ON dv.VendorNumber = ifnull(arc.BSIK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 WHERE NOT EXISTS
 (SELECT 1
 FROM fact_accountspayable ar
 WHERE ar.dd_AccountingDocNo = arc.BSIK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSIK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR,'Not Set')
 AND ar.dd_fiscalyear = arc.BSIK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSIK_MONAT
 AND ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId);


/* Update 2 */


/* Update column Dim_ClearedFlagId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_ClearedFlagId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_ClearedFlagId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_AccountPayableStatus ars
SET Dim_ClearedFlagId = ars.Dim_AccountPayableStatusId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND ars.Status = CASE WHEN BSAK_REBZG IS NOT NULL AND BSAK_REBZJ <> 0 THEN 'F - Partial' ELSE 'C - Cleared' END AND ars.RowIsCurrent = 1
AND fap.Dim_ClearedFlagId <> ars.Dim_AccountPayableStatusId;


/* Update column Dim_DateIdAccDocDateEntered */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_DateIdAccDocDateEntered = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdAccDocDateEntered <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt
SET Dim_DateIdAccDocDateEntered = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_CPUDT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdAccDocDateEntered <> dt.dim_dateid;


/* Update column Dim_DateIdBaseDateForDueDateCalc */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_DateIdBaseDateForDueDateCalc = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdBaseDateForDueDateCalc <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt
SET Dim_DateIdBaseDateForDueDateCalc = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_ZFBDT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdBaseDateForDueDateCalc <> dt.dim_dateid;


/* Update column Dim_DateIdCreated */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_DateIdCreated = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdCreated <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt
SET Dim_DateIdCreated = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_BLDAT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdCreated <> dt.dim_dateid;


/* Update column Dim_DateIdPosting */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_DateIdPosting = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdPosting <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt
SET Dim_DateIdPosting = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_BUDAT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdPosting <> dt.dim_dateid;


/* Update column Dim_BlockingPaymentReasonId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_BlockingPaymentReasonId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_BlockingPaymentReasonId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_BlockingPaymentReason bpr
SET Dim_BlockingPaymentReasonId = bpr.Dim_BlockingPaymentReasonId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND bpr.BlockingKeyPayment = BSAK_ZLSPR AND bpr.RowIsCurrent = 1
AND fap.Dim_BlockingPaymentReasonId <> bpr.Dim_BlockingPaymentReasonId;


/* Update column Dim_BusinessAreaId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_BusinessAreaId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_BusinessAreaId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_BusinessArea ba
SET Dim_BusinessAreaId = ba.Dim_BusinessAreaId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND ba.BusinessArea = BSAK_GSBER AND ba.RowIsCurrent = 1
AND fap.Dim_BusinessAreaId <> ba.Dim_BusinessAreaId;


/* Update column Dim_Currencyid */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_Currencyid = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_Currencyid <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_currency c
SET Dim_Currencyid = c.dim_currencyid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
 AND c.CurrencyCode = dcm.Currency
AND fap.Dim_Currencyid <> c.dim_currencyid;


/* Update column Dim_DateIdClearing */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_DateIdClearing = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DateIdClearing <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_date dt
SET Dim_DateIdClearing = dt.dim_dateid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND dt.DateValue = BSAK_AUGDT AND dt.CompanyCode = arc.BSAK_BUKRS
AND fap.Dim_DateIdClearing <> dt.dim_dateid;


/* Update column Dim_DocumentTypeId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_DocumentTypeId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_DocumentTypeId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,dim_documenttypetext dtt
SET Dim_DocumentTypeId = dtt.dim_documenttypetextid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
 AND dtt.type = BSAK_BLART AND dtt.RowIsCurrent = 1
AND fap.Dim_DocumentTypeId <> dtt.dim_documenttypetextid;


/* Update column Dim_PaymentReasonId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_PaymentReasonId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_PaymentReasonId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_PaymentReason pr
SET Dim_PaymentReasonId = pr.Dim_PaymentReasonId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND pr.PaymentReasonCode = BSAK_RSTGR AND pr.CompanyCode = BSAK_BUKRS AND pr.RowIsCurrent = 1
AND fap.Dim_PaymentReasonId <> pr.Dim_PaymentReasonId;


/* Update column Dim_PostingKeyId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_PostingKeyId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_PostingKeyId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_PostingKey pk
SET Dim_PostingKeyId = pk.Dim_PostingKeyId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND pk.PostingKey = BSAK_BSCHL AND pk.SpecialGLIndicator =  ifnull(BSAK_UMSKZ, 'Not Set') AND pk.RowIsCurrent = 1
AND fap.Dim_PostingKeyId <> pk.Dim_PostingKeyId;


/* Update column Dim_SpecialGLIndicatorId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_SpecialGLIndicatorId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_SpecialGLIndicatorId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_SpecialGLIndicator sgl
SET Dim_SpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND sgl.SpecialGLIndicator = BSAK_UMSKZ AND sgl.AccountType = 'D' AND sgl.RowIsCurrent = 1
AND fap.Dim_SpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId;


/* Update column Dim_TargetSpecialGLIndicatorId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_TargetSpecialGLIndicatorId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_TargetSpecialGLIndicatorId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_SpecialGLIndicator sgl
SET Dim_TargetSpecialGLIndicatorId = sgl.Dim_SpecialGLIndicatorId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND sgl.SpecialGLIndicator = BSAK_ZUMSK AND sgl.AccountType = 'D' AND sgl.RowIsCurrent = 1
AND fap.Dim_TargetSpecialGLIndicatorId <> sgl.Dim_SpecialGLIndicatorId;


/* Update column Dim_SpecialGlTransactionTypeId */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_SpecialGlTransactionTypeId = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND fap.Dim_SpecialGlTransactionTypeId <> 1;


UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_SpecialGlTransactionType sgt
SET Dim_SpecialGlTransactionTypeId = sgt.Dim_SpecialGlTransactionTypeId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND sgt.SpecialGlTransactionTypeId = BSAK_UMSKS AND sgt.RowIsCurrent = 1
AND fap.Dim_SpecialGlTransactionTypeId <> sgt.Dim_SpecialGlTransactionTypeId;

/* Update column Dim_CustomerPaymentTermsid */

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
 ,Dim_Term cpt
SET Dim_CustomerPaymentTermsid = cpt.Dim_Termid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
  AND cpt.TermCode = BSAK_ZTERM
AND arc.BSAK_ZTERM  IS NOT NULL;

UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET Dim_CustomerPaymentTermsid = 1
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
AND fap.dd_fiscalyear = arc.BSAK_GJAHR
AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
AND dcm.CompanyCode = arc.BSAK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSAK_LIFNR
AND dv.RowIsCurrent = 1
AND arc.BSAK_ZTERM IS NULL;
/* Now the columns that did not have inner subquery in mysql */

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET  amt_CashDiscountDocCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_WSKTO
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET amt_CashDiscountLocalCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_SKNTO
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1;


 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET amt_InLocalCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_DMBTR
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET amt_TaxInDocCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_WMWST
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET amt_TaxInLocalCurrency = (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) * BSAK_MWSTS
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET dd_AccountingDocItemNo = BSAK_BUZEI
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_AccountingDocItemNo  <>  BSAK_BUZEI;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET dd_AccountingDocNo = BSAK_BELNR
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_AccountingDocNo  <>  BSAK_BELNR;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET
dd_AssignmentNumber = ifnull(BSAK_ZUONR, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_AssignmentNumber  <>  ifnull(BSAK_ZUONR, 'Not Set');

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET dd_debitcreditid = (CASE WHEN BSAK_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END)
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET
dd_ClearingDocumentNo = ifnull(BSAK_AUGBL, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_ClearingDocumentNo  <>  ifnull(BSAK_AUGBL, 'Not Set');

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET
dd_FixedPaymentTerms = ifnull(BSAK_ZBFIX, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_FixedPaymentTerms  <>  ifnull(BSAK_ZBFIX, 'Not Set');

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET
dd_InvoiceNumberTransBelongTo = ifnull(BSAK_REBZG, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_InvoiceNumberTransBelongTo  <>  ifnull(BSAK_REBZG, 'Not Set');

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET fap.dim_companyid = dcm.Dim_CompanyId
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dim_companyid  <>  dcm.Dim_CompanyId;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET fap.Dim_VendorID = dv.Dim_Vendorid
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.Dim_VendorID  <>  dv.Dim_Vendorid;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET
dd_DocumentNo = ifnull(BSAK_EBELN, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_DocumentNo  <>  ifnull(BSAK_EBELN, 'Not Set');

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET dd_DocumentItemNo = BSAK_EBELP
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_DocumentItemNo  <>  BSAK_EBELP;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET
dd_ReferenceDocumentNo = ifnull(BSAK_XBLNR, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_ReferenceDocumentNo  <>  ifnull(BSAK_XBLNR, 'Not Set');

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET dd_CashDiscountPercentage1 = BSAK_ZBD1P
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_CashDiscountPercentage1  <>  BSAK_ZBD1P;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET dd_CashDiscountPercentage2 = BSAK_ZBD2P
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_CashDiscountPercentage2  <>  BSAK_ZBD2P;

 UPDATE fact_accountspayable fap
from BSAK arc, dim_company dcm, dim_Vendor dv
SET
dd_ProductionOrderNo = ifnull(BSAK_AUFNR, 'Not Set')
WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
 AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND fap.dd_fiscalyear = arc.BSAK_GJAHR
 AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
 AND dcm.CompanyCode = arc.BSAK_BUKRS
 AND dcm.RowIsCurrent = 1
 AND dv.VendorNumber = arc.BSAK_LIFNR
 AND dv.RowIsCurrent = 1
AND fap.dd_ProductionOrderNo  <>  ifnull(BSAK_AUFNR, 'Not Set');


/* End of Update 2 */


DELETE FROM NUMBER_FOUNTAIN
 WHERE table_name = 'fact_accountspayable';

INSERT INTO NUMBER_FOUNTAIN
   SELECT 'fact_accountspayable', ifnull(max(fact_accountspayableid), 0)
     FROM fact_accountspayable;



INSERT INTO fact_accountspayable(fact_accountspayableid,
                                 amt_CashDiscountDocCurrency,
                                 amt_CashDiscountLocalCurrency,
                                 amt_InDocCurrency,
                                 amt_InLocalCurrency,
                                 amt_TaxInDocCurrency,
                                 amt_TaxInLocalCurrency,
                                 dd_AccountingDocItemNo,
                                 dd_AccountingDocNo,
                                 dd_AssignmentNumber,
                                 Dim_ClearedFlagId,
                                 Dim_DateIdAccDocDateEntered,
                                 Dim_DateIdBaseDateForDueDateCalc,
                                 Dim_DateIdCreated,
                                 Dim_DateIdPosting,
                                 dd_debitcreditid,
                                 dd_ClearingDocumentNo,
                                 dd_FiscalPeriod,
                                 dd_FiscalYear,
                                 dd_FixedPaymentTerms,
                                 dd_InvoiceNumberTransBelongTo,
                                 Dim_BlockingPaymentReasonId,
                                 Dim_BusinessAreaId,
                                 Dim_CompanyId,
                                 Dim_CurrencyId,
                                 Dim_VendorId,
                                 Dim_DateIdClearing,
                                 Dim_DocumentTypeId,
                                 Dim_PaymentReasonId,
                                 Dim_PostingKeyId,
                                 Dim_SpecialGLIndicatorId,
                                 Dim_TargetSpecialGLIndicatorId,
                                 dd_DocumentNo,
                                 dd_DocumentItemNo,
                                 dd_ReferenceDocumentNo,
                                 dd_CashDiscountPercentage1,
                                 dd_CashDiscountPercentage2,
                                 dd_ProductionOrderNo,
                                 Dim_SpecialGlTransactionTypeId,
								 Dim_CustomerPaymentTermsid)
SELECT ((SELECT max_id
           FROM NUMBER_FOUNTAIN
          WHERE table_name = 'fact_accountspayable')
        + row_number() over ()) as fact_accountspayableid,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_WSKTO amt_CashDiscountDocCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_SKNTO amt_CashDiscountLocalCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_WRBTR amt_InDocCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_DMBTR amt_InLocalCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_WMWST amt_TaxInDocCurrency,
 (CASE WHEN BSAK_SHKZG = 'H' THEN -1 ELSE 1 END) *BSAK_MWSTS amt_TaxInLocalCurrency,
 BSAK_BUZEI dd_AccountingDocItemNo,
 BSAK_BELNR dd_AccountingDocNo,
 ifnull(BSAK_ZUONR, 'Not Set') dd_AssignmentNumber,
 ifnull((SELECT Dim_AccountPayableStatusId
 FROM Dim_AccountPayableStatus ars
 WHERE ars.Status = CASE WHEN BSAK_REBZG IS NOT NULL AND BSAK_REBZJ <> 0 THEN 'F - Partial' ELSE 'C - Cleared' END
 AND ars.RowIsCurrent = 1), 1) Dim_ClearedFlagId,
 ifnull((SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSAK_CPUDT
 AND dt.CompanyCode = BSAK_BUKRS
 ), 1) Dim_DateIdAccDocDateEntered,
 ifnull((SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSAK_ZFBDT
 AND dt.CompanyCode = BSAK_BUKRS
 ), 1 ) Dim_DateIdBaseDateForDueDateCalc,
 ifnull((SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSAK_BLDAT
 AND dt.CompanyCode = BSAK_BUKRS
 ), 1) Dim_DateIdCreated,
 ifnull((SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSAK_BUDAT
 AND dt.CompanyCode = BSAK_BUKRS
 ), 1) Dim_DateIdPosting,
 (CASE WHEN BSAK_SHKZG = 'H' THEN 'Credit' ELSE 'Debit' END) dd_debitcreditid,
 ifnull(BSAK_AUGBL,'Not Set') dd_ClearingDocumentNo,
 BSAK_MONAT dd_FiscalPeriod,
 BSAK_GJAHR dd_FiscalYear,
 ifnull(BSAK_ZBFIX, 'Not Set') dd_FixedPaymentTerms,
 ifnull(BSAK_REBZG,'Not Set') dd_InvoiceNumberTransBelongTo,
 ifnull(( SELECT Dim_BlockingPaymentReasonId
 FROM Dim_BlockingPaymentReason bpr
 WHERE bpr.BlockingKeyPayment = BSAK_ZLSPR
 AND bpr.RowIsCurrent = 1
 ), 1 ) Dim_BlockingPaymentReasonId,
 ifnull(( SELECT Dim_BusinessAreaId
 FROM Dim_BusinessArea ba
 WHERE ba.BusinessArea = BSAK_GSBER
 AND ba.RowIsCurrent = 1
 ), 1) Dim_BusinessAreaId,
 dc.Dim_CompanyId,
 ifnull((SELECT dim_currencyid
 FROM dim_currency c
 WHERE c.CurrencyCode = dc.Currency)
 , 1) Dim_Currencyid,
 dv.Dim_VendorId,
 ifnull((SELECT dim_dateid
 FROM dim_date dt
 WHERE dt.DateValue = BSAK_AUGDT
 AND dt.CompanyCode = BSAK_BUKRS
 ), 1) Dim_DateIdClearing,
 ifnull((SELECT dim_documenttypetextid
 FROM dim_documenttypetext dtt
 WHERE dtt.type = BSAK_BLART
 AND dtt.RowIsCurrent = 1
 ), 1) Dim_DocumentTypeId,
 ifnull((SELECT Dim_PaymentReasonId
 FROM Dim_PaymentReason pr
 WHERE pr.PaymentReasonCode = BSAK_RSTGR
 AND pr.CompanyCode = BSAK_BUKRS
 AND pr.RowIsCurrent = 1
 ), 1) Dim_PaymentReasonId,
 ifnull(( SELECT Dim_PostingKeyId
 FROM Dim_PostingKey pk
 WHERE pk.PostingKey= BSAK_BSCHL
 AND pk.SpecialGLIndicator = ifnull(BSAK_UMSKZ,'Not Set')
 AND pk.RowIsCurrent = 1
 ), 1) Dim_PostingKeyId,
 ifnull(( SELECT Dim_SpecialGLIndicatorId
 FROM Dim_SpecialGLIndicator sgl
 WHERE sgl.SpecialGLIndicator = BSAK_UMSKZ
 AND sgl.AccountType = 'D'
 AND sgl.RowIsCurrent = 1
 ), 1) Dim_SpecialGLIndicatorId,
 ifnull(( SELECT Dim_SpecialGLIndicatorId
 FROM Dim_SpecialGLIndicator sgl
 WHERE sgl.SpecialGLIndicator = BSAK_ZUMSK
 AND sgl.AccountType = 'D'
 AND sgl.RowIsCurrent = 1
 ), 1) Dim_TargetSpecialGLIndicatorId,
 ifnull(BSAK_EBELN, 'Not Set') dd_DocumentNo,
 BSAK_EBELP dd_DocumentItemNo,
 ifnull(BSAK_XBLNR,'Not Set') dd_ReferenceDocumentNo,
 BSAK_ZBD1P dd_CashDiscountPercentage1,
 BSAK_ZBD2P dd_CashDiscountPercentage2,
 ifnull(BSAK_AUFNR,'Not Set') dd_ProductionOrderNo,
 ifnull ( ( SELECT Dim_SpecialGlTransactionTypeId
 FROM Dim_SpecialGlTransactionType sgt
 WHERE sgt.SpecialGlTransactionTypeId = BSAK_UMSKS
 AND sgt.RowIsCurrent = 1 ) , 1) Dim_SpecialGlTransactionTypeId,
 ifnull ( ( SELECT Dim_Termid
 FROM Dim_Term cpt
 WHERE cpt.TermCode = BSAK_ZTERM) , 1) Dim_CustomerPaymentTermsid
 FROM BSAK arc
 INNER JOIN dim_company dc ON dc.CompanyCode = ifnull(BSAK_BUKRS,'Not Set') AND dc.RowIsCurrent = 1
 INNER JOIN dim_Vendor dv ON dv.VendorNumber = ifnull(BSAK_LIFNR,'Not Set') AND dv.RowIsCurrent = 1
 WHERE NOT EXISTS
 (SELECT 1
 FROM fact_accountspayable ar
 WHERE ar.dd_AccountingDocNo = arc.BSAK_BELNR
 AND ar.dd_AccountingDocItemNo = arc.BSAK_BUZEI
 AND ar.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
 AND ar.dd_fiscalyear = arc.BSAK_GJAHR
 AND ar.dd_FiscalPeriod = arc.BSAK_MONAT
 AND ar.dim_companyid = dc.Dim_CompanyId
 AND ar.dim_vendorid = dv.Dim_VendorId);

/* Split Update3 into 2 queries : (Dim_NetDueDateId1,Dim_NetDueDateId2,Dim_NetDueDateId3 in the first query,
   Dim_NetDueDateWrtCashDiscountTerms1 and Dim_NetDueDateWrtCashDiscountTerms2 in the 2nd query */


/* Update3 - Part 1 */


/* Update Dim_NetDueDateId1 */

UPDATE fact_accountspayable 
from BSIK arc, dim_Company dcm, dim_Vendor dv
SET Dim_NetDueDateId1 = 1
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId1 <> 1;

UPDATE fact_accountspayable 
from BSIK arc, dim_Company dcm, dim_Vendor dv,
DIM_DATE dt
SET Dim_NetDueDateId1 = dt.DIM_DATEID
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND dt.CompanyCode = arc.BSIK_BUKRS 
AND dt.DateValue = cast((ifnull(BSIK_ZFBDT, BSIK_BLDAT)+ (INTERVAL '1' DAY) * ifnull(BSIK_ZBD1T, 0)) AS DATE)
AND Dim_NetDueDateId1 <> dt.DIM_DATEID;

/* Update Dim_NetDueDateId2 */
UPDATE fact_accountspayable 
from BSIK arc, dim_Company dcm, dim_Vendor dv
SET Dim_NetDueDateId2 = 1
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId2 <> 1;

UPDATE fact_accountspayable 
from BSIK arc, dim_Company dcm, dim_Vendor dv,
DIM_DATE dt
SET Dim_NetDueDateId1 = dt.DIM_DATEID
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId1 <> 1
AND dt.CompanyCode = arc.BSIK_BUKRS 
AND dt.DateValue = cast((ifnull(BSIK_ZFBDT, BSIK_BLDAT)+ (INTERVAL '1' DAY) * ifnull(BSIK_ZBD2T, 0)) AS DATE)
AND Dim_NetDueDateId1 <> dt.DIM_DATEID;

/* Update Dim_NetDueDateId3 */
UPDATE fact_accountspayable 
from BSIK arc, dim_Company dcm, dim_Vendor dv
SET Dim_NetDueDateId3 = 1
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId3 <> 1;

UPDATE fact_accountspayable 
from BSIK arc, dim_Company dcm, dim_Vendor dv,
DIM_DATE dt
SET Dim_NetDueDateId1 = dt.DIM_DATEID
WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
AND dd_fiscalyear = arc.BSIK_GJAHR
AND dd_FiscalPeriod = arc.BSIK_MONAT
AND dcm.CompanyCode = arc.BSIK_BUKRS
AND dcm.RowIsCurrent = 1
AND dv.VendorNumber = arc.BSIK_LIFNR
AND dv.RowIsCurrent = 1
AND Dim_NetDueDateId1 <> 1
AND dt.CompanyCode = arc.BSIK_BUKRS 
AND dt.DateValue = cast((ifnull(BSIK_ZFBDT, BSIK_BLDAT)+ (INTERVAL '1' DAY) * ifnull(BSIK_ZBD3T, 0)) AS DATE)
AND Dim_NetDueDateId1 <> dt.DIM_DATEID;


/* Update3 - Part 2a */

UPDATE fact_accountspayable from BSIK arc, dim_Company dcm, dim_Vendor dv
   SET Dim_NetDueDateWrtCashDiscountTerms1 =
          ifnull((CASE BSIK_SHKZG WHEN 'S' THEN Dim_NetDueDateId1 END), 1),
       Dim_NetDueDateWrtCashDiscountTerms2 =
          ifnull((CASE BSIK_SHKZG WHEN 'S' THEN Dim_NetDueDateId1 END), 1)
 WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
       AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSIK_GJAHR
       AND dd_FiscalPeriod = arc.BSIK_MONAT
       AND dcm.CompanyCode = arc.BSIK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSIK_LIFNR
       AND dv.RowIsCurrent = 1;


/* Update3 - Part 2b */

UPDATE fact_accountspayable from BSIK arc, dim_Company dcm, dim_Vendor dv, DIM_DATE dt
   SET Dim_NetDueDateWrtCashDiscountTerms1 = DIM_DATEID
       WHERE dd_AccountingDocNo = arc.BSIK_BELNR
       AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSIK_GJAHR
       AND dd_FiscalPeriod = arc.BSIK_MONAT
       AND dcm.CompanyCode = arc.BSIK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSIK_LIFNR
       AND dv.RowIsCurrent = 1
       AND BSIK_ZBD1T IS NOT NULL
       AND dt.CompanyCode = arc.BSIK_BUKRS
       AND Dim_NetDueDateWrtCashDiscountTerms1 <> DIM_DATEID
       AND dt.DateValue = CAST((BSIK_ZFBDT + (INTERVAL '1' DAY) * ifnull(BSIK_ZBD1T, 0)) AS DATE);

/* Update3 - Part 2c */

UPDATE fact_accountspayable from BSIK arc, dim_Company dcm, dim_Vendor dv, DIM_DATE dt
   SET Dim_NetDueDateWrtCashDiscountTerms2 = DIM_DATEID
	WHERE     dd_AccountingDocNo = arc.BSIK_BELNR
       AND dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSIK_GJAHR
       AND dd_FiscalPeriod = arc.BSIK_MONAT
       AND dcm.CompanyCode = arc.BSIK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSIK_LIFNR
       AND dv.RowIsCurrent = 1
       AND BSIK_ZBD2T IS NOT NULL
       AND Dim_NetDueDateWrtCashDiscountTerms2 <> DIM_DATEID
       AND dt.CompanyCode = arc.BSIK_BUKRS
       AND dt.DateValue = CAST((BSIK_ZFBDT + (INTERVAL '1' DAY) * ifnull(BSIK_ZBD2T, 0)) AS DATE);

/* Update 4 */

DROP TABLE IF EXISTS TMP_FP_BSAK;
CREATE TABLE TMP_FP_BSAK
AS
SELECT *,
ifnull(BSAK_ZFBDT, BSAK_BLDAT) + int(ifnull(BSAK_ZBD1T, 0)) as bsak_datevalue1,
ifnull(BSAK_ZFBDT, BSAK_BLDAT) + int(ifnull(BSAK_ZBD2T, 0)) as bsak_datevalue2,
ifnull(BSAK_ZFBDT, BSAK_BLDAT) + int(ifnull(BSAK_ZBD3T, 0)) as bsak_datevalue3,
BSAK_ZFBDT + int(ifnull(BSAK_ZBD1T, 0)) as CDT1_datevalue,
BSAK_ZFBDT + int(ifnull(BSAK_ZBD2T, 0)) as CDT2_datevalue
FROM BSAK;


/* Update 4 - part 1 */

UPDATE fact_accountspayable from TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv, DIM_DATE dt
   SET Dim_NetDueDateId1 = DIM_DATEID
   WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR
       AND dv.RowIsCurrent = 1
       AND  dt.CompanyCode = arc.BSAK_BUKRS
       AND Dim_NetDueDateId1 <> DIM_DATEID
       AND dt.DateValue  = bsak_datevalue1;

UPDATE fact_accountspayable from TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv, DIM_DATE dt
       SET Dim_NetDueDateId2 = DIM_DATEID   
 WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR
       AND dv.RowIsCurrent = 1
       AND dt.CompanyCode = arc.BSAK_BUKRS
       AND Dim_NetDueDateId2 <> DIM_DATEID   
       AND dt.DateValue  = bsak_datevalue2;

UPDATE fact_accountspayable from TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv, DIM_DATE dt
       SET Dim_NetDueDateId3 = DIM_DATEID
            WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR
       AND dv.RowIsCurrent = 1
       AND dt.CompanyCode = arc.BSAK_BUKRS
       AND Dim_NetDueDateId3 <> DIM_DATEID
       AND dt.DateValue  = bsak_datevalue3;

/* Update 4 - part 2a */


UPDATE fact_accountspayable from BSAK arc, dim_Company dcm, dim_vendor dv
   SET Dim_NetDueDateWrtCashDiscountTerms1 =
          ifnull((CASE BSAK_SHKZG WHEN 'S' THEN Dim_NetDueDateId1 END), 1)
 WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR
       AND dv.RowIsCurrent = 1;

UPDATE fact_accountspayable from BSAK arc, dim_Company dcm, dim_vendor dv
   SET 
       Dim_NetDueDateWrtCashDiscountTerms2 =
          ifnull((CASE BSAK_SHKZG WHEN 'S' THEN Dim_NetDueDateId1 END), 1)
 WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR
       AND dv.RowIsCurrent = 1;


/* Update 4 - part 2b */


UPDATE fact_accountspayable from TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv
   SET Dim_NetDueDateWrtCashDiscountTerms1 =
          ifnull((SELECT DIM_DATEID
             FROM DIM_DATE dt
            WHERE dt.DateValue  = CDT1_datevalue
                  AND dt.CompanyCode = arc.BSAK_BUKRS), 1)
 WHERE     dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR
       AND dv.RowIsCurrent = 1
       AND BSAK_ZBD1T IS NOT NULL;


/* Update 4 - part 2c */

UPDATE fact_accountspayable from TMP_FP_BSAK arc, dim_Company dcm, dim_vendor dv, DIM_DATE dt
   SET Dim_NetDueDateWrtCashDiscountTerms2 = DIM_DATEID     
 WHERE dd_AccountingDocNo = arc.BSAK_BELNR
       AND dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND dd_fiscalyear = arc.BSAK_GJAHR
       AND dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR
       AND dv.RowIsCurrent = 1
       AND BSAK_ZBD2T IS NOT NULL
       AND dt.DateValue = CDT2_datevalue
       AND dt.CompanyCode = arc.BSAK_BUKRS
       AND Dim_NetDueDateWrtCashDiscountTerms2 <> DIM_DATEID;

/* Update 5  */
UPDATE fact_accountspayable fap from BSIK arc, dim_Company dcm, dim_Vendor dv
   SET amt_EligibleForDiscount = BSIK_SKFBT * amt_ExchangeRate
 WHERE     fap.dd_AccountingDocNo = arc.BSIK_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSIK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSIK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSIK_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSIK_MONAT
       AND dcm.CompanyCode = arc.BSIK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSIK_LIFNR
       AND dv.RowIsCurrent = 1
       AND amt_EligibleForDiscount <> (BSIK_SKFBT * amt_ExchangeRate);


/* Update 6  */

UPDATE fact_accountspayable fap from BSAK arc, dim_Company dcm, dim_Vendor dv
   SET amt_EligibleForDiscount = BSAK_SKFBT * amt_ExchangeRate
 WHERE     fap.dd_AccountingDocNo = arc.BSAK_BELNR
       AND fap.dd_AccountingDocItemNo = arc.BSAK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(arc.BSAK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = arc.BSAK_GJAHR
       AND fap.dd_FiscalPeriod = arc.BSAK_MONAT
       AND dcm.CompanyCode = arc.BSAK_BUKRS
       AND dcm.RowIsCurrent = 1
       AND dv.VendorNumber = arc.BSAK_LIFNR
       AND dv.RowIsCurrent = 1
       AND amt_EligibleForDiscount <> (BSAK_SKFBT * amt_ExchangeRate);

/* Update 7  */

DROP TABLE IF EXISTS tmp1_fact_accountspayable_MBEW;

CREATE TABLE tmp1_fact_accountspayable_MBEW
AS
   SELECT MATNR, BWKEY, MAX(MBEW_ZPLD1) max_MBEW_ZPLD1
     FROM MBEW
    WHERE MBEW_LBKUM > 0 AND BWTAR IS NULL
   GROUP BY MATNR, BWKEY;

DROP TABLE IF EXISTS tmp2_fact_accountspayable_MBEW;

CREATE TABLE tmp2_fact_accountspayable_MBEW
AS
   SELECT w.*
     FROM MBEW w, tmp1_fact_accountspayable_MBEW t
    WHERE     w.MATNR = t.MATNR
          AND w.BWKEY = t.BWKEY
          AND w.MBEW_ZPLD1 = t.max_MBEW_ZPLD1;



/* Update column fap.Dim_PartId */

UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
SET fap.Dim_PartId = 1
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.Dim_PartId <> 1;


UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
 ,dim_part p
SET fap.Dim_PartId = p.dim_partid
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
  AND p.Partnumber = rsg.RSEG_MATNR AND p.Plant = rsg.RSEG_WERKS AND p.RowIsCurrent = 1
AND fap.Dim_PartId <> p.dim_partid;


/* Update column fap.Dim_UnitOfMeasureId */

UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
SET fap.Dim_UnitOfMeasureId = 1
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.Dim_UnitOfMeasureId <> 1;


UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
 ,dim_unitofmeasure uom
SET fap.Dim_UnitOfMeasureId = uom.dim_unitofmeasureid
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
 AND uom.UOM = rsg.RSEG_BSTME AND uom.RowIsCurrent = 1
AND fap.Dim_UnitOfMeasureId <> uom.dim_unitofmeasureid;


/* Update column fap.amt_InvStdUnitPrice */

UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
SET fap.amt_InvStdUnitPrice = 0
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.amt_InvStdUnitPrice <> 0;


UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
 , tmp2_fact_accountspayable_MBEW w, dim_plant pl
SET fap.amt_InvStdUnitPrice = w.STPRS
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
  AND pl.PlantCode = rsg.RSEG_WERKS AND pl.RowIsCurrent = 1 AND w.MATNR = rsg.RSEG_MATNR AND w.BWKEY = pl.ValuationArea
AND fap.amt_InvStdUnitPrice <> w.STPRS;

/* Now update columns that did not have inner subquery */

UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
SET     fap.dd_PurchaseDocumentNo = rsg.RSEG_EBELN
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND     fap.dd_PurchaseDocumentNo  <>  rsg.RSEG_EBELN;

UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
SET fap.dd_PurchaseDocItemNo = rsg.RSEG_EBELP
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.dd_PurchaseDocItemNo  <>  rsg.RSEG_EBELP;

UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
SET fap.amt_InvoiceInDocCurrency = rsg.RSEG_WRBTR
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.amt_InvoiceInDocCurrency  <>  rsg.RSEG_WRBTR;

UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
SET fap.ct_InvoiceQty = rsg.RSEG_MENGE
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid
AND fap.ct_InvoiceQty  <>  rsg.RSEG_MENGE;

UPDATE fact_accountspayable fap
from rseg rsg, dim_date dt
SET fap.amt_InvPOUnitPrice = (CASE WHEN rsg.RSEG_BPMNG <> 0 THEN rsg.RSEG_WRBTR / rsg.RSEG_BPMNG ELSE 0 END)
WHERE     rsg.RSEG_BELNR = fap.dd_ReferenceDocumentNo
 AND  rsg.RSEG_GJAHR = fap.dd_FiscalYear
 AND  fap.Dim_DateIdAccDocDateEntered = dt.dim_dateid;



/* End of Update 7 */

/* Update 8 */

/* Update column Dim_MovementTypeid */

UPDATE fact_accountspayable fap
from ekbe et, dim_plant pl
SET Dim_MovementTypeid = 1
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
AND  fap.dd_PurchaseDocItemNo <> 0
AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
AND  et.EKBE_WERKS = pl.PlantCode
AND  pl.RowIsCurrent = 1
AND Dim_MovementTypeid <> 1;


UPDATE fact_accountspayable fap
from ekbe et, dim_plant pl
 ,dim_movementtype mt
SET Dim_MovementTypeid = mt.Dim_MovementTypeid
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
AND  fap.dd_PurchaseDocItemNo <> 0
AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
AND  et.EKBE_WERKS = pl.PlantCode
AND  pl.RowIsCurrent = 1
  AND mt.MovementType = et.EKBE_BWART AND mt.ConsumptionIndicator = 'Not Set' 
  AND mt.MovementIndicator = 'Not Set' AND mt.ReceiptIndicator = 'Not Set' 
  AND mt.SpecialStockIndicator = 'Not Set' AND mt.RowIsCurrent = 1
AND fap.Dim_MovementTypeid <> mt.Dim_MovementTypeid;


/* Update column fap.Dim_PoPartId */

UPDATE fact_accountspayable fap
from ekbe et, dim_plant pl
SET fap.Dim_PoPartId = 1
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
AND  fap.dd_PurchaseDocItemNo <> 0
AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
AND  et.EKBE_WERKS = pl.PlantCode
AND  pl.RowIsCurrent = 1
AND fap.Dim_PoPartId <> 1;


UPDATE fact_accountspayable fap
from ekbe et, dim_plant pl
 ,dim_part pt
SET fap.Dim_PoPartId = pt.Dim_Partid
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
AND  fap.dd_PurchaseDocItemNo <> 0
AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
AND  et.EKBE_WERKS = pl.PlantCode
AND  pl.RowIsCurrent = 1
  AND pt.PartNumber = et.EKBE_MATNR AND pt.Plant = et.EKBE_WERKS AND pt.RowIsCurrent = 1
AND fap.Dim_PoPartId <> pt.Dim_Partid;

UPDATE fact_accountspayable fap
from ekbe et, dim_plant pl
SET  fap.Dim_PoPlantId = pl.Dim_PlantId
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
 AND  fap.dd_PurchaseDocItemNo <> 0
 AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
 AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
 AND  et.EKBE_WERKS = pl.PlantCode
 AND  pl.RowIsCurrent = 1
AND  fap.Dim_PoPlantId  <>  pl.Dim_PlantId;

UPDATE fact_accountspayable fap
from ekbe et, dim_plant pl
SET fap.ct_PoQuantity = et.EKBE_MENGE
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
 AND  fap.dd_PurchaseDocItemNo <> 0
 AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
 AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
 AND  et.EKBE_WERKS = pl.PlantCode
 AND  pl.RowIsCurrent = 1
AND fap.ct_PoQuantity  <>  et.EKBE_MENGE;

UPDATE fact_accountspayable fap
from ekbe et, dim_plant pl
SET fap.amt_PoAmtInDocCurrency = et.EKBE_REEWR
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
 AND  fap.dd_PurchaseDocItemNo <> 0
 AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
 AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
 AND  et.EKBE_WERKS = pl.PlantCode
 AND  pl.RowIsCurrent = 1
AND fap.amt_PoAmtInDocCurrency  <>  et.EKBE_REEWR;

UPDATE fact_accountspayable fap
from ekbe et, dim_plant pl
SET
fap.dd_CreatedBy = ifnull(et.EKBE_ERNAM, 'Not Set')
WHERE     fap.dd_PurchaseDocumentNo <> 'Not Set'
 AND  fap.dd_PurchaseDocItemNo <> 0
 AND  fap.dd_PurchaseDocumentNo = et.EKBE_EBELN
 AND  fap.dd_PurchaseDocItemNo = et.EKBE_EBELP
 AND  et.EKBE_WERKS = pl.PlantCode
 AND  pl.RowIsCurrent = 1
AND fap.dd_CreatedBy  <>  ifnull(et.EKBE_ERNAM, 'Not Set');

/* End of Update 8 */


/* Update 9  */

UPDATE fact_accountspayable fap
   SET amt_StdPOUnitPrice = ifnull(
          (SELECT AVG(amt_StdUnitPrice)
             FROM fact_purchase p
            WHERE fap.dd_PurchaseDocumentNo = p.dd_DocumentNo
                  AND fap.dd_PurchaseDocItemNo = p.dd_DocumentItemNo
           GROUP BY p.dd_DocumentNo, p.dd_DocumentItemNo), 0)
 WHERE fap.dd_PurchaseDocumentNo <> 'Not Set'
       AND fap.dd_PurchaseDocItemNo <> 0;

/* Update 10  */
UPDATE fact_accountspayable fap from Dim_APMiscellaneous apmisc, BSIK bik
   SET fap.Dim_APMiscellaneousId = apmisc.Dim_APMiscellaneousId
 WHERE     fap.dd_AccountingDocNo = bik.BSIK_BELNR
       AND fap.dd_AccountingDocItemNo = bik.BSIK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(bik.BSIK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = bik.BSIK_GJAHR
       AND fap.dd_FiscalPeriod = bik.BSIK_MONAT
       AND apmisc.ItemsClearingReversed = ifnull(bik.BSIK_XRAGL, 'Not Set')
       AND apmisc.DocumentPostedYet = ifnull(bik.BSIK_XNETB, 'Not Set')
       AND fap.Dim_APMiscellaneousId <> apmisc.Dim_APMiscellaneousId;

/* Update 11  */

UPDATE fact_accountspayable fap from Dim_APMiscellaneous apmisc, BSAK bak
   SET fap.Dim_APMiscellaneousId = apmisc.Dim_APMiscellaneousId
 WHERE     fap.dd_AccountingDocNo = bak.BSAK_BELNR
       AND fap.dd_AccountingDocItemNo = bak.BSAK_BUZEI
       AND fap.dd_AssignmentNumber = ifnull(bak.BSAK_ZUONR, 'Not Set')
       AND fap.dd_fiscalyear = bak.BSAK_GJAHR
       AND fap.dd_FiscalPeriod = bak.BSAK_MONAT
       AND apmisc.ItemsClearingReversed = ifnull(bak.BSAK_XRAGL, 'Not Set')
       AND apmisc.DocumentPostedYet = ifnull(bak.BSAK_XNETB, 'Not Set')
       AND fap.Dim_APMiscellaneousId <> apmisc.Dim_APMiscellaneousId;

DROP TABLE IF EXISTS tmp_fact_accountspayable_MBEW;
DROP TABLE IF EXISTS tmp2_fact_accountspayable_MBEW;

DROP TABLE IF EXISTS TMP_FP_BSAK;
